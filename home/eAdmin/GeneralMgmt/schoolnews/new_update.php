<?php
# USING: 

############ Change Log [Start] ############
#   Date:   2020-03-05 (Ray)
#			added website code
#
#	Date:	2020-01-09 (Philips) [DM#3729]
#			Filter Suspend Target Group User
#			added parentStatusCheck in getParentStudentMappingInfo()
#
#	Date:	2019-12-05 (Philips) [2019-0628-1034-52206]
#			Send Push Message without cheching app login status
#
#   Date:   2019-09-10 (Tommy)
#           added $onTop for "display on top" function
#
#	Date:   2019-05-14 (Bill)
#           prevent SQL Injection
#
#   Date:   2019-04-29 (Anna)
#           added IntegerSafe for $email_alertavoid sql injection
#
#   Date:   2019-01-09 (Anna)
#           Added $sys_custom['SchoolNews']['DisplayEngTitleAndContent']
#
#   Date:   2018-12-27 (Anna)
#           added access right for $sys_custom['DHL'] PIC
#
#   Date:   2018-12-21 Anna
#           Added $sys_custom['DHL'] map groupid with departmentid 
#
#	Date:	2017-09-26	Bill	[2017-0904-1103-25073]
#			Send to Guardian if $stu_parent_alert = 1
#
#	Date:   2017-07-11	(HenryHM)
#			Stop including the child of parents
#
#	Date:   2016-10-31	(Villa)
#			unset the GroupID if the target is ALLUSER
#
#	Date:	2016-03-03	Kenneth
#			add schedule push message and add fromModule into INTRANET_APP_NOTIFY_MESSAGE_MODULE_RELATION
#
#	Date:	2014-10-14	Roy
#			send push message to parent and teacher that included in the selected groups
#
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
#	Date:	2011-03-28	YatWoon
#			change email notification subject & content data 
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
# - 2010-06-15 YatWoon
#	remove flag checking $special_announce_public_allowed, no need for this checking and assume this feature is as a general
#
# - 2010-04-13 YatWoon
#	change the description from textarea to fck editor (with upload image with Flash function)
#
############ Change Log [End] ############

	$CurrentPageArr['schoolNews'] = 1;
	
	$PATH_WRT_ROOT = "../../../../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libgrouping.php");
	include_once($PATH_WRT_ROOT."includes/libemail.php");
	include_once($PATH_WRT_ROOT."includes/libsendmail.php");
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
// 	include_once($PATH_WRT_ROOT."lang/email.php");
	include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	
	intranet_auth();
	intranet_opendb();
	
	if($sys_custom['DHL']){
	    include_once($PATH_WRT_ROOT."includes/DHL/libdhl.php");
	    $libdhl = new libdhl();
	    $isPIC = $libdhl->isPIC();
	}

	if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"] || ($isPIC && $sys_custom['DHL']))){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	
	### Handle SQL Injection + XSS [START]
	$AnnouncementID = IntegerSafe($AnnouncementID);
	$Target_user = IntegerSafe($Target_user);
	$GroupID = IntegerSafe($GroupID);
	$RecordStatus = IntegerSafe($RecordStatus);
	$onTop = IntegerSafe($onTop);
	### Handle SQL Injection + XSS [END]
	
	$li = new libgrouping();
	
	$Title = intranet_htmlspecialchars(trim($Title));
	if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
	    $TitleEng = intranet_htmlspecialchars(trim($TitleEng));
	}

	if($sys_custom['SchoolNews']['ShowWebsiteCode']) {
		$WebsiteCode = intranet_htmlspecialchars(trim($WebsiteCode));
		$WebsiteCode = strtoupper($WebsiteCode);
	}

	$pushMsgTitle = standardizeFormPostValue($_POST['Title']);
	if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
	{
		if ($Description==strip_tags($Description))
		{
			$Description = nl2br($Description);
		}
		
		if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
		    if ($DescriptionEng == strip_tags($DescriptionEng))
		    {
		        $DescriptionEng= nl2br($DescriptionEng);
		    }
		}
	}
	$Description = intranet_htmlspecialchars(trim($Description));
	if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
        $DescriptionEng = intranet_htmlspecialchars(trim($DescriptionEng));
	}
	
	$AnnouncementDate = intranet_htmlspecialchars(trim($AnnouncementDate));
	$EndDate = intranet_htmlspecialchars(trim($EndDate));
	$startStamp = strtotime($AnnouncementDate);
	$endStamp = strtotime($EndDate);
	
	$name_field = getNameFieldForRecord("");
	$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID = '$UserID'";
	$temp = $li->returnVector($sql);
	$PosterName = $temp[0];
	
	if($Target_user=='1') {
		unset($GroupID);
	}
	
	if($sys_custom['DHL']){
	    include_once($intranet_root."/includes/DHL/libdhl.php");
	    $libdhl = new libdhl();

	    $GroupID = $libdhl->getMappingIntranetGroupID($GroupID);
// 	    debug_pr($GroupID);
	}

	if (!intranet_validateDate($AnnouncementDate) || !intranet_validateDate($EndDate))
	{
	    $valid = false;
	}
	else if (compareDate($startStamp, $endStamp) > 0)   		// Start > End
	{
		$valid = false;
	}
	else if (compareDate($startStamp, time()) < 0)     // Start < Now
	{
		$valid = false;
	}
	else
	{
		$valid = true;
	}
	
	if ($valid)
	{
		$lf = new libfilesystem();
		
		# Set Attachment
		$folder = session_id()."_a";
		
		if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
		    $AdditionFields = " , TitleEng ";
		    $AdditionValues = " ,'$TitleEng' ";
		}

		if($sys_custom['SchoolNews']['ShowWebsiteCode']) {
			$AdditionFields .= " , WebsiteCode ";
			$AdditionValues .= " ,'$WebsiteCode' ";
		}

		# Insert Announcement
		$sql = "INSERT INTO INTRANET_ANNOUNCEMENT (Title, Description, AnnouncementDate, ReadFlag, UserID, PosterName, RecordStatus, DateInput, DateModified, EndDate, onTop $AdditionFields) VALUES ('$Title', '$Description', '$AnnouncementDate', ';$UserID;', '$UserID', '$PosterName', '$RecordStatus', now(), now(),'$EndDate', '$onTop' $AdditionValues)";
		$li->db_db_query($sql);
		$AnnouncementID = $li->db_insert_id();
		
		## Update Description if user upload image with Flash upload (fck) [Start]
		// $Description2 = stripslashes(intranet_undo_htmlspecialchars($Description));
		$Description2 = stripslashes(htmlspecialchars_decode($Description));
		$Description_updated = ($lf->copy_fck_flash_image_upload($AnnouncementID, $Description2, $PATH_WRT_ROOT, $cfg['fck_image']['SchoolNews'])); 
		$Description_updated = trim(intranet_htmlspecialchars($Description_updated));
		
		if($sys_custom['SchoolNews']['DisplayEngTitleAndContent']){
		    $DescriptionEng2 = stripslashes(trim($DescriptionEng));
		    $DescriptionEng_updated = ($lf->copy_fck_flash_image_upload($AnnouncementID.'en', $DescriptionEng2, $file_path, $cfg['fck_image']['SchoolNews']));
		    $DescriptionEng_updated= intranet_htmlspecialchars(trim($DescriptionEng_updated));
		    $AdditionFields = " , DescriptionEng = '".$li->Get_Safe_Sql_Query($DescriptionEng_updated)."'";
		}
        
		$sql = "UPDATE INTRANET_ANNOUNCEMENT SET Description = '".$li->Get_Safe_Sql_Query($Description_updated)."' $AdditionFields WHERE AnnouncementID = '$AnnouncementID'";
 		$li->db_db_query($sql);
		## Update Description if user upload image with Flash upload (fck) [End]
		
		# Insert Group Relation
		for($i=0; $i<sizeof($GroupID); $i++){
             $sql = "INSERT INTO INTRANET_GROUPANNOUNCEMENT (GroupID, AnnouncementID) VALUES ('".$GroupID[$i]."', '$AnnouncementID')";
             $li->db_db_query($sql);
		}
		
		# Copy files
		$path = "$file_path/file/announcement/$folder$AnnouncementID";
		$hasAttachment = false;
		$attachment_size = $attachment_size==""? 0 : $attachment_size;
		for ($i=0; $i<$attachment_size; $i++)
		{
			$key = "filea$i";
			$loc = ${"filea$i"};
			$file = stripslashes(${"hidden_userfile_name$i"});
			
			#$file = ${"filea$i"."_name"};
			$des = "$path/$file";
			if ($loc == "none" || $loc=="")
			{
				// do nothing
			} 
			else
			{
				if (strpos($file,"."==0)) {
					// do nothing
				}
				else{
                   if (!$hasAttachment)
                   {
                        //$lf->folder_new("$file_path/file/announcement");
                        $lf->folder_new ($path);
                        $hasAttachment = true;
                   }
                   $lf->lfs_copy($loc, $des);
				}
			}
		}
		
		# Update Attachment in DB
		if ($hasAttachment)
		{
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET Attachment = '$folder' WHERE AnnouncementID = '$AnnouncementID'";
			$li->db_db_query($sql);
		}
		
		# Copy Power Voice files
		if ($plugin['power_voice'])
		{
			if (($voiceFile!="")  && ($voicePath!="")) {	        
				if (!$hasAttachment){
					$lf->folder_new ($path);
					$hasAttachment = true;
				}
				
				$loc = $voicePath."/".$voiceFile;
				$des = $path."/".$voiceFile;
				$lf->lfs_move($loc, $des);     
				
				$sql = "UPDATE 
         					INTRANET_ANNOUNCEMENT 
         				SET 
         					VoiceFile = '".addslashes($des)."' 
         				WHERE 
         					AnnouncementID = '{$AnnouncementID}'
         				";
				$li->db_db_query($sql);
			}
		}
		
		# Update Announcement for All users
		if ($Target_user == 1)
		{
			$sql = "UPDATE INTRANET_ANNOUNCEMENT SET RecordType = 1 WHERE AnnouncementID = '$AnnouncementID'";
			$li->db_db_query($sql);
		}
		
		# Publish Notification
		$type = (sizeof($GroupID)==0? $Lang['EmailNotification']['SchoolNews']['School']: $Lang['EmailNotification']['SchoolNews']['Group']);
		
		# Get Target Group Users
		if (sizeof($GroupID)>0)
		{
			$Groups = $li->returnGroupNames($GroupID);
		}
		if(sizeof($GroupID) == 0)
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordStatus = '1'";
		}
		else
		{
// 			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID IN ('".implode("', '", $GroupID)."')";
			# 2020-01-09 (Philips) [DM#3729] - Filter Suspend User
			$sql = "SELECT iu.UserID FROM INTRANET_USERGROUP iug 
					LEFT JOIN INTRANET_USER iu ON iug.UserID = iu.UserID
					WHERE iug.GroupID IN ('".implode("', '", $GroupID)."') AND iu.RecordStatus = '1'";
		}
		$ToArray = $li->returnVector($sql);
		
		include_once($PATH_WRT_ROOT."includes/libschoolnews.php");
		$lschoolnews = new libschoolnews();
		$email_alert = IntegerSafe($email_alert);
		# Send Mail
		if($email_alert==1)
		{
			list($mailSubject, $mailBody) = $lschoolnews->returnEmailNotificationData($AnnouncementDate, $pushMsgTitle, $type, $Groups);
			
			$lwebmail = new libwebmail();
			$lwebmail->sendModuleMail($ToArray, $mailSubject, $mailBody);
		}
		
		# Send Push Message
		if ($push_message_alert == 1)
		{
			include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$libeClassApp = new libeClassApp();
			
			# Build Teacher and Student array
			$teacherAry = array();
			$studentAry = array();
            $studentAry_parent = array();
			foreach ($ToArray as $userId) {
			    $luser = new libuser($userId);

				if ($luser->RecordType == 1) { 			// Teacher
					$teacherAry[] = $userId;
				}
				else if ($luser->RecordType == 2) { 	// Student
					$studentAry[] = $userId;
					
					// [2017-0904-1103-25073]
					if(!$sys_custom['schoolNews']['disableSendToParentAlert'] && $stu_parent_alert == 1) {
						$studentAry_parent[] = $userId;
					}
				}
				else if ($luser->RecordType == 3) { 	// Parent (Get related Students)
					$childrenIds = $luser->getChildren();
					$studentAry_parent = array_merge($studentAry_parent, $childrenIds);
				}
			}
			
			# Filter Student and Teaching array (Check if using eClass App)
// 			$studentsWithParentUsingParentApp = $luser->getStudentWithParentUsingParentApp();

// 			$teachersUsingTeacherApp = $luser->getTeacherWithTeacherUsingTeacherApp();
// 			$studentsUsingStudentApp = $luser->getStudentWithStudentUsingStudentApp($includeNoPushMessageUser=true);
// 			$studentAryForParentApp = array_intersect($studentAry_parent, $studentsWithParentUsingParentApp);
// 			$teacherAry = array_intersect($teacherAry, $teachersUsingTeacherApp);
// 			$studentAryForStudentApp = array_intersect($studentAry, $studentsUsingStudentApp);
			
			### 20191205 Philips [2019-0628-1034-52206]
			### Send Push Message Whether app login status
			if(!$sys_custom['schoolNews']['disableSendToParentAlert'] && $stu_parent_alert == 1) {
				$studentAryForParentApp = array_merge($studentAry_parent, $studentAry);
			} else {
				$studentAryForParentApp = $studentAry_parent;
			}
			$studentAryForStudentApp = $studentAry;
			
			# Build Message Content
			$isPublic = "N";
			$sendTimeMode = standardizeFormPostValue($_POST['sendTimeMode']);
			$sendTimeString = standardizeFormPostValue($_POST['sendTimeString']);
			list($pushmessage_subject, $pushmessage_body) = $lschoolnews->returnPushMessageNotificationData($AnnouncementDate, $pushMsgTitle, $type, $Groups);
			
			# Send Message to Parent App
			$appType = $eclassAppConfig['appType']['Parent'];
			$individualMessageInfoAry = array();
			if (!empty($studentAryForParentApp)) {
				$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($studentAryForParentApp, '', false, $parentStatusCheck=true), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
			}
			else {
				$parentStudentAssoAry = array();
			}
	    	$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
			if (!empty($parentStudentAssoAry)) {
				if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
					$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
				else {
					$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
			}
			
			# Send Message to Teacher App
			$appType = $eclassAppConfig['appType']['Teacher'];
			$teacherIndividualMessageInfoAry = array();
			if (!empty($teacherAry)) {
				foreach ($teacherAry as $teacherId) {
					$_targetTeacherId = $libeClassApp->getDemoSiteUserId($teacherId);
					// link the message to be related to oneself
					$teacherAssoAry[$teacherId] = array($_targetTeacherId);
				}
			}
			else {
				$teacherAssoAry = array();
			}
	    	$teacherIndividualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
			if (!empty($teacherAssoAry)) {
				if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
					$notifyMessageId = $libeClassApp->sendPushMessageByBatch($teacherIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
				else {
					$notifyMessageId = $libeClassApp->sendPushMessage($teacherIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
			}
			
			# Send Message to Student App
			$appType = $eclassAppConfig['appType']['Student'];
			$studentAssoAry = array();
			$studentIndividualMessageInfoAry = array();
			if (!empty($studentAryForStudentApp)) {
				foreach ($studentAryForStudentApp as $_studentId) {
					$_targetStudentId = $libeClassApp->getDemoSiteUserId($_studentId);
					
					// link the message to be related to oneself
					$studentAssoAry[$_studentId] = array($_targetStudentId);
				}
			}
			else {
				$studentAssoAry = array();
			}
	    	$studentIndividualMessageInfoAry[0]['relatedUserIdAssoAry'] = $studentAssoAry;
			if (!empty($studentAssoAry)) {
				if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
					$notifyMessageId = $libeClassApp->sendPushMessageByBatch($studentIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
				else {
					$notifyMessageId = $libeClassApp->sendPushMessage($studentIndividualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString, '', '', 'SchoolNews', $AnnouncementID);
				}
			}
		}
		
		intranet_closedb();
		$msg = "add";
		header("Location: index.php?status=$RecordStatus&msg=$msg");
	}
	else
	{
		header("Location: new.php?t=$Title&d=$Description&ad=$AnnouncementDate&ed=$EndDate");
	}
?>