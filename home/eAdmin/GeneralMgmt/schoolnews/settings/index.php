<?php 
# using: 

################# Change Log [Start] ############
#-	2016-09-23(Villa)
#	add to module setting
#		"default using eclass App to notify"
#
# 
# - 2016-05-13 (Bill)	[2016-0513-1440-15206]
#	form method use post instead of get
#
# - 2011-03-10 (Henry Chow)
#	allow System Admin to view "School News"
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if(!($_SESSION['SSV_USER_ACCESS']["other-schoolNews"] || $_SESSION["SSV_PRIVILEGE"]["schoolsettings"]["isAdmin"])){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libschoolnews.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lschoolnews = new libschoolnews();
$CurrentPageArr['schoolNews'] = 1;
$CurrentPage = "SchoolNewsSettings";

# Left menu 
$TAGS_OBJ[] = array($Lang['eNotice']['Settings']);
$MODULE_OBJ = $lschoolnews->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($xmsg);


for($i=0;$i<=30;$i++)
	$data[] = array($i, $i);
$defaultNumDays_selection = getSelectByArray($data, "name='defaultNumDays'", $lschoolnews->defaultNumDays , 0, 1);	


// allowUserToViewPastNews settings
$htmlAry['allowUserToViewPastNewsDisplay'] = $lschoolnews->allowUserToViewPastNews? $Lang['General']['Yes'] : $Lang['General']['No']; 

$yes_checked = $lschoolnews->allowUserToViewPastNews? true : false;
$no_checked = $lschoolnews->allowUserToViewPastNews? false : true;
$x = '';
$x .= $linterface->Get_Radio_Button('allowUserToViewPastNews_yes', 'allowUserToViewPastNews', 1, $yes_checked, $Class="", $Lang['General']['Yes']);
$x .= '&nbsp;';
$x .= $linterface->Get_Radio_Button('allowUserToViewPastNews_no', 'allowUserToViewPastNews', 0, $no_checked, $Class="", $Lang['General']['No']);
$htmlAry['allowUserToViewPastNewsRadio'] = $x;

$yes_checked = $lschoolnews->sendPushMsg? true : false;
$no_checked = $lschoolnews->sendPushMsg? false : true;
$htmlAry['sendPushMsgDisplay'] = $yes_checked = $lschoolnews->sendPushMsg? $Lang['General']['Yes']: $Lang['General']['No'];
$htmlAry['sendPushMsg'] = $lschoolnews->allowUserToViewPastNews? $Lang['General']['Yes'] : $Lang['General']['No'];
$x = ''; //reset $x
$x .= $linterface->Get_Radio_Button('sendPushMsg_yes', 'sendPushMsg', 1, $yes_checked, $Class="", $Lang['General']['Yes']);
$x .= '&nbsp;';
$x .= $linterface->Get_Radio_Button('sendPushMsg_no', 'sendPushMsg', 0, $no_checked, $Class="", $Lang['General']['No']);

$htmlAry['sendPushMsg'] = $x;

?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}
//-->
</script>
		
<form name="form1" method="post" action="edit_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$eDiscipline['SettingName']?> </span>-</em>
		<p class="spacer"></p>
	</div>

	<table class="form_table_v30">
	
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['SchoolNews']['DefaultNumDays']?></td>
				<td>
				<span class="Edit_Hide"><?=$lschoolnews->defaultNumDays?></span>
				<span class="Edit_Show" style="display:none"><?=$defaultNumDays_selection?></span></td>
				
			</tr>
			
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['SysMgr']['SchoolNews']['AllowUserToViewPastNews']?></td>
				<td>
				<span class="Edit_Hide"><?=$htmlAry['allowUserToViewPastNewsDisplay']?></span>
				<span class="Edit_Show" style="display:none"><?=$htmlAry['allowUserToViewPastNewsRadio']?></span></td>
			</tr>
			<?php if($plugin['eClassApp']){ ?>
			<tr>
				<td class="field_title" nowrap><?= $Lang['eClassApp']['DefaultUserNotifyUsingApp']?></td>
				<td>
				<span class="Edit_Hide"><?=$htmlAry['sendPushMsgDisplay']?></span>
				<span class="Edit_Show" style="display:none"><?= $htmlAry['sendPushMsg'] ?></span> </td>
			</tr>
			<?php }?>
			
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>




<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
