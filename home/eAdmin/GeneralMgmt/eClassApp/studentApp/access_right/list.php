<?php
// using : 
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$returnMsgKey = $_GET['returnMsgKey'];


$CurrentPage = 'StudentApp_AccessRight';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['FunctionAccessRight']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


### get settings of enable / disable Student App
$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$enableStudentApp = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp']);
$enableStudentAppDisplay = ($enableStudentApp) ? $Lang['General']['Yes'] : $Lang['General']['No'];

### get all modules that can be applied to student app
$appType = $eclassAppConfig['appType']['Student'];
$appApplicableModuleAry = $indexVar['libeClassApp']->getAppApplicableModule($appType);
$numOfApplicableModule = count($appApplicableModuleAry);


### get access right settings of each modules
//$accessRightAssoAry[$moduleCode][$yearId]['RecordStatus'] = 1
$accessRightAssoAry = $indexVar['libeClassApp']->getAccessRightInfo($appType);


### get forms data
$libYear = new Year();
$formAry = $libYear->Get_All_Year_List();
$numOfForm = count($formAry);


### calculate column width
$moduleColWidth = 25;
if ($numOfForm == 0) {
	$formColWidth = 0;
}
else {
	$formColWidth = floor((100 - $moduleColWidth) / $numOfForm);
}

### construct table content
if ($enableStudentApp) {
$x = '';
$x .='<div id="parent">';
	$x .= '<table id="fixTable"  class="common_table_list_v30 view_table_list_v30 table">'."\r\n";
//	$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
		$x .= '<thead>'."\r\n";
			$x .= '<tr>'."\r\n";
				$x .= '<th style="width:'.$moduleColWidth.'%;">&nbsp;</th>'."\r\n";
				for ($i=0; $i<$numOfForm; $i++) {
					$_formName = $formAry[$i]['YearName'];
					$x .= '<th style="width:'.$formColWidth.'%; text-align:center;">'.$_formName.'</th>';
				}
			$x .= '</tr>'."\r\n";
		$x .= '</thead>'."\r\n";
		$x .= '<tbody>'."\r\n";
			for ($i=0; $i<$numOfApplicableModule; $i++) {
				$_moduleCode = $appApplicableModuleAry[$i];
				$_moduleName = $indexVar['libeClassApp']->getModuleName($_moduleCode);
				if($_moduleCode == $eclassAppConfig['moduleCode']['eAttendance'] || $_moduleCode == $eclassAppConfig['moduleCode']['groupMessage'] || $_moduleCode == $eclassAppConfig['moduleCode']['schoolInfo']){
					$_moduleName .= ' <a id="'.$_moduleCode.'" href="#" onclick="goAdvanceSetting(this.id);return false;">['.$Lang['eClassApp']['AdvancedSetting'].']</a>';
				}
				
				$x .= '<tr>'."\r\n";
					$x .= '<td>'.$_moduleName.'</td>'."\r\n";
					for ($j=0; $j<$numOfForm; $j++) {
						$__yearId = $formAry[$j]['YearID'];
						$__isModuleEnabled = $accessRightAssoAry[$__yearId][$_moduleCode]['RecordStatus'];
						
						if ($__isModuleEnabled) {
							$__imageName = 'icon_tick_green.gif';
						}
						else {
							$__imageName = 'icon_delete_b.gif';
						}
						
						$x .= '<td style="text-align:center;">'."\r\n";
							$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/'.$__imageName.'" width="18" height="18" border="0" align="absmiddle">'."\r\n";
						$x .= '</td>'."\r\n";
					}
				$x .= '</tr>'."\r\n";
			}
		$x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
$x .='</div>';
	$htmlAry['accessRightTable'] = '<br /><br />'.$x;
}


$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['EnableStudentApp'].'</td>'."\r\n";
			$x .= '<td>'.$enableStudentAppDisplay.$htmlAry['accessRightTable'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;





### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');


?>
<script src="/templates/jquery/jquery-2.1.3.js"></script>
<script src="/templates/jquery/tableHeadFixer.js"></script>
<script type="text/javascript">
$(document).ready(function() {	
	var tableheight = $('#fixTable').css('height');
	var taleheightwithoutpx = parseInt(tableheight);
	if(taleheightwithoutpx > Get_Screen_Height()-130){
		var parentheight =  Get_Screen_Height()-130;
		}else{
			var parentheight = taleheightwithoutpx;
			}
	$('#parent').css('height',parentheight).css('width', Get_Screen_Width()-250);
		
	$("#fixTable").tableHeadFixer({"left" : 1}); 
	$("#fixTable").tableHeadFixer({
		head: true,
		foot: false
	});	
});
function goEdit() {
	window.location = '?task=studentApp/access_right/edit';
}

function goAdvanceSetting(moduleCode){
	var settingPath = '';
	if (moduleCode =='<?=$eclassAppConfig['moduleCode']['schoolInfo']?>'){
		window.location = '?appType=<?=$eclassAppConfig['appType']['Common']?>&task=commonFunction/school_info/index';
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>