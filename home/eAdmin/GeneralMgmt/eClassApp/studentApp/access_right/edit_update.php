<?php
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$moduleEnableAssoAry = $_POST['moduleEnableAssoAry'];
$enableStudentApp = IntegerSafe(standardizeFormPostValue($_POST['enableStudentApp']));


$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$successAry = array();


### save send right to DB
$tmpSettingsAssoAry = array();
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableStudentApp']] = $enableStudentApp;
$successAry['saveEnableStatus'] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);



### get all modules that can be applied to parent app
$appType = $eclassAppConfig['appType']['Student'];
$appApplicableModuleAry = $indexVar['libeClassApp']->getAppApplicableModule($appType);
$numOfApplicableModule = count($appApplicableModuleAry);


### get forms data
$libYear = new Year();
$formAry = $libYear->Get_All_Year_List();
$numOfForm = count($formAry);

$consolidatedModuleAccessRightAssoAry = array();
for ($i=0; $i<$numOfApplicableModule; $i++) {
	$_moduleCode = $appApplicableModuleAry[$i];
	
	for ($j=0; $j<$numOfForm; $j++) {
		$__yearId = $formAry[$j]['YearID'];
		
		$__enableModuleInForm = ($moduleEnableAssoAry[$_moduleCode][$__yearId])? true : false;
		$consolidatedModuleAccessRightAssoAry[$_moduleCode][$__yearId] = $__enableModuleInForm;
	}
}

$successAry['saveFunctionAccessRight'] = $indexVar['libeClassApp']->saveModuleAccessRightSettings($appType, $consolidatedModuleAccessRightAssoAry);
$returnMsgKey = !in_array(false, (array)$successAry)? 'UpdateSuccess' : 'UpdateUnsuccess';

header('location: ?task=studentApp/access_right/list&returnMsgKey='.$returnMsgKey);
?>