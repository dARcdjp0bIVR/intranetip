<?php
// using :
/*
 * Change Log:
 *  2019-12-09 [Philips] : Fixed access right checking error
 *  2019-10-18 [Philips] : Added Class Teacher Only for parent login status
 * 	2012-10-05 [Ivan]: created this file
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."lang/eclassapp_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

$appType = ($_POST['appType'])? $_POST['appType'] : $_GET['appType'];
if ($appType == '') {
	$appType = $eclassAppConfig['appType']['Common'];
}  

$indexVar['libeClassApp'] = new libeClassApp();
$indexVar['libeClassApp']->checkAccessRight($appType);

$hasLicenseParent = $indexVar['libeClassApp']->isSchoolInLicense($eclassAppConfig['appType']['Parent']);
$hasLicenseTeacher = $indexVar['libeClassApp']->isSchoolInLicense($eclassAppConfig['appType']['Teacher']);

$CurrentPageArr['eClassApp'] = 1;
$indexVar['linterface'] = new interface_html();

$task = '';
if ($_POST['task'] != '') {
	$task = $_POST['task'];
}
else if ($_GET['task'] != '') {
	$task = $_GET['task'];
}


### Include corresponding php files
$indexVar['taskScript'] = '';
if ($task == '') {
	// go to module index page if not defined
	if ($sys_custom['DHL']) {
		$indexVar['taskScript'] .= 'teacherApp/login_status/list.php'; 
	}
	else {
		if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] && $plugin['eClassApp'] && $hasLicenseParent) {
			$indexVar['taskScript'] .= 'parentApp/access_right/list.php';
		}
		else if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"] && $plugin['eClassApp'] && $hasLicenseParent) {
			$indexVar['taskScript'] .= 'studentApp/access_right/list.php';
		}
		else if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"] && $plugin['eClassTeacherApp'] && $hasLicenseTeacher) {
			$indexVar['taskScript'] .= 'teacherApp/access_right/list.php';
		}
		else if (($plugin['eClassApp'] || $plugin['eClassTeacherApp']) && $_SESSION["SSV_PRIVILEGE"]["eClassApp"]["GroupMessage_GroupAdmin"]) {
			$indexVar['taskScript'] .= 'commonFunction/group_message/group_list.php';
		} else if($_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher']){
			$indexVar['taskScript'] .= "parentApp/login_status/list.php";
		}
	}
}
else {
	if($_SESSION["SSV_PRIVILEGE"]["eClassApp"]['classTeacher'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"]  && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassStudentApp"] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]
			&& ($task != 'parentApp/login_status/list' && $task != 'parentApp/login_status/report')){
		No_Access_Right_Pop_Up();
	}
	$indexVar['taskScript'] .= $task.'.php';
}

if (file_exists($indexVar['taskScript'])){
	include_once($indexVar['taskScript']);
}
else {
	No_Access_Right_Pop_Up();
}
intranet_closedb();
?>