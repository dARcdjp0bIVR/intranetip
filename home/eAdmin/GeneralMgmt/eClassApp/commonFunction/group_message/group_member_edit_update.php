<?php
// using : 
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();

$groupId = IntegerSafe($_POST['GroupID']);
$fromNew = IntegerSafe($_POST['FromNew']);
$appType = $_POST['appType'];
$userIdAry = Get_User_Array_From_Common_Choose($_POST['SelectedUserIdAry'], array(USERTYPE_STAFF, USERTYPE_PARENT));


$maxNumOfMember = $leClassApp_groupMessage->getMaxNumOfGroupMember();
$memberUserInfoAry = $leClassApp_groupMessage->getGroupMemberList($groupId);
$numOfMember = count($memberUserInfoAry);
$canAddMember = ( ($numOfMember + count($userIdAry)) > $maxNumOfMember)? 0 : 1;
if (!$canAddMember) {
	No_Access_Right_Pop_Up();
}


$successAry = array();
$indexVar['libeClassApp']->Start_Trans();

$successAry['saveLocal'] = $leClassApp_groupMessage->addGroupMember($groupId, $userIdAry);
if ($successAry['saveLocal']) {
	$successAry['saveCloud'] = $leClassApp_groupMessage->saveGroupMemberInCloud($groupId);
}


if (in_array(false, $successAry)) {
	$returnMsgKey = 'UpdateUnsuccess';
	$indexVar['libeClassApp']->RollBack_Trans();
}
else {
	$returnMsgKey = 'UpdateSuccess';
	$indexVar['libeClassApp']->Commit_Trans();
}

//if ($fromNew) {
//	$task = 'commonFunction/group_message/group_list';
//}
//else {
//	$task = 'commonFunction/group_message/group_list';
//}
$task = 'commonFunction/group_message/group_member';
header('location: ?appType='.$appType.'&task='.$task.'&returnMsgKey='.$returnMsgKey.'&GroupID='.$groupId);
?>