<?php

#editing: 

/******** Change Log ***********
 * 
 *  20180427 Isaac
 *              - Changed member selection to common User selection plugin from common choose.
 * 
 * 	20170615 Icarus
 * 				- Changed javascript:newWindow() to show_dyn_thickbox_ip()
 * 
 ********************************/

include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();

$returnMsgKey = $_GET['returnMsgKey'];
$GroupID = isset($_POST['GroupID'])? $_POST['GroupID'] : $_GET['GroupID'];
$GroupID = IntegerSafe($GroupID);
$appType = isset($_POST['appType'])? $_POST['appType'] : $_GET['appType'];
$FromNew = $_GET['FromNew'];


$maxNumOfMember = $leClassApp_groupMessage->getMaxNumOfGroupMember();
$memberUserInfoAry = $leClassApp_groupMessage->getGroupMemberList($GroupID);
$numOfMember = count($memberUserInfoAry);
$canAddMember = ($numOfMember >= $maxNumOfMember)? 0 : 1;


$CurrentPage = 'CommonFunction_GroupMessage';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();

$TAGS_OBJ[] = array($Lang['eClassApp']['ModuleNameAry']['GroupMessage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['Group'], "javascript:goGroupList()"); 
if ($FromNew == 1) {
	$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

	### Step Table
	$STEPS_OBJ[] = array($Lang['eClassApp']['Info'], false);
	$STEPS_OBJ[] = array($Lang['eClassApp']['SelectUser'], true);
	$htmlAry['stepTable'] = $indexVar['linterface']->GET_STEPS_IP25($STEPS_OBJ);
	
	### Cancel Button
	$htmlAry['cancelBtn'] = $indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="goGroupList()", $id="Btn_Cancel");
	
	$groupMemberUserIdAry = array();
}
else
{
	$PAGE_NAVIGATION[] = array($Lang['eClassApp']['MemberList'], "javascript:goMemberList();");
	$PAGE_NAVIGATION[] = array($Lang['eClassApp']['AddMember'], "");

	### Cancel Button
	$htmlAry['cancelBtn'] = $indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="goMemberList()", $id="Btn_Cancel");
	
	$groupMemberUserIdAry = Get_Array_By_Key($leClassApp_groupMessage->getGroupMemberList($GroupID), 'UserID');
}


### Get group info
$infoAry = $leClassApp_groupMessage->getGroupData($GroupID);


### Navigation
$htmlAry['navigation'] = $indexVar['linterface']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);


//### Choose Member Btn
// $userSel = '<select id="SelectedUserIDSel" name="SelectedUserIdAry[]" multiple="multiple" size="10"></select>';

$permittedTypeAry = array();
if ($plugin['eClassApp']) {
	$permittedTypeAry[] = USERTYPE_PARENT;
}
if ($plugin['eClassTeacherApp']) {
	$permittedTypeAry[] = USERTYPE_STAFF;
}

$excludeUserIdParam = '&exclude_user_id_ary[]='.implode('&exclude_user_id_ary[]=', $groupMemberUserIdAry);
//$param = "fieldname=SelectedUserIdAry[]&page_title=SelectMembers&permitted_type=".implode(',',$permittedTypeAry)."&excluded_type=4&filterAppParent=1&filterAppTeacher=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1&includeNoPushMessageUser=1".$excludeUserIdParam;
$param = "fieldname=SelectedUserIdAry[]&page_title=SelectMembers&permitted_type=".implode(',',$permittedTypeAry)."&excluded_type=4&DisplayGroupCategory=1&Disable_AddGroup_Button=1".$excludeUserIdParam;
#$chooseMemberBtn = $indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('".$PATH_WRT_ROOT."home/common_choose/index.php?$param', 9)");
$chooseMemberBtn = $indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "show_dyn_thickbox_ip('','".$PATH_WRT_ROOT."home/common_choose/index.php?$param&from_thickbox=1"."', '', 0, 565, 640, 1, 'fakeDiv', null)");
$removeMemberBtn = $indexVar['linterface']->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.form1.elements['SelectedUserIdAry[]'])");
$x = '';
$x .= '<div>'."\r\n";
	$x .= '<div id="divSelectUsers" style="float:left;">'.'</div>'."\r\n";
// 	$x .= '<div>'.$chooseMemberBtn.'<br>'.$removeMemberBtn.'</div>'."\r\n";
// 	$x .= '<br style="clear:both;" />'."\n";
// 	$x .= '<span class="tabletextremark">'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'</span>'."\n";
$x .= '</div>'."\r\n";
$htmlAry['addMemberSelection'] = $x;


### Submit Button
$htmlAry['submitBtn'] = $indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="goAddMember();", $id="Btn_Submit");
	

$x = '';
$x .= '<table width="100%" class="form_table_v30">'."\n";
	$x .= '<col class="field_title">';
	$x .= '<col class="field_c">';
	# Code
	if($sys_custom['eClassApp']['GroupMessage']['setCodeForGroup']){ 
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Code'].'</td>'."\n";
		$x .= '<td>'.$infoAry[0]['Code'].'</td>'."\n";
	$x .= '</tr>'."\n";
	}
	# Name
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Name'].'</td>'."\n";
		$x .= '<td>'.Get_Lang_Selection($infoAry[0]['NameCh'], $infoAry[0]['NameEn']).'</td>'."\n";
	$x .= '</tr>'."\n";
	# Member quota
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['eClassApp']['MemberQuota'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $numOfMember.' / '.$maxNumOfMember;
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	# Add member
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['Name'].'</td>'."\n";
		$x .= '<td>'.$htmlAry['addMemberSelection'].'</td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;
?>
<?= $indexVar['linterface']->Include_JS_CSS() ?>
<?php include_once ($PATH_WRT_ROOT . "home/common_choose/user_selection_plugin_js.php");?>
<script type="text/javascript">
var jNumOfQuotaLeft = <?=$maxNumOfMember?> - <?=$numOfMember?>; 
$(document).ready( function() {
	var divId ='divSelectUsers'
	var targetUserTypeAry = [1, 3]; 
	var userSelectedFieldName = 'SelectedUserIdAry[]';
	var userSelectedFieldId = 'SelectedUserIDSel';
	var excludeUserAry = <?php echo $jsonObj->encode($groupMemberUserIdAry)?>;
	Init_User_Selection_Menu(divId,targetUserTypeAry,userSelectedFieldName, userSelectedFieldId,'', excludeUserAry);
});

function goGroupList() {
	window.location = "?appType=<?=$eclassAppConfig['appType']['Common'] ?>&task=commonFunction/group_message/group_list";
}

function goAddMember() {
	var canSubmit = true;
	var objForm = document.getElementById('form1');
	
	checkOptionAll(objForm.elements["SelectedUserIdAry[]"]);
	if ($('select#SelectedUserIDSel > option').length > parseInt(jNumOfQuotaLeft)) {
		alert('<?=$Lang['eClassApp']['jsWarning']['selectedMemberExceedMemberLimit']?>');
		canSubmit = false;
	}

	if ($('select#SelectedUserIDSel > option').length == 0) {
		alert('<?=$Lang['General']['JS_warning']['SelectAtLeastOneRecord']?>');
		canSubmit = false;
	}
	
	if (canSubmit) {
		$('input#task').val('commonFunction/group_message/group_member_edit_update');
		$('form#form1').attr('action', 'index.php').submit();
	}
}

function goMemberList() {
	$('input#task').val('commonFunction/group_message/group_member');
	$('form#form1').submit();
}
</script>
<form id="form1" name="form1" method="post">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br style="clear:both;" />
	
	<?=$htmlAry['stepTable']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="GroupID" name="GroupID" value="<?=$GroupID?>" />
	<input type="hidden" id="FromNew" name="FromNew" value="<?=$FromNew?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>