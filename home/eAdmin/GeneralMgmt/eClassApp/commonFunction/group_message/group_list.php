<?php
// using : 
/*
 * 20200928 Ray:
 * - add batch create group message
 * 20170830 Roy:
 * - exclude $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom'] type in the group list
 * 
 */
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();

 
$returnMsgKey = $_GET['returnMsgKey'];

$CurrentPage = 'CommonFunction_GroupMessage';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['ModuleNameAry']['GroupMessage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$isAdminUser = false;
if ($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassApp"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]) {
	$isAdminUser = true;
}


### content tool
// $btnAry[] = array($btnClass, $onclickJs, $displayLang, $subBtnAry);
$btnAry = array();
if ($isAdminUser) {
	$btnAry[] = array('new', 'javascript: goNew();');
	$btnAry[] = array('', 'javascript: js_Go_Batch_Create_Group_Message();',$Lang['eClassApp']['GroupMessageBatchCreate']);
	$htmlAry['contentTool'] = $indexVar['linterface']->Get_Content_Tool_By_Array_v30($btnAry);
}


### group quota display
$canNewGroup = 0;
if ($isAdminUser) {
	$maxNumOfGroup = $leClassApp_groupMessage->getMaxNumOfGroup();
	$numOfCurGroup = count($leClassApp_groupMessage->getGroupData($groupIdAry='', $parCode='', $excludeGroupIdAry='', $excludeGroupTypeAry=array($eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom'])));
	$canNewGroup = ($numOfCurGroup >= $maxNumOfGroup)? 0 : 1;
	$x = '';
	$x .= '<table class="form_table_v30">'."\r\n";
		// Group Code
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['GroupQuota'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
				$x .= $numOfCurGroup.' / '.$maxNumOfGroup;
			$x .= '</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</table>'."\r\n";
	$htmlAry['infoTable'] = $x;
}


### DB table action buttons
// $btnAry[] = array($btnClass, $btnHref, $displayLang);
if ($isAdminUser) {
	$btnAry = array();
	$btnAry[] = array('edit', 'javascript: goEdit();');
	$btnAry[] = array('delete', 'javascript: goDelete();');
	$htmlAry['dbTableActionBtn'] = $indexVar['linterface']->Get_DBTable_Action_Button_IP25($btnAry);
}


### db table
$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
$field = ($field == '') ? 1 : $field;
$pageNo = ($pageNo == '') ? 1 : $pageNo;
$page_size = ($numPerPage == '') ? 50 : $numPerPage;

$li = new libdbtable2007($field, $order, $pageNo);
if($sys_custom['eClassApp']['GroupMessage']['setCodeForGroup']){
	$li->field_array = array("g.Code", "GroupName", "NumOfMember");	
}else{
	$li->field_array = array("GroupName", "NumOfMember");		
}
$li->sql = $leClassApp_groupMessage->getGroupListPageSql($isAdminUser, $sys_custom['eClassApp']['GroupMessage']['setCodeForGroup'], $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom']);
$li->no_col = count($li->field_array) + 1;	// "#" column 
if ($isAdminUser) {
	$li->no_col++;	// "checkbox" column
}
$li->IsColOff = "IP25_table";
$li->count_mode = 1;

$pos = 0;
$li->column_list .= "<th width='25' class='tabletoplink'>#</th>\n";
if($sys_custom['eClassApp']['GroupMessage']['setCodeForGroup']){
    $li->column_list .= "<th>".$li->column($pos++, $Lang['General']['Code'])."</th>\n";
}
$li->column_list .= "<th width='65%'>".$li->column($pos++, $Lang['General']['Name'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column($pos++, $Lang['Group']['NoOfMember'])."</th>\n";
if ($isAdminUser) {
	$li->column_list .= "<th width='1'>".$li->check("groupIdAry[]")."</th>\n";
}
$htmlAry['dataTable'] = $li->display();

$AcademicYearID = Get_Current_Academic_Year_ID();
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function js_Go_Batch_Create_Group_Message()
{
    $('a#BatchCreateGroupMessageLink').click();
}

function goNew() {
	if (<?=$canNewGroup?> == 1) {
		window.location = "?appType=<?=$eclassAppConfig['appType']['Common']?>&task=commonFunction/group_message/group_edit";
	}
	else {
		alert('<?=$Lang['eClassApp']['jsWarning']['reachedNumOfGroupLimit']?>');
	}
}

function goEdit() {
	if ($('input.groupIdChk:checked').length == 0) {
		alert(globalAlertMsg1);
	}
	else {
		$('input#task').val('commonFunction/group_message/group_edit');
		$('form#form1').submit();
	}
}

function goDelete() {
	if ($('input.groupIdChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		if (confirm(globalAlertMsg3)) {
			$('input#task').val('commonFunction/group_message/group_delete');
			$('form#form1').submit();
		}
	}
}

function goMemberList(parGroupId) {
	$('input#GroupID').val(parGroupId);
	$('input#task').val('commonFunction/group_message/group_member');
	$('form#form1').submit();
}

function Get_Batch_Create_Form() {
    BatchFormAjax = GetXmlHttpObject();

    if (BatchFormAjax == null)
    {
        alert (errAjax);
        return;
    }

    var url = 'index.php?appType=C&task=commonFunction/group_message/batch_create_group_message_form';

    var postContent = "AcademicYearID=<?=$AcademicYearID?>";

    BatchFormAjax.onreadystatechange = function() {
        if (BatchFormAjax.readyState == 4) {
            ResponseText = Trim(BatchFormAjax.responseText);
            document.getElementById('TB_ajaxContent').innerHTML = ResponseText;
        }
    };
    BatchFormAjax.open("POST", url, true);
    BatchFormAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    BatchFormAjax.send(postContent);
}

function js_Select_All_Class(jsYearID, jsChecked)
{
    $('input.ClassChk_' + jsYearID).each( function() {
        $(this).attr('checked', jsChecked);
    })
}

function js_UnCheck_SelectAll(jsYearID, jsChecked)
{
    if (jsChecked == false)
    {
        $('input#' + jsYearID + '-SelectAll').attr('checked', false);
    }
}

function js_Select_All_Class_Checkbox(jsChecked) {
    $('input.ClassChk').each( function() {
        $(this).attr('checked', jsChecked);
    })
}

function Check_Batch_Create_Group_Message() {
    document.getElementById("CreateGroupMessage").value = '0';
    var PostString = Get_Form_Values(document.getElementById("BatchCreateGroupMessageForm"));

    CheckBatchCreateGroupMessageAjax = GetXmlHttpObject();

    if (CheckBatchCreateGroupMessageAjax == null)
    {
        alert (errAjax);
        return;
    }

    var url = 'index.php?appType=C&task=commonFunction/group_message/check_batch_create_group_message';

    var PoseValue = PostString;
    CheckBatchCreateGroupMessageAjax.onreadystatechange = function() {
        if (CheckBatchCreateGroupMessageAjax.readyState == 4) {
            var ResponseText = Trim(CheckBatchCreateGroupMessageAjax.responseText);
            if (ResponseText != "") {
                var json_data = JSON.parse(ResponseText);
                if(json_data.error == 1) {
                    alert(json_data.msg);
                } else {
                    if(confirm(json_data.msg)) {
                        Batch_Create_Group_Message();
                    }
                }
            }
        }
    };
    CheckBatchCreateGroupMessageAjax.open("POST", url, true);
    CheckBatchCreateGroupMessageAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    CheckBatchCreateGroupMessageAjax.send(PoseValue);
}

function Batch_Create_Group_Message() {
    Block_Thickbox();
    document.getElementById("CreateGroupMessage").value = '1';
    var PostString = Get_Form_Values(document.getElementById("BatchCreateGroupMessageForm"));

    BatchCreateGroupMessageAjax = GetXmlHttpObject();

    if (BatchCreateGroupMessageAjax == null)
    {
        alert (errAjax);
        return;
    }

    var url = 'index.php?appType=C&task=commonFunction/group_message/check_batch_create_group_message';

    var PoseValue = PostString;
    BatchCreateGroupMessageAjax.onreadystatechange = function() {
        if (BatchCreateGroupMessageAjax.readyState == 4) {
            var ResponseText = Trim(BatchCreateGroupMessageAjax.responseText);
            window.top.tb_remove();
            UnBlock_Thickbox();
            if(ResponseText != "") {
                window.location = '?appType=<?=$appType?>&task=<?=$task?>&returnMsgKey='+ResponseText;
            }
        }
    };
    BatchCreateGroupMessageAjax.open("POST", url, true);
    BatchCreateGroupMessageAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    BatchCreateGroupMessageAjax.send(PoseValue);
}


</script>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.blockUI.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />

<form name="form1" id="form1" method="POST" action="index.php?task=commonFunction/group_message/group_list">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<br style="clear:both;">
		<?=$htmlAry['infoTable']?>
		<br style="clear:both;">

        <div style="float:left;display:none;">
        <a id="BatchCreateGroupMessageLink" href="#TB_inline?height=450&width=750&inlineId=FakeLayer" class="thickbox new" onclick="Get_Batch_Create_Form();" title="<?=$Lang['eClassApp']['GroupMessageBatchCreate']?>"><?=$Lang['eClassApp']['GroupMessageBatchCreate']?></a>
        </div>

	</div>
	<div class="table_board">
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$htmlAry['dataTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="GroupID" name="GroupID" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$li->pageNo?>" />
	<input type="hidden" id="order" name="order" value="<?=$li->order?>" />
	<input type="hidden" id="field" name="field" value="<?=$li->field?>" />
	<input type="hidden" id="page_size_change" name="page_size_change" value="" />
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>