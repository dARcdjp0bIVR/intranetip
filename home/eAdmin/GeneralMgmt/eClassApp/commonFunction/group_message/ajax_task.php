<?php
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();

$ajaxTask = $_POST['ajaxTask'];

if ($ajaxTask == 'isGroupCodeValid') {
	$groupId = IntegerSafe($_POST['GroupID']);
	$targetCode = standardizeFormPostValue($_POST['TargetCode']);
	
	$groupAry = $leClassApp_groupMessage->getGroupData($groupIdAry='', $targetCode, $groupId);
	$returnResult = (count($groupAry) == 0)? '1' : '0';
}

echo $returnResult;
?>