<?php
// using : 
include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();

$groupId = IntegerSafe($_POST['GroupID']);
$userIdAry = $_POST['userIdAry'];
$appType = $_POST['appType'];

$successAry = array();
$indexVar['libeClassApp']->Start_Trans();

$successAry['saveLocal'] = $leClassApp_groupMessage->deleteGroupMember($groupId, $userIdAry);
if ($successAry['saveLocal']) {
	$successAry['saveCloud'] = $leClassApp_groupMessage->saveGroupMemberInCloud($groupId);
}


if (in_array(false, $successAry)) {
	$returnMsgKey = 'UpdateUnsuccess';
	$indexVar['libeClassApp']->RollBack_Trans();
}
else {
	$returnMsgKey = 'UpdateSuccess';
	$indexVar['libeClassApp']->Commit_Trans();
}

$task = 'commonFunction/group_message/group_member';
header('location: ?appType='.$appType.'&task='.$task.'&GroupID='.$groupId.'&returnMsgKey='.$returnMsgKey);
?>