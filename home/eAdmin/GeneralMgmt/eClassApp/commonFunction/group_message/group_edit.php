<?php
// using : Roy

/*
 * 2017-06-26 (Roy):
 * bug fix for $sys_custom['DHL']
 * 
 * 2017-06-23 (Roy):
 * hide communication mode for DHL with flag $sys_custom['DHL']
 * 
 * 
 * 2017-05-11 (Roy): [ip.2.5.8.7.1]
 * add communication mode
 *
 */

include_once($PATH_WRT_ROOT.'includes/eClassApp/groupMessage/libeClassApp_groupMessage.php');
$leClassApp_groupMessage = new libeClassApp_groupMessage();

$groupId = ($_POST['groupIdAry'])? $_POST['groupIdAry'][0] : $_GET['GroupID'];
$groupId = IntegerSafe($groupId);
$returnMsgKey = $_GET['returnMsgKey'];
$isEdit = ($groupId == '')? false : true;

$CurrentPage = 'CommonFunction_GroupMessage';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['ModuleNameAry']['GroupMessage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


$PAGE_NAVIGATION[] = array($Lang['Header']['Menu']['Group'], "javascript:goGroupList()"); 
if ($isEdit) {
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
	$SubmitBtnText = $Lang['Btn']['Submit'];
	
	$infoAry = $leClassApp_groupMessage->getGroupData($groupId);
	$groupCode = $infoAry[0]['Code']; 
	$groupNameEn = $infoAry[0]['NameEn'];
	$groupNameCh = $infoAry[0]['NameCh'];
	$communicationMode = $infoAry[0]['CommunicationMode'];
}
else {
	$maxNumOfGroup = $leClassApp_groupMessage->getMaxNumOfGroup();
	$numOfCurGroup = count($leClassApp_groupMessage->getGroupData($groupIdAry='', $parCode='', $excludeGroupIdAry='', $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['GroupType']['UserChatroom']));
	
	if ($numOfCurGroup >= $maxNumOfGroup) {
		No_Access_Right_Pop_Up();
	}
	
	$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
	$SubmitBtnText = $Lang['Btn']['Continue'];
	
	### Step Table
	$stepAry = array();
	$stepAry[] = array($Lang['eClassApp']['Info'], true);
	$stepAry[] = array($Lang['eClassApp']['SelectUser'], false);
	$htmlAry['stepTable'] = $indexVar['linterface']->GET_STEPS_IP25($stepAry);
	
	
//	$defaultCommunicationMode = $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['Normal'];
	$communicationMode= $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['Normal'];
}

### communication mode selection
$communicationModes = array(
		array($eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['Normal'], $Lang['eClassApp']['GroupMessage']['CommunicationMode_Normal']),
		array($eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['DisableParentToParent'], $Lang['eClassApp']['GroupMessage']['CommunicationMode_DisableParentToParent'])
		
);

### Navigation
$htmlAry['navigation'] = $indexVar['linterface']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";

if($sys_custom['eClassApp']['GroupMessage']['setCodeForGroup']){
	// Group Code
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$indexVar['linterface']->RequiredSymbol().$Lang['General']['Code'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
				$x .= $indexVar['linterface']->GET_TEXTBOX_NAME('groupCodeTb', 'groupCode', $groupCode, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255));
				$x .= $indexVar['linterface']->Get_Form_Warning_Msg('groupCodeTbEmptyWarnDiv', $Lang['General']['JS_warning']['InputCode'], $Class='warnMsgDiv')."\n";
				$x .= $indexVar['linterface']->Get_Form_Warning_Msg('groupCodeTbInUseWarnDiv', $Lang['General']['JS_warning']['CodeIsInUse'], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
}
	// Group Name
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title" rowspan="2">'.$indexVar['linterface']->RequiredSymbol().$Lang['General']['Name'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30">(ENG)</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $indexVar['linterface']->GET_TEXTBOX_NAME('groupNameEnTb', 'groupNameEn', $groupNameEn, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255));
				$x .= $indexVar['linterface']->Get_Form_Warning_Msg('groupNameEnTbEmptyWarnDiv', $Lang['General']['JS_warning']['InputEnglishName'], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30">(CHI)</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $indexVar['linterface']->GET_TEXTBOX_NAME('groupNameChTb', 'groupNameCh', $groupNameCh, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255));
				$x .= $indexVar['linterface']->Get_Form_Warning_Msg('groupNameChTbEmptyWarnDiv',  $Lang['General']['JS_warning']['InputChineseName'], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	if ($isEdit) {
		$x .= "<input type='hidden' id='GroupID' name='GroupID' value='$groupId' />";
	}
//	} else {
		if (!$sys_custom['DHL']) {
			// Communication Mode
			$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$indexVar['linterface']->RequiredSymbol().$Lang['eClassApp']['GroupMessage']['CommunicationMode'].'</td>'."\r\n";
			$x .= '<td>'."\r\n";
			$x .= $indexVar['linterface']->GET_SELECTION_BOX($communicationModes, "id='communicationMode' name='communicationMode' onchange='changedCommunicationMode();' ", '', $communicationMode);
			$x .= $indexVar['linterface']->Get_Form_Warning_Msg('communicationModeWarn1Div', $Lang['eClassApp']['GroupMessage']['ChangeCommunicateModeWarning1'], $Class='')."\n";
			$x .= $indexVar['linterface']->Get_Form_Warning_Msg('communicationModeWarn2Div', $Lang['eClassApp']['GroupMessage']['ChangeCommunicateModeWarning2'], $Class='')."\n";
			$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		}
//	}

	
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($SubmitBtnText, "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goGroupList()", 'backBtn');
?>
<script type="text/javascript">
var originalCommunicationMode;
var isEdit = '<?php echo ($isEdit)? 1 : 0 ?>';
$(document).ready( function() {
	originalCommunicationMode = $('select#communicationMode').val();
	changedCommunicationMode();
});

function goGroupList() {
	window.location = "?appType=<?=$eclassAppConfig['appType']['Common'] ?>&task=commonFunction/group_message/group_list";
}

function changedCommunicationMode() {
	var curCommunicationMode = $('select#communicationMode').val();
//	if (isEdit == 1 && originalCommunicationMode == '<?php echo $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['Normal']?>' && curCommunicationMode == '<?php echo $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['DisableParentToParent']?>') {
//		$('div#communicationModeWarn1Div').show();
//		$('div#communicationModeWarn2Div').hide();
//	}
//	else if (isEdit == 1 && originalCommunicationMode == '<?php echo $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['DisableParentToParent']?>' && curCommunicationMode == '<?php echo $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['Normal']?>') {
//		$('div#communicationModeWarn1Div').hide();
//		$('div#communicationModeWarn2Div').show();
//	}
//	else {
//		$('div#communicationModeWarn1Div').hide();
//		$('div#communicationModeWarn2Div').hide();
//	}

	if (curCommunicationMode == '<?php echo $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['Normal']?>') {
		$('div#communicationModeWarn1Div').show();
		$('div#communicationModeWarn2Div').hide();
	}
	else if (curCommunicationMode == '<?php echo $eclassAppConfig['INTRANET_APP_MESSAGE_GROUP']['CommunicationMode']['DisableParentToParent']?>') {
		$('div#communicationModeWarn1Div').hide();
		$('div#communicationModeWarn2Div').show();
	}
}

function goSubmit() {
	var canSubmit = true;
	var isFocused = false;
	
	$('div.warnMsgDiv').hide();
	
	// disable action button to prevent double submission
	$('input.actionBtn').attr('disabled', 'disabled');
	
	// check required fields
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	
	// check code duplicated or not
	$.post(
		"index.php", 
		{ 
			appType: '<?=$appType?>',
			task: 'commonFunction/group_message/ajax_task',
			ajaxTask: 'isGroupCodeValid',
			GroupID: '<?=$groupId?>',
			TargetCode: $('input#groupCodeTb').val()
		},
		function(ReturnData) {
			var jsCanSubmit = false;
			if (ReturnData == '1') {
				canSubmit = true;
			}
			else {
				$('div#groupCodeTbInUseWarnDiv').show();
				$('input#groupCodeTb').focus();
				canSubmit = false;
			}
			
			if (canSubmit) {
				$('input#task').val('commonFunction/group_message/group_edit_update');
				$('form#form1').attr('action', 'index.php').submit();
			}
		}
	);
}
</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br style="clear:both;" />
	
	<?=$htmlAry['stepTable']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="GroupID" name="GroupID" value="<?=$groupId?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>