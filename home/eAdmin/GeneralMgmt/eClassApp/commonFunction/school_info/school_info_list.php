<?php
// using : Tiffany
/*
 * 
 */
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_schoolInfo.php");
$returnMsgKey = $_GET['returnMsgKey'];
$lib = new libdb();
$libeca_si = new libeClassApp_schoolInfo();

$CurrentPage = 'CommonFunction_SchoolInfo';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['ModuleNameAry']['SchoolInfo']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);
echo $indexVar['linterface']->Include_JS_CSS();
if(!isset($ParentMenuID) || $ParentMenuID=="") {
	$ParentMenuID = 0;
	$Level = 0;
}

$Level++;
if($goback==1){
	$Level--;
}
if($goback_list==1){
	$Level--;
	$Level--;
	
}
$status="";
### content tool
$btnAry = array();
$btnAry[] = array('New', 'javascript: goNew();');
$htmlAry['contentTool'] = $indexVar['linterface']->Get_Content_Tool_By_Array_v30($btnAry);

if($sys_custom['DHL']){
    $isAdmin = 0;
    $targetGroup = $_POST['targetGroup']==''?'0': $_POST['targetGroup'];
    $targetGroupArray = array($Lang['SchoolNews']['SpecificGroup']);
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]){
        $isAdmin = 1;
        $targetGroupArray= array($Lang['eSurvey']['public'],$Lang['SchoolNews']['SpecificGroup']);
    }
    $targetGroup = $isAdmin == '0'? '1': $targetGroup;
 
    $htmlAry['targetGroupSel'] = getSelectByAssoArray($targetGroupArray, ' id="targetGroup" name="targetGroup" onchange="changeTargetGroupType()"',$targetGroup,'',1);   
}
### DB table action buttons
$btnAry = array();
$btnAry[] = array('approve', 'javascript: change_status(1);',$Lang['eClassApp']['SchoolInfo']['Public']);
$btnAry[] = array('reject', 'javascript: change_status(2);',$Lang['eClassApp']['SchoolInfo']['Private']);
$btnAry[] = array('edit', 'javascript: goEdit();');
$btnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $indexVar['linterface']->Get_DBTable_Action_Button_IP25($btnAry);


// ### view tag
// // $viewTabAry[] = array($displayLang, $onclickJs, $iconPath, $isSelectedTab);
// $curViewTab = 'List View';
// $viewTabAry = array();
// $viewTabAry[] = array('Tree View', 'javascript:goTreeView();', '', $curViewTab=='Tree View');
// $viewTabAry[] = array('List View', 'javascript:void(0);', $PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN.'/eEnrollment/icon_member.gif', $curViewTab=='List View');
// $htmlAry['viewTag'] = $indexVar['linterface']->GET_CONTENT_TOP_BTN($viewTabAry);

$MenuNameField = Get_Lang_Selection('TitleChi', 'TitleEng');

if($ParentMenuID!=0){
	
    $result2 = $libeca_si->getParentTitle($MenuNameField,$ParentMenuID);
	$x = '';
	$x .= '<table class="form_table_v30">'."\r\n";
	  $x .= '<tbody>'."\r\n";
	    $x .= '<tr>'."\r\n";
       	  $x .= '<td class="field_title">'.$Lang['eClassApp']['SchoolInfo']['SelectedMenu'].'</td>'."\r\n";
	      $x .= '<td>'."\r\n";
	      $x .=$result2[0][Title];
	      $x .= '</td>'."\r\n";
	    $x .= '</tr>'."\r\n";
	  $x .= '</tbody>'."\r\n";
	$x .= '</table>'."\r\n";
	$htmlAry['formTable'] = $x;
	$back_parentMenuID = $result2[0][ParentMenuID];
}

$result = $libeca_si->getTitles($MenuNameField,$ParentMenuID);

$show_list .= "<table id='ContentTable' class='common_table_list_v30'>
	        	<thead>
		          <tr>
		           <th style='width:1'>#</th>
		           <th style='width:35%'>".$Lang['eClassApp']['SchoolInfo']['ItemTitle']."</th>
		           <th style='width:35%'>".$Lang['eClassApp']['SchoolInfo']['Sub-menusOrItems']."</th>
		           <th style='width:20%'>".$Lang['eClassApp']['SchoolInfo']['Status']."</th>
		           <th style='width:10%'></th>		           		
		           <th style='width:1'><input type='checkbox' id='All_Delete' onclick='$(\".selectItem\").attr(\"checked\",this.checked);'></th></tr></thead>";
$count_checkbox = count($result);


if(count($result)!=0){
	$show_list.= "<tbody>";
	if($sys_custom['DHL']){
	    include_once($intranet_root."/includes/DHL/libdhl.php");
	    $libdhl = new libdhl();
	    
	    $UserPICInfoAry = $libdhl->getPICResponsInfo();
	    $PICCompanyIDAry = $UserPICInfoAry['Department'];
	    
	    $filterResult = array();
	    for($i=0;$i<count($result);$i++){
	       
	        $targetGroupType = $libdhl->getSchoolNewsTargetGroup($result[$i][MenuID]);

	        if($targetGroup == '0' && ($targetGroupType == '1' || $targetGroupType == '')){ // ALL USER 
	            $filterResult[] = $result[$i];
	        }
	        
	        
	        if($targetGroup == '1' && $targetGroupType == '0'){ // SPECIFIC GROUP
	            $filterResult[] = $result[$i];
	        }
	    }
	   
	    
	    $resultAry = array();
	    for($i=0;$i<count($filterResult);$i++){
	        $targetGroupType = $libdhl->getSchoolNewsTargetGroup($filterResult[$i][MenuID]);
	      
	        if($targetGroupType == '1'  || $targetGroupType == ''){
    	        $resultAry[] = $filterResult[$i];
    	    }else if($targetGroupType == '0'){
  	      
    	        $DepartmentIDAry = $libdhl->getSchoolNewsDepartmentList($filterResult[$i][MenuID]);
    	        if(!empty($DepartmentIDAry)){
    	            for($j=0;$j<count($DepartmentIDAry);$j++){
    	                $DepartmentStaff = $libdhl->getDepartmentUserRecords(array('DepartmentID'=>$DepartmentIDAry[$j]));  	                
    	                $inDartmentStaffIDAry = Get_Array_By_Key($DepartmentStaff,'UserID');
    	                if(in_array($DepartmentIDAry[$j],$PICCompanyIDAry) || in_array($_SESSION['UserID'],$inDartmentStaffIDAry)){
    	                    $resultAry[] = $filterResult[$i];
    	                    break;
    	                }
    	            }
    	        }
    	    }
	    }
	  
	    if($isAdmin){
	        $result = $filterResult;	     
	    }else{
	        $result = $resultAry;
	    }	
	    $count_checkbox = count($result);
	}
	
	for($i=0;$i<count($result);$i++){
 
		  $show_list.="<tr id='tr_".$result[$i][MenuID]."'><td><span class='rowNumSpan'>".($i+1)."</span></td><td>".$result[$i][Title]."</td>";
		  if($result[$i][IsItem]==0){
		  	  $result1 = $libeca_si->getSubTitlesNum($result[$i][MenuID]);
		      $show_list .= "<td><a href='javascript:void(0);' onclick='goSubList(".$result[$i][MenuID].");'>".count($result1)."</a></td>";		  	 
		  }else{	  	
		  	$show_list .= "<td>---</td>";		  	 
		  }
		  if($result[$i][RecordStatus]==1){
		  	$status = $Lang['eClassApp']['SchoolInfo']['Public'];
		  }else if($result[$i][RecordStatus]==2){
		  	$status = $Lang['eClassApp']['SchoolInfo']['Private'];
		  }
		  $show_list.="<td>".$status."</td><td class='Dragable'>".$indexVar['linterface']->GET_LNK_MOVE("#", $Lang['Btn']['Move'])."</td><td>".$indexVar['linterface']->Get_Checkbox($i, 'selectItem', $result[$i][MenuID], $isChecked=0, $Class='selectItem', $Display='', $Onclick='', $Disabled='')."</td></tr>";
	}
	$show_list.= "</tbody>";
}
else {
	$show_list.="<tr><td colspan=\"6\" style=\"text-align:center;\">".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
}
$show_list .= "</table>";
### buttons
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack(".$back_parentMenuID.")", 'backBtn');
?>
<script type="text/javascript">
$(document).ready( function() {
	js_Init_DND_Table();
});

function changeTargetGroupType() {
	reloadPage();
}
function reloadPage() {
	$('input#task').val('commonFunction/school_info/index');
	$('form#form1').attr('target', '_self').submit();
}

function goTreeView() {
	$('input#task').val('commonFunction/school_info/school_info_tree');
	$('form#form1').submit();
}

function goBack(back_parentMenuID) {
	
	$('input#ParentMenuID').val(back_parentMenuID);
	$('input#goback_list').val('1');
	$('input#task').val('commonFunction/school_info/school_info_list');
	$('form#form1').submit();

}

function goNew() {
	$('input#task').val('commonFunction/school_info/school_info_new');
	$('input#MenuID').val('');
	$('form#form1').submit();
}

function goEdit() {
	if ($('input.selectItem:checked').length != 1) {
		alert(globalAlertMsg1);
	}
	else {
		for($i=0;$i<<?=$count_checkbox?>;$i++){
			 if($("#"+$i)[0].checked){
                 $('input#MenuID').val($("#"+$i).val());
			 }
		}
		$('input#task').val('commonFunction/school_info/school_info_new');
		$('form#form1').submit();
	}
} 

function goDelete() {
	if ($('input.selectItem:checked').length == 0) {
		alert('<?=$Lang['eClassApp']['SchoolInfo']['AlertDelete1']?>');
	}
	else if(confirm('<?=$Lang['eClassApp']['SchoolInfo']['AlertDelete2']?>')) {
		var menuID = '';
		for($i=0;$i<<?=$count_checkbox?>;$i++){
			 if($("#"+$i)[0].checked){
				 if(menuID=='') menuID = $("#"+$i).val();
				 else  menuID = menuID+','+$("#"+$i).val();                
			 }
		}
		$('input#MenuID').val(menuID);
		$('input#task').val('commonFunction/school_info/school_info_delete');
		$('form#form1').submit();
	}
}

function goSubList(parentMenuID) {
	$('input#ParentMenuID').val(parentMenuID);
	$('input#task').val('commonFunction/school_info/school_info_list');
	$('form#form1').submit();
}

function js_Init_DND_Table() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "ContentTable") {
				Block_Element(table.id);
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					{
						var thisID = rows[i].id;
						var thisIDArr = thisID.split('_');
						var thisObjectID = thisIDArr[1];
						RecordOrder += thisObjectID + ",";
					}
				}                
				// Update DB
				$.post(
					"commonFunction/school_info/ajax_update.php", 
					{ 
						Action: "Reorder",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						js_Reset_Display_Range();
						
						// Get system message
						if (ReturnData=='1') {
							jsReturnMsg = '<?=$Lang['eClassApp']['SchoolInfo']['ReturnMsgArr']['ReorderSuccess']?>';			
						}
						else {
							jsReturnMsg = '<?=$Lang['eClassApp']['SchoolInfo']['ReturnMsgArr']['ReorderFailed']?>';
						}

						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

//Reset the ranking display
function js_Reset_Display_Range(){
	var jsRowCounter = 0;
	$('span.rowNumSpan').each( function () {
		$(this).html(++jsRowCounter);
	});
}

function change_status(status){
	if ($('input.selectItem:checked').length == 0) {
		alert('<?=$Lang['eClassApp']['SchoolInfo']['AlertChangeStatus']?>');
	}
	else{
		var menuID = '';
		for($i=0;$i<<?=$count_checkbox?>;$i++){
			 if($("#"+$i)[0].checked){
				 if(menuID=='') menuID = $("#"+$i).val();
				 else  menuID = menuID+','+$("#"+$i).val();                
			 }
		}
		// Update DB
		$.post(
			"commonFunction/school_info/ajax_update_status.php", 
			{ 
				Action: status,
				MenuID: menuID
			},
			function(ReturnData)
			{		
				// Get system message
				if (ReturnData=='1') {
					jsReturnMsg = 'UpdateSuccess';			
				}
				else {
					jsReturnMsg = 'UpdateUnsuccess';
				}
				 window.location.href="?appType=<?=$appType?>&task=commonFunction/school_info/school_info_list&returnMsgKey="+jsReturnMsg+"&ParentMenuID=<?=$ParentMenuID?>&Level=<?=$Level?>";			 			 
			}
		);
	}

	
}
</script>
<form name="form1" id="form1" method="POST" action="index.php">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<br style="clear:both;">
	</div>
	
	<div class="content_top_tool">
		<?=$htmlAry['viewTag']?>
	</div>
	<div class="table_board">
	    <?=$htmlAry['formTable']?>
	    <br/>
	    <div class="table_filter">
			<?=$htmlAry['targetGroupSel']?>
		</div>
		
		<?=$htmlAry['dbTableActionBtn']?>
		<?=$show_list?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	<?if($ParentMenuID!=0){?>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['backBtn']?>
			<p class="spacer"></p>
		</div>
	<?}?>
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="MenuID" name="MenuID" value="" />
	<input type="hidden" id="ParentMenuID" name="ParentMenuID" value="<?=$ParentMenuID?>" />
	<input type="hidden" id="Level" name="Level" value="<?=$Level?>" />
	<input type="hidden" id="goback_list" name="goback_list" value="0" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>