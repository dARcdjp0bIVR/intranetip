<?php
// Using:

###################################################### Change Log ######################################################
# Date      : 2019-03-22 (Anna)
# Detail    : added Target_allUser for dhl admin 
#
# Date      : 2019-01-11 (Tiffany)
# Detail    : Support Description for Eng by $sys_custom['eClassApp']['SchoolInfo']['SupportEnglishContent'] = true
# Deploy    :
#
# Date      : 2018-12-31 (Anna)
# Detail    : Added DHL cust
#
#
# Date      : 2018-04-24 (Anna)
# Detail    : Added sfoc url field# 
# Deploy    :
#
# Date      : 2017-07-19 (Tiffany)
# Detail    : Support PDF modified, delete.
# Deploy    :
#
# Date      : 2017-07-19 (Roy)
# Detail    : Add PDF type
# Deploy    :
#
########################################################################################################################

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_schoolInfo.php");
$libeca_si = new libeClassApp_schoolInfo();
$li = new libfilesystem();
$objHtmlEditor = new FCKeditor ( 'Description' , "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($Description));
$objHtmlEditor->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['SchoolInfo'], $id);
$objHtmlEditorEng = new FCKeditor ( 'DescriptionEng' , "100%", "320", "", "Basic2_withInsertImageFlash", htmlspecialchars_decode($DescriptionEng));
$objHtmlEditorEng->Config['FlashImageInsertPath'] = $li->returnFlashImageInsertPath($cfg['fck_image']['SchoolInfo'], $id."_Eng");
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'CommonFunction_SchoolInfo';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['ModuleNameAry']['SchoolInfo']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);
$isEdit = ($groupId == '')? false : true;

$PAGE_NAVIGATION[] = array($Lang['eClassApp']['ModuleNameAry']['SchoolInfo'], "javascript:goList()"); 
if ($MenuID!="") {
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
}
else {
	$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
}

if(!isset($status) || $status=="")
{
	$status = "1";
}

if(!isset($IconType) || $IconType=="")
{
	$IconType = "0";
}

if(!isset($icon) || $icon=="")
{
	$icon = "0";
}

if(!isset($custom_icon) || $custom_icon=="")
{
	$custom_icon = "";
}

if(!isset($isItem) || $isItem=="")
{
	$isItem = "0";
}
$TypeDisabled = 0;

if($sys_custom['DHL']){
    $isAdmin = 0; 
    if($_SESSION["SSV_USER_ACCESS"]["eAdmin-eClassTeacherApp"]){
        $isAdmin = 1;
    }
}


if($MenuID!=""){
	//$TypeDisabled = 1;
    $result = $libeca_si->getEditDetail($MenuID);
	$ParentMenuID = $result[0][ParentMenuID];
	$Level = $result[0][Level];
	$groupTitleEn = $result[0][TitleEng];
	$groupTitleCh = $result[0][TitleChi];
	$IconType = $result[0][IconType];
	if($IconType==1){
		$icon = $result[0][Icon];
	}else if($IconType==2){
		$custom_icon = $result[0][Icon];
	}
	$isItem = $result[0][IsItem];
	$status = $result[0][RecordStatus];
	$description = $result[0][Description];
    $descriptionEng = $result[0][DescriptionEng];
    $objHtmlEditor->Value = htmlspecialchars_decode($description);
    $objHtmlEditorEng->Value = htmlspecialchars_decode($descriptionEng);
    $url = $result[0][Url];
	$pdfPath = $result[0][PdfPath];
	$pdfName = $result[0][PdfName];

    if($isItem==0){
        $subTitles = $libeca_si->getSubTitlesNum($MenuID);    	
    	if(count($subTitles)!=0){
    		$TypeDisabled = 1;    
    		$addHidden = "<input  type='hidden' id='type' name='type' value='".$isItem."'>";		
    	}
    }
    
    if($sys_custom['DHL']){
        include_once($intranet_root."/includes/DHL/libdhl.php");
        $libdhl = new libdhl();
        
        $targetGroupIDList = $libdhl->getSchoolNewsGroupList($MenuID);
        $targetDepartmentList = $libdhl->getMappingDepartmentGroupID($targetGroupIDList);
//         $target_group_id_ary = explode(',',$targetGroupIDList);
        $target_group_id_ary = $targetDepartmentList;
        $targetGroupType = $libdhl->getSchoolNewsTargetGroup($MenuID);
        if($isAdmin){
            $Target_allUser_check = $targetGroupType=='1'?'checked':'';
        }     
    }
    
    
}

### Navigation
$htmlAry['navigation'] = $indexVar['linterface']->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";

	//Title
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title" rowspan="2">'.$indexVar['linterface']->RequiredSymbol().$Lang['eClassApp']['Title'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30">('.$Lang['eClassApp']['SchoolInfo']['Eng'].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $indexVar['linterface']->GET_TEXTBOX_NAME('groupTitleEnTb', 'groupTitleEn', $groupTitleEn, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255));
				$x .= $indexVar['linterface']->Get_Form_Warning_Msg('groupTitleEnTbEmptyWarnDiv', $Lang['eClassApp']['SchoolInfo']['AlertEnglishTitle'], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= '<span class="sub_row_content_v30">('.$Lang['eClassApp']['SchoolInfo']['Chi'].')</span>'."\r\n";
			$x .= '<span class="row_content_v30">'."\r\n";
				$x .= $indexVar['linterface']->GET_TEXTBOX_NAME('groupTitleChTb', 'groupTitleCh', $groupTitleCh, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255));
				$x .= $indexVar['linterface']->Get_Form_Warning_Msg('groupTitleChTbEmptyWarnDiv',  $Lang['eClassApp']['SchoolInfo']['AlertChineseTitle'], $Class='warnMsgDiv')."\n";
			$x .= '</span>'."\r\n";
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
   //Status
	$x .= '<tr>'."\r\n";
	   $x .= '<td class="field_title" rowspan="2">'.$Lang['eClassApp']['SchoolInfo']['Status'].'</td>'."\r\n";
       $x .= '<td>'."\r\n";
	      $x .= $indexVar['linterface']->Get_Radio_Button('status1', 'status', '1', "", $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['Public'], $Onclick="",$isDisabled=0);
	   $x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
    $x .= '<tr>'."\r\n";
       $x .= '<td>'."\r\n";
	      $x .= $indexVar['linterface']->Get_Radio_Button('status2', 'status', '2', "", $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['Private'], $Onclick="",$isDisabled=0);
	   $x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	
	if($sys_custom['DHL']){
	$x .='<tr>'."\r\n";
	$x .='<td class="field_title" >'.$Lang['SchoolNews']['target'].'</td>';
    	$x .='<td>';
        	$x .='<table class="form_table_v30">';
            	$x .='<tr>';
                	$x .='<td colspan="3">';
                	if($isAdmin){
                	    $x .='<input type="radio" name="Target_user" id="Target_allUser" value="1" '.$Target_allUser_check.' onClick="javascript:checkingStatus(this);">
                            <label for="Target_allUser">'.$Lang['eSurvey']['public'].'</label>';
                	}
                	$x .='<input type="radio" name="Target_user" id="Target_specificUser" value="0" '.$Target_specificUser_check.' onClick="javascript:checkingStatus($(\'#Target_allUser\'));">
                            <label for="Target_specificUser">'.$Lang['SchoolNews']['SpecificGroup'].'</label>';
                	$x .='</td>';
            	$x .='</tr>';
            	
            	$x .='<tr id="groupSelectionBox">';
            	   $x .='<td class="field_title" valign="top">'.$i_admintitle_group.'</td>';
            	   $x .='<td>';            
            		    include_once($intranet_root."/includes/DHL/libdhl.php");
            		    $libdhl = new libdhl();

            		    $x .= "<table width=100% border=0 cellpadding=5 cellspacing=0 class=\"inside_form_table\">\n";
                		    $x .= "<tr>\n";
                    		    $x .= "<td class=tableContent width=50%>\n";
                    		    $x .= $libdhl->getIntranetGroupSelection("GroupID", "GroupID[]", count($target_group_id_ary)>0?$target_group_id_ary: array(0), $i___sMultiple=true, $____hasFirst=false, $____firstText='', $____tags=' size="10" style="width:100%;" ', $____valuePrefix='',$____display_prefix_code_in_group_name=false,$fromSchoolInfo=true);
                        		    $x .= "<div>\n";
                            		    $x .= $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=GroupID[]&SelectGroupOnly=1&page_title=SelectAudience&DisplayGroupCategory=1&isAdmin=$isAdmin',16);");
                            		    $x .= "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "remove_selection_option('GroupID',0);");
                        		    $x .= "</div>\n";
                    		    $x .= "</td>\n";
                		    $x .= "</tr>\n";
            		    $x .= "</table>\n";           
            	$x .= '</td>';           	
            	$x .= '</tr>';
            	
        	$x .= '</table>';
    	$x .='</td>';
	$x .='</tr>';
	}
	
	
	
   //Icon
	$x .= '<tr>'."\r\n";
	    $x .= '<td class="field_title" rowspan="3">'.$Lang['eClassApp']['SchoolInfo']['Icon'].'</td>'."\r\n";
	    $x .= '<td>'."\r\n";
            $x .= $indexVar['linterface']->Get_Radio_Button('icon0', 'icon', '0', "", $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['NoIcon'], $Onclick="select_icon0()",$isDisabled=0);
        $x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	$x .= '<tr>'."\r\n";
	    $x .= '<td>'."\r\n";
	        $x .= $indexVar['linterface']->Get_Radio_Button('icon1', 'icon', '1', "", $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['DefaultIcon'], $Onclick="select_icon1()",$isDisabled=0);
	        $x .= '<br><div id="default_icon_div" style="display:none"><table><tr>
	        		       <td id="icon_default1" style="border:2px solid white"><a href="javascript:selectDefaultIcon(1);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/school_info/icon_default1.png" /></a></td>
	        		       <td id="icon_default2" style="border:2px solid white"><a href="javascript:selectDefaultIcon(2);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/school_info/icon_default2.png" /></a></td>
	        		       <td id="icon_default3" style="border:2px solid white"><a href="javascript:selectDefaultIcon(3);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/school_info/icon_default3.png" /></a></td>
                           <td id="icon_default4" style="border:2px solid white"><a href="javascript:selectDefaultIcon(4);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/school_info/icon_default4.png" /></a></td>
                           <td id="icon_default5" style="border:2px solid white"><a href="javascript:selectDefaultIcon(5);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/school_info/icon_default5.png" /></a></td>
                           <td id="icon_default6" style="border:2px solid white"><a href="javascript:selectDefaultIcon(6);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/school_info/icon_default6.png" /></a></td>
                           <td id="icon_default7" style="border:2px solid white"><a href="javascript:selectDefaultIcon(7);"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/eClassApp/school_info/icon_default7.png" /></a></td>
	        	   </tr></table></div>';
	    $x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";	 
	$x .= '<tr>'."\r\n";
	    $x .= '<td>'."\r\n";
	        $x .= $indexVar['linterface']->Get_Radio_Button('icon2', 'icon', '2', "", $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['CustomIcon'], $Onclick="select_icon2();",$isDisabled=0);
	        $x .= '<br><div id="custom_icon_div" style="display:none"><input type="file" id="custom_icon" name="custom_icon" />';
	        if ($custom_icon) {
	        	$x .= '<br />'."\r\n";
	        	$x .= '<br />'."\r\n";
	        	$x .= $indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $Lang['Btn']['Delete'], "goDelete();", 'delete');
	        	$x .= '<br style="clear:both;" />'."\r\n";
	        	$x .= '<img src="'.$PATH_WRT_ROOT.$eclassAppConfig['urlBrowsePath'].$imagePath.$custom_icon.'" />'."\r\n";
	        }
	        $x .= '</div></td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	//Type
	$x .= '<tr>'."\r\n";
	    $x .= '<td class="field_title" rowspan="4">'.$Lang['eClassApp']['SchoolInfo']['Type'].'</td>'."\r\n";
	    $x .= '<td>'."\r\n";
	        $x .= $indexVar['linterface']->Get_Radio_Button('type0', 'type', 0, $isChecked=1, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['Menu'], $Onclick="select_type0()",$TypeDisabled);
	    $x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	
	$x .= '<tr>'."\r\n";
	    $x .= '<td>'."\r\n";
	        $x .= $indexVar['linterface']->Get_Radio_Button('type1', 'type', 1, $isChecked=0, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['Content'], $Onclick="select_type1()",$TypeDisabled);
	        $x .= '<br><div id="decription_div" style="display:none">';

	        if($sys_custom['eClassApp']['SchoolInfo']['SupportEnglishContent']){
                $x .= $Lang['itextbook']['Lang']['ch']." : \r\n";
            }
	        $x .= $objHtmlEditor->Create2();

            if($sys_custom['eClassApp']['SchoolInfo']['SupportEnglishContent']){
                $x .= $Lang['itextbook']['Lang']['en']." : \r\n";
                $x .= $objHtmlEditorEng->Create2();
            }

	        if($sys_custom['eClassApp']['SFOC']){
	            $x .= $Lang['eClassApp']['SchoolInfo']['Hyperlink']." : \r\n";
	            
	            $x .= $indexVar['linterface']->GET_TEXTBOX_NAME('SFOCurl', 'SFOCurl', $url, $OtherClass='', $OtherPar=array('maxlength'=>255));
	            $x .= $indexVar['linterface']->Get_Form_Warning_Msg('InputUrlEmptyWarnDiv',  $Lang['eClassApp']['SchoolInfo']['AlertInputUrl'], $Class='warnMsgDiv')."\n";
	        }
	        $x .= '</div>';
	    $x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";

	
    $x .= '<tr>'."\r\n";
	    $x .= '<td>'."\r\n";
	        $x .= $indexVar['linterface']->Get_Radio_Button('type2', 'type', 2, $isChecked=0, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['Hyperlink'], $Onclick="select_type2()",$TypeDisabled);
	        $x .= '<br><div id="hyperlink_div" style="display:none">';
	        $x .=  $indexVar['linterface']->GET_TEXTBOX_NAME('url', 'url', $url, $OtherClass='', $OtherPar=array('maxlength'=>255));
	     	$x .= $indexVar['linterface']->Get_Form_Warning_Msg('InputUrlEmptyWarnDiv',  $Lang['eClassApp']['SchoolInfo']['AlertInputUrl'], $Class='warnMsgDiv')."\n";
	        $x .= '</div>';
	    $x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
	// 20170719 Roy: add PDF type
	$x .= '<tr>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $indexVar['linterface']->Get_Radio_Button('type3', 'type', 3, $isChecked=0, $Class="", $Display=$Lang['eClassApp']['SchoolInfo']['PDF'], $Onclick="select_type3()",$TypeDisabled);
			$x .= '<br><div id="pdf_div" style="display:none">';
			if ($pdfPath) {       	
	        	$x .= $indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $Lang['Btn']['Delete'], "goDeletePdf();", 'delete');
	        	$path = $eclassAppConfig['urlFilePath'].$pdfPath;
	        	$x .= "<a class='tablelink' target=_blank href=\"/home/download_attachment.php?target_e=".getEncryptedText($path)."\" >".$pdfName."</a>";	
	        	$x .= '<br />'."\r\n";
	        }
			$x .= '<input type="file" id="upload_pdf" name="upload_pdf" accept="application/pdf"/>';
		    $x .= $indexVar['linterface']->Get_Form_Warning_Msg('UploadPDFEmptyWarnDiv',  $Lang['eClassApp']['SchoolInfo']['AlertFileMissing'], $Class='warnMsgDiv')."\n";
			$x .= $indexVar['linterface']->Get_Form_Warning_Msg('UploadPDFWrongTypeWarnDiv',  $Lang['eClassApp']['SchoolInfo']['AlertWrongFileType'], $Class='warnMsgDiv')."\n";
			$x .= '</div>';
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";

$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn( $Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goList()", 'backBtn');
?>
<script type="text/javascript">
$(document).ready( function() {

	 //status
	 $('#status'+<?=$status?>).attr( "checked", true );

     //icon
	 $('#icon'+<?=$IconType?>).attr( "checked", true );
	 select_icon<?=$IconType?>();
	
	 if(<?=$IconType?>==1){
		 $('#icon_default'+<?=$icon?>).css("border-color","grey");
		 $('#defaultIcon').val(<?=$icon?>);
     }
     
     //type
	 $('#type'+<?=$isItem?>).attr( "checked", true );
	 select_type<?=$isItem?>();

	 <?php if($sys_custom['DHL']){ ?>

	   if('<?=$Target_allUser_check?>'=='checked'){
	 		$('#Target_allUser').click();
	 	}
	 	else{
	 		$('#Target_specificUser').click();
	 	}

		checkOptionAll(document.getElementById('form1').elements["GroupID[]"]);	     
	 <? }?>
     
});

function goList() {
	$('input#task').val('commonFunction/school_info/school_info_list');
	$('input#goback').val('1');
	$('form#form1').attr('action', 'index.php').submit();
}

function goSubmit() {
	var canSubmit = true;
	var isFocused = false;
	
	$('div.warnMsgDiv').hide();
	
	// disable action button to prevent double submission
	$('input.actionBtn').attr('disabled', 'disabled');
	
	// check required fields
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});

	var icon = $("input#defaultIcon").val();
	var custom_icon = "<?=$custom_icon?>";
	var custom_icon_input = $("input#custom_icon").val();

    
	if($("input[name='icon']:checked").val()==1&&icon==''){
		$("input[name='icon']").get(0).checked = true;
    }
    
	if($("input[name='icon']:checked").val()==2&&custom_icon==''&&custom_icon_input==''){
		$("input[name='icon']").get(0).checked = true;
    }

    var type = $('input[name="type"]:checked').val();

    
    if(type==2){    	
    	if (isObjectValueEmpty('url')) {
			canSubmit = false;			
			$('div#InputUrlEmptyWarnDiv').show();			
			if (!isFocused) {
				// focus the first element
				$('#url').focus();
				isFocused = true;
			}
		}
    }

    if (type == 3) {
    	var pdfpath = "<?=$pdfPath?>";
        var pdfFilePath = $('#upload_pdf').val();
        if(pdfpath==""){
	        if (pdfFilePath == '') {
				canSubmit = false;
				$('div#UploadPDFEmptyWarnDiv').show();
	        } else {
	            var extension = pdfFilePath.split('.').pop().toLowerCase();
	            if (extension != 'pdf') {
	        		canSubmit = false;
	    			$('div#UploadPDFWrongTypeWarnDiv').show();
	            }
	        }
        }else{
        	if (pdfFilePath != '') {
	        	if (confirm('<?php echo $Lang['eClassApp']['SchoolInfo']['AlertFileOvercast']?>')) {
				   canSubmit = true;
				} else {
				   canSubmit = false;
				}
	        }       	
        }
    }

    <?php if($sys_custom['DHL']){?>
 	// dhl group checking 
	var Group = document.getElementById("GroupID");
	var PublicDisplay = document.getElementById("Target_specificUser");
	if(PublicDisplay.checked == true){
		if(Group.length==0){
			alert ("<?=$Lang['SysMgr']['SchoolNews']['WarnSelectGroup']?>");
			return false;
		}
	}
	checkOptionAll(document.getElementById('form1').elements["GroupID[]"]);
	<?php }?>
            
    
	if (canSubmit) {
		$('input#task').val('commonFunction/school_info/school_info_new_update');
		$('form#form1').attr('action', 'index.php').submit();
	}
}

function goDelete() {	
	$('input#task').val('commonFunction/school_info/custom_icon_delete');
	$('form#form1').submit();
}

function goDeletePdf() {	
    $('input#task').val('commonFunction/school_info/pdf_delete');
	$('form#form1').submit();
}

function selectDefaultIcon(id){

    if($('#defaultIcon').val()!=""){

    	$('#icon_default'+$('#defaultIcon').val()).css("border-color","white");
    }

	$('#icon_default'+id).css("border-color","grey");
    $('#defaultIcon').val(id);
}

function select_icon0(){
	$('#default_icon_div').hide();
	$('#custom_icon_div').hide();
}

function select_icon1(){
	$('#default_icon_div').show();
	$('#custom_icon_div').hide();
}

function select_icon2(){
	$('#default_icon_div').hide();
	$('#custom_icon_div').show();
}

function select_type0(){
	$('#decription_div').hide();
	$('#hyperlink_div').hide();
	$('#pdf_div').hide();
}

function select_type1(){
	$('#decription_div').show();
	$('#hyperlink_div').hide();
	$('#pdf_div').hide();
}

function select_type2(){
	$('#decription_div').hide();
	$('#hyperlink_div').show();
	$('#pdf_div').hide();
}

function select_type3(){
	$('#decription_div').hide();
	$('#hyperlink_div').hide();
	$('#pdf_div').show();
}


function checkingStatus(radioBtn){

	if(radioBtn.checked==true){
		$('#groupSelectionBox').hide();
	}
	else{

		$('#groupSelectionBox').show();
		checkOption1(document.getElementById("GroupID"));
	}
}
function checkOption1(obj){
	for(i=0; i<obj.length; i++){
		if(!parseInt(obj.options[i].value)){
			obj.options[i] = null;
		}
	}
}

</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	<br style="clear:both;" />
	
	<?=$htmlAry['stepTable']?>
	
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="MenuID" name="MenuID" value="<?=$MenuID?>" />
	<input type="hidden" id="ParentMenuID" name="ParentMenuID" value="<?=$ParentMenuID?>" />
	<input type="hidden" id="Level" name="Level" value="<?=$Level?>" />
	<input type="hidden" id="defaultIcon" name="defaultIcon" value="" />
	<input type="hidden" id="iconOld" name="iconOld" value="<?=$custom_icon?>" />
	<input type="hidden" id="iconTypeOld" name="iconTypeOld" value="<?=$IconType?>" />
    <input type="hidden" id="goback" name="goback" value="0" />
	<?=$addHidden?>
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>