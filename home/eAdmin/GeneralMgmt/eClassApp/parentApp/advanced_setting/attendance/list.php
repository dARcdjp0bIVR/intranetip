<?php
// editing: 
/*
 * change log:
 * 2017-03-07 Tiffany [ip.2.5.8.4.1] [G89311]: added settings to show / hide attendance status statistics in eAttendance module;
 * 2015-12-15 Roy [ip.2.5.7.1.1] [U88786]: added settings to show / hide attendance time in eAttendance module; improve layout and wordings
 * 2015-12-11 Ivan [ip.2.5.7.1.1] [K90132]: added settings to send push message for non-school day
 * 2015-11-17 Ivan [ip.2.5.7.1.1] [P88366]: added settings to send arrival and/or leave push message
 * 2015-07-10 Roy [ip.2.5.6.7.1.0]:
 * 	- add show attendance time, show attendance leave section and show eAttendance option
 * 2014-11-06 Ivan [ip.2.5.5.12.1]: create file
 */

include_once($PATH_WRT_ROOT.'includes/libclass.php');
include_once($PATH_WRT_ROOT.'includes/libinterface.php');

$lclass = new libclass();
$linterface= new interface_html();
 
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'ParentApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getParentAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['eAttendance']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();


### construct form table
$formAry = $lclass->getLevelArray();
$numOfForm = count($formAry);

$x = '';
$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:50%;">'.$Lang['General']['Form'].'</th>'."\r\n";
			$x .= '<th style="width:50%; text-align:center;">'.$Lang['General']['Status2'].'</th>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		for ($i=0; $i<$numOfForm; $i++) {
			$_classLevelId = $formAry[$i]['ClassLevelID'];
			$_levelName = $formAry[$i]['LevelName'];
			
			$_settingKey = $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_'.$_classLevelId;
			$_enablePushMessage = $eClassAppSettingsObj->getSettingsValue($_settingKey);
			if ($_enablePushMessage) {
				$_imageName = 'icon_tick_green.gif';
			}
			else {
				$_imageName = 'icon_delete_b.gif';
			}
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$_levelName.'</td>'."\r\n";
				$x .= '<td style="text-align:center;">'."\r\n";
					$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/'.$_imageName.'" width="18" height="18" border="0" align="absmiddle">'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### attendance status is showed in app home page
$attendanceStatusEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']);
if($attendanceStatusEnable == null){
	$attendanceStatusEnable = 1;
}
$statusShowInApp = ($attendanceStatusEnable == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

# 20150710 Roy
### attendance time is showed in app home page
$attendanceTimeEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']);
if($attendanceTimeEnable == null){
	$attendanceTimeEnable = 1;
}
$timeShowInApp = ($attendanceTimeEnable == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

### Leave Section is showed in app home page
$attendanceLeaveSectionEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']);
if($attendanceLeaveSectionEnable == null){
	$attendanceLeaveSectionEnable = 1;
}
$leaveSectionShowInApp = ($attendanceLeaveSectionEnable == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

### eAttendance module is showed in app menu
$eAttendanceEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']);
if($eAttendanceEnable == null){
	$eAttendanceEnable = 1;
}
$eAttendanceShowInApp = ($eAttendanceEnable == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

// 2015-12-15 U88786 Roy
### attendance time is showed in eAttendance module
$eAttendanceTimeEnable  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable']);
if($eAttendanceTimeEnable == null){
	$eAttendanceTimeEnable = 1;
}
$timeShowInEAttendance = ($eAttendanceTimeEnable == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

### attendance status statistics in eAttendance module
$eAttendanceStatusStatisticsEnable = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable']);
if($eAttendanceStatusStatisticsEnable == null){
	$eAttendanceStatusStatisticsEnable = 1;
}
$statusStatisticsShowInEAttendance = ($eAttendanceStatusStatisticsEnable == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

# tap card push message type
$sendArrivalMsg = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival']);
$sendLeaveMsg  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave']);
$statusDisplayAry = array();
if ($sendArrivalMsg) {
	$statusDisplayAry[] = $Lang['eClassApp']['TapCardStatusAry']['Arrival'];
}
if ($sendLeaveMsg) {
	$statusDisplayAry[] = $Lang['eClassApp']['TapCardStatusAry']['Leave'];
}
$statusDisplay = implode(', ', (array)$statusDisplayAry);
$htmlAry['pushMessageStatusDisplay'] = ($statusDisplay)? $statusDisplay : $Lang['General']['EmptySymbol'];


### send tap card push message on non-school day
$x = '';
$sendTapCardPushMessageOnNonSchoolDay  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']);
$x .= ($sendTapCardPushMessageOnNonSchoolDay == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];
if ($sendTapCardPushMessageOnNonSchoolDay) {
	$sendArrivalMsgNonSchoolDay = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival']);
	$sendLeaveMsgNonSchoolDay  = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave']);
	$statusDisplayAry = array();
	if ($sendArrivalMsgNonSchoolDay) {
		$statusDisplayAry[] = $Lang['eClassApp']['TapCardStatusAry']['Arrival'];
	}
	if ($sendLeaveMsgNonSchoolDay) {
		$statusDisplayAry[] = $Lang['eClassApp']['TapCardStatusAry']['Leave'];
	}
	$x .= '	<br>
			<div style="border:1px solid #CCCCCC; padding:10px;">
				'.$Lang['eClassApp']['SendTapCardPushMessageType'].': '.implode(', ', (array)$statusDisplayAry)
			.'</div>';
}
$htmlAry['sendTapCardPushMessageOnNonSchoolDayDisplay'] = $x;


### construct table content
$x = '';
$x .= $linterface->Get_Form_Separate_Title($Lang['eClassApp']['PushMessage']);
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['SendTapCardPushMessage'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['formTable'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['SendTapCardPushMessageType'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['pushMessageStatusDisplay'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['SendTapCardPushMessageOnNonSchoolDay'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['sendTapCardPushMessageOnNonSchoolDayDisplay'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
  $x .= '</table>'."\r\n";
  $x .= '<br/>';
  $x .= $linterface->Get_Form_Separate_Title($Lang['eClassApp']['AppHomePageDisplay']);
  $x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceStatus'].'</td>'."\r\n";
			$x .= '<td>'.$statusShowInApp.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceTime'].'</td>'."\r\n";
			$x .= '<td>'.$timeShowInApp.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceLeaveSection'].'</td>'."\r\n";
			$x .= '<td>'.$leaveSectionShowInApp.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
  $x .= '</table>'."\r\n";
  $x .= '<br/>';
  $x .= $linterface->Get_Form_Separate_Title($Lang['eClassApp']['AppEAttendanceDisplay']);
  $x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStudentAttendanceDetails'].'</td>'."\r\n";
			$x .= '<td>'.$eAttendanceShowInApp.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowAttendanceTimeInEAttendance'].'</td>'."\r\n";
			$x .= '<td>'.$timeShowInEAttendance.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['ShowStatusStatisticsInEAttendance'].'</td>'."\r\n";
			$x .= '<td>'.$statusStatisticsShowInEAttendance.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = '?task=parentApp/advanced_setting/attendance/edit';
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
</form>

<?
$indexVar['linterface']->LAYOUT_STOP();
?>