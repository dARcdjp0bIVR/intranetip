<?php
// editing: 

/*
 * change log:
 * 2017-03-07 Tiffany [ip.2.5.8.4.1] [G89311]: added settings to show / hide attendance status statistics in eAttendance module;
 * 2015-12-15 Roy [ip.2.5.7.1.1] [U88786]: added settings to show / hide attendance time in eAttendance module
 * 2015-12-11 Ivan [ip.2.5.7.1.1] [K90132]: added settings to send push message for non-school day
 * 2015-11-17 Ivan [ip.2.5.7.1.1] [P88366]: added settings to send arrival and/or leave push message
 * 2015-07-10 Roy [ip.2.5.6.7.1.0]:
 * 	- add show attendance time, show attendance leave section and show eAttendance option
 * 2014-11-06 Ivan: create file
 */
include_once($PATH_WRT_ROOT.'includes/libclass.php');
$lclass = new libclass();

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$successAry = array();


### save send right to DB
$tmpSettingsAssoAry = array();

$formAry = $lclass->getLevelArray();
$numOfForm = count($formAry);
for ($i=0; $i<$numOfForm; $i++) {
	$_classLevelId = $formAry[$i]['ClassLevelID'];
	$_settingKey = $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendancePushMessageEnable'].'_'.$_classLevelId;
	
	$tmpSettingsAssoAry[$_settingKey] = $_POST[$_settingKey];
}

### save attendance status show setting to DB
$attendanceStatusEnable =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceStatusEnable']] = $attendanceStatusEnable;

$attendanceTimeEnable =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceTimeEnable']] = $attendanceTimeEnable;

$attendanceLeaveSectionEnable =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['attendanceLeaveSectionEnable']] = $attendanceLeaveSectionEnable;

$eAttendanceEnable =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceEnable']] = $eAttendanceEnable;

$eAttendanceTimeEnable =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceTimeEnable']] = $eAttendanceTimeEnable;

$eAttendanceStatusStatisticsEnable =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['eAttendanceStatusStatisticsEnable']] = $eAttendanceStatusStatisticsEnable;

$tapCardPushMessageOnNonSchoolDayEnable =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageOnNonSchoolDay']] = $tapCardPushMessageOnNonSchoolDayEnable;
if ($tapCardPushMessageOnNonSchoolDayEnable) {
	$nonSchoolDayTapCardPushMessageType_Arrival =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival']];
	$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival']] = $nonSchoolDayTapCardPushMessageType_Arrival;
	$nonSchoolDayTapCardPushMessageType_Leave =  $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave']];
	$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave']] = $nonSchoolDayTapCardPushMessageType_Leave;
}
else {
	$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Arrival']] = 0;
	$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableNonSchoolDayTapCardPushMessageType_Leave']] = 0;
}



### tap card push message type
$sendArrivalMsg = $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival']];
$sendLeaveMsg  = $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave']];
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Arrival']] = ($sendArrivalMsg)? 1 : 0;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['enableTapCardPushMessageType_Leave']] = ($sendLeaveMsg)? 1 : 0;

$successAry[] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);


$returnMsgKey = (!in_array(false, $successAry))? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?task=parentApp/advanced_setting/attendance/list&returnMsgKey='.$returnMsgKey);
?>