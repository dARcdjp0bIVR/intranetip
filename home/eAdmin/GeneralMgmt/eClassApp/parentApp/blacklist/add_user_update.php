<?php
// using : 
$appType = $_POST['appType'];

$permittedTypeAry = array();
if ($appType == $eclassAppConfig['appType']['Parent']) {
	$appTypePath = 'parentApp';
	$permittedTypeAry[] = USERTYPE_PARENT;
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$appTypePath = 'studentApp';
	$permittedTypeAry[] = USERTYPE_STUDENT;
}


$userIdAry = Get_User_Array_From_Common_Choose($_POST['SelectedUserIdAry'], $permittedTypeAry);


$successAry = array();
$indexVar['libeClassApp']->Start_Trans();


$successAry['saveLocal'] = $indexVar['libeClassApp']->addBlacklistUser($userIdAry);


if (in_array(false, $successAry)) {
	$returnMsgKey = 'UpdateUnsuccess';
	$indexVar['libeClassApp']->RollBack_Trans();
}
else {
	$returnMsgKey = 'UpdateSuccess';
	$indexVar['libeClassApp']->Commit_Trans();
}


$task = $appTypePath.'/blacklist/list';
header('location: ?appType='.$appType.'&task='.$task.'&returnMsgKey='.$returnMsgKey);
?>