<?php
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$moduleEnableAssoAry = $_POST['moduleEnableAssoAry'];


### get all modules that can be applied to parent app
$appType = $eclassAppConfig['appType']['Parent'];
$appApplicableModuleAry = $indexVar['libeClassApp']->getAppApplicableModule($appType);
$numOfApplicableModule = count($appApplicableModuleAry);


### get forms data
$libYear = new Year();
$formAry = $libYear->Get_All_Year_List();
$numOfForm = count($formAry);

$consolidatedModuleAccessRightAssoAry = array();
for ($i=0; $i<$numOfApplicableModule; $i++) {
	$_moduleCode = $appApplicableModuleAry[$i];
	
	for ($j=0; $j<$numOfForm; $j++) {
		$__yearId = $formAry[$j]['YearID'];
		
		$__enableModuleInForm = ($moduleEnableAssoAry[$_moduleCode][$__yearId])? true : false;
		$consolidatedModuleAccessRightAssoAry[$_moduleCode][$__yearId] = $__enableModuleInForm;
	}
}

$success = $indexVar['libeClassApp']->saveModuleAccessRightSettings($appType, $consolidatedModuleAccessRightAssoAry);
$returnMsgKey = ($success)? 'UpdateSuccess' : 'UpdateUnsuccess';

header('location: ?task=parentApp/access_right/list&returnMsgKey='.$returnMsgKey);
?>