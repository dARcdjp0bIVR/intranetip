<?php
// using : anna
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$CurrentPage = 'ParentApp_AccessRight';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['FunctionAccessRight']);
$indexVar['linterface']->LAYOUT_START();



### get all modules that can be applied to parent app
$appType = $eclassAppConfig['appType']['Parent'];
$appApplicableModuleAry = $indexVar['libeClassApp']->getAppApplicableModule($appType);
$numOfApplicableModule = count($appApplicableModuleAry);


### get access right settings of each modules
//$accessRightAssoAry[$moduleCode][$yearId]['RecordStatus'] = 1
$accessRightAssoAry = $indexVar['libeClassApp']->getAccessRightInfo($appType);


### get forms data
$libYear = new Year();
$formAry = $libYear->Get_All_Year_List();
$numOfForm = count($formAry);


### calculate column width
$moduleColWidth = 25;
$formColWidth = floor((100 - $moduleColWidth) / $numOfForm);

### construct table content
$x = '';
$x .='<div id="parent">';
$x .= '<table id="fixTable"  class="common_table_list_v30 edit_table_list_v30" >'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:'.$moduleColWidth.'%;">&nbsp;</th>'."\r\n";
			for ($i=0; $i<$numOfForm; $i++) {
				$_formName = $formAry[$i]['YearName'];
				$x .= '<th style="width:'.$formColWidth.'%; text-align:center;">'.$_formName.'</th>';
			}
		$x .= '</tr>'."\r\n";
		// apply to all
		$x .= '<tr class="edit_table_head_bulk">'."\r\n";
			$x .= '<th><div style="float:right;">'.$indexVar['linterface']->Get_Checkbox('applyToAllChk', 'applyToAllChk', $Value=1, false, '', '', 'applyToAll(this.checked);').'</div></th>'."\r\n";
			for ($i=0; $i<$numOfForm; $i++) {
				$_yearId = $formAry[$i]['YearID'];
				
				$_checkboxId = 'applyAllModuleToFormChk_'.$_yearId;
				$_checkboxClass = 'applyAllModuleToFormChk';
				$_checkbox = $indexVar['linterface']->Get_Checkbox($_checkboxId, '', $_yearId, false, $_checkboxClass, '', 'applyAllModuleToForm(this.value, this.checked);');
				
				$x .= '<th style="text-align:center;">'."\r\n";
				$x .= $_checkbox."\r\n";
				$x .= '</th>'."\r\n";
			}
		$x .= '</tr>'."\r\n";
		
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		
	
		for ($i=0; $i<$numOfApplicableModule; $i++) {
			$_moduleCode = $appApplicableModuleAry[$i];
			$_moduleName = $indexVar['libeClassApp']->getModuleName($_moduleCode);
			
			$_checkboxId = 'applyAllFormToModuleChk_'.$_moduleCode;
			$_checkboxClass = 'applyAllFormToModuleChk';
			$_applyModuleToAllFormChk = $indexVar['linterface']->Get_Checkbox($_checkboxId, '', $_moduleCode, false, $_checkboxClass, '', 'applyAllFormToModule(this.value, this.checked);');
			
			$x .= '<tr>'."\r\n";
				$x .= '<td>'."\r\n";
					$x .= '<div style="float:left;">'.$_moduleName.'</div>'."\r\n";
					$x .= '<div style="float:right;">'.$_applyModuleToAllFormChk.'</div>'."\r\n";
				$x .= '</td>'."\r\n";
				for ($j=0; $j<$numOfForm; $j++) {
					$__yearId = $formAry[$j]['YearID'];
					$__isModuleEnabled = $accessRightAssoAry[$__yearId][$_moduleCode]['RecordStatus'];
					
					$_checkboxId = 'moduleEnableChk_'.$_moduleCode.'_'.$__yearId;
					$_checkboxName = 'moduleEnableAssoAry['.$_moduleCode.']['.$__yearId.']';
					$_checkboxClass = 'moduleEnableChk moduleEnableChk_moduleCode_'.$_moduleCode.' moduleEnableChk_yearId_'.$__yearId;
					$_checkbox = $indexVar['linterface']->Get_Checkbox($_checkboxId, $_checkboxName, $Value=1, $__isModuleEnabled, $_checkboxClass);
					
					$x .= '<td style="text-align:center;">'."\r\n";
						$x .= $_checkbox."\r\n";
					$x .= '</td>'."\r\n";
				}
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$x .='</div>';
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');


?>
<script src="/templates/jquery/jquery-2.1.3.js"></script>
<script src="/templates/jquery/tableHeadFixer.js"></script>
<script type="text/javascript">
$(document).ready(function() {	
	var tableheight = $('#fixTable').css('height');
	var taleheightwithoutpx = parseInt(tableheight);
	if(taleheightwithoutpx > Get_Screen_Height()-130){
		var parentheight =  Get_Screen_Height()-130;
		}else{
			var parentheight = taleheightwithoutpx;
			}
	$('#parent').css('height',parentheight).css('width', Get_Screen_Width()-190);
	$("#fixTable").tableHeadFixer({"left" : 1}); 
	$("#fixTable").tableHeadFixer({
		head: true,
		foot: false
	});	
});

function goBack() {
	window.location = '?task=parentApp/access_right/list';
}

function goSubmit() {
	$('input#task').val('parentApp/access_right/edit_update');
	$('form#form1').submit();
}

function applyToAll(parChecked) {
// 	$('input.applyAllModuleToFormChk, input.applyAllFormToModuleChk, input.moduleEnableChk').attr('checked', parChecked);
	$('input.applyAllModuleToFormChk, input.applyAllFormToModuleChk, input.moduleEnableChk').prop('checked', parChecked);
}

function applyAllModuleToForm(yearId, parChecked) {
	var targetChkClass = 'moduleEnableChk_yearId_' + yearId;
//	$('input.' + targetChkClass).attr('checked', parChecked);
	$('input.' + targetChkClass).prop('checked', parChecked);
	
	// uncheck apply to all if unchecked
	Uncheck_SelectAll('applyToAllChk', parChecked);
	if (!parChecked) {
//		$('input.applyAllFormToModuleChk').attr('checked', parChecked);
		$('input.applyAllFormToModuleChk').prop('checked', parChecked);
	}
}

function applyAllFormToModule(moduleCode, parChecked) {
	var targetChkClass = 'moduleEnableChk_moduleCode_' + moduleCode;
	$('input.' + targetChkClass).prop('checked', parChecked);
// 	$('input.' + targetChkClass).attr('checked', parChecked);
	
	// uncheck apply to all if unchecked
	Uncheck_SelectAll('applyToAllChk', parChecked);
	if (!parChecked) {
// 		$('input.applyAllModuleToFormChk').attr('checked', parChecked);
		$('input.applyAllModuleToFormChk').prop('checked', parChecked);
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>