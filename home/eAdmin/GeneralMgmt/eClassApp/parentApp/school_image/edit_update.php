<?php
// using : 
/********************************
 *  2017-03-12 Ronald
 *      - Add Parent App save banner
 *      - save image as default image if selecting default image
 *
 * 	2015-07-29 Evan
 * 		-Add parameter "schoolBadge" or "backgroundImage" when calling function saveSchoolImage() for the resizing procedure
 * 
 ********************************/
include_once($PATH_WRT_ROOT."includes/libuser.php");


$userObj = new libuser($_SESSION['UserID']);
if ($appType == $eclassAppConfig['appType']['Parent']) {
	// if app in trial, only broadlearning account can edit school banner
	$canEditBanner = true;
	if ($sys_custom['eClassApp']['trialMode'] && $userObj->UserLogin != 'broadlearning') {
		$canEditBanner = false;
	}
}
else if ($appType == $eclassAppConfig['appType']['Teacher']) {
	// if app in trial, only broadlearning account can edit school banner
	$canEditBanner = true;
	if ($sys_custom['eClassTeacherApp']['trialMode'] && $userObj->UserLogin != 'broadlearning') {
		$canEditBanner = false;
	}
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	// if app in trial, only broadlearning account can edit school banner
	$canEditBanner = true;
	if ($sys_custom['eClassStudentApp']['trialMode'] && $userObj->UserLogin != 'broadlearning') {
		$canEditBanner = false;
	}
}

$successAry = array();


$successAry['schoolBagde'] = $indexVar['libeClassApp']->saveSchoolImage($appType, $eclassAppConfig['imageType']['schoolBadge'], $_FILES['schoolBadge']);
if ($canEditBanner) {

    if($appType == $eclassAppConfig['appType']['Parent']){
        if($backgroundType == 2){
            $successAry['backgroundImage'] = $indexVar['libeClassApp']->saveSchoolDefaultImage($appType, $eclassAppConfig['imageType']['backgroundImage'], $defaultBackground);
        }else{
            $successAry['backgroundImage'] = $indexVar['libeClassApp']->saveSchoolImage($appType, $eclassAppConfig['imageType']['backgroundImage'], $_FILES['backgroundImage']);
        }

        if($bannerType == 2){
            $successAry['accountPageBanner'] = $indexVar['libeClassApp']->saveSchoolDefaultImage($appType, $eclassAppConfig['imageType']['accountPageBanner'], $defaultBanner);
        }else{
            $successAry['accountPageBanner'] = $indexVar['libeClassApp']->saveSchoolImage($appType, $eclassAppConfig['imageType']['accountPageBanner'], $_FILES['accountPageBanner']);
        }
    }

    if($appType == $eclassAppConfig['appType']['Teacher']){
        if($backgroundType == 2){
            $successAry['backgroundImage'] = $indexVar['libeClassApp']->saveSchoolDefaultImage($appType, $eclassAppConfig['imageType']['backgroundImage'], $defaultBackground);
        }else{
            $successAry['backgroundImage'] = $indexVar['libeClassApp']->saveSchoolImage($appType, $eclassAppConfig['imageType']['backgroundImage'], $_FILES['backgroundImage']);
        }
    }

    if($appType == $eclassAppConfig['appType']['Student']){
        $successAry['backgroundImage'] = $indexVar['libeClassApp']->saveSchoolImage($appType, $eclassAppConfig['imageType']['backgroundImage'], $_FILES['backgroundImage']);
    }
}
//Henry HM 2017-06-06
$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$eClassAppSettingsObj->saveSettings(array(
    'parentAppSchoolWebsiteURL'=>$_POST['schoolWebsiteURL']
));

$returnMsgKey = (!in_array(false, $successAry))? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?task=parentApp/school_image/list&appType='.$appType.'&returnMsgKey='.$returnMsgKey);
?>