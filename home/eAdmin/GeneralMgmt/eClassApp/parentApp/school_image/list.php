<?php
// using : 
/********************************
 *  2017-03-12 Ronald
 *      - Add Parent App display banner
 *      - Modify display size
 *
 * 	2015-07-29 Evan
 * 		-Add timestamp after the path of the pitures to prevent from fetching old pictures
 * 		 Delete default width style to show the real size of the pictures
 * 
 ********************************/
$returnMsgKey = $_GET['returnMsgKey'];

if ($appType == $eclassAppConfig['appType']['Parent']) {
	$CurrentPage = 'ParentApp_SchoolImage';
}
else if ($appType == $eclassAppConfig['appType']['Teacher']) {
	$CurrentPage = 'TeacherApp_SchoolImage';
}
else if ($appType == $eclassAppConfig['appType']['Student']) {
	$CurrentPage = 'StudentApp_SchoolImage';
}

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['SchoolImage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);


### warning box
$htmlAry['warningBox'] = $indexVar['linterface']->Get_Warning_Message_Box('', $Lang['eClassApp']['ImageResolutionWarning']);


### get school image info
$schoolBadgePath = $indexVar['libeClassApp']->getSchoolImageUrlPath($appType, $eclassAppConfig['imageType']['schoolBadge']);
if ($schoolBadgePath) {
	$schoolBadgeDisplay = '<img height="200px" src="'.$schoolBadgePath.'?ts='.time().'" />';
}
else {
	$schoolBadgeDisplay = $Lang['General']['EmptySymbol'];
}

$backgroundImagePath = $indexVar['libeClassApp']->getSchoolImageUrlPath($appType, $eclassAppConfig['imageType']['backgroundImage']);
if ($backgroundImagePath) {
	$backgroundImageDisplay = '<img height="200px" src="'.$backgroundImagePath.'?ts='.time().'" />';
}
else {
	$backgroundImageDisplay = $Lang['General']['EmptySymbol'];
}

if ($appType == $eclassAppConfig['appType']['Parent']) {
    $accountPageBannerPath = $indexVar['libeClassApp']->getSchoolImageUrlPath($appType, $eclassAppConfig['imageType']['accountPageBanner']);
    if ($accountPageBannerPath) {
        $accountPageBannerDisplay = '<img height="100px" src="'.$accountPageBannerPath.'?ts='.time().'" />';
    }
    else {
        $accountPageBannerDisplay = $Lang['General']['EmptySymbol'];
    }
}
$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$schoolWebsiteURL = $eClassAppSettingsObj->getSettingsValue('parentAppSchoolWebsiteURL');

### suggested resolution remarks display
$x = $Lang['eClassApp']['ImageResolutionRemarks'];
$x = str_replace('<!--width-->', $eclassAppConfig['suggestedResolution']['schoolBadge']['width'], $x);
$x = str_replace('<!--height-->', $eclassAppConfig['suggestedResolution']['schoolBadge']['height'], $x);
$htmlAry['schoolBadgeResolution'] = '<span class="tabletextremark">'.$x.'</span>';

$x = $Lang['eClassApp']['ImageResolutionRemarks'];
$x = str_replace('<!--width-->', $eclassAppConfig['suggestedResolution']['backgroundImage']['width'], $x);
$x = str_replace('<!--height-->', $eclassAppConfig['suggestedResolution']['backgroundImage']['height'], $x);
$htmlAry['backgroundImageResolution'] = '<span class="tabletextremark">'.$x.'</span>';

if ($appType == $eclassAppConfig['appType']['Parent']) {
    $x = $Lang['eClassApp']['ImageResolutionRemarks'];
    $x = str_replace('<!--width-->', $eclassAppConfig['suggestedResolution']['accountPageBanner']['width'], $x);
    $x = str_replace('<!--height-->', $eclassAppConfig['suggestedResolution']['accountPageBanner']['height'], $x);
    $htmlAry['accountPageBannerResolution'] = '<span class="tabletextremark">'.$x.'</span>';
}


### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['SchoolBadge'].'<br>'.$htmlAry['schoolBadgeResolution'].'</td>'."\r\n";
			$x .= '<td>'.$schoolBadgeDisplay.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['BackgroundImage'].'<br>'.$htmlAry['backgroundImageResolution'].'</td>'."\r\n";
			$x .= '<td>'.$backgroundImageDisplay.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		if ($appType == $eclassAppConfig['appType']['Parent']) {
    		$x .= '<tr>'."\r\n";
        		$x .= '<td class="field_title">'.$Lang['eClassApp']['AccountPageBanner'].'<br>'.$htmlAry['accountPageBannerResolution'].'</td>'."\r\n";
        		$x .= '<td>'.$accountPageBannerDisplay.'</td>'."\r\n";
    		$x .= '</tr>'."\r\n";
		}
        if($sys_custom['eClassApp']['SchoolWebsiteURL']) {
            $x .= '<tr>' . "\r\n";
                $x .= '<td class="field_title">' . $Lang['eClassApp']['SchoolWebsiteURL'] . '</td>' . "\r\n";
                $x .= '<td>' . ($schoolWebsiteURL == '' ? '---' : $schoolWebsiteURL) . '</td>' . "\r\n";
            $x .= '</tr>' . "\r\n";
        }
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');


?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = '?task=parentApp/school_image/edit&appType=<?=$appType?>';
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['warningBox']?>
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" name="appType" id="appType" value="<?=$appType?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>