<?php
// using : qiao
$returnMsgKey = $_GET['returnMsgKey'];


$CurrentPage = 'TeacherApp_AccessRight';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['FunctionAccessRight']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);



### get all modules that can be applied to parent app
$appType = $eclassAppConfig['appType']['Teacher'];
$appApplicableModuleAry = $indexVar['libeClassApp']->getAppApplicableModule($appType);
$numOfApplicableModule = count($appApplicableModuleAry);


### get access right settings of each modules
//$accessRightAssoAry[$moduleCode][$yearId]['RecordStatus'] = 1
$accessRightAssoAry = $indexVar['libeClassApp']->getAccessRightInfo($appType);


### construct table content
$x = '';
$x .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:50%;">'.$Lang['eClassApp']['Module'].'</th>'."\r\n";
			$x .= '<th style="width:50%; text-align:center;">'.$Lang['General']['Status'].'</th>';
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		for ($i=0; $i<$numOfApplicableModule; $i++) {
			$_moduleCode = $appApplicableModuleAry[$i];
			$_moduleName = $indexVar['libeClassApp']->getModuleName($_moduleCode);
			if($_moduleCode == $eclassAppConfig['moduleCode']['teacherStatus'] || $_moduleCode == $eclassAppConfig['moduleCode']['studentStatus'] || $_moduleCode == $eclassAppConfig['moduleCode']['groupMessage'] || $_moduleCode == $eclassAppConfig['moduleCode']['schoolInfo']){
				$_moduleName .= ' <a id="'.$_moduleCode.'" href="#" onclick="goAdvanceSetting(this.id);return false;">['.$Lang['eClassApp']['AdvancedSetting'].']</a>';
			}
			$x .= '<tr>'."\r\n";
				$x .= '<td>'.$_moduleName.'</td>'."\r\n";
				$__yearId = 0;	// teacher no form
				$__isModuleEnabled = $accessRightAssoAry[$__yearId][$_moduleCode]['RecordStatus'];
				
				if ($__isModuleEnabled) {
					$__imageName = 'icon_tick_green.gif';
				}
				else {
					$__imageName = 'icon_delete_b.gif';
				}
				
				$x .= '<td style="text-align:center;">'."\r\n";
					$x .= '<img src="'.$PATH_WRT_ROOT.'/images/'.$LAYOUT_SKIN.'/'.$__imageName.'" width="18" height="18" border="0" align="absmiddle">'."\r\n";
				$x .= '</td>'."\r\n";
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');


?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = '?task=teacherApp/access_right/edit&appType=<?=$appType?>';
}
function goAdvanceSetting(moduleCode){
	var settingPath = 'teacher_status_right';
	if(moduleCode =='<?=$eclassAppConfig['moduleCode']['studentStatus']?>'){
		settingPath = 'student_list_right';
		window.location = '?appType=<?=$appType?>&task=teacherApp/advanced_setting/'+settingPath+'/list';
	}
	else if(moduleCode =='<?=$eclassAppConfig['moduleCode']['teacherStatus']?>'){
		settingPath = 'teacher_status_right';
		window.location = '?appType=<?=$appType?>&task=teacherApp/advanced_setting/'+settingPath+'/list';
	}
	else if (moduleCode =='<?=$eclassAppConfig['moduleCode']['groupMessage']?>'){
		window.location = '?appType=<?=$eclassAppConfig['appType']['Common']?>&task=commonFunction/group_message/index';
	}
	else if (moduleCode =='<?=$eclassAppConfig['moduleCode']['schoolInfo']?>'){
		window.location = '?appType=<?=$eclassAppConfig['appType']['Common']?>&task=commonFunction/school_info/index';
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>