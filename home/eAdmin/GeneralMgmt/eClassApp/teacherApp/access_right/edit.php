<?php
// using : 

$CurrentPage = 'TeacherApp_AccessRight';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ[] = array($Lang['eClassApp']['FunctionAccessRight']);
$indexVar['linterface']->LAYOUT_START();



### get all modules that can be applied to parent app
$appType = $eclassAppConfig['appType']['Teacher'];
$appApplicableModuleAry = $indexVar['libeClassApp']->getAppApplicableModule($appType);
$numOfApplicableModule = count($appApplicableModuleAry);


### get access right settings of each modules
//$accessRightAssoAry[$moduleCode][$yearId]['RecordStatus'] = 1
$accessRightAssoAry = $indexVar['libeClassApp']->getAccessRightInfo($appType);


### construct table content
$x = '';
$x .= '<table class="common_table_list_v30 edit_table_list_v30">'."\r\n";
	$x .= '<thead>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<th style="width:50%;">'.$Lang['eClassApp']['Module'].'</th>'."\r\n";
			$x .= '<th style="width:50%; text-align:center;">'.$Lang['General']['Status'].'</th>';
		$x .= '</tr>'."\r\n";
	$x .= '</thead>'."\r\n";
	$x .= '<tbody>'."\r\n";
		// apply to all
		$x .= '<tr class="edit_table_head_bulk">'."\r\n";
			$x .= '<th>&nbsp;</th>'."\r\n";
			$_yearId = 0;	// teacher no form
			
			$_checkboxId = 'applyAllModuleToFormChk_'.$_yearId;
			$_checkboxClass = 'applyAllModuleToFormChk';
			$_checkbox = $indexVar['linterface']->Get_Checkbox($_checkboxId, '', $_yearId, false, $_checkboxClass, '', 'applyAllModuleToForm(this.value, this.checked);');
			
			$x .= '<th style="text-align:center;">'."\r\n";
				$x .= $_checkbox."\r\n";
			$x .= '</th>'."\r\n";
		$x .= '</tr>'."\r\n";
		
	
		for ($i=0; $i<$numOfApplicableModule; $i++) {
			$_moduleCode = $appApplicableModuleAry[$i];
			$_moduleName = $indexVar['libeClassApp']->getModuleName($_moduleCode);
			
			$x .= '<tr>'."\r\n";
				$__yearId = 0;	// teacher no form
				$__isModuleEnabled = $accessRightAssoAry[$__yearId][$_moduleCode]['RecordStatus'];
				
				$_checkboxId = 'moduleEnableChk_'.$_moduleCode.'_'.$__yearId;
				$_checkboxName = 'moduleEnableAssoAry['.$_moduleCode.']['.$__yearId.']';
				$_checkboxClass = 'moduleEnableChk moduleEnableChk_moduleCode_'.$_moduleCode.' moduleEnableChk_yearId_'.$__yearId;
				$_checkbox = $indexVar['linterface']->Get_Checkbox($_checkboxId, $_checkboxName, $Value=1, $__isModuleEnabled, $_checkboxClass);
				
				$x .= '<td>'."\r\n";
					$x .= '<div style="float:left;"><label for="'.$_checkboxId.'">'.$_moduleName.'</label></div>'."\r\n";
				$x .= '</td>'."\r\n";
				$x .= '<td style="text-align:center;">'."\r\n";
					$x .= $_checkbox."\r\n";
				$x .= '</td>'."\r\n";
				
			$x .= '</tr>'."\r\n";
		}
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;


### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');


?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = '?task=teacherApp/access_right/list&appType=<?=$appType?>';
}

function goSubmit() {
	$('input#task').val('teacherApp/access_right/edit_update');
	$('form#form1').submit();
}

function applyToAll(parChecked) {
	$('input.applyAllModuleToFormChk, input.applyAllFormToModuleChk, input.moduleEnableChk').attr('checked', parChecked);
}

function applyAllModuleToForm(yearId, parChecked) {
	var targetChkClass = 'moduleEnableChk_yearId_' + yearId;
	$('input.' + targetChkClass).attr('checked', parChecked);
	
	// uncheck apply to all if unchecked
	Uncheck_SelectAll('applyToAllChk', parChecked);
	if (!parChecked) {
		$('input.applyAllFormToModuleChk').attr('checked', parChecked);
	}
}

function applyAllFormToModule(moduleCode, parChecked) {
	var targetChkClass = 'moduleEnableChk_moduleCode_' + moduleCode;
	$('input.' + targetChkClass).attr('checked', parChecked);
	
	// uncheck apply to all if unchecked
	Uncheck_SelectAll('applyToAllChk', parChecked);
	if (!parChecked) {
		$('input.applyAllModuleToFormChk').attr('checked', parChecked);
	}
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
	
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>