<?php
// editing:

/*
 * change log:
 * 2014-11-05 qiao: create file
 */
 $isPublic = standardizeFormPostValue($_POST['IsPublic']);
 $GroupSelection = standardizeFormPostValue($_POST['GroupSelection']);
 
 $eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
 $successAry = array();
	if ($isPublic == 'Y') {
		//choose all teaching staff
        $tmpSettingsAssoAry = array();
        $tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherStatus']] = 1;
		$successAry[] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);
	}
	else {
		//get selected teaching staff
		$tmpSettingsAssoAry = array();
        $tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherStatus']] = 0;
		$successAry[] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);
		
		// get all userid
	    $allIntranetAppTeacherStatusUserDB = $indexVar['libeClassApp']->getAllIntranetAppTeacherStatusUser();
	   
		$recipientAry = $_POST['Recipient'];
		$userIDMatchAry = array();
		foreach ((array)$recipientAry as $value) {
		  $value = str_replace("U","",$value);
		  $userIDMatchAry[] = $value;
		 if(in_array($value, $allIntranetAppTeacherStatusUserDB)){
		 	//already in the DB
		 	continue;
		 }
		 else{
		 	//insert db
		 	$successAry[] = $indexVar['libeClassApp']->addIntranetAppTeacherStatusUsers($value);
		 }
		}		
		// delete removed user from db loop
		 $removedUserAry = array_diff($allIntranetAppTeacherStatusUserDB, $userIDMatchAry);
		 if(count($removedUserAry)>0){
		 $removedUserAryString = implode(",", $removedUserAry);
	     $successAry[] =  $indexVar['libeClassApp']->deleteIntranetAppTeacherStatusRemovedUsers($removedUserAryString);	
		 }
	}
	
	$tmpSettingsAssoAry = array();
	if($GroupSelection == 0){
	//enable specific groups
	$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']] = 0;
	###save these groups into table INTRANET_APP_TEACHER_LIST_ENABLE_GROUP
	$allIntranetAppTeacherListEnableGroupIDAryDB =  $indexVar['libeClassApp']->getIntranetAppTeacherListEnableGroup('GroupID');
	$groupIDAry = $_POST['GroupID'];
	foreach ($groupIDAry as &$value) {
		if(in_array($value, $allIntranetAppTeacherListEnableGroupIDAryDB)){
		 	//already in the DB
		 	continue;
		 }
		 else{
		 	//insert db
		 	$successAry[] = $indexVar['libeClassApp']->addIntranetAppTeacherStatusGroups($value);
		 }
	}
	$removedGroupAry = array_diff($allIntranetAppTeacherListEnableGroupIDAryDB,$groupIDAry);
	 if(count($removedGroupAry)>0){
	 	$removedGroupAryString = implode(",",$removedGroupAry);
	 	$successAry[] = $indexVar['libeClassApp']->deleteIntranetAppTeacherStatusRemovedGroups($removedGroupAryString);
	 }
		
	}else if($GroupSelection == 1){
	//disable all group
	$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']] = 1;
	}else if($GroupSelection == 2){
	//enable all group
	$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']] = 2;
	}
    $successAry[] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);
  $returnMsgKey = (!in_array(false, $successAry))? 'UpdateSuccess' : 'UpdateUnsuccess';
  header('location: ?task=teacherApp/advanced_setting/teacher_status_right/list&appType='.$appType.'&returnMsgKey='.$returnMsgKey);
 ?>