<?php
// editing: 

/* change log:
 * 2018-01-25 Isaac: fixed $authorizedGroupDisplay - missing Quotation mark issue.
 * 
 * 2017-06-15 icarus: changed the display format of $authorizedStaffDisplay
 * 
 * 2014-11-05 Qiao: create file
 */

$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'TeacherApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getTeacherAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['teacherStatus']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$_displayNameLang = Get_Lang_Selection('ChineseName', 'EnglishName');
###check the access module setting first
$accessRightInfo = $indexVar['libeClassApp']-> getAccessRightInfo($eclassAppConfig['appType']['Teacher']);
$teacherStatus = $accessRightInfo[0]['teacherStatus']['RecordStatus'];
if($teacherStatus == 0){
	
$htmlAry['formTable'] = $Lang['eClassApp']['TeacherStatus_UserNoAccessRight'];

}else{
	
$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$intranetAPPTeacherStatusUserSettingValue = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherStatus']);
$intranetAPPTeacherListGroupSettingValue = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPTeacherList_Group']);

###Staff List valid Users
if($intranetAPPTeacherStatusUserSettingValue == 1){
	$authorizedStaffDisplay = $Lang['eClassApp']['TeacherStatusAllTeacher'];
}else{
$authorizedStaffIDArray = $indexVar['libeClassApp']->getAllIntranetAppTeacherStatusUser();
$authorizedStaffArray = $indexVar['libeClassApp']->getAllIntranetAppTeacherStatusUserName(implode(",", $authorizedStaffIDArray),$_displayNameLang);	



########################################################################################
/* 	original code
$authorizedStaffDisplay = '<table style = "width:100%"><tr><td padding-bottom="2px" colspan="2">'.$Lang['eClassApp']['TeacherStatusAccessTeacher'].'</td></tr>';
for($ASACount = 0;$ASACount<count($authorizedStaffArray);$ASACount++){
	$authorizedStaffDisplay .='<tr><td style="border-bottom:none; padding-bottom: 0px">'.'- '.$authorizedStaffArray[$ASACount][$_displayNameLang].'</td></tr>';
}
*/


# the numbers of results show in a row
$numShowInRow = 3;
$authorizedStaffDisplay = '<table style = "width:100%"><tr><td padding-bottom="2px" colspan="'.$numShowInRow.'">'.$Lang['eClassApp']['TeacherStatusAccessTeacher'].'</td></tr>';
for($ASACount = 0;$ASACount<count($authorizedStaffArray);$ASACount+=$numShowInRow){
	$authorizedStaffDisplay .= '<tr>';
	for($i = $ASACount;$i<$ASACount+$numShowInRow;$i++){
		if ($i < count($authorizedStaffArray))
			$authorizedStaffDisplay .='<td style="border-bottom:none; padding-bottom: 0px">'.'- '.$authorizedStaffArray[$i][$_displayNameLang].'</td>';
	}
	$authorizedStaffDisplay .= '</tr>';
}
##########################################################################################

$authorizedStaffDisplay .= '</table>';
}
 ###Staff List valid Groups
 if($intranetAPPTeacherListGroupSettingValue == 1){
 	$authorizedGroupDisplay = $Lang['eClassApp']['TeacherStatusDisableGroup'];
 }else if($intranetAPPTeacherListGroupSettingValue == 2){
 	$authorizedGroupDisplay = $Lang['eClassApp']['TeacherStatusEnableAllGroup'];
 }else{
 	$authorizedGroupDisplay = '<table style = "width:100%"><tr><td>'.$Lang['eClassApp']['TeacherStatusAuthorizedGroup'].'</td></tr>';
 	$authorizedGroupAry = $indexVar['libeClassApp']->getIntranetAppTeacherListEnableGroup();
 	$TitleNameLang = Get_Lang_Selection('TitleChinese', 'Title');
 	for($i=0;$i<count($authorizedGroupAry);$i++){
 		$tempGroupTitle = $authorizedGroupAry[$i][$TitleNameLang] != null?$authorizedGroupAry[$i][$TitleNameLang]:$authorizedGroupAry[$i]['Title'];
 		$authorizedGroupDisplay .= '<tr><td style="border-bottom:none";>'.'- '.$tempGroupTitle.'</td></tr>';
 	}
 	$authorizedGroupDisplay .= '</table>';
  }

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['TeacherStatusAccessRole'].'<br></td>'."\r\n";
			$x .= '<td>'.$authorizedStaffDisplay.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
		$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['eClassApp']['TeacherStatusAccessGroup'] .'<br></td>'."\r\n";
		$x .= '<td>'.$authorizedGroupDisplay.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');	
}
?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = '?appType=T&task=teacherApp/advanced_setting/teacher_status_right/edit';
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>