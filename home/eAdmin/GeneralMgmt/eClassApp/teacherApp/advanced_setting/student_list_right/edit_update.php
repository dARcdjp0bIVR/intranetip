<?php
// editing:

/*
 * change log:
 * 2015-01-12  Qiao: create file
 */
 
$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$successAry = array();
//AllowViewAllStudentList_ClassTeacher
$moduleEnableAssoAry = $_POST['moduleEnableAssoAry'];
$classTeacherClassStudentListRight =  $moduleEnableAssoAry['ClassTeacher']['AllowViewAllStudentList']?1:0;
$classTeacherClassStudentContactRight =  $moduleEnableAssoAry['ClassTeacher']['AllowViewStudentContact']?1:0;
$nonClassTeacherClassStudentListRight =  $moduleEnableAssoAry['NonClassTeacher']['AllowViewAllStudentList']?1:0;
$nonClassTeacherClassStudentContactRight = $moduleEnableAssoAry['NonClassTeacher']['AllowViewStudentContact']?1:0;
$classTeacherCanViewOwnClassRight = $moduleEnableAssoAry['ClassTeacher']['AllowSendPushMessageToClassStudent']?1:0;
$classTeacherCanViewAllClassRight = $moduleEnableAssoAry['ClassTeacher']['AllowSendPushMessageToAllStudent']?1:0;
$nonClassTeacherCanViewAllClassRight = $moduleEnableAssoAry['NonClassTeacher']['AllowSendPushMessageToAllStudent']?1:0;
$classTeacherClassStudentContactClassRight =  $moduleEnableAssoAry['ClassTeacher']['AllowViewStudentContactClass']?1:0;

$classTeacherCanViewOwnClassRightStudentApp = $moduleEnableAssoAry['ClassTeacher']['AllowSendPushMessageToClassStudentApp'] ? 1 : 0;
$classTeacherCanViewAllClassRightStudentApp = $moduleEnableAssoAry['ClassTeacher']['AllowSendPushMessageToAllStudentApp'] ? 1 : 0;
$nonClassTeacherCanViewAllClassRightStudentApp = $moduleEnableAssoAry['NonClassTeacher']['AllowSendPushMessageToAllStudentApp'] ? 1 : 0;


### save send right to DB
$tmpSettingsAssoAry = array();
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewAllStudentList']] = $classTeacherClassStudentListRight;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContact']] = $classTeacherClassStudentContactRight;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewAllStudentList']] = $nonClassTeacherClassStudentListRight;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowViewStudentContact']] = $nonClassTeacherClassStudentContactRight;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudent']] = $classTeacherCanViewOwnClassRight;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudent']] = $classTeacherCanViewAllClassRight;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudent']] = $nonClassTeacherCanViewAllClassRight;


$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToClassStudentApp']] = $classTeacherCanViewOwnClassRightStudentApp;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowSendPushMessageToAllStudentApp']] = $classTeacherCanViewAllClassRightStudentApp;
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPNonClassTeacherAllowSendPushMessageToAllStudentApp']] = $nonClassTeacherCanViewAllClassRightStudentApp;

$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['intranetAPPClassTeacherAllowViewStudentContactClass']] = $classTeacherClassStudentContactClassRight;

$successAry[] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);

$returnMsgKey = (!in_array(false, $successAry))? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?task=teacherApp/advanced_setting/student_list_right/list&appType='.$appType.'&returnMsgKey='.$returnMsgKey);
?>