<?php
// editing:

/*
 * change log:IP25
 * 2015-7-23 Roy: create file
 */
$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'TeacherApp_AdvancedSetting';
$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getTeacherAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['pushMessage']);

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();

$receivePushMessageWhileLoggedOut = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);

### $receivePushMessageWhileLoggedOut status:
$checkYes = ($receivePushMessageWhileLoggedOut == 1) ? 1 : 0;
$checkNo = ($receivePushMessageWhileLoggedOut == 1) ? 0 : 1;
$x = '';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']."_yes", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut'], 1, $checkYes, $Class="", $Lang['General']['Yes'], $Onclick="",$isDisabled=0);
$x .= ' ';
$x .= $indexVar['linterface']->Get_Radio_Button($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']."_no", $eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut'], 0, $checkNo, $Class="", $Lang['General']['No'], $Onclick="",$isDisabled=0);
$htmlAry['receivePushMessageWhileLoggedOutRadioButton'] = $x;

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowTeacherReceivePushMessageWhileLoggedOut'].'</td>'."\r\n";
			$x .= '<td>'.$htmlAry['receivePushMessageWhileLoggedOutRadioButton'].'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['submitBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['backBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn');

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goBack() {
	window.location = '?task=teacherApp/advanced_setting/push_message_right/list&appType=<?=$appType?>';
}

function goSubmit() {
	
	$('input#task').val('teacherApp/advanced_setting/push_message_right/edit_update');
	$('form#form1').submit();
}

</script>
<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['backBtn']?>
		<p class="spacer"></p>
	</div>
		
	<input type="hidden" id="task" name="task" value="" />
	<input type="hidden" id="appType" name="appType" value="<?=$appType?>" />
	<input type="hidden" id="imageType" name="imageType" value="" />
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>