<?php
// editing:

/*
 * change log:
 * 2015-7-23 Roy: create file
 */
 
$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();
$successAry = array();

$receivePushMessageWhileLoggedOut = $_POST[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']];

### save send right to DB
$tmpSettingsAssoAry = array();
$tmpSettingsAssoAry[$eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']] = $receivePushMessageWhileLoggedOut;
$successAry[] = $eClassAppSettingsObj->saveSettings($tmpSettingsAssoAry);


$returnMsgKey = (!in_array(false, $successAry))? 'UpdateSuccess' : 'UpdateUnsuccess';
header('location: ?task=teacherApp/advanced_setting/push_message_right/list&appType='.$appType.'&returnMsgKey='.$returnMsgKey);
?>