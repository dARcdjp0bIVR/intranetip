<?php
// editing:

/*
 * change log:
 * 2015-7-23 Roy: create file
 */

$returnMsgKey = $_GET['returnMsgKey'];
$CurrentPage = 'TeacherApp_AdvancedSetting';

$MODULE_OBJ = $indexVar['libeClassApp']->getModuleObjArr();
$TAGS_OBJ = $indexVar['libeClassApp']->getTeacherAppAdvancedSettingsTagObj($eclassAppConfig['moduleCode']['pushMessage']);
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$indexVar['linterface']->LAYOUT_START($returnMsg);

$eClassAppSettingsObj = $indexVar['libeClassApp']->getAppSettingsObj();

$receivePushMessageWhileLoggedOut = $eClassAppSettingsObj->getSettingsValue($eclassAppConfig['INTRANET_APP_SETTINGS']['SettingName']['TeacherApp']['canReceivePushMessageWhileLoggedOut']);
$receivePushMessageWhileLoggedOutStatus = ($receivePushMessageWhileLoggedOut == 1) ? $Lang['General']['Yes'] : $Lang['General']['No'];

### construct table content
$x = '';
$x .= '<table class="form_table_v30">'."\r\n";
	$x .= '<tbody>'."\r\n";
		$x .= '<tr>'."\r\n";
			$x .= '<td class="field_title">'.$Lang['eClassApp']['AllowTeacherReceivePushMessageWhileLoggedOut'].'</td>'."\r\n";
			$x .= '<td>'.$receivePushMessageWhileLoggedOutStatus.'</td>'."\r\n";
		$x .= '</tr>'."\r\n";
	$x .= '</tbody>'."\r\n";
$x .= '</table>'."\r\n";
$htmlAry['formTable'] = $x;

### buttons
$htmlAry['editBtn'] = $indexVar['linterface']->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');	

?>
<script type="text/javascript">
$(document).ready( function() {
	
});

function goEdit() {
	window.location = '?appType=T&task=teacherApp/advanced_setting/push_message_right/edit';
}
</script>
<form name="form1" id="form1" method="POST">
	<div class="table_board">
		<?=$htmlAry['formTable']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['editBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
$indexVar['linterface']->LAYOUT_STOP();
?>