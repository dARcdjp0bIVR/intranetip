<?php
# using:

######################################
#   Date:   2019-09-26  Ray
#           created
#
######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ttmss_payment_receipt']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$libpos_ui = new libpos_ui();
$lpayment = new libpayment();
$CurrentPageArr['ePOS'] = 1;
$CurrentPage["PrintPage_Receipt"] = 1;

$linterface = new interface_html();

$lclass = new libclass();
$button_select = $i_status_all;
if($sys_custom['ttmss_payment_receipt']) {
	$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.action='';this.form.target='';this.form.submit()", $ClassName, "", '-- ' . $i_general_please_select . ' --');
} else {
	$select_class = $lclass->getSelectClass("name=ClassName onChange=this.form.action='';this.form.target='';this.form.submit()", $ClassName);
}

//$TAGS_OBJ[] = array($i_Payment_Menu_PrintPage_Receipt_PaymentItemList, $PATH_WRT_ROOT."home/admin/payment/printpages/receipt/paymentlist.php", 1);

$MODULE_OBJ = $libpos_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($msg == "2") $SysMsg = $linterface->GET_SYS_MSG("update");
?>
<br />
<script language='javascript'>
    function checkform(obj){
		<?php if($sys_custom['ttmss_payment_receipt']){ ?>
        var studentSelectionObj = document.getElementById('StudentID');
        var student_count = 0;
        if(!studentSelectionObj) {
            alert('<?=$Lang['General']['JS_warning']['SelectClass']?>');
            return;
        }

        for(var i=0;i<studentSelectionObj.options.length;i++){
            if(studentSelectionObj.options[i].selected){
                student_count++;
            }
        }
        
        if(student_count == 0) {
            alert('<?=$Lang['General']['JS_warning']['SelectStudent']?>');
            return;
        }
		<?php } ?>
        obj.action = "paymentlist_print_ttmss.php";
        obj.target = '_blank';
        obj.submit();
    }

    function SelectAll(obj)
    {
        for (i=0; i<obj.length; i++)
        {
            obj.options[i].selected = true;
        }
    }

</script>
<form name="form1" method="post" action="" >
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>
				<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr><td colspan="2" align="right"><?=$SysMsg?></td></tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$i_UserClassName?>
						</td>
						<td class="tabletext" width="70%">
							<?=$select_class?>
						</td>
					</tr>

					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$i_Profile_From?>
						</td>
						<td class="tabletext" width="70%">
							<?=$linterface->GET_DATE_PICKER('date_from',$date_from,'', "yy-mm-dd","","","","")?>
							<span class="tabletextremark">(yyyy-mm-dd)</span>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
							<?=$i_Profile_To?>
						</td>
						<td class="tabletext" width="70%">
							<?=$linterface->GET_DATE_PICKER('date_to',$date_to,'', "yy-mm-dd","","","","")?>
							<span class="tabletextremark">(yyyy-mm-dd)</span>
						</td>
					</tr>


					<?
					if ($ClassName != "") {
						//$select_student = $lclass->getStudentSelectByClass($ClassName,'name="StudentID[]" id="StudentID" multiple size="10" style="min-width:150px; width:200px;"');
						$select_student = $lpayment->getReceiptStudentSelectByClass($ClassName,'name="StudentID[]" id="StudentID" multiple size="10" style="min-width:150px; width:200px;" onchange="reloadPaymentItems();" ');
						?>

						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
								<?=$i_UserStudentName?>
							</td>
							<td class="tabletext" width="70%">
								<?=$select_student?>
								<?=$linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('StudentID'));"); ?>
							</td>
						</tr>
						<?
					}
					?>


					<?php if($sys_custom['ttmss_payment_receipt']){ ?>
						<tr>
							<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
								<?=$Lang['General']['Remark']?>:
							</td>
							<td class="tabletext" width="70%">
								<input type="text" name="ReceiptRemark" id="ReceiptRemark" value="">
							</td>
						</tr>
					<?php } ?>

				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:checkform(this.form)") ?>
			</td>
		</tr>
	</table>
</form>

<?
$linterface->LAYOUT_STOP();

if ($ClassName != "")
	print $linterface->FOCUS_ON_LOAD("form1.StudentID");
else
	print $linterface->FOCUS_ON_LOAD("form1.ClassName");

intranet_closedb();
?>
