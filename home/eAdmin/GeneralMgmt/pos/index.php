<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_auth();
intranet_opendb();

$ePOSUI = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

intranet_closedb();

header("Location: ".$PATH_WRT_ROOT."home/eAdmin/GeneralMgmt/pos/management/transaction/index.php");
?>