<?
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$linterface = new interface_html();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ManageTransaction'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['Transaction'], '', 0);

# Get Return Message
$msg = $_REQUEST['msg'];
$FromDate = $_REQUEST['FromDate'];
$ToDate = $_REQUEST['ToDate'];
$Keyword = stripslashes(urldecode(trim($_REQUEST['Keyword'])));
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

$pageSizeChangeEnabled = true; // globe variable to enable the change of number per page
	
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($ReturnMessage));

echo $libPOS_ui->Get_Manage_Transaction_Index($FromDate,$ToDate,$Keyword,$pageNo,$numPerPage,$order,$field);

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// ajax function
{
function Void_Transaction(TransactionLogID) {
	if (confirm('<?=$Lang['ePOS']['VoidTransactionConfirmMsg']?>')) {
		var InventoryReturnSelect = document.getElementsByName('InventoryReturn[]');
		var EvalStr = 'var PostVar = {';
		EvalStr += '"TransactionLogID":'+$('input#TransactionLogID').val()+',';
		for (var i=0; i< InventoryReturnSelect.length; i++) {
			EvalStr += '"InventoryReturn['+InventoryReturnSelect[i].id+']":"'+InventoryReturnSelect[i].selectedIndex+'",';
		}			
		EvalStr += '"Remark":"'+encodeURIComponent($('textarea#Remark').val())+'"';
		EvalStr += '};';
		
		eval(EvalStr);
		Block_Thickbox();
		$.post('ajax_void_transaction.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else if (data == '1') {
					$('input#ReturnMessage').val('<?=$Lang['ePOS']['VoidTransactionSuccess']?>');
					document.getElementById('form1').submit();
				}
				else {
					$('input#ReturnMessage').val('<?=$Lang['ePOS']['VoidTransactionFail']?>');
					document.getElementById('form1').submit();
				}
			});
		
	}
}

function Get_Void_Transaction_Form(TransactionLogID) {
	var PostVar = {
		"TransactionLogID":TransactionLogID
	};
	
	$.post('ajax_get_void_transaction_form.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
			}
		});
}
}

// DOM function 
{
function Change_Item_Return_Background(SelectionObj) {
	if (SelectionObj.selectedIndex == (SelectionObj.length-1)) {
		$('#'+SelectionObj.id).parent().parent().children().css('background-color','#EFFDDB');
	}
	else {
		$('#'+SelectionObj.id).parent().parent().children().css('background-color','#FFEDED');
	}
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

Thick_Box_Init();
</script>