<?
// Using : 
/*
 * 2020-04-04 (Cameron): pass $LogID to ajax_get_item_selection.php to avoid change item back
 *          - modify js_Change(), not allow to submit if item is not selected
 * 2019-01-03 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();

$LogID = trim($_REQUEST['LogID']);
$ItemIDs = $_REQUEST['ItemIDs'];
$LogType = trim($_REQUEST['LogType']);

$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$PageSize = $_REQUEST['num_per_page'];

echo $libPOS_ui->Get_Change_Thickbox_Content($LogID, $ItemIDs, $LogType, $pageNo, $PageSize, $order, $field);

?>


<script>
function js_Reload_Item_Selection(itemID, price){
	$.post('ajax_get_item_selection.php',{"CategoryID" : $('#CategoryIDFilter\\['+itemID+'\\]').val(), "ItemID" : itemID, "Price" : price, "LogID" : '<?php echo $LogID;?>'},
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else if (data != '') {
					$('#divItemSelection_'+itemID).html(data);
				}
			});
}

function js_Change(){
	var allItemSelected = true;
	$('select[name^="ItemIDFilter"]').each(function(){
		if ($(this).val() == '') {
			allItemSelected = false;
		}
	});

	if (allItemSelected == false ) {
		alert('<?php echo $Lang['ePOS']['SelectItemToReplaceWarning'];?>');
	}
	else {
    	if(confirm("<?=$Lang['ePOS']['jsWarningArr']['ChangeTheItems']?>")){
    		var objForm = document.getElementById('thickboxForm');
    		objForm.action = 'change_item_update.php';
    		objForm.target = '_self';
    		objForm.submit();
    	}
	}
}
</script>
<?
intranet_closedb();
?>