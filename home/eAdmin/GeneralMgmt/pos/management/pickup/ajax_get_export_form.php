<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$FromDate = $_REQUEST['FromDate'];
$ToDate = $_REQUEST['ToDate'];
$field = $_REQUEST['field'];
$order = $_REQUEST['order'];
$ClassName = $_REQUEST['ClassName'];
$keyword = $_REQUEST['keyword'];

$POSUI = new libpos_ui();

echo $POSUI->Get_Pickup_Export_Form($FromDate, $ToDate, $field, $order, $ClassName, $keyword);

intranet_closedb();
?>