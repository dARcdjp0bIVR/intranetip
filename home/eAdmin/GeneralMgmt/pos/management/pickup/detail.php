<?
// Using : 
/*
 * 2020-04-07 (Cameron): add parameter from to checkItem()
 *      pass actionFrom to onloadThickBox()
 *      
 * 2019-01-03 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ManagePickup'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['PickupManagement'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$ReturnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($ReturnMsg);

$LogID = trim($_REQUEST['LogID']);
$FromDate = trim($_REQUEST['FromDate']);
$ToDate = trim($_REQUEST['ToDate']);

$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$PageSize = $_REQUEST['num_per_page'];

echo $libPOS_ui->Get_Manage_Pickup_Detail($LogID, $FromDate, $ToDate,$void, $pageNo, $PageSize, $order, $field);

//$ItemID = $_REQUEST['ItemID'];
//$FromIndex = $_REQUEST['FromIndex'];
//
//# Preserve Table View of this page and the index page
///*
//if ($FromIndex == 1)
//{
//	# index table
//	$IndexInfoArr['Keyword'] = $_REQUEST['Keyword'];
//	$IndexInfoArr['CategoryIDFilter'] = $_REQUEST['CategoryIDFilter'];
//	$IndexInfoArr['pageNo'] = $_REQUEST['pageNo'];
//	$IndexInfoArr['order'] = $_REQUEST['order'];
//	$IndexInfoArr['field'] = $_REQUEST['field'];
//	$IndexInfoArr['num_per_page'] = $_REQUEST['num_per_page'];
//	
//	# log page table
//	$Keyword = '';
//	$pageNo = '';
//	$order = '';
//	$field = '';
//	$PageSize = '';
//	
//}
//else
//{ 
//*/
//	# index table
//	$IndexInfoArr['Keyword'] = isset($_REQUEST['Keyword_Index'])? $_REQUEST['Keyword_Index'] : $_REQUEST['Keyword'];
//	$IndexInfoArr['CategoryIDFilter'] = isset($_REQUEST['CategoryIDFilter_Index'])? $_REQUEST['CategoryIDFilter_Index'] : $_REQUEST['CategoryIDFilter'];
//	$IndexInfoArr['pageNo'] = isset($_REQUEST['pageNo_Index'])? $_REQUEST['pageNo_Index'] : $_REQUEST['pageNo'];
//	$IndexInfoArr['order'] = isset($_REQUEST['order_Index'])? $_REQUEST['order_Index'] : $_REQUEST['order'];
//	$IndexInfoArr['field'] = isset($_REQUEST['field_Index'])? $_REQUEST['field_Index'] : $_REQUEST['field'];
//	$IndexInfoArr['num_per_page'] = isset($_REQUEST['num_per_page_Index'])? $_REQUEST['num_per_page_Index'] : $_REQUEST['num_per_page'];
//	$IndexInfoArr['LogType'] = $_REQUEST['LogType'];
//
//	# log page table
//	$Keyword = isset($_REQUEST['Keyword'])? $_REQUEST['Keyword'] : $_REQUEST['Keyword_Index'];
//	$pageNo = isset($_REQUEST['pageNo'])? $_REQUEST['pageNo'] : $_REQUEST['pageNo_Index'];
//	$order = isset($_REQUEST['order'])?  $_REQUEST['order'] : $_REQUEST['order_Index'];
//	$field = isset($_REQUEST['field'])? $_REQUEST['field'] : $_REQUEST['field_Index'];
//	//$PageSize = isset($_REQUEST['num_per_page_Index'])? $_REQUEST['num_per_page_Index'] : $_REQUEST['num_per_page'];
//	$PageSize = isset($_REQUEST['numPerPage'])? $_REQUEST['numPerPage'] : $_REQUEST['num_per_page_Index'];
//	$LogType = $_REQUEST['LogType'];
//	
//	$date_range_cookies = $libPOS_ui->Get_Report_Date_Range_Cookies();
//	
//	if($StartDate==""){
//		$StartDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
//	}
//	if($EndDate==""){
//		$EndDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
//	}
//	//$StartDate = $_REQUEST['StartDate'];
//	//$EndDate = $_REQUEST['EndDate'];
////}
//
//$pageSizeChangeEnabled = true;
//
//echo $libPOS_ui->Get_Item_Inventory_Change_Log_UI($ItemID, $StartDate, $EndDate, $IndexInfoArr, $Keyword, $pageNo, $PageSize, $order, $field, $LogType);

?>


<script>

$(document).ready( function() {
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Report_Table();
	});
});

function js_Pickup_All(){
	if(confirm("<?=$Lang['ePOS']['jsWarningArr']['CollectAllItems']?>")){
		var objForm = document.getElementById('form1');
		objForm.action = 'pickup_item_update.php';
		objForm.target = '_self';
		objForm.submit();
	}
}

function returnCheckedElements(obj, element_name){
        len=obj.elements.length;
        var returnString='';
        var i=0;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name==element_name && obj.elements[i].checked)
				{
					//alert(obj.elements[i].value);
                returnString+= obj.elements[i].value+',';
				}
        }
        return returnString.slice(0, -1);
}

var elementString;
var url;
var logType;
var actionFrom;
function checkItem(obj,element,page,type,from){
	var obj=document.form1;
	actionFrom = from;
	if(countChecked(obj,element)==0){
    	alert(globalAlertMsg2);
	}
    else{
    	url = page;
    	elementString = returnCheckedElements(obj,element);
    	var title = '';
    	if(type == 'Change'){
    		title = '<?=$Lang['ePOS']['ChangeItem']?>';
    	}
    	else if(type == 'Void'){
    		title = '<?=$Lang['ePOS']['WriteoffItem']?>';
    	}
    	else if(type == 'Pickup'){
    		title = '<?=$Lang['ePOS']['Take']?>';
    	}
    	if(element == 'CompleteItemID[]'){
    		logType = 'Complete';
    	}
    	else if(element == 'NotCompleteItemID[]'){
    		logType = 'NotComplete';
    	}
    	
    	load_dyn_size_thickbox_ip(title, 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=780);
    }
}

function onloadThickBox() {
	$('div#TB_ajaxContent').load(
		url, 
		{ 
			ItemIDs : elementString,
			LogID : <?=$LogID?>,
			LogType : logType,
			ActionFrom: actionFrom
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}

function js_Back_To_Pickup_Index()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'index.php?FromLog=1';
	objForm.target = '_self';
	objForm.submit();
}

function js_Go_Print()
{
	var objForm = document.getElementById('form1');
	objForm.action = '../../report/pos_transaction_report/detail.php?LogID=<?=$LogID?>';
	objForm.target = '_blank';
	objForm.submit();
	
	// restore submit settings
	objForm.action = 'log_view.php';
	objForm.target = '_self';
}

function Void_Transaction(TransactionLogID) {
	if (confirm('<?=$Lang['ePOS']['VoidTransactionConfirmMsg']?>')) {
		var InventoryReturnSelect = document.getElementsByName('InventoryReturn[]');
		var EvalStr = 'var PostVar = {';
		EvalStr += '"TransactionLogID":'+$('input#TransactionLogID').val()+',';
		for (var i=0; i< InventoryReturnSelect.length; i++) {
			EvalStr += '"InventoryReturn['+InventoryReturnSelect[i].id+']":"'+InventoryReturnSelect[i].selectedIndex+'",';
		}			
		EvalStr += '"Remark":"'+encodeURIComponent($('textarea#Remark').val())+'"';
		EvalStr += '};';
		
		eval(EvalStr);
		Block_Thickbox();
		$.post('../transaction/ajax_void_transaction.php',PostVar,
			function(data){
				if (data == "die") 
					window.top.location = '/';
				else if (data == '1') {
//					$('input#ReturnMessage').val('<?=$Lang['ePOS']['VoidTransactionSuccess']?>');
//					document.getElementById('form1').submit();
					window.top.location = 'index.php?ReturnMessage=<?=$Lang['ePOS']['VoidTransactionSuccess']?>';
				}
				else {
//					$('input#ReturnMessage').val('<?=$Lang['ePOS']['VoidTransactionFail']?>');
//					document.getElementById('form1').submit();
					window.top.location = 'index.php?ReturnMessage=<?=$Lang['ePOS']['VoidTransactionFail']?>';
				}
			});
		
	}
}

function Get_Void_Transaction_Form(TransactionLogID) {
	var PostVar = {
		"TransactionLogID":TransactionLogID
	};
	
	$.post('../transaction/ajax_get_void_transaction_form.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
//				window.top.location = 'index.php';
				$('div#TB_ajaxContent').html(data);
			}
		});
}
</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>