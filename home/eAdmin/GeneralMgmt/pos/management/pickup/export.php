<?php
// Editing by 
/*
 * 2018-07-13 (Anna) : Added void for void transaction
 * 2017-03-17 (Carlos): Pass in $field and $order for sorting data to follow UI.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$libpos_ui = new libpos_ui();
$lexport = new libexporttext();

$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));
$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

$ExportContent = $libpos_ui->Get_POS_Transaction_Report_Export_Content($FromDate, $ToDate,$ClassName, $RecordStatus, $Keyword, $field, $order,$void);

intranet_closedb();

$filename = "pos_transaction_report.csv";
$lexport->EXPORT_FILE($filename, $ExportContent);
?>