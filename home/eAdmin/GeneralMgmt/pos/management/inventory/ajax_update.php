<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
intranet_opendb();


# Get data
$Action = stripslashes($_REQUEST['Action']);
$returnString = '';
if ($Action == "Update_Inventory")
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$AdjustedBy = stripslashes($_REQUEST['AdjustedBy']);
	$Remarks = stripslashes($_REQUEST['Remarks']);
	$ForIncrease = stripslashes($_REQUEST['ForIncrease']);
	$CostPrice = stripslashes($_REQUEST['CostPrice']);
	
	$objItem = new POS_Item($ItemID);
	$returnString = $objItem->Update_Inventory($AdjustedBy, $ForIncrease, $Remarks, $CostPrice);
}

intranet_closedb();

echo $returnString;
?>