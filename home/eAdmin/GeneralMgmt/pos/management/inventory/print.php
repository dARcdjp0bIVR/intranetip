<?php
// Editing by 
/*
 * 2015-03-02 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS = new libpos();
$libPOS_ui = new libpos_ui();

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));
$field = in_array($field,array(0,1,2,3,4,5,6))? $field : 0;
$asc_desc = $order == 1? " ASC" : " DESC";

$field_array = array("item.ItemName",
					 "cat.CategoryName",
					 "item.Barcode",
					 "item.ItemCount",
					 "item.UnitPrice",
					 "item.InventoryDateModify",
					 "item.InventoryModifyBy"
					 );

$sql = $libPOS->Get_Management_Inventory_Index_Sql($CategoryID, $Keyword, true);

$sql .= " ORDER BY ".$field_array[$field].$asc_desc;

$records = $libPOS->returnResultSet($sql);
$record_count = count($records);

$x = '<table cellpadding="5" cellspacing="0" border="0" align="center" width="95%" class="eSporttableborder">';
$x .= '<tr>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="1">#</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="15%">'.$Lang['ePOS']['Item'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="15%">'.$Lang['ePOS']['Category'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="10%">'.$Lang['ePOS']['Barcode'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="10%">'.$Lang['ePOS']['Inventory'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="10%">'.$Lang['ePOS']['UnitPrice'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="10%">'.$Lang['ePOS']['Photo'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="15%">'.$Lang['General']['LastUpdatedTime'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle" width="15%">'.$Lang['General']['LastUpdatedBy'].'</td>';
$x .= '</tr>';
for($i=0;$i<$record_count;$i++){
	
	$x .= '<tr>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($i+1).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['ItemName']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['CategoryName']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['Barcode']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['ItemCount']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['UnitPrice']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['PhotoLink']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['InventoryDateModify']).'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'.($records[$i]['InventoryModifyBy']).'</td>';
	$x .= '</tr>';
	
}
$x .= '</table>';


include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<style type="text/css" media="print">
.print_hide {display:none;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr>
		<td align="right">
			<?=$libPOS_ui->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "btn_print")?>
		</td>
	</tr>
</table>
<?php
if($sys_custom['ePOS']['ReportWithSchoolName']){
	$school_name = GET_SCHOOL_NAME();
	echo '<table width="95%" align="center" border="0">
			<tr>
				<td align="center"><h2><b>'.$school_name.'</b></h2></td>
			</tr>
		</table>';
}
?>
<table border="0" width="95%" cellpadding="2" align="center">
	<tr>
		<td align="center"><h3><b><?=$Lang['ePOS']['Inventory']?></b></h3></td>
	</tr>
</table>
<?=$x?>
<?php
//debug_r($records);
?>
<br>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td align="right">
			<?=$Lang['ePOS']['ReportGenerationTime']." : ".date('Y-m-d H:i:s')?>
		</td>
	</tr>
</table>
<br /><br />
<?php
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>