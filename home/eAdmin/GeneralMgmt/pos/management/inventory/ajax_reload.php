<?php
// Editing by 
/*
Change Log

Date: 2010-09-28 [Kenneth Chung]
Detail: Add flow "Inventory_Compare"
				disabled flow "Item_Inventory_Count" as of the discard of real time inventory check approach
*/
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);
$libpos_ui = new libpos_ui();

$returnString = '';
if ($Action == "Inventory_Table")
{
	$Keyword = stripslashes($_REQUEST['Keyword']);
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	
	$PageNumber = stripslashes($_REQUEST['PageNumber']);
	$Order = stripslashes($_REQUEST['Order']);
	$SortField = stripslashes($_REQUEST['SortField']);
	$PageSize = stripslashes($_REQUEST['PageSize']);
	$pageSizeChangeEnabled = true;
	
	$returnString = $libpos_ui->Get_Management_Inventory_Table($CategoryID, $Keyword, $PageNumber, $PageSize, $Order, $SortField);
}
else if ($Action == "Inventory_Edit_Layer")
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$ForIncrease = stripslashes($_REQUEST['ForIncrease']);
	
	$returnString = $libpos_ui->Get_Inventory_Edit_Layer($ItemID, $ForIncrease);
}
/*
else if ($Action == "Item_Inventory_Count")
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	include_once($PATH_WRT_ROOT."includes/libpos_item.php");
	
	$objItem = new POS_Item($ItemID);
	$counter = 0;
	while(1==1)
	{
		$counter++;
		$objItem->Get_Self_Info();
		
		echo $counter."<br>";
		
		echo '<script type="text/javascript">'."\n";
			echo 'window.parent.document.getElementById("ItemInventorySpan").innerHTML = "'.$objItem->ItemCount.'";'."\n";
			echo 'window.parent.document.getElementById("CurrentInventory").value = "'.$objItem->ItemCount.'";'."\n";
		echo '</script>'."\n";
		
		flush(); // used to send the echoed data to the client
		sleep(3); // a little break to unload the server CPU
	}
}
*/
else if ($Action == "Inventory_Compare") {
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$AdjustBy = stripslashes($_REQUEST['AdjustedBy']);
	include_once($PATH_WRT_ROOT."includes/libpos_item.php");
	
	$objItem = new POS_Item($ItemID);
	$returnString = (($objItem->ItemCount - $AdjustBy) >= 0)? "1":$Lang['ePOS']['WarningArr']['NoteEnoughtInventory']." ".$Lang['ePOS']['CurrentInventory'].": ".$objItem->ItemCount;
}
else if ($Action == "Inventory_Log_Table")
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$Keyword = stripslashes($_REQUEST['Keyword']);
	$StartDate = stripslashes($_REQUEST['StartDate']);
	$EndDate = stripslashes($_REQUEST['EndDate']);
	$LogType = stripslashes($_REQUEST['LogType']);
	
	$PageNumber = stripslashes($_REQUEST['PageNumber']);
	$Order = stripslashes($_REQUEST['Order']);
	$SortField = stripslashes($_REQUEST['SortField']);
	$PageSize = stripslashes($_REQUEST['PageSize']);
	$pageSizeChangeEnabled = true;
	
	$libpos_ui->Set_Report_Date_Range_Cookies($StartDate, $EndDate);
	
	$returnString = $libpos_ui->Get_Item_Inventory_Change_Log_Table($ItemID, $StartDate, $EndDate, $Keyword, $PageNumber, $PageSize, $Order, $SortField,$LogType);
}

intranet_closedb();

echo $returnString;
?>