<?
// Using : 
/*
 * 2016-01-29 Carlos: Apply date range cookies. 
 * 2015-11-03 Carlos: Fixed to get the correct named db table parameters.  
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ManageInventory'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['Inventory'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);

$ItemID = $_REQUEST['ItemID'];
$FromIndex = $_REQUEST['FromIndex'];

# Preserve Table View of this page and the index page
/*
if ($FromIndex == 1)
{
	# index table
	$IndexInfoArr['Keyword'] = $_REQUEST['Keyword'];
	$IndexInfoArr['CategoryIDFilter'] = $_REQUEST['CategoryIDFilter'];
	$IndexInfoArr['pageNo'] = $_REQUEST['pageNo'];
	$IndexInfoArr['order'] = $_REQUEST['order'];
	$IndexInfoArr['field'] = $_REQUEST['field'];
	$IndexInfoArr['num_per_page'] = $_REQUEST['num_per_page'];
	
	# log page table
	$Keyword = '';
	$pageNo = '';
	$order = '';
	$field = '';
	$PageSize = '';
	
}
else
{ 
*/
	# index table
	$IndexInfoArr['Keyword'] = isset($_REQUEST['Keyword_Index'])? $_REQUEST['Keyword_Index'] : $_REQUEST['Keyword'];
	$IndexInfoArr['CategoryIDFilter'] = isset($_REQUEST['CategoryIDFilter_Index'])? $_REQUEST['CategoryIDFilter_Index'] : $_REQUEST['CategoryIDFilter'];
	$IndexInfoArr['pageNo'] = isset($_REQUEST['pageNo_Index'])? $_REQUEST['pageNo_Index'] : $_REQUEST['pageNo'];
	$IndexInfoArr['order'] = isset($_REQUEST['order_Index'])? $_REQUEST['order_Index'] : $_REQUEST['order'];
	$IndexInfoArr['field'] = isset($_REQUEST['field_Index'])? $_REQUEST['field_Index'] : $_REQUEST['field'];
	$IndexInfoArr['num_per_page'] = isset($_REQUEST['num_per_page_Index'])? $_REQUEST['num_per_page_Index'] : $_REQUEST['num_per_page'];
	$IndexInfoArr['LogType'] = $_REQUEST['LogType'];

	# log page table
	$Keyword = isset($_REQUEST['Keyword'])? $_REQUEST['Keyword'] : $_REQUEST['Keyword_Index'];
	$pageNo = isset($_REQUEST['pageNo'])? $_REQUEST['pageNo'] : $_REQUEST['pageNo_Index'];
	$order = isset($_REQUEST['order'])?  $_REQUEST['order'] : $_REQUEST['order_Index'];
	$field = isset($_REQUEST['field'])? $_REQUEST['field'] : $_REQUEST['field_Index'];
	//$PageSize = isset($_REQUEST['num_per_page_Index'])? $_REQUEST['num_per_page_Index'] : $_REQUEST['num_per_page'];
	$PageSize = isset($_REQUEST['numPerPage'])? $_REQUEST['numPerPage'] : $_REQUEST['num_per_page_Index'];
	$LogType = $_REQUEST['LogType'];
	
	$date_range_cookies = $libPOS_ui->Get_Report_Date_Range_Cookies();
	
	if($StartDate==""){
		$StartDate = $date_range_cookies['StartDate']!=''? $date_range_cookies['StartDate'] : date('Y-m-d',getStartOfAcademicYear($today_ts));
	}
	if($EndDate==""){
		$EndDate = $date_range_cookies['EndDate']!=''? $date_range_cookies['EndDate'] : date('Y-m-d',getEndOfAcademicYear($today_ts));
	}
	//$StartDate = $_REQUEST['StartDate'];
	//$EndDate = $_REQUEST['EndDate'];
//}

$pageSizeChangeEnabled = true;

echo $libPOS_ui->Get_Item_Inventory_Change_Log_UI($ItemID, $StartDate, $EndDate, $IndexInfoArr, $Keyword, $pageNo, $PageSize, $order, $field, $LogType);

?>


<script>

$(document).ready( function() {
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Report_Table();
	});
});


function js_Reload_Report_Table()
{
	jsKeyword = Trim($('input#Keyword').val());
	jsItemID = Trim($('input#ItemID').val());
	jsStartDate = Trim($('input#StartDate').val());
	jsEndDate = Trim($('input#EndDate').val());
	jsLogType = (document.getElementById('LogType').checked)? "1":"";
	
	js_pageNo = Trim($('input#pageNo').val());
	js_order = Trim($('input#order').val());
	js_field = Trim($('input#field').val());
	js_numPerPage = Trim($('input#numPerPage').val());
	
	if(compareDate(jsEndDate, jsStartDate) < 0) 
	{
		$('input#StartDate').focus();
		alert('<?=$Lang['ePOS']['jsWarningArr']['StartDateCannotGreaterThanEndDate']?>');
		return false;
	}
	
	$('div#LogTableDiv').html('');
	Block_Element('LogTableDiv');
	
	$('div#LogTableDiv').load(
		"ajax_reload.php", 
		{ 
			Action: 'Inventory_Log_Table',
			Keyword: jsKeyword,
			StartDate: jsStartDate,
			EndDate: jsEndDate,
			ItemID: jsItemID,
			PageNumber: js_pageNo,
			Order: js_order,
			SortField: js_field,
			PageSize: js_numPerPage,
			"LogType": jsLogType
		},
		function(ReturnData)
		{
			UnBlock_Element('LogTableDiv');
		}
	);
}

function js_Back_To_Inventory_Index()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'inventory.php?FromLog=1';
	objForm.target = '_self';
	objForm.submit();
}

function js_Go_Print()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'log_print.php';
	objForm.target = '_blank';
	objForm.submit();
	
	// restore submit settings
	objForm.action = 'log_view.php';
	objForm.target = '_self';
}

</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>