<?php
// Editing by 
/*
 * 2016-02-15 (Carlos): Created.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
//include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
//include_once($PATH_WRT_ROOT."includes/libpos_item.php");
//include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS = new libpos();
//$libPOS_ui = new libpos_ui();
$lexport = new libexporttext();

$name_field1 = getNameFieldByLang2("u.");
$name_field2 = getNameFieldByLang2("u2.");

$sql = "select 
		i.ItemName, 
		c.CategoryName,
		i.Barcode,
		case 
		when l.LogType=2 then '".$Lang['ePOS']['VoidItem']."' 
		when l.LogType=1 then '".$Lang['ePOS']['SoldItem']."'
		when l.LogType=3 then '".$Lang['ePOS']['IncreaseItemInventory']."'
		when l.LogType=4 then '".$Lang['ePOS']['DecreaseItemInventory']."' 
		end as LogType,
		l.Amount,
		l.CountAfter,
		t.TransactionTime,
		IF(l.LogType=1, t.Details, '') as SalesLocation, 
		l.Remark,
		IF(u.UserID IS NOT NULL,".$name_field1.",".$name_field2.") as LastModifyBy,
		IF(l.DateModify IS NOT NULL,l.DateModify,l.DateInput) as LastModifyDate 
		from POS_ITEM_LOG as l 
		inner join POS_ITEM as i ON i.ItemID=l.ItemID 
		left join POS_ITEM_CATEGORY as c ON c.CategoryID=i.CategoryID 
		left join PAYMENT_OVERALL_TRANSACTION_LOG as t ON t.LogID=l.RefLogID 
		left join INTRANET_USER as u on u.UserID=l.ModifyBy 
		left join INTRANET_USER as u2 ON u2.UserID=l.InputBy 
		where l.DateInput between '$StartDate 00:00:00' and '$EndDate 23:59:59' ";
if(isset($CategoryIDFilter) && $CategoryIDFilter != ''){
	$sql .= " and i.CategoryID='$CategoryIDFilter' ";
}
$sql .= " order by c.CategoryName,i.ItemName,l.DateInput";
$records = $libPOS->returnResultSet($sql);
$record_count = count($records);

$header = array($Lang['ePOS']['Category'],$Lang['ePOS']['Item'],$Lang['ePOS']['Barcode'],$Lang['ePOS']['LogType'],$Lang['ePOS']['ChangedAmount'],$Lang['ePOS']['InventoryAfter'],$Lang['ePOS']['TranscationTime'],$Lang['ePOS']['SalesLocation'],$Lang['ePOS']['Remark'],$Lang['General']['LastModifiedBy'],$Lang['General']['LastUpdatedTime']);
$rows = array();

for($i=0;$i<$record_count;$i++){
	
	$row = array();
	$row[] = $records[$i]['CategoryName'];
	$row[] = $records[$i]['ItemName'];
	$row[] = $records[$i]['Barcode'];
	$row[] = $records[$i]['LogType'];
	$row[] = $records[$i]['Amount'];
	$row[] = $records[$i]['CountAfter'];
	$row[] = $records[$i]['TransactionTime'];
	$row[] = $records[$i]['SalesLocation'];
	$row[] = $records[$i]['Remark'];
	$row[] = $records[$i]['LastModifyBy'];
	$row[] = $records[$i]['LastModifyDate'];
	
	$rows[] = $row;	
}

$title = $Lang['ePOS']['Inventory']." - ".$Lang['ePOS']['ChangeLog']." ($StartDate ".$Lang['General']['To']." $EndDate)";
$filename = $title.".csv";
$export_content = $lexport->GET_EXPORT_TXT($rows, $header,"","\r\n","",0,"11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>