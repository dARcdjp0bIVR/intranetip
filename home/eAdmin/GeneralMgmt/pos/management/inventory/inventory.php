<?
// Using : 
/*
Change log
2020-09-25 (Cameron): modify js_Update_Inventory(), disable submit button to avoid adding duplicate record [case #R197957]
2020-04-14 (Cameron): modify js_View_Log(), pass Keyword_Index and empty Keyword before form post so that Keyword won't be
    confused with that in change log
2016-02-16 (Carlos): Added [Export Change Log] button.
2015-03-02 (Carlos): Added print and export buttons
Date: 2010-09-28 [Kenneth Chung]
Detail: disabled function "js_Start_Get_Current_Inventory"/"js_Stop_Get_Current_Inventory", 
				discarded the real time inventory check approach. 
				Modified function "js_Update_Inventory" to check for inventory if is deduct
				Added function "Real_Update_Inventory" for submit after inventory check
*/
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ManageInventory'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['Inventory'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);

if ($FromLog == 1)
{
	$CategoryID = $_REQUEST['CategoryIDFilter_Index'];
	$Keyword = $_REQUEST['Keyword_Index'];
	$pageNo = $_REQUEST['pageNo_Index'];
	$order = $_REQUEST['order_Index'];
	$field = $_REQUEST['field_Index'];
	$PageSize = $_REQUEST['num_per_page_Index'];
}
else
{
	$CategoryID = $_REQUEST['CategoryIDFilter'];
	$Keyword = $_REQUEST['Keyword'];
	$pageNo = $_REQUEST['pageNo'];
	$order = $_REQUEST['order'];
	$field = $_REQUEST['field'];
	$PageSize = $_REQUEST['num_per_page'];
}

$pageSizeChangeEnabled = true;
echo $libPOS_ui->Get_Management_Inventory_UI($CategoryID, $Keyword, $pageNo, $PageSize, $order, $field);
?>


<script>
var isChecking = 0;

$(document).ready( function() {
	initThickBox();
	
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Inventory_Table();
	});
});


function js_Show_Edit_Invertory_Layer(jsItemID, jsForIncrease)
{
	$.post(
		"ajax_reload.php", 
		{ 
			Action: "Inventory_Edit_Layer",
			ItemID: jsItemID,
			ForIncrease: jsForIncrease
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			$('input#AdjustedBy').focus();
			
			//js_Start_Get_Current_Inventory(jsItemID);
			js_Add_KeyUp_Integer_Checking('AdjustedBy', 'AdjustedByWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['InventoryAdjustment']?>', '<?=$Lang['ePOS']['WarningArr']['NotInteger']['InventoryAdjustment']?>', '<?=$Lang['ePOS']['WarningArr']['GreaterThanZero']['InventoryAdjustment']?>');
			
			//$('#debugArea').html(ReturnData);
		}
	);
}

function js_Update_Inventory(jsItemID, jsForIncrease)
{
    $('#Btn_Submit').attr('disabled', 'disabled');      // disable submit button first to avoid duplicate action

	var jsAdjustedBy = Trim($('input#AdjustedBy').val());
	//var jsCurrentInventory = Trim($('input#CurrentInventory').val());
	
	// check enough inventory - for deduct inventory
	if (jsForIncrease == 0)
	{
		PostVar = {
			Action: "Inventory_Compare",
			ItemID: jsItemID,
			AdjustedBy: jsAdjustedBy
		};
		$.post("ajax_reload.php",PostVar,
			function(data) {
				if (data != "1") {
					$('div#AdjustedByWarningDiv').html(data).show();
				}
				else {
					Real_Update_Inventory(jsItemID, jsForIncrease);
				}
			}
			);
		
		return false;
	}
	else {
		Real_Update_Inventory(jsItemID, jsForIncrease);
	}
}

function Real_Update_Inventory(jsItemID, jsForIncrease) {
	var jsCostPrice = Trim($('input#CostPrice').val());
	var jsAdjustedBy = Trim($('input#AdjustedBy').val());
	var jsRemarks = Trim($('textarea#Remarks').val());
	
	if (js_Check_Integer_Num('AdjustedBy', 'AdjustedByWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['InventoryAdjustment']?>', '<?=$Lang['ePOS']['WarningArr']['NotInteger']['InventoryAdjustment']?>', '<?=$Lang['ePOS']['WarningArr']['GreaterThanZero']['InventoryAdjustment']?>'))
		return false;
		
	// Stop the iFrame to update Inventory display
	// js_Stop_Get_Current_Inventory();
		
	Block_Thickbox();
	
	$.post(
		"ajax_update.php", 
		{
			Action: "Update_Inventory",
			ItemID: jsItemID,
			AdjustedBy: jsAdjustedBy,
			CostPrice : jsCostPrice,
			Remarks: jsRemarks,
			ForIncrease: jsForIncrease
		},
		function (ReturnData)
		{
			UnBlock_Thickbox();
			
			if (ReturnData == '-1')
			{
				// not enough inventory for decrease
				Get_Return_Message('<?=$Lang['ePOS']['ReturnMessage']['Update']['Failed']['NotEnoughInventory']?>');
				
				// Resume the iFrame update inventory
				//js_Start_Get_Current_Inventory(jsItemID);
			}
			else
			{
				js_Hide_ThickBox();
				js_Reload_Inventory_Table();
				
				// Show system messages
				if (ReturnData == "1")
					Get_Return_Message('<?=$Lang['ePOS']['ReturnMessage']['Update']['Success']['Inventory']?>');
				else
					Get_Return_Message('<?=$Lang['ePOS']['ReturnMessage']['Update']['Failed']['Inventory']?>');
			}
		}
	);
}

function js_Close_Edit_Layer()
{
	// js_Stop_Get_Current_Inventory();
	js_Hide_ThickBox();
}

function js_Reload_Inventory_Table()
{
	jsKeyword = Trim($('input#Keyword').val());
	jsCategoryID = Trim($('select#CategoryIDFilter').val());
	
	js_pageNo = Trim($('input#pageNo').val());
	js_order = Trim($('input#order').val());
	js_field = Trim($('input#field').val());
	js_numPerPage = Trim($('input#numPerPage').val());
	
	$('div#InventoryTableDiv').html('');
	Block_Element('InventoryTableDiv');
	
	$('div#InventoryTableDiv').load(
		"ajax_reload.php", 
		{ 
			Action: 'Inventory_Table',
			Keyword: jsKeyword,
			CategoryID: jsCategoryID,
			PageNumber: js_pageNo,
			Order: js_order,
			SortField: js_field,
			PageSize: js_numPerPage
		},
		function(ReturnData)
		{
			initThickBox();
			UnBlock_Element('InventoryTableDiv');
		}
	);
}

function js_View_Log(jsItemID)
{
	var objForm = document.getElementById('form1');
	$('#Keyword_Index').val($('#Keyword').val());
	$('#Keyword').val('');	
	objForm.action = "log_view.php?FromIndex=1&ItemID=" + jsItemID;
	objForm.submit();
}

/*
function js_Start_Get_Current_Inventory(jsItemID)
{
	Create_IFrame("GetLiveInventoryIframe");
	var Obj = document.getElementById("GetLiveInventoryIframe");
	Obj.src = 'ajax_reload.php?Action=Item_Inventory_Count&ItemID=' + jsItemID; 
}

function js_Stop_Get_Current_Inventory()
{
	document.body.removeChild(document.getElementById("GetLiveInventoryIframe"));
	//var Obj = document.getElementById("GetLiveInventoryIframe");
	//Obj.src = '';
}
*/

function js_Add_KeyUp_Integer_Checking(InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg, WarnGreaterThenZeroMsg)
{
	$("input#"+InputID).keyup(function(){
		js_Check_Integer_Num(InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg, WarnGreaterThenZeroMsg);
	});
}

function js_Check_Integer_Num(InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg, WarnGreaterThenZeroMsg)
{
	var jsShowHideSpeed = "100";
	var InputValue = Trim($("input#"+InputID).val());
	
	if (InputValue == '')
	{
		$('div#' + WarningDivID).html(WarnBlankMsg).show(jsShowHideSpeed);
		return true;
	}
	else if (InputValue <= 0)
	{
		$('div#' + WarningDivID).html(WarnGreaterThenZeroMsg).show(jsShowHideSpeed);
		return true;
	}
	else
	{
		$('div#' + WarningDivID).html("").hide(jsShowHideSpeed);
		
		var isInt = (InputValue.toString().search(/^-?[0-9]+$/) == 0);
		if(isInt == true)
		{
			$('div#' + WarningDivID).html("").hide(jsShowHideSpeed);
			return false;
		}
		else
		{
			$('div#' + WarningDivID).html(WarnFloatMsg).show(jsShowHideSpeed);
			return true;
		}
	}
}

function validateDate(dateVal)
{
	var dateObj = new Date(dateVal);
	var year = '' + dateObj.getFullYear();
	var month = dateObj.getMonth()+1;
	var day = dateObj.getDate();
	month = month < 10? '0' + month : '' + month;
	day = day < 10? '0' + day : '' + day;
	
	var dateStr = year + '-' + month + '-' + day;
	if(dateVal != dateStr){
		return false;
	}
	return true;
}

function openPrintPage()
{
	var FormObj = document.getElementById('form1');
	var old_url = FormObj.action;
	var old_target = FormObj.target;
    FormObj.action="print.php";
    FormObj.target="_blank";
    FormObj.submit();
    FormObj.action = old_url;
    FormObj.target= old_target;
}

function exportPage()
{
  var obj = document.getElementById('form1');
  var old_url = obj.action;
  var old_target = obj.target;
  obj.action="export.php";
  obj.target="_blank";
  obj.submit();
  obj.action = old_url;
  obj.target = old_target;
}

function exportChangeLog()
{
  var start_date_val = $.trim(document.getElementById('StartDate').value);
  var end_date_val = $.trim(document.getElementById('EndDate').value);
  if(!validateDate(start_date_val)){
  	$('#DPWL-StartDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
  	return;
  }else{
  	$('#DPWL-StartDate').hide();
  }

  if(!validateDate(end_date_val)){
  	$('#DPWL-EndDate').html('<?=$Lang['General']['InvalidDateFormat']?>').show();
  	return;
  }else{
  	$('#DPWL-EndDate').hide();
  }

  if(start_date_val > end_date_val){
  	$('#DPWL-StartDate').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>').show();
  	return;
  }else{
  	$('#DPWL-StartDate').hide();
  }

  var obj = document.getElementById('form1');
  var old_url = obj.action;
  var old_target = obj.target;
  obj.action="export_change_log.php";
  obj.target="_blank";
  obj.submit();
  obj.action = old_url;
  obj.target = old_target;
}
</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>