<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage_ui.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$ClassID = $_REQUEST['ClassID'];

$lfm_ui = new form_class_manage_ui();

echo $lfm_ui->Get_Student_Selection("StudentID", array($ClassID),'','',1,1,0);

intranet_closedb();
?>