<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libpos_category.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_opendb();

$libpos_ui = new libpos_ui();

$ItemID = trim(urldecode($_REQUEST['ItemID']));
$ItemName = trim(urldecode($_REQUEST['ItemName']));
$FromDate = trim(urldecode($_REQUEST['FromDate']));
$ToDate = trim(urldecode($_REQUEST['ToDate']));

echo $libpos_ui->Get_POS_Item_Report_Detail($FromDate,$ToDate,$ItemName, $ItemID);

intranet_closedb();
?>
