<?php
// Editing by 
/*
 * 2017-03-17 (Carlos): Pass in $field and $order for sorting to follow UI.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$libpos_ui = new libpos_ui();

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

echo $libpos_ui->Get_POS_Item_Report_PrintLayout($FromDate,$ToDate,$keyword,$field,$order);

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>
