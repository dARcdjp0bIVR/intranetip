<?php
// Editing by 
/*
 * 2015-08-05 (Carlos): Add StartDate and EndDate to cookie.
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

//debug_r($_REQUEST);

$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$ClassID = $_REQUEST['ClassID'];
$Format = $_REQUEST['Format'];

$POS_UI = new libpos_ui();

if(isset($_REQUEST['StartDate']) && isset($_REQUEST['EndDate'])){
	$POS_UI->Set_Report_Date_Range_Cookies($_REQUEST['StartDate'], $_REQUEST['EndDate']);
}

if($Format=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
echo $POS_UI->Get_Health_Class_Report($StartDate,$EndDate,$ClassID,$Format);
if($Format=="PRINT") include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>