<?php
// kenneth chung

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ReportHealthClass'] = 1;	// Left Menu

$linterface = new interface_html();

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['HealthClassReport'], '', 0);

$MODULE_OBJ = $libpos_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

echo $libpos_ui->Get_Health_Class_Report_Layout();

?>
<script language="javascript">
function Get_Health_Class_Report() {
	var ClassIDs = Get_Selection_Value("ClassID","Array");
	
	if (ClassIDs.length > 0 && document.getElementById('DPWL-StartDate').innerHTML == "" && document.getElementById('DPWL-EndDate').innerHTML == "") {
		var PostVar = {
			"ClassID[]":ClassIDs,
			"StartDate":$('#StartDate').val(),
			"EndDate":$('#EndDate').val(),
			"Format":"WEB"
			};
		
		Block_Element("ReportLayer");
		$('#ReportLayer').load("ajax_get_class_health.php",PostVar,
			function (data) {
				if (data == "die") {
					window.top.location = "/";
				}
				
				UnBlock_Element("ReportLayer");
			});
	}
	else {
		if (ClassIDs.length == 0) {
			$('#ClassWarningLayer').html('<?=$Lang['ePOS']['SelectAtLeast1ClassWarning']?>');
		}
	}
}

function PrintPage(obj) {
	old_url = obj.action;
	old_method = obj.method;
	obj.action="ajax_get_class_health.php?Format=PRINT";
	obj.method = "POST";
	obj.target = "_blank";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
	obj.target = "";
}
				   
function ExportPage(obj) {
	old_url = obj.action;
	old_method = obj.method;
	obj.action="ajax_get_class_health.php?Format=EXPORT";
	obj.method = "POST";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}

function ShowPanel(thisObj, PanelID) {
	var $PanelObj = $('#'+PanelID);
	var $ClickOnObj = $(thisObj);
	var $pos = $ClickOnObj.position();
	var offsetY = $ClickOnObj.height();
	$PanelObj.css('position', 'absolute');
	$PanelObj.css('left',$pos.left+'px');
	$PanelObj.css('top',($pos.top+offsetY)+'px');
	$PanelObj.css('visibility','visible');
	$PanelObj.slideDown();
}
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
