<?php
// Editing by 
/*
 * 2015-08-05 (Carlos): Save FromDate and ToDate to cookies.
 */
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos_ui = new libpos_ui();
if(isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])){
	$libpos_ui->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

echo $libpos_ui->Get_POS_Sales_Report_PrintLayout($FromDate, $ToDate,$CategoryID, $DisplayInvolvedItem);

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
