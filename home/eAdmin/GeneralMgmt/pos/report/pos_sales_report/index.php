<?php
// kenneth chung
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libpos_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['ReportSales'] = 1;	// Left Menu

$linterface = new interface_html();

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['POSSalesReport'], '', 0);

$MODULE_OBJ = $libpos_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

echo $libpos_ui->Get_POS_Sales_Report_Layout();

?>
<script src="/templates/calendar/browserSniffer.js" type="text/javascript" language="javascript"></script>
<script language="javascript">
// Calendar callback. When a date is clicked on the calendar
// this function is called so you can do as you want with it
function calendarCallback(date, month, year)
{
	if (String(month).length == 1) {
		month = '0' + month;
	}
	
	if (String(date).length == 1) {
		date = '0' + date;
	}
	dateValue =year + '-' + month + '-' + date;
	document.forms['form1'].FromDate.value = dateValue;

}
function calendarCallback2(date, month, year)
{
	if (String(month).length == 1) {
		month = '0' + month;
	}
	
	if (String(date).length == 1) {
		date = '0' + date;
	}
	dateValue =year + '-' + month + '-' + date;
	document.forms['form1'].ToDate.value = dateValue;

}
function checkForm(formObj){
	if(formObj==null)return false;
	
	fromV = formObj.FromDate;
	toV= formObj.ToDate;
	if(!checkDate(fromV)){
	//formObj.FromDate.focus();
		return false;
	}
	else if(!checkDate(toV)){
		//formObj.ToDate.focus();
		return false;
	}
	return true;
}
function checkDate(obj){
	if(!check_date(obj,"<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>")) return false;
	return true;
}
function submitForm(obj){
	if(checkForm(obj))
	obj.submit();
}
function exportPage(obj,url){
	old_url = obj.action;
	old_method= obj.method;
	obj.action=url;
	obj.method = "get";
	obj.submit();
	obj.action = old_url;
	obj.method = old_method;
}

function GetXmlHttpObject()
{
  var xmlHttp=null;
  try
  {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  {
    // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  
  return xmlHttp;
}

function Show_Class_Layer(UserType) {
	if (UserType != 1) 
		document.getElementById("ClassLayer").style.display = '';
	else 
		document.getElementById("ClassLayer").style.display = 'none';
}

function Show_Student_Layer(ClassName) {
	if (Trim(ClassName) != "") {
		wordXmlHttp = GetXmlHttpObject();
	   
	  if (wordXmlHttp == null)
	  {
	    alert (errAjax);
	    return;
	  } 
	    
	  var url = 'ajax_get_student_selection.php';
	  var postContent = 'ClassName='+encodeURIComponent(ClassName);
		wordXmlHttp.onreadystatechange = function() {
			if (wordXmlHttp.readyState == 4) {
				ResponseText = Trim(wordXmlHttp.responseText);
			  document.getElementById("StudentLayer").innerHTML = ResponseText;
			  document.getElementById("StudentLayer").style.display = "";
			}
		};
	  wordXmlHttp.open("POST", url, true);
		wordXmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		wordXmlHttp.send(postContent);
	}
	else {
		document.getElementById("StudentLayer").innerHTML = "";
		document.getElementById("StudentLayer").style.display = "none";
	}
}

function CheckDisplayFormat(obj)
{
	var display = $("input[name='display']:checked").val();
	var fromDate = $("input[name='FromDate']").val();
	var toDate = $("input[name='ToDate']").val();
	
	var url = (display=='html')?"print.php":"export.php";
	url += "?FromDate='+fromDate+'&ToDate='+toDate";
	
	obj.action=url;
	submitForm(obj);
	
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
