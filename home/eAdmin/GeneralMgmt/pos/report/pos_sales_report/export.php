<?php
// Editing by 
/*
 * 2015-08-05 (Carlos): Save FromDate and ToDate to cookies.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$libpos_ui = new libpos_ui();
$lexport = new libexporttext();

if(isset($_REQUEST['FromDate']) && isset($_REQUEST['ToDate'])){
	$libpos_ui->Set_Report_Date_Range_Cookies($_REQUEST['FromDate'], $_REQUEST['ToDate']);
}

$ExportContent = $libpos_ui->Get_POS_Sales_Report_Export_Content($FromDate, $ToDate);

intranet_closedb();

$filename = "sales_report.csv";
$lexport->EXPORT_FILE($filename, $ExportContent);
?>
