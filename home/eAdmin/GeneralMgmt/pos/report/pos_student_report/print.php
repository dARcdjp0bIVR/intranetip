<?php
// Editing by 
/*
 * 2017-03-17 (Carlos): Pass in $field and $order for sorting data to follow UI.
 */
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_opendb();
$libpos_ui = new libpos_ui();

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));
$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

echo $libpos_ui->Get_POS_Student_Report_PrintLayout($FromDate, $ToDate, $Keyword,$ClassName,$field,$order);

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>
