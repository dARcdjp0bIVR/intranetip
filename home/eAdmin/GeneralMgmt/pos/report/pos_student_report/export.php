<?php
// Editing by 
/*
 * 2017-03-17 (Carlos): Pass in $field and $order for sorting data to follow UI.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_opendb();

$libpos_ui = new libpos_ui();
$lexport = new libexporttext();

$Keyword = trim(stripslashes(urldecode($_REQUEST['keyword'])));
$ClassName = trim(stripslashes(urldecode($_REQUEST['ClassName'])));

$ExportContent = $libpos_ui->Get_POS_Student_Report_Export_Content($FromDate, $ToDate, $Keyword,$ClassName,$field,$order);

intranet_closedb();

$filename = "pos_student_report.csv";
$lexport->EXPORT_FILE($filename, $ExportContent);
?>
