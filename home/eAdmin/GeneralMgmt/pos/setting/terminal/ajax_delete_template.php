<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$TemplateID = $_REQUEST['TemplateID'];

$POS = new libpos();

$POS->Start_Trans();
if ($POS->Delete_Terminal_Template($TemplateID)) {
	echo $Lang['ePOS']['TemplateDeleteSuccess'];
	$POS->Commit_Trans();
}
else {
	echo $Lang['ePOS']['TemplateDeleteFail'];
	$POS->RollBack_Trans();
}

intranet_closedb();
?>