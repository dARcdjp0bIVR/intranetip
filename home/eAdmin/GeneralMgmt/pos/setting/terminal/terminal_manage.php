<?php
// Editing by 
/*
 * 2017-08-18 (Carlos): Added js_Disable_Terminal(TerminalID).
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$linterface = new interface_html();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['Terminal'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['GeneralTerminalSetting'], 'terminal.php', 0);
$TAGS_OBJ[] = array($Lang['ePOS']['TerminalManagementSetting'], 'terminal_manage.php', 1);

# Get Return Message
$msg = $_REQUEST['msg'];
	
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START(urldecode($msg));

echo $libPOS_ui->Get_Teminal_Manage_Index();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<script>
// Ajax function 
{
function Remove_Template(TemplateID) {
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			if (performAction) {
				if (confirm('<?=$Lang['ePOS']['RemoveSiteWarning']?>')) {
					Block_Element('ManageTerminalListLayer');
					$.post(
						"ajax_delete_template.php",
						{ 
							"TemplateID": TemplateID
						},
						function(data)
						{
							if (data == "die") 
								window.top.location = '/';
							else {
								Get_Return_Message(data);
								Get_Terminal_List();
							}
						}
					);
				}
			}
		}
	});
}
	
function Process_Sync_Photo_Request(TerminalID,NeedConfirmMessage) {
	var NeedConfirmMessage = NeedConfirmMessage || "NO";
	var TerminalID = TerminalID || "";
	
	if (NeedConfirmMessage != "NO") {
		if (!confirm('<?=$Lang['ePOS']['ConfirmSyncAllPhoto']?>')) 
			return false;
	}
	
	Block_Thickbox();
	var PostVar = {
		"TerminalID": TerminalID 
	};
	
	$.post('ajax_sync_photo_to_terminal.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				window.top.tb_remove();
			}
		});
}

function Get_Sync_Photo_Form(TerminalID) {
	var PostVar = {
		"TerminalID": TerminalID
	};
	
	$.post('ajax_get_sync_photo_form.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
			}
		});
}

function Save_Terminal_Template_Selection() {
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			if (performAction) {
				Block_Thickbox();
				
				var PostVar = {
					"TerminalID": $('input#TerminalID').val(),
					"TemplateID": Get_Selection_Value("TemplateID","String")
				};
				
				$.post('ajax_save_terminal_template_selection.php',PostVar,
					function(data) {
						if (data == "die") 
							window.top.location = '/';
						else {
							Get_Terminal_List();
							window.top.tb_remove();
							Get_Return_Message(data);
						}
					});
			}
		}
	});
}
	
function Get_Site_Selection_Form(TerminalID) {
	var PostVar = {
		"TerminalID": TerminalID
	};
	
	$.post('ajax_get_template_selection_form.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
			}
		});
}

function Remove_Terminal(TerminalID) {
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			if (performAction) {
				if (confirm('<?=$Lang['ePOS']['RemoveTerminalSetting']?>')) {
					Block_Element('ManageTerminalListLayer');
					$.post(
						"ajax_delete_terminal.php",
						{ 
							"TerminalID": TerminalID
						},
						function(data)
						{
							if (data == "die") 
								window.top.location = '/';
							else {
								Get_Return_Message(data);
								Get_Terminal_List();
							}
						}
					);
				}
			}
		}
	});
}	

function Toggle_Category_Linkage(CategoryID) {
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			if (performAction) {
				var jsLinkID = 'ActiveStatus_' + CategoryID;
				var TargetRecordStatus;
				var jsCanUpdateStatus = 1;
				var jsEnabledCount = $('span#enabledCountSpan').html();
				var jsDisabledCount = $('span#disabledCountSpan').html();
				if ($('a#' + jsLinkID).attr('class') == 'active_item')
				{
					// active -> inactive
					$('a#' + jsLinkID).attr('class', 'inactive_item').attr('title', '<?=$Lang['General']['Disabled']?>');
					TargetRecordStatus = 0;
					
					jsEnabledCount--;
					jsDisabledCount++;
					
					$('span#enabledCountSpan').html(jsEnabledCount);
					$('span#disabledCountSpan').html(jsDisabledCount);
				}
				else
				{
					// inactive -> active
					if (jsEnabledCount < 10)
					{
						$('a#' + jsLinkID).attr('class', 'active_item').attr('title', '<?=$Lang['General']['Enabled']?>');
						TargetRecordStatus = 1;
						
						jsEnabledCount++;
						jsDisabledCount--;
						
						$('span#enabledCountSpan').html(jsEnabledCount);
						$('span#disabledCountSpan').html(jsDisabledCount);
					}
					else
					{
						jsCanUpdateStatus = 0;
						alert('<?=$Lang['ePOS']['jsWarningArr']['Max10EnableCategory']?>');
					}
				}
				
				if (jsCanUpdateStatus == 1)
				{
					$.post(
						"ajax_toggle_terminal_category_linkage.php", 
						{ 
							"CategoryID": CategoryID,
							"TargetRecordStatus": TargetRecordStatus,
							"TemplateID": $("input#TemplateID").val()
						},
						function(data)
						{
							if (data == "die") 
								window.top.location = '/';
							else {
								//Get_Terminal_Category_Linkage_Table();
							}
						}
					);
				}
			}
		}
	});
}

function Get_Template_Form(TemplateID) {
	var TemplateID = TemplateID || "";
	var PostVar = {
		"TemplateID":TemplateID
	};
	
	$.post('ajax_get_template_form.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
			}
		});
}

function Get_Terminal_List() {
	var Keyword = $('Input#Keyword').val();
	
	var PostVar = {
		"Keyword":encodeURIComponent($('Input#Keyword').val()) 
	};
	
	Block_Element('ManageTerminalListLayer');
	$('div#ManageTerminalListLayer').load('ajax_get_terminal_list.php',PostVar,
		function(data) {
			if (data == "die") 
					window.top.location = '/';
			else {
				Thick_Box_Init();
				UnBlock_Element('ManageTerminalListLayer');
			}
		});
}

function Check_Template_Name(TemplateName) {
	if (TemplateName != "") {
		var PostVar = {
				"TemplateID":$('input#TemplateID').val(),
				"TemplateName":encodeURIComponent(TemplateName)
			};
			
		$.post('ajax_check_template_name.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else if (data == "1") {
					$('tr#TemplateNameWarningRow').css('display','none');
					$('div#TemplateNameWarningLayer').html('');
				}
				else {
					$('tr#TemplateNameWarningRow').css('display','');
					$('div#TemplateNameWarningLayer').html('<?=$Lang['ePOS']['SiteNameEmptyDuplicateWarning']?>');
				}
			});
	}
	else {
		$('tr#TemplateNameWarningRow').css('display','');
		$('div#TemplateNameWarningLayer').html('<?=$Lang['ePOS']['SiteNameEmptyDuplicateWarning']?>');
	}
}

function Save_Template() {
	if ($('div#TemplateNameWarningLayer').html() == "") {
		Block_Thickbox();
		var PostVar = {
				"TemplateID":$('input#TemplateID').val(),
				"TemplateName":encodeURIComponent($('input#TemplateName').val())
			};
		
		$.post('ajax_save_template.php',PostVar,
			function (data) {
				if (data == "die") 
					window.top.location = '/';
				else {
					Get_Terminal_List();
					window.top.tb_remove();
					Get_Return_Message(data);
				}
			});
	}
}

function Clear_MAC_Address(TerminalID) {
	if (confirm('<?=$Lang['ePOS']['ClearClientTerminalLinkageConfirmWarning']?>')) {
		var PostVar = {
			"TerminalID": TerminalID
		};
		Block_Element('ManageTerminalListLayer');
		$.post('ajax_clear_terminal_linkage.php',PostVar,
			function(data) {
				if (data == "die") 
						window.top.location = '/';
				else {
					Get_Return_Message(data);
					Get_Terminal_List();
					UnBlock_Element('ManageTerminalListLayer');
				}
			});
	}
}

function Get_Category_Setting_Form(TemplateID) {
	var PostVar = {
		"TemplateID":TemplateID
	};
	
	$.post('ajax_get_category_linkage_form.php',PostVar,
		function(data){
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#TB_ajaxContent').html(data);
			}
		});
}

function js_Disable_Terminal(TerminalID)
{
	ObjID = "TerminalStatus_"+TerminalID;
	if ($('#'+ObjID).attr('class') == 'active_item')
	{
		$('#'+ObjID).attr({'class':'inactive_item','title':'<?=$Lang['General']['Disabled']?>'});
		TerminalStatus = 0;
	}
	else
	{
		$('#'+ObjID).attr({'class':'active_item','title':'<?=$Lang['General']['Enabled']?>'});
		TerminalStatus = 1;
	}
	
	$.post(
		"ajax_disable_terminal.php", 
		{ 
			TerminalID	   : TerminalID,
			TerminalStatus : TerminalStatus
		},
		function(ReturnData)
		{
			Get_Return_Message(ReturnData);
		}
	);
}
}

// dom function
{
function Check_Go_Search(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Terminal_List();
	else
		return false;
}
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init(){   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

Thick_Box_Init();
</script>