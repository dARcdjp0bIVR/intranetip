<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	echo 'die';
	intranet_closedb();
	exit();
}

$TemplateID = $_REQUEST['TemplateID'];
$TemplateName = stripslashes(trim(urldecode($_REQUEST['TemplateName'])));

$POS = new libpos();

if ($POS->Check_Terminal_Template_Name($TemplateName,$TemplateID)) 
	echo '1';
else 
	echo '0';

intranet_closedb();
?>