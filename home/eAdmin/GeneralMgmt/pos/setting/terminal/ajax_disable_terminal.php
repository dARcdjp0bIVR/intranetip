<?php

/*
 * 2017-08-17 (Carlos): Created for enable/disable terminal.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	//echo 'die';
	intranet_closedb();
	exit();
}

$TerminalID = IntegerSafe($_REQUEST['TerminalID']);
$TerminalStatus = $_REQUEST['TerminalStatus'];

$POS = new libpos();

$POS->Start_Trans();
$success = $POS->Update_Terminal_Info($TerminalID, array('TerminalStatus'=>$TerminalStatus));
if($success){
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
	$POS->Commit_Trans();
}else{
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	$POS->RollBack_Trans();
}

intranet_closedb();
?>