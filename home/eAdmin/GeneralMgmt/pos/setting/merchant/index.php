<?php
/*
 *  2020-08-20 Cameron
 *  - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment'] || !$sys_custom['ePayment']['MultiPaymentGateway']) {
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    intranet_closedb();
    exit();
}

$lpayment = new libpayment();
$GeneralSetting = new libgeneralsettings();
$AlipayMerchantAccountID = $GeneralSetting->Get_General_Setting('ePOS',array('"AlipayMerchantAccountID"'));

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['MerchantSetting'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['MerchantSetting'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);

?>

<script>
    $(document).ready(function(){
        $('#btnSubmit').click(function(){
            if ($('#MerchantAccountID').val() == '') {
                alert("<?php echo $Lang['ePOS']['SelectMerchantID'];?>");
            }
            else {
                $('#form1').submit();
            }
        })
    });

</script>

    <form name="form1" id="form1" action="index_update.php" method="post">
        <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
                <td colspan="2">
                    <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
                        <tr>
                            <td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
                                <?=$Lang['ePayment']['MerchantAccount']?>
                            </td>
                            <td class="tabletext" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <?php echo $Lang['ePayment']['Alipay'];?>
                                        </td>
                                        <td>
                                            <?php
                                            $merchants = $lpayment->getMerchantAccounts(array('RecordStatus'=>1,'ServiceProvider'=>'ALIPAY','order_by_accountname'=>true));
                                            $merchant_selection_data = array();
                                            for($i=0;$i<count($merchants);$i++){
                                                $merchant_selection_data[] = array($merchants[$i]['AccountID'],$merchants[$i]['AccountName']);
                                            }
                                            echo $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID" name="MerchantAccountID" ', $Lang['General']['EmptySymbol'], $AlipayMerchantAccountID);
                                            ?>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>

            <tr>
                <td align="center" colspan="2">
                    <?php echo $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", '','btnSubmit');?>
                    <?php echo $linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset");?>
                </td>
            </tr>

        </table>
    </form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
