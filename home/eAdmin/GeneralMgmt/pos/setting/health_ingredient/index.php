<?php
// using 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['HealthIngredientSetting'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['HealthIngredientSetting'], '', 0);

# Get Return Message
$msg = $_REQUEST['msg'];
$ReturnMsg = '';
if ($msg == 'update')
	$ReturnMsg = $Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateSuccess'];
else if ($msg == 'update_failed')
	$ReturnMsg = $Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateFailed'];
	
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);

$Include_Js_Css = $libPOS_ui->Include_JS_CSS();

//$Keyword = $_REQUEST['Keyword'];
$CategoryUI = $libPOS_ui->Get_Settings_HealthIngredient_UI($Keyword);

?>
<br />

<?= $CategoryUI ?>

<script>
// Thickbox functions
	$(document).ready( function() {
		initThickBox();
		Init_DND_Table();
	});

//Ajax UI Related Function
	function js_Show_HealthIngredient_Add_Edit_Layer(HealthIngredientID)
	{
		$.post(
			"ajax_reload.php", 
			{ 
				RecordType: "HealthIngredient_Add_Edit_Layer",
				HealthIngredientID: HealthIngredientID
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				// Code validation
				js_Add_KeyUp_Unique_Checking('IngredientCode', 'CodeWarningDiv', 'HealthIngredientCode', '<?=$Lang['ePOS']['WarningArr']['Blank']['HealthIngredientCode']?>','<?=$Lang['ePOS']['WarningArr']['Unique']['HealthIngredientCode']?>');
				js_Add_KeyUp_Unique_Checking('IngredientName', 'NameWarningDiv', 'HealthIngredientName', '<?=$Lang['ePOS']['WarningArr']['Blank']['HealthIngredientName']?>','<?=$Lang['ePOS']['WarningArr']['Unique']['HealthIngredientName']?>');
				js_Add_KeyUp_Blank_Checking('UnitName', 'UnitNameWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['UnitName']?>');
				js_Add_KeyUp_Float_Checking('StandardIntakePerDay', 'StandardIntakeWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['StandardIntakePerDay']?>','<?=$Lang['ePOS']['WarningArr']['NotFloat']['StandardIntakePerDay']?>');
			}
		);
	}
	
	function js_Reload_Main_Table()
	{
		$.post(
			"ajax_reload.php", 
			{ 
				RecordType: "HealthIngredient_Reload_Main_Table"
			},
			function(ReturnData)
			{
				$('#MainTableDiv').html(ReturnData);
				initThickBox();
				Init_DND_Table();
			}
		);
	}

// Ajax Update Functions
	function Update_HealthIngredient(HealthIngredientID)
	{
		//Check if all warning div are hidden
		if($("#CodeWarningDiv:visible").html()||$("#NameWarningDiv:visible").html()||$("#UnitNameWarningDiv:visible").html()||$("#StandardIntakeWarningDiv:visible").html())
			return false;
		
		var Action;
		if(HealthIngredientID=='')
			Action = 'Add';
		else
		 	Action = 'Edit';
		
		$.post(
			"ajax_update_healthingredient.php", 
			{ 
				HealthIngredientID: HealthIngredientID,
				Action: Action,
				HealthIngredientCode: $("#IngredientCode").val(),
				HealthIngredientName: $("#IngredientName").val(),
				UnitName			: $("#UnitName").val(),
				StandardIntakePerDay: $("#StandardIntakePerDay").val()
			},
			function(ReturnData)
			{
				Get_Return_Message(ReturnData);
				js_Hide_ThickBox();
				js_Reload_Main_Table();
			}
		);
	}

	function js_Disable_Ingredient(HealthIngredientID)
	{
		ObjID = "ActiveStatus_"+HealthIngredientID;
		if ($('#'+ObjID).attr('class') == 'active_item')
		{
			$('#'+ObjID).attr('class', 'inactive_item');
			TargetRecordStatus = 0;
		}
		else
		{
			$('#'+ObjID).attr('class', 'active_item');
			TargetRecordStatus = 1;
		}
		
		$.post(
			"ajax_update_healthingredient.php", 
			{ 
				Action				: "Disable",
				HealthIngredientID	: HealthIngredientID,
				RecordStatus		: TargetRecordStatus
			},
			function(ReturnData)
			{
				Get_Return_Message(ReturnData);
				js_Reload_Main_Table();
			}
		);
	}
	
	function js_Delete_HealthIngredient(HealthIngredientID)
	{
		if(confirm('<?=$Lang['ePOS']['jsWarningArr']['DeleteHealthIngredient']?>')){
			$.post(
				"ajax_update_healthingredient.php",
				{
					Action 			   : "Delete",
					HealthIngredientID : HealthIngredientID
				},
				function(ReturnData)
				{
					Get_Return_Message(ReturnData);
					js_Reload_Main_Table();
				}
			);
		}
	}
	
//Checking Function
	// Add KeyUp Listener for Checking
	function js_Add_KeyUp_Unique_Checking(InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
	{
		$("#"+InputID).keyup(function(){
			js_Check_Unique(InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg);
		});
	}
	
	function js_Add_KeyUp_Float_Checking(InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg)
	{
		$("#"+InputID).keyup(function(){
			js_Check_Float_Num(InputID, WarningDivID,WarnBlankMsg, WarnFloatMsg);
		});
	}

	// validation 
	function js_Check_Unique(InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
	{
		var jsShowHideSpeed = "100";
		var InputValue = $("#"+InputID).val();
		var HealthIngredientID = $("#HealthIngredientID").val();
		if(Trim(InputValue) == '')
		{
			$('#' + WarningDivID).html(WarnBlankMsg);
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).html("");
			$('#' + WarningDivID).hide(jsShowHideSpeed);
			
			$.post(
				"ajax_update_healthingredient.php", 
				{ 
					Action				: 'CheckUnique',
					DBFieldName			: DBFieldName ,
					Value 				: InputValue,
					HealthIngredientID	: HealthIngredientID
				},
				function(ReturnData)
				{
					if(ReturnData==1)
					{
						$('#' + WarningDivID).html(WarnUniqueMsg);
						$('#' + WarningDivID).show(jsShowHideSpeed);
					}
					else
					{
						$('#' + WarningDivID).html("");
						$('#' + WarningDivID).hide(jsShowHideSpeed);
					}
				}
			);
		}
	}
	
	function js_Check_Float_Num(InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg)
	{
		var jsShowHideSpeed = "100";
		var InputValue = $("#"+InputID).val();
		var HealthIngredientID = $("#HealthIngredientID").val();
		if(Trim(InputValue) == '')
		{
			$('#' + WarningDivID).html(WarnBlankMsg);
			$('#' + WarningDivID).show(jsShowHideSpeed);
		}
		else
		{
			$('#' + WarningDivID).html("");
			$('#' + WarningDivID).hide(jsShowHideSpeed);
			var patt=/^([+]?((([0-9]+(\.)?)|([0-9]*\.[0-9]+))([eE][+\-]?[0-9]+)?))$/;
			if(patt.test(InputValue)==true)
			{
				$('#' + WarningDivID).html("");
				$('#' + WarningDivID).hide(jsShowHideSpeed);
			}
			else
			{
				$('#' + WarningDivID).html(WarnFloatMsg);
				$('#' + WarningDivID).show(jsShowHideSpeed);
			}
		}
	}


// Table DND Function
	function Init_DND_Table() 
	{
		var objDom = "table.DragAndDrop"
		
		var JQueryObj1 = $(objDom);
		
		// form drag and drop
		JQueryObj1.tableDnD({
		    onDrop: function(table, row) {
		        var rows = table.tBodies[0].rows;
		        var RecordOrder = new Array();
		        for (var i=0; i<rows.length; i++) {
		           	if ($(rows[i]).attr("hid") != "")
		        		RecordOrder[i]= $(rows[i]).attr("hid");
		        }
		        RecordOrderStr = RecordOrder.join(",");
		        
		        $.post(
					"ajax_update_healthingredient.php", 
					{ 
						Action				: 'Update_DisplayOrder',
						RecordOrderStr		: RecordOrderStr
					},
					function(ReturnData)
					{
						Get_Return_Message(ReturnData);
						js_Reload_Main_Table();
					}
				);
		    },
			onDragStart: function(table, row) {
				//$(#debugArea).html("Started dragging row "+row.id);
			},
			dragHandle: "Dragable", 
			onDragClass: "move_selected"
		});
	}

//debug function
	function debug(str)
	{
		$("#debugArea").html(str);
	}
</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>