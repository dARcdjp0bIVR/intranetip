<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';

switch ($RecordType)
{
	case "HealthIngredient_Add_Edit_Layer":
		$libpos_ui = new libpos_ui();
		$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
		
		$returnString = $libpos_ui->Get_HealthIngredient_Add_Edit_Layer($HealthIngredientID );
	break;
	
	case "HealthIngredient_Reload_Main_Table":
		$libpos_ui = new libpos_ui();		
		$returnString = $libpos_ui->Get_Ingredient_List_Table();
	break;
}



intranet_closedb();

echo $returnString;
?>