<?php
// using 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");

intranet_opendb();

$libPOS = new libpos();

# Get data
$Action = stripslashes($_REQUEST['Action']);

switch($Action)
{
	case "Add":
		$Value['HealthIngredientCode'] = stripslashes($_REQUEST['HealthIngredientCode']);
		$Value['HealthIngredientName'] = stripslashes($_REQUEST['HealthIngredientName']);
		$Value['UnitName'] = stripslashes($_REQUEST['UnitName']);
		$Value['StandardIntakePerDay'] = stripslashes($_REQUEST['StandardIntakePerDay']);
		
		$success = $libPOS->InsertHealthIngredient($Value);
		if($success)
			$msg = $Lang['ePOS']['ReturnMessage']['Add']['Success']['HealthIngredient'];
		else
			$msg = $Lang['ePOS']['ReturnMessage']['Add']['Fail']['HealthIngredient'];
			
		echo $msg;
	break;
	
	case "Edit":
		$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
		$Value['HealthIngredientCode'] = stripslashes($_REQUEST['HealthIngredientCode']);
		$Value['HealthIngredientName'] = stripslashes($_REQUEST['HealthIngredientName']);
		$Value['UnitName'] = stripslashes($_REQUEST['UnitName']);
		$Value['StandardIntakePerDay'] = stripslashes($_REQUEST['StandardIntakePerDay']);
		
		$success = $libPOS->UpdateHealthIngredient($Value,$HealthIngredientID);
		if($success)
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Success']['HealthIngredient'];
		else
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Fail']['HealthIngredient'];
			
		echo $msg;
	break;
	
	case "Disable":
		$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
		$RecordStatus = stripslashes($_REQUEST['RecordStatus']);
		$Value['RecordStatus'] = $RecordStatus;
		
		$success = $libPOS->UpdateHealthIngredient($Value,$HealthIngredientID);
		if($success)
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Success']['HealthIngredient'];
		else
			$msg = $Lang['ePOS']['ReturnMessage']['Edit']['Fail']['HealthIngredient'];
			
		echo $msg;
	break;
	
	case "CheckUnique":
		$CheckField = stripslashes($_REQUEST['DBFieldName']);
		$Value = stripslashes($_REQUEST['Value']);
		$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
	
		$exist = $libPOS->IsHealthIngredientExist($CheckField,$Value,$HealthIngredientID);
		echo $exist;
	break;
	
	case "Update_DisplayOrder":
		$OrderStr = stripslashes($_REQUEST['RecordOrderStr']);
		
		$RecordArr = explode(",",$OrderStr);
		$Order = 1;
		for($i=0 ; $i<sizeof($RecordArr); $i++)
		{
			$success = $libPOS->UpdateHealthIngredient(array("DisplayOrder" => $Order),$RecordArr[$i]);
			if(!$success)
			{ 
				echo $Lang['ePOS']['ReturnMessage']['Edit']['Fail']['HealthIngredient'];
				exit;
			}	
			$Order++;
		}
		echo $Lang['ePOS']['ReturnMessage']['Edit']['Success']['HealthIngredient'];
	break;
	
	case "Delete":
		$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
		$success = $libPOS->DeleteHealthIngredient($HealthIngredientID);
		if($success)
			$msg = $Lang['ePOS']['ReturnMessage']['Delete']['Success']['HealthIngredient'];
		else
			$msg = $Lang['ePOS']['ReturnMessage']['Delete']['Fail']['HealthIngredient'];
		echo $msg;
	break;
	
	default: return 0;
	
}

intranet_closedb();
?>