<?php
// using 
/*
 * 2014-02-13 (Carlos): Added $Action "Delete_Category"
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_category.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "Insert_Edit")
{
	$DataArr = array();
	
	$DataArr['CategoryCode'] = stripslashes($_REQUEST['CategoryCode']);
	$DataArr['CategoryName'] = stripslashes($_REQUEST['CategoryName']);
	
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	$SuccessArr = array();
	
	### Insert / Update Basic Info
	$objCategory = new POS_Category($CategoryID);
	if ($CategoryID == '')
	{
		$PhotoCategoryID = $objCategory->Insert_Object($DataArr);
		$SuccessArr['UpdateInfo'] = ($PhotoCategoryID)? true : false;
	}
	else
	{
		$PhotoCategoryID = $CategoryID;
		$SuccessArr['UpdateInfo'] = $objCategory->Update_Object($DataArr);
	}
		
	if (in_array(false, $SuccessArr))
		$returnString = $PhotoCategoryID.',0';
	else
		$returnString = $PhotoCategoryID.',1';
}
else if ($Action == "Upload_Photo")
{
	### Upload Photos
	if ($CategoryPhoto != '')
	{
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$CategoryID = stripslashes($_REQUEST['CategoryID']);
		$objCategory = new POS_Category($CategoryID);
		$returnString = $objCategory->Upload_Photo($CategoryPhoto, $CategoryPhoto_name);
	}
}
else if ($Action == "Delete_Photo")
{
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	$objCategory = new POS_Category($CategoryID);
	$returnString = $objCategory->Delete_Photo();
}
else if ($Action == "Edit")
{
	$CategoryID = stripslashes(urldecode($_REQUEST['CategoryID']));
	$DBFieldName = stripslashes(urldecode($_REQUEST['DBFieldName']));
	$UpdateValue = stripslashes(urldecode($_REQUEST['UpdateValue']));
	
	$DataArr = array();
	$DataArr[$DBFieldName] = $UpdateValue;
	
	$objCategory = new POS_Category($CategoryID);
	$returnString = $objCategory->Update_Object($DataArr);
}
else if ($Action == "DisplayOrder")
{
	$DisplayOrderString = $_REQUEST['DisplayOrderString'];
	$libCategory = new POS_Category();
	$displayOrderArr = $libCategory->Get_Update_Display_Order_Arr($DisplayOrderString);
	$returnString = $libCategory->Update_DisplayOrder($displayOrderArr);
}else if($Action == "Delete_Category")
{
	include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
	
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	$objCategory = new POS_Category($CategoryID);
	$result = $objCategory->Permanent_Delete_Category();
	$returnString = $result ? "1":"0";	
}

intranet_closedb();

echo $returnString;
?>