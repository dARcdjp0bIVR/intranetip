<?php
// Editing by 
/*
 * 2015-06-30 (Carlos): Modified $RecordType == "Item_Table", added post data RecordStatus.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_opendb();

# Get data
$RecordType = stripslashes($_REQUEST['RecordType']);

$returnString = '';
if ($RecordType == "Category_Add_Edit_Layer")
{
	$libpos_ui = new libpos_ui();
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	
	$returnString = $libpos_ui->Get_Category_Add_Edit_Layer($CategoryID);
}
else if ($RecordType == "Category_Table")
{
	$libpos_ui = new libpos_ui();
	$Keyword = stripslashes($_REQUEST['Keyword']);
	$IncludeSearchItem = stripslashes($_REQUEST['IncludeSearchItem']);
	
	$returnString = $libpos_ui->Get_Settings_Category_Table($Keyword, $IncludeSearchItem);
}
elseif ($RecordType == "Item_Add_Edit_Layer")
{
	$libpos_ui = new libpos_ui();
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	
	$returnString = $libpos_ui->Get_Item_Add_Edit_Layer($ItemID, $CategoryID);
}
else if ($RecordType == "Item_Table")
{
	$libpos_ui = new libpos_ui();
	$Keyword = stripslashes($_REQUEST['Keyword']);
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	$RecordStatus = $_REQUEST['RecordStatus'];
	
	$returnString = $libpos_ui->Get_Settings_Item_Table($Keyword, $CategoryID, $RecordStatus);
}
else if ($RecordType == "Item_Health_Ingredient_Table")
{
	$libpos_ui = new libpos_ui();
	$ItemID = stripslashes($_REQUEST['ItemID']);
	
	$returnString = $libpos_ui->Get_Settings_Item_Health_Ingredient_Table($ItemID);
}
elseif ($RecordType == "Health_Ingredient_Add_Edit_Layer")
{
	$libpos_ui = new libpos_ui();
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$HealthIngredientID = stripslashes($_REQUEST['HealthIngredientID']);
	
	$returnString = $libpos_ui->Get_Item_Health_Ingredient_Add_Edit_Layer($ItemID, $HealthIngredientID);
}
intranet_closedb();

echo $returnString;
?>