<?php
// using 
/*
 * 2020-02-05 (Cameron): bypass image size checking (Check_Valid_Photo_Dimension) so that user can upload image in original size
 * 
 * 2014-02-11 (Carlos): $Action=ItemName - added $ItemCategoryID
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "CategoryCode")
{
	include_once($PATH_WRT_ROOT."includes/libpos_category.php");
	$libCategory = new POS_Category();
	$CategoryCode = stripslashes($_REQUEST['Value']);
	$ExcludeCategoryID = stripslashes($_REQUEST['CategoryID']);
	
	$returnString = $libCategory->Is_Code_Valid($CategoryCode, $ExcludeCategoryID);
}
else if ($Action == "CategoryName")
{
	include_once($PATH_WRT_ROOT."includes/libpos_category.php");
	$libCategory = new POS_Category();
	$CategoryName = stripslashes($_REQUEST['Value']);
	$ExcludeCategoryID = stripslashes($_REQUEST['CategoryID']);
	
	$returnString = $libCategory->Is_Name_Valid($CategoryName, $ExcludeCategoryID);
}
else if ($Action == "Barcode")
{
	include_once($PATH_WRT_ROOT."includes/libpos_item.php");
	$libItem = new POS_Item();
	$Barcode = stripslashes($_REQUEST['Value']);
	$ExcludeItemID = stripslashes($_REQUEST['ItemID']);
	
	$returnString = $libItem->Is_Barcode_Valid($Barcode, $ExcludeItemID);
	if($returnString == 1) {
		if($sys_custom['ePayment_PaymentItem_ItemCode']) {
			$lpayment = new libpayment();
			$payment_items = $lpayment->getPaymentItemsByItemCode($Barcode);
			if(!empty($payment_items)) {
				$returnString = 0;
			}
		}
	}
}
else if ($Action == "ItemName")
{
	include_once($PATH_WRT_ROOT."includes/libpos_item.php");
	$libItem = new POS_Item();
	$ItemName = stripslashes($_REQUEST['Value']);
	$ExcludeItemID = stripslashes($_REQUEST['ItemID']);
	$ItemCategoryID = $_REQUEST['ItemCategoryID'];
	
	$returnString = $libItem->Is_Name_Valid($ItemName, $ExcludeItemID, $ItemCategoryID);
}
else if ($Action == "Check_Photo_Dimension")
{
	if ($CategoryPhoto != '' || $ItemPhoto != '')
	{
		include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
		
		$ForCategory = stripslashes($_REQUEST['ForCategory']);
		$ForItem = stripslashes($_REQUEST['ForItem']);
		
		if ($ForCategory == 1)
		{
			include_once($PATH_WRT_ROOT."includes/libpos_category.php");
			$libPOS = new POS_Category();
			$TargetPhoto = $CategoryPhoto;
		}
		else if ($ForItem == 1)
		{
			include_once($PATH_WRT_ROOT."includes/libpos_item.php");
			$libPOS = new POS_Item();
			$TargetPhoto = $ItemPhoto;
		}
		//$isFileValid = $libPOS->Check_Valid_Photo_Dimension($TargetPhoto);
		$isFileValid = true;
		
		echo '<script type="text/javascript">'."\n";
			if ($isFileValid)
			{
				echo 'window.parent.document.getElementById("IsPhotoDimensionValid").value = 1;'."\n";
				echo 'window.parent.document.getElementById("FileUploadWarningDiv").innerHTML = "";'."\n";
				echo 'window.parent.document.getElementById("FileUploadWarningDiv").style.display = "none";'."\n";
				
			}
			else
			{
				echo 'window.parent.document.getElementById("IsPhotoDimensionValid").value = 0;'."\n";
				echo 'window.parent.document.getElementById("FileUploadWarningDiv").innerHTML = "'.$Lang['ePOS']['WarningArr']['SelectedPhotoTooLarge'].'";'."\n";
				echo 'window.parent.document.getElementById("FileUploadWarningDiv").style.display = "block";'."\n";
			}
			echo 'window.parent.document.getElementById("Btn_Submit").disabled = false;'."\n";
		echo '</script>'."\n";
	}
}

intranet_closedb();

echo $returnString;
?>