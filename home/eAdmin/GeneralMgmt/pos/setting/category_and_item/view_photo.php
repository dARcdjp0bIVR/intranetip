<?php
// Using: 
/*
 *  2020-02-06 Cameron
 *      - retrieve photo from original folder (OriginalPhotoPath) if the image exist, otherwise, use PhotoPath
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();

$MODULE_OBJ['title'] = $Lang['ePOS']['Photo'];
$libPOS_ui = new interface_html("popup.html");
$libPOS_ui->LAYOUT_START();

$CategoryID = $_REQUEST['CategoryID'];
$ItemID = $_REQUEST['ItemID'];

if ($CategoryID != '')
{
	# View Category Photo
	include_once($PATH_WRT_ROOT."includes/libpos_category.php");
	$Obj = new POS_Category($CategoryID);
}
else if ($ItemID != '')
{
	# View Item Photo
	include_once($PATH_WRT_ROOT."includes/libpos_item.php");
	$Obj = new POS_Item($ItemID);
}

if (file_exists($intranet_root.$Obj->OriginalPhotoPath)){
    $image = $Obj->OriginalPhotoPath;
}
else {
    $image = $Obj->PhotoPath;
}
$imgHTML = '<img src="'.$image.'">';

?>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$imgHTML?></td>
	</tr>
</table>
</br />

<?
intranet_closedb();
$libPOS_ui->LAYOUT_STOP();
?>