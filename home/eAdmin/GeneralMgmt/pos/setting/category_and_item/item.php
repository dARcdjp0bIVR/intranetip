<?php
// Editing by 
/*
 * 2020-04-14 (Cameron): pass argument $hidKeyword to Get_Settings_Item_UI()
 * 2019-07-16 (Cameron): add Description field to js_Update_Item_Info()
 * 2015-06-30 (Carlos): Modified js_Reload_Item_Table(), added post data RecordStatus.
 * 2014-02-11 (Carlos): Modified to allow same item name under different categories, added js_Delete_Item()
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpos.php");
include_once($PATH_WRT_ROOT."includes/libpos_ui.php");
include_once($PATH_WRT_ROOT."includes/libpos_item.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-ePOS"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['ePOS'] || !$_SESSION['SSV_PRIVILEGE']['plugin']['payment']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit();
}

$libPOS_ui = new libpos_ui();
$CurrentPageArr['ePOS'] = 1;	// Top Menu
$CurrentPage['Category&ItemSetting'] = 1;	// Left Menu

# tag information
$TAGS_OBJ[] = array($Lang['ePOS']['ItemSetting'], '', 0);
$MODULE_OBJ = $libPOS_ui->GET_MODULE_OBJ_ARR();
$linterface = new interface_html();
$linterface->LAYOUT_START($ReturnMsg);

$CategoryID = $_REQUEST['CategoryID'];
$Keyword = $_REQUEST['Keyword'];
$IncludeSearchItem = $_REQUEST['IncludeSearchItem'];
$hidKeyword = $_POST['hidKeyword'];

echo $libPOS_ui->Get_Settings_Item_UI($CategoryID, $Keyword, $IncludeSearchItem, $hidKeyword);

?>


<script>
var isChecking = 0;

$(document).ready( function() {
	initThickBox();
	js_Init_DND_Table();
	js_Update_Client_Connection_Text_Color();
	
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Item_Table();
	});
});

function js_Update_Item_Info(jsActionType, jsItemID)
{
	jsItemID = jsItemID || '';
	
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			// perform action
			if (performAction == true)
			{
				if (jsActionType == 'Insert_Edit_Item')
				{
					var CategoryID = Trim($('select#ItemCategoryID').val());
					var ItemName = Trim($('input#ItemName').val());
					var Barcode = Trim($('input#Barcode').val());
					var UnitPrice = Trim($('input#UnitPrice').val());
					var ItemPhoto = Trim($('input#ItemPhoto').val());
					
					// Name checking
					if (js_Is_Input_Blank('ItemName', 'NameWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['Name']?>'))
					{
						$("input#ItemName").focus();
						return false;
					}
					// Name checking
					if (js_Is_Input_Blank('Barcode', 'BarcodeWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['Code']?>'))
					{
						$("input#ItemName").focus();
						return false;
					}
					// Unit Price checking
					if (js_Check_Float_Num(jsItemID, 'UnitPrice', 'UnitPriceWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['UnitPrice']?>', '<?=$Lang['ePOS']['WarningArr']['NotFloat']['UnitPrice']?>', '<?=$Lang['ePOS']['WarningArr']['GreaterThanZero']['UnitPrice']?>'))
					{
						$("input#UnitPrice").focus();
						return false;
					}
					// Photo checking
					if (ItemPhoto != '')
					{
						// Photo Dimension not valid
						if ($('input#IsPhotoDimensionValid').val() == 0)
						{
							$("input#ItemPhoto").focus();
							return false;
						}
						
						var FileExt = Get_File_Ext(document.getElementById('ItemPhoto').value);
						var FileName = Get_File_Name(document.getElementById('ItemPhoto').value);
						
						if ((FileExt == "jpg" || FileExt == "jpeg" || FileExt == "gif" || FileExt == "png") && FileExt != false) {
							// do nth
						}
						else {
							$('div#FileUploadWarningDiv').html('<?=$Lang['ePOS']['WarningArr']['InvalidFile']?>');
							$('div#FileUploadWarningDiv').show('fast');
							return false;
						}
					}
					
					$.post(
						"ajax_validate.php", 
						{
							Action: "ItemName",
							Value: ItemName,
							ItemID: jsItemID,
							ItemCategoryID: ( $('#ItemCategoryID').length>0 ? $('#ItemCategoryID').val() : 0)
						},
						function(ReturnData)
						{
							if (ReturnData == 1)
							{
								// valid code
								$('div#CodeWarningDiv').hide();
								
								$.post(
									"ajax_validate.php", 
									{
										Action: "Barcode",
										Value: Barcode,
										ItemID: jsItemID
									},
									function(ReturnData)
									{
										if (ReturnData == 1)
										{
											// valid name
											$('#NameWarningDiv').hide();
											
											Block_Thickbox();

											// Use iFrame to Insert or Edit basic info is too slow => Reload of UI is faster than update info
											// So, the updated info cannot be display
											// => use ajax to update basic data, then use iFrame to upload photo
											$.post(
												"ajax_update_item.php", 
												{
													Action: "Insert_Edit",
													CategoryID: CategoryID,
													ItemName: ItemName,
													Barcode: Barcode,
													UnitPrice: UnitPrice,
                                                    Description: Trim($('#Description').val()),
													ItemID: jsItemID
												},
												function (ReturnData)
												{
													var TextArr = ReturnData.split(',');
													
													var PhotoItemID = TextArr[0];
													var SuccessStatus = TextArr[1];
													
													// Update Category Information
													Create_IFrame('FileUploadIframe');
														
													var Obj = document.getElementById("ItemForm");
													Obj.target = 'FileUploadIframe'; 
													Obj.encoding = "multipart/form-data";
													Obj.method = 'post'; 
													Obj.action = "ajax_update_item.php?Action=Upload_Photo&ItemID=" + PhotoItemID;
													Obj.submit();
													
													UnBlock_Thickbox();
													js_Hide_ThickBox();
													js_Reload_Item_Table();
													
													// Show system messages
													if (jsItemID == '')
														js_Show_System_Message("add", SuccessStatus);
													else
														js_Show_System_Message("edit", SuccessStatus);
												}
											);
										}
										else
										{
											// invalid code => show warning
											$('div#BarcodeWarningDiv').html('<?=$Lang['ePOS']['WarningArr']['Duplicated']['Code']?>').show();
											$('input#Barcode').focus();
											
											//$('#debugArea').html('aaa ' + ReturnData);
										}
									}
								);
							}
							else
							{
								// invalid name => show warning
								$('div#NameWarningDiv').html('<?=$Lang['ePOS']['WarningArr']['Duplicated']['Name']?>').show();
								$('input#ItemName').focus();
								
								//$('#debugArea').html('aaa ' + ReturnData);
								
							}
						}
					);
				}
				else if (jsActionType == 'Change_Active_Status')
				{
					var jsLinkID = 'ActiveStatus_' + jsItemID;
					var TargetRecordStatus;
					if ($('a#' + jsLinkID).attr('class') == 'active_item')
					{
						$('a#' + jsLinkID).attr('class', 'inactive_item');
						TargetRecordStatus = 0;
					}
					else
					{
						$('a#' + jsLinkID).attr('class', 'active_item');
						TargetRecordStatus = 1;
					}
					
					$.post(
						"ajax_update_item.php", 
						{ 
							Action: 'Edit',
							ItemID: jsItemID,
							DBFieldName: 'RecordStatus',
							UpdateValue: TargetRecordStatus
						},
						function(ReturnData)
						{
							//$('#debugArea').html(ReturnData);
						}
					);
				}
				else if (jsActionType == 'Delete_Photo')
				{
					if (confirm('<?=$Lang['ePOS']['jsWarningArr']['DeletePhoto']?>'))
					{
						$.post(
							"ajax_update_item.php", 
							{
								Action: 'Delete_Photo',
								ItemID: jsItemID
							},
							function(ReturnData)
							{
								js_Reload_Item_Table();
								//$('#debugArea').html(ReturnData);
							}
						);
					}
				}
			}	// End if (performAction == true)
		}	// End if (settings.url == jsClientConnectionAjaxFile && hasChecked == false)
	});	// End ajaxComplete
}

function js_Check_Photo_Dimension()
{
	// Disable Submit Btn
	$('input#Btn_Submit').attr('disabled', true);
	
	var Obj = document.getElementById("ItemForm");
	Obj.target = 'CheckDimensionFileUploadIframe'; 
	Obj.encoding = "multipart/form-data";
	Obj.method = 'post';
	Obj.action = "ajax_validate.php?Action=Check_Photo_Dimension&ForItem=1";
	Obj.submit();
}

function js_Show_Item_Add_Edit_Layer(jsItemID, jsCategoryID)
{
	jsItemID = jsItemID || '';
	jsCategoryID = jsCategoryID || Get_Selection_Value('CategoryIDFilter','String');
	
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			
			if (performAction == true)
			{
				$.post(
					"ajax_reload.php", 
					{ 
						RecordType: "Item_Add_Edit_Layer",
						ItemID: jsItemID,
						CategoryID: jsCategoryID
					},
					function(ReturnData)
					{
						$('#TB_ajaxContent').html(ReturnData);
						$('input#ItemName').focus();
						
						// barcode must be Upper Case character
						$('input#Barcode').keyup(function(e){
							$(this).val($(this).val().toUpperCase());
						});
						
						js_Add_KeyUp_Unique_Checking(jsItemID, 'ItemName', 'NameWarningDiv', 'ItemName', '<?=$Lang['ePOS']['WarningArr']['Blank']['Name']?>','<?=$Lang['ePOS']['WarningArr']['Duplicated']['Name']?>');
						js_Add_KeyUp_Unique_Checking(jsItemID, 'Barcode', 'BarcodeWarningDiv', 'Barcode', '<?=$Lang['ePOS']['WarningArr']['Blank']['Code']?>','<?=$Lang['ePOS']['WarningArr']['Duplicated']['Code']?>');
						js_Add_KeyUp_Float_Checking(jsItemID, 'UnitPrice', 'UnitPriceWarningDiv', '<?=$Lang['ePOS']['WarningArr']['Blank']['UnitPrice']?>', '<?=$Lang['ePOS']['WarningArr']['NotFloat']['UnitPrice']?>', '<?=$Lang['ePOS']['WarningArr']['GreaterThanZero']['UnitPrice']?>');
						
						$('input#ItemPhoto').change( function() {
							js_Check_Photo_Dimension();
						});
						
						//$('#debugArea').html(ReturnData);
					}
				);
			}
			else
			{
				js_Hide_ThickBox();
			}
		}
	});
}

function js_Reload_Item_Table()
{
	jsKeyword = Trim($('input#Keyword').val());
	jsCategoryID = Trim($('select#CategoryIDFilter').val());
	jsRecordStatus = $('select#RecordStatus').length>0? Trim($('select#RecordStatus').val()) : '';
	
	$('div#ItemDiv').html('');
	Block_Element('ItemDiv');
	
	$('div#ItemDiv').load(
		"ajax_reload.php", 
		{ 
			RecordType: 'Item_Table',
			Keyword: jsKeyword,
			CategoryID: jsCategoryID,
			RecordStatus: jsRecordStatus 
		},
		function(ReturnData)
		{
			initThickBox();
			js_Init_DND_Table();
			UnBlock_Element('ItemDiv');
		}
	);
}

function js_Init_DND_Table() {
	var target_tBodiesIndex = 0;
	var JQueryObj = $(".common_table_list");
	JQueryObj.tableDnD({
		onDrop: 
		function(table, DroppedRow) {
			
			js_Check_Client_Connection();
			
			var hasChecked = false;
			var performAction = false;
			
			// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
			$().ajaxComplete(function(e, xhr, settings) {
				if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
				{
					var jsAllowClientConnection = xhr.responseText;
					
					if (jsAllowClientConnection == 1)
					{
						if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
						{
							// update settings
							js_Change_Client_Connection();
							performAction = true;
						}
						else
						{
							// recover the table
							js_Reload_Item_Table();
						}
					}
					else
					{
						performAction = true;
					}
					hasChecked = true;
					
					
					if (performAction == true)
					{
						// Reordering
						if(table.id == "ItemContentTable")
						{
							var rows = table.tBodies[0].rows;
							var RecordOrder = "";
							for (var i=0; i<rows.length; i++) {
								if (rows[i].id != "")
								{
									var thisID = rows[i].id;
									var thisIDArr = thisID.split('_');
									var thisObjectID = thisIDArr[1];
									RecordOrder += thisObjectID + ",";
								}
							}
							//alert("RecordOrder = " + RecordOrder);
							
							$.post(
								"ajax_update_item.php", 
								{ 
									Action: "DisplayOrder",
									DisplayOrderString: RecordOrder
								},
								function(ReturnData)
								{
									//$('#debugArea').html(ReturnData);
								}
							);
						}
						else
						{
							return false;
						}
					}
				}
			});
		},
		onDragStart: function(table, DraggedRow) 
		{
			//$('#debugArea').html("Started dragging row "+row.id);
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}



// Add KeyUp Listener for Checking
function js_Add_KeyUp_Unique_Checking(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
{
	jsItemID = jsItemID || '';
	
	$("input#"+InputID).keyup(function(){
		js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg);
	});
	$("input#"+InputID).change(function(){
		js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg);
	});
}

// validation 
function js_Check_Unique(jsItemID, InputID, WarningDivID, DBFieldName, WarnBlankMsg, WarnUniqueMsg)
{
	jsItemID = jsItemID || '';
	
	var jsShowHideSpeed = "100";
	var InputValue = Trim($("input#" + InputID).val());
	
	if (InputID == 'Barcode')
	{
		// Special checking of Barcode format
		var BarcodeValid = js_Is_Barcode_Format_Valid(InputValue);
		
		if (BarcodeValid == 'GreaterThanMaxLength')
		{
			$('div#' + WarningDivID).html('<?=$Lang['ePOS']['WarningArr']['Barcode']['LengthTooLong']?>').show(jsShowHideSpeed);
			return;
		}
		else if (BarcodeValid == 'InvalidFormat')
		{
			$('div#' + WarningDivID).html('<?=$Lang['ePOS']['WarningArr']['Barcode']['FormatInvalid']?>').show(jsShowHideSpeed);
			return;
		}
	}
	
	if(InputValue == '')
	{
		$('div#' + WarningDivID).html(WarnBlankMsg);
		$('div#' + WarningDivID).show(jsShowHideSpeed);
	}
	else
	{
		$('div#' + WarningDivID).html("");
		$('div#' + WarningDivID).hide(jsShowHideSpeed);
		
		$.post(
			"ajax_validate.php", 
			{ 
				Action: DBFieldName,
				Value: InputValue,
				ItemID: jsItemID,
				ItemCategoryID: ($('#ItemCategoryID').length>0? $('#ItemCategoryID').val(): 0)   
			},
			function(isVaild)
			{
				if(isVaild != 1)
					$('div#' + WarningDivID).html(WarnUniqueMsg).show(jsShowHideSpeed);
				else
					$('div#' + WarningDivID).html("").hide(jsShowHideSpeed);
			}
		);
	}
}

function js_Add_KeyUp_Float_Checking(jsItemID, InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg, WarnGreaterThenZeroMsg)
{
	jsItemID = jsItemID || '';
	WarnGreaterThenZeroMsg = WarnGreaterThenZeroMsg || '';
	
	$("input#"+InputID).keyup(function(){
		js_Check_Float_Num(jsItemID, InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg, WarnGreaterThenZeroMsg);
	});
}

function js_Check_Float_Num(jsItemID, InputID, WarningDivID, WarnBlankMsg, WarnFloatMsg, WarnGreaterThenZeroMsg)
{
	jsItemID = jsItemID || '';
	
	var jsShowHideSpeed = "100";
	var InputValue = Trim($("input#"+InputID).val());
	
	if (InputValue == '')
	{
		$('div#' + WarningDivID).html(WarnBlankMsg).show(jsShowHideSpeed);
		return true;
	}
	else if (InputValue <= 0)
	{
		$('div#' + WarningDivID).html(WarnGreaterThenZeroMsg).show(jsShowHideSpeed);
		return true;
	}
	else
	{
		$('div#' + WarningDivID).html("").hide(jsShowHideSpeed);
		var patt=/^([+]?((([0-9]+(\.)?)|([0-9]*\.[0-9]+))([eE][+\-]?[0-9]+)?))$/;
		if(patt.test(InputValue) == true)
		{
			$('div#' + WarningDivID).html("").hide(jsShowHideSpeed);
			return false;
		}
		else
		{
			$('div#' + WarningDivID).html(WarnFloatMsg).show(jsShowHideSpeed);
			return true;
		}
	}
}
	
	
function js_Go_Category_Page()
{
	var objForm = document.getElementById('ItemMainForm');
	objForm.action = 'category.php';
	objForm.submit();
}



function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "add":
			returnMessage = (Status)? '<?=$Lang['ePOS']['ReturnMessage']['Add']['Success']['Item']?>' : '<?=$Lang['ePOS']['ReturnMessage']['Add']['Failed']['Item']?>';
			break;
		case "edit":
			returnMessage = (Status)? '<?=$Lang['ePOS']['ReturnMessage']['Edit']['Success']['Item']?>' : '<?=$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['Item']?>';
			break;
	}
	
	Get_Return_Message(returnMessage);
}

function js_Delete_Item(ItemID)
{
	js_Check_Client_Connection();
	
	var hasChecked = false;
	var performAction = false;
	// jsClientConnectionAjaxFile is defined in /templates/2009a/js/pos.js to ensure the ajax response is from the checking function
	$().ajaxComplete(function(e, xhr, settings) {
		if (settings.url == jsClientConnectionAjaxFile && hasChecked == false) 
		{
			var jsAllowClientConnection = xhr.responseText;
			
			if (jsAllowClientConnection == 1)
			{
				if (confirm('<?=$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings']?>'))
				{
					// update settings
					js_Change_Client_Connection();
					performAction = true;
				}
			}
			else
			{
				performAction = true;
			}
			hasChecked = true;
			
			if (performAction == true)
			{
				if(confirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>')){
					Block_Element('ItemDiv');
					$.post(
						'ajax_update_item.php',
						{
							'Action': 'Delete_Item',
							'ItemID': ItemID 
						},
						function(result){
							if(result == '1'){
								Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>');
							}else{
								Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>');
							}
							js_Reload_Item_Table();
						}
					);
				}
			}
		}
	});
}
</script>

<br /> 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>