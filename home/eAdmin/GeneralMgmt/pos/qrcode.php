<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/phpqrcode/qrlib.php");

QRcode::png($encode, 
			$outfile = false,
		  	$level = QR_ECLEVEL_L,
		  	$size = 8,
		  	$margin = 2,
		  	$saveandprint = false);