<?php
# using: 

################### Change Log 
# 
#	Date:	2016-03-31	Bill	[2016-0331-0959-33206]
#			Fixed: Export Ordering Answer for Anonymous Survey
#
#	Date:	2015-12-16	Pun 
#			Add number of answer function for ordering
# 
#	Date:	2013-06-05	YatWoon
#			Fixed: trim tab	[Case#2013-0402-0936-10156]
#
#	Date:	2010-09-10	YatWoon
#			trim line break for question
#
##############################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);
$SurveyID = IntegerSafe($SurveyID);
$lsurvey = new libsurvey($SurveyID);

$exportColumn = array();
$ExportArr = array();
$thisRow = array();

     $queString = $lsurvey->Question;
     $lsurvey->splitQuestion($queString);
     $question_cnt = sizeof($lsurvey->question_array);
     $answers = $lsurvey->getRawDataTable(); 
     
     if ($lsurvey->RecordType != 1)
     {
         $exportColumn[] = $Lang['eSurvey']['StudentName'];
         $exportColumn[] = $Lang['eSurvey']['ClassName'];
         $exportColumn[] = $Lang['eSurvey']['ClassNumber'];
         for ($j=0; $j<$question_cnt; $j++)
         {
	     	$data = str_replace("\r\n"," ",intranet_undo_htmlspecialchars($lsurvey->question_array[$j][1]));
	     	$data = str_replace("\t"," ",$data);
         	//$exportColumn[] = str_replace("\r\n"," ",intranet_undo_htmlspecialchars($lsurvey->question_array[$j][1]));
         	$exportColumn[] = $data;
         	if($lsurvey->question_array[$j][0]==7){//ordering
         		$numberOfAnswer = $lsurvey->question_array[$j][3];
         		for ($k=0; $k<$numberOfAnswer-1; $k++){	
         			$exportColumn[] = "";
         		}
         	}
         }
		$exportColumn[] = $Lang['eSurvey']['SubmissionDate'];
     }
     else
     {
         for ($j=0; $j<$question_cnt; $j++)
         {
			$data = str_replace("\r\n"," ",intranet_undo_htmlspecialchars($lsurvey->question_array[$j][1]));
			$data = str_replace("\t"," ",$data);
			//$exportColumn[] = str_replace("\r\n"," ",intranet_undo_htmlspecialchars($lsurvey->question_array[$j][1]));
			$exportColumn[] = $data;
         	if($lsurvey->question_array[$j][0]==7){//ordering
         		$numberOfAnswer = $lsurvey->question_array[$j][3];
         		for ($k=0; $k<$numberOfAnswer-1; $k++){	
         			$exportColumn[] = "";
         		}
         	}
         }
     }
	for ($i=0; $i<sizeof($answers); $i++)
	{
		$thisRow = array();
		if ($sys_custom['SurveySaveWithDraft']) {
			list($id,$name,$class,$classnumber,$parsed,$fillTime,$isDraft) = $answers[$i];
		} else {
			list($id,$name,$class,$classnumber,$parsed,$fillTime) = $answers[$i];
		}
		$notIsDraft = true;
		if ($sys_custom['SurveySaveWithDraft'] && $isDraft == "1") {
			$notIsDraft= false;
		}
		if ($notIsDraft) {
			if ($lsurvey->RecordType != 1) {
				$thisRow[] = $name;
				$thisRow[] = $class;
				$thisRow[] = $classnumber;
				
				for ($j=0; $j<$question_cnt; $j++)
				{
					if($lsurvey->question_array[$j][0]==7)
					{
		              		$numberOfAnswer = $lsurvey->question_array[$j][3];
		              		$__orderingOptionAry = explode(",",$parsed[$j]);
		              		for ($k=0; $k<$numberOfAnswer; $k++){	
			              		$___orderOption = $__orderingOptionAry[$k];
			         			$thisRow[] = $lsurvey->question_array[$j][2][$___orderOption];
			         		}
		              	}
		              	else{
		                   $thisRow[] = $parsed[$j];
		              	}
	      		  }
				  $thisRow[] = $fillTime;
	          }
	          else
	          {
	              $delim = "";
	              for ($j=0; $j<$question_cnt; $j++)
	              {
	              		// [2016-0331-0959-33206]
		              	if($lsurvey->question_array[$j][0]==7)
		              	{
		              		$numberOfAnswer = $lsurvey->question_array[$j][3];
		              		$__orderingOptionAry = explode(",",$parsed[$j]);
		              		for ($k=0; $k<$numberOfAnswer; $k++){	
		              			$___orderOption = $__orderingOptionAry[$k];
		         				$thisRow[] = $lsurvey->question_array[$j][2][$___orderOption];
		         			}
		              	}
		              	else{
		                   $thisRow[] = $parsed[$j];
		              	}
	              }
	          }
	          $ExportArr[] = $thisRow;
		}
	}
	$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0);
	intranet_closedb();
	$filename = "surveyresult_$SurveyID.csv";
	$lexport->EXPORT_FILE($filename, $export_content);
?>