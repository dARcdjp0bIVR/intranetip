<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['eSurvey']['ViewSurveyResult'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);

$lsurvey = new libsurvey($SurveyID);

$targetGroups = $lsurvey->returnTargetGroups();
if (sizeof($targetGroups)==0)
{
	$target = "$i_general_WholeSchool";
}
else
{
	$target = implode(", ",$targetGroups);
}

?>

<br />

<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td align=left>
		<ol>
			<li><a class=functionlink_new href=result.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_Overall?></a></li>
			
			<? if (sizeof($targetGroups)!=0) { ?>
				<li><a class=functionlink_new href=list_group.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perGroup?></a></li>
			<? } ?>
			
			<li><a class=functionlink_new href=list_id.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perIdentity?></a></li>
			
			<li><a class=functionlink_new href=list_class.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perClass?></a></li>
			
			<? if ($lsurvey->RecordType != 1) { ?>
				<li><a class=functionlink_new href=list_user.php?SurveyID=<?=$SurveyID?>><?=$i_Survey_perUser?></a></li>
			<? } ?>
		</ol>
	</td>
</tr>
</table>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>