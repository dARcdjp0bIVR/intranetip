<?php
// Modifying by: 

##################################################
#	
#	Date:	2017-02-23 Anna	[2016-0928-1637-22240]
#			Create File		($sys_custom['SurveyImportFunction'])
#
##################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$limport = new libimporttext();
$lf = new libfilesystem();

$linterface = new interface_html();
$lsurvey = new libsurvey();

# Page Title
$CurrentPage = "PageSurveyList";
$CurrentPageArr['eAdmineSurvey'] = 1;

# Tag Information
$TAGS_OBJ[] = array($Lang['eSurvey']['SurveyList']);

# Left Menu
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

# Navigation
$PAGE_NAVIGATION[] = array($Lang['eSurvey']['SurveyList'], "javascript:history.back();");
$PAGE_NAVIGATION[] = array($Lang['eSurvey']['NewSurvey']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
$STEPS_OBJ[] = array($i_general_complete, 1);

$importMode = $_POST['importMode'];

// Get csv data from temp table
$sql = "select * from temp_survey_import where UserID=". $_SESSION["UserID"];
$data = $lsurvey->returnArray($sql);

// Import table 
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"common_table_list_v30 view_table_list_v30\">";
$x .= "<tr>";
$x .= "<th class=\"field_title\">#</td>";
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['Title']."</td>";
$x .= "<th class=\"field_title\">".$Lang['SysMgr']['SchoolNews']['Description']."</td>";
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['StartDate']."</td>";
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['EndDate']."</td>";
$x .= "<th class=\"field_title\">".$Lang['SysMgr']['SchoolNews']['StatusPublish'].'/'.$i_status_pending."</td>";
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['DisplayQuestionNumber'].'(Y/N)'."</td>";
$x .= "<th class=\"field_title\">".$i_Survey_AllRequire2Fill.'(Y/N)'."</td>";
$x .= "<th class=\"field_title\">".$i_Survey_Anonymous.'(Y/N)'."</td>";
if($importMode == 'new'){
	$x .= "<th class=\"field_title\">".$Lang['eSurvey']['Target'].'('.$Lang['eSurvey']['WholeSchool'].'/'.$i_admintitle_group.')'."</td>";
}
$x .= "</tr>";

// loop data
for($a=0; $a<sizeof($data); $a++)
{
	$dataAry = array();
	if($importMode == 'new'){
		$dataAry['Title'] = $data[$a]['Title'];
	}
	$dataAry['Description'] = $data[$a]['Description'];
	if($importMode == 'new'){
		$dataAry['Question'] = $data[$a]['Question'];
		$dataAry['DateStart'] = $data[$a]['DateStart'];
		$dataAry['DateEnd'] = $data[$a]['DateEnd'];
		$dataAry['PosterID'] = $data[$a]['PosterID'];
		$dataAry['PosterName'] = $data[$a]['PosterName'];
	}
	$dataAry['RecordStatus'] = $data[$a]['RecordStatus'];
	$dataAry['DisplayQuestionNumber'] = $data[$a]['DisplayQuestionNumber'];
	$dataAry['AllFieldsReq'] = $data[$a]['AllFieldsReq'];
	$dataAry['RecordType'] = $data[$a]['RecordType'];
	
	$GroupID = $data[$a]['GroupiD']=='0' ? "":$data[$a]['GroupiD'];
	
	# Insert / Update Survey
	$fields = array();
	$values = array();
	if($importMode == 'new') {
		foreach($dataAry as $fields[]=>$values[]);
		$sql = "INSERT INTO INTRANET_SURVEY (";
		foreach($fields as $field)	$sql .= $field .", ";
		$sql .= "DateInput, DateModified) values (";
		foreach($values as $value)	$sql .= "\"". addslashes($value) ."\", ";
		$sql .= "now(), now())";
		
		$lsurvey->db_db_query($sql);
		$SurveyID = $lsurvey->db_insert_id();
	}
	else if($importMode == 'update') {
		$SurveyID = $data[$a]['SurveyID'];
		$fieldname = "Description = '".addslashes($dataAry['Description'])."', ";
		$fieldname .= "RecordStatus = '".$dataAry['RecordStatus']."', ";		
		$fieldname .= "DisplayQuestionNumber = '".$dataAry['DisplayQuestionNumber']."', ";
		$fieldname .= "AllFieldsReq = '".$dataAry['AllFieldsReq']."',";
		$fieldname .= "RecordType = '".$dataAry['RecordType']."', ";
		$fieldname .= "DateModified = now()";
		
		$sql = "UPDATE INTRANET_SURVEY SET $fieldname WHERE SurveyID = '$SurveyID'";
		$lsurvey->db_db_query($sql);
	}
	
	# Insert related Group
	if ($GroupID > 0 && $SurveyID)
	{
		$group_survey_field = "";
		$delimiter = "";
		
		$group_survey_field .= "$delimiter(".$GroupID.", $SurveyID)";
		$delimiter = ",";
	
		$sql = "INSERT INTO INTRANET_GROUPSURVEY (GroupID, SurveyID) VALUES $group_survey_field";
		$lsurvey->db_db_query($sql);
	}
	
	# Display Content
	if($importMode == 'update') {
		if($data[$a]['DateStart'] == "0000-00-00 00:00:00")
			$data[$a]['DateStart'] = "";
		if($data[$a]['DateEnd'] == "0000-00-00 00:00:00")
			$data[$a]['DateEnd'] = "";
	}
	$data[$a]['DateStart'] = $data[$a]['DateStart']!=""? date("Y-m-d", strtotime($data[$a]['DateStart'])) : "";
	$data[$a]['DateEnd'] = $data[$a]['DateEnd']!=""? date("Y-m-d", strtotime($data[$a]['DateEnd'])) : "";
	$RecordStatus  = $data[$a]['RecordStatus']=="1"? $Lang['SysMgr']['SchoolNews']['StatusPublish'] :$i_status_pending ;
	$DisplayNum = $data[$a]['DisplayQuestionNumber'] =="1"? "Y": "N";
	$AllFieldsReq = $data[$a]['AllFieldsReq'] =="1"? "Y": "N";
	$RecordType = $data[$a]['RecordType'] =="1"? "Y": "N";
	
	$x .= "<tr>";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">".$data[$a]['Title']."</td>";
	$x .= "<td class=\"tabletext\">".$data[$a]['Description']."</td>";
	$x .= "<td class=\"tabletext\">".$data[$a]['DateStart']."</td>";
	$x .= "<td class=\"tabletext\">".$data[$a]['DateEnd']."</td>";
	$x .= "<td class=\"tabletext\">".$RecordStatus."</td>";
	$x .= "<td class=\"tabletext\">".$DisplayNum."</td>";
	$x .= "<td class=\"tabletext\">".$AllFieldsReq."</td>";
	$x .= "<td class=\"tabletext\">".$RecordType."</td>";
	if($importMode == 'new'){
		$x .= "<td class=\"tabletext\">". $data[$a]['GroupName'] ."</td>";
	}
	$x .= "</tr>";
}
$x .= "</table>";

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php'");

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr>
					<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<tr>
					<td align="center" colspan="2">
					<?=$import_button?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>