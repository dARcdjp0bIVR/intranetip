<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['eSurvey']['ViewSurveyResult'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$SurveyID = (is_array($SurveyID)? $SurveyID[0]:$SurveyID);
$SurveyID = IntegerSafe($SurveyID);
$lsurvey = new libsurvey($SurveyID);

$targetGroups = $lsurvey->returnTargetGroupsArray();
if (sizeof($targetGroups)==0)
{
	header("Location: result_type.php?SurveyID=$SurveyID");
	exit();
}
else
{
	$x = "<ol>\n";
	for ($i=0; $i<sizeof($targetGroups); $i++)
	{
		list($id,$name) = $targetGroups[$i];
		$x .= "<li><a class=list_link href=result_group.php?SurveyID=$SurveyID&GroupID=$id>$name</a></li>\n";
	}
	$x .= "</ol>\n";
}

?>

<br />
<table width="95%" border="0" cellspacing="0" cellpadding="5">

<tr>
	<td align=left>
		<?=$x?>
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
		<hr>
		<?=$linterface->GET_BTN($button_back, "button", "history.back();") ?>
	</td>
</tr>
</table>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>