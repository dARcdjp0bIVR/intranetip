<?
# Using: 

###################################
#
#	Date:	2017-11-02	Pun
#			- added $hideTitle for L&T cust
#
#	Date:	2016-12-09	Bill	[2016-1208-1705-56207]
#			- fixed display "<br>" in question when load reply slip with next line
#
#	Date:	2016-05-04	Bill	[2016-0428-1355-16207]
#			- copy from eNotice
#			- return selected eSurvey template content 
#
###################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lform = new libform();
$lsurvey = new libsurvey();

// get data from existing template
if ($templateID != -1)
{
    $lsurvey->retrieveRecord($templateID);
    
    $Title = $lsurvey->Title;
	$Description = $lsurvey->Description;
	$Question = stripslashes($lform->getConvertedString($lsurvey->Question));
	// [2016-1208-1705-56207] convert back to "\n", fixed display "<br>" instead of next line in question (when reply slip is loading from templates) 
	$Question = str_replace("<br>", "\n", $Question);
	//$DisplayQuestionNumber = $lsurvey->DisplayQuestionNumber;
}

// Date Setting
$now = time();
$start = date('Y-m-d',$now);
$end = date('Y-m-d',$now);
		
// special_announce_public_allowed
//$lo = new libgrouping();

// Build layout
$x = "<table width='90%' border='0' cellpadding='5' cellspacing='0' align='center'>
		<!-- Start Date -->
		<tr>
			<td class='tabletext formfieldtitle' nowrap='nowrap' valign='top'>".$Lang['eSurvey']['StartDate']." <span class='tabletextrequire'>*</span></td>
			<td align='left' valign='top'>
				".$linterface->GET_DATE_PICKER("StartDate", $start)."
			</td>
		</tr>
	
		<!-- End Date -->
		<tr>
			<td class='tabletext formfieldtitle' width='30%' valign='top'>".$Lang['eSurvey']['EndDate']." <span class='tabletextrequire'>*</span></td>
			<td align='left' valign='top'>
				".$linterface->GET_DATE_PICKER("EndDate", $end)."
			</td>
		</tr>";
if(!$hideTitle){
$x .= "	<!-- Title -->
		<tr>
			<td class='tabletext formfieldtitle' width='30%' valign='top'>".$Lang['eSurvey']['Title']." <span class='tabletextrequire'>*</span></td>
			<td align='left' valign='top'>
				<input type='text' class='textboxtext' name='Title' value='".$Title."'>
			</td>
		</tr>";
}
		
$x .= "  <!-- Description -->
		<tr>
			<td class='tabletext formfieldtitle' valign='top'>".$Lang['SysMgr']['SchoolNews']['Description']."</td>
			<td align='left' valign='top'>".$linterface->GET_TEXTAREA("Description", $Description)."</td>
		</tr>
	</table>

	<input type='hidden' name='qStr' value='".str_replace("___","#",str_replace("_AND_","&",$Question))."'>
	<input type='hidden' name='aStr' value=''>";
		
//		<!-- Record Status -->
//		<tr>
//			<td class='tabletext formfieldtitle' valign='top' rowspan='3'>".$Lang['SysMgr']['SchoolNews']['Status']."</td>
//			<td align='left' valign='top' style='line-height:18px; border-bottom:1px solid #EFEFEF;'>
//				<input type='radio' name='RecordStatus' id='RecordStatus1' value='1' CHECKED> <label for='RecordStatus1'>".$Lang['SysMgr']['SchoolNews']['StatusPublish']."</label> 
//				</br>
//			  	<span style='margin-left:20px;'>
//					<input type='checkbox' name='email_alert' id='email_alert' value='1'> <label for='email_alert'>".$Lang['SysMgr']['SchoolNews']['EmailAlert']."</label>
//				</span>
//				</br>
//				<span style='margin-left:20px;'>(".$Lang['eNotice']['NotifyRemark'].")</span>
//			</td>
//		</tr>
//		<tr>
//			<td style='line-height:18px; border-bottom:1px solid #EFEFEF;'>
//				<input type='radio' name='RecordStatus' id='RecordStatus2' value='2'> <label for='RecordStatus2'>".$i_status_pending."</label>  
//			</td>
//		</tr>
//		<tr>
//			<td style='line-height:18px;'>
//				<input type='radio' name='RecordStatus' id='RecordStatus3' value='3'> <label for='RecordStatus3'>".$i_status_template."</label>
//			</td>
//		</tr>
//		
//		<!-- Survey Form -->
//		<tr>
//			<td class='tabletext formfieldtitle' valign='top'>".$Lang['eSurvey']['SurveyForm']."</td>
//			<td style='line-height:18px;'>".
//				$linterface->GET_BTN($Lang['Btn']['Edit'], "button","newWindow('editform.php',1)")."
//				".$linterface->GET_BTN($Lang['eSurvey']['Preview'], "button","newWindow('preview.php',1)")."
//				</br>
//				<span>
//					<input name='DisplayQuestionNumber' type='checkbox' value='1' id='DisplayQuestionNumber' ".($DisplayQuestionNumber? "CHECKED" : "")."> <label for='DisplayQuestionNumber'>".$Lang['eSurvey']['DisplayQuestionNumber']."</label>
//				</span>
//				</br>
//				<span>
//					<input name='AllFieldsReq' type='checkbox' value='1' id='AllFieldsReq' CHECKED> <label for='AllFieldsReq'>".$i_Survey_AllRequire2Fill."</label>
//				</span>
//				</br>
//				<span>
//					<input type='checkbox' name='isAnonymous' value='1' id='isAnonymous'> <label for='isAnonymous'>".$i_Survey_Anonymous." (".$i_Survey_Anonymous_Description.")</label>
//				</span>
//			</td>
//		</tr>
//		
//		<tr>
//			<td>&nbsp;</td>
//			<td align='left' valign='top'>
//				<!-- Whole School -->
//				<span>
//					<input type='checkbox' name='publicdisplay' id='publicdisplay' onClick='groupSelection();' value='1' CHECKED> <label for='publicdisplay'>".$Lang['eSurvey']['WholeSchool']."</label>
//				</span>
//				<!-- Groups -->
//				<div id='groupSelectionBox' style='display:none;'>
//					<table width='100%' border='0' cellpadding='5' cellspacing='0'>
//						<tr>
//							<td valign='top'>
//								".$i_admintitle_group."
//							</td>
//						</tr>
//						<tr>
//							<td colspan='2'>".
//								$lo->displayAnnouncementGroups("", 1, 1)."
//							</td>
//						</tr>
//					</table>
//				</div>
//			</td>
//		</tr>
//	</table>

echo $x;
intranet_closedb();
?>