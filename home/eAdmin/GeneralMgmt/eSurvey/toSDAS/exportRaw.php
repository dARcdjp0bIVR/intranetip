<?php
//Using: Pun
/**
 * Change Log:
 * 2017-12-13 Pun
 *  - File create
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$sys_custom['iPf']['learnAndTeachReport'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system/libSDAS.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/learnAndTeach/learnAndTeach.php");

intranet_auth();
intranet_opendb();

$objDB = new libdb();
$lsurvey = new libsurvey();
$sdas = new libSDAS();
$year = new Year();
$lc = new Learning_Category();
$ay = new academic_year($AcademicYearID);
$fcm = new form_class_manage();
$lexport = new libexporttext();

$passCount = 0;
$result = array();
$sqlArr = array();

######## Helper function START ########
function getNumberInString($str){
    return preg_replace('/[^\d]/', '', $str);
}
######## Helper function END ########


######## Get Data START ########

#### Get Academic Year START ####
$academicYearName = $ay->Get_Academic_Year_Name();
#### Get Academic Year END ####

#### Get Class START ####
$allClass = $fcm->Get_All_Year_Class($AcademicYearID);
$allClassName = Get_Array_By_Key($allClass, Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN'));
$allClassNameUpper = array_map('strtoupper', $allClassName);
#### Get Class END ####

#### Get Subject START ####
$sql = "SELECT
    RecordID,
    EN_ABBR,
    CH_ABBR
FROM
    ASSESSMENT_SUBJECT
WHERE
    RecordStatus = 1
AND
    (
        CMP_CODEID = ''
    OR
        CMP_CODEID IS NULL
    )
";
$allSubject = $objDB->returnResultSet($sql);
$allSubjectAbbrId = array();
$allSubjectAbbr = array();
foreach($allSubject as $subj){
    $allSubjectAbbr[] = $subj['EN_ABBR'];
    $allSubjectAbbr[] = $subj['CH_ABBR'];
    $allSubjectAbbrId[ strtoupper($subj['EN_ABBR']) ] = $subj['RecordID'];
    $allSubjectAbbrId[ strtoupper($subj['CH_ABBR']) ] = $subj['RecordID'];
}
$allSubjectAbbr = array_unique($allSubjectAbbr);
$allSubjectAbbrUpper = array_keys($allSubjectAbbrId);
#### Get Subject END ####

#### Get Subject Teacher START ####
$sql = "SELECT
    STCT.UserID,
    ST.SubjectID
FROM
    SUBJECT_TERM_CLASS_TEACHER STCT
INNER JOIN
    SUBJECT_TERM_CLASS STC
ON
    STCT.SubjectGroupID = STC.SubjectGroupID
INNER JOIN
    SUBJECT_TERM ST
ON
    STC.SubjectGroupID = ST.SubjectGroupID
INNER JOIN
    ACADEMIC_YEAR_TERM AYT
ON
    ST.YearTermID = AYT.YearTermID
AND
    AYT.AcademicYearID = '{$AcademicYearID}'
";
$rs = $objDB->returnResultSet($sql);
$allTeacherId = Get_Array_By_Key($rs, 'UserID');
$allTeacherIdSql = implode("','", $allTeacherId);

$allTeacherSubjectId = array();
foreach($rs as $r){
    $allTeacherSubjectId[ $r['UserID'] ][] = $r['SubjectID'];
}

$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs1 = $objDB->returnArray($sql);
$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_ARCHIVE_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs2 = $objDB->returnArray($sql);
$teachers = array_merge($rs1, $rs2);

$allTeacherName = array();
$allTeacherNameId = array();
foreach($teachers as $teacher){
    $allTeacherName[] = $teacher['EnglishName'];
    $allTeacherName[] = $teacher['ChineseName'];
    $allTeacherNameId[ strtoupper($teacher['EnglishName']) ] = $teacher['UserID'];
    $allTeacherNameId[ strtoupper($teacher['ChineseName']) ] = $teacher['UserID'];
}
$allTeacherName = array_unique($allTeacherName);
$allTeacherNameUpper = array_keys($allTeacherNameId);
#### Get Subject Teacher END ####

#### Get Survey START ########
$sql = "SELECT
    *
FROM
    INTRANET_SURVEY
WHERE
    DateStart >= '{$StartDate} 00:00:00'
AND
    DateStart <= '{$EndDate} 23:59:59'
AND
    RecordStatus = 1
ORDER BY
    DateStart, Title
";
$rs = $objDB->returnResultSet($sql);

function filterSurvey($r){
    return preg_match('/[^-]+-[^-]+-[^-]+/', $r['Title']);
}
$rs = array_filter($rs, 'filterSurvey');
$allSurvey = BuildMultiKeyAssoc($rs, array('SurveyID'));
#### Get Survey END ########

#### Check Data Validation START ####
$correctSurveyId = array();
$incorrectSurveyId = array();
$surveyResult = array();
foreach($allSurvey as $surveyID => $surveyDetail){
    $title = $surveyDetail['Title'];
    list($className, $subjectAbbr, $teacherName) = explode('-', $title);

    $surveyResult[$surveyID] = array(
        'title' => $title,
        'className' => $className,
        'subjectAbbr' => $subjectAbbr,
        'teacherName' => $teacherName,
        'ERROR' => array()
    );

    ## Check empty START ##
    if(
        $className == '' ||
        $subjectAbbr == '' ||
        $teacherName == ''
    ){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_FORMAT;
        $incorrectSurveyId[] = $surveyID;
        continue;
    }
    ## Check empty END ##

    ## Check class exists START ##
    /* Removed, class name checking
     if(!in_array(strtoupper($className), $allClassNameUpper)){
     $surveyResult[$surveyID]['ERROR'][] = ERROR_CLASS_NAME;
     }*/
    ## Check class exists END ##

    ## Check subject exists START ##
    if(!in_array(strtoupper($subjectAbbr), $allSubjectAbbrUpper)){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_SUBJ_ABBR;
    }
    ## Check subject exists END ##

    ## Check teacher exists START ##
    $isValid = false;
    $teacherId = 0;
    foreach($allTeacherNameUpper as $tName){
        if(strpos($tName, strtoupper($teacherName)) !== false){
            $teacherId = $allTeacherNameId[$tName];
            $isValid = true;
            break;
        }
    }
    if(!$isValid){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_TEACHER_NAME;
    }
    ## Check teacher exists END ##

    ## Check teacher teach subject START ##
    $subjectID = $allSubjectAbbrId[strtoupper($subjectAbbr)];
    if(
        $teacherId &&
        $subjectID &&
        !in_array( $subjectID, (array)$allTeacherSubjectId[$teacherId] )
    ){
        $surveyResult[$surveyID]['ERROR'][] = ERROR_TEACHER_SUBJECT;
    }
    ## Check teacher teach subject END ##

    if(empty($surveyResult[$surveyID]['ERROR'])){
        $correctSurveyId[] = $surveyID;
    }else{
        $incorrectSurveyId[] = $surveyID;
    }
}
$passCount = count($correctSurveyId);
$failCount = count($incorrectSurveyId);

$surveyID = $correctSurveyId;
#### Check Data Validation END ####




#### Get Year START ####
$rs = $year->Get_All_Year_List();
$allYear = array();
foreach($rs as $r){
    $form = getNumberInString($r['YearName']);
    if($form){
        $allYear[ $form ] = $r['YearID'];
    }
}
#### Get Year END ####

#### Get Class START ####
$allClass = $year->Get_All_Classes($CheckClassTeacher=0, $AcademicYearID);
$allClassUpperDetails = array();
foreach($allClass as $r){
    $allClassUpperDetails[ strtoupper($r['ClassTitleEN']) ] = $r;
    $allClassUpperDetails[ strtoupper($r['ClassTitleB5']) ] = $r;
}
#### Get Class END ####

#### Get Learning Category START ####
$sql = "SELECT
    RecordID,
    LearningCategoryID
FROM
    ASSESSMENT_SUBJECT";
$rs = $objDB->returnResultSet($sql);
$allLearningCategory = BuildMultiKeyAssoc($rs, array('RecordID') , array('LearningCategoryID'), $SingleValue=1);
#### Get Learning Category END ####

#### Get Subject START ####
$sql = "SELECT
    RecordID,
    EN_ABBR,
    CH_ABBR
FROM
    ASSESSMENT_SUBJECT
WHERE
    RecordStatus = 1
AND
    (
        CMP_CODEID = ''
    OR
        CMP_CODEID IS NULL
    )
";
$allSubject = $objDB->returnResultSet($sql);
$allSubjectMapping = array();
$allSubjectAbbrId = array();
foreach($allSubject as $subj){
    $allSubjectAbbrId[ strtoupper($subj['EN_ABBR']) ] = $subj['RecordID'];
    $allSubjectAbbrId[ strtoupper($subj['CH_ABBR']) ] = $subj['RecordID'];
    
    $allSubjectMapping[$subj['RecordID']] = $subj;
}

#### Get Subject END ####

#### Get Subject Teacher START ####
$sql = "SELECT
    STCT.UserID,
    ST.SubjectID
FROM
    SUBJECT_TERM_CLASS_TEACHER STCT
INNER JOIN
    SUBJECT_TERM_CLASS STC
ON
    STCT.SubjectGroupID = STC.SubjectGroupID
INNER JOIN
    SUBJECT_TERM ST
ON
    STC.SubjectGroupID = ST.SubjectGroupID
INNER JOIN
    ACADEMIC_YEAR_TERM AYT
ON
    ST.YearTermID = AYT.YearTermID
AND
    AYT.AcademicYearID = '{$AcademicYearID}'
";
$rs = $objDB->returnResultSet($sql);
$allTeacherId = Get_Array_By_Key($rs, 'UserID');
$allTeacherIdSql = implode("','", $allTeacherId);

$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs1 = $objDB->returnArray($sql);
$sql = "SELECT UserID,ChineseName,EnglishName FROM INTRANET_ARCHIVE_USER WHERE UserID IN ('{$allTeacherIdSql}')";
$rs2 = $objDB->returnArray($sql);
$teachers = array_merge($rs1, $rs2);

$allTeacherNameId = array();
foreach($teachers as $teacher){
    $allTeacherNameId[ strtoupper($teacher['EnglishName']) ] = $teacher['UserID'];
    $allTeacherNameId[ strtoupper($teacher['ChineseName']) ] = $teacher['UserID'];
}
#### Get Subject Teacher END ####



#### Get all survey START ####
$surveyIdSql = implode("','", $surveyID);
$sql = "SELECT
    *
FROM
    INTRANET_SURVEY
WHERE
    SurveyID IN ('{$surveyIdSql}')
ORDER BY
    DateStart, Title
";
$allSurvey = $objDB->returnResultSet($sql);

$surveyYearId = array();
$surveyYearClassId = array();
$surveySpecialClassName = array();
$surveySubjectId = array();
$surveyTeacherId = array();
foreach($allSurvey as $survey){
    $surveyId = $survey['SurveyID'];
    $title = $survey['Title'];
    list($className, $subjectAbbr, $teacherName) = explode('-', $title);
    
    $classDetail = $allClassUpperDetails[ strtoupper($className) ];
    if($classDetail){
        
        $surveyYearId[$surveyId] = $classDetail['YearID'];
        $surveyYearClassId[$surveyId] = $classDetail['YearClassID'];
        $surveySpecialClassName[$surveyId] = '';
    }else{
        $surveyYearId[$surveyId] = 0;
        $surveyYearClassId[$surveyId] = 0;
        $surveySpecialClassName[$surveyId] = $className;
    }
    
    $surveySubjectId[$surveyId] = $allSubjectAbbrId[ strtoupper($subjectAbbr) ];
    
    foreach($allTeacherNameId as $tName => $tid){
        if(strpos($tName, strtoupper($teacherName)) !== false){
            $surveyTeacherId[$surveyId] = $tid;
            break;
        }
    }
}
#### Get all survey END ####

#### Get Survey Question START ####
$allQuestion = array();
if(count($allSurvey)){
    $lsurvey->splitQuestion($allSurvey[0]['Question']);
    $allQuestion = $lsurvey->question_array;
}
#### Get Survey Question END ####

#### Get all answer START ####
function answerToScore($answer){
    switch ($answer){
        case '0':
            return 5;
        case '1':
            return 4;
        case '2':
            return 3;
        case '3':
            return 2;
        case '4':
            return 1;
        case '5':
            return 0;
    }
    return 0;
}

$sql = "SELECT
    SurveyID,
    Answer
FROM
    INTRANET_SURVEYANSWER
WHERE
    SurveyID IN ('{$surveyIdSql}')";
$rs = $objDB->returnResultSet($sql);
// debug_rt($rs);

$surveyAnswer = array();
foreach($rs as $r){
    $surveyAnswer[$r['SurveyID']][] = $r['Answer'];
}
#### Get all answer END ####
######## Get Data END ########




$data = array();
foreach($allSubjectMapping as $subjectID => $subjectInfo){
    $subjectName = Get_Lang_Selection($subjectInfo['CH_ABBR'], $subjectInfo['EN_ABBR']);
    $teacherName = '';
}

foreach($allSurvey as $survey){
    $surveyId = $survey['SurveyID'];
    $title = $survey['Title'];
    list($className, $subjectAbbr, $teacherName) = explode('-', $title);
    
    $answer = $surveyAnswer[$surveyId];
    foreach((array)$answer as $index=>$ans){
        $_ans = explode('#ANS#', $ans);
        array_shift($_ans);
        $data[$subjectAbbr][$teacherName][$className]['#' . ($index+1)] = array();
        foreach($_ans as $_a){
            $data[$subjectAbbr][$teacherName][$className]['#' . ($index+1)][] = answerToScore($_a);
        }
    }
}
// debug_rt($allSurvey);
// debug_r($data);


######## Export START ########
$delimiter = "\t";
$valueQuote = "";

$Content = '';
$Content .= $valueQuote.$ec_iPortfolio_Report['subject'].$valueQuote.$delimiter;
$Content .= $valueQuote.$Lang['General']['Teacher'].$valueQuote.$delimiter;
$Content .= $valueQuote.$ec_iPortfolio['class'].$valueQuote.$delimiter;
$Content .= $valueQuote.$ec_iPortfolio['student'].$valueQuote.$delimiter;

for($i=0,$iMax=count($allQuestion);$i<$iMax;$i++){
    $Content .= $valueQuote.'Q'.($i+1).$valueQuote.$delimiter;
}
$Content = trim($Content, $delimiter);
$Content .= "\n";

foreach($data as $subjectAbbr => $d1){
    foreach($d1 as $teacherName => $d2){
        foreach($d2 as $className => $d3){
            foreach($d3 as $studentName =>$d4){
                $Content .= $valueQuote.$subjectAbbr.$valueQuote.$delimiter;
                $Content .= $valueQuote.$teacherName.$valueQuote.$delimiter;
                $Content .= $valueQuote.$className.$valueQuote.$delimiter;
                $Content .= $valueQuote.$studentName.$valueQuote.$delimiter;
                foreach($d4 as $ans){
                    $Content .= $valueQuote.$ans.$valueQuote.$delimiter;
                }
                $Content = trim($Content, $delimiter);
                $Content .= "\n";
            }
        }
    }
}


// Output the file to user browser
$lexport->EXPORT_FILE("Survey_{$StartDate}_{$EndDate}.csv", $Content);







