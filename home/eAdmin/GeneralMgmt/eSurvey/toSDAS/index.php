<?php
//Using: Pun
/**
 * Change Log:
 * 2017-12-13 Pun
 *  - Added export survey
 * 2017-05-24 Pun
 *  - File create
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$sys_custom['iPf']['learnAndTeachReport'] || !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/SDAS/customReport/learnAndTeach/learnAndTeach.php");

intranet_auth();
intranet_opendb();

$objDB = new libdb();
$linterface = new interface_html();
$lsurvey = new libsurvey();
$learnAndTeach = new learnAndTeach($objDB);


######## Get data START ########
$academicYearID = Get_Current_Academic_Year_ID();
$startDate = substr(getStartDateOfAcademicYear($academicYearID), 0, 10);
$endDate = substr(getEndDateOfAcademicYear($academicYearID), 0, 10);

$startDateHTML = $linterface->GET_DATE_PICKER("StartDate", $startDate);
$endDateHTML = $linterface->GET_DATE_PICKER("EndDate", $endDate);
$academicYearHTML = getSelectAcademicYear('AcademicYearID', $tag='', $noFirst=1, $noPastYear=0, $targetYearID='', $displayAll=0, $pastAndCurrentYearOnly=1, $OrderBySequence=1, $excludeCurrentYear=0, $excludeYearIDArr=array());

$allUpdateDetails = $learnAndTeach->getAllTransferDetails();
if(count($allUpdateDetails)){
    $lastUpdateDetails = $allUpdateDetails[0];
    $lastUpdateDetailHTML = Get_Last_Modified_Remark($date=$lastUpdateDetails['InputDate'], $byUserName='', $byUserID=$lastUpdateDetails['UserID'], $DisplayText='');
}else{
    $lastUpdateDetailHTML = '';
}
######## Get data END ########


######## UI START ########
# Left menu 
$TAGS_OBJ[] = array($Lang['eSurvey']['LearnAndTeach']['ToSDAS']);
$CurrentPageArr['eAdmineSurvey'] = 1;
$CurrentPage = "PageLearnAndTeachToSDAS";
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

# Start layout
$h_stepUI = $linterface->GET_IMPORT_STEPS(
    $CurrStep = 1,
    $CustStepArr = array(
        $Lang['eSurvey']['LearnAndTeach']['SelectData'],
        $Lang['General']['ImportArr']['ImportStepArr']['CSVConfirmation'],
        $Lang['General']['ImportArr']['ImportStepArr']['ImportResult']
    )
);
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
    <tr>
    	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
    </tr>
    <tr>
    	<td><?=$h_stepUI;?></td>
    </tr>
    
    <tr>
    	<td>
    		
            <form id="form1" name="form1" action="transfer.php" method="post">
            	<!-- Content -->
            	<div id="div_content">
            		<table width="90%" class='form_table_v30'>
                		<!-- Hint -->
                		<tr>
                			<td align="left" valign="top" colspan="2">
                				<b><?=$Lang['eSurvey']['LearnAndTeach']['SurveyTitleHint'] ?></b>
                			</td>
                		</tr>
                		
                		<!-- Start Date -->
                		<tr>
                			<td class="tabletext field_title" nowrap="nowrap" valign="top"><?=$Lang['eSurvey']['LearnAndTeach']['SurveyStartDate']?> <span class="tabletextrequire">*</span></td>
                			<td align="left" valign="top">
                				<?=$startDateHTML?>
                			</td>
                		</tr>
                	
                		<!-- End Date -->
                		<tr>
                			<td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['LearnAndTeach']['SurveyEndDate']?> <span class="tabletextrequire">*</span></td>
                			<td align="left" valign="top">
                				<?=$endDateHTML?>
                			</td>
                		</tr>
                		
                		<!-- Title -->
                		<tr>
            				<td class="tabletext field_title" width="30%" valign="top"><?=$Lang['eSurvey']['LearnAndTeach']['ResultAcademicYear']?> <span class="tabletextrequire">*</span></td>
                            <td align="left" valign="top">
                            	<?=$academicYearHTML ?>
                            </td>
                		</tr>
                		
                		<!-- Hint -->
                		<tr>
                			<td align="left" valign="top" colspan="2">
                				<span><?=$lastUpdateDetailHTML ?></span>
                			</td>
                		</tr>
                		
                	</table>
            	</div>
            	
            	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
            		<tr>
            			<td>
            				<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
            					<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
            					<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
            					<tr>
            						<td align="center">
            							<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
            							<?= $linterface->GET_ACTION_BTN($Lang['eSurvey']['LearnAndTeach']['ExportRaw'], "button", "document.form1.action = 'exportRaw.php';document.form1.submit();")?>
            							<?= $linterface->GET_ACTION_BTN($Lang['eSurvey']['LearnAndTeach']['ExportNotUnansweredUser'], "button", "document.form1.action = 'exportUnanswer.php';document.form1.submit();")?>
            						</td>
            					</tr>
            				</table>
            			</td>
            		</tr>
            	</table>
            </form>
    	</td>
    </tr>

</table>



<script>
(function($){

	$('#form1').submit(function(e){
		if(!check_date(document.form1.StartDate, '<?=$Lang['eSurvey']['ErrorDate'] ?>')){
			return false;
		}
		if(!check_date(document.form1.EndDate, '<?=$Lang['eSurvey']['ErrorDate'] ?>')){
			return false;
		}
		if(document.form1.StartDate.value > document.form1.EndDate.value){
    		alert('<?=$Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'] ?>');
    		return false;
		}

		return true;
	});
	
})(jQuery);
</script>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
