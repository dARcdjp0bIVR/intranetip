<?php

################## Change Log ##
#
#	Date:	2015-03-25	Charles Ma
#			Add noapprostat_lang
#	Date:	2011-03-09	YatWoon
#			Add "Display question number"
#
################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['eSurvey']['ViewSurveyResult'];
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$lsurvey = new libsurvey($SurveyID);

$ownerGroupID = $lsurvey->OwnerGroupID;
$ltargetGroup = new libgroup($GroupID);

     $queString = $lsurvey->Question;
     include_once($PATH_WRT_ROOT."includes/libform.php");
     $lform = new libform();
     $queString = $lform->getConvertedString($queString);
    
     $poster = $lsurvey->returnPosterName();
     $ownerGroup = $lsurvey->returnOwnerGroup();
     $targetGroups = $lsurvey->returnTargetGroups();

     if (sizeof($targetGroups)==0)
     {
         $target = "$i_general_WholeSchool";
     }
     else
     {
         $target = implode(", ",$targetGroups);
     }
     $answer = $lsurvey->returnAnswerForGroup($GroupID);

$survey_description = nl2br(intranet_convertAllLinks($lsurvey->Description));
$survey_description = $survey_description ? $survey_description : "---";
?>
<br />
<table width=95% cellspacing=0 cellpadding=5 align="center">
<tr><td class="tabletext formfieldtitle" width="30%"><?=$i_general_startdate?></td><td><?=$lsurvey->DateStart?></td></tr>
<tr><td class="tabletext formfieldtitle" width="30%"><?=$i_general_enddate?></td><td><?=$lsurvey->DateEnd?></td></tr>
<tr><td class="tabletext formfieldtitle" width="30%"><?=$i_general_title?></td><td><?=$lsurvey->Title?></td></tr>
<tr><td class="tabletext formfieldtitle" width="30%"><?=$i_general_description?></td><td><?=$survey_description?></td></tr>
<tr><td class="tabletext formfieldtitle" width="30%"><?=$i_Survey_Poster?></td><td><?=$poster?></td></tr>
<tr><td class="tabletext formfieldtitle" width="30%"><?=$i_general_TargetGroup?></td><td><?=$target?></td></tr>
<tr><td colspan=2>
<hr>
<B><?=$i_Survey_perGroup." ".displayArrow()." ".$ltargetGroup->Title?></B><br>

<? if ($lsurvey->RecordType != 1) { ?>
<a class=functionlink_new href=list_user.php?SurveyID=<?=$SurveyID?>&GroupID=<?=$GroupID?>>(<?=$i_Survey_perUser?>)</a>
<? } ?>
<?php if ($answer != "") { ?>
<script language="javascript" src="/templates/forms/form_view.js"></script>
<form name="ansForm" method="post" action="update.php">
        <input type=hidden name="qStr" value="">
        <input type=hidden name="aStr" value="">
</form>
<script language="Javascript">
<?=returnFormStatWords()?>
var myQue = "<?=$queString?>";
var myAns = new Array();
<?=$answer?>
var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';
var noapprostat_lang = '<?=$Lang['eSurvey']['NoApproStat']?>';
		var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';

var stats= new Statistics();
stats.qString = myQue;
stats.answer = myAns;
stats.analysisData();
document.write(stats.getStats());
</SCRIPT>
<? } else
{
    echo "<font color=red>$i_Survey_NoSurveyForCriteria</font>";
}
?>
<hr>
</td></tr>
<tr>
	<td colspan="2" align="center">
		<?=$linterface->GET_BTN($button_print, "button", "window.print();") ?>
		<?=$linterface->GET_BTN($button_close, "button", "window.close();") ?>
		<?=$linterface->GET_BTN($button_back, "button", "history.back();") ?>
	</td>
</tr>

</table>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>