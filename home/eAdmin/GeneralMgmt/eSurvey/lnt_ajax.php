<?
# Using: 

###################################
#
#	Date:	2017-11-03	Pun
#			- File Create
#
###################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


switch($action){
    case 'getAcademicYearTermSelect':
        echo getSelectSemester2('id="AcademicYearTerm" name="AcademicYearTerm"', $selected="", $ParQuoteValue=1, $AcademicYearID=$AcademicYearID);
        break;
    case 'getSubjectGroupSelect':
        $linterface = new interface_html();
        $scm = new subject_class_mapping();
        
        $subjectGroup = $scm->Get_Subject_Group_List($AcademicYearTerm);
        
        #### Get select UI START ####
        $selectedGroupIdList = array();
        $deselectedGroup = array();
        foreach ($subjectGroup["SubjectGroupList"] as $subjectGroupID => $groupInfo) {
            if(!strstr($groupInfo['ClassTitleB5'], "家課") && !strstr($groupInfo['ClassTitleEN'], "Homework")){
	            $deselectedGroup[] = array(
	                'value' => $subjectGroupID,
	                'name' => Get_Lang_Selection($groupInfo['ClassTitleB5'], $groupInfo['ClassTitleEN'])
	            );
        	}
        }

        $settings = array(
            'deselectedHeader' => $Lang['AccountMgmt']['SubjectGroups'],
            'deselectedSelectionName' => 'deselectedSubjectGroup',
            'deselectedList' => $deselectedGroup,
            'selectedHeader' => $Lang['eSurvey']['LearnAndTeach']['SelectedSubjectGroup'],
            'selectedSelectionName' => 'selectedSubjectGroup',
            'selectedList' => $selectedGroup
        );
        
        echo $linterface->generateUserSelection($settings);
        #### Get select UI END ####
        break;
}