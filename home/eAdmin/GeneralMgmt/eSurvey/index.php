<?php
// Modifying by: Isaac

##################################################
#   Date:	2018-03-14 (Isaac) [DM#3390]
#           added intranet_htmlspecialchars(trim($keyword));
#
#   Date:	2017-01-17 (Isaac)
#	        Improved: added EnglishName & ChineseName to keyword search's sql
#                     added Get_Safe_Sql_Like_Query to keyword search's sql
#
#	Date:	2017-11-02 Pun
#			Add L&T cust
#	
#	Date:	2017-02-23 Anna		[2016-0928-1637-22240]
#			Add Import Link		($sys_custom['SurveyImportFunction'])
#	
#	Date:	2010-11-30	YatWoon
#			IP25 UI Standard
#
##################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
 
$CurrentPageArr['eAdmineSurvey'] = 1;
if($sys_custom['iPf']['learnAndTeachReport'] && $QuestionType == 'LT'){
    $CurrentPage = "PageLearnAndTeachSurveyList";
}else{
    $CurrentPage = "PageSurveyList";
}

$lsurvey = new libsurvey();
$linterface = new interface_html();

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);

$keyword = convertKeyword(intranet_htmlspecialchars(trim($keyword)));
$RecordStatus = $RecordStatus ? $RecordStatus : 1;

$surveyTypeSelect = "";
$surveyTypeSelect = "<SELECT name='RecordStatus' onChange='this.form.submit()'>\n";
$surveyTypeSelect.= "<OPTION value=1 ".($RecordStatus==1 || $RecordStatus==""? "SELECTED":"").">$i_status_published</OPTION>\n";
$surveyTypeSelect.= "<OPTION value=2 ".($RecordStatus==2? "SELECTED":"").">$i_status_pending</OPTION>\n";
$surveyTypeSelect.= "<OPTION value=3 ".($RecordStatus==3? "SELECTED":"").">$i_status_template</OPTION>\n";
$surveyTypeSelect.= "</SELECT>&nbsp;\n";

$cond = '';
$joinGroup = "left join INTRANET_GROUPSURVEY as c on (c.SurveyID=a.SurveyID)";
if($sys_custom['iPf']['learnAndTeachReport']){
    if($QuestionType == 'LT'){
        $cond .= " AND a.QuestionType = 'LT'";
        $joinGroup = "left join INTRANET_SUBJECTGROUPSURVEY as c on (c.SurveyID=a.SurveyID)";
    }else{
        $cond .= " AND a.QuestionType IS NULL";
    }
}

$x = "";
$name_field = getNameFieldByLang("b.");
$sql =  "	SELECT 
				left(a.DateStart,10) as StartDate, 
				left(a.DateEnd, 10) as EndDate, 
				CONCAT('<a href=javascript:viewSurvey(', a.SurveyID, ')>', 
	       				'<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" alt=\"$button_view\" border=0></a>'),
				a.Title, 
				$name_field as Creator,";
				
if($RecordStatus==1)
	$sql .= "	CONCAT('<a href=\"javascript:view_result(', a.SurveyID ,');\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eOffice/icon_resultview.gif\" border=0></a>'),";
else
	$sql .= " '&nbsp', ";

$sql .= "		if(count(c.SurveyID)>0, '". $Lang['eSurvey']['SpecificGroups'] ."', '". $Lang['eSurvey']['WholeSchool'] ."') as SurveyType,
				a.DateModified,
				CONCAT('<input type=checkbox name=SurveyID[] value=', a.SurveyID ,'>')
			FROM 
				INTRANET_SURVEY as a
				left join INTRANET_USER as b on (b.UserID=a.PosterID) 
				{$joinGroup}
			WHERE 
				a.RecordStatus = $RecordStatus
				and ((a.Title like '%".$keyword."%') OR (b.EnglishName like '%".$keyword."%') OR
				(b.ChineseName like '%".$keyword."%'))
				{$cond}
			GROUP BY 
				a.SurveyID";
$li->sql = $sql;
$li->field_array = array("a.DateStart");
$li->fieldorder2 = ", a.DateInput desc";
$li->no_col = 9;
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN 
$li->column_list .= "<th>".$Lang['eSurvey']['StartDate']."</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['EndDate']."</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['ViewSurvey']."</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['Title']."</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['Creator']."</th>\n";
$li->column_list .= "<th>". $Lang['eSurvey']['ViewResults'] ."</th>\n";
$li->column_list .= "<th>". $Lang['eSurvey']['SurveyType'] ."</th>\n";
$li->column_list .= "<th>".$Lang['eSurvey']['LastModified']."</th>\n";
$li->column_list .= "<th width=1>".$li->check("SurveyID[]")."</th>\n";

### Button
// $AddBtn = $linterface->GET_LNK_NEW("new.php","","","","",0);
// $editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'SurveyID[]','edit.php')", "", "", "", "", 0);
// $delBtn = $linterface->GET_LNK_REMOVE("javascript:checkRemoveThis(document.form1,'SurveyID[]','remove_update.php')");

// $searchTag 	= "<table border=\"0\" cellspacing=\"0\" cellpadding=\"3\"><td>";
// $searchTag 	.= "&nbsp;<input type=\"text\" name=\"keyword\" class=\"formtextbox\" maxlength=\"50\" value=\"".stripslashes($keyword)."\" onFocus=\"this.form.pageNo.value=1;\"></td>";
// $searchTag 	.= "<td>".$linterface->GET_BTN($button_find, "submit", "","submit3"," class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\" ")."</td>";
// $searchTag 	.= "</td></table>";
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";

### Title ###
$TAGS_OBJ[] = array($Lang['eSurvey']['SurveyList']);
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
?>

<SCRIPT LANGUAGE=Javascript>
function checkRemoveThis(obj,element,page){
    if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
	else{
        if(confirm("<?=$i_Survey_RemoveConfirm?>")){
            obj.action=page;
            obj.method="POST";
            obj.submit();
        }
    }
}

function view_result(id)
{
	newWindow('result.php?SurveyID='+id,1);
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
} 

function viewSurvey(id)
{
    newWindow('view.php?SurveyID='+id,10);
}
</SCRIPT>

<form name="form1" method="get" action="index.php">

<div class="content_top_tool">
	<div class="Conntent_tool">
		<? if($sys_custom['iPf']['learnAndTeachReport'] && $QuestionType == 'LT') { ?>
			<a href="lnt_new.php" class="import"> <?=$Lang['eSurvey']['LearnAndTeach']['BatchCreate']?></a>
		<? }else{ ?>
    		<a href="new.php" class="new"> <?=$Lang['Btn']['New']?></a>
    		<? if($sys_custom['SurveyImportFunction']) { ?>
    			<a href="import.php" class="import"> <?=$Lang['Btn']['Import']?></a>
    		<? } ?>
		<? } ?>
	</div>
	<div class="Conntent_search" align="right"><?=$searchTag?></div>
	<br style="clear:both" />
</div>

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="common_table_tool">
	<td valign="bottom"><div class="table_filter"><?=$surveyTypeSelect?></div></td>
	<td valign="bottom">
		<div>
		<a href="javascript:checkEdit(document.form1,'SurveyID[]','edit.php')" class="tool_edit"><?=$Lang['Btn']['Edit']?></a>
		<a href="javascript:checkRemoveThis(document.form1,'SurveyID[]','remove_update.php')" class="tool_delete"><?=$Lang['Btn']['Delete']?></a>
		</div>
	</td>
</tr>
</table>
</div>

<?=$li->display();?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="QuestionType" value="<?=$QuestionType?>" />

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

if($sys_custom['PopupSurveyAfterCreated'] && !empty($SurveyID))
{
?>
<SCRIPT LANGUAGE=Javascript>
newWindow('view.php?SurveyID=<?=$SurveyID?>',10);
</script>
<?	
}
?>