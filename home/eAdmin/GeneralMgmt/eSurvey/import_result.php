<?php
// Modifying by: 

##################################################
#	
#	Date:	2017-02-23 Anna	[2016-0928-1637-22240]
#			Create File		($sys_custom['SurveyImportFunction'])
#
##################################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$limport = new libimporttext();
$lf = new libfilesystem();

$linterface = new interface_html();
$lsurvey = new libsurvey();
$lg = new libgrouping();

# Page title
$CurrentPageArr['eAdmineSurvey'] = 1;
$CurrentPage = "PageSurveyList";

# Tag Information
$TAGS_OBJ[] = array($Lang['eSurvey']['SurveyList']);

# Left Menu
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

# Navigation
$PAGE_NAVIGATION[] = array($Lang['eSurvey']['SurveyList'], "javascript:history.back();");
$PAGE_NAVIGATION[] = array($Lang['eSurvey']['NewSurvey']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 1);
$STEPS_OBJ[] = array($i_general_complete, 0);

# Import Mode
$importMode = $_POST['importMode'];

# File Type
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lf->file_ext($name));
if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}

# Get CSV data
$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data)) {
	$col_name = array_shift($data);
}

# Check Import Mode Header
$file_format = array();
if($importMode=='new'){
	$file_format = array('Template Title','Start Date','End Date','Title','Description','Publish / Pending','Display question number (Y/N)','All questions are required to be answered (Y/N)','Anonymous (Y/N)','Target (Whole School / Group Name)');
}
else if($importMode=='update'){
	$file_format = array('Start Date','End Date','Title','Description','Publish / Pending','Display question number (Y/N)','All questions are required to be answered (Y/N)','Anonymous (Y/N)');
}
else{
	header("location: import.php?xmsg=import_failed");
	exit();
}

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i]) {
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}

// special_announce_public_allowed
//$lo = new libgrouping();

# User
$UserID = $_SESSION["UserID"];
$lu = new libuser($UserID);
$UserName = $lu->getNameForRecord();

# Table Header
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" class=\"common_table_list_v30 view_table_list_v30\">";
$x .= "<tr>";
$x .= "<th class=\"field_title\">#</td>";
if($importMode=='new'){
	$x .= "<th class=\"field_title\">".$Lang['eSurvey']['TemplateTitle']."</td>";
}
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['StartDate']."</td>";
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['EndDate']."</td>";
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['Title']."</td>";
$x .= "<th class=\"field_title\">".$Lang['SysMgr']['SchoolNews']['Description']."</td>";
$x .= "<th class=\"field_title\">".$Lang['SysMgr']['SchoolNews']['StatusPublish'].'/'.$i_status_pending."</td>";
$x .= "<th class=\"field_title\">".$Lang['eSurvey']['DisplayQuestionNumber'].'(Y/N)'."</td>";
$x .= "<th class=\"field_title\">".$i_Survey_AllRequire2Fill.'(Y/N)'."</td>";
$x .= "<th class=\"field_title\">".$i_Survey_Anonymous.'(Y/N)'."</td>";
if($importMode=='new'){
	$x .= "<th class=\"field_title\">".$Lang['eSurvey']['Target'].'('.$Lang['eSurvey']['WholeSchool'].'/'.$i_admintitle_group.')'."</td>";
}
$x .= "<th class=\"field_title\">".$Lang['General']['Warning']."</td>";
$x .= "</tr>";

# Temp Table
$sql = "CREATE TABLE IF NOT EXISTS temp_survey_import
		(
			 TempID int(11) NOT NULL auto_increment,
			 TemplateID int(11) default NULL, 
		     Title varchar(255) NOT NULL,
		     Description mediumtext,
		     Question mediumtext,
		     DateStart datetime NOT NULL,
		     DateEnd datetime NOT NULL,
		     OwnerGroupID int(11) default NULL,
		     PosterID int(11) default NULL,
		     PosterName varchar(255) default NULL,
		     Internal char(2) default NULL,
		     AllFieldsReq int(11) default NULL,
		     RecordType char(2) default NULL,
		     RecordStatus char(2) default NULL,
		     DateInput datetime default NULL,
		     DateModified datetime default NULL,
		     DisplayQuestionNumber tinyint(1) default '0',
			 UserID  int(11),
			 GroupiD int(11) default NULL, 
			 GroupName varchar(255) NOT NULL,
		     PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$lsurvey->db_db_query($sql);

$sql = "ALTER TABLE temp_survey_import Add SurveyID int(11) default NULL";
$lsurvey->db_db_query($sql);

# Delete temp data in temp table
$sql = "delete from temp_survey_import where UserID=".$_SESSION["UserID"];
$lsurvey->db_db_query($sql) or die(mysql_error());

# Get Survey Templates
$survey_templates = $lsurvey->returnSurveyTemplates();
$templatesNum = sizeof($survey_templates);

# Get All Survey
$AllSurvey = $lsurvey->returnSurveyForMapping();
//$SurveyByDateStart =  BuildMultiKeyAssoc($AllSurvey, array("Title", "DateStart", "SurveyID"));
//$SurveyByDateEnd =  BuildMultiKeyAssoc($AllSurvey, array("Title", "DateEnd", "SurveyID"));

# Get Available Groups
$GroupInfo = $lg->returnSelectedCategoryGroupsFromType("", "INTRANET_GROUPANNOUNCEMENT", "AnnouncementID", 0, 1);
$GroupNum = sizeof($GroupInfo);

# Get Group Chinese Title
if($GroupNum > 0) {
	$GroupIDList = Get_Array_By_Key($GroupInfo, "0");
	$sql = "SELECT 
				GroupID, TitleChinese
		    FROM 
				INTRANET_GROUP
			WHERE 
				GroupID IN ('".implode("', '", $GroupIDList)."')";
	$thisGroupTitleList = $lsurvey->returnArray($sql);
	$thisGroupTitleList = build_assoc_array($thisGroupTitleList);
}

// loop data row
$error_occured = 0;
for($i=1; $i<sizeof($data); $i++)
{
	$error = array();
	
	// Get data
	if($importMode=='new'){
		list($TemplateTitle, $StartDate, $EndDate, $Title, $Description, $Status, $DisplayNumber, $AllRequired, $Anonymous, $Target) = $data[$i];
	} 
	else if ($importMode=='update'){
		list($StartDate, $EndDate, $Title, $Description, $Status, $DisplayNumber, $AllRequired, $Anonymous) = $data[$i];
	}
	
	// Data Handling
	$StartDate = trim($StartDate);
	$EndDate = trim($EndDate);
	$Title = trim($Title);
	$Description = trim($Description);
	$Status = trim($Status);
	$DisplayNumber = trim($DisplayNumber);
	$AllRequired = trim($AllRequired);
	$Anonymous = trim($Anonymous);
	
	$Question = "";
	$Titlename = $Title;
	$DescriptionText = $Description;
	
	$DateStart = getDefaultDateFormat($StartDate);
	$DateEnd = getDefaultDateFormat($EndDate);
	$startStamp = strtotime($DateStart);
	$endStamp = strtotime($DateEnd);
	
	$start_date_format = false;
	$end_date_format = false;
	$start_date_format = $lsurvey->checkDateFormat($DateStart);
	$end_date_format = $lsurvey->checkDateFormat($DateEnd);
	
	# Import New Survey
	if($importMode == 'new'){
		// Data Handling
		$TemplateTitle = trim($TemplateTitle);
		$Target = trim($Target);
		
		# Start Date and End Date
		// Empty Date
		if($StartDate=="" || $EndDate=="") {
			$error['RecordDate'] = $Lang['eSurvey']['MissingDate'];
		}
		// Date Format Error
		if(!$start_date_format || !$end_date_format) {
			$error['RecordDate'] = $Lang['General']['InvalidDateFormat'];
		}
		// Start Date > End Date
		else if(compareDate($startStamp, $endStamp) > 0) {
			$error['RecordDate'] = $Lang['General']['WarningArr']['EndDateCannotEarlierThanStartDate'] = "結束日期不能早於開始日期";;
		}
		// Start Date < Now
		else if(compareDate($startStamp, time()) < 0) {
			$error['RecordDate'] = $Lang['eSurvey']['StartDateWarning'];
		}
		
		# Template Title
		// No Template Title
		if($TemplateTitle == "") {
			$TemplateTitle = "";
			
			// Empty Title
			if($Title == ""){
				$error['Title'] = $Lang['eSurvey']['MissingTitle'] ;
			}
			else if($Title !== ""){
				$Titlename = $Title;
				$DescriptionText = $Description;
			}
		}
		// Have Template Title
		else if($TemplateTitle !==null) {
			$targetID = 0;
			$targetName = "";
			
			// Match survey templates with import template title 
			for($j=0; $j<$templatesNum; $j++) {
				if($TemplateTitle == trim($survey_templates[$j]['Title'])) {
					$targetID = $survey_templates[$j]['SurveyID'];
					$TemplateTitle = trim($survey_templates[$j]['Title']);
					
					// Set Title, Question and Description
					$lsurvey->retrieveRecord($targetID);
					$Titlename = $Title=="" ? trim($lsurvey->Title) : $Title;
					$Question = trim($lsurvey->Question);
					$DescriptionText = $Description==""? trim($lsurvey->Description) : $Description;
					
					break;
				}
			}
			
			// Cannot map any template
			if($targetID == 0){
				$error['TemplateTitle'] = $Lang['eSurvey']['ErrorTemplateTitle'] ;
			}
		}
		
		# Survey Target
		// No Target
		if($Target == "") {
			$error['Target'] = $Lang['eSurvey']['ErrorTarget'];
		}
		// Whole School
		else if($Target == "Whole School" ) {
			$targetName = 'Whole School';
			$GroupiD = "";
		}
		else { 
			// Match Target with import group name 
			$GroupiD = "";
			for($j=0; $j<$GroupNum; $j++) {
				$currentGroupID = $GroupInfo[$j][0];
				$currentGroupTitle = trim($thisGroupTitleList[$currentGroupID]);
				
				if($Target == $currentGroupTitle){
					$GroupiD = $currentGroupID;
					$targetName = $currentGroupTitle;
					break;
				}
			}
			
			// Cannot map any group
			if($GroupiD=="" || $GroupiD==0){
				$error['Target'] = $Lang['eSurvey']['ErrorTarget'];
			}
		}
	}
	# Update Existing Survey
	else if($importMode == 'update'){
		# Template Title
		// Empty Title
		if($Titlename == "" ){
			$error['Title'] = $Lang['eSurvey']['MissingTitle'] ;
		}
		else if ($Titlename !== ""){
			// Date Format Error
			if(!$start_date_format && !$end_date_format) {
				$error['RecordDate'] = $Lang['General']['InvalidDateFormat'];
			}
			else {
				$DateStartTime = "";
				$DateEndTime = "";
				if($start_date_format)
					$DateStartTime = $DateStart.' 00:00:00';
				if($end_date_format)
					$DateEndTime = $DateEnd.' 00:00:00';
				
				// loop Survey
				$SurveyIDArr = array();
				$matchedSurveyTitle = false;
				for($j=0; $j<sizeof($AllSurvey); $j++){
					// Match Survey with import title and start date / end date 
					if(trim($Titlename) == trim($AllSurvey[$j]['Title'])){
						if($DateStartTime == trim($AllSurvey[$j]['DateStart']) || $DateEndTime == trim($AllSurvey[$j]['DateEnd'])){
							$SurveyIDArr[] = $AllSurvey[$j]['SurveyID'];
						}
						else{
							$error['RecordDate'] = $Lang['eSurvey']['ErrorDate'];
						}
						$matchedSurveyTitle = true;
					}
				}
				
				// Cannot map any survey
				if(!$matchedSurveyTitle){
				 	$error['Title'] = $Lang['eSurvey']['ErrorTitle'];
				}
			}
		}
	}
	
	# Survey Status
	if($Status == 'Pending') {
		$RecordStatus = "2";
	}
	else if($Status == 'Publish') {
		$RecordStatus = "1";
	}
	// Wrong Status
	else {
		$error['Status'] = $Lang['eSurvey']['ErrorStatus'];
	}

	# Display Question Number
	if ($DisplayNumber == 'Y') {
		$DisplayQuestionNumber = 1;
	}
	else if($DisplayNumber == 'N') {
		$DisplayQuestionNumber = 0;
	}
	// Wrong Status
	else {
		$error['DisplayQNum'] = $Lang['eSurvey']['error']['DisplayQNum'];
	}
	
	# All questions are required to be answered
	if($AllRequired == "Y"){
		$AllFieldsReq = '1';
	}
	else if($AllRequired == 'N') {
		$AllFieldsReq = '0';
	}
	// Wrong Status
	else {
		$error['AllFieldsReq'] = $Lang['eSurvey']['error']['AllFieldsReq'];
	}		
	
	# Anonymous
	if($Anonymous == "Y") {
		$RecordType = '1';
	}
	else if($Anonymous == 'N') {
		$RecordType = '0';
	}
	// Wrong Status
	else {
		$error['RecordType'] = $Lang['eSurvey']['error']['RecordType'];
	}
	
	//$css = (sizeof($error)==0) ? "tabletext":"red";
	
	# Data row
	$x .= "<tr>";
	$x .= "<td class=\"tabletext\">".($i)."</td>";
	if($importMode=='new'){
		$x .= "<td class=\"".(empty($error['TemplateTitle'])? "tabletext" : "red")."\">".$TemplateTitle."</td>";
	}
	$x .= "<td class=\"".(empty($error['RecordDate'])? "tabletext" : "red")."\">".$DateStart."</td>";
	$x .= "<td class=\"".(empty($error['RecordDate'])? "tabletext" : "red")."\">".$DateEnd."</td>";
	$x .= "<td class=\"".(empty($error['Title'])? "tabletext" : "red")."\">".$Titlename."</td>";
	$x .= "<td class=\"tabletext\">".$DescriptionText."</td>";
	$x .= "<td class=\"".(empty($error['Status'])? "tabletext" : "red")."\">".$Status."</td>";
	$x .= "<td class=\"".(empty($error['DisplayQNum'])? "tabletext" : "red")."\">".$DisplayNumber."</td>";
	$x .= "<td class=\"".(empty($error['AllFieldsReq'])? "tabletext" : "red")."\">".$AllRequired."</td>";
	$x .= "<td class=\"".(empty($error['RecordType'])? "tabletext" : "red")."\">".$Anonymous."</td>";
	if($importMode=='new'){
		$x .= "<td class=\"".(empty($error['Target'])? "tabletext" : "red")."\">".$Target."</td>";
	}
	$x .= "<td class=\"".(sizeof($error)==0? "tabletext" : "red")."\">"; 
	
	// Error message
	if(sizeof($error) > 0) {
	 	if(is_array($error)){
		 	foreach($error as $Key=>$Value) {
		 		$x .= $Value.'<br/>';
		 	}
	 	}
	 	else{
	 		$x .= $error.'<br/>';
	 	}
	}
	else {
	 	$x .= "--";
	}
	$x .= "</tr>";
	
	if($error) {
	 	$error_occured++;
	}
	
	# Insert data into temp table
	if($importMode == 'new'){
		$sql = "insert into temp_survey_import
					(TemplateID, Title, Description, Question, DateStart, DateEnd, PosterID, PosterName, AllFieldsReq, RecordType, RecordStatus, DisplayQuestionNumber, UserID, GroupiD, GroupName) 
				values
					('$targetID', '".addslashes($Titlename)."', '".addslashes($DescriptionText)."', '".addslashes($Question)."', '$DateStart', '$DateEnd', '$UserID', '".addslashes($UserName)."', '$AllFieldsReq', '$RecordType', '$RecordStatus', '$DisplayQuestionNumber', '$UserID', '$GroupiD', '".addslashes($targetName)."') ";
		$lsurvey->db_db_query($sql) or die(mysql_error());
	}
	else if($importMode == 'update'){
		for($j=0; $j<sizeof($SurveyIDArr); $j++){
			$SurveyID = $SurveyIDArr[$j];
			$sql = "insert into temp_survey_import
						(Title, Description, DateStart, DateEnd, AllFieldsReq, RecordType, RecordStatus, DisplayQuestionNumber, UserID, SurveyID)
					values
						('".addslashes($Titlename)."', '".addslashes($DescriptionText)."', '$DateStart', '$DateEnd', '$AllFieldsReq', '$RecordType', '$RecordStatus', '$DisplayQuestionNumber', '$UserID', '$SurveyID') ";
			$lsurvey->db_db_query($sql) or die(mysql_error());
		}
	}
}
$x .= "</table>";

$js_onsubmit = ' onsubmit="submitConfirm(this); return false;" ';

if($error_occured) {
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
else {
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="JavaScript">
function submitConfirm(obj)
{
	obj.submit();
}
</script>	

<form name="form1"  action="import_update.php"  method="post"  enctype="multipart/form-data"<?=$js_onsubmit?>>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td align="right" valign="bottom"><?=$linterface->GET_SYS_MSG($msg, $xmsg);?></td>
		</tr>
		<tr>
			<td class="navigation">
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td colspan="2"><?=$x?></td>
					</tr>
				</table>
			</td> 
		</tr>
	</table>
	
	<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2">
						<?=$import_button?>
					</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

	<input type="hidden" name="CurrentTemplateIndex" value="0">
	<input type="hidden" name="importMode" id="importMode" value="<?=$importMode?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>