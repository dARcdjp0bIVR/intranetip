<?php
# using: yat

################# Change Log [Start] ############
# 
#	Date:	2011-06-02	YatWoon
#			add option "Do not allow users to edit replies"
#
#	Date:	2011-02-24	YatWoon
#			add option "Max. Reply Slip Option"
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$CurrentPageArr['eAdmineSurvey'] = 1;
$CurrentPage = "PageeSurveySettings_BasicSettings";
$lsurvey = new libsurvey();

# Left menu 
$TAGS_OBJ[] = array($Lang['eSurvey']['BasicSettings']);
$MODULE_OBJ = $lsurvey->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

# build select option object
for($i=3;$i<=100;$i++)
	$data1[] = array($i, $i);
$MaxReplySlipOption_selection = getSelectByArray($data1, "name='MaxReplySlipOption'", $lsurvey->MaxReplySlipOption , 0, 1);	

?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}
//-->
</script>


<form name="form1" method="post" action="edit_update.php">
<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$eDiscipline['SettingName']?> </span>-</em>
		<p class="spacer"></p>
	</div>

	<table class="form_table_v30">
	
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eSurvey']['NotAllowReSign']?></td>
				<td>
				<span class="Edit_Hide"><?=$lsurvey->NotAllowReSign ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="NotAllowReSign" value="1" <?=$lsurvey->NotAllowReSign ? "checked":"" ?> id="NotAllowReSign1"> <label for="NotAllowReSign1"><?=$i_general_yes?></label> 
				<input type="radio" name="NotAllowReSign" value="0" <?=$lsurvey->NotAllowReSign ? "":"checked" ?> id="NotAllowReSign0"> <label for="NotAllowReSign0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['Circular']['MaxNumberReplySlipOption']?></td>
				<td>
				<span class="Edit_Hide"><?=$lsurvey->MaxReplySlipOption?></span>
				<span class="Edit_Show" style="display:none"><?=$MaxReplySlipOption_selection?></span></td>
			</tr>
<?php if ($sys_custom['SurveySaveWithDraft']) { ?>
			<tr valign='top'>
				<td class="field_title" nowrap><?php echo $Lang['eSurvey']['DefaultAllowSaveDraft']; ?></td>
				<td>
				<span class="Edit_Hide"><?=$lsurvey->DefaultAllowSaveDraft? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="DefaultAllowSaveDraft" value="1" <?php echo $lsurvey->DefaultAllowSaveDraft? "checked":"" ?> id="DefaultAllowSaveDraft1"> <label for="DefaultAllowSaveDraft1"><?=$i_general_yes?></label> 
				<input type="radio" name="DefaultAllowSaveDraft" value="0" <?php echo $lsurvey->DefaultAllowSaveDraft? "":"checked" ?> id="DefaultAllowSaveDraft0"> <label for="DefaultAllowSaveDraft0"><?=$i_general_no?></label>
				</span></td>
			</tr>
<?php } ?>
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>


</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
