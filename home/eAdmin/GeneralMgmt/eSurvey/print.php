<?php
/*
 * Editing by
 * 
 * Modification Log:
 * 2015-06-16 [Ronald] [ip.2.5.6.7.1]
 *  - comment Parent Signature
 * 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eSurvey"])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();


## Get Data 
$SurveyID = IntegerSafe($SurveyID);
$viewUser = IntegerSafe($viewUser);


## Init
$invalidAccess = false;


## Library
$lform = new libform();
$lsurvey = new libsurvey($SurveyID);

if ($lsurvey->RecordType == 1)
{
    header("Location: result.php?SurveyID=$SurveyID");
    exit();
}


$MODULE_OBJ['title'] = $i_Survey_perUser;
$linterface = new interface_html("popup_no_layout.html");
$linterface->LAYOUT_START();

# retrieve survey
$sql = "SELECT Answer,UserName FROM INTRANET_SURVEYANSWER WHERE SurveyID = '$SurveyID' AND UserID = '$viewUser'";
$answer = $lsurvey->returnArray($sql,2);
list($aStr,$username) = $answer[0];
if ($aStr=="")
{
    header("Location: result_type.php?SurveyID=$SurveyID");
    exit();
}

$queString = $lsurvey->Question;
$queString = str_replace('"','&quot;',$queString);
$queString = str_replace("\n",'<br>',$queString);
$queString = str_replace("\r",'',$queString);
$aStr = str_replace('"','&quot;',$aStr);
$aStr = str_replace("\n",'<br>',$aStr);
$aStr = str_replace("\r",'',$aStr);

#$queString = str_replace('"','&quot;',$queString);
#$aStr = str_replace('"','&quot;',$aStr);


$poster = $lsurvey->returnPosterName();
$ownerGroup = $lsurvey->returnOwnerGroup();
$targetGroups = $lsurvey->returnTargetGroups();
$targetSubjectGroups = $lsurvey->returnTargetSubjectGroups();

if ($ownerGroup != "")
{
    $poster = "$poster<br>\n$ownerGroup";
}
if (sizeof($targetGroups)==0 && sizeof($targetSubjectGroups)==0)
{
    $target = $i_general_WholeSchool;
}
else
{
    $target = implode(", ",array_merge($targetGroups,$targetSubjectGroups));
}
$luser = new libuser($viewUser);
if ($luser->UserID == $viewUser)
{
//	    $username = $luser->UserNameClassNumber();
    $username = $luser->UserNameLang();
    $classname = $luser->ClassName;
    $classnumber = $luser->ClassNumber;
    $classlevel = $luser->ClassLevel;
}

$survey_description = nl2br(intranet_convertAllLinks($lsurvey->Description));
$survey_description = $survey_description ? $survey_description : "---";


## get school name
$school_name = GET_SCHOOL_NAME();

## get academic year	- no longer use  
//	$year = date("Y", strtotime($lsurvey->DateStart));
//	$academic_year = getAcademicYearByAcademicYearID($year, $intranet_session_language);




## Print date
$print_date = get_formatted_datetime();

//debug_r($_SESSION);die();
?>

<br />
<table width=95% cellspacing=0 cellpadding=5 border=0>
<tr>
	<td colspan="100%" align="center"><h3><?=$school_name?><br><?=$lsurvey->Title?></h3></td>
</tr>

<tr>
	<td><b><?=$Lang['eSurvey']['StudentName']?>: <u>&nbsp;&nbsp;<?=$username?>&nbsp;&nbsp;</u></b></td>
	<td>&nbsp;</td>
	<td align="right"><b><?=$Lang['eSurvey']['ClassAndNumber']?>: &nbsp;<?=$classname?>&nbsp;(<?=$classnumber?>)</u></b></td>
</tr>
<tr>
	<td colspan="100%">
		<script language="javascript" src="/templates/forms/form_view.js"></script>
			<script language="Javascript">
			var order_lang = '<?=$Lang['eSurvey']['Order']?>';
			var no_answer_lang = '<?=$Lang['eSurvey']['NotAnswered']?>';
			var DisplayQuestionNumber = '<?=$lsurvey->DisplayQuestionNumber?>';
			myQue = "<?=$queString?>";
			myAns = "<?=$aStr?>";
			document.write(viewForm(myQue, myAns));
		</SCRIPT>
	</td>
</tr>

<!--
<tr>
	<td>&nbsp;</td>
	<td align="right"><?=$Lang['eSurvey']['ParentSignature']?> :</td>
	<td style="width:180px; border-bottom: 1px solid black; ">&nbsp;</td>
</tr>
-->

<tr>
	<td colspan="100%"><?=$print_date?></td>
</tr>

<tr>
	<td colspan="100%" align="center" class="print_hide">
		<?=$linterface->GET_BTN($button_print, "button", "window.print();") ?>
		<?=$linterface->GET_BTN($button_close, "button", "window.close();") ?>
	</td>
</tr>
</table>

<?php

intranet_closedb();
$linterface->LAYOUT_STOP();
?>