<?php
# using:

########## Change Log [Start] ############
#
#	Date:	2016-08-04 (Bill)
#			- added handlingSpecialCharDisplay(), to handle html special characters display in HighCharts
#
#	Date:	2016-07-21 (Bill)
#			- hide HighCharts credits in Pie Chart
#			- hide Pie Chart if no available data 
#
#	Date:	2016-07-04 (Catherine)
#			embeded Highcharts graph, add getBarChartOption(), add getBarChartData()
#
#	Date:	2016-06-30 (Catherine)
#			embeded Highcharts graph, add getCountValue(), add getPieChartData()
#
#	Date:	2012-07-06 (YatWoon)
#			update download_attachment.php with encrypt logic
#
#	Date:	2011-12-07	YatWoon
#			Fixed: missing to check if attachment foler is null
#
#	Date:	2011-04-13	YatWoon
#			add choice to Z 
#
########## Change Log [End] ############


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/json.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "ePolling";
$lpolling = new libpolling();
$PollingID = IntegerSafe($PollingID);
$li = new libpolling($PollingID);
$json = new JSON_obj();

$hasAccessRight = $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$now = time();
$today = mktime(0,0,0,date("m",$now),date("d",$now),date("Y",$now));
$start = strtotime($li->DateStart);
if ($start > $today)
    $edit_btn = $linterface->GET_ACTION_BTN($button_edit, "submit");
else
    $edit_btn = "";

### Title ###
$top_menu_mode = 1;
$TAGS_OBJ[] = array($i_adminmenu_im_polling,"");
$MODULE_OBJ = $lpolling->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($button_view.($intranet_session_language=="en"?" ":""). $i_adminmenu_im_polling);

$pollingAttFolder = $li->Attachment;
if($pollingAttFolder)
{
	$path = "$file_path/file/polling/$pollingAttFolder";
	$lo = new libfiletable("", $path, 0, 0, "");
	$files = $lo->files;
}

### Retrieve Result
$letterAry = range('A', 'Z');
$countAry = $li->returnResult($PollingID);

$isAnonymous = $ia=="" ? $li->RecordType : $ia;


function handlingSpecialCharDisplay($str){
	$str = str_replace("&#039;", "\'", $str);
	$str = str_replace("&quot;", "\"", $str);
	$str = str_replace("&amp;", "&", $str);
	return $str;
}

function getCountValue($parOption){
	global $countAry, $li;
	
	$countValue = $li->getCount($parOption, $countAry);
	return $countValue;
}

function getPieChartData(){
	global $countValue, $li, $letterAry;
	
	$x .= "{name: '";
	$x .= addslashes($li->{Answer.$letterAry[0]});
	$x .= "', y: ";
	$x .= getCountValue($letterAry[0]);
	$x .= ", sliced: true,selected: true},";
	
	for ($i=1; $i<=25; $i++) {
		if ($li->{Answer.$letterAry[$i]}=="") {
		} else {
			$x .= "{name: '";
			$x .= addslashes($li->{Answer.$letterAry[$i]});
			$x .= "', y: ";
			$x .= getCountValue($letterAry[$i]);
			$x .= "}";
			if ($li->{Answer.$letterAry[($i+1)]}=="") {
			} else {
				$x .= ",";
			}
		}
	}
	
	return $x;
}

function getBarChartOption(){
	global $countValue, $li, $letterAry;
	
	for ($i=0; $i<=25; $i++) {
		if ($li->{Answer.$letterAry[$i]}=="") { } 
		else {
			$x .= "'";
			$x .= addslashes($li->{Answer.$letterAry[$i]});
			$x .= "'";
			if ($li->{Answer.$letterAry[($i+1)]}=="") {
			} else {
				$x .= ",";
			}
		}
	}
	
	return $x;
}

function getBarChartData(){
	global $countValue, $li, $letterAry;
	
	for ($i=0; $i<=25; $i++) {
		if ($li->{Answer.$letterAry[$i]}=="") { }
		else {
			$x .= getCountValue($letterAry[$i]);
			if ($li->{Answer.$letterAry[($i+1)]}=="") {
			} else {
				$x .= ",";
			}
		}
	}
	return $x;
}

// Call handlingSpecialCharDisplay() to prevent incorrect display in HighCharts
$PieChartData = handlingSpecialCharDisplay(getPieChartData());
$option = handlingSpecialCharDisplay(getBarChartOption());
$BarChartData = handlingSpecialCharDisplay(getBarChartData());

// hide pie chart if no signed polling
$showPieChart = true;
if(count((array)$countAry)==0)
	$showPieChart = false;

/*
$x = "";
$x .= "<table width='95%' border='01' cellpadding='2' cellspacing='1'>\n";
$x .= "<tr><td colspan=3><br></td></tr>\n";
$x .= ($li->AnswerA=="") ? "" : $li->countResult($li->AnswerA, "A", $li->Result);
$x .= ($li->AnswerB=="") ? "" : $li->countResult($li->AnswerB, "B", $li->Result);
$x .= ($li->AnswerC=="") ? "" : $li->countResult($li->AnswerC, "C", $li->Result);
$x .= ($li->AnswerD=="") ? "" : $li->countResult($li->AnswerD, "D", $li->Result);
$x .= ($li->AnswerE=="") ? "" : $li->countResult($li->AnswerE, "E", $li->Result);
$x .= ($li->AnswerF=="") ? "" : $li->countResult($li->AnswerF, "F", $li->Result);
$x .= ($li->AnswerG=="") ? "" : $li->countResult($li->AnswerG, "G", $li->Result);
$x .= ($li->AnswerH=="") ? "" : $li->countResult($li->AnswerH, "H", $li->Result);
$x .= ($li->AnswerI=="") ? "" : $li->countResult($li->AnswerI, "I", $li->Result);
$x .= ($li->AnswerJ=="") ? "" : $li->countResult($li->AnswerJ, "J", $li->Result);
$x .= ($li->Reference=="") ? "" : "<tr><td colspan=3 align=right><a href=javascript:newWindow('".$li->Reference."',5)>$i_PollingReference</a></td></tr>\n";
$x .= "</table>\n";
    */


?>


<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<?php
if (empty($isAnonymous) || !$sys_custom['Polling_Anonymous']) echo $linterface->GET_LNK_EXPORT("javascript:document.export_form.submit();","","","","",0);
else echo "<br>";
?>

<form name="form1" action="edit.php" method="post">

<table class="form_table_v30">

<tr valign="top">
	<td class="field_title"><?=$i_PollingTopic?></td>
	<td><?=intranet_wordwrap($li->Question,30,"\n",1)?> (<?=sizeof($countAry)?>)</td>
</tr>
	<tr valign="top">
	<td class="field_title"><?=$i_PollingPeriod?></td>
	<td><?=$li->DateStart?> ~ <?=$li->DateEnd?></td>
</tr>
<tr valign="top">
	<td class="field_title"><?=$i_PollingDateRelease?></td>
	<td><?=($li->DateRelease==""? $li->DateStart: $li->DateRelease)?></td>
</tr>

<? if(!empty($files)) {?>
<tr valign='top'>
	<td class='field_title'><?=$i_Notice_Attachment?></td>
	<td>
	<? 
	foreach($files as $k=>$d) {
		$this_path = $path . "/".$d[0];
		//$this_path = rawurlencode($this_path);
		echo "<img src=$image_path/$LAYOUT_SKIN/index/whatnews/icon_attachment.gif hspace=2 vspace=2 border=0 align=absmiddle><a href='/home/download_attachment.php?target_e=".getEncryptedText($this_path)."' class='indexpoplink'>$d[0]</a><br>";
	
	 } ?>
	
	</td>
</tr>   
<? } ?>

</table>

<!-- OLD BAR CHART
<table align="center" width="80%" border="0" cellpadding="0" cellspacing="0"  align="center">
	<tr>
		<td ><?=$li->displayResultChart()?></td>
	</tr>
</table>
-->


<?
######## Highcharts START ########
?>

<script src="/templates/highchart/highcharts.js"></script>

<table align="center" width="80%" border="0" cellpadding="0" cellspacing="0"  align="center">
	<tr>
		<td><div align="center" style="width:100%;" id="barchart">		
<script language="javascript">
var data = [<?php echo $BarChartData?>];
var dataSum = 0;
for (var i=0;i < data.length;i++) {
    dataSum += data[i]
}    

var chart = new Highcharts.Chart({
    chart: {
        renderTo:'barchart',
        type:'bar'
    },
    title:{
        text:''
    },
    credits:{enabled:false},
    legend:{
    },
    plotOptions: {
        series: {
            shadow:false,
            borderWidth:0,
            dataLabels:{
                enabled:true,
                formatter:function() {
                    var percentage = (this.y / dataSum) * 100;
                    return Highcharts.numberFormat(percentage,1,'.') + '% (' + this.y + ')';
                }
            }
        }
    },
    xAxis:{
        lineColor:'#999',
        lineWidth:1,
        tickColor:'#666',
        tickLength:3,
        categories: [<?php echo $option?>],
        title:{
            text:''
        },
    },
    yAxis:{
        lineColor:'#999',
        lineWidth:1,
        tickColor:'#666',
        tickWidth:1,
        tickLength:3,
        gridLineColor:'#ddd',
        title:{
            text:'',
            rotation:0,
            margin:50,
        },
        labels: {
            formatter:function() {
                var percentage = (this.value / dataSum) * 100;
                return Highcharts.numberFormat(percentage,0,',') + '%';
            }
        }
    },    
    series: [{
    	name: ' ',
    	colorByPoint: true,
        data: data
    }]
});
$('.highcharts-legend').hide();
</script>
		</div></td>
	</tr>

<? if($showPieChart){ ?>
	<tr>
		<td><div align="center" style="width:100%;" id="piechart">
<script language="javascript">
var loadingImage = new Image();
loadingImage.src = "/images/<?=$LAYOUT_SKIN?>/indicator.gif";

$(function () {
    $('#piechart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
    	credits:{enabled:false},
        tooltip: {
            pointFormat: '<b>{point.percentage:.1f}%</b> ({point.y})'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '{point.name}: {point.percentage:.1f}% ({point.y})',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{colorByPoint: true, data: [<?php echo $PieChartData?>]}]
    });
});

</script>
		</div></td>
	</tr>
<? } ?>

</table>

<?
######## Highcharts END ########
?>					

<div class="edit_bottom_v30">
	<p class="spacer"></p>
	<?=$edit_btn?> 
	<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back();","cancelbtn") ?>
	<p class="spacer"></p>
</div>			

<input type="hidden" name="PollingID[]" value="<?php echo $li->PollingID; ?>">
</form>

<form action="export_update.php" method="post" name="export_form">
<input type="hidden" name="PollingID" value="<?=$li->PollingID?>">
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
