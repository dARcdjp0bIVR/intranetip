<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libcampusquota.php");

intranet_auth();
intranet_opendb();

$hasAccessRight =  $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit; 
}

echo "<META http-equiv=Content-Type content='text/html; charset=utf-8'>";

$no_file = 5;
$path = "$file_path/file/polling/$folder";
if (!is_dir($path))
{
    $path = $path."tmp";
}

$li = new libfilesystem();
$li->folder_new($path);

# remove user deleted files

$lremove = new libfiletable("", $path, 0, 0, "");
$files = $lremove->files;
$attachStr=stripslashes($attachStr);
while (list($key, $value) = each($files)) {
     if(!strstr($attachStr,$files[$key][0])){
          $li->file_remove($path."/".$files[$key][0]);
     }
}

for($i=0;$i<$no_file;$i++){
     $loc = ${"userfile".$i};
     $file = stripslashes(${"hidden_userfile_name$i"});     
     $des = "$path/$file";
     if($loc=="none"){
     } else {
          if(strpos($file, ".")==0){
          }else{
               $li->lfs_copy($loc, $des);
          }
     }
}

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
?>


<script language="JavaScript1.2">
par = opener.window;
obj = opener.window.document.form1.elements["Attachment[]"];

par.checkOptionClear(obj);
<?php while (list($key, $value) = each($files)) echo "par.checkOptionAdd(obj, \"".$files[$key][0]." (".ceil($files[$key][1]/1000)."Kb".")\", \"".$files[$key][0]."\");\n"; ?>
par.checkOptionAdd(obj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
self.close();
</script>
