<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();

$hasAccessRight =  $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit; 
}

$li = new libdb();

$sql = "DELETE FROM INTRANET_GROUPPOLLING WHERE PollingID IN (".implode(",", $PollingID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_POLLINGRESULT WHERE PollingID IN (".implode(",", $PollingID).")";
$li->db_db_query($sql);

$sql = "DELETE FROM INTRANET_POLLING WHERE PollingID IN (".implode(",", $PollingID).")";
$li->db_db_query($sql);

intranet_closedb();
header("Location: index.php?filter=$filter&order=$order&field=$field&xmsg=delete&pageNo=$pageNo");
?>