<?php
# using: Bill

########## Change Log [Start] ############
#
#   Date:   2019-05-17 (Bill)
#           Change hidden input field name from "OriginalGroupID" to "OriginalGroupCode"
#
#   Date:   2019-05-02 (Bill)
#           prevent SQL Injection + Command Injection
#
#	Date:	2016-05-12 (Bill)
#			use session_unregister_intranet() for PHP 5.4
#
#	Date:	2011-04-13	YatWoon
#			add choice to Z 
#
#	Date:	2010-04-27 [YatWoon]
#			can update past polling (but only can update due date)
#
########## Change Log [End] ############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpolling.php");

intranet_auth();
intranet_opendb();
 
$hasAccessRight = $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit; 
}

### Handle SQL Injection + XSS [START]
$PollingID = IntegerSafe($PollingID);
if(isset($GroupID)) {
    $GroupID = IntegerSafe($GroupID);
}

$RecordStatus = IntegerSafe($RecordStatus);
$isAnonymous = IntegerSafe($isAnonymous);
### Handle SQL Injection + XSS [END]

//$li = new libdb();

$li = new libpolling($PollingID);
$editable = $li->isPollingEditable($PollingID);

$Question = intranet_htmlspecialchars(trim($Question));
for($i=65; $i<=90; $i++)
{
	${"Answer".chr($i)} = intranet_htmlspecialchars(trim(${"Answer".chr($i)}));
}
/*
$AnswerA = intranet_htmlspecialchars(trim($AnswerA));
$AnswerB = intranet_htmlspecialchars(trim($AnswerB));
$AnswerC = intranet_htmlspecialchars(trim($AnswerC));
$AnswerD = intranet_htmlspecialchars(trim($AnswerD));
$AnswerE = intranet_htmlspecialchars(trim($AnswerE));
$AnswerF = intranet_htmlspecialchars(trim($AnswerF));
$AnswerG = intranet_htmlspecialchars(trim($AnswerG));
$AnswerH = intranet_htmlspecialchars(trim($AnswerH));
$AnswerI = intranet_htmlspecialchars(trim($AnswerI));
$AnswerJ = intranet_htmlspecialchars(trim($AnswerJ));
*/

$Reference = intranet_htmlspecialchars(trim($Reference));
$Reference = ($Reference=="http://") ? "" : $Reference;
$DateStart = intranet_htmlspecialchars(trim($DateStart));
$DateEnd = intranet_htmlspecialchars(trim($DateEnd));
$DateRelease = intranet_htmlspecialchars(trim($DateRelease));
$DateRelease = ($DateRelease == "" || !intranet_validateDate($DateRelease))? $DateEnd : $DateRelease;
$isAnonymous = intranet_htmlspecialchars(trim($isAnonymous));

# Check Date
$start_stamp = strtotime($DateStart);
$due_stamp = strtotime($DateEnd);

if($UpdatePastPolling) 
{
    if(!intranet_validateDate($DateEnd))
    {
        $msg = "UpdateUnsuccess";
    }
    else
    {
    	$sql = "UPDATE INTRANET_POLLING SET
    	            DateEnd = '$DateEnd',
    				DateRelease = '$DateRelease',
    	            DateModified = NOW(),
    	            RecordType = '$isAnonymous'
    	        WHERE
    	            PollingID = '$PollingID' ";
    	$li->db_db_query($sql);
    	
    	$msg = "UpdateSuccess";
    }
}
else
{
    if ($li->compareDate($due_stamp, $start_stamp) < 0 || !intranet_validateDate($DateStart) || !intranet_validateDate($DateEnd))
	{
	    $msg = "UpdateUnsuccess";
	}
	else
	{
		$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
		$AttachmentStr = stripslashes($AttachmentStr);
		if(!empty($AttachmentStr))
		{
			$lu = new libfilesystem();
			
			$pollingAttFolder = OsCommandSafe($pollingAttFolder);
			$path = "$file_path/file/polling/".$pollingAttFolder;
			if (is_dir($path."tmp") && !is_dir($path))
			{
			    $command ="mv $path"."tmp $path";
			    exec($command);
			}
			
			$lo = new libfiletable("", $path, 0, 0, "");
			$files = $lo->files;
			while (list($key, $value) = each($files)) {
			     if(!strstr($AttachmentStr,$files[$key][0])) {
			          $lu->file_remove($path."/".$files[$key][0]);
			     }
			}
			
			$IsAttachment = (isset($Attachment)) ? 1 : 0;
			$Attachment = $pollingAttFolder;
		}
		
		for($i=65; $i<=90; $i++)
		{
			$fields .= "Answer". chr($i)." = '${"Answer".chr($i)}',";
			//$fields .= ",Answer".chr($i);
		}
		/*
		AnswerA = '$AnswerA',
		               AnswerB = '$AnswerB',
		               AnswerC = '$AnswerC',
		               AnswerD = '$AnswerD',
		               AnswerE = '$AnswerE',
		               AnswerF = '$AnswerF',
		               AnswerG = '$AnswerG',
		               AnswerH = '$AnswerH',
		               AnswerI = '$AnswerI',
		               AnswerJ = '$AnswerJ',
		*/
		
		if ($editable || !$sys_custom['Polling_Anonymous'])
		{
			$sql = "UPDATE INTRANET_POLLING SET
						Question = '$Question',
						$fields
						Reference = '$Reference',
						DateStart = '$DateStart',
						DateEnd = '$DateEnd',
						DateRelease = '$DateRelease',
						RecordStatus = '$RecordStatus',
						DateModified = NOW(),
						Attachment = '$Attachment'
					WHERE
						PollingID = '$PollingID' ";
		}
		else
		{
			$sql = "UPDATE INTRANET_POLLING SET
						Question = '$Question',
						$fields
						Reference = '$Reference',
						RecordType ='$isAnonymous',
						DateStart = '$DateStart',
						DateEnd = '$DateEnd',
						DateRelease = '$DateRelease',
						RecordStatus = '$RecordStatus',
						DateModified = NOW(),
						Attachment = '$Attachment'
					WHERE
						PollingID = '$PollingID' ";
		}
		$li->db_db_query($sql);
		
		$msg = "UpdateSuccess";
	}
}

if ($msg == "UpdateSuccess" && $sys_custom['Polling_User_Group_Option'])
{
	//if ($editable)         # Change group target
	
	# Remove all group first
	$sql = "DELETE FROM INTRANET_GROUPPOLLING WHERE PollingID = '$PollingID'";
	$li->db_db_query($sql);
	
	if (sizeof($GroupID) && $PollingID)
	{
		$group_polling_field = "";
		$delimiter = "";
		for ($i=0; $i<sizeof($GroupID); $i++)
		{
			$group_polling_field .= $delimiter." ('".$GroupID[$i]."', '".$PollingID."', '".$RecordStatus."', NOW(), NOW())";
			$delimiter = ",";
		}
		
		$sql = "INSERT INTO INTRANET_GROUPPOLLING (GroupID, PollingID, RecordStatus, DateInput, DateModified) VALUES $group_polling_field";
		$li->db_db_query($sql);
	}
	
	### Clear removed group answer
	if(!$isAnonymous)
	{
		$OriginalGroupIDArr = unserialize(urldecode($_POST['OriginalGroupCode']));
		$OriginalGroupIDArr = IntegerSafe($OriginalGroupIDArr);
		// $needClearGroupIDArr = array_diff($OriginalGroupIDArr,array_intersect((array)$_POST['GroupID'],$OriginalGroupIDArr));
		
		$OriginalGroupIDRemainArr = array_intersect((array)$GroupID, (array)$OriginalGroupIDArr);
		$needClearGroupIDArr = array_diff((array)$OriginalGroupIDArr, (array)$OriginalGroupIDRemainArr);
		if(!empty($needClearGroupIDArr))
		{
			$sql = "SELECT UserID FROM INTRANET_USERGROUP WHERE GroupID IN ('".implode("','",(array)$needClearGroupIDArr)."')";
			$UserIDArr = $li->returnVector($sql);
			if(count($UserIDArr) > 0 && $PollingID > 0) {
			    $sql = "Delete From INTRANET_POLLINGRESULT Where UserID IN ('".implode("','",(array)$UserIDArr)."') AND PollingID = '".$PollingID."' ";
				$li->db_db_query($sql);
			}
		}
	}
}

//session_unregister("pollingAttFolder");
session_unregister_intranet("pollingAttFolder");

intranet_closedb();
header("Location: index.php?xmsg=$msg");
?>