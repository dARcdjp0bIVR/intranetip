<?php
# using: Bill

###################################
#
#   Date:   2019-05-02 (Bill)
#           prevent SQL Injection + Command Injection
#
#	Date:	2016-05-12 (Bill)
#			use session_unregister_intranet() for PHP 5.4
#
#	Date:	2011-04-13	YatWoon
#			add choice to Z 
#
#	Date:	2010-12-28	YatWoon
#			- attachment
#
###################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
if(isset($GroupID)) {
    $GroupID = IntegerSafe($GroupID);
}

$RecordStatus = IntegerSafe($RecordStatus);
$isAnonymous = IntegerSafe($isAnonymous);
### Handle SQL Injection + XSS [END]

$li = new libdb();
$lf = new libfilesystem();

$hasAccessRight = $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$AttachmentStr = (sizeof($Attachment)==0) ? "" : implode(",", $Attachment);
$AttachmentStr = stripslashes($AttachmentStr);

$pollingAttFolder = OsCommandSafe($pollingAttFolder);
$path = "$file_path/file/polling/".$pollingAttFolder;

$lf = new libfilesystem();
$command ="mv $path"."tmp $path";
exec($command);

$lo = new libfiletable("", $path, 0, 0, "");
$files = $lo->files;
while (list($key, $value) = each($files)) {
    if(!strstr($AttachmentStr, $files[$key][0])){
        $lf->file_remove($path."/".$files[$key][0]);
    }
}
$IsAttachment = (isset($Attachment))? 1 : 0;
$Attachment = $IsAttachment? $pollingAttFolder : "";

$Question = intranet_htmlspecialchars(trim($Question));

/*
$AnswerA = intranet_htmlspecialchars(trim($AnswerA));
$AnswerB = intranet_htmlspecialchars(trim($AnswerB));
$AnswerC = intranet_htmlspecialchars(trim($AnswerC));
$AnswerD = intranet_htmlspecialchars(trim($AnswerD));
$AnswerE = intranet_htmlspecialchars(trim($AnswerE));
$AnswerF = intranet_htmlspecialchars(trim($AnswerF));
$AnswerG = intranet_htmlspecialchars(trim($AnswerG));
$AnswerH = intranet_htmlspecialchars(trim($AnswerH));
$AnswerI = intranet_htmlspecialchars(trim($AnswerI));
$AnswerJ = intranet_htmlspecialchars(trim($AnswerJ));
*/

for($i=65; $i<=90; $i++)
{
	${"Answer".chr($i)} = intranet_htmlspecialchars(trim(${"Answer".chr($i)}));
}

$Reference = intranet_htmlspecialchars(trim($Reference));
$Reference = ($Reference=="http://") ? "" : $Reference;
$DateStart = intranet_htmlspecialchars(trim($DateStart));
$DateEnd = intranet_htmlspecialchars(trim($DateEnd));
$DateRelease = intranet_htmlspecialchars(trim($DateRelease));
$isAnonymous = intranet_htmlspecialchars(trim($isAnonymous));

# Check date
# Start date >= curdate() && Start date <= due date
$start_stamp = strtotime($DateStart);
$due_stamp = strtotime($DateEnd);

$today = mktime(0,0,0,date("m"),date("d"),date("Y"));
if ($li->compareDate($start_stamp, $today) < 0 || $li->compareDate($due_stamp, $start_stamp) < 0 || !intranet_validateDate($DateStart) || !intranet_validateDate($DateEnd))
{
    $msg = "AddUnsuccess";
}
else
{
    if ($DateRelease == "" || !intranet_validateDate($DateRelease))
    {
        $DateRelease = $DateEnd;
    }
    
    for($i=65; $i<=90; $i++)
	{
		$fields .= ",Answer".chr($i);
		$answer_fields .= ",'" . ${"Answer".chr($i)} ."'";
	}
	
    $sql = "INSERT INTO INTRANET_POLLING (Question $fields, Reference, RecordType, DateStart, DateEnd, DateRelease, RecordStatus, DateInput, DateModified, Attachment) 
    		VALUES ('$Question' $answer_fields, '$Reference', '$isAnonymous', '$DateStart', '$DateEnd', '$DateRelease', '$RecordStatus', now(), now(), '$Attachment')";
    $li->db_db_query($sql);
    $PollingID = $li->db_insert_id();
    
    $msg = "AddSuccess";
    
    if (sizeof($GroupID) && $PollingID && $sys_custom['Polling_User_Group_Option'])
    {
    	$group_polling_field = "";
    	$delimiter = "";
    	for ($i=0; $i<sizeof($GroupID); $i++)
    	{
    		$group_polling_field .= $delimiter." ('".$GroupID[$i]."', '".$PollingID."', '".$RecordStatus."', NOW(), NOW())";
    		$delimiter = ",";
    	}
    	
    	$sql = "INSERT INTO INTRANET_GROUPPOLLING (GroupID, PollingID, RecordStatus, DateInput, DateModified) VALUES $group_polling_field";
    	$li->db_db_query($sql);
    }
}

//session_unregister("pollingAttFolder");
session_unregister_intranet("pollingAttFolder");

intranet_closedb();
header("Location: index.php?xmsg=$msg");
?>