<?php
# using: YAT

########## Change Log [Start] #############
#
#	Date:	2013-12-03	YatWoon
#			Fixed: hide parent user in export result [Case#2013-1129-0909-22066]
#
#	Date:	2012-07-24	YatWoon
#			Fixed: missing to define temp_userid in array, so that the export data is shifted
#
########## Change Log [End] #############

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libpolling.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_opendb();

$hasAccessRight =  $_SESSION["SSV_USER_ACCESS"]["eAdmin-ePolling"];
if(!$hasAccessRight)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	 $laccessright = new libaccessright();
	 $laccessright->NO_ACCESS_RIGHT_REDIRECT();
	 exit; 
}
$PollingID = IntegerSafe($PollingID);
$li = new libpolling($PollingID);
$lexport = new libexporttext();

$isAnonymous = $ia=="" ? $li->RecordType : $ia;

	# title
	$exportColumn = array();
	$exportColumn[] = array($i_adminmenu_im_polling . ":", $li->Question);
	$exportColumn[] = array($i_PollingPeriod . ":", $li->DateStart." ~ ".$li->DateEnd);
	
	$exportColumn[] = array("");
	# Header
	if ($isAnonymous != 1) {
		$exportColumn[] = array("English Name","Chinese Name","Class","Class Number","Identity","Answer","Date");
	} else {
		$exportColumn[] = array("Answer","Date");
	}
	
	$ExportArr = array();
	$row = $li->returnResultDetails($PollingID);
	for($i=0; $i<sizeof($row); $i++)
	{
		list($tmp_userid, $tmp_ename, $tmp_cname, $tmp_class, $tmp_classnumber, $tmp_recordtype, $tmp_answer, $tmp_date) = $row[$i];
		
		$thisRow = array();
		
		if(!($tmp_recordtype==1 || $tmp_recordtype==2))
			continue;
			
		switch($tmp_recordtype)
		{
			case 1: $rt = $i_identity_teachstaff; break;
			case 2: $rt = $i_identity_student; break;
			case 3: $rt = $i_identity_parent; break;
		}
		$an = $li->{Answer."$tmp_answer"};
		if (!empty($an)) {
			if ($isAnonymous != 1) {
				$thisRow[] = str_replace("\t", " ", $tmp_ename);
				$thisRow[] = str_replace("\t", " ", $tmp_cname);
				$thisRow[] = str_replace("\t", " ", $tmp_class);
				$thisRow[] = str_replace("\t", " ", $tmp_classnumber);
				$thisRow[] = str_replace("\t", " ", $rt);
			}
			$thisRow[] = str_replace("\t", " ", $an);
			$thisRow[] = $tmp_date;
			$ExportArr[] = $thisRow;
		}
	}
	
	$filename = "PollingResult.csv";
	$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0);
	intranet_closedb();
	
	$lexport->EXPORT_FILE($filename, $export_content);
?>