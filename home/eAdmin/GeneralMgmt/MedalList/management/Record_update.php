<?php
/*
 * 	Log:
 * 	Date:	2018-03-12 Anna
 *          Created this file
 */
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$libmedallist = new libmedallist();

// if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
// 	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

// $CategoryArr['CategoryName'] = $CategoryName;
// $CategoryArr['DisplayOrder'] = $DisplayOrder;
// $CategoryArr['OleCategoryID'] = ($_POST['OleCategoryID']=='')? 0 : $_POST['OleCategoryID'];
// $CategoryArr['CategoryTypeID'] = ($_POST['CategoryTypeID']=='')? 0 : $_POST['CategoryTypeID'];

// $successAry = array();
// $libenroll->Start_Trans();

$RecordArr['Year'] = $_POST['Year'];
$RecordArr['LocationChi'] = $_POST['LocationChi'];
$RecordArr['LocationEng'] = $_POST['LocationEng'];
$RecordArr['GameID'] = $_POST['GameID'];
$EventName = $_POST['Event'];
$SportName = $_POST['Sport'];
$RecordArr['NameOfAthleteChi'] = $_POST['NameOfAthleteChi'];
$RecordArr['NameOfAthleteEng'] = $_POST['NameOfAthleteEng'];
$RecordArr['MedalID'] = $_POST['MedalID'];
$RecordID = $_POST['RecordID'];
$RecordArr['WebsiteCode']= $_POST['WebsiteCode'];


//get event id by event name
$eventConds = "AND EventChiName = '$EventName' OR EventEngName='$EventName'";
$EventID  = $libmedallist->getEventIDByCondition($eventConds);

//no mapping event name. insert new event
if($EventID == ''){

    $EventArr['EventChiName'] = $EventName;
    $EventArr['EventEngName'] = $EventName;
    $EventID  = $libmedallist->addNewEvent($EventArr,$getID ='1');
}
$RecordArr['EventID'] = $EventID;

//get sport id by sport name
$sportConds = "AND SportChiName = '$SportName' OR SportEngName='$SportName'";
$SportID  = $libmedallist->getSportIDByCondition($sportConds);

//no mapping sport name. insert new sport
if($SportID== ''){
 
    $SportArr['SportChiName']= $SportName;
    $SportArr['SportEngName']= $SportName;
    $SportID= $libmedallist->addNewSport($SportArr);
}
$RecordArr['SportID'] = $SportID;




if ($RecordID== "") {
	
    $successAry['add'] = $libmedallist->addNewMedalListRecord($RecordArr);
	
} else {
    $RecordArr['RecordID'] = $RecordID;
	
	
    $successAry['edit'] = $libmedallist->editMedalListRecord($RecordArr);
}

if (in_array(false, $successAry)) {
//	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'UpdateUnsuccess'; 
}
else {
//	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: index.php?ReturnMsgKey=$ReturnMsgKey");
?>