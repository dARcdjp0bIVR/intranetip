<?php
/* 
 * Editing by  
 * 
 * Note: page called for using jquery.autocomplete.js only 
 * 
 * @Param	q : 	search string
 * 			field:	field name for which to retireve
 * 

 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libmedallist = new libmedallist();


########################################################################################################


## Get Data
$q = (isset($q) && $q != '') ? $q : '';

$field = (isset($field) && $field != '') ? $field : '';


## Init
$x = '';


## Use Library

// $admin_right = $_SESSION['LIBMS']['admin']['current_right']['admin'];

// if (!$admin_right['book management'] && !$_SESSION['SSV_USER_ACCESS']['eAdmin-LibraryMgmtSystem']  )
// {
// 	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
// 	$laccessright = new libaccessright();
// 	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
// 	exit;
// }


## Maing
if (($q != '') && ($field != '')) {
	$limit = 100;
	$validStr = false;
	$extraFilter = '';
	# search with input keyword
	switch ($field) {
		case 'Sport':
			$table = 'SFOC_SPORTS_TYPE';
			$Field1 = 'SportChiName';
			$Field2 = 'SportEngName';
			$selectField = Get_Lang_Selection($Field1,$Field2);
			$orderBy = $field;
			$validStr = true;
			$extraFilter = " OR ".$Field2." LIKE '%".$q."%'
                             AND Is_Deleted = 0";

			break;
		case 'Event':
		    $table = 'SFOC_EVENTS_SETTING';
		    $Field1 = 'EventChiName';
		    $Field2 = 'EventEngName';
		    $selectField = Get_Lang_Selection($Field1,$Field2);
		    $orderBy = $Field2;
		    $validStr = true;
		    $extraFilter = " OR ".$Field2." LIKE '%".$q."%'";
		    break;
	}
	
	if ($validStr) {
	    $filter = " where ".$Field1." like '%".$q."%'".$extraFilter;
		$sql = "select ".$selectField." from ".$table.$filter;
		$result = $libmedallist->returnArray($sql,null,2);	// numeric array only
		
		if(!empty($result)){
			foreach ($result as $row){
				$res = intranet_undo_htmlspecialchars($row[0]);
				$x .= $res."|".$res."\n";
			}
		}
	}
}

echo $x;