<?php 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libmedallist = new libmedallist();


$successAry = array();


$RecordIDAry = $RecordID;

for ($i = 0; $i < sizeof($RecordIDAry); $i++) {
    $successAry['delete'][] = $libmedallist->deleteMedalListRecord($RecordIDAry[$i]);
}

if (in_array(false, $successAry)) {
   
    $ReturnMsgKey = 'DeleteUnsuccess';
}
else {

    $ReturnMsgKey = 'DeleteSuccess';
}

// intranet_closedb();
header("Location: index.php?ReturnMsgKey=$ReturnMsgKey");
?>