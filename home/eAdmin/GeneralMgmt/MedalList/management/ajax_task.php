<?php 
################## Change Log [Start] #################
#   using  
#   Date:   2019-01-10 Anna
#           Added WebsiteCode
#
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");

intranet_auth();
intranet_opendb();


if($task == 'validateRecordImport'){
	//$webSAMSCheckingArr = array_filter(Get_Array_By_Key($data,'2'));	
	### Include js libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	
	$limport = new libimporttext();
	$libmedallist = new libmedallist();
	
	$targetFilePath = standardizeFormPostValue($_GET['targetFilePath']);
	
	### Get Data from the csv file
	$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
	$csvColName = array_shift($csvData);
	$numOfData = count($csvData);

	$errorCount = 0;
	$successCount = 0;

	##verify imported data
	for ($i=0; $i<$numOfData; $i++) {
		 
		$_year = $csvData[$i][0]; 
		$_locationEng = $csvData[$i][1]; 
		$_locationChi = $csvData[$i][2]; 
		$_gamesEng = $csvData[$i][3]; 
		$_gamesChi = $csvData[$i][4]; 
		$_eventEng = $csvData[$i][5];
		$_eventChi = $csvData[$i][6]; 
		$_sportEng = safeString($csvData[$i][7]);
		$_sportChi = safeString($csvData[$i][8]); 
		$_EngName = safeString($csvData[$i][9]);
		$_ChiName = safeString($csvData[$i][10]); 
		$_Medal = $csvData[$i][11];
		$_websiteCode = $csvData[$i][12];
		
		if($_Medal=='G'){
		    $_Medal = '1';
		}else if($_Medal == 'S'){
		    $_Medal = '2';
        }else{
            $_Medal = '3';
        }
        
		// find blank space
		if($_year == "" ){
		    $errorArr[$i][] = $Lang['SFOC']['MedalList']['emptyYear'];
// 		    $error_data[] = array($i,EMPTY_YEAR,$csvData[$i]);
		 }  	
		 if($_locationEng=="" || $_locationChi == ""){
		     $errorArr[$i][] =  $Lang['SFOC']['MedalList']['emptyLocation'];	 	
// 		     $error_data[] = array($i,EMPTY_LOCATION,$csvData[$i]);
		 }
		 if($_gamesEng=="" || $_gamesChi==""){
		     $errorArr[$i][] =  $Lang['SFOC']['MedalList']['emptyGame'];
// 		     $error_data[] = array($i,EMPTY_GAME,$csvData[$i]);
		 }
		 if($_eventEng=="" || $_eventChi==""){
		     $errorArr[$i][] = $Lang['SFOC']['MedalList']['emptyEvent'];
// 		     $error_data[] = array($i,EMPTY_EVENT,$csvData[$i]);
		 }
		 if($_sportEng=="" || $_sportChi==""){
		     $errorArr[$i][] = $Lang['SFOC']['MedalList']['emptySport'];
// 		     $error_data[] = array($i,EMPTY_SPORT,$csvData[$i]);
		 }
		 if($_EngName=="" || $_ChiName==""){
		     $errorArr[$i][] = $Lang['SFOC']['MedalList']['emptyName'];
// 		     $error_data[] = array($i,EMPTY_NAME,$csvData[$i]);
		 }
		 if($_Medal==""){
		     $errorArr[$i][] = $Lang['SFOC']['MedalList']['emptyMedal'];
// 		     $error_data[] = array($i,EMPTY_MEDAL,$csvData[$i]);
		 }
		 
		 
		
		
		 $gameConds = "AND GameChiName = '".$_gamesChi."' AND GameEngName= '".$_gamesEng."'";
		 $GameID  = $libmedallist->getGameInfoByConds($gameConds);
		 if($GameID==""){
		     $errorArr[$i][] = $Lang['SFOC']['MedalList']['noMappingGame'];
// 		     $error_data[] = array($i,NO_MAPPING_GAME,$csvData[$i]);
		 }
		 
		 $eventConds = "AND EventChiName = '".$_eventChi."' AND EventEngName='".$_eventEng."'";
		 $EventID  = $libmedallist->getEventIDByCondition($eventConds);
		 
		 if($EventID==""){
		     $errorArr[$i][] =  $Lang['SFOC']['MedalList']['noMappingEvent'];
// 		     $error_data[] = array($i,NO_MAPPING_EVENT,$csvData[$i]);
		 }
		 
		 $sportConds = "AND SportChiName = '".$_sportChi."' AND SportEngName='".$_sportEng."'";
		 $SportID  = $libmedallist->getSportIDByCondition($sportConds);
		 if($SportID==""){
		     $errorArr[$i][] =  $Lang['SFOC']['MedalList']['noMappingSport'];
// 		     $error_data[] = array($i,NO_MAPPING_SPORT,$csvData[$i]);
		 }

 		 ## Insert Data in Temp Table
		 $TempValueArr[$i] = '("'.($i+1).'","'.$_year.'","'.$_locationChi.'","'.$_locationEng.'","'.$GameID.'","'.$EventID.'","'.$SportID.'","'.$_ChiName.'","'.$_EngName.'","'.$_Medal .'","'.$_SESSION['UserID'] .'",now(),"'.$_websiteCode.'")';
	
		### Update Processing Display
		Flush_Screen_Display(1);
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($i + 1).'");';
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
	}	

	for ($i=0;$i<sizeof($csvData);$i++){
	    if ($errorArr[$i]==""){
	        $successCount++;
	    }
	}
	$x = '';
	if (sizeof($errorArr)>0)
    {
        $x .= '<table class="common_table_list_v30 view_table_list_v30">';
 //       $x .= "<tr><td>".$ec_guide['import_error_row']."</td><td>".$ec_guide['import_error_reason']."</td><td>".$ec_guide['import_error_detail']."</td></tr>\n";        
        $x .= '<tr>';
            $x .= '<th>#</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['Year'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['LocationChi'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['LocationEng'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['GameEng'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['GameChi'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['EventEng'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['Settings']['EventChi'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['SportEng'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['SportChi'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['NameOfAthleteEng'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['NameOfAthleteChi'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['Medal'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['WebsiteCode'].'</th>';
            $x .= '<th>'.$Lang['SFOC']['MedalList']['Remark'].'</th>';
        $x .= '</tr>';
        $x .= '<tbody>';

        foreach($errorArr as $i=>$ErrorAry)
    	{
    	
    	    $_year = $csvData[$i][0];
    	    $_locationEng = $csvData[$i][1];
    	    $_locationChi = $csvData[$i][2];
    	    $_gamesEng = $csvData[$i][3];
    	    $_gamesChi = $csvData[$i][4];
    	    $_eventEng = $csvData[$i][5];
    	    $_eventChi = $csvData[$i][6];
    	    $_sportEng = $csvData[$i][7];
    	    $_sportChi = $csvData[$i][8];
    	    $_EngName = $csvData[$i][9];
    	    $_ChiName = $csvData[$i][10];
    	    $_Medal = $csvData[$i][11];
    	    $_websiteCode = $csvData[$i][12];
    	    
    	    $x .= '<tr><td class="tabletext">'.($i+1).'</td>';
        	    $x .='<td class="tabletext">'.$_year.'</td>';
        	    $x .='<td class="tabletext">'.$_locationEng.'</td>';
        	    $x .='<td class="tabletext">'.$_locationChi.'</td>';   
        	    $x .='<td class="tabletext">'.$_gamesEng.'</td>';
        	    $x .='<td class="tabletext">'.$_gamesChi.'</td>';
        	    $x .='<td class="tabletext">'.$_eventEng.'</td>';
        	    $x .='<td class="tabletext">'.$_eventChi.'</td>';
        	    $x .='<td class="tabletext">'.$_sportEng.'</td>';
        	    $x .='<td class="tabletext">'.$_sportChi.'</td>';
        	    $x .='<td class="tabletext">'.$_EngName.'</td>';
        	    $x .='<td class="tabletext">'.$_ChiName.'</td>';
        	    $x .='<td class="tabletext">'.$_Medal.'</td>';
        	    $x .='<td class="tabletext">'.$_websiteCode.'</td>';
        	$x .='<td class="tabletext">'.implode('<br>',$ErrorAry).'</td></tr>';
    	}
    	$x .= '</tbody>';
    	$x .= '</table>';
    	$htmlAry['errorTbl'] = $x;  
    }
    # counting number of incorrect data
    $errorCount=count($errorArr);	
	### Display Record Error Table	//TODO
 	if($errorCount == 0){	    
        $libmedallist->insertMeadalListRecordTemp($TempValueArr);
    }
		 
  	$errorCountDisplay = ($errorCount > 0) ? "<font color=\"red\">".$errorCount."</font>" : $errorCount;
	
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.safeString($htmlAry['errorTbl']).'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$successCount.'\';';
	$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$errorCountDisplay.'\';';
	
	if ($errorCount == 0) {
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
		$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
	}
		
	$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}
intranet_closedb();

function safeString($str){
    $rs = str_replace('\'', '\\\'', $str);
    $rs = str_replace('\"', '\\\"', $rs);
    return $rs;
}
?>