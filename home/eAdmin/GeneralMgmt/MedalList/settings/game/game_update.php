<?php
/*
 * 	Log:
 * 	Date:	2018-03-12 Anna
 *          Created this file
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$libmedallist = new libmedallist();

// if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
// 	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

// $CategoryArr['CategoryName'] = $CategoryName;
// $CategoryArr['DisplayOrder'] = $DisplayOrder;
// $CategoryArr['OleCategoryID'] = ($_POST['OleCategoryID']=='')? 0 : $_POST['OleCategoryID'];
// $CategoryArr['CategoryTypeID'] = ($_POST['CategoryTypeID']=='')? 0 : $_POST['CategoryTypeID'];

// $successAry = array();
// $libenroll->Start_Trans();

$GameArr['GameChiName'] = $GameChiName;
$GameArr['GameEngName'] = $GameEngName;

if ($GameID== "") {
	
	$successAry['add'] = $libmedallist->addNewGame($GameArr);
	
} else {
    $GameArr['GameID'] = $GameID;
	
	
    $successAry['edit'] = $libmedallist->editGame($GameArr);
}

if (in_array(false, $successAry)) {
//	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'UpdateUnsuccess'; 
}
else {
//	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: index.php?ReturnMsgKey=$ReturnMsgKey");
?>