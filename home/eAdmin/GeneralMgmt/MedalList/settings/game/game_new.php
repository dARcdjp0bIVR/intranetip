<?php
/*
 * 	Log
 * 	Date:	2018-03-12 Anna
 *          Create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$libUser = new libuser($UserID);	
$linterface = new interface_html();
$libmedallist = new libmedallist();
	
	
// if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
// 		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
	$CurrentPage = "PageSysSettingGameSettings";
	$CurrentPageArr['MedalList'] = 1;
	
	$MODULE_OBJ = $libmedallist->GET_MODULE_OBJ_ARR();
	
// 	if ($libmedallist->hasAccessRight($_SESSION['UserID'])) {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $TAGS_OBJ[] = array($MedalListMenu['game'], "", 1);

        $linterface->LAYOUT_START();

		if (is_array($GameID)) {
		    $GameID= $GameID[0];
		}
		        
		$GameArr = $libmedallist->getGameInfoByID($GameID);
		$GameID = $GameArr['GameID'];
		$GameEngName= $GameArr['GameEngName']; 
		$GameChiName= $GameArr['GameChiName'];
		
		# page navigation (leave the array empty if no need)
		if ($GameID != "") {
		    $PAGE_NAVIGATION[] = array($button_edit." ".$MedalListMenu['game'], "");
			$button_title = $button_save;
		} else {
		    $PAGE_NAVIGATION[] = array($button_new." ".$MedalListMenu['game'], "");
			$button_title = $button_submit;
		}
		

					

?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	if(!check_text(obj.GameChiName, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) return false;
<?
//if ($sys_custom['eEnrolment']['CategoryType']) {
//	print "if(!check_text(obj.CategoryTypeID, \"". $i_alert_pleasefillin.$Lang['eEnrolment']['CategoryType']. "\")) return false;\n";
//}
?>
	obj.submit();
}
</SCRIPT>
<form name="form1" action="game_update.php" method="POST" enctype="multipart/form-data">
<br/>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
<tr><td>

<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['Settings']['GameChi']?></td>
		<td><input type="text" id="GameChiName" name="GameChiName" value="<?= htmlspecialchars($GameChiName)?>" class="textboxtext"></td>
	</tr>
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $Lang['SFOC']['Settings']['GameEng']?></td>
		<td><input type="text" id="GameEngName" name="GameEngName" value="<?= htmlspecialchars($GameEngName)?>" class="textboxtext"></td>
	</tr>
</table>

</td></tr>

</table>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_title, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'")?>
</div>
</td></tr>
</table>
<br/>
<input type="hidden" name="GameID" id="GameID" value="<?= $GameID?>" />

</form>
<?= $linterface->FOCUS_ON_LOAD("form1.GameChiName") ?>

    <?
  $linterface->LAYOUT_STOP();
//     }
 //   else
 //   {
    ?>
You have no priviledge to access this page.
    <?
//    }
