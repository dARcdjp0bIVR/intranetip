<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libmedallist = new libmedallist();


$successAry = array();

$GameIDAry = $GameID;

for ($i = 0; $i < sizeof($GameIDAry); $i++) {
    $successAry['delete'][] = $libmedallist->deleteGame($GameIDAry[$i]);
}

if (in_array(false, $successAry)) {
   
    $ReturnMsgKey = 'DeleteUnsuccess';
}
else {

    $ReturnMsgKey = 'DeleteSuccess';
}

// intranet_closedb();
header("Location: index.php?ReturnMsgKey=$ReturnMsgKey");
?>