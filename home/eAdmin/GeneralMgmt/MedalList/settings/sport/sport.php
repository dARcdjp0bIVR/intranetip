<?php
/*
 *  Change Log:
 * 	Date:	2018-06-26 Philips
 *          Created this file
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libmedallist = new libmedallist();
// $libmedallist->hasAccessRight($_SESSION['UserID'], 'Admin');

$CurrentPage = "PageSysSettingSportSettings";
$CurrentPageArr['MedalList'] = 1;

$MODULE_OBJ = $libmedallist->GET_MODULE_OBJ_ARR();


$TAGS_OBJ[] = array($MedalListMenu['sport'], "", 1);
$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_JS_CSS();

$ActionBtnArr = array();
$ActionBtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'SportID[]\',\'sport_new.php\')');
$ActionBtnArr[] = array('delete', 'javascript:checkRemove2(document.form1,\'SportID[]\',\'goDeleteSport();\')');

$fieldname = 'SportID, SportChiName, SportEngName';
$showDelete = ($_GET['showDelete']==1 ? 1 : 0);
$SportAry = $libmedallist->getAllSport($fieldname, $showDelete);
$numOfSports = count($SportAry);

$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="get" action="">'."\n";
    $x .= '<div class="table_board">'."\n";
        $x .= '<table id="html_body_frame" width="100%">'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                    $x .= '<div class="content_top_tool">'."\n";
                        $x .= '<div class="Conntent_tool">'."\n";
                            $x .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Sport();")."\n";
                            $x .= $linterface->Get_Content_Tool_v30("import","javascript:js_Go_Import_Sport();")."\n";
                        $x .= '</div>'."\n";
                      $x .= '</div>'."\n";
                $x .= '</td>'."\n";
            $x .= '</tr>'."\n";
            $x .= '<tr>'."\n";
                $x .= '<td>'."\n";
                        $x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);
                    $x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
                
                    $x .= '<thead>'."\n";
                        $x .= '<tr>'."\n";
                            $x .= '<th style="width:3%;">#</th>'."\n";
                            $x .= '<th style="width:47%;">'.$Lang['SFOC']['Settings']['SportChi'].'</th>'."\n";
                            $x .= '<th style="width:47%;">'.$Lang['SFOC']['Settings']['SportEng'].'</th>'."\n";
                            $x .= '<th style="width:3%;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'SportID[]\'):setChecked(0,this.form,\'SportID[]\')" name="checkmaster"></th>'."\n";
                        $x .= '<tr>'."\n";
                    $x .= '</thead>'."\n";
            
                    $x .= '<tbody>'."\n";
                    if ($numOfSports== 0) {
                        $x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
                    }
                    else {
                        for ($i=0; $i<$numOfSports; $i++) {
                            $thisSportID= $SportAry[$i]['SportID'];
                            $thisSportChiName= $SportAry[$i]['SportChiName'];
                            $thisSportEngName= $SportAry[$i]['SportEngName'];
                         
                            
                            $x .= '<tr id="tr_'.$thisSportID.'">'."\n";
                            $x .= '<td><span class="rowNumSpan">'.($i + 1).'</td>'."\n";
                            $x .= '<td><a href="sport_new.php?SportID[]='.$thisSportID.'">'.$thisSportChiName.'</a></td>'."\n";
                            $x .= '<td><a href="sport_new.php?SportID[]='.$thisSportID.'">'.$thisSportEngName.'</a></td>'."\n";

                            $x .= '<td>'."\n";
                                $x .= '<input type="checkbox" id="SportChk" class="SportChk" name="SportID[]" value="'.$thisSportID.'">'."\n";
                            $x .= '</td>'."\n";
                            $x .= '</tr>'."\n";
                        }
                    }
                    $x .= '</tbody>'."\n";
                $x .= '</table>'."\n";
            $x .= '</td>'."\n";
        $x .= '</tr>'."\n";
        $x .= '</table>'."\n";
    $x .= '</div>'."\n";
$x .= '</form>'."\n";
$x .= '<br />'."\n";
?>
<script language="javascript">
$(document).ready( function() {	
// 	js_Init_DND_Table();
});

function js_Go_New_Sport() {
	window.location = 'sport_new.php';
}
function js_Go_Import_Sport(){
	window.location = 'import.php';
}
	
//Delete Sport
function goDeleteSport() {
	var jsSelectedSportIDArr = new Array();
	$('input.SportChk:checked').each( function() {
		jsSelectedSportIDArr[jsSelectedSportIDArr.length] = $(this).val();
	});
	var jsSelectedSportIDList = jsSelectedSportIDArr.join(',');


	$('form#form1').attr('action', 'sport_remove.php').submit();

}

</script>

<?php
echo $x;
?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>