<?php
// Using:  anna

/***********************************
 *  Date:  (Anna)
 * Details: Create this page
 ************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libmedallist = new libmedallist();


$CurrentPage = "PageSysSettingSportSettings";
$CurrentPageArr['MedalList'] = 1;

$MODULE_OBJ = $libmedallist->GET_MODULE_OBJ_ARR();


$TAGS_OBJ[] = array($MedalListMenu['sport'], "", 1);
$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];

$linterface->LAYOUT_START($returnMsg);


### navigation
// $navigationAry[] = array($eEnrollment['role'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);

# Sample CSV
$SampleCSV = "sfoc_sport_list_sample.csv";	



//### Import Format
$ColumnTitleArr[] = $Lang['SFOC']['Settings']['SportChi'];
$ColumnTitleArr[] = $Lang['SFOC']['Settings']['SportEng'];
$ColumnTitleArr[] = $Lang['SFOC']['Settings']['SportIntroChi'];
$ColumnTitleArr[] = $Lang['SFOC']['Settings']['SportIntroEng'];

$ColumnPropertyArr = array(1,1,0,0);

$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
$htmlAry['columnRemarks'] = $ImportPageColumn;
?>
<script type="text/JavaScript" language="JavaScript">
function checkForm(){
	if($("#csvfile").val() == ''){
		alert("<?=$Lang['General']['warnSelectcsvFile']?>");
		return false;
	}
	else{
		return true;
	}
}

function goSubmit() {
	if(checkForm()==true){
		$('form#form1').attr('action', 'import_step2.php').submit();	
	}
}

function goBack() {
	window.location = 'sport.php';
}
</script>


<form name="form1" id="form1" method="POST" enctype="multipart/form-data">
	<?=$htmlAry['navigation']?>
	<br />	
	<?=$htmlAry['steps']?>
	
		
	       <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" >
 		   	<tr>
			<td>
				<table class="form_table_v30">
					<tr>
						<td class="field_title">
						 <?= $linterface->RequiredSymbol().$Lang['General']['SourceFile'].  $Lang['General']['CSVFileFormat'] ?> </td> 
						<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td> 
				
					</tr>
					
					<tr>
					    <td class="field_title">
						 <?= $Lang['General']['CSVSample'] ?>
						</td>
						<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
							<a id="SampleCsvLink" class="tablelink" href="<?=GET_CSV($SampleCSV)?>" target="_blank">
								<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
								<?=$i_general_clickheredownloadsample?>
							</a>
						</td>
				   </tr>
						   
				    <tr>					   	   				 
						<td class="field_title">
						 <?= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn'] ?>
						</td>
						<td width="75%" class='tabletext'>
						 <?= $htmlAry['columnRemarks']?>
						</td>
				 	</tr>	
				</table>
			</td>
		</tr>
		<tr><td class='tabletext' colspan="max"><?= $linterface->MandatoryField();?> </td></tr>
		</table>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=  $linterface->Get_Action_Btn($Lang['Btn']['Continue'],  "button", "goSubmit()", 'sumbitBtn' )?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'sport.php'")?>
			<p class="spacer"></p>
		</div>

</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>
