<?php
//using
################## Change Log [Start] #################
#
#	Date	:	2017-06-05	anna
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");




intranet_auth();
intranet_opendb();

$libmedallist = new libmedallist();
$linterface = new interface_html();

$CurrentPage = "PageSysSettingEventSettings";
$CurrentPageArr['MedalList'] = 1;

$MODULE_OBJ = $libmedallist->GET_MODULE_OBJ_ARR();


$TAGS_OBJ[] = array($MedalListMenu['game'], "", 1);
$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);

$result = array();
####importing data
$result = $libmedallist->getEventListRecordTemp();

$successArr = array();

foreach($result as $_RecordData){ 
    $successArr[] =  $libmedallist->addNewEvent($_RecordData);
}

if(in_array(false,$successArr)){
	$numSuccess = 0;
}
else{
	$numSuccess = count($successArr);
}



$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

### Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "done();");
?>

<script type="text/javascript">
$(document).ready( function() {
	
});

function done() {
	window.location = 'event.php';
}
function goCancel(){
	window.location = 'event.php';
	
}

</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<br />
	<?=$htmlAry['steps']?>
	
		<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=$numSuccess?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<?=$htmlAry['iframe']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
 <div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>