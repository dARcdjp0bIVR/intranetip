<?php 
/*****************
 *  Change Log:
 * 	Date:	2018-03-12 Anna
 *          Created this file
 * */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/SFOC/libmedallist.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libmedallist = new libmedallist();


$successAry = array();

$EventIDAry = $EventID;

for ($i = 0; $i < sizeof($EventIDAry); $i++) {
    $successAry['delete'][] = $libmedallist->deleteEvent($EventIDAry[$i]);
}

if (in_array(false, $successAry)) {
   
    $ReturnMsgKey = 'DeleteUnsuccess';
}
else {

    $ReturnMsgKey = 'DeleteSuccess';
}

// intranet_closedb();
header("Location: event.php?ReturnMsgKey=$ReturnMsgKey");
?>