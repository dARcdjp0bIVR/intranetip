<?php
# modifying : henry chow

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!isset($GroupID) || $GroupID=="") {
	header("Location: index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();


if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

if(!isset($FolderID) || $FolderID=="") {
	$FolderID = 0;
}

$subTagDisplay = $ldaUI->PageSubTagObj($GroupID, 1);

$groupInfo = $lda->getGroupInfo($GroupID);

$tagInfo = $lda->Get_Tag_Used_In_Group($GroupID);
$TagSelection = getSelectByArray($tagInfo, 'name="TagID[]" id="TagID[]" multiple size="5"', $TagID, 0, 1);

/*	commented, since should be serach the doc in previous academic years 
if(!isset($StartDate) || $StartDate=="")
	$StartDate = substr(getStartDateOfAcademicYear(Get_Current_Academic_Year_ID()), 0, 10);
*/
$searchOption = "
	<div id=\"resource_search\" class=\"resource_search_simple\">
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 1)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
		<div class=\"resource_search_keywords\"><input name=\"keyword\" id=\"keyword\" type=\"text\"  class=\"keywords\" style=\"\"/>
		</div>
		
	</div>";

$searchOptionAdv = "	
	<div id=\"resource_search_adv\" class=\"resource_search_advance\" style=\"display:none\">
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 0)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
		<div class=\"resource_search_keywords\"><input name=\"str\" id=\"str\" type=\"text\" class=\"keywords\" style=\"\"/></div>
			<p class=\"spacer\"></p>
			<div class=\"resource_search_advance_form\">
				<div class=\"table_board\">
					<table class=\"form_table \">
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["UploadDate"]."</td>
							<td>
								<input type=\"checkbox\" name=\"byDateRange\" id=\"byDateRange\" onClick=\"getCheckDateRange(this.checked)\" /> 
								".$linterface->GET_DATE_PICKER("StartDate", $StartDate, "disabled")." $i_To ".$linterface->GET_DATE_PICKER("EndDate", $EndDate, "disabled")."
							</td>
						</tr>
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["CreatedBy"]."</td>
							<td>
								<input type=\"text\" class=\"tabletext\" name=\"createdBy\" ID=\"createdBy\">
							</td>
						</tr>
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["Tag"]."</td>
							<td>
								$TagSelection".$ldaUI->GET_BTN($Lang['Btn']['SelectAll'],"button","checkOptionAll(document.form1.elements['TagID[]'])")."<br><span class=\"tabletextremark\">".$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']."</span>
							</td>
						</tr>
					</table>
				</div>
				<div class=\"edit_bottom\">
						".$linterface->GET_ACTION_BTN($button_search,"button","submitSearch(1)")."
						".$linterface->GET_ACTION_BTN($button_cancel,"button","goCancel()")."
				</div>
			</div>
		</div>
	</div>
	
	";


$canManage = 0;
$adminInGroup = $lda->IsAdminInGroup;
if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	$canManage = 1;
} else if($adminInGroup[$GroupID]) {
	if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-MANAGE")) {
		$canManage = 1;	
	}
} else {
	if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-MANAGE")) {
		$canManage = 1;	
	}	
}



$CurrentPageArr['DigitalArchive'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

if($msg!="")
	$displayMsg = $Lang['General']['ReturnMessage'][$msg];
	
$ldaUI->LAYOUT_START($displayMsg);

echo $ldaUI->Include_JS_CSS();

?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">


<script language="javascript">

var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";


function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) 
{
	$('#'+layername).hide();
}

function showSpan(layername) 
{
	$('#'+layername).show();
}

function Display_New_Folder() {

	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;

	
	$.post(
		"ajax_create_folder.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID
		},
		function(ReturnData)
		{
			initThickBox();
			$('#TB_ajaxContent').html(ReturnData);	
		}
	);			
}

function Display_Upload_New_File() {

	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;

	$.post(
		"ajax_upload_file.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID
		},
		function(ReturnData)
		{
			initThickBox();
			$('#TB_ajaxContent').html(ReturnData);		
		}
	);			
}

/*
function Display_Edit_File() {
	initThickBox();
	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;

	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	if(DocumentID.length==1) {
		if(SearchInArray()) {
			//initThickBox();
			$.post(
				"ajax_edit_file.php", 
				{ 
					GroupID: GroupID,
					FolderID: FolderID,
					DocumentID: DocumentID
				},
				function(ReturnData)
				{
					$('#TB_ajaxContent').html(ReturnData);	
				}
			);
		}
		
	} else {
		alert(globalAlertMsg1);
		window.tb_remove();
		return false;
	}		
}
*/
function Display_Edit() {
	initThickBox();
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	if(DocumentID.length==1) {
		if(SearchInArray()) {
			$('#editLink2').click();
		}
		
	} else {
		alert(globalAlertMsg1);
		window.tb_remove();
		return false;
	}		
}

function Display_Edit_Layer() {

	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	$.post(
		"ajax_edit_file.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID,
			DocumentID: DocumentID
		},
		function(ReturnData)
		{
			
			$('#TB_ajaxContent').html(ReturnData);	
		}
	);

		
	
}

function Get_Directory_List_Table() {

	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	
	$("#DirectoryListDiv").html(loading);
	//Block_Element("DirectoryListDiv");
	
	$.post(
		"ajax_get_directory_list_table.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID
		},
		function(ReturnData)
		{
			document.getElementById("status_option_value").value=0;
			$('#DirectoryListDiv').html(ReturnData);
			//UnBlock_Element("DirectoryListDiv");		
		}
	);			
}

function removeAction() {
	
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	
	if(DocumentID.length>0) {
		
		if(SearchInArray()) {
			$('#deleteWholeThread').val(1);
			
			var PostVar = Get_Form_Values(document.getElementById("form1"));
			
			if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
				$.post(
					"ajax_delete_record.php", PostVar,
					function(ReturnData)
					{
						Get_Return_Message(ReturnData);
						Get_Directory_List_Table();
					}
				);
			}
		}		
	} else {
		alert(globalAlertMsg2);
	}	
}

function checkFileExtract() {
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');
	if(DocumentID.length==0) {
		alert(globalAlertMsg2);	
	} else {
		//document.form1.action = "unzip_action_update.php";
		if(SearchInArray()) {
			var PostVar = Get_Form_Values(document.getElementById("form1"));
			self.location.href = "extra_action_confirm.php?action=extract&"+PostVar+"&DocID="+DocumentID;
		}
		
	}
}

function checkDownload() {
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');
	
	if(DocumentID.length==0) {
		alert(globalAlertMsg2);	
	} else {
		//if(SearchInArray()) {
			//document.form1.action = "download.php";
			//document.form1.submit();
			self.location.href = "download.php?GroupID="+document.getElementById('GroupID').value+"&FolderID="+document.getElementById('FolderID').value+"&DocID="+DocumentID;
		//}	
	}
}


function Move_To_Action(TargetFolderID) {
	
	var DocStr = Get_Check_Box_Value('DocumentID[]','Array');
	
	var obj = document.form1;
	
	if(DocStr.length>0) {
		
		if(SearchInArray()) {
			if(document.form1.FolderID.value==document.getElementById('TargetFolderID').value) {
				
				alert("<?=$Lang['DigitalArchive']['jsCannotCopyToCurrentFolder']?>");
				document.getElementById('TargetFolderID').selectedIndex = 0;	
				(document.getElementById('TargetFolderID')).options[0].select;
				
			} else if(confirm("<?=$Lang['DigitalArchive']['jsMoveAlertMsg']?>")) {
				
				var PostVar = Get_Form_Values(document.getElementById("form1"));
				/*
				$.post(
					"ajax_move_document_update.php", PostVar,
					function(ReturnData)
					{
						Get_Return_Message(ReturnData);
						Get_Directory_List_Table();	
					}
				);
				*/
				
				self.location.href = "extra_action_confirm.php?action=move&"+PostVar+"&DocID="+DocStr+"&TargetFolderID="+document.getElementById('TargetFolderID').value;
			
			} else {
				document.getElementById('TargetFolderID').selectedIndex = 0;
				(document.getElementById('TargetFolderID')).options[0].select;
			}
		} else {
			document.getElementById('TargetFolderID').selectedIndex = 0;
			(document.getElementById('TargetFolderID')).options[0].select;
		}
		
	} else {
		
		alert(globalAlertMsg2);
		document.getElementById('TargetFolderID').selectedIndex = 0;
		(document.getElementById('TargetFolderID')).options[0].select;
			
	}
}

function checkSpanDisplay(layername) {
	
	var hiddenField = layername+"_value"; 
	
	if(document.getElementById(hiddenField).value==1) {
		
		document.getElementById(hiddenField).value = 0;
		//hideSpan(layername);
		document.getElementById(layername).style.visibility = "hidden";
		
	} else {
		
		document.getElementById(hiddenField).value = 1;
		//showSpan(layername);
		document.getElementById(layername).style.visibility = "visible";
		
	}
	
}

function Create_Folder_Action() {
	
	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	var Title = document.getElementById('Title').value;
	var Description = document.getElementById('Description').value;
	
	$.post(
		"ajax_create_folder_update.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID,
			Title: Title,
			Description: Description
		},
		function(ReturnData)
		{
			window.tb_remove();
			Get_Directory_List_Table();
			Get_Return_Message(ReturnData);
		}
	);			

}

function showHideLayer(flag, val) {
	var obj = document.form1;
	if(val==1) {
		$('#'+flag).val(val);
		$('#resource_search').hide();	
		$('#resource_search_adv').show();
	} else {
		$('#'+flag).val(val);
		$('#resource_search_adv').hide();
		$('#resource_search').show();	
	}
	obj.reset();
}

function submitSearch(advSearch) {
	$('#advanceSearchFlag').val(advSearch);
	document.form1.action = "displayResult.php";
	document.form1.submit();
		
}

function goCancel() {
	document.form1.reset();
	showHideLayer('advanceSearchFlag', 0);
}

function getCheckDateRange(flag) {
	if(flag) {
		$('#StartDate').attr("disabled", false);
		$('#EndDate').attr("disabled", false);
	} else {
		$('#StartDate').attr("disabled", true);
		$('#EndDate').attr("disabled", true);		
	}
}

function SearchInArray() {
	var DocIdAry = Get_Check_Box_Value('DocumentID[]','Array');
	
	var editableAryLength = editableAry.length;
	
	if(DocIdAry.length > editableAryLength) {
		alert("<?=$Lang['DigitalArchive']['jsNoAccessRightToManage']?>");	
	} else {
		
		var doc;
		//var temp = "";
		for(var i=0; i<DocIdAry.length; i++) {
		
			var flag = false;
			doc = DocIdAry[i];
			for(j=0; j<editableAryLength; j++) {
				//temp += doc+'|'+editableAry[j]+'\n';
				if(doc == editableAry[j]) {
					flag = true;
					break;			
				}	
			}
			if(flag==false) break;
		}
		if(flag == false) {
			alert("<?=$Lang['DigitalArchive']['jsNoAccessRightToManage']?>");
			return false;
		} else {
			//alert(temp);
			return true;
		}
	}
	
}
</script>

<div id="content" style="padding-left: 10px; width=100%;">

<!-- Content [Start] -->

<!-- navigation [Start] -->
<br>
<div class="navigation_v30 navigation_2">
	<a href="javascript:goBack();"><?=$Lang["DigitalArchive"]["GroupList"]?></a>
	<span><?=intranet_htmlspecialchars($groupInfo['GroupTitle'])?></span>
</div>
<p class="spacer"></p>
<!-- navigation [End] -->

<!-- Search [Start] -->

<!-- Search [End] -->
                    
<p class="spacer"></p>


<!-- Sub navigation [Start] -->
<?=$subTagDisplay?>
<!-- Sub navigation [End] -->

<!-- Navigation [Start] -->
<div class="content_top_tool">
<form name="form2" id="form2" method="GET" action="displayResult.php">

<? if($canManage)  {?>
	<div class="Conntent_tool">
		<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer" onclick="Display_New_Folder(); return false;" class="setting_row thickbox" title="<?=$Lang["DigitalArchive"]["NewFolder"]?>"><?=$Lang["DigitalArchive"]["NewFolder"]?></a>
		<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer" onclick="Display_Upload_New_File(); return false;" class="setting_row thickbox" title="<?=$Lang['General']['UploadFiles']?>"><?=$Lang['General']['UploadFiles']?></a>
		
	</div>
	<? } ?>
	
	<?=$searchOption?>
<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="status_option_value" id="status_option_value" value="">
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="0">
</form>	
</div>

<!-- Navigation [End] -->

<form name="form1" id="form1" method="GET" action="displayResult.php" enctype="multipart/form-data" >
<!-- Directory List [Start] -->
<?=$searchOptionAdv?>
<div class="table_board" id="DirectoryListDiv"></div>

<!-- Directory List [End] -->

<!-- Content [End] -->


<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="status_option_value" id="status_option_value" value="">
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="1">
<input type="hidden" name="deleteWholeThread" id="deleteWholeThread" value="">

</form>
</div>

<script language="javascript">
Get_Directory_List_Table();
</script>

<?

$ldaUI->LAYOUT_STOP();
intranet_closedb();


?>