<?php

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
	
# add record to DB
$dataAry = array();
$dataAry['Title'] = $Title;
$dataAry['Description'] = $Description;
if(isset($Url) && $Url!="") {
	$dataAry['Url'] = $Url;
}
$dataAry['SubjectID'] = $SubjectID;
//$dataAry['SourceExtension'] = strtolower("url");

$result = $lda->updateResourceRecord($dataAry, $RecordID);	

$lda->updateRecordYearRelation($YearID, $RecordID);

# Update Tag relationship
$lda->Delete_Resource_Tag_Relation_By_ID($RecordID);
$lda->Update_Resource_Tag_Relation($Tag, $RecordID);

if($result)
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
else 
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	
intranet_closedb();


header("Location: displayMore.php?msg=$msg&flag=$flag&SubjectID=$SubjectID");
?>