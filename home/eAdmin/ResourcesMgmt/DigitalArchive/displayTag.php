<?php
# modifying : 

/**********************************
 * Date:    2014-02-24 (Tiffany)
 * Details: Add function loadCampusTV
 * 
 * Date:	2013-04-02 (Rita)
 * Details:	add Tag Edit, add js function - Display_Edit_Tag()
 ***********************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!isset($tagid) || $tagid=="") {
	header("Location: index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

$x = $ldaUI->Get_Resource_Record_By_Tag_ID($tagid);

$tagName = returnModuleTagNameByTagID($tagid, $lda->Module);

$CurrentPageArr['DigitalArchive_SubjectReference'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(1);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();


# 2013-04-02 add for Edit Tag
# Check Access Right for Subject eResources Admin 
if($_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	$tagEditBtn = '';
	$tagEditBtn .= '<div class="table_row_tool">';
	$tagEditBtn .= '<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer;" onClick="Display_Edit_Tag()" class="thickbox edit_dim" ></a>';
	$tagEditBtn .= '</div>';
}

echo $ldaUI->Include_JS_CSS();

?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript">
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";


function goCancel() {
	document.form1.reset();
	showHideLayer('advanceSearchFlag', 0);
}

function checkboxSelect(field, val) {
	if(val==true) {
		$('input[name='+field+']').attr('checked', true);
	} else {
		$('input[name='+field+']').attr('checked', false);	
	}
}

function Update_Like_Status(FunctionName, FunctionID, Status)
{
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	
	$.post(
		"ajax_update_like_status.php",
		{
			Status:Status,
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			var d = "Div_"+FunctionName+"_"+FunctionID;
			if(d != 'undefined') {
				$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			}
			//Get_Resource_MyFavourite_Table();
		}
	)
	
}

function goBack() {
	self.location.href = "index.php";
}


function Display_Edit_Tag() {
	initThickBox();
	var TagId = '<?=$tagid?>';
	
	$.post(
		"ajax_edit_tag.php", 
		{ 
			TagID: TagId
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			
		}
	);

}
function loadCampusTV(id)
{
 	js_Show_ThickBox('',450,750);
	$.post(
		'ajax_load_campus_tv.php',
		{
			'tvID': id
		},
		function(data){
			$('#TB_ajaxContent').html(data);	
		}
	)		
}
</script>


<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1">
<div id="content" style="padding-left: 10px; width=100%;">
	<div class="navigation_v30 navigation_2">
		<a href="javascript:goBack();"><?=$i_frontpage_menu_home?></a>
		<span><?=$Lang["DigitalArchive"]["CommonUseTag"]?></span>
	</div>
	<p class="spacer"></p>
	<br style="clear:both">
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
	
				<div class="reading_board" id="resource_tag">
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span id="tagNameDisplay"><?=$tagName[0]?></span></h1><?=$tagEditBtn?>
					</div></div>
					
					<div class="reading_board_left"><div class="reading_board_right">
						<div class="tag_list"></div>						
						<?=$x?>
					
					</div></div>
					
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				</div>
			</td>
		</tr>
	</table>
</div>


</form>
</div>
<iframe id="download_frame" name="download_frame" width="0" height="0" tabindex="-1" style="display:none;"></iframe>
<script language="javascript" src="digitalarchive.js"></script>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>