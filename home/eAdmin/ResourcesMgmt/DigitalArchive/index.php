<?php
# modifying : Siuwan

/************************************
 * Date:    2014-02-24 (Tiffany)
 * Details: Add function loadCampusTV
 * 
 * Date:	2013-04-08 (Rita)
 * Details: swith subject and class selection	
 * 
 * Date:	2013-04-02 (Rita)
 * Details: modified Admin Setting Btn font size to 3
 * 			fix like btn display issue
 * 
 * Date:	2013-03-26 (Rita)
 * Details: add Admin Setting Btn
 ************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['subject_resources']) {
	header("Location: /");
	exit;
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();

$fcmy = new Year();
$subject = new subject();
$cloud = new wordCloud();
$lda = new libdigitalarchive();

$CurrentPageArr['DigitalArchive_SubjectReference'] = 1;

$AcademicYearID = Get_Current_Academic_Year_ID();

if($_SESSION['UserType']==USERTYPE_STAFF) {
	$sql = "SELECT yc.YearID, y.YearName FROM YEAR_CLASS_TEACHER yct INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID AND yc.AcademicYearID='$AcademicYearID') INNER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE yct.UserID='$UserID' GROUP BY y.YearID ORDER BY y.Sequence";
} else if($_SESSION['UserType']==USERTYPE_STUDENT) {
	$sql = "SELECT yc.YearID, y.YearName FROM YEAR_CLASS yc INNER JOIN YEAR_CLASS_USER ycu ON (ycu.YearClassID=yc.YearClassID AND ycu.UserID='$UserID' AND yc.AcademicYearID='$AcademicYearID') INNER JOIN YEAR y ON (y.YearID=yc.YearID) GROUP BY y.YearID ORDER BY y.Sequence";
}
$formAry = $lda->returnArray($sql,2);
$YearID = $formAry[0][0];

$tagUsageArray = $lda->returnTagRankingArray($returnAmount=$ldaUI->HighlightLimit, $lda->Module);

# find the max amount of usage 
$max = 0;
for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	$max = ($tagUsageArray[$i]['Total']>$max) ? $tagUsageArray[$i]['Total'] : $max;
}

for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	list($tagid, $tagname, $total) = $tagUsageArray[$i];
	
	# define tag class 
	if($max==0 || $total/$max < 0.4) {
		$tagclass = "tag_small";
	} else if($total/$max > 0.8) {
		$tagclass = "tag_big";
	} else {
		$tagclass = "tag_normal";	
	}
	
	$cloud->addWord(array('word'=>intranet_htmlspecialchars($tagname), 'size'=>1, 'url'=>'displayTag.php?tagid='.$tagid, 'class'=>$tagclass));
	
}

$TAGS_OBJ = $ldaUI->PageTagObj(1);

$allForms = $fcmy->Get_All_Year_List();
$allSubjects = $subject->Get_All_Subjects();

$tempAry = array();
for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
	$tempAry[$i] = array($allSubjects[$i]['RecordID'], Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']));
}
$allSubjectsRevised = $tempAry;


$formDisplay = getSelectByArray($allForms, 'name="ClassLevelID[]" id="ClassLevelID[]" multiple size="10"', $ClassLevelID, 0, 1);

$subjectDisplay = getSelectByArray($allSubjectsRevised, 'name="SubjectID[]" id="SubjectID[]" multiple size="10"', $SubjectID, 0, 1);


$searchOption = "
	<div id=\"resource_search\" class=\"resource_search_simple\">
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 1)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
		<div class=\"resource_search_keywords\"> <input name=\"keyword\" id=\"keyword\" type=\"text\"  class=\"keywords\" style=\"\"/> 
			<select name=\"SubjectID1\" id=\"SubjectID1\">
				<option value=\"0\">".$Lang['SysMgr']['SubjectClassMapping']['All']['Subject']."</option>";
for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
	$subjectName = Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']);
	$searchOption .= "<option value=\"".$allSubjects[$i]['RecordID']."\">$subjectName</option>";
}
$searchOption .= "
			</select> 
			<input type=\"button\" value=\"".$Lang["DigitalArchive"]["Search"]."\" class=\"formsmallbutton\" onClick=\"submitSearch(0)\" />
		</div>
	</div>



	<div id=\"resource_search_adv\" class=\"resource_search_advance\" style=\"display:none\" >
		<a href=\"javascript:;\" class=\"link_advanced_search\" onClick=\"showHideLayer('advanceSearchFlag', 0)\">".$Lang["DigitalArchive"]["AdvanceSearch"]."</a>
			<p class=\"spacer\"></p>
			<div class=\"resource_search_advance_form\">
				<div class=\"table_board\">
					<table width='570' border='0' align='center'>
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["SearchFor"]."</td>
							<td><input name=\"str\" id=\"str\" type=\"text\" class=\"keywords\" style=\"\"/></span>								
							</td>
						</tr>
						
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["Subject"]."</td>
							<td>$subjectDisplay".$ldaUI->GET_BTN($Lang['Btn']['SelectAll'],"button","checkOptionAll(document.form1.elements['SubjectID[]'])")."<br><span class=\"tabletextremark\">".$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']."</span>
							</td>
						</tr>
						<tr>
							<td class=\"field_title\">".$Lang["DigitalArchive"]["Level"]."</td>
							<td>$formDisplay".$ldaUI->GET_BTN($Lang['Btn']['SelectAll'],"button","checkOptionAll(document.form1.elements['ClassLevelID[]'])")."<br><span class=\"tabletextremark\">".$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']."</span>								
							</td>
						</tr>
					
						<tr>
							<td class=\"field_title\">&nbsp;</td>
							<td><input type='checkbox' name='mylike' id='mylike' value='1'><label for='mylike'".($mylike ? " checked" : "").">".$Lang["DigitalArchive"]["MyLike"]."</label>
							</td>
						</tr>
					</table>
				</div>
				<div class=\"edit_bottom\" >
						".$linterface->GET_ACTION_BTN($button_search,"button","submitSearch(1)")."
						".$linterface->GET_ACTION_BTN($button_cancel,"button","goCancel()")."
				</div>
			</div>
		</div>
	</div>
	";
	


$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

//$linterface = new interface_html();

$ldaUI->LAYOUT_START();
echo $ldaUI->Include_JS_CSS();
?>
<script language="javascript">
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var js_PATH_WRT_ROOT = "<?=$PATH_WRT_ROOT?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";
var CurYearID = "<?=$YearID?>";
</script>
<link href="<?=$PATH_WRT_ROOT?>templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript" src="digitalarchive.js"></script>
<script language="javascript">
function showHideLayer(flag, val) {
	var obj = document.form1;
	if(val==1) {
		$('#'+flag).val(val);
		$('#resource_search').hide();	
		$('#resource_search_adv').show();
	} else {
		$('#'+flag).val(val);
		$('#resource_search_adv').hide();
		$('#resource_search').show();	
	}
	obj.reset();
}

function goSubmit() {
	/*
	$('#flag').val('<?=FUNCTION_ADVANCE_SEARCH?>');
	document.form1.action = "displayMore.php";
	document.form1.submit();
	*/	
}

function submitSearch(advSearch) {
	$('#flag').val('<?=FUNCTION_ADVANCE_SEARCH?>');
	$('#advanceSearchFlag').val(advSearch);
	document.form1.action = "displayMore.php";
	document.form1.submit();
		
}

function goCancel() {
	document.form1.reset();
	showHideLayer('advanceSearchFlag', 0);
}

function checkboxSelect(field, val) {
	if(val==true) {
		$('input[name='+field+']').attr('checked', true);
	} else {
		$('input[name='+field+']').attr('checked', false);	
	}
}
/*
function Get_Resource_Newest_Table(YearID) {
	
	//$("#resource_new").html(loading);
	Block_Element("resource_new");
	
	$.post(
		"ajax_get_newest_resource_record.php", 
		{ 
			YearID: YearID
		},
		function(ReturnData)
		{
			//$('#tempdiv').html(ReturnData);
			$('#resource_new').html(ReturnData);
			UnBlock_Element("resource_new");
		}
	);		
}

function Get_Resource_MyFavourite_Table() {
	//$("#resource_fav").html(loading);
	Block_Element("resource_fav");
	
	$.post(
		"ajax_get_my_favourite_resource_record.php", 
		{ 
			YearID: ""
		},
		function(ReturnData)
		{
			$('#resource_fav').html(ReturnData);
			UnBlock_Element("resource_fav");
		}
	);		
}

function Get_My_Subject_Table(YearID) {
	//$("#resource_subject").html(loading);
	Block_Element("resource_subject");
	
	$.post(
		"ajax_get_my_subject_table.php", 
		{ 
			YearID: YearID
		},
		function(ReturnData)
		{
			$('#resource_subject').html(ReturnData);
			UnBlock_Element("resource_subject");
		}
	);		
}

function Get_Resource_Favourite_Table(YearID) {
	//$("#resource_popular").html(loading);
	Block_Element("resource_popular");
	
	$.post(
		"ajax_get_favourite_record.php", 
		{ 
			YearID: YearID
		},
		function(ReturnData)
		{
			$('#resource_popular').html(ReturnData);
			UnBlock_Element("resource_popular");
		}
	);		
}
*/
function Update_Like_Status(FunctionName, FunctionID, Status)
{
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);

	$.post(
		"ajax_update_like_status.php",
		{
			Status:Status,
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			var d = "Div_"+FunctionName+"_"+FunctionID;
			if(d != 'undefined') {
				$(".Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			}
			Get_Resource_MyFavourite_Table();
			UpdateResourceTable(FunctionName);
		}
	)
	
}

function checkDisplayFlag(table) {
	var flag = document.getElementById(table+'_Flag').value;
	
	if(flag==1) {
		$('#'+table+'_Flag').val(0);
	} else {
		$('#'+table+'_Flag').val(1);
	}	
}

function resetAllTableFlag() {
	
	$('#Get_Resource_Favourite_Table_Flag').val(0);
	$('#Get_My_Subject_Table_Flag').val(0);
	$('#Get_Resource_Newest_Table_Flag').val(0);
		
}

function resetOtherFlag(exceptionFlag) {
	var ary = new Array();
	ary[0] = "Get_Resource_Favourite_Table_Flag";
	ary[1] = "Get_My_Subject_Table_Flag";
	ary[2] = "Get_Resource_Newest_Table_Flag";
	
	for(var i=0; i<ary.length; i++) {
		if(ary[i]==exceptionFlag) {
			$('#'+exceptionFlag).val(1);
		} else {
			$('#'+ary[i]).val(0);
		}
	}
}
function loadCampusTV(id)
{
    js_Show_ThickBox('',450,750);
	$.post(
		'ajax_load_campus_tv.php',
		{
			'tvID': id
		},
		function(data){
			$('#TB_ajaxContent').html(data);	
		}
	)		
}
</script>

<div id="tempdiv"></div>

<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="GET" action="">



<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<?php if($_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {?>
			<div class="Conntent_tool" style="float: right;">
				<a href="subject_eResources_settings/group/index.php" ><font size="3"><?=$Lang['DigitalArchive']['AdminSettings']?></font></a>
			</div>
		<?}?>
		</td>
	</tr>
	<tr>
		<td>
			<?=$searchOption?>
		</td>
	</tr>
	<tr>
		<td>
		<!-- Content [Start] -->
		<div class="resource_portal">
			<div class="resource_portal_left">
				<? if($_SESSION['UserType']==USERTYPE_STAFF) {?>
				<div class="reading_board" id="resource_setting">
					<div class="reading_board_top_left"><div class="reading_board_top_right"></div></div>
					<div class="reading_board_left"><div class="reading_board_right"> 
						<div class="Conntent_tool">
							<a href="new.php" class="new" title="<?=$Lang["DigitalArchive"]["NewFile"]?>">&nbsp;<?=$Lang["DigitalArchive"]["NewFile"]?></a>
						</div>
						<p class="spacer"></p>        			
					</div></div>
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<br style="clear:both"/>
				   
				</div>
			  	<? } ?>
				<p class="spacer"></p>
			
				<!-- MY LIKE [Start] -->                           
				<div class="reading_board" id="resource_fav"></div>
				<!-- MY LIKE [End] -->
			
				<p class="spacer"></p>
			
				<!-- Subject [Start] -->
				<div class="reading_board" id="resource_subject"></div>
				<!-- Subject [End] -->
			
			</div>
			<div class="resource_portal_right">
			
			<!-- Favourite [Start] -->
			<div class="reading_board" id="resource_popular"></div>
			<!-- Favourite [End] -->
		
		                               
			<!-- Newest [Start] -->
			<div class="reading_board" id="resource_new"></div>
			<!-- Newest [End] -->
		                               
		                               
			<div class="reading_board" id="resource_tag">
		        <div class="reading_board_top_left"><div class="reading_board_top_right">
		        	<h1><span><?=$Lang["DigitalArchive"]["CommonUseTag"]?></span></h1><a class="reading_more" href="displayMore.php?flag=<?=FUNCTION_TAG?>"><?=$Lang["DigitalArchive"]["More"]?>..</a>
				</div></div>
		                        
		        <div class="reading_board_left">
		        	<div class="reading_board_right">
						<div class="tag_list"><?=$cloud->showCloud();?></div>
			            <p class="spacer"></p>
					</div>
				</div>
				<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
			</div>
		</div>
	</div>               
	<p class="spacer"></p>
	<!-- Content [End] -->
		</td>
	</tr>
</table>

<br class="clear:both" />
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="">
<input type="hidden" name="flag" id="flag" value="">
<input type="hidden" name="Get_Resource_Favourite_Table_Flag" id="Get_Resource_Favourite_Table_Flag" value="0">
<input type="hidden" name="Get_My_Subject_Table_Flag" id="Get_My_Subject_Table_Flag" value="0">
<input type="hidden" name="Get_Resource_Newest_Table_Flag" id="Get_Resource_Newest_Table_Flag" value="0">


</form>
</div>
<div id="1">
</div>
<script language="javascript">
	Get_Resource_MyFavourite_Table();
	Get_My_Subject_Table("<?=$YearID?>");
	Get_Resource_Favourite_Table("<?=$YearID?>");
	Get_Resource_Newest_Table("<?=$YearID?>");
</script>

<iframe id="download_frame" name="download_frame" width="0" height="0" tabindex="-1" style="display:none;"></iframe>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>