<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

$lda->Start_Trans();

if(in_array(false,$lda->Unlike_Subject_Resource_Record($RecordID))) {
	$lda->RollBack_Trans();
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];	
} else {
	$lda->Commit_Trans();
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}

intranet_closedb();


if($advanceSearchFlag) {
	for($i=0; $i<sizeof($ClassLevelID); $i++) {
		$par .= "&ClassLevelID[]=".$ClassLevelID[$i];	
	}
	for($i=0; $i<sizeof($SubjectID); $i++) {
		$par .= "&SubjectID[]=".$SubjectID[$i];	
	}	
	$par .= "&str=$str&advanceSearchFlag=$advanceSearchFlag";
} else {
	$par = "&SubjectID=$SubjectID1";	
}

header("Location: displayMore.php?flag=$flag&msg=$msg".$par);
?>