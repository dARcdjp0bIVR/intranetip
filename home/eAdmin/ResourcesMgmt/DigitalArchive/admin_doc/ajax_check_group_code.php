<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$GroupID = $_REQUEST['GroupID'];
$GroupCode= trim(urldecode(stripslashes($_REQUEST['GroupCode'])));

$lda = new libdigitalarchive();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}


if ($lda->Check_Group_Code($GroupCode, $GroupID)) {
	echo "1"; // ok
}
else {
	echo "0"; // not ok
}


intranet_closedb();
?>