<?php

# modifying : Henry Chan

/**********************
 * 
 * Date:	2013-07-12 (Henry Chan)
 * Details: The Coding is finished
 * 
 * Date:	2013-07-11 (Henry Chan)
 * Details: File Created
 * 
 **********************/

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

intranet_auth();
intranet_opendb();

$lgs = new libgeneralsettings();

//if ($_REQUEST['txt_upload_notice'] != '') {
	if ($lgs->Save_General_Setting("DigitalArchive", array (
			"UploadNotice" => intranet_htmlspecialchars(trim($_REQUEST['txt_upload_notice']))
		)) && $lgs->Save_General_Setting("DigitalArchive", array (
			"UploadNoticeEnable" => intranet_htmlspecialchars($_REQUEST['chkEnable'])
		))) {
		$msg = "UpdateSuccess";
	} else {
		$msg = "UpdateUnsuccess";
	}
//}

intranet_closedb();

header("Location: upload_notice.php?msg=$msg");
?>