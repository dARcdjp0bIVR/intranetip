<?php
# modifying : henry chow

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$cloud = new wordCloud();
$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();


if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && sizeof($lda->UserInGroupList())==0) {
	header("Location: /home/eAdmin/ResourcesMgmt/DigitalArchive/index.php");
	exit;	
}

# check Scaner Ready
$scannerReady = "";
if(file_exists($PATH_WRT_ROOT."rifolder/global.php")) {
	include_once($PATH_WRT_ROOT."rifolder/global.php");
	if(sizeof($PrinterIPLincense)>0) {
		$scannerReady = 1;	
	}
} 

if($scannerReady) {
	$DivId = " bg_scanready";
	$DivId2 = "resource_adminsetting";
} else { 
	$DivId = "";
	$DivId2 = "resource_subject";
}

$tagUsageArray = $lda->returnTagRankingArray($returnAmount=10, $lda->AdminModule);

# find the max amount of usage 
$max = 0;
for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	$max = ($tagUsageArray[$i]['Total']>$max) ? $tagUsageArray[$i]['Total'] : $max;
}

for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	list($tagid, $tagname, $total) = $tagUsageArray[$i];
	
	# define tag class 
	if($max==0 || $total/$max < 0.4) {
		$tagclass = "tag_small";
	} else if($total/$max > 0.8) {
		$tagclass = "tag_big";
	} else {
		$tagclass = "tag_normal";	
	}
	
	$cloud->addWord(array('word'=>intranet_htmlspecialchars($tagname), 'size'=>1, 'url'=>'displayTag.php?tagid='.$tagid, 'class'=>$tagclass));
	
}

$groupDisplay = $ldaUI->Get_My_Group();

$CurrentPageArr['DigitalArchive'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(2);

# display elements on top right corner
$right_element = $ldaUI->Get_Right_Element();
$TAGS_OBJ_RIGHT[] = array($right_element);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script>
function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) {
	$('#'+layername).hide();
	//document.getElementById(layername).style.display = "none";	
}

function showSpan(layername) {
	$('#'+layername).show();
	//document.getElementById(layername).style.display = "inline";	
}

var ClickID = '';

var callback_member_list = {
	success: function ( o )
    {
		var tmp_str = o.responseText;
		document.getElementById('ref_list').innerHTML = tmp_str;
		DisplayPosition();
		//document.getElementById('ref_list').style.visibility = 'visible';
	}
}

function show_membere_list(click, id)
{
	Hide_Window("ref_list");
	ClickID = click;
	//document.getElementById('task').value = type;
	document.getElementById('gp_id').value = id;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_load_member.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_member_list);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition()
{
	MoveLayerPosition();
	
	document.getElementById('ref_list').style.visibility = 'visible';
}

function MoveLayerPosition() {
	id = document.getElementById('gp_id').value;

	var aryPosition = ObjectPosition(document.getElementById('doc_icon_'+id)); 
	var x = aryPosition[0];
	var y = aryPosition[1];
	
		if(navigator.appName == "Microsoft Internet Explorer") {
			x += 50;
			y += 15;
		} else {
			x += 20;
			y += 15;
		}

	document.getElementById('ref_list').style.left = x + "px";
	document.getElementById('ref_list').style.top = y + "px";
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility = 'hidden';
}

function ObjectPosition(obj) {     
	var curleft = 0;       
	var curtop = 0;       
	if (obj.offsetParent) {             
		do {                   
			curleft += obj.offsetLeft;                   
			curtop += obj.offsetTop;             
		} while (obj = obj.offsetParent);       
	}       
	return [curleft,curtop]; 
} 


</script>
<table width="99%" border="0" cellspacing="0" cellpadding="0">
<tr> 
<td class="main_content<?=$DivId?>">
<p class="spacer"></p>
<div id="content" style="padding-left: 10px; width=100%;">
	<form name="form1" id="form1" method="POST" action="">

	<? if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) { ?>
	<div class="reading_board" id="<?=$DivId2?>">
		<div class="reading_board_top_left"><div class="reading_board_top_right">
			<h1><span><?=$Lang['DigitalArchive']['SystemAdmin']?></span></h1>
		</div></div>
		    
		<div class="reading_board_left">
			<div class="reading_board_right">
				 <div class="Conntent_tool"><a href="access_right.php" class="setting_content"><?=$Lang['DigitalArchive']['AccessRightSettings']?></a></div>
				<p class="spacer"></p>
			</div>
		</div>
		<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
		<p class="spacer"></p>
		<br style="clear:both" />
		<br style="clear:both" />
	</div>
	
	<? } ?>
	
	
	<div class="reading_board" id="resource_admin_doc_group" style="width:100%">
		<div class="reading_board_top_left"><div class="reading_board_top_right">
			<h1><span><?=$Lang['DigitalArchive']['MyGroups']?></span></h1>
		</div></div>
	<div class="reading_board_left"><div class="reading_board_right"><div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>
		<?=$groupDisplay?>
		<p class="spacer"></p>
	</div></div>
	
	<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
	<p class="spacer"></p>
	</div>
	
	<p class="spacer"></p>
	<br style="clear:both" />
	
	<!-- TAG Content [Start] -->
	<div class="reading_board" id="resource_tag">
        <div class="reading_board_top_left"><div class="reading_board_top_right">
        	<h1><span><?=$Lang["DigitalArchive"]["CommonUseTag"]?></span></h1><a class="reading_more" href="displayResult.php?flag=<?=FUNCTION_TAG?>"><?=$Lang["DigitalArchive"]["More"]?>..</a>
		</div></div>
                        
        <div class="reading_board_left">
        	<div class="reading_board_right">
				<div class="tag_list"><?=$cloud->showCloud();?></div>
	            <p class="spacer"></p>
			</div>
		</div>
		<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
	</div>
			
	<p class="spacer"></p>
	<br style="clear:both" />
	<!-- TAG Content [End] -->

	<!-- Content [End] -->
	
	<input type="hidden" id="task" name="task"/>
	<input type="hidden" id="gp_id" name="gp_id"/>
	<input type="hidden" name="ClickID" id="ClickID" value="">
	</form>
</div>
</td>
</tr>
</table>
<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>