<?php
# modifying : 

##### Change Log [Start] #####
#	Date	:	2017-09-19 (Carlos) - Cater intranetdata folder for KIS.
#	Date	:	2013-12-20 (Henry)
#	Details : 	Fix bug on unzip chinese words folder
#
#	Date	: 	2013-04-16 (Rita) 
#	Details : 	add import tag handling 
#
#	Date	:	2012-03-21 (Henry Chow), keep file extension as title
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			
intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$lfs = new libfilesystem();

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';

$lda->Start_Trans();

for($a=0, $a_max=sizeof($DocumentID); $a<$a_max; $a++) {

	$data = $lda->Get_Admin_Doc_Info($DocumentID[$a]);
	$fileext = strtolower($data['FileExtension']);
		
	# Zip File
	if($fileext == "zip") {
		
		# upload file handling [Start]			
		$filename = $data['FileHashName'];
		$destinationPath = $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/admin_doc";
		$originalfile = $destinationPath."/$filename";
		$tmpfolder = $destinationPath."/tmp";
		$lfs->folder_new($tmpfolder);
		
		# if original file does not exist, skip this file
		if(!file_exists($originalfile)) continue;
		
		if($lfs->unzipFile($originalfile,$tmpfolder))
		{
			$tmpfilelist = $lfs->return_folderlist($tmpfolder);	
		}
		//**************************************
		
		$folders = $lfs->return_folder($tmpfolder);	
		$folderNames = array();
		
		for ($i=0; $i<sizeof($folders); $i++)
		{
			$folderNow = $folders[$i];
			$folderNow = str_replace($tmpfolder."/", "", $folderNow);
			list($mailFolder, $throw) = explode("/", $folderNow);
			
			if (!in_array($mailFolder, $folderNames))
			{
				$folderNames[] = $mailFolder;	
			}
		}
		//**************************************		
		if(count($tmpfilelist)>0)
		{	
			$dataAry = array();
			$NoOfUploadSuccess = 0;
			$timestamp = date('YmdHis');
			
			sort($tmpfilelist);
			
			$newFolderId = array();
			//debug_pr($tmpfilelist); exit;
						
			##################    Tag Handling Start   ####################
			foreach($tmpfilelist as $_key=>$_value){
				$filename = substr($_value, (strrpos($_value, "/"))+1);
				$title = substr($filename, 0, strrpos($filename, "."));					
				# Obtain Information From 'file_tags.csv'
				if($title == 'file_tags'){			
					include_once($PATH_WRT_ROOT."includes/libimporttext.php");
					$limport = new libimporttext();					
					$fileTabsCsvData = $limport->GET_IMPORT_TXT($_value);
					$tagArray = array();
					
					$numOfFileTagsCsvData = count($fileTabsCsvData);					
					for($j=1;$j<$numOfFileTagsCsvData;$j++){																
						$thisFileName = trim($fileTabsCsvData[$j][0]);
					 	$numOfTags = count($fileTabsCsvData[$j]);
					 	for($k=1;$k<$numOfTags;$k++){
					 		if(trim($fileTabsCsvData[$j][$k])!=''){
					 			$tagArray[$thisFileName][] = trim($fileTabsCsvData[$j][$k]);
					 		}
					 	}					 						
					}
				}
			}
			#####################   Tag Hadling End    #####################			
			$i = 0;
			foreach($tmpfilelist as $_key=>$_value)
			{				
				$filename = substr($_value, (strrpos($_value, "/"))+1);
				$title = substr($filename, 0, strrpos($filename, "."));					
				$timestamp_zip = "u".$UserID."_".$timestamp."z".$a.$i;
				$fileext = substr($lfs->file_ext($_value),1);
					
				# if directory, skip
				if(is_dir($_value)) {					
					$pathAry = explode('/', substr(str_replace($tmpfolder, '', $_value),1));				
					if(count($pathAry)>0) {						
						if($filename=='updatefilesofdigitalarchive') continue;						
						$existFolderID = $lda->getFolderIDByFolderName($GroupID, $TargetFolderID, $pathAry);						
						if($existFolderID) {
							$newFolderAry[$filename] = $existFolderID; 
						} else {
							//Henry Added [20131220]
							$tmpFolderAry['Title'] = iconv("big5", "utf-8", $filename);
							$tmpFolderAry['FileFolderName'] = iconv("big5", "utf-8", $filename);
							$tmpFolderAry['Description'] = ""; 
							$tmpFolderAry['ParentFolderID'] = (count($pathAry)==1) ? $TargetFolderID : $newFolderAry[$pathAry[count($pathAry)-2]];
							$tmpFolderAry['GroupID'] = $GroupID;
						
							$result = $lda->Create_Admin_Folder($tmpFolderAry);
							$newFolderAry[$filename] = $lda->db_insert_id();
						}	
					}					
					continue;
				}
								
				if(file_exists($_value)) {					
					# check duplication of file name 
					$IsDuplicateFileName = $lda->Is_Duplicate_Admin_Document($GroupID, $TargetFolderID, $title, $filename);
					if($IsDuplicateFileName && $consequence==3)	{	# duplicate name & ignore action
						continue;	
					}
					
					if(in_array($filename,$folderNames)) {	// folder				
						$dataAry['Title'] = iconv("big5", "utf-8", $filename);						
					} else {	// file					
						$lfs->file_rename($_value, $tmpfolder."/".$timestamp_zip);
						$lfs->lfs_move($tmpfolder."/".$timestamp_zip, $destinationPath."/".$timestamp_zip);
						//echo $tmpfolder."/".$timestamp_zip.'<br>';
						//echo $destinationPath."/".$timestamp_zip.'<br><br>';
						//continue;
						$dataAry['Title'] = iconv("big5", "utf-8", $filename);						
						$dataAry['FileExtension'] = strtolower($fileext);
						$dataAry['SizeInBytes'] = filesize($destinationPath."/".$timestamp_zip);						
					}
					
					$newTargetFolderID = $TargetFolderID;					
					for($p=0; $p<count($folderNames); $p++) {
						if(strpos($_value, $folderNames[$p]) != FALSE) {
							$newTargetFolderID = $newFolderId[$folderNames[$p]];
							break;
						}	
					}
					
					$pathAry = explode('/', substr(str_replace($tmpfolder, '', $_value),1));
				
					$dataAry['FileFolderName'] = iconv("big5", "utf-8", $filename);
					$dataAry['FileHashName'] = $timestamp_zip;
					$dataAry['VersionNo'] = 1;
					$dataAry['IsLatestFile'] = 1;
					//$dataAry['ParentFolderID'] = $newTargetFolderID;
					$dataAry['ParentFolderID'] = (count($pathAry)==1) ? $TargetFolderID : $newFolderAry[$pathAry[count($pathAry)-2]];
					$dataAry['GroupID'] = $GroupID;
					$dataAry['IsFolder'] = (in_array($filename,$folderNames)) ? 1 : 0;	
					//debug_pr($dataAry);
					//continue;
								
					$thisRecordID = $lda->insertAdminRecord($dataAry);
					$RecordID[] = ($thisRecordID>0) ? $thisRecordID : false;
											
					# store folder id
					if($dataAry['IsFolder']==1) {
						$newFolderId[$filename]	= $thisRecordID;
					}
					
					# check duplication record
					$IsDuplicateFlag = $lda->Is_Duplicate_Admin_Document($GroupID, $dataAry['ParentFolderID'], $dataAry['Title'], $dataAry['FileFolderName'], $thisRecordID);
					
					if($IsDuplicateFlag) {	# "Title" / "File Name" is duplicated					
						if($consequence==1) {	# overwrite
							$lda->Update_Duplicate_Record($thisRecordID, $GroupID, $dataAry['ParentFolderID'], $dataAry['Title'], $dataAry['FileFolderName']);
						} else if($consequence==2) {	# rename
							$lda->Rename_Duplicate_Record($thisRecordID, $GroupID, $dataAry['ParentFolderID'], $dataAry['Title'], $dataAry['FileFolderName']);
						} 						
					}
										
					##############   Start To Insert Tags	 ##############
					if(count($tagArray)>0){	
						$thisTagArray = array();
						$tagArrayInString = '';
						if(count($pathAry)>2){	# Under Folder				
							$thisFolderWithFileNameArr = array();	
							for($j=1;$j<count($pathAry);$j++){
								$thisFolderWithFileNameArr[]= $pathAry[$j];
							}						
							$thisFolderWithFileName = implode('/',$thisFolderWithFileNameArr);		
							$thisTagArray = $tagArray[$thisFolderWithFileName];			
						}else{	# Not Under Folder 					
							$thisTagArray = $tagArray[$filename];						
						}
						
						if(count($thisTagArray)>0 && $thisRecordID!=''){
							$tagArrayInString = implode(',', array_unique($thisTagArray));							
							# Remove Tags of Existing Files
							if($IsDuplicateFlag){
								$tagRemoveResult = $lda->Delete_Resource_Tag_Relation_By_ID($thisRecordID);
							}							
							$tagUpdateResult = $lda->Update_Admin_Doc_Tag_Relation($tagArrayInString,$thisRecordID,$lda->AdminModule);
						}																			
					}
					##############   End Of Insert Tags	  ###############															
				}
				$i++;
			}						
		}	
		# upload file handling [End]		
	}
}

if(isset($tmpfolder) && $tmpfolder!="" && file_exists($tmpfolder)) {
	$lfs->deleteDirectory($tmpfolder);
}
		
//debug_pr($newFolderId);

if((sizeof($RecordID)==0 || in_array(false,$RecordID)) && sizeof($newFolderAry)==0) {
	
	$lda->RollBack_Trans();
	header("Location: fileList.php?GroupID=$GroupID&FolderID=$FolderID&msg=UnzipUnsuccess");
	exit;
	
} else {
	
	$lda->Commit_Trans();
	header("Location: fileList.php?GroupID=$GroupID&FolderID=$FolderID&msg=UnzipSuccess");
	exit;
	
}

intranet_closedb();

?>