<?php
# modifying : 

##### Change Log [Start] #####
#	Date	: 	2013-10-10 (Yuen)
#
###### Change Log [End] ######

# NEED TO ASSIGN GroupID!!!
$GroupID = 1;


@SET_TIME_LIMIT(6000);

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
			
intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] ) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$lfs = new libfilesystem();


//$FolderArray = array("2010");
// done -  $FolderArray = array("2005 & 2006", "1983-1984");
// done - $FolderArray = array("1973-1979", "1980-1982", "1985", "1986", "1987");

// done - $FolderArray = array("1988", "1989", "1990", "1991", "1992");
// done - $FolderArray = array("1993", "1994", "1995", "1996", "1997");
// done - $FolderArray = array("1998", "1999", "2000", "2001", "2002");
// done - $FolderArray = array("2002 & 2003", "2004", "2006 & 2007", "2007", "2008");
// done - $FolderArray = array("2009", "2010", "2011", "2012", "2013");


# import to DA
# tag: year (folder name), name (chi & eng), Rcord File, Report Card 


$lda->Start_Trans();

# optional - disable this if to import all files read from source folder
$Allowed = "梁麗怡,LEUNG LAI YI,19811124_LEUNGLAIYI.PDF,\student\2005\19811124_LEUNGLAIYI.PDF,P420917A 
林秋紅,LAM CHAU HUNG,19830921_LAMCHAUHUNG.PDF,\student\2005\19830921_LAMCHAUHUNG.PDF,P7682801 
馬影霞,MA YING HA,19831105_MAYINGHA.PDF,\student\2005\19831105_MAYINGHA.PDF,P7297797 
林躍希,LAM YEUK HEI,19831107_LAMYEUKHEI.PDF,\student\2005\19831107_LAMYEUKHEI.PDF,P9158980 
李振寧,LEE CHUN LING,19840723_LEECHUNLING.PDF,\student\2005\19840723_LEECHUNLING.PDF,Z3980392 
陳信安,CHAN SHUN ON,19841012_CHANSHUNON.PDF,\student\2005\19841012_CHANSHUNON.PDF,Z5321035 
陳小燕,CHAN SIU YIN,19841025_CHANSIUYIN.PDF,\student\2005\19841025_CHANSIUYIN.PDF,V0458813 
周漢明,CHOW HON MING,19841127_CHOWHONMING.PDF,\student\2005\19841127_CHOWHONMING.PDF,Z2845904 
梁家豪,LEUNG KA HO,19841218_LEUNGKAHO.PDF,\student\2005\19841218_LEUNGKAHO.PDF,Z6234934 
鍾少青,CHUNG SIU CHING,19850109_CHUNGSIUCHING.PDF,\student\2005\19850109_CHUNGSIUCHING.PDF,Z6779507 
張美玲,CHEUNG MEI LING,19850118_CHEUNGMEILING.PDF,\student\2005\19850118_CHEUNGMEILING.PDF,Z6799389 
周富怡,CHOW FU YI,19850127_CHOWFUYI.PDF,\student\2005\19850127_CHOWFUYI.PDF,P8293086 
馮志明,FUNG CHI MING,19850128_FUNGCHIMING.PDF,\student\2005\19850128_FUNGCHIMING.PDF,P8281061 
張民遠,CHEUNG MAN YUEN,19850314_CHEUNGMANYUEN.PDF,\student\2005\19850314_CHEUNGMANYUEN.PDF,Z6628533 
呂秋生,LUI CHAU SANG,19850325_LUICHAUSANG.PDF,\student\2005\19850325_LUICHAUSANG.PDF,P764635A 
陳紫晴,CHAN TSZ CHING,19850329_CHANTSZCHING.PDF,\student\2005\19850329_CHANTSZCHING.PDF,Z6157522 
陳小雄,CHAN SIU HUNG,19850415_CHANSIUHUNG.PDF,\student\2005\19850415_CHANSIUHUNG.PDF,P8509259 
利芷玲,LEI TSZ LING,19850501_LEITSZLING.PDF,\student\2005\19850501_LEITSZLING.PDF,Z6522913 
林藝昌,LAM NGAI CHEONG,19850614_LAMNGAICHEONG.PDF,\student\2005\19850614_LAMNGAICHEONG.PDF,P7535957 
吳嘉偉,NG KA WAI,19850701_NGKAWAI.PDF,\student\2005\19850701_NGKAWAI.PDF,Z6599991 
黎嘉榮,LAI KA WENG,19850719_LAIKAWENG.PDF,\student\2005\19850719_LAIKAWENG.PDF,P6263163 
周展鴻,CHAU CHIN HUNG,19850805_CHAUCHINHUNG.PDF,\student\2005\19850805_CHAUCHINHUNG.PDF,Z6461167 
陳偉聰,CHAN WAI CHUNG,19850821_CHANWAICHUNG.PDF,\student\2005\19850821_CHANWAICHUNG.PDF,V1002406 
趙學勤,CHIU HOK KAN,19850821_CHIUHOKKAN.PDF,\student\2005\19850821_CHIUHOKKAN.PDF,Z6601597 
鍾妙玲,CHUNG MIU LING,19850826_CHUNGMIULING.PDF,\student\2005\19850826_CHUNGMIULING.PDF,P7558930 
周潔瑩,CHOW KIT YING,19850906_CHOWKITYING.PDF,\student\2005\19850906_CHOWKITYING.PDF,Z6442588 
簡麗薰,KAN LAI FAN,19851014_KANLAIFAN.PDF,\student\2005\19851014_KANLAIFAN.PDF,Z6772340 
麥玉珍,MAK YUK CHUN,19851017_MAKYUKCHUN.PDF,\student\2005\19851017_MAKYUKCHUN.PDF,P838734A 
馮宛瑩,FUNG YUEN YING,19851111_FUNGYUENYING.PDF,\student\2005\19851111_FUNGYUENYING.PDF,Z6346767 
梁詠恩,LEUNG WING YAN,19851125_LEUNGWINGYAN.PDF,\student\2005\19851125_LEUNGWINGYAN.PDF,Z6621415 
伍珍儀,NG CHUN YI,19851205_NGCHUNYI.PDF,\student\2005\19851205_NGCHUNYI.PDF,P7328870 
李善思,LI SINCERE,19860110_LISINCERE.PDF,\student\2005\19860110_LISINCERE.PDF,Z7588659 
陳浩輝,CHAN HO FAI,19860120_CHANHOFAI.PDF,\student\2005\19860120_CHANHOFAI.PDF,Z7247118 
林北欣,LAM PAK YAN,19860123_LAMPAKYAN.PDF,\student\2005\19860123_LAMPAKYAN.PDF,Z7355433 
張欣瑜,CHEUNG YAN YU,19860126_CHEUNGYANYU.PDF,\student\2005\19860126_CHEUNGYANYU.PDF,Z7480351 
吳燕玲,NG YIN LING,19860304_NGYINLING.PDF,\student\2005\19860304_NGYINLING.PDF,Z7003669 
項瑜瑾,HONG YU KAN,19860323_HONGYUKAN.PDF,\student\2005\19860323_HONGYUKAN.PDF,Z7303565 
莫釗深,MOK CHIU SUM,19860429_MOKCHIUSUM.PDF,\student\2005\19860429_MOKCHIUSUM.PDF,Z7928344 
陳福生,CHAN FUK SANG,19860517_CHANFUKSANG.PDF,\student\2005\19860517_CHANFUKSANG.PDF,Z7860839 
麥皓然,MAK HO YIN ALEX,19860614_MAKHOYINALEX.PDF,\student\2005\19860614_MAKHOYINALEX.PDF,Z7052392 
梁安琪,LEUNG ON KI,19860717_LEUNGONKI.PDF,\student\2005\19860717_LEUNGONKI.PDF,Z7835966 
陳天恆,CHAN TIN HANG,19860718_CHANTINHANG.PDF,\student\2005\19860718_CHANTINHANG.PDF,Z7686529 
張仲文,CHEUNG CHUNG MAN,19860729_CHEUNGCHUNGMAN.PDF,\student\2005\19860729_CHEUNGCHUNGMAN.PDF,Z7594993 
劉卓睿,LAU CHEUK YUI,19860827_LAUCHEUKYUI.PDF,\student\2005\19860827_LAUCHEUKYUI.PDF,Z7806842 
何斯妍,HO SZE IN,19860831_HOSZEIN.PDF,\student\2005\19860831_HOSZEIN.PDF,Z7918764 
劉浩然,LAU HO YIN,19861014_LAUHOYIN.PDF,\student\2005\19861014_LAUHOYIN.PDF,Z7472731 
周時睿,CHAU SZE YUI,19861025_CHAUSZEYUI.PDF,\student\2005\19861025_CHAUSZEYUI.PDF,V0580821 
梁昭宇,LEUNG CHIU YU,19861025_LEUNGCHIUYU.PDF,\student\2005\19861025_LEUNGCHIUYU.PDF,Z7119985 
林倩嫣,LAM SIN YIN,19861026_LAMSINYIN.PDF,\student\2005\19861026_LAMSINYIN.PDF,Z7936185 
錢思俊,CHIN SZE CHUN,19861031_CHINSZECHUN.PDF,\student\2005\19861031_CHINSZECHUN.PDF,Z7402628 
陳淑雲,CHAN SHUK WAN,19861104_CHANSHUKWAN.PDF,\student\2005\19861104_CHANSHUKWAN.PDF,P8138552 
翟鍚銘,CHAK SHEK MING,19861119_CHAKSHEKMING.PDF,\student\2005\19861119_CHAKSHEKMING.PDF,V1426591 
莊文輝,CHONG MAN FAI,19861123_CHONGMANFAI.PDF,\student\2005\19861123_CHONGMANFAI.PDF,P8222820 
李逸朗,LEE YAT LONG,19861123_LEEYATLONG.PDF,\student\2005\19861123_LEEYATLONG.PDF,Z7825650 
陳家昌,CHAN KA CHEONG,19861130_CHANKACHEONG.PDF,\student\2005\19861130_CHANKACHEONG.PDF,Z8422266 
梁志楓,LEUNG CHI FUNG,19861211_LEUNGCHIFUNG.PDF,\student\2005\19861211_LEUNGCHIFUNG.PDF,Z8620076 
練佰強,LIN PAK KEUNG,19861228_LINPAKKEUNG.PDF,\student\2005\19861228_LINPAKKEUNG.PDF,V1323598 
周耀恆,CHAU YIU HANG,19870108_CHAUYIUHANG.PDF,\student\2005\19870108_CHAUYIUHANG.PDF,Z8244139 
文啟成,MAN KAI SHING,19870215_MANKAISHING.PDF,\student\2005\19870215_MANKAISHING.PDF,Z8586498 
林鉅順,LAM KUI SHUN,19870301_LAMKUISHUN.PDF,\student\2005\19870301_LAMKUISHUN.PDF,V1116878 
黎詠欣,LAI WING YAN,19870329_LAIWINGYAN.PDF,\student\2005\19870329_LAIWINGYAN.PDF,Z8793515 
張兆儀,CHEUNG SIU YEE,19870417_CHEUNGSIUYEE.PDF,\student\2005\19870417_CHEUNGSIUYEE.PDF,V1346210 
凌浩然,LING HO YIN,19870515_LINGHOYIN.PDF,\student\2005\19870515_LINGHOYIN.PDF,Z8872512 
許淏鈞,HUI HO KWAN,19870615_HUIHOKWAN.PDF,\student\2005\19870615_HUIHOKWAN.PDF,Z8105740 
郭綺琳,KWOK YEE LAM,19870617_KWOKYEELAM.PDF,\student\2005\19870617_KWOKYEELAM.PDF,Z8554901 
劉敏儀,LAU MAN YEE,19870819_LAUMANYEE.PDF,\student\2005\19870819_LAUMANYEE.PDF,V1124978 
范嘉煒,FAN KA WAI,19870920_FANKAWAI.PDF,\student\2005\19870920_FANKAWAI.PDF,Z8416975 
顧嘉明,KU KA MING,19870928_KUKAMING.PDF,\student\2005\19870928_KUKAMING.PDF,V1226770 
劉幸兒,LAU HANG YEE,19871004_LAUHANGYEE.PDF,\student\2005\19871004_LAUHANGYEE.PDF,Z8799815 
張懿婷,CHEUNG YI TING,19871028_CHEUNGYITING.PDF,\student\2005\19871028_CHEUNGYITING.PDF,Z845463A 
呂麗珊,LUI LAI SHAN,19871028_LUILAISHAN.PDF,\student\2005\19871028_LUILAISHAN.PDF,V0989770 
簡麗琪,KAN LAI KEI,19871111_KANLAIKEI.PDF,\student\2005\19871111_KANLAIKEI.PDF,P8363114 
陸暉縉,LUK FAI CHUN,19871111_LUKFAICHUN.PDF,\student\2005\19871111_LUKFAICHUN.PDF,Z8561029 
傅賢煒,FU YIN WAI,19871124_FUYINWAI.PDF,\student\2005\19871124_FUYINWAI.PDF,V1062638 
蔣凱琳,CHIANG HOI LAM,19871209_CHIANGHOILAM.PDF,\student\2005\19871209_CHIANGHOILAM.PDF,Z904057A 
吳景堯,NG KING YIU,19871218_NGKINGYIU.PDF,\student\2005\19871218_NGKINGYIU.PDF,Z9350145 
周俊禧,CHOW CHUN HEI,19871221_CHOWCHUNHEI.PDF,\student\2005\19871221_CHOWCHUNHEI.PDF,Z942274A 
梁曉芹,LEUNG HIU KAN ERICA,19871224_LEUNGHIUKANERICA.PDF,\student\2005\19871224_LEUNGHIUKANERICA.PDF,Z9404776 
周偉業,CHOW WAI YIP,19871230_CHOWWAIYIP.PDF,\student\2005\19871230_CHOWWAIYIP.PDF,Z9769731 
梁倩儀,LEUNG SIN YEE,19880115_LEUNGSINYEE.PDF,\student\2005\19880115_LEUNGSINYEE.PDF,Z9243233 
陳雅妍,CHAN NGA YIN,19880123_CHANNGAYIN.PDF,\student\2005\19880123_CHANNGAYIN.PDF,Z9261576 
關翠儀,KWAN CHUI YEE,19880130_KWANCHUIYEE.PDF,\student\2005\19880130_KWANCHUIYEE.PDF,Z9788450 
朱天奇,CHU TIN KEI,19880208_CHUTINKEI.PDF,\student\2005\19880208_CHUTINKEI.PDF,Z9568264 
郭麗瑩,KWOK LAI YING,19880217_KWOKLAIYING.PDF,\student\2005\19880217_KWOKLAIYING.PDF,Z978854A 
伍嘉曉,NG KA HIU,19880218_NGKAHIU.PDF,\student\2005\19880218_NGKAHIU.PDF,Z9496913 
李敏儀,LI MAN YEE,19880219_LIMANYEE.PDF,\student\2005\19880219_LIMANYEE.PDF,Z9640240 
朱廣恩,CHU KWONG YAN,19880304_CHUKWONGYAN.PDF,\student\2005\19880304_CHUKWONGYAN.PDF,Z9533223 
許耀東,HUI YIU TUNG,19880331_HUIYIUTUNG.PDF,\student\2005\19880331_HUIYIUTUNG.PDF,Z9408011 
高俊媚,KO CHUN MEI,19880418_KOCHUNMEI.PDF,\student\2005\19880418_KOCHUNMEI.PDF,Z9661213 
吳嘉如,NG KA YU,19880423_NGKAYU.PDF,\student\2005\19880423_NGKAYU.PDF,Z9772740 
梁鳯菱,LEUNG FUNG LING,19880515_LEUNGFUNGLING.PDF,\student\2005\19880515_LEUNGFUNGLING.PDF,Z9393634 
陳子軒,CHAN TSZ HIN,19880523_CHANTSZHIN.PDF,\student\2005\19880523_CHANTSZHIN.PDF,Z9410156 
張進壹,CHEUNG CHON IAT,19880525_CHEUNGCHONIAT.PDF,\student\2005\19880525_CHEUNGCHONIAT.PDF,P8755330 
陳鳳玲,CHAN FUNG LING,19880601_CHANFUNGLING.PDF,\student\2005\19880601_CHANFUNGLING.PDF,Z9760661 
黎丹冰,LAI TAN PING,19880607_LAITANPING.PDF,\student\2005\19880607_LAITANPING.PDF,Z968082A 
孔肇麒,HUNG SIU KI,19880612_HUNGSIUKI.PDF,\student\2005\19880612_HUNGSIUKI.PDF,Z9447068 
羅逸文,LAW YAT MAN DANIEL,19880615_LAWYATMANDANIEL.PDF,\student\2005\19880615_LAWYATMANDANIEL.PDF,P8894357 
梁達榮,LEUNG TAT WING,19880621_LEUNGTATWING.PDF,\student\2005\19880621_LEUNGTATWING.PDF,Z9051040 
周梓晴,CHOW TSZ CHING,19880622_CHOWTSZCHING.PDF,\student\2005\19880622_CHOWTSZCHING.PDF,Y0185831 
古惠琪,KOO WAI KEI,19880627_KOOWAIKEI.PDF,\student\2005\19880627_KOOWAIKEI.PDF,P8888985 
梁雲朗,LEUNG WAN LONG,19880630_LEUNGWANLONG.PDF,\student\2005\19880630_LEUNGWANLONG.PDF,Z9447742 
梁灝仁,LEUNG HO YAN,19880705_LEUNGHOYAN.PDF,\student\2005\19880705_LEUNGHOYAN.PDF,Z9233874 
霍啟謙,FOK KAI HIM,19880713_FOKKAIHIM.PDF,\student\2005\19880713_FOKKAIHIM.PDF,Z9396269 
許澤昌,HUI CHAK CHEONG,19880714_HUICHAKCHEONG.PDF,\student\2005\19880714_HUICHAKCHEONG.PDF,Z9483110 
毛家裕,MO KA YU,19880718_MOKAYU.PDF,\student\2005\19880718_MOKAYU.PDF,Z976215A 
郭兆強,KWOK SIU KEUNG,19880724_KWOKSIUKEUNG.PDF,\student\2005\19880724_KWOKSIUKEUNG.PDF,Z9574361 
吳誌宏,NG CHI WANG,19880728_NGCHIWANG.PDF,\student\2005\19880728_NGCHIWANG.PDF,Z9483307 
鄭強,CHENG KEUNG,19880816_CHENGKEUNG.PDF,\student\2010\19880816_CHENGKEUNG.pdf,P8924345 
呂啓聰,LUI KAI CHUNG,19880818_LUIKAICHUNG.PDF,\student\2005\19880818_LUIKAICHUNG.PDF,Z9521225 
許淑雯,HUI SHUK MAN,19880829_HUISHUKMAN.PDF,\student\2005\19880829_HUISHUKMAN.PDF,Z8682857 
鄭向勤,CHENG HEUNG KAN,19880831_CHENGHEUNGKAN.PDF,\student\2005\19880831_CHENGHEUNGKAN.PDF,Z9450395 
陳心樂,CHAN SUM LOK,19880903_CHANSUMLOK.PDF,\student\2005\19880903_CHANSUMLOK.PDF,Z9094939 
莊狄迅,CHONG TIK SHUN,19880904_CHONGTIKSHUN.PDF,\student\2005\19880904_CHONGTIKSHUN.PDF,Z9647792 
吳慧芙,NG WEI FO,19880904_NGWEIFO.PDF,\student\2005\19880904_NGWEIFO.PDF,Z9647512 
林梓聰,LAM TSZ CHUNG,19880906_LAMTSZCHUNG.PDF,\student\2005\19880906_LAMTSZCHUNG.PDF,Z9558366 
麥婷蔚,MAK TING WAI,19880917_MAKTINGWAI.PDF,\student\2005\19880917_MAKTINGWAI.PDF,Z9703374 
駱健怡,LOK KIN YEE MARIA,19880918_LOKKINYEEMARIA.PDF,\student\2005\19880918_LOKKINYEEMARIA.PDF,Z9307401 
陳韋軒,CHAN WAI HIN,19880919_CHANWAIHIN.PDF,\student\2005\19880919_CHANWAIHIN.PDF,Z9307770 
郭嘉善,KWOK KA SIN,19880928_KWOKKASIN.PDF,\student\2005\19880928_KWOKKASIN.PDF,Y1778372 
郭雨晞,KOK YU HEI,19880929_KOKYUHEI.PDF,\student\2005\19880929_KOKYUHEI.PDF,Z9630997 
林家立,LAM KA LAP,19880930_LAMKALAP.PDF,\student\2005\19880930_LAMKALAP.PDF,Z916578A 
莊淑雯,CHONG SUK MAN,19881011_CHONGSUKMAN.PDF,\student\2005\19881011_CHONGSUKMAN.PDF,Z9255231 
關敏兒,KWAN MAN YI,19881015_KWANMANYI.PDF,\student\2005\19881015_KWANMANYI.PDF,Z9785672 
關雅善,KWAN NGA SIN,19881016_KWANNGASIN.PDF,\student\2005\19881016_KWANNGASIN.PDF,Z9649353 
馮穎瑜,FUNG WING YU,19881017_FUNGWINGYU.PDF,\student\2005\19881017_FUNGWINGYU.PDF,Z9765159 
廖麗明,LIU LAI MING,19881017_LIULAIMING.PDF,\student\2005\19881017_LIULAIMING.PDF,Z9238515 
陳紀戎,CHAN KEI YUNG,19881019_CHANKEIYUNG.PDF,\student\2005\19881019_CHANKEIYUNG.PDF,Z9649477 
趙嘉豪,CHIU KA HO,19881102_CHIUKAHO.PDF,\student\2005\19881102_CHIUKAHO.PDF,Z9400231 
李梓浩,LEE TSZ HO,19881108_LEETSZHO.PDF,\student\2005\19881108_LEETSZHO.PDF,Z9790919 
傅慧翔,FU WAI CHEUNG,19881117_FUWAICHEUNG.PDF,\student\2005\19881117_FUWAICHEUNG.PDF,V0606065 
鍾少恆,CHUNG SIU HANG,19881125_CHUNGSIUHANG.PDF,\student\2005\19881125_CHUNGSIUHANG.PDF,Z9686909 
劉庭康,LAU TING HONG,19881125_LAUTINGHONG.PDF,\student\2005\19881125_LAUTINGHONG.PDF,Z9364243 
劉潔盈,LAU KIT YING,19881130_LAUKITYING.PDF,\student\2005\19881130_LAUKITYING.PDF,Y042528A 
陳嘉雯,CHAN KA MAN,19881207_CHANKAMAN.PDF,\student\2005\19881207_CHANKAMAN.PDF,Z934823A 
廖美珊,LIU MEI SHAN,19881207_LIUMEISHAN.PDF,\student\2005\19881207_LIUMEISHAN.PDF,Y020058A 
程金鳳,CHING KAM FUNG,19881216_CHINGKAMFUNG.PDF,\student\2005\19881216_CHINGKAMFUNG.PDF,P9199024 
劉明輝,LAU MING FAI,19881218_LAUMINGFAI.PDF,\student\2005\19881218_LAUMINGFAI.PDF,Y0245168 
洪寶怡,HUNG PO YI,19881220_HUNGPOYI.PDF,\student\2005\19881220_HUNGPOYI.PDF,Y0683130 
梁芷燁,LEUNG TSZ YIP,19881220_LEUNGTSZYIP.PDF,\student\2005\19881220_LEUNGTSZYIP.PDF,Y0260477 
鍾俊彥,CHUNG CHUN YIN,19881223_CHUNGCHUNYIN.PDF,\student\2005\19881223_CHUNGCHUNYIN.PDF,Y0743974 
凌嘉欣,LING KA YAN,19881225_LINGKAYAN.PDF,\student\2005\19881225_LINGKAYAN.PDF,Y0561787 
張秀芬,CHEUNG SAU FAN,19881230_CHEUNGSAUFAN.PDF,\student\2005\19881230_CHEUNGSAUFAN.PDF,Y0230837 
陳香枝,CHAN HEUNG CHI,19890208_CHANHEUNGCHI.PDF,\student\2005\19890208_CHANHEUNGCHI.PDF,P9345039 
李嘉雯,LI KA MAN,19890323_LIKAMAN.PDF,\student\2005\19890323_LIKAMAN.PDF,P9369442 
陳俊濤,CHAN CHUN TO,19890606_CHANCHUNTO.PDF,\student\2005\19890606_CHANCHUNTO.PDF,Y0694809 
梁漪潁,LEUNG YEE WING,19890703_LEUNGYEEWING.PDF,\student\2005\19890703_LEUNGYEEWING.PDF,Y0267625 
吳敏加,NG MAN KA,19890728_NGMANKA.PDF,\student\2005\19890728_NGMANKA.PDF,V1097261 
陳嘉恩,CHAN KA YAN,19890806_CHANKAYAN.PDF,\student\2005\19890806_CHANKAYAN.PDF,Y0449081 
黎潔心,LAI KIT SUM,19890810_LAIKITSUM.PDF,\student\2005\19890810_LAIKITSUM.PDF,Y0617154 
盧建倫,LO KIN LUN,19890824_LOKINLUN.PDF,\student\2005\19890824_LOKINLUN.PDF,R0225087 
鄭建業,CHENG KIN YIP,19891111_CHENGKINYIP.PDF,\student\2005\19891111_CHENGKINYIP.PDF,Y0685915 
何永傑,HO WING KIT,19891205_HOWINGKIT.PDF,\student\2005\19891205_HOWINGKIT.PDF,Y0364175 
馬立賢,MA LAP YIN,19900210_MALAPYIN.PDF,\student\2005\19900210_MALAPYIN.PDF,Y1056342 
鄭崇錡,CHENG SUNG KI,19900218_CHENGSUNGKI.PDF,\student\2005\19900218_CHENGSUNGKI.PDF,Y1017851 
吳穎欣,NG WING YAN,19900220_NGWINGYAN.PDF,\student\2005\19900220_NGWINGYAN.PDF,V1438859 
陳仲熙,CHAN CHUNG HEI,19900225_CHANCHUNGHEI.PDF,\student\2005\19900225_CHANCHUNGHEI.PDF,Y112657A 
陳詠芝,CHAN WING CHI,19900302_CHANWINGCHI.PDF,\student\2005\19900302_CHANWINGCHI.PDF,Y1426093 
李萃婷,LI SUI TING,19900308_LISUITING.PDF,\student\2005\19900308_LISUITING.PDF,Y0686598 
鄭曉盈,CHANG HIU YING,19900602_CHANGHIUYING.PDF,\student\2005\19900602_CHANGHIUYING.PDF,Y0973333 
李步高,LEE PO KO,19900618_LEEPOKO.PDF,\student\2005\19900618_LEEPOKO.PDF,Y0681596 
吳烙謙,NG LOK HIM,19900618_NGLOKHIM.PDF,\student\2005\19900618_NGLOKHIM.PDF,Y1033350 
廖誌立,LIU CHI LAP,19900726_LIUCHILAP.PDF,\student\2005\19900726_LIUCHILAP.PDF,Y1369839 
何倩宜,HO SIN YEE,19900728_HOSINYEE.PDF,\student\2005\19900728_HOSINYEE.PDF,Y146386A 
陳景祺,CHAN KING KI,19900821_CHANKINGKI.PDF,\student\2005\19900821_CHANKINGKI.PDF,Y1201539 
朱嘉榮,CHU KA WING,19900822_CHUKAWING.PDF,\student\2005\19900822_CHUKAWING.PDF,Y1119905 
陳凱玲,CHAN HOI LING,19900901_CHANHOILING.PDF,\student\2010\19900901_CHANHOILING.pdf,Y1370837 
梁培泉,LEUNG PUI CHUEN,19900926_LEUNGPUICHUEN.PDF,\student\2005\19900926_LEUNGPUICHUEN.PDF,Y0896851 
黎偉森,LAI WAI SUM,19901013_LAIWAISUM.PDF,\student\2005\19901013_LAIWAISUM.PDF,Y1302874 
陳菀鈞,CHAN YUEN KWAN,19901104_CHANYUENKWAN.PDF,\student\2005\19901104_CHANYUENKWAN.PDF,Y0690498 
陳穎雯,CHAN WING MAN,19901115_CHANWINGMAN.PDF,\student\2005\19901115_CHANWINGMAN.PDF,Y1318290 
區卓宇,AU CHEUK YU,19901201_AUCHEUKYU.PDF,\student\2009\19901201_AUCHEUKYU.pdf,Y1135528 
陳仲堯,CHAN CHUNG YIU,19901206_CHANCHUNGYIU.PDF,\student\2005\19901206_CHANCHUNGYIU.PDF,Y1067352 
馮承康,FUNG SHING HONG,19901229_FUNGSHINGHONG.PDF,\student\2005\19901229_FUNGSHINGHONG.PDF,Y1652433 
陳敬培,CHAN KING PUI,19910202_CHANKINGPUI.PDF,\student\2010\19910202_CHANKINGPUI.pdf,Y1418910 
陳婉彤,CHAN YUEN TUNG,19910208_CHANYUENTUNG.PDF,\student\2010\19910208_CHANYUENTUNG.pdf,Y1619584 
陳思敏,CHAN SZE MAN,19910301_CHANSZEMAN.PDF,\student\2010\19910301_CHANSZEMAN.pdf,Y1865712 
陳芍彤,CHAN CHEUK TUNG,19910328_CHANCHEUKTUNG.PDF,\student\2010\19910328_CHANCHEUKTUNG.pdf,Y2156979 
鄭曉彤,CHENG HIU TUNG,19910829_CHENGHIUTUNG.PDF,\student\2010\19910829_CHENGHIUTUNG.pdf,Y2087497 
鄭焯豪,CHENG CHEUK HO,19910831_CHENGCHEUKHO.PDF,\student\2010\19910831_CHENGCHEUKHO.pdf,Y1908411 
陳楚騫,CHAN CHO HIN,19910911_CHANCHOHIN.PDF,\student\2005\19910911_CHANCHOHIN.PDF,Y2209258 
陳桂冰,CHAN KWAI PING,19910922_CHANKWAIPING.PDF,\student\2010\19910922_CHANKWAIPING.pdf,R1430288 
陳嘉穎,CHAN KA WING,19911021_CHANKAWING.PDF,\student\2010\19911021_CHANKAWING.pdf,Y192171A 
周文樂,CHAU MAN LOK,19920215_CHAUMANLOK.PDF,\student\2010\19920215_CHAUMANLOK.pdf,Y2256841 
何頌頣,HO CHUNG YEE,19920304_HOCHUNGYEE.PDF,\student\2005\19920304_HOCHUNGYEE.PDF,Y2814224 
區希汶,AU HEI MAN,19920516_AUHEIMAN.PDF,\student\2005\19920516_AUHEIMAN.PDF,Y2672691 
陳嘉怡,CHAN KA YI,19920610_CHANKAYI.PDF,\student\2010\19920610_CHANKAYI.pdf,R1926852 
陳俊豪,CHAN CHUN HO,19921110_CHANCHUNHO.PDF,\student\2010\19921110_CHANCHUNHO.pdf,Y2921098 
周慧姍,CHAU WAI SHAN,19921120_CHAUWAISHAN.PDF,\student\2010\19921120_CHAUWAISHAN.pdf,V1468723 
陳嘉敏,CHAN KA MAN,19930121_CHANKAMAN.PDF,\student\2010\19930121_CHANKAMAN.pdf,Y3020817 
陳諾賢,CHAN NOK YIN,19930206_CHANNOKYIN.PDF,\student\2010\19930206_CHANNOKYIN.pdf,Y3649383 
陳健昇,CHAN KIN SING,19930213_CHANKINSING.PDF,\student\2010\19930213_CHANKINSING.pdf,Y3634084 
陳昭敏,CHAN CHIU MAN,19930331_CHANCHIUMAN.PDF,\student\2010\19930331_CHANCHIUMAN.pdf,Y2942966 
鄭玲紅,CHENG LING HUNG,19930623_CHENGLINGHUNG.PDF,\student\2010\19930623_CHENGLINGHUNG.pdf,Y3655359 
鄭楚文,CHENG CHOR MAN,19930818_CHENGCHORMAN.PDF,\student\2010\19930818_CHENGCHORMAN.pdf,Y3569274 
陳計叡,CHAN KAI YUI,19931019_CHANKAIYUI.PDF,\student\2010\19931019_CHANKAIYUI.pdf,Y3646333 
陳玉明,CHAN YUK MING,19931125_CHANYUKMING.PDF,\student\2010\19931125_CHANYUKMING.pdf,R3246905 
陳翠珠,CHAN CHUI CHU,19931222_CHANCHUICHU.PDF,\student\2010\19931222_CHANCHUICHU.pdf,Y3127999 
陳成豐,CHAN SHING FUNG,19940926_CHANSHINGFUNG.PDF,\student\2010\19940926_CHANSHINGFUNG.pdf,Y4200614";

if ($Allowed!="")
{
	$AllowedArr = explode("\n", $Allowed);

	for ($j=0; $j<sizeof($AllowedArr); $j++)
	{
		$AllowedColumns = explode(",", $AllowedArr[$j]);
		$thisFileName = trim($AllowedColumns[2]);
		$FileIndexing[strtoupper($thisFileName)] = $AllowedColumns;
	}
}

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';

$destinationPath = $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/admin_doc";
$FolderPath = $PATH_WRT_ROOT."../RicohScanned";
for ($i=0; $i<sizeof($FolderArray); $i++)
{
	$FolderName = $FolderArray[$i];
	$SubFolderPath = $FolderPath."/student/".$FolderName;
	if (file_exists($SubFolderPath))
	{
		$dataAry = array();
		
		# create (year) folder
		$dataAry['Title'] = trim(addslashes($FolderName));
		$dataAry['FileFolderName'] = trim(addslashes($FolderName));
		$dataAry['Description'] = "Scanned by Ricoh & Uploaded by eClass";
		$dataAry['ParentFolderID'] = ""; //$FolderID;
		$dataAry['GroupID'] = $GroupID;
		
		$ExistingRecord = $lda->Get_Folder_Info_By_Name($dataAry['FileFolderName'], $dataAry['GroupID'], $dataAry['ParentFolderID']);
		
		if (is_array($ExistingRecord) && sizeof($ExistingRecord)>0 )
		{
			$FolderID = $ExistingRecord[0]["DocumentID"];
		} else
		{
			$FolderID = $lda->Create_Admin_Folder($dataAry);
		}
		
		if ($FolderID=="" || $FolderID<0)
		{
			echo $FolderName ." <font color='red'>cannot be created!</font><br />";
		}
		
		echo "<br /><hr /><br /><b>".$SubFolderPath ." ({$FolderID}) found!</b><br />";
		
		# read the files from folder
		unset($tmpfilelist);
		unset($FileArray);
		unset($RecordID);
		$tmpfilelist = array();
		$FileArray = array();
		$RecordID = array();
		$tmpfilelist = $lfs->return_folderlist($SubFolderPath);
		for ($j=0; $j<sizeof($tmpfilelist); $j++)
		{
			$FileArray[] = trim(str_replace($SubFolderPath."/", "", $tmpfilelist[$j]));
		}
		
		# clear!
		$tmpfilelist = array();
		
		# get the information from indexing.txt
		/*
		$limport = new libimporttext();
		$IndexingFile = $FolderPath."/indexing.txt";
		$IndexArray = $limport->GET_IMPORT_TXT($IndexingFile);

		$FileIndexing = array();
		for ($j=0; $j<sizeof($IndexArray); $j++)
		{
			$thisFileName = trim($IndexArray[$j][2]);
			if (is_array($FileArray) && in_array($thisFileName, $FileArray))
			{
				$FileIndexing[strtoupper($thisFileName)] = $IndexArray[$j];
			}
		}
		*/
		//debug_r($FileIndexing);
		
		# clear!
		$IndexArray = array();
		
		if (sizeof($FileArray)>0 )
		{
			echo "<u>About to import now</u> <br />";
			for ($k=0; $k<sizeof($FileArray); $k++)
			{
				$filename = $FileArray[$k];
				$title = substr($filename, 0, strrpos($filename, "."));	
				$timestamp = date('YmdHis');			
				$timestamp_zip = "u".$UserID."t".$timestamp."z".$k."m".md5($filename)."f".$FolderID;
				$fileext = substr($lfs->file_ext($filename),1);
				//debug($filename);
				//debug($title);
				//debug($timestamp_zip);
				//debug($fileext);
				//continue;
				
				$thisFilePath = $SubFolderPath."/".$filename;
				
				if(file_exists($thisFilePath) && !is_dir($thisFilePath))
				{	
					# check duplication of file name 
					$IsDuplicateFileName = $lda->Is_Duplicate_Admin_Document($GroupID, $FolderID, $title, $filename);
					if ($IsDuplicateFileName)	{	# duplicate name & ignore action
						echo $filename ." <font color='red'>already exist in the DA folder!</font><br />";
						continue;	
					} else
					{
						//echo $filename ." <font color='green'>(new file)</font><br />";
					}
					
					# skip if not included in $Allowed
					if ($Allowed!="" && !is_array($FileIndexing[strtoupper($filename)]))
					{
						continue;
					} else
					{
						debug_r($FileIndexing[strtoupper($filename)]);
						//echo "to be handled<br />";
					}

					//continue;
					
					if (false && in_array($filename,$folderNames)) {	// folder				
						$dataAry['Title'] = iconv("big5", "utf-8", $filename);						
					} else {	// file					
						//$lfs->file_rename($thisFilePath, $tmpfolder."/".$timestamp_zip);
						$lfs->lfs_move($thisFilePath, $destinationPath."/".$timestamp_zip);
						//echo $tmpfolder."/".$timestamp_zip.'<br>';
						//echo $destinationPath."/".$timestamp_zip.'<br><br>';
						//continue;
						//$dataAry['Title'] = iconv("big5", "utf-8", $filename);						
						$dataAry['Title'] = $FileIndexing[strtoupper($filename)][0].$FileIndexing[strtoupper($filename)][1]."_".$filename;
						$dataAry['FileExtension'] = strtolower($fileext);
						$dataAry['SizeInBytes'] = filesize($destinationPath."/".$timestamp_zip);						
					}
					
					/*
					$newTargetFolderID = $FolderID;					
					for($p=0; $p<count($folderNames); $p++) {
						if(strpos($thisFilePath, $folderNames[$p]) != FALSE) {
							$newTargetFolderID = $newFolderId[$folderNames[$p]];
							break;
						}	
					}
					*/
					
					$pathAry = explode('/', substr(str_replace($tmpfolder, '', $thisFilePath),1));
				
					//$dataAry['FileFolderName'] = iconv("big5", "utf-8", $filename);
					$dataAry['FileFolderName'] = $FileIndexing[strtoupper($filename)][0].$FileIndexing[strtoupper($filename)][1]."_".$filename;
					$dataAry['FileHashName'] = $timestamp_zip;
					$dataAry['VersionNo'] = 1;
					$dataAry['IsLatestFile'] = 1;
					//$dataAry['ParentFolderID'] = $newTargetFolderID;
					$dataAry['ParentFolderID'] = $FolderID;
					//$dataAry['ParentFolderID'] = (count($pathAry)==1) ? $FolderID : $newFolderAry[$pathAry[count($pathAry)-2]];
					$dataAry['GroupID'] = $GroupID;
					//$dataAry['IsFolder'] = (in_array($filename,$folderNames)) ? 1 : 0;	
					$dataAry['IsFolder'] = 0;
					//debug_pr($dataAry);
					//continue;
								
					$thisRecordID = $lda->insertAdminRecord($dataAry);
					$RecordID[] = ($thisRecordID>0) ? $thisRecordID : false;
											
					# store folder id
					/*
					if($dataAry['IsFolder']==1) {
						$newFolderId[$filename]	= $thisRecordID;
					}
					*/
					
					# check duplication record
					/*
					$IsDuplicateFlag = $lda->Is_Duplicate_Admin_Document($GroupID, $dataAry['ParentFolderID'], $dataAry['Title'], $dataAry['FileFolderName'], $thisRecordID);
					
					if($IsDuplicateFlag) {	# "Title" / "File Name" is duplicated					
						if($consequence==1) {	# overwrite
							$lda->Update_Duplicate_Record($thisRecordID, $GroupID, $dataAry['ParentFolderID'], $dataAry['Title'], $dataAry['FileFolderName']);
						} else if($consequence==2) {	# rename
							$lda->Rename_Duplicate_Record($thisRecordID, $GroupID, $dataAry['ParentFolderID'], $dataAry['Title'], $dataAry['FileFolderName']);
						} 						
					}
					*/
					$thisTagArray = array();
					$thisTagArray = array($FolderName, "Report Card", "Record File");
					if (trim($FileIndexing[strtoupper($filename)][4])!="")
					{
						$thisTagArray[] = trim($FileIndexing[strtoupper($filename)][4]);
					}
					/*
					if (trim($FileIndexing[$filename][0])!="")
					{
						$thisTagArray[] = $FileIndexing[$filename][0];
					}
					if (trim($FileIndexing[$filename][1])!="")
					{
						$thisTagArray[] = $FileIndexing[$filename][1];
					}
					*/
					$tagArrayInString = implode(',', array_unique($thisTagArray));
					$tagUpdateResult = $lda->Update_Admin_Doc_Tag_Relation($tagArrayInString,$thisRecordID,$lda->AdminModule);
										
					##############   Start To Insert Tags	 ##############
					/*					
					if(count($tagArray)>0){	
						$thisTagArray = array();
						$tagArrayInString = '';
						if(count($pathAry)>2){	# Under Folder				
							$thisFolderWithFileNameArr = array();	
							for($j=1;$j<count($pathAry);$j++){
								$thisFolderWithFileNameArr[]= $pathAry[$j];
							}						
							$thisFolderWithFileName = implode('/',$thisFolderWithFileNameArr);		
							$thisTagArray = $tagArray[$thisFolderWithFileName];			
						}else{	# Not Under Folder 					
							$thisTagArray = $tagArray[$filename];						
						}
						
						if(count($thisTagArray)>0 && $thisRecordID!=''){
							$tagArrayInString = implode(',', array_unique($thisTagArray));
							# Remove Tags of Existing Files
							if($IsDuplicateFlag){
								$tagRemoveResult = $lda->Delete_Resource_Tag_Relation_By_ID($thisRecordID);
							}						
							$tagUpdateResult = $lda->Update_Admin_Doc_Tag_Relation($tagArrayInString,$thisRecordID,$lda->AdminModule);
						}																			
					}
							*/	
					##############   End Of Insert Tags	  ###############															
				}
			}
			if (sizeof($RecordID)!=sizeof($FileArray))
			{
				echo "<font color='red'>Some files may not be imported successfully: ".sizeof($FileArray)."(source) vs ".sizeof($RecordID)."(outcome) </font><br />";
			} else
			{
				echo "<font color='blue'>".sizeof($RecordID)." files have been imported successfully.</font><br />";
			}
		} else
		{
			echo "<font color='red'>There is no file to import @ ". $SubFolderPath ." !</font><br />";
		}
	} else
	{
		echo $SubFolderPath ." <font color='red'>doesn't exist!</font><br />";
	}
}

	$lda->Commit_Trans();
	
	
intranet_closedb();
?>