<?php
// editing by : 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$lda = new libdigitalarchive();

$GroupTitle = trim((stripslashes($_REQUEST['GroupTitle'])));
$GroupID = $_REQUEST['GroupID'];
if ($GroupID=="" || $GroupID<0)
{
	$GroupID = $lda->GET_ACCESS_RIGHT_GROUP_ID($lda->Get_Safe_Sql_Query($GroupTitle));
}
$IsNew = ($GroupID>0);
//debug_r($_REQUEST);
//debug($IsNew);die();
$GroupDescription = trim((stripslashes($_REQUEST['GroupDescription'])));
$Right = (is_array($_REQUEST['Right']))? $_REQUEST['Right']:array();

if(trim($GroupTitle) == "" || trim($GroupCode)=="")
{
	header("Location: access_right.php");
	intranet_closedb();
	exit();
}


$lda->Start_Trans();
$GroupID = $lda->Insert_Group_Access_Right($GroupID, $GroupTitle, $GroupDescription, $GroupCode, $Right);

$lda->UpdateCategoryLinkage($GroupCategoryID, $GroupID);

if($GroupID != false) {
	$lda->Commit_Trans();
	$msg = ($IsNew) ? "UpdateSuccess" : "AddSuccess";
}
else {
	$lda->RollBack_Trans();
	$msg = ($IsNew) ? "UpdateUnsuccess" : "AddUnsuccess";
}

header("Location: access_right.php?msg=".urlencode($msg));
intranet_closedb();
?>