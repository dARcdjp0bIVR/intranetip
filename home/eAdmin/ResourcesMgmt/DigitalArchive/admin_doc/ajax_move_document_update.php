<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}


$lda ->Start_Trans();
if ($lda->MoveAdminRecord($DocumentID, $TargetFolderID, $GroupID, $consequence)) {
	$lda->Commit_Trans();
	// echo $Lang['General']['ReturnMessage']['FileMoveSuccess'];
	$msg = "FileMoveSuccess";
}
else {
	$lda->RollBack_Trans();
	// echo $Lang['General']['ReturnMessage']['FileMoveUnsuccess'];
	$msg = "FileMoveUnsuccess";
}

intranet_closedb();

header("Location: fileList.php?GroupID=$GroupID&FolderID=$FolderID&msg=$msg");
?>