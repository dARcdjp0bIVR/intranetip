<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

$data = $lda->Get_File_List_With_All_Version($Thread, $VersionNo);

$description = $data[0]['Description'];

if($data[0]['RecordStatus']==0) {
	
	if($description!="") {
		$description .= "<br>";
	}
	if($intranet_session_language=="en") {
		
		$description .= "<span class='tabletextremark'>".$Lang['DigitalArchive']['FileIsDeleted_1'].$data[0]['modifyBy'].$Lang['DigitalArchive']['FileIsDeleted_2'].$data[0]['DateModified'].$Lang['DigitalArchive']['FileIsDeleted_3']."</span>";
		
	} else {
		
		$description .= "<span class='tabletextremark'>".$Lang['DigitalArchive']['FileIsDeleted_1'].$data[0]['DateModified'].$Lang['DigitalArchive']['FileIsDeleted_2'].$data[0]['modifyBy'].$Lang['DigitalArchive']['FileIsDeleted_3']."</span>";
		
	}
	
}

echo $description;

intranet_closedb();
?>