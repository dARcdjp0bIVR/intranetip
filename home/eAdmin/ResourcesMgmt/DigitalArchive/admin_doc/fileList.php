<?php
# modifying : 
/*
 * 2019-05-15 (Bill): fixed SQL Injection + Cross-Site Scripting
 * 
 * 2016-01-20 (Carlos) [ip2.5.7.3.1]: modified js Create_Folder_Action(), added AllowIP field.
 * 
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: added libdigitalarchive->Authenticate_DigitalArchive()
 * 
 * 2014-05-30 (Carlos): removeAction(flag), modified to check at least select one file to do deletion
 * 
 * 2014-05-28 (Carlos): Modified js Display_Edit_Folder() use js_Show_ThickBox() to call thickbox
 * 
 * 2014-05-12 (Henry): Bug Fixed (Case# 2014-0509-1126-35071)
 * 
 * 2014-04-07 (Tiffany): Modified Display_Upload_New_File()
 * 
 * 2013-07-29 (Henry): Implementation finished
 * 
 * 2013-07-24 (Henry): File Created
 * 
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
if(isset($FolderID)) {
    $FolderID = IntegerSafe($FolderID);
}
$GroupID = IntegerSafe($GroupID);
if(isset($TagId)) {
    $TagId = IntegerSafe($TagId);
}

if($folderTreeSelect != '' && $folderTreeSelect != 'selected') {
    $folderTreeSelect = '';
}
### Handle SQL Injection + XSS [END]

$linterface = new interface_html();

//$cloud = new wordCloud();
$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

$lda->Authenticate_DigitalArchive();

$CurrentPage = "PageReports_AdministrativeDocument";

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && sizeof($lda->UserInGroupList()) == 0) {
	header("Location: /home/eAdmin/ResourcesMgmt/DigitalArchive/index.php");
	exit;
}

/*
# check Scaner Ready
$scannerReady = "";
if(file_exists($PATH_WRT_ROOT."rifolder/global.php")) {
	include_once($PATH_WRT_ROOT."rifolder/global.php");
	if(sizeof($PrinterIPLincense)>0) {
		$scannerReady = 1;	
	}
} 

if($scannerReady) {
	$DivId = " bg_scanready";
	$DivId2 = "resource_adminsetting";
} else { 
	$DivId = "";
	$DivId2 = "resource_subject";
}
*/

if (!isset($FolderID) || $FolderID == "") {
	$FolderID = 0;
}

/*
$tagUsageArray = $lda->returnTagRankingArray($returnAmount=10, $lda->AdminModule);

# find the max amount of usage 
$max = 0;
for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	$max = ($tagUsageArray[$i]['Total']>$max) ? $tagUsageArray[$i]['Total'] : $max;
}

for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	list($tagid, $tagname, $total) = $tagUsageArray[$i];
	
	# define tag class 
	if($max==0 || $total/$max < 0.4) {
		$tagclass = "tag_small";
	} else if($total/$max > 0.8) {
		$tagclass = "tag_big";
	} else {
		$tagclass = "tag_normal";	
	}
	
	$cloud->addWord(array('word'=>intranet_htmlspecialchars($tagname), 'size'=>1, 'url'=>'displayTag.php?tagid='.$tagid, 'class'=>$tagclass));
	
}
*/

# Check whether can manage file
$canManage = 0;

//$adminInGroup = $ldaUI->IsAdminInGroup;
$adminInGroup = $lda->returnAdmin();
if ($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'])
{
	$canManage = 1;
}
else
{
	if ($adminInGroup[$GroupID]) {
		if ($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-ADMIN_OWN-MANAGE") || $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-ADMIN_OTHERS-MANAGE")) {
			$canManage = 1;
		}
	} else {
		if ($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-MEMBER_OWN-MANAGE") || $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP" . $GroupID . "-MEMBER_OTHERS-MANAGE")) {
			$canManage = 1;
		}
	}
}

$displayTagContent = (isset($TagId) && $TagId != "") ? 1 : 0;
if ($displayTagContent) {
	$callAjaxContent = "Get_Tag_Content();\n";
	$sortOption = "<option value=\"date\">" . $Lang['DigitalArchive']['Location'] . "</option>";

	$tagInfo = returnModuleTagNameByTagID($TagId);
	$tagname = $tagInfo[0];

	$revisedCss = "navigation_v30_v2";
} else {
	$callAjaxContent = "Get_Content();\n";

	$revisedCss = "navigation_v30";
}

//$groupDisplay = $ldaUI->Get_My_Group();

$CurrentPageArr['DigitalArchive'] = 1;
$TAGS_OBJ = $ldaUI->PageTagObj(2);

# Display elements on top right corner
$right_element = $ldaUI->Get_Right_Element();
$TAGS_OBJ_RIGHT[] = array (
	$right_element
);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

if ($msg != "") {
	if ($Lang['General']['ReturnMessage'][$msg] == "") {
		$displayMsg = $msg;
	} else {
		$displayMsg = $Lang['General']['ReturnMessage'][$msg];
	}
}
$ldaUI->LAYOUT_START($displayMsg);

echo $ldaUI->Include_JS_CSS();
?>

<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript" src="scrolltext.js"></script>
<script language="Javascript" src='/templates/tooltip.js'></script>

<script>
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";
//initThickBox();

function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) {
	$('#'+layername).hide();
	//document.getElementById(layername).style.display = "none";	
}

function showSpan(layername) {
	$('#'+layername).show();
	//document.getElementById(layername).style.display = "inline";	
}
/*
var ClickID = '';

var callback_member_list = {
	success: function ( o )
    {
		var tmp_str = o.responseText;
		document.getElementById('ref_list').innerHTML = tmp_str;
		DisplayPosition();
		//document.getElementById('ref_list').style.visibility = 'visible';
	}
}

function show_membere_list(click, id)
{
	Hide_Window("ref_list");
	ClickID = click;
	//document.getElementById('task').value = type;
	document.getElementById('gp_id').value = id;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_load_member.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_member_list);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition()
{
	MoveLayerPosition();
	
	document.getElementById('ref_list').style.visibility = 'visible';
}

function MoveLayerPosition() {
	id = document.getElementById('gp_id').value;

	var aryPosition = ObjectPosition(document.getElementById('doc_icon_'+id)); 
	var x = aryPosition[0];
	var y = aryPosition[1];
	
		if(navigator.appName == "Microsoft Internet Explorer") {
			x += 50;
			y += 15;
		} else {
			x += 20;
			y += 15;
		}

	document.getElementById('ref_list').style.left = x + "px";
	document.getElementById('ref_list').style.top = y + "px";
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility = 'hidden';
}

function ObjectPosition(obj) {     
	var curleft = 0;       
	var curtop = 0;       
	if (obj.offsetParent) {             
		do {                   
			curleft += obj.offsetLeft;                   
			curtop += obj.offsetTop;             
		} while (obj = obj.offsetParent);       
	}       
	return [curleft,curtop]; 
} 
*/

function swapHighLight(emtSelected)
{
	if(emtSelected==1) {
		changeClass('tab_a1', 'current_tab_a');
		changeClass('tab_a2', '');
	} else {
		changeClass('tab_a1', '');
		changeClass('tab_a2', 'current_tab_a');
	}
}

function changeClass(objID, classname)
{
	if ($('#'+objID).length>0)
	{
		$('#'+objID).attr("class",classname);
	}
}

function Display_Blue_Content(emtSelected) {
	//swapHighLight(emtSelected);
	//Get_Content();
	self.location.href = 'index.php?selected='+emtSelected;
}

function Display_Green_Content(groupId, folderId) {
	var str;
	
	if(folderId != 'undefined') {
		str = '&FolderID='+folderId;
	}
	self.location.href = 'fileList.php?GroupID='+groupId+str;	
}

function Get_Navigation_Path() {
	//Block_Element("navigationDiv");
	$('#navigationDiv').html(loading);
	
	$.post(
		"ajax_get_record.php", 
		{ 
			flag: 'navigation',
			GroupID: $('#GroupID').val(),
			FolderID: $('#FolderID').val()
		},
		function(ReturnData)
		{
			$('#navigationDiv').html(ReturnData);
			//UnBlock_Element("navigationDiv");
			//initThickBox();
		}
	);
}

function Load_Folder_Tree(layername) {
	/*
	$.post(
		"ajax_get_record.php", 
		{ 
			flag: 'navigation',
			GroupID: $('#GroupID').val(),
			FolderID: $('#FolderID').val()
		},
		function(ReturnData)
		{
			$('#FolderTreeContentDiv').html(ReturnData);
		}
	);
	*/
	MM_showHideLayers(layername,'','show');
}

var globalFolderCache = {};
function Get_Folder_Hierarchy(divId,groupId) {
	if(globalFolderCache[groupId]) {
		//$('#'+divId).html(globalFolderCache[groupId]);
	}else {
		Block_Element(divId);
		$('#'+divId).load(
			'ajax_get_record.php',
			{
				'flag':'GetFolderHierarchy',
				'GroupID':groupId,
				'SelectedGroupID':$('#GroupID').val() 
			},function(data){
				globalFolderCache[groupId] = data;
				UnBlock_Element(divId);
			}
		);
	}
}

function Hide_Folder_Tree(layername) {
	MM_showHideLayers(layername,'','hide');
	//MM_showHideLayers(FolderTreeContentDiv,'','hide');
}

//function Swap_Folder_Tree(layername) {
//if(document.getElementById(layername).style.visibility=='hidden') {
//	MM_showHideLayers(layername,'','show');
//	document.getElementById(layername).style.display = 'block';
//	document.getElementById('folder_list_li').className = "selected";
//	document.getElementById(layername).style.width = '190px';
//	document.getElementById(layername).style.position = 'relative';
//	document.getElementById(layername).style.left = '0px';
//	document.getElementById('file_list').style.position = 'relative';
//	document.getElementById("DA_file_list").className = "tree_open";
//		//document.getElementById('file_list').style.left = '200px';
//		//document.getElementById('file_list').style.width = (document.getElementById('file_list').offsetWidth - 200) +'px';
//	
//	document.getElementById(layername).style.height = "100%";
//}
//else{
//	MM_showHideLayers(layername,'','hide');
//	$("tree_list").hide();
//	document.getElementById('folder_list_li').className = "";
//	document.getElementById(layername).style.position = 'relative';
//	document.getElementById(layername).style.width = '0px';
//	//if(document.getElementById('file_list').offsetHeight > parseInt(document.getElementById(layername).style.height)){
//		document.getElementById(layername).style.height = document.getElementById('file_list').offsetHeight+"px";
//	//}
//	document.getElementById(layername).style.left = '-190px';
//	document.getElementById('file_list').style.position = 'absolute';
//	document.getElementById('file_list').style.left = '0px';
//	document.getElementById('file_list').style.width = '100%';
//}
//}

function Swap_Folder_Tree(layername)
{
if(document.getElementById(layername) == null)
	return;

if(document.getElementById(layername).style.visibility=='hidden') {
	MM_showHideLayers(layername,'','show');
	document.getElementById(layername).style.display = 'block';
	document.getElementById('folder_list_li').className = "selected";
	document.getElementById(layername).style.width = '380px';
	//document.getElementById(layername).style.position = 'relative';
	//document.getElementById(layername).style.left = '0px';
	//document.getElementById('file_list').style.position = 'relative';
	document.getElementById("DA_file_list").className = "tree_open";
	document.getElementById(layername).style.height = "auto";
//	document.getElementById(layername).style.height = "95%";
//	document.getElementById(layername).style.position = 'absolute';
	document.getElementById('folderTreeSelect').value = "selected";
}
else{
	MM_showHideLayers(layername,'','hide');
	$("tree_list").hide();
	document.getElementById('folder_list_li').className = "";
	document.getElementById(layername).style.display = 'none';
	//document.getElementById(layername).style.position = 'relative';
	//document.getElementById(layername).style.width = '0px';
	//if(document.getElementById('file_list').offsetHeight > parseInt(document.getElementById(layername).style.height)){
		//document.getElementById(layername).style.height = document.getElementById('file_list').offsetHeight+"px";
	//}
	//document.getElementById(layername).style.left = '-380px';
	//document.getElementById('file_list').style.position = 'absolute';
	//document.getElementById('file_list').style.left = '0px';
	//document.getElementById('file_list').style.width = '100%';
	document.getElementById("DA_file_list").className = "";
	document.getElementById('folderTreeSelect').value = "";
}
}

function file_folder_onclick(groupid, folderid){
	self.location.href='fileList.php?GroupID=' + groupid + '&FolderID=' + folderid + '&folderTreeSelect=' + document.getElementById('folderTreeSelect').value;
}

//function nav_file_folder_onclick(groupid, folderid){
//	self.location.href='fileList2.php?GroupID=' + groupid + '&FolderID=' + folderid + '&folderTreeSelect=' + document.getElementById('folderTreeSelect').value;
//}

function Check_Folder_List(){
	//document.getElementById('folderTreeSelect').value = document.getElementById('folder_list_li').className;	
	var qs = "<?=remove_qs_key($_SERVER['QUERY_STRING'], "folderTreeSelect")?>";
	self.location.href = "fileList_Icon.php?"+qs+"&folderTreeSelect="+document.getElementById('folder_list_li').className;
}
function Check_Tag_Folder_List(){
	//document.getElementById('folderTreeSelect').value = document.getElementById('folder_list_li').className;	
	var qs = "<?=remove_qs_key($_SERVER['QUERY_STRING'], "folderTreeSelect")?>";
	self.location.href = "fileList_Icon.php?"+qs+"&folderTreeSelect="+document.getElementById('folderTreeSelect').value;
}

<?function remove_qs_key($url, $key) {
	$url = preg_replace('/(?:&|(\?))' . $key . '=[^&]*(?(1)&|)?/i', "$1", $url);
	return $url;
}?>

function Get_Content()
{
	//Block_Element("contentDiv");
	$('#contentDiv').html(loading);
	$.post(
		"ajax_get_record.php", 
		{ 
			flag: 'FileList2',
			GroupID: $('#GroupID').val(),
			FolderID: $('#FolderID').val(),
			folderTreeSelect: $('#folderTreeSelect').val(),
			OrderBy: $('#orderBy').val(),
			OrderBy2: $('#orderBy2').val()
		},
		function(ReturnData)
		{
			$('#contentDiv').html(ReturnData);
			//UnBlock_Element("contentDiv");
		}
	);		
}

function Get_Tag_Content() {
	
	Block_Element("contentDiv");
	
	$.post(
		"ajax_get_record.php", 
		{ 
			flag: 'Tag',
			TagId: $('#TagId').val(),
			OrderBy: $('#orderBy').val(),
			OrderBy2: $('#orderBy2').val()
		},
		function(ReturnData)
		{
			$('#contentDiv').html(ReturnData);
			UnBlock_Element("contentDiv");
		}
	);		
}

function moveLeft() {
	move('-=100px');
}

function moveRight() {
	move('+=100px');
}

function move(px) {
	$('#green_inner_layer').animate({
		'marginLeft' : px
	});
}

function Get_Directory_List_Table() {
	Get_Navigation_Path();
	<?=$callAjaxContent?>
}

function Display_New_Folder() {
	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();

	$.post(
		"ajax_create_folder.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID
		},
		function(ReturnData)
		{
			//initThickBox();
			$('#TB_ajaxContent').html(ReturnData);	
		}
	);			
}

function Create_Folder_Action() {
	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	var Title = document.getElementById('Title').value;
	var Description = document.getElementById('Description').value;
	var AllowIP = document.getElementById('AllowIP').value;
	var folderTreeSelect = document.getElementById('folderTreeSelect').value;
	
	$.post(
		"ajax_create_folder_update.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID,
			Title: Title,
			Description: Description,
			AllowIP: AllowIP 
		},
		function(ReturnData)
		{
			window.tb_remove();
			/*
			Get_Directory_List_Table();
			Get_Return_Message(ReturnData);
			*/
			self.location.href = "fileList.php?GroupID="+GroupID+"&FolderID="+FolderID+"&folderTreeSelect="+folderTreeSelect+"&msg="+ReturnData;
		}
	);
}
 
function Display_Upload_New_File() {
    var DocumentIDAry = Get_Check_Box_Value('DocumentID[]','Array');	
    DocumentID = DocumentIDAry.join();
	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();
 	var folderTreeSelect = $("#folderTreeSelect").val();
 	
	$.post(
		"ajax_upload_file.php", 
		{   
			DocumentID: DocumentID,
			GroupID: GroupID,
			FolderID: FolderID,
			viewMode: 'list',
			folderTreeSelect: folderTreeSelect
		},
		function(ReturnData)
		{
			//initThickBox();
			$('#TB_ajaxContent').html(ReturnData);
		}
	);
}

function Display_Edit() {
	//initThickBox();
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	if(DocumentID.length==1) {
		if(SearchInArray()) {
			$('#editLink2').click();
		}
	} else {
		alert(globalAlertMsg1);
		window.tb_remove();
		return false;
	}
}

function Display_Edit_Layer(flag) {
	js_Show_ThickBox('<?=$Lang['Btn']['Edit']." ".$Lang['DigitalArchive']['Folder']?>',500,750);
	
	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	if(flag==1) {
		var DocumentID = "";
	} else {
		var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');	
	}
	
	//initThickBox();
	
	$.post(
		"ajax_edit_file.php", 
		{ 
			GroupID: GroupID,
			FolderID: FolderID,
			DocumentID: DocumentID
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);	
		}
	);
}

function Move_To_Action(TargetFolderID) {
	var DocStr = Get_Check_Box_Value('DocumentID[]','Array');
	
	var obj = document.form1;
	
	if(DocStr.length>0) {
		if(SearchInArray()) {
			if(document.form1.FolderID.value==document.getElementById('TargetFolderID').value) {
				alert("<?=$Lang['DigitalArchive']['jsCannotCopyToCurrentFolder']?>");
				document.getElementById('TargetFolderID').selectedIndex = 0;	
				(document.getElementById('TargetFolderID')).options[0].select;
			} else if(confirm("<?=$Lang['DigitalArchive']['jsMoveAlertMsg']?>")) {
				var PostVar = Get_Form_Values(document.getElementById("form1"));
				self.location.href = "extra_action_confirm.php?action=move&"+PostVar+"&DocID="+DocStr+"&TargetFolderID="+document.getElementById('TargetFolderID').value;
			} else {
				document.getElementById('TargetFolderID').selectedIndex = 0;
				(document.getElementById('TargetFolderID')).options[0].select;
			}
		} else {
			document.getElementById('TargetFolderID').selectedIndex = 0;
			(document.getElementById('TargetFolderID')).options[0].select;
		}
	} else {
		alert(globalAlertMsg2);
		document.getElementById('TargetFolderID').selectedIndex = 0;
		(document.getElementById('TargetFolderID')).options[0].select;
	}
}

function Move_To_Action2(TargetFolderID) {
	var DocStr = Get_Check_Box_Value('DocumentID[]','Array');
	
	var obj = document.form1;
	
	if(DocStr.length>0) {
		if(SearchInArray()) {
			if(document.form1.FolderID.value==document.getElementById('TargetFolderID2').value) {
				alert("<?=$Lang['DigitalArchive']['jsCannotCopyToCurrentFolder']?>");
			} else if(confirm("<?=$Lang['DigitalArchive']['jsMoveAlertMsg']?>")) {
				var PostVar = Get_Form_Values(document.getElementById("form1"));
				
				self.location.href = "extra_action_confirm.php?action=move&"+PostVar+"&DocID="+DocStr+"&TargetFolderID="+document.getElementById('TargetFolderID2').value;
			} else {
				
			}
		} else {
			
		}
	} else {
		alert(globalAlertMsg2);
	}
}

function checkFileExtract() {
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');
	if(DocumentID.length==0) {
		alert(globalAlertMsg2);	
	} else {
		
		if(SearchInArray()) {
			var PostVar = Get_Form_Values(document.getElementById("form1"));
			self.location.href = "extra_action_confirm.php?action=extract&"+PostVar+"&DocID="+DocumentID;
		}
		
	}
}

function checkDownload() {
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');
	
	if(DocumentID.length==0) {
		alert(globalAlertMsg2);	
	} else {
		self.location.href = "download.php?GroupID="+document.getElementById('GroupID').value+"&FolderID="+document.getElementById('FolderID').value+"&DocID="+DocumentID;
		
	}
}

function checkSpanDisplay(layername, objLayer) {
	
	//if(objLayer != 'undefined') {
		var pos = $("#"+objLayer).position(); 
		var topPos = pos.top + 25;
		var leftPos = pos.left + 15;
	//}
	$("#"+layername).css({"top": topPos, "left": leftPos });	
	
	var hiddenField = layername+"_value";
	
	if(document.getElementById(hiddenField).value==1) {
		
		document.getElementById(hiddenField).value = 0;
		document.getElementById(layername).style.visibility = "hidden";
		
	} else {
		
		document.getElementById(hiddenField).value = 1;
		document.getElementById(layername).style.visibility = "visible";
		
	}
	
}

function SearchInArray() {
	var DocIdAry = Get_Check_Box_Value('DocumentID[]','Array');
	
	var editableAryLength = editableAry.length;
	
	if(DocIdAry.length > editableAryLength) {
		alert("<?=$Lang['DigitalArchive']['jsNoAccessRightToManage']?>");	
	} else {
		
		var doc;
		
		for(var i=0; i<DocIdAry.length; i++) {
		
			var flag = false;
			doc = DocIdAry[i];
			for(j=0; j<editableAryLength; j++) {
				
				if(doc == editableAry[j]) {
					flag = true;
					break;			
				}	
			}
			if(flag==false) break;
		}
		if(flag == false) {
			alert("<?=$Lang['DigitalArchive']['jsNoAccessRightToManage']?>");
			return false;
		} else {
			
			return true;
		}
	}
}

function storePosition(objName) {
	var pos = $("#"+objName).position(); 
	$("#leftPos").val(pos.left);
	$("#topPos").val(pos.top);
	 	
}


function DisplayGroupHierachy(getObjName, setObjName) {
	
	var pos = $("#"+getObjName).position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	topPos = parseInt(topPos) + 30;
	
	$("#"+setObjName).css({"position": "absolute", "top": topPos, "left": leftPos });	
}

function DisplayAdvanceSearch() {
	
	var pos = $("#AdvanceSearchText").position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	topPos = parseInt(topPos) + 15;
	leftPos = parseInt(leftPos) - 682;
	
	$("#DA_search").css({"position": "absolute", "top": topPos, "left": leftPos, "z-index":9 });
	MM_showHideLayers('DA_search','','show');	
}

function Remove_Tag(tagId) {

	var obj = document.form1;

	if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {

		self.location.href = "remove_tag_update.php?TagID="+tagId;

	}

}


function Display_Edit_Tag() {

	var TagId = $("#TagId").val();
	
	$.post(
		"ajax_edit_tag.php", 
		{ 
			TagID: TagId
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);	
		}
	);

}

function removeAction(flag) {
	
	// flag : 1 = folder; 0 = file;
	
	var GroupID = document.form1.GroupID.value;
	var FolderID = document.form1.FolderID.value;
	if(flag==1) {
		var DocumentID = FolderID;
	} else {
		var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');
		if(DocumentID.length == 0) {
			alert(globalAlertMsg2);
			return;
		}
	}
	
		if(SearchInArray()) {
			$('#deleteWholeThread').val(1);
			
			var PostVar = Get_Form_Values(document.getElementById("form1"));
			var DocIDList = DocumentID;
			PostVar += "&DocIDList="+DocIDList;
			
			if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
				Block_Document('<?=$Lang['General']['Procesesing']?>');
				$.post(
					"ajax_delete_record.php",PostVar,
					function(ReturnData)
					{
						if(flag==1) {
							self.location.href = "fileList.php?GroupID="+GroupID+"&msg="+ReturnData;
						} else {
							UnBlock_Document();
							Get_Return_Message(ReturnData);
							Get_Directory_List_Table();
						}
					}
				);
			}
		}		
	
}

function SearchInArray() {

	var DocIdAry = Get_Check_Box_Value('DocumentID[]','Array');
	
	var editableAryLength = editableAry.length;
	
	if(DocIdAry.length > editableAryLength) {
		alert("<?=$Lang['DigitalArchive']['jsNoAccessRightToManage']?>");	
	} else {
		
		var doc;
		//var temp = "";
		for(var i=0; i<DocIdAry.length; i++) {
		
			var flag = false;
			doc = DocIdAry[i];
			for(j=0; j<editableAryLength; j++) {
				//temp += doc+'|'+editableAry[j]+'\n';
				if(doc == editableAry[j]) {
					flag = true;
					break;			
				}	
			}
			if(flag==false) break;
		}
		if(flag == false) {
			alert("<?=$Lang['DigitalArchive']['jsNoAccessRightToManage']?>");
			return false;
		} else {
			//alert(temp);
			return true;
		}
	}
	
}

function Get_Green_Tab()
{
	Block_Element('DA_tab_B1');
	$('#myScrollContent').load(
		'ajax_get_green_tab.php',
		{
			'GroupID':'<?=$GroupID?>'
		},
		function(data){
			UnBlock_Element('DA_tab_B1');
		}
	);
}

var imgObj=0;
var callback = {
        success: function ( o )
        {
                writeToLayer('ToolMenu2',o.responseText);
                showMenu2("img_"+imgObj,"ToolMenu2");
        }
}
function hideMenu2(menuName){
		objMenu = document.getElementById(menuName);
		if(objMenu!=null)
			objMenu.style.visibility='hidden';
		setDivVisible(false, menuName, "lyrShim2");
}
function showMenu2(objName,menuName){
		  hideMenu2('ToolMenu2');
           objIMG = document.getElementById(objName);
           objToolMenu2 = document.getElementById('ToolMenu2');
           objkeyword = document.getElementById('keyword');
			offsetX = (objIMG==null)?0:objIMG.width;
			offsetY =0;            
            var pos_left = getPostion(objIMG,"offsetLeft");
			var pos_top  = getPostion(objIMG,"offsetTop");
			
			objDiv = document.getElementById(menuName);
			
			if(objDiv!=null){
				objDiv.style.visibility='visible';
				objDiv.style.top = pos_top+offsetY+"px";
				objDiv.style.left = pos_left+offsetX+"px";
				setDivVisible(true, menuName, "lyrShim2");
			}
}

function showInfo(val)
{
        obj = document.form1;

        var myElement = document.getElementById("ToolMenu2");

        writeToLayer('ToolMenu2','');
        imgObj = val;
        
		showMenu2("img_"+val,"ToolMenu2");
        YAHOO.util.Connect.setForm(obj);

        var path = "getHelpInfo.php?Val=" + val;
        var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
</script>


<form name="form1" id="form1" method="post">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
<?php if (!$sys_custom['digital_archive']['hide_groupmenu']) { ?>    
			<!-- DA_tab [Start] -->
			<div class="DA_tab">
				<? if(false && $_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {?>
				<div class="Conntent_tool">
					<a href="access_right.php" class="setting_content"><?=$Lang['DigitalArchive']['AdminSettings']?></a>
				</div>
			    <? } ?>             
				<!-- DA_tab_A [Start] -->
				<?=$ldaUI->Get_Blue_Tab()?>
				<!-- DA_tab_A [End] -->
				
				<!-- DA_tab_B [Start] -->
				<div class="DA_tab_B" id="DA_tab_B1">
					<div id="moveLeftDiv">
						<ul><li class="arrow_prev" id="moveLeftIcon"><a title="<?


/*=$Lang['DigitalArchive']['MoveLeft']*/
?>" onMouseOver="test.scrollWest()" onMouseOut="test.endScroll()"></a></li></ul>
					</div>
					<div id="myScrollContainer" style="position:absolute;">
						<div id="myScrollContent" style="position:absolute;">
							<?php


echo $ldaUI->Get_Green_Tab($GroupID);
?>
						</div>
					</div>
					<div style="position:relative;float:right;z-index:0;" id="moveRightDiv"><ul><li class="arrow_next" id="moveRightIcon"><a title="<?


/*=$Lang['DigitalArchive']['MoveRight']*/
?>" onMouseOver="test.scrollEast()" onMouseOut="test.endScroll()" style="float:right;"></a></li></ul></div>
						
				</div>
				<!-- DA_tab_B [End] -->
				
			</div>
<?php } ?>
			<!-- DA_tab [End] -->     
			
			<p class="spacer"></p>

<!--#### Navigator start ####-->
						
                      <div id="navigationDiv">
                      	<div class="<?=$revisedCss?>">
                      		<? if($tagname!="") {?>
							<span><?=$Lang['DigitalArchive']['Tag']?></span>
							<span><?=$tagname?></span>
							
							<?}?>
							
							<? if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) { ?>
								<div class="table_row_tool">
									<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer;" onClick="Display_Edit_Tag()" class="thickbox edit_dim" title="<?=$Lang['Btn']['Edit']." ".$Lang['DigitalArchive']['Tag']?>"></a>
									
									<a href="javascript:;" onClick="Remove_Tag(<?=$TagId?>)" class="delete_dim" title="<?=$Lang['Btn']['Delete']." ".$Lang['DigitalArchive']['Tag']?>"></a>
								</div>
							<?}?>
							<br style="spacer" />
							
						</div>
					  </div>
					<br style="spacer" />	

        			<!--#### Navigator end ####-->

        			<div class="DA_content_tool"> <div class="DA_content_tool_right"><div class="DA_content_tool_bg">

                    
					<?if(!$displayTagContent && $canManage) {?>
                   	<div class="Conntent_tool" style="position:relative;z-index:1;">
						<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer" onclick="Display_New_Folder(); return false;" class="setting_row thickbox new_folder" title="<?=$Lang["DigitalArchive"]["NewFolder"]?>"><?=$Lang["DigitalArchive"]["NewFolder"]?></a>
						<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer" onclick="Display_Upload_New_File(); return false;" class="upload setting_row thickbox" title="<?=$Lang['General']['UploadFiles']?>"><?=$Lang['General']['UploadFiles']?></a>
					</div>
                        <a href="javascript:showInfo(1)"><img id='img_1' src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/inventory/icon_help.gif" border=0></a>     
					
					<?}?>

                    
					<div class="thumb_list_tab" id="sortMenu">
						<?=$Lang['DigitalArchive']['SortBy']?> : 
						<select name="orderBy" id="orderBy" onChange="<?=$callAjaxContent?>">
							<option value="filename" selected="selected"><?=$Lang["DigitalArchive"]["Title"]?></option>
							<option value="date"><?=$Lang['DigitalArchive']['ModifiedDate']?></option>
							<?=$sortOption?>
						</select>
						<select name="orderBy2" id="orderBy2" onChange="<?=$callAjaxContent?>">
							<option value="ASC"><?=$Lang['DigitalArchive']['ASC']?></option>
							<option value="DESC"><?=$Lang['DigitalArchive']['DESC']?></option>
						</select>
					</div>
			<!-- Filter [End] -->

					<div class="common_table_tool">							  
			    <!-- folder move to at table tool start-->
				<?


/*
# check whether can manage file
$canManage = 0;
$adminInGroup = $ldaUI->IsAdminInGroup;
if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	$canManage = 1;
} else if($adminInGroup[$GroupID]) {
	if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OWN-MANAGE") || $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-MANAGE")) {
		$canManage = 1;	
	}
} else {
	if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-MANAGE") || $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-MANAGE")) {
		$canManage = 1;	
	}	
}
*/
$array = $lda->Get_Folder_Hierarchy_Array($GroupID);
$folderHierarchyMoveTo2 = $ldaUI->Display_Folder_Hierarchy_For_Swap($array, 0, $FolderID, $NoReload = 0, $MoveTo = 1);

if ($canManage) {
	$moveTo .= '
									<div id="moveToDiv" class="common_table_tool_movefolder">
										<div class="selectbox_group selectbox_group_filter" style="height:20px; line-height:20px;" id="moveToText">
											<a href="javascript:;" onClick="checkSpanDisplay(\'status_option2\',\'moveToText\')">-- ' . $Lang['DigitalArchive']['MoveTo'] . ' --</a>
										</div>
										<div id="status_option2" class="selectbox_layer select_folder_tree">
											' . $folderHierarchyMoveTo2 . '                                      
											<p class="spacer"></p>
										</div>
									</div>
								';
}
?>
				<?if(!$displayTagContent) {?>
			    <div class="common_table_tool_movefolder">
			    <?=$moveTo?>
			
			    </div>
			
			     <!-- folder move to at table tool end-->
				
     			
    
			    <span>|</span>
				<span id="toolBarDiv">
			     <a href="javascript:checkDownload()" class="tool_set" title="<?=$Lang['Button']['Download']?>"><?=$Lang['Button']['Download']?></a> 
			     <a href="javascript:checkFileExtract()" class="tool_undo" title="<?=$Lang['Button']['Extract']?>"><?=$Lang['Button']['Extract']?></a> 
		<?php if($canManage) { ?>
			     <a href="javascript:;" onClick="Display_Edit();" class="tool_edit" title="<?=$button_edit?>"><?=$button_edit?></a>
			     <a href="javascript:removeAction(0)" class="tool_delete" title="<?=$button_remove?>"><?=$button_remove?></a>
		<?php } ?>
			     </span>
			     <!--<span>| </span>
			     <span id="checkboxAllDiv"><label for="CheckAll"><?=$Lang['General']['All']?></label><input type="checkbox" id="CheckAll" onClick = "(this.checked)?setChecked(1,this.form,'DocumentID[]',1):setChecked(0,this.form,'DocumentID[]',1)"/></span>-->
			     </div>
				 
			     <a href="#TB_inline?height=500&width=750&inlineId=FakeLayer;" onClick="Display_Edit_Layer(0);" class="setting_row thickbox" title="<?=$button_edit?>" id="editLink2">&nbsp;</a>
			     <?}?>
					<ul class="view_toggle">
					<?if($TagId == ""){?>
			     	<li id="folder_list_li" <?if($GroupID != "" && $folderTreeSelect == "selected") echo 'class="selected"';?>><a href="javascript:;" onClick="Swap_Folder_Tree('tree_list')" class="icon_tree_list"><?=$Lang['DigitalArchive']['FolderTree']?></a></li>
			     	<li>|</li>
			     	<?}?>
			     	<li><?=$Lang['StudentRegistry']['View']?> : </li>
			     	<li><span style="position: relative; z-index: 0;"><a href="javascript:;" class="icon_view" onclick="Check_Tag_Folder_List()"><?=$Lang["DigitalArchive"]["Icon"]?></a></span></li>
			        <li class="selected"><span style="position: relative; z-index: 0;"><a href="#" class="icon_list"><?=$Lang["DigitalArchive"]["List"]?></a></span></li>
			        <?if($TagId == ""){?>
			        <li>|</li>
			        <?}?>
			     </ul><p class="spacer"></p>
                    </div></div></div>
					
                    <p class="spacer"></p>
					
					
			<!-- Log list start-->
			
			<div id="contentDiv">
	
			</div>
			<!--Log list end-->
		</td>
	</tr>
</table>

<iframe id='lyrShim2'  scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; visibility:hidden; display:none;'></iframe>
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"></div>

<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="TagId" id="TagId" value="<?=$TagId?>">
<input type="hidden" name="TargetFolderID2" id="TargetFolderID2" value="">
<input type="hidden" name="status_option_value" id="status_option_value" value="">
<input type="hidden" name="status_option2_value" id="status_option2_value" value="">
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="1">
<input type="hidden" name="deleteWholeThread" id="deleteWholeThread" value="">
<input type="hidden" name="leftPos" id="leftPos" value="">
<input type="hidden" name="topPos" id="topPos" value="">
<input type="hidden" name="folderTreeSelect" id="folderTreeSelect" value="<?=$folderTreeSelect?>">
</form>

<script language="javascript">
<? if(!$displayTagContent) { ?>
	Get_Navigation_Path();
<?}?>

<?= $callAjaxContent?>

//var LeftPos = document.getElementById("moveLeftDiv").offsetLeft; 
//var RightPos = document.getElementById("moveRightDiv").offsetLeft;
//var RightPos = $("#moveRightDiv").offset(); 
//alert(RightPos.left);

//var test = new ypSimpleScroll("myScroll", 350, 176, 845, 300, 150, 4000);
var test = new ypSimpleScroll("myScroll", 350, 176, 845, 300, 150, 7800);

$(document).ready(function() {
    //$( "#contentDiv" ).height($("#file_list").height());
    //alert("hihi");
   
  if($("#DA_tab_B1").length == 0) {
  	return;
  }
  var containerWidth = $("#DA_tab_B1").width();
  var minus = 0;
  if(navigator.appName == "Microsoft Internet Explorer") {
	minus = 40;
  } else {
	minus = 60;
  }
  containerWidth = parseInt(containerWidth) - minus;
  $('#myScrollContainer').width(containerWidth);
  
  //Get_Green_Tab();
  test.load();
  
  repositionGreenTab();
  $(window).resize(function(){
  	repositionGreenTab();
  });
   $(".icon_tree_list").click();
});

function repositionGreenTab()
{
	var dv = $('#myScrollContainer');
	var ref = $('#moveLeftDiv');
  	if(dv.length > 0 && ref.length > 0) {
  		dv.css('top',ref.position().top);
  	}
}

<? if($canManage) {?> 
	var browserName = navigator.appName; 
	if (browserName=="Microsoft Internet Explorer")
	{ 
		if ($("#EditNavigationDiv")){
			document.getElementById("navigationDiv").style.top = -50+"px";
		}
	}
	else 
	{
		document.getElementById("navigationDiv").style.position = "relative";
		document.getElementById("navigationDiv").style.zIndex = 0;
		document.getElementById("moveToText").style.position = "relative";
		document.getElementById("moveToText").style.zIndex = 0;
		document.getElementById("toolBarDiv").style.position = "relative";
		document.getElementById("toolBarDiv").style.zIndex = 0;
		//document.getElementById("checkboxAllDiv").style.position = "relative";
		//document.getElementById("checkboxAllDiv").style.zIndex = 0;
		document.getElementById("sortMenu").style.position = "relative";
		document.getElementById("sortMenu").style.zIndex = 0;
		document.getElementById("contentDiv").style.position = "relative";
		document.getElementById("contentDiv").style.zIndex = 0;
		//document.getElementById("contentDiv").style.overflow = "auto";
		if(document.getElementById("myScrollContent")) {
			document.getElementById("myScrollContent").style.zIndex = 1;
		}
		document.getElementById("status_option2").style.position = "absolute";
		document.getElementById("status_option2").style.zIndex = 9;
	}
	
<?}?>
</script>

<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>