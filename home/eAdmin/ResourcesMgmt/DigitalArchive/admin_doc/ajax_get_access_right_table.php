<?
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$ldaUI = new libdigitalarchive_ui();

$result = $ldaUI->Get_Access_Right_Group_Table($keyword, $CategoryID);

intranet_closedb();

echo $result;
?>