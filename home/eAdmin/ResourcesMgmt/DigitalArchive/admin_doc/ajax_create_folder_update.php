<?
// Editing by 
/*
 * 2019-05-15 (Bill): fixed SQL Injection + Cross-Site Scripting
 * 2016-01-20 (Carlos): Added [Allow IP] field.
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

$GroupID = IntegerSafe($GroupID);
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

$dataAry['Title'] = intranet_htmlspecialchars(trim($Title));
$dataAry['FileFolderName'] = intranet_htmlspecialchars(trim($Title));
$dataAry['Description'] = intranet_htmlspecialchars(trim($Description));
$dataAry['AllowIP'] = intranet_htmlspecialchars(trim($AllowIP));
$dataAry['ParentFolderID'] = IntegerSafe($FolderID);
$dataAry['GroupID'] = $GroupID;
$result = $lda->Create_Admin_Folder($dataAry);

intranet_closedb();

if(!is_numeric($result))
	echo $result;
else if($result)
	//echo $Lang['General']['ReturnMessage']['AddSuccess'];
	echo "AddSuccess";
else 
	//echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
	echo "AddUnsuccess";
?>