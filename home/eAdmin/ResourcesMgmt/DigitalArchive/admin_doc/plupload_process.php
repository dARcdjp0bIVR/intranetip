<?php
// Editing by 
/*
 * 2017-09-19 (Carlos): Cater intranetdata folder for KIS.
 * 2014-09-04 (Carlos): Base on security consideration, do not use sudo command to create root directory. Will create manully when install module. 
 * 2014-08-29 (Carlos): Use shell command to create root directory /digital_archive/admin_doc/ if php file operation fail
 */
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$libfilesystem = new libfilesystem();

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';

$linux_username = get_current_user(); // usually is junior on EJ, eclass if IP25

$TargetFolder = $_REQUEST['TargetFolder'];

$targetPath_Level1 = "/digital_archive/";
$targetPath_Level2 = "/temp/";


$target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath_Level1);

if ( !file_exists($target1stPath) || !is_dir($target1stPath)) {
	$resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
	/*
	if(!$resultCreatedFolder){ // try use shell command if php file operation fail
		$absolute_target1stPath = str_replace('//', '/', $intranet_root."../intranetdata/".$targetPath_Level1);
		shell_exec("sudo mkdir $absolute_target1stPath");
		shell_exec("sudo chown ".$linux_username.".".$linux_username." $absolute_target1stPath");
		shell_exec("sudo chmod 0777 $absolute_target1stPath");
	}
	*/
}

$target2ndPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath_Level1.$targetPath_Level2);
if ( !file_exists($target2ndPath) || !is_dir($target2ndPath)) {
	$resultCreatedFolder = $libfilesystem->folder_new($target2ndPath);
	/*
	if(!$resultCreatedFolder){ // try use shell command if php file operation fail
		$absolute_target2ndPath = str_replace('//', '/', $intranet_root."../intranetdata/".$targetPath_Level1.$targetPath_Level2);
		shell_exec("sudo mkdir $absolute_target2ndPath");
		shell_exec("sudo chown ".$linux_username.".".$linux_username." $absolute_target2ndPath");
		shell_exec("sudo chmod 0777 $absolute_target2ndPath");
	}
	*/
	if(file_exists($target2ndPath)) {
		chmod($target2ndPath, 0777);
	}
}

$plupload_tmp_path = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath_Level1.$targetPath_Level2.$TargetFolder);
if(file_exists($plupload_tmp_path)) {
	chmod($plupload_tmp_path, 0777);
}

$pluploadPar['targetDir'] = $plupload_tmp_path;

include_once($PATH_WRT_ROOT."includes/plupload.php");

intranet_closedb();
?>