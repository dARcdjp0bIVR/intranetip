<?php
# modifying : 
/**************************************************
 * 2019-05-15 (Bill): fixed SQL Injection + Cross-Site Scripting
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: added libdigitalarchive->Authenticate_DigitalArchive()
 * 2013-03-19 (Carlos): added js repositionGreenTab() to adjust the green tab position attached to left arrow anchor point
 * 2013-02-08 (Carlos): comment out unused call $ldaUI->Get_My_Group()
 * 2013-01-08 (Rita) : added $CurrentPage
 * 2013-01-02 (Carlos): changed to load folder hierarchy by ajax when mouse over
 * 2012-12-24 (Carlos): changed to load green tab (which is a list of folder structure) by ajax
 **************************************************/

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

//$cloud = new wordCloud();
$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

$lda->Authenticate_DigitalArchive();
/*
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && sizeof($lda->UserInGroupList())==0) {
	header("Location: /home/eAdmin/ResourcesMgmt/DigitalArchive/index.php");
	exit;	
}
*/

# Check Scaner Ready
$scannerReady = "";
if(file_exists($PATH_WRT_ROOT."rifolder/global.php")) {
	include_once($PATH_WRT_ROOT."rifolder/global.php");
	if(sizeof($PrinterIPLincense) > 0) {
		$scannerReady = 1;
	}
}

if($scannerReady) {
	$DivId = " bg_scanready";
	$DivId2 = "resource_adminsetting";
} else { 
	$DivId = "";
	$DivId2 = "resource_subject";
}

if(!isset($selected) || $selected=="") $selected = 3;
$selected = IntegerSafe($selected);

/*
$tagUsageArray = $lda->returnTagRankingArray($returnAmount=10, $lda->AdminModule);

# find the max amount of usage 
$max = 0;
for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	$max = ($tagUsageArray[$i]['Total']>$max) ? $tagUsageArray[$i]['Total'] : $max;
}

for($i=0, $i_max=sizeof($tagUsageArray); $i<$i_max; $i++) {
	list($tagid, $tagname, $total) = $tagUsageArray[$i];
	
	# define tag class 
	if($max==0 || $total/$max < 0.4) {
		$tagclass = "tag_small";
	} else if($total/$max > 0.8) {
		$tagclass = "tag_big";
	} else {
		$tagclass = "tag_normal";	
	}
	
	$cloud->addWord(array('word'=>intranet_htmlspecialchars($tagname), 'size'=>1, 'url'=>'displayTag.php?tagid='.$tagid, 'class'=>$tagclass));
}
*/

//$groupDisplay = $ldaUI->Get_My_Group();

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "PageReports_AdministrativeDocument";

$TAGS_OBJ = $ldaUI->PageTagObj(2);

# display elements on top right corner
$right_element = $ldaUI->Get_Right_Element();
$TAGS_OBJ_RIGHT[] = array($right_element);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

if($msg != "") {
	if($Lang['General']['ReturnMessage'][$msg]=="") {
		$displayMsg = $msg;	
	} else {
		$displayMsg = $Lang['General']['ReturnMessage'][$msg];	
	}
}

$ldaUI->LAYOUT_START($displayMsg);

echo $ldaUI->Include_JS_CSS();
?>

<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script language="javascript" src="scrolltext.js"></script>
<script>
function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) {
	$('#'+layername).hide();
	//document.getElementById(layername).style.display = "none";	
}

function showSpan(layername) {
	$('#'+layername).show();
	//document.getElementById(layername).style.display = "inline";	
}
/*
var ClickID = '';

var callback_member_list = {
	success: function ( o )
    {
		var tmp_str = o.responseText;
		document.getElementById('ref_list').innerHTML = tmp_str;
		DisplayPosition();
		//document.getElementById('ref_list').style.visibility = 'visible';
	}
}

function show_membere_list(click, id)
{
	Hide_Window("ref_list");
	ClickID = click;
	//document.getElementById('task').value = type;
	document.getElementById('gp_id').value = id;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_load_member.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_member_list);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition()
{
	MoveLayerPosition();
	
	document.getElementById('ref_list').style.visibility = 'visible';
}

function MoveLayerPosition() {
	id = document.getElementById('gp_id').value;

	var aryPosition = ObjectPosition(document.getElementById('doc_icon_'+id)); 
	var x = aryPosition[0];
	var y = aryPosition[1];
	
		if(navigator.appName == "Microsoft Internet Explorer") {
			x += 50;
			y += 15;
		} else {
			x += 20;
			y += 15;
		}

	document.getElementById('ref_list').style.left = x + "px";
	document.getElementById('ref_list').style.top = y + "px";
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility = 'hidden';
}

function ObjectPosition(obj) {     
	var curleft = 0;       
	var curtop = 0;       
	if (obj.offsetParent) {             
		do {                   
			curleft += obj.offsetLeft;                   
			curtop += obj.offsetTop;             
		} while (obj = obj.offsetParent);       
	}       
	return [curleft,curtop]; 
} 
*/
function swapHighLight(emtSelected) {
	
	if(emtSelected==1) {
		changeClass('tab_a1', 'current_tab_a');
		changeClass('tab_a2', '');
		changeClass('tab_a3', '');
	} else if(emtSelected==2) {
		changeClass('tab_a1', '');
		changeClass('tab_a2', 'current_tab_a');
		changeClass('tab_a3', '');
	} else if(emtSelected==3) {
		changeClass('tab_a1', '');
		changeClass('tab_a2', '');
		changeClass('tab_a3', 'current_tab_a');
	}
}

function changeClass(objID, classname)
{
	if ($('#'+objID).length>0)
	{
		$('#'+objID).attr("class",classname);
	}
}

function Display_Blue_Content(emtSelected) {
	swapHighLight(emtSelected);
	Get_Content(emtSelected);
}

function Display_Green_Content(groupId, folderId) {
	var str;
	
	if(folderId != 'undefined') {
		str = '&FolderID='+folderId;
	}
	self.location.href = 'fileList.php?GroupID='+groupId+str;	
}

function Get_Directory_List_Table() {
	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();
	
	self.location.href = "fileList.php?GroupID="+GroupID+"&FolderID="+FolderID;
}

function Load_Folder_Tree(layername) {
	/*
	$.post(
		"ajax_get_record.php2.php", 
		{ 
			flag: 'navigation',
			GroupID: $('#GroupID').val(),
			FolderID: $('#FolderID').val()
		},
		function(ReturnData)
		{
			$('#FolderTreeContentDiv').html(ReturnData);
			//UnBlock_Element("FolderTreeContentDiv");
		}
	);
	*/
	MM_showHideLayers(layername,'','show');
}

var globalFolderCache = {};
function Get_Folder_Hierarchy(divId,groupId) {
	if(globalFolderCache[groupId]) {
		//$('#'+divId).html(globalFolderCache[groupId]);
	}else {
		Block_Element(divId);
		$('#'+divId).load(
			'ajax_get_record.php',
			{
				'flag':'GetFolderHierarchy',
				'GroupID':groupId,
				'SelectedGroupID':$('#GroupID').val()
			},function(data){
				globalFolderCache[groupId] = data;
				UnBlock_Element(divId);
			}
		);
	}
}

function Hide_Folder_Tree(layername) {
	MM_showHideLayers(layername,'','hide');
	//MM_showHideLayers(FolderTreeContentDiv,'','hide');
}

function Get_My_Group() {

	Block_Element("contentDiv");
	
	$.post(
		"ajax_get_record.php", 
		{ 
			flag: 'Groups2',
			ViewAmount: $('#viewAmount').val(),
			OrderBy: $('#orderBy').val(),
			OrderBy2: $('#orderBy2').val()
		},
		function(ReturnData)
		{
			$('#contentDiv').html(ReturnData);
			UnBlock_Element("contentDiv");
			reassignZindex();
		}
	);		
	
}

function Get_Latest_Record() {

	Block_Element("contentDiv");
	
	$.post(
		"ajax_get_record.php", 
		{ 
			flag: 'Latest',
			ViewAmount: $('#viewAmount').val(),
			OrderBy: $('#orderBy').val(),
			OrderBy2: $('#orderBy2').val()
		},
		function(ReturnData)
		{
			$('#contentDiv').html(ReturnData);
			UnBlock_Element("contentDiv");
			reassignZindex();
		}
	);		
	
}

function Get_LastView_Record() {

	Block_Element("contentDiv");
	
	$.post(
		"ajax_get_record.php", 
		{ 
			flag: 'LastView',
			ViewAmount: $('#viewAmount').val(),
			OrderBy: $('#orderBy').val(),
			OrderBy2: $('#orderBy2').val()
		},
		function(ReturnData)
		{
			$('#contentDiv').html(ReturnData);
			UnBlock_Element("contentDiv");
			reassignZindex();
		}
	);		
	
}

function Get_Content(emtSelected) {
	
	if(emtSelected=='undefined') {
		// do nothing
	} else {
		//alert(emtSelected);
		swapHighLight(emtSelected);
	}
	
	if($('#tab_a3').hasClass('current_tab_a')) {
		Get_My_Group();
	} else if($('#tab_a1').hasClass('current_tab_a')) {
		Get_Latest_Record();
	} else {
		Get_LastView_Record();
	}
	
}

function moveLeft() {
	move('-=100px');
}

function moveRight() {
	move('+=100px');
}

function move(px) {
	$('#green_inner_layer').animate({
		'marginLeft' : px
	});
}
/*	
function getPosition(objName) {
	var pos = $("#"+objName).position(); 
	$("#leftPos").val(pos.left);
	$("#topPos").val(pos.top);
	alert(pos.top+"|"+pos.left);
}
*/
function DisplayGroupHierachy(getObjName, setObjName) {
	
	var pos = $("#"+getObjName).position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	topPos = parseInt(topPos) + 30;
	
	$("#"+setObjName).css({"position": "absolute", "top": topPos, "left": leftPos });	
}

function DisplayAdvanceSearch() {
	
	var pos = $("#AdvanceSearchText").position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	
	topPos = parseInt(topPos) + 15;
	leftPos = parseInt(leftPos) - 682;
	
	$("#DA_search").css({"position": "absolute", "top": topPos, "left": leftPos, "z-index":9 });
	MM_showHideLayers('DA_search','','show');	
}

function Get_Green_Tab()
{
	Block_Element('DA_tab_B1');
	$('#myScrollContent').load(
		'ajax_get_green_tab.php',
		{},
		function(data){
			UnBlock_Element('DA_tab_B1');
		}
	);
}

function displayQuickviewFolder(GroupID)
{
	$.fancybox({
		href: 'ajax_get_record.php?flag=GetQuickviewFolder&GroupID='+GroupID,
		type: 'ajax',
		width: 640,
		height: 480,
		scrolling: 'no',
		autoDimensions: false
	});
}
</script>

<form name="form1" id="form1" method="post">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
<?php if (!$sys_custom['digital_archive']['hide_groupmenu']) { ?>
			<!-- DA_tab [Start] -->
			<div class="DA_tab">
				<? if(false && $_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {?>
    				<div class="Conntent_tool">
    					<a href="access_right.php" class="setting_content"><?=$Lang['DigitalArchive']['AdminSettings']?></a>
    				</div>
			    <? } ?>
			    
				<!-- DA_tab_A [Start] -->
				<?=$ldaUI->Get_Blue_Tab(3)?>
				<!-- DA_tab_A [End] -->
				
				<!-- DA_tab_B [Start] -->
				<div class="DA_tab_B" id="DA_tab_B1">
					
					<ul><li class="arrow_prev" id="moveLeftIcon"><a title="<?/*=$Lang['DigitalArchive']['MoveLeft']*/?>" onMouseOver="test.scrollWest()" onMouseOut="test.endScroll()"></a></li></ul>
					<div id="myScrollContainer" style="position:absolute;">
						<div id="myScrollContent" style="position:absolute;">
							<?php 
								echo $ldaUI->Get_Green_Tab();
							?>
						</div>
					</div>
					
					<div style="position:relative;float:right;z-index:0;"><ul><li class="arrow_next" id="moveRightIcon"><a title="<?/*=$Lang['DigitalArchive']['MoveRight']*/?>" onMouseOver="test.scrollEast()" onMouseOut="test.endScroll()" style="float:right;"></a></li></ul></div>
					
				</div>
				<!-- DA_tab_B [End] -->
				
			</div>
			<!-- DA_tab [End] -->
<?php } ?>  
			
			<p class="spacer"></p>
			
			<!-- Filter [Start] -->
			<p class="spacer"></p>
			
			<div class="thumb_list_tab" id="FilterDiv"> 
				<?=$Lang['Btn']['View']?> : 
				<select name="viewAmount" id="viewAmount" onChange="Get_Content()">
					<option selected="selected" value="5"><?=$Lang['DigitalArchive']['Latest']?> 5 <?=$Lang['DigitalArchive']['Records']?></option>
					<option value="10"><?=$Lang['DigitalArchive']['Latest']?> 10 <?=$Lang['DigitalArchive']['Records']?></option>
					<option value="20"><?=$Lang['DigitalArchive']['Latest']?> 20 <?=$Lang['DigitalArchive']['Records']?></option>
					<option value="50"><?=$Lang['DigitalArchive']['Latest']?> 50 <?=$Lang['DigitalArchive']['Records']?></option>
				</select>&nbsp;&nbsp;&nbsp;
				<?=$Lang['DigitalArchive']['SortBy']?> : 
				<select name="orderBy" id="orderBy" onChange="Get_Content()">
					<option value="filename"><?=$Lang["DigitalArchive"]["Title"]?></option>
					<option value="date" selected="selected"><?=$Lang['DigitalArchive']['ModifiedDate']?></option>
					<option value="location"><?=$Lang['DigitalArchive']['Location']?></option>
				</select>
				<select name="orderBy2" id="orderBy2" onChange="Get_Content()">
					<option value="ASC"><?=$Lang['DigitalArchive']['ASC']?></option>
					<option value="DESC" selected="selected"><?=$Lang['DigitalArchive']['DESC']?></option>
				</select>
			</div>
			<!-- Filter [End] -->
			
			<!-- Log list start-->
			<div id="contentDiv">
			
			</div>
			<!--Log list end-->
			
		</td>
	</tr>
</table>

<input type="hidden" name="GroupID" id="GroupID" value="">
<input type="hidden" name="FolderID" id="FolderID" value="">
<input type="hidden" name="status_option_value" id="status_option_value" value="">
<input type="hidden" name="leftPos" id="leftPos" value="">
<input type="hidden" name="topPos" id="topPos" value="">
</form>

<script language="javascript">
Get_Content(<?=$selected?>);

//var test = new ypSimpleScroll("myScroll", 350, 176, 790, 300, 150, 4000);
var test = new ypSimpleScroll("myScroll", 385, 176, 790, 300, 150, 10000);

$(document).ready(function() {
    if($("#DA_tab_B1").length == 0){
    	return;
    }
    var containerWidth = $("#DA_tab_B1").width();
    var minus = 0;
    if(navigator.appName == "Microsoft Internet Explorer") {
    	minus = 46;
    } else {
    	minus = 66;
    }
    containerWidth = parseInt(containerWidth) - minus;
    $('#myScrollContainer').width(containerWidth);
    
    // Get_Green_Tab();
    test.load();
    
    repositionGreenTab();
    $(window).resize(function(){
    	repositionGreenTab();
    });
});

function reassignZindex()
{
	var browserName = navigator.appName; 
	if (browserName=="Microsoft Internet Explorer")
	{
		// do nothing
	}
	else
	{
		document.getElementById("FilterDiv").style.position = "relative";
		document.getElementById("FilterDiv").style.zIndex = 0;
		document.getElementById("contentDiv").style.position = "relative";
		document.getElementById("contentDiv").style.zIndex = 0;
		if(document.getElementById("myScrollContent")) {
			document.getElementById("myScrollContent").style.zIndex = 1;
		}
	}
}

function repositionGreenTab()
{
	var dv = $('#myScrollContainer');
	var ref = $('#moveLeftIcon');
  	if(dv.length > 0 && ref.length > 0) {
  		dv.css('top',ref.position().top);
  	}
}
</script>

<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>