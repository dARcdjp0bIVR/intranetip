<?php
// editing by : 
/* 2017-08-31 (Carlos): Cater add users by group with values "GXXX".
 * 2013-09-12 (Carlos): exclude existing members to add, otherwise add duplicated member would fail
 */
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$lsr = new libdigitalarchive();
$lg = new libgrouping();

$tempAry = array();
$groupIdAry = array();
for($i=0, $i_max=sizeof($student); $i<$i_max; $i++) {
	if(substr($student[$i],0,1) == 'G'){
		$groupIdAry[] = substr($student[$i],1);
	}else{
		$tempAry[] = is_numeric($student[$i]) ? $student[$i] : substr($student[$i],1);
	}
	//$temp = is_numeric($student[$i]) ? $student[$i] : substr($student[$i],1);
	//if($temp!="")
	//	$tempAry[] = $temp; 	
}
if(count($groupIdAry)>0){
	$group_user_ids = $lg->returnUserIDByGroup($groupIdAry);
	$group_user_ids = $lg->returnUserIDWithCondition(" AND RecordType='".USERTYPE_STAFF."' AND UserID IN (".implode(",",(array)$group_user_ids).")");
	if(count($group_user_ids)>0){
		$tempAry = array_merge($tempAry, $group_user_ids);
		$tempAry = array_values(array_unique($tempAry));
	}
}
$result = true;
if(sizeof($tempAry)>0){
	$existUserIdAry = $lsr->memberInMgmtGroup($GroupID);
	$newUserIdAry = array_values(array_diff($tempAry, $existUserIdAry, array('','0')));
	if(sizeof($newUserIdAry)>0) {
		$result = $lsr->Add_Group_Member($GroupID, $newUserIdAry);
	}
}

intranet_closedb();

$msg = ($result) ? "AddSuccess" : "AddUnsuccess";

header("Location: access_right_user.php?msg=$msg&GroupID=".$GroupID);
exit;
?>