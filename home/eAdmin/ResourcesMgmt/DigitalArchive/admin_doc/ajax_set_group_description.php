<?php
//editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

$GroupDescription= trim(urldecode(stripslashes($_REQUEST['GroupDescription'])));
$GroupID = $_REQUEST['GroupID'];

# access right checking
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}


if ($lda->Set_Group_Description($GroupDescription, $GroupID)) {
	echo $Lang['StaffAttendance']['ChangeGroupDescriptionSuccess'];
}
else {
	echo $Lang['StaffAttendance']['ChangeGroupDescriptionFail'];
}


intranet_closedb();
?>