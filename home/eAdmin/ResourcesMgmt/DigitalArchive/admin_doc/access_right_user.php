<?php

/**********************************
 *  Date	: 	20130214 (Rita)
 *  Details	: 	add $CurrentPageArr['DigitalArchiveAdminMenu'] for menu display
 * 
 * 	Date	:	2013-01-08 (Rita)				
 * 	Details	: 	Comment CSS style sheet					
 * 
 **********************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$laccessright = new libaccessright();

/*
if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/
if(!$_REQUEST['GroupID'] || $_REQUEST['GroupID'] == '')
{
	header("Location: group_access_right_detail.php");
	intranet_closedb();
	exit();
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

$content = $ldaUI->Get_Group_Access_Right_Users_Index($_REQUEST['GroupID'], $xmsg);
?>
<!--<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">-->
<?=$content?>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
<div id=tempdiv></div>
<script type="text/JavaScript" language="JavaScript">

<?
if($msg!="")
	echo "Get_Return_Message(\"".$Lang['General']['ReturnMessage'][$msg]."\")\n";
?>


function goBack() {
	self.location.href = "access_right.php";	
}


// dom function 
{
function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		Get_Group_User_List();
	else
		return false;
}
}

// ajax function
{
function Get_Group_User_List()
{
	var PostVar = {
			"GroupID": $('Input#GroupID').val(),
			"Keyword": encodeURIComponent($('Input#Keyword').val())
			}

	Block_Element("UserListLayer");
	$.post('ajax_get_group_user_list.php',PostVar,
					function(data){
						if (data == "die") 
							window.top.location = '/';
						else {
							$('div#UserListLayer').html(data);
							Thick_Box_Init();
							UnBlock_Element("UserListLayer");
						}
					});
}

function Get_Add_Member_Form()
{
	var PostVar = {
		"GroupID": $('Input#GroupID').val()
	};
	
	$.post('ajax_get_add_group_member_form.php',PostVar,
		function(data) {
			if (data == "die") 
				window.top.location = '/';
			else 
				$('div#TB_ajaxContent').html(data);
		});
}

function Add_Member()
{
	var PostVar = {
		"GroupID":$('input#GroupID').val(),
		"AddUserID[]":Get_Selection_Value('AddUserID[]','Array')
	}
	
	Block_Thickbox();
	$.post('ajax_add_member.php',PostVar,
		function (data) {
			if (data == "die") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
				Get_Group_User_List();
				window.top.tb_remove();
				Scroll_To_Top();
			}
		});
}

function Delete_Member()
{
	var StaffID = Get_Check_Box_Value('StaffID[]','Array');
	
	if (StaffID.length > 0) {
		if (confirm('<?=$Lang['StaffAttendance']['AccessRightDeleteGroupMemberWarning']?>')) {
			var PostVar = {
				"GroupID":$('input#GroupID').val(),
				"StaffID[]":StaffID
			}
			
			$.post('ajax_delete_member.php',PostVar,
				function(data) {
					
					if (data == "die") {
						window.top.location = '/';
					}
					else {
						Get_Return_Message(data);
						Get_Group_User_List();
						Scroll_To_Top();
					}
				});
		}
	}
	else {
		alert('<?=$Lang['StaffAttendance']['SelectAtLeastOneWarning']?>');
	}
}

function Set_Role(memberType)
{

	var StaffID = Get_Check_Box_Value('StaffID[]','Array');

	if(StaffID.length==0) {
		
		alert(globalAlertMsg1);
		
	} else {
		
		var PostVar = {
			"GroupID": $('input#GroupID').val(),
			"StaffID[]": StaffID,
			MemberType: memberType
		}
		
		$.post('ajax_set_group_role.php',PostVar,
			function(data) {
				
				if (data == "die") {
					window.top.location = '/';
				}
				else {
					Get_Return_Message(data);
					Get_Group_User_List();
					Scroll_To_Top();
				}
			});
		
	}
	
}

}

function Check_Group_Title(GroupTitle,GroupID) {
	var GroupID = GroupID || "";
	var PostVar = {
			"GroupID": GroupID,
			"GroupTitle": encodeURIComponent(GroupTitle)
			}
	var ElementObj = $('div#GroupTitleWarningLayer');
	
	if (Trim(GroupTitle) != "") {
		$.post('ajax_check_group_title.php',PostVar,
				function(data){
					if (data == "die") 
						window.top.location = '/';
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = 'none';
					}
					else {
						ElementObj.html('<?=$Lang['StaffAttendance']['GroupTitleDuplicateWarning']?>');
						ElementObj.show('fast');
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = '';
					}
				});
	}
	else if(Trim(GroupTitle) == ""){
		ElementObj.html('<?=$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('GroupTitleWarningRow')) 
			document.getElementById('GroupTitleWarningRow').style.display = '';
	}
}

function Show_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', '');
	//LayerObj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	//LayerObj.style.backgroundPosition = "center right";
	//LayerObj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', 'edit_dim');
	//LayerObj.style.backgroundImage = "";
	//LayerObj.style.backgroundPosition = "";
	//LayerObj.style.backgroundRepeat = "";
}

// jEditable function 
function Init_JEdit_Input(objDom)
{
	var WarningLayer = "div#GroupTitleWarningLayer";
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	if (Trim($(WarningLayer).html()) == "" && ElementObj[0].revert != value)
    	{
    		if($('Input#GroupID').val() == '')
	    	{
	    		ElementObj.html(value);
	    		$('span#GroupTitleNavLayer').html(value);
				$('Input#GroupTitle').val(value);
	    	}
	    	else
	    	{
	    		var PostVar = {
	    			//"GroupID":ElementObj.attr("id"),
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupTitle":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_rename_group.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    			ElementObj.html(value);
						$('span#GroupTitleNavLayer').html(value);
						$('Input#GroupTitle').val(value);
		    		});
	    	}
		}
		else {
			ElementObj[0].reset();
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	$(WarningLayer).html('');
	    	$(WarningLayer).hide();
	  	}
  	  }
  	);
  
  $(objDom).keyup
  (
  	 function()
  	 {
		var GroupTitle = Trim($('form input').val());
		//var GroupID = $(this).attr('id');
		//var GroupTitle = Trim($('Input#GroupTitle').val());
		var GroupID = $('Input#GroupID').val();
		Check_Group_Title(GroupTitle,GroupID);
	 }
  );
}

function Init_JEdit_Input2(objDom)
{
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	ElementObj.html(value);
		$('Input#GroupDescription').val(value);
		
		if($('Input#GroupID').val() != '' && ElementObj[0].revert != value)
		{
			var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupDescription":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_set_group_description.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    		});
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	
	  	}
  	  }
  	);
}


function Init_JEdit_Input3(objDom)
{
	var WarningLayer = "div#GroupCodeWarningLayer";
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	
    	if (Trim($(WarningLayer).html()) == "" && ElementObj[0].revert != value)
    	{
    		if($('Input#GroupID').val() == '')
	    	{
	    		ElementObj.html(value);
	    		$('span#GroupCodeNavLayer').html(value);
				$('Input#GroupCode').val(value);
	    	}
	    	else
	    	{
	    		var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupCode":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_set_group_code.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    			ElementObj.html(value);
						$('span#GroupCodeNavLayer').html(value);
						$('Input#GroupCode').val(value);
		    		});
	    	}
		}
		else {
			ElementObj[0].reset();
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	$(WarningLayer).html('');
	    	$(WarningLayer).hide();
	  	}
  	  }
  	);
  
  $(objDom).keyup
  (
  	 function()
  	 {
		var GroupCode = Trim($('form input').val());
		var GroupID = $('Input#GroupID').val();
		Check_Group_Code(GroupCode,GroupID);
		
	 }
  );
  
}

function UpdateCategoryLinkage()
{
	var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupCategoryID":$('Input#GroupCategoryID').val()
		};
    				     
    	$.post('ajax_set_group_category_linkage.php',PostVar,
    		function (data) {
    			Get_Return_Message(Trim(data));
    			//ElementObj.html(value);
				//$('span#GroupCodeNavLayer').html(value);
				//$('Input#GroupCode').val(value);
    		});
}

function jsClickEdit()
{
	$('span#EditableGroupTitle').click();
}

function jsClickEdit2()
{
	$('span#EditableGroupDescription').click();
}
function jsClickEdit3()
{
	$('span#EditableGroupCode').click();
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

$(document).ready(
	function()
	{		
		Init_JEdit_Input('span.jEditInput');
		Init_JEdit_Input2('span.jEditInput2');
		Init_JEdit_Input3('span.jEditInput3');
	}
);
</script>