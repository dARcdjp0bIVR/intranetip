<?php
// Editing by 
/*
 * 2019-05-15 (Bill): fixed SQL Injection + Cross-Site Scripting
 * 2016-01-20 (Carlos): Added [Allow IP] field.
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$FolderID = IntegerSafe($FolderID);
$GroupID = IntegerSafe($GroupID);
### Handle SQL Injection + XSS [END]

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
?>
<script language="javascript">

function checkCreateFolder() {
	
	resetAllSpanInnerHtml();
	
	var obj = document.formCreateFolder;
	error_no = 0;
	focus_field = "";
	
	if(!check_text_30(obj.Title, "<?=$i_alert_pleasefillin.$Lang["DigitalArchive"]["FolderName"]?>.","div_err_Title"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Title";  
	}
	if(obj.Title.value.length > <?=$lda->MaxLengthOfFileName?>) {
		alert("<?=$Lang['DigitalArchive']['FolderNameWarning']?>");
		if(focus_field=="")	focus_field = "File";
		error_no++;
	}
	if(!validateFilename(obj.Title.value, '<?=$Lang['DigitalArchive']['jsFolderNameCannotWithSpecialCharacters']?>')) 
	{
		return false;	
	}
	var allow_ip = $.trim(obj.AllowIP.value);
	var ip_valid = true;
	if(allow_ip != ''){
		var ips = allow_ip.split('\n');
		for(var i=0;i<ips.length;i++){
			if(!validateIPAddress(ips[i])){
				$('#AllowIP_error').show();
				ip_valid = false;
				error_no++;
			}
		}
	}
	if(ip_valid){
		$('#AllowIP_error').hide();
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		Create_Folder_Action();
	}
}

function resetAllSpanInnerHtml() {
	$('#div_err_Title').html('');
}
</script>

<br style="clear:both" />
<form name="formCreateFolder" id="formCreateFolder" method="POST" action="">

<table class='form_table_v30'>
	<tr>
		<td class='field_title' width='45%'><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["FolderName"]?></td>
		<td width='55%'><input type="text" name="Title" id="Title" value="" class="textboxtext"><span id="div_err_Title"></span></td>
	</tr>
	<tr>
		<td class='field_title' width='45%'><?=$Lang["DigitalArchive"]["Description"]?></td>
		<td width='55%'><input type="text" name="Description" id="Description" value="" class="textboxtext"></td>
	</tr>
	<tr>
		<td class='field_title' width='45%'><?=$Lang['DigitalArchive']['AllowIPAddresses']?></td>
		<td width='55%'>
			<?=$ldaUI->GET_TEXTAREA("AllowIP", "")?>
			<div id="AllowIP_error" style="color:red;display:none;"><?=$Lang['DigitalArchive']['InvalidIPAddress']?></div>
			<div class="tabletextremark">
				<?=str_replace('<!--IP-->',getRemoteIpAddress(),$Lang['DigitalArchive']['AllowIPAddressesRemark'])?>
			</div>
		</td>
	</tr>
</table>
<span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span>
<br style="clear:both" />
<div class="edit_bottom">
	<br style="clear:both" />
	<?=
		$ldaUI->GET_ACTION_BTN($button_submit, "button", "checkCreateFolder()")."&nbsp;".
		$ldaUI->GET_ACTION_BTN($button_cancel, "button", "window.tb_remove();")
	?>
</div>

<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
</form>

<?
echo $ldaUI->FOCUS_ON_LOAD("formCreateFolder.Title"); 
intranet_closedb();
?>