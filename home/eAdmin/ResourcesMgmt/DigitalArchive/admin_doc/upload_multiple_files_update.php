<?php
// Editing by 
/*
 * 2019-08-16 (Ray): add duplicate file can overwrite by owner
 * 2019-05-15 (Bill): fixed SQL Injection + Cross-Site Scripting
 * 2017-09-19 (Carlos): Cater intranetdata folder for KIS.
 * 2014-09-04 (Carlos): Base on security consideration, do not use sudo command to create root directory. Will create manully when install module. 
 * 2014-08-29 (Carlos): Use shell command to create root directory /digital_archive/admin_doc/ if php file operation fail
 * 2013-07-08 (Carlos): Created for saving files uploaded with plupload
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$FolderID = IntegerSafe($FolderID);
$GroupID = IntegerSafe($GroupID);

$Description = intranet_htmlspecialchars(trim($Description));
// $TagName = intranet_htmlspecialchars(trim($TagName));

if($folderTreeSelect != '' && $folderTreeSelect != 'selected') {
    $folderTreeSelect = '';
}
### Handle SQL Injection + XSS [END]

$libfilesystem = new libfilesystem();
$lgs = new libgeneralsettings();
$lda = new libdigitalarchive();

$resultCreatedFolder = false;
$linux_username = get_current_user(); // usually is junior on EJ, eclass if IP25

$intranetdata_foldername = isKIS()? $intranet_db.'data' : 'intranetdata';
$TargetFolder = $_REQUEST['TargetFolder'];

$result = array();
$returnMsg = '';
if (!empty($TargetFolder))
{
	# Restricted file formats and maximum file size
	$tempFileFormatAry = $lda->Get_File_Format_Setting();
	$FileFormatAry = array();
	$maxFileSizeSettings = $lgs->Get_General_Setting("DigitalArchive", array("'MaximumFileSize'"));
	$MaxFileSize = $maxFileSizeSettings['MaximumFileSize']; // MB
	$RestrictFileSize = is_numeric($MaxFileSize) && $MaxFileSize > 0;
	$MaxFileSizeInBytes = floatval($MaxFileSize) * 1024 * 1024;
	for($i=0;$i<count($tempFileFormatAry);$i++) {
		$file_format = strtolower(trim($tempFileFormatAry[$i]['FileFormat']));
		if($file_format != '') {
			$FileFormatAry[] = $file_format;
		}
	}
	$RestrictFileFormat = count($FileFormatAry) > 0;
	
	# create folder
	$targetPath = "/digital_archive/";
	$targetPath_2ndLevel = "/admin_doc/";
	$target1stPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath);
	if (!file_exists($target1stPath) || !is_dir($target1stPath)) {
		$resultCreatedFolder = $libfilesystem->folder_new($target1stPath);
		/*
		if(!$resultCreatedFolder){ // try use shell command if php file operation fail
			$absolute_target1stPath = str_replace('//', '/', $intranet_root."/../../".$targetPath);
			shell_exec("sudo mkdir $absolute_target1stPath");
			shell_exec("sudo chown ".$linux_username.".".$linux_username." $absolute_target1stPath");
			shell_exec("sudo chmod 0777 $absolute_target1stPath");
		}
		*/
	}
	
	$target2ndPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath.$targetPath_2ndLevel);
	if (!file_exists($target2ndPath) || !is_dir($target2ndPath)) {
		$resultCreatedFolder = $libfilesystem->folder_new($target2ndPath);
		/*
		if(!$resultCreatedFolder){
			$absolute_target2ndPath = str_replace('//', '/', $intranet_root."../../".$targetPath.$targetPath_2ndLevel);
			shell_exec("sudo mkdir $absolute_target2ndPath");
			shell_exec("sudo chown ".$linux_username.".".$linux_username." $absolute_target2ndPath");
			shell_exec("sudo chmod 0777 $absolute_target2ndPath");
		}
		*/
	}
	
	$targetFullPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/".$targetPath.$targetPath_2ndLevel);
	$tempPath = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp/".$TargetFolder);
	chmod($targetFullPath, 0777);
	
	$tempFileAry = $libfilesystem->return_folderlist($tempPath);
	$numOfFiles = count($tempFileAry);
	for($i=0; $i<$numOfFiles; $i++)
	{
		# get file name
		$origFileName = $libfilesystem->get_file_basename($tempFileAry[$i]);
		if (empty($origFileName)) continue;
		
		# Check file size
		$fileSize = filesize($tempFileAry[$i]);
		if($RestrictFileSize && $fileSize > $MaxFileSizeInBytes) {
			$result[] = false;
			$returnMsg = 'FileSizeExceedLimit';
			continue;
		}
		
		# Extension
		$ext = substr($libfilesystem->file_ext($origFileName),1);
		
		# Check file format
		if($RestrictFileFormat && in_array(strtolower($ext),$FileFormatAry)){
			$result[] = false;
			$returnMsg = 'FileTypeIsNotSupported';
			continue;
		}
		
		# Check if zip files contains restricted file types
		if($RestrictFileFormat && $ext == 'zip')
		{
			$isZipValid = true;
			$tempUnzipfolder = str_replace('//', '/', $PATH_WRT_ROOT."../$intranetdata_foldername/digital_archive/temp/".$TargetFolder."/tmp_unzip");
			if(file_exists($tempUnzipfolder)) {
				$libfilesystem->deleteDirectory($tempUnzipfolder);
			}
			$libfilesystem->folder_new($tempUnzipfolder);
			if(file_exists($tempUnzipfolder)) {
				chmod($tempUnzipfolder, 0777);
			}
			
			$libfilesystem->file_unzip($tempFileAry[$i],$tempUnzipfolder);
			$tempUnzipFileAry = $libfilesystem->return_folderlist($tempUnzipfolder);
			for($j=0;$j<count($tempUnzipFileAry);$j++) {
				$tempUnzipFileExt = strtolower( substr($libfilesystem->file_ext($tempUnzipFileAry[$j]),1) );
				if(in_array($tempUnzipFileExt, $FileFormatAry)) {
					$isZipValid = false;
					break;
				}
			}
			if(file_exists($tempUnzipfolder)) {
				$libfilesystem->deleteDirectory($tempUnzipfolder);
			}
			if(!$isZipValid) {
				$result[] = false;
				$returnMsg = 'ZipFileContainsRestrictedFileType';
				continue;
			}
		}
		
		$timestamp = date('YmdHis');
		
		$sqlSafeOrigFileName = $lda->Get_Safe_Sql_Query($origFileName);
		$Title = $sqlSafeOrigFileName;
		
		# encode file name
		$targetFileHashName = "u".$UserID."_".$timestamp.$i;
		$targetFile = $targetFullPath.$targetFileHashName;
		
		# Check duplication
		$IsDuplicateFlag = $lda->Is_Duplicate_Admin_Document($GroupID, $FolderID, $Title, $sqlSafeOrigFileName);
		if($IsDuplicateFlag) {
			$DuplicateDocumentDetail = $lda->Get_Duplicate_Admin_Document($GroupID, $FolderID, $Title, $sqlSafeOrigFileName);
		}
		$canManageOthers = 0;
		$adminInGroup = $lda->returnAdmin();
		if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
			// is super admin ?
			$canManageOthers = 1;
		} else if($adminInGroup[$GroupID]) {
			// does this admin user has group manage others file access right?
			if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-ADMIN_OTHERS-MANAGE")) {
				$canManageOthers = 1;	
			}
		} else if($lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OTHERS-MANAGE")) {
			// does this member user has group manage others file access right?
			$canManageOthers = 1;	
		} else if($IsDuplicateFlag && $DuplicateDocumentDetail[0]['InputBy'] == $UserID && $lda->CHECK_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID."-MEMBER_OWN-MANAGE")) {
			$canManageOthers = 1;
		}

		if($IsDuplicateFlag && !$canManageOthers){
			$result[] = false;
			continue;
		}
		
		# upload file
		$uploadResult = $libfilesystem->lfs_copy($tempFileAry[$i], $targetFile);
		if(file_exists($targetFile)){
			chmod($targetFile, 0777);
		}
		
		if($uploadResult)
		{
			# add record to DB
			$dataAry = array();
			$dataAry['Title'] = $sqlSafeOrigFileName;
			$dataAry['Description'] = $Description;
			$dataAry['FileFolderName'] = $sqlSafeOrigFileName;
			$dataAry['FileHashName'] = $targetFileHashName;
			$dataAry['FileExtension'] = strtolower($ext);
			$dataAry['VersionNo'] = 1;
			$dataAry['IsLatestFile'] = 1;
			$dataAry['ParentFolderID'] = $FolderID;
			$dataAry['GroupID'] = $GroupID;
			$dataAry['SizeInBytes'] = $fileSize;
			
			//if(isset($RecordID) && $RecordID!="") {	# update
				//$RecordID = $lda->updateResourceRecord($RecordID, $dataAry);
			//} else {								# insert
			$RecordID = $lda->insertAdminRecord($dataAry);
			//}
			$result[] = $RecordID;
			
			if($TagName != "") {
				$lda->Update_Admin_Doc_Tag_Relation($TagName, $RecordID, $lda->AdminModule);
			} else {
				$inHeritTag = 1;
			}
			
			if($IsDuplicateFlag) {
				$lda->Update_Duplicate_Record($RecordID, $GroupID, $FolderID, $Title, $sqlSafeOrigFileName, $inHeritTag);
			}
		}
	}
	
	// Do clean up
	$libfilesystem->deleteDirectory($tempPath);
	$sql = "DELETE FROM DIGITAL_ARCHIVE_TEMP_UPLOAD_FOLDER WHERE UserID = '".$_SESSION['UserID']."' AND Folder = '".$TargetFolder."'";
	$lda->db_db_query($sql);
}
intranet_closedb();

if($returnMsg != '') {
	$msg = $returnMsg;
}else if(!in_array(false,$result)){
	$msg = "AddSuccess";
}else {
	$msg = "AddUnsuccess";
}

if($viewMode == "icon") {
	header("Location: fileList_Icon.php?GroupID=$GroupID&FolderID=$FolderID&folderTreeSelect=$folderTreeSelect&msg=$msg");
}else{
	header("Location: fileList.php?GroupID=$GroupID&FolderID=$FolderID&folderTreeSelect=$folderTreeSelect&msg=$msg");
}
?>