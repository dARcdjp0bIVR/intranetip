<?php
// Editing by 
/*
 * 2018-05-30 (Carlos): Added error handler for plupload.
 * 2013-07-08 (Carlos): Created. Included in ajax_upload_file.php
 */
/*
 * Variables required: 
 * $pluploadButtonId - button id for triggering file selection dialog
 * $pluploadDropTargetId - droppable element id for drag and drop files
 * $pluploadFileListDivId - container element id for displaying uploaded file name, file size and upload status
 * $tempFolderPath - temp folder name (not the whole folder path, because it is visible in js coding) for storing uploaded files
 */
include_once($intranet_root."/includes/libdb.php");
include_once($intranet_root."/includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($intranet_root."/includes/libdigitalarchive.php");
include_once($intranet_root."/includes/libinterface.php");

$lgs = new libgeneralsettings();
$lda = new libdigitalarchive();
$lfs = new libfilesystem();
$linterface = new interface_html();

/*
$pluploadButtonId = 'UploadButton';
$pluploadDropTargetId = 'DropFileArea';
$pluploadFileListDivId = 'FileListDiv';
$pluploadContainerId = 'pluploadDiv';

$tempFolderPath = '/u'.$_SESSION['UserID'].'_'.time();
*/

$tempFileFormatAry = $lda->Get_File_Format_Setting();
$FileFormatAry = array();
$maxFileSizeSettings = $lgs->Get_General_Setting("DigitalArchive", array("'MaximumFileSize'"));
$MaxFileSize = $maxFileSizeSettings['MaximumFileSize']; // MB
$MaxFileSizeInBytes = $MaxFileSize * 1024 * 1024; // bytes

for($i=0;$i<count($tempFileFormatAry);$i++) {
	$file_format = strtolower(trim($tempFileFormatAry[$i]['FileFormat']));
	if($file_format != '') {
		$FileFormatAry[] = $file_format;
	}
}

echo $linterface->Include_Plupload_JS_CSS();
?>
<style type="text/css">
.DropFileArea
{
	width:100%;
	height:100px;
	line-height:100px;
	text-align:center;
	vertical-align:middle;
	border:2px dashed #CCCCCC;
}

.DropFileAreaDragOver 
{
	width:100%;
	height:100px;
	line-height:100px;
	text-align:center;
	vertical-align:middle;
	border: 2px dashed #00FF55;
}
</style>
<script type="text/javascript" language="JavaScript">
var TargetFolder = '<?=$tempFolderPath?>';
var BannedFileFormats = [<?= count($FileFormatAry)>0? '"'.implode('","',$FileFormatAry).'"' : ""?>];
var MaxFileSizeInBytes = <?=$MaxFileSizeInBytes?>;

function jsDeleteTempFile(fileId)
{
	var filename = $('div#'+fileId + ' span').html();
	$.post(
		'plupload_remove_file.php',
		{
			'TargetFolder':TargetFolder,
			'FileName':encodeURIComponent(filename)
		},
		function(data){
			$('div#'+fileId).remove();
		}
	);
}

function jsDeleteTempFileByName(filename)
{
	$.post(
		'plupload_remove_file.php',
		{
			'TargetFolder':TargetFolder,
			'FileName':encodeURIComponent(filename)
		},
		function(data){
			
		}
	);
}

function jsIsFileUploadCompleted()
{
	var totalSelected = uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<uploader.files.length;i++) {
		if(uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	
	return totalDone == totalSelected;
}

function jsNumberOfFileUploaded()
{
	var dv = $('#<?=$pluploadFileListDivId?> div');
	return dv.length;
}

var uploader = new plupload.Uploader({
			        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
			        autostart : true,
			        browse_button : '<?=$pluploadButtonId?>',
			        drop_element : '<?=$pluploadDropTargetId?>',
			        container : '<?=$pluploadContainerId?>',
					flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
					silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
					url : 'plupload_process.php?TargetFolder=<?=$tempFolderPath?>',
		<?php if(is_numeric($MaxFileSize) && $MaxFileSize > 0) { ?>		
					max_file_size : '<?=$MaxFileSize?>mb',
		<?php } ?>
					chunk_size : '1mb',
					unique_names :false
			    });

uploader.bind('Init', function(up, params) {
    if (uploader.features.dragdrop) {
		var target = document.getElementById("<?=$pluploadDropTargetId?>");
	      
	    target.ondragover = function(event) {
	        event.dataTransfer.dropEffect = "copy";
	        this.className = "DropFileAreaDragOver";
	    };
	      
	    target.ondragenter = function() {
	        this.className = "DropFileAreaDragOver";
	    };
	      
	    target.ondragleave = function() {
	        this.className = "DropFileArea";
	    };
	      
	    target.ondrop = function(event) {
	    	event.preventDefault();
	        this.className = "DropFileArea";
	    };
	      
	    $('#<?=$pluploadDropTargetId?>').show();
      	
      	if($('#TB_ajaxContent').length > 0) {
      		$('#TB_ajaxContent').css('overflow-x','hidden');
      	}
    }else{
    	$('#<?=$pluploadDropTargetId?>').hide();
    }
});

uploader.init();

uploader.bind('FilesAdded', function(up, files) {
    $.each(files, function(i, file) {
    	var ok = true;
    	if(MaxFileSizeInBytes > 0 && file.size > MaxFileSizeInBytes) {
    		ok = false;
    	}
    	var ext = Get_File_Ext(file.name);
    	if(ext && BannedFileFormats.length>0 && BannedFileFormats.indexOf(ext) != -1){
    		ok = false;
    	}
    	if(ok) {
	        $('#<?=$pluploadFileListDivId?>').append(
	            '<div id="' + file.id + '">' +
	            '<span>' + file.name + '</span> (' + plupload.formatSize(file.size) + ') <b></b>' +
	        	'</div>'
	        );
    	}else{
    		jsDeleteTempFileByName(file.name);
    		up.removeFile(file);
    	}
    });
	
    up.refresh(); // Reposition Flash/Silverlight
    up.start();
});

uploader.bind('UploadProgress', function(up, file) {
    $('#' + file.id + " b").html(file.percent + "%");
});

uploader.bind('UploadComplete', function(up, files) {
	$.each(files, function(i, file) {
		if( $('#' + file.id + ' a').length == 0) {
			$('#' + file.id).append('<a href="javascript:void(0);" onclick="jsDeleteTempFile(\''+file.id+'\');"> [ <?=$Lang['Btn']['Delete']?> ]</a>');
		}
	});
});

uploader.bind('Error', function(up, err){
	console.log(err);
	if(err.code == -600){
	<?php if(is_numeric($MaxFileSize) && $MaxFileSize > 0) { ?>		
		alert('<?=str_replace('<!--SIZE-->',$MaxFileSize,$Lang['DigitalArchive']['jsMaxFileSizeWarning'])?>');
	<?php }else{ ?>
		alert(err.message);
	<?php } ?>
	}else{
		alert(err.message);
	}
});

</script>