<?
// editing by 
/*
 * 2014-05-21 (Carlos): fix $flag=='navigation' [Edit folder button]
 * 2013-07-26 (Henry): added flag=Groups2, FileList2, FileList_Icon
 * 2013-07-24 (Carlos): added flag=GetQuickviewFolder
 * 2012-12-31 (Carlos): added flag=GetFolderHierarchy
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

if($flag=='Latest') {
	$content = $ldaUI->Get_Latest_Table($ViewAmount, $OrderBy, $OrderBy2);
}
else if($flag=='Groups') {
	$content = $ldaUI->Get_My_Group_Table();
}
else if($flag=='Groups2') {
	$content = $ldaUI->Get_My_Group_Table2();
}
else if($flag=='LastView') {
	$content = $ldaUI->Get_Last_View_Table($ViewAmount, $OrderBy, $OrderBy2);
}
else if($flag=='FileList') {
	$content = $ldaUI->Display_File_List_Table($GroupID, $FolderID, $skipFolder=1, $OrderBy, $OrderBy2);
}
else if($flag=='FileList2') {
	$content = $ldaUI->Display_File_List_Table2($GroupID, $FolderID, $skipFolder=1, $OrderBy, $OrderBy2);
}
else if($flag=='FileList_Icon') {
	$content = $ldaUI->Display_File_List_Table_Icon($GroupID, $FolderID, $skipFolder=1, $OrderBy, $OrderBy2);
}
else if($flag=='navigation') {
	$browser = $_SERVER['HTTP_USER_AGENT'];
	//if(strstr($browser,"MSIE")) $top = "top:-10px;";
	
	$content = "<div class='navigation_v30'>";
	$content .= $ldaUI->Get_Folder_Navigation($FolderID, "", $IsListTable=1);
	$content .= "</div>";
	
	$editableRecordIdByGroupID = $lda->Get_Editable_Record_ID_By_GroupID($GroupID);
	
	if(($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] || in_array($FolderID, $editableRecordIdByGroupID)) && $FolderID!=0) { 
		$content .= '
			<div class="table_row_tool" id="EditNavigationDiv" style="position:relative;left:5px;'.$top.'">
				<a href="#TB_inline?height=500&width=750&inlineId=FakeLayer" onClick="Display_Edit_Layer(1);" class="thickbox edit_dim" title="'.$Lang['Btn']['Edit']." ".$Lang['DigitalArchive']['Folder'].'"></a>
				<a href="javascript:void(0);" onClick="removeAction(1)" class="delete_dim" title="'.$Lang['Btn']['Delete']." ".$Lang['DigitalArchive']['Folder'].'"></a>
			</div>';
	}
	
	$content .= "<br style=\"spacer\" />";
}
else if($flag=='Tag') {
	if(isset($TagId) && $TagId!="") {
		$content = $ldaUI->Get_Tag_Record_Table($TagId, $OrderBy, $OrderBy2);
	}
}
else if($flag=='TagNavigation') {
	$tagInfo = returnModuleTagNameByTagID($TagID);
	$tagname = $tagInfo[0];
	
	$content . '<div class="navigation_v30_v2">
                      		
							<span>'.$Lang['DigitalArchive']['Tag'].'</span>
							<span>'.$tagname.'</span>';
	if($_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {						
	$content .= '						
								<div class="table_row_tool">
									<a href="javascript:;" class="edit_dim" title="'.$Lang['Btn']['Edit']." ".$Lang['DigitalArchive']['Tag'].'"></a>
									<a href="javascript:;" onClick="Remove_Tag('.$TagId.')" class="delete_dim" title="'.$Lang['Btn']['Delete']." ".$Lang['DigitalArchive']['Tag'].'"></a>
								</div>';
	}
	$content .= '				<br style="spacer" />
							
						</div>';
}
else if ($flag == 'GetFolderHierarchy') {
	$GroupID = $_REQUEST['GroupID'];
	$SelectedGroupID = $_REQUEST['SelectedGroupID'];
	$array = $lda->Get_Folder_Hierarchy_Array($GroupID);
	echo $folderHierarchySwap = $ldaUI->Display_Folder_For_Swap($array, 0, $FolderID=0, "", "", $GroupID, $SelectedGroupID);
	echo '<p class="spacer"></p>';
}
else if ($flag == 'GetQuickviewFolder') {
	$GroupID = $_REQUEST['GroupID'];
	$SelectedGroupID = $_REQUEST['SelectedGroupID'];
	$array = $lda->Get_Folder_Hierarchy_File_Count_Array($GroupID);
	echo $folderHierarchySwap = $ldaUI->Get_Quickview_Folder_Div($array, 0, $FolderID=0, "", "", $GroupID, $SelectedGroupID);
}

echo $content;

intranet_closedb();
?>