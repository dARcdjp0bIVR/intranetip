<?php
// Editing by 
/*
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();
$lgs = new libgeneralsettings();

//$lda->Authenticate_DigitalArchive();

$settings = $lgs->Get_General_Setting("DigitalArchive", array("'AccessSetting'"));

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Settings_AccessSetting";

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalArchive']['AccessSetting'],"",1);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}
$ldaUI->LAYOUT_START($Msg);

echo $ldaUI->Include_JS_CSS();

?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
function checkForm()
{
	if($('input[name="AccessSetting"]:checked').length == 0){
		return false;
	}
	
	return true;
}

function js_Toggle_View(opt)
{
	if(opt == 2){
		$('.EditView').show();
		$('.DisplayView').hide();
	}else{
		$('.EditView').hide();
		$('.DisplayView').show();
	}
}
</script>
<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="access_setting_update.php" onsubmit="return checkForm();">
<div class="table_board">
	<div style="text-align:right; height:20px;"><?=$linterface->Get_Small_Btn($Lang['Btn']['Edit'], "button", "js_Toggle_View(2)", "", "", "DisplayView")?></div>
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><label for="<?=$settings['AccessSetting']=='1'?"AccessSettingY":"AccessSettingN"?>"><?=$Lang['DigitalArchive']['AccessSettingDescription']?></label></td>
			<td>
				<div class="EditView" style="display:none">
					<?=$linterface->Get_Radio_Button("AccessSettingY", "AccessSetting", "1", $settings['AccessSetting']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0)?>
					<?=$linterface->Get_Radio_Button("AccessSettingN", "AccessSetting", "0", $settings['AccessSetting']!='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0)?>
				</div>
				<div class="DisplayView"><?=$settings['AccessSetting']=='1'?$Lang['General']['Yes']:$Lang['General']['No']?></div>
			</td>
		</tr>
	</table>
	<div class="edit_bottom_v30 EditView" style="display:none">
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","")?>
		<?='&nbsp;'.$linterface->Get_Action_Btn($Lang['Btn']['Cancel'],"button","js_Toggle_View(1)")?>
	</div>
</div>
</form>
<?php
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>
