<?php
# modifying : 
/*
 * 2015-04-08 (Carlos) [ip2.5.6.5.1]: simplied advance search logic. 
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: added libdigitalarchive->Authenticate_DigitalArchive()
 * 
 * Date :   2014-05-07  (Tiffany)
 *          add the All matches and Partial matches to the tags on advance search.
 * 
 * Date	:	2013-07-16	(Henry Chan)
 * 			fixed the wrong title of the page
 * 			2013-07-16  (Carlos)
 * 			call thickbox with js instead of by link at tool Edit button 
 * 
 * Date	:	2013-07-04	(Henry Chan)
 * 			added the SQL statement to support searching with group
 *
 * Date	:	2013-07-03	(Henry Chan)
 * 			modified the SQL statement to support searching with tag independently
 *			added hidden fields
 * 
 * Date	:	2013-04-25	(yuen)
 * 			modified Get_Admin_Doc_AdvanceSearchResult_SQL() to support searching with tag
 * 
 * 2013-01-07 (Carlos): Removed hidden elements StartDate and EndDate which have same id and name with that in search panel
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");


if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);


/*
if ($page_size_change == 1)
{
    //setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    //$ck_page_size = $numPerPage;
}
*/

if($order == "") $order = 1;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();

$lda->Authenticate_DigitalArchive();

# update time of last visit time of group
if(!isset($_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]) || $_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]=="" || $_SESSION['DigitalArchive']['LastGroupViewTime'][$GroupID]!=date('Y-m-d H:i:s')) {
	$lda->updateLastVisitOfDigitalArchive($GroupID);	
}

$groupInfo = $lda->getGroupInfo($GroupID);

$navTitle = $Lang["DigitalArchive"]["SearchResult"];

$flag = ($flag=="") ? FUNCTION_ADVANCE_SEARCH : $flag;

$tag = trim($tag);
if($tag!=""){
	$tags_handle = explode(",", $tag);
    for($i=0;$i<count($tags_handle);$i++){
    	$tags_handle_new[$i] = trim($tags_handle[$i],'[]"\\');
    }
	$tag = implode(",", $tags_handle_new);		
}

if ($advance)
{
	if(strlen($StartDate)==10) $StartDate .= " 00:00:00";
	if(strlen($EndDate)==10) $EndDate .= " 23:59:59";
	//Henry Modified
	$tag = trim($tag);
	$str = trim($str);
	$createdBy = trim($createdBy);
	//Tiffany Added
//	$documentIDs = $lda->AllMatches($tag);
	//old tag search
	//$tagIDs = $lda->SearchTags($tag);
	
	//if ($tagIDs!=-1 && $tag!='')
/*
	if( $tag!=''&&$choose_tag=="all"&&$documentIDs!="")
	{
		//$conds_keyword = " AND tu.TagID IN ({$tagIDs}) ";
			$conds_keyword = "AND tu.DocumentID IN ({$documentIDs}) ";
	}	
	elseif( $tag!=''&&$choose_tag=="part")
	{
		$tags_part = explode(",", $tag);
	    for($i=0;$i<count($tags_part);$i++){
	    	$tags_part_new[$i] = "'".$tags_part[$i]."'";
	    }
		$tag_part = implode(",",$tags_part_new);
		if($tag_part!="")
			$conds_keyword = "AND t.TagName IN ({$tag_part}) ";
		else $conds_keyword = " AND FALSE ";
		

	}
	else{
		$conds_keyword = " AND FALSE ";
	}
*/
	if($tag != '')
	{
		$tagID_csv = $lda->SearchTags(explode(",",$tag), $choose_tag=="all");
		$conds_keyword = " AND tu.TagID IN (".$tagID_csv.") ";
	}
	
	//End Modified
	if($StartDate!='' && $EndDate!='') $conds .= " AND (rec.DateInput BETWEEN '$StartDate' AND '$EndDate')";	
	//Henry Modified
	if($str!='') $conds .= " AND (rec.Title LIKE '%$str%' OR rec.FileFolderName LIKE '%$str%' OR rec.Description LIKE '%$str%')";
	if ($tag!='') $conds .= $conds_keyword;
	//End Modified
	if($createdBy!='') $conds .= " AND (USR.ChineseName LIKE '%$createdBy%' OR USR.EnglishName LIKE '%$createdBy%' OR USR.UserLogin LIKE '%$createdBy%')";
	if(sizeof($TagID)>0) $conds .= " AND tu.TagID IN (".implode(',',$TagID).")";
	//else $conds .= " AND rec.GroupID='-1' ";
	
	//for searching by group(s)
	$tempGroupIDArr = '';
	if($_GET['GroupIDArr'] == ''){
		$tempGroupIDArr = explode(',',$_REQUEST['selectedGroups']);
	}
	else{
	$tempGroupIDArr = $_GET['GroupIDArr'];
	}
	if($tempGroupIDArr != '' && $tempGroupIDArr[0] != '' && $tempGroupIDArr[0] != '-1'){
		$firstRecord = 0;
		foreach ($tempGroupIDArr as $groupid){
			
			if($firstRecord == 0){
				$conds .= " AND (rec.GroupID='$groupid'";
				$firstRecord = 1;
			}
			else{
				$conds .= " OR rec.GroupID='$groupid'";
			}
		}
		$conds .= ")";
	}
	
} else {
	$keyword = trim($keyword);
	$tagIDs = $lda->SearchTags($keyword);
	if ($tagIDs!=-1)
	{
		$conds_keyword = " OR tu.TagID IN ({$tagIDs}) ";
	}
	if($keyword!='') $conds .= " AND (rec.Title LIKE '%$keyword%' OR rec.FileFolderName LIKE '%$keyword%' OR rec.Description LIKE '%$keyword%' {$conds_keyword})";
	//$conds .= " AND rec.GroupID='$GroupID'";	
	
}
$table = $ldaUI->Get_Display_AdvanceSearchResult_Table($field, $order, $pageNo, $flag, $conds, $GroupID, $tagIDs);

$tagInfo = $lda->Get_Tag_Used_In_Group($GroupID);
$TagSelection = getSelectByArray($tagInfo, 'name="TagID[]" id="TagID[]" multiple size="5"', $TagID, 0, 1);

# action button
$actionBtn = '
<td valign="bottom"><div class="common_table_tool">';
//$actionBtn .= '<a href="javascript:unlike_record(document.form1, \'RecordID[]\')" class="tool_delete">'.$Lang["DigitalArchive"]["Unlike"].'</a>';
if($flag!=FUNCTION_TAG) {
	//$actionBtn .= '<a href="#TB_inline?height=600&width=750&inlineId=FakeLayer" onClick="Display_Edit_File();return false;" class="tool_edit setting_row thickbox" title="'.$button_edit.'">'.$button_edit.'</a>';
	$actionBtn .= '<a href="javascript:void(0);" onClick="Display_Edit_File();return false;" class="tool_edit setting_row" title="'.$button_edit.'">'.$button_edit.'</a>';
	$actionBtn .= '<a href="javascript:removeAction(document.form1, \'RecordID[]\')" class="tool_delete">'.$button_delete.'</a>';
} else {
	//$actionBtn .= '<a href="#TB_inline?height=600&width=750&inlineId=FakeLayer" onClick="Display_Edit_Tag();return false;" class="tool_edit setting_row thickbox" title="'.$button_edit.'">'.$button_edit.'</a>';
	$actionBtn .= '<a href="javascript:void(0);" onClick="Display_Edit_Tag();return false;" class="tool_edit setting_row" title="'.$button_edit.'">'.$button_edit.'</a>';
	$actionBtn .= '<a href="javascript:remove_tag(document.form1, \'TagID[]\')" class="tool_delete">'.$button_delete.'</a>';	
}


$actionBtn .= '</div></td>';

$CurrentPageArr['DigitalArchive'] = 1;

//$TAGS_OBJ = $ldaUI->PageTagObj(2);
$TAGS_OBJ[] = array($Lang["DigitalArchive"]["SearchResult"]);

# display elements on top right corner
$right_element = $ldaUI->Get_Right_Element($_GET);
$TAGS_OBJ_RIGHT[] = array($right_element);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

echo $ldaUI->Include_JS_CSS();
?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?=$LAYOUT_SKIN?>/css/reading_scheme.css" rel="stylesheet" type="text/css">


<script language="javascript">
var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";

function DisplayAdvanceSearch() {
	
	var pos = $("#AdvanceSearchText").position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	
	topPos = parseInt(topPos) + 15;
	leftPos = parseInt(leftPos) - 682;
	
	$("#DA_search").css({"position": "absolute", "top": topPos, "left": leftPos, "z-index":9 });
	MM_showHideLayers('DA_search','','show');	
}


function goCancel() {
	document.form1.reset();
	document.form2.reset();
	showHideLayer('advanceSearchFlag', 0);
}

function Update_Like_Status(FunctionName, FunctionID, Status)
{
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	
	$.post(
		"ajax_update_like_status.php",
		{
			Status:Status,
			FunctionName:FunctionName,
			FunctionID:FunctionID
		},
		function(ReturnData){
			var d = "Div_"+FunctionName+"_"+FunctionID;
			if(d != 'undefined') {
				$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			}
			//Get_Resource_MyFavourite_Table();
		}
	)
	
}

function showHideLayer(flag, val) {
	var obj = document.form1;
	if(val==1) {
		$('#'+flag).val(val);
		$('#resource_search').hide();	
		$('#resource_search_adv').show();
	} else {
		$('#'+flag).val(val);
		$('#resource_search_adv').hide();
		$('#resource_search').show();	
	}
	obj.reset();
}

function goBack() {
	self.location.href = "index.php";
}


function Display_Edit_File() {

	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	if(DocumentID.length==1) {
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',600,750);
		$.post(
			"ajax_edit_file.php", 
			{ 
				DocumentID: DocumentID,
				flag: '<?=$flag?>',
				SubjectID: '<?=$SubjectID?>'
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				//Get_Directory_List_Table();	
			}
		);	
	} else {
		alert(globalAlertMsg1);	
	}		
}

function Display_Edit_Tag() {

	var TagID = Get_Check_Box_Value('TagID[]','Array');

	if(TagID.length==1) {
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',600,750);
		$.post(
			"ajax_edit_tag.php", 
			{ 
				TagID: TagID,
				flag: '<?=$flag?>'
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
				//Get_Directory_List_Table();	
			}
		);	
	} else {
		alert(globalAlertMsg1);	
	}		
}

function removeAction() {
	
	var DocumentID = Get_Check_Box_Value('DocumentID[]','Array');

	var GroupID = $('#GroupID').val();
	var FolderID = $('#FolderID').val();
	
	if(DocumentID.length>0) {
		
		$('#deleteWholeThread').val(1);
		
		var PostVar = Get_Form_Values(document.getElementById("form1"));
		
		if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
			$.post(
				"ajax_delete_record.php", PostVar,
				function(ReturnData)
				{
					
					//$('#tempdiv').html(ReturnData);
					$('#msg').val(ReturnData);
					Get_Return_Message(ReturnData);
					Get_Directory_List_Table();
				}
			);
		}		
	} else {
		alert(globalAlertMsg2);
	}	
}

function Get_Directory_List_Table() {
	document.form1.submit();
}	
/*
function submitSearch(advSearch) {
	//$('#advanceSearchFlag').val(advSearch);
	//document.form1.action = "displayResult.php";
	$('#advance').val(advSearch);
	document.form1.action = "searchResult.php";
	document.form1.submit();
		
}
*/
function remove_tag(obj, emt) {
	if(countChecked(obj, emt)==0) {
		alert(globalAlertMsg2);
	} else {
		if(confirm("<?=$Lang['DigitalArchive']['jsDeleteAlertMsg']?>")) {
			obj.action = "remove_tag_update.php";
			obj.method = "POST";
			obj.submit();
		}
	}
}

function getCheckDateRange(flag) {
	if(flag) {
		$('#StartDate').attr("disabled", false);
		$('#EndDate').attr("disabled", false);
	} else {
		$('#StartDate').attr("disabled", true);
		$('#EndDate').attr("disabled", true);		
	}
}

<?
if($msg!="")
	echo "Get_Return_Message(\"$msg\")";
?>
</script>

<div id="content" style="padding-left: 10px; width=100%;">

<br style="clear:both">
<div id="content" style="padding-left: 10px; width=100%;">
	<div class="navigation_v30 navigation_2">
		<a href="javascript:goBack();"><?=$i_frontpage_menu_home?></a>
		<? if(sizeof($groupInfo)>0) {?>
		<a href="list.php?GroupID=<?=$GroupID?>&FolderID=<?=$FolderID?>"><?=intranet_htmlspecialchars($groupInfo['GroupTitle'])?></a>
		<? } ?>
		<span><?=$navTitle?></span>
	</div>
	<br style="clear:both">
	
	<table width="99%" border="0" cellspacing="0" cellpadding="0">
		
		<tr>
			<td>
				<form name="form1" id="form1">
				
				<? if($flag!=FUNCTION_TAG) echo $searchOptionAdv; ?>
				<div class="reading_board" id="resource_popular_list">
					<div class="reading_board_top_left"><div class="reading_board_top_right">
						<h1><span><?=$Lang["DigitalArchive"]["List"]?></span></h1>
					</div></div>
					
					<div class="reading_board_left"><div class="reading_board_right">
						
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<?=$actionBtn?>
							</tr>
						</table> 
						
						<div id="tableContentDiv">
						<?=$table?>
						</div>

					
					</div></div>
					
					<div class="reading_board_bottom_left"><div class="reading_board_bottom_right"></div></div>
					<p class="spacer"></p>
				</div>
<input type='hidden' name='msg' id='msg' value='' />
<input type='hidden' name='flag' id='flag' value='<?=$flag?>' />
<input type='hidden' name='GroupID' id='GroupID' value='<?=$GroupID?>' />
<input type='hidden' name='FolderID' id='FolderID' value='<?=$FolderID?>' />
<input type='hidden' name='deleteWholeThread' id='deleteWholeThread' value='' />
<input type="hidden" name="advanceSearchFlag" id="advanceSearchFlag" value="1">
<input type="hidden" name="keyword" id="keyword" value="<?=intranet_htmlspecialchars($keyword)?>">
<input type="hidden" name="str" id="str" value="<?=intranet_htmlspecialchars($str)?>">
<!-- Henry Modified -->
<input type="hidden" name="tag" id="tag" value="<?=intranet_htmlspecialchars($tag)?>">
<input type="hidden" name="choose_tag" id="choose_tag" value="<?=intranet_htmlspecialchars($choose_tag)?>">
<input type="hidden" name="selectedGroups" id="selectedGroups" value="<?=intranet_htmlspecialchars(implode(',',$tempGroupIDArr))?>">
<!--<input type="hidden" name="StartDate" id="StartDate" value="<?=$StartDate?>">-->
<!--<input type="hidden" name="EndDate" id="EndDate" value="<?=$EndDate?>">-->
<input type="hidden" name="createdBy" id="createdBy" value="<?=$createdBy?>">
<input type="hidden" name="advance" id="advance" value="<?=$advance?>">
<input type="hidden" name="byDateRange" id="byDateRange" value="<?=$byDateRange?>">
				</form>
			</td>
		</tr>
	</table>
</div>


</div>

<script language="javascript">
	<?=$jsFunction?>
	showHideLayer('advanceSearchFlag', '<?=$advanceSearchFlag?>');
	getCheckDateRange('<?=$byDateRange?>');
</script>

<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>