<?php
# modifying : 

##### Change Log [Start] #####
# 	Date	: 	20130214 (Rita)
# 			  	add $CurrentPageArr['DigitalArchiveAdminMenu'] for menu display
#
#	Date	:	2013-01-08 (Rita)
#				Comment CSS style sheet					
#
#	Date	:	2012-03-12 (Henry Chow)
#				edit javascript checking on GroupTitle and GroupCode (fields cannot be blanked)
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Settings_GroupCategory";

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

//echo $ldaUI->Include_JS_CSS();

echo $ldaUI->Get_Group_Category_Edit_Form($GroupCategoryID);

$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>

<script type="text/JavaScript" language="JavaScript">
function CheckForm()
{
	var obj = document.form1;
	var CategoryCode = $.trim($('input#CategoryCode').val());
	if(Trim(CategoryCode)=="")
    {
            alert("<?=$Lang['DigitalArchive']['NoGroupCode']?>");
            obj.CategoryCode.focus();
            return false;
	}
	var CategoryTitle = $.trim($('input#CategoryTitle').val());
	if(Trim(CategoryTitle)=="")
    {
            alert("<?=$Lang['DigitalArchive']['NoGroupName']?>");
            obj.CategoryTitle.focus();
            return false;
	}
	var DisplayOrder = $.trim($('input#DisplayOrder').val());
	if(Trim(DisplayOrder)=="")
    {
            alert("<?=$Lang['DigitalArchive']['NoDisplayOrder']?>");
            obj.DisplayOrder.focus();
            return false;
	}
	
	document.form1.submit();
	
}
</script>

<?=	$linterface->FOCUS_ON_LOAD("form1.CategoryCode") ?>