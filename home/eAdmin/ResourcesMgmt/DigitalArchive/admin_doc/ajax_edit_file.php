<?
// Editing by 
/*
 * 2019-05-15 (Bill): fixed SQL Injection + Cross-Site Scripting
 * 2016-01-20 (Carlos): Added [Allow IP] field.
 * 2014-05-29 (Tiffany): add group shown in press add tag button
 * 2014-04-07 (Tiffany): add folders to tags button
 * 2013-09-25 (Carlos): multicomplete jquery version is conflicted with global included jquery-1.3.2, use $.noConflict() to avoid confliction
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$FolderID = IntegerSafe($FolderID);
if(isset($DocumentID)) {
    $DocumentID = IntegerSafe($DocumentID);
}
$GroupID = IntegerSafe($GroupID);
### Handle SQL Injection + XSS [END]

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

$libdb = new libdb(); 
if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && !$lda->CHECK_SECTION_ACCESS("DIGITAL_ARCHIVE-GROUP".$GroupID)) {
	$lda->NO_ACCESS_RIGHT_REDIRECT();
	exit;		
}

if(sizeof($_POST)==0) {
	echo "<script language='javascript'>window.tb_remove();</script>";
	exit;	
} 

$documentID = is_array($DocumentID)? $DocumentID[0] : ((!isset($DocumentID) || $DocumentID=="")? $FolderID : $DocumentID);
//$documentID = $FolderID;

$data = $lda->Get_Admin_Doc_Info($documentID);
$tagDisplay = $lda->Display_Tag_By_Admin_Document_ID($documentID, 'textWithoutLink');

$FormButton = $ldaUI->GET_ACTION_BTN($button_submit, "button", "checkForm()")."&nbsp;".
				$ldaUI->GET_ACTION_BTN($button_reset, "reset", "resetAllSpanInnerHtml()")."&nbsp;".
				$ldaUI->GET_ACTION_BTN($button_cancel, "button", "window.tb_remove()");

if(!$data['IsFolder'])
{
	$tagAry = returnModuleAvailableTag($lda->AdminModule, 1);
	$tagNameAry = array_values($tagAry);
	
	$delim = "";
	if(sizeof($tagNameAry > 0)) {
		foreach($tagNameAry as $tag) {
			$availableTagName .= $delim."\"".addslashes($tag)."\"";
			$delim = ", ";
		}
	}
}

//echo $ldaUI->Include_JS_CSS();
# add folders to tags button
$tagAarry = explode(',', $tagDisplay);
for($j=0;$j<count($tagAarry);$j++){
    $this_tag = trim($tagAarry[$j]);
    if(!isset($exist_tag[$this_tag]))
    {
    	$exist_tag[$this_tag] = true;
    }
}

$file_folderID = $documentID;
$folders = "";
$groupTitle = "";
$isFolder = false;
do{
    $sql = "select dr.ParentFolderID,dr.FileFolderName,rg.GroupTitle from DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD as dr LEFT JOIN DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP as rg ON dr.GroupID=rg.GroupID where DocumentID = '".$file_folderID."'";
    $folder_array = $libdb->returnArray($sql);
    if($isFolder==true && !isset($exist_tag[$folder_array[0][1]])){
        $folders .= $folder_array[0][1].",";
        $exist_tag[$folder_array[0][1]] = true;
    }
    $file_folderID = $folder_array[0][0];
    
    $isFolder = true;
} while ($file_folderID!=0);
if(!isset($exist_tag[$folder_array[0][2]])){
	$groupTitle = $folder_array[0][2];
}
$folders = $folders.$groupTitle;
//$folders = rtrim($folders,",");

if($tagDisplay=="") {
    $newTags = $folders;
}
else if($folders=="") {
    $newTags = $tagDisplay;
}
else {
    $newTags = $tagDisplay.",".$folders;
}
$newTags = intranet_htmlspecialchars($newTags);
?>

<script language="">
function checkForm() {
	resetAllSpanInnerHtml();
	
	var obj = document.editForm;	
	error_no = 0;
	focus_field = "";
	
	if(!check_text_30(obj.Title, "<?=$i_alert_pleasefillin.$Lang["DigitalArchive"]["Title"]?>.","div_err_Title"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Title";  
	}
	if(obj.Title.value.length > <?=$lda->MaxLengthOfFileName?>) {
		alert("<?= ($data['IsFolder'] ? $Lang['DigitalArchive']['FolderNameWarning'] : $Lang['DigitalArchive']['FileNameWarning'])?>");
		if(focus_field=="")	focus_field = "Title";
		error_no++;
	}
	if(!validateFilename(obj.Title.value, '<?=$Lang['DigitalArchive']['jsDisplayNameCannotWithSpecialCharacters']?>')) 
	{
		return false;	
	}
	<? if(!$data['IsFolder']){?>
	if(!validateFilename(obj.FileName.value, '<?=$Lang['DigitalArchive']['jsFileNameCannotWithSpecialCharacters']?>')) 
	{
		return false;	
	}	
	if(obj.FileName.value.indexOf(".")==-1) {
		alert("<?=$Lang['DigitalArchive']['jsFileNameInvalid']?>");
		return false;	
	} else {
		var pos = obj.FileName.value.indexOf(".");
		var ext = obj.FileName.value.substring(pos+1);
		if(ext.length<=1) {
			alert("<?=$Lang['DigitalArchive']['jsFileNameInvalid']?>");
			return false;
		}	
	}
	<?}?>
	
	<? if($data['IsFolder']){ ?>
	var allow_ip = $.trim(obj.AllowIP.value);
	var ip_valid = true;
	if(allow_ip != ''){
		var ips = allow_ip.split('\n');
		for(var i=0;i<ips.length;i++){
			if(!validateIPAddress(ips[i])){
				$('#AllowIP_error').show();
				ip_valid = false;
				error_no++;
			}
		}
	}
	if(ip_valid){
		$('#AllowIP_error').hide();
	}
	<? } ?>
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		checkFileNameDuplication();
	}
}

function checkFileNameDuplication() {
	
	var obj = document.editForm;
	
	<? if($data['IsFolder']){?>
	$('#FileName').val(obj.Title.value);
	<?}?>
	var PostVar = Get_Form_Values(document.getElementById("editForm"));

	$.post(
		"ajax_check_duplicate_record.php",PostVar,
		function(ReturnData)
		{
			if(ReturnData==1) {
				$('#div_err_Title').html('<font color="red"><?=$Lang["DigitalArchive"]["Title"].$Lang['DigitalArchive']['jsIsDuplicated']?></font>');
				obj.Title.select();
				return false;	
				
			} else {
				Update_Edit_Info();
			}	
		}
	);
}

function Update_Edit_Info() {

	var obj = document.editForm;
	
	
	$.post(
		"ajax_edit_update.php", 
		{ 
			Title: obj.Title.value,
			Description: obj.Description.value,
			<? if($data['IsFolder']) { ?>
			AllowIP: obj.AllowIP.value,
			<? } ?>
			<? if(!$data['IsFolder']) { ?>
			TagName: obj.TagName.value,
			<?}?>
			GroupID: obj.GroupID.value,
			FolderID: obj.FolderID.value,
			DocumentID: obj.documentID.value,
			FileName: obj.FileName.value
		},
		function(ReturnData)
		{
			/*
			Get_Return_Message(ReturnData);
			Get_Directory_List_Table();
			
			window.tb_remove();
			*/
			self.location.href = "<?=$_SERVER['HTTP_REFERER']?>?GroupID=<?=$GroupID?>&FolderID=<?=$FolderID?>&msg="+ReturnData;
		}
	);	

}

function resetAllSpanInnerHtml() {
	$('#div_err_Title').html('');
}

function addFoldersToTags(){
	document.getElementById("TagName").value="<?=$newTags?>";
}
</script>

<br style="clear:both" />

<form name="editForm" id="editForm" method="POST" action="">

<div class="table_board">
	<table class="form_table_v30 ">
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?= ($data['IsFolder'] ? $Lang["DigitalArchive"]["FolderName"] : $Lang["DigitalArchive"]["DisplayName"]) ?></td>
			<td>
				<input name="Title" id="Title" type="text" class="textboxtext" value="<?=intranet_htmlspecialchars($data['Title'])?>" />
				<span id="div_err_Title"></span>
			</td>
		</tr>
		<? if(!$data['IsFolder']) { ?>
    		<tr>
    			<td class="field_title"><?=$Lang["DigitalArchive"]["FileName"]?></td>
    			<td>
    				<input type="text" name="FileName" id="FileName" class="textbox" value="<?=intranet_htmlspecialchars($data['FileFolderName'])?>"/>
    			</td>
    		</tr>
		<? } ?>		
		<tr>
			<td class="field_title"><?=$Lang["DigitalArchive"]["Description"]?></td>
			<td><?=$ldaUI->GET_TEXTAREA("Description", $data['Description'], 70, 5, "", "", "", "textboxtext");?></td>
		</tr>
		<? if(!$data['IsFolder']) { ?>
    		<tr>
    			<td class="field_title"><?=$Lang["DigitalArchive"]["Tag"]?></td>
    			<td>
    				<input type="text" name="TagName" id="TagName" class="textbox" value="<?=intranet_htmlspecialchars($tagDisplay)?>"/>
    			    <span class="tabletextremark">(<?=$Lang['StudentRegistry']['CommaAsSeparator'].", ".$Lang["DigitalArchive"]["Max10Words"]?>)</span><br/>
    			    <input type="button" name="ForlderTag" onclick="addFoldersToTags();" value="<?=$Lang['DigitalArchive']['ForlderTag']?>"/>
    			</td>
    		</tr>
		<? } ?>
		<? if($data['IsFolder']) { ?>
    		<tr>
        		<td class="field_title"><?=$Lang['DigitalArchive']['AllowIPAddresses']?></td>
        		<td>
        			<?=$ldaUI->GET_TEXTAREA("AllowIP", $data['AllowIP'])?>
        			<div id="AllowIP_error" style="color:red;display:none;"><?=$Lang['DigitalArchive']['InvalidIPAddress']?></div>
        			<div class="tabletextremark">
        				<?=str_replace('<!--IP-->',getRemoteIpAddress(),$Lang['DigitalArchive']['AllowIPAddressesRemark'])?>
        			</div>
        		</td>
        	</tr>
		<? } ?>
	</table>
	<span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span> 
	<p class="spacer"></p>
</div>			
<div id="supplementaryInfoDiv"></div>
<div class="edit_bottom">
	<p class="spacer"></p>
	<?=$FormButton?>
	<p class="spacer"></p>
</div>			

<input type="hidden" name="GroupID" id="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="FolderID" id="FolderID" value="<?=$FolderID?>">
<input type="hidden" name="IsDuplicateFlag" id="IsDuplicateFlag" value="">
<? if($data['IsFolder']) {?>
	<input type="hidden" name="FileName" id="FileName" value="">
<?}?>
<input type="hidden" name="documentID" id="documentID" value="<?=$documentID?>">

</form>

<? if(!$data['IsFolder']) { ?>
<script language="javascript">
	var jQuery_1_3_2 = $.noConflict(true);
</script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script language="javascript">
var jQuery_1_4_2 = $.noConflict();

(function($){
	$('#TagName').multicomplete({
		source: [<?=$availableTagName?>]
	});
})(jQuery_1_4_2);

$ = jQuery_1_3_2;
</script>
<?}?>

<?
echo $ldaUI->FOCUS_ON_LOAD("editForm.Title"); 
intranet_closedb();
?>