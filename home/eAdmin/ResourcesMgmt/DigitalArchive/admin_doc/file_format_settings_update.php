<?php

# modifying : 

/**********************
 * 
 * Date:	2013-07-12 (Henry Chan)
 * Details: The Coding is finished
 * 
 * Date:	2013-07-10 (Henry Chan)
 * Details: File Created
 * 
 **********************/

$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

intranet_auth();
intranet_opendb();

$lda = new libdigitalArchive();

if ($_REQUEST['txt_file_format_list'] != '') {
	$fileFormatArr = explode("\n", $_REQUEST['txt_file_format_list']);
	if ($lda->Save_File_Format_Setting($fileFormatArr)) {
		$msg = "AddSuccess";
	} else {
		$msg = "AddUnsuccess";
	}
}

if ($_REQUEST['record_id']) {
	foreach ($_REQUEST['record_id'] as $rid) {
		if ($lda->Delete_File_Format_Setting($rid))
			$msg = "DeleteSuccess";
		else
			$msg = "DeleteUnsuccess";
	}
}

intranet_closedb();

header("Location: file_format_settings.php?msg=$msg");
?>