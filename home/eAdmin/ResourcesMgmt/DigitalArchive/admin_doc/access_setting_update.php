<?php
// Editing by 
/*
 * 2014-10-03 (Carlos) [ip2.5.5.10.1]: created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdigitalarchive.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;
}

intranet_auth();
intranet_opendb();

$lgs = new libgeneralsettings();

$success = $lgs->Save_General_Setting("DigitalArchive", array("AccessSetting" => $AccessSetting));
$msg = $success? "UpdateSuccess" : "UpdateUnsuccess";

intranet_closedb();
header("Location: access_setting.php?msg=$msg");
?>