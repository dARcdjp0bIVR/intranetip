<?php
# modifying : 

##### Change Log [Start] #####
# 	Date	: 	20130214 (Rita)
# 				add $CurrentPageArr['DigitalArchiveAdminMenu'] for menu display
#
#	Date	:	2013-01-08 (Rita)
#				Comment CSS style sheet					
#
#	Date	:	2012-03-12 (Henry Chow)
#				edit javascript checking on GroupTitle and GroupCode (fields cannot be blanked)
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;

$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ = $ldaUI->PageTagObj(2);

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$ldaUI->LAYOUT_START();

//echo $ldaUI->Include_JS_CSS();

echo $ldaUI->Get_Group_Access_Right_Settings_Index($GroupID);

$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>
<!--<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">-->


<script language="javascript">
<?
if($msg!="")
	echo "Get_Return_Message(\"".$Lang['General']['ReturnMessage'][$msg]."\")\n";
?>

function goBack() {
	self.location.href = "access_right.php";	
}


function checkAccessRight() {
	var obj = document.GroupRightForm;
	if(obj.MGMT_RegistryInformation_ViewBasic.checked==false) {
		obj.MGMT_RegistryInformation_ManageBasic.checked = false;
		//obj.MGMT_RegistryInformation_ManageBasic.parentNode.className = 'rights_not_select';
		var emt = obj.MGMT_RegistryInformation_ManageBasic;
		
		$('Input#'+emt.id).parent().removeClass('rights_select');
		$('Input#'+emt.id).parent().addClass('rights_not_selected');
	}
	
}

function goSubmit() {
	document.getElementById('SaveBtn').focus();
	
	var gTitle = $("#GroupTitle").val();
	var gCode = $("#GroupCode").val();

	if(gTitle=="" || gCode=="") {
		alert("<?=$i_alert_pleasefillin.$Lang['AccountMgmt']['Settings']['GroupName']." ".$Lang['MassMailing']['And']." ".$Lang['DigitalArchive']['GroupCode']?>");
		//return false;
		//setTimeout("submitAction();", 5000);	
	} else {
		//setTimeout(document.GroupRightForm.submit(),1000);
		//return true;
		//alert(1);
		document.getElementById('GroupRightForm').submit();		
	}
	
}
/*
function submitAction() {
	//document.GroupRightForm.submit();
	var gTitle = $("#GroupTitle").val();
	var gCode = $("#GroupCode").val();
	
	if(gTitle=="" || gCode=="") {
		alert("error");
		return false;	
	} else {
		return true;		
	}	
}
*/

function Check_Group(Obj)
{
	if (Obj.checked) 
		StyleClass = 'rights_selected';
	else 
		StyleClass = 'rights_not_select';
	
	$('input.'+Obj.id+'-Check').attr('checked',Obj.checked);
	$('td.'+Obj.id+'-Cell').attr('class',Obj.id+'-Cell '+StyleClass);
	
}

function Check_Single(Obj)
{

	if(Obj != null)
	{
		if(Obj.checked)
		{ 
			$('Input#'+Obj.id).parent().removeClass('rights_not_select');
			$('Input#'+Obj.id).parent().addClass('rights_selected');
			
			switch(Obj.id) {
				
				// if check "Manage", should also check "View"
				case 'ADMIN_OWN_MANAGE'	: if(document.getElementById('ADMIN_OWN_VIEW').checked==false) {$('#ADMIN_OWN_VIEW').click();} break;
				case 'ADMIN_OTHERS_MANAGE'	: if(document.getElementById('ADMIN_OTHERS_VIEW').checked==false) {$('#ADMIN_OTHERS_VIEW').click();} break;
				case 'MEMBER_OWN_MANAGE'	: if(document.getElementById('MEMBER_OWN_VIEW').checked==false) {$('#MEMBER_OWN_VIEW').click();} break;
				case 'MEMBER_OTHERS_MANAGE'	: if(document.getElementById('MEMBER_OTHERS_VIEW').checked==false) {$('#MEMBER_OTHERS_VIEW').click();} break;
				
			}
		}else
		{
			switch(Obj.id) {
				
				// if uncheck "View", should also uncheck "Manage"
				case 'ADMIN_OWN_VIEW'	: if(document.getElementById('ADMIN_OWN_MANAGE').checked==true) {$('#ADMIN_OWN_MANAGE').click();} break;
				case 'ADMIN_OTHERS_VIEW'	: if(document.getElementById('ADMIN_OTHERS_MANAGE').checked==true) {$('#ADMIN_OTHERS_MANAGE').click();} break;
				case 'MEMBER_OWN_VIEW'	: if(document.getElementById('MEMBER_OWN_MANAGE').checked==true) {$('#MEMBER_OWN_MANAGE').click();} break;
				case 'MEMBER_OTHERS_VIEW'	: if(document.getElementById('MEMBER_OTHERS_MANAGE').checked==true) {$('#MEMBER_OTHERS_MANAGE').click();} break;
				
			}
			
			$('Input#'+Obj.id).parent().removeClass('rights_selected');
			$('Input#'+Obj.id).parent().addClass("rights_not_select");
		}
	}
}

function Check_Manage_Checkbox(flag, section, fnc) {
	var f = section+"_"+fnc+"_Manage";
	if(flag==true) {
		document.getElementById(f).checked = false;
		document.getElementById(f).disabled = false;
		$('Input#'+f).parent().addClass("rights_not_select");
		$('Input#'+f).parent().removeClass('rights_selected');
	} else {
		document.getElementById(f).checked = false;
		document.getElementById(f).disabled = true;
		$('Input#'+f).parent().addClass("rights_not_select");
		$('Input#'+f).parent().removeClass('rights_selected');
	}
}

function Show_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', '');
	
}

function Hide_Edit_Icon(IconID)
{
	$('#'+IconID).attr('class', 'edit_dim');
	
}


function Check_Group_Title(GroupTitle,GroupID)
{
	var GroupID = GroupID || "";
	var PostVar = {
			"GroupID": GroupID,
			"GroupTitle": encodeURIComponent(GroupTitle)
			}
	var ElementObj = $('div#GroupTitleWarningLayer');
	
	if (Trim(GroupTitle) != "") {
		$.post('ajax_check_group_title.php',PostVar,
				function(data){
					
					if (data == "die"){ 
						window.top.location = '/';
					}
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = 'none';
					}
					else {
						ElementObj.html('<?=$Lang['DigitalArchive']['GroupNameDuplicateWarning']?>');
						ElementObj.show('fast');
						if (document.getElementById('GroupTitleWarningRow')) 
							document.getElementById('GroupTitleWarningRow').style.display = '';
					}
				}
				)
				;
	}
	else if(Trim(GroupTitle) == ""){
		ElementObj.html('<?=$Lang['DigitalArchive']['GroupNameDuplicateWarning']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('GroupTitleWarningRow')) 
			document.getElementById('GroupTitleWarningRow').style.display = '';
	}
	
}

function Check_Group_Code(GroupCode,GroupID)
{
	var GroupID = GroupID || "";
	var PostVar = {
			"GroupID": GroupID,
			"GroupCode": encodeURIComponent(GroupCode)
			}
	var ElementObj = $('div#GroupCodeWarningLayer');
	
	if (Trim(GroupCode) != "") {
		$.post('ajax_check_group_code.php',PostVar,
				function(data){
					
					if (data == "die"){ 
						window.top.location = '/';
					}
					else if (data == "1") {
						ElementObj.html('');
						ElementObj.hide();
						if (document.getElementById('GroupCodeWarningRow')) 
							document.getElementById('GroupCodeWarningRow').style.display = 'none';
					}
					else {
						
						ElementObj.html('<?=$Lang['DigitalArchive']['GroupCodeDuplicateWarning']?>');
						ElementObj.show('fast');
						if (document.getElementById('GroupCodeWarningRow')) 
							document.getElementById('GroupCodeWarningRow').style.display = '';
					}
				}
				)
				;
	}
	else if(Trim(GroupCode) == ""){
		ElementObj.html('<?=$Lang['DigitalArchive']['GroupCodeDuplicateWarning']?>');
		ElementObj.show('fast');
		
		if (document.getElementById('GroupCodeWarningRow')) 
			document.getElementById('GroupCodeWarningRow').style.display = '';
	}
	
}

// jEditable function 
function Init_JEdit_Input(objDom)
{
	
	var WarningLayer = "div#GroupTitleWarningLayer";
	
	$(objDom).editable
	(
	 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	
    	if (Trim($(WarningLayer).html()) == "" && ElementObj[0].revert != value)
    	{
    		if($('Input#GroupID').val() == '')
	    	{
	    		ElementObj.html(value);
	    		$('span#GroupTitleNavLayer').html(value);
				$('Input#GroupTitle').val(value);
	    	}
	    	else
	    	{
	    		var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupTitle":encodeURIComponent(value)
	    		};
		    				    
		    	$.post('ajax_rename_group.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    			ElementObj.html(value);
						$('span#GroupTitleNavLayer').html(value);
						$('Input#GroupTitle').val(value);
		    		});
	    	}
		}
		else {
			ElementObj[0].reset();
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	$(WarningLayer).html('');
	    	$(WarningLayer).hide();
	  	}
  	  }
  	);
  
  $(objDom).keyup
  (
  	 function()
  	 {
		var GroupTitle = Trim($('form input').val());
		var GroupID = $('Input#GroupID').val();
		Check_Group_Title(GroupTitle,GroupID);
		
	 }
  );
  
}

function Init_JEdit_Input2(objDom)
{
	//$(objDom).attr('style', '');
	
	$(objDom).editable
	(
      function(value, settings)
      {
    	var ElementObj = $(this);
    	ElementObj.html(value);
		$('Input#GroupDescription').val(value);
	
		if($('Input#GroupID').val() != '' && ElementObj[0].revert != value)
		{
			var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupDescription":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_set_group_description.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    		});
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	
	  	}
  	  }
  	);
}

function Init_JEdit_Input3(objDom)
{
	var WarningLayer = "div#GroupCodeWarningLayer";
	$(objDom).editable
	( 
      function(value, settings)
      {
    	var ElementObj = $(this);
    	
    	if (Trim($(WarningLayer).html()) == "" && ElementObj[0].revert != value)
    	{
    		if($('Input#GroupID').val() == '')
	    	{
	    		ElementObj.html(value);
	    		$('span#GroupCodeNavLayer').html(value);
				$('Input#GroupCode').val(value);
	    	}
	    	else
	    	{
	    		var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupCode":encodeURIComponent(value)
	    		};
		    				     
		    	$.post('ajax_set_group_code.php',PostVar,
		    		function (data) {
		    			Get_Return_Message(Trim(data));
		    			ElementObj.html(value);
						$('span#GroupCodeNavLayer').html(value);
						$('Input#GroupCode').val(value);
		    		});
	    	}
		}
		else {
			ElementObj[0].reset();
		}
	  }, 
	  {
	    tooltip   : "<?=$Lang['SysMgr']['FormClassMapping']['ClickToEdit']?>",
	    event : "click",
	    onblur : "submit",
	    type : "text",     
	    style  : "display: inline",
	    height: "20px",
	    maxlength: 100,
	    onreset: function() {
	    	$(WarningLayer).html('');
	    	$(WarningLayer).hide();
	  	}
  	  }
  	);
  
  $(objDom).keyup
  (
  	 function()
  	 {
		var GroupCode = Trim($('form input').val());
		var GroupID = $('Input#GroupID').val();
		Check_Group_Code(GroupCode,GroupID);
		
	 }
  );
  
}

function UpdateCategoryLinkage()
{
	var PostVar = {
	    			"GroupID":$('Input#GroupID').val(),
	    			"GroupCategoryID":$('Input#GroupCategoryID').val()
		};
    				     
    	$.post('ajax_set_group_category_linkage.php',PostVar,
    		function (data) {
    			Get_Return_Message(Trim(data));
    			//ElementObj.html(value);
				//$('span#GroupCodeNavLayer').html(value);
				//$('Input#GroupCode').val(value);
    		});
}

function jsClickEdit()
{
	$('span#EditableGroupTitle').click();
}

function jsClickEdit2()
{
	$('span#EditableGroupDescription').click();
}

function jsClickEdit3()
{
	$('span#EditableGroupCode').click();
}

// thick box function 
{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}

$(document).ready(
	function()
	{
		Check_Single('MGMT_RegistryInformation_View');
		Check_Single('MGMT_RegistryInformation_Manage');
		
		Check_Single('SETTINGS_AccessRight_Access');
		
		Init_JEdit_Input('span.jEditInput');
		Init_JEdit_Input2('span.jEditInput2');
		Init_JEdit_Input3('span.jEditInput3');
		Thick_Box_Init();
	}
);



<?
if($GroupID=="") {
	echo "$('#ADMIN').click();\n";
	echo "$('#MEMBER_OWN_VIEW').click();\n";
	echo "$('#MEMBER_OWN_MANAGE').click();\n";
	echo "jsClickEdit();\n";
} 
?>
</script>

