<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");

intranet_auth();
intranet_opendb();

$ldamu = new libdigitalarchive_moduleupload();
//$lda = $ldamu->Get_libdigitalarchive();
//if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive'] && sizeof($lda->UserInGroupList())==0){
//	exit;
//}
if(!$ldamu->User_Can_Archive_File()){
	exit;
}

$task = $_REQUEST['task'];

if($task=='Check_Is_Module_Files_Duplicated'){
	$ModuleTmpFolder = $_REQUEST['ModuleTmpFolder'];
	$GroupID = $_REQUEST['GroupID'];
	$DocumentID = $_REQUEST['DocumentID'];
	$Files = $_REQUEST['Files'];
	for($i=0;$i<count($Files);$i++) {
		//$Files[$i] = trim(urldecode(stripslashes($Files[$i])));
		$Files[$i] = getDecryptedText($Files[$i]);
	}
	$duplicatedFileNames = $ldamu->Check_Is_Module_Files_Duplicated($Files,$GroupID,$DocumentID);
	
	if(count($duplicatedFileNames)>0){
		echo implode("\n",$duplicatedFileNames);
	}
}else if($task=='Upload_Module_Files_To_Admin_Doc'){
	$ModuleTmpFolder = $_REQUEST['ModuleTmpFolder'];
	$FromModule = trim(urldecode(stripslashes($_REQUEST['FromModule'])));
	$GroupID = $_REQUEST['GroupID'];
	$DocumentID = $_REQUEST['DocumentID'];
	$TagName = trim(urldecode(stripslashes($_REQUEST['TagName'])));
	$IsDuplicateFlag = $_REQUEST['IsDuplicateFlag']==1;
	$Files = $_REQUEST['Files'];
	for($i=0;$i<count($Files);$i++) {
		//$Files[$i] = trim(urldecode(stripslashes($Files[$i])));
		$Files[$i] = getDecryptedText($Files[$i]);
	}
	
	$dataAry = $ldamu->Transform_Module_Files_To_Data_Array($Files,$GroupID,$DocumentID,$FromModule);
	$success = $ldamu->Upload_Module_Files_To_Admin_Doc($ModuleTmpFolder,$dataAry,$TagName,$IsDuplicateFlag);

	if($success){
		echo $Lang['DigitalArchiveModuleUpload']['ReturnMsg']['UploadFilesToDASuccess'];
	}else{
		echo $Lang['DigitalArchiveModuleUpload']['ReturnMsg']['UploadFilestoDAUnsuccess'];
	}
}else if($task=='Check_Is_Folder_Name_Duplicated') {
	$FolderName = trim(rawurldecode(stripslashes($_REQUEST['FolderName'])));
	$GroupID = $_REQUEST['GroupID'];
	$ParentFolderID = $_REQUEST['ParentFolderID'];
	
	$isDuplicated = $ldamu->Check_Is_Folder_Name_Duplicated($FolderName, $GroupID, $ParentFolderID);
	if($isDuplicated){
		echo '1';
	}else{
		echo '0';
	}
}else if($task=='Create_New_Admin_Folder') {
	$FolderName = trim(rawurldecode(stripslashes($_REQUEST['FolderName'])));
	$GroupID = $_REQUEST['GroupID'];
	$ParentFolderID = $_REQUEST['ParentFolderID'];
	
	$newFolderID = $ldamu->Create_New_Admin_Folder($FolderName,$FolderName,$GroupID,$ParentFolderID);
	echo $newFolderID;
	/*
	if(is_numeric($newFolderID)){
		echo $Lang['DigitalArchiveModuleUpload']['ReturnMsg']['CreateFolderSuccess'];
	}else{
		echo $Lang['DigitalArchiveModuleUpload']['ReturnMsg']['CreateFolderUnsuccess'];
	} */
}else if($task=='Remove_Temp_Folder'){
	$ModuleTmpFolder = trim(rawurldecode(stripslashes($_REQUEST['ModuleTmpFolder'])));
	$ldamu->Remove_Temp_Folder($ModuleTmpFolder);
}else if($task=='Get_Group_Folder_Selection_Table') {
	echo $ldamu->Get_Group_Folder_Selection_Table($_SESSION['UserID'],$_REQUEST['TargetGroupID'],$_REQUEST['SelectedDocumentID']);
}else if($task=='Get_Module_Files_List_Table') {
	$ModuleTmpFolder = trim(rawurldecode(stripslashes($_REQUEST['ModuleTmpFolder'])));
	echo $ldamu->Get_Module_Files_List_Table($ModuleTmpFolder);
}else if($task=='Rename_Temp_File') {
	$newFileName = trim(rawurldecode(stripslashes($_REQUEST['newFileName'])));
	//$oldFilePath = trim(rawurldecode(stripslashes($_REQUEST['oldFilePath'])));
	$oldFilePath = getDecryptedText($_REQUEST['oldFilePath']);
	$success = $ldamu->Rename_Temp_File($newFileName,$oldFilePath);
	if($success){
		echo $Lang['DigitalArchiveModuleUpload']['ReturnMsg']['RenameFileSuccess'];
	}else{
		echo $Lang['DigitalArchiveModuleUpload']['ReturnMsg']['RenameFileUnsuccess'];
	}
}

intranet_closedb();
?>