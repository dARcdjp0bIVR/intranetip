<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

$lda->Update_Like_Status($FunctionID, $Status);

$x = $ldaUI->Get_Like_Content($FunctionName, $FunctionID, $Delimiter);

intranet_closedb();

echo $x;
?>