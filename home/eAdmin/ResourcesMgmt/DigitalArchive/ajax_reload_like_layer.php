<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();
switch($action){
	case 'update_rating':
		$result = $lda->Insert_Rating_Record($RecordID,$rating);  
		if($result)
		echo $ldaUI->Get_Like_Content($FunctionName, $RecordID, $Delimiter);                                               
	break;
	case 'update_hitrate':
		$result = $lda->Insert_View_Record($RecordID);
		if($result)
		echo $ldaUI->Get_Like_Content($FunctionName, $RecordID, $Delimiter);        		                                                
	break;	
	case 'gen_rating_stat':
		$rateInfo = $lda->Get_Rating_Detail_By_Resource_ID($RecordID);
		$rateCount = sizeof($rateInfo);
		$RatingAry = Get_Array_By_Key($rateInfo,'Rating');
		$RatingStatAry = array_count_values($RatingAry);
		$AvgRating = $rateCount>0?my_round(array_sum($RatingAry)/$rateCount,1):0;
		$Title = $Lang['DigitalArchive']['TotalReviews'].": ".$rateCount." ".$Lang['DigitalArchive']['AverageRating'].": ".$AvgRating;
		
		include_once("templates/rating_graph.php");

	break;
	case 'view_hitrate_list':
		$data = $lda->Get_Resource_View_Data_For_Layer($RecordID);
		$hitrate_cnt = count($data);
		include_once("templates/hitrate_list.php"); 
	break;	
	default:
		$data = $lda->Get_Resource_Like_Data_For_Layer($RecordID, $IncludeCurrentUser=0);
		$like_cnt = count($data);
		include_once("templates/like_list.php"); 
}


intranet_closedb();

//echo $x;
?>