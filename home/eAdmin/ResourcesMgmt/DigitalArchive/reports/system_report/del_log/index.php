<?php
//using by: 

/***************************************
 * Date: 2013-07-10 (Rita)
 * Details: add advance search function
 * 
 * Date: 2013-02-14 (Rita)
 * Details: add $CurrentPageArr['DigitalArchiveAdminMenu'] for menu display
 * 
 * Date:	2013-01-07 (Rita)
 * Details: create this page
 ***************************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("digital_archive_reports_delete_log_page_size", $numPerPage, 0, "", "", 0);
    $digital_archive_reports_delete_log_page_size = $numPerPage;
}

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalArchive']) {
	header("Location: index.php");
	exit;	
}

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();
$linterface = new interface_html();

### Obtain POST data ###
$StartDate = $StartDate ? $StartDate : date("Y-m-d");
$EndDate = $EndDate ? $EndDate : date("Y-m-d");
$keyword = trim($_GET['keyword']);
$fileOrFolderName = trim($_GET['fileOrFolderName']);
$deletedBy =  trim($_GET['deletedBy']);
$createdBy =  trim($_GET['createdBy']);
$groupName = trim($_GET['groupName']);

### Page Title ###
$TAGS_OBJ[] = array($Lang['Tag']['DigitalArchive']['DeleteLog']);

# Display Admin Menu
$CurrentPageArr['DigitalArchiveAdminMenu'] = true;
$CurrentPageArr['DigitalArchive'] = 1;
$CurrentPage = "Reports_SystemReport";
$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

### TABLE INFO ###
if (isset($digital_archive_reports_delete_log_page_size) && $digital_archive_reports_delete_log_page_size != "") $page_size = $digital_archive_reports_delete_log_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.LogDate");

$sql = $lda->getDeleteLogSql($keyword, $StartDate, $EndDate, $fileOrFolderName, $groupName, $createdBy, $deletedBy);
$li->sql = $sql;
$li->no_col = 3;
$li->IsColOff = "GeneralDisplayWithNavigation";

# TABLE COLUMN
$li->column_list .= "<th width='20%'>".$Lang['DigitalArchive']['Reports']['SystemReport']['DeletedDate']."</th>\n";
$li->column_list .= "<th width='20%'>".$Lang['DigitalArchive']['Reports']['SystemReport']['DeletedBy']."</th>\n";
//$li->column_list .= "<th>".$Lang['DigitalArchive']['Reports']['SystemReport']['RecordType']."</th>\n";
$li->column_list .= "<th>".$Lang['DigitalArchive']['Reports']['SystemReport']['RecordInfo']."</th>\n";

//### Button
//$delBtn = "<a href=\"javascript:clickDelete()\" class=\"tool_delete\">" . $Lang['eDiscipline']['DeleteRecordsInDateRange'] . "</a>";

### Filter - Date Range
$date_select = $eNotice['Period_Start'] .": ";
$date_select .= $linterface->GET_DATE_PICKER("StartDate",$StartDate);
$date_select .= $eNotice['Period_End']."&nbsp;";
$date_select .= $linterface->GET_DATE_PICKER("EndDate",$EndDate);
$date_select .= $linterface->GET_BTN($button_submit, "submit");

### Search Box ###
//$searchBoxDisplay = $linterface->Get_Search_Box_Div('keyword', $keyword, $Others='');

$searchBoxDisplay = '<table><tr><td nowrap><div class="Conntent_search" ><input name="keyword" id="keyword" type="text" class="keywords" value="'.intranet_htmlspecialchars($_GET['keyword']).'"/>
	                     |</td><td><a href="javascript:void(0)" onclick="DisplayAdvanceSearch();" id="AdvanceSearchText">'.$Lang['DigitalArchive']['Advanced'].'</a> '.$AdminButton.'</div></td></tr></table>';

$linterface->LAYOUT_START();


###### Advance Search Div ######
$advanceSearchDiv = '';

$advanceSearchDiv .= '<div id="DA_search"  style="display: block; width: 500px;padding: 7px 7px 0px 7px;border: 1px solid #CCCCCC;background: #f4f4f4;position: absolute;visibility: hidden;" >';
	$advanceSearchDiv .= '<table class="form_table">';
	
//		# Keywords
//		$advanceSearchDiv .= '<tr>';
//			$advanceSearchDiv .= '<td class="field_title">'.$Lang['DigitalArchive']['Keywords'].'</td>';
//			$advanceSearchDiv .= '<td><label>';
//			$advanceSearchDiv .= '<input name="keyword" id="str" type="text" class="keyword" style="" value="'.intranet_htmlspecialchars($_GET['keyword']).'" />';
//			$advanceSearchDiv .= '</label></td>';
//		$advanceSearchDiv .= '</tr>';
		
		# File Name / Folder Name
		$advanceSearchDiv .= '<tr>';
			$advanceSearchDiv .= '<td class="field_title">'.$Lang['DigitalArchiveModuleUpload']['FileOrFolderName'].'</td>';
			$advanceSearchDiv .= '<td>';
			$advanceSearchDiv .= '<input type="text" class="tabletext" name="fileOrFolderName" ID="fileOrFolderName" value="'.intranet_htmlspecialchars($fileOrFolderName).'">';
			$advanceSearchDiv .= '</td>';
		$advanceSearchDiv .= '</tr>';
		
		# Group Name
		$advanceSearchDiv .= '<tr>';
			$advanceSearchDiv .= '<td class="field_title">'.$Lang["DigitalArchive"]["GroupName"].'</td>';
			$advanceSearchDiv .= '<td>';
			$advanceSearchDiv .= '<input type="text" class="tabletext" name="groupName" ID="groupName" value="'.intranet_htmlspecialchars($groupName).'">';
			$advanceSearchDiv .= '</td>';
		$advanceSearchDiv .= '</tr>';		

		# Created By
		$advanceSearchDiv .= '<tr>';
			$advanceSearchDiv .= '<td class="field_title">'.$Lang["DigitalArchive"]["CreatorName"].'</td>';
			$advanceSearchDiv .= '<td>';
			$advanceSearchDiv .= '<input type="text" class="tabletext" name="createdBy" ID="createdBy" value="'.intranet_htmlspecialchars($createdBy).'">';
			$advanceSearchDiv .= '</td>';
			
			
//			$advanceSearchDiv .= '<td>
//									'.$ldaUI->Get_My_Group_Select_Menu("GroupIDArr[]", intranet_htmlspecialchars($groupIds), "", "", $IsMultiple=1).'
//									<br/>
//									<span class="tabletextremark">
//										'.$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'].'
//									</span>										
//								</td>';
			
			
		$advanceSearchDiv .= '</tr>';
		
		# Deleted By
		$advanceSearchDiv .= '<tr>';
			$advanceSearchDiv .= '<td class="field_title">'.$Lang['DigitalArchive']['Reports']['SystemReport']['DeletedBy'].'</td>';
			$advanceSearchDiv .= '<td>';
			$advanceSearchDiv .= '<input type="text" class="tabletext" name="deletedBy" ID="deletedBy" value="'.intranet_htmlspecialchars($deletedBy).'">';
			$advanceSearchDiv .= '</td>';
		$advanceSearchDiv .= '</tr>';
	$advanceSearchDiv .= '</table>';
					
	# Submit Button				
	$advanceSearchDiv .= '<div class="edit_bottom">';
		$advanceSearchDiv .= $linterface->GET_ACTION_BTN($Lang['Btn']['Search'],"button","submitSearch()");
		$advanceSearchDiv .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","MM_showHideLayers('DA_search','','hide')");
	$advanceSearchDiv .= '</div>';

$advanceSearchDiv .= '</div>';


?>
<SCRIPT LANGUAGE=Javascript>
<!--//
function clickDelete()
{
	document.form1.action = "remove_update.php";
	AlertPost(document.form1, "remove_update.php", "<?=$Lang['eDiscipline']['ConfirmDeleteRecordsInDateRange']?>");
}

function check_form()
{
	obj = document.form1;
	
	if(!check_date(obj.StartDate,"<?=$i_invalid_date?>"))
	{
		obj.StartDate.focus();
		return false;
	}
	
	if(!check_date(obj.EndDate,"<?=$i_invalid_date?>"))
	{
		obj.EndDate.focus();
		return false;
	}
	
	if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
		obj.StartDate.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}	
	
	return true;
}
//-->

function DisplayAdvanceSearch() {
	
	var pos = $("#AdvanceSearchText").position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	
	topPos = parseInt(topPos) + 15;
	leftPos = parseInt(leftPos) - 452;
	
	$("#DA_search").css({"position": "absolute", "top": topPos, "left": leftPos, "z-index":9 });
	MM_showHideLayers('DA_search','','show');	
}

function submitSearch() {
	document.form1.action = "index.php";
	document.form1.submit();
		
}

</SCRIPT>


<form name="form1" method="get" action="index.php" onSubmit="return check_form();">

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
	<?=$date_select?> 
	</td>
	

	
	<td valign="bottom" align="right">
	 	<?=$searchBoxDisplay?>
	</td>
<tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
	<div class="common_table_tool">
		<?=$delBtn?>
	</div>
	</td>
</tr>
</table>
</div>

<?=$li->display();?>

<?=$advanceSearchDiv?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

