<?php

//$x .= "<th>".$Lang['DigitalArchive']['NoOfReview']."</th>";
//$x .= "<th>".$Lang['DigitalArchive']['Rating']."</th>";
$x = '';
$y = '';
for($i=0;$i<=10;$i++){
	$x .= "<th>".$i."</th>";
	$y .= "<td>".$RatingStatAry[$i]."</td>";
}



?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.3.2.min.js"></script>
<link href="<?=$PATH_WRT_ROOT?>home/library_sys/reports/css/visualize.css" rel="stylesheet" type="text/css">
<link href="<?=$PATH_WRT_ROOT?>home/library_sys/reports/css/visualize-light.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>home/library_sys/reports/js/excanvas.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>home/library_sys/reports/js/number_rounding.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>home/library_sys/reports/js/visualize.jQuery.js"></script>
<script type="text/javascript">
$(function(){
	$('.rating_table').visualize({type: 'bar', width: '600px', height : '550px',appendKey:false});

});
</script>

<table class="rating_table" style="display:none;">
	<caption><?=$Title?></caption>
	<thead>
		<tr><td></td><?=$x?></tr>
	</thead>
	<tbody>
		<tr><th></th><?=$y?></tr>
	</tbody>
</table>
