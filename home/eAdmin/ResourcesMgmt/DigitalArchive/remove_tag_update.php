<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

$lda->Start_Trans();

if(in_array(false,$lda->Set_Resource_Record_Not_Use_Tag_By_TagID($TagID, $lda->Module))) {
	$lda->RollBack_Trans();
	$msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];	
} else {
	$lda->Commit_Trans();
	$msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}

intranet_closedb();

header("Location: displayMore.php?flag=$flag&msg=$msg");
?>