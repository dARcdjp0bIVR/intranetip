<?php

// Using: 

/********************************************
 * Date: 2013-04-02 (Rita)
 * Details:	Create this page for updating tag
 ********************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Check Access Right
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: displayMore.php?flag=Tag&msg=".$Lang['General']['ReturnMessage']['UpdateUnsuccess']);
	exit;
}

$lda = new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();

# Steps #
# 1) check existing TAG in table "TAG", 
#  1a)if no, then add a new Tag in "TAG"
#  1b) if exist, remove original Tag relationship in table "TAG_RELATIONSHIP"
# 2) add entry in "TAG_RELATIONSHIP" (since not in use in this module before)
# 3) update TagID in table "DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE"


# Get Existing Tag By Name
$TagIdAry = returnModuleTagIDByTagName($Tag);

# If no existing Tag Name, create one & update create TagModuleRelation
if(sizeof($TagIdAry)==0) {
	$updatedTagID = insertModuleTag($Tag, $lda->Module);
} else { # If existing one, tag ID = old tagID
	$updatedTagID = $TagIdAry[0];
	removeTagModuleRelation($TagID, $lda->Module);
	createTagModuleRelation($updatedTagID, $lda->Module);
}

# Update Relationship 
$subjectResourcesInfo = $lda->getResourceInfoByTagID($TagID);
$sujectResourcesInfoAssoc = BuildMultiKeyAssoc($subjectResourcesInfo, 'SubjectResourceID');
$subjectResourcesIDArr = array_keys($sujectResourcesInfoAssoc);

if($updatedTagID!=''){
	$numOfSubjectResourcesID = count($subjectResourcesIDArr);
	for($i=0;$i<$numOfSubjectResourcesID;$i++){
		$thisRecordID = $subjectResourcesIDArr[$i];
		$result = $lda->Update_Subject_eResources_Tag_Name_By_Admin($TagID,$thisRecordID,$updatedTagID);
	}
	
	if($numOfSubjectResourcesID==''){
		$result = $lda->Update_Subject_eResources_Tag_Name_By_Admin($TagID,'', $updatedTagID);
	}
	
}

intranet_closedb();
$msg='';
if($result!=''){
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}	


header("Location: displayMore.php?flag=Tag&msg=".$msg);

?>