/*
using: 

var js_LAYOUT_SKIN = "<?php echo $LAYOUT_SKIN;?>";
var loading = "<img src='"+js_PATH_WRT_ROOT + "/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";
var icon_book_path = js_PATH_WRT_ROOT + "/images/" + js_LAYOUT_SKIN + "/reading_scheme/icon_book.gif";
*/
function Get_Resource_Newest_Table(YearID) {
	
	//$("#resource_new").html(loading);
	Block_Element("resource_new");
	
	$.post(
		"ajax_get_newest_resource_record.php", 
		{ 
			YearID: YearID
		},
		function(ReturnData)
		{
			//$('#tempdiv').html(ReturnData);
			$('#resource_new').html(ReturnData);
			UnBlock_Element("resource_new");
		}
	);		
}

function Get_Resource_MyFavourite_Table() {
	//$("#resource_fav").html(loading);
	Block_Element("resource_fav");
	
	$.post(
		"ajax_get_my_favourite_resource_record.php", 
		{ 
			YearID: ""
		},
		function(ReturnData)
		{
			$('#resource_fav').html(ReturnData);
			UnBlock_Element("resource_fav");
		}
	);		
}

function Get_My_Subject_Table(YearID) {
	//$("#resource_subject").html(loading);
	Block_Element("resource_subject");
	
	$.post(
		"ajax_get_my_subject_table.php", 
		{ 
			YearID: YearID
		},
		function(ReturnData)
		{
			$('#resource_subject').html(ReturnData);
			UnBlock_Element("resource_subject");
		}
	);		
}

function Get_Resource_Favourite_Table(YearID) {
	//$("#resource_popular").html(loading);
	Block_Element("resource_popular");
	
	$.post(
		"ajax_get_favourite_record.php", 
		{ 
			YearID: YearID
		},
		function(ReturnData)
		{
			$('#resource_popular').html(ReturnData);
			UnBlock_Element("resource_popular");
		}
	);		
}
function js_View_Details(RecordID,action){
	var h=480,w=640;
	
	if(action=='gen_rating_stat'){
		newWindow('ajax_reload_like_layer.php?RecordID='+RecordID+'&action='+action,10);
	}else{
		if(action=='view_like_list'){
			w=320;h=300;
		//}else if(action=='gen_rating_stat'){
		//	w=750;h=550;		
		}else if(action=='view_hitrate_list'){
			w=350;h=320;		
		}
		$.fancybox({
			href: 'ajax_reload_like_layer.php?RecordID='+RecordID+'&action='+action,
			type: 'ajax',
			width: w,
			height: h,
			scrolling: 'no',
			autoDimensions: false
		});
	}
}
function UpdateResourceTable(FunctionName){
	if(FunctionName=='ResourceNewest'){
		Get_Resource_Favourite_Table(CurYearID);
	}else if(FunctionName=='ResourceFavourite'){
		Get_Resource_Newest_Table(CurYearID);
	}else if(FunctionName=='ResourceMyFavourite'){
		Get_Resource_Newest_Table(CurYearID);
		Get_Resource_Favourite_Table(CurYearID);
	}
}
function js_Update_Rating(FunctionName, FunctionID, RefreshTable){
	var rating = $('#Rating_'+FunctionName+'_'+FunctionID).val();
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	$.post(
		"ajax_reload_like_layer.php",
		{
			RecordID: FunctionID,
			FunctionName: FunctionName,
			rating: rating,
			action: 'update_rating'
		},
		function(ReturnData){
	
			$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			if(RefreshTable){
				UpdateResourceTable(FunctionName);	
			}	
		}
	)
}
function js_Update_HitRate(FunctionName, FunctionID, Link, RefreshTable){
	$("#Div_"+FunctionName+"_"+FunctionID).html(loading);
	$.post(
		"ajax_reload_like_layer.php",
		{
			RecordID: FunctionID,
			FunctionName: FunctionName,
			action: 'update_hitrate'
		},
		function(ReturnData){
			$("#Div_"+FunctionName+"_"+FunctionID).html(ReturnData);
			if(RefreshTable){
				UpdateResourceTable(FunctionName);	
			}
			if(Link){
				$("#download_frame").attr("src",Link);
			}
		}
	)
}
function js_Show_Rating_Status(FunctionName, FunctionID){
	$('#Div_'+FunctionName+'_Like_'+FunctionID).toggle();
	$('#Div_'+FunctionName+'_Rate_'+FunctionID).toggle();	
}
function js_Open_Like_List(obj, FunctionName, FunctionID)
{

	var layername = "Div_"+FunctionName+"_"+FunctionID;
	
	
	//$(obj).after('<div class="like_list_layer">'+loading+'</div>');
	$('#like_num_' +  FunctionID).after('<div class="like_list_layer">'+loading+'</div>');
	
	var pos = $(obj).position()
	$(".like_list_layer").css(
		{
			'position':'absolute',
			'left': pos.left+15,
			'top': pos.top+20
		}
	)	
	
	$.post(
		"ajax_reload_like_layer.php",
		{
			RecordID: FunctionID
		},
		function(ReturnData){
			$(".like_list_layer").html(ReturnData);
		}
	)
	
}

function js_Hide_Like_List()
{	
	$(".like_list_layer").remove();
}

function js_Show_ClassLevel(obj, jsFunctionName, YearID)
{

	if(document.getElementById(jsFunctionName+'_Flag').value==1) {
		js_Hide_ClassLevel();
		
	} else {
	
		$(obj).after('<div class="like_list_layer" onClick="js_Hide_ClassLevel()">'+loading+'</div>');
		
		var pos = $(obj).position()
		$(".like_list_layer").css(
			{
				'position':'absolute',
				'left': pos.left+15,
				'top': pos.top+20
			}
		)	
		
		$.post(
			"ajax_reload_classlevel_layer.php",
			{
				//RecordID: FunctionID
				jsFunctionName : jsFunctionName,
				YearID: YearID
			},
			function(ReturnData){
				$(".like_list_layer").html(ReturnData);
			}
		)
		
	} 	
}

function js_Hide_ClassLevel() {
	$(".like_list_layer").remove();
}
