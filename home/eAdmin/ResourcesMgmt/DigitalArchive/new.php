<?php
# modifying : 

/********************************************** 
 * Date:    2014-02-20 (Tiffany)
 * Details:  Add js_New_Level_DA(), js_show_file_DA(),js_show_choose_clip(). Modified checkForm().
 * 
 * Date:	2013-04-03 (Rita)
 * Details: add checking for subject & form 
 *********************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcampustv.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lda=new libdigitalarchive();
$ldaUI = new libdigitalarchive_ui();
$ldamu = new libdigitalarchive_moduleupload();
$lctv = new libcampustv(); 
$fcmy = new Year();
$subject = new subject();

$CurrentPageArr['DigitalArchive_SubjectReference'] = 1;

$TAGS_OBJ = $ldaUI->PageTagObj(1);

$allForms = $fcmy->Get_All_Year_List();
$allSubjects = $subject->Get_All_Subjects();

$tempAry = array();
/* TBD: should handle 3 cases
 * 1. for module admin, provide him/her all subjects
 * 2. for subject admin, provide him/her related subjects (and subjects in case 3 below)
 * 3. for subject teacher, provide him/her related subjects
 */
for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
	$tempAry[$i] = array($allSubjects[$i]['RecordID'], Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']));
}
$allSubjectsRevised = $tempAry;


//$subjectDisplay = getSelectByArray($allSubjectsRevised, 'name="SubjectID" id="SubjectID"', $SubjectID, 0, 1);
//$formDisplay = getSelectByArray($allForms, 'name="YearID[]" id="YearID[]" multiple size="10"', $YearID, 0, 1);


# TAG data
$tagAry = returnModuleAvailableTag($lda->Module, 1);

$tagNameAry = array_values($tagAry);

$delim = "";
if(sizeof($tagNameAry>0)) {
	foreach($tagNameAry as $tag) {
		$availableTagName .= $delim."\"".addslashes($tag)."\"";
		$delim = ", ";
	}
}
//-----Tiffany edit-----
#choose DA thickbox
$show_DA.='<div id="DigitalArchiveDiv" style="display:none"><div style="height:400px;overflow-x:auto; overflow-y:auto"><table>';

$myGroupList = $lda->GetMyGroup();
$folderID=0;
for($i=0;$i<sizeof( $myGroupList);$i++){
$show_DA.='<p id="pg_'.$myGroupList[$i][0].'"><a href="javascript:js_New_Level('.$myGroupList[$i][0].','.$folderID.');" id="'.$myGroupList[$i][0].'" >&nbsp;&nbsp;+&nbsp;&nbsp;</a>'.$myGroupList[$i][1].'</p>';
}

$show_DA.='</table></div>';
$show_DA.='<div class="edit_bottom" style="height:10px;">
           <p class="spacer"></p>
		   <input name="close_button" type="button" class="formbutton" onclick="tb_remove();" onmouseover="this.className="formbuttonon"" onmouseout="this.className="formbutton"" value="'.$Lang['Btn']['Close'].'" />
           <p class="spacer"></p>
           </div>';
        
$show_DA.='</div>';
           
#choose Campus TV thickbox
$show_CTV.='<div id="CampusTVDiv" style="display:none"><div style="height:400px; overflow-x:auto; overflow-y:auto;"><br><table class="common_table_list_v30">';

$channels=$lctv->returnChannels();

$show_CTV.='<thead><tr><th>'.$Lang["DigitalArchive"]["Channels"].'</th><th>'.$Lang["DigitalArchive"]["Clips"].'</th><th></th></tr></thead><tbody>';
for($i=0;$i<sizeof($channels);$i++){
	$clips = $lctv->returnClips($channels[$i][0]);
	if(!empty($clips)){
	$show_CTV.='<tr><td rowspan='.count($clips).'>'.$channels[$i][1].'</td><td id="'.$clips[0][0].'">'.$clips[0][1].'</td><td width="10px"><input type="radio" name="chosen_clip" value="'.$clips[0][0].'" onclick="js_show_choose_clip();"></td></tr>';
    for($j=1;$j<count($clips);$j++){
    $show_CTV.='<tr><td id="'.$clips[$j][0].'">'.$clips[$j][1].'</td><td width="10px"><input type="radio" name="chosen_clip" value="'.$clips[$j][0].'" onclick="js_show_choose_clip();"></td></tr>';
    }
	}
}
$show_CTV.='</tbody></table></div>';
$show_CTV.='<div class="edit_bottom" style="height:10px;">
           <p class="spacer"></p>
		   <input name="close_button" type="button" class="formbutton" onclick="tb_remove();" onmouseover="this.className="formbuttonon"" onmouseout="this.className="formbutton"" value="'.$Lang['Btn']['Close'].'" />
           <p class="spacer"></p>
           </div>';
           
$show_CTV.='</div>';
           



$FormButton = $linterface->GET_ACTION_BTN($button_submit, "submit", "", "DA_Submit")."&nbsp;".
				$linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()");

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$ldaUI->LAYOUT_START();


?>
<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">
<script>

$(document).ready(function(){
	
	js_reload_subject();
	
});


function js_reload_subject(){	
	
	var action = 'reloadSubject';	
	
	$.ajax({
	url:      	"ajax_reload_subject_form.php",
	type:     	"POST",
	data:     	'&action='+action,
	async:		false,
	error:    	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
			  	},
	success: function(data)
			 {				
				if(data)
				{			
					$("#subjectSelectionDisplayLayer").html(data);								
				}else{
					$("#subjectSelectionDisplayLayer").html('<?=$Lang['DigitalArchive']['HaveNotAssignedToSubjectGroup']?>');	
				}
				js_reload_form();
			}
	 });	
	
}

function js_reload_form(){
	var action = 'reloadForm';	
	var selectedSubjectID = $('#SubjectID option:selected').val();
	$.ajax({
	url:      	"ajax_reload_subject_form.php",
	type:     	"POST",
	data:     	'&action='+action + '&selectedSubjectID=' + selectedSubjectID,
	async:		false,
	error:    	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
			  	},
	success: function(data)
			 {				
			 	
				if(data)
				{			
					$("#formSelectionDisplayLayer").html(data);		
					$("#SelectAllFormBtn").show();
					$("#SelectAllFormRemark").show();			
					$("#DA_Submit").attr('disabled', false).show();			
				}else{
					$("#formSelectionDisplayLayer").html('<?=$Lang['DigitalArchive']['HaveNotAssignedToForm']?>');	
					$("#SelectAllFormBtn").hide();
					$("#SelectAllFormRemark").hide();	
					$("#DA_Submit").hide().attr('disabled', true);
				}
			}
	 });
	
	
}


function checkForm() {
	
	var obj = document.form1;
	var error_no = 0;
	var focus_field = "";
	reset_innerHtml();
	
	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang["DigitalArchive"]["Title"]; ?>.", "title_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Title";
	}
	if(!validateFilename(obj.Title.value, '<?=$Lang['DigitalArchive']['jsNameCannotWithSpecialCharacters']?>')) 
	{
		return false;	
	}
	
    if(obj.FileType[0].checked==true) {
		if(!check_text_30(obj.File, "<?php echo $i_alert_pleaseselect.$Lang["DigitalArchive"]["File"]; ?>.", "source_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "File";
		}		
	}
    if(obj.FileType[1].checked==true) {
		
		if($('span#DigitalArchiveDiv_showfile').html()==""){			
			$('span#source_err_msg').html('<font color=red><?php echo $i_alert_pleaseselect.$Lang["DigitalArchive"]["DigitalArchiveName"]; ?>.</font>'); 
            error_no++;          
		  }
	}  
	 if(obj.FileType[2].checked==true) {
		
		if($('span#CampusTVDiv_showclip').html()==""){			
			$('span#source_err_msg').html('<font color=red><?php echo $i_alert_pleaseselect.$Lang["DigitalArchive"]["CampusTV"]; ?>.</font>'); 
            error_no++;          
		  }
	} 
	if(obj.FileType[3].checked==true){
		if(!check_text_30(obj.Url, "<?php echo $i_alert_pleasefillin.$Lang["DigitalArchive"]["Url"]; ?>.", "source_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "Url";
		}		
		
	}
	
	
	if(countOption(document.form1.elements['YearID[]'])==0) {
		$('#level_err_msg').html('<font color=red><?php echo $i_alert_pleaseselect.$Lang["DigitalArchive"]["Level"]; ?>.</font>');
		error_no++;
		//if(focus_field=="")	focus_field = "YearID[]";
		
	}
	if(countOption(document.form1.elements['SubjectID'])==0) {
		$('#subject_err_msg').html('<font color=red><?php echo $i_alert_pleaseselect.$Lang["DigitalArchive"]["Subject"]; ?>.</font>');
		error_no++;
		if(focus_field=="")	focus_field = "SubjectID";
		
	}
	
	if(error_no>0)
	{
		if(focus_field!="")
			eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true;
	}
}

function reset_innerHtml()
{
 	$('#title_err_msg').html('');
 	$('#source_err_msg').html('');
 	$('#level_err_msg').html('');
 	$('#subject_err_msg').html('');
}

function goBack() {
	self.location.href = "index.php";	
}

function hideSpan(layername) {
	$('#'+layername).hide();
	//document.getElementById(layername).style.display = "none";	
}

function showSpan(layername) {
	$('#'+layername).show();
	//document.getElementById(layername).style.display = "inline";	
}
function js_New_Level(groupID,folderID){
 if(folderID==0){
    obj=document.getElementById(groupID);
    if(obj.innerHTML=="&nbsp;&nbsp;+&nbsp;&nbsp;"){
       if($('div#dg_'+groupID).length>0){
          $('div#dg_'+groupID).show();
          obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
       }else{
       $.post("ajax_get_DA.php",
              {groupID:groupID,folderID:folderID},
              function(array){
              obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
              $('p#pg_'+groupID).after(array);
              }
             );
       }
    }else{
    obj.innerHTML="&nbsp;&nbsp;+&nbsp;&nbsp;";
    $('div#dg_'+groupID).hide();
    }
 }else{
    obj=document.getElementById(folderID);
    if(obj.innerHTML=="&nbsp;&nbsp;+&nbsp;&nbsp;"){
       if($('div#df_'+folderID).length>0){
          $('div#df_'+folderID).show();
          obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
       }else{
       $.post("ajax_get_DA.php",
              {groupID:groupID,folderID:folderID},
              function(array){
              obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
              $('p#pf_'+folderID).after(array);
              }
             );
       }     
    }else{
    obj.innerHTML="&nbsp;&nbsp;+&nbsp;&nbsp;";
    $('div#df_'+folderID).hide();
    }
 }    
}

function js_show_file_DA(){	
	
     var chosen_file = $('input[name="chosen_file"]:checked').val() ; 
//   if(typeof(chosen_file)=="undefined"){
//       alert("<?=$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseSelectAtLeastOneFile']?>");
//   }   
//   else{

       tb_remove();
       $('span#DigitalArchiveDiv_showfile').html("");
       $('span#DigitalArchiveDiv_showfile').append('<p>'+$('span#s_'+chosen_file).text()+'</p>');   
//       var div= document.getElementById("div1");	
//       var input = document.createElement("input");
//       input.type = "hidden";
//       input.name="chosen_file";
//       input.value=id;
//       div.appendChild(input);
//   }
}


function js_show_choose_clip(){	
	
     var chosen_clip = $('input[name="chosen_clip"]:checked').val() ;    
//   if(typeof(chosen_clip)=="undefined"){
//      alert("<?=$Lang['DigitalArchive']['PleaseSelectAtLeastOneProgram']?>");   
//   }
//   else{

       tb_remove();
       $('span#CampusTVDiv_showclip').html("");
       $('span#CampusTVDiv_showclip').append('<p>'+$('td#'+chosen_clip).html()+'</p>');
//       var div= document.getElementById("div1");	
//       var input = document.createElement("input");
//       input.type = "hidden";
//       input.name="chosen_clip";
//       input.value=id;
//       div.appendChild(input);
//   }
   
}

</script>

<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="new_update.php" onSubmit="return checkForm();" enctype="multipart/form-data">
<!-- Content [Start] -->
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td class="main_content">
			<div class="navigation_v30 navigation_2">
				<a href="javascript:goBack()"><?=$i_frontpage_menu_home?></a>
				<span><?=$Lang["DigitalArchive"]["NewFile"]?> </span>
			</div>
			<p class="spacer"></p>
			
			<div class="table_board">
				<table class="form_table_v30 ">
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Title"]?></td>
						<td>
							<input name="Title" id="Title" type="text" class="textboxtext" value="" />
							<span id="title_err_msg"></span>
						</td>
					</tr>
					<tr>
						<td class="field_title"><?=$Lang["DigitalArchive"]["Description"]?></td>
						<td><?=$linterface->GET_TEXTAREA("Description", $Description, 70, 5, "", "", "", "textboxtext");?></td>
					</tr>
					<tr>
						<td class="field_title">
							<span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Source"]?>
						</td>
						<td> 
							<input name="FileType" type="radio" id="typeFile" value="1" checked="checked" onClick="showSpan('FileDiv1');hideSpan('FileDiv2');hideSpan('FileDiv3');hideSpan('FileDiv4');"/><label for="typeFile"><?=$Lang["DigitalArchive"]["File"]?></label>
						<?if($ldamu->User_Can_Archive_File()){?>	
							<input name="FileType" type="radio" id="typeDA" value="3"  onClick="showSpan('FileDiv3');hideSpan('FileDiv1');hideSpan('FileDiv2');hideSpan('FileDiv4');"/><label for="typeDA"><?=$Lang["DigitalArchive"]["DigitalArchiveName"]?></label>
						<?}?>
						<?if($plugin['tv']){?>
							<input name="FileType" type="radio" id="typeTV" value="4"  onClick="showSpan('FileDiv4');hideSpan('FileDiv1');hideSpan('FileDiv2');hideSpan('FileDiv3');"/><label for="typeTV"><?=$Lang["DigitalArchive"]["CampusTV"]?></label>							
						<?}?>
							<input type="radio" name="FileType" id="typeUrl" value="2" onClick="showSpan('FileDiv2');hideSpan('FileDiv1');hideSpan('FileDiv3');hideSpan('FileDiv4');"/><label for="typeUrl"><?=$Lang["DigitalArchive"]["Url"]?></label>
							<span id="FileDiv1" style="display:inline;">
								<input type="file" name="File" id="File" class="textbox" />
							</span>
							<span id="FileDiv2" style="display:none;">
								<input type="text" name="Url" id="Url" value="" class="textboxtext2">
							</span>
							<span id="FileDiv3" style="display:none;">
							    <span id="DigitalArchiveDiv_showfile"></span>
                                <p><a href="#TB_inline?height=480&width=600&inlineId=DigitalArchiveDiv" title="<?=$Lang["DigitalArchive"]["SelectFile"]?>" class="thickbox" type="button"><?=$Lang["DigitalArchive"]["SelectFile"]?></a></p>					    
							    <?=$show_DA?>
							</span>
							<span id="FileDiv4" style="display:none;">
							    <span id="CampusTVDiv_showclip"></span>
							    <p><a href="#TB_inline?height=480&width=600&inlineId=CampusTVDiv" title="<?=$Lang["DigitalArchive"]["SelectProgram"]?>" class="thickbox" type="button"><?=$Lang["DigitalArchive"]["SelectProgram"]?></a></p>					    
							    <?=$show_CTV?>
							</span>
							<span id="source_err_msg"></span>
						</td>
					
					</tr>
					<!-- Subject Selection Box Display !-->
					
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Subjects"]?></td>
						<td><span id="subjectSelectionDisplayLayer"></span>
						<!--
						<?=$subjectDisplay?><span id="subject_err_msg"></span>
						!-->
						</td>
					</tr>
					<tr>
						<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["DigitalArchive"]["Level"]?></td>
						<td>
						<span id="formSelectionDisplayLayer"></span>
							<?=$formDisplay?>
							<?=$ldaUI->GET_BTN($Lang['Btn']['SelectAll'], "button", "checkOptionAll(document.form1.elements['YearID[]'])","SelectAllFormBtn")?><br>
							<span id="SelectAllFormRemark" class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></span>
							<br><span id="level_err_msg"></span>
						</td>
					</tr>
				
					<tr>
						<td class="field_title"><?=$Lang["DigitalArchive"]["Tag"]?></td>
						<td><?/*=$linterface->GET_TEXTAREA("Tag", $Tag, 70, 5, "", "", "", "textboxtext");*/?><input type="text" name="Tag" id="Tag" value="" class="textboxtext2"><br>
						<span class="tabletextremark">(<?=$Lang['StudentRegistry']['CommaAsSeparator'].", ".$Lang["DigitalArchive"]["Max10Words"]?>)</span></td>
					</tr>
				</table>
				<span class="tabletextremark"><?=$Lang['General']['RequiredField']?></span> 
				<p class="spacer"></p>
				</div>			
			
				<div class="edit_bottom">
				<p class="spacer"></p>
				<?=$FormButton?>
				<p class="spacer"></p>
			</div>			
		</td>
	</tr>
</table>

<!-- Content [End] -->
<div id="div1"></div>

</form>
</div>

<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css" type="text/css" media="all" />
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js" type="text/javascript"></script>
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" />
<script language="javascript">

$('#Tag').multicomplete({
	source: [<?=$availableTagName?>]
});

</script>

<?
echo $ldaUI->FOCUS_ON_LOAD("form1.Title"); 

$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>