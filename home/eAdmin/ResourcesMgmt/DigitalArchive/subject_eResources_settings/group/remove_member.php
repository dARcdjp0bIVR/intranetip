<?php
// Using: 
/********************************************************
 *  Modification Log
 * Date: 2013-03-26 (Rita)
 * Detail: add this page for removing group member
 ********************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");

intranet_auth();
intranet_opendb();

$lda = new libdigitalarchive();

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$groupId = $_GET['groupId'];
$user_id = $_POST['user_id'];

$lda->Start_Trans();

if(count($user_id)>0){	
	$successResult =  $lda->Remove_Subject_eResources_Group_Member($groupId,$user_id);	
}

$xmsg ='';
if($successResult){
	$xmsg = 'DeleteSuccess';
	$lda->Commit_Trans();
}else{
	$xmsg = 'DeleteUnsuccess';
	$lda->RollBack_Trans();
}

header("Location:group_member.php?xmsg=$xmsg&GroupID[]=$groupId");


?>