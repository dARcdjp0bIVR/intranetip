<?php

//using: 

/****************************************
 * Date: 	2013-04-02 (Rita)
 * Details:	create this page for adding new group member
 * 
 *****************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

//debug_pr($_SESSION['SSV_USER_ACCESS']);

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();
$fcmy = new Year();
$subject = new subject();

$CurrentPageArr['DigitalArchiveSubjecteResourcesAdminMenu'] = true;
$CurrentPageArr['DigitalArchive_SubjectReference'] = true;

$CurrentPage = "Settings_Group";

$TAGS_OBJ[] = array($Lang['Menu']['DigitalArchive']['Settings']['AccessRight'] );

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();
$ldaUI->LAYOUT_START();

$groupID = $_GET['groupId'];

# Get Group Name 
$groupSql = $lda->Get_Subject_eResources_Group_Sql($groupID);
$groupInfoArr =  $lda->returnArray($groupSql);
$GroupTitle = $groupInfoArr[0]['GroupTitle'];
$PAGE_NAVIGATION[] = array($GroupTitle);

# Get Existing Users
$Sql = $lda->Get_Subject_eResources_Group_Member_Sql($groupID);
$stdInGroupArr = $lda->returnArray($Sql);
$numOfStdInGroupArr = count($stdInGroupArr);
$stdInGroup = array();
for($i=0;$i<$numOfStdInGroupArr;$i++){
	$stdInGroup[]= $stdInGroupArr[$i]['UserID'];
}

$name_field = getNameFieldByLang("USR.");
$typeList = "1";		# 1-teacher only	

if(sizeof($stdInGroup)>0)
	$conds = " AND USR.UserID NOT IN (".implode(',', $stdInGroup).")";

	$sql1 = "SELECT USR.UserID, $name_field as name, ycu.ClassNumber, UserLogin FROM INTRANET_USER USR LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) WHERE USR.RecordType IN ($typeList) AND USR.RecordStatus=1 $conds GROUP BY USR.UserID ORDER BY UserLogin";

	$result1 = $lda->returnArray($sql1,5);

for ($j = 0; $j < sizeof($result1); $j++) 
{
	list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
	$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
}

if(!empty($data_ary)) 
{
	#define yui array (Search by input format )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
	
		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
		$temp_str2 = $this_stu_name;
		
		$liArr .= "[\"". addslashes(intranet_undo_htmlspecialchars($temp_str)) ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}
	
	foreach ($data_ary as $key => $row) 
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);
	
	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
		$temp_str2 = $this_stu_name;
		
		$liArr2 .= "[\"". $this_userlogin ."\", \"". addslashes(intranet_undo_htmlspecialchars($temp_str2)) ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
	### end for searching
}
$student_selected = $linterface->GET_SELECTION_BOX($array_student, "name='student[]' id='student[]' class='select_studentlist' size='15' multiple='multiple'", "");

$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('student[]'))");

$permitted_type = (isset($permitted_type) && $permitted_type!="") ? $permitted_type : "1";

?>

<link href="<?=$PATH_WRT_ROOT?>/templates/<?php echo $LAYOUT_SKIN;?>/css/reading_scheme.css" rel="stylesheet" type="text/css">

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}
      
	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<!-- In-memory JS array begins-->
<!-- Libary begins -->
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->
<script type="text/javascript">

var statesArray = [
    <?= $liArr?>
];

var loginidArray = [
    <?= $liArr2?>
];

var delimArray = [
    ";"
];

function goBack() {
	self.location.href = "group_member.php";	
}

function checkForm()
{
    obj = document.form1;
    
        if(obj.elements["student[]"].length != 0) {
	        for(i=0; i<obj.elements["student[]"].length; i++) {
		     	obj.elements["student[]"].options[i].selected = true;   
	        }
        	return true;
		}
        else
        {
                alert('<?=$i_Discipline_System_alert_PleaseSelectMember?>');
                return false;
        }
}

</script>

<form name="form1" method="POST" onsubmit="return checkForm();" action="new_member_update.php">

<br />


<div>	
	<!-- <a href="index.php"><?=$Lang['DigitalArchive']['SchoolAdminDoc']?></a>
	
	<a href="javascript:goBack();"><?=$Lang['DigitalArchive']['AccessRightSettings']?></a> -->
	
	<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION);?><br />
	<input type="hidden" id="GroupID" name="GroupID" value="'.$groupID.'" />
</div>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<br style="clear: both;"/>
             
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="40%"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Choose_Member ?>  
								:</span></td>
								<td width="10" nowrap="nowrap"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="10"></td>
								<td width="60%" nowrap="nowrap"><span class="tabletext"><?=$i_Discipline_System_Group_Right_Navigation_Selected_Member2 ?> 
								:</span></td>
							</tr>
							<tr>
								<td valign="top" class="tablerow2">
									<table width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td class="tabletext"><?=$eDiscipline["SelectTeacherStaff"]?> </td>
										</tr>
										<tr>
										<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/index.php?fieldname=student[]&page_title=SelectMembers&permitted_type={$permitted_type}&DisplayGroupCategory=1', 9)")?></td>
											</tr>
										<tr>
											<td class="tabletext"><i><?=$i_general_or?></i></td>
										</tr>
										<tr>
											<td class="tabletext"><?=$i_general_search_by_loginid?><br \>
												<div id="statesautocomplete">
													<input type="text" class="tabletext" name="search2" id="search2">
													<div id="statescontainerCC" style="left:142px; top:0px;">
														<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
															<div style="display: none;" class="yui-ac-hd"></div>
															<div class="yui-ac-bd"></div>
															<div style="display: none;" class="yui-ac-ft"></div>
														</div>
														<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
													</div>
												</div>

											</td>
										</tr>
									</table>
								</td>
								<td valign="top" nowrap="nowrap">&nbsp;</td>
								<td valign="top" nowrap="nowrap">
									<table width="100%" border="0" cellpadding="3" cellspacing="0">
									<tr>
										<td><?=$student_selected ?>
										</td>
									</tr>
									<tr>
										<td align="right"><?=$button_remove_html?></td>
									</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
			</table>
		<br>
		</td>
	</tr>
	<tr>
		<td align="center">
			<input type="hidden" name="flag" value="0" />
			<input type="hidden" name="GroupID" value="<?=$groupID ?>" />
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location.href='group_member.php?GroupID[]=$groupID'") ?>
		</td>
	</tr>
</table>

</form>
<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
    var oACDS, oACDS2, oAutoComp, oAutoComp2;
    return {
        init: function() {

            // Instantiate first JS Array DataSource
            /*
            oACDS = new YAHOO.widget.DS_JSArray(statesArray);

            // Instantiate first AutoComplete            
            oAutoComp = new YAHOO.widget.AutoComplete('search1','student[]', 'statescontainer', oACDS);
            oAutoComp.queryDelay = 0;
            oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp.useShadow = true;
            oAutoComp.minQueryLength = 0;
            */
            oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
            oAutoComp2 = new YAHOO.widget.AutoComplete('search2','student[]', 'statescontainerCC', oACDS2);
            oAutoComp2.queryDelay = 0;
            oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp2.useShadow = true;
            oAutoComp2.minQueryLength = 0;
        },

        validateForm: function() {
            // Validate form inputs here
            return false;
        }
    };
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
