<?php

//using: 

/***************************************
 * Date:	2017-03-10 (Pun)
 * Details:  Modified checkForm() fixed check duplicate group code function does not work
 * Date:	2013-04-02 (Rita)
 * Details:  Create this page for adding group
 *
 ****************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive.php");
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

intranet_auth();
intranet_opendb();

//debug_pr($_SESSION['SSV_USER_ACCESS']);

# Check Access Right for Subject eResources Admin 
if(!$_SESSION['SSV_USER_ACCESS']['eLearning-SubjecteResources']) {
	header("Location: ../../index.php");
	exit;	
}

$linterface = new interface_html();
$ldaUI = new libdigitalarchive_ui();
$lda = new libdigitalarchive();
$fcmy = new Year();
$subject = new subject();

$CurrentPageArr['DigitalArchiveSubjecteResourcesAdminMenu'] = true;
$CurrentPageArr['DigitalArchive_SubjectReference'] = true;

$CurrentPage = "Settings_Group";

$TAGS_OBJ[] = array($Lang['Menu']['DigitalArchive']['Settings']['AccessRight'] );

$MODULE_OBJ = $ldaUI->GET_MODULE_OBJ_ARR();
$ldaUI->LAYOUT_START();

# Page Navigation 
$PAGE_NAVIGATION[] = array($button_new);


//$allForms = $fcmy->Get_All_Year_List();
//$formDisplay = getSelectByArray($allForms, 'name="ClassLevelID[]" id="ClassLevelID[]" multiple size="10"', $ClassLevelID, 0, 1);


# Obtain Current Data
$groupID = $_GET['GroupID'][0];
$groupInfoArray=array();

if($groupID!=''){
	$Sql = $lda->Get_Subject_eResources_Group_Sql($groupID);
	$groupInfoArray = $lda->returnArray($Sql);
	
	$thisGroupID = $groupInfoArray[0]['GroupID'];
	$thisGroupName = $groupInfoArray[0]['GroupTitle'];
	$thisGroupCode = $groupInfoArray[0]['GroupCode'];
	$thisDescription = $groupInfoArray[0]['Description'];
	$thisSubjectID = $groupInfoArray[0]['SubjectID']; 

	$thisClassLevelIDString = $groupInfoArray[0]['FormIDArrSting']; 
	
}


# Obtain Subject Data
$allSubjects = $subject->Get_All_Subjects();
$tempAry = array();
for($i=0, $i_max=sizeof($allSubjects); $i<$i_max; $i++) {
	$tempAry[$i] = array($allSubjects[$i]['RecordID'], Get_Lang_Selection($allSubjects[$i]['CH_DES'], $allSubjects[$i]['EN_DES']));
}
$allSubjectsRevised = $tempAry;

# Subject Selection Box Display 
$subjectDisplay = getSelectByArray($allSubjectsRevised, 'name="SubjectID[]" onchange="js_Change_Subject(\'ChangeSubject\');" id="SubjectID" ', $thisSubjectID, 0, 1);

# Select All Btn for Form
$SelectAllClassBtn = $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll('ClassLevelID'); return false;");
			





$html['DisplayTable'] = '';
$html['DisplayTable'] = '<table width="100%" class="form_table_v30">';

	# Group Code
	$html['DisplayTable'] .= '<tr>';
		$html['DisplayTable'] .= '<td class="field_title">' .$linterface->RequiredSymbol() . $Lang['DigitalArchive']['GroupCode'] . '<td>';
		$html['DisplayTable'] .= '<td  width="75%">' .  $linterface->GET_TEXTBOX('groupCode', 'groupCode', $thisGroupCode) . $linterface->Get_Form_Warning_Msg('groupCodeWarningDiv', $Lang['DigitalArchive']['PleaseFillIn'] . $Lang['DigitalArchive']['GroupCode'], '') . $linterface->Get_Form_Warning_Msg('groupCodeExistWarningDiv', $Lang['DigitalArchive']['GroupCodeAlreadyExists'], '') . '<td>';
	$html['DisplayTable'] .= '</tr>';

	# Group Name
	$html['DisplayTable'] .= '<tr>';
		$html['DisplayTable'] .= '<td class="field_title">' . $linterface->RequiredSymbol() . $Lang['DigitalArchive']['GroupName'] . '<td>';
		$html['DisplayTable'] .= '<td  width="75%">' . $linterface->GET_TEXTBOX('groupName', 'groupName', $thisGroupName) . $linterface->Get_Form_Warning_Msg('groupNameWarningDiv', $Lang['DigitalArchive']['PleaseFillIn'] . $Lang['DigitalArchive']['GroupName'], '') .'<td>';
	$html['DisplayTable'] .= '</tr>';

	# Group Description
	$html['DisplayTable'] .= '<tr>';
		$html['DisplayTable'] .= '<td class="field_title">' . $Lang["DigitalArchive"]["Description"]. '<td>';
		$html['DisplayTable'] .= '<td  width="75%">' . $linterface->GET_TEXTAREA('description', $thisDescription) .'<td>';
	$html['DisplayTable'] .= '</tr>';
	
	# Subject 
	$html['DisplayTable'] .= '<tr>';
		$html['DisplayTable'] .= '<td class="field_title">' . $linterface->RequiredSymbol() . $Lang["DigitalArchive"]["Subject"]. '<td>';
		$html['DisplayTable'] .= '<td  width="75%">' . $subjectDisplay . $linterface->Get_Form_Warning_Msg('subjectWarningDiv', $Lang['DigitalArchive']['PleaseFillIn'] . $Lang['DigitalArchive']['Subject'], '') .'<td>';
	$html['DisplayTable'] .= '</tr>'; 
	 

	# Form 
	$html['DisplayTable'] .= '<tr>';
		$html['DisplayTable'] .= '<td class="field_title">' . $linterface->RequiredSymbol() . $Lang["General"]["Form"]. '<td>';
		$html['DisplayTable'] .= '<td width="75%"><span id="FormSelectionDisplayLayer"></span>'.$SelectAllClassBtn. $linterface->Get_Form_Warning_Msg('formWarningDiv', $Lang['DigitalArchive']['PleaseFillIn'] . $Lang["General"]["Form"], '') . '<td>';
	$html['DisplayTable'] .= '</tr>'; 
		
	$html['DisplayTable'] .= '<input type="hidden" id="groupID" name="groupID" value="'.$thisGroupID.'" />';
	$html['DisplayTable'] .= '<input type="hidden" id="originalGroupCode" name="originalGroupCode" value="'.intranet_htmlspecialchars($thisGroupCode).'" />';
	$html['DisplayTable'] .= '<input type="hidden" id="ClassLevelIDArrString" name="ClassLevelIDArrString" value="'.$thisClassLevelIDString.'" />';
	
	
$html['DisplayTable'] .= '</table>';
		


?>

<script language="javascript">

$(document).ready(function(){
	var groupID =  $.trim($('#groupID').val());
	if(!groupID){
		js_Change_Subject('ChangeSubject');
	}
	else{
		js_Change_Subject('RemainUnchanged');
	}
});



function js_Change_Subject(Action){
	var selectedSubjectID = $('#SubjectID option:selected').val();
	var ClassLevelIDArrString = $('#ClassLevelIDArrString').val();
	
	$.ajax({
	url:      	"ajax_reload_form.php",
	type:     	"POST",
	data:     	'&selectedSubjectID='+selectedSubjectID + '&ClassLevelIDArrString=' + ClassLevelIDArrString ,
	async:		false,
	error:    	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
			  	},
	success: function(data)
			 {		
				if(data)
				{			
					$("#FormSelectionDisplayLayer").html(data);	
					
					if(Action=='ChangeSubject'){
						$(".ClassLevelID option").attr('selected','selected');
					}				
				}
			}
	 });	

}


function SelectAll(ObjID)
{
	$("#"+ObjID+" option").attr("selected","selected");
}


function checkForm() {
	obj = document.form1;
	
	var error = 0;
	var groupID =  $.trim($('#groupID').val());
	var groupCode = $('#groupCode').val();
	
	var orignalGroupCode = $('#originalGroupCode').val(); 
	
	var groupName =  $.trim($('#groupName').val());
	
	var selectedSubjectID = $('#SubjectID option:selected').val();
	var classLevelID = $('#ClassLevelID option:selected').val();
	
	
	var str = $("form1").serialize();

	if(groupName==''){
		error++;
		$('#groupNameWarningDiv').attr('style', '');
	}else{
		$('#groupNameWarningDiv').attr('style', 'display:none');
	}
	
	if(!selectedSubjectID){
		error++;
		$('#subjectWarningDiv').attr('style', '');
	}
	else{
		$('#subjectWarningDiv').attr('style', 'display:none');
	}
	if(!classLevelID){
		error++;
		$('#formWarningDiv').attr('style', '');
	}else{
		$('#formWarningDiv').attr('style', 'display:none');
	}
	
	if(groupCode==''){		
		error++;
		$('#groupCodeWarningDiv').attr('style', '');
	}else{
		$('#groupCodeWarningDiv').attr('style', 'display:none');
		
		 $.post( 
         "ajax_check_group_code.php",
         { groupID:  groupID ,
           groupCode: groupCode ,
           orignalGroupCode: orignalGroupCode
         },
         function(data) {
         //alert(data);
         	
			if(data=='GroupCodeAlreadyExist')
			{										
				$('#groupCodeExistWarningDiv').attr('style', '');	
				error++;			
			}else{
				$('#groupCodeExistWarningDiv').attr('style', 'display:none;');			
				
            	if(error==0){
            		obj.submit();
            	}
			}
         });
	
	}
	
}

</script>



<form name="form1" id="form1" action="new_update.php" method="POST">
	<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result, $SpMessage);?></td>
		</tr>
		<tr>
			<td>
			<?=$html['DisplayTable'];?>
			</td>	
		</tr>
		<tr>
			<td class="dotline">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		<tr>
			<td align="center">	
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript: return checkForm()")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php'")?>					
			</td>
		</tr>
	</table>
</form>



<?
$ldaUI->LAYOUT_STOP();
intranet_closedb();
?>