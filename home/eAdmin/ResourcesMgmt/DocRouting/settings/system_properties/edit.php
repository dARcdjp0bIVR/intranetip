<?php
// using : ivan

/******************************
 * Change Log:
 * 2017-07-11 Anna
 * - added AuthenticationSetting
 ******************************/
 
# Page Title
$TAGS_OBJ[] = array($Lang['General']['SystemProperties']);

# Layout Start
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Settings_SystemProperties');

$settingAssoAry = $indexVar['libDocRouting']->returnSettingAry();


// debug_pr($settingAssoAry);
// debug_pr($docRoutingConfig['settings']);
$x = '';
### email notification
$x .= $indexVar['libDocRouting_ui']->GET_NAVIGATION2_IP25($Lang['DocRouting']['EmailNotification']);
$x .= '<br />'."\r\n";
$x .= '<table class="form_table_v30">'."\r\n";
	// new route received by users
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['DocRouting']['NewRouteReceived'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button('emailNotification_newRouteReceived_EnableRadio', 'emailNotification_newRouteReceived', $docRoutingConfig['settings']['emailNotification_newRouteReceived']['enable'], ($settingAssoAry['emailNotification_newRouteReceived']==$docRoutingConfig['settings']['emailNotification_newRouteReceived']['enable']), $Class="", $Lang['Btn']['Enable']);
			$x .= '&nbsp;&nbsp;';
			$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button('emailNotification_newRouteReceived_DisableRadio', 'emailNotification_newRouteReceived', $docRoutingConfig['settings']['emailNotification_newRouteReceived']['disable'], ($settingAssoAry['emailNotification_newRouteReceived']==$docRoutingConfig['settings']['emailNotification_newRouteReceived']['disable']), $Class="", $Lang['Btn']['Disable']);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";

	// new feedback in route
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['DocRouting']['NewFeedbackReceivedInOwnRouting'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button('emailNotification_newFeedbackInRoute_EnableRadio', 'emailNotification_newFeedbackInRoute', $docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['enable'], ($settingAssoAry['emailNotification_newFeedbackInRoute']==$docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['enable']), $Class="", $Lang['Btn']['Enable']);
			$x .= '&nbsp;&nbsp;';
			$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button('emailNotification_newFeedbackInRoute_DisableRadio', 'emailNotification_newFeedbackInRoute', $docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['disable'], ($settingAssoAry['emailNotification_newFeedbackInRoute']==$docRoutingConfig['settings']['emailNotification_newFeedbackInRoute']['disable']), $Class="", $Lang['Btn']['Disable']);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";

$x .= $indexVar['libDocRouting_ui']->GET_NAVIGATION2_IP25($Lang['DocRouting']['Security']);
$x .= '<table class="form_table_v30">'."\r\n";
// new route received by users
	$x .= '<tr>'."\r\n";
		$x .= '<td class="field_title">'.$Lang['DocRouting']['AuthenticationSetting'].'</td>'."\r\n";
		$x .= '<td>'."\r\n";
			$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button('AuthenticationSetting_EnableRadio', 'AuthenticationSetting', $docRoutingConfig['settings']['AuthenticationSetting']['enable'], ($settingAssoAry['AuthenticationSetting']==$docRoutingConfig['settings']['AuthenticationSetting']['enable']), $Class="", $Lang['Btn']['Enable']);
			$x .= '&nbsp;&nbsp;';
			$x .= $indexVar['libDocRouting_ui']->Get_Radio_Button('AuthenticationSetting_DisableRadio', 'AuthenticationSetting', $docRoutingConfig['settings']['AuthenticationSetting']['disable'], ($settingAssoAry['AuthenticationSetting']==$docRoutingConfig['settings']['AuthenticationSetting']['disable']), $Class="", $Lang['Btn']['Disable']);
		$x .= '</td>'."\r\n";
	$x .= '</tr>'."\r\n";
$x .= '</table>'."\r\n";

$htmlAry['contentTable'] = $x;


$pe = $indexVar['libDocRouting']->getEncryptedParameter('settings/system_properties', 'edit_update');
$htmlAry['submitButton'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'goSubmit(\''.$pe.'\');');

$pe = $indexVar['libDocRouting']->getEncryptedParameter('settings/system_properties', 'index');
$htmlAry['cancelButton'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'docRoutingObj.goToPage(\''.$pe.'\');');


include_once($indexVar['templateScript']);
$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>