<?php
// using : RITA

/******************************
 * Change Log:
 * 
 ******************************/
# Define Variable
$PresetType = $docRoutingConfig['INTRANET_DR_PRESET']['PresetType']['DocumentInstruction'];
$clearCoo =  $indexVar['paramAry']['clearCoo'];


# Page Title

# Access Right Checking
if($indexVar['drUserIsAdmin']){
	
	$targetUser = $indexVar['paramAry']['targetUser'];
	
	include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");
	
	$libDocRouting_preset = new libDocRouting_preset();
	$accessRightCheckingArr = $libDocRouting_preset->getAccessRightTagControl($PresetType, $targetUser);
	
	$targetUser = $accessRightCheckingArr[0];
	$newPresetInstructionLink = $accessRightCheckingArr[1];
	$TAGS_OBJ = $accessRightCheckingArr[2];
	
}else{
	
	$TAGS_OBJ[] = array($Lang['DocRouting']['PresetDocInstruction']);
	$targetUser = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['NonAdminView'];
	
	# Add New Preset Instruction
	$newPresetInstructionLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/preset_doc_instruction', 'new', array('PresetID' => $PresetID));	
}

#Set Cookies
$arrCookies = array();
$arrCookies[] = array("doc_settings_preset_doc_instruction_page_size", "numPerPage");
$arrCookies[] = array("doc_settings_preset_doc_instruction_page_number", "pageNo");
$arrCookies[] = array("doc_settings_preset_doc_instruction_page_order", "order");
$arrCookies[] = array("doc_settings_preset_doc_instruction_page_field", "field");	

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$doc_settings_preset_doc_instruction_page_size = '';
}
else{
	updateGetCookies($arrCookies);
}

# Get Return Message 
$msg = $indexVar['paramAry']['msg'];

# Layout Start
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Settings_PresetDocInstruction', $msg);

# Get Display Result Table 
$htmlAry['PresetDocDisplay']  = $indexVar['libDocRouting_ui']->getPresetDisplayTable($newPresetInstructionLink, $PresetType, $field, $order, $pageNo, $targetUser);
//$htmlAry['PresetDocDisplay']  = $indexVar['libDocRouting_ui']->getPresetDisplayTable($newPresetInstructionLink, $PresetType, $field, $order, $pageNo, $targetUser);



include_once($indexVar['templateScript']);
$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>