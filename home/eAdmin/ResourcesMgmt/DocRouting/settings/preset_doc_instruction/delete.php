<?php

$ldocrouting = $indexVar['libDocRouting'];

/******************************
 * Change Log:
 * 
 ******************************/
include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");	
$libDocRouting_preset = new libDocRouting_preset();

$PresetIDArr = $_POST['PresetID'];
$numOfPresetIDArr = count($PresetIDArr);

if($indexVar['drUserIsAdmin']){
	if(isset($_POST['targetUser'])){
		$targetUser =  $_POST['targetUser'];
	}else{	
		$targetUser =  $indexVar['paramAry']['targetUser'];
	}
}


for($i=0;$i<$numOfPresetIDArr;$i++){
	$PresetID = $PresetIDArr[$i];

	if($PresetID!=''){
		$result = $libDocRouting_preset -> removePresetRecord($PresetID);
	}
}

if(!in_array(false,array($result))){
	$libDocRouting_preset ->Commit_Trans();
	$msg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}else{
	$libDocRouting_preset ->RollBack_Trans();	
	$msg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}

if($indexVar['drUserIsAdmin']){
	$redirecLink = $ldocrouting->getEncryptedParameter('settings/preset_doc_instruction', 'index', array('targetUser' => $targetUser, 'msg' => $msg));
}
else{
	$redirecLink = $ldocrouting->getEncryptedParameter('settings/preset_doc_instruction', 'index', array('msg' => $msg));
}				 				
header("Location:"."?pe=".$redirecLink.'&msg='.$msg);

?>