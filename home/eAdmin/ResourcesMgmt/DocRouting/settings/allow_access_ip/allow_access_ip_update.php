<?php 


$allowed_IPs = trim($indexVar['libDocRouting']->returnSettingValueByName("AllowAccessIP"));
$ip_addresses = explode("\n", $allowed_IPs);
checkCurrentIP($ip_addresses, $Lang['DocRouting']['DocRouting']);


$successAry = array();


$indexVar['libDocRouting']->Start_Trans();

$successAry['AllowAccessIP'] = $indexVar['libDocRouting']->updateSetting('AllowAccessIP', $indexVar['paramAry']['AllowAccessIP']);

if (in_array(false, $successAry)) {
	$indexVar['libDocRouting']->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}
else {
	$indexVar['libDocRouting']->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}

$paramAry = array();
$paramAry['returnMsgKey'] = $returnMsgKey;
$pe = $indexVar['libDocRouting']->getEncryptedParameter('settings/allow_access_ip', 'index', $paramAry);

header('Location: ?pe='.$pe);
?>