<?php 
$clearCoo =  $indexVar['paramAry']['clearCoo'];
# Page Title

# Access Right Checking
if($indexVar['drUserIsAdmin']){
	$targetUser = $indexVar['paramAry']['targetUser'];
	
	include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");
	
	$libDocRouting_preset = new libDocRouting_preset();

	$TAGS_OBJ[] = array($Lang['DocRouting']['AllowAccessIP']);
	
}else{
	$TAGS_OBJ[] = array($Lang['DocRouting']['AllowAccessIP']);
	$targetUser = $docRoutingConfig['INTRANET_DR_ROUTE_Page']['NonAdminView'];
	
}


$msg = $indexVar['paramAry']['msg'];

# Layout Start
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Settings_AllowAccessIP', $msg);
$AllowAccessIP= '';
$AllowAccessIP = trim($indexVar['libDocRouting']->returnSettingValueByName("AllowAccessIP"));


$settingAssoAry = $indexVar['libDocRouting']->returnSettingAry();
$x ='<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" >';
	$x .='<tr><td valign="top" nowrap="nowrap" class="tabletext">';
		$x .=$Lang['Security']['TerminalIPList'];
		$x .='</td><td class="tabletext" width="70%"><span class="tabletextremark">';
		$x .=$Lang['Security']['TerminalIPInput'];
$x .='<br />';
$x .=$Lang['Security']['TerminalYourAddress'].':'.getRemoteIpAddress().'</span><br />';
$x .=$indexVar['libDocRouting_ui']->GET_TEXTAREA("AllowAccessIP", $AllowAccessIP);
$x .='</td></tr><tr><td colspan="2" class="formfieldtitle tabletext"><br />'.$Lang['Security']['TerminalIPSettingsDescription'].'</td></tr></table>';
$htmlAry['contentTable'] = $x;


$pe = $indexVar['libDocRouting']->getEncryptedParameter('settings/allow_access_ip', 'allow_access_ip_update');
$htmlAry['submitButton'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button', 'goSubmit(\''.$pe.'\');');

//$pe = $indexVar['libDocRouting']->getEncryptedParameter('settings/allow_access_ip', 'allow_access_ip_view');
//$htmlAry['cancelButton'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Cancel'], 'button', 'docRoutingObj.goToPage(\''.$pe.'\');');
$htmlAry['resetButton'] .= $indexVar['libDocRouting_ui']->GET_ACTION_BTN($Lang['Btn']['Reset'], 'reset');


include_once($indexVar['templateScript']);
$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>