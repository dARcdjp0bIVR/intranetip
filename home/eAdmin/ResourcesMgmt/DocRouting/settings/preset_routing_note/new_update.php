<?php

$ldocrouting = $indexVar['libDocRouting'];

/******************************
 * Change Log:
 * 
 ******************************/
$PresetType = $_POST['PresetType'];
$presetTitle = $_POST['presetTitle'];
$presetContent = $_POST['presetContents'];
$targetType = $_POST['targetType'];
$inputUserID = $_POST['UserID'];
$PresetID = $_POST['PresetID'];

if($indexVar['drUserIsAdmin']){
	if(isset($_POST['targetUser'])){
		$targetUser =  $_POST['targetUser'];
	}else{	
		$targetUser =  $indexVar['paramAry']['targetUser'];
	}
}


include_once($PATH_WRT_ROOT."/includes/DocRouting/libDocRouting_preset.php");	
$libDocRouting_preset = new libDocRouting_preset();
if($PresetID!=''){
	$result = $libDocRouting_preset -> updatePresetRecord($PresetID, $PresetType, $presetTitle, $presetContents,$targetType );

}elseif($PresetID==''){
	$result = $libDocRouting_preset -> addNewPresetRecord($PresetType, $presetTitle, $presetContents,$targetType );
}



if(!in_array(false,array($result))){
	if($PresetID!=''){
		$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
		
	}else{
		$msg = $Lang['General']['ReturnMessage']['AddSuccess'];
	}
}else{
	if($PresetID!=''){
		$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
		
	}else{
		$msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
	}
}

if($indexVar['drUserIsAdmin']){

	$redirecLink = $ldocrouting->getEncryptedParameter('settings/preset_routing_note', 'index', array('targetUser' => $targetUser, 'msg' => $msg));					 				
		
}
else{
	$redirecLink = $ldocrouting->getEncryptedParameter('settings/preset_routing_note', 'index', array('msg' => $msg));					 				
}

//
//$redirecLink = $ldocrouting->getEncryptedParameter('settings/preset_routing_note', 'index', array('msg' => $msg));
					 				
header("Location:"."?pe=".$redirecLink.'&msg='.$msg);

?>