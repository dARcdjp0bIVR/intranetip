<?php

//$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];

$DocumentID = $_REQUEST['DocumentID'];
$RouteID  = $_REQUEST['TargetRouteID'];
$FeedbackID = $_REQUEST['TargetFeedbackID'];
$Comment = $_REQUEST['TargetComment'];

if(trim($FeedbackID) == ''){
	$FeedbackID = $ldocrouting->insertComment($DocumentID,$RouteID,$indexVar['drUserId'], $Comment);
}

if($FeedbackID){
	$success = $ldocrouting->addFile($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'], $FeedbackID, $_FILES);
}
if($success){
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

$ldocrouting->releaseRouteLock($RouteID);

echo '<script>';
echo 'if(window.parent.js_Finish_Upload_Files){
		window.parent.js_Finish_Upload_Files('.$RouteID.',"'.$msg.'");
		}';
echo '</script>';

$indexVar['libDocRouting']->unsetSignDocumentSession();
?>