<?php
// Editing by 
/*
 * 2016-01-28 (Carlos): Added and update StatusChangedBy and StatusChangedDate to trace the last change status person and time. 
 */

$ldocrouting = $indexVar['libDocRouting'];

$DocumentID = $_REQUEST['DocumentID'];
$RecordStatus = $_REQUEST['RecordStatus'];

$sql = "UPDATE INTRANET_DR_ENTRY SET RecordStatus='".$RecordStatus."', StatusChangedBy='".$indexVar['drUserId']."', StatusChangedDate=NOW() WHERE DocumentID='".$DocumentID."'";
$success = $ldocrouting->db_db_query($sql);

echo $success?$Lang['General']['ReturnMessage']['UpdateSuccess']:$Lang['General']['ReturnMessage']['UpdateUnsuccess'];

$indexVar['libDocRouting']->unsetSignDocumentSession();
?>