<?php

/*
 * 2016-07-06 (Carlos): $sys_custom['DocumentRouting_IgnoreRoutingDependency'] - ignore period dependency of previous routings.
 */

$ldocrouting = $indexVar['libDocRouting'];
$documentID = $_REQUEST['DocumentID'];

if(!$sys_custom['DocumentRouting_IgnoreRoutingDependency'])
{
	$routings = $ldocrouting->getDocRoutingUserStatusCount($documentID);
	
	$latest_datetime = '';
	for($i=0;$i<count($routings);$i++){
		if($routings[$i]['EffectiveType'] == $docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']){
			$latest_datetime = $routings[$i]['EffectiveEndDate'];
		}
	}
}

echo $latest_datetime;

$indexVar['libDocRouting']->unsetSignDocumentSession();
?>