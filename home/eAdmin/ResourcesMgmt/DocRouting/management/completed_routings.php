<?php
// using : 

/******************************
 * Change Log:
 * 2016-02-19 (Carlos): pass page type to getAdvanceSearchDiv() for determine where academic year selection filter should be displayed. 
 * 2013-10-03 (Henry): add advance search button
 ******************************/
$clearCoo = $indexVar['paramAry']['clearCoo'];
# Page Title
$TAGS_OBJ[] = array($Lang['DocRouting']['CompletedRoutings']);

//#Set Cookies
$arrCookies = array();
//$arrCookies[] = array("ck_doc_management_completed_rountings_star_staus", "starStatus");
//$arrCookies[] = array("ck_doc_management_completed_rountings_create_status", "createStatus");
//$arrCookies[] = array("ck_doc_management_completed_rountings_follow_status", "followStatus");
//
$arrCookies[] = array("ck_doc_management_current_routings_keyword", 'keyword');
$arrCookies[] = array("ck_doc_management_current_routings_routingTitle", 'routingTitle');
$arrCookies[] = array("ck_doc_management_current_routings_byDateRange", 'byDateRange');
$arrCookies[] = array("ck_doc_management_current_routings_StartDate", 'StartDate');
$arrCookies[] = array("ck_doc_management_current_routings_EndDate", 'EndDate');
$arrCookies[] = array("ck_doc_management_current_routings_byDateRange2", 'byDateRange2');
$arrCookies[] = array("ck_doc_management_current_routings_StartDate2", 'StartDate2');
$arrCookies[] = array("ck_doc_management_current_routings_EndDate2", 'EndDate2');
$arrCookies[] = array("ck_doc_management_current_routings_createdBy", 'createdBy');
$arrCookies[] = array("ck_doc_management_current_routings_tag", 'tag');
$arrCookies[] = array("ck_doc_management_current_routings_academic_year", 'academic_year');
$arrCookies[] = array("ck_doc_management_current_routings_flag", 'flag');

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_doc_management_current_rountings_star_staus = '';
	$ck_doc_management_current_rountings_create_status = '';
	$ck_doc_management_current_rountings_follow_status = '';
}
else{
	updateGetCookies($arrCookies);
}

# Layout Start
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Mgmt_CompletedRoutings');
echo $indexVar['libDocRouting_ui']->Include_Cookies_JS_CSS();
$libuser = new libuser();

# Add New Routing Link
//$newRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'edit_doc', '');

# Instruction Message
//$instructionMessage =  $indexVar['libDocRouting_ui'] -> Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'], $others="");	  

# Search Box
$searchBox = $indexVar['libDocRouting_ui'] -> getAdvanceSearchDiv($docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings']);
$searchBox .= $indexVar['libDocRouting_ui'] -> Get_Search_Box_Div('keyword', stripslashes(stripslashes($keyword)), 'onkeydown="checkGoSearch(event);"');
$DisplayUpdateLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'completed_routings', '');

$htmlAry['CurrentDisplay'] = $indexVar['libDocRouting_ui'] ->getRoutingDisplayTable($newRoutingLink, $DisplayUpdateLink,$searchBox,$instructionMessage, false, $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings']);
//$htmlAry['CurrentDisplay'] = $indexVar['libDocRouting_ui'] ->getRoutingDisplayTable($newRoutingLink, $DisplayUpdateLink,$searchBox,$instructionMessage, false, $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings']);

# Routing Link For JS  
$newRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'ajax_reload_routing', '');
$updateStarStatusLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'ajax_update_star_status', '');
  
include_once($indexVar['templateScript']);

$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>