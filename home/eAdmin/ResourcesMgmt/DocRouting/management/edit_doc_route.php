<?php
// editing by 
/*
 * 2014-02_19 (Tiffay): handle DA files
 * 2013-01-08 (Carlos): assign file versioning for same name attachments
 */
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

# Page Title
$pageType = ($indexVar['paramAry']['pageType']=='')? $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings'] : $indexVar['paramAry']['pageType'];
if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']){
	$TAGS_OBJ[] = array($Lang['DocRouting']['CurrentRoutings']);
	$pageCode = 'Mgmt_CurrentRoutings';
}
else{
	$TAGS_OBJ[] = array($Lang['DocRouting']['DraftRoutings']);
	$pageCode = 'Mgmt_DraftRoutings';
}
$returnMsgKey = $indexVar['paramAry']['returnMsgKey'];
$indexVar['libDocRouting_ui']->echoModuleLayoutStart($pageCode, $Lang['General']['ReturnMessage'][$returnMsgKey]);



$lfilesystem = new libfilesystem();
$indexVar['libfilesystem'] = $lfilesystem;
$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];

$dataAry = array();

$dataAry['PageType'] = $pageType;
$dataAry['DocumentID'] = $_POST['DocumentID'];
$dataAry['Title'] = $_POST['Title'];
$dataAry['Instruction'] = $_POST['Instruction'];
$dataAry['DocumentType'] = $_POST['DocumentType'];
$dataAry['PhysicalLocation'] = $_POST['PhysicalLocation'];
$dataAry['AllowAdHocRoute'] = $_POST['AllowAdHocRoute'];
$dataAry['DocTags'] = $_POST['DocTags'];
$dataAry['RecordStatus'] = $_POST['RecordStatus'];
$dataAry['tmpRecordStatus'] = $_POST['tmpRecordStatus'];

$htmlAry['ajaxGetPresetContentPath'] = $ldocrouting->getEncryptedParameter('management', 'ajax_get_preset_content');

//if($_POST['DocumentID'] == '' && $_POST['tmpRecordStatus']==''){ // new temp doc
//	$dataAry['tmpRecordStatus'] = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp'];
//}
if($dataAry['tmpRecordStatus']==$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp']){
	$dataAry['RecordStatus'] = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp'];
}

$documentID = $ldocrouting->updateDREntry($dataAry);
$dataAry['DocumentID'] = $documentID;
$dataAry['RecordStatus'] = $_POST['RecordStatus'];

$indexVar['dataAry'] = $dataAry;

if($dataAry['DocumentType'] == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File'] && count($_FILES)>0){
	$ldocrouting->addFile($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document'], $documentID, $_FILES);
}

if($dataAry['DocumentType'] == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA'] && count($chosen_file)>0){
	$ldocrouting->addDAFile($chosen_file, $documentID,$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document']);
}

if($dataAry['DocumentType'] != $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File'] && count($_POST['FileID'])>0){
	$ldocrouting->deleteDRAttachment($_POST['FileID']);
}
if($dataAry['DocumentType'] != $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA'] && count($_POST['FileID_DA'])>0){
	$ldocrouting->deleteDRAttachment($_POST['FileID_DA']);
}

if(count($_POST['DeleteFileID'])>0){
	$ldocrouting->deleteDRAttachment($_POST['DeleteFileID']);
}

$tagIDs = $ldocrouting->getEntryTag(array($documentID));
if(count($tagIDs)>0){
	$tagIdAry = Get_Array_By_Key($tagIDs,'TagID');
	$ldocrouting->deleteEntryTagDbRecord($documentID, $tagIdAry);
}
if(trim($dataAry['DocTags']) != ""){
	$ldocrouting->saveEntryTag($documentID,addslashes(trim($dataAry['DocTags'])));
}

if($dataAry['DocumentType'] == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']) {
	// update file versioning for same file name files after add/remove
	$ldocrouting->updateDocFileVersioning($dataAry['DocumentID'],$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document']);
}
if($dataAry['DocumentType'] == $docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['DA']) {
	// update file versioning for same file name files after add/remove
	$ldocrouting->updateDocFileVersioning($dataAry['DocumentID'],$docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Document']);
}



include_once($indexVar['templateScript']);

$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>