<?php
// Editing by 
/*
 * 2014-11-17 (Carlos): Send alert email after signed routing
 * 2014-11-05 (Carlos): Added sign routing by token logic
 */
ini_set("memory_limit", "300M");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

$ldocrouting = $indexVar['libDocRouting'];

$DocumentID = $_REQUEST['DocumentID'];
$RouteID = $_REQUEST['RouteID'];
//$FeedbackID = $_REQUEST['FeedbackID'];

if(isset($indexVar['tok'])){
	$token = $indexVar['tok'];
	$sign_record = $indexVar['libDocRouting']->getSignEmailDocumentByToken($indexVar['tok']);
	if(count($sign_record)>0){ // it is a valid token
		$token_valid = true;
	}
}

$success = array();
$ldocrouting->Start_Trans();


//$success['Sign'] = $ldocrouting->updateFeedbackStatus($docRoutingConfig['INTRANET_DR_ROUTE_FEEDBACK']['RecordStatus']['Completed'],'','',$FeedbackID);
$success['Sign'] = $ldocrouting->updateTargetUserStatus($docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed'],$RouteID,$indexVar['drUserId']);

$routingStatusCount = $ldocrouting->getDocRoutingUserStatusCount($DocumentID);

$isCurrentRouteCompleted = false;
$nextRouteID = '';
for($i=0;$i<count($routingStatusCount);$i++){
	if($routingStatusCount[$i]['RouteID'] == $RouteID){
		$isCurrentRouteCompleted = ($routingStatusCount[$i]['UserTotal'] == $routingStatusCount[$i]['CompletedTotal']);
		$nextRouteID = $routingStatusCount[$i+1]['RouteID'];
		break;
	}
}

if (in_array(false, $success)) {
	$ldocrouting->RollBack_Trans();
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}
else {
	$ldocrouting->Commit_Trans();
	if ($isCurrentRouteCompleted && $nextRouteID !='' && is_numeric($nextRouteID) && $nextRouteID > 0 && $ldocrouting->returnSettingValueByName('emailNotification_newRouteReceived')) {
		$success['Email'] = $ldocrouting->sendNotificationEmailForNewRouteReceived($nextRouteID);
	}
	if(isset($indexVar['tok']) && $token_valid){
		$sign_route_record = $ldocrouting->getSignEmailRoute($sign_record[0]['RecordID'],$RouteID);
		if(count($sign_route_record)>0){
			$ldocrouting->signDocumentRouting($sign_route_record[0]['RecordID']);
			$ldocrouting->sendNotificationEmailForRouteSigned($RouteID, $sign_record[0]['UserID']);
		}
		$indexVar['libDocRouting']->unsetSignDocumentSession();
	}
	
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
?>