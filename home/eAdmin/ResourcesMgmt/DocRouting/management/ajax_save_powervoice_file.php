<?php

$ldocrouting = $indexVar['libDocRouting'];

$DocumentID = $_REQUEST['DocumentID'];
$user_id = $indexVar['drUserId'];
$RouteID  = $_REQUEST['RouteID'];
$VoiceFile = trim(rawurldecode(stripslashes($_REQUEST['VoiceFile'])));
$VoicePath = rawurldecode($_REQUEST['VoicePath']);

$FeedbackID = $ldocrouting->insertComment($DocumentID,$RouteID,$user_id,'');
/*
$sql = "SELECT FeedbackID FROM INTRANET_DR_ROUTE_FEEDBACK WHERE RouteID='".$RouteID."' AND UserID='".$user_id."'";
$tmpRecord = $ldocrouting->returnVector($sql);
$FeedbackID = $tmpRecord[0];
*/

if($FeedbackID > 0){
	$fileAry = array();
	$fileAry[] = array('name'=>$VoiceFile,'tmp_name'=>$VoicePath."/".$VoiceFile);
	
	$success = $ldocrouting->addFile($docRoutingConfig['INTRANET_DR_FILE']['LinkToType']['Feedback'], $FeedbackID, $fileAry,$docRoutingConfig['INTRANET_DR_FILE']['RecordType']['PowerVoice']);
}
if($success){
	echo $Lang['General']['ReturnMessage']['UpdateSuccess'];
}else{
	echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}

$ldocrouting->releaseRouteLock($RouteID);

$indexVar['libDocRouting']->unsetSignDocumentSession();
?>