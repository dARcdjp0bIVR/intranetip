<?php
// Editing by 
/*
 * 2020-04-24 (Tommy): add checking draft routing to current routing for sending email to user
 * 2014-10-15 (Carlos) [ip2.5.5.10.1]: Fix new routing notification email wrong first routeID bug
 */
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

$ldocrouting = $indexVar['libDocRouting'];
$backToStep1 = $_POST['BackToStep1'];

$result = array();

$docDataAry = array();
$docDataAry['DocumentID'] = $_POST['DocumentID'];
$docDataAry['Title'] = $_POST['Title'];
$docDataAry['Instruction'] = $_POST['Instruction'];
$docDataAry['DocumentType'] = $_POST['DocumentType'];
$docDataAry['PhysicalLocation'] = $_POST['PhysicalLocation'];
$docDataAry['AllowAdHocRoute'] = $_POST['AllowAdHocRoute'];
$docDataAry['DocTags'] = $_POST['DocTags'];
$docDataAry['RecordStatus'] = $_POST['RecordStatus'];
$docDataAry['pastRecordStatus'] = $_POST['pastRecordStatus'];
$isNewRecord = $_POST['tmpRecordStatus'] == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp']; 
$isDraft = $docDataAry['RecordStatus'] == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft'];

if(!$isNewRecord && $docDataAry['RecordStatus'] != $docDataAry['pastRecordStatus']){
    if($docDataAry['pastRecordStatus'] == $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']){
        $isNewRecord = $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Temp'];
    }
}

if($docDataAry['DocumentID'] != ''){
	$docRecord = $ldocrouting->getEntryData(array($docDataAry['DocumentID']));
	$docDataAry['Title'] = $docRecord[0]['Title'];
	$docDataAry['Instruction'] = $instruction = $docRecord[0]['Instruction'];
	
	$tagNames = $ldocrouting->getEntryTagName($docDataAry['DocumentID']);
	if(count($tagNames) > 0){
		$docDataAry['DocTags'] = implode(", ",$tagNames);
	}
}

$documentID = $ldocrouting->updateDREntry($docDataAry);

if($documentID != '' && $documentID > 0){
	
	$result['route'] = $ldocrouting->addDRRoute($_POST, $_FILES);
	$tagIDs = $ldocrouting->getEntryTag(array($documentID));
	if(count($tagIDs)>0){
		$tagIdAry = Get_Array_By_Key($tagIDs,'TagID');
		$ldocrouting->deleteEntryTagDbRecord($documentID, $tagIdAry);
	}
	
	if(trim($docDataAry['DocTags']) != ""){
		$result['tag'] = $ldocrouting->saveEntryTag($docDataAry['DocumentID'],addslashes($docDataAry['DocTags']));
	}

	// send notification mail to first route users
	if($isNewRecord && $_POST['RecordStatus']==$docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Active']){
		if ($ldocrouting->returnSettingValueByName('emailNotification_newRouteReceived')) {
			$tmpRouteRecords = $ldocrouting->getRouteData($documentID);
			//if(isset($_POST['RouteID_1'])){
			if($tmpRouteRecords[0]['RouteID']){
				//$firstRouteID = $_POST['RouteID_1'];
				$firstRouteID = $tmpRouteRecords[0]['RouteID'];
				$firstRouteUsers = $ldocrouting->stripWordFromArray($_POST['users_1'],"U");
				$result['Email'] = $ldocrouting->sendNotificationEmailForNewRouteReceived($firstRouteID,$firstRouteUsers);
			}
		}
	}
	
}else{
	$result[] = false;
}

$ldocrouting->cleanTempDRRecords();

if(!in_array(false,$result)){
	$msg = 'UpdateSuccess';
}else{
	$msg = 'UpdateUnsuccess';
}
if($isDraft){
	$location = "draft_routings";
}else{
	$location = "index";
}

if ($backToStep1) {
	$param = $ldocrouting->getEncryptedParameter('management','edit_doc',array('DocumentID'=>$docDataAry['DocumentID'], 'returnMsgKey'=>$msg));
}
else {
	$param = $ldocrouting->getEncryptedParameter('management',$location,array('returnMsgKey'=>$msg));
}

//debug_r($_POST);
header("Location: index.php?pe=".$param);
?>