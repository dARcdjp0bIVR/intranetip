<?php
// editing by 
/*
 * 2016-02-19 (Carlos): Added checking of pageType to determine which routing type is because added [Followed Routings] page.  
 */
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];
$ldamu = new libdigitalarchive_moduleupload();
$indexVar['libdigitalarchive_moduleupload'] = $ldamu;

if(isset($indexVar['paramAry']['documentId'])){
	$DocumentID = $indexVar['paramAry']['documentId'];
}else{
	$DocumentID = $_REQUEST['documentId'];
}

$indexVar['entryData'] = $ldocrouting->getEntryData($DocumentID);

switch($indexVar['entryData'][0]['RecordStatus'])
{
	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']:
		$PagePath = 'completed_routings';
		$PageCode = "Mgmt_CompletedRoutings";
		$PageWord = $Lang['DocRouting']['CompletedRoutings'];
	break;
	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Deleted']:
		$PagePath = 'deleted_routings';
		$PageCode = "Mgmt_DeletedRoutings";
		$PageWord = $Lang['DocRouting']['DeletedRoutings'];
	break;
	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']:
		$PagePath = 'draft_routings';
		$PageCode = "Mgmt_DraftRoutings";
		$PageWord = $Lang['DocRouting']['DraftRoutings'];
	break;
	default :
		if(isset($indexVar['paramAry']['pageType']) && $indexVar['paramAry']['pageType'] == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
			$PagePath = 'followed_routings';
			$PageCode = "Mgmt_FollowedRoutings";
			$PageWord = $Lang['DocRouting']['FollowedRoutings'];
		}else{
			$PagePath = 'index';
			$PageCode = "Mgmt_CurrentRoutings";
			$PageWord = $Lang['DocRouting']['CurrentRoutings'];
		}
}

# Page Title
$TAGS_OBJ[] = array($PageWord);
$returnMsgKey = $indexVar['paramAry']['returnMsgKey'];
$indexVar['libDocRouting_ui']->echoModuleLayoutStart($PageCode,$Lang['General']['ReturnMessage'][$returnMsgKey]);

$libReplySlipMgr = new libReplySlipMgr();
$libTableReplySlipMgr = new libTableReplySlipMgr();
$indexVar['libReplySlipMgr'] = $libReplySlipMgr;
$indexVar['libTableReplySlipMgr'] = $libTableReplySlipMgr;

$htmlAry['ajaxGetPresetContentPath'] = $ldocrouting->getEncryptedParameter('management', 'ajax_get_preset_content');

include_once($indexVar['templateScript']);

$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>