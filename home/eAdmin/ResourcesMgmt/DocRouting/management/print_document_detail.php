<?php
// Editing by 
ini_set("memory_limit", "300M");

include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
//echo $indexVar['libDocRouting_ui']->Include_Cookies_JS_CSS();

$ldamu = new libdigitalarchive_moduleupload();
$indexVar['libdigitalarchive_moduleupload'] = $ldamu;
$libReplySlipMgr = new libReplySlipMgr();
$libTableReplySlipMgr = new libTableReplySlipMgr();
$indexVar['libReplySlipMgr'] = $libReplySlipMgr;
$indexVar['libTableReplySlipMgr'] = $libTableReplySlipMgr;

$DocumentID = $indexVar['paramAry']['DocumentID'];
$IsArchive = $indexVar['paramAry']['IsArchive'] == 1;
$SelfFeedback = $indexVar['paramAry']['SelfFeedback'] == 1;
$FilterRouteId = $indexVar['paramAry']['FilterRouteId'];

$htmlAry['PrintPage'] = $indexVar['libDocRouting_ui']->getDocumentDetailPrintPage($DocumentID,$IsArchive,$SelfFeedback,$FilterRouteId);

if($indexVar['paramAry']['HtmlContentOnly'] == 1){
	echo $htmlAry['PrintPage'];	
}else{
	include_once($indexVar['templateScript']);
}
//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>