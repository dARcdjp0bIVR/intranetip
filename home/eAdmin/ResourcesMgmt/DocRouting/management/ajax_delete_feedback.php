<?php
/*
 * 2017-05-16 (Carlos): $sys_custom['DocRouting']['DeleteFeedback'] created for deleting feedback
 */
$ldocrouting = $indexVar['libDocRouting'];

$success = false;
$FeedbackID = $_REQUEST['FeedbackID'];
if($sys_custom['DocRouting']['DeleteFeedback'] && is_numeric($FeedbackID) && $FeedbackID > 0)
{
	$success = $ldocrouting->deleteFeedback($FeedbackID);
}

echo !$success?$Lang['General']['ReturnMessage']['DeleteSuccess']:$Lang['General']['ReturnMessage']['DeleteUnsuccess'];
?>