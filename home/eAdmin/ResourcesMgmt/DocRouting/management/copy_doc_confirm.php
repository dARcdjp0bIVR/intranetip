<?php
// Editing by 
/*
 * 2016-02-12 (Carlos): add Followed Routings.
 */
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/DocRouting/libDocRouting_preset.php");

# Page Title
$pageType = ($indexVar['paramAry']['pageType']=='')? $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings'] : $indexVar['paramAry']['pageType'];
if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CurrentRountings']) {
	$TAGS_OBJ[] = array($Lang['DocRouting']['CurrentRoutings']);
	$pageCode = 'Mgmt_CurrentRoutings';
}
else if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']) {
	$TAGS_OBJ[] = array($Lang['DocRouting']['DraftRoutings']);
	$pageCode = 'Mgmt_DraftRoutings';
}
else if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['CompletedRountings']) {
	$TAGS_OBJ[] = array($Lang['DocRouting']['CompletedRoutings']);
	$pageCode = 'Mgmt_CompletedRoutings';
}else if($pageType == $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['FollowedRountings']){
	$TAGS_OBJ[] = array($Lang['DocRouting']['FollowedRoutings']);
	$pageCode = 'Mgmt_FollowedRoutings';
}
$returnMsgKey = $indexVar['paramAry']['returnMsgKey'];
$indexVar['libDocRouting_ui']->echoModuleLayoutStart($pageCode, $Lang['General']['ReturnMessage'][$returnMsgKey]);


$lfilesystem = new libfilesystem();
$indexVar['libfilesystem'] = $lfilesystem;
$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];

$dataAry = array();
$CopyDoc = $_POST['CopyDoc'];
$CopyRoute = $_POST['CopyRoute'];

if(isset($indexVar['paramAry']['DocumentID'])){
	$dataAry['DocumentID'] = $indexVar['paramAry']['DocumentID'];
}else{
	$dataAry['DocumentID'] = $_REQUEST['DocumentID'];
}

include_once($indexVar['templateScript']);

$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>