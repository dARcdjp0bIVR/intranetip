<?php
// Editing by 
include_once($PATH_WRT_ROOT."includes/libdigitalarchive_moduleupload.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

$ldocrouting = $indexVar['libDocRouting'];
$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldamu = new libdigitalarchive_moduleupload();
$indexVar['libdigitalarchive_moduleupload'] = $ldamu;
$tok = $indexVar['tok'];

$record = $ldocrouting->getSignEmailDocumentByToken($tok);
$indexVar['signDocumentData'] = $record[0];
//debug_r($indexVar['signDocumentData']);
$signRouteAry = $ldocrouting->getSignEmailRoute($indexVar['signDocumentData']['RecordID']);
//debug_r($signRouteAry);
$signRouteIdToRecord = array();
for($i=0;$i<count($signRouteAry);$i++){
	$signRouteIdToRecord[$signRouteAry[$i]['RouteID']] = $signRouteAry[$i];
}
$indexVar['signRouteIdToRecord'] = $signRouteIdToRecord;
//debug_r($indexVar['signDocumentData']);
//debug_r($indexVar['signRouteIdToRecord']);

//$indexVar['paramAry']['documentId'] = $record[0]['DocumentID'];

$DocumentID = $record[0]['DocumentID'];

$indexVar['signCountData'] = $ldocrouting->getRoutesSignCountByToken($tok);
//debug_r($signCountData);

$indexVar['entryData'] = $ldocrouting->getEntryData($DocumentID);

switch($indexVar['entryData'][0]['RecordStatus'])
{
	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Complete']:
		$PagePath = 'completed_routings';
		$PageCode = "Mgmt_CompletedRoutings";
		$PageWord = $Lang['DocRouting']['CompletedRoutings'];
	break;
	case $docRoutingConfig['INTRANET_DR_ENTRY']['RecordStatus']['Draft']:
		$PagePath = 'draft_routings';
		$PageCode = "Mgmt_DraftRoutings";
		$PageWord = $Lang['DocRouting']['DraftRoutings'];
	break;
	default :
		$PagePath = 'index';
		$PageCode = "Mgmt_CurrentRoutings";
		$PageWord = $Lang['DocRouting']['CurrentRoutings'];
}

# Page Title
//$TAGS_OBJ[] = array($PageWord);
//$returnMsgKey = $indexVar['paramAry']['returnMsgKey'];
//$indexVar['libDocRouting_ui']->echoModuleLayoutStart($PageCode,$Lang['General']['ReturnMessage'][$returnMsgKey]);

$libReplySlipMgr = new libReplySlipMgr();
$libTableReplySlipMgr = new libTableReplySlipMgr();
$indexVar['libReplySlipMgr'] = $libReplySlipMgr;
$indexVar['libTableReplySlipMgr'] = $libTableReplySlipMgr;

//$htmlAry['ajaxGetPresetContentPath'] = $ldocrouting->getEncryptedParameter('management', 'ajax_get_preset_content');

include_once($indexVar['templateScript']);

//$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>