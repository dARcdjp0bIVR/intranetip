<?php
// using: 
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

$replySlipFileField = $_GET['ReplySlipFileField'];
$routingNumber = $_GET['routingNumber'];
$replySlipType = $_GET['ReplySlipType'];

if($replySlipType == $docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']) {
	$libReplySlipMgr = new libTableReplySlipMgr();
} else {
	$libReplySlipMgr = new libReplySlipMgr();
}
$libReplySlipMgr->setCsvFilePath($_FILES[$replySlipFileField]['tmp_name']);
$isValid = $libReplySlipMgr->isCsvFileValid();
?>
<script language="javascript">
<? if ($isValid) { ?>
window.parent.hideReplySlipCsvInvalidWarning('<?=$routingNumber?>');
<? } else { ?>
window.parent.showReplySlipCsvInvalidWarning('<?=$routingNumber?>');
<? } ?>
</script>