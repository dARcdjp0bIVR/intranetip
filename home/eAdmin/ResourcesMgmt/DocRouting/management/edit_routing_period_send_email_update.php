<?php
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");
include_once($PATH_WRT_ROOT."includes/tableReplySlip/libTableReplySlipMgr.php");

$libReplySlipMgr = new libReplySlipMgr();
$libTableReplySlipMgr = new libTableReplySlipMgr();
$indexVar['libReplySlipMgr'] = $libReplySlipMgr;
$indexVar['libTableReplySlipMgr'] = $libTableReplySlipMgr;
$ldocrouting = $indexVar['libDocRouting'];

$routeID = $_POST['RouteID'];
$SendEmail = $_POST['SendEmail'];
$EmailTarget = $_POST['EmailTarget'];
$EmailContent = $_POST['EmailContent'];
$i = 1;
$effectiveType = $_POST['EffectiveType_'.$i]; // EffectiveType
$startDate = $_POST['EffectiveStartDate_'.$i]; // EffectiveStartDate;
$startTime = $_POST['EffectiveStartTime_'.$i.'_hour'].':'.$_POST['EffectiveStartTime_'.$i.'_min'].':'.$_POST['EffectiveStartTime_'.$i.'_sec'];
$startDatetime = $startDate." ".$startTime;
$endDate = $_POST['EffectiveEndDate_'.$i]; // EffectiveEndDate
$endTime = $_POST['EffectiveEndTime_'.$i.'_hour'].':'.$_POST['EffectiveEndTime_'.$i.'_min'].':'.$_POST['EffectiveEndTime_'.$i.'_sec'];
$endDatetime = $endDate." ".$endTime;

$success = array();
				
if($routeID!=''){
	$sql = "UPDATE INTRANET_DR_ROUTES SET EffectiveType='$effectiveType',
				EffectiveStartDate='$startDatetime',EffectiveEndDate='$endDatetime',DateModified=NOW(),ModifiedBy='".$indexVar['drUserId']."' 
			WHERE RouteID='".$routeID."' ";
	$success[] = $ldocrouting->db_db_query($sql);
}

if($SendEmail == 'Y'){
	
	$targetUser = $ldocrouting->getRouteTargetUsers($routeID);
	$selectedUserID = array();
	foreach($targetUser as $aTargetUser){
		if(in_array($aTargetUser['RecordStatus'],$EmailTarget)){
			$selectedUserID[] = $aTargetUser['UserID'];
		}
		
	}
	$success[] = $ldocrouting->sendNotificationEmailForNewRouteReceived($routeID, $selectedUserID, $EmailContent);
}

if(in_array(false,$success)){
	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}else{
	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'].($SendEmail == 'Y'?' '.$Lang['DocRouting']['ReturnMessage']['EmailSentSuccess']:'');
}

//$success = $ldocrouting->addDRRoute($_POST, $_FILES, true);
//
//if($success){
//	$msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
//	
//	if ($ldocrouting->returnSettingValueByName('emailNotification_newRouteReceived')) {
//		$display_order = $_POST['DisplayOrder'];
//		if(isset($_POST['RouteID_'.$display_order])){
//			$firstRouteID = $_POST['RouteID_'.$display_order];
//			$firstRouteUsers = $ldocrouting->stripWordFromArray($_POST['users_'.$display_order],"U");
//			$result['Email'] = $ldocrouting->sendNotificationEmailForNewRouteReceived($firstRouteID,$firstRouteUsers);
//		}
//	}
//	
//}else{
//	$msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
//}

echo '<script>';
echo 'if(window.parent.js_Finish_Edit_Routing_Period_Send_Email){
		window.parent.js_Finish_Edit_Routing_Period_Send_Email("'.$msg.'");
		}';
echo '</script>';

?>