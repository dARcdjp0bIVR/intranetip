<?php
// using : 
/*************************************
 * Date: 2013-01-17 (Rita)
 * Details: create this page for copy routing
 * 
 ****************************************/
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Mgmt_CurrentRoutings', $Lang['General']['ReturnMessage'][$returnMsgKey],  $forPopup=true);

//$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT.'includes/libdbobject.php');
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlip.php");

$TAGS_OBJ[] = array($Lang['DocRouting']['CurrentRoutings']);

# Get Value
$documentID = $_GET['documentID'];
$routingNumber = $_GET['routingNumber'];
$newRouteType = $_GET['newRouteType'];



$htmlAry['copyRouteDisplayTable'] = '';
$htmlAry['copyRouteDisplayTable'] = $indexVar['libDocRouting_ui']->getCopyRouteDisplay($documentID, $routingNumber, $newRouteType);



include_once($indexVar['templateScript']);
//$linterface->LAYOUT_STOP();
$indexVar['libDocRouting_ui']->echoModuleLayoutStop();

?>