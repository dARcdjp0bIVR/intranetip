<?php
// using : 

/******************************
 * Change Log:
 *  
 * 2013-10-03 (Henry): add advance search button
 ******************************/

# Page Title
$TAGS_OBJ[] = array($Lang['DocRouting']['DraftRoutings']);


$clearCoo = $indexVar['paramAry']['clearCoo'];

//#Set Cookies
$arrCookies = array();
//$arrCookies[] = array("ck_doc_management_draft_rountings_star_staus", "starStatus");
//$arrCookies[] = array("ck_doc_management_draft_rountings_create_status", "createStatus");
//$arrCookies[] = array("ck_doc_management_draft_rountings_follow_status", "followStatus");
//
$arrCookies[] = array("ck_doc_management_current_routings_keyword", 'keyword');
$arrCookies[] = array("ck_doc_management_current_routings_routingTitle", 'routingTitle');
$arrCookies[] = array("ck_doc_management_current_routings_byDateRange", 'byDateRange');
$arrCookies[] = array("ck_doc_management_current_routings_StartDate", 'StartDate');
$arrCookies[] = array("ck_doc_management_current_routings_EndDate", 'EndDate');
$arrCookies[] = array("ck_doc_management_current_routings_byDateRange2", 'byDateRange2');
$arrCookies[] = array("ck_doc_management_current_routings_StartDate2", 'StartDate2');
$arrCookies[] = array("ck_doc_management_current_routings_EndDate2", 'EndDate2');
$arrCookies[] = array("ck_doc_management_current_routings_createdBy", 'createdBy');
$arrCookies[] = array("ck_doc_management_current_routings_tag", 'tag');
$arrCookies[] = array("ck_doc_management_current_routings_academic_year", 'academic_year');
$arrCookies[] = array("ck_doc_management_current_routings_flag", 'flag');

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_doc_management_current_rountings_star_staus = '';
	$ck_doc_management_current_rountings_create_status = '';
	$ck_doc_management_current_rountings_follow_status = '';
	$ck_doc_management_current_routings_keyword = '';
	$ck_doc_management_current_routings_routingTitle = '';
	$ck_doc_management_current_routings_byDateRange = '';
	$ck_doc_management_current_routings_StartDate = '';
	$ck_doc_management_current_routings_EndDate = '';
	$ck_doc_management_current_routings_byDateRange2 = '';
	$ck_doc_management_current_routings_StartDate2 = '';
	$ck_doc_management_current_routings_EndDate2 = '';
	$ck_doc_management_current_routings_createdBy = '';
	$ck_doc_management_current_routings_tag = '';
	$ck_doc_management_current_routings_academic_year = '';
	$ck_doc_management_current_routings_flag = '';
}
else{
	updateGetCookies($arrCookies);
}

# Layout Start
$indexVar['libDocRouting_ui']->echoModuleLayoutStart('Mgmt_DraftRoutings');
echo $indexVar['libDocRouting_ui']->Include_Cookies_JS_CSS();
$libuser = new libuser();

# Add New Routing Link
$newRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'edit_doc',  array('pageType' => $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']));

# Instruction Message
//$instructionMessage =  $indexVar['libDocRouting_ui'] -> Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'], $others="");	  

# Search Box
$searchBox = $indexVar['libDocRouting_ui'] -> getAdvanceSearchDiv();
$searchBox .= $indexVar['libDocRouting_ui'] -> Get_Search_Box_Div('keyword', stripslashes(stripslashes($keyword)), 'onkeydown="checkGoSearch(event);"');
$DisplayUpdateLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'draft_routings', '');

$htmlAry['CurrentDisplay'] = $indexVar['libDocRouting_ui'] ->getRoutingDisplayTable($newRoutingLink,$DisplayUpdateLink,$searchBox,$instructionMessage, true, $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']);

//$htmlAry['CurrentDisplay'] = $indexVar['libDocRouting_ui'] ->echoRoutingDisplayTable($newRoutingLink,$DisplayUpdateLink,$searchBox,$instructionMessage, true, $docRoutingConfig['INTRANET_DR_ROUTE_Page']['Management']['DraftRountings']);

 
 
 # Routing Link For JS
$reloadRoutingLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'ajax_reload_routing', '');
$updateStarStatusLink = $indexVar['libDocRouting']->getEncryptedParameter('management', 'ajax_update_star_status', '');
   


include_once($indexVar['templateScript']);

$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>