<?php 
//editing anna
/*
 * 2017-07-11 Anna
 * - created this page 
 */



?>
<script language="javascript">
function goSubmit(pe) {
	var input_ip = document.getElementById('AllowAccessIP').value.Trim();
	var is_valid = true;
	
	if(input_ip != '') {
		var ip_ary = input_ip.split('\n');
		for(var i=0;i<ip_ary.length;i++) {
			if(!ip_ary[i].match(/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+|\[\d+-\d+\])(\/[0-9]+)*/)){
				is_valid = false;
				break;
			}
		}
	}
	if(!is_valid) {
		alert('<?=$Lang['Security']['WarnMsgArr']['InvalidIPAddress']?>');
		return false;
	}
	
	$('#form1').attr('action', '?pe=' + pe).attr('method', 'post').submit();
}

</script>

<br />
<form id="form1" name="form1">
	<?=$htmlAry['contentTable']?>
	<br />
	
	<div class="edit_bottom">
		<p class="spacer"></p>
		<?=$htmlAry['submitButton']?>
		<?=$htmlAry['resetButton']?>
		<p class="spacer"></p>
	</div>
</form>
<br />