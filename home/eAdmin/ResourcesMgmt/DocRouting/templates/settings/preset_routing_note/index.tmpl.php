<?php
// using : 
/*
 * Change Log:
 * 	
 */
# Add New Routing Link
$deletePresetRecordsLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/preset_routing_note', 'delete', '');
$editPresetRecordsLink = $indexVar['libDocRouting']->getEncryptedParameter('settings/preset_routing_note', 'new', '');


?>
<script language="javascript">


//function js_Select_All_Checkboxes(){
//	if($('#preDocInstructionMasterChk').is(':checked') == true){		
//		$('.preDocInstruction').is(':checked');	
//	}
//}


function js_Go_Update_Preset_Record(PresetID){
	$('#PresetID').val(PresetID);	
	$('#form1').attr('action', '?pe="<?=$editPresetRecordsLink?>"').submit();
}


function js_Edit_Preset_Record(){
	var PresetID ='';
	
	if($('.PresetID:checked').length == 0){
		alert(globalAlertMsg2);
	}
	else if($('.PresetID:checked').length >1){
		alert('<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAllowBooking'] ?>');
	}
	else if($('.PresetID').is(':checked')){
		PresetID = $('.PresetID:checked').val();
	}
		
	if(PresetID){
		$('#PresetID').val(PresetID);	
		$('#form1').attr('action', '?pe="<?=$editPresetRecordsLink?>"').submit();		            
	}

}


function js_Check_Remove(){
	var PresetID ='';
	
	if($('.PresetID:checked').length == 0){
		alert(globalAlertMsg2);
	}
	else if(confirm("<?=$Lang['RepairSystem']['DeleteRequestSummaryConfirm'];?>")){	      
		$('#form1').attr('action', '?pe="<?=$deletePresetRecordsLink?>"').submit();
	}
}


</script>

<?=$htmlAry['PresetNoteDisplay']?>
