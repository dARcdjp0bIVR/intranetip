<?php
// using : 
/*
 * Change Log:
 * 
 *  2014-12-02 [Carlos]: Modified applySelectedPresetNoteContent() make a dummy call Editor1.InsertHtml('') to cater webkit browser(e.g. Chrome) first time no focus problem
 *	2013-09-30 [Henry]: Modified applySelectedPresetNoteContent to append the template in the fckEditor
 * 
 * 	2012-10-05 [Ivan]: created this file
 */

$htmlAry['ajaxGetPresetContentPath'] = $indexVar['libDocRouting']->getEncryptedParameter('management', 'ajax_get_preset_content');

?>
<script language="javascript">
var docRoutingObj = {
	_urlPathParamSeparator: '<?=$docRoutingConfig['urlPathParamSeparator']?>',
	_urlVariableSeparator: '<?=$docRoutingConfig['urlVariableSeparator']?>',
	_urlVariableKeyValueSeparator: '<?=$docRoutingConfig['urlVariableKeyValueSeparator']?>',
	
	
	getParameterText: function(filePath, fileName, variableObj){
		variableObj = variableObj || new Object();
	
		var variableTextAry = new Array();
		$.each(variableObj, function(_variableKey, _variableValue) {
			if (typeof _variableValue == 'object') {
				variableTextAry[variableTextAry.length] = docRoutingObj.getArrayParameterTextRecursion(_variableValue, '', _variableKey, true);
			}
			else {
				variableTextAry[variableTextAry.length] = _variableKey + docRoutingObj._urlVariableKeyValueSeparator + _variableValue;
			}
		});
		var variableText = variableTextAry.join(docRoutingObj._urlVariableSeparator);
		
		return filePath + docRoutingObj._urlPathParamSeparator + fileName + docRoutingObj._urlPathParamSeparator + variableText;
	},
	
	getArrayParameterTextRecursion: function(variableObj, prevText, parentVariableName, addParentName) {
		var tempText = '';
		
		if (typeof variableObj == 'object') {
			$.each(variableObj, function(_variableKey, _variableValue) {
				if (addParentName) {
					prevText += parentVariableName;
					addParentName = false;
				}
				tempText += docRoutingObj.getArrayParameterTextRecursion(_variableValue, prevText + '[' + _variableKey + ']', parentVariableName, false);
			});
		}
		else {
			tempText = prevText + docRoutingObj._urlVariableKeyValueSeparator + variableObj + docRoutingObj._urlVariableSeparator;
		}
		
		return tempText;
	},
	
	goToPage: function(pe) {
		window.location = '?pe=' + pe;
	},
	
	applySelectedPresetNoteContent: function(routeNum) {
		var OptionValue = $("#presetNotesSelection_" + routeNum).val();
		var Editor1 = FCKeditorAPI.GetInstance('Note_' + routeNum);
		Editor1.InsertHtml('');
		if (OptionValue != ''){
			$.post(
				"index.php", 
				{ 
					pe: '<?=$htmlAry['ajaxGetPresetContentPath']?>',
					PresetID: OptionValue
				},
				function(ReturnData) {
					//Editor1.SetHTML(ReturnData);
					Editor1.InsertHtml(ReturnData+"<br/>");
				}
			);
		}
	}
};
</script>