<?php

//using: 

/*****************************************
 * 2013-10-04 (Henry): modify copyback() to print the list of user if the user selection box is hidden
 ****************************************/


# Get Route Data
$routeDataArr = $indexVar['libDocRouting']->getRouteData($documentID);
$routeDataIDAssocArr = BuildMultiKeyAssoc($routeDataArr, 'RouteID');


?>

<script language="Javascript">
function copyback() {
	
	var SelectedRouteID = '';
	var SelectedRouteNum = '';
	var RouteNumber = $('#RouteNumber').val();
	var newRouteType = $('#newRouteType').val();
	
	// Check Which Option Is Selected  
	$('.selectedRouting').each(function(){
		if($(this).is(':checked')){
			SelectedRouteID = $(this).val();
			SelectedRouteNum = $(this).attr('id');
		}
	});
	
	if(SelectedRouteNum==''){
		alert(globalAlertMsg2);
	}
	else
	{
		
		if(newRouteType!=1){
			// Instruction Value 
			//  var CopyFromNote = window.opener.$("#Note_"+SelectedRouteNum).val();	
			var inst = opener.FCKeditorAPI.GetInstance("Note_"+SelectedRouteNum);
	  		var CopyFromNote = inst.GetHTML();
		  
		}else{
			// Instruction Value 
			
		    var CopyFromNote = $('#Note_'+SelectedRouteID).val();	
		}	
			
		// Instruction
		var oEditor = window.opener.FCKeditorAPI.GetInstance("Note_"+RouteNumber) ;
		oEditor.SetHTML(CopyFromNote) ;
		
		
		// Reply Slip //
		//Upload file
		var SelectedFileValue = window.opener.$('#ReplySlipFile_' + SelectedRouteNum).val();
		if(SelectedFileValue!=''){
			window.opener.$('#ReplySlipFile_' + RouteNumber).val(SelectedFileValue);
		}
				
		// Preview CurrentReplySlip Btn
		var newRelpySlipBtn = window.opener.$('#PreviewCurrentReplySlipBtn_' + SelectedRouteNum).clone();
		newRelpySlipBtn.attr('id', 'PreviewCurrentReplySlipBtn_' + RouteNumber);
		window.opener.$('#replySlipCurrentBtnLayer_' + RouteNumber).html(newRelpySlipBtn);
		
		// Hidden Value field
		//var newReplySlipHidden = '<input type="hidden" id="CopyReplySlip[' + RouteNumber + '][' + SelectedRouteID + ']"></input>';
		
		var newReplySlipHidden = '<input type="hidden" id="CopyReplySlip_' + RouteNumber +'" name="CopyReplySlip_' + RouteNumber +'"  value="'+ SelectedRouteID +'"></input>';
		window.opener.$('#replySlipCurrentHiddenFieldLayer_' + RouteNumber).html(newReplySlipHidden);
		
		if(newRouteType!=1){

			// Loop ALL Tag And Copy Value From Selected Route
			window.opener.$('.CopyClass_'+RouteNumber).each( function() {	
				var _copyToObjId = $(this).attr('id');
				var _copyFromObjId = _copyToObjId.replace(RouteNumber, SelectedRouteNum);		 	
			 	var _tagName = $(this).get(0).tagName;
				 	
			 	switch (_tagName) {	 		
					case 'INPUT':
					var objectType = window.opener.$('#' + _copyToObjId).attr('type').toUpperCase();
						switch (objectType) {
							case 'TEXT':
								 window.opener.$('#' + _copyToObjId).val( window.opener.$('#' + _copyFromObjId).val());
								break;
							case 'RADIO':
							case 'CHECKBOX':
								 window.opener.$('#' + _copyToObjId).attr('checked',  window.opener.$('#' + _copyFromObjId).attr('checked'));
								
									// Show or Hide Layer base on Option
									var CopyFromActionAllowReplySlip = window.opener.$("#ActionAllowReplySlip_"+SelectedRouteNum).is(':checked');
									
									if(CopyFromActionAllowReplySlip==true){
										window.opener.$("#routeReplySlipSettingsDiv_"+RouteNumber).show();
										window.opener.$("#releaseResultSettingsDiv_"+RouteNumber).show();
										window.opener.$("#showUserNameDisplaySettingsDiv_"+RouteNumber).show();
										
									}else{
										window.opener.$("#routeReplySlipSettingsDiv_"+RouteNumber).hide();
										window.opener.$("#releaseResultSettingsDiv_"+RouteNumber).hide();
										window.opener.$("#showUserNameDisplaySettingsDiv_"+RouteNumber).hide();
									}
									
									var CopyFromEffectiveTypePeriod =  window.opener.$("#EffectiveTypePeriod_"+SelectedRouteNum).is(':checked'); 
		
									if(CopyFromEffectiveTypePeriod==true){
										window.opener.$('#VisiblePeriod_' + RouteNumber).show();	
									}else{
										window.opener.$('#VisiblePeriod_' + RouteNumber).hide();
									}	
									
									var CopyFromViewRightViewByUser = window.opener.$("#ViewRightViewByUser_"+SelectedRouteNum).is(':checked'); 
									if(CopyFromViewRightViewByUser==true){
										window.opener.$('#VisibleAccessUsers_' + RouteNumber).show();	 
									}else{
										window.opener.$('#VisibleAccessUsers_' + RouteNumber).hide();	 
									}
										
								break;
						}			
					break;
												
					case 'TEXTAREA':
						 window.opener.$('#' + _copyToObjId).val( window.opener.$('#' + _copyFromObjId).val());
					break;
					
					case 'SELECT':		
					
					if($(this).attr('multiple')) {
						var CopyFromMultiSelectionBox = window.opener.$('#' + _copyFromObjId).html();
						window.opener.$('#' + _copyToObjId).html(CopyFromMultiSelectionBox);
				
					}else{		
						var CopyFromSingleSelection = window.opener.$('#' + _copyFromObjId).val();
						$(this).val(CopyFromSingleSelection);
					}	
									
					break;
			 	}
			});

		}else{
			// User ID List Value 
		    var TargetUserIDList = $('#TargetUserIDList_'+SelectedRouteID).val();
		    var	TargetUserListArr = TargetUserIDList.split(',');
					   
		    // User Name List Value 
		    var TargetUserNameList = $('#TargetUserNameList_'+SelectedRouteID).val();
		    var	TargetUserNameListArr = TargetUserNameList.split(',');
		   		  
		    // Instruction Value 
		     var CopyFromNote = $('#Note_'+SelectedRouteID).val();
		   
		   	// Action Value 
		   	var Actions =  $('#Actions_'+SelectedRouteID).val();  
		 	ActionArry = Actions.split(',');
		
		 	// Reply Slip
		 	var CopyReplySlipType = $('#ReplySlipType_'+SelectedRouteID).val(); 
		   	var CopyFromReplySlipReleaseType = $('#ReplySlipReleaseType_'+SelectedRouteID).val();  
		   	var CopyUserNameDisplayType =  $('#UserNameDisplayType_'+SelectedRouteID).val();  
		   	var CopyReplySlipID = $('#ReplySlipID_'+SelectedRouteID).val();    
		   	
		    // Effective Value
		    var CopyFromEffectiveType = $('#EffectiveType_'+SelectedRouteID).val();
		    var CopyFromEffectiveStartDate = $('#EffectiveStartDate_'+SelectedRouteID).val();
		    var CopyFromEffectiveStartTimeHour = $('#EffectiveStartTimeHour_'+SelectedRouteID).val();
		    var CopyFromEffectiveStartTimeMin = $('#EffectiveStartTimeMinute_'+SelectedRouteID).val();
		    var CopyFromEffectiveStartTimeSec = $('#EffectiveStartTimeSecond_'+SelectedRouteID).val();
		    
		    var CopyFromEffectiveEndDate = $('#EffectiveEndDate_'+SelectedRouteID).val();
		    var CopyFromEffectiveEndTimeHour = $('#EffectiveEndTimeHour_'+SelectedRouteID).val();
		   	var CopyFromEffectiveEndTimeMin = $('#EffectiveEndTimeMinute_'+SelectedRouteID).val();
		    var CopyFromEffectiveEndTimeSec = $('#EffectiveEndTimeSecond_'+SelectedRouteID).val();
		    
		    
		    // View Value 
		    var CopyFromViewRight = $('#ViewRight_'+SelectedRouteID).val();
		     
		    // Lock     
		    var CopyFromEnableLock = $('#EnableLock_'+SelectedRouteID).val();
		    var CopyFromLockDuration = $('#LockDuration_'+SelectedRouteID).val();
			
			// ********     Assign Value to Parent Window    ************* // 
			// User 
			var newUserDisplay = window.opener.$("#users_"+RouteNumber);	
			newUserDisplay.html('');
			for(i=0;i<TargetUserListArr.length;i++){
				newUserDisplay.append ("<option value='" + TargetUserListArr [i] + "'>" + TargetUserNameListArr [i] + "</option>");
			}
			
			// Actions
			if($.inArray('<?=$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['ReplySlip']?>', ActionArry) > -1){
				window.opener.$("#ActionAllowReplySlip_"+RouteNumber).attr('checked', 'checked');
				window.opener.$("#routeReplySlipSettingsDiv_"+RouteNumber).show();
				window.opener.$("#releaseResultSettingsDiv_"+RouteNumber).show();
				window.opener.$("#showUserNameDisplaySettingsDiv_"+RouteNumber).show();
				
				// Reply Slip Type 
				if(CopyReplySlipType != <?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>){
					window.opener.$("#ReplySlipType_"+RouteNumber + "_1").attr('checked', 'checked');
				}else{
					window.opener.$("#ReplySlipType_"+RouteNumber + "_2").attr('checked', 'checked');
				}	

				
				// Reply Slip Display					
				var ReplySlipPreviewBtn = "<input type=\"button\" onmouseout=\"this.className='formsmallbutton '\" onmouseover=\"this.className='formsmallbuttonon '\" value=\"<?=$Lang['DocRouting']['PreviewCurrentReplySlip']?>\" id=\"PreviewCurrentReplySlipBtn_'" + SelectedRouteNum + "'\" name=\"PreviewCurrentReplySlipBtn_'" + SelectedRouteNum + "'\" onclick=\"javascript:js_Submit_View_ReplySlip('" + SelectedRouteNum + "', '" + CopyReplySlipID + "', '" + RouteNumber + "');\" class=\"formsmallbutton \">";
				window.opener.$("#replySlipCurrentBtnLayer_"+RouteNumber).html(ReplySlipPreviewBtn);	
				
				var newReplySlipHidden = '<input type="hidden" id="CopyReplySlip_' + RouteNumber +'" name="CopyReplySlip_' + RouteNumber +'"  value="'+ SelectedRouteID +'"></input>';
				window.opener.$('#replySlipCurrentHiddenFieldLayer_' + RouteNumber).html(newReplySlipHidden);	
				
				// Reply Slip Setting Display
				if(CopyFromReplySlipReleaseType == <?=$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseAnytime']?>){
					window.opener.$("#ReleaseAnytime_"+RouteNumber).attr('checked', 'checked');
				}else if (CopyFromReplySlipReleaseType == <?=$docRoutingConfig['INTRANET_DR_ROUTES']['ONLINEFORM']['ReleaseResultAfterSubmission']?>){
					window.opener.$("#ReleaseResultAfterSubmission_"+RouteNumber).attr('checked', 'checked');
				}else{
					window.opener.$("#ReleaseResultAfterRouteCompeleted_"+RouteNumber).attr('checked', 'checked');
				}
				
				if(CopyUserNameDisplayType == 1){			
					window.opener.$("#DisplayUserName_"+RouteNumber).attr('checked', 'checked');
				}else {
					window.opener.$("#HideUserName_"+RouteNumber).attr('checked', 'checked');
				}
			
				
			}else{
				window.opener.$("#ActionAllowReplySlip_"+RouteNumber).attr('checked', '');
				window.opener.$("#routeReplySlipSettingsDiv_"+RouteNumber).hide();
				window.opener.$("#releaseResultSettingsDiv_"+RouteNumber).hide();
				window.opener.$("#showUserNameDisplaySettingsDiv_"+RouteNumber).hide();
			} 
			if($.inArray('<?=$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Comment']?>', ActionArry) > -1){
				window.opener.$("#ActionAllowComment_"+RouteNumber).attr('checked', 'checked');		
			}else{
				window.opener.$("#ActionAllowComment_"+RouteNumber).attr('checked', '');			
			}
			if($.inArray('<?=$docRoutingConfig['INTRANET_DR_ROUTE_ACTION']['Action']['Attachment']?>', Actions) > -1){
				window.opener.$("#ActionAllowAttachment_"+RouteNumber).attr('checked', 'checked');				
			}else{
				window.opener.$("#ActionAllowAttachment_"+RouteNumber).attr('checked', '');			
			}
			
	
				
			// Effective
		    //   EffectiveTypePeriod   
			if(CopyFromEffectiveType == '<?=$docRoutingConfig['INTRANET_DR_ROUTES']['EffectiveType']['Period']?>'){
				window.opener.$("#EffectiveTypePeriod_"+RouteNumber).attr('checked', 'checked');
			
				window.opener.$('#VisiblePeriod_' + RouteNumber).show();
				
				// Date 
				window.opener.$("#EffectiveStartDate_"+RouteNumber).val(CopyFromEffectiveStartDate);
				window.opener.$("#EffectiveEndDate_"+RouteNumber).val(CopyFromEffectiveEndDate);
				
				// Time 
			
				window.opener.$("#EffectiveStartTime_"+RouteNumber+"_hour ").val(CopyFromEffectiveStartTimeHour);
				window.opener.$("#EffectiveStartTime_"+RouteNumber+"_min ").val(CopyFromEffectiveStartTimeMin);
				window.opener.$("#EffectiveStartTime_"+RouteNumber+"_sec ").val(CopyFromEffectiveStartTimeSec);
			
				window.opener.$("#EffectiveEndTime_"+RouteNumber+"_hour").val(CopyFromEffectiveEndTimeHour);
				window.opener.$("#EffectiveEndTime_"+RouteNumber+"_min").val(CopyFromEffectiveEndTimeMin);
				window.opener.$("#EffectiveEndTime_"+RouteNumber+"_sec").val(CopyFromEffectiveEndTimeSec);
				
				
			//  EffectiveTypePrevRouteFinished	
			}else{ 	
		     	window.opener.$("#EffectiveTypePrevRouteFinished_"+RouteNumber).attr('checked', 'checked');
				window.opener.$('#VisiblePeriod_' + RouteNumber).hide(); 		
			}
			
			// View by
			
			if(CopyFromViewRight == '<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['TargetUserOnly']?>'){		
				window.opener.$("#ViewRightAll_"+RouteNumber).attr('checked', 'checked');
				window.opener.$("#VisibleAccessUsers_"+RouteNumber).hide();	
			}else if(CopyFromViewRight == '<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ViewRight']['AlsoViewByUser']?>'){
				window.opener.$("#ViewRightViewByUser_"+RouteNumber).attr('checked', 'checked');
				window.opener.$("#VisibleAccessUsers_"+RouteNumber).show();
				
				// Extra User
				// User ID List Value 
			    var ExtraTargetUserIDList = $('#ExtraTargetUserIDList_'+SelectedRouteID).val();
			    var	ExtraTargetUserIDListArr = ExtraTargetUserIDList.split(',');					   
			   
			   // User Name List Value 
			    var ExtraTargetUserNameList = $('#ExtraTargetUserNameList_'+SelectedRouteID).val();
			    var	ExtraTargetUserNameListArr = ExtraTargetUserNameList.split(',');
				var newExtraUserDisplay = window.opener.$("#extraUsers_"+RouteNumber);	
				newExtraUserDisplay.html('');
				for(i=0;i<ExtraTargetUserIDListArr.length;i++){
					newExtraUserDisplay.append ("<option value='" + ExtraTargetUserIDListArr [i] + "'>" + ExtraTargetUserNameListArr [i] + "</option>");
				}
				 
			}
				
			// Lock routing when giving feedback
			if(CopyFromEnableLock =='1'){
				window.opener.$("#EnableLock_"+RouteNumber).attr('checked', 'checked');
				window.opener.$("#lockDurationDiv_"+RouteNumber).show();			
				window.opener.$("#LockDuration_"+RouteNumber).val(CopyFromLockDuration);
			}else{
				window.opener.$("#EnableLock_"+RouteNumber).attr('checked', '');
				window.opener.$("#lockDurationDiv_"+RouteNumber).hide();
			}
			
			// Lock routing when giving feedback
			if(CopyFromEnableLock =='1'){
				window.opener.$("#EnableLock_"+RouteNumber).attr('checked', 'checked');
				window.opener.$("#lockDurationDiv_"+RouteNumber).show();			
				window.opener.$("#lockDurationDiv_"+RouteNumber).val(CopyFromLockDuration);
			}else{
				window.opener.$("#EnableLock_"+RouteNumber).attr('checked', '');
				window.opener.$("#lockDurationDiv_"+RouteNumber).hide();
			}	
			
		}
		
		 window.opener.$("#ViewUserDiv"+RouteNumber+"_list").text($('#ExtraTargetUserNameList_'+SelectedRouteID).val().replace(/,/g, ', '));
		 
		// Close This Pop Up Window 
   		 self.close();
			
	}
	

}
</script>

<?=$htmlAry['copyRouteDisplayTable']?> 
