<?php
// editing by 

/***********************************************
 * 2017-05-16 (Carlos): $sys_custom['DocRouting']['RequestNumber'] added js_Delete_Feedback(routeID,feedbackID).
 * 2014-12-02 (Carlos): added js_GetSendArchivedDocumentEmailForm() and js_SendArchivedDocumentEmail()
 * 2014-11-18 (Carlos): added LoadArchiveDocumentTBForm()
 * 2014-05-13 (Carlos): added LoadArchiveTBFormForStarFile(documentId, linkToType), js_Star_File(), js_Reload_Star_File_Div()
 * 2014-02-06 (Tiffany): add FCK_Get_Length(); modified js_Submit_Comment() and js_Save_Draft() and js_Request_Lock() 
 * 
 * 2014-01-10 (Henry): added function js_update_User_List_Div() and selectAll()
 * 						modified function js_User_Option_Remove() and function js_Extra_User_Option_Remove()
 * 
 * 2013-06-17 (Carlos): added js js_onChangeShowHideReplySlipUserOption()
 * 
 * Date:	2013-02-15 (Rita)
 * Detail:	modified js_Submit_AddHoc_Routing(), js_Submit_View_ReplySlip()
 ***********************************************/

//include_once($PATH_WRT_ROOT."/home/eAdmin/ResourcesMgmt/DocRouting/management/dr_detail_tmp_functions.php");
/*
$ldocrouting_ui = $indexVar['libDocRouting_ui'];
$ldocrouting = $indexVar['libDocRouting'];

if(isset($indexVar['paramAry']['documentId'])){
	$DocumentID = $indexVar['paramAry']['documentId'];
}else{
	$DocumentID = $_REQUEST['documentId'];
}
*/
//debug_r($ldocrouting->getDocRoutingUserStatusCount($DocumentID));

?>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<?=$indexVar['libDocRouting_ui']->Include_SuperTable_JS_CSS()?>

<script type="text/javascript" language="JavaScript">


$(document).ready( function () {
	
});

function js_Reload_Routing_Detail_Block(routeID)
{
	Block_Element('DivRoute_'+routeID);
	$('#DivRoute_'+routeID).load(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_routing_detail',array())?>',
		{
			'RouteID':routeID
		},
		function(data){
			UnBlock_Element('DivRoute_'+routeID);
		}
	);
}

function js_Get_Upload_Form(routeID,feedbackID)
{
	tb_show("<?=$Lang['DocRouting']['UploadAttachment']?>","#TB_inline?height=300&width=450&inlineId=FakeLayer");
	
	var editorName = 'Comment_'+routeID;
	var comment = "";
	if(typeof(FCKeditorAPI) == "object"){
		comment = getEditorValue(editorName);
	}
	
	
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_upload_attachment_form',array())?>',
		{
			'DocumentID':'<?=$DocumentID?>',
			'RouteID':routeID,
			'FeedbackID':feedbackID,
			'Comment':comment
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
		});
}



function js_Show_Hide_Member_Status(obj,routeID,selectedType)
{
	var thisObj = $(obj);
	var dv = $('#member_status_list_'+routeID+'_'+selectedType);
	var isVisible = dv.is(':visible');
	$('#MemberListBtn_'+routeID+' li').removeClass();
	
	$('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['Completed']?>').hide();
	$('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']?>').hide();
	$('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']?>').hide();
	if(isVisible){
		dv.hide();
	}else{
		thisObj.closest('li').removeClass().addClass('selected');
		dv.show();
	}
}

function js_Show_Hide_Comment(routeID, isFocus)
{
	isFocus = isFocus || false;
	
	var dv = $('#write_comment_'+routeID);
	var draft_li = $('#LiRouteDraft_'+routeID); 	   
    if(dv.is(':visible')){
		dv.hide();
		if(draft_li.length > 0){
			draft_li.show();
		}
	}else{
		dv.show();
		
		if (isFocus) {
			var oEditor = FCKeditorAPI.GetInstance('Comment_'+routeID);  
			oEditor.Focus();  
		}
		
		if(draft_li.length > 0){
			draft_li.hide();
		}
	}
}

  
function js_Check_All_Users(routeID,checked)
{
	var inProgressUsers = document.getElementsByName('InprogressUsers_'+routeID+'[]');
	var notViewedUsers = document.getElementsByName('NotViewedUsers_'+routeID+'[]');
	
	for(var i=0;i<inProgressUsers.length;i++){
		inProgressUsers[i].checked = checked;
	}
	for(var i=0;i<notViewedUsers.length;i++){
		notViewedUsers[i].checked = checked;
	}
}

//function js_Submit_Comment(documentID,routeID)
//{
	//var comment = $.trim($('#Comment_'+routeID).val());
//	if(comment == ''){
//		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInComment']?>');
//	}else{
//		Block_Element('DivRoute_'+routeID);
//		$.post(
//			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_update_comment',array())?>',
//			{
//				'DocumentID':documentID,
//				'RouteID':routeID,
//				'Comment':encodeURIComponent(comment)
//			},
//			function(response){
//				UnBlock_Element('DivRoute_'+routeID);
//				Get_Return_Message(response);
//				js_Reload_Routing_Detail_Block(routeID);
//				Scroll_To_Top();
//			}
//		);
//	}
//}


function js_Submit_Comment(documentID,routeID)
{
	var editorName = 'Comment_'+routeID;
	var comment =getEditorValue(editorName);
	//if(comment == ''){	
	if(FCK_Get_Length(editorName)<=0){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInComment']?>');
	}else{
		Block_Element('DivRoute_'+routeID);
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_request_lock',array())?>',
			{
				'RouteID':routeID
			},
			function(result){
				var resultAry = result.split(',');
				if(resultAry[0] == '1') {
					$.post(
						'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_update_comment',array())?>',
						{
							'DocumentID':documentID,
   					    	'RouteID':routeID,
							'Comment':encodeURIComponent(comment)
						},
						function(response){
							UnBlock_Element('DivRoute_'+routeID);
							Get_Return_Message(response);
							js_Reload_Routing_Detail_Block(routeID);
							window.Scroll_To_Top();
						}
					);
				} else {
					alert('<?=$Lang['DocRouting']['WarningMsg']['RouteLockTiemout']?>');
					UnBlock_Element('DivRoute_'+routeID);
				}
			}
		);
		
		
	}
	
}


function FCK_Get_Length(FCKName)
{ 
    var oEditor = FCKeditorAPI.GetInstance(FCKName) ; 
    var checkContent= oEditor.EditorDocument ; 
    var contentLength ; 
    if ( document.all ){ 
        contentLength= checkContent.body.innerText.trim().length ;
	} 
	else{ 
	    var r = checkContent.createRange() ; 
	    r.selectNodeContents( checkContent.body ) ; 
	    contentLength= r.toString().trim().length ; 
	}
 
    return contentLength; 
} 

String.prototype.trim = function() 
{ 
    return this.replace(/(^[\s]*)|([\s]*$)/g, ""); 
} 


function js_Send_Reminder(routeID)
{
	var inProgressUsers = document.getElementsByName('InprogressUsers_'+routeID+'[]');
	var notViewedUsers = document.getElementsByName('NotViewedUsers_'+routeID+'[]');
	var users = [];
	var dv_inprogress = $('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['InProgress']?>');
	var dv_notviewed = $('#member_status_list_'+routeID+'_<?=$docRoutingConfig['INTRANET_DR_ROUTE_TARGET']['RecordStatus']['NotViewedYet']?>');
	
	if(dv_inprogress.is(':visible')){
		for(var i=0;i<inProgressUsers.length;i++){
			if(inProgressUsers[i].checked){
				users.push(inProgressUsers[i].value);
			}
		}
	}
	if(dv_notviewed.is(':visible')){
		for(var i=0;i<notViewedUsers.length;i++){
			if(notViewedUsers[i].checked){
				users.push(notViewedUsers[i].value);
			}
		}
	}
	
	if(users.length == 0){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseSelectUsers']?>');
	}else{
		Block_Element('DivRoute_'+routeID);
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_send_reminder',array())?>',
			{
				'RouteID':routeID,
				'TargetUserID[]':users 
			},
			function(response){
				Get_Return_Message(response);
				UnBlock_Element('DivRoute_'+routeID);
				window.Scroll_To_Top();
			}
		);
	}
}

function js_Show_Hide_Replyslip_Stat(routeID,replySlipID,replySlipType)
{
	var dv_stat = $('#reply_slip_stat_'+routeID);
	var dv_replyslip = $('#reply_slip_content_'+routeID);	
	if(dv_stat.is(':visible')){
		dv_stat.hide();
		dv_replyslip.show();
	}else{
		js_Get_Reply_Slip_Stat(routeID,replySlipID,replySlipType);
		dv_stat.show();
		dv_replyslip.hide();

	}
}

function js_Sign_Routing(routeID,replySlipID,replySlipType)
{
	var success = true;
	if(confirm('<?=$Lang['DocRouting']['WarningMsg']['AreYouSureWantToSign']?>')){
		Block_Element('DivRoute_'+routeID);
		
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_check_draft',array())?>',
			{
				'RouteID':routeID 
			},
			function(data) {
				var canSubmit = true;
				if(data > 0) {
					if(confirm('<?=$Lang['DocRouting']['WarningMsg']['SignIgnoreDraftedComment']?>')) {
						canSubmit = true;
					}else{
						canSubmit = false;
					}
				}
				if(canSubmit) {
					if(replySlipID){
						if(replySlipType=='<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'){
							success = tableReplySlipObj.submitReplySlip(replySlipID);
						} else {
							success = replySlipObj.submitReplySlip(replySlipID);
						}
					}
			
					if(success){
						$.post(
							'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_sign_routing',array())?>',
							{
								'DocumentID':<?=$DocumentID?>,
								'RouteID':routeID 
							},
							function(response){
								Get_Return_Message(response);
								js_Reload_Routing_Detail_Block(routeID);
								window.Scroll_To_Top();
							}
						);
					}else{
						UnBlock_Element('DivRoute_'+routeID);
					}
				}else{
					UnBlock_Element('DivRoute_'+routeID);
				}
			}
		);
	}
}

function js_Update_Doc_Status(documentID,status)
{
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_update_doc_status',array())?>',
		{
			'DocumentID':documentID,
			'RecordStatus':status
		},
		function(response){
			window.location.href = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','dr_detail',array('documentId'=>$DocumentID,'returnMsgKey'=>'UpdateSuccess'))?>';
		}
	);
}

function js_Add_File_Field()
{
	var divUploadFile = $('div#DivUploadFile');
	var num = $('input[type=file]').length;
	divUploadFile.append('<input name="UploadFile_'+num+'" type="file" class="Mandatory"><br />');
}

function js_Check_Upload_Form()
{
	var TBEditForm = document.getElementById('TBEditForm');
	var Files = $('input[name*=UploadFile_]');
	var WarningDiv = $('div#UploadFileWarningDiv');
	var CountFile = 0;
	for(var i=0;i<Files.length;i++){
		if($.trim(Files[i].value) != ''){
			CountFile++;
		}
	}
	if(CountFile==0){
		WarningDiv.show();
		return;
	}else{
		WarningDiv.hide();
	}
	
	Block_Thickbox();
	
	TBEditForm.action = "index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_feedback_upload_attachment',array())?>";
	TBEditForm.target = "FileUploadFrame";
	TBEditForm.encoding = "multipart/form-data";
	TBEditForm.method = 'post';
	TBEditForm.submit();
}

function js_Finish_Upload_Files(routeID,returnMsg)
{
	Get_Return_Message(returnMsg);
	window.tb_remove();
	js_Reload_Routing_Detail_Block(routeID);
	window.Scroll_To_Top();
}

function js_Delete_File(routeID,fileID,feedbackID)
{
	if(confirm('<?=$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisFile']?>')){
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_delete_file',array())?>',
			{
				'FileID':fileID,
				'FeedbackID':feedbackID 
			},
			function(response){
				Get_Return_Message(response);
				js_Reload_Routing_Detail_Block(routeID);
				js_Reload_Star_File_Div();
				window.Scroll_To_Top();
			}
		);
	}
}

<?php if($sys_custom['DocRouting']['DeleteFeedback']){ ?>
function js_Delete_Feedback(routeID,feedbackID)
{
	if(confirm('<?=$Lang['DocRouting']['WarningMsg']['ConfirmDeleteFeedback']?>')){
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_delete_feedback',array())?>',
			{
				'FeedbackID':feedbackID 
			},
			function(response){
				Get_Return_Message(response);
				js_Reload_Routing_Detail_Block(routeID);
				js_Reload_Star_File_Div();
				window.Scroll_To_Top();
			}
		);
	}
}
<?php } ?>

function js_Get_Reply_Slip_Stat(routeID,replySlipID,replySlipType)
{
	if($('#reply_slip_stat_'+routeID).html()==''){
		Block_Element('reply_slip_stat_'+routeID);
		var listTypeLink = "<?=$PATH_WRT_ROOT?>home/common_reply_slip/ajax_get_reply_slip_stat_div.php";
		var tableTypeLink = "<?=$PATH_WRT_ROOT?>home/common_table_reply_slip/ajax_get_reply_slip_stat_div.php";
		var replySlipLink = replySlipType == '<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'?tableTypeLink:listTypeLink;
		
		$('#reply_slip_stat_'+routeID).load(
			replySlipLink, 
			{ 
				ReplySlipID: replySlipID,
				DefaultShowDiv: 1
			},
			function(ReturnData)
			{
				initSuperTable(replySlipID, true);
				UnBlock_Element('reply_slip_stat_'+routeID);
			}
		);
	}
}

function js_Get_Reply_Slip_Answer(routeID,userID,replySlipID,disableAnswer,replySlipType,initJs)
{
	initJs = initJs || "";
	Block_Element('reply_slip_content_'+routeID);
	var listTypeLink = "<?=$PATH_WRT_ROOT?>home/common_reply_slip/ajax_get_reply_slip_div.php";
	var tableTypeLink = "<?=$PATH_WRT_ROOT?>home/common_table_reply_slip/ajax_get_reply_slip_div.php";
	var replySlipLink = replySlipType == '<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'?tableTypeLink:listTypeLink;
	
	$('#reply_slip_content_'+routeID).load(
		replySlipLink, 
		{ 
			ReplySlipID: replySlipID,
			ReplySlipUserID: userID,
			ShowUserAnswer: 1,
			DisableAnswer: disableAnswer 
		},
		function(ReturnData)
		{
			initSuperTable(replySlipID, false);
			UnBlock_Element('reply_slip_content_'+routeID);
			
			if (initJs != null && initJs != "") {
				eval(initJs);
			}
		}
	);
}

function js_Update_Star_Status(obj)
{
	var thisObj = $(obj);
	var star_class = thisObj.attr('class');
	var classes = ['','btn_set_important','btn_set_important_on'];
	var starDisplayCss = star_class == 'btn_set_important'?2:1;
	
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_update_star_status',array())?>',
		{
			'Action':'UpdateStarStatus',
			'documentID':'<?=$DocumentID?>',
			'userID':'<?=$indexVar['drUserId']?>',
			'starDisplayCss':starDisplayCss
		},
		function(ReturnData){
			thisObj.attr('class',classes[starDisplayCss]);
		}
	);
}

function js_Request_Lock(routeId)
{
	var dv = $('#write_comment_'+routeId);
	if(dv.is(':visible')){
		js_Release_Lock(routeId);
		js_Show_Hide_Comment(routeId,true);
	}else {

		Block_Element('DivRoute_'+routeId,'<?=$Lang['DocRouting']['CheckingRoutingStatus']?>');
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_request_lock',array())?>',
			{
				'RouteID':routeId
			},
			function(result){
				UnBlock_Element('DivRoute_'+routeId);
				var resultAry = result.split(',');
				if(resultAry[0] == '1') {
					js_Show_Hide_Comment(routeId,true);
				} else {
					var msg = '<?=$Lang['DocRouting']['WarningMsg']['AnotherPersonGivingFeedback']?>';
					msg = msg.replace('<!--TIMEOUT-->',resultAry[1]);
					alert(msg);
				}
			}
		);
	
	}
}

function js_Release_Lock(routeId)
{
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_release_lock',array())?>',
		{
			'RouteID':routeId
		},
		function(result){
			
		}
	);
}

function js_Save_Draft(routeID)
{
	var editorName = 'Comment_'+routeID;
	var comment = getEditorValue(editorName);
	
//	if(comment == ''){
    if(FCK_Get_Length(editorName)<=0){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInComment']?>');
	}else{
		Block_Element('DivRoute_'+routeID);
		$.post(
			'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_save_draft',array())?>',
			{
				'RouteID':routeID,
				'Comment':encodeURIComponent(comment)
			},
			function(response){
				UnBlock_Element('DivRoute_'+routeID);
				Get_Return_Message(response);
				js_Reload_Routing_Detail_Block(routeID);
				window.Scroll_To_Top();
			}
		);
	}
}
//Henry Added [20140528] [start]
function js_Get_Edit_Routing_Period_Send_Email(documentID)
{
	tb_show("<?=$Lang['DocRouting']['EditRoutingPeriodAndSendEmail']?>","#TB_inline?height=500&width=800&inlineId=FakeLayer");
	
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_edit_routing_period_send_email',array())?>',
		{
			'DocumentID':documentID 
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
			$('#TB_window').css('z-index',10000);
		});
}

function js_Submit_Edit_Routing_Period_Send_Email(documentID)
{
	var formObj = document.form1;
	var error = 0;
	var routingNum = 1;

 	var typePeriodChecked = $('input#EffectiveTypePeriod_'+routingNum).is(':checked');

	if(typePeriodChecked){
		var startDateObj = document.getElementById('EffectiveStartDate_'+routingNum);
		var endDateObj = document.getElementById('EffectiveEndDate_'+routingNum);
		var startDate = $.trim(startDateObj.value);
		var endDate = $.trim(endDateObj.value);
		var startTimeHr = $('#EffectiveStartTime_'+routingNum+'_hour').val();
		startTimeHr = parseInt(startTimeHr)<10? '0'+startTimeHr : startTimeHr;
		var startTimeMin = $('#EffectiveStartTime_'+routingNum+'_min').val();
		startTimeMin = parseInt(startTimeMin)<10? '0'+startTimeMin : startTimeMin;
		var startTimeSec = $('#EffectiveStartTime_'+routingNum+'_sec').val();
		startTimeSec = parseInt(startTimeSec)<10? '0'+startTimeSec : startTimeSec;
		var startTime = startTimeHr + ':' + startTimeMin + ':' + startTimeSec;
		var endTimeHr = $('#EffectiveEndTime_'+routingNum+'_hour').val();
		endTimeHr = parseInt(endTimeHr)<10? '0'+endTimeHr : endTimeHr;
		var endTimeMin = $('#EffectiveEndTime_'+routingNum+'_min').val();
		endTimeMin = parseInt(endTimeMin)<10? '0'+endTimeMin : endTimeMin;
		var endTimeSec = $('#EffectiveEndTime_'+routingNum+'_sec').val();
		endTimeSec = parseInt(endTimeSec)<10? '0'+endTimeSec : endTimeSec;
		var endTime = endTimeHr + ':' + endTimeMin + ':' + endTimeSec;
		startDateTime = startDate + ' ' + startTime;
		endDateTime = endDate + ' ' + endTime;
		
		if(!check_date_without_return_msg(startDateObj) || !check_date_without_return_msg(endDateObj) || startDate > endDate){
			error++;
 			$('#EffectiveTypeWarningDiv_'+routingNum).html('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange']?>').show();
		}else{
 			if(startDate == endDate){
 				if(startTime >= endTime){
 					error++;
 					$('#EffectiveTypeWarningDiv_'+routingNum).html('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange']?>').show();
 				}else{
 					$('#EffectiveTypeWarningDiv_'+routingNum).hide();
 				}
 			}
		}
	}
	if($('input#SendEmailYes').is(':checked') && !$('input#EmailTargetInProgress').is(':checked') && !$('input#EmailTargetNotViewedYet').is(':checked')){
		alert("<?=$Lang['DocRouting']['WarningMsg']['PleaseSelectEmailTarget']?>");
		return false;
	}
	
	if($('input#SendEmailYes').is(':checked') && $.trim($('textarea#EmailContent').val()) == ''){
		alert("<?=$Lang['DocRouting']['WarningMsg']['PleaseEnterEmailContent']?>");
		return false;
	}
	
	if(error==0){
		Block_Thickbox();
		
//		if(typePeriodChecked){
//			$.post(
//				'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_last_routing_datetime',array())?>',
//				{
//					'DocumentID':'<?=$DocumentID?>'
//				},
//				function(ReturnDateTime) {
//					var err = 0;
//					var msg = '<?=$Lang['DocRouting']['WarningMsg']['PleaseSetStartDateAfterDateTime']?>';
//					msg = msg.replace('<!--DATETIME-->',ReturnDateTime);
//					if(ReturnDateTime != ''){
//						if(startDateTime <= ReturnDateTime){
//							err++;
//							$('#EffectiveTypeWarningDiv_'+routingNum).html(msg).show();
//							UnBlock_Thickbox();
//						}
//					}
//					if(err == 0){
//						formObj.target = "UpdateFrame";
//						formObj.encoding = "multipart/form-data";
//						formObj.method = 'post';
//						formObj.submit();
//					}
//				}
//			);
//		}else{
			formObj.target = "UpdateFrame";
			formObj.encoding = "multipart/form-data";
			formObj.method = 'post';
			formObj.submit();
//		}
	}
}

function js_Finish_Edit_Routing_Period_Send_Email(returnMsg)
{
	Get_Return_Message(returnMsg);
	window.tb_remove();
	window.location.href = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','dr_detail',array('documentId'=>$DocumentID))?>';
}
//Henry Added [20140528] [end]

//////////////// Start AdHoc routing ////////////////
function js_Get_AddHoc_Routing_Form(documentID)
{
	tb_show("<?=$Lang['DocRouting']['AddAdHocRouting']?>","#TB_inline?height=640&width=850&inlineId=FakeLayer");
	
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_addhoc_routing_form',array())?>',
		{
			'DocumentID':documentID 
		},
		function(ReturnData) {
			$('div#TB_ajaxContent').html(ReturnData);
			$('#TB_window').css('z-index',10000);
		});
}

function js_Document_Show(routingNumber){
		
	$('#VisiblePeriod_' + routingNumber).show();

	
}

function js_Document_Hide(routingNumber){
	
	$('#VisiblePeriod_' + routingNumber).hide();

	
}

function js_Extra_User_Option_Show(routingNumber){
	$('#VisibleAccessUsers_' + routingNumber).show();

}


function js_Extra_User_Option_Hide(routingNumber){

	$('#VisibleAccessUsers_' + routingNumber).hide();
}

function js_User_Option_Remove(routingNumber){
	$('#users_' + routingNumber)
    .find('option:selected')
    .remove()
    .end()
;
}

function js_Extra_User_Option_Remove(routingNumber){
	$('#extraUsers_' + routingNumber)
    .find('option:selected')
    .remove()
    .end()
;
}

//function js_Change_Selection(routingNumber){
//	var OptionValue = $("#presetNotesSelection_" + routingNumber).val();
//	var OptionText = $("#presetNotesSelection_"+ routingNumber +" option[value='"+OptionValue+"']").text(); 
//	var Editor1 = FCKeditorAPI.GetInstance('Note_' + routingNumber);
//	
//	if(OptionValue!=''){	
//		Editor1.SetHTML(OptionText);	     
//	} 
//	else{
//		Editor1.SetHTML('');	
//	}
//	
//	return false; 
//}

function js_Enable_ReplySlip(routingNumber,isChecked)
{
	$('#ActionAllowReplySlip_'+routingNumber).attr('disabled',!isChecked);
	if(!isChecked){
		$('#ActionAllowReplySlip_'+routingNumber).attr('checked',false);
		js_Enable_Edit_ReplySlip(routingNumber,isChecked);
	}
}

function js_Enable_Edit_ReplySlip(routingNumber,isChecked)
{
	//$('#PreviewReplySlipBtn_'+routingNumber).attr('disabled',!isChecked);

	if(isChecked){
		//$('#PreviewReplySlipBtn_'+routingNumber).show();
		//$('#ReplySlipFile_'+routingNumber).show();
		$('div#routeReplySlipSettingsDiv_'+routingNumber).show();
	//	$('#releaseResultSettingsDiv_'+routingNumber).attr('style', 'padding-left:25px');
	//	$('#showUserNameDisplaySettingsDiv_'+routingNumber).attr('style', 'padding-left:25px;');
		
	}else{
		//$('#PreviewReplySlipBtn_'+routingNumber).hide();
		//$('#ReplySlipFile_'+routingNumber).hide();
		$('div#routeReplySlipSettingsDiv_'+routingNumber).hide();
		$('#ReplySlipFile_'+routingNumber).val('');
		$('input#ReplySlipContent_'+routingNumber).val('');
	//	$('#releaseResultSettingsDiv_'+routingNumber).attr('style', 'display:none' );
	//	$('#showUserNameDisplaySettingsDiv_'+routingNumber).attr('style', 'display:none' );
		
	}
}

function js_Enable_Edit_Comment(routingNumber,isChecked)
{
	if(isChecked){
		$('div#ActionAllowCommentSettingsDiv_'+routingNumber).show();
	}else{
		$('div#ActionAllowCommentSettingsDiv_'+routingNumber).hide();
	}
}

function js_Enable_Edit_Attachment(routingNumber,isChecked){
	if(isChecked){
		$('div#ActionAllowAttachmentSettingsDiv_'+routingNumber).show();
	}else{
		$('div#ActionAllowAttachmentSettingsDiv_'+routingNumber).hide();
	}
}

//function js_Submit_View_ReplySlip(routingNumber)
//{
//	var formObj = document.form1;
//	var oldTarget = document.form1.target;
//	var oldAction = document.form1.action;
//	var fileVal = $.trim($('#ReplySlipFile_'+routingNumber).val());
//	var replySlipType = $('input[name="ReplySlipType_'+routingNumber+'"]:checked').val();
//	var listTypeLink = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('common','reply_slip_form',array())?>'+'&ReplySlipFileField=ReplySlipFile_'+routingNumber;
//	var tableTypeLink = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('common','table_reply_slip_form',array())?>'+'&ReplySlipFileField=ReplySlipFile_'+routingNumber;
//	if(fileVal == ''){
//		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseUploadReplySlipFile']?>');
//	}else{
//		formObj.target = '_blank';
//		formObj.action = replySlipType=='<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'?tableTypeLink:listTypeLink;
//		formObj.submit();
//		formObj.target = oldTarget;
//		formObj.action = oldAction;
//	}
//}


function js_Submit_View_ReplySlip(selectedRoutingNumber, replySlipId, routingNumber)
{
	
	replySlipId = replySlipId || '';

	var formObj = document.form1;
	var oldTarget = document.form1.target;
	var oldAction = document.form1.action;
	var fileVal = $.trim($('#ReplySlipFile_'+selectedRoutingNumber).val());
	var replySlipType = $('input[name="ReplySlipType_'+selectedRoutingNumber+'"]:checked').val();
	
	var listTypeLink = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('common','reply_slip_form',array())?>'+'&ReplySlipFileField=ReplySlipFile_'+selectedRoutingNumber+'&replySlipId='+replySlipId;
	var tableTypeLink = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('common','table_reply_slip_form',array())?>'+'&ReplySlipFileField=ReplySlipFile_'+selectedRoutingNumber+'&replySlipId='+replySlipId;
	if(fileVal == '' && replySlipId == ''){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseUploadReplySlipFile']?>');
	}else{
		formObj.target = '_blank';
		formObj.action = replySlipType=='<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'?tableTypeLink:listTypeLink;
		formObj.submit();
		formObj.target = oldTarget;
		formObj.action = oldAction;
	}
}



function js_Submit_AddHoc_Routing(documentID)
{
	var formObj = document.form1;
	var error = 0;
	var routingNum = $('input#DisplayOrder').val();
	
//	var foundOption =  document.getElementsByName('users_'+routingNum+'[]'); 
// 	var foundOptionArr = foundOption.length;

	var foundOptionArr =  $('#users_'+ routingNum + ' option' ).length; 
	
 	var actionOptions = $('input[name="ActionAllow_'+routingNum+'[]"]:checked');
 	var typePeriodChecked = $('input#EffectiveTypePeriod_'+routingNum).is(':checked');
 	//var viewByUserChecked = $('input#ViewRightViewByUser_'+routingNum).is(':checked');
 	var actionReplySlipChecked = $('input#ActionAllowReplySlip_'+routingNum).is(':checked');
 	var replySlipContent = $.trim($('input#ReplySlipFile_'+routingNum).val());
 	var replySlipId = $('input#replySlipId_' + routingNum).val();
 	var lockChecked = $('input#EnableLock_' + routingNum).is(':checked');
	var lockDuration = $.trim($('input#LockDuration_' + routingNum).val());
 	
 	var startDateTime = '';
 	var endDateTime = '';
 	var copyReplySlip = $('#CopyReplySlip_' + routingNum).val();
 	 	
 	if(foundOptionArr== 0){		
		error++;
		$('#UserWarningDiv_'+routingNum).show();
	}else{
		checkOptionAll(document.getElementById('users_'+routingNum));
		$('#UserWarningDiv_'+routingNum).hide();
	}
	
	if(actionOptions.length == 0){
		error++;
		$('#ActionWarningDiv_'+routingNum).show();
	}else{
		$('#ActionWarningDiv_'+routingNum).hide();
	}
	
	if (actionReplySlipChecked) {
		if (replySlipId == '' && replySlipContent == '' && typeof copyReplySlip=='undefined' ) {
			error++;
			$('#ActionAllowReplySlipWarningDiv_'+routingNum).show();
		}
		else if (isReplySlipCsvWarningShown(routingNum)){	// csv valid or not checked in onchange of file input
			error++;
			$('#ActionAllowReplySlip_'+routingNum).focus();
		}
	}
	else {
		$('#ActionAllowReplySlipWarningDiv_'+routingNum).hide();
	}
	
	if(typePeriodChecked){
		var startDateObj = document.getElementById('EffectiveStartDate_'+routingNum);
		var endDateObj = document.getElementById('EffectiveEndDate_'+routingNum);
		var startDate = $.trim(startDateObj.value);
		var endDate = $.trim(endDateObj.value);
		var startTimeHr = $('#EffectiveStartTime_'+routingNum+'_hour').val();
		startTimeHr = parseInt(startTimeHr)<10? '0'+startTimeHr : startTimeHr;
		var startTimeMin = $('#EffectiveStartTime_'+routingNum+'_min').val();
		startTimeMin = parseInt(startTimeMin)<10? '0'+startTimeMin : startTimeMin;
		var startTimeSec = $('#EffectiveStartTime_'+routingNum+'_sec').val();
		startTimeSec = parseInt(startTimeSec)<10? '0'+startTimeSec : startTimeSec;
		var startTime = startTimeHr + ':' + startTimeMin + ':' + startTimeSec;
		var endTimeHr = $('#EffectiveEndTime_'+routingNum+'_hour').val();
		endTimeHr = parseInt(endTimeHr)<10? '0'+endTimeHr : endTimeHr;
		var endTimeMin = $('#EffectiveEndTime_'+routingNum+'_min').val();
		endTimeMin = parseInt(endTimeMin)<10? '0'+endTimeMin : endTimeMin;
		var endTimeSec = $('#EffectiveEndTime_'+routingNum+'_sec').val();
		endTimeSec = parseInt(endTimeSec)<10? '0'+endTimeSec : endTimeSec;
		var endTime = endTimeHr + ':' + endTimeMin + ':' + endTimeSec;
		startDateTime = startDate + ' ' + startTime;
		endDateTime = endDate + ' ' + endTime;
		
		if(!check_date_without_return_msg(startDateObj) || !check_date_without_return_msg(endDateObj) || startDate > endDate){
			error++;
 			$('#EffectiveTypeWarningDiv_'+routingNum).html('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange']?>').show();
		}else{
 			if(startDate == endDate){
 				if(startTime >= endTime)
 				{
 					error++;
 					$('#EffectiveTypeWarningDiv_'+routingNum).html('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange']?>').show();
 				}else
 				{
 					$('#EffectiveTypeWarningDiv_'+routingNum).hide();
 				}
 			}
		}
	}
	
//	if(viewByUserChecked){
//		var numExtraUsers = $('select[name=extraUsers_'+routingNum+'[]] option').length;
//		if(numExtraUsers == 0){
//			error++;
//			$('#ExtraUserWarningDiv_'+routingNum).show();
//		}else{
////			checkOptionAll(document.getElementById('extraUsers_'+routingNum+'[]'));
//
//			checkOptionAll(document.getElementById('extraUsers_'+routingNum));
//			
//			$('#ExtraUserWarningDiv_'+routingNum).hide();
//		}
//	}
	checkOptionAll(document.getElementById('extraUsers_'+routingNum));
	
	if(lockChecked) {
		if(lockDuration == '' || !is_positive_int(lockDuration) || lockDuration == 0) {
			error++;
			$('#LockDurationWarningDiv_' + routingNum).show();
		}else{
			$('#LockDurationWarningDiv_' + routingNum).hide();
		}
	}
	
	if(error==0){
		Block_Thickbox();
		
		if(typePeriodChecked){
			$.post(
				'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_get_last_routing_datetime',array())?>',
				{
					'DocumentID':'<?=$DocumentID?>'
				},
				function(ReturnDateTime) {
					var err = 0;
					var msg = '<?=$Lang['DocRouting']['WarningMsg']['PleaseSetStartDateAfterDateTime']?>';
					msg = msg.replace('<!--DATETIME-->',ReturnDateTime);
					if(ReturnDateTime != ''){
						if(startDateTime <= ReturnDateTime){
							err++;
							$('#EffectiveTypeWarningDiv_'+routingNum).html(msg).show();
							UnBlock_Thickbox();
						}
					}
					if(err == 0){
						formObj.target = "UpdateFrame";
						formObj.encoding = "multipart/form-data";
						formObj.method = 'post';
						formObj.submit();
					}
				}
			);
		}else{
			formObj.target = "UpdateFrame";
			formObj.encoding = "multipart/form-data";
			formObj.method = 'post';
			formObj.submit();
		}
	}
}

function validateReplySlipCsv(routingNumber) {
	var fileInputId = 'ReplySlipFile_' + routingNumber;
	var fileVal = $.trim($('#' + fileInputId).val());
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber; 
	var replySlipType = $('input[name="ReplySlipType_'+routingNumber+'"]:checked').val();
	
	if (fileVal == '') {
		hideReplySlipCsvInvalidWarning(routingNumber);
	}
	else {
		var targetFrameId = 'replySlipIFrame_' + routingNumber;
		var formObj = document.form1;
		var oldTarget = document.form1.target;
		var oldAction = document.form1.action;
		
		formObj.target = targetFrameId;
		formObj.action = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('management','ajax_validate_reply_slip_csv')?>' + '&ReplySlipFileField=' + fileInputId + '&routingNumber=' + routingNumber  + '&ReplySlipType=' + replySlipType;
		formObj.submit();
		formObj.target = oldTarget;
		formObj.action = oldAction;
	}
}

function showReplySlipCsvInvalidWarning(routingNumber) {
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber;
	$('div#' + warningDivId).show();
}
function hideReplySlipCsvInvalidWarning(routingNumber) {
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber;
	$('div#' + warningDivId).hide();
}
function isReplySlipCsvWarningShown(routingNumber) {
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber;
	return $('div#' + warningDivId).is(':visible');
}

function js_Finish_Update_Addhoc_Routing(returnMsg)
{
	Get_Return_Message(returnMsg);
	window.tb_remove();
	window.location.href = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','dr_detail',array('documentId'=>$DocumentID))?>';
}

function js_Enable_Lock(obj, timeoutDivId)
{
	var elem = $(obj);
	
	if(elem.is(':checked')) {
		$('#'+timeoutDivId).show();
	} else {
		$('#'+timeoutDivId).hide();
	}
}

function clickedSelectViewOnlyUser(routingNumber) {
	var selectedRouteUserObj = document.getElementById('users_' + routingNumber);
	
	var excludeUserIdPara = '';
	$('select#users_' + routingNumber + ' option').each( function () {
		excludeUserIdPara += "exclude_user_id_ary[]=" + $(this).val() + "&";
	});
	excludeUserIdPara = excludeUserIdPara.substr(0,(excludeUserIdPara.length - 1));
	
	newWindow('/home/common_choose/index.php?fieldname=extraUsers_' + routingNumber + '[]&ppl_type=pic&permitted_type=1&excluded_type=4&'+excludeUserIdPara, 9);
}

function js_onChangeShowHideReplySlipUserOption(showHide, targetObjId)
{
	if(showHide == '<?=$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['hide']?>'){
		$('#'+targetObjId).attr('checked',true);
	}
}

//////////////////////// End adhoc routing /////////////////////////

<?php
	if ($plugin['power_voice']){ 
?>
		var NewRecordBefore = false;
		var CurRecordRouteID = 0;
		
		function listenPVoice2(fileName)
		{		
			newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/pvoice_listen_now_intranet2.php?from_intranet=1&location="+fileName, 18);
		}	
		
		function editPVoice(routeID)
		{
			var fileName = $('input[name=voiceFile_'+routeID+']').val();		
			CurRecordRouteID = routeID;
			newWindow("http://<?=$eclass40_httppath?>/src/tool/powervoice/index.php?from_intranet=1&fileName="+fileName, 17);
		}	
		
		function clearPVoice(routeID)
		{		
			var voiceObj = document.getElementById("voiceDiv_"+routeID);
			var clearObj = document.getElementById("clearDiv_"+routeID);
			
			voiceObj.innerHTML = "";
			$('input[name=voiceFile_'+routeID+']').val("");		
			clearObj.style.display = "none";
		}

		function file_exists()
		{
			alert("<?=$classfiles_alertMsgFile12?>");
		}
		
		function file_saved(fileName,filePath,fileHttpPath,fileEmpty)
		{		
			var voiceObj = document.getElementById("voiceDiv_"+CurRecordRouteID);
			var clearObj = document.getElementById("clearDiv_"+CurRecordRouteID);
			
			if (fileName != "")
			{
				if ((fileHttpPath != undefined) &&  (fileHttpPath != ""))
					voiceObj.innerHTML = "<a href=\"javascript:listenPVoice2('"+fileHttpPath+"')\" ><?=$VoiceClip?>"+fileName+"</a>&nbsp;&nbsp;";
				else				
					voiceObj.innerHTML = fileName+"&nbsp;&nbsp;";
				if (fileEmpty=="1")
				{
					$('input[name=is_empty_voice_'+CurRecordRouteID+']').val(1);
				}
				else
				{
					NewRecordBefore = true;
					$('input[name=is_empty_voice_'+CurRecordRouteID+']').val("");
				}
					
				$('input[name=voiceFile_'+CurRecordRouteID+']').val(fileName);
				$('input[name=voicePath_'+CurRecordRouteID+']').val(filePath);
				clearObj.style.display = "block";
				
				if(fileEmpty != "1"){
					js_Save_Powervoice_File(CurRecordRouteID);
				}
			}
		}
		
		function js_Save_Powervoice_File(routeID)
		{
			Block_Element('DivRoute_'+routeID);
			var voiceFile = $('input[name=voiceFile_'+routeID+']').val();
			var voicePath = $('input[name=voicePath_'+routeID+']').val();
			
			$.post(
				'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_save_powervoice_file',array())?>',
				{
					'DocumentID':'<?=$DocumentID?>',
					'RouteID':routeID,
					'VoiceFile':encodeURIComponent(voiceFile),
					'VoicePath':voicePath 
				},
				function(response){
					Get_Return_Message(response);
					js_Reload_Routing_Detail_Block(routeID);
					window.Scroll_To_Top();
				}
			);
			
		}
		
<? 
	} 
?>

function LoadArchiveTBForm(documentId, linkToType)
{
	js_Show_ThickBox('<?=$Lang['DigitalArchiveModuleUpload']['Archive']?>', <?=$ldamu->TBHeight?>, <?=$ldamu->TBWidth?>, '');
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_archive_to_DA',array())?>',
		{
			'DocumentID':documentId, 
			'LinkToType':linkToType
		},
		function(data){
			$('div#TB_ajaxContent').html(data);
		}
	);
}

function LoadArchiveTBFormForStarFile(documentId, linkToType)
{
	js_Show_ThickBox('<?=$Lang['DigitalArchiveModuleUpload']['Archive']?>', <?=$ldamu->TBHeight?>, <?=$ldamu->TBWidth?>, '');
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_archive_to_DA',array('Starred'=>1))?>',
		{
			'DocumentID[]':documentId, 
			'LinkToType':linkToType 
		},
		function(data){
			$('div#TB_ajaxContent').html(data);
		}
	);
}

function initSuperTable(replySlipId, statTable) {
	var jsScreenWidth = parseInt(Get_Screen_Width());
	var jsTableWidth = jsScreenWidth - 300;
	var jsScreenHeight = parseInt(Get_Screen_Height());
	var jsTableHeight = jsScreenHeight - 100;
	
	var tablePrefix = (statTable)? 'replySlipStatTbl_' : 'replySlipTbl_';
	var hiddenFieldPrefix = (statTable)? 'tableStatReplySlip' : 'tableReplySlip';
	
	var tableId = tablePrefix + replySlipId;
	var numOfHeaderRow = $('input#' + hiddenFieldPrefix + 'NumOfHeaderRow_' + replySlipId).val(); 
	var numOfFixedCol = $('input#' + hiddenFieldPrefix + 'NumOfFixedCol_' + replySlipId).val();
	var originalTableHeight = $('table#' + tableId).height();
	
	if (numOfHeaderRow > 0 && numOfFixedCol > 0) {
		$('table#' + tableId).toSuperTable({
	    	width: 		jsTableWidth + "px", 
	    	height: 	jsTableHeight + "px", 
	    	headerRows:	numOfHeaderRow,
	    	fixedCols: 	numOfFixedCol,
	    	onFinish: 	function () {
	    					var _tableId = this.sDataTable.id;
	    					var _replySlipId = parseInt(_tableId.replace(tablePrefix, ''));
	    					
	    					if (originalTableHeight < jsTableHeight) {
	    						fixSuperTableHeight(_tableId, originalTableHeight + 10);
	    					}
	    				}
		});
	}
}

//Henry Added
function js_update_User_List_Div(user_ddl,userlist_div){
	var fld = document.getElementById(user_ddl);
	selectAll(fld,true);
	var values = "";
	for (var i = 0; i < fld.options.length; i++) {
	  if (fld.options[i].selected && fld.options[i].text.replace(/^\s+|\s+$/g, '') != "") {
	    values += fld.options[i].text+", ";
	  }
	}
	values = values.substring(0, values.length - 2);
	$("#"+userlist_div).text(values);
	selectAll(fld,false);
}
function selectAll(selectBox,selectAll) { 
        for (var i = 0; i < selectBox.options.length; i++) { 
             selectBox.options[i].selected = selectAll; 
        }
}

function js_Star_File(obj,fileId)
{
	var update_url = '';
	var change_class = '';
	var obj_class = $(obj).attr('className');
	if(obj_class == 'btn_set_important'){
		change_class = 'btn_set_important_on';
		update_url = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_file_star_update',array('ajax_task'=>'addFileStar'))?>';
	}else if(obj_class == 'btn_set_important_on'){
		change_class = 'btn_set_important';
		update_url = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_file_star_update',array('ajax_task'=>'deleteFileStar'))?>';
	}
	
	$.post(
		update_url,
		{
			'FileID':fileId 
		},
		function(response){
			if(response == "1"){
				$(obj).attr('className',change_class);
				js_Reload_Star_File_Div();
			}
		}
	);
}

function js_Reload_Star_File_Div()
{
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_reload_star_file_div',array())?>',
		{
			'DocumentID':'<?=$DocumentID?>'
		},
		function(data){
			$('div#StarFileDiv').replaceWith(data);
		}
	);
}

function LoadArchiveDocumentTBForm(documentId)
{
	js_Show_ThickBox('<?=$Lang['DocRouting']['ArchiveDocument']?>', <?=$ldamu->TBHeight?>, <?=$ldamu->TBWidth?>, '');
	$.post(
		'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_archive_document',array())?>',
		{
			'DocumentID':documentId
		},
		function(data){
			$('div#TB_ajaxContent').html(data);
		}
	);
}

function js_GetSendArchivedDocumentEmailForm(link)
{
	tb_show("<?=$Lang['DocRouting']['SendEmail']?>","#TB_inline?height=420&width=640&inlineId=FakeLayer");
	$.get(
		'index.php?pe=' + link,
		{},
		function(data){
			$('div#TB_ajaxContent').html(data);
		}
	);
}

function js_SendArchivedDocumentEmail(link)
{
	var valid = true;
	$('.Warning').hide();
	checkOptionAll(document.getElementById('TargetUser'));
	if($('select#TargetUser option:selected').length==0){
		valid = false;
		$('#TargetUserWarning').show();
	}
	
	if($('input[name="TargetEmail\\[\\]"]:checked').length==0){
		valid = false;
		$('#TargetEmailWarning').show();
	}
	
	if(valid)
	{
		Block_Thickbox();
		$.post(
			'index.php?pe='+link,
			$('#TBForm1').serialize(),
			function(ReturnMsg){
				window.tb_remove();
				Get_Return_Message(ReturnMsg);
				window.Scroll_To_Top();
			}
		);
	}
}
</script>
<?php

echo $indexVar['libDocRouting_ui']->getDocDetailForm($DocumentID);
?>