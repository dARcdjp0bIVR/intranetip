<?php
// using : 
/*************
 * Change Log:
 *  2016-02-02 (Carlos): added resetFiltersToAllStatus() which is called before doing search to reset all filters to include all conditions. 
 *  2014-10-20 (Carlos)[ip2.5.5.10.1]: added thickbox js & css, js_GetSendSignEmailForm(link), js_SendSignEmail(link)
 * 	2013-10-03 (Henry): added DisplayAdvanceSearch() and HiddenAdvanceSearch() and add flag in jsReloadCurrentRouting()
 * 	2013-04-15 (Carlos): add filter expireStatus
 *************/

?>

<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>

<script language="javascript">

$(document).ready(function(){
	var flag = <?=(stripslashes(stripslashes($flag))?stripslashes(stripslashes($flag)):0)?>;
	jsReloadCurrentRouting(flag); //Henry Modified

	$('#statusButton').click(function(){
		
		jsReloadCurrentRouting(0);
		
	});
		
	
	$("#starStatus").change(function() {
      jsReloadCurrentRouting(0);
      changedstarStatus($("#starStatus").val());
    });
    
    $("#createStatus").change(function() {
      jsReloadCurrentRouting(0);
      changedcreateStatus($("#createStatus").val());
    });
        
    $("#followStatus").change(function() {
      jsReloadCurrentRouting(0);
      changedfollowStatus($("#followStatus").val());
    });
    
    $("#expireStatus").change(function(){
    	jsReloadCurrentRouting(0);
      	changedexpireStatus($("#expireStatus").val());
    });
    
//    # Cookie 
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "ck_doc_management_current_rountings_star_staus";
	arrCookies[arrCookies.length] = "ck_doc_management_current_rountings_create_status";
	arrCookies[arrCookies.length] = "ck_doc_management_current_rountings_follow_status";
	arrCookies[arrCookies.length] = "ck_doc_management_current_routings_expire_status";
    <? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
//	<? } else { ?>
//		$.cookies.set('ck_doc_management_current_rountings_star_staus', $('select#starStatus').val().toString());
//		$.cookies.set('ck_doc_management_current_rountings_create_status', $('select#createStatus').val().toString());
//		$.cookies.set('ck_doc_management_current_rountings_follow_status', $('select#followStatus').val().toString());
//	
	<? } ?>
	
})
	

function changedstarStatus(newValue) {
	$.cookies.set('ck_doc_management_current_rountings_star_staus', newValue.toString());
}
function changedcreateStatus(newValue) {
	$.cookies.set('ck_doc_management_current_rountings_create_status', newValue.toString());
}
function changedfollowStatus(newValue) {
	$.cookies.set('ck_doc_management_current_rountings_follow_status', newValue.toString());
}
function changedexpireStatus(newValue) {
	$.cookies.set('ck_doc_management_current_routings_expire_status', newValue.toString());
}

function jsReloadCurrentRouting(flag)
{
	if(flag != 1){
		$.cookies.set('ck_doc_management_current_routings_keyword', $('#keyword').val().toString());
		$.cookies.set('ck_doc_management_current_routings_flag', '0');
	}
	else{
		resetFiltersToAllStatus();
		if($('#StartDate').val() > $('#EndDate').val() && $('#byDateRange').is(":checked") || 
			$('#StartDate2').val() > $('#EndDate2').val() && $('#byDateRange2').is(":checked")){
			alert("<?=$Lang['General']['JS_warning']['InvalidDateRange']?>");
			return false;
		}
		$.cookies.set('ck_doc_management_current_routings_routingTitle', $('#routingTitle').val().toString());
		$.cookies.set('ck_doc_management_current_routings_byDateRange', $('#byDateRange').is(":checked").toString());
		$.cookies.set('ck_doc_management_current_routings_StartDate', $('#StartDate').val().toString());
		$.cookies.set('ck_doc_management_current_routings_EndDate', $('#EndDate').val().toString());
		$.cookies.set('ck_doc_management_current_routings_byDateRange2', $('#byDateRange2').is(":checked").toString());
		$.cookies.set('ck_doc_management_current_routings_StartDate2', $('#StartDate2').val().toString());
		$.cookies.set('ck_doc_management_current_routings_EndDate2', $('#EndDate2').val().toString());
		$.cookies.set('ck_doc_management_current_routings_createdBy', $('#createdBy').val().toString());
		$.cookies.set('ck_doc_management_current_routings_tag', $('#tag').val().toString());
		$.cookies.set('ck_doc_management_current_routings_academic_year', $('#academic_year').val().toString());
		$.cookies.set('ck_doc_management_current_routings_flag', '1');
	}

	var formValue = $("form").serialize();
	var action ='CurrentRoutings';
	
	Block_Document();
		
	$.ajax({
	url:      	"?pe=<?php echo $reloadRoutingLink ?>",
	type:     	"POST",
	data:     	formValue +'&Action='+action+'&flag='+flag ,
	async:		true,
	error:    	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
			  	},
	success: function(data)
			 {		
				if(data)
				{						 
					$("#currentRoutingItemDisplayReload").html(data);
					UnBlock_Document();
				}
			}
	 });	
}

function js_Update_Star_Status(documentID, starDisplayCss, userID){

	var action = 'UpdateStarStatus';
	Block_Document();
	
	$.ajax({
		url:      	"?pe=<?php echo $updateStarStatusLink ?>",
		type:     	"POST",
		data:     	'&Action='+action + '&starDisplayCss='+starDisplayCss + '&userID='+userID + '&documentID=' +documentID,
		async:		true,
		error:    	function(xhr, ajaxOptions, thrownError){
						alert(xhr.responseText);
				  	},
		success: function(data)
				 {		
				 	
					if(data)
					{				 
						jsReloadCurrentRouting(0);
						UnBlock_Document();					
					}
				}
		 });	

}



function js_Delete_Alert(obj, page){
    if(confirm("<?=$Lang['RepairSystem']['DeleteRequestSummaryConfirm'];?>")){	            
	    obj.action='?pe='+page;                
	    obj.method="POST";
	    obj.submit();
    }   
}

function js_Filter_Selected_Tag(tagID){

	 $('#selectedTag').val(tagID);
	 
	 jsReloadCurrentRouting(0);
}

function DisplayAdvanceSearch() {
	
	var pos = $("#AdvanceSearchText").position(); 
	
	topPos = pos.top;
	leftPos = pos.left;
	
	topPos = parseInt(topPos) + 25;
	leftPos = parseInt(leftPos) - 482;
	
	//MM_showHideLayers('DR_search','','show');
	$("#DR_search").show();$("#DR_search").css({"position": "absolute", "top": topPos, "left": leftPos, "z-index":99, "background":"none repeat scroll 0% 0% rgb(244, 244, 244)", "padding":"7px 7px 0px","border":"1px solid rgb(204, 204, 204)","width":"500px"});
}

function HiddenAdvanceSearch(){
	$("#DR_search").hide();
}

function resetFiltersToAllStatus()
{
	if($('#createStatus').length > 0){
		$('#createStatus').val('');
	}
	
	if($('#followStatus').length > 0){
		$('#followStatus').val('');
	}
	
	if($('#expireStatus').length > 0){
		$('#expireStatus').val('');
	}
}

function checkGoSearch(evt) {
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) { // enter
		resetFiltersToAllStatus();
		jsReloadCurrentRouting(0);
	}
	else {
		return false;
	}
}

function js_GetSendSignEmailForm(link)
{
	tb_show("<?=$Lang['DocRouting']['SendEmail']?>","#TB_inline?height=480&width=640&inlineId=FakeLayer");
	$.get(
		'index.php?pe=' + link,
		{},
		function(data){
			$('div#TB_ajaxContent').html(data);
		}
	);
}

function js_SendSignEmail(link)
{
	var valid = true;
	$('.Warning').hide();
	checkOptionAll(document.getElementById('TargetUser'));
	if($('select#TargetUser option:selected').length==0){
		valid = false;
		$('#TargetUserWarning').show();
	}
	
	if($('input[name="TargetEmail\\[\\]"]:checked').length==0){
		valid = false;
		$('#TargetEmailWarning').show();
	}
	
	
	if($('input[name="TargetRoute\\[\\]"]:checked').length==0){
		valid = false;
		$('#TargetRouteWarning').show();
	}
	
	if(valid)
	{
		Block_Thickbox();
		$.post(
			'index.php?pe='+link,
			$('#TBForm1').serialize(),
			function(ReturnMsg){
				window.tb_remove();
				Get_Return_Message(ReturnMsg);
				window.Scroll_To_Top();
			}
		);
	}
}

</script>

<?=$htmlAry['CurrentDisplay']?>
