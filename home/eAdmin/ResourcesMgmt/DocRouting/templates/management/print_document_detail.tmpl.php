<?php
// Editing by 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html style="background-color: #ffffff">
<head>
<meta http-equiv='pragma' content='no-cache' />
<?= returnHtmlMETA() ?>
<title>eClass IP</title>
<style type='text/css' media='print'>
.print_hide {display:none;}
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #ffffff; background-image: none">
<style type="text/css">

html, body { margin: 0px; padding: 0px; } 

@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }
 
@media print
{
	
  table { page-break-after:auto }
  tr    { page-break-inside:avoid; page-break-after:auto }
  td    { page-break-inside:avoid; page-break-after:auto }
  thead { display:table-header-group }
  tfoot { display:table-footer-group }
  
  #footer {
         display:block;
		   position:fixed;
		   bottom:0px;
		   width:100%;
  } 
}

html, body, table, select, input, textarea{   font-family:Verdana, Arial, Helvetica, sans-serif, 新細明體;} 
html, body, table, select, input, textarea{ font-size:12px; }
html, body, table{ text-align:left;}

.common_table_list_v30{	border: 1px solid #666666; border-right:none; border-bottom:none}
.common_table_list_v30 tr th{	margin:0px;	padding:3px;	padding-top:5px;	padding-bottom:5px;	background-color: #A6A6A6;	font-weight: bold;	color: #FFFFFF;	text-align:left;	border-bottom: 1px solid #666666;		border-right: 1px solid #666666	;}
.common_table_list_v30 tr th{	background-color: #ffffff/* #71AA2F*/; color:#000000; border-bottom: 1px solid #666666;}
.common_table_list_v30 tr td{margin:0px;padding:5px 3px 5px 3px;background-color: #FFFFFF;	border-bottom: 1px solid #666666;	border-right: 1px solid #666666	;vertical-align:top;}
.common_table_list_v30{	width: 100%; margin:0 auto;	border-collapse:separate;	border-spacing: 0px; 	*border-collapse: expression('separate', cellSpacing = '0px');	clear:both;}

/*.Conntent_tool { display:none; }*/

p.breakhere {page-break-before: always}

</style>
<?php
echo $htmlAry['PrintPage'];
?>
</body>
</html>