<?php
// Editing by 

/*****************************************************************************
 * Modification Log:
 * 
 * Date:	2019-11-15 Sam: Create this file
 *****************************************************************************/
 ?>


<script type="text/javascript" language="JavaScript">

function CheckAll(CheckBoxType)
{
	var checked = $("input#CheckAll"+CheckBoxType).attr("checked");
	$("input."+CheckBoxType).attr("checked",checked);
}


function js_Submit_Form(){
	var NumOfChecked = 0;
	$('.Doc').each(function(){
		if($(this).is(':checked')){
			NumOfChecked++;
		}
	});
	
	
	if(NumOfChecked==0){
		alert(globalAlertMsg2);
	}else{
		$('#frm1').submit();
	}
	
}

</script>
<?php
    echo $ldocrouting_ui->getDeleteDocumentsStep1DisplayTable($pageType);
?>