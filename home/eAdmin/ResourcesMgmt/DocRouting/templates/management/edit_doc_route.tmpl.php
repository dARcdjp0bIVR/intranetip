<?php
// editing by 

/*******************************************
 * 2015-05-04 (Carlos): modified js clickedSelectViewOnlyUser(routingNumber) added &FromDR=1 to common_choose/index.php
 * 2013-09-30 (Henry): added function js_update_User_List_Div and selectAll
 * 2013-06-17 (Rita): add first route option text swap 
 * 2013-06-17 (Carlos): added js js_onChangeShowHideReplySlipUserOption()
 * 2013-02-15 (Rita): modified js - js_Submit_Form()
 * 			
 * 2013-01-30 (Carlos): modified validateReplySlipCsv() and js_Submit_View_ReplySlip() to handle different reply slip type
 * 
 * Date: 2013-01-28 (Rita)
 * Details: add js_Print_Instruction();
 ********************************************/
 
$documentID = $dataAry['DocumentID'];
$effectiveType ='';
if($documentID){
	$routes = $ldocrouting->getRouteData(array($documentID));
	if($routes[0]['RouteID']){
	$effectiveType = $routes[0]['EffectiveType'];
	}
}
?>
<script type="text/javascript" language="JavaScript">

$(document).ready(function(){
	js_Change_First_Route_Text();
	saveFormValues();
});	


function js_Change_First_Route_Text(){
	var routeNum = document.getElementsByName('routeCounter[]')[0].value;
	var content = '<?=$Lang['DocRouting']['StartRouteNow']?>';
	$('#optionFinishedSpan_' +routeNum).html(content);		
}

function js_Go_Back(){
	var saveFirst = false;
	
	if (isFormChanged()) {
		if (confirm('<?=$Lang['General']['JS_warning']['FormIsChanged']?>')) {
			saveFirst = true;
		}
	}
	
	if (saveFirst) {
		js_Submit_Form('<?=$dataAry['RecordStatus']?>', true);
	}
	else {
		//window.location='index.php';
		document.form1.action = 'index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','edit_doc',array())?>';
		document.form1.submit();
	}
}

function js_Document_Show(routingNumber){
		
	$('#VisiblePeriod_' + routingNumber).show();
	
}

function js_Document_Hide(routingNumber){
	
	$('#VisiblePeriod_' + routingNumber).hide();

	
}

function js_Extra_User_Option_Show(routingNumber){
	$('#VisibleAccessUsers_' + routingNumber).show();

}


function js_Extra_User_Option_Hide(routingNumber){

	$('#VisibleAccessUsers_' + routingNumber).hide();
}

function js_User_Option_Remove(routingNumber){
	$('#users_' + routingNumber)
    .find('option:selected')
    .remove()
    .end()
;
}

function js_Extra_User_Option_Remove(routingNumber){
	$('#extraUsers_' + routingNumber)
    .find('option:selected')
    .remove()
    .end()
;
}



function js_Enable_ReplySlip(routingNumber,isChecked)
{
	$('#ActionAllowReplySlip_'+routingNumber).attr('disabled',!isChecked);
	if(!isChecked){
		$('#ActionAllowReplySlip_'+routingNumber).attr('checked',false);
		js_Enable_Edit_ReplySlip(routingNumber,isChecked);
	}
}

function js_Enable_Edit_ReplySlip(routingNumber,isChecked)
{
	//$('#PreviewReplySlipBtn_'+routingNumber).attr('disabled',!isChecked);
	if(isChecked){
		//$('#PreviewReplySlipBtn_'+routingNumber).show();
		//$('#ReplySlipFile_'+routingNumber).show();
		$('div#routeReplySlipSettingsDiv_'+routingNumber).show();
	//	$('#releaseResultSettingsDiv_'+routingNumber).attr('style', 'padding-left:25px;');
	//	$('#showUserNameDisplaySettingsDiv_'+routingNumber).attr('style', 'padding-left:25px;');
		
	}else{
		//$('#PreviewReplySlipBtn_'+routingNumber).hide();
		//$('#ReplySlipFile_'+routingNumber).hide();
		$('div#routeReplySlipSettingsDiv_'+routingNumber).hide();
		$('#ReplySlipFile_'+routingNumber).val('');
		$('input#ReplySlipContent_'+routingNumber).val('');
	//	$('#releaseResultSettingsDiv_'+routingNumber).attr('style', 'display:none' );
	//	$('#showUserNameDisplaySettingsDiv_'+routingNumber).attr('style', 'display:none' );
	}
}

function js_Enable_Edit_Comment(routingNumber,isChecked)
{
	if(isChecked){
		$('div#ActionAllowCommentSettingsDiv_'+routingNumber).show();
	}else{
		$('div#ActionAllowCommentSettingsDiv_'+routingNumber).hide();
	}
}

function js_Enable_Edit_Attachment(routingNumber,isChecked){
	if(isChecked){
		$('div#ActionAllowAttachmentSettingsDiv_'+routingNumber).show();
	}else{
		$('div#ActionAllowAttachmentSettingsDiv_'+routingNumber).hide();
	}
}

function js_Add_New_Routing(DocumentID){

	var num = 1;
	//count num of routing 
	//$('.counter').each(function() {
 	//	num ++ ;
	//});
	
	var counters = document.getElementsByName('routeCounter[]');
	num = parseInt(counters[counters.length-1].value) + 1;
	
	$.ajax({
	url:      	"index.php?pe=<?=$ldocrouting->getEncryptedParameter('management','ajax_add_new_routing',array())?>",
	type:     	"POST",
	data:     	'&NumOfRouting='+num +'&DocumentID=' + DocumentID,
	async:		false,
	error:    	function(xhr, ajaxOptions, thrownError){
					alert(xhr.responseText);
			  	},
	success: function(data)
			 {		
				if(data)
				{	
				
					 $('#new_routing_display').append(data);
					 js_Reorder_RoutingNumber();
				}
			}
	 });		
}


function js_Reorder_RoutingNumber()
{
	var spans = $('.classRouteNumber');
	var num = 1;
	spans.each(function(i){
		$(this).html(num);
		num+=1;
	});
}

function js_Remove_Route(routeNumber)
{
	if(confirm('<?=$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisRouting']?>')){
		if($('.classRouteNumber').length == 1){
			alert('<?=$Lang['DocRouting']['WarningMsg']['ThereShouldBeAtLeastOneRouting']?>');
			return;
		}
		
		var routeID = $('#RouteID_'+routeNumber).val();
		if(routeID != ''){
			$(document.form1).append('<input type="hidden" name="ToRemoveRouteID[]" value="'+routeID+'" />');
		}
		$('#DivRoute_'+routeNumber).remove();
		js_Reorder_RoutingNumber();
	}
	
	js_Change_First_Route_Text();
}


function js_Submit_Form(recordStatus, backToStep1){
	var submit_btn = $('input#btnSubmit');
	var save_as_draft_btn = $('input#btnSaveAsDraft');
	var error = 0;
	var routeCounters = document.getElementsByName('routeCounter[]');
	//var routeCount = $('input[name=routeCounter[]]').length;
	var routingNum = routeCounters[0].value;
	var latest_datetime = '';
	
	submit_btn.attr('disabled','disabled');
	save_as_draft_btn.attr('disabled','disabled');
	//for(routingNum=1;routingNum<=routeCount;routingNum++){
	for(var i=0;i<routeCounters.length;i++){
		routingNum = routeCounters[i].value;
	  	
	 	//var foundOption =  document.getElementsByName('users_'+routingNum+'[]'); 
	 	// var foundOptionArr = foundOption.length;
	 	var foundOptionArr =  $('#users_'+ routingNum + ' option' ).length; 
	 
	 	
	 	var actionOptions = $('input[name="ActionAllow_'+routingNum+'[]"]:checked');
	 	var typePeriodChecked = $('input#EffectiveTypePeriod_'+routingNum).is(':checked');
	 	//var viewByUserChecked = $('input#ViewRightViewByUser_'+routingNum).is(':checked');
	 	var actionReplySlipChecked = $('input#ActionAllowReplySlip_'+routingNum).is(':checked');
	 	var replySlipContent = $.trim($('input#ReplySlipFile_'+routingNum).val());
	 	var replySlipId = $('input#replySlipId_' + routingNum).val();
	 	var lockChecked = $('input#EnableLock_' + routingNum).is(':checked');
	 	var lockDuration = $.trim($('input#LockDuration_' + routingNum).val());
	 	
	 	
	 	var copyReplySlip = $('#CopyReplySlip_' + routingNum).val();
	 	//alert(copyReplySlip);
	 
	 	if(foundOptionArr== 0){		
 			error++;
 			$('#UserWarningDiv_'+routingNum).show();
 		}else{
 			checkOptionAll(document.getElementById('users_'+routingNum));
 			$('#UserWarningDiv_'+routingNum).hide();
 		}
 		
 		if(actionOptions.length == 0){
 			error++;
 			$('#ActionWarningDiv_'+routingNum).show();
 		}else{
 			$('#ActionWarningDiv_'+routingNum).hide();
 		}
 	
 		if (actionReplySlipChecked) {
 			if (replySlipId == '' && replySlipContent == '' && typeof copyReplySlip=='undefined' ) {
 				error++;
 				$('#ActionAllowReplySlipWarningDiv_'+routingNum).show();
 			}
 			else if (isReplySlipCsvWarningShown(routingNum)){	// csv valid or not checked in onchange of file input
 				error++;
 				$('#ActionAllowReplySlip_'+routingNum).focus();
 			}
 		}
 		else {
 			$('#ActionAllowReplySlipWarningDiv_'+routingNum).hide();
 		}
 		
// 		if(i==0){
// 			if($('#EffectiveTypePrevRouteFinished_'+routingNum).is(':checked')){
// 				$('#EffectiveTypePrevRouteWarningDiv_'+routingNum).show();
// 				error++;
// 			}else{
// 				$('#EffectiveTypePrevRouteWarningDiv_'+routingNum).hide();
// 			}
// 		}
 		if(typePeriodChecked){
 			var startDateObj = document.getElementById('EffectiveStartDate_'+routingNum);
 			var endDateObj = document.getElementById('EffectiveEndDate_'+routingNum);
 			var startDate = $.trim(startDateObj.value);
 			var endDate = $.trim(endDateObj.value);
 			var startTimeHr = $('#EffectiveStartTime_'+routingNum+'_hour').val();
 			startTimeHr = parseInt(startTimeHr)<10? '0'+startTimeHr : startTimeHr;
 			var startTimeMin = $('#EffectiveStartTime_'+routingNum+'_min').val();
 			startTimeMin = parseInt(startTimeMin)<10? '0'+startTimeMin : startTimeMin;
 			var startTimeSec = $('#EffectiveStartTime_'+routingNum+'_sec').val();
 			startTimeSec = parseInt(startTimeSec)<10? '0'+startTimeSec : startTimeSec;
 			var startTime = startTimeHr + ':' + startTimeMin + ':' + startTimeSec;
 			var endTimeHr = $('#EffectiveEndTime_'+routingNum+'_hour').val();
 			endTimeHr = parseInt(endTimeHr)<10? '0'+endTimeHr : endTimeHr;
 			var endTimeMin = $('#EffectiveEndTime_'+routingNum+'_min').val();
 			endTimeMin = parseInt(endTimeMin)<10? '0'+endTimeMin : endTimeMin;
 			var endTimeSec = $('#EffectiveEndTime_'+routingNum+'_sec').val();
 			endTimeSec = parseInt(endTimeSec)<10? '0'+endTimeSec : endTimeSec;
 			var endTime = endTimeHr + ':' + endTimeMin + ':' + endTimeSec;
 			var startDateTime = startDate + ' ' + startTime;
 			var endDateTime = endDate + ' ' + endTime;
 			
 			if(!check_date_without_return_msg(startDateObj) || !check_date_without_return_msg(endDateObj) || startDate > endDate){
 				error++;
	 			$('#EffectiveTypeWarningDiv_'+routingNum).html('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange']?>').show();
 			}else{
 				if(startDateTime >= endDateTime){
 					error++;
 					$('#EffectiveTypeWarningDiv_'+routingNum).html('<?=$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange']?>').show();
 				}else if(latest_datetime != '' && startDateTime <= latest_datetime){
 					error++;
 					var msg = '<?=$Lang['DocRouting']['WarningMsg']['PleaseSetStartDateAfterDateTime']?>';
					msg = msg.replace('<!--DATETIME-->',latest_datetime);
 					$('#EffectiveTypeWarningDiv_'+routingNum).html(msg).show();
 				}
 				else{
 					$('#EffectiveTypeWarningDiv_'+routingNum).hide();
 				}
 				latest_datetime = endDateTime;
 			}
 		}
 		
// 		if(viewByUserChecked){
// 			//var numExtraUsers = $('select[name=extraUsers_'+routingNum+'[]] option').length;
// 			var numExtraUsers =  $('#extraUsers_'+ routingNum + ' option' ).length;
//
// 			if(numExtraUsers == 0){
// 				error++;
// 				$('#ExtraUserWarningDiv_'+routingNum).show();
// 			}else{
// 				//checkOptionAll(document.getElementById('extraUsers_'+routingNum+'[]'));
// 				checkOptionAll(document.getElementById('extraUsers_'+routingNum));
// 				$('#ExtraUserWarningDiv_'+routingNum).hide();
// 			}
// 		}
		checkOptionAll(document.getElementById('extraUsers_'+routingNum));
 		
 		if(lockChecked) {
 			if(lockDuration == '' || !is_positive_int(lockDuration) || lockDuration == 0) {
 				error++;
 				$('#LockDurationWarningDiv_' + routingNum).show();
 			}else{
 				$('#LockDurationWarningDiv_' + routingNum).hide();
 			}
 		}
	}
	
	if(error == 0){
		if (backToStep1) {
			$('input#BackToStep1').val('1');
		}
		$('input#RecordStatus').val(recordStatus);
		$('form').submit();
	}else{
		submit_btn.attr('disabled','');
		save_as_draft_btn.attr('disabled','');
	}
}

function validateReplySlipCsv(routingNumber) {
	var fileInputId = 'ReplySlipFile_' + routingNumber;
	var fileVal = $.trim($('#' + fileInputId).val());
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber; 
	var replySlipType = $('input[name="ReplySlipType_'+routingNumber+'"]:checked').val();	
	if (fileVal == '') {
		hideReplySlipCsvInvalidWarning(routingNumber);
	}
	else {
		var targetFrameId = 'replySlipIFrame_' + routingNumber;
		var formObj = document.form1;
		var oldTarget = document.form1.target;
		var oldAction = document.form1.action;
		
		formObj.target = targetFrameId;
		formObj.action = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('management','ajax_validate_reply_slip_csv')?>' + '&ReplySlipFileField=' + fileInputId + '&routingNumber=' + routingNumber + '&ReplySlipType=' + replySlipType;
		formObj.submit();
		formObj.target = oldTarget;
		formObj.action = oldAction;
	}
}

function showReplySlipCsvInvalidWarning(routingNumber) {
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber;
	$('div#' + warningDivId).show();
}
function hideReplySlipCsvInvalidWarning(routingNumber) {
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber;
	$('div#' + warningDivId).hide();
}
function isReplySlipCsvWarningShown(routingNumber) {
	var warningDivId = 'ReplySlipCsvInvalidWarningDiv_' + routingNumber;
	return $('div#' + warningDivId).is(':visible');
}

function js_Submit_View_ReplySlip(routingNumber, replySlipId)
{
	
	replySlipId = replySlipId || '';

	var formObj = document.form1;
	var oldTarget = document.form1.target;
	var oldAction = document.form1.action;
	var fileVal = $.trim($('#ReplySlipFile_'+routingNumber).val());
	var replySlipType = $('input[name="ReplySlipType_'+routingNumber+'"]:checked').val();
	var listTypeLink = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('common','reply_slip_form',array())?>'+'&ReplySlipFileField=ReplySlipFile_'+routingNumber+'&replySlipId='+replySlipId;
	var tableTypeLink = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('common','table_reply_slip_form',array())?>'+'&ReplySlipFileField=ReplySlipFile_'+routingNumber+'&replySlipId='+replySlipId;
	if(fileVal == '' && replySlipId == ''){
		alert('<?=$Lang['DocRouting']['WarningMsg']['PleaseUploadReplySlipFile']?>');
	}else{
		formObj.target = '_blank';
		formObj.action = replySlipType=='<?=$docRoutingConfig['INTRANET_DR_ROUTES']['ReplySlipType']['Table']?>'?tableTypeLink:listTypeLink;
		formObj.submit();
		formObj.target = oldTarget;
		formObj.action = oldAction;
	}
}

//function js_Change_Selection(routingNumber){
//	var OptionValue = $("#presetNotesSelection_" + routingNumber).val();
//	var OptionText = $("#presetNotesSelection_"+ routingNumber +" option[value='"+OptionValue+"']").text(); 
//	var Editor1 = FCKeditorAPI.GetInstance('Note_' + routingNumber);
//	
//	if(OptionValue!=''){	
//		Editor1.SetHTML(OptionText);	     
//	} 
//	else{
//		Editor1.SetHTML('');	
//	}
//	
//	return false; 
//}

function js_Enable_Lock(obj, timeoutDivId)
{
	var elem = $(obj);
	
	if(elem.is(':checked')) {
		$('#'+timeoutDivId).show();
	} else {
		$('#'+timeoutDivId).hide();
	}
}


//function js_Print_Instruction(routeNum){
//	var action = 'index.php?pe='+'<?=$ldocrouting->getEncryptedParameter('management','print_instruction')?>'+'&routeNum='+routeNum;
//	newWindow(action, '10');
//	return false;		
//}

function clickedSelectViewOnlyUser(routingNumber) {
	var selectedRouteUserObj = document.getElementById('users_' + routingNumber);
	
	var excludeUserIdPara = '';
	$('select#users_' + routingNumber + ' option').each( function () {
		excludeUserIdPara += "exclude_user_id_ary[]=" + $(this).val() + "&";
	});
	excludeUserIdPara = excludeUserIdPara.substr(0,(excludeUserIdPara.length - 1));
	
	newWindow('/home/common_choose/index.php?fieldname=extraUsers_' + routingNumber + '[]&ppl_type=pic&permitted_type=1&excluded_type=4&DisplayGroupCategory=1&DisplayInternalRecipientGroup=1&Disable_AddGroup_Button=1'+excludeUserIdPara<?=($indexVar['drUserIsAdmin']?"+'&FromDR=1'":"")?>, 9);
}

function js_onChangeShowHideReplySlipUserOption(showHide, targetObjId, targetObjId2, targetObjId3, is_disable)
{
	if(showHide == '<?=$replySlipConfig['INTRANET_REPLY_SLIP']['ShowUserInfoInResult']['hide']?>'){
		if(is_disable){
			$('#'+targetObjId).attr('checked',true);
			$('#'+targetObjId2).attr('disabled',true);
			$('#'+targetObjId3).attr('disabled',true);
		}
		else{
			$('#'+targetObjId2).attr('disabled',false);
			$('#'+targetObjId3).attr('disabled',false);
		}
	}
}
//Henry Added
function js_update_User_List_Div(user_ddl,userlist_div){
	var fld = document.getElementById(user_ddl);
	selectAll(fld,true);
	var values = "";
	for (var i = 0; i < fld.options.length; i++) {
	  if (fld.options[i].selected && fld.options[i].text.replace(/^\s+|\s+$/g, '') != "") {
	    values += fld.options[i].text+", ";
	  }
	}
	values = values.substring(0, values.length - 2);
	$("#"+userlist_div).text(values);
	selectAll(fld,false);
}
function selectAll(selectBox,selectAll) { 
        for (var i = 0; i < selectBox.options.length; i++) { 
             selectBox.options[i].selected = selectAll; 
        }
}
</script>
<?php


echo $ldocrouting_ui->getDocRoutingEditForm($indexVar['dataAry']);


?>
