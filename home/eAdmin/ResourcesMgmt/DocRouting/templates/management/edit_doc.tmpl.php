<?php
// Editing 
/*
 * Change Log:
 *  2014-12-02 [Carlos]: Modified js_Select_PresetNotes() make a dummy call Editor1.InsertHtml('') to cater webkit browser(e.g. Chrome) first time no focus problem
 *  2014-02-18 [Tiffany]: Add js_New_Level(),js_show_file_DA(), modified js_Select_DocumentType()
 *	2013-09-27 [Henry]: Modified js_Select_PresetNotes to append the template in the fckEditor
 *
 */
?>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js" type="text/javascript" charset="utf-8"></script>-->
<script src="/templates/jquery/jquery.inputselect.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.inputselect.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" />
<!--<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">-->

<script type="text/javascript" language="JavaScript">
var DocumentTypeFile = '<?=$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['File']?>';
var DocumentTypeLocation = '<?=$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']?>';
var DocumentTypeNA = '<?=$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['NA']?>';
var fileNumber = 1;
 
function js_Select_PresetNotes()
{
	// load presets via ajax...
//	var OptionValue = $("#PresetNotes").val();
//	var OptionText = $("#PresetNotes option[value='"+OptionValue+"']").text(); 
//	var Editor1 = FCKeditorAPI.GetInstance('Instruction');
//	
//	if(OptionValue!=''){	
//		Editor1.SetHTML(OptionText);
//	} 
//	else{
//		Editor1.SetHTML('');	
//	}
	
	var OptionValue = $("#PresetNotes").val();
	var Editor1 = FCKeditorAPI.GetInstance('Instruction');
	Editor1.InsertHtml('');
	if (OptionValue != ''){
		$.post(
			"index.php", 
			{ 
				pe: '<?=$htmlAry['ajaxGetPresetContentPath']?>',
				PresetID: OptionValue
			},
			function(ReturnData) {
				//Editor1.SetHTML(ReturnData);
				Editor1.InsertHtml(ReturnData+"<br/>");
			}
		);
	}
}

function js_Select_DocumentType()
{
	if($('input#DocumentType1').is(':checked')){
		$('div#DocUploadDiv').show();
	}else{
		$('div#DocUploadDiv').hide();
	}
	
	if($('input#DocumentType4').is(':checked')){
		$('div#DigitalArchiveDiv').show();
	}else{
		$('div#DigitalArchiveDiv').hide();
	}
	
	if($('input#DocumentType2').is(':checked')){
		$('div#PhysicalLocationDiv').show();
		$('input#PhysicalLocationTb').focus();
	}else{
		$('div#PhysicalLocationDiv').hide();
	}
	
	
}

function js_Add_More_Upload(obj)
{
	var addObj = $(obj);
	addObj.before('<div><input type="file" name="DocumentUploaded_'+fileNumber+'" size="80" style="float:left" /><span class="table_row_tool" style="float:left"><a onclick="js_Remove_Upload(this);" href="javascript:void(0);" class="delete_dim" title="<?=$Lang['Btn']['Delete']?>"></a></span><br style="clear:both;" /></div>');
	fileNumber+=1;
}

function js_Remove_Upload(obj)
{
	var removeObj = $(obj);
	removeObj.closest('div').remove();
}

function js_Delete_File(obj,fileID)
{
	if(confirm('<?=$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisFile']?>')){
		var thisObj = $(obj);
		$('div#DocUploadDiv').append('<input type="hidden" name="DeleteFileID[]" value="'+fileID+'" />');
		thisObj.closest('div').remove();
	}
}

function js_Get_FCKEditor_Value(instanceName) 
{
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;

	// Get the editor contents as XHTML.
	return oEditor.GetXHTML( true ) ; // "true" means you want it formatted.
}

function js_Check_Submit_Form(formObj)
{	
	var submit_btn = $('input[type=submit]');
	var title = $.trim($('input#Title').val());
	var instruction = getEditorValue('Instruction');
	instruction = $.trim(instruction.replace(/(<br \/>)|(<br>)|(&nbsp;)/gi,''));
	var documentType = $('input[name=DocumentType]:checked').val();
	var docTags = $.trim($('input#DocTags').val());
	var isValid = true;
	var isFocused = false;
	
	submit_btn.attr('disabled','disabled');
	if(title == ''){
		$('#TitleWarningDiv').show();
		isValid = false;
		
		if (!isFocused) {
			$('input#Title').focus();
		}
	}else{
		$('#TitleWarningDiv').hide();
	}
	
	if(instruction == ''){
		$('#InstructionWarningDiv').show();
		isValid = false;
		
		if (!isFocused) {
			$('select#PresetNotes').focus();
		}
	}else{
		$('#InstructionWarningDiv').hide();
	}
	
	if (documentType == '<?=$docRoutingConfig['INTRANET_DR_ENTRY']['DocumentType']['Location']?>' && Trim($('input#PhysicalLocationTb').val())=='') {
		$('div#PhysicalLocationWarningDiv').show();
		isValid = false;
		
		if (!isFocused) {
			$('input#PhysicalLocationTb').focus();
		}
	}
	else {
		$('div#PhysicalLocationWarningDiv').hide();
	}
	
	var docTagsAry = docTags.split(',');
	if (docTagsAry.length > parseInt('<?=$docRoutingConfig['maxNumOfTags']?>')) {
		$('#TagsWarningDiv').show();
		isValid = false;
		
		if (!isFocused) {
			$('input#DocTags').focus();
		}
	}
	else {
		$('#TagsWarningDiv').hide();
	}
	
	if(isValid){
		formObj.submit();
	}else{
		submit_btn.attr('disabled','');
		
	}
}
$().ready( function(){
	$("input.inputselect").inputselect({
		image_path: '/images/2009a/', 
		ajax_script: 'index.php?pe=<?=$indexVar['libDocRouting']->getEncryptedParameter('management','ajax_location_suggestion',array())?>', 
		js_lang_alert: {'no_records' : '<?=$Lang['libms']['NoRecordAtThisMoment']?>' }
	});
});
function js_Set_Input_Width(){
	var divObj = $('#PhysicalLocationTb_inputselect_div');
	$('#PhysicalLocationTb_selection_list').width(divObj.width());
	
}
function js_New_Level(groupID,folderID){
 if(folderID==0){
    obj=document.getElementById(groupID);
    if(obj.innerHTML=="&nbsp;&nbsp;+&nbsp;&nbsp;"){
       if($('div#dg_'+groupID).length>0){
          $('div#dg_'+groupID).show();
          obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
       }else{
       $.post("index.php?pe=<?=$indexVar['libDocRouting']->getEncryptedParameter('management','ajax_get_DA',array())?>",
              {groupID:groupID,folderID:folderID},
              function(array){
              obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
              $('p#pg_'+groupID).after(array);
              }
             );
       }
    }else{
    obj.innerHTML="&nbsp;&nbsp;+&nbsp;&nbsp;";
    $('div#dg_'+groupID).hide();
    }
 }else{
    obj=document.getElementById(folderID);
    if(obj.innerHTML=="&nbsp;&nbsp;+&nbsp;&nbsp;"){
       if($('div#df_'+folderID).length>0){
          $('div#df_'+folderID).show();
          obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
       }else{
       $.post("index.php?pe=<?=$indexVar['libDocRouting']->getEncryptedParameter('management','ajax_get_DA',array())?>",
              {groupID:groupID,folderID:folderID},
              function(array){
              obj.innerHTML="&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;";
              $('p#pf_'+folderID).after(array);
              }
             );
       }     
    }else{
    obj.innerHTML="&nbsp;&nbsp;+&nbsp;&nbsp;";
    $('div#df_'+folderID).hide();
    }
 }    
}

function js_show_file_DA(){
	
	
var chosen_file =[];    
$('input[name="chosen_file[]"]:checked').each(function(){    
  chosen_file.push($(this).val());    
  });  	
if(chosen_file.length!=0){
	tb_remove();
if($('div#DigitalArchiveDiv_showfile').length>0){
   $('div#DigitalArchiveDiv_showfile').html("");
   for(var i=0;i<chosen_file.length;i++){
      $('div#DigitalArchiveDiv_showfile').append('<p>'+$('span#s_'+chosen_file[i]).text()+'</p>');
   }
}
else{
   $('div#DigitalArchiveDiv').prepend('<div id="DigitalArchiveDiv_showfile"></div>');
   for(var i=0;i<chosen_file.length;i++){
      $('div#DigitalArchiveDiv_showfile').append('<p>'+$('span#s_'+chosen_file[i]).text()+'</p>');
   }
}
}
else{
alert("<?=$Lang['DigitalArchiveModuleUpload']['WarningMsg']['PleaseSelectAtLeastOneFile']?>");
}
}


</script>

<?php
echo $ldocrouting_ui->getDocEditForm($indexVar['dataAry']);
?>