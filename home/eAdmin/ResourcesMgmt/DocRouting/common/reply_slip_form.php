<?php
// using: ivan
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/replySlip/libReplySlipMgr.php");

$replySlipFileField = $_GET['ReplySlipFileField'];
$replySlipId = $_GET['replySlipId'];

$libimport = new libimporttext();
$libReplySlipMgr = new libReplySlipMgr();
$libReplySlipMgr->setReplySlipId($replySlipId);

$indexVar['libDocRouting_ui']->echoModuleLayoutStart('', '', $forPopup=true);

$navigationAry = array();
$navigationAry[] = array($Lang['DocRouting']['PreviewReplySlip'], "");
$htmlAry['navigation'] = $indexVar['libDocRouting_ui']->GET_NAVIGATION_IP25($navigationAry);

if ($replySlipId) {
	$libReplySlipMgr->setReplySlipId($replySlipId);
	$html['replySlip'] = $libReplySlipMgr->returnReplySlipHtml($forCsvPreview=false, $showUserAnswer=false, $disabledAnswer=true);
}
else {
	$libReplySlipMgr->setCsvFilePath($_FILES[$replySlipFileField]['tmp_name']);
	$libReplySlipMgr->setCurUserId($indexVar['drUserId']);
	$html['replySlip'] = $libReplySlipMgr->returnReplySlipHtmlByCsvData();
}

?>
<br />
<?=$htmlAry['navigation']?>
<br />
<?=$html['replySlip']?>
<?php
$indexVar['libDocRouting_ui']->echoModuleLayoutStop();
?>