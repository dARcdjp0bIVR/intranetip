<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->allowViewBudgetReport)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinvoice_ui.php");

$linterface = new interface_html();
$linvoice_ui = new libinvoice_ui();

$allCategory = $linvoice->Get_All_Category();
$adminGroup = $linvoice->returnAdminGroup();
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
$lfcm = new form_class_manage();
$AcademicYearAry = $lfcm->Get_Academic_Year_List('', $OrderBySequence, $excludeYearIDArr, $noPastYear, $pastAndCurrentYearOnly, $excludeCurrentYear);


$data = $linvoice->Load_Budget_Data($AcademicYearID, $groups);
//debug_pr($_POST);
if($flag == "year") {
	$content = $linvoice_ui->Load_Year_Budget_Report($data, $AcademicYearID[0], $groups, $view=1);
} else if($flag == "group") {
	$content = $linvoice_ui->Load_Group_Budget_Report($data, $groups[0], $AcademicYearID, $view=1);
}

intranet_closedb();

echo $content;
?>