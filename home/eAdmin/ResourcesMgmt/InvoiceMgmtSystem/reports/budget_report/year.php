<?
//Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->allowViewBudgetReport)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

$linterface = new interface_html();

if($AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();

### Title ###
$TAGS_OBJ[] = array($Lang['Invoice']['YearReport'], "year.php", 1);
$TAGS_OBJ[] = array($Lang['Invoice']['GroupReport'], "group.php", 0);
$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage = "Reports_BudgetReport";
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();

# Resources Mgmt Group filter
$yearFilter = getSelectAcademicYear("AcademicYearID[]", '', 1, 0, $AcademicYearID);

# academic year filter
if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {
	$adminGroup = $linvoice->returnAdminGroup();
} else {
	$adminGroup = $linvoice->returnAdminGroup($UserID, $returnIsLeader=1);
}
$groupFilter = getSelectByArray($adminGroup, ' name="groups[]" id="groups" multiple size=5', $groups, 1, 1);

# Select All button
$selectAllBtn = $linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "SelectAll('groups')");

$linterface->LAYOUT_START();

$optionString2 = "<td id=\"tdOption\"><span id=\"spanShowOption\" style=\"display:none\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
$optionString2 .= "<span id=\"spanHideOption\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";


echo $linterface->Include_JS_CSS();

?>
<script language="javascript">
function checkForm() {
	
	reset_innerHtml();
	
	var item = document.getElementById('groups');
	var itemLen = item.length; 
	var c = 0;
	var obj = document.form1;
	var error_no = 0;
	
	
	for(var i=0; i<itemLen; i++) {
		if(document.getElementById('groups').options[i].selected == true) {
			c++;
		}
	}
	
	if(c==0)
	{ 
		document.getElementById('div_groups_err_msg').innerHTML = '<font color="red"><?= $i_alert_pleaseselect." ".$Lang['Invoice']['Groups']?></font>';
		error_no++;
	}
	
	if(error_no>0)
	{
		return false;
	} else {
		generateContent();
	}
	
}

function reset_innerHtml()
{
 	document.getElementById('div_groups_err_msg').innerHTML = "";
}

function SelectAll(emt) {
	var item = document.getElementById(emt);
	checkOptionAll(item);
	
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
}

var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var js_PATH_WRT_ROOT = "<?=$PATH_WRT_ROOT?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";

function generateContent() {
	initThickBox();
	$("#ContentDiv").html(loading);
	$('#flag').val('year');
	
	var PostVar = Get_Form_Values(document.getElementById("form1"));
	
	$.post(
		"ajax_generate_report.php", PostVar,
		function(ReturnData){
			if(ReturnData != "") 
				$("#ContentDiv").html(ReturnData);
			else 
				$("#ContentDiv").html('');
		}
	)
	
	hideOption();
}

function goExport() {
	$('#flag').val('year');
	document.form1.action = "export.php";
	document.form1.submit();
}

function Display_Detail(year_id, group_id, cat_id) {
	initThickBox();
	
	$.post(
		"ajax_display_detail.php",
		{
			AcademicYearID: year_id,
			GroupID: group_id,
			CategoryID: cat_id	
		} 
		,
		function(ReturnData){
			$('#TB_ajaxContent').html(ReturnData);	
		}
	)

}


</script>

<br style="clear:both">
<form name="form1" id="form1" method="post" action="export.php">

<table width="100%" border="0" cellspacing="0" cellpadding="3">
	<tr align="left" id="option2">
		<?=$optionString2?>
	</tr>
</table>

<span id="spanOptionContent">
<table class="form_table_v30" style="width:700px;">
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['General']['AcademicYear']?></td>
		<td class="row_content">
			<?=$yearFilter?>
		</td>
	</tr>
	<tr>
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['Groups']?></td>
		<td class="row_content">
			<?=$groupFilter?>
			<?=$selectAllBtn?>
			<div id="div_groups_err_msg"></div>
		</td>
	</tr>
</table>
<span class="tabletextremark">
	<?=$Lang['General']['RequiredField']?>
</span>
<p class="spacer"></p>
<div class="edit_bottom_v30" style="width:700px;">
	<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "checkForm()")?>
</div>
</span>

<br style="clear:both">
<br style="clear:both">

<div id="ContentDiv"></div>

<input type="hidden" name="flag" id="flag" value="year">

</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>