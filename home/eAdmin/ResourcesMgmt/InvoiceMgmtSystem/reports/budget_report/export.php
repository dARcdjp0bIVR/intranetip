<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinvoice_ui.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->allowViewBudgetReport)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



$linterface = new interface_html();
$linvoice_ui = new libinvoice_ui();
$lexport = new libexporttext();

$filename = "budget_report_$flag.csv";

if($flag != "detail") {
	$data = $linvoice->Load_Budget_Data($AcademicYearID, $groups);

	$allCategory = $linvoice->Get_All_Category();
	$adminGroup = $linvoice->returnAdminGroup();
}


if($flag == "year") {
	$exportContent = $linvoice_ui->Load_Year_Budget_Report($data, $AcademicYearID[0], $groups, $view=2);
} else if($flag == "group") {
	$exportContent = $linvoice_ui->Load_Group_Budget_Report($data, $groups[0], $AcademicYearID, $view=2);
} else if($flag == "detail") {
	$exportContent = $linvoice_ui->Display_Budget_Detail($AcademicYearID, $GroupID, $CategoryID, $view=2);	
}

/*
if ($g_encoding_unicode) {
	
	$Temp = explode("\n", urldecode($exportContent));
	$exportColumn = explode("\t", trim($Temp[0], "\r"));
	for ($i = 1; $i < sizeof($Temp); $i++) {
		$result[] = explode("\t", trim($Temp[$i], "\r"));
	}
	
	$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
	
} else {
*/
	$export_content .= ($exportContent);	
	$lexport->EXPORT_FILE($filename, $export_content);
//}

intranet_closedb();
?>