<?php
//Modifying by: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$linvoice = new libinvoice();

$linterface         = new interface_html();
$CurrentPage = "Reports_DeletionLog";



$thisModule = strtoupper($linvoice->Module);
$StartDate = $StartDate ? $StartDate : date("Y-m-d");
$EndDate = $EndDate ? $EndDate : date("Y-m-d");

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if($field=="") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.LogDate");

$name_field = getNameFieldWithClassNumberByLang("b.");
$sql = "select 
		left(a.LogDate,10),
		$name_field as logby,
		a.Section,
		a.RecordDetail
		from 
			MODULE_RECORD_DELETE_LOG as a 
			INNER join INTRANET_USER as b on (b.UserID = a.LogBy)
		where
			left(a.LogDate,10) >= '$StartDate' and left(a.LogDate,10) <= '$EndDate'
			and a.Module = '". $linvoice->Module."'
		";

$li->sql = $sql;
$li->no_col = 4;
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$li->column_list .= "<th>".$Lang['Invoice']['DeletedDate']."</th>\n";
$li->column_list .= "<th>".$Lang['Invoice']['DeletedBy']."</th>\n";
$li->column_list .= "<th>".$Lang['Invoice']['RecordType']."</th>\n";
$li->column_list .= "<th>".$Lang['Invoice']['RecordInfo']."</th>\n";

### Button
$delBtn = "<a href=\"javascript:clickDelete()\" class=\"tool_delete\">" . $Lang['eDiscipline']['DeleteRecordsInDateRange'] . "</a>";

### Filter - Date Range
$date_select = $eNotice['Period_Start'] .": ";
$date_select .= $linterface->GET_DATE_PICKER("StartDate",$StartDate);
$date_select .= $eNotice['Period_End']."&nbsp;";
$date_select .= $linterface->GET_DATE_PICKER("EndDate",$EndDate);
$date_select .= $linterface->GET_BTN($button_submit, "submit");

### Title ###
$TAGS_OBJ[] = array($Lang['Invoice']['DeletionLog'],"../del_log/",1);
$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
<!--//
function clickDelete()
{
	document.form1.action = "remove_update.php";
	AlertPost(document.form1, "remove_update.php", "<?=$Lang['eDiscipline']['ConfirmDeleteRecordsInDateRange']?>");
}

function check_form()
{
	obj = document.form1;
	
	if(!check_date(obj.StartDate,"<?=$i_invalid_date?>"))
	{
		obj.StartDate.focus();
		return false;
	}
	
	if(!check_date(obj.EndDate,"<?=$i_invalid_date?>"))
	{
		obj.EndDate.focus();
		return false;
	}
	
	if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
		obj.StartDate.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}	
	
	return true;
}
//-->
</SCRIPT>


<form name="form1" method="get" action="index.php" onSubmit="return check_form();">

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom" class="table-action-bar">
	<?=$date_select?>
	</td>
	<td valign="bottom" class="table-action-bar">
	<div class="common_table_tool">
		<?=$delBtn?>
	</div>
	</td>
</tr>
</table>
</div>

<?=$li->display();?>


<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

