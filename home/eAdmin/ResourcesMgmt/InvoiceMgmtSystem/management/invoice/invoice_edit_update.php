<?php
// modifying : 

############ Change Log Start ###############
#
#	Date	:	2020-05-07 Tommy
#				modified Attachment checking, since empty(attachment) result will not be true if there is no attachment
#               change to checking attachment size and error
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linvoice		= new libinvoice();

# retrieve data 
if(empty($InvoiceNo))
{
	$InvoiceNo = $linvoice->GenerateInvoiceNumber($InvoiceDate);
}
$DiscountAmount = $DiscountAmount ? $DiscountAmount : 0;
$item_funding = $item_funding ? $item_funding : 0;
if(!empty($pic))
{
	$pic_str = implode(",",$pic);
	$pic_str = str_replace("U","",$pic_str);
	$pic_sql = "PIC = '$pic_str',";
}
//if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'])

$sql = "update INVOICEMGMT_INVOICE set
			AcademicYearID = $YearID,
			InvoiceDate = '$InvoiceDate',
			Company = '$InvoiceCompany',
			InvoiceNo = '$InvoiceNo',
			InvoiceDescription = '$InvoiceDescription',
			DiscountAmount = '$DiscountAmount',
			TotalAmount = '$TotalAmount',
			AccountDate = '$AccountDate',
			FundingSource = '$item_funding',
			Remarks = '$Remarks',
			$pic_sql
			DateModified = now(),
			ModifyBy = '$UserID'
		where 
			RecordID=$RecordID";
$linvoice->db_db_query($sql);

### Attachment
// $attach = $_FILES['att'];
// if(!empty($attach))
// {
// 	# check dir
// 	$path = "$file_path/file/InvoiceMgmtSysAtt/".$RecordID;
// 	$lf = new libfilesystem();
	
// 	$att_size = sizeof($attach['name']);
	
// 	for($i=0;$i<$att_size;$i++)
// 	{
// 		$filename = stripslashes($attach['name'][$i]);
// 		if(!empty($filename))
// 		{
// 			if (!is_dir($path))
// 			{
// 				$lf->folder_new($path);
// 			}
	
// 			$target = "$path/".$filename;		
// 			$tmp_file = $attach['tmp_name'][$i];
// 			$lf->lfs_copy($tmp_file, $target);
// 		}
// 	}
// }

### Attachment
$attach = $_FILES['att'];

# check dir
$path = "$file_path/file/InvoiceMgmtSysAtt/".$RecordID;
$lf = new libfilesystem();
    
$att_size = sizeof($attach['name']);
    
for($i=0;$i<$att_size;$i++)
{
    if($attach["size"][$i] > 0 && $attach["error"][$i] == 0)
    {
        $filename = stripslashes($attach['name'][$i]);
        if(!empty($filename))
        {
            if (!is_dir($path))
            {
                $lf->folder_new($path);
            }
            
            $target = "$path/".$filename;
            $tmp_file = $attach['tmp_name'][$i];
            $lf->lfs_copy($tmp_file, $target);
        }
    }
}

intranet_closedb();

header("location: view.php?RecordID=$RecordID&msg=UpdateSuccess");	


?>

