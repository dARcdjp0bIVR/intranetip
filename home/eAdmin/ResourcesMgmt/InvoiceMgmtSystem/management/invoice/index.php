<?php
// modifying : 

############ Change Log Start ###############
#
#	Date	:	2017-07-13 (Pun)
#				add filter for export
#
#	Date	:	2017-07-10 (Pun)
#				add filter academic year for PIC filter selection
#
#	Date	:	2011-12-15 (Henry Chow)
#				add "Mgmt Group" filter
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Management_InvoiceList";
$linterface 	= new interface_html();
$linvoice		= new libinvoice();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($Lang['Invoice']['Invoice']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);


if(!isset($AcademicYearID) || $AcademicYearID=="")
    $AcademicYearID = Get_Current_Academic_Year_ID();

if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {
	$picAry = $linvoice->ReturnAllPicAry($AcademicYearID);
	$picSelect = getSelectByArray($picAry, 'name="pic" id="pic" onChange="document.form1.action=\'index.php\';document.form1.submit();"', $pic, 0, 0, '- '.$Lang['Invoice']['AllPic'].' -');
	
	if(isset($pic) && $pic!="") $conds .= " AND CONCAT(',',i.PIC,',') LIKE '%,$pic,%'";
}	
		
if(isset($AcademicYearID))
	$conds .= " AND AcademicYearID = '$AcademicYearID'";	

# mgmt group
$sql = "SELECT AdminGroupID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
$arr_group = $linventory->returnArray($sql,2);
$group_selection = getSelectByArray($arr_group, ' name="targetGroup" onChange="this.form.submit();" ', $targetGroup,1,0,$Lang['eInventory']['FieldTitle']['AllResourceMgmtGroup']);


$dbtable = " LEFT JOIN INVOICEMGMT_ITEM t ON (t.InvoiceRecordID=i.RecordID AND t.RecordStatus=".RECORDSTATUS_ACTIVE.") LEFT JOIN INVENTORY_ADMIN_GROUP_MEMBER m ON (m.AdminGroupID=t.ResourceMgmtGroup AND m.UserID='$UserID')";

if(isset($targetGroup) && $targetGroup!="") {
	$conds .= " AND t.ResourceMgmtGroup='$targetGroup'";	
}

$keyword = trim($keyword);
if(isset($keyword) && $keyword!="") {
	$conds .= " AND (i.InvoiceDate LIKE '$keyword' OR i.Company LIKE '%$keyword%' OR i.InvoiceNo LIKE '%$keyword%' OR i.InvoiceDescription LIKE '%$keyword%' OR i.TotalAmount='$keyword')";	
}

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$linvoice->isViewerGroupMember) {
	$group_arr = $linvoice->returnAdminGroup($UserID, $leader=1);
	
	$ary = array();
	for($i=0; $i<count($group_arr); $i++) {
		$ary[] = $group_arr[$i][0];	
	}
	$addition = " OR i.InputBy='$UserID'";
	$conds .= " AND (t.ResourceMgmtGroup IN ('".implode('\',\'',$ary)."') OR i.InputBy='$UserID')";
	
}		
$groupBy = " GROUP BY i.RecordID";
		
		
if ($page_size_change == 1)
{
    $ck_page_size = $numPerPage;
}

if($order == "") $order = 0;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);


$sql = "
		SELECT 
			i.InvoiceDate, 
			CONCAT('<a href=\"view.php?AcademicYearID=$AcademicYearID&RecordID=',i.RecordID,'\">',i.InvoiceNo,'</a>') as InvoiceNo, 
			i.Company, 
			t.InvoiceRecordID as ResourceAdminGroup,
			IF(i.InvoiceDescription='','-',i.InvoiceDescription) as InvoiceDescription, 
			CONCAT('$', i.TotalAmount) as TotalAmount,
			i.RecordID,
			i.PIC,
			IF(('".$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']."'=1 $addition),CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', i.RecordID ,'>'),'-')
		FROM 
			INVOICEMGMT_INVOICE i 
			$dbtable
		WHERE
			i.RecordStatus = ".RECORDSTATUS_ACTIVE."
			$conds
		$groupBy
	";
//echo $sql;

$li->sql = $sql;
$li->field_array = array("InvoiceDate", "InvoiceNo", "Company", "ResourceAdminGroup", "InvoiceDescription", "TotalAmount", "Subtotal", "PIC");
$li->no_col = sizeof($li->field_array)+2;
//$li->IsColOff = "IP25_table";
$li->IsColOff = "Invoice_List_Table";
$li->fieldorder2 = ", RecordID DESC";

$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['Invoice']['InvoiceDate'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column_IP25($pos++, $Lang['Invoice']['InvoiceNo'])."</th>\n";
$li->column_list .= "<th width='15%' >".$li->column_IP25($pos++, $Lang['Invoice']['InvoiceCompany'])."</th>\n";
 $li->column_list .= "<th width='20%' >".$Lang['Invoice']['ResourceMgmtGroup']."</th>\n"; $pos++;
$li->column_list .= "<th width='20%' >".$li->column_IP25($pos++, $Lang['Invoice']['Description'])."</th>\n";
$li->column_list .= "<th width='8%' >".$li->column_IP25($pos++, $Lang['Invoice']['TotalAmount'])."</th>\n";
$li->column_list .= "<th width='7%' >".$Lang['Invoice']['SubTotalAmount2']."</th>\n";
$li->column_list .= "<th width='10%' >".$Lang['Invoice']['PIC']."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("RecordID[]")."</th>\n";	

# year Filter
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.action=\'index.php\';document.form1.submit();"', 1, 0, $AcademicYearID);


$admin_group_leader_arr = $linvoice->returnAdminGroup($UserID, $leader=1);


# table button
$BtnArr = array();
if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || count($admin_group_leader_arr) > 0) {
	# toolbar button 
	$NewBtn = $linterface->GET_LNK_ADD("new.php", $Lang['Btn']['New'] ,"","","",0);
	$ExportBtn = $linterface->GET_LNK_EXPORT("export.php?task=invoice&AcademicYearID=$AcademicYearID&targetGroup=$targetGroup&pic=$pic", $Lang['Btn']['Export'] ,"","","",0);
	
	
	# table button
	$BtnArr[] = array("edit", "javascript:checkEdit(document.form1,'RecordID[]','invoice_edit.php')", $button_edit);
	$BtnArr[] = array("delete", "javascript:checkRemove(document.form1,'RecordID[]','remove_update.php')", $button_delete);
}

$actionBtn = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);

?>
<script language="javascript">
<!--

//-->
</script>
<form name="form1" method="POST" action="">

<div class="content_top_tool">
	<?=$NewBtn?><?=$ExportBtn?>
	<div class="Conntent_search"><input name="keyword" id="keyword" type="text" value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>" onkeyup="Check_Go_Search(event);" /></div>
	<br style="clear:both" />
</div>

<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr class="table-action-bar">
<td height="30">
<?=$yearFilter?> <?=$group_selection?> <?=$picSelect?>
</td>
<td align="right" valign="bottom">
<?=$actionBtn?>
</td>
</tr>
</table>
<?=$li->display();?>

<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo;?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order;?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field;?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />

</form>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>




