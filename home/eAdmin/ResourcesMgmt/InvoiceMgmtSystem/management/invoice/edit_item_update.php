<?php
# using: yat


/*************************************************************************
 *  modification log
 * 
 * 
 * ************************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

$item_chi_name = $item_chi_name1;
$item_eng_name = $item_eng_name1;
$item_total_qty = $item_total_qty1;
	
### insert data to INVOICEMGMT_ITEM, INVOICEMGMT_GROUP_BUDGET_USED
$dataAry = array();
$dataAry['NameChi'] = $item_chi_name; 
$dataAry['NameEng'] = $item_eng_name; 
$dataAry['Price'] = $item_price; 
$dataAry['Quantity'] = $item_total_qty; 
$dataAry['CategoryID'] = $ItemCategory; 
$dataAry['ResourceMgmtGroup'] = $GroupID; 

$linvoice->MODIFY_ITEM_RECORD($InvoiceItemID, $dataAry);

$data = $linvoice->RetrieveInvoiceBasicInfo($RecordID);
$linvoice->CheckOverBudget($data[0]['AcademicYearID'], array($GroupID));

intranet_closedb();

header("location: invoice_budget_summary.php?RecordID=$RecordID");

?>