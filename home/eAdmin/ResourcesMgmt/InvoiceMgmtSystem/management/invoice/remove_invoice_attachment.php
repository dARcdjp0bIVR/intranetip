<?php
//using: 
############ Change Log [start] #################
#
#   2020-05-14 Tommy
#   - removed $linvoice, cause mysql_select_db() error and never used
#
############ Change Log [end] #################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

intranet_auth();

// $linvoice = new libinvoice();
$lf = new libfilesystem();

$path = "$file_path/file/InvoiceMgmtSysAtt/".$RecordID."/". stripslashes($filename);

if (is_file($path))
{
	$result = $lf->file_remove($path);
	
	if($result)
		$msg = "FileDeleteSuccess";
	else
		$msg = "FileDeleteUnsuccess";
}
	
header("location: invoice_edit.php?RecordID=$RecordID&msg=$msg");

	
?>