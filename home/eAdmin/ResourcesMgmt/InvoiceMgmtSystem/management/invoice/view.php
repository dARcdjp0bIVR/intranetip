<?php
// modifying : yat

#################
#
#	Date:	2011-10-11	YatWoon
#			add "print payment voucher"
#
#################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinvoice_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();


$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Management_InvoiceList";
$linterface 	= new interface_html();
$linvoice		= new libinvoice();
$linvoice_ui	= new libinvoice_ui();

# invoice info
$data = $linvoice->RetrieveInvoiceAllInfo($RecordID);


$allowToViewThisInvoiceRecord = $linvoice->Check_Allow_View_Invoice_Record($RecordID);

if((!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] && !$allowToViewThisInvoiceRecord && !$linvoice->isViewerGroupMember)  || !isset($RecordID) || $RecordID=="" || sizeof($data)==0) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


# attachment
$attachmentResult = $linvoice->GetAttachmentListByRecordID($RecordID);
$NoOfAttachment = count($attachmentResult);
$attachmentList = "";
for($i=0; $i<$NoOfAttachment; $i++) {
	$attachmentList .= "- <a href='". $attachmentResult[$i]."' target='_blank'>".stripslashes(substr($attachmentResult[$i],(strrpos($attachmentResult[$i], "/")+1)))."</a><br>";	
}
if($attachmentList=="") $attachmentList = $Lang['General']['EmptySymbol'];


$admin_group_leader_arr = $linvoice->returnAdminGroup($UserID, $leader=1); // is leader of admin group

# edit button

	$editDiv = "<br style='clear:both'><div class='edit_bottom'>";
	if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || $linvoice->isInvoiceCreator($RecordID, $UserID)) {
		$editDiv .= $linterface->GET_ACTION_BTN($Lang['Btn']['Edit'], "submit", "")."&nbsp;";
	} 
	$editDiv .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "javascript:back();")."&nbsp;";
	
	if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || $linvoice->isInvoiceCreator($RecordID, $UserID)) {
		$editDiv .= $linterface->GET_ACTION_BTN($Lang['Invoice']['PrintPaymentVoucher'], "button", "PrintPaymentVoucher();")."&nbsp;";
	} 
	$editDiv .= "</div>";	




# invoice item
if($_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem'] || count($admin_group_leader_arr) > 0) {
	$NewBtn = $linterface->GET_LNK_ADD("new_item.php?RecordID=$RecordID", $button_new ,"","","",0);
	$ExportBtn = $linterface->GET_LNK_EXPORT("export.php?task=item&AcademicYearID=$AcademicYearID&RecordID=$RecordID", $Lang['Btn']['Export'] ,"","","",0);
	
	$itemTable = '
		<div class="content_top_tool">
			'.$NewBtn.$ExportBtn.'
			<br style="clear:both" />
		</div>';
}
$itemTable .= $linvoice_ui->Display_Invoice_Item_Table($RecordID);



$PAGE_NAVIGATION[] = array($Lang['Invoice']['Invoice'],"index.php");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View']);

$TAGS_OBJ[] = array($Lang['Invoice']['Invoice']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

# Funding Source
$arr_funding_source = $linvoice->returnFundingSource();
$NoOfSource = count($arr_funding_source);
$fundingSource = array();
for($a=0; $a<$NoOfSource; $a++) {
	$fundingSource[$arr_funding_source[$a][0]] = $arr_funding_source[$a][1];
}

$funding = $fundingSource[$data['FundingSource']];
if($funding=="") $funding = $Lang['General']['EmptySymbol'];

$accountDate = ($data['AccountDate']=="0000-00-00") ? $Lang['General']['EmptySymbol'] : $data['AccountDate'];

# PIC
$picAry = $linvoice->GetPicByUserId($data['PIC']);
$pic = sizeof($picAry)>0 ? implode(', ',$picAry) : "-";

/*
# Academic Year Filter
$YearSelection = getSelectAcademicYear("YearID","onChange='document.form1.submit();'",1,0,$data['AcademicYearID']);

# PIC selection
$PICSel = "<select name='pic[]' id='pic[]' size='6' multiple='multiple'></select>";
$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");

# Funding Source
$arr_funding_source = $linvoice->returnFundingSource();
$funding_selection = getSelectByArray($arr_funding_source,"name=item_funding", $item_funding);
*/
?>

<script language="javascript">
<!--
function back() {
	self.location.href = "index.php?AcademicYearID=<?=$AcademicYearID?>";
}

function PrintPaymentVoucher()
{
	newWindow("./payment_voucher/print_payment_voucher.php?RecordID=<?=$RecordID?>", 24);
}

//-->
</script>
<form name="form1" action="invoice_edit.php" method="post">
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<div class="this_main_content">
<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
	<td><?=getAcademicYearByAcademicYearID($data['AcademicYearID'], $intranet_session_language)?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['InvoiceDate']?></td>
	<td><?=$data['InvoiceDate']?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['InvoiceCompany']?></td>
	<td><?=intranet_htmlspecialchars($data['Company'])?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['InvoiceNo']?></td>
	<td><?=$data['InvoiceNo']?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['Description']?></td>
	<td><?=($data['InvoiceDescription']==""?$Lang['General']['EmptySymbol']:nl2br($data['InvoiceDescription']))?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['DiscountAmount']?></td>
	<td>$<?=$data['DiscountAmount']?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['TotalAmount']?></td>
	<td>$<?=$data['TotalAmount']?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['FundingSource']?></td>
	<td><?=$funding?></td>
</tr> 

<tr>
	<td class="field_title"><?=$Lang['Invoice']['AccountDate']?></td>
	<td><?=$accountDate?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Invoice']['PIC']?></td>
	<td>
		<table border="0" cellpadding="0" cellspacing="0" class="inside_form_table">
		<tr>
			<td colspan="100%"><?=$pic?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$i_InventorySystem_Item_Attachment?></td>
	<td>
		<?=$attachmentList?>

	</td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['General']['Remark']?></td>
	<td><?=($data['Remarks']==""?$Lang['General']['EmptySymbol']:nl2br($data['Remarks']))?></td>
</tr>
		
</table>

<?=$editDiv?>



<?=$itemTable?>

<div class="edit_bottom_v30">
	<?/*=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:back();") */?>
	<p class="spacer"></p>
</div>	


</div>

<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">
</form>

<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>




