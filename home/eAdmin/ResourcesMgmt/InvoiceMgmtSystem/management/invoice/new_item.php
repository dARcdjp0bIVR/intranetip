<?php
# using: 
/**
 * Change Log:
 * 2017-07-13 Pun [ip.2.5.8.7.1]
 *  - Fixed can submit form many times by click the submit button many times
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Management_InvoiceList";
$linterface		= new interface_html();
$linvoice		= new libinvoice();

$STEPS_OBJ[] = array($Lang['Invoice']['EnterItemInfo'], 1);
$STEPS_OBJ[] = array($Lang['Invoice']['EnterInventoryItemInfo1'],0);
$STEPS_OBJ[] = array($Lang['Invoice']['EnterInventoryItemInfo2'],0);
$TAGS_OBJ[] = array($Lang['Invoice']['Invoice']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]); 

### Retrieve Invoice details
//$Invoice_Info = $linvoice->RetrieveInvoiceBasicInfo($RecordID);
$Invoice_Info = $linvoice->RetrieveInvoiceInfo($RecordID);
$AcademicYearID = $Invoice_Info[0]['AcademicYearID']; 
$InvoiceDate = $Invoice_Info[0]['InvoiceDate'];
$Company = $Invoice_Info[0]['Company'];
$InvoiceNo = $Invoice_Info[0]['InvoiceNo'];
$SchoolYear = getAcademicYearByAcademicYearID($AcademicYearID);
$InvoiceNameTemp = $Invoice_Info[0]['InvoiceDescription'];
$item_chi_name = $item_chi_name ? $item_chi_name : $InvoiceNameTemp;
$item_eng_name = $item_eng_name ? $item_eng_name : $InvoiceNameTemp;

### Item Category 
$arr_category = $linvoice->getCategoryName();
$category_selection = getSelectByArray($arr_category, " name=\"ItemCategory\" ",$ItemCategory);

### Resource Mgmt Group
$thisUserID = $linvoice->IS_ADMIN_USER($UserID) ? "" : $UserID;
$group_arr = $linvoice->returnAdminGroup($thisUserID);
$group_selection = getSelectByArray($group_arr, "name=\"GroupID\"", $GroupID, 0, 0);

?>

<script language="javascript">
<!--

function checkForm()
{
	//// Reset div innerHtml
	reset_innerHtml();
	
	var obj=document.form1;
	var error_no = 0;
	var focus_field = "";
	
	// Item Name (Chinese)
	if(!check_text_30(obj.item_chi_name1, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_Item_ChineseName; ?>.", "div_ChiName_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "item_chi_name1";
	}
	
	// Item Name (English)
	if(!check_text_30(obj.item_eng_name1, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_Item_EnglishName; ?>.", "div_EngName_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "item_eng_name1";
	}
	
	// Price
	if(!checkUnitPrice(obj.item_price,obj.item_price.value) || obj.item_price.value=='' || obj.item_price.value==0) 
	{
		document.getElementById('div_ItemPrice_err_msg').innerHTML = "<font color=red><?=$Lang['Invoice']['InvalidItemPrice']?></font>";
		error_no++;
		if(focus_field=="")	focus_field = "item_price";
	}
	
	// Quantity
	if(!check_text_30(obj.item_total_qty1, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_NumOfItemAdd; ?>.", "div_Qty_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "item_total_qty1";
	}
	else
	{
		if(!isInteger(obj.item_total_qty1.value))
		{
			document.getElementById('div_Qty_err_msg').innerHTML = "<font color=red><?=$i_InventorySystem_StockCheck_ValidQuantityWarning;?></font>";
			error_no++;
			if(focus_field=="")	focus_field = "item_total_qty1";
		}
	}
	
	// Group
	if(!check_select_30(obj.GroupID, "<?php echo $i_alert_pleaseselect.$Lang['Invoice']['ResourceMgmtGroup']; ?>.","","div_Group_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "GroupID";  
	}
	
	// Category
	if(!check_select_30(obj.ItemCategory, "<?php echo $i_alert_pleaseselect.$Lang['Invoice']['Category']; ?>.","","div_Category_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "ItemCategory";  
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
// 		Big5FileUploadHandler();
// 		document.form1.action = "new_item2.php";
		obj.submit();
		obj.btn1.disabled = true;
		obj.btn2.disabled = true;
	}
}
 

function reset_innerHtml()
{
	document.getElementById('div_EngName_err_msg').innerHTML = "";
  	document.getElementById('div_ChiName_err_msg').innerHTML = "";
 	document.getElementById('div_ItemPrice_err_msg').innerHTML = "";
  	document.getElementById('div_Category_err_msg').innerHTML = "";
  	document.getElementById('div_Group_err_msg').innerHTML = "";
  	document.getElementById('div_Qty_err_msg').innerHTML = "";
}

function checkUnitPrice(fieldName, fieldValue) {
	decallowed = 2;  // how many decimals are allowed?
	
	if (isNaN(fieldValue) || fieldValue == "") {
		return false;
	}
	else {
		if (fieldValue.indexOf('.') == -1) fieldValue += ".";
		dectext = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);

		if (dectext.length > decallowed)
		{
			return false;
      	}
		else {
			return true;
      	}
   	}
}

function synContentTo(targetObj, srcContent)
{
	if (targetObj.value=="")
	{
		targetObj.value = srcContent;
	}
}

function changeSubmitBtn()
{
	var obj=document.form1;
	
	if(obj.isAsset[0].checked)	// need input eInventory data
	{
		obj.btn1.value = "<?=$Lang['Invoice']['NewItemNextBtn1']?>";
		document.getElementById('btn2').style.display='none';

	}
	else
	{
		obj.btn1.value = "<?=$Lang['Invoice']['NewItemNextBtn3']?>";
		document.getElementById('btn2').style.display='';
	}	
}

//-->
</script>


<?= $linterface->GET_STEPS_IP25($STEPS_OBJ) ?>
<form name="form1" method="post" action="new_item2.php">

<div class="this_table">
<table class="form_table_v30">

<tr>
	<td class="field_title"><?=$Lang['Invoice']['InvoiceDate']?></td>
	<td><?=$InvoiceDate?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['InvoiceCompany']?></td>
	<td><?=$Company?></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['Invoice']['InvoiceNo']?></td>
	<td><?=$InvoiceNo?></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Item_ChineseName?></td>
	<td><input name="item_chi_name1" type="text" value="<?=$item_chi_name?>" class="textboxtext" onBlur="synContentTo(this.form.item_eng_name1, this.value)" />
	<span id='div_ChiName_err_msg'></span>
	</td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_Item_EnglishName?></td>
	<td><input name="item_eng_name1" type="text" value="<?=$item_eng_name?>" class="textboxtext" onBlur="synContentTo(this.form.item_chi_name1, this.value)" />
	<span id='div_EngName_err_msg'></span>
	</td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['Invoice']['ItemPrice']?> </td>
	<td>$ <input name="item_price" type="text" value="<?=$item_price ? $item_price : 0?>" class="textboxnum"> <span id="div_ItemPrice_err_msg"></span></td>
</tr>	

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$i_InventorySystem_NumOfItemAdd?></td>
	<td><input name="item_total_qty1" type="text" value="<?=$item_total_qty?>" class="textboxnum"> <span id="div_Qty_err_msg"></span></td>
</tr>	

<tr>
	<td class="field_title"><?=$Lang['Invoice']['IsAsset']?></td>
	<td><input type="radio" name="isAsset" value=1 id="isAsset1" onClick="changeSubmitBtn();"> <label for="isAsset1"><?=$i_general_yes?></label> <span class="tabletextremark"><?=$Lang['Invoice']['IsAssetRemark']?></span>
		<input type="radio" name="isAsset" value=0 checked id="isAsset0" onClick="changeSubmitBtn();"> <label for="isAsset0"><?=$i_general_no?></label></td>
</tr>	

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['Invoice']['ResourceMgmtGroup']?></td>
	<td><?=$group_selection?> <span id="div_Group_err_msg"></span></td>
</tr>	 

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang['Invoice']['Category']?></td>
	<td><?=$category_selection?> <span id="div_Category_err_msg"></span></td>
</tr>

</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($Lang['Invoice']['NewItemNextBtn2'], "button","document.form1.add_next.value=1; checkForm();","btn2", " style='display:' ");?> 
<?=$linterface->GET_ACTION_BTN($Lang['Invoice']['NewItemNextBtn3'], "button","checkForm();","btn1");?> 
<?=$linterface->GET_ACTION_BTN($button_reset, "button", "javascript: reset_innerHtml(); this.form.reset();","reset2");?> 
<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='view.php?RecordID=$RecordID'","cancelbtn");?>
<p class="spacer"></p>
</div>
 
</div>

<input type="hidden" name="exist_item_code" value="">
<input type="hidden" name="purchase_type" value="">
<input type="hidden" name="category_id" value="">
<input type="hidden" name="category2_id" value="">
<input type="hidden" name="add_next" value="0">
<input type="hidden" name="RecordID" value="<?=$RecordID?>">
</form>





<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>