<?php

# yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();


if(count($ItemID)>0) {
	$linvoice->Start_Trans();
	$result = $linvoice->DeleteItem($ItemID);	
	
	if(in_array(FALSE,$result)) {
		$linvoice->RollBack_Trans();
		$msg = "DeleteUnsuccess";
	} else { 
		$linvoice->Commit_Trans();
		$msg = "DeleteSuccess";
	}
	
} else {
	$msg = "DeleteUnsuccess";
}

intranet_closedb();

header("Location: view.php?RecordID=$RecordID&msg=$msg");
?>