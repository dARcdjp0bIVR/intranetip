<?php
# using: yat


/*************************************************************************
 *  modification log
 * 
 * 
 * ************************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Management_InvoiceList";
$linterface 	= new interface_html();

### Title ###
$TAGS_OBJ[] = array($Lang['Invoice']['Invoice']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### Retrieve Invoice details
$Invoice_Info = $linvoice->RetrieveInvoiceBasicInfo($RecordID);
$InvoiceNo = $Invoice_Info[0]['InvoiceNo'];

$PAGE_NAVIGATION[] = array($Lang['Invoice']['Invoice'],"index.php");
$PAGE_NAVIGATION[] = array($InvoiceNo,"view.php?RecordID=$RecordID");
$PAGE_NAVIGATION[] = array($Lang['Invoice']['InvoiceBudgetSummary']);


$InvoiceRecordID = $RecordID;

### Retrieve Invoice data
$InvoiceData = $linvoice->RetrieveInvoiceBasicInfo($InvoiceRecordID);
// debug_pr($InvoiceData);
$thisAcademicYearID = $InvoiceData[0]['AcademicYearID'];

$sql = "select 
			a.ResourceMgmtGroup, 
			a.CategoryID, 
			sum(a.Price),
			". $linvoice->getInventoryNameByLang("b.") ." as GroupName
		from 
			INVOICEMGMT_ITEM as a
			inner join INVENTORY_ADMIN_GROUP as b on b.AdminGroupID=a.ResourceMgmtGroup
		where 
			a.InvoiceRecordID=$InvoiceRecordID and
			a.RecordStatus = 1
		group by 
			a.ResourceMgmtGroup, a.CategoryID
		order by b.DisplayOrder
		";
$result = $linvoice->returnArray($sql,4,2);

$ary = array();
$category_ary = array();
$group_name = array();

if(!empty($result))
{
	for($i=0;$i<sizeof($result);$i++)
	{
		list($thisGroupID, $thisCategoryID, $thisAmount, $thisGroupName) = $result[$i];
		
		$ary[$thisGroupID][$thisCategoryID] = $thisAmount;
		$group_name[$thisGroupID] = $thisGroupName; 
		if(!in_array($thisCategoryID, $category_ary))	$category_ary[] = $thisCategoryID;
	}
	
	### Category data
	$category_str = implode(",",$category_ary);
	$sql = "select CategoryID, ". $linvoice->getInventoryNameByLang() ." as CategoryName from INVOICEMGMT_ITEM_CATEGORY where CategoryID in ($category_str)";
	$category_info = $linvoice->returnArray($sql, 2, 1);
	
	### Group budget data
	$GroupIDStr = implode(",",array_keys($ary));
	$sql = "select 
				a.AdminGroupID,	
				a.CategoryID,
	        	IFNULL(b.Budget,0) - a.BudgetUsed
			from 
				INVOICEMGMT_GROUP_BUDGET_USED as a
	        	left join INVOICEMGMT_GROUP_BUDGET as b on (b.AdminGroupID=a.AdminGroupID and b.CategoryID=a.CategoryID and b.AcademicYearID=$thisAcademicYearID)
			where 
				a.AcademicYearID=$thisAcademicYearID and
				a.AdminGroupID in ($GroupIDStr) 
			group by 
	        	a.AdminGroupID, a.CategoryID
		";
	$GroupBudgetDataTemp = $linvoice->returnArray($sql,3,2);
	$GroupBudgetData = array();
	foreach($GroupBudgetDataTemp as $k=>$d)
	{
		list($gid, $cid, $bal) = $d;
		$GroupBudgetData[$gid][$cid] = $bal;
	}
	
	### table content
	$table_content = "";
	foreach($ary as $thisGroupID => $data)
	{
		$table_content .= "<tr>\n";
		$table_content .= "<td>". $group_name[$thisGroupID]."</td>\n";
		
		for($i=0;$i<sizeof($category_info);$i++)
		{
			$thisCategoryID = $category_info[$i]['CategoryID'];
			$thisAmount = $ary[$thisGroupID][$thisCategoryID] ? $ary[$thisGroupID][$thisCategoryID] : 0;
			
			$thisBalance = $GroupBudgetData[$thisGroupID][$thisCategoryID];
			if($thisBalance < 0)
			{
				$thisBalance = "<font color='red'>". $thisBalance ."</font>";
			}
			$thisAmtStr = $thisAmount ? $thisAmount ." / " . $thisBalance : "---";
			$table_content .= "<td>". $thisAmtStr ."</td>\n";
		}
		
		$table_content .= "</tr>\n";
	}

?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<BR>

<div class="table_board">

<table class="common_table_list">
	<thead>
	<tr>
		<th><?=$i_InventorySystem_Group_Name?></th>
		<? for($i=0;$i<sizeof($category_info);$i++)
		{
		echo "<th>". $category_info[$i]['CategoryName'] ." / " . $Lang['Invoice']['Balance'] ."</th>";
		} ?>
	</tr>
   </thead>

   <?=$table_content?>
      
</table>   


</div>
<?

}
else
{
	echo "<div align=center>".$Lang['Invoice']['NoInvoiceItem']."</div>";
}

?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?=$linterface->GET_ACTION_BTN($button_back, "button","javascript:window.location='view.php?RecordID=$InvoiceRecordID'");?> 
<p class="spacer"></p>
</div>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>