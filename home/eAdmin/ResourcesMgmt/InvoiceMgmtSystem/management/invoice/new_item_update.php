<?php
// Using: yat
/********************************* Modification Log *************************************
 * 2011-06-16 [YatWoon]: User userid as attachment temp folder 
 * 2011-06-04 [YatWoon]: Improved: add "createdby" data [case#2011-0124-1006-09067]
 * 2011-06-02 [Yuen]: Fixed: cannot save record due to null purchase price of single item
 * 2011-05-31 [YatWoon]: Fixed: cannot save >1 record if quantity/invoice/tender include special characters
 * 2011-05-30 [YatWoon]: case #2011-0527-1216-26067, comment out: purchase price = purchase price / quantity
 * 2011-05-27 [YatWoon]: Fixed: use itemid as a folder to store the attachment (prevnet overwrite / delete file if same filename)
 * 						 Fixed: cannot store the attachment
 * 2011-05-18 [Carlos]: Fix upload item photo
 * 2011-03-31 (Carlos): Rename photo name as Item Code
 * 2011-02-24 (Yuen) : 
 * 		For single item, purchase price = purchase price / quantity
 ****************************************************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linventory		= new libinventory();
$linvoice = new libinvoice();
$lf				= new libfilesystem();

### insert data to INVOICEMGMT_ITEM, INVOICEMGMT_GROUP_BUDGET_USED
$dataAry = array();
$dataAry['InvoiceRecordID'] = $RecordID; 
$dataAry['NameChi'] = $item_chi_name1; 
$dataAry['NameEng'] = $item_eng_name1; 
$dataAry['Price'] = $item_price; 
$dataAry['Quantity'] = $item_total_qty1; 
$dataAry['CategoryID'] = $ItemCategory; 
$dataAry['ResourceMgmtGroup'] = $GroupID; 
$dataAry['IsAssetItem'] = 1; 
$InvoiceItemID = $linvoice->INSERT_ITEM_RECORD($dataAry);	
	
$curr_date = date("Y-m-d");

$final_item_chi_name = $item_chi_name;
$final_item_eng_name = $item_eng_name;
$final_item_chi_discription = $item_chi_discription;
$final_item_eng_discription = $item_eng_discription;
	
if($targetItemType == ITEM_TYPE_SINGLE)
{	
	# Upload Photo #
	$re_path = "/file/photo/inventory/";
	$path = $intranet_root.$re_path;
	$tmp_path = $intranet_root."/file/inventory/tmp_photo";
	
	$photo = stripslashes(${"hidden_item_photo"});
	$target = "";
	
	if($item_photo=="none" || $item_photo== ""){
	}
	else
	{
		$lf = new libfilesystem();
		if (!is_dir($path))
		{
			$lf->folder_new($path);
		}
		
		$ext = $lf->file_ext($photo);
		$extUC = strtoupper($ext);
		if ($extUC == ".JPG" || $extUC == ".GIF" || $extUC == ".PNG")
		{
			$filename = array();
			for($j=0; $j<$item_total_qty; $j++){
				$filename[$j] = ${item_code_.$j}.$ext;
				$target = "$path/".$filename[$j];		
				$tmp_file = "$tmp_path/$photo";
				$lf->lfs_copy($tmp_file, $target);
			}
		}
		
		$lf->folder_remove_recursive($tmp_path);
	}
	# End of upload photo #
	
	for($i=0; $i<$item_total_qty; $i++)
	{
		# insert a new record for a new single item
		$sql = "INSERT INTO 
					INVENTORY_ITEM 
					(ItemType, CategoryID, Category2ID, NameChi, NameEng, DescriptionChi, DescriptionEng, 
					ItemCode, Ownership, PhotoLink, RecordType, RecordStatus, DateInput, DateModified, CreatedBy)
				VALUES
					($targetItemType, $targetCategory, $targetCategory2, '$final_item_chi_name', '$final_item_eng_name', '$final_item_chi_discription', '$final_item_eng_discription',
					'${"item_code_$i"}', $item_ownership, '', 0, 1, NOW(), NOW(), '$UserID')
				";
				
		$result['NewSingleItem'] = $linventory->db_db_query($sql) or die(mysql_error()); 
		
		# get the single item id
		$tmp_item_id = $linventory->db_insert_id();
		
		# insert into table INVOICEMGMT_ITEMID_RELATIONSHIP
		$linvoice->INSERT_ITEMID_RELATIONSHIP($InvoiceItemID, $tmp_item_id);
		
		# insert the photo for the new single item
		if($item_photo != "")
		{
			$sql = "INSERT INTO 
							INVENTORY_PHOTO_PART 
							(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
							($targetCategory, $targetCategory2, $tmp_item_id, '$re_path', '".$filename[$i]."', NOW(), NOW())
					";
			$linventory->db_db_query($sql) or die(mysql_error()); 
			$tmp_photo_link = $linventory->db_insert_id();
			
			$sql = "UPDATE 
						INVENTORY_ITEM
					SET
						PhotoLink = $tmp_photo_link
					WHERE
						ItemID = $tmp_item_id
					";
			$linventory->db_db_query($sql) or die(mysql_error()); 
		}
		
		# insert ext info for the new single item
		/*
		$item_supplier = stripslashes($item_supplier);
		$item_supplier_contact = stripslashes($item_supplier_contact);
		$item_supplier_description = stripslashes($item_supplier_description);
		$item_brand = stripslashes($item_brand);
		$item_maintain_info = stripslashes($item_maintain_info);
		$item_remark = stripslashes($item_remark);
		*/
		$item_barcode = strtoupper(${"item_barcode_$i"});

		$sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_EXT
					(ItemID, PurchaseDate, PurchasedPrice, UnitPrice, ItemRemark, SupplierName, SupplierContact, SupplierDescription, 
					InvoiceNo, QuotationNo, TenderNo, TagCode, Brand, GroupInCharge, LocationID, 
					FundingSource, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber, MaintainInfo)
				VALUES
					($tmp_item_id, '$item_purchase_date', '$item_purchase_price', '$item_unit_price', '$item_remark', '$item_supplier', '$item_supplier_contact', '$item_supplier_description',
					'$item_invoice', '$item_quotation', '$item_tender', '$item_barcode', '$item_brand', ${"targetGroup_$i"}, ${"targetLocation_$i"},
					$item_funding, '${"item_warranty_expiry_date_$i"}', '${"item_license_$i"}', '${"item_serial_$i"}', '$item_maintain_info')
				";
				
		$linventory->db_db_query($sql) or die(mysql_error());
		
		# insert log record for a new signle item
		$sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, NewStatus, PersonInCharge, Remark, 
					RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($tmp_item_id, '$curr_date', ".ITEM_ACTION_PURCHASE.", ".ITEM_STATUS_NORMAL.", $UserID, '',
					0, 0, NOW(), NOW())
				";
				
		$linventory->db_db_query($sql) or die(mysql_error()); 
		
		# Upload Attachment #
		$re_attach_path = "/file/inventory/attachment/".$tmp_item_id."/";
		$attach_path = "$intranet_root$re_attach_path";
		$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment/".$UserID."/";
		
		for($k=0; $k<$attachment_size; $k++)
		{
		    $attach_file = ${"hidden_item_attachment_".$k};
		    $re_attach_file = ${"item_attachment_".$k};

			$target_attachment = "";
			
			if($re_attach_file == "none" || $re_attach_file == ""){
			}
			else
			{
				$lf = new libfilesystem();
				if (!is_dir($attach_path))
				{
					$lf->folder_new($attach_path);
				}
							
				$target_attachment = "$attach_path/$attach_file";
				$attachment = "$attach_file";			
				$tmp_attachment = "$tmp_attach_path/$attach_file";
				
				$tmp_attachment = stripslashes($tmp_attachment);
				$target_attachment = stripslashes($target_attachment);
				
				if($lf->lfs_copy($tmp_attachment, $target_attachment) == 1)
				{
					$attachment = array();
					$attachment[$i]['FileName'] = $attachementname;
					$attachment[$i]['Path'] = $re_attach_path;
				}
			}
		}
		
		# insert attachment into single item
		for($j=0; $j<$attachment_size; $j++)
		{		
			$attach_file = ${"hidden_item_attachment_$j"};
			$re_attach_file = ${"item_attachment_$j"};

			if($re_attach_file != "")
			{
				$sql = "INSERT INTO 
							INVENTORY_ITEM_ATTACHMENT_PART
							(ItemID, AttachmentPath, FileName, DateInput, DateModified)
						VALUES
							($tmp_item_id, '$re_attach_path', '$attach_file', NOW(), NOW())
						";
				$linventory->db_db_query($sql) or die(mysql_error()); 
			}
		}
	}
	
	## remove attachment from temp folder
	for($k=0; $k<$attachment_size; $k++)
	{
	    $attach_file = ${"hidden_item_attachment_".$k};
		$tmp_attachment = "$tmp_attach_path/$attach_file";
		$lf->lfs_remove($tmp_attachment);
	}
}

if($targetItemType == ITEM_TYPE_BULK)
{
	# Upload Photo #
	$re_path = "/file/photo/inventory/";
	$path = "$intranet_root$re_path";
	$tmp_path = "$intranet_root"."/file/inventory/tmp_photo";

	$photo = stripslashes(${"hidden_item_photo"});
	$target = "";
	
	if($item_photo=="none" || $item_photo== ""){
	}
	else
	{
		$lf = new libfilesystem();
		if (!is_dir($path))
		{
			$lf->folder_new($path);
		}
		
		$ext = $lf->file_ext($photo);
		$extUC = strtoupper($ext);
		if ($extUC == ".JPG" || $extUC == ".GIF" || $extUC == ".PNG")
		{
			$filename = $item_code_0.$ext;
			$target = "$path/".$filename;		
			$tmp_file = "$tmp_path/$photo";
			$lf->lfs_move($tmp_file, $target);
		}
	}
	# End of upload photo #
	
	if($purchase_type == 1)		# New Item
	{
		$sql = "INSERT INTO 
					INVENTORY_ITEM 
					(ItemType, CategoryID, Category2ID, NameChi, NameEng, DescriptionChi, DescriptionEng, 
					ItemCode, Ownership, PhotoLink, RecordType, RecordStatus, DateInput, DateModified, CreatedBy)
				VALUES
					($targetItemType, $targetCategory, $targetCategory2, '$final_item_chi_name', '$final_item_eng_name', '$final_item_chi_discription', '$final_item_eng_discription',
					'$item_code_0', $item_ownership, '', 0, 1, NOW(), NOW(),'$UserID')
				";
		$result['NewBulkItem'] = $linventory->db_db_query($sql) or die(mysql_error()); 
		
		# get bulk item id
		$tmp_item_id = $linventory->db_insert_id();
		
		# insert into table INVOICEMGMT_ITEMID_RELATIONSHIP
		$linvoice->INSERT_ITEMID_RELATIONSHIP($InvoiceItemID, $tmp_item_id);
		
		# insert the photo for the new single item
		if($item_photo != "")
		{
			$sql = "INSERT INTO 
							INVENTORY_PHOTO_PART 
							(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
							($targetCategory, $targetCategory2, $tmp_item_id, '$re_path', '$filename', NOW(), NOW())
					";
			$linventory->db_db_query($sql);
			$tmp_photo_link = $linventory->db_insert_id();
			
			$sql = "UPDATE 
						INVENTORY_ITEM
					SET
						PhotoLink = $tmp_photo_link
					WHERE
						ItemID = $tmp_item_id
					";
			$linventory->db_db_query($sql);
		}
		
		# insert ext info into bulk item
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_EXT
					(ItemID, Quantity, BulkItemAdmin)
				VALUES
					($tmp_item_id, $item_total_qty, '$bulk_item_admin')
				";
		$linventory->db_db_query($sql);
		
		if($item_funding == "")
		{
			$item_funding = $targetBulkFunding;
		}
		
		# insert record into bulk location
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOCATION
					(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
				VALUES
					($tmp_item_id, $targetGroup_0, $targetLocation_0, $item_funding, $item_total_qty)
				";
		$linventory->db_db_query($sql);
		
		# insert log record into bulk 
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyChange, QtyNormal, PurchaseDate, PurchasedPrice, FundingSource, 
					SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, 
					PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, 
					LocationID, GroupInCharge, UnitPrice, MaintainInfo)
				VALUES
					($tmp_item_id, '$curr_date', ".ITEM_ACTION_PURCHASE.", $item_total_qty, $item_total_qty, '$item_purchase_date', '$item_purchase_price', $item_funding,
					'$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_invoice', '$item_quotation', '$item_tender',
					$UserID, '$item_remark', '', 0, 0, NOW(), NOW(),
					$targetLocation_0, $targetGroup_0, $item_unit_price, '$item_maintain_info')
				";
		$linventory->db_db_query($sql);
	}
	else		# Exist Item
	{		
		//echo "Exist Item<BR><BR>";
		$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE ItemCode = '$exist_item_code'";
		$arr_exist_item_id = $linventory->returnVector($sql);
		$exist_item_id = $arr_exist_item_id[0];
		
		$sql = "UPDATE
					INVENTORY_ITEM_BULK_EXT
				SET
					Quantity = Quantity + $item_total_qty
				WHERE
					ItemID = $exist_item_id
				";
		//echo $sql."<BR>";
		$linventory->db_db_query($sql);
		
		# insert into table INVOICEMGMT_ITEMID_RELATIONSHIP
		$linvoice->INSERT_ITEMID_RELATIONSHIP($InvoiceItemID, $exist_item_id);
		
		$sql = "SELECT 
					RecordID 
				FROM 
					INVENTORY_ITEM_BULK_LOCATION 
				WHERE 
					ItemID = $exist_item_id AND 
					LocationID = $targetLocation_0
				";
		$arr_rec_id = $linventory->returnVector($sql);
		if(sizeof($arr_rec_id)>0)
		{
			$rec_id = $arr_rec_id[0];
			$sql = "UPDATE
						INVENTORY_ITEM_BULK_LOCATION
					SET
						Quantity = Quantity + $item_total_qty
					WHERE
						RecordID = $rec_id
					";
			$linventory->db_db_query($sql);
			
			# retrieve funding data
			$sql = "select GroupInCharge, FundingSourceID from INVENTORY_ITEM_BULK_LOCATION where RecordID=$rec_id";
			$result = $linventory->returnArray($sql);
			list($targetGroup_0, $targetBulkFunding) = $result[0];
	
		}
		else
		{
			if($item_funding == "")
			{
				$item_funding = $targetBulkFunding;
			}
		
			$sql = "INSERT INTO
						INVENTORY_ITEM_BULK_LOCATION
						(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
					VALUES
						($exist_item_id, $targetGroup_0, $targetLocation_0, $item_funding, $item_total_qty)
					";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
		}
		
		/*
		$item_supplier = stripslashes($item_supplier);
		$item_supplier_contact = stripslashes($item_supplier_contact);
		$item_supplier_description = stripslashes($item_supplier_description);
		$item_maintain_info = stripslashes($item_maintain_info);
		*/
		
		if($item_funding == "")
		{
			$item_funding = $targetBulkFunding;
		}
		
		$sql = "INSERT INTO
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyChange, QtyNormal, PurchaseDate, PurchasedPrice, FundingSource, 
					SupplierName, SupplierContact, SupplierDescription, InvoiceNo, QuotationNo, TenderNo, 
					PersonInCharge, Remark, UserRemark, RecordType, RecordStatus, DateInput, DateModified, 
					LocationID, GroupInCharge, UnitPrice, MaintainInfo)
				VALUES
					($exist_item_id, '$curr_date', ".ITEM_ACTION_PURCHASE.", $item_total_qty, $item_total_qty, '$item_purchase_date', '$item_purchase_price', $item_funding,
					'$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_invoice', '$item_quotation', '$item_tender',
					$UserID, '$item_remark', '', 0, 0, NOW(), NOW(),
					$targetLocation_0, $targetGroup_0, $item_unit_price, '$item_maintain_info')
				";
		$linventory->db_db_query($sql) or die(mysql_error());
		
		# insert attachment into bulk item
		for($i=0; $i<$no_file; $i++)
		{
			$attach_file = ${"hidden_item_attachment_$i"};
			$re_attach_file = ${"item_attachment_$i"};

			if($attach_file != "")
			{
				$sql = "INSERT INTO 
							INVENTORY_ITEM_ATTACHMENT_PART
							(ItemID, AttachmentPath, FileName, DateInput, DateModified)
						VALUES
							($exist_item_id, '$re_attach_path', '$attach_file', NOW(), NOW())
						";
				//echo $sql."<BR>";
				$linventory->db_db_query($sql);
			}
		}
	}
	
	# Upload Attachment #
	$no_file = 5;
	
	$tmp_item_id = $exist_item_id ? $exist_item_id : $tmp_item_id;
	
	$re_attach_path = "/file/inventory/attachment/".$tmp_item_id."/";
	$attach_path = "$intranet_root$re_attach_path";
	$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment/".$UserID."/";
	for($i=0; $i<$no_file; $i++)
	{
	    $attach_file = ${"hidden_item_attachment_".$i};
	    $re_attach_file = ${"item_attachment_".$i};
		$target_attachment = "";
		
		if($re_attach_file == "none" || $re_attach_file == ""){
		}
		else
		{
			$lf = new libfilesystem();
			if (!is_dir($attach_path))
			{
				$lf->folder_new($attach_path);
			}
						
			$target_attachment = "$attach_path/$attach_file";
			$attachment .= "$attach_file";			
			$tmp_attachment = "$tmp_attach_path/$attach_file";
			
			$tmp_attachment = stripslashes($tmp_attachment);
			$target_attachment = stripslashes($target_attachment);
				
			if($lf->lfs_move($tmp_attachment, $target_attachment) == 1)
			{
				$attachment = array();
				$attachment[$i]['FileName'] = $attachementname;
				$attachment[$i]['Path'] = $re_attach_path;
			}
		}
	}
	# End of upload attachment #
	
	# insert attachment into bulk item
	for($i=0; $i<$no_file; $i++)
	{		
		$attach_file = ${"hidden_item_attachment_$i"};
		$re_attach_file = ${"item_attachment_$i"};

		if($re_attach_file != "")
		{
			$sql = "INSERT INTO 
						INVENTORY_ITEM_ATTACHMENT_PART
						(ItemID, AttachmentPath, FileName, DateInput, DateModified)
					VALUES
						($tmp_item_id, '$re_attach_path', '$attach_file', NOW(), NOW())
					";
			//echo $sql."<BR>";
			$linventory->db_db_query($sql);
		}
	}
	
}

## remove tmp attachment
if($attachment_size)
{
	$tmp_attach_path = "$intranet_root"."/file/inventory/tmp_attachment/".$UserID."/";
	for($k=0; $k<$attachment_size; $k++)
	{
		$attach_file = ${"hidden_item_attachment_".$k};
		$tmp_attachment = "$tmp_attach_path/$attach_file";
		$tmp_attachment = stripslashes($tmp_attachment);
		$lf->lfs_remove($tmp_attachment);
	}
}

### Add other item or not
if($add_next)
{
	header("location: new_item.php?RecordID=$RecordID&msg=AddSuccess&GroupID=$GroupID&ItemCategory=$ItemCategory");
}
else
{
	# found out all groups in this invoice and check any group need send email notification
	$data = $linvoice->RetrieveInvoiceBasicInfo($RecordID);
	$GroupIDAry = $linvoice->getAllAdminGroupIDOfInvoice($RecordID);
	$linvoice->CheckOverBudget($data[0]['AcademicYearID'], $GroupIDAry);
		
	header("location: invoice_budget_summary.php?RecordID=$RecordID");
}

intranet_closedb();
?>