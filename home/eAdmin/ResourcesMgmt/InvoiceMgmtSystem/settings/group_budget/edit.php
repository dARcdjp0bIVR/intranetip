<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Settings_GroupBudget";
$linterface 	= new interface_html();
$linvoice		= new libinvoice();

### Title ###
$TAGS_OBJ[] = array($Lang['Invoice']['GroupBudget']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);

$AcademicYearName = getAcademicYearByAcademicYearID($YearID);
$GroupName = $linvoice->returnAdminGroupName($AdminGroupID);

$PAGE_NAVIGATION[] = array($AcademicYearName,"javascript:back()");
$PAGE_NAVIGATION[] = array($GroupName);

# Retrieve Invoice item category
$category_ary = $linvoice->returnGroupCategoriesBudget($YearID, $AdminGroupID);
if(sizeof($category_ary) > 0)
{
	for($i=0; $i<sizeof($category_ary); $i++)
	{
		$j++;
		list($category_id, $category_name, $this_budget) = $category_ary[$i];
		
		$table_content .= "<tr>\n";
		$table_content .= "<td>$j</td>\n";
		$table_content .= "<td>".intranet_htmlspecialchars($category_name)."</td>\n";
		$table_content .= "<td><input type='text' name='Category_$category_id' value='$this_budget'>
								<span id='div_Category_". $category_id ."_err_msg'></span></td>\n";
		$table_content .= "</tr>\n";
	}
}
else
{
	$table_content .= "<tr>\n";
	$table_content .= "<td colspan='100%' style='text-align:right'>". $Lang['Invoice']['NoItemCategory']."</td>\n";
	$table_content .= "</tr>\n";
}
?>

<script language="javascript">
<!--
function apply_all()
{
	obj = document.form1;
	var all_budget = obj.top_budget.value;
	
	<? if(sizeof($category_ary) > 0)
		for($i=0; $i<sizeof($category_ary); $i++)
		{ 
			list($category_id, $category_name, $this_budget) = $category_ary[$i];
	?>
		obj.Category_<?=$category_id?>.value = all_budget;
		
	<? } ?>
}

function back()
{
	window.location="index.php?YearID=<?=$YearID?>";	
}

function reset_innerHtml()
{
	<? if(sizeof($category_ary) > 0)
		for($i=0; $i<sizeof($category_ary); $i++)
		{ 
			list($category_id, $category_name, $this_budget) = $category_ary[$i];
	?>
 		document.getElementById('div_Category_<?=$category_id?>_err_msg').innerHTML = "";
 	<? } ?>
}

function check_form()
{
	reset_innerHtml();
	
	obj = document.form1;
	var this_budget = 0;
	var error_no = 0;
	var focus_field = "";
	
	<? if(sizeof($category_ary) > 0)
		for($i=0; $i<sizeof($category_ary); $i++)
		{ 
			list($category_id, $category_name, $this_budget) = $category_ary[$i];
	?>
			this_budget_obj = obj.Category_<?=$category_id?>;
			this_error_span = "div_Category_<?=$category_id?>_err_msg";
			if(this_budget_obj.value=="")	this_budget_obj.value = "0.00";
			if(!check_positive_float_30(this_budget_obj, "<?=$Lang['General']['JS_warning']['InputPositiveValue']?>",this_error_span))
			{ 
				error_no++;
				if(focus_field=="")	focus_field = "Category_<?=$category_id?>";
			}
	<? } ?>
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		obj.submit();
	}
}	

function reset_form()
{
	reset_innerHtml();
	document.form1.reset();	
}

//-->
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<form name="form1" action="edit_update.php" method="post">
<div class="table_board">

<table class="common_table_list_v30 edit_table_list_v30">
	<thead>
	<tr>
		<th class="num_check">#</th>
		<th width="50%"><?=$Lang['Invoice']['Category']?></th>
		<th width="50%"><?=$Lang['Invoice']['Budget']?></th>
	</tr>
	</thead>
	
	<tr class="edit_table_head_bulk">
		<th>&nbsp;</th>
		<th>&nbsp;</th>
		<th>
			<span class="table_filter">
				<input type="text" name="top_budget" value="">
			</span>
			<div class="table_row_tool">
				<a href="javascript:apply_all()" class="icon_batch_assign"></a>
			</div>
		</th>
		
		</tr>

   <?=$table_content?>
      
</table>   

<div class="edit_bottom_v30">
	<?=$linterface->GET_ACTION_BTN($button_save, "button", "javascript:check_form();") ?>
	<?=$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:reset_form();") ?>
	<?=$linterface->GET_ACTION_BTN($button_back, "button", "javascript:back();") ?>
	<p class="spacer"></p>
</div>	

</div>

<input type="hidden" name="YearID" value="<?=$YearID?>">
<input type="hidden" name="AdminGroupID" value="<?=$AdminGroupID?>">
</form>



<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>

