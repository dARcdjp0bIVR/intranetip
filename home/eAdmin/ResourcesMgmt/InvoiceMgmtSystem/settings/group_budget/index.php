<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['InvoiceMgmtSystem'] = 1;
$CurrentPage	= "Settings_GroupBudget";
$linterface 	= new interface_html();
$linvoice		= new libinvoice();

### Title ###
$TAGS_OBJ[] = array($Lang['Invoice']['GroupBudget']);
$MODULE_OBJ = $linvoice->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

# Academic Year Filter
$YearID = $YearID ? $YearID : Get_Current_Academic_Year_ID();
$YearSelection = getSelectAcademicYear("YearID","onChange='document.form1.submit();'",1,0,$YearID);

# Retrieve Resources Mgmt Group
if (($intranet_session_language =="b5" || $intranet_session_language == "gb"))
{
	$firstChoice = "a.NameChi";
	$altChoice = "a.NameEng";
}
else
{
	$firstChoice = "a.NameEng";
	$altChoice = "a.NameChi";
}
$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";
$sql = "SELECT 
			a.AdminGroupID, 
			$namefield,
			IFNULL( sum(b.Budget), '0.00' )
		FROM 
			INVENTORY_ADMIN_GROUP as a
			left join INVOICEMGMT_GROUP_BUDGET as b on b.AdminGroupID=a.AdminGroupID and b.AcademicYearID=$YearID
		group by 
			a.AdminGroupID
		ORDER BY 
			a.DisplayOrder ";
$result = $linvoice->returnArray($sql);
if(sizeof($result) > 0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		$j++;
		list($admin_gp_id, $admin_name,$budget) = $result[$i];
		
		$table_content .= "<tr>\n";
		$table_content .= "<td>$j</td>\n";
		$table_content .= "<td>".intranet_htmlspecialchars($admin_name)."</td>\n";
		$table_content .= "<td style='text-align:right'><a href='javascript:EditGroupBudget(". $admin_gp_id .");'>$budget</a></td>\n";
		$table_content .= "</tr>\n";
	}
}
else
{
	$table_content .= "<tr>\n";
	$table_content .= "<td colspan='100%' style='text-align:center'>$i_no_record_exists_msg</td>\n";
	$table_content .= "</tr>\n";
}

?>

<script language="javascript">
<!--
function EditGroupBudget(group_id)
{
	obj = document.form1;
	obj.action="edit.php";
	obj.AdminGroupID.value=group_id;
	obj.submit();
}
//-->
</script>

<form name="form1" method="post">
<div class="table_board">

<!-- Filter //-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<div class="table_filter">
			<?=$YearSelection?>
		</div>
	</td>
</tr>
</table>


<table class="common_table_list">
	<thead>
	<tr>
		<th class="num_check">#</th>
		<th><?=$i_InventorySystem_Group_Name?></th>
		<th style='text-align:right'><?=$Lang['Invoice']['TotalBudget']?></th>
	</tr>
   </thead>

   <?=$table_content?>
      
</table>   


</div>
<input type="hidden" name="AdminGroupID" value="">
</form>



<?

intranet_closedb();
$linterface->LAYOUT_STOP();
?>

