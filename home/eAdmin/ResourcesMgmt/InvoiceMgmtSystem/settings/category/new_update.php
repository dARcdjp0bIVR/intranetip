<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinvoice.php");

if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-InvoiceMgmtSystem']) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

intranet_auth();
intranet_opendb();

$linvoice = new libinvoice();

$TitleEng = trim($TitleEng);
$TitleChi = trim($TitleChi);

# check duplication of "Title"
$categoryId = $linvoice->GetCategoryIdByTitle($TitleEng, $TitleChi);
if($categoryId!="") {
	$msg = $Lang['Invoice']['Error_Duplicate_CategoryName'];
	header("Location: new.php?msg=$msg&TitleChi=$TitleChi&TitleEng=$TitleEng");
	exit;	
} else {
	$linvoice->Start_Trans();
	$record = $linvoice->NewCategory($TitleEng, $TitleChi);	
	if($record) {
		$linvoice->Commit_Trans();
		$msg = "AddSuccess";
	} else {
		$linvoice->RollBack_Trans(); 
		$msg = "AddUnsuccess";
	}
		
	header("Location: index.php?msg=$msg");
}

intranet_closedb();
?>