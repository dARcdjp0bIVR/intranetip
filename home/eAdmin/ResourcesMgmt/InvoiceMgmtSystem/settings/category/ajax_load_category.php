<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinvoice_ui.php");


intranet_auth();
intranet_opendb();

$linvoiceUI = new libinvoice_ui();

echo $linvoiceUI->Get_Category_Table();

intranet_closedb();

?>