<?php 
extract($_PAGE['views_data']);
$linterface = $_PAGE['libPCM_ui'];

$navigationAry[] = array($Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.category\';');
$navigationAry[] = array($Lang['Btn']['Import']);

$htmlAry['Navigation'] = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);
$htmlAry['StepsBox'] = $linterface->GET_IMPORT_STEPS($CurrStep=3, $CustStepArr='');
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", 'backBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

switch ($status){
	case 1:
		$display = str_replace('<!--count-->',$success,$Lang['ePCM']['Import']['Success']);
		break;
	case -1:
		$display = $Lang['ePCM']['Import']['Fail1'];
		break;
	case -2:
		$display = $Lang['ePCM']['Import']['Done'];
		break;
	default:
		$display = $Lang['ePCM']['Import']['Fail2'];
		break;
}
?>
<script>
var backLocation = 'index.php?p='+'<?php echo $origin?>';
function goBack(){
	if(typeof backLocation != 'undefined'){
		window.location = backLocation;
	}
	else{
		console.log('please add a js global var backLocation');
		window.history.back();
	}
}
</script>
<?php echo $htmlAry['Navigation']?>
<br>
<?php echo $htmlAry['StepsBox']?>
<br>
<div style="text-align:center;"><?php echo $display?></div>
<div class="table_board">
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?php echo $htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>