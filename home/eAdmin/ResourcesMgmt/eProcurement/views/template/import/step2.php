<?php 
extract($_PAGE['views_data']);
$linterface = $_PAGE['libPCM_ui'];

### page for validation ###
$thisSrc = "index.php?p=setting.import.validate.".$type."&tmpfile=".$tmpfile;
// $htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;"></iframe>';
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="display:none;"></iframe>';
### page for validation ###

$htmlAry['Navigation'] = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);
$htmlAry['StepsBox'] = $linterface->GET_IMPORT_STEPS($CurrStep=2, $CustStepArr='');

$processingMsg = $linterface->Get_Ajax_Loading_Image(1).' &nbsp;'.$Lang['ePCM']['Import']['Step2Loading'];
?>
<script>
var backLocation = '<?php echo $js['backLocation']?>';
function checkForm(){
	var canSubmit = true;
	$('form#form1').attr({'action':'<?php echo $js['submitLocation']?>'});

	return canSubmit;
}

$(document).ready( function() {
	//Block_Document('<?=$processingMsg?>');
});
</script>
<?php echo $htmlAry['Navigation']?>
<br>
<?php echo $htmlAry['StepsBox']?>
<br>
<div class="table_board">
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?php echo $Lang['General']['SuccessfulRecord']?></td>
			<td><div id="SuccessCountDiv"></div></td>
		</tr>
		<tr>
			<td class="field_title"><?php echo $Lang['General']['FailureRecord']?></td>
			<td><div id="FailCountDiv"></div></td>
		</tr>
	</table>
	<div id="ErrorTableDiv">
	</div>
	<?php echo $htmlAry['iframe'] ?>
</div>
<input type="hidden" name="tmp" value="<?php echo $tmpfile?>">