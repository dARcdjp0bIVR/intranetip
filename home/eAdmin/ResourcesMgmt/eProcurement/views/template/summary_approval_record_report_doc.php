<?php
$linterface = $_PAGE['libPCM_ui'];
$parData = $_PAGE['views_data'];
$caseInfoAry = $parData['CaseInfoAry'];
$quotations = $parData['Quotations'];
$result = $parData['Result'];
$priceItem = $parData['priceItem'];
if(empty($priceItem)){
	$priceComparisonTable = false;
}else{
	$priceComparisonTable = true;
	$priceItem = BuildMultiKeyAssoc($priceItem,array('QuotationID','PriceItemID'));
	$priceComparisonTable = $linterface->getPriceComparisonTable($priceItem,$quotations);
}



$quotationType = $caseInfoAry['QuotationType'];
switch($quotationType){
	case 'Q';
		$typeName = 'Quotation';
	break;
	case 'T';
		$typeName = 'Tender';
	break;
}

$countLateQuotation = 0;
foreach($quotations as $_quote){
	if($_quote['IsLate']){
		$countLateQuotation++;
	}
}

$code = $caseInfoAry['Code'];
$groupName = Get_Lang_Selection($caseInfoAry['GroupNameChi'],$caseInfoAry['GroupName']);
$reason = $caseInfoAry['Description'];

$dateOfIssue = $caseInfoAry['QuotationStartDate'];
$tenderClosingDate = $caseInfoAry['QuotationEndDate'];
$numOfTenderReceived = count($quotations);
$numOfLateTender = $countLateQuotation;
$dateOfOpening = $caseInfoAry['TenderOpeningStartDate'];
$dateOfApproval = $caseInfoAry['TenderApprovalStartDate'];

//School Name 
$schoolName = GET_SCHOOL_NAME();
if($_PAGE['libPCM']->isEJ()){
	$schoolName = convert2unicode($schoolName, $isAbsOn="", $direction=1);
}
$x = '<center><u><b>'.$schoolName.'</b></u></center>';


$x .= '<center><u><b>'.$typeName .' Summary and Approval Record of '.$typeName.'</b></u></center>';

$x .= '<table width="100%">';
$x .= '<tr>';
$x .= '<td width="25%">';
$x .= $typeName.' No. :';
$x .= '</td>';
$x .= '<td width="25%" class="underlined">';
$x .= $code;
$x .= '</td>';
$x .= '<td width="25%">';
$x .= 'Dept. / Subject :';
$x .= '</td>';
$x .= '<td width="25%"  class="underlined">';
$x .= $groupName;
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td>';
$x .= $typeName.' Content:';
$x .= '</td>';
$x .= '<td colspan="3"  class="underlined">';
$x .= $reason;
$x .= '</td>';
$x .= '</tr>';
$x .= '</table>';

$x .= '<span class="small">(Re: ☐ Subscription/General Fund;	☐ School & Class Grant; 	☐ Special Subsidies from   SAM Grant  ☐ Non-Recurrent(F/E) Grants; 	☐ Non-recurrent(Major Repair) Grants)</span>';
$x .= '<br>';
$x .= '<table width="100%">';
$x .= '<tr>';
$x .= '<td width="25%">';
$x .= 'Date of Issue of '.$typeName.':';
$x .= '</td>';
$x .= '<td width="20%"  class="underlined">';
$x .= $dateOfIssue;
$x .= '</td>';
$x .= '<td width="10%">';
$x .= '</td>';
$x .= '<td width="25%">';
$x .= $typeName.' Closing Date:';
$x .= '</td>';
$x .= '<td width="20%"  class="underlined"> ';
$x .= $tenderClosingDate;
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td width="25%">';
$x .= 'No. of '.$typeName.' received:';
$x .= '</td>';
$x .= '<td width="20%" class="underlined">';
$x .= $numOfTenderReceived;
$x .= '</td>';
$x .= '<td width="10%">';
$x .= '</td>';
$x .= '<td width="25%">';
$x .= 'No. of Late '.$typeName.':';
$x .= '</td>';
$x .= '<td width="20%"  class="underlined"> ';
$x .= $numOfLateTender;
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr>';
$x .= '<td width="25%">';
$x .= 'Date of Opening:';
$x .= '</td>';
$x .= '<td width="20%"  class="underlined">';
$x .= $dateOfOpening;
$x .= '</td>';
$x .= '<td width="10%">';
$x .= '</td>';
$x .= '<td width="25%">';
$x .= 'Date of Approval:';
$x .= '</td>';
$x .= '<td width="20%" class="underlined">';
$x .= $dateOfApproval;
$x .= '</td>';
$x .= '</tr>';
$x .= '</table>';
$x .= '<br>';
$x .= '<table width="100%" class="border">';
$x .= '<caption>Price Comparison Table (Please refer to the attachment(s))</caption>';
$x .= '<tr>';
$x .= '<th>';
$x .= '</th>';
$x .= '<th>';
$x .= 'Supplier';
$x .= '</th>';
$x .= '<th>';
$x .= 'Offer';
$x .= '</th>';
$x .= '<th>';
$x .= 'No offer';
$x .= '</th>';
$x .= '<th>';
$x .= 'Received Late';
$x .= '</th>';
$x .= '<th>';
$x .= 'No Reply';
$x .= '</th>';
$x .= '<th>';
$x .= 'Recommendation';
$x .= '</th>';
$x .= '</tr>';

foreach($quotations as $_key => $_quote){
	$_isGetOffer = false;
	$x .= '<tr>';
	$x .= '<td width="5%">';
	$x .= $_key+1;
	$x .= '</td>';
	$x .= '<td width="30%">';
	$x .= Get_Lang_Selection($_quote['SupplierNameChi'],$_quote['SupplierName']);
	$x .= '</td>';
	$x .= '<td width="5%" class="text_center">';
	if($_quote['SupplierID']==$result['SupplierID']){
		$_isGetOffer = true;
		$x .= 'O';
	}
	$x .= '</td>';
	$x .= '<td width="5%" class="text_center">';
	//No offer
	if(!$_quote['IsNoReply']&&!$_quote['IsLate']&&!$_isGetOffer){
		$x .= 'O';
	}
	$x .= '</td>';
	$x .= '<td width="10%" class="text_center">';
	//receive late
	if($_quote['IsLate']){
		$x .= 'O';
	}
	$x .= '</td>';
	$x .= '<td width="5%" class="text_center">';
	//no reply
	if($_quote['IsNoReply']){
		$x .= 'O';
	}
	$x .= '</td>';
	$x .= '<td width="40%">';
	//Recommendation
	if($_quote['SupplierID']==$result['SupplierID']){
		$x .= $result['Description'];
	}
	$x .= '</td>';
	$x .= '</tr>';
}
$x .= '</table>';
$x .= '<br>';
$x .= '<table>';
	
$x .= '<tr>';
$x .= '<td colspan="2">';
$x .= '</td>';
$x .= '<td class="border">';
$x .= 'Interest to declare(Please circle)';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr>';
$x .= '<td  class="border">';
$x .= 'We certify that we have date-stamped, initialed on and vetted every tender.';
$x .= '</td>';
$x .= '<td class="border">';
$x .= '<table class="no_border">';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Signature of TOVC Member :';
$x .= '</td>';
$x .= '<td class="underlined">';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Name (in Block Letter) :';
$x .= '</td>';
$x .= '<td>';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= 'Signature of TOVC Member :';
$x .= '</td>';
$x .= '<td class="underlined">';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Name (in Block Letter) :';
$x .= '</td>';
$x .= '<td>';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '</table>';
$x .= '</td>';
$x .= '<td class="border">';
$x .= '<table class="no_border" >';
$x .= '<tr class="no_border">';
$x .= '<td class="text_center">';
$x .= 'Yes / No';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td  class="text_center">';
$x .= 'Yes / No';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '</table>';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr>';
$x .= '<td  class="border">';
$x .= 'I recommend the tender as indicated above.';
$x .= '</td>';
$x .= '<td class="border">';
$x .= '<table class="no_border">';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Signature of teacher/ administrative staff :';
$x .= '</td>';
$x .= '<td class="underlined">';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Name (in Block Letter) :';
$x .= '</td>';
$x .= '<td>';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '</table>';
$x .= '</td>';
$x .= '<td class="border">';
$x .= '<table class="no_border">';
$x .= '<tr class="no_border">';
$x .= '<td class="text_center">';
$x .= 'Yes / No';
$x .= '</td>';
$x .= '</tr>';
$x .= '</tr>';
$x .= '</table>';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr>';
$x .= '<td  class="border">';
$x .= 'We agree that all the information presented in the meeting should be kept confidential.  <br> ';
$x .= '☐*We certify that the tender vetting procedures are in order and approve the above recommendation. <br> ';
$x .= '<b>OR</b><br>';
$x .= '☐*We recommend that re-tender is required. <br> ';
$x .= '<b>OR</b><br>';
$x .= '☐*We disapprove the recommendation. <br> ';
$x .= 'Reason:';
$x .= '<table class="no_border" style="width:80%;margin-left:30px;">';
$x .= '<tr class="no_border">';
$x .= '<td class="underlined">&nbsp;</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td class="underlined">&nbsp;</td>';
$x .= '</tr>';
$x .= '</table>';
$x .= '<br>&nbsp;<br>&nbsp;';
$x .= '</td>';
	
$x .= '<td class="border">';
$x .= '<table class="no_border">';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Signature of TAC Chairperson';
$x .= '</td>';
$x .= '<td class="underlined">';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Name (in Block Letter) :';
$x .= '</td>';
$x .= '<td>';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '(Supervisor)';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= 'Signature of TAC  Member :';
$x .= '</td>';
$x .= '<td class="underlined">';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Name (in Block Letter) :';
$x .= '</td>';
$x .= '<td>';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '(Principal)';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= 'Signature of TAC  Member :';
$x .= '</td>';
$x .= '<td class="underlined">';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Name (in Block Letter) :';
$x .= '</td>';
$x .= '<td>';
$x .= '';
$x .= '</td>';
$x .= '</tr>';

$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '(SGM)';
$x .= '</td>';
$x .= '</tr>';

$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= 'Signature of TAC  Member :';
$x .= '</td>';
$x .= '<td class="underlined">';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td style="width:50%">';
$x .= 'Name (in Block Letter) :';
$x .= '</td>';
$x .= '<td>';
$x .= '';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '(Chairman of PTA)';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
	
	
$x .= '</table>';
$x .= '</td>';
$x .= '<td class="border">';
$x .= '<table class="no_border" >';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td class="text_center">';
$x .= 'Yes / No';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td  class="text_center">';
$x .= 'Yes / No';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td  class="text_center">';
$x .= 'Yes / No';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td  class="text_center">';
$x .= 'Yes / No';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '<tr class="no_border">';
$x .= '<td>';
$x .= '&nbsp;';
$x .= '</td>';
$x .= '</tr>';
$x .= '</table>';
$x .= '</td>';
$x .= '</tr>';
	
$x .= '</table>';

if($priceComparisonTable){
	$x .= '<br>';
	$x .= '<caption>Price Comparison Table</caption>';
	$x .= $priceComparisonTable;
}
?>
<?= $x ?>
