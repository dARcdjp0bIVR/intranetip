<?php
################# Change Log ###############
#
#	Date:	20160715	Kenneth
#	- add default back action for no backlocation
#
############################################



$linterface = $_PAGE['libPCM_ui'];

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<script>
var canSubmit = true;
function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
	    console.log(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}

function goCancel(){
    if (typeof ajax_recover_tempRemove == 'function') {
        ajax_recover_tempRemove();
    }

	if(typeof backLocation != 'undefined'){
		window.location = backLocation;
	}
	else{
		console.log('please add a js global var backLocation');
		window.history.back();
	}
}

</script>
<form name='form1' id='form1' method="POST"  enctype="multipart/form-data">
<div class="table_board">
	<!--FRAME CONTENT-->
	<br style="clear:both;" />
	<div style="text-align:left">
	<?=$linterface->MandatoryField()?>
	</div>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
</form>