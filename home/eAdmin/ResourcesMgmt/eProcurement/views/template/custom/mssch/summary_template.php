<?php
$linterface = $_PAGE['libPCM_ui'];
$parData = $_PAGE['views_data'];
$caseInfoAry = $parData['CaseInfoAry'];
$quotations = $parData['Quotations'];
$result = $parData['Result'];
$priceItem = $parData['priceItem'];
$remarks = BuildMultiKeyAssoc($parData['Remarks'], 'StageID');

if($caseInfoAry['QuotationType'] != 'Q'){
	$html = $_PAGE['Controller']->getViewContent('/template/summary_approval_record_report_doc.php');
	echo $html;
}else{
	if(empty($result)){
		// do nth
	}else{
		$resultSupplier = $result['SupplierName'];
	}
	
	if(empty($priceItem)){
		// do nth
	}else{
		$priceItem = BuildMultiKeyAssoc($priceItem,'QuotationID',array(), 0,1);
	}
	
	$quotationType = $caseInfoAry['QuotationType'];
	switch($quotationType){
		case 'Q';
			$typeName = 'Quotation';
		break;
		case 'T';
			$typeName = 'Tender';
		break;
	}
	
	$countLateQuotation = 0;
	foreach($quotations as $_quote){
		if($_quote['IsLate']){
			$countLateQuotation++;
		}
	}
	
	$code = $caseInfoAry['Code'];
	$groupName = Get_Lang_Selection($caseInfoAry['GroupNameChi'],$caseInfoAry['GroupName']);
	$reason = $caseInfoAry['Description'];
	
	$dateOfIssue = $caseInfoAry['QuotationStartDate'];
	$tenderClosingDate = $caseInfoAry['QuotationEndDate'];
	$numOfTenderReceived = count($quotations);
	$numOfLateTender = $countLateQuotation;
	$dateOfOpening = $caseInfoAry['TenderOpeningStartDate'];
	$dateOfApproval = $caseInfoAry['TenderApprovalStartDate'];
	
	//School Name 
	
	// $x .= '<style> * { letter-spacing:20;}</style>';
	// style="letter-spacing:5px;
	
	function getUnderline($length=12, $string=''){
		$fullSpace = '　';
		$output = '';
		if($string != ''){
			$newLength = ($length - strlen($string)) <0 ? 1: $length - strlen($string);
			$output = $string;
			for($x = 0 ; $x < $newLength ; $x+=2){
				$output = $fullSpace.$output.$fullSpace;
			}
		}else{
			for($x = 0 ; $x < $length ; $x++){
				$output .= $fullSpace;
			}
		}
		
		return '<u>'.$output.'</u>';
	}
	function getFullSpace($length=1){
		$fullSpace = '　';
		$output = '';
		for($x = 0 ; $x < $length ; $x++){
			$output .= $fullSpace;
		}
		return $output;
	}
	
	$x .= '<html>';
	$x .= '<head>';
	$x .= '<style>';
	$x .= '	body { font-size:11pt; font-family:"細明體";} 
			span { letter-spacing:5;}
			span u {letter-spacing:1;}
			.schoolName {font-size:14pt;}';
	$x .= '</style>';
	$x .= '</head>';
	$x .= '<body>';
	$x .= '<center class="schoolName">瑪利諾中學</center>';
	
	$x .= '<span>學校檔案：'.$caseInfoAry['Code'].'</span>';
	$x .= '<br>';
	$x .= '<span>致：*校長/副校長</span>';
	$x .= '<br>';
	
	$x .= '<center><b>按 書 面 報 價 購 貨 表 格</b>（5,001元或以上至 50,000元以下）</center>';
	$x .= '<span>'.getFullSpace(2).'本校邀請下列供應商，提供物料或服務的口頭報價，就供應商所提供的物料或服務的價錢和質素與市場的供應情況作出適當的比較後，現推薦採納由'.getUnderline(12, $resultSupplier).' 提出的*較低報價/較高報價。不採納最低報價的原因是：'.getUnderline(12, $remarks[4]['Remark']).'。</span>';
	$x .= '<br>';
	$x .= '<br>';
	
	$x .= '<table width="100%" class="border">';
	$x .= '<tr style="text-align:center;">';
	$x .= '<td>項目<br>編號</td>';
	$x .= '<td>物品說明</td>';
	$x .= '<td>所需<br>數量</td>';
	$x .= '<td>供應商名稱及電話號碼</td>';
	$x .= '<td>單價</td>';
	$x .= '<td>總價</td>';
	$x .= '<td>採納出價<br>「✔」</td>';
	$x .= '</tr>';
	if(count($quotations) > 0){
		foreach((array)$quotations as $_quoInfo){
			$_quoID = $_quoInfo['QuotationID'];
			$_supplierName = Get_Lang_Selection($_quoInfo['SupplierNameChi'], $_quoInfo['SupplierNameChi']);
			$_supplierPhone = $_quoInfo['Phone'];
			$_quoItemArr = $priceItem[$_quoID];
			$_itemSum = array_sum(Get_Array_By_Key($_quoItemArr, 'Price'));
			if(count($_quoItemArr) == 1){
				$_itemName = $_quoItemArr[0]['PriceItemName'];
			}else{
				$_itemName = '';
			}
			$x .= '<tr>';
			$x .= '<td></td>';
			$x .= '<td></td>';
			$x .= '<td></td>';
			$x .= '<td>'.$_supplierName.'<br>'.$_supplierPhone.'</td>';
			$x .= '<td></td>';
			$x .= '<td>'.$_itemSum.'</td>';
			$x .= '<td></td>';
			$x .= '</tr>';
		}
	}else{
		
	}
	$x .= '</table>';
	
	$x .= '<br>';
	$x .= '<span style="line-height: 150%;">如邀請/收到少於兩名供應商報價，請在方格內加以說明：</span>';
	$x .= '<table width="100%" class="border">';
	$x .= '<tr>';
	$x .= '<td style="line-height: 150%;">';
	$x .= '邀請/收到少於兩名供應商報價的原因是：'.getUnderline(18,$remarks[2]['Remark']).'。<br>';
	$x .= getFullSpace(6).'批簽：'.getUnderline(6).'(職級'.getUnderline(6).'/職位'.getUnderline(6).')';
	$x .= '</td>';
	$x .= '</tr>';
	$x .= '</table>';
	$x .= '<br>';
	
	$x .= '<span>'.getFullSpace(2).'由於上述物品須供應'.getUnderline(14).'使用，因此，如獲批准，本人會要求供應商在'.getUnderline(4).'年'.getUnderline(3).'月'.getUnderline(3).'日或之前送貨。物品驗收無誤後，即可在'.getUnderline(4).'年'.getUnderline(3).'月'.getUnderline(3).'日付款。</span>';
	$x .= '<br>';
	$x .= '<span>'.getFullSpace(2).'特此聲明，上述物品只屬單一的採購。校方並非把多個部分所組成的物品分單採購。'.'</span>';
	
	$x .= '<table width="100%">';
	$x .= '<tr>';
	$x .= '<td width="12%">報價由：</td>';
	$x .= '<td width="28%" class="underlined"></td>';
	$x .= '<td width="20%">邀請</td>';
	$x .= '<td width="12%">簽署	：</td>';
	$x .= '<td width="28%" class="underlined"></td>';
	$x .= '</tr>';
	$x .= '<tr>';
	$x .= '<td>職級 	：</td>';
	$x .= '<td class="underlined"></td>';
	$x .= '<td></td>';
	$x .= '<td>日期	：</td>';
	$x .= '<td class="underlined"></td>';
	$x .= '</tr>';
	$x .= '</table>';
	$x .= '<br>';
	$x .= '<span>------------------------------------------------</span>';
	$x .= '<br>';
	
	$x .= '<span>*本人證實，口頭報價的程序妥當及對於上述推薦給予批准；或</span>';
	$x .= '<br>';
	$x .= '<span>*本人認為須重新邀請口頭報價；或</span>';
	$x .= '<br>';
	$x .= '<span>*本人不批准上述推薦，原因是'.getUnderline(12).'。</span>';
	$x .= '<br>';
	
	$x .= '<table width="100%">';
	$x .= '<tr>';
	$x .= '<td width="12%">簽署：</td>';
	$x .= '<td width="28%" class="underlined"></td>';
	$x .= '<td width="20%"></td>';
	$x .= '<td width="12%">簽署：</td>';
	$x .= '<td width="28%" class="underlined"></td>';
	$x .= '</tr>';
	$x .= '<tr style="text-align:center;">';
	$x .= '<td></td>';
	$x .= '<td>副校長</td>';
	$x .= '<td></td>';
	$x .= '<td></td>';
	$x .= '<td>校長</td>';
	$x .= '</tr>';
	$x .= '<tr>';
	$x .= '<td>日期：</td>';
	$x .= '<td class="underlined"></td>';
	$x .= '<td></td>';
	$x .= '<td>日期：</td>';
	$x .= '<td class="underlined"></td>';
	$x .= '</tr>';
	$x .= '<tr>';
	$x .= '<td colspan="5">*請刪去不適用者</td>';
	$x .= '</tr>';
	$x .= '</table>';
	$x .= '</body></html>';
	// if($priceComparisonTable){
	// 	$x .= '<br>';
	// 	$x .= '<caption>Price Comparison Table</caption>';
	// 	$x .= $priceComparisonTable;
	// }
	echo $x;
}
?>
