<?php 
/*
 * Change Log:
 * Date: 2017-03-01 Villa	
 * 	-#X100660 add SMC Cust to the filter
 */
$linterface = $_PAGE['libPCM_ui'];
$groupID = $_PAGE['POST']['groupID'];
$roleID = $_PAGE['POST']['roleID'];
$type = $_PAGE['POST']['type'];
// debug_pr($roleID);


//   debug_pr($type);
//   debug_pr($groupID);
//   debug_pr($roleID);

$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');
// $libgroup = $_PAGE['Controller']->requestObject('libgroup','includes/libgroup.php');

//Get Group(ePCM) user: for group only
if($type=='group'){
	$groupInfo = $db->getGroupMemberInfo($groupID);
	$userIDAry = Get_Array_By_Key($groupInfo,'UserID');
}else if($type=='role'){
	$groupInfo = $db->getRoleUser($roleID, $_PAGE["POST"]["academicYearId"]);
	$userIDAry = Get_Array_By_Key($groupInfo,'UserID');
}

//Get Intranet Group / Category
$catInfoArr = $db->getIntranetCategoryInfo();
$catAssoArr = BuildMultiKeyAssoc($catInfoArr,'GroupCategoryID');
$groupCatIdArr = Get_Array_By_Key($catInfoArr,'GroupCategoryID');
$groupArr = $db->getIntranetGroupInfo($groupCatIdArr);

$optionAry = array();
$role = $Lang['SysMgr']['RoleManagement']['UserType'];
$optionAry[$role]["I1"] = $Lang['SysMgr']['RoleManagement']['TeachingStaff'];
$optionAry[$role]["I3"] = $Lang['SysMgr']['RoleManagement']['SupportStaff'];
$optionAry[$role]["P"] = $Lang['SysMgr']['RoleManagement']['Parent'];
//SMC Cust
if($sys_custom['ePCM']['SMC']){
	$optionAry[$role]["SMC"] = $Lang['ePCM']['RoleManagement']['SMC'];
}

foreach((array)$groupArr as $_groupInfo){
    $_groupCatLang = $catAssoArr[$_groupInfo['GroupCategoryID']]['CategoryName'];
    $_optionValue = 'G'.$_groupInfo['GroupID'];
    $_optionLang = Get_Lang_Selection($_groupInfo['TitleChinese'], $_groupInfo['TitleEn']);
    $optionAry[$_groupCatLang][$_optionValue] = $_optionLang;
}

$htmlAry['groupCatSelection'] = getSelectByAssoArray($optionAry, 'name="groupCat" id="groupCat" onchange="Get_User_List();"', $SelectedType, $all=0, $noFirst=1);

?>
<script>
<?php if($singleUser){ ?>
function hideMultiple() {
	 $('.multipleSelectionOnly').hide();
}
<?php } ?>

var AutoCompleteObj;
function Init_JQuery_AutoComplete (inputID) {
	var notAvalUserAry = getNotAvalUser();
	notAvalUserAry = notAvalUserAry.join(",");
// 	alert(notAvalUserAry);
    AutoCompleteObj = $( "#"+inputID ).autocomplete(
    	"index.php",
		{
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1], li.selectValue);
			},
			formatItem: function(row) {
				//alert(row);
				return row[0];
			},
			maxItemsToShow: 100,
			minChars: 1,
			delay: 0,
			width: 200,
			extraParams: {  //to pass extra parameter in ajax file.
		        "p" : "ajax.searchUser",
		        "type":"<?=$type?>",
		        "groupID": "<?=$groupID?>",
		        "roleID": "<?=$roleID?>",
		        "notAvalUserAry" : notAvalUserAry,
		        "includeParent" : 1
		    }
		});
}

function Update_Auto_Complete_Extra_Para(){
//	alert('Update_Auto_Complete_Extra_Para');	
	ExtraPara = new Array();
	ExtraPara['notAvalUserAry'] = getNotAvalUser();
	ExtraPara['groupID'] = '<?= $groupID ?>';
    ExtraPara['p'] ="ajax.searchUser";
    ExtraPara['roleID'] = '<?=$roleID?>';
    ExtraPara['type'] ='<?=$type?>';
    ExtraPara['includeParent'] = 1;
	//ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function Add_Role_Member(){
	var url = '';
	if('<?=$type?>' == 'group'){
		url = "index.php?p=setting.groupAddMember.update";
	}
	else{
		url = "index.php?p=setting.roleAddMember.update";
	}
	
	var selectedUserIDAry = [];
	var selectedAry = $('select[name="AddUserID[]"] option').each(function(){
		selectedUserIDAry.push($(this).val());
	});
// 	alert(selectedUserIDAry);
	selectedUserIDAry = selectedUserIDAry.join(",");
	$('div#TB_ajaxContent').load(
			url, 
		{ 
			userIDAry: selectedUserIDAry,
			groupID: '<?= $groupID ?>',
			roleID: '<?= $roleID ?>',
            academicYearId: '<?=  $_PAGE["POST"]["academicYearId"]?>'
		},
		function(ReturnData) {
			window.top.tb_remove(); 
			location.reload();
			return false;
		}
	);
}

function Get_User_List(){

	//Get Teaching OR Non-teaching	
	var groupCat = $('#groupCat option:selected').val();	
	
	//Get existing selected userID
	var selectedUserIDAry = [];
	$('select[name="AddUserID[]"] option').each(function(){
		selectedUserIDAry.push($(this).val());
	});
//	alert(selectedUserIDAry);
	
	//Get Member in Group 
	var memberInGroup = [];
	$('input[name="groupMemberUserIDAry[]"]').each(function(){
		memberInGroup.push($(this).val());
	});
// 	alert(memberInGroup);
	
	var mergedAry = $.merge(memberInGroup,selectedUserIDAry);
//	alert(mergedAry);
	mergedAry = mergedAry.join(",");
	$('div#AvalUserLayer').load(
			"index.php?p=ajax.refreshUserList", 
			{ 

				groupCat: groupCat,
				userIDAry: mergedAry
			},
			function(ReturnData) {
				$('div#AvalUserLayer').html(ReturnData);
//				Init_JQuery_AutoComplete('UserSearch');
			}
		);
	
}
</script>
<table class="form_table"  style="width: 100%">
	<tr>
		<td><br></td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td width="50%" bgcolor="#EEEEEE">
									<?=$Lang['Group']['GroupCategory'].':'.$htmlAry['groupCatSelection']?>
								</td>
								<td width="40"></td>
								<td width="50%" bgcolor="#EFFEE2" class="steptitletext"><?=$Lang['SysMgr']['RoleManagement']['SelectedUser']?></td>
							</tr>
							<tr>
								<td bgcolor="#EEEEEE" align="center">
									<div id="AvalUserLayer">
	 									<?=$linterface->Get_User_Selection('I1',$userIDAry)?>
    
									</div>
									<span class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></span>
								</td>
								<td><input name="AddAll" onclick="Add_All_User();" type="button"
									class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'"
									value="&gt;&gt;" style="width: 40px;"
									title="<?=$Lang['Btn']['AddAll']?>" /> <br /> <input name="Add"
									onclick="Add_Selected_Class_User();" type="button"
									class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'" value="&gt;"
									style="width: 40px;" title="<?=$Lang['Btn']['AddSelected']?>" />
									<br />
								<br /> <input name="Remove" onclick="Remove_Selected_User();"
									type="button" class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'" value="&lt;"
									style="width: 40px;"
									title="<?=$Lang['Btn']['RemoveSelected']?>'" /> <br /> <input
									name="RemoveAll" onclick="Remove_All_User();" type="button"
									class="formsmallbutton"
									onmouseover="this.className='formsmallbuttonon'"
									onmouseout="this.className='formsmallbutton'"
									value="&lt;&lt;" style="width: 40px;"
									title="<?=$Lang['Btn']['RemoveAll']?>" /></td>
								<td rowspan="2" bgcolor="#EFFEE2" id="SelectedUserCell" class="multipleSelectionOnly"><select
									name="AddUserID[]" id="AddUserID[]" size="10"
									style="width: 99%" multiple="true">
								</select></td>
							</tr>
							<tr>
								<td width="50%" bgcolor="#EEEEEE">
                                    <?=$Lang['SysMgr']['FormClassMapping']['Or']?> <br>
                                    <?=$Lang['SysMgr']['RoleManagement']['SearchUser']?> <br>
									<div style="float: left;">
										<input type="text" id="UserSearch" name="UserSearch" value="" />
									</div>
								</td>
								<td>&nbsp;</td>
							</tr>
						</table>
						<p class="spacer"></p>
					</td>
				</tr>
			</table> <br />
		</td>
	</tr>
</table>
</div>
<div class="edit_bottom">
	<span> </span>
	<p class="spacer"></p>
	<input type="hidden" name="groupID" id="groupID" value="<?$groupID?>">
	<input name="AddMemberSubmitBtn" id="AddMemberSubmitBtn" type="button"
		class="formbutton" onclick="Add_Role_Member(); return false;"
		onmouseover="this.className='formbuttonon'"
		onmouseout="this.className='formbutton'"
		value="<?=$Lang['Btn']['Add']?>" /> <input name="AddMemberCancelBtn"
		id="AddMemberCancelBtn" type="button" class="formbutton"
		onclick="window.top.tb_remove(); return false;"
		onmouseover="this.className='formbuttonon'"
		onmouseout="this.className='formbutton'"
		value="<?=$Lang['Btn']['Cancel']?>" />