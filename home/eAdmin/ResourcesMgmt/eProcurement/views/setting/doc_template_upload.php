<?php 
$template_key = $_PAGE['view_data']['template_key'];
$linterface = $_PAGE['libPCM_ui'];

$caseID = -1;
// for plupload
$editMode = true;
$sessionToken = time();
$pluploadButtonId = 'UploadButton';
$pluploadFileListDivId = 'FileListDiv';
$pluploadContainerId = 'pluploadDiv';
$deleteFileDivId = 'willDeleteFileDiv';
$pluploadBtn = $linterface->GET_SMALL_BTN($Lang['General']['PleaseSelectFiles'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
$fileWarningDiv = $linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv tabletextrequire');

$PAGE_NAVIGATION[] = array($Lang['ePCM']['SettingsArr']['SampleDocumentArr']['MenuTitle'],'javascript: window.location=\'index.php?p=setting.template\';');
$PAGE_NAVIGATION[] = array($Lang['Btn']['Upload']);
$PAGE_NAVIGATION[] = array($Lang['ePCM']['Setting']['Template'][$template_key]);
$htmlAry['navigation'] = $linterface->getNavigation_PCM($PAGE_NAVIGATION);

$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseID);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('session', 'session', $sessionToken);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('template_key', 'template_key', $template_key);
$htmlAry['hidden'] = $hiddenF;

### action buttons
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "ajax_recover_tempRemove();goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<?=$linterface->Include_Plupload_JS_CSS()?>
<script>
var backLocation = 'index.php?p=setting.template';
$(document).ready( function() {
	<?php if($editMode){ ?>
    	ajax_reload_saved_attachment();
	<?php } ?>
	initUploadAttachment('<?=$pluploadContainerId?>','<?=$pluploadButtonId?>');
});

var canSubmit = true;
function goSubmit(){

	if(canSubmit == false){
		return false;
	}
	canSubmit = false;
	
	try {
		var checkResult =  checkForm();
	}
	catch(err) {
// 	    alert(err);
	}

	formAction = $('form#form1').attr('action');
	if(checkResult && formAction != ''){
		$('form#form1').submit();
	}
	else{
// 		alert('missing form checkFrom return result / form action');
		canSubmit = true;
		return false;
	}
}

function goCancel(){
	if(typeof backLocation != 'undefined'){
		window.location = backLocation;
	}
	else{
// 		alert('please add a js global var backLocation');
	}
}

function checkForm(){
	var canSubmit = true; console.log(canSubmit);
	$('#FileWarnDiv').hide();
	// check file upload
	var warnDv = $('#FileWarnDiv');
	if(!jsIsFileUploadCompleted()) {
		warnDv.html('<?=$Lang['ePCM']['General']['jsWaitAllFilesUploaded']?>').show();
		canSubmit = false;
	}
	
	$('form#form1').attr('action', 'index.php?p=setting.template.update');
	return canSubmit;
}

//below are for plupload
var FileCount = 0;
var file_uploader = {};
function initUploadAttachment(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length == 0) return;
	
	file_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'index.php?p=file.process&session=<?=$sessionToken?>',
			max_file_size : '200mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>
	    });
	    
	file_uploader.init();
	
	file_uploader.bind('BeforeUpload', function(up, file){
		FileCount += 1; // global var
	});
	
	file_uploader.bind('FilesAdded', function(up, files) {
		
        $.each(files, function(i, file) {
            $('#'+containerId).append(
                '<div id="' + file.id + '">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
            '</div>');
        });
 		
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });
    
    file_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
	});
	
    file_uploader.bind('UploadComplete', function(up, files) {
		var postData = $('form#form1').serialize();
		var fileCount = files.length;
		files.reverse();
		ajax_update_attachment(fileCount, postData, files);
	});
}

function ajax_update_attachment(numberLeft, postData, files)
{
	var index = numberLeft - 1;
	if(index >= 0) {
		var finalPostData = postData;
		<?php if($is_mobile_tablet_platform){ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['target_name']);
		<?php }else{ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['name']);
		<?php } ?>
		$.post(
			'index.php?p=file.attach.update',
			finalPostData,
			function(data){
				eval(data);
				if(file_uploader.removeFile) {
					$('#' + files[index].id).remove();
					file_uploader.removeFile(files[index]);
				}
				FileCount -= 1; // global var
				numberLeft -= 1;
				if(numberLeft > 0) {
					ajax_update_attachment(numberLeft, postData, files);
				}
			}
		);
	}
}

// for hide original plupload file list, real file list is return from  ajax_update_attachment
function bindClickEventToElements()
{
	$('a').bind('click',afterClickedLinksButtons);
}
var isCloseWindow = true;
function afterClickedLinksButtons()
{
	isCloseWindow = false;
	setTimeout(function(){isCloseWindow=true;},1);
}

function Check_Total_Filesize(CheckNewOnly){
	return true;
}

function ajax_delete_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.attach.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_delete_saved_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function download_attachment(target){
	window.location = "index.php?p=file.download."+target;
}

function ajax_reload_saved_attachment(){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.caseList",
        data: {
			"session" : "<?=$sessionToken?>",
			"caseID"  : "<?=$caseID?>",
			"stageID" : "<?=$template_key?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_recover_tempRemove(){
	postdata = $('form#form1').serialize()
	$.post(
			'index.php?p=file.recover',
			postdata,
			function(response){
			}
	);
}

function jsIsFileUploadCompleted(){
	var totalSelected = file_uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<file_uploader.files.length;i++) {
		if(file_uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	return totalDone == totalSelected;
}
</script>
<?php echo $htmlAry['navigation'];?>
<br>
<form name='form1' id='form1' method="POST">
<div class="table_board">
<table class="form_table_v30">
	<tbody>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['SettingsArr']['SampleDocumentArr']['MenuTitle']?></td>
			<td>
				<?=$pluploadBtn?>
	                <div id="<?=$pluploadContainerId?>"></div>
	                <div id="<?=$pluploadFileListDivId?>"></div>
	                <div id="<?=$deleteFileDivId?>"></div>
	                <?=$fileWarningDiv?>
			</td>
		</tr>
	</tbody>
</table>
<?php echo $htmlAry['hidden']?>
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['submitBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
</div>
</form>