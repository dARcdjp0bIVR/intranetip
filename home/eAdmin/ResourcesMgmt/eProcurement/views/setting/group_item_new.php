<?php 
$linterface = $_PAGE['libPCM_ui'];
$groupID = $_PAGE['groupID'];

$data = $_PAGE['view_data'];
$targetID = $data['targetID'];
$actionLang = $Lang['Btn']['New'];
if($targetID != ''){
	$code = $data['itemInfo'][$targetID]['Code'];
	$itemName = $data['itemInfo'][$targetID]['ItemName'];
	$itemNameChi = $data['itemInfo'][$targetID]['ItemNameChi'];
	$actionLang = $Lang['Btn']['Edit'];
}

$PAGE_NAVIGATION[] = array($Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'],'javascript: window.location=\'index.php?p=setting.group\';');
$PAGE_NAVIGATION[] = array($Lang['ePCM']['Group']['GroupItemName']);
//$PAGE_NAVIGATION[] = array($itemName);
$PAGE_NAVIGATION[] = array($actionLang);
$htmlAry['navigation'] = $linterface->getNavigation_PCM($PAGE_NAVIGATION);

$requireSymbol = $linterface->RequiredSymbol();

$tableContent = array(); 
$tableContent[] = array(
	'title' => $requireSymbol.$Lang['ePCM']['Group']['GroupItemName'],
	'content' => 
		$linterface->GET_TEXTBOX('itemName', 'itemName', $itemName, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
		.$linterface->Get_Form_Warning_Msg('itemNameEmptyWarnDiv', $Lang['ePCM']['Group']['Please input Item Name'], $Class='warnMsgDiv')
	);
$tableContent[] = array(
	'title' => $requireSymbol.$Lang['ePCM']['Group']['GroupItemNameChi'],
	'content' => 
		$linterface->GET_TEXTBOX('itemNameChi', 'itemNameChi', $itemNameChi, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
		.$linterface->Get_Form_Warning_Msg('itemNameEmptyWarnDiv', $Lang['ePCM']['Group']['Please input Item Name'], $Class='warnMsgDiv')
	);
$tableContent[] = array(
	'title' => $requireSymbol.$Lang['General']['Code'],
	'content' => 
		$linterface->GET_TEXTBOX('code', 'code', $code, $OtherClass='requiredField', $OtherPar=array('maxlength'=>5))
		.$linterface->Get_Form_Warning_Msg('codeEmptyWarnDiv', $Lang['ePCM']['Group']['Please input Code'], $Class='warnMsgDiv') 
		.$linterface->Get_Form_Warning_Msg('codeDuplicateWarnDiv', $Lang['ePCM']['Group']['Duplicated code'], $Class='warnMsgDiv')
	);

$tableHTML = $linterface->getFormTableHTML($tableContent);
$htmlAry['formTable'] = $tableHTML;

$hiddenField = '';
$hiddenField .= $linterface->GET_HIDDEN_INPUT('groupID', 'groupID', $groupID);
if($targetID > 0){
	$hiddenField .= $linterface->GET_HIDDEN_INPUT('targetID', 'targetID', $targetID);
}
$htmlAry['hiddenField'] = $hiddenField;
?>
<script>
var backLocation = 'index.php?p=setting.group.item.' + '<?=$groupID?>';

function doBeforeCheckForm(){
	var itemName = $('#itemName').val();
	var itemNameChi = $('#itemNameChi').val();
	if(itemName=='' && itemNameChi!=''){
		$('#itemName').removeClass('requiredField');
	}
	if(itemName!='' && itemNameChi==''){
		$('#itemNameChi').removeClass('requiredField');
	}
}

function checkForm(){
	$('form#form1').attr('action', 'index.php?p=setting.group.item.update');
	
	doBeforeCheckForm();
	
	canSubmit = true;
	var isFocused = false;
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});

	//check duplicate code
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'3',
			'value':$('#code').val().trim(),
			'excluded_value':'<?php echo $code; ?>',
		},
		'success':function(data){
			//alert(data);
			if(data=='DUPLICATED'){
				canSubmit=false;
				$('#codeDuplicateWarnDiv').show();
			}
		},
		'async':false
	});

	
	//alert(canSubmit);
	return canSubmit;
}
</script>
<?=$htmlAry['navigation']?>
<br/>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?php echo $Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle']; ?> </td>
		<td>
			<?php echo $_PAGE['views_data']['group_name']; ?>
		</td>
	</tr>
</table>
<br/>
<?= $htmlAry['formTable']?>
<?= $htmlAry['hiddenField']?>