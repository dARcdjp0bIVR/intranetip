<?php

$linterface = $_PAGE['libPCM_ui'];

$text=$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr'];
$current_ip=isset($_SERVER)?$_SERVER['REMOTE_ADDR']:$HTTP_SERVER_VARS['REMOTE_ADDR'];
$txt_ip_list=$linterface->GET_TEXTAREA("IPList", $_PAGE['IPList']);
$display_value=($_PAGE['IPList']==''?$Lang['General']['EmptySymbol']:nl2br($_PAGE['IPList']));
$instruction=$linterface->Get_Warning_Message_Box($text['InstructionTitle'],$text['InstructionContent']);
?>
<script language="javascript" type="text/javascript">
function checkForm(){
	return true;
}
$(document).ready( function() {
	$('form#form1').attr({'action':'index.php?p=setting.securityupdate.network'});
});
</script>

<?php echo $instruction; ?>
<table class="form_table_v30">
	<tr>
		<td class="field_title">
			<?php echo $text['SettingTitle'];?>
		</td>
		<td>
			<span class="tabletextremark"><?php echo $text['SettingControlTitle'];?>: <?php echo $current_ip; ?></span><br />
			<div class="EditView" style="display:none">
				<?php echo $txt_ip_list; ?>
			</div>
			<div class="DisplayView"><?php echo $display_value; ?></div>
		</td>
	</tr>
</table>