<?php 

$postVar = $_PAGE['views_data'];
$categoryInfo = $postVar['categoryInfo'];
$categoryID = $categoryInfo['CategoryID'];
$categoryName = $categoryInfo['CategoryName'];
$categoryNameChi = $categoryInfo['CategoryNameChi'];
$code = $categoryInfo['Code'];

$navigationAry[] = array($Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.category\';');
if($categoryID == ''){
	$navigationAry[] = array($Lang['Btn']['New']);
}
else{
//	$navigationAry[] = array($categoryName);
	$navigationAry[] = array($Lang['Btn']['Edit']);
}
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

//Hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('categoryID', 'categoryID', $categoryID);

//Table field
$tableContent = ''; 
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Category']['Code'],
	'content'=>$_PAGE['libPCM_ui']->GET_TEXTBOX('code', 'code', $code, $OtherClass='requiredField', $OtherPar=array('maxlength'=>5))
				.$_PAGE['libPCM_ui']->Get_Form_Warning_Msg('codeEmptyWarnDiv', $Lang['ePCM']['Category']['Please input Code'], $Class='warnMsgDiv') 
				.$_PAGE['libPCM_ui']->Get_Form_Warning_Msg('codeDuplicateWarnDiv', $Lang['ePCM']['Category']['Duplicated code'], $Class='warnMsgDiv') 
	);
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Category']['CategoryName'],
	'content'=>$_PAGE['libPCM_ui']->GET_TEXTBOX('categoryName', 'categoryName', $categoryName, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
				.$_PAGE['libPCM_ui']->Get_Form_Warning_Msg('categoryNameEmptyWarnDiv', $Lang['ePCM']['Category']['Please input Category Name'], $Class='warnMsgDiv')
	);
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Category']['CategoryNameChi'],
	'content'=>$_PAGE['libPCM_ui']->GET_TEXTBOX('categoryNameChi', 'categoryNameChi', $categoryNameChi, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
				.$_PAGE['libPCM_ui']->Get_Form_Warning_Msg('categoryNameEmptyWarnDiv', $Lang['ePCM']['Category']['Please input Category Name'], $Class='warnMsgDiv')
	);

echo $_PAGE['libPCM_ui']->Include_Thickbox_JS_CSS();
?>
<script>
var backLocation = 'index.php?p=setting.category';

function doBeforeCheckForm(){
	var categoryName = $('#categoryName').val();
	var categoryNameChi = $('#categoryNameChi').val();
	if(categoryName=='' && categoryNameChi!=''){
		$('#categoryName').removeClass('requiredField');
	}
	if(categoryName!='' && categoryNameChi==''){
		$('#categoryNameChi').removeClass('requiredField');
	}
}

function checkForm(){
	$('form#form1').attr('action', 'index.php?p=setting.category.update');
	
	doBeforeCheckForm();
	
	$('.warnMsgDiv').hide();
	
	canSubmit = true;
	var isFocused = false;
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	
	//check duplicate code
	$.ajax({
		'type':'POST',
		'url':'index.php?p=ajax.checkUnique',
		'data':{
			'profile':'1',
			'value':$('#code').val().trim(),
			'excluded_value':'<?php echo addslashes($code); ?>',
		},
		'success':function(data){
			//alert(data);
			if(data=='DUPLICATED'){
				canSubmit=false;
				$('#codeDuplicateWarnDiv').show();
			}
		},
		'async':false
	});
	
	//alert(canSubmit);
	return canSubmit;
}
</script>
<?=$navigation?>
<table class="form_table_v30">
	<?php foreach($tableContent as $content){ ?>
	<tr>
		<td class="field_title"><?php echo $content['title'] ?></td>
		<td>
			<?php echo $content['content'] ?>
		</td>
	</tr>
	<?php } ?>
</table>
<?= $hiddenF ?>
<a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);" onClick="load_dyn_size_thickbox_ip('Title here', 'onloadThickBox();')" style="display:none;">Dynamic size</a>