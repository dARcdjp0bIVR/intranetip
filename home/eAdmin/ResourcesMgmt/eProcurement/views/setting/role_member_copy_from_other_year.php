<?php
$dbSetting = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');

##Academic YearID With Members
$sql = 'Select AcademicYearID  From INTRANET_PCM_ROLE_USER WHERE IsDeleted=0 & IsActive=1 group by AcademicYearID ';
$academicYearIDWithMember = $dbSetting->returnResultSet($sql);
foreach($academicYearIDWithMember as $i=>$academicYearID){
    $academicYearIDWithMember[$i]= $academicYearIDWithMember[$i]['AcademicYearID'];
}

##Academic Year selection
$yearSelection1 .= $_PAGE['libPCM_ui']->getSelectAcademicYear('roleAcademicYearIdFrom','', 1, 0, $academicYearId)."\r\n";
$yearSelection2 .= $_PAGE['libPCM_ui']->getSelectAcademicYear('roleAcademicYearIdTo','', 1, 0, $academicYearId,'','','','',$academicYearIDWithMember)."\r\n";

#button
$htmlAry['submitBtn'] = $_PAGE['libPCM_ui']->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$htmlAry['cancelBtn'] = $_PAGE['libPCM_ui']->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "goCancel()", 'cancelBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");



?>
<script>
    function goSubmit(){
        $('form#form1').submit();
    }
    function goCancel(){
        window.location="index.php?p=setting.role&academicYearId="+<?php echo $_GET['academicYearId']; ?>;
    }
</script>
<form method="POST" name="form1" id="form1" action="index.php?p=setting.roleAddMember.RoleUpdateCopyFromOtherYear">
    <table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
            <td align='right'><?=$_PAGE['libPCM_ui']->GET_SYS_MSG($xmsg,$xmsg2);?></td>
        </tr>
        <tr>
            <td><?= $_PAGE['libPCM_ui']->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
        </tr>
        <tr>
            <td><?/*= $linterface->GET_STEPS($STEPS_OBJ) */?></td>
        </tr>
        <tr>
            <td>
                <table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td><br />
                            <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr>
                                    <td class="formfieldtitle" align="left" width="30%"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear']?></td>
                                    <td class="tabletext"><?=$yearSelection1?></td>
                                </tr>
                                <tr>
                                    <td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear']?> </td>
                                    <td class="tabletext"><?=$yearSelection2?></td>
                                </tr>
                                <tr>
                                    <td height="1" class="dotline" colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                        <?=$htmlAry['submitBtn']?>
                                        <?=$htmlAry['cancelBtn'] ?>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form>