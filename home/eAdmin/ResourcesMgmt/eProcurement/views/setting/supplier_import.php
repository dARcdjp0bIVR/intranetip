<?php

$linterface = $_PAGE['libPCM_ui'];
$requiredSymbol = $linterface->RequiredSymbol();

### navigation
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['SupplierArr']['TabTitle'], 'javascript: window.location=\'index.php?p=setting.supplierType\';');
$navigationAry[] = array($Lang['Btn']['Import']);

$htmlAry['navigation'] = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

$htmlAry['StepsBox'] = $linterface->GET_IMPORT_STEPS($CurrStep=1, $CustStepArr='');

$ColumnTitleArr = array();
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name Chi'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Website'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Description'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Fax'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Email'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierArr']['Type'];
$ColumnPropertyArr = array(1,1,0,0,1,0,0,1);
$RemarksArr[7] = '<a id="supplyType" class="tablelink" href="javascript:Load_Reference(\'supplyType\');">['.$Lang['ePCM']['SettingsArr']['SupplierArr']['Import']['Reference']['Type'].']</a>';
$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
$FileDescription = $ImportPageColumn;

$sampleLink = "<a class=\"tablelink\" href=\"views/setting/import_eProcurement_supplier_sample.csv\" download=\"import_eProcurement_supplier_type_sample.csv\">[".$Lang['General']['ClickHereToDownloadSample']."]</a><br>";
?>
<script>
var backLocation = 'index.php?p=setting.supplier';
function checkForm(){
	var canSubmit = true;
	$('form#form1').attr({'action':'index.php?p=setting.supplier.importUpdate'});

	return canSubmit;
}
function Load_Reference(ref) {
	$('#remarkDiv_type').html('');
	
	$.post(
		"index.php?p=ajax.import.load_reference", 
		{ 
			Action: "load_reference",
			flag: ref
		},
		function(ReturnData)
		{
			$('#remarkDiv_type').html(ReturnData);
		}
	);
	displayReference(ref);
}
function displayReference(ref) {
	var p = $("#"+ref);
	var position = p.position();
	
	var t = position.top + 15;
	var l = position.left + p.width()+5;
	$("#remarkDiv_type").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
}
function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}
function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}
</script>

<?=$htmlAry['navigation']?>
<p class="spacer"></p>
<?=$htmlAry['StepsBox']?>
<div class="table_board">
	<table  class="form_table_v30" style="width:85%; margin:0 auto;">
		<tr>
			<td class="field_title"><?=$requiredSymbol.$Lang['General']['SourceFile'] ?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat'] ?></span></td>
			<td><input class="file" type="file" name="userfile"></td>
		</tr>
		<tr>
			<td class="field_title"><?=	$Lang['General']['CSVSample'] ?></td>
			<td><?=$sampleLink ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['General']['ImportArr']['DataColumn'] ?></td>
			<td><?=$FileDescription ?></td>
		</tr>
	</table>
</div>
<div id="remarkDiv_type" class="selectbox_layer" style="width:400px"></div>