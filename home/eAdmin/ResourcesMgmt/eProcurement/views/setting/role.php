<?php
$dbSetting = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');

$academicYearId=$_GET["academicYearId"];
if($academicYearId=='') {
    $settings = $dbSetting->getGeneralSettings();
    $academicYearId = $settings['defaultAcademicYearID'];
}

$dbSetting->roleYearID = $academicYearId;
$postVar = $_PAGE['views_data'];
$order = $postVar['order'];
$field = $postVar['field'];
$pageNo = $postVar['pageNo'];
$page_size = $postVar['numPerPage'];
$columns = $postVar['columns'];
$btnAry = $postVar['btnAry'];
$dbTableBtnAry = $postVar['dbTableBtnAry'];
$sql = $postVar['sql'];
$keyword = $postVar['keyword'];


$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
$pageNo = ($postVar['pageNo'] == '') ? 1 : $postVar['pageNo'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];


$columns = array();
$columns[] = array('RoleName',$Lang['ePCM']['Setting']['Role']['Role'],'60%');
$columns[] = array('MemberCount',$Lang['ePCM']['Setting']['Role']['Member'],'40%');

$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size);
### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);
$htmlAry['hiddenField'] = $hiddenF;


### Buttons
$btnAry[] = array('new', 'javascript: addNew();');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);
$htmlAry['contentTool'].='<div class="Conntent_tool"><div><a href="javascript:copyFromOtherYear()" class="copy">' . $Lang['ePCM']['Setting']['Role']['CopyFromOtherAcademicYear'] . '</a></div></div>';

## Search Box
$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);

##Academic Year selection
$yearSelection .= $_PAGE['libPCM_ui']->getSelectAcademicYear('roleAcademicYearID','', 1, 0, $academicYearId)."\r\n";

### DB table action buttons
##dbTable button array
// $dbTableBtnAry[] = array($btnClass, $btnHref, $displayLang);
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
//$dbTableBtnAry[] = array('other', 'javascript: void(0);', 'Inactivate');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);



?>
<script>
function changeRoleYear(){
    var yearSelected = $('select[name=roleAcademicYearID]').val();
    $('form#form1').attr('action', 'index.php?p=setting.role&academicYearId='+yearSelected);
    $('form#form1').submit();
}
function copyFromOtherYear() {
    var yearSelected = $('select[name=roleAcademicYearID]').val();
    window.location="index.php?p=setting.roleAddMember.copyFromOtherYear&academicYearId="+yearSelected;
}
function addNew(){
	location.href = ("index.php?p=setting.role.new");
}
function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Role']['Warning']['ChooseARole'] ?>");
		return;
	}else if(checked.length!=1){
		alert("<?=$Lang['ePCM']['Setting']['Role']['Warning']['ChooseNoMoreThanARole'] ?>");
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=setting.role.edit');
		$('form#form1').submit();
	}
}
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Role']['Warning']['ChooseOneOrMoreRole'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.role.delete');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
</script>
<form id="form1" name="form1" method="POST" action="">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	<div class="table_filter">
		<?= $yearSelection ?>
	</div>
	<p class="spacer"></p>
	<br style="clear:both;" />
	<br style="clear:both;" />
	<?=$htmlAry['dbTableActionBtn']?>
	<?= $libdbtable?>
	
	<?= $htmlAry['hiddenField']?>
</form>
<script>
document.getElementById("roleAcademicYearID").onchange = function(){changeRoleYear();};
</script>