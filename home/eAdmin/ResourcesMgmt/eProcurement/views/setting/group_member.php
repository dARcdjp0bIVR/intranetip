<?php

echo $_PAGE['libPCM_ui']->Include_Thickbox_JS_CSS();
echo $_PAGE['libPCM_ui']->Include_AutoComplete_JS_CSS();
echo $_PAGE['libPCM_ui']->Include_JS_CSS();

$postVar = $_PAGE['views_data'];
$sql = $postVar['sql'];
$groupInfo = $postVar['groupInfo'];
$groupMemberInfo = $postVar['groupMemberInfo'];
$groupMemberUserIDAry = Get_Array_By_Key($groupMemberInfo,'UserID');

$groupID = $groupInfo['GroupID'];
$groupName = $groupInfo['GroupName'];
$code = $groupInfo['Code'];



## section title
$htmlAry['sectionNavigation'] = $_PAGE['libPCM_ui']->GET_NAVIGATION2_IP25($Lang['ePCM']['Group']['Member']);

//debug_pr($postVar);

$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
$pageNo = ($postVar['pageNo'] == '') ? 1 : $postVar['pageNo'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];


$columns = array();
$columns[] = array('UserID',$Lang['ePCM']['Group']['Member'],'100%');

$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size,1);
	
### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);

$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('groupID', 'groupID', $groupID);
foreach($groupMemberUserIDAry as $userID){
	$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('groupMemberUserIDAry[]', 'groupMemberUserIDAry[]', $userID);
}
$htmlAry['hiddenField'] = $hiddenF;


### Buttons
## Button Array
$btnAry[] = array('new', 'javascript: addNew();');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

//## Search Box
//$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);

### DB table action buttons
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('other', 'javascript: goSetAdmin();', $Lang['ePCM']['Group']['SetGroupHead']);
$dbTableBtnAry[] = array('other', 'javascript: goRemoveAdmin();', $Lang['ePCM']['Group']['RemoveGroupHead']);
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

### navigation
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.group\';');
$navigationAry[] = array($groupName);
$navigationAry[] = array($Lang['ePCM']['Setting']['Group']['Edit Member']);
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

?>
<script>
$(document).ready(function(){
	
});

function addNew(){
	//Get thick box ui
	load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['Setting']['Group']['AddMember']?>', 'onloadThickBox();', inlineID='', defaultHeight=450, defaultWidth=600);
}

function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupMember'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.groupMemberDelete.<?= $groupID ?>');
		$('form#form1').submit();
	}
}
function goSetAdmin(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupMember'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['Group']['Warning']['ConfirmSetToAdmin'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.groupMemberAssignAdmin.<?= $groupID ?>.1');
		$('form#form1').submit();
	}
} 
function goRemoveAdmin(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupMember'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['Group']['Warning']['ConfirmRemoveFromAdmin'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.groupMemberAssignAdmin.<?= $groupID ?>.0');
		$('form#form1').submit();
	}
} 
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
function onloadThickBox() {

	$('div#TB_ajaxContent').load(
		"index.php?p=setting.groupAddMember", 
		{ 
			groupID: <?= $groupID ?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			Init_JQuery_AutoComplete('UserSearch');
		}
	);
}
function Add_All_User(){
//	alert('Add_All_User');
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	// union selected and newly add user list
	if (ClassUser.length > UserSelected.length) {
		var cloneCell = document.getElementById('SelectedUserCell');

		// clone element from avaliable user list
		var cloneFrom = ClassUser.cloneNode(1);
		cloneFrom.setAttribute('id','AddUserID[]');
		cloneFrom.setAttribute('name','AddUserID[]');
		var j=0;
		for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			User = UserSelected.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      cloneFrom.add(elOptNew, cloneFrom.options[j]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      cloneFrom.add(elOptNew, j); // IE only
	    }
			UserSelected.options[i] = null;
			j++;
		}

		cloneCell.replaceChild(cloneFrom,UserSelected);
	}
	else {
		for (var i = (ClassUser.length -1); i >= 0 ; i--) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//cloneFrom.options[cloneFrom.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	// update auto complete parameter list
	Update_Auto_Complete_Extra_Para();

	// empty the avaliable user list
	document.getElementById('AvalUserLayer').innerHTML = '<select name="AvalUserList[]" id="AvalUserList[]" size="10" style="width:99%" multiple="true"></select>';
	/*for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		User = ClassUser.options[i];
		UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
		ClassUser.options[i] = null;
	}

	Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();*/
	
//	Init_JQuery_AutoComplete('UserSearch');
}
function Add_Selected_Class_User(){
//	alert('Add_Selected_Class_User');
	var ClassUser = document.getElementById('AvalUserList[]');
	var UserSelected = document.getElementById('AddUserID[]');
	//var IdentityType = document.getElementById('IdentityType').options[document.getElementById('IdentityType').selectedIndex].value;

	for (var i = (ClassUser.length -1); i >= 0 ; i--) {
		if (ClassUser.options[i].selected) {
			User = ClassUser.options[i];
			var elOptNew = document.createElement('option');
	    elOptNew.text = User.text;
	    elOptNew.value = User.value;
			//UserSelected.options[UserSelected.length] = new Option(User.text+'('+IdentityType+')',User.value);
			//UserSelected.options[UserSelected.length] = new Option(User.text,User.value);
			try {
	      UserSelected.add(elOptNew, UserSelected.options[UserSelected.length]); // standards compliant; doesn't work in IE
	    }
	    catch(ex) {
	      UserSelected.add(elOptNew, UserSelected.length); // IE only
	    }
			ClassUser.options[i] = null;
		}
	}

	//Reorder_Selection_List('AddUserID[]');

	Update_Auto_Complete_Extra_Para();
//	Init_JQuery_AutoComplete('UserSearch');
}
function Remove_Selected_User(){
//	alert('Remove_Selected_User');
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
		if (UserSelected.options[i].selected)
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
//	Init_JQuery_AutoComplete('UserSearch');
}
function Remove_All_User(){
//	alert('Remove_All_User');
	var UserSelected = document.getElementById('AddUserID[]');

	for (var i = (UserSelected.length -1); i >= 0 ; i--) {
			UserSelected.options[i] = null;
	}

	Get_User_List();
	Update_Auto_Complete_Extra_Para();
//	Init_JQuery_AutoComplete('UserSearch');
}
// function Add_Role_Member(){
// //	alert('Add_Role_Member');
// 	var selectedUserIDAry = [];
// 	var selectedAry = $('select[name="AddUserID[]"] option').each(function(){
// 		selectedUserIDAry.push($(this).val());
// 	});
// //	alert(selectedUserIDAry);
// 	selectedUserIDAry = selectedUserIDAry.join(",");
// //	alert(selectedUserIDAry);
// 	$('div#TB_ajaxContent').load(
// 		"index.php?p=ajax.new.groupMemberQuery", 
// 		{ 
// 			userIDAry: selectedUserIDAry,

// 		},
// 		function(ReturnData) {
// //			alert('done');
// 			window.top.tb_remove(); 
// 			location.reload();
// 			return false;
// 		}
// 	);
// }
// function Get_User_List(){
// //	alert('Get_User_List');
// //	Init_JQuery_AutoComplete('UserSearch');
// 	//Get Teaching OR Non-teaching	
// 	if($('#IdentityType option:selected').val()=='Teaching'){
// 		teaching = 1;
// 	}else{
// 		teaching = 0;
// 	}
	
// 	//Get existing selected userID
// 	var selectedUserIDAry = [];
// 	$('select[name="AddUserID[]"] option').each(function(){
// 		selectedUserIDAry.push($(this).val());
// 	});
// //	alert(selectedUserIDAry);
	
// 	//Get Member in Group 
// 	var memberInGroup = [];
// 	$('input[name="groupMemberUserIDAry[]"]').each(function(){
// 		memberInGroup.push($(this).val());
// 	});
// //	alert(memberInGroup);
	
// 	var mergedAry = $.merge(memberInGroup,selectedUserIDAry);
// //	alert(mergedAry);
// 	mergedAry = mergedAry.join(",");
// 	$('div#AvalUserLayer').load(
// 			"index.php?p=ajax.new.groupMemberUserSelection", 
// 			{ 

// 				teaching: teaching,
// 				userIDAry: mergedAry
// 			},
// 			function(ReturnData) {
// 				$('div#AvalUserLayer').html(ReturnData);
// //				Init_JQuery_AutoComplete('UserSearch');
// 			}
// 		);
	
// }
// function Update_Auto_Complete_Extra_Para(){
// //	alert('Update_Auto_Complete_Extra_Para');	
// 	ExtraPara = new Array();
// 	ExtraPara['SelectedUserIDList'] = getNotAvalUser();

// 	//ExtraPara['SelectedUserIDList'] = Get_Selection_Value('target[]', 'Array', true);
// 	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
// }
function Add_Selected_User(UserID, UserName){
//	alert(UserID);
//	alert(UserName);
	UserID = UserID.replace("U","");
//	alert(UserID);
	$('select[name="AddUserID[]"]').append("<option value="+ UserID +">"+ UserName +"</option>");

	//clear search box
	$('#UserSearch').val('');
	Get_User_List();
    Update_Auto_Complete_Extra_Para();
//	Init_JQuery_AutoComplete('UserSearch');
}
function getNotAvalUser(){
	var notavalUser = [];
	$('select[name="AddUserID[]"] option').each(function(){
		notavalUser.push($(this).val());
	});
//	alert(notavalUser);
	return notavalUser;
}

</script>
<?php echo $navigation; ?>
<br/>
<form id="form1" name="form1" method="POST" action="">
<div class="table_board">
	<?php //echo $htmlAry['sectionNavigation']?>
	<br style="clear:both;" />
	
	<table class="form_table_v30">
		<tr> 
			<td class="field_title"><?= $Lang['ePCM']['Group']['GroupName'] ?></td>
			<td><?=$groupName?></td>
		</tr>
		<tr>
			<td class="field_title"><?= $Lang['General']['Code'] ?></td>
			<td><?=$code?></td>
		</tr>
	</table>
	<div class="table_filter">
	<?= $filter ?>
	</div>
	<p class="spacer"></p>
	<br style="clear:both;" />
	<br style="clear:both;" />
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	<?=$htmlAry['dbTableActionBtn']?>
	<?= $libdbtable?>
	<br/>
	<span class="tabletextremark">
		<?php echo $Lang['ePCM']['Group']['GroupHeadRemarks']; ?>
	</span>
</div>
<?= $htmlAry['hiddenField']?>
</form>
