<?php 
$linterface = $_PAGE['libPCM_ui'];
?>
<script>
function goUpload(target){
	window.location = 'index.php?p=setting.template.upload.' + target;
}
</script>
<table class="common_table_list_v30">
	<thead>
		<tr class="tabletop">
			<th width="20%"><?php echo $Lang['ePCM']['Template'] ?></th>
			<th><?php echo $Lang['General']['File'] ?></th>
		<tr>
	</thead>
	<tbody>
	<?php
	foreach((array)$ePCMcfg['Template'] as $template_key){
		echo '<tr>';
		echo '<td><a href="javascript:goUpload('.$template_key.')" class="icon_edit_dim">'.$Lang['ePCM']['Setting']['Template'][$template_key].'</a><div class="table_row_tool row_content_tool"><a href="javascript:goUpload('.$template_key.')" class="icon_edit_dim"></a></div></td>';
		echo '<td>'.$linterface->getFileListUI($_PAGE['view_data']['fileArr'][$template_key], $displaySize=false).'</td>';
		echo '</tr>';
	}
	?>
	</tbody>
</table>