<?php

$linterface = $_PAGE['libPCM_ui'];
$requiredSymbol = $linterface->RequiredSymbol();

### navigation
$navigationAry[] = array($Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TabTitle'], 'javascript: window.location=\'index.php?p=setting.supplierType\';');
$navigationAry[] = array($Lang['Btn']['Import']);

$htmlAry['navigation'] = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

$htmlAry['StepsBox'] = $linterface->GET_IMPORT_STEPS($CurrStep=1, $CustStepArr='');

$ColumnTitleArr = array();
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeNameChi'];
$ColumnTitleArr[] = $Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeName'];
$ColumnPropertyArr = array(1,1);
$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
$FileDescription = $ImportPageColumn;

$sampleLink = "<a class=\"tablelink\" href=\"views/setting/import_eProcurement_supplier_type_sample.csv\" download=\"import_eProcurement_supplier_type_sample.csv\">[".$Lang['General']['ClickHereToDownloadSample']."]</a><br>";

?>
<script>
var backLocation = 'index.php?p=setting.supplierType';
function checkForm(){
	var canSubmit = true;
	$('form#form1').attr({'action':'index.php?p=setting.supplierType.importUpdate'});

	return canSubmit;
}
</script>

<?=$htmlAry['navigation']?>
<p class="spacer"></p>
<?=$htmlAry['StepsBox']?>
<div class="table_board">
	<table  class="form_table_v30" style="width:85%; margin:0 auto;">
		<tr>
			<td class="field_title"><?=$requiredSymbol.$Lang['General']['SourceFile'] ?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat'] ?></span></td>
			<td><input class="file" type="file" name="userfile"></td>
		</tr>
		<tr>
			<td class="field_title"><?=	$Lang['General']['CSVSample'] ?></td>
			<td><?=$sampleLink ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['General']['ImportArr']['DataColumn'] ?></td>
			<td><?=$FileDescription ?></td>
		</tr>
	</table>
</div>
