<?php 

$postVar = $_PAGE['views_data'];
$roleInfo = $postVar['roleInfo'];


$roleID = $roleInfo['RoleID'];
$roleName = $roleInfo['RoleName'];
$roleNameChi = $roleInfo['RoleNameChi'];
$forCaseApproval = $roleInfo['ForCaseApproval'];
$forQuotationApproval= $roleInfo['ForQuotationApproval'];
$forTenderAuditing = $roleInfo['ForTenderAuditing'];
$forTenderApproval = $roleInfo['ForTenderApproval'];
$isPrincipal = $roleInfo['IsPrincipal'];

$navigationAry[] = array($Lang['ePCM']['SettingsArr']['RoleArr']['MenuTitle'], 'javascript: window.location=\'index.php?p=setting.role\';');
if($roleID == ''){
	$navigationAry[] = array($Lang['Btn']['New']);
}
else{
//	$navigationAry[] = array($roleName);
	$navigationAry[] = array($Lang['Btn']['Edit']);
}
$navigation = $_PAGE['libPCM_ui']->getNavigation_PCM($navigationAry);

if(!$forCaseApproval||!$forQuotationApproval||!$forTenderAuditing||!$forTenderApproval){
	$selectAllCheck = false;
}else{
	$selectAllCheck = true;
}


// Permission Checkbox table
// $checkboxAry[] = array($id, $name, $value, $checked, $class, $displayLang, $onclick);
$checkboxAry = array();
$checkboxAry[] = array('forCaseApproval', 'forCaseApproval', 1, $forCaseApproval, 'checkboxGroup', $Lang['ePCM']['Setting']['Rule']['CaseApproval']);
$checkboxAry[] = array('forQuotationApproval', 'forQuotationApproval', 1, $forQuotationApproval, 'checkboxGroup', $Lang['ePCM']['Setting']['Rule']['QuotationApproval']);
$checkboxAry[] = array('forTenderAuditing', 'forTenderAuditing', 1, $forTenderAuditing, 'checkboxGroup', $Lang['ePCM']['Setting']['Rule']['TenderAuditing']);
$checkboxAry[] = array('forTenderApproval', 'forTenderApproval', 1, $forTenderApproval, 'checkboxGroup', $Lang['ePCM']['Setting']['Rule']['TenderApproval']);
$x = '<tr>'."\r\n";
$x .= '<td class="field_title">'.$Lang['ePCM']['Setting']['Role']['Responsibility'].'</td>'."\r\n";
$x .= '<td>'."\r\n";
$x .= $_PAGE['libPCM_ui']->Get_Checkbox_Table('checkboxGroup', $checkboxAry, $itemPerRow=4, $selectAllCheck);
$x .= '</td>'."\r\n";
$x .= '</tr>'."\r\n";
$permissionCheckBox = $x;

//Hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('roleID', 'roleID', $roleID);

if($isPrincipal){
	$principal_yes = 1;
	$principal_no = 0;
}else{
	$principal_yes = 0;
	$principal_no = 1;
}

//$_PAGE['POST']['abc'];
//Table field
$tableContent = ''; 
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Setting']['Role']['RoleName'],
	'content'=>$_PAGE['libPCM_ui']->GET_TEXTBOX('roleName', 'roleName', $roleName, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
				.$_PAGE['libPCM_ui']->Get_Form_Warning_Msg('roleNameEmptyWarnDiv', $Lang['ePCM']['Setting']['Role']['Please input Role Name'], $Class='warnMsgDiv')
	);
$tableContent[] = array(
	'title'=>$_PAGE['libPCM_ui']->RequiredSymbol().$Lang['ePCM']['Setting']['Role']['RoleNameChi'],
	'content'=>$_PAGE['libPCM_ui']->GET_TEXTBOX('roleNameChi', 'roleNameChi', $roleNameChi, $OtherClass='requiredField', $OtherPar=array('maxlength'=>255))
				.$_PAGE['libPCM_ui']->Get_Form_Warning_Msg('roleNameEmptyWarnDiv', $Lang['ePCM']['Setting']['Role']['Please input Role Name'], $Class='warnMsgDiv')
	);

	

echo $_PAGE['libPCM_ui']->Include_Thickbox_JS_CSS();
?>
<script>
backLocation = 'index.php?p=setting.role';

function doBeforeCheckForm(){
	var roleName = $('#roleName').val();
	var roleNameChi = $('#roleNameChi').val();
	if(roleName=='' && roleNameChi!=''){
		$('#roleName').removeClass('requiredField');
	}
	if(roleName!='' && roleNameChi==''){
		$('#roleNameChi').removeClass('requiredField');
	}
}

function checkForm(){
	$('form#form1').attr('action', 'index.php?p=setting.role.update');
	
	doBeforeCheckForm();
	
	canSubmit = true;
	var isFocused = false;
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	//alert(canSubmit);
	return canSubmit;
}
</script>
<?= $navigation ?>
<table class="form_table_v30">
	<?php foreach($tableContent as $content){ ?>
	<tr>
		<td class="field_title"><?php echo $content['title'] ?></td>
		<td>
			<?php echo $content['content'] ?>
		</td>
	</tr>
	<?php } ?>
	<?= $permissionCheckBox ?>

</table>
<?= $hiddenF ?>
<a id="dynSizeThickboxLink" class="tablelink" href="javascript:void(0);" onClick="load_dyn_size_thickbox_ip('Title here', 'onloadThickBox();')" style="display:none;">Dynamic size</a>