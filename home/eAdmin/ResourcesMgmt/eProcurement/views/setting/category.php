<?php
$postVar = $_PAGE['views_data'];
$order = $postVar['order'];
$field = $postVar['field'];
$pageNo = $postVar['pageNo'];
$page_size = $postVar['numPerPage'];
$columns = $postVar['columns'];
$btnAry = $postVar['btnAry'];
$dbTableBtnAry = $postVar['dbTableBtnAry'];
$sql = $postVar['sql'];
$keyword = $postVar['keyword'];

$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
$pageNo = ($postVar['page_size_change'] == '') ? 1 : $postVar['page_size_change'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];

$columns = array();
$columns[] = array('Code',$Lang['ePCM']['Category']['Code'],'20%');
$columns[] = array('CategoryName',$Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'],'80%');
$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size);

### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);
$htmlAry['hiddenField'] = $hiddenF;

### Buttons
$btnAry = array();
$btnAry[] = array('new', 'javascript: addNew();');
$btnAry[] = array('import', 'index.php?p=setting.import.step1.category');
$btnAry[] = array('export', "javascript: goExport('index.php?p=setting.export.category')");
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

## Search Box
$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);

### DB table action buttons
##dbTable button array
// $dbTableBtnAry[] = array($btnClass, $btnHref, $displayLang);
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: goEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
//$dbTableBtnAry[] = array('other', 'javascript: void(0);', 'Inactivate');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

?>
<script>

function addNew(){
	location.href = ("index.php?p=setting.category.new");
}
function goEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Category']['Warning']['ChooseACategory'] ?>");
		return;
	}else if(checked.length!=1){
		alert("<?=$Lang['ePCM']['Category']['Warning']['ChooseNoMoreThanACategory'] ?>");
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=setting.category.edit');
		$('form#form1').submit();
	}
}
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Category']['Warning']['ChooseOneOrMoreCategory'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.category.delete');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}

function goExport(actionUrl){
    $('form#form1').attr('action', actionUrl);
    $('form#form1').submit();
    $('form#form1').attr('action', 'index.php?p=setting.category');
}
</script>
<form id="form1" name="form1" method="POST" action="index.php?p=setting.category">

	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
	<div class="table_filter">
		<?= $filter ?>
	</div>
	<p class="spacer"></p>
	<br style="clear:both;" />
	<br style="clear:both;" />
		
	<?=$htmlAry['dbTableActionBtn']?>
	<?= $libdbtable ?>
	<?= $htmlAry['hiddenField']?>
</form>