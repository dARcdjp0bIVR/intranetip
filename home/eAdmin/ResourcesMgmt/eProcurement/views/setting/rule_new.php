<?php
echo $_PAGE['libPCM_ui']->Include_Thickbox_JS_CSS();
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$ruleInfo = $postVar['ruleInfo'];
$sessionToken = time();

if(!empty($ruleInfo)){
	$json = $_PAGE['Controller']->requestObject('JSON_obj','includes/json.php');
	$approvalRoleSortJson = $json->encode(array_keys($json->decode($ruleInfo['CaseApprovalRule'])));
	if($ruleInfo['CustCaseApprovalRule'] != ''){
		$custApprovalRoleSortJson = $json->encode(array_keys($json->decode($ruleInfo['CustCaseApprovalRule'])));
	}
}

$forCaseApprovalGroup = $postVar['forCaseApprovalGroup'];
$forQuotationApprovalGroup = $postVar['forQuotationApprovalGroup'];
$forTenderAuditingGroup = $postVar['forTenderAuditingGroup'];
$forTenderApprovalGroup = $postVar['forTenderApprovalGroup'];
$principalInfoAry = $postVar['principalInfoAry'];

### Convert case Approval to -1,-2 is 上級
if($ruleInfo['NeedGroupHeadApprove']==0){
	//nothing happen
	if($ruleInfo['NeedVicePrincipalApprove']){
		$ruleInfo['ApprovalRoleID'] = -5;
	}
}else if($ruleInfo['NeedGroupHeadApprove']==1){
	if($ruleInfo['ApprovalRoleID']==0){
		if($ruleInfo['NeedVicePrincipalApprove']){
			//上級 and vice prin
			$ruleInfo['ApprovalRoleID'] = -3;
		}else{
			//上級only
			$ruleInfo['ApprovalRoleID'] = -1;
		}
	}else{
		if($ruleInfo['NeedVicePrincipalApprove']){
			//上級 and vice prin 及校長
			$ruleInfo['ApprovalRoleID'] = -4;
		}else{
			//上級及校長
			$ruleInfo['ApprovalRoleID'] = -2;
		}
		
	}
}
$quotationType = $ruleInfo['QuotationType'];
if($quotationType=='N'){
	$isCheckQuotationNo = 1;
	$isCheckQuotationQuote = 0;
	$isCheckQuotationTender = 0;
	$isCheckQuotationVerbal = 0;
}else if($quotationType=='Q'){
	$isCheckQuotationNo = 0;
	$isCheckQuotationQuote = 1;
	$isCheckQuotationTender = 0;
	$isCheckQuotationVerbal = 0;
}else if($quotationType=='T'){
	$isCheckQuotationNo = 0;
	$isCheckQuotationQuote = 0;
	$isCheckQuotationTender = 1;
	$isCheckQuotationVerbal = 0;
}else if($quotationType=='V'){
	$isCheckQuotationNo = 0;
	$isCheckQuotationQuote = 0;
	$isCheckQuotationTender = 0;
	$isCheckQuotationVerbal = 1;
}

//cal case approval selection box
if($ruleInfo['ApprovalMode'] == 'T'){
	$isCheckApprovalMode_T = true;
	$isCheckApprovalMode_S = false;
}else if($ruleInfo['ApprovalMode'] == 'S'){
	$isCheckApprovalMode_T = false;
	$isCheckApprovalMode_S = true;
}else{
	$isCheckApprovalMode_T = true;
	$isCheckApprovalMode_S = false;
}

if($ruleInfo['CustApprovalMode'] == 'T'){
    $isCheckCustApprovalMode_T = true;
    $isCheckCustApprovalMode_S = false;
}else if($ruleInfo['CustApprovalMode'] == 'S'){
    $isCheckCustApprovalMode_T = false;
    $isCheckCustApprovalMode_S = true;
}else{
    $isCheckCustApprovalMode_T = true;
    $isCheckCustApprovalMode_S = false;
}

$ruleInfo['RuleID']?$ruleID = $ruleInfo['RuleID']: $ruleID = -$sessionToken;
$db = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
$fileArr = $db->getCaseStageAttachment($CaseID=-1, $Stage='', $AssocWithStage=true, $type='0', $ruleID);
$_PAGE['view_data']['fileArr'] = $fileArr;

//Hidden field
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('quotationType', 'quotationType', $quotationType);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('ruleID', 'ruleID', $ruleID);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('sessionToken', 'sessionToken', $sessionToken);

//required symbol
$requiredSymbol = $linterface->RequiredSymbol();

function approvalGroupToStandardSelectArray($groupInfoArray,$allowNoNeed=0){
	global $Lang;
	$returnedInfoGroupAry = array();

	foreach($groupInfoArray as $num => $groupInfo){
		$returnedInfoGroupAry[$num][0] = $groupInfo['RoleID']; //ID
		$returnedInfoGroupAry[$num][1] = $groupInfo[Get_Lang_Selection('RoleNameChi','RoleName')];//Name
	}
	if($allowNoNeed){
		$additionalArray = array(array(0,$Lang['ePCM']['Setting']['Rule']['NoNeed']));
		$returnedInfoGroupAry = array_merge($additionalArray,$returnedInfoGroupAry);
	}
	return $returnedInfoGroupAry;
}
function caseApprovalSelectBox($approvalGroupAry,$principalInfoAry,$ParSelected=''){
	global $_PAGE,$Lang;
	
	$selectionAry = array(
			array('-1',$Lang['ePCM']['Setting']['Rule']['GroupHead']),
// 			array('-2',$Lang['ePCM']['Setting']['Rule']['GroupHeadAndPricipal']),
// 			array('-3',$Lang['ePCM']['Setting']['Rule']['GroupHeadAndVicePrincipal']),
// 			array('-4',$Lang['ePCM']['Setting']['Rule']['GroupHeadAndVicePrincipalAndPrincipal']),
// 			array('-5',$Lang['ePCM']['Setting']['Rule']['VicePrincipalAndPrincipal']),
	);
	$selectionAry = array_merge($selectionAry,$principalInfoAry);
	$selectionAry = array_merge($selectionAry,$approvalGroupAry);
	$selectionAry = array_map("unserialize", array_unique(array_map("serialize", $selectionAry)));
	$selectionAry = array_values($selectionAry);
	
	foreach($selectionAry as $_roleInfo){
		$_id = $_roleInfo[0];
		$_name = $_roleInfo[1];
		$checkboxArr[] = '<input id="role_'.$_id.'" type="checkbox" value="'.$_name.'" v-model="selectedRoles"><label for="role_'.$_id.'">'.$_name.'</label>';
	}
	
	return implode(' ',$checkboxArr);
	//return $_PAGE['libPCM_ui']->GET_SELECTION_BOX($selectionAry, $ParTags='name="approvalRoleID" id="approvalRoleID" multiple v-model="selectedRoles"', "", $ParSelected, $CheckType=false);
}
$approvalGroupOptionArr1 = array(array('-1',$Lang['ePCM']['Setting']['Rule']['GroupHead']));
$approvalGroupOptionArr2 = approvalGroupToStandardSelectArray($forCaseApprovalGroup);
$approvalGroupOptionArr = array_merge($approvalGroupOptionArr1, $approvalGroupOptionArr2);

$json = $_PAGE['Controller']->requestObject('JSON_obj','includes/json.php');
$approvalGroupOptionJson = $json->encode($approvalGroupOptionArr);

//Table field
// $tableContent[] = array(
// 		'content'=> caseApprovalSelectBox(approvalGroupToStandardSelectArray($forCaseApprovalGroup,0),approvalGroupToStandardSelectArray($principalInfoAry,0),$ruleInfo['ApprovalRoleID'])
// 		.$linterface->Get_Form_Warning_Msg('ApprovalRoleIDEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Please choose case approval'], $Class='warnMsgDiv')
// 		.'<br>'
// 		.'<div id="divApprovalMode">'
// 		.$linterface->Get_Radio_Button('ApprovalMode_T', 'ApprovalMode', $Value='T', $isCheckApprovalMode_T, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['ApprovalTogether'], $Onclick="",$isDisabled=0)
// 		.$linterface->Get_Radio_Button('ApprovalMode_S', 'ApprovalMode', $Value='S', $isCheckApprovalMode_S, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['ApprovalSeperately'], $Onclick="",$isDisabled=0)
// 		.$Lang['ePCM']['Setting']['Rule']['ApprovalSeperatelyRemarks']
// 		.'</div>'
		//'content' => $linterface->GET_SELECTION_BOX(approvalGroupToStandardSelectArray($forCaseApprovalGroup,1), $ParTags='name="approvalRoleID"', $ParDefault, $ParSelected = $ruleInfo['ApprovalRoleID'], $CheckType=false),
		//'content2' => $linterface->Get_Checkbox('needGroupHeadApproval', 'needGroupHeadApproval', $Value=1, $isChecked=$ruleInfo['NeedGroupHeadApprove'], $Class='', $Display='需連同上級批核申請', $Onclick='', $Disabled='')
// );


$quoApprovalArr = approvalGroupToStandardSelectArray($forQuotationApprovalGroup);
$quoApprovalArr[] = array('-1',$Lang['ePCM']['Setting']['Rule']['GroupHead']);
$quoApprovalArr[] = array('-999',$Lang['ePCM']['Mgmt']['Case']['Applicant']);

if($sys_custom['ePCM']['VueEnv'] == 'dev'){
	$vue = 'vue.js';
}else{
	$vue = 'vue.min.js';
}
?>
<style>
.quotation, .tender{
	display:none;
}
</style>
<script src="/templates/vuejs/<?echo $vue?>"></script>
<script>
$( document ).ready(function() {
	selectQuotationType('<?=$quotationType?>');
	approvalModeShowHide();
	$( "#approvalRoleID" ).change(function() {
		approvalModeShowHide();
	});
});

var backLocation = 'index.php?p=setting.rule';
function selectQuotationType(Type){
	switch (Type){
		case 'N':
			$('.quotation, .tender').hide();
			$('.noneed').show();
		break;
		case 'Q':
		case 'V':
			$('.tender').hide();
			$('.quotation').show();
		break;
		case 'T':
			$('.quotation').hide();
			$('.tender').show();
		break;
	}
    switch (Type){
        case 'N':
        case 'V':
            $('#template_type_1').show();
            $('#template_type_2').hide();
            $('#template_type_3').hide();
            $('#template_type_4').show();
            break;
        case 'Q':
            $('#template_type_1').show();
            $('#template_type_2').show();
            $('#template_type_3').hide();
            $('#template_type_4').show();
            break;
        case 'T':
            $('#template_type_1').show();
            $('#template_type_2').show();
            $('#template_type_3').show();
            $('#template_type_4').show();
            break;
    }
	changeCaseApprovalName(Type);
    document.getElementById("quotationType").value = Type;
}
function changeCaseApprovalName(Type){
	if(Type=='N'){
		$('#caseApprovalTd').html('<?=$requiredSymbol.$Lang['ePCM']['Mgmt']['Case']['Endorsement'] ?>');
	}else{
		$('#caseApprovalTd').html('<?=$requiredSymbol.$Lang['ePCM']['Setting']['Rule']['CaseApproval'] ?>');
	}
}
function checkForm(){
	$('form#form1').attr('action', 'index.php?p=setting.rule.update');

	$('.warnMsgDiv').hide();
	
	canSubmit = true;
	var isFocused = false;
	
	$('.requiredField').each( function() {
		var _elementId = $(this).attr('id');
		if (isObjectValueEmpty(_elementId)) {
			canSubmit = false;
			
			$('div#' + _elementId + 'EmptyWarnDiv').show();
			
			if (!isFocused) {
				// focus the first element
				$('#' + _elementId).focus();
				isFocused = true;
			}
		}
	});
	
	var ApprovalRoleID=$('select[name=\'approvalRoleID\']').val();
	if(ApprovalRoleID==''){
		canSubmit=false;
		$('#ApprovalRoleIDEmptyWarnDiv').show();
	}
	
	if($('input[name=\'quotationType\']:checked').length<1){
		canSubmit=false;
		$('#quotationTypeEmptyWarnDiv').show();
	}

	var quotationType = $('input[name=quotationType]:checked').val();
	if(quotationType=='T'){
		if(!isSelectionBoxValid('tenderAuditingRoleID')){
			$('div#' + 'tenderAuditingRoleID' + 'EmptyWarnDiv').show();
			canSubmit=false;
		}
		if(!isSelectionBoxValid('tenderApprovalRoleID')){
			$('div#' + 'tenderApprovalRoleID' + 'EmptyWarnDiv').show();
			canSubmit=false;
		}
	}else if(quotationType=='Q'||quotationType=='V'){
		if(!isSelectionBoxValid('quotationApprovalRoleID')){
			$('div#' + 'quotationApprovalRoleID' + 'EmptyWarnDiv').show();
			canSubmit=false;
		}
	}
	if ($("input[name='selectedRoles[]']").length == 0){
		$('div#' + 'selectedRoles' + 'EmptyWarnDiv').show();
		canSubmit=false;
	}
	if(quotationType!='N'){
		if(!isSelectionBoxValid('minQuote')){
			$('div#' + 'minQuote' + 'EmptyWarnDiv').show();
			canSubmit=false;
		}
		
		<?php if ($sys_custom['ePCM']['extraApprovalFlow']):?>
		if ($("input[name='cust_selectedRoles[]']").length == 0){
			$('div#' + 'cust_selectedRoles' + 'EmptyWarnDiv').show();
			canSubmit=false;
		}
		<?php endif;?>
	}
	//Check if floorPrice is positive
	if(!(parseInt($('#floorPrice').val())>=0)){
		$('#priceNegativeWarnDiv').show();
		canSubmit=false;
	}
	var cellingPrice = $('#cellingPrice').val();
	if($.trim(cellingPrice)!=''){
		if(!(parseInt($('#cellingPrice').val())>=0)){
			$('#priceNegativeWarnDiv').show();
			canSubmit=false;
		}
	}

	//check if ceilling price smaller than floor price
	var floorPrice = $('#floorPrice').val();
	if($.trim(cellingPrice)!=''){
		if(parseInt(cellingPrice)<parseInt(floorPrice)){
			$('#floorPriceLargerWarnDiv').show();
			canSubmit=false;
		}
	}

	//alert(canSubmit);
	return canSubmit;
}
function isSelectionBoxValid(id){
	var value = $('#'+id+' option:selected').val();
	if(value==''){
		return false;
	}else{
		return true;
	}
}
function approvalModeShowHide(){
	if($( "#approvalRoleID").val() != '' && $( "#approvalRoleID").val() > -2 ){
		$('#divApprovalMode').hide();
		$('#ApprovalMode_T').attr('checked',true);
	}
	else if( $("#approvalRoleID").val() == ''){
		$('#divApprovalMode').hide();
		$('#ApprovalMode_T').attr('checked',true);
	}
	else{
		$('#divApprovalMode').show();
	}
}
</script>
<style>
  .div-table {
      display:table;
  }
  .div-tr {
    display: table-row;
  }
  .div-td {
    display: table-cell;
  }
</style>
<table class="form_table_v30">
	<tr>
		<td class="field_title">
			<?php echo $requiredSymbol.$Lang['General']['Code']?>
		</td>
		<td>
			<?php echo $linterface->GET_TEXTBOX('code', 'code', $ruleInfo['Code'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>5,'style'=>'width:100px')).$linterface->Get_Form_Warning_Msg('codeEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Please input code'], $Class='warnMsgDiv')?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?php echo $requiredSymbol.$Lang['ePCM']['Setting']['Rule']['PriceRange']?>
		</td>
		<td>
			<?php echo $Lang['ePCM']['Setting']['Rule']['From'].' '.
			$linterface->GET_TEXTBOX_NUMBER('floorPrice', 'floorPrice', $ruleInfo['FloorPrice'], $OtherClass='requiredField', $OtherPar=array('maxlength'=>255,'style'=>'width:200px')).' '.
			$Lang['ePCM']['Setting']['Rule']['To'].' '.$linterface->GET_TEXTBOX_NUMBER('cellingPrice', 'cellingPrice', $ruleInfo['CeillingPrice'], $OtherClass='', $OtherPar=array('maxlength'=>255,'placeholder' => $Lang['ePCM']['Setting']['Rule']['BlankAsUnlimited'],'style'=>'width:200px'))
			.'<br>'
			.'<span class="tabletextremark">'.$Lang['ePCM']['Setting']['Rule']['Remarks']['PriceRange'].'</span>'
		    .$linterface->Get_Form_Warning_Msg('floorPriceEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Please input price range (from)'], $Class='warnMsgDiv').' '
			.$linterface->Get_Form_Warning_Msg('priceNegativeWarnDiv', $Lang['ePCM']['Setting']['Rule']['PriceRangeNegative'], $Class='warnMsgDiv')
			.$linterface->Get_Form_Warning_Msg('floorPriceLargerWarnDiv', $Lang['ePCM']['Setting']['Rule']['floorPriceLarger'], $Class='warnMsgDiv')
			?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?php echo $requiredSymbol.$Lang['ePCM']['Setting']['Rule']['QuotationType']?>
		</td>
		<td>
			<?php echo $linterface->Get_Radio_Button('quotationType_no', 'quotationType', $Value='N', $isCheckQuotationNo, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['NoNeed'], $Onclick="selectQuotationType(this.value);",$isDisabled=0). 
			$linterface->Get_Radio_Button('quotationType_verbal', 'quotationType', $Value='V', $isCheckQuotationVerbal, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['Verbal'], $Onclick="selectQuotationType(this.value);",$isDisabled=0).
			$linterface->Get_Radio_Button('quotationType_quotation', 'quotationType', $Value='Q', $isCheckQuotationQuote, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['Quotation'], $Onclick="selectQuotationType(this.value);",$isDisabled=0).
			$linterface->Get_Radio_Button('quotationType_tender', 'quotationType', $Value='T', $isCheckQuotationTender, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['Tender'], $Onclick="selectQuotationType(this.value);",$isDisabled=0)
			.$linterface->Get_Form_Warning_Msg('quotationTypeEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Please select quotation type'], $Class='warnMsgDiv') 
			?>
		</td>
	</tr>
	<tr class="quotation tender noneed">
		<td class="field_title" id="caseApprovalTd">
			<?php echo $requiredSymbol.($isCheckQuotationNo)?$Lang['ePCM']['Mgmt']['Case']['Endorsement']:$Lang['ePCM']['Setting']['Rule']['CaseApproval']?>
		</td>
		<td>
			<div class="div-table" width="100%">
				<div class="div-tr" id="vue-approval">
					<div class="div-td" id="role-select">
						<div class="div-tr" v-for="role in roles">
							<input type="checkbox" :id="'role_'+role[0]" :value="role[0]" v-model="selectedRoles"><label :for="'role_'+role[0]">{{role[1]}}</label>
						</div>
					</div>
					<div class="div-td" id="role-sequence">
						<div class="div-table" width="100%">
							<div class="div-tr" v-for="(selectedRole, key) in selectedRoles">
								<div class="div-td" width="5%">{{key+1}}.</div>
								<div class="div-td" width="45%">{{selectedRole|getRoleName(rolesAssoc)}}</div>
								<div class="div-td">
									<?php echo $Lang['ePCM']['Setting']['Rule']['Required']?>
									<select :id="'mode_'+key" :name="'approveNum['+selectedRole+']'">
										<option value="0"><?php echo $Lang['ePCM']['Setting']['Rule']['AllPeople']?></option>
										<option value="1"><?php echo str_replace('<!--n-->','1',$Lang['ePCM']['Setting']['Rule']['AtLeast'])?></option>
										<option value="2"><?php echo str_replace('<!--n-->','2',$Lang['ePCM']['Setting']['Rule']['AtLeast'])?></option>
										<option value="3"><?php echo str_replace('<!--n-->','3',$Lang['ePCM']['Setting']['Rule']['AtLeast'])?></option>
									</select>
									<?php echo $Lang['ePCM']['Setting']['Rule']['Approve']?>
								</div>
								<input type="hidden" name="selectedRoles[]" :value="selectedRole">
							</div>
						</div>
					</div>
					<div class="div-td" id="reset-select" style="vertical-align:bottom;">
						<button v-if="selectedRoles.length > 0" type="button" id="resetBtn" @click="uncheckAll"><?php echo $Lang['ePCM']['Setting']['Rule']['Reset']?></button>
					</div>
				</div>
				<div class="div-tr" id="divApprovalMode" style="display:table-row;">
				 		<?php echo $linterface->Get_Radio_Button('ApprovalMode_T', 'ApprovalMode', $Value='T', $isCheckApprovalMode_T, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['ApprovalTogether'], $Onclick="",$isDisabled=0)?>
				 		<?php echo $linterface->Get_Radio_Button('ApprovalMode_S', 'ApprovalMode', $Value='S', $isCheckApprovalMode_S, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['ApprovalSeperately'], $Onclick="",$isDisabled=0)?>
				 		<?php echo $linterface->Get_Form_Warning_Msg('selectedRolesEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['PleaseSelectRole'], $Class='warnMsgDiv')?>
		 		</div>
	 		</div>
		</td>
	</tr>
	<?php if ($sys_custom['ePCM']['extraApprovalFlow']):?>
	<tr class="quotation tender">
		<td class="field_title" id="caseApprovalTd">
			<?php echo $requiredSymbol.$Lang['ePCM']['Mgmt']['Case']['Endorsement']?>
		</td>
		<td>
			<div class="div-table" width="100%">
				<div class="div-tr" id="vue-approval-cust">
					<div class="div-td" id="cust_role-select">
						<div class="div-tr" v-for="role in roles">
							<input type="checkbox" :id="'cust_role_'+role[0]" :value="role[0]" v-model="selectedRoles"><label :for="'cust_role_'+role[0]">{{role[1]}}</label>
						</div>
					</div>
					<div class="div-td" id="cust_role-sequence">
						<div class="div-table" width="100%">
							<div class="div-tr" v-for="(selectedRole, key) in selectedRoles">
								<div class="div-td" width="5%">{{key+1}}.</div>
								<div class="div-td" width="45%">{{selectedRole|getRoleName(rolesAssoc)}}</div>
								<div class="div-td">
									<?php echo $Lang['ePCM']['Setting']['Rule']['Required']?>
									<select :id="'cust_mode_'+key" :name="'cust_approveNum['+selectedRole+']'">
										<option value="0"><?php echo $Lang['ePCM']['Setting']['Rule']['AllPeople']?></option>
										<option value="1"><?php echo str_replace('<!--n-->','1',$Lang['ePCM']['Setting']['Rule']['AtLeast'])?></option>
										<option value="2"><?php echo str_replace('<!--n-->','2',$Lang['ePCM']['Setting']['Rule']['AtLeast'])?></option>
										<option value="3"><?php echo str_replace('<!--n-->','3',$Lang['ePCM']['Setting']['Rule']['AtLeast'])?></option>
									</select>
									<?php echo $Lang['ePCM']['Setting']['Rule']['Approve']?>
								</div>
								<input type="hidden" name="cust_selectedRoles[]" :value="selectedRole">
							</div>
						</div>
					</div>
					<div class="div-td" id="reset-select" style="vertical-align:bottom;">
						<button v-if="selectedRoles.length > 0" type="button" id="resetBtn" @click="uncheckAll"><?php echo $Lang['ePCM']['Setting']['Rule']['Reset']?></button>
					</div>
				</div>
				<div class="div-tr" id="divApprovalMode" style="display:table-row;">
				 		<?php echo $linterface->Get_Radio_Button('cust_ApprovalMode_T', 'cust_ApprovalMode', $Value='T', $isCheckCustApprovalMode_T, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['ApprovalTogether'], $Onclick="",$isDisabled=0)?>
				 		<?php echo $linterface->Get_Radio_Button('cust_ApprovalMode_S', 'cust_ApprovalMode', $Value='S', $isCheckCustApprovalMode_S, $Class="", $Display=$Lang['ePCM']['Setting']['Rule']['ApprovalSeperately'], $Onclick="",$isDisabled=0)?>
				 		<?php echo $linterface->Get_Form_Warning_Msg('cust_selectedRolesEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['PleaseSelectRole'], $Class='warnMsgDiv')?>
		 		</div>
	 		</div>
		</td>
	</tr>
	<?php endif;?>
	<tr class="quotation tender">
		<td class="field_title"> 
			<?php echo $requiredSymbol.$Lang['ePCM']['Setting']['Rule']['MinInvitation'] ?>
		</td>
		<td>
			<?php echo $linterface->Get_Number_Selection('minQuote', $MinValue=1, $MaxValue=9, $SelectedValue=$ruleInfo['MinQuotation'], $Onchange='', $noFirst=0, $isAll=0, $FirstTitle='', $Disabled=0)
		.$linterface->Get_Form_Warning_Msg('minQuoteEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Warning']['Select']['MinQuote'], $Class='warnMsgDiv')?>
		</td>
	</tr>
	<tr class="quotation">
		<td class="field_title"><?php echo $requiredSymbol.$Lang['ePCM']['Setting']['Rule']['QuotationApproval']?></td>
		<td>
			<?php echo $linterface->GET_SELECTION_BOX( $quoApprovalArr , $ParTags='name="quotationApprovalRoleID" id="quotationApprovalRoleID"', $ParDefault=$Lang['ePCM']['General']['Select'], $ParSelected=$ruleInfo['QuotationApprovalRoleID'], $CheckType=false)
		.$linterface->Get_Form_Warning_Msg('quotationApprovalRoleIDEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Warning']['Select']['QuotationApproval'], $Class='warnMsgDiv')?>
		</td>
	</tr>
	<tr class="tender">
		<td class="field_title"><?php echo $requiredSymbol.$Lang['ePCM']['Setting']['Rule']['TenderAuditing']?></td>
		<td>
			<?php echo $linterface->GET_SELECTION_BOX(approvalGroupToStandardSelectArray($forTenderAuditingGroup), $ParTags='name="tenderAuditingRoleID" id="tenderAuditingRoleID"', $ParDefault=$Lang['ePCM']['General']['Select'], $ParSelected=$ruleInfo['TenderAuditingRoleID'], $CheckType=false)  
		.$linterface->Get_Form_Warning_Msg('tenderAuditingRoleIDEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Warning']['Select']['TenderAuditing'], $Class='warnMsgDiv')?>
		</td>
	</tr>
	<tr class="tender">
		<td class="field_title"><?php echo $requiredSymbol.$Lang['ePCM']['Setting']['Rule']['TenderApproval']?></td>
		<td>
			<?php echo $linterface->GET_SELECTION_BOX(approvalGroupToStandardSelectArray($forTenderApprovalGroup), $ParTags='name="tenderApprovalRoleID" id="tenderApprovalRoleID"', $ParDefault=$Lang['ePCM']['General']['Select'], $ParSelected=$ruleInfo['TenderApprovalRoleID'], $CheckType=false)
		.$linterface->Get_Form_Warning_Msg('tenderApprovalRoleIDEmptyWarnDiv', $Lang['ePCM']['Setting']['Rule']['Warning']['Select']['TenderApproval'], $Class='warnMsgDiv')?>
		</td>
	</tr>
    <tr>
        <td class="field_title"><?php echo $Lang['ePCM']['Setting']['Rule']['uploadSampleFile']?></td>
        <td id="ruleTemplate">
                <table id="ruleTemplateTable" class="common_table_list_v30">
                    <thead>
                    <tr class="tabletop">
                        <th width="20%"><?php echo $Lang['ePCM']['Template'] ?></th>
                        <th><?php echo $Lang['General']['File'] ?></th>
                    <tr>
                    </thead>
                    <tbody>

                    <?php
                    foreach((array)$ePCMcfg['Template'] as $template_key){
                        echo '<tr id="template_type_'.$template_key.'">';
                        echo '<td><a href="javascript:goUpload('.$template_key.')" class="icon_edit_dim">'.$Lang['ePCM']['Setting']['Template'][$template_key].'</a><div class="table_row_tool row_content_tool"><a href="javascript:goUpload('.$template_key.')" class="icon_edit_dim"></a></div></td>';
                        echo '<td>'.$linterface->getFileListUI($_PAGE['view_data']['fileArr'][$template_key], $displaySize=false, $returnArr=false).'</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
            <?= $Lang['ePCM']['Setting']['Rule']['Remarks']['rollbackTemplateFile']; ?>
        </td>
    </tr>
</table>

<?= $hiddenF ?>
<div id="thickboxContainerDiv" class="edit_pop_board">
<script>
var lastApprovalRuleJson = '<?php echo $ruleInfo['CaseApprovalRule']?>';
var lastApprovalRuleSortJson = '<?php echo $approvalRoleSortJson?>';
var lastApprovalRule = {};
var lastApprovalSort = [];

var custLastApprovalRuleJson = '<?php echo $ruleInfo['CustCaseApprovalRule']?>';
var custLastApprovalRuleSortJson = '<?php echo $custApprovalRoleSortJson?>'; 
var custLastApprovalRule = {};
var custLastApprovalSort = [];

var rolesJson = '<?php echo $approvalGroupOptionJson?>';

function ajax_recover_tempRemove(){
    $.post(
        'index.php?p=file.recover',
        {
            session: "<?= $sessionToken ?>",
            ruleID:  "<?= $ruleID ?>"
        },
        function(response){
        }
    );
}

$(document).ready( function() {

	if(lastApprovalRuleJson != ''){
		lastApprovalRule = JSON.parse(lastApprovalRuleJson);
		lastApprovalSort = JSON.parse(lastApprovalRuleSortJson);
	}
	if(custLastApprovalRuleJson != ''){	
		custLastApprovalRule = JSON.parse(custLastApprovalRuleJson);
		custLastApprovalSort = JSON.parse(custLastApprovalRuleSortJson);
	}
	new Vue({
	  el: '#vue-approval',
	  data: {
		  roles:  JSON.parse(rolesJson),
		  rolesAssoc : [],
		  selectedRoles : [],
		  selectedRoles_string : ''
	  },
	  methods: {
		uncheckAll: function(){
			this.selectedRoles = [];
		}
	  },
	  filters: {
		  getRoleName :function(value, map){
			  return map[value];
		  }
	  },
	  mounted: function(){
		  this.rolesAssoc = this.roles.reduce(function(map,obj){
			  map[obj[0]] = obj[1];
			  return map;
		  },{});
		  if(lastApprovalRuleJson != ''){
			  $.each(lastApprovalSort, function(index, value) {
				  $('#role_' + value).click();
			  });
			  Vue.nextTick(function () {
				  key = 0;
				  $.each(lastApprovalSort, function(index, value) {
					  selectedValue = lastApprovalRule[value];
					  $('#mode_' + key).val(selectedValue);
					  key++;
				  });
			  }) 
		  }
	  }
	})
	new Vue({
		  el: '#vue-approval-cust',
		  data: {
			  roles:  JSON.parse(rolesJson),
			  rolesAssoc : [],
			  selectedRoles : [],
			  selectedRoles_string : ''
		  },
		  methods: {
			uncheckAll: function(){
				this.selectedRoles = [];
			}
		  },
		  filters: {
			  getRoleName :function(value, map){
				  return map[value];
			  }
		  },
		  mounted: function(){
			  this.rolesAssoc = this.roles.reduce(function(map,obj){
				  map[obj[0]] = obj[1];
				  return map;
			  },{});
			  if(lastApprovalRuleJson != ''){
				  $.each(custLastApprovalSort, function(index, value) {
					  $('#cust_role_' + value).click();
				  });
				  Vue.nextTick(function () {
					  key = 0;
					  $.each(custLastApprovalSort, function(index, value) {
						  selectedValue = custLastApprovalRule[value];
						  $('#cust_mode_' + key).val(selectedValue);
						  key++;
					  });
				  }) 
			  }
		  }
		})
});


function goUpload(type){

load_dyn_size_thickbox_ip('<?=$Lang['ePCM']['SettingsArr']['SampleDocumentArr']['MenuTitle']?>', 'onloadTBeditUploadFile('+type+');', inlineID='', defaultHeight=500, defaultWidth=1000);
}
function onloadTBeditUploadFile(type) {
    $('div#TB_ajaxContent').load(
        "?p=setting.template.upload."+type,
        {
            ruleID: '<?= $ruleID ?>',
            sessionToken: '<?= $sessionToken ?>',
            quotationType: '' + document.getElementById("quotationType").value
        },
        function(ReturnData) {
            adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
        }
    );
}
</script>