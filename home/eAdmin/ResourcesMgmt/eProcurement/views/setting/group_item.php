<?php 
$linterface = $_PAGE['libPCM_ui'];
$groupID = $_PAGE['groupID'];

# POST
$postVar = $_PAGE['views_data'];
$order = $postVar['order'];
$field = $postVar['field'];
$pageNo = $postVar['pageNo'];
$page_size = $postVar['numPerPage'];
$sql = $postVar['sql'];
$academicYearID = ''; // get current setting academicYearID

$PAGE_NAVIGATION[] = array($Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'],'javascript: window.location=\'index.php?p=setting.group\';');
$PAGE_NAVIGATION[] = array($Lang['ePCM']['Group'][Get_Lang_Selection('GroupItemNameChi','GroupItemName')]);
$htmlAry['navigation'] = $linterface->getNavigation_PCM($PAGE_NAVIGATION);

# initiation
$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
$pageNo = ($postVar['page_size_change'] == '') ? 1 : $postVar['page_size_change'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];

# getDb Table
$columns = array();
$columns[] = array('Code',$Lang['General']['Code'],'50%');
$columns[] = array('ItemName',$Lang['ePCM']['Group'][Get_Lang_Selection('GroupItemNameChi','GroupItemName')],'50%');
$columnCustArr = array(22,22);
$htmlAry['dbTable'] = $linterface->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, true, $columnCustArr);

# DB table action buttons
$dbTableBtnAry = array();
$dbTableBtnAry[] = array('edit', 'javascript: submitToEdit();');
$dbTableBtnAry[] = array('delete', 'javascript: submitToDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);

# get Html element
$btnAry[] = array('new', 'javascript: goNew('.$groupID.');');
// $btnAry[] = array('back', 'javascript: goBack('.$groupID.');', 'This is Back button');
$htmlAry['contentTool'] = $_PAGE['libPCM_ui']->Get_Content_Tool_By_Array_v30($btnAry);

# hidden field
$hiddenField = '';
$hiddenField = $linterface->GET_HIDDEN_INPUT('groupID', 'groupID', $groupID);
?>
<script>
$(document).ready( function() {

});

function goNew(groupID){
	location.href = "index.php?p=setting.group.item.new." + groupID;
}

function goBack(){
	location.href = "index.php?p=setting.group";
}

function submitToEdit(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseAGroupItem'] ?>");
		return;
	}else if(checked.length!=1){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroupItem'] ?>");
		return;
	}else{
		$('form#form1').attr('action', 'index.php?p=setting.group.item.edit').submit();
	}
		
}

function submitToDelete(){

	var checked = targetIDAry();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupItem'] ?>");
		return;
	}else{
		if(!confirm("<?=$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] ?>")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=setting.group.item.delete').submit();
	}
	
		
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
</script>
<?=$htmlAry['navigation']?>
<br>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?php echo $Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle']; ?> </td>
		<td>
			<?php echo $postVar[Get_Lang_Selection('group_name_chi','group_name')]; ?>
		</td>
	</tr>
</table>
<br/>
<form id="form1" name="form1" method="POST" action="">
<div class="content_top_tool">
	<?=$htmlAry['contentTool']?>
	<br style="clear:both;">
</div>
<?=$htmlAry['dbTableActionBtn']?>
<?=$htmlAry['dbTable']?>
<?=$hiddenField?>
</form>