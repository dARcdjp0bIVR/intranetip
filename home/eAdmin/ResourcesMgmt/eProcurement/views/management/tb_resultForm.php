<?php
/*
 * Date: 2016-09-19 Omas
 * Added addNewSupplierTypeDiv
 */


$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$requiredSymbol = $linterface->RequiredSymbol();
//Get exist result if any
$result = $postVar['result'];
$caseInfoAry = $postVar['caseInfoAry'];
$supplierTypeInfoAry = $postVar['supplierTypeAry'];

if(!(empty($result)||$result=='')){
	$hiddenF .= $linterface->GET_HIDDEN_INPUT('resultID', 'resultID', $result['ResultID']);
	$htmlAry['hiddenField'] = $hiddenF;
	$editMode = true;
}

$ruleID = $caseInfoAry['RuleID'];
$caseID = $caseInfoAry['CaseID'];

// for plupload
$sessionToken = time();
$pluploadButtonId = 'UploadButton';
$pluploadFileListDivId = 'FileListDiv';
$pluploadContainerId = 'pluploadDiv';
$deleteFileDivId = 'willDeleteFileDiv';
$pluploadBtn = $linterface->GET_SMALL_BTN($Lang['General']['PleaseSelectFiles'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
$templateBox = $linterface->getTemplateFileUI($ePCMcfg['Template']['Result'], $ruleID, $getAttachWithoutRuleIDAndRuleID=true, $caseID);
$fileWarningDiv = $linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv tabletextrequire');

$caseID = $postVar['caseID'];
$supplierListAry = $postVar['supplierListAry'];
//$fundingList = $postVar['fundingListOpt'];
//$funding_selection = getSelectByAssoArray($fundingList, "name= 'fundingId1' id= 'funding_selection' onchange='disableUnitPrice();'", $result['FundingSource1'], $all=0, $noFirst=1);
//$funding_selection_2 = getSelectByAssoArray($fundingList,"name= 'fundingId2' id= 'funding_selection_2' onchange='disableUnitPrice2();'", $result['FundingSource2'], $all=0, $noFirst=1);
if($_PAGE['libPCM']->enableFundingSource()){
    $FundingSourceArr = $_PAGE['views_data']['FundingSourceArr'];
    foreach ((array)$FundingSourceArr as $_FundingSourceArr){ //loop every funding
        foreach ((array)$_FundingSourceArr['Case'] as $_Case){ //loop every case
            if($_Case['CaseID']==$caseInfoAry['CaseID']){ //print only this case
                $FundingID['FundingID'][] = $_FundingSourceArr['FundingID'];
                $FundingID['Budget'][] = $_Case['Amount'];
            }
        }
    }
    if(sizeof($FundingID)){
        foreach ($FundingID['FundingID']as $FundingKey => $_FundingID){
            $BudgetUsed = $FundingID['Budget'][$FundingKey];
            $htmlAry['FundingSource_SelectionBox'] .= '<tr><td>'.$linterface->getFundingByCategorySelectionBox('Funding_Selection_Box_'.$FundingKey,'Funding_Selection_Box',$_FundingID,$BudgetUsed).'</td></tr>';
        }
    }else{
        $htmlAry['FundingSource_SelectionBox'] = '<tr><td>'.$linterface->getFundingByCategorySelectionBox().'</td></tr>';
    }
    $htmlAry['FundingSource_AddBtn'] = "<div class='table_row_tool row_content_tool' style='float:left;'><a href='javascript:void(0)' class='add_dim' onclick='AddFundingSourceNewRow();'></a></div>";
}

if($caseInfoAry['QuotationType']=='N'){
    $supplierTypeSel = '<div id="typeDiv">'.getSelectByAssoArray($supplierTypeInfoAry, $tags='name="typeID" id="typeID" onchange="onChangeRersultSupplierType(this.value);"', $selected="", $all=1, $noFirst=0, $FirstTitle="", $ParQuoteValue=1).'</div>';
}

//$supplierSel = getSelectByArray($supplierListAry, $tags='name="supplierID" id="supplierID"', $result['SupplierID'], $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
$supplierRadios='';
$supplierRadios.='<div id="supplierDiv">';
$counter = 1;
$numToChangeRow = 1;


foreach($supplierListAry as $_supplier){
    $isChecked = 0;
    if($result['SupplierID']==$_supplier[0]){
        $isChecked = 1;
    }
    if($_supplier[2]!=''){
        $displayPrice = ' ($'.$_supplier[2].')';
    }
    else{
        $displayPrice = '';
    }
    $Class = ($_supplier[3])?'notMin' : 'min';

    $supplierRadios .= $linterface->Get_Radio_Button('supplierRadio_'.$_supplier[0], 'supplierRadio', $_supplier[0], $isChecked, $Class, $_supplier[1].$displayPrice, $Onclick="onClickSuppiler(this.value);",$isDisabled=0);
    if($counter%$numToChangeRow==0){
        $supplierRadios .= '<br>';
    }
    $counter++;
}
$supplierRadios .= '</div>';
if($caseInfoAry['QuotationType']=='N'){
	$supplierRadios .= '<a href="javascript:showAddNewSupplier();"> + '.$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier'].'</a><br/>';
}


$dealPriceInput = $linterface->GET_TEXTBOX_NUMBER('dealPrice', 'dealPrice', $result['DealPrice'],'requiredField');
$reasonTA = $linterface->GET_TB_TEXTAREA('reason', $result['Reason'], $taCols=70, $taRows=5);
$reasonWarningDiv = $linterface->Get_Thickbox_Warning_Msg_Div('reasonWarningDiv', '* '.$Lang['ePCM']['Mgmt']['Case']['Result']['ReasonWarning'],'reasonWarningDiv tabletextrequire');
$descTA = $linterface->GET_TB_TEXTAREA('description', $result['Description'], $taCols=70, $taRows=5);
$dealDateDatePicker = $linterface->GET_DATE_PICKER('datePickerDealDate', $result['DealDate']);

$htmlAry['hiddenField'] .= $linterface->GET_HIDDEN_INPUT('session', 'session', $sessionToken);
$htmlAry['hiddenField'] .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseID);

$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="checkForm();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "ajax_recover_tempRemove();js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$quotationType = $postVar['quotationType'];

?>
<?=$linterface->Include_Plupload_JS_CSS()?>
<script>
function checkForm(){
	var warnDv = $('#FileWarnDiv');
	$('#FileWarnDiv').hide();
	$('#reasonWarningDiv').hide();
	//if(parseFloat( $('#item_unit_price1').val() ) + parseFloat( $('#item_unit_price2').val() ) > parseFloat( $('#dealPrice').val() ) ){
	//	alert('<?//=$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_Total']?>//');
	//	return false;
	//}

    //ajax checking Funding Source > dealPrice
    <?php  if($_PAGE['libPCM']->enableFundingSource()){?>
    var FundingSum=0;
    $("input[name=Funding_Selection_Box_usingBudget]").each(function(){
       FundingSum = FundingSum + parseFloat($(this).val());
    });
    if(FundingSum > parseFloat( $('#dealPrice').val())){
        alert('<?=$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_Total']?>');
        return false;
    }
    <?php }?>
    
	////	check funding price value
	//if( $('#funding_selection').val() !=0 && $('#item_unit_price1').val() ==0 ){
	//	alert('<?//=$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_1']?>//');
	//	return false;
	//}
    //
	////	check funding2 price value
	//if( $('#funding_selection_2').val() !=0 && $('#item_unit_price2').val() ==0 ){
	//	alert('<?//=$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_2']?>//');
	//	return false;
	//}
    //
	//// check if the funding source 1 and funding source 2 are the same
	//if( $('#funding_selection').val() == $('#funding_selection_2').val() && $('#funding_selection').val() !=0 ){
	//	alert('<?//=$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_SAME']?>//');
	//	return false;
	//}

	// check file upload
	if(!jsIsFileUploadCompleted()) {
		warnDv.html('<?=$Lang['ePCM']['General']['jsWaitAllFilesUploaded']?>').show();
		return false;
	}
	
	//if("<? echo $quotationType?>" == 'Q'){
		
		if($('input[name=' + 'supplierRadio' + ']').length >1){
			if( !$("#reason").attr("disabled") && $("#reason").val() == ''){
				$('#reasonWarningDiv').show();
				return false;
			}
		}
	//}
	
	
	submitResult();
	return true;
	//$('#id').attr('disabled', true)
	//$('#id').removeAttr('disabled')
}

// file_uploader must place before $(document).ready inside thickbox
var file_uploader = {};
var FileCount = 0;
$(document).ready( function() {
	<?php if($editMode){ ?>
		ajax_reload_saved_attachment();
    <?php } ?>
    <?php if($caseInfoAry['QuotationType']=='N'){ ?>
        onChangeRersultSupplierType();
    <?php } ?>
    ajax_financial_item(<?=$caseInfoAry['ApplicantGroupID']?>);
	initUploadAttachment('pluploadDiv','UploadButton');
	// disableUnitPrice();
	// disableUnitPrice2();

	// init reason
	if($( ".notMin:selected" ).length > 0 ){
		$("#notMinRow").show();
		$("#reason").removeAttr("disabled");
	}else{
		$("#notMinRow").hide();
		$("#reason").attr("disabled", true);
	}
	
	// not Min price
	$( ".notMin" ).click(function() {
		$("#notMinRow").show();
		$("#reason").removeAttr("disabled");
	});

	$( ".min" ).click(function() {
		$("#notMinRow").hide();
		$("#reason").attr("disabled", true);
	});

    var delayTimer;
    $('#dealPrice').bind('input', function(){
        clearTimeout(delayTimer);
        delayTimer = setTimeout(function() {
            // Will do after 0.5 s
            checkBudgetExceed();
            if($('#Funding_Selection_Box_0').val() > 0 && $('#dealPrice').val() ){
                $('#Funding_Selection_Box_0_usingBudget').val($('#dealPrice').val());
            }
        }, 500);
    });


});

function onChangeRersultSupplierType(typeID=''){
    $('#supplierDiv').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
    $('#supplierDiv').load(
        "index.php?p=ajax.supplierRadioList",
        {
            typeID: typeID,
            selectedSupplierID:"<?=$result['SupplierID']?>"
        },
        function(ReturnData) {
            adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
        }
    );
}


function initUploadAttachment(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length == 0) return;

	file_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'index.php?p=file.process&session=<?=$sessionToken?>',
			max_file_size : '200mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>
	    });
	    
	file_uploader.init();

	file_uploader.bind('BeforeUpload', function(up, file){
		FileCount += 1; // global var
	});
	
	file_uploader.bind('FilesAdded', function(up, files) {
		
        $.each(files, function(i, file) {
            $('#'+containerId).append(
                '<div id="' + file.id + '">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
            '</div>');
        });
 		
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });
    
    file_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
	});
	
    file_uploader.bind('UploadComplete', function(up, files) {
		var postData = $('form#ajax_form').serialize();
		var fileCount = files.length;
		files.reverse();
		ajax_update_attachment(fileCount, postData, files);
	});
    container.attr('style','');
}

function ajax_update_attachment(numberLeft, postData, files)
{
	var index = numberLeft - 1;
	if(index >= 0) {
		var finalPostData = postData;
		<?php if($is_mobile_tablet_platform){ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['target_name']);
		<?php }else{ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['name']);
		<?php } ?>
		$.post(
			'index.php?p=file.attach.update',
			finalPostData,
			function(data){
				eval(data);
				if(file_uploader.removeFile) {
					$('#' + files[index].id).remove();
					file_uploader.removeFile(files[index]);
				}
				FileCount -= 1; // global var
				numberLeft -= 1;
				if(numberLeft > 0) {
					ajax_update_attachment(numberLeft, postData, files);
				}
			}
		);
	}
}

// hide original plupload file list, actual file list is return from  ajax_update_attachment
function bindClickEventToElements()
{
	$('a').bind('click',afterClickedLinksButtons);
}
var isCloseWindow = true;
function afterClickedLinksButtons()
{
	isCloseWindow = false;
	setTimeout(function(){isCloseWindow=true;},1);
}

function Check_Total_Filesize(CheckNewOnly){
	return true;
}

function ajax_delete_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.attach.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function download_attachment(target){
	window.location = "index.php?p=file.download."+target;
}

function ajax_reload_saved_attachment(){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.caseList",
        data: {
			"session" : "<?=$sessionToken?>",
			"caseID"  : "<?=$caseID?>",
			"stageID" : "5",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_delete_saved_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_recover_tempRemove(){
	postdata = $('form#ajax_form').serialize()
	$.post(
			'index.php?p=file.recover',
			postdata,
			function(response){
			}
	);
}

function jsIsFileUploadCompleted(){
	var totalSelected = file_uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<file_uploader.files.length;i++) {
		if(file_uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	return totalDone == totalSelected;
}
function onClickSuppiler(supplierID){
	var dealPrice = $('#dealPrice').val();
	
	$('#ajax_price').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	
	$.ajax({
	       type:"POST",
	       url: "index.php?p=ajax.priceSummary.get",
	      data: {
			"supplierID"		:	supplierID,
				"caseID"  : <?=$caseID?>
			},
	      success:function(data)
	        {
	       	$('#dealPrice').val(data);
            checkBudgetExceed();
            if($('#Funding_Selection_Box_0').val() > 0 && $('#dealPrice').val() > 0 ){
                $('#Funding_Selection_Box_0_usingBudget').val($('#dealPrice').val());
            }
	       	$('#ajax_price').html('');

	       }
	     })
	
}
function afterAddSupplierJs(){
// 	$('#TB_closeWindowButton').click();
	setResult();
}

function AddFundingSourceNewRow(){
    var TableBody = document.getElementById("fundingSourceTable").tBodies[0];
    var RowIndex = document.getElementById("fundingSourceNewBtn").rowIndex ;
// 	var id = "Funding_Selection_Box_"+RowIndex;
    var row = TableBody.insertRow(RowIndex);
    var cell1 = row.insertCell(0);
    cell1.innerHTML = '<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">';
    $.ajax({
        type: "POST",
        url: "index.php?p=ajax.fundingSource.getSelectionBox",
        data: {
            "id": "Funding_Selection_Box_"+RowIndex
        },
        success: function(ReturnData){
            cell1.innerHTML = ReturnData
        }
    });
}

function ajax_financial_item(value){
    //reset check budget
    checkBudgetExceed();

    $('#ajax_financial_item').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
    $.ajax({
        type:"POST",
        url:"index.php?p=ajax.financialItem",
        data:{
            groupID: value,
            itemID: '<?=$caseInfoAry['ItemID'] ?>'
        },
        success: function(response){
            $('#ajax_financial_item').html(response);
            bindFundingMappingHandler();
        }
    });

    //hide budget statment
    $('#ajax_budget').html('');
    $('#ajax_budget_exceed').html('');

}
function bindFundingMappingHandler(){
    $('.financialItemRadio').click(function(){
        var fundingId = $(this).attr("data-fund");

        if(fundingId > 0){
            $('select#Funding_Selection_Box_0').val(fundingId);
        }else{
            $('select#Funding_Selection_Box_0').val(0);
        }
    });
}

function checkBudgetExceed(){
    var budget = $('#budget').val();
    var groupID = $('#groupID option:selected').val();
    var itemID = $('input[name="itemID"]:checked').val();
    if(!(budget>0&&groupID>0&&itemID>0)){
        //skip
        return;
    }else{
        $('#ajax_budget_exceed').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
        $.ajax({
            type:"POST",
            url: "index.php?p=ajax.checkExceedBudget",
            data: {
                "budget" : budget,
                "groupID"  : groupID,
                "itemID" : itemID,
            },
            success:function(data)
            {
                switch(data){
                    case 'OK':
                        $('#ajax_budget_exceed').html('');
                        over_budget = false;
                        break;
                    case 'GROUP':
                        $('#ajax_budget_exceed').html('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Group'] ?>');
                        over_budget = true;
                        break;
                    case 'ITEM':
                        $('#ajax_budget_exceed').html('<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Item'] ?>');
                        over_budget = true;
                        break;
                    case 'GROUP:ITEM':
                        $('#ajax_budget_exceed').html('<?= $Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem']?>');
                        over_budget = true;
                        break;
                }
            }
        });
    }
}

// function disableUnitPrice(){
//
//
// 	if(	$('#funding_selection').val()== 0 ){
//
// 		$('#item_unit_price1').attr('disabled', true);
//
// 	}
// 	else{
// 		$('#item_unit_price1').removeAttr('disabled');
// 	}
//
// }
//
// function disableUnitPrice2(){
//
//
// 	if(	$('#funding_selection_2').val()== 0 ){
//
// 		$('#item_unit_price2').attr('disabled', true);
//
// 	}
// 	else{
// 		$('#item_unit_price2').removeAttr('disabled');
// 	}
//
// }
</script>
<div id="thickboxContainerDiv" class="edit_pop_board">
<div style="display:none" id="addNewSupplierDiv" >
	
	<?php include 'views/template/addSingleSupplier.php' ?>
		
</div>
<div style="display:none" id="addNewSupplierTypeDiv" >
	
	<?php include 'views/template/addSingleSupplierType.php' ?>
		
</div>
<div id="dbTablediv" >
<form id="ajax_form" method="post" enctype="multipart/form-data">

	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$requiredSymbol.$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'] ?></td>
			<td colspan="4"><?=$supplierTypeSel.$supplierRadios ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSymbol.$Lang['ePCM']['Mgmt']['Case']['Result']['DealPrice'] ?></td>
			<td><?=$dealPriceInput ?><span id="ajax_price"></span><?=$linterface->Get_Form_Warning_Msg('dealPriceEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DealPrice'], $Class='warnMsgDiv') ?></td>
		</tr>
		<tr id="notMinRow" style="display:none;">
			<td class="field_title"><?=$requiredSymbol.$Lang['ePCM']['Mgmt']['Case']['Result']['Reason']?></td>
			<td colspan="4"><?=$reasonTA ?><?php echo $reasonWarningDiv?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['Result']['Description'] ?></td>
			<td colspan="4"><?=$descTA ?></td>
		</tr>
        <?php  if($_PAGE['libPCM']->enableFundingSource()){?>
            <tr>
                <td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['FundingSource']?></td>
                <td>

                    <table id='fundingSourceTable' width="100%" class="inside_form_table_v30 inside_form_table">
                        <?=$htmlAry['FundingSource_SelectionBox']?>
                        <tr id='fundingSourceNewBtn'>
                            <td>
                                <?=$htmlAry['FundingSource_AddBtn']?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        <?php }?>
<!--		<tr>-->
<!--			<td class="field_title">--><?//=$Lang['ePCM']['Mgmt']['Case']['Result']['FundingSource']?><!-- (1)</td>-->
<!--			<td>--><?//=$funding_selection ?><!-- </td>-->
<!--			<td class="field_title">--><?//=$Lang['ePCM']['Mgmt']['Funding']['Unit_Price']?><!-- (1)</td>-->
<!--			<td colspan="4">$ <input name="itemUnitPrice1" id= 'item_unit_price1' type="text" value="--><?//=($result[UnitPrice1] !='')? ($result[UnitPrice1]) : 0 ?><!--" class="textboxnum"></td>-->
<!--			-->
<!--		</tr>-->
<!--		<tr>-->
<!--			<td class="field_title">--><?//=$Lang['ePCM']['Mgmt']['Case']['Result']['FundingSource']?><!-- (2)</td>-->
<!--			<td>--><?//=$funding_selection_2 ?><!-- </td>-->
<!--			<td class="field_title">--><?//=$Lang['ePCM']['Mgmt']['Funding']['Unit_Price']?><!-- (2)</td>-->
<!--			<td colspan="4">$ <input name="itemUnitPrice2" id= 'item_unit_price2' type="text" value="--><?//=($result[UnitPrice2] !='')? ($result[UnitPrice2]) : 0 ?><!-- "  class="textboxnum"></td>-->
<!--		</tr> -->
		<tr>
			<td class="field_title"><?=$requiredSymbol.$Lang['ePCM']['Mgmt']['Case']['Result']['DealDate'] ?></td>
			<td colspan="4"><?=$dealDateDatePicker ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['Document'] ?></td>
			<td colspan="4">
				<?=$templateBox?>
				<?=$pluploadBtn?>
	            <div id="<?=$pluploadContainerId?>"></div>
	            <div id="<?=$pluploadFileListDivId?>"></div>
	            <div id="<?=$deleteFileDivId?>"></div>
	            <?=$fileWarningDiv?>
			</td>
		</tr>
	</table>



<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $htmlAry['submitBtn'] ?>
		<?= $htmlAry['cancelBtn'] ?>
	<p class="spacer"></p>
</div>
<div id="ajax_result"></div>
<?=$htmlAry['hiddenField']?>
</div>
</form>
</div>