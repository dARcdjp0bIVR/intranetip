<?php
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$quotations = $postVar['Quotations'];
$quotationIdAry = Get_Array_By_Key($quotations, "QuotationID");
$supplierNameAry = Get_Array_By_Key($quotations, Get_Lang_Selection("SupplierNameChi","SupplierName"));
$countOfSupplier = count($quotations);
$data = array();
for($i=0;$i<$countOfSupplier;$i++){
	$data[$i] = array($quotationIdAry[$i],$supplierNameAry[$i]);
}
$supplier_selector = getSelectByArray($data, $tags='id="review_email_quotationID"  onchange="refreshEmailReview()"', $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);

$subject = $Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Subject'];
$content =$Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Body'];

$token_url_start = '<a href="javascript:void(0);">';
$token_url = 'http://123.456.78.1/api/sample.php?t=abcdefghijklmnop';
$token_url_end = '</a>';
$supplier_name = '{供應商名稱}';
$case_name = '{採購名稱}';
$case_number = '{採購編號}';
$api_password = '{samplepassword}';
$school_name = '{學校名稱}';

$sender_email = 'abc@abc.com';

?>
<script>
var token_url_start = '<?=$token_url_start?>';
var token_url = '<?= $token_url ?>';
var token_url_end = '<?= $token_url_end ?>';
var supplier_name = '<?= $supplier_name ?>';
var api_password = '<?= $api_password ?>';
var case_name = '<?= $case_name ?>';
var case_number = '<?= $case_number ?>';
var school_name = '<?= $school_name ?>';
function updateSubject(subject){
	setSubject(filter(subject));
}
function updateContent(content){
	setContent(filter(content));
}
function getSubject(){
	var subject = $('#subject_preview').html();
	return subject;
}
function getContent(){
	var content = $('#content_preview').html();
	return content;
}
function setSubject(text){
	$('#subject_preview').html(text);
}
function setContent(text){
	$('#content_preview').html(text);
}
function filter(text){
	text = text.replace("<!--TOKEN_URL_START-->",token_url_start);
	text = text.replace("<!--TOKEN_URL-->",token_url);
	text = text.replace("<!--TOKEN_URL_END-->",token_url_end);
	text = text.replace("<!--SUPPLIER_NAME-->",supplier_name);
	text = text.replace("<!--CASE_NAME-->",case_name);
	text = text.replace("<!--CASE_NUMBER-->",case_number);
	text = text.replace("<!--API_PASSWORD-->",api_password);
	text = text.replace("<!--SCHOOL_NAME-->",school_name);
	return text;
}
function performFiltering(){
	setSubject(filter(getSubject()));
	setContent(filter(getContent()));
}
$(function(){
	performFiltering();
})
function refreshEmailReview(){
	var quotationID = $('#review_email_quotationID option:selected').val();
	$('#ajax_email_preview').html('<img src="<?=$PATH_WRT_ROOT?>images/2009a/indicator.gif">');
	$.ajax({
        type:"POST",
        url: "index.php?p=ajax.emailpreview.get",
        data: {
			"quotationID"		:	quotationID
			},
        success:function(data)
        {
        	$('#ajax_email_preview').html(data);
        }
      })
}
</script>
<div id="thickboxContainerDiv"  class="edit_pop_board">

<form id="ajax_form" method="post" enctype="multipart/form-data">

	<table class="form_table_v30">
		<tr style="display:none">
			<td class="field_title">Subject</td>
			<td><?= $linterface->GET_TEXTBOX($Id='email_subject', $Name='email_subject', $subject, $OtherClass='', $OtherPar=array('onkeyup'=>"updateSubject(this.value)")) ?></td>
		</tr>
		<tr style="display:none">
			<td class="field_title">Content</td>
			<td><?= $linterface->GET_TEXTAREA($taName='email_content', $taContents=$content, $taCols=70, $taRows=5, $OnFocus = "", $readonly = "", $other='onkeyup="updateContent(this.value)"', $class='', $taID='', $CommentMaxLength='') ?></td>
		</tr>
		<tr>
			<td class="field_title"><?= $Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'] ?></td>
			<td><?= $supplier_selector ?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Email']['Preview'] ?></td>
			<td id="ajax_email_preview">

			</td>
			 
		</tr>
	</table>

	<div id="editBottomDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
			<?= $htmlAry['submitBtn'] ?>
			<?= $htmlAry['cancelBtn'] ?>
		<p class="spacer"></p>
	</div>
	
	<?=$htmlAry['hiddenField'] ?>
</form>
</div>