<?php 
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$requiredSymbol = $linterface->RequiredSymbol();
$quotesDetails = $postVar['quotesDetails'];
$status = $postVar['status'];

// for plupload
$editMode = (empty($quotesDetails))? false:true;
$sessionToken = time();
$pluploadButtonId = 'UploadButton';
$pluploadFileListDivId = 'FileListDiv';
$pluploadContainerId = 'pluploadDiv';
$deleteFileDivId = 'willDeleteFileDiv';
$pluploadBtn = $linterface->GET_SMALL_BTN($Lang['General']['PleaseSelectFiles'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
$fileWarningDiv = $linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv tabletextrequire');

$quoteStatusAry = array(array('-1','No submition'),array('-2','late submition'),array('1','submitted'));
$hiddenF = '';
$hiddenF .= $linterface->GET_HIDDEN_INPUT('quotationID', 'quotationID', $quotesDetails['QuotationID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $quotesDetails['CaseID']);
$hiddenF .= $linterface->GET_HIDDEN_INPUT('session', 'session', $sessionToken);
$htmlAry['hiddenField'] = $hiddenF;

if(!empty($quotesDetails)){
	$quoteDateTime = explode(' ',$quotesDetails['QuoteDate']);
	$quoteDate = $quoteDateTime[0];
	$quoteTime = $quoteDateTime[1];
}
$quoteDateDatePicker = $linterface->GET_DATE_PICKER('datePickerQuoteDate', $quoteDate);
$quoteDateTimeSel = $linterface->Get_Time_Selection('timeSelectQuoteDate', $quoteTime, $others_tab='',$hideSecondSeletion=true, array(1,5,1));

$remarksTA = $linterface->GET_TB_TEXTAREA('remarks', $quotesDetails['Remarks'], $taCols=70, $taRows=5);

//Status

$status_array = array('1'=>$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['Submitted'],'0'=>$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['NoReply']);
foreach($status_array as $_key => $_value){
	if($status==$_key){
		$checked = true;
	}
	$statusRadio .= $linterface->Get_Radio_Button('status_'.$_key, 'status', $Value=$_key, $checked, $Class="", $Display=$_value, $Onclick="statusRadioClick(this.value);",$isDisabled=0).'&nbsp;&nbsp;';
	$checked = false;
}
$htmlAry['submitBtn']=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="checkForm();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
$htmlAry['cancelBtn']=$linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "ajax_recover_tempRemove();js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>
<?=$linterface->Include_Plupload_JS_CSS()?>
<style>
<?php if($status==0){ ?>
.submit{
	display:none;
}
<?php } ?>
</style>
<script>
function statusRadioClick(value){
	switch(value){
	case '0':
		$('.submit').hide();
	break;
	case '1':
		$('.submit').show();
	break;
	}
}
function checkForm(){
	$('#FileWarnDiv').hide();
	// check file upload
	var warnDv = $('#FileWarnDiv');
	if(!jsIsFileUploadCompleted()) {
		warnDv.html('<?=$Lang['ePCM']['General']['jsWaitAllFilesUploaded']?>').show();
		return false;
	}

	//var status = $('input[name="status"]:checked').val();
	
	submitQuotationDetails();
}

// file_uploader must place before $(document).ready inside thickbox
var file_uploader = {};
var FileCount = 0;
$(document).ready( function() {
	<?php if($editMode){ ?>
		ajax_reload_saved_attachment();
    <?php } ?>
	initUploadAttachment('pluploadDiv','UploadButton');
});

function initUploadAttachment(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length == 0) return;

	file_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'index.php?p=file.process&session=<?=$sessionToken?>',
			max_file_size : '200mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>
	    });
	    
	file_uploader.init();

	file_uploader.bind('BeforeUpload', function(up, file){
		FileCount += 1; // global var
	});
	
	file_uploader.bind('FilesAdded', function(up, files) {
		
        $.each(files, function(i, file) {
            $('#'+containerId).append(
                '<div id="' + file.id + '">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
            '</div>');
        });
 		
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });
    
    file_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
	});
	
    file_uploader.bind('UploadComplete', function(up, files) {
		var postData = $('form#ajax_form').serialize();
		var fileCount = files.length;
		files.reverse();
		ajax_update_attachment(fileCount, postData, files);
	});
    container.attr('style','');
}

function ajax_update_attachment(numberLeft, postData, files)
{
	var index = numberLeft - 1;
	if(index >= 0) {
		var finalPostData = postData;
		<?php if($is_mobile_tablet_platform){ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['target_name']);
		<?php }else{ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['name']);
		<?php } ?>
		$.post(
			'index.php?p=file.attach.update',
			finalPostData,
			function(data){
				eval(data);
				if(file_uploader.removeFile) {
					$('#' + files[index].id).remove();
					file_uploader.removeFile(files[index]);
				}
				FileCount -= 1; // global var
				numberLeft -= 1;
				if(numberLeft > 0) {
					ajax_update_attachment(numberLeft, postData, files);
				}
			}
		);
	}
}

// hide original plupload file list, actual file list is return from  ajax_update_attachment
function bindClickEventToElements()
{
	$('a').bind('click',afterClickedLinksButtons);
}
var isCloseWindow = true;
function afterClickedLinksButtons()
{
	isCloseWindow = false;
	setTimeout(function(){isCloseWindow=true;},1);
}

function Check_Total_Filesize(CheckNewOnly){
	return true;
}

function ajax_delete_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.attach.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function download_attachment(target){
	window.location = "index.php?p=file.download."+target;
}

function ajax_reload_saved_attachment(){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.quoList",
        data: {
			"session" : "<?=$sessionToken?>",
			"quoID"  : "<?=$quotesDetails['QuotationID']?>",
			"caseID" : "<?=$quotesDetails['CaseID']?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_delete_saved_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_recover_tempRemove(){
	postdata = $('form#ajax_form').serialize()
	$.post(
			'index.php?p=file.recover',
			postdata,
			function(response){
			}
	);
}

function jsIsFileUploadCompleted(){
	var totalSelected = file_uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<file_uploader.files.length;i++) {
		if(file_uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	return totalDone == totalSelected;
}
</script>
<div id="thickboxContainerDiv"  class="edit_pop_board">

<form id="ajax_form" method="post" enctype="multipart/form-data">

	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier']?></td>
			<td><?=$quotesDetails[Get_Lang_Selection('SupplierNameChi','SupplierName')]?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['Status'] ?></td>
			<td><?=$statusRadio ?></td>
		</tr>
		<tr class="submit">
			<td class="field_title"><?=$requiredSymbol.$Lang['ePCM']['Mgmt']['Case']['QuotationDate']?></td>
			<td><?=$quoteDateDatePicker?> <?=$quoteDateTimeSel ?></td>
		</tr>
		<tr class="submit">
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['Document']?></td>
			<td>
				<?=$templateBox?>
    			<?=$pluploadBtn?>
                <div id="<?=$pluploadContainerId?>"></div>
                <div id="<?=$pluploadFileListDivId?>"></div>
                <div id="<?=$deleteFileDivId?>"></div>
                <?=$fileWarningDiv?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['Remarks'] ?></td>
			<td><?=$remarksTA ?></td>
		</tr>
	</table>

	<div id="editBottomDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
			<?= $htmlAry['submitBtn'] ?>
			<?= $htmlAry['cancelBtn'] ?>
		<p class="spacer"></p>
	</div>
	<div id="ajax_quotation_details"></div>
	<?=$htmlAry['hiddenField'] ?>
</form>
</div>