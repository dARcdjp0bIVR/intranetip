<?php
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['view_data'];
$caseID = $postVar['caseID'];
$stageID = $_PAGE['view_data']['stageID'];
$ruleID = $_PAGE['POST']['ruleID'];
// for plupload
$sessionToken = time();
$pluploadButtonId = 'UploadButton';
$pluploadFileListDivId = 'FileListDiv';
$pluploadContainerId = 'pluploadDiv';
$deleteFileDivId = 'willDeleteFileDiv';
$pluploadBtn = $linterface->GET_SMALL_BTN($Lang['General']['PleaseSelectFiles'], "button", "", $pluploadButtonId, $ParOtherAttribute="", $OtherClass="", $ParTitle='');
if($stageID==2){
	$templateBox = $linterface->getTemplateFileUI($ePCMcfg['Template']['Invitation'], $ruleID, $getAttachWithoutRuleIDAndRuleID=true, $caseID);
}
$fileWarningDiv = $linterface->Get_Thickbox_Warning_Msg_Div('FileWarnDiv','','FileWarnDiv tabletextrequire');

$htmlAry['hiddenField'] .= $linterface->GET_HIDDEN_INPUT('session', 'session', $sessionToken);
$htmlAry['hiddenField'] .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseID);
$htmlAry['hiddenField'] .= $linterface->GET_HIDDEN_INPUT('stageID', 'stageID', $stageID);

$htmlAry['submitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="uploadInvitationDocument();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "ajax_recover_tempRemove();js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
?>
<?=$linterface->Include_Plupload_JS_CSS()?>
<script>
// file_uploader must place before $(document).ready inside thickbox
var file_uploader = {};
var FileCount = 0;
$(document).ready( function() {
	ajax_reload_saved_attachment();
	initUploadAttachment('pluploadDiv','UploadButton');
	adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
});

function initUploadAttachment(containerId,buttonId)
{
	var container = $('#' + containerId);
	var btn = $('#' + buttonId);
	
	if(container.length == 0) return;

	file_uploader = new plupload.Uploader({
	        runtimes : 'html5,flash,silverlight,html4,gears,browserplus',
	        browse_button : buttonId,
	        container : containerId,
			flash_swf_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.flash.swf',
			silverlight_xap_url : '<?=$PATH_WRT_ROOT?>templates/jquery/plupload/plupload.silverlight.xap',
			url : 'index.php?p=file.process&session=<?=$sessionToken?>',
			max_file_size : '200mb',
			chunk_size : '1mb',
			unique_names : <?=($is_mobile_tablet_platform?"true":"false")?>
	    });
	    
	file_uploader.init();

	file_uploader.bind('BeforeUpload', function(up, file){
		FileCount += 1; // global var
	});
	
	file_uploader.bind('FilesAdded', function(up, files) {
		
        $.each(files, function(i, file) {
            $('#'+containerId).append(
                '<div id="' + file.id + '">' +
                file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
            '</div>');
        });
 		
        up.refresh(); // Reposition Flash/Silverlight
        up.start();
    });
    
    file_uploader.bind('UploadProgress', function(up, file) {
	        $('#' + file.id + " b").html(file.percent + "%");
	});
	
    file_uploader.bind('UploadComplete', function(up, files) {
		var postData = $('form#ajax_form').serialize();
		var fileCount = files.length;
		files.reverse();
		ajax_update_attachment(fileCount, postData, files);
	});

    container.attr('style','');
}

function ajax_update_attachment(numberLeft, postData, files)
{
	var index = numberLeft - 1;
	if(index >= 0) {
		var finalPostData = postData;
		<?php if($is_mobile_tablet_platform){ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['target_name']);
		<?php }else{ ?>
		finalPostData += '&upload_file_name=' + encodeURIComponent(files[index]['name']);
		<?php } ?>
		$.post(
			'index.php?p=file.attach.update',
			finalPostData,
			function(data){
				eval(data);
				if(file_uploader.removeFile) {
					$('#' + files[index].id).remove();
					file_uploader.removeFile(files[index]);
				}
				FileCount -= 1; // global var
				numberLeft -= 1;
				if(numberLeft > 0) {
					ajax_update_attachment(numberLeft, postData, files);
				}
			}
		);
	}
}

// hide original plupload file list, actual file list is return from  ajax_update_attachment
function bindClickEventToElements()
{
	$('a').bind('click',afterClickedLinksButtons);
}
var isCloseWindow = true;
function afterClickedLinksButtons()
{
	isCloseWindow = false;
	setTimeout(function(){isCloseWindow=true;},1);
}

function Check_Total_Filesize(CheckNewOnly){
	return true;
}

function ajax_delete_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.attach.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function download_attachment(target){
	window.location = "index.php?p=file.download."+target;
}

function ajax_reload_saved_attachment(){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.caseList",
        data: {
			"session" : "<?=$sessionToken?>",
			"caseID"  : "<?=$caseID?>",
			"stageID" : "<?=$stageID ?>",
			"type" : "2"
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_delete_saved_attachment(target){
	$.ajax({
        type:"POST",
        url: "index.php?p=file.delete",
        data: {
			"AttachmentID"		:	target,
			"session" : "<?=$sessionToken?>",
			},
        success:function(data)
        {
        	eval(data);
        }
      })
}

function ajax_recover_tempRemove(){
	postdata = $('form#ajax_form').serialize()
	$.post(
			'index.php?p=file.recover',
			postdata,
			function(response){
			}
	);
}

function uploadInvitationDocument(){

	$('#FileWarnDiv').hide();
	// check file upload
	var warnDv = $('#FileWarnDiv');
	if(!jsIsFileUploadCompleted()) {
		warnDv.html('<?=$Lang['ePCM']['General']['jsWaitAllFilesUploaded']?>').show();
		return false;
	}
	
	var ajaxFormData = $('form#ajax_form').serialize();
	$.post(
			"index.php?p=mgmt.invitationDocument.update", 
			ajaxFormData,
			function(ReturnData)
			{
				$('div#ajax_result').html(ReturnData);
	 			if(ReturnData=='Saved'){
	 				js_Hide_ThickBox();
	 				location.reload();
	 			}
			}
		);
}

function jsIsFileUploadCompleted(){
	var totalSelected = file_uploader.files.length;
	var totalDone = 0;
	for(var i=0;i<file_uploader.files.length;i++) {
		if(file_uploader.files[i].status == plupload.DONE) {
			totalDone++;
		}
	}
	return totalDone == totalSelected;
}
</script>
<div id="thickboxContainerDiv">
<div id="thickboxContentDiv">
<form id="ajax_form" method="post" enctype="multipart/form-data">
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['Document'] ?></td>
			<td>
				<?=$templateBox?>
				<?=$pluploadBtn?>
	            <div id="<?=$pluploadContainerId?>"></div>
	            <div id="<?=$pluploadFileListDivId?>"></div>
	            <div id="<?=$deleteFileDivId?>"></div>
	            <?=$fileWarningDiv?>
			</td>
		</tr>
	</table>
	<?=$htmlAry['hiddenField']?>
	<div id="editBottomDiv" class="edit_bottom_v30">
		<p class="spacer"></p>
			<?= $htmlAry['submitBtn'] ?>
			<?= $htmlAry['cancelBtn'] ?>
		<p class="spacer"></p>
	</div>
	<div id="ajax_result"></div>
</form>
</div>
</div>