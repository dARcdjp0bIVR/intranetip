<?php
$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];

$caseID = $postVar['caseID'];

$htmlAry['delReasonTA'] = $linterface->GET_TEXTAREA('delReason', $delReason, $taCols=70, $taRows=5);
$htmlAry['hiddenField'] .= $linterface->GET_HIDDEN_INPUT('caseID', 'caseID', $caseID);

$htmlAry['submitBtn']=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="goTermination();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
$htmlAry['cancelBtn']=$linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

?>

<form id="ajax_form" method="post">
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$Lang['ePCM']['Mgmt']['Case']['DeleteReason'] ?></td>
		<td><?=$htmlAry['delReasonTA'] ?><?=$linterface->Get_Form_Warning_Msg('reasonEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['InputDeleteReason'], $Class='warnMsgDiv') ?></td>
	</tr>
</table>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $htmlAry['submitBtn'] ?>
		<?= $htmlAry['cancelBtn'] ?>
	<p class="spacer"></p>
</div>
<div id="ajax_termination"></div>
<?= $htmlAry['hiddenField'] ?>
</form>