<?php
/*
#######	Change Log ############################################################
#	Date:	2016-09-19	Omas
#	- add "add supplier type" when $sys_custom['ePCM']['enableGroupSupplierType'] is on
#
#	Date:	2016-07-05	Kenneth
#	- add search function
#
###############################################################################
*/

$PATH_WRT_ROOT = $_PAGE['PATH_WRT_ROOT'];
$linterface = $_PAGE['libPCM_ui'];
$postVar = $_PAGE['views_data'];
$requiredSymbol = $linterface->RequiredSymbol();

$supplierTypeInfoAry = $postVar['supplierTypeAry'];
$supplierTypeInfoAryForAdd = $postVar['supplierTypeAryForAdd'];

$supplierTypeSel = getSelectByAssoArray($supplierTypeInfoAry, $tags='name="typeID" id="typeID" onchange="onChangeSupplierType(this.value);"', $selected="", $all=1, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);
$supplierTypeSel_addNewSupplier = getSelectByAssoArray($supplierTypeInfoAry, $tags='name="typeID_addNew" id="typeID_addNew"', $selected="", $all=0, $noFirst=0, $FirstTitle="", $ParQuoteValue=1);

$htmlAry['submitBtn']=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], 'button', $ParOnClick="submitAddSupplier();", $ParName="", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="");
$htmlAry['cancelBtn']=$linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

//search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);


//Add new supplier 
$btnAry[] = array('new', 'javascript: showAddNewSupplier();');
$htmlAry['contentTool'] = $linterface->Get_Content_Tool_By_Array_v30($btnAry);
?>
<script>

$('#keyword').keypress(function (e) {
    if (e.which == 13) {
        //Search Enter Key press
        var keyword = $('#keyword').val().trim();
    	onSearchSupplier(keyword);
    }
});

$(function() {
	onChangeSupplierType('');
});

function afterAddSupplierJs(){
	onChangeSupplierType('');
}
</script>



<div id="thickboxContainerDiv">
	<?=$htmlAry['searchBox'] ?>
	<?= $htmlAry['contentTool'] ?>
	<br>
	<div style="display:none" id="addNewSupplierDiv" >
	
	<?php include 'views/template/addSingleSupplier.php' ?>
		
	</div>
	<div style="display:none" id="addNewSupplierTypeDiv" >
	
	<?php include 'views/template/addSingleSupplierType.php' ?>
	
	</div>
	<div id="dbTablediv" >
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$Lang['ePCM']['SettingsArr']['SupplierArr']['Type']?></td>
				<td>
					<div id="typeDiv"><?=$supplierTypeSel?></div>
					<?=$linterface->Get_Form_Warning_Msg('supplierIDEmptyWarnDiv', $Lang['ePCM']['Mgmt']['Case']['Warning']['SelectSupplier'], $Class='warnMsgDiv') ?>
				</td>
			</tr>
		</table>
		<div id="ajax_supplier_table"></div>
		<div id="ajax_show_supplier_info"></div>
		
		
			<div id="editBottomDiv" class="edit_bottom_v30">
				<p class="spacer"></p>
					<?= $htmlAry['submitBtn'] ?>
					<?= $htmlAry['cancelBtn'] ?>
				<p class="spacer"></p>
			</div>
		<div id="ajax_add_supplier"></div>
	</div>
</div>