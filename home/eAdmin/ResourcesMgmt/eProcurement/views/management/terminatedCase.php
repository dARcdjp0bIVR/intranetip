<?php
$postVar = $_PAGE['views_data'];
$sql = $postVar['sql'];


$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
$pageNo = ($postVar['pageNo'] == '') ? 1 : $postVar['pageNo'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];

$columns = array();
$columns[] = array('Code',$Lang['ePCM']['Group']['Code'],'10%');
$columns[] = array('CaseName',$Lang['ePCM']['Mgmt']['Case']['CaseName2'],'20%');
$columns[] = array('ItemName',$Lang['ePCM']['Mgmt']['Case']['FinancialItem'],'20%');
$columns[] = array('CategoryName',$Lang['ePCM']['Category']['Category'],'20%');
$columns[] = array('Applicant',$Lang['ePCM']['Mgmt']['Case']['Applicant'],'10%');
$columns[] = array('ApplicationDate',$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'],'10%');
$columns[] = array('Budget',$Lang['ePCM']['Mgmt']['Case']['Budget'],'10%');
$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, false);

### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('filterType', 'filterType', $filterType);
$htmlAry['hiddenField'] = $hiddenF;

$dbTableBtnAry = array();
//$dbTableBtnAry[] = array('delete', 'javascript: goDelete();');
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);


?>
<script>
function goDelete(){
	var checked = targetIDAry();
	if(checked.length<=0){
		alert("Please choose at least one role");
		return;
	}else{
		if(!confirm("Confrim Delete?")){
			return;
		}
		$('form#form1').attr('action', 'index.php?p=mgmt.case.perminantDelete');
		$('form#form1').submit();
	}
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
</script>
<form id="form1" name="form1" method="POST" action="">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
<div class="table_filter">
<?= $filter ?>
</div>
<p class="spacer"></p>
<br style="clear:both;" />
<br style="clear:both;" />
<?=$htmlAry['dbTableActionBtn']?>
<?= $libdbtable?>

<?= $htmlAry['hiddenField']?>
</form>