<?php
/*
 * Date 2019-06-03	Henry
 * - support confirm completed case by principal
 * Date 2016-11-09	Villa
 * - add Academic Year Filter
 */
$postVar = $_PAGE['views_data'];
$sql = $postVar['sql'];
$keyword = $_POST['keyword'];
$currentType = $_POST['currentType'];
$currentConfirmStatus = $_POST['currentConfirmStatus'];
$currentFinancialItem = $_POST['currentFinancialItem'];
$currentCategory = $_POST['currentCategory'];
$currentSupplier = $_POST['currentSupplier'];
$currentApplicantUser = $_POST['currentApplicantUser'];
$AcademicYearfilterValue = $_POST['AcademicYearfilterValue']? $_POST['AcademicYearfilterValue']:$_SESSION['ePCM']['Setting']['General']['defaultAcademicYearID'];

$db = $_PAGE['Controller']->requestObject('libPCM_db','includes/ePCM/libPCM_db.php');

if(!empty($AcademicYearfilterValue)){
	$sql .= " AND ipc.AcademicYearID = '".$AcademicYearfilterValue."'";
}
if(isset($currentConfirmStatus) && $currentConfirmStatus !== ''){
	$sql .= " AND ipc.IsPrincipalApproved  = '".$currentConfirmStatus."' AND rule.CeillingPrice > 0 AND rule.CeillingPrice <= '".$sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']."'";
}
if(!empty($currentType) && $currentType !== ''){
	$sql .= " AND rule.QuotationType = '".$currentType."'";
}
if(!empty($currentFinancialItem) && $currentFinancialItem !== ''){
	$sql .= " AND ipgf.ItemID = '".$currentFinancialItem."'";
}
if(!empty($currentCategory) && $currentCategory !== ''){
	$sql .= " AND cat.CategoryID = '".$currentCategory."'";
}
if(!empty($currentSupplier) && $currentSupplier !== ''){
	$sql .= " AND dealsupplier.SupplierID = '".$currentSupplier."'";
}
if(!empty($currentApplicantUser) && $currentApplicantUser !== ''){
	if($currentApplicantUser[0] == 'U'){
		$sql .= " AND ApplicantType='I' AND applicantuser.UserID = '".substr($currentApplicantUser, 1)."'";
	}
	else if($currentApplicantUser[0] == 'G'){
		$sql .= " AND ApplicantType<>'I' AND applicantgroup.GroupID = '".substr($currentApplicantUser, 1)."'";
	}
}
if(!empty($keyword) && $keyword!==''){
	$sql .= " AND (
				ipc.Code LIKE '%".$db->Get_Safe_Sql_Like_Query($keyword)."%'
				OR ipc.CaseName LIKE '%".$db->Get_Safe_Sql_Like_Query($keyword)."%'	
				OR ipc.Description LIKE '%".$db->Get_Safe_Sql_Like_Query($keyword)."%'			
				) ";
}

$order = ($postVar['order'] == '') ? 1 : $postVar['order'];	// 1 => asc, 0 => desc
$field = ($postVar['field'] == '') ? 0 : $postVar['field'];
// $pageNo = ($postVar['page_size_change'] == '') ? 1 : $postVar['page_size_change'];
$pageNo = ($postVar['pageNo'] == '') ? 1 : $postVar['pageNo'];
$page_size = ($postVar['numPerPage'] == '') ? 50 : $postVar['numPerPage'];

$columns = array();
$columns[] = array('Code',$Lang['ePCM']['Group']['Code'],'10%');
$columns[] = array('CaseName',$Lang['ePCM']['Mgmt']['Case']['CaseName2'],'10%');
$columns[] = array('ItemName',$Lang['ePCM']['Mgmt']['Case']['FinancialItem'],'10%');
$columns[] = array('CategoryName',$Lang['ePCM']['Category']['Category'],'10%');
$columns[] = array('Applicant',$Lang['ePCM']['Mgmt']['Case']['Applicant'],'10%');
$columns[] = array('ApplicationDate',$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'],'10%');
$columns[] = array('Budget',$Lang['ePCM']['Mgmt']['Case']['Budget'],'10%');
$columns[] = array('DealPrice',$Lang['ePCM']['Mgmt']['Case']['Result']['DealPrice'],'8%');
$columns[] = array('DealDate',$Lang['ePCM']['Mgmt']['Case']['Result']['DealDate'],'8%');
$columns[] = array('DealSupplier',$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'],'10%');
if($sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']){
	$columns[] = array('RecordStatus',$Lang['ePCM']['Mgmt']['Case']['Status']['ConfirmStatus'],'4%');
}

$setting = $_PAGE['Controller']->requestObject('libPCM_db_setting','includes/ePCM/libPCM_db_setting.php');
$principalUserIDAry = $setting->getPrincipalUserIDAry();
$isPrincipal = false;
foreach($principalUserIDAry as $principalUserID){
	if($principalUserID['UserID'] == $_SESSION['UserID']){
		$isPrincipal = true;
		break;
	}
}

$libdbtable = $_PAGE['libPCM_ui']->getDbTable($sql, $columns, $field, $order, $pageNo, $page_size, $sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']?$isPrincipal:false);

## Search Box
$htmlAry['searchBox'] = $_PAGE['libPCM_ui']->Get_Search_Box_Div('keyword', $keyword);

### filter hidden field
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentConfirmStatus', 'currentConfirmStatus', $currentConfirmStatus);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentType', 'currentType', $currentType);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentFinancialItem', 'currentFinancialItem', $currentFinancialItem);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentCategory', 'currentCategory', $currentCategory);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentSupplier', 'currentSupplier', $currentSupplier);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('currentApplicantUser', 'currentApplicantUser', $currentApplicantUser);

$htmlAry['hiddenField'] .= $hiddenF;

### db table hidden field
$hiddenF = '';
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('pageNo', 'pageNo', $pageNo);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('order', 'order', $order);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('field', 'field', $field);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('page_size_change', 'page_size_change', '');
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('numPerPage', 'numPerPage', $page_size);
$hiddenF .= $_PAGE['libPCM_ui']->GET_HIDDEN_INPUT('filterType', 'filterType', $filterType);
$htmlAry['hiddenField'] .= $hiddenF;

$AcademicYearFilter = $_PAGE['libPCM_ui']->getSelectAcademicYear('AcademicYearID','OnChange="selectAcademicYearFilter(this)"', 1, 0, $AcademicYearfilterValue).'';

### Quotation Type filter
$quotationTypeAry = array(array('N',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['N']),array('V',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['V']),array('Q',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['Q']),array('T',$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['T']));
$idArray = Get_Array_By_Key($quotationTypeAry,'0');
$showTextAry = Get_Array_By_Key($quotationTypeAry,'1');
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentType, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['AllType'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_quotationType')."\r\n";

### Financial Item filter
$financialItemAry = $db->getFinancialItem();
$idArray = Get_Array_By_Key($financialItemAry,'ItemID');
$showTextAry = Get_Array_By_Key($financialItemAry,Get_Lang_Selection('ItemNameChi','ItemName'));
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentFinancialItem, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Filter']['FinancialItem']['AllType'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_financialItem')."\r\n";

### Catrgory Type filter
$categoryInfoAry = $db->getCategoryInfo('');
$idArray = Get_Array_By_Key($categoryInfoAry,'CategoryID');
$showTextAry = Get_Array_By_Key($categoryInfoAry,Get_Lang_Selection('CategoryNameChi','CategoryName'));
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentCategory, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Filter']['Category']['AllType'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_category')."\r\n";

### pplicant User filter
$applicantUserInfoAry = $db->getCompletedCaseApplicantUserInfo($AcademicYearfilterValue);
$idArray = Get_Array_By_Key($applicantUserInfoAry,'UserID');
$showTextAry = Get_Array_By_Key($applicantUserInfoAry,Get_Lang_Selection('ChineseName','EnglishName'));
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentApplicantUser, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Filter']['Applicant']['AllType'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_applicantUser')."\r\n";

### Supplier filter
$supplierInfoAry = $db->getSupplierInfo();
$idArray = Get_Array_By_Key($supplierInfoAry['MAIN'],'SupplierID');
$showTextAry = Get_Array_By_Key($supplierInfoAry['MAIN'],Get_Lang_Selection('SupplierNameChi','SupplierName'));
$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentSupplier, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Filter']['Supplier']['AllType'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_supplier')."\r\n";

### Confirm Status filter
if($sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']){
	$confirmStatusAry = array(array('0',$Lang['ePCM']['Mgmt']['Case']['Status']['PendingForConfirmation']),array('1',$Lang['ePCM']['Mgmt']['Case']['Status']['Confirmed']));
	$idArray = Get_Array_By_Key($confirmStatusAry,'0');
	$showTextAry = Get_Array_By_Key($confirmStatusAry,'1');
	$filter .= $_PAGE['libPCM_ui']->getFilter($idArray,$showTextAry,$currentConfirmStatus, $ParSelected=true, $ParOnChange='selectFilter(this);', $ParSelLang=$Lang['ePCM']['Mgmt']['Case']['Status']['AllStatusForConfirmation'], $noFirst=0,$CategoryTypeID=0,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='sel_confirmStatus')."\r\n";
}

$auth = $_PAGE['Controller']->requestObject('libPCM_auth','includes/ePCM/libPCM_auth.php');

$dbTableBtnAry = array();

if($isPrincipal && $sys_custom['ePCM']['enablePrincipalApprovalCompletedCaseWithBudgetAtOrBelow']){
    $dbTableBtnAry[] = array('approve','javascript: goApprove();',$Lang['ePCM']['Mgmt']['Case']['Confirm']);
    $dbTableBtnAry[] = array('reject','javascript: goReject();',$Lang['ePCM']['Mgmt']['Case']['UndoConfirm']);
}
$htmlAry['dbTableActionBtn'] = $_PAGE['libPCM_ui']->Get_DBTable_Action_Button_IP25($dbTableBtnAry);
?>
<script>
function selectFilter(obj){
	switch(obj.name){
		case 'sel_confirmStatus':
			$('#currentConfirmStatus').val(obj.value);
			form1.submit();
		break;
		case 'sel_quotationType':
			$('#currentType').val(obj.value);
			form1.submit();
		break;
		case 'sel_financialItem':
			$('#currentFinancialItem').val(obj.value);
			form1.submit();
		break;
		case 'sel_category':
			$('#currentCategory').val(obj.value);
			form1.submit();
		break;
		case 'sel_supplier':
			$('#currentSupplier').val(obj.value);
			form1.submit();
		break;
		case 'sel_applicantUser':
			$('#currentApplicantUser').val(obj.value);
			form1.submit();
		break;
	}
}
function selectAcademicYearFilter(obj){
	$('#AcademicYearfilterValue').val(obj.value);
	form1.submit();
}

function goApprove(){
	var checked = targetIDAry();
	var checked2 = targetIDAry2();
	var checked3 =  targetIDAry3();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseAItem'] ?>");
		return;
	}else{
		if(checked2.length=='0'){
			if(checked3){
				$('form#form1').attr('action', 'index.php?p=mgmt.ApproveFinish');
				$('form#form1').submit();
			}else{
				alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['ApproveRight'] ?>");
			}
		}else{
			alert("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['ApplicationIsConfirmed'] ?>");
		}		
	}
}
function goReject(){
	var checked = targetIDAry();
	var checked3 =  targetIDAry3();
	var checked4 = targetIDAry4();
	if(checked.length<=0){
		alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseAItem'] ?>");
		return;
	}else{
		if(checked4.length=='0'){
			if(checked3){
				$('form#form1').attr('action', 'index.php?p=mgmt.RejectFinish');
				$('form#form1').submit();
			}else{
				alert("<?=$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['RejectRight'] ?>");
			}
		}else{
			alert("<?=$Lang['ePCM']['Mgmt']['Case']['Warning']['ApplicationIsNotConfirmed'] ?>");
		}
	}
		
}
function targetIDAry(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		checked.push($(this).attr('value'));
	});
	return checked;
}
function targetIDAry2(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		if($(this).attr('isapprove')){
			checked.push($(this).attr('isapprove'));
		}
		
	});
	return checked;
}
function targetIDAry3(){
	var checked = true;
	$('input[name=targetIdAry[]]:checked').each(function(){
		if($(this).attr('canapprove')){
			 checked = true;
		}else{
			checked =false;
		}
	});
	return checked;
}
function targetIDAry4(){
	var checked = [];
	$('input[name=targetIdAry[]]:checked').each(function(){
		if(!$(this).attr('isapprove')){
			checked.push($(this).attr('isapprove'));
		}
		
	});
	return checked;
}
</script>
<form id="form1" name="form1" method="POST" action="">
	<div class="content_top_tool">
		<?=$htmlAry['contentTool']?>
		<?=$htmlAry['searchBox']?>
		<br style="clear:both;">
	</div>
<!--<div class="table_filter">
<?= $filter ?>
</div>-->
<p class="spacer"></p>
<br style="clear:both;" />
<br style="clear:both;" />
<div align="left"><?= $AcademicYearFilter?><?= $filter ?></div>
<?=$htmlAry['dbTableActionBtn']?>
<?= $libdbtable?>

<?= $htmlAry['hiddenField']?>
<input type='hidden' id='AcademicYearfilterValue' name='AcademicYearfilterValue' value='<?=$AcademicYearfilterValue?>'>

</form>