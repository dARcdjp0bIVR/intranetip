<?php
$PATH_WRT_ROOT=$_PAGE['PATH_WRT_ROOT'];
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

function print_report_start(&$pdf,$academic_year,$str_rule){
	global $Lang;
	
	$pdf->WriteHTML('        <div class="title">'.$academic_year.$Lang['ePCM']['Report']['FCS']['AcademicYear'].'</div>');
	$pdf->WriteHTML('        <div class="title">'.$Lang['ePCM']['Report']['PCMRS']['Title'].'</div>');
	$pdf->WriteHTML('        <div class="budget">'.$str_rule.'</div>');
	$pdf->WriteHTML('        <table class="data">');
	$pdf->WriteHTML('            <thead>');
	$pdf->WriteHTML('                <tr>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Code'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Mgmt']['Case']['CaseName2'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Mgmt']['Case']['FinancialItem'].'</th>');
// 	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Mgmt']['Case']['Category'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Report']['FCS']['Applicant'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Group']['Budget'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Mgmt']['Case']['Result']['DealPrice'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['Mgmt']['Case']['Result']['DealDate'].'</th>');
	$pdf->WriteHTML('                    <th>'.$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'].'</th>');
	$pdf->WriteHTML('                </tr>');
	$pdf->WriteHTML('            </thead>');
	$pdf->WriteHTML('            <tbody>');
}

function print_report_end(&$pdf){
	$pdf->WriteHTML('            </tbody>');
	$pdf->WriteHTML('        </table>');
}

function print_page_break(&$pdf){
	$pdf->WriteHTML('        <pagebreak />');
}

$rows_case=$_PAGE['rows_case'];


$year_from=substr($_PAGE['data']['date_from'],0,4);
$year_to=substr($_PAGE['data']['date_to'],0,4);
$month_day_from=substr($_PAGE['data']['date_from'],4,6);
$month_day_to=substr($_PAGE['data']['date_to'],4,6);
if($month_day_from<'-09-01'){
	$year_from--;
}
if($month_day_to>='-09-01'){
	$year_to++;
}
if($_PAGE['data']['academic_year']!=''){
	$academic_year=date('Y',getStartOfAcademicYear('',$_PAGE['data']['academic_year'])).' - '.date('Y',getEndOfAcademicYear('',$_PAGE['data']['academic_year']));
}else{
	$academic_year=$year_from.' - '.$year_to;
}

//$rows_rule=$db->getRuleInfo($array_rule);
//$str_rule='';
//foreach((array)$rows_rule as $row_rule){
//	$str_rule.='$'.number_format($row_rule['FloorPrice'],2).($row_rule['CeillingPrice']==''?$Lang['ePCM']['Report']['FCS'][' or above']:(' - $'.number_format($row_rule['CeillingPrice'],2))).'<br/>';
//}

//start creating PDF
//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
$margin_left = 5; //mm
$margin_right = 5;
$margin_top = 5;
$margin_bottom = 10;
$margin_header = 0;
$margin_footer = 5;
$orientation='L';
$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer);
$pdf->setHTMLFooter('<div style="width:100%;text-align:center;">{PAGENO} / {nb}</div>');
$pdf->AddPage($orientation);
// Chinese use mingliu 
$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

//$pdf->WriteHTML('body{font-family:serif;}');
$pdf->WriteHTML('.title{font-size:24px;margin:auto;width:100%;text-align:center;font-weight:bold;}',1);
// $pdf->WriteHTML('.budget{margin:auto;width:100%;text-align:center;}',1);
$pdf->WriteHTML('table.data{border-collapse:collapse;border:1px solid #000000;margin-top:30px;table-layout:fixed;width:100%;}',1);
$pdf->WriteHTML('table.data th{border:1px solid #000000;padding:10px;font-size:14px;}',1);
$pdf->WriteHTML('table.data td{border:1px solid #000000;padding:5px;font-size:11px;}',1);
// $pdf->WriteHTML('table.data td.case_no{}',1);
// $pdf->WriteHTML('table.data td.amount{text-align:right;}',1);
// $pdf->WriteHTML('table.data td.remarks{}',1);
// $pdf->WriteHTML('table.data td.applicant{}',1);
// $pdf->WriteHTML('table.data td.date{text-align:center;}',1);
$pdf->WriteHTML('table.data td.center{text-align:center;}',1);

$pdf->WriteHTML('<html>');
$pdf->WriteHTML('    <head>');
$pdf->WriteHTML('    </head>');
$pdf->WriteHTML('    <body>');
$current_rule_range='###';
print_report_start($pdf,$academic_year,$str_rule);
// debug_pr($rows_case);
// die();

foreach((array)$rows_case as $row_case){

	
	if($row_case['ApplicantType']=='I'){
		$applicant=$row_case['ApplicantUserName'];
	}elseif($row_case['ApplicantType']=='G'){
		$applicant=$row_case[Get_Lang_Selection('GroupNameChi','GroupName')].'<br>('.$row_case['ApplicantUserName'].')';
	}

	
	$pdf->WriteHTML('                <tr>');
	$pdf->WriteHTML('                    <td class="case_no" width="10%">'.Get_String_Display($row_case['Code']).'</td>');
	$pdf->WriteHTML('                    <td class="case_name" width="10%">'.$row_case['CaseName'].'</td>');
	$pdf->WriteHTML('                    <td class="item" width="10%">'.Get_Lang_Selection($row_case['ItemNameChi'],$row_case['ItemName']).'</td>');
// 	$pdf->WriteHTML('                    <td class="category canBr" width="10%">'.Get_Lang_Selection($row_case['GroupNameChi'],$row_case['GroupName']).'</td>');
	$pdf->WriteHTML('                    <td class="applicant" width="10%">'.$applicant.'</td>');
	$applicationDateAry = explode(' ',$row_case['DateInput']);
	$pdf->WriteHTML('                    <td class="application_date center" width="10%">'.$applicationDateAry[0].'</td>');
	$pdf->WriteHTML('                    <td class="budget center" width="10%">$'.$row_case['Budget'].'</td>');
	if($row_case['rejected']){
		$pdf->WriteHTML('                    <td class="deal_price center" width="30%" colspan="3">'.$Lang['ePCM']['Report']['FCS']['Rejected'].'</td>');
	}else if($row_case['CurrentStage']!=10){
		$pdf->WriteHTML('                    <td class="deal_price center" width="30%" colspan="3">'.$Lang['ePCM']['Report']['FCS']['Application in progress'].'</td>');
	}else{
		$pdf->WriteHTML('                    <td class="deal_price center" width="10%">$'.$row_case['DealPrice'].'</td>');
		$pdf->WriteHTML('                    <td class="deal_date center" width="10%">'.Get_String_Display($row_case['DealDate']).'</td>');
		$pdf->WriteHTML('                    <td class="supplier" width="10%">'.Get_String_Display(Get_Lang_Selection($row_case['SupplierNameChi'],$row_case['SupplierName'])).'</td>');
	}
	$pdf->WriteHTML('                </tr>');
// 	$current_rule_range=$row_case['rule_range'];
	

	
}
print_report_end($pdf);
if(count($rows_case)<1){
	print_report_end($pdf);
	$pdf->WriteHTML('        '.$Lang['ePCM']['Report']['FCS']['No Applications']);
}else{
	print_report_end($pdf);
}
$pdf->WriteHTML('    </body>');
$pdf->WriteHTML('</html>');
	
$pdf->Output('Procurement_Record_Summary_'.date('YmdHis').'.pdf', 'I');

?>