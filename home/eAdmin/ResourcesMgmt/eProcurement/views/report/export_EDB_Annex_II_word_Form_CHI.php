<?php
$postVar= $_PAGE['POST'];
header("Content-type: application/vnd.ms-word");
# replace Wordfile.doc with whatever you want the filename to default to
header("Content-Disposition: attachment;Filename=".$postVar['codeEdit']."_".$postVar['nameEdit']."_Purchase-by-Oral_Quotation_Form _ANNEX II.doc");
header("Pragma: no-cache");
header("Expires: 0");

$current_date = date('d-m-Y');
$quotationItemNo = 0;
$resultExportAry = $postVar['resultForExportAry'];

$QuotationsAry = array_filter((array)$postVar['QuotationsAry']);
!empty($QuotationsAry)? $QuotationsAry = $postVar['QuotationsAry'] : $QuotationsAry = array($resultExportAry,array());
count($QuotationsAry)<2? array_push($QuotationsAry, array()):'';

//echo "<div style='font-size: 1em; line-height: 1.6em; color: #4E6CA3; padding:10px;' align='right'>Report Date: $current_date</div>";
//echo "<div style='font-size: 1em; line-height: 1.6em; color: #4E6CA3; padding:10px;' align='left'>$heading</div>";
//echo "<div style='font-size: 1em; line-height: 1.6em; color: #4E6CA3; padding:10px;' align='left'>$content</div>";

?>


<html xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office"
      xmlns:w="urn:schemas-microsoft-com:office:word"
      xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
      xmlns="http://www.w3.org/TR/REC-html40">

<head>
    <meta http-equiv=Content-Type content="text/html; charset=<?=returnCharset()?>>
    <meta name=ProgId content=Word.Document>
    <meta name=Generator content="Microsoft Word 15">
    <meta name=Originator content="Microsoft Word 15">
    <link rel=File-List
          href="EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/filelist.xml">
    <title>Guidelines on Tendering and Purchasing Procedures</title>
    <!--[if gte mso 9]><xml>
        <o:DocumentProperties>
            <o:Author>cir_clk</o:Author>
            <o:Template>Normal</o:Template>
            <o:LastAuthor>IsaacCheng</o:LastAuthor>
            <o:Revision>2</o:Revision>
            <o:TotalTime>10</o:TotalTime>
            <o:LastPrinted>2013-06-27T09:15:00Z</o:LastPrinted>
            <o:Created>2018-09-06T02:42:00Z</o:Created>
            <o:LastSaved>2018-09-06T02:42:00Z</o:LastSaved>
            <o:Pages>2</o:Pages>
            <o:Words>134</o:Words>
            <o:Characters>764</o:Characters>
            <o:Company>EDUCATION AND MANPOWER BUREAU</o:Company>
            <o:Lines>6</o:Lines>
            <o:Paragraphs>1</o:Paragraphs>
            <o:CharactersWithSpaces>897</o:CharactersWithSpaces>
            <o:Version>16.00</o:Version>
        </o:DocumentProperties>
    </xml><![endif]-->
    <link rel=themeData
          href="EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/themedata.thmx">
    <link rel=colorSchemeMapping
          href="EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/colorschememapping.xml">
    <!--[if gte mso 9]><xml>
        <w:WordDocument>
            <w:SpellingState>Clean</w:SpellingState>
            <w:GrammarState>Clean</w:GrammarState>
            <w:TrackMoves>false</w:TrackMoves>
            <w:TrackFormatting/>
            <w:DoNotHyphenateCaps/>
            <w:PunctuationKerning/>
            <w:DrawingGridHorizontalSpacing>6 點</w:DrawingGridHorizontalSpacing>
            <w:DrawingGridVerticalSpacing>6 點</w:DrawingGridVerticalSpacing>
            <w:DisplayHorizontalDrawingGridEvery>0</w:DisplayHorizontalDrawingGridEvery>
            <w:DisplayVerticalDrawingGridEvery>0</w:DisplayVerticalDrawingGridEvery>
            <w:UseMarginsForDrawingGridOrigin/>
            <w:ValidateAgainstSchemas/>
            <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>
            <w:IgnoreMixedContent>false</w:IgnoreMixedContent>
            <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>
            <w:DoNotPromoteQF/>
            <w:LidThemeOther>EN-US</w:LidThemeOther>
            <w:LidThemeAsian>ZH-TW</w:LidThemeAsian>
            <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>
            <w:DoNotShadeFormData/>
            <w:Compatibility>
                <w:BreakWrappedTables/>
                <w:SnapToGridInCell/>
                <w:WrapTextWithPunct/>
                <w:UseAsianBreakRules/>
                <w:UseWord2010TableStyleRules/>
                <w:DontGrowAutofit/>
                <w:SplitPgBreakAndParaMark/>
                <w:UseFELayout/>
            </w:Compatibility>
            <m:mathPr>
                <m:mathFont m:val="Cambria Math"/>
                <m:brkBin m:val="before"/>
                <m:brkBinSub m:val="&#45;-"/>
                <m:smallFrac m:val="off"/>
                <m:dispDef/>
                <m:lMargin m:val="0"/>
                <m:rMargin m:val="0"/>
                <m:defJc m:val="centerGroup"/>
                <m:wrapIndent m:val="1440"/>
                <m:intLim m:val="subSup"/>
                <m:naryLim m:val="undOvr"/>
            </m:mathPr></w:WordDocument>
    </xml><![endif]--><!--[if gte mso 9]><xml>
        <w:LatentStyles DefLockedState="false" DefUnhideWhenUsed="false"
                        DefSemiHidden="false" DefQFormat="false" LatentStyleCount="375">
            <w:LsdException Locked="false" QFormat="true" Name="Normal"/>
            <w:LsdException Locked="false" QFormat="true" Name="heading 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="heading 9"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index 9"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toc 9"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footnote text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="header"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footer"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="index heading"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            QFormat="true" Name="caption"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="table of figures"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="envelope address"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="envelope return"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="footnote reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="line number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="page number"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="endnote reference"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="endnote text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="table of authorities"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="toa heading"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Bullet 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Number 5"/>
            <w:LsdException Locked="false" QFormat="true" Name="Title"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Closing"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Signature"/>
            <w:LsdException Locked="false" Priority="1" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Default Paragraph Font"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="List Continue 2"/>
            <w:LsdException Locked="false" QFormat="true" Name="Subtitle"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Salutation"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Date"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text First Indent"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text First Indent 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Note Heading"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Body Text Indent 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Block Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Hyperlink"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="FollowedHyperlink"/>
            <w:LsdException Locked="false" QFormat="true" Name="Strong"/>
            <w:LsdException Locked="false" QFormat="true" Name="Emphasis"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Document Map"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Plain Text"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="E-mail Signature"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Top of Form"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Bottom of Form"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal (Web)"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Acronym"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Address"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Cite"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Code"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Definition"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Keyboard"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Preformatted"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Sample"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Typewriter"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="HTML Variable"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Normal Table"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="annotation subject"/>
            <w:LsdException Locked="false" Priority="99" SemiHidden="true"
                            UnhideWhenUsed="true" Name="No List"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Outline List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Simple 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Classic 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Colorful 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Columns 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Grid 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 4"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 5"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 6"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 7"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table List 8"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table 3D effects 3"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Contemporary"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Elegant"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Professional"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Subtle 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Subtle 2"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 1"/>
            <w:LsdException Locked="false" SemiHidden="true" UnhideWhenUsed="true"
                            Name="Table Web 2"/>
            <w:LsdException Locked="false" Priority="99" SemiHidden="true"
                            Name="Placeholder Text"/>
            <w:LsdException Locked="false" Priority="1" QFormat="true" Name="No Spacing"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 1"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="99" SemiHidden="true" Name="Revision"/>
            <w:LsdException Locked="false" Priority="34" QFormat="true"
                            Name="List Paragraph"/>
            <w:LsdException Locked="false" Priority="29" QFormat="true" Name="Quote"/>
            <w:LsdException Locked="false" Priority="30" QFormat="true"
                            Name="Intense Quote"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 1"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 1"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 1"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 1"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 1"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 2"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 2"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 2"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 2"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 2"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 2"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 3"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 3"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 3"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 3"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 3"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 3"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 4"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 4"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 4"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 4"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 4"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 4"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 5"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 5"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 5"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 5"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 5"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 5"/>
            <w:LsdException Locked="false" Priority="60" Name="Light Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="61" Name="Light List Accent 6"/>
            <w:LsdException Locked="false" Priority="62" Name="Light Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="63" Name="Medium Shading 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="64" Name="Medium Shading 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="65" Name="Medium List 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="66" Name="Medium List 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="67" Name="Medium Grid 1 Accent 6"/>
            <w:LsdException Locked="false" Priority="68" Name="Medium Grid 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="69" Name="Medium Grid 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="70" Name="Dark List Accent 6"/>
            <w:LsdException Locked="false" Priority="71" Name="Colorful Shading Accent 6"/>
            <w:LsdException Locked="false" Priority="72" Name="Colorful List Accent 6"/>
            <w:LsdException Locked="false" Priority="73" Name="Colorful Grid Accent 6"/>
            <w:LsdException Locked="false" Priority="19" QFormat="true"
                            Name="Subtle Emphasis"/>
            <w:LsdException Locked="false" Priority="21" QFormat="true"
                            Name="Intense Emphasis"/>
            <w:LsdException Locked="false" Priority="31" QFormat="true"
                            Name="Subtle Reference"/>
            <w:LsdException Locked="false" Priority="32" QFormat="true"
                            Name="Intense Reference"/>
            <w:LsdException Locked="false" Priority="33" QFormat="true" Name="Book Title"/>
            <w:LsdException Locked="false" Priority="37" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Bibliography"/>
            <w:LsdException Locked="false" Priority="39" SemiHidden="true"
                            UnhideWhenUsed="true" QFormat="true" Name="TOC Heading"/>
            <w:LsdException Locked="false" Priority="41" Name="Plain Table 1"/>
            <w:LsdException Locked="false" Priority="42" Name="Plain Table 2"/>
            <w:LsdException Locked="false" Priority="43" Name="Plain Table 3"/>
            <w:LsdException Locked="false" Priority="44" Name="Plain Table 4"/>
            <w:LsdException Locked="false" Priority="45" Name="Plain Table 5"/>
            <w:LsdException Locked="false" Priority="40" Name="Grid Table Light"/>
            <w:LsdException Locked="false" Priority="46" Name="Grid Table 1 Light"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark"/>
            <w:LsdException Locked="false" Priority="51" Name="Grid Table 6 Colorful"/>
            <w:LsdException Locked="false" Priority="52" Name="Grid Table 7 Colorful"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 1"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 1"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 1"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 2"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 2"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 2"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 3"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 3"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 3"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 4"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 4"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 4"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 5"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 5"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 5"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="Grid Table 1 Light Accent 6"/>
            <w:LsdException Locked="false" Priority="47" Name="Grid Table 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="48" Name="Grid Table 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="49" Name="Grid Table 4 Accent 6"/>
            <w:LsdException Locked="false" Priority="50" Name="Grid Table 5 Dark Accent 6"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="Grid Table 6 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="Grid Table 7 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="46" Name="List Table 1 Light"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark"/>
            <w:LsdException Locked="false" Priority="51" Name="List Table 6 Colorful"/>
            <w:LsdException Locked="false" Priority="52" Name="List Table 7 Colorful"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 1"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 1"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 1"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 1"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 1"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 1"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 2"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 2"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 2"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 2"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 2"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 2"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 3"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 3"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 3"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 3"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 3"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 3"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 4"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 4"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 4"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 4"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 4"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 4"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 5"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 5"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 5"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 5"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 5"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 5"/>
            <w:LsdException Locked="false" Priority="46"
                            Name="List Table 1 Light Accent 6"/>
            <w:LsdException Locked="false" Priority="47" Name="List Table 2 Accent 6"/>
            <w:LsdException Locked="false" Priority="48" Name="List Table 3 Accent 6"/>
            <w:LsdException Locked="false" Priority="49" Name="List Table 4 Accent 6"/>
            <w:LsdException Locked="false" Priority="50" Name="List Table 5 Dark Accent 6"/>
            <w:LsdException Locked="false" Priority="51"
                            Name="List Table 6 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="52"
                            Name="List Table 7 Colorful Accent 6"/>
            <w:LsdException Locked="false" Priority="99" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Mention"/>
            <w:LsdException Locked="false" Priority="99" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Smart Hyperlink"/>
            <w:LsdException Locked="false" Priority="99" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Hashtag"/>
            <w:LsdException Locked="false" Priority="99" SemiHidden="true"
                            UnhideWhenUsed="true" Name="Unresolved Mention"/>
        </w:LatentStyles>
    </xml><![endif]-->
    <style>
        <!--
        /* Font Definitions */
        @font-face
        {font-family:Wingdings;
            panose-1:5 0 0 0 0 0 0 0 0 0;
            mso-font-charset:2;
            mso-generic-font-family:auto;
            mso-font-pitch:variable;
            mso-font-signature:0 268435456 0 0 -2147483648 0;}
        @font-face
        {font-family:新細明體;
            panose-1:2 2 5 0 0 0 0 0 0 0;
            mso-font-alt:PMingLiU;
            mso-font-charset:136;
            mso-generic-font-family:roman;
            mso-font-pitch:variable;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        @font-face
        {font-family:細明體;
            panose-1:2 2 5 9 0 0 0 0 0 0;
            mso-font-alt:MingLiU;
            mso-font-charset:136;
            mso-generic-font-family:modern;
            mso-font-pitch:fixed;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        @font-face
        {font-family:"Cambria Math";
            panose-1:2 4 5 3 5 4 6 3 2 4;
            mso-font-charset:0;
            mso-generic-font-family:roman;
            mso-font-pitch:variable;
            mso-font-signature:3 0 0 0 1 0;}
        @font-face
        {font-family:"\@細明體";
            panose-1:2 2 5 9 0 0 0 0 0 0;
            mso-font-charset:136;
            mso-generic-font-family:modern;
            mso-font-pitch:fixed;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        @font-face
        {font-family:"\@新細明體";
            panose-1:2 2 5 0 0 0 0 0 0 0;
            mso-font-charset:136;
            mso-generic-font-family:roman;
            mso-font-pitch:variable;
            mso-font-signature:-1610611969 684719354 22 0 1048577 0;}
        @font-face
        {font-family:sөũ;
            panose-1:0 0 0 0 0 0 0 0 0 0;
            mso-font-alt:"Times New Roman";
            mso-font-charset:0;
            mso-generic-font-family:roman;
            mso-font-format:other;
            mso-font-pitch:auto;
            mso-font-signature:0 0 0 0 0 0;}
        /* Style Definitions */
        p.MsoNormal, li.MsoNormal, div.MsoNormal
        {mso-style-unhide:no;
            mso-style-qformat:yes;
            mso-style-parent:"";
            margin:0cm;
            margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:12.0pt;
            mso-bidi-font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            mso-no-proof:yes;}
        p.MsoFootnoteText, li.MsoFootnoteText, div.MsoFootnoteText
        {mso-style-noshow:yes;
            mso-style-unhide:no;
            margin:0cm;
            margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            layout-grid-mode:char;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            mso-no-proof:yes;}
        p.MsoHeader, li.MsoHeader, div.MsoHeader
        {mso-style-unhide:no;
            margin:0cm;
            margin-bottom:.0001pt;
            mso-pagination:widow-orphan;
            tab-stops:center 216.0pt right 432.0pt;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            mso-ansi-language:EN-GB;}
        p.MsoFooter, li.MsoFooter, div.MsoFooter
        {mso-style-unhide:no;
            margin:0cm;
            margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            tab-stops:center 207.65pt right 415.3pt;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            mso-no-proof:yes;}
        span.MsoFootnoteReference
        {mso-style-noshow:yes;
            mso-style-unhide:no;
            mso-style-parent:"";
            vertical-align:super;}
        p.MsoClosing, li.MsoClosing, div.MsoClosing
        {mso-style-unhide:no;
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:5.0pt;
            margin-bottom:.0001pt;
            mso-para-margin-top:0cm;
            mso-para-margin-right:0cm;
            mso-para-margin-bottom:0cm;
            mso-para-margin-left:18.0gd;
            mso-para-margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:12.0pt;
            mso-bidi-font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            letter-spacing:2.0pt;
            mso-no-proof:yes;}
        p.MsoSalutation, li.MsoSalutation, div.MsoSalutation
        {mso-style-unhide:no;
            mso-style-next:內文;
            margin:0cm;
            margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:12.0pt;
            mso-bidi-font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            letter-spacing:2.0pt;
            mso-no-proof:yes;}
        a:link, span.MsoHyperlink
        {mso-style-unhide:no;
            mso-style-parent:"";
            color:blue;
            text-decoration:underline;
            text-underline:single;}
        a:visited, span.MsoHyperlinkFollowed
        {mso-style-unhide:no;
            mso-style-parent:"";
            color:purple;
            text-decoration:underline;
            text-underline:single;}
        p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
        {mso-style-noshow:yes;
            mso-style-unhide:no;
            margin:0cm;
            margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:9.0pt;
            font-family:"Arial",sans-serif;
            mso-fareast-font-family:新細明體;
            mso-bidi-font-family:"Times New Roman";
            mso-no-proof:yes;}
        p.BodyText22, li.BodyText22, div.BodyText22
        {mso-style-name:"Body Text 22";
            mso-style-unhide:no;
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:-1.4pt;
            margin-bottom:.0001pt;
            text-align:justify;
            text-justify:inter-ideograph;
            text-indent:1.4pt;
            line-height:150%;
            mso-pagination:none;
            tab-stops:48.0pt 81.0pt;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:12.0pt;
            mso-bidi-font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            mso-no-proof:yes;}
        p.BodyText21, li.BodyText21, div.BodyText21
        {mso-style-name:"Body Text 21";
            mso-style-unhide:no;
            margin-top:0cm;
            margin-right:0cm;
            margin-bottom:0cm;
            margin-left:12.0pt;
            margin-bottom:.0001pt;
            text-align:justify;
            text-justify:inter-ideograph;
            text-indent:-12.0pt;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:14.0pt;
            mso-bidi-font-size:10.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";
            mso-no-proof:yes;}
        p.pagetitle, li.pagetitle, div.pagetitle
        {mso-style-name:page_title;
            mso-style-unhide:no;
            mso-margin-top-alt:auto;
            margin-right:0cm;
            mso-margin-bottom-alt:auto;
            margin-left:0cm;
            mso-pagination:widow-orphan;
            font-size:13.5pt;
            font-family:"sөũ",serif;
            mso-fareast-font-family:新細明體;
            mso-bidi-font-family:新細明體;
            color:#BB3333;
            font-weight:bold;}
        .MsoChpDefault
        {mso-style-type:export-only;
            mso-default-props:yes;
            font-size:10.0pt;
            mso-ansi-font-size:10.0pt;
            mso-bidi-font-size:10.0pt;
            mso-ascii-font-family:"Times New Roman";
            mso-fareast-font-family:新細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-font-kerning:0pt;}
        /* Page Definitions */
        @page
        {mso-page-border-surround-header:no;
            mso-page-border-surround-footer:no;
            mso-footnote-separator:url("EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/header.htm") fs;
            mso-footnote-continuation-separator:url("EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/header.htm") fcs;
            mso-endnote-separator:url("EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/header.htm") es;
            mso-endnote-continuation-separator:url("EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/header.htm") ecs;}
        @page WordSection1
        {size:21.0cm 842.0pt;
            margin:63.8pt 63.75pt 2.0cm 70.9pt;
            mso-header-margin:42.55pt;
            mso-footer-margin:34.0pt;
            mso-even-footer:url("EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/header.htm") ef1;
            mso-footer:url("EDB_Sample_Annex%20II_Oral%20Quotation%20Form_CHI.files/header.htm") f1;
            mso-paper-source:0;}
        div.WordSection1
        {page:WordSection1;}
        /* List Definitions */
        @list l0
        {mso-list-id:-2;
            mso-list-type:simple;
            mso-list-template-ids:390868780;}
        @list l0:level1
        {mso-level-start-at:0;
            mso-level-number-format:bullet;
            mso-level-text:*;
            mso-level-tab-stop:none;
            mso-level-number-position:left;
            margin-left:0cm;
            text-indent:0cm;}
        @list l1
        {mso-list-id:45616009;
            mso-list-type:hybrid;
            mso-list-template-ids:-1876530542 -1966806558 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l1:level1
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%1\)";
            mso-level-tab-stop:42.55pt;
            mso-level-number-position:right;
            margin-left:42.55pt;
            text-indent:-25.55pt;}
        @list l1:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;}
        @list l1:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l1:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l1:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l1:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l1:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l1:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l1:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l2
        {mso-list-id:93013125;
            mso-list-type:hybrid;
            mso-list-template-ids:-375456266 -1094159646 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l2:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l2:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l2:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l2:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l2:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l2:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l2:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l2:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l2:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3
        {mso-list-id:121845813;
            mso-list-template-ids:-1215636462;}
        @list l3:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l3:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l3:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4
        {mso-list-id:250283106;
            mso-list-type:hybrid;
            mso-list-template-ids:-1102707592 -2061761262 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l4:level1
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%1\)";
            mso-level-tab-stop:21.0pt;
            mso-level-number-position:left;
            margin-left:21.0pt;
            text-indent:-21.0pt;
            mso-ansi-font-size:8.0pt;}
        @list l4:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l4:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5
        {mso-list-id:290981023;
            mso-list-template-ids:-1215636462;}
        @list l5:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l5:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l5:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l6
        {mso-list-id:342324607;
            mso-list-type:hybrid;
            mso-list-template-ids:-1658443752 -613416392 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l6:level1
        {mso-level-start-at:5;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-48.0pt;}
        @list l6:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;}
        @list l6:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l6:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l6:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l6:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l6:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l6:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l6:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l7
        {mso-list-id:382413845;
            mso-list-template-ids:1268911580;}
        @list l7:level1
        {mso-level-text:"1\.%1";
            mso-level-tab-stop:25.5pt;
            mso-level-number-position:left;
            margin-left:25.5pt;
            text-indent:-25.5pt;}
        @list l7:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l7:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l7:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l7:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l7:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l7:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l7:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l7:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l8
        {mso-list-id:547301787;
            mso-list-type:hybrid;
            mso-list-template-ids:-1635611894 -1094159646 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l8:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l8:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l8:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l8:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l8:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l8:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l8:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l8:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l8:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l9
        {mso-list-id:640230030;
            mso-list-type:hybrid;
            mso-list-template-ids:1621418122 -1094159646 67698691 -777082688 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l9:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l9:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l9:level3
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%3\)";
            mso-level-tab-stop:69.0pt;
            mso-level-number-position:left;
            margin-left:69.0pt;
            text-indent:-21.0pt;
            mso-ansi-font-size:12.0pt;
            mso-bidi-font-size:12.0pt;
            mso-ascii-font-family:"Times New Roman";
            mso-fareast-font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";}
        @list l9:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l9:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l9:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l9:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l9:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l9:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l10
        {mso-list-id:698314264;
            mso-list-template-ids:-1703915042;}
        @list l10:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l10:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l10:level3
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%3\)";
            mso-level-tab-stop:69.0pt;
            mso-level-number-position:left;
            margin-left:69.0pt;
            text-indent:-21.0pt;
            mso-ansi-font-size:8.0pt;}
        @list l10:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l10:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l10:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l10:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l10:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l10:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l11
        {mso-list-id:784158853;
            mso-list-type:hybrid;
            mso-list-template-ids:-1969569310 -777082688 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l11:level1
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%1\)";
            mso-level-tab-stop:69.2pt;
            mso-level-number-position:left;
            margin-left:69.2pt;
            text-indent:-21.0pt;
            mso-ansi-font-size:12.0pt;
            mso-bidi-font-size:12.0pt;
            mso-ascii-font-family:"Times New Roman";
            mso-fareast-font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";}
        @list l11:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.2pt;
            mso-level-number-position:left;
            margin-left:48.2pt;
            text-indent:-24.0pt;}
        @list l11:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.2pt;
            mso-level-number-position:right;
            margin-left:72.2pt;
            text-indent:-24.0pt;}
        @list l11:level4
        {mso-level-tab-stop:96.2pt;
            mso-level-number-position:left;
            margin-left:96.2pt;
            text-indent:-24.0pt;}
        @list l11:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.2pt;
            mso-level-number-position:left;
            margin-left:120.2pt;
            text-indent:-24.0pt;}
        @list l11:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.2pt;
            mso-level-number-position:right;
            margin-left:144.2pt;
            text-indent:-24.0pt;}
        @list l11:level7
        {mso-level-tab-stop:168.2pt;
            mso-level-number-position:left;
            margin-left:168.2pt;
            text-indent:-24.0pt;}
        @list l11:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.2pt;
            mso-level-number-position:left;
            margin-left:192.2pt;
            text-indent:-24.0pt;}
        @list l11:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.2pt;
            mso-level-number-position:right;
            margin-left:216.2pt;
            text-indent:-24.0pt;}
        @list l12
        {mso-list-id:803080047;
            mso-list-type:hybrid;
            mso-list-template-ids:-1629988968 -1094159646 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l12:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l12:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l12:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l12:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l12:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l12:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l12:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l12:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l12:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l13
        {mso-list-id:842937356;
            mso-list-template-ids:1279929924;}
        @list l13:level1
        {mso-level-start-at:42;
            mso-level-tab-stop:46.5pt;
            mso-level-number-position:left;
            margin-left:46.5pt;
            text-indent:-46.5pt;
            font-family:"Times New Roman",serif;}
        @list l13:level2
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%2\)";
            mso-level-tab-stop:45.0pt;
            mso-level-number-position:left;
            margin-left:45.0pt;
            text-indent:-21.0pt;
            font-family:"Times New Roman",serif;}
        @list l13:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l13:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l13:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l13:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l13:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l13:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l13:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l14
        {mso-list-id:876741762;
            mso-list-type:hybrid;
            mso-list-template-ids:655660370 -1094159646 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l14:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l14:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l14:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l14:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l14:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l14:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l14:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l14:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l14:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l15
        {mso-list-id:1055738952;
            mso-list-template-ids:1621418122;}
        @list l15:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l15:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l15:level3
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%3\)";
            mso-level-tab-stop:69.0pt;
            mso-level-number-position:left;
            margin-left:69.0pt;
            text-indent:-21.0pt;
            mso-ansi-font-size:12.0pt;
            mso-bidi-font-size:12.0pt;
            mso-ascii-font-family:"Times New Roman";
            mso-fareast-font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";}
        @list l15:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l15:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l15:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l15:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l15:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l15:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l16
        {mso-list-id:1092625993;
            mso-list-type:hybrid;
            mso-list-template-ids:198209038 -560162624 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l16:level1
        {mso-level-start-at:51;
            mso-level-tab-stop:42.75pt;
            mso-level-number-position:left;
            margin-left:42.75pt;
            text-indent:-42.75pt;}
        @list l16:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;}
        @list l16:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l16:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l16:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l16:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l16:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l16:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l16:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l17
        {mso-list-id:1134568457;
            mso-list-template-ids:-1635611894;}
        @list l17:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l17:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l17:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l17:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l17:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l17:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l17:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l17:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l17:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l18
        {mso-list-id:1157457918;
            mso-list-type:hybrid;
            mso-list-template-ids:463088432 635850812 800749590 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l18:level1
        {mso-level-text:"1\.%1";
            mso-level-tab-stop:25.5pt;
            mso-level-number-position:left;
            margin-left:25.5pt;
            text-indent:-25.5pt;
            font-family:"Times New Roman",serif;
            letter-spacing:0pt;}
        @list l18:level2
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%2\)";
            mso-level-tab-stop:45.0pt;
            mso-level-number-position:left;
            margin-left:45.0pt;
            text-indent:-21.0pt;
            font-family:"Times New Roman",serif;
            letter-spacing:0pt;}
        @list l18:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l18:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l18:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l18:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l18:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l18:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l18:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l19
        {mso-list-id:1325355318;
            mso-list-template-ids:167387496;}
        @list l19:level1
        {mso-level-start-at:50;
            mso-level-tab-stop:42.75pt;
            mso-level-number-position:left;
            margin-left:42.75pt;
            text-indent:-42.75pt;}
        @list l19:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;}
        @list l19:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l19:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l19:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l19:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l19:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l19:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l19:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l20
        {mso-list-id:1368262325;
            mso-list-type:hybrid;
            mso-list-template-ids:49814960 -1094159646 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l20:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l20:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l20:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l20:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l20:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l20:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l20:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l20:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l20:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l21
        {mso-list-id:1623684481;
            mso-list-type:hybrid;
            mso-list-template-ids:167387496 -1177635752 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l21:level1
        {mso-level-start-at:50;
            mso-level-tab-stop:42.75pt;
            mso-level-number-position:left;
            margin-left:42.75pt;
            text-indent:-42.75pt;}
        @list l21:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;}
        @list l21:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l21:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l21:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l21:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l21:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l21:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l21:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l22
        {mso-list-id:1682200773;
            mso-list-type:hybrid;
            mso-list-template-ids:-1910503722 -1742016060 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l22:level1
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%1\)";
            mso-level-tab-stop:21.0pt;
            mso-level-number-position:left;
            margin-left:21.0pt;
            text-indent:-21.0pt;
            mso-ansi-font-size:12.0pt;
            mso-bidi-font-size:12.0pt;
            font-family:"Times New Roman",serif;
            letter-spacing:0pt;}
        @list l22:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l22:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l22:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l22:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l22:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l22:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l22:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l22:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l23
        {mso-list-id:1706253734;
            mso-list-type:hybrid;
            mso-list-template-ids:-1071476708 -566567650 1298425440 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l23:level1
        {mso-level-start-at:43;
            mso-level-tab-stop:46.5pt;
            mso-level-number-position:left;
            margin-left:46.5pt;
            text-indent:-46.5pt;
            font-family:"Times New Roman",serif;}
        @list l23:level2
        {mso-level-number-format:alpha-lower;
            mso-level-text:"\(%2\)";
            mso-level-tab-stop:45.0pt;
            mso-level-number-position:left;
            margin-left:45.0pt;
            text-indent:-21.0pt;
            font-family:"Times New Roman",serif;}
        @list l23:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l23:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l23:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l23:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l23:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l23:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l23:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l24
        {mso-list-id:1759206338;
            mso-list-type:hybrid;
            mso-list-template-ids:-813237794 -1094159646 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l24:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l24:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l24:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l24:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l24:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l24:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l24:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l24:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l24:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25
        {mso-list-id:1807623786;
            mso-list-type:hybrid;
            mso-list-template-ids:-1215636462 -1094159646 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l25:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:24.0pt;
            mso-level-number-position:left;
            margin-left:24.0pt;
            text-indent:-24.0pt;
            mso-ansi-font-size:8.0pt;
            font-family:Wingdings;}
        @list l25:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l25:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l26
        {mso-list-id:2033142239;
            mso-list-type:hybrid;
            mso-list-template-ids:895547484 -791274866 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l26:level1
        {mso-level-start-at:4;
            mso-level-tab-stop:42.75pt;
            mso-level-number-position:left;
            margin-left:42.75pt;
            text-indent:-42.75pt;
            font-family:"Times New Roman",serif;
            mso-fareast-font-family:細明體;
            color:windowtext;}
        @list l26:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;}
        @list l26:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l26:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l26:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l26:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l26:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l26:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l26:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l27
        {mso-list-id:2056076343;
            mso-list-type:hybrid;
            mso-list-template-ids:2027610914 390868780 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l27:level1
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:none;
            mso-level-number-position:left;
            mso-level-legacy:yes;
            mso-level-legacy-indent:21.25pt;
            mso-level-legacy-space:0cm;
            margin-left:49.55pt;
            text-indent:-21.25pt;
            mso-ansi-font-size:6.0pt;
            font-family:Wingdings;}
        @list l27:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l27:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l27:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l27:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l27:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l27:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l27:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l27:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l28
        {mso-list-id:2087141534;
            mso-list-type:hybrid;
            mso-list-template-ids:-1883993104 1264342434 67698713 67698715 67698703 67698713 67698715 67698703 67698713 67698715;}
        @list l28:level1
        {mso-level-start-at:4;
            mso-level-tab-stop:18.0pt;
            mso-level-number-position:left;
            margin-left:18.0pt;
            text-indent:-18.0pt;
            font-family:細明體;
            mso-hansi-font-family:"Times New Roman";
            mso-bidi-font-family:"Times New Roman";}
        @list l28:level2
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%2、;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;}
        @list l28:level3
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:right;
            margin-left:72.0pt;
            text-indent:-24.0pt;}
        @list l28:level4
        {mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;}
        @list l28:level5
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%5、;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;}
        @list l28:level6
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:right;
            margin-left:144.0pt;
            text-indent:-24.0pt;}
        @list l28:level7
        {mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;}
        @list l28:level8
        {mso-level-number-format:ideograph-traditional;
            mso-level-text:%8、;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;}
        @list l28:level9
        {mso-level-number-format:roman-lower;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:right;
            margin-left:216.0pt;
            text-indent:-24.0pt;}
        @list l29
        {mso-list-id:2097554029;
            mso-list-type:simple;
            mso-list-template-ids:-668693678;}
        @list l29:level1
        {mso-level-start-at:3;
            mso-level-tab-stop:none;
            mso-level-number-position:left;
            mso-level-legacy:yes;
            mso-level-legacy-indent:54.0pt;
            mso-level-legacy-space:0cm;
            margin-left:54.0pt;
            text-indent:-54.0pt;
            font-family:細明體;}
        @list l30
        {mso-list-id:2117560975;
            mso-list-type:hybrid;
            mso-list-template-ids:-2147029614 2077634484 67698691 67698693 67698689 67698691 67698693 67698689 67698691 67698693;}
        @list l30:level1
        {mso-level-start-at:2;
            mso-level-number-format:bullet;
            mso-level-text:-;
            mso-level-tab-stop:18.0pt;
            mso-level-number-position:left;
            margin-left:18.0pt;
            text-indent:-18.0pt;
            font-family:"Times New Roman",serif;
            mso-fareast-font-family:新細明體;}
        @list l30:level2
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:48.0pt;
            mso-level-number-position:left;
            margin-left:48.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l30:level3
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:72.0pt;
            mso-level-number-position:left;
            margin-left:72.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l30:level4
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:96.0pt;
            mso-level-number-position:left;
            margin-left:96.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l30:level5
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:120.0pt;
            mso-level-number-position:left;
            margin-left:120.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l30:level6
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:144.0pt;
            mso-level-number-position:left;
            margin-left:144.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l30:level7
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:168.0pt;
            mso-level-number-position:left;
            margin-left:168.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l30:level8
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:192.0pt;
            mso-level-number-position:left;
            margin-left:192.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l30:level9
        {mso-level-number-format:bullet;
            mso-level-text:;
            mso-level-tab-stop:216.0pt;
            mso-level-number-position:left;
            margin-left:216.0pt;
            text-indent:-24.0pt;
            font-family:Wingdings;}
        @list l0:level1 lfo1
        {mso-level-start-at:1;
            mso-level-numbering:continue;
            mso-level-text:;
            mso-level-tab-stop:none;
            mso-level-number-position:left;
            mso-level-legacy:yes;
            mso-level-legacy-indent:21.25pt;
            mso-level-legacy-space:0cm;
            margin-left:49.55pt;
            text-indent:-21.25pt;
            mso-ansi-font-size:6.0pt;
            font-family:Wingdings;}
        ol
        {margin-bottom:0cm;}
        ul
        {margin-bottom:0cm;}
        -->
    </style>
    <!--[if gte mso 10]>
    <style>
        /* Style Definitions */
        table.MsoNormalTable
        {mso-style-name:表格內文;
            mso-tstyle-rowband-size:0;
            mso-tstyle-colband-size:0;
            mso-style-noshow:yes;
            mso-style-priority:99;
            mso-style-parent:"";
            mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
            mso-para-margin:0cm;
            mso-para-margin-bottom:.0001pt;
            mso-pagination:widow-orphan;
            font-size:10.0pt;
            font-family:"Times New Roman",serif;}
        table.MsoTableGrid
        {mso-style-name:表格格線;
            mso-tstyle-rowband-size:0;
            mso-tstyle-colband-size:0;
            mso-style-unhide:no;
            border:solid windowtext 1.0pt;
            mso-border-alt:solid windowtext .5pt;
            mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
            mso-border-insideh:.5pt solid windowtext;
            mso-border-insidev:.5pt solid windowtext;
            mso-para-margin:0cm;
            mso-para-margin-bottom:.0001pt;
            line-height:150%;
            mso-pagination:none;
            mso-layout-grid-align:none;
            punctuation-wrap:simple;
            text-autospace:none;
            font-size:10.0pt;
            font-family:"Times New Roman",serif;}
    </style>
    <![endif]--><!--[if gte mso 9]><xml>
        <o:shapedefaults v:ext="edit" spidmax="2049"/>
    </xml><![endif]--><!--[if gte mso 9]><xml>
        <o:shapelayout v:ext="edit">
            <o:idmap v:ext="edit" data="1"/>
        </o:shapelayout></xml><![endif]-->
</head>

<body lang=ZH-TW link=blue vlink=purple style='tab-interval:24.1pt'>

<div class=WordSection1>

    <p class=MsoNormal align=right style='text-align:right;tab-stops:9.0cm right 446.55pt'><b
                style='mso-bidi-font-weight:normal'><span style='mso-ascii-font-family:"Times New Roman";
letter-spacing:1.5pt'>附件</span></b><b style='mso-bidi-font-weight:normal'><span
                    lang=EN-US style='font-family:"Times New Roman",serif'>II<span
                        style='letter-spacing:1.5pt'><o:p></o:p></span></span></b></p>

    <p class=MsoNormal align=center style='text-align:center;tab-stops:9.0cm right 446.55pt'><span
                style='font-size:11.0pt;line-height:150%;mso-hansi-font-family:細明體;letter-spacing:
1.5pt'>（只供學校參考之用）</span><b style='mso-bidi-font-weight:normal'><span
                    lang=EN-US style='font-family:"Times New Roman",serif;letter-spacing:1.5pt'><o:p></o:p></span></b></p>

    <p class=MsoNormal style='line-height:12.0pt;tab-stops:9.0cm right 446.55pt'><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>學校檔號：<span lang=EN-US><span
                        style='mso-tab-count:1'>                         </span><br>
</span>致：<span lang=EN-US>*</span>校長<span lang=EN-US>/</span>副校長<span
                    lang=EN-US><o:p></o:p></span></span></p>

    <p class=MsoNormal align=center style='margin-top:12.0pt;text-align:center;
line-height:15.0pt;mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><b
                style='mso-bidi-font-weight:normal'><span style='mso-bidi-font-size:12.0pt;
mso-hansi-font-family:細明體;letter-spacing:1.5pt'>按口頭報價購貨表格<span lang=EN-US><o:p></o:p></span></span></b></p>

    <p class=MsoNormal align=center style='margin-bottom:6.0pt;mso-para-margin-bottom:
.5gd;text-align:center;line-height:15.0pt;mso-line-height-rule:exactly;
tab-stops:9.0cm right 446.55pt'><span lang=EN-US style='font-size:11.0pt;
mso-hansi-font-family:細明體;letter-spacing:1.5pt'><span
                    style='mso-spacerun:yes'> </span><o:p></o:p></span></p>

    <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><span
                    style='mso-tab-count:1'>     </span></span><span style='font-size:11.0pt;
letter-spacing:1.5pt'>本校邀請下列供應商，提供物料或服務的口頭報價（</span><span lang=EN-US
                                                          style='font-size:11.0pt;font-family:"Times New Roman",serif;mso-fareast-font-family:
新細明體'>5,000</span><span style='font-size:11.0pt;mso-hansi-font-family:細明體;
letter-spacing:1.0pt'>元以上至</span><span lang=EN-US style='font-size:11.0pt;
font-family:"Times New Roman",serif;mso-fareast-font-family:新細明體'>50,000</span><span
                style='font-size:11.0pt;mso-hansi-font-family:細明體;letter-spacing:1.5pt'>元</span><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>），</span><span style='font-size:
11.0pt;mso-ascii-font-family:"Times New Roman";letter-spacing:1.5pt'>就</span><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>供應商所提供的物料或服務的價錢和質素與市場的供應情況作出適當的比較後，現推薦採納由<u><span
                        lang=EN-US><span style='mso-spacerun:yes'><?=$resultExportAry['SupplierName'] ?> </span></span></u><span
                    lang=EN-US><span style='mso-spacerun:yes'> </span></span>提出的<span lang=EN-US>*</span>較低報價<span
                    lang=EN-US>/</span>較高報價。不採納最低報價的原因是：<u><?= (isset($resultExportAry['NotCheapestReason'])&&$resultExportAry['NotCheapestReason']!='')? $postVar['resultForExportAry']['NotCheapestReason'] : "<span lang=EN-US><span
                            style='mso-tab-count:2'>            </span><span
                            style='mso-spacerun:yes'>  </span><span style='mso-tab-count:1'> </span><span
                            style='mso-spacerun:yes'> </span></span></u>。<u><span lang=EN-US><span
                            style='mso-spacerun:yes'>                                                                            
</span><span style='mso-spacerun:yes'>     </span><span
                            style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'> </span><span
                            style='mso-spacerun:yes'>                       </span><span
                            style='mso-spacerun:yes'>     </span><span
                            style='mso-spacerun:yes'>         </span><span
                            style='mso-spacerun:yes'> </span></span></u><span lang=EN-US><span
                        style='mso-spacerun:yes'> </span><u><span
                            style='mso-spacerun:yes'>             </span>" ?></u><o:p></o:p></span></span></p>

    <p class=MsoNormal style='line-height:9.0pt;mso-line-height-rule:exactly;
tab-stops:right 248.1pt left 403.25pt'><span lang=EN-US style='font-size:11.0pt;
letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=616
           style='width:462.1pt;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .75pt;
 mso-padding-alt:0cm 1.4pt 0cm 1.4pt;mso-border-insideh:.75pt solid windowtext;
 mso-border-insidev:.75pt solid windowtext'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=49 valign=top style='width:36.85pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
  mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>項目<span lang=EN-US><br>
  </span>編號<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=95 valign=top style='width:70.9pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
  solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
  mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>物品說明<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=57 valign=top style='width:42.5pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
  solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
  mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>所需 數量<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=204 valign=top style='width:153.05pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
  solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
  mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>供應商名稱及電話號碼<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=51 valign=top style='width:38.3pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
  solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
  mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>單價<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=76 valign=top style='width:2.0cm;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
  solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                <p class=MsoNormal align=center style='text-align:center;line-height:17.0pt;
  mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>總價<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=85 valign=top style='width:63.8pt;border:solid windowtext 1.0pt;
  border-left:none;mso-border-left-alt:solid windowtext .75pt;mso-border-alt:
  solid windowtext .75pt;padding:0cm 1.4pt 0cm 1.4pt'>
                <p class=MsoNormal align=center style='margin-bottom:6.0pt;mso-para-margin-bottom:
  .5gd;text-align:center;line-height:17.0pt;mso-line-height-rule:exactly;
  tab-stops:9.0cm right 446.55pt'><span style='font-size:11.0pt;letter-spacing:
  1.5pt'>採納出價「</span><span lang=EN-US style='font-size:11.0pt;font-family:Wingdings;
  mso-ascii-font-family:細明體;mso-hansi-font-family:"Times New Roman";letter-spacing:
  1.5pt;mso-char-type:symbol;mso-symbol-font-family:Wingdings'><span
                                style='mso-char-type:symbol;mso-symbol-font-family:Wingdings'>ü</span></span><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>」<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
        </tr>
        <?php $quotationItemNo = 0;
        foreach((array)$QuotationsAry as $quotation){
            $quotationItemNo += 1; ?>
            <tr style='mso-yfti-irow:1;height:19.5pt'>
                <td width=49 valign=top style='width:36.85pt;border:solid windowtext 1.0pt;
      border-top:none;mso-border-top-alt:solid windowtext .75pt;mso-border-alt:
      solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                    <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
      margin-bottom:3.0pt;margin-left:0cm;text-align:center;line-height:18.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p><?= isset($quotation['SupplierName'])?$quotationItemNo : '&nbsp;'; ?></o:p></span></p>
                </td>
                <td width=95 valign=top style='width:70.9pt;border-top:none;border-left:none;
      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
      mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
      mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                    <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
      margin-bottom:3.0pt;margin-left:0cm;text-align:center;line-height:18.0pt;
      mso-line-height-rule:exactly;tab-stops:9.0cm right 446.55pt'><span
                                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p><?=isset($quotation['SupplierName'])?$postVar['nameEdit']:''; ?></o:p><br><o:p><?= isset($quotation['SupplierName'])?$postVar['description']:''; ?></o:p></span></p>
                </td>
                <td width=57 valign=top style='width:42.5pt;border-top:none;border-left:none;
      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
      mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
      mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                    <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
      margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                lang=EN-US style='font-size:11.0pt;line-height:150%;letter-spacing:1.5pt'><o:p><?= $quotation['Quantity'] ?></o:p></span></p>
                </td>
                <td width=204 style='width:153.05pt;border-top:none;border-left:none;
      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
      mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
      mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                    <p class=MsoNormal style='margin-top:3.0pt;margin-right:0cm;margin-bottom:
      3.0pt;margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
      tab-stops:9.0cm right 446.55pt'><span lang=EN-US style='font-size:11.0pt;
      line-height:150%;font-family:"Times New Roman",serif'>(<?= $quotationItemNo; ?>)<o:p><?= $quotation['SupplierName']; ?></o:p><br><o:p><?= $quotation['Phone']; ?></o:p></span></p>
                </td>
                <td width=51 valign=top style='width:38.3pt;border-top:none;border-left:none;
      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
      mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
      mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                    <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
      margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                lang=EN-US style='font-size:11.0pt;line-height:150%;letter-spacing:1.5pt'><o:p><?= $quotation['Accepted']?$resultExportAry['Price']:$quotation['Price']; ?></o:p></span></p>
                </td>
                <td width=76 valign=top style='width:2.0cm;border-top:none;border-left:none;
      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
      mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
      mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                    <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
      margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                lang=EN-US style='font-size:11.0pt;line-height:150%;letter-spacing:1.5pt'><o:p><?= $quotation['Accepted']?$resultExportAry['Price']:$quotation['Price']; ?></o:p></span></p>
                </td>
                <td width=85 valign=top style='width:63.8pt;border-top:none;border-left:none;
      border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
      mso-border-top-alt:solid windowtext .75pt;mso-border-left-alt:solid windowtext .75pt;
      mso-border-alt:solid windowtext .75pt;mso-border-bottom-alt:solid windowtext .5pt;
      padding:0cm 1.4pt 0cm 1.4pt;height:19.5pt'>
                    <p class=MsoNormal align=center style='margin-top:3.0pt;margin-right:0cm;
      margin-bottom:3.0pt;margin-left:0cm;text-align:center;tab-stops:9.0cm right 446.55pt'><span
                                lang=EN-US style='font-size:11.0pt;line-height:150%;letter-spacing:1.5pt'><o:p><?= $quotation['Accepted']?'&#10003;':''; ?></o:p></span></p>
                </td>
            </tr>
        <?php } ?>
    </table>

    <p class=MsoNormal style='margin-top:12.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:18.0pt;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>如邀請<span lang=EN-US>/</span>收到少於兩名供應商報價，請在方格內加以說明：<span
                    lang=EN-US><o:p></o:p></span></span></p>

    <table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 width=621
           style='width:466.1pt;border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-yfti-tbllook:480;mso-padding-alt:0cm 5.4pt 0cm 5.4pt;mso-border-insideh:
 .5pt solid windowtext;mso-border-insidev:.5pt solid windowtext'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes'>
            <td width=621 valign=top style='width:466.1pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  line-height:17.0pt;mso-line-height-rule:exactly;tab-stops:right 248.1pt left 403.25pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>邀請<span lang=EN-US>/</span>收到少於兩名供應商報價的原因是：<u><?= isset($postVar['supplierLessThenReason'])? $postVar['supplierLessThenReason'] :"<span
                                    lang=EN-US><span style='mso-tab-count:2'>                       </span><span
                                        style='mso-spacerun:yes'>  </span><span style='mso-tab-count:1'>  </span><span
                                        style='mso-spacerun:yes'> </span></span>" ?></u>。<span lang=EN-US><span
                                    style='mso-spacerun:yes'>  </span><o:p></o:p></span></span></p>
                <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:
  6.0pt;margin-left:0cm;mso-para-margin-top:.5gd;mso-para-margin-right:0cm;
  mso-para-margin-bottom:.5gd;mso-para-margin-left:0cm;text-indent:156.25pt;
  mso-char-indent-count:12.5;mso-line-height-alt:0pt;tab-stops:right 248.1pt left 294.0pt 403.25pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>批簽：<u><span lang=EN-US><span
                                        style='mso-spacerun:yes'>          </span></span></u><span lang=EN-US>(</span>職級<u><span
                                    lang=EN-US><span style='mso-spacerun:yes'>        </span></span></u><span
                                lang=EN-US>/</span>職位<u><span lang=EN-US><span
                                        style='mso-spacerun:yes'>        </span></span></u><span lang=EN-US>)<o:p></o:p></span></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-top:12.0pt;text-align:justify;text-justify:
inter-ideograph;line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><span
                    style='mso-tab-count:1'>     </span></span><span style='font-size:11.0pt;
letter-spacing:1.5pt'>由於上述物品須供應<u><span lang=EN-US><span
                            style='mso-spacerun:yes'>               </span><span
                            style='mso-spacerun:yes'>          </span><span
                            style='mso-spacerun:yes'>    </span></span></u>使用，因此，如獲批准，本人會要求供應商在<u><span
                        lang=EN-US><span style='mso-spacerun:yes'>  </span><span
                            style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'> </span><span
                            style='mso-spacerun:yes'>  </span><span style='mso-spacerun:yes'>   </span></span></u>年<u><span
                        lang=EN-US><span style='mso-spacerun:yes'>   </span><span
                            style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'>  </span></span></u>月<u><span
                        lang=EN-US><span style='mso-spacerun:yes'>     </span></span></u>日或之前送貨。物品驗收無誤後，即可在<u><span
                        lang=EN-US><span style='mso-spacerun:yes'>    </span><span
                            style='mso-spacerun:yes'>   </span><span style='mso-spacerun:yes'>  </span></span></u>年<u><span
                        lang=EN-US><span style='mso-spacerun:yes'>    </span><span
                            style='mso-spacerun:yes'> </span><span style='mso-spacerun:yes'> </span></span></u>月<u><span
                        lang=EN-US><span style='mso-spacerun:yes'>     </span></span></u>日付款。<span
                    lang=EN-US><o:p></o:p></span></span></p>

    <p class=MsoNormal style='margin-top:6.0pt;margin-right:0cm;margin-bottom:6.0pt;
margin-left:0cm;mso-para-margin-top:.5gd;mso-para-margin-right:0cm;mso-para-margin-bottom:
.5gd;mso-para-margin-left:0cm;text-align:justify;text-justify:inter-ideograph;
line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><span
                    style='mso-tab-count:1'>     </span></span><span style='font-size:11.0pt;
letter-spacing:1.5pt'>特此聲明，上述物品只屬單一的採購。校方並非把多個部分所組成的物品分單採購。<span lang=EN-US><o:p></o:p></span></span></p>

    <table class=MsoNormalTable border=0 cellspacing=0 cellpadding=0 width=625
           style='width:468.6pt;border-collapse:collapse;mso-yfti-tbllook:480;mso-padding-alt:
 0cm 5.4pt 0cm 5.4pt'>
        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
            <td width=64 valign=top style='width:47.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
  mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
  0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
  line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:0cm right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>報價由<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=19 valign=top style='width:14.15pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;margin-right:-9.5pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
  mso-para-margin-right:-.79gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
  0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
  line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:0cm right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=158 valign=top style='width:118.35pt;border:none;border-bottom:
  solid windowtext 1.0pt;mso-border-bottom-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
            </td>
            <td width=97 valign=top style='width:73.05pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>邀請<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=47 valign=top style='width:35.4pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
  mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
  0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
  line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>簽署<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=221 valign=top style='width:165.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
            </td>
        </tr>
        <tr style='mso-yfti-irow:1;mso-yfti-lastrow:yes'>
            <td width=64 valign=top style='width:47.95pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
  mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
  0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
  line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>職級 <span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=19 valign=top style='width:14.15pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;margin-right:-9.5pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
  mso-para-margin-right:-.79gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
  0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
  line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=158 valign=top style='width:118.35pt;border:none;border-bottom:
  solid windowtext 1.0pt;mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
            </td>
            <td width=97 valign=top style='width:73.05pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
            </td>
            <td width=47 valign=top style='width:35.4pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;margin-right:-5.4pt;margin-bottom:
  0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:.5gd;
  mso-para-margin-right:-.45gd;mso-para-margin-bottom:0cm;mso-para-margin-left:
  0cm;mso-para-margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;
  line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>日期<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=19 valign=top style='width:14.2pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            style='font-size:11.0pt;letter-spacing:1.5pt'>：<span lang=EN-US><o:p></o:p></span></span></p>
            </td>
            <td width=221 valign=top style='width:165.5pt;border:none;border-bottom:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt'>
                <p class=MsoNormal style='margin-top:6.0pt;mso-para-margin-top:.5gd;
  text-align:justify;text-justify:inter-ideograph;line-height:16.0pt;
  mso-line-height-rule:exactly;tab-stops:35.45pt right 248.1pt left 389.85pt'><span
                            lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'><o:p>&nbsp;</o:p></span></p>
            </td>
        </tr>
    </table>

    <p class=MsoNormal style='margin-top:12.0pt;margin-right:-.05pt;margin-bottom:
0cm;margin-left:0cm;margin-bottom:.0001pt;mso-para-margin-top:1.0gd;mso-para-margin-right:
-.05pt;mso-para-margin-bottom:0cm;mso-para-margin-left:0cm;mso-para-margin-bottom:
.0001pt;text-align:justify;text-justify:inter-ideograph;line-height:9.0pt;
mso-line-height-rule:exactly;tab-stops:35.45pt'><span lang=EN-US
                                                      style='font-size:11.0pt;letter-spacing:1.5pt'>------------------------------------------------------------<o:p></o:p></span></p>

    <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt'><span
                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'>*</span><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>本人證實，口頭報價的程序妥當及對於上述推薦給予批准；或<span
                    lang=EN-US><o:p></o:p></span></span></p>

    <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt'><span
                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'>*</span><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>本人認為須重新邀請口頭報價；或<span lang=EN-US><o:p></o:p></span></span></p>

    <p class=MsoNormal style='text-align:justify;text-justify:inter-ideograph;
line-height:16.0pt;mso-line-height-rule:exactly;tab-stops:35.45pt'><span
                lang=EN-US style='font-size:11.0pt;letter-spacing:1.5pt'>*</span><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>本人不批准上述推薦，原因是<u><span lang=EN-US><span
                            style='mso-spacerun:yes'>               </span><span
                            style='mso-spacerun:yes'>          </span><span
                            style='mso-spacerun:yes'>    </span></span>。</u><span lang=EN-US><o:p></o:p></span></span></p>

    <p class=MsoNormal align=right style='margin-top:24.0pt;margin-right:0cm;
margin-bottom:0cm;margin-left:354.4pt;margin-bottom:.0001pt;mso-para-margin-top:
2.0gd;mso-para-margin-right:0cm;mso-para-margin-bottom:0cm;mso-para-margin-left:
354.4pt;mso-para-margin-bottom:.0001pt;text-align:right;text-indent:-4.0cm;
mso-line-height-alt:0pt;tab-stops:212.65pt 9.0cm;word-break:break-all'><span
                style='font-size:11.0pt;letter-spacing:1.5pt'>簽署：<u><span lang=EN-US><span
                            style='mso-spacerun:yes'>                 </span><o:p></o:p></span></u></span></p>

    <p class=MsoNormal align=right style='margin-left:354.4pt;text-align:right;
text-indent:-99.25pt;tab-stops:212.65pt 269.35pt;word-break:break-all'><span
                lang=EN-US style='font-size:11.0pt;line-height:150%;letter-spacing:1.5pt'><span
                    style='mso-spacerun:yes'> </span>*</span><span style='font-size:11.0pt;
line-height:150%;letter-spacing:1.5pt'>校長<span lang=EN-US>/</span>副校長<span
                    lang=EN-US><span style='mso-spacerun:yes'>   </span><o:p></o:p></span></span></p>

    <p class=MsoNormal align=right style='margin-left:9.0cm;text-align:right;
line-height:15.0pt;mso-line-height-rule:exactly;tab-stops:212.65pt 269.35pt 318.95pt;
word-break:break-all'><span style='font-size:11.0pt;letter-spacing:1.5pt'>日期：<u><span
                        lang=EN-US><span style='mso-tab-count:1'>   </span><span
                            style='mso-spacerun:yes'>             </span></span></u><span lang=EN-US><o:p></o:p></span></span></p>

    <p class=MsoNormal style='line-height:14.0pt;mso-line-height-rule:exactly;
tab-stops:212.65pt 269.35pt 318.95pt'><span lang=EN-US style='font-size:11.0pt;
letter-spacing:1.5pt'>*</span><span style='font-size:11.0pt;letter-spacing:
1.5pt'>請刪去不適用者<span lang=EN-US><o:p></o:p></span></span></p>

</div>

</body>

</html>

