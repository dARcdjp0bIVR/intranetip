<?php
/*
##############################
##	Date	2016-10-24	Omas
##	Details	Improved loop to show Group without item
##
##############################
*/
$PATH_WRT_ROOT=$_PAGE['PATH_WRT_ROOT'];
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

function print_report_start(&$pdf,$academic_year,$group_name){
	global $Lang;
	
	$pdf->WriteHTML('        <div class="title">'.$academic_year.$Lang['ePCM']['Report']['BER']['AcademicYear'].'</div>');
	$pdf->WriteHTML('        <div class="title">'.$Lang['ePCM']['Report']['BER']['Budget and Expenses Report'].'</div>');
	$pdf->WriteHTML('        <div class="title">'.$group_name.'</div>');
	$pdf->WriteHTML('        <table class="data">');
	$pdf->WriteHTML('            <thead>');
	$pdf->WriteHTML('                <tr>');
	$pdf->WriteHTML('                    <th style="width:25%;">'.$Lang['ePCM']['Report']['BER']['Item'].'</th>');
	$pdf->WriteHTML('                    <th style="width:25%;">'.$academic_year.'<br/>'.$Lang['ePCM']['Report']['BER']['Budget'].'<br/>'.$Lang['ePCM']['Report']['BER']['Dollar'].'</th>');
	$pdf->WriteHTML('                    <th style="width:25%;">'.$academic_year.'<br/>'.$Lang['ePCM']['Report']['BER']['Deal Price'].'<br/>'.$Lang['ePCM']['Report']['BER']['Dollar'].'</th>');
	$pdf->WriteHTML('                    <th style="width:25%;">'.$academic_year.'<br/>'.$Lang['ePCM']['Report']['BER']['Difference'].'<br/>'.$Lang['ePCM']['Report']['BER']['Dollar'].'</th>');
	$pdf->WriteHTML('                </tr>');
	$pdf->WriteHTML('                <tr>');
	$pdf->WriteHTML('                    <th colspan="4"><hr style="margin:0px;" /></th>');
	$pdf->WriteHTML('                </tr>');
	$pdf->WriteHTML('            </thead>');
	$pdf->WriteHTML('            <tbody>');
}

function print_report_end(&$pdf,$sum_budget=-1,$sum_deal_price=-1){
	global $Lang;
	
	if($sum_budget!=-1 && $sum_deal_price!=-1){
		$pdf->WriteHTML('                <tr>');
		$pdf->WriteHTML('                    <td colspan="4"><hr style="margin:0px;" /></td>');
		$pdf->WriteHTML('                </tr>');
		$pdf->WriteHTML('                <tr>');
		$pdf->WriteHTML('                    <td>'.$Lang['ePCM']['Report']['BER']['Sum'].'</td>');
		$pdf->WriteHTML('                    <td><u>'.account_number_format($sum_budget,2).'</u></td>');
		$pdf->WriteHTML('                    <td><u>'.account_number_format($sum_deal_price,2).'</u></td>');
		$pdf->WriteHTML('                    <td><u>'.account_number_format($sum_budget-$sum_deal_price,2).'</u></td>');
		$pdf->WriteHTML('                </tr>');	
	}
	$pdf->WriteHTML('            </tbody>');
	$pdf->WriteHTML('        </table>');
}

function print_page_break(&$pdf){
	$pdf->WriteHTML('        <pagebreak />');
}

function account_number_format($value,$dp=2){
	if($value<0){
		return '('.number_format($value,$dp).')';
	}else{
		return number_format($value,$dp);
	}
}

$rows_item=$_PAGE['rows_item'];

$year_from=substr($_PAGE['data']['date_from'],0,4);
$year_to=substr($_PAGE['data']['date_to'],0,4);
$month_day_from=substr($_PAGE['data']['date_from'],4,6);
$month_day_to=substr($_PAGE['data']['date_to'],4,6);
if($month_day_from<'-09-01'){
	$year_from--;
}
if($month_day_to>='-09-01'){
	$year_to++;
}
if($_PAGE['data']['academic_year']!=''){
	$academic_year=date('Y',getStartOfAcademicYear('',$_PAGE['data']['academic_year'])).' - '.date('Y',getEndOfAcademicYear('',$_PAGE['data']['academic_year']));
}else{
	$academic_year=$year_from.' - '.$year_to;
}

//start creating PDF
//new mPDF ($mode,$format,$default_font_size,$default_font,$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation)
$margin_left = 10; //mm
$margin_right = 10;
$margin_top = 10;
$margin_bottom = 10;
$margin_header = 0;
$margin_footer = 5;
$orientation='P';
$pdf = new mPDF('','A4',0,'',$margin_left,$margin_right,$margin_top,$margin_bottom,$margin_header,$margin_footer,$orientation);
$pdf->setHTMLFooter('<div style="width:100%;text-align:center;">{PAGENO} / {nb}</div>');
$pdf->AddPage($orientation);

// Chinese use mingliu 
$pdf->backupSubsFont = array('mingliu');
$pdf->useSubstitutions = true;

//$pdf->WriteHTML('body{font-family:serif;}');
$pdf->WriteHTML('.title{font-size:24px;margin:auto;width:100%;text-align:center;font-weight:bold;}',1);
$pdf->WriteHTML('table.data{border-collapse:collapse;border:1px solid #000000;margin-top:30px;table-layout:fixed;width:100%;}',1);
$pdf->WriteHTML('table.data th{border:0px solid #000000;padding:10px;font-size:14px;text-align:center}',1);
$pdf->WriteHTML('table.data td{border:0px solid #000000;padding:10px;font-size:14px;text-align:center}',1);

$pdf->WriteHTML('<html>');
$pdf->WriteHTML('    <head>');
$pdf->WriteHTML('    </head>');
$pdf->WriteHTML('    <body>');

$previous_group_id=-1;
$sum_budget = 0;
$sum_deal_price = 0;

$GroupNameField = Get_Lang_Selection('GroupNameChi','GroupName');
$GroupNameAssoArr =  BuildMultiKeyAssoc($rows_item, 'GroupID',array($GroupNameField), 1);

$rows_item_assoArr = BuildMultiKeyAssoc($rows_item, 'GroupID',array(), 0,1);
$numGroup = count($rows_item_assoArr);
$count = 0;
foreach((array)$rows_item_assoArr as $groupID => $groupRows_item){
	$count ++;

	// loop groups
	print_report_start($pdf,$academic_year,$GroupNameAssoArr[$groupID]);
	
	$sum_budget = 0;
	$sum_deal_price = 0;
	foreach((array)$groupRows_item as $row_item){
		// loop group's items 
		if($row_item['ItemID'] != ''){
			$pdf->WriteHTML('                <tr>');
			$pdf->WriteHTML('                    <td>'.$row_item[Get_Lang_Selection('ItemNameChi','ItemName')].'</td>');
			$pdf->WriteHTML('                    <td>'.account_number_format($row_item['ItemBudget'],2).'</td>');
			$pdf->WriteHTML('                    <td>'.account_number_format($row_item['ItemExpense'],2).'</td>');
			$pdf->WriteHTML('                    <td>'.account_number_format($row_item['ItemBudget']-$row_item['ItemExpense'],2).'</td>');
			$pdf->WriteHTML('                </tr>');
			$sum_budget += $row_item['ItemBudget'];
			$sum_deal_price += $row_item['ItemExpense'];
		}else{
			$pdf->WriteHTML('                <tr>');
			$pdf->WriteHTML('                	<td colspan="4" align="center">'.$Lang['ePCM']['Report']['BER']['No Items'].'</td>');
			$pdf->WriteHTML('                </tr>');
		}
	}
	print_report_end($pdf,$sum_budget,$sum_deal_price);
	if($count < $numGroup ){
		print_page_break($pdf);
	}
}

// foreach((array)$rows_item as $row_item){
	
// 	if($row_item['ItemID'] != ''){
// 		if($previous_group_id != $row_item['GroupID']){
// 			if($previous_group_id!=-1){
// 				print_report_end($pdf,$sum_budget,$sum_deal_price);
// 				print_page_break($pdf);
// 			}
	
// 			print_report_start($pdf,$academic_year,$row_item[Get_Lang_Selection('GroupNameChi','GroupName')]);
// 			$sum_budget = 0;
// 			$sum_deal_price = 0;
// 		}
// 		$pdf->WriteHTML('                <tr>');
// 		$pdf->WriteHTML('                    <td>'.$row_item[Get_Lang_Selection('ItemNameChi','ItemName')].'</td>');
// 		$pdf->WriteHTML('                    <td>'.account_number_format($row_item['Budget'],2).'</td>');
// 		$pdf->WriteHTML('                    <td>'.account_number_format($row_item['DealPrice'],2).'</td>');
// 		$pdf->WriteHTML('                    <td>'.account_number_format($row_item['Budget']-$row_item['DealPrice'],2).'</td>');
// 		$pdf->WriteHTML('                </tr>');
// 		$sum_budget += $row_item['Budget'];
// 		$sum_deal_price += $row_item['DealPrice'];
		
// 		$previous_group_id = $row_item['GroupID'];
// 	}
// 	else{
// 	}
// }


// if(count($rows_item)<1){
// 	print_report_start($pdf,$academic_year,'');
// 	print_report_end($pdf);
// 	$pdf->WriteHTML('        '.$Lang['ePCM']['Report']['BER']['No Items']);
// }else{
// 	print_report_end($pdf,$sum_budget,$sum_deal_price);
// }
$pdf->WriteHTML('    </body>');
$pdf->WriteHTML('</html>');
	
$pdf->Output('Budget_Expenses_Report_'.date('YmdHis').'.pdf', 'I');

?>