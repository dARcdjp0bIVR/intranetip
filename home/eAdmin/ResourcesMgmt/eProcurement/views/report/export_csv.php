<?php 
function account_number_format($value,$dp=2){
	if($value<0){
		return '('.number_format($value,$dp).')';
	}else{
		return number_format($value,$dp);
	}
}
$rawData = $_PAGE['rows_item'];
$row = 0;
#patching csv data
switch($_PAGE['reportType']){
	case 'pcmrs':
		$csvHeader = array(
				$Lang['ePCM']['Report']['FCS']['Code'],
				$Lang['ePCM']['Mgmt']['Case']['CaseName2'],
				$Lang['ePCM']['Mgmt']['Case']['FinancialItem'],
				$Lang['ePCM']['Report']['FCS']['Applicant'],
				$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'],
				$Lang['ePCM']['Group']['Budget'],
				$Lang['ePCM']['Mgmt']['Case']['Result']['DealPrice'],
				$Lang['ePCM']['Mgmt']['Case']['Result']['DealDate'],
				$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier']
		);
		#content Start
		foreach((array)$rawData as $row=>$_rawData){
			foreach ((array)$_rawData as $key=>$__rawData){
				$_rawData[$key] = str_replace('<br>', ' ', $__rawData);
			}
			$exportArr[$row][] = Get_String_Display($_rawData['Code']);
			$exportArr[$row][] = $_rawData['CaseName'];
			$exportArr[$row][] = Get_Lang_Selection($_rawData['ItemNameChi'], $_rawData['ItemName']);
			if($_rawData['ApplicantType']=='I'){
				$applicant = $_rawData['ApplicantUserName'];
			}elseif($_rawData['ApplicantType']=='G'){
				$applicant = $_rawData[Get_Lang_Selection('GroupNameChi','GroupName')].' ('.$_rawData['ApplicantUserName'].')';
			}
			$exportArr[$row][] = $applicant;
			$exportArr[$row][] = date('Y-m-d',strtotime($_rawData['DateInput']));
			$exportArr[$row][] = $_rawData['Budget'];
			$exportArr[$row][] = Get_String_Display($_rawData['DealPrice']);
			$exportArr[$row][] = Get_String_Display($_rawData['DealDate']);
			$exportArr[$row][] = Get_String_Display(Get_Lang_Selection( $_rawData['SupplierNameChi'], $_rawData['SupplierName']) );
		}
		#content End
		$filename = 'Procurement_Record_Summary_'.date('YmdHis').'.csv';
		break;
	case 'fcs':
		foreach ((array)$rawData as $_rawData){
			$dataArr[$_rawData['rule_range']][] = $_rawData;
		}
		foreach ((array)$dataArr as $ruleRange => $_dataArr){
			$isFirst = true;
			foreach ((array)$_dataArr as $caseData){
				foreach ((array)$caseData as $key=>$_caseData){
					$caseData[$key] = str_replace('<br>', ' ', $_caseData);
				}
				if($isFirst){
					#rule_range
					$rule_range = '$'.str_replace('-', '-$', $caseData['rule_range']);
					$exportArr[$row][] = $rule_range;
					$row++;
					#Header Printing
					$exportArr[$row][] = $Lang['ePCM']['Report']['FCS']['Application Date'];
					$exportArr[$row][] = $Lang['ePCM']['Report']['FCS']['Code'];
					$exportArr[$row][] = $Lang['ePCM']['Report']['FCS']['Name'];
					if($caseData['QuotationType']!='N'){
						$exportArr[$row][] = ($caseData['quotation_type']=='T'?$Lang['ePCM']['Report']['FCS']['TStart']:$Lang['ePCM']['Report']['FCS']['QStart']);
						$exportArr[$row][] = ($caseData['quotation_type']=='T'?$Lang['ePCM']['Report']['FCS']['TEnd']:$Lang['ePCM']['Report']['FCS']['QEnd']);
					}
					$exportArr[$row][] = $Lang['ePCM']['Report']['FCS']['Applicant'];
					$exportArr[$row][] = $Lang['ePCM']['Report']['FCS']['Remarks'];
					$row++;
					$isFirst = false;
				}
				
				//content
				$exportArr[$row][] = date('Y-m-d',strtotime($caseData['DateInput']));
				$exportArr[$row][] = $caseData['Code'];
				$exportArr[$row][] = $caseData['CaseName'];
				if($caseData['QuotationType']!='N'){
					$startDate = ($caseData['QuotationType']=='T'?$caseData['TenderOpeningStartDate']:$caseData['QuotationStartDate']);
					$exportArr[$row][] =  date('Y-m-d',strtotime($startDate));
					$endDate = ($caseData['QuotationType']=='T'?$caseData['TenderOpeningEndDate']:$caseData['QuotationEndDate']);
					$exportArr[$row][] = date('Y-m-d',strtotime($endDate));
				}
				if($caseData['ApplicantType']=='I'){
					$applicant = $caseData['ApplicantUserName'];
				}elseif($caseData['ApplicantType']=='G'){
					$applicant = $caseData[Get_Lang_Selection('GroupNameChi','GroupName')].'&nbsp;('.$caseData['ApplicantUserName'].')';
				}
				
				$exportArr[$row][] = $applicant;
				
				$remarks = '';
				if($caseData['rejected']=='1'){
					$remarks .= $Lang['ePCM']['Report']['FCS']['Rejected'];
				}else{
					if($caseData['CurrentStage']=='10'){
						$remarks .= $Lang['ePCM']['Report']['FCS']['Completed'];
					}else{
						$remarks .= $Lang['ePCM']['Report']['FCS']['Application in progress'];
					}
				}
				$exportArr[$row][] = $remarks;
				$row++;
			}
		}
		$filename = 'File_Code_Summary_'.date('YmdHis').'.csv';
		break;
	case'ber' :
		foreach ((array)$rawData as $_rawData){
			$dataArr[$_rawData['GroupID']][] = $_rawData;
		}
		foreach ((array)$dataArr as $GroupID => $_dataArr){
			//for each group
			$isFirst = true;
			unset($sumOfBudget);
			unset($sumOfExpense);
			unset($sumOfDiff);
			foreach ((array)$_dataArr as $key=>$itemData){
				foreach ((array)$itemData as $key=>$_itemData){
					$itemData[$key] = str_replace('<br>', ' ', $_itemData);
				}
				//for each item
				//first item
				if($isFirst){
					$exportArr[$row][]  = Get_Lang_Selection($itemData['GroupNameChi'], $itemData['GroupName']);
					$row++;
					$exportArr[$row][] = $Lang['ePCM']['Report']['BER']['Item'];
					$exportArr[$row][] = $Lang['ePCM']['Report']['BER']['Budget'].'('.$Lang['ePCM']['Report']['BER']['Dollar'].')';
					$exportArr[$row][] = $Lang['ePCM']['Report']['BER']['Deal Price'].'('.$Lang['ePCM']['Report']['BER']['Dollar'].')';
					$exportArr[$row][] = $Lang['ePCM']['Report']['BER']['Difference'].'('.$Lang['ePCM']['Report']['BER']['Dollar'].')';
					$row++;
					$isFirst = false;
				}
				if($itemData['ItemID']){
					$exportArr[$row][] = Get_Lang_Selection($itemData['ItemNameChi'], $itemData['ItemName']);
					$ItemBudget =  $itemData['ItemBudget']?$itemData['ItemBudget']:'0';
					$exportArr[$row][] = account_number_format($ItemBudget);
					$ItemExpense =  $itemData['ItemExpense']?$itemData['ItemExpense']:'0';
					$exportArr[$row][] = account_number_format($ItemExpense);
					$ItemDiff =  ($itemData['ItemBudget']- $itemData['ItemExpense'])?($itemData['ItemBudget']- $itemData['ItemExpense']):'0';
					$exportArr[$row][] = account_number_format($ItemDiff);
					
					$sumOfBudget += $ItemBudget;
					$sumOfExpense += $ItemExpense;
					$sumOfDiff += $ItemDiff;
					$row++;
				}else{
					$exportArr[$row][] = $Lang['ePCM']['Report']['BER']['No Items'];
					$sumOfBudget = '0';
					$sumOfExpense = '0';
					$sumOfDiff = '0';
					$row++;
				}
				//Print Sum(last item of Group) 
				if(($key+1)==count($_dataArr)){
					$exportArr[$row][] = $Lang['ePCM']['Report']['BER']['Sum'];
					$exportArr[$row][] = account_number_format($sumOfBudget);
					$exportArr[$row][] = account_number_format($sumOfExpense);
					$exportArr[$row][] = account_number_format($sumOfDiff);
					$row++;
				}
			}
		}
		$filename = 'Budget_Expenses_Report_'.date('YmdHis').'.csv';
	break;
}
$export_content = 	$_PAGE['lexport']->GET_EXPORT_TXT($exportArr, $csvHeader, "\t", "\r\n", "\t", 0, "11",1);
if( $_PAGE['libPCM']->isEJ()){
	//EJ
	$_PAGE['lexport']->EXPORT_FILE_UNICODE($filename, $export_content, $isXLS = false, $ignoreLength = false, $BaseCode="UTF-8");
}else{
	//IP
	$_PAGE['lexport'] ->EXPORT_FILE($filename, $export_content);
}

?>