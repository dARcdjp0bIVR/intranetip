<?php
// using :
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking_ui 		= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "../room/view.php", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "view.php", 1);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$CategoryID = $_REQUEST['CategoryID'];
$Category2ID = $_REQUEST['Category2ID'];
$ItemID = $_REQUEST['ItemID'];
echo $lebooking_ui->Get_Settings_GeneralPermission_Item_View_Details_UI($ItemID);

?>

<script language="javascript">
$(document).ready( function() {	
	//$('select#SelectedCategoryID').focus();
});

function js_Go_Back_To_Item_List()
{
	window.location = 'view.php?CategoryID=<?=$CategoryID?>&Category2ID=<?=$Category2ID?>';
}

function js_Edit_Item()
{
	window.location = 'new.php?CategoryID=<?=$CategoryID?>&Category2ID=<?=$Category2ID?>&ItemID=<?=$ItemID?>';
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>