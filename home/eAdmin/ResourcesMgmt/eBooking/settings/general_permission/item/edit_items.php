<?php
// using :

#############################################################################
##	Date	:	2017-03-23 (Villa) #W114642 
##				add js_Update_AllowBookingIndependence_Status
##				modified js_Show_Selection_Layer() - add allowBookingIndependence Related
##				modified js_Change_Layer_Position - handle case AllowBookingIndependence
##				modified js_Change_Layer_Position - handle case AllowBookingIndependence
##				modified js_Update_AllowBooking_Status -  handle hide/show AllowBookingIndependence selection btn
##
##	Date	:	2011-07-12 (Marcus)
##				do not force client to select follow/approve group 
###########################################################################
 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking_ui 		= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "../room/view.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "view.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$CategoryID = $_REQUEST['CategoryID'];
$Category2ID = $_REQUEST['Category2ID'];
$DisplayedItemIDList = $_REQUEST['DisplayedItemIDList'];
$ItemIDArr = explode(',', $DisplayedItemIDList);
echo $lebooking_ui->Get_Settings_GeneralPermission_Item_EditMode_UI($CategoryID, $Category2ID, $ItemIDArr);


$AllowBookingIcon = $lebooking_ui->Get_Allow_Booking_Icon(1);
$NotAllowBookingIcon = $lebooking_ui->Get_Allow_Booking_Icon(0);
?>

<script language="javascript">
var jsCurCategoryID = '<?=$CategoryID?>';
var jsCurCategory2ID = '<?=$Category2ID?>';

var jsAllowBookingIcon = "<?=$AllowBookingIcon?>";
var jsNotAllowBookingIcon = "<?=$NotAllowBookingIcon?>";

var jsNumOfGroupLang = "<?=$Lang['eBooking']['Settings']['GeneralPermission']['Group(s)']?>";
var jsNumOfRuleLang = "<?=$Lang['eBooking']['Settings']['GeneralPermission']['Rule(s)']?>";

$(document).ready( function() {	
	
});

function Go_View_Mode()
{
	if (confirm("<?=$Lang['eBooking']['eService']['jsWarningArr']['GoBackViewModeAlert']?>"))
		window.location = 'view.php';
		//window.location = 'view.php?CategoryID=' + jsCurCategoryID + '&Category2ID=' + jsCurCategory2ID;
}

function js_Save_Items()
{
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'edit_items_update.php';
	jsFormObj.submit();
}


function js_Hide_Selection_Layer()
{
	MM_showHideLayers('SelectionLayerDiv','','hide');
}

function js_Hide_Apply_All_Layer()
{
	MM_showHideLayers('AllowBookingDiv_All','','hide');
	MM_showHideLayers('MgmtGroupDiv_All','','hide');
	MM_showHideLayers('UserBookingRuleDiv_All','','hide');
}

function js_Show_Apply_All_Layer(jsDivID)
{
	js_Hide_Selection_Layer();
	js_Hide_Apply_All_Layer();
	MM_showHideLayers(jsDivID, '', 'show');
}

function js_Show_Selection_Layer(jsType, jsClickedObjID, jsTargetHiddenFieldID, jsItemID)
{
	js_Hide_Selection_Layer();
	js_Hide_Apply_All_Layer();
	
	var jsAction = '';
	if (jsType == 'AllowBooking')
		jsAction = 'Reload_AllowBooking_Selection_Layer';
	else if (jsType == 'MgmtGroup')
		jsAction = 'Reload_MgmtGroup_Selection_Layer';
	else if (jsType == 'FollowUpGroup')
		jsAction = 'Reload_FollowUpGroup_Selection_Layer';
	else if (jsType == 'UserBookingRule')
		jsAction = 'Reload_UserBookingRule_Selection_Layer';
	else if (jsType == 'ShowItemDescription')
		jsAction = 'ShowItemDescriptionWithCloseBtn';
	else if (jsType == 'ManageItemAttachment')
		jsAction = 'Reload_Item_Attachment_Layer';
	else if (jsType == 'AllowBookingIndependence')
		jsAction = 'Reload_AllowBookingIndependence_Selection_Layer';
		
	$('div#SelectionLayerDiv').html('<?=$Lang['General']['Loading']?>');	
	js_Change_Layer_Position(jsType, jsClickedObjID);
	MM_showHideLayers('SelectionLayerDiv','','show');
		
	$('div#SelectionLayerDiv').load(
		"ajax_reload.php", 
		{ 
			Action: jsAction,
			ItemID: jsItemID,
			SelectedOptionList: $('input#' + jsTargetHiddenFieldID).val()
		},
		function(returnString)
		{
			
		}
	);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsType, jsClickedObjID) {
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft');
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') + 25;
		
	var jsLayerObj = document.getElementById('SelectionLayerDiv');
	jsLayerObj.style.left = posleft + "px";
	jsLayerObj.style.top = postop + "px";
	jsLayerObj.style.visibility = 'visible';
	if (jsType == 'AllowBooking')
	{
		jsLayerObj.style.width = '160px';
		jsLayerObj.style.height = '90px';
	}
	else if (jsType == 'AllowBookingIndependence')
	{
		jsLayerObj.style.width = '160px';
		jsLayerObj.style.height = '90px';
	}
	else if(jsType == 'ShowItemDescription')
	{
		jsLayerObj.style.width = '400px';
	}
	else
	{
		jsLayerObj.style.width = '300px';
		jsLayerObj.style.height = '150px';
	}
}



function js_Apply_To_All(jsType)
{
	js_Hide_Selection_Layer();
	
	var jsItemIDArr = document.getElementById('ItemIDList').value.split(',');
	if (jsType == 'AllowBooking')
	{
		jsAllowBooking = 0;
		if ($('input#AllowBooking_Yes_All').attr('checked'))
			jsAllowBooking = 1;
			
		var jsThisItemID;
		for (i=0; i<jsItemIDArr.length; i++)
		{
			jsThisItemID = jsItemIDArr[i];
			js_Update_AllowBooking_Status(jsThisItemID, 1, jsAllowBooking);
		}
	}
	else if (jsType == 'BookingDayBeforehand')
	{
		jsTargetValue = $('input#BookingDayBeforehand_All').val();
		
		$('input.BookingDayBeforehandTb').each( function() {
			$(this).val(jsTargetValue);
		});
	}
	else if (jsType == 'MgmtGroup')
	{
//		if (js_Check_At_Least_Select_One_Option('MgmtGroupChk_All') == false)
//		{
//			alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//			return false;	
//		}
		
		var jsThisItemID;
		for (i=0; i<jsItemIDArr.length; i++)
		{
			jsThisItemID = jsItemIDArr[i];
			js_Update_MgmtGroup_Status(jsThisItemID, 1);
		}
	}
	else if (jsType == 'FollowUpGroup')
	{
//		if (js_Check_At_Least_Select_One_Option('FollowUpGroupChk_All') == false)
//		{
//			alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//			return false;	
//		}
		
		var jsThisItemID;
		for (i=0; i<jsItemIDArr.length; i++)
		{
			jsThisItemID = jsItemIDArr[i];
			js_Update_FollowUpGroup_Status(jsThisItemID, 1);
		}
	}
	else if (jsType == 'UserBookingRule')
	{
		if (js_Check_At_Least_Select_One_Option('UserBookingRuleChk_All') == false)
		{
			alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneUserBookingRule']?>");
			return false;	
		}
		
		var jsThisItemID;
		for (i=0; i<jsItemIDArr.length; i++)
		{
			jsThisItemID = jsItemIDArr[i];
			js_Update_UserBookingRule_Status(jsThisItemID, 1);
		}
	}
	else if (jsType == 'AllowBookingIndependence')
	{
		jsAllowBooking = 0;
		if ($('input#AllowBookingIndependence_Yes_All').attr('checked'))
			jsAllowBooking = 1;
			
		var jsThisItemID;
		for (i=0; i<jsItemIDArr.length; i++)
		{
			jsThisItemID = jsItemIDArr[i];
			js_Update_AllowBookingIndependence_Status(jsThisItemID, 1, jsAllowBooking);
		}
	}
}

function js_Update_AllowBooking_Status(jsItemID, jsApplyAll, jsAllowBooking)
{
	if (jsApplyAll != 1)
	{
		jsAllowBooking = 0;
		if ($('input#AllowBooking_Yes').attr('checked'))
			jsAllowBooking = 1;
	}
	
	if (jsAllowBooking == 1)
	{
		// show selections
		$('input#BookingDayBeforehand_' + jsItemID).show();
		$('span#Separator_' + jsItemID).show();
		$('div#MgmtGroupDiv_' + jsItemID).show();
		$('div#FollowUpGroupDiv_' + jsItemID).show();
		$('div#UserBookingRuleDiv_' + jsItemID).show();
		$('div#AllowBookingIndependenceDiv_' + jsItemID).show();
		
		// change the selection icon
		$('div#AllowBookingSelContentDiv_' + jsItemID).html(jsAllowBookingIcon);
		
		// change the row color
		$('tr#Tr_' + jsItemID).removeClass('record_Waived');
		
		// change the hidden field storing the allow booking value
		$('input#AllowBooking_' + jsItemID).val('1');
	}
	else
	{
		$('input#BookingDayBeforehand_' + jsItemID).hide();
		$('span#Separator_' + jsItemID).hide();
		$('div#MgmtGroupDiv_' + jsItemID).hide();
		$('div#FollowUpGroupDiv_' + jsItemID).hide();
		$('div#UserBookingRuleDiv_' + jsItemID).hide();
		$('div#AllowBookingIndependenceDiv_' + jsItemID).hide();
		
		$('div#AllowBookingSelContentDiv_' + jsItemID).html(jsNotAllowBookingIcon);
		
		$('tr#Tr_' + jsItemID).addClass('record_Waived');
		
		$('input#AllowBooking_' + jsItemID).val('0');
	}
	
	js_Hide_Selection_Layer();
}

function js_Update_AllowBookingIndependence_Status(jsItemID, jsApplyAll, jsAllowBooking){
	if (jsApplyAll != 1)
	{
		jsAllowBooking = 0;
		if ($('input#AllowBookingIndependence_Yes').attr('checked'))
			jsAllowBooking = 1;
	}
	if(jsAllowBooking){
		// change the selection icon
		$('div#AllowBookingIndependenceSelContentDiv_' + jsItemID).html(jsAllowBookingIcon);
		// change the hidden field storing the allow booking value
		$('input#AllowBookingIndependence_' + jsItemID).val('1');
	}else{
		$('div#AllowBookingIndependenceSelContentDiv_' + jsItemID).html(jsNotAllowBookingIcon);
		
		$('input#AllowBookingIndependence_' + jsItemID).val('0');
	}
	js_Hide_Selection_Layer();
}

function js_Update_MgmtGroup_Status(jsItemID, jsFromApplyAll)
{
	// get selected management group
	var jsMgmtGroupIDArr = new Array();
	var counter = 0;
	var ThisObjID = '';
	var TempArr;
	var ThisMgmtGroupID = '';
	var TargetClass = (jsFromApplyAll==1)? 'MgmtGroupChk_All' : 'MgmtGroupChk';
	
	$('input.' + TargetClass + ':checked').each( function() {
		ThisObjID = $(this).attr('id');
		TempArr = ThisObjID.split('_');
		ThisMgmtGroupID = TempArr[1];
		
		jsMgmtGroupIDArr[counter++] = ThisMgmtGroupID;
	});
	
	// update selection display
	var numOfSelectedMgmtGroup = jsMgmtGroupIDArr.length;
	var newDisplayContent = '';
//	if (numOfSelectedMgmtGroup == 0)
//	{
//		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//		return false;
//	}
//	else 
	if (numOfSelectedMgmtGroup == 1)
	{
		newDisplayContent = $('span#MgmtGroupNameSpan_' + jsMgmtGroupIDArr[0]).html();
	}
	else
	{
		newDisplayContent = numOfSelectedMgmtGroup + ' ' + jsNumOfGroupLang;
	}
	$('div#MgmtGroupSelContentDiv_' + jsItemID).html(newDisplayContent);
	
	// update hidden field value
	$('input#MgmtGroup_' + jsItemID).val(jsMgmtGroupIDArr.toString());
	
	js_Hide_Selection_Layer();
}

function js_Update_FollowUpGroup_Status(jsItemID, jsFromApplyAll)
{
	// get selected management group
	var jsFollowUpGroupIDArr = new Array();
	var counter = 0;
	var ThisObjID = '';
	var TempArr;
	var ThisFollowUpGroupID = '';
	var TargetClass = (jsFromApplyAll==1)? 'FollowUpGroupChk_All' : 'FollowUpGroupChk';
	
	$('input.' + TargetClass + ':checked').each( function() {
		ThisObjID = $(this).attr('id');
		TempArr = ThisObjID.split('_');
		ThisFollowUpGroupID = TempArr[1];
		
		jsFollowUpGroupIDArr[counter++] = ThisFollowUpGroupID;
	});
	
	// update selection display
	var numOfSelectedFollowUpGroup = jsFollowUpGroupIDArr.length;
	var newDisplayContent = '';
//	if (numOfSelectedFollowUpGroup == 0)
//	{
//		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup']?>");
//		return false;
//	}
//	else 
	if (numOfSelectedFollowUpGroup == 1)
	{
		newDisplayContent = $('span#FollowUpGroupNameSpan_' + jsFollowUpGroupIDArr[0]).html();
	}
	else
	{
		newDisplayContent = numOfSelectedFollowUpGroup + ' ' + jsNumOfGroupLang;
	}
	$('div#FollowUpGroupSelContentDiv_' + jsItemID).html(newDisplayContent);
	
	// update hidden field value
	$('input#FollowUpGroup_' + jsItemID).val(jsFollowUpGroupIDArr.toString());
	
	js_Hide_Selection_Layer();
}

function js_Update_UserBookingRule_Status(jsItemID, jsFromApplyAll)
{
	// get selected management group
	var jsRuleIDArr = new Array();
	var counter = 0;
	var ThisObjID = '';
	var TempArr;
	var ThisRuleID = '';
	var TargetClass = (jsFromApplyAll==1)? 'UserBookingRuleChk_All' : 'UserBookingRuleChk';
	
	$('input.' + TargetClass + ':checked').each( function() {
		ThisObjID = $(this).attr('id');
		TempArr = ThisObjID.split('_');
		ThisRuleID = TempArr[1];
		
		jsRuleIDArr[counter++] = ThisRuleID;
	});
	
	// update selection display
	var numOfSelectedRule = jsRuleIDArr.length;
	var newDisplayContent = '';
	
	if (numOfSelectedRule == 0)
	{
		alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneUserBookingRule']?>");
		return false;
	}
	else if (numOfSelectedRule == 1)
	{
		newDisplayContent = $('span#UserBookingRuleNameSpan_' + jsRuleIDArr[0]).html();
	}
	else
	{
		newDisplayContent = numOfSelectedRule + ' ' + jsNumOfRuleLang;
	}
	$('div#UserBookingRuleSelContentDiv_' + jsItemID).html(newDisplayContent);
	
	// update hidden field value
	$('input#UserBookingRule_' + jsItemID).val(jsRuleIDArr.toString());
	
	js_Hide_Selection_Layer();
}


function js_Select_All(jsTargetClass, jsChecked)
{
	$('input.' + jsTargetClass).each( function() {
		$(this).attr('checked', jsChecked);
	});
}

function js_Check_Uncheck_Select_All(jsSelectAllObjID, jsChecked)
{
	if (jsChecked == false)
		$('input#' + jsSelectAllObjID).attr('checked', false);
}

function js_Check_At_Least_Select_One_Option(jsObjClass)
{
	var jsCounter = 0;
	$('input.' + jsObjClass).each( function() {
		if ($(this).attr('checked') == true)
			jsCounter++;
	});
	
	if (jsCounter > 0)
		return true;
	else
		return false;
}

function js_Delete_Item(jsItemID)
{
	var jsAction = "CheckItemExistInPandingBooking";
	$.post(
		"ajax_validate.php",
		{
			"Action" : jsAction,
			"ItemID" : jsItemID
		},
		function(responseText)
		{
			if(responseText == "1")
			{
				if (confirm("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['DeleteItem']?>"))
				{
					var jsReturnMsg = '';
					
					$.post(
						"ajax_update.php", 
						{ 
							Action: 'Delete_Item',
							ItemID: jsItemID
						},
						function(ReturnData)
						{
							if (ReturnData == '1')
								jsReturnMsg = "<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemSuccess']?>";
							else
								jsReturnMsg = "<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemFailed']?>";
							
							Get_Return_Message(jsReturnMsg);
							js_Reload_Edit_Item_Table();
							//$('#IndexDebugArea').html(ReturnData);
						}
					);
				}
			}
			else
			{
				alert("<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['ItemDeleteFailed_AlreadyExistInPandingBooking'];?>");
			}
		}
	);
}

function js_Reload_Edit_Item_Table()
{
	$('div#ItemEditTableDiv').html('');
	Block_Element('ItemEditTableDiv');
	
	$('div#ItemEditTableDiv').load(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Item_EditMode_Table',
			CategoryID: jsCurCategoryID,
			Category2ID: jsCurCategory2ID
		},
		function(ReturnData)
		{
			UnBlock_Element('ItemEditTableDiv');
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
}

function js_Reload_Item_Attachment_Table(jsItemID)
{
	$('div#SelectionLayerDiv').html('');
	Block_Element('SelectionLayerDiv');
	
	$('div#SelectionLayerDiv').load(
		"ajax_reload.php", 
		{ 
			"Action": 'Reload_Item_Attachment_Layer',
			"ItemID": jsItemID
		},
		function(ReturnData)
		{
			UnBlock_Element('SelectionLayerDiv');
		}
	);
}

function js_Delete_Item_Attachment(jsItemID)
{
	if(confirm('<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['DeleteAttachment']?>')){
		$.post(
			"ajax_update.php",
			{
				"Action":"Delete_Item_Attachment",
				"ItemID":jsItemID
			},
			function(responseText){
				if(responseText == '1'){
					jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentSuccess']?>';
				}else{
					jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemAttachmentFailed']?>';
				}
				Get_Return_Message(jsReturnMsg);
				js_Reload_Item_Attachment_Table(jsItemID);
			}
		);
	}
}

function js_Upload_Item_Attachment(jsItemID)
{
	var attachment = $.trim($('input#Attachment').val());
	if(attachment != ''){
		Block_Element('SelectionLayerDiv');
		var Obj = document.getElementById("FormAttachment");
		Obj.target = 'FileUploadIframe'; 
		Obj.encoding = "multipart/form-data";
		Obj.method = 'post';
		Obj.action = "ajax_update.php?Action=Upload_Item_Attachment&ItemID="+jsItemID;
		Obj.submit();
	}else{
		alert('<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAttachment']?>');
	}
}

function Upload_Attachemnt_Feedback(jsItemID,response)
{
	var jsReturnMsg = '';
	if(response==1)
		jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['UploadItemAttachmentSuccess']?>';
	else
		jsReturnMsg = '<?=$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['UploadItemAttachmentFailed']?>';
	
	Get_Return_Message(jsReturnMsg);
	js_Reload_Item_Attachment_Table(jsItemID);
	UnBlock_Element('SelectionLayerDiv');
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>