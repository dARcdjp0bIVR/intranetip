<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
intranet_auth();
intranet_opendb();

define("ERROR_MISSING",1);
define("ERROR_NOT_FOUND",2);
define("ERROR_NOT_MATCH",3);
define("ERROR_IS_EXIST",4);
define("ERROR_INVALID",5);



$limport = new libimporttext();
$lo = new libfilesystem();
$libebooking = new libebooking();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();
$linterface = new interface_html();
$libdb = new libdb();
$CurrentPage	= "Settings_GeneralPermission";

		
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "../room/view.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "view.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();



$importMode = $_POST['importMode'];
$filename = $_FILES['userfile']['name'];
$filePath = $_FILES['userfile']['tmp_name'];
$ext = strtoupper($lo->file_ext($filename));
# check extension of import file 
if($ext != ".CSV" && $ext != ".TXT")
{
	exit(header("Location: import.php"));
}	

if($limport->CHECK_FILE_EXT($filename)) {
	# read file into array
	# return 0 if fail, return csv array if success
	$data = $limport->GET_IMPORT_TXT($filePath);
}
# check any record in import file
if(sizeof($data)==0) {
	
	exit(header("Location: import.php"));	
}
if(is_array($data))
{
	$col_name = array_shift($data);
}

if($importMode == 'new'){
	$file_format = array('Resource Category','Subcategory','Code','Name (English)','Name (Chinese)','Description (English)','Description (Chinese)','Location','Allow Booking','Booking Day Beforehand','Approval Group','Follow-up Group','User permision rule');
	$existencyCheckingArray = array(1,1,1,1,1,0,0,1,1,0,0,0,1);
}else if($importMode == 'update'){
	$file_format = array('Existing Code','Resource Category','Subcategory','Code','Name (English)','Name (Chinese)','Description (English)','Description (Chinese)','Location','Allow Booking','Booking Day Beforehand','Approval Group','Follow-up Group','User permision rule');
	$existencyCheckingArray = array(1,1,1,1,1,1,0,0,1,1,0,0,0,1);
}
//debug_pr($file_format);
//debug_pr($col_name);
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong){
	header("Location: import.php");
	exit();
}


#########################################
#	Delete old data from Temp DB Table
#########################################
$sql = "DELETE FROM TEMP_INTRANET_EBOOKING_ITEM WHERE InputBy = $UserID ";
$libdb->db_db_query($sql);

$errorArray = array();
foreach($data as $i => $dataArray){
	
	if($importMode == 'new'){
		list($_dataArray['ResourceCategoryID'],$_dataArray['SubcategoryID'],$_dataArray['Code'],$_dataArray['NameEng'],$_dataArray['NameChi'],$_dataArray['DescEng'],$_dataArray['DescChi'],$_dataArray['LocationID'],$_dataArray['AllowBooking'],$_dataArray['BookingDayBeforehand'],$_dataArray['ManagementGroupID'],$_dataArray['FollowupGroupID'],$_dataArray['RuleID'])=$dataArray;
	}else if($importMode == 'update'){
		list($_dataArray['ExistingCode'],$_dataArray['ResourceCategoryID'],$_dataArray['SubcategoryID'],$_dataArray['Code'],$_dataArray['NameEng'],$_dataArray['NameChi'],$_dataArray['DescEng'],$_dataArray['DescChi'],$_dataArray['LocationID'],$_dataArray['AllowBooking'],$_dataArray['BookingDayBeforehand'],$_dataArray['ManagementGroupID'],$_dataArray['FollowupGroupID'],$_dataArray['RuleID'])=$dataArray;
	}
	###############################################
	#
	#	Start Checking
	#
	###############################################
	
	#####Step 1: existency checking
	/**
	 * Note: $errorColumn = -1 => no error
	 * if $errorColumn>=0 => $data[$i][$errorColumn] failed
	 */
	$errorColumn = $limport->GET_EMPTY_COLUMN($dataArray,$existencyCheckingArray);
	if($errorColumn==-1){
		$passExistencyXhecking = true;
	}else{
		$passExistencyXhecking = false;
	}
	if(!$passExistencyXhecking){
		$errorArray[] = array('ErrorMessage'=>ERROR_MISSING,'Column'=>$file_format[$errorColumn],'Row'=>$i);
		continue;
	}
	
	#####Step 2: Check is Existing Code are exist in db
	if($importMode == 'update'){
		$itemInfo = $libebooking->Get_Item_Info_By_Item_Code($_dataArray['ExistingCode']);
		$itemID = $itemInfo[0]['ItemID'];
		
		//if $itemID = '' => Not exist => error
		if($itemID==''){
			$errorArray[] = array('ErrorMessage'=>ERROR_NOT_FOUND,'Column'=>'Existing Code','Row'=>$i);
			continue;
		}
	}else if($importMode == 'new'){
		$itemID='';
	}
	
	#####Step 3: Check is Resource Category is exist in db
	$catInfo = $libebooking->Get_Item_Category($_dataArray['ResourceCategoryID']);
	if(empty($catInfo)){
		//if empty => Category not exist => error
		$errorArray[] = array('ErrorMessage'=>ERROR_NOT_FOUND,'Column'=>'Resource Category','Row'=>$i);
		continue;
	}
	
	#####Step 4: Check is subcategory exist in db in particular category
	$subcatInfo = $libebooking->Get_Item_SubCategory($_dataArray['ResourceCategoryID'],$_dataArray['SubcategoryID']);
	if(empty($subcatInfo)){
		//if empty => Category not exist => error
		$errorArray[] = array('ErrorMessage'=>ERROR_NOT_MATCH,'Column'=>'Subcategory','Row'=>$i);
		continue;
	}
	
	#####Step 5: Check Code is avaliable
	$itemInfo2 = $libebooking->Get_Item_Info_By_Item_Code($_dataArray['Code']);
	if($itemID==''){
		if(empty($itemInfo2)){
			// good to go
		}else{
			// code exist => error
			$errorArray[] = array('ErrorMessage'=>ERROR_IS_EXIST,'Column'=>'Code','Row'=>$i);
			continue;
		}
	}else{
		//check code avaliable beside itself
		if(empty($itemInfo2)){
			// good to go
		}else{
			if($itemID==$itemInfo2[0]['ItemID']){
				//exist only because itself => good to go
			}else{
				// code exist => error
				$errorArray[] = array('ErrorMessage'=>ERROR_IS_EXIST,'Column'=>'Code','Row'=>$i);
				continue;
			}
		}
	}
	
	#####Step 6: Check Location is exist
	$locationArray = $libebooking->isLocationExistByID($_dataArray['LocationID']);
	if(empty($locationArray)){
		// NOT Exist => error
		$errorArray[] = array('ErrorMessage'=>ERROR_NOT_FOUND,'Column'=>'Location','Row'=>$i);
		continue;
	}
	
	#####Step 7: Check Allow Booking
	$allowBooking = $limport->CHECK_VALID_YES_NO($_dataArray['AllowBooking']);
	if($allowBooking==-1){
		//Invalid => error
		$errorArray[] = array('ErrorMessage'=>ERROR_INVALID,'Column'=>'Allow Booking','Row'=>$i);
		continue;
	}
	$_dataArray['AllowBooking']=$allowBooking;
	
	#####Step 8: Check existance and validation of Booking Days beforehand (only of allow booking)
	if($allowBooking){
		if($_dataArray['BookingDayBeforehand']==''){
			//NOT EXIST => error
			$errorArray[] = array('ErrorMessage'=>ERROR_MISSING,'Column'=>'Booking Day Beforehand','Row'=>$i);
			continue;
		}
		if(!($_dataArray['BookingDayBeforehand']>=0)){
			//in valid => error
			$errorArray[] = array('ErrorMessage'=>ERROR_INVALID,'Column'=>'Booking Day Beforehand','Row'=>$i);
			continue;
		}
	}else{
		//Set $_dataArray['BookingDayBeforehand'] = 0 if not allow booking
		$_dataArray['BookingDayBeforehand'] = 0;
	}
	
	#####Step 9: Check approval group is valid
	if($_dataArray['ManagementGroupID']!=''){
		$managementGroupIdArray =  explode(';',$_dataArray['ManagementGroupID']);
		//Check valid
		$__continue = false;
		foreach($managementGroupIdArray as $mGroupID){
			$returnMGroupArray = $libebooking->getAllManagementGroupInfo($mGroupID);
			if(empty($returnMGroupArray)){
				//Not exist in db
				$errorArray[] = array('ErrorMessage'=>ERROR_NOT_MATCH,'Column'=>'Approval Group','Row'=>$i);
				$__continue = true;
				continue;
			}
		}
		if($__continue){
			continue;
		}
		asort($managementGroupIdArray);
		$_dataArray['ManagementGroupID'] = implode(',',$managementGroupIdArray);
	}
	
	#####Step 10: Check followup group is valid
	if($_dataArray['FollowupGroupID']!=''){
		$fuGroupIdArray =  explode(';',$_dataArray['FollowupGroupID']);
		//Check valid
		$__continue = false;
		foreach($fuGroupIdArray as $fuGroupID){
			$returnFuGroupArray = $libebooking->getAllFollowupGroupInfo($fuGroupID);
			if(empty($returnFuGroupArray)){
				//Not exist in db
				$errorArray[] = array('ErrorMessage'=>ERROR_NOT_MATCH,'Column'=>'Follow-up Group','Row'=>$i);
				$__continue = true;
				continue;
			}
		}
		if($__continue){
			continue;
		}
		asort($fuGroupIdArray);
		$_dataArray['FollowupGroupID'] = implode(',',$fuGroupIdArray);
	}
	
	#####Step 11: Check rule is valid
	$ruleIdArray = explode(';',$_dataArray['RuleID']);
	foreach($ruleIdArray as $ruleID){
		$returnRuleArray = $libebooking->Get_User_Booking_Rule($ruleID);
		$__continue = false;
		if(empty($returnRuleArray)){
			//Not exist in db
			$errorArray[] = array('ErrorMessage'=>ERROR_NOT_MATCH,'Column'=>'User permision rule','Row'=>$i);
			$__continue = true;
			continue;
		}
	}
	if($__continue){
		continue;
	}
	asort($ruleIdArray);
	$_dataArray['RuleID'] = implode(',',$ruleIdArray);
	
	###############################################
	#
	#	END Checking
	#
	###############################################

	
	
	#########################################
	#	Store to Temp DB Table
	#########################################
	$Field = array('ItemID','ItemType','CategoryID','Category2ID','NameChi','NameEng','DescriptionChi','DescriptionEng','ItemCode','LocationID','BookingDayBeforehand','AllowBooking','RecordStatus','DateInput','InputBy','DateModified','ModifiedBy','TempManagementGroupID','TempFollowUpGroupID','TempUserBookingRuletID');
	$Value = array($itemID,1,$_dataArray['ResourceCategoryID'],$_dataArray['SubcategoryID'],$_dataArray['NameChi'],$_dataArray['NameEng'],$_dataArray['DescChi'],$_dataArray['DescEng'],$_dataArray['Code'],$_dataArray['LocationID'],$_dataArray['BookingDayBeforehand'],$_dataArray['AllowBooking'],1,date('Y-m-d h:i:s'),$UserID,date('Y-m-d h:i:s'),$UserID,$_dataArray['ManagementGroupID'],$_dataArray['FollowupGroupID'],$_dataArray['RuleID']);
	$sql = "INSERT INTO TEMP_INTRANET_EBOOKING_ITEM (".implode(',',$Field).") VALUES ('".implode('\',\'',$Value)."')";
	$libdb->db_db_query($sql);

}


##### error msg
$errorArray = BuildMultiKeyAssoc($errorArray, array('Row'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=0);
$FailedNum = 0;
$seperator = Get_Lang_Selection(" ","");
foreach($errorArray as $rowNum => $errorDetails){
	
	switch ($errorDetails['ErrorMessage']){
		case ERROR_MISSING:
			$errorMessage = $errorDetails['Column'].$seperator.$Lang['eBooking']['Settings']['Items']['Import']['error']['missing'];
		break;
		case ERROR_NOT_FOUND:
			$errorMessage = $errorDetails['Column'].$seperator.$Lang['eBooking']['Settings']['Items']['Import']['error']['notFound'];
		break;
		case ERROR_NOT_MATCH:
			$errorMessage = $errorDetails['Column'].$seperator.$Lang['eBooking']['Settings']['Items']['Import']['error']['notMatch'];
		break;
		case ERROR_IS_EXIST:
			$errorMessage = $errorDetails['Column'].$seperator.$Lang['eBooking']['Settings']['Items']['Import']['error']['isExist'];
		break;
		case ERROR_INVALID:
			$errorMessage = $errorDetails['Column'].$seperator.$Lang['eBooking']['Settings']['Items']['Import']['error']['invalid'];
		break;
		default:
			$errorMessage = $errorDetails['Column'].$seperator.$Lang['eBooking']['Settings']['Items']['Import']['error']['unknown'];
		break;

	}
	$FailedNum++;
	$errorArray[$rowNum]['FinalizedMessage']=$errorMessage;;
}
$linterface->LAYOUT_START();
$successNum = count($data)-$FailedNum ;

echo $lebooking_ui->Get_Settings_Import_Step2_UI($successNum,$FailedNum,$col_name,$data,$errorArray);


?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>