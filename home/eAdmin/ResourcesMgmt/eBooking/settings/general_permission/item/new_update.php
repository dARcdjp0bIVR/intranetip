<?php
// using : 
/*
 * Change Log:
 * Date 2017-03-23 Villa #W114642 
 * - add AllowBookingIndependence
 * 
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_item.php");

intranet_auth();
intranet_opendb();



$DataArr = array();
$DataArr['CategoryID'] = $_REQUEST['SelectedCategoryID'];
$DataArr['Category2ID'] = $_REQUEST['SelectedCategory2ID'];
$DataArr['ItemCode'] = stripslashes($_REQUEST['ItemCode']);
$DataArr['NameEng'] = stripslashes($_REQUEST['NameEng']);
$DataArr['NameChi'] = stripslashes($_REQUEST['NameChi']);
$DataArr['DescriptionEng'] = stripslashes($_REQUEST['DescriptionEng']);
$DataArr['DescriptionChi'] = stripslashes($_REQUEST['DescriptionChi']);
$DataArr['LocationID'] = stripslashes($_REQUEST['RoomID']);
$DataArr['AllowBooking'] = stripslashes($_REQUEST['AllowBooking']);
$DataArr['BookingDayBeforehand'] = stripslashes($_REQUEST['BookingDayBeforehand']);
$DataArr['RecordStatus'] = 1;
$DataArr['ItemType'] = 1; // Item added from eBooking must be single item
$DataArr['AllowBookingIndependence'] = stripslashes($_REQUEST['AllowBookingIndependence']); #W114642 

$ItemID = $_REQUEST['ItemID'];
if ($ItemID != '')
{
	// edit
	$objItem = new eBooking_Item($ItemID);
	$SuccessArr['Update_ItemInfo'] = $objItem->Update_Object($DataArr);
	$FinalItemID = $ItemID;
}
else
{
	// new
	$libItem = new eBooking_Item();
	$insertedItemID = $libItem->Insert_Object($DataArr);
	$objItem = new eBooking_Item($insertedItemID);
	$FinalItemID = $insertedItemID;
}

if($Attachment!=""){
	if($_FILES["Attachment"]["error"]>0){
		$SuccessArr['Upload_Attachment'] = false;
	}else{
		$attachment_location = $_FILES["Attachment"]["tmp_name"];
		$attachment_name = stripslashes(trim($_FILES["Attachment"]["name"]));
		$SuccessArr['Upload_Attachment'] = $objItem->Upload_Item_Attachment($FinalItemID,$attachment_location,$attachment_name);
	}
}

$SuccessArr['Update_ManagementGroup'] = $objItem->Update_Item_Management_Group($MgmtGroupIDArr);

$SuccessArr['Update_FollowUpGroup'] = $objItem->Update_Item_FollowUp_Group($FollowUpGroupIDArr);

$SuccessArr['Update_UserBookingRule'] = $objItem->Update_Item_User_Booking_Rule($RuleIDArr);

intranet_closedb();

$CategoryID = $DataArr['CategoryID'];
$Category2ID = $DataArr['Category2ID'];

if ($ItemID != '')
{
	$ReturnMsgKey = (in_array(false, $SuccessArr) == false)? 'EditItemSuccess' : 'EditItemFailed';
	$ReturnPage = 'view_item_details.php';
}
else
{
	$ReturnMsgKey = (in_array(false, $SuccessArr) == false)? 'AddItemSuccess' : 'AddItemFailed';
	$ReturnPage = 'view.php';
}

header("Location: $ReturnPage?CategoryID=$CategoryID&Category2ID=$Category2ID&ItemID=$ItemID&ReturnMsgKey=$ReturnMsgKey");
?>