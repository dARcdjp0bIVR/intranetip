<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
intranet_auth();
intranet_opendb();

$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();
$linterface = new interface_html();
$CurrentPage	= "Settings_GeneralPermission";

		
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "../room/view.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "view.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

echo $lebooking_ui->Get_Settings_Import_Step1_UI();

?>
<script>
$( document ).ready(function() {
	var mode = $('input[name=importMode]:checked').val();
	if(mode=='update'){
		$('.update').show();
		$('.new').hide();
	}
});
function Load_Reference(ref) {

	$('#remarkDiv_type').html('');
	var AcademicYearID = $('#AcademicYearID').val();
	
	$.post(
		"ajax_reload.php", 
			{ 
			Action: "load_reference",
			flag: ref,
			AcademicYearID: AcademicYearID
		},
		function(ReturnData)
		{
			$('#remarkDiv_type').html(ReturnData);
		}
	);
	
	displayReference(ref);
}
function displayReference(ref) {
	var mode = $('input[name=importMode]:checked').val();
	var p = $("."+ mode +" #"+ref);
	var position = p.position();
	
	var t = position.top + 15;
	var l = position.left + p.width()+5;
	$("#remarkDiv_type").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
}
function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}
function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}
function onClickImportMode(obj){
	var mode = obj.value;
	if(mode=='update'){
		$('.update').show();
		$('.new').hide();
	}else if(mode=='new'){
		$('.update').hide();
		$('.new').show();
	}
}
</script>
<style>
.update{
	display:none;
}

</style>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>