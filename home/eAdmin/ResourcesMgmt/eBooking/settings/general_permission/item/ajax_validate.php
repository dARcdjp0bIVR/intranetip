<?php
// using : 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_item.php");

intranet_auth();
intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);
$libItem = new eBooking_Item();
$lebooking = new libebooking();

if ($Action == 'Check_Unique_ItemCode')
{
	$ExcludeItemID = stripslashes($_REQUEST['ItemID']);
	$ItemCode = stripslashes($_REQUEST['ItemCode']);
	
	$returnString = $libItem->Is_Code_Valid($ItemCode, $ExcludeItemID);
}
else if($Action == 'CheckItemExistInPandingBooking')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	
	$returnString = $libItem->Is_Item_Exist_In_Panding_BookingRequest($ItemID);
}

echo $returnString;

intranet_closedb();
?>