<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking_item.php");
intranet_auth();
intranet_opendb();


$limport = new libimporttext();
$lo = new libfilesystem();
$libebooking = new libebooking();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();
$linterface = new interface_html();
$libdb = new libdb();
$CurrentPage	= "Settings_GeneralPermission";

		
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "../room/view.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "view.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$sql = "SELECT * FROM TEMP_INTRANET_EBOOKING_ITEM WHERE InputBy = $UserID";
$tempDataArray = $libdb->returnArray($sql);
foreach($tempDataArray as $tempData){
	
	$DataArr = array();
	$DataArr['CategoryID'] = $tempData['CategoryID'];
	$DataArr['Category2ID'] = $tempData['Category2ID'];
	$DataArr['ItemCode'] = $tempData['ItemCode'];
	$DataArr['NameEng'] = $tempData['NameEng'];
	$DataArr['NameChi'] = $tempData['NameChi'];
	$DataArr['DescriptionEng'] = $tempData['DescriptionEng'];
	$DataArr['DescriptionChi'] = $tempData['DescriptionChi'];
	$DataArr['LocationID'] = $tempData['LocationID'];
	$DataArr['AllowBooking'] = $tempData['AllowBooking'];
	$DataArr['BookingDayBeforehand'] = $tempData['BookingDayBeforehand'];
	$DataArr['RecordStatus'] = $tempData['RecordStatus'];
	$DataArr['ItemType'] = $tempData['ItemType']; // Item added from eBooking must be single item
		
	if($tempData['ItemID']==0){
		//INSERT NEW
		$libItem = new eBooking_Item();
		$insertedItemID = $libItem->Insert_Object($DataArr);
		$objItem = new eBooking_Item($insertedItemID);
		$FinalItemID = $insertedItemID;
	}else{
		//UPDATE ITEM
		$objItem = new eBooking_Item($tempData['ItemID']);
		$SuccessArr['Update_ItemInfo'] = $objItem->Update_Object($DataArr);
		$FinalItemID = $tempData['ItemID'];
	}
	$MgmtGroupIDArr = explode(',',$tempData['TempManagementGroupID']);
	$SuccessArr['Update_ManagementGroup'] = $objItem->Update_Item_Management_Group($MgmtGroupIDArr);
	$FollowUpGroupIDArr = explode(',',$tempData['TempFollowUpGroupID']);
	$SuccessArr['Update_FollowUpGroup'] = $objItem->Update_Item_FollowUp_Group($FollowUpGroupIDArr);
	$RuleIDArr = explode(',',$tempData['TempUserBookingRuletID']);
	$SuccessArr['Update_UserBookingRule'] = $objItem->Update_Item_User_Booking_Rule($RuleIDArr);
	

}

echo $lebooking_ui->Get_Settings_Import_Step3_UI();


?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>