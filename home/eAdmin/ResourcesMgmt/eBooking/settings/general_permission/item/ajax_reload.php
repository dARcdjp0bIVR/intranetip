<?php
// using : 
/*
 * Change Log:
 * 
 * Date:    2019-05-13 Cameron
 * - fix potential sql injection problem by enclosing var with apostrophe
 * 
 * Date:	2017-03-23 Villa #W114642 
 * - add handle Reload_AllowBookingIndependence_Selection_Layer
 */
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);
$libebooking_ui = new libebooking_ui();
$libebooking = new libebooking();
$linterface = new interface_html();

if ($Action == 'Reload_SubCategory_Selection')
{
	$CategoryID = stripslashes($_POST['CategoryID']);
	$Category2ID = stripslashes($_POST['Category2ID']);
	$OnChange = stripslashes($_POST['OnChange']);
	$ID_Name = stripslashes($_POST['ID_Name']);
	$isAll = stripslashes($_POST['isAll']);
	
	$returnString = $libebooking_ui->Get_Item_SubCategory_Selection($ID_Name, $CategoryID, $Category2ID, $OnChange, $isAll);
}
else if ($Action == 'Reload_Item_View_Table')
{
	$CategoryID = stripslashes($_POST['CategoryID']);
	$Category2ID = stripslashes($_POST['Category2ID']);
	$PageNo = stripslashes($_POST['PageNo']);
	$NumPerPage = stripslashes($_POST['NumPerPage']);
	
	$arrCookies[] = array("CategoryID", "CategoryID");
	$arrCookies[] = array("Category2ID", "Category2ID");
	
	updateGetCookies($arrCookies);
	
	
	$returnString = $libebooking_ui->Get_Settings_GeneralPermission_Item_View_Table($CategoryID, $Category2ID, $PageNo, $NumPerPage);
}
else if ($Action == 'Reload_Item_MgmtGroup_Layer')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$returnString = $libebooking_ui->Get_Item_MgmtGroup_Layer_Table($ItemID);
}
else if ($Action == 'Reload_Item_FollowUpGroup_Layer')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$returnString = $libebooking_ui->Get_Item_FollowUpGroup_Layer_Table($ItemID);
}
else if ($Action == 'Reload_Item_UserBookingRule_Layer')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$returnString = $libebooking_ui->Get_Item_UserBookingRule_Layer_Table($ItemID);
}
else if ($Action == 'Reload_AllowBooking_Selection_Layer')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$AllowBooking = stripslashes($_REQUEST['SelectedOptionList']);
	
	$returnString = $libebooking_ui->Get_Allow_Booking_Selection_Table($ItemID, $AllowBooking);
}
elseif($Action =='Reload_AllowBookingIndependence_Selection_Layer'){
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$AllowBooking = stripslashes($_REQUEST['SelectedOptionList']);
	
	$returnString = $libebooking_ui->Get_Allow_Booking_Independence_Selection_Table($ItemID,$AllowBooking);
}
else if ($Action == 'Reload_MgmtGroup_Selection_Layer')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$MgmtGroupIDList = stripslashes($_REQUEST['SelectedOptionList']);
	$MgmtGroupIDArr = explode(',', $MgmtGroupIDList);
	
	$returnString = $libebooking_ui->Get_Management_Group_Selection_Table($ItemID, $MgmtGroupIDArr);
}
else if ($Action == 'Reload_FollowUpGroup_Selection_Layer')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$FollowUpGroupIDList = stripslashes($_REQUEST['SelectedOptionList']);
	$FollowUpGroupIDArr = explode(',', $FollowUpGroupIDList);
	
	$returnString = $libebooking_ui->Get_FollowUp_Group_Selection_Table($ItemID, $FollowUpGroupIDArr);
}
else if ($Action == 'Reload_UserBookingRule_Selection_Layer')
{
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$RuleIDList = stripslashes($_REQUEST['SelectedOptionList']);
	$RuleIDArr = explode(',', $RuleIDList);
	
	$returnString = $libebooking_ui->Get_User_Booking_Rule_Selection_Table($ItemID, $RuleIDArr);
}
else if ($Action == 'Reload_Item_EditMode_Table')
{
	$CategoryID = stripslashes($_REQUEST['CategoryID']);
	$Category2ID = stripslashes($_REQUEST['Category2ID']);
	
	$returnString = $libebooking_ui->Get_Settings_GeneralPermission_Item_EditMode_Table($CategoryID, $Category2ID);
}
else if($Action == 'ShowItemDescription')
{
	$sql = "SELECT DescriptionChi, DescriptionEng FROM INTRANET_EBOOKING_ITEM WHERE ItemID = '$ItemID'";
	$arrResult = $libebooking_ui->returnArray($sql,2);
	
	$returnString = "<table id='Table_".$ItemID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$returnString .= "<tbody>";
	for($i=0; $i<sizeof($arrResult); $i++)
	{
		list($chi_description, $eng_description) = $arrResult[$i];
		if($_SESSION['intranet_session_language'] == 'en'){
			$item_description = $eng_description;
		}else{
			$item_description = $chi_description;
		}
		$returnString .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none' colspan='3'>".nl2br($item_description)."</td></tr>";
	}
	$returnString .= "</tbody>";
	$returnString .= "</table>";
}
else if($Action == 'ShowItemDescriptionWithCloseBtn')
{
	$sql = "SELECT DescriptionChi, DescriptionEng FROM INTRANET_EBOOKING_ITEM WHERE ItemID = '$ItemID'";
	$arrResult = $libebooking_ui->returnArray($sql,2);
	
	$returnString = "<table id='Table_".$ItemID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$returnString .= "<tbody>";
	$returnString .= "<tr><td align='right'><a href='javascript:js_Hide_Selection_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
	for($i=0; $i<sizeof($arrResult); $i++)
	{
		list($chi_description, $eng_description) = $arrResult[$i];
		if($_SESSION['intranet_session_language'] == 'en'){
			$item_description = $eng_description;
		}else{
			$item_description = $chi_description;
		}
		$returnString .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none' colspan='3'>".nl2br($item_description)."</td></tr>";
	}
	$returnString .= "</tbody>";
	$returnString .= "</table>";
}else if($Action == 'Reload_Item_Attachment_Layer'){
	$ItemID = stripslashes($_REQUEST['ItemID']);
	$returnString = $libebooking_ui->Get_Item_Attachment_Layer_Table($ItemID);
}else if($Action == 'load_reference'){
	if($flag == 'resourcesCategory'){
		$resourcesCategoryArray = $libebooking->Get_Item_Category('','1','CategoryName');
		$titleArray = array($Lang['General']['Code'],$Lang['eBooking']['Settings']['FieldTitle']['Category']);
		$numOfCat = count($resourcesCategoryArray);
		for($i=0;$i<$numOfCat;$i++){
			$contentArray[] = array($resourcesCategoryArray[$i]['CategoryID'],$resourcesCategoryArray[$i]['CategoryName']);
		}
		//$output = $linterface->getReferenceThickBox($titleArray,$contentArray,'');
	}else if ($flag == 'resourcesSubcategory'){
		$resourcesSubcategoryArray = $libebooking->Get_Item_SubCategory('','','CategoryName');
		//debug_pr($resourcesSubcategoryArray);
		$titleArray = array($Lang['General']['Code'],$Lang['eBooking']['General']['FieldTitle']['Category2'],$Lang['eBooking']['Settings']['FieldTitle']['Category']);
		$numOfSubcat = count($resourcesSubcategoryArray);
		for($i=0;$i<$numOfSubcat;$i++){
			$contentArray[] = array($resourcesSubcategoryArray[$i]['Category2ID'],$resourcesSubcategoryArray[$i]['SubCategoryName'],$resourcesSubcategoryArray[$i]['CategoryName']);
		}
	}else if ($flag == 'code'){
		$itemArray = $libebooking->Get_Item_Info_By_ItemID();
		$titleArray = array($Lang['General']['Code'],	$Lang['General']['Name']);
		$numOfItem = count($itemArray);
		for($i=0;$i<$numOfItem;$i++){
			$contentArray[] = array($itemArray[$i]['ItemCode'],Get_Lang_Selection($itemArray[$i]['NameChi'],$itemArray[$i]['NameEng']));
		}
	}else if($flag == 'location'){
		$locationArray = $libebooking->getAllLocationInfo('BuildingID,LocationLevelID,LocationID');
		//debug_pr($locationArray);
		$titleArray = array($Lang['General']['Code'],$i_InventorySystem_Location,$i_InventorySystem_Item_Location,$Lang['eBooking']['Settings']['SystemProperty']['Building']);
		$numOfLocation = count($locationArray);
		for($i=0;$i<$numOfLocation;$i++){
			$contentArray[] = array($locationArray[$i]['LocationID'],Get_Lang_Selection($locationArray[$i]['RoomNameChi'],$locationArray[$i]['RoomNameEng']),Get_Lang_Selection($locationArray[$i]['LevelNameChi'],$locationArray[$i]['LevelNameEng']),Get_Lang_Selection($locationArray[$i]['BuildingNameChi'],$locationArray[$i]['BuildingNameEng']));
		}
	}else if($flag == 'allowBooking'){
		$titleArray = array($Lang['General']['Code'],$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['AllowBooking']);
		$contentArray[] = array("Y",$Lang['General']['Yes']);
		$contentArray[] = array("N",$Lang['General']['No']);
	}else if($flag == 'managementGroup'){
		$mgtGroupInfo = $libebooking->getAllManagementGroupInfo();
		$titleArray = array($Lang['General']['Code'],$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup']);
		$numOfGroup = count($mgtGroupInfo);
		for($i=0;$i<$numOfGroup;$i++){
			$contentArray[] = array($mgtGroupInfo[$i]['GroupID'],$mgtGroupInfo[$i]['GroupName']);
		}
	}else if($flag == 'followupGroup'){
		$GroupInfo = $libebooking->getAllFollowupGroupInfo();
		$titleArray = array($Lang['General']['Code'],$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup']);
		$numOfGroup = count($GroupInfo);
		for($i=0;$i<$numOfGroup;$i++){
			$contentArray[] = array($GroupInfo[$i]['GroupID'],$GroupInfo[$i]['GroupName']);
		}
	}else if($flag == 'rule'){
		$rules = $libebooking->Get_User_Booking_Rule();
		$titleArray = array($Lang['General']['Code'],	$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['UserBookingRule']);
		$numOfRules = count($rules);
		for($i=0;$i<$numOfRules;$i++){
			$contentArray[] = array($rules[$i]['RuleID'],$rules[$i]['RuleName']);
		}
	}
	else{
		echo '';
		return;
	}
	echo $linterface->getReferenceThickBox($titleArray,$contentArray,'');;
}

echo $returnString;

intranet_closedb();
?>