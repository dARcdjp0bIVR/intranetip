<?php
// using : 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("CategoryID_eBooking_Settings_GeneralPermission_Item_View", "CategoryID");
$arrCookies[] = array("Category2ID_eBooking_Settings_GeneralPermission_Item_View", "Category2ID");
$arrCookies[] = array("pageNo_eBooking_Settings_GeneralPermission_Item_View", "pageNo");
$arrCookies[] = array("numPerPage_eBooking_Settings_GeneralPermission_Item_View", "numPerPage");

//$CategoryID = (isset($_POST['CategoryID']) )? $_POST['CategoryID'] : $CategoryID;
//$Category2ID = (isset($_POST['Category2ID']) )? $_POST['Category2ID'] : $Category2ID;
//$pageNo = (isset($_POST['pageNo']) && $_POST['pageNo'] != '')? $_POST['pageNo'] : $pageNo;
//$numPerPage = (isset($_POST['numPerPage']) && $_POST['numPerPage'] != '')? $_POST['numPerPage'] : $numPerPage;

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$CategoryID = '';
	$Category2ID = '';
	$pageNo = '';
	$numPerPage = '';
}
else 
	updateGetCookies($arrCookies);

	
intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking_ui 		= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "../room/view.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "view.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

echo $lebooking_ui->initJavaScript();
echo $lebooking_ui->Get_Settings_GeneralPermission_Item_View_UI($CategoryID, $Category2ID, $pageNo, $numPerPage);

?>

<script language="javascript">
var jsCurCategoryID = '<?=$CategoryID?>';
var jsCurCategory2ID = '<?=$Category2ID?>';
var jsPageNo = "<?=$pageNo?>";
var jsNumPerPage = "<?=$numPerPage?>";

var arrCookies = new Array();
arrCookies[arrCookies.length] = "CategoryID_eBooking_Settings_GeneralPermission_Item_View";
arrCookies[arrCookies.length] = "Category2ID_eBooking_Settings_GeneralPermission_Item_View";
arrCookies[arrCookies.length] = "pageNo_eBooking_Settings_GeneralPermission_Item_View";
arrCookies[arrCookies.length] = "numPerPage_eBooking_Settings_GeneralPermission_Item_View";

$(document).ready( function() {	
	$('select#CategoryID').focus();
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
});

function js_Go_New_Item()
{
	jsCurCategoryID = $('select#CategoryID').val();
	jsCurCategory2ID = $('select#Category2ID').val();
	jsCurCategory2ID = jsCurCategory2ID || '';
	
	AssignCookies();
	window.location = 'new.php?CategoryID=' + jsCurCategoryID + '&Category2ID=' + jsCurCategory2ID;
}

function js_Go_To_Item_Details(jsItemID)
{
	jsCurCategoryID = $('select#CategoryID').val();
	jsCurCategory2ID = $('select#Category2ID').val();
	jsCurCategory2ID = jsCurCategory2ID || '';
	
	AssignCookies();
	window.location = 'view_item_details.php?CategoryID=' + jsCurCategoryID + '&Category2ID=' + jsCurCategory2ID + '&ItemID=' + jsItemID;
}

function Go_Edit_Mode()
{
	var jsNumOfItem = $('input#NumOfItem').val();
	if (jsNumOfItem == '' || jsNumOfItem == null || jsNumOfItem <= 0)
	{
		alert('<?=$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['CannotGoToEditMode']?>');
		return;
	}
	
//	AssignCookies();
	
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'edit_items.php';
	ObjForm.submit();
}

function js_Reload_SubCategory_Selection(jsCategoryID, jsCategory2ID)
{
	jsCategory2ID = jsCategory2ID || '';
	
	if (jsCategoryID != '')
	{
		$('#SubCategorySelectionDiv').html('').load(
			"ajax_reload.php", 
			{ 
				Action: 'Reload_SubCategory_Selection',
				CategoryID: jsCategoryID,
				Category2ID: jsCategory2ID,
				OnChange: 'js_Reload_Item_Table();',
				ID_Name: 'Category2ID',
				isAll: 1
			},
			function(ReturnData)
			{
				js_Reload_Item_Table();
				//$('#IndexDebugArea').html(ReturnData);
			}
		);
	}
	else
	{
		$('div#ItemListDiv').html('');
		$('div#SubCategorySelectionDiv').html('');
		js_Reload_Item_Table();
	}
	
	AssignCookies();
}

function js_Reload_Item_Table()
{
	jsCurCategoryID = $('select#CategoryID').val();
	jsCurCategory2ID = $('select#Category2ID').val();
	
	$('div#ItemListDiv').html('');
//	Block_Element('ItemListDiv');
	
	// Reset the table to page 1
	$.cookies.set('pageNo_eBooking_Settings_GeneralPermission_Item_View', '1');
	jsPageNo = 1;

	$('div#ItemListDiv').load(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Item_View_Table',
			CategoryID: jsCurCategoryID,
			Category2ID: jsCurCategory2ID,
			PageNo : jsPageNo,
			NumPerPage: jsNumPerPage
		},
		function(ReturnData)
		{
			UnBlock_Element('ItemListDiv');
			//$('#IndexDebugArea').html(ReturnData);
		}
	);
	
//	SetCookies();
//	AssignCookies();
}

function js_Show_Detail_Layer(jsDetailType, jsItemID, jsClickedObjID)
{
	js_Hide_Detail_Layer();
	
	var jsAction = '';
	if (jsDetailType == 'MgmtGroup'){
		jsAction = 'Reload_Item_MgmtGroup_Layer';
		jsOffsetLeft = 450;
		jsOffsetTop = -15;
	}	
	else if (jsDetailType == 'FollowUpGroup'){
		jsAction = 'Reload_Item_FollowUpGroup_Layer';
		jsOffsetLeft = 450;
		jsOffsetTop = -15;
	}	
	else if (jsDetailType == 'UserBookingRule'){
		jsAction = 'Reload_Item_UserBookingRule_Layer';
		jsOffsetLeft = 450;
		jsOffsetTop = -15;
	}else if (jsDetailType == 'ShowItemDescription'){
		jsAction = 'ShowItemDescription';
		jsOffsetLeft = 0;
		jsOffsetTop = -10;
	}
	
	$('div#DetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
	js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop);
	MM_showHideLayers('DetailsLayer','','show');
			
	$('div#DetailsLayerContentDiv').load(
		"ajax_reload.php", 
		{ 
			Action: jsAction,
			ItemID: jsItemID
		},
		function(returnString)
		{
			$('div#DetailsLayerContentDiv').css('z-index', '999');
		}
	);
}

function js_Hide_Detail_Layer()
{
	MM_showHideLayers('DetailsLayer','','hide');
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop) 
{		
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
	
	document.getElementById('DetailsLayer').style.left = posleft + "px";
	document.getElementById('DetailsLayer').style.top = postop + "px";
	document.getElementById('DetailsLayer').style.visibility = 'visible';
}

function js_Import_From_Inventory()
{
//	if(confirm("<?=$Lang['eBooking']['jsConfirmMsg']['ImportItemFromInventory']?>"))
//	{
		window.location="import_from_inventory_confirm.php"		
//	}
}
function js_import_new_items(){
	window.location="import.php";
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>