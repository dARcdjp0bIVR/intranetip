<?php
// using: 
/********************************** Change Log *******************************************************
 * 2011-03-16 (Carlos): Fix wrongly getting cookie/post values problem when switching View / Edit mode 
 *****************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

$ModuleName = $_POST['ModuleName'];
$targetBuilding = !isset($_POST['targetBuilding'])?$_COOKIE['targetBuilding']:$_POST['targetBuilding'];
$targetFloor = !isset($_POST['targetFloor'])?$_COOKIE['targetFloor']:$_POST['targetFloor'];
$targetRoom = !isset($_POST['targetRoom'])?$_COOKIE['targetRoom']:$_POST['targetRoom'];

if($ModuleName == "Floor")
{
	$arrFloor = $lebooking->Get_All_Allow_Booking_Floor_Array($targetBuilding);
	//$floor_selection = getSelectByArray($arrFloor, " name='targetFloor' id='targetFloor' onChange='this.form.submit();'",$targetFloor,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Floor']." -- ");
	
	if($targetBuildiing == "")
	{
		$targetFloor = "";
	}
	
	if($targetFloor == "")
	{
		$targetFloor = $_POST['targetFloor'];
	}
	
	if($disabled)
		$floor_selection = getSelectByArray($arrFloor, " name='targetFloor' id='targetFloor' onChange='javascript: Reload_Location_Selection(\"Room\",this.value); reloadMainContent();' DISABLED",$targetFloor,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Floor']." -- ");
	else
		$floor_selection = getSelectByArray($arrFloor, " name='targetFloor' id='targetFloor' onChange='javascript: Reload_Location_Selection(\"Room\",this.value); reloadMainContent();'",$targetFloor,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Floor']." -- ");
	
	echo $floor_selection;
}
else if($ModuleName == "Room")
{
	$arrRoom = $lebooking->Get_All_Allow_Booking_Room_Array($targetBuilding,$targetFloor);
	//$room_selection = getSelectByArray($arrRoom, " name='targetRoom' id='targetRoom' onChange='this.form.submit();'",$targetRoom,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Room']." -- ");
	
	if($disabled)
		$room_selection = getSelectByArray($arrRoom, " name='targetRoom' id='targetRoom' onChange='javascript: reloadMainContent()' DISABLED",$targetRoom,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Room']." -- ");
	else
		$room_selection = getSelectByArray($arrRoom, " name='targetRoom' id='targetRoom' onChange='javascript: reloadMainContent()'",$targetRoom,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Room']." -- ");
	
	echo $room_selection;
}

intranet_closedb();
?>