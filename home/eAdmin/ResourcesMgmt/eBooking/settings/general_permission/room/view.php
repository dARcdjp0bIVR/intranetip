<?php
// using : 
/********************************** Change Log *******************************************************
 * 2011-03-16 (Carlos): Fix wrongly getting cookie/post values problem when switching View / Edit mode 
 *****************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("targetBuilding", "targetBuilding");
$arrCookies[] = array("targetFloor", "targetFloor");
$arrCookies[] = array("targetRoom", "targetRoom");
$arrCookies[] = array("pageNo_eBooking_Settings_GeneralPermission_Room_View", "pageNo_eBooking_Settings_GeneralPermission_Room_View");
$arrCookies[] = array("numPerPage_eBooking_Settings_GeneralPermission_Room_View", "numPerPage_eBooking_Settings_GeneralPermission_Room_View");

$targetBuilding = (isset($_POST['targetBuilding']))? $_POST['targetBuilding'] : $targetBuilding;
$targetFloor 	= (isset($_POST['targetFloor']))? $_POST['targetFloor'] : $targetFloor;
$targetRoom 	= (isset($_POST['targetRoom']))? $_POST['targetRoom'] : $targetRoom;
$pageNo 		= (isset($_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View']) && $_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'] != '')? $_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'] : $pageNo_eBooking_Settings_GeneralPermission_Room_View;
$numPerPage 	= (isset($_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View']) && $_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View'] != '')? $_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View'] : $numPerPage_eBooking_Settings_GeneralPermission_Room_View;
$action			= (isset($action) && $action != "") ? $action : "";

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$targetBuilding = '';
	$targetFloor = '';
	$targetRoom = '';
	$pageNo = '';
	$numPerPage = '';
}
else 
{
	if(isset($_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View']))
		$pageNo_eBooking_Settings_GeneralPermission_Room_View = $_POST['pageNo_eBooking_Settings_GeneralPermission_Room_View'];
	
	if(isset($_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View']))
		$numPerPage_eBooking_Settings_GeneralPermission_Room_View = $_POST['numPerPage_eBooking_Settings_GeneralPermission_Room_View'];
		
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$linventory		= new libinventory();
$li 			= new libdbtable2007($SortField, $Order, $PageNumber);

$lebooking->Check_Page_Permission();

# Default Table Settings
$pageNo = ($pageNo == '')? $li->pageNo=1 : $li->pageNo=$pageNo;
$numPerPage = ($numPerPage == '')? $li->page_size=20 : $li->page_size=$numPerPage;
$Order = ($Order == '')? 1 : $Order;
$SortField = ($SortField == '')? 0 : $SortField;

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'], "view.php?clearCoo=1", 1);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'], "../item/view.php?clearCoo=1", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arrBuilding = $lebooking->Get_All_Allow_Booking_Building_Array();
$building_selection = getSelectByArray($arrBuilding, " name='targetBuilding' id='targetBuilding' onChange='javascript: Reload_Location_Selection(\"Floor\",this.value); '",$targetBuilding,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Building']." -- ");
$building_filter .= "<div id='BuildingDiv' style='display: inline;'>".$building_selection."</div>";

$floor_filter .= "<div id='FloorDiv' style='display: inline;'>".$floor_selection."</div>";

$room_filter .= "<div id='RoomDiv' style='display: inline;'>".$room_selection."</div>";

$ModeArr = '';
$ModeArr[] = array($Lang['Btn']['View'], "icon_view.gif", "", 1);
$ModeArr[] = array($Lang['Btn']['Edit'], "icon_edit_b.gif", "javascript:Go_Edit_Mode();", 0);
$mode_toolbar = $lebooking_ui->Get_Mode_Toolbar($ModeArr);

$toolbar .= "<table width='96%' border='0' cellpadding='3' cellspacing='0'>";
$toolbar .= "<tr>";
$toolbar .= "<td align='left'>".$building_filter.$floor_filter.$room_filter."</td>";
$toolbar .= "</tr>";
$toolbar .= "</table>";

$table_content = $lebooking_ui->getRoomPermissionGeneralViewTable($targetBuilding,$targetFloor,$targetRoom,$pageNo,$numPerPage);
echo $lebooking_ui->initJavaScript();
?>

<script language='javascript'>
	
	var targetBuilding = "<?=$targetBuilding;?>";
	var targetFloor = "<?=$targetFloor;?>";
	var targetRoom = "<?=$targetRoom;?>";
	var action = "<?=$action;?>";
	
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "targetBuilding";
	arrCookies[arrCookies.length] = "targetFloor";
	arrCookies[arrCookies.length] = "targetRoom";
	arrCookies[arrCookies.length] = "pageNo_eBooking_Settings_GeneralPermission_Room_View";
	arrCookies[arrCookies.length] = "numPerPage_eBooking_Settings_GeneralPermission_Room_View";
	
	$(document).ready( function() {
		SetCookies();
		<? if($clearCoo) { ?>
			for(i=0; i<arrCookies.length; i++)
			{
				var obj = arrCookies[i];
				$.cookies.del(obj);
			}
		<? } ?>

		if(action != "") {
			Reload_Location_Selection("Floor",targetBuilding,targetFloor);
			Reload_Location_Selection("Room",targetFloor,targetRoom);
		}
		else
		{
			if((targetBuilding != "") && (targetFloor != "")) {
				Reload_Location_Selection("Floor",targetBuilding,targetFloor);
				Reload_Location_Selection("Room",targetFloor,targetRoom);
			}
		}
	});
	
	function jsViewDetails(BuildingID, LocationID, SubLocationID)
	{
		//if((LocationID != "") && (SubLocationID != ""))
		//{
			//window.location = 'view_details.php?building_id='+BuildingID+'&location_id='+LocationID+'&sub_location_id='+SubLocationID;
		//}
		SubLocationID = SubLocationID.toString();
		$.cookies.set("SubLocationID",SubLocationID);
		
		// Set the Cookies
		AssignCookies();
		
		window.location = 'view_details.php';
	}
	
	function js_Show_Detail_Layer(jsDetailType, jsSubLocationID, jsClickedObjID)
	{
		js_Hide_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'ShowFacilities'){
			jsAction = 'Show_Facilities';
			jsOffsetLeft = 450;
			jsOffsetTop = -15;
		}
		else if(jsDetailType == 'ShowManagement'){
			jsAction = 'Show_Management';
			jsOffsetLeft = 450;
			jsOffsetTop = -15;
		}	
		else if(jsDetailType == 'ShowFollowUp'){
			jsAction = 'Show_FollowUp';
			jsOffsetLeft = 450;
			jsOffsetTop = -15;
		}	
		else if(jsDetailType == 'ShowBookingRule'){
			jsAction = 'Show_BookingRule';
			jsOffsetLeft = 450;
			jsOffsetTop = -15;
		}
		else if(jsDetailType == 'ShowLocationDescription'){
			jsAction = 'ShowLocationDescription';
			jsOffsetLeft = 0;
			jsOffsetTop = -10;
		}
			
			
		$('div#DetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>');	
		js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop);
		MM_showHideLayers('DetailsLayer','','show');
				
		$('div#DetailsLayerContentDiv').load(
			"ajax_reload.php", 
			{ 
				Action: jsAction,
				SubLocationID: jsSubLocationID
			},
			function(returnString)
			{
				$('div#DetailsLayerContentDiv').css('z-index', '999');
				$('div#DetailsLayerContentDiv').css('overflow', 'auto');
				if($('div#DetailsLayerContentDiv').height() > 400) {
					$('div#DetailsLayerContentDiv').css('height', '400px');
				} else {
					$('div#DetailsLayerContentDiv').css('height', 'auto');
				}
			}
		);
	}
	
	function js_Hide_Detail_Layer()
	{
		MM_showHideLayers('DetailsLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID,jsOffsetLeft,jsOffsetTop) 
	{
		var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
		
		document.getElementById('DetailsLayer').style.left = posleft + "px";
		document.getElementById('DetailsLayer').style.top = postop + "px";
		document.getElementById('DetailsLayer').style.visibility = 'visible';
	}
	
	function Go_Edit_Mode()
	{
		var targetBuilding = $('#targetBuilding').val();
		
		if($('#targetFloor').length != 0)
			var targetFloor = $('#targetFloor').val();
		else
			var targetFloor = "#";
		
		if($('#targetRoom').length != 0)
			var targetRoom = $('#targetRoom').val();
		else
			var targetRoom = "#";
		
		// Set the Cookies
		AssignCookies();
		
		document.form1.action = "edit.php";
		document.form1.submit();
		// Redirect
		//window.location = 'edit.php';
	}
	
	function reloadMainContent(targetBuilding, targetFloor, targetRoom, pageNo, numPerPage, Order, SortField)
	{
		targetBuilding = targetBuilding || $('#targetBuilding').val();
		
		if(targetBuilding != "")
			targetFloor = targetFloor || $('#targetFloor').val();
		else
			targetFloor = "";
		
		if($('#pageNo_eBooking_Settings_GeneralPermission_Room_View').length)
			pageNo = pageNo || $('#pageNo_eBooking_Settings_GeneralPermission_Room_View').val();
		else
			pageNo = 1; 
			
		if($('#numPerPage_eBooking_Settings_GeneralPermission_Room_View').length)	
			numPerPage = numPerPage || $('#numPerPage_eBooking_Settings_GeneralPermission_Room_View').val();
		else
			numPerPage = 20;
		
		if($('#targetRoom').length == 0) {
			targetRoom = targetRoom || '';
		} else {
			targetRoom = $('#targetRoom').val();
		}
		
		// Assign Cookies
		AssignCookies();
		// Set Cookies
		SetCookies();
		
		if(targetBuilding == ""){
			$.cookies.del("targetFloor");
			$.cookies.del("targetRoom");
		}
		
		Block_Document();
		$.post(
			"ajax_load_room_permission_view.php",
			{
				"targetBuilding":targetBuilding,
				"targetFloor":targetFloor,
				"targetRoom":targetRoom,
				"pageNo":pageNo, 
				"numPerPage":numPerPage
			},
			function(responseText){
				$('#room_permission_general_view').html(responseText);
				UnBlock_Document();
				initThickBox();
			}
		);
	}
	
	function Reload_Location_Selection(ModuleName,val,selected_val)
	{
		selected_val = selected_val || '';
		
		// Set Cookies
		SetCookies();
		// Assign Cookies
		AssignCookies();
		
//		Block_Document();
		
		if(ModuleName == "Floor")
		{
			$("#FloorDiv").hide();
			$("#RoomDiv").hide();
			
			if(val != "")
			{
				$.post(
					"ajax_load_location_selection.php",
					{
						"ModuleName" : ModuleName,
						"targetBuilding" : val,
						"targetFloor" : selected_val,
						"disabled" : 0
					},
					function(responseText){
						$("#room_permission_general_view").html();
						$("#FloorDiv").show();
						$("#FloorDiv").html(responseText);
						reloadMainContent();
//						UnBlock_Document();
						initThickBox();
					}
				);
			}
			else
			{
				//$.cookies.set("targetBuilding","-1");
				//$.cookies.set("targetFloor","-1");
				//$.cookies.set("targetRoom","-1");
			}
			
		}
		else if(ModuleName == "Room")
		{
			if($('#targetRoom').length != 0)
				$('#targetRoom').val('');
				
			var targetBuilding = $('#targetBuilding').val();
			$.post(
				"ajax_load_location_selection.php",
				{
					"ModuleName" : ModuleName,
					"targetBuilding" : targetBuilding,
					"targetFloor" : val,
					"targetRoom" : selected_val,
					"disabled" : 0
				},
				function(responseText){
					$("#RoomDiv").show();
					$("#RoomDiv").html(responseText);
					reloadMainContent();
//					UnBlock_Document();
					initThickBox();
					
				}
			);
		}
	}
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				if( ($('#'+arrCookies[i]).is(':hidden')) && (arrCookies[i]!='pageNo_eBooking_Settings_GeneralPermission_Room_View') )
				{
					$.cookies.set(arrCookies[i],"");
				}
				else
				{
					$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
				}
			}
			else
			{
				//alert("B: "+$.cookies.get(arrCookies[i]));
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
	
</script>

<form name="form1" action="view.php" method="POST" >
	<br>
	<?=$toolbar;?>
	<div id="room_permission_general_view">
		<?=$table_content;?>
	</div>
	
	<div id="DetailsLayer" class="selectbox_layer" style="visibility:hidden; width:500px; overflow:auto;">
		<table cellspacing="0" cellpadding="3" border="0" width="100%">
			<tbody>
				<tr>
					<td align="left" style="border-bottom: medium none;">
						<div id="DetailsLayerContentDiv"></div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</form>

<?
$linterface->LAYOUT_STOP();
?>