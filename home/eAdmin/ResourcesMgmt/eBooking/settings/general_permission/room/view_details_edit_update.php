<?php
// using :
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_GeneralPermission";
$li = new libdb();
$libebooking = new libebooking();

### Update Allow Booking
$sql = "UPDATE INVENTORY_LOCATION SET AllowBooking = '$AllowBooking' WHERE LocationID = '$RoomID'";
$result[] = $li->db_db_query($sql);

### Update User Booking Rule
$sql = "DELETE FROM INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION WHERE LocationID = '$RoomID'";
$li->db_db_query($sql);
for($i=0; $i<sizeof($UserBookingRule); $i++)
{
	$BookingRuleID = $UserBookingRule[$i];
	$sql = "INSERT INTO 
				INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION 
				(LocationID,UserBookingRuleID,InputBy,ModifiedBy,DateInput,DateModified)
			VALUES
				('$RoomID','$BookingRuleID','$UserID','$UserID',NOW(),NOW())";
	$result[] = $li->db_db_query($sql);
}

### Update Location Booking Rule
//$sql = "SELECT BookingRuleID FROM INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION WHERE LocationID = $RoomID";
//$arrLocationBookingRuleID = $li->returnVector($sql);
//$LocationBookingRuleID = $arrLocationBookingRuleID[0];
//$sql = "UPDATE INTRANET_EBOOKING_LOCATION_BOOKING_RULE SET DaysBeforeUse = '$DayBeforeBooking' WHERE RuleID = $LocationBookingRuleID";
//$result[] = $li->db_db_query($sql);
$result[] = $libebooking->Insert_Update_Location_Booking_Rule($RoomID, $DayBeforeBooking);

### Update Location Mgmt Group 
$sql = "DELETE FROM INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION WHERE LocationID = '$RoomID'";
$li->db_db_query($sql);
for($i=0; $i<sizeof($MgmtGroup); $i++)
{
	$mgmt_group_id = $MgmtGroup[$i];
	$sql = "INSERT INTO 
				INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION 
				(LocationID,GroupID,InputBy,ModifiedBy,DateInput,DateModified)
			VALUES
				('$RoomID','$mgmt_group_id','$UserID','$UserID',NOW(),NOW())";
	$result[] = $li->db_db_query($sql);
}

### Update Location Follow-up Group
$sql = "DELETE FROM INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION WHERE LocationID = '$RoomID'";
$li->db_db_query($sql);
for($i=0; $i<sizeof($FollowUpGroup); $i++)
{
	$follow_up_group_id = $FollowUpGroup[$i];
	$sql = "INSERT INTO 
				INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION 
				(LocationID,GroupID,DateInput,InputBy,DateModified,ModifiedBy)
			VALUES
				('$RoomID','$follow_up_group_id',NOW(),'$UserID',NOW(),'$UserID')";
	 $result[] = $li->db_db_query($sql);
}

### Upload Attachment
if($Attachment!=""){
	if($_FILES["Attachment"]["error"]>0){
		$result['Upload_Attachment'] = false;
	}else{
		
		$attachment_location = $_FILES["Attachment"]["tmp_name"];
		$attachment_name = stripslashes(trim($_FILES["Attachment"]["name"]));
		$result['Upload_Attachment'] = $libebooking->Upload_Room_Attachment($RoomID,$attachment_location,$attachment_name);
	}
}

if(!in_array(false,$result)){
	header("location: view_details.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditGeneralPermissionSuccessfully']));
}else{
	header("location: view_details.php?msg=".rawurlencode($Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditGeneralPermissionSuccessfully']));
}
exit();
intranet_closedb();
?>