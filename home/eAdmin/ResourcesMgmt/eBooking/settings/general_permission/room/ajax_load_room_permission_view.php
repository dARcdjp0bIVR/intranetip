<?php
// using : 
/********************************** Change Log *******************************************************
 * 2011-03-16 (Carlos): Fix wrongly getting cookie/post values problem when switching View / Edit mode 
 *****************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$lebooking_ui = new libebooking_ui();

// Get data from POST, not COOKIES
$targetBuilding = !isset($_POST['targetBuilding'])?$_COOKIE['targetBuilding']:$_POST['targetBuilding'];
$targetFloor = !isset($_POST['targetFloor'])?$_COOKIE['targetFloor']:$_POST['targetFloor'];
$targetRoom = !isset($_POST['targetRoom'])?$_COOKIE['targetRoom']:$_POST['targetRoom'];
$pageNo = $_POST['pageNo'];
$numPerPage = $_POST['numPerPage'];

echo $lebooking_ui->getRoomPermissionGeneralViewTable($targetBuilding,$targetFloor,$targetRoom,$pageNo,$numPerPage);

intranet_closedb();
?>