<?php
// using :  

############################################
##
##  Date    :   2019-05-13 Cameron
##              - fix potential sql injection problem by enclosing var with apostrophe
##
##  Date    :   2018-09-04 (Cameron) [ip.2.5.9.10.1]
##              add "Apply to Selected Location" button to ShowEdit_AllRoomAvaliableBooking, ShowEdit_AllRoomBookingRule, ShowEdit_AllManagementGroup, ShowEdit_AllFollowUpGroup, ShowEdit_AllUserBookingRule
##
##  Date    :   2018-08-31 (Cameron) [ip.2.5.9.10.1]
##              add close button to ShowEdit_ManagmentGroup, ShowEdit_FollowUpGroup, ShowEdit_UserBookingRule
##
##	Date	:	2017-07-17 (Villa)
##				add DisableBooking for booking permission
##
##	Date	:	2011-06-23 (YatWoon)
##				update Show_BookingRule, Display exactly allow booking days number
##
############################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lebooking		= new libebooking();
$linventory 	= new libinventory();


$Action = trim($Action);
if($_POST['SubLocationID'] != "")
{
	$SubLocationID = $_POST['SubLocationID'];
}

if($Action == 'Show_Facilities')
{
	$layer_content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr><td align='right'><a href='javascript:js_Hide_Detail_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
	$layer_content .= "<tr class='tabletop'>";
	$layer_content .= "<td width='1'>#</td>";
	$layer_content .= "<td width='20%'>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item']."</td>";
	$layer_content .= "<td width='20%'>".$Lang['eBooking']['Settings']['FieldTitle']['Category']."</td>";
	$layer_content .= "<td width='20%'>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Quantity']."</td>";
	$layer_content .= "<td width='20%'>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['IncludedWhenBooking']."</td>";
	$layer_content .= "<td width='20%'>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['ShowForReference']."</td>";
	$layer_content .= "</tr>";
	
//	$sql = "
//		(
//			SELECT 
//				item.ItemID, 
//				2 AS ItemType, 
//				".$linventory->getInventoryNameByLang("item.").", 
//				CONCAT(
//					".$linventory->getInventoryNameByLang("cat.").",
//					' > ',
//					".$linventory->getInventoryNameByLang("sub_cat.")."
//				), 
//				bulk_location.Quantity, 
//				IncludedWhenBooking, 
//				ShowForReference 
//			FROM 
//				INVENTORY_ITEM AS item 
//				INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS bulk_location ON (item.ItemID = bulk_location.ItemID) 
//				LEFT OUTER JOIN INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION AS relation ON (relation.LocationID = $SubLocationID AND item.ItemID = relation.BulkItemID) 
//				INNER JOIN INVENTORY_CATEGORY AS cat ON (item.CategoryID = cat.CategoryID) 
//				INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS sub_cat ON (item.Category2ID = sub_cat.Category2ID) 
//			WHERE 
//				item.ItemType = 2 
//				AND item.RecordStatus = 1 
//				AND bulk_location.LocationID = $SubLocationID
//				AND bulk_location.Quantity >0
//		) 
//		UNION 
//		(
//			SELECT 
//				ItemID, 
//				1 AS ItemType, 
//				".$linventory->getInventoryNameByLang("item.").", 
//				CONCAT(
//					".$linventory->getInventoryNameByLang("cat.").",
//				' > ',
//				".$linventory->getInventoryNameByLang("sub_cat.")."
//				), 
//				1 AS Quantity, 
//				IncludedWhenBooking, 
//				ShowForReference 
//			FROM 
//				INTRANET_EBOOKING_ITEM AS item 
//				INNER JOIN INVENTORY_CATEGORY AS cat ON (item.CategoryID = cat.CategoryID) 
//				INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS sub_cat ON (item.Category2ID = sub_cat.Category2ID) 
//			WHERE 
//				LocationID = $SubLocationID 
//				AND	AllowBooking = 1
//		)";
//	$arrItemList = $lebooking->returnArray($sql,6);
	$arrItemList = $lebooking->Get_Room_Item_Info($SubLocationID);
	
	if(sizeof($arrItemList)>0) {
		for($a=0; $a<sizeof($arrItemList); $a++) {
			list($ItemID, $ItemType, $ItemName, $ItemCat, $Qty, $IncludedWhenBooking, $ShowForReference) = $arrItemList[$a];
			if($IncludedWhenBooking)
				$IncludedWhenBooking_Check = " CHECKED ";
			else
				$IncludedWhenBooking_Check = " ";
			if($ShowForReference)
				$ShowForReference_Check = " CHECKED ";
			else
				$ShowForReference_Check = " ";
			$layer_content .= "<tr class='row_approved'>";
			$layer_content .= "<td >".($a+1)."</td>";
			$layer_content .= "<td >$ItemName</td>";
			$layer_content .= "<td >$ItemCat</td>";
			$layer_content .= "<td >$Qty</td>";
			if($ItemType == 1) {
				//$layer_content .= "<td ><input type='checkbox' name='Single_IncludeWhenBooking[]' value='$ItemID' $IncludedWhenBooking_Check></td>";
				$layer_content .= "<td > - </td>";
				$layer_content .= "<td ><input type='checkbox' name='Single_ShowForReference[]' value='$ItemID' $ShowForReference_Check DISABLED></td>";
				$layer_content .= "<input type='hidden' name='SingleItemID[]' value='$ItemID'>";
			} else {
				$layer_content .= "<td ><input type='checkbox' name='Bulk_IncludeWhenBooking[]' value='$ItemID' $IncludedWhenBooking_Check DISABLED></td>";
				//$layer_content .= "<td ><input type='checkbox' name='Bulk_ShowForReference[]' value='$ItemID' $ShowForReference_Check></td>";
				$layer_content .= "<td > - </td>";
				$layer_content .= "<input type='hidden' name='BulkItemID[]' value='$ItemID'>";
			}
			$layer_content .= "</tr>";
		}
	} else {
		$layer_content .= "<tr class='row_approved'><td colspan='6' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='6' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "Show_Management")
{
	$layer_content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr><td align='right'><a href='javascript:js_Hide_Detail_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
	$layer_content .= "<tr class='tabletop'>";
	$layer_content .= "<td>#</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['ManagementGroup']['GroupName']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['ManagementGroup']['Description']."</td>";
	$layer_content .= "</tr>";
	
	$sql = "SELECT management_group.GroupID, management_group.GroupName, management_group.Description FROM INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS relation INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS management_group ON (relation.GroupID = management_group.GroupID) WHERE relation.LocationID = '$SubLocationID'";
	$arr_group_result = $lebooking->returnArray($sql,3);
	
	if(sizeof($arr_group_result)>0){
		for($a=0; $a<sizeof($arr_group_result); $a++){
			list($group_id, $group_name, $group_desc) = $arr_group_result[$a];
			$layer_content .= "<tr class='row_approved'>";
			$layer_content .= "<td>".($a+1)."</td>";
			$layer_content .= "<td>".$group_name."</td>";
			$layer_content .= "<td>".$group_desc."</td>";
			$layer_content .= "</tr>";
		}
	} else {
		$layer_content .= "<tr><td colspan='3' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='3' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "Show_FollowUp")
{
	$layer_content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr><td align='right'><a href='javascript:js_Hide_Detail_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
	$layer_content .= "<tr class='tabletop'>";
	$layer_content .= "<td>#</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['FollowUpGroup']['GroupName']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['FollowUpGroup']['Description']."</td>";
	$layer_content .= "</tr>";
	
	$sql = "SELECT follow_up.GroupID, follow_up.GroupName, follow_up.Description FROM INTRANET_EBOOKING_FOLLOWUP_GROUP as follow_up INNER JOIN INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION as relation ON (follow_up.GroupID = relation.GroupID) WHERE relation.LocationID = '$SubLocationID'";
	$arr_group_result = $lebooking->returnArray($sql,3);
	
	if(sizeof($arr_group_result)>0){
		for($a=0; $a<sizeof($arr_group_result); $a++){
			list($group_id, $group_name, $group_desc) = $arr_group_result[$a];
			$layer_content .= "<tr class='row_approved'>";
			$layer_content .= "<td>".($a+1)."</td>";
			$layer_content .= "<td>".$group_name."</td>";
			$layer_content .= "<td>".$group_desc."</td>";
			$layer_content .= "</tr>";
		}
	} else {
		$layer_content .= "<tr><td colspan='3' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='3' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "Show_BookingRule")
{
	$layer_content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr><td align='right'><a href='javascript:js_Hide_Detail_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
	$layer_content .= "<tr class='tabletop'>";
	$layer_content .= "<td>#</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['RuleTitle']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserType']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Target']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['DisableBooking']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NeedApproval']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingWithin']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['CanBookForOthers']."</td>";
	$layer_content .= "</tr>";
	
	$sql = "SELECT 
				rule.RuleID, rule.RuleName, rule.DisableBooking, rule.NeedApproval, rule.DaysBeforeUse, rule.CanBookForOthers
			FROM
				INTRANET_EBOOKING_USER_BOOKING_RULE AS rule 
				INNER JOIN INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION as relation ON (rule.RuleID = relation.UserBookingRuleID)
			WHERE 
				relation.LocationID = '$SubLocationID'";
	$arrUserRule = $lebooking->returnArray($sql,8);
	
	if(sizeof($arrUserRule)>0){
		for($a=0; $a<sizeof($arrUserRule); $a++){
			list($rule_id, $rule_name, $DisableBooking, $need_approval, $day_before_use, $can_book_for_others ) = $arrUserRule[$a];
			
			if($need_approval)
				$txtNeedApproval = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Yes'];
			else
				$txtNeedApproval = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['No'];
												
			if($can_book_for_others)
				$txtCanBookForOthers = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Yes'];
			else
				$txtCanBookForOthers = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['No'];
			
			$sql = "SELECT target_user.UserType, IFNULL(year.YearName,' - ') FROM INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER AS target_user LEFT OUTER JOIN YEAR AS year on (target_user.YearID = year.YearID) WHERE target_user.RuleID = '$rule_id'";
			$arrRuleTargetUser = $lebooking->returnArray($sql,2);
												
			unset($arrTargetUser);
			unset($arrTargetUserForm);
			for($b=0; $b<sizeof($arrRuleTargetUser); $b++)
			{
				list($target_user,$target_form) = $arrRuleTargetUser[$b];
				$arrTargetUser[] = $target_user;
				$arrTargetUserForm[$target_user][] = $target_form;
			}
			$TargetUser = $arrTargetUser[0];
			switch ($TargetUser){
				case 1:
					$txt_TargetUser = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['TeacherAndStaff'];
					break;
				case 2: 
					$txt_TargetUser = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Students'];
					break;
				case 3:
					$txt_TargetUser = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Parents'];
					break;
			}
			if(is_array($arrTargetUserForm[$TargetUser]))
				$TargetUserForm = implode(", ",$arrTargetUserForm[$TargetUser]);
			else
				$TargetUserForm = " - ";
			/*
			switch($day_before_use){
				case 0:
					$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NoLimit'];
					break;
				case 7:
					$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][1];
					break;
				case 30:
					$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][2];
					break;
				case 365:
					$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][3];
					break;
			}
			*/
			if($day_before_use)
			{
				if($_SESSION['intranet_session_language'] == "en")
					$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Within']." ". $day_before_use . " " . $Lang['eBooking']['WithinDays'];
				else
					$txt_day_before_use = $day_before_use." ". $Lang['eBooking']['WithinDays'];
			}
			else
			{
				$txt_day_before_use = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NoLimit'];
			}
			if($DisableBooking){
				$txtDisableBooking =  $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Yes'];
				$txtNeedApproval = Get_String_Display('');
				$txt_day_before_use= Get_String_Display('');
				$txtCanBookForOthers= Get_String_Display('');
			}else{
				$txtDisableBooking = $Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['No'];
			}
			
			$layer_content .= "<tr class='row_approved'>";
			$layer_content .= "<td>".($a+1)."</td>";
			$layer_content .= "<td>$rule_name</td>";
			$layer_content .= "<td>$txt_TargetUser</td>";
			$layer_content .= "<td>$TargetUserForm</td>";
			$layer_content .= "<td>$txtDisableBooking</td>";
			$layer_content .= "<td>$txtNeedApproval</td>";
			$layer_content .= "<td>".$txt_day_before_use."</td>";
			$layer_content .= "<td>$txtCanBookForOthers</td>";
			$layer_content .= "</tr>";
		}
	} else {
		$layer_content .= "<tr><td colspan='7' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='7' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_AllRoomAvaliableBooking")
{	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>
						<tbody>
						<tr class='row_approved'>
							<td nowrap style='border-bottom:none' class='tabletext' width='5%'><input type='radio' name='All_AvaliableBooking' id='All_AvaliableBooking_Yes' value='1'></td>
							<td nowrap style='border-bottom:none' class='tabletext' align='left'><label for='All_AvaliableBooking_Yes'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_tick_green.gif' border='0'>".$Lang['eBooking']['Settings']['GeneralPermission']['Available']."</lable></td>
						</tr>
						<tr class='row_approved'>
							<td nowrap style='border-bottom:none' class='tabletext' width='5%'><input type='radio' name='All_AvaliableBooking' id='All_AvaliableBooking_No' value='0'></td>
							<td nowrap style='border-bottom:none' class='tabletext' align='left'><label for='All_AvaliableBooking_No'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_norecord_bw.gif' border='0'>".$Lang['eBooking']['Settings']['GeneralPermission']['NotAvailable']."</lable></td>
						</tr>
						<tr height='10px'>
							<td></td>
						</tr>
						<tr>
							<td align='center' colspan='2'>".$linterface->GET_SMALL_BTN($Lang['Btn']['ApplyToAll'],"Button","javascript:ApplyToAll('All_AvaliableBooking')").
							"&nbsp;".$linterface->GET_SMALL_BTN($Lang['eBooking']['Settings']['GeneralPermission']['ApplyToSelected'],"Button","javascript:ApplyToSelected('SelectedAvaliableBooking')").
							"&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>
						</tr>
						</tbody>
					</table>";
	
}
else if($Action == "ShowEdit_AllRoomBookingRule")
{
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr class='row_approved'><td>";
	$layer_content .= "<input id='AllBookingDay' name='AllBookingDay' class='textboxnum' maxlength=11> ".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Days'];
	$layer_content .= "<div id='WarnAllBookingDay'></div>";
//	$layer_content .= "<select name='AllBookingDay' id='AllBookingDay'>";
//	for($i=0; $i<sizeof($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay']); $i++) 
//	{
//		$layer_content .= "<option value=".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'][$i][0].">".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'][$i][1];
//	}
//	$layer_content .= "</select>";
	$layer_content .= "</td></tr>";
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center'>".$linterface->GET_SMALL_BTN($Lang['Btn']['ApplyToAll'],"Button","javascript:ApplyToAll('AllRoomBookingRule')").
    	"&nbsp;".$linterface->GET_SMALL_BTN($Lang['eBooking']['Settings']['GeneralPermission']['ApplyToSelected'],"Button","javascript:ApplyToSelected('SelectedRoomBookingRule')").
    	"&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_AllManagementGroup")
{
	$sql = "SELECT GroupID, GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP ORDER BY GroupName";
	$arrMgmtGroup = $lebooking->returnArray($sql,2);
	
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	
	for($i=0; $i<sizeof($arrMgmtGroup); $i++)
	{
		list($mgmt_group_id, $mgmt_group_name) = $arrMgmtGroup[$i];
		$layer_content .= "<tr class='row_approved'><td valign='top'><input type='checkbox' name='AllManagementGroup[]' value='$mgmt_group_id'></td><td valign='top'>$mgmt_group_name</td></tr>";
	}
	
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td coslpan='2'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2'>".$linterface->GET_SMALL_BTN($Lang['Btn']['ApplyToAll'],"Button","javascript:ApplyToAll('AllManagementGroup')").
	   "&nbsp;".$linterface->GET_SMALL_BTN($Lang['eBooking']['Settings']['GeneralPermission']['ApplyToSelected'],"Button","javascript:ApplyToSelected('SelectedManagementGroup')").
	   "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
	
}
else if($Action == "ShowEdit_AllFollowUpGroup")
{
	$sql = "SELECT GroupID, GroupName FROM INTRANET_EBOOKING_FOLLOWUP_GROUP ORDER BY GroupName";
	$arrFollowUpGroup = $lebooking->returnArray($sql,2);
		
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	for($i=0; $i<sizeof($arrFollowUpGroup); $i++)
	{
		list($follow_up_group_id, $follow_up_group_name) = $arrFollowUpGroup[$i];
		$layer_content .= "<tr class='row_approved'><td valign='top'><input type='checkbox' name='AllFollowUpGroup[]' value='$follow_up_group_id'></td><td valign='top'>$follow_up_group_name</td></tr>";
	}
	
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td coslpan='2'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2'>".$linterface->GET_SMALL_BTN($Lang['Btn']['ApplyToAll'],"Button","javascript:ApplyToAll('AllFollowUpGroup')").
	   "&nbsp;".$linterface->GET_SMALL_BTN($Lang['eBooking']['Settings']['GeneralPermission']['ApplyToSelected'],"Button","javascript:ApplyToSelected('SelectedFollowUpGroup')").
	   "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
	
}
else if($Action == "ShowEdit_AllUserBookingRule")
{
	$sql = "SELECT RuleID, RuleName FROM INTRANET_EBOOKING_USER_BOOKING_RULE";
	$arrBookingRule = $lebooking->returnArray($sql,2);
			
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	for($i=0; $i<sizeof($arrBookingRule); $i++){
		list($rule_id, $rule_name) = $arrBookingRule[$i];
		$layer_content .= "<tr class='row_approved'><td valign='top'><input type='checkbox' name='AllUserBookingRule[]' value='$rule_id'></td><td valign='top'>$rule_name</td></tr>";
	}
	
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2'>".$linterface->GET_SMALL_BTN($Lang['Btn']['ApplyToAll'],"Button","javascript:ApplyToAll('AllUserBookingRule')").
	   "&nbsp;".$linterface->GET_SMALL_BTN($Lang['eBooking']['Settings']['GeneralPermission']['ApplyToSelected'],"Button","javascript:ApplyToSelected('SelectedUserBookingRule')").
	   "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</table>";
	$layer_content .= "</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_AvaliableBooking")
{
	$sql = "SELECT AllowBooking FROM INVENTORY_LOCATION WHERE LocationID = '$SubLocationID'";
	$arrAllowBooking = $lebooking->returnVector($sql);
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	if($arrAllowBooking[0] == 1){
		$allowbooking_checked = " CHECKED ";
		$non_allowbooking_checked = " ";
	}else{
		$allowbooking_checked = " ";
		$non_allowbooking_checked = " CHECKED ";
	}
	$layer_content .= "<tr class='row_approved'>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext' width='10%' align='left' ><input type='radio' name='AvaliableBooking_".$SubLocationID."' id='AvaliableBooking_".$SubLocationID."_Yes' value='1' $allowbooking_checked></td>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext'><label for='AvaliableBooking_".$SubLocationID."_Yes'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_tick_green.gif' border='0'>".$Lang['eBooking']['Settings']['GeneralPermission']['Available']."</lable></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr class='row_approved'>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext' width='10%' align='left' ><input type='radio' name='AvaliableBooking_".$SubLocationID."' id='All_AvaliableBooking_".$SubLocationID."_No' value='0' $non_allowbooking_checked></td>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext'><label for='AvaliableBooking_".$SubLocationID."_No'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_norecord_bw.gif' border='0'>".$Lang['eBooking']['Settings']['GeneralPermission']['NotAvailable']."</lable></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],"Button","javascript:ApplyToSelectedLocation('AvaliableBooking',$SubLocationID)")."&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_DayBeforeBooking")
{
	$sql = "SELECT Rule.DaysBeforeUse FROM INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION AS Relation INNER JOIN INTRANET_EBOOKING_LOCATION_BOOKING_RULE AS Rule ON (Relation.BookingRuleID = Rule.RuleID) WHERE Relation.LocationID = '$SubLocationID'";
	$arrLocationBookingRule = $lebooking->returnArray($sql,1);
	if(sizeof($arrLocationBookingRule)>0) {
		list($day_before_booking) = $arrLocationBookingRule[0];
	} else {
		$day_before_booking = 0;
	}
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr class='row_approved'><td nowrap style='border-bottom:none'>";
	$layer_content .= "<input name='BookingDay_".$SubLocationID."' id='BookingDay_".$SubLocationID."' class='textboxnum' value='$day_before_booking' maxlength=11> ".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Days'];
	$layer_content .= "<div id='WarnBookingDay_".$SubLocationID."'></div>";
//	$layer_content .= "<select name='BookingDay_".$SubLocationID."' id='BookingDay_".$SubLocationID."'>";
//	
//	for($i=0; $i<sizeof($Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay']); $i++) 
//	{
//		if($day_before_booking == $Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'][$i][0])
//			$layer_content .= "<option value=".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'][$i][0]." SELECTED>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'][$i][1];
//		else
//			$layer_content .= "<option value=".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'][$i][0]." >".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'][$i][1];
//	}
//	
//	$layer_content .= "</select>";
	$layer_content .= "</td></tr>";															
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],"Button","javascript:ApplyToSelectedLocation('DayBeforeBooking',$SubLocationID)")."&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_Facilities")
{
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='0' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr class='tabletop'>";
	$layer_content .= "<td>#</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['FieldTitle']['Category']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Quantity']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['IncludedWhenBooking']."</td>";
	$layer_content .= "<td>".$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['ShowForReference']."</td>";
	$layer_content .= "</tr>";
	
//	$sql = "(SELECT item.ItemID, 2 AS ItemType, ".$linventory->getInventoryNameByLang("item.").", CONCAT(".$linventory->getInventoryNameByLang("cat.").",' > ',".$linventory->getInventoryNameByLang("sub_cat.")."), bulk_location.Quantity, IncludedWhenBooking, ShowForReference FROM INVENTORY_ITEM AS item INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS bulk_location ON (item.ItemID = bulk_location.ItemID) LEFT OUTER JOIN INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION AS relation ON (relation.LocationID = $SubLocationID AND item.ItemID = relation.BulkItemID) INNER JOIN INVENTORY_CATEGORY AS cat ON (item.CategoryID = cat.CategoryID) INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS sub_cat ON (item.Category2ID = sub_cat.Category2ID) WHERE item.ItemType = 2 AND item.RecordStatus = 1 AND bulk_location.LocationID = $SubLocationID) 
//			UNION 
//			(SELECT ItemID, 1 AS ItemType, ".$linventory->getInventoryNameByLang("item.").", CONCAT(".$linventory->getInventoryNameByLang("cat.").",' > ',".$linventory->getInventoryNameByLang("sub_cat.")."), 1 AS Quantity, IncludedWhenBooking, ShowForReference FROM INTRANET_EBOOKING_ITEM AS item INNER JOIN INVENTORY_CATEGORY AS cat ON (item.CategoryID = cat.CategoryID) INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS sub_cat ON (item.Category2ID = sub_cat.Category2ID) WHERE LocationID = $SubLocationID AND AllowBooking = 1)";
//	$arrItemList = $lebooking->returnArray($sql,6);
	$arrItemList = $lebooking->Get_Room_Item_Info($SubLocationID);
	if(sizeof($arrItemList)>0) 
	{
		for($i=0; $i<sizeof($arrItemList); $i++) 
		{
			list($ItemID, $ItemType, $ItemName, $ItemCat, $Qty, $IncludedWhenBooking, $ShowForReference) = $arrItemList[$i];
			
			if($IncludedWhenBooking)
				$IncludedWhenBooking_Check = " CHECKED ";
			else
				$IncludedWhenBooking_Check = " ";
			if($ShowForReference)
				$ShowForReference_Check = " CHECKED ";
			else
				$ShowForReference_Check = " ";
			
			$layer_content .= "<tr class='row_approved'>";
			$layer_content .= "<td >".($i+1)."</td>";
			$layer_content .= "<td >$ItemName</td>";
			$layer_content .= "<td >$ItemCat</td>";
			$layer_content .= "<td >$Qty</td>";
			if($ItemType == 1) {
				$layer_content .= "<td > - </td>";
				$layer_content .= "<td ><input type='checkbox' name='Single_ShowForReference[]' value='$ItemID' $ShowForReference_Check></td>";
				$layer_content .= "<input type='hidden' name='SingleItemID[]' value='$ItemID'>";
			} else {
				$layer_content .= "<td ><input type='checkbox' name='Bulk_IncludeWhenBooking[]' value='$ItemID' $IncludedWhenBooking_Check></td>";
				$layer_content .= "<td > - </td>";
				$layer_content .= "<input type='hidden' name='BulkItemID[]' value='$ItemID'>";
			}
			$layer_content .= "</tr>";				
		}
	}
	else
	{
		$layer_content .= "<tr class='row_approved'><td colspan='6' align='center'>".$Lang['General']['NoRecordAtThisMoment']."</td></tr>";
	}
												
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='6' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],"Button","javascript:ApplyToSelectedLocation('Facilities',$SubLocationID)")."&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_ManagmentGroup")
{
	$sql = "SELECT management_group.GroupID, management_group.GroupName FROM INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS relation INNER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS management_group ON (relation.GroupID = management_group.GroupID) WHERE relation.LocationID = '$SubLocationID'";
	$arrManagementGroup = $lebooking->returnArray($sql,2);
	
	if(sizeof($arrManagementGroup) == 1) {
		list($management_group_id, $management_group_name) = $arrManagementGroup[0];
		$txt_management_group = $management_group_name;
	} else if(sizeof($arrManagementGroup)>0) {
		$txt_management_group = sizeof($arrManagementGroup)." ".$Lang['eBooking']['Settings']['GeneralPermission']['Group(s)'];
	} else {
		$txt_management_group = " - ";
	}
	for($i=0; $i<sizeof($arrManagementGroup); $i++)
	{
		list($selected_group_id, $selected_group_name) = $arrManagementGroup[$i];
		$arr_selected_management_group[$SubLocationID][] = $selected_group_id;
	}
	
	$sql = "SELECT GroupID, GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP ORDER BY GroupName";
	$arrMgmtGroup = $lebooking->returnArray($sql,2);
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	for($i=0; $i<sizeof($arrMgmtGroup); $i++)
	{
		list($mgmt_group_id, $mgmt_group_name) = $arrMgmtGroup[$i];
		if(is_array($arr_selected_management_group[$SubLocationID]))
		{
			if(in_array($mgmt_group_id,$arr_selected_management_group[$SubLocationID])){
				$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetManagementGroup".$SubLocationID."[]' value='$mgmt_group_id' checked></td><td valign='top' nowrap style='border-bottom:none'>$mgmt_group_name</td></tr>";
			}else{
				$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetManagementGroup".$SubLocationID."[]' value='$mgmt_group_id'></td><td valign='top' nowrap style='border-bottom:none'>$mgmt_group_name</td></tr>";
			}
		}
		else
		{
			$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetManagementGroup".$SubLocationID."[]' value='$mgmt_group_id'></td><td valign='top' nowrap style='border-bottom:none'>$mgmt_group_name</td></tr>";
		}
	}
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],"Button","javascript:ApplyToSelectedLocation('ManagmentGroup',$SubLocationID)")."&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_FollowUpGroup")
{
	$sql = "SELECT follow_up.GroupID, follow_up.GroupName FROM INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION AS relation INNER JOIN INTRANET_EBOOKING_FOLLOWUP_GROUP AS follow_up ON (relation.GroupID = follow_up.GroupID) WHERE relation.LocationID = '$SubLocationID'";
	$arrFollowUpGroup = $lebooking->returnArray($sql,2);
	
	if(sizeof($arrFollowUpGroup) == 1) {
		list($follow_up_group_id, $follow_up_group_name) = $arrFollowUpGroup[0];
		$txt_follow_up_group = $follow_up_group_name;
	} else if(sizeof($arrFollowUpGroup)>0) {
		$txt_follow_up_group = sizeof($arrFollowUpGroup)." ".$Lang['eBooking']['Settings']['GeneralPermission']['Group(s)'];
	} else {
		$txt_follow_up_group = " - ";
	}
	for($i=0; $i<sizeof($arrFollowUpGroup); $i++)
	{
		list($selected_follow_up_group_id, $selected_follow_up_group_name) = $arrFollowUpGroup[$i];
		$arr_selected_follow_up_group[$SubLocationID][] = $selected_follow_up_group_id;
	}
	
	$sql = "SELECT GroupID, GroupName FROM INTRANET_EBOOKING_FOLLOWUP_GROUP ORDER BY GroupName";
	$arrFollowUpGroup = $lebooking->returnArray($sql,2);
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	for($i=0; $i<sizeof($arrFollowUpGroup); $i++)
	{
		list($follow_up_group_id, $follow_up_group_name) = $arrFollowUpGroup[$i];
		if(is_array($arr_selected_follow_up_group[$SubLocationID]))
		{
			if(in_array($follow_up_group_id,$arr_selected_follow_up_group[$SubLocationID])){
				$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetFollowUpGroup".$SubLocationID."[]' value='$follow_up_group_id' checked></td><td valign='top' nowrap style='border-bottom:none'>$follow_up_group_name</td></tr>";
			}else{
				$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetFollowUpGroup".$SubLocationID."[]' value='$follow_up_group_id'></td><td valign='top' nowrap style='border-bottom:none'>$follow_up_group_name</td></tr>";
			}
		}
		else
		{
			$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetFollowUpGroup".$SubLocationID."[]' value='$follow_up_group_id'></td><td valign='top' nowrap style='border-bottom:none'>$follow_up_group_name</td></tr>";
		}
	}
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],"Button","javascript:ApplyToSelectedLocation('FollowUpGroup',$SubLocationID)")."&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == "ShowEdit_UserBookingRule")
{
	$sql = "SELECT rule.RuleID, rule.RuleName FROM INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION AS relation INNER JOIN INTRANET_EBOOKING_USER_BOOKING_RULE AS rule ON (relation.UserBookingRuleID = rule.RuleID) WHERE relation.LocationID = '$SubLocationID'";
	$arrUserBookingRule = $lebooking->returnArray($sql,2);
	if(sizeof($arrUserBookingRule) == 1) {
		list($user_booking_rule_id, $user_booking_rule) = $arrUserBookingRule[0];
		$txt_user_booking_rule = $user_booking_rule;
	} else if(sizeof($arrUserBookingRule) > 0) {
		$txt_user_booking_rule = sizeof($arrUserBookingRule)." ".$Lang['eBooking']['Settings']['GeneralPermission']['Rule(s)'];
	} else {
		$txt_user_booking_rule = " - ";
	}
	for($i=0; $i<sizeof($arrUserBookingRule); $i++)
	{
		list($user_booking_rule_id, $user_booking_rule) = $arrUserBookingRule[$i];
		$arr_selected_user_booking_rule[$SubLocationID][] = $user_booking_rule_id;
	}
	
	$sql = "SELECT RuleID, RuleName FROM INTRANET_EBOOKING_USER_BOOKING_RULE";
	$arrBookingRule = $lebooking->returnArray($sql,2);
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	for($i=0; $i<sizeof($arrBookingRule); $i++)
	{
		list($rule_id, $rule_name) = $arrBookingRule[$i];
		if(is_array($arr_selected_user_booking_rule[$SubLocationID]))
		{
			if(in_array($rule_id,$arr_selected_user_booking_rule[$SubLocationID]))
			{
				$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetUserBookingRule_".$SubLocationID."[]' value='$rule_id' checked></td><td valign='top' nowrap style='border-bottom:none'>$rule_name</td></tr>";
			}
			else
			{
				$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetUserBookingRule_".$SubLocationID."[]' value='$rule_id'></td><td valign='top' nowrap style='border-bottom:none'>$rule_name</td></tr>";
			}	
		}
		else
		{
			$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none'><input type='checkbox' name='TargetUserBookingRule_".$SubLocationID."[]' value='$rule_id'></td><td valign='top' nowrap style='border-bottom:none'>$rule_name</td></tr>";
		}
	}
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],"Button","javascript:ApplyToSelectedLocation('UserBookingRule',$SubLocationID)")."&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == 'ShowLocationDescription')
{
	$sql = "SELECT Description FROM INVENTORY_LOCATION WHERE LocationID = '$SubLocationID'";
	$arrResult = $lebooking->returnArray($sql);
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr><td align='right'><a href='javascript:js_Hide_Detail_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
	for($i=0; $i<sizeof($arrResult); $i++)
	{
		list($description) = $arrResult[$i];
		$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none' colspan='3'>$description</td></tr>";
	}
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}else if($Action == 'ShowRoomAttachment')
{
	$lebooking_ui = new libebooking_ui();
	$layer_content = $lebooking_ui->Get_Room_Attachment_Layer_Table($SubLocationID);
}

echo $layer_content;
?>