<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

$lebooking = new libebooking();

intranet_auth();
intranet_opendb();

$targetBuilding = (isset($_POST['targetBuilding'])/* && $_POST['targetBuilding'] != ""*/) ? $_POST['targetBuilding'] : $targetBuilding;
$targetFloor 	= (isset($_POST['targetFloor'])/* && $_POST['targetFloor'] != ""*/) ? $_POST['targetFloor'] : $targetFloor;
$targetRoom		= (isset($_POST['targetRoom'])/* && $_POST['targetRoom'] != ""*/) ? $_POST['targetRoom'] : $targetRoom;
$displayedSubLocationIdList		= (isset($_POST['displayedSubLocationIdList'])/* && $_POST['displayedSubLocationIdList'] != ""*/) ? $_POST['displayedSubLocationIdList'] : $displayedSubLocationIdList;

$lebooking->Start_Trans();

if($targetRoomID == "")
{
	$displayedSubLocationIdAry = explode(',', $displayedSubLocationIdList);
	$sql = "SELECT 
				Room.LocationID 
			FROM 
				INVENTORY_LOCATION_BUILDING AS Building 
				INNER JOIN INVENTORY_LOCATION_LEVEL AS Floor ON (Building.BuildingID = Floor.BuildingID)
				INNER JOIN INVENTORY_LOCATION AS Room ON (Floor.LocationLevelID = Room.LocationLevelID)
			WHERE
				Building.RecordStatus = 1 AND Floor.RecordStatus = 1 AND Room.RecordStatus = 1 
				AND	Building.BuildingID = $targetBuilding AND Floor.LocationLevelID = $targetFloor AND Room.LocationID IN ('".implode("','", (array)$displayedSubLocationIdAry)."')";
	
	$arrLocation = $lebooking->returnVector($sql);
	if(sizeof($arrLocation)>0)
	{
		$targetRoomID = implode(",",$arrLocation);
	}
}

$sql = "UPDATE INVENTORY_LOCATION SET AllowBooking = $AvaliableForBooking WHERE LocationID IN ($targetRoomID) AND RecordStatus = 1";
$result["UpdateAvaliableForBooking"] = $lebooking->db_db_query($sql);

if (in_array(false,$result)) {
	$lebooking->RollBack_Trans();
	echo 0;
} else {
	$lebooking->Commit_Trans();
	echo 1;
}

intranet_closedb();
?>