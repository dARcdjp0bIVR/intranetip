<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$li = new libebooking_ui();

$li->Check_Page_Permission();

include_once($PATH_WRT_ROOT."templates/2009a/layout/print_header.php");

echo $li->Get_Print_Available_Period_UI($AcademicYearID, $FacilityType, $FacilityID);


include_once($PATH_WRT_ROOT."templates/2009a/layout/print_footer.php");

intranet_closedb();
?>