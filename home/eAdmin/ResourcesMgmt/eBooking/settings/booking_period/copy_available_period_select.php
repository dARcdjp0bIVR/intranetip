<?php
//using : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
//include_once($PATH_WRT_ROOT."includes/liblocation.php");
//include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");
//include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_BookingPeriodSettings";
$linterface 	= new interface_html();
//$linventory		= new libinventory();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
//$llocation_ui	= new liblocation_ui();

$lebooking->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'], "general_available_period.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'], "specific_available_period.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($msg);

echo $lebooking_ui->Get_Copy_Available_Period_Select_UI($AcademicYearID, $FacilityType, $FacilityID);


intranet_closedb();
$linterface->LAYOUT_STOP();
?>
<script>
function js_Check_All_Class(ClassName, thisChecked)
{
	$("input."+ClassName).attr("checked",thisChecked)
}

function showOption(thisName)
{
	$("#spanShowOption_"+thisName).hide();
	$("#spanHideOption_"+thisName).show();
	$("#PeriodTable_"+thisName).show();
}

function hideOption(thisName)
{
	$("#spanShowOption_"+thisName).show();
	$("#spanHideOption_"+thisName).hide();
	$("#PeriodTable_"+thisName).hide();
}

function CheckForm()
{
	if($(".CopyCheckBox_1:checked").length==0 && $(".CopyCheckBox_2:checked").length==0 )
	{
		alert("<?=$Lang['eBooking']['BookingPeriod']['CopyBookingPeriod']['jsWarningMessage']['PleaseSelectFacility']?>")
		return false;
	}
	return true;
}

</script>

