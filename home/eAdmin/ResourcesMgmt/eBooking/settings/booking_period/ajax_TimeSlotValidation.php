<?php
//using : Roanld

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

/*
$sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '$StartDate' BETWEEN PeriodStart AND PeriodEnd";
$arrCheckStartDate = $li->returnVector($sql);

$sql = "SELECT PeriodID FROM INTRANET_CYCLE_GENERATION_PERIOD WHERE '$EndDate' BETWEEN PeriodStart AND PeriodEnd";
$arrCheckEndDate = $li->returnVector($sql);


if($arrCheckStartDate[0] != $arrCheckEndDate[0])
{
	// start date and end date is not in the same period(time zone).
	echo $Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DateRangebNotInSamePeriod'];
}
*/

$checkTimeone = ($_POST['CheckTimeZone']=='' || $_POST['CheckTimeZone']==1)? true : false; 	// default check
$checkAcademicYear = ($_POST['CheckAcademicYear']=='' || $_POST['CheckAcademicYear']==0)? false : true;	// default don't check


if($checkTimeone && !$lebooking->Time_Slot_Validation($StartDate,$EndDate)) {
	// start date and end date is not in the same period(time zone).
	echo $Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DateRangebNotInSamePeriod'];
}
else if ($checkAcademicYear && $SchoolYearID != '') {
	// start date and end date must in the same academic year
	$startDateAcademicYearId = $lebooking->Get_AcademicYearID_By_Date($StartDate);
	$endDateAcademicYearId = $lebooking->Get_AcademicYearID_By_Date($EndDate);
	
	if ($startDateAcademicYearId != $SchoolYearID || $endDateAcademicYearId != $SchoolYearID) {
		echo $Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DateRangebNotInSelectedSchoolYear'];
	}
}

intranet_closedb();
?>