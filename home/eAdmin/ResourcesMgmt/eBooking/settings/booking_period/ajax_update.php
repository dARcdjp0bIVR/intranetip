<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$li = new libebooking();

switch ($task)
{
	case "Save_BookingPeriod_TimeZone_Edit_Layer":
		echo $li->Save_TimeZone_BookingPeriod($SelectedSlot,$TimezoneSettingID,$TimeZoneID, $FacilityType, $FacilityID, $SpecialTimetableSettingID);
	break;
	case "RemoveFacilityBookingPeriod":
		$Result = $li->Remove_Facility_Booking_Period($AcademicYearID, $FacilityType, $FacilityID);
		$li->Start_Trans();
		if($Result)
		{
			$li->Commit_Trans();
			echo $Lang['General']['ReturnMessage']['DeleteSuccess'];	
		}
		else
		{
			$li->RollBack_Trans();
			echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		}
	break;
	case "RemoveTimezoneBookingPeriod":
		$Result = $li->Remove_Facility_Timezone_Booking_Period($TimezoneSettingID);
		$li->Start_Trans();
		if($Result)
		{
			$li->Commit_Trans();
			echo $Lang['General']['ReturnMessage']['DeleteSuccess'];	
		}
		else
		{
			$li->RollBack_Trans();
			echo $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
		}
	break;
	
}
echo $output;

intranet_closedb();
?>