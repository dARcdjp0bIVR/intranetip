<?php
// using : Ronald

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

$lebooking->Start_Trans();

if($IsSuspension)
	$result = $lebooking->DeleteBookingDaySuspension($PeriodID);
else
	$result = $lebooking->DeleteBookingPeriod($PeriodID);

if($result)
{
	echo 1;
	$lebooking->Commit_Trans();
}
else
{
	echo 0;
	$lebooking->RollBack_Trans();
}

intranet_closedb();
?>