<?php
//using : Ronald

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();

$arrCycleName = $lcycleperiods->getCycleNamesByDateRange($StartDate,$EndDate);

if(sizeof($arrCycleName)>0) {
	for($i=0; $i<sizeof($arrCycleName); $i++){
		list($short_format, $eng_cycle_name) = $arrCycleName[$i];
		$arr_temp[] = $short_format;
	}
}
$content = "<table border='0' cellpadding='3' cellspacing='0'>";
$content .= "<tr><td>";
if(sizeof($arr_temp)>0)
{
	for($i=0; $i<sizeof($arr_temp); $i++)
	{
		$content .= "<input type='checkbox' name='cycleday_text[]' id='cycleday_text_$i' value='".$arr_temp[$i]."'>$arr_temp[$i] ";
	}
}
else
{
	echo "<font color='red'>".$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['CycleDayIsNotSet']."</font>";
}
$content .= "</td></tr>";
$content .= "</table>";

echo $content;
intranet_closedb();
?>