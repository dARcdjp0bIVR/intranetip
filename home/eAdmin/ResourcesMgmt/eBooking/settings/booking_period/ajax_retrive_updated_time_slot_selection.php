<?php
/*******************************************************
 *  Modification:
 *  20191120 Cameron
 *      - don't show time slot period for EverydayTimetable [case #M165759]
 *
 * 	20110406 Marcus:
 * 		- return empty selection instead of return NO_TIMETABLE if no timeslot are retrieved
 * *******************************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();

if ($sys_custom['eBooking']['EverydayTimetable']) {
    $result = $lebooking->getTimeslotListOfTimetableWithMaxNumOfSlotsByDate($StartDate);
}
else {

    $sql = "SELECT 
			TT.TimeSlotID,
			CONCAT(TT.TimeSlotName,' (',TT.StartTime,' - ',TT.EndTime,')'),
			CONCAT(TT.StartTime,' - ',TT.EndTime)
		FROM 
			INTRANET_CYCLE_GENERATION_PERIOD AS period 
			INNER JOIN INTRANET_PERIOD_TIMETABLE_RELATION AS PT_relation ON (period.PeriodID = PT_relation.PeriodID) 
			INNER JOIN INTRANET_TIMETABLE_TIMESLOT_RELATION AS TT_relation ON (PT_relation.TimetableID = TT_relation.TimetableID) 
			INNER JOIN INTRANET_TIMETABLE_TIMESLOT AS TT ON (TT_relation.TimeSlotID = TT.TimeSlotID) 
		WHERE 
			(period.PeriodStart <= '$StartDate' AND '$EndDate' <= period.PeriodEnd)";
    $result = $lebooking->returnArray($sql, 3);
}
//if(sizeof($result)>0)
//{
	$periodSelection = getSelectByArray($result , "name='AvaliableTimeSlot_1' id='AvaliableTimeSlot_1' disabled", $AvaliableTimeSlot);
//}
//else
//{
//	$periodSelection = "NO_TIMETABLE";
//}

echo $periodSelection;
intranet_closedb();
?>