<?php
// Using : Ronald

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$lcycleperiods = new libcycleperiods();
$lebooking = new libebooking();

//debug_pr($_POST);
$lcycleperiods->Start_Trans();

$edit_mode = false;
if($PeriodID != "")
{
	$edit_mode = true;
	if(!$IsSuspension)
		$lebooking->DeleteBookingPeriod($PeriodID);
	else
		$lebooking->DeleteBookingDaySuspension($PeriodID);
}


	if (($RepeatOption == 0) || ($RepeatOption == 1)) // does not repeat || daily
	{
//		$NumOfDay = round( abs(strtotime($EndDate)-strtotime($StartDate)) / 86400, 0 );
		$NumOfDay = round(abs(intranet_time_diff($EndDate,$StartDate)), 0 );
	
		for($i=0; $i<=$NumOfDay; $i++) 
		{
			$ts_EventStartDate = strtotime($StartDate);
			$targetDate = $ts_EventStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
			$array_targetDate[] = date("Y-m-d",$targetDate);
		}
		
//		$Success = $lebooking->Separate_Booking_Period_By_DateArr($array_targetDate, 0, 0);
//		$str_targetDate = implode(",",$array_targetDate);

//		$targetStartDate = $array_targetDate[0];
//		$targetEndDate = $array_targetDate[$NumOfDay];
		
//		for($i=0; $i<sizeof($array_targetDate); $i++)
//		{
//			$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." GROUP By RepeatType, RepeatValue";
//			$arrResult = $lebooking->returnArray($sql,3);
//			if(sizeof($arrResult) > 0){
//				for($j=0; $j<sizeof($arrResult); $j++){
//					list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//					if($repeat_value == ""){
//						$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//					}else{
//						$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//					}
//					$lebooking->db_db_query($sql);
//				}
//			}
//		
//			$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." GROUP By RepeatType, RepeatValue";
//			$arrResult = $lebooking->returnArray($sql,3);
//			if(sizeof($arrResult) > 0){
//				for($j=0; $j<sizeof($arrResult); $j++){
//					list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//					if($repeat_value == ""){
//						$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//					}else{
//						$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//					}
//					$lebooking->db_db_query($sql);
//				}
//			}
//		
//			$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." GROUP By RepeatType, RepeatValue";
//			$arrResult = $lebooking->returnArray($sql,3);
//			if(sizeof($arrResult) > 0){
//				for($j=0; $j<sizeof($arrResult); $j++){
//					list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//					if($repeat_value == ""){
//						$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//					}else{
//						$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//					}
//					$lebooking->db_db_query($sql);
//				}
//			}
//		}
//		$sql = "DELETE FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate IN ($str_targetDate)";
//		$lebooking->db_db_query($sql);
//		
//		$sql = "SELECT MIN(PeriodID), RelatedPeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate NOT IN($str_targetDate) GROUP By RepeatType, RepeatValue";
//		$arrResult = $lebooking->returnArray($sql);
//		if(sizeof($arrResult) > 0){
//			for($i=0; $i<sizeof($arrResult); $i++){
//				list($new_related_periodID, $original_periodID) = $arrResult[$i];
//				$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedPeriodID = $original_periodID";
//				$lebooking->db_db_query($sql);  
//			}
//		}
	}
	else if($RepeatOption == 2) 
	{
		if(sizeof($ArrayWeekday)>0) 
		{
			unset($arrRepeatValue);
			foreach($ArrayWeekday as $key=>$val)
			{
				$arrTargetWeekday[] = "'".$val."'";
				$arrRepeatValue[] = $val;
			}
			$targetWeekday = implode(",",$arrTargetWeekday);
			$final_repeat_value = implode(",",$arrRepeatValue);
			
			$start_year = substr($StartDate,0,4);
			$start_month = substr($StartDate,5,2);
			$start_day = substr($StartDate,-2);
			$strDateDiff = strtotime($EndDate) - strtotime($StartDate);
			$numOfDayDiff = round(($strDateDiff + 86400)/ 86400 );
			
			unset($parent_period_id);
			$NoOfDayCounter = 0;
			for($i=0; $i<$numOfDayDiff; $i++)
			{
				$ts_targetDate = mktime(0,0,0,$start_month,$start_day+$i,$start_year); // build date
				$tmp_weekday = date("w",$ts_targetDate); // get week day
				
				if(in_array($tmp_weekday,$ArrayWeekday)) // check whether week day was selected
				{
					$NoOfDayCounter++;
					$array_targetDate[] = date("Y-m-d",$ts_targetDate);
				}
			}
			
//			$Success = $lebooking->Separate_Booking_Period_By_DateArr($array_targetDate, 0, 0);
//			$str_targetDate = implode(",",$array_targetDate);
//			
//			for($i=0; $i<sizeof($array_targetDate); $i++)
//			{
//				$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." GROUP By RepeatType, RepeatValue";
//				$arrResult = $lebooking->returnArray($sql,3);
//				if(sizeof($arrResult) > 0){
//					for($j=0; $j<sizeof($arrResult); $j++){
//						list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//						if($repeat_value == ""){
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//						}else{
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//						}
//						$lebooking->db_db_query($sql);
//					}
//				}
//				
//				$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." GROUP By RepeatType, RepeatValue";
//				$arrResult = $lebooking->returnArray($sql,3);
//				if(sizeof($arrResult) > 0){
//					for($j=0; $j<sizeof($arrResult); $j++){
//						list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//						if($repeat_value == ""){
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//						}else{
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//						}	
//						$lebooking->db_db_query($sql);
//					}
//				}
//				
//				$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." GROUP By RepeatType, RepeatValue";
//				$arrResult = $lebooking->returnArray($sql,3);
//				if(sizeof($arrResult) > 0){
//					for($j=0; $j<sizeof($arrResult); $j++){
//						list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//						if($repeat_value == ""){
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//						}else{
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//						}
//						$lebooking->db_db_query($sql);
//					}
//				}
//			}
//			$sql = "DELETE FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate IN ($str_targetDate)";
//			$lebooking->db_db_query($sql);
//			
//			$sql = "SELECT MIN(PeriodID), RelatedPeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate NOT IN($str_targetDate) GROUP By RepeatType, RepeatValue";
//			$arrResult = $lebooking->returnArray($sql);
//			if(sizeof($arrResult) > 0){
//				for($i=0; $i<sizeof($arrResult); $i++){
//					list($new_related_periodID, $original_periodID) = $arrResult[$i];
//					$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedPeriodID = $original_periodID";
//					$lebooking->db_db_query($sql);  
//				}
//			}
		}
	}
	else if($RepeatOption == 3)
	{
		if(sizeof($ArrayCycleday)>0)
		{
			unset($arrRepeatValue);
			foreach($ArrayCycleday as $key=>$val)
			{
				$arrCycleDay[] = "'".$val."'";
				$arrRepeatValue[] = $val;
			}
			$targetCycleDay = implode(",",$arrCycleDay);
			$final_repeat_value = implode(",",$arrRepeatValue);
			
			$sql = "SELECT RecordDate FROM INTRANET_CYCLE_DAYS WHERE RecordDate BETWEEN '$StartDate' AND '$EndDate' AND TextShort IN ($targetCycleDay)";
			$arrTargetRecordDate = $lcycleperiods->returnVector($sql,1);
			
			if(sizeof($arrTargetRecordDate)>0) 
			{
				for($i=0; $i<sizeof($arrTargetRecordDate); $i++) 
				{
					$array_targetDate[]  = $arrTargetRecordDate[$i];
				}
			}
			
//			$Success = $lebooking->Separate_Booking_Period_By_DateArr($array_targetDate, 0, 0);
//			$str_targetDate = implode(",",$array_targetDate);			
//			for($i=0; $i<sizeof($array_targetDate); $i++)
//			{
//				$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." GROUP By RepeatType, RepeatValue";
//				$arrResult = $lebooking->returnArray($sql,3);
//				if(sizeof($arrResult) > 0){
//					for($j=0; $j<sizeof($arrResult); $j++){
//						list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//						if($repeat_value == ""){
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//						}else{
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//						}	
//						$lebooking->db_db_query($sql);
//					}
//				}
//				
//				$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." GROUP By RepeatType, RepeatValue";
//				$arrResult = $lebooking->returnArray($sql,3);
//				if(sizeof($arrResult) > 0){
//					for($j=0; $j<sizeof($arrResult); $j++){
//						list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//						if($repeat_value == ""){
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//						}else{
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate > ".$array_targetDate[$i]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//						}
//						$lebooking->db_db_query($sql);
//					}
//				}
//				
//				$sql = "SELECT MIN(PeriodID), RepeatType, RepeatValue FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." GROUP By RepeatType, RepeatValue";
//				$arrResult = $lebooking->returnArray($sql,3);
//				if(sizeof($arrResult) > 0){
//					for($j=0; $j<sizeof($arrResult); $j++){
//						list($new_related_periodID, $repeat_type, $repeat_value) = $arrResult[$j];
//						if($repeat_value != ""){
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." AND RepeatType = '$repeat_type' AND RepeatValue IS NULL";
//						}else{
//							$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate < ".$array_targetDate[$i]." AND RecordDate > ".$array_targetDate[$i-1]." AND RepeatType = '$repeat_type' AND RepeatValue = '$repeat_value'";
//						}	
//						$lebooking->db_db_query($sql);
//					}
//				}
//			}
//			$sql = "DELETE FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate IN ($str_targetDate)";
//			$lebooking->db_db_query($sql);
//			
//			$sql = "SELECT MIN(PeriodID), RelatedPeriodID FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND RelatedSubLocationID = 0 AND RecordDate NOT IN($str_targetDate) GROUP By RepeatType, RepeatValue";
//			$arrResult = $lebooking->returnArray($sql);
//			if(sizeof($arrResult) > 0){
//				for($i=0; $i<sizeof($arrResult); $i++){
//					list($new_related_periodID, $original_periodID) = $arrResult[$i];
//					$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = $new_related_periodID WHERE RelatedPeriodID = $original_periodID";
//					$lebooking->db_db_query($sql);  
//				}
//			}
		}
	}

### Finalize the StartTime & EndTime
if(!$IsSuspension)
{
	if(sizeof($ArrayTimeSlot)==0)
	{
		// using specific time
		for($i=0; $i<sizeof($ArrayStartTime); $i++){
			$arrFinalStartTime[] = $ArrayStartTime[$i].":00";
		}
		for($i=0; $i<sizeof($ArrayEndTime); $i++){
			//$arrFinalEndTime[] = $ArrayEndTime[$i].":59";
			$arrFinalEndTime[] = $ArrayEndTime[$i].":00";
		}
	}
	else
	{
		// using time slot, need to get the specific time for the selected slot
		$targetTimeSlot = implode(",",$ArrayTimeSlot);
		
		$sql = "SELECT TimeSlotID, StartTime, EndTime FROM INTRANET_TIMETABLE_TIMESLOT WHERE TimeSlotID In ($targetTimeSlot)";
		$arrResult = $lebooking->returnArray($sql);
		if(sizeof($arrResult)>0){
			for($i=0; $i<sizeof($arrResult); $i++){
				list($TimeSlotID, $StartTime, $EndTime) = $arrResult[$i];
				$tmpEndTime = explode(":",$EndTime);
				//$finalEndTime = $tmpEndTime[0].":".$tmpEndTime[1].":59";
				$finalEndTime = $tmpEndTime[0].":".$tmpEndTime[1].":00";
				$arrFinalTimeSlotID[] = $TimeSlotID;   
				$arrFinalStartTime[] = $StartTime;
				$arrFinalEndTime[] = $finalEndTime; 
			}
		}
	}
}

//if (($RepeatOption == 0) || ($RepeatOption == 1))		## Do Not Repeat / Repeat Every Day
//{
//	$NumOfDay = round( abs(strtotime($EndDate)-strtotime($StartDate)) / 86400, 0 );
//	
//	for($i=0; $i<=$NumOfDay; $i++) 
//	{
//		$ts_EventStartDate = strtotime($StartDate);
//		$targetDate = $ts_EventStartDate + ($i * 86400);		## 86400 is UNIX Timestamp for 1 day
//		$targetDate = date("Y-m-d",$targetDate);
//				
//		$DateArr[] = $targetDate;
////		for($j=0; $j<$NoOfTimePeriod; $j++)
////		{
////			$FinalStartTime = $arrFinalStartTime[$j];
////			$FinalEndTime = $arrFinalEndTime[$j];
////			if(sizeof($arrFinalTimeSlotID)>0){
////				$TimeSlotID = $arrFinalTimeSlotID[$j];
////			}else{
////				$TimeSlotID = "";
////			}
////			$insert_values = "('$targetDate','$TimeSlotID','$FinalStartTime','$FinalEndTime','$RepeatOption','','0','0',$UserID,$UserID,NULL,NULL,NOW(),NOW())";
////			$sql = "INSERT INTO 
////						INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD
////						(RecordDate, TimeSlotID, StartTime, EndTime, RepeatType, RelatedPeriodID, RelatedItemID, RelatedSubLocationID, InputBy, ModifiedBy, RecordType, RecordStatus, DateInput, DateModified) 
////					VALUES ".$insert_values;
////			$result['CreateBookingPeriod'][] = $lcycleperiods->db_db_query($sql);
////				
////			if( (($i==0) && ($j==0)) || ($parent_period_id == ""))
////			{
////				$parent_period_id = $lcycleperiods->db_insert_id();
////				$child_period_id = $lcycleperiods->db_insert_id();
////			}
////			else
////			{
////				$child_period_id = $lcycleperiods->db_insert_id();
////			}
////			
////			$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = '$parent_period_id' WHERE PeriodID = $child_period_id";
////			$result['UpdateBookingPeriodRelation'][] = $lcycleperiods->db_db_query($sql);			
////		}
//	}
//} 
//else if($RepeatOption == 2) 
//{
//	## Week Day
//	if(sizeof($ArrayWeekday)>0) 
//	{
//		unset($arrRepeatValue);
//		foreach($ArrayWeekday as $key=>$val)
//		{
//			$arrTargetWeekday[] = "'".$val."'";
//			$arrRepeatValue[] = $val;
//		}
//		$targetWeekday = implode(",",$arrTargetWeekday);
//		$final_repeat_value = implode(",",$arrRepeatValue);
//
//		$start_year = substr($StartDate,0,4);
//		$start_month = substr($StartDate,5,2);
//		$start_day = substr($StartDate,-2);
//		$strDateDiff = strtotime($EndDate) - strtotime($StartDate);
//		$numOfDayDiff = round(($strDateDiff + 86400)/ 86400 );
//				
//		unset($parent_period_id);
//		for($i=0; $i<$numOfDayDiff; $i++)
//		{
//			$ts_targetDate = mktime(0,0,0,$start_month,$start_day+$i,$start_year);
//			$tmp_weekday = date("w",$ts_targetDate);
//			
//			if(in_array($tmp_weekday,$ArrayWeekday))
//			{
//				$targetDate = date("Y-m-d",$ts_targetDate);
//				
//				$DateArr[] = $targetDate;
////				for($j=0; $j<$NoOfTimePeriod; $j++)
////				{
////					$FinalStartTime = $arrFinalStartTime[$j];
////					$FinalEndTime = $arrFinalEndTime[$j];
////					if(sizeof($arrFinalTimeSlotID)>0){
////						$TimeSlotID = $arrFinalTimeSlotID[$j];
////					}else{
////						$TimeSlotID = "";
////					}
////					$sql = "INSERT INTO 
////								INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD 
////								(RecordDate, TimeSlotID, StartTime, EndTime, RepeatType, RepeatValue, RelatedPeriodID, RelatedItemID, RelatedSubLocationID, InputBy, ModifiedBy, RecordType, RecordStatus, DateInput, DateModified) 
////							VALUES 
////								('$targetDate','$TimeSlotID','$FinalStartTime','$FinalEndTime','$RepeatOption','$final_repeat_value','','0','0',$UserID,$UserID,NULL,NULL,NOW(),NOW())";
////								
////					$result['CreateBookingPeriod'][] = $lcycleperiods->db_db_query($sql);
////					
////					if( (($i==0) && ($j==0)) || ($parent_period_id == ""))
////					{
////						$parent_period_id = $lcycleperiods->db_insert_id();
////						$child_period_id = $lcycleperiods->db_insert_id();
////					}
////					else
////					{
////						$child_period_id = $lcycleperiods->db_insert_id();
////					}
////
////					$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = '$parent_period_id' WHERE PeriodID = $child_period_id";
////					$result['UpdateBookingPeriodRelation'][] = $lcycleperiods->db_db_query($sql);
////				}
//			}
//			else
//			{
//				$error++;
//			}
//		}
//		if($error == $numOfDayDiff)
//		{
//			echo $Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['WeekdayNotInDateRange'];
//			$lcycleperiods->RollBack_Trans();
//			exit();
//		}
//	}
//} 
//else if($RepeatOption == 3) 
//{
//	## Cycle Day
//	if(sizeof($ArrayCycleday)>0)
//	{
//		unset($arrRepeatValue);
//		foreach($ArrayCycleday as $key=>$val)
//		{
//			$arrCycleDay[] = "'".$val."'";
//			$arrRepeatValue[] = $val;
//		}
//		$targetCycleDay = implode(",",$arrCycleDay);
//		$final_repeat_value = implode(",",$arrRepeatValue);
//		
//		$sql = "SELECT RecordDate FROM INTRANET_CYCLE_DAYS WHERE RecordDate BETWEEN '$StartDate' AND '$EndDate' AND TextShort IN ($targetCycleDay)";
//		$arrTargetRecordDate = $lcycleperiods->returnArray($sql,1);
//		
//		if(sizeof($arrTargetRecordDate)>0) 
//		{
//			for($i=0; $i<sizeof($arrTargetRecordDate); $i++) 
//			{
//				list($targetRecordDate) = $arrTargetRecordDate[$i];
//				
//				$DateArr[] = $targetRecordDate;
////				for($j=0; $j<$NoOfTimePeriod; $j++)
////				{
////					$FinalStartTime = $arrFinalStartTime[$j];
////					$FinalEndTime = $arrFinalEndTime[$j];
////					if(sizeof($arrFinalTimeSlotID)>0){
////						$TimeSlotID = $arrFinalTimeSlotID[$j];
////					}else{
////						$TimeSlotID = "";
////					}
////					$sql = "INSERT INTO 
////									INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD 
////									(RecordDate, TimeSlotID, StartTime, EndTime, RepeatType, RepeatValue, RelatedPeriodID, RelatedItemID, RelatedSubLocationID, InputBy, ModifiedBy, RecordType, RecordStatus, DateInput, DateModified) 
////								VALUES 
////									('$targetRecordDate','$TimeSlotID','$FinalStartTime','$FinalEndTime','$RepeatOption','$final_repeat_value','','0','0',$UserID,$UserID,NULL,NULL,NOW(),NOW())";
////					$result['CreateBookingPeriod'][] = $lcycleperiods->db_db_query($sql);
////					
////					if($i==0 && $j==0)
////					{
////						$parent_period_id = $lcycleperiods->db_insert_id();
////						$child_period_id = $lcycleperiods->db_insert_id();
////					}
////					else
////					{
////						$child_period_id = $lcycleperiods->db_insert_id();
////					}
////					
////					$sql = "UPDATE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD SET RelatedPeriodID = '$parent_period_id' WHERE PeriodID = $child_period_id";
////					$result['UpdateBookingPeriodRelation'][] = $lcycleperiods->db_db_query($sql);
////				}
//			}
//		}
//	}
//}
if($Overwrite == 1)
{
	$Success = $lebooking->Separate_Booking_Period_By_DateArr($array_targetDate, $FacilityType, $FacilityID, $IsSuspension);	
}

if(!$IsSuspension)
	$result = $lebooking->Insert_Update_Booking_Available_Period($FacilityType, $FacilityID, $array_targetDate, $arrFinalStartTime, $arrFinalEndTime, $arrFinalTimeSlotID, $RepeatOption, $final_repeat_value);
else
	$result = $lebooking->Insert_Update_Booking_Day_Suspension($FacilityType, $FacilityID, $array_targetDate, $RepeatOption, $final_repeat_value);
	
//debug_pr($lcycleperiods->returnArray("SELECT * FROM INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD WHERE RelatedItemID = 0 AND  RelatedSubLocationID = 0"));
//$lcycleperiods->RollBack_Trans();
//die;

if($result)
{
	//echo "Booking Period Created Successfully.";
	echo 1;
	$lcycleperiods->Commit_Trans();
}
else
{
	echo -1;
	$lcycleperiods->RollBack_Trans();
}

intranet_closedb();
?>