<?php
//using : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_BookingPeriodSettings";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$lebooking->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'], "general_available_period.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'], "specific_available_period.php?clearCoo=1", 1);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($msg);

echo $lebooking_ui->Get_Copy_Available_Period_Confirm_UI($AcademicYearID, $FacilityType, $FacilityID, $CopyArr);

intranet_closedb();
$linterface->LAYOUT_STOP();
?>

