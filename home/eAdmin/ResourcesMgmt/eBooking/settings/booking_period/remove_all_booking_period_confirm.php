<?php
//using : Ronald
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_BookingPeriodSettings";
$linterface = new interface_html();
$linventory = new libinventory();
$lebooking_ui = new libebooking_ui();

if($FacilityType == 0){
	$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'], "general_available_period.php?clearCoo=1", 1);
	$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'], "specific_available_period.php?clearCoo=1", 0);
}else{
	$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'], "general_available_period.php?clearCoo=1", 0);
	$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'], "specific_available_period.php?clearCoo=1", 1);
}
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SchoolYearInfo = getAcademicYearByAcademicYearID($AcademicYearID,$intranet_session_language);

if($FacilityType == 1){
	$sql = "SELECT 
				CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
			FROM
				INVENTORY_LOCATION_BUILDING as building
				INNER JOIN INVENTORY_LOCATION_LEVEL as floor on (building.BuildingID = floor.BuildingID)
				INNER JOIN INVENTORY_LOCATION as room on (floor.LocationLevelID = room.LocationLevelID)
			WHERE
				room.LocationID = $FacilityID";
}else if($FacilityType == 2){
	$sql = "SELECT 
				CONCAT(".$linventory->getInventoryNameByLang("cat.").",' > ',".$linventory->getInventoryNameByLang("sub_cat.").",' > ',".$linventory->getInventoryNameByLang("item.").")
			FROM
				INVENTORY_CATEGORY as cat
				INNER JOIN INVENTORY_CATEGORY_LEVEL2 as sub_cat on (cat.CategoryID = sub_cat.CategoryID)
				INNER JOIN INTRANET_EBOOKING_ITEM as item on (sub_cat.Category2ID = item.Category2ID)
			WHERE
				item.ItemID = $FacilityID";
}
$arrFacilityName = $linventory->returnVector($sql);

if($FacilityType == 0){
	$AvaliableBookingPeriodResult = $lebooking_ui->retriveAllAvailableBookingPeriodBySchoolYearID($AcademicYearID);
}else{
	$AvaliableBookingPeriodResult = $lebooking_ui->retriveAllAvailableBookingPeriodBySchoolYearID($AcademicYearID, $FacilityType, $FacilityID);
}

if(sizeof($AvaliableBookingPeriodResult)>0)
{
	for($i=0; $i<sizeof($AvaliableBookingPeriodResult); $i++)
	{
		list($period_id, $targert_date, $start_time, $end_time, $repeat_type, $repeat_value, $related_period_id) = $AvaliableBookingPeriodResult[$i];
		
		$arrRelatedPeriodID[] = $related_period_id;
		$arrTargetPeriodID[$related_period_id][] = $period_id;
		$arrTargetDate[$related_period_id][] = $targert_date;
		$arrTargetStartTime[$related_period_id][] = $start_time;
		$arrTargetEndTime[$related_period_id][] = $end_time;
		$arrTargetRepeatType[$related_period_id][] = $repeat_type;
		$arrTargetRepeatValue[$related_period_id][] = $repeat_value;
	}
	$arrRelatedPeriodID = array_unique($arrRelatedPeriodID);
}
else
{
	$arrRelatedPeriodID = array();
}
$no_of_RelatedBookingPeriod = sizeof($arrRelatedPeriodID);


$table_content .= '<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">';
$table_content .= '<tr>';
$table_content .= '<td align="center">'.$linterface->GET_WARNING_TABLE($Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['RemoveAll']['WarningMsg']['AllBookingPeriodWillBeDeleted']).'</td>';
$table_content .= '</tr>';
$table_content .= '<tr><td>';
$table_content .= '<table width="70%" border="0" cellpadding="3" cellspacing="0" align="right">';
$table_content .= '<tr>';
$table_content .= '<td>'.$Lang['General']['SchoolYear'].'</td><td>'.$SchoolYearInfo.'</td>';
$table_content .= '</tr>';
if(($arrFacilityName[0] != "") && ($FacilityType == 1)){
	$table_content .= '<tr>';
	$table_content .= '<td>'.$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'].'</td><td>'.$arrFacilityName[0].'</td>';
	$table_content .= '</tr>';
}else if(($arrFacilityName[0] != "") && ($FacilityType == 2)){
	$table_content .= '<tr>';
	$table_content .= '<td>'.$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'].'</td><td>'.$arrFacilityName[0].'</td>';
	$table_content .= '</tr>';
}
$table_content .= '<tr>';
$table_content .= '<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['RemoveAll']['TotalNumOfBookingPeriodRule'].'</td><td>'.$no_of_RelatedBookingPeriod.'</td>';
$table_content .= '</tr>';
$table_content .= '</table>';
$table_content .= '</td></tr>';
$table_content .= '<tr>';
$table_content .= '<td align="center">';
$table_content .= '<div class="edit_bottom_v30">';
$table_content .= '<p class="spacer"></p>';
$table_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['DeleteAll'], "submit", "JavaScript:checkForm();", "", "", "", "formbutton_alert");
$table_content .= '&nbsp;';
if($FacilityType == 1){
	//$table_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "JavaScript:window.location='specific_available_period.php?AcademicYearID=$AcademicYearID&FacilityType=$FacilityType&targetRoom=$FacilityID'", "", "", "", "formbutton_v30");
	$table_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "JavaScript:GoBack($FacilityType);", "", "", "", "formbutton_v30");
}else if($FacilityType == 2){
	//$table_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "JavaScript:window.location='specific_available_period.php?AcademicYearID=$AcademicYearID&FacilityType=$FacilityType&targetItem=$FacilityID'", "", "", "", "formbutton_v30");
	$table_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "JavaScript:GoBack($FacilityType);", "", "", "", "formbutton_v30");	
}else{
	//$table_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "JavaScript:window.location='specific_available_period.php?AcademicYearID=$AcademicYearID", "", "", "", "formbutton_v30");
	$table_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "JavaScript:GoBack($FacilityType);", "", "", "", "formbutton_v30");
}
$table_content .= '<p class="spacer"></p>';
$table_content .= '</td>';
$table_content .= '</tr>';
$table_content .= '</table>';
if($FacilityType == 1){
	$table_content .= "<input type='hidden' name='FacilityType' id='FacilityType' value='$FacilityType'>";
	$table_content .= "<input type='hidden' name='targetRoom' id='targetRoom' value='$FacilityID'>";
}else if($FacilityType == 2){
	$table_content .= "<input type='hidden' name='FacilityType' id='FacilityType' value='$FacilityType'>";
	$table_content .= "<input type='hidden' name='targetItem' id='targetItem' value='$FacilityID'>";
}

?>
<SCRIPT language="JavaScript">
function checkForm()
{
	if(confirm("<?=$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['RemoveAll']['JSWarning']['DeleteAllBookingPeriod'];?>"))
	{
		document.form1.action = "remove_all_booking_period.php";
		document.form1.method = "POST";
		document.form1.submit();
	}
	else
	{
		return false;
	}
}

function GoBack(FacilityType)
{
	if(FacilityType == 0)
		document.form1.action = "general_available_period.php";
	else
		document.form1.action = "specific_available_period.php";
	
	document.form1.submit();
}
</SCRIPT> 
<br>

<form name="form1" id="form1" method="POST" action="">
<?=$table_content;?>
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID;?>"
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>