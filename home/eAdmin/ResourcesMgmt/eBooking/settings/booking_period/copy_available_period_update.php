<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_BookingPeriodSettings";

$linterface 	= new interface_html();
$li = new libebooking_ui();

$li->Check_Page_Permission();

$CopyArr = unserialize(urldecode($CopyArr));
foreach($CopyArr as $thisFacilityType => $FacilityIDArr)
{
	foreach($FacilityIDArr as $thisFacilityID)
	{
		$CopyToArr[] = array($thisFacilityType, $thisFacilityID);		
	}	
}

$li->Start_Trans();

$Result = $li->Copy_Booking_Period_Setting($AcademicYearID, $FacilityType, $FacilityID, $CopyToArr);

if($Result)
	$li->Commit_Trans();
else
	$li->RollBack_Trans();


$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'], "general_available_period.php?clearCoo=1", 0);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'], "specific_available_period.php?clearCoo=1", 1);
$MODULE_OBJ = $li->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($msg);

echo $li->Get_Copy_Available_Period_Result_UI($AcademicYearID, $FacilityType, $FacilityID, $CopyToArr, $Result);

intranet_closedb();
$linterface->LAYOUT_STOP();


intranet_closedb();
?>