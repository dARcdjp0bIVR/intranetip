<?php
// Using : 

##################################
#
#   Date:   2019-11-20 Cameron
#           set default currentView = 1 (Special Booking) for $sys_custom['eBooking']['EverydayTimetable']
#
#   Date:   2019-06-26 Cameron
#           set default currentView = 2 (Suspension) for $sys_custom['eBooking']['EverydayTimetable'] <- revised on 2019-11-20
#
#	Date:	2015-10-12	Omas
#			update js function js_Save_TimeZone_Booking_Period(), fixed double submission problem - disable button after submit
#
#	Date:	2011-02-25	YatWoon
#			update js function js_Check_Date_Range_Valid(), fixed incorrect date compare method
#
##################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_BookingPeriodSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$lebooking->Check_Page_Permission();

$specific_available_period?$specific_page=1:$general_page=1;
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'], "general_available_period.php?clearCoo=1", $general_page);
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'], "general_available_period.php?specific_available_period=1&clearCoo=1", $specific_page);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

if($AcademicYearID == "")
{
	$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
}

$linterface->LAYOUT_START();

if($specific_available_period)
{
	$FacilityType = $FacilityType?$FacilityType:1;
	$filter = $lebooking_ui->Get_Facility_Type_Selection("FacilityType", $FacilityType, " onChange='this.form.submit();' ");

	$x .= '<table border="0" cellpadding="3" cellspacing="3" width="96%">';
		$x .= '<tr><td valign="top" width="15%" nowrap="nowrap" class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['FacilityType'].'</td><td>'.$filter.'</td></tr>';

	if($FacilityType == 1)
	{
		$facility_filter = $lebooking_ui->Get_Booking_Location_Selection("FacilityID",$FacilityID,"  onChange='ChangeSchoolYear();' ",0,0,1);
		$x .= "<tr><td valign='top' width='15%' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eBooking']['General']['FieldTitle']['Location']."</td><td>$facility_filter</td></tr>";
	}
	else
	{
		$facility_filter = $lebooking_ui->Get_Booking_Item_Selection("FacilityID",$FacilityID,"  onChange='ChangeSchoolYear();' ",0,1);
		$x .= "<tr><td valign='top' width='15%' nowrap='nowrap' class='formfieldtitle tabletext'>".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ItemName']."</td><td>".$facility_filter."</td></tr>";
	}
	$x .= '</table>';
}
else
{
	$x = '<input type="hidden" id="FacilityType" name="FacilityType" value=0>';
	$x .= '<input type="hidden" id="FacilityID" name="FacilityID" value=0>';
}

?> 

<br>
<form name='form1' action='' method='POST'>
			<?=$x?>
	
	<table border='0' cellpadding='3' cellspacing='3' width='96%'>
			<tr><td colspan='2'><?=$lebooking_ui->getBookingPeriodCalendar($AcademicYearID);?></td></tr>
	</table>
</form>

<?//$lebooking_ui->getBookingPeriodCalendar($CurrentAcademicYearID);?>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

<script type="text/javascript">
	
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "targetBuilding";
	arrCookies[arrCookies.length] = "targetFloor";
	arrCookies[arrCookies.length] = "targetRoom";
	arrCookies[arrCookies.length] = "pageNo";
	arrCookies[arrCookies.length] = "numPerPage";
	
	var currentView = 0;
<?php
if ($sys_custom['eBooking']['EverydayTimetable']) {
    echo "currentView = 1;";
}
?>
	$(document).ready( function() {
		<? if($clearCoo) { ?>
			for(i=0; i<arrCookies.length; i++)
			{
				var obj = arrCookies[i];
				$.cookies.del(obj);
			}
		<? } ?>
		ChangeSchoolYear();
	});
	
	// show add/edit Thickbox
	function js_Show_BookingPeriod_Add_Layer(period_id, isSuspension)
	{
		var PeriodID = period_id || "";
		var isSuspension = isSuspension || "";

		var FacilityType = $('#FacilityType').val();
		var FacilityID = $('#FacilityID').val();
		
		$.post(
			"ajax_show_avaliable_period.php",
			{
				"SchoolYearID" : $('#academic_year').val(),
				"PeriodID" : PeriodID,
				"IsSuspension" : isSuspension,
				"FacilityType" : FacilityType,
				"FacilityID" : FacilityID
			},
			function(responseText){
				$('#TB_ajaxContent').html(responseText);
				
				$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
				$('#StartDate').datepick({
					dateFormat: 'yy-mm-dd',
					dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
					changeFirstDay: false,
					firstDay: 0
				});
				$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
				$('#EndDate').datepick({
					dateFormat: 'yy-mm-dd',
					dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
					changeFirstDay: false,
					firstDay: 0
				});
			}
		);
	}
	
	// AJAX function - used to change the repeat selection box base on the date range and repeat option
	function changeRepeatFrequency(val)
	{
		$("#div_weekday").hide();
		$("#div_cycleday").hide();
		$("#div_repeat_option1").hide();
		$("#div_repeat_option2").hide();
		
		if(val == 2)
		{
			$("#div_repeat_option1").show();
			$("#div_repeat_option2").show();
			$("#div_weekday").show();
		}
		if(val == 3){
			if(($("#StartDate").val() == "") && ($("#EndDate").val() == "")) {
				alert("<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InputDateRange'];?>");
				$("#RepeatOption").val("0");
			} else {
				$.post(
					"ajax_retriveCycleDayTextByDateRange.php",
					{
						"StartDate" : $("#StartDate").val(),
						"EndDate" : $("#EndDate").val()
					},
					function(responseText){
						$("#div_cycleday").html(responseText);
						$("#div_repeat_option1").show();
						$("#div_repeat_option2").show();
						$("#div_cycleday").show();
					}
				);
			}
		}
	}
	
	// AJAX function - used to change the cycle day repeat option when date range is changed
	function changeCycleDaySelection()
	{
		if($("#div_cycleday").is(":visible"))
		{
			if(($("#StartDate").val() == "") && ($("#EndDate").val() == "")) 
			{
				alert("<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InputDateRange'];?>");
				$("#RepeatOption").val("0");
			}
			else 
			{
				$.post(
					"ajax_retriveCycleDayTextByDateRange.php",
					{
						"StartDate" : $("#StartDate").val(),
						"EndDate" : $("#EndDate").val()
					},
					function(responseText){
						$("#div_cycleday").html(responseText);
						$("#div_repeat_option1").show();
						$("#div_repeat_option2").show();
						$("#div_cycleday").show();
					}
				);
			}
		}
	}
	
	// AJAX function - used to change the time slot option when date range is changed
	function changeTimeSlot()
	{
		$("#div_warning_msg").html("");
		//if($("#AvaliableTimeSlot").is(":visible"))
		//{
		
		var checkTimeZone, checkAcademicYear;
		if ($('#TimeTableSlot').attr('checked')) {
			checkTimeZone = 1;
			checkAcademicYear = 1;
		}
		else {
			// do not check time-zone settings if using specific time range
			checkTimeZone = 0;
			checkAcademicYear = 1;
		}
		
		$.post(
			"ajax_TimeSlotValidation.php",
			{
				"StartDate" : $("#StartDate").val(),
				"EndDate" : $("#EndDate").val(),
				"SchoolYearID" : $("#academic_year").val(),
				"CheckTimeZone" : checkTimeZone,
				"CheckAcademicYear" : checkAcademicYear
			},
			function(responseText){
				if(responseText != "")
				{
					$("#div_warning_msg").html("<font color='red'>"+responseText+"</font>");
				}
			}
		);
			
		//}
	}
	
//	function retriveUpdatedTimeSlotSelection()
//	{
//		$.post(
//			"ajax_retrive_updated_time_slot_selection.php",
//			{
//				"StartDate" : $("#StartDate").val(),
//				"EndDate" : $("#EndDate").val()
//			},
//			function(responseText){
//				if(responseText != "NO_TIMETABLE")
//				{
//					$("#div_time_slot_selection1").show();
//					$("#div_time_slot_selection2").show();
//					$("#div_time_slot_selection3").show();
//					$("#div_time_slot_selection3").html(responseText);
//				}
//				else
//				{
//					$("#div_time_slot_selection1").hide();
//					$("#div_time_slot_selection2").hide();
//					$("#div_time_slot_selection3").hide();
//				}
//			}
//		);
//	}
	
	function retriveUpdatedTimeSlotSelection()
	{
		$.post(
			"ajax_retrive_updated_time_slot_selection.php",
			{
				"StartDate" : $("#StartDate").val(),
				"EndDate" : $("#EndDate").val()
			},
			function(responseText){
				if(responseText != "NO_TIMETABLE")
				{
					$("#DIV_TimeTableSlot").children().find('select').remove()
					$("#DIV_TimeTableSlot").children().prepend(responseText)
					if($("#TimeTableSlot").attr('checked'))
						changeTimeOption(0)
					rearrangeSelectionIDName();
				}
				else
				{
				}
			}
		);
	}
	
	// JS function - used to control the start time & end time / time slot selection box disable or not
	function changeTimeOption(val)
	{
		if(val == 1){
			$("#DIV_SpecifyTime").children().each(function() {
            	$(this).find("select").each(function() {
            		$(this).removeAttr("disabled");
				});
			});
			$('#BTN_SpecifyTime').show();

			$("#DIV_TimeTableSlot").children().each(function() {
            	$(this).find("select").each(function() {
            		$(this).attr("disabled","disabled");
				});
			});
			$('#BTN_TimeTableSlot').hide();
		}
		
		if(val == 0){
			$("#DIV_SpecifyTime").children().each(function() {
            	$(this).find("select").each(function() {
            		$(this).attr("disabled","disabled");
				});
			})
			$('#BTN_SpecifyTime').hide();

			$("#DIV_TimeTableSlot").children().each(function() {
            	$(this).find("select").each(function() {
            		$(this).removeAttr("disabled");
				});
			});
			$('#BTN_TimeTableSlot').show();
		}
		
		changeTimeSlot();
	}
	
	// JS function - Compare start date and end date 
	function js_Check_Date_Range_Valid(StartDate, EndDate){
		var str1 = StartDate;
		var str2 = EndDate;
		
		/*
		var start_year  = parseInt(str1.substring(0,4),10);
		var start_mon =  parseInt(str1.substring(3,5),10);
		var start_day  = parseInt(str1.substring(8,10),10);
		var end_year  = parseInt(str2.substring(0,4),10);
		var end_mon = parseInt(str2.substring(3,5),10);
		var end_day  = parseInt(str2.substring(8,10),10);
		var date1 = new Date(start_year, start_mon, start_day);
		var date2 = new Date(end_year, end_mon, end_day);
		
		
		if(date2 < date1)
		{	
			alert("<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidDateRange'];?>");
        	return false;
		}
		else
		{
			return true;
		} 
		
		*/
		
		if(compareDate(str2,str1) < 0)
		{	
			alert("<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidDateRange'];?>");
        	return false;
		}
		else
		{
			return true;
		} 
		
	}
	
	// AJAX function - insert/update the avaliable booking period
	var ReturnMsgArr = new Object();
	ReturnMsgArr['BookingPeriodCreatedSuccess'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Created']['Success']?>";
	ReturnMsgArr['BookingPeriodCreatedFail'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Created']['Fail']?>";
	ReturnMsgArr['BookingPeriodEditSuccess'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Edit']['Success']?>";
	ReturnMsgArr['BookingPeriodEditFail'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Edit']['Fail']?>";
	ReturnMsgArr['BookingPeriodDeletedSuccess'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Deleted']['Success']?>";
	ReturnMsgArr['BookingPeriodDeletedFail'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriod']['Deleted']['Fail']?>";
	ReturnMsgArr['BookingPeriodAssign'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['BookingPeriod']['Assign']?>";
	
	ReturnMsgArr['DaySuspensionCreatedSuccess'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Created']['Success']?>";
	ReturnMsgArr['DaySuspensionCreatedFail'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Created']['Fail']?>";
	ReturnMsgArr['DaySuspensionEditSuccess'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Edit']['Success']?>";
	ReturnMsgArr['DaySuspensionEditFail'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Edit']['Fail']?>";
	ReturnMsgArr['DaySuspensionDeletedSuccess'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Deleted']['Success']?>";
	ReturnMsgArr['DaySuspensionDeletedFail'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['DaySuspension']['Deleted']['Fail']?>";
	ReturnMsgArr['DaySuspensionAssign'] = "<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DaySuspension']['Assign']?>"; 

	function js_Insert_Edit_AvaliableBookingPeriod(targetPeriodID, IsSuspension)
	{
		var PeriodID = targetPeriodID || "";
		var IsSuspension = IsSuspension || "";
		var LangKey1 = IsSuspension?"DaySuspension":"BookingPeriod";
		var LangKey2 = PeriodID?"Edit":"Created";
		var StartDate = $("#StartDate").val();
		var EndDate = $("#EndDate").val();
		var FacilityType = $("#FacilityType").val();
		var FacilityID = $("#FacilityID").val();
		
		var arrStartTime = new Array;
		var arrEndTime = new Array; 
		var arrTimeSlot = new Array;
		var strStartHour = "";
		var strStartMin = "";
		var strEndHour = "";
		var strEndMin = "";
		var NoOfTimePeriod = "";
		
		if(!IsSuspension)
		{
			if($("input[name=TimeSelection]:checked").val()==1) 
			{
				// combine all the selected specify time
				$("#DIV_SpecifyTime").children("").each(function(i) {
					$(this).find("select").each(function(j) {
						if(j == 0){
							strStartHour = $(this).val();
							if(strStartHour < 10)
								strStartHour = '0'+strStartHour;
						}
						if(j == 1){
							strStartMin = $(this).val();
							if(strStartMin < 10)
								strStartMin = '0'+strStartMin;
	
							var strStartTime = strStartHour+":"+strStartMin;
							arrStartTime.push(strStartTime);
						}
						if(j == 2){
							strEndHour = $(this).val();
							if(strEndHour < 10)
								strEndHour = '0'+strEndHour;
						}
						if(j == 3){
							strEndMin = $(this).val();
							if(strEndMin < 10)
								strEndMin = '0'+strEndMin;
	
							var strEndTime = strEndHour+":"+strEndMin;
							arrEndTime.push(strEndTime);
						}
					});
					NoOfTimePeriod = i+1;
				});
			}
			else
			{
				//combine all the selected Time Table Slot
				$("#DIV_TimeTableSlot").children("").each(function(i) {
					$(this).find("select").each(function(j) {
						arrTimeSlot.push($(this).val());
						NoOfTimePeriod = i+1;
					});
				});
			}
		}
		
		if(js_Check_Date_Range_Valid(StartDate, EndDate))
		{
			if($("#RepeatOption").val()==2) 
			{
				var tmp_weekday = document.getElementsByName('weekday[]');
				var arrWeekday = new Array;
				
				for(var i=0; i < tmp_weekday.length; i++)
				{
					if(tmp_weekday[i].checked)
					{
						arrWeekday.push(tmp_weekday[i].value);
					}
				}
			}
			else if($("#RepeatOption").val()==3) 
			{
				var tmp_cycleday = document.getElementsByName('cycleday_text[]');
				var arrCycledayText = new Array;
				
				for(var i=0; i < tmp_cycleday.length; i++)
				{
					if(tmp_cycleday[i].checked)
					{
						arrCycledayText.push(tmp_cycleday[i].value);
					}
				}
			}
			
			// check any duplicate StartTime and EndTime
			$.post(
				"ajax_time_range_validation.php",
				{
					"StartDate" : StartDate,
					"EndDate" : EndDate,
					"ArrayWeekday[]" : arrWeekday,
					"ArrayCycleday[]" : arrCycledayText,
					"ArrayStartTime[]" : arrStartTime,
					"ArrayEndTime[]" : arrEndTime,
					"ArrayTimeSlot[]" : arrTimeSlot,
					"PeriodID" : PeriodID,
					"FacilityType" : FacilityType,
					"FacilityID" : FacilityID,
					"RepeatOption" : $("#RepeatOption").val(),
					"NoOfTimePeriod" : NoOfTimePeriod,
					"IsSuspension" :　IsSuspension
				},
				function(responseText)
				{
					if(responseText == -1)
					{
						alert ("<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidTimeSelection']?>");
					}
					else if(responseText == 1 || responseText == 0)
					{
					 	if($("#div_warning_msg").html() != "")
					 		return false;
					 	
						if(responseText==0)
						{
							var Overwrite = 1;
							if(!confirm(ReturnMsgArr[LangKey1+'Assign']))
								return false;
						}	
						else	
							var Overwrite = 0; 
							
						$.post(
							"ajax_UpdateBookingPeriod.php",
							{
								"StartDate" : StartDate,
								"EndDate" : EndDate,
								"ArrayWeekday[]" : arrWeekday,
								"ArrayCycleday[]" : arrCycledayText,
								"ArrayStartTime[]" : arrStartTime,
								"ArrayEndTime[]" : arrEndTime,
								"ArrayTimeSlot[]" : arrTimeSlot,
								"PeriodID" : PeriodID,
								"FacilityType" : FacilityType,
								"FacilityID" : FacilityID,
								"RepeatOption" : $("#RepeatOption").val(),
								"NoOfTimePeriod" : NoOfTimePeriod,
								"Overwrite" : Overwrite,
								"IsSuspension" : IsSuspension 
							},
							function(responseText)
							{
								if(responseText == 1)
								{
									var msg = ReturnMsgArr[LangKey1+LangKey2+'Success'];
								}
								else if(responseText == -1)
								{
									var msg = ReturnMsgArr[LangKey1+LangKey2+'Fail'];
								}
								else
								{
									return false;
								}
								
								window.top.tb_remove();
								ChangeSchoolYear()
								Get_Return_Message(msg);
								
							}
						);	
					} // end if(responseText == 1 || responseText == 0)
				}
				
			);
		}
	}
	
	//AJAX function - delete booking period
	function js_DeleteBookingPeriod(PeriodID, IsSuspension)
	{
		var IsSuspension = IsSuspension||"";
		var LangKey1 = IsSuspension?"DaySuspension":"BookingPeriod";
		var answer = confirm("<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSMsg']['SureToDeleteBookingPerion'];?>")
		if(answer)
		{
			$.post(
				"ajax_DeleteBookingPeriod.php",
				{
					"PeriodID" : PeriodID, 
					"IsSuspension": IsSuspension
				},
				function(responseText) {
					if(responseText == 1)
					{
						ChangeSchoolYear()
						var msg = ReturnMsgArr[LangKey1+'DeletedSuccess'];
						Get_Return_Message(msg);
					}
					else
					{
						ChangeSchoolYear()
						var msg = ReturnMsgArr[LangKey1+'DeletedFail'];
						Get_Return_Message(msg);
					}
				}
			);
		}
	}

	// AJAX function - change school year => change calendar and condition table
	function ChangeSchoolYear()
	{
		var FacilityType = $('#FacilityType').val();
		var FacilityID = $('#FacilityID').val();
		var schoolYearID = $("#academic_year").val();
		
		Block_Document();
		$.post(
			"ajax_change_school_year.php",
			{
				"targetSchoolYearID":schoolYearID,
				"FacilityType" : FacilityType,
				"FacilityID" : FacilityID
			},
			function(responseText){
				UnBlock_Document();
				$('#main_body').html(responseText);
				js_Change_Setting_View(currentView);
				
			}
		);
	}
	
	function addElement()
	{
		if($("#SpecifyTime").attr('checked'))
		{
			$("#DIV_SpecifyTime").children(":first").clone().each(function() {
				var index = $("#DIV_SpecifyTime").children().length + 1;
				$(this).attr("id","ROW_SpecifyTime_"+index); 				// set DIV id 		
            	$(this).find("select").each(function() {					// set the attr for select element
            		$(this).attr("id", $(this).attr("id").substring(0,$(this).attr("id").lastIndexOf("_")+1)+index);
            		$(this).attr("name", $(this).attr("name").substring(0,$(this).attr("name").lastIndexOf("_")+1)+index);
            		$(this).removeAttr("disabled");
				});
				$(this).find("a").each(function() {							// set the attr for hyperlink element
            		$(this).attr("href", "javascript: removeElement('"+index+"');" );
            		$(this).show();
				});
			}).appendTo("#DIV_SpecifyTime");
		}
		else if($("#TimeTableSlot").attr('checked'))  
		{
			$("#DIV_TimeTableSlot").children(":first").clone().each(function() {
				var index = $("#DIV_TimeTableSlot").children().length + 1;
				$(this).attr("id","ROW_TimeTableSlot_"+index); 				// set DIV id 		
            	$(this).find("select").each(function() {					// set the attr for select element
            		$(this).attr("id", $(this).attr("id").substring(0,$(this).attr("id").lastIndexOf("_")+1)+index);
            		$(this).attr("name", $(this).attr("name").substring(0,$(this).attr("name").lastIndexOf("_")+1)+index);
            		$(this).removeAttr("disabled");
				});
				$(this).find("a").each(function() {							// set the attr for hyperlink element
            		$(this).attr("href", "javascript: removeElement('"+index+"');" );
            		$(this).show();
				});
			}).appendTo("#DIV_TimeTableSlot");
		}
	}
	
//	function removeElement(div_id)
//	{
//		if($("#SpecifyTime").attr('checked'))
//		{
//			$("#ROW_SpecifyTime_"+div_id).remove();
//			$("#DIV_SpecifyTime").children().each(function(i) {
//				var index = i + 1;
//				$(this).attr('id', 'ROW_SpecifyTime_'+index);
//				$(this).find("select").each(function() {			// set the attr for select element
//            		$(this).attr("id", $(this).attr("id").substring(0,$(this).attr("id").lastIndexOf("_")+1)+index);
//            		$(this).attr("name", $(this).attr("name").substring(0,$(this).attr("name").lastIndexOf("_")+1)+index);
//				});
//				$(this).find("a").each(function() {							// set the attr for hyperlink element
//            		$(this).attr("href", "javascript: removeElement('"+index+"');" );
//				});
//			});
//		}
//		else if($("#TimeTableSlot").attr('checked'))  
//		{
//			$("#ROW_TimeTableSlot_"+div_id).remove();
//			$("#DIV_TimeTableSlot").children().each(function(i) {
//				var index = i + 1;
//				$(this).attr('id', 'ROW_TimeTableSlot_'+index);
//				$(this).find("select").each(function() {			// set the attr for select element
//            		$(this).attr("id", $(this).attr("id").substring(0,$(this).attr("id").lastIndexOf("_")+1)+index);
//            		$(this).attr("name", $(this).attr("name").substring(0,$(this).attr("name").lastIndexOf("_")+1)+index);
//				});
//				$(this).find("a").each(function() {							// set the attr for hyperlink element
//            		$(this).attr("href", "javascript: removeElement('"+index+"');" );
//				});
//			});
//		}
//		$("#DIV_"+TimeName).children().each(function(i) {
//			var index = i + 1;
//			$(this).attr('id', 'ROW_'+TimeName+'_'+index);
//			$(this).find("select").each(function() {			// set the attr for select element
//        		$(this).attr("id", $(this).attr("id").substring(0,$(this).attr("id").lastIndexOf("_")+1)+index);
//        		$(this).attr("name", $(this).attr("name").substring(0,$(this).attr("name").lastIndexOf("_")+1)+index);
//			});
//			$(this).find("a").each(function() {							// set the attr for hyperlink element
//        		$(this).attr("href", "javascript: removeElement('"+index+"');" );
//			});
//		});
//	}
	
	function removeElement(div_id)
	{
		var TimeName = $("#SpecifyTime").attr('checked')?"SpecifyTime":"TimeTableSlot";
		$("#ROW_"+TimeName+"_"+div_id).remove(); // remove row
		rearrangeSelectionIDName(); // rearrange the id/name
	}
	
	function rearrangeSelectionIDName()
	{
		var TimeName = $("#SpecifyTime").attr('checked')?"SpecifyTime":"TimeTableSlot";
		$("#DIV_"+TimeName).children().each(function(i) {
			var index = i + 1;
			$(this).attr('id', 'ROW_'+TimeName+'_'+index);
			$(this).find("select").each(function() {			// set the attr for select element
        		$(this).attr("id", $(this).attr("id").substring(0,$(this).attr("id").lastIndexOf("_")+1)+index);
        		$(this).attr("name", $(this).attr("name").substring(0,$(this).attr("name").lastIndexOf("_")+1)+index);
			});
			$(this).find("a").each(function() {							// set the attr for hyperlink element
        		$(this).attr("href", "javascript: removeElement('"+index+"');" );
			});
		});
	}
	
	function js_Show_BookingPeriod_TimeZone_Edit_Layer(TimeZoneID, SpecialTimetableSettingID)
	{
		var FacilityType = $("#FacilityType").val();
		var FacilityID = $("#FacilityID").val();
		
		$.post(
			"ajax_task.php",
			{
				task:"Get_BookingPeriod_TimeZone_Edit_Layer",
				TimeZoneID:TimeZoneID,
				SpecialTimetableSettingID:SpecialTimetableSettingID,
				FacilityType:FacilityType,
				FacilityID:FacilityID
			},
			function(responseText){
				$('#TB_ajaxContent').html(responseText);
			}
		);
		
	}
	
	function js_CheckAll_Class(ClassName)
	{
		var isChecked = $("#chk_all_"+ClassName).attr("checked");
		$("."+ClassName).attr("checked",isChecked);
	}
	
	function js_Save_TimeZone_Booking_Period(TimezoneSettingID, TimeZoneID)
	{
		$('#submitBtn').attr('disabled','disabled');
		
//		var FacilityType = $("#FacilityType").val();
//		var FacilityID = $("#FacilityID").val();
		var FormValue = Get_Form_Values(document.TBForm);
		$.post(
			"ajax_update.php?"+FormValue,
			{
				task:"Save_BookingPeriod_TimeZone_Edit_Layer"
			},
			function(responseText){
				Get_Return_Message(responseText);
				if(responseText.substring(0,1)==1)
				{
					js_Hide_ThickBox();
					ChangeSchoolYear()
				}
			}
		);
	}
	
	function js_DeleteTimeZoneBookingPeriod(TimezoneSettingID)
	{
		if(confirm("<?=$Lang['eBooking']['Settings']['DefaultSettings']['JSMsg']['SureToDeleteBookingPerion'];?>"))
		{
			$.post(
				"ajax_update.php",
				{
					task:"RemoveTimezoneBookingPeriod",
					TimezoneSettingID:TimezoneSettingID
				},
				function(responseText){
					Get_Return_Message(responseText);
					ChangeSchoolYear()
				}
			);
		}
	}
	
	function js_Toggle_Special_Timetable(no)
	{
		$("tr.SpecialTimeTable_"+no).toggle();
	}
	
	function js_Change_Setting_View(type)
	{
		view_idx = 2-type; 
		var FacilityType = $("#FacilityType").val();
		var FacilityID = $("#FacilityID").val();		
		$("a.thumb_list_tab_on ,#tablist_div").removeClass('thumb_list_tab_on');
		$("#tablist_div").find("a:eq("+view_idx+")").addClass('thumb_list_tab_on');
		
		Block_Element("SettingTableDiv");
		$.post(
			"ajax_task.php",
			{
				task:"Get_Booking_Table_Setting_View",
				SettingView:type,
				FacilityType:FacilityType,
				FacilityID:FacilityID,
				AcademicYearID:$('#academic_year').val()
			},
			function(ReturnData){
				$("div#SettingTableDiv").html(ReturnData);
				UnBlock_Element("SettingTableDiv");
				currentView = type;
				initThickBox();
			}
		)
	}
	
	//Specific Setting
	function js_Display_Setting_Table()
	{
		$(".BookingPeriodSetting").show();
		$("#SettingBtn").hide();
	}

	function js_Copy_From()
	{
		js_Show_ThickBox("<?=$Lang['eBooking']['Button']['FieldTitle']['Copy']?>", 500, 650);
		js_Show_SpecificBookingPeriod_Copy_Layer();
	}

	function js_Show_SpecificBookingPeriod_Copy_Layer()
	{

		var FacilityType = $('#FacilityType').val();
		var FacilityID = $('#FacilityID').val();
		
		if((FacilityType != "") && (FacilityID != ""))
		{
			$.post(
				"ajax_show_copy_specific_avaliable_period_layer.php",
				{
					"SchoolYearID" : $('#academic_year').val(),
					"FacilityType" : FacilityType,
					"FacilityID" : FacilityID
				},
				function(responseText){
					$('#TB_ajaxContent').html(responseText);
					js_Change_CopyFrom_Category()
				}
			);
		}
	}


	function js_Copy_To()
	{
		var FacilityType = $('#FacilityType').val();
		var FacilityID = $('#FacilityID').val();
		var AcademicYearID = $("#academic_year").val();
		window.location.href="copy_available_period_select.php?AcademicYearID="+AcademicYearID+"&FacilityType="+FacilityType+"&FacilityID="+FacilityID
	}
	
	function js_Remove_Setting()
	{
		var FacilityType = $('#FacilityType').val();
		var FacilityID = $('#FacilityID').val();
		var AcademicYearID = $("#academic_year").val();
		
		if(confirm("<?=$Lang['eBooking']['BookingPeriod']['ConfirmMessage']['RemoveFacilityBookingPeriod'][$FacilityType]?>"))
		{
			$.post(
				"ajax_update.php",
				{
					task:"RemoveFacilityBookingPeriod",
					FacilityType:FacilityType,
					FacilityID:FacilityID,
					AcademicYearID:AcademicYearID
				},
				function(responseText){
					Get_Return_Message(responseText);
					ChangeSchoolYear()
				}
			);
		}
	}

	function js_Change_CopyFrom_Category()
	{
		if($("#Layer_FacilityType").val()==0) // get booking period of default setting
		{
			$("#DIV_SelectTargetItemOrRoom").html("");
			js_Change_CopyFrom_Target(1)
		}
		else 
		{
			if($("#Layer_FacilityType").val() == $("#FacilityType").val())
				excludeFacilityID = $("#FacilityID").val()
			else
				excludeFacilityID = '';
				
			$.post(
				"ajax_task.php",
				{
					task : "ChangeCopyFromCategory",
					SchoolYearID : $("#SchoolYearID").val(),
					TargetCategoryID : $("#Layer_FacilityType").val(),
					excludeFacilityID : excludeFacilityID
				},
				function(responseText){
					$("#DIV_SelectTargetItemOrRoom").html(responseText);
					js_Change_CopyFrom_Target()
				}
			);
		}
	}
	
	function js_Change_CopyFrom_Target(isDefault)
	{
		var TargetFacilityID, TargetCategoryID;
		if(isDefault == 1)
		{
			TargetFacilityID = 0;
			TargetCategoryID = 0;
		}
		else
		{
			TargetFacilityID = $("#Layer_FacilityID").val();
			TargetCategoryID = $("#Layer_FacilityType").val();
		}
		
		if(TargetCategoryID != 0 && TargetFacilityID == "")
		{
			$("#DIV_SpecificBookingPeriodInfo").html("");
		}
		else
		{
			$.post(
				"ajax_task.php",
				{
					task : "ShowSelectTargetBookingPeriod",
					SchoolYearID : $("#SchoolYearID").val(),
					TargetCategoryID : TargetCategoryID,
					TargetFacilityID : TargetFacilityID
				},
				function(responseText){
					$("#DIV_SpecificBookingPeriodInfo").html(responseText);
				}
			);
		}
	}
	
	function js_Change_CopyFrom_Confirm()
	{
		var CopyFromType = $("#Layer_FacilityType").val();
		var CopyToType = $("#FacilityType").val();
		var CopyFromFacilityID = $("#Layer_FacilityID").val();
		var CopyToFacilityID = $('#FacilityID').val();
		
		if(CopyFromFacilityID != "" || CopyFromType == 0)
		{
			if(confirm("<?=$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['JSWarning']['AreYouSureWantToCopy'];?>")){
				if(CopyFromType == 0)
					CopyFromFacilityID = 0;

				$.post(
					"ajax_task.php",
					{
						task : "CopyStart",
						SchoolYearID : $("#SchoolYearID").val(),
						CopyFromType : CopyFromType,
						CopyFromFacilityID : CopyFromFacilityID,
						CopyToType : CopyToType,
						CopyToFacilityID : CopyToFacilityID
					},
					function(responseText){
						if(responseText == 1){
							var msg = '<?=$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['ReturnMsg']['BookingPeriodCopiedSuccessfully'];?>';
						}else{
							var msg = '<?=$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['ReturnMsg']['BookingPeriodCopiedFailed'];?>';
						}
						window.top.tb_remove();
						ChangeSchoolYear()
						Get_Return_Message(msg);
					}
				);
			}
		}
		else
		{
			alert("<?=$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['JSWarning']['PleaseSelectCopyTarget'];?>");
		}
	}
</script>