<?php
// using : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

### Cookies handling
# set cookies
/*
$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

//debug_r($arrCookies);
*/

if(isset($clearCoo) && $clearCoo == 1)
{
	if(isset($_COOKIE['booking_rule_pageNo'])) {
		$_COOKIE['booking_rule_pageNo'] = "";
		$cat_setting2_pageNo = "";
	}
	if(isset($_COOKIE['booking_rule_numPerPage'])) {
		$_COOKIE['booking_rule_numPerPage'] = "";
		$booking_rule_numPerPage = "";
	}
	if(isset($_POST['booking_rule_pageNo']))
	{
		$_POST['booking_rule_pageNo'] = "";
		$booking_rule_pageNo = "";
	}
}
else
{
	// for click 'Next page' & 'Prev page' use
	if(isset($_POST['booking_rule_pageNo']))
		$booking_rule_pageNo = $_POST['booking_rule_pageNo'];
}

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_UserBookingRule";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$li 			= new libdbtable2007($SortField, $Order, $PageNumber);

$lebooking->Check_Page_Permission();

# Default Table Settings
$booking_rule_pageNo = ($booking_rule_pageNo == '')? $li->pageNo=1 : $li->pageNo=$booking_rule_pageNo;
$booking_rule_numPerPage = ($booking_rule_numPerPage == '')? $li->page_size=20 : $li->page_size=$booking_rule_numPerPage;
$Order = ($Order == '')? 1 : $Order;
$SortField = ($SortField == '')? 0 : $SortField;

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$toolbar1 .= $linterface->GET_LNK_NEW("javascript:checkNew('user_booking_rules_insert_edit.php')",$Lang['eBooking']['Button']['FieldTitle']['NewRule'],"","","",0)."&nbsp;";

$toolbar1 .= $linterface->Get_Warning_Message_Box("", $Lang['eBooking']['Instruction']['WarnDeleteRule']);

$tabletool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'RuleID[]','user_booking_rules_insert_edit.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
$tabletool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$tabletool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RuleID[]','user_booking_rules_delete.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_delete
					</a>
				</td>";

//$table_content .= $lebooking_ui->Get_User_Booking_Rules_Table('RuleID[]');
$table_content .= $lebooking_ui->Get_User_Booking_Rules_Table('RuleID[]', 0, 0, '', array(), $booking_rule_numPerPage, $booking_rule_pageNo, $Order, $SortField);

$linterface->LAYOUT_START($msg);
?>

<script language='javascript'>
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "booking_rule_pageNo";
	arrCookies[arrCookies.length] = "booking_rule_numPerPage";
	
	$(document).ready( function() {	
		<? if($clearCoo) { ?>
			for(i=0; i<arrCookies.length; i++)
			{
				var obj = arrCookies[i];
				$.cookies.del(obj);
			}
		<? } ?>
	});
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				//alert("A: "+$('#'+arrCookies[i]).val());
				$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
			}
			else
			{
				//alert("B: "+$.cookies.get(arrCookies[i]));
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
</script>

<br>
<form name='form1' action='user_booking_rules.php' method='POST'>
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr height="10px"><td colspan="2"></td></tr>
	<tr>
		<td align="left"><?= $toolbar1 ?></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr height="3px"><td></td></tr>
	<tr class="table-action-bar">
		<td colspan="" valign="bottom" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr class="table-action-bar">
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$tabletool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?=$table_content;?>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>