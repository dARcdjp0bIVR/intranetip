<?
// Using:

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$cat_id_list = implode(",",$cat_id);

if($cat_id_list != "")
{
	# Check Any Items Exist #
	$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE CategoryID IN ($cat_id_list)";
	$arr_result = $linventory->returnArray($sql,1);

	if(sizeof($arr_result)>0)
	{
		header("location: category_setting.php?msg=13");
	}
	else
	{
		$sql = "DELETE FROM INVENTORY_PHOTO_PART WHERE CategoryID IN ($cat_id_list)";
		$result_1 = $linventory->db_db_query($sql);
		
		$sql = "DELETE FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID IN ($cat_id_list)";
		$result_2 = $linventory->db_db_query($sql);
		
		$sql = "DELETE FROM INVENTORY_CATEGORY WHERE CategoryID IN ($cat_id_list)";
		$result_3 = $linventory->db_db_query($sql);
		
		if($result_1 && $result_2 && $result_3)
		{
			header("location: category_setting.php?msg=3");
		}
		else
		{
			header("location: category_setting.php?msg=13");
		}
	}
}
else
{
	header("location: category_setting.php?msg=13");
}
intranet_closedb();
?>