<?php
// Using:

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################
/*
 *  2019-04-29 Cameron
 *      - fix potential sql injection problem by enclosed var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_auth();
intranet_opendb();

$lebooking		= new libebooking();
$lebooking->Check_Page_Permission();

$li 			= new libdb();
$lf				= new libfilesystem();
$linventory		= new libinventory();

$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE DisplayOrder = '$cat_display_order'";
$result = $linventory->returnVector($sql);
$existingCatID = $result[0];
if($existingCatID != "")
{
	$sql = "SELECT MAX(DisplayOrder)+1 FROM INVENTORY_CATEGORY";
	$result = $linventory->returnVector($sql);
	$new_order = $result[0];
	$sql = "UPDATE INVENTORY_CATEGORY SET DisplayOrder = $new_order WHERE CategoryID = '$existingCatID'";
	$linventory->db_db_query($sql);
}

if($sys_custom['eInventory_PriceCeiling']){
	$fields = "Code, NameChi, NameEng, DisplayOrder, PhotoLink, PriceCeiling, ApplyPriceCeilingToBulk, RecordStatus, DateInput, DateModified";
	$values = "'$cat_code', '$cat_chi_name', '$cat_eng_name', '$cat_display_order', '', '$price_ceiling', '$apply_to_bulk', '1', NOW(), NOW()";
}else{
	$fields = "Code, NameChi, NameEng, DisplayOrder, PhotoLink, RecordStatus, DateInput, DateModified";
	$values = "'$cat_code', '$cat_chi_name', '$cat_eng_name', '$cat_display_order', '', '1', NOW(), NOW()";
}
$sql = "INSERT INTO INVENTORY_CATEGORY ($fields) VALUES ($values)";
$result = $linventory->db_db_query($sql);

if($photo_usage == 0){
	$default_photo = "no_photo.jpg";
}

if ($result)
{
	$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '$cat_code'";
	$result = $linventory->returnArray($sql,1);
	if(sizeof($result)>0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			list($cat_id) = $result[$i];
		}
		
		if($default_photo == "none")
		{
			$lf = new libfilesystem();
			# Upload Photo #
			$re_path = "/file/photo/inventory";
			$path = "$intranet_root$re_path";
			if (!is_dir($path))
			{
				$lf->folder_new($path);
			}
			
			$filename = $_FILES['cat_photo']['name'];
			$ext = strtoupper($lf->file_ext($filename));
			
			if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG")
			{
				$target = $path."/".$filename;
				$result = $lf->lfs_copy($cat_photo, $target);
			}
	    }
	    else
	    {
		    $re_path = "/images/inventory/";
		    $filename = "/$default_photo";
	    }
			
	    $values = "($cat_id, '0', '0', '$re_path', '$filename', NOW(), NOW())";
		$sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
						(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES 
						$values";
		$li->db_db_query($sql);
		
		if($li->db_affected_rows($sql) > 0)
		{
			$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID = 0 AND ItemID = 0 ";
			
			$result = $li->returnArray($sql,1);
			
			if(sizeof($result)>0)
			{
				list($part_id) = $result[0];
			}
		}
		
		$sql = "UPDATE INVENTORY_CATEGORY SET PhotoLink = '$part_id' WHERE CategoryID = '$cat_id'";
		$linventory->db_db_query($sql);
	}
	header("Location: category_setting.php?msg=1");
}
else
{
	header("Location: category_setting.php?msg=12");
}

intranet_closedb();

?>