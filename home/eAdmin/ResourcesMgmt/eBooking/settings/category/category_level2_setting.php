<?php
// Using: 

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

global $pageSizeChangeEnabled;

### Cookies handling
# set cookies
if(isset($clearCoo) && $clearCoo == 1)
{
	if(isset($_COOKIE['cat_setting2_pageNo'])) {
		$_COOKIE['cat_setting2_pageNo'] = "";
		$cat_setting2_pageNo = "";
	}
	if(isset($_COOKIE['cat_setting2_numPerPage'])) {
		$_COOKIE['cat_setting2_numPerPage'] = "";
		$cat_setting2_numPerPage = "";
	}
	if(isset($_POST['cat_setting2_pageNo']))
	{
		$_POST['cat_setting2_pageNo'] = "";
		$cat_setting2_pageNo = "";
	}
}
else
{
	// for click 'Next page' & 'Prev page' use
	if(isset($_POST['cat_setting2_pageNo']))
		$cat_setting2_pageNo = $_POST['cat_setting2_pageNo'];
}

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$li				= new libdbtable2007($SortField, $Order, $PageNumber);
$lebooking->Check_Page_Permission();

// for PHP5.4 getting value from cookie
$cat_setting_category_id = $_COOKIE['cat_setting_category_id'];

### Category ###
$sql = "SELECT ".$linventory->getInventoryNameByLang()." as CatName FROM INVENTORY_CATEGORY WHERE CategoryID = '$cat_setting_category_id'";
$CategoryInfoArr = $linventory->returnArray($sql);
$arr_cat_name = $CategoryInfoArr[0]['CatName'];

$temp[] = array("<a href='category_setting.php?clearCoo=1'>".$i_InventorySystem['Category']."</a> > ".intranet_htmlspecialchars($arr_cat_name)." > ".$i_InventorySystem['SubCategory']);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['Category'], "", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Setting_SubCatgeory_DeleteFail");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

$toolbar = '<table border="0" cellpadding="0" cellspacing="0">';
$toolbar .= '<tr>';
$toolbar .= '<td>';
$toolbar .= $linterface->GET_LNK_NEW("javascript:checkNew('category_level2_insert.php?cat_id=$cat_setting_category_id')","","","","",0)."&nbsp;";
$toolbar .= '</td>';
$toolbar .= '<td>';
$toolbar .= $linterface->GET_LNK_IMPORT("category_level2_import.php?category_id=".$cat_setting_category_id."","","","","",0);	
$toolbar .= '<td>';
$toolbar .= '</tr>';
$toolbar .= '</table>';

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:AssignCookies(); SetCookies(); checkRemove(document.form1,'cat2_id[]','category_level2_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_delete
					</a>
				</td>";


$namefield = $linventory->getInventoryNameByLang();

$sql = "SELECT 
				Category2ID, 
				CategoryID, 
				Code,
				$namefield
		FROM 
				INVENTORY_CATEGORY_LEVEL2
		WHERE
				CategoryID = '$cat_setting_category_id'
		ORDER BY
				DisplayOrder";
$result = $linventory->returnArray($sql,5);

	# Default Table Settings
	$cat_setting2_pageNo = ($cat_setting2_pageNo == '')? $li->pageNo=1 : $li->pageNo=$cat_setting2_pageNo;
	$cat_setting2_numPerPage = ($cat_setting2_numPerPage == '')? $li->page_size=20 : $li->page_size=$cat_setting2_numPerPage;
	$Order = ($Order == '')? 1 : $Order;
	$SortField = ($SortField == '')? 0 : $SortField;
	
	if($cat_setting2_pageNo == 1)
	{
		$start = $cat_setting2_pageNo;
		$end = $cat_setting2_numPerPage;
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($result),($li->pageNo*$li->page_size));
	}
	else 
	{
		$start = ($cat_setting2_pageNo*$cat_setting2_numPerPage)-$cat_setting2_numPerPage+1;
		$end = ($cat_setting2_pageNo*$cat_setting2_numPerPage);
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($result),($li->pageNo*$li->page_size));
	}

	$table_content = '';
	$table_content .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\">";
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletoplink\" width=\"1%\">#</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_InventorySystem_SubCategory_Code</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"70%\">$i_InventorySystem_SubCategory_Name</td>";
	$table_content .= "<td class=\"tabletoplink\" width=\"1%\" align=\"right\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'cat2_id[]'):setChecked(0,this.form,'cat2_id[]')\"></td>";
	$table_content .= "</tr>";
	
	if(sizeof($result)>0)
	{
		for($i=$start-1; $i<$end; $i++)
		{
			list($cat2_id, $cat_id, $cat2_code, $cat2_name) = $result[$i];
			$j++;
			$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
			
			if($cat2_id != "")
			{
				$thisCanDelete = $lebooking->Check_Can_Delete_Category('', $cat2_id);
				$thisDisabled = ($thisCanDelete)? '' : 'disabled';
				
				$table_content .= "<tr class=\"$css\">";
				$table_content .= "<td class=\"tabletext\">$j</td>\n";
				$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($cat2_code)."</td>\n";
				//$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($cat2_name)."</td>\n";
				$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href='#' onClick='GoEditSubCategoryPage($cat_id, $cat2_id)'>".intranet_htmlspecialchars($cat2_name)."</a></td>\n";
				$table_content .= "<td class=\"tabletext\"  align=\"right\"><input type=\"checkbox\" name=\"cat2_id[]\" value=\"$cat2_id\" $thisDisabled></td>\n";
				$table_content .= "<input name=\"cat_id\" type=\"hidden\" value=\"$cat_id\">\n";
				$table_content .= "</tr>\n";
			}
		}
	}
	else
	{
		$table_content .= "<tr class=\"tablerow2 tabletext\">";
		$table_content .= "<td class=\"tabletext\" colspan=\"4\" align=\"center\">$i_no_record_exists_msg</td>";
		$table_content .= "</tr>\n";
	}
	### Table Navigation Bar ###
	$li->page_size = $cat_setting2_numPerPage;
	$li->total_row = sizeof($result);
	$li->form_name = "form1";
	$li->pageNo_name = "cat_setting2_pageNo";
	$li->numPerPage_name = "cat_setting2_numPerPage";
	
	$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"4\">";
	if(sizeof($result))
		$table_content .= $li->navigation_ebooking();
	$table_content .= "</td></tr>";
	
	$table_content .= "<tr><td>";
	$table_content .= "<input type='hidden' name='CheckboxChecked' value=0>";
	$table_content .= "<input type='hidden' id='cat_setting2_pageNo' name='cat_setting2_pageNo' value='".$li->pageNo."'>";
	$table_content .= "<input type='hidden' id='order' name='order' value='".$li->order."'>";
	$table_content .= "<input type='hidden' id='field' name='field' value='".$li->field."'>";
	$table_content .= "<input type='hidden' id='page_size_change' name='page_size_change' value=''>";
	$table_content .= "</td></tr>";
	
$table_content .= "</table>";

### eBooking & eInventory Data Warning
$WarningMsgArr = array();
if ($plugin['eBooking'] && $plugin['Inventory'])
	$WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['DataChangeAffect_eInventory'];
$WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['CannotDeleteCategoryWithItem'];
$WarningMsg = implode('<br />', $WarningMsgArr);
$WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Caution'].'</font>', $WarningMsg, $others="");
?>

<?=$lebooking_ui->initJavaScript();?>

<script language='javascript'>
	var arrCookies = new Array();
	arrCookies[arrCookies.length] = "cat_setting2_pageNo";
	arrCookies[arrCookies.length] = "cat_setting2_numPerPage";	
	
	$(document).ready( function() {
		<? if($clearCoo) { ?>
			for(i=0; i<arrCookies.length; i++)
			{
				var obj = arrCookies[i];
				$.cookies.del(obj);
			}
		<? } ?>
	});
	
	function AssignCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if($('#'+arrCookies[i]).length != 0)
			{
				//alert("A: "+$('#'+arrCookies[i]).val());
				$.cookies.set(arrCookies[i],$('#'+arrCookies[i]).val());
			}
			else
			{
				//alert("B: "+$.cookies.get(arrCookies[i]));
				$.cookies.set(arrCookies[i],$.cookies.get(arrCookies[i]));
			}
		}
	}
	
	function SetCookies()
	{
		for(i=0; i<arrCookies.length; i++)
		{
			if(($.cookies.get(arrCookies[i]) != "") || ($.cookies.get(arrCookies[i]) != "null"))
			{
				$( arrCookies[i] ).cookieBind();
			}
		}
	}
	
	function GoEditSubCategoryPage(cat_id, cat2_id)
	{
		window.location = 'category_level2_edit.php?cat_id=' + cat_id + '&cat2_id=' + cat2_id;
	}
</script>

<br>
<form name="form1" action="category_level2_setting.php" method="POST">
<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td colspan="2" align="right"><?= $infobar1 ?></td>
</tr>
</table>
<table width="100%%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td colspan="2" align="right"><?= $infobar2 ?></td>
</tr>
</table>

<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
	<tr><td colspan="2" align="center"><?=$WarningBox?></td></tr>
</table>

<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tbody>
					<tr>
						<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" height="23" width="21"></td>
						<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tbody>
									<tr>
										<?=$table_tool?>
									</tr>
								</tbody>
							</table>
						</td>
						<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" height="23" width="6"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr><td><?=$table_content?></td></tr>
</table>

</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>