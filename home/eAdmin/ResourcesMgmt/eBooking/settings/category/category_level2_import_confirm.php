<?php
// Using: Ronald

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking		= new libebooking();
$limport 		= new libimporttext();
$li 			= new libdb();
$lo 			= new libfilesystem();

$lebooking->Check_Page_Permission();


$filepath 		= $itemfile;
$filename 		= $itemfile_name;

$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($button_new." > ".$i_InventorySystem['SubCategory'])."</td></tr>"; 

$file_format = array("Category Code","Sub-Category Code","Chinese Name","English Name");
	
# Create temp single item table
$sql = "DROP TABLE TEMP_INVENTORY_CATEGORY_LEVEL2";
$li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_CATEGORY_LEVEL2
		(
		 CategoryID int(11),
		 Code varchar(10),
		 NameChi varchar(255),
		 NameEng varchar(255),
		 HasSoftwareLicenseModel tinyint(4),
		 HasWarrantyExpiryDate tinyint(4),
		 HasSerialNumber tinyint(4),
		 DisplayOrder int(10)
	    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql);


$ext = strtoupper($lo->file_ext($filename));
if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: category_level2_import.php?msg=15");
	exit();
}

if($limport->CHECK_FILE_EXT($filename))
{
	# read file into array
	# return 0 if fail, return csv array if success
	
	$data = $limport->GET_IMPORT_TXT($filepath);
	$col_name = array_shift($data);
		
	# check the csv file's first row is correct or not
	$format_wrong = false;
	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}
	
	if($format_wrong)
	{
		header("location: category_level2_import.php?msg=15");
		exit();
	}
	
	### Remove Empty Row in CSV File ###
	for($i=0; $i<sizeof($data); $i++)
	{
		if(sizeof($data[$i])!=0)
		{
			$arr_new_data[] = $data[$i];
			$record_row++;
		}
		else
		{
			$empty_row++;
		}
	}
	$file_original_row = sizeof($data);
	$file_new_row = sizeof($arr_new_data);
	
	for($i=0; $i<sizeof($arr_new_data); $i++)
	{
		list($category_code,$category_subcode,$category_chi_name,$category_eng_name,$category_license,$category_warranty,$category_serial,$category_display_order) = $arr_new_data[$i];
		
		$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '$category_code'";
		$arr_exist_item = $linventory->returnArray($sql,1);
		$category_id = $arr_exist_item[0]['CategoryID'];
		
		# Check category Code Exist #
		if($category_code != "")
		{
			$sql = "SELECT Code FROM INVENTORY_CATEGORY WHERE Code = '$category_code'";
			$arr_result1 = $linventory->returnArray($sql,1);

			$sql = "SELECT Code FROM INVENTORY_CATEGORY_LEVEL2 WHERE Code = '$category_subcode'";
			$arr_result2 = $linventory->returnArray($sql,1);
			
			if(sizeof($arr_result1)<=0)
					$error[$i]['type'] = 1;		
				
			if(sizeof($arr_result2)>0)
					$error[$i]['type'] = 9;
		}
		else
			$error[$i]['type'] = 2;
		
		# Check Item SubCode #
		if($category_subcode == "")
			$error[$i]['type'] = 3;
		
		# Check Item Chi Name #
		if($category_chi_name == "")
			$error[$i]['type'] = 4;
		
		# Check Item Eng Name #
		if($category_eng_name == "")
			$error[$i]['type'] = 5;
			
		$category_chi_name = intranet_htmlspecialchars(addslashes($category_chi_name));
		$category_eng_name = intranet_htmlspecialchars(addslashes($category_eng_name));
				
		$values = "('$category_id','$category_subcode','$category_chi_name','$category_eng_name','$category_license','$category_warranty','$category_serial','$category_display_order')";
		$sql = "INSERT INTO TEMP_INVENTORY_CATEGORY_LEVEL2 (CategoryID, Code, NameChi, NameEng, HasSoftwareLicenseModel,HasWarrantyExpiryDate,HasSerialNumber,DisplayOrder) VALUES $values";
		
		$linventory->db_db_query($sql);
					
		# check any duplicate category code in the csv file
		$sql = "SELECT Code FROM TEMP_INVENTORY_CATEGORY_LEVEL2";
		$arr_tmp_checkCategoryCode = $linventory->returnVector($sql);
		if(sizeof($arr_tmp_checkCategoryCode) != sizeof(array_unique($arr_tmp_checkCategoryCode)))
			$error[$i]['type'] = 19;
	}
	
	### Show Import Result ###
	if($record_row == "")
		$record_row = 0;
	if($empty_row == "")
		$empty_row = 0;
	$table_content .= "<tr>";
	$table_content .= "<td class=\"tabletext\" colspan=\"26\">
							$i_InventorySystem_ImportItem_TotalRow: $file_original_row<br>
							$i_InventorySystem_ImportItem_RowWithRecord: $record_row<br>
							$i_InventorySystem_ImportItem_EmptyRowRecord: $empty_row
						</td>";
	$table_content .= "</tr>";
	### END ###
	$table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_CategoryCode</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Category2Code</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Category_ChineseName</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Category_EnglishName</td>";
	
	if(sizeof($error)>0)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
	}
	$table_content .= "</tr>\n";
	
	
	if(sizeof($error) == 0)
	{
		$sql = "SELECT 
					a.CategoryID,
					b.Code,
					a.Code, 
					a.NameChi, 
					a.NameEng, 
					a.HasSoftwareLicenseModel,
					a.HasWarrantyExpiryDate,
					a.HasSerialNumber,
					a.DisplayOrder
				FROM
					TEMP_INVENTORY_CATEGORY_LEVEL2 as a
					INNER JOIN INVENTORY_CATEGORY as b ON (a.CategoryID = b.CategoryID)";
		$arr_result = $linventory->returnArray($sql,9);
		
		if(sizeof($arr_result) > 0)
		{
			for ($i=0; $i<sizeof($arr_result); $i++)
			{
				$j=$i+1;
				if($j%2 == 0)
					$table_row_css = " class=\"tablerow1\" ";
				else
					$table_row_css = " class=\"tablerow2\" ";
				
				list($category_id, $category_code,  $category_subcode, $category_chi_name, $category_eng_name, $HasSoftwareLicenseModel, $HasWarrantyExpiryDate, $HasSerialNo, $category_display_order) = $arr_result[$i];
								
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$category_code</td>";
				$table_content .= "<td class=\"tabletext\">$category_subcode</td>";
				$table_content .= "<td class=\"tabletext\">$category_chi_name</td>";
				$table_content .= "<td class=\"tabletext\">$category_eng_name</td>";
			}
		}
		$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"8\"></td></tr>";
		$table_content .= "<tr><td colspan=8 align=right>".
							$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
							$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='category_level2_import.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
						  "</td></tr>";
	}

	if(sizeof($error) > 0)
	{
		for($i=0; $i<sizeof($arr_new_data); $i++)
		{
			$j=$i+1;
			if($j%2 == 0)
				$table_row_css = " class=\"tablerow1\" ";
			else
				$table_row_css = " class=\"tablerow2\" ";
				
			list($category_id, $category_subcode, $category_chi_name, $category_eng_name, $HasSoftwareLicenseModel, $HasWarrantyExpiryDate, $HasSerialNo, $category_display_order) = $data[$i];
			if($error[$i]["type"] == "")
			{
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$category_id</td>";
				$table_content .= "<td class=\"tabletext\">$category_subcode</td>";
				$table_content .= "<td class=\"tabletext\">$category_chi_name</td>";
				$table_content .= "<td class=\"tabletext\">$category_eng_name</td>";
				$table_content .= "<td class=\"tabletext\"> - </td></tr>";
			}
			if($error[$i]["type"] != "")
			{
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$category_id</td>";
				$table_content .= "<td class=\"tabletext\">$category_subcode</td>";
				$table_content .= "<td class=\"tabletext\">$category_chi_name</td>";
				$table_content .= "<td class=\"tabletext\">$category_eng_name</td>";
				$table_content .= "<td class=\"tabletext\">".$i_InventorySystem_Category2ImportError[$error[$i]["type"]]."</td></tr>";
			}
		}
		$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"9\"></td></tr>";
		$table_content .= "<tr><td colspan=9 align=right>".
							$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='category_level2_import.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
						  "</td></tr>";
	}
}
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['Category'], "", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br>

<form name="form1" action="category_level2_import_update.php" method="post">
<table border="0" width="100%" cellspacing="0" cellpadding="5">
<?=$infobar;?>
</table>
<br>
<table border="0" width="100%" cellspacing="0" cellpadding="5">
<?=$table_content;?>
</table>
<input type="hidden" name="format" value=<?=$format;?>>
<input type="hidden" name="category_id" value="<?=$category_id?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>