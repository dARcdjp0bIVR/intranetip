<?php
// Using:

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking		= new libebooking();
$lebooking->Check_Page_Permission();

$sql = "SELECT 
				Code, 
				NameChi, 
				NameEng, 
				DisplayOrder 
		FROM 
				TEMP_INVENTORY_CATEGORY";

$arr_result = $linventory->returnArray($sql,5);

if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($category_code,$category_namechi, $category_nameeng, $category_displayorder) = $arr_result[$i];
		
		$values = "'$category_code', '$category_namechi', '".$category_nameeng."', '$category_displayorder', '1', NOW(), NOW()";
		
		$sql = "INSERT INTO INVENTORY_CATEGORY
						(Code, 
						NameChi, 
						NameEng, 
						DisplayOrder,
						RecordStatus,
						DateInput, 
						DateModified)
				VALUES
						($values)";
		
		$result['NewInvItem'.$i] = $linventory->db_db_query($sql);
		
		$sql = "SELECT 
						CategoryID
				FROM
						INVENTORY_CATEGORY
				WHERE
						Code = '$category_code'
				";
		$arr_cat_id = $linventory->returnVector($sql);
		
		if(sizeof($arr_cat_id)>0)
		{
			$category_id = $arr_cat_id[0];
		}
		
		$re_path = "/images/inventory/";
		$filename = "/no_photo.jpg";
		
		$values = "($category_id, '0', '0', '$re_path', '$filename', NOW(), NOW())";
		$sql = "INSERT IGNORE INTO INVENTORY_PHOTO_PART 
						(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
				VALUES 
						$values";
		
		$linventory->db_db_query($sql);
		
		if($linventory->db_affected_rows($sql) > 0)
		{
			$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$category_id' AND Category2ID = 0 AND ItemID = 0 ";
			
			$result = $linventory->returnArray($sql,1);
			
			if(sizeof($result)>0)
			{
				list($part_id) = $result[0];
			}
		}
		
		$sql = "UPDATE INVENTORY_CATEGORY SET PhotoLink = '$part_id' WHERE CategoryID = '$category_id'";
		
		$linventory->db_db_query($sql);
	}

}

if (in_array(false,$result)) {
	header("location: category_import.php?msg=12");
}
else {
	header("location: category_import.php?msg=1");
}
intranet_closedb();
?>