<?php
// Using:

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();


$cat2_id_list = implode("','",$cat2_id);

if($cat_id != "" && $cat2_id_list != "")
{
	$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE CategoryID = '$cat_id' AND Category2ID IN ('$cat2_id_list')";
	$arr_result = $linventory->returnArray($sql,1);
	
	if(sizeof($arr_result)>0)
	{
		header("location: category_level2_setting.php?category_id=".$cat_id."&msg=13");
	}
	else
	{
		$sql = "DELETE FROM INVENTORY_PHOTO_PART WHERE CategoryID = '$cat_id' AND Category2ID IN ('$cat2_id_list') AND ItemID = 0";
		$result_1 = $linventory->db_db_query($sql);
		
		$sql = "DELETE FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '$cat_id' AND Category2ID IN ('$cat2_id_list')";
		$result_2 = $linventory->db_db_query($sql);
		
		if($result_1 && $result_2)
		{
			header("location: category_level2_setting.php?category_id=".$cat_id."&msg=3");
		}
		else
		{
			header("location: category_level2_setting.php?category_id=".$cat_id."&msg=13");
		}
	}
}
else
{
	header("location: category_level2_setting.php?category_id=".$cat_id."&msg=13");
}
intranet_closedb();

?>