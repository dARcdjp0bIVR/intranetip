<?php
// Using:

###############################################################################
###### If edited this page, please edit relative page in eInventory also ######
###############################################################################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$lebooking		= new libebooking();

$lebooking->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['Category'], "", 0);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
	if ($msg == 15) $SysMsg = $linterface->GET_SYS_MSG("import_failed");
	if ($msg == 16) $SysMsg = $linterface->GET_SYS_MSG("import_failed2");
}

?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>


<script language="JavaScript">
isMenu = true;
</script>
<div id="ToolMenu" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
<!--
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
-->
<script language="javascript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
var callback2 = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu3", o.responseText );
        }
}
// start AJAX
function retrieveItemCode(type)
{
    obj = document.form1;
    var myElement = document.getElementById("ToolMenu2");
                
    myElement.innerHTML = "<table border=1 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td><?=$i_InventorySystem_Loading?></td></tr></table>";
    
    YAHOO.util.Connect.setForm(obj);

    // page for processing and feedback
    var path = "getCategoryCode.php?item_type=" + type;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
function retrieveCategory2Index(type)
{
    obj = document.form1;
    
    if(type==1)
    	var myElement = document.getElementById("ToolMenu2");
    if(type==2)
    	var myElement = document.getElementById("ToolMenu3");
    
    myElement.innerHTML = "<table border=1 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td><?=$i_InventorySystem_Loading?></td></tr></table>";
    
    YAHOO.util.Connect.setForm(obj);

    // page for processing and feedback
    var path = "getCategory2Index.php?item_type=" + type;
    if(type==1)
    	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
    if(type==2)
    	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

function jHIDE_DIV(InputTable)
{
	jChangeContent( InputTable,"");
}
-->
</script>
<br>

<form name="form1" action="category_level2_import_confirm.php" method="POST" enctype="multipart/form-data">
<table border="0" width="96%" cellpadding="5" cellspacing="0">
<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2" align="right"><?=$SysMsg?></td>
				</tr>
				<tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
	                	<?=$i_select_file?>
	                </td>
	                <td class="tabletext" width="70%">
	    				<input class="file" type="file" name="itemfile"><br>
	    				<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
	    			</td>
                </tr>
                <tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $i_general_Format ?>
					</td>
					<td class="tabletext">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr>
								
								<td valign="top" class="tabletext" style="padding-top:5px;">
									<label for="format1">
											<?=$i_InventorySystem_Category2Import_Format1_Row1?>&nbsp;[<a class="tablelink" href=javascript:retrieveItemCode(2)><?=$i_InventorySystem_Import_CheckCategory?></a>]<br>
											<?=$i_InventorySystem_Category2Import_Format1_Row2?>&nbsp;[<a class="tablelink" href=javascript:retrieveCategory2Index(1)><?=$i_InventorySystem_Import_CheckCategory2?></a>]<br>
											<?=$i_InventorySystem_Category2Import_Format1_Row3?><br>
											<?=$i_InventorySystem_Category2Import_Format1_Row4?><br>
									</label><br />
									<a class="tablelink" href="<?= GET_CSV("sample_import_category2.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
								</td>
								<td>
									<table width="100%" border="0" cellpadding="0" cellspacing="0" align="right" >
									<tr>
										<td>
											<span id="ToolMenu2"></span>
										</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td colspan="3">&nbsp;</td>
							</tr>
							
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='category_level2_setting.php?category_id=".$category_id."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>

<input type="hidden" name="category_id" value="<?=$category_id?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
