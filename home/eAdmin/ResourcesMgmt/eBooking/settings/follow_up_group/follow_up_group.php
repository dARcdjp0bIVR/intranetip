<?php
// using : Ronald
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_FollowUpGroup";
$linterface 	= new interface_html();
$lebooking_ui 		= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'], "", 0);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$Keyword = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword']));
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$PageSize = $_REQUEST['numPerPage'];

echo $lebooking_ui->Get_Settings_FollowUpGroup_UI($Keyword, $pageNo, $PageSize, $order, $field);
?>

<script language='javascript'>
$('input#Keyword').focus().keyup(function(e){
	var keynum = Get_KeyNum(e);
		
	if (keynum==13) // keynum==13 => Enter
		js_Reload_FollowUpGroup_Table();
});

function js_Reload_FollowUpGroup_Table()
{
	jsKeyword = Trim($('input#Keyword').val());
	
	js_pageNo = Trim($('input#pageNo').val());
	js_order = Trim($('input#order').val());
	js_field = Trim($('input#field').val());
	js_numPerPage = Trim($('input#numPerPage').val());
	
	$('div#FollowGroupDiv').html('');
	Block_Element('FollowGroupDiv');
	
	$('div#FollowGroupDiv').load(
		"ajax_reload.php", 
		{ 
			Action: 'FollowUpGroup_Table',
			Keyword: jsKeyword,
			PageNumber: js_pageNo,
			Order: js_order,
			SortField: js_field,
			PageSize: js_numPerPage
		},
		function(ReturnData)
		{
			UnBlock_Element('FollowGroupDiv');
		}
	);
}

function js_Go_New_Group()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'follow_up_group_new1.php';
	objForm.submit();
}

function js_Go_Update_Group_Info(jsGroupID)
{
	var objForm = document.getElementById('form1');
	objForm.action = 'follow_up_group_new1.php?GroupID=' + jsGroupID;
	objForm.submit();
}

function js_Go_View_Member_List(jsGroupID)
{
	var objForm = document.getElementById('form1');
	objForm.action = 'follow_up_group_member_list.php?FromIndex=1&GroupID=' + jsGroupID;
	objForm.submit();
}

function js_Delete_Group()
{
	checkRemove(document.form1,'GroupIDArr[]','follow_up_group_delete.php','<?=$Lang['eBooking']['Settings']['FollowUpGroup']['JSWarningArr']['DeleteFollowUpGroup']?>');
}
 
</script>

<?
$linterface->LAYOUT_STOP();
?>