<?php
// using : Ronald
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");

intranet_auth();
intranet_opendb();

# Get data
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$GroupID = $_REQUEST['GroupID'];
$SearchValue = stripslashes(urldecode($_REQUEST['q']));

### Get Select User and exclude them in the search result
$libebooking = new libebooking();
$SelectedUserArr = explode(',', $SelectedUserIDList);
$ExcludeUserIDArr = $libebooking->Get_User_Array_From_Selection($SelectedUserArr);

### Get User who can be added to the management group
$AvailableUserInfoArr = $libebooking->Get_Available_FollowUp_User($GroupID, $SearchValue, $ExcludeUserIDArr);

$numOfAvailableUser = count($AvailableUserInfoArr);

$returnString = '';
for ($i=0; $i<$numOfAvailableUser; $i++)
{
	$thisUserID = $AvailableUserInfoArr[$i]['UserID'];
	$thisUserLogin = $AvailableUserInfoArr[$i]['UserLogin'];
	$thisUserName = $AvailableUserInfoArr[$i]['UserName'];
	
	$returnString .= $thisUserName.'|'.$thisUserLogin.'|'.$thisUserID."\n";
}

intranet_closedb();

echo $returnString;
?>