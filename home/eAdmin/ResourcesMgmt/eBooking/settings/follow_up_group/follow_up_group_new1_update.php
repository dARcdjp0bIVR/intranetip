<?php
// using : Ronald
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

intranet_opendb();

$lebooking = new libebooking();

$isEdit = $_REQUEST['isEdit'];
$GroupID = $_REQUEST['GroupID'];
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

$GroupInfoArr = array();
$GroupInfoArr['GroupName'] = stripslashes($_REQUEST['GroupName']);
$GroupInfoArr['Description'] = stripslashes($_REQUEST['Description']);

if ($isEdit)
{
	# edit
	$GroupName = $lebooking->Get_Safe_Sql_Query($_REQUEST['GroupName']);
	$Description = $lebooking->Get_Safe_Sql_Query($_REQUEST['Description']);
	
	$lebooking->Start_Trans();
	
	$sql = "UPDATE 
				INTRANET_EBOOKING_FOLLOWUP_GROUP 
			SET
				GroupName = '".$GroupName."',
				Description = '".$Description."',
				GroupColor = '".$GroupColor."'
			WHERE 
				GroupID = ".$GroupID;
	$result = $lebooking->db_db_query($sql);
	
	if(!$result) {
		$lebooking->RollBack_Trans();
		$ReturnMsgKey = "EditFollowUpGroupFailed";
	} else {
		$lebooking->Commit_Trans();
		$ReturnMsgKey = "EditFollowUpGroupSuccess";
	}
	$returnPage = "follow_up_group.php";
}
else
{
	# new
	$GroupName = $lebooking->Get_Safe_Sql_Query($_REQUEST['GroupName']);
	$Description = $lebooking->Get_Safe_Sql_Query($_REQUEST['Description']);
	
	$lebooking->Start_Trans();
	
	$sql = "INSERT INTO 
				INTRANET_EBOOKING_FOLLOWUP_GROUP 
				(GroupName, Description, GroupColor, InputBy, ModifiedBy, DateInput, DateModified) 
			VALUES 
				('$GroupName', '$Description', '$GroupColor', $UserID, $UserID, NOW(), NOW())";
	$result = $lebooking->db_db_query($sql);
	
	if(!$result) {
		$lebooking->RollBack_Trans();
		$ReturnMsgKey = "AddFollowUpGroupFailed";
	} else {
		$GroupID = $lebooking->db_insert_id();
		$lebooking->Commit_Trans();
		$ReturnMsgKey = "AddFollowUpGroupSuccess";
	}
	$returnPage = "follow_up_group_new_member.php";
}

intranet_closedb();

$para = "GroupID=$GroupID&Keyword=$Keyword&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage&ReturnMsgKey=$ReturnMsgKey";
header("Location: $returnPage?$para");
?>