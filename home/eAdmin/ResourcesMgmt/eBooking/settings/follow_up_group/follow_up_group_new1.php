<?php
// using: 
/*
 *  2019-05-13 Cameron
 *      - fix potential sql injection problem by enclosing var with apostrophe
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_FollowUpGroup";
$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();


$GroupID = $_REQUEST['GroupID'];
$IndexInfoArr['pageNo'] = $_REQUEST['pageNo'];
$IndexInfoArr['order'] = $_REQUEST['order'];
$IndexInfoArr['field'] = $_REQUEST['field'];
$IndexInfoArr['numPerPage'] = $_REQUEST['numPerPage'];

$isEdit = ($GroupID!='')? true : false;

$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['FollowUpGroup']['FollowUpGroupList'], "javascript:js_Go_Back_To_Group_List()"); 
if ($isEdit)
{
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['FollowUpGroup']['EditGroup'], "");
	
	$sql = "SELECT GroupName, Description, GroupColor FROM INTRANET_EBOOKING_FOLLOWUP_GROUP WHERE GroupID = '$GroupID'";
	$arrResult = $lebooking_ui->returnArray($sql,3);
	
	if(sizeof($arrResult)>0){
		list($GroupName, $Description, $GroupColor) = $arrResult[0];
	}
	
	$SubmitBtnText = $Lang['Btn']['Submit'];
}
else
{
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['FollowUpGroup']['NewGroup'], "");
	
	### Step Table
	$STEPS_OBJ[] = array($Lang['eBooking']['Settings']['FollowUpGroup']['NewGroupStepArr']['Step1'], 1);
	$STEPS_OBJ[] = array($Lang['eBooking']['Settings']['FollowUpGroup']['NewGroupStepArr']['Step2'], 0);
	$StepTable = $linterface->GET_STEPS($STEPS_OBJ);
	
	$SubmitBtnText = $Lang['Btn']['Continue'];
}

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'], "", 0);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['FollowUpGroup']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

### Navigation
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);


### Textbox for Group Name
$GroupNameTb = '<input type="text" class="textboxtext" name="GroupName" id="GroupName" maxlength="'.$maxLength.'" value="'.intranet_htmlspecialchars($GroupName).'">';

### Textarea for Description
$DescriptionTextArea = $linterface->GET_TEXTAREA('Description', $Description);

### Continue Button
$btn_Continue = $linterface->GET_ACTION_BTN($SubmitBtnText, "button", $onclick="js_Update_Group_Info('$GroupID');", $id="Btn_Submit");
		
### Cancel Button
$btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Go_Back_To_Group_List()", $id="Btn_Cancel");
		

$thisHTML = '';
$thisHTML .= '<br />'."\n";
$thisHTML .= '<form id="form1" name="form1" method="post">'."\n";

	# Navigation & Steps
	$thisHTML .= '<table width="98%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td align="left" class="navigation">'.$PageNavigation.'</td>'."\n";
		$thisHTML .= '</tr>'."\n";
		$thisHTML .= '<tr><td>&nbsp;</td></tr>'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td align="center">'.$StepTable.'</td>'."\n";
		$thisHTML .= '</tr>'."\n";
	$thisHTML .= '</table>'."\n";
	
	$thisHTML .= '<br />'."\n";
	$thisHTML .= '<br />'."\n";
	
	# Group Info
	$thisHTML .= '<table width="88%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td>'."\n";
				$thisHTML .= '<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">'."\n";
					$thisHTML .= '<tr>'."\n";
						$thisHTML .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['FollowUpGroup']['GroupName'].'</td>'."\n";
						$thisHTML .= '<td>'."\n";
							$thisHTML .= $GroupNameTb;
							$thisHTML .= $linterface->Get_Thickbox_Warning_Msg_Div("FollowUpGroupNameWarningDiv");
						$thisHTML .= '</td>'."\n";
					$thisHTML .= '</tr>'."\n";
					$thisHTML .= '<tr valign="top">'."\n";
						$thisHTML .= '<td class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['FollowUpGroup']['Description'].'</td>'."\n";
						$thisHTML .= '<td>'.$DescriptionTextArea.'</td>'."\n";
					$thisHTML .= '</tr>'."\n";
					$thisHTML .= '<tr valign="top">'."\n";
						$thisHTML .= '<td class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['ManagementGroup']['Color'].'</td>'."\n";
						if($GroupColor == "")
							$GroupColor = "#b00408";
						$thisHTML .= '<td><input type="text" name="GroupColor" id="GroupColor" value="'.$GroupColor.'"></td>'."\n";
					$thisHTML .= '</tr>'."\n";
					$thisHTML .= '<tr><td>&nbsp;</td></tr>'."\n";
					$thisHTML .= '<tr><td colspan="2"><div class="edit_bottom"><br />'.$btn_Continue.'&nbsp;'.$btn_Cancel.'</div></td></tr>'."\n";
				$thisHTML .= '</table>'."\n";
			$thisHTML .= '</td>'."\n";
		$thisHTML .= '</tr>'."\n";
	$thisHTML .= '</table>'."\n";	
	
	$thisHTML .= '<input type="hidden" id="isEdit" name="isEdit" value="'.$isEdit.'" />'."\n";
	$thisHTML .= '<input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />'."\n";
	
	### Hidden value to preserve index page table view
	$thisHTML .= '<input type="hidden" id="Keyword" name="Keyword" value="'.intranet_htmlspecialchars($IndexInfoArr['Keyword']).'" />'."\n";
	$thisHTML .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$IndexInfoArr['pageNo'].'" />'."\n";
	$thisHTML .= '<input type="hidden" id="order" name="order" value="'.$IndexInfoArr['order'].'" />'."\n";
	$thisHTML .= '<input type="hidden" id="field" name="field" value="'.$IndexInfoArr['field'].'" />'."\n";
	$thisHTML .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$IndexInfoArr['numPerPage'].'" />'."\n";
$thisHTML .= '</form>'."\n";

echo $lebooking_ui->initJavaScript();
echo $thisHTML;
?>

<script language="javascript">
$(document).ready( function() {	
	$('input#GroupName').focus();
	
	// reset default color
	$.fn.colorPicker.defaultColors = ['b00408', 'e97e0b', 'd2a800', '467a13', '809121', '4db308', '8ad00f', '09759d', '21a6ae', '65979b', '2f75e9', '3ea6ff', '1e2cd8', '8c66d9', '994499', '5c1fa0', 'dd4477', 'd96666', 'ac5c14', '7f4b21'];
	$('input#GroupColor').colorPicker();
	
	js_Add_KeyUp_Unique_Checking('GroupName', 'FollowUpGroupNameWarningDiv', '* <?=$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['GroupNameBlank']?>', '* <?=$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['GroupNameInUse']?>');
});

function js_Update_Group_Info(jsGroupID)
{
	var WarningDivID = 'GroupNameWarningDiv';
	if (js_Is_Input_Blank('GroupName', WarningDivID, '* <?=$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['GroupNameBlank']?>'))
	{
		$("input#CategoryName").focus();
		return false;
	}
	
	$.post(
		"ajax_validate.php", 
		{ 
			Action: 'Check_Unique_GroupName',
			Value: $('input#GroupName').val(),
			GroupID: $('input#GroupID').val()
		},
		function(isVaild)
		{
			if(isVaild != 1)
			{
				$('div#' + WarningDivID).html('* <?=$Lang['eBooking']['Settings']['FollowUpGroup']['WarningArr']['GroupNameInUse']?>');
				$('div#' + WarningDivID).show();
			}
			else
			{
				$('div#' + WarningDivID).html("");
				$('div#' + WarningDivID).hide();
				
				var objForm = document.getElementById('form1');
				objForm.action = 'follow_up_group_new1_update.php';
				objForm.submit();
			}
		}
	);
}

function js_Go_Back_To_Group_List()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'follow_up_group.php';
	objForm.submit();
}

// Add KeyUp Listener for Checking
function js_Add_KeyUp_Unique_Checking(InputID, WarningDivID, WarnBlankMsg, WarnUniqueMsg)
{
	$("input#" + InputID).keyup(function(){
		js_Check_Unique(InputID, WarningDivID, WarnBlankMsg, WarnUniqueMsg);
	});
}

// validation 
function js_Check_Unique(InputID, WarningDivID, WarnBlankMsg, WarnUniqueMsg)
{
	var InputValue = $("input#" + InputID).val();
	
	if(Trim(InputValue) == '')
	{
		$('div#' + WarningDivID).html(WarnBlankMsg);
		$('div#' + WarningDivID).show();
	}
	else
	{
		$('div#' + WarningDivID).html("");
		$('div#' + WarningDivID).hide();
		
		$.post(
			"ajax_validate.php", 
			{ 
				Action: 'Check_Unique_GroupName',
				Value: InputValue,
				GroupID: $('input#GroupID').val()
			},
			function(isVaild)
			{
				if(isVaild != 1)
				{
					$('div#' + WarningDivID).html(WarnUniqueMsg);
					$('div#' + WarningDivID).show();
				}
				else
				{
					$('div#' + WarningDivID).html("");
					$('div#' + WarningDivID).hide();
				}
			}
		);
	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>