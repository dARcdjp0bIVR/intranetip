<?php
// using : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_DoorAccess";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$extraCardReaderObj = new liblocation_cardReader();
$lebooking->Check_Page_Permission();


### Title ###
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['DoorAccess']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);


## Get parameters from form
if (isset($_GET['ReaderID'])) {
	$ReaderID = trim($_GET['ReaderID']);
}
else{
    $ReaderID = trim($_POST['ReaderID'][0]);
}

$LocationID = $_POST['LocationID'];
$Remark = $_POST['ReaderRemarks'];
$Status = $_POST['CardReaderStatus'];
$AccessRight = 'External';

$displayForm = $extraCardReaderObj->getDoorAccessEditPage($ReaderID, $LocationID, $AccessRight);

echo $displayForm;

?>
<script language="javascript">
function goBack() {
	window.location = 'index.php';
}

function CheckForm()
{
	var error_no = '';
	
	
	//Check if ReaderNameEng is empty / used more than two times
	if($("input#ReaderNameEng").val().Trim()=='')
	{
		$('div#readerNameEngWarningDiv').show();
		$("input#ReaderNameEng").focus();
		error_no++;
	}else{
		$('div#readerNameEngWarningDiv').hide();
	
	}
	
	//Check if ReaderNameChi is empty / used more than two times
	if($("input#ReaderNameChi").val().Trim()=='')
	{
		$('div#readerNameChiWarningDiv').show();
		$("input#ReaderNameChi").focus();
		error_no++;
	}else{
		$('div#readerNameChiWarningDiv').hide();
	
	}
	
	//Check error no.
	if(error_no>0){
		return false;
	}
	else{
		return true;
	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>