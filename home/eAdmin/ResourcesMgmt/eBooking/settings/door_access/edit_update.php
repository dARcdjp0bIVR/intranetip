<?php
// using : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
intranet_auth();
intranet_opendb();


## Get parameters from form
$ReaderID = trim($_GET['ReaderID']);
$ReportNameEng = standardizeFormPostValue($_POST['ReaderNameEng']);
$ReportNameChi = standardizeFormPostValue($_POST['ReaderNameChi']);
$Remark = standardizeFormPostValue($_POST['ReaderRemarks']);
$Status = standardizeFormPostValue($_POST['CardReaderStatus']);

$libdb = new libdb();
$extraCardReaderObj = new liblocation_cardReader($ReaderID);

//debug_pr($ReaderID);
//debug_pr($LocationID);
//debug_pr($ReportNameEng);
//debug_pr($Code);
//debug_pr($ReportNameChi);
//debug_pr($Status);

$successAry = array();
$libdb->Start_Trans();


### Save Reader Name
$extraCardReaderObj->setNameEng($ReportNameEng);
$extraCardReaderObj->setNameChi($ReportNameChi);

### Save Remarks
$extraCardReaderObj->setRemarks($Remark);

### Save Status
$extraCardReaderObj->setRecordStatus($Status);

### Save Delete
$extraCardReaderObj->setIsDeleted($locationConfigAry['INVENTORY_LOCATION_CARD_READER']['IsDeleted']['notDeleted']);

### Save Date
$extraCardReaderObj->setModifiedDate('now()');

### Save user id
$extraCardReaderObj->setModifiedBy($_SESSION['UserID']);

$extraCardReaderObj->save();

//debug_pr($extraCardReaderObj);

if (in_array(false, $successAry)) {
	$libdb->Rollback_Trans();
	$returnMsgKey = 'UpdateUnSuccess';
}
else {
	$libdb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: index.php?returnMsgKey=$returnMsgKey");