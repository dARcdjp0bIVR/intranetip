<?php
// using : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");
intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "Settings_DoorAccess";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();
$extraCardReaderObj = new liblocation_cardReader();
$lebooking->Check_Page_Permission();


### Cookie ###
$arrCookies = array();
$arrCookies[] = array("eBooking_Setting_ReaderInfo_page_size", "numPerPage");
$arrCookies[] = array("eBooking_Setting_ReaderInfo_field", "field");	
$arrCookies[] = array("eBooking_Setting_ReaderInfo_order", "order");
$arrCookies[] = array("eBooking_Setting_ReaderInfo_pageNo", "pageNo");
$arrCookies[] = array("eBooking_Setting_ReaderInfo_Keyword", "Keyword");
$arrCookies[] = array("eBooking_Setting_ReaderInfo_status", "status");
$arrCookies[] = array("eBooking_Setting_ReaderInfo_Location", "LocationID");

$LocationID = (isset($_POST['LocationID']))? $_POST['LocationID'] : $LocationID;
$AccessRight = 'External';

if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$status = '';
}
else {
	updateGetCookies($arrCookies);
}


### Title ###
$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['DoorAccess']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

### Return Message ###
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$returnMsgKey = $_GET['returnMsgKey'];
$linterface->LAYOUT_START($ReturnMsg);


##Filtering
$locationSelectionDisplay ="";
$locationAry = $lebooking_ui->getAllBookableLocationName();
$htmlAry['toolbar2'] = $lebooking_ui->Get_Booking_Location_Selection('LocationID', $LocationID, ' onchange="document.form1.submit()" ',0 , 1);
$htmlAry['toolbar2'] .= $extraCardReaderObj->Get_Status_Selection('status', $status, ' onchange="document.form1.submit()" ');

### Search box
$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('Keyword', $Keyword);

### DB table action button
$TableBtnAry = array();
$TableBtnAry[] = array('approve', 'javascript:enableReaderStatus()', $Lang['Btn']['Enable']);
$TableBtnAry[] = array('reject', 'javascript:disableReaderStatus()', $Lang['Btn']['Disable']);
$TableBtnAry[] = array('edit', 'javascript:goEdit()');


$htmlAry['dbTableBtn'] = $linterface->Get_DBTable_Action_Button_IP25($TableBtnAry);



### Display DB Table
$htmlAry['dbTable'] = $extraCardReaderObj->geteBookingSettingDoorAccessDBTable($field, $order, $pageNo, $Keyword, $LocationID, $status, $AccessRight);


?>


<script language="javascript">

function goEdit() {
	checkEdit(document.getElementById('form1'), 'ReaderID[]', 'edit.php');
}

function disableReaderStatus() {
	var disable = "<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Disable'] ?>";
	checkEditMultiple2(document.getElementById('form1'), 'ReaderID[]', 'submitFormConfirm(\'disable\');');
}

function enableReaderStatus() {
	var enable = "<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Enable']  ?>";
	checkEditMultiple2(document.getElementById('form1'), 'ReaderID[]', 'submitFormConfirm(\'enable\');');
}

function goDelete() {
	checkRemove2(document.getElementById('form1'), 'ReaderID[]', 'submitForm(\'delete\');');
}

function submitFormConfirm(actionType) {
	var warningMsg = '';
	if (actionType == 'disable') {
		warningMsg = '<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Disable'] ?>';
	}
	else if (actionType == 'enable') {
		warningMsg = '<?= $Lang['SysMgr']['Location']['CardReader']['Confirm']['Enable'] ?>';
	}
	
	if (confirm(warningMsg)) {
		submitForm(actionType);
	}
}

function submitForm(actionType) {
	$('input#actionType').val(actionType);
	$('form#form1').attr('action', 'delete.php').submit();
}


</script>

<form id="form1" name="form1" method="post" action="index.php">

<div class="content_top_tool">
		<div class="Conntent_tool">
	       <?=$htmlAry['toolbar']?>  
		</div>
		<br style="clear:both" />
		<div class="Conntent_tool">
			<?=$htmlAry['toolbar2']?>
		</div>
      	  <?=$htmlAry['searchBox']?>
		<br style="clear:both" />
	</div>
	
	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom"><?=$htmlAry['filterSignature']?><?=$htmlAry['filterSchoolChop']?></td>
			<td valign="bottom"><?=$deleteBtn?></td>
		</tr>
		</table>
			<?=$htmlAry['dbTableBtn']?>
			<?=$htmlAry['dbTable']?>
	</div>
	
	<input type="hidden" id="actionType" name="actionType" value="" />


</form>



<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>