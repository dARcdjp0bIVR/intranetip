<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$lebooking->Check_Page_Permission();

# Get data
$GroupIDArr = $_REQUEST['GroupIDArr'];
$Keyword = $_REQUEST['Keyword'];
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

$libMgmtGroup = new eBooking_MgmtGroup();
$success = $libMgmtGroup->Delete_Object($GroupIDArr);

$ReturnMsgKey = ($success)? "DeleteMgmtGroupSuccess" : "DeleteMgmtGroupFailed";

intranet_closedb();

$para = "Keyword=$Keyword&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage";
header("Location: management_group.php?".$para."&ReturnMsgKey=".$ReturnMsgKey);
?>