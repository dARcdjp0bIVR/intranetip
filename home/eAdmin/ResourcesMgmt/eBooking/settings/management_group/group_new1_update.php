<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

# Get data
$isEdit = $_REQUEST['isEdit'];
$GroupID = $_REQUEST['GroupID'];
$Keyword = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword']));
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

$GroupInfoArr = array();
$GroupInfoArr['GroupName'] = stripslashes($_REQUEST['GroupName']);
$GroupInfoArr['Description'] = stripslashes($_REQUEST['Description']);

if ($isEdit)
{
	# edit
	$objMgmtGroup = new eBooking_MgmtGroup($GroupID);
	$Success = $objMgmtGroup->Update_Object($GroupInfoArr);
	$ReturnMsgKey = ($Success)? 'EditMgmtGroupSuccess' : 'EditMgmtGroupFailed';
	
	$nextPage = 'management_group.php';
}
else
{
	# new
	$libMgmtGroup = new eBooking_MgmtGroup();
	$GroupID = $libMgmtGroup->Insert_Object($GroupInfoArr);
	
	$nextPage = 'group_new2.php';
}

intranet_closedb();

$para = "GroupID=$GroupID&Keyword=$Keyword&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage&ReturnMsgKey=$ReturnMsgKey";
header("Location: $nextPage?$para");
?>