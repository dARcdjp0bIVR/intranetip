<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_ManagementGroup";
$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$GroupID = $_REQUEST['GroupID'];
$Keyword = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword']));
$pageNo = $_REQUEST['pageNo'];
$order = $_REQUEST['order'];
$field = $_REQUEST['field'];
$numPerPage = $_REQUEST['numPerPage'];

$FromMemberList = $_REQUEST['FromMemberList'];

if ($FromMemberList)
{
	$IndexInfoArr['Keyword'] = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword_Index']));
	$IndexInfoArr['pageNo'] = $_REQUEST['pageNo_Index'];
	$IndexInfoArr['order'] = $_REQUEST['order_Index'];
	$IndexInfoArr['field'] = $_REQUEST['field_Index'];
	$IndexInfoArr['numPerPage'] = $_REQUEST['numPerPage_Index'];

	### Navigation
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['MgmtGroupList'], "javascript:js_Back_To_Group_List()"); 
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['MemberList'], "javascript:js_Back_To_Member_List()"); 
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['AddMember'], ""); 
	$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
	
	### Cancel Button
	$btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Back_To_Member_List()", $id="Btn_Cancel");
	
	### Group Info Display
	$objMgmtGroup = new eBooking_MgmtGroup($GroupID);
	$GroupName = $objMgmtGroup->GroupName;
	$Description = nl2br($objMgmtGroup->Description);
		
	$GropuInfoTable = '';
	$GropuInfoTable .= '<table width="88%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
		$GropuInfoTable .= '<tr>'."\n";
			$GropuInfoTable .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['ManagementGroup']['GroupName'].'</td>'."\n";
			$GropuInfoTable .= '<td class="tabletext">'.$GroupName.'</td>'."\n";
		$GropuInfoTable .= '</tr>'."\n";
		$GropuInfoTable .= '<tr>'."\n";
			$GropuInfoTable .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['ManagementGroup']['Description'].'</td>'."\n";
			$GropuInfoTable .= '<td class="tabletext">'.$Description.'</td>'."\n";
		$GropuInfoTable .= '</tr>'."\n";
	$GropuInfoTable .= '</table>'."\n";
}
else
{
	### Navigation
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['MgmtGroupList'], "javascript:js_Back_To_Group_List()"); 
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['NewGroup'], ""); 
	$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);
	
	### Step Table
	$STEPS_OBJ[] = array($Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step1'], 0);
	$STEPS_OBJ[] = array($Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step2'], 1);
	$StepTable = $linterface->GET_STEPS($STEPS_OBJ);
	
	### Cancel Button
	$btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Back_To_Group_List()", $id="Btn_Cancel");
}


$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'], "", 0);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);




### Choose Member Btn
$btn_ChooseMember = $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button", "javascript:newWindow('../choose_member.php?fieldname=SelectedUserIDArr[]', 9)");

### Auto-complete login search
$UserLoginInput = '';
$UserLoginInput .= '<div style="float:left;">';
	$UserLoginInput .= '<input type="text" id="UserSearchTb" name="UserSearchTb" value=""/>';
$UserLoginInput .= '</div>';


### Member Selection Box & Remove all Btn
//$objMgmtGroup = new eBooking_MgmtGroup($GroupID);
//$MemberArr = $objMgmtGroup->Get_Member_List();
$MemberSelectionBox = $linterface->GET_SELECTION_BOX($MemberArr, "name='SelectedUserIDArr[]' id='SelectedUserIDArr[]' class='select_studentlist' size='15' multiple='multiple'", "");
$btn_RemoveSelected = $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button", "javascript:checkOptionRemove(document.getElementById('SelectedUserIDArr[]'))");


### Continue Button
$btn_Submit = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", $onclick="js_Add_Member();", $id="Btn_Submit");
		
		

$thisHTML = '';
$thisHTML .= '<br />'."\n";
$thisHTML .= '<form id="form1" name="form1" method="post">'."\n";

	# Navigation & Steps
	$thisHTML .= '<table width="98%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td align="left" class="navigation">'.$PageNavigation.'</td>'."\n";
		$thisHTML .= '</tr>'."\n";
		$thisHTML .= '<tr><td>&nbsp;</td></tr>'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td align="center">'.$StepTable.'</td>'."\n";
		$thisHTML .= '</tr>'."\n";
	$thisHTML .= '</table>'."\n";
	
	$thisHTML .= $GropuInfoTable;
	
	$thisHTML .= '<br />'."\n";
	$thisHTML .= '<br />'."\n";
	
	# Group Info
	$thisHTML .= '<table width="88%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td class="tabletext" width="40%">'.$Lang['eBooking']['Settings']['ManagementGroup']['ChooseUser'].'</td>'."\n";
			$thisHTML .= '<td class="tabletext"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>'."\n";
			$thisHTML .= '<td class="tabletext" width="60%">'.$Lang['eBooking']['Settings']['ManagementGroup']['SelectedUser'].'</td>'."\n";
		$thisHTML .= '</tr>'."\n";
		$thisHTML .= '<tr>
						<td class="tablerow2" valign="top">
							<table width="100%" border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td class="tabletext">'.$i_general_from_class_group.'</td>
							</tr>
							<tr>
								<td class="tabletext">'.$btn_ChooseMember.'</td>
							</tr>
							<tr>
								<td class="tabletext"><i>'.$i_general_or.'</i></td>
							</tr>
							<tr>
								<td class="tabletext">
									'.$i_general_search_by_loginid.'
									<br />
									'.$UserLoginInput.'
								</td>
							</tr>
							</table>
						</td>
						<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'"/10x10.gif" width="10" height="1"></td>
						<td align="left" valign="top">
							<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td align="left">
									'. $MemberSelectionBox .'
									'.$btn_RemoveSelected.'
								</td>
							</tr>
							<tr>
								<td>
									'.$linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']).'
								</td>
							</tr>
							</table>
						</td>
					</tr>'."\n";
		$thisHTML .= '<tr><td>&nbsp;</td></tr>'."\n";
		$thisHTML .= '<tr><td colspan="4"><div class="edit_bottom"><br />'.$btn_Submit.'&nbsp;'.$btn_Cancel.'</div></td></tr>'."\n";
	$thisHTML .= '</table>'."\n";
	
	
	$thisHTML .= '<input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />'."\n";
	$thisHTML .= '<input type="hidden" id="FromMemberList" name="FromMemberList" value="'.$FromMemberList.'" />'."\n";
	
	if ($FromMemberList)
	{
		$thisHTML .= '<input type="hidden" id="Keyword_Index" name="Keyword_Index" value="'.intranet_htmlspecialchars($IndexInfoArr['Keyword']).'" />'."\n";
		$thisHTML .= '<input type="hidden" id="pageNo_Index" name="pageNo_Index" value="'.$IndexInfoArr['pageNo'].'" />'."\n";
		$thisHTML .= '<input type="hidden" id="order_Index" name="order_Index" value="'.$IndexInfoArr['order'].'" />'."\n";
		$thisHTML .= '<input type="hidden" id="field_Index" name="field_Index" value="'.$IndexInfoArr['field'].'" />'."\n";
		$thisHTML .= '<input type="hidden" id="numPerPage_Index" name="numPerPage_Index" value="'.$IndexInfoArr['numPerPage'].'" />'."\n";
	}
	
	### Hidden value to preserve index page table view
	$thisHTML .= '<input type="hidden" id="Keyword" name="Keyword" value="'.intranet_htmlspecialchars($Keyword).'" />'."\n";
	$thisHTML .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$pageNo.'" />'."\n";
	$thisHTML .= '<input type="hidden" id="order" name="order" value="'.$order.'" />'."\n";
	$thisHTML .= '<input type="hidden" id="field" name="field" value="'.$field.'" />'."\n";
	$thisHTML .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$numPerPage.'" />'."\n";
$thisHTML .= '</form>'."\n";

echo $lebooking_ui->initJavaScript();
echo $thisHTML;
?>

<script language="javascript">
$(document).ready( function() {
	// initialize jQuery Auto Complete plugin
	var jsGroupID = $('input#GroupID').val();
	Init_JQuery_AutoComplete('UserSearchTb');
	
	$('input#UserSearchTb').focus();
});

var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("input#" + InputID).autocomplete("ajax_search_user.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1],li.selectValue);
			},
			formatItem: function(row) {
				return row[0] + " (LoginID: " + row[1] + ")";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('SelectedUserIDArr[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	
	Update_Auto_Complete_Extra_Para();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('SelectedUserIDArr[]', 'Array', true);
	ExtraPara['GroupID'] = document.getElementById('GroupID').value;
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Add_Member()
{
	var objForm = document.getElementById('form1');
	
	// Check if the selections contains current members of this group
	var jsSelectedUserList = Get_Selection_Value('SelectedUserIDArr[]', 'String', true);
	$.post(
		"ajax_validate.php", 
		{
			Action: "Validate_Selected_Member", 
			GroupID : $("input#GroupID").val(),
			SelectedUserList : jsSelectedUserList
		},
		function(ReturnData)
		{
			if (ReturnData == '')
			{
				$('div#MemberSelectionWarningDiv').hide();
				checkOptionAll(objForm.elements["SelectedUserIDArr[]"]);
				objForm.action = 'group_new2_update.php';
				objForm.submit();
			}
			else
			{
				$("select#SelectedUserIDArr\\[\\] option[value='" + ReturnData + "']").attr('selected', 'selected'); 
				$('div#MemberSelectionWarningDiv').show();
			}
		}
	);
}

function js_Back_To_Group_List()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'management_group.php';
	objForm.submit();
}

function js_Back_To_Member_List()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'member_list.php';
	objForm.submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>