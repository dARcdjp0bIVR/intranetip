<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_ManagementGroup";
$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission();


$GroupID = $_REQUEST['GroupID'];
$IndexInfoArr['Keyword'] = intranet_htmlspecialchars(stripslashes($_REQUEST['Keyword']));
$IndexInfoArr['pageNo'] = $_REQUEST['pageNo'];
$IndexInfoArr['order'] = $_REQUEST['order'];
$IndexInfoArr['field'] = $_REQUEST['field'];
$IndexInfoArr['numPerPage'] = $_REQUEST['numPerPage'];

$isEdit = ($GroupID!='')? true : false;

$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['MgmtGroupList'], "javascript:js_Go_Back_To_Group_List()"); 
if ($isEdit)
{
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['EditGroup'], "");
	
	$objMgmtGroup = new eBooking_MgmtGroup($GroupID);
	$GroupName = $objMgmtGroup->GroupName;
	$Description = $objMgmtGroup->Description;
	
	$SubmitBtnText = $Lang['Btn']['Submit'];
}
else
{
	$PAGE_NAVIGATION[] = array($Lang['eBooking']['Settings']['ManagementGroup']['NewGroup'], "");
	
	### Step Table
	$STEPS_OBJ[] = array($Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step1'], 1);
	$STEPS_OBJ[] = array($Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step2'], 0);
	$StepTable = $linterface->GET_STEPS($STEPS_OBJ);
	
	$SubmitBtnText = $Lang['Btn']['Continue'];
}

$TAGS_OBJ[] = array($Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'], "", 0);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);


### Navigation
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);


### Textbox for Group Name
$libMgmtGroup = new eBooking_MgmtGroup();
$maxLength = $libMgmtGroup->MaxLength['GroupName'];
$GroupNameTb = '<input type="text" class="textboxtext" name="GroupName" id="GroupName" maxlength="'.$maxLength.'" value="'.intranet_htmlspecialchars($GroupName).'">';

### Textarea for Description
$DescriptionTextArea = $linterface->GET_TEXTAREA('Description', $Description);

### Continue Button
$btn_Continue = $linterface->GET_ACTION_BTN($SubmitBtnText, "button", $onclick="js_Update_Group_Info('$GroupID');", $id="Btn_Submit");
		
### Cancel Button
$btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Go_Back_To_Group_List()", $id="Btn_Cancel");
		

$thisHTML = '';
$thisHTML .= '<br />'."\n";
$thisHTML .= '<form id="form1" name="form1" method="post">'."\n";

	# Navigation & Steps
	$thisHTML .= '<table width="98%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td align="left" class="navigation">'.$PageNavigation.'</td>'."\n";
		$thisHTML .= '</tr>'."\n";
		$thisHTML .= '<tr><td>&nbsp;</td></tr>'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td align="center">'.$StepTable.'</td>'."\n";
		$thisHTML .= '</tr>'."\n";
	$thisHTML .= '</table>'."\n";
	
	$thisHTML .= '<br />'."\n";
	$thisHTML .= '<br />'."\n";
	
	# Group Info
	$thisHTML .= '<table width="88%" border="0" cellpadding"0" cellspacing="0" align="center">'."\n";
		$thisHTML .= '<tr>'."\n";
			$thisHTML .= '<td>'."\n";
				$thisHTML .= '<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">'."\n";
					$thisHTML .= '<tr>'."\n";
						$thisHTML .= '<td width="30%" class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['ManagementGroup']['GroupName'].'</td>'."\n";
						$thisHTML .= '<td>'."\n";
							$thisHTML .= $GroupNameTb;
							$thisHTML .= $linterface->Get_Thickbox_Warning_Msg_Div("GroupNameWarningDiv");
						$thisHTML .= '</td>'."\n";
					$thisHTML .= '</tr>'."\n";
					$thisHTML .= '<tr valign="top">'."\n";
						$thisHTML .= '<td class="formfieldtitle tabletext">'.$Lang['eBooking']['Settings']['ManagementGroup']['Description'].'</td>'."\n";
						$thisHTML .= '<td>'.$DescriptionTextArea.'</td>'."\n";
					$thisHTML .= '</tr>'."\n";
					$thisHTML .= '<tr><td>&nbsp;</td></tr>'."\n";
					$thisHTML .= '<tr><td colspan="2"><div class="edit_bottom"><br />'.$btn_Continue.'&nbsp;'.$btn_Cancel.'</div></td></tr>'."\n";
				$thisHTML .= '</table>'."\n";
			$thisHTML .= '</td>'."\n";
		$thisHTML .= '</tr>'."\n";
	$thisHTML .= '</table>'."\n";	
	
	$thisHTML .= '<input type="hidden" id="isEdit" name="isEdit" value="'.$isEdit.'" />'."\n";
	$thisHTML .= '<input type="hidden" id="GroupID" name="GroupID" value="'.$GroupID.'" />'."\n";
	
	### Hidden value to preserve index page table view
	$thisHTML .= '<input type="hidden" id="Keyword" name="Keyword" value="'.intranet_htmlspecialchars($IndexInfoArr['Keyword']).'" />'."\n";
	$thisHTML .= '<input type="hidden" id="pageNo" name="pageNo" value="'.$IndexInfoArr['pageNo'].'" />'."\n";
	$thisHTML .= '<input type="hidden" id="order" name="order" value="'.$IndexInfoArr['order'].'" />'."\n";
	$thisHTML .= '<input type="hidden" id="field" name="field" value="'.$IndexInfoArr['field'].'" />'."\n";
	$thisHTML .= '<input type="hidden" id="numPerPage" name="numPerPage" value="'.$IndexInfoArr['numPerPage'].'" />'."\n";
$thisHTML .= '</form>'."\n";

echo $thisHTML;
?>

<script language="javascript">
$(document).ready( function() {	
	$('input#GroupName').focus();
	js_Add_KeyUp_Unique_Checking('GroupName', 'GroupNameWarningDiv', '* <?=$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameBlank']?>', '* <?=$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameInUse']?>');
});

function js_Update_Group_Info(jsGroupID)
{
	var WarningDivID = 'GroupNameWarningDiv';
	if (js_Is_Input_Blank('GroupName', WarningDivID, '* <?=$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameBlank']?>'))
	{
		$("input#CategoryName").focus();
		return false;
	}
	
	$.post(
		"ajax_validate.php", 
		{ 
			Action: 'Check_Unique_GroupName',
			Value: $('input#GroupName').val(),
			GroupID: $('input#GroupID').val()
		},
		function(isVaild)
		{
			if(isVaild != 1)
			{
				$('div#' + WarningDivID).html('* <?=$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameInUse']?>');
				$('div#' + WarningDivID).show();
			}
			else
			{
				$('div#' + WarningDivID).html("");
				$('div#' + WarningDivID).hide();
				
				var objForm = document.getElementById('form1');
				objForm.action = 'group_new1_update.php';
				objForm.submit();
			}
		}
	);
}

function js_Go_Back_To_Group_List()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'management_group.php';
	objForm.submit();
}

// Add KeyUp Listener for Checking
function js_Add_KeyUp_Unique_Checking(InputID, WarningDivID, WarnBlankMsg, WarnUniqueMsg)
{
	$("input#" + InputID).keyup(function(){
		js_Check_Unique(InputID, WarningDivID, WarnBlankMsg, WarnUniqueMsg);
	});
}

// validation 
function js_Check_Unique(InputID, WarningDivID, WarnBlankMsg, WarnUniqueMsg)
{
	var InputValue = $("input#" + InputID).val();
	
	if(Trim(InputValue) == '')
	{
		$('div#' + WarningDivID).html(WarnBlankMsg);
		$('div#' + WarningDivID).show();
	}
	else
	{
		$('div#' + WarningDivID).html("");
		$('div#' + WarningDivID).hide();
		
		$.post(
			"ajax_validate.php", 
			{ 
				Action: 'Check_Unique_GroupName',
				Value: InputValue,
				GroupID: $('input#GroupID').val()
			},
			function(isVaild)
			{
				if(isVaild != 1)
				{
					$('div#' + WarningDivID).html(WarnUniqueMsg);
					$('div#' + WarningDivID).show();
				}
				else
				{
					$('div#' + WarningDivID).html("");
					$('div#' + WarningDivID).hide();
				}
			}
		);
	}
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>