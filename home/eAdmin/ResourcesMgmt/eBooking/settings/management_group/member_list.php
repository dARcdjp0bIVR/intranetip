<?php
// using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libebooking.php");
include_once ($PATH_WRT_ROOT . "includes/libebooking_ui.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Settings_ManagementGroup";
$linterface = new interface_html();
$lebooking_ui = new libebooking_ui();
$lebooking_ui->Check_Page_Permission();

$GroupID = $_REQUEST['GroupID'];

// Preserve Table View of this page and the member list page
if ($FromIndex == 1) {
    // index table
    $IndexInfoArr['Keyword'] = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword']));
    $IndexInfoArr['pageNo'] = $_REQUEST['pageNo'];
    $IndexInfoArr['order'] = $_REQUEST['order'];
    $IndexInfoArr['field'] = $_REQUEST['field'];
    $IndexInfoArr['numPerPage'] = $_REQUEST['numPerPage'];
    
    // member list page table
    $Keyword = '';
    $pageNo = '';
    $order = '';
    $field = '';
    $PageSize = '';
} else {
    // index table
    $IndexInfoArr['Keyword'] = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['Keyword_Index']));
    $IndexInfoArr['pageNo'] = $_REQUEST['pageNo_Index'];
    $IndexInfoArr['order'] = $_REQUEST['order_Index'];
    $IndexInfoArr['field'] = $_REQUEST['field_Index'];
    $IndexInfoArr['numPerPage'] = $_REQUEST['numPerPage_Index'];
    
    // member list page table
    $Keyword = stripslashes($_REQUEST['Keyword']);
    $pageNo = $_REQUEST['pageNo'];
    $order = $_REQUEST['order'];
    $field = $_REQUEST['field'];
    $PageSize = $_REQUEST['numPerPage'];
}

$TAGS_OBJ[] = array(
    $Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'],
    "",
    0
);
$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR();

$ReturnMsg = $Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$pageSizeChangeEnabled = true;
echo $lebooking_ui->Get_Settings_ManagementGroup_MemberList_UI($GroupID, $IndexInfoArr, $Keyword, $pageNo, $PageSize, $order, $field);
?>

<script language="javascript">
$(document).ready( function() {	
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_MemberList_Table();
	});

	if($('.red').length > 1){
		$('.redRemarks').show();
	}
});

function js_Reload_MemberList_Table()
{
	jsGroupID = Trim($('input#GroupID').val());
	jsKeyword = Trim($('input#Keyword').val());
	
	js_pageNo = Trim($('input#pageNo').val());
	js_order = Trim($('input#order').val());
	js_field = Trim($('input#field').val());
	js_numPerPage = Trim($('input#numPerPage').val());
	
	$('div#MemberListDiv').html('');
	Block_Element('MemberListDiv');
	
	$('div#MemberListDiv').load(
		"ajax_reload.php", 
		{ 
			Action: 'MemberList_Table',
			GroupID: jsGroupID,
			Keyword: jsKeyword,
			PageNumber: js_pageNo,
			Order: js_order,
			SortField: js_field,
			PageSize: js_numPerPage
		},
		function(ReturnData)
		{
			UnBlock_Element('MemberListDiv');
		}
	);
}

function js_Go_Add_Member()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'group_new2.php?FromMemberList=1';
	objForm.submit();
}

function js_Delete_Member()
{
	checkRemove(document.form1,'DeleteUserIDArr[]','member_delete.php');
}

function js_Go_Back_To_Group_List()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'management_group.php?FromMemberList=1';
	objForm.submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>