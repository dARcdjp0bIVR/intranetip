<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_mgmt_group.php");

intranet_auth();
intranet_opendb();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "Validate_Selected_Member")
{
	$GroupID = stripslashes($_REQUEST['GroupID']);
	$SelectedUserList = stripslashes($_REQUEST['SelectedUserList']);
	
	$objMgmtGroup = new eBooking_MgmtGroup($GroupID);
	$returnString = $objMgmtGroup->Get_Invalidate_Member_Selection($SelectedUserList);
}
else if ($Action == "Check_Unique_GroupName")
{
	$GroupID = stripslashes($_REQUEST['GroupID']);
	$Value = stripslashes($_REQUEST['Value']);
	
	$libMgmtGroup = new eBooking_MgmtGroup();
	$returnString = $libMgmtGroup->Is_Name_Valid($Value, $GroupID);
}

intranet_closedb();

echo $returnString;
?>