<?php
// using : 
/**
 *
 * Date     :   2019-09-11 (Cameron)
 *              add argument $includeUnbookableLocation to Get_Booking_Location_Selection() [case #K169643]
 *
 * Date     :   2018-08-31 (Cameron)
 *              set default booking type to room booking
 *              
 * Date 	:   2012-10-15 (Rita)
 * 				change function name from monthly to usage
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);
$linventory = new libinventory();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageReports_UsageReport";
$TAGS_OBJ[] = array($Lang['eBooking']['Reports']['FieldTitle']['UsageReport']);

$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService);

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

##### Multi Selection #####
# Item
$OnChange = 'js_Changed_Item_SubCat_Selection();';
$itemCatSelection = $lebooking_ui->Get_Item_Category_MultiSelection('', $OnChange, '');
# Room
$roomCatSelection = $lebooking_ui->Get_Booking_Location_Selection('roomID[]', '', 'multiple size=10' ,0, '', 1, $ShowIfFacilityPeriodSetInSchoolYearID = 0, $excludedFacilityID = array(), $includeUnbookableLocation = true);
$roomCatSelection .= $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('roomID[]', 1, '');");
$roomCatSelection .= $linterface->spacer();  	 
$roomCatSelection .= $linterface->MultiSelectionRemark(); 

if ($bookingType == '') {
    $bookingType = LIBEBOOKING_FACILITY_TYPE_ROOM;
}

##### Form Table #####  
$formTableDisplay = $lebooking_ui->Get_Usage_Report_Form_Table($targetFromDate, $targetToDate, $bookingType, $dataUnits, $roomCatSelection, $itemCatSelection, $itemSubCatSelection, $itemSelection); 
	

?>
<script language="javascript">

$(document).ready(
	function()
	{
		js_Hide_Option();
				
	}
)

function js_Changed_Item_SubCat_Selection() 
{
	js_Reload_Item_SubCat_Selection();
}


function js_Reload_Item_SubCat_Selection(jsInitial)
{
	var jsCategoryIDList = Get_Selection_Value('CategoryIDArr[]' , 'String');
	var jsSubCategoryIDList = Get_Selection_Value('targetItemSubCat[]' , 'String');
	
	$('span#subCatSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Item_Category_Selection',
			SelectionID: 'targetItemSubCat[]',
			CategoryID: jsCategoryIDList,
			targetItemSubCat: 'targetItemSubCat[]',
			IsMultiple: 1,
			NoFirst: 1
		},
		
		function(ReturnData)
		{
			js_Select_All('targetItemSubCat[]', 1, 'js_Changed_Item_Selection();');				
		}
		
	);
}


function js_Changed_Item_Selection() 
{
	js_Reload_Item_Selection();

}

function js_Reload_Item_Selection(jsInitial)
{
	var jsCategoryIDList = Get_Selection_Value('CategoryIDArr[]' , 'String');
	var jsSubCategoryIDList = Get_Selection_Value('targetItemSubCat[]' , 'String');
	
	$('span#itemSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Item_Selection',
			SelectionID: 'itemID[]',
			CategoryID: jsCategoryIDList,
			targetItemSubCat: jsSubCategoryIDList,
			IsMultiple: 1,
			NoFirst: 1
		},
		
		function(ReturnData)
		{			
			js_Select_All('targetItem[]', 1);							
		}
		
	);
}

function js_Hide_Option(){
	
	if($('#room').is(':checked')) {
		
		$('#roomSelectionSpan').show();
		$('#itemCatSelectionSpan').hide();
		$('#subCatSelectionSpan').hide();
		$('#itemSelectionSpan').hide();
		js_Select_All('roomID[]', 1, '');
							
	}
	else{
	
		$('#roomSelectionSpan').hide();
		$('#itemCatSelectionSpan').show();
		$('#subCatSelectionSpan').show();
		$('#itemSelectionSpan').show();
		
		js_Select_All('CategoryIDArr[]', 1);		
		js_Changed_Item_SubCat_Selection();
		//js_Changed_Item_SubCat_Selection();
		
	}	
}


function checkForm()
{
	
	var obj = document.form1;
	var error_no = 0;
	
	$('#div_DateEnd_err_msg').hide();
	$('#div_Item_err_msg').hide();
	$('#div_Room_err_msg').hide();
	
	///// Period
	if (compareDate(obj.targetFromDate.value, obj.targetToDate.value) > 0) 
	{
		//document.getElementById('div_DateEnd_err_msg').innerHTML = '<span class="tabletextrequire"><?=$i_invalid_date?></span>';
		$('#div_DateEnd_err_msg').show();
		$('input#targetFromDate').focus();
		
		error_no++;
	}

	///// Room
	var roomIDList = Get_Selection_Value('roomID[]' , 'String');
	if ($('#room').is(':checked') && roomIDList=='') 
	{				
		//document.getElementById('div_Room_err_msg').innerHTML = '<span class="tabletextrequire"><?=$i_alert_pleaseselect.$Lang['eBooking']['General']['FieldTitle']['Room']?></span>';
		$('#div_Room_err_msg').show();
		$('#div_Item_err_msg').hide();
		$('#room').focus();
		error_no++;	
	}
	
	///// Item
	var categoryIDArr = Get_Selection_Value('CategoryIDArr[]' , 'String');
	var targetItemSubCatList = Get_Selection_Value('targetItemSubCat[]' , 'String');	
	var itemIDList = Get_Selection_Value('targetItem[]' , 'String');
	
	if ($('#item').is(':checked') && (itemIDList=='' || categoryIDArr=='' || targetItemSubCatList=='')) 
	{			
		//document.getElementById('div_Item_err_msg').innerHTML = '<span class="tabletextrequire"><?=$i_alert_pleaseselect.$Lang['eBooking']['General']['FieldTitle']['Item']?></span>';
		$('#div_Room_err_msg').hide();
		$('#div_Item_err_msg').show();
		$('#item').focus();

		error_no++;
	}
	
	if(error_no>0)
	{		
		return false;
	}
	else
	{
		return true
	}
}

</script>

<?=$formTableDisplay?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
