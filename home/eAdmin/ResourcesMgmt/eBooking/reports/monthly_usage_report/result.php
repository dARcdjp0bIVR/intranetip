<?php
// using : 
/**
 *
 * Date     :   2019-09-11 (Cameron)
 *              - add argument $includeUnbookable to Get_Item_MultiSelection() [case #K169643]
 *              - add argument $includeUnbookableLocation to Get_Booking_Location_Selection()
 *              - fix js_Hide_Option(), move js_Select_All('CategoryIDArr[]', 1, '') before js_Reload_Item_SubCat_Selection()
 *
 * Date 	:   2012-10-15 (Rita)
 * 				change function name from monthly to usage
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

##### make sure form submit
if(empty($targetToDate))
{
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();
$libebooking = new libebooking();
$lebooking_ui = new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);
$linventory = new libinventory();
$libenroll = new libclubsenrol();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageReports_UsageReport";
$TAGS_OBJ[] = array($Lang['eBooking']['Reports']['FieldTitle']['UsageReport']);

$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService);
$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

## Get form variables
$targetFromDate = $_POST['targetFromDate'];
$targetToDate = $_POST['targetToDate'];
$targetItemSubCat = $_POST['targetItemSubCat'];
$roomID = $_POST['roomID'];
$bookingType = $_POST['bookingType'];
$dataUnits = $_POST['dataUnits'];
$CategoryArr = $_POST['CategoryArr'];
$targetItem = $_POST['targetItem'];

if($bookingType == LIBEBOOKING_FACILITY_TYPE_ROOM){
	$type = 'Room';	
	$FacilityID = $roomID;	
	
}
elseif($bookingType == LIBEBOOKING_FACILITY_TYPE_ITEM){
	$type = 'Item';
	$FacilityID = $targetItem;
	$CategoryIDArrList = implode(',', $CategoryIDArr);	
	$targetItemSubCatArr = implode(',', $targetItemSubCat);
	$OnChange = 'js_Changed_Item_SubCat_Selection();';
	$itemSubCatSelection = $lebooking_ui->Get_Item_SubCategory_MultiSelection($CategoryIDArrList, $targetItemSubCat, $OnChange, $isAll=0, $isDisabled=0);
//	$OnChange = 'js_Changed_Item_Selection();';
	$itemSelection = $lebooking_ui->Get_Item_MultiSelection($CategoryIDArrList, $targetItemSubCatArr, $targetItem, $includeUnbookable=true);
}


###### Selection Box ######
# item
$OnChange = 'js_Changed_Item_SubCat_Selection();';
$itemCatSelection = $lebooking_ui->Get_Item_Category_MultiSelection($CategoryIDArr, $OnChange, '');

# room
$roomIDArr = $roomID;
$roomCatSelection = $lebooking_ui->Get_Booking_Location_Selection('roomID[]', $roomIDArr,'multiple="multiple" size=10' ,0, '', 1, '', '', $includeUnbookableLocation = true);
$roomCatSelection .= $linterface->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('roomID[]', 1);");
$roomCatSelection .= $linterface->spacer();  	 
$roomCatSelection .= $linterface->MultiSelectionRemark(); 
//$roomCatSelection .= "<div id='div_Room_err_msg'></div>";

###### Get Booking Records ######
$result = $libebooking->Get_Usage_Report_Result_Ary($bookingType, $FacilityID, $targetFromDate, $targetToDate);
//debug_pr($result);

//$sql = "SELECT COUNT(bd.FacilityID) As Times FROM 
//			INTRANET_EBOOKING_RECORD br
//	        INNER JOIN INTRANET_EBOOKING_BOOKING_DETAILS bd ON br.BookingID = bd.BookingID
//			LEFT JOIN INVENTORY_LOCATION il on bd.FacilityID =  il.LocationID
//			WHERE 1 AND br.Date >= '2010-01-01' 
//					AND br.Date <= '2012-08-30' 
//					
//					AND FacilityID = 69
//					AND br.RecordStatus = 1
//			GROUP BY FacilityID
//			ORDER BY FacilityID ASC
//			;";
//
//	$Verifiedresult = $libebooking ->returnArray($sql);
//	
//	debug_pr($Verifiedresult);

###### Get Each Month ######
$monthAry = $libebooking->Get_Month_in_Date_Range($targetFromDate, $targetToDate);

###### Display Result Table ######	
$display = $lebooking_ui->Get_Usage_Report_Result($targetFromDate, $targetToDate, $bookingType, $result, $monthAry, $dataUnits, $version);

###### Form Table ######	
$formTableDisplay = $lebooking_ui->Get_Usage_Report_Form_Table($targetFromDate, $targetToDate, $bookingType, $dataUnits, $roomCatSelection, $itemCatSelection, $itemSubCatSelection, $itemSelection); 
	
?>

<script language="javascript">

$(document).ready(	
	function()
	{
		<?if($bookingType == LIBEBOOKING_FACILITY_TYPE_ROOM){?>
			$('#roomSelectionSpan').show();
			$('#itemCatSelectionSpan').hide();
			$('#subCatSelectionSpan').hide();
			$('#itemSelectionSpan').hide();
		<?}?>
		<?if($bookingType == LIBEBOOKING_FACILITY_TYPE_ITEM){?>
			$('#itemCatSelectionSpan').show();
			$('#subCatSelectionSpan').show();
			$('#itemSelectionSpan').show();
			$('#roomSelectionSpan').hide();
		<?}?>
		// js_Hide_Option();			
	}
)


function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function hideOptionLayer()
{
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
	

}

function showOptionLayer()
{
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}


function js_Changed_Item_SubCat_Selection() 
{
	js_Reload_Item_SubCat_Selection();

}


function js_Reload_Item_SubCat_Selection(jsInitial)
{
	
	var jsCategoryIDList = Get_Selection_Value('CategoryIDArr[]' , 'String');
	var jsSubCategoryIDList = Get_Selection_Value('targetItemSubCat[]' , 'String');
	
	$('span#subCatSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Item_Category_Selection',
			SelectionID: 'CategoryIDArr[]',
			CategoryID: jsCategoryIDList,
			targetItemSubCat: jsSubCategoryIDList,
			ItemID: 'ItemID[]',
			IsMultiple: 1,
			NoFirst: 1
		},
		
		function(ReturnData)
		{
			//js_Preset_Selection('targetItemSubCat[]', jsSubCategoryIDList, '');
			js_Select_All('targetItemSubCat[]', 1);
			js_Reload_Item_Selection();
				
		}
		
	);
}


function js_Changed_Item_Selection() 
{
	js_Reload_Item_Selection();

}

function js_Reload_Item_Selection(jsInitial)
{
	var jsCategoryIDList = Get_Selection_Value('CategoryIDArr[]' , 'String');
	var jsSubCategoryIDList = Get_Selection_Value('targetItemSubCat[]' , 'String');
	var jstargetItemIDList = Get_Selection_Value('targetItem[]' , 'String');
	
	$('span#itemSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Item_Selection',
			SelectionID: 'targetItem[]',
			SubCategoryid: 'targetItemSubCat[]',
			CategoryID: jsCategoryIDList,
			targetItemSubCat: jsSubCategoryIDList,
			IsMultiple: 1,
			NoFirst: 1
			
		},
		
		function(ReturnData)
		{
			
			//js_Preset_Selection('targetItem[]', jstargetItemIDList, '');
			js_Select_All('targetItem[]', 1);
							
		}
		
	);
}


function js_Hide_Option(){
	
	if($('#room').is(':checked')) {
		$('#roomSelectionSpan').show();		
		$('#itemCatSelectionSpan').hide();
		$('#subCatSelectionSpan').hide();
		$('#itemSelectionSpan').hide();	
		js_Select_All('roomID[]', 1, '');
	}
	else{
	
		$('#roomSelectionSpan').hide();
		
		$('#itemCatSelectionSpan').show();
		$('#subCatSelectionSpan').show();
		$('#itemSelectionSpan').show();
		
		js_Reload_Item_Selection();
        js_Select_All('CategoryIDArr[]', 1, '');

		js_Reload_Item_SubCat_Selection();

		js_Select_All('targetItemSubCat[]', 1, '');
		js_Select_All('targetItem[]', 1, '');
		
	
	}	
	
}

function checkForm()
{
	var obj = document.form1;	
	var error_no = 0;

	$('#div_DateEnd_err_msg').hide();
	$('#div_Room_err_msg').hide();
	$('#div_Item_err_msg').hide();
	
	///// Period
	if (compareDate(obj.targetFromDate.value, obj.targetToDate.value) > 0) 
	{
		//document.getElementById('div_DateEnd_err_msg').innerHTML = '<span class="tabletextrequire"><?=$i_invalid_date?></span>';
		$('#div_DateEnd_err_msg').show();
		$('input#targetFromDate').focus();
		error_no++;
	}

	///// Room
	var roomIDList = Get_Selection_Value('roomID[]' , 'String');
	if ($('#room').is(':checked') && roomIDList=='') 
	{
				
		//document.getElementById('div_Room_err_msg').innerHTML = '<span class="tabletextrequire"><?=$i_alert_pleaseselect.$Lang['eBooking']['General']['FieldTitle']['Room']?></span>';
		$('#div_Room_err_msg').show();
		$('#div_Item_err_msg').hide();
		$('#room').focus();
		error_no++;
	
	}
	
	///// Item	
	var categoryIDArr = Get_Selection_Value('CategoryIDArr[]' , 'String');
	var targetItemSubCatList = Get_Selection_Value('targetItemSubCat[]' , 'String');	
	var itemIDList = Get_Selection_Value('targetItem[]' , 'String');
	
	if ($('#item').is(':checked') && (itemIDList=='' || categoryIDArr=='' || targetItemSubCatList=='')) 
	{				
		//document.getElementById('div_Item_err_msg').innerHTML = '<span class="tabletextrequire"><?=$i_alert_pleaseselect.$Lang['eBooking']['General']['FieldTitle']['Item']?></span>';
		$('#div_Item_err_msg').show();
		$('#div_Room_err_msg').hide();
		$('#Item').focus();
		error_no++;	
	}
	
	if(error_no>0)
	{		
		return false;
	}
	else
	{
		return true
	}
}


function click_export()
{
	//checkOption(document.form1.elements['targetItemSubCat[]']);

	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "result.php";
}

function click_print()
{
	document.form1.action="print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action="result.php";
	document.form1.target="_self";
}


//function js_Preset_Selection(jsSelectionObjID, jsPresetArr, jsWithOptGroup)
//{
//	jsWithOptGroup = jsWithOptGroup || false;
//	var jsOptGroupSelector = '';
//	
//	jsSelectionObjID = jsSelectionObjID.replace('[', '\\[').replace(']', '\\]');
//	
//	$('select#' + jsSelectionObjID + ' > option').each( function() {
//		if (jIN_ARRAY(jsPresetArr, $(this).val()) == true) {
//			$(this).attr("selected","selected");
//		}	
//		else {
//			$(this).attr('selected', false);
//		}	
//	});
//	
//	if (jsWithOptGroup) {
//		$('select#' + jsSelectionObjID + ' optgroup > option').each( function() {
//			if (jIN_ARRAY(jsPresetArr, $(this).val()) == true) {
//				$(this).attr("selected","selected");
//			}
//			else {
//				$(this).attr('selected', false);
//			}
//		});
//	}
//}


</script>

<div id="div_form" class="report_option report_show_option">
	<span id="spanShowOption" class="spanShowOption">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	<p class="spacer"></p> 
	<span class="Form_Span" style="display:none">
		<?=$formTableDisplay?>			
	</span>
</div>

<!-- Export & Print button //-->
<div class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>
	</div>
<br style="clear:both" />
</div>

<!-- Display result //-->
<?=$display?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
