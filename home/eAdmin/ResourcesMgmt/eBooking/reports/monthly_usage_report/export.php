<?php
// using : 
/**
 * Date		:	2019-09-11 (Cameron)
 * 				- add argument $includeUnbookable to getAllBookableLocationName() and getAllBookableCategoryItem()
 *
 * Date 	:   2012-10-15 (Rita)
 * 				change function name from monthly to usage
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);
$linventory = new libinventory();
$libenroll = new libclubsenrol();
$le = new libexporttext();
$libebooking = new libebooking();

$targetFromDate = $_POST['targetFromDate'];
$targetToDate = $_POST['targetToDate'];
$targetItemSubCat = $_POST['targetItemSubCat'];
$roomID = $_POST['roomID'];
$bookingType = $_POST['bookingType'];
$dataUnits = $_POST['dataUnits'];
$CategoryArr = $_POST['CategoryArr'];
$targetItem = $_POST['targetItem'];


if($bookingType == LIBEBOOKING_FACILITY_TYPE_ROOM){
	$type = 'Room';
	$FacilityID = $roomID;	
}
elseif($bookingType == LIBEBOOKING_FACILITY_TYPE_ITEM){
	$type='Item';
	$FacilityID = $targetItem;
}

###### Get Booking Records ######
$result = $libebooking->Get_Usage_Report_Result_Ary($bookingType, $FacilityID, $targetFromDate, $targetToDate);

###### Get Each Month ######
$monthAry = $libebooking->Get_Month_in_Date_Range($targetFromDate, $targetToDate);

$export_ary = array();		
###### Display Result Table ######		
if(!empty($result))
	{	
	$export_ary[] = array($Lang['eBooking']['Reports']['UsageReport']['Date'],$targetFromDate, $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['To'],$targetToDate );
	$export_ary[] = array($Lang['eBooking']['Reports']['UsageReport']['Type'],$type);
	$export_ary[] = array($Lang['eBooking']['Reports']['UsageReport']['DataUnits'],$dataUnits);
		
	##### 	 Table header 	 #####	
	if($bookingType == LIBEBOOKING_FACILITY_TYPE_ROOM){			
		$header[] = $Lang['eBooking']['General']['FieldTitle']['Room'];			
	}
	elseif($bookingType == LIBEBOOKING_FACILITY_TYPE_ITEM)
	{				
		$header[] = $Lang['eBooking']['General']['FieldTitle']['Item'];		
	}	
	
	# Loop Each month
	$numOfYearMonth = count($monthAry);
	for($j=0;$j<$numOfYearMonth; $j++){
		$display .= "<th>$monthAry[$j]</th>";
		$header[] = $monthAry[$j];			
	}

	$header[] = $Lang["eEnrolment"]["ActivityParticipationReport"]["Total"];
	$export_ary[] = $header;

	##### END of Table header #####
						
	##### 	Table content	  #####
	$numOfResult = count($result);						
	
	for($j=0; $j<$numOfResult; $j++){
		$FacilityID = $result[$j]['FacilityID'];
		$Date = $result[$j]['Date'];					
		$thisDate = explode('-',$Date);
		$thisYear = $thisDate[0];
		$thisMonth = $thisDate[1];
		$thisYearMonth = $thisYear . '-' . $thisMonth;
							
		$data_ary[$FacilityID][$thisYearMonth][]= $result[$j];
																					
	}	
		      				
	foreach ($data_ary as $FacilityID => $resultAry){
		
		if($bookingType == LIBEBOOKING_FACILITY_TYPE_ROOM){			
			$roomName = $libebooking->getAllBookableLocationName($FacilityID, $BuildingID = '', $includeUnbookable=true);
			$roomFullNameChi = $roomName[0]['FullNameCH'];
			$roomFullNameEng = $roomName[0]['FullNameEN'];					
			$RoomNameLang = Get_Lang_Selection($roomFullNameChi,$roomFullNameEng);
				
			$typeNameDisplay = $RoomNameLang;				
		}
		elseif($bookingType == LIBEBOOKING_FACILITY_TYPE_ITEM)
		{				
			$itemName = $libebooking->getAllBookableCategoryItem('', $FacilityID, $AllowBookingIndependence = false, $includeUnbookable=true);
			$categoryName = $itemName[0]['CategoryName'];
			$subCategoryName = $itemName[0]['Category2Name'];
			$itemName = $itemName[0]['RawItemName'];
			
			$typeNameDisplay = $categoryName. ' > ' . $subCategoryName .' > ' . $itemName;
		}	
		
		
		$content[$FacilityID][]= $typeNameDisplay;
		
		$TotalTimeAry='';
		if($dataUnits == $Lang['eBooking']['Reports']['UsageReport']['Times']){ ##Times
				
			//foreach ($resultAry as $thisYearMonth=> $detailResultAry){
				
			for($j=0;$j<$numOfYearMonth; $j++){
					
				$thisMonth = $monthAry[$j];
				
				$timePerMonth=0;				
				$timesPerMonth = count($resultAry[$thisMonth]);
	
				$content[$FacilityID][]= $timesPerMonth;
	
				$TotalTimeAry += $timesPerMonth;
			}							
		

			$content[$FacilityID][]= $TotalTimeAry;
			$export_ary[]=$content[$FacilityID];
		 
		 }elseif($dataUnits == $Lang['eBooking']['Reports']['UsageReport']['Hours']){	##Hours

			$TotalHour='';
			for($j=0;$j<$numOfYearMonth; $j++){
				
				$thisMonth = $monthAry[$j];
				$hourPerRoom=0;
				for($k=0; $k<count($resultAry[$thisMonth]); $k++)
				{
					$endTime[$k]=$resultAry[$thisMonth][$k]['EndTime'];
					$startTime[$k]=$resultAry[$thisMonth][$k]['StartTime'];
					$hours[$j] = intranet_time_diff($endTime[$k], $startTime[$k], $returnUnit='hour');
					$hourPerRoom +=  $hours[$j];
				}
		
				$content[$FacilityID][]  = number_format($hourPerRoom,2);
				$TotalHour += $hourPerRoom;
			}							
		
			$content[$FacilityID][]  = number_format($TotalHour,2);
			$export_ary[]=$content[$FacilityID];
				
  	 	}				      								    			   						
	}				 			   
			
}
else
{
	$export_ary[] = array($Lang['eBooking']['Reports']['UsageReport']['Date'],$targetFromDate, $Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['To'],$targetToDate );
	$export_ary[] = array($Lang['eBooking']['Reports']['UsageReport']['Type'],$type);
	$export_ary[] = array($Lang['eBooking']['Reports']['UsageReport']['DataUnits'],$dataUnitsDisplay);										
	$export_ary[]= array($Lang['General']['NoRecordFound']);			
}
					

$utf_content = "";
foreach($export_ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();

$filename = "usage_report.csv";
$le->EXPORT_FILE($filename, $utf_content); 
?>
