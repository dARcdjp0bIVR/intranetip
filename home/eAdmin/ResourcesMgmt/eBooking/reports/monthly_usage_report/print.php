<?php
// using : 
/**
 * Date 	:   2012-10-15 (Rita)
 * 				change function name from monthly to usage
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
intranet_auth();
intranet_opendb();

$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);
$linventory = new libinventory();
$libenroll = new libclubsenrol();
$libebooking = new libebooking();
$linterface = new interface_html();

$targetFromDate = $_POST['targetFromDate'];
$targetToDate = $_POST['targetToDate'];
$targetItemSubCat = $_POST['targetItemSubCat'];
$roomID = $_POST['roomID'];
$bookingType = $_POST['bookingType'];
$dataUnits = $_POST['dataUnits'];
$CategoryArr = $_POST['CategoryArr'];
$targetItem = $_POST['targetItem'];

###### Get Booking Records ######
if($bookingType == LIBEBOOKING_FACILITY_TYPE_ROOM){
	$FacilityID = $roomID;	
}
elseif($bookingType == LIBEBOOKING_FACILITY_TYPE_ITEM){
	$FacilityID = $targetItem;
}

###### Get Booking Records ######
$result = $libebooking->Get_Usage_Report_Result_Ary($bookingType, $FacilityID, $targetFromDate, $targetToDate);

###### 	  Get Each Month 	######
$monthAry = $libebooking->Get_Month_in_Date_Range($targetFromDate, $targetToDate);

###### Display Result Table ######	
$version = 'Print';
$display = $lebooking_ui->Get_Usage_Report_Result($targetFromDate, $targetToDate, $bookingType, $result, $monthAry, $dataUnits, $version);


?>
<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<!-- Display result //-->
<?=$display?>

<?
intranet_closedb();
?>
