<?php
// using : 
/**
 * Date     :   2019-09-11 (Cameron)
 *              add argument $includeUnbookable = true to Get_Item_MultiSelection [case #K169643]
 *
 * Date 	:   2012-10-15 (Rita)
 * 				change function name from monthly to usage
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);
$linventory = new libinventory();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Item_Category_Selection")
{
	
	$CategoryID =  $_REQUEST['CategoryID'];
	$SelectionID = $_REQUEST['SelectionID'];
	$FormStageIDArr = explode(",", $_REQUEST['FormStageID']);

	$OnChange = 'js_Changed_Item_Selection();';

	echo $lebooking_ui->Get_Item_SubCategory_MultiSelection($CategoryID, $SelectionID, $OnChange, $isAll=0, $isDisabled=0);
}
elseif ($Action == "Item_Selection")
{

	$targetItemSubCat =  $_REQUEST['targetItemSubCat'];
	$CategoryID =  $_REQUEST['CategoryID'];		
	$SelectionID = $_REQUEST['SelectionID'];
	
	echo $lebooking_ui->Get_Item_MultiSelection($CategoryID, $targetItemSubCat, $SelectionID, $includeUnbookable=true);
}

intranet_closedb();
?>