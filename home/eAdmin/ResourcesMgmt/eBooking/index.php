<?php
// redirect to the default page base on the user role
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

if($_SESSION["eBooking"]["role"] == "ADMIN" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_GROUP_MEMBER" || $_SESSION["eBooking"]["role"] == "MANAGEMENT_AND_FOLLOW_UP_GROUP_MEMBER")
{
	header("Location: management/booking_request.php?clearCoo=1");
}
else if($_SESSION["eBooking"]["role"] == "FOLLOW_UP_GROUP_MEMBER")
{
	header("Location: management/follow_up_work.php?clearCoo=1");
}	                
?>