<?php
// Using : 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageCheckInCheckOut_TodayBooking";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$TAGS_OBJ[] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['TodayRecords'],$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/today_records.php?clearCoo=1");
$TAGS_OBJ[] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['CheckOutBorrow'],$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/check_in_records.php?clearCoo=1",1);
$TAGS_OBJ[] = array($Lang['eBooking']['CheckInCheckOut']['FieldTitle']['History'],$PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eBooking/check_in_check_out/check_out_records.php?clearCoo=1");

$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($returnMsg);

$arrTempBookingID = explode(",",$str_BookingID);
$arrTempBookingID = array_unique($arrTempBookingID);

$BookingFacilityAssoc = array();
$RoomIDArr = array();
$ItemIDArr = array();
foreach($arrTempBookingID as $key=>$booking_id)
{
	$arrBookingID[] = $booking_id;
	$arrBookingDetails = ${$booking_id."BookingRecord"};
	if(is_array($arrBookingDetails))
	{
		for($j=0; $j<sizeof($arrBookingDetails); $j++)
		{
			if(strpos($arrBookingDetails[$j],"ROOM_") === 0)
			{
				$room_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
				$BookingFacilityAssoc[$booking_id]['Room'][] = $room_id;
				$RoomIDArr[] = $room_id;
			}
			else if(strpos($arrBookingDetails[$j],"ITEM_") === 0)
			{
				$item_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
				$BookingFacilityAssoc[$booking_id]['Item'][] = $item_id;
				$ItemIDArr[] = $item_id;
			}
		}
	}
}

echo $lebooking_ui->Get_Check_Out_Confirm_UI($BookingFacilityAssoc, $RoomIDArr, $ItemIDArr, $edit);
	
?>
<script>
function js_Assign_All_Reason()
{
	$("input.reason").val($("input#AssignReason").val());
}
</script>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>