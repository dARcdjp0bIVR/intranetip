<?php
// using : 
/*********************************************** Change Log *****************************************************
 * 2011-03-07 (Carlos): modified rollback transaction logic for each record, not rollback whole batch of records
 ****************************************************************************************************************/
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$linventory = new libinventory();
$lwebmail = new libwebmail();

$BookingRecord = unserialize(urldecode($_POST['BookingRecord']));

if(!$edit)
{
	$SetCheckOutTime = " ,CheckOutTime = NOW() ";
}

foreach($BookingRecord as $booking_id => $BookingDetail)
{
	$result = array();
	$lebooking->Start_Trans();
	
	for($i=0; $i<sizeof($BookingDetail['Room']); $i++)
	{
		$room_id = $BookingDetail['Room'][$i];
		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckOut.", CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($reason['Room'][$room_id]))."' $SetCheckOutTime, DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
		
		$result['UpdateRecordDetails_'.$booking_id.'_room_'.$room_id] = $lebooking->db_db_query($sql);				
	}
	
	for($i=0; $i<sizeof($BookingDetail['Item']); $i++)
	{
		$item_id = $BookingDetail['Item'][$i];
		$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckOut.", CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($reason['Item'][$item_id]))."' $SetCheckOutTime, DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
		
		$result['UpdateRecordDetails_'.$booking_id.'_item_'.$item_id] = $lebooking->db_db_query($sql);				
	}

	if(!in_array(false,$result)){
		$lebooking->Commit_Trans();
		$OverallResult[] = true;
	}else{
		$lebooking->RollBack_Trans();
		$OverallResult[] = false;
	}
}

intranet_closedb();
if($edit==1)
{
	$url = "check_out_records.php";
	if(!in_array(false,$OverallResult))
		$msg = urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['UpdateRemarkSuccessfully']);
	else
		$msg = urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['UpdateRemarkFailed']);
	
	
}
else
{
	$url = "check_in_records.php";
	if(!in_array(false,$OverallResult))
		$msg = urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutSuccessfully']);
	else
		$msg = urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutFailed']);
}

//if(!in_array(false,$OverallResult))
//{
//	$msg = urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutSuccessfully']);
//}else
//{
//	$msg = urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutFailed']);
//}
header("Location: $url?returnMsg=".$msg);
exit();

?>