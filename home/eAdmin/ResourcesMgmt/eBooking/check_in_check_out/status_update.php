<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_auth();
intranet_opendb();

$lebooking = new libebooking();
$linventory = new libinventory();
$lwebmail = new libwebmail();
$libdb = new libdb();

$lebooking->Start_Trans();
$arrTempBookingID = explode(",",$str_BookingID);
$arrTempBookingID = array_unique($arrTempBookingID);
	
foreach($arrTempBookingID as $key=>$val)
{
	$arrBookingID[] = $val;
}

if(sizeof($arrBookingID) > 0)
{
	for($i=0; $i<sizeof($arrBookingID); $i++)
	{
		$booking_id = $arrBookingID[$i];
		
		$sql = "select FacilityID, FacilityType from INTRANET_EBOOKING_BOOKING_DETAILS where BookingID = '$booking_id' ";
		
		$facilityInfoArr = $libdb->returnResultSet($sql);
		
		foreach($facilityInfoArr as $facilityInfo){
			$facilityType = $facilityInfo['FacilityType'];
			switch($facilityType){
				case "1":
					$facilityTypeWithFacilityId = "ROOM_" . $facilityInfo['FacilityID'];
					break;
				case "2":
					$facilityTypeWithFacilityId = "ITEM_" . $facilityInfo['FacilityID'];
					break;
			}	
// 			debug_pr($facilityType);

			if(strpos($facilityTypeWithFacilityId,"ROOM_") === 0)
			{
				$room_id = substr($facilityTypeWithFacilityId,5,strlen($facilityTypeWithFacilityId));
				
				if($page_action == "CheckIn"){
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckIn.", DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "CheckOut"){
					$remark = ${$booking_id."_".$room_id."_remark"};
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckOut.", CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "CancelCheckIn"){
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '0', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "CancelCheckOut"){
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '".LIBEBOOKING_BOOKING_CheckIn."', CheckOutTime = NULL, DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "EditRemark"){
					$remark = ${$booking_id."_".$room_id."_remark"};
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}
			}
			else if(strpos($facilityTypeWithFacilityId,"ITEM_") === 0)
			{
				$item_id = substr($facilityTypeWithFacilityId,5,strlen($facilityTypeWithFacilityId));
				
				if($page_action == "CheckIn"){
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckIn.", DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "CheckOut"){
					$remark = ${$booking_id."_".$item_id."_remark"};
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckOut.", CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "CancelCheckIn"){
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '0', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "CancelCheckOut"){
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '".LIBEBOOKING_BOOKING_CheckIn."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}else if($page_action == "EditRemark"){
					$remark = ${$booking_id."_".$item_id."_remark"};
					$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
					$arrResult[] = $lebooking->db_db_query($sql);
				}
			}
		}
		
		$arrBookingDetails = ${$booking_id."BookingRecord"};
		
// 		if(is_array($arrBookingDetails))
// 		{
// 			$TotalNumOfBookingDetail = $NumOfRoomBooking[0] + $NumOfItemBooking[0];
			
// 			for($j=0; $j<sizeof($arrBookingDetails); $j++)
// 			{
				
// 				if(strpos($arrBookingDetails[$j],"ROOM_") === 0)
// 				{
// 					$room_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
					
// 					if($page_action == "CheckIn"){
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckIn.", DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}else if($page_action == "CheckOut"){
// 						$remark = ${$booking_id."_".$room_id."_remark"};
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckOut.", CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}else if($page_action == "CancelCheckIn"){
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '0', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}else if($page_action == "CancelCheckOut"){
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '".LIBEBOOKING_BOOKING_CheckIn."', CheckOutTime = NULL, DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}else if($page_action == "EditRemark"){
// 						$remark = ${$booking_id."_".$room_id."_remark"};
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ROOM." AND FacilityID = $room_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}
// 				}
// 				else if(strpos($arrBookingDetails[$j],"ITEM_") === 0)
// 				{
// 					$item_id = substr($arrBookingDetails[$j],5,strlen($arrBookingDetails[$j]));
					
// 					if($page_action == "CheckIn"){
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckIn.", DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}else if($page_action == "CheckOut"){
// 						$remark = ${$booking_id."_".$item_id."_remark"};
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = ".LIBEBOOKING_BOOKING_CheckOut.", CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}else if($page_action == "CancelCheckIn"){
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '0', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}else if($page_action == "CancelCheckOut"){
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CurrentStatus = '".LIBEBOOKING_BOOKING_CheckIn."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql); 
// 					}else if($page_action == "EditRemark"){
// 						$remark = ${$booking_id."_".$item_id."_remark"};
// 						$sql = "UPDATE INTRANET_EBOOKING_BOOKING_DETAILS SET CheckInCheckOutRemark = '".$lebooking->Get_Safe_Sql_Query(stripslashes($remark))."', DateModified = NOW() WHERE BookingID = $booking_id AND FacilityType = ".LIBEBOOKING_FACILITY_TYPE_ITEM." AND FacilityID = $item_id ";
// 						$arrResult[] = $lebooking->db_db_query($sql);
// 					}
// 				}
// 			}
// 		}
	}
}
// debug_pr($arrResult);
if(!in_array(false,$arrResult)){
	$lebooking->Commit_Trans();	
	
	switch($page_action){
		case "CheckIn":
			$return_path = "today_records.php?returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedInSuccessfully']);
		break;
		case "CheckOut":
			$return_path = "check_in_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutSuccessfully']);
		break;
		case "CancelCheckIn":
			$return_path = "check_in_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckInSuccessfully']);
		break;
		case "CancelCheckOut":
			$return_path = "check_out_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckOutSuccessfully']);
		break;
		case "EditRemark":
			$return_path = "check_out_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['UpdateRemarkSuccessfully']);
		break;
	}
	
	header("Location: $return_path");
	exit();
}else{
	$lebooking->RollBack_Trans();
	
	switch($page_action){
		case "CheckIn":
			$return_path = "today_records.php?returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedInFailed']);
		break;
		case "CheckOut":
			$return_path = "check_in_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['ItemCheckedOutFailed']);
		break;
		case "CancelCheckIn":
			$return_path = "check_in_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckInFailed']);
		break;
		case "CancelCheckOut":
			$return_path = "check_out_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['CancelCheckOutFailed']);
		break;
		case "EditRemark":
			$return_path = "check_out_records.php?StartDate=$StartDate&EndDate=$EndDate&returnMsg=".urlencode($Lang['eBooking']['CheckInCheckOut']['ReturnMsg']['UpdateRemarkFailed']);
		break;
	}
	
	header("Location: $return_path");
	exit();
}

intranet_closedb();
?>