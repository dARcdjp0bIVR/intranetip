<?php 

//using : Isaac
################################################## Change Log ############################################################
## Date		:	2018-01-15 (Isaac)
##              seperated Booked By	into Booked By and Booking Date columns 
## Date		:	2018-01-15 (Isaac)
##              fixed linebreak for $arrBookingDetails[$booking_id]['Remark'] and GET_EXPORT_TXT calling.
##	Date    :	2017-11-22 (Isaac)
##              Post original values of the Checkin-checkout filter to prevent consistent use of the new value before user click the apply button in Today_records.php\
##
## Date		:	2017-11-20 (Isaac)
##              add Booking Remark
## 
##########################################################################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");

include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$field=trim($field)!=''?$field:1;
$order=trim($order)!=''?$order:1;

$linventory 	= new libinventory();
$lebooking 		= new libebooking();
$lexport = new libexporttext();
$li 			= new libdbtable2007($field, $order, $PageNumber);

$keyword = standardizeFormPostValue($_POST['savedkeyword']);

//header
$HeaderArr = array();

// $Header[] = '';
$Header[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem'];
$Header[] =	$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)'];
$Header[] =	$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time'];
$Header[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson'];
$Header[] = $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy'];
$Header[] = $Lang['eBooking']['Mail']['FieldTitle']['BookingDate'];
$Header[] = $i_BookingRemark;

$HeaderArr = array($Header);

$arrTempResult = $lebooking->Get_All_Facility_Booking_Record('',LIBEBOOKING_BOOKING_STATUS_APPROVED, $StartDate, $EndDate, '', '', LIBEBOOKING_BOOKING_CheckIn);

if(sizeof($arrTempResult)>0)
{
	for($i=0; $i<sizeof($arrTempResult); $i++)
	{
		list($booking_id,$period_id,$booking_remark,$booking_date_string,$booking_start_time,$booking_end_time,
		$requested_by,$request_date,$responsible_ppl,
		$room_id,$room_name,$room_PIC,$room_booking_process_date,$room_booking_status,
		$item_id,$item_name,$item_PIC,$item_booking_process_date,$item_booking_status,
		$is_reserve, $room_check_in_out_remarks, $item_check_in_out_remarks, $room_PICID, $item_PICID,
		$room_reject_reason, $item_reject_reason, $Attachment, $room_current_status, $item_current_status,
		$room_check_in_time, $item_check_in_time) = $arrTempResult[$i];

		$arrBookingID[] = $booking_id;
		$arrBookingDetails[$booking_id]['Date'] = $booking_date_string;
		$arrBookingDetails[$booking_id]['RelatedPeriod'] = $period_id;
		$arrBookingDetails[$booking_id]['Remark'] = htmlspecialchars($booking_remark);
		$arrBookingDetails[$booking_id]['StartTime'] = $booking_start_time;
		$arrBookingDetails[$booking_id]['EndTime'] = $booking_end_time;
		$arrBookingDetails[$booking_id]['RequestedBy'] = $requested_by;
		$arrBookingDetails[$booking_id]['RequestedDate'] = $request_date;
		$arrBookingDetails[$booking_id]['IsReserve'] = $is_reserve;
	
		$curr_date = date("Y-m-d");

			$booking_day_before = $request_date;

		$arrBookingDetails[$booking_id]['RequestedDayBefore'] = $booking_day_before;
		
		$arrBookingDetails[$booking_id]['ResponsiblePerson'] = $responsible_ppl;
		if($room_id != ""){
			$arrBookingDetails[$booking_id]['RoomBooking'] = 1;
			$arrBookingDetails[$booking_id]['RelatedRoom'][] = $room_id;
			$arrBookingDetails[$booking_id][$room_id]['RoomName'] = $room_name;
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingPIC'] = $room_PIC;
			$arrBookingDetails[$booking_id][$room_id]['RoomProcessDate'] = $room_booking_process_date;
					
			$curr_date = date("Y-m-d");
				$room_booking_process_day_before = $room_booking_process_date;
			$arrBookingDetails[$booking_id][$room_id]['RoomProcessDayBefore'] = $room_booking_process_day_before;
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
		}else{
			$arrBookingDetails[$booking_id]['RoomBooking'] = 0;
			$arrBookingDetails[$booking_id]['RelatedRoom'][] = array();
			$arrBookingDetails[$booking_id][$room_id]['RoomBookingStatus'] = $room_booking_status;
		}
		
		if($item_id != ""){
			$arrBookingDetails[$booking_id]['ItemBooking'] = 1;
			$arrBookingDetails[$booking_id]['RelatedItem'][] = $item_id;
			$arrBookingDetails[$booking_id][$item_id]['ItemName'] = $item_name;
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingPIC'] = $item_PIC;
			$arrBookingDetails[$booking_id][$item_id]['ItemProcessDate'] = $item_booking_process_date;
	
			$curr_date = date("Y-m-d");
//			$item_booking_process_day_before = floor((strtotime($curr_date)-strtotime($item_booking_process_date))/86400);
//			if($room_booking_process_day_before > 7) {
				$item_booking_process_day_before = $item_booking_process_date;
//			} else {
//				$item_booking_process_day_before .= $Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'];
//			}
			$arrBookingDetails[$booking_id][$item_id]['ItemProcessDayBefore'] = $item_booking_process_day_before;
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
		}else{
			$arrBookingDetails[$booking_id]['ItemBooking'] = 0;
			$arrBookingDetails[$booking_id]['RelatedItem'][] = array();
			$arrBookingDetails[$booking_id][$item_id]['ItemBookingStatus'] = $item_booking_status;
		}
	}
	$arrBookingID = array_unique($arrBookingID);
	ksort($arrBookingID);

	# Default Table Settings
	$pageNo = ($pageNo == '')? $li->pageNo=1 : $li->pageNo=$pageNo;
	$numPerPage = ($numPerPage == '')? $li->page_size=20 : $li->page_size=$numPerPage;
	$Order = ($Order == '')? 1 : $Order;
	$SortField = ($SortField == '')? 0 : $SortField;
	
	if($pageNo == 1)
	{
		$start = $pageNo;
		$end = $numPerPage;
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($arrBookingID),($li->pageNo*$li->page_size));
	}
	else 
	{
		$start = ($pageNo*$numPerPage)-$numPerPage+1;
		
		if($start>sizeof($arrBookingID)){
			$start = 1;
		}
		
		$end = ($pageNo*$numPerPage);
		$li->n_start = $start-1;
		$li->n_end = min(sizeof($arrBookingID),($li->pageNo*$li->page_size));
	}
	
	foreach($arrBookingID as $key=>$booking_id)
	{
		if($booking_id != "")
			$arrSortedBookingID[] = $booking_id;
	}

	for($i = $start-1; $i < $end; $i++)
	{
		$arrDisplayBookingID[] = $arrSortedBookingID[$i];
	}
	
	foreach($arrDisplayBookingID as $key=>$booking_id)
	{
		$arrRoomBookingDetails = array();
		$arrItemBookingDetails = array();
		
		if(is_array($arrBookingDetails[$booking_id]['RelatedRoom']))
			$arrTempRoomBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedRoom']);
		if(is_array($arrBookingDetails[$booking_id]['RelatedItem']))
			$arrTempItemBookingDetails = array_unique($arrBookingDetails[$booking_id]['RelatedItem']);
		
		if(sizeof($arrTempRoomBookingDetails) > 0)
		{
			foreach($arrTempRoomBookingDetails as $key=>$val)
			{
				if(($val != "") && ($val != "0"))
				{
					$arrRoomBookingDetails[] = $val;
				}
			}
		}
		
		if(sizeof($arrTempItemBookingDetails) > 0)
		{
			foreach($arrTempItemBookingDetails as $key=>$val)
			{
				if(($val != "") && ($val != "0"))
				{
					$arrItemBookingDetails[] = $val;
				}
			}
		}
		
		$row_num = $start++;
	
		if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1) )
		{
				## Book for room & item at the same time
				$room_id = $arrRoomBookingDetails[0];
				$RoomName =$arrBookingDetails[$booking_id][$room_id]['RoomName'];
				$Date = $arrBookingDetails[$booking_id]['Date'];
				$Time = date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']));
				$ResponsiblePerson =$arrBookingDetails[$booking_id]['ResponsiblePerson'];
				$RequestedBy = $arrBookingDetails[$booking_id]['RequestedBy'];
				$BookingDate = $arrBookingDetails[$booking_id]['RequestedDayBefore'];
				$Remark = $arrBookingDetails[$booking_id]['Remark'];	
				$Content[] = array($RoomName,$Date,$Time,$ResponsiblePerson,$RequestedBy, $BookingDate, $Remark);
				
				for($i=0; $i<sizeof($arrItemBookingDetails); $i++)
					{
						$item_id = $arrItemBookingDetails[$i];
						$ItemName = $arrBookingDetails[$booking_id][$item_id]['ItemName'];
						$ItemDate = '';
						$ItemTime = '';
						$ItemResponsiblePerson = '';
						$ItemRequestedBy ='';
						$ItemBookingDate ='';
						$ItemRemark = $arrBookingDetails[$booking_id]['Remark'];
	
						$Content[] = array($ItemName,$ItemDate,$ItemTime,$ItemResponsiblePerson,$ItemRequestedBy, $ItemBookingDate, $ItemRemark);
						}
		}
		else if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 1) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 0) )
		{
				## Book for Room Only
				$room_id = $arrRoomBookingDetails[0];
	
				$RoomItemName =$arrBookingDetails[$booking_id][$room_id]['RoomName'];
				$Date = $arrBookingDetails[$booking_id]['Date'];
				$Time = date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']));
				$ResponsiblePerson = $arrBookingDetails[$booking_id]['ResponsiblePerson'];
				$RequestedBy = $arrBookingDetails[$booking_id]['RequestedBy'];
				$BookingDate = $arrBookingDetails[$booking_id]['RequestedDayBefore'];
				$Remark = $arrBookingDetails[$booking_id]['Remark'];
				
				
				$Content[] = array($RoomItemName,$Date,$Time,$ResponsiblePerson,$RequestedBy, $BookingDate, $Remark);
				
					
		}
		else if( ($arrBookingDetails[$booking_id]['RoomBooking'] == 0) && ($arrBookingDetails[$booking_id]['ItemBooking'] == 1) )
		{
			## Book for Item Only
			for($i=0; $i<sizeof($arrItemBookingDetails); $i++)
			{
				$item_id = $arrItemBookingDetails[$i];
				
				$RoomItemName=$arrBookingDetails[$booking_id][$item_id]['ItemName'];
				$Date = $arrBookingDetails[$booking_id]['Date'];
				$Time = date("H:i",strtotime($arrBookingDetails[$booking_id]['StartTime']))." - ".date("H:i",strtotime($arrBookingDetails[$booking_id]['EndTime']));
				$ResponsiblePerson= $arrBookingDetails[$booking_id]['ResponsiblePerson'];
				$RequestedBy= $arrBookingDetails[$booking_id]['RequestedBy'];
				$BookingDate = $arrBookingDetails[$booking_id]['RequestedDayBefore'];
				$Remark = $arrBookingDetails[$booking_id]['Remark'];
				
				$Content[] = array($RoomItemName,$Date,$Time,$ResponsiblePerson, $BookingDate, $RequestedBy,$Remark);
					
			}
		}
	}
}
						
$export_content = $lexport->GET_EXPORT_TXT($Content, $HeaderArr, "", "\r\n", "", 0, "11", 1);
							
intranet_closedb();
							
$filename = "check_in_record.csv";
$lexport->EXPORT_FILE($filename, $export_content);
?>