<?php
# using: 

################################################## Change Log ############################################################
## Date		:	2017-08-28 (Isaac)
##              added argumant $RemarkNum for checkout remark to show batch correct checkout remark for batch booking of room + items
##
## Date		:	2017-11-24 (Isaac)
##              added $RemarkActionArr and foreach() function to run the switch statement through the array
##
##########################################################################################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

$lebooking = new libebooking();
$lebooking_ui = new libebooking_ui();


intranet_auth();
intranet_opendb();

//debug_pr($Action);
$RemarkActionArr = explode( ',', $Action );
// debug_pr($RemarkAction);
foreach ((array)$RemarkActionArr as $case){
    //  debug_pr($case);
    switch ($case){
        // switch ($Action){
        case "Reload_Remark":
            $layer_content = $lebooking_ui->Get_My_Booking_Record_Detail_Layer($BookingID);
            //echo $layer_content;
            break;
        case "Reload_CheckOutRemark":
            $layer_content = $lebooking_ui->Get_Booking_Record_Check_Out_Remark_Layer($BookingID, $FacilityType, $FacilityID, $RemarkNum);
            //echo $layer_content;
            break;
            
    }
    echo $layer_content;
}

intranet_closedb();
?>