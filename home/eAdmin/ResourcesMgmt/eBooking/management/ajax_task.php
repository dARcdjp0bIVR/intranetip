<?php
// using:  

################################################## Change Log ##################################################
##
##	Date	:	2019-06-04 (Cameron)
##	Details :	-call Get_Booking_Timetable_Everyday for $sys_custom['eBooking']['EverydayTimetable'] in Get_Booking_Timetable
##
##	Date	:	2019-05-03 (Cameron)
##	Details :	- add parameter $Keyword to ShowRoomBookingRecordInListView() and ShowItemBookingRecordInListView()
##
##	Date	:	2019-04-30 (Cameron)
##	Details :	- fix potential sql injection problem by cast variables to integer
##
##  Date	:	2018-08-15 (Henry)
##  Details     fixed XSS
##
##	Date	:	2017-05-31	Omas
##				added $sys_custom['eBooking']['SpecialBookingLogic']['lstlcw'] - #L110062
##
##	Date	:	2015-10-12	Omas
##	Details	:	added ShowRoomBookingRecordInMonthView ,ShowItemBookingRecordInMonthView
##
##	Date	:	2015-05-29	Omas
##	Details		Fixed: Import Check time clash wrongly
##
##	Date	:	2015-05-12	Omas
## 	Details		Create this page
##
##	Date	:	2015-03-30 (Omas)
##	Details	:	-add SaveDayViewSetting , ShowRoomBookingRecordInDayView
##
##	Date	:	2014-06-20 (Bill)
##	Details	:	modified LoadMultiSubCatTable
##
##	Date	:	2013-08-22 (Carlos)
##	Details :	modified $task GetRoomAttachment, for $sys_custom['eBooking_ReserveMultipleRooms']
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "EditBookingRequest"
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "CancelBookingRequest"
##
##	Date	:	2010-06-09 (Ronald)
##	Details :	Add a new case "DeleteBookingRequest"

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_auth();
intranet_opendb();

$Keyword = cleanHtmlJavascript($Keyword);
$numPerPage = cleanHtmlJavascript($numPerPage);

$li = new libebooking_ui();

switch ($task)
{
	case "Get_Booking_Calender":
		include_once($PATH_WRT_ROOT."includes/libcal.php");
		include_once($PATH_WRT_ROOT."includes/libcalevent.php");
		$newts = mktime(0,0,0,$month,1,$year);
		
		$FacilityType = IntegerSafe($FacilityType);
		$FacilityID = IntegerSafe($FacilityID);
		$From_eAdmin = IntegerSafe($From_eAdmin);
		
		$newYear =date("Y",$newts);
		$newMonth = date("n",$newts);
		$selected = explode(",",$selected);
		
		$Calendar = $li->Get_Booking_Calendar($FacilityType,$FacilityID,$newMonth,$newYear,$selected, $From_eAdmin);
		
		if($Calendar)
			echo $Calendar;
	break;
	case "Get_Booking_Timetable":
	    $FacilityType = IntegerSafe($FacilityType);
	    $FacilityID = IntegerSafe($FacilityID);
	    $WeekDiff = IntegerSafe($WeekDiff);
	    $From_eAdmin = IntegerSafe($From_eAdmin);
	    
		if($sys_custom['eBooking']['SpecialBookingLogic']['lstlcw']){
			$Timetable = $li->Get_Booking_Timetable_lstlcw($FacilityType, $FacilityID, $WeekDiff, $From_eAdmin);
		}
		else if ($sys_custom['eBooking']['EverydayTimetable']) {
		    $Timetable = $li->Get_Booking_Timetable_Everyday($FacilityType, $FacilityID, $WeekDiff, $From_eAdmin);
		}else{
			$Timetable = $li->Get_Booking_Timetable($FacilityType, $FacilityID, $WeekDiff, $From_eAdmin);
		}
		if($Timetable)
			echo $Timetable;
	break;	
	case "LoadMultiSubCatTable":
	    $CategoryID = IntegerSafe($CategoryID);
		if($CategoryID)	
		{
			$SubCatTable = $li->Get_Item_Sub_Category_Multi_Select_Table($CategoryID,true);
			echo $SubCatTable;
		} 
	break;	
	
	case "LoadBookingItemsList":
	    $CategoryID = IntegerSafe($CategoryID);
		if($CategoryID&&!$SubCatIDList)
			break;
			
		if($SubCatIDList)	
			$SubCatIDArr = explode(",",$SubCatIDList);
			$SubCatIDArr = IntegerSafe($SubCatIDArr);
			$From_eAdmin = IntegerSafe($From_eAdmin);
		$ItemTable = $li->Get_New_Booking_Items_List_Table($CategoryID,$SubCatIDArr, $Keyword, $From_eAdmin);
		echo $ItemTable;
	break;
	
	case "LoadReserveItemsList":
	    $CategoryID = IntegerSafe($CategoryID);
		if($CategoryID&&!$SubCatIDList)
			break;
			
		if($SubCatIDList)	
			$SubCatIDArr = explode(",",$SubCatIDList);
			$SubCatIDArr = IntegerSafe($SubCatIDArr);
		$ItemTable = $li->Get_Reserve_Items_List_Table($CategoryID,$SubCatIDArr, $Keyword);
		echo $ItemTable;
	break;
	
	case "CheckTimeCrash":
		$DateArr = explode(",",$DateList);
		$StartTimeArr = explode(",",$StartTimeStr);
		$EndTimeArr = explode(",",$EndTimeStr);
		$FacilityType = IntegerSafe($FacilityType);
		$FacilityID = IntegerSafe($FacilityID);
		
		$TimeSelectTable = $li->Get_Specific_Time_Range_Date_Select_Table($FacilityType, $FacilityID, $DateArr, $StartTimeArr, $EndTimeArr, $AllowReserve, $SelectedDateList);
		
		echo $TimeSelectTable;
	break;
	
	case "LoadItemBookingLayerTable":
		$ItemID = IntegerSafe($_REQUEST['ItemID']);
		
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		
		# Get Booking Day Beforehand
		include_once($PATH_WRT_ROOT."includes/libebooking_item.php");
		$objItem = new eBooking_Item($ItemID);
		$BookingDayBeforehand = $objItem->BookingDayBeforehand;
		
		# Get Booking Info
		$DateTimeArr = unserialize(rawurldecode($_REQUEST['DateTimeArr']));
		$ItemBookingDetailsTable = $li->Get_Item_Booking_Details_Table($ItemID, $DateTimeArr);
		
		# Get Available Booking Period
		$DateArr = array_keys($DateTimeArr);
		$SinceDate = $DateArr[0];
		$UntilDate = $DateArr[count($DateArr)-1];
		$ItemAvailableBookingPeriodTable = $li->Get_Available_Booking_Period_Layer_Table($FacilityType=2, $ItemID, $SinceDate, $UntilDate);
		
				
		
		$thisReturn = '';
		$thisReturn .= $Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDayBeforehand'].': '.$BookingDayBeforehand.' '.$Lang['eBooking']['Settings']['GeneralPermission']['DayBefore'];
		$thisReturn .= '<span class="tabletextremark"> ('.$Lang['eBooking']['Settings']['GeneralPermission']['RemarksArr']['ZeroBookingDayBeforehandMeansNoLimit'].')</span>';
		$thisReturn .= '<br />';
		$thisReturn .= '<br />';
		$thisReturn .= '<div class="edit_bottom" style="height:1px;"></div>';
		$thisReturn .= $linterface->GET_NAVIGATION2($Lang['eBooking']['eService']['FieldTitle']['ExistingBookingRecord']);
		$thisReturn .= '<br />';
		$thisReturn .= $ItemBookingDetailsTable;
		$thisReturn .= '<br />';
		$thisReturn .= '<br />';
		$thisReturn .= $linterface->GET_NAVIGATION2($Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod']);
		$thisReturn .= $ItemAvailableBookingPeriodTable;
		$thisReturn .= '<br />';
		
		echo $thisReturn;
	break;
	
	case "EditBookingRequest" :
		### Only can be edit if the booking status = pending
	    $strBookingID = IntegerSafe($strBookingID);
		$arrBookingID = explode(",",$strBookingID);
		
		if(sizeof($arrBookingID) > 0) {
			for($i=0; $i<sizeof($arrBookingID); $i++) {
				$booking_id = $arrBookingID[$i];
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ROOM."'";
				$arrRoomResult = $li->returnVector($sql);
				
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ITEM."'";
				$arrFacilitiesResult = $li->returnVector($sql);
				
				
				if(sizeof($arrRoomResult) > 0) {
					if($arrRoomResult[0] == 0) {
						$pass[$booking_id]['RoomCheck'] = 1;
					} else {
						$pass[$booking_id]['RoomCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['RoomCheck'] = 1;
				}
					
				if(sizeof($arrFacilitiesResult) > 0) {
					if($arrFacilitiesResult[0] == 0) {
						$pass[$booking_id]['FacilityCheck'] = 1;
					} else {
						$pass[$booking_id]['FacilityCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['FacilityCheck'] = 1;
				}
				
				if(in_array(0,$pass[$booking_id])) {
					### Cannot Delete, some request is already approved
					$result[] = -1;
				} else {
					### Can be deleted
					$result[] = 1;
				}
			}
			if(!in_array(-1, $result)) {
				echo 1;
			}else{
				echo -1;
			}
		}
	break;
	
	case "CancelBookingRequest" :
		### Only can be cancel if the booking status = approved
	    $strBookingID = IntegerSafe($strBookingID);
		$arrBookingID = explode(",",$strBookingID);
		
		if(sizeof($arrBookingID) > 0) {
			for($i=0; $i<sizeof($arrBookingID); $i++) {
				$booking_id = $arrBookingID[$i];
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ROOM."'";
				$arrRoomResult = $li->returnVector($sql);
				
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ITEM."'";
				$arrFacilitiesResult = $li->returnVector($sql);
				
				
				if(sizeof($arrRoomResult) > 0) {
					if(($arrRoomResult[0] == 1)||($arrRoomResult[0] == -1)) {
						$pass[$booking_id]['RoomCheck'] = 1;
					} else {
						$pass[$booking_id]['RoomCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['RoomCheck'] = 1;
				}
					
				if(sizeof($arrFacilitiesResult) > 0) {
					if(($arrFacilitiesResult[0] == 1)||($arrFacilitiesResult[0] == -1)) {
						$pass[$booking_id]['FacilityCheck'] = 1;
					} else {
						$pass[$booking_id]['FacilityCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['FacilityCheck'] = 1;
				}
				
				if(in_array(0,$pass[$booking_id])) {
					### Cannot Delete, some request is already approved
					$result[] = -1;
				} else {
					### Can be deleted
					$result[] = 1;
				}
			}
			if(!in_array(-1, $result)) {
				echo 1;
			}else{
				echo -1;
			}
		}
	break;
	
	case "DeleteBookingRequest" :
		### Only can be edit if the booking status = pending / deleted
	    $strBookingID = IntegerSafe($strBookingID);
		$arrBookingID = explode(",",$strBookingID);
		
		if(sizeof($arrBookingID) > 0) {
			for($i=0; $i<sizeof($arrBookingID); $i++) {
				$booking_id = $arrBookingID[$i];
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ROOM."'";
				$arrRoomResult = $li->returnVector($sql);
				
				$sql = "SELECT BookingStatus FROM INTRANET_EBOOKING_BOOKING_DETAILS WHERE BookingID = '$booking_id' AND FacilityType = '".LIBEBOOKING_FACILITY_TYPE_ITEM."'";
				$arrFacilitiesResult = $li->returnVector($sql);
				
				
				if(sizeof($arrRoomResult) > 0) {
					if(($arrRoomResult[0] == 0) || ($arrRoomResult[0] == -1)) {
						$pass[$booking_id]['RoomCheck'] = 1;
					} else {
						$pass[$booking_id]['RoomCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['RoomCheck'] = 1;
				}
					
				if(sizeof($arrFacilitiesResult) > 0) {
					if(($arrFacilitiesResult[0] == 0)||($arrFacilitiesResult[0] == -1)) {
						$pass[$booking_id]['FacilityCheck'] = 1;
					} else {
						$pass[$booking_id]['FacilityCheck'] = 0;
					}
				} else {
					$pass[$booking_id]['FacilityCheck'] = 1;
				}
				
				if(in_array(0,$pass[$booking_id])) {
					### Cannot Delete, some request is already approved
					$result[] = -1;
				} else {
					### Can be deleted
					$result[] = 1;
				}
			}
			if(!in_array(-1, $result)) {
				echo 1;
			}else{
				echo -1;
			}
		}
	break;
	
	case "ShowRoomBookingRecordInDayView":
	    $LocationID = $_REQUEST['LocationID'];
	    if ($LocationID != 'ALL') {
	        $LocationID = IntegerSafe($LocationID);
	    }
	    echo $li->ShowRoomBookingRecordInDayView($_REQUEST['targetDate'],$LocationID,$_REQUEST['BookingStatus'],$_REQUEST['ManagementGroupID']);
	break;
	
	case "SaveDayViewSetting":
		echo $result = $li->SaveDayViewSetting($_REQUEST['StartTime'],$_REQUEST['EndTime'],$_REQUEST['TimeInterval']);
	break;
	
	case "ShowRoomBookingRecordInWeekView":
	    if ($LocationID != 'ALL') {
	        $LocationID = IntegerSafe($LocationID);
	    }
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
	    $BookingStatus = IntegerSafe($BookingStatus);
		$table_content = $li->ShowRoomBookingRecordInWeekView($WeekStartTimeStamp, $LocationID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	case "ShowRoomBookingRecordInMonthView":
		$Month = $_POST['targetMonth'];
		$Year = $_POST['targetYear'];
		if ($LocationID != 'ALL') {
		    $LocationID = IntegerSafe($LocationID);
		}
		$ManagementGroupID = IntegerSafe($ManagementGroupID);
		$table_content = $li->ShowRoomBookingRecordInMonthView($Month,$Year, $LocationID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	case "ChangeDisplayWeek":
		/*
		$tsNumOfDay = $ChangeValue*24*60*60;
		$newWeekStartTimeStamp = $WeekStartTimeStamp + $tsNumOfDay;
		
		$curr_date = date("Y-m-d", $newWeekStartTimeStamp);
		$curr_year = substr($curr_date,0,4);
		$curr_month = substr($curr_date,5,2);
		$curr_day = substr($curr_date,8,2);
		
		$StartOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday, $curr_year));
		$EndOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday+6, $curr_year));
		
		$StartYearOfTheWeek = substr($StartOfTheWeek,0,4);
		$StartMonthOfTheWeek = substr($StartOfTheWeek,5,2);
		$StartDayOfTheWeek = substr($StartOfTheWeek,8,2);
		
		$EndYearOfTheWeek = substr($EndOfTheWeek,0,4);
		$EndMonthOfTheWeek = substr($EndOfTheWeek,5,2);
		$EndDayOfTheWeek = substr($EndOfTheWeek,8,2);
		
		$DisplayStartOfTheWeek = $StartDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$StartMonthOfTheWeek].",".$StartYearOfTheWeek;
		$DisplayEndOfTheWeek = $EndDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$EndMonthOfTheWeek].",".$EndYearOfTheWeek;
		
		echo $newWeekStartTimeStamp.":::".$DisplayStartOfTheWeek." - ".$DisplayEndOfTheWeek;
		*/
		$returnValue = $li->ChangeDisplayWeek($WeekStartTimeStamp, $ChangeValue);
		echo $returnValue;
	break;
	
	case "ChangeDisplayWeekByCal":
		/*
		$selected_year = substr($WeekStartDate,0,4);
		$selected_month = substr($WeekStartDate,5,2);
		$selected_day = substr($WeekStartDate,8,2);
		$selected_weekday = date("w", mktime(0, 0, 0, $selected_month , $selected_day, $selected_year));
		
		$new_date = date("Y-m-d", mktime(0, 0, 0, $selected_month , $selected_day-$selected_weekday, $selected_year));
		$newWeekStartTimeStamp = strtotime($new_date);
		
		$curr_date = date("Y-m-d", $newWeekStartTimeStamp);
		$curr_year = substr($curr_date,0,4);
		$curr_month = substr($curr_date,5,2);
		$curr_day = substr($curr_date,8,2);
		
		$StartOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday, $curr_year));
		$EndOfTheWeek = date("Y-m-d",mktime(0,0,0,$curr_month , $curr_day-$curr_weekday+6, $curr_year));
		
		$StartYearOfTheWeek = substr($StartOfTheWeek,0,4);
		$StartMonthOfTheWeek = substr($StartOfTheWeek,5,2);
		$StartDayOfTheWeek = substr($StartOfTheWeek,8,2);
		
		$EndYearOfTheWeek = substr($EndOfTheWeek,0,4);
		$EndMonthOfTheWeek = substr($EndOfTheWeek,5,2);
		$EndDayOfTheWeek = substr($EndOfTheWeek,8,2);
		
		$DisplayStartOfTheWeek = $StartDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$StartMonthOfTheWeek].",".$StartYearOfTheWeek;
		$DisplayEndOfTheWeek = $EndDayOfTheWeek." ".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$EndMonthOfTheWeek].",".$EndYearOfTheWeek;
		
		echo $newWeekStartTimeStamp.":::".$DisplayStartOfTheWeek." - ".$DisplayEndOfTheWeek;
		*/
		$returnValue = $li->ChangeDisplayWeekByCal($WeekStartDate);
		echo $returnValue;
	break;
	
	case "ShowRoomBookingDetail":
		
////		$sql = "SELECT RelatedTo FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
////		$arrRelatedTo = $li->returnVector($sql);
////		$targetRelatedTo = implode(",",$arrRelatedTo);
////		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $targetRelatedTo";
////		$arrTargetBookingID = $li->returnVector($sql);
////		$targetBookingID = implode(",",$arrTargetBookingID);
//		$TargetBooingArr = $li->Get_Related_Booking_Record_By_BookingID($BookingID);
//		$targetBookingID = Get_Array_By_Key($TargetBooingArr, "BookingID");
//		
////		$sql = "SELECT 
////					DISTINCT booking_record.BookingID,
////					booking_record.Date, 
////					booking_record.StartTime, 
////					booking_record.EndTime, 
////					CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("request_ppl.").") AS RequestPerson,
////					IF(booking_record.RequestedBy <> booking_record.ResponsibleUID, CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("responsible_ppl.")."), ' - ') AS ResponsiblePerson,
////					IFNULL(CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("room_pic.")."),'".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System']."'),
////					room_details.BookingStatus,
////					booking_record.Remark
////				FROM
////					INTRANET_EBOOKING_RECORD AS booking_record
////					LEFT OUTER JOIN INTRANET_EBOOKING_ROOM_BOOKING_DETAILS AS room_details ON (booking_record.BookingID = room_details.BookingID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS AS item_details ON (booking_record.BookingID = item_details.BookingID)
////					LEFT OUTER JOIN INTRANET_USER AS request_ppl ON (booking_record.RequestedBy = request_ppl.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS responsible_ppl ON (booking_record.ResponsibleUID = responsible_ppl.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS room_pic ON (room_details.PIC = room_pic.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS item_pic ON (item_details.PIC = item_pic.UserID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM AS item_name ON (item_details.ItemID = item_name.ItemID)
////					LEFT OUTER JOIN INVENTORY_LOCATION AS location ON (room_details.RoomID = location.LocationID)
////					LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (location.LocationLevelID = floor.LocationLevelID)
////					LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (item_details.ItemID = item_group.ItemID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group1 ON (item_group.GroupID = mgmt_group1.GroupID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS room_group ON (room_details.RoomID = room_group.LocationID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group2 ON (room_group.GroupID = mgmt_group2.GroupID)
////				WHERE
////					booking_record.BookingID IN ($targetBookingID)";
////		$arrResult = $li->returnArray($sql,7);
//
//		$arrResult = $li->Get_All_Facility_Booking_Record($targetBookingID);
//		
//		$layer_content = "<table border='0' width='100%' cellpadding='3' cellspacing='0'>";
//		if(sizeof($arrResult)>0)
//		{
//			for($i=0; $i<sizeof($arrResult); $i++)
//			{
////				list($booking_id, $booking_date, $booking_start_time, $booking_end_time, $booking_request_by, $booking_responsible, $booking_PIC, $booking_status, $booking_remark) = $arrResult[$i];
//				list($booking_id,$period_id,$booking_remark,$booking_date,$booking_start_time,$booking_end_time,
//					$booking_request_by,$request_date,$booking_responsible,
//					$room_id,$room_name,$room_PICName,$room_booking_process_date,$booking_status,
//					$item_id,$item_name,$item_PICName,$item_booking_process_date,$item_booking_status,$is_reserve,
//					$room_check_in_check_out_remark,$item_check_in_check_out_remark, $room_PIC, $item_PIC
//				) = $arrResult[$i];
//
//				$booking_PIC = '';
//				if($room_PIC!='')
//					$booking_PIC = "<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">";
//				
//				$booking_PIC .= $room_PICName;
//				
//				$year = substr($booking_date,0,4);
//				$month = substr($booking_date,5,2);
//				$day = substr($booking_date,8,2);
//				$arrYear[] = $year;
//				$arrMonth[$year][] = $month;
//				if(sizeof($arrDay[$year][$month])>0)
//				{
//					if(in_array($day,$arrDay[$year][$month]))
//						continue;					
//					else
//						$arrDay[$year][$month][] = $day;						
//				}
//				else
//				{
//					$arrDay[$year][$month][] = $day;
//				} 
//			}
//		}
//		
//		### Show Date(s) ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Dates']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>";
//
//		$display_date = "<table border='0' csllpadding='0' cellspacing='0'>";		
//		foreach($arrDay as $year=>$arr1)
//		{
//			$display_date .= "<tr><td><u>".$year."</u></td></tr>";
//			foreach($arr1 as $month=>$arr2)
//			{
//				$display_date .= "<tr><td>".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$month]."</td>";
//				$display_date .= "<td>";
//				foreach($arr2 as $key=>$day)
//				{
//					if($key > 0)
//						$display_date .= ", ";
//					$display_date .= $day;
//				}
//				$display_date .= "</td></tr>";
//			}
//		}
//		$display_date .= "</table>";
//		$layer_content .= $display_date;
//		$layer_content .= "</td>";
//		$layer_content .= "</tr>";
//		### Show Time ###
//		$display_time = substr($booking_start_time,0,5)." - ".substr($booking_end_time,0,5);
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Time']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$display_time."</td>";
//		$layer_content .= "</tr>";
//		if(sizeof($arrResult) > 1){
//			$layer_content .= "<tr>";
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking']." : ".sizeof($arrResult)."</td>";
//			$layer_content .= "</tr>";
//		}
//		### Show Request By ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['BookedBy']." : <img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">".$booking_request_by."</td>";
//		$layer_content .= "</tr>";
//		### Show Responsible ###
//		$layer_content .= "<tr>";
//		if($booking_responsible!=$booking_request_by)
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : <img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">".$booking_responsible."</td>";
//		else
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : - </td>";
//		$layer_content .= "</tr>";
//		### Show PIC ###
//		$layer_content .= "<tr>";
//		if(($booking_status == 1) || ($booking_status == -1)) {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : ".$booking_PIC."</td>";
//		} else {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : - </td>";
//		}
//		$layer_content .= "</tr>";
//		### Show Booking Remark ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']." : ".$booking_remark."</td>";
//		$layer_content .= "</tr>";
//		
//		$layer_content .= "</table>";
        $BookingID = IntegerSafe($BookingID);
		$layer_content = $li->ShowRoomBookingDetail($BookingID);

		echo $layer_content;
	break;
	
	case "ChangeSelectedTab";
		if($ViewMode == "WeekView")
		{
			$tab_value = "<ul style='float: right'>
							<li id='current'><a href='#'><span>".$Lang['eBooking']['Settings']['FieldTitle']['WeekView']."</span></li>
							<li><a href='#' onClick='ChangeView(\"ListView\");'><span>".$Lang['eBooking']['Settings']['FieldTitle']['ListView']."</span></li>
						</ul>";
		}
		else if($ViewMode == "ListView")
		{
			$tab_value = "<ul style='float: right'>
							<li><a href='#' onClick='ChangeView(\"WeekView\");'><span>".$Lang['eBooking']['Settings']['FieldTitle']['WeekView']."</span></li>
							<li id='current'><a href='#'><span>".$Lang['eBooking']['Settings']['FieldTitle']['ListView']."</span></li>
						</ul>";
		}
		echo $tab_value;
	break;
	
	case "ChangeSelectedTab_Item";
		if($ViewMode == "WeekView")
		{
			$tab_value = "<ul style='float: right'>
							<li id='current'><a href='#'><span>".$Lang['eBooking']['Settings']['FieldTitle']['WeekView']."</span></li>
							<li><a href='#' onClick='ChangeView(\"ListView\");'><span>".$Lang['eBooking']['Settings']['FieldTitle']['ListView']."</span></li>
						</ul>";
		}
		else if($ViewMode == "ListView")
		{
			$tab_value = "<ul style='float: right'>
							<li><a href='#' onClick='ChangeView(\"WeekView\");'><span>".$Lang['eBooking']['Settings']['FieldTitle']['WeekView']."</span></li>
							<li id='current'><a href='#'><span>".$Lang['eBooking']['Settings']['FieldTitle']['ListView']."</span></li>
						</ul>";
		}
		echo $tab_value;
	break;
	
	case "ShowRoomBookingRecordInListView":
	    if ($LocationID != 'ALL') {
	        $LocationID = IntegerSafe($LocationID);
	    }
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
	    $BookingStatus = IntegerSafe($BookingStatus);
	    $BookingDateType = cleanHtmlJavascript($BookingDateType);
	    $Keyword = cleanHtmlJavascript($Keyword);
		$table_content = $li->ShowRoomBookingRecordInListView($LocationID, $ManagementGroupID, $BookingStatus, $BookingDateType, $pageNo, $order, $numPerPage, $Keyword);
		echo $table_content ;
	break;
	
	case "ShowRoomBookingRemark":
	    $BookingID = IntegerSafe($BookingID);
		$table_content = $li->showBookingRemark($BookingID);
		echo $table_content; 
	break;
	
	case "ShowItemBookingRecordInWeekView":
	    $CategoryID = IntegerSafe($CategoryID);
	    $SubCategoryID = IntegerSafe($SubCategoryID);
	    $ItemID = IntegerSafe($ItemID);
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
		if(($CategoryID != "") && ($SubCategoryID != "") && ($ItemID == "")) {
			$ItemID = "ALL";
		}
		$table_content = $li->ShowItemBookingRecordInWeekView($WeekStartTimeStamp, $CategoryID, $SubCategoryID, $ItemID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	case "ShowItemBookingRecordInMonthView":
	    $ItemID = IntegerSafe($ItemID);
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
		if(($CategoryID != "") && ($SubCategoryID != "") && ($ItemID == "")) {
			$ItemID = "ALL";
		}
		$Month = $_POST['targetMonth'];
		$Year = $_POST['targetYear'];
		$table_content = $li->ShowItemBookingRecordInMonthView($Month,$Year, $CategoryID, $SubCategoryID, $ItemID, $ManagementGroupID, $BookingStatus);
		echo $table_content ;
	break;
	
	case "ShowItemBookingDetail":
		
////		$sql = "SELECT RelatedTo FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
////		$arrRelatedTo = $li->returnVector($sql);
////		$targetRelatedTo = implode(",",$arrRelatedTo);
////		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $targetRelatedTo";
////		$arrTargetBookingID = $li->returnVector($sql);
////		$targetBookingID = implode(",",$arrTargetBookingID);
//		$TargetBooingArr = $li->Get_Related_Booking_Record_By_BookingID($BookingID);
//		$targetBookingID = Get_Array_By_Key($TargetBooingArr, "BookingID");
//		
////		$sql = "SELECT 
////					DISTINCT booking_record.BookingID,
////					booking_record.Date, 
////					booking_record.StartTime, 
////					booking_record.EndTime, 
////					CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("request_ppl.").") AS RequestPerson,
////					IF(booking_record.RequestedBy <> booking_record.ResponsibleUID, CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("responsible_ppl.")."), ' - ') AS ResponsiblePerson,
////					IFNULL(CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("item_pic.")."),'".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System']."'),
////					item_details.BookingStatus,
////					booking_record.Remark
////				FROM
////					INTRANET_EBOOKING_RECORD AS booking_record
////					LEFT OUTER JOIN INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS AS item_details ON (booking_record.BookingID = item_details.BookingID)
////					LEFT OUTER JOIN INTRANET_USER AS request_ppl ON (booking_record.RequestedBy = request_ppl.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS responsible_ppl ON (booking_record.ResponsibleUID = responsible_ppl.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS item_pic ON (item_details.PIC = item_pic.UserID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM AS item_name ON (item_details.ItemID = item_name.ItemID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (item_details.ItemID = item_group.ItemID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group1 ON (item_group.GroupID = mgmt_group1.GroupID)
////				WHERE
////					booking_record.BookingID IN ($targetBookingID)";
////		$arrResult = $li->returnArray($sql,7);
//		$arrResult = $li->Get_Facility_Booking_Record(LIBEBOOKING_FACILITY_TYPE_ITEM, '', '', '', '', $targetBookingID, $WithName=1, $WithUserName=1);
//		$layer_content = "<table border='0' width='100%' cellpadding='3' cellspacing='0'>";
//		if(sizeof($arrResult)>0)
//		{
//			for($i=0; $i<sizeof($arrResult); $i++)
//			{
////				list($booking_id, $booking_date, $booking_start_time, $booking_end_time, $booking_request_by, $booking_responsible, $booking_PIC, $booking_status, $booking_remark) = $arrResult[$i];
//				 $booking_id = $arrResult[$i]['BookingID'];
//				 $item_id = $arrResult[$i]['FacilityID'];
//				 $booking_date = $arrResult[$i]['Date'];
//				 $booking_start_time = $arrResult[$i]['StartTime'];
//				 $booking_end_time = $arrResult[$i]['EndTime'];
//				 $booking_related_to = $arrResult[$i]['RelatedTo'];
//				 $booking_remark = $arrResult[$i]['Remark'];
//				 $booking_request_by = $arrResult[$i]['RequestedByUserName'];
//				 $booking_request_date = $arrResult[$i]['RequestDate'];
//				 $booking_responsible = $arrResult[$i]['ReponsibleUserName'];
//				 $booking_pic = $arrResult[$i]['PICUserName'];
//				 $booking_process_date = $arrResult[$i]['ProcessDate'];
//				 $booking_status = $arrResult[$i]['BookingStatus'];
//				 $item_name = $arrResult[$i]['FacilityName'];	
//				 $PIC = $arrResult[$i]['PIC'];
//				 
//				$year = substr($booking_date,0,4);
//				$month = substr($booking_date,5,2);
//				$day = substr($booking_date,8,2);
//				$arrYear[] = $year;
//				$arrMonth[$year][] = $month;
//				if(sizeof($arrDay[$year][$month])>0)
//				{
//					if(in_array($day,$arrDay[$year][$month]))
//						continue;					
//					else
//						$arrDay[$year][$month][] = $day;						
//				}
//				else
//				{
//					$arrDay[$year][$month][] = $day;
//				}
//			}
//		}
//		
//		### Show Date(s) ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Dates']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>";
//
//		$display_date = "<table border='0' csllpadding='0' cellspacing='0'>";		
//		foreach($arrDay as $year=>$arr1)
//		{
//			$display_date .= "<tr><td><u>".$year."</u></td></tr>";
//			foreach($arr1 as $month=>$arr2)
//			{
//				$display_date .= "<tr><td>".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$month]."</td>";
//				$display_date .= "<td>";
//				foreach($arr2 as $key=>$day)
//				{
//					if($key > 0)
//						$display_date .= ", ";
//					$display_date .= $day;
//				}
//				$display_date .= "</td></tr>";
//			}
//		}
//		$display_date .= "</table>";
//		$layer_content .= $display_date;
//		$layer_content .= "</td>";
//		$layer_content .= "</tr>";
//		### Show Time ###
//		$display_time = substr($booking_start_time,0,5)." - ".substr($booking_end_time,0,5);
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Time']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$display_time."</td>";
//		$layer_content .= "</tr>";
//		if(sizeof($arrResult) > 1){
//			$layer_content .= "<tr>";
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking']." : ".sizeof($arrResult)."</td>";
//			$layer_content .= "</tr>";
//		}
//		### Show Request By ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['BookedBy']." : <img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">".$booking_request_by."</td>";
//		$layer_content .= "</tr>";
//		### Show Responsible ###
//		$layer_content .= "<tr>";
//		if($booking_responsible!=$booking_request_by)
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : <img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">".$booking_responsible."</td>";
//		else
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : - </td>";
//		$layer_content .= "</tr>";
//		### Show PIC ###
//		$layer_content .= "<tr>";
//		if(($booking_status == 1) || ($booking_status == -1)) {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : ".$booking_pic."</td>";
//		} else {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : - </td>";
//		}
//		$layer_content .= "</tr>";
//		### Show Booking Remark ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>Remark : ".$booking_remark."</td>";
//		$layer_content .= "</tr>";
//		
//		$layer_content .= "</table>";
        $BookingID = IntegerSafe($BookingID);
		$layer_content = $li->ShowItemBookingDetail($BookingID);
		echo $layer_content;
	break;
	
	case "ShowItemBookingRecordInListView":
	    $CategoryID = IntegerSafe($CategoryID);
	    $SubCategoryID = IntegerSafe($SubCategoryID);
	    $ItemID = IntegerSafe($ItemID);
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
		if(($CategoryID != "") && ($SubCategoryID != "") && ($ItemID == "")) {
			$ItemID = "ALL";
		}
		$table_content = $li->ShowItemBookingRecordInListView($CategoryID, $SubCategoryID, $ItemID, $ManagementGroupID, $BookingStatus, $BookingDateType, $pageNo, $order, $numPerPage, $Keyword);
		echo $table_content ;		
	break;
	
	case "ShowFollowUpWorkInWeekView":
	    $FollowUpGroup = IntegerSafe($FollowUpGroup);
		$table_content .= $li->ShowFollowUpWorkInWeekView($WeekStartTimeStamp,$FollowUpGroup,$RemarkSelection);
		echo $table_content; 
	break;
	
	case "ShowFollowUpDetail":
////		$sql = "SELECT RelatedTo FROM INTRANET_EBOOKING_RECORD WHERE BookingID = $BookingID";
////		$arrRelatedTo = $li->returnVector($sql);
////		$targetRelatedTo = implode(",",$arrRelatedTo);
////		$sql = "SELECT BookingID FROM INTRANET_EBOOKING_RECORD WHERE RelatedTo = $targetRelatedTo";
////		$arrTargetBookingID = $li->returnVector($sql);
////		$targetBookingID = implode(",",$arrTargetBookingID);
//		$TargetBooingArr = $li->Get_Related_Booking_Record_By_BookingID($BookingID);
//		$targetBookingID = Get_Array_By_Key($TargetBooingArr, "BookingID");
//		
////		$sql = "SELECT 
////					DISTINCT booking_record.BookingID,
////					booking_record.Date, 
////					booking_record.StartTime, 
////					booking_record.EndTime, 
////					CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("request_ppl.").") AS RequestPerson,
////					IF(booking_record.RequestedBy <> booking_record.ResponsibleUID, CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("responsible_ppl.")."), ' - ') AS ResponsiblePerson,
////					IFNULL(CONCAT('<img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">',".getNameFieldByLang("room_pic.")."),'".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System']."'),
////					room_details.BookingStatus
////				FROM
////					INTRANET_EBOOKING_RECORD AS booking_record
////					LEFT OUTER JOIN INTRANET_EBOOKING_ROOM_BOOKING_DETAILS AS room_details ON (booking_record.BookingID = room_details.BookingID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS AS item_details ON (booking_record.BookingID = item_details.BookingID)
////					LEFT OUTER JOIN INTRANET_USER AS request_ppl ON (booking_record.RequestedBy = request_ppl.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS responsible_ppl ON (booking_record.ResponsibleUID = responsible_ppl.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS room_pic ON (room_details.PIC = room_pic.UserID)
////					LEFT OUTER JOIN INTRANET_USER AS item_pic ON (item_details.PIC = item_pic.UserID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM AS item_name ON (item_details.ItemID = item_name.ItemID)
////					LEFT OUTER JOIN INVENTORY_LOCATION AS location ON (room_details.RoomID = location.LocationID)
////					LEFT OUTER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (location.LocationLevelID = floor.LocationLevelID)
////					LEFT OUTER JOIN INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP AS item_group ON (item_details.ItemID = item_group.ItemID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group1 ON (item_group.GroupID = mgmt_group1.GroupID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION AS room_group ON (room_details.RoomID = room_group.LocationID)
////					LEFT OUTER JOIN INTRANET_EBOOKING_MANAGEMENT_GROUP AS mgmt_group2 ON (room_group.GroupID = mgmt_group2.GroupID)
////				WHERE
////					booking_record.BookingID IN ($targetBookingID)";
////		$arrResult = $li->returnArray($sql,8);
//		$arrResult = $li->Get_All_Facility_Booking_Record($targetBookingID);
//		
//		$layer_content = "<table border='0' width='100%' cellpadding='3' cellspacing='0'>";
//		if(sizeof($arrResult)>0)
//		{
//			for($i=0; $i<sizeof($arrResult); $i++)
//			{
////				list($booking_id, $booking_date, $booking_start_time, $booking_end_time, $booking_request_by, $booking_responsible, $booking_PIC, $booking_status) = $arrResult[$i];
//				list($booking_id,$period_id,$booking_remark,$booking_date,$booking_start_time,$booking_end_time,
//					$booking_request_by,$request_date,$booking_responsible,
//					$room_id,$room_name,$room_PICName,$room_booking_process_date,$booking_status,
//					$item_id,$item_name,$item_PICName,$item_booking_process_date,$item_booking_status,$is_reserve,
//					$room_check_in_check_out_remark,$item_check_in_check_out_remark, $room_PIC, $item_PIC
//				) = $arrResult[$i];
//
//				$year = substr($booking_date,0,4);
//				$month = substr($booking_date,5,2);
//				$day = substr($booking_date,8,2);
//				$arrYear[] = $year;
//				$arrMonth[$year][] = $month;
//				if(sizeof($arrDay[$year][$month])>0)
//				{
//					if(in_array($day,$arrDay[$year][$month]))
//						continue;					
//					else
//						$arrDay[$year][$month][] = $day;						
//				}
//				else
//				{
//					$arrDay[$year][$month][] = $day;
//				} 
//			}
//		}
//		
//		### Show Date(s) ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Dates']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>";
//
//		$display_date = "<table border='0' csllpadding='0' cellspacing='0'>";		
//		foreach($arrDay as $year=>$arr1)
//		{
//			$display_date .= "<tr><td><u>".$year."</u></td></tr>";
//			foreach($arr1 as $month=>$arr2)
//			{
//				$display_date .= "<tr><td>".$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'][(int)$month]."</td>";
//				$display_date .= "<td>";
//				foreach($arr2 as $key=>$day)
//				{
//					if($key > 0)
//						$display_date .= ", ";
//					$display_date .= $day;
//				}
//				$display_date .= "</td></tr>";
//			}
//		}
//		$display_date .= "</table>";
//		$layer_content .= $display_date;
//		$layer_content .= "</td>";
//		$layer_content .= "</tr>";
//		### Show Time ###
//		$display_time = substr($booking_start_time,0,5)." - ".substr($booking_end_time,0,5);
//		$layer_content .= "<tr>";
//		$layer_content .= "<td><em><u>".$Lang['eBooking']['Management']['FieldTitle']['Time']."</u></em></td>";
//		$layer_content .= "</tr>";
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$display_time."</td>";
//		$layer_content .= "</tr>";
//		if(sizeof($arrResult) > 1){
//			$layer_content .= "<tr>";
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking']." : ".sizeof($arrResult)."</td>";
//			$layer_content .= "</tr>";
//		}
//		### Show Request By ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['BookedBy']." : <img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\"> ".$booking_request_by."</td>";
//		$layer_content .= "</tr>";
//		### Show Responsible ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['Responsible']." : <img height=\"20\" width=\"20\" align=\"absmiddle\" src=\"{$image_path}/{$LAYOUT_SKIN}/iMail/icon_ppl_ex.gif\">".$booking_responsible."</td>";
//		$layer_content .= "</tr>";
//		/*
//		### Show PIC ###
//		$layer_content .= "<tr>";
//		if(($booking_status == 1) || ($booking_status == -1)) {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : ".$booking_PIC."</td>";
//		} else {
//			$layer_content .= "<td>".$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy']." : - </td>";
//		}
//		$layer_content .= "</tr>";
//		### Show Booking Remark ###
//		$layer_content .= "<tr>";
//		$layer_content .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark']." : ".$booking_remark."</td>";
//		$layer_content .= "</tr>";
//		*/
//		$layer_content .= "</table>";
        $BookingID = IntegerSafe($BookingID);
		$layer_content = $li->ShowFollowUpDetail($BookingID);
		echo $layer_content;
	break;
	
	case "ShowDescription":
		if($FacilityType == 1){
		    $FacilityID = IntegerSafe($FacilityID);
			$sql = "SELECT Description FROM INVENTORY_LOCATION WHERE LocationID = '$FacilityID'";
		}
		else if($FacilityType == 2){
		    $FacilityID = IntegerSafe($FacilityID);
			$descField = Get_Lang_Selection('DescriptionChi', 'DescriptionEng');
			$sql = "SELECT $descField FROM INTRANET_EBOOKING_ITEM WHERE ItemID = '$FacilityID'";
		}
			$arrDesc = $li->returnVector($sql);
						
			$tableContent .= '<table border="0" border="0" cellpadding="3" cellspacing="0" width="300px">';
			$tableContent .= '<tr class="tabletop"><td>'.$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Description'].'</td></tr>';
			$tableContent .= '<tr class="tablerow1"><td>'.($arrDesc[0]? nl2br($arrDesc[0]) : $Lang['General']['EmptySymbol']).'</td></tr>';
			$tableContent .= '</table>';
			
			
			### Hide Layer Button
			$layerContent .= '<table cellspacing="0" cellpadding="3" border="0">'."\n";
				$layerContent .= '<tbody>'."\n";
					$layerContent .= '<tr>'."\n";
						$layerContent .= '<td align="right" style="border-bottom: medium none;">'."\n";
							$layerContent .= '<a href="javascript:js_Hide_Information_Layer(\'DIV_DisplayInformation\')"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
						$layerContent .= '</td>'."\n";
					$layerContent .= '</tr>'."\n";
					$layerContent .= '<tr>'."\n";
						$layerContent .= '<td align="left" style="border-bottom: medium none;">'."\n";
							$layerContent .= '<div id="DetailsLayerContentDiv">'.$tableContent.'</div>'."\n";
						$layerContent .= '</td>'."\n";
					$layerContent .= '</tr>'."\n";
				$layerContent .= '</tbody>'."\n";
			$layerContent .= '</table>'."\n";			
		
		echo $layerContent;
	break;
	
	case "ShowTimezone":
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			$lfcm = new form_class_manage();
			$lcycleperiods = new libcycleperiods();
			
			$academicYearAry = $lfcm->Get_Academic_Year_List($AcademicYearIDArr='', $OrderBySequence=0, $excludeYearIDArr=array(), $NoPastYear=1);
			$academicYearIdAry = Get_Array_By_Key($academicYearAry, 'AcademicYearID'); 
			
			$timezoneAry = $lcycleperiods->Get_All_TimeZone_By_Academic_Year($academicYearIdAry);
			$numOfTimezone = count($timezoneAry);
						
			$tableContent .= '<table border="0" border="0" cellpadding="3" cellspacing="0" width="350px">';
				$tableContent .= '<tr class="tabletop"><td style="width:16%;">#</td><td style="width:42%;">'.$Lang['General']['StartDate'].'</td><td style="width:42%;">'.$Lang['General']['EndDate'].'</td></tr>';
				for ($i=0; $i<$numOfTimezone; $i++) {
					$_startDate = $timezoneAry[$i]['PeriodStart'];
					$_endDate = $timezoneAry[$i]['PeriodEnd'];
					
					$tableContent .= '<tr class="tablerow1"><td>'.($i+1).'</td><td>'.$_startDate.'</td><td>'.$_endDate.'</td></tr>';
				}
			$tableContent .= '</table>';
			
			
			### Hide Layer Button
			$layerContent .= '<table cellspacing="0" cellpadding="3" border="0">'."\n";
				$layerContent .= '<tbody>'."\n";
					$layerContent .= '<tr>'."\n";
						$layerContent .= '<td align="right" style="border-bottom: medium none;">'."\n";
							$layerContent .= '<a href="javascript:js_Hide_Information_Layer(\'DIV_DisplayInformation\')"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
						$layerContent .= '</td>'."\n";
					$layerContent .= '</tr>'."\n";
					$layerContent .= '<tr>'."\n";
						$layerContent .= '<td align="left" style="border-bottom: medium none;">'."\n";
							$layerContent .= '<div id="DetailsLayerContentDiv">'.$tableContent.'</div>'."\n";
						$layerContent .= '</td>'."\n";
					$layerContent .= '</tr>'."\n";
				$layerContent .= '</tbody>'."\n";
			$layerContent .= '</table>'."\n";			
		
		echo $layerContent;
	break;
	
	case "GetRoomAttachment":
		$layer_content = "";
		if($FacilityType==1){
		    $FacilityID = IntegerSafe($FacilityID);
			if($sys_custom['eBooking_ReserveMultipleRooms']) {
				$sql = "SELECT LocationID,Attachment FROM INVENTORY_LOCATION WHERE LocationID IN ($FacilityID) AND Attachment IS NOT NULL AND Attachment !=''";
				$attachment_arr = $li->returnArray($sql);
				
				for($i=0;$i<count($attachment_arr);$i++) {
					if(trim($attachment_arr[$i]['Attachment'])!=''){
						$encryptedPath = getEncryptedText($intranet_root."/file/ebooking/room/r".$attachment_arr[$i]['LocationID']."/".$attachment_arr[$i]['Attachment']);
						$attachment_link = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$encryptedPath;
						$layer_content .= "<br><a href=\"".$attachment_link."\" target=\"_blank\"><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif' border='0' align=\"absmiddle\" valign=\"2\">".$attachment_arr[$i]['Attachment']."</a>";
					}
				}
			}else{
				$sql = "SELECT Attachment FROM INVENTORY_LOCATION WHERE LocationID = '$FacilityID' ";
				$attachment_arr = $li->returnVector($sql);
				
				if(sizeof($attachment_arr)>0 && trim($attachment_arr[0])!=''){
					//$attachment_link = $PATH_WRT_ROOT."home/download_attachment.php?target=".$intranet_root."/file/ebooking/room/r".$FacilityID."/".urlencode($attachment_arr[0]);
					$encryptedPath = getEncryptedText($intranet_root."/file/ebooking/room/r".$FacilityID."/".$attachment_arr[0]);
					$attachment_link = $PATH_WRT_ROOT."home/download_attachment.php?target_e=".$encryptedPath;
					$layer_content = "<a href=\"".$attachment_link."\" target=\"_blank\"><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_attachment.gif' border='0' align=\"absmiddle\" valign=\"2\">".$attachment_arr[0]."</a>";
				}
			}
		}
		echo $layer_content;
	break;
	
	case "ShowBookingDetail":
	    $BookingID = IntegerSafe($BookingID);
		$BookingIDArr = explode(",",$BookingID);
		$Layer = $li->Get_Booking_Detail_Layer($BookingIDArr, $thisTimeslotID);
		echo $Layer;
	break;
	
	case "Get_User_Bookable_Day":
	    $FacilityType = IntegerSafe($FacilityType);
	    $FacilityID = IntegerSafe($FacilityID);
	    $UserID = IntegerSafe($UserID);
	    $From_eAdmin = IntegerSafe($From_eAdmin);
		list($SinceDate,$UntilDate) = $li->getFacilityBookingAvailableDateRange($FacilityType, $FacilityID, $UserID, $From_eAdmin);
		echo $SinceDate."|=|".$UntilDate;
	break;
	
	case "ValidateImport":
		
		include_once($PATH_WRT_ROOT.'includes/libimporttext.php');
		include_once($PATH_WRT_ROOT.'includes/libuser.php');
		include_once($PATH_WRT_ROOT.'includes/libebooking_ui.php');
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		$linterface = new interface_html();
		$limport = new libimporttext();
		$luser = new libuser($_SESSION['UserID']);
		//$lebooking = new libebooking();
		$lebooking = new libebooking_ui();
		$from_eAdmin = $_SESSION['EBOOKING_BOOKING_FROM_EADMIN'];
		
		//### Error Message
		$ErrorSettingArr[0] = 'LocationNotFound'; //0
		$ErrorSettingArr[1] = 'DateFormatInvalid'; //1
		$ErrorSettingArr[2] = 'StartEndTimeNotCorrect'; //2
		$ErrorSettingArr[3] = 'OccupiedUserNotFound'; //3
		$ErrorSettingArr[4] = 'NotInBookableTimePeriod'; //4
		$ErrorSettingArr[5] = 'TimeClash'; //5
		$ErrorSettingArr[6] = 'SelfTimeClash'; //6
		$ErrorSettingArr[7] = 'RoomNotBookable'; //7
		$ErrorSettingArr[8] = 'WrongTimeFormat'; //8
		$ErrorSettingArr[9] = '???';
		
		//### Include js libraries
		$jsInclude = '';
		$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
		$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
		$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
		echo $jsInclude;
		
		//### Get Data from the csv file
		$columnTitleAry = $lebooking->getImportHeader();
		$columnPropertyAry = $lebooking->getImportColumnProperty();
		$csvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($targetFilePath, '', '', $columnTitleAry, $columnPropertyAry);
		$csvColName = array_shift($csvData);
		$numOfData = count($csvData);
		$data = $csvData;
		
		//### 1st loop - data preparation
		foreach($data as $_rowNum => $_rowArr){
			$_BuildingCode = trim($_rowArr[0]); // column A
			$_LocationLevelCode= trim($_rowArr[1]); // column B
			$_LocationCode= trim($_rowArr[2]); // column C
			$_BookingDate = trim($_rowArr[3]); // column D
			$_StartTime = trim($_rowArr[4]); // column E
			$_EndTime = trim($_rowArr[5]); // column F
			//## make sure booking date time have the same format
			if(preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $_BookingDate)){
				//# yyyy-mm-dd
				$_BookingDate = date("Y-m-d", strtotime($_BookingDate));
			}
			else if(preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $_BookingDate)){
				//# dd/mm/yyyy
				//# convert into yyyy-mm-dd
				$_BookingDate = str_replace('/','-',$_BookingDate);
				$_BookingDate = date("Y-m-d", strtotime($_BookingDate));
			}
			if(preg_match("/^[0-9]{1}:[0-9]{2}$/", $_StartTime)){
				$_StartTime = '0'.$_StartTime;
			}
			if(preg_match("/^[0-9]{1}:[0-9]{2}$/", $_EndTime)){
				$_EndTime = '0'.$_EndTime;
			}
			$importTimePeriodArr[$_BuildingCode][$_LocationLevelCode][$_LocationCode][$_BookingDate][$_rowNum][] = $_StartTime.' - '.$_EndTime;
			
			$_OccupiedBy = trim($_rowArr[7]); // column H
			$userLoginCheckingArr[] =  trim($_OccupiedBy);
			
		}
		//## Prepare for Userlogin checking
		$userLoginCheckingArr = array_unique($userLoginCheckingArr);
		$userLoginArr = $luser->getUserInfoByUserLogin($userLoginCheckingArr);
		$userLoginAssoArr = BuildMultiKeyAssoc($userLoginArr, 'UserLogin');
		
		//##Prepare for LocationName info
		$LocationCodeAssoAry = $lebooking->getExistingLocationAssoAry(array('BuildingCode','LocationLevelCode','RoomCode'));	
		
		//## Prepare data check interval
		$settingTimeInterval = ($sys_custom['eBooking_booking_time_interval']!='')? $sys_custom['eBooking_booking_time_interval'] : 5;
		$numOfSection = 60 / $settingTimeInterval;
		$x = 0;
		for($i = 0 ; $i<$numOfSection ;$i++){
			if($x>=60){
				break;
			}
			if($x<10){
				$ValidMinuteArray[] = '0'.$x;	
			}
			else{
				$ValidMinuteArray[] = $x;
			}
			$x = $x + $settingTimeInterval;
		}	
		
		//### 2nd loop - data validation
		$errorCount = 0;
		$successCount = 0;
		$rowErrorRemarksAry = array();
		$insertTempDataAry = array();
		
		foreach($data as $_rowNum => $_rowArr){
			$_BuildingCode = trim($_rowArr[0]); // column A
			$_LocationLevelCode= trim($_rowArr[1]); // column B
			$_LocationCode= trim($_rowArr[2]); // column C
			$_BookingDate = trim($_rowArr[3]); // column D
			$_StartTime = trim($_rowArr[4]); // column E
			$_EndTime = trim($_rowArr[5]); // column F
			$_Remarks= trim($_rowArr[6]); // column G
			$_OccupiedBy = trim($_rowArr[7]); // column H
		
			//## Check Whether Location is Existing
			$LocationExist = $lebooking->validateLocation($_BuildingCode,$_LocationLevelCode,$_LocationCode,true); // if true reture LocationID 
			$TargetLocationID = $LocationExist;
			if(!$LocationExist){
				//$ErrorArr[$_rowNum][] = 'LocationNotFound';
				$ErrorArr[$_rowNum][] = $ErrorSettingArr[0];
			}
			else{
				$DisplayLocationNameArr[$_rowNum] = $lebooking->getLocationNameByCode($_BuildingCode,$_LocationLevelCode,$_LocationCode);
			}
			
			//## Check Location is bookable
			$_isLocationBookable = $lebooking->isLocationBookable($TargetLocationID);
			if(!$_isLocationBookable){
				$ErrorArr[$_rowNum][] = $ErrorSettingArr[7];
			}
			
			//## Date Validation
			if(preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $_BookingDate)){
				//# yyyy-mm-dd
				$_BookingDate = date("Y-m-d", strtotime($_BookingDate));
			}
			else if(preg_match("/^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/", $_BookingDate)){
				//# dd/mm/yyyy convert into yyyy-mm-dd
				$_BookingDate = str_replace('/','-',$_BookingDate);
				$_BookingDate = date("Y-m-d", strtotime($_BookingDate));
			}
			else{
				$ErrorArr[$_rowNum][] = $ErrorSettingArr[1];
			}
			if($_BookingDate =='1970-01-01'){
				$ErrorArr[$_rowNum][] = $ErrorSettingArr[1];
			}
			$_DisplayBookingDateArr[$_rowNum] = $_BookingDate;
		
			//## Check Validation of Start End Time
			//# check time format
			if(preg_match("/^[0-9]{1}:[0-9]{2}$/", $_StartTime)){
				$_StartTime = '0'.$_StartTime;
			}
			if(preg_match("/^[0-9]{1}:[0-9]{2}$/", $_EndTime)){
				$_EndTime = '0'.$_EndTime;
			}
			if( !preg_match("/^[0-9]{1,2}:[0-9]{2}$/", $_StartTime) || !preg_match("/^[0-9]{1,2}:[0-9]{2}$/", $_EndTime)){
				
				$ErrorArr[$_rowNum][] = $ErrorSettingArr[8];
			}
			//# check time range valid
			if( strtotime($_EndTime) < strtotime($_StartTime) || strtotime($_EndTime) == strtotime($_StartTime) ){
				//$ErrorArr[$_rowNum][] = 'StartEndTimeNotCorrect';
				$ErrorArr[$_rowNum][] = $ErrorSettingArr[2];
			}
			//# check time interval valid
			if(!empty($_StartTime)&&!empty($_EndTime)){
				$_StartTimeArr = explode(':',$_StartTime);
				$_EndTimeArr = explode(':',$_EndTime);
				if(!in_array($_StartTimeArr[1],$ValidMinuteArray) || !in_array($_EndTimeArr[1],$ValidMinuteArray)){
					$ErrorArr[$_rowNum][] = $ErrorSettingArr[9];
				}
			}
			
			//## Check Whether Occupied User is Existing
			if($_OccupiedBy != ''){
				$OccupiedByUserID = $userLoginAssoArr[$_OccupiedBy]['UserID'];
				
				if(empty($OccupiedByUserID)){
					//$ErrorArr[$_rowNum][] = 'OccupiedUserNotFound';
					$ErrorArr[$_rowNum][] = $ErrorSettingArr[3];
				}
				else{
					$DisplayOccupiedUserArr[$_rowNum] = $userLoginAssoArr[$_OccupiedBy]['Name'];	
				}
			}
			else{
				$OccupiedByUserID = $_SESSION['UserID'];
				$DisplayOccupiedUserArr[$_rowNum] = $luser->StandardDisplayName;
			}
			
			//## Check Time clash between the time of newly import records
			$sameBookingDateLocationArr = $importTimePeriodArr[$_BuildingCode][$_LocationLevelCode][$_LocationCode][$_BookingDate];
			foreach((array)$sameBookingDateLocationArr as $_ROW => $_sameLocationImportPeroidArr){
		
				if($_ROW == $_rowNum){
					continue;
				}
				else{
					$isTimeClashInSameFile = $lebooking->Is_Time_Clash($_StartTime,$_EndTime, $_sameLocationImportPeroidArr);
					if($isTimeClashInSameFile){
						$ErrorArr[$_rowNum][$ErrorSettingArr[6]][] = $_ROW;
						$ErrorArr[$_ROW][$ErrorSettingArr[6]][] = $_rowNum;
					}
				}
			}
				
			//## No Error start checking bookable time period
			if(empty($ErrorArr[$_rowNum])){
				$FacilityType = 1; //Room
				$availableTimeslotArr = $lebooking->Get_Facility_Available_Booking_Period_Of_User($FacilityType, (array)$TargetLocationID, $_SESSION['UserID'], '', $_BookingDate, $_BookingDate,1);
				$RoomAvailablePeriodAssocAry = $lebooking->Union_Time_Range_Of_Date_Assoc($availableTimeslotArr);
				$isInTimePeriod = $lebooking->In_Time_Period($_StartTime,$_EndTime, $RoomAvailablePeriodAssocAry[$_BookingDate]);
		
				if(!$isInTimePeriod){
					//$ErrorArr[$_rowNum][] = 'NotInBookableTimePeriod';
					$ErrorArr[$_rowNum][] = $ErrorSettingArr[4];
				}
			}
			
			//## No Error start checking time clash with existing booking
			if(empty($ErrorArr[$_rowNum])){
				//# Get Approved Booking Record of the same date
				$FacilityType = 1; //Room
				$BookingRecordArr = $lebooking->Get_Facility_Booking_Record($FacilityType,(array)$TargetLocationID, $_BookingDate, $_BookingDate, LIBEBOOKING_BOOKING_STATUS_APPROVED, '', 0, 1);
				
				$_DateBookingArr = array();
				foreach((array)$BookingRecordArr as $thisBookingRecord){
					$_DateBookingArr[$thisBookingRecord['Date']][$thisBookingRecord['BookingID']] = array($lebooking->Format_Display_Time_Period($thisBookingRecord));
				}
				########
				$TargetDayBookingArr = $_DateBookingArr[$_BookingDate];
				$isTimeClash = false;
				foreach((array)$TargetDayBookingArr as $thisBookingID => $thisBookingRecordPeriod){
					$isTimeClash = $lebooking->Is_Time_Clash($_StartTime,$_EndTime, $thisBookingRecordPeriod);
					if($isTimeClash){
						if($from_eAdmin){
							$overWriteBookingRecord[] = $thisBookingID;		
						}
						else{
							$ErrorArr[$_rowNum][] = $ErrorSettingArr[5];		
						}
						break;
					}
				}
				########
			}
			
			//## for insert to temp table
			//# convert hh:mm into hh:mm:ss
			$TempStartTime = $_StartTime.':00';
			$TempEndTime =  $_EndTime.':00';
			$TempValueArr[] = '("'.$_SESSION['UserID'].'","'.$_rowNum.'","'.$TargetLocationID.'","'.$_BookingDate.'","'.$TempStartTime.'","'.$TempEndTime.'","'.$OccupiedByUserID.'","'.$_Remarks.'", now())';
			
			//## Update Processing Display
			Flush_Screen_Display(1);
			$thisJSUpdate = '';
			$thisJSUpdate .= '<script language="javascript">'."\n";
				$thisJSUpdate .= '$("span#BlockUISpan", window.parent.document).html("'.($_rowNum + 1).'");';
			$thisJSUpdate .= '</script>'."\n";
			echo $thisJSUpdate;
		}
		
		//### 3rd loop - print error rows
		if(count($ErrorArr)>0){
		ksort($ErrorArr);
			foreach((array)$ErrorArr as $_row => $_ErrorArr){
				$col1_css = '';
				$col2_css = '';
				$col3_css = '';
				$col4_css = '';
				$DisplayError = '';
				$rowNumDisplayArr = array();
				foreach((array)$_ErrorArr as $__Error){
					if(!is_array($__Error)){
											
						//## Red the columns that have error
						switch ($__Error) {
						    case $ErrorSettingArr[0]:
						    $col1_css = 'red';
						    $DisplayError .= $Lang['eBooking']['Import']['WarningMessage']['LocationNotFound'].'<br>'; 
						        break;
						    case $ErrorSettingArr[1]:
						    $col2_css = 'red';
						    $DisplayError .= $Lang['General']['InvalidDateFormat'].'<br>';
						        break;
						    case $ErrorSettingArr[2]:
						    $col3_css = 'red';
						    $DisplayError .= $Lang['eBooking']['Import']['WarningMessage']['ReserveTimeInvalid'].'<br>';
						        break;
						    case $ErrorSettingArr[3]:
						    $col4_css = 'red';
						    $DisplayError .= $Lang['eBooking']['Import']['WarningMessage']['UserNotFound'].'<br>';
						        break;
						    case $ErrorSettingArr[4]:
						    $col2_css = 'red'; 
						    $col3_css = 'red';
						    $DisplayError .= $Lang['eBooking']['Import']['WarningMessage']['NotInBookableTimePeriod'].'<br>';
						        break;
						    case $ErrorSettingArr[5]:
						    $col2_css = 'red'; 
						    $col3_css = 'red';
						    $DisplayError .= $Lang['eBooking']['Import']['WarningMessage']['TimeClash'].'<br>';
						        break;
						    case $ErrorSettingArr[7]:
						    $col1_css = 'red';
						    $DisplayError .= $Lang['eBooking']['Import']['WarningMessage']['RoomNotBookable'].'<br>';
						    	break;
						    case $ErrorSettingArr[8]:
						    $col3_css = 'red';
						    $DisplayError .= $Lang['eBooking']['Import']['WarningMessage']['ReserveTimeFormatInvalid'].'<br>';
						    	break;
						    case $ErrorSettingArr[9]:
						    $col3_css = 'red';
						    $DisplayError .= str_replace('<!!-TimeInterval-!!>',$settingTimeInterval,$Lang['eBooking']['Import']['WarningMessage']['TimeInterval']).'<br>';
						    	break;
						    default:
						    //do noth
						    	break;
						}
					}
					else{
						//## Time clash between the time of newly import records
						$clashRowArr = $__Error;
						$rowClashedWith = array_unique($clashRowArr);
						
						$col2_css = 'red'; 
						$col3_css = 'red';
						
						foreach((array)$rowClashedWith as $row){
							$rowNumDisplayArr[] = 	$row + 3;
						}
						
						$DisplayError .= str_replace('<!!-RowNumber-!!>',implode(',',$rowNumDisplayArr),$Lang['eBooking']['Import']['WarningMessage']['SelfTimeClash']).'<br>';
					}
					
				}
				
				$_BuildingCode = $data[$_row][0]; // column A
				$_LocationLevelCode= $data[$_row][1]; // column B
				$_LocationCode= $data[$_row][2]; // column C
				$_BookingDate = $data[$_row][3]; // column D
				$_StartTime = $data[$_row][4]; // column E
				$_EndTime = $data[$_row][5]; // column F
				$_Remarks= $data[$_row][6]; // column G
				$_OccupiedBy = $data[$_row][7]; // column H
				$_DisplayBookingDate = $_DisplayBookingDateArr[$_row]; // BookingDate after conversion
				$_DisplayUserName = $DisplayOccupiedUserArr[$_row];
				$_DisplayLocationName = $DisplayLocationNameArr[$_row];
				
				if($_DisplayUserName == ''){
					$DisplayOccupiedBy = $_OccupiedBy ;	
				}
				else{
					$DisplayOccupiedBy = $_DisplayUserName;
				}
			
				if($_DisplayLocationName == ''){
					$DisplayLocation = intranet_htmlspecialchars($_BuildingCode.' > '.$_LocationLevelCode.' > '.$_LocationCode);
				}else{
					$DisplayLocation = intranet_htmlspecialchars($_DisplayLocationName);
				}
				$displayRowNumber = $_row + 3;
				$confirmTable .= '<tr '.$rowClass.'>';
				$confirmTable .= '<td class="tabletext">'.$displayRowNumber.'</td>';
				$confirmTable .= '<td class="tabletext '.$col1_css.'">'.$DisplayLocation.'</td>';
				$confirmTable .= '<td class="tabletext '.$col2_css.'">'.$_DisplayBookingDate.'</td>';
				$confirmTable .= '<td class="tabletext '.$col3_css.'">'.$_StartTime.'</td>';
				$confirmTable .= '<td class="tabletext '.$col3_css.'">'.$_EndTime.'</td>';
				$confirmTable .= '<td class="tabletext ">'.$_Remarks.'</td>';
				$confirmTable .= '<td class="tabletext '.$col4_css.'">'.$DisplayOccupiedBy.'</td>';
				$confirmTable .= '<td class="tabletext">'.$DisplayError.'</td>';
				$confirmTable .= '</tr>';
			}
			
			$x = '<table class="common_table_list_v30 view_table_list_v30" width="90%" border="0" cellspacing="0" cellpadding="5" align="center">';
			$x .='<tr>';
			$x .='<th width="1">'.$Lang['General']['ImportArr']['Row'].'</th>';
			$x .='<th width="20%">'.$Lang['eBooking']['General']['FieldTitle']['Location'].'</th>';
			$x .='<th width="10%">'.$Lang['eBooking']['General']['Export']['FieldTitle']['Date'].'</th>';
			$x .='<th width="10%">'.$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['StartTime'].'</th>';
			$x .='<th width="10%">'.$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['EndTime'].'</th>';
			$x .='<th width="15%">'.$Lang['General']['Remark'].'</th>';
			$x .='<th width="10%">'.$Lang['eBooking']['OccupiedBy'].'</th>';
			$x .='<th>'.$Lang['General']['Error'].'</th>';
			$x .='</tr>';
			$x .= $confirmTable;
			$x .='</table>';
			$htmlAry['errorTbl'] = $x;
		}
		else{
			//## Over write
			if(count($overWriteBookingRecord)>0 && $from_eAdmin){
				$overWriteBookingRecord = array_unique($overWriteBookingRecord);
				
				$overWriteBookingInfoArr = $lebooking->getBookingRecordDetails($overWriteBookingRecord);
				$overWriteUserIDArr = Get_Array_By_Key($overWriteBookingInfoArr,'ResponsibleUID');
				$overWriteBookingInfoArr = BuildMultiKeyAssoc($overWriteBookingInfoArr, 'BookingID');
				
				$x = '';
				$x .= '<table class="common_table_list" width="100%"><tbody>';
				$x .= '<tr>';
				$x .= '<th>#</th>';
				$x .= '<th>'.$Lang['General']['Date'].'</th>';
				$x .= '<th>'.$Lang['eBooking']['Period'].'</th>';
				$x .= '<th>'.$Lang['eBooking']['General']['FieldTitle']['RoomOrItem'].'</th>';
				$x .= '<th>'.$Lang['eBooking']['BookingUser'].'</th>';
				$x .= '<th>'.$Lang['eBooking']['BookingStatus'].'</th>';
				$x .= '</tr>';
				$libuser = new libuser('','',$overWriteUserIDArr);
				$count = 0;
				foreach((array)$overWriteBookingRecord as $overWriteBookingID){
					$count++;
					$_ResponsibleUID = $overWriteBookingInfoArr[$overWriteBookingID]['ResponsibleUID'];
					$libuser->LoadUserData($_ResponsibleUID);
					$x .= '';
					$x .= '<tr>';
					$x .= '<td>'.$count.'</td>';
					$x .= '<td>'.$overWriteBookingInfoArr[$overWriteBookingID]['Date'].'</td>';
					$x .= '<td>'.$overWriteBookingInfoArr[$overWriteBookingID]['StartTime'].'-'.$overWriteBookingInfoArr[$overWriteBookingID]['EndTime'].'</td>';
					$x .= '<td>'.$lebooking->getLocationNameByID($overWriteBookingInfoArr[$overWriteBookingID]['FacilityID']).'</td>';
					$x .= '<td>'.$libuser->StandardDisplayName.'</td>';
					$x .= '<td>'.$overWriteBookingInfoArr[$overWriteBookingID]['BookingStatus'].'</td>';
					$x .= '</tr>';
				}
				$x .= '</tbody></table>';
				$x = '<div>'. $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['eBooking']['Admin']['ReserveBookingAdminOverwriteWarning'].'</font>', $x).'</div>';
				$htmlAry['errorTbl'] = $x.'';
				$htmlAry['errorTbl'] = str_replace("'","\'",$htmlAry['errorTbl']);
			}
			else{
				//## no error
				$htmlAry['errorTbl'] = '';
			}
		}
				
		$numData = count($data);
		$numError = count($ErrorArr);
		$numOK = $numData - $numError;
		if($numError == 0 && $numOK > 0){
			$lebooking->insertImportTempData($TempValueArr);
		}
		
		$thisJSUpdate = '';
		$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ErrorTableDiv").innerHTML = \''.$htmlAry['errorTbl'].'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("SuccessCountDiv").innerHTML = \''.$numOK.'\';';
		$thisJSUpdate .= 'window.parent.document.getElementById("FailCountDiv").innerHTML = \''.$numError.'\';';
		
		if ($numError == 0) {
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).removeClass("formbutton_disable").addClass("formbutton");';
			$thisJSUpdate .= '$("input#ImportBtn", window.parent.document).attr("disabled","");';
		}
		
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
		$thisJSUpdate .= '</script>'."\n";
		echo $thisJSUpdate;
		
		if(!empty($overWriteBookingRecord)){
			$thisJSUpdate = '';
			$thisJSUpdate .= '<script language="javascript">'."\n";
			$thisJSUpdate .= 'parent.needOverWriteRecord = 1;'."\n";
			$thisJSUpdate .= '</script>'."\n";
			echo $thisJSUpdate;
		}
		
	break;

}

intranet_closedb();
die;
?>