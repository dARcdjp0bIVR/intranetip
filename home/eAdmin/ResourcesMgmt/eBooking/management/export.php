<?php
// using : 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");
$arrCookies[] = array("ck_eBooking_BookingStatus", "BookingStatus");
$arrCookies[] = array("ck_eBooking_BookingDate", "BookingDate");
$arrCookies[] = array("ck_eBooking_ManagementGroup", "ManagementGroup");
$arrCookies[] = array("ck_eBooking_My_Booking_Record_Keyword", "keyword");	

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);
	
$lexport = new libexporttext();
$lebooking = new libebooking();
$lebooking->Check_Page_Permission($AdminOnly=0);

$BookingStatus 	= (isset($BookingStatus) && $BookingStatus != "") ? $BookingStatus : -999;
$BookingDate 	= (isset($BookingDate) && $BookingDate != "") ? $BookingDate : 1;
$keyword		= (isset($keyword) && $keyword != "") ? $keyword : "";
$field=trim($field)!=''?$field:1;
$order=trim($order)!=''?$order:1;

if($BookingDate==3)
{
	if(!$StartDate || !$EndDate )
	{
		$AcademicYearID = Get_Current_Academic_Year_ID();
		list($YearStartDate,$YearEndDate) = getPeriodOfAcademicYear($AcademicYearID);
		if(!$StartDate) $StartDate = date("Y-m-d",strtotime($YearStartDate));
		if(!$EndDate) $EndDate = date("Y-m-d",strtotime($YearEndDate));
	}
}

if($BookingDate != "") {
	$curr_date = date("Y-m-d");
	if($BookingDate == 3){
	}else if($BookingDate == 1){
		$StartDate = $curr_date;
		$EndDate = '';
	}else{
		$StartDate = '';
		$EndDate = $curr_date;
	}
}

if($keyword != "") {
	$keyword = addslashes(trim($keyword));
}

$arrManagemntGroup = $lebooking->Get_User_Related_Mgmt_Group_Array($UserID);
if($lebooking->IS_ADMIN_USER()) {
	// do nth
} else {
	if($ManagementGroup == "")
		$ManagementGroup = $arrManagemntGroup[0][0];
}


$exportHeaderAry = $lebooking->Get_Booking_Record_Export_Header();
$exportDataAry = $lebooking->Get_Booking_Record_Export_Content($keyword, $FacilityType, $BookingStatus, '', $StartDate, $EndDate, $ManagementGroup, $TargetUserID=-1);
$export_content = $lexport->GET_EXPORT_TXT($exportDataAry, $exportHeaderAry, "", "\r\n", "", 0, "11", $includeLineBreak=1);


intranet_closedb();
$filename = "booking_application.csv";
$lexport->EXPORT_FILE($filename, $export_content);
?>