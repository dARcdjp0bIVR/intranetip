<?php
// using : 
/*
 *  2019-05-03 Cameron
 *      - fix: add parameter $Keyword to Show_Item_Booking_List_Print()
 *      
 *  2019-05-01 Cameron
 *      - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$CurrentPageArr['eBooking'] = 1;
$isPrint = 1;
$include_JS_CSS = $lebooking_ui->initJavaScript();

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$CategoryID = IntegerSafe($CategoryID);
$SubCategoryID = IntegerSafe($SubCategoryID);

$sql = "SELECT CONCAT(".$linventory->getInventoryNameByLang("cat.").",' > ',".$linventory->getInventoryNameByLang("subcat.").") FROM INVENTORY_CATEGORY as cat INNER JOIN INVENTORY_CATEGORY_LEVEL2 as subcat ON (cat.CategoryID = subcat.CategoryID) WHERE cat.CategoryID = '$CategoryID' AND subcat.Category2ID = '$SubCategoryID'";
$arrCategory = $lebooking_ui->returnVector($sql);
if(sizeof($arrCategory) > 0){
	$category_info = '<table border="0">';
	$category_info .= '<tr><td>'.$Lang['eBooking']['Settings']['FieldTitle']['Category'].' : </td>';
	$category_info .= '<td>'.$arrCategory[0].'</td></tr>';
	$category_info .= '</table>';
}

$MgmtGroup_info = '<table border="0">';
$MgmtGroup_info .= '<tr><td>'.$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'].' : </td>';
if($ManagementGroupID == "")
{
	$MgmtGroup_info .= '<td><span>'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup'].'</span></td></tr>';
} else {
	if($ManagementGroupID != "-999"){
	    $ManagementGroupID = IntegerSafe($ManagementGroupID);
		$sql = "SELECT GroupName FROM INTRANET_EBOOKING_MANAGEMENT_GROUP WHERE GroupID = '$ManagementGroupID'";
		$arrMgmtGroupInfo = $lebooking_ui->returnVector($sql);
		$MgmtGroup_info .= '<td><span>'.$arrMgmtGroupInfo[0].'</span></td></tr>';
	}else{
		$MgmtGroup_info .= '<td><span>'.$Lang['eBooking']['Settings']['FieldTitle']['NoMgmtGroup'].'</span></td></tr>';
	}
}
$MgmtGroup_info .= "</table>";

$BookingStatus = IntegerSafe($BookingStatus);
$BookingStatusArr = explode(",",$BookingStatus); 
$show_wait = in_array(0,$BookingStatusArr)?"1":"0";
$show_approve = in_array(1,$BookingStatusArr)?"1":"0";
$show_reject = in_array(-1,$BookingStatusArr)?"1":"0";
$show_tempory = in_array(999,$BookingStatusArr)?"1":"0";
$booking_status_content = '<table border="0">';
$booking_status_content .= '<tr><td>'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'].' : </td>';
if($show_wait){
	$booking_status_content .= '<td><span class="tablelink" style="border:1px solid; padding:1px;" >'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'].'</td>';
}
if($show_approve){
	$booking_status_content .= '<td><span  style="border:1px solid; padding:1px; color: rgb(0, 153, 0);">'.$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'].'</td>';
}
$booking_status_content .= '</tr>';
$booking_status_content .= '</table>';

$DateType_info = '<table border="0">';
$DateType_info .= '<tr><td>'.$Lang['eBooking']['Management']['RoomBooking']['FieldTitle']['RequestDate'].' : </td>';

$BookingDateType = cleanHtmlJavascript($BookingDateType);
switch ($BookingDateType)
{
	case "Future":
		$DateType_info .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllComingRequest']."</td></tr>";
	break;
	
	case "ThisWeek":
		$DateType_info .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisWeek']."</td></tr>";
	break;
	
	case "NextWeek":
		$DateType_info .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextWeek']."</td></tr>";
	break;
	
	case "ThisMonth":
		$DateType_info .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ThisMonth']."</td></tr>";
	break;
	
	case "NextMonth":
		$DateType_info .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['NextMonth']."</td></tr>";
	break;
	
	case "Past":
		$DateType_info .= "<td>".$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['PastRequest']."</td></tr>";
	break;
}
$DateType_info .= '</table>';

$addition_info = $category_info.$MgmtGroup_info.$booking_status_content.$DateType_info;

if($ItemID == ""){
	$ItemID = "ALL";
}
else {
    $ItemID = IntegerSafe($ItemID);
}
$Keyword = cleanHtmlJavascript(rawurldecode($_GET['Keyword']));
$print_content = $lebooking_ui->Show_Item_Booking_List_Print($CategoryID,$SubCategoryID,$ItemID,$ManagementGroupID,$BookingStatus,$BookingDateType,$pageNo = "", $order = "", $numPerPage = "",$Keyword);
?>

<?=$include_JS_CSS;?>

<br>

<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print">
			<?=$Lang['eBooking']['Management']['ItemBooking']['FieldTitle']['ReportTitle']['ItemBookingList'];?>
		</td>
	</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?=$addition_info;?>
		</td>
	</tr>
</table>

<br>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td>
			<?=$print_content;?>
		</td>
	</tr>
</table>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>