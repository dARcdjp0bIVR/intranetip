<?php
// using : 
/*
 * 2019-05-03 Cameron
 *      - fix: resume to original action after export in js_Export_ItemBooking_Week()
 *
 *  2019-05-01 Cameron
 *      - fix potential sql injection problem by cast parameter to integer
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_ItemBooking";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['ItemBooking']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

if( isset($BookStatus) && (sizeof($BookStatus)>0) )
{
	$BookingStatusList = implode(",", $BookStatus);
}
else
{
	//$BookingStatusList = isset($BookingStatusList)? $BookingStatusList : '1,0';
	if(isset($_GET['BookingStatus'])) {
		$BookingStatusList = $_GET['BookingStatus']; 
	} else {
		$BookingStatusList = isset($BookingStatusList)? $BookingStatusList : '1,0';
	}
}
$BookingStatusList = IntegerSafe($BookingStatusList);

if($_POST['CategoryID'] != "") {
	$CategoryID = $_POST['CategoryID'];
} else {
	if(isset($_GET['CategoryID'])) {
		$CategoryID = $_GET['CategoryID'];
	} else {
		$CategoryID = "";
	}
}	 
$CategoryID = IntegerSafe($CategoryID);

if($_POST['SubCategoryID'] != "") {
	$SubCategoryID = $_POST['SubCategoryID'];
} else {
	if(isset($_GET['SubCategoryID'])) {
		$SubCategoryID = $_GET['SubCategoryID'];
	} else {
		$SubCategoryID = "";
	}
}
$SubCategoryID = IntegerSafe($SubCategoryID);

if(isset($_GET['ItemID'])) {
	$FacilityID = $_GET['ItemID'];
}
$FacilityID = IntegerSafe($FacilityID);

if(isset($_GET['ManagementGroupID'])) {
	$ManagementGroup = $_GET['ManagementGroupID']; 
}
$ManagementGroup = IntegerSafe($ManagementGroup);

echo $lebooking_ui->Get_Item_Booking_UI($CategoryID, $SubCategoryID, $FacilityID, $ManagementGroup, $BookingStatusList, $WeekStartTimeStamp);
echo $lebooking_ui->initJavaScript();
?>

<script language='JavaScript'>
	var CurrentView = "";
	
	$(document).ready( function() {
		CurrentView = "WeekView";
		
		$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar', mandatory: true});
		$('#WeekStartDate').datepick({
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0
		});
		
		jsChangeItem();
	});
	
	function jsChangeItem()
	{
		var task = "ShowItemBookingRecordInWeekView";
		Block_Document();
		
		$.post(
			"ajax_task.php",
			{
				task : task,
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				CategoryID : $("#CategoryID").val(),
				SubCategoryID : $("#SubCategoryID").val(),
				ItemID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				$("#DIV_RoomBooking").html(responseData);
				$("#DIV_RoomBooking").show();
				UnBlock_Document();
				initThickBox();
			}
		);
	}
		
	function UpdateStatusList()
	{
		var StatusListAry = new Array();
		var check_box_checked = 0;
	
		$("input.StatusCheckBox:checked").each(function(){
			StatusListAry.push($(this).val());
			check_box_checked++;
		});
	
		$("#BookingStatusList").val(StatusListAry.join(","));
		
		if(check_box_checked == 0){
			alert("<?=$Lang['eBooking']['Management']['General']['JSWarning']['PleaseSelectStatus'];?>");
		}else{
			refreshBookRecord();
		}
	}
	
	function refreshBookRecord()
	{
		MM_showHideLayers('status_option','','hide');
		jsChangeItem();
	}
	
	function jsChangeWeek()
	{
		$.post(
			"ajax_task.php",
			{
				task : "ChangeDisplayWeekByCal",
				WeekStartDate : $("#WeekStartDate").val(),
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsChangeItem();
			}
		);
	}
	
	function NextWeek()
	{
		$.post(
			"ajax_task.php",
			{
				task : "ChangeDisplayWeek",
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				ChangeValue : 7,
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsChangeItem();
			}
		);
	}
	
	function PrevWeek()
	{
		$.post(
			"ajax_task.php",
			{
				task : "ChangeDisplayWeek",
				WeekStartTimeStamp : $("#WeekStartTimeStamp").val(),
				ChangeValue : -7,
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				var ReturnVal = responseData.split(":::");
				
				$("#WeekStartTimeStamp").val(ReturnVal[0]);
				$("#DIV_WeeklyDisplay").html(ReturnVal[1]);
				jsChangeItem();
			}
		);
	}
	
	function ChangeView(val)
	{
		if(val == "WeekView")
		{
			CurrentView = "WeekView";
			$("div#caltabs_right").load(
				"ajax_task.php",
				{
					task : "ChangeSelectedTab_Item",
					ViewMode : "WeekView"
				},
				function(returnString)
				{
					jsChangeItem();
				}
			);
		}
		else if(val == "ListView")
		{
			var CategoryID = $("#CategoryID").val();
			var SubCategoryID = $("#SubCategoryID").val();
			var ItemID = $("#FacilityID").val();
			var ManagementGroupID = $("#ManagementGroup").val();
			var BookingStatus = $("#BookingStatusList").val();
			var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
			
			var param = '?CategoryID=' + CategoryID + '&SubCategoryID=' + SubCategoryID + '&ItemID=' + ItemID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus + '&WeekStartTimeStamp=' + WeekStartTimeStamp;  
			window.location = "item_booking_list.php" + param;
		}
	}
	
	function js_Show_Booking_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID)
	{
		js_Hide_Booking_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'ItemBookingDetail')
			jsAction = 'ShowItemBookingDetail';
		
		$('div#BookingDetailContentDiv').html('<?=$Lang['General']['Loading']?>');	
		js_Change_Layer_Position(jsClickedObjID);
		MM_showHideLayers('BookingDetailLayer','','show');
				
		$('div#BookingDetailContentDiv').load(
			"ajax_task.php", 
			{ 
				task: jsAction,
				BookingID: jsBookingID
			},
			function(returnString)
			{
				$('div#BookingDetailContentDiv').css('z-index', '999');
			}
		);
	}
	
	function js_Hide_Booking_Detail_Layer()
	{
		MM_showHideLayers('BookingDetailLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID) 
	{
		var jsOffsetLeft, jsOffsetTop;
		
		jsOffsetLeft = 165;
		jsOffsetTop = -50;
			
		var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
				
		document.getElementById('BookingDetailLayer').style.left = posleft + "px";
		document.getElementById('BookingDetailLayer').style.top = postop + "px";
		document.getElementById('BookingDetailLayer').style.visibility = 'visible';
	}
	
	function js_Print_ItemBooking_Week()
	{
		var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
		var CategoryID = $("#CategoryID").val();
		var SubCategoryID = $("#SubCategoryID").val();
		var ItemID = $("#FacilityID").val();
		var ManagementGroupID = $("#ManagementGroup").val();
		var BookingStatus = $("#BookingStatusList").val();

		if((CategoryID == "")||(SubCategoryID == ""))
		{
			alert("<?=$Lang['eBooking']['Management']['FieldTitle']['SelectItem'];?>");
		}
		else
		{
			var param = '?WeekStartTimeStamp=' + WeekStartTimeStamp + '&CategoryID=' + CategoryID + '&SubCategoryID=' + SubCategoryID + '&ItemID=' + ItemID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus;
			var url = "item_booking_week_print.php" + param;
			newWindow(url, 10);
		}
	}
	
	function js_Export_ItemBooking_Week()
	{
		var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
		var CategoryID = $("#CategoryID").val();
		var SubCategoryID = $("#SubCategoryID").val();
		var ItemID = $("#FacilityID").val();
		var ManagementGroupID = $("#ManagementGroup").val();
		var BookingStatus = $("#BookingStatusList").val();

		if((CategoryID == "")||(SubCategoryID == ""))
		{
			alert("<?=$Lang['eBooking']['Management']['FieldTitle']['SelectItem'];?>");
		}
		else
		{
			//var param = '?WeekStartTimeStamp=' + WeekStartTimeStamp + '&CategoryID=' + CategoryID + '&SubCategoryID=' + SubCategoryID + '&ItemID=' + ItemID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus;
			//var url = "item_booking_week_export.php" + param;
			var url = "item_booking_week_export.php";
			var originalAction = document.form1.action;
			document.form1.action = url;
			document.form1.submit();
			document.form1.action = originalAction;
		}
	}
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>