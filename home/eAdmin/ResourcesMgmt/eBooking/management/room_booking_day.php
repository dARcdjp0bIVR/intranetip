<?php
// using : 
############################ Change Log ###########################
##
##  Date    : 2019-05-01 (Cameron)
##              fix potential sql injection problem or cross site scripting by cast related variables to integer
##
##	Date	: 2015-04-21 (Omas)
##			Add export
##
##	Date	: 2015-01-23 (Omas)
##			Create this page
##	
###################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_RoomBooking";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

if( isset($BookStatus) && (sizeof($BookStatus)>0) )
{
	$BookingStatusList = implode(",", $BookStatus);
}
else
{
	if(isset($_GET['BookingStatus'])){
		$BookingStatusList = $_GET['BookingStatus']; 
	}else{
		$BookingStatusList = isset($BookingStatusList)? $BookingStatusList : '1,0';
	}
}
$BookingStatusList = IntegerSafe($BookingStatusList);

$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['RoomBooking']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

$LocationID = IntegerSafe($LocationID);
$ManagementGroup = IntegerSafe($ManagementGroup);

?>

<?=$lebooking_ui->Get_Room_Booking_DayView_UI($LocationID, $ManagementGroup, $BookingStatusList, $WeekStartTimeStamp);?>
<?=$lebooking_ui->initJavaScript();?>

<style type="text/css">

	
	.bookingStatus_1 {
	    color: #4db308;
	    border-top: 1px solid #4db308;
	    border-bottom: 1px solid #4db308;
	    border-right: 1px solid #4db308;
	    border-left: 1px solid #4db308;
	}
	
	.bookingStatus_0 {
	    color: #2f75e9;
	    border-top: 1px solid #2f75e9;
	    border-bottom: 1px solid #2f75e9;
	    border-right: 1px solid #2f75e9;
	    border-left: 1px solid #2f75e9;
	}
	
	.bookingStatus_-1 {
	    color: #2f75e9;
	    border-top: 1px solid #2f75e9;
	    border-bottom: 1px solid #2f75e9;
	    border-right: 1px solid #2f75e9;
	    border-left: 1px solid #2f75e9;
	}
	
</style>
<?//$linterface->Include_SuperTable_JS_CSS()?>
<!--script type="text/javascript" src= "<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" type="text/css" media="screen" /-->
<script language='JavaScript'>
var currentDate = new Date();
var currentDD = currentDate.getDate();
var currentMM = currentDate.getMonth();
var currentYYYY = currentDate.getFullYear();

$(document).ready( function() {
	getTargetDate();
			$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar', mandatory: true});
		$('#TargetDate').datepick({
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0
		});
});

function jsChangeDateByArrow(change){
	currentDate.setDate(currentDate.getDate() + change);
	currentDD = currentDate.getDate();
	currentMM = currentDate.getMonth();
	currentYYYY = currentDate.getFullYear();
	getTargetDate();
}

function jsChangeDateByCal(){
	var PickedDate = $("#TargetDate").val();
	
	currentDate = new Date(PickedDate);
	currentDD = currentDate.getDate();
	currentMM = currentDate.getMonth();
	currentYYYY = currentDate.getFullYear();
	
	$('span#SPAN_CurrentDate').html(PickedDate);
	$('input#TargetDate').val(PickedDate);
	LoadingDayView(PickedDate);
}

function getTargetDate(){
	
	var targetDate = '';
	var dd = currentDD;
	var mm = currentMM + 1; //January is 0
	var yyyy = currentYYYY;
	
	if(dd<10) {
	    dd='0'+dd
	} 
	
	if(mm<10) {
	    mm='0'+mm
	}
	
	targetDate = yyyy + '-' + mm + '-' + dd;
	$('span#SPAN_CurrentDate').html(targetDate);
	$('input#TargetDate').val(targetDate);
	LoadingDayView(targetDate);
}

function LoadingDayView(targetDate)
{
	Block_Element('BlockTable');
		
	var jsAction = '';
		jsAction = 'ShowRoomBookingRecordInDayView';

		$.post(
		"ajax_task.php", 
		{ 
			task: jsAction,
			targetDate: targetDate,
			BookingStatus: $("#BookingStatusList").val(),
			LocationID: $("#LocationID").val(),
			ManagementGroupID : $("#ManagementGroup").val()
		},
		function(ReturnData)
		{
			UnBlock_Element('BlockTable');
			
			$('div#DIV_DailyDisplay').html(ReturnData);
			
			
			$('.bookingRecord')
				  .css('cursor', 'pointer')
				  .click(
				    function(){
				    	var objID = $(this).attr('id');
				    	var jsBookingRecordID = objID.replace('bookingRecord_','');
				     
				     MM_showHideLayers('BookingDetailLayer', '', 'show');
				     changeLayerPosition($(this).attr('id'), 'BookingDetailLayer', 0, 20);
				     ajaxLoadBookingDetails(jsBookingRecordID);
				    }
				  )
				  .hover(
				  	
						function(){
						$(this).css('color', '#FFFFFF');
						  if($(this).attr('bookingstatus')==1){
					      	$(this).css('background-color', '#4db308');
					      }
					      else{
					      	$(this).css('background-color', '#2f75e9');
					      }
					    },
					    function(){
					      $(this).css('background-color', '#FFFFFF');
					      if($(this).attr('bookingstatus')==1){
					      	$(this).css('color', '#4db308');
					      }
					      else{
					      	$(this).css('color', '#2f75e9');
					      }
					    }    
   
				  );
			initThickBox();
		}
	)		
}

function UpdateStatusList()
{
	var StatusListAry = new Array();
	var check_box_checked = 0;

	$("input.StatusCheckBox:checked").each(function(){
		StatusListAry.push($(this).val());
		check_box_checked++;
	});

	$("#BookingStatusList").val(StatusListAry.join(","));
	
	if(check_box_checked == 0){
		alert("<?=$Lang['eBooking']['Management']['General']['JSWarning']['PleaseSelectStatus'];?>");
	}else{
		MM_showHideLayers('status_option','','hide');
		getTargetDate();
	}
}

function ajaxLoadBookingDetails(jsBookingRecordID){
	
	var jsAction = 'ShowRoomBookingDetail';
	var jsBookingID = jsBookingRecordID;

	$('div#BookingDetailLoadingDiv').css('display','block');
	$('div#BookingDetailContentDiv').html('');
	
	$.post(
		"ajax_task.php", 
		{ 
			task: jsAction,
			BookingID: jsBookingID
		},
		function(ReturnData)
		{
			$('div#BookingDetailLoadingDiv').css('display','none');
			$('div#BookingDetailContentDiv').html(ReturnData);

		}
	);
}

function js_Hide_Booking_Detail_Layer()
{
	MM_showHideLayers('BookingDetailLayer','','hide');
}

function js_Show_Thick_Box()
{
	$('a#thickboxButton').click();
}

function js_Print_RoomBooking_Day()
{
	var targetDate = $('#SPAN_CurrentDate').html();
	var BookingStatus= $("#BookingStatusList").val();
	var LocationID= $("#LocationID").val();
	var ManagementGroupID = $("#ManagementGroup").val();
			
	var param = '?targetDate=' + targetDate + '&LocationID=' + LocationID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus;
	var url = "room_booking_day_print.php" + param;
	newWindow(url, 10);
}

function js_Export_RoomBooking_Day(){
	
	var url = "room_booking_day_export.php";
	
	document.form1.action = url;
	document.form1.submit();	
}

function jsSaveSetting(){
	var StartTime = $('#StartHour').val() + ':' + $('#StartMin').val() + ':00';
	var EndTime = $('#EndHour').val() + ':' + $('#EndMin').val() + ':00';
	var TimeInterval = $('#timeInterval').val();  
	var jsAction = 'SaveDayViewSetting';
	js_Hide_ThickBox();
	$.post(
		"ajax_task.php", 
		{ 
			task: jsAction,
			StartTime: StartTime,
			EndTime: EndTime,
			TimeInterval: TimeInterval
			
		},
		function(ReturnData)
		{
			getTargetDate();
		}
	);
}

function checkTime(){
	var StartHour = $('#StartHour').val();
	var EndHour = $('#EndHour').val();
	var StartMin = $('#StartMin').val();
	var EndMin = $('#EndMin').val();
	var TimeInterval = $('#timeInterval').val(); 
	var HourDiff = 0;
	var MinDiff = 0;
	
	HourDiff = EndHour - StartHour;
	MinDiff = EndMin - StartMin;
	
	var valid = 1;
	
	if(StartHour > EndHour){
		valid = 0;
	}
	else if(StartHour == EndHour && StartMin > EndMin){
		valid = 0;
	}
	
	if(TimeInterval == 0 ){
		valid = 0;
	}
	
	if(HourDiff ==0 && MinDiff == 0){
		valid = 0;
	}
	else if(HourDiff <0){
		valid = 0;
	}
	
	if(valid > 0){
		jsSaveSetting();
	}
	else{
		alert('Please Select Time Range Correctly');
		return false;
	}
	
}
</script>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
