<?php
################## Change Log [Start] #################
#
#	Date	:	2015-05-12	Omas
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$limport = new libimporttext();
$lo = new libfilesystem();

### Get the Temp Data To be Insert
$importDataAry = $lebooking->getImportTempData();
$numOfData = count($importDataAry);
foreach($importDataAry as $importRow){
	$date = $importRow['TargetDate'];
	$startTime = $importRow['StartTime'];
	$endTime = $importRow['EndTime'];
	
	$DateTimeArr[$date][0] = array( 'StartTime' => $startTime, 'EndTime' => $endTime);
	
	$RoomID = $importRow['LocationID'];
	$ResponsibleUID = $importRow['OccupiedBy'];
	$BookingRemarks = $importRow['Remarks'];
	$settingArr['CancelDayBookingIfOneItemIsNA'][$RoomID] = 0;
	
	$successArr[] = $lebooking->New_Room_Reserve($RoomID, $DateTimeArr, '', array(), $BookingRemarks, $ResponsibleUID, $settingArr, array());
	
	unset($DateTimeArr);
	unset($settingArr);
}

if(in_array(false,$successArr)){
	$numSuccess = 0;
}
else{
	$numSuccess = count($successArr);
}
### PageInfo
$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_BookingRequest";
$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
$navigationAry[] =array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest'], 'javascript: goBack();');
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

### Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "goCancel();");

$linterface->LAYOUT_START();
?>

<script type="text/javascript">
$(document).ready( function() {
	
});

function goCancel() {
	window.location = 'booking_request.php';
}
</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<p class="spacer"></p>
	
	<?=$htmlAry['steps']?>
	
	<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=$numSuccess?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<?=$htmlAry['iframe']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>