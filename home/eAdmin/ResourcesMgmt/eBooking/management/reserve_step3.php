<?php
// using : Cameron   

############################################ Change Log ############################################
##
##	Date	:	2019-06-11 (Cameron)
##	         	should retrieve the actual TimeSlotID for the selected date by default TimeSlotID for EverydayTimetable when Booking Type is Periodic [case #M141869]
##
##	Date	:	2019-05-17 (Cameron)
##	         	pass TimeSlotID to $timeArr, hence to $DateTimeArr for $BookingMethod="Specific Period" so that it can store it for time slot adjustment [case #M141869]
##
##	Date	:	2019-05-01 (Cameron)
##	         	fix potential sql injection problem by applying IntegerSafe function to variables
##
##	Date	:	2018-08-22 (Cameron)
##				hide select attachment function for HKPF
##
##	Date	:	2018-08-15 (Henry)
##				fixed sql injection
##
##	Date	:	2015-04-21 (Omas)
##				add a new parameter for AdminEdit
##
##	Date	:	2015-01-16 (Omas)
##				new Setting - Booking Category enable by $sys_custom['eBooking_BookingCategory']
##
##	Date	:	2015-01-15 (Omas)
##				new $sys_custom['eBooking_MultipleRooms'] - allow reserve multiple rooms for specific time method in eAdmin & eService
##
##	Date	:	2014-10-30 (Bill)
##				Modified js_Go_New_Room_Booking() to show confirm dialog if overwrite any resources
##
##	Date	:	2014-08-01 (Carlos)
##				Modified js_Show_Event_Title(), disable/enable EventCalID selection
##
##  Date	:	2013-08-22 (Carlos) 
##				$sys_custom['eBooking_ReserveMultipleRooms'] - allow reserve multiple rooms for specific time method
##
##	Date	:	2013-03-26 (Carlos)
##				modified BookingMethod==2 use number index as datetime array instead of use date as key
##
##	Date	: 	2012-10-03 (Rita)
##				add js checking for 'Event' field.
##	
##	Date	: 	2012-09-19 (Rita)	
##				Ament js_Go_New_Room_Booking(), alert door access message
##
##	Date	:	2011-11-14 (YatWoon)
##				Improved: customization for remark ($sys_custom['eBooking_Cust_Remark']['WongKamFai'])
##
####################################################################################################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation_cardReader.php");		
			
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
//$linventory	= new libinventory();
$lebooking_ui 	= new libebooking_ui();
$lebooking_ui->Check_Page_Permission($AdminOnly=0);
$extraCardReaderObj = new liblocation_cardReader();		

$From_eAdmin = $_SESSION['EBOOKING_BOOKING_FROM_EADMIN']?1:0;
$CurrentBookingID = IntegerSafe($BookingID);

if($From_eAdmin)
{
	$CurrentPageArr['eBooking'] = 1;
	$CurrentPage	= "PageManagement_BookingRequest";
	$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['BookingRequest']);
}
else
{
	$CurrentPage	= "eService_MyBookingRecord";
	$TAGS_OBJ[] = array($Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord']);
	$From_eService=1;
}

$AdminEdit = IntegerSafe($AdminEdit);
if($AdminEdit){ //2015-04-21
	$CurrentBookingID = IntegerSafe($_REQUEST['RecordID']);
}

$MODULE_OBJ = $lebooking_ui->GET_MODULE_OBJ_ARR($From_eService);

$ReturnMsg = $Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$FacilityType = IntegerSafe($_REQUEST['FacilityType']);
$FacilityID = $_REQUEST['FacilityID'];

$StoredValueForBack = unserialize(urldecode($StoredValueForBack));
$BookingMethod = IntegerSafe($BookingMethod);
if( ($sys_custom['eBooking_MultipleRooms'] || ($sys_custom['eBooking_ReserveMultipleRooms'] && $From_eAdmin)) && $BookingMethod == LIBEBOOKING_DEFAULT_BOOKING_METHOD_TIME && $FacilityType == LIBEBOOKING_FACILITY_TYPE_ROOM){
	$isReserveMultipleRooms = true;
	// Expected $FacilityID to be array type
	if(!is_array($_REQUEST['FacilityID'])){
		$FacilityID = explode(",",$_REQUEST['FacilityID']);
	}
}
$FacilityID = IntegerSafe($FacilityID);
$BookingType = IntegerSafe($BookingType);

$DateTimeArr = array();
if($CurrentBookingID == '')
{
	if($BookingMethod==2) // Specific Time
	{
		/*
		foreach($FinalDateArr as $thisDate)
		{
			$DateArr[] = $thisDate;
			$StartTimeArr[] = $FinalStartTimeArr[$thisDate];
			$EndTimeArr[] = $FinalEndTimeArr[$thisDate];
		}
		*/
		for($i=0;$i<count($FinalDateArr);$i++) {
			$DateArr[] = $FinalDateArr[$i];
			$StartTimeArr[] = $FinalStartTimeArr[$i];
			$EndTimeArr[] = $FinalEndTimeArr[$i];
		}
			
		$DateTimeArr = $lebooking_ui->Build_DateTimeArr($DateArr, $StartTimeArr, $EndTimeArr);
		$StoredValueForBack['SelectedDateArr'] = $SelectedDateArr;
		$StoredValueForBack['StartHour'] = $StartHour;
		$StoredValueForBack['StartMin'] = $StartMin;
		$StoredValueForBack['EndHour'] = $EndHour;
		$StoredValueForBack['EndMin'] = $EndMin;
		$StoredValueForBack['FinalDateArr'] = $FinalDateArr;
	}
	else // Specific Period
	{
		if($BookingType == 1) // Single
		{
		    $TimeslotIDArr= array_unique((array)array_values_recursive($SelectedSlot));
		    $TimeSlotArr = $lebooking_ui->Get_Timeslot_By_TimetableID('',$TimeslotIDArr);
			$TimeSlotArr = BuildMultiKeyAssoc($TimeSlotArr, "TimeSlotID");
			
			foreach((array)$SelectedSlot as $DateTimestamp => $thisTimeslotArr)
			{
				$timeArr = array();
				
				foreach((array)$thisTimeslotArr as $thisTimeSlotID)
				{
					$thisDate = date("Y-m-d",$DateTimestamp);
					$timeArr[$TimeSlotArr[$thisTimeSlotID]['StartTime']]['StartTime'] = $TimeSlotArr[$thisTimeSlotID]['StartTime'];
					$timeArr[$TimeSlotArr[$thisTimeSlotID]['StartTime']]['EndTime'] = $TimeSlotArr[$thisTimeSlotID]['EndTime'];
					$timeArr[$TimeSlotArr[$thisTimeSlotID]['StartTime']]['TimeSlotID'] = $thisTimeSlotID;
				}
				ksort($timeArr);
				
				$DateTimeArr[$thisDate] = array_values($timeArr);
			}
			$StoredValueForBack['SelectedSlot'] = $SelectedSlot;			
		}
		else
		{
		    if ($sys_custom['eBooking']['EverydayTimetable']) {
		        // $TimeRangeArr stores default timetable TimeSlotID, need to lookup actual TimeSlotID of each selected date
		        foreach((array)$TimeRangeArr as $_defaultTimeSlotID) {
		            $_dateAry = $TimeSlotBookingDate[$_defaultTimeSlotID];
		            foreach((array)$_dateAry as $__date) {
		                $__timeslotInfoAry = $lebooking_ui->getActualTimeslotInfoByDefaltTimeslot($_defaultTimeSlotID, $__date);
		                $DateTimeArr[$__date][] = $__timeslotInfoAry;
		            }
		        }
		    }
		    else {
    			$TimeSlotArr = $lebooking_ui->Get_Timeslot_By_TimetableID('',$TimeRangeArr);
    			$TimeSlotArr = BuildMultiKeyAssoc($TimeSlotArr, "TimeSlotID");
    			
    			foreach((array)$TimeRangeArr as $thisTimeslotID)
    			{
    				$thisDate = date("Y-m-d",$DateTimestamp);       // 2019-06-11: not used ?
    				$timeArr['StartTime'] = $TimeSlotArr[$thisTimeslotID]['StartTime'];
    				$timeArr['EndTime'] = $TimeSlotArr[$thisTimeslotID]['EndTime'];
    				$timeArr['TimeslotID'] = $thisTimeslotID;
    				foreach((array)$TimeSlotBookingDate[$thisTimeslotID] as $thisBookingDate)
    				{
    					$DateTimeArr[$thisBookingDate][] = $timeArr;
    				}
    			}
		    }
			$StoredValueForBack['TimeRangeArr'] = $TimeRangeArr;
		}		
	}
	ksort($DateTimeArr);
}

//echo $lebooking_ui->Get_New_Booking_Step3_UI($DateTimeArr, $FacilityType, $FacilityID, $SelectedDateList, $CurrentBookingID, $From_eAdmin, $BookingMethod, $BookingType, $StoredValueForBack,$BookingCategoryID);
echo $lebooking_ui->Get_New_Booking_Step3_UI($DateTimeArr, $FacilityType, $FacilityID, $SelectedDateList, $CurrentBookingID, $From_eAdmin, $BookingMethod, $BookingType, $StoredValueForBack,$BookingCategoryID, $AdminEdit);

?>

<script language="javascript">
var originalRemarks = '';
$(document).ready( function() {
	//MM_showHideLayers('ItemBookingDetailsLayer','','show');
	js_Show_Event_Title()
	
	originalRemarks = $('textarea#BookingRemarks').val();
});

<?php if($isReserveMultipleRooms){ ?>
function js_Show_Event_Title()
{
	var FacilityIDArr = $('#FacilityID').val().split(',');
	for(var i=0;i<FacilityIDArr.length;i++) {
		var roomId = FacilityIDArr[i];
		if($("input#CreateEventYes\\["+roomId+"\\]").is(':checked')) {
			$("#iCalEventTitle\\["+roomId+"\\]").attr("disabled",false);
			if($("#EventCalID\\["+roomId+"\\]").length>0){
				$("#EventCalID\\["+roomId+"\\]").attr("disabled",false);
			}
		}else{
			$("#iCalEventTitle\\["+roomId+"\\]").attr("disabled",true);
			if($("#EventCalID\\["+roomId+"\\]").length>0){
				$("#EventCalID\\["+roomId+"\\]").attr("disabled",true);
			}
		}
	}
}
<?php }else{ ?>
function js_Show_Event_Title()
{
	if($("input.CreateEvent:checked").val()==1) {
		$("#iCalEventTitle").attr("disabled",false);
		var FacilityID = $('#FacilityID').val();
		if($("#EventCalID\\["+FacilityID+"\\]").length>0){
			$("#EventCalID\\["+FacilityID+"\\]").attr("disabled",false);
		}
	} else {
		$("#iCalEventTitle").attr("disabled",true);
		var FacilityID = $('#FacilityID').val();
		if($("#EventCalID\\["+FacilityID+"\\]").length>0){
			$("#EventCalID\\["+FacilityID+"\\]").attr("disabled",true);
		}
	}
}
<?php } ?>

function js_Go_Back_Step1()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'reserve_step1.php';
	objForm.submit();
}

function js_Go_Back_Step2()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'reserve_step2.php';
	objForm.submit();
}

function js_Go_Cancel_Booking()
{
	var objForm = document.getElementById('form1');
	if(<?=$From_eAdmin?>)
		objForm.action = 'booking_request.php';
	else
		objForm.action = '<?=$PATH_WRT_ROOT?>home/eService/eBooking/index_list.php';
	objForm.submit();
}

<?php if($isReserveMultipleRooms){ ?>
function js_Go_New_Room_Booking(parConfirmOverwrite)
{
	var isValid = false;
	var FacilityIDArr = $('#FacilityID').val().split(',');
	var objForm = document.getElementById('form1');
	
	$('input#Btn_Submit').attr('disabled', 'disabled');
	$('input#Btn_Back').attr('disabled', 'disabled');
	$('input#Btn_Cancel').attr('disabled', 'disabled');
	
	for(var i=0;i<FacilityIDArr.length;i++) {
		var roomId = FacilityIDArr[i];
		var isAlert = true;
		<? if($sys_custom['eBooking_Cust_Remark']['WongKamFai']) {?>
			if(!countChecked(document.form1, "ActivityType"))
			{
				alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectActivityType'];?>");
				document.form1.ActivityType[0].focus();
			}
			else if($("#ActivityName").val() == "") 
			{
				alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputActivityName'];?>");
				$("#ActivityName").focus();
			}
			else if($("#Attendance").val() == "") 
			{
				alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputParticipants'];?>");
				$("#Attendance").focus();
			}
			else if($("#PIC").val() == "") 
			{
				alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PIC'];?>");
				$("#PIC").focus();
			}
			else if( !is_positive_int(document.form1.IT_Teachnician.value))
			{
				alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
				$("#IT_Teachnician").focus();
			}
			else if( !is_positive_int(document.form1.Photographer.value))
			{
				alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
				$("#Photographer").focus();
			}
			else if( !is_positive_int(document.form1.Clerical.value))
			{
				alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
				$("#Clerical").focus();
			}
			else if( !is_positive_int(document.form1.Janitor.value))
			{
				alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
				$("#Janitor").focus();
			}
			else
		<? } ?>
		if($("#CreateEventYes\\["+roomId+"\\]").attr('checked') && $("#iCalEventTitle\\["+roomId+"\\]").val() == "") 
		{
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputCalendarEventTitle'];?>");
			$("#iCalEventTitle\\["+roomId+"\\]").focus();
		} 
			//check Event
		<? if ($sys_custom['eBooking_Booking_ExtraEvent_Input'] == true) { ?>
			 else if($("#BookingEventTb\\["+roomId+"\\]").val() == ""){			
				alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputEvent'];?>");
				$("#BookingEventTb\\["+roomId+"\\]").focus();
			 }
			 
		<? }?>
	
		else
		{
			isValid = true;
			isAlert = false;
			
			<? if($lebooking_ui->Check_Enable_Door_Access_Right()==true){
				$doorAccessCheck = $extraCardReaderObj ->getCardReaderInfoByLocationId($FacilityID, 1);	
				if(count($doorAccessCheck)>0){
			 ?>				
				alert("<?=$Lang['eBooking']['eService']['DoorAccessappliedRemind'];?>");
			<?} }?>
			//var objForm = document.getElementById('form1');
<?php if (!$sys_custom['project']['HKPF']):?>			
			checkOption(objForm.elements["AttachmentArr["+roomId+"]"]);
			checkOptionAll(objForm.elements["AttachmentArr["+roomId+"]"]);
<?php endif;?>
			//objForm.action = 'reserve_update.php';
			//objForm.submit();
		}
		if(isAlert){
			isValid = false;
			break;
		}
	}
	
	if (isValid && parConfirmOverwrite == '1' && !confirm('<?= $Lang['eBooking']['ReserveBookingAdminOverwriteConfirm'] ?>')) {
		isValid = false;
	}
	
	if (!isValid) {
		$('input#Btn_Submit').attr('disabled', '');
		$('input#Btn_Back').attr('disabled', '');
		$('input#Btn_Cancel').attr('disabled', '');
	}else{
		objForm.action = 'reserve_update.php';
		objForm.submit();
	}
}
<? }else{ // normal flow ?>

function js_Go_New_Room_Booking(parConfirmOverwrite)
{
	var isValid = false;

	$('input#Btn_Submit').attr('disabled', 'disabled');
	$('input#Btn_Back').attr('disabled', 'disabled');
	$('input#Btn_Cancel').attr('disabled', 'disabled');
	
	<? if($sys_custom['eBooking_Cust_Remark']['WongKamFai']) {?>
		if(!countChecked(document.form1, "ActivityType"))
		{
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectActivityType'];?>");
			document.form1.ActivityType[0].focus();
		}
		else if($("#ActivityName").val() == "") 
		{
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputActivityName'];?>");
			$("#ActivityName").focus();
		}
		else if($("#Attendance").val() == "") 
		{
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputParticipants'];?>");
			$("#Attendance").focus();
		}
		else if($("#PIC").val() == "") 
		{
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PIC'];?>");
			$("#PIC").focus();
		}
		else if( !is_positive_int(document.form1.IT_Teachnician.value))
		{
			alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
			$("#IT_Teachnician").focus();
		}
		else if( !is_positive_int(document.form1.Photographer.value))
		{
			alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
			$("#Photographer").focus();
		}
		else if( !is_positive_int(document.form1.Clerical.value))
		{
			alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
			$("#Clerical").focus();
		}
		else if( !is_positive_int(document.form1.Janitor.value))
		{
			alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>");
			$("#Janitor").focus();
		}
		else
	<? } ?>
	
	if($("#CreateEventYes").attr('checked') && $("#iCalEventTitle").val() == "") 
	{
		alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputCalendarEventTitle'];?>");
		$("#iCalEventTitle").focus();
	} 
		//check Event
	<? if ($sys_custom['eBooking_Booking_ExtraEvent_Input'] == true) { ?>
		 else if($("#BookingEventTb").val() == ""){			
			alert("<?=$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputEvent'];?>");
			$("#BookingEventTb").focus();
		 }
		 
	<? }?>
	
	<? if ($sys_custom['eBooking_CheckRemarksDifferentForNewBooking']) { ?>
		else if ('<?=$CurrentBookingID?>' == '' && originalRemarks == $('textarea#BookingRemarks').val()) {
			alert("<?=$Lang['eBooking']['jsWarningArr']['PleaseInputRemarks']?>");
			$("#BookingRemarks").focus();
		}
	<? } ?>
	
	else
	{
		isValid = true;
		
		<? if($lebooking_ui->Check_Enable_Door_Access_Right()==true){
			$doorAccessCheck = $extraCardReaderObj ->getCardReaderInfoByLocationId($FacilityID, 1);	
			if(count($doorAccessCheck)>0){
		 ?>				
			alert("<?=$Lang['eBooking']['eService']['DoorAccessappliedRemind'];?>");
		<?} }?>
		
		if (parConfirmOverwrite != '1' || parConfirmOverwrite == '1' && confirm('<?= $Lang['eBooking']['ReserveBookingAdminOverwriteConfirm'] ?>')) {
			var objForm = document.getElementById('form1');
<?php if (!$sys_custom['project']['HKPF']):?>			
			checkOption(objForm.elements["AttachmentArr"]);
			checkOptionAll(objForm.elements["AttachmentArr"]);
<?php endif;?>			
			objForm.action = 'reserve_update.php';
			objForm.submit();
		} 
		else {
			isValid = false;
		}
		
	}

	if (!isValid) {
		$('input#Btn_Submit').attr('disabled', '');
		$('input#Btn_Back').attr('disabled', '');
		$('input#Btn_Cancel').attr('disabled', '');
	}
}
<?php } ?>

<?php if($isReserveMultipleRooms) { ?>
function UpdateResponsiblePerson(jsUserID, jsUserName, jsFacilityID)
{
	$('input#ResponsibleUID\\['+jsFacilityID+'\\]').val(jsUserID);
	$('span#ResponsibleUserNameSpan\\['+jsFacilityID+'\\]').html(jsUserName);
}
<?php }else{ ?>
function UpdateResponsiblePerson(jsUserID, jsUserName)
{
	$('input#ResponsibleUID').val(jsUserID);
	$('span#ResponsibleUserNameSpan').html(jsUserName);
}
<?php } ?>

function js_Show_Item_Booking_Details(jsItemID, jsClickedObjID)
{
	js_Hide_Detail_Layer();
	js_Change_Layer_Position(jsItemID, jsClickedObjID);
	
//	$('div#ItemBookingDetailsLayerContentDiv').html('<?=$Lang['General']['Loading']?>').load(
//		"ajax_task.php", 
//		{ 
//			task: 'LoadItemBookingLayerTable',
//			ItemID: jsItemID,
//			DateTimeArr: $('input#DateTimeArr').val()
//		},
//		function(ReturnData)
//		{
			js_Show_Detail_Layer(jsItemID);
//		}
//	);
}

function js_Hide_Detail_Layer(jsItemID)
{
//	$('#ItemBookingDetailsLayer'+jsItemID).hide();
	MM_showHideLayers('ItemBookingDetailsLayer'+jsItemID,'','hide');
}

function js_Show_Detail_Layer(jsItemID)
{
//	$('#ItemBookingDetailsLayer'+jsItemID).show();
	MM_showHideLayers('ItemBookingDetailsLayer'+jsItemID,'','show');
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function js_Change_Layer_Position(jsItemID, jsClickedObjID) {
	var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') + 15;
	var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') + 10;
	
	var ObjLayer = document.getElementById('ItemBookingDetailsLayer'+jsItemID);
	ObjLayer.style.left = posleft + "px";
	ObjLayer.style.top = postop+10 + "px";
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>