<?php
//using : 
/*
 *  2019-05-01 Cameron
 *      - fix potential sql injection problem by cast related variables to integer
 */

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();
$li = new libfilesystem();
$lexport = new libexporttext();


$CurrentPageArr['eBooking'] = 1;

//$table_content .= $lebooking_ui->ShowFollowUpWorkPrintPage($WeekStartTimeStamp,$FollowUpGroup,1);

$exportColumn = array($Lang['eBooking']['Management']['FieldTitle']['FollowUpWork'],'','','','','','','');

// empty row
$Rows[] = array('');

$FollowUpGroup = IntegerSafe($FollowUpGroup);
$targetFollowUpGroup = implode(",",$FollowUpGroup);

$sql = "SELECT GroupName, GroupColor FROM INTRANET_EBOOKING_FOLLOWUP_GROUP WHERE GroupID IN ($targetFollowUpGroup) Order By GroupName";
$arrGroupInfo = $lebooking_ui->returnArray($sql,3);

if(sizeof($arrGroupInfo) > 0){
	$arr_group_info[] = $Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'];
	for($i=0; $i<sizeof($arrGroupInfo); $i++){
		list($group_name, $group_color) = $arrGroupInfo[$i];
		$arr_group_info[] = $group_name;
	}
	if(in_array("-999",$FollowUpGroup)){
		$arr_group_info[] = $Lang['eBooking']['Settings']['FieldTitle']['NoFollowUpGroup'];
	}
}
$Rows[] = $arr_group_info;

// empty row
$Rows[] = array('');

// csv main content 
$csv_content = $lebooking_ui->ShowFollowUpWorkExport($WeekStartTimeStamp,$targetFollowUpGroup,$RemarkSelection);

// combine additional information and main content
$final_csv_content = array_merge($Rows,$csv_content);
$export_content = $lexport->GET_EXPORT_TXT($final_csv_content, $exportColumn);

$filename = "item_booking_list".date('Y-m-d').".csv";
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>