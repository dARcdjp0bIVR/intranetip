<?php
// using : 
################## Change Log [Start] #################
#
#	Date	:	2015-05-12	Omas
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$lebooking = new libebooking();

$targetBuilding = $_REQUEST['BuildingCode'];

$LocationAssoAry = $lebooking->getExistingLocationAssoAry(array('BuildingCode','LocationLevelCode','RoomCode'));
if($targetBuilding == ''){
	foreach($LocationAssoAry as $BuildingCode => $_LocationArr){
		foreach($_LocationArr as $_LocationCode => $__SubLocationArr){
			foreach($__SubLocationArr as $__SubLocationCode=> $__SubLocationInfoArr){
				$exportDataAry[] = array($BuildingCode,$_LocationCode,$__SubLocationCode);
			}
		}
	}
}
else{
	$_LocationArr = $LocationAssoAry[$targetBuilding];
	$BuildingCode = $targetBuilding;
	if($_LocationArr != ''){
		foreach($_LocationArr as $_LocationCode => $__SubLocationArr){
				foreach($__SubLocationArr as $__SubLocationCode=> $__SubLocationInfoArr){
					$exportDataAry[] = array($BuildingCode,$_LocationCode,$__SubLocationCode);
				}
		}
	}
}

$exportHeaderAry = $lebooking->getImportHeader();
$colProperty = $lebooking->getImportColumnProperty();
$export_header = $lexport->GET_EXPORT_HEADER_COLUMN($exportHeaderAry,$colProperty);
$export_content = $lexport->GET_EXPORT_TXT($exportDataAry, $export_header);

intranet_closedb();
$filename = "import_booking_application_template.csv";
$lexport->EXPORT_FILE($filename, $export_content);
?>