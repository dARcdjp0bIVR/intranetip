<?php
// using : 
############################ Change Log ###########################
##
##	Date	: 2015-10-05 (Omas)
##			Create this page
##	
###################################################################
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eBooking'] = 1;
$CurrentPage	= "PageManagement_RoomBooking";
$linterface 	= new interface_html();
$lebooking 		= new libebooking();
$lebooking_ui	= new libebooking_ui();

$TAGS_OBJ[] = array($Lang['eBooking']['Management']['FieldTitle']['RoomBooking']);
$MODULE_OBJ = $lebooking->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($returnMsg);

//$BookingStatusList = isset($BookingStatusList)? $BookingStatusList : '1,0';

if( isset($BookStatus) && (sizeof($BookStatus)>0) )
{
	$BookingStatusList = implode(",", $BookStatus);
}
else
{
	if(isset($_GET['BookingStatus'])){
		$BookingStatusList = $_GET['BookingStatus']; 
	}else{
		$BookingStatusList = isset($BookingStatusList)? $BookingStatusList : '1,0';
	}
}

if(isset($_GET['LocationID'])){
	$FacilityID = $_GET['LocationID'];
}
if(isset($_GET['ManagementGroupID'])){
	$ManagementGroup = $_GET['ManagementGroupID'];
}
?>
<!--<div id="debugDiv">
</div>-->
<?

echo $lebooking_ui->Get_Room_Booking_MonthView_UI($FacilityID, $ManagementGroup, $BookingStatusList, $targetMonthTimeStamp);
echo $lebooking_ui->initJavaScript();
?>

<script language='JavaScript'>
	var CurrentView = "";
	var currentDate = new Date();
	currentDate.setDate('01'); 
	var currentDD = currentDate.getDate();
	var currentMM = currentDate.getMonth();
	var currentYYYY = currentDate.getFullYear();
	var MonthLang = new Array("<?=implode('","',$Lang['General']['month'])?>");
	
	$(document).ready( function() {
		CurrentView = "WeekView";
		
		$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar', mandatory: true});
		$('#TargetDate').datepick({
			dateFormat: 'yy-mm-dd',
			dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
			changeFirstDay: false,
			firstDay: 0
		});
		
		jsChangeLocation();
	});
	
	function jsChangeLocation()
	{
		var task = "ShowRoomBookingRecordInMonthView";
		var targetMonth = $("#targetMonth").val();
		var targetYear = $("#targetYear").val();
		Block_Document();
				
		$.post(
			"ajax_task.php",
			{
				task : task,
				targetMonth : targetMonth,
				targetYear : targetYear,
				LocationID : $("#FacilityID").val(),
				ManagementGroupID : $("#ManagementGroup").val(),
				BookingStatus : $("#BookingStatusList").val()
			},
			function (responseData)
			{
				$("#DIV_RoomBooking").html(responseData);
				$("#DIV_RoomBooking").show();
				UnBlock_Document();
				initThickBox();
			}
		);
	}
	
	function UpdateStatusList()
	{
		var StatusListAry = new Array();
		var check_box_checked = 0;
	
		$("input.StatusCheckBox:checked").each(function(){
			StatusListAry.push($(this).val());
			check_box_checked++;
		});
	
		$("#BookingStatusList").val(StatusListAry.join(","));
		
		if(check_box_checked == 0){
			alert("<?=$Lang['eBooking']['Management']['General']['JSWarning']['PleaseSelectStatus'];?>");
		}else{
			refreshBookRecord();
		}
	}
	
	function refreshBookRecord()
	{
		MM_showHideLayers('status_option','','hide');
		jsChangeLocation();
	}
	
	function jsChangeMonthByCal(){
		var PickedDate = $("#TargetDate").val();
		
		currentDate = new Date(PickedDate);
		currentDD = currentDate.getDate();
		currentMM = currentDate.getMonth();
		currentYYYY = currentDate.getFullYear();
		getTargetMonth();
	}
	
	function jsChangeMonthByArrow(change){
		currentDate.setMonth(currentDate.getMonth() + change);
		currentDate.setDate('01');
		currentDD = currentDate.getDate();
		currentMM = currentDate.getMonth();
		currentYYYY = currentDate.getFullYear();
		getTargetMonth();
	}
	
	function getTargetMonth(){
		
		var targetDate = '';
		var dd = currentDD;
		var mm = currentMM + 1; //January is 0
		var yyyy = currentYYYY;
		
		$('input#targetMonth').val(mm);
		$('input#targetYear').val(yyyy);
//		LoadingMonthView(yyyy,mm);
		displayLang = MonthLang[mm] + " - " + yyyy;
		$('#DIV_WeeklyDisplay').html(displayLang);
		jsChangeLocation();
	}
	
	function ChangeView(val)
	{
		if(val == "WeekView")
		{
			CurrentView = "WeekView";
			$("div#caltabs_right").load(
				"ajax_task.php",
				{
					task : "ChangeSelectedTab",
					ViewMode : "WeekView"
				},
				function(returnString)
				{
					jsChangeLocation();
				}
			);
		}
		else if(val == "ListView")
		{
			var LocationID = $("#FacilityID").val();
			var ManagementGroupID = $("#ManagementGroup").val();
			var BookingStatus = $("#BookingStatusList").val();
			var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
			
			//window.location = "room_booking_list.php?clearCoo=1";
			var param = '?LocationID=' + LocationID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus + '&WeekStartTimeStamp=' + WeekStartTimeStamp; 
			window.location = "room_booking_list.php" + param;
		}
	}
	
	function js_Show_Booking_Detail_Layer(jsDetailType, jsBookingID, jsClickedObjID)
	{
		js_Hide_Booking_Detail_Layer();
		
		var jsAction = '';
		if (jsDetailType == 'RoomBookingDetail')
			jsAction = 'ShowRoomBookingDetail';
		
		$('div#BookingDetailContentDiv').html('<?=$Lang['General']['Loading']?>');	
		js_Change_Layer_Position(jsClickedObjID);
		MM_showHideLayers('BookingDetailLayer','','show');
				
		$('div#BookingDetailContentDiv').load(
			"ajax_task.php", 
			{ 
				task: jsAction,
				BookingID: jsBookingID
			},
			function(returnString)
			{
				//$('div#debugDiv').html(returnString);
				$('div#BookingDetailContentDiv').css('z-index', '999');
			}
		);
	}
	
	function js_Hide_Booking_Detail_Layer()
	{
		MM_showHideLayers('BookingDetailLayer','','hide');
	}
	
	function getPosition(obj, direction)
	{
		var objStr = "obj";
		var pos_value = 0;
		while (typeof(eval(objStr))!="undefined" && (eval(objStr + ".tagName")!="BODY" && eval(objStr + ".tagName")!="HTML"))
		{
			pos_value += eval(objStr + "." + direction);
			objStr += ".offsetParent";
		}
	
		return pos_value;
	}
	
	function js_Change_Layer_Position(jsClickedObjID) 
	{
		var jsOffsetLeft, jsOffsetTop;
		
		jsOffsetLeft = 165;
		jsOffsetTop = -50;
			
		var posleft = getPosition(document.getElementById(jsClickedObjID), 'offsetLeft') - jsOffsetLeft;
		var postop = getPosition(document.getElementById(jsClickedObjID), 'offsetTop') - jsOffsetTop;
				
		document.getElementById('BookingDetailLayer').style.left = posleft + "px";
		document.getElementById('BookingDetailLayer').style.top = postop + "px";
		document.getElementById('BookingDetailLayer').style.visibility = 'visible';
	}
	
	function js_Print_RoomBooking_Week()
	{
		var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
		var LocationID = $("#FacilityID").val();
		var ManagementGroupID = $("#ManagementGroup").val();
		var BookingStatus = $("#BookingStatusList").val();
				
		var param = '?WeekStartTimeStamp=' + WeekStartTimeStamp + '&LocationID=' + LocationID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus;
		var url = "room_booking_week_print.php" + param;
		newWindow(url, 10);
	}
	
	function js_Export_RoomBooking_Week()
	{
		var WeekStartTimeStamp = $("#WeekStartTimeStamp").val();
		var LocationID = $("#FacilityID").val();
		var ManagementGroupID = $("#ManagementGroup").val();
		var BookingStatus = $("#BookingStatusList").val();
				
		//var param = '?WeekStartTimeStamp=' + WeekStartTimeStamp + '&LocationID=' + LocationID + '&ManagementGroupID=' + ManagementGroupID + '&BookingStatus=' + BookingStatus;
		//var url = "room_booking_week_export.php" + param;
		var url = "room_booking_week_export.php";
		
		document.form1.action = url;
		document.form1.submit();		
	}
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>