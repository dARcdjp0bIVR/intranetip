<?php
/*
 *  Date: 2018-04-13 Henry modified photoHighLight css
 *  Date:2016-10-13 Villa open the file - displaying highlight
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldc = new libdigitalchannels();
$ldcUI = new libdigitalchannels_ui();
$arrCookies = array();
$arrCookies[] = array("ck_resourcesmanagment_digitalchannels_from_eService","From_eService");
$arrCookies[] = array("ck_resourcesmanagment_digitalchannels_AcademicYearID", "SchoolYear");
if(isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
    $From_eService = (isset($_GET['From_eService']))? $_GET['From_eService'] : $From_eService;
    updateGetCookies($arrCookies);
}
else {
    updateGetCookies($arrCookies);
}

if ($From_eService) {
    $CurrentPageArr['eServiceDigitalChannels'] = 1;
}
else{
    $CurrentPageArr['DigitalChannels'] = 1;
}

$home_header_no_EmulateIE7 = true;

echo '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery-1.3.2.min.js"></script>';
echo $ldcUI->Include_JS_CSS();
?>
<script type="text/javascript">
    $(function(){
        jQuery(function($){
            $('.photo_thumb_effect').mosaic({
                animation	:	'slide'
            });
        });
        if (navigator.userAgent.indexOf("Firefox") === -1) {
            $("video").click(function() {
                var current_video = $("video").get(0);
                if (current_video.paused) {
                    current_video.play();
                }
                else {
                    current_video.pause();
                }
            });
        }
    });

    $(document).ready(function() {
        $('body').addClass('elib_bg bookshelf_type_01  bookshelf_type_01_sky theme_rainbow');
        document.title += ' > Digital Channels';
        var isOldBrowser = 0;
        if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
            isOldBrowser = 1;
            alert('<?=$Lang['DigitalChannels']['Msg']['BrowserReminder']?>!');
        }
    });

    function NextPhoto(){
        var photoArray = $(".photo_block");
        for(var i=0; i < photoArray.length; i++){
            if(photoArray.get(i).style.display != 'none'){
                photoArray.get(i).style.display = 'none';
                if(i+1 < photoArray.length){
                    photoArray.get(i+1).style.display = '';
                }
                else{
                    photoArray.get(0).style.display = '';
                }
                break;
            }
        }
    }

    function PrevPhoto(){
        var photoArray = $(".photo_block");
        for(var i=0; i < photoArray.length; i++){
            if(photoArray.get(i).style.display != 'none'){
                photoArray.get(i).style.display = 'none';
                if(i-1 >= 0){
                    photoArray.get(i-1).style.display = '';
                }
                else{
                    photoArray.get(photoArray.length-1).style.display = '';
                }
                break;
            }
        }
    }


</script>
<style>
    #photoHighLight {
<?php if ($_GET['srcFrom'] == 'portal'): ?>
        transform: scale(0.59, 0.54);
<?php else:?>
        transform: scale(0.46);
<?php endif;?>
        transform-origin: left;
    }
</style>
<body bgcolor='#FFFFFF'>
<div id='photoHighLight'><?=$ldcUI->Display_Index_Page_highLight($_GET['srcFrom']);?></div>
</body>
<?php intranet_closedb();
?>
