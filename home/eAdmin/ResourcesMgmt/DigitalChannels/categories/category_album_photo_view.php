<?php
// Editing by 
/*
 * Change Log:
 * 2020-03-09 (Henry): not allow to right click the img file [Case#Y179425]
 * 2018-08-08 (Henry): fixed sql injection 
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2015-03-30 (Henry): apply the fancybox to view photo
 * 2014-11-10 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldc = new libdigitalchannels();
$ldcUI = new libdigitalchannels_ui();

$arrCookies = array();
$arrCookies[] = array("ck_resourcesmanagment_digitalchannels_from_eService","From_eService");			
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}
if ($From_eService) {
	$CurrentPageArr['eServiceDigitalChannels'] = 1;
}
else{
	$CurrentPageArr['DigitalChannels'] = 1;
}

$AlbumID = IntegerSafe($AlbumID);
$AlbumPhotoID = IntegerSafe($AlbumPhotoID);

if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
	header("Location: ../index.php");
	exit;
}

if ($AlbumID && !$ldc->isAlbumReadable($AlbumID)){
	header("Location: ../index.php");
	exit;
}

if(!$ldc->getAlbumPhoto($AlbumPhotoID)){
	header("Location: ../index.php");
	exit;
}

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['Category'],"",1);

$CurrentPage = "Categories";

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}
$home_header_no_EmulateIE7 = true;

//increase the view number
$ldc->Add_Album_Photo_View($AlbumPhotoID);

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$ldcUI->LAYOUT_START($Msg);

echo $ldcUI->Include_JS_CSS();

include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
$lgs = new libgeneralsettings();
$settings = $lgs->Get_General_Setting("DigitalChannels", array("'AllowDownloadOriginalPhoto'"));
?>
<script type="text/javascript" src="../script/photo_thumb.js"></script> 
<script type="text/javascript" src="/templates/jquery/jquery.fancybox.pack.js">	</script>
<link rel="stylesheet" type="text/css" href="/templates/jquery/jquery.fancybox.css" media="screen"/>  
<style>

#fancybox-left {
	top:35%;
    height:30%;
}

#fancybox-right {
	top:35%;
    height:30%;
}
</style>
<script>
var $j = $.noConflict();

$(function(){
	
	$("a[rel=example_group]").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'cyclic'        	:  true,
		/*'titlePosition' 	: 'over',
		'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
			return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		}*/
	});
	
	$(".various3").fancybox({
		'width'				: '100%',
		'height'			: '100%',
		'autoScale'			: false,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'cyclic'        	:  true,
		'type'				: 'iframe',
		/*'titlePosition' 	: 'over',
		'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
			return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		}*/
	});
		
	jQuery(function($){
		$('.photo_thumb_effect').mosaic({
			animation	:	'slide'
		});
	});
	if (navigator.userAgent.indexOf("Firefox") === -1) {
		$("video").click(function() {
	    	var current_video = $("video").get(0);
	    	if (current_video.paused) {
	    		current_video.play();
	  		}
	  		else {
	    		current_video.pause();
	  		}
		});
	}
	loadEnjoyUsers();
	loadComments();
	
	<?if(!$settings['AllowDownloadOriginalPhoto']){?>
    //disable the right click on the photo
    $('div.view_images a, #fancybox-img, #fancybox-overlay, #fancybox-outer').live('contextmenu', function(e) {
	    return false;
	});
    
    //disable drag and drop of the image
    $('div.view_images img, div#fancybox-inner img').live('mousedown', function (e) {
           return false;
    });
    <?}?>
});

function Update_Photo_Favorites(photoID){
	$.post('../organize/ajax.php?action=enjoyphoto', { PhotoID: photoID },function(res){
		//alert(res);	
		if(res > 0){
			$('.icon_enjoy').addClass('active');
			$("#enjoy_count").text(parseInt($("#enjoy_count").text())+1);
			$('.icon_enjoy').attr('title', '<?=$Lang['DigitalChannels']['Organize']['RemoveEnjoyThisPhoto']?>');
		}
		else{
			$('.icon_enjoy').removeClass('active');
			$("#enjoy_count").text(parseInt($("#enjoy_count").text())-1);
			$('.icon_enjoy').attr('title', '<?=$Lang['DigitalChannels']['Organize']['EnjoyThisPhoto']?>');
		}
		loadEnjoyUsers();
	});
}

function loadEnjoyUsers(){
	$j.fancybox.showLoading();
	$('body #enjoy_user_list').load('../organize/ajax.php?action=getEnjoyUsers&AlbumID=<?=$AlbumID?>&AlbumPhotoID=<?=$AlbumPhotoID?>',function(){
        $j.fancybox.hideLoading();
        $(this).fadeIn()
    });
}

function loadComments(){
	$j.fancybox.showLoading();
	$('body #comment_content').load('../organize/ajax.php?action=getComments&AlbumID=<?=$AlbumID?>&AlbumPhotoID=<?=$AlbumPhotoID?>',function(){
        updateViewFavCommentsTotal();
        $j.fancybox.hideLoading();
        $(this).fadeIn()
    });
}

function updateViewFavCommentsTotal(){
//	$('body #comment_count').load('../organize/ajax.php?action=updateViewFavCommentsTotal',{ AlbumID: <?=$AlbumID?>, hidPhotoID: $('#hidPhotoID').val()}, function(){
//    });
    $.post('../organize/ajax.php?action=updateViewFavCommentsTotal',{ AlbumID: <?=$AlbumID?>, hidPhotoID: $('#hidPhotoID').val()}, function(res){
    	var res = $j.parseJSON(res);
    	$("#comment_count").text(res.NumComment);
    	$("#enjoy_count").text(res.NumFav);
    	$("#view_count").text(res.NumView);
    });
}

function addComments(){
	$j.fancybox.showLoading();
	$.post('../organize/ajax.php?action=addComments', { AlbumID: <?=$AlbumID?>, hidPhotoID: $('#hidPhotoID').val(), txtComment: $('#txtComment').val() }, function(res){
		loadComments();
        $j.fancybox.hideLoading();
        $(this).fadeIn();
        $('#txtComment').val('');
	});
}

function deleteComments(CommentID){
	if(confirm("<?=$Lang['DigitalChannels']['Msg']['RemoveComment']?>")){
		$j.fancybox.showLoading();
		$.post('../organize/ajax.php?action=deleleComments', { AlbumID: <?=$AlbumID?>, CommentID: CommentID }, function(res){
			loadComments();
	        $j.fancybox.hideLoading();
	        $(this).fadeIn();
		});
	}
}

function editCommentClick(CommentID){
	$('.hided_comment_text').show();
	$('.hided_comment_text').removeClass('hided_comment_text');
	$('#divEditComment').remove();
	
	var editComment ='<div class="comment_text" id="divEditComment"><textarea id="txtEditComment" name="txtEditComment" class="comment_text_input" rows="3">'+$('#comment_text'+CommentID).text()+'</textarea><input id="btnCancelEditComment" name="btnCancelEditComment" class="comment_btn" value="<?=$Lang['DigitalChannels']['General']['Cancel']?>" style="" type="button" onClick="cancelEditComments('+CommentID+')"> <input id="btnEditComment" name="btnEditComment" class="comment_btn" value="<?=$Lang['DigitalChannels']['General']['Edit']?>" style="" type="button" onClick="editComments('+CommentID+')"></div>';
	$('#comment_text'+CommentID).addClass('hided_comment_text');
	$('#comment_text'+CommentID).hide();
	$('#comment_text'+CommentID).after(editComment);
}

function cancelEditComments(CommentID){
	MM_showHideLayers('comment_edit'+CommentID,'','hide');
	$('.hided_comment_text').show();
	$('.hided_comment_text').removeClass('hided_comment_text');
	$('#divEditComment').remove();
}

function editComments(CommentID){
	//if(confirm("<?=$Lang['DigitalChannels']['Msg']['RemoveComment']?>")){
		$j.fancybox.showLoading();
		$.post('../organize/ajax.php?action=editComments', { AlbumID: <?=$AlbumID?>, CommentID: CommentID, txtEditComment: $('#txtEditComment').val() }, function(res){
			loadComments();
	        $j.fancybox.hideLoading();
	        $(this).fadeIn();
		});
	//}
}

function Add_Photo_Comment(){
	
}

function Update_Photo_Comment(){
	
}

function checkForm(){
	if(document.getElementById('txtComment').value.trim() == ''){
		alert("<?=$Lang['DigitalChannels']['Msg']['enterComment']?>");
		return false;
	}
	addComments();
	return false;
}

</script>	    
		
<div class="digital_channels_content"> <div id="container">
<?echo $ldcUI->Display_Title_Menu();?>
<?echo $ldcUI->Display_Category_Album_Photo_View($AlbumID, $AlbumPhotoID, true);?>         
</div></div>
<script type="text/javascript">
$(document).ready( function() {
	$('a#dynSizeThickboxLink').click( function() {
		load_dyn_size_thickbox_ip('', 'onloadThickBox();');
	});
	
	$('a#fixedSizeThickboxLink').click( function() {
		load_dyn_size_thickbox_ip('', 'onloadThickBox();', inlineID='', defaultHeight=200, defaultWidth=400);
	});
});

function onloadThickBox() {
//	$.post(
//		"thickbox_content.php", 
//		{ 
//			academicYearId: 1,
//			yearTermId: 2
//		},
//		function(ReturnData) {
//			$('div#TB_ajaxContent').html(ReturnData);
//			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
//		}
//	);
	
	$('div#TB_ajaxContent').load(
		"/home/eAdmin/ResourcesMgmt/DigitalChannels/organize/view_original_main.php", 
		{ 
			AlbumID: <?=$AlbumID?>,
			AlbumPhotoID: <?=$AlbumPhotoID?>
		},
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
</script>
<?php
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>