<?php
// Editing by 
/*
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."includes/libdbtable.php");
include_once ($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
	header("Location: ../index.php");
	exit;
}

$linterface = new interface_html();
$lduUI = new libdigitalchannels_ui();
$ldu = new libdigitalchannels();

# Display Admin Menu
$CurrentPageArr['DigitalChannelsAdminMenu'] = true;

$CurrentPageArr['DigitalChannels'] = 1;
$CurrentPage = "Settings_Category";

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['Category'],"",1);



 
$Code = (is_array($CategoryID)) ? $CategoryID[0] : $CategoryID;
if($Code)
$RespondInfo = $ldu->Get_Category($Code);

$CategoryCode = $RespondInfo[0]['CategoryCode'];
$DescriptionEn = $RespondInfo[0]['DescriptionEn'];
$DescriptionChi = $RespondInfo[0]['DescriptionChi'];


$PAGE_NAVIGATION[] = array($Lang['DigitalChannels']['Settings']['Category'], "category.php");
if($Code)
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
else
	$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
$MODULE_OBJ = $lduUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$lduUI->LAYOUT_START($Msg);

echo $lduUI->Include_JS_CSS();

?>
<script language="javascript">
<!--
function checkForm(form1) {
	
	//for english validation
	var english = /^[A-Za-z0-9]*$/;
	
	if(form1.CategoryCode.value=='')	 {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['DigitalChannels']['Settings']['CategoryCode'] ?>");	
		form1.CategoryCode.focus();
		return false;
	} else if(!english.test(form1.CategoryCode.value))	 {
		alert("<?= $Lang['DigitalChannels']['Settings']['PleaseFillInEnglish'].' '.$Lang['DigitalChannels']['Settings']['CategoryCode'] ?>");	
		form1.CategoryCode.focus();
		return false;
	} else if(form1.DescriptionEn.value=="") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['DigitalChannels']['Settings']['DescriptionEn'] ?>");	
		form1.DescriptionEn.focus(); 
		return false;
	} else if(form1.DescriptionChi.value=="") {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['DigitalChannels']['Settings']['DescriptionChi'] ?>");	
		form1.DescriptionChi.focus(); 
		return false;
	} else {
		return true;
	}
}

function submitCallback(json){
	if (json.success && json.unique){
		document.form1.submit();
	}else{
		$('#in_CategoryCode').focus().css('background-color','yellow');
		alert('<?=$Lang['DigitalChannels']['Settings']['Msg']['DuplicatedCategoryCode']?>');
	}
}

$(function(){
	$('#form1').submit(function(e){
		if(!checkForm(this)){
			return false;
		}
		if ( $('#in_Code').val() !=  $('#in_CategoryCode').val() ){
			$.getJSON('ajax_unique_check.php',{
				type :  'cat',
				needle : $('#in_CategoryCode').val()
			},submitCallback);
		e.preventDefault();
		return false;
		}
	});
});

//-->
</script>


<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form id='form1' name="form1" method="post" action="category_edit_update.php">

<table width="90%" align="center" border="0">
<tr><td>
	<input id='in_Code' name="Code" type="hidden" class="textboxtext" value="<?=$Code?>" />
	<table class="form_table_v30">
	<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['DigitalChannels']['Settings']['CategoryCode']?></td>
			<td><input id='in_CategoryCode' name="CategoryCode" type="text" class="textboxtext" value="<?=$CategoryCode?>" maxlength="8" /></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['DigitalChannels']['Settings']['DescriptionEn']?></td>
			<td><input name="DescriptionEn" type="text" class="textboxtext" value="<?=$DescriptionEn?>" /></td>
		</tr>
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['DigitalChannels']['Settings']['DescriptionChi']?></td>
			<td><input name="DescriptionChi" type="text" class="textboxtext" value="<?=$DescriptionChi?>" /></td>
		</tr>
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='category.php'")?>
	</div>	

</td></tr>
</table>

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.CategoryCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
