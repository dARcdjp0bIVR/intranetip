<?php
// Editing by 
/*
 * 2019-04-30 (Henry): security issue fix for SQL injection
 * 2014-11-10 (Henry): added option AllowUserToComment
 * 2014-10-09 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."lang/digitalarchive_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
	header("Location: ../index.php");
	exit;
}

intranet_auth();
intranet_opendb();

$lgs = new libgeneralsettings();

$success = array();
$success[] = $lgs->Save_General_Setting("DigitalChannels", array("KeepOriginalPhoto" => IntegerSafe($KeepOriginalPhoto)));
$success[] = $lgs->Save_General_Setting("DigitalChannels", array("AllowDownloadOriginalPhoto" => IntegerSafe($AllowDownloadOriginalPhoto)));
$success[] = $lgs->Save_General_Setting("DigitalChannels", array("DisallowUserToComment" => IntegerSafe($DisallowUserToComment)));

$msg = in_array(false,$success)? "UpdateUnsuccess" : "UpdateSuccess";

intranet_closedb();
header("Location: basic_settings.php?msg=$msg");
?>