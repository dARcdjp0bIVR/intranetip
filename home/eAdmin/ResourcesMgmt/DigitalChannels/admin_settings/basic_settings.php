<?php
// Editing by 
/*
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2014-11-10 (Henry): added option AllowUserToComment
 * 2014-10-09 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalchannels_lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
	header("Location: ../index.php");
	exit;
}

$linterface = new interface_html();
$lduUI = new libdigitalchannels_ui();
$ldu = new libdigitalchannels();
$lgs = new libgeneralsettings();

//$ldu->Authenticate_DigitalArchive();

$settings = $lgs->Get_General_Setting("DigitalChannels", array("'KeepOriginalPhoto'", "'AllowDownloadOriginalPhoto'", "'DisallowUserToComment'"));

# Display Admin Menu
$CurrentPageArr['DigitalChannelsAdminMenu'] = true;

$CurrentPageArr['DigitalChannels'] = 1;
$CurrentPage = "Settings_BasicSetting";

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['BasicSettings'],"",1);

$MODULE_OBJ = $lduUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$lduUI->LAYOUT_START($Msg);

echo $lduUI->Include_JS_CSS();

?>
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_25.css" rel="stylesheet" type="text/css">
<link href="/templates/<?php echo $LAYOUT_SKIN;?>/css/content_30.css" rel="stylesheet" type="text/css">
<script language="javascript" type="text/javascript">
function checkForm()
{
	if($('input[name="KeepOriginalPhoto"]:checked').length == 0){
		return false;
	}
	else if($('input[name="AllowDownloadOriginalPhoto"]:checked').length == 0){
		return false;
	}
	return true;
}

function js_Toggle_View(opt)
{
	if(opt == 2){
		$('.EditView').show();
		$('.DisplayView').hide();
	}else{
		$('.EditView').hide();
		$('.DisplayView').show();
	}
}
</script>
<div id="content" style="padding-left: 10px; width=100%;">
<form name="form1" id="form1" method="POST" action="basic_settings_update.php" onsubmit="return checkForm();">
<div class="table_board">
	<div style="text-align:right; height:20px;"><?=$linterface->Get_Small_Btn($Lang['Btn']['Edit'], "button", "js_Toggle_View(2)", "", "", "DisplayView")?></div>
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><label for="<?=$settings['KeepOriginalPhoto']=='1'?"KeepOriginalPhotoY":"KeepOriginalPhotoN"?>"><?=$Lang['DigitalChannels']['Settings']['KeepOriginalPhoto']?></label></td>
			<td>
				<div class="EditView" style="display:none">
					<?=$linterface->Get_Radio_Button("KeepOriginalPhotoY", "KeepOriginalPhoto", "1", $settings['KeepOriginalPhoto']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0)?>
					<?=$linterface->Get_Radio_Button("KeepOriginalPhotoN", "KeepOriginalPhoto", "0", $settings['KeepOriginalPhoto']!='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0)?>
				</div>
				<div class="DisplayView"><?=$settings['KeepOriginalPhoto']=='1'?$Lang['General']['Yes']:$Lang['General']['No']?></div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><label for="<?=$settings['AllowDownloadOriginalPhoto']=='1'?"AllowDownloadOriginalPhotoY":"AllowDownloadOriginalPhotoN"?>"><?=$Lang['DigitalChannels']['Settings']['AllowDownloadOriginalPhoto']?></label></td>
			<td>
				<div class="EditView" style="display:none">
					<?=$linterface->Get_Radio_Button("AllowDownloadOriginalPhotoY", "AllowDownloadOriginalPhoto", "1", $settings['AllowDownloadOriginalPhoto']=='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0)?>
					<?=$linterface->Get_Radio_Button("AllowDownloadOriginalPhotoN", "AllowDownloadOriginalPhoto", "0", $settings['AllowDownloadOriginalPhoto']!='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0)?>
				</div>
				<div class="DisplayView"><?=$settings['AllowDownloadOriginalPhoto']=='1'?$Lang['General']['Yes']:$Lang['General']['No']?></div>
			</td>
		</tr>
		<tr>
			<td class="field_title"><label for="<?=$settings['DisallowUserToComment']=='1'?"DisallowUserToCommentY":"DisallowUserToCommentN"?>"><?=$Lang['DigitalChannels']['Settings']['AllowUserToComment']?></label></td>
			<td>
				<div class="EditView" style="display:none">
					<?=$linterface->Get_Radio_Button("DisallowUserToCommentY", "DisallowUserToComment", "0", $settings['DisallowUserToComment']!='1', $Class_="", $Lang['General']['Yes'], $Onclick_="",$isDisabled_=0)?>
					<?=$linterface->Get_Radio_Button("DisallowUserToCommentN", "DisallowUserToComment", "1", $settings['DisallowUserToComment']=='1', $Class_="", $Lang['General']['No'], $Onclick_="",$isDisabled_=0)?>
				</div>
				<div class="DisplayView"><?=$settings['DisallowUserToComment']!='1'?$Lang['General']['Yes']:$Lang['General']['No']?></div>
			</td>
		</tr>
	</table>
	<div class="edit_bottom_v30 EditView" style="display:none">
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'],"submit","")?>
		<?='&nbsp;'.$linterface->Get_Action_Btn($Lang['Btn']['Cancel'],"button","js_Toggle_View(1)")?>
	</div>
</div>
</form>
<?php
$lduUI->LAYOUT_STOP();
intranet_closedb();
?>
