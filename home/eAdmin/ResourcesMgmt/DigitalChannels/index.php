<?php
# modifying : Pun
/**************************************************
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2014-10-07 (Henry): file created
 * 
 **************************************************/
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldc = new libdigitalchannels();
$ldcUI = new libdigitalchannels_ui();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: /");
//	exit;
//}
$arrCookies = array();
$arrCookies[] = array("ck_resourcesmanagment_digitalchannels_from_eService","From_eService");
$arrCookies[] = array("ck_resourcesmanagment_digitalchannels_AcademicYearID", "SchoolYear");
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
	$From_eService = (isset($_GET['From_eService']))? $_GET['From_eService'] : $From_eService;
	updateGetCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

if ($From_eService) {
	$CurrentPageArr['eServiceDigitalChannels'] = 1;
}
else{
	$CurrentPageArr['DigitalChannels'] = 1;
}
//$CurrentPage = "PageReports_AdministrativeDocument";

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if($msg!="") {
	if($Lang['General']['ReturnMessage'][$msg]=="") {
		$displayMsg = $msg;	
	} else {
		$displayMsg = $Lang['General']['ReturnMessage'][$msg];	
	}
}

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$home_header_no_EmulateIE7 = true;
$ldcUI->LAYOUT_START($displayMsg);

echo $ldcUI->Include_JS_CSS();

?>
<script type="text/javascript" >
$(function(){	
	jQuery(function($){
		$('.photo_thumb_effect').mosaic({
			animation	:	'slide'
		});
	});
	if (navigator.userAgent.indexOf("Firefox") === -1) {
		$("video").click(function() {
	    	var current_video = $("video").get(0);
	    	if (current_video.paused) {
	    		current_video.play();
	  		}
	  		else {
	    		current_video.pause();
	  		}
		});
	}
});

$(document).ready(function() {
    $('body').addClass('elib_bg bookshelf_type_01  bookshelf_type_01_sky theme_rainbow');
    // document.title += ' > Digital Channels';
	var isOldBrowser = 0;
	if(navigator.appName.indexOf("Internet Explorer")!=-1 && navigator.appVersion.indexOf("MSIE 1")==-1){
		isOldBrowser = 1;
		alert('<?=$Lang['DigitalChannels']['Msg']['BrowserReminder']?>!');
	}
});

function NextPhoto(){
	var photoArray = $(".photo_block");
	for(var i=0; i < photoArray.length; i++){
		if(photoArray.get(i).style.display != 'none'){
			photoArray.get(i).style.display = 'none';
			if(i+1 < photoArray.length){
				photoArray.get(i+1).style.display = '';
			}
			else{
				photoArray.get(0).style.display = '';
			}
			break;
		}
	}
}

function PrevPhoto(){
	var photoArray = $(".photo_block");
	for(var i=0; i < photoArray.length; i++){
		if(photoArray.get(i).style.display != 'none'){
			photoArray.get(i).style.display = 'none';
			if(i-1 >= 0){
				photoArray.get(i-1).style.display = '';
			}
			else{
				photoArray.get(photoArray.length-1).style.display = '';
			}
			break;
		}
	}
}

function Update_Photo_Favorites(photoID){
	$.post('organize/ajax.php?action=enjoyphoto', { PhotoID: photoID },function(res){
		//alert(res);	
		if(res > 0){
			$("#icon_star_"+photoID).addClass('active');
			$("#enjoy_count_"+photoID).text(parseInt($("#enjoy_count_"+photoID).text())+1);
			$('.icon_star').attr('title', '<?=$Lang['DigitalChannels']['Organize']['RemoveEnjoyThisPhoto']?>');
		}
		else{
			$("#icon_star_"+photoID).removeClass('active');
			$("#enjoy_count_"+photoID).text(parseInt($("#enjoy_count_"+photoID).text())-1);
			$('.icon_star').attr('title', '<?=$Lang['DigitalChannels']['Organize']['EnjoyThisPhoto']?>');
		}
	});
}
</script>
    
<div class="digital_channels_content"> <div id="container">
<?echo $ldcUI->Display_Title_Menu();?>
<?echo $ldcUI->Display_Index_Page();?>
</div><p class="spacer"></p></div>

<?
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>