<style>
.fillwidth { 
   width: 100%;
   max-width: none;
   height: auto; 
}
.fillheight { 
   height: 100vh;
   max-width: none;
   width: auto; 
}
#wrapper td {
   vertical-align: middle;
   margin:0;
   padding:0;
   border:0;
}
</style>
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script>
$(function(){

function stretchImg(){
	
    $('body').each(function() {
//    	alert($("#photo_content",window.parent.document.body).height());
//    	alert($(this).find('video').height());
      ($("#photo_content", window.parent.document.body).height() < $(this).find('video').height()) 
        ? $(this).find('video').removeClass('fillwidth').addClass('fillheight')
        : '';
      ($("#photo_content", window.parent.document.body).width() < $(this).find('video').width()) 
        ? $(this).find('video').removeClass('fillheight').addClass('fillwidth')
        : '';
    });
}
stretchImg();
//$( window ).resize(function() {
//    strechImg();
//});
});
</script>
<body style="margin:0;height:100%"><center>
<?if($isvideo){?>
<video style='width:100%;height:100%;' controls>
<source type="video/mp4" src="<?=$url?>"></source>
<!--<source type="video/ogg" src="mov_bbb.ogg"></source>-->
Your browser does not support HTML5 video.
</video>
<?}else{?>
	<table id="wrapper" style="height: 100%;margin:0;padding:0;border:0">
      <tr>
         <td><img style="" src="<?=$url?>" /></td>
      </tr>
   </table>
<?}?>
</center></body>