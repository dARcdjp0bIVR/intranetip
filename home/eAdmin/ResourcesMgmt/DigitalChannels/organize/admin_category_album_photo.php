<?php
// Editing by Pun
/*
 * Change Log:
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2014-10-13 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldcUI = new libdigitalchannels_ui();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
	header("Location: ../index.php");
	exit;
}

if ($AlbumID && !$ldc->isAlbumEditable($AlbumID)){
	header("Location: ../index.php");
	exit;
}

if (!$AlbumID && !$ldc->isAlbumNewable()){
	header("Location: ../index.php");
	exit;
}

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['Category'],"",1);

$CurrentPage = "Organize";

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$home_header_no_EmulateIE7 = true;
$ldcUI->LAYOUT_START($Msg);

echo $ldcUI->Include_JS_CSS();
//debug_pr($CategoryCode);
?>
<script type="text/javascript" src="../script/photo_thumb.js"></script>  
<script>
$(function(){		
	jQuery(function($){
		$('.photo_thumb_effect').mosaic({
			animation	:	'slide'
		});
	});
});
</script>	    
		
<div class="digital_channels_content"> <div id="container">
<?echo $ldcUI->Display_Title_Menu();?>
<?echo $ldcUI->Display_Category_Album_Photo($AlbumID);?>         
</div></div>
<?php
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>