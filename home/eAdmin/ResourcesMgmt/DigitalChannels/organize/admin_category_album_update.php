<?php
// Editing by  
/*
 * 2019-03-08 (Cameron)
 * - add specific user handling in target group 
 * 
 * 2019-01-08 (Cameron) 
 *  - add these fields: chiTitle, chiDescription, albumCode
 *  - pass chiEventTitle to updateAlbumEvent()
 * 
 * 2018-03-14 (Cameron): add updateAlbumEvent() for QuickAdd
 *
 * 2014-10-13 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT . "includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
// header("Location: ../index.php");
// exit;
// }

$linterface = new interface_html();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0) {
    header("Location: ../index.php");
    exit();
}

if ($AlbumID && ! $ldc->isAlbumEditable($AlbumID)) {
    header("Location: ../index.php");
    exit();
}

if (! $AlbumID && ! $ldc->isAlbumNewable()) {
    header("Location: ../index.php");
    exit();
}

// debug_pr($_POST);
// debug_pr($_REQUEST);

$params = array(
    'CategoryCode' => $CategoryCode,
    'title' => htmlspecialchars($title),
    'description' => htmlspecialchars($description),
    'cover_photo_id' => $cover_photo_id,
    'place' => htmlspecialchars($place),
    'date' => $AccessDate,
    'since' => $StartDate,
    'until' => $EndDate,
    'album_id' => ($AlbumID ? $AlbumID : $album_id),
    'chiTitle' => standardizeFormPostValue($chiTitle),
    'chiDescription' => standardizeFormPostValue($chiDescription),
    'albumCode' => standardizeFormPostValue($albumCode)
);

if (! $AlbumID) {
    $result = $ldc->Add_Album($params);
    $AlbumID = $result;
} else {
    $result = $ldc->Update_Album($params);
}

$ldc->updateAlbumPICs($PIC, $AlbumID);

switch($share_to) {
    case '':
        $select_groups = array();
        $select_users = array();
        break;
    case 'all':
        $select_groups = array();
        $select_users = array();
        break;
    case 'groups':
        $select_users = array();
        break;
    case 'users':
        $select_groups = array();
        foreach((array)$select_users as $index=>$_userID) {
            $select_users[$index] = str_replace('U','',$_userID);
        }
        break;
}

$select_groups = $share_to ? $select_groups : array();
$ldc->resetAlbumUsersGroups($share_to == 'all', $select_groups, $select_users, $AlbumID);

if ($_POST['isQuickAdd']) {
    $ldc->updateAlbumEvent($_POST['photo_ids'], standardizeFormPostValue($_POST['eventTitle']), $_POST['eventDate'], standardizeFormPostValue($_POST['chiEventTitle']));
}
$msg = ($result > 0 ? "UpdateSuccess" : "UpdateUnsuccess");

intranet_closedb();
header("Location: admin_category_album_photo.php?AlbumID=$AlbumID&msg=$msg");
?>