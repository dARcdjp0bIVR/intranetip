
var app = angular.module("DigiCh", []);
var digich_scope;

var current_video_id = -1, current_tab = "get_video", current_cat = '';
var preview_video_id = -1;
var preview_inaction=false;
app.controller("dcCtrl", function($scope) {
    $scope.naviarr = []; 
    $scope.videoarr = [];
    $scope.albumarr = [];
    $scope.categoryarr = [];
    $scope.selected_video_arr = [];
    $scope.edit_checked = false;    
});

$(document).ready(function(){	

	digich_scope = angular.element($("#video_container")).scope();
	attach_action();
	get("");
	
	$(".tab").on("click tap", function(){

		if($(".tab .tab_current").length == 0){
			var current = this;
			
			$(".tab_current").removeClass("tab_current", 200, "easeInBack", function(){
				$(current).toggleClass("tab_current", 500, "easeInBack");
			});
		}
		current_tab = $(this).attr("get");
		clear_selected_video();
		get();
	})
});

function get(search){
	 digich_scope.$apply(function(){	    	//video_title
 		digich_scope.naviarr = [];
 		digich_scope.categoryarr  = [];
 	});
	$("#cat_box").css("display", "none");
	$(".btn_feature_current").toggleClass("btn_feature_current");
	
	current_cat = '';
	current_tab = 'get_album';
	switch (current_tab){
		case "get_video":
			 get_video(search);
			break;
		case "get_album":
			$("#cat_box").css("display", "block");
			get_category();
			get_album(search);
			break;
		case "get_favorite":
			get_favorite(search);
			break;
	}
	
}

function clear_selected_video(){
	 digich_scope.$apply(function(){	    	
 		digich_scope.selected_video_arr = [];
 		$(".photo_thumb_current").removeClass("photo_thumb_current");
 	});
}

function get_album(search){
	$.ajax({
	    url: '/api/get_digitalchannels_video.php',
	    type: 'POST',    
	    data: {
	    	"action" : "get_album",
	    	"keyword" : search,
	    	"orderby" : $("#orderby").val()
	    },
	    error: function(xhr) {
	      alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {	    	
	    	
	    	var unpackArr = jQuery.parseJSON( response );
	    	
	    	for(var i = 0; i<unpackArr.length; i++){
	    		if(unpackArr[i]["Title"] == ""){
	    			unpackArr[i]["Title"] = untitled_album_wording;
	    		}
	    		var d = new Date(unpackArr[i]["DateModified"]);
	    	    var n = d.getTime();
	    	    unpackArr[i]["DateModified"] = time_diff(n);
	    	}
	    	
	    	$( ".container" ).animate({
	    		   "opacity" : "0"
	    		  }, 200, function() {
	    			  $( ".container" ).css("display", "none");
	    			  digich_scope.$apply(function(){	    	//video_title
	    				  	if(current_cat == ""){
		    		    		digich_scope.albumarr =unpackArr;
	    				  	}else{	    				  		
	    				  		digich_scope.albumarr = [];
	    				  		for(var i = 0; i < unpackArr.length; i++){
	    				  			if(unpackArr[i]["CategoryCode"] == current_cat){
	    				  				digich_scope.albumarr.push(unpackArr[i]);
	    				  			}
	    				  		}
	    				  	}
	    				  	digich_scope.naviarr = [{'name' : album_wording, 'category' : current_cat , 'folder' : ''}];
	    		    	});
	    			  
	    			  $( "#album_container" ).css("display", "block");
	    			  $( "#album_container" ).animate({
	   	    		   "opacity" : "1"
	   	    		  }, 200, function() {	   			  

	   	    			  attach_action();
	   	    		});
	    		});
	    	
	    	
	    }
  });
}

function set_selected_video(){
	for(var j =0; j < digich_scope.videoarr.length; j++){		 
		  if(edit_video_code != '' && digich_scope.videoarr[j]["id"] == edit_video_code){
			  if(school_code == edit_school_code){				  
				  digich_scope.$apply(function(){	    	//video_title	
					    digich_scope.edit_checked = true;
			    		digich_scope.videoarr[j]["current"] = "photo_thumb_current";
			    		digich_scope.selected_video_arr.push(digich_scope.videoarr[j]);
			    	});				  
			  }
			  edit_video_code = '';
			  break;
		  }else{
			  for(var i =0; i < digich_scope.selected_video_arr.length; i++){			  
				  if(digich_scope.selected_video_arr[i]["id"] == digich_scope.videoarr[j]["id"]){
						digich_scope.$apply(function(){	    	//video_title		    							
				    		digich_scope.videoarr[j]["current"] = "photo_thumb_current";
				    	});
					}
			  }	  
		  }		  
	}
}

function get_video(search,albumid){
	$.ajax({
	    url: '/api/get_digitalchannels_video.php',
	    type: 'POST',    
	    data: {
	    	"action" : "get_video",
	    	"keyword" : search,
	    	"orderby" : $("#orderby").val(),
	    	"albumid" : albumid
	    },
	    error: function(xhr) {
	      alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {	    	
	    	var unpackArr = jQuery.parseJSON( response );
	    	
	    	$( ".container" ).animate({
	    		   "opacity" : "0"
	    		  }, 200, function() {
	    			  $( ".container" ).css("display", "none");
	    			  digich_scope.$apply(function(){	    	//video_title
	    		    		digich_scope.videoarr =unpackArr;
	    		    	});
	    			  set_selected_video();
	    			  
	    			  $( "#video_container" ).css("display", "block");
	    			  $( "#video_container" ).animate({
	   	    		   "opacity" : "1"
	   	    		  }, 200, function() {	   			  

	   	    			  attach_action();
	   	    		});
	    		});
	    	
	    	
	    }
  });
}

function get_category(){
	$.ajax({
	    url: '/api/get_digitalchannels_video.php',
	    type: 'POST',    
	    data: {
	    	"action" : "get_category"
	    },
	    error: function(xhr) {
	      alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {	    	
	    	var unpackArr = jQuery.parseJSON( response );
			 
	    	digich_scope.$apply(function(){	    	//video_title
	    		digich_scope.categoryarr =unpackArr;
	    	});	    	
	    }
  });
}

function get_favorite(search){
	$.ajax({
	    url: '/api/get_digitalchannels_video.php',
	    type: 'POST',    
	    data: {
	    	"action" : "get_favorite",
	    	"keyword" : search,
	    	"orderby" : $("#orderby").val()
	    },
	    error: function(xhr) {
	      alert('Ajax request 發生錯誤');
	    },
	    success: function(response) {	    	
	    	var unpackArr = jQuery.parseJSON( response );
	    	
	    	
	    	$( ".container" ).animate({
	    		   "opacity" : "0"
	    		  }, 200, function() {
	    			  $( ".container" ).css("display", "none");
	    			  digich_scope.$apply(function(){	    	//video_title
	    		    		digich_scope.videoarr =unpackArr;
	    		    	});
	    			  set_selected_video();
	    			  
	    			  $( "#video_container" ).css("display", "block");
	    			  $( "#video_container" ).animate({
	   	    		   "opacity" : "1"
	   	    		  }, 200, function() {	   		
	   	    			  attach_action();
	   	    		});
	    		});	    	
	    }
  });
}

function attach_action(){
	$('.photo_thumb_effect').mosaic({
		animation	:	'slide'
	});
	
	$('#search_btn').off('click tap').on('click tap', function(){
		get($("#search_text").val());
	});
	
	$( "#search_text" ).off("keydown").on("keydown",function(e) {
	  var k = e.keyCode || e.charCode;
	  if(k== 13){
		  get($("#search_text").val());
	  }
	});
	
	$('.btn_feature').off('click tap').on('click tap', function(){
		if(preview_inaction){
			return false;
		}
		preview_inaction = true;
		
		$('.btn_feature_current').toggleClass("btn_feature_current");
		
		var current_id = $(this).attr("value");
		
		var current_offsetTop = $("#video_"+current_id)[0]["offsetTop"];
		var current = $("#video_"+current_id);
		var i = 0, isLastRow = false;
		console.log("A");
		var currentElementSibling =  $("#video_"+current_id);
		var lastElement = "";
		while(true){
			i++;
			nextElementSibling = $(currentElementSibling[0]["nextElementSibling"]);
			if(nextElementSibling[0] == undefined){
				lastElement = currentElementSibling;
				isLastRow = true;
				break;
			}
			var nextOffsetTop = nextElementSibling[0]["offsetTop"];
			if(nextOffsetTop != current_offsetTop){
				break;
			}else{
				currentElementSibling = nextElementSibling;
			}
			if(i>3)break;
		}
		i = 0;
		if(isLastRow){
			while(true){
				i++;
				previousElementSibling = $(currentElementSibling[0]["previousElementSibling"]);
				if(previousElementSibling[0] == undefined){
					currentElementSibling = lastElement;
					break;
				}
				
				var previousOffsetTop = previousElementSibling[0]["offsetTop"];
				currentElementSibling = previousElementSibling;
				if(previousOffsetTop != current_offsetTop || i>3){
					break;
				}
			}
		}
		
		$("#photo_thumb_list > ul > .preview_video").remove();
		
		if(preview_video_id == current_id){
			preview_inaction = false;
			preview_video_id = -1;
			return false;
		}else{
			$(this).toggleClass("btn_feature_current");
		}
		var clone_preview_video = $(".preview_video").clone();
		currentElementSibling.after(clone_preview_video);		
		var current_preview = $(currentElementSibling[0]["nextElementSibling"]);
		
		preview_video_id = current_id;
		
//		$("#photo_thumb_list").scrollTo( clone_preview_video ,800, {offset: function() { return {top:-100}; }} );
		
		setTimeout(function(){
			$("#photo_thumb_list").scrollTo( clone_preview_video ,{  offset: -50, duration: 1000});
			
			current_preview.toggleClass("current_preview_video");		
			var OriginalFilePath = "";
			for(var j =0; j < digich_scope.videoarr.length; j++){
				if(current_id == digich_scope.videoarr[j]["id"]){
					OriginalFilePath = digich_scope.videoarr[j]["OriginalFilePath"];
					break;
				}
			}		    	
	    	
			current_preview.animate({
	 		   "height" : "284px",
	 		   "opacity" : 1
			  }, {
			    duration: 500,
			    specialEasing: {
			      height: "easeInQuart"
			    },
			    complete: function() {
					preview_inaction = false;
			    	$(".current_preview_video video source").attr("src", "http://192.168.0.146:31002"+OriginalFilePath);
			    	$(".current_preview_video video")[0].load();
			    }
			  });
		},100);

	});
		
	$('.photo_thumb_info').off('click tap').on('click tap', function(){
		
		var current = this;		
//		$(".photo_thumb_block").removeClass("photo_thumb_current", 200, "easeInBack", function(){});
		if(is_edit){
			digich_scope.selected_video_arr = [];
			
			for(var j =0; j < digich_scope.videoarr.length; j++){
				if($(current).attr("id") == digich_scope.videoarr[j]["id"]){
					digich_scope.$apply(function(){	    	//video_title
						var current_video_obj = digich_scope.videoarr[j];
						current_video_obj["temp_index"] = j;
			    		digich_scope.selected_video_arr.push(digich_scope.videoarr[j]);
			    		digich_scope.videoarr[j]["current"] = "photo_thumb_current";
			    	});
				}else{
					digich_scope.$apply(function(){	
						digich_scope.videoarr[j]["current"] = "";
					});
				}
			}			
		}else{
			if($(current).parent().hasClass("photo_thumb_current")){
				for(var j =0; j < digich_scope.selected_video_arr.length; j++){
					if($(current).attr("id") == digich_scope.selected_video_arr[j]["id"]){					
						digich_scope.$apply(function(){	    	//video_title
							var temp_index = digich_scope.selected_video_arr[j]["temp_index"];
				    		digich_scope.videoarr[temp_index]["current"] = "";
				    		digich_scope.selected_video_arr.splice(j, 1);
				    	});
					}
				}				
			}else{
				for(var j =0; j < digich_scope.videoarr.length; j++){
					if($(current).attr("id") == digich_scope.videoarr[j]["id"]){
						digich_scope.$apply(function(){	    	//video_title
							var current_video_obj = digich_scope.videoarr[j];
							current_video_obj["temp_index"] = j;
				    		digich_scope.selected_video_arr.push(digich_scope.videoarr[j]);
				    		digich_scope.videoarr[j]["current"] = "photo_thumb_current";
				    	});
					}
				}
			}		
		}
		
//		$(current).toggleClass("photo_thumb_current", 500, "easeInBack");
		current_video_id = $(this).attr("id");
	});
	
	$(".album").off('click tap').on('click tap', function(){	
		
		var current = this;		
		get_video("",$(this).attr("id"));

		digich_scope.$apply(function(){	    	//video_title
			digich_scope.naviarr = [];
		  	digich_scope.naviarr.push({'name' : album_wording , 'category' : current_cat , 'folder' : ''});
		  	digich_scope.naviarr.push({'name' : $(current).attr("title"), 'category' : current_cat , 'folder' : ''});
		  	
    	});
	});
	
	$('#orderby').off('change').on('change', function(){
		get();
	});
	
	$('#category').off('change').on('change', function(){
		
		current_cat = $(this).val();
		get_album();
	});
	
	$('.navi').off('click tap').on('click tap', function(){	
		
		var current = this;		
		if($(this).attr("folder") == ""){			
			get_album();
		}
		
	});
}

function time_diff(unix){
	var d = new Date();
	var now = d.getTime();
    var difference = (now - unix)/1000;
    var message = "";
        
    if (difference < 10)
    {
        message = just_wording;
    }
    else if (difference < 60)
    {
        message = a_few_seconds_ago_wording;
    }
    else if (difference < 60 * 2) {
        message = a_minute_ago_wording;
    }
    else if (difference < 60 * 60) {
        message = parseInt(difference / 60) + minutes_ago_wording;
    }
    else  if (difference < 60 * 60 * 2) {
    	message = hour_ago_wording;
    }
    else  if (difference < 60 * 60 * 24) {
    	message = parseInt(difference / 60 / 60 ) + hours_ago_wording;
    }else if (difference < 60 * 60 * 24 * 365){
    	message = parseInt(difference / 60 / 60 /24) + day_ago_wording;
    }else{
    	message = parseInt(difference / 60 / 60 /24 /365) + year_ago_wording;
    }
    
    return message;
}