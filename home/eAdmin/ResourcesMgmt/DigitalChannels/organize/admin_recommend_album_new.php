<?php
// Editing by Pun
/*
 * Change Log:
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2015-02-26 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldcUI = new libdigitalchannels_ui();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

if ($RecommendID && count($ldc->Get_Recommend_Album($RecommendID)) == 0){
	header("Location: ../index.php");
	exit;
}

if (!$RecommendID && !$ldc->isAlbumNewable()){
	header("Location: ../index.php");
	exit;
}

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['Category'],"",1);

$CurrentPage = "Recommend";

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$home_header_no_EmulateIE7 = true;
$ldcUI->LAYOUT_START($Msg);

echo $ldcUI->Include_JS_CSS();
//debug_pr($AlbumID);
?>
<link type="text/css" rel="stylesheet" media="screen" href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript" src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript" src="/templates/jquery/plupload/plupload.full.js"></script>
<script>
$(function(){
KIS_UPLOADER_URL = '/templates/jquery/plupload/';
KIS_UPLOADER_DEFAULT_PARAMS = {
    runtimes : 'gears,html5,flash,silverlight,browserplus',
    browse_button : 'uploader_button',
    multipart_params: {},
    multi_selection: true,
    drop_element: 'attach_file_area',
    max_file_size: '500mb',
    url : '.',
    flash_swf_url : KIS_UPLOADER_URL+'plupload.flash.swf',
    silverlight_xap_url : KIS_UPLOADER_URL+'plupload.silverlight.xap',
};

kis = {uploaders	: [],uploader: function(params){
	
	params = $.extend({}, KIS_UPLOADER_DEFAULT_PARAMS, params);  
	var uploader = new plupload.Uploader(params);
	
	$('#'+params.drop_element).append($('.attach_file_overlay').clone())
	
	uploader.bind('Init', function(up, init_params) {
	    
	    if (init_params.runtime=='html5'){
		
		var hide_interval; 
		$('#'+params.drop_element).on('dragover dragenter',function(e){
			//alert("testing");
		    $(this).find('.attach_file_overlay').show();
		    clearTimeout(hide_interval);
		    return false;
		}).on('dragleave dragend drop',function(e){
		    hide_interval = setTimeout(function(){
			$('#'+params.drop_element+' .attach_file_overlay').hide();
		    },500);
		    
		    return false;
		});
	    }else{
		$('#'+params.drop_element+' .attach_file_overlay').hide();
	    }
	});
	
	uploader.init();
	
	if (params.start_button != null){
	    $('#'+params.startButton).click(function(){
		uploader.start();
	    });
	}
	
	uploader.bind('FilesAdded', function(up, files) {
	    $('#'+params.drop_element+' .attach_file_overlay').hide();
	});
	if (params.auto_start){
	    uploader.bind('FilesAdded', function(up, files) {
		up.refresh(); 
		this.start();
	    });
	}
	uploader.bind('FilesAdded', params.onFilesAdded);
	uploader.bind('UploadProgress', params.onUploadProgress);
	uploader.bind('FileUploaded', params.onFileUploaded);
	uploader.bind('Error', params.onError);
	
	this.uploaders.push(uploader);
	return uploader;
    }};
    
    kis.album = {
    
    albums_init: function(){
	$('#my_album_only').click(function(){
	    $(this).closest('form').submit();
	    
	    
	})
	$('.album_list').masonry();
    },
    album_form_init: function(lang, options, targets){
	
	var uploading_files_count = 0;
	var saveAlbum = function(callback){
//	    $.post('ajax.php?action=setalbum', $('.newalbum').serialize(),function(res){
//		//kis.unlock();
//		$('.newalbum input[name="AlbumID"]').val(res.album_id);
//		$('#span_current_target').html(targets[res.share_to]);
//		$('.album_form_remove').fadeIn();
//		//kis.setNavigationItems([res.album_title]);
//		if (typeof(callback)=='function') callback();
//		//alert('callback');
//	    },'json');
	}
	
	var savePhotoOrder = function(){
		$('#NeedUpdatePhoto').val('1');
		//alert('savePhotoOrder');
	    var photo_ids = $('.edit_photo_list .photo_ids').map(function(){return $(this).val()}).get();
	    var album_id =$('.newalbum input[name="AlbumID"]').val();
	    //$.post('ajax.php?action=reorderphotos', {photo_ids: photo_ids, album_id: album_id});
	}
	
	$('.edit_photo_list ul').sortable({
	    containment: "#container",
	    zIndex: '7010',
	    update: savePhotoOrder,
	    opacity: 0.8,
	    items: '>li.uploaded'
	});
	
	var title = $('.album_title').text();;
	if (title != ''){
	    kis.setNavigationItems([title]);
	}

	if (options.photo_count==0){
	    $('.filter_ordering').hide();
	    $('.edit_photo_list .attach_file_overlay').show();
	    
	}
	
	$('.album_form :input').change(saveAlbum);
    
	$('.edit_photo_list').on('change','.input_desc', function(){
	
	    var photo_id = $(this).siblings('.photo_ids').val();
	    var album_id = $('.newalbum input[name="AlbumID"]').val();
//	    $.post('ajax.php?action=updatephotodescription', {description: $(this).val(), photo_id: photo_id, album_id: album_id},function(){
//		//kis.unlock();
//		
//	    });
    
	});
		
	$('.edit_bottom .formbutton').click(function(){
	    
	    //var album_id = $('.newalbum input[name="AlbumID"]').val();
	    //$.address.value(album_id? '/apps/album/'+album_id+'/': '/apps/album/');
 
	});
	
	$('.filter_ordering a').click(function(){
	    
	    var sort_field = $(this).attr('href').replace('#','');
	    var to_int = $(this).hasClass('int')
	    var order;
	    
	    if ($(this).hasClass('order_asc')){
		order = -1;
		$(this).removeClass('order_asc').addClass('order_dec');
	    }else{
		order = 1;
		$(this).removeClass('order_dec').addClass('order_asc');
	    }
	    
	    $(this).siblings().removeClass('order_dec order_asc');
	   
	    
	    var items = $('.edit_photo_list li.uploaded').sort(function(a, b){
		
		a = $(a).find('.'+sort_field).val().toLowerCase();
		b = $(b).find('.'+sort_field).val().toLowerCase();
		
		if (to_int){
		    a = parseInt(a);
		    b = parseInt(b);
		}
		
		if(a > b){ return 1*order; }else if(a < b){ return -1*order; }else{ return 0;}

	    });
	    
	    $('.edit_photo_list ul').html(items);
	    savePhotoOrder();
	    
	    return false;
	    
	});
	
	$('.edit_photo_list').on('click', 'li.uploaded .copy_dim', function(){
	    
	    if (confirm(lang.areyousureto+$(this).attr('title')+'?')){
		
		var photo_id = $(this).parent().siblings('.photo_ids').val();
		var album_id = $('.newalbum input[name="AlbumID"]').val();
		
		//$.post('ajax.php?action=updatecoverphoto', {photo_id: photo_id, album_id: album_id});
	
	    }
  
	    return false;
	});
	
	$('.edit_photo_list').on('click', 'li.uploaded .delete_dim', function(){
	    
	    if (confirm(lang.areyousureto+$(this).attr('title')+'?')){
	    $('#NeedUpdatePhoto').val('1');
		var photo_id = $(this).parent().siblings('.photo_ids').val();
		var album_id =$('.newalbum input[name="AlbumID"]').val();
		
		$(this).closest('li').fadeOut(function(){$(this).remove();});
//		$.post('ajax.php?action=removephoto', {photo_id: photo_id, album_id: album_id}, function( data ) {
//			savePhotoOrder();
//		});
//		savePhotoOrder();
	    }
	    
	    return false;
	});
	
	$('.album_form_remove').click(function(){
	   
	    if (confirm('<?=$Lang['DigitalChannels']['Msg']['RemoveAlbum']?>')){
	    	$('#form1').attr('action', "admin_recommend_album_remove.php");
  			$('#form1').submit();
	    }
	    
	    return false;
	    
	});
	
	$('.btn_select_ppl').click(function(){
	    
	    $('.album_form_date_access_select').fadeToggle();
	
	    return false;
	});
	
	$('#share_to_all').click(function(){

	    $('.select_date').fadeIn();
	    $('#select_group_lists').hide();
		$('#select_group_lists select').prop('disabled', true);
		$('#select_group_lists input').prop('disabled', true);
	});
	
	$('#share_to_myself').click(function(){
	    
	    $('.select_date').hide();
	    $('#select_group_lists').hide();
		$('#select_group_lists select').prop('disabled', true);
		$('#select_group_lists input').prop('disabled', true);
	});
	
	$('#share_to_groups').click(function(){
	    
	    $('.select_date').fadeIn();
	    $('#select_group_lists').fadeIn();
		$('#select_group_lists select').prop('disabled', false);
		$('#select_group_lists input').prop('disabled', false);
	});
	
	
    },
    album_init:function(){
	   
	var title = $('.album_title').text();
	
	kis.setNavigationItems([title]);
	$('.photo_thumb_list ul').masonry();

	
    }
    
}

    kis.album.album_form_init({
	areyousureto : '<?=$Lang['DigitalChannels']['Msg']['AreYouSureTo']?>',
	allphotoswillberemoved: '<?=$Lang['DigitalChannels']['Msg']['AllPhotosWillBeRemoved']?>!',
	stopuploadingphotos: '<?=$Lang['DigitalChannels']['Msg']['StopUploadingPhotos']?>'		      
    },{photo_count: <?=sizeof($photos)?>},{all:'<?=$Lang['DigitalChannels']['Organize']['AllUsers']?>',groups:'<?=$Lang['DigitalChannels']['Organize']['Groups']?>',myself:'<?=$Lang['DigitalChannels']['Organize']['Private']?>'});
});

function checkForm()
{
	// Checking Start Date and End Date
	var start = $('#StartDate').val();
	var end = $('#EndDate').val();
	var noChecking = start.trim() == "" || end.trim() == "";
	if(!noChecking && compareDate(start, end) === 1 && $('#form1').attr('action') == "admin_recommend_album_update.php"){
		alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
		return false;
	}
	
//	if($("#overlay").length > 0 && !confirm('<?=$Lang['DigitalChannels']['Remarks']['DiscardUploadMedia']?>')){
//		kis.abort();
//		return false;
//	}
	
	return true;
}

function cancelCheck(recommendID){
		if(recommendID){
			window.location='admin_recommend_album_photo.php?RecommendID='+recommendID;
		}
		else{
			window.location='admin_recommend_album.php';
		}
}

</script>

<div class="digital_channels_content"> <div id="container">
<?echo $ldcUI->Display_Title_Menu();?>
<?echo $ldcUI->Display_Recommend_Album_New($RecommendID);?>         
</div></div>
<script type="text/javascript">
$(document).ready( function() {
	$('input#dynSizeThickboxLink').click( function() {
		load_dyn_size_thickbox_ip('', 'onloadThickBox();');
	});
	
	$('input#fixedSizeThickboxLink').click( function() {
		load_dyn_size_thickbox_ip('', 'onloadThickBox();', inlineID='', defaultHeight=600, defaultWidth=800);
	});
});

function onloadThickBox() {
//	$.post(
//		"thickbox_content.php", 
//		{ 
//			academicYearId: 1,
//			yearTermId: 2
//		},
//		function(ReturnData) {
//			$('div#TB_ajaxContent').html(ReturnData);
//			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
//		}
//	);
	
	$('div#TB_ajaxContent').load(
		"/home/eAdmin/ResourcesMgmt/DigitalChannels/organize/admin_recommend_photo_selection.php", 
		function(ReturnData) {
			adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
		}
	);
}
</script>
<?php
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>