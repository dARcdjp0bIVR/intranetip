<?php
// Editing by 
/*
 * Change Log:
 * 2017-01-04 (Henry):
 * 		- support category pic
 * 2015-11-25 (Pun):
 * 		- Added force encoding to UTF-8 (for EJ)
 * 2014-10-13 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

$ldc = new libdigitalchannels();

if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels'] && count($ldc->Get_Albums_By_Pic($UserID)) <= 0 && !$ldc->isCategoryPic()) {
	header("Location: ../index.php");
	exit;
}

$linterface = new interface_html();
$ldcUI = new libdigitalchannels_ui();

$arrCookies = array();
$arrCookies[] = array("ck_resourcesmanagment_digitalchannels_AcademicYearID", "SchoolYear");		
if(isset($clearCoo) && $clearCoo == 1) {
	clearCookies($arrCookies);
}
else {
	updateGetCookies($arrCookies);
}

$CurrentPageArr['DigitalChannels'] = 1;

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['Category'],"",1);
$CurrentPage = "Organize";
$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
} 

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$home_header_no_EmulateIE7 = true;
$ldcUI->LAYOUT_START($Msg);

echo $ldcUI->Include_JS_CSS();

//$categoryInfoAry = $ldc->Get_Category();
//$numOfCategory = count($categoryInfoAry);

?>
<div class="digital_channels_content"> <div id="container">
<?echo $ldcUI->Display_Title_Menu(array(),true);?>
<?echo $ldcUI->Display_Category_Thumbnail($SchoolYear);?>         
</div></div>
<?php
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>