<?php
// Editing by Henry
/*
 * 2014-10-30 (Bill): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldc = new libdigitalchannels();
$ldcUI = new libdigitalchannels_ui();

$CurrentPageArr['DigitalChannels'] = 1;

if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
	header("Location: ../index.php");
	exit;
}

if ($AlbumID && !$ldc->isAlbumEditable($AlbumID)){
	header("Location: ../index.php");
	exit;
}

if (!$AlbumID && !$ldc->isAlbumNewable()){
	header("Location: ../index.php");
	exit;
}

# Display tab pages
$TAGS_OBJ[] = array($Lang['DigitalChannels']['Settings']['Category'],"",1);

$CurrentPage = "Organize";

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])){
	$Msg = $Lang['General']['ReturnMessage'][$msg];
}
$home_header_no_EmulateIE7 = true;

//increase the view number
$ldc->Add_Album_Photo_View($AlbumPhotoID);
?>
<style>
a.view_images_nav_next {
    right: 0px;
    left: auto;
    background-position: 4px -151px;
}
a{
	position:absolute;
	top:20px;
	left:0px;
	border:0;
     background: url(/images/2009a/digital_channels/view/view.png) no-repeat 0 -85px;
	width:28px;
	height:32px;
	text-indent:-9000px;
	cursor:pointer;
	opacity:0.7;
	filter: alpha(opacity=70);
	outline:none;
	
}
a:hover{
	opacity:1;
	filter: alpha(opacity=100);
}
</style>	    
<?echo $ldcUI->Display_Category_Album_Photo_View_Control($AlbumID, $AlbumPhotoID, false, $RecommendID);?>
         
<?php
intranet_closedb();
?>