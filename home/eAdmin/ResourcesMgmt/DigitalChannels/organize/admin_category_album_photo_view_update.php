<?php
// Editing by Henry
/*
 * 2014-10-13 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
	header("Location: ../index.php");
	exit;
}

if ($AlbumID && !$ldc->isAlbumEditable($AlbumID)){
	header("Location: ../index.php");
	exit;
}

if (!$AlbumID && !$ldc->isAlbumNewable()){
	header("Location: ../index.php");
	exit;
}

//debug_pr($_POST);
//debug_pr($_REQUEST);

if($hidPhotoID && $txtComment !=''){
	$result = $ldc->Add_Album_Photo_Comment($hidPhotoID, $txtComment);
}

$msg = ($result > 0? "UpdateSuccess" : "UpdateUnsuccess");

intranet_closedb();
if($ViewMode)
	header("Location: ../categories/category_album_photo_view.php?AlbumID=".$AlbumID."&AlbumPhotoID=".$hidPhotoID."&msg=".$msg);
else
	header("Location: admin_category_album_photo_view.php?AlbumID=".$AlbumID."&AlbumPhotoID=".$hidPhotoID."&msg=".$msg);
?>