<?php
// Editing by 
/*
 * Change Log:
 * 
 * 2019-04-03 (Henry)
 * - bug fix for Record updated message appear if click Cancel when create new album
 * 
 * 2019-03-08 (Cameron)
 * - add specific user handling in target group
 * 
 * 2019-01-08 (Cameron)
 * - add event handling for chiEventTitle and chiDescription
 *  
 * 2018-03-09 (Cameron)
 * - add two fiels: EventDate and EventTitle
 * - add field change event handler (updateEventDate) for the above fields
 *
 * 2017-01-04 (Henry):
 * - support category pic
 * 2016-02-25 (Pun):
 * - Added onError() to the uploader, for file size ecxceed checking
 * 2015-11-25 (Pun):
 * - Added force encoding to UTF-8 (for EJ)
 * 2014-10-13 (Henry): file created
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT . "includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "lang/digitalchannels_lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
// header("Location: ../index.php");
// exit;
// }

$linterface = new interface_html();
$ldcUI = new libdigitalchannels_ui();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0) {
    header("Location: ../index.php");
    exit();
}

if ($AlbumID && ! $ldc->isAlbumEditable($AlbumID)) {
    header("Location: ../index.php");
    exit();
}

if (! $AlbumID && ! $ldc->isAlbumNewable($CategoryCode)) {
    header("Location: ../index.php");
    exit();
}

// Display tab pages
$TAGS_OBJ[] = array(
    $Lang['DigitalChannels']['Settings']['Category'],
    "",
    1
);

$CurrentPage = "Organize";

$MODULE_OBJ = $ldcUI->GET_MODULE_OBJ_ARR();

if ($msg != '' && isset($Lang['General']['ReturnMessage'][$msg])) {
    $Msg = $Lang['General']['ReturnMessage'][$msg];
}

$g_encoding_unicode = true; // Force encoding to UTF-8 (for EJ)
$home_header_no_EmulateIE7 = true;
$ldcUI->LAYOUT_START($Msg);

echo $ldcUI->Include_JS_CSS();
// debug_pr($AlbumID);
?>
<link type="text/css" rel="stylesheet" media="screen"
	href="/templates/jquery/ui-1.9.2/jquery-ui-1.9.2.custom.min.css">
<script type="text/javascript"
	src="/templates/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript"
	src="/templates/jquery/ui-1.9.2/jquery-ui.custom.min.js"></script>
<script type="text/javascript"
	src="/templates/jquery/plupload/plupload.full.js"></script>
<script>
$(function(){
KIS_UPLOADER_URL = '/templates/jquery/plupload/';
KIS_UPLOADER_DEFAULT_PARAMS = {
    runtimes : 'gears,html5,flash,silverlight,browserplus',
    browse_button : 'uploader_button',
    multipart_params: {},
    multi_selection: true,
    drop_element: 'attach_file_area',
    <?if(libdigitalchannels::$max_upload_size > 0 ){?>
    max_file_size: '<?=libdigitalchannels::$max_upload_size?>mb',
    <?}?>
    url : '.',
    flash_swf_url : KIS_UPLOADER_URL+'plupload.flash.swf',
    silverlight_xap_url : KIS_UPLOADER_URL+'plupload.silverlight.xap',
};

kis = {uploaders	: [],uploader: function(params){
	
	params = $.extend({}, KIS_UPLOADER_DEFAULT_PARAMS, params);  
	var uploader = new plupload.Uploader(params);
	
	$('#'+params.drop_element).append($('.attach_file_overlay').clone())
	
	uploader.bind('Init', function(up, init_params) {
	    
	    if (init_params.runtime=='html5'){
		
		var hide_interval; 
		$('#'+params.drop_element).on('dragover dragenter',function(e){
			//alert("testing");
		    $(this).find('.attach_file_overlay').show();
		    clearTimeout(hide_interval);
		    return false;
		}).on('dragleave dragend drop',function(e){
		    hide_interval = setTimeout(function(){
			$('#'+params.drop_element+' .attach_file_overlay').hide();
		    },500);
		    
		    return false;
		});
	    }else{
		$('#'+params.drop_element+' .attach_file_overlay').hide();
	    }
	});
	
	uploader.init();
	
	if (params.start_button != null){
	    $('#'+params.startButton).click(function(){
		uploader.start();
	    });
	}
	
	uploader.bind('FilesAdded', function(up, files) {
	    $('#'+params.drop_element+' .attach_file_overlay').hide();
	});
	if (params.auto_start){
	    uploader.bind('FilesAdded', function(up, files) {
		up.refresh(); 
		this.start();
	    });
	}
	uploader.bind('FilesAdded', params.onFilesAdded);
	uploader.bind('UploadProgress', params.onUploadProgress);
	uploader.bind('FileUploaded', params.onFileUploaded);
	uploader.bind('Error', params.onError);
	
	this.uploaders.push(uploader);
	return uploader;
    }};
    
    kis.album = {
    
    albums_init: function(){
	$('#my_album_only').click(function(){
	    $(this).closest('form').submit();
	    
	    
	})
	$('.album_list').masonry();
    },
    album_form_init: function(lang, options, targets){
	
	var uploading_files_count = 0;
	var uploadedCounter = 0;		// keep until all files uploaded in each bulk upload
	var saveAlbum = function(callback){
	    $.post('ajax.php?action=setalbum', $('.newalbum').serialize(),function(res){
		//kis.unlock();
		$('.newalbum input[name="AlbumID"]').val(res.album_id);
		$('#span_current_target').html(targets[res.share_to]);
		$('.album_form_remove').fadeIn();
		//kis.setNavigationItems([res.album_title]);
		if (typeof(callback)=='function') callback();
		//alert('callback');
	    },'json');
	}
	
	var savePhotoOrder = function(){
		//alert('savePhotoOrder');
	    var photo_ids = $('.edit_photo_list .photo_ids').map(function(){return $(this).val()}).get();
	    var album_id =$('.newalbum input[name="AlbumID"]').val();
	    $.post('ajax.php?action=reorderphotos', {photo_ids: photo_ids, album_id: album_id});
	}
	
	kis.uploader({
	    browse_button:  'add_photos',
	    url: 'ajax.php?action=addphoto',
	    //resize : {width : 1920, height : 1920, quality : 100},
	    auto_start: false,
	    onFilesAdded: function(up, files) {
    		//alert("123");
    		var uploader = this;
    		uploading_files_count+=files.length;
    		uploadedCounter = uploading_files_count;
    		var photoCounter;
    		var eventDateName;
    		
    		$.each(files, function(i, file){
    			if($('#' + file.id).length > 0){ // File size too large
    				return true;
    			}
    		
    		    var item = $('#edit_photo_list_template li').clone();
    		    
    		    photoCounter = parseInt($('#photoCounter').val()) + i + 1;
		    
<?php if ($sys_custom['DigitalChannels']['showEventDate']): ?>

    		    eventDateName = 'eventDate' + photoCounter;
    			var eventDateHtml = item.find('span.eventDate').html();
    			eventDateHtml = eventDateHtml.replace(/eventDate/g,eventDateName);
    			item.find('span.eventDate').html(eventDateHtml);
<?php endif;?>
    		    item.attr('id', file.id).find('span').attr('title', file.name).append($('<div class="photo_progress">').progressbar());
    		    
    		    $('.edit_photo_list ul').append(item.fadeIn());
//    		    $('#photoCounter').val(photoCounter);		// update photo counter
    		   
    		});

    		saveAlbum(function(){
    		    //alert("1243");
    		    //kis.lock(lang.areyousureto+lang.stopuploadingphotos+'?');
    		    //pick up new mail_id
    		    uploader.settings.multipart_params= {album_id: $('.newalbum input[name="AlbumID"]').val()};
    		    uploader.start();
    		});
	    },
	    onUploadProgress: function(up, file) {
    		//alert('onUploadProgress');
    		$('#'+file.id).find('.photo_progress').progressbar('value', file.percent);
    		if (file.percent == 100 && !($("#overlay").length > 0)){
    			var layer = $("<table id='overlay'><tbody><tr height='30%'>&nbsp;</tr><tr height='25%''><td><?= $Lang['DigitalChannels']['Remarks']['MediaConvert'] ?></td></tr><tr height='15%'><td><img src='/images/2009a/loadingAnimation.gif' width='180px;' ></img></td></tr><tr height='30%'>&nbsp;</tr></tbody></table>").css({
    				"position": "relative",
    				"top" : 0,
    				"left": 0,
    				"height": "210px",
    				"width" : "190px",
    				"margin-top" : "-235px",
    				"background-color": "grey",
    				"border-radius" : "5px",
    				"z-index" : 100,
    	        	"opacity" : 0.7,
    	        	"color" : "#fff",
    	        	"font-size" : "20px",
    	        	"font-weight" : "bold",
            		"text-align": "center",
            		"vertical-align": "middle"
    			});
    			$('#'+file.id).append(layer);
    			
    		}
	    },
	    onFileUploaded: function(up, file, info) {
    		//alert(info.response);
    		var res = $.parseJSON(info.response);
    		if ($("#overlay").length > 0){
    			$("#overlay").remove();
    		}
    		if (res.error){
    		    
    		    $('#'+file.id+' .photo_progress').remove();
    		    $('#'+file.id+' span').html('<h5>'+file.name+'</h5> '+res.error);
    		    $('#'+file.id).fadeOut(3000);
    		      
    		}else if (res.thumbnail){
    		    $('#'+file.id+' .photo_progress').remove();
    		    $('#'+file.id+' span').css({'background-image': 'url('+res.thumbnail+')'});
<?php if ($sys_custom['DigitalChannels']['showEventTitle']):?>
				$('#'+file.id+' .input_event_title').prop('disabled', false);
    		    $('#'+file.id+' .input_chi_event_title').prop('disabled', false);
<?php endif;?>    		    
    		    $('#'+file.id+' .input_desc').prop('disabled', false);
    		    $('#'+file.id+' .input_chi_desc').prop('disabled', false);
    		    $('#'+file.id+' .photo_ids').val(res.photo_id);
    		    $('#'+file.id+' .title').val(res.title);
    		    $('#'+file.id+' .date_taken').val(res.date_taken);
    		    $('#'+file.id+' .date_uploaded').val(res.date_uploaded);
    		    $('#'+file.id+' .table_row_tool').show();
    		    $('#'+file.id).addClass('uploaded');
    		    
    		    $('.filter_ordering').fadeIn();

<?php if ($sys_custom['DigitalChannels']['showEventTitle']):?>    		    
        		var photoCounter;
        		var eventDateName;
        		for (var i=1; i<uploading_files_count; i++) {
            		photoCounter = parseInt($('#photoCounter').val()) + i ;
            		eventDateName = 'eventDate' + photoCounter;
        			$('input#' + eventDateName).ready(function(){
    					$('input#'+ eventDateName).datepick({
    						dateFormat: 'yy-mm-dd',
    						dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    						firstDay: 0,
    						onSelect: function(dateText, inst) {
    							Date_Picker_Check_Date_Format_CanEmpty(document.getElementById(eventDateName),'DPWL-' + eventDateName,'');
    							updateEventDate(this);
    						}
    					});
    
    					// for newly added items
    					$('input#'+ eventDateName).change(function(){
    						updateEventDate(this);
    					});
    					
    				});
    
        			$('#' + eventDateName).parent().css('background-image','none');
        			$('#' + eventDateName).parent().find('span').css('background-image','none');
        		}
    			$('#photoCounter').val(photoCounter);		// update photo counter
<?php endif;?>    			
    		}
    		
    		if (--uploading_files_count == 0){
    		    //kis.unlock();
    		}
    		savePhotoOrder();

	    },

    	onError: function(up, err) {
			var message, details = "";

			if(err.code == plupload.FILE_SIZE_ERROR){
				
			    var item = $('#edit_photo_list_template li').clone();
			    item.attr('id', err.file.id).find('span').attr('title', err.file.name).append($('<div class="photo_progress">').progressbar());
			    
			    $('.edit_photo_list ul').append(item.fadeIn());
			   
				if ($("#overlay").length > 0){
					$("#overlay").remove();
				}
			    $('#'+err.file.id+' .photo_progress').remove();
			    $('#'+err.file.id+' span').html('<h5>'+err.file.name+'</h5> <?=$Lang['DigitalChannels']['Organize']['FileSizeTooLarge'].'!'?>');
			    $('#'+err.file.id).fadeOut(3000);
			}
		}
	});
	
	//kis.datepicker('.album_form_date_access input.date');
	
	$('.edit_photo_list ul').sortable({
	    containment: "#container",
	    zIndex: '7010',
	    update: savePhotoOrder,
	    opacity: 0.8,
	    items: '>li.uploaded'
	});
	
	var title = $('.album_title').text();;
	if (title != ''){
	    kis.setNavigationItems([title]);
	}

	if (options.photo_count==0){
	    $('.filter_ordering').hide();
	    $('.edit_photo_list .attach_file_overlay').show();
	    
	}
	
	$('.album_form :input').change(saveAlbum);
    
	$('.edit_photo_list').on('change','.input_desc', function(){
	
	    var photo_id = $(this).siblings('.photo_ids').val();
	    var album_id = $('.newalbum input[name="AlbumID"]').val();
	    $.post('ajax.php?action=updatephotodescription', {description: $(this).val(), photo_id: photo_id, album_id: album_id},function(){
		//kis.unlock();
		
	    });
    
	});

	$('.edit_photo_list').on('change','.input_chi_desc', function(){
		
	    var photo_id = $(this).siblings('.photo_ids').val();
	    var album_id = $('.newalbum input[name="AlbumID"]').val();
	    $.post('ajax.php?action=updatePhotoChiDescription', {chiDescription: $(this).val(), photo_id: photo_id, album_id: album_id},function(){
	    });
    
	});
	
	$('.edit_photo_list').on('change','.input_event_title', function(){
		
	    var photo_id = $(this).siblings('.photo_ids').val();
	    var album_id = $('.newalbum input[name="AlbumID"]').val();
		    $.post('ajax.php?action=updateEventTitle', {eventTitle: $(this).val(), photo_id: photo_id, album_id: album_id},function(){
	    });
    
	});

	$('.edit_photo_list').on('change','.input_chi_event_title', function(){
		
	    var photo_id = $(this).siblings('.photo_ids').val();
	    var album_id = $('.newalbum input[name="AlbumID"]').val();
		    $.post('ajax.php?action=updateChiEventTitle', {chiEventTitle: $(this).val(), photo_id: photo_id, album_id: album_id},function(){
	    });
    
	});

	$('.edit_bottom .formbutton').click(function(){
	    
	    //var album_id = $('.newalbum input[name="AlbumID"]').val();
	    //$.address.value(album_id? '/apps/album/'+album_id+'/': '/apps/album/');
 
	});
	
	$('.filter_ordering a').click(function(){
	    
	    var sort_field = $(this).attr('href').replace('#','');
	    var to_int = $(this).hasClass('int')
	    var order;
	    
	    if ($(this).hasClass('order_asc')){
		order = -1;
		$(this).removeClass('order_asc').addClass('order_dec');
	    }else{
		order = 1;
		$(this).removeClass('order_dec').addClass('order_asc');
	    }
	    
	    $(this).siblings().removeClass('order_dec order_asc');
	   
	    
	    var items = $('.edit_photo_list li.uploaded').sort(function(a, b){
		
		a = $(a).find('.'+sort_field).val().toLowerCase();
		b = $(b).find('.'+sort_field).val().toLowerCase();
		
		if (to_int){
		    a = parseInt(a);
		    b = parseInt(b);
		}
		
		if(a > b){ return 1*order; }else if(a < b){ return -1*order; }else{ return 0;}

	    });
	    
	    $('.edit_photo_list ul').html(items);
	    savePhotoOrder();
	    
	    return false;
	    
	});
	
	$('.edit_photo_list').on('click', 'li.uploaded .copy_dim', function(){
	    
	    if (confirm(lang.areyousureto+$(this).attr('title')+'?')){
		
		var photo_id = $(this).parent().siblings('.photo_ids').val();
		var album_id = $('.newalbum input[name="AlbumID"]').val();
		
		$.post('ajax.php?action=updatecoverphoto', {photo_id: photo_id, album_id: album_id});
	
	    }
  
	    return false;
	});
	
	$('.edit_photo_list').on('click', 'li.uploaded .delete_dim', function(){
	    
	    if (confirm(lang.areyousureto+$(this).attr('title')+'?')){
	    
		var photo_id = $(this).parent().siblings('.photo_ids').val();
		var album_id =$('.newalbum input[name="AlbumID"]').val();
		
		$(this).closest('li').fadeOut(function(){$(this).remove();});
		$.post('ajax.php?action=removephoto', {photo_id: photo_id, album_id: album_id}, function( data ) {
			savePhotoOrder();
		});
		savePhotoOrder();
	    }
	    
	    return false;
	});
	
	$('.album_form_remove').click(function(){
	   
	    if (confirm('<?=$Lang['DigitalChannels']['Msg']['RemoveAlbum']?>')){
	    	$('#form1').attr('action', "admin_category_album_remove.php");
  			$('#form1').submit();
	    }
	    
	    return false;
	    
	});
	
	$('.btn_select_ppl').click(function(){
	    
	    $('.album_form_date_access_select').fadeToggle();
	
	    return false;
	});
	
	$('#share_to_all').click(function(){

	    $('.select_date').fadeIn();
	    $('#select_group_lists').hide();
		$('#select_group_lists select').prop('disabled', true);
		$('#select_group_lists input').prop('disabled', true);
		$('#select_specific_users').hide();
	});
	
	$('#share_to_myself').click(function(){
	    
	    $('.select_date').hide();
	    $('#select_group_lists').hide();
		$('#select_group_lists select').prop('disabled', true);
		$('#select_group_lists input').prop('disabled', true);
		$('#select_specific_users').hide();
	});
	
	$('#share_to_groups').click(function(){
	    
	    $('.select_date').fadeIn();
	    $('#select_group_lists').fadeIn();
		$('#select_group_lists select').prop('disabled', false);
		$('#select_group_lists input').prop('disabled', false);
		$('#select_specific_users').hide();
	});

	$('#share_to_users').click(function(){
	    $('#select_group_lists').hide();
		$('#select_group_lists select').prop('disabled', true);
		$('#select_group_lists input').prop('disabled', true);
		$('#select_specific_users').fadeIn();
	});
	
	
    },
    album_init:function(){
	   
	var title = $('.album_title').text();
	
	kis.setNavigationItems([title]);
	$('.photo_thumb_list ul').masonry();

	
    }
    
}

    kis.album.album_form_init({
	areyousureto : '<?=$Lang['DigitalChannels']['Msg']['AreYouSureTo']?>',
	allphotoswillberemoved: '<?=$Lang['DigitalChannels']['Msg']['AllPhotosWillBeRemoved']?>!',
	stopuploadingphotos: '<?=$Lang['DigitalChannels']['Msg']['StopUploadingPhotos']?>'		      
    },{photo_count: <?=sizeof($photos)?>},{all:'<?=$Lang['DigitalChannels']['Organize']['AllUsers']?>',groups:'<?=$Lang['DigitalChannels']['Organize']['Groups']?>',myself:'<?=$Lang['DigitalChannels']['Organize']['Private']?>'});

    // for existing items
    $(":input[name^='eventDate']").change(function(){
    	updateEventDate(this);		
    });
    
});

function checkForm()
{
    // Set selected PIC for updating DB
    var obj = document.form1;
    if(obj.elements["PIC[]"].length != 0) {
	    for(i=0; i<obj.elements["PIC[]"].length; i++) {
			obj.elements["PIC[]"].options[i].selected = true;   
	    }
	}
	
	// Set select Target Group(s)
	if(obj.elements["select_groups[]"].length != 0) {
	    for(i=0; i<obj.elements["select_groups[]"].length; i++) {
			obj.elements["select_groups[]"].options[i].selected = true;   
	    }
	}

	// Set select Target Group(s) - specific users
	if(obj.elements["select_users[]"].length != 0) {
	    for(i=0; i<obj.elements["select_users[]"].length; i++) {
			obj.elements["select_users[]"].options[i].selected = true;   
	    }
	}
	
	// Checking Start Date and End Date
	var start = $('#StartDate').val();
	var end = $('#EndDate').val();
	var noChecking = start.trim() == "" || end.trim() == "";
	if(!noChecking && compareDate(start, end) === 1 && $('#form1').attr('action') == "admin_category_album_update.php"){
		alert('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
		return false;
	}
	
//	if($("#overlay").length > 0 && !confirm('<?=$Lang['DigitalChannels']['Remarks']['DiscardUploadMedia']?>')){
//		kis.abort();
//		return false;
//	}
	
	return true;
}

function cancelCheck(categoryCode){
	var newAlbum = <?= $isNew? '1' : '0'; ?>;
	var withData = document.form1.title.value.length > 0 || document.form1.description.value.length > 0 || $(".uploaded").length > 0;
	
//	if($("#overlay").length > 0 && !confirm('<?=$Lang['DigitalChannels']['Remarks']['DiscardUploadMedia']?>')){
//		return false;
//	}
	
	if(newAlbum){
		if(!withData || confirm('<?=$Lang['DigitalChannels']['Remarks']['DiscardThisAlbum']?>')){
			$('#form1').attr('action', "admin_category_album_remove.php?withoutData="+(withData?0:1));
	  		$('#form1').submit();
		}
	} else {
		window.location='admin_category_album.php?CategoryCode='+categoryCode;
	}
	
}

function updateEventDate(obj)
{
	var $this = $(obj);	
    var photo_id = $this.parent().parent().find('.photo_ids').val();
    var album_id = $('.newalbum input[name="AlbumID"]').val();

	if (check_date_without_return_msg(obj)) {
    	$.post('ajax.php?action=updateEventDate', {eventDate: $this.val(), photo_id: photo_id, album_id: album_id},function(){
	    });
	}
}
</script>

<div class="digital_channels_content">
	<div id="container">
<?echo $ldcUI->Display_Title_Menu();?>
<?echo $ldcUI->Display_Category_Album_New($AlbumID, $CategoryCode);?>         
</div>
</div>
<?php
$ldcUI->LAYOUT_STOP();
intranet_closedb();
?>