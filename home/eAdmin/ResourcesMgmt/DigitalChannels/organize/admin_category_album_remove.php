<?php
// Editing by 
/*
 * 2019-04-03 (Henry): control the update msg by passing "withoutData"
 * 2014-10-14 (Henry): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

//debug_pr($_REQUEST);
$AlbumID = $AlbumID?$AlbumID:$album_id;

if ($AlbumID && count($ldc->Get_Album($AlbumID)) == 0){
	header("Location: ../index.php");
	exit;
}

if ($AlbumID && !$ldc->isAlbumEditable($AlbumID)){
	header("Location: ../index.php");
	exit;
}

if (!$AlbumID && !$ldc->isAlbumNewable()){
	header("Location: ../index.php");
	exit;
}

if($AlbumID || $album_id){
	$result = $ldc->Remove_Album($AlbumID?$AlbumID:$album_id);
	$AlbumID = $result;
}

if(!$withoutData){
	$msg = ($result > 0? "UpdateSuccess" : "UpdateUnsuccess");
}

intranet_closedb();
header("Location: admin_category_album.php?CategoryCode=$CategoryCode&msg=$msg");
?>