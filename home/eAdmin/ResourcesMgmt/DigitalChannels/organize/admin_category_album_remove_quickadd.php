<?php
// Editing by 
/*
 * 2018-03-16 (Cameron): file created
 */
 
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT."includes/global.php");
include_once ($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT."includes/libinterface.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels.php");
include_once ($PATH_WRT_ROOT."includes/DigitalChannels/libdigitalchannels_ui.php");
include_once ($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT."lang/digitalchannels_lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();

//if (!$_SESSION['SSV_USER_ACCESS']['eAdmin-DigitalChannels']) {
//	header("Location: ../index.php");
//	exit;
//}

$linterface = new interface_html();
$ldc = new libdigitalchannels();

$CurrentPageArr['DigitalChannels'] = 1;

$albumID = $_POST['AlbumID'];
$photoIDAry = $_POST['photo_ids'];
$categoryCode = $_POST['CategoryCode'];

if (count($photoIDAry) == 0) {
    header("Location: ../index.php");
	exit;
}

if ($albumID&& !$ldc->isAlbumEditable($albumID)){
	header("Location: ../index.php");
	exit;
}

$result = $ldc->removeAlbumPhotos($albumID, $photoIDAry);

$msg = ($result > 0? "UpdateSuccess" : "UpdateUnsuccess");

intranet_closedb();
header("Location: admin_category_album.php?CategoryCode=$categoryCode&msg=$msg");
?>