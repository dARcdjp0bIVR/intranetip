<?php

######################################
#
#	Date:	2011-03-10	YatWoon
#			revised Display order coding
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();

$ReasonTypeEngName = addslashes($ReasonTypeEngName);
$ReasonTypeChiName = addslashes($ReasonTypeChiName);

#added by Kelvin Ho 2008-12-19 (get the other category order if exists)
$sql = "select DisplayOrder from INVENTORY_WRITEOFF_REASON_TYPE where ReasonTypeID = $ReasonType_id";
$tmpOrder = $linventory->returnVector($sql);
$orgOrder = $tmpOrder[0];

##### Display order issue [start]
if($orgOrder < $ReasonType_display_order)
{
		$sql = "update INVENTORY_WRITEOFF_REASON_TYPE set DisplayOrder=DisplayOrder-1 where DisplayOrder>$orgOrder and DisplayOrder<=$ReasonType_display_order";
		$linventory->db_db_query($sql);
	}
	else if($orgOrder > $ReasonType_display_order)
	{
		$sql = "update INVENTORY_WRITEOFF_REASON_TYPE set DisplayOrder=DisplayOrder+1 where DisplayOrder<$orgOrder and DisplayOrder>=$ReasonType_display_order";
	$linventory->db_db_query($sql);
}
##### Display order issue [end]
        
        
$sql = "UPDATE
				INVENTORY_WRITEOFF_REASON_TYPE 
		SET
				ReasonTypeNameEng = '$ReasonTypeEngName', 
				ReasonTypeNameChi = '$ReasonTypeChiName', 
				DisplayOrder = $ReasonType_display_order, 
				DateModified = NOW()
		WHERE
				ReasonTypeID = $ReasonType_id";

$result = $linventory->db_db_query($sql);

/*
#added by Kelvin Ho 2008-12-19 (modify the other category order if exists)
$sql = "select ReasonTypeID from INVENTORY_WRITEOFF_REASON_TYPE where DisplayOrder = '$ReasonType_display_order' and ReasonTypeID!=$ReasonType_id";
$tmpOrder = $linventory->returnVector($sql);
$order = $tmpOrder[0];
if($order)
{
	$sql = "update INVENTORY_WRITEOFF_REASON_TYPE set DisplayOrder = '$orgOrder' where ReasonTypeID = ".$order;
        $linventory->db_db_query($sql);
}
*/

##### Display order issue [Start]
# re-order
$sql = "SELECT ReasonTypeID FROM INVENTORY_WRITEOFF_REASON_TYPE order by DisplayOrder";
$result = $linventory->returnVector($sql);
for($i=0;$i<sizeof($result);$i++)
{
	$sql="update INVENTORY_WRITEOFF_REASON_TYPE set DisplayOrder=$i+1 where ReasonTypeID=". $result[$i];
	$linventory->db_db_query($sql);
}
##### Display order issue [End]
		
if ($result)
{
	header("location: write_off_reason_setting.php?msg=2");
}
else
{
	header("location: write_off_reason_setting.php?msg=14");
}
intranet_closedb();
?>