<?php
/*************************************************
 * 2016-02-04 (Henry): php 5.4 issue move "if(!isset($funding_id)){}" after includes file 
 *************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

if(!isset($Reason_id))
{
	header("location: write_off_reason_setting.php");
	exit();
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_WriteOffReason";
$linterface 	= new interface_html();
$linventory	= new libinventory();

### Category ###
//$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['Settings']." > ".$i_InventorySystem['FundingSource']." > ".$button_edit)."</td></tr>"; 
//$temp[] = array($i_InventorySystem['Settings']);
$temp[] = array($i_InventorySystem_Setting_EditWriteOffReason);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 
//$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['WriteOffReason']." > ".$button_edit)."</td></tr>"; 

/*
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['WriteOffReason'], $PATH_WRT_ROOT."home/admin/inventory/settings/write_off_reason/write_off_reason_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);
*/
$TAGS_OBJ[] = array($i_InventorySystem['WriteOffReason'], "", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


$sql = "SELECT 
					ReasonTypeID, 
					ReasonTypeNameEng, 
					ReasonTypeNameChi, 
					DisplayOrder 
				FROM 
					INVENTORY_WRITEOFF_REASON_TYPE 
				where 
					ReasonTypeID = $Reason_id[0]";

$result = $linventory->returnArray($sql,4);

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($ReasonType_id, $ReasonTypeNameEng, $ReasonTypeNameChi, $ReasonType_display_order) = $result[$i];
		
		$sql = "SELECT COUNT(*) FROM INVENTORY_WRITEOFF_REASON_TYPE";
		$total_rec = $linventory->returnVector($sql);
		$display_order .= "<select name=\"ReasonType_display_order\">";
		for($j=0; $j<$total_rec[0]; $j++)
		{
			$order=$j+1;
			if($ReasonType_display_order==$order) {
				$selected = "SELECTED";
			}
			else {
				$selected = "";
			}
			$display_order .= "<option value=\"$order\" $selected>$order</option>";
		}
		$display_order .= "</select>";
		
		$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_InventorySystem_Setting_WriteOffReason_ChineseName."</td><td class=\"tabletext\" valign=\"top\"><input name=\"ReasonTypeChiName\" value='".intranet_htmlspecialchars($ReasonTypeNameChi)."' type=\"text\" class=\"textboxtext\" size=\"200\" maxlength=\"200\"></td></tr>";
		$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_InventorySystem_Setting_WriteOffReason_EnglishName."</td><td class=\"tabletext\" valign=\"top\"><input name=\"ReasonTypeEngName\" value='".intranet_htmlspecialchars($ReasonTypeNameEng)."' type=\"text\" class=\"textboxtext\" size=\"200\" maxlength=\"200\"></td></tr>";
		$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$i_InventorySystem_Category_DisplayOrder."</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>";
		$table_content .= "<input name=\"ReasonType_id\" type=\"hidden\" value=\"$ReasonType_id\">\n";
	}
	$table_content .= "<tr><td colspan=2 align=center>".
						$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
						$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
						$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
					 ."</td></tr>";
}
?>
<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	var passed = 0;
	
	if(check_text(obj.ReasonTypeChiName,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning?>"))
		if(check_text(obj.ReasonTypeEngName,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning?>"))
			passed = 1;
			
	if(passed == 1)
	{
		obj.action = "write_off_reason_edit_update.php";
		return true;
	}
	else
	{
		obj.action = "";
		//obj.submit();
		return false;
	}
	
}
</script>

<br>
<form name="form1" action="" method="POST" onSubmit="return checkForm();">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar1 ?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar2 ?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content?>
</table>
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>