<?php

######################################
#
#	Date:	2011-03-10	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linventory	= new libinventory();

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$target_reason_list = implode(",",$Reason_id);

if($target_reason_list != "")
{
	$sql = "DELETE FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID IN ($target_reason_list)";
	$result = $linventory->db_db_query($sql);
	
	if($result)
	{
		
		##### Display order issue [Start]
		# re-order
		$sql = "SELECT ReasonTypeID FROM INVENTORY_WRITEOFF_REASON_TYPE order by DisplayOrder";
		$result = $linventory->returnVector($sql);
		for($i=0;$i<sizeof($result);$i++)
		{
			$sql="update INVENTORY_WRITEOFF_REASON_TYPE set DisplayOrder=$i+1 where ReasonTypeID=". $result[$i];
			$linventory->db_db_query($sql);
		}
		##### Display order issue [End]
		
		header("location: write_off_reason_setting.php?msg=3");
	}
	else
	{
		header("location: write_off_reason_setting.php?msg=13");
	}
}
else
{
	header("location: write_off_reason_setting?msg=13");
}
intranet_closedb();
?>