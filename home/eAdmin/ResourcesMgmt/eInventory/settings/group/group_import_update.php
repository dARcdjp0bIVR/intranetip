<?php
# using: 

######################################
#
#	Date:	2014-05-26	YatWoon
#			fixed: failed to import special character [Case#H62358]
#
#	2013-02-07 (Carlos): added random Barcode for new group
#
#	Date:	2011-03-02	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Settings_BasicSettings";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$sql = "SELECT 
				Code, 
				NameChi, 
				NameEng, 
				DisplayOrder 
		FROM 
				TEMP_INVENTORY_ADMIN_GROUP";

$arr_result = $linventory->returnArray($sql,5);

if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($admin_group_code,$admin_group_chi_name, $admin_group_eng_name, $admin_group_display_order) = $arr_result[$i];
		
		$admin_group_chi_name = addslashes(intranet_undo_htmlspecialchars($admin_group_chi_name));
		$admin_group_eng_name = addslashes(intranet_undo_htmlspecialchars($admin_group_eng_name));
		
		##### Display order issue [Start]
		$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE DisplayOrder = $admin_group_display_order";
		$result = $linventory->returnVector($sql);
		$existingAdminGroupID = $result[0];
		if($existingAdminGroupID != "")
		{
			$sql = "update INVENTORY_ADMIN_GROUP set DisplayOrder=DisplayOrder+1 where DisplayOrder>=$admin_group_display_order";
			$linventory->db_db_query($sql);
		}
		##### Display order issue [End]
		
		$Barcode = implode(",",$linventory->getUniqueBarcode(1,"group"));
		$values = "'$admin_group_code', '$admin_group_chi_name', '".$admin_group_eng_name."','$Barcode', '$admin_group_display_order', NOW(), NOW()";
		
		$sql = "INSERT INTO INVENTORY_ADMIN_GROUP
						(Code, 
						NameChi, 
						NameEng,
						Barcode, 
						DisplayOrder, 
						DateInput, 
						DateModified)
				VALUES
						($values)";
		
		$result['NewInvItem'.$i] = $linventory->db_db_query($sql);
		
	}
	
	# re-order
	$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP order by DisplayOrder";
	$result = $linventory->returnVector($sql);
	for($i=0;$i<sizeof($result);$i++)
	{
		$sql="update INVENTORY_ADMIN_GROUP set DisplayOrder=$i+1 where AdminGroupID=". $result[$i];
		$linventory->db_db_query($sql) or die(mysql_error());
	}

}

if (in_array(false,$result)) {
	header("location: group_import.php?xmsg=ImportUnsuccess");
}
else {
	header("location: group_import.php?xmsg=ImportSuccess");
}
intranet_closedb();
?>