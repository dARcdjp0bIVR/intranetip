<?php
// Editing by 
/*
 * 2015-06-10 (Henry):	add addslashes for js validation
 * 2013-02-06 (Carlos): add data field Barcode
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage 	= "Settings_Group";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$BarcodeFormat = $linventory->getBarcodeFormat();

### Category ###
$temp[] = array($i_InventorySystem_Setting_NewManagementGroup);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

// get all category's Codem, Chi and Eng name
$sql = "SELECT Code, NameChi, NameEng, Barcode FROM INVENTORY_ADMIN_GROUP";
$GroupNameList = $linventory->returnArray($sql,4);

$js_existing_group_barcode_arr = "var jsGroupBarcodeArr = [];\n";
for($i=0;$i<sizeof($GroupNameList);$i++) {
	$group_barcode = trim($GroupNameList[$i]['Barcode']);
	if($group_barcode != "") {
		$js_existing_group_barcode_arr .= "jsGroupBarcodeArr.push('".$group_barcode."');\n";
	}
}
//

$sql = "SELECT COUNT(*) FROM INVENTORY_ADMIN_GROUP";
$total_rec = $linventory->returnVector($sql);
$display_order .= "<select name=\"group_display_order\">";
for($i=0; $i<=$total_rec[0]; $i++)
{
	$j=$i+1;
	if($j==$total_rec[0])
		$selected = "SELECTED=\"selected\"";
	$display_order .= "<option value=\"$j\" $selected>$j</option>";
}
$display_order .= "</select>";

$sql = "SELECT 
				DISTINCT a.GroupID,
				a.Title
		FROM 
				INTRANET_GROUP AS a INNER JOIN 
				INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) INNER JOIN
				INTRANET_USER AS c ON (b.UserID = c.UserID)
		WHERE
				a.AcademicYearID = ".$_SESSION['CurrentSchoolYearID']." AND 
				c.RecordType = 1 AND 
				c.RecordStatus IN (1)";
$result = $linventory->returnArray($sql,2);

$intranet_group_selection = getSelectByArray($result,"name=\"link_intranet_group\" ");

$Barcode = implode(",",$linventory->getUniqueBarcode(1,"group"));

$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Caretaker_Code</td><td class=\"tabletext\" valign=\"top\"><input name=\"GroupCode\" type=\"text\" class=\"textboxnum\" maxlength=\"10\" value=\"$GroupCode\"></td></tr>\n";
$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_ManagementGroup_ChineseName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"group_chi_name\" type=\"text\" class=\"textboxtext\" value=\"$group_chi_name\" size=\"200\" maxlength=\"200\"></td></tr>";
$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_ManagementGroup_EnglishName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"group_eng_name\" type=\"text\" class=\"textboxtext\" value=\"$group_eng_name\" size=\"200\" maxlength=\"200\"></td></tr>";
$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Barcode}</td><td class=\"tabletext\" valign=\"top\"><input name=\"Barcode\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($Barcode)."' size=\"200\" maxlength=\"100\">";
$table_content .= $linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Code","Button","javascript:GenBarcode();", "", "");
$table_content .= "</td></tr>\n";
$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Category_DisplayOrder}</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>";
$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Import_Member_From}</td><td class=\"tabletext\" valign=\"top\">".$intranet_group_selection."<br/><span class=\"tabletextremark\">$i_InventorySystem_ImportCaretaker_Warning</span></td>";

$table_content .= "<tr><td colspan=2 align=center>".
					$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
					$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
					$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='group_setting.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
				 ."</td></tr>";

?>
<script language="javascript">
<?=$js_existing_group_barcode_arr?>

function checkGroupCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.GroupCode.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_Group_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
		for ($i=0;$i< sizeof($GroupNameList); $i++) {
	?>
			if (obj.GroupCode.value == "<?=$GroupNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_Group_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
		}
	?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkGroupChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
		for ($i=0;$i< sizeof($GroupNameList); $i++) {
	?>
			if (obj.group_chi_name.value == "<?=addslashes($GroupNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_Group_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
		}
	?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkGroupEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
		for ($i=0;$i< sizeof($GroupNameList); $i++) {
	?>
			if (obj.group_eng_name.value == "<?=addslashes($GroupNameList[$i]['NameEng'])?>") {
					alert("<?=$i_InventorySystem_Input_Group_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
		}
	?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkBarcodeFormat(barcode)
{
	var barcode_max_length = <?=$BarcodeMaxLength;?>;
	var barcode_format = <?=$BarcodeFormat;?>;
	var barcode_checking = 0;
	
	if(barcode_format == 1)
	{
		var ValidChars = new RegExp("[0-9\ \.\/\|\$\-]");
		var tmp_length = barcode.length;
		var IsNumber=true;
		var Char;
		if(barcode != "")
		{
			if(tmp_length > barcode_max_length)
			{
				IsNumber=false;
			}
			for(i=0; i<tmp_length; i++)
			{
				if(ValidChars.test(Char = barcode.charAt(i)) == false)
				{
					IsNumber=false;
				}
			}
		}
		else
		{
			IsNumber = false;
		}
		if(IsNumber == true)
		{
			barcode_checking++;
		}
		else
		{
			barcode_checking--;
		}
		
	}
   
	if(barcode_format == 2)
	{
		var ValidChars = new RegExp("[0-9A-Z\ \.\/\|\$\-]");
		var tmp_length = barcode.length;
		var IsNumber = true;
		var Char;
		if(barcode != "")
		{
			if(tmp_length > barcode_max_length)
			{
				IsNumber=false;
			}
			for(i=0; i<tmp_length; i++)
			{
				if(ValidChars.test(Char = barcode.charAt(i)) == false)
				{
					IsNumber=false;
				}
			}
		}
		else
		{
			IsNumber = false;
		}
		if(IsNumber == true)
		{
			barcode_checking++;
		}
		else
		{
			barcode_checking--;
		}
	}
	return barcode_checking > 0;
}

function checkBarcode()
{
	var obj = document.form1;
	var barcode = obj.Barcode.value.Trim();
	if(barcode == "") {
		alert('<?=$i_InventorySystem_Input_Item_Barcode_Warning?>');
		return false;
	}
	
	if(!checkBarcodeFormat(barcode)){
		alert('<?=$Lang['SysMgr']['Location']['Warning']['BarcodeNotValid']?>');
		return false;
	}
	
	if(jIN_ARRAY(jsGroupBarcodeArr,barcode)) {
		alert('<?=$i_InventorySystem_Input_Item_Barcode_Exist_Warning?>');
		return false;
	}
	
	return true;
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;
	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
		
	if(check_text(obj.GroupCode,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
		if(checkGroupCode()) {
			if(check_text(obj.group_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
				if(checkGroupChiName()) {
					if(check_text(obj.group_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						if(checkGroupEngName()) {
							if(checkBarcode()) {
								passed = 1;	
							} else {
								error_cnt = 4;
								passed = 0;
							}
						}
						else {
								error_cnt = 3;
								passed = 0;
						}
					}
					else {
							passed = 0;
					}
				}
				else {
						error_cnt = 2;
						passed = 0;
				}
			}
			else {
					passed = 0;
			}
		}
		else {
				error_cnt = 1;
				passed = 0;
		}
	}
	else {
			passed = 0;
	}

				
	if(passed == 1)
	{
		obj.action = "group_insert_update.php";
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.GroupCode.focus();
		if(error_cnt == 2)
			obj.group_chi_name.focus();
		if(error_cnt == 3)
			obj.group_eng_name.focus();
		if(error_cnt == 4)
			obj.Barcode.focus();
			
		obj.action = "";
		return false;
	}
}

function GenBarcode()
{
	$.get(
		'generateAdminGroupBarcode.php',
		{},
		function(data){
			$('input[name="Barcode"]').val(data);
		}
	);
}
</script>

<br>

<form name="form1" method="post" action="" onSubmit="return checkForm();">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar1 ?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar2 ?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
</form>

</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>