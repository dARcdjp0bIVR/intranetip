<?php
/**
 * 2012-10-11 Rita - add updation customization
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "StockList_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$target_member_list = implode(",",$group_member_id);

# set as group leader #
if($type == MANAGEMENT_GROUP_ADMIN)
{
	$sql = "UPDATE
				INVENTORY_ADMIN_GROUP_MEMBER
		SET
				RecordType = ".MANAGEMENT_GROUP_ADMIN."
		WHERE
				UserID IN ($target_member_list) AND 
				AdminGroupID = $group_id
				";
	$result = $linventory->db_db_query($sql);
}

if($type == MANAGEMENT_GROUP_MEMBER)
{
	$sql = "UPDATE
					INVENTORY_ADMIN_GROUP_MEMBER
			SET
					RecordType = ".MANAGEMENT_GROUP_MEMBER."
			WHERE
					UserID IN ($target_member_list) AND 
					AdminGroupID = $group_id
					";
	$result = $linventory->db_db_query($sql);
}


if ($linventory->enableMemberUpdateLocationRight())
{

	if($type == MANAGEMENT_GROUP_HELPER)
	{
		$sql = "UPDATE
						INVENTORY_ADMIN_GROUP_MEMBER
				SET
						RecordType = ".MANAGEMENT_GROUP_HELPER."
				WHERE
						UserID IN ($target_member_list) AND 
						AdminGroupID = $group_id
						";
		$result = $linventory->db_db_query($sql);
	}

}

if($result > 0)	
{
	intranet_closedb();
	header("location: group_member_setting.php?group_id=$group_id&msg=2");
}
else
{
	intranet_closedb();
	header("location: group_member_setting.php?group_id=$group_id&msg=14");
}

?>