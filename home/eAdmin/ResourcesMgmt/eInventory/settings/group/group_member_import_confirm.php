<?php
/**
 * 2012-10-11 Rita - add updation customization
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Group";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$limport 		= new libimporttext();
$li 			= new libdb();
$lo 			= new libfilesystem();

$filepath 		= $itemfile;
$filename 		= $itemfile_name;

/*if($format == 1)
	$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($button_new." > ".$i_InventorySystem_ItemType_Single)."</td></tr>"; 
if($fomrat == 2)
	$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($button_new." > ".$i_InventorySystem_ItemType_Bulk)."</td></tr>"; 
*/

$file_format = array("Group Code","User Login","Identity");
	
# Create temp single item table
$sql = "DROP TABLE TEMP_INVENTORY_ADMIN_GROUP_MEMBER";
$li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_ADMIN_GROUP_MEMBER
		(
		 AdminGroupCode varchar(10),
		 UserLogin varchar(20),
		 RecordType int(11)	
	    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql);


$ext = strtoupper($lo->file_ext($filename));
if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: group_member_import.php?xmsg=WrongFileFormat");
	exit();
}

if($limport->CHECK_FILE_EXT($filename))
{
	# read file into array
	# return 0 if fail, return csv array if success
	//$data = $lo->file_read_csv($filepath);
	//$col_name = array_shift($data);                   # drop the title bar
	$data = $limport->GET_IMPORT_TXT($filepath);
	$col_name = array_shift($data);
	
	# check the csv file's first row is correct or not
	$format_wrong = false;
	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}
	
	if($format_wrong)
	{
		header("location: group_member_import.php?group_id=$group_id&msg=15");
		exit();
	}
		
	### Remove Empty Row in CSV File ###
	for($i=0; $i<sizeof($data); $i++)
	{
		if(sizeof($data[$i])!=0)
		{
			$arr_new_data[] = $data[$i];
			$record_row++;
		}
		else
		{
			$empty_row++;
		}
	}
	$file_original_row = sizeof($data);
	$file_new_row = sizeof($arr_new_data);
	
	for($i=0; $i<sizeof($arr_new_data); $i++)
	{
		list($admin_group_code,$user_login,$user_identity) = $arr_new_data[$i];
								
		# Check location Code Exist #
		if($admin_group_code != "")
		{
			$sql = "SELECT Code	FROM INVENTORY_ADMIN_GROUP WHERE Code = '$admin_group_code'";
			$arr_result = $linventory->returnArray($sql,1);
			
			if(sizeof($arr_result) == 0)
				$error[$i]['type'] = 1;
		}
		else
		{
			$error[$i]['type'] = 2;
		}
		
		# Check User Info #
		if($user_login == "")
		{
			$error[$i]['type'] = 3;
		}
		else
		{
			$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$user_login'";
			$arr_user_id = $linventory->returnVector($sql);
			if(sizeof($arr_user_id) == 0)
			{
				$error[$i]['type'] = 4;
			}
		}
		
		# Check User Identity #
		if($user_identity == "")
		{
			$error[$i]['type'] = 5;	
		}
		else
		{
			
			if ($linventory->enableMemberUpdateLocationRight())
			{
				switch (strtoupper($user_identity))
				{
					case 1: 
										$user_type = MANAGEMENT_GROUP_ADMIN;
										break;
					case 2: 
										$user_type = MANAGEMENT_GROUP_HELPER;
										break;
					case 3: 
										$user_type = MANAGEMENT_GROUP_MEMBER;
										break;
					default:			
										$error[$i]['type'] = 6;
										break;
				}
				
			}else{
				
				switch (strtoupper($user_identity))
				{
					case 1: 
										$user_type = MANAGEMENT_GROUP_ADMIN;
										break;
					case 2: 
										$user_type = MANAGEMENT_GROUP_MEMBER;
										break;
					default:			
										$error[$i]['type'] = 6;
										break;
				}
				
			}
	
		}
		
		$sql = "Select AdminGroupCode, UserLogin From TEMP_INVENTORY_ADMIN_GROUP_MEMBER";
		$tmpGroupMemberInfoAry = $linventory->returnArray($sql);
		$numOfRecord = count($tmpGroupMemberInfoAry);
		for ($j=0; $j<$numOfRecord; $j++) {
			$_adminGroupCode = $tmpGroupMemberInfoAry[$j]['AdminGroupCode'];
			$_userLogin = $tmpGroupMemberInfoAry[$j]['UserLogin'];
			
			if (trim($_adminGroupCode) == trim($admin_group_code) && trim($_userLogin) == trim($user_login)) {
				$error[$i]['type'] = 7;
				break;
			}
		}
		
		$values = "('$admin_group_code','$user_login','$user_type')";
		$sql = "INSERT INTO TEMP_INVENTORY_ADMIN_GROUP_MEMBER (AdminGroupCode, UserLogin, RecordType) VALUES $values";
		$linventory->db_db_query($sql);
		
		# check any duplicate user login in the csv file
//		$sql = "SELECT UserLogin FROM TEMP_INVENTORY_ADMIN_GROUP_MEMBER";
//		$arr_tmp_checkAdminGroupMember = $linventory->returnVector($sql);
//		if(sizeof($arr_tmp_checkAdminGroupMember) != sizeof(array_unique($arr_tmp_checkAdminGroupMember)))
//			$error[$i]['type'] = 7;
	}
	
	### Show Import Result ###
	if($record_row == "")
		$record_row = 0;
	if($empty_row == "")
		$empty_row = 0;
	$table_content .= "<tr>";
	$table_content .= "<td class=\"tabletext\" colspan=\"4\">
							$i_InventorySystem_ImportItem_TotalRow: $file_original_row<br>
							$i_InventorySystem_ImportItem_RowWithRecord: $record_row<br>
							$i_InventorySystem_ImportItem_EmptyRowRecord: $empty_row
						</td>";
	$table_content .= "</tr>";
	### END ###
	$table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_GroupCode</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_UserLogin</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_identity</td>";
		
	if(sizeof($error)>0)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
	}
	$table_content .= "</tr>\n";
	
	if(sizeof($error) == 0)
	{
		$sql = "SELECT 
					AdminGroupCode, 
					UserLogin, 
					RecordType
				FROM
					TEMP_INVENTORY_ADMIN_GROUP_MEMBER";
		
		$arr_result = $linventory->returnArray($sql,3);

		if(sizeof($arr_result) > 0)
		{
			for ($i=0; $i<sizeof($arr_result); $i++)
			{
				$j=$i+1;
				if($j%2 == 0)
					$table_row_css = " class=\"tablerow1\" ";
				else
					$table_row_css = " class=\"tablerow2\" ";
				
				list($admin_group_code, $user_login, $user_identity) = $arr_result[$i];
				
				if ($linventory->enableMemberUpdateLocationRight())
				{		
					if($user_identity == MANAGEMENT_GROUP_ADMIN)
						$user_type = $i_InventorySystem_Group_MemberPosition_Leader;
					else if($user_identity == MANAGEMENT_GROUP_HELPER)
						$user_type = $Lang['Inventoty']['Settigs']['Helper'];				
					elseif($user_identity == MANAGEMENT_GROUP_MEMBER)
						$user_type = $i_InventorySystem_Group_MemberPosition_Member;
					else
						$user_type = $user_identity;
				}else{	
					if($user_identity == MANAGEMENT_GROUP_ADMIN)
						$user_type = $i_InventorySystem_Group_MemberPosition_Leader;
					else if($user_identity == MANAGEMENT_GROUP_MEMBER)
						$user_type = $i_InventorySystem_Group_MemberPosition_Member;
					else
						$user_type = $user_identity;
				}
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$admin_group_code</td>";
				$table_content .= "<td class=\"tabletext\">$user_login</td>";
				$table_content .= "<td class=\"tabletext\">$user_type</td></tr>";
			}
		}
		$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"12\"></td></tr>";
		$table_content .= "<tr><td colspan=12 align=right>".
							$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
							$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='group_member_import.php?group_id=$group_id'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
						  "</td></tr>";
	}
	if(sizeof($error) > 0)
	{
		for($i=0; $i<sizeof($arr_new_data); $i++)
		{
			$j=$i+1;
			if($j%2 == 0)
				$table_row_css = " class=\"tablerow1\" ";
			else
				$table_row_css = " class=\"tablerow2\" ";
				
			list($admin_group_code,$user_login,$user_identity) = $arr_new_data[$i];
			
			
			if ($linventory->enableMemberUpdateLocationRight())
			{					
				if($user_identity == MANAGEMENT_GROUP_ADMIN)
						$user_type = $i_InventorySystem_Group_MemberPosition_Leader;
					
					else if($user_identity == MANAGEMENT_GROUP_HELPER)
						$user_type = $Lang['Inventoty']['Settigs']['Helper'];	
					else if($user_identity == MANAGEMENT_GROUP_MEMBER)
						$user_type = $i_InventorySystem_Group_MemberPosition_Member;
					else
						$user_type = "";
			}else{
				if($user_identity == MANAGEMENT_GROUP_ADMIN)
						$user_type = $i_InventorySystem_Group_MemberPosition_Leader;
					else if($user_identity == MANAGEMENT_GROUP_MEMBER)
						$user_type = $i_InventorySystem_Group_MemberPosition_Member;
					else
						$user_type = "";		
			}
				
			if($error[$i]["type"] == "")
			{
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$admin_group_code</td>";
				$table_content .= "<td class=\"tabletext\">$user_login</td>";
				$table_content .= "<td class=\"tabletext\">$user_type</td>";
				$table_content .= "<td class=\"tabletext\"> - </td></tr>";
			}
			if($error[$i]["type"] != "")
			{
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$admin_group_code</td>";
				$table_content .= "<td class=\"tabletext\">$user_login</td>";
				$table_content .= "<td class=\"tabletext\">$user_type</td>";
				$table_content .= "<td class=\"tabletext\">".$i_InventorySystem_GroupMemberImportError[$error[$i]["type"]]."</td></tr>";
			}
		}
		$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"12\"></td></tr>";
		$table_content .= "<tr><td colspan=12 align=right>".
							$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='group_member_import.php?group_id=$group_id'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
						  "</td></tr>";
	}
}
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br>

<form name="form1" action="group_member_import_update.php" method="post">
<table border="0" width="96%" cellspacing="0" cellpadding="5">
<?=$infobar;?>
</table>
<br>
<table border="0" width="96%" cellspacing="0" cellpadding="5">
<?=$table_content;?>
</table>
<input type="hidden" name="format" value=<?=$format;?>>
<input type="hidden" name="group_id" value="<?=$group_id?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>