<?php
/**
 * 2019-05-13 [Henry] Security fix: SQL without quote
 * 2018-02-22 Henry add access right checking [Case#E135442]
 * 2012-10-11 Rita - add updation customization
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Settings_Group";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// ## Category ###
$sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID = '".$admin_group_id."'";
$arr_group_name = $linventory->returnVector($sql);
$temp[] = array(
    "<a href='group_setting.php'>" . $i_InventorySystem['Caretaker'] . "</a> > <a href='group_member_setting.php?group_id=$admin_group_id'>" . intranet_htmlspecialchars($arr_group_name[0]) . "</a> > " . $i_InventorySystem_Group_MemberPosition_Member . " > " . $button_new
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

// ## Title ###
$TAGS_OBJ[] = array(
    $i_InventorySystem['Caretaker'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$sql = "SELECT UserID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE AdminGroupID = '".$admin_group_id."'";

$result = $linventory->returnVector($sql);

if (sizeof($result) > 0) {
    $member_list = implode(",", $result);
    $cond = " UserID NOT IN ($member_list) AND ";
} else {
    $cond = " ";
}

$namefield = getNameFieldWithClassNumberByLang();

if ($sys_custom['BL_Inventory_StudentHelper']) {
    $sql = "SELECT
					UserID,
					$namefield 
			FROM
					INTRANET_USER
			WHERE
					$cond
					RecordType IN (1,2) AND
					RecordStatus IN (1)
			ORDER BY
					EnglishName";
} else {
    
    $sql = "SELECT
					UserID,
					$namefield 
			FROM
					INTRANET_USER
			WHERE
					$cond
					RecordType = 1 AND
					RecordStatus IN (0,1,2)
			ORDER BY
					EnglishName";
}
// echo $sql;
$result = $linventory->returnArray($sql, 2);

$user_selection = getSelectByArray($result, "name=TargetUser");

if ($linventory->enableMemberUpdateLocationRight()) {
    $arr_member_role = array(
        array(
            MANAGEMENT_GROUP_ADMIN,
            $i_InventorySystem_Group_MemberPosition_Leader
        ),
        array(
            MANAGEMENT_GROUP_HELPER,
            $Lang['Inventoty']['Settigs']['Helper']
        ),
        array(
            MANAGEMENT_GROUP_MEMBER,
            $i_InventorySystem_Group_MemberPosition_Member
        )
    );
} else {
    $arr_member_role = array(
        array(
            MANAGEMENT_GROUP_ADMIN,
            $i_InventorySystem_Group_MemberPosition_Leader
        ),
        array(
            MANAGEMENT_GROUP_MEMBER,
            $i_InventorySystem_Group_MemberPosition_Member
        )
    );
}

$role_selection = getSelectByArray($arr_member_role, "name=targetRole");

$table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" width=\"40%\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_ManagementGroup_SelectUser}</td>";
$table_content .= "<td class=\"tabletext\" valign=\"top\">" . $user_selection . "</td>";
$table_content .= "<tr>";
$table_content .= "<td td valign=\"top\" nowrap=\"nowrap\" width=\"40%\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem_Group_MemberPosition . "</td>";
$table_content .= "<td>$role_selection</td>";
$table_content .= "</tr>";
$table_content .= "<input type=\"hidden\" name=\"AdminGroupID\" value=\"$admin_group_id\">";

$table_content .= "<tr><td colspan=2 align=center>" . $linterface->GET_ACTION_BTN($button_submit, "submit", "") . " " . $linterface->GET_ACTION_BTN($button_reset, "reset", "", "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . " " . $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='group_member_setting.php?group_id=$admin_group_id'", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "</td></tr>";

?>

<br>
<form name="form1" method="post" action="group_member_insert_update.php">
	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $infobar1 ?></td>
		</tr>
	</table>
	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $infobar2 ?></td>
		</tr>
	</table>
	<br>
	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
	<?=$table_content?>
</table>
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>