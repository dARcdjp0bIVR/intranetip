<?php
// using: 

// ###############################################
//
// Date: 2019-07-25 (Tommy)
// add $data['itemCodeNumberOfDigit'], $data['ItemCodeSeparator'] for new item code format setting
//
// Date: 2018-02-22 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2013-01-28 YatWoon
// Improved: skip to check the item code format is already set or not [Case#2013-0125-1413-47156]
//
// Date: 2011-05-25 YatWoon
// Improved: store the setting data to table GENERAL_SETTING instead of text file
//
// ###############################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$lgeneralsettings = new libgeneralsettings();

if (sizeof($ItemCodeFormat) > 0) {
    foreach($ItemCodeFormat as $k => $d){
        if($d == 'NotUsing'){
            unset($ItemCodeFormat[$k]);
        }
    }
    $itemcode_setting = implode(",", $ItemCodeFormat);
}

// ## Check Barcode max length is changed
$originalBarcodeMaxLength = $linventory->getBarcodeMaxLength();
if ($originalBarcodeMaxLength != $barcode_max_length) {
    $maxLengthChanged = 1;
} else {
    $maxLengthChanged = 0;
}

// ## Change Barcorde format is changed
$originalBarcodeFormat = $linventory->getBarcodeFormat();
if ($originalBarcodeFormat != $barcode_format) {
    $formatChanged = 1;
} else {
    $formatChanged = 0;
}

// Store setting data to GENERAL_SETTING
$data = array();
$data['Leader_AddItemRight'] = $Leader_AddItemRight;
$data['stocktake_period_start'] = $stocktake_period_start;
$data['stocktake_period_end'] = $stocktake_period_end;
$data['warranty_expiry_warning'] = $warranty_expiry_warning;
$data['barcode_max_length'] = $barcode_max_length;
$data['barcode_format'] = $barcode_format;
$data['groupLeaderNowAllowWriteOffItem'] = $groupLeaderNowAllowWriteOffItem;
$data['StockTakePriceSingle'] = (int) $StockTakePriceSingle;
$data['StockTakePriceBulk'] = (int) $StockTakePriceBulk;
// if($linventory->checkItemCodeFormatSetting() == false)
// {
$data['itemcode_setting'] = $itemcode_setting;
// For new item code setting
$data['itemCodeNumberOfDigit'] = $item_code_digit_length;
$data['ItemCodeSeparator'] = $item_code_separator;
// }

$lgeneralsettings->Save_General_Setting($linventory->ModuleName, $data);

// set $this->
$linventory->Leader_AddItemRight = $Leader_AddItemRight;
$linventory->stocktake_period_start = $stocktake_period_start;
$linventory->stocktake_period_end = $stocktake_period_end;
$linventory->StockTakePriceSingle = $StockTakePriceSingle;
$linventory->StockTakePriceBulk = $StockTakePriceBulk;
$linventory->warranty_expiry_warning = $warranty_expiry_warning;
$linventory->barcode_max_length = $barcode_max_length;
$linventory->barcode_format = $barcode_format;
if ($linventory->checkItemCodeFormatSetting() == false) {
    $linventory->itemcode_setting = $itemcode_setting;
}
$linventory->itemCodeNumberOfDigit = $ItemCodeDigitLength;
$linventory->ItemCodeSeparator = $ItemCodeSeparator;
$linventory->groupLeaderNowAllowWriteOffItem = $groupLeaderNowAllowWriteOffItem;

// update session
foreach ($data as $name => $val)
    $_SESSION["SSV_PRIVILEGE"]["eInventory"][$name] = $val;

if (($originalBarcodeMaxLength != $barcode_max_length) || ($originalBarcodeFormat != $barcode_format)) {
    $sql = "SELECT LogID FROM INVENTORY_BARCODE_SETTING_LOG WHERE RecordStatus = 0";
    $tmp_LogID = $linventory->returnVector($sql);
    if (sizeof($tmp_LogID) > 0) {
        $targetLogID = implode(",", $tmp_LogID);
        $sql = "UPDATE INVENTORY_BARCODE_SETTING_LOG SET RecordStatus = 1 WHERE LogID IN ($targetLogID)";
        $linventory->db_db_query($sql) or die(mysql_error());
    }
    $sql = "INSERT INTO INVENTORY_BARCODE_SETTING_LOG (MaxLengthChanged, FormatChanged, RecordStatus, DateInput, DateModified) VALUES ($maxLengthChanged,$formatChanged,0,NOW(),NOW())";
    $linventory->db_db_query($sql) or die(mysql_error());
}

intranet_closedb();

$xmsg = "UpdateSuccess";
header("Location: others_setting.php?xmsg=$xmsg");
?>