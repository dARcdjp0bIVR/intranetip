<?php

// #####################################
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2012-07-05 YatWoon
// Improved: add "Status"
//
// Date: 2011-03-02 YatWoon
// re-update Display order
//
// #####################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$lf = new libfilesystem();

if ($photo_usage == 0) {
    $default_photo = "no_photo.jpg";
}

if ($default_photo == "none") {
    // Upload Photo #
    $re_path = "/file/photo/inventory";
    $path = "$intranet_root$re_path";
    $photo = stripslashes(${"hidden_cat2_photo"});
    $target = "";
    
    if ($cat2_photo == "none" || $cat2_photo == "") {} else {
        $lf = new libfilesystem();
        if (! is_dir($path)) {
            $lf->folder_new($path);
        }
        
        $ext = strtoupper($lf->file_ext($photo));
        if ($ext == ".JPG" || $ext == ".GIF" || $ext == ".PNG") {
            $target = "$path/$photo";
            $filename .= "/$photo";
            $lf->lfs_copy($cat2_photo, $target);
        }
    }
} else {
    $re_path = "/images/inventory/";
    $filename = "/$default_photo";
}

if ($default_photo != "") {
    $sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$cat_id."' AND Category2ID = '".$cat2_id."' AND ItemID = 0";
    $arr_result = $linventory->returnArray($sql, 1);
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($part_id) = $arr_result[$i];
            
            $sql = "UPDATE INVENTORY_PHOTO_PART SET PhotoPath = '$re_path', PhotoName = '$filename' WHERE PartID = '".$part_id."'";
            $linventory->db_db_query($sql);
        }
    }
    if (sizeof($arr_result) == 0) {
        $sql = "INSERT INTO INVENTORY_PHOTO_PART 
					(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
		VALUES
					($cat_id, $cat2_id, '0', '$re_path', '$filename', NOW(), NOW())";
        $linventory->db_db_query($sql);
    }
}

$sql = "SELECT PartID FROM INVENTORY_PHOTO_PART WHERE CategoryID = '".$cat_id."' AND Category2ID = '".$cat2_id."' AND ItemID = 0";
$arr_PartID = $linventory->returnArray($sql, 1);

if (sizeof($arr_PartID) > 0) {
    for ($j = 0; $j < sizeof($arr_PartID); $j ++) {
        list ($part_id) = $arr_PartID[$j];
        
        // #### Display order issue [start]
        $sql = "select DisplayOrder from INVENTORY_CATEGORY_LEVEL2 where Category2ID = '".$cat2_id."'";
        $tmpOrder = $linventory->returnVector($sql);
        $orgOrder = $tmpOrder[0];
        
        if ($orgOrder < $cat2_display_order) {
            $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=DisplayOrder-1 where CategoryID = '".$cat_id."' and DisplayOrder>$orgOrder and DisplayOrder<=$cat2_display_order";
            $linventory->db_db_query($sql);
        } else 
            if ($orgOrder > $cat2_display_order) {
                $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=DisplayOrder+1 where CategoryID = '".$cat_id."' and DisplayOrder<$orgOrder and DisplayOrder>=$cat2_display_order";
                $linventory->db_db_query($sql);
            }
        // #### Display order issue [end]
        
        $sql = "UPDATE 
						INVENTORY_CATEGORY_LEVEL2
				SET
						Code = '$cat2_code',
						NameChi = '$cat2_chi_name',
						NameEng = '$cat2_eng_name',
						DisplayOrder = '$cat2_display_order',
						PhotoLink = '$part_id',
						HasSoftwareLicenseModel = '$cat2_license',
						HasWarrantyExpiryDate = '$cat2_warranty',
						HasSerialNumber = '$cat2_serial',
						RecordStatus = '$recordstatus',
						DateModified = NOW()
				WHERE
						Category2ID = '".$cat2_id."' AND
						CategoryID = '".$cat_id."'";
        
        $result['UpdateCatL2' . $i] = $linventory->db_db_query($sql);
    }
}

if (! in_array(false, $result)) {
    
    // #### Display order issue [Start]
    // re-order
    $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 where CategoryID = '".$cat_id."' order by DisplayOrder";
    $result = $linventory->returnVector($sql);
    for ($i = 0; $i < sizeof($result); $i ++) {
        $sql = "update INVENTORY_CATEGORY_LEVEL2 set DisplayOrder=$i+1 where Category2ID='" . $result[$i]."'";
        $linventory->db_db_query($sql);
    }
    // #### Display order issue [End]
    intranet_closedb();
    
    header("location: category_level2_setting.php?category_id=" . $cat_id . "&msg=2");
} else {
    
    intranet_closedb();
    header("location: category_level2_setting.php?category_id=" . $cat_id . "&msg=14");
}
?>