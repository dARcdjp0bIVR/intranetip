<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory	= new libinventory();
//linterface->LAYOUT_START();

$layer_content .= "<table border=0 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
//$layer_content .= "<tr class=tabletop><td colspan=5 align=right><input type=button value=' X ' onClick=hideMenu('ToolMenu2')></td></tr>";

$layer_content .= "<tr class=tabletop><td colspan=5 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu2')></td></tr>";

$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_CategoryName."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Item_CategoryCode."</td></tr>";

if($item_type != "")
{
	$conds = '';
	if ($show_einventory_only == 1)
		$conds = " Where AddedFrom_eBooking != 1";
	else if ($show_ebooking_only == 1)
		$conds = " Where AddedFrom_eBooking = 1";
		
	$sql = "SELECT ".$linventory->getInventoryItemNameByLang().", Code FROM INVENTORY_CATEGORY " . $conds;
	
	$arr_result = $linventory->returnArray($sql,2);
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($item_name, $item_code) = $arr_result[$i];
			
			$layer_content .= "<tr><td class=\"tabletext\">$item_name</td>";
			$layer_content .= "<td class=\"tabletext\">$item_code</td>";
		}
	}
	if(sizeof($arr_result)==0)
	{
		$layer_content .= "<tr><td class=\"tabletex\" colspan=2>$i_no_record_exists_msg</td></tr>";
	}
}

if($item_type == 1)
	$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\" class=\"tabletext\"><font color=\"red\">$i_InventorySystem_Import_CorrectCategoryCodeWarning</font></td></tr>";
if($item_type == 2)
	$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\" class=\"tabletext\"><font color=\"red\">$i_InventorySystem_Import_CorrectCategoryCodeWarning2</font></td></tr>";
	
$layer_content .= "</table>";

$layer_content = "<br /><br />".$layer_content;

$response = $layer_content;
//$response = iconv("Big5","UTF-8",$layer_content);

echo $response;
?>