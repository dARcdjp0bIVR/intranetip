<?php
# using: yat

#########################################
#	Date:	2012-07-03	YatWoon
#			Improved: Add "status"
#			Improved: IP25 layout standard
#########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface 	= new interface_html();
$linventory		= new libinventory();
$CurrentPage	= "Settings_Category";

### Get The Category ###
$namefield = $linventory->getInventoryItemNameByLang();

$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

/*
if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Setting_Catgeory_DeleteFail");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
*/

$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "Cat.NameChi";
	$altChoice = "Cat.NameEng";
}
else
{
	$firstChoice = "Cat.NameEng";
	$altChoice = "Cat.NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$table_content = "<thead><tr>";
$table_content .= "<th width=\"1%\">#</th>";
$table_content .= "<th width=\"20%\">$i_InventorySystem_Category_Code</th>";
$table_content .= "<th width=\"35%\">$i_InventorySystem_Category_Name</th>";
$table_content .= "<th width=\"15%\">".$Lang['eInventory']['FieldTitle']['NoOfSubCategory']."</th>";

if($sys_custom['eInventory_PriceCeiling'])
{
	$table_content .= "<th width=\"10%\">$i_InventorySystem_CategorySetting_PriceCeiling</th>";
	$table_content .= "<th width=\"15%\">$i_InventorySystem_CategorySetting_PriceCeiling_ApplyToBulk</th>";
	$table_content .= "<th width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setCatChecked(1,this.form,'cat_id[]'):setCatChecked(0,this.form,'cat_id[]')\"></th>";
}

$table_content .= "<th width=\"10%\">".$Lang['eInventory']['Status']."</th>";
$table_content .= "<th width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setCatChecked(1,this.form,'cat_id[]'):setCatChecked(0,this.form,'cat_id[]')\"></th>";
$table_content .= "</tr></thead>";

if($sys_custom['eInventory_PriceCeiling']){
	$sql = "SELECT 
					Cat.CategoryID, 
					Cat.Code, 
					$namefield,
					Cat.PriceCeiling, 
					Cat.ApplyPriceCeilingToBulk,
					Count(SubCat.Category2ID),
					if(Cat.RecordStatus=1, '". $Lang['eInventory']['InUse'] ."', '". $Lang['eInventory']['NotInUse'] ."')
			FROM 
					INVENTORY_CATEGORY as Cat
					Left Outer Join
					INVENTORY_CATEGORY_LEVEL2 as SubCat
					On (Cat.CategoryID = SubCat.CategoryID)
			Group By
					Cat.CategoryID
			ORDER BY 
					Cat.DisplayOrder
			";
	$result = $linventory->returnArray($sql,5);
	
	if(sizeof($result) > 0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			$j++; 
			list($cat_id, $cat_code, $cat_name, $price_ceiling, $apply_to_bulk, $num_subcat, $recordstatus) = $result[$i];
			if($apply_to_bulk == 1){
				$str_apply_to_bulk = "Y";
			}else{
				$str_apply_to_bulk = "N";
			}
			
// 			$thisCanDelete = $linventory->Check_Can_Delete_Category($cat_id, '');
// 			$thisDisabled = ($thisCanDelete)? '' : 'disabled';
			
			$table_content .= "<tr>";
			$table_content .= "<td>$j</td>\n";
			$table_content .= "<td>".intranet_htmlspecialchars($cat_code)."</td>\n";
			$table_content .= "<td><a class=\"tablelink\" href='#' onClick='GoEditCategoryPage($cat_id)'>".intranet_htmlspecialchars($cat_name)."</a></td>\n";
			$table_content .= "<td><a class=\"tablelink\" href='#' onClick='GoSubCategoryPage($cat_id)'>".$num_subcat."</a></td>\n";
			$table_content .= "<td>$price_ceiling</td>";
			$table_content .= "<td>$str_apply_to_bulk</td>";
			$table_content .= "<td>". $recordstatus ."</td>\n";
			$table_content .= "<td><input type=\"checkbox\" name=\"cat_id[]\" value=\"$cat_id\" $thisDisabled></td>";
			$table_content .= "</tr>";
		}
	}
	else
	{
		$table_content .= "<tr>";
		$table_content .= "<td colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td>";
		$table_content .= "</tr>\n";
	}
}else{
	$sql = "SELECT 
					Cat.CategoryID, 
					Cat.Code, 
					$namefield,
					Count(SubCat.Category2ID),
					if(Cat.RecordStatus=1, '". $Lang['eInventory']['InUse'] ."', '". $Lang['eInventory']['NotInUse'] ."')
			FROM 
					INVENTORY_CATEGORY as Cat
					Left Outer Join
					INVENTORY_CATEGORY_LEVEL2 as SubCat
					On (Cat.CategoryID = SubCat.CategoryID)
			Group By
					Cat.CategoryID
			ORDER BY 
					Cat.DisplayOrder
			";
	$result = $linventory->returnArray($sql,3);
	
	if(sizeof($result) > 0)
	{
		for($i=0; $i<sizeof($result); $i++)
		{
			$j++; 
			list($cat_id, $cat_code, $cat_name, $num_subcat, $recordstatus) = $result[$i];
			
// 			$thisCanDelete = $linventory->Check_Can_Delete_Category($cat_id, '');
// 			$thisDisabled = ($thisCanDelete)? '' : 'disabled';
			
			$table_content .= "<tr>";
			$table_content .= "<td>$j</td>\n";
			$table_content .= "<td>".intranet_htmlspecialchars($cat_code)."</td>\n";
			$table_content .= "<td><a class=\"tablelink\" href='#' onClick='GoEditCategoryPage($cat_id)'>".intranet_htmlspecialchars($cat_name)."</a></td>\n";
			$table_content .= "<td><a class=\"tablelink\" href='#' onClick='GoSubCategoryPage($cat_id)'>".$num_subcat."</a></td>\n";
			$table_content .= "<td>". $recordstatus ."</td>\n";
			$table_content .= "<td><input type=\"checkbox\" name=\"cat_id[]\" value=\"$cat_id\" $thisDisabled></td>";
			$table_content .= "</tr>";
		}
	}
	else
	{
		$table_content .= "<tr>";
		$table_content .= "<td colspan=\"6\" align=\"center\">$i_no_record_exists_msg</td>";
		$table_content .= "</tr>\n";
	}
}

### eBooking & eInventory Data Warning
$WarningMsgArr = array();
if ($plugin['eBooking'] && $plugin['Inventory'])
{
	$WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['DataChangeAffect_eInventory'];
	$WarningMsgArr[] = $Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['CannotDeleteCategoryWithItem'];
	$WarningMsg = implode('<br />', $WarningMsgArr);
	// $WarningBox = $linterface->Get_Warning_Message_Box('<font color="red">'.$Lang['General']['Warning'].'</font>', $WarningMsg, $others="");
	
	$WarningBox = "<fieldset class='instruction_warning_box_v30'><legend>". $Lang['General']['Warning'] ."</legend> ". $WarningMsg ."</fieldset>";
}
?>

<script language='javascript'>
	
	function GoSubCategoryPage(cat_id)
	{
		window.location = 'category_level2_setting.php?category_id=' + cat_id;
	}
	
	function GoEditCategoryPage(cat_id)
	{
		window.location = 'category_edit.php?cat_id=' + cat_id;
	}
	
	function setCatChecked(val,obj,element){
		if(val == 1)
		{
			for(i=0; i<document.getElementsByName(element).length; i++){
				if(document.getElementsByName(element)[i].disabled == false)
				{
					document.getElementsByName(element)[i].checked = true; 
				}
			}	
		}
		else
		{
			for(i=0; i<document.getElementsByName(element).length; i++){
				if(document.getElementsByName(element)[i].disabled == false)
				{
					document.getElementsByName(element)[i].checked = false; 
				}
			}
		}
	}
	
</script>

<div class="content_top_tool">
	<div class="Conntent_tool">
		<?=$linterface->GET_LNK_NEW("javascript:checkNew('category_insert.php')","","","","",0);?>
		<?=$linterface->GET_LNK_IMPORT("category_import.php","","","","",0);?>
	</div>
<br style="clear:both" />
</div>

<?=$WarningBox?>
 
<form name="form1" action="" method="post">

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="bottom">
	<div class="common_table_tool">
		<a href="javascript:checkRemove(document.form1,'cat_id[]','category_update_status.php?status=1','<?=$Lang['eInventory']['UpdateStatusConfirm']?>')" class="tool_other"><?=$Lang['eInventory']['SetInUse']?></a>
		<a href="javascript:checkRemove(document.form1,'cat_id[]','category_update_status.php?status=-1','<?=$Lang['eInventory']['UpdateStatusConfirm']?>')" class="tool_other"><?=$Lang['eInventory']['SetNotInUse']?></a>
		<a href="javascript:checkRemove(document.form1,'cat_id[]','category_remove.php')" class="tool_delete"><?=$button_delete?></a>
	</div>
</td>
</tr>
</table>

<table class="common_table_list">
<?=$table_content?>
</table>


</div>
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

