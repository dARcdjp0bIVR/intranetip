<?php
# using: yat

#######################################
#
#	Date:	2011-07-11	YatWoon
#			- improve: "Display order" field is not a mandatory field
# 
#	Date:	2011-03-11	YatWoon
#			- update IP25 standard layout
#			- update wordings
#
#######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Category";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem['Category'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$cat_id = $category_id;
?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>


<script language="JavaScript">
isMenu = true;
</script>
<div id="ToolMenu" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
<!--
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
-->
<script language="javascript">
<!--
// AJAX follow-up
var callback = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu2", o.responseText );
        }
}
var callback2 = {
        success: function ( o )
        {
                jChangeContent( "ToolMenu3", o.responseText );
        }
}
// start AJAX
function retrieveItemCode(type)
{
    obj = document.form1;
    var myElement = document.getElementById("ToolMenu2");
                
    myElement.innerHTML = "<table border=1 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td><?=$i_InventorySystem_Loading?></td></tr></table>";
    
    YAHOO.util.Connect.setForm(obj);

    // page for processing and feedback
    var path = "getCategoryCode.php?item_type=" + type;
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}
function retrieveCategory2Index(type)
{
    obj = document.form1;
    
    if(type==1)
    	var myElement = document.getElementById("ToolMenu2");
    if(type==2)
    	var myElement = document.getElementById("ToolMenu3");
    
    myElement.innerHTML = "<table border=1 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA><tr><td><?=$i_InventorySystem_Loading?></td></tr></table>";
    
    YAHOO.util.Connect.setForm(obj);

    // page for processing and feedback
    var path = "getCategory2Index.php?item_type=" + type;
    if(type==1)
    	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
    if(type==2)
    	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

function jHIDE_DIV(InputTable)
{
	jChangeContent( InputTable,"");
}
-->
</script>

<div class="div_form_table_v30">
<form name="form1" action="category_level2_import_confirm.php" method="POST" enctype="multipart/form-data">

<table class="form_table_v30">
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
    	<?=$i_select_file?>
    </td>
    <td >
		<input class="file" type="file" name="itemfile"><br>
		<?= $linterface->GET_IMPORT_CODING_CHKBOX() ?>
	</td>
</tr>
<tr>
	<td valign="top" nowrap="nowrap" class="field_title">
		<?= $i_general_Format ?>
	</td>
	<td class="tabletext">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="inside_form_table">
			<tr>
				
				<td valign="top" class="tabletext" style="padding-top:5px;">
					<label for="format1">
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][0]['FieldTitle']?>&nbsp;[<a class="tablelink" href=javascript:retrieveItemCode(2)><?=$i_InventorySystem_Import_CheckCategory?></a>]<br>
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][1]['FieldTitle']?>&nbsp;[<a class="tablelink" href=javascript:retrieveCategory2Index(1)><?=$i_InventorySystem_Import_CheckCategory2?></a>]<br>
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][2]['FieldTitle']?><br>
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][3]['FieldTitle']?><br>
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][4]['FieldTitle']?><br>
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][5]['FieldTitle']?><br>
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][6]['FieldTitle']?><br>
							<?=$Lang['eInventory']['FieldTitle']['Import']['SubCategory'][7]['FieldTitle']?><br>
					</label><br />
					<a class="tablelink" href="<?= GET_CSV("sample_import_category2.csv")?>" target="_blank">[<?=$i_general_clickheredownloadsample?>]</a>
				</td>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" align="right" >
					<tr>
						<td>
							<span id="ToolMenu2"></span>
						</td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>


</table>

<?=$linterface->MandatoryField();?>

<input type="hidden" name="cat_id" id="cat_id" value="<?=$cat_id?>">
<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='category_level2_setting.php?category_id=".$category_id."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
<p class="spacer"></p>
</div>

</form>
</div>



<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
