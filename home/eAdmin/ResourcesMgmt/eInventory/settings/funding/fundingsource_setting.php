<?php
// Editing by 
#############################################
#
#	2013-02-07 (Carlos): added data column Barcode
#	
#	Date:	2012-07-05	YatWoon
#			Improved: Add "status"
#			Improved: IP25 layout standard
#
#	Date:	2011-07-21	YatWoon
#			add "Sponsoring body" for funding type
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_FundingSource";
$linterface 	= new interface_html();
$linventory	= new libinventory();

### Title ###
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);
		
$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";
$sql = "SELECT FundingSourceID, FundingType, Code, $namefield, if(RecordStatus=1, '". $Lang['eInventory']['InUse'] ."', '". $Lang['eInventory']['NotInUse'] ."'), Barcode FROM INVENTORY_FUNDING_SOURCE ORDER BY DisplayOrder";
$result = $linventory->returnArray($sql,4);

$table_content = "<thead><tr>";
$table_content .= "<th width=\"1%\">#</th>";
$table_content .= "<th width=\"10%\">$i_InventorySystem_Funding_Code</th>";
$table_content .= "<th width=\"50%\">{$i_InventorySystem_Setting_Funding_Name}</th>";
$table_content .= "<th width=\"10%\">$i_InventorySystem_Funding_Type</th>";
$table_content .= "<th width=\"10%\">$i_InventorySystem_Item_Barcode</th>";
$table_content .= "<th width=\"10%\">". $Lang['eInventory']['Status'] ."</th>";
$table_content .= "<th width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'funding_id[]'):setChecked(0,this.form,'funding_id[]')\"></th>";
$table_content .= "</tr></thead>";

if(sizeof($result) > 0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		$j++;
		list($funding_id, $funding_type, $funding_code, $funding_name, $recordstatus, $barcode) = $result[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		
		$table_content .= "<tr>\n";
		$table_content .= "<td>$j</td>\n";
		$table_content .= "<td>".intranet_htmlspecialchars($funding_code)."</td>\n";
		$table_content .= "<td>".intranet_htmlspecialchars($funding_name)."</td>\n";
		if($funding_type == ITEM_SCHOOL_FUNDING)
			$table_content .= "<td>". $Lang['eInventory']['FundingType']['School'] ."</td>\n";
		else if($funding_type == ITEM_GOVERNMENT_FUNDING)
			$table_content .= "<td>". $Lang['eInventory']['FundingType']['Government'] ."</td>\n";
		else
			$table_content .= "<td>". $Lang['eInventory']['FundingType']['SponsoringBody'] ."</td>\n";
		$table_content .= "<td>".intranet_htmlspecialchars($barcode)."</td>\n";
		
		$table_content .= "<td>$recordstatus</td>\n";	
		$table_content .= "<td><input type=\"checkbox\" name=\"funding_id[]\" value=\"$funding_id\"></td>\n";
		$table_content .= "</tr>\n";
	}
}
else
{
	$table_content .= "<tr>\n";
	$table_content .= "<td colspan=\"7\" align=\"center\">$i_no_record_exists_msg</td>\n";
	$table_content .= "</tr>\n";
}

// $table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"5\"></td></tr>";

?>

<div class="content_top_tool">
	<div class="Conntent_tool">
		<?=$linterface->GET_LNK_NEW("javascript:checkNew('fundingsource_insert.php')","","","","",0);?>
		<?=$linterface->GET_LNK_IMPORT("fundingsource_import.php","","","","",0);?>
	</div>
<br style="clear:both" />
</div>

<form name="form1" action="" method="post">

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="bottom">
	<div class="common_table_tool">
		<a href="javascript:checkRemove(document.form1,'funding_id[]','funding_update_status.php?status=1','<?=$Lang['eInventory']['UpdateStatusConfirm']?>')" class="tool_other"><?=$Lang['eInventory']['SetInUse']?></a>
		<a href="javascript:checkRemove(document.form1,'funding_id[]','funding_update_status.php?status=-1','<?=$Lang['eInventory']['UpdateStatusConfirm']?>')" class="tool_other"><?=$Lang['eInventory']['SetNotInUse']?></a>
		<a href="javascript:checkEdit(document.form1,'funding_id[]','fundingsource_edit.php')" class="tool_edit"><?=$button_edit?></a>
		<a href="javascript:checkRemove(document.form1,'funding_id[]','fundingsource_remove.php')" class="tool_delete"><?=$button_delete?></a>
	</div>
</td>
</tr>
</table>

<table class="common_table_list">
<?=$table_content?>
</table>

</div>
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

