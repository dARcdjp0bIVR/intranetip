<?
# using: yat

######################################
#
#
######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();
$linventory		= new libinventory();

$funding_id_list = implode(",",$funding_id);

$sql = "update INVENTORY_FUNDING_SOURCE set RecordStatus=$status where FundingSourceID in ($funding_id_list)";
$linventory->db_db_query($sql);

intranet_closedb();
header("location: fundingsource_setting.php?xmsg=UpdateSuccess");
?>