<?php
// Editing by 
######################################
#
#	Date:	2014-05-26	YatWoon
#			fixed: failed to import special character [Case#H62358]
#
#	2013-02-07 (Carlos): added random Barcode for new funding source
#
#	Date:	2012-07-05	YatWoon
#			default record status "in use"
#
#	Date:	2011-03-02	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory		= new libinventory();

$sql = "SELECT 
				Code, 
				NameChi, 
				NameEng, 
				FundingType,
				DisplayOrder 
		FROM 
				TEMP_INVENTORY_FUNDING_SOURCE";

$arr_result = $linventory->returnArray($sql,5);

if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($funding_code, $funding_chi_name, $funding_eng_name, $funding_type_id, $funding_display_order) = $arr_result[$i];
		
		$funding_chi_name = addslashes(intranet_undo_htmlspecialchars($funding_chi_name));
		$funding_eng_name = addslashes(intranet_undo_htmlspecialchars($funding_eng_name));
		
		##### Display order issue [Start]
		$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE WHERE DisplayOrder = $funding_display_order";
		$result = $linventory->returnVector($sql);
		$existingFundingSourceID = $result[0];
		if($existingFundingSourceID != "")
		{
			$sql = "update INVENTORY_FUNDING_SOURCE set DisplayOrder=DisplayOrder+1 where DisplayOrder>=$funding_display_order";
			$linventory->db_db_query($sql);
		}
		##### Display order issue [End]
		
		$Barcode = implode(",",$linventory->getUniqueBarcode(1,"funding"));
		$values = "'$funding_code', '$funding_chi_name', '".$funding_eng_name."', '$Barcode', '$funding_type_id', '$funding_display_order', 1,NOW(), NOW()";
		
		$sql = "INSERT INTO INVENTORY_FUNDING_SOURCE
						(Code, 
						NameChi, 
						NameEng, 
						Barcode,
						FundingType,
						DisplayOrder, 
						RecordStatus,
						DateInput, 
						DateModified)
				VALUES
						($values)";
		
		$result['NewInvItem'.$i] = $linventory->db_db_query($sql);
	}
	
	##### Display order issue [Start]
	# re-order
	$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE order by DisplayOrder";
	$result = $linventory->returnVector($sql);
	for($i=0;$i<sizeof($result);$i++)
	{
		$sql="update INVENTORY_FUNDING_SOURCE set DisplayOrder=$i+1 where FundingSourceID=". $result[$i];
		$linventory->db_db_query($sql) or die(mysql_error());
	}
	##### Display order issue [End]

}


if (in_array(false,$result)) {
	header("location: fundingsource_import.php?xmsg=ImportUnsuccess");
}
else {
	header("location: fundingsource_import.php?xmsg=ImportSuccess");
}
intranet_closedb();
?>