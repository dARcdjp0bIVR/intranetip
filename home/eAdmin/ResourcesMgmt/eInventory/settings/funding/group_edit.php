<?php
// Editing by Carlos
/*
 * 2013-02-06 (Carlos): add data field Barcode
 */
if(!isset($group_id))
{
	header("location: group_setting.php");
	exit();
}
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Group";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Category ###
$temp[] = array($i_InventorySystem_Setting_EditManagementGroup);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

/*
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['WriteOffReason'], $PATH_WRT_ROOT."home/admin/inventory/settings/write_off_reason/write_off_reason_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);
*/
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// get all category's Codem, Chi and Eng name
$sql = "SELECT Code, NameChi, NameEng FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID != $group_id[0]";
$GroupNameList = $linventory->returnArray($sql,3);
//

$sql = "SELECT 
				AdminGroupID, 
				Code, 
				NameChi, 
				NameEng, 
				IntranetGroupID,
				DisplayOrder,
				Barcode 
		FROM 
				INVENTORY_ADMIN_GROUP 
		WHERE 
				AdminGroupID = $group_id[0]";

$result = $linventory->returnArray($sql,7);

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($group_id, $GroupCode, $group_chi_name, $group_eng_name, $intranet_group_id, $group_display_order, $Barcode) = $result[$i];
		
		$sql = "SELECT COUNT(*) FROM INVENTORY_ADMIN_GROUP";
		$total_rec = $linventory->returnVector($sql);
		$display_order .= "<select name=\"group_display_order\">";
		
		for($j=0; $j<$total_rec[0]; $j++)
		{
			$order=$j+1;
		
			if($group_display_order==$order)
				$selected = "SELECTED=\"selected\"";
			else
				$selected = "";
			$display_order .= "<option value=\"$order\" $selected>$order</option>";
		}
		$display_order .= "</select>";
		
		//$sql = "SELECT GroupID, Title FROM INTRANET_GROUP";
		$sql = "SELECT 
				DISTINCT a.GroupID,
				a.Title
		FROM 
				INTRANET_GROUP AS a INNER JOIN 
				INTRANET_USERGROUP AS b ON (a.GroupID = b.GroupID) INNER JOIN
				INTRANET_USER AS c ON (b.UserID = c.UserID)
		WHERE
				c.RecordType = 1 AND 
				c.RecordStatus IN (1)";
		
		$targetGroup = $linventory->returnArray($sql,2);
		$intranet_group_selection = getSelectByArray($targetGroup,"name=link_intranet_group",$intranet_group_id);

		$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Caretaker_Code</td><td class=\"tabletext\" valign=\"top\"><input name=\"GroupCode\" type=\"text\" class=\"textboxnum\" maxlength=\"10\" value='".intranet_htmlspecialchars($GroupCode)."'></td></tr>\n";		
		$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_ManagementGroup_ChineseName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"group_chi_name\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($group_chi_name)."' size=\"200\" maxlength=\"200\"></td></tr>\n";
		$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_ManagementGroup_EnglishName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"group_eng_name\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($group_eng_name)."' size=\"200\" maxlength=\"200\"></td></tr>\n";
		$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Barcode}</td><td class=\"tabletext\" valign=\"top\"><input name=\"Barcode\" type=\"text\" class=\"textboxtext\" value='".intranet_htmlspecialchars($Barcode)."' size=\"200\" maxlength=\"100\">";
		$table_content .= $linterface->GET_BTN("$i_InventorySystem_Input_Item_Generate_Item_Code","Button","javascript:GenBarcode();", "", "");
		$table_content .= "</td></tr>\n";
		$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Category_DisplayOrder}</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>\n";
		//$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Import_Member_From}</td><td class=\"tabletext\" valign=\"top\">$intranet_group_selection</td></tr>\n";
		$table_content .= "<input name=\"group_id\" type=\"hidden\" value=\"$group_id\">\n";
	}
	$table_content .= "<tr><td colspan=2 align=center>".
						$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
						$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
						$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='group_setting.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
					 ."</td></tr>";
}
?>
<script language="javascript">
function checkGroupCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.GroupCode.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_Group_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
		for ($i=0;$i< sizeof($GroupNameList); $i++) {
	?>
			if (obj.GroupCode.value == "<?=$GroupNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_Group_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
		}
	?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkGroupChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
		for ($i=0;$i< sizeof($GroupNameList); $i++) {
	?>
			if (obj.group_chi_name.value == "<?=addslashes($GroupNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_Group_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
		}
	?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkGroupEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
		for ($i=0;$i< sizeof($GroupNameList); $i++) {
	?>
			if (obj.group_eng_name.value == "<?=$GroupNameList[$i]['NameEng']?>") {
					alert("<?=$i_InventorySystem_Input_Group_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
		}
	?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkBarcode()
{
	var obj = document.form1;
	if(obj.Barcode.value.Trim() == "") {
		alert('<?=$i_InventorySystem_Input_Item_Barcode_Warning?>');
		return false;
	}
	return true;
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;
	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
		
	if(check_text(obj.GroupCode,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
		if(checkGroupCode()) {
			if(check_text(obj.group_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
				if(checkGroupChiName()) {
					if(check_text(obj.group_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						if(checkGroupEngName()) {
								if(checkBarcode()) {
									passed = 1;	
								} else {
									error_cnt = 4;
									passed = 0;
								}
						}
						else {
								error_cnt = 3;
								passed = 0;
						}
					}
					else {
							passed = 0;
					}
				}
				else {
						error_cnt = 2;
						passed = 0;
				}
			}
			else {
					passed = 0;
			}
		}
		else {
				error_cnt = 1;
				passed = 0;
		}
	}
	else {
			passed = 0;
	}

				
	if(passed == 1)
	{
		obj.action = "group_edit_update.php";
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.GroupCode.focus();
		if(error_cnt == 2)
			obj.group_chi_name.focus();
		if(error_cnt == 3)
			obj.group_eng_name.focus();
		if(error_cnt == 4)
			obj.Barcode.focus();
			
		obj.action = "";
		return false;
	}
}

function GenBarcode()
{
	$.get(
		'generateAdminGroupBarcode.php',
		{},
		function(data){
			$('input[name="Barcode"]').val(data);
		}
	);
}

</script>

<br>
<form name="form1" action="" method="POST" onSubmit="return checkForm();">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar1 ?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar2 ?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content?>
</table>
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>