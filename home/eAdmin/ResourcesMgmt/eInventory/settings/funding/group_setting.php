<?php
// Editing by 
/*
 * 2013-02-06 (Carlos): Add display column Barcode
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Group";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Title ###
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Setting_ResourceManagementGroup_DeleteFail");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

//$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('group_insert.php')","","","","",0)." ";
//$toolbar .= $linterface->GET_LNK_IMPORT("group_import.php","","","","",0);
$toolbar = '';
$toolbar .= '<div class="content_top_tool">'."\n";
	$toolbar .= '<div class="Conntent_tool">'."\n";
		$toolbar .= $linterface->Get_Content_Tool_v30('new', "javascript:checkNew('group_insert.php');");
		$importOptionArr = array();
		$importOptionArr[] = array('group_import.php', $i_InventorySystem['Caretaker']);
		$importOptionArr[] = array('group_member_import.php', $i_InventorySystem_Group_Member);
		$toolbar .= $linterface->Get_Content_Tool_v30('import', "javascript:void(0);", $text="", $importOptionArr);		
	$toolbar .= '</div>'."\n";
$toolbar .= '</div>'."\n";
	
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'group_id[]','group_remove.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'group_id[]','group_edit.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
					
$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$table_content = "<tr class=\"tabletop\">";
$table_content .= "<td class=\"tabletoplink\" width=\"1%\">#</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_InventorySystem_Caretaker_Code</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"50%\">$i_InventorySystem_Group_Name</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_InventorySystem_Item_Barcode</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'group_id[]'):setChecked(0,this.form,'group_id[]')\"></td>";
$table_content .= "</tr>";

$sql = "SELECT AdminGroupID, Code, $namefield, IntranetGroupID, Barcode FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder ";

$result = $linventory->returnArray($sql,5);

if(sizeof($result) > 0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		$j++;
		list($admin_gp_id, $admin_gp_code ,$admin_name, $intranet_gp_id, $barcode) = $result[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		
		$table_content .= "<tr class=\"$css\">\n";
		$table_content .= "<td class=\"tabletext\">$j</td>\n";
		$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($admin_gp_code)."</td>\n";
		$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href='group_member_setting.php?group_id=$admin_gp_id'>".intranet_htmlspecialchars($admin_name)."</a></td>\n";
		$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($barcode)."</td>\n";
		$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"group_id[]\" value=\"$admin_gp_id\"></td>\n";
		$table_content .= "</tr>\n";
		$table_content .= "<input type=\"hidden\" name=\"level_id\" value=$level_id>";
	}
}
else
{
	$table_content .= "<tr class=\"tablerow2 tabletext\">\n";
	$table_content .= "<td class=\"tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td>\n";
	$table_content .= "</tr>\n";
}
$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"5\"></td></tr>";

?>

<br>
<form name="form1" action="" method="post">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar ?></td>
	</tr>
</table>

<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>

<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
</form>
</br>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>