<?php

######################################
#
#	Date:	2012-07-05	YatWoon
#			update system message
#
#	Date:	2011-03-01	YatWoon
#			re-update Display order
#
######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory	= new libinventory();

$funding_id_list = implode(",",$funding_id);

if($funding_id_list != "")
{
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE FundingSource IN ($funding_id_list)";
	$arr_result_single = $linventory->returnArray($sql,1);
	
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_BULK_LOG WHERE FundingSource In ($funding_id_list)";
	$arr_result_bulk = $linventory->returnArray($sql,1);
	
	if((sizeof($arr_result_single)>0) || (sizeof($arr_result_bulk)>0))
	{
		header("location: fundingsource_setting.php?xmsg=DeleteUnsuccess");
	}
	else
	{
		$sql = "DELETE FROM INVENTORY_FUNDING_SOURCE WHERE FundingSourceID IN ($funding_id_list)";
		$result = $linventory->db_db_query($sql);
		
		if($result)
		{
			##### Display order issue [Start]
			# re-order
			$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE order by DisplayOrder";
			$result = $linventory->returnVector($sql);
			for($i=0;$i<sizeof($result);$i++)
			{
				$sql="update INVENTORY_FUNDING_SOURCE set DisplayOrder=$i+1 where FundingSourceID=". $result[$i];
				$linventory->db_db_query($sql);
			}
			##### Display order issue [End]
		
			header("location: fundingsource_setting.php?xmsg=DeleteSuccess");
		}
		else
		{
			header("location: fundingsource_setting.php?xmsg=DeleteUnsuccess");
		}
	}
}
else
{
	header("location: fundingsource_setting.php?xmsg=DeleteUnsuccess");
}
intranet_closedb();
?>