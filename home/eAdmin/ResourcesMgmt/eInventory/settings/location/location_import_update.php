<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$curr_date = date("Y-m-d");

$sql = "SELECT 
				LocationLevelID,
				Code, 
				NameChi, 
				NameEng, 
				DisplayOrder 
		FROM 
				TEMP_INVENTORY_LOCATION";

$arr_result = $linventory->returnArray($sql,5);

if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($location_level_id,$location_code,$location_namechi, $location_nameeng, $location_displayorder) = $arr_result[$i];
						
		$values = "'$location_level_id','$location_code', '".addslashes($location_namechi)."', '".$location_nameeng."', '$location_displayorder', NOW(), NOW()";
		
		$sql = "INSERT INTO INVENTORY_LOCATION
						(LocationLevelID,
						Code, 
						NameChi, 
						NameEng, 
						DisplayOrder, 
						DateInput, 
						DateModified)
				VALUES
						($values)";
		$result['NewInvItem'.$i] = $linventory->db_db_query($sql);
		
	}

}

if (in_array(false,$result)) {
	header("location: location_import.php?msg=12");
}
else {
	header("location: location_import.php?msg=1");
}
intranet_closedb();
?>