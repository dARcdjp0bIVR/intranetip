<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Location";
$linterface 	= new interface_html();
$linventory		= new libinventory();

### Category ###
//$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['Settings']." > ".$i_InventorySystem_Location_Level." > ".$button_add)."</td></tr>"; 
$temp[] = array($i_InventorySystem_Setting_NewLocation);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 
//$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem_Location_Level." > ".$button_add)."</td></tr>"; 

/*
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['WriteOffReason'], $PATH_WRT_ROOT."home/admin/inventory/settings/write_off_reason/write_off_reason_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);
*/
$TAGS_OBJ[] = array($i_InventorySystem['Location'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// get all category's Codem, Chi and Eng name
$sql = "SELECT Code, NameChi, NameEng FROM INVENTORY_LOCATION_LEVEL";
$LocationNameList = $linventory->returnArray($sql,3);
//

$sql = "SELECT COUNT(*) FROM INVENTORY_LOCATION_LEVEL";
$total_rec = $linventory->returnVector($sql);
$display_order .= "<select name=\"level_display_order\">";
for($i=0; $i<=$total_rec[0]; $i++)
{
	$j=$i+1;
	if($j==$total_rec[0])
		$selected = "SELECTED=\"selected\"";
	$display_order .= "<option value=\"$j\" $selected>$j</option>";
}
$display_order .= "</select>";

$table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Location_Level_Code</td><td class=\"tabletext\" valign=\"top\"><input name=\"LocationCode\" type=\"text\" class=\"textboxnum\" maxlength=\"10\" value=\"$LocationCode\"></td></tr>";
$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_Location_ChineseName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"level_chi_name\" type=\"text\" class=\"textboxtext\" size=\"200\"></td></tr>\n";
$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_Location_EnglishName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"level_eng_name\" type=\"text\" class=\"textboxtext\" size=\"200\"></td></tr>\n";
$table_content .= "<tr><td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Category_DisplayOrder}</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>\n";

$table_content .= "<tr><td colspan=2 align=center>".
					$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
					$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ".
					$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='location_level_setting.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")
				 ."</td></tr>\n";
				 
?>
<script language="javascript">
function checkLocationLevelCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.LocationCode.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_LocationLevel_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
		for ($i=0;$i< sizeof($LocationNameList); $i++) {
	?>
			if (obj.LocationCode.value == "<?=$LocationNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_LocationLevel_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
		}
	?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkLocationLevelChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
		for ($i=0;$i< sizeof($LocationNameList); $i++) {
	?>
			if (obj.level_chi_name.value == "<?=addslashes($LocationNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_LocationLevel_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
		}
	?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkLocationLevelEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
		for ($i=0;$i< sizeof($LocationNameList); $i++) {
	?>
			if (obj.level_eng_name.value == "<?=$LocationNameList[$i]['NameEng']?>") {
					alert("<?=$i_InventorySystem_Input_LocationLevel_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
		}
	?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;

	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
	
	
	if(check_text(obj.LocationCode,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
		if(checkLocationLevelCode()) {
			if(check_text(obj.level_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
				if(checkLocationLevelChiName()) {
					if(check_text(obj.level_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						if(checkLocationLevelEngName()) {
								passed = 1;	
						}
						else {
								error_cnt = 3;
								passed = 0;
						}
					}
					else {
							passed = 0;
					}
				}
				else {
						error_cnt = 2;
						passed = 0;
				}
			}
			else {
					passed = 0;
			}
		}
		else {
				error_cnt = 1;
				passed = 0;
		}
	}
	else {
			passed = 0;
	}
				
	if(passed == 1)
	{
		obj.action = "location_level_insert_update.php";
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.LocationCode.focus();
		if(error_cnt == 2)
			obj.level_chi_name.focus();
		if(error_cnt == 3)
			obj.level_eng_name.focus();
			
		obj.action = "";
		return false;
	}
}

</script>

<br>
<form name="form1" method="post" action="" onSubmit="return checkForm();">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar1 ?></td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right"><?= $infobar2 ?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
<input type="hidden" name="attachStr" value="" />
</form>
</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>