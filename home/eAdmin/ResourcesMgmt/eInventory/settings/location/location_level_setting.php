<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface 	= new interface_html();
$linventory		= new libinventory();
$CurrentPage	= "Settings_Location";


### Button
//$AddBtn 	= "<a href=\"javascript:checkNew('house_new.php')\" class='contenttool'><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_new.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\" /> " . $button_new . "</a>";
//$delBtn 	= "<a href=\"javascript:checkRemove(document.form1,'level_id[]','house_category_remove.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_remove . "</a>";
//$editBtn 	= "<a href=\"javascript:checkEdit(document.form1,'level_id[]','house_category_edit.php')\" class=\"tabletool\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\" /> " . $button_edit . "</a>";	

### Category ###
//$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem['Settings']." > ".$i_InventorySystem_Location_Level)."</td></tr>"; 
//$temp[] = array($i_InventorySystem['Settings']);
//$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

### Title ###
/*
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['WriteOffReason'], $PATH_WRT_ROOT."home/admin/inventory/settings/write_off_reason/write_off_reason_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);
*/
$TAGS_OBJ[] = array($i_InventorySystem['Location'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

//$SysMsg = $linventory->getResponseMsg($msg);

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	//if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Setting_Location_DeleteFail");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('location_level_insert.php')")."&nbsp;";
$toolbar .= $linterface->GET_LNK_IMPORT("location_level_import.php");		
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'level_id[]','location_level_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'level_id[]','location_level_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
					
$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$table_content = "<tr class=\"tabletop\">";
$table_content .= "<td class=\"tabletoplink\" width=\"1%\">#</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"20%\">$i_InventorySystem_Location_Level_Code</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"70%\">$i_InventorySystem_Location_Level_Name</td>";
$table_content .= "<td class=\"tabletoplink\" width=\"1%\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'level_id[]'):setChecked(0,this.form,'level_id[]')\"></td>";
$table_content .= "</tr>";

$sql = "SELECT LocationLevelID, Code, $namefield FROM INVENTORY_LOCATION_LEVEL ORDER BY DisplayOrder";

$result = $linventory->returnArray($sql,3);

if(sizeof($result) > 0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		$j++;
		list($level_id, $level_code, $level_name) = $result[$i];
		$css = $i%2==0?"tablerow1 tabletext":"tablerow2 tabletext";
		
		$table_content .= "<tr class=\"$css\">";
		$table_content .= "<td class=\"tabletext\">$j</td>\n";
		$table_content .= "<td class=\"tabletext\">".intranet_htmlspecialchars($level_code)."</td>\n";
		$table_content .= "<td class=\"tabletext\"><a class=\"tablelink\" href=location_setting.php?level_id=$level_id>".intranet_htmlspecialchars($level_name)."</a></td>\n";
		$table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"level_id[]\" value=\"$level_id\"></td>";
		$table_content .= "</tr>";
	}
}
else
{
	$table_content .= "<tr class=\"tablerow2 tabletext\">";
	$table_content .= "<td class=\"tabletext\" colspan=\"4\" align=\"center\">$i_no_record_exists_msg</td>";
	$table_content .= "</tr>\n";
}

$table_content .= "<tr height=\"25px\"><td class=\"tablebottom\" colspan=\"4\"></td></tr>";

?>

<br>
<form name="form1" action="" method="post">
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr>
	<td colspan="2" align="right"><?= $infobar1 ?></td>
</tr>
</table>

<table width="90%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>

<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$table_content?>
</table>
</form>
</br>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>

