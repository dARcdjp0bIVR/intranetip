<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "StockList_Location";
$linterface 	= new interface_html();
$linventory	= new libinventory();

//$TAGS_OBJ[] = array($i_InventorySystem['Location'], "", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/settings/category/category_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Location'], $PATH_WRT_ROOT."home/admin/inventory/settings/location/location_level_setting.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Caretaker'], $PATH_WRT_ROOT."home/admin/inventory/settings/group/group_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['FundingSource'], $PATH_WRT_ROOT."home/admin/inventory/settings/funding/fundingsource_setting.php", 0);
$TAGS_OBJ[] = array($i_InventorySystem['Others'], $PATH_WRT_ROOT."home/admin/inventory/settings/others/others_setting.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
#added by Kelvin Ho 2008-12-19 (get the other category order if exists)
$sql = "select DisplayOrder from INVENTORY_LOCATION_LEVEL where LocationLevelID = $level_id";
$tmpOrder = $linventory->returnVector($sql);
$orgOrder = $tmpOrder[0];
        
$sql = "UPDATE 
				INVENTORY_LOCATION_LEVEL 
		SET
				Code = '$LocationCode',
				NameChi = '$location_chi_name',
				NameEng = '$location_eng_name',
				DisplayOrder = $location_display_order,
				DateModified = NOW()
		WHERE
				LocationLevelID = $level_id";

$result = $linventory->db_db_query($sql);

#added by Kelvin Ho 2008-12-19 (modify the other category order if exists)
$sql = "select LocationLevelID from INVENTORY_LOCATION_LEVEL where DisplayOrder = '$location_display_order' and LocationLevelID!=$level_id";
$tmpOrder = $linventory->returnVector($sql);
$order = $tmpOrder[0];
if($order)
{
	$sql = "update INVENTORY_LOCATION_LEVEL set DisplayOrder = '$orgOrder' where LocationLevelID = ".$order;
    $linventory->db_db_query($sql);
}

if($result)
{
	header("location: location_level_setting.php?msg=2");
}
else
{	
	header("location: location_level_setting.php?msg=14");
}

intranet_closedb();
?>