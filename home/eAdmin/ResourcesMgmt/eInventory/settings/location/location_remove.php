<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Settings_Location";
$linterface = new interface_html();
$linventory	= new libinventory();

$location_id_list = implode(",",$location_id);

if($location_id_list != "")
{
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE LocationID IN ($location_id_list)";
	$arr_result_single = $linventory->returnArray($sql,1);
	
	$sql = "SELECT ItemID FROM INVENTORY_ITEM_BULK_LOG WHERE LocationID IN ($location_id_list)";
	$arr_result_bulk = $linventory->returnArray($sql,1);
	
	if((sizeof($arr_result_single)>0) || (sizeof($arr_result_bulk)>0))
	{
		header("location: location_setting.php?level_id=".$level_id."&msg=13");
	}
	else
	{
		$sql = "DELETE FROM INVENTORY_LOCATION WHERE LocationID IN ($location_id_list)";
		$result = $linventory->db_db_query($sql);
				
		if($result)
		{
			header("location: location_setting.php?level_id=".$level_id."&msg=3");
		}
		else
		{
			header("location: location_setting.php?level_id=".$level_id."&msg=13");
		}
	}
}
else
{
	header("location: location_setting.php?level_id=".$level_id."&msg=13");
}
intranet_closedb();
?>