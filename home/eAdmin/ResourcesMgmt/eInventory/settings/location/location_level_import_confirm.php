<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Settings_Location";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$limport 		= new libimporttext();
$li 			= new libdb();
$lo 			= new libfilesystem();

$filepath 		= $itemfile;
$filename 		= $itemfile_name;

/*if($format == 1)
	$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($button_new." > ".$i_InventorySystem_ItemType_Single)."</td></tr>"; 
if($fomrat == 2)
	$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($button_new." > ".$i_InventorySystem_ItemType_Bulk)."</td></tr>"; 
	*/


$file_format = array("Location Code","Chinese Name","English Name","Display Order");
//$item_type = 1;
	
# Create temp single item table
$sql = "DROP TABLE TEMP_INVENTORY_LOCATION_LEVEL";
$li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_LOCATION_LEVEL
		(
		 Code varchar(10),
		 NameChi varchar(255),
		 NameEng varchar(255),
		 DisplayOrder int(10)
	    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql);


$ext = strtoupper($lo->file_ext($filename));
if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import_item.php?msg=15");
	exit();
}

if($limport->CHECK_FILE_EXT($filename))
{
	# read file into array
	# return 0 if fail, return csv array if success
	//$data = $lo->file_read_csv($filepath);
	//$col_name = array_shift($data);                   # drop the title bar
	$data = $limport->GET_IMPORT_TXT($filepath);
	$col_name = array_shift($data);
	
	# check the csv file's first row is correct or not
	$format_wrong = false;
	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}
	
	if($format_wrong)
	{
		header("location: location_level_import.php?msg=15");
		exit();
	}
	
	### Remove Empty Row in CSV File ###
	for($i=0; $i<sizeof($data); $i++)
	{
		if(sizeof($data[$i])!=0)
		{
			$arr_new_data[] = $data[$i];
			$record_row++;
		}
		else
		{
			$empty_row++;
		}
	}
	$file_original_row = sizeof($data);
	$file_new_row = sizeof($arr_new_data);
		
	for($i=0; $i<sizeof($arr_new_data); $i++)
	{
		list($location_code,$location_chi_name,$location_eng_name,$location_display_order) = $arr_new_data[$i];
		
						
		# Check location Code Exist #
		if($location_code != "")
		{
			$sql = "SELECT Code	FROM INVENTORY_LOCATION_LEVEL WHERE Code = '$location_code'";
			$arr_result = $linventory->returnArray($sql,1);
			
			if(sizeof($arr_result)>0)
				$error[$i]['type'] = 1;
		}
		else
		{
			$error[$i]['type'] = 2;
		}
		
		# Check Item Chi Name #
		if($location_chi_name == "")
			$error[$i]['type'] = 3;
		
		# Check Item Eng Name #
		if($location_eng_name == "")
			$error[$i]['type'] = 4;
		
		# Check Item Display Order #
		if($location_display_order == "")
			$error[$i]['type'] = 5;	

		$location_chi_name = intranet_htmlspecialchars(addslashes($location_chi_name));
		$location_eng_name = intranet_htmlspecialchars(addslashes($location_eng_name));	
			
		$values = "('$location_code','$location_chi_name','$location_eng_name','$location_display_order')";
		$sql = "INSERT INTO TEMP_INVENTORY_LOCATION_LEVEL (Code, NameChi, NameEng, DisplayOrder) VALUES $values";
		$linventory->db_db_query($sql);
					
		# check any duplicate location code in the csv file
		$sql = "SELECT Code FROM TEMP_INVENTORY_LOCATION_LEVEL";
		$arr_tmp_checkLocationCode = $linventory->returnVector($sql);
		if(sizeof($arr_tmp_checkLocationCode) != sizeof(array_unique($arr_tmp_checkLocationCode)))
			$error[$i]['type'] = 19;
		
	}
	
	### Show Import Result ###
	if($record_row == "")
		$record_row = 0;
	if($empty_row == "")
		$empty_row = 0;
	$table_content .= "<tr>";
	$table_content .= "<td class=\"tabletext\" colspan=\"26\">
							$i_InventorySystem_ImportItem_TotalRow: $file_original_row<br>
							$i_InventorySystem_ImportItem_RowWithRecord: $record_row<br>
							$i_InventorySystem_ImportItem_EmptyRowRecord: $empty_row
						</td>";
	$table_content .= "</tr>";
	### END ###
	$table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_LocationCode</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Category_ChineseName</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Category_EnglishName</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Category_DisplayOrder</td>";
		
	if(sizeof($error)>0)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
	}
	$table_content .= "</tr>\n";
	
	if(sizeof($error) == 0)
	{
		$sql = "SELECT 
					Code, 
					NameChi, 
					NameEng, 
					DisplayOrder
				FROM
					TEMP_INVENTORY_LOCATION_LEVEL";
					
		$arr_result = $linventory->returnArray($sql,4);

		if(sizeof($arr_result) > 0)
		{
			for ($i=0; $i<sizeof($arr_result); $i++)
			{
				$j=$i+1;
				if($j%2 == 0)
					$table_row_css = " class=\"tablerow1\" ";
				else
					$table_row_css = " class=\"tablerow2\" ";
				
				list($location_code, $location_chi_name, $location_eng_name, $location_display_order) = $arr_result[$i];
								
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$location_code</td>";
				$table_content .= "<td class=\"tabletext\">$location_chi_name</td>";
				$table_content .= "<td class=\"tabletext\">$location_eng_name</td>";
				$table_content .= "<td class=\"tabletext\">$location_display_order</td></tr>";
			}
		}
		$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"12\"></td></tr>";
		$table_content .= "<tr><td colspan=12 align=right>".
							$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
							$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='location_level_import.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
						  "</td></tr>";
	}
	if(sizeof($error) > 0)
	{
		for($i=0; $i<sizeof($arr_new_data); $i++)
		{
			$j=$i+1;
			if($j%2 == 0)
				$table_row_css = " class=\"tablerow1\" ";
			else
				$table_row_css = " class=\"tablerow2\" ";
				
			list($location_code,$item_chi_name,$item_eng_name,$location_display_order) = $arr_new_data[$i];
							
			if($error[$i]["type"] == "")
			{
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$location_code</td>";
				$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
				$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
				$table_content .= "<td class=\"tabletext\">$location_display_order</td>";
				$table_content .= "<td class=\"tabletext\"> - </td></tr>";
			}
			if($error[$i]["type"] != "")
			{
				$table_content .= "<tr $table_row_css><td class=\"tabletext\">$location_code</td>";
				$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
				$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
				$table_content .= "<td class=\"tabletext\">$location_display_order</td>";
				
				$table_content .= "<td class=\"tabletext\">".$i_InventorySystem_LocationLevelImportError[$error[$i]["type"]]."</td></tr>";
			}
		}
		$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"12\"></td></tr>";
		$table_content .= "<tr><td colspan=12 align=right>".
							$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='location_level_import.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
						  "</td></tr>";
	}
	
	
}
$TAGS_OBJ[] = array($i_InventorySystem['Location'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br>

<form name="form1" action="location_level_import_update.php" method="post">
<table border="0" width="96%" cellspacing="0" cellpadding="5">
<?=$infobar;?>
</table>
<br>
<table border="0" width="96%" cellspacing="0" cellpadding="5">
<?=$table_content;?>
</table>
<input type="hidden" name="format" value=<?=$format;?>>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>