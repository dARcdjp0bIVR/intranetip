<?php
// Using:

/**
 * **************************** Change Log [Start] *******************************
 * 2019-05-13 (Henry): Security fix: SQL without quote
 * 2018-02-26 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log [End] ********************************
 */
if (! isset($location_id)) {
    header("location: location_level_setting.php");
    exit();
}
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable{$LAYOUT_SKIN}.php");

intranet_auth();
intranet_opendb();

$CurrentPage = "Settings_Location";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// ## Category ###
$sql = "SELECT " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION_LEVEL WHERE LocationLevelID = '".$level_id."'";
$arr_level_name = $linventory->returnVector($sql);
$temp[] = array(
    "<a href='location_level_setting.php'>" . $i_InventorySystem_Location_Level . "</a> > <a href='location_setting.php?level_id=$level_id'>" . intranet_htmlspecialchars($arr_level_name[0]) . "</a> > " . $i_InventorySystem_Location . " > " . $button_edit
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

$TAGS_OBJ[] = array(
    $i_InventorySystem['Location'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// get all category's Codem, Chi and Eng name
$sql = "SELECT Code, NameChi, NameEng FROM INVENTORY_LOCATION WHERE LocationID != '".$location_id[0]."'";
$LocationNameList = $linventory->returnArray($sql, 3);
//

$sql = "SELECT 
				Code,
				NameChi, 
				NameEng, 
				DisplayOrder 
		FROM 
				INVENTORY_LOCATION 
		WHERE 
				LocationID = '".$location_id[0]."' AND 
				LocationLevelID = '".$level_id."'";
$result = $linventory->returnArray($sql, 3);

if (sizeof($result) > 0) {
    for ($i = 0; $i < sizeof($result); $i ++) {
        list ($location_code, $location_chi_name, $location_eng_name, $location_display_order) = $result[$i];
        
        $sql = "SELECT COUNT(*) FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$level_id."'";
        
        $total_rec = $linventory->returnVector($sql);
        $display_order .= "<select name=\"location_display_order\">";
        for ($i = 0; $i < $total_rec[0]; $i ++) {
            $j = $i + 1;
            
            if ($location_display_order == $j) {
                $selected = "SELECTED=\"selected\"";
            } else {
                $selected = "";
            }
            
            $display_order .= "<option value=\"$j\" $selected>$j</option>";
        }
        $display_order .= "</select>";
        
        $table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Location_Code</td><td class=\"tabletext\" valign=\"top\"><input name=\"LocationCode\" type=\"text\" class=\"textboxnum\" maxlength=\"10\" value='" . intranet_htmlspecialchars($location_code) . "'></td></tr>\n";
        $table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_SubLocation_ChineseName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"location_chi_name\" type=\"text\" class=\"textboxtext\" value='" . intranet_htmlspecialchars($location_chi_name) . "'></td></tr>\n";
        $table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Setting_SubLocation_EnglishName}</td><td class=\"tabletext\" valign=\"top\"><input name=\"location_eng_name\" type=\"text\" class=\"textboxtext\" value='" . intranet_htmlspecialchars($location_eng_name) . "'></td></tr>\n";
        $table_content .= "<tr><td valign=\"top\" width=\"35%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">{$i_InventorySystem_Category_DisplayOrder}</td><td class=\"tabletext\" valign=\"top\">$display_order</td></tr>\n";
        $table_content .= "<input type=\"hidden\" width=\"35%\" name=\"level_id\" value=$level_id>\n";
        $table_content .= "<input type=\"hidden\" name=\"location_id\" value=$location_id[0]>\n";
        
        $table_content .= "<tr><td colspan=2 align=center>" . $linterface->GET_ACTION_BTN($button_submit, "submit", "") . " " . $linterface->GET_ACTION_BTN($button_reset, "reset", "", "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . " " . $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "</td></tr>\n";
    }
}

?>
<script language="javascript">
function checkLocationCode()
{
	var obj = document.form1;
	var failed_1 = 0;
	var re = /[~`!@#$%^&*\(\)-+=\[\]\{\}:;'"\<\>,.?]/i;
	if(obj.LocationCode.value.match(re))
	{
		alert("<?=$i_InventorySystem_Input_Location_Code_RegExp_Warning?>");
		failed_1++;
	}
	else
	{
	<?
for ($i = 0; $i < sizeof($LocationNameList); $i ++) {
    ?>
			if (obj.LocationCode.value == "<?=$LocationNameList[$i]['Code']?>") {
					alert("<?=$i_InventorySystem_Input_Location_Code_Exist_Warning?>");
					failed_1++;
			}
	<?
}
?>
	}
	if(failed_1 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkLocationChiName()
{
	var obj = document.form1;
	var failed_2 = 0;
	
	<?
for ($i = 0; $i < sizeof($LocationNameList); $i ++) {
    ?>
			if (obj.location_chi_name.value == "<?=addslashes($LocationNameList[$i]['NameChi'])?>") {
					alert("<?=$i_InventorySystem_Input_Location_Name_Exist_Warning?>");
					failed_2++;
			}
	<?
}
?>
	if(failed_2 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkLocationEngName()
{
	var obj = document.form1;
	var failed_3 = 0;
		
	<?
for ($i = 0; $i < sizeof($LocationNameList); $i ++) {
    ?>
			if (obj.location_eng_name.value == "<?=$LocationNameList[$i]['NameEng']?>") {
					alert("<?=$i_InventorySystem_Input_Location_Name_Exist_Warning?>");
					failed_3++;
			}
	<?
}
?>
	if(failed_3 > 0)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function checkForm()
{
	var passed = 0;
	var obj = document.form1;

	var error_cnt = 0;
	var tmp_1,tmp_2,tmp_3;
	
	
	if(check_text(obj.LocationCode,"<?=$i_InventorySystem_Input_Category_Code_Warning;?>")) {
		if(checkLocationCode()) {
			if(check_text(obj.location_chi_name,"<?=$i_InventorySystem_Input_Category_Item_ChineseName_Warning;?>")) {
				if(checkLocationChiName()) {
					if(check_text(obj.location_eng_name,"<?=$i_InventorySystem_Input_Category_Item_EnglishName_Warning;?>")) {
						if(checkLocationEngName()) {
								passed = 1;	
						}
						else {
								error_cnt = 3;
								passed = 0;
						}
					}
					else {
							passed = 0;
					}
				}
				else {
						error_cnt = 2;
						passed = 0;
				}
			}
			else {
					passed = 0;
			}
		}
		else {
				error_cnt = 1;
				passed = 0;
		}
	}
	else {
			passed = 0;
	}
				
	if(passed == 1)
	{
		obj.action = "location_edit_update.php";
		return true;
	}
	else
	{
		if(error_cnt == 1)
			obj.LocationCode.focus();
		if(error_cnt == 2)
			obj.location_chi_name.focus();
		if(error_cnt == 3)
			obj.location_eng_name.focus();
			
		obj.action = "";
		return false;
	}
}

</script>

<br>

<form name="form1" action="" method="POST"
	onSubmit="return checkForm();">
	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $infobar1 ?></td>
		</tr>
	</table>
	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right"><?= $infobar2 ?></td>
		</tr>
	</table>
	<br>
	<table width="90%" border="0" cellpadding="5" cellspacing="0"
		align="center">
<?=$table_content?>
</table>
</form>

</br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>