<?php
// Using: 
/**
 * ******************************* Modification Log *************************************
 * 2020-02-05 Tommy
 *  Customization for plkchc #A170032
 *
 * **************************************************************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Report_FixedAssetsRegister_PLKCHC";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();
$llocation_ui = new liblocation_ui();

// ## Check user have set all the basic setting ###
$sql = "SELECT LocationID FROM INVENTORY_LOCATION";
$arr_location_check = $linventory->returnVector($sql);

$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
$arr_group_check = $linventory->returnVector($sql);

$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY";
$arr_category_check = $linventory->returnVector($sql);

$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE";
$arr_funding_check = $linventory->returnVector($sql);

if ((sizeof($arr_location_check) == 0) || (sizeof($arr_group_check) == 0) || (sizeof($arr_category_check) == 0) || (sizeof($arr_funding_check) == 0)) {
    if ($sys_custom['PowerClass']) {
        header("Location: " . $PATH_WRT_ROOT . "home/PowerClass/reports.php?error=MissingBasicSetting");
    } else {
        header("Location: " . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php");
    }
}
// ## END ###

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_FixedAssetsRegister,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arr_file_type = array(
    array(
        3,
        "PDF"
    ),
    array(
        1,
        "HTML"
    ),
    array(
        2,
        "CSV"
    )
);
$file_type_selection = getSelectByArray($arr_file_type, "name=\"targetFileType\" onChange=\"if(this.options[this.selectedIndex].value==2){displayTable('display_options', 'none');jsDisplayFieldByClass('HTMLonly', 0);}else{displayTable('display_options', '');jsDisplayFieldByClass('HTMLonly', 1);}\"", $targetFileType, 0, 1);
$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_FileFormat</td>";
$table_selection .= "<td class=\"tabletext\">$file_type_selection</td></tr>";

// to be enabled later for SMC or IMC customization
$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Purchase_Date</td>";
if (isset($PurchaseDateFrom)) {
    $date_from = $PurchaseDateFrom;
} else {
    $date_from = ($PurchaseDateFrom != "") ? $PurchaseDateFrom : date("Y-m-d", getStartOfAcademicYear());
}
if (isset($PurchaseDateTo)) {
    $date_to = $PurchaseDateTo;
} else {
    $date_to = ($PurchaseDateTo != "") ? $PurchaseDateTo : date("Y-m-d", getEndOfAcademicYear());
}
// $purchase_period = "<table border='0' ><tr>
// <td>".strtolower($Lang['General']['From'])." </td>
// <td>" . $linterface->GET_DATE_FIELD("PurchaseDateFrom", "form1", "PurchaseDateFrom", $date_from) . "</td>
// <td nowrap='nowrap'>&nbsp; ".strtolower($Lang['General']['To'])." </td>
// <td>" . $linterface->GET_DATE_FIELD("PurchaseDateTo", "form1", "PurchaseDateTo", $date_to) . "</td>
// </tr></table>";

$purchase_period = "<table border='0' ><tr>
					<td>" . strtolower($Lang['General']['From']) . " </td>
					<td>" . $linterface->GET_DATE_PICKER("PurchaseDateFrom", $date_from, "", "", "", "", "", "", "", "", 1) . "</td>
					<td nowrap='nowrap'>&nbsp; " . strtolower($Lang['General']['To']) . " </td>
					<td>" . $linterface->GET_DATE_PICKER("PurchaseDateTo", $date_to, "", "", "", "", "", "", "", "", 1) . "</td>
					</tr></table>";

$table_selection .= "<td class=\"tabletext\">$purchase_period</td></tr>";

// display items
if ($targetFileType == 2) {
    $display_option_hide = "style='display:none;'";
}
$table_selection .= "<tr  ><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['AccountMgmt']['display'] . "</td>";
$table_selection .= "<td><i>" . $Lang['eInventory']['DisplayColumns'] . "</i>";

$table_selection .= "<div id='display_options' {$display_option_hide} >";

/*
 * $ShowSchoolLogoChecked = ($ShowSchoolLogo || !isset($groupBy)) ? "checked='checked'" : "";
 * $table_selection .= "<div><input type='checkbox' name='ShowSchoolLogo' id='ShowSchoolLogo' value='1' $ShowSchoolLogoChecked> <label for='ShowSchoolLogo'>". $i_adminmenu_basic_settings_badge ."</label> </div>\n";
 */

$ShowItemCodeChecked = ($ShowItemCode || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowItemCode' id='ShowItemCode' value='1'' $ShowItemCodeChecked> <label for='ShowItemCode'>$i_InventorySystem_Item_Code</label> </div>\n";

$ShowCategoryChecked = ($ShowCategory || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowCategory' id='ShowCategory' value='1' $ShowCategoryChecked> <label for='ShowCategory'>" . $i_InventorySystem['Category'] . "</label> </div>\n";

$ShowSubCategoryChecked = ($ShowSubCategory || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowSubCategory' id='ShowSubCategory' value='1' $ShowSubCategoryChecked> <label for='ShowSubCategory'>" . $i_InventorySystem['SubCategory'] . "</label> </div>\n";

$ShowUnitPriceChecked = ($ShowUnitPrice || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowUnitPrice' id='ShowUnitPrice' value='1'' $ShowUnitPriceChecked> <label for='ShowUnitPrice'>$i_InventorySystem_Unit_Price (" . $Lang['eInventory']['Avg'] . ")</label> </div>\n";

$ShowPurchasePriceChecked = ($ShowPurchasePrice || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowPurchasePrice' id='ShowPurchasePrice' value='1' $ShowPurchasePriceChecked> <label for='ShowPurchasePrice'>$i_InventorySystem_Item_Price</label> </div>\n";

$ShowLocationChecked = ($ShowLocation || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowLocation' id='ShowLocation' value='1' $ShowLocationChecked> <label for='ShowLocation'>$i_InventorySystem_Report_Col_Location</label> </div>\n";

$ShowWriteOffInfoChecked = ($ShowWriteOffInfo || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowWriteOffInfo' id='ShowWriteOffInfo' value='1' $ShowWriteOffInfoChecked> <label for='ShowWriteOffInfo'>$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off</label> </div>\n";

$ShowSignatureChecked = ($ShowSignature) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowSignature' id='ShowSignature' value='1' $ShowSignatureChecked> <label for='ShowSignature'>$i_InventorySystem_Report_Col_Signature</label> </div>\n";

$ShowRemarkChecked = ($ShowRemark) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowRemark' id='ShowRemark' value='1' $ShowRemarkChecked> <label for='ShowRemark'>$i_InventorySystem_Report_Col_Remarks</label> </div>\n";

// S/N option
$ShowSNChecked = ($ShowSN) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowSN' id='ShowSN' value='1'' $ShowSNChecked> <label for='ShowSN'>$i_InventorySystem_Item_Serial_Num</label> </div>\n";

// Quotation Number option
$ShowQuoNoChecked = ($ShowQuoNo) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowQuoNo' id='ShowQuoNo' value='1'' $ShowQuoNoChecked> <label for='ShowQuoNo'>$i_InventorySystem_Item_Quot_Num</label> </div>\n";

// Supplier option
$ShowSupplierChecked = ($ShowSupplier) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowSupplier' id='ShowSupplier' value='1'' $ShowSupplierChecked> <label for='ShowSupplier'>$i_InventorySystem_Item_Supplier_Name</label> </div>\n";

// Tender No option
$ShowTenderNoChecked = ($ShowTenderNo) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowTenderNo' id='ShowTenderNo' value='1'' $ShowTenderNoChecked> <label for='ShowTenderNo'>$i_InventorySystem_Item_Tender_Num</label> </div>\n";

// Invoice Number option
$ShowInvNoChecked = ($ShowInvNo) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowInvNo' id='ShowInvNo' value='1'' $ShowInvNoChecked> <label for='ShowInvNo'>$i_InventorySystem_Item_Invoice_Num</label> </div>\n";

$table_selection .= "</div>";
$ShowFundingNameChecked = ($ShowFundingName || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowFundingName' id='ShowFundingName' value='1' $ShowFundingNameChecked> <label for='ShowFundingName'>$i_InventorySystem_Item_Funding</label> </div>\n";

$ShowCaretakerChecked = ($ShowCaretaker || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowCaretaker' id='ShowCaretaker' value='1' $ShowCaretakerChecked> <label for='ShowCareTaker'>".$i_InventorySystem['Caretaker']."</label> </div>\n";

$ShowPurchasePriceTotalChecked = ($ShowPurchasePriceTotal || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection .= "<div><input type='checkbox' name='ShowPurchasePriceTotal' id='ShowPurchasePriceTotal' value='1' $ShowPurchasePriceTotalChecked> <label for='ShowPurchasePriceTotal'>" . $Lang['eInventory']['TotalAssets'] . "</label> </div>\n";

$table_selection .= "</td></tr>";

$ShowSchoolLogoChecked = ($ShowSchoolLogo || ! isset($groupBy)) ? "checked='checked'" : "";
$table_selection_badge = "<div><input type='checkbox' name='ShowSchoolLogo' id='ShowSchoolLogo' value='1' $ShowSchoolLogoChecked> <label for='ShowSchoolLogo'>" . $i_adminmenu_basic_settings_badge . "</label> </div>\n";

$SignatureChecked = ($IsSignature) ? "checked='checked'" : "";
$table_selection .= "<tr class='HTMLonly' ><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">&nbsp;</td><td><i>" . $Lang['eInventory']['DisplayMisc'] . "</i><br />{$table_selection_badge}<input type='checkbox' id='IsSignature1' name='IsSignature' $SignatureChecked /> <label for='IsSignature1'>" . $Lang['eInventory']['DisplaySignature'] . "</label></td></tr>";

// # display written-off items
if ($display_writeoff == 1) {
    $display_writeoff1_checked = "checked='checked'";
} else {
    $display_writeoff0_checked = "checked='checked'";
}
$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['DisplayWrittenOffItem'] . "</td>";
$table_selection .= "<td class=\"tabletext\"><input type='radio' name='display_writeoff' value='1' id='display_writeoff1' {$display_writeoff1_checked} /> <label for='display_writeoff1'>$i_general_yes</label> <input type='radio' name='display_writeoff' value='0' id='display_writeoff0' {$display_writeoff0_checked} /> <label for='display_writeoff0'>$i_general_no</label> </td></tr>";

// # display not require stock-taking item or not
if (! isset($display_not_req_stocktaking)) {
    $display_not_req_stocktaking = 1; // default show all items
}
$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['DisplayNotReqStocktakingItem'] . "</td>";
$table_selection .= "<td class=\"tabletext\"><input type='radio' name='display_not_req_stocktaking' value='1' id='display_not_req_stocktaking1'" . ($display_not_req_stocktaking ? " checked='checked'" : "") . " /> <label for='display_not_req_stocktaking1'>$i_general_yes</label> <input type='radio' name='display_not_req_stocktaking' value='0' id='display_not_req_stocktaking0' " . ($display_not_req_stocktaking ? "" : " checked='checked'") . " /> <label for='display_not_req_stocktaking0'>$i_general_no</label> </td></tr>";

$arr_groupBy = array(
    array(
        1,
        $i_InventorySystem['Location']
    ),
    array(
        2,
        $i_InventorySystem['item_type']
    ),
    array(
        3,
        $i_InventorySystem['Caretaker']
    ),
    array(
        4,
        $i_InventorySystem['FundingSource']
    )
);	
$groupby_selection = getSelectByArray($arr_groupBy, "name=\"groupBy\" id=\"groupBy\" onChange=\"document.form1.flag.value=''; document.form1.action=''; document.form1.target=''; this.form.submit(); \"  ", $groupBy);
$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Report_Stocktake_groupby</td>";
$table_selection .= "<td>$groupby_selection</td></tr>";

// # group by - location
if ($groupBy == 1) {
    // $building_selection = $llocation_ui->Get_Building_Selection($TargetBuilding, 'TargetBuilding', 'document.form1.flag.value=\'\'; resetLocationSelection(\'Building\'); this.form.submit();', 0, 0, '');
    
    $table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Location'] . "</td>";
    $table_selection .= "<td>" . $llocation_ui->Get_Building_Floor_Selection($TargetFloor, 'TargetFloor', 'document.form1.flag.value=\'\'; resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '') . "</td>";
    
    /*
     * $table_selection .= "<td>".$building_selection."</td>";
     *
     * if($TargetBuilding != "") {
     * $floor_selection = $llocation_ui->Get_Floor_Selection($TargetBuilding, $TargetFloor, 'TargetFloor', 'document.form1.flag.value=\'\'; resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '');
     *
     * $table_selection .= "<tr><td></td>";
     * $table_selection .= "<td>".$floor_selection."</td>";
     * }
     * if(($TargetBuilding != "") && ($TargetFloor != "")) {
     */
    if ($TargetFloor != "") {
        
        $sql = "SELECT LocationID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$TargetFloor."' AND RecordStatus = 1 order by DisplayOrder";
        $result = $linventory->returnArray($sql, 2);
        
        $room_selection = "<select id=\"TargetRoom[]\" name=\"TargetRoom[]\" multiple size=\"10\">";
        if (sizeof($TargetRoom) == 0) {
            $room_selection .= "<option value=\"\" selected> - select room - </option>";
        } else {
            $room_selection .= "<option value=\"\" > - select room - </option>";
        }
        
        if (sizeof($result) > 0) {
            for ($i = 0; $i < sizeof($result); $i ++) {
                list ($location_id, $location_name) = $result[$i];
                
                $selected = "";
                if (is_array($TargetRoom)) {
                    if (in_array($location_id, $TargetRoom)) {
                        $selected = " SELECTED ";
                    }
                }
                $room_selection .= "<option value=\"$location_id\" $selected>$location_name</option>";
            }
        }
        $room_selection .= "</select>";
        
        $table_selection .= "<tr><td></td>";
        $table_selection .= "<td>" . $room_selection . "</td>";
    }
}
// # group by - resource mgmt group
if ($groupBy == 3) {
    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " as GroupName  FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
    $result = $linventory->returnArray($sql, 2);
    
    // $group_selection = getSelectByArray($arr_group, " name=\"targetGroup\" ", $targetGroup, 1, 0);
    
    $group_selection = "<select id=\"targetGroup[]\" name=\"targetGroup[]\" multiple size=\"10\">";
    if (sizeof($targetGroup) == 0) {
        $group_selection .= "<option value=\"\" selected> - select group - </option>";
    } else {
        $group_selection .= "<option value=\"\"> - select group - </option>";
    }
    
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($group_id, $group_name) = $result[$i];
            
            $selected = "";
            if (is_array($targetGroup)) {
                if (in_array($group_id, $targetGroup)) {
                    $selected = " SELECTED ";
                }
            }
            $group_selection .= "<option value=\"$group_id\" $selected>$group_name</option>";
        }
    }
    $group_selection .= "</select>";
    
    $table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Caretaker'] . "</td>";
    $table_selection .= "<td>" . $group_selection . "</td></tr>";
}

// # group by - funding source
if ($groupBy == 4){
       $sql = "SELECT FundingSourceID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_FUNDING_SOURCE WHERE RecordStatus = 1 ORDER BY DisplayOrder";
       $result = $linventory->returnArray($sql, 2);
       
       $fundSource_selection = "<select id=\"targetFundSource[]\" name=\"targetFundSource[]\" multiple size=\"10\">";
       if (sizeof($targetFundSource) == 0){
           $fundSource_selection .= "<option value=\"\" selected> - select funding source - </option>";
       }else{
           $fundSource_selection .= "<option value=\"\"> - select funding source - </option>";
       }
       
       if(sizeof($result > 0)){
           for ($i = 0; $i < sizeof($result); $i++){
               list($fundSource_id, $fundSource_name) = $result[$i];
               
               $selected = "";
               if(is_array($targetFundSource)){
                   if(in_array($fundSource_id, $targetFundSource)){
                        $selected = " SELECTED ";
                   }
               }
               $fundSource_selection .= "<option value=\"$fundSource_id\" $selected>$fundSource_name</option>";
           }
       }
       $fundSource_selection .= "</select>";
       
       $table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['FundingSource'] . "</td>";
       $table_selection .= "<td>" . $fundSource_selection . "</td></tr>";
}

$stocktake_start_date = $linventory->getStocktakePeriodStart();
$stocktake_end_date = $linventory->getStocktakePeriodEnd();

if ($StockTakeDateFrom == "") {
    $StockTakeDateFrom = $stocktake_start_date;
}
if ($StockTakeDateEnd == "") {
    $StockTakeDateEnd = $stocktake_end_date;
}

$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['OtherFilters'] . "</td>";
if ($CountStocktakeOnly) {
    $CountStocktakeOnlyChecked = "checked='checked'";
} else {
    $CountStocktakeOnlyDisplay = "style='display:none;'";
}
$table_selection .= "<td><input type='checkbox' name='CountStocktakeOnly' id='CountStocktakeOnly' onClick=\"jsDisplayField('StockTakePeriod', this.checked)\" {$CountStocktakeOnlyChecked} /> <label for='CountStocktakeOnly'>" . $Lang['eInventory']['CountStocktakeOnly'] . "</label><br />";
$table_selection .= "<table border='0' id='StockTakePeriod' {$CountStocktakeOnlyDisplay}><tr>
					<td width='20'>&nbsp;</td>
					<td>" . strtolower($Lang['General']['From']) . " </td>
					<td>" . $linterface->GET_DATE_PICKER("StockTakeDateFrom", $StockTakeDateFrom, "", "", "", "", "", "", "", "", 1) . "</td>
					<td nowrap='nowrap'>&nbsp; " . strtolower($Lang['General']['To']) . " </td>
					<td>" . $linterface->GET_DATE_PICKER("StockTakeDateEnd", $StockTakeDateEnd, "", "", "", "", "", "", "", "", 1) . "</td>
					</tr></table>";
if ($CountByPrice) {
    $CountByPriceChecked = "checked='checked'";
} else {
    $CountByPriceDisplay = "style='display:none;'";
}
$table_selection .= "<input type='checkbox' name='CountByPrice' id='CountByPrice' onClick=\"jsDisplayField('CountByPriceValue', this.checked)\" {$CountByPriceChecked} /> <label for='CountByPrice'>" . $Lang['eInventory']['CountByPrice'] . "</label><br />";
$table_selection .= "<table border='0' id='CountByPriceValue' {$CountByPriceDisplay} ><tr>
					<td width='20'>&nbsp;</td>
					<td>[" . $i_InventorySystem_ItemType_Single . "]: </td>
					<td>" . $Lang['ePOS']['UnitPrice'] . " " . strtolower($Lang['General']['From']) . " </td>
					<td>$<input type='text' name='FilterSingleItemPriceFrom' id='FilterSingleItemPriceFrom' value='{$FilterSingleItemPriceFrom}' size='4' /></td>
					<td nowrap='nowrap'>&nbsp; " . strtolower($Lang['General']['To']) . " </td>
					<td>$<input type='text' name='FilterSingleItemPriceEnd' id='FilterSingleItemPriceEnd' value='{$FilterSingleItemPriceEnd}' size='4' /></td>
					<td width='20'>&nbsp;</td>
					<td width='20'>&nbsp;</td>
					<td width='20'>&nbsp;</td>
					</tr><tr>
					<td width='20'>&nbsp;</td>
					<td>[" . $i_InventorySystem_ItemType_Bulk . "]: </td>
					<td>" . $Lang['eInventory']['total_single_item_cost'] . " " . strtolower($Lang['General']['From']) . " </td>
					<td>$<input type='text' name='FilterBulkItemPriceFrom' id='FilterBulkItemPriceFrom' value='{$FilterBulkItemPriceFrom}' size='4' /></td>
					<td nowrap='nowrap'>&nbsp; " . strtolower($Lang['General']['To']) . " </td>
					<td>$<input type='text' name='FilterBulkItemPriceEnd' id='FilterBulkItemPriceEnd' value='{$FilterBulkItemPriceEnd}' size='4' /></td>
					<td width='20'>&nbsp;</td>
					<td width='20'>&nbsp;</td>
					<td width='20'>&nbsp;</td>
					</tr><tr><td>&nbsp;</td><td colspan='7'><span class='tabletextremark'>* " . $Lang['eInventory']['RemarkBoundary'] . "</span></td></table>";
$table_selection .= "</td></tr>";
/*
 * $ShowFundingNameChecked = "";
 * if($ShowFundingName){
 * $ShowFundingNameChecked = "CHECKED";
 * }
 * $table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['eInventory']['FieldTitle']['Report']['ShowFundingName']."</td>";
 * $table_selection .= "<td><input type='checkbox' name='ShowFundingName' id='ShowFundingName' value=1 $ShowFundingNameChecked></td></tr>";
 */

// ## Start to gen report ###
if ($step == "final") {
    $sql = "
	SELECT 
		DISTINCT(ITEM.ItemID),
		ITEM.ItemCode, 
		" . $linventory->getInventoryItemNameByLang("ITEM.") . ", 
		IF(ITEM.ITEMTYPE=2,BL.PURCHASEDATE,SE.PURCHASEDATE) PURCHASEDATE,
		IF(ITEM.OWNERSHIP=1,IF(ITEM.ITEMTYPE=1,SE.PURCHASEDPRICE,BE.QUANTITY*BL.UNITPRICE),'') GOVTFUND,
		IF(ITEM.OWNERSHIP=2,IF(ITEM.ITEMTYPE=1,SE.PURCHASEDPRICE,BE.QUANTITY*BL.UNITPRICE),'') SCHFUND,
		IF(ITEM.ITEMTYPE=2,BL.PURCHASEDPRICE,SE.PURCHASEDPRICE) PURCHASEDPRICE,
		IF(ITEM.ITEMTYPE=2,BL.UNITPRICE,SE.PURCHASEDPRICE) UNITPRICE,
		IF(ITEM.ITEMTYPE=2,BE.QUANTITY,'1') QUANTITY,
		" . $linventory->getInventoryItemLocationByLang("L.") . " LOCATION,
		WOR.RECORDDATE,
		" . $linventory->getInventoryItemWriteOffReason("WROT.") . ",
		CONCAT(''),
		" . $linventory->getInventoryItemRemarkByLang("FS.") . " Remark
	FROM 
		INVENTORY_ITEM ITEM 
		LEFT JOIN INVENTORY_ITEM_BULK_LOG BL ON ITEM.ITEMID=BL.ITEMID
		LEFT JOIN INVENTORY_ITEM_SINGLE_EXT SE ON ITEM.ITEMID=SE.ITEMID
		LEFT JOIN INVENTORY_ITEM_BULK_EXT BE ON BE.ITEMID=ITEM.ITEMID
		LEFT JOIN INVENTORY_LOCATION L ON L.LOCATIONID=SE.LOCATIONID OR L.LOCATIONID=BL.LOCATIONID
		LEFT JOIN INVENTORY_ITEM_WRITE_OFF_RECORD WOR ON WOR.ITEMID=ITEM.ITEMID
		LEFT JOIN INVENTORY_WRITEOFF_REASON_TYPE WROT ON WROT.ReasonTypeID = WOR.WriteOffReasonID
		LEFT JOIN INVENTORY_FUNDING_SOURCE FS ON FS.FUNDINGSOURCEID=SE.FUNDINGSOURCE OR FS.FUNDINGSOURCEID=BL.FUNDINGSOURCE
	";
    $arr_result = $linventory->returnArray($sql, 12);
    
    $table_content = "<tr class=\"tablebluetop\">
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Item_Code . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Item . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Description . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Purchase_Date . "</td>
	<td class=\"tabletopnolink\" colspan=\"2\" align=\"center\">" . $i_InventorySystem_Report_Col_Cost . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Sch_Unit_Price . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Sch_Purchase_Price . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Quantity . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Location . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Date_Reason_of_Write_Off . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Signature . "</td>
	<td class=\"tabletopnolink\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Remarks . "</td>
	</tr>";
    $table_content .= "<tr class=\"tablebluetop\">
	<td class=\"tabletopnolink\">" . $i_InventorySystem_Report_Col_Govt_Fund . "</td>
	<td class=\"tabletopnolink\">" . $i_InventorySystem_Report_Col_Sch_Fund . "</td></tr>";
    
    if (is_array($arr_result) && count($arr_result) > 0) {
        foreach ($arr_result as $Key => $Value) {
            $j ++;
            if ($j % 2 == 0)
                $row_css = " class=\"tablerow1\" ";
            else
                $row_css = " class=\"tablerow2\" ";
            
            $table_content .= "<tr $row_css>";
            list ($item_id, $item_code, $item_name, $purchase_date, $gov_fund, $sch_fund, $purchased_price, $unit_price, $quantity, $location, $write_off_date, $write_off_reason, $signature, $remark) = $Value;
            
            $table_content .= "<td class=\"tabletext\">" . $item_id . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $item_code . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $item_name . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $purchase_date . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $gov_fund . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $sch_fund . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $purchased_price . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $unit_price . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $quantity . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $location . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $write_off_date;
            if ($write_off_reason)
                $table_content .= "(" . $write_off_reason . ")";
            $table_content .= "</td>";
            $table_content .= "<td class=\"tabletext\">" . $signature . "</td>";
            $table_content .= "<td class=\"tabletext\">" . $remark . "</td>";
            $table_content .= "</tr>";
        }
    }
}

?>
<script language="javascript">
$( document ).ready(function() {
    if($('[name=targetFileType]').val() == 2){
    	displayTable('display_options', 'none');
    	jsDisplayFieldByClass('HTMLonly', 0);
    } 
});

function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("TargetFloor")){
			document.getElementById("TargetFloor").value = "";
		}
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
	document.form1.flag.value=0;
	document.form1.action = "";
	document.form1.target = "";
	return false;
}

function checkForm()
{
	var obj = document.form1;
	var groupby = obj.groupBy.value;
	var location_cb = 0;
	var group_cb = 0;
	var fs_cb = 0;
	
	if(obj.flag.value == 1)
	{
		if(groupby == 1) {
			if(document.getElementById('TargetRoom[]')) {
				for(i=0; i<document.getElementById('TargetRoom[]').length; i++) {
					if(document.getElementById('TargetRoom[]')[i].selected == true) {
						if(document.getElementById('TargetRoom[]')[i].value != "") {
							location_cb++;
						}
					}
				}
			}
		}
		if(groupby == 3){
			if(document.getElementById('targetGroup[]')) {
				for(i=0; i<document.getElementById('targetGroup[]').length; i++) {
					if(document.getElementById('targetGroup[]')[i].selected == true) {
						if(document.getElementById('targetGroup[]')[i].value != "") {
							group_cb++;
						}
					}
				}
			}
		}
		
		if (groupby == 4){
			if(document.getElementById('targetFundSource[]')){
				for(i=0; i<document.getElementById('targetFundSource[]').length; i++){
					if(document.getElementById('targetFundSource[]')[i].selected == true){
						if(document.getElementById('targetFundSource[]')[i].value != ""){
							fs_cb++;
						}
					}
				}
			}
		}
		
		
		if(location_cb>0||group_cb>0||fs_cb > 0 || groupby==2)
		{
			if(obj.targetFileType.value == 1)
			{
				obj.action = "fixed_assets_register_print_cust.php?CatID=<?=$targetCategory?>&Cat2ID=<?=$targetSubCategory?>&LocationID=<?=$targetLocation?>&Location2ID=<?=$targetSubLocation?>";
				obj.target='intranet_popup10';
		        newWindow('about:blank', 10);
		        obj.flag.value = 0;
				return true;
			}
			if(obj.targetFileType.value == 2)
			{
				obj.action = "fixed_assets_register_export_cust.php?CatID=<?=$targetCategory?>&Cat2ID=<?=$targetSubCategory?>&LocationID=<?=$targetLocation?>&Location2ID=<?=$targetSubLocation?>";
				obj.flag.value = 0;
				return true;
			}
			if(obj.targetFileType.value == 3)
			{
				obj.action = "fixed_assets_register_pdf_cust.php?CatID=<?=$targetCategory?>&Cat2ID=<?=$targetSubCategory?>&LocationID=<?=$targetLocation?>&Location2ID=<?=$targetSubLocation?>";
		        obj.flag.value = 0;
				return true;
			}
		}
		else if(groupby==1)
		{
			alert('<?=$i_InventorySystem['jsLocationCheckBox']?>');
			return false;
		}
		else if(groupby==3)
		{
			alert('<?=$i_InventorySystem['jsGroupCheckBox']?>');
			return false;	
		}
		else if(groupby == 4){
			alert('<?=$Lang['eInventory']['FundingSource']['jsFundingSourceCheckBox']?>');
			return false;
		}
		else if(groupby=="")
		{
			alert('<?=$i_InventorySystem['jsLocationGroupCheckBox']?>');
			return false;	
		}
	}
	obj.flag.value = 0;
	obj.action = "";
	return false;
}

function jsDisplayField(ItemName, isShow)
{
	if (isShow)
	{
		$('#'+ItemName).attr('style', '');
	} else
	{
		$('#'+ItemName).attr('style', 'display: none');
	}
}

function jsDisplayFieldByClass(ItemName, isShow)
{
	if (isShow)
	{
		$('.'+ItemName).attr('style', '');
	} else
	{
		$('.'+ItemName).attr('style', 'display: none');
	}
}

</script>

<br>
<form name="form1" action="" method="post" onSubmit="return checkForm()" ">
	<br>
	<table border="0" cellpadding="3" cellspacing="0" width="90%"
		class="systemmsg" align="center">
		<tr>
			<td><span class=tabletextrequire2><?=$i_InventorySystem_FixedAssetsRegister_Warning;?></span></td>
		</tr>
	</table>
	<br>
	<table border="0" width="90%" cellpadding="0" cellspacing="5"
		align="center">
<?=$table_selection;?>
</table>
	<table border="0" width="90%" cellpadding="0" cellspacing="5"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr>
			<td align="center">
	<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.flag.value=1;")?>
</td>
		</tr>
	</table>
	<!--
<br>
<table border="0" width="96%" cellspacing="0" cellpadding="5">
<?=$toolbar;?>
</table>
<br>
<table border="0" width="96%" cellpadding="0" cellspacing="0" align="center">
<?=$table_content;?>
</table>
-->
	<input type="hidden" name="flag" value="0"> <input type="hidden"
		name="step" value="">
</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>