<?php
// using:

/**
 * ******************************* Modification Log *************************************
 * 2020-08-14 (Henry)
 *      display stocktake location in remarks field if not in correct location [Case#P186248]
 * 
 * 2019-07-15 (Henry)
 *      bug fix for wrong remark display after stocktake an item [Case#G189505]
 * 
 * 2019-05-13 (Henry)
 *      Security fix: SQL without quote
 * 
 * Date: 2018-02-22 Henry
 * add access right checking [Case#E135442]
 *
 * Date: 2017-12-19 Cameron
 * redirect to reports.php in PowerClass with error message if basic setting is not setup
 *
 * Date: 2017-08-16 Henry
 * stocktake for bulk item with different location bug fix [Case#P121300]
 *
 * Date: 2014-07-30 Bill
 * add list for user to export results with Basic Information and details
 *
 * # Date: 2012-06-01 YatWoon
 * # display funding source for bulk item
 * # cater as multi "group" and "funding" for bulk item
 * #
 * # Date: 2011-03-16 YatWoon
 * # change wordings from "Deleted Staff" to "User account not exists."
 * #
 * # Date: 2011-03-01 YatWoon
 * # update query, hidden bulk item if quantity is 0
 * #
 * 2011-02-22 (Yuen) :
 * i. added Get_Building_Floor_Selection() to display building and floor list in one pull-down menu
 * ii. fixed division by zero error for this code - $total_cost[0]/$total_qty[0];
 *
 * **************************************************************************************
 */
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Report_StockTake";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$llocation_ui = new liblocation_ui();

// Init JS for date picker #
$js_init .= '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.js"></script>';
$js_init .= '<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.css" type="text/css" />';

// ## Check user have set all the basic setting ###
$sql = "SELECT LocationID FROM INVENTORY_LOCATION";
$arr_location_check = $linventory->returnVector($sql);

$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
$arr_group_check = $linventory->returnVector($sql);

$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY";
$arr_category_check = $linventory->returnVector($sql);

$sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE";
$arr_funding_check = $linventory->returnVector($sql);

if ((sizeof($arr_location_check) == 0) || (sizeof($arr_group_check) == 0) || (sizeof($arr_category_check) == 0) || (sizeof($arr_funding_check) == 0)) {
    if ($sys_custom['PowerClass']) {
        header("Location: " . $PATH_WRT_ROOT . "home/PowerClass/reports.php?error=MissingBasicSetting");
    } else {
        header("Location: " . $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php");
    }
}
// ## END ###

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_Stocktake . ' ' . $i_InventorySystem['Report'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$stocktake_start_date = $linventory->getStocktakePeriodStart();
$stocktake_end_date = $linventory->getStocktakePeriodEnd();

if ($start_date == "") {
    $start_date = $stocktake_start_date;
}
if ($end_date == "") {
    $end_date = $stocktake_end_date;
}

// # Date Range ##
$table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Setting_StockCheckDateRange</td>";
$table_content .= "<td class=\"tabletext\">
						$i_InventorySystem_From &nbsp; <input type=\"text\" name=\"start_date\" id=\"start_date\" class=\"textboxnum\" maxlength=\"10\" value=\"$start_date\"> 
						$i_InventorySystem_To &nbsp; <input type=\"text\" name=\"end_date\" id=\"end_date\" class=\"textboxnum\" maxlength=\"10\" value=\"$end_date\">
					</td></tr>\n";

$arr_groupBy = array(
    array(
        1,
        $i_InventorySystem['Location']
    ),
    array(
        2,
        $i_InventorySystem['item_type']
    ),
    array(
        3,
        $i_InventorySystem['Caretaker']
    )
);
$groupby_selection = getSelectByArray($arr_groupBy, "name=\"groupBy\" id=\"groupBy\" onChange=\"this.form.submit();\" ", $groupBy);

// # report - group by
$table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Report_Stocktake_groupby</td>";
$table_content .= "<td>$groupby_selection</td></tr>\n";

// # group by - location
if ($groupBy == 1) {
    // $building_selection = $llocation_ui->Get_Building_Selection($TargetBuilding, 'TargetBuilding', 'resetLocationSelection(\'Building\'); this.form.submit();', 0, 0, '');
    
    $table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td>" . $llocation_ui->Get_Building_Floor_Selection($TargetFloor, 'TargetFloor', 'resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '') . "</td>";
    
    /*
     * $table_content .= "<td>".$building_selection."</td>";
     * if($TargetBuilding != "") {
     * $floor_selection = $llocation_ui->Get_Floor_Selection($TargetBuilding, $TargetFloor, 'TargetFloor', 'resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '');
     *
     * $table_content .= "<tr><td></td>";
     * $table_content .= "<td>".$floor_selection."</td>";
     * }
     * if(($TargetBuilding != "") && ($TargetFloor != "")) {
     */
    
    if ($TargetFloor != "") {
        
        $sql = "SELECT LocationID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$TargetFloor."' AND RecordStatus = 1";
        $result = $linventory->returnArray($sql, 2);
        
        $room_selection = "<select id=\"TargetRoom[]\" name=\"TargetRoom[]\" multiple size=\"10\">";
        if (sizeof($TargetRoom) == 0) {
            $room_selection .= "<option value=\"\" selected> -- " . $Lang['SysMgr']['Location']['Select']['Room'] . " -- </option>";
        } else {
            $room_selection .= "<option value=\"\"> -- " . $Lang['SysMgr']['Location']['Select']['Room'] . " -- </option>";
        }
        
        if (sizeof($result) > 0) {
            for ($i = 0; $i < sizeof($result); $i ++) {
                list ($location_id, $location_name) = $result[$i];
                
                $selected = "";
                if (is_array($TargetRoom)) {
                    if (in_array($location_id, $TargetRoom)) {
                        $selected = " SELECTED ";
                    }
                }
                $room_selection .= "<option value=\"$location_id\" $selected>$location_name</option>";
            }
        }
        $room_selection .= "</select>";
        
        $table_content .= "<tr><td></td>";
        $table_content .= "<td>" . $room_selection . "</td>";
    }
}
// # group by - resource mgmt group
if ($groupBy == 3) {
    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " as GroupName  FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
    $result = $linventory->returnArray($sql, 2);
    
    // $group_selection = getSelectByArray($arr_group, " name=\"targetGroup\" ", $targetGroup, 1, 0);
    
    $group_selection = "<select id=\"targetGroup[]\" name=\"targetGroup[]\" multiple size=\"10\">";
    if (sizeof($targetGroup) == 0) {
        $group_selection .= "<option value=\"\" selected> - select group - </option>";
    } else {
        $group_selection .= "<option value=\"\"> - select group - </option>";
    }
    
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($group_id, $group_name) = $result[$i];
            
            $selected = "";
            if (is_array($targetGroup)) {
                if (in_array($group_id, $targetGroup)) {
                    $selected = " SELECTED ";
                }
            }
            $group_selection .= "<option value=\"$group_id\" $selected>$group_name</option>";
        }
    }
    $group_selection .= "</select>";
    
    $table_content .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td>" . $group_selection . "</td>";
}

$table_content .= "<tr><td colspan=\"2\" align=\"center\">" . $linterface->GET_ACTION_BTN($button_submit, "submit") . " " . "</td></tr>\n";
$table_content .= "<tr><td colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";

// ## Start to gen the report ###
if ($step == "final") {
    if (sizeof($TargetRoom)) {
        $groupby_val = implode(",", $TargetRoom);
    }
    if (sizeof($targetGroup)) {
        $groupby_val = implode(",", $targetGroup);
    }
    // $toolbar .= "<tr><td class=\"tabletext\">".$linterface->GET_LNK_PRINT("javascript:openPrintPage('$start_date','$end_date','$targetLocation','$targetGroup', '$targetProgress','$groupBy','$groupby_val')","","","","",0);
    // $toolbar .= $linterface->GET_LNK_EXPORT("stocktake_result_export.php?start_date='$start_date'&end_date='$end_date'&targetLocation='$targetLocation'&targetGroup='$targetGroup'&targetProgress='$targetProgress'&groupBy=$groupBy&selected_cb=$groupby_val","","","","",0)."</td></tr>\n";
    
    // Toolbar: allow 2 export types
    $toolbar .= "<tr><td class=\"tabletext\">";
    $toolbar .= "<div class=\"Conntent_tool\">";
    
    // print
    $toolbar .= $linterface->GET_LNK_PRINT_IP25("javascript:openPrintPage('$start_date','$end_date','$targetLocation','$targetGroup', '$targetProgress','$groupBy','$groupby_val')");
    
    // export
    $toolbar .= "<div class=\"btn_option\">
				<a href=\"javascript:\" class=\"export\" id=\"btn_export\" onclick=\"MM_showHideLayers('export_option','','show');document.getElementById('btn_export').className = 'export parent_btn';\"> " . $Lang['Btn']['Export'] . "</a>
				<br style=\"clear:both\" />
					<div class=\"btn_option_layer\" id=\"export_option\" onclick=\"MM_showHideLayers('export_option','','hide');document.getElementById('btn_export').className = 'export';\">
					  <a href=\"javascript:click_export(1);\" class=\"sub_btn\"> " . $Lang['eInventory']['BasicInfo'] . "</a>
					  <a href=\"javascript:click_export(2);\" class=\"sub_btn\"> " . $Lang['eInventory']['Details'] . "</a>
					</div>
				</div>";
    
    $toolbar .= "</div>";
    $toolbar .= "</td></tr>";
    
    if ($groupBy == 1) {
        if (is_array($TargetRoom))
            $cond = "WHERE LocationID in (" . implode($TargetRoom, ",") . ")";
        
        $sql = "Select DISTINCT(CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",' > '," . $linventory->getInventoryItemNameByLang("b.") . ",' > '," . $linventory->getInventoryItemNameByLang("a.") . ")) as LocationName, LocationID 
				FROM INVENTORY_LOCATION AS a INNER JOIN 
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID) 
				$cond 
				ORDER BY building.DisplayOrder, b.DisplayOrder, a.DisplayOrder";
        
        $arr_location = $linventory->returnArray($sql, 2);
        
        for ($a = 0; $a < sizeof($arr_location); $a ++) {
            list ($location_name, $locationID) = $arr_location[$a];
            
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
            }
            
            if ($target_admin_group != "") {
                $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
            }
            
            // ## Single Item ###
            $price_ceiling_cond = "";
            if ($sys_custom['eInventory_PriceCeiling']) {
                $price_ceiling_cond = "  AND (b.UnitPrice>=f.PriceCeiling) ";
            }
            
            $sql = "SELECT 
							a.ItemCode, 
							a.ItemID,
							b.TagCode,
							" . $linventory->getInventoryItemNameByLang("a.") . ",
							" . $linventory->getInventoryNameByLang("c.") . ",
							d.LocationID,
							CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
							b.UnitPrice,
							b.PurchasedPrice,
							a.StockTakeOption
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					WHERE
							a.ItemType = 1 AND 
							d.LocationID = " . $locationID . " AND
							a.RecordStatus = 1
					ORDER BY
							a.ItemCode";
            $arr_single_result = $linventory->returnArray($sql);
            $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
            
            $location_table_content .= "<table border=\"0\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
            $location_table_content .= "<tr><td colspan=\"10\">" . $linterface->GET_NAVIGATION2(intranet_htmlspecialchars("$location_name")) . "</td></tr>\n";
            $location_table_content .= "<tr><td class=\"tablename\" colspan=\"10\">$i_InventorySystem_ItemType_Single</td></tr>\n";
            $location_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\" width=\"2%\"> </td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
            $location_table_content .= "</tr>\n";
            if (sizeof($arr_single_result) > 0) {
                for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                    list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category) = $arr_single_result[$i];
                    
                    $namefield = getNameFieldByLang2("b.");
                    $sql = "SELECT 
									a.Action,
									a.DateInput,
									IFNULL($namefield,'" . $Lang['General']['UserAccountNotExists'] . "'),
									IF(a.Remark != '', a.Remark, ' - ')
							FROM 
									INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
									INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
							WHERE 
									a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
							ORDER BY
									a.DateInput DESC LIMIT 0,1
							";
                    // echo $sql."<BR>";
                    $arr_result = $linventory->returnArray($sql, 4);
                    if (sizeof($arr_result) > 0) {
                        for ($j = 0; $j < sizeof($arr_result); $j ++) {
                            
                            list ($item_action, $item_record_date, $user_name, $remark) = $arr_result[$j];
                            
                            if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                                $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                            } else {
                                $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                            }
                            
                            $sql = "SELECT 
										CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
							 		FROM 
										INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
										INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
										INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
										INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
									WHERE 
										b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
									ORDER BY
										b.DateInput DESC LIMIT 0,1";
										
                            $location_result = $linventory->returnArray($sql, 1);
                            
                            if($location_result){
	                            $location_found = $location_result[0][0];
	                            
	                            if(trim($remark) !='-'){
	                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
	                            }
	                            else{
	                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
	                            }
                            }
                            
                            $table_row_css = " class=\"Single\" ";
                            $location_table_content .= "<tr $table_row_css>";
                            $location_table_content .= "<td class=\"single tabletext\">$action_img</td>";
                            $location_table_content .= "<td class=\"single tabletext\">$single_item_code</td>";
                            $location_table_content .= "<td class=\"single tabletext\">$single_item_name</td>";
                            $location_table_content .= "<td class=\"single tabletext\">$single_item_barcode</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                            $location_table_content .= "<td class=\"single tabletext\">$user_name</td>";
                            $location_table_content .= "<td class=\"single tabletext\">$item_record_date</td>";
                            $location_table_content .= "<td class=\"single tabletext\">$remark</td>";
                            $location_table_content .= "</tr>\n";
                        }
                    } else {
                        $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                        $table_row_css = " class=\"Single\" ";
                        $location_table_content .= "<tr $table_row_css>";
                        $location_table_content .= "<td class=\"single tabletext\">$action_img</td>";
                        $location_table_content .= "<td class=\"single tabletext\">$single_item_code</td>";
                        $location_table_content .= "<td class=\"single tabletext\">$single_item_name</td>";
                        $location_table_content .= "<td class=\"single tabletext\">$single_item_barcode</td>";
                        $location_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                        $location_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                        // if($user_name == "")
                        // $user_name = " - ";
                        // if($date_input == "")
                        // $date_input = " - ";
                        // if($item_remark == "")
                        // $item_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                        $user_name = " - ";
                        $date_input = " - ";
                        $item_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                        $location_table_content .= "<td class=\"single tabletext\">$user_name</td>";
                        $location_table_content .= "<td class=\"single tabletext\">$date_input</td>";
                        $location_table_content .= "<td class=\"single tabletext\">$item_remark</td>";
                        $location_table_content .= "</tr>\n";
                    }
                }
            }
            if (sizeof($arr_single_result) == 0) {
                $location_table_content .= "<tr class=\"single\"><td colspan=\"11\" class=\"single tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
            }
            
            // ## Bulk item ###
            if ($sys_custom['eInventory_PriceCeiling']) {
                $arr_targetSuccessBulkItem = array();
                $arr_targetFailBulkItem = array();
                $success_bulk_item_cond = "";
                $fail_bulk_item_cond = "";
                $sql = "SELECT 
								a.ItemID, a.CategoryID 
						FROM 
								INVENTORY_ITEM AS a INNER JOIN
								INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
								INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
								INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
								INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
								INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						WHERE
								a.ItemType = 2 AND 
								d.LocationID = " . $locationID . "
						ORDER BY
								a.NameEng
						";
                $tmp_result = $linventory->returnArray($sql, 2);
                if (sizeof($tmp_result) > 0) {
                    for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                        list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                        $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $tmp_item_id AND Action = " . ITEM_ACTION_PURCHASE . "";
                        $total_cost = $linventory->returnVector($sql);
                        
                        $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $tmp_item_id";
                        $total_qty = $linventory->returnVector($sql);
                        
                        if ($total_qty[0] != 0)
                            $unit_price = $total_cost[0] / $total_qty[0];
                        
                        $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = $tmp_cat_id";
                        $tmp_result2 = $linventory->returnArray($sql, 2);
                        if (sizeof($tmp_result2) > 0) {
                            for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                                list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                                if ($apply_to_bulk == 1) {
                                    if ($unit_price >= $price_ceiling) {
                                        $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                    } else {
                                        $arr_targetFailBulkItem[] = $tmp_item_id;
                                    }
                                } else {
                                    $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                }
                            }
                        }
                    }
                }
                if (sizeof($arr_targetSuccessBulkItem) > 0) {
                    $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                    $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
                }
                if (sizeof($arr_targetFailBulkItem) > 0) {
                    $targetFailList = implode(",", $arr_targetFailBulkItem);
                    $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
                }
            }
            
            $sql = "SELECT 
							a.ItemCode, 
							a.ItemID,
							" . $linventory->getInventoryItemNameByLang("a.") . ",
							" . $linventory->getInventoryNameByLang("c.") . ",
							d.locationid,
							CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
							" . $linventory->getInventoryNameByLang("funding.") . ",
							b.GroupInCharge,
							b.FundingSourceID,
							b.Quantity,
							a.StockTakeOption
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
					WHERE
							a.ItemType = 2 AND 
							d.LocationID = " . $locationID . "
							$success_bulk_item_cond
							$fail_bulk_item_cond
					ORDER BY
							a.NameEng
							";
            $arr_bulk_result = $linventory->returnArray($sql);
            $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
            
            $location_table_content .= "</table>";
            $location_table_content .= "<br>";
            $location_table_content .= "<table border=\"0\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
            $location_table_content .= "<tr><td class=\"tablename\" colspan=\"11\">$i_InventorySystem_ItemType_Bulk</td></tr>\n";
            $location_table_content .= "<tr class=\"tabletop\">";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Group_Name</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_TotalMissingQty</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_TotalSurplusQty</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
            $location_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
            $location_table_content .= "</tr>\n";
            
            if (sizeof($arr_bulk_result) > 0) {
                for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                    list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                    
                    $bulk_lastmodified = "";
                    $person_in_charge = "";
                    $stocktake_remark = "";
                    
                    /*
                     * $sql = "SELECT
                     * IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')),
                     * IFNULL($namefield,'".$Lang['General']['UserAccountNotExists']."')
                     * FROM
                     * INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN
                     * INTRANET_USER AS b ON (a.PersonInCharge = b.UserID)
                     * WHERE
                     * a.ACTION = 2 AND
                     * a.ItemID = $bulk_item_id AND
                     * a.LocationID = ".$bulk_item_locationid." AND
                     * (a.RecordDate BETWEEN '$start_date' and '$end_date')
                     * GROUP BY
                     * a.ItemID
                     * ";
                     */
                    
                    /*
                     * $sql = "SELECT
                     * IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')),
                     * IFNULL($namefield,'".$Lang['General']['UserAccountNotExists']."')
                     * FROM
                     * INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN
                     * INTRANET_USER AS b ON (a.PersonInCharge = b.UserID)
                     * WHERE
                     * a.ACTION = 2 AND
                     * a.ItemID = $bulk_item_id AND
                     * a.LocationID = ".$bulk_item_locationid." AND
                     * (a.RecordDate BETWEEN '$start_date' and '$end_date') AND
                     * a.DateInput IN (SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = $bulk_item_id AND LocationID = $bulk_item_locationid AND (RecordDate BETWEEN '$start_date' and '$end_date'))
                     * GROUP BY
                     * a.ItemID
                     * ";
                     */
                    
                    $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = $bulk_item_id AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                    $MaxDateInput = $linventory->returnVector($sql);
                    $namefield = getNameFieldByLang2("b.");
                    $sql = "SELECT
								IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
								IFNULL($namefield,'" . $Lang['General']['UserAccountNotExists'] . "')
							FROM 
								INVENTORY_ITEM_BULK_LOG AS a 
								LEFT OUTER JOIN INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
							WHERE 
								a.ACTION = 2 AND 
								a.ItemID = $bulk_item_id AND
								a.LocationID = " . $bulk_item_locationid . " 
								and a.GroupInCharge=" . $bulk_group_id . " 
								and a.FundingSource =" . $bulk_funding_id . " and
								(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
								a.DateInput IN ('" . $MaxDateInput[0] . "')
							GROUP BY
								a.ItemID
							";
                    
                    $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                    if (sizeof($arr_stock_take_time_person) > 0)
                        list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                        
                        /*
                     * ##### bulk_item_quantity
                     * $sql = "SELECT
                     * b.Quantity
                     * FROM
                     * INVENTORY_ITEM AS a INNER JOIN
                     * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                     * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                     * WHERE
                     * a.ItemID = ".$bulk_item_id." AND
                     * b.LocationID = ".$bulk_item_locationid;
                     * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                     * if(sizeof($arr_stock_take_quantity) > 0)
                     * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                     * if($bulk_item_quantity=="")
                     * $bulk_item_quantity=0;
                     */
                        
                    // #### stocktake_qty
                    $sql = "SELECT 
									StockCheckQty,
									IF(Remark='' OR Remark is null,'-',Remark),
									QtyChange
							FROM 
									INVENTORY_ITEM_BULK_LOG 
							WHERE 
									ItemID = $bulk_item_id AND 
									LocationID = $bulk_item_locationid AND
									GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
									Action IN (2,3,4) AND
									(RecordDate BETWEEN '$start_date' and '$end_date') AND
									DateInput IN ('" . $MaxDateInput[0] . "')
							ORDER BY
									DateInput DESC
							";
                    $arr_stocktake_qty = $linventory->returnArray($sql, 2);
                    if (sizeof($arr_stocktake_qty) > 0) {
                        list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                    } else {
                        $stocktake_remark = "";
                        $stocktake_qty_change = 0;
                        $stocktake_qty = 0;
                    }
                    // $item_different = $stocktake_qty - $bulk_item_quantity;
                    $item_different = $stocktake_qty_change;
                    // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                    
                    $table_row_css = " class=\"Bulk\" ";
                    $location_table_content .= "<tr $table_row_css><td class=\"bulk tabletext\">$bulk_item_code</td>";
                    $location_table_content .= "<td class=\"bulk tabletext\">$bulk_item_name</td>";
                    $location_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_item_category) . "</td>";
                    $location_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_item_group) . "</td>";
                    $location_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                    // $location_table_content .= "<td class=\"bulk tabletext\">$bulk_item_quantity</td>";
                    $location_table_content .= "<td class=\"bulk tabletext\">$expect_qty</td>";
                    if ($item_different > 0) {
                        $location_table_content .= "<td class=\"bulk tabletext\">0</td>";
                        $location_table_content .= "<td class=\"bulk tabletext\">$item_different</td>";
                    }
                    if ($item_different < 0) {
                        $location_table_content .= "<td class=\"bulk tabletext\">$item_different</td>";
                        $location_table_content .= "<td class=\"bulk tabletext\">0</td>";
                    }
                    if ($item_different == 0) {
                        $location_table_content .= "<td class=\"bulk tabletext\">0</td>";
                        $location_table_content .= "<td class=\"bulk tabletext\">0</td>";
                    }
                    if ($person_in_charge == "")
                        $person_in_charge = " - ";
                    if ($bulk_lastmodified == "")
                        $bulk_lastmodified = " - ";
                    if ($stocktake_remark == "")
                        $stocktake_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                    
                    $location_table_content .= "<td class=\"bulk tabletext\">$person_in_charge</td>";
                    $location_table_content .= "<td class=\"bulk tabletext\">$bulk_lastmodified</td>";
                    $location_table_content .= "<td class=\"bulk tabletext\">$stocktake_remark</td></tr>\n";
                }
            }
            if (sizeof($arr_bulk_result) == 0) {
                $location_table_content .= "<tr class=\"bulk\"><td colspan=\"10\" class=\"bulk tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
            }
            
            // $location_table_content .= "<tr><td colspan=\"11\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";
            $location_table_content .= "</table><br><br>";
        }
    } else 
        if ($groupBy == 2) {
            if ($linventory->IS_ADMIN_USER($UserID))
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
            else
                $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
            
            $tmp_arr_group = $linventory->returnVector($sql);
            if (sizeof($tmp_arr_group) > 0) {
                $target_admin_group = implode(",", $tmp_arr_group);
                $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
            }
            
            // # single item ##
            $price_ceiling_cond = "";
            if ($sys_custom['eInventory_PriceCeiling']) {
                $price_ceiling_cond = "  AND (b.UnitPrice>=f.PriceCeiling) ";
            }
            $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						b.TagCode,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.LocationID,
						CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . "),
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
						b.UnitPrice,
						b.PurchasedPrice,
						a.StockTakeOption
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS building ON (e.BuildingID = building.BuildingID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
				WHERE
						a.ItemType = 1 AND a.RecordStatus = 1
				ORDER BY
						a.ItemCode";
            $arr_single_result = $linventory->returnArray($sql);
            $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
            
            $single_table_content .= "<tr><td class=\"tablename\" colspan=\"10\">$i_InventorySystem_ItemType_Single</td></tr>\n";
            $single_table_content .= "<tr class=\"tabletop\">";
            $single_table_content .= "<td class=\"tabletopnolink\" width=\"2%\"> </td>";
            $single_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
            $single_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td></tr>\n";
            
            if (sizeof($arr_single_result) > 0) {
                for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                    list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_item_locationid, $single_item_location, $single_category) = $arr_single_result[$i];
                    
                    $namefield = getNameFieldByLang2("b.");
                    $sql = "SELECT 
								a.Action,
								a.DateInput,
								IFNULL($namefield,'" . $Lang['General']['UserAccountNotExists'] . "'),
								IF(a.Remark != '', a.Remark, ' - ')
						FROM 
								INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
								INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
								a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
						ORDER BY
								a.DateInput DESC LIMIT 0,1
						";
                    $arr_result = $linventory->returnArray($sql, 4);
                    
                    if (sizeof($arr_result) > 0) {
                        for ($j = 0; $j < sizeof($arr_result); $j ++) {
                            list ($item_action, $date_input, $user_name, $item_remark) = $arr_result[$j];
                            
                            if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                                $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                            } else {
                                $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                            }
                            
                            $sql = "SELECT 
										CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
							 		FROM 
										INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
										INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
										INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
										INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
									WHERE 
										b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
									ORDER BY
										b.DateInput DESC LIMIT 0,1";
										
                            $location_result = $linventory->returnArray($sql, 1);
                            
                            if($location_result){
	                            $location_found = $location_result[0][0];
	                            
	                            if(trim($remark) !='-'){
	                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
	                            }
	                            else{
	                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
	                            }
                            }
                            
                            $table_row_css = " class=\"single\" ";
                            $single_table_content .= "<tr $table_row_css><td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$action_img</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$single_item_code</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$single_item_name</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$single_item_barcode</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">" . intranet_htmlspecialchars($single_category) . "</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_location) . "</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$user_name</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$date_input</td>";
                            $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$item_remark</td></tr>\n";
                        }
                    } else {
                        $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                        $single_table_content .= "<tr $table_row_css><td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$action_img</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$single_item_code</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$single_item_name</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$single_item_barcode</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">" . intranet_htmlspecialchars($single_category) . "</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_location) . "</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                        // if($user_name == "")
                        // $user_name = " - ";
                        // if($date_input == "")
                        // $date_input = " - ";
                        // if($item_remark == "")
                        // $item_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                        $user_name = " - ";
                        $date_input = " - ";
                        $item_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$user_name</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$date_input</td>";
                        $single_table_content .= "<td bgcolor=\"#FFFFCC\" class=\"single tabletext\">$item_remark</td></tr>\n";
                    }
                }
            }
            if (sizeof($arr_single_result) == 0) {
                $single_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"7\" class=\"single tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
            }
            
            // # Bulk Item ##
            if ($sys_custom['eInventory_PriceCeiling']) {
                $arr_targetSuccessBulkItem = array();
                $arr_targetFailBulkItem = array();
                $success_bulk_item_cond = "";
                $fail_bulk_item_cond = "";
                $sql = "SELECT 
							a.ItemID, a.CategoryID
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					WHERE
							a.ItemType = 2
					ORDER BY
							a.ItemCode";
                
                $tmp_result = $linventory->returnArray($sql, 2);
                if (sizeof($tmp_result) > 0) {
                    for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                        list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                        $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $tmp_item_id AND Action = " . ITEM_ACTION_PURCHASE . "";
                        $total_cost = $linventory->returnVector($sql);
                        
                        $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $tmp_item_id";
                        $total_qty = $linventory->returnVector($sql);
                        
                        $unit_price = $total_cost[0] / $total_qty[0];
                        
                        $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = $tmp_cat_id";
                        $tmp_result2 = $linventory->returnArray($sql, 2);
                        if (sizeof($tmp_result2) > 0) {
                            for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                                list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                                if ($apply_to_bulk == 1) {
                                    if ($unit_price >= $price_ceiling) {
                                        $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                    } else {
                                        $arr_targetFailBulkItem[] = $tmp_item_id;
                                    }
                                } else {
                                    $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                }
                            }
                        }
                    }
                }
                if (sizeof($arr_targetSuccessBulkItem) > 0) {
                    $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                    $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
                }
                if (sizeof($arr_targetFailBulkItem) > 0) {
                    $targetFailList = implode(",", $arr_targetFailBulkItem);
                    $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
                }
            }
            
            $sql = "SELECT 
						a.ItemCode, 
						a.ItemID,
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryNameByLang("c.") . ",
						d.LocationID,
						CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("e.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . "),
						CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
					" . $linventory->getInventoryNameByLang("funding.") . ",
					b.GroupInCharge,
					b.FundingSourceID,
					b.Quantity,
					a.StockTakeOption
				FROM
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
						INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
						INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS building ON (e.BuildingID = building.BuildingID) INNER JOIN
						INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
						INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
				WHERE
						a.ItemType = 2
						$success_bulk_item_cond
						$fail_bulk_item_cond
				ORDER BY
						a.ItemCode";
            $arr_bulk_result = $linventory->returnArray($sql);
            $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
            
            $bulk_table_content .= "<tr class=\"tabletop\"><td class=\"tablename\" bgcolor=\"#FFFFFF\" colspan=\"12\">$i_InventorySystem_ItemType_Bulk</td></tr>\n";
            $bulk_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Location</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_TotalMissingQty</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_TotalSurplusQty</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
            $bulk_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td></tr>\n";
            
            if (sizeof($arr_bulk_result) > 0) {
                for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                    list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_location, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                    
                    $bulk_lastmodified = "";
                    $person_in_charge = "";
                    $stocktake_remark = "";
                    
                    $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = $bulk_item_id AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id  AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                    $MaxDateInput = $linventory->returnVector($sql);
                    
                    $namefield = getNameFieldByLang2("b.");
                    $sql = "SELECT
							IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
							IFNULL($namefield,'" . $Lang['General']['UserAccountNotExists'] . "')
						FROM 
							INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
							INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
						WHERE 
							a.ACTION = 2 AND 
							a.ItemID = $bulk_item_id AND
							a.LocationID = " . $bulk_item_locationid . " 
							and a.GroupInCharge=" . $bulk_group_id . " 
							and a.FundingSource =" . $bulk_funding_id . " and
							(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
							a.DateInput IN ('" . $MaxDateInput[0] . "')
						GROUP BY
							a.ItemID
						";
                    $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                    if (sizeof($arr_stock_take_time_person) > 0)
                        list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                        
                        /*
                     * $sql = "SELECT
                     * b.Quantity
                     * FROM
                     * INVENTORY_ITEM AS a INNER JOIN
                     * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                     * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                     * WHERE
                     * a.ItemID = ".$bulk_item_id." AND
                     * b.LocationID = ".$bulk_item_locationid;
                     *
                     * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                     * if(sizeof($arr_stock_take_quantity) > 0)
                     * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                     * if($bulk_item_quantity=="")
                     * $bulk_item_quantity=0;
                     */
                    
                    $sql = "SELECT 
								StockCheckQty,
								IF(Remark='' OR Remark is null,'-',Remark),
								QtyChange
						FROM 
								INVENTORY_ITEM_BULK_LOG 
						WHERE 
								ItemID = $bulk_item_id AND 
								LocationID = $bulk_item_locationid AND
								GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
								Action IN (2,3,4) AND
								(RecordDate BETWEEN '$start_date' and '$end_date') AND
								DateInput IN ('" . $MaxDateInput[0] . "')
						ORDER BY
								DateInput DESC
						";
                    $arr_stocktake_qty = $linventory->returnArray($sql, 2);
                    if (sizeof($arr_stocktake_qty) > 0) {
                        list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                    } else {
                        $stocktake_remark = "";
                        $stocktake_qty_change = 0;
                        $stocktake_qty = 0;
                    }
                    
                    // $item_different = $stocktake_qty - $bulk_item_quantity;
                    $item_different = $stocktake_qty_change;
                    // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                    
                    $table_row_css = " class=\"Bulk\" ";
                    $bulk_table_content .= "<tr $table_row_css><td class=\"bulk tabletext\">$bulk_item_code</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">$bulk_item_name</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_item_category) . "</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_item_location) . "</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_item_group) . "</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">$expect_qty</td>";
                    if ($item_different > 0) {
                        $bulk_table_content .= "<td class=\"bulk tabletext\">0</td>";
                        $bulk_table_content .= "<td class=\"bulk tabletext\">$item_different</td>";
                    }
                    if ($item_different < 0) {
                        $bulk_table_content .= "<td class=\"bulk tabletext\">$item_different</td>";
                        $bulk_table_content .= "<td class=\"bulk tabletext\">0</td>";
                    }
                    if ($item_different == 0) {
                        $bulk_table_content .= "<td class=\"bulk tabletext\">0</td>";
                        $bulk_table_content .= "<td class=\"bulk tabletext\">0</td>";
                    }
                    if ($person_in_charge == "")
                        $person_in_charge = " - ";
                    if ($bulk_lastmodified == "")
                        $bulk_lastmodified = " - ";
                    if ($stocktake_remark == "")
                        $stocktake_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">$person_in_charge</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">$bulk_lastmodified</td>";
                    $bulk_table_content .= "<td class=\"bulk tabletext\">$stocktake_remark</td></tr>\n";
                }
            }
            if (sizeof($arr_bulk_result) == 0) {
                $bulk_table_content .= "<tr class=\"tablebluerow1\"><td colspan=\"11\" class=\"bulk tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
            }
        } else 
            if ($groupBy == 3) {
                if (is_array($targetGroup)) {
                    $cond = "WHERE AdminGroupID in (" . implode($targetGroup, ",") . ")";
                    $group_list = implode(",", $targetGroup);
                }
                
                if ($linventory->IS_ADMIN_USER($UserID)) {
                    $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
                } else {
                    $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
                }
                $arr_group = $linventory->returnArray($sql, 2);
                
                for ($a = 0; $a < sizeof($arr_group); $a ++) {
                    list ($group_name, $admin_group_id) = $arr_group[$a];
                    
                    if ($linventory->IS_ADMIN_USER($UserID))
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                    
                    $tmp_arr_group = $linventory->returnVector($sql);
                    if (sizeof($tmp_arr_group) > 0) {
                        $target_admin_group = implode(",", $tmp_arr_group);
                        $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
                    }
                    
                    // ## Single Item ###
                    $price_ceiling_cond = "";
                    if ($sys_custom['eInventory_PriceCeiling']) {
                        $price_ceiling_cond = " AND (b.UnitPrice>=f.PriceCeiling) ";
                    }
                    $sql = "SELECT 
							a.ItemCode, 
							a.ItemID,
							b.TagCode,
							" . $linventory->getInventoryItemNameByLang("a.") . ",
							" . $linventory->getInventoryNameByLang("c.") . ",
							d.LocationID,
							CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
							b.UnitPrice,
							b.PurchasedPrice,
							a.StockTakeOption
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition $price_ceiling_cond) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
					WHERE
							a.ItemType = 1 AND 
							c.AdminGroupID = " . $admin_group_id . " AND
							a.RecordStatus = 1
					ORDER BY
							a.ItemCode";
                    $arr_single_result = $linventory->returnArray($sql);
                    $arr_single_result = $linventory->FilterForStockTakeSingle($arr_single_result);
                    
                    $group_table_content .= "<table border=\"0\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
                    $group_table_content .= "<tr><td colspan=\"10\">" . $linterface->GET_NAVIGATION2("$group_name") . "</td></tr>\n";
                    $group_table_content .= "<tr><td class=\"tablename\" colspan=\"10\">$i_InventorySystem_ItemType_Single</td></tr>\n";
                    $group_table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\" width=\"2%\"> </td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
                    $group_table_content .= "</tr>\n";
                    if (sizeof($arr_single_result) > 0) {
                        for ($i = 0; $i < sizeof($arr_single_result); $i ++) {
                            list ($single_item_code, $single_item_id, $single_item_barcode, $single_item_name, $single_item_group, $single_locationid, $single_item_category) = $arr_single_result[$i];
                            
                            $namefield = getNameFieldByLang2("b.");
                            $sql = "SELECT 
									a.Action,
									a.DateInput,
									IFNULL($namefield,'" . $Lang['General']['UserAccountNotExists'] . "'),
									IF(a.Remark != '', a.Remark, ' - ')
							FROM 
									INVENTORY_ITEM_SINGLE_STATUS_LOG AS a LEFT OUTER JOIN 
									INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
							WHERE 
									a.Action IN (2,3,4) AND a.ItemID = $single_item_id AND (a.RecordDate BETWEEN '$start_date' and '$end_date')
							ORDER BY
									a.DateInput DESC LIMIT 0,1
							";
                            $arr_result = $linventory->returnArray($sql, 4);
                            if (sizeof($arr_result) > 0) {
                                for ($j = 0; $j < sizeof($arr_result); $j ++) {
                                    list ($item_action, $item_record_date, $user_name, $remark) = $arr_result[$j];
                                    
                                    if (($item_action == ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION) || ($item_action == ITEM_ACTION_STOCKTAKE_OTHER_LOCATION)) {
                                        $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                                    } else {
                                        $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                                    }
                                    
		                            $sql = "SELECT 
												CONCAT(" . $linventory->getInventoryNameByLang("building1.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("f.") . ")
									 		FROM 
												INVENTORY_ITEM_SURPLUS_RECORD AS b INNER JOIN
												INVENTORY_LOCATION AS f ON (b.LocationID = f.LocationID) INNER JOIN 
												INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
												INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID)
											WHERE 
												b.ItemID = $single_item_id AND (b.RecordDate BETWEEN '$start_date' and '$end_date')
											ORDER BY
												b.DateInput DESC LIMIT 0,1";
												
		                            $location_result = $linventory->returnArray($sql, 1);
		                            
		                            if($location_result){
			                            $location_found = $location_result[0][0];
			                            
			                            if(trim($remark) !='-'){
			                            	$remark .= "<br/>(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
			                            }
			                            else{
			                            	$remark = "(".$Lang['eInventory']['StocktakeLocation'].": ".$location_found.")";
			                            }
		                            }
                                    
                                    $table_row_css = " class=\"Single\" ";
                                    $group_table_content .= "<tr $table_row_css>";
                                    $group_table_content .= "<td class=\"single tabletext\">$action_img</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$single_item_code</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$single_item_name</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$single_item_barcode</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$user_name</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$item_record_date</td>";
                                    $group_table_content .= "<td class=\"single tabletext\">$remark</td>";
                                    $group_table_content .= "</tr>\n";
                                }
                            } else {
                                $action_img = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                                $table_row_css = " class=\"Single\" ";
                                $group_table_content .= "<tr $table_row_css>";
                                $group_table_content .= "<td class=\"single tabletext\">$action_img</td>";
                                $group_table_content .= "<td class=\"single tabletext\">$single_item_code</td>";
                                $group_table_content .= "<td class=\"single tabletext\">$single_item_name</td>";
                                $group_table_content .= "<td class=\"single tabletext\">$single_item_barcode</td>";
                                $group_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_category) . "</td>";
                                $group_table_content .= "<td class=\"single tabletext\">" . intranet_htmlspecialchars($single_item_group) . "</td>";
                                // if($user_name == "")
                                // $user_name = " - ";
                                // if($item_record_date == "")
                                // $item_record_date = " - ";
                                // if($remark == "")
                                // $remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                                $user_name = " - ";
                                $item_record_date = " - ";
                                $item_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                                $group_table_content .= "<td class=\"single tabletext\">$user_name</td>";
                                $group_table_content .= "<td class=\"single tabletext\">$item_record_date</td>";
                                $group_table_content .= "<td class=\"single tabletext\">$item_remark</td>";
                                $group_table_content .= "</tr>\n";
                            }
                        }
                    }
                    if (sizeof($arr_single_result) == 0) {
                        $group_table_content .= "<tr class=\"single\"><td colspan=\"11\" class=\"single tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
                    }
                    
                    // ## Bulk item ###
                    if ($linventory->IS_ADMIN_USER($UserID))
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                    else
                        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                    
                    $tmp_arr_group = $linventory->returnVector($sql);
                    if (sizeof($tmp_arr_group) > 0) {
                        $target_admin_group = implode(",", $tmp_arr_group);
                        $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
                    }
                    
                    if ($sys_custom['eInventory_PriceCeiling']) {
                        $arr_targetSuccessBulkItem = array();
                        $arr_targetFailBulkItem = array();
                        $success_bulk_item_cond = "";
                        $fail_bulk_item_cond = "";
                        $sql = "SELECT 
								a.ItemID, a.CategoryID
						FROM
								INVENTORY_ITEM AS a INNER JOIN
								INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
								INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
								INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
								INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
								INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
						WHERE
								a.ItemType = 2 AND 
								c.AdminGroupID = " . $admin_group_id . "
						ORDER BY
								a.NameEng
						";
                        $tmp_result = $linventory->returnArray($sql, 2);
                        if (sizeof($tmp_result) > 0) {
                            for ($i = 0; $i < sizeof($tmp_result); $i ++) {
                                list ($tmp_item_id, $tmp_cat_id) = $tmp_result[$i];
                                $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $tmp_item_id AND Action = " . ITEM_ACTION_PURCHASE . "";
                                $total_cost = $linventory->returnVector($sql);
                                
                                $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $tmp_item_id";
                                $total_qty = $linventory->returnVector($sql);
                                
                                $unit_price = $total_cost[0] / $total_qty[0];
                                
                                $sql = "SELECT PriceCeiling, ApplyPriceCeilingToBulk FROM INVENTORY_CATEGORY WHERE CategoryID = $tmp_cat_id";
                                $tmp_result2 = $linventory->returnArray($sql, 2);
                                if (sizeof($tmp_result2) > 0) {
                                    for ($j = 0; $j < sizeof($tmp_result2); $j ++) {
                                        list ($price_ceiling, $apply_to_bulk) = $tmp_result2[$j];
                                        if ($apply_to_bulk == 1) {
                                            if ($unit_price >= $price_ceiling) {
                                                $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                            } else {
                                                $arr_targetFailBulkItem[] = $tmp_item_id;
                                            }
                                        } else {
                                            $arr_targetSuccessBulkItem[] = $tmp_item_id;
                                        }
                                    }
                                }
                            }
                        }
                        if (sizeof($arr_targetSuccessBulkItem) > 0) {
                            $targetSuccessList = implode(",", $arr_targetSuccessBulkItem);
                            $success_bulk_item_cond = " AND a.ItemID IN ($targetSuccessList) ";
                        }
                        if (sizeof($arr_targetFailBulkItem) > 0) {
                            $targetFailList = implode(",", $arr_targetFailBulkItem);
                            $fail_bulk_item_cond = " AND a.ItemID NOT IN ($targetFailList) ";
                        }
                    }
                    
                    $sql = "SELECT 
							a.ItemCode, 
							a.ItemID,
							" . $linventory->getInventoryItemNameByLang("a.") . ",
							" . $linventory->getInventoryNameByLang("c.") . ",
							d.locationid,
							CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
							" . $linventory->getInventoryNameByLang("funding.") . ",
							b.GroupInCharge,
							b.FundingSourceID,
							b.Quantity,
							a.StockTakeOption
					FROM
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0) INNER JOIN
							INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID $cond) INNER JOIN
							INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS e ON (d.LocationLevelID = e.LocationLevelID) INNER JOIN
							INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID AND a.RecordStatus = 1 $item_type_condition) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
					WHERE
							a.ItemType = 2 AND 
							c.AdminGroupID = " . $admin_group_id . "
							$success_bulk_item_cond
							$fail_bulk_item_cond
					ORDER BY
							a.NameEng
					";
                    $arr_bulk_result = $linventory->returnArray($sql);
                    $arr_bulk_result = $linventory->FilterForStockTakeBulk($arr_bulk_result);
                    
                    $group_table_content .= "</table>";
                    $group_table_content .= "<br>";
                    $group_table_content .= "<table border=\"0\" width=\"100%\" align=\"center\" cellspacing=\"0\" cellpadding=\"5\">";
                    $group_table_content .= "<tr><td class=\"tablename\" colspan=\"11\">$i_InventorySystem_ItemType_Bulk</td></tr>\n";
                    $group_table_content .= "<tr class=\"tabletop\">";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Category'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_TotalMissingQty</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_TotalSurplusQty</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_LastStocktakeBy</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Last_Stock_Take_Time</td>";
                    $group_table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
                    $group_table_content .= "</tr>\n";
                    
                    if (sizeof($arr_bulk_result) > 0) {
                        for ($i = 0; $i < sizeof($arr_bulk_result); $i ++) {
                            list ($bulk_item_code, $bulk_item_id, $bulk_item_name, $bulk_item_group, $bulk_item_locationid, $bulk_item_category, $bulk_funding, $bulk_group_id, $bulk_funding_id, $expect_qty) = $arr_bulk_result[$i];
                            
                            $bulk_lastmodified = "";
                            $person_in_charge = "";
                            $stocktake_remark = "";
                            
                            $sql = "SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE Action = 2 And ItemID = $bulk_item_id AND LocationID = $bulk_item_locationid and GroupInCharge= $bulk_group_id and FundingSource = $bulk_funding_id AND (RecordDate BETWEEN '$start_date' and '$end_date')";
                            $MaxDateInput = $linventory->returnVector($sql);
                            
                            $namefield = getNameFieldByLang2("b.");
                            $sql = "SELECT
								IF(MAX(a.DateInput) != '', MAX(a.DateInput), CONCAT(' - ')), 
								IFNULL($namefield,'" . $Lang['General']['UserAccountNotExists'] . "')
							FROM 
								INVENTORY_ITEM_BULK_LOG AS a LEFT OUTER JOIN 
								INTRANET_USER AS b ON (a.PersonInCharge = b.UserID) 
							WHERE 
								a.ACTION = 2 AND 
								a.ItemID = $bulk_item_id AND
								a.LocationID = " . $bulk_item_locationid . " 
								and a.GroupInCharge=" . $bulk_group_id . " 
								and a.FundingSource =" . $bulk_funding_id . " and
								(a.RecordDate BETWEEN '$start_date' and '$end_date') AND 
								a.DateInput IN ('" . $MaxDateInput[0] . "')
							GROUP BY
								a.ItemID
							";
                            $arr_stock_take_time_person = $linventory->returnArray($sql, 2);
                            
                            if (sizeof($arr_stock_take_time_person) > 0)
                                list ($bulk_lastmodified, $person_in_charge) = $arr_stock_take_time_person[0];
                                
                                /*
                             * $sql = "SELECT
                             * b.Quantity
                             * FROM
                             * INVENTORY_ITEM AS a INNER JOIN
                             * INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) INNER JOIN
                             * INVENTORY_ITEM_BULK_EXT AS c ON (a.ItemID = c.ItemID AND c.Quantity != 0)
                             * WHERE
                             * a.ItemID = ".$bulk_item_id." AND
                             * b.LocationID = ".$bulk_item_locationid;
                             *
                             * $arr_stock_take_quantity = $linventory->returnArray($sql,2);
                             * if(sizeof($arr_stock_take_quantity) > 0)
                             * list($bulk_item_quantity) = $arr_stock_take_quantity[0];
                             * if($bulk_item_quantity=="")
                             * $bulk_item_quantity=0;
                             */
                            
                            $sql = "SELECT 
									StockCheckQty,
									IF(Remark='' OR Remark is null,'-',Remark),
									QtyChange
							FROM 
									INVENTORY_ITEM_BULK_LOG 
							WHERE 
									ItemID = $bulk_item_id AND 
									LocationID = $bulk_item_locationid AND
									GroupInCharge=$bulk_group_id and FundingSource=$bulk_funding_id and
									Action IN (2,3,4) AND
									(RecordDate BETWEEN '$start_date' and '$end_date') AND
									DateInput IN ('" . $MaxDateInput[0] . "')
							ORDER BY
									DateInput DESC
							";
                            $arr_stocktake_qty = $linventory->returnArray($sql);
                            if (sizeof($arr_stocktake_qty) > 0) {
                                list ($stocktake_qty, $stocktake_remark, $stocktake_qty_change) = $arr_stocktake_qty[0];
                            } else {
                                $stocktake_remark = "";
                                $stocktake_qty_change = 0;
                                $stocktake_qty = 0;
                            }
                            
                            // $item_different = $stocktake_qty - $bulk_item_quantity;
                            $item_different = $stocktake_qty_change;
                            // $expect_qty = $stocktake_qty - $stocktake_qty_change;
                            
                            $table_row_css = " class=\"Bulk\" ";
                            $group_table_content .= "<tr $table_row_css><td class=\"bulk tabletext\">$bulk_item_code</td>";
                            $group_table_content .= "<td class=\"bulk tabletext\">$bulk_item_name</td>";
                            $group_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_item_category) . "</td>";
                            $group_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_item_group) . "</td>";
                            $group_table_content .= "<td class=\"bulk tabletext\">" . intranet_htmlspecialchars($bulk_funding) . "</td>";
                            $group_table_content .= "<td class=\"bulk tabletext\">$expect_qty</td>";
                            if ($item_different > 0) {
                                $group_table_content .= "<td class=\"bulk tabletext\">0</td>";
                                $group_table_content .= "<td class=\"bulk tabletext\">$item_different</td>";
                            }
                            if ($item_different < 0) {
                                $group_table_content .= "<td class=\"bulk tabletext\">$item_different</td>";
                                $group_table_content .= "<td class=\"bulk tabletext\">0</td>";
                            }
                            if ($item_different == 0) {
                                $group_table_content .= "<td class=\"bulk tabletext\">0</td>";
                                $group_table_content .= "<td class=\"bulk tabletext\">0</td>";
                            }
                            if ($person_in_charge == "")
                                $person_in_charge = " - ";
                            if ($bulk_lastmodified == "")
                                $bulk_lastmodified = " - ";
                            if ($stocktake_remark == "")
                                $stocktake_remark = "<font color=\"red\">$i_InventorySystem_Report_Stocktake_NotDone</font>";
                            
                            $group_table_content .= "<td class=\"bulk tabletext\">$person_in_charge</td>";
                            $group_table_content .= "<td class=\"bulk tabletext\">$bulk_lastmodified</td>";
                            $group_table_content .= "<td class=\"bulk tabletext\">$stocktake_remark</td></tr>\n";
                        }
                    }
                    if (sizeof($arr_bulk_result) == 0) {
                        $group_table_content .= "<tr class=\"bulk\"><td colspan=\"10\" class=\"bulk tabletext\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
                    }
                    
                    // $group_table_content .= "<tr><td colspan=\"10\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";
                    $group_table_content .= "</table><br><br>";
                }
            }
}

?>
<?=$js_init?>
<script language="javascript">

function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("TargetFloor")){
			document.getElementById("TargetFloor").value = "";
		}
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("TargetRoom")){
			document.getElementById("TargetRoom").value = "";
		}
	}
}
function openPrintPage(StartDate, EndDate, TargetLocation, TargetGroup, TargetProgress,GroupBy, GroupByValue)
{
	newWindow("stocktake_result_print.php?start_date='"+StartDate+"'&end_date='"+EndDate+"'&targetLocation="+TargetLocation+"&targetGroup="+TargetGroup+"&targetProgress="+TargetProgress+"&groupBy="+GroupBy+"&selected_cb="+GroupByValue,10);
}

function checkForm()
{
	var groupby = document.getElementById('groupBy').value;
	var location_cb = 0;
	var group_cb = 0;
	
	if(groupby == 1) {
		if(document.getElementById('TargetRoom[]')) {
			for(i=0; i<document.getElementById('TargetRoom[]').length; i++) {
				if(document.getElementById('TargetRoom[]')[i].selected == true) {
					if(document.getElementById('TargetRoom[]')[i].value != "") {
						location_cb++;
					}
				}
			}
		}
	}
	if(groupby == 3){
		if(document.getElementById('targetGroup[]')) {
			for(i=0; i<document.getElementById('targetGroup[]').length; i++) {
				if(document.getElementById('targetGroup[]')[i].selected == true) {
					if(document.getElementById('targetGroup[]')[i].value != "") {
						group_cb++;
					}
				}
			}
		}
	}
		
	if(location_cb>0||group_cb>0||groupby==2){
		document.form1.step.value = "final";
		return true;
	}
	else if(groupby==1)
	{
		alert('<?=$i_InventorySystem['jsLocationCheckBox']?>');
		return false;
	}
	else if(groupby==3)
	{
		alert('<?=$i_InventorySystem['jsGroupCheckBox']?>');
		return false;	
	}
	else if(groupby=="")
	{
		alert('<?=$i_InventorySystem['jsLocationGroupCheckBox']?>');
		return false;	
	}
}

$(document).ready(function(){ 
	$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
	$('#start_date').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
	$('#end_date').datepick({
	dateFormat: 'yy-mm-dd',
	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
	changeFirstDay: false,
	firstDay: 0
	});
});

// direct to export page
function click_export(x){
	window.location="stocktake_result_export.php?start_date='<?=$start_date?>'&end_date='<?=$end_date?>'&targetLocation='<?=$targetLocation?>'&targetGroup='<?=$targetGroup?>'&targetProgress='<?=$targetProgress?>'&groupBy=<?=$groupBy?>&selected_cb=<?=$groupby_val?>&export_type="+x;
}

</script>

<br>
<form name="form1" action="stocktake_result.php" method="POST"
	onSubmit="return checkForm()">
	<table border="0" width="90%" align="center" cellspacing="0"
		cellpadding="5">
<?=$table_content;?>
</table>
	<br>

<? if($groupBy!="") { ?>
<table border="0" width="90%" cellspacing="0" cellpadding="5">
<?=$toolbar;?>
</table>
	<br>
<? } ?>

<? if($groupBy==2) {?>	
<table border="0" width="100%" align="center" cellpadding="4"
		cellspacing="0">
<?=$single_table_content;?>
</table>
	<br>
	<br>
<? } ?>

<? if($groupBy==2) {?>
<table border="0" width="100%" align="center" cellspacing="0"
		cellpadding="5">
<?=$bulk_table_content;?>
</table>
<?}?>

<?
if ($groupBy == 1)
    echo $location_table_content;
else 
    if ($groupBy == 3)
        echo $group_table_content;
?>

<input type="hidden" name="flag" value="1"> <input type="hidden"
		name="step" value="">
</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>