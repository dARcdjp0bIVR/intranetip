<?php

// ###################################################
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2013-06-24 YatWoon
// fixed: set the checking array to array type to prevent error that cause by empty value with function in_array [Case#2013-0624-0959-35071]
//
// Date: 2013-05-10 YatWoon
// fixed: a vairable is overwrite the foreach loop variable that cause the result is messed. [Case#2013-0508-1106-39156]
//
// Date: 2012-05-24 YatWoon
// fixed: incorrect sql query
//
// Date: 2011-06-13 YatWoon
// revised retrieve location query (fixed: retrieve written-off items)
//
// ###################################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$lexport = new libexporttext();

$start_date = str_replace("'", "", stripslashes($start_date));
$end_date = str_replace("'", "", stripslashes($end_date));
$targetLocation = str_replace("'", "", stripslashes($targetLocation));
$targetGroup = str_replace("'", "", stripslashes($targetGroup));
$targetProgress = str_replace("'", "", stripslashes($targetProgress));

if ($start_date == "") {
    $start_date = $linventory->getStocktakePeriodStart();
}
if ($end_date == "") {
    $end_date = $linventory->getStocktakePeriodEnd();
}

// get stock checked bulk item#
if ($targetLocation != 1) {
    $cond1 = " ";
} else {
    $cond1 = " AND b.LocationID IN ($selected_cb) ";
}
if ($groupBy != 3) {
    $cond2 = " ";
} else {
    $cond2 = " AND b.GroupInCharge IN ($selected_cb) ";
}
$cond3 = " AND c.Action = 2";

if ($targetLocationLevel == "") {
    $cond4 = " ";
} else {
    $cond4 = " AND e.LocationLevelID = $targetLocationLevel";
}

$sql = "SELECT
				DISTINCT b.RecordID
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN 
				INVENTORY_ITEM_BULK_LOG AS c ON (b.ItemID = c.ItemID) INNER JOIN 
				INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN 
				INVENTORY_LOCATION_LEVEL e ON (d.LocationLevelID = e.LocationLevelID)
		WHERE
				(c.RecordDate BETWEEN '$start_date' AND '$end_date')
				$cond1
				$cond2
				$cond3
				$cond4
		";

$arr_result = $linventory->returnArray($sql, 1);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($checked_bulk_item) = $arr_result[$i];
        $arr_checked_bulk_list[] = $checked_bulk_item;
    }
}
if (sizeof($arr_checked_bulk_list) > 0) {
    $checked_bulk_list = implode(",", $arr_checked_bulk_list);
} else {
    $checked_bulk_list = "";
}

// get stock checked single item #
if ($targetLocation == "") {
    $cond1 = " ";
} else {
    $cond1 = " AND b.LocationID = $targetLocation ";
}
if ($targetGroup == "") {
    $cond2 = " ";
} else {
    $cond2 = " AND b.GroupInCharge = $targetGroup ";
}

$cond3 = " AND a.Action = 2";

if ($targetLocationLevel == "") {
    $cond4 = " ";
} else {
    $cond4 = " AND e.LocationLevelID = $targetLocationLevel";
}
$sql = "SELECT 
					DISTINCT a.ItemID 
			FROM 
					INVENTORY_ITEM_SINGLE_STATUS_LOG AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
			WHERE 
					(a.RecordDate BETWEEN '$start_date' AND '$end_date')
					$cond1
					$cond2
					$cond3
			";

$arr_result = $linventory->returnArray($sql, 1);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($checked_single_item) = $arr_result[$i];
        $arr_checked_single_list[$i] = $checked_single_item;
    }
}
if (sizeof($arr_checked_single_list) > 0) {
    $checked_single_list = implode(",", $arr_checked_single_list);
} else {
    $checked_single_list = "";
}

$file_content .= "\"$i_InventorySystem_Report_Stocktake_Progress\"\n";
$file_content .= "\"$i_general_startdate\",";
$file_content .= "\"$start_date\"\n";
$file_content .= "\"$i_general_enddate\",";
$file_content .= "\"$end_date\"\n";

$exportColumn = array(
    $i_InventorySystem_Report_Stocktake_Progress,
    '',
    '',
    '',
    ''
);
$rows[] = array(
    $i_general_startdate,
    $start_date
);
$rows[] = array(
    $i_general_enddate,
    $end_date
);

if ($groupBy == 1) {
    $single_cond = "AND b.LocationID in ($selected_cb)";
    $bulk_cond = "AND a.LocationID in ($selected_cb)";
    $selected_location = $selected_cb;
    $arr_selected_location = explode(",", $selected_cb);
}
if ($groupBy == 2) {}
if ($groupBy == 3) {
    $single_cond = "AND b.LocationID in ($selected_cb)";
    $bulk_cond = "AND a.LocationID in ($selected_cb)";
}
// ## Get Non-written off Item ###
$sql = "SELECT ItemID FROM INVENTORY_ITEM WHERE RecordStatus = 1";
$arr_item_list = $linventory->returnVector($sql);

if (sizeof($arr_item_list) > 0) {
    $item_list = implode(",", $arr_item_list);
}

if ($groupBy == 1) {
    // $selected_location;
    
    if ($linventory->IS_ADMIN_USER($UserID))
        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
    else
        $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
    
    $tmp_arr_group = $linventory->returnVector($sql);
    if (sizeof($tmp_arr_group) > 0) {
        $target_admin_group = implode(",", $tmp_arr_group);
    }
    // # end ##
    
    // get all location with related admin group #
    $sql = "SELECT 
				DISTINCT a.LocationID, b.GroupInCharge, c.GroupInCharge,
					b1.RecordStatus,c1.RecordStatus  
			FROM 
				INVENTORY_LOCATION AS a LEFT OUTER JOIN 
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.LocationID = b.LocationID) 
				LEFT join INVENTORY_ITEM as b1 on (b1.ItemID=b.ItemID)
				LEFT OUTER JOIN 
				INVENTORY_ITEM_BULK_LOCATION AS c ON (a.LocationID = c.LocationID and c.Quantity>0) 
				LEFT join INVENTORY_ITEM as c1 on (c1.ItemID=c.ItemID)
			WHERE
				a.LocationID IN ($selected_location)
				and (b1.RecordStatus=1 or c1.RecordStatus=1)
				";
    $tmp_arr = $linventory->returnArray($sql);
    
    if (sizeof($tmp_arr) > 0) {
        for ($i = 0; $i < sizeof($tmp_arr); $i ++) {
            list ($location_id, $single_item_group, $bulk_item_group, $single_status, $bulk_status) = $tmp_arr[$i];
            $arr_tmp_location[] = $location_id;
            
            if (($single_item_group != "") && ($bulk_item_group != "") && $single_status && $bulk_status) {
                if ($single_item_group != $bulk_item_group) {
                    $arr_tmp_group[$location_id][] = $single_item_group;
                    $arr_tmp_group[$location_id][] = $bulk_item_group;
                    $arr_tmp_location_record_type[$location_id]['type'] = 3;
                } else {
                    $arr_tmp_group[$location_id][] = $single_item_group;
                    $arr_tmp_location_record_type[$location_id]['type'] = 3;
                }
            } else 
                if (($single_item_group != "") && ($bulk_item_group == "") && $single_status) {
                    $arr_tmp_group[$location_id][] = $single_item_group;
                    $arr_tmp_location_record_type[$location_id]['type'] = 1;
                } else 
                    if (($single_item_group == "") && ($bulk_item_group != "") && $bulk_status) {
                        $arr_tmp_group[$location_id][] = $bulk_item_group;
                        $arr_tmp_location_record_type[$location_id]['type'] = 2;
                    }
        }
    }
    if (! empty($arr_tmp_location)) {
        $arr_tmp_location = array_unique($arr_tmp_location);
        
        // ## Start to Gen stocktake done ###
        foreach ($arr_tmp_location as $location_id) {
            $admin_list = implode(",", $arr_tmp_group[$location_id]);
            $arrayGroupInCharge = array_unique($arr_tmp_group[$location_id]);
            
            foreach ($arrayGroupInCharge as $key => $group_in_charge) {
                // ## single
                $sql = "SELECT 
									a.LocationID, a.GroupInCharge, b.PersonInCharge, MAX(b.DateInput)
							FROM 
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
									INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) 
							WHERE 
									a.LocationID = $location_id AND 
									a.GroupInCharge IN ($group_in_charge) AND 
									b.Action IN (2,3,4) AND
									a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge ='".$group_in_charge."' AND LocationID = $location_id) AND
									(b.RecordDate BETWEEN '$start_date' AND '$end_date') AND
									b.DateInput IN (SELECT MAX(b.DateInput) FROM INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) WHERE a.LocationID = $location_id AND  a.GroupInCharge IN ($group_in_charge) AND b.Action IN (2,3,4) AND a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = '".$group_in_charge."' AND LocationID = $location_id) AND (b.RecordDate BETWEEN '$start_date' AND '$end_date'))
							GROUP BY 
									a.LocationID, a.GroupInCharge";
                $temp_array1 = $linventory->returnArray($sql, 4);
                if (sizeof($temp_array1) > 0) {
                    list ($location_id, $group_id, $uid, $date_input) = $temp_array1[0];
                    $single_stocktake_done_rec[] = array(
                        $location_id,
                        $group_id,
                        $uid,
                        $date_input
                    );
                }
                
                // ## bulk
                $sql = "SELECT
									LocationID, GroupInCharge, PersonInCharge, MAX(RecordDate)
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									LocationID = $location_id AND
									GroupInCharge IN ($admin_list) AND 
									Action IN (2,3,4) AND
									ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = '".$group_in_charge."' AND LocationID = $location_id) AND 
									(RecordDate BETWEEN '$start_date' AND '$end_date') AND
									DateInput IN (SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE LocationID = $location_id AND GroupInCharge IN ($admin_list) AND  Action IN (2,3,4) AND ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = '".$group_in_charge."' AND LocationID = $location_id) AND  (RecordDate BETWEEN '$start_date' AND '$end_date'))
							GROUP BY
									LocationID, GroupInCharge";
                $temp_array2 = $linventory->returnArray($sql, 4);
                if (sizeof($temp_array2) > 0) {
                    list ($location_id, $group_id, $uid, $date_input) = $temp_array2[0];
                    $bulk_stocktake_done_rec[] = array(
                        $location_id,
                        $group_id,
                        $uid,
                        $date_input
                    );
                }
            }
            
            // debug_pr("======================= single_stocktake_done_rec ");
            // debug_pr($single_stocktake_done_rec);
            //
            // debug_pr("======================= bulk_stocktake_done_rec ");
            // debug_pr($bulk_stocktake_done_rec);
            
            if (sizeof($single_stocktake_done_rec) > 0) {
                for ($i = 0; $i < sizeof($single_stocktake_done_rec); $i ++) {
                    list ($location_id, $admin_group_id, $user_id, $record_date) = $single_stocktake_done_rec[$i];
                    
                    if (in_array($admin_group_id, $tmp_arr_group)) {
                        // only add in array if record not exists
                        $exists = 0;
                        if (sizeof($stocktake_done_record) > 0) {
                            foreach ($stocktake_done_record as $k => $d) {
                                list ($this_location_id, $this_admin_group_id, $this_user_id, $this_record_date) = $d;
                                if ($location_id == $this_location_id && $admin_group_id == $this_admin_group_id) {
                                    $exists = 1;
                                    break;
                                }
                            }
                        }
                        
                        if (! $exists) {
                            $stocktake_done[] = $location_id;
                            $stocktake_done_record[] = array(
                                $location_id,
                                $admin_group_id,
                                $user_id,
                                $record_date
                            );
                        }
                    }
                }
            }
            
            if (sizeof($bulk_stocktake_done_rec) > 0) {
                for ($i = 0; $i < sizeof($bulk_stocktake_done_rec); $i ++) {
                    list ($location_id, $admin_group_id, $user_id, $record_date) = $bulk_stocktake_done_rec[$i];
                    
                    if (in_array($admin_group_id, $tmp_arr_group)) {
                        // only add in array if record not exists
                        $exists = 0;
                        if (sizeof($stocktake_done_record) > 0) {
                            foreach ($stocktake_done_record as $k => $d) {
                                list ($this_location_id, $this_admin_group_id, $this_user_id, $this_record_date) = $d;
                                if ($location_id == $this_location_id && $admin_group_id == $this_admin_group_id) {
                                    $exists = 1;
                                    break;
                                }
                            }
                        }
                        
                        if (! $exists) {
                            $stocktake_done[] = $location_id;
                            $stocktake_done_record[] = array(
                                $location_id,
                                $admin_group_id,
                                $user_id,
                                $record_date
                            );
                        }
                    }
                }
            }
            
            if ($arr_tmp_location_record_type[$location_id]['type'] == 0) {
                $arr_no_item_record[] = $location_id;
            }
        }
        // ## Gen Stocktake DONE End ###
    }
    
    if (! empty($arr_tmp_group)) {
        // #### Build stocktake_not_done array
        foreach ($arr_tmp_group as $this_location_id => $this_groups) {
            // if not in stocktake_done_record, then append in stocktake_not_done_rec
            foreach ($this_groups as $temp_k => $this_group) {
                $exists = 0;
                if (sizeof($stocktake_done_record) > 0) {
                    foreach ($stocktake_done_record as $k => $d) {
                        list ($temp_location_id, $temp_admin_group_id, $temp_user_id, $temp_record_date) = $d;
                        if ($this_location_id == $temp_location_id && $this_group == $temp_admin_group_id) {
                            $exists = 1;
                            break;
                        }
                    }
                }
                if (! $exists) {
                    $stocktake_not_done_location[] = $this_location_id;
                    $stocktake_not_done_rec[$this_location_id][] = $this_group;
                }
            }
        }
    }
    
    // ## Gen Stocktake Not Done ###
    if (sizeof($arr_no_item_record) > 0) {
        if (sizeof($stocktake_done) > 0) {
            $stocktake_not_done = array_diff($arr_selected_location, $stocktake_done);
            if (is_array($arr_no_item_record))
                $stocktake_not_done = array_diff($stocktake_not_done, $arr_no_item_record);
        } else {
            $stocktake_not_done = $arr_selected_location;
        }
        
        foreach ($stocktake_not_done as $not_done_location) {
            if (sizeof($arr_tmp_group[$not_done_location]) > 0) {
                for ($i = 0; $i < sizeof($arr_tmp_group[$not_done_location]); $i ++) {
                    $arr_tmp_group[$not_done_location] = array_unique($arr_tmp_group[$not_done_location]);
                    $target_admin_group = $arr_tmp_group[$not_done_location][$i];
                    
                    // ## Check Stocktake not done location is belong to user or not ###
                    if (in_array($target_admin_group, $tmp_arr_group)) {
                        $stocktake_not_done_record[] = array(
                            $not_done_location,
                            $target_admin_group
                        );
                    }
                }
            }
        }
    }
    
    if (sizeof($stocktake_not_done_location) > 0) {
        $stocktake_not_done_location = array_unique($stocktake_not_done_location);
        
        foreach ($stocktake_not_done_location as $stocktake_not_done_location_id) {
            if (sizeof($stocktake_not_done_rec[$stocktake_not_done_location_id]) > 0) {
                $temp_stocktake_not_done_arr = array_unique($stocktake_not_done_rec[$stocktake_not_done_location_id]);
                
                foreach ($temp_stocktake_not_done_arr as $stocktake_not_done_group_id) {
                    $stocktake_not_done_record[] = array(
                        $stocktake_not_done_location_id,
                        $stocktake_not_done_group_id
                    );
                }
            }
        }
    }
    // ## End of Gen stocktake not done ###
    
    $file_content .= "\n";
    $file_content .= "\"$i_InventorySystem_Stocktake_ProgressFinished\"\n";
    $file_content .= "\"" . $i_InventorySystem['Location'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\"\n";
    
    $rows[] = array(
        ''
    );
    $rows[] = array(
        $i_InventorySystem_Stocktake_ProgressFinished
    );
    $rows[] = array(
        $i_InventorySystem['Location'],
        $i_InventorySystem['Caretaker'],
        $i_InventorySystem_Stocktake_LastStocktakeBy,
        $i_InventorySystem_Last_Stock_Take_Time
    );
    
    if (sizeof($stocktake_done_record) > 0) {
        for ($i = 0; $i < sizeof($stocktake_done_record); $i ++) {
            list ($location_id, $admin_group_id, $user_id, $record_date) = $stocktake_done_record[$i];
            
            $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
						CONCAT(" . $linventory->getInventoryNameByLang("c.") . "),
						IFNULL(" . getNameFieldByLang2(".d.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
					FROM
						INVENTORY_LOCATION AS a INNER JOIN
						INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
						INVENTORY_ADMIN_GROUP AS c,
						INTRANET_USER AS d
					WHERE
						a.LocationID = $location_id AND
						c.AdminGroupID = $admin_group_id AND
						d.UserID = $user_id
					";
            $arr_result = $linventory->returnArray($sql, 3);
            
            if (sizeof($arr_result) > 0) {
                list ($location, $admin_group, $user_name) = $arr_result[0];
                
                $file_content .= "\"$location\",\"$admin_group\",\"$user_name\",\"$record_date\"\n";
                $rows[] = array(
                    $location,
                    $admin_group,
                    $user_name,
                    $record_date
                );
            }
        }
    } else {
        $file_content .= "\"$i_no_record_exists_msg\"\n";
        $rows[] = array(
            $i_no_record_exists_msg
        );
    }
    
    $file_content .= "\n";
    $file_content .= "\"$i_InventorySystem_Stocktake_ProgressNotFinished\"\n";
    $file_content .= "\"" . $i_InventorySystem['Location'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\"\n";
    
    $rows[] = array(
        ''
    );
    $rows[] = array(
        $i_InventorySystem_Stocktake_ProgressNotFinished
    );
    $rows[] = array(
        $i_InventorySystem['Location'],
        $i_InventorySystem['Caretaker']
    );
    
    if (sizeof($stocktake_not_done_record) > 0) {
        for ($i = 0; $i < sizeof($stocktake_not_done_record); $i ++) {
            list ($location_id, $admin_group_id) = $stocktake_not_done_record[$i];
            $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
						CONCAT(" . $linventory->getInventoryNameByLang("c.") . ")
					FROM 
						INVENTORY_LOCATION AS a INNER JOIN
						INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
						INVENTORY_ADMIN_GROUP AS c
					WHERE 
						a.LocationID = $location_id AND
						c.AdminGroupID = $admin_group_id
					";
            $arr_result = $linventory->returnArray($sql, 2);
            if (sizeof($arr_result) > 0) {
                list ($location, $admin_group) = $arr_result[0];
                
                $file_content .= "\"$location\",\"$admin_group\"\n";
                $rows[] = array(
                    $location,
                    $admin_group
                );
            }
        }
    } else {
        $file_content .= "\"$i_no_record_exists_msg\"\n";
        $rows[] = array(
            $i_no_record_exists_msg
        );
    }
} else 
    if ($groupBy == 3) {
        $group_list = $selected_cb;
        $groupByGroupid = explode(",", $group_list);
        
        // # use to control what data will be shown ##
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
        }
        // # end ##
        
        // get all location with related Management group #
        $sql = "SELECT 
				DISTINCT a.AdminGroupID, b.LocationID, c.LocationID,
					b1.RecordStatus,c1.RecordStatus
			FROM 
				INVENTORY_ADMIN_GROUP AS a LEFT OUTER JOIN 
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.AdminGroupID = b.GroupInCharge) 
				LEFT join INVENTORY_ITEM as b1 on (b1.ItemID=b.ItemID)
				LEFT OUTER JOIN 
				INVENTORY_ITEM_BULK_LOCATION AS c ON (a.AdminGroupID = c.GroupInCharge and c.Quantity>0) 
				LEFT join INVENTORY_ITEM as c1 on (c1.ItemID=c.ItemID)
			WHERE
				a.AdminGroupID IN ($group_list)
				 and (b1.RecordStatus=1 or c1.RecordStatus=1)
				";
        $tmp_arr = $linventory->returnArray($sql);
        
        if (sizeof($tmp_arr) > 0) {
            for ($i = 0; $i < sizeof($tmp_arr); $i ++) {
                list ($admin_group_id, $single_location_id, $bulk_location_id, $single_status, $bulk_status) = $tmp_arr[$i];
                $arr_tmp_admin_group[] = $admin_group_id;
                
                if (($single_location_id != "") && ($bulk_location_id != "") && $single_status && $bulk_status) {
                    if ($single_location_id != $bulk_location_id) {
                        $arr_tmp_location[$admin_group_id][] = $single_location_id;
                        $arr_tmp_location[$admin_group_id][] = $bulk_location_id;
                        $arr_tmp_location_record_type[$admin_group_id]['type'] = 3;
                    } else {
                        $arr_tmp_location[$admin_group_id][] = $single_location_id;
                        // $arr_tmp_group[$location_id][] = $bulk_item_group;
                        $arr_tmp_location_record_type[$admin_group_id]['type'] = 3;
                    }
                } else 
                    if (($single_location_id != "") && ($bulk_location_id == "") && $single_status) {
                        $arr_tmp_location[$admin_group_id][] = $single_location_id;
                        $arr_tmp_location_record_type[$admin_group_id]['type'] = 1;
                    } else 
                        if (($single_location_id == "") && ($bulk_location_id != "") && $bulk_status) {
                            $arr_tmp_location[$admin_group_id][] = $bulk_location_id;
                            $arr_tmp_location_record_type[$admin_group_id]['type'] = 2;
                        }
            }
        }
        if (sizeof($arr_tmp_admin_group)) {
            $arr_tmp_admin_group = array_unique($arr_tmp_admin_group);
            
            // ## Start to Gen stocktake done ###
            foreach ($arr_tmp_admin_group as $admin_group_id) {
                $stocktake_done_rec = array();
                if ($arr_tmp_location_record_type[$admin_group_id]['type'] == 3) {
                    $arr_location_list = array_unique($arr_tmp_location[$admin_group_id]);
                    $location_list = implode(",", $arr_location_list);
                    $arrayLocationList = array_unique($arr_tmp_location[$admin_group_id]);
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT 
										a.LocationID, a.GroupInCharge, b.PersonInCharge, MAX(b.DateInput)
								FROM 
										INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
										INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) 
								WHERE 
										a.GroupInCharge = $admin_group_id AND 
										a.LocationID IN ($location_id) AND 
										b.Action IN (2,3,4) AND
										a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
										(b.RecordDate BETWEEN '$start_date' AND '$end_date') AND
										b.DateInput IN (SELECT MAX(b.DateInput) FROM INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) WHERE a.LocationID = $location_id AND  a.GroupInCharge IN ($admin_group_id) AND b.Action IN (2,3,4) AND a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (b.RecordDate BETWEEN '$start_date' AND '$end_date'))
								GROUP BY 
										a.LocationID";
                        $temp_array1 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array1) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array1[0];
                            $single_stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    
                    if (sizeof($single_stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($single_stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $single_stocktake_done_rec[$i];
                            
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT
									LocationID, GroupInCharge, PersonInCharge, MAX(RecordDate)
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									GroupInCharge = $admin_group_id AND
									LocationID IN ($location_id) AND 
									Action IN (2,3,4) AND
									ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
									(RecordDate BETWEEN '$start_date' AND '$end_date') AND 
									DateInput IN (SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE GroupInCharge = $admin_group_id AND LocationID IN ($location_id) AND Action IN (2,3,4) AND ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (RecordDate BETWEEN '$start_date' AND '$end_date'))
							GROUP BY
									LocationID";
                        
                        $temp_array2 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array2) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array2[0];
                            $bulk_stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    
                    if (sizeof($bulk_stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($bulk_stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $bulk_stocktake_done_rec[$i];
                            
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    
                    if (sizeof($stocktake_done_location[$admin_group_id]) > 0) {
                        $arr_not_done_location = array_diff($arr_location_list, $stocktake_done_location[$admin_group_id]);
                        if (sizeof($arr_not_done_location) > 0) {
                            foreach ($arr_not_done_location as $not_done_location_id) {
                                $stocktake_not_done_record[] = array(
                                    $admin_group_id,
                                    $not_done_location_id
                                );
                            }
                        }
                    }
                }
                
                if ($arr_tmp_location_record_type[$admin_group_id]['type'] == 2) {
                    // ## Stocktake done - only Bulk Item ###
                    $arr_location_list = array_unique($arr_tmp_location[$admin_group_id]);
                    $location_list = implode(",", $arr_location_list);
                    $arrayLocationList = array_unique($arr_tmp_location[$admin_group_id]);
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT
									LocationID, GroupInCharge, PersonInCharge, MAX(RecordDate)
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									GroupInCharge = $admin_group_id AND
									LocationID IN ($location_id) AND 
									Action IN (2,3,4) AND
									ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
									(RecordDate BETWEEN '$start_date' AND '$end_date') AND 
									DateInput IN (SELECT MAX(DateInput) FROM INVENTORY_ITEM_BULK_LOG WHERE GroupInCharge = $admin_group_id AND LocationID IN ($location_id) AND Action IN (2,3,4) AND ItemID IN (SELECT Distinct ItemID FROM INVENTORY_ITEM_BULK_LOCATION WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (RecordDate BETWEEN '$start_date' AND '$end_date'))
							GROUP BY
									LocationID";
                        
                        $temp_array3 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array3) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array3[0];
                            $stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    if (sizeof($stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $stocktake_done_rec[$i];
                            
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    
                    if (sizeof($stocktake_done_location[$admin_group_id]) > 0) {
                        $arr_not_done_location = array_diff($arr_location_list, $stocktake_done_location[$admin_group_id]);
                        if (sizeof($arr_not_done_location) > 0) {
                            foreach ($arr_not_done_location as $not_done_location_id) {
                                $stocktake_not_done_record[] = array(
                                    $admin_group_id,
                                    $not_done_location_id
                                );
                            }
                        }
                    }
                }
                if ($arr_tmp_location_record_type[$admin_group_id]['type'] == 1) {
                    // ## Stocktake done - only Single Item ###
                    $arr_location_list = array_unique($arr_tmp_location[$admin_group_id]);
                    $location_list = implode(",", $arr_location_list);
                    $arrayLocationList = array_unique($arr_tmp_location[$admin_group_id]);
                    foreach ($arrayLocationList as $key => $location_id) {
                        $sql = "SELECT 
									a.LocationID, a.GroupInCharge, b.PersonInCharge, MAX(b.DateInput)
							FROM 
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
									INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) 
							WHERE 
									a.GroupInCharge = $admin_group_id AND 
									a.LocationID IN ($location_id) AND 
									b.Action IN (2,3,4) AND
									a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND
									(b.RecordDate BETWEEN '$start_date' AND '$end_date') AND
									b.DateInput IN (SELECT MAX(b.DateInput) FROM INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) WHERE a.LocationID = $location_id AND  a.GroupInCharge IN ($admin_group_id) AND b.Action IN (2,3,4) AND a.ItemID IN (SELECT DISTINCT ItemID FROM INVENTORY_ITEM_SINGLE_EXT WHERE GroupInCharge = $admin_group_id AND LocationID = $location_id) AND (b.RecordDate BETWEEN '$start_date' AND '$end_date'))
							GROUP BY 
									a.LocationID";
                        $temp_array4 = $linventory->returnArray($sql, 4);
                        if (sizeof($temp_array4) > 0) {
                            list ($location_id, $group_id, $uid, $date_input) = $temp_array4[0];
                            $stocktake_done_rec[] = array(
                                $location_id,
                                $group_id,
                                $uid,
                                $date_input
                            );
                        }
                    }
                    if (sizeof($stocktake_done_rec) > 0) {
                        for ($i = 0; $i < sizeof($stocktake_done_rec); $i ++) {
                            list ($location_id, $tmp_admin_group_id, $user_id, $record_date) = $stocktake_done_rec[$i];
                            
                            if (in_array($tmp_admin_group_id, $tmp_arr_group)) {
                                $stocktake_done[] = $tmp_admin_group_id;
                                if (! in_array($location_id, (array) $stocktake_done_location[$tmp_admin_group_id])) {
                                    $stocktake_done_location[$tmp_admin_group_id][] = $location_id;
                                    $stocktake_done_record[] = array(
                                        $location_id,
                                        $tmp_admin_group_id,
                                        $user_id,
                                        $record_date
                                    );
                                }
                            }
                        }
                    }
                    
                    if (sizeof($stocktake_done_location[$admin_group_id]) > 0) {
                        $arr_not_done_location = array_diff($arr_location_list, $stocktake_done_location[$admin_group_id]);
                        if (sizeof($arr_not_done_location) > 0) {
                            foreach ($arr_not_done_location as $not_done_location_id) {
                                $stocktake_not_done_record[] = array(
                                    $admin_group_id,
                                    $not_done_location_id
                                );
                            }
                        }
                    }
                }
                if ($arr_tmp_location_record_type[$location_id]['type'] == 0) {
                    $arr_no_item_record[] = $location_id;
                }
            }
        }
        // ## Gen Stocktake DONE End ###
        
        // ## Gen Stocktake Not Done ###
        if (sizeof($stocktake_done) > 0) {
            $stocktake_not_done = array_diff($groupByGroupid, $stocktake_done);
            if (is_array($arr_no_item_record))
                $stocktake_not_done = array_diff($stocktake_not_done, $arr_no_item_record);
        } else {
            $stocktake_not_done = $groupByGroupid;
        }
        
        foreach ($stocktake_not_done as $not_done_group) {
            if (sizeof($arr_tmp_location[$not_done_group]) > 0)
                $arr_recorded_location = array_unique($arr_tmp_location[$not_done_group]);
            else
                $arr_recorded_location = $arr_tmp_location[$not_done_group];
            
            if (sizeof($arr_recorded_location) > 0) {
                foreach ($arr_recorded_location as $target_location) {
                    // ## Check Stocktake not done location is belong to user or not ###
                    if (in_array($not_done_group, $tmp_arr_group)) {
                        $stocktake_not_done_record[] = array(
                            $not_done_group,
                            $target_location
                        );
                    }
                }
            }
        }
        // ## End of Gen stocktake not done ###
        
        $file_content .= "\n";
        $file_content .= "\"$i_InventorySystem_Stocktake_ProgressFinished\"\n";
        $file_content .= "\"" . $i_InventorySystem['Location'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\",\"$i_InventorySystem_Stocktake_LastStocktakeBy\",\"$i_InventorySystem_Last_Stock_Take_Time\"\n";
        
        $rows[] = array(
            ''
        );
        $rows[] = array(
            $i_InventorySystem_Stocktake_ProgressFinished
        );
        $rows[] = array(
            $i_InventorySystem['Location'],
            $i_InventorySystem['Caretaker'],
            $i_InventorySystem_Stocktake_LastStocktakeBy,
            $i_InventorySystem_Last_Stock_Take_Time
        );
        
        if (sizeof($stocktake_done_record) > 0) {
            for ($i = 0; $i < sizeof($stocktake_done_record); $i ++) {
                list ($location_id, $admin_group_id, $user_id, $record_date) = $stocktake_done_record[$i];
                
                $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
						CONCAT(" . $linventory->getInventoryNameByLang("c.") . "),
						IFNULL(" . getNameFieldByLang2(".d.") . ",'" . $Lang['eInventory']['FieldTitle']['DeletedStaff'] . "')
					FROM
						INVENTORY_LOCATION AS a INNER JOIN
						INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
						INVENTORY_ADMIN_GROUP AS c,
						INTRANET_USER AS d
					WHERE
						a.LocationID = $location_id AND
						c.AdminGroupID = $admin_group_id AND
						d.UserID = $user_id
					";
                $arr_result = $linventory->returnArray($sql, 3);
                
                if (sizeof($arr_result) > 0) {
                    list ($location, $admin_group, $user_name) = $arr_result[0];
                    
                    $file_content .= "\"$location\",\"$admin_group\",\"$user_name\",\"$record_date\"\n";
                    $rows[] = array(
                        $location,
                        $admin_group,
                        $user_name,
                        $record_date
                    );
                }
            }
        } else {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        
        $file_content .= "\n";
        $file_content .= "\"$i_InventorySystem_Stocktake_ProgressNotFinished\"\n";
        $file_content .= "\"" . $i_InventorySystem['Location'] . "\",\"" . $i_InventorySystem['Caretaker'] . "\"\n";
        
        $rows[] = array(
            ''
        );
        $rows[] = array(
            $i_InventorySystem_Stocktake_ProgressNotFinished
        );
        $rows[] = array(
            $i_InventorySystem['Location'],
            $i_InventorySystem['Caretaker']
        );
        
        if (sizeof($stocktake_not_done_record) > 0) {
            for ($i = 0; $i < sizeof($stocktake_not_done_record); $i ++) {
                list ($admin_group_id, $location_id) = $stocktake_not_done_record[$i];
                $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("b.") . ",'>'," . $linventory->getInventoryNameByLang("a.") . "),
						CONCAT(" . $linventory->getInventoryNameByLang("c.") . ")
					FROM 
						INVENTORY_LOCATION AS a INNER JOIN
						INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID),
						INVENTORY_ADMIN_GROUP AS c
					WHERE 
						a.LocationID = $location_id AND
						c.AdminGroupID = $admin_group_id
					";
                $arr_result = $linventory->returnArray($sql, 2);
                if (sizeof($arr_result) > 0) {
                    list ($location, $admin_group) = $arr_result[0];
                    
                    $file_content .= "\"$location\",\"$admin_group\"\n";
                    $rows[] = array(
                        $location,
                        $admin_group
                    );
                }
            }
        } else {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
    }

$display = $file_content;

intranet_closedb();

$filename = "stocktake_result_unicode.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);
?>