<?php

# using: 

#########################
#	Date:	2016-02-02	Henry
#			PHP 5.4 fix: change split to explode
#
#	Date:	2012-03-20 (YatWoon)
#			fixed for bulk item, get written-off item qty missing (sum)
#
#	Date:	2012-03-07 (YatWoon)
#			support period setting on purchase date
#
#	Date:	2011-07-22	YatWoon
#			add "Sponsoring body" for funding type
#			distinct purchase date for bulk item
#
#	Date:	2011-06-30 (YatWoon)
# 			add option "Display"
#
#	Date:	2011-06-09 (YatWoon)
# 			add option "Display written-off items" (with var $display_writeoff) [Case#2011-0607-1458-36073]
#
#	Date:	2011-06-02 (Yuen)
#		calculate price for bulk item base on average unit price * quantity
#
#	Date:	2011-04-21	YatWoon
#		 Check bulk item quantity > 0
#
#########################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Report_FixedAssetsRegister";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$li 			= new libfilesystem();

$DisplayPurchasePrice = $ShowPurchasePrice;
$DisplayLocation = $ShowLocation;
$DisplayWriteOffInfo = $ShowWriteOffInfo;
$DisplaySignature = $ShowSignature;
$DisplayRemark = $ShowRemark;

if ($PurchaseDateFrom!="")
{
	$sql_purchase_period1 = " unix_timestamp(b.purchasedate)>=unix_timestamp('{$PurchaseDateFrom}') ";
	$sql_purchase_period2 = " unix_timestamp(e.purchasedate)>=unix_timestamp('{$PurchaseDateFrom}') ";
}
if ($PurchaseDateTo!="")
{
	$sql_purchase_period1 .= ($sql_purchase_period1 ? "AND" : "")." unix_timestamp(b.purchasedate)<=unix_timestamp('{$PurchaseDateTo}') ";
	$sql_purchase_period2 .= ($sql_purchase_period2 ? "AND" : "")." unix_timestamp(e.purchasedate)<=unix_timestamp('{$PurchaseDateTo}') ";
}
if($sql_purchase_period1)
	$sql_purchase_period = "and ((". $sql_purchase_period1 .") or (e.Action = 1 and ". $sql_purchase_period2 ."))";


$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root."/file/schoolbadge.txt");
if ($school_badge != "")
{
    $badge_image = "<img src='/file/$school_badge'>";
}


$report_header .= "<tr><td class=\"eSportprinttext\" width=\"10%\">".$i_InventorySystem['Category']."</td>";
$report_header .= "<td class=\"eSportprinttext\">";
if($CatID != "")
{
	$CatID = IntegerSafe($CatID);
	$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY WHERE CategoryID = '".$CatID."'";
	$arr_cat_name = $linventory->returnVector($sql);
	$report_header .= $arr_cat_name[0];

	if($Cat2ID != "")
	{
		$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$CatID."' AND Category2ID = '".$Cat2ID."'";
		$arr_sub_cat_name = $linventory->returnVector($sql);
		$report_header .= " > ".$arr_sub_cat_name[0];
	}
}
else
{
	$report_header .= $i_InventorySystem_ViewItem_All_Category;
}
$report_header .= "</td></tr>";

$report_header .= "<tr><td class=\"eSportprinttext\" width=\"10%\">$i_InventorySystem_Location_Level</td>";
$report_header .= "<td class=\"eSportprinttext\">";

if($LocationID != "")
{
	$LocationID = IntegerSafe($LocationID);
	$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_LOCATION_LEVEL WHERE LocationLevelID = '".$LocationID."'";
	$arr_location = $linventory->returnVector($sql);
	$report_header .= $arr_location[0];

	if($Location2ID != "")
	{
		$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$LocationID."' AND LocationID = '".$Location2ID."'";
		$arr_sub_locaton = $linventory->returnVector($sql);
		$report_header .= " > ".$arr_sub_locaton[0];
	}
}
else
{
	$report_header .= $i_InventorySystem_ViewItem_All_Location_Level;
}
$report_header .= "</td></tr>";
	                                                                                                                          
	
if($CatID != "" || $Cat2ID != "" || $LocationID != "" || $Location2ID != "")
	$cond .= " WHERE ";

if($CatID != "")
{
	$cond .= " ITEM.CategoryID = '".$CatID."' ";
	if($Cat2ID != "")
		$cond .= " AND ITEM.Category2ID = '".$Cat2ID."' ";
	$cnt++;
}

if($LocationID != "")
{
	if($cnt > 0)
		$cond .= " AND ";
	$cond .= " LL.LocationLevelID = '".$LocationID."' ";
	if($Location2ID != "")
		$cond .= " AND L.LocationID = '".$Location2ID."' ";
}

if($groupBy==1)
{
	# get all location #
	$location_namefield = $linventory->getInventoryNameByLang("a.");
	$location_level_namefield = $linventory->getInventoryNameByLang("b.");
	$sql = "SELECT a.LocationLevelID, a.LocationID, $location_namefield as LocName, $location_level_namefield as LevName 
	FROM INVENTORY_LOCATION a , INVENTORY_LOCATION_LEVEL b WHERE a.LocationLevelID = b.LocationLevelID ORDER BY b.DisplayOrder, a.DisplayOrder";
	$location_array = $linventory->returnArray($sql,2);

	if (is_array($location_array))
	{
		foreach($location_array as $Key=>$Value)
		{
			if($_POST['location_cb'.$Value['LocationID']])
			{
				$selected_cb .= $Value['LocationID'].",";
			}
		}
	}
	
	$selected_cb = substr($selected_cb,0,-1);

	$sql = "Select 
				DISTINCT a.LocationID, CONCAT(".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("a.").")
			FROM 
				INVENTORY_LOCATION AS a INNER JOIN
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID)
			WHERE 
				a.LocationID in (".$selected_cb.") 
			ORDER BY 
				b.DisplayOrder, a.DisplayOrder";
	$arr_location = $linventory->returnArray($sql,1);
	
	for($a=0; $a<sizeof($arr_location); $a++)
	{	
		list($locationID, $location_name) = $arr_location[$a];
		
		if(!$display_writeoff)
		{
			$wo_con = " and a.RecordStatus=1 ";
			$wo_conb = " and c.Quantity>0";
		}
		
		# Get Bulk item id, item code, item name, item description #
		$sql = "SELECT 
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					".$linventory->getInventoryNameByLang("a.").",
					".$linventory->getInventoryDescriptionNameByLang("a.").",
					".$linventory->getInventoryNameByLang("AG.").",
					AG.AdminGroupID
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID OR c.LocationID = d.LocationID)
					left join INVENTORY_ITEM_BULK_LOG as e on (e.ItemID=a.ItemID)
					left join INVENTORY_ADMIN_GROUP AS AG ON (AG.AdminGroupID = b.GroupInCharge or AG.AdminGroupID = c.GroupInCharge) 
				WHERE
					d.LocationID IN ($locationID)
					$sql_purchase_period
					$wo_con
				ORDER BY
					a.ItemCode
				";
				//debug_pr($sql);
		$arr_result = $linventory->returnArray($sql);
		
		$location_table_content .= "<br>"; 
		$location_table_content .= "<table border=\"0\" width=\"100%\" class=\"eSporttableborder\" style='table-layout:auto' align=\"center\" cellspacing=\"0\" cellpadding=\"3\">";
		/*
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"95px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"95px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"80px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"60px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"50px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"50px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"50px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"50px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"50px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"80px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"80px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"80px\">\n";
		$location_table_content .= "<col style=\"word-wrap:break-word\" width=\"80px\">\n";
		*/
		
		$location_table_content .= "<col width=\"95px\">\n";
		$location_table_content .= "<col width=\"95px\">\n";
		$location_table_content .= "<col width=\"80px\">\n";
		$location_table_content .= "<col width=\"60px\">\n";
		$location_table_content .= "<col width=\"50px\">\n";
		$location_table_content .= "<col width=\"50px\">\n";
		$location_table_content .= "<col width=\"50px\">\n";
		$location_table_content .= "<col width=\"50px\">\n";
		if ($DisplayPurchasePrice)
			$location_table_content .= "<col width=\"50px\">\n";
		if ($DisplayLocation)
			$location_table_content .= "<col width=\"80px\">\n";
		if ($DisplayWriteOffInfo)
			$location_table_content .= "<col width=\"80px\">\n";
		if ($DisplaySignature)
			$location_table_content .= "<col width=\"80px\">\n";
		if ($DisplayRemark)
			$location_table_content .= "<col width=\"80px\">\n";
			
		$location_table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" bgcolor=\"#FFFFFF\" colspan=\"14\">".$i_InventorySystem_Item_Location." : ".intranet_htmlspecialchars($location_name)."</td></tr>";
		$location_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" rowspan=\"2\">$i_InventorySystem_Item_Code</td>";
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Item."</td>";
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Description."</td>";
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Purchase_Date."</td>";
		
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Sch_Unit_Price."</td>";
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Quantity."</td>";
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"3\" align=\"center\">".$i_InventorySystem_Report_Col_Cost."</td>";
		
		if ($DisplayPurchasePrice)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."</td>";
		if ($DisplayLocation)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Location."</td>";
		if ($DisplayWriteOffInfo)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off."</td>";
		if ($DisplaySignature)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Signature."</td>";
		if ($DisplayRemark)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Remarks."</td></tr>";
		$location_table_content .="<tr >
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['School']."</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['Government']."</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['SponsoringBody']."</td></tr>";
		
		if(sizeof($arr_result)>0 )
		{
			for($i=0; $i<sizeof($arr_result); $i++)
			{
				list($item_id, $item_type, $item_code, $item_name, $item_description, $item_group, $item_group_id) = $arr_result[$i];
				$location_table_content.="<tr><td class=\"eSporttdborder eSportprinttext\">".$item_code."</td>";
				$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">".$item_name."</td>";
				$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">".(($item_description!="")?$item_description:'&nbsp')."</td>";
				
				if($item_type == ITEM_TYPE_SINGLE)
				{
					$sql = "SELECT
									a.PurchaseDate, 
									if(a.PurchasedPrice != '',a.PurchasedPrice,'0'),
									if(a.UnitPrice != '',a.UnitPrice,'0'),
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									d.FundingType,
									".$linventory->getInventoryNameByLang("d.").",
									e.ApproveDate,
									e.WriteOffReason,
									a.ItemRemark
							FROM
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
							WHERE
									a.ItemID = $item_id
							";
					$arr_sub_result = $linventory->returnArray($sql);
					if(sizeof($arr_sub_result)>0)
					{
						for($j=0; $j<sizeof($arr_sub_result); $j++)
						{
							list($purchase_date,$purchase_price,$unit_price,$location_id,$location_name,$funding_type,$funding_name,$write_off_date,$write_off_reason,$remark) = $arr_sub_result[$j];
							$quantity = 1;
							
							if ($purchase_price > $unit_price * $quantity & $unit_price>0)
								$purchase_price = $unit_price * $quantity;
							
							if ($purchase_date=="0000-00-00")
								$purchase_date = "--";
								
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$purchase_date."</td>";
							
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>". number_format($unit_price,2) ."</td>";
							
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>".(($quantity!="")?$quantity:'&nbsp')."</td>";
							
							/*
							if($ShowFundingName){
								$str_funding_name = "<br />(".$funding_name.")";
							} else
							{
								$str_col_fund = " align='right' ";
							}
							*/
							
							
							$school_funding = "0.00";
							$gov_funding = "0.00";
							$sponsor_funding = "0.00";
							if($ShowFundingName)
							{
								$str_funding_name = "<br />(".$funding_name.")";
							}
							if ($purchase_price==0)
							{
								$str_funding_name = "";
							}
							$str_col_fund = " align='right' ";
							if($funding_type == ITEM_SCHOOL_FUNDING)
							{
								$school_funding = number_format($purchase_price,2).$str_funding_name;
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{
								$gov_funding = number_format($purchase_price,2).$str_funding_name;
							}
							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
							{
								$sponsor_funding = number_format($purchase_price,2).$str_funding_name;
							}
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund>". $school_funding ."</td>";
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund>". $gov_funding ."</td>";
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund>". $sponsor_funding ."</td>";
							
							if ($DisplayPurchasePrice)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>". number_format($purchase_price,2) ."</td>";
							if ($DisplayLocation)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($location_name!="")?intranet_htmlspecialchars($location_name):'&nbsp')."</td>";
							if ($DisplayWriteOffInfo)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($write_off_date!="")?$write_off_date:'&nbsp').(($write_off_reason)?"(".$write_off_reason.")":'&nbsp')."</td>";
							if ($DisplaySignature)			
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($signature!="")?$signature:'&nbsp')."</td>";
							if ($DisplayRemark)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($remark!="")?$remark:'&nbsp')."</td>";
							$location_table_content .= "</tr>";
						}
					}
					
				}
				if($item_type == ITEM_TYPE_BULK)
				{
					$final_purchase_date = "";
					
					/*
					AND
									LocationID = $locationID
									*/
					### GET PURCHASE DATE ###
					$sql = "SELECT
									distinct(IF(PurchaseDate IS NULL, '', PurchaseDate))
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									ItemID = $item_id AND
									Action = 1 
									and GroupInCharge=$item_group_id
							order by PurchaseDate 
							";
					$arr_purchase_date = $linventory->returnArray($sql,1);
					
					if(sizeof($arr_purchase_date)>0)
					{
						$final_purchase_date .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						for($j=0; $j<sizeof($arr_purchase_date); $j++)
						{
							list($purchase_date) = $arr_purchase_date[$j];
							if($purchase_date != '')
							{
								$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
							}else{
								$final_purchase_date = '<tr><td class=\"eSportprinttext\"> - </td></tr>';
							}
						}
						$final_purchase_date .= "</table>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$final_purchase_date</td>";
					}
					else
					{
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\"> - </td>";
					}
					
					### GET COST, UNIT PRICE, PURCHASED PRICE ###
					$total_school_cost = 0;
					$total_gov_cost = 0;
					$final_unit_price = 0;
					$final_purchase_price = 0;
					$total_qty = 0;
					$tmp_unit_price = 0;
					$tmp_purchase_price = 0;
					$written_off_qty = 0;
					$location_qty = 0;
					$ini_total_qty = 0;

					## get Total purchase price ##
					//$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					$sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($total_purchase_price) = $tmp_result[0];
					}
					## get Total Qty Of the target Item ##
					//$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($totalQty) = $tmp_result[0];
					}
					## get written-off qty ##
					$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = $item_id and RecordStatus=1";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($written_off_qty) = $tmp_result[0];
					}
					
					## get the qty in the target location ##
// 					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $locationID";
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $locationID and GroupInCharge=$item_group_id";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($location_qty) = $tmp_result[0];
					}
					
					## Location origial qty (by location only)##
// 					$sql = "SELECT WriteOffQty FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = $item_id AND LocationID = $locationID and RecordStatus=1";
					$sql = "SELECT WriteOffQty FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = $item_id AND LocationID = $locationID and AdminGroupID=$item_group_id and RecordStatus=1";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$ini_location_qty = $tmp_result[0] + $location_qty;
					}else{
						$ini_location_qty = $location_qty;
					}
					/*
					$sql = "SELECT 
									a.FundingSource, 
									b.FundingType,
									".$linventory->getInventoryNameByLang("b.")."
							FROM 
									INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
									INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
							WHERE 
									a.ItemID = $item_id AND
									a.Action IN (1,7) AND
									a.LocationID = $locationID AND 
									a.FundingSource IS NOT NULL
							";
					$tmp_result = $linventory->returnArray($sql);
					if(sizeof($tmp_result)>0){
						list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
					}
					*/
					$ini_total_qty = $totalQty + $written_off_qty;
					if ($ini_total_qty>0)
						$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
					//$total_funding_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
					$total_funding_cost = $location_qty*$final_unit_price;
					$final_purchase_price = $total_funding_cost;
					if($ShowFundingName){
						$str_funding_name = "<br>(".$funding_name.")";
					} 
					$str_col_fund = " align='right' ";
					if ($total_funding_cost<=0)
					{
						$str_funding_name = "";
					}	
					
					### Get Qty By Location ###
					$sql = "SELECT 
									a.Quantity,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									f.FundingType,
									". $linventory->getInventoryNameByLang("f.") ." as funding_name,
									a.FundingSourceID
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
									inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
							WHERE
									a.ItemID = $item_id AND 
									a.LocationID = $locationID 
									and a.GroupInCharge=$item_group_id
							";
					$arr_tmp_qty = $linventory->returnArray($sql);
					if(sizeof($arr_tmp_qty)>0)
					{
						$display_qty = "";
						$display_location_name = "";
						
						for($j=0; $j<sizeof($arr_tmp_qty); $j++)
						{
							list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
							
							if($qty != "")
							{
								$display_qty = $qty;
							}
							if($location_name != "")
							{
								$display_location_name = intranet_htmlspecialchars($location_name)."<BR>";
							}
						}	
					}			
					$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($final_unit_price,2)."</td>";
					# change from $display_qty to $location_qty;
					$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($location_qty)."</td>";	
					
					# retrieve funding data
					$tmp_funding_src = array();
					for($j=0; $j<sizeof($arr_tmp_qty); $j++)
					{
						list($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
						
						$tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
						$tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
						$tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
					}
					$str_col_fund = " align='right' ";
					for($type_i=1;$type_i<=3;$type_i++)
					{
						$this_field = "";
						if(!empty($tmp_funding_src[$type_i]))
						{
							$this_fund_cost = 0;
							
							foreach($tmp_funding_src[$type_i] as $k=>$d)
							{
								if($ShowFundingName)
								{
									$this_fund_cost = number_format($d['cost'],2);
									$this_fund_src = ($d['cost']>0) ?"<br />(". $d['src_name'].")" : "";
									$this_field .= ($this_field ? "<br><br>" : "") . $this_fund_cost . $this_fund_src;
								}
								else
								{
									$this_fund_cost += $d['cost'];
									$this_field = number_format($this_fund_cost,2); 
								}
							}
						}
						$this_field = $this_field ? $this_field : "0.00";
						
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
					}				
					/*
					if($funding_type == ITEM_SCHOOL_FUNDING)
					{
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_gov_cost,2)."</td>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";

					}
					if($funding_type == ITEM_GOVERNMENT_FUNDING)
					{
									
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_school_cost,2)."</td>";
					}			
					*/
					
					if ($DisplayPurchasePrice)
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($total_funding_cost,2)."</td>";
					if ($DisplayLocation)
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">$display_location_name</td>";	
						
					if ($DisplayWriteOffInfo)
					{
						### Get Write-Off Date & Reason ###
						$display_write_off = "";
						$sql = "SELECT
										ApproveDate,
										WriteOffQty,
										WriteOffReason
								FROM
										INVENTORY_ITEM_WRITE_OFF_RECORD
								WHERE
										ItemID = $item_id AND 
										LocationID = $locationID
								";
						$arr_write_off = $linventory->returnArray($sql,2);
						if(sizeof($arr_write_off)>0)
						{
							$display_write_off .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<sizeof($arr_write_off); $j++)
							{
								list($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
								
								if($write_off_date != "")
								{
									$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
									$tmp_write_off = $write_off_date."<BR>".$write_off_qty."<BR>".$write_off_reason;
								
									if($j!=0){
										$display_write_off .= "<tr height=\"5px\"><td></td></tr>";
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}else{
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}
								}
							}
							$display_write_off .= "</table>";
	
							if($display_write_off != "")
								$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">$display_write_off</td>";
							else
								$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
						else
						{
							$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
					}
					
					### Empty Space for Signature and remark ###
					If ($DisplaySignature)
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
					If ($DisplayRemark)	
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
				}			
			}
		}
		if(sizeof($arr_result) == 0 )
		{
			$location_table_content .= "<tr><td colspan=\"14\" class=\"eSporttdborder eSportprinttext \" align=\"center\">$i_no_record_exists_msg</td></tr>";
		}
		$location_table_content.="</table><br> ";
	}
			
}
else if($groupBy==2)
{
	$arr_item_type = array(array("1",$i_InventorySystem_ItemType_Single),array("2",$i_InventorySystem_ItemType_Bulk));

	if(sizeof($arr_item_type)>0)
	{
		for($a=0; $a<sizeof($arr_item_type); $a++)
		{
			list($item_type, $item_type_name) = $arr_item_type[$a];
			
			$table_content .= "<br>";
			$table_content .= "<table border=\"0\" width=\"100%\" class=\"eSporttableborder\" style='table-layout:auto' align=\"center\" cellspacing=\"0\" cellpadding=\"3\" >";
			
			$table_content .= "<col width=\"95px\">\n";
			$table_content .= "<col width=\"95px\">\n";
			$table_content .= "<col width=\"80px\">\n";
			$table_content .= "<col width=\"60px\">\n";
			$table_content .= "<col width=\"50px\">\n";
			$table_content .= "<col width=\"50px\">\n";
			$table_content .= "<col width=\"50px\">\n";
			$table_content .= "<col width=\"50px\">\n";
			if ($DisplayPurchasePrice)
				$table_content .= "<col width=\"50px\">\n";
			if ($DisplayLocation)
				$table_content .= "<col width=\"80px\">\n";
			if ($DisplayWriteOffInfo)
				$table_content .= "<col width=\"80px\">\n";
			if ($DisplaySignature)	
				$table_content .= "<col width=\"80px\">\n";
			if ($DisplayRemark)
				$table_content .= "<col width=\"80px\">\n";
				
			$table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" bgcolor=\"#FFFFFF\" colspan=\"14\">".$i_InventorySystem['item_type']." : ".$item_type_name."</td></tr>";
			$table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" rowspan=\"2\">$i_InventorySystem_Item_Code</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Item."</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Description."</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Purchase_Date."</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Sch_Unit_Price."</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Quantity."</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"3\" align=\"center\">".$i_InventorySystem_Report_Col_Cost."</td>";
			if ($DisplayPurchasePrice)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."</td>";
			if ($DisplayLocation)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Location."</td>";
			if ($DisplayWriteOffInfo)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off."</td>";
			if ($DisplaySignature)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Signature."</td>";
			if ($DisplayRemark)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Remarks."</td></tr>";
			$table_content .= "<tr>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['School']."</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['Government']."</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['SponsoringBody']."</td>
								</tr>";
									
			if(!$display_writeoff)
			{
				$wo_con = " and a.RecordStatus=1 ";
				$wo_conb = " and c.Quantity>0";
			}
		
			$sql = "SELECT 
						DISTINCT a.ItemID,
						a.ItemType,
						a.ItemCode,
						".$linventory->getInventoryNameByLang("a.").",
						".$linventory->getInventoryDescriptionNameByLang("a.")."
					FROM
						INVENTORY_ITEM AS a LEFT OUTER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
						INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID)
						left join INVENTORY_ITEM_BULK_LOG as e on e.ItemID=a.ItemID
					WHERE
						a.ItemType IN ($item_type)
						$wo_con 
						$sql_purchase_period
					ORDER BY
						a.ItemCode
					";
			$arr_result = $linventory->returnArray($sql,5);
			
			if(sizeof($arr_result)>0)
			{
				for($i=0; $i<sizeof($arr_result); $i++)
				{
					list($item_id, $item_type, $item_code, $item_name, $item_description) = $arr_result[$i];
					
					if($item_type == ITEM_TYPE_SINGLE)
					{
						$sql = "SELECT
										a.PurchaseDate, 
										if(a.PurchasedPrice != '',a.PurchasedPrice,'0'),
										if(a.UnitPrice != '',a.UnitPrice,'0'),
										a.LocationID,
										CONCAT(".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
										d.FundingType,
										".$linventory->getInventoryNameByLang("d.").",
										e.ApproveDate,
										e.WriteOffReason,
										a.ItemRemark
								FROM
										INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
										INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
										INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
										INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
										INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
								WHERE
										a.ItemID = $item_id
								";
						$arr_sub_result = $linventory->returnArray($sql,8);
						
						$table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
						$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
						if($item_description != "")
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_description</td>";
						else
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						
						if(sizeof($arr_sub_result)>0)
						{
							for($j=0; $j<sizeof($arr_sub_result); $j++)
							{
								list($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark) = $arr_sub_result[$j];
								$quantity = 1;
							
								if ($purchase_price > $unit_price * $quantity & $unit_price>0)
									$purchase_price = $unit_price * $quantity;
								
								if ($purchase_date=="0000-00-00")
									$purchase_date = "--";
									
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$purchase_date."</td>";
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($unit_price,2)."</td>";
								
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>".(($quantity!="")?$quantity:'&nbsp')."</td>";
								
								if($ShowFundingName){
									$str_funding_name = "<br />(".$funding_name.")";
								} 
								$col_fund_align = " align='right' ";
								# no need to funding name if cost = 0
								if ($purchase_price<=0)
								{
									$str_funding_name = "";
								}
								
								$school_funding = "0.00";
								$gov_funding = "0.00";
								$sponsor_funding = "0.00";
								if($funding_type == ITEM_SCHOOL_FUNDING)
								{
									$school_funding = number_format($purchase_price,2).$str_funding_name;
								}
								if($funding_type == ITEM_GOVERNMENT_FUNDING)
								{
									$gov_funding = number_format($purchase_price,2).$str_funding_name;
								}
								if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
								{
									$sponsor_funding = number_format($purchase_price,2).$str_funding_name;
								}
								
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>". $school_funding."</td>";
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>". $gov_funding."</td>";
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>". $sponsor_funding."</td>";
								
								if ($DisplayPurchasePrice)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($purchase_price,2)."</td>";
								
								if ($DisplayLocation)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($location_name!="")?intranet_htmlspecialchars($location_name):'&nbsp')."</td>";
								if ($DisplayWriteOffInfo)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($write_off_date!="")?$write_off_date:'&nbsp').(($write_off_reason)?"(".$write_off_reason.")":'&nbsp')."</td>";
								if ($DisplaySignature)	
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($signature!="")?$signature:'&nbsp')."</td>";
								if ($DisplayRemark)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($remark!="")?$remark:'&nbsp')."</td>";	
								$table_content .= "</tr>";
							}
						}
					}
					if($item_type == ITEM_TYPE_BULK)
					{
						$final_purchase_date = "";
						
						if(!$display_writeoff)
						{
							$wo_conb = " and b.Quantity > 0 ";	
						}
						
						$sql = "SELECT
										distinct(IF(a.PurchaseDate IS NULL, '', a.PurchaseDate))
								FROM
										INVENTORY_ITEM_BULK_LOG as a
										left join INVENTORY_ITEM_BULK_LOCATION as b on (b.ItemID=a.ItemID)
								WHERE
										a.ItemID = $item_id AND
										a.Action = 1 
										$wo_conb
								order by PurchaseDate
								";		
						$arr_purchase_date = $linventory->returnArray($sql);
						
						if(sizeof($arr_purchase_date)>0)
						{
							$table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
							$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
							if($item_description != "")
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_description</td>";
							else
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						
							$final_purchase_date .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<sizeof($arr_purchase_date); $j++)
							{
								list($purchase_date) = $arr_purchase_date[$j];
								if($purchase_date != '')
								{
									/*
									if($j!=0){
										$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
										$final_purchase_date .= "<tr height=\"5px\"><td></td></tr>";
									}else{
										$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
									}
									*/
									$final_purchase_date .= "<tr><td class=\"eSportprinttext\" nowrap>$purchase_date</td></tr>";
								}else{
									//$final_purchase_date = '<tr><td class=\"eSportprinttext\"> - </td></tr>';
								}
							}
							$final_purchase_date .= "</table>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" vlaign=\"top\">$final_purchase_date</td>";
						}
						else
						{
							//$table_content.="<td class=\"eSporttdborder eSportprinttext\"> - </td>";
							continue;
						}
						
						### GET COST, UNIT PRICE, PURCHASED PRICE ###
						$total_school_cost = 0;
						$total_gov_cost = 0;
						$final_unit_price = 0;
						$final_purchase_price = 0;
						$total_qty = 0;
						$tmp_unit_price = 0;
						$tmp_purchase_price = 0;
						$written_off_qty = 0;
						$location_qty = 0;
						$ini_total_qty = 0;
						
						## get Total purchase price ##
						//$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
						$sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($total_purchase_price) = $tmp_result[0];
						}
						## get Total Qty Of the target Item ##
						$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($totalQty) = $tmp_result[0];
						}
						## get written-off qty ##
						$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = $item_id";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($written_off_qty) = $tmp_result[0];
						}
						
						## get the qty in the target location ##
						/* no use in this type of report
						$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($location_qty) = $tmp_result[0];
						}
						*/
						/*
						$sql = "SELECT 
										a.FundingSource, 
										b.FundingType,
										".$linventory->getInventoryNameByLang("b.")."
								FROM 
										INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
										INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
								WHERE 
										a.ItemID = $item_id AND
										a.Action IN (1,7) AND
										a.FundingSource IS NOT NULL
								";
						$tmp_result = $linventory->returnArray($sql);
						if(sizeof($tmp_result)>0){
							list($funding_source, $funding_type,$funding_name)=$tmp_result[0];
						}
						*/
						
						### Get Qty By Location ###
						$sql = "SELECT 
										a.Quantity,
										a.LocationID,
										CONCAT(".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
										f.FundingType,
										". $linventory->getInventoryNameByLang("f.") ." as funding_name,
										a.FundingSourceID
								FROM
										INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
										INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
										INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
										inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
								WHERE
										a.ItemID = $item_id 
								";
						$arr_tmp_qty = $linventory->returnArray($sql);

						if(sizeof($arr_tmp_qty)>0)
						{
							$display_qty = 0;
							$display_location_name = "";
							$display_location_name .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<sizeof($arr_tmp_qty); $j++)
							{
								list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
								if($qty != "")
								{
									$display_qty = $display_qty+$qty;
								}
								if($location_name != "")
								{
									if($j!=0){
										$display_location_name .= "<tr height=\"5px\"><td></td></tr>";
										$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
									}else{
										$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
									}									
								}
							}
							$display_location_name .= "</table>";		
						}
						
						if ($ShowFundingName){
								$str_funding_name = "<br />(".$funding_name.")";
							}
								$str_col_fund = " align='right' ";
							$final_purchase_price = $total_purchase_price;
							$ini_total_qty = $totalQty + $written_off_qty;
							if ($ini_total_qty>0)
								$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
							//$total_fund_cost = $ini_total_qty*($total_purchase_price/$ini_total_qty);
							$total_fund_cost = $final_unit_price * $display_qty;
							
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($final_unit_price,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($display_qty)."</td>";
							
							# no need to funding name if cost = 0
							if ($total_fund_cost<=0)
							{
								$str_funding_name = "";
							}
							
							# retrieve funding data
							$tmp_funding_src = array();
							for($j=0; $j<sizeof($arr_tmp_qty); $j++)
							{
								list($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
								
								$tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
								$tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
								$tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
							}
							for($type_i=1;$type_i<=3;$type_i++)
							{
								$this_field = "";
								if(!empty($tmp_funding_src[$type_i]))
								{
									$this_fund_cost = 0;
									
									foreach($tmp_funding_src[$type_i] as $k=>$d)
									{
										if($ShowFundingName)
										{
											$this_fund_cost = number_format($d['cost'],2);
											$this_fund_src = ($d['cost']>0) ? "<br />(". $d['src_name'].")" : "";
											$this_field .= ($this_field ? "<br><br>" : "") . $this_fund_cost . $this_fund_src;
										}
										else
										{
											$this_fund_cost += $d['cost'];
											$this_field = number_format($this_fund_cost,2); 
										}
									}
								}
								$this_field = $this_field ? $this_field : "0.00";
								
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
							}
							
							/*
							if($funding_type == ITEM_SCHOOL_FUNDING)
							{	
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_gov_cost,2)."</td>";
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_fund_cost,2).$str_funding_name."</td>";
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{				
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_fund_cost,2).$str_funding_name."</td>";
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_school_cost,2)."</td>";
							}
							*/
							/*
						if($funding_type == ITEM_SCHOOL_FUNDING)
						{	
							$final_purchase_price = $total_purchase_price;
							$ini_total_qty = $totalQty + $written_off_qty;
							if ($ini_total_qty>0)
								$final_unit_price = $total_purchase_price/$ini_total_qty;
							//$total_school_cost = $ini_total_qty*($total_purchase_price/$ini_total_qty);
							$total_school_cost = $final_unit_price*$display_qty;
							$final_purchase_price = $total_school_cost;
													
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($total_gov_cost,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($total_school_cost,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($final_unit_price,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($final_purchase_price,2)."</td>";
						}
						if($funding_type == ITEM_GOVERNMENT_FUNDING)
						{
							$final_purchase_price = $total_purchase_price;
							$ini_total_qty = $totalQty + $written_off_qty;
							if ($ini_total_qty>0)
								$final_unit_price = $total_purchase_price/$ini_total_qty;
							//$total_gov_cost = $ini_total_qty*($total_purchase_price/$ini_total_qty);
							$total_gov_cost = $final_unit_price*$display_qty;
							$final_purchase_price = $total_gov_cost;
							
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($total_gov_cost,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($total_school_cost,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($final_unit_price,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">".round($final_purchase_price,2)."</td>";
						}
						*/
					
						if ($DisplayPurchasePrice)
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($total_fund_cost,2)."</td>";
						if ($DisplayLocation)
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_location_name</td>";		
								
							/*
						if(sizeof($arr_tmp_qty) == 0)
						{
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">-</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">-</td>";
						} else
						{
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">$display_qty</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_location_name</td>";	
						}
						*/
						
						if ($DisplayWriteOffInfo)
						{
							### Get Write-Off Date & Reason ###
							$display_write_off = "";
							$sql = "SELECT
											ApproveDate,
											WriteOffQty,
											WriteOffReason
									FROM
											INVENTORY_ITEM_WRITE_OFF_RECORD
									WHERE
											ItemID = $item_id 
									";
									
							$arr_write_off = $linventory->returnArray($sql,2);
							
							if(sizeof($arr_write_off)>0)
							{
								$display_write_off .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
								
								for($j=0; $j<sizeof($arr_write_off); $j++)
								{
									list($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
									if($write_off_date != "")
									{
										$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
										$tmp_write_off = $write_off_date."<BR>".$write_off_qty."<BR>".$write_off_reason;
									
										if($j!=0){
											$display_write_off .= "<tr height=\"5px\"><td></td></tr>";
											$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
										}else{
											$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
										}
									}
								}
								$display_write_off .= "</table>";
								
								if($display_write_off != "")
									$table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_write_off</td>";
								else
									$table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
							}
							else
							{
								$table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
							}
						}
						
						### Empty Space for Signature and remark ###
						if ($DisplaySignature)
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						if ($DisplayRemark)	
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
					}
				}
			}
			if(sizeof($arr_result)==0)
			{
				$table_content .= "<tr><td colspan=\"14\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>";
			}
			$table_content .= "</table>";
			$table_content .= "<br>";
		}
	}
}
else if($groupBy==3)
{
	$group_namefield = $linventory->getInventoryNameByLang();

	$sql = "SELECT AdminGroupID, $group_namefield as GroupName FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
	$group_array = $linventory->returnArray($sql,2);
		
	if (is_array($group_array))
	{
		foreach($group_array as $Key=>$Value)
		{
			if($_POST['group_cb'.$Value['AdminGroupID']])
			{
				$selected_cb .= $Value['AdminGroupID'].",";
			}
		}
	}

	$selected_cb = substr($selected_cb,0,-1);

	$cond = "WHERE AdminGroupID in (".$selected_cb.")";
	$sql="Select DISTINCT(".$linventory->getInventoryItemNameByLang().") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
		$arr_group = $linventory->returnArray($sql,2);
		
	for($a=0; $a<sizeof($arr_group); $a++)
	{	
		list($group_name,$admin_group_id) = $arr_group[$a];
		
		$group_table_content .= "<br>";
		$group_table_content .= "<table border=\"0\" width=\"100%\" class=\"eSporttableborder\" style='table-layout:auto' align=\"center\" cellspacing=\"0\" cellpadding=\"3\">";
		
		$group_table_content .= "<col width=\"95px\">\n";
		$group_table_content .= "<col width=\"95px\">\n";
		$group_table_content .= "<col width=\"80px\">\n";
		$group_table_content .= "<col width=\"60px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		if ($DisplayPurchasePrice)
			$group_table_content .= "<col width=\"50px\">\n";
		if ($DisplayLocation)
			$group_table_content .= "<col width=\"80px\">\n";
		if ($DisplayWriteOffInfo)
			$group_table_content .= "<col width=\"80px\">\n";
		if ($DisplaySignature)
			$group_table_content .= "<col width=\"80px\">\n";
		if ($DisplayRemark)
			$group_table_content .= "<col width=\"80px\">\n";
			
		$group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" bgcolor=\"#FFFFFF\" colspan=\"14\">".$i_InventorySystem['Caretaker']." : ".intranet_htmlspecialchars($group_name)."</td></tr>";
		$group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" rowspan=\"2\">$i_InventorySystem_Item_Code</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Item."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Description."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Purchase_Date."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Sch_Unit_Price."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Quantity."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"3\" align=\"center\">".$i_InventorySystem_Report_Col_Cost."</td>";
		if ($DisplayPurchasePrice)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."</td>";
		if ($DisplayLocation)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Location."</td>";
		if ($DisplayWriteOffInfo)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off."</td>";
		if ($DisplaySignature)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Signature."</td>";
		if ($DisplayRemark)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Remarks."</td></tr>";
		$group_table_content .= "<tr>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['School']."</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['Government']."</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['SponsoringBody']."</td>
								</tr>";
							
		if(!$display_writeoff)
		{
			$wo_con = " and a.RecordStatus=1 ";
			$wo_conb = " and c.Quantity>0 ";
		}
		
		$sql = "SELECT 
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					".$linventory->getInventoryNameByLang("a.").",
					".$linventory->getInventoryDescriptionNameByLang("a.")."
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID)
					left join INVENTORY_ITEM_BULK_LOG as e on e.ItemID=a.ItemID
				WHERE
					d.AdminGroupID IN ($admin_group_id)
					$wo_con
					$sql_purchase_period
				ORDER BY
					a.ItemCode
				";
		$arr_result = $linventory->returnArray($sql,5);
		
		if(sizeof($arr_result)>0)
		{
			for($i=0; $i<sizeof($arr_result); $i++)
			{
				list($item_id, $item_type, $item_code, $item_name, $item_description) = $arr_result[$i];
				
				$group_table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
				$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
				if($item_description != "")
					$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">$item_description</td>";
				else
					$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						
				if($item_type == ITEM_TYPE_SINGLE)
				{
					$sql = "SELECT
									a.PurchaseDate, 
									if(a.PurchasedPrice != '',a.PurchasedPrice,'0'),
									if(a.UnitPrice != '',a.UnitPrice,'0'),
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									d.FundingType,
									".$linventory->getInventoryNameByLang("d.").",
									e.ApproveDate,
									e.WriteOffReason,
									a.ItemRemark
							FROM
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
							WHERE
									a.ItemID = $item_id
							";
					$arr_sub_result = $linventory->returnArray($sql);
					if(sizeof($arr_sub_result)>0)
					{
						for($j=0; $j<sizeof($arr_sub_result); $j++)
						{
							list($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark) = $arr_sub_result[$j];
							$quantity = 1;
							
							if ($purchase_price > $unit_price * $quantity & $unit_price>0)
								$purchase_price = $unit_price * $quantity;
							
							if ($purchase_date=="0000-00-00")
								$purchase_date = "--";
								
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$purchase_date."</td>";
							
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($unit_price, 2)."</td>";
							
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".(($quantity!="")?$quantity:'&nbsp')."</td>";
							
							if($ShowFundingName){
								$str_funding_name = "<br />(".$funding_name.")";
							} 
							if ($purchase_price==0)
							{
								$str_funding_name = "";
							}
							
							$school_funding = "0.00";
							$gov_funding = "0.00";
							$sponsor_funding = "0.00";
							$str_col_fund = " align='right' ";

							if($funding_type == ITEM_SCHOOL_FUNDING)
							{
								$school_funding = number_format($purchase_price,2).$str_funding_name;
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{
								$gov_funding = number_format($purchase_price,2).$str_funding_name;
							}
							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
							{
								$sponsor_funding = number_format($purchase_price,2).$str_funding_name;
							}
							
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $school_funding ."</td>";
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $gov_funding ."</td>";
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $sponsor_funding ."</td>";
							if ($DisplayPurchasePrice)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($purchase_price,2)."</td>";
							if ($DisplayLocation)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($location_name!="")?intranet_htmlspecialchars($location_name):'&nbsp')."</td>";
							if ($DisplayWriteOffInfo)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($write_off_date!="")?$write_off_date:'&nbsp').(($write_off_reason)?"(".$write_off_reason.")":'&nbsp')."</td>";
							if ($DisplaySignature)			
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($signature!="")?$signature:'&nbsp')."</td>";
							if ($DisplayRemark)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($remark!="")?$remark:'&nbsp')."</td>";
							$group_table_content .= "</tr>";
						}
					}
				}
				if($item_type == ITEM_TYPE_BULK)
				{
					$final_purchase_date = "";
					
					### GET PURCHASE DATE ###
					$sql = "SELECT
									distinct(IF(a.PurchaseDate IS NULL, '', a.PurchaseDate))
							FROM
									INVENTORY_ITEM_BULK_LOG AS a 
							WHERE
									a.ItemID = $item_id AND
									a.Action = 1 AND
									a.GroupInCharge = $admin_group_id
							order by PurchaseDate
							";
					
					$arr_purchase_date = $linventory->returnArray($sql);
					if(sizeof($arr_purchase_date)>0)
					{
						$final_purchase_date .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						for($j=0; $j<sizeof($arr_purchase_date); $j++)
						{
							list($purchase_date) = $arr_purchase_date[$j];
							if($purchase_date != '')
							{
								$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
							}else{
								$final_purchase_date = '<tr><td class=\"eSportprinttext\"> - </td></tr>';
							}
						}
						$final_purchase_date .= "</table>";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$final_purchase_date</td>";
					}
					else
					{
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\"> - </td>";
						//continue;
					}
					
					### GET COST, UNIT PRICE, PURCHASED PRICE ###
					$total_school_cost = 0;
					$total_gov_cost = 0;
					$final_unit_price = 0;
					$final_purchase_price = 0;
					$total_qty = 0;
					$tmp_unit_price = 0;
					$tmp_purchase_price = 0;
					$written_off_qty = 0;
					$location_qty = 0;
					$ini_total_qty = 0;
					
					## get Total purchase price ##
					//$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					$sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($total_purchase_price) = $tmp_result[0];
					}
					
					## get Total Qty Of the target Item ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($totalQty) = $tmp_result[0];
					}
					if(!$display_writeoff && $totalQty==0)	continue;
					
					## get written-off qty ##
					$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = $item_id";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($written_off_qty) = $tmp_result[0];
					}
					
					## get the qty in the target location ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND GroupInCharge = $admin_group_id";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($location_qty) = $tmp_result[0];
					}
					
					## Location origial qty (by location only)##
					$sql = "SELECT WriteOffQty FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = $item_id AND AdminGroupID = $admin_group_id";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$ini_location_qty = $tmp_result[0] + $location_qty;
					}else{
						$ini_location_qty = $location_qty;
					}
					/*
					$sql = "SELECT 
									a.FundingSource, 
									b.FundingType,
									".$linventory->getInventoryNameByLang("b.")."
							FROM 
									INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
									INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
							WHERE 
									a.ItemID = $item_id AND
									a.Action IN (1,7) AND
									a.GroupInCharge = $admin_group_id AND
									a.FundingSource IS NOT NULL
							";
					$tmp_result = $linventory->returnArray($sql);
					if(sizeof($tmp_result)>0){
						list($funding_source, $funding_type,$funding_name)=$tmp_result[0];
					}
					*/
					$ini_total_qty = $totalQty + $written_off_qty;
					if ($ini_total_qty>0)
						$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
					//$total_funding_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
					
					$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($final_unit_price,2)."</td>";
					
					if($ShowFundingName){
						$str_funding_name = "<br / >(".$funding_name.")";
					} else
					{
						$str_col_fund = " align='right' ";
					}
					
					if ($total_funding_cost<=0)
					{
						$str_funding_name = "";
					}
					
					### Get Qty By Location ###
					$sql = "SELECT 
									a.Quantity,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									f.FundingType,
									". $linventory->getInventoryNameByLang("f.") ." as funding_name,
									a.FundingSourceID
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
									inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
							WHERE
									a.ItemID = $item_id AND 
									a.GroupInCharge = $admin_group_id
							";
					$arr_tmp_qty = $linventory->returnArray($sql);
					if(sizeof($arr_tmp_qty)>0)
					{
						$display_qty = 0;
						$qty = 0;
						$display_location_name = "";
						
						$display_location_name .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						for($j=0; $j<sizeof($arr_tmp_qty); $j++)
						{
							list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
							if($qty != "")
							{
								$display_qty = $display_qty + $qty;
							}
							if($location_name != "")
							{
								if($j!=0){
									$display_location_name .= "<tr height=\"5px\"><td></td></tr>";
									$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
								}else{
									$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
								}
							}
						}
						$display_location_name .= "</table>";
					}
					
					if(sizeof($arr_tmp_qty)==0)
					{
						$display_location_name = "&nbsp;";
					}
					
					$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($display_qty)."</td>";
					
					$total_funding_cost = $display_qty*$final_unit_price;
					$final_purchase_price = $total_funding_cost;
					
					# retrieve funding data
					$tmp_funding_src = array();
					for($j=0; $j<sizeof($arr_tmp_qty); $j++)
					{
						list($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
						
						$tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
						$tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
						$tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
					}
					$str_col_fund = " align='right' ";
					for($type_i=1;$type_i<=3;$type_i++)
					{
						$this_field = "";
						if(!empty($tmp_funding_src[$type_i]))
						{
							$this_fund_cost = 0;
							
							foreach($tmp_funding_src[$type_i] as $k=>$d)
							{
								if($ShowFundingName)
								{
									$this_fund_cost = number_format($d['cost'],2);
									$this_fund_src = ($d['cost']>0) ? "<br />(". $d['src_name'].")" : "";
									$this_field .= ($this_field ? "<br><br>" : "") . $this_fund_cost . $this_fund_src;
								}
								else
								{
									$this_fund_cost += $d['cost'];
									$this_field = number_format($this_fund_cost,2); 
								}
							}
						}
						$this_field = $this_field ? $this_field : "0.00";
						
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
					}
					
					/*
					if($funding_type == ITEM_SCHOOL_FUNDING)
					{						
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_gov_cost,2)."</td>";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
					}
					if($funding_type == ITEM_GOVERNMENT_FUNDING)
					{
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_school_cost,2)."</td>";
					}
					*/
					
					if ($DisplayPurchasePrice)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($final_purchase_price,2)."</td>";
					if ($DisplayLocation)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_location_name</td>";
						
					if ($DisplayWriteOffInfo)
					{
						### Get Write-Off Date & Reason ###
						$display_write_off = "";
						$sql = "SELECT
										ApproveDate,
										WriteOffQty,
										WriteOffReason
								FROM
										INVENTORY_ITEM_WRITE_OFF_RECORD
								WHERE
										ItemID = $item_id AND 
										AdminGroupID = $admin_group_id
								";
						
						$arr_write_off = $linventory->returnArray($sql,2);
						if(sizeof($arr_write_off)>0)
						{
							$display_write_off .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<sizeof($arr_write_off); $j++)
							{
								list($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
								if($write_off_date != "")
								{
									$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
									$tmp_write_off = $write_off_date."<BR>".$write_off_qty."<BR>".$write_off_reason;
									
									if($j!=0){
										$display_write_off .= "<tr height=\"5px\"><td></td></tr>";
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}else{
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}
								}
							}
							$display_write_off .= "</table>";
	
							if($display_write_off != "")
								$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_write_off</td>";
							else
								$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
						else
						{
							$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
					}
					### Empty Space for Signature and remark ###
					if ($DisplaySignature)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
					if ($DisplayRemark)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
				}
			}
		}
		if(sizeof($arr_result)==0)
		{
			$group_table_content .= "<tr><td colspan=\"14\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>";
		}
		$group_table_content .= "</table>";
		$group_table_content .= "<br>";
	}
}
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>
<br>
<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<br>
<table width="90%" border="0" align="center" >
	<tr><td class="eSportprinttext" align="center"><font size="5"><?=$i_InventorySystem_Report_FixedAssetsRegister?></font></td></tr>
	<tr><td class="eSportprinttext" align="center"><font size="5"><?=$school_name?></font><?=$badge_image?></td></tr>
</table>
<br> 
<!--
<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center">
<?=$report_header;?>
</table>
-->

<? if($groupBy==1) { ?>
<?=$location_table_content;?>
<? } ?>

<? if($groupBy==2) { ?>
<?=$table_content;?>
<? } ?>

<? if($groupBy==3) { ?>
<?=$group_table_content;?>
<? } ?>

<br>
<?
intranet_closedb();
?>