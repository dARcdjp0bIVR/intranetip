<?php
// using: Henry
// ####################################
//
// with $sys_custom['CatholicEducation_eInventory'] or $sys_custom['StocktakeListReport']
//
// ####################################
// Date:   2018-02-12  Isaac
// Fixed sql incorrect usage of UNION and ORDER BY
//
// Date:   2018-10-10  Isaac
// Added ORDER BY a.ItemCode to sql for export to consistent with stock take
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2014-12-19 Henry
// Fixed: items show incorrect quantity at Fixed Asset Stocktake list [Case#F73099]
// Deploy: ip.2.5.6.1.1
//
// Date: 2014-10-07 YatWoon
// Fixed: hide non-stocktake item in the report [Case#J68923]
// Deploy: ip.2.5.5.10.1
//
// ####################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "templates/" . $LAYOUT_SKIN . "/layout/print_header_no25css.php");

if (! sizeof($_POST)) {
    header("Location: index.php");
}
intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// Retreive item
$data_ary = array();
foreach ($targetLocation as $k => $this_LocationID) {
    // Location Name
    $thisRoom = new Room($this_LocationID);
    $thisLocationName = $thisRoom->RoomName;
    
    // Stocktake period
    $StocktakePeriodStart = $linventory->getStocktakePeriodStart();
    $StocktakePeriodEnd = $linventory->getStocktakePeriodEnd();
    $StocktakePeriod = $StocktakePeriodStart . " " . $Lang['General']['To'] . " " . $StocktakePeriodEnd;
    
    // #### Single item
    $sql1 = "SELECT
					a.ItemCode,
					" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName,
					1 as Qty, 
					b.UnitPrice as unit_price, 
					a.StockTakeOption as StockTakeOption
			FROM 
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) 
			WHERE
					a.ItemType = 1 AND
					a.RecordStatus = 1 AND
					b.LocationID IN ($this_LocationID) and 
					(a.StockTakeOption='YES' OR (a.StockTakeOption='FOLLOW_SETTING' and b.UnitPrice>=" . $linventory->StockTakePriceSingle . "))
			";
    // #### Bulk item
    $sql2 = "SELECT
					a.ItemCode,
					" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName,
					b.Quantity as Qty, 
					avg(c.UnitPrice) as unit_price, 
					a.StockTakeOption as StockTakeOption
			FROM
					INVENTORY_ITEM AS a 
					INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) 
					LEFT JOIN INVENTORY_ITEM_BULK_LOG as c on (c.ItemID=a.ItemID and c.Action=1)
			WHERE
					a.ItemType = 2 AND
					a.RecordStatus = 1 AND
					b.LocationID IN ($this_LocationID)
			group by 
				b.ItemID 
			having
				StockTakeOption ='YES' or (StockTakeOption='FOLLOW_SETTING' and Qty*unit_price >=" . $linventory->StockTakePriceBulk . ")
			";
    $sql = $sql1 . " union " . $sql2 . " ORDER BY ItemCode";
    $arr_result = $linventory->returnArray($sql);
    
    // #####################
    // Report Title
    // #####################
    $table .= "<font size='5'>" . $Lang['eInventory']['Report']['StocktakeList'] . "</font>";
    $table .= "<table border='0' width='100%'>";
    
    // #####################
    // records list table
    // #####################
    $table .= "<tr><td>";
    $table .= "<table border='0' width='100%' cellpadding='02' cellspacing='0' class='eSporttableborder'>";
    $table .= "<thead>";
    $table .= "<tr>";
    $table .= "<td colspan='6' class='eSporttdborder eSportprinttext'>";
    $table .= "<div align='right'>" . $Lang['eInventory']['Report']['StocktakePeriod'] . ": " . $StocktakePeriod . "</div>";
    $table .= "<div align='center'>" . $thisLocationName . "</div>";
    $table .= "</td>";
    $table .= "</tr>";
    $table .= "<tr>";
    $table .= "<td width='10%' class='eSporttdborder eSportprinttext'>" . $i_InventorySystem_Item_Code . "</td>";
    $table .= "<td width='10%' class='eSporttdborder eSportprinttext'>" . $i_InventorySystem_Item_Name . "</td>";
    $table .= "<td width='5%' class='eSporttdborder eSportprinttext' align='center'>" . $i_InventorySystem_Item_Qty . "</td>";
    $table .= "<td width='5%' class='eSporttdborder eSportprinttext' align='center'>" . $i_InventorySystem_ItemStatus_Normal . "</td>";
    $table .= "<td width='5%' class='eSporttdborder eSportprinttext' align='center'>" . $Lang['eInventory']['ItemStatus_Damaged'] . "</td>";
    $table .= "<td width='100%' class='eSporttdborder eSportprinttext'>" . $i_InventorySystem_Item_Remark . "</td>";
    $table .= "</tr>";
    $table .= "</thead>";
    
    if (sizeof($arr_result) > 0) {
        foreach ($arr_result as $k1 => $data) {
            // #####################
            // table content
            // #####################
            $table .= "<tr style='page-break-inside:avoid;'>";
            $table .= "<td class='eSporttdborder eSportprinttext' nowrap>" . $data['ItemCode'] . "</td>";
            $table .= "<td class='eSporttdborder eSportprinttext'>" . $data['ItemName'] . "</td>";
            $table .= "<td class='eSporttdborder eSportprinttext' align='center'>" . $data['Qty'] . "</td>";
            $table .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
            $table .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
            $table .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
            $table .= "</tr>";
        }
    } else {
        $table .= "<tr>";
        $table .= "<td colspan='6' class='eSporttdborder eSportprinttext' >" . $Lang['General']['NoRecordFound'] . "</td>";
        $table .= "</tr>";
    }
    $table .= "</table>";
    $table .= "</td></tr>";
    
    // #####################
    // Signature
    // #####################
    $table .= "<tr><td style='page-break-inside:avoid;'>";
    $table .= "<br><br><table border='0' width='100%' cellpadding='10' cellspacing='0'>";
    $table .= "<tr><td colspan='6'><br><br>";
    $table .= $Lang['eInventory']['SignatureSubmittedBy'] . " : ____________________<br><br>";
    $table .= $Lang['eInventory']['SignatureCheckedBy'] . " : ____________________<br><br>";
    $table .= $Lang['eInventory']['SignatureAudit'] . " : ____________________<br><br>";
    $table .= "</td></tr>";
    $table .= "</table>";
    $table .= "</td></tr>";
    
    $table .= "</table>";
    $table .= "<div style='page-break-after:always'>&nbsp;</div>";
}
?>
<?
 
/*
    * ?>
    * <style type="text/css">
    * @media print
    * {
    * .table { page-break-after:auto }
    * .tr { page-break-inside:avoid; page-break-after:auto }
    * .td { page-break-inside:avoid; page-break-after:auto }
    * .thead { display:table-header-group }
    * .tfoot { display:table-footer-group }
    * }
    * </style>
    * <?
    */
?>

<table width="98%" border="0" cellpadding="0" cellspacing="0"
	align="center" class='print_hide'>
	<tr>
		<td align="right">
			<?= $linterface->GET_BTN($button_print, "submit", "window.print(); return false;"); ?>
		</td>
	</tr>
</table>

<?=$table?>


<?
intranet_closedb();
?>