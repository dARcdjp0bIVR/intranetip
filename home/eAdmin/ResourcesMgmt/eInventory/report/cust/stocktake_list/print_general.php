<?php
// using: Henry
// ####################################
//
// with no $sys_custom
//
// ####################################
// Date:   2018-10-10  Isaac
// Added ORDER BY a.ItemCode to sql for export to consistent with stock take
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2015-05-12 Henry
// File created
// ####################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "templates/" . $LAYOUT_SKIN . "/layout/print_header_no25css.php");
// include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

if (! sizeof($_POST)) {
    header("Location: index.php");
}
intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// Retreive item
$with_data = 0;

$StockTakePriceSingle = $linventory->getStockTakePriceSingle();
$StockTakePriceBulk = $linventory->getStockTakePriceBulk();

foreach ($targetLocation as $k => $this_LocationID) {
    // Location Name
    $thisRoom = new Room($this_LocationID);
    $thisLocationName = $thisRoom->RoomName;
    
    for ($this_item_type = 1; $this_item_type <= 2; $this_item_type ++) {
        $thisLocationResult = array();
        $this_location_group = array();
        if (empty($targetGroup)) {
            $this_location_group[0] = - 1;
        } else {
            $this_location_group = $targetGroup;
        }
        // Loop with Resource ManagementGroup
        foreach ($this_location_group as $k2 => $this_GroupID) {
            if ($this_GroupID != - 1) {
                $thisGroupName = $linventory->returnGroupNameByGroupID($this_GroupID);
                $sql_cond = "AND b.GroupInCharge = '$this_GroupID' ";
            }
            if ($this_item_type == 1) {
                // #### Single item
                $single_funding = "if(b.FundingSource2 is NULL or b.FundingSource2 ='', f.code, concat(f.code,', ',f2.code))";
                $sql = "SELECT
								a.ItemCode, 
								$single_funding as FundingCode, 
								" . $linventory->getInventoryNameByLang("d2.") . " as SubCatName,
								" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName, 
								" . $linventory->getInventoryDescriptionNameByLang("a.") . " as ItemDescription, 
								1 as Qty
						FROM 
								INVENTORY_ITEM AS a 
								INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON a.ItemID = b.ItemID 
								INNER JOIN INVENTORY_FUNDING_SOURCE AS f on f.FundingSourceID = b.FundingSource 
								LEFT JOIN INVENTORY_FUNDING_SOURCE AS f2 ON b.FundingSource2 = f2.FundingSourceID 
								INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID 
						WHERE
								a.ItemType = 1 AND
								a.RecordStatus = 1 AND
								b.LocationID = '$this_LocationID' 
								$sql_cond and 
								(a.StockTakeOption='YES' OR (a.StockTakeOption='FOLLOW_SETTING' and b.UnitPrice>=" . $StockTakePriceSingle . ")) 
						ORDER BY
					            a.ItemCode";
            } else {
                // #### Bulk item
                $sql = "SELECT
							a.ItemCode, 
							c.code as FundingCode, 
							" . $linventory->getInventoryNameByLang("d2.") . " as SubCatName,
							" . $linventory->getInventoryItemNameByLang("a.") . " as ItemName, 
							" . $linventory->getInventoryDescriptionNameByLang("a.") . " as ItemDescription, 
							b.Quantity as Qty, 
							avg(d.UnitPrice) as unit_price, 
							a.StockTakeOption as StockTakeOption
					FROM
							INVENTORY_ITEM AS a 
							INNER JOIN INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID AND b.Quantity != 0) 
							INNER JOIN INVENTORY_FUNDING_SOURCE as c on c.FundingSourceID = b.FundingSourceID 
							INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID  
							LEFT JOIN INVENTORY_ITEM_BULK_LOG as d on (d.ItemID=a.ItemID and d.Action=1)
					WHERE
							a.ItemType = 2 AND
							a.RecordStatus = 1 AND
							b.LocationID ='$this_LocationID'  
							$sql_cond
					group by 
						b.ItemID, b.FundingSourceID  
					having
						StockTakeOption ='YES' or (StockTakeOption='FOLLOW_SETTING' and Qty*unit_price >=" . $StockTakePriceBulk . ")
					ORDER BY
					    a.ItemCode";
            }
            // $sql = $sql1 . " union " . $sql2 ." ORDER BY ItemCode";
            $arr_result = $linventory->returnArray($sql);
            
            if (empty($arr_result))
                continue;
            $with_data = 1;
            
            // #####################
            // Report Title
            // #####################
            $table .= "<table border='0' width='100%'>";
            $table .= "<tr>";
            $table .= "<td align='center' colspan='3'><font size=3><b>" . GET_SCHOOL_NAME() . "</b></font></td>";
            $table .= "</tr>";
            $table .= "<tr>";
            $table .= "<td width='33%'>" . ($this_item_type == 1 ? $i_InventorySystem_ItemType_Single : $i_InventorySystem_ItemType_Bulk) . "</td>";
            $table .= "<td width='34%' align='center'>" . $Lang['eInventory']['FixedAssetsStocktakeLocation'] . ": " . $thisLocationName . "</td>";
            $table .= "<td width='33%'>&nbsp;</td>";
            $table .= "</tr>";
            if ($this_GroupID != - 1) {
                $table .= "<tr>";
                $table .= "<td colspan='3' align='right'>" . $thisGroupName . "</td>";
                $table .= "</tr>";
            }
            
            $table .= "</table>";
            
            // #####################
            // records list table
            // #####################
            
            $table .= "<table border='0' width='100%' cellpadding='02' cellspacing='0' class='eSporttableborder'>";
            if ($this_item_type == 1) {
                $table .= "<tr>";
                $table .= "<td width='17.3%' align='center' class='eSporttdborder' rowspan='2'><b>" . $i_InventorySystem_Item_Code . "</b></td>";
                $table .= "<td width='14%' align='center' class='eSporttdborder' rowspan='2'><b>" . $i_InventorySystem_Item_Name . "</b></td>";
                $table .= "<td width='20%' align='center' class='eSporttdborder' rowspan='2'><b>" . $Lang['eInventory']['ItemDetails'] . "</b></td>";
                $table .= "<td width='9.3%' align='center' class='eSporttdborder' rowspan='2'><b>" . $i_InventorySystem_StockTakeResult . "<br>" . $Lang['eInventory']['DataCorrect'] . "</b></td>";
                $table .= "<td width='23.4%' align='center' class='eSporttdborder' colspan='2'><b>" . $i_InventorySystem_StockTakeResult . ": " . $Lang['eInventory']['DataInCorrect'] . "</b></td>";
                $table .= "<td width='16%' align='center' class='eSporttdborder' rowspan='2'><b>" . $Lang['eInventory']['StocktakeFollowupRemark'] . "</b></td>";
                $table .= "</tr>";
                $table .= "<tr>";
                $table .= "<td align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['NoSuchItem'] . "</b></td>";
                $table .= "<td align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['Variation'] . "</b></td>";
                $table .= "</tr>";
            } else {
                $table .= "<tr>";
                $table .= "<td width='17.3%' align='center' class='eSporttdborder' rowspan='2'><b>" . $i_InventorySystem_Item_Code . "</b></td>";
                $table .= "<td width='14%' align='center' class='eSporttdborder' rowspan='2'><b>" . $i_InventorySystem_Item_Name . "</b></td>";
                $table .= "<td width='20%' align='center' class='eSporttdborder' rowspan='2'><b>" . $Lang['eInventory']['ItemDetails'] . "</b></td>";
                $table .= "<td width='10%' align='center' class='eSporttdborder' rowspan='2'><b>" . $i_InventorySystem_Stocktake_ExpectedQty . "</b></td>";
                $table .= "<td width='5.9%' align='center' class='eSporttdborder' rowspan='2'><b>" . $i_InventorySystem_StockTakeResult . "<br>" . $Lang['eInventory']['DataCorrect'] . "</b></td>";
                $table .= "<td width='17.7%' align='center' class='eSporttdborder' colspan='3'><b>" . $i_InventorySystem_StockTakeResult . ": " . $Lang['eInventory']['DataInCorrect'] . "</b></td>";
                $table .= "<td width='16%' align='center' class='eSporttdborder' rowspan='2'><b>" . $Lang['eInventory']['StocktakeFollowupRemark'] . "</b></td>";
                $table .= "</tr>";
                $table .= "<tr>";
                $table .= "<td align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['NoSuchItem'] . "</b></td>";
                $table .= "<td align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['CurrentQty'] . "</b></td>";
                $table .= "<td align='center' class='eSporttdborder'><b>" . $Lang['eInventory']['Variation'] . "</b></td>";
                $table .= "</tr>";
            }
            if (sizeof($arr_result) > 0) {
                foreach ($arr_result as $k1 => $data) {
                    // #####################
                    // table content
                    // #####################
                    $table .= "<tr>";
                    $table .= "<td class='eSporttdborder'>" . $data['ItemCode'] . "</td>";
                    $table .= "<td class='eSporttdborder'>" . $data['ItemName'] . "</td>";
                    $table .= "<td class='eSporttdborder'>" . $data['ItemDescription'] . "</td>";
                    if ($this_item_type == 2) {
                        $table .= "<td class='eSporttdborder' align='center'>" . $data['Qty'] . "</td>";
                        $table .= "<td class='eSporttdborder'>&nbsp;</td>";
                    }
                    $table .= "<td class='eSporttdborder'>&nbsp;</td>";
                    $table .= "<td class='eSporttdborder'>&nbsp;</td>";
                    $table .= "<td class='eSporttdborder'>&nbsp;</td>";
                    $table .= "<td class='eSporttdborder'>&nbsp;</td>";
                    $table .= "</tr>";
                }
            }
            
            // add empty rows at the end
            for ($row_i = 0; $row_i < 5; $row_i ++) {
                $table .= "<tr>";
                $cell_no = $this_item_type == 1 ? 7 : 9;
                for ($cell_i = 0; $cell_i < $cell_no; $cell_i ++)
                    $table .= "<td class='eSporttdborder'>&nbsp;<br>&nbsp;</td>";
                $table .= "</tr>";
            }
            $table .= "</table>";
            
            // #####################
            // Footer
            // #####################
            $table .= "<br><br><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
            $table .= "<tr><td><br><br>" . $Lang['eInventory']['StocktakeDateTime'] . ": </td><td><br><br> ________________________________________</td><td rowspan='4' valign='top'><br><br>" . $Lang['eInventory']['StocktakeListRemark'] . "</td></tr>";
            $table .= "<tr><td><br><br>" . $Lang['eInventory']['SotcktakePICSignatureForGeneral'] . ": </td><td><br><br> ________________________________________ </td></tr>";
            $table .= "<tr><td width='170'><br><br>" . $Lang['eInventory']['SotcktakeApproverSignatureForGeneral'] . ": </td><td><br><br> ________________________________________ </td></tr>";
            $table .= "<tr><td><br><br>" . $Lang['eInventory']['PrincipalSignatureForGeneral'] . ": </td><td><br><br> ________________________________________ </td></tr>";
            // $table .= "<tr><td align='center'><br><br><b>". $Lang['eInventory']['SKH_StocktakeListRemark'] ."</b></td></tr>";
            $table .= "</table>";
            
            $table .= "<div style='page-break-after:always'>&nbsp;</div>";
        }
    }
}

if (! $with_data) {
    $table .= "<table border='0' width='100%' >";
    $table .= "<tr>";
    $table .= "<td align='center'><br><br><br><br><br>" . $Lang['General']['NoRecordFound'] . "</td>";
    $table .= "</tr>";
    $table .= "</table>";
}

?>
<?if($with_data){?>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	align="center" class='print_hide'>
	<tr>
		<td align="right">
			<?= $linterface->GET_BTN($button_print, "submit", "window.print(); return false;"); ?>
		</td>
	</tr>
</table>
<? } ?>
<style type='text/css'>
html, body, table {
	font-family: Times New Roman;
}
</style>
<?=$table?>
<?intranet_closedb();?>