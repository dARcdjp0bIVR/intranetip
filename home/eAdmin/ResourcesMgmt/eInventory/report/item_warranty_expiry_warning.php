<?php
// using:

// #################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2011-02-02 YatWoon
// update mysql error
//
// ##################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $i_InventorySystem_ItemWarrantyExpiryWarning;
$linterface = new interface_html("popup.html");
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$linterface->LAYOUT_START();

$curr_date = date("Y-m-d");
$DayPeriod = $linventory->getWarrantyExpiryWarningDayPeriod();
$date_range = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") + $DayPeriod, date("Y")));

if ($linventory->IS_ADMIN_USER($UserID))
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
else
    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";

$tmp_arr_group = $linventory->returnVector($sql);
if (sizeof($tmp_arr_group) > 0) {
    $target_admin_group = implode(",", $tmp_arr_group);
}

if ($target_admin_group != "") {
    $cond = " AND c.AdminGroupID IN ($target_admin_group) ";
}

$sql = "SELECT 
					a.ItemCode, 
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					concat(" . $linventory->getInventoryNameByLang("building.") . ",'>'," . $linventory->getInventoryNameByLang("floor.") . ",'>'," . $linventory->getInventoryNameByLang("d.") . "),
					b.WarrantyExpiryDate, 
					b.SupplierName, 
					b.SupplierContact, 
					b.MaintainInfo 
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND b.WarrantyExpiryDate != '') INNER JOIN
					INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID) INNER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS floor ON (d.LocationLevelID = floor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID)
			WHERE 
					b.WarrantyExpiryDate BETWEEN '$curr_date' AND '$date_range'
					$cond
			ORDER BY
					b.WarrantyExpiryDate";
$arr_expiry = $linventory->returnArray($sql);

$table_content .= "<tr><td class=\"tablegreentop tabletopnolink\" width=\"14%\">$i_InventorySystem_Item_Code</td>";
$table_content .= "<td class=\"tablegreentop tabletopnolink\" width=\"14%\">$i_InventorySystem_Report_ItemName</td>";
$table_content .= "<td class=\"tablegreentop tabletopnolink\" width=\"14%\">$i_InventorySystem_Group_Name</td>";
$table_content .= "<td class=\"tablegreentop tabletopnolink\" width=\"14%\">$i_InventorySystem_Item_Location</td>";
$table_content .= "<td class=\"tablegreentop tabletopnolink\" width=\"14%\">$i_InventorySystem_Report_WarrantyExpiryDate</td>";
$table_content .= "<td class=\"tablegreentop tabletopnolink\" width=\"14%\">$i_InventorySystem_Item_Supplier_Name</td>";
$table_content .= "<td class=\"tablegreentop tabletopnolink\" width=\"14%\">$i_InventorySystem_Item_Supplier_Contact</td>";
$table_content .= "<td class=\"tablegreentop tabletopnolink\" width=\"16%\">$i_InventorySystemItemMaintainInfo</td></tr>";

if (sizeof($arr_expiry) > 0) {
    for ($i = 0; $i < sizeof($arr_expiry); $i ++) {
        list ($item_code, $item_name, $item_group, $item_location, $item_warranty_expiry, $item_supplier_name, $item_supplier_contact, $item_maintain_info) = $arr_expiry[$i];
        
        $j = $i + 1;
        if ($i % 2 == 0)
            $table_row_css = " CLASS = \"tablegreenrow1\" ";
        else
            $table_row_css = " CLASS = \"tablegreenrow2\" ";
        
        $table_content .= "<tr $table_row_css><td class=\"tabletext\">$item_code</td>";
        $table_content .= "<td class=\"tabletext\">$item_name</td>";
        $table_content .= "<td class=\"tabletext\">$item_group</td>";
        $table_content .= "<td class=\"tabletext\">$item_location</td>";
        $table_content .= "<td class=\"tabletext\">$item_warranty_expiry</td>";
        $table_content .= "<td class=\"tabletext\">$item_supplier_name</td>";
        $table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
        $table_content .= "<td class=\"tabletext\">$item_maintain_info</td></tr>";
    }
}
if (sizeof($arr_expiry) == 0) {
    $table_content .= "<tr class=\"tablegreenrow1\"><td class=\"tabletext\" colspan=\"8\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}

// include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
?>

<br>
<table width="90%" border="0" cellspacing="0" cellpadding="5"
	align="center" class="eSporttableborder">
	<?=$table_content;?>
</table>
<table width="90%" border="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5"
				align="center">
				<tr>
					<td class="dotline"><img
						src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10"
						height="1" /></td>
				</tr>
				<tr>
					<td align="center"><?= $linterface->GET_BTN($button_close, "button", "self.close();","closebtn") ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>