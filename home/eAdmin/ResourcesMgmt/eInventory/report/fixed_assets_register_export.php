<?php

// Using: 
/********************************* Modification Log *************************************
 * 
 * Date: 2020-01-20 (Tommy)
 * add checking $special_feature['eInventory']['CheckForOnlyOneInvoice'] to get record's purchase price which have only one invoice
 * 
 * 2019-10-14 (Henry)
 * add $sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem']
 * 
 * 2019-10-14 (Tommy)
 * fix: wrong title name of funding source
 * 
 * 2019-08-16 (Henry)
 * set the purchase to 0 for the write off item
 * 
 * 2019-06-20 (Tommy)
 * add "funding source" print out
 * 
 * 2019-05-13 (Henry)
 *      Security fix: SQL without quote
 * 
 * 2018-02-22 Henry
 *      add access right checking [Case#E135442]
 *
 * 2017-05-05 (Cameron)
 * 		improved: add filter "DisplayNotReqStocktakingItem"
 *  
 * 2016-02-02 (Henry)
 *			PHP 5.4 fix: change split to explode
 *
 * 2015-05-15 (Henry)
 * 		display "Purchase Price Total" [Case#R76591]
 * 
 * 2015-01-27 (Henry) < ip.2.5.6.3.1
 *		fixed: Fail to show purchase date if exported Fixed Assets Register [Case#Y74456]
 * 
 * 2014-12-04 (Henry) < ip.2.5.5.12.1
 *		fixed: display Real Cost for purchasing the asset (Purchase quantity x Unit Price) [Case#V72270]
 * 
 * 2014-11-28 (Henry) < ip.2.5.5.12.1
 *		fixed: show write off reason after the book restore from writeoff [Case#V72272]
 *
 * 2014-11-25 (Henry) < ip.2.5.5.12.1
 *		modified get Total purchase price [Case#B70033]
 * 
 * 2014-11-17 (Henry) < ip.2.5.5.12.1
 * 		fixed: CSV problem (missing location column now) [Case#D71480]
 * 
 * 2014-07-22 (YatWoon) 
 * 		fixed: Display 0 bulk items even selected "don't display Display written-off item"	[Case#P64059]
 * 
 * 2013-10-31 (YatWoon)
 *		improved: add option S/N, Quotation Number, Supplier, Tender No and Invoice Number [Case#2013-0916-1056-37066]
 *
 * 2013-05-28 (YatWoon)
 *		improved: display funding name even the cost is $0 [Case#2013-0520-1638-56073]
 *
 * 2013-03-12 (YatWoon)
 *		improved: retrieve record from INVENTORY_ITEM_BULK_LOG with RecordStatus (check the record is deleted or not)
 *
 * 2012-11-26 (YatWoon)
 *		fixed: display 0 qty for single item if the item is write-off [Case#2012-1030-1053-56073]
 * 
 * 2012-07-24 (Yuen)
 *		i. add 2 filters to show stocktake items, and/or according to price
 *  
 * 2012-07-10 (YatWoon)
 *		Enhanced: support 2 funding source for single item
 *
 * 2012-06-22 (YatWoon)
 *		improved: add "Category" and "SubCategory" column
 *
 * 2012-05-29 (YatWoon)
 *		fixed: missing for display bulk item remark
 *
 * 2012-03-20 (YatWoon)
 *		fixed for bulk item, get written-off item qty missing (sum)
 *
 * 2012-01-30 (YatWoon)
 *		update total purchase price for export
 *
 * 2011-12-02 (YatWoon)
 *		update purchase date logic, please refer to case #2011-1128-1644-04071
 		4 scenario (sample):
		1. From 2011-05-01 To 2011-05-30
		Search records with the above date range.
		
		2. From [blank] To [blank]
		Search records without purchase date checking, i.e. search all
		
		3. From 2011-05-01 To [blank]
		Search records with purchase date >= 2011-05-01  (to future)
		
		4. From [blank] To 2011-05-30
		Search records with purchase date <= 2011-05-30
 *
 * 2011-07-21 (YatWoon)
 *		add "Sponsoring body" for funding type
 *		fixed display different funding source for bulk item
 *
 * 2011-06-30 (YatWoon)
 *		i. Add option "Display written-off item"
 *
 * 2011-06-23 (Yuen)
 *			corrected the initial quantity of bulk item by not counting the pending write-off items 
 *
 * 2011-06-02 (Yuen)
 *		calculate price for bulk item base on average unit price * quantity
 *
 * 2011-04-19 (Yuen)
 *		for single item's cost and total, user purchase price if it is lower than the unit price * quantity 
 *	
 * 2011-04-18 (YatWoon)
 *	i. update Government and School wordings
 *
 * 2011-04-08 (YatWoon)
 *		i. Check bulk item quantity > 0
 *
 ****************************************************************************************/
 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPage	= "Report_FixedAssetsRegister";
$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li 			= new libfilesystem();
$lexport 		= new libexporttext();


if ($CountStocktakeOnly && $StockTakeDateFrom!="" && $StockTakeDateEnd!="")
{
	//debug($StockTakeDateFrom, $StockTakeDateEnd);
	$sql = "SELECT 
									DISTINCT a.ItemID
							FROM 
									INVENTORY_ITEM_SINGLE_STATUS_LOG AS a 
							WHERE 
									a.Action IN (2,3,4) AND (a.RecordDate BETWEEN '$StockTakeDateFrom' and '$StockTakeDateEnd')
							ORDER BY
									a.ItemID
							";
	$StocktakeResultArray = $linventory->returnVector($sql);
	
	$sql = "SELECT DISTINCT a.ItemID FROM INVENTORY_ITEM_BULK_LOG AS a WHERE a.Action = 2  AND (a.RecordDate BETWEEN '$StockTakeDateFrom' and '$StockTakeDateEnd')
							ORDER BY a.ItemID";
	$StocktakeBulkResultArray = $linventory->returnVector($sql);
	//debug_r($StocktakeBulkResultArray);
} else
{
	$StockTakeDateFrom = null;
	$StockTakeDateEnd = null;
}


if ($CountByPrice)
{
	//debug($FilterSingleItemPriceFrom, $FilterSingleItemPriceEnd);
	//debug($FilterBulkItemPriceFrom, $FilterBulkItemPriceEnd);
} else
{
	$FilterSingleItemPriceFrom = null;
	$FilterSingleItemPriceEnd = null;
	$FilterBulkItemPriceFrom = null;
	$FilterBulkItemPriceEnd = null;
}

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root."/file/schoolbadge.txt");
if ($school_badge != "")
{
    $badge_image = "<img src=/file/$school_badge>";
}

$exportColumn = array($i_InventorySystem_Report_FixedAssetsRegister,'','','','','','','','','','','','','','');

$file_content .= "\"$i_InventorySystem_Report_FixedAssetsRegister\"\n";
$file_content .= "\n";

/*
// prepare for purchase period
if ($PurchaseDateFrom=="yyyy-mm-dd")
	$PurchaseDateFrom = "";
if ($PurchaseDateTo=="yyyy-mm-dd")
	$PurchaseDateTo = "";
if ($PurchaseDateFrom!="" && $PurchaseDateTo=="")
{
	// from to 
	$PurchaseDateTo = $PurchaseDateFrom;
} elseif ($PurchaseDateFrom=="" && $PurchaseDateTo!="")
{
	$PurchaseDateFrom = $PurchaseDateTo;
}
if ($PurchaseDateFrom!="" && $PurchaseDateTo!="")
{
	$sql_purchase_period = " AND unix_timestamp([PURCHASE_DATE])>=unix_timestamp('{$PurchaseDateFrom}') AND unix_timestamp([PURCHASE_DATE])<=unix_timestamp('{$PurchaseDateTo}') ";
}
*/

if($groupBy==1)
{
	# get all location #
	// added main buliding name [Case#D71480]
	$arrTempTargetRoom = $TargetRoom;
	if(array_shift($arrTempTargetRoom) == ""){
		$TargetRoom = $arrTempTargetRoom;
	}
	$selected_cb = implode(",",$TargetRoom);

	$sql = "Select 
				DISTINCT a.LocationID, CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("a.").")
			FROM 
				INVENTORY_LOCATION AS a INNER JOIN
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID) 
			WHERE 
				a.LocationID in (".$selected_cb.") 
			ORDER BY 
				b.DisplayOrder, a.DisplayOrder";
	$arr_location = $linventory->returnArray($sql,1);
	
	for($a=0; $a<sizeof($arr_location); $a++)
	{	
		list($locationID, $location_name) = $arr_location[$a];
		
		if(!$display_writeoff)
		{
			$wo_con = " and a.RecordStatus=1 ";
			$wo_conb = " and c.Quantity>0";
		}
		
		
		/* 2012-07-25 Yuen - may have problem for bulk items according to purchase date and location
		
		if($PurchaseDateFrom && $PurchaseDateTo)
		{
			$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or 
									(e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";
		}
		if($PurchaseDateFrom && !$PurchaseDateTo)
		{
			$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."') or 
									(e.PurchaseDate>='". $PurchaseDateFrom ."'))";
		}
		if(!$PurchaseDateFrom && $PurchaseDateTo)
		{
			$purchase_date_sql = " and ((b.PurchaseDate<='". $PurchaseDateTo ."') or 
									(e.PurchaseDate<='". $PurchaseDateTo ."'))";
		}
		$sql = "SELECT 
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					".$linventory->getInventoryNameByLang("a.").",
					".$linventory->getInventoryDescriptionNameByLang("a.").",
					".$linventory->getInventoryNameByLang("c1.").",
					".$linventory->getInventoryNameByLang("c2.")."
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID OR c.LocationID = d.LocationID)
					left outer join INVENTORY_ITEM_BULK_LOG as e on (e.ItemID=a.ItemID and e.LocationID IN ($locationID))
					LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
					LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
				WHERE
					d.LocationID IN ($locationID)
					$purchase_date_sql
					$wo_con
				ORDER BY
					a.ItemCode
				";
		*/
		
		if($PurchaseDateFrom && $PurchaseDateTo)
		{
			$purchase_date_sql = "  ((b.LocationID in ($locationID) and b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or 
									(c.LocationID in ($locationID) and e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";
		} elseif($PurchaseDateFrom && !$PurchaseDateTo)
		{
			$purchase_date_sql = "  ((b.LocationID in ($locationID) and b.PurchaseDate>='". $PurchaseDateFrom ."') or 
									(c.LocationID in ($locationID) and e.PurchaseDate>='". $PurchaseDateFrom ."'))";
		} elseif(!$PurchaseDateFrom && $PurchaseDateTo)
		{
			$purchase_date_sql = "  ((b.LocationID in ($locationID) and b.PurchaseDate<='". $PurchaseDateTo ."') or 
									(c.LocationID in ($locationID) and e.PurchaseDate<='". $PurchaseDateTo ."'))";
		} else
		{
			$purchase_date_sql = "  (b.LocationID in ($locationID) or c.LocationID in ($locationID))";
		}
		$sql = "SELECT 
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					".$linventory->getInventoryNameByLang("a.").",
					".$linventory->getInventoryDescriptionNameByLang("a.").",
					".$linventory->getInventoryNameByLang("c1.").",
					".$linventory->getInventoryNameByLang("c2.")."
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) 
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_LOG as e on (e.ItemID=a.ItemID AND e.ItemID=c.ItemID)
					LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
					LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
				WHERE
					$purchase_date_sql
					$wo_con
				ORDER BY
					a.ItemCode
				";
		$arr_result = $linventory->returnArray($sql);
// 		$file_content .= "\"$i_InventorySystem_Item_Location\",\"$location_name\"\n";
// 		$file_content .= "\"\",\"\",\"\",\"\",\"$i_InventorySystem_Report_Col_Cost\",\"$i_InventorySystem_Report_Col_Cost\",\"$i_InventorySystem_Report_Col_Cost\"\n";
// 		$file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Report_Col_Item\",\"$i_InventorySystem_Report_Col_Description\",\"$i_InventorySystem_Report_Col_Purchase_Date\",\"". $Lang['eInventory']['FundingType']['School'] ."\",\"". $Lang['eInventory']['FundingType']['Government'] ."\",\"". $Lang['eInventory']['FundingType']['SponsoringBody'] ."\",\"$i_InventorySystem_Report_Col_Sch_Unit_Price\",\"".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."\",\"$i_InventorySystem_Report_Col_Quantity\",\"$i_InventorySystem_Report_Col_Location\",\"$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off\",\"$i_InventorySystem_Report_Col_Signature\",\"$i_InventorySystem_Report_Col_Remarks\"\n";
		
		$temprows = array($i_InventorySystem_Item_Location,$location_name);
		$rows[] = $temprows;
		$temprows = array('','','','','','',$i_InventorySystem_Report_Col_Cost,$i_InventorySystem_Report_Col_Cost,$i_InventorySystem_Report_Col_Cost);
		$rows[] = $temprows;
		$temprows = array($i_InventorySystem['Category'], $i_InventorySystem['SubCategory'], $i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$Lang['eInventory']['FundingType']['School'],$Lang['eInventory']['FundingType']['Government'],$Lang['eInventory']['FundingType']['SponsoringBody'],$i_InventorySystem_Report_Col_Sch_Unit_Price,$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'],$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location,$i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Quot_Num, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num, $i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
		$rows[] = $temprows;
		
		$totalPurchasePrice = 0;
		
		if(sizeof($arr_result)>0 )
		{
			for($i=0; $i<sizeof($arr_result); $i++)
			{
				list($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];
				
				# filter out no stock-taken item if necessary
				if ($item_type==ITEM_TYPE_SINGLE && is_array($StocktakeResultArray) && !in_array($item_id, $StocktakeResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				} elseif ($item_type==ITEM_TYPE_BULK && is_array($StocktakeBulkResultArray) && !in_array($item_id, $StocktakeBulkResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				}
				
				
				if($item_type == ITEM_TYPE_SINGLE)
				{
					// added main buliding name [Case#D71480]
					$sql = "SELECT
									a.PurchaseDate, 
									if(a.PurchasedPrice != '',a.PurchasedPrice,'0') as PurchasedPrice,
									if(a.UnitPrice != '',a.UnitPrice,'0') as UnitPrice,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									d.FundingType,
									".$linventory->getInventoryNameByLang("d.").",
									e.ApproveDate,
									e.WriteOffReason,
									a.ItemRemark,
									".$linventory->getInventoryNameByLang("d2.").",
									a.UnitPrice1,
									a.UnitPrice2,
									d2.FundingType as FundingType2,
									i.StockTakeOption
							FROM
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
									INVENTORY_ITEM i ON (i.ItemID=a.ItemID AND i.ItemType=1) INNER JOIN 
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_FUNDING_SOURCE AS d2 ON (a.FundingSource2 = d2.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
							WHERE
									a.ItemID = $item_id
							"; 
					$arr_sub_result = $linventory->returnArray($sql);
					
					if (!$display_not_req_stocktaking) { 
						$rs =  $linventory->FilterForStockTakeSingle($arr_sub_result);
						if (!count($rs)) {
							continue;
						}
					}
					
					if ($FilterSingleItemPriceFrom!=null || $FilterSingleItemPriceEnd!=null)
					{
						list($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark, $this_item_id, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[0];
						if ($FilterSingleItemPriceFrom!=null && $unit_price<$FilterSingleItemPriceFrom)
						{
							continue;
						} elseif ($FilterSingleItemPriceEnd!=null && $unit_price>$FilterSingleItemPriceEnd)
						{
							continue;
						}
					}
					
					if(sizeof($arr_sub_result)>0)
					{
						for($j=0; $j<sizeof($arr_sub_result); $j++)
						{
							list($purchase_date,$purchase_price,$unit_price,$location_id,$location_name,$funding_type,$funding_name,$write_off_date,$write_off_reason, $remark, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[$j];
// 							debug_pr($arr_sub_result[$j]);
							
							
											
// 							$quantity = 1;
							$quantity = $write_off_date ? "0" : "1";
							if ($unit_price>0)
								$purchase_price = $unit_price * $quantity;
							else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
                            	$purchase_price = 0;
                            }
                            
							$totalPurchasePrice +=$purchase_price;
							
							if ($purchase_date=="0000-00-00")
								$purchase_date = "";
							else
								$purchase_date = " " . $purchase_date;
								
							/*
							$school_funding = "0";
							$gov_funding = "0";
							$sponsor_funding = "0";
							if($ShowFundingName)
							{
								$str_funding_name = " (".$funding_name.")";
							}
							if($purchase_price==0)	$str_funding_name = "";
								
							if($funding_type == ITEM_SCHOOL_FUNDING)
							{
								$school_funding = $purchase_price.$str_funding_name;
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{
								$gov_funding = $purchase_price.$str_funding_name;
								
							}
							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
							{
								$sponsor_funding = $purchase_price.$str_funding_name;
							}
							*/
							
							$Cost_Ary = array();
							$Funding_ary = array();
							$Cost_Ary[$funding_type] = $unit_price1;
							if($ShowFundingName)
								$Funding_ary[$funding_type] = ($Funding_ary[$funding_type] ? ", ":"") . $funding_name;
							if($funding_type2)
							{
								$Cost_Ary[$funding_type2] += $unit_price2;
								if($ShowFundingName)
									$Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", ":"") . $funding_name2;
							}
							
// 							if($funding_type == ITEM_SCHOOL_FUNDING)
// 							{
// 								$school_funding = $purchase_price.$str_funding_name;
								$school_funding = number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2).($Funding_ary[ITEM_SCHOOL_FUNDING]? " (".$Funding_ary[ITEM_SCHOOL_FUNDING].")":"");
// 							}
// 							if($funding_type == ITEM_GOVERNMENT_FUNDING)
// 							{
// 								$gov_funding = $purchase_price.$str_funding_name;
								$gov_funding = number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2).($Funding_ary[ITEM_GOVERNMENT_FUNDING]? " (".$Funding_ary[ITEM_GOVERNMENT_FUNDING].")":"");
// 							}
// 							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
// 							{
								//$sponsor_funding = $purchase_price.$str_funding_name;
								$sponsor_funding = number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2).($Funding_ary[ITEM_SPONSORING_BODY_FUNDING]? " (".$Funding_ary[ITEM_SPONSORING_BODY_FUNDING].")":"");
// 							}
							
							if($write_off_date != "")
							{
								if($write_off_reason != "")
								{
									$write_off_date_reason = $write_off_date." (".$write_off_reason.")";
								}
								else
								{
									$write_off_date_reason = $write_off_date;
								}
							}
							else
							{
								$write_off_date_reason = "";
							}
						}
					}
					
					# S/N, Quotation No, Supplier, Tender No., Invoice No.
					$sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID=$item_id";
					$result2 = $linventory->returnArray($sql);
					list($thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $result2[0];					

// 					$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$purchase_date\",\"$school_funding\",\"$gov_funding\",\"$sponsor_funding\",\"$unit_price\",\"$purchase_price\",\"$quantity\",\"$location_name\",\"$write_off_date_reason\",\"\",\"\"\n";
					$temprows = array($item_category, $item_subcategory, $item_code,$item_name,$item_description,$purchase_date,$school_funding,$gov_funding,$sponsor_funding,$unit_price,$purchase_price,$quantity,$location_name,
										$thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo, 
										$write_off_date_reason,"",$remark);
					$rows[] = $temprows;
				}
				if($item_type == ITEM_TYPE_BULK)
				{
					if (!$display_not_req_stocktaking) {
						$sql = "SELECT ItemID, StockTakeOption FROM INVENTORY_ITEM WHERE ItemID='".$item_id."'";
						$rs = $linventory->returnResultSet($sql);
						$rs = $linventory->FilterForStockTakeBulk($rs);
						if (!count($rs)) {
							continue;
						}
					}					
					
					$final_purchase_date = "";
					
					### GET PURCHASE DATE ###
					if($PurchaseDateFrom && $PurchaseDateTo)
					{
					    $purchase_date_sql = " and (PurchaseDate>='". $PurchaseDateFrom ."' and PurchaseDate<='". $PurchaseDateTo ."')";
					}
					if($PurchaseDateFrom && !$PurchaseDateTo)
					{
					    $purchase_date_sql = " and (PurchaseDate>='". $PurchaseDateFrom ."')" ;
					}
					if(!$PurchaseDateFrom && $PurchaseDateTo)
					{
					    $purchase_date_sql = " and (PurchaseDate<='". $PurchaseDateTo ."') ";
					}
					$sql = "SELECT
									distinct(IF(PurchaseDate IS NULL, '', PurchaseDate))
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									ItemID = $item_id AND
									Action = 1 
									and (RecordStatus=0 or RecordStatus is NULL) 
                                    $purchase_date_sql
							";
                                    	/*
							AND
									LocationID = $locationID */
					$arr_purchase_date = $linventory->returnArray($sql,1);
					$tmp_arr_purchase_date = array();
					if(sizeof($arr_purchase_date)>0)
					{
						for($j=0; $j<sizeof($arr_purchase_date); $j++)
						{
							list($purchase_date) = $arr_purchase_date[$j];
							if($purchase_date != ''){
								//$final_purchase_date = $final_purchase_date."\r\n".$purchase_date;
								$tmp_arr_purchase_date[] = " " . $purchase_date;
							}else{
								//$final_purchase_date = ' ';
							}
						}
						if(sizeof($tmp_arr_purchase_date)>0){
							$final_purchase_date = implode("\r\n",$tmp_arr_purchase_date);
						}
					}
					else
					{
						$final_purchase_date = ' ';
					}
					
					### GET COST, UNIT PRICE, PURCHASED PRICE ###
					$total_school_cost = 0;
					$total_gov_cost = 0;
					$total_sponsor_cost = 0;
					$final_unit_price = 0;
					$final_purchase_price = 0;
					$total_qty = 0;
					$tmp_unit_price = 0;
					$tmp_purchase_price = 0;
					$written_off_qty = 0;
					$location_qty = 0;
					$ini_total_qty = 0;
					
					## get Total purchase price ##
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}else{
					    //$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					    $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}

					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$total_purchase_price = $tmp_result[0];
						# [Case#B70033]
						if($total_purchase_price == 0){
							$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
							$tmp_result = $linventory->returnVector($sql);
							if($tmp_result[0] > 0)
								$total_purchase_price = $tmp_result[0];
						}
					}
					
					$sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$item_remark = implode(";\r\n", array_unique($tmp_result));
					}
					
					## get Total Qty Of the target Item ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($totalQty) = $tmp_result[0];
					}
					## get written-off qty ##
					$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($written_off_qty) = $tmp_result[0];
					}
					
					## get the qty in the target location ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."' AND LocationID = '".$locationID."'";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($location_qty) = $tmp_result[0];
					}
					
					## Location origial qty (by location only)##
					$sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND LocationID = '".$locationID."'";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$ini_location_qty = $tmp_result[0] + $location_qty;
					}else{
						$ini_location_qty = $location_qty;
					}
					
					$sql = "SELECT 
									a.FundingSource, 
									b.FundingType,
									".$linventory->getInventoryNameByLang("b.")."
							FROM 
									INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
									INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
							WHERE 
									a.ItemID = '".$item_id."' AND
									a.Action IN (1,7) AND
									a.LocationID = '".$locationID."' AND 
									a.FundingSource IS NOT NULL 
									and (a.RecordStatus=0 or a.RecordStatus is NULL) 
							";
					$tmp_result = $linventory->returnArray($sql,3);
					if(sizeof($tmp_result)>0){
						list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
					}
					
					### Get Qty By Location ###
					// added main buliding name [Case#D71480]
					$sql = "SELECT 
									a.Quantity,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.").")
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID)
							WHERE
									a.ItemID = '".$item_id."' AND 
									a.LocationID = '".$locationID."'
							";
					$arr_tmp_qty = $linventory->returnArray($sql,3);
					if(sizeof($arr_tmp_qty)>0)
					{
						$display_qty = 0;
						$qty = 0;
						$display_location_name = "";
						$tmp_arr_display_location_name = array();
						for($j=0; $j<sizeof($arr_tmp_qty); $j++)
						{
							list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
							if($qty != "")
							{
								$display_qty = $display_qty + $qty;
							}
							if($location_name != "")
							{
								//$display_location_name = $display_location_name."\r\n".$location_name;
								$tmp_arr_display_location_name[] = $location_name;
							}
						}
						if(sizeof($tmp_arr_display_location_name)>0){
							$display_location_name = implode("\r\n",$tmp_arr_display_location_name);
						}
					}
					
					$ini_total_qty = $totalQty + $written_off_qty;
					if ($ini_total_qty>0)
						$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
					
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $this_cost = round($total_purchase_price,2);
					}else{
					    $this_cost = round($final_unit_price*$display_qty,2);
					}

					$final_purchase_price = $this_cost;
					# [Case#V72270]
					if($display_writeoff){
						$this_cost = round($final_unit_price*$ini_location_qty,2);
						$final_purchase_price = round($final_unit_price*$display_qty,2);
					}
					
					$totalPurchasePrice +=$final_purchase_price;
					
					if ($FilterBulkItemPriceFrom!=null || $FilterBulkItemPriceEnd!=null)
					{
						if ($FilterBulkItemPriceFrom!=null && $final_purchase_price<$FilterBulkItemPriceFrom)
						{
							$rows_shown --;
							continue;
						} elseif ($FilterBulkItemPriceEnd!=null && $final_purchase_price>$FilterBulkItemPriceEnd)
						{
							$rows_shown --;
							continue;
						}
					}
					
					if($ShowFundingName){
						//$str_funding_name = $final_purchase_price > 0 ? " (".$funding_name.")" : "";
						$str_funding_name = " (".$funding_name.")";
						$this_cost .= $str_funding_name;
					}
									
					if($funding_type == ITEM_SCHOOL_FUNDING)
					{
						$total_school_cost = $this_cost;
						/*
						$ini_total_qty = $totalQty + $written_off_qty;
						if ($ini_total_qty>0)
							$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
						//$total_school_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
						$total_school_cost = round($final_unit_price*$display_qty,2);
						if($ShowFundingName){
							$str_funding_name = " (".$funding_name.")";
							$total_school_cost .= $str_funding_name;
						}
						$final_purchase_price = $total_school_cost;
						*/
					}
					if($funding_type == ITEM_GOVERNMENT_FUNDING)
					{
						$total_gov_cost = $this_cost;
						/*
						$ini_total_qty = $totalQty + $written_off_qty;
						if ($ini_total_qty>0)
							$final_unit_price =  round($total_purchase_price/$ini_total_qty,2);
						//$total_gov_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
						$total_gov_cost = round($final_unit_price*$display_qty);
						if($ShowFundingName){
							$str_funding_name = " (".$funding_name.")";
							$total_gov_cost .= $str_funding_name;
						}
						$final_purchase_price = $total_gov_cost;
						*/
					}
					if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
					{
						$total_sponsor_cost = $this_cost;
					}
					$final_unit_price = round($final_unit_price, 2);
					
					### Get Write-Off Date & Reason ###
					$display_write_off = "";
					
					$sql = "SELECT
									ApproveDate,
									WriteOffQty,
									WriteOffReason
							FROM
									INVENTORY_ITEM_WRITE_OFF_RECORD
							WHERE
									ItemID = '".$item_id."' AND 
									LocationID = '".$locationID."'
									 AND RecordStatus = 1
							";
							
					$arr_write_off = $linventory->returnArray($sql,2);
					
					if(sizeof($arr_write_off)>0)
					{
						$tmp_write_off = array();
						for($j=0; $j<sizeof($arr_write_off); $j++)
						{
							list($write_off_date,$write_off_qty,$write_off_reason) = $arr_write_off[$j];
							if($write_off_date != "")
							{
								$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
								$tmp_write_off[] = $write_off_date."\r\n".$write_off_qty."\r\n".$write_off_reason;
							}
						}

						if(sizeof($tmp_write_off)>0){
							for($k=0; $k<sizeof($tmp_write_off);$k++){
								$str_reason = $tmp_write_off[$k];
								$display_write_off .= $str_reason;
								if($k!=0){
									$display_write_off .= "\r\n";
								}
							}
						}
					}
					else
					{
						$display_write_off = "";
					}
					
// 					$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$final_purchase_date\",\"$total_school_cost\",\"$total_gov_cost\",\"$total_sponsor_cost\",\"$final_unit_price\",\"$final_purchase_price\",\"$display_qty\",\"$display_location_name\",\"$display_write_off\"\n";

					# Quotation No, Supplier, Tender No., Invoice No.	
					$sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID='".$item_id."'";
					$result2 = $linventory->returnArray($sql);
					if(!empty($result2))
					{
						$thisDataAry = array();
						
						foreach($result2 as $k2=>$d2)
						{
							list($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
							if($thisQuotationNo)
								$thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
							if($thisSupplierName)
								$thisDataAry['Supplier'][] = trim($thisSupplierName);
							if($thisTenderNo)
								$thisDataAry['TenderNo'][] = trim($thisTenderNo);
							if($thisInvoiceNo)
								$thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
						}
					}
					
					$SNStr = "";
					$QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ",array_unique($thisDataAry['QuotationNo'])) : "";
					$SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ",array_unique($thisDataAry['Supplier'])) : "";
					$TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ",array_unique($thisDataAry['TenderNo'])) : "";
					$InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ",array_unique($thisDataAry['InvoiceNo'])) : "";
					
					$temprows = array($item_category, $item_subcategory, $item_code,$item_name,$item_description,$final_purchase_date,$total_school_cost,$total_gov_cost,$total_sponsor_cost,$final_unit_price,$final_purchase_price,$display_qty,$display_location_name,
										$SNStr, $QnoNoStr, $SupplierStr, $TenderNoStr, $InvoiceNoStr, 
										$display_write_off,"",$item_remark);
					$rows[] = $temprows;
				}
			}
			if($ShowPurchasePriceTotal)
				$rows[] = array('', '', '','','','','','','',$Lang['eInventory']['TotalAssets'],$totalPurchasePrice,'','','', '', '', '', '', '','','');
		}
		if(sizeof($arr_result) == 0 )
		{
			$file_content .= "\"$i_no_record_exists_msg\"\n";
			$rows[] = array($i_no_record_exists_msg);
			
		}
		$file_content .= "\n";
		$file_content .= "\n";
		$rows[] = array("\n");
		$rows[] = array("\n");
	}	
}
else if($groupBy==2)
{
	$arr_item_type = array(array("1",$i_InventorySystem_ItemType_Single),array("2",$i_InventorySystem_ItemType_Bulk));
	
	if(sizeof($arr_item_type)>0)
	{
		for($a=0; $a<sizeof($arr_item_type); $a++)
		{
			list($item_type, $item_type_name) = $arr_item_type[$a];
									
			$file_content .= "\"".$i_InventorySystem['item_type']."\",\"$item_type_name\"\n";
			$file_content .= "\"\",\"\",\"\",\"\",\"$i_InventorySystem_Report_Col_Cost\",\"$i_InventorySystem_Report_Col_Cost\"\n";
			//$file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Report_Col_Item\",\"$i_InventorySystem_Report_Col_Description\",\"$i_InventorySystem_Report_Col_Purchase_Date\",\"$i_InventorySystem_Report_Col_Govt_Fund\",\"$i_InventorySystem_Report_Col_Sch_Fund\",\"$i_InventorySystem_Report_Col_Sch_Unit_Price\",\"$i_InventorySystem_Report_Col_Sch_Purchase_Price\",\"$i_InventorySystem_Report_Col_Quantity\",\"$i_InventorySystem_Report_Col_Location\",\"$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off\",\"$i_InventorySystem_Report_Col_Signature\",\"$i_InventorySystem_Report_Col_Remarks\"\n";
			$file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Report_Col_Item\",\"$i_InventorySystem_Report_Col_Description\",\"$i_InventorySystem_Report_Col_Purchase_Date\",\"". $Lang['eInventory']['Ownership']['Government'] ."\",\"". $Lang['eInventory']['Ownership']['School'] ."\",\"$i_InventorySystem_Report_Col_Sch_Unit_Price\",\"".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."\",\"$i_InventorySystem_Report_Col_Quantity\",\"$i_InventorySystem_Report_Col_Location\",\"$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off\",\"$i_InventorySystem_Report_Col_Signature\",\"$i_InventorySystem_Report_Col_Remarks\"\n";
			
			//$temprows = array($item_code, $item_tag_code, $item_chi_name, $item_eng_name, $item_chi_desc, $item_eng_desc, $item_cat_code, $item_cat2_code, $item_group_code, $item_location_level_code, $item_location_code, $item_funding_code, $item_ownership, $item_warranty_expiry_date, $item_license, $item_serial_num, $item_brand, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_quotation, $item_tender, $item_invoice, $item_purchase_date, $item_purchased_price, $item_unit_price, $item_maintain_info, $item_remark);
			//$rows[] = $temprows;
								
			$temprows = array($i_InventorySystem['item_type'],$item_type_name);
			$rows[] = $temprows;
			$temprows = array('','','','','','',$i_InventorySystem_Report_Col_Cost,$i_InventorySystem_Report_Col_Cost,$i_InventorySystem_Report_Col_Cost);
			$rows[] = $temprows;
			//$temprows = array($i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$i_InventorySystem_Report_Col_Govt_Fund,$i_InventorySystem_Report_Col_Sch_Fund,$i_InventorySystem_Report_Col_Sch_Unit_Price,$i_InventorySystem_Report_Col_Sch_Purchase_Price,$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location,$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
			//$temprows = array($i_InventorySystem['Category'], $i_InventorySystem['SubCategory'], $i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$Lang['eInventory']['FundingType']['School'],$Lang['eInventory']['FundingType']['Government'],$Lang['eInventory']['FundingType']['SponsoringBody'],$i_InventorySystem_Report_Col_Sch_Unit_Price,$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'],$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location,$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
			
			if($item_type == ITEM_TYPE_SINGLE)
			{	
				if(!$display_writeoff)
				{
					$wo_con = " and a.RecordStatus=1 ";
				}
				$temprows = array($i_InventorySystem['Category'], $i_InventorySystem['SubCategory'], $i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$Lang['eInventory']['FundingType']['School'],$Lang['eInventory']['FundingType']['Government'],$Lang['eInventory']['FundingType']['SponsoringBody'],$i_InventorySystem_Report_Col_Sch_Unit_Price,$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'],$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location,$i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Quot_Num, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num, $i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
			}
			else
			{
				if(!$display_writeoff)
				{
					//$wo_conb = " and c.Quantity>0";
					$wo_con = " and a.RecordStatus=1 and c.Quantity>0";
				}
				$temprows = array($i_InventorySystem['Category'], $i_InventorySystem['SubCategory'], $i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$Lang['eInventory']['FundingType']['School'],$Lang['eInventory']['FundingType']['Government'],$Lang['eInventory']['FundingType']['SponsoringBody'],$i_InventorySystem_Report_Col_Sch_Unit_Price,$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'],$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location, $i_InventorySystem_Item_Quot_Num, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num, $i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
			
			}
			$rows[] = $temprows;
			
			/*
			if(!$display_writeoff)
			{
				$wo_con = " and a.RecordStatus=1 ";
				$wo_conb = " and c.Quantity>0";
			}
			*/
			
// 			$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or (e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";

			if($PurchaseDateFrom && $PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or 
										(e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";
			}
			if($PurchaseDateFrom && !$PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."') or 
										(e.PurchaseDate>='". $PurchaseDateFrom ."'))";
			}
			if(!$PurchaseDateFrom && $PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate<='". $PurchaseDateTo ."') or 
										(e.PurchaseDate<='". $PurchaseDateTo ."'))";
			}
		
			$sql = "SELECT 
						DISTINCT a.ItemID,
						a.ItemType,
						a.ItemCode,
						".$linventory->getInventoryNameByLang("a.").",
						".$linventory->getInventoryDescriptionNameByLang("a.").",
						".$linventory->getInventoryNameByLang("c1.").",
						".$linventory->getInventoryNameByLang("c2.")."
					FROM
						INVENTORY_ITEM AS a LEFT OUTER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID) LEFT OUTER JOIN
						INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID)
						left outer join INVENTORY_ITEM_BULK_LOG as e on (e.ItemID=a.ItemID)
						LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
						LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
					WHERE
						a.ItemType IN ($item_type)
						$purchase_date_sql
						$wo_con  
					ORDER BY
						a.ItemCode
					";
			$arr_result = $linventory->returnArray($sql);
			
			$totalPurchasePrice = 0;
			
			if(sizeof($arr_result)>0)
			{
				for($i=0; $i<sizeof($arr_result); $i++)
				{
					list($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];
					
					# filter out no stock-taken item if necessary
					if ($item_type==ITEM_TYPE_SINGLE && is_array($StocktakeResultArray) && !in_array($item_id, $StocktakeResultArray))
					{
						//debug($item_id, $item_code, $item_name);
						continue;						
					} elseif ($item_type==ITEM_TYPE_BULK && is_array($StocktakeBulkResultArray) && !in_array($item_id, $StocktakeBulkResultArray))
					{
						//debug($item_id, $item_code, $item_name);
						continue;						
					}
					
					if($item_type == ITEM_TYPE_SINGLE)
					{
						// added main buliding name [Case#D71480]
						$sql = "SELECT
										a.PurchaseDate, 
										if(a.PurchasedPrice != '',a.PurchasedPrice,'0') as PurchasedPrice,
										if(a.UnitPrice != '',a.UnitPrice,'0') as UnitPrice,
										a.LocationID,
										CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
										d.FundingType,
										".$linventory->getInventoryNameByLang("d.").",
										e.ApproveDate,
										e.WriteOffReason,
										a.ItemRemark,
										".$linventory->getInventoryNameByLang("d2.").",
										a.UnitPrice1,
										a.UnitPrice2,
										d2.FundingType as FundingType2,
										i.StockTakeOption
								FROM
										INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
										INVENTORY_ITEM i ON (i.ItemID=a.ItemID AND i.ItemType=1) INNER JOIN
										INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
										INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
										INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID) INNER JOIN
										INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
										INVENTORY_FUNDING_SOURCE AS d2 ON (a.FundingSource2 = d2.FundingSourceID) LEFT OUTER JOIN
										INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
								WHERE
										a.ItemID = '".$item_id."'
								"; 
						$arr_sub_result = $linventory->returnArray($sql,10);

						if (!$display_not_req_stocktaking) { 
							$rs =  $linventory->FilterForStockTakeSingle($arr_sub_result);
							if (!count($rs)) {
								continue;
							}
						}
						
						if ($FilterSingleItemPriceFrom!=null || $FilterSingleItemPriceEnd!=null)
						{
							list($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark, $this_item_id, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[0];
							if ($FilterSingleItemPriceFrom!=null && $unit_price<$FilterSingleItemPriceFrom)
							{
								continue;
							} elseif ($FilterSingleItemPriceEnd!=null && $unit_price>$FilterSingleItemPriceEnd)
							{
								continue;
							}
						}
						
						if(sizeof($arr_sub_result)>0)
						{
							for($j=0; $j<sizeof($arr_sub_result); $j++)
							{
								list($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[$j];
// 								$quantity = 1;
								$quantity = $write_off_date ? "0" : "1";
								if ($unit_price>0)
									$purchase_price = $unit_price * $quantity;
								else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
	                            	$purchase_price = 0;
	                            }
	                            
								$totalPurchasePrice +=$purchase_price;
								
								if ($purchase_date=="0000-00-00")
									$purchase_date = "";
								else
									$purchase_date = " " . $purchase_date;
								
									/*
								$gov_funding = "0";
								$school_funding = "0";
								$sponsor_funding = "0";
								
								if($ShowFundingName)
								{
									$str_funding_name = " (".$funding_name.")";
								}
								if($purchase_price==0)	$str_funding_name="";
								
								if($funding_type == ITEM_SCHOOL_FUNDING)
								{
									$school_funding = $purchase_price.$str_funding_name;
								}
								if($funding_type == ITEM_GOVERNMENT_FUNDING)
								{
									$gov_funding = $purchase_price.$str_funding_name;
								}
								if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
								{
									$sponsor_funding = $purchase_price.$str_funding_name;
								}
								*/
								$Cost_Ary = array();
								$Funding_ary = array();
								$Cost_Ary[$funding_type] += $unit_price1;
								if($ShowFundingName)
									$Funding_ary[$funding_type] .= ($Funding_ary[$funding_type] ? ", ":"") . $funding_name;
								if($funding_type2)
								{
									$Cost_Ary[$funding_type2] += $unit_price2;
									if($ShowFundingName)
										$Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", ":"") . $funding_name2;
								}
									
								$school_funding = number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2).($Funding_ary[ITEM_SCHOOL_FUNDING]? " (".$Funding_ary[ITEM_SCHOOL_FUNDING].")":"");
								$gov_funding = number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2).($Funding_ary[ITEM_GOVERNMENT_FUNDING]? " (".$Funding_ary[ITEM_GOVERNMENT_FUNDING].")":"");
								$sponsor_funding = number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2).($Funding_ary[ITEM_SPONSORING_BODY_FUNDING]? " (".$Funding_ary[ITEM_SPONSORING_BODY_FUNDING].")":"");
							
								if($write_off_date != "")
								{
									if($write_off_reason != "")
									{
										$write_off_date_reason = $write_off_date." (".$write_off_reason.")";
									}
									else
									{
										$write_off_date_reason = $write_off_date;
									}
								}
								else
								{
									$write_off_date_reason = "";
								}
							}
						}		
						
						# S/N, Quotation No, Supplier, Tender No., Invoice No.
						$sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID=$item_id";
						$result2 = $linventory->returnArray($sql);
						list($thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $result2[0];					

						//$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$purchase_date\",\"$school_funding\",\"$gov_funding\",\"$sponsor_funding\",\"$unit_price\",\"$purchase_price\",\"$quantity\",\"$location_name\",\"$write_off_date_reason\",\"\",\"\"\n";
						$temprows = array($item_category, $item_subcategory,$item_code,$item_name,$item_description,$purchase_date,$school_funding,$gov_funding,$sponsor_funding,$unit_price,$purchase_price,$quantity,$location_name,
										$thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo, $write_off_date_reason,"",$remark);
						$rows[] = $temprows;
					}
					if($item_type == ITEM_TYPE_BULK)
					{
						if (!$display_not_req_stocktaking) {
							$sql = "SELECT ItemID, StockTakeOption FROM INVENTORY_ITEM WHERE ItemID='".$item_id."'";
							$rs = $linventory->returnResultSet($sql);
							$rs = $linventory->FilterForStockTakeBulk($rs);
							if (!count($rs)) {
								continue;
							}
						}					
						
						$final_purchase_date = "";
						
						### GET PURCHASE DATE ###
						if($PurchaseDateFrom && $PurchaseDateTo)
						{
						    $purchase_date_sql = " and (PurchaseDate>='". $PurchaseDateFrom ."' and PurchaseDate<='". $PurchaseDateTo ."')";
						}
						if($PurchaseDateFrom && !$PurchaseDateTo)
						{
						    $purchase_date_sql = " and (PurchaseDate>='". $PurchaseDateFrom ."')" ;
						}
						if(!$PurchaseDateFrom && $PurchaseDateTo)
						{
						    $purchase_date_sql = " and (PurchaseDate<='". $PurchaseDateTo ."') ";
						}
						$sql = "SELECT
										IF(PurchaseDate IS NULL, '', PurchaseDate)
								FROM
										INVENTORY_ITEM_BULK_LOG
								WHERE
										ItemID = '".$item_id."' AND
										Action = 1 
										and (RecordStatus=0 or RecordStatus is NULL) 
                                        $purchase_date_sql
								";
						$arr_purchase_date = $linventory->returnArray($sql,1);
						
						$tmp_arr_final_purchase_date = array();
						if(sizeof($arr_purchase_date)>0)
						{
							for($j=0; $j<sizeof($arr_purchase_date); $j++)
							{
								list($purchase_date) = $arr_purchase_date[$j];
								if($purchase_date != ''){
									//$final_purchase_date = $final_purchase_date."\r\n".$purchase_date;
									$tmp_arr_final_purchase_date[] = " " .$purchase_date;
								}else{
									//$final_purchase_date = ' ';
								}
							}
							if(sizeof($tmp_arr_final_purchase_date)>0){
								$final_purchase_date = implode("\r\n",$tmp_arr_final_purchase_date);
							}
						}
						else
						{
							$final_purchase_date = ' ';
						}
						
						### GET COST, UNIT PRICE, PURCHASED PRICE ###
						$total_school_cost = 0;
						$total_gov_cost = 0;
						$final_unit_price = 0;
						$final_purchase_price = 0;
						$total_qty = 0;
						$tmp_unit_price = 0;
						$tmp_purchase_price = 0;
						$written_off_qty = 0;
						$location_qty = 0;
						$ini_total_qty = 0;
						
						## get Total purchase price ##
						$count = $linventory->getNumOfBulkItemInvoice($item_id);
						if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
						    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
						}else{
						    //$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
						    $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
						}

						$tmp_result = $linventory->returnVector($sql);
						if(sizeof($tmp_result)>0){
							$total_purchase_price = $tmp_result[0];
							# [Case#B70033]
							if($total_purchase_price == 0){
								$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
								$tmp_result = $linventory->returnVector($sql);
								if($tmp_result[0] > 0)
									$total_purchase_price = $tmp_result[0];
							}
						}
						
						$sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
						$tmp_result = $linventory->returnVector($sql);
						if(sizeof($tmp_result)>0){
							$item_remark = implode(";\r\n", array_unique($tmp_result));
						}
					
						## get Total Qty Of the target Item ##
						$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."'";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($totalQty) = $tmp_result[0];
						}
						## get written-off qty ##
						$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."'";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($written_off_qty) = $tmp_result[0];
						}
						
						## get the qty in the target location ##
						/*
						$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($location_qty) = $tmp_result[0];
						}
						*/
						
						/*
						$sql = "SELECT 
										a.FundingSource, 
										b.FundingType,
										".$linventory->getInventoryNameByLang("b.")."
								FROM 
										INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
										INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
								WHERE 
										a.ItemID = $item_id AND
										a.Action IN (1,7) AND
										a.FundingSource IS NOT NULL
								";
						$tmp_result = $linventory->returnArray($sql,3);
						if(sizeof($tmp_result)>0){
							list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
						}
						*/
						
						### Get Qty By Location ###
						// added main buliding name [Case#D71480]
						$sql = "SELECT 
										a.Quantity,
										a.LocationID,
										CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
										f.FundingType,
										". $linventory->getInventoryNameByLang("f.") ." as funding_name,
										a.FundingSourceID
								FROM
										INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
										INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
										INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
										INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID) 
										inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
								WHERE
										a.ItemID = '".$item_id."'
								";
						$arr_tmp_qty = $linventory->returnArray($sql);
						
						if(sizeof($arr_tmp_qty)>0)
						{
							$display_qty = 0;
							$qty = 0;
							$display_location_name = "";
							$tmp_arr_display_location_name = array();
							
							unset($location_showed);
							$location_showed = array();
						
							for($j=0; $j<sizeof($arr_tmp_qty); $j++)
							{
								list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
								if($qty != "")
								{
									$display_qty = $display_qty + $qty;
								}
								if($location_name != "" && !$location_showed[$location_id])
								{
									//$display_location_name = $display_location_name."\r\n".$location_name;
									$tmp_arr_display_location_name[] = $location_name;
									$location_showed[$location_id] = true;
								}
							}
							if(sizeof($tmp_arr_display_location_name)>0){
								$display_location_name = implode("\r\n",$tmp_arr_display_location_name);
							}
						}
						
						$ini_total_qty = $totalQty + $written_off_qty;
						if ($ini_total_qty>0)
							$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
						
						$count = $linventory->getNumOfBulkItemInvoice($item_id);
						if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
						    $current_total = $total_purchase_price;
						}else{
						    $current_total = $final_unit_price * $display_qty;
						}
						
						$totalPurchasePrice +=$current_total;
						
						if ($FilterBulkItemPriceFrom!=null || $FilterBulkItemPriceEnd!=null)
						{
							if ($FilterBulkItemPriceFrom!=null && $current_total<$FilterBulkItemPriceFrom)
							{
								$rows_shown --;
								continue;
							} elseif ($FilterBulkItemPriceEnd!=null && $current_total>$FilterBulkItemPriceEnd)
							{
								$rows_shown --;
								continue;
							}
						}
						
						$final_purchase_price = number_format($final_unit_price * $ini_total_qty,2);

						# retrieve funding data
						$tmp_funding_src = array();
						for($j=0; $j<sizeof($arr_tmp_qty); $j++)
						{
							list($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
							# [Case#V72270]
							## Location origial qty (by location only)##
							if($display_writeoff){
								$sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND FundingSourceID = '".$fund_id."'";
								$tmp_result = $linventory->returnVector($sql);
								if(sizeof($tmp_result)>0 && $j==0){
									$qty = $tmp_result[0] + $qty;
								}
							}
							$count = $linventory->getNumOfBulkItemInvoice($item_id);
							if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
							    $tmp_funding_src[$funding_type][$fund_id]['cost'] = $total_purchase_price;
							}else{
							    $tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
							}

							$tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
							$tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
						}
						
						$this_field_display = array();
						$this_final_total_cost = 0;
						for($type_i=1;$type_i<=3;$type_i++)
						{
							$this_field = "";
							if(!empty($tmp_funding_src[$type_i]))
							{
								$this_fund_cost = 0;
								foreach($tmp_funding_src[$type_i] as $k=>$d)
								{
									if($ShowFundingName)
									{
										$this_fund_cost = number_format($d['cost'],2);
										//$this_fund_src = ($d['cost']>0) ? " (". $d['src_name'].")" : "";
										$this_fund_src = " (". $d['src_name'].")";
										$this_field .= ($this_field ? "; " : "") . $this_fund_cost . $this_fund_src;
									}
									else
									{
										$this_fund_cost += $d['cost'];
										$this_field = number_format($this_fund_cost,2);
									}
									
									$this_final_total_cost = $this_fund_cost;
								}
							}
							$this_field_display[$type_i] = $this_field ? $this_field : "0.00";
							
							//$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
						}
						/*
						if($funding_type == ITEM_SCHOOL_FUNDING)
						{	
							$final_purchase_price = $total_purchase_price;
							$ini_total_qty = $totalQty + $written_off_qty;
							if ($ini_total_qty>0)
								$final_unit_price =  round($total_purchase_price/$ini_total_qty,2);
							//$total_school_cost = $ini_total_qty*($total_purchase_price/$ini_total_qty);
							$total_school_cost = round($final_unit_price * $display_qty, 2);
							if($ShowFundingName){
								$str_funding_name = " (".$funding_name.")";
								$total_school_cost .= $str_funding_name;
							}
							$final_purchase_price = $total_school_cost;
						}
						if($funding_type == ITEM_GOVERNMENT_FUNDING)
						{
							$final_purchase_price = $total_purchase_price;
							$ini_total_qty = $totalQty + $written_off_qty;
							if ($ini_total_qty>0)
								$final_unit_price =  round($total_purchase_price/$ini_total_qty,2);
							//$total_gov_cost = $ini_total_qty*($total_purchase_price/$ini_total_qty);
							$total_gov_cost = round($final_unit_price * $display_qty, 2);
							if($ShowFundingName){
								$str_funding_name = " (".$funding_name.")";
								$total_gov_cost .= $str_funding_name;
							}
							$final_purchase_price = $total_gov_cost;
						}
						*/
						
						$final_unit_price = round($final_unit_price, 2);
						
						### Get Write-Off Date & Reason ###
						$display_write_off = "";
						
						$sql = "SELECT
										ApproveDate,
										WriteOffQty,
										WriteOffReason
								FROM
										INVENTORY_ITEM_WRITE_OFF_RECORD
								WHERE
										ItemID = '".$item_id."' AND RecordStatus = 1
								";
						$arr_write_off = $linventory->returnArray($sql,2);
						
						if(sizeof($arr_write_off)>0)
						{
							$tmp_write_off = array();
							for($j=0; $j<sizeof($arr_write_off); $j++)
							{
								list($write_off_date,$write_off_qty,$write_off_reason) = $arr_write_off[$j];
								if($write_off_date != "")
								{
									//$tmp_write_off = $write_off_date." ".$write_off_reason;
									//$display_write_off = $display_write_off." ".$tmp_write_off;
									$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
									$tmp_write_off[] = $write_off_date."\r\n".$write_off_qty."\r\n".$write_off_reason;
								}
							}
	
							if(sizeof($tmp_write_off)>0){
								for($k=0; $k<sizeof($tmp_write_off);$k++){
									$str_reason = $tmp_write_off[$k];
									$display_write_off .= $str_reason;
									if($k!=0){
										$display_write_off .= "\r\n";
									}
								}
							}
						}
						else
						{
							$display_write_off = "";
						}
						
						# Quotation No, Supplier, Tender No., Invoice No.	
						$sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID='".$item_id."'";
						$result2 = $linventory->returnArray($sql);
						if(!empty($result2))
						{
							$thisDataAry = array();
							
							foreach($result2 as $k2=>$d2)
							{
								list($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
								if($thisQuotationNo)
									$thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
								if($thisSupplierName)
									$thisDataAry['Supplier'][] = trim($thisSupplierName);
								if($thisTenderNo)
									$thisDataAry['TenderNo'][] = trim($thisTenderNo);
								if($thisInvoiceNo)
									$thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
							}
						}
						
						$QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ",array_unique($thisDataAry['QuotationNo'])) : "";
						$SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ",array_unique($thisDataAry['Supplier'])) : "";
						$TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ",array_unique($thisDataAry['TenderNo'])) : "";
						$InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ",array_unique($thisDataAry['InvoiceNo'])) : "";
						
						
// 						$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$final_purchase_date\",\"". $this_field_display[1] ."\",\"". $this_field_display[2] ."\",\"". $this_field_display[3] ."\",\"$final_purchase_price\",\"$display_qty\",\"$display_location_name\",\"$display_write_off\"\n";
// 						$temprows = array($item_code,$item_name,$item_description,$final_purchase_date,$this_field_display[1],$this_field_display[2],$this_field_display[3],$final_unit_price,$final_purchase_price,$display_qty,$display_location_name,$display_write_off);
						//$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$final_purchase_date\",\"". $this_field_display[1] ."\",\"". $this_field_display[2] ."\",\"". $this_field_display[3] ."\",\"$this_final_total_cost\",\"$display_qty\",\"$display_location_name\",\"$display_write_off\"\n";
						$temprows = array($item_category, $item_subcategory,$item_code,$item_name,$item_description,$final_purchase_date,$this_field_display[1],$this_field_display[2],$this_field_display[3],$final_unit_price,$current_total,$display_qty,$display_location_name,
										$QnoNoStr, $SupplierStr, $TenderNoStr, $InvoiceNoStr,$display_write_off,"",$item_remark);
						$rows[] = $temprows;
					}
				}
				if($ShowPurchasePriceTotal)
					$rows[] = array('', '', '','','','','','','',$Lang['eInventory']['TotalAssets'],$totalPurchasePrice,'','','', '', '', '', '', '','','');
			}
			if(sizeof($arr_result)==0)
			{
				$file_content .= "\"$i_no_record_exists_msg\"\n";
				$rows[] = array($i_no_record_exists_msg);
			}
			$file_content .= "\n";
			$file_content .= "\n";
			$rows[] = array("\n");
			$rows[] = array("\n");
		}
	}
}
else if($groupBy==3)
{
	# get all location #
	$group_namefield = $linventory->getInventoryNameByLang();

	$sql = "SELECT AdminGroupID, $group_namefield as GroupName  FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
	$group_array = $linventory->returnArray($sql,2);
		
	if (is_array($group_array))
	{
		foreach($group_array as $Key=>$Value)
		{
			if($_POST['group_cb'.$Value['AdminGroupID']])
			{
				$selected_cb .= $Value['AdminGroupID'].",";
			}
		}
	}

	//$selected_cb = substr($selected_cb,0,-1);
	$arrTempTargetGroup = $targetGroup;
	if(array_shift($arrTempTargetGroup) == "")
		$targetGroup = $arrTempTargetGroup;
	$selected_cb = implode(",",$targetGroup);

	$cond = "WHERE AdminGroupID in (".$selected_cb.")";
	$sql="Select DISTINCT(".$linventory->getInventoryItemNameByLang().") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
		$arr_group = $linventory->returnArray($sql,2);
	
	for($a=0; $a<sizeof($arr_group); $a++)
	{	
		list($group_name,$admin_group_id) = $arr_group[$a];
		
// 		$file_content .= "\"".$i_InventorySystem['Caretaker']."\",\"$group_name\"\n";
// 		$file_content .= "\"\",\"\",\"\",\"\",\"$i_InventorySystem_Report_Col_Cost\",\"$i_InventorySystem_Report_Col_Cost\"\n";
// 		//$file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Report_Col_Item\",\"$i_InventorySystem_Report_Col_Description\",\"$i_InventorySystem_Report_Col_Purchase_Date\",\"$i_InventorySystem_Report_Col_Govt_Fund\",\"$i_InventorySystem_Report_Col_Sch_Fund\",\"$i_InventorySystem_Report_Col_Sch_Unit_Price\",\"$i_InventorySystem_Report_Col_Sch_Purchase_Price\",\"$i_InventorySystem_Report_Col_Quantity\",\"$i_InventorySystem_Report_Col_Location\",\"$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off\",\"$i_InventorySystem_Report_Col_Signature\",\"$i_InventorySystem_Report_Col_Remarks\"\n";
// 		$file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Report_Col_Item\",\"$i_InventorySystem_Report_Col_Description\",\"$i_InventorySystem_Report_Col_Purchase_Date\",\"". $Lang['eInventory']['Ownership']['Government'] ."\",\"". $Lang['eInventory']['Ownership']['School'] ."\",\"$i_InventorySystem_Report_Col_Sch_Unit_Price\",\"".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."\",\"$i_InventorySystem_Report_Col_Quantity\",\"$i_InventorySystem_Report_Col_Location\",\"$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off\",\"$i_InventorySystem_Report_Col_Signature\",\"$i_InventorySystem_Report_Col_Remarks\"\n";
		
		//$temprows = array($i_InventorySystem_Item_Location,$location_name);
		$temprows = array($i_InventorySystem['Caretaker'],$group_name);
		$rows[] = $temprows;
		$temprows = array('','','','','','',$i_InventorySystem_Report_Col_Cost,$i_InventorySystem_Report_Col_Cost, $i_InventorySystem_Report_Col_Cost);
		$rows[] = $temprows;
		//$temprows = array($i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$i_InventorySystem_Report_Col_Govt_Fund,$i_InventorySystem_Report_Col_Sch_Fund,$i_InventorySystem_Report_Col_Sch_Unit_Price,$i_InventorySystem_Report_Col_Sch_Purchase_Price,$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location,$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
		$temprows = array($i_InventorySystem['Category'], $i_InventorySystem['SubCategory'], $i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$Lang['eInventory']['FundingType']['School'],$Lang['eInventory']['FundingType']['Government'],$Lang['eInventory']['FundingType']['SponsoringBody'],$i_InventorySystem_Report_Col_Sch_Unit_Price,$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'],$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location,$i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Quot_Num, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num,$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
		$rows[] = $temprows;
				
		if(!$display_writeoff)
		{
			$wo_con = " and a.RecordStatus=1 ";
			$wo_conb = " and c.Quantity>0";
		}
		
// 		$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or (e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";
		if($PurchaseDateFrom && $PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or 
										(e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";
			}
			if($PurchaseDateFrom && !$PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."') or 
										(e.PurchaseDate>='". $PurchaseDateFrom ."'))";
			}
			if(!$PurchaseDateFrom && $PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate<='". $PurchaseDateTo ."') or 
										(e.PurchaseDate<='". $PurchaseDateTo ."'))";
			}
			
		$sql = "SELECT 
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					".$linventory->getInventoryNameByLang("a.").",
					".$linventory->getInventoryDescriptionNameByLang("a.").",
					".$linventory->getInventoryNameByLang("c1.").",
					".$linventory->getInventoryNameByLang("c2.")."
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID)
					left outer join INVENTORY_ITEM_BULK_LOG as e on (e.ItemID=a.ItemID)
					LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
					LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
				WHERE
					d.AdminGroupID IN ($admin_group_id)
					$purchase_date_sql
					$wo_con
				ORDER BY
					a.ItemCode
				";
		$arr_result = $linventory->returnArray($sql);
		
		$totalPurchasePrice = 0;
		
		if(sizeof($arr_result)>0)
		{
			for($i=0; $i<sizeof($arr_result); $i++)
			{
				list($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];
				
				# filter out no stock-taken item if necessary
				if ($item_type==ITEM_TYPE_SINGLE && is_array($StocktakeResultArray) && !in_array($item_id, $StocktakeResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				} elseif ($item_type==ITEM_TYPE_BULK && is_array($StocktakeBulkResultArray) && !in_array($item_id, $StocktakeBulkResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				}
				
				if($item_type == ITEM_TYPE_SINGLE)
				{
					// added main buliding name [Case#D71480]
					$sql = "SELECT
									a.PurchaseDate, 
									if(a.PurchasedPrice != '',a.PurchasedPrice,'0') as PurchasedPrice,
									if(a.UnitPrice != '',a.UnitPrice,'0') as UnitPrice,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									d.FundingType,
									".$linventory->getInventoryNameByLang("d.").",
									e.ApproveDate,
									e.WriteOffReason,
									a.ItemRemark,
									".$linventory->getInventoryNameByLang("d2.").",
									a.UnitPrice1,
									a.UnitPrice2,
									d2.FundingType as FundingType2,
									i.StockTakeOption
							FROM
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
									INVENTORY_ITEM i ON (i.ItemID=a.ItemID AND i.ItemType=1) INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_FUNDING_SOURCE AS d2 ON (a.FundingSource2 = d2.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
							WHERE
									a.ItemID = '".$item_id."'
							"; 
					$arr_sub_result = $linventory->returnArray($sql);

					if (!$display_not_req_stocktaking) { 
						$rs =  $linventory->FilterForStockTakeSingle($arr_sub_result);
						if (!count($rs)) {
							continue;
						}
					}
					
					if ($FilterSingleItemPriceFrom!=null || $FilterSingleItemPriceEnd!=null)
					{
						list($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark, $this_item_id, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[0];
						if ($FilterSingleItemPriceFrom!=null && $unit_price<$FilterSingleItemPriceFrom)
						{
							continue;
						} elseif ($FilterSingleItemPriceEnd!=null && $unit_price>$FilterSingleItemPriceEnd)
						{
							continue;
						}
					}
					
					if(sizeof($arr_sub_result)>0)
					{
						for($j=0; $j<sizeof($arr_sub_result); $j++)
						{
							list($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[$j];
// 							$quantity = 1;
							$quantity = $write_off_date ? "0" : "1";
							if ($unit_price>0)
								$purchase_price = $unit_price * $quantity;
							else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
                            	$purchase_price = 0;
                            }
                            
							$totalPurchasePrice +=$purchase_price;
							
							if ($purchase_date=="0000-00-00")
								$purchase_date = "";
							else
								$purchase_date = " " . $purchase_date;
								
							/*
							$gov_funding = "0";
							$school_funding = "0";	
							$sponsor_funding = "0";
							if($ShowFundingName)
							{
								$str_funding_name = " (".$funding_name.")";
							}
							if($purchase_price==0)	$str_funding_name="";
							if($funding_type == ITEM_SCHOOL_FUNDING)
							{
								$school_funding = $purchase_price.$str_funding_name;
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{
								$gov_funding = $purchase_price.$str_funding_name ;
							}
							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
							{
								$sponsor_funding = $purchase_price.$str_funding_name ;
							}
							*/
							
							$Cost_Ary = array();
							$Funding_ary = array();
							$Cost_Ary[$funding_type] += $unit_price1;
							if($ShowFundingName)
								$Funding_ary[$funding_type] .= ($Funding_ary[$funding_type] ? ", ":"") . $funding_name;
							if($funding_type2)
							{
								$Cost_Ary[$funding_type2] += $unit_price2;
								if($ShowFundingName)
									$Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", ":"") . $funding_name2;
							}
								
							$school_funding = number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2).($Funding_ary[ITEM_SCHOOL_FUNDING]? " (".$Funding_ary[ITEM_SCHOOL_FUNDING].")":"");
							$gov_funding = number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2).($Funding_ary[ITEM_GOVERNMENT_FUNDING]? " (".$Funding_ary[ITEM_GOVERNMENT_FUNDING].")":"");
							$sponsor_funding = number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2).($Funding_ary[ITEM_SPONSORING_BODY_FUNDING]? " (".$Funding_ary[ITEM_SPONSORING_BODY_FUNDING].")":"");
							
							if($write_off_date != "")
							{
								if($write_off_reason != "")
								{
									$write_off_date_reason = $write_off_date." (".$write_off_reason.")";
								}
								else
								{
									$write_off_date_reason = $write_off_date;
								}
							}
							else
							{
								$write_off_date_reason = "";
							}
						}
					}	
					
					# S/N, Quotation No, Supplier, Tender No., Invoice No.
					$sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID=$item_id";
					$result2 = $linventory->returnArray($sql);
					list($thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $result2[0];	
					
// 					$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$purchase_date\",\"$school_funding\",\"$gov_funding\",\"$sponsor_funding\",\"$unit_price\",\"$purchase_price\",\"$quantity\",\"$location_name\",\"$write_off_date_reason\",\"\",\"\"\n";
					$temprows = array($item_category, $item_subcategory, $item_code,$item_name,$item_description,$purchase_date,$school_funding,$gov_funding,$sponsor_funding,$unit_price,$purchase_price,$quantity,$location_name,
										$thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo, $write_off_date_reason,"",$remark);
					$rows[] = $temprows;
				}
				if($item_type == ITEM_TYPE_BULK)
				{
					if (!$display_not_req_stocktaking) {
						$sql = "SELECT ItemID, StockTakeOption FROM INVENTORY_ITEM WHERE ItemID='".$item_id."'";
						$rs = $linventory->returnResultSet($sql);
						$rs = $linventory->FilterForStockTakeBulk($rs);
						if (!count($rs)) {
							continue;
						}
					}					
					
					$final_purchase_date = "";
					
					### GET PURCHASE DATE ###
					if($PurchaseDateFrom && $PurchaseDateTo)
					{
					    $purchase_date_sql = " and (PurchaseDate>='". $PurchaseDateFrom ."' and PurchaseDate<='". $PurchaseDateTo ."')";
					}
					if($PurchaseDateFrom && !$PurchaseDateTo)
					{
					    $purchase_date_sql = " and (PurchaseDate>='". $PurchaseDateFrom ."')" ;
					}
					if(!$PurchaseDateFrom && $PurchaseDateTo)
					{
					    $purchase_date_sql = " and (PurchaseDate<='". $PurchaseDateTo ."') ";
					}
					$sql = "SELECT
									IF(PurchaseDate IS NULL, '', PurchaseDate)
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									ItemID = '".$item_id."' AND
									Action = 1
									and (RecordStatus=0 or RecordStatus is NULL) 
                                    $purchase_date_sql
							";
							 /*AND
									GroupInCharge = $admin_group_id*/
					$arr_purchase_date = $linventory->returnArray($sql,1);
					
					$tmp_arr_purchase_date = array();
					if(sizeof($arr_purchase_date)>0)
					{
						for($j=0; $j<sizeof($arr_purchase_date); $j++)
						{
							list($purchase_date) = $arr_purchase_date[$j];
							
							if($purchase_date != ''){
								//$final_purchase_date = $final_purchase_date."\r\n".$purchase_date;
								$tmp_arr_purchase_date[] = " " .$purchase_date;
							}
							else{
								//$final_purchase_date = ' ';
							}
						}
						if(sizeof($tmp_arr_purchase_date)>0){
							$final_purchase_date = implode("\r\n",$tmp_arr_purchase_date);
						}
						
					}
					else
					{
						$final_purchase_date = ' ';
					}
					
					### GET COST, UNIT PRICE, PURCHASED PRICE ###
					$total_school_cost = 0;
					$total_gov_cost = 0;
					$final_unit_price = 0;
					$final_purchase_price = 0;
					$total_qty = 0;
					$tmp_unit_price = 0;
					$tmp_purchase_price = 0;
					$written_off_qty = 0;
					$location_qty = 0;
					$ini_total_qty = 0;
					
					## get Total purchase price ##
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}else{
					    //$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					    $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}

					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$total_purchase_price = $tmp_result[0];
						# [Case#B70033]
						if($total_purchase_price == 0){
							$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
							$tmp_result = $linventory->returnVector($sql);
							if($tmp_result[0] > 0)
								$total_purchase_price = $tmp_result[0];
						}
					}
					
					$sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$item_remark = implode(";\r\n", array_unique($tmp_result));
					}
					
					## get Total Qty Of the target Item ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($totalQty) = $tmp_result[0];
					}
					## get written-off qty ##
					$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($written_off_qty) = $tmp_result[0];
					}
					
					## get the qty in the target location ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."' AND GroupInCharge = '".$admin_group_id."'";
					$tmp_result = $linventory->returnArray($sql,1);
					if(sizeof($tmp_result)>0){
						list($location_qty) = $tmp_result[0];
					}
					
					## Location origial qty (by location only)##
					$sql = "SELECT WriteOffQty FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND AdminGroupID = '".$admin_group_id."'";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$ini_location_qty = $tmp_result[0] + $location_qty;
					}else{
						$ini_location_qty = $location_qty;
					}
						
					/*
					$sql = "SELECT 
									a.FundingSource, 
									b.FundingType,
									".$linventory->getInventoryNameByLang("b.")."
							FROM 
									INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
									INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
							WHERE 
									a.ItemID = $item_id AND
									a.Action IN (1,7) AND
									a.FundingSource IS NOT NULL
							";
					$tmp_result = $linventory->returnArray($sql,3);
					if(sizeof($tmp_result)>0){
						list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
					}
					*/
					
					### Get Qty By Location ###
					$sql = "SELECT 
									a.Quantity,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									f.FundingType,
									". $linventory->getInventoryNameByLang("f.") ." as funding_name,
									a.FundingSourceID
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN 
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID)
									inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
							WHERE
									a.ItemID = '".$item_id."' AND 
									a.GroupInCharge = '".$admin_group_id."'
							";
					$arr_tmp_qty = $linventory->returnArray($sql);
					if(sizeof($arr_tmp_qty)>0)
					{
						$display_qty = 0;
						$qty = 0;
						$display_location_name = "";
						$tmp_arr_display_location_name = array();
						unset($location_showed);
						$location_showed = array();
						
						for($j=0; $j<sizeof($arr_tmp_qty); $j++)
						{
							list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
							if($qty != "")
							{
								$display_qty = $display_qty + $qty;
							}
							if($location_name != "" && !$location_showed[$location_id])
							{
								//$display_location_name = $display_location_name."\r\n".$location_name;
								$tmp_arr_display_location_name[] = $location_name;
								$location_showed[$location_id] = true;
							}
						}
						if(sizeof($tmp_arr_display_location_name)>0){
							$display_location_name = implode("\r\n",$tmp_arr_display_location_name);
						}
					}
					
					$ini_total_qty = $totalQty + $written_off_qty;
					if ($ini_total_qty>0)
						$final_unit_price =  round($total_purchase_price/$ini_total_qty,2);
					
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $current_total = $total_purchase_price;
					}else{
					   $current_total = $final_unit_price * $display_qty;
					}
					
					$totalPurchasePrice +=$current_total;
					
					if ($FilterBulkItemPriceFrom!=null || $FilterBulkItemPriceEnd!=null)
					{
						if ($FilterBulkItemPriceFrom!=null && $current_total<$FilterBulkItemPriceFrom)
						{
							$rows_shown --;
							continue;
						} elseif ($FilterBulkItemPriceEnd!=null && $current_total>$FilterBulkItemPriceEnd)
						{
							$rows_shown --;
							continue;
						}
					}
					
					$final_purchase_price = number_format($final_unit_price * $ini_total_qty,2);
							
					# retrieve funding data
					$tmp_funding_src = array();
					for($j=0; $j<sizeof($arr_tmp_qty); $j++)
					{
						list($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
						# [Case#V72270]
						## Location origial qty (by location only)##
						if($display_writeoff){
							$sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND FundingSourceID = '".$fund_id."'";
							$tmp_result = $linventory->returnVector($sql);
							if(sizeof($tmp_result)>0 && $j==0){
								$qty = $tmp_result[0] + $qty;
							}
						}
						$count = $linventory->getNumOfBulkItemInvoice($item_id);
						if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
						    $tmp_funding_src[$funding_type][$fund_id]['cost'] = $total_purchase_price;
						}else{
						    $tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
						}

						$tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
						$tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
					}

					$this_field_display = array();
					$this_final_total_cost = 0;
					for($type_i=1;$type_i<=3;$type_i++)
					{
						$this_field = "";
						if(!empty($tmp_funding_src[$type_i]))
						{
							$this_fund_cost = 0;
							foreach($tmp_funding_src[$type_i] as $k=>$d)
							{
								if($ShowFundingName)
								{
									$this_fund_cost = number_format($d['cost'],2);
									//$this_fund_src = ($d['cost']>0) ? " (". $d['src_name'].")" : "";
									$this_fund_src = " (". $d['src_name'].")";
									$this_field .= ($this_field ? "; " : "") . $this_fund_cost . $this_fund_src;
									
								}
								else
								{
									$this_fund_cost += $d['cost'];
									$this_field = number_format($this_fund_cost,2);
								}
								$this_final_total_cost = $this_fund_cost;
							}
						}
						$this_field_display[$type_i] = $this_field ? $this_field : "0.00";
						
						//$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
					}
/*
					if($funding_type == ITEM_SCHOOL_FUNDING)
					{
						$ini_total_qty = $totalQty + $written_off_qty;
						if ($ini_total_qty>0)
							$final_unit_price =  round($total_purchase_price/$ini_total_qty,2);
						//$total_school_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
						$total_school_cost = round($final_unit_price*$display_qty, 2);
						if($ShowFundingName){
							$str_funding_name = " (".$funding_name.")";
							$total_school_cost .= $str_funding_name;
						}
						$final_purchase_price = $total_school_cost;
					}
					if($funding_type == ITEM_GOVERNMENT_FUNDING)
					{	
						$ini_total_qty = $totalQty + $written_off_qty;
						if ($ini_total_qty>0)
							$final_unit_price =  round($total_purchase_price/$ini_total_qty,2);
						//$total_gov_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
						$total_gov_cost = round($final_unit_price*$display_qty, 2);
						if($ShowFundingName){
							$str_funding_name = " (".$funding_name.")";
							$total_gov_cost .= $str_funding_name;
						}
						$final_purchase_price = $total_gov_cost;
					}
					*/
					$final_unit_price = round($final_unit_price, 2);
					
					
					$display_write_off = "";
					
					$sql = "SELECT
									ApproveDate,
									WriteOffQty,
									WriteOffReason
							FROM
									INVENTORY_ITEM_WRITE_OFF_RECORD
							WHERE
									ItemID = $item_id AND 
									AdminGroupID = $admin_group_id
									 AND RecordStatus = 1
							";
					
					$arr_write_off = $linventory->returnArray($sql,3);
					
					if(sizeof($arr_write_off)>0)
					{
						$tmp_write_off = array();
						for($j=0; $j<sizeof($arr_write_off); $j++)
						{
							list($write_off_date,$write_off_qty,$write_off_reason) = $arr_write_off[$j];
							if($write_off_date != "")
							{
								$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
								$tmp_write_off[] = $write_off_date."\r\n".$write_off_qty."\r\n".$write_off_reason;
							}
						}
						
						if(sizeof($tmp_write_off)>0){
							for($k=0; $k<sizeof($tmp_write_off);$k++){
								$str_reason = $tmp_write_off[$k];
								$display_write_off .= $str_reason;
								if($k!=0){
									$display_write_off .= "\r\n";
								}
							}
						}
					}
					else
					{
						$display_write_off = "";
					}
					
					# Quotation No, Supplier, Tender No., Invoice No.	
					$sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID=$item_id";
					$result2 = $linventory->returnArray($sql);
					if(!empty($result2))
					{
						$thisDataAry = array();
						
						foreach($result2 as $k2=>$d2)
						{
							list($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
							if($thisQuotationNo)
								$thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
							if($thisSupplierName)
								$thisDataAry['Supplier'][] = trim($thisSupplierName);
							if($thisTenderNo)
								$thisDataAry['TenderNo'][] = trim($thisTenderNo);
							if($thisInvoiceNo)
								$thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
						}
					}
					
					$SNStr = "";
					$QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ",array_unique($thisDataAry['QuotationNo'])) : "";
					$SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ",array_unique($thisDataAry['Supplier'])) : "";
					$TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ",array_unique($thisDataAry['TenderNo'])) : "";
					$InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ",array_unique($thisDataAry['InvoiceNo'])) : "";
					
					
// 					$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$final_purchase_date\",\"". $this_field_display[1] ."\",\"". $this_field_display[2] ."\",\"". $this_field_display[3] ."\",\"$final_purchase_price\",\"$display_qty\",\"$display_location_name\",\"$display_write_off\"\n";
// 					$temprows = array($item_code,$item_name,$item_description,$final_purchase_date,$this_field_display[1],$this_field_display[2],$this_field_display[3],$final_unit_price,$final_purchase_price,$display_qty,$display_location_name,$display_write_off);
// 					$file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$final_purchase_date\",\"". $this_field_display[1] ."\",\"". $this_field_display[2] ."\",\"". $this_field_display[3] ."\",\"$this_final_total_cost\",\"$display_qty\",\"$display_location_name\",\"$display_write_off\"\n";
					$temprows = array($item_category, $item_subcategory, $item_code,$item_name,$item_description,$final_purchase_date,$this_field_display[1],$this_field_display[2],$this_field_display[3],$final_unit_price,$current_total,$display_qty,$display_location_name,
									$SNStr, $QnoNoStr, $SupplierStr, $TenderNoStr, $InvoiceNoStr, $display_write_off,"",$item_remark);
					
					$rows[] = $temprows;
				}
			}
			if($ShowPurchasePriceTotal)
				$rows[] = array('', '', '','','','','','','',$Lang['eInventory']['TotalAssets'],$totalPurchasePrice,'','','', '', '', '', '', '','','');
		}
		if(sizeof($arr_result)==0)
		{
			$file_content .= "\"$i_no_record_exists_msg\"\n";
			$rows[] = array($i_no_record_exists_msg);
		}
		$file_content .= "\n";
		$file_content .= "\n";
	}
}
else if ($groupBy == 4) {
    // get all location #
    $funding_source_namefield = $linventory->getInventoryNameByLang();

    $sql = "SELECT FundingSourceID, $funding_source_namefield FROM INVENTORY_FUNDING_SOURCE ORDER BY DisplayOrder";
    $funding_source_array = $linventory->returnArray($sql, 2);

    if (is_array($funding_source_array)) {
        foreach ($funding_source_array as $Key => $Value) {
            if ($_POST['funding_source_cb' . $Value['FundingSourceID']]) {
                $selected_cb .= $Value['FundingSourceID'] . ",";
            }
        }
    }

    // $selected_cb = substr($selected_cb,0,-1);
    $arrTempTargetFundSource = $targetFundSource;
    if (array_shift($arrTempTargetFundSource) == "")
        $targetFundSource = $arrTempTargetFundSource;
    $selected_cb = implode(",", $targetFundSource);

    $cond = "WHERE FundingSourceID in (" . $selected_cb . ")";
    $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . "), FundingSourceID FROM INVENTORY_FUNDING_SOURCE $cond ORDER BY DisplayOrder";
    $arr_funding_source = $linventory->returnArray($sql, 2);

    for ($a = 0; $a < sizeof($arr_funding_source); $a ++) {
        list ($funding_source_name, $funding_source_id) = $arr_funding_source[$a];

        // $file_content .= "\"".$i_InventorySystem['Caretaker']."\",\"$group_name\"\n";
        // $file_content .= "\"\",\"\",\"\",\"\",\"$i_InventorySystem_Report_Col_Cost\",\"$i_InventorySystem_Report_Col_Cost\"\n";
        // //$file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Report_Col_Item\",\"$i_InventorySystem_Report_Col_Description\",\"$i_InventorySystem_Report_Col_Purchase_Date\",\"$i_InventorySystem_Report_Col_Govt_Fund\",\"$i_InventorySystem_Report_Col_Sch_Fund\",\"$i_InventorySystem_Report_Col_Sch_Unit_Price\",\"$i_InventorySystem_Report_Col_Sch_Purchase_Price\",\"$i_InventorySystem_Report_Col_Quantity\",\"$i_InventorySystem_Report_Col_Location\",\"$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off\",\"$i_InventorySystem_Report_Col_Signature\",\"$i_InventorySystem_Report_Col_Remarks\"\n";
        // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Report_Col_Item\",\"$i_InventorySystem_Report_Col_Description\",\"$i_InventorySystem_Report_Col_Purchase_Date\",\"". $Lang['eInventory']['Ownership']['Government'] ."\",\"". $Lang['eInventory']['Ownership']['School'] ."\",\"$i_InventorySystem_Report_Col_Sch_Unit_Price\",\"".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."\",\"$i_InventorySystem_Report_Col_Quantity\",\"$i_InventorySystem_Report_Col_Location\",\"$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off\",\"$i_InventorySystem_Report_Col_Signature\",\"$i_InventorySystem_Report_Col_Remarks\"\n";

        // $temprows = array($i_InventorySystem_Item_Location,$location_name);
        $temprows = array(
            $i_InventorySystem['FundingSource'],
            $funding_source_name
        );
        $rows[] = $temprows;
        $temprows = array(
            '',
            '',
            '',
            '',
            '',
            '',
            $i_InventorySystem_Report_Col_Cost,
            $i_InventorySystem_Report_Col_Cost,
            $i_InventorySystem_Report_Col_Cost
        );
        $rows[] = $temprows;
        // $temprows = array($i_InventorySystem_Item_Code,$i_InventorySystem_Report_Col_Item,$i_InventorySystem_Report_Col_Description,$i_InventorySystem_Report_Col_Purchase_Date,$i_InventorySystem_Report_Col_Govt_Fund,$i_InventorySystem_Report_Col_Sch_Fund,$i_InventorySystem_Report_Col_Sch_Unit_Price,$i_InventorySystem_Report_Col_Sch_Purchase_Price,$i_InventorySystem_Report_Col_Quantity,$i_InventorySystem_Report_Col_Location,$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,$i_InventorySystem_Report_Col_Signature,$i_InventorySystem_Report_Col_Remarks);
        $temprows = array(
            $i_InventorySystem['Category'],
            $i_InventorySystem['SubCategory'],
            $i_InventorySystem_Item_Code,
            $i_InventorySystem_Report_Col_Item,
            $i_InventorySystem_Report_Col_Description,
            $i_InventorySystem_Report_Col_Purchase_Date,
            $Lang['eInventory']['FundingType']['School'],
            $Lang['eInventory']['FundingType']['Government'],
            $Lang['eInventory']['FundingType']['SponsoringBody'],
            $i_InventorySystem_Report_Col_Sch_Unit_Price,
            $Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'],
            $i_InventorySystem_Report_Col_Quantity,
            $i_InventorySystem_Report_Col_Location,
            $i_InventorySystem_Item_Serial_Num,
            $i_InventorySystem_Item_Quot_Num,
            $i_InventorySystem_Item_Supplier_Name,
            $i_InventorySystem_Item_Tender_Num,
            $i_InventorySystem_Item_Invoice_Num,
            $i_InventorySystem_Report_Col_Date_Reason_of_Write_Off,
            $i_InventorySystem_Report_Col_Signature,
            $i_InventorySystem_Report_Col_Remarks
        );
        $rows[] = $temprows;

        if (! $display_writeoff) {
            $wo_con = " and a.RecordStatus=1 ";
            $wo_conb = " and c.Quantity>0";
        }

        // $purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or (e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";
        if ($PurchaseDateFrom && $PurchaseDateTo) {
            $purchase_date_sql = " and ((b.PurchaseDate>='" . $PurchaseDateFrom . "' and b.PurchaseDate<='" . $PurchaseDateTo . "') or
										(e.PurchaseDate>='" . $PurchaseDateFrom . "' and e.PurchaseDate<='" . $PurchaseDateTo . "'))";
        }
        if ($PurchaseDateFrom && ! $PurchaseDateTo) {
            $purchase_date_sql = " and ((b.PurchaseDate>='" . $PurchaseDateFrom . "') or
										(e.PurchaseDate>='" . $PurchaseDateFrom . "'))";
        }
        if (! $PurchaseDateFrom && $PurchaseDateTo) {
            $purchase_date_sql = " and ((b.PurchaseDate<='" . $PurchaseDateTo . "') or
										(e.PurchaseDate<='" . $PurchaseDateTo . "'))";
        }

        $sql = "SELECT
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					" . $linventory->getInventoryNameByLang("a.") . ",
					" . $linventory->getInventoryDescriptionNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c1.") . ",
					" . $linventory->getInventoryNameByLang("c2.") . "
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID)
					left outer join INVENTORY_ITEM_BULK_LOG as e on (e.ItemID=a.ItemID)
					LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
					LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
                    LEFT JOIN INVENTORY_FUNDING_SOURCE as ifs on (ifs.FundingSourceID = b.FundingSource AND ifs.FundingSourceID = e.FundingSource)
				WHERE
					(b.FundingSource IN ($funding_source_id) OR e.FundingSource IN ($funding_source_id)) 
					$purchase_date_sql
					$wo_con
				ORDER BY
					a.ItemCode
				";
        $arr_result = $linventory->returnArray($sql);

        $totalPurchasePrice = 0;

        if (sizeof($arr_result) > 0) {
            for ($i = 0; $i < sizeof($arr_result); $i ++) {
                list ($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];

                // filter out no stock-taken item if necessary
                if ($item_type == ITEM_TYPE_SINGLE && is_array($StocktakeResultArray) && ! in_array($item_id, $StocktakeResultArray)) {
                    // debug($item_id, $item_code, $item_name);
                    continue;
                } elseif ($item_type == ITEM_TYPE_BULK && is_array($StocktakeBulkResultArray) && ! in_array($item_id, $StocktakeBulkResultArray)) {
                    // debug($item_id, $item_code, $item_name);
                    continue;
                }

                if ($item_type == ITEM_TYPE_SINGLE) {
                    // added main buliding name [Case#D71480]
                    $sql = "SELECT
									a.PurchaseDate,
									if(a.PurchasedPrice != '',a.PurchasedPrice,'0') as PurchasedPrice,
									if(a.UnitPrice != '',a.UnitPrice,'0') as UnitPrice,
									a.LocationID,
									CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . "),
									d.FundingType,
									" . $linventory->getInventoryNameByLang("d.") . ",
									e.ApproveDate,
									e.WriteOffReason,
									a.ItemRemark,
									" . $linventory->getInventoryNameByLang("d2.") . ",
									a.UnitPrice1,
									a.UnitPrice2,
									d2.FundingType as FundingType2,
									i.StockTakeOption
							FROM
									INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
									INVENTORY_ITEM i ON (i.ItemID=a.ItemID AND i.ItemType=1) INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID) INNER JOIN
									INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_FUNDING_SOURCE AS d2 ON (a.FundingSource2 = d2.FundingSourceID) LEFT OUTER JOIN
									INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
							WHERE
									a.ItemID = '" . $item_id . "'
							";
                    $arr_sub_result = $linventory->returnArray($sql);

                    if (! $display_not_req_stocktaking) {
                        $rs = $linventory->FilterForStockTakeSingle($arr_sub_result);
                        if (! count($rs)) {
                            continue;
                        }
                    }

                    if ($FilterSingleItemPriceFrom != null || $FilterSingleItemPriceEnd != null) {
                        list ($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark, $this_item_id, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[0];
                        if ($FilterSingleItemPriceFrom != null && $unit_price < $FilterSingleItemPriceFrom) {
                            continue;
                        } elseif ($FilterSingleItemPriceEnd != null && $unit_price > $FilterSingleItemPriceEnd) {
                            continue;
                        }
                    }

                    if (sizeof($arr_sub_result) > 0) {
                        for ($j = 0; $j < sizeof($arr_sub_result); $j ++) {
                            list ($purchase_date, $purchase_price, $unit_price, $location_id, $location_name, $funding_type, $funding_name, $write_off_date, $write_off_reason, $remark, $funding_name2, $unit_price1, $unit_price2, $funding_type2) = $arr_sub_result[$j];
                            // $quantity = 1;
                            $quantity = $write_off_date ? "0" : "1";
                            if ($unit_price > 0)
                                $purchase_price = $unit_price * $quantity;
                            else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
                            	$purchase_price = 0;
                            }

                            $totalPurchasePrice += $purchase_price;

                            if ($purchase_date == "0000-00-00")
                                $purchase_date = "";
                            else
                                $purchase_date = " " . $purchase_date;

                            /*
                             * $gov_funding = "0";
                             * $school_funding = "0";
                             * $sponsor_funding = "0";
                             * if($ShowFundingName)
                             * {
                             * $str_funding_name = " (".$funding_name.")";
                             * }
                             * if($purchase_price==0) $str_funding_name="";
                             * if($funding_type == ITEM_SCHOOL_FUNDING)
                             * {
                             * $school_funding = $purchase_price.$str_funding_name;
                             * }
                             * if($funding_type == ITEM_GOVERNMENT_FUNDING)
                             * {
                             * $gov_funding = $purchase_price.$str_funding_name ;
                             * }
                             * if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
                             * {
                             * $sponsor_funding = $purchase_price.$str_funding_name ;
                             * }
                             */

                            $Cost_Ary = array();
                            $Funding_ary = array();
                            $Cost_Ary[$funding_type] += $unit_price1;
                            if ($ShowFundingName)
                                $Funding_ary[$funding_type] .= ($Funding_ary[$funding_type] ? ", " : "") . $funding_name;
                            if ($funding_type2) {
                                $Cost_Ary[$funding_type2] += $unit_price2;
                                if ($ShowFundingName)
                                    $Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", " : "") . $funding_name2;
                            }

                            $school_funding = number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2) . ($Funding_ary[ITEM_SCHOOL_FUNDING] ? " (" . $Funding_ary[ITEM_SCHOOL_FUNDING] . ")" : "");
                            $gov_funding = number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2) . ($Funding_ary[ITEM_GOVERNMENT_FUNDING] ? " (" . $Funding_ary[ITEM_GOVERNMENT_FUNDING] . ")" : "");
                            $sponsor_funding = number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2) . ($Funding_ary[ITEM_SPONSORING_BODY_FUNDING] ? " (" . $Funding_ary[ITEM_SPONSORING_BODY_FUNDING] . ")" : "");

                            if ($write_off_date != "") {
                                if ($write_off_reason != "") {
                                    $write_off_date_reason = $write_off_date . " (" . $write_off_reason . ")";
                                } else {
                                    $write_off_date_reason = $write_off_date;
                                }
                            } else {
                                $write_off_date_reason = "";
                            }
                        }
                    }

                    // S/N, Quotation No, Supplier, Tender No., Invoice No.
                    $sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID=$item_id";
                    $result2 = $linventory->returnArray($sql);
                    list ($thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $result2[0];

                    // $file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$purchase_date\",\"$school_funding\",\"$gov_funding\",\"$sponsor_funding\",\"$unit_price\",\"$purchase_price\",\"$quantity\",\"$location_name\",\"$write_off_date_reason\",\"\",\"\"\n";
                    $temprows = array(
                        $item_category,
                        $item_subcategory,
                        $item_code,
                        $item_name,
                        $item_description,
                        $purchase_date,
                        $school_funding,
                        $gov_funding,
                        $sponsor_funding,
                        $unit_price,
                        $purchase_price,
                        $quantity,
                        $location_name,
                        $thisSerialNumber,
                        $thisQuotationNo,
                        $thisSupplierName,
                        $thisTenderNo,
                        $thisInvoiceNo,
                        $write_off_date_reason,
                        "",
                        $remark
                    );
                    $rows[] = $temprows;
                }
                if ($item_type == ITEM_TYPE_BULK) {
                    if (! $display_not_req_stocktaking) {
                        $sql = "SELECT ItemID, StockTakeOption FROM INVENTORY_ITEM WHERE ItemID='" . $item_id . "'";
                        $rs = $linventory->returnResultSet($sql);
                        $rs = $linventory->FilterForStockTakeBulk($rs);
                        if (! count($rs)) {
                            continue;
                        }
                    }

                    $final_purchase_date = "";

                    // ## GET PURCHASE DATE ###
                    if ($PurchaseDateFrom && $PurchaseDateTo) {
                        $purchase_date_sql = " and (PurchaseDate>='" . $PurchaseDateFrom . "' and PurchaseDate<='" . $PurchaseDateTo . "')";
                    }
                    if ($PurchaseDateFrom && ! $PurchaseDateTo) {
                        $purchase_date_sql = " and (PurchaseDate>='" . $PurchaseDateFrom . "')";
                    }
                    if (! $PurchaseDateFrom && $PurchaseDateTo) {
                        $purchase_date_sql = " and (PurchaseDate<='" . $PurchaseDateTo . "') ";
                    }
                    $sql = "SELECT
									IF(PurchaseDate IS NULL, '', PurchaseDate)
							FROM
									INVENTORY_ITEM_BULK_LOG
							WHERE
									ItemID = '" . $item_id . "' AND
									Action = 1
									and (RecordStatus=0 or RecordStatus is NULL)
                                    $purchase_date_sql
							";
                    /*
                     * AND
                     * GroupInCharge = $admin_group_id
                     */
                    $arr_purchase_date = $linventory->returnArray($sql, 1);

                    $tmp_arr_purchase_date = array();
                    if (sizeof($arr_purchase_date) > 0) {
                        for ($j = 0; $j < sizeof($arr_purchase_date); $j ++) {
                            list ($purchase_date) = $arr_purchase_date[$j];

                            if ($purchase_date != '') {
                                // $final_purchase_date = $final_purchase_date."\r\n".$purchase_date;
                                $tmp_arr_purchase_date[] = " " . $purchase_date;
                            } else {
                                // $final_purchase_date = ' ';
                            }
                        }
                        if (sizeof($tmp_arr_purchase_date) > 0) {
                            $final_purchase_date = implode("\r\n", $tmp_arr_purchase_date);
                        }
                    } else {
                        $final_purchase_date = ' ';
                    }

                    // ## GET COST, UNIT PRICE, PURCHASED PRICE ###
                    $total_school_cost = 0;
                    $total_gov_cost = 0;
                    $final_unit_price = 0;
                    $final_purchase_price = 0;
                    $total_qty = 0;
                    $tmp_unit_price = 0;
                    $tmp_purchase_price = 0;
                    $written_off_qty = 0;
                    $location_qty = 0;
                    $ini_total_qty = 0;

                    // # get Total purchase price ##
                    $count = $linventory->getNumOfBulkItemInvoice($item_id);
                    if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
                        $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                    }else{
                        // $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
                        $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                    }

                    $tmp_result = $linventory->returnVector($sql);
                    if (sizeof($tmp_result) > 0) {
                        $total_purchase_price = $tmp_result[0];
                        // [Case#B70033]
                        if ($total_purchase_price == 0) {
                            $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                            $tmp_result = $linventory->returnVector($sql);
                            if ($tmp_result[0] > 0)
                                $total_purchase_price = $tmp_result[0];
                        }
                    }

                    $sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                    $tmp_result = $linventory->returnVector($sql);
                    if (sizeof($tmp_result) > 0) {
                        $item_remark = implode(";\r\n", array_unique($tmp_result));
                    }

                    // # get Total Qty Of the target Item ##
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '" . $item_id . "'";
                    $tmp_result = $linventory->returnArray($sql, 1);
                    if (sizeof($tmp_result) > 0) {
                        list ($totalQty) = $tmp_result[0];
                    }
                    // # get written-off qty ##
                    $sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_id . "'";
                    $tmp_result = $linventory->returnArray($sql, 1);
                    if (sizeof($tmp_result) > 0) {
                        list ($written_off_qty) = $tmp_result[0];
                    }

                    // # get the qty in the target location ##
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '" . $item_id . "' AND FundingSourceID = '" . $funding_source_id . "'";
                    $tmp_result = $linventory->returnArray($sql, 1);
                    if (sizeof($tmp_result) > 0) {
                        list ($location_qty) = $tmp_result[0];
                    }

                    // # Location origial qty (by location only)##
                    $sql = "SELECT WriteOffQty FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_id . "' AND FundingSourceID = '" . $funding_source_id . "'";
                    $tmp_result = $linventory->returnVector($sql);
                    if (sizeof($tmp_result) > 0) {
                        $ini_location_qty = $tmp_result[0] + $location_qty;
                    } else {
                        $ini_location_qty = $location_qty;
                    }

                    /*
                     * $sql = "SELECT
                     * a.FundingSource,
                     * b.FundingType,
                     * ".$linventory->getInventoryNameByLang("b.")."
                     * FROM
                     * INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
                     * INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
                     * WHERE
                     * a.ItemID = $item_id AND
                     * a.Action IN (1,7) AND
                     * a.FundingSource IS NOT NULL
                     * ";
                     * $tmp_result = $linventory->returnArray($sql,3);
                     * if(sizeof($tmp_result)>0){
                     * list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
                     * }
                     */

                    // ## Get Qty By Location ###
                    $sql = "SELECT
									a.Quantity,
									a.LocationID,
									CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . "),
									f.FundingType,
									" . $linventory->getInventoryNameByLang("f.") . " as funding_name,
									a.FundingSourceID
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID)
									inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
							WHERE
									a.ItemID = '" . $item_id . "' AND
									a.FundingSourceID = '" . $funding_source_id . "'
							";
                    $arr_tmp_qty = $linventory->returnArray($sql);
                    if (sizeof($arr_tmp_qty) > 0) {
                        $display_qty = 0;
                        $qty = 0;
                        $display_location_name = "";
                        $tmp_arr_display_location_name = array();
                        unset($location_showed);
                        $location_showed = array();

                        for ($j = 0; $j < sizeof($arr_tmp_qty); $j ++) {
                            list ($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
                            if ($qty != "") {
                                $display_qty = $display_qty + $qty;
                            }
                            if ($location_name != "" && ! $location_showed[$location_id]) {
                                // $display_location_name = $display_location_name."\r\n".$location_name;
                                $tmp_arr_display_location_name[] = $location_name;
                                $location_showed[$location_id] = true;
                            }
                        }
                        if (sizeof($tmp_arr_display_location_name) > 0) {
                            $display_location_name = implode("\r\n", $tmp_arr_display_location_name);
                        }
                    }

                    $ini_total_qty = $totalQty + $written_off_qty;
                    if ($ini_total_qty > 0)
                        $final_unit_price = round($total_purchase_price / $ini_total_qty, 2);

                    $count = $linventory->getNumOfBulkItemInvoice($item_id);
                    if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
                        $current_total = $total_purchase_price;
                    }else{
                        $current_total = $final_unit_price * $display_qty;
                    }

                    $totalPurchasePrice += $current_total;

                    if ($FilterBulkItemPriceFrom != null || $FilterBulkItemPriceEnd != null) {
                        if ($FilterBulkItemPriceFrom != null && $current_total < $FilterBulkItemPriceFrom) {
                            $rows_shown --;
                            continue;
                        } elseif ($FilterBulkItemPriceEnd != null && $current_total > $FilterBulkItemPriceEnd) {
                            $rows_shown --;
                            continue;
                        }
                    }

                    $final_purchase_price = number_format($final_unit_price * $ini_total_qty, 2);

                    // retrieve funding data
                    $tmp_funding_src = array();
                    for ($j = 0; $j < sizeof($arr_tmp_qty); $j ++) {
                        list ($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
                        // [Case#V72270]
                        // # Location origial qty (by location only)##
                        if ($display_writeoff) {
                            $sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_id . "' AND FundingSourceID = '" . $fund_id . "'";
                            $tmp_result = $linventory->returnVector($sql);
                            if (sizeof($tmp_result) > 0 && $j == 0) {
                                $qty = $tmp_result[0] + $qty;
                            }
                        }

                        $count = $linventory->getNumOfBulkItemInvoice($item_id);
                        if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
                            $tmp_funding_src[$funding_type][$fund_id]['cost'] = $total_purchase_price;
                        }else{
                            $tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
                        }
                        $tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
                        $tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
                    }

                    $this_field_display = array();
                    $this_final_total_cost = 0;
                    for ($type_i = 1; $type_i <= 3; $type_i ++) {
                        $this_field = "";
                        if (! empty($tmp_funding_src[$type_i])) {
                            $this_fund_cost = 0;
                            foreach ($tmp_funding_src[$type_i] as $k => $d) {
                                if ($ShowFundingName) {
                                    $this_fund_cost = number_format($d['cost'], 2);
                                    // $this_fund_src = ($d['cost']>0) ? " (". $d['src_name'].")" : "";
                                    $this_fund_src = " (" . $d['src_name'] . ")";
                                    $this_field .= ($this_field ? "; " : "") . $this_fund_cost . $this_fund_src;
                                } else {
                                    $this_fund_cost += $d['cost'];
                                    $this_field = number_format($this_fund_cost, 2);
                                }
                                $this_final_total_cost = $this_fund_cost;
                            }
                        }
                        $this_field_display[$type_i] = $this_field ? $this_field : "0.00";

                        // $table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
                    }
                    /*
                     * if($funding_type == ITEM_SCHOOL_FUNDING)
                     * {
                     * $ini_total_qty = $totalQty + $written_off_qty;
                     * if ($ini_total_qty>0)
                     * $final_unit_price = round($total_purchase_price/$ini_total_qty,2);
                     * //$total_school_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
                     * $total_school_cost = round($final_unit_price*$display_qty, 2);
                     * if($ShowFundingName){
                     * $str_funding_name = " (".$funding_name.")";
                     * $total_school_cost .= $str_funding_name;
                     * }
                     * $final_purchase_price = $total_school_cost;
                     * }
                     * if($funding_type == ITEM_GOVERNMENT_FUNDING)
                     * {
                     * $ini_total_qty = $totalQty + $written_off_qty;
                     * if ($ini_total_qty>0)
                     * $final_unit_price = round($total_purchase_price/$ini_total_qty,2);
                     * //$total_gov_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
                     * $total_gov_cost = round($final_unit_price*$display_qty, 2);
                     * if($ShowFundingName){
                     * $str_funding_name = " (".$funding_name.")";
                     * $total_gov_cost .= $str_funding_name;
                     * }
                     * $final_purchase_price = $total_gov_cost;
                     * }
                     */
                    $final_unit_price = round($final_unit_price, 2);

                    $display_write_off = "";

                    $sql = "SELECT
									ApproveDate,
									WriteOffQty,
									WriteOffReason
							FROM
									INVENTORY_ITEM_WRITE_OFF_RECORD
							WHERE
									ItemID = $item_id AND
									FundingSourceID = $funding_source_id
									 AND RecordStatus = 1
							";

                    $arr_write_off = $linventory->returnArray($sql, 3);

                    if (sizeof($arr_write_off) > 0) {
                        $tmp_write_off = array();
                        for ($j = 0; $j < sizeof($arr_write_off); $j ++) {
                            list ($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
                            if ($write_off_date != "") {
                                $write_off_qty = $write_off_qty . $i_InventorySystem_Report_PCS;
                                $tmp_write_off[] = $write_off_date . "\r\n" . $write_off_qty . "\r\n" . $write_off_reason;
                            }
                        }

                        if (sizeof($tmp_write_off) > 0) {
                            for ($k = 0; $k < sizeof($tmp_write_off); $k ++) {
                                $str_reason = $tmp_write_off[$k];
                                $display_write_off .= $str_reason;
                                if ($k != 0) {
                                    $display_write_off .= "\r\n";
                                }
                            }
                        }
                    } else {
                        $display_write_off = "";
                    }

                    // Quotation No, Supplier, Tender No., Invoice No.
                    $sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID=$item_id";
                    $result2 = $linventory->returnArray($sql);
                    if (! empty($result2)) {
                        $thisDataAry = array();

                        foreach ($result2 as $k2 => $d2) {
                            list ($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
                            if ($thisQuotationNo)
                                $thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
                            if ($thisSupplierName)
                                $thisDataAry['Supplier'][] = trim($thisSupplierName);
                            if ($thisTenderNo)
                                $thisDataAry['TenderNo'][] = trim($thisTenderNo);
                            if ($thisInvoiceNo)
                                $thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
                        }
                    }

                    $SNStr = "";
                    $QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ", array_unique($thisDataAry['QuotationNo'])) : "";
                    $SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ", array_unique($thisDataAry['Supplier'])) : "";
                    $TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ", array_unique($thisDataAry['TenderNo'])) : "";
                    $InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ", array_unique($thisDataAry['InvoiceNo'])) : "";

                    // $file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$final_purchase_date\",\"". $this_field_display[1] ."\",\"". $this_field_display[2] ."\",\"". $this_field_display[3] ."\",\"$final_purchase_price\",\"$display_qty\",\"$display_location_name\",\"$display_write_off\"\n";
                    // $temprows = array($item_code,$item_name,$item_description,$final_purchase_date,$this_field_display[1],$this_field_display[2],$this_field_display[3],$final_unit_price,$final_purchase_price,$display_qty,$display_location_name,$display_write_off);
                    // $file_content .= "\"$item_code\",\"".addslashes($item_name)."\",\"".addslashes($item_description)."\",\"$final_purchase_date\",\"". $this_field_display[1] ."\",\"". $this_field_display[2] ."\",\"". $this_field_display[3] ."\",\"$this_final_total_cost\",\"$display_qty\",\"$display_location_name\",\"$display_write_off\"\n";
                    $temprows = array(
                        $item_category,
                        $item_subcategory,
                        $item_code,
                        $item_name,
                        $item_description,
                        $final_purchase_date,
                        $this_field_display[1],
                        $this_field_display[2],
                        $this_field_display[3],
                        $final_unit_price,
                        $current_total,
                        $display_qty,
                        $display_location_name,
                        $SNStr,
                        $QnoNoStr,
                        $SupplierStr,
                        $TenderNoStr,
                        $InvoiceNoStr,
                        $display_write_off,
                        "",
                        $item_remark
                    );

                    $rows[] = $temprows;
                }
            }
            if ($ShowPurchasePriceTotal)
                $rows[] = array(
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    $Lang['eInventory']['TotalAssets'],
                    $totalPurchasePrice,
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    ''
                );
        }
        if (sizeof($arr_result) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        $file_content .= "\n";
        $file_content .= "\n";
        $rows[] = array(
            ''
        );
        $rows[] = array(
            ''
        );
    }
}

$file_content = str_replace("&#039;","'",$file_content);
$file_content = stripslashes($file_content);
$display = $file_content;

intranet_closedb();

$filename = "fixed_assets_register_export_unicode.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>