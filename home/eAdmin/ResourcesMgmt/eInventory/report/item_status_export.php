<?php
// using:

// #########################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2012-07-10 (YatWoon)
// Enhanced: support 2 funding source for single item
//
// Date: 2011-03-18 YatWoon
// update query, hidden bulk item if quantity is 0
// - Fixed: display incorrect item latest status data
//
// #########################################
$PATH_WRT_ROOT = "../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();
$lexport = new libexporttext();

// $targetLocation = stripslashes($selected_cb);
// $targetGroup = stripslashes($selected_cb);

$targetLocation = stripslashes($selected_cb);
$targetGroup = stripslashes($selected_cb);
$targetSubCategory = stripslashes($selected_cb);
$SubCatID = stripslashes($SubCatFilter);
$SubLocationID = stripslashes($SubLocationFilter);

// Filter condition - Sub Cat
if ($SubCatID != "") {
    $SubCatCond = " AND SUBCAT.Category2ID IN ($SubCatID) ";
}
// Filter condition - Sub Location
if ($SubLocationID != "") {
    $SubLocationCond = "  AND L.LocationID IN ($SubLocationID) ";
}

if ($groupBy == 1) {
    $cond1 = " AND L.LocationID IN ($targetLocation) ";
} else {
    $cond1 = " ";
}
if ($groupBy == 3) {
    $cond2 = " AND AG.AdminGroupID IN ($targetGroup) ";
} else {
    $cond2 = " ";
}
if ($groupBy == 4) {
    $cond4 = " AND SUBCAT.Category2ID IN ($targetGroup) ";
} else {
    $cond4 = " ";
}

$exportColumn = array(
    $i_InventorySystem_Report_ItemStatus,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);

$file_content .= "\"$i_InventorySystem_Report_ItemStatus\"\n";

if ($groupBy == 1) {
    $cond = "WHERE LocationID in (" . $selected_cb . ")";
    
    $sql = "Select DISTINCT(CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",' > '," . $linventory->getInventoryItemNameByLang("b.") . ",' > '," . $linventory->getInventoryItemNameByLang("a.") . ")) as LocationName, LocationID 
				FROM INVENTORY_LOCATION AS a INNER JOIN 
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID) 
				$cond 
				ORDER BY b.LocationLevelID";
    
    $arr_location = $linventory->returnArray($sql, 1);
    
    for ($a = 0; $a < sizeof($arr_location); $a ++) {
        list ($location_name, $locationID) = $arr_location[$a];
        
        // single items #
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
        }
        
        if ($target_admin_group != "") {
            $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
        }
        
        $sql = "SELECT 
						a.ItemID 
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID $cond) INNER JOIN 
						INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
				WHERE
						a.RecordStatus = 1 AND
						c.LocationID = $locationID
				ORDER BY
						a.ItemCode
				";
        
        $arr_tmp_single_item = $linventory->returnArray($sql, 1);
        
        // $file_content .= "\"$location_name\"\n";
        // $file_content .= "\"$i_InventorySystem_ItemType_Single\"\n";
        // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystemItemStatus\",\"$i_LastModified\"\n";
        
        $rows[] = array(
            $location_name
        );
        $rows[] = array(
            $i_InventorySystem_ItemType_Single
        );
        $rows[] = array(
            $i_InventorySystem_Item_Code,
            $i_InventorySystem_Item_Name,
            $i_InventorySystem['Category'],
            $i_InventorySystem['Location'],
            $i_InventorySystem['Caretaker'],
            $i_InventorySystem['FundingSource'],
            $i_InventorySystemItemStatus,
            $i_LastModified
        );
        
        if (sizeof($arr_tmp_single_item) > 0) {
            for ($i = 0; $i < sizeof($arr_tmp_single_item); $i ++) {
                list ($tmp_single_item_id) = $arr_tmp_single_item[$i];
                
                $sql = "SELECT 
								DISTINCT ITEM.ItemID,
								ITEM.ItemCode,
								CONCAT(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
								CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
									" . $linventory->getInventoryNameByLang("funding2.") . "
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
								INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) LEFT OUTER JOIN
								INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
						WHERE 
								ITEM.ItemID = $tmp_single_item_id AND
								ITEM.RecordStatus = 1 AND
								ITEM.ItemType=1 AND 
								L.LocationID='" . $locationID . "' 
								$SubCatCond
						LIMIT 0,1
						";
                
                $arr_single_result = $linventory->returnArray($sql);
                
                if (is_array($arr_single_result) && count($arr_single_result) > 0) {
                    foreach ($arr_single_result as $Key => $Value) {
                        $item_status = "";
                        $date_modified = "";
                        
                        list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $item_funding, $item_funding2) = $Value;
                        
                        $sql = "SELECT 
										NewStatus, DateModified 
								FROM 
										INVENTORY_ITEM_SINGLE_STATUS_LOG
								WHERE
										ItemID = $item_id AND
										ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
								ORDER BY
										DateModified DESC 
								LIMIT 0,1
								";
                        $tmp_single_item_status = $linventory->returnArray($sql, 2);
                        
                        if (sizeof($tmp_single_item_status) > 0) {
                            list ($item_status, $date_modified) = $tmp_single_item_status[0];
                        }
                        
                        if ($item_status == "")
                            $status = " - ";
                        else 
                            if ($item_status == ITEM_STATUS_NORMAL)
                                $status = $i_InventorySystem_ItemStatus_Normal;
                            else 
                                if ($item_status == ITEM_STATUS_DAMAGED)
                                    $status = $i_InventorySystem_ItemStatus_Damaged;
                                else 
                                    if ($item_status == ITEM_STATUS_REPAIR)
                                        $status = $i_InventorySystem_ItemStatus_Repair;
                                    else
                                        $status = " - ";
                            
                            // $file_content .= "\"$item_code\",\"$item_name\",\"$item_cat\",\"$location\",\"$resource_mgt_group\",\"$status\",\"$date_modified\"\n";
                        $item_funding_str = $item_funding . ($item_funding2 ? ", " . $item_funding2 : "");
                        $rows[] = array(
                            $item_code,
                            $item_name,
                            $item_cat,
                            $location,
                            $resource_mgt_group,
                            $item_funding_str,
                            $status,
                            $date_modified
                        );
                    }
                }
            }
        }
        if (sizeof($arr_tmp_single_item) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        // single items end #
        
        // bulk items #
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
            $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
        }
        
        $sql = "SELECT 
						a.ItemID, b.GroupInCharge, b.FundingSourceID 
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0 $cond) INNER JOIN 
						INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
				WHERE
						a.RecordStatus = 1 AND
						c.LocationID = $locationID
				ORDER BY
						a.ItemCode, b.RecordID
				";
        $arr_tmp_bulk_item = $linventory->returnArray($sql);
        
        // $file_content .= "\"$i_InventorySystem_ItemType_Bulk\"\n";
        // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystem_ItemStatus_Normal\",\"$i_InventorySystem_ItemStatus_Repair\",\"$i_InventorySystem_ItemStatus_Damaged\",\"$i_LastModified\"\n";
        
        $rows[] = array(
            $i_InventorySystem_ItemType_Bulk
        );
        $rows[] = array(
            $i_InventorySystem_Item_Code,
            $i_InventorySystem_Item_Name,
            $i_InventorySystem['Category'],
            $i_InventorySystem['Location'],
            $i_InventorySystem['Caretaker'],
            $i_InventorySystem['FundingSource'],
            $i_InventorySystem_ItemStatus_Normal,
            $i_InventorySystem_ItemStatus_Repair,
            $i_InventorySystem_ItemStatus_Damaged,
            $i_LastModified
        );
        
        if (sizeof($arr_tmp_bulk_item) > 0) {
            for ($i = 0; $i < sizeof($arr_tmp_bulk_item); $i ++) {
                list ($tmp_bulk_item_id, $tmp_bulk_group_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$i];
                
                $sql = "SELECT 
								DISTINCT ITEM.ItemID,
								ITEM.ItemCode,
								concat(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
								concat(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								LOCATION.GroupInCharge,
								LOCATION.FundingSourceID
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN 
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.CategoryID = SUBCAT.CategoryID) INNER JOIN 
								INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID and LOCATION.Quantity>0) LEFT OUTER JOIN
								INVENTORY_LOCATION AS L ON (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID)
						WHERE 
								ITEM.ItemID = $tmp_bulk_item_id AND
								ITEM.RecordStatus = 1 AND
								ITEM.ItemType = 2 AND 
								L.LocationID='" . $locationID . "' AND LOCATION.GroupInCharge=$tmp_bulk_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
								$SubCatCond
						LIMIT 0,1
						";
                $arr_bulk_result = $linventory->returnArray($sql, 5);
                
                if (is_array($arr_bulk_result) && count($arr_bulk_result) > 0) {
                    foreach ($arr_bulk_result as $Key => $Value) {
                        $qty_normal = "";
                        $qty_repair = "";
                        $qty_damage = "";
                        $date_modified = "";
                        
                        $single_table_content .= "<tr>";
                        
                        list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $bulk_funding, $bulk_group_id, $bulk_funding_id) = $Value;
                        
                        $sql = "SELECT 
										if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
										if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
										if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
										BL.DateModified
								FROM
										INVENTORY_ITEM_BULK_LOG AS BL 
								WHERE
										BL.ItemID = $item_id AND
										BL.LocationID = $locationID AND
										BL.GroupInCharge=$bulk_group_id and BL.FundingSource=$bulk_funding_id and
										BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
								ORDER BY
										DateModified DESC, RecordID DESC
								LIMIT 0,1										
								";
                        
                        $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                        
                        if (sizeof($tmp_bulk_item_status) > 0) {
                            list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                        }
                        
                        if ($qty_normal == "")
                            $qty_normal = " - ";
                        if ($qty_repair == "")
                            $qty_repair = " - ";
                        if ($qty_damage == "")
                            $qty_damage = " - ";
                        if ($date_modified == "")
                            $date_modified = " - ";
                            
                            // $file_content .= "\"$item_code\",\"$item_name\",\"$item_cat\",\"$location\",\"$resource_mgt_group\",\"$qty_normal\",\"$qty_repair\",\"$qty_damage\",\"$date_modified\"\n";
                        $rows[] = array(
                            $item_code,
                            $item_name,
                            $item_cat,
                            $location,
                            $resource_mgt_group,
                            $bulk_funding,
                            $qty_normal,
                            $qty_repair,
                            $qty_damage,
                            $date_modified
                        );
                    }
                }
            }
        }
        if (sizeof($arr_tmp_bulk_item) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        $file_content .= "\n";
        $file_content .= "\n";
    }
} else 
    if ($groupBy == 2) {
        // single item #
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
            $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
        }
        
        $sql = "SELECT 
					a.ItemID 
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID $cond) INNER JOIN 
					INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
			WHERE
					a.RecordStatus = 1
			ORDER BY
					a.ItemCode
			";
        $arr_tmp_single_item = $linventory->returnArray($sql, 1);
        
        // $file_content .= "\"$i_InventorySystem_ItemType_Single\"\n";
        // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystemItemStatus\",\"$i_LastModified\"\n";
        
        $rows[] = array(
            $i_InventorySystem_ItemType_Single
        );
        $rows[] = array(
            $i_InventorySystem_Item_Code,
            $i_InventorySystem_Item_Name,
            $i_InventorySystem['Category'],
            $i_InventorySystem['Location'],
            $i_InventorySystem['Caretaker'],
            $i_InventorySystem['FundingSource'],
            $i_InventorySystemItemStatus,
            $i_LastModified
        );
        
        if (sizeof($arr_tmp_single_item) > 0) {
            for ($i = 0; $i < sizeof($arr_tmp_single_item); $i ++) {
                list ($tmp_single_item_id) = $arr_tmp_single_item[$i];
                
                $sql = "SELECT 
							DISTINCT ITEM.ItemID,
							ITEM.ItemCode,
							CONCAT(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
							" . $linventory->getInventoryItemNameByLang("ITEM.") . ",						
							CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
							" . $linventory->getInventoryNameByLang("AG.") . ",
							" . $linventory->getInventoryNameByLang("funding.") . ",
							" . $linventory->getInventoryNameByLang("funding2.") . "
					FROM 
							INVENTORY_ITEM AS ITEM INNER JOIN 
							INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN 
							INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) INNER JOIN
							INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
							left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
					WHERE 
							ITEM.ItemID = $tmp_single_item_id AND
							ITEM.ItemType=1 AND
							ITEM.RecordStatus = 1
					";
                
                $arr_single_result = $linventory->returnArray($sql);
                
                if (is_array($arr_single_result) && count($arr_single_result) > 0) {
                    foreach ($arr_single_result as $Key => $Value) {
                        $item_status = "";
                        $date_modified = "";
                        
                        list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $item_funding, $item_funding2) = $Value;
                        
                        $sql = "SELECT 
									NewStatus, DateModified 
							FROM 
									INVENTORY_ITEM_SINGLE_STATUS_LOG
							WHERE
									ItemID = $item_id AND
									ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
							ORDER BY
									DateModified DESC 
							LIMIT 0,1
							";
                        
                        $tmp_single_item_status = $linventory->returnArray($sql, 2);
                        
                        if (sizeof($tmp_single_item_status) > 0) {
                            list ($item_status, $date_modified) = $tmp_single_item_status[0];
                        }
                        
                        if ($item_status == "")
                            $status = " - ";
                        else 
                            if ($item_status == ITEM_STATUS_NORMAL)
                                $status = $i_InventorySystem_ItemStatus_Normal;
                            else 
                                if ($item_status == ITEM_STATUS_DAMAGED)
                                    $status = $i_InventorySystem_ItemStatus_Damaged;
                                else 
                                    if ($item_status == ITEM_STATUS_REPAIR)
                                        $status = $i_InventorySystem_ItemStatus_Repair;
                                    else
                                        $status = " - ";
                        
                        if ($date_modified == "")
                            $date_modified = " - ";
                            
                            // $file_content .= "\"$item_code\",\"$item_name\",\"$item_cat\",\"$location\",\"$resource_mgt_group\",\"$status\",\"$date_modified\"\n";
                        $item_funding_str = $item_funding . ($item_funding2 ? ", " . $item_funding2 : "");
                        $rows[] = array(
                            $item_code,
                            $item_name,
                            $item_cat,
                            $location,
                            $resource_mgt_group,
                            $item_funding_str,
                            $status,
                            $date_modified
                        );
                    }
                }
            }
        }
        if (sizeof($arr_tmp_single_item) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        // end of single item #
        
        // bulk items #
        if ($linventory->IS_ADMIN_USER($UserID))
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
        else
            $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
        
        $tmp_arr_group = $linventory->returnVector($sql);
        if (sizeof($tmp_arr_group) > 0) {
            $target_admin_group = implode(",", $tmp_arr_group);
            $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
        }
        
        $sql = "SELECT 
					a.ItemID, b.LocationID, b.GroupInCharge, b.FundingSourceID 
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0 $cond) INNER JOIN 
					INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID)
			WHERE
					a.RecordStatus = 1
			ORDER BY
					a.ItemCode, b.RecordID
			";
        $arr_tmp_bulk_item = $linventory->returnArray($sql);
        
        // $file_content .= "\n";
        // $file_content .= "\"$i_InventorySystem_ItemType_Bulk\"\n";
        // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystem_ItemStatus_Normal\",\"$i_InventorySystem_ItemStatus_Repair\",\"$i_InventorySystem_ItemStatus_Damaged\",\"$i_LastModified\"\n";
        
        $rows[] = array(
            $i_InventorySystem_ItemType_Bulk
        );
        $rows[] = array(
            $i_InventorySystem_Item_Code,
            $i_InventorySystem_Item_Name,
            $i_InventorySystem['Category'],
            $i_InventorySystem['Location'],
            $i_InventorySystem['Caretaker'],
            $i_InventorySystem['FundingSource'],
            $i_InventorySystem_ItemStatus_Normal,
            $i_InventorySystem_ItemStatus_Repair,
            $i_InventorySystem_ItemStatus_Damaged,
            $i_LastModified
        );
        
        if (sizeof($arr_tmp_bulk_item) > 0) {
            for ($i = 0; $i < sizeof($arr_tmp_bulk_item); $i ++) {
                list ($tmp_bulk_item_id, $tmp_bulk_item_location, $tmp_bulk_group_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$i];
                
                $sql = "SELECT 
							DISTINCT ITEM.ItemID,
							ITEM.ItemCode,
							concat(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
							" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
							L.LocationID,
							concat(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
							" . $linventory->getInventoryNameByLang("AG.") . ",
							" . $linventory->getInventoryNameByLang("funding.") . ",
							LOCATION.GroupInCharge,
							LOCATION.FundingSourceID
					FROM 
							INVENTORY_ITEM AS ITEM INNER JOIN 
							INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN 
							INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN 
							INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID AND LOCATION.Locationid = $tmp_bulk_item_location and LOCATION.Quantity>0) LEFT OUTER JOIN
							INVENTORY_LOCATION AS L ON (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
							left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID)
					WHERE 
							ITEM.ItemID = $tmp_bulk_item_id AND
							ITEM.RecordStatus = 1 AND
							ITEM.ItemType = 2 
							AND LOCATION.LocationID=$tmp_bulk_item_location AND LOCATION.GroupInCharge=$tmp_bulk_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
					LIMIT 0,1
					";
                $arr_bulk_result = $linventory->returnArray($sql);
                
                if (is_array($arr_bulk_result) && count($arr_bulk_result) > 0) {
                    foreach ($arr_bulk_result as $Key => $Value) {
                        $qty_normal = "";
                        $qty_repair = "";
                        $qty_damage = "";
                        $date_modified = "";
                        
                        list ($item_id, $item_code, $item_cat, $item_name, $location_id, $location, $resource_mgt_group, $bulk_funding, $bulk_group_id, $bulk_funding_id) = $Value;
                        
                        $sql = "SELECT 
									if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
									if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
									if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
									BL.DateModified
							FROM
									INVENTORY_ITEM_BULK_LOG AS BL 
							WHERE
									BL.ItemID = $item_id AND
									BL.LocationID = $location_id AND
									BL.GroupInCharge=$bulk_group_id and BL.FundingSource=$bulk_funding_id and
									BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
							ORDER BY
									DateModified DESC, RecordID DESC
							LIMIT 0,1										
							";
                        $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                        
                        if (sizeof($tmp_bulk_item_status) > 0) {
                            list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                        }
                        
                        if ($qty_normal == "")
                            $qty_normal = " - ";
                        if ($qty_repair == "")
                            $qty_repair = " - ";
                        if ($qty_damage == "")
                            $qty_damage = " - ";
                        if ($date_modified == "")
                            $date_modified = " - ";
                            
                            // $file_content .= "\"$item_code\",\"$item_name\",\"$item_cat\",\"$location\",\"$resource_mgt_group\",\"$qty_normal\",\"$qty_repair\",\"$qty_damage\",\"$date_modified\"\n";
                        $rows[] = array(
                            $item_code,
                            $item_name,
                            $item_cat,
                            $location,
                            $resource_mgt_group,
                            $bulk_funding,
                            $qty_normal,
                            $qty_repair,
                            $qty_damage,
                            $date_modified
                        );
                    }
                }
            }
        }
        if (sizeof($arr_tmp_bulk_item) == 0) {
            $file_content .= "\"$i_no_record_exists_msg\"\n";
            $rows[] = array(
                $i_no_record_exists_msg
            );
        }
        $file_content .= "\n";
    } else 
        if ($groupBy == 3) {
            // if(is_array($groupByGroupid))
            // $cond = "WHERE AdminGroupID in (".implode($groupByGroupid,",").")";
            $cond = "WHERE AdminGroupID in (" . $selected_cb . ")";
            
            $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY AdminGroupID";
            $arr_group = $linventory->returnArray($sql, 2);
            
            for ($a = 0; $a < sizeof($arr_group); $a ++) {
                list ($group_name, $admin_group_id) = $arr_group[$a];
                
                // single items #
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                    $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
                }
                
                $sql = "SELECT 
						a.ItemID 
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID $cond) INNER JOIN 
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID)
				WHERE
						a.RecordStatus = 1 AND
						c.AdminGroupID = $admin_group_id
				ORDER BY
						a.ItemCode
				";
                $arr_tmp_single_item = $linventory->returnArray($sql, 1);
                
                // $file_content .= "\"$group_name\"\n";
                // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystemItemStatus\",\"$i_LastModified\"\n";
                
                $rows[] = array(
                    $group_name
                );
                $rows[] = array(
                    $i_InventorySystem_Item_Code,
                    $i_InventorySystem_Item_Name,
                    $i_InventorySystem['Category'],
                    $i_InventorySystem['Location'],
                    $i_InventorySystem['Caretaker'],
                    $i_InventorySystem['FundingSource'],
                    $i_InventorySystemItemStatus,
                    $i_LastModified
                );
                
                if (sizeof($arr_tmp_single_item) > 0) {
                    for ($i = 0; $i < sizeof($arr_tmp_single_item); $i ++) {
                        list ($tmp_single_item_id) = $arr_tmp_single_item[$i];
                        
                        $sql = "SELECT 
								DISTINCT ITEM.ItemID,
								ITEM.ItemCode,
								concat(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
								concat(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								" . $linventory->getInventoryNameByLang("funding2.") . "
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN 
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN 
								INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) INNER JOIN
								INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
						WHERE 
								ITEM.ItemType = 1 AND 
								ITEM.RecordStatus = 1 AND
								ITEM.ItemID = $tmp_single_item_id AND
								AG.AdminGroupID='" . $admin_group_id . "' 
						";
                        $arr_single_result = $linventory->returnArray($sql);
                        
                        if (sizeof($arr_single_result) > 0) {
                            for ($j = 0; $j < sizeof($arr_single_result); $j ++) {
                                list ($single_item_id, $single_item_code, $item_cat, $single_item_name, $single_item_location, $single_item_group, $item_funding, $item_funding2) = $arr_single_result[$j];
                                
                                $sql = "SELECT 
										NewStatus, DateModified 
								FROM 
										INVENTORY_ITEM_SINGLE_STATUS_LOG
								WHERE
										ItemID = $single_item_id AND
										ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
								ORDER BY
										DateModified DESC 
								LIMIT 0,1
								";
                                
                                $tmp_single_item_status = $linventory->returnArray($sql, 2);
                                
                                if (sizeof($tmp_single_item_status) > 0) {
                                    list ($single_item_status, $single_item_date_modified) = $tmp_single_item_status[0];
                                }
                                
                                if ($single_item_status == "")
                                    $single_item_status = " - ";
                                else 
                                    if ($single_item_status == ITEM_STATUS_NORMAL)
                                        $single_item_status = $i_InventorySystem_ItemStatus_Normal;
                                    else 
                                        if ($single_item_status == ITEM_STATUS_DAMAGED)
                                            $single_item_status = $i_InventorySystem_ItemStatus_Damaged;
                                        else 
                                            if ($single_item_status == ITEM_STATUS_REPAIR)
                                                $single_item_status = $i_InventorySystem_ItemStatus_Repair;
                                            else
                                                $single_item_status = " - ";
                                    
                                    // $file_content .= "\"$single_item_code\",\"$single_item_name\",\"$item_cat\",\"$single_item_group\",\"$single_item_location\",\"$single_item_status\",\"$single_item_date_modified\"\n";
                                $item_funding_str = $item_funding . ($item_funding2 ? ", " . $item_funding2 : "");
                                $rows[] = array(
                                    $single_item_code,
                                    $single_item_name,
                                    $item_cat,
                                    $single_item_location,
                                    $single_item_group,
                                    $item_funding_str,
                                    $single_item_status,
                                    $single_item_date_modified
                                );
                            }
                        }
                    }
                }
                if (sizeof($arr_tmp_single_item) == 0) {
                    $file_content .= "\"$i_no_record_exists_msg\"\n";
                    $rows[] = array(
                        $i_no_record_exists_msg
                    );
                }
                
                // bulk item #
                if ($linventory->IS_ADMIN_USER($UserID))
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
                else
                    $sql = "SELECT DISTINCT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                
                $tmp_arr_group = $linventory->returnVector($sql);
                if (sizeof($tmp_arr_group) > 0) {
                    $target_admin_group = implode(",", $tmp_arr_group);
                    $cond = " AND b.GroupInCharge IN ($target_admin_group) ";
                }
                
                $sql = "SELECT 
						a.ItemID, b.LocationID, b.FundingSourceID
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and b.Quantity>0 $cond) INNER JOIN 
						INVENTORY_ADMIN_GROUP AS c ON (b.GroupInCharge = c.AdminGroupID)
				WHERE
						a.RecordStatus = 1 AND
						c.AdminGroupID = $admin_group_id
				ORDER BY
						a.ItemCode, b.RecordID
				";
                $arr_tmp_bulk_item = $linventory->returnArray($sql);
                
                // $file_content .= "\"$i_InventorySystem_ItemType_Bulk\"\n";
                // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystem_ItemStatus_Normal\",\"$i_InventorySystem_ItemStatus_Repair\",\"$i_InventorySystem_ItemStatus_Damaged\",\"$i_LastModified\"\n";
                
                $rows[] = array(
                    $i_InventorySystem_ItemType_Bulk
                );
                $rows[] = array(
                    $i_InventorySystem_Item_Code,
                    $i_InventorySystem_Item_Name,
                    $i_InventorySystem['Category'],
                    $i_InventorySystem['Location'],
                    $i_InventorySystem['Caretaker'],
                    $i_InventorySystem['FundingSource'],
                    $i_InventorySystem_ItemStatus_Normal,
                    $i_InventorySystem_ItemStatus_Repair,
                    $i_InventorySystem_ItemStatus_Damaged,
                    $i_LastModified
                );
                
                if (sizeof($arr_tmp_bulk_item) > 0) {
                    for ($i = 0; $i < sizeof($arr_tmp_bulk_item); $i ++) {
                        list ($tmp_bulk_item_id, $tmp_location_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$i];
                        
                        $sql = "SELECT 
								DISTINCT ITEM.ItemID,
								ITEM.ItemCode,
								concat(" . $linventory->getInventoryItemNameByLang("CAT.") . ",'>'," . $linventory->getInventoryItemNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
								concat(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								LOCATION.FundingSourceID,
								L.LocationID
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN 
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN 
								INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID and LOCATION.Quantity>0 and LOCATION.LocationID=$tmp_location_id) LEFT OUTER JOIN
								INVENTORY_LOCATION AS L ON  (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID) 
						WHERE 
								ITEM.ItemType=2 AND 
								ITEM.ItemID = $tmp_bulk_item_id AND
								AG.AdminGroupID='" . $admin_group_id . "'
								AND LOCATION.GroupInCharge=$admin_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
						LIMIT 0,1
						";
                        
                        $arr_bulk_result = $linventory->returnArray($sql, 5);
                        
                        if (sizeof($arr_bulk_result) > 0) {
                            for ($j = 0; $j < sizeof($arr_bulk_result); $j ++) {
                                list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $bulk_funding, $bulk_funding_id, $location_id) = $arr_bulk_result[$j];
                                
                                $qty_normal = "";
                                $qty_repair = "";
                                $qty_damage = "";
                                $date_modified = "";
                                
                                $sql = "SELECT 
										if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
										if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
										if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
										BL.DateModified
								FROM
										INVENTORY_ITEM_BULK_LOG AS BL 
								WHERE
										BL.ItemID = $item_id AND
										BL.GroupInCharge = $admin_group_id AND
										BL.LocationID = $location_id AND BL.FundingSource=$bulk_funding_id and
										BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
								ORDER BY
										DateModified DESC, RecordID DESC
								LIMIT 0,1										
								";
                                
                                $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                                
                                if (sizeof($tmp_bulk_item_status) > 0) {
                                    list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                                }
                                
                                if ($qty_normal == "")
                                    $qty_normal = " - ";
                                if ($qty_repair == "")
                                    $qty_repair = " - ";
                                if ($qty_damage == "")
                                    $qty_damage = " - ";
                                if ($date_modified == "")
                                    $date_modified = " - ";
                                    
                                    // $file_content .= "\"$item_code\",\"$item_name\",\"$item_cat\",\"$location\",\"$resource_mgt_group\",\"$qty_normal\",\"$qty_repair\",\"$qty_damage\",\"$date_modified\"\n";
                                $rows[] = array(
                                    $item_code,
                                    $item_name,
                                    $item_cat,
                                    $location,
                                    $resource_mgt_group,
                                    $bulk_funding,
                                    $qty_normal,
                                    $qty_repair,
                                    $qty_damage,
                                    $date_modified
                                );
                            }
                        }
                    }
                }
                if (sizeof($arr_tmp_bulk_item) == 0) {
                    $file_content .= "\"$i_no_record_exists_msg\"\n";
                    $rows[] = array(
                        $i_no_record_exists_msg
                    );
                }
                $file_content .= "\n";
                $file_content .= "\n";
            }
        } else 
            if ($groupBy == 4) {
                $single_empty = 0;
                $bulk_empty = 0;
                
                $targetSubCategory = explode(",", $selected_cb);
                if (is_array($targetSubCategory)) {
                    $sub_cat_id_list = implode(",", $targetSubCategory);
                    $sub_cat_cond = "Category2ID IN ($sub_cat_id_list)";
                }
                
                for ($i = 0; $i < sizeof($targetSubCategory); $i ++) {
                    $targetSubCatID = $targetSubCategory[$i];
                    $sql = "SELECT CONCAT(" . $linventory->getInventoryNameByLang("a.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ") FROM INVENTORY_CATEGORY AS a INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID) WHERE Category2ID = '".$targetSubCatID."' ORDER BY a.DisplayOrder, b.DisplayOrder";
                    $CategoryFullName = $linventory->returnVector($sql);
                    
                    // $file_content .= "\"$CategoryFullName[0]\"\n";
                    // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystemItemStatus\",\"$i_LastModified\"\n";
                    
                    $rows[] = array(
                        $CategoryFullName[0]
                    );
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_Name,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem['Location'],
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystemItemStatus,
                        $i_LastModified
                    );
                    
                    // single items #
                    $sql = "SELECT 
						a.ItemID 
				FROM 
						INVENTORY_ITEM AS a
				WHERE
						a.RecordStatus = 1 AND
						a.ItemType = 1 AND 
						a.Category2ID IN ($targetSubCatID)
				ORDER BY
						a.ItemCode
				";
                    $arr_tmp_single_item = $linventory->returnArray($sql, 1);
                    if (sizeof($arr_tmp_single_item) > 0) {
                        for ($j = 0; $j < sizeof($arr_tmp_single_item); $j ++) {
                            list ($tmp_single_item_id) = $arr_tmp_single_item[$j];
                            
                            if ($TargetSubLocation != "") {
                                $Cond_SubLocation = " AND L.LocationID = $TargetSubLocation ";
                            }
                            
                            $sql = "SELECT 
								DISTINCT ITEM.ItemID,
								ITEM.ItemCode,
								CONCAT(" . $linventory->getInventoryNameByLang("CAT.") . ",'>'," . $linventory->getInventoryNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
								CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								" . $linventory->getInventoryNameByLang("funding2.") . "
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
								INVENTORY_ITEM_SINGLE_EXT AS EXT ON (ITEM.ITEMID =  EXT.ITEMID) LEFT OUTER JOIN
								INVENTORY_LOCATION AS L ON (L.LocationID = EXT.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (EXT.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=EXT.FundingSource)
								left join INVENTORY_FUNDING_SOURCE as funding2 on (funding2.FundingSourceID=EXT.FundingSource2)
						WHERE 
								ITEM.ItemID = $tmp_single_item_id AND
								ITEM.RecordStatus = 1 AND
								ITEM.ItemType=1 AND 
								ITEM.Category2ID='" . $targetSubCatID . "' 
								$SubLocationCond 
						LIMIT 0,1";
                            $arr_single_result = $linventory->returnArray($sql);
                            
                            if (is_array($arr_single_result) && count($arr_single_result) > 0) {
                                foreach ($arr_single_result as $Key => $Value) {
                                    list ($item_id, $item_code, $item_cat, $item_name, $location, $resource_mgt_group, $item_funding, $item_funding2) = $Value;
                                    
                                    $sql = "SELECT 
										NewStatus, DateModified 
								FROM 
										INVENTORY_ITEM_SINGLE_STATUS_LOG
								WHERE
										ItemID = $item_id AND
										Action IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . ")
								ORDER BY
										DateModified DESC 
								LIMIT 0,1
								";
                                    $tmp_single_item_status = $linventory->returnArray($sql, 2);
                                    
                                    if (sizeof($tmp_single_item_status) > 0) {
                                        list ($item_status, $date_modified) = $tmp_single_item_status[0];
                                    }
                                    
                                    if ($item_status == "")
                                        $status = " - ";
                                    else 
                                        if ($item_status == ITEM_STATUS_NORMAL)
                                            $status = $i_InventorySystem_ItemStatus_Normal;
                                        else 
                                            if ($item_status == ITEM_STATUS_DAMAGED)
                                                $status = $i_InventorySystem_ItemStatus_Damaged;
                                            else 
                                                if ($item_status == ITEM_STATUS_REPAIR)
                                                    $status = $i_InventorySystem_ItemStatus_Repair;
                                                else
                                                    $status = " - ";
                                        
                                        // $file_content .= "\"$item_code\",\"$item_name\",\"$item_cat\",\"$location\",\"$resource_mgt_group\",\"$status\",\"$date_modified\"\n";
                                    $item_funding_str = $item_funding . ($item_funding2 ? ", " . $item_funding2 : "");
                                    $rows[] = array(
                                        $item_code,
                                        $item_name,
                                        $item_cat,
                                        $location,
                                        $resource_mgt_group,
                                        $item_funding_str,
                                        $status,
                                        $date_modified
                                    );
                                }
                            } else {
                                $single_empty ++;
                            }
                        }
                    }
                    if ((sizeof($arr_tmp_single_item) == 0) || (sizeof($arr_tmp_single_item) == $single_empty)) {
                        $file_content .= "\"$i_no_record_exists_msg\"\n";
                        $rows[] = array(
                            $i_no_record_exists_msg
                        );
                    }
                    // single items end #
                    
                    // Bulk item start #
                    $sql = "SELECT 
						a.ItemID, b.LocationID, b.GroupInCharge, b.FundingSourceID   
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID $cond)
				WHERE
						a.RecordStatus = 1 AND
						a.ItemType = 2 AND 
						a.Category2ID IN ($targetSubCatID)
				ORDER BY
						a.ItemCode, b.RecordID
				";
                    $arr_tmp_bulk_item = $linventory->returnArray($sql);
                    
                    // $file_content .= "\"$i_InventorySystem_Item_Code\",\"$i_InventorySystem_Item_Name\",\"".$i_InventorySystem['Category']."\",\"".$i_InventorySystem['Location']."\",\"".$i_InventorySystem['Caretaker']."\",\"$i_InventorySystem_ItemStatus_Normal\",\"$i_InventorySystem_ItemStatus_Damaged\",\"$i_InventorySystem_ItemStatus_Repair\",\"$i_LastModified\"\n";
                    
                    $rows[] = array(
                        $i_InventorySystem_Item_Code,
                        $i_InventorySystem_Item_Name,
                        $i_InventorySystem['Category'],
                        $i_InventorySystem['Location'],
                        $i_InventorySystem['Caretaker'],
                        $i_InventorySystem['FundingSource'],
                        $i_InventorySystem_ItemStatus_Normal,
                        $i_InventorySystem_ItemStatus_Damaged,
                        $i_InventorySystem_ItemStatus_Repair,
                        $i_LastModified
                    );
                    
                    if (sizeof($arr_tmp_bulk_item) > 0) {
                        for ($j = 0; $j < sizeof($arr_tmp_bulk_item); $j ++) {
                            list ($tmp_bulk_item_id, $tmp_bulk_item_location, $tmp_bulk_group_id, $tmp_bulk_funding_id) = $arr_tmp_bulk_item[$j];
                            
                            if ($TargetSubLocation != "") {
                                $Cond_SubLocation = " AND L.LocationID = $TargetSubLocation ";
                            }
                            
                            $sql = "SELECT 
								DISTINCT ITEM.ItemID,
								ITEM.ItemCode,
								CONCAT(" . $linventory->getInventoryNameByLang("CAT.") . ",'>'," . $linventory->getInventoryNameByLang("SUBCAT.") . "),
								" . $linventory->getInventoryItemNameByLang("ITEM.") . ",
								L.LocationID,
								CONCAT(" . $linventory->getInventoryItemNameByLang("building.") . ",'>'," . $linventory->getInventoryItemNameByLang("floor.") . ",'>'," . $linventory->getInventoryItemNameByLang("L.") . "),
								" . $linventory->getInventoryNameByLang("AG.") . ",
								" . $linventory->getInventoryNameByLang("funding.") . ",
								LOCATION.GroupInCharge,
								LOCATION.FundingSourceID,
								L.LocationID
						FROM 
								INVENTORY_ITEM AS ITEM INNER JOIN 
								INVENTORY_CATEGORY AS CAT ON (ITEM.CategoryID = CAT.CategoryID) INNER JOIN
								INVENTORY_CATEGORY_LEVEL2 AS SUBCAT ON (ITEM.Category2ID = SUBCAT.Category2ID) INNER JOIN
								INVENTORY_ITEM_BULK_LOCATION AS LOCATION ON (ITEM.ITEMID = LOCATION.ITEMID and LOCATION.Quantity>0) LEFT OUTER JOIN
								INVENTORY_LOCATION AS L ON (L.LocationID = LOCATION.LocationID $cond1) INNER JOIN
								INVENTORY_LOCATION_LEVEL AS floor ON (L.LocationLevelID = floor.LocationLevelID) INNER JOIN
								INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) INNER JOIN
								INVENTORY_ADMIN_GROUP AS AG ON (LOCATION.GroupInCharge = AG.AdminGroupID $cond2) 
								left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=LOCATION.FundingSourceID)
						WHERE 
								ITEM.ItemID = $tmp_bulk_item_id AND
								ITEM.RecordStatus = 1 AND
								ITEM.ItemType = 2 AND 
								ITEM.Category2ID='" . $targetSubCatID . "' 
								AND LOCATION.LocationID=$tmp_bulk_item_location AND LOCATION.GroupInCharge=$tmp_bulk_group_id AND LOCATION.FundingSourceID=$tmp_bulk_funding_id
								$SubLocationCond 
						";
                            $arr_bulk_result = $linventory->returnArray($sql);
                            
                            if (is_array($arr_bulk_result) && count($arr_bulk_result) > 0) {
                                foreach ($arr_bulk_result as $Key => $Value) {
                                    $qty_normal = "";
                                    $qty_repair = "";
                                    $qty_damage = "";
                                    $date_modified = "";
                                    
                                    list ($item_id, $item_code, $item_cat, $item_name, $locationID, $location, $resource_mgt_group, $bulk_funding, $bulk_group_id, $bulk_funding_id, $location_id) = $Value;
                                    
                                    $sql = "SELECT 
										if(BL.QtyNormal=\"\",' - ',BL.QtyNormal),
										if(BL.QtyDamage=\"\",' - ',BL.QtyDamage),
										if(BL.QtyRepair=\"\",' - ',BL.QtyRepair),
										BL.DateModified
								FROM
										INVENTORY_ITEM_BULK_LOG AS BL 
								WHERE
										BL.ItemID = $item_id AND
										BL.LocationID = $locationID AND
										BL.GroupInCharge=$bulk_group_id and BL.FundingSource=$bulk_funding_id and
										BL.ACTION IN (" . ITEM_ACTION_PURCHASE . "," . ITEM_ACTION_UPDATESTATUS . "," . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ")
								ORDER BY
										DateModified DESC, RecordID DESC
								LIMIT 0,1										
								";
                                    
                                    $tmp_bulk_item_status = $linventory->returnArray($sql, 4);
                                    
                                    if (sizeof($tmp_bulk_item_status) > 0) {
                                        list ($qty_normal, $qty_damage, $qty_repair, $date_modified) = $tmp_bulk_item_status[0];
                                    }
                                    
                                    if ($qty_normal == "")
                                        $qty_normal = " - ";
                                    if ($qty_repair == "")
                                        $qty_repair = " - ";
                                    if ($qty_damage == "")
                                        $qty_damage = " - ";
                                    if ($date_modified == "")
                                        $date_modified = " - ";
                                        
                                        // $file_content .= "\"$item_code\",\"$item_name\",\"$item_cat\",\"$location\",\"$resource_mgt_group\",\"$qty_normal\",\"$qty_damage\",\"$qty_repair\",\"$date_modified\"\n";
                                    $rows[] = array(
                                        $item_code,
                                        $item_name,
                                        $item_cat,
                                        $location,
                                        $resource_mgt_group,
                                        $bulk_funding,
                                        $qty_normal,
                                        $qty_damage,
                                        $qty_repair,
                                        $date_modified
                                    );
                                }
                            } else {
                                $bulk_empty ++;
                            }
                        }
                    }
                    if ((sizeof($arr_tmp_bulk_item) == 0) || (sizeof($arr_tmp_bulk_item) == $bulk_empty)) {
                        $file_content .= "\"$i_no_record_exists_msg\"\n";
                        $rows[] = array(
                            $i_no_record_exists_msg
                        );
                    }
                }
            }

$display = $file_content;

intranet_closedb();

$filename = "item_status_unicode.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>