<?php
// Using: 
/********************************* Modification Log *************************************
 * Date: 2020-02-24 (Tommy)
 * fix bulk item funding source location sql 
 * 
 * Date: 2020-01-20 (Tommy)
 * add checking $special_feature['eInventory']['CheckForOnlyOneInvoice'] to get record's purchase price which have only one invoice
 * 
 * Date: 2019-10-14 (Henry)
 * add $sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem']
 * 
 * Date: 2019-09-02 (Tommy)
 * add maximum width and height to school badge
 * 
 * 2019-08-16 (Henry)
 * set the purchase to 0 for the write off item
 * 
 * 2019-06-20 (Tommy)
 *      add "funding source" print out
 * 
 * 2019-05-13 (Henry)
 *      Security fix: SQL without quote
 * 
 * 2018-05-25 Isaac
 *      Fixed purchase date overflow issue for both $PurchaseDateFrom and $PurchaseDateTo to ensure data export shown the correct data [case #W137639]
 * 
 * 2018-02-22 Henry
 *      add access right checking [Case#E135442]
 *
 * 2017-05-05 (Cameron)
 * 		improved: add filter "DisplayNotReqStocktakingItem"
 *  
 * 2017-02-03 (Cameron)
 * 		- set memory size to unlimit to prevent exhausted memeory error [case #T112390]
 * 		- unset recordset variable after used to release memory
 * 		- pass parameter to returnArray() to return numerical indices array
 * 			or use returnResultSet() to reduce the array size as half
 * 
 * 2016-10-14 (Cameron)
 * 		fix bug: Column value shift position for some records [case #X105373]
 * 		Reason: substr can break multi-byte characters and produce wrong fragment
 * 		Solution: break on end of html tag '</td>'
 * 
 * 2016-05-18 (Henry)
 * 		file created
 * 
 ****************************************************************************************/

ini_set('memory_limit','1G');
ini_set('max_execution_time','600');

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");

$DebugMode=1;
intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li 			= new libfilesystem();

$DisplaySchoolLogo = $ShowSchoolLogo;
$DisplayCategory = $ShowCategory;
$DisplaySubCategory = $ShowSubCategory;
$DisplayPurchasePrice = $ShowPurchasePrice;
$DisplayPurchasePriceTotal = $ShowPurchasePriceTotal;
$DisplayLocation = $ShowLocation;
$DisplayWriteOffInfo = $ShowWriteOffInfo;
$DisplaySignature = $ShowSignature;
$DisplayRemark = $ShowRemark;
$DisplayItemCode = $ShowItemCode;
$DisplayUnitPrice = $ShowUnitPrice;
$DisplaySponsoringBody = $ShowSponsoringBody;

// counting the position of total amount #
$totalAmountPosition = 6;

if ($DisplayCategory)
    $totalAmountPosition ++;
if ($DisplaySubCategory)
    $totalAmountPosition ++;
if ($DisplayItemCode)
    $totalAmountPosition ++;
if ($DisplayUnitPrice)
    $totalAmountPosition ++;
if ($DisplayPurchasePrice)
    $totalAmountPosition ++;
if ($DisplayLocation)
    $totalAmountPosition ++;
if ($ShowSN)
    $totalAmountPosition ++;
if ($ShowQuoNo)
    $totalAmountPosition ++;
if ($ShowSupplier)
    $totalAmountPosition ++;
if ($ShowTenderNo)
    $totalAmountPosition ++;
if ($ShowInvNo)
    $totalAmountPosition ++;
if ($DisplayWriteOffInfo)
    $totalAmountPosition ++;
if ($DisplaySignature)
    $totalAmountPosition ++;
if ($DisplayRemark)
    $totalAmountPosition ++;
if ($DisplaySponsoringBody)
    $totalAmountPosition ++;

if ($IsSignature)
{
	$SignatureBlock = "<table border='0' width='100% cellpadding='20' cellspacing='20' align='center'><tr><td>".$Lang['eInventory']['SignatureSubmittedBy']." : </td></tr><tr><td>".$Lang['eInventory']['SignatureCheckedBy']." : </td></tr><tr><td>".$Lang['eInventory']['SignatureAudit']." : </td></tr></table>";
}
// prepare for purchase period
/*
if ($PurchaseDateFrom=="yyyy-mm-dd")
	$PurchaseDateFrom = "";
if ($PurchaseDateTo=="yyyy-mm-dd")
	$PurchaseDateTo = "";
if ($PurchaseDateFrom!="" && $PurchaseDateTo=="")
{
	// from to 
	$PurchaseDateTo = $PurchaseDateFrom;
} elseif ($PurchaseDateFrom=="" && $PurchaseDateTo!="")
{
	$PurchaseDateFrom = $PurchaseDateTo;
}
if ($PurchaseDateFrom!="" && $PurchaseDateTo!="")
{
	$sql_purchase_period = " AND unix_timestamp([PURCHASE_DATE])>=unix_timestamp('{$PurchaseDateFrom}') AND unix_timestamp([PURCHASE_DATE])<=unix_timestamp('{$PurchaseDateTo}') ";
}
*/

if ($CountStocktakeOnly && $StockTakeDateFrom!="" && $StockTakeDateEnd!="")
{
	//debug($StockTakeDateFrom, $StockTakeDateEnd);
	$sql = "SELECT 
									DISTINCT a.ItemID
							FROM 
									INVENTORY_ITEM_SINGLE_STATUS_LOG AS a 
							WHERE 
									a.Action IN (2,3,4) AND (a.RecordDate BETWEEN '$StockTakeDateFrom' and '$StockTakeDateEnd')
							ORDER BY
									a.ItemID
							";
	$StocktakeResultArray = $linventory->returnVector($sql);
	
	$sql = "SELECT DISTINCT a.ItemID FROM INVENTORY_ITEM_BULK_LOG AS a WHERE a.Action = 2  AND (a.RecordDate BETWEEN '$StockTakeDateFrom' and '$StockTakeDateEnd')
							ORDER BY a.ItemID";
	$StocktakeBulkResultArray = $linventory->returnVector($sql);
	//debug_r($StocktakeBulkResultArray);
} else
{
	$StockTakeDateFrom = null;
	$StockTakeDateEnd = null;
}


if ($CountByPrice)
{
	//debug($FilterSingleItemPriceFrom, $FilterSingleItemPriceEnd);
	//debug($FilterBulkItemPriceFrom, $FilterBulkItemPriceEnd);
} else
{
	$FilterSingleItemPriceFrom = null;
	$FilterSingleItemPriceEnd = null;
	$FilterBulkItemPriceFrom = null;
	$FilterBulkItemPriceEnd = null;
}

if ($PurchaseDateFrom!="")
{
	$sql_purchase_period = " AND [PURCHASE_DATE]>='{$PurchaseDateFrom}' ";
}
if ($PurchaseDateTo!="")
{
	$sql_purchase_period .= " AND [PURCHASE_DATE]<='{$PurchaseDateTo}' ";
}

$school_data = explode("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root."/file/schoolbadge.txt");
$badge_size = getimagesize($PATH_WRT_ROOT."/file/".$school_badge);
$badge_width = $badge_size[0];
$badge_height = $badge_siz[1];
$maxWidth = 120;
$maxHeight = 60;
if($badge_width > $maxWidth){
    $badge_width = $maxWidth;
}
if($badge_height > $maxHeight){
    $badge_height = $maxHeight;
}
if ($school_badge != "" && $DisplaySchoolLogo)
{
    $badge_image = "<img src='".$PATH_WRT_ROOT."/file/$school_badge' width='$badge_width'px height='$badge_height'px>";
}

$school_header = "<table width='20%' border='0' align='center' ><tr>";
if ($badge_image!="")
	$school_header .= "<td rowspan='2'>". $badge_image ."</td>";
$school_header .= "<td class='eSportprinttext' align='center' nowrap='nowrap'><font size='5'>".$i_InventorySystem_Report_FixedAssetsRegister."</font></td></tr>
						<tr><td class='eSportprinttext' align='center' nowrap='nowrap'><font size='5'>".$school_name."</font></td></tr>
					</table>";
					
/*
	$report_header .= "<tr><td class=\"eSportprinttext\" width=\"10%\">".$i_InventorySystem['Category']."</td>";
	$report_header .= "<td class=\"eSportprinttext\">";
	if($CatID != "")
	{
		$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY WHERE CategoryID = $CatID";
		$arr_cat_name = $linventory->returnVector($sql);
		$report_header .= $arr_cat_name[0];
	
		if($Cat2ID != "")
		{
			$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $CatID AND Category2ID = $Cat2ID";
			$arr_sub_cat_name = $linventory->returnVector($sql);
			$report_header .= " > ".$arr_sub_cat_name[0];
		}
	}
	else
	{
		$report_header .= $i_InventorySystem_ViewItem_All_Category;
	}
	$report_header .= "</td></tr>";
	
	$report_header .= "<tr><td class=\"eSportprinttext\" width=\"10%\">$i_InventorySystem_Location_Level</td>";
	$report_header .= "<td class=\"eSportprinttext\">";
	if($LocationID != "")
	{
		$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_LOCATION_LEVEL WHERE LocationLevelID = $LocationID";
		$arr_location = $linventory->returnVector($sql);
		$report_header .= $arr_location[0];
	
		if($Location2ID != "")
		{
			$sql = "SELECT ".$linventory->getInventoryNameByLang()." FROM INVENTORY_LOCATION WHERE LocationLevelID = $LocationID AND LocationID = $Location2ID";
			$arr_sub_locaton = $linventory->returnVector($sql);
			$report_header .= " > ".$arr_sub_locaton[0];
		}
	}
	else
	{
		$report_header .= $i_InventorySystem_ViewItem_All_Location_Level;
	}
	$report_header .= "</td></tr>";
		
	if($CatID != "" || $Cat2ID != "" || $LocationID != "" || $Location2ID != "")
		$cond .= " WHERE ";
	
	if($CatID != "")
	{
		$cond .= " ITEM.CategoryID = $CatID ";
		if($Cat2ID != "")
			$cond .= " AND ITEM.Category2ID = $Cat2ID ";
		$cnt++;
	}
	
	if($LocationID != "")
	{
		if($cnt > 0)
			$cond .= " AND ";
		$cond .= " LL.LocationLevelID = $LocationID ";
		if($Location2ID != "")
			$cond .= " AND L.LocationID = $Location2ID ";
	}
*/		                                                                                                                          

if (!$display_not_req_stocktaking) {
	$single_req_stocktaking_fields = ",i.StockTakeOption ";
	$single_req_stocktaking_join = "INNER JOIN INVENTORY_ITEM i ON (i.ItemID=a.ItemID AND i.ItemType=1) ";

	$bulk_req_stocktaking_fields = ",i.StockTakeOption ";
	$bulk_req_stocktaking_join = "INNER JOIN INVENTORY_ITEM i ON (i.ItemID=a.ItemID AND i.ItemType=2) ";
}
else {
	$single_req_stocktaking_fields = "";
	$single_req_stocktaking_join = "";
	
	$bulk_req_stocktaking_fields = "";
	$bulk_req_stocktaking_join = "";
}

# preloading: single items
$sql = "SELECT
		a.PurchaseDate, 
		if(a.PurchasedPrice != '',a.PurchasedPrice,'0') as PurchasePrice,
		if(a.UnitPrice != '',a.UnitPrice,'0') as UnitPrice,
		a.LocationID,
		CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.").") as LocationName,
		d.FundingType,
		".$linventory->getInventoryNameByLang("d.")." as FundingName,
		e.ApproveDate as WriteOffDate,
		e.WriteOffReason,
		a.ItemRemark as Remark,
		a.ItemID,
		".$linventory->getInventoryNameByLang("d2.")." as FundingName2,
		a.UnitPrice1,
		a.UnitPrice2,
		d2.FundingType as FundingType2".$single_req_stocktaking_fields." 
FROM
		INVENTORY_ITEM_SINGLE_EXT AS a ".$single_req_stocktaking_join."INNER JOIN
		INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
		INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
		INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID) INNER JOIN
		INVENTORY_FUNDING_SOURCE AS d ON (a.FundingSource = d.FundingSourceID) LEFT OUTER JOIN
		INVENTORY_FUNDING_SOURCE AS d2 ON (a.FundingSource2 = d2.FundingSourceID) LEFT OUTER JOIN
		INVENTORY_ITEM_WRITE_OFF_RECORD AS e ON (a.ItemID = e.ItemID AND e.RecordStatus = 1)
WHERE
		a.ItemID>0 ".str_replace("[PURCHASE_DATE]", "a.purchasedate", $sql_purchase_period)."
"; 
$arr_all_records = $linventory->returnResultSet($sql);

if (!$display_not_req_stocktaking) { 
	$arr_all_records =  $linventory->FilterForStockTakeSingle($arr_all_records);
}
 
for ($i=0,$iMax=sizeof($arr_all_records); $i<$iMax; $i++)
{
	$arr_all_single_items[$arr_all_records[$i]["ItemID"]][] = $arr_all_records[$i];
}
unset($arr_all_records);

# preloading: bulk items PURCHASE DATE
# check Quantity > 0
if(!$display_writeoff)
{
	$qty_con = "and b.Quantity > 0";
}
$sql = "SELECT
				DISTINCT IF(a.PurchaseDate IS NULL, '', a.PurchaseDate) as PurchaseDate, a.ItemID".$bulk_req_stocktaking_fields."
		FROM
				INVENTORY_ITEM_BULK_LOG as a ".$bulk_req_stocktaking_join." 
				left join INVENTORY_ITEM_BULK_LOCATION as b on (b.ItemID=a.ItemID)
		WHERE
				a.Action = 1 ".str_replace("[PURCHASE_DATE]", "purchasedate", $sql_purchase_period)."
				$qty_con
				and (a.RecordStatus=0 or a.RecordStatus is NULL) 
				ORDER BY a.PurchaseDate ASC
		";

/*
$sql = "SELECT
				DISTINCT IF(PurchaseDate IS NULL, '', PurchaseDate), ItemID
		FROM
				INVENTORY_ITEM_BULK_LOG
		WHERE
				Action = 1 ".str_replace("[PURCHASE_DATE]", "purchasedate", $sql_purchase_period)."
				ORDER BY PurchaseDate ASC
		";
*/		
$arr_all_records = $linventory->returnResultSet($sql);

if (!$display_not_req_stocktaking) { 
	$arr_all_records =  $linventory->FilterForStockTakeBulk($arr_all_records);
}

for ($i=0,$iMax=sizeof($arr_all_records); $i<$iMax; $i++)
{
	$arr_all_bulk_purchases[$arr_all_records[$i]["ItemID"]][] = $arr_all_records[$i];
}
unset($arr_all_records);

if($groupBy==1)
{
	# get all location #
	$arrTempTargetRoom = $TargetRoom;
	if(array_shift($arrTempTargetRoom) == ""){
		$TargetRoom = $arrTempTargetRoom;
	}
	$selected_cb = implode(",",$TargetRoom);

	$sql = "Select 
				DISTINCT a.LocationID, CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("a.").")
			FROM 
				INVENTORY_LOCATION AS a INNER JOIN
				INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (b.BuildingID = building.BuildingID) 
			WHERE 
				a.LocationID in (".$selected_cb.") 
			ORDER BY 
				b.DisplayOrder, a.DisplayOrder";
	$arr_location = $linventory->returnArray($sql,null,2);
	
	for($a=0,$aMax=sizeof($arr_location); $a<$aMax; $a++)
	{	
		
		list($locationID, $location_name) = $arr_location[$a];
		
		if(!$display_writeoff)
		{
			$wo_con = " and a.RecordStatus=1 ";
			$wo_conb = " and c.Quantity>0";
		}
		
		# Get Bulk item id, item code, item name, item description #
		$sql = "SELECT 
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					".$linventory->getInventoryNameByLang("a.").",
					".$linventory->getInventoryDescriptionNameByLang("a.").",
					".$linventory->getInventoryNameByLang("c1.").",
					".$linventory->getInventoryNameByLang("c2.")."
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
					INVENTORY_LOCATION AS d ON (b.LocationID = d.LocationID OR c.LocationID = d.LocationID)
					LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
					LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID 
				WHERE
					d.LocationID IN ($locationID)
					 $wo_con
		/*			and c.Quantity is not NULL 	*/
				ORDER BY
					a.ItemCode
				";
		$arr_result = $linventory->returnArray($sql,null,2);
		
		$location_table_content .= $school_header;
		$location_table_content .= "<table border=\"0\" width=\"100%\" class=\"eSporttableborder\" style='table-layout:auto' align=\"center\" cellspacing=\"0\" cellpadding=\"3\">";
		if ($DisplayCategory)
			$location_table_content .= "<col width=\"95px\">\n";
		if ($DisplaySubCategory)
			$location_table_content .= "<col width=\"95px\">\n";
		if($DisplayItemCode)
			$location_table_content .= "<col width=\"80px\">\n";
		$location_table_content .= "<col width=\"60px\">\n";
		$location_table_content .= "<col width=\"50px\">\n";
		$location_table_content .= "<col width=\"50px\">\n";
		if($DisplayUnitPrice)
			$location_table_content .= "<col width=\"50px\">\n";
		$location_table_content .= "<col width=\"50px\">\n";
		if ($DisplayPurchasePrice)
			$location_table_content .= "<col width=\"50px\">\n";
		if ($DisplayLocation)
			$location_table_content .= "<col width=\"80px\">\n";
		if ($DisplayWriteOffInfo)
			$location_table_content .= "<col width=\"80px\">\n";
		if ($DisplaySignature)
			$location_table_content .= "<col width=\"80px\">\n";
		if ($DisplayRemark)
			$location_table_content .= "<col width=\"80px\">\n";
			
		$location_table_content .= "<thead><tr><td class=\"eSporttdborder eSportprinttext\" bgcolor=\"#FFFFFF\" colspan=\"". $totalAmountPosition ."\" style=\"word-wrap:normal\" ><b>".$i_InventorySystem_Item_Location." : ".intranet_htmlspecialchars($location_name)."</b></td></tr>";
		$location_table_content .= "<tr>";
		
		if ($DisplayCategory){
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem['Category']."</td>";
		}
		if ($DisplaySubCategory){
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem['SubCategory']."</td>";
		}
		if($DisplayItemCode){
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" rowspan=\"2\">$i_InventorySystem_Item_Code</td>";
		}
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Item."</td>";
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Description."</td>";		
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Purchase_Date."</td>";
		if($DisplayUnitPrice){
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Sch_Unit_Price ."(". $Lang['eInventory']['Avg'] .")</td>";
		}
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Quantity."</td>";
		$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"". ($DisplaySponsoringBody?3:2)."\" align=\"center\">".$i_InventorySystem_Report_Col_Cost."</td>";
		
		if ($DisplayPurchasePrice){
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."</td>";
		}
		if ($DisplayLocation)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Location."</td>";
		
		# S/N
		if($ShowSN)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Serial_Num."</td>";			
		
		# Quotation Number
		if($ShowQuoNo)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Quot_Num."</td>";			
					
		# Supplier
		if($ShowSupplier)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Supplier_Name."</td>";			
		
		# Tender No
		if($ShowTenderNo)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Tender_Num."</td>";			
		
		# Invoice Number
		if($ShowInvNo)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Invoice_Num."</td>";			
		
		if ($DisplayWriteOffInfo)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off."</td>";
		if ($DisplaySignature)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Signature."</td>";
		if ($DisplayRemark)
			$location_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Remarks."</td></tr>";
		$location_table_content .="<tr >
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['School']."</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['Government']."</td>";
		if($DisplaySponsoringBody){
			$location_table_content .="<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['SponsoringBody']."</td>";
		}
		$location_table_content .="</tr></thead>";
		
		$rows_shown = 0;
		$totalPurchasePrice = 0;
		$iMax = sizeof($arr_result); 
		if($iMax>0 )
		{
			for($i=0; $i<$iMax; $i++)
			{
				list($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];
				$arr_sub_result = array();
				$arr_purchase_date = array();
				
				# select the records and according to period
				if($item_type == ITEM_TYPE_SINGLE)
				{
					$arr_sub_result = $arr_all_single_items[$item_id];
				} elseif($item_type == ITEM_TYPE_BULK)
				{						
					$arr_purchase_date = $arr_all_bulk_purchases[$item_id];
				}
				if ((!is_array($arr_sub_result) || sizeof($arr_sub_result)<1) && (!is_array($arr_purchase_date) || sizeof($arr_purchase_date)<1))
				{
					continue;
				}
				
				# filter out no stock-taken item if necessary
				if ($item_type==ITEM_TYPE_SINGLE && is_array($StocktakeResultArray) && !in_array($item_id, $StocktakeResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				} elseif ($item_type==ITEM_TYPE_BULK && is_array($StocktakeBulkResultArray) && !in_array($item_id, $StocktakeBulkResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				}
				
				if ($item_type == ITEM_TYPE_SINGLE && ($FilterSingleItemPriceFrom!=null || $FilterSingleItemPriceEnd!=null))
				{
					$rs = $arr_sub_result[0];
					$purchase_date = $rs['PurchaseDate']; 
					$purchase_price = $rs['PurchasePrice']; 
					$unit_price = $rs['UnitPrice']; 
					$location_id = $rs['LocationID']; 
					$location_name = $rs['LocationName']; 
					$funding_type = $rs['FundingType']; 
					$funding_name = $rs['FundingName']; 
					$write_off_date = $rs['WriteOffDate']; 
					$write_off_reason = $rs['WriteOffReason']; 
					$remark = $rs['Remark']; 
					$this_item_id = $rs['ItemID']; 
					$funding_name2 = $rs['FundingName2']; 
					$unit_price1 = $rs['UnitPrice1']; 
					$unit_price2 = $rs['UnitPrice2']; 
					$funding_type2 = $rs['FundingType2'];
					unset($rs);
										
					if ($FilterSingleItemPriceFrom!=null && $unit_price<$FilterSingleItemPriceFrom)
					{
						continue;
					} elseif ($FilterSingleItemPriceEnd!=null && $unit_price>$FilterSingleItemPriceEnd)
					{
						continue;
					}
				}
				
				$rows_shown ++;
				$location_table_content_new_initial = "<tr>";
				if ($DisplayCategory)
					$location_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">".$item_category."</td>";
				if ($DisplaySubCategory)
					$location_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">".$item_subcategory."</td>";
				if($DisplayItemCode)			
					$location_table_content_new_initial.="<td width=\"100px\" class=\"eSporttdborder eSportprinttext\">".$item_code."</td>";
				$location_table_content_new_initial.="<td width=\"100px\" class=\"eSporttdborder eSportprinttext\">".$item_name."</td>";
				$location_table_content_new_initial.="<td width=\"100px\" class=\"eSporttdborder eSportprinttext\">".(($item_description!="")?nl2br($item_description):'&nbsp;')."</td>";
				
				if($item_type == ITEM_TYPE_SINGLE)
				{
						for($j=0,$jMax=sizeof($arr_sub_result); $j<$jMax; $j++)
						{
							$rs = $arr_sub_result[$j];
							$purchase_date = $rs['PurchaseDate']; 
							$purchase_price = $rs['PurchasePrice']; 
							$unit_price = $rs['UnitPrice']; 
							$location_id = $rs['LocationID']; 
							$location_name = $rs['LocationName']; 
							$funding_type = $rs['FundingType']; 
							$funding_name = $rs['FundingName']; 
							$write_off_date = $rs['WriteOffDate']; 
							$write_off_reason = $rs['WriteOffReason']; 
							$remark = $rs['Remark']; 
							$this_item_id = $rs['ItemID']; 
							$funding_name2 = $rs['FundingName2']; 
							$unit_price1 = $rs['UnitPrice1']; 
							$unit_price2 = $rs['UnitPrice2']; 
							$funding_type2 = $rs['FundingType2'];
							unset($rs);
							
							//$quantity = 1; 
							$quantity = $write_off_date ? "0" : "1";
							if ($unit_price>0)
								$purchase_price = $unit_price * $quantity;
							else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
                            	$purchase_price = 0;
                            }
                            
							$totalPurchasePrice +=$purchase_price;
							
							if ($purchase_date=="0000-00-00")
								$purchase_date = "--";
							
							$location_table_content .= $location_table_content_new_initial;
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$purchase_date."</td>";
							
							if($DisplayUnitPrice)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($unit_price,2)."</td>";
							
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".(($quantity!="")?$quantity:'&nbsp;')."</td>";
							
							/*
							if($ShowFundingName){
								$str_funding_name = "<br />(".$funding_name.")";
							}
							*/
							$str_col_fund = " align='right' ";
							/*
							if ($purchase_price==0)
							{
								$str_funding_name = "";
							}
							*/
							
							$Cost_Ary = array();
							$Funding_ary = array();
							$Cost_Ary[$funding_type] += $unit_price1;
							if($ShowFundingName)
								$Funding_ary[$funding_type] .= ($Funding_ary[$funding_type] ? ", ":"") . $funding_name;
							if($funding_type2)
							{
								$Cost_Ary[$funding_type2] += $unit_price2;
								if($ShowFundingName)
									$Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", ":"") . $funding_name2;
							}
								
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2).($Funding_ary[ITEM_SCHOOL_FUNDING]? "<br>(".$Funding_ary[ITEM_SCHOOL_FUNDING].")":"")."</td>";
							$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2).($Funding_ary[ITEM_GOVERNMENT_FUNDING]? "<br>(".$Funding_ary[ITEM_GOVERNMENT_FUNDING].")":"")."</td>";
							if($DisplaySponsoringBody)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING],2).($Funding_ary[ITEM_SPONSORING_BODY_FUNDING]? "<br>(".$Funding_ary[ITEM_SPONSORING_BODY_FUNDING].")":"")."</td>";
								
							/*
							if($funding_type == ITEM_SCHOOL_FUNDING)
							{
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($purchase_price,2).$str_funding_name."</td>";
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($purchase_price,2).$str_funding_name."</td>";
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
							}
							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
							{
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($purchase_price,2).$str_funding_name."</td>";
							}
							*/
							
							if ($DisplayPurchasePrice)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($purchase_price,2)."</td>";
							if ($DisplayLocation)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($location_name!="")?intranet_htmlspecialchars($location_name):'&nbsp;')."</td>";
								
							# S/N, Quotation No, Supplier, Tender No., Invoice No.
							$sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID='".$item_id."'";
							$result2 = $linventory->returnResultSet($sql);
							$rs = $result2[0];
							$thisSerialNumber = $rs['SerialNumber']; 
							$thisQuotationNo = $rs['QuotationNo']; 
							$thisSupplierName = $rs['SupplierName']; 
							$thisTenderNo = $rs['TenderNo']; 
							$thisInvoiceNo = $rs['InvoiceNo'];
							unset($rs);
							
							if($ShowSN)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisSerialNumber!="")?$thisSerialNumber:'&nbsp;')."</td>";
							if($ShowQuoNo)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisQuotationNo!="")?$thisQuotationNo:'&nbsp;')."</td>";
							if($ShowSupplier)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisSupplierName!="")?$thisSupplierName:'&nbsp;')."</td>";
							if($ShowTenderNo)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisTenderNo!="")?$thisTenderNo:'&nbsp;')."</td>";
							if($ShowInvNo)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisInvoiceNo!="")?$thisInvoiceNo:'&nbsp;')."</td>";
							
								
							if ($DisplayWriteOffInfo)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($write_off_date)!="")?$write_off_date:'&nbsp;').((trim($write_off_reason))?"(".$write_off_reason.")":'&nbsp;')."</td>";
							if ($DisplaySignature)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($signature)!="")?$signature:'&nbsp;')."</td>";
							if ($DisplayRemark)
								$location_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($remark)!="")?nl2br($remark):'&nbsp;')."</td>";
							$location_table_content .= "</tr>";
						}	// end loop $arr_sub_result
						unset($arr_sub_result);
				}	// single item
				if($item_type == ITEM_TYPE_BULK)
				{
					$final_purchase_date = "";					
					$jMax = sizeof($arr_purchase_date);
					if($jMax>0)
					{
						$final_purchase_date .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						for($j=0; $j<$jMax; $j++)
						{
							$purchase_date = $arr_purchase_date[$j]['PurchaseDate'];
							if($purchase_date != '')
							{
								/*
								if($j!=0){
									$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
									$final_purchase_date .= "<tr height=\"5px\"><td></td></tr>";
								}else{
									$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
								}
								*/
								$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
							}else{
								$final_purchase_date .= '<tr><td class=\"eSportprinttext\"> </td></tr>';
							}
						}
						$final_purchase_date .= "</table>";
						$location_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$final_purchase_date</td>";
					}
					else
					{
						$location_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\"> - </td>";
					}
					unset($arr_purchase_date);
					
					### GET COST, UNIT PRICE, PURCHASED PRICE ###
					$total_school_cost = 0;
					$total_gov_cost = 0;
					$final_unit_price = 0;
					$final_purchase_price = 0;
					$total_qty = 0;
					$tmp_unit_price = 0;
					$tmp_purchase_price = 0;
					$written_off_qty = 0;
					$location_qty = 0;
					$ini_total_qty = 0;
			
					## get Total purchase price ##
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}else{
					    //$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					    $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}

					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$total_purchase_price = $tmp_result[0];
						# [Case#B70033]
						if($total_purchase_price == 0){
							$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
							$tmp_result = $linventory->returnVector($sql);
							if($tmp_result[0] > 0)
								$total_purchase_price = $tmp_result[0];
						}
					}
					
					$sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$item_remark = implode(";<br />", array_unique($tmp_result));
					}
					
					## get Total Qty Of the target Item ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,null,2);
					if(sizeof($tmp_result)>0){
						list($totalQty) = $tmp_result[0];
					}
					
					## get written-off qty ##
					$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,null,2);
					if(sizeof($tmp_result)>0){
						list($written_off_qty) = $tmp_result[0];
					}
					
					## get the qty in the target location ##
					$sql = "SELECT sum(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."' AND LocationID = '".$locationID."'";
					$tmp_result = $linventory->returnArray($sql,null,2);
					if(sizeof($tmp_result)>0){
						list($location_qty) = $tmp_result[0];
					}

					## Location origial qty (by location only)##
					$sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND LocationID = '".$locationID."'";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$ini_location_qty = $tmp_result[0] + $location_qty;
					}else{
						$ini_location_qty = $location_qty;
					}
					
					$sql = "SELECT 
									a.FundingSource, 
									b.FundingType,
									".$linventory->getInventoryNameByLang("b.")."
							FROM 
									INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
									INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
							WHERE 
									a.ItemID = '".$item_id."' AND
									a.Action IN (1,7) AND
									a.LocationID = '".$locationID."' AND 
									a.FundingSource IS NOT NULL 
									and (a.RecordStatus=0 or a.RecordStatus is NULL) 
							";
					$tmp_result = $linventory->returnArray($sql,null,2);
					if(sizeof($tmp_result)>0){
						list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
					}
					unset($tmp_result);
					
					$ini_total_qty = $totalQty + $written_off_qty;
					
					if ($ini_total_qty>0)
						$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
					//$total_funding_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $total_funding_cost = $total_purchase_price;
					}else{
					    $total_funding_cost = $location_qty*$final_unit_price;
					}

					if($display_writeoff)
						$total_funding_cost = $ini_location_qty*$final_unit_price;
					
					$final_purchase_price = $total_funding_cost;
					if($display_writeoff)
						$final_purchase_price = $location_qty*$final_unit_price;
					
					$totalPurchasePrice +=$final_purchase_price;
					
					if ($FilterBulkItemPriceFrom!=null || $FilterBulkItemPriceEnd!=null)
					{
						if ($FilterBulkItemPriceFrom!=null && $total_funding_cost<$FilterBulkItemPriceFrom)
						{
							$rows_shown --;
							continue;
						} elseif ($FilterBulkItemPriceEnd!=null && $total_funding_cost>$FilterBulkItemPriceEnd)
						{
							$rows_shown --;
							continue;
						}
					}
					
					$location_table_content .= $location_table_content_new_initial;
					
					if($ShowFundingName){
						$str_funding_name = "<br>(".$funding_name.")";
					} else
					{
						$str_col_fund = " align='right' ";
					}
					
					/*
					if ($total_funding_cost<=0)
					{
						$str_funding_name = "";
					}	
					*/
					
					### Get Qty By Location ###
					$sql = "SELECT 
									a.Quantity,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.").")
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID)
							WHERE
									a.ItemID = '".$item_id."' AND 
									a.LocationID = '".$locationID."'
							";
					$arr_tmp_qty = $linventory->returnArray($sql,null,2);
					$jMax = sizeof($arr_tmp_qty); 
					//debug_pr($arr_tmp_qty);
					if($jMax>0)
					{
						$display_qty = 0;
						$display_location_name = "";
						
						for($j=0; $j<$jMax; $j++)
						{
							list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
							
							if($qty != "")
							{
								$display_qty = $qty;
							}
							if($location_name != "")
							{
								$display_location_name = intranet_htmlspecialchars($location_name)."<BR>";
							}
						}
					}
					unset($arr_tmp_qty);
										
					if($jMax == 0)
					{
						$display_qty = 0;
						$display_location_name = "&nbsp;";		
					}
					
					if($DisplayUnitPrice)
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($final_unit_price,2)."</td>";
					$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($location_qty)."</td>";
					if($funding_type == ITEM_SCHOOL_FUNDING)
					{
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						if($DisplaySponsoringBody)
							$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";

					}
					if($funding_type == ITEM_GOVERNMENT_FUNDING)
					{
									
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
						if($DisplaySponsoringBody)
							$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
					}				
					if($funding_type == ITEM_SPONSORING_BODY_FUNDING)	
					{
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						if($DisplaySponsoringBody)
							$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
					}	
					if(!in_array($funding_type, array(ITEM_SCHOOL_FUNDING, ITEM_GOVERNMENT_FUNDING, ITEM_SPONSORING_BODY_FUNDING))){
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
					}
						
					if ($DisplayPurchasePrice)
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($final_purchase_price,2)."</td>";
					if ($DisplayLocation)
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">$display_location_name</td>";	
				
					# Quotation No, Supplier, Tender No., Invoice No.	
					$sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID='".$item_id."'";
					$result2 = $linventory->returnArray($sql,null,2);
					if(!empty($result2))
					{
						$thisDataAry = array();
						
						foreach($result2 as $k2=>$d2)
						{
							list($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
							if($thisQuotationNo)
								$thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
							if($thisSupplierName)
								$thisDataAry['Supplier'][] = trim($thisSupplierName);
							if($thisTenderNo)
								$thisDataAry['TenderNo'][] = trim($thisTenderNo);
							if($thisInvoiceNo)
								$thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
						}
					}
					unset($result2);
					
					if($ShowSN)	
					{
						# no S/N for bulk item
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >&nbsp;</td>";	
					}
					
					if($ShowQuoNo)
					{
						$QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ",array_unique($thisDataAry['QuotationNo'])) : "&nbsp;";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $QnoNoStr ."</td>";
					}
					
					if($ShowSupplier)
					{
						$SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ",array_unique($thisDataAry['Supplier'])) : "&nbsp;";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $SupplierStr ."</td>";
					}
						
					if($ShowTenderNo)
					{
						$TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ",array_unique($thisDataAry['TenderNo'])) : "&nbsp;";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $TenderNoStr ."</td>";
					}
					
					if($ShowInvNo)
					{
						$InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ",array_unique($thisDataAry['InvoiceNo'])) : "&nbsp;";
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $InvoiceNoStr ."</td>";
					}
					
					if ($DisplayWriteOffInfo)
					{
						### Get Write-Off Date & Reason ###
						$display_write_off = "";
						$sql = "SELECT
										ApproveDate,
										WriteOffQty,
										WriteOffReason
								FROM
										INVENTORY_ITEM_WRITE_OFF_RECORD
								WHERE
										ItemID = '".$item_id."' AND 
										LocationID = '".$locationID."'
										 AND RecordStatus = 1
								";
						$arr_write_off = $linventory->returnArray($sql,null,2);
						$jMax = sizeof($arr_write_off); 
						if($jMax>0)
						{
							$display_write_off .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<$jMax; $j++)
							{
								list($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
								
								if($write_off_date != "")
								{								
									$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
									$tmp_write_off = $write_off_date."<BR>".$write_off_qty."<BR>".$write_off_reason;
																
									if($j!=0){
										$display_write_off .= "<tr height=\"5px\"><td></td></tr>";
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}else{
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}
								}
							}
							$display_write_off .= "</table>";
	
							if($display_write_off != "")
								$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">$display_write_off</td>";
							else
								$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
						else
						{
							$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
						unset($arr_write_off);
					}
					### Empty Space for Signature and remark ###
					If ($DisplaySignature)
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
					If ($DisplayRemark)	
						$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">". ((trim($item_remark)!="")?$item_remark:'&nbsp;') ."</td>";
				}	// end bulk item			
			}		// end loop $i
			if($DisplayPurchasePriceTotal && $rows_shown!=0)
			    $location_table_content .= "<tr><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='" . ($totalAmountPosition - 4) . "'><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='2' align='right'>" . $Lang['eInventory']['TotalAssets'] . "</td><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" align='right'>" . number_format($totalPurchasePrice, 2) . "</td><td class=\"eSporttdborder eSportprinttext\" colspan='" . (21 - $totalAmountPosition) . "'></td></tr>";
		  }	// end $iMax > 0
		unset($arr_result);
		
		if($rows_shown==0 )
		{
		    $location_table_content .= "<tr><td colspan=\"". $totalAmountPosition ."\" class=\"eSporttdborder eSportprinttext \" align=\"center\">$i_no_record_exists_msg</td></tr>";
		}
		$location_table_content.="</table>";
		
		
				
		if ($IsSignature)
		{
			$location_table_content .= $SignatureBlock;
		}
		
		if($a < sizeof($arr_location) - 1)
		{
			$location_table_content .= "<div style='page-break-after:always;'><br /></div>";
		}
	}	// end loop for $aMax
	unset($arr_location);
			
}	// end $groupBy==1
else if($groupBy==2)
{
	$arr_item_type = array(array("1",$i_InventorySystem_ItemType_Single),array("2",$i_InventorySystem_ItemType_Bulk));
	
	$aMax = sizeof($arr_item_type); 
	if($aMax>0)
	{
		for($a=0; $a<$aMax; $a++)
		{
			list($item_type, $item_type_name) = $arr_item_type[$a];
			
			$table_content .= $school_header;
			$table_content .= "<table border=\"0\" width=\"100%\" class=\"eSporttableborder\" style='table-layout:auto' align=\"center\" cellspacing=\"0\" cellpadding=\"3\" >";
			if ($DisplayCategory)
				$table_content .= "<col width=\"95px\">\n";
			if ($DisplaySubCategory)
				$table_content .= "<col width=\"95px\">\n";
			if($DisplayItemCode)
				$table_content .= "<col width=\"80px\">\n";
			$table_content .= "<col width=\"60px\">\n";
			$table_content .= "<col width=\"50px\">\n";
			$table_content .= "<col width=\"50px\">\n";
			$table_content .= "<col width=\"50px\">\n";
			
			$table_content .= "<col width=\"50px\">\n";
			if ($DisplayPurchasePrice)
				$table_content .= "<col width=\"50px\">\n";
			if ($DisplayLocation)
				$table_content .= "<col width=\"80px\">\n";
			if ($DisplayWriteOffInfo)
				$table_content .= "<col width=\"80px\">\n";
			if ($DisplaySignature)	
				$table_content .= "<col width=\"80px\">\n";
			if ($DisplayRemark)
				$table_content .= "<col width=\"80px\">\n";
				
			$table_content .= "<thead><tr><td class=\"eSporttdborder eSportprinttext\" bgcolor=\"#FFFFFF\" colspan=\"". $totalAmountPosition ."\" style=\"word-wrap:normal\"><b>".$i_InventorySystem['item_type']." : ".$item_type_name."</b></td></tr>";
			$table_content .= "<tr>";
			
			if ($DisplayCategory){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem['Category']."</td>";
			}
			if ($DisplaySubCategory){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem['SubCategory']."</td>";
			}
			if($DisplayItemCode){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" rowspan=\"2\">$i_InventorySystem_Item_Code</td>";
			}
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Item."</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Description."</td>";
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Purchase_Date."</td>";
			if($DisplayUnitPrice){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\" nowrap>".$i_InventorySystem_Report_Col_Sch_Unit_Price ."(". $Lang['eInventory']['Avg'] .")</td>";
			}
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Quantity."</td>";
			
			$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"". ($DisplaySponsoringBody?3:2) ."\" align=\"center\">".$i_InventorySystem_Report_Col_Cost."</td>";
			if ($DisplayPurchasePrice){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."</td>";
			}
			if ($DisplayLocation)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Location."</td>";
			
			# S/N - for single item only
			if($ShowSN && $item_type==ITEM_TYPE_SINGLE)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Serial_Num."</td>";			
			
			# Quotation Number
			if($ShowQuoNo)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Quot_Num."</td>";			
						
			# Supplier
			if($ShowSupplier)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Supplier_Name."</td>";			
			
			# Tender No
			if($ShowTenderNo)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Tender_Num."</td>";			
			
			# Invoice Number
			if($ShowInvNo)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Invoice_Num."</td>";			
				
			if ($DisplayWriteOffInfo)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off."</td>";
			if ($DisplaySignature)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Signature."</td>";
			if ($DisplayRemark)
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Remarks."</td></tr>";
			$table_content .= "<tr>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['School']."</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['Government']."</td>";
			if($DisplaySponsoringBody){
				$table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['SponsoringBody']."</td>";
			}
			$table_content .= "</tr></thead>";
									
			if(!$display_writeoff)
			{
				$wo_con = " and a.RecordStatus=1 ";
				$wo_conb = " and c.Quantity>0";
			}
			
			if($PurchaseDateFrom && $PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."' and b.PurchaseDate<='". $PurchaseDateTo ."') or 
										(e.PurchaseDate>='". $PurchaseDateFrom ."' and e.PurchaseDate<='". $PurchaseDateTo ."'))";
			}
			if($PurchaseDateFrom && !$PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate>='". $PurchaseDateFrom ."') or 
										(e.PurchaseDate>='". $PurchaseDateFrom ."'))";
			}
			if(!$PurchaseDateFrom && $PurchaseDateTo)
			{
				$purchase_date_sql = " and ((b.PurchaseDate<='". $PurchaseDateTo ."') or 
										(e.PurchaseDate<='". $PurchaseDateTo ."'))";
			}
			
			$sql = "SELECT 
						DISTINCT a.ItemID,
						a.ItemType,
						a.ItemCode,
						".$linventory->getInventoryNameByLang("a.").",
						".$linventory->getInventoryDescriptionNameByLang("a.").",
						".$linventory->getInventoryNameByLang("c1.").",
						".$linventory->getInventoryNameByLang("c2.")."
					FROM
						INVENTORY_ITEM AS a LEFT OUTER JOIN
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
						INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID $wo_conb) LEFT OUTER JOIN
						INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID) 
						left outer join INVENTORY_ITEM_BULK_LOG as e on (e.ItemID=a.ItemID)
						LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
						LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
					WHERE
						a.ItemType IN ($item_type)  
						$purchase_date_sql
						$wo_con
					ORDER BY
						a.ItemCode
					";
			$arr_result = $linventory->returnArray($sql,null,2);
		//	".str_replace("[PURCHASE_DATE]", "b.purchasedate", $sql_purchase_period)."
		
			$rows_shown = 0;
			$totalPurchasePrice = 0;
			
			$iMax = sizeof($arr_result); 
			if($iMax>0)
			{		
				for($i=0; $i<$iMax; $i++)
				{
					list($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];
					
					$arr_sub_result = array();
					$arr_purchase_date = array();
					
					# select the records and according to period
					if($item_type == ITEM_TYPE_SINGLE)
					{
						$arr_sub_result = $arr_all_single_items[$item_id];
					} elseif($item_type == ITEM_TYPE_BULK)
					{						
						$arr_purchase_date = $arr_all_bulk_purchases[$item_id];
					}
					if ((!is_array($arr_sub_result) || sizeof($arr_sub_result)<1) && (!is_array($arr_purchase_date) || sizeof($arr_purchase_date)<1))
					{
						continue;
					}
					# filter out no stock-taken item if necessary
					if ($item_type==ITEM_TYPE_SINGLE && is_array($StocktakeResultArray) && !in_array($item_id, $StocktakeResultArray))
					{
						//debug($item_id, $item_code, $item_name);
						continue;						
					} elseif ($item_type==ITEM_TYPE_BULK && is_array($StocktakeBulkResultArray) && !in_array($item_id, $StocktakeBulkResultArray))
					{
						//debug($item_id, $item_code, $item_name);
						continue;						
					}
					
					if ($item_type == ITEM_TYPE_SINGLE && ($FilterSingleItemPriceFrom!=null || $FilterSingleItemPriceEnd!=null))
					{
						$rs = $arr_sub_result[0];
						$purchase_date = $rs['PurchaseDate']; 
						$purchase_price = $rs['PurchasePrice']; 
						$unit_price = $rs['UnitPrice']; 
						$location_id = $rs['LocationID']; 
						$location_name = $rs['LocationName']; 
						$funding_type = $rs['FundingType']; 
						$funding_name = $rs['FundingName']; 
						$write_off_date = $rs['WriteOffDate']; 
						$write_off_reason = $rs['WriteOffReason']; 
						$remark = $rs['Remark']; 
						$this_item_id = $rs['ItemID']; 
						$funding_name2 = $rs['FundingName2']; 
						$unit_price1 = $rs['UnitPrice1']; 
						$unit_price2 = $rs['UnitPrice2']; 
						$funding_type2 = $rs['FundingType2'];
						unset($rs);
						
						if ($FilterSingleItemPriceFrom!=null && $unit_price<$FilterSingleItemPriceFrom)
						{
							continue;
						} elseif ($FilterSingleItemPriceEnd!=null && $unit_price>$FilterSingleItemPriceEnd)
						{
							continue;
						}
					}
					
					$rows_shown ++;
					
					$table_content_new_initial = "<tr>";
					if ($DisplayCategory)
						$table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_category</td>";
					if ($DisplaySubCategory)
						$table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_subcategory</td>";
					if($DisplayItemCode)
						$table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
					$table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
					if($item_description != "")
						$table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">". nl2br($item_description) ."</td>";
					else
						$table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
					
					if($item_type == ITEM_TYPE_SINGLE)
					{
							for($j=0,$jMax=sizeof($arr_sub_result); $j<$jMax; $j++)
							{
								$rs = $arr_sub_result[$j];
								$purchase_date = $rs['PurchaseDate']; 
								$purchase_price = $rs['PurchasePrice']; 
								$unit_price = $rs['UnitPrice']; 
								$location_id = $rs['LocationID']; 
								$location_name = $rs['LocationName']; 
								$funding_type = $rs['FundingType']; 
								$funding_name = $rs['FundingName']; 
								$write_off_date = $rs['WriteOffDate']; 
								$write_off_reason = $rs['WriteOffReason']; 
								$remark = $rs['Remark']; 
								$this_item_id = $rs['ItemID']; 
								$funding_name2 = $rs['FundingName2']; 
								$unit_price1 = $rs['UnitPrice1']; 
								$unit_price2 = $rs['UnitPrice2']; 
								$funding_type2 = $rs['FundingType2'];
								unset($rs);
								
								//$quantity = 1;
								$quantity = $write_off_date ? "0" : "1";
								if ($unit_price>0) 
								{
									$purchase_price = $unit_price * $quantity;
								} 
								else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
	                            	$purchase_price = 0;
	                            }
	                            
								$totalPurchasePrice +=$purchase_price;
									
								if ($purchase_date=="0000-00-00")
									$purchase_date = "--";
								
								$table_content .= $table_content_new_initial;
								
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap>".$purchase_date."</td>";
								if($DisplayUnitPrice)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($unit_price,2)."</td>";
								
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>".(($quantity!="")?$quantity:'&nbsp;')."</td>";
								
								/*
								if($ShowFundingName){
									$str_funding_name = "<br />(".$funding_name.")";
								} else
								{
									$col_fund_align = " align='right' ";
								}
								# no need to funding name if cost = 0
								if ($purchase_price<=0)
								{
									$str_funding_name = "";
								}
								if($funding_type == ITEM_SCHOOL_FUNDING)
								{
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format($purchase_price,2).$str_funding_name."</td>";
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
								}
								if($funding_type == ITEM_GOVERNMENT_FUNDING)
								{
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format($purchase_price,2).$str_funding_name."</td>";
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
								}
								if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
								{
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format($purchase_price,2).$str_funding_name."</td>";
								}
								if(!in_array($funding_type, array(ITEM_SCHOOL_FUNDING, ITEM_GOVERNMENT_FUNDING, ITEM_SPONSORING_BODY_FUNDING))){
									$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
									$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
									$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
								}
								*/
								
								$Cost_Ary = array();
								$Funding_ary = array();
								$Cost_Ary[$funding_type] += $unit_price1;
								if($ShowFundingName)
									$Funding_ary[$funding_type] .= ($Funding_ary[$funding_type] ? ", ":"") . $funding_name;
								if($funding_type2)
								{
									$Cost_Ary[$funding_type2] += $unit_price2;
									if($ShowFundingName)
										$Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", ":"") . $funding_name2;
								}
								
								$str_col_fund = " align='right' ";
									
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2).($Funding_ary[ITEM_SCHOOL_FUNDING]? "<br>(".$Funding_ary[ITEM_SCHOOL_FUNDING].")":"")."</td>";
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2).($Funding_ary[ITEM_GOVERNMENT_FUNDING]? "<br>(".$Funding_ary[ITEM_GOVERNMENT_FUNDING].")":"")."</td>";
								if($DisplaySponsoringBody)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING],2).($Funding_ary[ITEM_SPONSORING_BODY_FUNDING]? "<br>(".$Funding_ary[ITEM_SPONSORING_BODY_FUNDING].")":"")."</td>";
						
								if ($DisplayPurchasePrice)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($purchase_price,2)."</td>";
								
								if ($DisplayLocation)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($location_name!="")?intranet_htmlspecialchars($location_name):'&nbsp;')."</td>";
								
								# S/N, Quotation No, Supplier, Tender No., Invoice No.
								$sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID='".$item_id."'";
								$result2 = $linventory->returnArray($sql);
								list($thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $result2[0];
								
								if($ShowSN)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisSerialNumber!="")?$thisSerialNumber:'&nbsp;')."</td>";
								if($ShowQuoNo)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisQuotationNo!="")?$thisQuotationNo:'&nbsp;')."</td>";
								if($ShowSupplier)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisSupplierName!="")?$thisSupplierName:'&nbsp;')."</td>";
								if($ShowTenderNo)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisTenderNo!="")?$thisTenderNo:'&nbsp;')."</td>";
								if($ShowInvNo)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisInvoiceNo!="")?$thisInvoiceNo:'&nbsp;')."</td>";
									
								if ($DisplayWriteOffInfo)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($write_off_date)!="")?$write_off_date:'&nbsp;').((trim($write_off_reason))?"(".$write_off_reason.")":'&nbsp;')."</td>";
								if ($DisplaySignature)	
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($signature)!="")?$signature:'&nbsp;')."</td>";
								if ($DisplayRemark)
									$table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($remark)!="")?nl2br($remark):'&nbsp;')."</td>";					
								
								$table_content .= "</tr>";
							}	// end loop for $arr_sub_result
							unset($arr_sub_result);
					} elseif ($item_type == ITEM_TYPE_BULK)
					{
						$final_purchase_date = "";
						
						$jMax = sizeof($arr_purchase_date); 
						if($jMax>0)
						{
							$final_purchase_date .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<$jMax; $j++)
							{
								$purchase_date = $arr_purchase_date[$j]['PurchaseDate'];
								if($purchase_date != '')
								{
									/*
									if($j!=0){
										$final_purchase_date .= "<tr><td class=\"eSportprinttext\" nowrap='nowrap'>$purchase_date</td></tr>";
										$final_purchase_date .= "<tr height=\"5px\"><td></td></tr>";
									}else{
										$final_purchase_date .= "<tr><td class=\"eSportprinttext\" nowrap='nowrap'>$purchase_date</td></tr>";
									}
									*/
									$final_purchase_date .= "<tr><td class=\"eSportprinttext\" nowrap>$purchase_date</td></tr>";
								}else{
									//$final_purchase_date .= '<tr><td class=\"eSportprinttext\"> </td></tr>';
								}
							}
							$final_purchase_date .= "</table>";
							$table_content_new_initial.="<td class=\"eSporttdborder eSportprinttext\" vlaign=\"top\">$final_purchase_date</td>";
						}
						else
						{
							$table_content_new_initial.="<td class=\"eSporttdborder eSportprinttext\"> - </td>";
						}
						unset($arr_purchase_date);
						
						
						### GET COST, UNIT PRICE, PURCHASED PRICE ###
						$total_school_cost = 0;
						$total_gov_cost = 0;
						$total_sponsor_cost = 0;
						
						$final_unit_price = 0;
						$final_purchase_price = 0;
						$total_qty = 0;
						$tmp_unit_price = 0;
						$tmp_purchase_price = 0;
						$written_off_qty = 0;
						$location_qty = 0;
						$ini_total_qty = 0;
						$item_remark = "";
												
						## get Total purchase price ##
						$count = $linventory->getNumOfBulkItemInvoice($item_id);
						if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
						    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
						}else{
						    //$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
						    $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
						}
						$tmp_result = $linventory->returnVector($sql);
						if(sizeof($tmp_result)>0){
							$total_purchase_price = $tmp_result[0];
							# [Case#B70033]
							if($total_purchase_price == 0){
								$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
								$tmp_result = $linventory->returnVector($sql);
								if($tmp_result[0] > 0)
									$total_purchase_price = $tmp_result[0];
							}
						}
						
						$sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
						$tmp_result = $linventory->returnVector($sql);
						if(sizeof($tmp_result)>0){
							$item_remark = implode(";<br />", array_unique($tmp_result));
						}

						## get Total Qty Of the target Item ##
						$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."'";
						$tmp_result = $linventory->returnArray($sql,null,2);
						if(sizeof($tmp_result)>0){
							list($totalQty) = $tmp_result[0];
						}
						## get written-off qty ##
						$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."'";
						$tmp_result = $linventory->returnArray($sql,null,2);
						if(sizeof($tmp_result)>0){
							list($written_off_qty) = $tmp_result[0];
						}
						
						## get the qty in the target location ##
						/* no use in this type of report
						$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
						$tmp_result = $linventory->returnArray($sql,1);
						if(sizeof($tmp_result)>0){
							list($location_qty) = $tmp_result[0];
						}
						*/
						
						/*
						## retrieve funding src and cost and name...
						$sql = "SELECT 
										a.FundingSource, 
										b.FundingType,
										".$linventory->getInventoryNameByLang("b.")."
								FROM 
										INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
										INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
								WHERE 
										a.ItemID = $item_id AND
										a.Action IN (1,7) AND
										a.FundingSource IS NOT NULL
								";
						$tmp_result = $linventory->returnArray($sql,3);
						if(sizeof($tmp_result)>0){
							list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
						}
						*/
						
						### Get Qty By Location ###
						$sql = "SELECT 
										a.Quantity,
										a.LocationID,
										CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
										f.FundingType,
										". $linventory->getInventoryNameByLang("f.") ." as funding_name,
										a.FundingSourceID
								FROM
										INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
										INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
										INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
										INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID) 
										inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
								WHERE
										a.ItemID = '".$item_id."' 
								";
						$arr_tmp_qty = $linventory->returnArray($sql, null, 2);
						
						$jMax = sizeof($arr_tmp_qty); 
						if($jMax>0)
						{
							$display_qty = 0;
							$display_location_name = "";
							unset($location_showed);
							$location_showed = array();
						
							$display_location_name .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<$jMax; $j++)
							{
								list($qty, $location_id, $location_name, $funding_source, $funding_name, $fund_id) = $arr_tmp_qty[$j];
								if($qty != "")
								{
									$display_qty = $display_qty+$qty;
								}
								if($location_name != "" && !$location_showed[$location_id])
								{
									if($j!=0){
										$display_location_name .= "<tr height=\"5px\"><td></td></tr>";
										$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
									}else{
										$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
									}
									$location_showed[$location_id] = true;							
								}
							}
							$display_location_name .= "</table>";
							
							/*
							if ($ShowFundingName){
								$str_funding_name = "<br />(".$funding_name.")";
							} else
							{
								$str_col_fund = " align='right' ";
							}
							*/
							$final_purchase_price = $total_purchase_price;
							
							$ini_total_qty = $totalQty + $written_off_qty;
							if ($ini_total_qty>0)
								$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
							//$total_fund_cost = $ini_total_qty*($total_purchase_price/$ini_total_qty);
							$count = $linventory->getNumOfBulkItemInvoice($item_id);
							if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
							    $total_fund_cost = $total_purchase_price;
							}else{
							    $total_fund_cost = $final_unit_price * $display_qty;
							}

							#debug($total_purchase_price, $ini_total_qty, $total_fund_cost);
							
							$totalPurchasePrice +=$total_fund_cost;
							
							if ($FilterBulkItemPriceFrom!=null || $FilterBulkItemPriceEnd!=null)
							{
								if ($FilterBulkItemPriceFrom!=null && $total_fund_cost<$FilterBulkItemPriceFrom)
								{
									$rows_shown --;
									continue;
								} elseif ($FilterBulkItemPriceEnd!=null && $total_fund_cost>$FilterBulkItemPriceEnd)
								{
									$rows_shown --;
									continue;
								}
							}
							
							
							$table_content .= $table_content_new_initial;
								
							if($DisplayUnitPrice)
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($final_unit_price,2)."</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($display_qty)."</td>";
							
							# retrieve funding data
							$tmp_funding_src = array();
							for($j=0; $j<$jMax; $j++)
							{
								list($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
								
								# [Case#V72270]
								## Location origial qty (by location only)##
								if($display_writeoff){
									$sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND FundingSourceID = $fund_id";
									$tmp_result = $linventory->returnVector($sql);
									if(sizeof($tmp_result)>0 && $j==0)
										$qty = $tmp_result[0] + $qty;
									unset($tmp_result);
								}
								$count = $linventory->getNumOfBulkItemInvoice($item_id);
								if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
								    $tmp_funding_src[$funding_type][$fund_id]['cost'] = $total_purchase_price;
								}else{
								    $tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
								}
								$tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
								$tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
							}
							unset($arr_tmp_qty);
							
							$str_col_fund = " align='right' ";
							for($type_i=1;$type_i<=($DisplaySponsoringBody?3:2);$type_i++)
							{
								$this_field = "";
								if(!empty($tmp_funding_src[$type_i]))
								{
									$this_fund_cost = 0;
									
									foreach($tmp_funding_src[$type_i] as $k=>$d)
									{
										if($ShowFundingName)
										{
											$this_fund_cost = number_format($d['cost'],2);
											//$this_fund_src = ($d['cost']>0) ? "<br />(". $d['src_name'].")" : ""; 
											$this_fund_src = "<br />(". $d['src_name'].")";
											$this_field .= ($this_field ? "<br><br>" : "") . $this_fund_cost . $this_fund_src;
										}
										else
										{
											$this_fund_cost += $d['cost'];
											$this_field = number_format($this_fund_cost,2); 
										}
									}
								}
								$this_field = $this_field ? $this_field : "0.00";
								
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
							}
							/*
							if($funding_type == ITEM_SCHOOL_FUNDING)
							{	
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_fund_cost,2).$str_funding_name."</td>";
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{				
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_fund_cost,2).$str_funding_name."</td>";
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
							}
							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
							{
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
								$table_content .= "<td class=\"eSporttdborder eSportprinttext\" {$col_fund_align}>".number_format(0, 2)."</td>";
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_fund_cost,2).$str_funding_name."</td>";
							}
							*/
							if ($DisplayPurchasePrice)
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right'>".number_format($total_fund_cost,2)."</td>";
							if ($DisplayLocation)
								$table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_location_name</td>";			
						}	// $jMax > 0
						if($jMax == 0)
						{
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">-</td>";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">-</td>";
						}
						
						# Quotation No, Supplier, Tender No., Invoice No.	
						$sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID='".$item_id."'";
						$result2 = $linventory->returnArray($sql,null,2);
						if(!empty($result2))
						{
							$thisDataAry = array();
							
							foreach($result2 as $k2=>$d2)
							{
								list($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
								if($thisQuotationNo)
									$thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
								if($thisSupplierName)
									$thisDataAry['Supplier'][] = trim($thisSupplierName);
								if($thisTenderNo)
									$thisDataAry['TenderNo'][] = trim($thisTenderNo);
								if($thisInvoiceNo)
									$thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
							}
						}
						
						if($ShowQuoNo)
						{
							$QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ",array_unique($thisDataAry['QuotationNo'])) : "&nbsp;";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $QnoNoStr ."</td>";
						}
						
						if($ShowSupplier)
						{
							$SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ",array_unique($thisDataAry['Supplier'])) : "&nbsp;";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $SupplierStr ."</td>";
						}
							
						if($ShowTenderNo)
						{
							$TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ",array_unique($thisDataAry['TenderNo'])) : "&nbsp;";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $TenderNoStr ."</td>";
						}
						
						if($ShowInvNo)
						{
							$InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ",array_unique($thisDataAry['InvoiceNo'])) : "&nbsp;";
							$table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $InvoiceNoStr ."</td>";
						}	
						
						if ($DisplayWriteOffInfo)
						{
							### Get Write-Off Date & Reason ###
							$display_write_off = "";
							$sql = "SELECT
											ApproveDate,
											WriteOffQty,
											WriteOffReason
									FROM
											INVENTORY_ITEM_WRITE_OFF_RECORD
									WHERE
											ItemID = '".$item_id."' AND RecordStatus = 1
									";
							$arr_write_off = $linventory->returnArray($sql,null,2);
							
							$jMax = sizeof($arr_write_off); 
							if($jMax>0)
							{
								$display_write_off .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
								
								for($j=0; $j<$jMax; $j++)
								{
									list($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
									if($write_off_date != "")
									{
										$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
										$tmp_write_off = $write_off_date."<br />".$write_off_qty."<br />".$write_off_reason;
										
										if($j!=0){
											$display_write_off .= "<tr height=\"5px\"><td></td></tr>";
											$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
										}else{
											$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
										}
									}
								}
								$display_write_off .= "</table>";
								
								if($display_write_off != "")
									$table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_write_off</td>";
								else
									$table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
							}
							else
							{
								$table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
							}
							unset($arr_write_off);
						}	// end display write-off info
						### Empty Space for Signature and remark ###
						if ($DisplaySignature)
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						if ($DisplayRemark)	
							$table_content.="<td class=\"eSporttdborder eSportprinttext\">". ((trim($item_remark)!="")?$item_remark:'&nbsp;') ."</td>";
					}	// end bulk item
				}	// end loop for $i
				unset($arr_result);
				
				if($DisplayPurchasePriceTotal && $rows_shown!=0)
				    $table_content .= "<tr><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='" . ($totalAmountPosition - 4) . "'><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='2' align='right'>" . $Lang['eInventory']['TotalAssets'] . "</td><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" align='right'>" . number_format($totalPurchasePrice, 2) . "</td><td class=\"eSporttdborder eSportprinttext\" colspan='" . (21 - $totalAmountPosition) . "'></td></tr>";
			}	// end $iMax>0
			if($rows_shown==0)
			{
				$table_content .= "<tr><td class=\"\" align='center' style=\"word-wrap:normal\" colspan=\"". $totalAmountPosition ."\">$i_no_record_exists_msg</td></tr>";
			}
			$table_content .= "</table>";
			
			if ($IsSignature)
			{
				$table_content .= $SignatureBlock;
			}
			
			if($a < sizeof($arr_item_type) - 1)
			{
				$table_content .= "<div style='page-break-after:always;'><br /></div>";
			}
		}	// end loop for $aMax
		unset($arr_item_type);
	}	// end $aMax > 0
}	// end groupBy == 2
else if($groupBy==3)
{
	# get all location #
	/*
	$group_namefield = $linventory->getInventoryNameByLang();

	$sql = "SELECT AdminGroupID, $group_namefield as GroupName FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
	$group_array = $linventory->returnArray($sql,2);
		
	if (is_array($group_array))
	{
		foreach($group_array as $Key=>$Value)
		{
			if($_POST['group_cb'.$Value['AdminGroupID']])
			{
				$selected_cb .= $Value['AdminGroupID'].",";
			}
		}
	}

	$selected_cb = substr($selected_cb,0,-1);
	*/
	$arrTempTargetGroup = $targetGroup;
	if(array_shift($arrTempTargetGroup) == "")
	{
		$targetGroup = $arrTempTargetGroup;
	}
	$selected_cb = implode(",",$targetGroup);

	$cond = "WHERE AdminGroupID in (".$selected_cb.")";
	$sql="Select DISTINCT(".$linventory->getInventoryItemNameByLang().") as GroupName, AdminGroupID FROM INVENTORY_ADMIN_GROUP $cond ORDER BY DisplayOrder";
	$arr_group = $linventory->returnArray($sql,null,2);
		
	for($a=0,$aMax=sizeof($arr_group); $a<$aMax; $a++)
	{	
		list($group_name,$admin_group_id) = $arr_group[$a];
		
		$group_table_content .= $school_header;
		$group_table_content .= "<table border=\"0\" width=\"100%\" class=\"eSporttableborder\" style='table-layout:auto' align=\"center\" cellspacing=\"0\" cellpadding=\"3\">";
		if ($DisplayCategory)	
			$group_table_content .= "<col width=\"95px\">\n";
		if ($DisplaySubCategory)
			$group_table_content .= "<col width=\"95px\">\n";
		if($DisplayItemCode)
			$group_table_content .= "<col width=\"80px\">\n";
		$group_table_content .= "<col width=\"60px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		$group_table_content .= "<col width=\"50px\">\n";
		if ($DisplayPurchasePrice)
			$group_table_content .= "<col width=\"50px\">\n";
		if ($DisplayLocation)
			$group_table_content .= "<col width=\"80px\">\n";
		if ($DisplayWriteOffInfo)
			$group_table_content .= "<col width=\"80px\">\n";
		if ($DisplaySignature)
			$group_table_content .= "<col width=\"80px\">\n";
		if ($DisplayRemark)
			$group_table_content .= "<col width=\"80px\">\n";
		
		$group_table_content .= "<thead><tr><td class=\"eSporttdborder eSportprinttext\" bgcolor=\"#FFFFFF\" colspan=\"". $totalAmountPosition ."\" style=\"word-wrap:normal\"><b>".$i_InventorySystem['Caretaker']." : ".intranet_htmlspecialchars($group_name)."</b></td></tr>";
		$group_table_content .= "<tr>";
		
		if ($DisplayCategory){
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem['Category']."</td>";
		}
		if ($DisplaySubCategory){
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem['SubCategory']."</td>";
		}
		if($DisplayItemCode){
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" rowspan=\"2\">$i_InventorySystem_Item_Code</td>";
		}
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Item."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Description."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Purchase_Date."</td>";
		if($DisplayUnitPrice){
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Sch_Unit_Price ."(". $Lang['eInventory']['Avg'] .")</td>";
		}
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Quantity."</td>";
		$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"". ($DisplaySponsoringBody?3:2)."\" align=\"center\">".$i_InventorySystem_Report_Col_Cost."</td>";
		if ($DisplayPurchasePrice){
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice']."</td>";
		}
		if ($DisplayLocation)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Location."</td>";
		
		# S/N
		if($ShowSN)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Serial_Num."</td>";			
		
		# Quotation Number
		if($ShowQuoNo)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Quot_Num."</td>";			
					
		# Supplier
		if($ShowSupplier)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Supplier_Name."</td>";			
		
		# Tender No
		if($ShowTenderNo)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Tender_Num."</td>";			
		
		# Invoice Number
		if($ShowInvNo)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Item_Invoice_Num."</td>";	
			
			
		if ($DisplayWriteOffInfo)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off."</td>";
		if ($DisplaySignature)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Signature."</td>";
		if ($DisplayRemark)
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">".$i_InventorySystem_Report_Col_Remarks."</td></tr>";
		$group_table_content .= "<tr>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['School']."</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['Government']."</td>";
		if($DisplaySponsoringBody){
			$group_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eInventory']['FundingType']['SponsoringBody']."</td>";
		}
		$group_table_content .= "</tr></thead>";
		if(!$display_writeoff)
		{
			$wo_con = " and a.RecordStatus=1 ";
		}
								
		$sql = "SELECT 
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					".$linventory->getInventoryNameByLang("a.").",
					".$linventory->getInventoryDescriptionNameByLang("a.").",
					".$linventory->getInventoryNameByLang("c1.").",
					".$linventory->getInventoryNameByLang("c2.")."
				FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID)
					LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
					LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
				WHERE
					d.AdminGroupID IN ($admin_group_id)
					$wo_con
				ORDER BY
					a.ItemCode
				";
		$arr_result = $linventory->returnArray($sql,null,2);
		
		$rows_shown = 0;
		$totalPurchasePrice = 0;
		
		$iMax = sizeof($arr_result); 
		if($iMax>0)
		{
			for($i=0; $i<$iMax; $i++)
			{
				list($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];
				
				
				$arr_sub_result = array();
				$arr_purchase_date = array();
				
				# select the records and according to period
				if($item_type == ITEM_TYPE_SINGLE)
				{
					$arr_sub_result = $arr_all_single_items[$item_id];
				} elseif($item_type == ITEM_TYPE_BULK)
				{						
					$arr_purchase_date = $arr_all_bulk_purchases[$item_id];
				}
				//debug_pr($arr_sub_result);
				//debug_pr($arr_purchase_date);
				if ((!is_array($arr_sub_result) || sizeof($arr_sub_result)<1) && (!is_array($arr_purchase_date) || sizeof($arr_purchase_date)<1))
				{
					continue;
				}
				
				# filter out no stock-taken item if necessary
				if ($item_type==ITEM_TYPE_SINGLE && is_array($StocktakeResultArray) && !in_array($item_id, $StocktakeResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				} elseif ($item_type==ITEM_TYPE_BULK && is_array($StocktakeBulkResultArray) && !in_array($item_id, $StocktakeBulkResultArray))
				{
					//debug($item_id, $item_code, $item_name);
					continue;						
				}
				
				if ($item_type == ITEM_TYPE_SINGLE && ($FilterSingleItemPriceFrom!=null || $FilterSingleItemPriceEnd!=null))
				{
					$rs = $arr_sub_result[0];
					$purchase_date = $rs['PurchaseDate']; 
					$purchase_price = $rs['PurchasePrice']; 
					$unit_price = $rs['UnitPrice']; 
					$location_id = $rs['LocationID']; 
					$location_name = $rs['LocationName']; 
					$funding_type = $rs['FundingType']; 
					$funding_name = $rs['FundingName']; 
					$write_off_date = $rs['WriteOffDate']; 
					$write_off_reason = $rs['WriteOffReason']; 
					$remark = $rs['Remark']; 
					$this_item_id = $rs['ItemID']; 
					$funding_name2 = $rs['FundingName2']; 
					$unit_price1 = $rs['UnitPrice1']; 
					$unit_price2 = $rs['UnitPrice2']; 
					$funding_type2 = $rs['FundingType2'];
					unset($rs);
					
					if ($FilterSingleItemPriceFrom!=null && $unit_price<$FilterSingleItemPriceFrom)
					{
						continue;
					} elseif ($FilterSingleItemPriceEnd!=null && $unit_price>$FilterSingleItemPriceEnd)
					{
						continue;
					}
				}

				$rows_shown ++;
				
				$group_table_content_new_initial = "<tr>";
				
				if ($DisplayCategory)
					$group_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_category</td>";
				if ($DisplaySubCategory)
					$group_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_subcategory</td>";
				if($DisplayItemCode)
					$group_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
				$group_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
				if($item_description != "")
					$group_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">". nl2br($item_description) ."</td>";
				else
					$group_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
				
				if($item_type == ITEM_TYPE_SINGLE)
				{
						for($j=0,$jMax=sizeof($arr_sub_result); $j<$jMax; $j++)
						{
							$rs = $arr_sub_result[$j];
							$purchase_date = $rs['PurchaseDate']; 
							$purchase_price = $rs['PurchasePrice']; 
							$unit_price = $rs['UnitPrice']; 
							$location_id = $rs['LocationID']; 
							$location_name = $rs['LocationName']; 
							$funding_type = $rs['FundingType']; 
							$funding_name = $rs['FundingName']; 
							$write_off_date = $rs['WriteOffDate']; 
							$write_off_reason = $rs['WriteOffReason']; 
							$remark = $rs['Remark']; 
							$this_item_id = $rs['ItemID']; 
							$funding_name2 = $rs['FundingName2']; 
							$unit_price1 = $rs['UnitPrice1']; 
							$unit_price2 = $rs['UnitPrice2']; 
							$funding_type2 = $rs['FundingType2'];
							unset($rs);
							
							$quantity = $write_off_date ? "0" : "1";
							
							if ($unit_price>0)
								$purchase_price = $unit_price * $quantity;
							else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
                            	$purchase_price = 0;
                            }
                            
							$totalPurchasePrice +=$purchase_price;
							
							if ($purchase_date=="0000-00-00")
								$purchase_date = "--";
							
							$group_table_content .= $group_table_content_new_initial;
							
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$purchase_date."</td>";
							
							if($DisplayUnitPrice)
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($unit_price, 2)."</td>";
							
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".(($quantity!="")?$quantity:'&nbsp;')."</td>";
							
							/*
							if($ShowFundingName){
								$str_funding_name = "<br />(".$funding_name.")";
							} 
							if ($purchase_price==0)
							{
								$str_funding_name = "";
							}
							$str_col_fund = " align='right' ";
							if($funding_type == ITEM_SCHOOL_FUNDING)
							{
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($purchase_price, 2).$str_funding_name."</td>";
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
							}
							if($funding_type == ITEM_GOVERNMENT_FUNDING)
							{
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($purchase_price,2).$str_funding_name."</td>";
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
							}
							if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
							{
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($purchase_price,2).$str_funding_name."</td>";
							}
							if(!in_array($funding_type, array(ITEM_SCHOOL_FUNDING, ITEM_GOVERNMENT_FUNDING, ITEM_SPONSORING_BODY_FUNDING))){
								$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
								$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
								$location_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
							}
							*/
							
							$Cost_Ary = array();
							$Funding_ary = array();
							$Cost_Ary[$funding_type] += $unit_price1;
							if($ShowFundingName)
								$Funding_ary[$funding_type] .= ($Funding_ary[$funding_type] ? ", ":"") . $funding_name;
							if($funding_type2)
							{
								$Cost_Ary[$funding_type2] += $unit_price2;
								if($ShowFundingName)
									$Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", ":"") . $funding_name2;
							}
							
							$str_col_fund = " align='right' ";
								
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2).($Funding_ary[ITEM_SCHOOL_FUNDING]? "<br>(".$Funding_ary[ITEM_SCHOOL_FUNDING].")":"")."</td>";
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2).($Funding_ary[ITEM_GOVERNMENT_FUNDING]? "<br>(".$Funding_ary[ITEM_GOVERNMENT_FUNDING].")":"")."</td>";
							if($DisplaySponsoringBody)
							$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING],2).($Funding_ary[ITEM_SPONSORING_BODY_FUNDING]? "<br>(".$Funding_ary[ITEM_SPONSORING_BODY_FUNDING].")":"")."</td>";
						
							if ($DisplayPurchasePrice)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($purchase_price,2)."</td>";
							if ($DisplayLocation)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($location_name!="")?intranet_htmlspecialchars($location_name):'&nbsp;')."</td>";
							
							# S/N, Quotation No, Supplier, Tender No., Invoice No.
							$sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID=$item_id";
							$result2 = $linventory->returnArray($sql,null,2);
							list($thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $result2[0];
							unset($result2);
							
							if($ShowSN)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisSerialNumber!="")?$thisSerialNumber:'&nbsp;')."</td>";
							if($ShowQuoNo)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisQuotationNo!="")?$thisQuotationNo:'&nbsp;')."</td>";
							if($ShowSupplier)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisSupplierName!="")?$thisSupplierName:'&nbsp;')."</td>";
							if($ShowTenderNo)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisTenderNo!="")?$thisTenderNo:'&nbsp;')."</td>";
							if($ShowInvNo)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".(($thisInvoiceNo!="")?$thisInvoiceNo:'&nbsp;')."</td>";
									
								
							if ($DisplayWriteOffInfo)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($write_off_date)!="")?$write_off_date:'&nbsp;').((trim($write_off_reason))?"(".$write_off_reason.")":'&nbsp;')."</td>";
							if ($DisplaySignature)			
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($signature)!="")?$signature:'&nbsp;')."</td>";
							if ($DisplayRemark)
								$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\">".((trim($remark)!="")?nl2br($remark):'&nbsp;')."</td>";
							$group_table_content .= "</tr>";
						}	// end loop for $jMax
						unset($arr_sub_result);
				}	// end single item
				if($item_type == ITEM_TYPE_BULK)
				{
					$final_purchase_date = "";
					
					$jMax = sizeof($arr_purchase_date); 
					if($jMax>0)
					{
						$final_purchase_date .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						for($j=0; $j<$jMax; $j++)
						{
							$purchase_date = $arr_purchase_date[$j]['PurchaseDate'];
							if($purchase_date != '')
							{
								/*
								if($j!=0){
									$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
									$final_purchase_date .= "<tr height=\"5px\"><td></td></tr>";
								}else{
									$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
								}
								*/
								$final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
							}else{
								$final_purchase_date .= '<tr><td class=\"eSportprinttext\">&nbsp;</td></tr>';
							}
						}
						$final_purchase_date .= "</table>";
						$group_table_content_new_initial.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$final_purchase_date</td>";
					}
					else
					{
						$group_table_content_new_initial.="<td class=\"eSporttdborder eSportprinttext\"> - </td>";
					}
					unset($arr_purchase_date);
					
					### GET COST, UNIT PRICE, PURCHASED PRICE ###
					$total_school_cost = 0;
					$total_gov_cost = 0;
					$final_unit_price = 0;
					$final_purchase_price = 0;
					$total_qty = 0;
					$tmp_unit_price = 0;
					$tmp_purchase_price = 0;
					$written_off_qty = 0;
					$location_qty = 0;
					$ini_total_qty = 0;
					$item_remark = "";
					
					## get Total purchase price ##
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}else{
					    //$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
					    $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					}

					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$total_purchase_price = $tmp_result[0];
						# [Case#B70033]
						if($total_purchase_price == 0){
							$sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
							$tmp_result = $linventory->returnVector($sql);
							if($tmp_result[0] > 0)
								$total_purchase_price = $tmp_result[0];
						}
					}
					
					$sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$item_id."' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$item_remark = implode(";<br />", array_unique($tmp_result));
					}
					
					## get Total Qty Of the target Item ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,null,2);
					if(sizeof($tmp_result)>0){
						list($totalQty) = $tmp_result[0];
					}
					## get written-off qty ##
					$sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."'";
					$tmp_result = $linventory->returnArray($sql,null,2);
					if(sizeof($tmp_result)>0){
						list($written_off_qty) = $tmp_result[0];
					}
					
					## get the qty in the target location ##
					$sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."' AND GroupInCharge = $admin_group_id";
					$tmp_result = $linventory->returnArray($sql,null,2);
					if(sizeof($tmp_result)>0){
						list($location_qty) = $tmp_result[0];
					}
					
					## Location origial qty (by location only)##
					$sql = "SELECT WriteOffQty FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND AdminGroupID = $admin_group_id";
					$tmp_result = $linventory->returnVector($sql);
					if(sizeof($tmp_result)>0){
						$ini_location_qty = $tmp_result[0] + $location_qty;
					}else{
						$ini_location_qty = $location_qty;
					}
					unset($tmp_result);
					
					/*
					$sql = "SELECT 
									a.FundingSource, 
									b.FundingType,
									".$linventory->getInventoryNameByLang("b.")."
							FROM 
									INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
									INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID)
							WHERE 
									a.ItemID = $item_id AND
									a.Action IN (1,7) AND
									a.GroupInCharge = $admin_group_id AND
									a.FundingSource IS NOT NULL
							";
					$tmp_result = $linventory->returnArray($sql,3);
					if(sizeof($tmp_result)>0){
						list($funding_source, $funding_type, $funding_name)=$tmp_result[0];
					}
					*/
					
					$ini_total_qty = $totalQty + $written_off_qty;
					if ($ini_total_qty>0)
						$final_unit_price = round($total_purchase_price/$ini_total_qty,2);
					//$total_funding_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);
					
					if($DisplayUnitPrice)
					$group_table_content_new_initial.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($final_unit_price,2)."</td>";
					
					if($ShowFundingName){
						$str_funding_name = "<br / >(".$funding_name.")";
					} 
// 					else
// 					{
// 						$str_col_fund = " align='right' ";
// 					}
					$str_col_fund = " align='right' ";
					
					/*
					if ($total_funding_cost<=0)
					{
						$str_funding_name = "";
					}
					*/
						
					### Get Qty By Location ###
					$sql = "SELECT 
									a.Quantity,
									a.LocationID,
									CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.")."),
									f.FundingType,
									". $linventory->getInventoryNameByLang("f.") ." as funding_name,
									a.FundingSourceID
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID)
									inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
							WHERE
									a.ItemID = '".$item_id."' AND 
									a.GroupInCharge = $admin_group_id
							";
					$arr_tmp_qty = $linventory->returnArray($sql,null,2);
					
					$display_qty = 0;
					$jMax = sizeof($arr_tmp_qty); 
					if($jMax>0)
					{
						$qty = 0;
						$display_location_name = "";
						unset($location_showed);
						$location_showed = array();
						
						$display_location_name .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
						for($j=0; $j<$jMax; $j++)
						{
							list($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
							if($qty != "")
							{
								$display_qty = $display_qty + $qty;
							}
							if($location_name != "" && !$location_showed[$location_id])
							{
								if($j!=0){
									$display_location_name .= "<tr height=\"5px\"><td></td></tr>";
									$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
								}else{
									$display_location_name .= "<tr><td class=\"eSportprinttext\">".intranet_htmlspecialchars($location_name)."</td></tr>";
								}
								$location_showed[$location_id] = true;
							}
						}
						$display_location_name .= "</table>";
					}
					$count = $linventory->getNumOfBulkItemInvoice($item_id);
					if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
					    $total_funding_cost = $total_purchase_price;
					}else{
					    $total_funding_cost = $display_qty*$final_unit_price;
					}
					$final_purchase_price = $total_funding_cost;
					
					$totalPurchasePrice +=$final_purchase_price;
					
					if ($FilterBulkItemPriceFrom!=null || $FilterBulkItemPriceEnd!=null)
					{
						if ($FilterBulkItemPriceFrom!=null && $total_funding_cost<$FilterBulkItemPriceFrom)
						{
							$rows_shown --;
							continue;
						} elseif ($FilterBulkItemPriceEnd!=null && $total_funding_cost>$FilterBulkItemPriceEnd)
						{
							$rows_shown --;
							continue;
						}
					}
					
					$group_table_content .= $group_table_content_new_initial;
					if($jMax==0)
					{
						$display_location_name = "&nbsp;";
					}
					
					$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($display_qty)."</td>";
							
					# retrieve funding data
					$tmp_funding_src = array();
					for($j=0; $j<$jMax; $j++)
					{
						list($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];
						
						# [Case#V72270]
						## Location origial qty (by location only)##
						if($display_writeoff){
							$sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '".$item_id."' AND FundingSourceID = $fund_id";
							$tmp_result = $linventory->returnVector($sql);
							if(sizeof($tmp_result)>0 && $j==0)
								$qty = $tmp_result[0] + $qty;
							unset($tmp_result);								
						}
						
						$count = $linventory->getNumOfBulkItemInvoice($item_id);
						if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
						    $tmp_funding_src[$funding_type][$fund_id]['cost'] = $total_purchase_price;
						}else{
						    $tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
						}

						$tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
						$tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
					}
					unset($arr_tmp_qty);

					for($type_i=1;$type_i<=($DisplaySponsoringBody?3:2);$type_i++)
					{
						$this_field = "";
						if(!empty($tmp_funding_src[$type_i]))
						{
							$this_fund_cost = 0;
							foreach($tmp_funding_src[$type_i] as $k=>$d)
							{
								if($ShowFundingName)
								{
									$this_fund_cost = number_format($d['cost'],2);
									//$this_fund_src = ($d['cost']>0) ? "<br />(". $d['src_name'].")" :"";
									$this_fund_src = "<br />(". $d['src_name'].")";
									$this_field .= ($this_field ? "<br><br>" : "") . $this_fund_cost . $this_fund_src;
								}
								else
								{
									$this_fund_cost += $d['cost'];
									$this_field = number_format($this_fund_cost,2);
								}
							}
						}
						$this_field = $this_field ? $this_field : "0.00";
						
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >". $this_field."</td>";
					}
							
					/*
					if($funding_type == ITEM_SCHOOL_FUNDING)
					{						
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
					}
					if($funding_type == ITEM_GOVERNMENT_FUNDING)
					{
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
					}
					if($funding_type == ITEM_SPONSORING_BODY_FUNDING)
					{
						$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0,2)."</td>";
						$group_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format($total_funding_cost,2).$str_funding_name."</td>";
					}
					*/
					
					if ($DisplayPurchasePrice)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='right' >".number_format($final_purchase_price,2)."</td>";
					if ($DisplayLocation)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_location_name</td>";
						
						
					# Quotation No, Supplier, Tender No., Invoice No.	
					$sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID='".$item_id."'";
					$result2 = $linventory->returnArray($sql,null,2);
					if(!empty($result2))
					{
						$thisDataAry = array();
						
						foreach($result2 as $k2=>$d2)
						{
							list($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
							if($thisQuotationNo)
								$thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
							if($thisSupplierName)
								$thisDataAry['Supplier'][] = trim($thisSupplierName);
							if($thisTenderNo)
								$thisDataAry['TenderNo'][] = trim($thisTenderNo);
							if($thisInvoiceNo)
								$thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
						}
					}
					unset($result2);
					
					if($ShowSN)	
					{
						# no S/N for bulk item
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >&nbsp;</td>";	
					}
					
					if($ShowQuoNo)
					{
						$QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ",array_unique($thisDataAry['QuotationNo'])) : "&nbsp;";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $QnoNoStr ."</td>";
					}
					
					if($ShowSupplier)
					{
						$SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ",array_unique($thisDataAry['Supplier'])) : "&nbsp;";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $SupplierStr ."</td>";
					}
						
					if($ShowTenderNo)
					{
						$TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ",array_unique($thisDataAry['TenderNo'])) : "&nbsp;";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $TenderNoStr ."</td>";
					}
					
					if($ShowInvNo)
					{
						$InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ",array_unique($thisDataAry['InvoiceNo'])) : "&nbsp;";
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" align='left' >". $InvoiceNoStr ."</td>";
					}	
					
					if ($DisplayWriteOffInfo)
					{
						### Get Write-Off Date & Reason ###
						$display_write_off = "";
						$sql = "SELECT
										ApproveDate,
										WriteOffQty,
										WriteOffReason
								FROM
										INVENTORY_ITEM_WRITE_OFF_RECORD
								WHERE
										ItemID = '".$item_id."' AND 
										AdminGroupID = $admin_group_id
										 AND RecordStatus = 1
								";
						
						$arr_write_off = $linventory->returnArray($sql,null,2);
						$jMax = sizeof($arr_write_off); 
						if($jMax>0)
						{
							$display_write_off .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
							for($j=0; $j<$jMax; $j++)
							{
								list($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
								if($write_off_date != "")
								{
									$write_off_qty = $write_off_qty.$i_InventorySystem_Report_PCS;
									$tmp_write_off = $write_off_date."<BR>".$write_off_qty."<BR>".$write_off_reason;
									
									if($j!=0){
										$display_write_off .= "<tr height=\"5px\"><td></td></tr>";
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}else{
										$display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
									}
								}
							}
							$display_write_off .= "</table>";
	
							if($display_write_off != "")
								$group_table_content.="<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_write_off</td>";
							else
								$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
						else
						{
							$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
						}
						unset($arr_write_off);
					}	// $DisplayWriteOffInfo
					### Empty Space for Signature and remark ###
					if ($DisplaySignature)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
					if ($DisplayRemark)
						$group_table_content.="<td class=\"eSporttdborder eSportprinttext\">". ((trim($item_remark)!="")?$item_remark:'&nbsp;') ."</td>";
				}	// bulk item
			}		// loop for $i
			unset($arr_result);
			
			if($DisplayPurchasePriceTotal && $rows_shown!=0)
			    $group_table_content .= "<tr><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='" . ($totalAmountPosition - 4) . "'><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='2' align='right'>" . $Lang['eInventory']['TotalAssets'] . "</td><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" align='right'>" . number_format($totalPurchasePrice, 2) . "</td><td class=\"eSporttdborder eSportprinttext\" colspan='" . (21 - $totalAmountPosition) . "'></td></tr>";
		}	// end $iMax > 0
		if($rows_shown==0)
		{
			$group_table_content .= "<tr><td colspan=\"". $totalAmountPosition ."\" align=\"center\" class=\"\"  style=\"word-wrap:normal\">$i_no_record_exists_msg</td></tr>";
		}
		$group_table_content .= "</table>";
		
		
				
		if ($IsSignature)
		{
			$group_table_content .= $SignatureBlock;
		}
		
		if($a < sizeof($arr_group) - 1)
		{
			$group_table_content .= "<div style='page-break-after:always;'><br /></div>";
		}
	}	// loop for $aMax
	unset($arr_group);
}		// groupBy = 3

else if ($groupBy == 4) {
    // get all funding source #

    $arrTempTargetFundSouce = $targetFundSource;
    if (array_shift($arrTempTargetFundSouce) == "") {
        $targetFundSource = $arrTempTargetFundSouce;
    }
    $selected_cb = implode(",", $targetFundSource);

    $cond = "WHERE FundingSourceID in (" . $selected_cb . ")";
    $sql = "Select DISTINCT(" . $linventory->getInventoryItemNameByLang() . ") , FundingSourceID FROM INVENTORY_FUNDING_SOURCE $cond ORDER BY DisplayOrder";
    $arr_funding_source = $linventory->returnArray($sql, null, 2);

    for ($a = 0, $aMax = sizeof($arr_funding_source); $a < $aMax; $a ++) {
        list ($funding_source_name, $funding_source_id) = $arr_funding_source[$a];


        $fundSource_table_content .= $school_header;
        $fundSource_table_content .= "<table border=\"0\" width=\"100%\" class=\"eSporttableborder\" style='table-layout:auto' align=\"center\" cellspacing=\"0\" cellpadding=\"3\">";
        if ($DisplayCategory)
            $fundSource_table_content .= "<col width=\"95px\">\n";
        if ($DisplaySubCategory)
            $fundSource_table_content .= "<col width=\"95px\">\n";
        if ($DisplayItemCode)
            $fundSource_table_content .= "<col width=\"80px\">\n";
        $fundSource_table_content .= "<col width=\"60px\">\n";
        $fundSource_table_content .= "<col width=\"50px\">\n";
        $fundSource_table_content .= "<col width=\"50px\">\n";
        $fundSource_table_content .= "<col width=\"50px\">\n";
        $fundSource_table_content .= "<col width=\"50px\">\n";
        if ($DisplayPurchasePrice)
            $fundSource_table_content .= "<col width=\"50px\">\n";
        if ($DisplayLocation)
            $fundSource_table_content .= "<col width=\"80px\">\n";
        if ($DisplayWriteOffInfo)
            $fundSource_table_content .= "<col width=\"80px\">\n";
        if ($DisplaySignature)
            $fundSource_table_content .= "<col width=\"80px\">\n";
        if ($DisplayRemark)
            $fundSource_table_content .= "<col width=\"80px\">\n";

        $fundSource_table_content .= "<thead><tr><td class=\"eSporttdborder eSportprinttext\" bgcolor=\"#FFFFFF\" colspan=\"". $totalAmountPosition ."\" style=\"word-wrap:normal\"><b>" . $i_InventorySystem['FundingSource'] . " : " . intranet_htmlspecialchars($funding_source_name) . "</b></td></tr>";
        $fundSource_table_content .= "<tr>";

        if ($DisplayCategory) {
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem['Category'] . "</td>";
        }
        if ($DisplaySubCategory) {
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem['SubCategory'] . "</td>";
        }
        if ($DisplayItemCode) {
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" bgcolor=\"#FFFFFF\" rowspan=\"2\">$i_InventorySystem_Item_Code</td>";
        }
        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Item . "</td>";
        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Description . "</td>";
        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Purchase_Date . "</td>";
        if ($DisplayUnitPrice) {
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Sch_Unit_Price . "(" . $Lang['eInventory']['Avg'] . ")</td>";
        }
        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Quantity . "</td>";
        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" colspan=\"" . ($DisplaySponsoringBody ? 3 : 2) . "\" align=\"center\">" . $i_InventorySystem_Report_Col_Cost . "</td>";
        if ($DisplayPurchasePrice) {
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $Lang['eInventory']['Report']['FixedAssetsRegister']['FieldTitle']['TotalPrice'] . "</td>";
        }
        if ($DisplayLocation)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Location . "</td>";

        // S/N
        if ($ShowSN)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Item_Serial_Num . "</td>";

        // Quotation Number
        if ($ShowQuoNo)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Item_Quot_Num . "</td>";

        // Supplier
        if ($ShowSupplier)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Item_Supplier_Name . "</td>";

        // Tender No
        if ($ShowTenderNo)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Item_Tender_Num . "</td>";

        // Invoice Number
        if ($ShowInvNo)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Item_Invoice_Num . "</td>";

        if ($DisplayWriteOffInfo)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Date_Reason_of_Write_Off . "</td>";
        if ($DisplaySignature)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Signature . "</td>";
        if ($DisplayRemark)
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\" rowspan=\"2\">" . $i_InventorySystem_Report_Col_Remarks . "</td></tr>";
        $fundSource_table_content .= "<tr>
									<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['eInventory']['FundingType']['School'] . "</td>
									<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['eInventory']['FundingType']['Government'] . "</td>";
        if ($DisplaySponsoringBody) {
            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['eInventory']['FundingType']['SponsoringBody'] . "</td>";
        }
        $fundSource_table_content .= "</tr></thead>";
        if (! $display_writeoff) {
            $wo_con = " and a.RecordStatus=1 ";
        }
        
        if ($PurchaseDateFrom && $PurchaseDateTo) {
            $purchase_date_sql = " and ((b.PurchaseDate>='" . $PurchaseDateFrom . "' and b.PurchaseDate<='" . $PurchaseDateTo . "') or
										(e.PurchaseDate>='" . $PurchaseDateFrom . "' and e.PurchaseDate<='" . $PurchaseDateTo . "'))";
        }
        if ($PurchaseDateFrom && ! $PurchaseDateTo) {
            $purchase_date_sql = " and ((b.PurchaseDate>='" . $PurchaseDateFrom . "') or
										(e.PurchaseDate>='" . $PurchaseDateFrom . "'))";
        }
        if (! $PurchaseDateFrom && $PurchaseDateTo) {
            $purchase_date_sql = " and ((b.PurchaseDate<='" . $PurchaseDateTo . "') or
										(e.PurchaseDate<='" . $PurchaseDateTo . "'))";
        }

        $sql = "SELECT
					DISTINCT a.ItemID,
					a.ItemType,
					a.ItemCode,
					" . $linventory->getInventoryNameByLang("a.") . ",
					" . $linventory->getInventoryDescriptionNameByLang("a.") . ",
					" . $linventory->getInventoryNameByLang("c1.") . ",
					" . $linventory->getInventoryNameByLang("c2.") . "
				FROM
					INVENTORY_ITEM AS a 
                    LEFT OUTER JOIN INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) 
                    LEFT OUTER JOIN INVENTORY_ITEM_BULK_LOCATION AS c ON (a.ItemID = c.ItemID) 	
					LEFT OUTER JOIN INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID OR c.GroupInCharge = d.AdminGroupID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_LOG as e on e.ItemID=a.ItemID				
                    LEFT JOIN INVENTORY_CATEGORY as c1 on c1.CategoryID=a.CategoryID
					LEFT JOIN INVENTORY_CATEGORY_LEVEL2 as c2 on c2.Category2ID=a.Category2ID
                    LEFT JOIN INVENTORY_FUNDING_SOURCE as ifs on (ifs.FundingSourceID = b.FundingSource AND ifs.FundingSourceID = e.FundingSource)
				WHERE
					(b.FundingSource IN ($funding_source_id) OR e.FundingSource IN ($funding_source_id))
                    $purchase_date_sql 
					$wo_con
				ORDER BY
					a.ItemCode
				";
        $arr_result = $linventory->returnArray($sql, null, 2);
        
        $rows_shown = 0;
        $totalPurchasePrice = 0;
        $iMax = sizeof($arr_result); 
        
        if ($iMax > 0) {
            for ($i = 0; $i < $iMax; $i ++) {
                list ($item_id, $item_type, $item_code, $item_name, $item_description, $item_category, $item_subcategory) = $arr_result[$i];
                $arr_sub_result = array();
                $arr_purchase_date = array();
                //debug_pr($item_type);

                // select the records and according to period
                if ($item_type == 1) {
                    $arr_sub_result = $arr_all_single_items[$item_id];
                    
                } elseif ($item_type == 2) {
                    $arr_purchase_date = $arr_all_bulk_purchases[$item_id];
                }
                
                if ((! is_array($arr_sub_result) || sizeof($arr_sub_result) < 1) && (! is_array($arr_purchase_date) || sizeof($arr_purchase_date) < 1)) {
                    continue;
                }

                // filter out no stock-taken item if necessary
                if ($item_type == 1 && is_array($StocktakeResultArray) && ! in_array($item_id, $StocktakeResultArray)) {
                    // debug($item_id, $item_code, $item_name);
                    continue;
                } elseif ($item_type == 2 && is_array($StocktakeBulkResultArray) && ! in_array($item_id, $StocktakeBulkResultArray)) {
                    // debug($item_id, $item_code, $item_name);
                    continue;
                }

                if ($item_type == 1 && ($FilterSingleItemPriceFrom != null || $FilterSingleItemPriceEnd != null)) {
                    $rs = $arr_sub_result[0];
                    $purchase_date = $rs['PurchaseDate'];
                    $purchase_price = $rs['PurchasePrice'];
                    $unit_price = $rs['UnitPrice'];
                    $location_id = $rs['LocationID'];
                    $location_name = $rs['LocationName'];
                    $funding_type = $rs['FundingType'];
                    $funding_name = $rs['FundingSource'];
                    $write_off_date = $rs['WriteOffDate'];
                    $write_off_reason = $rs['WriteOffReason'];
                    $remark = $rs['Remark'];
                    $this_item_id = $rs['ItemID'];
                    $funding_name2 = $rs['FundingSource2'];
                    $unit_price1 = $rs['UnitPrice1'];
                    $unit_price2 = $rs['UnitPrice2'];
                    $funding_type2 = $rs['FundingType2'];
                    unset($rs);
                    
                    if ($FilterSingleItemPriceFrom != null && $unit_price < $FilterSingleItemPriceFrom) {
                        continue;
                    } elseif ($FilterSingleItemPriceEnd != null && $unit_price > $FilterSingleItemPriceEnd) {
                        continue;
                    }
                }

                $rows_shown ++;

                $fundSource_table_content_new_initial = "<tr>";

                if ($DisplayCategory)
                    $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_category</td>";
                if ($DisplaySubCategory)
                    $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_subcategory</td>";
                if ($DisplayItemCode)
                    $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_code</td>";
                $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">$item_name</td>";
                if ($item_description != "")
                    $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">" . nl2br($item_description) . "</td>";
                else
                    $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";

                if ($item_type == ITEM_TYPE_SINGLE) {
                    for ($j = 0, $jMax = sizeof($arr_sub_result); $j < $jMax; $j ++) {
                        $rs = $arr_sub_result[$j];
                        $purchase_date = $rs['PurchaseDate'];
                        $purchase_price = $rs['PurchasePrice'];
                        $unit_price = $rs['UnitPrice'];
                        $location_id = $rs['LocationID'];
                        $location_name = $rs['LocationName'];
                        $funding_type = $rs['FundingType'];
                        $funding_name = $rs['FundingSource'];
                        $write_off_date = $rs['WriteOffDate'];
                        $write_off_reason = $rs['WriteOffReason'];
                        $remark = $rs['Remark'];
                        $this_item_id = $rs['ItemID'];
                        $funding_name2 = $rs['FundingSource2'];
                        $unit_price1 = $rs['UnitPrice1'];
                        $unit_price2 = $rs['UnitPrice2'];
                        $funding_type2 = $rs['FundingType2'];
                        unset($rs);

                        $quantity = $write_off_date ? "0" : "1";

                        if ($unit_price > 0)
                            $purchase_price = $unit_price * $quantity;
                        else if($sys_custom['eInventory']['showZeroUnitPriceOfWriteOffItem'] && $write_off_date){
                        	$purchase_price = 0;
                        }

                        $totalPurchasePrice += $purchase_price;

                        if ($purchase_date == "0000-00-00")
                            $purchase_date = "--";

                        $fundSource_table_content .= $fundSource_table_content_new_initial;

                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $purchase_date . "</td>";

                        if ($DisplayUnitPrice)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >" . number_format($unit_price, 2) . "</td>";

                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >" . (($quantity != "") ? $quantity : '&nbsp;') . "</td>";

                        $Cost_Ary = array();
                        $Funding_ary = array();
                        $Cost_Ary[$funding_type] += $unit_price1;
                        if ($ShowFundingSource)
                            $Funding_ary[$funding_type] .= ($Funding_ary[$funding_type] ? ", " : "") . $funding_name;
                        if ($funding_type2) {
                            $Cost_Ary[$funding_type2] += $unit_price2;
                            if ($ShowFundingSource)
                                $Funding_ary[$funding_type2] .= ($Funding_ary[$funding_type2] ? ", " : "") . $funding_name2;
                        }

                        $str_col_fund = " align='right' ";

                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >" . number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2) . ($Funding_ary[ITEM_SCHOOL_FUNDING] ? "<br>(" . $Funding_ary[ITEM_SCHOOL_FUNDING] . ")" : "") . "</td>";
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >" . number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2) . ($Funding_ary[ITEM_GOVERNMENT_FUNDING] ? "<br>(" . $Funding_ary[ITEM_GOVERNMENT_FUNDING] . ")" : "") . "</td>";
                        if ($DisplaySponsoringBody)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >" . number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2) . ($Funding_ary[ITEM_SPONSORING_BODY_FUNDING] ? "<br>(" . $Funding_ary[ITEM_SPONSORING_BODY_FUNDING] . ")" : "") . "</td>";

                        if ($DisplayPurchasePrice)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >" . number_format($purchase_price, 2) . "</td>";
                        if ($DisplayLocation)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . (($location_name != "") ? intranet_htmlspecialchars($location_name) : '&nbsp;') . "</td>";

                        // S/N, Quotation No, Supplier, Tender No., Invoice No.
                        $sql = "select SerialNumber, QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_SINGLE_EXT where ItemID=$item_id";
                        $result2 = $linventory->returnArray($sql, null, 2);
                        list ($thisSerialNumber, $thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $result2[0];
                        unset($result2);

                        if ($ShowSN)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . (($thisSerialNumber != "") ? $thisSerialNumber : '&nbsp;') . "</td>";
                        if ($ShowQuoNo)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . (($thisQuotationNo != "") ? $thisQuotationNo : '&nbsp;') . "</td>";
                        if ($ShowSupplier)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . (($thisSupplierName != "") ? $thisSupplierName : '&nbsp;') . "</td>";
                        if ($ShowTenderNo)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . (($thisTenderNo != "") ? $thisTenderNo : '&nbsp;') . "</td>";
                        if ($ShowInvNo)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . (($thisInvoiceNo != "") ? $thisInvoiceNo : '&nbsp;') . "</td>";

                        if ($DisplayWriteOffInfo)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . ((trim($write_off_date) != "") ? $write_off_date : '&nbsp;') . ((trim($write_off_reason)) ? "(" . $write_off_reason . ")" : '&nbsp;') . "</td>";
                        if ($DisplaySignature)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . ((trim($signature) != "") ? $signature : '&nbsp;') . "</td>";
                        if ($DisplayRemark)
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . ((trim($remark) != "") ? nl2br($remark) : '&nbsp;') . "</td>";
                        $fundSource_table_content .= "</tr>";
                        
                    } // end loop for $jMax
                    unset($arr_sub_result);
                } // end single item
                if ($item_type == ITEM_TYPE_BULK) {
                    $final_purchase_date = "";

                    $jMax = sizeof($arr_purchase_date);
                    if ($jMax > 0) {
                        $final_purchase_date .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
                        for ($j = 0; $j < $jMax; $j ++) {
                            $purchase_date = $arr_purchase_date[$j]['PurchaseDate'];
                            if ($purchase_date != '') {
                                $final_purchase_date .= "<tr><td class=\"eSportprinttext\">$purchase_date</td></tr>";
                            } else {
                                $final_purchase_date .= '<tr><td class=\"eSportprinttext\">&nbsp;</td></tr>';
                            }
                        }
                        $final_purchase_date .= "</table>";
                        $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$final_purchase_date</td>";
                    } else {
                        $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\"> - </td>";
                    }
                    unset($arr_purchase_date);

                    // ## GET COST, UNIT PRICE, PURCHASED PRICE ###
                    $total_school_cost = 0;
                    $total_gov_cost = 0;
                    $final_unit_price = 0;
                    $final_purchase_price = 0;
                    $total_qty = 0;
                    $tmp_unit_price = 0;
                    $tmp_purchase_price = 0;
                    $written_off_qty = 0;
                    $location_qty = 0;
                    $ini_total_qty = 0;
                    $item_remark = "";

                    // # get Total purchase price ##
                    $count = $linventory->getNumOfBulkItemInvoice($item_id);
                    if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
                        $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                    }else{
                        // $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
                        $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                    }

                    $tmp_result = $linventory->returnVector($sql);
                    if (sizeof($tmp_result) > 0) {
                        $total_purchase_price = $tmp_result[0];
                        // [Case#B70033]
                        if ($total_purchase_price == 0) {
                            $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                            $tmp_result = $linventory->returnVector($sql);
                            if ($tmp_result[0] > 0)
                                $total_purchase_price = $tmp_result[0];
                        }
                    }

                    $sql = "SELECT Remark FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_id . "' AND Action = 1 and (RecordStatus=0 or RecordStatus is NULL) ";
                    $tmp_result = $linventory->returnVector($sql);
                    if (sizeof($tmp_result) > 0) {
                        $item_remark = implode(";<br />", array_unique($tmp_result));
                    }

                    // # get Total Qty Of the target Item ##
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '" . $item_id . "'";
                    $tmp_result = $linventory->returnArray($sql, null, 2);
                    if (sizeof($tmp_result) > 0) {
                        list ($totalQty) = $tmp_result[0];
                    }
                    // # get written-off qty ##
                    $sql = "SELECT sum(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_id . "'";
                    $tmp_result = $linventory->returnArray($sql, null, 2);
                    if (sizeof($tmp_result) > 0) {
                        list ($written_off_qty) = $tmp_result[0];
                    }

                    // # get the qty in the target location ##
                    $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '" . $item_id . "' AND FundingSourceID ='" . $funding_source_id . "'";
                    $tmp_result = $linventory->returnArray($sql, null, 2);
                    if (sizeof($tmp_result) > 0) {
                        list ($location_qty) = $tmp_result[0];
                    }

                    // # Location origial qty (by location only)##
                    $sql = "SELECT WriteOffQty FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_id . "' AND FundingSourceID ='" . $funding_source_id . "'";
                    $tmp_result = $linventory->returnVector($sql);
                    if (sizeof($tmp_result) > 0) {
                        $ini_location_qty = $tmp_result[0] + $location_qty;
                    } else {
                        $ini_location_qty = $location_qty;
                    }
                    unset($tmp_result);

                    $ini_total_qty = $totalQty + $written_off_qty;
                    if ($ini_total_qty > 0)
                        $final_unit_price = round($total_purchase_price / $ini_total_qty, 2);
                    // $total_funding_cost = $ini_location_qty*($total_purchase_price/$ini_total_qty);

                    if ($DisplayUnitPrice)
                        $fundSource_table_content_new_initial .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >" . number_format($final_unit_price, 2) . "</td>";

                    if ($ShowFundingSource) {
                        $str_funding_name = "<br / >(" . $funding_name . ")";
                    }

                    $str_col_fund = " align='right' ";

                    // ## Get Qty By Location ###
                    $sql = "SELECT
									a.Quantity,
									a.LocationID,
									CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . "),
									f.FundingType,
									" . $linventory->getInventoryNameByLang("f.") . " as funding_name,
									a.FundingSourceID
							FROM
									INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
									INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
									INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
									INVENTORY_LOCATION_BUILDING AS building ON (c.BuildingID = building.BuildingID)
									inner join INVENTORY_FUNDING_SOURCE as f on (f.FundingSourceID=a.FundingSourceID)
							WHERE
									a.ItemID = '" . $item_id . "'AND 
									a.FundingSourceID = '".$funding_source_id."'
							";
                    $arr_tmp_qty = $linventory->returnArray($sql, null, 2);

                    $display_qty = 0;
                    $jMax = sizeof($arr_tmp_qty);
                    if ($jMax > 0) {
                        $qty = 0;
                        $display_location_name = "";
                        unset($location_showed);
                        $location_showed = array();

                        $display_location_name .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
                        for ($j = 0; $j < $jMax; $j ++) {
                            list ($qty, $location_id, $location_name) = $arr_tmp_qty[$j];
                            if ($qty != "") {
                                $display_qty = $display_qty + $qty;
                            }
                            if ($location_name != "" && ! $location_showed[$location_id]) {
                                if ($j != 0) {
                                    $display_location_name .= "<tr height=\"5px\"><td></td></tr>";
                                    $display_location_name .= "<tr><td class=\"eSportprinttext\">" . intranet_htmlspecialchars($location_name) . "</td></tr>";
                                } else {
                                    $display_location_name .= "<tr><td class=\"eSportprinttext\">" . intranet_htmlspecialchars($location_name) . "</td></tr>";
                                }
                                $location_showed[$location_id] = true;
                            }
                        }
                        $display_location_name .= "</table>";
                    }
                    $count = $linventory->getNumOfBulkItemInvoice($item_id);
                    if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
                        $total_funding_cost = $total_purchase_price;
                    }else{
                        $total_funding_cost = $display_qty * $final_unit_price;
                    }

                    $final_purchase_price = $total_funding_cost;

                    $totalPurchasePrice += $final_purchase_price;

                    if ($FilterBulkItemPriceFrom != null || $FilterBulkItemPriceEnd != null) {
                        if ($FilterBulkItemPriceFrom != null && $total_funding_cost < $FilterBulkItemPriceFrom) {
                            $rows_shown --;
                            continue;
                        } elseif ($FilterBulkItemPriceEnd != null && $total_funding_cost > $FilterBulkItemPriceEnd) {
                            $rows_shown --;
                            continue;
                        }
                    }

                    $fundSource_table_content .= $fundSource_table_content_new_initial;
                    if ($jMax == 0) {
                        $display_location_name = "&nbsp;";
                    }

                    $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >" . number_format($display_qty) . "</td>";

                    // retrieve funding data
                    $tmp_funding_src = array();
                    for ($j = 0; $j < $jMax; $j ++) {
                        list ($qty, $location_id, $location_name, $funding_type, $funding_name, $fund_id) = $arr_tmp_qty[$j];

                        // [Case#V72270]
                        // # Location origial qty (by location only)##
                        if ($display_writeoff) {
                            $sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_id . "' AND FundingSourceID = $fund_id";
                            $tmp_result = $linventory->returnVector($sql);
                            if (sizeof($tmp_result) > 0 && $j == 0)
                                $qty = $tmp_result[0] + $qty;
                            unset($tmp_result);
                        }
                        $count = $linventory->getNumOfBulkItemInvoice($item_id);
                        if($special_feature['eInventory']['CheckForOnlyOneInvoice'] && $count[0] == 1){
                            $tmp_funding_src[$funding_type][$fund_id]['cost'] = $total_purchase_price;
                        }else{
                            $tmp_funding_src[$funding_type][$fund_id]['cost'] += $qty * $final_unit_price;
                        }
                        $tmp_funding_src[$funding_type][$fund_id]['src_name'] = $funding_name;
                        $tmp_funding_src[$funding_type][$fund_id]['type'] = $funding_type;
                    }
                    unset($arr_tmp_qty);

                    for ($type_i = 1; $type_i <= ($DisplaySponsoringBody ? 3 : 2); $type_i ++) {
                        $this_field = "";
                        if (! empty($tmp_funding_src[$type_i])) {
                            $this_fund_cost = 0;
                            foreach ($tmp_funding_src[$type_i] as $k => $d) {
                                if ($ShowFundingSource) {
                                    $this_fund_cost = number_format($d['cost'], 2);
                                    // $this_fund_src = ($d['cost']>0) ? "<br />(". $d['src_name'].")" :"";
                                    $this_fund_src = "<br />(" . $d['src_name'] . ")";
                                    $this_field .= ($this_field ? "<br><br>" : "") . $this_fund_cost . $this_fund_src;
                                } else {
                                    $this_fund_cost += $d['cost'];
                                    $this_field = number_format($this_fund_cost, 2);
                                }
                            }
                        }
                        $this_field = $this_field ? $this_field : "0.00";

                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >" . $this_field . "</td>";
                    }

                    if ($DisplayPurchasePrice)
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='right' >" . number_format($final_purchase_price, 2) . "</td>";
                    if ($DisplayLocation)
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_location_name</td>";

                    // Quotation No, Supplier, Tender No., Invoice No.
                    $sql = "select QuotationNo, SupplierName, TenderNo, InvoiceNo from INVENTORY_ITEM_BULK_LOG where ItemID='" . $item_id . "'";
                    $result2 = $linventory->returnArray($sql, null, 2);
                    if (! empty($result2)) {
                        $thisDataAry = array();

                        foreach ($result2 as $k2 => $d2) {
                            list ($thisQuotationNo, $thisSupplierName, $thisTenderNo, $thisInvoiceNo) = $d2;
                            if ($thisQuotationNo)
                                $thisDataAry['QuotationNo'][] = trim($thisQuotationNo);
                            if ($thisSupplierName)
                                $thisDataAry['Supplier'][] = trim($thisSupplierName);
                            if ($thisTenderNo)
                                $thisDataAry['TenderNo'][] = trim($thisTenderNo);
                            if ($thisInvoiceNo)
                                $thisDataAry['InvoiceNo'][] = trim($thisInvoiceNo);
                        }
                    }
                    unset($result2);

                    if ($ShowSN) {
                        // no S/N for bulk item
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='left' >&nbsp;</td>";
                    }

                    if ($ShowQuoNo) {
                        $QnoNoStr = is_array($thisDataAry['QuotationNo']) ? implode(", ", array_unique($thisDataAry['QuotationNo'])) : "&nbsp;";
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='left' >" . $QnoNoStr . "</td>";
                    }

                    if ($ShowSupplier) {
                        $SupplierStr = is_array($thisDataAry['Supplier']) ? implode(", ", array_unique($thisDataAry['Supplier'])) : "&nbsp;";
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='left' >" . $SupplierStr . "</td>";
                    }

                    if ($ShowTenderNo) {
                        $TenderNoStr = is_array($thisDataAry['TenderNo']) ? implode(", ", array_unique($thisDataAry['TenderNo'])) : "&nbsp;";
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='left' >" . $TenderNoStr . "</td>";
                    }

                    if ($ShowInvNo) {
                        $InvoiceNoStr = is_array($thisDataAry['InvoiceNo']) ? implode(", ", array_unique($thisDataAry['InvoiceNo'])) : "&nbsp;";
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" align='left' >" . $InvoiceNoStr . "</td>";
                    }

                    if ($DisplayWriteOffInfo) {
                        // ## Get Write-Off Date & Reason ###
                        $display_write_off = "";
                        $sql = "SELECT
										ApproveDate,
										WriteOffQty,
										WriteOffReason
								FROM
										INVENTORY_ITEM_WRITE_OFF_RECORD
								WHERE
										ItemID = '" . $item_id . "' 
										 AND RecordStatus = 1
								";

                        $arr_write_off = $linventory->returnArray($sql, null, 2);
                        $jMax = sizeof($arr_write_off);
                        if ($jMax > 0) {
                            $display_write_off .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
                            for ($j = 0; $j < $jMax; $j ++) {
                                list ($write_off_date, $write_off_qty, $write_off_reason) = $arr_write_off[$j];
                                if ($write_off_date != "") {
                                    $write_off_qty = $write_off_qty . $i_InventorySystem_Report_PCS;
                                    $tmp_write_off = $write_off_date . "<BR>" . $write_off_qty . "<BR>" . $write_off_reason;

                                    if ($j != 0) {
                                        $display_write_off .= "<tr height=\"5px\"><td></td></tr>";
                                        $display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
                                    } else {
                                        $display_write_off .= "<tr><td class=\"eSportprinttext\">$tmp_write_off</td></tr>";
                                    }
                                }
                            }
                            $display_write_off .= "</table>";

                            if ($display_write_off != "")
                                $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\" valign=\"top\">$display_write_off</td>";
                            else
                                $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
                        } else {
                            $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
                        }
                        unset($arr_write_off);
                    } // $DisplayWriteOffInfo
                      // ## Empty Space for Signature and remark ###
                    if ($DisplaySignature)
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">&nbsp;</td>";
                    if ($DisplayRemark)
                        $fundSource_table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . ((trim($item_remark) != "") ? $item_remark : '&nbsp;') . "</td>";
                } // bulk item
            } // loop for $i
            unset($arr_result);

            if ($DisplayPurchasePriceTotal && $rows_shown != 0)
                $funding_source_content .= "<tr><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='" . ($totalAmountPosition - 4) . "'><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" colspan='2' align='right'>" . $Lang['eInventory']['TotalAssets'] . "</td><td style='border-right:0' class=\"eSporttdborder eSportprinttext\" align='right'>" . number_format($totalPurchasePrice, 2) . "</td><td class=\"eSporttdborder eSportprinttext\" colspan='" . (21 - $totalAmountPosition) . "'></td></tr>";
        } // end $iMax > 0
        if ($rows_shown == 0) {
            $fundSource_table_content .= "<tr><td colspan=\"". $totalAmountPosition ."\" align=\"center\" class=\"\"  style=\"word-wrap:normal\">$i_no_record_exists_msg</td></tr>";
        }
        $fundSource_table_content .= "</table>";

        if ($IsSignature) {
            $fundSource_table_content .= $SignatureBlock;
        }

        if ($a < sizeof($arr_funding_source) - 1) {
            $fundSource_table_content .= "<div style='page-break-after:always;'><br /></div>";
        }
    } // loop for $aMax
    unset($arr_funding_source);
}		// groupBy = 4


//include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

# for debug and performance tuning
/*
$ProcessTime = round(StopTimer(2, true), 4);
	$memory_benchmrk = "Total memory used in this page: <font color='yellow' size='3'>".number_format((memory_get_usage())/1024)."KB (".number_format((memory_get_usage())/1024/1024, 1)."MB)";
	$ProcessTimeShow = ($ProcessTime>1) ? "<font color='red' size='4'>".$ProcessTime."</font>" : "<font color='yellow' size='3'>".$ProcessTime."</font>";
	$QueryExecuted = "QUERIES : ". (($GLOBALS[debug][db_query_count]>10) ? "<font color='red' size='4'>" : "<font color='yellow' size='3'>").$GLOBALS[debug][db_query_count] . "</font>";
	$ProcessTimeShowHTML = "<table width='100%' style='border:1px dotted #BBBBBB' border='0' cellpadding='0' cellspacing='0' id='ProcessTimeTable'><tr><td align='center' bgcolor='#000000'><font color='white' size='2'>Page generated in {$ProcessTimeShow} seconds.  $memory_benchmrk.</font> $QueryExecuted </td></tr></table>\n";
	$ProcessTimeShowHTML .= "<script language='javascript'>\nsetTimeout(\"displayTable('ProcessTimeTable', 'none')\", 9000);\n</script>\n";
echo $ProcessTimeShowHTML;
*/

$margin_top = '7';
$mpdf = new mPDF('','A4-L',0,'',$margin_left=5,$margin_right=5,$margin_top=10,$margin_bottom=10,$margin_header,$margin_footer); 
$mpdf->mirrorMargins = 1;
$mpdf->backupSubsFont = array('mingliu');
$mpdf->useSubstitutions = true;
$mpdf->shrink_tables_to_fit=0;
$mpdf->allow_charset_conversion=true;
$mpdf->setFooter('<div style="text-align: right;font-size: 12px;">{PAGENO} / {nb}</div>');
$css = '<style type="text/css" media="print">
 .print_hide {display:none;}
</style>
<style type="text/css"> 
.eSportprinttabletitle {
		font-size: 13px;
		color: #000000;
		background-color: #EEEEEE;
	}
.eSporttdborder {
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-right-style: solid;
	border-bottom-style: solid;
	border-right-color: #333333;
	border-bottom-color: #333333;
}
.eSporttableborder {
	border-top-width: 1px;
	border-right-width: 1px;
	border-bottom-width: 1px;
	border-left-width: 2px;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
	border-top-color: #333333;
	border-right-color: #333333;
	border-bottom-color: #333333;
	border-left-color: #333333;
}
 P.breakhere {page-break-before: always}
</style>
<style type="text/css">
	table { page-break-inside:auto }
	tr { page-break-inside:avoid; page-break-after:auto }
	thead { display:table-header-group }
</style>';

$mpdf->WriteHTML($css);

if($groupBy==1) { 
	$html = $location_table_content;
} 

if($groupBy==2) { 
	$html = $table_content;
}

if($groupBy==3) {
	$html = $group_table_content;
}

if($groupBy == 4){
    $html = $fundSource_table_content;
}

//$long_html = strlen($html);
//$long_int  = intval($long_html/10000);
//
//if($long_int > 0)
//{
//    for($i = 0; $i<$long_int; $i++)
//    {
//        $temp_html = substr($html, ($i*10000),9999);
//        $mpdf->WriteHTML($temp_html);
//    }
//    //Last block
//    $temp_html = substr($html, ($i*10000),($long_html-($i*10000)));
//    $mpdf->WriteHTML($temp_html);
//}

$encoding = 'UTF-8';
$block_size = 10000;	// number of character per block
$long_html = mb_strlen($html,$encoding);
$long_int  = intval($long_html/$block_size);

if($long_int > 0)
{
	$i = 1;
	while ($long_html > $block_size) {
    	$pos = mb_strpos($html,'</td>',$block_size-200,$encoding);
    	$pos = $pos === false ? $block_size : $pos + 5;
   		$temp_html = mb_substr($html, 0, $pos, $encoding);
   		$mpdf->WriteHTML($temp_html);
   		
		$html = mb_substr($html, $pos, mb_strlen($html,$encoding)-$pos, $encoding);
		$long_html = mb_strlen($html,$encoding);
		$i++;
		
		// Last block
		if ($long_html <= $block_size) {
			$mpdf->WriteHTML($html);
		}
	}
} 

else
{
    $mpdf->WriteHTML($html);
}

$filename = "fixed_assets_register.pdf";

$mpdf->Output($filename, 'D');

intranet_closedb();
?>