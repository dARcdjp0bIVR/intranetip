<?php
// using : 
/**
 * **************************** Change Log **************************************************
 * 2019-04-30 (Henry): security issue fix
 * 2018-02-07 (Henry): add access right checking [Case#E135442]
 * Page created on 2013-02-20
 * ****************************** Change Log ************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

// init no. of barcode label per page
$label_per_page = 16;

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$targetGroup = str_replace("'","",stripslashes($_REQUEST['targetGroup']));
$barcodeWidth = str_replace("'","",stripslashes($_REQUEST['barcodeWidth']));

$targetGroup = IntegerSafe($targetGroup);
$barcodeWidth = IntegerSafe($barcodeWidth);

if($barcodeWidth == "")
{
	$barcodeWidth = DEFAULT_BARCODE_WIDTH;
}


$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$sql = "SELECT AdminGroupID, $namefield, Barcode FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID IN ($targetGroup) ORDER BY DisplayOrder";
$result = $linventory->returnArray($sql);

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($admin_group_id, $group_name, $group_barcode) = $result[$i];
		
		$ans = $i % ($label_per_page);

		if(($ans == 0) && ($i != 0))
		{
			$report_content .= "<div style='page-break-before: always'>";
		}
		if($ans == 0){
			$report_content .= "&nbsp;";
			$report_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">\n";
		}
		
		if($i%2 == 0){
			$report_content .= "<tr>\n";
		}
		
		$barcode_label = "<img src='barcode.php?barcode=".rawurlencode($group_barcode)."&width=$barcodeWidth'>";
		$report_content .= "<td class=\"eSporttdborder eSportprinttext\" align='center'>$barcode_label<br>$group_name</td>\n";
		
		if($i%2 == 1){
			$report_content .= "</tr>\n";
		}
		
		if($ans == $label_per_page-1)
		{
			$report_content .= "</table>\n</div>\n";
		}
	}	
}
?>
<STYLE TYPE="text/css">
     
</STYLE> 

<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?=$report_content;?>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>