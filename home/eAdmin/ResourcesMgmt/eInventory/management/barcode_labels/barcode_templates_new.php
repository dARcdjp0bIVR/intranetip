<?php
// using: 

/**
 * **************************** Change Log **************************************************
 * 2019-10-03 Tommy
 * - add $item_barcode_num for output barcode with barcode number
 * 
 * 2019-02-18 Isaac
 * -added ownership to labelInfoOrder array
 * 
 * 2018-05-18 (Cameron)
 * - add sub-location code to Label information and information order [case #P138005]
 * - use function getNumberOfOrderByFieldInLabel to get $option_no
 * - use loop in checkForm() to check if none item is selected
 * 
 * 2018-02-22 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log ************************************************
 */
if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) { 
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}
 

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");

intranet_auth();

intranet_opendb();
$CurrentPage	= "Management_BarcodeLabels";

$linterface = new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$min_barcode_width = DEFAULT_MINIMUM_BARCODE_WIDTH;
$max_barcode_width = DEFAULT_MAXIMUN_BARCODE_WIDTH;


$TAGS_OBJ[] = array($i_InventorySystem_Report_ItemDetails, "item_details.php", 1);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['LocationBarcode'], "location_barcode.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['ResourceMgmtGroupBarcode'], "admin_group_barcode.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['FundingSourceBarcode'], "funding_source_barcode.php", 0);

$PAGE_NAVIGATION[] = array($i_InventorySystem['CustomBarcodeFormats'] , "item_details.php?format=auto");
$PAGE_NAVIGATION[] = array($i_InventorySystem['ManagementBarcodeTemplates'] , "management_barcode_templates.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_New , "");

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


# option for open  ......

foreach ($Lang['eInventory']['label']['unit'] as $unit =>$disp_unit ){
	$metric_unit[]=array($unit,$disp_unit);
}

$labelInfoOrder = array();

$labelInfoOrder[] = array('item_tag_code',$i_InventorySystem_Item_Barcode);
$labelInfoOrder[] = array('item_barcode_num','Barcode (with number)');
$labelInfoOrder[] = array('item_name',$i_InventorySystem_Item_Name);
$labelInfoOrder[] = array('item_code',$i_InventorySystem_Item_Code);
$labelInfoOrder[] = array('item_admin_group',$i_InventorySystem_Group_Name);
$labelInfoOrder[] = array('item_admin_group_code',$i_InventorySystem_Caretaker_Code);
$labelInfoOrder[] = array('item_location',$i_InventorySystem_Item_Location);
$labelInfoOrder[] = array('item_sublocation',$i_InventorySystem_Location);
$labelInfoOrder[] = array('item_sublocation_code',$i_InventorySystem_Location_Code);
$labelInfoOrder[] = array('item_funding',$i_InventorySystem_Item_Funding);
$labelInfoOrder[] = array('item_brand',$i_InventorySystem_Item_Brand_Name);
$labelInfoOrder[] = array('item_warranty_expiry_date',$i_InventorySystem_Item_WarrantyExpiryDate);
$labelInfoOrder[] = array('item_license',$i_InventorySystem_Item_License);
$labelInfoOrder[] = array('item_supplier_name',$i_InventorySystem_Item_Supplier_Name);
$labelInfoOrder[] = array('item_supplier_contact',$i_InventorySystem_Item_Supplier_Contact);
$labelInfoOrder[] = array('item_supplier_desc',$i_InventorySystem_Item_Supplier_Description);
$labelInfoOrder[] = array('item_serial_num',$i_InventorySystem_Item_Serial_Num);
$labelInfoOrder[] = array('item_tender_num',$i_InventorySystem_Item_Tender_Num);
$labelInfoOrder[] = array('item_invoice_num',$i_InventorySystem_Item_Invoice_Num);
$labelInfoOrder[] = array('item_purchase_date',$i_InventorySystem_Report_Col_Purchase_Date);
$labelInfoOrder[] = array('item_remark',$i_InventorySystem_Report_Col_Remarks);
$labelInfoOrder[] = array('item_ownership',$i_InventorySystem_Item_Ownership);


//$labelInfoOrderSelections = '';
//for($i=0; $i<count($labelInfoOrder); $i++){
//	$labelInfoOrderSelections .= '('.($i+1).') '.$linterface->GET_SELECTION_BOX($labelInfoOrder, 'id="info_order_'.($i+1).'" name="info_order_'.($i+1).'"', $Lang["libms"]["status"]["na"], "").'<br/>';
//}

$option_no = $linventory->getNumberOfOrderByFieldInLabel();

$labelInfoOrderSelections_left="";
for($i=0; $i<10; $i++){
	$labelInfoOrderSelections_left .= '('.($i+1).') '.$linterface->GET_SELECTION_BOX($labelInfoOrder, 'id="info_order_'.($i+1).'" name="info_order_'.($i+1).'"', $i_general_na, ${'info_order_'.($i+1)}).'<br/>';
}

$labelInfoOrderSelections_right="";
for($i=10; $i<$option_no; $i++){
	$labelInfoOrderSelections_right .= '('.($i+1).') '.$linterface->GET_SELECTION_BOX($labelInfoOrder, 'id="info_order_'.($i+1).'" name="info_order_'.($i+1).'"', $i_general_na, ${'info_order_'.($i+1)}).'<br/>';
}

?>

<script language="javascript">
function checkForm(form1) {

	var name, val_null, val_wrong;
	val_null = val_wrong = false;
	
	$('input.number').each(function(){
		if (!(val_null || val_wrong)){
			if ($(this).val() == "") {
				name = $(this).attr("id")
				alert("<?= $i_alert_pleasefillin.' '?>"+name);	
				$(this).focus();
				val_null = true;
				}
			
			if (($(this).attr("name") != "name") && (!val_null) && (!IsValidInput($(this)))){
				$(this).focus();
				val_wrong = true;
			}
		}
	});
				
	if(!val_null && !val_wrong){
		var selectNone = true;
		$(":select[id^='info_order_']").each(function(){
			if ($(this).val() != '') {
				selectNone = false;
			}
		});
		
//		if($('#info_order_1').val() == '' && $('#info_order_2').val() == '' && $('#info_order_3').val() == '' && $('#info_order_4').val() == '' && $('#info_order_5').val() == '' && $('#info_order_6').val() == ''&& $('#info_order_7').val() == ''&& $('#info_order_8').val() == ''&& $('#info_order_9').val() == ''&& $('#info_order_10').val() == ''&& $('#info_order_11').val() == ''&& $('#info_order_12').val() == ''&& $('#info_order_13').val() == ''&& $('#info_order_14').val() == ''&& $('#info_order_15').val() == ''&& $('#info_order_16').val() == ''&& $('#info_order_17').val() == ''&& $('#info_order_18').val() == ''&& $('#info_order_19').val() == ''){
		if (selectNone == true ) {
			alert( "<?=$Lang["eInventory"]["label"]["msg"]["LabelAtLeastOne"]?>" );
			$('#info_order_1').focus();
			return false;
		}
		return true;
	}
		
		if (val_null || val_wrong)
		return false;
		
		return true;
}

function IsValidInput(meme) {

	var inputPat = /^(\d{1,5})(.(\d{1,2}))?$/;
	var matchArray = meme.val().match(inputPat);
	if (matchArray == null) {
		name = meme.attr("id");
		alert(" <?= $Lang['eInventory']['label']['input_msg'] ?> "+name);
		return false;
	}
	return true;
}


function update_unit(unit){
	var units= new Array();<?php 
		foreach ($Lang['eInventory']['label']['unit'] as $unit =>$disp_unit ){
			echo "units[\"{$unit}\"]= \"$disp_unit\";\n";
		}
	?>
	$('.unit').html(" ( "+units[unit]+" )");
}


$(function(){
	update_unit('mm');
	$('#select_metric').change(function(){
		update_unit($(this).val());
	});
	
});


</script>



<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<form name="form1" id="form1" method="post" action="barcode_templates_new_update.php" onSubmit="return checkForm(this)">

<table width="90%" align="center" border="0">
<tr><td>

	<table class="form_table_v30">
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['name']?></td>
			<td><input id="<?=$Lang['eInventory']['label']['name']?>" name="name" type="text" class="textboxnum number" value="" style="width:100%;"></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['NX']?> ( <?=$Lang['eInventory']['label']['piece']?> )</td>
			<td><input id="<?=$Lang['eInventory']['label']['NX']?>" name="NX" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['NY']?> ( <?=$Lang['eInventory']['label']['piece']?> )</td>
			<td><input id="<?=$Lang['eInventory']['label']['NY']?>" name="NY" type="text" class="textboxnum number" value=""></td>
		</tr>
				<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['font_size']?> (pt)</td>
			<td><input id="<?=$Lang['eInventory']['label']['font_size']?>" name="font_size" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['metric']?> <label class='unit'></label></td>
			<td><?=$linterface->GET_SELECTION_BOX($metric_unit, " id ='select_metric' name='metric' class='textboxnum number' ", '', 'mm');?></td>
		</tr>
				
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['paper-size-width']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['paper-size-width']?>" name="paper_size_width" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['paper-size-height']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['paper-size-height']?>" name="paper_size_height" type="text" class="textboxnum number" value=""></td>
		</tr>


		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['SpaceX']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['SpaceX']?>" name="SpaceX" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['SpaceY']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['SpaceY']?>" name="SpaceY" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['width']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['width']?>" name="width" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['height']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['height']?>" name="height" type="text" class="textboxnum number" value=""></td>
		</tr>
			
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['printing_margin_h']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['printing_margin_h']?>" name="printing_margin_h" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['printing_margin_v']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['printing_margin_v']?>" name="printing_margin_v" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['max_barcode_width']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['max_barcode_width']?>" name="max_barcode_width" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['max_barcode_height']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['max_barcode_height']?>" name="max_barcode_height" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['lMargin']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['lMargin']?>" name="lMargin" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['tMargin']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['tMargin']?>" name="tMargin" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class='field_title'><span class="tabletextrequire">*</span> <?=$Lang['eInventory']['label']['lineHeight']?><label class='unit'></label></td>
			<td><input id="<?=$Lang['eInventory']['label']['lineHeight']?>" name="lineHeight" type="text" class="textboxnum number" value=""></td>
		</tr>
		
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span> <?=$Lang["eInventory"]["label"]["info_order"]?></td>
			<td>
			   <table>
			      <tr>
			        <td><?=$labelInfoOrderSelections_left?></td>
			        <td><?=$labelInfoOrderSelections_right?></td>
			      </tr>
			   </table>
			</td>
		</tr>
		
		
	</table>
	
	<?=$linterface->MandatoryField();?>
	
	<div class="edit_bottom_v30">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='management_barcode_templates.php'")?>
	</div>	

</td></tr>
</table>

</form><?php
echo $linterface->FOCUS_ON_LOAD("form1.ResponsibilityCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
