<?php
// using: 

// #############################################
// Date: 2020-05-14 Tommy
// - changed sql order by
//
// Date: 2019-02-18 Isaac
// added ownership to the export data
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2015-09-25 Cameron
// add filter PurchaseDate range
//
// Date: 2015-06-08 Henry > ip.2.5.6.7.1
// add Funding Source Code Option [Case#W78607]
//
// Date: 2014-12-23 Henry > ip.2.5.5.12.1
// add QLabel format (GB) [Case#J72883]
//
// Date: 2014-11-12 Henry > ip.2.5.5.12.1
// add display_zero option [Case#T68957]
//
// Date: 2014-07-11 YatWoon [P64241]
// Fixed: Missing header column "Brand"
// deploy: ip.2.5.5.8.1
//
// Date: 2014-04-04 YatWoon
// SKH eInventory, revised sequence, format ($sys_custom['eInventoryCustForSKH'])
//
// Date: 2013-10-30 YatWoon
// add option "Remarks" [Case#2013-0808-1349-47073]
//
// Date: 2013-05-16 YatWoon
// fixed: cannot display multiple barcode label for same bulk item with different location/group/funding [Case#2013-0515-1633-02073]
//
// Date: 2013-05-10 YatWoon
// add option "sub-location code" with flag $special_feature['eDiscipline']['BarcodeDisplaySubLocationCode']
//
// Date: 2013-04-25 YatWoon
// add option "sub-location", "resources group code"
//
// Date: 2013-04-03 YatWoon
// add option "single item" and "bulk item" [Case#2013-0402-1200-31073]
//
// Date: 2013-02-26 Carlos
// Added fetching bulk items info and barcode
//
// Date: 2912-11-12 YatWoon
// QLabel format need save with "," and Big5
//
// Date: 2012-11-05 YatWoon
// Add bar code format, support QLabel (just export xxxxxx)
//
// Date: 2012-10-29 Rita
// Add "Purchase Date"
//
// Date: 2012-08-06 YatWoon
// Fixed: Missing to cater the selection option
// Improved: cater with 2 funding source for single item
//
// Date: 2012-07-27 YatWoon
// Add "barcode format" [Case#2012-0223-1108-40073]
//
// Date: 2011-11-08 YatWoon
// Fixed: csv header mis-match with mail merge field [Case#2011-1006-0941-18071]
//
// #############################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$file_content = "";

// #"Item Code","Barcode","Item Chinese Name","Item English Name","Chinese Description","English Description","Category Code","Sub-category Code","Group Code","Sub-Location Code","Funding Source Code","Ownership","Warranty Expiry Date","License","Serial No","Brand","Supplier","Supplier Contact","Supplier Description","Quotation No","Tender No","Invoice No","Purchase Date","Total Purchase Amount","Unit Price","Maintanence Details","Remarks"
// $exportColumn = array($i_InventorySystem_Item_Barcode, $i_InventorySystem_Item_Name, $i_InventorySystem_Item_Code, $i_InventorySystem_Group_Name, $i_InventorySystem_Item_Location, $i_InventorySystem_Item_Funding, $i_InventorySystem_Item_Brand_Name, $i_InventorySystem_Item_WarrantyExpiryDate, $i_InventorySystem_Item_License, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Supplier_Contact, $i_InventorySystem_Item_Supplier_Description, $i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num);

if ($sys_custom['eInventoryCustForSKH']) {
    $exportColumn = array(
        $i_InventorySystem_Item_Code,
        $i_InventorySystem_Item_Funding,
        $i_InventorySystem_Item_FundingSourceCode,
        $Lang['eInventory']['GenearlDescription'],
        $i_InventorySystem_Item_Brand_Name,
        $i_InventorySystem_Item_Location,
        $i_InventorySystem_Location
    );
    array_push($exportColumn, $i_InventorySystem_Item_Barcode, $i_InventorySystem_Item_Name, $i_InventorySystem_Group_Name, $i_InventorySystem_Caretaker_Code);
    array_push($exportColumn, $i_InventorySystem_Item_WarrantyExpiryDate, $i_InventorySystem_Item_License, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Supplier_Contact, $i_InventorySystem_Item_Supplier_Description, $i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num, $i_InventorySystem_Report_Col_Purchase_Date);
} else {
    $exportColumn = array(
        $i_InventorySystem_Item_Barcode,
        $i_InventorySystem_Item_Name,
        $i_InventorySystem_Item_Code,
        $i_InventorySystem_Group_Name,
        $i_InventorySystem_Caretaker_Code,
        $i_InventorySystem_Item_Location,
        $i_InventorySystem_Location,
        $i_InventorySystem_Item_Ownership
    );
    if ($special_feature['eDiscipline']['BarcodeDisplaySubLocationCode'])
        array_push($exportColumn, $i_InventorySystem_Location_Code);
    array_push($exportColumn, $i_InventorySystem_Item_Funding, $i_InventorySystem_Item_FundingSourceCode, $i_InventorySystem_Item_Brand_Name, $i_InventorySystem_Item_WarrantyExpiryDate, $i_InventorySystem_Item_License, $i_InventorySystem_Item_Supplier_Name, $i_InventorySystem_Item_Supplier_Contact, $i_InventorySystem_Item_Supplier_Description, $i_InventorySystem_Item_Serial_Num, $i_InventorySystem_Item_Tender_Num, $i_InventorySystem_Item_Invoice_Num, $i_InventorySystem_Report_Col_Purchase_Date);
    
    if ($linventory->enablePhotoDisplayInBarcodeRight()) {
        array_push($exportColumn, $Lang['eInventory']['ItemPhoto']);
    }
}
array_push($exportColumn, $i_InventorySystem_Item_Remark . " (" . $Lang['eInventory']['ForSingleItemOnly'] . ")");

// get the column list index to export
if (! isset($ColumnName)) {
    $SelectedColumn = $exportColumn;
    // $file_content.="\"$i_InventorySystem_Item_TagCode\",";
    // $file_content.="\"$i_InventorySystem_Item_Name\",";
    // $file_content.="\"$i_InventorySystem_Item_Code\",";
    // $file_content.="\"$i_InventorySystem_Group_Name\",";
    // $file_content.="\"$i_InventorySystem_Caretaker_Code\",";
    // $file_content.="\"$i_InventorySystem_Item_Location\",";
    // $file_content.="\"$i_InventorySystem_Location\",";
    //
    // if($special_feature['eDiscipline']['BarcodeDisplaySubLocationCode'])
    // $file_content.="\"$i_InventorySystem_Location_Code\",";
    //
    // $file_content.="\"$i_InventorySystem_Item_Funding\",";
    // $file_content.="\"$i_InventorySystem_Item_Brand_Name\",";
    // $file_content.="\"$i_InventorySystem_Item_WarrantyExpiryDate\",";
    // $file_content.="\"$i_InventorySystem_Item_License\",";
    // $file_content.="\"$i_InventorySystem_Item_Supplier_Name\",";
    // $file_content.="\"$i_InventorySystem_Item_Supplier_Contact\",";
    // $file_content.="\"$i_InventorySystem_Item_Supplier_Description\",";
    // $file_content.="\"$i_InventorySystem_Item_Serial_Num\",";
    // $file_content.="\"$i_InventorySystem_Item_Tender_Num\",";
    // $file_content.="\"$i_InventorySystem_Item_Invoice_Num\",";
    //
    // $file_content.="\"$i_InventorySystem_Report_Col_Purchase_Date\"";
    // $file_content.="\"$i_InventorySystem_Item_Remark\"";
    //
    // $file_content.="\n";
} else {
    // get the column name to export
    for ($i = 0; $i < sizeof($ColumnName); $i ++) {
        $SelectedColumn[] = $exportColumn[$ColumnName[$i]];
        // if($i != 0)
        // $file_content .= ",";
        // $file_content.="\"".$exportColumn[$ColumnName[$i]]."\"";
        // if ($i==(sizeof($ColumnName)-1))
        // {
        // $file_content .= "\n";
        // }
    }
}

if ($target_item_list != "") {
    $cond .= " AND a.ItemID IN ($target_item_list) ";
}

if (trim($search_code) != '') {
    $cond .= " And (a.ItemCode like '%" . $search_code . "%' or a.NameChi like '%" . $search_code . "%' or a.NameEng like '%" . $search_code . "%')";
}
if (is_array($location_id) && sizeof($location_id) > 0) {
    $cond .= " And c.LocationID in ('" . implode("','", $location_id) . "')";
}
if (is_array($category_id) && sizeof($category_id) > 0) {
    $cond .= " And d.CategoryID in ('" . implode("','", $category_id) . "')";
}
if (is_array($group_id) && sizeof($group_id) > 0) {
    $cond .= " And e.AdminGroupID in ('" . implode("','", $group_id) . "')";
}
if (isset($LocationLevelID)) {
    $cond .= " And c.LocationLevelID = '" . $LocationLevelID . "'";
}

if ($purchase_date_start) {
    $cond .= " And b.PurchaseDate>='" . $purchase_date_start . "'";
}
if ($purchase_date_end) {
    $cond .= " And b.PurchaseDate<='" . $purchase_date_end . "'";
}

if (isset($display_zero) && ! $display_zero) {
    $cond_display_zero = " And b1.Quantity>0";
}

$barcode_str = $barcodeformat == 1 ? "CONCAT('!',b.TagCode,'!')" : ($barcodeformat == 3 ? "b.TagCode" : "CONCAT('(!',b.TagCode,'!)')");
$bulk_barcode_str = $barcodeformat == 1 ? "CONCAT('!',ext.Barcode,'!')" : ($barcodeformat == 3 ? "ext.Barcode" : "CONCAT('(!',ext.Barcode,'!)')");

if ($select_single_item) {
    if ($sys_custom['eInventoryCustForSKH']) {
        // $funding_selection = "if(b.FundingSource2 is NULL or b.FundingSource2 ='', concat(f.Code,' ', ". $linventory->getInventoryNameByLang("f.") ." ) , concat(f.Code,' ',". $linventory->getInventoryNameByLang("f.") ." ,', ', f2.Code, ' ', ".$linventory->getInventoryNameByLang("f2.")."))";
        $funding_selection = "if(b.FundingSource2 is NULL or b.FundingSource2 ='', " . $linventory->getInventoryNameByLang("f.") . ", concat(" . $linventory->getInventoryNameByLang("f.") . ",', '," . $linventory->getInventoryNameByLang("f2.") . "))";
        $funding_selection .= ", if(b.FundingSource2 is NULL or b.FundingSource2 ='', f.Code , concat(f.Code,', ', f2.Code))";
    } else {
        $funding_selection = "if(b.FundingSource2 is NULL or b.FundingSource2 ='', " . $linventory->getInventoryNameByLang("f.") . ", concat(" . $linventory->getInventoryNameByLang("f.") . ",', '," . $linventory->getInventoryNameByLang("f2.") . "))";
        $funding_selection .= ", if(b.FundingSource2 is NULL or b.FundingSource2 ='', f.Code , concat(f.Code,', ', f2.Code))";
    }
    
    if ($select_single_item && $select_bulk_item) {
        $sql .= "(";
    }
    
    $sql .= "SELECT 
				a.ItemType,
				a.ItemID,
				a.CategoryID,
				a.category2ID,
				" . $linventory->getInventoryItemNameByLang("a.") . ",
				" . $linventory->getInventoryDescriptionNameByLang("a.") . ",
				a.ItemCode,
				IF(a.Ownership = 1,'$i_InventorySystem_Ownership_School',IF(a.Ownership = 2,'$i_InventorySystem_Ownership_Government',' - ')),
				b.PurchaseDate,
				CONCAT('$',b.PurchasedPrice),
				IF(b.SupplierName = '', ' ', b.SupplierName),
				IF(b.SupplierContact = '', ' ', b.SupplierContact),
				IF(b.SupplierDescription = '', ' ', b.SupplierDescription),
				IF(b.InvoiceNo = '', ' ', b.InvoiceNo),
				IF(b.QuotationNo = '', ' ', b.QuotationNo),
				IF(b.TenderNo = '', ' ', b.TenderNo),
				$barcode_str,
				IF(b.Brand = '', ' ', b.Brand),
				" . $linventory->getInventoryNameByLang("e.") . ",
				CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("floor.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") as location,
				$funding_selection,
				IF(b.WarrantyExpiryDate = '', ' ', b.WarrantyExpiryDate),
				IF(b.SoftwareLicenseModel = '', ' ', b.SoftwareLicenseModel),
				b.SerialNumber,
				' - ' as item_photo, 
				" . $linventory->getInventoryNameByLang("c.") . ",
				e.Code,
				c.Code,
				b.ItemRemark,
				" . $linventory->getInventoryNameByLang("d.") . ",
				" . $linventory->getInventoryNameByLang("d2.") . "
		FROM 
				INVENTORY_ITEM AS a INNER JOIN 
				INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID AND a.RecordStatus = 1) LEFT OUTER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) LEFT OUTER JOIN
				INVENTORY_LOCATION_LEVEL AS floor ON (c.LocationLevelID = floor.LocationLevelID) LEFT OUTER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (floor.BuildingID = building.BuildingID) LEFT OUTER JOIN
				INVENTORY_CATEGORY AS d ON (a.CategoryID = d.CategoryID)
				LEFT JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID 
				LEFT JOIN INVENTORY_ADMIN_GROUP AS e ON (b.GroupInCharge = e.AdminGroupID) LEFT OUTER JOIN
				INVENTORY_FUNDING_SOURCE AS f ON (b.FundingSource = f.FundingSourceID)
				LEFT OUTER JOIN INVENTORY_FUNDING_SOURCE AS f2 ON (b.FundingSource2 = f2.FundingSourceID) 
		WHERE
				a.ItemType = 1
				$cond ";
}

if ($select_single_item && $select_bulk_item) {
    $sql .= ") UNION (";
}

if ($select_bulk_item) {
    
    if ($sys_custom['eInventoryCustForSKH']) {
        // $funding_selection_bulk = "if(b1.FundingSourceID is NULL or b1.FundingSourceID ='', ' - ', concat(f.Code,' ', ". $linventory->getInventoryNameByLang("f.") ." ) ) as funding";
        $funding_selection_bulk = "if(b1.FundingSourceID is NULL or b1.FundingSourceID ='', ' - ', " . $linventory->getInventoryNameByLang("f.") . ")";
        $funding_selection_bulk .= ", if(b1.FundingSourceID is NULL or b1.FundingSourceID ='', ' - ', f.code)";
    } else {
        $funding_selection_bulk = "if(b1.FundingSourceID is NULL or b1.FundingSourceID ='', ' - ', " . $linventory->getInventoryNameByLang("f.") . ")";
        $funding_selection_bulk .= ", if(b1.FundingSourceID is NULL or b1.FundingSourceID ='', ' - ', f.code)";
    }
    
    $sql .= "SELECT 
			a.ItemType,
			a.ItemID,
			a.CategoryID,
			a.category2ID,
			" . $linventory->getInventoryItemNameByLang("a.") . ",
			" . $linventory->getInventoryDescriptionNameByLang("a.") . ",
			a.ItemCode,
			IF(a.Ownership = 1,'$i_InventorySystem_Ownership_School',IF(a.Ownership = 2,'$i_InventorySystem_Ownership_Government',' - ')),
			b.PurchaseDate,
			CONCAT('$',b.PurchasedPrice),
			IF(b.SupplierName = '', ' - ', b.SupplierName),
			IF(b.SupplierContact = '', ' - ', b.SupplierContact),
			IF(b.SupplierDescription = '', ' - ', b.SupplierDescription),
			IF(b.InvoiceNo = '', ' - ', b.InvoiceNo),
			IF(b.QuotationNo = '', ' - ', b.QuotationNo),
			IF(b.TenderNo = '', ' - ', b.TenderNo),
			$bulk_barcode_str,
			' - ' as Brand,
			" . $linventory->getInventoryNameByLang("e.") . " as group_name,
			CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("floor.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") as location,
			$funding_selection_bulk as funding, 
			' - ' as WarrantyExpiryDate,
			' - ' as SoftwareLicenseModel,
			' - ' as SerialNumber,
			' - ' as item_photo, 
			" . $linventory->getInventoryNameByLang("c.") . ",
			e.Code,
			c.Code,
			b.Remark,
			" . $linventory->getInventoryNameByLang("d.") . ",
			" . $linventory->getInventoryNameByLang("d2.") . "
		FROM INVENTORY_ITEM AS a 
		INNER JOIN INVENTORY_ITEM_BULK_LOCATION as b1 on b1.ItemID=a.ItemID
		INNER JOIN INVENTORY_ITEM_BULK_LOG as b ON b.ItemID=a.ItemID
		INNER JOIN INVENTORY_ITEM_BULK_EXT as ext ON ext.ItemID=b.ItemID  
		LEFT JOIN INVENTORY_LOCATION AS c ON b1.LocationID = c.LocationID 
		LEFT JOIN INVENTORY_LOCATION_LEVEL AS floor ON c.LocationLevelID = floor.LocationLevelID 
		LEFT JOIN INVENTORY_LOCATION_BUILDING AS building ON floor.BuildingID = building.BuildingID 
		LEFT JOIN INVENTORY_CATEGORY AS d ON a.CategoryID = d.CategoryID 
		LEFT JOIN INVENTORY_CATEGORY_LEVEL2 AS d2 ON a.Category2ID = d2.Category2ID 
		LEFT JOIN INVENTORY_ADMIN_GROUP AS e ON b1.GroupInCharge = e.AdminGroupID 
		LEFT JOIN INVENTORY_FUNDING_SOURCE AS f ON b1.FundingSourceID = f.FundingSourceID 
		LEFT JOIN INVENTORY_PHOTO_PART AS ipp ON a.PhotoLink = ipp.PartID 
		WHERE 
			a.ItemType=2 AND a.RecordStatus = 1
			$cond 
			$cond_display_zero 
		GROUP BY a.ItemID, group_name, location, funding";
}

if ($select_single_item && $select_bulk_item) {
    $sql .= ")";
}

if($display_order == 1){
    $orderby = "ItemCode";
}else if($display_order == 2){
    $orderby = "location, ItemCode";
}else{
    $orderby = "ItemCode";
}
$sql .= " ORDER BY ".$orderby;

$arr_result = $linventory->returnArray($sql, 22);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        
        list ($item_type, $item_id, $item_cat, $item_cat2, $item_name, $item_desc, $item_code, $item_ownership, $item_purchase_date, $item_purchased_price, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_invoice, $item_quotation, $item_tender, $item_tag_code, $item_brand, $item_admin_group, $item_location, $item_funding, $item_funding_code, $item_warranty_expiry_date, $item_license, $item_serial_num, $item_photo, $item_sublocation, $item_admin_group_code, $item_sublocation_code, $item_remark, $item_cateogry, $item_subcateogry) = $arr_result[$i];
        
        if ($item_purchase_date == "0000-00-00")
            $item_purchase_date = "";
        if ($item_warranty_expiry_date == "0000-00-00")
            $item_warranty_expiry_date = "";
        
        if ($item_type == 2) {
            $item_remark = "";
        }
        
        /*
         * if($linventory->enablePhotoDisplayInBarcodeRight()){
         * $temprows = array($item_tag_code, $item_name, $item_code, $item_admin_group, $item_location, $item_funding, $item_brand, $item_warranty_expiry_date, $item_license, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_serial_num, $item_tender_num, $item_invoice_num, $item_purchase_date, $item_photo);
         * }else{
         * $temprows = array($item_tag_code, $item_name, $item_code, $item_admin_group, $item_location, $item_funding, $item_brand, $item_warranty_expiry_date, $item_license, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_serial_num, $item_tender_num, $item_invoice_num, $item_purchase_date);
         * }
         */
        
        if ($sys_custom['eInventoryCustForSKH']) {
            $item_genearl_description = $item_cateogry . " - " . $item_subcateogry . " (" . $item_name . ")";
            
            $temprows = array(
                $item_code,
                $item_funding,
                $item_funding_code,
                $item_genearl_description,
                $item_brand,
                $item_location,
                $item_sublocation
            );
            array_push($temprows, $item_tag_code, $item_name, $item_admin_group, $item_admin_group_code);
            array_push($temprows, $item_warranty_expiry_date, $item_license, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_serial_num, $item_tender, $item_invoice, $item_purchase_date);
        } else {
            $temprows = array(
                $item_tag_code,
                $item_name,
                $item_code,
                $item_admin_group,
                $item_admin_group_code,
                $item_location,
                $item_sublocation,
                $item_ownership
            );
            if ($special_feature['eDiscipline']['BarcodeDisplaySubLocationCode'])
                array_push($temprows, $item_sublocation_code);
            array_push($temprows, $item_funding, $item_funding_code, $item_brand, $item_warranty_expiry_date, $item_license, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_serial_num, $item_tender, $item_invoice, $item_purchase_date);
            if ($linventory->enablePhotoDisplayInBarcodeRight()) {
                array_push($temprows, " ");
            }
        }
        array_push($temprows, $item_remark);
        
        if (! isset($ColumnName)) {
            $rows[] = $temprows;
            // $file_content .= "\"$item_tag_code\",\"$item_name\",\"$item_admin_group_code\",\"$item_code\",\"$item_admin_group\",\"$item_location\"";
            // if($special_feature['eDiscipline']['BarcodeDisplaySubLocationCode'])
            // $file_content .= "\"$item_sublocation_code\",";
            // $file_content .= "\"$item_sublocation\",\"$item_funding\",\"$item_brand\",\"$item_warranty_expiry_date\",\"$item_license\",\"$item_supplier_name\",\"$item_supplier_contact\",\"$item_supplier_desc\",\"$item_remark\"\n";
        } else {
            for ($j = 0; $j < sizeof($ColumnName); $j ++) {
                
                // if($j!=0)
                // $file_content .= ",";
                $rows[$i][] = $temprows[$ColumnName[$j]];
                // $file_content .= "\"".$temprows[$ColumnName[$j]]."\"";
                //
                // if ($j == (sizeof($ColumnName)-1)) {
                // $file_content .= "\n";
                // }
            }
        }
    }
}
if (sizeof($arr_result) == 0) {
    // $file_content .= "\"$i_no_record_exists_msg\"\n";
    
    $rows[] = array(
        $i_no_record_exists_msg
    );
}

// $display = $file_content;

intranet_closedb();

$filename = "item_details_unicode.csv";
if ($barcodeformat == 4) // for QLabel format (GB)
{
    $export_content .= $lexport->GET_EXPORT_TXT($rows, $SelectedColumn, ",", "\r\n", ",", "", "", 1);
    $lexport->EXPORT_FILE($filename, $export_content, '', '', 'GBK');
} else 
    if ($barcodeformat == 3) // for QLabel format (Big5)
{
        $export_content .= $lexport->GET_EXPORT_TXT($rows, $SelectedColumn, ",", "\r\n", ",", "", "", 1);
        $lexport->EXPORT_FILE($filename, $export_content, '', '', 'Big5');
    } else {
        $export_content .= $lexport->GET_EXPORT_TXT($rows, $SelectedColumn);
        $lexport->EXPORT_FILE($filename, $export_content);
    }

?>
