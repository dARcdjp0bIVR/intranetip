<?php
// modifying :
/**
 * **************************** Change Log **************************************************
 * 2018-02-22 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log ************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");

// page-break css
// $page_breaker = "<P style='page-break-before: always'>";

// init no. of barcode label per page
$label_per_page = 16;

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$targetBuilding = str_replace("'", "", stripslashes($targetBuilding));
$targetFloor = str_replace("'", "", stripslashes($targetFloor));
$targetRooms = str_replace("'", "", stripslashes($targetRooms));
$barcodeWidth = str_replace("'", "", stripslashes($barcodeWidth));

if ($barcodeWidth == "") {
    $barcodeWidth = DEFAULT_BARCODE_WIDTH;
}

if ($targetBuilding != "") {
    $cond_building = " building.BuildingID = $targetBuilding AND ";
}
if ($targetFloor != "") {
    $cond_floor = " floor.LocationLevelID = $targetFloor AND ";
}
if ($targetRooms != "") {
    $cond_rooms = " room.LocationID IN ($targetRooms) AND ";
}

$sql = "SELECT 
			CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("floor.") . ",' > '," . $linventory->getInventoryNameByLang("room.") . "),
			CONCAT(building.Barcode,'+',floor.Barcode,'+',room.Barcode)
		FROM 
			INVENTORY_LOCATION_BUILDING AS building 
			INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
			INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
		WHERE
			$cond_building
			$cond_floor
			$cond_rooms
			building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
		ORDER BY
			building.DisplayOrder,building.NameEng, floor.DisplayOrder, floor.NameEng, room.DisplayOrder, room.NameEng";
$result = $linventory->returnArray($sql, 2);

if (sizeof($result) > 0) {
    for ($i = 0; $i < sizeof($result); $i ++) {
        list ($full_name, $location_barcode) = $result[$i];
        
        $ans = $i % ($label_per_page);
        
        if (($ans == 0) && ($i != 0)) {
            // $report_content .= $page_breaker;
            $report_content .= "<div style='page-break-before: always'>";
        }
        if ($ans == 0) {
            $report_content .= "&nbsp;";
            $report_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">\n";
        }
        
        if ($i % 2 == 0) {
            $report_content .= "<tr>\n";
        }
        
        $barcode_label = "<img src='barcode.php?barcode=" . rawurlencode($location_barcode) . "&width=$barcodeWidth'>";
        $report_content .= "<td class=\"eSporttdborder eSportprinttext\" align='center'>$barcode_label<br>$full_name</td>\n";
        
        if ($i % 2 == 1) {
            $report_content .= "</tr>\n";
        }
        
        if ($ans == $label_per_page - 1) {
            $report_content .= "</table>\n</div>\n";
        }
    }
}

?>
<STYLE TYPE="text/css">
</STYLE>

<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?=$report_content;?>
<?include_once ($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_footer.php");intranet_closedb();?>