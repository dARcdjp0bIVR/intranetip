<?php
// using: yat

##########################################################################################
#
#	Date:	2013-03-13	YatWoon
#			allow search for bulk item
#
##########################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_opendb();

$libdb 	= new libdb();
$lv 	= new libinventory();


$InputValue = stripslashes(urldecode($_REQUEST['q']));

$InputValue = str_replace(',',';',$InputValue);
$SearchValue = strstr($InputValue,";")?trim(substr($InputValue,strrpos($InputValue,";")+1)):trim($InputValue);  

if($SearchValue=='') die;

/*
AND
			ItemType = 1
*/
			
$sql = "SELECT 
			ItemCode, ".$lv->getInventoryNameByLang()." AS ItemName 
		FROM 
			INVENTORY_ITEM 
		WHERE 
			RecordStatus=1
		AND
			(ItemCode like '%".$SearchValue."%' OR NameChi like '%".$SearchValue."%' OR NameEng like '%".$SearchValue."%') 
		ORDER BY 
			ItemCode
		";
$Result = $libdb->returnArray($sql,3); 
			
for ($i=0; $i< sizeof($Result); $i++) {
	echo $Result[$i]['ItemCode']."|[".$Result[$i]['ItemCode']."]&nbsp;&nbsp;".htmlspecialchars($Result[$i]['ItemName'])."\n";
}

intranet_closedb();
die;
?>