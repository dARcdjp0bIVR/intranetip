<?php
// using :
/*
 * 2019-05-13 Henry
 * Security fix: SQL without quote
 *
 * 2019-05-01 (Henry)
 * security issue fix for SQL
 *
 * 2018-02-22 (Henry)
 * add access right checking [Case#E135442]
 *
 * 2015-11-03 (Cameron)
 * - Fix bug: disable extra Building_Floor_Selection option list
 *
 * 2013-02-21 (Carlos): Add navigation tag [Resource Mgmt Group Barcode] and [Funding Source Barcode]
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$min_barcode_width = DEFAULT_MINIMUM_BARCODE_WIDTH;
$max_barcode_width = DEFAULT_MAXIMUN_BARCODE_WIDTH;

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_BarcodeLabels";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$targetBuilding = IntegerSafe($targetBuilding);
$targetLocationLevel = IntegerSafe($targetLocationLevel);

$llocation = new liblocation();
$llocation_ui = new liblocation_ui();

$TAGS_OBJ[] = array(
    $i_InventorySystem_Report_ItemDetails,
    "item_details.php",
    0
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['LocationBarcode'],
    "location_barcode.php",
    1
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['ResourceMgmtGroupBarcode'],
    "admin_group_barcode.php",
    0
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['FundingSourceBarcode'],
    "funding_source_barcode.php",
    0
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$opt_Building = $llocation_ui->Get_Building_Selection($targetBuilding, "targetBuilding", "document.form1.flag.value=0; document.form1.DisplayBarcode.value=0; javascript:resetLocationSelection('Building'); this.form.submit();", 0, 0, $Lang['SysMgr']['Location']['All']['Building']);
$building_selection = "<tr><td valign=\"top\" width=\"30%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_alert_pleaseselect . "</td><td>" . $opt_Building . "</td></tr>";

// Generate Location Selection Box - Floor #
if ($targetBuilding != "") {
    $opt_LocationLevel = $llocation_ui->Get_Floor_Selection($targetBuilding, $targetLocationLevel, "targetLocationLevel", "document.form1.flag.value=0; document.form1.DisplayBarcode.value=0; javascript:resetLocationSelection('Floor'); this.form.submit();", 0, 0, $Lang['SysMgr']['Location']['LocationList']);
    $location_level_selection .= "<tr><td></td><td>" . $opt_LocationLevel . "</td></tr>";
    ;
}
// $opt_LocationLevel = $llocation_ui->Get_Building_Floor_Selection($TargetFloor, 'TargetFloor', 'resetLocationSelection(\'Floor\'); this.form.submit();', 0, 0, '');
// $location_level_selection .= "<tr><td></td><td>".$opt_LocationLevel."</td></tr>";;

// Generate Location Selection Box - Room #
if (($targetLocationLevel != "") && ($targetBuilding != "")) {
    
    $sql = "SELECT LocationID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_LOCATION WHERE LocationLevelID = '".$targetLocationLevel."' AND RecordStatus = 1";
    $result = $linventory->returnArray($sql, 2);
    
    $room_selection = "<select id=\"targetLocation[]\" name=\"targetLocation[]\" multiple size=\"10\">";
    if (sizeof($targetLocation) == 0) {
        $room_selection .= "<option value=\"\" selected> - " . $i_alert_pleaseselect . " - </option>";
    } else {
        $room_selection .= "<option value=\"\"> - " . $i_alert_pleaseselect . " - </option>";
    }
    
    if (sizeof($result) > 0) {
        for ($i = 0; $i < sizeof($result); $i ++) {
            list ($location_id, $location_name) = $result[$i];
            
            $selected = "";
            if (is_array($targetLocation)) {
                if (in_array($location_id, $targetLocation)) {
                    $selected = " SELECTED ";
                }
            }
            $room_selection .= "<option value=\"$location_id\" $selected>$location_name</option>";
        }
    }
    $room_selection .= "</select>";
    
    $location_selection .= "<tr><td></td>";
    $location_selection .= "<td>" . $room_selection . "</td>";
}
$barcode_width_selection .= "<tr>";
$barcode_width_selection .= "<td valign=\"top\" width=\"30%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['FieldTitle']['BarcodeWidth'] . "</td>";
if ($barcodeWidth == "") {
    // $barcode_width_selection .= "<td><input type='text' name='barcodeWidth' id='barcodeWidth' value='".DEFAULT_BARCODE_WIDTH."'></td>";
    
    $barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
    do {
        $barcodeWidthSelection .= "<option value=" . $min_barcode_width . ">" . $min_barcode_width . "</option>";
        $min_barcode_width = $min_barcode_width + DEFAULT_BARCODE_WIDTH_INCREMENT;
    } while ($min_barcode_width <= $max_barcode_width);
    $barcodeWidthSelection .= "</select>";
    $barcode_width_selection .= "<td>" . $barcodeWidthSelection . "</td>";
} else {
    // $barcode_width_selection .= "<td><input type='text' name='barcodeWidth' id='barcodeWidth' value='".$barcodeWidth."'></td>";
    $barcodeWidthSelection = "<select name='barcodeWidth' id='barcodeWidth'>";
    do {
        if ($barcodeWidth == $min_barcode_width) {
            $barcodeWidthSelection .= "<option value=" . $min_barcode_width . " selected>" . $min_barcode_width . "</option>";
        } else {
            $barcodeWidthSelection .= "<option value=" . $min_barcode_width . ">" . $min_barcode_width . "</option>";
        }
        $min_barcode_width = $min_barcode_width + DEFAULT_BARCODE_WIDTH_INCREMENT;
    } while ($min_barcode_width <= $max_barcode_width);
    $barcodeWidthSelection .= "</select>";
    $barcode_width_selection .= "<td>" . $barcodeWidthSelection . "</td>";
}
$barcode_width_selection .= "</tr>";
// End #

if (($flag == 1) && ($DisplayBarcode == 1)) {
    $barcode_table .= "<br>";
    $barcode_table .= "<table border='0' width='87%' align='center' cellpadding='4' cellspacing='0'>";
    
    if (($targetBuilding == "") && ($targetLocationLevel == "") && (! is_array($targetLocation))) {
        // # List All Location
        $barcode_table .= "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetBuilding','$targetLocationLevel','','$barcodeWidth')", "", "", "", "", 0) . "</td><td></td></tr>";
        $barcode_table .= "<tr class=\"tabletop\">";
        $barcode_table .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['LocationBarcode'] . "</td>";
        $barcode_table .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['FloorName'] . "</td>";
        $barcode_table .= "</tr>";
        
        $sql = "SELECT 
					CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("floor.") . ",' > '," . $linventory->getInventoryNameByLang("room.") . "),
					CONCAT(building.Barcode,'+',floor.Barcode,'+',room.Barcode)
				FROM 
					INVENTORY_LOCATION_BUILDING AS building 
					INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
					INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
				WHERE
					building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
				ORDER BY
					building.DisplayOrder,building.NameEng, floor.DisplayOrder, floor.NameEng, room.DisplayOrder, room.NameEng";
        
        $result = $linventory->returnArray($sql, 2);
        
        if (sizeof($result) > 0) {
            for ($i = 0; $i < sizeof($result); $i ++) {
                list ($full_name, $location_barcode) = $result[$i];
                $css = ($i % 2 == 0) ? " class=\"tablerow1\" " : " class=\"tablerow2\" ";
                
                $barcode_label = "<img src='barcode.php?barcode=" . rawurlencode($location_barcode) . "&width=$barcodeWidth'>";
                
                $barcode_table .= "<tr $css>";
                $barcode_table .= "<td>$barcode_label</td>";
                $barcode_table .= "<td>$full_name</td>";
                $barcode_table .= "</tr>";
            }
        }
    }
    if (($targetBuilding != "") && ($targetLocationLevel == "") && (! is_array($targetLocation))) {
        // # List All Location In Selected Building
        $barcode_table .= "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetBuilding','$targetLocationLevel','','$barcodeWidth')", "", "", "", "", 0) . "</td><td></td></tr>";
        $barcode_table .= "<tr class=\"tabletop\">";
        $barcode_table .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['LocationBarcode'] . "</td>";
        $barcode_table .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['FloorName'] . "</td>";
        $barcode_table .= "</tr>";
        
        $sql = "SELECT 
					CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("floor.") . ",' > '," . $linventory->getInventoryNameByLang("room.") . "),
					CONCAT(building.Barcode,'+',floor.Barcode,'+',room.Barcode)
				FROM 
					INVENTORY_LOCATION_BUILDING AS building 
					INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
					INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
				WHERE
					building.BuildingID = '".$targetBuilding."' AND
					building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
				ORDER BY
					building.DisplayOrder,building.NameEng, floor.DisplayOrder, floor.NameEng, room.DisplayOrder, room.NameEng";
        $result = $linventory->returnArray($sql, 2);
        
        if (sizeof($result) > 0) {
            for ($i = 0; $i < sizeof($result); $i ++) {
                list ($full_name, $location_barcode) = $result[$i];
                $css = ($i % 2 == 0) ? " class=\"tablerow1\" " : " class=\"tablerow2\" ";
                
                $barcode_label = "<img src='barcode.php?barcode=" . rawurlencode($location_barcode) . "&width=$barcodeWidth'>";
                
                $barcode_table .= "<tr $css>";
                $barcode_table .= "<td>$barcode_label</td>";
                $barcode_table .= "<td>$full_name</td>";
                $barcode_table .= "</tr>";
            }
        }
    } else {
        if (sizeof($targetLocation) > 0) {
            // # List Selected Location In Selected Floor
            $targetLocationID = implode(",", $targetLocation);
            $barcode_table .= "<tr><td class=\"tabletext\">" . $linterface->GET_LNK_PRINT("javascript:openPrintPage('$targetBuilding','$targetLocationLevel','$targetLocationID','$barcodeWidth')", "", "", "", "", 0) . "</td><td></td></tr>";
            $barcode_table .= "<tr class=\"tabletop\">";
            $barcode_table .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['LocationBarcode'] . "</td>";
            $barcode_table .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['FloorName'] . "</td>";
            $barcode_table .= "</tr>";
            
            $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("floor.") . ",' > '," . $linventory->getInventoryNameByLang("room.") . "),
						CONCAT(building.Barcode,'+',floor.Barcode,'+',room.Barcode)
					FROM 
						INVENTORY_LOCATION_BUILDING AS building 
						INNER JOIN INVENTORY_LOCATION_LEVEL AS floor ON (building.BuildingID = floor.BuildingID)
						INNER JOIN INVENTORY_LOCATION AS room ON (floor.LocationLevelID = room.LocationLevelID)
					WHERE
						building.BuildingID = '".$targetBuilding."' AND
						floor.LocationLevelID = '".$targetLocationLevel."' AND 
						room.LocationID IN ($targetLocationID) AND 
						building.RecordStatus = 1 AND floor.RecordStatus = 1 AND room.RecordStatus = 1
					ORDER BY
						building.DisplayOrder,building.NameEng, floor.DisplayOrder, floor.NameEng, room.DisplayOrder, room.NameEng";
            $result = $linventory->returnArray($sql, 2);
            
            if (sizeof($result) > 0) {
                for ($i = 0; $i < sizeof($result); $i ++) {
                    list ($full_name, $location_barcode) = $result[$i];
                    
                    $css = ($i % 2 == 0) ? " class=\"tablerow1\" " : " class=\"tablerow2\" ";
                    
                    $barcode_label = "<img src='barcode.php?barcode=" . rawurlencode($location_barcode) . "&width=$barcodeWidth'>";
                    
                    $barcode_table .= "<tr $css>";
                    $barcode_table .= "<td>$barcode_label</td>";
                    $barcode_table .= "<td>$full_name</td>";
                    $barcode_table .= "</tr>";
                }
            }
        }
    }
    
    $barcode_table .= "</table>";
}
?>

<script language="javascript">
	function openPrintPage(targetBuilding,targetFloor,targetRooms,barcodeWidth)
	{
		newWindow("location_barcode_print.php?targetBuilding='"+targetBuilding+"'&targetFloor='"+targetFloor+"'&targetRooms='"+targetRooms+"'&barcodeWidth='"+barcodeWidth+"'",10);
	}
	function resetLocationSelection(CallFrom){
		if(CallFrom == 'Building'){
			if(document.getElementById("targetLocationLevel")){
				document.getElementById("targetLocationLevel").value = "";
			}
			if(document.getElementById("targetLocation")){
				document.getElementById("targetLocation").value = "";
			}
		}
		if(CallFrom == 'Floor'){
			if(document.getElementById("targetLocation")){
				document.getElementById("targetLocation").value = "";
			}
		}
	}
	function checkForm()
	{
		var obj = document.form1;
		var jChecking = true;
		
		if(document.getElementById('targetLocation[]'))
		{
			for(i=0; i<document.getElementById('targetLocation[]').options.length; i++)
			{
				if((document.getElementById('targetLocation[]').options[i].selected) && (i==0))
				{
					jChecking = false;
					alert("<?=$i_InventorySystem_JSWarning_NewItem_SelectSubLocation;?>");
					break;
				}
			}
		}
		
		if(jChecking == true)
		{
			if (obj.flag.value == "1")
			{
				if(check_positive_nonzero_int(obj.barcodeWidth,"<?=$Lang['eInventory']['JSWarning']['InvalidBarcodeWidth'];?>"))
				{
					obj.action = "location_barcode.php";
					document.form1.submit();
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				obj.action = "location_barcode.php";
				document.form1.submit();
				return true;	
			}
		}
		return false;
	}
	
</script>
<br>
<form name="form1" action="" method="POST" onSubmit="return checkForm()">
	<table border="0" cellspacing="0" cellpadding="5" width="90%"
		align="center">
		<?=$building_selection;?>
		<?=$location_level_selection;?>
		<?=$location_selection;?>
		<?=$barcode_width_selection;?>
		<tr>
			<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.flag.value=1; document.form1.DisplayBarcode.value=1;"); ?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="dotline"><img src=\
				"<?=$image_path;?>/<?=$LAYOUT_SKIN;?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<input type="hidden" name="flag" value="">
		<input type="hidden" name="DisplayBarcode" value="0">
	</table>
	<?=$barcode_table;?>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>