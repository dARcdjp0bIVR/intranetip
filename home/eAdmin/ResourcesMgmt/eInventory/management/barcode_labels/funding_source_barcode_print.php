<?php
// using : 
/*
 * Page created on 2013-02-20
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

// init no. of barcode label per page
$label_per_page = 16;

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$linventory		= new libinventory();

$targetFundingSource = str_replace("'","",stripslashes($_REQUEST['targetFundingSource']));
$barcodeWidth = str_replace("'","",stripslashes($_REQUEST['barcodeWidth']));
if($barcodeWidth == "")
{
	$barcodeWidth = DEFAULT_BARCODE_WIDTH;
}


$lang = ($intranet_session_language =="b5" || $intranet_session_language == "gb");
if ($lang)
{
	$firstChoice = "NameChi";
	$altChoice = "NameEng";
}
else
{
	$firstChoice = "NameEng";
	$altChoice = "NameChi";
}

$namefield = "IF($firstChoice IS NULL OR TRIM($firstChoice) = '',$altChoice,$firstChoice)";

$sql = "SELECT FundingSourceID, $namefield, Barcode FROM INVENTORY_FUNDING_SOURCE WHERE FundingSourceID IN ($targetFundingSource) ORDER BY DisplayOrder";
$result = $linventory->returnArray($sql);

if(sizeof($result)>0)
{
	for($i=0; $i<sizeof($result); $i++)
	{
		list($funding_source_id, $funding_name, $funding_barcode) = $result[$i];
		
		$ans = $i % ($label_per_page);

		if(($ans == 0) && ($i != 0))
		{
			$report_content .= "<div style='page-break-before: always'>";
		}
		if($ans == 0){
			$report_content .= "&nbsp;";
			$report_content .= "<table width=\"90%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\" align=\"center\" class=\"eSporttableborder\">\n";
		}
		
		if($i%2 == 0){
			$report_content .= "<tr>\n";
		}
		
		$barcode_label = "<img src='barcode.php?barcode=".rawurlencode($funding_barcode)."&width=$barcodeWidth'>";
		$report_content .= "<td class=\"eSporttdborder eSportprinttext\" align='center'>$barcode_label<br>$funding_name</td>\n";
		
		if($i%2 == 1){
			$report_content .= "</tr>\n";
		}
		
		if($ans == $label_per_page-1)
		{
			$report_content .= "</table>\n</div>\n";
		}
	}	
}
?>
<STYLE TYPE="text/css">
     
</STYLE> 

<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?=$report_content;?>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>