<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

# Generate Page Tag #
$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/items_full_list.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/category.php", 0);
# End #

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");

if($item_type == ITEM_TYPE_SINGLE)
{
	$sql = "UPDATE 
				INVENTORY_ITEM_SINGLE_EXT 
			SET
				LocationID = $targetSubLocation
			WHERE
				ItemID = $item_id
			";
	$result = $linventory->db_db_query($sql);
	
	### get original location info ###
	$sql = "SELECT 
					CONCAT(".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("a.").")
			FROM
					INVENTORY_LOCATION AS a INNER JOIN
					INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID)
			WHERE 
					a.LocationID = $origianl_location_id";
	$arr_orignial_location = $linventory->returnVector($sql);
	
	### get New location info ###
	$sql = "SELECT 
					CONCAT(".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("a.").")
			FROM
					INVENTORY_LOCATION AS a INNER JOIN
					INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID)
			WHERE 
					a.LocationID = $targetSubLocation";
	$arr_new_location = $linventory->returnVector($sql);
	
	$original_location = $arr_orignial_location[0];
	$new_location = $arr_new_location[0];
	
	$remark = "$i_InventorySystem_From: $original_location<BR>";
	$remark .= "$i_InventorySystem_To: $new_location";
	$remark = addslashes($remark);
	
	$sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, DateInput, DateModified)
			VALUES
					($item_id, '$curr_date', ".ITEM_ACTION_CHANGELOCATION.", $UserID, '$remark', NOW(), NOW())
			";
	$result = $linventory->db_db_query($sql);
	
}
if($item_type == ITEM_TYPE_BULK)
{
	$bulk_new_qty = intval($bulk_new_qty);
	$qty_change = $bulk_new_qty;
	
	$sql = "UPDATE 
					INVENTORY_ITEM_BULK_LOCATION 
			SET
					Quantity = Quantity - $qty_change
			WHERE 
					ItemID = $item_id AND
					LocationID = $targetOldLocation AND
					GroupInCharge = $targetOldGroup
			";
			
	//echo "Part1: ".$sql."<BR>";		
	$linventory->db_db_query($sql);
	
	### Check any exist bult item in the destination ###
	$sql = "SELECT RecordID FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetNewSubLocation";
	$arr_exist_record = $linventory->returnVector($sql);
	
	if(sizeof($arr_exist_record) > 0)
	{
		$record_id = $arr_exist_record[0];
		
		### Get the destination admin group for the bulk item ###
		$sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetNewSubLocation";
		$arr_destination_admin_group = $linventory->returnVector($sql,1);
		if(sizeof($arr_destination_admin_group)>0)
		{
			$destination_admin_group_id = $arr_destination_admin_group[0];
		}
		
		### Get the destination Funding Source ID for the bulk item ###
		$sql = "SELECT FundingSource FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND LocationID = $targetNewSubLocation AND GroupInCharge = $destination_admin_group_id AND Action IN (".ITEM_ACTION_PURCHASE.",".ITEM_ACTION_CHANGELOCATION.") ORDER BY DateInput DESC LIMIT 0,1";
		$arr_destination_funding_source = $linventory->returnVector($sql);
		if(sizeof($arr_destination_funding_source)>0)
		{
			$destination_funding_source = $arr_destination_funding_source[0];
		}
		
		//echo "Destination ADMIN: ".$destination_admin_group_id."<BR>";
		//echo "Destination ID: ".$targetNewSubLocation."<BR>";
		//echo "Destination Funding: ".$destination_funding_source."<BR>";
		
		$sql = "UPDATE 
						INVENTORY_ITEM_BULK_LOCATION 
				SET
						Quantity = Quantity + $qty_change 
				WHERE 
						ItemID = $item_id AND
						LocationID = $targetNewSubLocation AND
						GroupInCharge = $destination_admin_group_id
				";
		//echo "Part 2.1: ".$sql."<BR>";
		$linventory->db_db_query($sql);
		
		
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
				VALUES
						($item_id, '$curr_date',".ITEM_ACTION_CHANGELOCATION.",-$qty_change,$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
				";
		//echo "Part 2.2: ".$sql."<BR>";
		$result = $linventory->db_db_query($sql);
				
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, FundingSource, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
				VALUES
						($item_id, '$curr_date',".ITEM_ACTION_CHANGELOCATION.",$qty_change,$destination_funding_source,$targetNewSubLocation,$targetOldGroup,$UserID,NOW(),NOW())
				";
		//echo "Part 2.3: ".$sql."<BR>";
		$result = $linventory->db_db_query($sql);
		
		### Update Normal, Damage, Repair Qty in the original location ###
		$sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup AND Action IN (".ITEM_ACTION_UPDATESTATUS.",".ITEM_ACTION_PURCHASE.") ORDER BY DateInput DESC LIMIT 0,1";
		//echo "1.1 :".$sql."<BR>";
		$arr_tmp_status = $linventory->returnArray($sql,3);
		if(sizeof($arr_tmp_status)>0)
		{
			list($normal_qty, $damage_qty, $repair_qty) = $arr_tmp_status[0];
			if($normal_qty != "")
				$normal_qty = $normal_qty - $qty_change;
			$sql = "INSERT INTO 
							INVENTORY_ITEM_BULK_LOG	
							(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
					VALUES
							($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$normal_qty','$damage_qty','$repair_qty',$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
					";
			//echo "1.1.1 :".$sql."<BR>";
			$linventory->db_db_query($sql);
		}
		else
		{
			$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup";
			//echo "1.2 :".$sql."<BR>";
			$arr_tmp_result = $linventory->returnArray($sql);
			
			if(sizeof($arr_tmp_result)>0)
			{
				list($normal_qty) = $arr_tmp_result[0];
				$normal_qty = $normal_qty - $qty_change;
			}
			
			$sql = "INSERT INTO 
							INVENTORY_ITEM_BULK_LOG
							(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
					VALUES
							($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$normal_qty','','',$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
					";
			//echo "1.2.1 :".$sql."<BR>";
			$linventory->db_db_query($sql);
		}
		
		### Update Normal Qty in the new location ###
		$sql = "INSERT INTO
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyNormal, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
				VALUES
						($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$qty_change',$targetNewSubLocation,$targetOldGroup,$UserID,NOW(),NOW())
				";
		//echo "1.3 :".$sql."<BR>";
		$linventory->db_db_query($sql);
	}
	else
	{
		# get the funding source from original location #
		$sql = "SELECT FundingSource FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup AND Action = ".ITEM_ACTION_PURCHASE;
		$arr_exist_funding_source = $linventory->returnVector($sql);
		if(sizeof($arr_exist_funding_source)>0)
		{
			$exist_funding_source_id = $arr_exist_funding_source[0];
		}
		else
		{
			$sql = "SELECT FundingSourceID FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup";
			$arr_current_funding_source = $linventory->returnVector($sql);
			$exist_funding_source_id = $arr_current_funding_source[0];
		}
		
		//echo "Original Funding Source: ".$exist_funding_source_id."<BR>";
		
		$sql = "INSERT INTO
						INVENTORY_ITEM_BULK_LOCATION
						(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
				VALUES
						($item_id, $targetOldGroup, $targetNewSubLocation, $exist_funding_source_id, $qty_change)
				";
		//echo "Part 3.1: ".$sql."<BR>";
		$linventory->db_db_query($sql);
		
		## Create change location log (from original location) ##
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
				VALUES
						($item_id, '$curr_date',".ITEM_ACTION_CHANGELOCATION.",-$qty_change,$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
				";
		//echo "Part 3.1: ".$sql."<BR>";
		$result = $linventory->db_db_query($sql);
		
		## Create change location log (to new location) ##
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, FundingSource, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
				VALUES
						($item_id, '$curr_date',".ITEM_ACTION_CHANGELOCATION.",$qty_change,$exist_funding_source_id,$targetNewSubLocation,$targetOldGroup,$UserID,NOW(),NOW())
				";
		//echo "Part 3.2: ".$sql."<BR>";
		$result = $linventory->db_db_query($sql);
		
		if($result){
			### Update Normal, Damage, Repair Qty in the original location ###
			$sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup AND Action IN (".ITEM_ACTION_UPDATESTATUS.",".ITEM_ACTION_PURCHASE.") ORDER BY DateInput DESC LIMIT 0,1";
			//echo "2.1 :".$sql."<BR>";
			$arr_tmp_status = $linventory->returnArray($sql,3);
			
			if(sizeof($arr_tmp_status)>0)
			{
				list($normal_qty, $damage_qty, $repair_qty) = $arr_tmp_status[0];
				if($normal_qty != ""){
					if($qty_change > $normal_qty){
						if($damage_qty != 0){
							$damage_qty = $damage_qty-($qty_change-$normal_qty);
							$repair_qty = $repair_qty;
							$normal_qty = 0;
						}
						if($damage_qty < 0){
							$repair_qty = $repair_qty-(abs($damage_qty));
							$damage_qty = 0;
						}
					}else{
						$normal_qty = $normal_qty - $qty_change;
					}
				}
				
				$sql = "INSERT INTO 
								INVENTORY_ITEM_BULK_LOG	
								(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
						VALUES
								($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$normal_qty','$damage_qty','$repair_qty',$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
						";
				//echo "2.1.1 :".$sql."<BR>";
				$result2 = $linventory->db_db_query($sql);
				
				if($result2){
					### Update Normal Qty in the new location ###
					$sql = "INSERT INTO
									INVENTORY_ITEM_BULK_LOG
									(ItemID, RecordDate, Action, QtyNormal, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
							VALUES
									($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$qty_change',$targetNewSubLocation,$targetOldGroup,$UserID,NOW(),NOW())
							";
					//echo "2.3 :".$sql."<BR>";
					$linventory->db_db_query($sql);
				}
			}
			else
			{
				$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup";
				//echo "2.2 :".$sql."<BR>";
				$arr_tmp_result = $linventory->returnArray($sql);
				
				if(sizeof($arr_tmp_result)>0)
				{
					list($normal_qty) = $arr_tmp_result[0];
					$normal_qty = $normal_qty - $qty_change;
				}
				
				$sql = "INSERT INTO 
								INVENTORY_ITEM_BULK_LOG
								(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
						VALUES
								($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$normal_qty','','',$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
						";
				//echo "2.2.1 :".$sql."<BR>";
				$result2 = $linventory->db_db_query($sql);
				
				if($result2){
					### Update Normal Qty in the new location ###
					$sql = "INSERT INTO
									INVENTORY_ITEM_BULK_LOG
									(ItemID, RecordDate, Action, QtyNormal, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
							VALUES
									($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$qty_change',$targetNewSubLocation,$targetOldGroup,$UserID,NOW(),NOW())
							";
					//echo "2.3 :".$sql."<BR>";
					$linventory->db_db_query($sql);
				}
			}
		}
	}
		
	### Check the original location quantity is 0 or not ###
	$sql = "SELECT 
					Quantity 
			FROM 
					INVENTORY_ITEM_BULK_LOCATION 
			WHERE 
					ItemID = $item_id AND
					LocationID = $targetOldLocation AND
					GroupInCharge = $targetOldGroup
			";
	$arr_tmp_exist_qty = $linventory->returnVector($sql);
	if(sizeof($arr_tmp_exist_qty)>0)
	{
		$exist_qty = $arr_tmp_exist_qty[0];
	}
	
	### quantity = 0, delete record ###
	if($exist_qty == 0)
	{
		$sql = "DELETE FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup";
		//echo $sql."<BR>";
		$linventory->db_db_query($sql);
		
		//$sql = "UPDATE INVENTORY_ITEM_BULK_LOG SET FundingSource = '' WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup AND Action != ".ITEM_ACTION_PURCHASE;
		//echo $sql."<BR>";
		//$linventory->db_db_query($sql);
	}
	
}
if($result)
{
	header("location: items_full_list.php?msg=2");
}
else
{
	header("location: items_full_list.php?msg=14");
}
intranet_closedb();
?>