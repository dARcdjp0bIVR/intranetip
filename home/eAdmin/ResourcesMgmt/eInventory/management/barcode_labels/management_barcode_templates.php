<?php
// using : 

### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
	//$order = 1;		# default in ascending order
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable{$LAYOUT_SKIN}.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


intranet_auth();
intranet_opendb();

$min_barcode_width = DEFAULT_MINIMUM_BARCODE_WIDTH;
$max_barcode_width = DEFAULT_MAXIMUN_BARCODE_WIDTH;

$CurrentPage	= "Management_BarcodeLabels";
$linterface 	= new interface_html();
$linventory		= new libinventory();
############################################################################################################
///*
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == "") ? 1 : $order;
$field = ($field == "") ? 0 : $field;


$metric_sql_mapping = 'CASE `metric` ';
foreach ($Lang['eInventory']['label']['unit'] as $unit => $display_unit){
	$metric_sql_mapping .= "WHEN '{$unit}' THEN '$display_unit' ";
}
$metric_sql_mapping .= 'END';


$sql = "SELECT 
			`name`, `paper_size_width`,`paper_size_height`,`NX`, `NY`, {$metric_sql_mapping} , 
            CONCAT('<input type=\'checkbox\' name=\'Code[]\' id=\'Code[]\' value=\'', id ,'\' />')
		FROM 
			INVENTORY_LABEL_FORMAT
        ";

$li = new libdbtable2007($field, $order, $pageNo);

global $intranet_db;
$li->db = $intranet_db;

$li->field_array = array("name", "paper_size_width","paper_size_height", "NX", "NY", "metric");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='20' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='25%' >".$li->column($pos++, $Lang['eInventory']['label']['name'])."</td>\n";
$li->column_list .= "<td width='15%' >".$li->column($pos++, $Lang['eInventory']['label']['paper-size-width'])."</td>\n";
$li->column_list .= "<td width='15%' >".$li->column($pos++, $Lang['eInventory']['label']['paper-size-height'])."</td>\n";
$li->column_list .= "<td width='15%' >".$li->column($pos++, $Lang['eInventory']['label']['NX'])."</td>\n";
$li->column_list .= "<td width='15%' >".$li->column($pos++, $Lang['eInventory']['label']['NY'])."</td>\n";
$li->column_list .= "<td width='10%' >".$li->column($pos++, $Lang['eInventory']['label']['metric'])."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("Code[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'barcode_templates_new.php')",$button_new,"","","",0);

//*/
############################################################################################################

$TAGS_OBJ[] = array($i_InventorySystem_Report_ItemDetails, "item_details.php", 1);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['LocationBarcode'], "location_barcode.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['ResourceMgmtGroupBarcode'], "admin_group_barcode.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['FundingSourceBarcode'], "funding_source_barcode.php", 0);

$PAGE_NAVIGATION[] = array($i_InventorySystem['CustomBarcodeFormats'] , "item_details.php?format=auto");
$PAGE_NAVIGATION[] = array($i_InventorySystem['ManagementBarcodeTemplates'] , "");

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.autocomplete.css" type="text/css" />

<script language="javascript">

</script>
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>
<form name="form1" method="post" action="">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'Code[]','barcode_templates_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:checkRemove(document.form1,'Code[]','barcode_templates_delete.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>