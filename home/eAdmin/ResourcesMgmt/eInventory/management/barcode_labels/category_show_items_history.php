<?php
### set cookies
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_category_item_browsing_view_history_record_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_category_item_browsing_view_history_record_page_number", $pageNo, 0, "", "", 0);
	$ck_category_item_browsing_view_history_record_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_category_item_browsing_view_history_record_page_number!="")
{
	$pageNo = $ck_category_item_browsing_view_history_record_page_number;
}

if ($ck_category_item_browsing_view_history_record_page_order!=$order && $order!="")
{
	setcookie("ck_category_item_browsing_view_history_record_page_order", $order, 0, "", "", 0);
	$ck_category_item_browsing_view_history_record_page_order = $order;
} else if (!isset($order) && $ck_category_item_browsing_view_history_record_page_order!="")
{
	$order = $ck_category_item_browsing_view_history_record_page_order;
}

if ($ck_category_item_browsing_view_history_record_page_field!=$field && $field!="")
{
	setcookie("ck_category_item_browsing_view_history_record_page_field", $field, 0, "", "", 0);
	$ck_category_item_browsing_view_history_record_page_field = $field;
} else if (!isset($field) && $ck_category_item_browsing_view_history_record_page_field!="")
{
	$field = $ck_category_item_browsing_view_history_record_page_field;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php", 0);

# get the name of infobar #
$namefield1 = $linventory->getInventoryItemNameByLang("a.");
$namefield2 = $linventory->getInventoryItemNameByLang("b.");
$namefield3 = $linventory->getInventoryItemNameByLang("c.");
$sql = "SELECT 
				a.ItemCode, 
				$namefield1				
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
		WHERE
				a.ItemID = $item_id";
$result = $linventory->returnArray($sql,2);

$temp[] = array("<a href=\"items_full_list.php\">$i_InventorySystem_BackToFullList</a>");
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$titlebar .= "<tr><td align=\"left\">";
$titlebar .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_section.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" />";
$titlebar .= "<span class=\"sectiontitle\">".$result[0][0]." - ".$result[0][1]."</span>";
$titlebar .= "</td>";
$titlebar .= "<td align=\"right\">$toolbar";
$titlebar .= "</td></tr>";

# Gen the table tool (edit & remove) #
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'cat2_id[]','category_level2_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'cat2_id[]','category_level2_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql,1);

if(sizeof($result)>0)
{
	list($item_type) = $result[0];
}

if($item_type == 1)
{
	$namefield = getNameFieldByLang2("c.");
	
	$sql = "SELECT 
					b.DateInput,
					IF(b.Action = ".ITEM_ACTION_PURCHASE.",'$i_InventorySystem_Action_Purchase',
						IF(b.Action = ".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION.",'$i_InventorySystem_Action_Stocktake_Original_Location',
							IF(b.Action = ".ITEM_ACTION_STOCKTAKE_OTHER_LOCATION.",'$i_InventorySystem_Action_Stocktake_Other_Location',
								IF(b.Action = ".ITEM_ACTION_STOCKTAKE_NOT_FOUND.",'$i_InventorySystem_Action_Stocktake_Not_Found',
									IF(b.Action = ".ITEM_ACTION_WRITEOFF.",'$i_InventorySystem_Action_WriteOff',
										IF(b.Action = ".ITEM_ACTION_UPDATESTATUS.",'$i_InventorySystem_Action_UpdateStatus',
											IF(b.Action = ".ITEM_ACTION_CHANGELOCATION.",'$i_InventorySystem_Action_ChangeLocation',
												IF(b.Action = ".ITEM_ACTION_CHANGELOCATIONQTY.",'$i_InventorySystem_Action_ChangeLocationQty',
													IF(b.Action = ".ITEM_ACTION_MOVEBACKTOORGINIAL.",'$i_InventorySystem_Action_MoveBackToOriginalLocation',
														IF(b.Action = ".ITEM_ACTION_IGNORE_SURPLUS.", '$i_InventorySystem_Action_Surplus_Ignore', 
															IF(b.Action = ".ITEM_ACTION_EDIT_ITEM.", '$i_InventorySystem_Action_Edit_Item' ,' - '))))))))))),
					IF(b.PastStatus = 0,' - ',IF(b.PastStatus = 1,'$i_InventorySystem_ItemStatus_Normal',IF(b.PastStatus = 2,'$i_InventorySystem_ItemStatus_Damaged',IF(b.PastStatus = 3,'$i_InventorySystem_ItemStatus_Repair',' - ')))),
					IF(b.NewStatus = 0,' - ',IF(b.NewStatus = 1,'$i_InventorySystem_ItemStatus_Normal',IF(b.NewStatus = 2,'$i_InventorySystem_ItemStatus_Damaged',IF(b.NewStatus = 3,'$i_InventorySystem_ItemStatus_Repair',' - ')))),
					IF(b.Remark = '',' - ',IFNULL(b.Remark,' - ')),
					IFNULL($namefield,'".$Lang['eInventory']['FieldTitle']['DeletedStaff']."'),
					IF(a.ItemType=1,CONCAT('single'),CONCAT('bulk'))
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_STATUS_LOG AS b ON (a.ItemID = b.ItemID) LEFT OUTER JOIN
					INTRANET_USER AS c ON (b.PersonInCharge = c.UserID)
			WHERE
					a.ItemID = $item_id";
					
	if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
	$pageSizeChangeEnabled = true;
	
	$field=$field==""?1:$field;
	if (!isset($order)) $order = 0;
	
	# TABLE INFO
	$li = new libdbtable2007($field, $order, $pageNo, true);
	$li->field_array = array("","b.DateInput","b.Action","b.PastStatus","b.NewStatus","b.Remark","c.EnglishName");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+1;
	$li->title = "";
	$li->wrap_array = array();
	//$li->IsColOff = 2;
	$li->IsColOff = "eInventoryList";
	//echo $li->built_sql();
	
	// TABLE COLUMN
	$pos = 1;
	$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>#</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Record_Date)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Action)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Past_Status)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_New_Status)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Remark)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_PIC)."</td>\n";
	//$li->column_list .= "<td width='1'>".$li->check("ItemID[]")."</td>\n";
}

if($item_type == 2)
{
	$namefield = getNameFieldByLang2("d.");
	
	$sql = "SELECT
					b.DateInput,
					IF(b.Action = ".ITEM_ACTION_PURCHASE.",'$i_InventorySystem_Action_Purchase',
						IF(b.Action = ".ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION.",'$i_InventorySystem_Action_Stocktake_Original_Location',
							IF(b.Action = ".ITEM_ACTION_STOCKTAKE_OTHER_LOCATION.",'$i_InventorySystem_Action_Stocktake_Other_Location',
								IF(b.Action = ".ITEM_ACTION_STOCKTAKE_NOT_FOUND.",'$i_InventorySystem_Action_Stocktake_Not_Found',
									IF(b.Action = ".ITEM_ACTION_WRITEOFF.",'$i_InventorySystem_Action_WriteOff',
										IF(b.Action = ".ITEM_ACTION_UPDATESTATUS.",'$i_InventorySystem_Action_UpdateStatus',
											IF(b.Action = ".ITEM_ACTION_CHANGELOCATION.",'$i_InventorySystem_Action_ChangeLocation',
												IF(b.Action = ".ITEM_ACTION_CHANGELOCATIONQTY.",'$i_InventorySystem_Action_ChangeLocationQty',
													IF(b.Action = ".ITEM_ACTION_MOVEBACKTOORGINIAL.",'$i_InventorySystem_Action_MoveBackToOriginalLocation',
														IF(b.Action = ".ITEM_ACTION_IGNORE_SURPLUS.", '$i_InventorySystem_Action_Surplus_Ignore', 
															IF(b.Action = ".ITEM_ACTION_EDIT_ITEM.", '$i_InventorySystem_Action_Edit_Item' ,' - '))))))))))),
					IF(b.Action = ".ITEM_ACTION_CHANGELOCATION.", 
						IF(b.QtyChange > 0, CONCAT('".$i_InventorySystem_To."&nbsp;&nbsp;',".$linventory->getInventoryNameByLang("e.").",' > ',".$linventory->getInventoryNameByLang("c.")."),CONCAT('".$i_InventorySystem_From."&nbsp;&nbsp;',".$linventory->getInventoryNameByLang("e.").",' > ',".$linventory->getInventoryNameByLang("c.").")),
							CONCAT(".$linventory->getInventoryNameByLang("LocBuilding.").",' > ',".$linventory->getInventoryNameByLang("e.").",' > ',".$linventory->getInventoryNameByLang("c.").")),
					IF(b.FundingSource != '', f.NameChi, ' - '),
					IF(b.Action = ".ITEM_ACTION_WRITEOFF.",IF(b.QtyChange != '', CONCAT('-',b.QtyChange), ' - '),IF(b.QtyChange != '', b.QtyChange, ' - ')),
					IF(b.QtyNormal IS NULL, ' - ', b.QtyNormal),
					IF(b.QtyDamage IS NULL, ' - ', b.QtyDamage),
					IF(b.QtyRepair IS NULL, ' - ', b.QtyRepair),
					IF(b.StockCheckQty IS NULL, ' - ', b.StockCheckQty),
					IF(b.Remark != '', IFNULL(b.Remark,' - '), ' - '),
					IFNULL($namefield,'".$Lang['eInventory']['FieldTitle']['DeletedStaff']."'),
					IF(a.ItemType=1,CONCAT('single'),CONCAT('bulk'))
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_BULK_LOG AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS e ON (c.LocationLevelID = e.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (e.BuildingID = LocBuilding.BuildingID) LEFT OUTER JOIN
					INTRANET_USER AS d ON (b.PersonInCharge = d.UserID) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS f ON (b.FundingSource = f.FundingSourceID)
			WHERE
					a.ItemID = $item_id";
	if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
	$pageSizeChangeEnabled = true;
	$field=$field==""?1:$field;
	if (!isset($order)) $order = 0;
	
	# TABLE INFO
	$li = new libdbtable2007($field, $order, $pageNo, true);
	$li->field_array = array("","b.RecordID DESC, b.DateInput","b.Action","e.NameEng,c.NameEng","b.QtyChange","b.QtyNormal","b.QtyDamage","b.QtyRepair","b.StockCheckQty","b.Remark","d.EnglishName");
	$li->sql = $sql;
	$li->no_col = sizeof($li->field_array)+2;
	$li->title = "";
	$li->wrap_array = array();
	$li->IsColOff = "eInventoryList";
	//echo $li->built_sql();

	// TABLE COLUMN
	$pos = 1;
	$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>#</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Record_Date)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Action)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Location)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Funding)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Qty)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_ItemStatus_Normal)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_ItemStatus_Damaged)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_ItemStatus_Repair)."</td>\n";
	//$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_New_Status)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Quantity_StockChecked)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_Remark)."</td>\n";
	$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $i_InventorySystem_Item_PIC)."</td>\n";
	//$li->column_list .= "<td width='1'>".$li->check("ItemID[]")."</td>\n";
}

?>

<br>

<form name="form1" enctype="multipart/form-data" action="" method="POST">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<?=$infobar1?>
</table>
<br><br>
<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
<?=$titlebar?>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<? if($item_type == 1) { ?>
<tr><td>
	<table border=0 align="center" width="96%">
		<tr class="single"><td height="20px" valign="top" nowrap="nowrap" class="tabletext">
			<a href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
			<a href="category_show_items_history.php?type=5&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
		</td></tr>
	</table>
</td></tr>
<? } ?>
<? if($item_type == 2) { ?>
<tr><td>
	<table border=0 align="center" width="96%">
		<tr class="bulk"><td height="20px" valign="top" nowrap="nowrap" class="tabletext">
			<a href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
			<a href="category_show_items_location.php?type=2&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==2) echo"<B>"; else echo " "; ?><?=$i_InventorySystem['Location']." ".$i_InventorySystem_Search_And2." ".$i_InventorySystem['Caretaker']?><? IF($type==2) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
			<!--<a href="category_show_items_group.php?type=3&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==3) echo"<B>"; else echo " "; ?><?=$i_InventorySystem['Caretaker']?><? IF($type==3) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;-->
			<a href="category_show_items_invoice.php?type=4&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==4) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Invoice?><? IF($type==4) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
			<a href="category_show_items_history.php?type=5&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
		</td></tr>
	</table>
</td></tr>
<? } ?>
</table>
<br>
<?echo $li->display("94%"); ?>

<br>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
<input type="hidden" name="order" value="<?php echo $li->order; ?>">
<input type="hidden" name="field" value="<?php echo $li->field; ?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>