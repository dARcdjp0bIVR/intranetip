<?php
// using:

/**
 * **************************** Change Log **************************************************
 * 
 * 2018-05-18 (Cameron)
 * - use function getNumberOfOrderByFieldInLabel to get $option_no [case #P138005]
 * 
 * 2018-02-22 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log ************************************************
 */
if (! function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {

    function magicQuotes_awStripslashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
    $gpc = array(
        &$_GET,
        &$_POST,
        &$_COOKIE,
        &$_REQUEST
    );
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

$PATH_WRT_ROOT = "../../../../../../";
// ###### Trim all request #######
function recursive_trim(&$array)
{
    foreach ($array as &$item) {
        if (is_array($item)) {
            recursive_trim($item);
        } else {
            if (is_string($item))
                $item = trim($item);
        }
    }
}
recursive_trim($_REQUEST);
// ###### END OF Trim all request #######
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/pdump/pdump.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if (sizeof($_POST) == 0) {
    header("Location: management_barcode_templates.php");
}

$dataAry['name'] = intranet_htmlspecialchars($name);
$dataAry['paper_size_width'] = intranet_htmlspecialchars($paper_size_width);
$dataAry['paper_size_height'] = intranet_htmlspecialchars($paper_size_height);
$dataAry['metric'] = intranet_htmlspecialchars($metric);
$dataAry['lMargin'] = intranet_htmlspecialchars($lMargin);
$dataAry['tMargin'] = intranet_htmlspecialchars($tMargin);
$dataAry['NX'] = intranet_htmlspecialchars($NX);
$dataAry['NY'] = intranet_htmlspecialchars($NY);
$dataAry['SpaceX'] = intranet_htmlspecialchars($SpaceX);
$dataAry['SpaceY'] = intranet_htmlspecialchars($SpaceY);
$dataAry['width'] = intranet_htmlspecialchars($width);
$dataAry['height'] = intranet_htmlspecialchars($height);
$dataAry['font_size'] = intranet_htmlspecialchars($font_size);
$dataAry['lineHeight'] = intranet_htmlspecialchars($lineHeight);
$dataAry['printing_margin_h'] = intranet_htmlspecialchars($printing_margin_h);
$dataAry['printing_margin_v'] = intranet_htmlspecialchars($printing_margin_v);
$dataAry['max_barcode_width'] = intranet_htmlspecialchars($max_barcode_width);
$dataAry['max_barcode_height'] = intranet_htmlspecialchars($max_barcode_height);
//$option_no = 19;
$option_no = $linventory->getNumberOfOrderByFieldInLabel();
for ($i = 1; $i <= $option_no; $i ++)
    $dataAry['info_order_' . $i] = intranet_htmlspecialchars(${"info_order_" . $i});
    /*
 * $dataAry['info_order_2'] = intranet_htmlspecialchars($info_order_2);
 * $dataAry['info_order_3'] = intranet_htmlspecialchars($info_order_3);
 * $dataAry['info_order_4'] = intranet_htmlspecialchars($info_order_4);
 * $dataAry['info_order_5'] = intranet_htmlspecialchars($info_order_5);
 * $dataAry['info_order_6'] = intranet_htmlspecialchars($info_order_6);
 * $dataAry['info_order_7'] = intranet_htmlspecialchars($info_order_7);
 * $dataAry['info_order_8'] = intranet_htmlspecialchars($info_order_8);
 * $dataAry['info_order_9'] = intranet_htmlspecialchars($info_order_9);
 * $dataAry['info_order_10'] = intranet_htmlspecialchars($info_order_10);
 * $dataAry['info_order_11'] = intranet_htmlspecialchars($info_order_11);
 * $dataAry['info_order_12'] = intranet_htmlspecialchars($info_order_12);
 * $dataAry['info_order_13'] = intranet_htmlspecialchars($info_order_13);
 * $dataAry['info_order_14'] = intranet_htmlspecialchars($info_order_14);
 * $dataAry['info_order_15'] = intranet_htmlspecialchars($info_order_15);
 * $dataAry['info_order_16'] = intranet_htmlspecialchars($info_order_16);
 * $dataAry['info_order_17'] = intranet_htmlspecialchars($info_order_17);
 * $dataAry['info_order_18'] = intranet_htmlspecialchars($info_order_18);
 * $dataAry['info_order_19'] = intranet_htmlspecialchars($info_order_19);
 */

$result = $linventory->ADD_LABEL_FORMAT($dataAry);

intranet_closedb();

header("Location: management_barcode_templates.php?xmsg=add");

?>
