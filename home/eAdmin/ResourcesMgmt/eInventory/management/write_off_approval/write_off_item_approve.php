<?php
// using:

// ###############################
//
// Date : 2020-06-03 (Cameron)
// fix: bulk item write_off approval should consider ItemID + LocationID + AdminGroupID + FundingSourceID instead of just ItemID [case #K186276]
//      can approve multiple records for the same ItemID with different LocationID
//
// Date : 2019-04-30 (Henry)
// security issue fix
//
// Date : 2018-02-22 (Henry)
// add access right checking [Case#E135442]
//
// Date : 2017-08-29 (Henry)
// fixed: double submission when double press enter key [Case#Z122974]
//
// Date : 2016-02-04 (Henry)
// fixed: php 5.4 issue move "if(!isset($RecordID)){}" after includes file
//
// Date : 2015-11-25 (Henry)
// fixed: item status incorrect after write-off [Case#W89255]
//
// Date : 2015-07-06 (Henry)
// fixed: wrongly write-off the items with other funding [Case#A80553]
//
// Date : 2013-04-18 (YatWoon)
// update item status qty after write-off [Case#2013-0411-1603-10073]
//
// Date : 2012-10-25 (YatWoon)
// add eBooking integration, send email to eBooking admin
//
// Date: 2012-05-25 YatWoon
// cater "group and funding" for bluk item
//
// ###############################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

if (! isset($RecordID)) {
    header("location: write_off_item.php");
    exit();
}

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$arr_success_rec = Array();
$curr_date = date("Y-m-d");

if ($RecordID != "") {
	$RecordID = IntegerSafe($RecordID);
    $record_list = implode(",", $RecordID);
    
    $sql = "SELECT 
					b.ItemType,
					a.RecordID,
					a.ItemID, 
					a.LocationID,
					a.AdminGroupID,
					a.WriteOffQty,
					a.FundingSourceID,
					a.ByStocktake, 
					a.WriteOffReason
			FROM 
					INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
					INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID)
			WHERE 
					a.RecordID IN ($record_list) AND a.RecordStatus <> 1
			";
    $arr_target_item = $linventory->returnArray($sql);
    
    if (sizeof($arr_target_item) > 0) {
        for ($i = 0; $i < sizeof($arr_target_item); $i ++) {
            list ($target_type, $target_record, $target_item_id, $target_location_id, $target_group_id, $target_qty, $target_funding_id, $byStocktake, $writeoff_reason) = $arr_target_item[$i];
            if ($target_type == 1) {
                $arr_write_off_single_id[] = $target_item_id;
                $arr_write_off_single[$target_item_id]['RecordID'] = $target_record;
                $arr_write_off_single[$target_item_id]['LocationID'] = $target_location_id;
                $arr_write_off_single[$target_item_id]['GroupID'] = $target_group_id;
                $arr_write_off_single[$target_item_id]['Qty'] = $target_qty;
                $arr_write_off_single[$target_item_id]['writeoff_reason'] = $writeoff_reason;
            }
            if ($target_type == 2) {
//                 $arr_write_off_bulk_id[] = $target_item_id;
//                 $arr_write_off_bulk[$target_item_id]['RecordID'] = $target_record;
//                 $arr_write_off_bulk[$target_item_id]['LocationID'] = $target_location_id;
//                 $arr_write_off_bulk[$target_item_id]['GroupID'] = $target_group_id;
//                 $arr_write_off_bulk[$target_item_id]['Qty'] = $target_qty;
//                 $arr_write_off_bulk[$target_item_id]['FundingID'] = $target_funding_id;
//                 $arr_write_off_bulk[$target_item_id]['byStocktake'] = $byStocktake;
//                 $arr_write_off_bulk[$target_item_id]['writeoff_reason'] = $writeoff_reason;

                $thisBulkItem = array();
                $thisBulkItem['ItemID'] = $target_item_id;
                $thisBulkItem['RecordID'] = $target_record;
                $thisBulkItem['LocationID'] = $target_location_id;
                $thisBulkItem['GroupID'] = $target_group_id;
                $thisBulkItem['FundingID'] = $target_funding_id;
                $thisBulkItem['Qty'] = $target_qty;
                $thisBulkItem['byStocktake'] = $byStocktake;
                $thisBulkItem['writeoff_reason'] = $writeoff_reason;
                $arr_write_off_bulk[] = $thisBulkItem;
            }
        }
    }
    
    if (sizeof($arr_write_off_single_id) > 0) {
        foreach ($arr_write_off_single_id as $key => $single_item_id) {
            $location_id = $arr_write_off_single[$single_item_id]['LocationID'];
            $group_id = $arr_write_off_single[$single_item_id]['GroupID'];
            $qty = $arr_write_off_single[$single_item_id]['Qty'];
            $rec_id = $arr_write_off_single[$single_item_id]['RecordID'];
            
            // $sql = "SELECT Action FROM INVENTORY_ITEM_SINGLE_STATUS_LOG WHERE ItemID = $single_item_id AND Action = 3";
            // $arr_tmp_result = $linventory->returnVector($sql);
            
            // if(sizeof($arr_tmp_result)==0)
            // {
            $sql = "UPDATE INVENTORY_ITEM SET RecordStatus = 0 WHERE ItemID = '".$single_item_id."'";
            $linventory->db_db_query($sql);
            
            $sql = "INSERT INTO 
							INVENTORY_ITEM_SINGLE_STATUS_LOG
							(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
							($single_item_id, '$curr_date', " . ITEM_ACTION_WRITEOFF . ", $UserID, '', 0, 0, NOW(), NOW())
						";
            $linventory->db_db_query($sql);
            
            $arr_success_rec[] = $rec_id;
            // }
            
            // eBooking integration - send email notification
            $linventory->sendEmailNotification_eBooking(1, $single_item_id, $arr_write_off_single[$single_item_id]['writeoff_reason'], 5);
            
            // Resources Booking #
            $sql = "SELECT ResourceID, PendingMode FROM INTRANET_RESOURCE_WRITE_OFF WHERE ItemID = '".$single_item_id."'";
            $arr_resoruce_id = $linventory->returnArray($sql, 2);
            
            if (sizeof($arr_resoruce_id) > 0) {
                list ($resource_id, $pending_mode) = $arr_resoruce_id[0];
                
                // # Archival
                // $conds = "a.ResourceID = $resource_id AND a.BookingDate < CURDATE()";
                // $sql = "INSERT INTO INTRANET_BOOKING_ARCHIVE
                // (BookingDate,TimeSlot,Username,Category,Item,ItemCode,FinalStatus,TimeApplied,LastAction)
                // SELECT a.BookingDate, CONCAT(c.Title,' ',c.TimeRange),
                // CONCAT(b.EnglishName, IF (b.ClassNumber IS NULL OR b.ClassNumber = '', '', CONCAT(' (',b.ClassName,'-',b.ClassNumber,')') ) ),
                // d.ResourceCategory, d.Title, d.ResourceCode,
                // a.RecordStatus, a.TimeApplied, a.LastAction
                // FROM INTRANET_BOOKING_RECORD as a, INTRANET_USER as b, INTRANET_SLOT as c, INTRANET_RESOURCE as d
                // WHERE a.UserID = b.UserID AND c.BatchID = d.TimeSlotBatchID AND a.ResourceID = d.ResourceID AND a.TimeSlot = c.SlotSeq
                // AND $conds";
                // $linventory->db_db_query($sql);
                //
                // # Remove records
                // $cond2 = "ResourceID = $resource_id";
                // $sql = "DELETE FROM INTRANET_BOOKING_RECORD WHERE $cond2";
                // $linventory->db_db_query($sql);
                include_once ($PATH_WRT_ROOT . "includes/librb.php");
                $librb = new librb();
                $librb->Archive_Booking_Record($resource_id);
                
                if ($pending_mode == 1) {
                    $sql = "DELETE FROM INTRANET_GROUPRESOURCE WHERE ResourceID = '".$resource_id."'";
                    $linventory->db_db_query($sql);
                    
                    $sql = "DELETE FROM INTRANET_RESOURCE WHERE ResourceID = '".$resource_id."'";
                    $linventory->db_db_query($sql);
                } else 
                    if ($pending_mode == 2) {
                        $sql = "UPDATE INTRANET_RESOURCE SET RecordStatus = 0 WHERE ResourceID = '".$resource_id."'";
                        $linventory->db_db_query($sql);
                    } else {}
                $sql = "DELETE FROM INTRANET_RESOURCE_INVENTORY_ITEM_LIST WHERE ResourceID = '".$resource_id."' AND ItemID = '".$single_item_id."'";
                $linventory->db_db_query($sql);
                
                $sql = "DELETE FROM INTRANET_RESOURCE_WRITE_OFF WHERE ItemID = '".$single_item_id."' AND ResourceID = '".$resource_id."'";
                $linventory->db_db_query($sql);
            }
            // end of Resources Booking #
        }
    }
    
    if (sizeof($arr_write_off_bulk) > 0) {
        foreach ((array)$arr_write_off_bulk as $key => $thisBulkItem) {
            $bulk_item_id = $thisBulkItem['ItemID'];
            $location_id = $thisBulkItem['LocationID'];
            $group_id = $thisBulkItem['GroupID'];
            $funding_id = $thisBulkItem['FundingID'];
            $qty = $thisBulkItem['Qty'];
            $rec_id = $thisBulkItem['RecordID'];
            $byStocktake = $thisBulkItem['byStocktake'];
            
            $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$bulk_item_id."' AND GroupInCharge = '".$group_id."' AND LocationID = '".$location_id."' and FundingSourceID = '".$funding_id."'";
            $arr_tmp_qty1 = $linventory->returnVector($sql);
            if ($qty <= $arr_tmp_qty1[0]) {
                $tmp_check_1 = true;
            } else {
                $tmp_check_1 = false;
            }
            
            $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_EXT WHERE ItemID = '".$bulk_item_id."'";
            $arr_tmp_qty2 = $linventory->returnVector($sql);
            if ($qty <= $arr_tmp_qty2[0]) {
                $tmp_check_2 = true;
            } else {
                $tmp_check_2 = false;
            }
            
            if ($tmp_check_1 && $tmp_check_2) {
                $sql = "UPDATE INVENTORY_ITEM_BULK_LOCATION SET Quantity = Quantity - $qty WHERE ItemID = '".$bulk_item_id."' AND GroupInCharge = '".$group_id."' AND LocationID = '".$location_id."' and FundingSourceID = '".$funding_id."'";
                $linventory->db_db_query($sql);
                
                $sql = "UPDATE INVENTORY_ITEM_BULK_EXT SET Quantity = Quantity - $qty WHERE ItemID = '".$bulk_item_id."'";
                $linventory->db_db_query($sql);
                
                $sql = "INSERT INTO 
							INVENTORY_ITEM_BULK_LOG 
							(ItemID, RecordDate, Action, QtyChange, PersonInCharge, LocationID, GroupInCharge, FundingSource, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
							($bulk_item_id, '$curr_date', " . ITEM_ACTION_WRITEOFF . ", $qty, $UserID, $location_id, $group_id, $funding_id, 0, 0, NOW(), NOW())";
                $linventory->db_db_query($sql);
                
                // if the write-off request is inserted by stocktake variance, then need to add remark if the variance handling not finish
                if ($byStocktake) {
                    $append_remark = $Lang['eInventory']['ItemAlreadyWrittenOff'];
                    $append_remark = str_replace("[WRITE_OFF_QTY]", $qty, $append_remark);
                    $append_remark = str_replace("[WRITE_OFF_DATE]", $curr_date, $append_remark);
                    
                    $sql = "update INVENTORY_VARIANCE_HANDLING_REMINDER set Remark = concat(Remark, '<br>" . $append_remark . "') 
							where ItemID = '".$bulk_item_id."' and NewLocationID = '".$location_id."' and GroupInCharge = '".$group_id."' and FundingSourceID = '".$funding_id."' and RecordStatus=0";
                    $linventory->db_db_query($sql) or die(mysql_error());
                }
                
                // udpate item status qty
                // retrieve latest item status
                $sql = "select QtyNormal, QtyDamage, QtyRepair, Remark from INVENTORY_ITEM_BULK_LOG where (Action=" . ITEM_ACTION_UPDATESTATUS . " or Action=" . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . " or Action=" . ITEM_ACTION_PURCHASE . " ) and ItemID = '".$bulk_item_id."' and LocationID = '".$location_id."' and FundingSource = '".$funding_id."' and GroupInCharge = '".$group_id."' order by RecordID desc limit 0, 1";
                $result = $linventory->returnArray($sql);
                if (! empty($result)) {
                    $QtyNormal = $result[0]['QtyNormal'];
                    $QtyDamage = $result[0]['QtyDamage'];
                    $QtyRepair = $result[0]['QtyRepair'];
                }
                $QtyNormal = ($QtyDamage + $QtyRepair + $QtyNormal == 0) ? $arr_tmp_qty1[0] : $QtyNormal;
                $qty_ary = array(
                    $QtyDamage,
                    $QtyRepair,
                    $QtyNormal
                );
                $qty_remain = $qty;
                for ($i = 0; $i <= 2; $i ++) {
                    if ($qty_ary[$i] >= $qty_remain) {
                        $qty_ary[$i] -= $qty_remain;
                        break;
                    } else {
                        $qty_remain -= $qty_ary[$i];
                        $qty_ary[$i] = 0;
                    }
                }
                $sql = "INSERT INTO
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, PersonInCharge, Remark,
						LocationID, GroupInCharge, FundingSource, DateInput, DateModified)
				VALUES
						($bulk_item_id, '$curr_date', " . ITEM_ACTION_UPDATESTATUS . ",$qty_ary[2],$qty_ary[0],$qty_ary[1],$UserID,'',
						$location_id,$group_id,$funding_id,NOW(),NOW())
				";
                $linventory->db_db_query($sql);
                // udpate item status qty - end
                
                /*
                 * $total_qty = $arr_tmp_qty2[0];
                 * $ans = $total_qty - $qty;
                 * if($ans == 0)
                 * {
                 * $sql = "UPDATE INVENTORY_ITEM SET RecordStatus = 0 WHERE ItemID = $single_item_id";
                 * $linventory->db_db_query($sql);
                 * }
                 */
                $arr_success_rec[] = $rec_id;
                
                // eBooking integration - send email notification
                $linventory->sendEmailNotification_eBooking(2, $bulk_item_id, $arr_write_off_bulk[$bulk_item_id]['writeoff_reason'], 5, $location_id, $group_id, $funding_id, $qty);
            } else {
                $arr_fail_not_enough_qty[] = $rec_id;
            }
        }
    }
}

if (sizeof($arr_success_rec) > 0) {
    $success_record_list = implode(",", $arr_success_rec);
    
    // $sql = "LOCK TABLES INVENTORY_ITEM_WRITE_OFF_RECORD WRITE";
    // $linventory->db_db_query($sql);
    
    $sql = "UPDATE 
					INVENTORY_ITEM_WRITE_OFF_RECORD
			SET
					ApproveDate = '$curr_date',
					ApprovePerson = $UserID,
					RecordStatus = 1,
					DateModified = NOW()
			WHERE
					RecordID IN ($success_record_list)
			";
    
    $result = $linventory->db_db_query($sql);
    
    // $sql = "UNLOCK TABLES";
    // $linventory->db_db_query($sql) or die(mysql_error());
    
    if ($result) {
        $x = "2";
    } else {
        $x = "14";
    }
} else {
    if (sizeof($arr_fail_not_enough_qty) > 0) {
        $x = "-1";
    } else {
        $x = "14";
    }
}

intranet_closedb();
header("location: write_off_item.php?msg=$x");
?>