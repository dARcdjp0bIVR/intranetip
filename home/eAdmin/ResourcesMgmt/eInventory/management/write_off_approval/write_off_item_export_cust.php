<?php

// modifying : 

// using : if you want to upload it to 149 or client, pls update write_off_item.php & write_off_item_print.php as well!

/**
 * ***********************************************************************
 * modification log
 *
 * 2020-02-07 Tommy
 *  Customization for plkchc #A170032
 *
 * ***********************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linventory = new libinventory();
$lexport = new libexporttext();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

if ($linventory->IS_ADMIN_USER($UserID)) {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
    $arr_admin_group_leader = $linventory->returnVector($sql);
} else {
    $groupLeaderNowAllowWriteOffItem = $linventory->retriveGroupLeaderNotAllowWriteOffItem();
    // if(!$groupLeaderNowAllowWriteOffItem)
    // {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = $UserID AND RecordType = 1";
    // $arr_admin_group_leader = $linventory->returnVector($sql);
    // }
}
$arr_admin_group_leader = $linventory->returnVector($sql);

if (sizeof($arr_admin_group_leader) > 0) {
    $target_group_list = implode(",", $arr_admin_group_leader);
}

// ## Set SQL Condition - Request date ###
if($date_from != '' && $date_to != ''){
    $cond = " AND ";
    
    $cond .= " (a.RequestDate >= '".$date_from."' AND a.RequestDate <= '".$date_to."')";
}

$namefield1 = getNameFieldByLang2("f.");
$namefield2 = getNameFieldByLang2("f2.");
$namefield3 = getNameFieldByLang2("f3.");

$archivename1 = "(select " . getNameFieldByLang2("af.") . " from INTRANET_ARCHIVE_USER as af WHERE a.RequestPerson = af.UserID)";
$archivename2 = "(select " . getNameFieldByLang2("af2.") . "from INTRANET_ARCHIVE_USER as af2 WHERE a.ApprovePerson = af2.UserID)";
$archivename3 = "(select " . getNameFieldByLang2("af3.") . "from INTRANET_ARCHIVE_USER as af3 WHERE a.RejectPerson = af3.UserID)";

$cond_RecordStatus = $RecordStatus > 0 ? "a.RecordStatus <> 0" : "a.RecordStatus = 0";
$cond_ItemType = $targetItemType > 0 ? " AND b.ItemType =  $targetItemType " : "";

// #### handling ordering
if($groupBy == 1){
    $order = " b.ItemCode ";
}elseif($groupBy == 2){
    $order = " a.RequestDate ";
}elseif($groupBy == 3){
    $order = " a.WriteOffReason, a.RequestDate ";
}else{
    $order = $RecordStatus > 0 ? "if(a.RecordStatus=1,a.ApproveDate,a.RejectDate) desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder" : "a.RequestDate desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder";
}

$sql = "SELECT 
				a.RecordID,
				a.ItemID,
				b.ItemType,
				b.ItemCode,
				" . $linventory->getInventoryItemNameByLang("b.") . " AS ItemName,
				a.RequestDate,
				a.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") AS ItemLocation,
				a.AdminGroupID,
				" . $linventory->getInventoryNameByLang("d.") . " AS MgmtGroup,
				a.WriteOffQty,
				a.WriteOffReason,
				a.RequestPerson,
				if($namefield1 is null or trim($namefield1)='', IF($archivename1 IS NULL,'" . $Lang['General']['UserAccountNotExists'] . "',CONCAT('*',$archivename1)), $namefield1) AS Requestor,
				if (b.DescriptionEng<>'', DescriptionEng, DescriptionChi) AS ItemDescription,
				b.ItemType,
				b.ownership,
				if(a.RecordStatus=1,'" . $i_Discipline_System_Award_Punishment_Approved . "','" . $i_Discipline_System_Award_Punishment_Rejected . "') as approve_status,
				if(a.RecordStatus=1,a.ApproveDate,a.RejectDate) as approve_date,
				if(a.RecordStatus=1, 
					if(f2.UserID is NULL, IF($archivename2 IS NULL,'" . $Lang['General']['UserAccountNotExists'] . "',CONCAT('*',$archivename2)), $namefield2),
					if(a.RejectPerson is NULL, ' ',if(f3.UserID is NULL, IF($archivename3 IS NULL,'" . $Lang['General']['UserAccountNotExists'] . "',CONCAT('*',$archivename3)), $namefield3))
				) as approve_by
		FROM
				INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID AND a.AdminGroupID IN ($target_group_list)) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS g ON (c.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (g.BuildingID = building.BuildingID)
				left join INTRANET_USER AS f ON (a.RequestPerson = f.UserID) 
				left join INTRANET_USER AS f2 ON (a.ApprovePerson = f2.UserID) 
				left join INTRANET_USER AS f3 ON (a.RejectPerson = f3.UserID) 
		WHERE
				$cond_RecordStatus
				$cond_ItemType
				$cond
                $groupby
		ORDER BY 
				$order
		";

$arr_result = $linventory->returnArray($sql);

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        $itemIDs_found .= (($itemIDs_found == "") ? "" : ",") . $arr_result[$i]['ItemID'];
    }
}

// find all itemIDs
if (trim($itemIDs_found) != "") {
    // find category and sub-category if needed
    $sql_others = "SELECT ii.ItemID, CONCAT(" . $linventory->getInventoryNameByLang("ic.") . ",' > '," . $linventory->getInventoryNameByLang("icl.") . ") AS ItemCategory
					FROM INVENTORY_ITEM AS ii
						INNER JOIN INVENTORY_CATEGORY AS ic ON ii.CategoryID = ic.CategoryID
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS icl ON ii.Category2ID = icl.Category2ID
					WHERE ii.ItemID IN ($itemIDs_found) ";
    
    $arr_result_others = $linventory->returnArray($sql_others);
    
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['ItemCategory'] = $arr_result_others[$i]['ItemCategory'];
        }
    }
    
    // $sql_single = "SELECT iise.ItemID, iise.InvoiceNo, iise.UnitPrice, iise.PurchaseDate, iise.FundingSource, iise.SerialNumber, iise.SoftwareLicenseModel, ".$linventory->getInventoryNameByLang("ifs.")." AS FundingSrcName, ifs.FundingType
    // FROM INVENTORY_ITEM_SINGLE_EXT AS iise
    // LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (iise.FundingSource = ifs.FundingSourceID)
    // WHERE iise.ItemID IN ($itemIDs_found) ";
    $sql_single = "SELECT 
					iise.ItemID, 
					iise.InvoiceNo, 
					iise.UnitPrice, 
					iise.PurchaseDate, 
					iise.FundingSource, 
					iise.SerialNumber, iise.SoftwareLicenseModel, 
					" . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , 
					" . $linventory->getInventoryNameByLang("ifs2.") . " AS FundingSrcName2 , 
					ifs.FundingType as FundingType1,
					iise.UnitPrice1,
					iise.UnitPrice2,
					ifs2.FundingType as FundingType2,
                    iise.QuotationNo,
                    iise.MaintainInfo
					FROM 
						INVENTORY_ITEM_SINGLE_EXT AS iise
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (iise.FundingSource = ifs.FundingSourceID)
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs2 ON (iise.FundingSource2 = ifs2.FundingSourceID)
					WHERE iise.ItemID IN ($itemIDs_found) ";
    $arr_result_others = $linventory->returnArray($sql_single);
    
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingSource'] = $arr_result_others[$i]['FundingSource'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['PurchaseDate'] = $arr_result_others[$i]['PurchaseDate'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['InvoiceNo'] = $arr_result_others[$i]['InvoiceNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice'] = $arr_result_others[$i]['UnitPrice'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice1'] = $arr_result_others[$i]['UnitPrice1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice2'] = $arr_result_others[$i]['UnitPrice2'];
            // $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingSrcName'] = $arr_result_others[$i]['FundingSrcName'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingSrcName'] = $arr_result_others[$i]['FundingSrcName'] . ($arr_result_others[$i]['FundingSrcName2'] ? ", " . $arr_result_others[$i]['FundingSrcName2'] : "");
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['SerialNumber'] = $arr_result_others[$i]['SerialNumber'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['LicenseModel'] = $arr_result_others[$i]['SoftwareLicenseModel'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType'] = $arr_result_others[$i]['FundingType1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType2'] = $arr_result_others[$i]['FundingType2'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['QuotationNo'] = $arr_result_others[$i]['QuotationNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['MaintainInfo'] = $arr_result_others[$i]['MaintainInfo'];
        }
    }
}

$FieldPurchaseDate = true;
$FieldLocation = true;
$FieldCaretaker = true;
$FieldUnitPrice = true;
$FieldTotalPrice = true;


$ShowField['Category'] = $FieldCategory;
$ShowField['Description'] = $FieldDescription;
$ShowField['Location'] = $FieldLocation;
$ShowField['Caretaker'] = $FieldCaretaker;
$ShowField['PurchaseDate'] = $FieldPurchaseDate;
$ShowField['Invoice'] = $FieldInvoice;
$ShowField['SerialNumber'] = $FieldSerialNumber;
$ShowField['LicenseModel'] = $FieldLicenseModel;
$ShowField['UnitPrice'] = $FieldUnitPrice;
$ShowField['TotalPrice'] = $FieldTotalPrice;
$ShowField['FundingCost'] = $FieldFundingCost;
$ShowField['FundingSource'] = $FieldFundingSource;
$ShowField['QuotationNo'] = $FieldQuotationNo;
$ShowField['MaintainInfo'] = $FieldMaintainInfo;

$exportColumn[] = array('','','','',$_SESSION["SSV_PRIVILEGE"]["school"]["name"]);
$exportColumn[] = array('','','','',$i_InventorySystem_WriteOffItemApproval);
$exportColumn[] = array('');
$exportColumn[] = array('');

$rows[0][] = $i_InventorySystem_Item_Code;
if ($ShowField['Category']) {
    $rows[0][] = $i_InventorySystem['Category'];
}
$rows[0][] = $i_InventorySystem_Item_Name;
if ($ShowField['Description']) {
    $rows[0][] = $i_InventorySystem_Item_Description;
}
if ($ShowField['PurchaseDate']) {
    $rows[0][] = $Lang['eInventory']['PurchaseDate_PLKCHC'];
}
if ($ShowField['Location']) {
    $rows[0][] = $i_InventorySystem_Item_Location;
}
if ($ShowField['Caretaker']) {
    $rows[0][] = $Lang['eInventory']['Caretaker_PLKCHC'];
}
// $rows[0][] = $i_InventorySystem['FundingSource'];
if ($ShowField['Invoice']) {
    $rows[0][] = $i_InventorySystem_Item_Invoice;
}
if ($ShowField['SerialNumber']) {
    $rows[0][] = $i_InventorySystem_Item_Serial_Num;
}
if ($ShowField['LicenseModel']) {
    $rows[0][] = $i_InventorySystem_Category2_License;
}
if ($ShowField['FundingCost']) {
    $rows[0][] = "[" . $i_InventorySystem_Report_Col_Cost . "] " . $Lang['eInventory']['FundingType']['School'];
    $rows[0][] = "[" . $i_InventorySystem_Report_Col_Cost . "] " . $Lang['eInventory']['FundingType']['Government'];
    $rows[0][] = "[" . $i_InventorySystem_Report_Col_Cost . "] " . $Lang['eInventory']['FundingType']['SponsoringBody'];
}
if ($ShowField['FundingSource']) {
    $rows[0][] = $i_InventorySystem['FundingSource'];
}
if ($ShowField['QuotationNo']){
    $rows[0][] = $i_InventorySystem_Item_Quot_Num;
}
if ($ShowField['MaintainInfo']){
    $rows[0][] = $i_InventorySystemItemMaintainInfo;
}
$rows[0][] = $i_InventorySystem_Write_Off_RequestQty;
if ($ShowField['UnitPrice']) {
    $rows[0][] = $i_InventorySystem_Unit_Price;
}
if ($ShowField['TotalPrice']) {
    $rows[0][] = $Lang['eInventory']['TotalPrice_PLKCHC'];
}
$rows[0][] = $i_InventorySystem_Write_Off_Reason;
$rows[0][] = $i_InventorySystem_RequestPerson;
$rows[0][] = $i_InventorySystem_Write_Off_RequestTime;

if (sizeof($arr_result) > 0) {
    $totalPrice = 0;
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        $item_now = $arr_result[$i];
        $symbol_for_null = ($item_now['ItemType'] != ITEM_TYPE_SINGLE) ? "n/a" : "--";
        
        $thisLocatioinID = $item_now['LocationID'];
        $thisItemID = $item_now['ItemID'];
        
        // found out funding type (funding type of single item is retrieved above)
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            $sql_bulk = "SELECT " . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , ifs.FundingType
							FROM INVENTORY_ITEM_BULK_LOCATION AS a
								 LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (a.FundingSourceID = ifs.FundingSourceID)
							WHERE a.ItemID = $thisItemID and a.LocationID=$thisLocatioinID ";
            $arr_result_others = $linventory->returnArray($sql_bulk);
            if (sizeof($arr_result_others) > 0) {
                $thisFundingSrcName = $arr_result_others[0]['FundingSrcName'];
                $thisFundingType = $arr_result_others[0]['FundingType'];
            }
        } else {
            $thisFundingSrcName = $ItemOtherInfo[$thisItemID]['FundingSrcName'];
            $thisFundingType = $ItemOtherInfo[$thisItemID]['FundingType'];
        }
        
        if ($item_now['ItemType'] == ITEM_TYPE_BULK && $AVG_UNIT_PRICE[$item_now['ItemID']] == '') {
            // find the average unit price
            $total_school_cost = 0;
            $final_unit_price = 0;
            $final_purchase_price = 0;
            $total_qty = 0;
            $written_off_qty = 0;
            $ini_total_qty = 0;
            
            // # get Total purchase price ##
            // $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
            $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = " . $item_now['ItemID'] . " AND Action = 1";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($total_purchase_price) = $tmp_result[0];
            }
            
            // # get Total Qty Of the target Item ##
            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = " . $item_now['ItemID'];
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($totalQty) = $tmp_result[0];
            }
            
            // # get written-off qty ##
            $sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = " . $item_now['ItemID'];
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($written_off_qty) = $tmp_result[0];
            }
            $ini_total_qty = $totalQty + $written_off_qty;
            
            if ($ini_total_qty > 0)
                $AVG_UNIT_PRICE[$item_now['ItemID']] = round($total_purchase_price / $ini_total_qty, 2);
        }
        
        if (trim($item_now['ItemDescription']) == "") {
            $item_now['ItemDescription'] = "--";
        }
        
        if ($ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] = $symbol_for_null;
        }
        
        /*
         * $ItemTotal = 0;
         * if ($ItemOtherInfo[$item_now['ItemID']]['UnitPrice']=="")
         * {
         * $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $AVG_UNIT_PRICE[$item_now['ItemID']];
         * }
         * $ItemTotal = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] * $item_now['WriteOffQty'];
         * $ItemTotalSum += $ItemTotal;
         * $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = round($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'], 2);
         * $ItemTotal = round($ItemTotal, 2);
         */
        
        $Cost_Ary = array();
        $ItemTotal = 0;
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            if ($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] == "") {
                $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $AVG_UNIT_PRICE[$item_now['ItemID']];
            }
            
            $ItemTotal = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] * $item_now['WriteOffQty'];
            // $ItemTotalSum += $ItemTotal;
            $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'];
            
            $Cost_Ary[$thisFundingType] = $ItemTotal;
        } else {
            $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice1'];
            $ItemTotal += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice1'];
            
            if ($ItemOtherInfo[$item_now['ItemID']]['FundingType2']) {
                $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType2']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice2'];
                $ItemTotal += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice2'];
            }
        }
        
        if ($thisFundingSrcName == "") {
            $thisFundingSrcName = $symbol_for_null;
        }
        
        $rows[$i + 1][] = $item_now['ItemCode'];
        if ($ShowField['Category']) {
            $rows[$i + 1][] = $ItemOtherInfo[$item_now['ItemID']]['ItemCategory'];
        }
        $rows[$i + 1][] = $item_now['ItemName'];
        if ($ShowField['Description']) {
            $rows[$i + 1][] = $item_now['ItemDescription'];
        }
        if ($ShowField['PurchaseDate']) {
            $rows[$i + 1][] = $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'];
        }
        if ($ShowField['Location']) {
            $rows[$i + 1][] = $item_now['ItemLocation'];
        }
        if ($ShowField['Caretaker']) {
            $rows[$i + 1][] = $item_now['MgmtGroup'];
        }
        // $rows[$i+1][] = $thisFundingSrcName;
        if ($ShowField['Invoice']) {
            $rows[$i + 1][] = $ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'];
        }
        if ($ShowField['SerialNumber']) {
            $rows[$i + 1][] = $ItemOtherInfo[$item_now['ItemID']]['SerialNumber'];
        }
        if ($ShowField['LicenseModel']) {
            $rows[$i + 1][] = $ItemOtherInfo[$item_now['ItemID']]['LicenseModel'];
        }
        if ($ShowField['FundingCost']) {
            /*
             * if($thisFundingType== ITEM_SCHOOL_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $rows[$i+1][] = $ItemTotal;
             * $rows[$i+1][] = 0;
             * $rows[$i+1][] = 0;
             * } elseif($thisFundingType== ITEM_GOVERNMENT_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $rows[$i+1][] = 0;
             * $rows[$i+1][] = $ItemTotal;
             * $rows[$i+1][] = 0;
             * } elseif($thisFundingType== ITEM_SPONSORING_BODY_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $rows[$i+1][] = 0;
             * $rows[$i+1][] = 0;
             * $rows[$i+1][] = $ItemTotal;
             * } else
             * {
             * $rows[$i+1][] = 0;
             * $rows[$i+1][] = 0;
             * $rows[$i+1][] = 0;
             * }
             */
            
            $rows[$i + 1][] = number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2);
            $rows[$i + 1][] = number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2);
            $rows[$i + 1][] = number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2);
        }
        $ItemTotal = $Cost_Ary[ITEM_SCHOOL_FUNDING] + $Cost_Ary[ITEM_GOVERNMENT_FUNDING] + $Cost_Ary[ITEM_SPONSORING_BODY_FUNDING];
        $totalPrice += $ItemTotal;
        if ($ShowField['FundingSource']) {
            $rows[$i + 1][] = $thisFundingSrcName;
        }
        if ($ShowField['QuotationNo']){
            if($ItemOtherInfo[$item_now['ItemID']]['QuotationNo'] != ""){
                $rows[$i + 1][] = $ItemOtherInfo[$item_now['ItemID']]['QuotationNo'];
            }else{
                $rows[$i + 1][] = " -- ";   
            }
        }
        if ($ShowField['MaintainInfo']) {
            if($ItemOtherInfo[$item_now['ItemID']]['MaintainInfo'] != ""){
                $rows[$i + 1][] = $ItemOtherInfo[$item_now['ItemID']]['MaintainInfo'];
            }else{
                $rows[$i + 1][] = " -- ";
            }
        }
        $rows[$i + 1][] = $item_now['WriteOffQty'];
        if ($ShowField['UnitPrice']) {
            $rows[$i + 1][] = number_format($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'], 2);
        }
        if ($ShowField['TotalPrice']) {
            $rows[$i + 1][] = number_format($ItemTotal, 2);
        }
        $rows[$i + 1][] = $item_now['WriteOffReason'];
        $rows[$i + 1][] = $item_now['Requestor'];
        $rows[$i + 1][] = $item_now['RequestDate'];
        
    }
}
if (sizeof($arr_result) == 0) {
    $file_content .= "\"$i_no_record_exists_msg\"\n";
    $rows[] = array(
        $i_no_record_exists_msg
    );
}

$rows[] = array();
$rows[] = array(
    '',
    '',
    '',
    '',
    '',
    '',    
    $Lang['eInventory']['TotalWriteOffPrice'],
    number_format($totalPrice, 2)
);

$rows[] = array();
$rows[] = array();

$rows[] = array(
    '',
    '',
    $Lang['eInventory']['PrincipalSignatureForGeneral'],
    '',
    '',
    '',
    '',
    $Lang['eInventory']['ApprovedDate'],
);

$display = $file_content;

intranet_closedb();

$filename = "write_off_approval_unicode.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>