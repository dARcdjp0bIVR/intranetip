<?php

// modifying : 

/**
 * ***********************************************************************
 * modification log
 * 2020-02-07 Tommy
 *  Customization for plkchc #A170032
 *
 * ***********************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_WriteOffApproval_Print";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$li = new libfilesystem();

$school_data = explode("\n", get_file_content("$intranet_root/file/school_data.txt"));
$school_name = $school_data[0];

$school_badge = $li->file_read($intranet_root . "/file/schoolbadge.txt");
if ($school_badge != "") {
    $badge_image = "<img src='/file/$school_badge' width=90% height=auto>";
}

$TAGS_OBJ[] = array(
    $i_InventorySystem_WriteOffItemApproval,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

if ($linventory->IS_ADMIN_USER($UserID)) {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
    $arr_admin_group_leader = $linventory->returnVector($sql);
} else {
    $groupLeaderNowAllowWriteOffItem = $linventory->retriveGroupLeaderNotAllowWriteOffItem();
    // if(!$groupLeaderNowAllowWriteOffItem)
    // {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."' AND RecordType = 1";
    // $arr_admin_group_leader = $linventory->returnVector($sql);
    // }
}
$arr_admin_group_leader = $linventory->returnVector($sql);

if (sizeof($arr_admin_group_leader) > 0) {
    $target_group_list = implode(",", $arr_admin_group_leader);
}

if($date_from != '' && $date_to != ''){
    $cond = " AND ";
    
    $cond .= " (a.RequestDate >= '".$date_from."' AND a.RequestDate <= '".$date_to."')";
}

$namefield1 = getNameFieldByLang2("f.");
$namefield2 = getNameFieldByLang2("f2.");
$namefield3 = getNameFieldByLang2("f3.");

$archivename1 = "(select " . getNameFieldByLang2("af.") . " from INTRANET_ARCHIVE_USER as af WHERE a.RequestPerson = af.UserID)";
$archivename2 = "(select " . getNameFieldByLang2("af2.") . "from INTRANET_ARCHIVE_USER as af2 WHERE a.ApprovePerson = af2.UserID)";
$archivename3 = "(select " . getNameFieldByLang2("af3.") . "from INTRANET_ARCHIVE_USER as af3 WHERE a.RejectPerson = af3.UserID)";

$cond_RecordStatus = $RecordStatus > 0 ? "a.RecordStatus <> 0" : "a.RecordStatus = 0";
$cond_ItemType = $targetItemType > 0 ? " AND b.ItemType =  $targetItemType " : "";

// #### handling ordering
// #### handling ordering
if($groupBy == 1){
    $order = " b.ItemCode ";
}elseif($groupBy == 2){
    $order = " a.RequestDate ";
}elseif($groupBy == 3){
    $order = " a.WriteOffReason, a.RequestDate ";
}else{
    $order = $RecordStatus > 0 ? "if(a.RecordStatus=1,a.ApproveDate,a.RejectDate) desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder" : "a.RequestDate desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder";
}

/*
 * not use since April 2011
 * $sql = "SELECT
 * a.RecordID,
 * a.ItemID,
 * b.ItemType,
 * b.ItemCode,
 * ".$linventory->getInventoryItemNameByLang("b.").",
 * a.RequestDate,
 * a.LocationID,
 * CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("g.").",' > ',".$linventory->getInventoryNameByLang("c.")."),
 * a.AdminGroupID,
 * ".$linventory->getInventoryNameByLang("d.").",
 * a.WriteOffQty,
 * a.WriteOffReason,
 * a.RequestPerson,
 * $namefield1,
 * IF(h.AttachmentID != '', 'Y', 'N')
 * FROM
 * INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
 * INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
 * INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
 * INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID AND a.AdminGroupID IN ($target_group_list)) INNER JOIN
 * INTRANET_USER AS f ON (a.RequestPerson = f.UserID) INNER JOIN
 * INVENTORY_LOCATION_LEVEL AS g ON (c.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
 * INVENTORY_LOCATION_BUILDING AS building ON (g.BuildingID = building.BuildingID) LEFT OUTER JOIN
 * INVENTORY_ITEM_WRITE_OFF_ATTACHMENT AS h ON (a.RecordID = h.RequestWriteOffID)
 * WHERE
 * a.RecordStatus = 0
 * ORDER BY
 * b.ItemType, g.DisplayOrder, c.DisplayOrder, b.ItemCode
 * ";
 */

// ## Set SQL Condition - Keyword ###
if ($keyword != "") {
    $cond .= " AND ";
    
    $cond .= " ((b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
				(b.ItemCode LIKE '%$keyword%') 
				)";
}

$sql = "SELECT 
				a.RecordID,
				a.ItemID,
				b.ItemType,
				b.ItemCode,
				" . $linventory->getInventoryItemNameByLang("b.") . " AS ItemName,
				a.RequestDate,
				a.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("building.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") AS ItemLocation,
				a.AdminGroupID,
				" . $linventory->getInventoryNameByLang("d.") . " AS MgmtGroup,
				a.WriteOffQty,
				a.WriteOffReason,
				a.RequestPerson,
				if($namefield1 is null or trim($namefield1)='', IF($archivename1 IS NULL,'<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<font color=\"red\">*</font>',$archivename1)), $namefield1) AS Requestor,
				if (b.DescriptionEng<>'', DescriptionEng, DescriptionChi) AS ItemDescription,
				b.ItemType,
				b.ownership,
				if(a.RecordStatus=1,'" . $i_Discipline_System_Award_Punishment_Approved . "','" . $i_Discipline_System_Award_Punishment_Rejected . "') as approve_status,
				if(a.RecordStatus=1,a.ApproveDate,a.RejectDate) as approve_date,
				if(a.RecordStatus=1, 
					if(f2.UserID is NULL, IF($archivename2 IS NULL,'<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<font color=\"red\">*</font>',$archivename2)), $namefield2),
					if(a.RejectPerson is NULL, ' ',if(f3.UserID is NULL, IF($archivename3 IS NULL,'<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<font color=\"red\">*</font>',$archivename3)), $namefield3))
				) as approve_by
		FROM
				INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS d ON (a.AdminGroupID = d.AdminGroupID AND a.AdminGroupID IN ($target_group_list)) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS g ON (c.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
				INVENTORY_LOCATION_BUILDING AS building ON (g.BuildingID = building.BuildingID)
				left JOIN INTRANET_USER AS f ON (a.RequestPerson = f.UserID) 
				left join INTRANET_USER AS f2 ON (a.ApprovePerson = f2.UserID) 
				left join INTRANET_USER AS f3 ON (a.RejectPerson = f3.UserID) 
		WHERE
				$cond_RecordStatus
				$cond_ItemType
				$cond
		ORDER BY 
				$order
		";

$arr_result = $linventory->returnArray($sql);

if (sizeof($arr_result) > 0) {
    $totalPrice = 0;
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        $itemIDs_found .= (($itemIDs_found == "") ? "" : ",") . $arr_result[$i]['ItemID'];
    }
}

// find all itemIDs
if (trim($itemIDs_found) != "") {
    // find category and sub-category if needed
    $sql_others = "SELECT ii.ItemID, CONCAT(" . $linventory->getInventoryNameByLang("ic.") . ",' > '," . $linventory->getInventoryNameByLang("icl.") . ") AS ItemCategory
					FROM INVENTORY_ITEM AS ii
						INNER JOIN INVENTORY_CATEGORY AS ic ON ii.CategoryID = ic.CategoryID
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS icl ON ii.Category2ID = icl.Category2ID
					WHERE ii.ItemID IN ($itemIDs_found) ";
    
    $arr_result_others = $linventory->returnArray($sql_others);
    
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['ItemCategory'] = $arr_result_others[$i]['ItemCategory'];
        }
    }
    
    $sql_single = "SELECT 
					iise.ItemID, 
					iise.InvoiceNo, 
					iise.UnitPrice, 
					iise.PurchaseDate, 
					iise.FundingSource, 
					iise.SerialNumber, iise.SoftwareLicenseModel, 
					" . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , 
					" . $linventory->getInventoryNameByLang("ifs2.") . " AS FundingSrcName2 , 
					ifs.FundingType as FundingType1,
					iise.UnitPrice1,
					iise.UnitPrice2,
					ifs2.FundingType as FundingType2,
                    iise.QuotationNo,
                    iise.MaintainInfo
					FROM 
						INVENTORY_ITEM_SINGLE_EXT AS iise
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (iise.FundingSource = ifs.FundingSourceID)
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs2 ON (iise.FundingSource2 = ifs2.FundingSourceID)
					WHERE iise.ItemID IN ($itemIDs_found) ";
    $arr_result_others = $linventory->returnArray($sql_single);
    
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['PurchaseDate'] = $arr_result_others[$i]['PurchaseDate'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['InvoiceNo'] = $arr_result_others[$i]['InvoiceNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice'] = $arr_result_others[$i]['UnitPrice'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice1'] = $arr_result_others[$i]['UnitPrice1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice2'] = $arr_result_others[$i]['UnitPrice2'];
            // $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingSrcName'] = $arr_result_others[$i]['FundingSrcName'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingSrcName'] = $arr_result_others[$i]['FundingSrcName'] . ($arr_result_others[$i]['FundingSrcName2'] ? ", " . $arr_result_others[$i]['FundingSrcName2'] : "");
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['SerialNumber'] = $arr_result_others[$i]['SerialNumber'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['LicenseModel'] = $arr_result_others[$i]['SoftwareLicenseModel'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType'] = $arr_result_others[$i]['FundingType1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType2'] = $arr_result_others[$i]['FundingType2'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['QuotationNo'] = $arr_result_others[$i]['QuotationNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['MaintainInfo'] = $arr_result_others[$i]['MaintainInfo'];
        }
    }
    // debug_r($arr_result_others);
    
    /*
     * $sql_single = "SELECT * FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID IN ($itemIDs_found) ";
     *
     * $arr_result_others = $linventory->returnArray($sql_single);
     * //debug_r($arr_result_others);
     */
}

$FieldPurchaseDate = true;
$FieldLocation = true;
$FieldCaretaker = true;
$FieldUnitPrice = true;
$FieldTotalPrice = true;

$ShowField['Category'] = $FieldCategory;
$ShowField['Description'] = $FieldDescription;
$ShowField['Location'] = $FieldLocation;
$ShowField['Caretaker'] = $FieldCaretaker;
$ShowField['PurchaseDate'] = $FieldPurchaseDate;
$ShowField['Invoice'] = $FieldInvoice;
$ShowField['SerialNumber'] = $FieldSerialNumber;
$ShowField['LicenseModel'] = $FieldLicenseModel;
$ShowField['UnitPrice'] = $FieldUnitPrice;
$ShowField['TotalPrice'] = $FieldTotalPrice;
$ShowField['FundingCost'] = $FieldFundingCost;
$ShowField['FundingSource'] = $FieldFundingSource;
$ShowField['QuotationNo'] = $FieldQuotationNo;
$ShowField['MaintainInfo'] = $FieldMaintainInfo;

if ($ShowField['FundingCost']) {
    $rowspan = "rowspan='2'";
}

$fieldsTotal = 3; // item code, item name & quantity
$fieldsTotalEnd = 3; // Write-off Reason, Requestor, Request Date, Supervisor's / Principal's Signature
$table_content .= "<tr>";
$table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_Item_Code</td>";
if ($ShowField['Category']) {
    $table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystem['Category'] . "</td>";
    $fieldsTotal ++;
}
$table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_Item_Name</td>";
if ($ShowField['Description']) {
    $table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_Item_Description</td>";
    $fieldsTotal ++;
}
if ($ShowField['PurchaseDate']) {
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $Lang['eInventory']['PurchaseDate_PLKCHC'] . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['Location']) {
    $table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_Item_Location</td>";
    $fieldsTotal ++;
}
if ($ShowField['Caretaker']) {
    $group_new_title = ($sys_custom['eInventoryCustForSMC']) ? "Team / Department" : $Lang['eInventory']['Caretaker_PLKCHC'];
    $table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $group_new_title . "</td>";
    $fieldsTotal ++;
}
// $table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>".$i_InventorySystem['FundingSource']."</td>";
if ($ShowField['Invoice']) {
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystem_Item_Invoice . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['SerialNumber']) {
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystem_Item_Serial_Num . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['LicenseModel']) {
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystem_Category2_License . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['FundingCost']) {
    $table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" colspan='3'>" . $i_InventorySystem_Report_Col_Cost . "</td>";
    $fieldsTotal ++;
    $fieldsTotal ++;
    $fieldsTotal ++;
}
if ($ShowField['FundingSource']) {
    $table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystem['FundingSource'] . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['QuotationNo']){
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystem_Item_Quot_Num . "</td>";
}
if ($ShowField['MaintainInfo']){
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystemItemMaintainInfo . "</td>";
}
$table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_Write_Off_RequestQty</td>";
if ($ShowField['UnitPrice']) {
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $i_InventorySystem_Unit_Price . "</td>";
    $fieldsTotal ++;
}
if ($ShowField['TotalPrice']) {
    $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>" . $Lang['eInventory']['TotalPrice_PLKCHC'] . "</td>";
}
$table_content .= "<td width=\"15%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_Write_Off_Reason</td>";
$table_content .= "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_RequestPerson</td>";
$table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\" $rowspan>$i_InventorySystem_Write_Off_RequestTime</td>";
// $table_content .= "<td width=\"5%\" class=\"eSporttdborder eSportprinttabletitle\">$i_InventorySystem_Write_Off_Attachment</td>";

$table_content .= "</tr>";

if ($ShowField['FundingCost']) {
    $table_content .= "<tr >
		<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['eInventory']['FundingType']['School'] . "</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['eInventory']['FundingType']['Government'] . "</td>
		<td class=\"eSporttdborder eSportprinttabletitle\">" . $Lang['eInventory']['FundingType']['SponsoringBody'] . "</td></tr>";
}

$ItemTotalSum = 0;
if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        $item_now = $arr_result[$i];
        $symbol_for_null = ($item_now['ItemType'] != ITEM_TYPE_SINGLE) ? "n/a" : "--";
        
        $thisLocatioinID = $item_now['LocationID'];
        $thisItemID = $item_now['ItemID'];
        
        // found out funding type (funding type of single item is retrieved above)
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            $sql_bulk = "SELECT " . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , ifs.FundingType
							FROM INVENTORY_ITEM_BULK_LOCATION AS a
								 LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (a.FundingSourceID = ifs.FundingSourceID)
							WHERE a.ItemID = '".$thisItemID."' and a.LocationID='".$thisLocatioinID."' ";
            $arr_result_others = $linventory->returnArray($sql_bulk);
            if (sizeof($arr_result_others) > 0) {
                $thisFundingSrcName = $arr_result_others[0]['FundingSrcName'];
                $thisFundingType = $arr_result_others[0]['FundingType'];
            }
        } else {
            $thisFundingSrcName = $ItemOtherInfo[$thisItemID]['FundingSrcName'];
            $thisFundingType = $ItemOtherInfo[$thisItemID]['FundingType'];
        }
        
        if ($item_now['ItemType'] == ITEM_TYPE_BULK && $AVG_UNIT_PRICE[$item_now['ItemID']] == '') {
            // find the average unit price
            $total_school_cost = 0;
            $final_unit_price = 0;
            $final_purchase_price = 0;
            $total_qty = 0;
            $written_off_qty = 0;
            $ini_total_qty = 0;
            
            // # get Total purchase price ##
            // $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
            $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_now['ItemID'] . "' AND Action = 1";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($total_purchase_price) = $tmp_result[0];
            }
            
            // # get Total Qty Of the target Item ##
            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '" . $item_now['ItemID']."'";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($totalQty) = $tmp_result[0];
            }
            
            // # get written-off qty ##
            $sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_now['ItemID']."'";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($written_off_qty) = $tmp_result[0];
            }
            $ini_total_qty = $totalQty + $written_off_qty;
            
            if ($ini_total_qty > 0)
                $AVG_UNIT_PRICE[$item_now['ItemID']] = round($total_purchase_price / $ini_total_qty, 2);
        }
        
        if (trim($item_now['ItemDescription']) == "") {
            $item_now['ItemDescription'] = "--";
        }
        
        if ($ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] = $symbol_for_null;
        }
        
        $Cost_Ary = array();
        $ItemTotal = 0;
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            if ($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] == "") {
                $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $AVG_UNIT_PRICE[$item_now['ItemID']];
            }
            
            $ItemTotal = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] * $item_now['WriteOffQty'];
            // $ItemTotalSum += $ItemTotal;
            $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'];
            
            $Cost_Ary[$thisFundingType] = $ItemTotal;
            // $ItemTotal = number_format($ItemTotal,2);
        } else {
            $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice1'];
            $ItemTotal += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice1'];
            
            if ($ItemOtherInfo[$item_now['ItemID']]['FundingType2']){
                $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType2']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice2'];
                $ItemTotal += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice2'];
            }
        }
        
        if ($thisFundingSrcName == "") {
            $thisFundingSrcName = $symbol_for_null;
        }
        
        $table_content .= "<tr>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemCode'] . "</td>";
        
        if ($ShowField['Category']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['ItemCategory'] . "</td>";
        }
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemName'] . "</td>";
        if ($ShowField['Description']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemDescription'] . "</td>";
        }
        if ($ShowField['PurchaseDate']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] . "</td>";
        }
        if ($ShowField['Location']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['ItemLocation'] . "</td>";
        }
        if ($ShowField['Caretaker']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['MgmtGroup'] . "</td>";
        }
        // $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".$thisFundingSrcName."</td>";

        if ($ShowField['Invoice']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] . "</td>";
        }
        if ($ShowField['SerialNumber']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] . "</td>";
        }
        
        if ($ShowField['LicenseModel']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] . "</td>";
        }
        if ($ShowField['FundingCost']) {
            /*
             * if($thisFundingType == ITEM_SCHOOL_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".$ItemTotal."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * } elseif($thisFundingType== ITEM_GOVERNMENT_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".$ItemTotal."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * } elseif($thisFundingType== ITEM_SPONSORING_BODY_FUNDING && $ItemTotal!=$symbol_for_null)
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\" $str_col_fund >".$ItemTotal."</td>";
             * } else
             * {
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".number_format(0, 2)."</td>";
             * $table_content .= "<td class=\"eSporttdborder eSportprinttext\">".number_format(0, 2)."</td>";
             * }
             */
            
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . number_format($Cost_Ary[ITEM_SCHOOL_FUNDING], 2) . "</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . number_format($Cost_Ary[ITEM_GOVERNMENT_FUNDING], 2) . "</td>";
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . number_format($Cost_Ary[ITEM_SPONSORING_BODY_FUNDING], 2) . "</td>";
        }
        
        $ItemTotal = $Cost_Ary[ITEM_SCHOOL_FUNDING] + $Cost_Ary[ITEM_GOVERNMENT_FUNDING] + $Cost_Ary[ITEM_SPONSORING_BODY_FUNDING];
        $ItemTotalSum += $ItemTotal;
        if ($ShowField['FundingSource']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $thisFundingSrcName . "</td>";
        }
        if ($ShowField['QuotationNo']){
            if($ItemOtherInfo[$item_now['ItemID']]['QuotationNo'] != ""){
                $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . $ItemOtherInfo[$item_now['ItemID']]['QuotationNo'] . "</td>";
            }else{
                $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . " -- " . "</td>";
            }
        }
        if ($ShowField['MaintainInfo']){
            if($ItemOtherInfo[$item_now['ItemID']]['MaintainInfo'] != ""){
                $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . $ItemOtherInfo[$item_now['ItemID']]['MaintainInfo'] . "</td>";
            }else{
                $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . " -- " . "</td>";
            }
        }
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['WriteOffQty'] . "</td>";
        if ($ShowField['UnitPrice']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . number_format($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'], 2) . "</td>";
        }
        if ($ShowField['TotalPrice']) {
            $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . number_format($ItemTotal, 2) . "</td>";
        }
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['WriteOffReason'] . "&nbsp;</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\">" . $item_now['Requestor'] . "</td>";
        $table_content .= "<td class=\"eSporttdborder eSportprinttext\" nowrap='nowrap'>" . $item_now['RequestDate'] . "</td>";
        
        $table_content .= "</tr>";
    }
}
$totalPrice = number_format($ItemTotalSum, 2);
if (sizeof($arr_result) == 0) {
    $table_content .= "<tr><td class=\"eSporttdborder eSportprinttext\" colspan=\"16\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}

$table_content .= "<tr>";
$table_content .= "</tr>";

include_once ($PATH_WRT_ROOT . "/templates/" . $LAYOUT_SKIN . "/layout/print_header.php");
?>

<table width="90%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>

<?php

if ($sys_custom['eInventoryCustForSMC']) {
    $badge_image = "<img src='/images/customization/smc_badge.jpg' border='0' width='70' />";
    ?>
<table width="90%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="eSportprinttext"><?=$badge_image?></td>
	</tr>
	<tr>
		<td class="eSportprinttext"><i>Nil magnum nisi bonum</i></td>
	</tr>
	<tr>
		<td class="eSportprinttext"><i>No greatness without goodness</i></td>
	</tr>
	<tr>
		<td class="eSportprinttext">&nbsp;</td>
	</tr>
	<tr>
		<td class="eSportprinttext"><font size="4">Write-off Application</font></td>
	</tr>
	<tr>
		<td class="eSportprinttext">&nbsp;</td>
	</tr>
</table>

<?php
} else {
    ?>

<table width="95%" border="0" cellspacing="0" cellpadding="0"
	align="center">
	<tr>
		<td rowspan="2" width="80px"><?=$badge_image?></td>
		<td class="eSportprinttext" align="center"><font size="5"><?=$school_name?></font></td>
		<td rowspan="2" width="80px"></td>
	</tr>
	<tr>
		<td class="eSportprinttext" align="center"><font size="5"><?=$i_InventorySystem_WriteOffItemApproval?></font></td>
	</tr>
</table>

<?php
}
?>

<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5"
	align="center" class="common_table_list_v30 view_table_list_v30">
	<?=$table_content;?>
</table>
<?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?>



<br />
<br />
<table width="100%" border="0">
			<tr>
				<td colspan="2" align='right'><?php echo $Lang['eInventory']['TotalWriteOffPrice']; ?>: <?=$totalPrice?></td>
				<td colspan="2" align='right'></td>
			</tr>
	<tr>
		<td>
			<table width='340' border='0'>
				<tr>
					<td nowrap='nowrap'><?=$Lang['eInventory']['PrincipalSignatureForGeneral']?></td>
					<td width="340" style="border-bottom: 1px solid #555555;">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
		<td align="right"><table width='220' border='0'>
				<tr>
					<td nowrap='nowrap'><?=$Lang['eInventory']['ApprovedDate']?></td>
					<td width="220" style="border-bottom: 1px solid #555555;">&nbsp;</td>
				</tr>
			</table></td>
	</tr>
</table>

<?

intranet_closedb();
?>