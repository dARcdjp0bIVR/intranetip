<?php
// modifying : 

// if you want to upload it to 149 or client, pls update write_off_item_export.php & write_off_item_print.php as well!

// ########################################
//
// 2020-02-07 Tommy
//  Customization for plkchc #A170032
//
// ##########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

// preset fields for different customizations and general
if (! isset($FieldLocationTmp) && ! $sys_custom['eInventoryCustForHKBTS']) {
    $FieldLocation = true;
}
if (! isset($FieldCaretakerTmp)) {
    $FieldCaretaker = true;
}

if (! isset($FieldPurchaseDateTmp) && ($sys_custom['eInventoryCustForSMC'] || $sys_custom['eInventoryCustForHKBTS'] || $sys_custom['eInventory']['plkchc_report'])) {
    $FieldPurchaseDate = true;
}

if (! isset($FieldFundingCostTmp) && $sys_custom['eInventoryCustForSMC']) {
    $FieldFundingCost = true;
}
if (! isset($FieldDescriptionTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldDescription = true;
}

if (! isset($FieldSerialNumberTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldSerialNumber = true;
}
if (! isset($FieldLicenseModelTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldLicenseModel = true;
}
if (! isset($FieldInvoiceTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldInvoice = true;
}
if (! isset($FieldUnitPriceTmp) && ($sys_custom['eInventoryCustForHKBTS'] || $sys_custom['eInventory']['plkchc_report'])) {
    $FieldUnitPrice = true;
}
if(! isset($FieldTotalPriceChecked) && $sys_custom['eInventory']['plkchc_report']){
    $FieldTotalPrice = true;
}
if (! isset($FieldFundingSourceTmp) && $sys_custom['eInventoryCustForHKBTS']) {
    $FieldFundingSource = true;
}

if(! isset($FieldQuotationNoTmp) && $sys_custom['eInventoryCustForHKBTS']){
    $FieldQuotationNo = true;
}

if(! isset($FieldMaintainInfoTmp) && $sys_custom['eInventoryCustForHKBTS']){
    $FieldMaintainInfo = true;
}

$FieldCategoryChecked = ($FieldCategory) ? "checked='checked'" : "";
$FieldDescriptionChecked = ($FieldDescription) ? "checked='checked'" : "";
$FieldLocationChecked = ($FieldLocation) ? "checked='checked'" : "";
$FieldCaretakerChecked = ($FieldCaretaker) ? "checked='checked'" : "";
$FieldPurchaseDateChecked = ($FieldPurchaseDate) ? "checked='checked'" : "";
$FieldInvoiceChecked = ($FieldInvoice) ? "checked='checked'" : "";
$FieldSerialNumberChecked = ($FieldSerialNumber) ? "checked='checked'" : "";
$FieldLicenseModelChecked = ($FieldLicenseModel) ? "checked='checked'" : "";
$FieldUnitPriceChecked = ($FieldUnitPrice) ? "checked='checked'" : "";
$FieldTotalPriceChecked = ($FieldTotalPrice) ? "checked='checked'" : "";
$FieldFundingCostChecked = ($FieldFundingCost) ? "checked='checked'" : "";
$FieldFundingSourceChecked = ($FieldFundingSource) ? "checked='checked'" : "";
$FieldQuotationNoChecked = ($FieldQuotationNo) ? "checked = 'checked'": "";
$FieldMaintainInfoChecked = ($FieldMaintainInfo) ? "checked = 'checked'": "";

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_WriteOffApproval_PLKCHC";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// get group leader #
if ($linventory->IS_ADMIN_USER($UserID)) {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
} else {
    $groupLeaderNowAllowWriteOffItem = $linventory->retriveGroupLeaderNotAllowWriteOffItem();
    // if(!$groupLeaderNowAllowWriteOffItem)
    // {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."' AND RecordType = 1";
    // $arr_admin_group_leader = $linventory->returnVector($sql);
    // }
}
$arr_admin_group_leader = $linventory->returnVector($sql);

if (sizeof($arr_admin_group_leader) > 0) {
    $target_group_list = implode(",", $arr_admin_group_leader);
}
// end #

// if($target_group_list == "")
// {
// header("location: ../inventory/items_full_list.php");
// exit();
// }

$TAGS_OBJ[] = array(
    $i_InventorySystem_WriteOffItemApproval,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arr_file_type = array(
    array(
        1,
        "HTML"
    ),
    array(
        2,
        "CSV"
    )
);
$file_type_selection = getSelectByArray($arr_file_type, "name=\"targetFileType\" ", $targetFileType, 0, 1);
$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_FileFormat</td>";
$table_selection .= "<td class=\"tabletext\">$file_type_selection</td></tr>";

// display items
$table_selection .= "<tr  ><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['RequestDate_PLKCHC'] . "</td>";
if (isset($PurchaseDateFrom)) {
    $date_from = $PurchaseDateFrom;
} else {
    $date_from = ($PurchaseDateFrom != "") ? $PurchaseDateFrom : date("Y-m-d", getStartOfAcademicYear());
}
if (isset($PurchaseDateTo)) {
    $date_to = $PurchaseDateTo;
} else {
    $date_to = ($PurchaseDateTo != "") ? $PurchaseDateTo : date("Y-m-d");
}

// $purchase_period = "<table border='0' ><tr>
// <td>".strtolower($Lang['General']['From'])." </td>
// <td>" . $linterface->GET_DATE_FIELD("PurchaseDateFrom", "form1", "PurchaseDateFrom", $date_from) . "</td>
// <td nowrap='nowrap'>&nbsp; ".strtolower($Lang['General']['To'])." </td>
// <td>" . $linterface->GET_DATE_FIELD("PurchaseDateTo", "form1", "PurchaseDateTo", $date_to) . "</td>
// </tr></table>";

$purchase_period = "<table border='0' ><tr>
					<td>" . strtolower($Lang['General']['From']) . " </td>
					<td>" . $linterface->GET_DATE_PICKER("PurchaseDateFrom", $date_from, "", "", "", "", "", "", "", "", 1) . "</td>
					<td nowrap='nowrap'>&nbsp; " . strtolower($Lang['General']['To']) . " </td>
					<td>" . $linterface->GET_DATE_PICKER("PurchaseDateTo", $date_to, "", "", "", "", "", "", "", "", 1) . "</td>
					</tr></table>";

$table_selection .= "<td class=\"tabletext\">$purchase_period</td></tr>";

/*
 * $ShowSchoolLogoChecked = ($ShowSchoolLogo || !isset($groupBy)) ? "checked='checked'" : "";
 * $table_selection .= "<div><input type='checkbox' name='ShowSchoolLogo' id='ShowSchoolLogo' value='1' $ShowSchoolLogoChecked> <label for='ShowSchoolLogo'>". $i_adminmenu_basic_settings_badge ."</label> </div>\n";
 */


$table_selection .= "</td></tr>";

$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['WriteOffStatus_PLKCHC'] . "</td>";
$table_selection .= "<td><select name='RecordStatus'>";
$table_selection .= "<option value='0' " . ($RecordStatus < 1 ? "selected" : "") . ">" . $Lang['eInventory']['WaitingForApproval'] . "</option>";
$table_selection .= "<option value='3'" . ($RecordStatus > 0 ? "selected" : "") . ">" . $Lang['eInventory']['Handled'] . "</option>";
$table_selection .= "</select> ";
$table_selection .= "</td></tr>";

$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $Lang['eInventory']['ItemType_PLKCHC'] . "</td>";
$table_selection .= "<td>".getSelectByArray($i_InventorySystem_ItemType_Array, "name=\"targetItemType\" ", $targetItemType, 1, 0, "$i_InventorySystem_ItemType_All");
$table_selection .= "</td></tr>";

$arr_groupBy = array(
    array(
        1,
        $i_InventorySystem_Item_Code
    ),
    array(
        2,
        $Lang['eInventory']['RequestDate_PLKCHC']
    ),
    array(
        3,
        $i_InventorySystem_Write_Off_Reason
    )
);	

$groupby_selection = getSelectByArray($arr_groupBy, "name=\"groupBy\" id=\"groupBy\" ", $groupBy);
$table_selection .= "<tr><td valign=\"top\" width=\"15%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">".$Lang['eInventory']['SortBy']."</td>";
$table_selection .= "<td>$groupby_selection</td></tr>";

// Create Search box #
$searchbox .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\">&nbsp;";
$searchbox .= "<input class=\"textboxnum\" type=\"text\" id=\"keyword\" name=\"keyword\" maxlength=\"50\" value=\"" . stripslashes($keyword) . "\"  onKeyPress=\"return submitenter(this,event)\">&nbsp;\n";
// End #
$search_option .= $searchbox;

// End #

// ## Set SQL Condition - Keyword ###
if ($keyword != "") {
    $cond .= " AND ";
    
    $cond .= " ((b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
				(b.ItemCode LIKE '%$keyword%') 
				)";
}

$namefield1 = getNameFieldByLang2("f.");
$namefield2 = getNameFieldByLang2("f2.");
$namefield3 = getNameFieldByLang2("f3.");

$archivename1 = "(select " . getNameFieldByLang2("af.") . " from INTRANET_ARCHIVE_USER as af WHERE a.RequestPerson = af.UserID)";
$archivename2 = "(select " . getNameFieldByLang2("af2.") . "from INTRANET_ARCHIVE_USER as af2 WHERE a.ApprovePerson = af2.UserID)";
$archivename3 = "(select " . getNameFieldByLang2("af3.") . "from INTRANET_ARCHIVE_USER as af3 WHERE a.RejectPerson = af3.UserID)";

$cond_RecordStatus = $RecordStatus > 0 ? "a.RecordStatus <> 0" : "a.RecordStatus = 0";
// #### handling $targetItemType
$cond_ItemType = $targetItemType > 0 ? " AND b.ItemType =  $targetItemType " : "";

// #### handling ordering
$order = $RecordStatus > 0 ? "if(a.RecordStatus=1,a.ApproveDate,a.RejectDate) desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder" : "a.RequestDate desc, b.ItemCode, building.DisplayOrder, g.DisplayOrder, c.DisplayOrder";

// find all itemIDs
if (trim($itemIDs_found) != "") {
    // find category and sub-category if needed
    $sql_others = "SELECT ii.ItemID, CONCAT(" . $linventory->getInventoryNameByLang("ic.") . ",' > '," . $linventory->getInventoryNameByLang("icl.") . ") AS ItemCategory
					FROM INVENTORY_ITEM AS ii
						INNER JOIN INVENTORY_CATEGORY AS ic ON ii.CategoryID = ic.CategoryID
						INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS icl ON ii.Category2ID = icl.Category2ID
					WHERE ii.ItemID IN ($itemIDs_found) ";
    
    $arr_result_others = $linventory->returnArray($sql_others);
    
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['ItemCategory'] = $arr_result_others[$i]['ItemCategory'];
        }
    }
    
    // single
    $sql_single = "SELECT 
					iise.ItemID, 
					iise.InvoiceNo, 
					iise.UnitPrice, 
					iise.PurchaseDate, 
					iise.FundingSource, 
					iise.SerialNumber, iise.SoftwareLicenseModel, 
					" . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , 
					" . $linventory->getInventoryNameByLang("ifs2.") . " AS FundingSrcName2 , 
					ifs.FundingType as FundingType1,
					iise.UnitPrice1,
					iise.UnitPrice2,
					ifs2.FundingType as FundingType2,
                    iise.QuotationNo,
                    iise.MaintainInfo
					FROM 
						INVENTORY_ITEM_SINGLE_EXT AS iise
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (iise.FundingSource = ifs.FundingSourceID)
						LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs2 ON (iise.FundingSource2 = ifs2.FundingSourceID)
					WHERE iise.ItemID IN ($itemIDs_found) ";
    $arr_result_others = $linventory->returnArray($sql_single);
    if (sizeof($arr_result_others) > 0) {
        for ($i = 0; $i < sizeof($arr_result_others); $i ++) {
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['PurchaseDate'] = $arr_result_others[$i]['PurchaseDate'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['InvoiceNo'] = $arr_result_others[$i]['InvoiceNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice'] = $arr_result_others[$i]['UnitPrice'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice1'] = $arr_result_others[$i]['UnitPrice1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['UnitPrice2'] = $arr_result_others[$i]['UnitPrice2'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingSrcName'] = $arr_result_others[$i]['FundingSrcName'] . ($arr_result_others[$i]['FundingSrcName2'] ? ", " . $arr_result_others[$i]['FundingSrcName2'] : "");
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['SerialNumber'] = $arr_result_others[$i]['SerialNumber'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['LicenseModel'] = $arr_result_others[$i]['SoftwareLicenseModel'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType'] = $arr_result_others[$i]['FundingType1'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['FundingType2'] = $arr_result_others[$i]['FundingType2'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['QuotationNo'] = $arr_result_others[$i]['QuotationNo'];
            $ItemOtherInfo[$arr_result_others[$i]['ItemID']]['MaintainInfo'] = $arr_result_others[$i]['MaintainInfo'];
        }
    }
}

$ShowField['Category'] = $FieldCategory;
$ShowField['Description'] = $FieldDescription;
$ShowField['Location'] = $FieldLocation;
$ShowField['Caretaker'] = $FieldCaretaker;
$ShowField['PurchaseDate'] = $FieldPurchaseDate;
$ShowField['Invoice'] = $FieldInvoice;
$ShowField['SerialNumber'] = $FieldSerialNumber;
$ShowField['LicenseModel'] = $FieldLicenseModel;
$ShowField['UnitPrice'] = $FieldUnitPrice;
$ShowField['TotalPrice'] = $FieldTotalPrice;
$ShowField['FundingCost'] = $FieldFundingCost;
$ShowField['FundingSource'] = $FieldFundingSource;
$ShowField['QuotationNo'] = $FieldQuotationNo;
$ShowField['MaintainInfo'] = $FieldMaintainInfo;

if ($ShowField['FundingCost']) {
    $rowspan = "rowspan='2'";
}

if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        $item_now = $arr_result[$i];
        $symbol_for_null = ($item_now['ItemType'] != ITEM_TYPE_SINGLE) ? "n/a" : "--";
        
        $thisLocatioinID = $item_now['LocationID'];
        $thisItemID = $item_now['ItemID'];
        
        // found out funding type (funding type of single item is retrieved above)
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            $sql_bulk = "SELECT " . $linventory->getInventoryNameByLang("ifs.") . " AS FundingSrcName , ifs.FundingType
							FROM INVENTORY_ITEM_BULK_LOCATION AS a
								 LEFT JOIN INVENTORY_FUNDING_SOURCE AS ifs ON (a.FundingSourceID = ifs.FundingSourceID)
							WHERE a.ItemID = $thisItemID and a.LocationID=$thisLocatioinID ";
            $arr_result_others = $linventory->returnArray($sql_bulk);
            if (sizeof($arr_result_others) > 0) {
                $thisFundingSrcName = $arr_result_others[0]['FundingSrcName'];
                $thisFundingType = $arr_result_others[0]['FundingType'];
            }
            
            $sql_bulk = "SELECT PurchaseDate FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$thisItemID."' AND PurchaseDate>0 ORDER BY PurchaseDate";
            $arr_result_others = $linventory->returnArray($sql_bulk);
            $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] = (count($arr_result_others) > 1 ? '<font style="color:red">^</font>' : '') . $arr_result_others[0]['PurchaseDate'];
        } else {
            $thisFundingSrcName = $ItemOtherInfo[$thisItemID]['FundingSrcName'];
            $thisFundingType = $ItemOtherInfo[$thisItemID]['FundingType'];
        }
        
        if ($item_now['ItemType'] == ITEM_TYPE_BULK && $AVG_UNIT_PRICE[$item_now['ItemID']] == '') {
            // find the average unit price
            $total_school_cost = 0;
            $final_unit_price = 0;
            $final_purchase_price = 0;
            $total_qty = 0;
            $written_off_qty = 0;
            $ini_total_qty = 0;
            
            // # get Total purchase price ##
            // $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND Action = 1";
            $sql = "SELECT SUM(UnitPrice*QtyChange) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '" . $item_now['ItemID'] . "' AND Action = 1";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($total_purchase_price) = $tmp_result[0];
            }
            
            // # get Total Qty Of the target Item ##
            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '" . $item_now['ItemID']."'";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($totalQty) = $tmp_result[0];
            }
            
            // # get written-off qty ##
            $sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE RecordStatus=1 AND ItemID = '" . $item_now['ItemID']."'";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($written_off_qty) = $tmp_result[0];
            }
            $ini_total_qty = $totalQty + $written_off_qty;
            
            if ($ini_total_qty > 0)
                $AVG_UNIT_PRICE[$item_now['ItemID']] = round($total_purchase_price / $ini_total_qty, 2);
        }
        
        if (trim($item_now['ItemDescription']) == "") {
            $item_now['ItemDescription'] = "--";
        }
        
        if ($ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['PurchaseDate'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['InvoiceNo'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['SerialNumber'] = $symbol_for_null;
        }
        if ($ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] == "") {
            $ItemOtherInfo[$item_now['ItemID']]['LicenseModel'] = $symbol_for_null;
        }
        
        $Cost_Ary = array();
        if ($item_now['ItemType'] == ITEM_TYPE_BULK) {
            $ItemTotal = 0;
            if ($ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] == "") {
                $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $AVG_UNIT_PRICE[$item_now['ItemID']];
            }
            
            $ItemTotal = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] * $item_now['WriteOffQty'];
            $ItemTotalSum += $ItemTotal;
            $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'] = $ItemOtherInfo[$item_now['ItemID']]['UnitPrice'];
            
            $Cost_Ary[$thisFundingType] = $ItemTotal;
            // $ItemTotal = number_format($ItemTotal,2);
        } else {
            $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice1'];
            if ($ItemOtherInfo[$item_now['ItemID']]['FundingType2'])
                $Cost_Ary[$ItemOtherInfo[$item_now['ItemID']]['FundingType2']] += $ItemOtherInfo[$item_now['ItemID']]['UnitPrice2'];
        }
        
        if ($thisFundingSrcName == "") {
            $thisFundingSrcName = $symbol_for_null;
        }
    }
}

?>

<script language="javascript">

function submitenter(myfield,e)
{
var keycode;
if (window.event) keycode = window.event.keyCode;
else if (e) keycode = e.which;
else return true;

if (keycode == 13)
{
myfield.form.submit();
return false;
}
else
return true;
}

function check_form()
{
	var obj = document.form1;

	var date_from = obj.PurchaseDateFrom.value;
	var date_to = obj.PurchaseDateTo.value;
	var groupby = obj.groupBy.value;

	if(obj.targetFileType.value == 1)
	{
		obj.action = "write_off_item_print_cust.php?date_from="+date_from+"&date_to="+date_to+"&groupBy="+groupby;
		obj.target='intranet_popup10';
        newWindow('about:blank', 10);
        obj.value = 0;
		return true;
	}
	if(obj.targetFileType.value == 2)
	{
		obj.action = "write_off_item_export_cust.php?date_from="+date_from+"&date_to="+date_to;
		obj.value = 0;
		return true;
	}
	obj.value = 0;
	obj.action = "";
// 	return false;
	return false;
}

</script>


<form name="form1" action="" method="post"
	onSubmit="return check_form()">

<? if($groupLeaderNowAllowWriteOffItem) {?> 
<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Remark']?></legend>
		<?=$Lang['eInventory']['DoNotAllowGroupLeaderWriteOffItem']?>
</fieldset>
<? } ?>

<form name="form1" action="" method="post" onSubmit="return checkForm()" ">
	<br>
	<br>
	<table border="0" width="90%" cellpadding="0" cellspacing="5"
		align="center">
<?=$table_selection;?>
</table>
	<table border="0" width="90%" cellpadding="0" cellspacing="5"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr>
			<td align="center">
	<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "document.form1.value=1;")?>
</td>
		</tr>
	</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td align="left">
		
												<br style="clear: both">
		
		</td>
			<td valign="bottom" align="right">
		</td>
		</tr>
	</table>

</form>

<br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>