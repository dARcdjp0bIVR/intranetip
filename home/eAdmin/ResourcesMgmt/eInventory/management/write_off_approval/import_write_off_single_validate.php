<?php
// modifying : 
/*
 * 	Log
 * 
 * 	2016-01-19 Cameron
 * 		create this file
 * 
 */

# this page only handle import of single item
# format = 1 Single Item
# format = 2 Bulk Items 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
if($format == "" || $format == 2)
{
	header("Location: import_write_off_item.php?returnMsgKey=WrongFileFormat&format=1");
	exit();
}
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();


$CurrentPage	= "Management_WriteOffApproval";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$limport 		= new libimporttext();
$li 			= new libdb();
$lo 			= new libfilesystem();

// $_FILES is an array storing name, type, tmp_name, error and size
$filepath 		= $itemfile;			// tmp_name
$filename 		= $itemfile_name;		// original file name

$infobar .= "<tr><td colspan=\"2\" class=\"navigation_v30\">
				<a href=\"import_write_off_item.php\">".$Lang['eInventory']['ImportWriteoffItem']."</a>
				<span>".$i_InventorySystem_ItemType_Single."</span>
			</td></tr>\n"; 

$file_format = array("Item Code","Item Chinese Name","Item English Name","Write-off Reason","Request Date","Approve Date","Remarks");

	
# Create temp single item table
$sql = "DROP TABLE IF EXISTS TEMP_INVENTORY_WRITE_OFF_ITEM";
$result = $li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_WRITE_OFF_ITEM
			(
			ItemType int(11),
			NameChi varchar(255),
			NameEng varchar(255),
			ItemCode varchar(100),			 
			LocationLevelCode varchar(10),
			LocationCode varchar(10),
			GroupInChargeCode varchar(10),
			FundingSourceCode varchar(10),
			LocationLevelID int(11),
			LocationID int(11),
			GroupInChargeID int(11),
			FundingSourceID int(11),
			Quantity int(11),
			WriteOffReason varchar(255),
			RequestDate date,
			ApproveDate date,		
			ItemRemark text
		    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$result = $li->db_db_query($sql);

$ext = strtoupper($lo->file_ext($filename));

if($ext != ".CSV" && $ext != ".TXT")
{
	
	header("location: import_write_off_item.php?returnMsgKey=WrongFileFormat&format=1");
	exit();
}

if($limport->CHECK_FILE_EXT($filename))
{
	# read file into array
	# return 0 if fail, return csv array if success
	$data = $limport->GET_IMPORT_TXT($filepath);
	$col_name = array_shift($data);		# drop the title bar
	
	# check the csv file's first row is correct or not
	$format_wrong = false;
	for($i=0,$iMax=count($file_format); $i<$iMax; $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}
	if($format_wrong)
	{
		header("location: import_write_off_item.php?returnMsgKey=ImportUnsuccess_IncorrectHeaderFormat&format=1");
		exit();
	}
	
	### Remove Empty Row in CSV File ###
	for($i=0,$iMax=count($data); $i<$iMax; $i++)
	{
		if(count($data[$i])!=0)
		{
			$arr_new_data[] = $data[$i];
		}
	}
	
	###########################################################################
	// Start validating data
	$file_original_row = count($arr_new_data);

	$item_code_array = array();
	for($i=0,$iMax=count($arr_new_data); $i<$iMax; $i++)
	{
		$item_code_array[] = trim($linventory->Get_Safe_Sql_Query($arr_new_data[$i][0]));
	}
	
	if (count($item_code_array) > 0) {
		# Get existing single item that match the code 
		$sql = "SELECT ItemID, ItemCode	FROM INVENTORY_ITEM WHERE ItemType = '".ITEM_TYPE_SINGLE."' AND ItemCode IN ('".implode("','",$item_code_array)."')";
		$arr_exist_item = $linventory->returnArray($sql);
		
		### RecordStatus = 2 -> Rejected
		### the checking list should allow rejected record to request write-off again, hence, waiting for approval(0) and approved (1) are banned 
		$sql = "SELECT DISTINCT i.ItemID, i.ItemCode FROM INVENTORY_ITEM i " .
				"INNER JOIN INVENTORY_ITEM_WRITE_OFF_RECORD w ON w.ItemID=i.ItemID " .
				"WHERE i.ItemType = '".ITEM_TYPE_SINGLE."' AND w.RecordStatus<>'2' AND i.ItemCode IN ('".implode("','",$item_code_array)."')";
		$arr_exist_write_off_item = $linventory->returnArray($sql);
	}
	else {
		header("location: import_write_off_item.php?returnMsgKey=ImportUnsuccess_NoRecord&format=1");
		exit();
	}
		
	## write-off reason
	$sql = "SELECT ReasonTypeNameEng, ReasonTypeNameChi FROM INVENTORY_WRITEOFF_REASON_TYPE";
	$rs = $linventory->returnArray($sql);
	$write_off_reason_array = array();
	for($i=0,$iMax=count($rs); $i<$iMax; $i++) {
		$write_off_reason_array[] = $rs[$i]['ReasonTypeNameEng'];
		$write_off_reason_array[] = $rs[$i]['ReasonTypeNameChi'];
	}
		
	$validRow = 0;
	$invalidRow = 0;	
	$rowNo = '';
	$delim = '';
	$table_content = '';		
	$exist_item = array();
	$exist_write_off_item = array();
	$error_code = array();
	$error_css = "style=\"color:red\"";
	
	for($i=0,$iMax=count($arr_exist_item); $i<$iMax; $i++)
	{
		list($exist_item_id, $exist_item_code) = $arr_exist_item[$i];
		$exist_item[$exist_item_code]['ItemID'] = $exist_item_id;
	}

	for($i=0,$iMax=count($arr_exist_write_off_item); $i<$iMax; $i++)
	{
		list($exist_item_id, $exist_item_code) = $arr_exist_write_off_item[$i];
		$exist_write_off_item[$exist_item_code]['ItemID'] = $exist_item_id;
	}

	for($i=0,$iMax=count($arr_new_data); $i<$iMax; $i++)
	{
		$error = false;
		list($item_code,$item_chi_name,$item_eng_name,$item_write_off_reason,$item_request_date,$item_approve_date,$item_remark) = $arr_new_data[$i];
		if(!isset($exist_item[$item_code]['ItemID']) || $exist_item[$item_code]['ItemID']=="") {
			$error_code[$i][] = 1;		// Item does not exist
			$error = true;
		} 

		if (trim($item_write_off_reason) == '') {
			$error_code[$i][] = 2;		// Write-off reason is empty
			$error = true;
		}
		else {
			if (!in_array($item_write_off_reason,(array)$write_off_reason_array)) {
				$error_code[$i][] = 7;		// Write-off reason not in list
				$error = true;
			}
		}

		if(!checkDateIsValid(getDefaultDateFormat($item_request_date)))
		{
			$error_code[$i][] = 3;		// Request Date is empty or invalid
			$error = true;
		}

		if(!checkDateIsValid(getDefaultDateFormat($item_approve_date)))
		{
			$error_code[$i][] = 4;		// Approve Date is empty or invalid
			$error = true;
		}
				
		if (isset($exist_write_off_item[$item_code]['ItemID']) && $exist_write_off_item[$item_code]['ItemID']!="") {
			$error_code[$i][] = 5;		// Write-off record already exist
			$error = true;
		}
			
		for($jMax=count($arr_new_data),$j=$jMax-1; $j>$i; $j--)
		{
			if ($arr_new_data[$j][0] == $item_code) {
				$error_code[$i][] = 6;	// Write-off record already exist
				$error = true;
			} 	
		}
			
		if ($error) {						
			$invalidRow++;	
			$rowNo .= $delim.($i+1);
			$delim = ", ";
		}
		else {
			$validRow++;
			
			// add valid data to db
			$item_chi_name = addslashes(intranet_htmlspecialchars($item_chi_name));
			$item_eng_name = addslashes(intranet_htmlspecialchars($item_eng_name));
			$item_code = intranet_htmlspecialchars(addslashes($item_code));
			$item_write_off_reason = intranet_htmlspecialchars(addslashes($item_write_off_reason));
			$item_request_date = getDefaultDateFormat($item_request_date);
			$item_approve_date = getDefaultDateFormat($item_approve_date);
			$item_remark = intranet_htmlspecialchars(addslashes($item_remark));

			$importAry[] = " (1,'".$item_chi_name."', '".$item_eng_name."', '".$item_code."', 1, '".$item_write_off_reason."', '".
							$item_request_date."', '".$item_approve_date."', '".$item_remark."') ";
		}
	}		// loop number of rows in data

	if (count($importAry) > 0) {
		$field = "ItemType, NameChi, NameEng, ItemCode, Quantity, WriteOffReason, RequestDate, ApproveDate, ItemRemark";
		$insertChunkAry = array_chunk($importAry, 1000);
		$numOfChunk = count($insertChunkAry);
		
		for ($i=0; $i<$numOfChunk; $i++) {
			$_insertAry = $insertChunkAry[$i];
			$sql = "INSERT INTO TEMP_INVENTORY_WRITE_OFF_ITEM ($field) VALUES ".implode(', ', $_insertAry);
			$result = $linventory->db_db_query($sql);
		}
	}
		
	
	if($invalidRow>0) {
		$invalidRowMsg = $Lang['eInventory']['RowLine1']." ".$rowNo." ".$Lang['eInventory']['RowLine2']." ".$Lang['eInventory']['InvalidRecordRow'];
		$invalidRowMsg = $Lang['eInventory']['InvalidRecord']." : <span $error_css> $invalidRow ($invalidRowMsg)</span>";
	} 
	
	$table_content .= "<tr>";
	$table_content .= "<td class=\"tabletext\" colspan=\"".($invalidRow > 0 ? '9':'8')."\">
							$i_InventorySystem_ImportItem_TotalRow : $file_original_row<br>
							".$Lang['eInventory']['ValidRecord']." : $validRow<br>
							$invalidRowMsg							
						</td>";
	$table_content .= "</tr>\n";
	
	$table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">#</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseName</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishName</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_Reason</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Write_Off_RequestTime</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_ApproveDate</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
	if($invalidRow>0)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
	}
	$table_content .= "</tr>\n";
	
	for($i=0,$iMax=count($arr_new_data); $i<$iMax; $i++)
	{
		$item_code_css = '';
		$write_off_reason_css = '';
		$request_date_css = '';
		$approve_date_css = '';
		$error_cell = '';
		
		list($item_code,$item_chi_name,$item_eng_name,$item_write_off_reason,$item_request_date,$item_approve_date,$item_remark) = $arr_new_data[$i];

		if($invalidRow>0) {
			$item_error = "";
			if (count($error_code[$i]) > 0) {
				$delimiter = "- ";
				for ($j=0,$jMax=count($error_code[$i]);$j<$jMax;$j++) {
					$item_error .= $delimiter . $Lang['eInventory']['ImportWriteoffError'][$error_code[$i][$j]];
					$delimiter = "<br>- ";
					
					switch ($error_code[$i][$j]) {
						case 1:						
						case 5:
						case 6:
							$item_code_css = $error_css;
							break;
						case 2:
							$write_off_reason_css = $error_css;
							break;
						case 3:
							$request_date_css = $error_css;
							break;
						case 4:
							$approve_date_css = $error_css;
							break;
					}
				}
			}
			$error_cell = "<td class=\"tabletext\">$item_error</td>";
		}
		
		$j=$i+1;
		$table_row_css = " class=\"tablerow".(($j%2 == 0) ? "1":"2")."\" ";

		$table_content .= "<tr $table_row_css><td class=\"tabletext\">$j</td>";
		$table_content .= "<td class=\"tabletext\" $item_code_css>".(empty($item_code)? "***" : $item_code)."</td>";
		$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
		$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
		$table_content .= "<td class=\"tabletext\" $write_off_reason_css>".(empty($item_write_off_reason)? "***" : $item_write_off_reason)."</td>";
		$table_content .= "<td class=\"tabletext\" $request_date_css>".(empty($item_request_date)? "***" : $item_request_date)."</td>";
		$table_content .= "<td class=\"tabletext\" $approve_date_css>".(empty($item_approve_date)? "***" : $item_approve_date)."</td>";
		$table_content .= "<td class=\"tabletext\">$item_remark</td>";
		$table_content .= $error_cell;
		$table_content .= "</tr>";
	}
	$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"100%\"></td></tr>\n";
	$table_content .= "<tr><td colspan=100% align=center>";
	if($invalidRow<=0) {
		$table_content .= $linterface->GET_ACTION_BTN($button_submit, "submit", "");
	}
	$table_content .= " ".$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_write_off_item.php?format=1'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
						  "</td></tr>\n";
}
$TAGS_OBJ[] = array($button_import, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

### step display
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['eInventory']['ImportStep'][2], 1);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);

?>

<br>

<form name="form1" action="import_write_off_item_update.php" method="post">
<table border="0" width="99%" cellspacing="0" cellpadding="0">
<?=$infobar?>
</table>
<br>
<table border="0" width="96%" cellspacing="0" cellpadding="5">
	<tr>
		<td colspan="<?=($invalidRow > 0 ? '9':'8')?>"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
<?=$table_content;?>
</table>
<input type="hidden" name="format" id="format" value=<?=$format;?>>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
