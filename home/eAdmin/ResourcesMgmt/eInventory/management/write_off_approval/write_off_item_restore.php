<?php
# using: 
################################
#	Date	:	2017-08-29 (Henry)
#				fixed: double submission when double press enter key [Case#Z122974]
#
#	Date:	2016-02-04 (Henry)
#			fixed: php 5.4 issue move "if(!isset($RecordID)){}" after includes file 
#	
#	Date:	2013-07-25	YatWoon
#			add "restore" function
#
################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");

if(!isset($RecordID))
{
	header("location: write_off_item.php");
	exit;
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory		= new libinventory();

$arr_success_rec = Array();
$curr_date = date("Y-m-d");

// $hv_pending_reqeust = 0;

if($RecordID != "")
{
	$record_list = implode(",",$RecordID);
	
	$sql = "SELECT 
					b.ItemType,
					a.RecordID,
					a.ItemID, 
					a.LocationID,
					a.AdminGroupID,
					a.WriteOffQty,
					a.FundingSourceID,
					a.RecordStatus
			FROM 
					INVENTORY_ITEM_WRITE_OFF_RECORD AS a INNER JOIN
					INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID)
			WHERE 
					a.RecordID IN ($record_list) AND a.RecordStatus <> 0
			";
	$arr_target_item = $linventory->returnArray($sql);
	
	foreach($arr_target_item as $k => $data)
	{
		list($this_ItemType, $this_RecordID, $this_ItemID, $this_LocationID, $this_AdminGroupID, $this_WriteOffQty, $this_FundingSourceID, $this_RecordStatus) = $data;
		
		if($this_ItemType==1)		##### Single
		{
			# check is there any writeoff request is pending for the same item (bulk item need to check with location, group and funding)
			$sql = "select count(*) from INVENTORY_ITEM_WRITE_OFF_RECORD where RecordStatus=0 and ItemID=$this_ItemID";
			$result = $linventory->returnVector($sql);
			if($result[0]>0)
			{
// 				$hv_pending_reqeust = 1;
				continue;	
			}
				
			if($this_RecordStatus==1)
			{
				# add history log
				$sql = "INSERT INTO INVENTORY_ITEM_SINGLE_STATUS_LOG
							(ItemID, RecordDate, Action, PersonInCharge, DateInput, DateModified)
						VALUES
							($this_ItemID, '$curr_date', ".ITEM_ACTION_WRITEOFF_RESTORE.", $UserID, NOW(), NOW())
						";
				$linventory->db_db_query($sql);
			}
			
			# single item - update INVENTORY_ITEM RecordStatus
			$sql = "update INVENTORY_ITEM set RecordStatus=1 where ItemID=$this_ItemID";
			$linventory->db_db_query($sql);
				
			# update INVENTORY_ITEM_WRITE_OFF_RECORD RecordStatus to Pending
			$sql = "update INVENTORY_ITEM_WRITE_OFF_RECORD set RecordStatus=0 where RecordID=$this_RecordID";
			$linventory->db_db_query($sql);	
		}
		else				##### Bulk
		{
			# check Location, Funding, Group first
			if($this_LocationID && $this_AdminGroupID && $this_FundingSourceID)
			{
				# check is there any writeoff request is pending for the same item (bulk item need to check with location, group and funding)
				$sql = "select count(*) from INVENTORY_ITEM_WRITE_OFF_RECORD where RecordStatus=0 and ItemID=$this_ItemID";
				$sql .= " and LocationID=$this_LocationID and AdminGroupID=$this_AdminGroupID and FundingSourceID=$this_FundingSourceID";
				$result = $linventory->returnVector($sql);
				if($result[0]>0)
				{
// 					$hv_pending_reqeust = 1;
					continue;	
				}
					
				# bulk item - add back qty if original record status is "approved"
				if($this_RecordStatus==1)
				{
					# add history log
					$sql = "INSERT INTO INVENTORY_ITEM_BULK_LOG 
								(ItemID, RecordDate, Action, QtyChange, PersonInCharge, LocationID, GroupInCharge, FundingSource, DateInput, DateModified)
							VALUES
								($this_ItemID, '$curr_date', ".ITEM_ACTION_WRITEOFF_RESTORE.", $this_WriteOffQty, $UserID, $this_LocationID, $this_AdminGroupID, $this_FundingSourceID, NOW(), NOW())";
					$linventory->db_db_query($sql);
				
					# location qty
					# check location is exists or not
					$sql = "select RecordID from INVENTORY_ITEM_BULK_LOCATION where ItemID=$this_ItemID and LocationID=$this_LocationID and GroupInCharge=$this_AdminGroupID and FundingSourceID=$this_FundingSourceID";
					$result = $linventory->returnVector($sql);
					if(empty($result))	# insert record
					{
						$sql = "insert into INVENTORY_ITEM_BULK_LOCATION 
								(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity) 
								values 
								($this_ItemID, $this_AdminGroupID, $this_LocationID, $this_FundingSourceID, $this_WriteOffQty)";
					}
					else				# update qty
					{
						$sql = "update INVENTORY_ITEM_BULK_LOCATION set Quantity = Quantity+$this_WriteOffQty where RecordID=". $result[0];
					}
					$linventory->db_db_query($sql);
					
					# total qty	
					$sql = "update INVENTORY_ITEM_BULK_EXT set Quantity = Quantity+$this_WriteOffQty where ItemID=$this_ItemID";
					$linventory->db_db_query($sql);
					
					# udpate item status qty
					$sql = "select QtyNormal, QtyDamage, QtyRepair from INVENTORY_ITEM_BULK_LOG where (Action=". ITEM_ACTION_UPDATESTATUS ." or Action=". ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING ." ) and ItemID=$this_ItemID and LocationID=$this_LocationID and FundingSource=$this_FundingSourceID and GroupInCharge=$this_AdminGroupID order by RecordID desc limit 0, 1";
					$result = $linventory->returnArray($sql);
					if(!empty($result))
					{
						$QtyNormal = $result[0]['QtyNormal'];
						$QtyDamage = $result[0]['QtyDamage'];
						$QtyRepair = $result[0]['QtyRepair'];
					}
					$sql = "INSERT INTO
							INVENTORY_ITEM_BULK_LOG
							(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, PersonInCharge, 
							LocationID, GroupInCharge, FundingSource, DateInput, DateModified)
					VALUES
							($this_ItemID, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",". ($QtyNormal+$this_WriteOffQty) .",$QtyDamage,$QtyRepair,$UserID,
							$this_LocationID,$this_AdminGroupID,$this_FundingSourceID,NOW(),NOW())
					";
					$linventory->db_db_query($sql);
					# udpate item status qty - end
				}
				
				# update INVENTORY_ITEM_WRITE_OFF_RECORD RecordStatus to Pending
				$sql = "update INVENTORY_ITEM_WRITE_OFF_RECORD set RecordStatus=0 where RecordID=$this_RecordID";
				$linventory->db_db_query($sql);
			}
			else
			{
				$x = "14";
			}
			
		}
	}
}
else
{
	$x = "14";
}

intranet_closedb();
header("location: write_off_item.php?RecordStatus=3&keyword=$keyword&msg=$x");
?>