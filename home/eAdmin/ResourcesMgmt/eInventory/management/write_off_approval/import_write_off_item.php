<?php
# modifying : 
/*
 * 	Log
 * 
 *  2016-07-07 [Henry] fixed duplicate click submit button
 * 	2016-01-18 [Cameron] crete this file
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$linventory		= new libinventory();

$temp[] = array("<a href=\"write_off_item.php\">$i_InventorySystem_WriteOffItemApproval</a>");
$infobar1 .= "<tr><td class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>";
 
$CurrentPage	= "Management_WriteOffApproval";
$TAGS_OBJ[] = array($button_import, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$returnMsgKey = standardizeFormGetValue($_GET['returnMsgKey']);

//$PAGE_NAVIGATION[] = array($Lang['eInventory']['ImportWriteoffItem']);
//$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

### step display
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['SelectCSVFile'], 1);
$STEPS_OBJ[] = array($Lang['eInventory']['ImportStep'][2], 0);
$STEPS_OBJ[] = array($Lang['General']['ImportArr']['ImportStepArr']['ImportResult'], 0);


# single item format
$single_format_insert = "";
$single_mandatory_index = array(1,4,5,6);
for($i=1,$iMax=count($Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single']); $i<=$iMax; $i++)
{ 
	if (in_array($i,$single_mandatory_index)) {
		$single_format_insert .= "<span class='tabletextrequire'>*</span>";
	}
	$single_format_insert .= $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][$i]['FieldTitle'];
	
	if($Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][$i]['AjaxLinkTitle'] !="")
	{
//		$remark_icon = "&nbsp;<a href='javascript:showInfo(1, \"". $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][$i]['AjaxLink'] ."\")'><img id='img_". $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][$i]['AjaxLink'] ."' src='".$image_path."/".$LAYOUT_SKIN."/inventory/icon_help.gif' border=0 alt=".$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][$i]['AjaxLinkTitle']."></a>";
		$remark_icon = "&nbsp;<a href='javascript:viewCodes(\"". $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][$i]['AjaxLink'] ."\",1)' class='tablelink'>[".$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Single'][$i]['AjaxLinkTitle']."]</a>";
		$single_format_insert .= $remark_icon;
	}
	$single_format_insert .= "<BR>";
}


# bulk item format
$bulk_format_insert = "";
$bulk_mandatory_index = array(1,4,5,6,7,8,9,10,11);
for($i=1,$iMax=count($Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk']); $i<=$iMax; $i++)
{
	if (in_array($i,$bulk_mandatory_index)) {
		$bulk_format_insert .= "<span class='tabletextrequire'>*</span>";
	}
	
	$bulk_format_insert .= $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][$i]['FieldTitle'];
	
	if($Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][$i]['AjaxLinkTitle'] !="")
	{
//		$remark_icon = "&nbsp;<a href='javascript:showInfo(2, \"". $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][$i]['AjaxLink'] ."\")'><img id='img_". $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][$i]['AjaxLink'] ."' src='".$image_path."/".$LAYOUT_SKIN."/inventory/icon_help.gif' border=0 alt=".$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][$i]['AjaxLinkTitle']."></a>";
		$remark_icon = "&nbsp;<a href='javascript:viewCodes(\"". $Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][$i]['AjaxLink'] ."\",2)' class='tablelink'>[".$Lang['eInventory']['FieldTitle']['ImportWriteoff']['Bulk'][$i]['AjaxLinkTitle']."]</a>";
		$bulk_format_insert .= $remark_icon;
	}
	$bulk_format_insert .= "<BR>";
}


?>
<script language="Javascript" src='/templates/tooltip.js'></script>
<style type="text/css">
     #ToolMenu{position:absolute; top: 0px; left: 0px; z-index:4; visibility:show;}
</style>
<!--
<div id="ToolMenu" style="position:absolute; width=0px; height=0px; visibility:hidden"></div>
-->
<!--<iframe id='lyrShim2'  scrolling='no' frameborder='0' style='position:absolute; top:0px; left:0px; visibility:hidden; display:none;'></iframe>
<div id="ToolMenu2" style="position:absolute; width=0px; height=0px; visibility:hidden; z-index:999999"></div>
-->
<script type="text/javascript" src="/templates/jquery/thickbox.js"></script>
<link rel="stylesheet" href="/templates/jquery/thickbox.css" type="text/css" media="screen" />

<script language="javascript">
<!--
//var imgObj=0;
//var callback = {
//        success: function ( o )
//        {
//                writeToLayer('ToolMenu2',o.responseText);
//                showMenu2("img_"+imgObj,"ToolMenu2");
//        }
//}
//function hideMenu2(menuName){
//
//		objMenu = document.getElementById(menuName);
//		if(objMenu!=null)
//			objMenu.style.visibility='hidden';
//		setDivVisible(false, menuName, "lyrShim2");
//}
//function showMenu2(objName,menuName)
//{
//  	hideMenu2('ToolMenu2');
//	objIMG = document.getElementById(objName);
//	
//	offsetX = -300;
//	offsetY = 20;    
//	 	
//    var pos_left = getPostion(objIMG,"offsetLeft");
//	var pos_top  = getPostion(objIMG,"offsetTop");
//	objDiv = document.getElementById(menuName);
//	if(objDiv!=null){
//		objDiv.style.visibility='visible';
//		objDiv.style.top = pos_top+offsetY+"px";
//		objDiv.style.left = pos_left+offsetX+"px";
//		setDivVisible(true, menuName, "lyrShim2");
//	}
//}
//function showInfo(type,val)
//{
//    obj = document.form1;
//
//    var myElement = document.getElementById("ToolMenu2");
//
//    writeToLayer('ToolMenu2','');
//    imgObj = val;
//        
//	showMenu2("img_"+val,"ToolMenu2");
//	
//	YAHOO.util.Connect.setForm(obj);
//
//    var path = "../inventory/retrieveInfo.php?Type="+type+"&Val=" + val;
//    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
//}

function viewCodes(CodeType, ItemType)
{
	var CaptionTitle;
	switch (CodeType) {
		case "ItemCode":
			CaptionTitle = "<?=$i_InventorySystem_Item_Code?>";
			break;
		case "WriteOffReason":
			CaptionTitle = "<?=$i_InventorySystem['WriteOffReason']?>";
			break;
		case "LocationCode":
			CaptionTitle = "<?=$i_InventorySystem_Item_LocationCode?>";
			break;
		case "SubLocationCode":
			CaptionTitle = "<?=$i_InventorySystem_Item_Location2Code?>";
			break;
		case "FundingCode":
			CaptionTitle = "<?=$i_InventorySystem_Item_FundingSourceCode?>";
			break;
		case "GroupCode":
			CaptionTitle = "<?=$i_InventorySystem_Item_GroupCode?>";
			break;
	}
	tb_show(CaptionTitle, "view_codes.php?&CodeType="+CodeType+"&ItemType="+ItemType+"&KeepThis=true&height=500&width=680"); 
}

function checkForm() {
	document.getElementById('submitbtn').disabled  = true; 
	var obj = document.form1;

	if(obj.itemfile.value == "") {
		alert("<?=$Lang['eInventory']['PleaseSelectFile']?>");
		obj.itemfile.select();
		document.getElementById('submitbtn').disabled  = false; 
	} else {
		
		if ($('#format1').attr('checked')) { 
			obj.action = "import_write_off_single_validate.php";
			obj.submit();
		} else if($('#format2').attr('checked')) {
			obj.action = "import_write_off_bulk_validate.php";
			obj.submit();
		} else{
			document.getElementById('submitbtn').disabled  = false; 
		}
		
	} 
}


function click_item_type()
{
	obj = document.form1;
	var sample_link = document.getElementById('import_csv'); 
	var csv_format = document.getElementById("csv_format");
	
	if(obj.format[0].checked)	// Single item
	{
		sample_link.href = "<?= GET_CSV("sample_import_write_off_single_item.csv")?>";
		csv_format.innerHTML = "<?=addslashes($single_format_insert)?>";
	}	
	else							// Bulk item
	{
		sample_link.href = "<?= GET_CSV("sample_import_write_off_bulk_item.csv")?>";
		csv_format.innerHTML = "<?=addslashes($bulk_format_insert)?>";
	}
}


-->
</script>
<br>

<form name="form1" action="" method="POST" enctype="multipart/form-data">
<table border="0" width="96%" cellpadding="5" cellspacing="0">
<?=$infobar1?>
	<tr>
    	<td>
		    <table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td colspan="2"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
				</tr>
		
				<tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_InventorySystem['item_type']?></td>
		            <td class="tabletext">
		                <?=$linterface->Get_Radio_Button("format1", "format", "1",($format=='' || $format==1)? 1:0,"",$i_InventorySystem_ItemType_Single, "click_item_type()");?>
		                <?=$linterface->Get_Radio_Button("format2", "format", "2",($format==2)? 1:0,"",$i_InventorySystem_ItemType_Bulk, "click_item_type()");?>
					</td> 
		        </tr>
		            
		        <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_select_file?></td>
		            <td class="tabletext" width="70%"><input class="file" type="file" name="itemfile"></td>
		        </tr>    
		            
				<tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['General']['CSVSample']?></td>
		            <td class="tabletext" width="70%"><a class="tablelink" id="import_csv" href="">
		            <img src="<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif" border="0" align="absmiddle" hspace="3">
		            <?=$i_general_clickheredownloadsample?></a>
		            </td>
		        </tr>                
		        
		        <tr>
			    	<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_general_Format?></td>
		            <td class="tabletext" width="70%" border="0">
						<table width="100%" border="0" cellpadding="0" cellspacing="0" align="right" >
							<tr>
								<td valign="top"><span id="csv_format"></span></td>
								<td><span id="ToolMenu2xxxx"></span></td>
							</tr>
						</table>
					</td>
		        </tr>
		        
				<tr>
					<td colspan="2" align="left" class="tabletextremark">
						<?=$Lang['General']['RequiredField']?>
					</td>
				</tr>
		        
			</table>
		</td>
	</tr>

	<tr>
    	<td>
		    <table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    	<tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
		    </table>
		    
	    </td>
	</tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "button", "checkForm()", "submitbtn") ?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='write_off_item.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>

</form>

<script language="javascript">
<!--
click_item_type();
//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
