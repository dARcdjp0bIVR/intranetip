<?php
// modifying :
/**
 * **************************** Change Log **************************************************
 * 2018-03-19 (Isaac): add txt tamplates for single item and bulk item
 * 
 * 2018-02-07 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log ************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_Stocktake";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Online']['Title'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/index.php",
    0
);
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Title'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/offline.php",
    1
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

if ($xmsg != "") {
    $returnMsg = $xmsg;
}

$linterface->LAYOUT_START($returnMsg);

$STEPS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step1'],
    1
);
$STEPS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step2'],
    0
);
$STEPS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step3'],
    0
);

$csvFile = "<a href='offline_stocktake_application.AGX'>" . $Lang['eInventory']['FieldTitle']['DownloadOfflineReaderApplication'] . "</a>";
$singleItemTemplateTxt = "<a href='offline_stocktake_single_item_template.txt' download>" . $Lang['eInventory']['FieldTitle']['DownloadofflineStocktakeSingleItemTemplate'] ."</a>";
$bulkItemTemplateTxt = "<a href='offline_stocktake_bulk_item_template.txt' download>" . $Lang['eInventory']['FieldTitle']['DownloadofflineStocktakeBulkItemTemplate'] . "</a>";
?>

<br>

<form name="form1" action="offline_import_confirm.php" method="POST"
	enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
		<tr>
			<td colspan='2'>
				<table width='90%' border='0' cellpadding='5' cellspacing='0'
					align='center'>
					<tr>
						<td class='formfieldtitle' align='left'><?=$Lang['General']['SourceFile']?><span
							class='tabletextremark'><?=$Lang['General']['CSVFileFormat']?></span></td>
						<td class='tabletext'><input class='file' type='file'
							name='csvfile' id='csvfile'></td>
					</tr>
					<tr>
						<td class='formfieldtitle' align='left'><?=$Lang['General']['CSVSample']?></td>
						<td class='tabletext'><?=$csvFile?></br><?=$singleItemTemplateTxt?></br><?=$bulkItemTemplateTxt?></td>
					</tr>
					<tr>
						<td class='formfieldtitle' align='left'><?=$i_InventorySystem['Format']?></td>
						<td class='tabletext'>
							<table>
								<tr><td><b><?= $Lang['eInventory']['FieldTitle']['ForSingleItem'] ?></b></td></tr>
								<?php foreach($Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray']['Single'] as $columnFormat):?>
									<tr><td><?= $columnFormat?></td></tr>
								<?php endforeach;?>
								<tr><td></br></td></tr>
								<tr><td><b><?= $Lang['eInventory']['FieldTitle']['ForBulkItem']?></td></b></tr>
								<?php foreach($Lang['eInventory']['FieldTitle']['OfflineStocktakeFormatArray']['Bulk'] as $columnFormat):?>
									<tr><td><?= $columnFormat?></td></tr>
								<?php endforeach;?>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan='2'>
				<table width='95%' border='0' cellpadding='0' cellspacing='0'
					align='center'>
					<tr>
						<td colspan='3' class='dotline'><img
							src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10'
							height='1' />
							
							</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align='center' colspan='2'>
		<?= $linterface->GET_ACTION_BTN($Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit'], "submit")." ".$linterface->GET_ACTION_BTN($Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Cancel'], "button", "window.location='index.php'")?>
	</td>
		</tr>
	</table>
</form>
<?								
$linterface->LAYOUT_STOP();
intranet_closedb();
?>