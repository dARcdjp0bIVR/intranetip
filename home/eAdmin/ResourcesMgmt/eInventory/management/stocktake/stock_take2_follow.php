<?php

// using: Henry

// ##################################################
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2016-02-01 Henry
// PHP 5.4 fix: change split to explode
//
// Date: 2015-01-05 Henry
// Created this file
//
// #################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

if (($bulk_item_id == "") && ($item_id_list == "") && ($wrong_item_list == "")) {
    header("location: index.php");
}

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");

intranet_auth();
intranet_opendb();

$no_file = 1;

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_Stocktake";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['Stocktake']['Online']['Title'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/index.php",
    1
);
// $TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Title'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/offline.php", 0);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arr_single_upload = array();
$arr_bulk_upload = array();

$infobar .= $linterface->GET_NAVIGATION2($i_InventorySystem_Stocktake_List . " (" . $Lang['eInventory']['FieldTitle']['StocktakeType']['NotDone'] . ")");

// $STEPS_OBJ[] = array($i_InventorySystem_StockCheck_Step1,0);
$STEPS_OBJ[] = array(
    $i_InventorySystem_StockCheck_Step2,
    0
);
$STEPS_OBJ[] = array(
    $i_InventorySystem_StockCheck_Step3,
    1
);

if ($item_id_list != "") {
    $sql = "SELECT
					a.ItemID,
					a.ItemCode,
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					b.TagCode,
					b.GroupInCharge,
					b.LocationID
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
			WHERE
					a.ItemID IN ($item_id_list)
			ORDER BY
					a.ItemCode";
    $arr_single_item = $linventory->returnArray($sql, 6);
    
    $single_table .= "<tr><td colspan=\"5\" class=\"tablename\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
    $single_table .= "<tr class=\"tablename\">";
    $single_table .= "<td class=\"tablename2\" align=\"left\">$i_InventorySystem_ItemType_Single</td>";
    $single_table .= "</tr>\n";
    $single_table .= "</table></td></tr>\n";
    $single_table .= "<tr class=\"tabletop\">";
    $single_table .= "<td class=\"tabletopnolink\" width=\"10px\"></td>";
    $single_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Code</td>";
    $single_table .= "<td class=\"tabletopnolink\" width=\"40%\">$i_InventorySystem_Item_Name</td>";
    $single_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Barcode</td>";
    $single_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Remark</td>";
    // $single_table .= "<td class=\"tabletopnolink\">$i_InventorySystem_RequestWriteOff</td>";
    $single_table .= "</tr>";
    
    if (sizeof($arr_single_item) > 0) {
        for ($i = 0; $i < sizeof($arr_single_item); $i ++) {
            list ($single_item_id, $single_item_code, $single_item_name, $single_item_tag, $single_item_group_id, $single_item_location_id) = $arr_single_item[$i];
            
            // get item past status
            $sql = "SELECT IF(PastStatus = 0, NewStatus, PastStatus) FROM INVENTORY_ITEM_SINGLE_STATUS_LOG WHERE ItemID IN ($single_item_id) ORDER BY DateInput DESC LIMIT 0,1";
            $arr_single_item_past_status = $linventory->returnArray($sql, 1);
            
            if (sizeof($arr_single_item_past_status) > 0) {
                list ($item_past_status_id) = $arr_single_item_past_status[0];
                
                if (($item_past_status_id == 0) || ($item_past_status_id == ""))
                    $item_past_status = " - ";
                if ($item_past_status_id == 1)
                    $item_past_status = $i_InventorySystem_ItemStatus_Normal;
                if ($item_past_status_id == 2)
                    $item_past_status = $i_InventorySystem_ItemStatus_Damaged;
            }
            
            // $j=$i+1;
            // if($j%2 == 0)
            // $table_row_css = " class=\"tablerow1\" ";
            // else
            // $table_row_css = " class=\"tablerow2\" ";
            
            // if(${"hidden_status_$single_item_tag"} == 1)
            if (${"hidden_status_$single_item_id"} == 1) {
                $single_item_found = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_tick.gif\">";
                $single_table .= "<input type=\"hidden\" name=\"single_item_hidden_status_$single_item_id\" value=1>";
                // $disabled = " ";
            }
            if ((${"hidden_status_$single_item_id"} == 0) || (${"hidden_status_$single_item_id"} == "")) {
                $single_item_found = "<img src=\"$image_path/$LAYOUT_SKIN/inventory/icon_question.gif\">";
                $single_table .= "<input type=\"hidden\" name=\"single_item_hidden_status_$single_item_id\" value=0>";
                // $disabled = " DISABLED ";
            }
            
            // $new_status_selection = getSelectByArray($i_InventorySystem_ItemStatus_Array, " name=\"single_item_status_$single_item_id\" $disabled ");
            
            /*
             * # Get Default Write Off Reason #
             * $sql = "SELECT ReasonTypeID, ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE";
             * $arr_write_off_reason = $linventory->returnArray($sql,2);
             * $reason_selection = getSelectByArray($arr_write_off_reason, " name=\"single_item_write_off_reason_$single_item_id\" DISABLED");
             * # End #
             */
            
            $single_table .= "<tr class=\"single\">";
            $single_table .= "<td class=\"tabletext\">$single_item_found</td>";
            $single_table .= "<td class=\"tabletext\">$single_item_code</td>";
            $single_table .= "<td class=\"tabletext\">$single_item_name</td>";
            $single_table .= "<td class=\"tabletext\">$single_item_tag</td>";
            
            // retrieve last remark
            $sql = "select Remark from INVENTORY_ITEM_SINGLE_STATUS_LOG where ItemID=$single_item_id and (Action=2 or Action=4) order by DateModified desc limit 1";
            $result = $linventory->returnVector($sql);
            $this_remark = intranet_htmlspecialchars($result[0]);
            
            $single_table .= "<td class=\"tabletext\" width=\"\">";
            if (${"hidden_status_$single_item_id"} != 1)
                $single_table .= "<b><font color=\"red\">$i_InventorySystem_Stocktake_MissingRemark</font></b>";
                // $single_table .= "<textarea name=\"single_item_remark_$single_item_id\" class=\"textboxtext\" rows=\"2\" cols=\"50\">". $this_remark ."</textarea></td>";
            $single_table .= "<input type=\"text\" name=\"single_item_remark_$single_item_id\" class=\"textboxtext\" value=\"" . $this_remark . "\"></td>";
            
            // // // // // if(${"hidden_status_$single_item_id"} == 1)
            // // // // // {
            // // // // // $single_table .= "<td class=\"tabletext\" width=\"\"><textarea name=\"single_item_remark_$single_item_id\" class=\"textboxtext\" rows=\"2\" cols=\"50\"></textarea></td>";
            // // // // // $single_table .= "<td class=\"tabletext\">
            // // // // // <input type=\"checkbox\" name=\"write_off_item_id[]\" value=\"$single_item_id\" onClick=\"(this.checked)?enabledSingleItemWriteOffReason(0,$single_item_id):enabledSingleItemWriteOffReason(1,$single_item_id)\">&nbsp;$reason_selection <br>
            // // // // // <input type=\"file\" class=\"file\" name=\"single_item_write_off_attachment_$single_item_id\" DISABLED>
            // // // // // ";
            // // // // // $single_table .= "<input type=\"hidden\" name=\"hidden_single_item_write_off_attachment_".$single_item_id."\" value=\"\">";
            // // // // //
            // // // // // /*
            // // // // // $single_table .= "<td class=\"tabletext\">";
            // // // // // $single_table .= "<input type=\"checkbox\" name=\"write_off_item_id[]\" value=\"$single_item_id\" onClick=\"(this.checked)?enabledSingleItemWriteOffReason(0,$single_item_id):enabledSingleItemWriteOffReason(1,$single_item_id)\">&nbsp;$reason_selection <br>";
            // // // // // $single_table .= "
            // // // // // <table border=\"0\" id=\"upload_file_list_$single_item_id\" cellpadding=\"0\" cellspacing=\"0\" >
            // // // // //
            // // // // // <script language=\"javascript\">
            // // // // //
            // // // // // for(i=0;i<no_of_upload_file;i++)
            // // // // // {
            // // // // // document.writeln('<tr><td><input class=\"file\" type=\"file\" name=\"single_item_write_off_attachment_$single_item_id'+'_'+i+'\" size=\"40\" DISABLED>');
            // // // // // document.writeln('<input type=\"hidden\" name=\"hidden_single_item_write_off_attachment_$single_item_id'+'_'+i+'\"></td></tr>');
            // // // // // }
            // // // // // </script>
            // // // // // </table>
            // // // // // <input name=\"addField_$single_item_id\" type=\"button\" value=\" + \" onClick=\"add_field(1, $single_item_id)\" DISABLED>
            // // // // // <input type=\"hidden\" name=\"no_of_file_upload_$single_item_id\" value=\"\">
            // // // // // ";
            // // // // // $single_table .= "</td>";
            // // // // // */
            // // // // //
            // // // // // //$arr_single_upload[] .= "single_item_write_off_attachment_".$single_item_id;
            // // // // // //$arr_single_upload[] .= "hidden_single_item_write_off_attachment_".$single_item_id;
            // // // // // $arr_single_upload[] = $single_item_id;
            // // // // // }
            // // // // // if((${"hidden_status_$single_item_id"} == 0) || (${"hidden_status_$single_item_id"} == ""))
            // // // // // {
            // // // // // $single_table .= "<td class=\"tabletext\" width=\"\">
            // // // // // <b><font color=\"red\">$i_InventorySystem_Stocktake_MissingRemark</font></b>
            // // // // // </td>";
            // // // // // $single_table .= "<td class=\"tabletext\">
            // // // // // </td>";
            // // // // // }
            
            $single_table .= "</tr>\n";
            $single_table .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=\"6\"></td></tr>\n";
            $single_table .= "<input type=\"hidden\" name=\"single_item_group_id_$single_item_id\" value=\"$single_item_group_id\">";
            // henry added
            $single_table .= "<input type=\"hidden\" name=\"single_item_location_id_$single_item_id\" value=\"$single_item_location_id\">";
            $arr_single_item[$i] = $single_item_id;
        }
        $single_item_list = implode(",", $arr_single_item);
        $single_table .= "<input type=\"hidden\" name=\"single_item_list\" value=\"$single_item_list\">";
    } else {
        $single_table .= "<tr class=\"tablerow1\"><td class=\"tabletext\" colspan=\"6\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
    }
    $single_table .= "<tr><td class=\"dotline\" colspan=\"6\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";
    $single_table .= "<tr height=\"10px\"><td></td></tr>\n";
}

$single_unlisted_table .= "<tr><td colspan=\"6\" class=\"tablename\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
$single_unlisted_table .= "<tr class=\"tablename\">";
$single_unlisted_table .= "<td class=\"tablename2\" align=\"left\">$i_InventorySystem_Stocktake_UnlistedItem</td>";
$single_unlisted_table .= "</tr>\n";
$single_unlisted_table .= "</table></td></tr>\n";
$single_unlisted_table .= "<tr class=\"tabletop\">";
$single_unlisted_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Code</td>";
$single_unlisted_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Name</td>";
$single_unlisted_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Barcode</td>";
$single_unlisted_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Stocktake_ExpectedLocation</td>";
$single_unlisted_table .= "<td class=\"tabletopnolink\" width=\"20%\">$i_InventorySystem_Item_Remark</td>";
$single_unlisted_table .= "</tr>\n";
if ($wrong_item_list != "") {
    $arr_unlisted_target = explode(",", $wrong_item_list);
    
    for ($i = 0; $i < sizeof($arr_unlisted_target); $i ++) {
        $unlisted_barcode = $arr_unlisted_target[$i];
        
        $sql = "SELECT 
						a.ItemID, 
						a.ItemCode, 
						" . $linventory->getInventoryItemNameByLang("a.") . ", 
						b.TagCode,
						CONCAT(" . $linventory->getInventoryNameByLang("e.") . ", ' > '," . $linventory->getInventoryNameByLang("d.") . ", ' > ', " . $linventory->getInventoryNameByLang("c.") . ")
				FROM 
						INVENTORY_ITEM AS a INNER JOIN 
						INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
						INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
						INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS e ON (e.BuildingID = d.BuildingID) 
				WHERE 
						b.TagCode = '$unlisted_barcode'
						and a.RecordStatus = 1
						";
        $arr_unlisted_item = $linventory->returnArray($sql);
        if (sizeof($arr_unlisted_item) > 0) {
            for ($j = 0; $j < sizeof($arr_unlisted_item); $j ++) {
                list ($unlisted_item_id, $unlisted_item_code, $unlisted_item_name, $unlisted_item_barcode, $unlisted_item_location) = $arr_unlisted_item[$j];
                $single_unlisted_table .= "<tr class=\"single\">";
                $single_unlisted_table .= "<td class=\"tabletext\">$unlisted_item_code</td>";
                $single_unlisted_table .= "<td class=\"tabletext\">$unlisted_item_name</td>";
                $single_unlisted_table .= "<td class=\"tabletext\">$unlisted_item_barcode</td>";
                $single_unlisted_table .= "<td class=\"tabletext\">$unlisted_item_location</td>";
                $single_unlisted_table .= "<td class=\"tabletext\"><textarea name=\"unlisted_item_remark\" row=\"3\" col=\"50\"></textarea></td>";
                $single_unlisted_table .= "</tr>\n";
                $single_unlisted_table .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=7></td></tr>\n";
                $single_unlisted_table .= "<input type=\"hidden\" name=\"unlisted_item_id[]\" value=\"$unlisted_item_id\">";
            }
        } else {
            $single_unlisted_table .= "<tr class=\"single\">";
            $single_unlisted_table .= "<td class=\"tabletext\"> - </td>";
            $single_unlisted_table .= "<td class=\"tabletext\"> - </td>";
            $single_unlisted_table .= "<td class=\"tabletext\">$unlisted_barcode</td>";
            $single_unlisted_table .= "<td class=\"tabletext\"> - </td>";
            $single_unlisted_table .= "<td class=\"tabletext\">$i_InventorySystem_Stocktake_ItemNotExist</td>";
            $single_unlisted_table .= "</tr>\n";
            $single_unlisted_table .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=7></td></tr>\n";
        }
    }
} else {
    $single_unlisted_table .= "<tr class=\"single\"><td class=\"tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
}

$single_unlisted_table .= "<tr><td class=\"dotline\" colspan=\"6\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>\n";

if ($bulk_item_id != "") {
    $arr_bulk_item_id[] = explode(",", $bulk_item_id);
    
    $check_group_filter = $admin_group_id ? " and b.GroupInCharge=$admin_group_id" : "";
    $sql = "SELECT 
					a.ItemID,
					a.ItemCode,
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					b.Quantity,
					b.GroupInCharge,
					" . $linventory->getInventoryItemNameByLang("g.") . ",
					" . $linventory->getInventoryItemNameByLang("funding.") . ",
					b.FundingSourceID,
					c.Barcode,
					b.LocationID 
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) 
					INNER JOIN INVENTORY_ITEM_BULK_EXT AS c ON a.ItemID=c.ItemID 
					left join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=b.FundingSourceID)
					left join INVENTORY_ADMIN_GROUP AS g ON (g.AdminGroupID=b.GroupInCharge) 
			WHERE
					a.ItemID IN ($bulk_item_id) AND
					b.LocationID IN ($targetLocation) AND 
					b.GroupInCharge IN ($group_list)
					$check_group_filter
			ORDER BY
					a.ItemCode";
    $arr_bulk_item = $linventory->returnArray($sql, 5);
    
    $bulk_table .= "<tr><td colspan=\"10\" class=\"tablename\"><table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
    $bulk_table .= "<tr class=\"tablename\">";
    $bulk_table .= "<td class=\"tablename2\" align=\"left\">$i_InventorySystem_ItemType_Bulk</td>";
    $bulk_table .= "</tr>\n";
    $bulk_table .= "</table></td></tr>\n";
    /*
     * $bulk_table .= "<tr class=\"tabletop\">
     * <td rowspan=\"3\" class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>
     * <td rowspan=\"3\" class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>
     * <td rowspan=\"3\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>
     * <td colspan=\"6\" align=\"center\" class=\"whiteborder tabletopnolink\"><DIV align=\"center\" class=\"tabletopnolink\">$i_InventorySystem_Stocktake_StocktakeQty</DIV></td>
     * </tr>\n
     * <tr class=\"tablerow2\">
     * <td colspan=\"2\" align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_NormalQty</td>
     * <td colspan=\"2\" align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_RequestWriteOffQty</td>
     * <td rowspan=\"2\" align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_TotalQty</td>
     * <td rowspan=\"2\" align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_VarianceQty</td>
     * </tr>\n
     * <tr class=\"tablerow2\">
     * <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Item_Qty</td>
     * <td width=\"10%\" align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Item_Remark</td>
     * <td align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Item_Qty</td>
     * <td width=\"10%\" align=\"center\" bgcolor=\"#CCCCCC\" class=\" whiteborder tabletext\">$i_InventorySystem_Stocktake_ReasonAndAttachment</td>
     * </tr>\n";
     */
    $bulk_table .= "<tr class=\"tabletop\">
					<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>
					<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>
					<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>
					<td class=\"tabletopnolink\">" . $i_InventorySystem['Caretaker'] . "</td>
					<td class=\"tabletopnolink\">" . $i_InventorySystem['FundingSource'] . "</td>
					<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_ExpectedQty</td>
					<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_StocktakeQty</td>
					<td class=\"tabletopnolink\">$i_InventorySystem_Stocktake_VarianceQty</td>
					<td width=\"20%\" class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>
                  </tr>\n";
    if (sizeof($arr_bulk_item) > 0) {
        $arr_bulk_item_count = sizeof($arr_bulk_item);
        for ($i = 0; $i < $arr_bulk_item_count; $i ++) {
            list ($bulk_item_id, $bulk_item_code, $bulk_item_name, $bulk_item_qty, $bulk_item_group_id, $group_name, $funding_name, $funding_id, $bulk_item_barcode, $bulk_item_location_id) = $arr_bulk_item[$i];
            
            if (${"bulk_item_location_" . $bulk_item_id . "_" . $bulk_item_location_id} != $bulk_item_location_id) {
                unset($arr_bulk_item[$i]);
                continue;
            }
            // $j=$i+1;
            // if($j%2 == 0)
            // $table_row_css = " class=\"tablerow1\" ";
            // else
            // $table_row_css = " class=\"tablerow2\" ";
            
            $bulk_item_found = ${"bulk_item_qty_found_" . $bulk_item_id . "_" . $bulk_item_group_id . "_" . $funding_id . "_" . $bulk_item_location_id};
            if ($bulk_item_found == "")
                $bulk_item_found = 0;
                // $bulk_item_write_off = ${"bulk_item_qty_write_off_".$bulk_item_id."_".$bulk_item_group_id.""};
                
            // check any item missing #
                // $bulk_item_diff = ($bulk_item_found + $bulk_item_write_off) - $bulk_item_qty;
            $bulk_item_diff = $bulk_item_found - $bulk_item_qty;
            // end #
            
            // $bulk_item_total_qty = $bulk_item_found + $bulk_item_write_off;
            
            if ($bulk_item_diff == 0) {
                $item_variance_qty = $bulk_item_diff;
            }
            if ($bulk_item_diff > 0) {
                $item_variance_qty = "<font color=\"green\">+" . $bulk_item_diff . "</font>";
            }
            if ($bulk_item_diff < 0) {
                $item_variance_qty = "<font color=\"red\">" . $bulk_item_diff . "</font>";
            }
            
            $bulk_table .= "<tr class=\"bulk\">";
            $bulk_table .= "<td class=\"tabletext\">$bulk_item_code</td>";
            $bulk_table .= "<td class=\"tabletext\">$bulk_item_barcode</td>";
            $bulk_table .= "<td class=\"tabletext\">$bulk_item_name</td>";
            $bulk_table .= "<td class=\"tabletext\">$group_name</td>";
            $bulk_table .= "<td class=\"tabletext\">$funding_name</td>";
            $bulk_table .= "<td class=\"tabletext\">$bulk_item_qty</td>";
            // $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_qty_found_".$bulk_item_id."_".$bulk_item_group_id."\" value=\"$bulk_item_found\">";
            // $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_qty_write_off_".$bulk_item_id."_".$bulk_item_group_id."\" value=\"$bulk_item_write_off\">";
            // $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_change_".$bulk_item_id."_".$bulk_item_group_id."\" value=$bulk_item_diff>";
            $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_qty_found_" . $bulk_item_id . "_" . $bulk_item_group_id . "_" . $funding_id . "_" . $bulk_item_location_id . "\" value=\"$bulk_item_found\">";
            $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_change_" . $bulk_item_id . "_" . $bulk_item_group_id . "_" . $funding_id . "_" . $bulk_item_location_id . "\" value=$bulk_item_diff>";
            $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_group_id[]\" value=\"$bulk_item_group_id\">";
            $bulk_table .= "<input type=\"hidden\" name=\"funding_id_ary[]\" value=\"$funding_id\">";
            // henry added
            $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_location_id_ary[]\" value=\"$bulk_item_location_id\">";
            
            $bulk_table .= "<td class=\"tabletext\">$bulk_item_found</td>";
            $bulk_table .= "<td class=\"tabletext\">$item_variance_qty</td>";
            
            // retrieve last remark
            $sql = "select Remark from INVENTORY_ITEM_BULK_LOG where ItemID=$bulk_item_id and LocationID=$location and GroupInCharge=$bulk_item_group_id and FundingSource=$funding_id and Action=2 order by DateModified desc limit 1";
            $result = $linventory->returnVector($sql);
            $this_remark = intranet_htmlspecialchars($result[0]);
            // $bulk_table .= "<td class=\"tabletext\" width=\"10%\"><textarea name=\"bulk_item_remark_".$bulk_item_id."_".$bulk_item_group_id."_".$funding_id."\" rows=\"2\" cols=\"50\" class=\"textboxtext\">". $this_remark ."</textarea></td>";
            $bulk_table .= "<td class=\"tabletext\" width=\"10%\"><input type=\"text\" name=\"bulk_item_remark_" . $bulk_item_id . "_" . $bulk_item_group_id . "_" . $funding_id . "_" . $bulk_item_location_id . "\" class=\"textboxtext\" value=\"" . $this_remark . "\"></td>";
            
            // $bulk_table .= "<td class=\"tabletext\">$bulk_item_write_off</td>";
            // $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_write_off_qty_".$bulk_item_id."_".$bulk_item_group_id."\" value=\"$bulk_item_write_off\">";
            
            // if($bulk_item_write_off>0)
            // $disable = " ";
            // else
            // $disable = " DISABLED ";
            
            // # Get Default Write Off Reason #
            // $sql = "SELECT ReasonTypeID, ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE";
            // $arr_write_off_reason = $linventory->returnArray($sql,2);
            // $write_off_reason = getSelectByArray($arr_write_off_reason," name=\"bulk_item_write_off_reason_".$bulk_item_id."_".$bulk_item_group_id."\" $disable");
            // # End #
            
            /*
             * $bulk_table .= "<td class=\"tabletext\" width=\"10%\">
             * $write_off_reason <br>
             * <input type=\"file\" class=\"file\" name=\"bulk_item_write_off_attachment_".$bulk_item_id."_".$bulk_item_group_id."\" $disable>
             * </td>";
             * $bulk_table .= "<input type=\"hidden\" name=\"hidden_bulk_item_write_off_attachment_".$bulk_item_id."_".$bulk_item_group_id."\" value=\"\">";
             */
            
            /*
             * $bulk_table .= "<td class=\"tabletext\" width=\"10%\">";
             * $bulk_table .= "$write_off_reason <br>";
             * $bulk_table .= "
             * <table border=\"0\" id=\"upload_file_list_$bulk_item_id\" cellpadding=\"0\" cellspacing=\"0\" >
             *
             * <script language=\"javascript\">
             *
             * for(i=0;i<no_of_upload_file;i++)
             * {
             * document.writeln('<tr><td><input class=\"file\" type=\"file\" name=\"bulk_item_write_off_attachment_$bulk_item_id'+'_'+i+'\" size=\"40\" $disable>');
             * document.writeln('<input type=\"hidden\" name=\"hidden_bulk_item_write_off_attachment_$bulk_item_id'+'_'+i+'\"></td></tr>');
             * }
             * </script>
             * </table>
             * <input name=\"addField_$bulk_item_id\" type=\"button\" value=\" + \" onClick=\"add_field(2, $bulk_item_id)\" $disable>
             * <input type=\"hidden\" name=\"no_of_file_upload_$bulk_item_id\" value=\"\">
             * ";
             * $bulk_table .= "</td>";
             *
             * $bulk_table .= "<td class=\"tabletext\">$bulk_item_total_qty</td>";
             * $bulk_table .= "<td class=\"tabletext\">$item_variance_qty</td>";
             *
             * $bulk_attachment1 = "bulk_item_write_off_attachment_".$bulk_item_id."_".$bulk_item_group_id;
             * $bulk_attachment2 = "hidden_bulk_item_write_off_attachment_".$bulk_item_id."_".$bulk_item_group_id;
             *
             * $arr_bulk_upload[] .= "bulk_item_write_off_attachment_".$bulk_item_id."_".$bulk_item_group_id;
             * $arr_bulk_upload[] .= "hidden_bulk_item_write_off_attachment_".$bulk_item_id."_".$bulk_item_group_id;
             */
            
            $bulk_table .= "</tr>\n";
            $bulk_table .= "<tr bgcolor=\"#A6A6A6\"><td height=\"1\" colspan=\"10\"></td></tr>\n";
            
            $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_group_id_$bulk_item_id" . "_" . "$bulk_item_location_id\" value=$bulk_item_group_id>";
            
            $arr_bulk_item[$i] = $bulk_item_id;
            $arr_group[$i] = $bulk_item_group_id;
            $arr_funding[$i] = $funding_id;
        }
        $bulk_item_list = implode(",", $arr_bulk_item);
        $bulk_table .= "<input type=\"hidden\" name=\"bulk_item_list\" value=\"$bulk_item_list\">";
    }
    if (sizeof($arr_bulk_item) == 0) {
        $bulk_table .= "<tr class=\"tablerow1\"><td class=\"tabletext\" colspan=\"10\" align=\"center\">$i_no_record_exists_msg</td></tr>\n";
    }
}
// $tmp_arr = array_merge($arr_single_upload,$arr_bulk_upload);
// $tmp_str = implode(",",$tmp_arr);

// echo generateFileUploadNameHandler2("form1",$tmp_arr);
?>

<?
/*
 * ?>
 *
 * <script language=javascript>
 * var no_of_upload_file = <?echo $no_file ==""?1:$no_file;?>;
 * </script>
 *
 * <script language="javascript">
 * function add_field(item_type, item_id)
 * {
 * var tmpObj = "upload_file_list_"+item_id;
 * var table = document.getElementById(tmpObj);
 * var tmp_no_file = eval("document.form1.no_of_file_upload_"+item_id);
 * var tmp_val = tmp_no_file.value;
 * if(tmp_val == "")
 * {
 * tmp_val = 1;
 * no_of_upload_file = 1;
 * }
 * var row = table.insertRow(tmp_val);
 *
 * if (document.all)
 * {
 * var cell = row.insertCell(0);
 *
 * if(item_type == 1)
 * {
 * x= '<input class="file" type="file" name="single_item_write_off_attachment_'+item_id+'_'+tmp_val+'" size="40">';
 * x+='<input type="hidden" name="hidden_single_item_write_off_attachment_'+item_id+'_'+tmp_val+'">';
 * }
 * if(item_type == 2)
 * {
 * x= '<input class="file" type="file" name="bulk_item_write_off_attachment_'+item_id+'_'+tmp_val+'" size="40">';
 * x+='<input type="hidden" name="hidden_bulk_item_write_off_attachment_'+item_id+'_'+tmp_val+'">';
 * }
 *
 * cell.innerHTML = x;
 * no_of_upload_file++;
 *
 * var tmp_no_file = eval("document.form1.no_of_file_upload_"+item_id);
 * tmp_no_file.value = no_of_upload_file;
 * }
 * }
 *
 * function setBulkItemWriteOffChecked(val,obj)
 * {
 * <?for($i=0; $i<sizeof($arr_bulk_item); $i++)
 * {
 * $bulk_item_id = $arr_bulk_item[$i];
 * $bulk_item_group_id = $arr_group[$i];
 * ?>
 * tmp1 = <?echo $bulk_item_id;?>;
 * tmp2 = <?echo $bulk_item_group_id;?>;
 *
 * tmp = eval("document.form1.bulk_item_write_off_"+tmp1+"_"+tmp2);
 * tmp.checked = val;
 * if(val == 1)
 * enableBulkItemWriteOffReason(0, tmp1, tmp2);
 * if(val == 0)
 * enableBulkItemWriteOffReason(1, tmp1, tmp2);
 * <?
 * }
 * ?>
 * }
 * function setSingleItemWriteOffChecked(val, obj, element_name){
 * len=obj.elements.length;
 * var i=0;
 * for( i=0 ; i<len ; i++) {
 * if (obj.elements[i].name==element_name)
 * obj.elements[i].checked=val;
 * }
 * if(val == 1)
 * enabledSingleItemWriteOffReason(0, 0)
 * if(val == 0)
 * enabledSingleItemWriteOffReason(1, 0)
 * }
 * function enableBulkItemWriteOffReason(val, bulk_item_id, bulk_item_group_id)
 * {
 * tmp1 = eval("document.form1.bulk_item_write_off_reason_"+bulk_item_id+"_"+bulk_item_group_id);
 * tmp2 = eval("document.form1.bulk_item_write_off_qty_"+bulk_item_id+"_"+bulk_item_group_id);
 * tmp1.disabled = val;
 * tmp2.disabled = val;
 * }
 * function enabledSingleItemWriteOffReason(val, single_item_id)
 * {
 * if(single_item_id == 0)
 * {
 * <?for($i=0; $i<sizeof($arr_single_item); $i++)
 * {
 * $single_item_id = $arr_single_item[$i];
 * ?>
 * tmp_id = <?echo $single_item_id;?>;
 * tmp1 = eval("document.form1.single_item_write_off_reason_"+tmp_id);
 * tmp2 = eval("document.form1.single_item_write_off_attachment_"+tmp_id);
 * tmp1.disabled = val;
 * tmp2.disabled = val;
 * <?
 * }
 * ?>
 * }
 * else
 * {
 * tmp1 = eval("document.form1.single_item_write_off_reason_"+single_item_id);
 * tmp2 = eval("document.form1.no_of_file_upload_"+single_item_id);
 * var tmp_val = tmp2.value;
 * if(tmp_val == ''){
 * tmp_val = 1;
 * tmp2.value = 1;
 * }
 *
 * for(i=0; i<tmp_val; i++)
 * {
 * tmp4 = eval("document.form1.single_item_write_off_attachment_"+single_item_id+'_'+i);
 * tmp4.disabled = val;
 * }
 * tmp3 = eval("document.form1.addField_"+single_item_id);
 * tmp1.disabled = val;
 * tmp3.disabled = val;
 * }
 * }
 * function setTextEnable(clicked,num,id,group_id)
 * {
 * var obj = eval("document.form1.bulk_item_status"+num+"_"+id+"_"+group_id);
 * if(clicked == 1)
 * {
 * obj.value = 0;
 * obj.disabled = false;
 * obj.style.backgroundColor = "#FFFFFF";
 * }
 * if(clicked == 0)
 * {
 * obj.value = "";
 * obj.disabled = true;
 * obj.style.backgroundColor = "#CCCCCC";
 * }
 * }
 *
 * function Big5FileUploadHandler() {
 *
 * <?
 * if(sizeof($arr_single_item)>0)
 * {
 * for($i=0; $i<sizeof($arr_single_item); $i++)
 * {
 * $single_item_id = $arr_single_item[$i];
 * ?>
 * var tmp_single_item_id = <?=$single_item_id;?>;
 *
 * var file_value = eval("document.form1.no_of_file_upload_"+tmp_single_item_id);
 * if(file_value != null)
 * {
 * upload_file_value = file_value.value;
 * if(upload_file_value == '')
 * upload_file_value = 0;
 * }
 * else
 * {
 * upload_file_value = 0;
 * }
 *
 * for(j=0;j<=no_of_upload_file;j++)
 * {
 * objSingleWriteOffFile = eval('document.form1.single_item_write_off_attachment_'+tmp_single_item_id+'_'+j);
 * objSingleHiddenWriteOffFile = eval('document.form1.hidden_single_item_write_off_attachment_'+tmp_single_item_id+'_'+j);
 * if(objSingleWriteOffFile!=null && objSingleWriteOffFile.value!='' && objSingleHiddenWriteOffFile!=null)
 * {
 * var Ary = objSingleWriteOffFile.value.split('\\');
 * objSingleHiddenWriteOffFile.value = Ary[Ary.length-1];
 * }
 * }
 *
 * <?
 * }
 * }
 * ?>
 * <?
 * if(sizeof($arr_bulk_item)>0)
 * {
 * for($i=0; $i<sizeof($arr_bulk_item); $i++)
 * {
 * $bulk_item_id = $arr_bulk_item[$i];
 * ?>
 * var tmp_bulk_item_id = <?=$bulk_item_id;?>;
 *
 * var file_value = eval("document.form1.no_of_file_upload_"+tmp_bulk_item_id);
 *
 * if(file_value != null)
 * {
 * upload_file_value = file_value.value;
 * if(upload_file_value == '') {
 * upload_file_value = 0;
 * file_value.value = 1;
 * }
 * }
 * else
 * {
 * upload_file_value = 0;
 * }
 *
 * for(j=0;j<=no_of_upload_file;j++)
 * {
 * objBulkWriteOffFile = eval('document.form1.bulk_item_write_off_attachment_'+tmp_bulk_item_id+'_'+j);
 * objBulkHiddenWriteOffFile = eval('document.form1.hidden_bulk_item_write_off_attachment_'+tmp_bulk_item_id+'_'+j);
 * if(objBulkWriteOffFile!=null && objBulkWriteOffFile.value!='' && objBulkHiddenWriteOffFile!=null)
 * {
 * var Ary2 = objBulkWriteOffFile.value.split('\\');
 * objBulkHiddenWriteOffFile.value = Ary2[Ary2.length-1];
 * }
 * }
 * <?
 * }
 * }
 * ?>
 * return true;
 * }
 *
 * function checkForm()
 * {
 * var single_check = 1;
 * var bulk_check = 1;
 *
 * <?if(sizeof($arr_single_item)>0)
 * {
 * ?>
 * var objArr = document.forms["form1"]["write_off_item_id[]"];
 * if(objArr)
 * {
 * for(i=0; i<objArr.length; i++)
 * {
 * if(objArr[i].checked)
 * {
 * var tmp_single_id = objArr[i].value;
 * tmp_single_select = eval("document.form1.single_item_write_off_reason_"+tmp_single_id);
 *
 * if(check_select(tmp_single_select,"Please select the reason",0))
 * {
 * single_check = 1;
 * }
 * else
 * {
 * single_check = 0;
 * return false;
 * }
 * }
 * }
 * }
 * <?
 * }
 * else
 * {
 * ?>
 * single_check = 1;
 * <?
 * }
 * ?>
 *
 * <?
 * if(sizeof($arr_bulk_item)>0)
 * {
 * for($i=0; $i<sizeof($arr_bulk_item); $i++)
 * {
 * $bulk_item_id = $arr_bulk_item[$i];
 * $bulk_item_group_id = $arr_group[$i];
 * ?>
 * tmp_bulk_id = <?echo $bulk_item_id;?>;
 * tmp_bulk_group_id = <?echo $bulk_item_group_id;?>;
 * tmp_qty = eval("document.form1.bulk_item_write_off_qty_"+tmp_bulk_id+"_"+tmp_bulk_group_id);
 *
 * if(tmp_qty.value != 0)
 * {
 * tmp_select = eval("document.form1.bulk_item_write_off_reason_"+tmp_bulk_id+"_"+tmp_bulk_group_id);
 * if(check_select(tmp_select,"Please select the reason",0))
 * {
 * bulk_check = 1;
 * }
 * else
 * {
 * bulk_check = 0;
 * return false;
 * }
 * }
 * else
 * {
 * bulk_check = 1;
 * }
 * <?
 * }
 * }
 * else
 * {
 * ?>
 * bulk_check = 1;
 * <?
 * }
 * ?>
 *
 * if(single_check == 1 && bulk_check == 1)
 * {
 * Big5FileUploadHandler();
 * document.form1.action = "stock_take_update.php";
 * return true;
 * }
 * }
 * <?
 */
?>
</script>

<br>

<form name="form1" action="stock_take_update_follow.php" method="POST">

	<table width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
		</tr>
	</table>
	<br>

	<table width="90%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td><?=$infobar; ?></td>
		</tr>
	</table>

	<table border="0" cellpadding="5" cellspacing="0" width="90%"
		align="center">
<?=$single_table;?>
</table>

	<table border="0" cellpadding="5" cellspacing="0" width="90%"
		align="center">
<?=$single_unlisted_table;?>
</table>
	<br>

	<table border="0" cellpadding="5" cellspacing="0" width="90%"
		align="center">
<?=$bulk_table;?>
</table>

	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr height="10px">
			<td></td>
		</tr>
		<tr>
			<td class="dotline"><img
				src="<?=$image_path/$LAYOUT_SKIN ?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>
	<input type="hidden" name="targetLocation" value="<?=$targetLocation?>">
	<input type="hidden" name="group_list" value="<?=$group_list?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>