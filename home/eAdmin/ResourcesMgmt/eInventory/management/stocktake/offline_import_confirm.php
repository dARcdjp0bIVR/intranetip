<?php

// modifying : 

/*************************************************************************
 *  modification log
 *  
 *  Date:   2019-06-28 (Tommy)
 *          add write-off item error message
 *  
 *  Date:   2018-02-07 (Henry)
 *          add access right checking [Case#E135442]
 *  
 * 	Date:	2013-06-07 (Rita)
 * 			remove Qty for Single item
 * 
 *  Date:	2013-03-18	Carlos
 * 			simplied error msg for invalid group and invalid funding source
 * 
 *  Date:	2013-03-13	Carlos
 * 			add column [Item Type] 
 * 
 *  Date:	2013-02-21	Carlos
 * 			add checking for Resource Management Group barcode and Funding Source barcode
 *  
 *	Date:	2011-12-15	YatWoon
 *			add UserID checking for temp table
 *
 *	Date:	2011-07-15	YatWoon
 *			improved: add checking for resource mgmt group
 *
 *	Date:	2011-04-26	Yuen
 *			fixed for taking bulk items by CSV
 *
 * 
 * ************************************************************************/
 
 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext-kiosk.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_Stocktake";
$linterface 	= new interface_html();
$linventory		= new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$limport 		= new libimporttext();
$li 			= new libdb();
$lo 			= new libfilesystem();
$llocation_ui	= new liblocation_ui();

$errorCount = 0;
$successCount = 0;
$error_result = array();
		
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: offline.php?xmsg=".urlencode($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['IncorrectFileExtention']));
	exit();
}

$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Online']['Title'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/index.php", 0);
$TAGS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Title'], $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/stocktake/offline.php", 1);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step1'], 0);
$STEPS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step2'], 1);
$STEPS_OBJ[] = array($Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step3'], 0);

// $sql = "DROP TABLE TEMP_INVENTORY_STOCKTAKE_RESULT";
// $linventory->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_STOCKTAKE_RESULT
		(	TempStocktakeID int(11) NOT NULL auto_increment,
			RowNum int(11),
			BuildingID varchar(255),
			LocationLevelID varchar(255),
			LocationID varchar(255),
			ItemID int(11),
			ItemType int(11),
			Quantity int(11),
			StocktakeDate date,
			StocktakeTime time,
			UserID int(11),
			PRIMARY KEY (TempStocktakeID)
		)ENGINE=InnoDB DEFAULT CHARSET=utf8";
$linventory->db_db_query($sql);

$sql = "ALTER TABLE TEMP_INVENTORY_STOCKTAKE_RESULT ADD COLUMN AdminGroupID int(11) AFTER LocationID";
$linventory->db_db_query($sql);

$sql = "ALTER TABLE TEMP_INVENTORY_STOCKTAKE_RESULT ADD COLUMN FundingSourceID int(11) AFTER AdminGroupID";
$linventory->db_db_query($sql);

$sql = "delete from TEMP_INVENTORY_STOCKTAKE_RESULT where UserID=$UserID";
$linventory->db_db_query($sql);

$ItemTypeAry = array(); // Row Number => Item Type

###########################
### insert into temp db ###
###########################

if($limport->CHECK_FILE_EXT($name))
{
	$data = $limport->GET_IMPORT_TXT($csvfile);
	foreach($data as $row=>$value)
	{
		$num_of_column = count($value);
		if($num_of_column == 4) { // single item
			list($LocationCode,$Code,$StocktakeDate,$StocktakeTime) = $value;
			$Qty = 1;
			$item_type = ITEM_TYPE_SINGLE;
		} else { // bulk item
			list($LocationCode,$AdminGroupCode,$FundingSourceCode,$Code,$Qty,$StocktakeDate,$StocktakeTime) = $value;
			$item_type = ITEM_TYPE_BULK;
		}
		$row_no++;
		
		## Get ItemID
		$arr_item_info = $linventory->getImportStocktakeItem($Code);
		if(sizeof($arr_item_info)>0){
			list($item_id, $item_name) = $arr_item_info[0];
		}else{
			$item_id = "";
			$item_name = "";
		}
		
		## Get BuildingID, LocationLevelID, LocationID
		$LocationArray = $linventory->getImportStocktakeLocationID($LocationCode);
		if(sizeof($LocationArray)>0){
			$BuildingID = $LocationArray[0]['BuildingID'];
			$LocationLevelID = $LocationArray[0]['LocationLevelID'];
			$LocationID = $LocationArray[0]['LocationID'];
		}else{
			$BuildingID = "";
			$LocationLevelID = "";
			$LocationID = "";
		}
		
		$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '$item_id'";
		$ItemType = $linventory->returnVector($sql);
		
		$ItemTypeAry[$row] = $item_type;
		
		$AdminGroupIDValue = "NULL";
		$FundingSourceIDValue = "NULL";
		if($ItemType[0] == ITEM_TYPE_BULK) {
			if($AdminGroupCode != "") {
				$tmp_result = $linventory->checkImportStocktakeAdminGroup($AdminGroupCode);
				if($tmp_result) {
					$AdminGroupIDValue = "'$tmp_result'";
				}
			}
			if($FundingSourceCode != "") {
				$tmp_result = $linventory->checkImportStocktakeFundingSource($FundingSourceCode);
				if($tmp_result) {
					$FundingSourceIDValue = "'$tmp_result'";
				}
			}
		}
		
		$sql = "INSERT INTO TEMP_INVENTORY_STOCKTAKE_RESULT 
					(RowNum, BuildingID, LocationLevelID, LocationID, AdminGroupID, FundingSourceID, ItemID, ItemType, Quantity, StocktakeDate, StocktakeTime,UserID)
				VALUES
					('$row_no','$BuildingID','$LocationLevelID','$LocationID',$AdminGroupIDValue,$FundingSourceIDValue,'$item_id','".$ItemType[0]."','$Qty','$StocktakeDate','$StocktakeTime', $UserID)";
		
		$linventory->db_db_query($sql);
		/*
		$sql = "INSERT INTO TEMP_INVENTORY_STOCKTAKE_RESULT 
					(RowNum, BuildingID, LocationLevelID, LocationID, ItemID, ItemType, Quantity, StocktakeDate, StocktakeTime,UserID)
				VALUES
					('$row_no','$BuildingID','$LocationLevelID','$LocationID','$item_id','".$ItemType[0]."','$Qty','$StocktakeDate','$StocktakeTime', $UserID)";
		$linventory->db_db_query($sql);
		*/
	}
}
	
if($limport->CHECK_FILE_EXT($name))
{
	$csv_data = $limport->GET_IMPORT_TXT($csvfile);
	$LocationError = array();
	$ItemError = array();
	$AdminGroupError = array();
	$FundingSourceError = array();
	$QtyError = array();
	$DateError = array();
	$TimeError = array();
	foreach($csv_data as $row=>$value)
	{
		$num_of_column = count($value);
	
		if($num_of_column == 4) { // single item
			list($LocationCode,$Code,$StocktakeDate,$StocktakeTime) = $value;
			$Qty = 1;
			$item_type = ITEM_TYPE_SINGLE;
		} else { // bulk item
			list($LocationCode,$AdminGroupCode,$FundingSourceCode,$Code,$Qty,$StocktakeDate,$StocktakeTime) = $value;
			$item_type = ITEM_TYPE_BULK;
		}
		$RowNum++;
		$Validation = true;
		$error_msg = array();
		
		## Check Location Code is valid
		if(!$linventory->checkImportStocktakeLocation($LocationCode)){
			$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['LocationNotFound'];
			$LocationError[] = $RowNum;
		}
		//debug_r("row ".$row." item type ");
		//debug_r($ItemTypeAry[$row]);
		## Check Item Code / Barcode is valid
		if($linventory->checkImportStocktakeCode($Code, $item_type)  == 'fail'){
			$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemNotFound'];
			$ItemError[] = $RowNum;
		}else if($linventory->checkImportStocktakeCode($Code, $item_type) == 'writeOff'){
		    $error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['WrittenOff'];
		    $ItemError[] = $RowNum;
		}
		else if($linventory->checkImportStocktakeCode($Code, $item_type) == 'success')
		{
			## check user has access right for stocktake this item
			
			
			## Check Item Is in correct location		
			if(!$linventory->checkItemInCorrectLocation($Code,$LocationCode))
			{
				$Code = str_replace("+","_",trim($Code));
				
				$sql = "SELECT 
							CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',
							".$linventory->getInventoryNameByLang("level.").",' > ',
							".$linventory->getInventoryNameByLang("location.").")
						FROM 
							INVENTORY_ITEM AS a INNER JOIN 
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_LOCATION AS location ON (b.LocationID = location.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS level ON (location.LocationLevelID = level.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS building ON (level.BuildingID = building.BuildingID)
						WHERE
							a.ItemCode = '$Code'
						";
				$PossibleLocationList = $linventory->returnArray($sql,1);
				
				if(sizeof($PossibleLocationList)>0)
				{
					$possible_location_layer .= "<div id=$RowNum><table border='0'>";
					for($i=0; $i<sizeof($PossibleLocationList); $i++)
					{
						list($location_name) = $PossibleLocationList[$i];

						$possible_location_layer .= "<tr><td width='10%' align='right'> - </td><td>$location_name</td></tr>";
					}
					$possible_location_layer .= "</table></div>";
				}
				$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemShouldLocation']." : ".$possible_location_layer;
				$ItemError[] = $RowNum;
			}
			
			#### For bulk item
			if($ItemTypeAry[$row] == ITEM_TYPE_BULK) {
				## Check Resource Management Group is valid
				$admin_group_id = $linventory->checkImportStocktakeAdminGroup($AdminGroupCode);
				if(!$admin_group_id) {
					$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidResourceMgmtGroup'];
					$AdminGroupError[] = $RowNum;
				}
				
				## Check item owned by correct Resource Management Group
				if($admin_group_id) {
					$is_owned_group = $linventory->checkImportStocktakeItemOwnedByAdminGroup($Code,$AdminGroupCode);
					if(!$is_owned_group) {
						/*
						$PossibleAdminGroupList = $linventory->getImportStocktakeItemAdminGroup($Code);
						$possible_group_layer .= "<div id=$RowNum><table border='0'>";
						for($i=0; $i<sizeof($PossibleAdminGroupList); $i++)
						{
							list($group_id, $group_name, $group_barcode) = $PossibleAdminGroupList[$i];
							$possible_group_layer .= "<tr><td width='10%' align='right'> - </td><td>$group_name</td></tr>";
						}
						$possible_group_layer .= "</table></div>";
						$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemShouldManagedBy']." : ".$possible_group_layer;
						$AdminGroupError[] = $RowNum;
						*/
						$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidResourceMgmtGroup'];
						$AdminGroupError[] = $RowNum;
					}
				}
				
				## Check Funding Source is valid
				$funding_source_id = $linventory->checkImportStocktakeFundingSource($FundingSourceCode);
				if(!$funding_source_id) {
					$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidFundingSource'] ;
					$FundingSourceError[] = $RowNum;
				}
				
				## Check item is funded by correct Funding Source
				if($funding_source_id) {
					$is_funding_source = $linventory->checkImportStocktakeItemFundingSource($Code,$FundingSourceCode);
					if(!$is_funding_source) {
						/*
						$PossibleFundingSourceList = $linventory->getImportStocktakeItemFundingSource($Code);
						$possible_funding_layer .= "<div id=$RowNum><table border='0'>";
						for($i=0; $i<sizeof($PossibleFundingSourceList); $i++)
						{
							list($funding_id, $funding_name, $funding_barcode) = $PossibleFundingSourceList[$i];
							$possible_funding_layer .= "<tr><td width='10%' align='right'> - </td><td>$funding_name</td></tr>";
						}
						$possible_funding_layer .= "</table></div>";
						$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemShouldFundedBy']." : ".$possible_funding_layer;
						$FundingSourceError[] = $RowNum;
						*/
						$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidFundingSource'] ;
						$FundingSourceError[] = $RowNum;
					}
				}
				
			} // for bulk item only
		}
		
		## Check Qty is valid
		if($Qty >= 0) {
			
			//continue
			/*
			if(strpos($Code,"+") > 0)
			{
				// bulk item, continue	
				// for bulk item, qty >= 0 is accepted, so continue
			}
			else
			{
				if($Qty == 1)
				{
					// single item
					// for single item, qty can only equal to 1, so continue	
				}
				else
				{
					$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidQty'];
					$QtyError[] = $RowNum;
				}
			}
			*/
		} else {
			$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidQty'];
			$QtyError[] = $RowNum;
		}
		
		## Check Stocktake Date is valid
		if($linventory->checkImportStocktakeDate($StocktakeDate) == -1) {
			$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidDate'];
			$DateError[] = $RowNum;
		} elseif ($linventory->checkImportStocktakeDate($StocktakeDate) == -2) {
			$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidDateFormat'];
			$DateError[] = $RowNum;
		} else {
			//continue
		}
		
		## Check Stocktake Time is valid
		
		if(!$linventory->checkImportStocktakeTime($StocktakeTime)) {
			$error_msg[] = $Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidTime'];
			$TimeError[] = $RowNum;
		}
		
		if(empty($error_msg)) {
			$successCount++;
		} else {
			$error_result[$RowNum] = $error_msg;
			$error_RowNum_str .=  $RowNum . ",";
			$errorCount++;
		}
	}
	
	$x .= "<BR>\n";
	$x .= "<BR>\n";
	$x .= "<form name='form1' id='form1' method='post' action='offline_import_update.php' enctype='multipart/form-data'>\n";
	$x .= "<table width='100%' border='0' cellpadding='5' cellspacing='0' align='center'>\n";
	$x .= "<tr><td>\n";
	
	### Show step table ###
	$x .= "<table width='100%' border='0' cellpadding=\"3\" cellspacing=\"0\">\n";
	$x .= "<tr><td colspan='2'>".$linterface->GET_STEPS($STEPS_OBJ)."</td></tr>\n";
	$x .= "<tr><td colspan='2'></td></tr>\n";
	$x .= "</table>\n";
	
	### Show import result summary ###
	$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
	$x .= "<tr>\n";
	$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['SuccessfulRecord'] ."</td>\n";
	$x .= "<td class='tabletext'>". $successCount ."</td>\n";
	$x .= "</tr>\n";
	$x .= "<tr>\n";
	$x .= "<td class='formfieldtitle' width='30%' align='left'>". $Lang['General']['FailureRecord'] ."</td>\n";
	$x .= "<td class='tabletext'>". ($errorCount ? "<font color='red'>":"") . $errorCount . ($errorCount ? "</font>":"") ."</td>\n";
	$x .= "</tr>\n";
	$x .= "</table><br>\n";
	
	### Show import result w/ details ###
	$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
	$x .= "<tr>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['RowNum']."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Code']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_InventorySystem['item_type']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ItemName']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Location']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ResourceMgmtGroup']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['FundingSource']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Quantity']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['StocktakeDate']."</td>\n";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['StocktakeTime']."</td>\n";
	if($errorCount > 0){
		$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Remark']."</td>\n";
	}
	$x .= "</tr>\n";
	
	foreach($csv_data as $row=>$value){
		$num_of_column = count($value);
		if($num_of_column == 4) { // single item
			list($LocationCode,$Code,$StocktakeDate,$StocktakeTime) = $value;
			$Qty = 1;
			$item_type = 1;
		} else { // bulk item
			list($LocationCode,$AdminGroupCode,$FundingSourceCode,$Code,$Qty,$StocktakeDate,$StocktakeTime) = $value;
			$item_type = 2;
		}
		$row++;
		
		if($row%2 == 0)
			$table_row_css = " class=\"tablerow2\" ";
		else
			$table_row_css = " class=\"tablerow1\" ";
			
		$arr_item_info = $linventory->getImportStocktakeItem($Code);
		if(sizeof($arr_item_info)>0){
			list($item_id, $item_name) = $arr_item_info[0];
		}else{
			$item_id = "";
			$item_name = " - ";
		}
		
		$arr_location_info = $linventory->getImportStocktakeLocation($LocationCode);
		if(sizeof($arr_location_info)>0){
			list($location_code) = $arr_location_info[0];
		}else{
			$location_code = str_replace("+", ">", $LocationCode);
		}
		
		$admin_group_name = " - ";
		$funding_source = " - ";
		if($item_type == 2) {
			$arr_group_info = $linventory->getAdminGroupByBarcode($AdminGroupCode);
			$arr_funding_info = $linventory->getFundingSourceByBarcode($FundingSourceCode);
			if(sizeof($arr_group_info)>0) {
				$admin_group_name = $arr_group_info[0]['AdminGroupName'];
			}
			if(sizeof($arr_funding_info)>0) {
				$funding_source = $arr_funding_info[0]['FundingSourceName'];
			}
		}
		
		$x .= "<tr $table_row_css >\n";
		$x .= "<td>$row</td>\n";
		
		if(in_array($row,$ItemError))
		{
			$x .= "<td><font color='red'>$Code</td>\n";
		}else{
			$x .= "<td>$Code</td>\n";
		}
		$x .= "<td>".($item_type==ITEM_TYPE_SINGLE?$Lang['eInventory']['SingleItem']:$Lang['eInventory']['BulkItem'])."</td>\n";
		$x .= "<td>$item_name</td>\n";
		if(in_array($row,$LocationError))
		{
			$x .= "<td><font color='red'>$location_code</font></td>\n";
		}else{
			$x .= "<td>$location_code</td>\n";
		}
		if(in_array($row,$AdminGroupError))
		{
			$x .= "<td><font color='red'>$admin_group_name</font></td>\n";
		}else{
			$x .= "<td>$admin_group_name</td>\n";
		}
		if(in_array($row,$FundingSourceError))
		{
			$x .= "<td><font color='red'>$funding_source</font></td>\n";
		}else{
			$x .= "<td>$funding_source</td>\n";
		}
		if(in_array($row,$QtyError)){
			$x .= "<td><font color='red'>$Qty</font></td>\n";
		}else{
			$x .= "<td>$Qty</td>\n";
		}
		if(in_array($row,$DateError)){
			$x .= "<td><font color='red'>$StocktakeDate</font></td>";
			
		}else{
			$x .= "<td>$StocktakeDate</td>";
		}
		if(in_array($row,$TimeError)){
			$x .= "<td><font color='red'>$StocktakeTime</font></td>";
			
		}else{
			$x .= "<td>$StocktakeTime</td>";
		}
		
		$error_remark = "";
		if($errorCount > 0)
		{
			if(is_array($error_result[$row]))
			{
				foreach($error_result[$row] as $k1=>$d1)
				{
					$error_remark .= "<li><font color='red'>". $d1."</font></li>";
				}
			}
			$x .= "<td>$error_remark</td>\n";		
		}
		$x .= "</tr>\n";
	}
	
	if($errorCount>0)
	{
		$import_button = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='offline.php'");
	}
	else
	{
		$import_button = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")." &nbsp;".$linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='offline.php'");
	}
	$x .= "</table>\n";
	$x .= "<table width='90%' border='0' cellpadding='3' cellspacing='0' align='center'>\n";
	$x .= "<tr>\n";
	$x .= "<td class='dotline'><img src='$image_path'/$LAYOUT_SKIN/10x10.gif' width='10' height='1' /></td>\n";
	$x .= "</tr>\n";			
	$x .= "<tr><td align='center' colspan='2'>$import_button</td></tr>";
	$x .= "</table>\n";

	$x .= "</td></tr>\n";
	$x .= "</table><br>\n";
	$x .= "<input type='hidden' name='AcademicYearID' id='AcademicYearID' value='$AcademicYearID'>\n";
	$x .= "</form\n";
}

### Show import result layout
echo $x;

$linterface->LAYOUT_STOP();
intranet_closedb();
?>