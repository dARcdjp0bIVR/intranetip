<?php
# using: yat

##############################################
#
#	Date:	2011-05-31	YatWoon
#			[Single]	Remove column Qty, barcode
#					 	Add column "Stocktake location"
#
##############################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_VarianceHandlingNotice";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$TAGS_OBJ[] = array($i_InventorySystem['VarianceHandlingNotice'], "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if ($xmsg != "") {
	$SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

$single_table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkFinish(document.form1,'SingleRecordID[]','notice_update.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_finish
					</a>
				</td>";
$bulk_table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkFinish(document.form1,'BulkRecordID[]','notice_update.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_finish
					</a>
				</td>";				

$sql = "SELECT 
			a.ReminderID, 
			b.ItemID,
			b.ItemType,
			".$linventory->getInventoryNameByLang("b.").",
			b.ItemCode,
			IF(c.TagCode != '', c.TagCode, ' - '),
			CONCAT(".$linventory->getInventoryNameByLang("e.").",' > ',".$linventory->getInventoryNameByLang("d.")."),
			a.OriginalQty,
			a.Quantity, 
			f.LocationID AS OrigianlLocationID,
			h.LocationID AS NewLocationID,
			CONCAT(".$linventory->getInventoryNameByLang("building1.").",' > ',".$linventory->getInventoryNameByLang("g.").",' > ',".$linventory->getInventoryNameByLang("f.")."),
			CONCAT(".$linventory->getInventoryNameByLang("building2.").",' > ',".$linventory->getInventoryNameByLang("i.").",' > ',".$linventory->getInventoryNameByLang("h.")."),
			".getNameFieldByLang2("j.").",
			a.SendDate,
			a.GroupInCharge,
			a.FundingSourceID,
			a.Remark,
			".$linventory->getInventoryNameByLang("ag.").",
			".$linventory->getInventoryNameByLang("funding.")."
		FROM
			INVENTORY_VARIANCE_HANDLING_REMINDER AS a INNER JOIN
			INVENTORY_ITEM AS b ON (b.ItemID = a.ItemID) LEFT OUTER JOIN
			INVENTORY_ITEM_SINGLE_EXT AS c ON (c.ItemID = b.ItemID) LEFT OUTER JOIN
			INVENTORY_CATEGORY AS d ON (d.CategoryID = b.CategoryID) LEFT OUTER JOIN
			INVENTORY_CATEGORY_LEVEL2 AS e ON (e.Category2ID = b.Category2ID) LEFT OUTER JOIN
			INVENTORY_LOCATION AS f ON (f.LocationID = a.OriginalLocationID) LEFT OUTER JOIN
			INVENTORY_LOCATION_LEVEL AS g ON (g.LocationLevelID = f.LocationLevelID) LEFT OUTER JOIN
			INVENTORY_LOCATION_BUILDING AS building1 ON (g.BuildingID = building1.BuildingID) LEFT OUTER JOIN 
			INVENTORY_LOCATION AS h ON (h.LocationID = a.NewLocationID) LEFT OUTER JOIN
			INVENTORY_LOCATION_LEVEL AS i ON (i.LocationLevelID = h.LocationLevelID) LEFT OUTER JOIN
			INVENTORY_LOCATION_BUILDING AS building2 ON (i.BuildingID = building2.BuildingID) LEFT OUTER JOIN
			INTRANET_USER AS j ON (j.UserID = a.SenderID)
			LEFT join INVENTORY_ADMIN_GROUP as ag on (ag.AdminGroupID=a.GroupInCharge)
			LEFT join INVENTORY_FUNDING_SOURCE as funding on (funding.FundingSourceID=a.FundingSourceID)
		WHERE
			a.HandlerID = $UserID AND
			a.RecordStatus = 0 
		order by building1.DisplayOrder, g.DisplayOrder, f.DisplayOrder
		";
$arr_result = $linventory->returnArray($sql);
if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
// 		list($reminder_id, $item_id, $item_type, $item_name, $item_code, $item_barcode, $item_category, $item_original_qty, $item_qty, $curr_location_id, $new_location_id, $item_curr_location, $item_new_location, $sender_name, $send_date, $remark) = $arr_result[$i];
		list($reminder_id, $item_id, $item_type, $item_name, $item_code, $item_barcode, $item_category, $item_original_qty, $item_qty, $curr_location_id, $new_location_id, $item_curr_location, $item_new_location, $sender_name, $send_date, $item_group_id, $item_funding_id, $remark, $this_group_name, $this_funding_name) = $arr_result[$i];
		
		if($item_type == ITEM_TYPE_SINGLE)
			$arr_single_notice[] = array($reminder_id,$item_code,$item_name,$item_barcode,$item_category,$item_original_qty,$item_qty,$curr_location_id,$new_location_id,$item_curr_location,$item_new_location,$sender_name,$send_date);
// 			$arr_single_notice[] = array($reminder_id,$item_code,$item_name,$item_barcode,$item_category,$item_original_qty,$item_qty,$curr_location_id,$new_location_id,$item_curr_location,$item_new_location,$sender_name,$send_date, $item_id);
			
		if($item_type == ITEM_TYPE_BULK)
			$arr_bulk_notice[] = array($reminder_id,$item_code,$item_name,$item_barcode,$item_category,$item_original_qty,$item_qty,$curr_location_id,$new_location_id,$item_curr_location,$item_new_location,$sender_name,$send_date, $item_group_id, $item_funding_id, $item_id, $remark, $this_group_name, $this_funding_name);
// 			$arr_bulk_notice[] = array($reminder_id,$item_code,$item_name,$item_barcode,$item_category,$item_original_qty,$item_qty,$curr_location_id,$new_location_id,$item_curr_location,$item_new_location,$sender_name,$send_date, $remark, $item_id);
	}
}

$single_notice_content .= "<tr class=\"tabletop\">";
$single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
$single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
// $single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>";
$single_notice_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem['Category']."</td>";
// $single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>";
// $single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandlingNotice_CurrentLocation</td>";
$single_notice_content .= "<td class=\"tabletopnolink\">". $Lang['eInventory']['StocktakeLocation'] ."</td>";
$single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandlingNotice_NewLocation</td>";
$single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandlingNotice_Sender</td>";
$single_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandlingNotice_SendDate</td>";
$single_notice_content .= "<td class=\"tabletopnolink\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'SingleRecordID[]'):setChecked(0,this.form,'SingleRecordID[]')\"></td>";
$single_notice_content .= "</tr>";

if(sizeof($arr_single_notice)>0)
{
	for($i=0; $i<sizeof($arr_single_notice); $i++)
	{
		list ($reminder_id,$item_code,$item_name,$item_barcode,$item_category,$item_original_qty,$item_qty,$curr_location_id,$new_location_id,$item_curr_location,$item_new_location,$sender_name,$send_date, $item_id) = $arr_single_notice[$i];
		
		
		$single_notice_content .= "<tr class=\"single\">";
		$single_notice_content .= "<td class=\"tabletext\">$item_code</td>";
		$single_notice_content .= "<td class=\"tabletext\">$item_name</td>";
// 		$single_notice_content .= "<td class=\"tabletext\">$item_barcode</td>";
		$single_notice_content .= "<td class=\"tabletext\">$item_category</td>";
// 		$single_notice_content .= "<td class=\"tabletext\">$item_qty</td>";

		$single_notice_content .= "<td class=\"tabletext\">$item_curr_location</td>";	# current location
		
// 		# current location 
// 		$sql = "select LocationID from INVENTORY_ITEM_SINGLE_EXT where ItemID=$item_id";
// 		$result = $linventory->returnVector($sql);
// 		$single_notice_content .= "<td class=\"tabletext\">". $linventory->returnLocationStr($result[0]) ."</td>";
		$single_notice_content .= "<td class=\"tabletext\">$item_new_location</td>";
		$single_notice_content .= "<td class=\"tabletext\">$sender_name</td>";
		$single_notice_content .= "<td class=\"tabletext\">$send_date</td>";
		$single_notice_content .= "<td><input type=\"checkbox\" name=\"SingleRecordID[]\" value=\"$reminder_id\"></td>";
		$single_notice_content .= "</tr>";
		$single_notice_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"10\"></td></tr>";
	}
}
if(sizeof($arr_single_notice) == 0)
{
	$single_notice_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"10\" align=\"center\">$i_no_record_exists_msg</td></tr>";
	$single_notice_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"10\"></td></tr>";
}
 
$bulk_notice_content .= "<tr class=\"tabletop\">";
$bulk_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Name</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem['Category']."</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Location_Level</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\">".$i_InventorySystem['Caretaker']."</td>";
// $bulk_notice_content .= "<td class=\"tabletopnolink\" width=\"10\">$i_InventorySystem_Stocktake_StocktakeQty</td>";
// $bulk_notice_content .= "<td class=\"tabletopnolink\" width=\"10\">$i_InventorySystem_VarianceHandling_New_Quantity</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\">Remark</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_VarianceHandlingNotice_Sender</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\" >$i_InventorySystem_VarianceHandlingNotice_SendDate</td>";
$bulk_notice_content .= "<td class=\"tabletopnolink\"><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'BulkRecordID[]'):setChecked(0,this.form,'BulkRecordID[]')\"></td>";
$bulk_notice_content .= "</tr>";

if(sizeof($arr_bulk_notice)>0)
{
	for($i=0; $i<sizeof($arr_bulk_notice); $i++)
	{
		//list ($reminder_id,$item_code,$item_name,$item_barcode,$item_category,$item_original_qty,$item_qty,$curr_location_id,$new_location_id,$item_curr_location,$item_new_location,$sender_name,$send_date, $remark, $item_id) = $arr_bulk_notice[$i];
		list ($reminder_id,$item_code,$item_name,$item_barcode,$item_category,$item_original_qty,$item_qty,$curr_location_id,$new_location_id,$item_curr_location,$item_new_location,$sender_name,$send_date, $item_group_id, $item_funding_id, $this_item_id, $remark, $this_group_name, $this_funding_name) = $arr_bulk_notice[$i];
		
		$sql = "SELECT 
						a.StockCheckQty
				FROM 
						INVENTORY_ITEM_BULK_LOG AS a INNER JOIN 
						INVENTORY_ITEM_BULK_LOCATION AS b ON (b.ItemID = a.ItemID AND b.LocationID = a.LocationID AND b.GroupInCharge = a.GroupInCharge and b.FundingSourceID=a.FundingSource)
				WHERE 
						b.ItemID = $item_id AND b.LocationID = $curr_location_id AND a.Action IN (2)
						and b.GroupInCharge=$item_group_id and b.FundingSourceID=$item_funding_id
				order by a.RecordID desc
				";
		$arr_tmp_result = $linventory->returnArray($sql);
		if(sizeof($arr_tmp_result)>0)
		{
			list ($stocktake_qty) = $arr_tmp_result[0];
		}
		
		$bulk_notice_content .= "<tr class=\"bulk\">";
		$remark = $remark ? $remark : $Lang['General']['EmptySymbol'];
		
		$bulk_notice_content .= "<td class=\"tabletext\">$item_code</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$item_name</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$item_category</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$item_curr_location</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$this_group_name</td>";
// 		$bulk_notice_content .= "<td class=\"tabletext\">$stocktake_qty</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$item_qty</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$remark</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$sender_name</td>";
		$bulk_notice_content .= "<td class=\"tabletext\">$send_date</td>";
		$bulk_notice_content .= "<td><input type=\"checkbox\" name=\"BulkRecordID[]\" value=\"$reminder_id\"></td>";
		$bulk_notice_content .= "</tr>";
		$bulk_notice_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"12\"></td></tr>";
	}
}
if(sizeof($arr_bulk_notice)==0)
{
	$bulk_notice_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"12\" align=\"center\">$i_no_record_exists_msg</td></tr>";
	$bulk_notice_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"12\"></td></tr>";
}
?>

<script language="javascript">
function checkFinish(obj,element,page)
{
	if(countChecked(obj,element)!=0) {
            obj.action=page;
            obj.submit();
    } else {
            alert("Please select at lease one item");
    }
}
</script>

<br>

<form name="form1" action="" method="post" onSubmit="">

<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
</table>

<!-- Single Item Notice -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$single_table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table border="0" width="100%" cellpadding="5" cellspacing="0">
<tr>
	<td colspan="12" class="tablename">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr class="tablename">
			<td class="tablename2" align="left"><?=$i_InventorySystem_ItemType_Single;?></td>
		</tr>
		</table>
	</td>
</tr>
<?=$single_notice_content?>
</table>

<br>

<!-- Bulk Item Notice -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td colspan="2" align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$bulk_table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<table border="0" width="100%" cellpadding="5" cellspacing="0">
<tr>
	<td colspan="12" class="tablename">
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr class="tablename">
			<td class="tablename2" align="left"><?=$i_InventorySystem_ItemType_Bulk;?></td>
		</tr>
		</table>
	</td>
</tr>
<?=$bulk_notice_content?>
</table>

</form>

</br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
