<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_opendb();
$li = new libdb();

//$MODULE_OBJ['title'] = "Time table";
$linterface = new interface_html("popup.html");

$linterface->LAYOUT_START();

//$sql = "SELECT Title, RecordStatus FROM INTRANET_SLOT_BATCH ORDER BY RecordStatus DESC, Title ASC";
$sql = "SELECT a.Title, a.RecordStatus, b.Title, b.TimeRange, b.BatchID
        FROM INTRANET_SLOT_BATCH as a, INTRANET_SLOT as b
        WHERE a.BatchID = b.BatchID
        ORDER BY a.RecordStatus DESC, a.Title ASC, b.SlotSeq ASC";
$result = $li->returnArray($sql,5);
$prevBatch = -1;

//$table_content .= "<tr class=\"tablegreentop tabletopnolink\"><td>Title</td></tr>";
$table_content .= "<tr class=\"tablegreenrow1\"><td class=\"tabletext\">";
$table_content .= "<ul>\n";

for ($i=0; $i<sizeof($result); $i++)
{
     list($batchName,$school,$slotname,$slottime,$bid) = $result[$i];
     if ($bid != $prevBatch)
     {
         if ($school == 1)
             $batchName .= ' *';
         if ($prevBatch != -1)
         {
             $table_content .= "</ol>\n";
         }
         $table_content .= "<li> $batchName <ol>\n";
         $prevBatch = $bid;
     }
	$table_content .= "<li class=numberlist>&nbsp;$slotname&nbsp;$slottime</li>";
}

$table_content .= "</ol></ul>\n";
$table_content .= "* - $i_ResourcePopScheme\n";
$table_content .= "</td></tr>";
?>

<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center" class="eSporttableborder">
	<?=$table_content;?>
</table>
<table width="90%" border="0">
	<tr> 
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
                <tr>
                	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                </tr>
                <tr>
					<td align="center"><?= $linterface->GET_BTN($button_close, "button", "self.close();","closebtn") ?></td>
				</tr>
            </table>     		
		</td>
	</tr>
</table>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>