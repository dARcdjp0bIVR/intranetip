<?php
// using:

// ##########################################
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-07 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2016-02-04 Henry
// php 5.4 issue move set cookies after includes file
//
// Date: 2011-04-26 Yuen
// customized for Sacred Heart Canossian College
//
// Date: 2011-04-13 YatWoon
// check only Admin and leader can edit
//
// ##########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

// ## set cookies
if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
// preserve table view
if ($ck_category_item_browsing_view_voucher_record_page_number != $pageNo && $pageNo != "") {
    setcookie("ck_category_item_browsing_view_voucher_record_page_number", $pageNo, 0, "", "", 0);
    $ck_category_item_browsing_view_voucher_record_page_number = $pageNo;
} else 
    if (! isset($pageNo) && $ck_category_item_browsing_view_voucher_record_page_number != "") {
        $pageNo = $ck_category_item_browsing_view_voucher_record_page_number;
    }

if ($ck_category_item_browsing_view_voucher_record_page_order != $order && $order != "") {
    setcookie("ck_category_item_browsing_view_voucher_record_page_order", $order, 0, "", "", 0);
    $ck_category_item_browsing_view_voucher_record_page_order = $order;
} else 
    if (! isset($order) && $ck_category_item_browsing_view_voucher_record_page_order != "") {
        $order = $ck_category_item_browsing_view_voucher_record_page_order;
    }

if ($ck_category_item_browsing_view_voucher_record_page_field != $field && $field != "") {
    setcookie("ck_category_item_browsing_view_voucher_record_page_field", $field, 0, "", "", 0);
    $ck_category_item_browsing_view_voucher_record_page_field = $field;
} else 
    if (! isset($field) && $ck_category_item_browsing_view_voucher_record_page_field != "") {
        $field = $ck_category_item_browsing_view_voucher_record_page_field;
    }

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$canEdit = ($linventory->getAccessLevel($UserID) != 3) ? 1 : 0;

// Generate System Message #
/*
 * if ($xmsg != "") {
 * $SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
 * } else {
 * if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
 * if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
 * if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
 * if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
 * if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_ItemFullList_DeleteItemFail");
 * if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
 * if ($msg == 15) $SysMsg = $linterface->GET_SYS_MSG("","$i_InventorySystem_Warning_SetBulkItemAsResourceItem");
 * }
 */
// End #

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);

// get the name of infobar #
$namefield1 = $linventory->getInventoryItemNameByLang("a.");
$namefield2 = $linventory->getInventoryItemNameByLang("b.");
$namefield3 = $linventory->getInventoryItemNameByLang("c.");
$sql = "SELECT 
				a.ItemCode, 
				$namefield1				
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
		WHERE
				a.ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 2);

$temp[] = array(
    "<a href=\"items_full_list.php\">$i_InventorySystem_BackToFullList</a>"
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

$titlebar .= "<tr><td align=\"left\">";
$titlebar .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_section.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" />";
$titlebar .= "<span class=\"sectiontitle\">" . $result[0][0] . " - " . $result[0][1] . "</span>";
$titlebar .= "</td>";
$titlebar .= "<td align=\"right\">$toolbar";
$titlebar .= "</td></tr>";
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

// Gen the table tool (edit) #
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'RecordID[]','invoice_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 1);

if (sizeof($result) > 0) {
    list ($item_type) = $result[0];
}

if ($item_type == 1) {
    $sql = "SELECT AttachmentPath, FileName FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE ItemID = '".$item_id."'";
    $linventory->returnArray($sql, 2);
}

if ($item_type == 2) {
    $checkbox_display = $canEdit ? ",CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' value=',a.RecordID,'>')" : "";
    if (! $sys_custom['eInventoryCustForSMC']) {
        $sql_purchase_price = "CONCAT('$',a.PurchasedPrice),";
    }
    $sql = "SELECT
					a.InvoiceNo,
					a.SupplierName,
					a.PurchaseDate,
					a.QtyChange,
					$sql_purchase_price
					CONCAT('$',a.UnitPrice), 
					" . getNameFieldByLang2("b.") . "
					$checkbox_display
			FROM
					INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
					INTRANET_USER AS b ON (a.PersonInCharge = b.UserID)
			WHERE
					ItemID = '".$item_id."' AND
					Action = 1";
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$field = $field == "" ? 1 : $field;
if (! isset($order))
    $order = 0;
    
    // TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
if (! $sys_custom['eInventoryCustForSMC']) {
    $li->field_array = array(
        "",
        "a.InvoiceNo",
        "a.SupplierName",
        "a.PurchaseDate",
        "a.QtyChange",
        "a.PurchasedPrice",
        "a.UnitPrice",
        "b.EnglishName",
        ""
    );
} else {
    $li->field_array = array(
        "",
        "a.InvoiceNo",
        "a.SupplierName",
        "a.PurchaseDate",
        "a.QtyChange",
        "a.UnitPrice",
        "b.EnglishName",
        ""
    );
}
$li->sql = $sql;
if ($canEdit)
    $li->no_col = sizeof($li->field_array);
else
    $li->no_col = sizeof($li->field_array) - 1;
$li->title = "";
$li->column_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->IsColOff = 2;

// TABLE COLUMN
$pos = 1;
$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, "$i_InventorySystem_Item_Invoice_Num") . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, "$i_InventorySystem_Item_Supplier_Name") . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, "$i_InventorySystem_Item_Purchase_Date") . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, "$i_InventorySystem_Item_Qty") . "</td>\n";
if (! $sys_custom['eInventoryCustForSMC'])
    $li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, "$i_InventorySystem_Item_Price") . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, "$i_InventorySystem_Unit_Price") . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, "$i_InventorySystem_Item_PIC") . "</td>\n";
if ($canEdit)
    $li->column_list .= "<td width='1'>" . $li->check("RecordID[]") . "</td>\n";
    // echo $li->built_sql();
?>

<br>

<form name="form1" action="" method="POST">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
<?=$infobar1?>
</table>
	<br>
	<br>
	<table width="96%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<?=$titlebar?>
</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="5"
		align="center">
		<tr height="10px">
			<td></td>
		</tr>
<? if($item_type == 1) { ?>
<tr>
			<td>
				<table border=0 align="center" width="96%">
					<tr class="single">
						<td height="20px" valign="top" nowrap="nowrap" class="tabletext">
							<a
							href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_history.php?type=5&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<? } ?>
<? if($item_type == 2) { ?>
<tr>
			<td>
				<table border=0 align="center" width="96%">
					<tr class="bulk">
						<td height="20px" valign="top" nowrap="nowrap" class="tabletext">
							<a
							href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_location.php?type=2&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==2) echo"<B>"; else echo " "; ?><?=$i_InventorySystem['Location']." ".$i_InventorySystem_Search_And2." ".$i_InventorySystem['Caretaker']?><? IF($type==2) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<!--<a href="category_show_items_group.php?type=3&item_id=<?=$item_id?>" class="tablegreenlink"><? IF($type==3) echo"<B>"; else echo " "; ?><?=$i_InventorySystem['Caretaker']?><? IF($type==3) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;-->
							<a
							href="category_show_items_invoice.php?type=4&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==4) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Invoice?><? IF($type==4) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_history.php?type=5&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<? } ?>
</table>

	<table width="90%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right" class="tabletext"><?= $SysMsg ?></td>
		</tr>
		<tr height="10px">
			<td colspan="2"></td>
		</tr>
	</table>

<? if($canEdit) {?>
<table width="90%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr height="3px">
			<td></td>
		</tr>
		<tr>
			<td colspan="" valign="bottom" align="right">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif"
							width="21" height="23"></td>
						<td
							background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
								<?=$table_tool?>
							</tr>
							</table>
						</td>
						<td width="6"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif"
							width="6" height="23"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<? } ?>

<?echo $li->display("90%"); ?>
<input type="hidden" name="Type" value="<?=$type;?>"> <input
		type="hidden" name="ItemID" value="<?=$item_id;?>"> <input
		type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"> <input
		type="hidden" name="order" value="<?php echo $li->order; ?>"> <input
		type="hidden" name="field" value="<?php echo $li->field; ?>"> <input
		type="hidden" name="page_size_change" value=""> <input type="hidden"
		name="numPerPage" value="<?=$li->page_size?>">
</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>