<?php
// Using : Henry

// ####################################### Change Log ######################################
// Date : 2019-04-30 (Henry)
// Details : scurity issue fix for SQL
//
// Date : 2018-02-07 (Henry)
// Details : add access right checking [Case#E135442]
//
// Date : 20170602 (By Henry)
// Details : item code cust for SPCCPS [Case#P115830]
//
// Date : 20140116 (By YatWoon)
// Details : use strrpos rather than strpos, to prevent Categorycode with _ [Case#D57665]
//
// Date : 20100106 (By Ronald)
// Details : Modified the sql to get the latest item code, now will only get the
// latest item code base on the select category and sub-category.
//
// #########################################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$arr_final_item_code = array();

$item_cat1 = IntegerSafe($item_cat1);
$item_cat2 = IntegerSafe($item_cat2);

// get Category Code #
$sql = "SELECT Code FROM INVENTORY_CATEGORY WHERE CategoryID = '".$item_cat1."'";
$arr_Cat1Code = $linventory->returnArray($sql, 1);

// get Category2 Code #
$sql = "SELECT Code FROM INVENTORY_CATEGORY_LEVEL2 WHERE Category2ID = '".$item_cat2."' AND CategoryID = '".$item_cat1."'";
$arr_Cat2Code = $linventory->returnArray($sql, 1);

if (sizeof($arr_Cat1Code) > 0) {
    list ($cat1_code) = $arr_Cat1Code[0];
}
if (sizeof($arr_Cat2Code) > 0) {
    list ($cat2_code) = $arr_Cat2Code[0];
}

// set the item code prefix
if (isset($sys_custom['eInventory_ItemCodeNoUnderscore']) && $sys_custom['eInventory_ItemCodeNoUnderscore']) {
    $prefixItemCode = $cat1_code . $cat2_code;
} else 
    if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) {
        list ($y, $m, $d) = explode("-", date('Y-m-d'));
        $item_year = $m . $d >= "0901" ? substr($y, 2, 2) : substr($y - 1, 2, 2);
        $prefixItemCode = "SPCCPS" . $cat1_code . $cat2_code . $item_year . "-";
    } else {
        $prefixItemCode = $cat1_code . $cat2_code . "_";
    }

// set item code num length
if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) {
    $itemCodeNumLength = 3;
} else {
    $itemCodeNumLength = DEFAULT_ITEM_CODE_NUM_LENGTH;
}

// get total num of item code #
// $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID = $item_cat1 AND Category2ID = $item_cat2"; ### old method - count no of item
// $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = $item_cat1 AND Category2ID = $item_cat2"; ### new method - get later item code
// $sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = $item_cat1 AND Category2ID = $item_cat2 AND ItemCode LIKE '$prefixItemCode%' "; ### new method - get later item code with base on the selected cat and sub-cat
$sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE ItemCode LIKE '".$linventory->Get_Safe_Sql_Like_Query($prefixItemCode)."%' "; // ## new method - get later item code with base on the prefix
$arr_ItemCode = $linventory->returnArray($sql, 1);
if (sizeof($arr_ItemCode) > 0) {
    list ($num_ItemCode) = $arr_ItemCode[0];
    
    if (isset($sys_custom['eInventory_ItemCodeNoUnderscore']) && $sys_custom['eInventory_ItemCodeNoUnderscore']) {
        $start_pos = strlen($prefixItemCode);
    } else 
        if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) {
            $start_pos = strrpos($num_ItemCode, "-");
        } else {
            // $start_pos = strpos($num_ItemCode,"_");
            $start_pos = strrpos($num_ItemCode, "_");
        }
    
    $num_ItemCode = substr($num_ItemCode, $start_pos + 1, strlen($num_ItemCode));
    
    if ($item_type == ITEM_TYPE_SINGLE) {
        /*
         * OLD Method
         * for($i=0; $i<$total_item; $i++)
         * {
         * $ItemCodePrefix = "";
         * $start_pos = $num_ItemCode+1;
         * $length = strlen($start_pos);
         *
         * if($length < DEFAULT_ITEM_CODE_NUM_LENGTH)
         * {
         * for($j=0; $j<(DEFAULT_ITEM_CODE_NUM_LENGTH - $length); $j++)
         * {
         * $default_prefix = "0";
         * $ItemCodePrefix .= $default_prefix;
         * }
         * $num_ItemCode = $ItemCodePrefix.$start_pos;
         * $new_item_code = $cat1_code.$cat2_code."_".$num_ItemCode;
         * array_push($arr_final_item_code,$new_item_code);
         * }else{
         * $new_item_code = $cat1_code.$cat2_code."_".$start_pos;
         * array_push($arr_final_item_code,$new_item_code);
         * }
         * }
         */
        
        /* NEW method */
        while (sizeof($arr_final_item_code) != $total_item) {
            // $i++;
            $ItemCodePrefix = "";
            $start_pos = $num_ItemCode + 1;
            $length = strlen($start_pos);
            if ($length < $itemCodeNumLength) {
                for ($j = 0; $j < ($itemCodeNumLength - $length); $j ++) {
                    $default_prefix = "0";
                    $ItemCodePrefix .= $default_prefix;
                }
                $num_ItemCode = $ItemCodePrefix . $start_pos;
                
                if (isset($sys_custom['eInventory_ItemCodeNoUnderscore']) && $sys_custom['eInventory_ItemCodeNoUnderscore'])
                    $new_item_code = $cat1_code . $cat2_code . $num_ItemCode;
                else 
                    if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS'])
                        $new_item_code = "SPCCPS" . $cat1_code . $cat2_code . $item_year . "-" . $num_ItemCode;
                    else
                        $new_item_code = $cat1_code . $cat2_code . "_" . $num_ItemCode;
                // array_push($arr_final_item_code,$new_item_code);
            } else {
                if (isset($sys_custom['eInventory_ItemCodeNoUnderscore']) && $sys_custom['eInventory_ItemCodeNoUnderscore'])
                    $new_item_code = $cat1_code . $cat2_code . $start_pos;
                else 
                    if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS'])
                        $new_item_code = "SPCCPS" . $cat1_code . $cat2_code . $item_year . "-" . $start_pos;
                    else
                        $new_item_code = $cat1_code . $cat2_code . "_" . $start_pos;
                // array_push($arr_final_item_code,$new_item_code);
            }
            $sql = "SELECT ItemCode FROM INVENTORY_ITEM WHERE ItemCode = '$new_item_code'";
            $arr_tmp_item_code = $linventory->returnVector($sql);
            if ((sizeof($arr_tmp_item_code) == 0) && (! array_search($arr_tmp_item_code, $arr_final_item_code)))
                array_push($arr_final_item_code, $new_item_code);
        }
    }
    if ($item_type == ITEM_TYPE_BULK) {
        while (sizeof($arr_final_item_code) != $total_item) {
            $ItemCodePrefix = "";
            $start_pos = $num_ItemCode + 1;
            $length = strlen($start_pos);
            if ($length < $itemCodeNumLength) {
                for ($j = 0; $j < ($itemCodeNumLength - $length); $j ++) {
                    $default_prefix = "0";
                    $ItemCodePrefix .= $default_prefix;
                }
                $num_ItemCode = $ItemCodePrefix . $start_pos;
                
                if (isset($sys_custom['eInventory_ItemCodeNoUnderscore']) && $sys_custom['eInventory_ItemCodeNoUnderscore'])
                    $new_item_code = $cat1_code . $cat2_code . $num_ItemCode;
                else 
                    if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS'])
                        $new_item_code = "SPCCPS" . $cat1_code . $cat2_code . $item_year . "-" . $num_ItemCode;
                    else
                        $new_item_code = $cat1_code . $cat2_code . "_" . $num_ItemCode;
                // array_push($arr_final_item_code,$new_item_code);
            } else {
                if (isset($sys_custom['eInventory_ItemCodeNoUnderscore']) && $sys_custom['eInventory_ItemCodeNoUnderscore'])
                    $new_item_code = $cat1_code . $cat2_code . $start_pos;
                else 
                    if (isset($sys_custom['eInventory_ItemCodeFormat']['SPCCPS']) && $sys_custom['eInventory_ItemCodeFormat']['SPCCPS'])
                        $new_item_code = "SPCCPS" . $cat1_code . $cat2_code . $item_year . "-" . $start_pos;
                    else
                        $new_item_code = $cat1_code . $cat2_code . "_" . $start_pos;
                // array_push($arr_final_item_code,$new_item_code);
            }
            $sql = "SELECT ItemCode FROM INVENTORY_ITEM WHERE ItemCode = '$new_item_code'";
            $arr_tmp_item_code = $linventory->returnVector($sql);
            if ((sizeof($arr_tmp_item_code) == 0) && (! array_search($arr_tmp_item_code, $arr_final_item_code)))
                array_push($arr_final_item_code, $new_item_code);
        }
    }
}

if (sizeof($arr_final_item_code) > 0)
    $response = implode(",", $arr_final_item_code);

echo $response;
?>