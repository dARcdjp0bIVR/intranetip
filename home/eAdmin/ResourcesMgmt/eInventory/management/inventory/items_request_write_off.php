<?php
// using:

// ################################
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-21 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2017-11-07 (Henry)
// fix: cannot upload more than one attachments
//
// Date: 2017-06-27 (Henry)
// display the name of left staff [Case#V119089]
//
// Date: 2013-09-11 YatWoon
// Improved: set submit button disabled during the form is processing [Case#2013-0910-1641-00073]
//
// Date: 2013-01-28 YatWoon
// display "User account not exists." if requestor account is removed. [Case#2013-0108-1025-29156]
//
// Date: 2012-07-16 (YatWoon)
// update download_attachment.php with encrypt logic
//
// Date: 2012-05-25 YatWoon
// add "group and funding" selection for bulk item
//
// Date: 2012-03-13 YatWoon
// Improved: No need display the "note" if the request action is just processed. [Case#2012-0228-1005-07071]
//
// Date: 2011-11-18 YatWoon
// Improved: update logic, if the item is waiting for approval (write-off), then other user cannot request again, and original requestor can cancel the request
//
// ################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$no_of_file = 1;

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$item_id = $ItemID ? $ItemID : $item_id;
$item_location_id = $ItemLocation ? $ItemLocation : $item_location_id;

// $TAGS_OBJ[] = array($i_InventorySystem_Item_UpdateStatus, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_status_edit.php?item_id=$item_id&targetLocation=$targetLocation", 0);
// $TAGS_OBJ[] = array($i_InventorySystem_Item_UpdateLocation, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_location_edit.php?item_id=$item_id&targetOldLocation=$targetLocation", 0);
// $TAGS_OBJ[] = array($i_InventorySystem_Item_RequestWriteOff, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_request_write_off.php?item_id=$item_id&targetLocation=$targetLocation", 1);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_UpdateStatus,
    "javascript:click_tab('update_status');",
    0
);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_UpdateLocation,
    "javascript:click_tab('change_location');",
    0
);
$TAGS_OBJ[] = array(
    $i_InventorySystem_Item_RequestWriteOff,
    "javascript:click_tab('request_writeoff');",
    1
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$sql = "SELECT 
				" . $linventory->getInventoryItemNameByLang("a.") . ",
				" . $linventory->getInventoryItemNameByLang("b.") . ",
				" . $linventory->getInventoryItemNameByLang("c.") . "
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
		WHERE
				a.ItemID = $item_id";
$result = $linventory->returnArray($sql, 3);

$PAGE_NAVIGATION[] = array(
    $result[0][1] . " > " . $result[0][2] . " > " . $result[0][0] . " > " . $Lang['eInventory']['FieldTitle']['ItemRequestWriteOff']
);

// $infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>";
// $infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($result[0][2]." > ".$result[0][0]." > ".$Lang['eInventory']['FieldTitle']['ItemRequestWriteOff'])."</td></tr>";

// Get User Inventory Admin Group #
$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
$arr_result = $linventory->returnVector($sql);
if (sizeof($arr_result) > 0) {
    $inventory_admin_group = implode(",", $arr_result);
}
// end of getting user inventory admin group #

// Get Default Write Off Reason #
$sql = "SELECT ReasonTypeID, " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE";
$arr_write_off_reason = $linventory->returnArray($sql, 2);
$reason_selection = getSelectByArray($arr_write_off_reason, " name=\"targetWriteOffReason\" ");
// End #

// Get Item Type #
$sql = "SELECT ItemType, " . $linventory->getInventoryItemNameByLang("") . " FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$arr_result = $linventory->returnArray($sql);
if (sizeof($arr_result) > 0) {
    list ($item_type, $item_name) = $arr_result[0];
}
// End of getting item type #

if ($item_type == ITEM_TYPE_SINGLE) {
    $sql = "SELECT 
					" . $linventory->getInventoryItemNameByLang("a.") . ",
					c.LocationID,
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocFloor.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
					d.AdminGroupID,
					" . $linventory->getInventoryNameByLang("d.") . "					
			FROM 
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS LocFloor ON (c.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID) INNER JOIN
					INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID)
			WHERE
					a.ItemID = '".$item_id."'
			";
    $arr_result = $linventory->returnArray($sql, 5);
    
    if (sizeof($arr_result) > 0) {
        list ($item_name, $item_location_id, $item_location_name, $item_admin_group_id, $item_admin_group_name) = $arr_result[0];
        $targetLocation = $item_location_id;
        /*
         * $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Item_Name</td><td class=\"tabletext\">$item_name</td></tr>\n";
         * $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Location</td><td class=\"tabletext\">$item_location_name</td></tr>\n";
         * $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Group_Name</td><td class=\"tabletext\">$item_admin_group_name</td></tr>\n";
         * $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_WriteOffReason_Name</td><td class=\"tabletext\">$reason_selection</td></tr>\n";
         */
        $sql = "SELECT ResourceID FROM INTRANET_RESOURCE_INVENTORY_ITEM_LIST WHERE ItemID = $item_id";
        $arr_is_resource_item = $linventory->returnVector($sql);
        if (sizeof($arr_is_resource_item) > 0) {
            $arr_resource_item_status = array(
                array(
                    1,
                    "$i_InventorySystem_RequestWriteOff_ResourceItemStatus1"
                ),
                array(
                    2,
                    "$i_InventorySystem_RequestWriteOff_ResourceItemStatus2"
                ),
                array(
                    3,
                    "$i_InventorySystem_RequestWriteOff_ResourceItemStatus3"
                )
            );
            $resoruce_item_status_selection = getSelectByArray($arr_resource_item_status, " name=\"resource_item_status\" ");
            // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_RequestWriteOff_ResourceItemStatusSelection</td><td>$resoruce_item_status_selection</td></tr>";
            $is_resource_item = "<tr valign='top'>";
            $is_resource_item .= "<td nowrap class='field_title'>" . $i_InventorySystem_WriteOffReason_Name . "</td>";
            $is_resource_item .= "<td>" . $reason_selection . "</td>";
            $is_resource_item .= "</tr>";
        }
        
        /*
         * $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Write_Off_Attachment</td>";
         * $table_content .= "<td>";
         * $table_content .= "
         * <table border=\"0\" id=\"upload_file_list\" cellpadding=\"0\" cellspacing=\"0\" >
         *
         * <script language=\"javascript\">
         *
         * for(i=0;i<no_of_upload_file;i++)
         * {
         * document.writeln('<tr><td><input class=\"file\" type=\"file\" name=\"item_write_off_attachment_'+i+'\" size=\"40\">');
         * document.writeln('<input type=\"hidden\" name=\"hidden_item_write_off_attachment_'+i+'\"></td></tr>');
         * }
         * </script>
         * </table>
         * <input name=\"\" type=\"button\" value=\" + \" onClick=\"add_field()\">
         * <input type=\"hidden\" name=\"no_of_file_upload\" value=\"\">
         * ";
         * $table_content .= "</td>";
         * $table_content .= "</tr>";
         */
        // $table_content .= "<input type=\"hidden\" name=\"hidden_write_off_attachment\" value=\"\">";
        // $table_content .= "<input type=\"hidden\" name=\"ItemID\" value=\"$item_id\">\n";
        // $table_content .= "<input type=\"hidden\" name=\"ItemType\" value=\"$item_type\">\n";
        // $table_content .= "<input type=\"hidden\" name=\"ItemLocation\" value=\"$item_location_id\">\n";
        // $table_content .= "<input type=\"hidden\" name=\"ItemAdminGroup\" value=\"$item_admin_group_id\">\n";
        
        // echo generateFileUploadNameHandler("form1","write_off_attachment","hidden_write_off_attachment");
    }
}
if ($item_type == ITEM_TYPE_BULK) {
    // get Item Location #
    if (! $linventory->IS_ADMIN_USER($UserID)) {
        $admin_group_list = $linventory->getInventoryAdminGroup();
        $group_con = " AND GroupInCharge IN ($admin_group_list)";
    }
    $sql = "SELECT 
					DISTINCT b.LocationID,
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocFloor.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
			FROM
					INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
					INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS LocFloor ON (b.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID)
			WHERE
					ItemID = $item_id 
					$group_con
			order by LocBuilding.DisplayOrder, LocFloor.DisplayOrder, b.DisplayOrder
			";
    $arr_location = $linventory->returnArray($sql, 2);
    $location_selection = getSelectByArray($arr_location, " name=\"targetLocation\" onChange=\"change_filter('location');\" ", "$targetLocation", 0, 1);
    $item_location_name = $location_selection;
    // End #
    
    // group #
    if (! $targetGroupID) {
        // retrieve the first groupid
        $check_own_group = empty($admin_group_list) ? "" : " and GroupInCharge in ($admin_group_list)";
        $sql = "select GroupInCharge from INVENTORY_ITEM_BULK_LOCATION where LocationID = $targetLocation and ItemID = $item_id $check_own_group limit 1";
        $result = $linventory->returnVector($sql);
        $targetGroupID = $result[0];
    }
    $check_own_group = empty($admin_group_list) ? "" : " and c.GroupInCharge in ($admin_group_list)";
    $sql = "SELECT 
				distinct(a.AdminGroupID), 
				" . $linventory->getInventoryNameByLang() . " as GroupName
			FROM 
				INVENTORY_ADMIN_GROUP as a 
				left join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID and b.RecordType=1)
				left join INVENTORY_ITEM_BULK_LOCATION as c on (c.GroupInCharge=a.AdminGroupID and c.LocationID = $targetLocation)
			WHERE 
				c.ItemID = $item_id
				$check_own_group
			 ORDER BY 
			 	a.DisplayOrder
			";
    $group_array = $linventory->returnArray($sql);
    $group_selection = getSelectByArray($group_array, "name=targetGroupID onChange=\"change_filter('group');\" ", $targetGroupID, 0, 1);
    // end group #
    
    // funding #
    if (! $targetFundingID) {
        // retrieve the first groupid
        $sql = "select 
					a.FundingSourceID 
				from 
					INVENTORY_ITEM_BULK_LOCATION as a
					left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSourceID
				where 
					a.LocationID = $targetLocation and 
					a.ItemID = $item_id and 
					a.GroupInCharge=$targetGroupID 
				ORDER BY 
					b.DisplayOrder 
				limit 1
				";
        $result = $linventory->returnVector($sql);
        $targetFundingID = $result[0];
    }
    
    $sql = "SELECT 
				a.FundingSourceID, 
				" . $linventory->getInventoryItemNameByLang() . "
			FROM 
				INVENTORY_FUNDING_SOURCE as a
				left join INVENTORY_ITEM_BULK_LOCATION as b on (b.FundingSourceID=a.FundingSourceID and b.LocationID = $targetLocation and b.GroupInCharge=$targetGroupID)
			where 	
				b.ItemID = $item_id
			order by 
				a.DisplayOrder";
    $arr_funding_source = $linventory->returnArray($sql);
    $funding_selection = getSelectByArray($arr_funding_source, " name=\"targetFundingID\" onChange=\"change_filter('funding');\" ", $targetFundingID, 0, 1);
    // end funding #
    
    //
    // if($targetLocation != "")
    // {
    // $item_location_id = $targetLocation;
    
    $sql = "SELECT 
							Quantity
					FROM
							INVENTORY_ITEM_BULK_LOCATION
					WHERE
							ItemID = $item_id AND
							LocationID = $targetLocation and
							GroupInCharge = $targetGroupID and
							FundingSourceID = $targetFundingID
					";
    $arr_result = $linventory->returnArray($sql, 1);
    if (sizeof($arr_result) > 0) {
        list ($item_qty_assigned) = $arr_result[0];
    }
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_StockAssigned</td><td class=\"tabletext\">$item_qty_assigned</td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_WriteOffQty</td><td><input type=\"text\" class=\"textboxnum\" name=\"item_write_off_qty\"></td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_WriteOffReason_Name</td><td class=\"tabletext\">$reason_selection</td></tr>\n";
    // $table_content .= "<tr><td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_Write_Off_Attachment</td>";
    //
    // $table_content .= "<td>";
    // $table_content .= "
    // <table border=\"0\" id=\"upload_file_list\" cellpadding=\"0\" cellspacing=\"0\" >
    //
    // <script language=\"javascript\">
    //
    // for(i=0;i<no_of_upload_file;i++)
    // {
    // document.writeln('<tr><td><input class=\"file\" type=\"file\" name=\"item_write_off_attachment_'+i+'\" size=\"40\">');
    // document.writeln('<input type=\"hidden\" name=\"hidden_item_write_off_attachment_'+i+'\"></td></tr>');
    // }
    // </script>
    // </table>
    // <input name=\"\" type=\"button\" value=\" + \" onClick=\"add_field()\">
    // <input type=\"hidden\" name=\"no_of_file_upload\" value=\"\">
    // ";
    // $table_content .= "</td>";
    // $table_content .= "</tr>";
    
    // $table_content .= "<input type=\"hidden\" name=\"ItemID\" value=\"$item_id\">\n";
    // $table_content .= "<input type=\"hidden\" name=\"ItemType\" value=\"$item_type\">\n";
    // $table_content .= "<input type=\"hidden\" name=\"ItemLocation\" value=\"$targetLocation\">\n";
    // $table_content .= "<input type=\"hidden\" name=\"ItemAdminGroup\" value=\"$targetAdminGroup\">\n";
    // }
}

// check is there someone else request write-off already
$sql = "select 
			RecordID, RequestDate, RequestPerson,
			if(b.UserID is NULL, IF(ab.UserID IS NULL, '<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<font color=\"red\">*</font>'," . getNameFieldByLang2("ab.") . ")), " . getNameFieldByLang2("b.") . "),
			WriteOffQty, WriteOffReason
		from 
			INVENTORY_ITEM_WRITE_OFF_RECORD as a
			left join INTRANET_USER as b on b.UserID=a.RequestPerson
			left JOIN INTRANET_ARCHIVE_USER as ab ON (a.RequestPerson = ab.UserID)
		where 
			ItemID=$item_id and LocationID=$targetLocation and a.RecordStatus=0 ";
if ($item_type == ITEM_TYPE_BULK) {
    $sql .= " and AdminGroupID=$targetGroupID and FundingSourceID=$targetFundingID";
}
$result = $linventory->returnArray($sql);
if (! empty($result))
    list ($this_recordid, $this_requestdate, $this_requestbyID, $this_requestby, $this_qty, $this_reason) = $result[0];

$display_request_reason = $this_reason ? $this_reason : $reason_selection;
$display_qty = $this_qty ? $this_qty : "<input type=\"text\" class=\"textboxnum\" name=\"item_write_off_qty\">";

if ($this_recordid) {
    $sql = "SELECT PhotoPath, PhotoName FROM INVENTORY_ITEM_WRITE_OFF_ATTACHMENT WHERE RequestWriteOffID = '" . $this_recordid."'";
    $arr_attachment = $linventory->returnArray($sql, 2);
    if (sizeof($arr_attachment) > 0) {
        for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
            list ($photo_path, $photo_name) = $arr_attachment[$j];
            $write_off_photo = $photo_path . "/" . $photo_name;
            // $url = rawurlencode($file_path.$write_off_photo);
            $url = $file_path . $write_off_photo;
            $this_attachment .= "<a class=\"tablelink\" target=\"_blank\" href=\"/home/download_attachment.php?target_e=" . getEncryptedText($url) . "\" >" . $photo_name . "</a><br>";
        }
    }
}

?>
<script language=javascript>
var no_of_upload_file = <?echo $no_file ==""?1:$no_file;?>;
</script>

<script language="javascript">
function add_field()
{
	var tmpObj = "upload_file_list";
	var table = document.getElementById(tmpObj); 
	var tmp_no_file = eval("document.form1.no_of_file_upload");
	var tmp_val = tmp_no_file.value;
	if(tmp_val == "")
	{
		tmp_val = 1;
		no_of_upload_file = 1;
	}
	var row = table.insertRow(tmp_val);
	
	if (document.getElementById)
	{
		var cell = row.insertCell(0);
	
		x= '<input class="file" type="file" name="item_write_off_attachment_'+tmp_val+'" size="40">';
		x+='<input type="hidden" name="hidden_item_write_off_attachment_'+tmp_val+'">';
		
		cell.innerHTML = x;
		no_of_upload_file++;
		
		var tmp_no_file = eval("document.form1.no_of_file_upload");
		tmp_no_file.value = no_of_upload_file;
	}
}

function Big5FileUploadHandler() 
{
	var file_value = eval("document.form1.no_of_file_upload");
	if(file_value != null)
	{
		upload_file_value = file_value.value;
		if(upload_file_value == '') 
			upload_file_value = 0;
	}
	else
	{
		upload_file_value = 0;
	}
	
	for(j=0;j<=no_of_upload_file;j++)
	{
		objWriteOffFile = eval('document.form1.item_write_off_attachment_'+j);
		objHiddenWriteOffFile = eval('document.form1.hidden_item_write_off_attachment_'+j);
		if(objWriteOffFile!=null && objWriteOffFile.value!='' && objHiddenWriteOffFile!=null)
		{
			var Ary = objWriteOffFile.value.split('\\');
			objHiddenWriteOffFile.value = Ary[Ary.length-1];
		}
	}
	
	return true;
}


function checkForm()
{
	<?
if ($item_qty_assigned != "") {
    ?>
		var assigned_item_qty = <?=$item_qty_assigned;?>;
	<?
}
?>
	var item_type = <?=$item_type;?>;
	obj = document.form1;
		
	if(item_type == 2)
	{
		if(obj.flag.value == 1)
		{
			if(check_positive_nonzero_int(obj.item_write_off_qty,"<?=$i_InventorySystem_UpdateItemRequestWriteOffQty_Warning?>"))
			{
				if(obj.item_write_off_qty.value <= assigned_item_qty)
				{
					if(check_select(document.form1.targetWriteOffReason,"<?=$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning;?>",0))
					{
						document.getElementById('submit_btn').disabled  = true; 
						document.getElementById('submit_btn').className  = "formbutton_disable print_hide"; 
		
						Big5FileUploadHandler();
						obj.action = "items_request_write_off_update.php";
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					alert("<?=$i_InventorySystem_UpdateItemRequestWriteOffQty_Warning;?>");
					return false;
				}
			}
			else
			{
				return false;
			}
		}
	}
	else
	{
		var resource_item_check = 1;
		var write_off_reason_check = 1;
		
		if(check_select(document.form1.targetWriteOffReason,"<?=$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning;?>",0))
		{
			write_off_reason_check = 1;
		}
		else
		{
			write_off_reason_check = 0;
			return false;
		}
		
		if(<?echo sizeof($arr_is_resource_item);?> > 0)
		{
			if(check_select(document.form1.resource_item_status,"Please select the resource item setting",0))
			{
				resource_item_check = 1;
			}
			else
			{
				resource_item_check = 0;
				return fales;
			}
		}
		
		if((write_off_reason_check == 1) && (resource_item_check == 1))
		{
			document.getElementById('submit_btn').disabled  = true; 
			document.getElementById('submit_btn').className  = "formbutton_disable print_hide"; 
		
			Big5FileUploadHandler();
			obj.action = "items_request_write_off_update.php";
			return true;
		}
		else
		{
			return false;
		}
	}
}

function cancel_request()
{
	if(confirm("<?=$Lang['eInventory']['JSWarning']['SureCancelRequest']?>")) 
	{
		document.form1.action = "items_request_write_off_cancel.php";
		document.form1.submit();
	}
}

function change_filter(filter)
{
	var obj = document.form1;
	
	if(filter=="location")
	{
		obj.targetGroupID.selectedIndex=-1; 
		obj.targetFundingID.selectedIndex=-1; 
	}
	
	if(filter=="group")
	{
		obj.targetFundingID.selectedIndex=-1; 
	}
	
	obj.action="items_request_write_off.php";
	obj.submit();
}

function click_tab(tab)
{
	var obj = document.form1;
	if(tab=="update_status")
		obj.action="items_status_edit.php";
	if(tab=="change_location")
		obj.action="items_location_edit.php";
	if(tab=="request_writeoff")
		obj.action="items_request_write_off.php";
	obj.submit();
}
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<form name="form1" enctype="multipart/form-data"
	action="items_request_write_off.php" method="post"
	onSubmit="return checkForm();">


	<div class="form_table_content">

<? if($this_recordid && !$xmsg) {?> 
<fieldset class="instruction_warning_box_v30">
			<legend><?=$Lang['General']['Note']?></legend>
		<?=$Lang['eInventory']['WritOffWaitingForApproval']?>
</fieldset>
<? } ?>

<table class="form_table_v30">
<? 
/*
    * ?>
    * <tr valign='top'>
    * <td nowrap class='field_title'><?=$i_InventorySystem_Item_Name?></td>
    * <td><?=$item_name?></td>
    * </tr>
    * <?
    */
?>

<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_Location_Level?></td>
				<td><?=$item_location_name?></td>
			</tr>

<? if($item_type == ITEM_TYPE_SINGLE) {?>
	<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_Group_Name?></td>
				<td><?=$item_admin_group_name?></td>
			</tr> 
<? } else {?>
	<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem['Caretaker']?></td>
				<td><?=$group_selection?></td>
			</tr>

			<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem['FundingSource']?></td>
				<td><?=$funding_selection?></td>
			</tr>

			<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_StockAssigned?></td>
				<td><?=$item_qty_assigned?></td>
			</tr>

			<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_WriteOffQty?></td>
				<td><?=$display_qty?></td>
			</tr> 
	
<? } ?>

<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_WriteOffReason_Name?></td>
				<td><?=$display_request_reason?></td>
			</tr>

<?=$is_resource_item?>

<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_Write_Off_Attachment?></td>
				<td>
		<? if(!$this_recordid) {?>
			<table border="0" id="upload_file_list" cellpadding="0"
						cellspacing="0" class="inside_form_table">
						<script language="javascript">
			
			for(i=0;i<no_of_upload_file;i++)
			{
				document.writeln('<tr><td><input class="file" type="file" name="item_write_off_attachment_'+i+'" size="40">');
			    document.writeln('<input type="hidden" name="hidden_item_write_off_attachment_'+i+'"></td></tr>');
			}
			</script>
					</table> <input name="" type="button" value=" + "
					onClick="add_field()"> <input type="hidden"
					name="no_of_file_upload" value="">
		<? } else {?>
			<?=$this_attachment ? $this_attachment : "---"?>
		<? } ?>
	
	</td>
			</tr>

<? if($this_recordid) {?>
	<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_RequestPerson?></td>
				<td><?=$this_requestby?></td>
			</tr>

			<tr valign='top'>
				<td nowrap class='field_title'><?=$i_InventorySystem_Write_Off_RequestTime?></td>
				<td><?=$this_requestdate?></td>
			</tr> 
<? } ?>

</table>
		<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>
	</div>

	<div class="edit_bottom_v30">
		<p class="spacer"></p>
	<? if(!$this_recordid) {?>
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit_btn")?> 
		<?=$linterface->GET_ACTION_BTN($button_reset, "reset")?> 
	<?  } else if($UserID==$this_requestbyID) {?>
		<?=$linterface->GET_ACTION_BTN($Lang['eInventory']['CancelRequest'], "button","cancel_request();","","","","formbutton_alert")?> 
	<? } ?>
	<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='category_show_items_detail.php?type=$item_type&item_id=$item_id'")?>
<p class="spacer"></p>
	</div>

	<input type="hidden" name="flag" value="1"> <input type="hidden"
		name="item_id" value="<?=$item_id?>"> <input type="hidden"
		name="ItemType" value="<?=$item_type?>"> <input type="hidden"
		name="ItemAdminGroup" value="<?=$item_admin_group_id?>"> <input
		type="hidden" name="WriteOffRecordID" value="<?=$this_recordid?>">
<? if($item_type == ITEM_TYPE_SINGLE) {?>
<input type="hidden" name="targetLocation"
		value="<?=$item_location_id?>">
<? } ?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>