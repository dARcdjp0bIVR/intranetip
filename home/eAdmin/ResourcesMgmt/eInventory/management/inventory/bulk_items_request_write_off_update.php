<?php

//Using: 

/****************************** Change Log [Start] *******************************
 * 2012-10-30 Rita revise insert attachment SQL add Get_Safe_Sql_Query
 ******************************* Change Log [End] *********************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory	= new libinventory();
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");
$re_path = "/file/inventory/write_off";
$path = "$intranet_root$re_path";

# Get item ID from form 
$item_id = $_POST['item_id'];

foreach ($item_id as $ItemID)
{
	# Get other variables values from form 
	$ItemLocation = $_POST['targetLocation_'.$ItemID];
	$ItemAdminGroup = $_POST['ItemAdminGroup_'.$ItemID];
	$targetWriteOffReason = $_POST['targetWriteOffReason_'.$ItemID];
	$ItemType = $_POST['ItemType_'.$ItemID];
	$no_of_file_upload = $_POST['no_of_file_upload_'.$ItemID];
	$ignored = $_POST['ignoredItem_'.$ItemID];
	$resource_item_status = $_POST['resource_item_status_'.$ItemID];
			
	if(empty($ignored)) # Check if item is ignored
	{		
		# Update DB record 
		$sql = "SELECT ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = $targetWriteOffReason";
		$arr_write_off_reason = $linventory->returnVector($sql);
		$write_off_reason = $arr_write_off_reason[0];
		$write_off_reason = addslashes($write_off_reason);
		
		$sql = "INSERT INTO
						INVENTORY_ITEM_WRITE_OFF_RECORD
						(ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, 
						RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($ItemID, '$curr_date', $ItemLocation, $ItemAdminGroup, 1, '$write_off_reason', '$curr_date', $UserID, 
						0, 0, NOW(), NOW())
				";
	
		$result['NewInvItemSingleStatusLog'] = $linventory->db_db_query($sql);
		$write_off_logID = $linventory->db_insert_id();
					
		# Set attachment		
		$no_of_file = $no_of_file_upload;
		if($no_of_file == "")
		{
			$no_of_file = 1;
		}
				
		for($i=0; $i<$no_of_file; $i++)
		{			
			
			$file = stripslashes($_FILES['item_write_off_attachment_'."$ItemID".'_'."$i".'']['name']);
			$re_file = stripslashes($_FILES['item_write_off_attachment_'."$ItemID".'_'."$i".'']['tmp_name']);
					
			if($re_file == "none" || $re_file == ""){
			}
			else
			{
				$photo_path = $path . "/".$write_off_logID;
				$photo_re_path = $re_path . "/".$write_off_logID;
				
				$lf = new libfilesystem();
				if (!is_dir($photo_path))
				{
					$lf->folder_new($photo_path);
				}
				
				$target = "$photo_path/$file";
				$attachementname = "/$file";
				
				if($lf->lfs_copy($re_file, $target) == 1)
				{
					
					$sql = "INSERT INTO 
									INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
									(ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
							VALUES
									($ItemID, $write_off_logID, '$photo_re_path', '".$linventory->Get_Safe_Sql_Query($file)."', NOW(), NOW())
							";
					$linventory->db_db_query($sql);
				}
			}
		}
				
		# eBooking #
		if($resource_item_status != "")
		{
			$sql = "SELECT ResourceID FROM INTRANET_RESOURCE_INVENTORY_ITEM_LIST WHERE ItemID = $ItemID";
			$arr_resource_item = $linventory->returnVector($sql);
			if(sizeof($arr_resource_item)>0)
			{
				$resource_id = $arr_resource_item[0];
				$sql = "CREATE TABLE IF NOT EXISTS INTRANET_RESOURCE_WRITE_OFF
						 (ItemID int(11) NOT NULL,
						  ResourceID int(11) NOT NULL,
						  PendingMode int(1) NOT NULL,
						  PRIMARY KEY (ItemID),
						  UNIQUE ResourceItemMapID (ItemID, ResourceID)
						 )";
				$linventory->db_db_query($sql);
				
				$sql = "INSERT INTO INTRANET_RESOURCE_WRITE_OFF (ItemID, ResourceID, PendingMode) VALUES ($ItemID, $resource_id, $resource_item_status)";
				$linventory->db_db_query($sql);
			}
		}
		# end of eBooking #
	}
}

if (!in_array(false,(array)$result)) 
{
	$returnMsgKey = 'UpdateSuccess';
}
else 
{
	$returnMsgKey = 'UpdateUnSuccess';
}

header("location: items_full_list.php?returnMsgKey=$returnMsgKey");

intranet_closedb();
?>