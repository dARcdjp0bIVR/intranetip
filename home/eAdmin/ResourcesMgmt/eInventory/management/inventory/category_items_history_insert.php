<?php
/*
 * Log
 * Date: 2019-05-13 [Henry] Security fix: SQL without quote
 * Date: 2018-02-07 [Henry] add access right checking [Case#E135442]
 * Date: 2015-04-16 [Cameron] use double quote for string "$image_path/$LAYOUT_SKIN"
 *
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// $arr_action = array( array(1,"New Purchase"), array(2,"Write Off"), array(3,"Out Of Order"), array(4,"Repair"));
$select_action = getSelectByArray($i_InventorySystem_AdminAction, "name=\"targetAction\" onChange=\"this.form.submit();\" ", $targetAction);

$namefield = $linventory->getInventoryItemNameByLang();
$sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP";
$arr_admin_group = $linventory->returnArray($sql, 2);

// get the item type
$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$arr_item_type = $linventory->returnVector($sql);

if (sizeof($arr_item_type) > 0) {
    $item_type = $arr_item_type[0];
}

$action_selection = "<tr><td class=\"tabletext\" align=\"right\" width=\"45%\">$i_InventorySystem_ReportType</td><td class=\"tabletext\">$select_action</td></tr>";
// $action_selection .= "<tr><td class=\"dotline\" colspan=\"2\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";

// ## 1 - New Purchase ###
if ($targetAction == 1) {
    $table_content .= "<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Price}</td><td class=\"tabletext\"><input type=\"text\" name=\"item_purchase_date\"></td></tr>";
    $table_content .= "<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Qty}</td><td class=\"tabletext\"><input type=\"text\" name=\"item_qty\"></td></tr>";
    $table_content .= "<tr><td class=\"formfieldtitle tabletext\">{$i_InventorySystem_Item_Price}</td><td class=\"tabletext\"><input type=\"text\" name=\"item_purchase_price\"></td></tr>";
    $table_content .= "<tr><td height=\"10px\"></td></tr>";
    
    $table_content .= "<tr><td colspan=\"2\" align=\"left\">";
    $table_content .= "<table border=\"0\" id=\"supplier_list\" cellpadding=\"5\" cellspacing=\"0\" >";
    $table_content .= "<script language=\"javascript\">";
    $table_content .= "for(i=0;i<no_of_supplier;i++)";
    $table_content .= "{";
    $table_content .= "	document.writeln('<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Supplier_Name}</td>');";
    $table_content .= "	document.writeln('<td class=\"tabletext\"><input type=\"text\" name=\"item_supplier_name'+i+'\"></td></tr>');";
    $table_content .= "	document.writeln('<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Supplier_Contact}</td>');";
    $table_content .= "	document.writeln('<td class=\"tabletext\"><input type=\"text\" name=\"item_supplier_contact'+i+'\"></td></tr>');";
    $table_content .= "	document.writeln('<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Supplier_Description}</td>');";
    $table_content .= "	document.writeln('<td class=\"tabletext\"><input type=\"text\" name=\"item_supplier_description'+i+'\"></td></tr>');";
    $table_content .= "	document.writeln('<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Invoice_Num}</td>');";
    $table_content .= "	document.writeln('<td class=\"tabletext\"><input type=\"text\" name=\"item_invoice'+i+'\"></td></tr>');";
    $table_content .= "	document.writeln('<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Quot_Num}</td>');";
    $table_content .= "	document.writeln('<td class=\"tabletext\"><input type=\"text\" name=\"item_quotation'+i+'\"></td></tr>');";
    $table_content .= "	document.writeln('<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Tender_Num}</td>');";
    $table_content .= "	document.writeln('<td class=\"tabletext\"><input type=\"text\" name=\"item_tender'+i+'\"></td></tr>');";
    $table_content .= " document.writeln('<input type=\"hidden\" name=\"hidden_userfile_name'+i+'\"></td></tr>');";
    $table_content .= "}";
    $table_content .= "</script>";
    $table_content .= "</table>";
    $table_content .= "<input type=button value=\" + \" onClick=\"add_field()\">";
    $table_content .= "</td></tr>";
    
    $table_content .= "<tr><td class=\"tabletext\">{$i_InventorySystem_Item_Remark} &nbsp;<span class=\"extraInfo\">[{$i_InventorySystem_Optional}]</span></td><td class=\"tabletext\"><textarea name=\"item_remark\" rows=\"3\" cols=\"40\"></textarea></td></tr>";
    
    $table_content .= "<tr><td class=\"tabletext\">{$i_InventorySystem_Item_AssignTo}</td>";
    if (sizeof($arr_admin_group) > 0) {
        $table_content .= "<td>";
        for ($i = 0; $i < sizeof($arr_admin_group); $i ++) {
            $j = $i + 1;
            
            list ($group_id, $group_name) = $arr_admin_group[$i];
            $table_content .= "<input type=\"checkbox\" name=\"targetGroup[]\" value=$group_id><font class=\"tabletext\">$group_name</font>&nbsp;&nbsp;&nbsp;";
            
            if ($j % 3 == 0)
                $table_content .= "<br>";
        }
        $table_content .= "</td></tr>";
    }
}

// ## 2 - Write Off ###
if ($targetAction == 2) {
    /*
     * $sql = "SELECT
     * b.AdminGroupID,
     * ".$linventory->getInventoryNameByLang("b.").",
     * c.LocationID,
     * ".$linventory->getInventoryNameByLang("c.").",
     * a.Quantity
     * FROM
     * INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
     * INVENTORY_ADMIN_GROUP AS b ON (a.GroupInCharge = b.AdminGroupID) INNER JOIN
     * INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID)
     * WHERE
     * ItemID = $item_id";
     */
    $sql = "SELECT 
					b.AdminGroupID, 
					" . $linventory->getInventoryNameByLang("b.") . ",
					c.LocationID,
					" . $linventory->getInventoryNameByLang("c.") . ",
					a.Quantity,
					IF(d.StockCheckQty IS NULL, ' - ', d.StockCheckQty)
			FROM 
					INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
					INVENTORY_ADMIN_GROUP AS b ON (a.GroupInCharge = b.AdminGroupID) INNER JOIN
					INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOG AS d ON (d.LocationID = a.LocationID AND d.Action = 4)
			WHERE 
					a.ItemID = $item_id";
    // echo $sql;
    $arr_result = $linventory->returnArray($sql, 5);
    
    $table_content .= "<tr class=\"tabletop\"><td class=\"tabletext\" width=\"20%\">" . $i_InventorySystem['Group'] . "</td>";
    $table_content .= "<td class=\"tabletext\" width=\"20%\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td class=\"tabletext\" width=\"10%\">{$i_InventorySystem_Report_InitialQty}</td>";
    $table_content .= "<td class=\"tabletext\" width=\"10%\">{$i_InventorySystem_Item_Stock_Qty}</td>";
    $table_content .= "<td class=\"tabletext\" width=\"10%\">{$i_InventorySystem_Action_WriteOff}</td>";
    $table_content .= "<td class=\"tabletext\" width=\"20%\">{$i_InventorySystem_Item_Remark}</td>";
    $table_content .= "<td width=\"1%\"><input type=\"checkbox\" name=\"enableAll\" onClick=\"(this.checked)?checkEnableAll(1):checkEnableAll(0)\"></td></tr>\n";
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            $j = $i + 1;
            if ($j % 2 == 0)
                $table_row_css = " class=\"tablerow1\" ";
            else
                $table_row_css = " class=\"tablerow2\" ";
            
            list ($group_id, $group_name, $location_id, $location_name, $assigned_qty, $stock_check_qty) = $arr_result[$i];
            $arr_group[] = $group_id;
            $arr_location[] = $location_id;
            
            $table_content .= "<tr $table_row_css><td class=\"tabletext\">$group_name</td>";
            $table_content .= "<td class=\"tabletext\">$location_name</td>";
            $table_content .= "<td class=\"tabletext\">$assigned_qty</td>";
            $table_content .= "<td class=\"tabletext\">$stock_check_qty</td></tr>";
            $table_content .= "<td class=\"tabletext\"><input type=\"text\" name=\"write_off_qty_" . $group_id . "_" . $location_id . "\" size=\"5\" DISABLED style=\"background-color:#CCCCCC\" ></td>";
            $table_content .= "<td class=\"tabletext\"><textarea name=\"remark_" . $group_id . "_" . $location_id . "\" \" DISABLED style=\"background-color:#CCCCCC\" ></textarea></td>";
            $table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"setEnable_" . $group_id . "_" . $location_id . "\" value=$group_id onClick=\"(this.checked)?checkEnable($group_id,$location_id,1):checkEnable($group_id,$location_id,0)\" ></td>";
        }
    }
    
    if (is_array($arr_group) && count($arr_group) > 0) {
        $group_list = implode(",", $arr_group);
    }
    if (is_array($arr_location) && count($arr_location) > 0) {
        $location_list = implode(",", $arr_location);
    }
    $table_content .= "<input type=\"hidden\" name=\"item_id\" value=\"$item_id\">";
    $table_content .= "<input type=\"hidden\" name=\"group_list\" value=\"$group_list\">";
    $table_content .= "<input type=\"hidden\" name=\"location_list\" value=\"$location_list\">";
}

// ## 3 - Out Of Order ###
/*
 * if($targetAction == 3)
 * {
 * $sql = "SELECT
 * b.AdminGroupID,
 * ".$linventory->getInventoryNameByLang("b.").",
 * c.LocationID,
 * ".$linventory->getInventoryNameByLang("c.").",
 * a.Quantity
 * FROM
 * INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
 * INVENTORY_ADMIN_GROUP AS b ON (a.GroupInCharge = b.AdminGroupID) INNER JOIN
 * INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID)
 * WHERE
 * ItemID = $item_id";
 *
 * $arr_result = $linventory->returnArray($sql,5);
 *
 * $table_content .= "<tr class=\"tabletop\"><td class=\"tabletext\" width=\"20%\">Group</td>";
 * $table_content .= "<td class=\"tabletext\" width=\"20%\">Location</td>";
 * $table_content .= "<td class=\"tabletext\" width=\"10%\">Assigned</td>";
 * $table_content .= "<td class=\"tabletext\" width=\"10%\">Out Of Order</td>";
 * $table_content .= "<td class=\"tabletext\" width=\"20%\">Remark</td>";
 * $table_content .= "<td width=\"1%\"><input type=\"checkbox\" name=\"enableAll\" onClick=\"(this.checked)?checkEnableAll(1):checkEnableAll(0)\"></td></tr>\n";
 *
 * if(sizeof($arr_result)>0)
 * {
 * for($i=0; $i<sizeof($arr_result); $i++)
 * {
 * $j=$i+1;
 * if($j%2 == 0)
 * $table_row_css = " class=\"tablerow1\" ";
 * else
 * $table_row_css = " class=\"tablerow2\" ";
 *
 * list($group_id, $group_name, $location_id, $location_name, $assigned_qty) = $arr_result[$i];
 * $arr_group[] = $group_id;
 * $arr_location[] = $location_id;
 *
 * $table_content .= "<tr $table_row_css><td class=\"tabletext\">$group_name</td>";
 * $table_content .= "<td class=\"tabletext\">$location_name</td>";
 * $table_content .= "<td class=\"tabletext\">$assigned_qty</td>";
 * $table_content .= "<td class=\"tabletext\"><input type=\"text\" name=\"write_off_qty_".$group_id."_".$location_id."\" size=\"5\" DISABLED style=\"background-color:#CCCCCC\" ></td>";
 * $table_content .= "<td class=\"tabletext\"><textarea name=\"remark_".$group_id."_".$location_id."\" DISABLED style=\"background-color:#CCCCCC\" ></textarea></td>";
 * $table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"setEnable_".$group_id."_".$location_id."\" value=$group_id onClick=\"(this.checked)?checkEnable($group_id,$location_id,1):checkEnable($group_id,$location_id,0)\"></td>";
 * }
 * }
 * $group_list = implode(",",$arr_group);
 * $location_list = implode(",",$arr_location);
 * $table_content .= "<input type=\"hidden\" name=\"item_id\" value=\"$item_id\">";
 * $table_content .= "<input type=\"hidden\" name=\"group_list\" value=\"$group_list\">";
 * $table_content .= "<input type=\"hidden\" name=\"location_list\" value=\"$location_list\">";
 * }
 */

// ## 3 - Repairing ###
if ($targetAction == 3) {
    $sql = "SELECT 
					b.AdminGroupID, 
					" . $linventory->getInventoryNameByLang("b.") . ",
					c.LocationID,
					" . $linventory->getInventoryNameByLang("c.") . ",
					a.Quantity 
			FROM 
					INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
					INVENTORY_ADMIN_GROUP AS b ON (a.GroupInCharge = b.AdminGroupID) INNER JOIN
					INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID)
			WHERE 
					ItemID = $item_id";
    
    $arr_result = $linventory->returnArray($sql, 5);
    
    $table_content .= "<tr class=\"tabletop\"><td class=\"tabletext\" width=\"20%\">" . $i_InventorySystem['Group'] . "</td>";
    $table_content .= "<td class=\"tabletext\" width=\"20%\">" . $i_InventorySystem['Location'] . "</td>";
    $table_content .= "<td class=\"tabletext\" width=\"10%\">{$i_InventorySystem_Assigned}</td>";
    $table_content .= "<td class=\"tabletext\" width=\"10%\">{$i_InventorySystem_Action_Repair}</td>";
    $table_content .= "<td class=\"tabletext\" width=\"20%\">{$i_InventorySystem_Item_Remark}</td>";
    $table_content .= "<td width=\"1%\"><input type=\"checkbox\" name=\"enableAll\" onClick=\"(this.checked)?checkEnableAll(1):checkEnableAll(0)\"></td></tr>\n";
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            $j = $i + 1;
            if ($j % 2 == 0)
                $table_row_css = " class=\"tablerow1\" ";
            else
                $table_row_css = " class=\"tablerow2\" ";
            
            list ($group_id, $group_name, $location_id, $location_name, $assigned_qty) = $arr_result[$i];
            $arr_group[] = $group_id;
            $arr_location[] = $location_id;
            
            $table_content .= "<tr $table_row_css><td class=\"tabletext\">$group_name</td>";
            $table_content .= "<td class=\"tabletext\">$location_name</td>";
            $table_content .= "<td class=\"tabletext\">$assigned_qty</td>";
            $table_content .= "<td class=\"tabletext\"><input type=\"text\" name=\"write_off_qty_" . $group_id . "_" . $location_id . "\" size=\"5\" DISABLED style=\"background-color:#CCCCCC\" ></td>";
            $table_content .= "<td class=\"tabletext\"><textarea name=\"remark_" . $group_id . "_" . $location_id . "\" DISABLED style=\"background-color:#CCCCCC\" ></textarea></td>";
            $table_content .= "<td class=\"tabletext\"><input type=\"checkbox\" name=\"setEnable_" . $group_id . "_" . $location_id . "\" value=$group_id onClick=\"(this.checked)?checkEnable($group_id,$location_id,1):checkEnable($group_id,$location_id,0)\"></td>";
        }
    }
    if (is_array($arr_group) && count($arr_group) > 0) {
        $group_list = implode(",", $arr_group);
    }
    if (is_array($arr_location) && count($arr_location) > 0) {
        $location_list = implode(",", $arr_location);
    }
    $table_content .= "<input type=\"hidden\" name=\"item_id\" value=\"$item_id\">";
    $table_content .= "<input type=\"hidden\" name=\"group_list\" value=\"$group_list\">";
    $table_content .= "<input type=\"hidden\" name=\"location_list\" value=\"$location_list\">";
}

?>

<script language="javascript">
var no_of_supplier = <? echo $no_supplier==""?1:$no_supplier; ?>;

function checkEnableAll(checked)
{
	<?
if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($group_id, $group_name, $location_id, $location_name, $assigned_qty) = $arr_result[$i];
        ?>
			var gid = <?echo $group_id;?>;
			var lid = <?echo $location_id;?>;
			
			var tmp_field1 = eval("document.form1.write_off_qty_"+gid+"_"+lid);
			var tmp_field2 = eval("document.form1.remark_"+gid+"_"+lid);
			var tmp_checkbox1 = eval("document.form1.setEnable_"+gid+"_"+lid);
	
			if(checked == 1)
			{
				tmp_field1.disabled = false;
				tmp_field2.disabled = false;
				tmp_checkbox1.checked = true;
				tmp_field1.style.backgroundColor = "#FFFFFF";
				tmp_field2.style.backgroundColor = "#FFFFFF";
			}
			if(checked == 0)
			{
				tmp_field1.disabled = true;
				tmp_field2.disabled = true;
				tmp_checkbox1.checked = false;
				tmp_field1.style.backgroundColor = "#CCCCCC";
				tmp_field2.style.backgroundColor = "#CCCCCC";
			}
	<?
    }
}
?>
}

function checkEnable(gid,lid,checked)
{
	var tmp_field1 = eval("document.form1.write_off_qty_"+gid+"_"+lid);
	var tmp_field2 = eval("document.form1.remark_"+gid+"_"+lid);
	
	if(checked == 1)
	{
		tmp_field1.disabled = false;
		tmp_field2.disabled = false;
		tmp_field1.style.backgroundColor = "#FFFFFF";
		tmp_field2.style.backgroundColor = "#FFFFFF";
	}
	if(checked == 0)
	{
		tmp_field1.disabled = true;
		tmp_field2.disabled = true;
		tmp_field1.style.backgroundColor = "#CCCCCC";
		tmp_field2.style.backgroundColor = "#CCCCCC";
	}
}

function checkForm()
{
	var obj = document.form1;
	var final_check = 0;
	<?
if (($targetAction != "")) {
    ?>
		var action = <? echo $targetAction; ?>;
	<?
} else {
    ?>
		var action = 0;
	<?
}
?>
	
	if(action == 1)
	{
		var tmp_check = 0;
		if(check_text(document.form1.item_purchase_date,"Please Input Purchase Date"))
		{
			if(check_text(document.form1.item_qty,"Please Input Purchase Qty"))
			{
				if(check_text(document.form1.item_purchase_price,"Plesae Input Purchase Price"))
				{
					for(i=0; i<no_of_supplier; i++)
					{
						var tmp_field1 = eval("document.form1.item_supplier_name"+i);
						var tmp_field2 = eval("document.form1.item_supplier_contact"+i);
						var tmp_field3 = eval("document.form1.item_supplier_description"+i);
						var tmp_field4 = eval("document.form1.item_invoice"+i);
						var tmp_field5 = eval("document.form1.item_quotation"+i);
						var tmp_field6 = eval("document.form1.item_tender"+i);
						if(check_text(tmp_field1,"Please Input Supplier Name"))
							if(check_text(tmp_field2,"Please Input Supplier Contact"))
								if(check_text(tmp_field3,"Please Input Supplier Description"))
									if(check_text(tmp_field4,"Please Input Invoice No."))
										if(check_text(tmp_field5,"Please Input Quotation No."))
											if(check_text(tmp_field6,"Please Input Tender No."))
												tmp_check = 1;
					}
					if(tmp_check == 1)
					{
						if(check_checkbox(document.form1,"targetGroup[]"))
						{
							final_check = 1;
						}
						else
						{
							final_check = 0;
						}
					}
				}
			}
		}
		if(final_check == 1)
		{
			obj.total_no_supplier.value = no_of_supplier;
			obj.action = "category_items_history_insert2.php";
		}
	}
	if(action == 2)
	{
		var tmp_check = 0;
		var tmp_count = 0;
		<?
if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($group_id, $group_name, $location_id, $location_name, $assigned_qty) = $arr_result[$i];
        ?>
				var gid = <?echo $group_id;?>;
				var lid = <?echo $location_id;?>;
				var qty = <?echo $assigned_qty;?>;
				
				var tmp_field1 = eval("document.form1.write_off_qty_"+gid+"_"+lid);
				var tmp_field2 = eval("document.form1.remark_"+gid+"_"+lid);
				var tmp_checkbox1 = eval("document.form1.setEnable_"+gid+"_"+lid);
				
				if(tmp_field1.disabled == false)
				{
					tmp_count = tmp_count+1;
					if(check_text(tmp_field1,"Plase Input Write Off Amount"))
					{
						if(check_int(tmp_field1,"","Plase Input A Valid Write Off Amount"))
						{
							tmp_check = 1;
						}
					}
				}
		<?
    }
}
?>
		if(tmp_count == 0)
		{
			alert("Please Select At Least One Item");
		}
		else
		{
			if(tmp_check == 1)
			{
				obj.action = "category_items_history_insert_update.php";
			}
		}
	}
	if(action == 3)
	{
		var tmp_check = 0;
		var tmp_count = 0;
		<?
if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($group_id, $group_name, $location_id, $location_name, $assigned_qty) = $arr_result[$i];
        ?>
				var gid = <?echo $group_id;?>;
				var lid = <?echo $location_id;?>;
				var qty = <?echo $assigned_qty;?>;
				
				var tmp_field1 = eval("document.form1.write_off_qty_"+gid+"_"+lid);
				var tmp_field2 = eval("document.form1.remark_"+gid+"_"+lid);
				var tmp_checkbox1 = eval("document.form1.setEnable_"+gid+"_"+lid);
				
				if(tmp_field1.disabled == false)
				{
					tmp_count = tmp_count+1;
					if(check_text(tmp_field1,"Plase Input Out Of Order Amount"))
					{
						if(check_int(tmp_field1,"","Plase Input A Valid Out Of Order Amount"))
						{
							tmp_check = 1;
						}
					}
				}
		<?
    }
}
?>
		if(tmp_count == 0)
		{
			alert("Please Select At Least One Item");
		}
		else
		{
			if(tmp_check == 1)
			{
				obj.action = "category_items_history_insert_update.php";
			}
		}
	}
	if(action == 4)
	{
		var tmp_check = 0;
		var tmp_count = 0;
		<?
if (sizeof($arr_result) > 0) {
    for ($i = 0; $i < sizeof($arr_result); $i ++) {
        list ($group_id, $group_name, $location_id, $location_name, $assigned_qty) = $arr_result[$i];
        ?>
				var gid = <?echo $group_id;?>;
				var lid = <?echo $location_id;?>;
				var qty = <?echo $assigned_qty;?>;
				
				var tmp_field1 = eval("document.form1.write_off_qty_"+gid+"_"+lid);
				var tmp_field2 = eval("document.form1.remark_"+gid+"_"+lid);
				var tmp_checkbox1 = eval("document.form1.setEnable_"+gid+"_"+lid);
				
				if(tmp_field1.disabled == false)
				{
					tmp_count = tmp_count+1;
					if(check_text(tmp_field1,"Plase Input Repair Amount"))
					{
						if(check_int(tmp_field1,"","Plase Input A Valid Repair Amount"))
						{
							tmp_check = 1;
						}
					}
				}
		<?
    }
}
?>
		if(tmp_count == 0)
		{
			alert("Please Select At Least One Item");
		}
		else
		{
			if(tmp_check == 1)
			{
				obj.action = "category_items_history_insert_update.php";
			}
		}
	}

	obj.submit();
}

function add_field()
{
	var table = document.getElementById("supplier_list");
	
	if(no_of_supplier == 1)
	{
		var temp_row = 1;
	}
	else
	{
		var temp_row = no_of_supplier;
	}
	
	var no_of_supplier_info_1 = no_of_supplier * 5 + temp_row;
	var no_of_supplier_info_2 = no_of_supplier_info_1 + 1;
	var no_of_supplier_info_3 = no_of_supplier_info_2 + 1;
	var no_of_supplier_info_4 = no_of_supplier_info_3 + 1;
	var no_of_supplier_info_5 = no_of_supplier_info_4 + 1;
	var no_of_supplier_info_6 = no_of_supplier_info_5 + 1;
	var no_of_supplier_info_7 = no_of_supplier_info_6 + 1;
		
	var row1 = table.insertRow(no_of_supplier_info_1);
	var row2 = table.insertRow(no_of_supplier_info_2);
	var row3 = table.insertRow(no_of_supplier_info_3);
	var row4 = table.insertRow(no_of_supplier_info_4);
	var row5 = table.insertRow(no_of_supplier_info_5);
	var row6 = table.insertRow(no_of_supplier_info_6);
	var row7 = table.insertRow(no_of_supplier_info_7);
	
	if (document.all)
	{
		var cell_1_0 = row1.insertCell(0);
		var cell_1_1 = row1.insertCell(1);
		var cell_2_0 = row2.insertCell(0);
		var cell_2_1 = row2.insertCell(1);
		var cell_3_0 = row3.insertCell(0);
		var cell_3_1 = row3.insertCell(1);
		var cell_4_0 = row4.insertCell(0);
		var cell_4_1 = row4.insertCell(1);
		var cell_5_0 = row5.insertCell(0);
		var cell_5_1 = row5.insertCell(1);
		var cell_6_0 = row6.insertCell(0);
		var cell_6_1 = row6.insertCell(1);
		var cell_7_0 = row7.insertCell(0);
		var cell_7_1 = row7.insertCell(1);
		
		x1='<font class=tabletext>Supplier Name</font>';
		y1='<input type="text" name="item_supplier_name'+no_of_supplier+'">';
		
		x2='<font class=tabletext>Supplier Contact</font>';
		y2='<input type="text" name="item_supplier_contact'+no_of_supplier+'">';
		
		x3='<font class=tabletext>Supplier Description</font>';
		y3='<input type="text" name="item_supplier_description'+no_of_supplier+'">';
		
		x4='<font class=tabletext>Invoice No.</font>';
		y4='<input type="text" name="item_invoice'+no_of_supplier+'">';
		
		x5='<font class=tabletext>Quotation No.</font>';
		y5='<input type="text" name="item_quotation'+no_of_supplier+'">';
		
		x6='<font class=tabletext>Tender No.</font>';
		y6='<input type="text" name="item_tender'+no_of_supplier+'">';
		
		x7='<input type="hidden" name="___total_no_supplier" value="'+no_of_supplier+'"></td></tr>';

		cell_1_0.innerHTML = x1;
		cell_1_1.innerHTML = y1;
		cell_2_0.innerHTML = x2;
		cell_2_1.innerHTML = y2;
		cell_3_0.innerHTML = x3;
		cell_3_1.innerHTML = y3;
		cell_4_0.innerHTML = x4;
		cell_4_1.innerHTML = y4;
		cell_5_0.innerHTML = x5;
		cell_5_1.innerHTML = y5;
		cell_6_0.innerHTML = x6;
		cell_6_1.innerHTML = y6;
		cell_7_0.innerHTML = x7;
		
		no_of_supplier++;
	}
}
</script>

<br>
<form name="form1" action="" method="POST" onSubmit="checkForm();">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
	<?=$infobar?>
</table>
	<table width="100%" border="0" cellpadding="5" cellspacing="0"
		align="center">
	<?=$action_selection?>
</table>
	<br>
	<table width="70%" border="0" cellpadding="5" cellspacing="0"
		align="center">
	<?=$table_content?>
</table>

	<table width="96%" border="0" cellpadding="5" cellspacing="0"
		align="center">
		<tr>
			<td class="dotline"><img
				src="<?="$image_path/$LAYOUT_SKIN" ?>/10x10.gif" width="10"
				height="1" /></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back()","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>

	<input type="hidden" name="flag" value=1> <input type="hidden"
		name="item_id" value=<?=$item_id?>> <input type="hidden"
		name="total_no_supplier" value="">

</form>
</br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb;
?>