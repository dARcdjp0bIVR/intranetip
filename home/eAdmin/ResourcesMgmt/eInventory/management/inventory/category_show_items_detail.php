<?php
// using by: 

/**
 * ***********************************************************************
 * modification log
 * Date: 2018-08-08 (Henry)
 * fixed sql injection
 * 
 * Date: 2018-02-07 [Henry]
 * add access right checking [Case#E135442]
 *
 * Date: 2017-06-23 (Henry)
 * display the name of left staff in item detail [Case#V119089]
 *
 * Date: 2017-01-03 (Henry)
 * Fixed: Fail to download inventory attachment with chinese file name [Case#A72702]
 *
 * Date: 2015-01-21 (Henry)
 * Improved: add remark col at invoice table [Case#U74249]
 *
 * Date: 2014-03-12 (YatWoon)
 * Improved: display empty if purchase / warranty date is 0000-00-00 [Case#C59680]
 *
 * Date: 2013-10-10 (YatWoon)
 * Fixed: failed to display invoice if the PIC is deleted [Case#2013-1008-1552-06066]
 *
 * Date: 2013-08-30 (YatWoon)
 * Improved: add new flag for allow user remove invoice ($special_feature['eInventory']['CanRemoveInvoice']) [Case#2013-0801-1411-50167]
 *
 * Date: 2013-07-05 (YatWoon)
 * Fixed: count the write-off qty with sum() [Case#2013-0702-1120-04071]
 *
 * Date: 2013-06-03 (YatWoon)
 * Improved: no limit for display the image with 100x100px, just set the width with 100px [Case#2013-0531-1649-04073]
 *
 * Date: 2013-03-12 (YatWoon)
 * $sys_custom['CatholicEducation_eInventory']
 * > allow delete invoice record
 * > display warning msg if item qty not match with invoice's total qty
 *
 * Date: 2013-02-25 (Carlos)
 * Display Barcode for bulk item
 *
 * Date: 2012-12-12 (Yuen)
 * Support tags
 *
 * Date: 2012-12-05 (YatWoon)
 * Display "view" button in extra information for member
 *
 * Date: 2012-12-03 (YatWoon)
 * - Improved UI
 * - Remove "Invoice" link
 * - Display "extra information" for bulk item
 *
 * Date: 2012-07-10 (YatWoon)
 * - Enhanced: support 2 funding source for single item
 *
 * Date: 2011-11-02 (YatWoon)
 * -Improved: Add "Edit" button in this page [Case#2011-1031-1627-40073] (admin and group leader only)
 *
 * Date: 2011-06-04 (YatWoon)
 * -Improved: display "date input" and "CreatedBy" data
 *
 * Date: 2011-05-27 YatWoon
 * - Fixed: filename display missing the first character
 *
 * Date: 2011-04-27 YatWoon
 * - add nl2br for textarea data
 * - hidden License, Serial Number fields if the subcategory is not allow input those data
 *
 * Date: 2011-04-26 Yuen
 * customized for Sacred Heart Canossian College
 *
 * ***********************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libuser.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);

$item_id = IntegerSafe($item_id);

// get the name of infobar #
$namefield1 = $linventory->getInventoryItemNameByLang("a.");
$namefield2 = $linventory->getInventoryItemNameByLang("b.");
$namefield3 = $linventory->getInventoryItemNameByLang("c.");
$sql = "SELECT 
				a.ItemCode, 
				$namefield1				
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
		WHERE
				a.ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 2);

$temp[] = array(
    "<a href=\"items_full_list.php\">$i_InventorySystem_BackToFullList</a>"
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

$titlebar .= "<tr><td align=\"left\">";
$titlebar .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_section.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" />";
$titlebar .= "<span class=\"sectiontitle\">" . $result[0][0] . " - " . $result[0][1] . "</span>";
$titlebar .= "</td>";
$titlebar .= "<td align=\"right\">$toolbar";
$titlebar .= "</td></tr>";

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$item_namefield1 = $linventory->getInventoryItemNameByLang("a.");
$item_namefield2 = $linventory->getInventoryItemNameByLang("b.");
$item_namefield3 = $linventory->getInventoryItemNameByLang("c.");
$item_desc_namefield = $linventory->getInventoryDescriptionNameByLang("a.");

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 1);

if (sizeof($result) > 0) {
    list ($item_type) = $result[0];
}

if ($item_type == 1) {
    // CONCAT('<img src=\"$PATH_WRT_ROOT',h.PhotoPath,'/',h.PhotoName,'\" width=\"100px\" height=\"100px\">'),
    $sql = "SELECT
					a.ItemID,
					a.ItemType,
					$item_namefield1,
					$item_namefield2,
					$item_namefield3,
					$item_desc_namefield,
					a.ItemCode,
					IF(a.OwnerShip = " . ITEM_OWN_BY_SCHOOL . ",'$i_InventorySystem_Ownership_School',IF(a.OwnerShip = " . ITEM_OWN_BY_GOVERNMENT . ",'$i_InventorySystem_Ownership_Government', IF(a.OwnerShip = " . ITEM_OWN_BY_DONOR . ",'$i_InventorySystem_Ownership_Donor',' - '))),
					CONCAT('<img src=\"$PATH_WRT_ROOT',h.PhotoPath,'/',h.PhotoName,'\" width=\"100px\">'),
					if(d.PurchaseDate='0000-00-00', '', d.PurchaseDate),
					CONCAT('$ ',ROUND(d.PurchasedPrice,2)),
					CONCAT('$ ',ROUND(d.UnitPrice,2)),
					d.SupplierName,
					d.SupplierContact,
					d.SupplierDescription,
					d.InvoiceNo,
					d.QuotationNo,
					d.TenderNo,
					d.TagCode,
					d.Brand,
					" . $linventory->getInventoryNameByLang("i.") . ",
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocLevel.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
					d.FundingSource,
					if(d.WarrantyExpiryDate='0000-00-00', '', d.WarrantyExpiryDate),
					d.SoftwareLicenseModel,
					d.SerialNumber,
					" . $linventory->getInventoryNameByLang("f.") . ",
					d.MaintainInfo,
					d.ItemRemark,
					c.HasSerialNumber,
					c.HasSoftwareLicenseModel,
					c.HasWarrantyExpiryDate,
					a.DateInput,
					a.CreatedBy,
					d.FundingSource2,
					d.UnitPrice1,
					d.UnitPrice2,
					a.StockTakeOption
			FROM
					INVENTORY_ITEM AS a LEFT OUTER JOIN
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) LEFT OUTER JOIN
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID) LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS d ON (a.ItemID = d.ItemID) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS f ON (d.FundingSource = f.FundingSourceID) LEFT OUTER JOIN
					INVENTORY_LOCATION AS g ON (d.LocationID = g.LocationID) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS LocLevel ON (g.LocationLevelID = LocLevel.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocLevel.BuildingID = LocBuilding.BuildingID) LEFT OUTER JOIN
					INVENTORY_PHOTO_PART AS h ON (a.PhotoLink = h.PartID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS i ON (d.GroupInCharge = i.AdminGroupID) LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_STATUS_LOG AS j ON (a.ItemID = j.ItemID AND j.Action = 1) 
			WHERE
					a.ItemID = '".$item_id."'";
    
    $single_result = $linventory->returnArray($sql);
    for ($i = 0; $i < sizeof($single_result); $i ++) {
        list ($item_id, $item_type, $item_name, $item_cat_name, $item_cat2_name, $item_description, $item_code, $item_ownership, $item_photolink, $item_purchase_date, $item_purchased_price, $item_unit_price, $item_supplier_name, $item_supplier_contact, $item_supplier_description, $item_invoice_no, $item_quotation_no, $item_tender_no, $item_tag_code, $item_brand, $item_group_in_charge, $item_location, $item_funding_source, $item_warranty, $item_license, $item_serial, $item_funding_source_name, $item_maintain_info, $item_remark, $item_HasSerialNumber, $item_HasSoftwareLicenseModel, $item_HasWarrantyExpiryDate, $item_dateinput, $item_createdby, $item_funding_source2, $item_unit_price1, $item_unit_price2, $stocktake_option) = $single_result[$i];
        
        // retrieve funding source (2)
        if ($item_funding_source2) {
            $funding2_sql = "select " . $linventory->getInventoryNameByLang() . " from INVENTORY_FUNDING_SOURCE where FundingSourceID=$item_funding_source2";
            $funding2_result = $linventory->returnVector($funding2_sql);
            $item_funding_source2_name = $funding2_result[0];
        }
        
        // ## Show Single Item Info ###
        $table_content .= "<tr><td><table class=\"form_table_v30\">";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Code}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_code</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Barcode}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_tag_code</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Name}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_name</td></tr>\n";
        if (! $sys_custom['eInventoryCustForSMC'])
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Description}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . nl2br($item_description) . "&nbsp;</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['Category'] . "</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . intranet_htmlspecialchars($item_cat_name) . " > " . intranet_htmlspecialchars($item_cat2_name) . "</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['Location'] . "</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . intranet_htmlspecialchars($item_location) . "</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Group_Name</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . intranet_htmlspecialchars($item_group_in_charge) . "</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Ownership}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_ownership</td></tr>\n";
        
        if (! $sys_custom['eInventoryCustForSMC']) {
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Brand_Name}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_brand &nbsp;</td></tr>\n";
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Supplier_Name}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_supplier_name &nbsp;</td></tr>\n";
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Supplier_Contact}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_supplier_contact &nbsp;</td></tr>\n";
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Supplier_Description}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_supplier_description &nbsp;</td></tr>\n";
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Quot_Num}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_quotation_no &nbsp;</td></tr>\n";
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Tender_Num}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_tender_no &nbsp;</td></tr>\n";
        }
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Invoice_Num}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_invoice_no &nbsp;</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Purchase_Date}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_purchase_date &nbsp;</td></tr>\n";
        
        if (! $sys_custom['eInventoryCustForSMC'])
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Price}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_purchased_price &nbsp;</td></tr>\n";
            
            // Funding
        $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['FundingSource'] . "</td>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">";
        $table_content .= intranet_htmlspecialchars($item_funding_source_name) . " ($" . $item_unit_price1 . ")";
        if ($item_funding_source2) {
            $table_content .= "<br>" . intranet_htmlspecialchars($item_funding_source2_name) . " ($" . $item_unit_price2 . ")";
        }
        $table_content .= "</td></tr>\n";
        
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Unit_Price}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_unit_price &nbsp;</td></tr>\n";
        switch (strtoupper($stocktake_option)) {
            case "FOLLOW_SETTING":
                $stocktake_str = $i_InventorySystem['StockTakeOptionPrice'];
                break;
            case "YES":
                $stocktake_str = $i_InventorySystem['StockTakeOptionCapital'];
                break;
            case "NO":
                $stocktake_str = $i_InventorySystem['StockTakeOptionNo'];
                break;
        }
        $table_content .= "<tr>
							<td class='field_title'>{$i_InventorySystem['StockTake']}</td>
							<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$stocktake_str</td>
							</tr>\n";
        
        if ($item_HasSerialNumber)
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Serial_Num}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_serial &nbsp;</td></tr>\n";
        
        if ($item_HasSoftwareLicenseModel)
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_License_Type}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_license &nbsp;</td></tr>\n";
        if ($item_HasWarrantyExpiryDate && $item_warranty != "0000-00-00")
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Warrany_Expiry}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_warranty &nbsp;</td></tr>\n";
        
        if (! $sys_custom['eInventoryCustForSMC'])
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystemItemMaintainInfo}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . nl2br($item_maintain_info) . "&nbsp;</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Remark}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . nl2br($item_remark) . "&nbsp;</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Attachment}</td>";
        $table_content .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">";
        
        $sql = "SELECT AttachmentPath, Filename FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE ItemID = '".$item_id."'";
        $arr_attachment = $linventory->returnArray($sql, 2);
        
        if (sizeof($arr_attachment) > 0) {
            for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
                // list($attachment_path, $attachment_name) = $arr_attachment[$j];
                // $filename = substr($attachment_name,1,strlen($attachment_name));
                list ($attachment_path, $attachment_name) = $arr_attachment[$j];
                $attachment_link = "<a class=\"tablelink\" href=\"" . $attachment_path . rawurlencode($attachment_name) . "\" target=\"_blank\">$attachment_name</a><br>";
                
                $table_content .= $attachment_link;
            }
        }
        
        $table_content .= "&nbsp;</td></tr>\n";
        
        $tagsDisplay = $linventory->displayTags($item_id, true);
        $table_content .= "<tr><td class='field_title'>" . $Lang['eDiscipline']['Tag'] . "</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$tagsDisplay &nbsp;</td></tr>\n";
        
        // date input
        $table_content .= "<tr><td class='field_title'>{$Lang['General']['RecordDate']}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_dateinput</td></tr>\n";
        // created by
        $lu = new libuser($item_createdby);
        $creator = $lu->UserName();
        $archivedUserName = $lu->getNameWithClassNumber($item_createdby, 1);
        if ($item_createdby) {
            $creator = trim($creator) ? $creator : (trim($archivedUserName) ? '<font color="red">*</font>' . $archivedUserName : $Lang['General']['UserAccountNotExists']);
        } else {
            $creator = $Lang['General']['EmptySymbol'];
        }
        $table_content .= "<tr><td class='field_title'>{$Lang['General']['CreatedBy']}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . $creator . "</td></tr>\n";
        
        $table_content .= "</table></td>";
        // ######################
        
        // ## Show Item's Pic ###
        $table_content .= "<td width=\"200\" align=\"center\" valign=\"top\">";
        $table_content .= "<table align=\"center\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" >";
        $table_content .= "<tr><td class=\"photoborder\">$item_photolink</td></tr>";
        $table_content .= "</table></td></tr>";
        // ######################
    }
}

if ($item_type == 2) {
    // CONCAT('<img src=\"$PATH_WRT_ROOT',e.PhotoPath,'/',e.PhotoName,'\" width=\"100px\" height=\"100px\">'),
    $sql = "SELECT 
					a.ItemID,
					a.ItemType,
					$item_namefield1,
					$item_namefield2,
					$item_namefield3,
					$item_desc_namefield,
					a.ItemCode,
					IF(a.OwnerShip = " . ITEM_OWN_BY_SCHOOL . ",'$i_InventorySystem_Ownership_School',IF(a.OwnerShip = " . ITEM_OWN_BY_GOVERNMENT . ",'$i_InventorySystem_Ownership_Government', IF(a.OwnerShip = " . ITEM_OWN_BY_DONOR . ",'$i_InventorySystem_Ownership_Donor',' - '))),
					CONCAT('<img src=\"$PATH_WRT_ROOT',e.PhotoPath,'/',e.PhotoName,'\" width=\"100px\">'),
					d.Quantity,
					a.DateInput,
					a.CreatedBy,
					a.StockTakeOption,
					d.Barcode 
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID) LEFT OUTER JOIN
					INVENTORY_PHOTO_PART AS e ON (a.PhotoLink = e.PartID)
			WHERE
					a.ItemID = '".$item_id."'";
    
    $bulk_result = $linventory->returnArray($sql);
    for ($i = 0; $i < sizeof($bulk_result); $i ++) {
        list ($item_id, $item_type, $item_name, $item_cat_name, $item_cat2_name, $item_description, $item_code, $item_ownership, $item_photolink, $item_qty, $item_dateinput, $item_createdby, $stocktake_option, $item_barcode) = $bulk_result[$i];
        
        // check the item's qty is equal to invoice's total qty (need count with write-off
        $sql = "select Quantity from INVENTORY_ITEM_BULK_EXT where ItemID='".$item_id."'";
        $result = $linventory->returnVector($sql);
        $this_item_qty = $result[0];
        
        $sql = "select sum(WriteOffQty) from INVENTORY_ITEM_WRITE_OFF_RECORD where ItemID='".$item_id."' and RecordStatus=1";
        $result = $linventory->returnVector($sql);
        $this_item_writeoff_qty = $result[0] + 0;
        $this_item_qty_total = $this_item_qty + $this_item_writeoff_qty;
        
        $sql = "select sum(QtyChange) from INVENTORY_ITEM_BULK_LOG where ItemID='".$item_id."' and (RecordStatus=0 or RecordStatus is NULL) and Action=1";
        $result = $linventory->returnVector($sql);
        $this_invoice_qty = $result[0];
        
        // debug_pr("Qty in system: " . $this_item_qty_total);
        // debug_pr("Qty sum by invoice: " . $this_invoice_qty);
        
        // ## Show Bulk Item Info ###
        $table_content .= "<tr><td><table class=\"form_table_v30\">";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Code}</td><td class=\"tabletext\">$item_code</td></tr>";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Barcode}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_barcode</td></tr>\n";
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Name}</td><td class=\"tabletext\">$item_name</td></tr>";
        $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['Category'] . "</td><td class=\"tabletext\">" . intranet_htmlspecialchars($item_cat_name) . " > " . intranet_htmlspecialchars($item_cat2_name) . "</td></tr>";
        if (! $sys_custom['eInventoryCustForSMC'])
            $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Description}</td><td class=\"tabletext\">" . nl2br($item_description) . "&nbsp;</td></tr>";
        
        $table_content .= "<tr><td class='field_title'>{$Lang['eInventory']['ExistingQty']}</td><td class=\"tabletext\">" . $this_item_qty . "&nbsp;</td></tr>";
        $table_content .= "<tr><td class='field_title'>{$Lang['eInventory']['WriteOffQty']}</td><td class=\"tabletext\">" . $this_item_writeoff_qty . "&nbsp;</td></tr>";
        
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Ownership}</td><td class=\"tabletext\">$item_ownership</td></tr>";
        
        switch (strtoupper($stocktake_option)) {
            case "FOLLOW_SETTING":
                $stocktake_str = $i_InventorySystem['StockTakeOptionPrice'];
                break;
            case "YES":
                $stocktake_str = $i_InventorySystem['StockTakeOptionCapital'];
                break;
            case "NO":
                $stocktake_str = $i_InventorySystem['StockTakeOptionNo'];
                break;
        }
        $table_content .= "<tr>
							<td class='field_title'>{$i_InventorySystem['StockTake']}</td>
							<td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$stocktake_str</td>
							</tr>\n";
        
        $table_content .= "<tr><td class='field_title'>{$i_InventorySystem_Item_Attachment}</td>";
        $table_content .= "<td class=\"tabletext\">";
        
        $sql = "SELECT AttachmentPath, Filename FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE ItemID = '".$item_id."'";
        $arr_attachment = $linventory->returnArray($sql, 2);
        
        if (sizeof($arr_attachment) > 0) {
            for ($j = 0; $j < sizeof($arr_attachment); $j ++) {
                list ($attachment_path, $attachment_name) = $arr_attachment[$j];
                // $filename = substr($attachment_name,1,strlen($attachment_name));
                
                $attachment_link = "<a class=\"tablelink\" href=\"" . $attachment_path . rawurlencode($attachment_name) . "\" target=\"_blank\">$attachment_name</a><br>";
                
                $table_content .= $attachment_link;
            }
        }
        
        $table_content .= "&nbsp;</td></tr>\n";
        
        $tagsDisplay = $linventory->displayTags($item_id, true);
        $table_content .= "<tr><td class='field_title'>" . $Lang['eDiscipline']['Tag'] . "</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$tagsDisplay</td></tr>\n";
        
        // date input
        $table_content .= "<tr><td class='field_title'>{$Lang['General']['RecordDate']}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">$item_dateinput</td></tr>\n";
        // created by
        $lu = new libuser($item_createdby);
        $creator = $lu->UserName();
        $archivedUserName = $lu->getNameWithClassNumber($item_createdby, 1);
        if ($item_createdby) {
            $creator = trim($creator) ? $creator : (trim($archivedUserName) ? '<font color="red">*</font>' . $archivedUserName : $Lang['General']['UserAccountNotExists']);
        } else {
            $creator = $Lang['General']['EmptySymbol'];
        }
        $table_content .= "<tr><td class='field_title'>{$Lang['General']['CreatedBy']}</td><td valign=\"top\" nowrap=\"nowrap\" class=\"tabletext\">" . $creator . "</td></tr>\n";
        
        $table_content .= "</table></td>";
        // #########################
        
        // ## Show Bulk Item Pic ###
        $table_content .= "<td valign=\"top\"><table border=\"0\" width=\"50%\" align=\"center\">";
        $table_content .= "<tr><td align=\"right\">$item_photolink</td></tr>";
        $table_content .= "</table></td></tr>";
        // #########################
    }
}
?>

<br />
<form name="form1" action="category_show_items_edit.php" method="POST">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="0" align="center">
<?=$infobar1?>
</table>
	<br>
	<br>
	<table width="100%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<?=$titlebar?>
</table>

<? if($this_item_qty_total!=$this_invoice_qty) {?> 
<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Warning']?></legend>
		<?=$Lang['eInventory']['QuantityMismatchWithInvoice']?>
</fieldset>
<? } ?>

<table width="100%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<? if($item_type == 1) { ?>
<tr>
			<td>
				<table border=0 cellpadding="5" cellspacing="0" align="center"
					width="100%">
					<tr class="single">
						<td height="20px" valign="top" nowrap="nowrap" class="tabletext">
							<a
							href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_history.php?type=5&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<? } ?>
<? if($item_type == 2) { ?>
<tr>
			<td>
				<table border=0 cellpadding="5" cellspacing="0" align="center"
					width="100%">
					<tr class="bulk">
						<td height="20px" valign="top" nowrap="nowrap" class="tabletext">
							<a
							href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_location.php?type=2&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==2) echo"<B>"; else echo " "; ?><?=$i_InventorySystem['Location']." ".$i_InventorySystem_Search_And2." ".$i_InventorySystem['Caretaker']?><? IF($type==2) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_history.php?type=5&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<? } ?>
</table>

	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="0" align="center">
<?=$table_content?>
</table>
	<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>

<? if($linventory->getAccessLevel($UserID)!=3) {?>
<input type="hidden" name="item_list" value="<?=$item_id?>">
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_edit, "submit")?>
</div>
<? } ?>
</form>

<?

if ($item_type == 2) {
    echo $linterface->GET_NAVIGATION2($Lang['eInventory']['ExtraInformation']);
    $canEdit = ($linventory->getAccessLevel($UserID) != 3) ? 1 : 0;
    
    if (! $sys_custom['eInventoryCustForSMC']) {
        $sql_purchase_price = "if(a.RecordStatus=0 or a.RecordStatus is NULL, concat('$',a.PurchasedPrice), concat('<i>$',a.PurchasedPrice,'</i>')),";
    }
    
    if ($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanRemoveInvoice']) {
        $sql_code = " if(a.RecordStatus=0 or a.RecordStatus is NULL, concat('INV',a.RecordID), concat('<i>INV',a.RecordID,'</i>')), ";
        $sql_remove_select = " concat('<i>'," . getNameFieldByLang2("c.") . ",' (', a.DateModified ,')</i>'), ";
        $sql_from = "left join INTRANET_USER as c on c.UserID=a.DateModifiedBy  ";
    }
    
    $sql = "SELECT
					$sql_code
					if(a.RecordStatus=0 or a.RecordStatus is NULL, a.InvoiceNo, concat('<i>',a.InvoiceNo,'</i>')),
					if(a.RecordStatus=0 or a.RecordStatus is NULL, a.SupplierName, concat('<i>',a.SupplierName,'</i>')),
					if(a.RecordStatus=0 or a.RecordStatus is NULL, if(a.PurchaseDate='0000-00-00', '', a.PurchaseDate), concat('<i>', if(a.PurchaseDate='0000-00-00', '', a.PurchaseDate) ,'</i>')),
					if(a.RecordStatus=0 or a.RecordStatus is NULL, a.QtyChange, concat('<i>',a.QtyChange,'*</i>')),
					$sql_purchase_price
					if(a.RecordStatus=0 or a.RecordStatus is NULL, concat('$',a.UnitPrice), concat('<i>$',a.UnitPrice,'</i>')),
					if(a.RecordStatus=0 or a.RecordStatus is NULL, a.Remark, concat('<i>',a.Remark,'</i>')),
					if(a.RecordStatus=0 or a.RecordStatus is NULL,  
							if(b.UserID is NULL, IF(ab.UserID IS NULL,'" . $Lang['General']['UserAccountNotExists'] . "',CONCAT('<font color=\"red\">*</font>'," . getNameFieldByLang2("ab.") . ")), " . getNameFieldByLang2("b.") . "), 
							if(b.UserID is NULL, IF(ab.UserID IS NULL,'<i>" . $Lang['General']['UserAccountNotExists'] . "</i>',CONCAT('<i><font color=\"red\">*</font>'," . getNameFieldByLang2("ab.") . ",'</i>')), concat('<i>', " . getNameFieldByLang2("b.") . ",'</i>'))
						),
					$sql_remove_select
					CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' value=',a.RecordID,'>')
			FROM
					INVENTORY_ITEM_BULK_LOG AS a 
					left JOIN INTRANET_USER AS b ON (a.PersonInCharge = b.UserID)
					left JOIN INTRANET_ARCHIVE_USER as ab ON (a.PersonInCharge = ab.UserID)
					$sql_from
			WHERE
					ItemID = '".$item_id."' AND
					Action = 1";
    
    // TABLE INFO
    $li = new libdbtable2007(0, 1, $pageNo);
    $li->field_array = array(
        "a.RecordID"
    );
    $li->sql = $sql;
    
    $li->no_col = ($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanRemoveInvoice']) ? 12 : 10;
    $li->title = "";
    $li->IsColOff = "IP25_table";
    $li->page_size = 1000;
    // TABLE COLUMN
    $pos = 1;
    $li->column_list .= "<th class='num_check'>#</td>\n";
    if ($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanRemoveInvoice']) {
        $li->column_list .= "<th>" . $Lang['General']['RefNo'] . "</th>\n";
    }
    $li->column_list .= "<th>" . $i_InventorySystem_Item_Invoice_Num . "</th>\n";
    $li->column_list .= "<th>" . $i_InventorySystem_Item_Supplier_Name . "</th>\n";
    $li->column_list .= "<th>" . $i_InventorySystem_Item_Purchase_Date . "</th>\n";
    $li->column_list .= "<th>" . $i_InventorySystem_Item_Qty . "</th>\n";
    if (! $sys_custom['eInventoryCustForSMC'])
        $li->column_list .= "<th>" . $i_InventorySystem_Item_Price . "</th>\n";
    $li->column_list .= "<th>" . $i_InventorySystem_Unit_Price . "</th>\n";
    $li->column_list .= "<th>" . $i_InventorySystem_Item_Remark . "</th>\n";
    $li->column_list .= "<th>" . $i_InventorySystem_Item_PIC . "</th>\n";
    
    if ($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanRemoveInvoice']) {
        $li->column_list .= "<th>" . $Lang['General']['DeletedBy'] . " (" . $Lang['General']['DeletedDate'] . ")</th>\n";
    }
    
    $li->column_list .= "<th width='1'>&nbsp;</td>\n";
    
    $li->no_navigation_row = 1;
    ?>

<div class="table_board">
	<form name="form2" action="" method="POST">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
					<div class="common_table_tool">
			<? if($canEdit) {?>
			
			<?=$linterface->GET_LNK_EDIT("javascript:checkEdit(document.form2,'RecordID[]','invoice_edit.php')", $button_edit ,"","","",0);?>
			
			<? if($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanRemoveInvoice']) {?>
				<?=$linterface->GET_LNK_REMOVE("javascript:checkRemove(document.form2,'RecordID[]','invoice_remove_update.php')");?>
			<? } ?>
			
			</div>
			<? } else {?>
			<a
					href="javascript:checkEdit(document.form2,'RecordID[]','invoice_edit.php')"
					class="tool_other"><?=$button_view?></a> 
			<? } ?>
		</td>
			</tr>
		</table>
	
	<?=$li->display();?>
	
	<? if($sys_custom['CatholicEducation_eInventory'] || $special_feature['eInventory']['CanRemoveInvoice']) {?>
	<div class="tabletextremark">* <?=$Lang['eInventory']['DeletedInvoice']?></div>
	<? } ?>
	<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>
		<input type="hidden" name="ItemID" value="<?=$item_id;?>">
	</form>
</div>
<? } ?>

 
 
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>