<?php
// modifying : 

/*
 * NOW confirm page split into "import_single_confirm.php" and "import_bulk_confirm.php"
 * so this page is no longer in use.
 */

/********************************** Change Log ********************************************
 * 2019-07-25 [Tommy]:      add new item code format import
 * 2011-04-19 [YatWoon]:	check admin group for same item at same location
 * 2011-04-18 [YatWoon]:	allow bulk item import same item code (with same item name)
 * 2011-01-17 [Carlos]:		Escape ' from imported data
 *******************************************************************************************/
if($format == "")
{
	header("Location: import_item.php");
	exit();
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$limport 		= new libimporttext();
$li 			= new libdb();
$lo 			= new libfilesystem();

$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$BarcodeFormat = $linventory->getBarcodeFormat();

$filepath 		= $itemfile;
$filename 		= $itemfile_name;

if($format == 1)
{
	$temp[] = array($button_import." > ".$i_InventorySystem_ItemType_Single);
	$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>\n"; 
}
if($format == 2)
{
	$temp[] = array($button_import." > ".$i_InventorySystem_ItemType_Bulk);
	$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>\n"; 
}

if($format == 1)	# Single Item
{
	$file_format = array("Item Code","Barcode","Item Chinese Name","Item English Name","Chinese Description","English Description","Category Code","Sub-category Code","Group Code","Building Code","Location Code","Sub-location Code","Funding Source Code","Ownership","Warranty Expiry Date","License","Serial No","Brand","Supplier","Supplier Contact","Supplier Description","Quotation No","Tender No","Invoice No","Purchase Date","Total Purchase Amount","Unit Price","Maintanence Details","Remarks");
	$item_type = 1;
	
	# Create temp single item table
	$sql = "DROP TABLE TEMP_INVENTORY_ITEM";
	$li->db_db_query($sql);
	
	$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_ITEM
			(
			 ItemType int(11),
			 CategoryID int(11),
			 Category2ID int(11),
			 CategoryCode varchar(10),
			 Category2Code varchar(10),
			 NameChi varchar(255),
			 NameEng varchar(255),
			 DescriptionChi text,
			 DescriptionEng text,
			 ItemCode varchar(25),
			 TagCode varchar(100),
			 Ownership int(11),
			 GroupInChargeCode varchar(10),
			 BuildingCode varchar(10),
			 LocationLevelCode varchar(10),
			 LocationCode varchar(10),
			 FundingSourceCode varchar(10),
			 WarrantyExpiryDate date,
			 SoftwareLicenseModel varchar(100),
			 SerialNumber varchar(100),
			 Brand varchar(100),
			 SupplierName varchar(255),
			 SupplierContact text,
			 SupplierDescription text,
			 QuotationNo varchar(255),
			 TenderNo varchar(255),
			 InvoiceNo varchar(255),
			 PurchaseDate date,
			 PurchasedPrice float,
			 UnitPrice float,
			 MaintainInfo text,
			 ItemRemark text,
			 ExistItem int(11)
		    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
	$li->db_db_query($sql);
}
else if($format == 2)	# Bulk Item
{
	$file_format = array ("Item Code","Item Chinese Name","Item English Name","Chinese Description","English Description","Category Code","Sub-category Code","Group Code","Building Code","Location Code","Sub-location Code","Funding Source Code","Ownership","Variance Manager","Quantity","Supplier","Supplier Contact","Supplier Description","Quotation No","Tender No","Invoice No","Purchase Date","Total Purchase Amount","Unit Price","Maintanence Details","Remarks");
	$item_type = 2;
	
	$sql = "DROP TABLE TEMP_INVENTORY_ITEM";
	$li->db_db_query($sql);
	
	$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_ITEM
			(
			 ItemType int(11),
			 CategoryID int(11),
			 Category2ID int(11),
			 CategoryCode varchar(10),
			 Category2Code varchar(10),
			 NameChi varchar(255),
			 NameEng varchar(255),
			 DescriptionChi text,
			 DescriptionEng text,
			 ItemCode varchar(25),
			 Ownership int(11),
			 GroupInChargeCode varchar(10),
			 BuildingCode varchar(10),
			 LocationLevelCode varchar(10),
			 LocationCode varchar(10),
			 FundingSourceCode varchar(10),
			 VarianceManager varchar(10),
			 Quantity int(11),
			 ExistItem int(11),
			 SupplierName varchar(255),
			 SupplierContact text,
			 SupplierDescription text,
			 QuotationNo varchar(255),
			 TenderNo varchar(255),
			 InvoiceNo varchar(255),
			 PurchaseDate date,
			 PurchasedPrice float,
			 UnitPrice float,
			 MaintainInfo text,
			 Remarks text
		    )ENGINE=InnoDB DEFAULT CHARSET=utf8";	    
	$li->db_db_query($sql);
}
$ext = strtoupper($lo->file_ext($filename));

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import_item.php?msg=15");
	exit();
}

if($limport->CHECK_FILE_EXT($filename))
{
	# read file into array
	# return 0 if fail, return csv array if success
	$data = $limport->GET_IMPORT_TXT($filepath);
	$col_name = array_shift($data);		# drop the title bar
	
	# check the csv file's first row is correct or not
	$format_wrong = false;
	for($i=0; $i<sizeof($file_format); $i++)
	{
		if ($col_name[$i]!=$file_format[$i])
		{
			$format_wrong = true;
			break;
		}
	}
	if($format_wrong)
	{
		header("location: import_item.php?msg=15");
		exit();
	}
	
	### Remove Empty Row in CSV File ###
	for($i=0; $i<sizeof($data); $i++)
	{
		if(sizeof($data[$i])!=0)
		{
			$arr_new_data[] = $data[$i];
			$record_row++;
		}
		else
		{
			$empty_row++;
		}
	}
	
	$file_original_row = sizeof($data);
	$file_new_row = sizeof($arr_new_data);
	
	if($format == 1)	# Single Item
	{
		$arr_final_barcode = array();
		
		# Get exist single item details 
		$sql = "SELECT ItemID, ItemCode	FROM INVENTORY_ITEM WHERE ItemType = $item_type";
		
		$arr_exist_item = $linventory->returnArray($sql,2);
				
		if(sizeof($arr_exist_item) >= 0)
		{
			for($i=0; $i<sizeof($arr_exist_item); $i++)
			{
				list($exist_item_id, $exist_item_code) = $arr_exist_item[$i];
				$exist_item[$exist_item_code]['ItemID'] = $exist_item_id;
			}
			
			for($i=0; $i<sizeof($arr_new_data); $i++)
			{
				list($item_code,$item_tag_code,$item_chi_name,$item_eng_name,$item_chi_description,$item_eng_description,$item_categoryCode,$item_category2Code,$item_admin_group_code,$item_buildingCode,$item_locationLevelCode,$item_locationCode,$item_fundingCode,$item_ownership,$item_warranty,$item_license,$item_serial,$item_brand,$item_supplier,$item_supplier_contact,$item_supplier_description,$item_quotation_no,$item_tender_no,$item_invoice_no,$item_purchase_date,$item_purchase_price,$item_unit_price,$item_maintain_info,$item_remarks) = $arr_new_data[$i];

				// convert the date string in to date format of PHP
				if(strpos($item_warranty,"/") > 0)
				{
					$WarrantyArray = Explode("/", $item_warranty);
					$item_warranty = date("Y-m-d", mktime(0,0,0,$WarrantyArray[1],$WarrantyArray[0],$WarrantyArray[2]));
				}
				
				if(strpos($item_purchase_date,"/") > 0)
				{
					$PurchaseDateArray = Explode("/", $item_purchase_date);
					$item_purchase_date = date("Y-m-d", mktime(0,0,0,$PurchaseDateArray [1],$PurchaseDateArray [0],$PurchaseDateArray [2]));
				}
				
				$arr_tmp_data[] = $data[$i];
				
				# Check Item Code Exist, if exist - Update, else New #
				if($exist_item[$item_code]['ItemID'] != "")
				{
					$updateItem[$i] = $data[$i];
				}else{
					$newItem[$i] = $data[$i];
				}
			}
			
			if(sizeof($updateItem)>0)	# update single item
			{
				//for($i=0; $i<sizeof($updateItem); $i++)
				foreach($updateItem as $row=>$key)
				{
					list($item_code,$item_tag_code,$item_chi_name,$item_eng_name,$item_chi_description,$item_eng_description,$item_categoryCode,$item_category2Code,$item_admin_group_code,$item_buildingCode,$item_locationLevelCode,$item_locationCode,$item_fundingCode,$item_ownership,$item_warranty,$item_license,$item_serial,$item_brand,$item_supplier,$item_supplier_contact,$item_supplier_description,$item_quotation_no,$item_tender_no,$item_invoice_no,$item_purchase_date,$item_purchase_price,$item_unit_price,$item_maintain_info,$item_remarks) = $updateItem[$row];
					
					### Check Category Code ###	
					//if(($item_categoryCode != "") && ($item_category2Code != ""))
					//{
						if($item_categoryCode != "")
						{
							if($item_category2Code != "")
							{
								$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_categoryCode)."'";
								$arr_tmp_result = $linventory->returnVector($sql);
								$cat_id = $arr_tmp_result[0];
								
								$sql = "SELECT Count(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $cat_id AND Code = '".$linventory->Get_Safe_Sql_Query($item_category2Code)."'";
								$arr_result = $linventory->returnVector($sql);
			
								$matched_row = $arr_result[0];
								if($matched_row == 0){
									$error[$row]['type'] = 30;	# Invalid Category Code / Sub-category Code
								}
							}
							else
							{
								$error[$row]['type'] = 8;	# Sub-category Code is empty
							}
						}
						else
						{
							$error[$row]['type'] = 27;	# Category Code is empty
						}
					//}
					
					if($item_admin_group_code != "")
					{
						$sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_admin_group_code)."'";
						$arr_result = $linventory->returnArray($sql,1);
						if(sizeof($arr_result)==0){
							$error[$row]['type'] = 9;
						}
						if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-eInventory']) {
							# check whether current user is the leader of group (only leader can insert record)
							$sql = "SELECT * FROM INVENTORY_ADMIN_GROUP_MEMBER m INNER JOIN INVENTORY_ADMIN_GROUP g ON (g.AdminGroupID=m.AdminGroupID AND g.Code='".$linventory->Get_Safe_Sql_Query($item_admin_group_code)."') WHERE m.UserID='$UserID' AND m.RecordType=1";
							$arr_result = $linventory->returnArray($sql);
							
							if(sizeof($arr_result)==0) {
								$error[$row]['type'] = 32;	
							}
						}
					}
					else
					{
						//$error[$row]['type'] = 10;
						
					}
					
					if(($item_buildingCode != "") && ($item_locationLevelCode != "") && ($item_locationCode != ""))
					{
						if($item_buildingCode != "")
						{
							if($item_locationLevelCode != "")
							{
								if($item_locationCode != "")
								{
									//$sql = "SELECT a.Code FROM INVENTORY_LOCATION_LEVEL AS a INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationLevelID = b.LocationLevelID) WHERE b.Code = '$item_locationCode'";
									$sql = "SELECT 
												DISTINCT room.Code
											FROM 
												INVENTORY_LOCATION_BUILDING as building INNER JOIN 
												INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
												INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
											WHERE
												building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1 and 
												building.Code = '".$linventory->Get_Safe_Sql_Query($item_buildingCode)."' and floor.Code = '".$linventory->Get_Safe_Sql_Query($item_locationLevelCode)."'and room.Code = '".$linventory->Get_Safe_Sql_Query($item_locationCode)."'";
									$arr_result = $linventory->returnVector($sql);
									if($arr_result[0] != $item_locationCode){
										$error[$row]['type'] = 11;
									}
								}
								else
								{
									$error[$row]['type'] = 28;
								}
							}
							else
							{
								$error[$row]['type'] = 12;
							}
						}
						else
						{
							## building code missed
							$error[$row]['type'] = 31;
						}
					}
					
					if($item_fundingCode != "")
					{
						$sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_fundingCode)."'";
						
						$arr_result = $linventory->returnArray($sql,1);
						if(sizeof($arr_result)==0){
							$error[$row]['type'] = 13;
						}
					}
					else
					{
						//$error[$row]['type'] = 14;
					}
					
					if($item_ownership == "")
					{
						//$error[$row]['type'] = 15;
					}
					else
					{
						if($item_ownership != 1)
						{
							if($item_ownership != 2)
							{
								if($item_ownership != 3)
								{
									$error[$row]['type'] = 17;
								}
							}
						}
					}
					
					# check purchase price is empty or not #
					if($item_purchase_price == "")
					{
						$item_purchase_price = 0;
					}
					else
					{
						if($item_purchase_price < 0)
						{
							$error[$row]['type'] = 22;
						}
						else
						{
							if(strpos($item_purchase_price,".") > 0)
							{
								$tmp_purchase_price = substr($item_purchase_price,strpos($item_purchase_price,"."));
								if(strlen($tmp_purchase_price) > 4)
								{
									$error[$row]['type'] = 22;
								}
							}
						}
					}	
					
					# check unit price is empty or not #
					if($item_unit_price == "")
					{
						$item_unit_price = 0;
					}
					else
					{
						if($item_unit_price < 0)
						{
							$error[$row]['type'] = 24;
						}
						else
						{
							if(strpos($item_unit_price,".") > 0)
							{
								$tmp_unit_price = substr($item_unit_price,strpos($item_unit_price,"."));
								if(strlen($tmp_unit_price) > 4)
								{
									$error[$row]['type'] = 24;
								}
							}
						}
					}
					
					$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_categoryCode)."'";
					$tmp_category_id = $linventory->returnVector($sql);
					$category_id = $tmp_category_id[0];
											
					$sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND b.CategoryID = $category_id) WHERE a.Code = '".$linventory->Get_Safe_Sql_Query($item_category2Code)."'";
					$tmp_category2_id = $linventory->returnVector($sql);
					$category2_id = $tmp_category2_id[0];
												
					$item_chi_name = addslashes(intranet_htmlspecialchars(convert2unicode($item_chi_name)));
					$item_eng_name = addslashes(intranet_htmlspecialchars(convert2unicode($item_eng_name)));
					$item_chi_description = intranet_htmlspecialchars(addslashes($item_chi_description));
					$item_eng_description = intranet_htmlspecialchars(addslashes($item_eng_description));
					$item_brand = intranet_htmlspecialchars(addslashes($item_brand));
					$item_supplier = intranet_htmlspecialchars(addslashes($item_supplier));
					$item_supplier_contact = intranet_htmlspecialchars(addslashes($item_supplier_contact));
					$item_supplier_description = intranet_htmlspecialchars(addslashes($item_supplier_description));
					$item_maintain_info = intranet_htmlspecialchars(addslashes($item_maintain_info));
					$item_remarks = intranet_htmlspecialchars(addslashes($item_remarks));
					$item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
					$item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
					$item_code = $linventory->Get_Safe_Sql_Query($item_code);
					$item_tag_code = $linventory->Get_Safe_Sql_Query($item_tag_code);
					$item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
					$item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
					$item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
					$item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
					$item_fundingCode = $linventory->Get_Safe_Sql_Query($item_fundingCode);
					$item_warranty = $linventory->Get_Safe_Sql_Query($item_warranty);
					$item_license = $linventory->Get_Safe_Sql_Query($item_license);
					$item_serial = $linventory->Get_Safe_Sql_Query($item_serial);
					$item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
					$item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
					$item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
					
					$values = "(1,$category_id,$category2_id,'$item_categoryCode','$item_category2Code','$item_chi_name','$item_eng_name', '$item_chi_description', '$item_eng_description', '$item_code','$item_tag_code',$item_ownership,'$item_admin_group_code','$item_buildingCode','$item_locationLevelCode','$item_locationCode','$item_fundingCode','$item_warranty','$item_license', '$item_serial', '$item_brand', '$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_quotation_no', '$item_tender_no', '$item_invoice_no', '$item_purchase_date', '$item_purchase_price', '$item_unit_price', '$item_maintain_info', '$item_remarks', '1')";
					$sql = "INSERT INTO TEMP_INVENTORY_ITEM	(ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, TagCode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber, Brand, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, ItemRemark, ExistItem) VALUES $values";
					$linventory->db_db_query($sql);
				}
			}
		}
		//debug_pr($newItem);
		if(sizeof($newItem)>0)		# insert single item
		{
			foreach($newItem as $row=>$key)
			{
				list($item_code,$item_tag_code,$item_chi_name,$item_eng_name,$item_chi_description,$item_eng_description,$item_categoryCode,$item_category2Code,$item_admin_group_code,$item_buildingCode,$item_locationLevelCode,$item_locationCode,$item_fundingCode,$item_ownership,$item_warranty,$item_license,$item_serial,$item_brand,$item_supplier,$item_supplier_contact,$item_supplier_description,$item_quotation_no,$item_tender_no,$item_invoice_no,$item_purchase_date,$item_purchase_price,$item_unit_price,$item_maintain_info,$item_remarks) = $newItem[$row];
				
				# Check Item Code Empty #
				if($item_code == "")
				{
					$item_code = generateItemCode($item_categoryCode,$item_category2Code,$item_purchase_date,$item_admin_group_code,$item_locationCode,$item_fundingCode);
				}
				
				# Check Barcode
				if($item_tag_code != "")
				{
					$item_tag_code = trim($item_tag_code);
					
					if(strlen($item_tag_code)>$BarcodeMaxLength){
						$error[$row]['type'] = 28;
					}else{
						if($BarcodeFormat == 1){
							$patten = "/^[0-9\$\s\/.|-]+$/";
						}else{
							$patten = "/^[0-9A-Z\$\s\/.|-]+$/";
						}
						
						if(preg_match($patten,$item_tag_code) == false){
							$error[$row]['type'] = 28;
						}else{
							$sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '".$linventory->Get_Safe_Sql_Query($item_tag_code)."'";
							$arr_result = $linventory->returnArray($sql,1);
							if(sizeof($arr_result)>0)
							{
								$error[$row]['type'] = 3;
							}
							$arr_tmp_barcode[] = strtoupper($item_tag_code);
						}
					}
				}
				else
				{
					while(sizeof($arr_final_barcode)!=sizeof($newItem))
					{
						$tmp_barcode = $linventory->random_barcode();
						$sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '".$linventory->Get_Safe_Sql_Query($tmp_barcode)."'";
						$arr_tmp_barcode = $linventory->returnVector($sql);
						if((sizeof($arr_tmp_barcode)==0) && (!array_search($tmp_barcode,$arr_final_barcode)))
							array_push($arr_final_barcode,$tmp_barcode);
					}
					$item_tag_code = $arr_final_barcode[$row];
				}
				
				# Check Item Chi Name #
				if($item_chi_name == ""){
					$error[$row]['type'] = 5;
				}
				
				# Check Item Eng Name #
				if($item_eng_name == ""){
					$error[$row]['type'] = 6;
				}
				
				### Check Category Code ###						
				if($item_categoryCode != "")
				{
					if($item_category2Code != "")
					{
						$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_categoryCode)."'";
						$arr_tmp_result = $linventory->returnVector($sql);
						$cat_id = $arr_tmp_result[0];
						
						$sql = "SELECT Count(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $cat_id AND Code = '".$linventory->Get_Safe_Sql_Query($item_category2Code)."'";
						$arr_result = $linventory->returnVector($sql);
	
						$matched_row = $arr_result[0];
						if($matched_row == 0){
							$error[$row]['type'] = 30;
						}
					}
					else
					{
						$error[$row]['type'] = 8;
					}
				}
				else
				{
					$error[$row]['type'] = 27;
				}
				
				if($item_admin_group_code != "")
				{
					$sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_admin_group_code)."'";
					$arr_result = $linventory->returnArray($sql,1);
					
					if(sizeof($arr_result)==0){
						$error[$row]['type'] = 9;
					} else {
						if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-eInventory']) {
							# check whether current user is the leader of group (only leader can insert record)
							$sql = "SELECT * FROM INVENTORY_ADMIN_GROUP_MEMBER m INNER JOIN INVENTORY_ADMIN_GROUP g ON (g.AdminGroupID=m.AdminGroupID AND g.Code='".$linventory->Get_Safe_Sql_Query($item_admin_group_code)."') WHERE m.UserID='$UserID' AND m.RecordType=1";
							$arr_result = $linventory->returnArray($sql);
							
							if(sizeof($arr_result)==0) {
								$error[$row]['type'] = 32;	
							}
						}
					}
				}
				else
				{
					$error[$row]['type'] = 10;
				}
				
				if($item_buildingCode != "")
				{
					if($item_locationLevelCode != "")
					{
						if($item_locationCode != "")
						{
							$sql = "SELECT 
										DISTINCT room.Code
									FROM 
										INVENTORY_LOCATION_BUILDING as building INNER JOIN 
										INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
										INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
									WHERE
										building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1 and 
										building.Code = '".$linventory->Get_Safe_Sql_Query($item_buildingCode)."' and floor.Code = '".$linventory->Get_Safe_Sql_Query($item_locationLevelCode)."'and room.Code = '".$linventory->Get_Safe_Sql_Query($item_locationCode)."'";
							$arr_result = $linventory->returnVector($sql);
							if($arr_result[0] != $item_locationCode){
								$error[$row]['type'] = 11;
							}
						}
						else
						{
							$error[$row]['type'] = 28;
						}
					}
					else
					{
						$error[$row]['type'] = 12;
					}
				}
				else
				{
					## error - Building Code empty
					$error[$row]['type'] = 31;
				}
				
				if($item_fundingCode != "")
				{
					$sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_fundingCode)."'";
					
					$arr_result = $linventory->returnArray($sql,1);
					if(sizeof($arr_result)==0){
						$error[$row]['type'] = 13;
					}
				}
				else
				{
					$error[$row]['type'] = 14;
				}
				
				if($item_ownership == "")
				{
					$error[$row]['type'] = 15;
				}
				else
				{
					if($item_ownership != 1)
					{
						if($item_ownership != 2)
						{
							if($item_ownership != 3)
							{
								$error[$row]['type'] = 17;
							}
						}
					}
				}
				
				# check purchase price is empty or not #
				if($item_purchase_price == "")
				{
					$item_purchase_price = 0;
				}
				else
				{
					if($item_purchase_price < 0)
					{
						$error[$row]['type'] = 22;
					}
					else
					{
						if(strpos($item_purchase_price,".") > 0)
						{
							$tmp_purchase_price = substr($item_purchase_price,strpos($item_purchase_price,"."));
							if(strlen($tmp_purchase_price) > 4)
							{
								$error[$row]['type'] = 22;
							}
						}
					}
				}	
				
				# check unit price is empty or not #
				if($item_unit_price == "")
				{
					$item_unit_price = 0;
				}
				else
				{
					if($item_unit_price < 0)
					{
						$error[$row]['type'] = 24;
					}
					else
					{
						if(strpos($item_unit_price,".") > 0)
						{
							$tmp_unit_price = substr($item_unit_price,strpos($item_unit_price,"."));
							if(strlen($tmp_unit_price) > 4)
							{
								$error[$row]['type'] = 24;
							}
						}
					}
				}

				if($exist_item[$item_code]['ItemID'] == "")
				{
					$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_categoryCode)."'";
					$tmp_category_id = $linventory->returnVector($sql);
					$category_id = $tmp_category_id[0];
											
					$sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND b.CategoryID = $category_id) WHERE a.Code = '".$linventory->Get_Safe_Sql_Query($item_category2Code)."'";
					$tmp_category2_id = $linventory->returnVector($sql);
					$category2_id = $tmp_category2_id[0];
												
					$item_chi_name = intranet_htmlspecialchars(addslashes($item_chi_name));
					$item_eng_name = intranet_htmlspecialchars(addslashes($item_eng_name));
					$item_chi_description = intranet_htmlspecialchars(addslashes($item_chi_description));
					$item_eng_description = intranet_htmlspecialchars(addslashes($item_eng_description));
					$item_brand = intranet_htmlspecialchars(addslashes($item_brand));
					$item_supplier = intranet_htmlspecialchars(addslashes($item_supplier));
					$item_supplier_contact = intranet_htmlspecialchars(addslashes($item_supplier_contact));
					$item_supplier_description = intranet_htmlspecialchars(addslashes($item_supplier_description));
					$item_maintain_info = intranet_htmlspecialchars(addslashes($item_maintain_info));
					$item_remarks = intranet_htmlspecialchars(addslashes($item_remarks));
					$item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
					$item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
					$item_code = $linventory->Get_Safe_Sql_Query($item_code);
					$item_tag_code = $linventory->Get_Safe_Sql_Query($item_tag_code);
					$item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
					$item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
					$item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
					$item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
					$item_fundingCode = $linventory->Get_Safe_Sql_Query($item_fundingCode);
					$item_warranty = $linventory->Get_Safe_Sql_Query($item_warranty);
					$item_license = $linventory->Get_Safe_Sql_Query($item_license);
					$item_serial = $linventory->Get_Safe_Sql_Query($item_serial);
					$item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
					$item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
					$item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
					
					$values = "(1,$category_id,$category2_id,'$item_categoryCode','$item_category2Code','$item_chi_name','$item_eng_name', '$item_chi_description', '$item_eng_description', '$item_code','$item_tag_code',$item_ownership,'$item_admin_group_code','$item_buildingCode','$item_locationLevelCode','$item_locationCode','$item_fundingCode','$item_warranty','$item_license', '$item_serial', '$item_brand', '$item_supplier', '$item_supplier_contact', '$item_supplier_description', '$item_quotation_no', '$item_tender_no', '$item_invoice_no', '$item_purchase_date', '$item_purchase_price', '$item_unit_price', '$item_maintain_info', '$item_remarks', '0')";
					$sql = "INSERT INTO TEMP_INVENTORY_ITEM	(ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, TagCode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode, WarrantyExpiryDate, SoftwareLicenseModel, SerialNumber, Brand, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, ItemRemark, ExistItem) VALUES $values";
				
					$linventory->db_db_query($sql);
				}
				# check any duplicate item code in the csv file
				/*
				$sql = "SELECT ItemCode FROM TEMP_INVENTORY_ITEM";
				$arr_tmp_checkItemCode = $linventory->returnVector($sql);
				
				//debug_pr($arr_tmp_checkItemCode);
				if(sizeof($arr_tmp_checkItemCode) != sizeof(array_unique($arr_tmp_checkItemCode)))
				{
					$error[$row]['type'] = 19;
				}
				*/
				$sql = "SELECT COUNT(ItemCode) FROM TEMP_INVENTORY_ITEM WHERE ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."'";
				$arr_tmp_existing_ItemCode = $linventory->returnVector($sql);
				if($arr_tmp_existing_ItemCode[0]>1)
					$error[$row]['type'] = 19;
				
				# check any duplicate barcode in the csv file	
				/*							
				$sql = "SELECT TagCode FROM TEMP_INVENTORY_ITEM WHERE ExistItem IS NULL";
				$arr_tmp_checkBarcode = $linventory->returnVector($sql);
				if(sizeof($arr_tmp_checkBarcode) != sizeof(array_unique($arr_tmp_checkBarcode)))
				{
					$error[$row]['type'] = 20;
				}
				*/
				$sql = "SELECT COUNT(TagCode) FROM TEMP_INVENTORY_ITEM WHERE TagCode='".$linventory->Get_Safe_Sql_Query($item_tag_code)."'";
				$arr_tmp_existing_Barcode = $linventory->returnVector($sql);
				if($arr_tmp_existing_Barcode[0]>1)
					$error[$row]['type'] = 20;
				
			}			
		}
	}
	
	if($format == 2)	# Bulk Item
	{
		# Get exist item details
		$sql = "SELECT ItemID, ItemCode	FROM INVENTORY_ITEM WHERE ItemType = $item_type";
		$arr_exist_item = $linventory->returnArray($sql,2);
		
		if(sizeof($arr_exist_item)>=0)
		{
			for($i=0; $i<sizeof($arr_exist_item); $i++)
			{
				list($exist_item_id,$exist_item_code) = $arr_exist_item[$i];
				$exist_item[$exist_item_code]['ItemID'] = $exist_item_id;
				
			}
			
			for($i=0; $i<sizeof($arr_new_data); $i++)
			{
				
				list($item_code,$item_chi_name,$item_eng_name,$item_chi_description,$item_eng_description,$item_categoryCode,$item_category2Code,$item_admin_group_code,$item_buildingCode,$item_locationLevelCode,$item_locationCode,$item_fundingCode,$item_ownership,$item_variance_manager,$item_qty,$item_supplier,$item_supplier_contact,$item_supplier_description,$item_quotation_no,$item_tender_no,$item_invoice_no,$item_purchase_date,$item_purchase_price,$item_unit_price,$item_maintain_info,$item_remarks) = $arr_new_data[$i];

				if(strpos($item_purchase_date,"/") > 0)
				{								
					$PurchaseDateArray = Explode("/", $item_purchase_date);
					$item_purchase_date = date("Y-m-d", mktime(0,0,0,$PurchaseDateArray [1],$PurchaseDateArray [0],$PurchaseDateArray [2]));
				}
					
				if($exist_item[$item_code]['ItemID'] == "")
				{
					$item_exist = 0;
					
				}
				if($exist_item[$item_code]['ItemID'] != "")
				{
					$item_exist = 1;
				}
				
				if($item_code == "")
				{
					$item_code = generateItemCode($item_categoryCode,$item_category2Code,$item_purchase_date,$item_admin_group_code,$item_locationCode,$item_fundingCode);
				}
				
				if($item_chi_name == "")
					$error[$i]['type'] = 5;
				
				if($item_eng_name == "")
					$error[$i]['type'] = 6;
				
				### Check Category Code ###		
				if($item_categoryCode != "")
				{
					if($item_category2Code != "")
					{
						$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_categoryCode)."'";
						$arr_tmp_result = $linventory->returnVector($sql);
						$cat_id = $arr_tmp_result[0];
						
						$sql = "SELECT Count(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $cat_id AND Code = '".$linventory->Get_Safe_Sql_Query($item_category2Code)."'";
						$arr_result = $linventory->returnVector($sql);
	
						$matched_row = $arr_result[0];
						if($matched_row == 0){
							$error[$i]['type'] = 30;
						}
					}
					else
					{
						$error[$i]['type'] = 8;
					}
				}
				else
				{
					$error[$i]['type'] = 27;
				}
				
				### Check Category Code and Sub-Category Code is in pair or not ###
				if(($item_categoryCode != "")&&($item_category2Code != ""))
				{
					$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_categoryCode)."'";
					$arr_tmp_result = $linventory->returnVector($sql);
					$cat_id = $arr_tmp_result[0];
					
					$sql = "SELECT Count(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $cat_id AND Code = '".$linventory->Get_Safe_Sql_Query($item_category2Code)."'";
					$arr_result = $linventory->returnVector($sql);

					$matched_row = $arr_result[0];
					if($matched_row == 0){
						$error[$i]['type'] = 7;
					}
				}
				
				if($item_admin_group_code != "")
				{
					$sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_admin_group_code)."'";
					$arr_result = $linventory->returnArray($sql,1);
					if(sizeof($arr_result)==0) {
						$error[$i]['type'] = 9;	# invalid group code
					}
					else {	
						if(!$_SESSION['SSV_USER_ACCESS']['eAdmin-eInventory']) {
							# check whether current user is the leader of group (only leader can insert record)
							$sql = "SELECT * FROM INVENTORY_ADMIN_GROUP_MEMBER m INNER JOIN INVENTORY_ADMIN_GROUP g ON (g.AdminGroupID=m.AdminGroupID AND g.Code='".$linventory->Get_Safe_Sql_Query($item_admin_group_code)."') WHERE m.UserID='$UserID' AND m.RecordType=1";
							
							$arr_result = $linventory->returnArray($sql);
							
							if(sizeof($arr_result)==0) {
								$error[$i]['type'] = 32;	# not group leader
							}
						}
					}
				}
				else
				{
					$error[$i]['type'] = 10;
				}
				
				if($item_buildingCode != "")
				{
					if($item_locationLevelCode != "")
					{
						if($item_locationCode != "")
						{
							$sql = "SELECT 
										DISTINCT room.Code
									FROM 
										INVENTORY_LOCATION_BUILDING as building INNER JOIN 
										INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
										INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
									WHERE
										building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1 AND 
										building.Code = '".$linventory->Get_Safe_Sql_Query($item_buildingCode)."' and floor.Code = '".$linventory->Get_Safe_Sql_Query($item_locationLevelCode)."' and room.Code = '".$linventory->Get_Safe_Sql_Query($item_locationCode)."'";
							$arr_result = $linventory->returnVector($sql);
							if($arr_result[0] != $item_locationCode){
								$error[$i]['type'] = 11;
							}
						}
						else
						{
							$error[$i]['type'] = 28;
						}
					}
					else
					{
						$error[$i]['type'] = 12;
					}
				}
				else
				{
					### error - Building Code error ###
					$error[$i]['type'] = 31;
				}
				
				if($item_fundingCode != "")
				{
					$sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_fundingCode)."'";
					$arr_result = $linventory->returnArray($sql,1);
					if(sizeof($arr_result)==0)
						$error[$i]['type'] = 13;
				}
				else
				{
					$error[$i]['type'] = 14;
				}
				
				if($item_ownership == "")
				{
					$error[$i]['type'] = 15;
				}
				else
				{
					if($item_ownership != 1)
					{
						if($item_ownership != 2)
						{
							if($item_ownership != 3)
							{
								$error[$i]['type'] = 17;
							}
						}
					}
				}
				
				if($item_variance_manager != "")
				{
					$sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_variance_manager)."'";
					$arr_result = $linventory->returnArray($sql,1);
					if(sizeof($arr_result)==0)
						$error[$i]['type'] = 25;
				}
				
				if($item_qty == "")
				{
					$error[$i]['type'] = 16;
				}
				else
				{
					if($item_qty<=0)
					{
						$error[$i]['type'] = 18;
					}
					if(strpos($item_qty,".") != 0)
					{
						$error[$i]['type'] = 18;
					}
				}
				
				# check purchase price is empty or not #
				if($item_purchase_price == "")
				{
					$item_purchase_price = 0;
				}
				else
				{
					if($item_purchase_price < 0)
					{
						$error[$i]['type'] = 22;
					}
					else
					{
						if(strpos($item_purchase_price,".") > 0)
						{
							$tmp_purchase_price = substr($item_purchase_price,strpos($item_purchase_price,"."));
							if(strlen($tmp_purchase_price) > 4)
							{
								$error[$i]['type'] = 22;
							}
						}
					}
				}
				
				# check unit price is empty or not #
				if($item_unit_price == "")
				{
					$item_unit_price = 0;
				}
				else
				{
					if($item_unit_price < 0)
					{
						$error[$i]['type'] = 24;
					}
					else
					{
						if(strpos($item_unit_price,".") > 0)
						{
							$tmp_unit_price = substr($item_unit_price,strpos($item_unit_price,"."));
							if(strlen($tmp_unit_price) > 4)
							{
								$error[$i]['type'] = 24;
							}
						}
					}
				}
				
				
				$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($item_categoryCode)."'";
				$tmp_category_id = $linventory->returnVector($sql);
				$category_id = $tmp_category_id[0];
										
				$sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND b.CategoryID = $category_id) WHERE a.Code = '".$linventory->Get_Safe_Sql_Query($item_category2Code)."'";
				$tmp_category2_id = $linventory->returnVector($sql);
				$category2_id = $tmp_category2_id[0];
				
				$item_chi_name = intranet_htmlspecialchars(addslashes($item_chi_name));
				$item_eng_name = intranet_htmlspecialchars(addslashes($item_eng_name));
				$item_chi_description = intranet_htmlspecialchars(addslashes($item_chi_description));
				$item_eng_description = intranet_htmlspecialchars(addslashes($item_eng_description));
				//$item_brand = intranet_htmlspecialchars(addslashes($item_brand));
				$item_supplier = intranet_htmlspecialchars(addslashes($item_supplier));
				$item_supplier_contact = intranet_htmlspecialchars(addslashes($item_supplier_contact));
				$item_supplier_description = intranet_htmlspecialchars(addslashes($item_supplier_description));
				$item_maintain_info = intranet_htmlspecialchars(addslashes($item_maintain_info));
				$item_remarks = intranet_htmlspecialchars(addslashes($item_remarks));
				$item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
				$item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
				$item_code = $linventory->Get_Safe_Sql_Query($item_code);
				$item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
				$item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
				$item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
				$item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
				$item_fundingCode = $linventory->Get_Safe_Sql_Query($item_fundingCode);
				$item_variance_manager = $linventory->Get_Safe_Sql_Query($item_variance_manager);
				$item_license = $linventory->Get_Safe_Sql_Query($item_license);
				$item_serial = $linventory->Get_Safe_Sql_Query($item_serial);
				$item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
				$item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
				$item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
				
				$sql = "INSERT INTO TEMP_INVENTORY_ITEM
								(ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode, VarianceManager, Quantity, ExistItem, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, Remarks)
						VALUES 
							($item_type,$category_id,$category2_id,'$item_categoryCode','$item_category2Code','$item_chi_name','$item_eng_name','$item_chi_description','$item_eng_description','$item_code',$item_ownership,'$item_admin_group_code','$item_buildingCode','$item_locationLevelCode','$item_locationCode','$item_fundingCode', '$item_variance_manager', $item_qty, $item_exist,'$item_supplier','$item_supplier_contact','$item_supplier_description','$item_quotation_no','$item_tender_no','$item_invoice_no','$item_purchase_date','$item_purchase_price','$item_unit_price','$item_maintain_info','$item_remarks')";
				$linventory->db_db_query($sql);
				
				//$sql = "SELECT COUNT(ItemCode) FROM TEMP_INVENTORY_ITEM WHERE ItemCode='$item_code'";
				$sql = "SELECT COUNT(ItemCode) FROM TEMP_INVENTORY_ITEM 
				WHERE 
				ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."' 
				and not (NameChi='".$linventory->Get_Safe_Sql_Query($item_chi_name) ."' and NameEng='".$linventory->Get_Safe_Sql_Query($item_eng_name)."')
				";
				$arr_tmp_existing_ItemCode = $linventory->returnVector($sql);
				
// 				if($arr_tmp_existing_ItemCode[0]>1) 
				if($arr_tmp_existing_ItemCode[0]>0) 
				{
					$error[$i]['type'] = 19;
				}
				
				# check same item code, should be same variance group
				//item_variance_manager
				$sql = "SELECT distinct(VarianceManager) FROM TEMP_INVENTORY_ITEM 
				WHERE 
				ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."' 
				";
				$arr_tmp_existing_VarianceManager = $linventory->returnVector($sql);
// 				debug_pr($sql);
// 				debug_pr("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~".sizeof($arr_tmp_existing_VarianceManager));
// 				debug_pr($arr_tmp_existing_VarianceManager);
				if(sizeof($arr_tmp_existing_VarianceManager)>1) 
				{
					$error[$i]['type'] = 25;
				}
				
				# check same item code, should be same variance group (in existings records)
				$sql = "
					select 
						c.Code
					from 
						INVENTORY_ITEM as a
						left join INVENTORY_ITEM_BULK_EXT as b on (b.ItemID=a.ItemID)
						left join INVENTORY_ADMIN_GROUP as c on (c.AdminGroupID = b.BulkItemAdmin)
					where 
						a.ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."'
				";
				$arr_tmp_existing_VarianceManager = $linventory->returnVector($sql);
// 				debug_pr($sql);
// 				debug_pr("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~".sizeof($arr_tmp_existing_VarianceManager));
// 				debug_pr($arr_tmp_existing_VarianceManager);
				if(sizeof($arr_tmp_existing_VarianceManager)>0) 
				{
					if($arr_tmp_existing_VarianceManager[0] != $item_variance_manager)
						$error[$i]['type'] = 25;
				}
				
				/*
				# check same item code, same location, but different admin group (in temp db)
				$sql = "SELECT GroupInChargeCode FROM TEMP_INVENTORY_ITEM 
				WHERE 
				ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."' and
				BuildingCode='".$linventory->Get_Safe_Sql_Query($item_buildingCode)."' and
				LocationLevelCode='".$linventory->Get_Safe_Sql_Query($item_locationLevelCode)."' and
				LocationCode='".$linventory->Get_Safe_Sql_Query($item_locationCode)."'
				";
				$arr_tmp_existing_AdminGroup = $linventory->returnVector($sql);
				if(sizeof($arr_tmp_existing_AdminGroup)>1) 
				{
// 					debug_pr($arr_tmp_existing_AdminGroup);
					$error[$i]['type'] = 32;
				}
				
				# check same item code, same location, but different admin group (in existings records)
				$sql = "
					select 
						e.Code
					from 
						INVENTORY_ITEM as a
						left join INVENTORY_ITEM_BULK_LOG as b on (b.ItemID=a.ItemID)
						left join INVENTORY_ADMIN_GROUP as e on (e.AdminGroupID = b.GroupInCharge)
						left join INVENTORY_LOCATION as c on (c.LocationID = b.LocationID)
						left join INVENTORY_LOCATION_LEVEL as d on (d.LocationLevelID=c.LocationLevelID)
						left join INVENTORY_LOCATION_BUILDING as f on (d.BuildingID=d.BuildingID)
					where 
						a.ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."'
						and c.Code='".$linventory->Get_Safe_Sql_Query($item_locationCode)."'
						and d.Code='".$linventory->Get_Safe_Sql_Query($item_locationLevelCode)."'
						and f.Code='".$linventory->Get_Safe_Sql_Query($item_buildingCode)."'
				";
// 				debug_pr($sql);
//						and e.Code='".$linventory->Get_Safe_Sql_Query($item_admin_group_code)."'
				$arr_tmp_existing_AdminGroup = $linventory->returnVector($sql);
				if(sizeof($arr_tmp_existing_AdminGroup)>0) 
				{
					if($arr_tmp_existing_AdminGroup[0] != $item_admin_group_code)
						$error[$i]['type'] = 32;
				}
				*/
			}
		}
			
			
	}
	
	### Show Import Result ###
	if($record_row == "")
		$record_row = 0;
	if($empty_row == "")
		$empty_row = 0;
	$table_content .= "<tr>";
	$table_content .= "<td class=\"tabletext\" colspan=\"29\">
							$i_InventorySystem_ImportItem_TotalRow: $file_original_row<br>
							$i_InventorySystem_ImportItem_RowWithRecord: $record_row<br>
							$i_InventorySystem_ImportItem_EmptyRowRecord: $empty_row
						</td>";
	$table_content .= "</tr>\n";
	### END ###
	
	$table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
	if($format == 1)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Barcode</td>";
	}
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseName</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishName</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseDescription</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishDescription</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_CategoryCode</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Category2Code</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_GroupCode</td>";
	$table_content .= "<td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['BuildingCode']."</td>";
	$table_content .= "<td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['FloorCode']."</td>";
	$table_content .= "<td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['RoomCode']."</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_FundingSourceCode</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Ownership</td>";
	if($format == 1)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Warrany_Expiry</td>";
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_License</td>";
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Serial_Num</td>";
	}
	if($format == 2)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_BulkItemAdmin</td>";
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>";
	}
	if($format == 1){
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Brand_Name</td>";
	}
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Name</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Contact</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Description</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Quot_Num</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Tender_Num</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Invoice_Num</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Purchase_Date</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Price</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Unit_Price</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystemItemMaintainInfo</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
	
	if(sizeof($error)>0)
	{
		$table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
	}
	$table_content .= "</tr>\n";
	
	if($format == 1)
	{
		if(sizeof($error) == 0)
		{
			$sql = "SELECT 
						CategoryID, 
						Category2ID,
						CategoryCode, 
						Category2Code, 
						NameChi, 
						NameEng, 
						DescriptionChi,
						DescriptionEng,
						ItemCode, 
						TagCode,
						Ownership, 
						GroupInChargeCode, 
						BuildingCode,
						LocationLevelCode,
						LocationCode, 
						FundingSourceCode, 
						IF(WarrantyExpiryDate = '', '-', WarrantyExpiryDate), 
						IF(SoftwareLicenseModel = '', '-', SoftwareLicenseModel),
						IF(SerialNumber = '', '-', SerialNumber),
						IF(Brand = '','-',Brand),
						IF(SupplierName = '', '-', SupplierName),
						IF(SupplierContact = '', '-', SupplierContact),
						IF(SupplierDescription = '', '-', SupplierDescription),
						IF(QuotationNo = '','-',QuotationNo),
						IF(TenderNo = '','-',TenderNo),
						IF(InvoiceNo = '','-',InvoiceNo),
						IF(PurchaseDate='','-',PurchaseDate),
						CONCAT('$',PurchasedPrice),
						CONCAT('$',UnitPrice),
						IF(MaintainInfo='','-',MaintainInfo),
						IF(ItemRemark='','-',ItemRemark)
					FROM
						TEMP_INVENTORY_ITEM";
						
			$arr_result = $linventory->returnArray($sql,30);
			if(sizeof($arr_result) > 0)
			{
				
				for ($i=0; $i<sizeof($arr_result); $i++)
				{
					$j=$i+1;
					if($j%2 == 0)
						$table_row_css = " class=\"tablerow1\" ";
					else
						$table_row_css = " class=\"tablerow2\" ";
					
					list($category_id, $category2_id, $item_categoryCode, $item_category2Code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_code, $item_tag, $item_ownership, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode, $item_warranty, $item_license, $item_serial, $item_brand, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_unit_price, $item_maintain_info, $item_remarks) = $arr_result[$i];
													
					$table_content .= "<tr $table_row_css><td class=\"tabletext\">$item_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_tag</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
					$table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_ownership</td>";
					$table_content .= "<td class=\"tabletext\">$item_warranty</td>";
					$table_content .= "<td class=\"tabletext\">$item_license</td>";
					$table_content .= "<td class=\"tabletext\">$item_serial</td>";
					$table_content .= "<td class=\"tabletext\">$item_brand</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
					$table_content .= "<td class=\"tabletext\">$item_remarks</td></tr>\n";
				}
			}
			$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"29\"></td></tr>\n";
			$table_content .= "<tr><td colspan=\"29\" align=\"right\">".
								$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
								$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
							  "</td></tr>\n";
		}

		if(sizeof($error) > 0)
		{
			for($i=0; $i<sizeof($arr_new_data); $i++)
			{
				$j=$i+1;
				if($j%2 == 0)
					$table_row_css = " class=\"tablerow1\" ";
				else
					$table_row_css = " class=\"tablerow2\" ";
					
				list($item_code,$item_tag_code,$item_chi_name,$item_eng_name,$item_chi_description,$item_eng_description,$item_categoryCode,$item_category2Code,$item_admin_group_code, $item_buildingCode, $item_locationLevelCode,$item_locationCode,$item_fundingCode,$item_ownership,$item_warranty,$item_license,$item_serial,$item_brand,$item_supplier,$item_supplier_contact,$item_supplier_description,$item_quotation_no,$item_tender_no,$item_invoice_no,$item_purchase_date,$item_purchase_price,$item_unit_price,$item_maintain_info,$item_remarks) = $arr_new_data[$i];
				
				if($error[$i]["type"] == "")
				{
					$table_content .= "<tr $table_row_css><td class=\"tabletext\">$item_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_tag_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
					$table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_ownership</td>";
					$table_content .= "<td class=\"tabletext\">$item_warranty</td>";
					$table_content .= "<td class=\"tabletext\">$item_license</td>";
					$table_content .= "<td class=\"tabletext\">$item_serial</td>";
					$table_content .= "<td class=\"tabletext\">$item_brand</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
					$table_content .= "<td class=\"tabletext\">$item_remarks</td>";
					$table_content .= "<td class=\"tabletext\"> - </td></tr>\n";
				}
				if($error[$i]["type"] != "")
				{
					$table_content .= "<tr $table_row_css><td class=\"tabletext\">$item_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_tag_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
					$table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_ownership</td>";
					$table_content .= "<td class=\"tabletext\">$item_warranty</td>";
					$table_content .= "<td class=\"tabletext\">$item_license</td>";
					$table_content .= "<td class=\"tabletext\">$item_serial</td>";
					$table_content .= "<td class=\"tabletext\">$item_brand</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
					$table_content .= "<td class=\"tabletext\">$item_remarks</td>";
					$table_content .= "<td class=\"tabletext\"><font color=\"red\">".$i_InventorySystem_ImportError[$error[$i]["type"]]."</font></td></tr>\n";
				}
			}
			$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"30\"></td></tr>\n";
			$table_content .= "<tr><td colspan=\"30\" align=\"right\">".
								$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
							  "</td></tr>\n";
		}
	}
	if($format == 2)
	{
		if(sizeof($error) == 0)
		{
			$sql = "SELECT 
						CategoryID, 
						Category2ID, 
						CategoryCode, 
						Category2Code, 
						NameChi, 
						NameEng, 
						IF(DescriptionChi='','-',DescriptionChi),
						IF(DescriptionEng='','-',DescriptionEng),
						ItemCode, 
						Ownership, 
						GroupInChargeCode, 
						BuildingCode,
						LocationLevelCode,
						LocationCode, 
						FundingSourceCode, 
						if(VarianceManager='','-',VarianceManager),
						Quantity,
						IF(SupplierName='','-',SupplierName),
						IF(SupplierContact='','-',SupplierContact),
						IF(SupplierDescription='','-',SupplierDescription),
						IF(QuotationNo='','-',QuotationNo),
						IF(TenderNo='','-',TenderNo),
						IF(InvoiceNo='','-',InvoiceNo),
						IF(PurchaseDate='','-',PurchaseDate),
						CONCAT('$',PurchasedPrice),
						CONCAT('$',UnitPrice),
						IF(MaintainInfo='','-',MaintainInfo),
						IF(Remarks='','-',Remarks)
					FROM
						TEMP_INVENTORY_ITEM";

			$arr_result = $linventory->returnArray($sql,24);
			if(sizeof($arr_result) > 0)
			{
				for ($i=0; $i<sizeof($arr_result); $i++)
				{
					$j=$i+1;
					if($j%2 == 0)
						$table_row_css = " class=\"tablerow1\" ";
					else
						$table_row_css = " class=\"tablerow2\" ";
					
					list($category_id, $category2_id, $item_categoryCode, $item_category2Code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_code, $item_ownership, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode, $item_variance_manager, $item_qty, $item_supplier,$item_supplier_contact,$item_supplier_description,$item_quotation_no,$item_tender_no,$item_invoice_no,$item_purchase_date,$item_purchase_price,$item_unit_price,$item_maintain_info,$item_remarks) = $arr_result[$i];
					$table_content .= "<tr $table_row_css><td class=\"tabletext\">$item_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
					$table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_ownership</td>";
					$table_content .= "<td class=\"tabletext\">$item_variance_manager</td>";
					$table_content .= "<td class=\"tabletext\">$item_qty</td>";
					//$table_content .= "<td class=\"tabletext\">$item_brand</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
					$table_content .= "<td class=\"tabletext\">$item_remarks</td>";
					$table_content .= "</tr>\n";
				}
			}
			$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"28\"></td></tr>\n";
			$table_content .= "<tr><td colspan=28 align=right>".
								$linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
								$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
							  "</td></tr>\n";
		}

		if(sizeof($error) > 0)
		{

			for($i=0; $i<sizeof($arr_new_data); $i++)
			{
				
				$j=$i+1;
				if($j%2 == 0)
					$table_row_css = " class=\"tablerow1\" ";
				else
					$table_row_css = " class=\"tablerow2\" ";
					
				list($item_code,$item_chi_name,$item_eng_name,$item_chi_description,$item_eng_description,$item_categoryCode,$item_category2Code,$item_admin_group_code,$item_buildingCode,$item_locationLevelCode,$item_locationCode,$item_fundingCode,$item_ownership,$item_variance_manager,$item_qty,$item_supplier,$item_supplier_contact,$item_supplier_description,$item_quotation_no,$item_tender_no,$item_invoice_no,$item_purchase_date,$item_purchase_price,$item_unit_price,$item_maintain_info,$item_remarks) = $arr_new_data[$i];
				if($error[$i]["type"] == "")
				{
					
					$table_content .= "<tr $table_row_css><td class=\"tabletext\">$item_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
					$table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_ownership</td>";
					$table_content .= "<td class=\"tabletext\">$item_variance_manager</td>";
					$table_content .= "<td class=\"tabletext\">$item_qty</td>";
					//$table_content .= "<td class=\"tabletext\">$item_brand</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
					$table_content .= "<td class=\"tabletext\">$item_remarks</td>";
					$table_content .= "<td class=\"tabletext\"> - </td></tr>\n";
				}
				if($error[$i]["type"] != "")
				{
					
					$table_content .= "<tr $table_row_css><td class=\"tabletext\">$item_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
					$table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
					$table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
					$table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
					$table_content .= "<td class=\"tabletext\">$item_ownership</td>";
					$table_content .= "<td class=\"tabletext\">$item_variance_manager</td>";
					$table_content .= "<td class=\"tabletext\">$item_qty</td>";
					//$table_content .= "<td class=\"tabletext\">$item_brand</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
					$table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
					$table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
					$table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
					$table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
					$table_content .= "<td class=\"tabletext\">$item_remarks</td>";
					$table_content .= "<td class=\"tabletext\"><font color=\"red\">".$i_InventorySystem_ImportError[$error[$i]["type"]]."</font></td>\n";
				}
			}
			$table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"29\"></td></tr>\n";
			$table_content .= "<tr><td colspan=29 align=right>".
								$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
							  "</td></tr>\n";
		}
	}
}
$TAGS_OBJ[] = array($button_import, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br>

<form name="form1" action="import_item_update.php" method="post">
<table border="0" width="99%" cellspacing="0" cellpadding="5">
<?=$infobar;?>
</table>
<br>
<table border="0" width="96%" cellspacing="0" cellpadding="5">
<?=$table_content;?>
</table>
<input type="hidden" name="format" id="format" value=<?=$format;?>>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<? 
function generateItemCode($CatCode,$SubCatCode,$PurchaseDate,$GroupCode,$SubLocationCode,$FundingCode)
{
	$linventory	= new libinventory();
	global $sys_custom;
	
	if($sys_custom['eInventory_ItemCodeFormat_New'])
	{
		$PurchaseYear = substr($PurchaseDate,0,4);
		$arr_itemCodeFormat = $linventory->retriveItemCodeFormat();

		if(sizeof($arr_itemCodeFormat)>0){
			for($i=0; $i<sizeof($arr_itemCodeFormat); $i++){
				if($arr_itemCodeFormat[$i] == 1){
					$itemCodeFormat_Year = 1;
				}
				if($arr_itemCodeFormat[$i] == 2){
					$itemCodeFormat_Group = 1;
				}
				if($arr_itemCodeFormat[$i] == 3){
					$itemCodeFormat_Funding = 1;
				}
				if($arr_itemCodeFormat[$i] == 4){
					$itemCodeFormat_Location = 1;
				}
			}
		}
		//debug_pr($itemCodeFormat_Year);
		
		if($itemCodeFormat_Year == 1){
			$itemCode_Prefix .= $PurchaseYear;
		}
		if($itemCodeFormat_Group == 1){
			$itemCode_Prefix .= $GroupCode;
		}
		if($itemCodeFormat_Funding == 1){
			$itemCode_Prefix .= $FundingCode;
		}
		if($itemCodeFormat_Location == 1){
			$itemCode_Prefix .= $SubLocationCode;
		}

	} else if($sys_custom['eInventory_SysPpt']){
	    $PurchaseYear = substr($PurchaseDate,0,4);
	    $arr_itemCodeFormat = $linventory->retriveItemCodeFormat();
	    
	    if(sizeof($arr_itemCodeFormat)>0){
	        for($i=0; $i<sizeof($arr_itemCodeFormat); $i++){
	            if($arr_itemCodeFormat[$i] == 'PurchaseYear'){
	                $itemCodeFormat_Year = 1;
	            }
	            if($arr_itemCodeFormat[$i] == 'CategoryCode'){
	                $itemCodeFormat_Cat = 1;
	            }
	            if($arr_itemCodeFormat[$i] == 'SubCategoryCode'){
	                $itemCodeFormat_Cat2 = 1;
	            }
	            if($arr_itemCodeFormat[$i] == 'ResourceManagementGroupCode'){
	                $itemCodeFormat_Group = 1;
	            }
	            if($arr_itemCodeFormat[$i] == 'FundingSource'){
	                $itemCodeFormat_Funding = 1;
	            }
	            if($arr_itemCodeFormat[$i] == 'FundingSource2'){
	                $itemCodeFormat_Funding2 = 1;
	            }
	            if($arr_itemCodeFormat[$i] == 'LocationCode'){
	                $itemCodeFormat_Location = 1;
	            }
	        }
	    }
	    
	    $ItemCodeSeparator = $linventory->getItemCodeSeparator();
	    $separator = "";
	    if ($ItemCodeSeparator == 0) {
	        $separator = '';
	    } else if ($ItemCodeSeparator == 1) {
	        $separator = '_';
	    } else if ($ItemCodeSeparator == 2) {
	        $separator = '/';
	    } else if ($ItemCodeSeparator == 3) {
	        $separator = '-';
	    }
	    
	    if($itemCodeFormat_Year == 1){
	        $itemCode_Prefix .= $PurchaseYear . $separator;
	    }
	    if($itemCodeFormat_Cat == 1){
	        $itemCode_Prefix .= $CatCode . $separator;
	    }
	    if($itemCodeFormat_Cat2 == 1){
	        $itemCode_Prefix .= $SubCatCode . $separator;
	    }
	    if($itemCodeFormat_Group == 1){
	        $itemCode_Prefix .= $GroupCode . $separator;
	    }
	    if($itemCodeFormat_Funding == 1){
	        $itemCode_Prefix .= $FundingCode . $separator;
	    }
	    if($itemCodeFormat_Funding2 == 1){
	        if($Funding2Code = ""){
	            
	        }else{
	           $itemCode_Prefix .= $Funding2Code . $separator;
	        }

	    }
	    if($itemCodeFormat_Location == 1){
	        $itemCode_Prefix .= $SubLocationCode . $separator;
	    }
	    
	}else{
		$itemCode_Prefix = $CatCode.$SubCatCode;
	}
	$sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '".$linventory->Get_Safe_Sql_Query($CatCode)."'";
	$arrCatID = $linventory->returnVector($sql);
	$CatID = $arrCatID[0];
	
	$sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = '".$CatID."' AND Code = '".$linventory->Get_Safe_Sql_Query($SubCatCode)."'";
	$arrSubCatID = $linventory->returnVector($sql);
	$SubCatID = $arrSubCatID[0];
			
	$sql = "SELECT MAX(ItemCode) FROM INVENTORY_ITEM WHERE CategoryID = '".$CatID."' AND Category2ID = '".$SubCatID."' AND ItemCode LIKE '".$linventory->Get_Safe_Sql_Query($itemCode_Prefix)."%'";
	$arrLastItemCode = $linventory->returnVector($sql);
	
	if(sizeof($arrLastItemCode)>0)
	{
		$LastItemCode = $arrLastItemCode[0];
		$tmp_str_pos = strrpos($LastItemCode,"_");
		$postfix = substr($LastItemCode,$tmp_str_pos+1,strlen($LastItemCode));
		
		$postfix = $postfix+1;
		$length = strlen($postfix);
		
		if($sys_custom['eInventory_ItemCodeFormat_New'])
		{
			$new_item_code = $itemCode_Prefix.$postfix;
		}elseif($sys_custom['eInventory_SysPpt']){
		    $numOfDigit = $linventory->getItemCodeDigitLength();
		    $numOfZero = "";
		    $count = $numOfDigit - strlen($postfix);
		    for ($i = 0; $i < $count; $i ++) {
		        $numOfZero .= (string) 0;
		    }
		    $new_item_code = $itemCode_Prefix.$numOfZero.$postfix;
		    
		}else{
			if($length < DEFAULT_ITEM_CODE_NUM_LENGTH)
			{
				for($j=0; $j<(DEFAULT_ITEM_CODE_NUM_LENGTH - $length); $j++)
				{
					$default_postfix = "0";
					$ItemCodepostfix .= $default_postfix;
				}
				$TmpItemCode = $ItemCodepostfix.$postfix;
				$new_item_code = $itemCode_Prefix."_".$TmpItemCode;
				//array_push($arr_final_item_code,$new_item_code);
			}else{
				$new_item_code = $itemCode_Prefix."_".$postfix;
			}
		}
	}
	return $new_item_code;
}
?>