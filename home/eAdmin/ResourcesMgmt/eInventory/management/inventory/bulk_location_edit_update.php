<?php
//using : yat

#################################################
#
#	Date:	2014-10-09	YatWoon
#			Improved: add flag $special_feature['eInventory']['EditLocationBulkQty'], allow client update bulk location qty if necessary [Case#K67403]
#						if the flag is on, only can edit qty, disallow move the item to other location
#			Deploy: ip.2.5.5.10.1
#
#	Date:	2013-10-11	YatWoon
#			fixed: add bulk_log with location and PIC data that can display the history log for item
#
#	Date:	2013-10-10	YatWoon
#			fixed: duplicate record if same item, location, group and funding
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if($special_feature['eInventory']['EditLocationBulkQty'])
{
	$sql = "update INVENTORY_ITEM_BULK_LOCATION set Quantity=$location_qty where RecordID=$RecordID";
	$result = $linventory->db_db_query($sql);
}
else	# Below is the normal workflow
{
	# retrieve LocationID, Qty
	$sql = "select LocationID, Quantity from INVENTORY_ITEM_BULK_LOCATION where RecordID=$RecordID";
	$result = $linventory->returnArray($sql);
	$thisLocationID = $result[0]['LocationID'];
	$thisQty = $result[0]['Quantity'];
	
	# rebuild data if same itemid + group + location + funding source
	$sql = "select RecordID from INVENTORY_ITEM_BULK_LOCATION where LocationID=$thisLocationID and GroupInCharge=$targetGroup and FundingSourceID=$item_funding and ItemID=$ItemID and RecordID<>$RecordID";
	$result = $linventory->returnVector($sql);
	$existsRecordID = $result[0];
	
	if($existsRecordID)	# sum-up qty
	{
		$sql = "update INVENTORY_ITEM_BULK_LOCATION set Quantity =Quantity+$thisQty where RecordID=$existsRecordID";
		
		# remove the original record
		$sql1 = "delete from INVENTORY_ITEM_BULK_LOCATION where RecordID=$RecordID";
		$result = $linventory->db_db_query($sql1);
	}
	else	# update data
	{
		$sql = "update INVENTORY_ITEM_BULK_LOCATION set GroupInCharge=$targetGroup, FundingSourceID=$item_funding where RecordID=$RecordID";
	}
	$result = $linventory->db_db_query($sql);
	
	$curr_date = date("Y-m-d");
	
	# insert INVENTORY_ITEM_BULK_LOG
	$sql = "insert into INVENTORY_ITEM_BULK_LOG 
			(ItemID, RecordDate, Action, FundingSource,GroupInCharge, LocationID, DateInput, PersonInCharge, DateModifiedBy)
			values
			($ItemID, '$curr_date',". ITEM_ACTION_EDIT_ITEM .", $item_funding, $targetGroup, $thisLocationID, now(), $UserID, $UserID)";
	$result = $linventory->db_db_query($sql);
}
	
	
intranet_closedb();		
header("location: category_show_items_location.php?type=2&item_id=$ItemID&xmsg=UpdateSuccess");

?>