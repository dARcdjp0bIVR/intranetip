<?php
// using by: 

// ##################################
// Date: 2020-03-26 Tommy
// fix: null location can be submitted, add error message
//
// Date : 2019-05-13 (Henry)
// Security fix: SQL without quote
// 
// Date	: 2018-08-08 (Henry)
// Fix: sql injection issue
//
// Date: 2018-02-07 [Henry]
// add access right checking [Case#E135442]
//
// Date: 2017-07-20 Cameron
// fix bug: happen on changing language in edit mode
// implode() [function.implode]: Bad arguments
// Cannot modify header information - headers already sent by
//
// Date: 2017-06-30 Henry
// attachment bug fix [Case#F119590]
//
// Date: 2016-02-01
// PHP 5.4 fix: change split to explode
//
// Date: 2015-05-20 Cameron [Case #2014-0916-1711-52206]
// Prompt confirm message to user before delete attachment
//
// Date: 2015-02-13 Cameron [Case #2015-0206-1522-57207]
// Modify the sorting order of location to be consistent to that of School Settings > Campus
//
// Date: 2014-12-23 Henry [Case#J73005]
// $sys_custom['eInventory']['PurchaseDateRequired'] > Purchase date cannot be empty
//
// Date: 2014-09-05 YatWoon
// Requested by CS and ED [Case#A66966]
// Updated: can input $0 for purchase price
//
// Date: 2014-06-09 YatWoon
// Purchase Price becomes Mandatory field
//
// Date: 2014-04-01 YatWoon
// [Customization for SKH]
// Purchase Date becomes Mandatory field
//
// Date: 2014-03-12 YatWoon
// allow item_purchase_date and item_warranty_expiry_date empty
//
// Date: 2014-01-03 Henry
// date picker for item_purchase_date and item_warranty_expiry_date
//
// Date: 2013-02-25 Carlos
// Display Barcode for bulk item
//
// Date: 2012-12-06 Yuen
// Enhanced: support tags
//
// Date: 2012-12-03 (YatWoon)
// Improved UI
// Improved: change the cancel link to "details page"
//
// Date: 2012-09-07 (YatWoon)
// Fixed: sql error in $targetFundingSource2 is null [Case#2012-0906-1845-01132]
//
// Date: 2012-08-22 (YatWoon)
// Fixed: JS error in reset_innerHtml(), missing to check item type
//
// Date: 2012-07-10 (YatWoon)
// Enhanced: support 2 funding source for single item
//
// Date: 2012-07-05 YatWoon
// Improved: display "in use" and original funding
//
// Date: 2012-07-04 YatWoon
// Improved: display "in use" and original sub-category
//
// Date: 2012-05-15 YatWoon
// Fixed: Display sub-category, missing CategoryID checking
//
// Date: 2012-03-27 YatWoon
// Fixed: cannot edit "Funding source" for bulk item in "item edit" page, moved to "location and resources mgmt group" for edit funding
//
// Date: 2012-03-09 YatWoon
// Fixed: retrive incorrect resource management group (only retrieve own group with leader rights only - if not admin)
//
// Date: 2011-05-27 YatWoon
// Fixed: display attachment filename
//
// Date: 2011-04-27 YatWoon
// according subcategory settings, hidden serial number, license, warranty date
//
// Date: 2011-04-26 Yuen
// customized for Sacred Heart Canossian College

// Date: 2011-03-15 YatWoon
// add checking, group member cannot edit item
//
// ##################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$ItemID = IntegerSafe($ItemID,':');

if ($item_list == "") {
    $item_list = implode(",", (array) $ItemID);
}

if ($item_list == "") {
    header("location: items_full_list.php");
    exit();
}

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();
$llocation_ui = new liblocation_ui();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$quotation_no = cleanHtmlJavascript($quotation_no);
$supplier_desc = cleanHtmlJavascript($supplier_desc);

// Init JS for date picker #
$js_init .= '<script type="text/javascript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.js"></script>';
$js_init .= '<link rel="stylesheet" href="' . $PATH_WRT_ROOT . 'templates/jquery/jquery.datepick.css" type="text/css" />';

$no_file = 1;

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// show current title #
$temp[] = array(
    $button_edit . " " . $i_InventorySystem_ItemDetails,
    ""
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";
// $infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION2($i_InventorySystem_ItemDetails)."</td></tr>";

$item_id_temp = explode(":", $item_list);
$item_list = $item_id_temp[0];

if ($item_list != "") {
    $sql = "SELECT 
					a.ItemID,
					a.ItemType,
					a.CategoryID,
					" . $linventory->getInventoryNameByLang("b.") . ",
					a.Category2ID,
					a.NameChi,
					a.NameEng,
					a.DescriptionChi,
					a.DescriptionEng,
					a.ItemCode,
					a.Ownership,
					a.PhotoLink,
					a.StockTakeOption
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID)
			WHERE
					a.ItemID = '".$item_list."'";
    $arr_result = $linventory->returnArray($sql, 12);
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($item_id, $item_type, $item_cat, $item_cat_name, $item_cat2, $item_chi_name, $item_eng_name, $item_chi_desc, $item_eng_desc, $item_code, $item_ownership, $item_photo_id, $item_StockTakeOption) = $arr_result[$i];
            
            $item_StockTakeOption = strtoupper($item_StockTakeOption);
            
            $sql = "SELECT
					Category2ID,
					" . $linventory->getInventoryNameByLang() . "
			FROM
					INVENTORY_CATEGORY_LEVEL2
			where
					CategoryID = '".$item_cat."'
					and (RecordStatus=1 or Category2ID = '".$item_cat2."')
					";
            $temp = $linventory->returnArray($sql, 2);
            
            if ($targetCat2 == "") {
                // $cat2_selection = getSelectByArray($temp, " name=\"targetCat2\" onChange=\"this.form.submit(); document.form1.flag.value=0;\" ", $item_cat2, 0, 1);
                $cat2_selection = getSelectByArray($temp, " name=\"targetCat2\"", $item_cat2, 0, 1);
            } else {
                // $cat2_selection = getSelectByArray($temp, " name=\"targetCat2\" onChange=\"this.form.submit(); document.form1.flag.value=0;\" ", $targetCat2, 0, 1);
                $cat2_selection = getSelectByArray($temp, " name=\"targetCat2\" ", $targetCat2, 0, 1);
                
                $sql = "SELECT 
								a.CategoryID,
								" . $linventory->getInventoryNameByLang("a.") . "
						FROM
								INVENTORY_CATEGORY AS a INNER JOIN
								INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID)
						WHERE
								b.Category2ID = '".$targetCat2."'
						ORDER BY
								a.CategoryID, b.Category2ID";
                
                $arr_category = $linventory->returnArray($sql, 2);
                
                if (sizeof($arr_category) > 0) {
                    for ($j = 0; $j < sizeof($arr_category); $j ++) {
                        list ($cat_id, $cat_name) = $arr_category[$j];
                        $item_cat = $cat_id;
                        $item_cat_name = $cat_name;
                    }
                }
            }
            
            $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['Category'] . "</td>";
            $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\">$item_cat_name</td></tr>";
            $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['SubCategory'] . " <span class=\"tabletextrequire\">*</span></td>";
            $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\">$cat2_selection</td></tr>";
            
            $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_ChineseName <span class=\"tabletextrequire\">*</span></td>";
            $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\"><input name=\"item_chi_name\" type=\"text\" value=\"" . $item_chi_name . "\" class=\"textboxtext2\"> <span id='div_ChiName_err_msg'></span></td></tr>\n";
            $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_EnglishName <span class=\"tabletextrequire\">*</span></td>";
            $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\"><input name=\"item_eng_name\" type=\"text\" value=\"" . $item_eng_name . "\" class=\"textboxtext2\"> <span id='div_EngName_err_msg'></span></td></tr>\n";
            if (! $sys_custom['eInventoryCustForSMC']) {
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_ChineseDescription</td>";
                $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\"><textarea name=\"item_chi_discription\" rows=\"2\" cols=\"50\" class=\"textboxtext2\">$item_chi_desc</textarea></td></tr>\n";
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_EnglishDescription</td>";
                $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\"><textarea name=\"item_eng_discription\" rows=\"2\" cols=\"50\" class=\"textboxtext2\">$item_eng_desc</textarea></td></tr>\n";
            }
            $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Code</td>";
            $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\">$item_code</td></tr>\n";
            if ($item_type == 1) {
                $sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT WHERE ItemID = '".$item_id."'";
                $arr_item_barcode = $linventory->returnVector($sql);
                $barcode = $arr_item_barcode[0];
                
                $table_content .= "<tr>
										<td class='field_title'>$i_InventorySystem_Item_Barcode</td>
										<td class=\"tabletext\" valign=\"top\" colspan=\"3\">$barcode</td>
									</tr>\n";
            } else 
                if ($item_type == 2) {
                    $sql = "SELECT Barcode FROM INVENTORY_ITEM_BULK_EXT WHERE ItemID = '".$item_id."'";
                    $arr_item_barcode = $linventory->returnVector($sql);
                    $barcode = $arr_item_barcode[0];
                    
                    $table_content .= "<tr>
										<td class='field_title'>$i_InventorySystem_Item_Barcode</td>
										<td class=\"tabletext\" valign=\"top\" colspan=\"3\">$barcode</td>
									</tr>\n";
                }
            
            if (($item_ownership == "") or ($item_ownership == 1)) {
                $CHECKED_1 = "CHECKED";
                $CHECKED_2 = "";
                $CHECKED_3 = "";
            }
            if ($item_ownership == 2) {
                $CHECKED_1 = "";
                $CHECKED_2 = "CHECKED";
                $CHECKED_3 = "";
            }
            if ($item_ownership == 3) {
                $CHECKED_1 = "";
                $CHECKED_2 = "";
                $CHECKED_3 = "CHECKED";
            }
            $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Ownership <span class=\"tabletextrequire\">*</span></td>";
            $table_content .= "<td class=\"tabletext\" valign=\"top\" colspan=\"3\">
									<input name=\"item_ownership\" id=\"ownership1\" type=\"radio\" value=1 $CHECKED_1><label for=\"ownership1\">$i_InventorySystem_Ownership_School</label> 
									<input name=\"item_ownership\" id=\"ownership2\" type=\"radio\" value=2 $CHECKED_2><label for=\"ownership2\">$i_InventorySystem_Ownership_Government</label>
									<input name=\"item_ownership\" id=\"ownership3\" type=\"radio\" value=3 $CHECKED_3><label for=\"ownership3\">$i_InventorySystem_Ownership_Donor</label>
								</td></tr>";
            
            if ($item_type == 1) {
                $sql = "SELECT 
								if(PurchaseDate='0000-00-00', '', PurchaseDate),
								PurchasedPrice,
								UnitPrice,
								ItemRemark,
								SupplierName,
								SupplierContact,
								SupplierDescription,
								MaintainInfo, 
								InvoiceNo,
								QuotationNo,
								TenderNo,
								TagCode,
								Brand,
								GroupInCharge,
								LocationID,
								FundingSource,
								WarrantyExpiryDate,
								SoftwareLicenseModel,
								SerialNumber,
								FundingSource2,
								UnitPrice1,
								UnitPrice2
						FROM
								INVENTORY_ITEM_SINGLE_EXT
						WHERE
								ItemID = '".$item_id."'";
                
                $arr_result = $linventory->returnArray($sql);
                if (sizeof($arr_result) > 0) {
                    list ($purchase_date, $purchase_price, $unit_price, $item_remark, $supplier_name, $supplier_contact, $supplier_desc, $item_maintain_info, $invoice_no, $quotation_no, $tender_no, $barcode, $brand_name, $targetGroup, $targetLocation, $targetFundingSource, $warranty_expiry, $license, $serial_num, $targetFundingSource2, $unit_price1, $unit_price2) = $arr_result[0];
                }
                
                $targetFundingSource2 = $targetFundingSource2 ? $targetFundingSource2 : 0;
                
                // ## Check user has rights edit the item or not
                // ## Role admin, Group leader <== can edit
                // ## Group member <== can't edit
                if (! ($linventory->IS_GROUP_ADMIN($targetGroup) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"])) {
                    $linventory->NO_ACCESS_RIGHT_REDIRECT();
                    exit();
                }
                
                /*
                 * ### Get the group for the inventory ###
                 * $namefield = $linventory->getInventoryItemNameByLang();
                 * $sql = "SELECT AdminGroupID, $namefield FROM INVENTORY_ADMIN_GROUP";
                 * $group_result = $linventory->returnArray($sql,2);
                 * $group_selection = getSelectByArray($group_result,"name=targetGroup", $targetGroup,0,1);
                 */
                
                // get my resource groups #
                if ($linventory->IS_ADMIN_USER($UserID)) {
                    $group_namefield = $linventory->getInventoryNameByLang();
                    $sql = "SELECT AdminGroupID, $group_namefield as GroupName FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
                } else {
                    $group_namefield = $linventory->getInventoryNameByLang();
                    $sql = "
							SELECT 
								a.AdminGroupID, 
								$group_namefield as GroupName
							FROM 
								INVENTORY_ADMIN_GROUP as a 
								inner join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID and b.RecordType=1)
							WHERE 
								b.UserID =  '".$UserID."'
							 ORDER BY 
							 	a.DisplayOrder
							";
                }
                $group_array = $linventory->returnArray($sql, 2);
                $group_selection = getSelectByArray($group_array, "name=targetGroup", $targetGroup, 0, 1);
                
                // ## Get the Funding (1)###
                $namefield = $linventory->getInventoryItemNameByLang();
                $sql = "SELECT FundingSourceID, $namefield FROM INVENTORY_FUNDING_SOURCE ";
                $sql .= "where RecordStatus=1 or FundingSourceID=$targetFundingSource ";
                $sql .= "order by DisplayOrder";
                $funding_result = $linventory->returnArray($sql, 2);
                $funding_selection = getSelectByArray($funding_result, "name=targetFundingSource", $targetFundingSource, 0, 1);
                
                // ## Get the Funding (2)###
                $sql = "SELECT FundingSourceID, $namefield FROM INVENTORY_FUNDING_SOURCE ";
                $sql .= "where RecordStatus=1 or FundingSourceID=$targetFundingSource2 ";
                $sql .= "order by DisplayOrder";
                $funding_result = $linventory->returnArray($sql);
                $funding_selection2 = getSelectByArray($funding_result, "name=targetFundingSource2", $targetFundingSource2);
                
                // ## Get the location ###
                // $location_namefield = $linventory->getInventoryNameByLang();
                // $sql = "SELECT LocationID, CONCAT(".$linventory->getInventoryNameByLang("a.").",' > ',".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("c.").") FROM INVENTORY_LOCATION_BUILDING AS a INNER JOIN INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID) WHERE a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1 ORDER BY a.DisplayOrder, b.DisplayOrder, c.DisplayOrder";
                // $location_result = $linventory->returnArray($sql,2);
                // $location_selection = getSelectByArray($location_result, "name=targetLocation", $targetLocation,0,1);
                
                $location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetLocation, "targetLocation");
                
                $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY_LEVEL2 WHERE Category2ID = '".$targetCategory2."'";
                $arr_category = $linventory->returnArray($sql, 1);
                
                if (sizeof($arr_category) > 0) {
                    list ($category_id) = $arr_category[0];
                    $table_content .= "<input type=\"hidden\" name=\"targetCategory\" value=\"$category_id\">";
                }
                
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_AssignTo <span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\" colspan=\"3\">$group_selection</td></tr>\n";
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Location <span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\" colspan=\"3\">$level_selection$location_selection<span id=\"div_Location_err_msg\"></td></tr>\n";
                
                if (! $sys_custom['eInventoryCustForSMC']) {
                    $table_content .= "<tr>
											<td class='field_title'>$i_InventorySystem_Item_Brand_Name</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_brand\" type=\"text\" class=\"textboxtext2\" value=\"$brand_name\"></td>
											<td class='field_title'>$i_InventorySystem_Item_Supplier_Name</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_supplier\" type=\"text\" class=\"textboxtext2\" value=\"$supplier_name\"></td>
										</tr>\n";
                    
                    $table_content .= "<tr>
											<td class='field_title' width=\"20%\">$i_InventorySystem_Item_Supplier_Contact</td><td class=\"tabletext\" valign=\"top\" width=\"20%\"><input name=\"item_supplier_contact\" type=\"text\" class=\"textboxtext2\" value=\"$supplier_contact\"></td>				
											<td class='field_title' width=\"20%\">$i_InventorySystem_Item_Supplier_Description</td><td class=\"tabletext\" valign=\"top\" width=\"40%\"><input name=\"item_supplier_description\" type=\"text\" class=\"textboxtext2\" value=\"$supplier_desc\"></td>
										</tr>\n";
                    $table_content .= "<tr>
											<td class='field_title'>$i_InventorySystem_Item_Quot_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_quotation\" type=\"text\" class=\"textboxtext2\" value=\"$quotation_no\"></td>
											<td class='field_title'>$i_InventorySystem_Item_Tender_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_tender\" type=\"text\" class=\"textboxtext2\" value=\"$tender_no\"></td>
										</tr>\n";
                }
                // $purchase_date = ($purchase_date == "")? date('Y-m-d') : $purchase_date;
                $purchase_date = $purchase_date == "0000-00-00" ? "" : $purchase_date;
                $table_content .= "<tr>
										<td class='field_title'>$i_InventorySystem_Item_Invoice_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_invoice\" type=\"text\" class=\"textboxtext2\" value=\"$invoice_no\"></td>
										<td class='field_title'>$i_InventorySystem_Item_Purchase_Date " . ($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired'] ? "<span class=\"tabletextrequire\">*</span>" : "") . "</td><td class=\"tabletext\" valign=\"top\">" . $linterface->GET_DATE_PICKER('item_purchase_date', $purchase_date, $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1, $Disable = false, $cssClass = "textboxnum") . " <span id='div_PurchaseDate_err_msg'></span></td>
									</tr>\n";
                
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Funding (1) <span class=\"tabletextrequire\">*</span></td><td class=\"tabletext\" valign=\"top\">$funding_selection <span id='div_Funding_err_msg'></span></td>
										<td class='field_title'>$i_InventorySystem_Unit_Price (1) </td><td class=\"tabletext\" valign=\"top\"><input name=\"item_unit_price1\" type=\"text\" class=\"textboxtext2\" value=\"$unit_price1\" onBlur=\"update_unit_price();\"> <span id=\"div_UnitPrice_err_msg1\"></span></td></tr>\n";
                
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Funding (2) </td><td class=\"tabletext\" valign=\"top\">$funding_selection2</td>
										<td class='field_title'>$i_InventorySystem_Unit_Price (2) </td><td class=\"tabletext\" valign=\"top\"><input name=\"item_unit_price2\" type=\"text\" class=\"textboxtext2\" value=\"$unit_price2\" onBlur=\"update_unit_price();\"> <span id=\"div_UnitPrice_err_msg2\"></span></td></tr>\n";
                
                if (! $sys_custom['eInventoryCustForSMC']) {
                    $table_content .= "<tr>
											<td class='field_title'>$i_InventorySystem_Item_Price </td><td class=\"tabletext\" valign=\"top\"><input name=\"item_purchase_price\" type=\"text\" class=\"textboxtext2\" value=\"$purchase_price\">  <span id=\"div_PurchasePrice_err_msg\"></span></td>
											<td class='field_title'>$i_InventorySystem_Unit_Price</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_unit_price\" type=\"text\" class=\"textboxtext2\" value=\"$unit_price\" readonly></td>
										</tr>\n";
                } else {
                    
                    $table_content .= "<tr>
										<td class='field_title'>$i_InventorySystem_Unit_Price</td><td class=\"tabletext\" valign=\"top\" colspan=\"3\"><input name=\"item_unit_price\" type=\"text\" class=\"textboxtext2\" value=\"$unit_price\"></td>
									</tr>\n";
                }
                
                if ($item_StockTakeOption == "YES") {
                    $StockTakeOptionCapitalChecked = "checked='checked'";
                } elseif ($item_StockTakeOption == "NO") {
                    $StockTakeOptionNoChecked = "checked='checked'";
                } else {
                    $StockTakeOptionPriceChecked = "checked='checked'";
                }
                $StockTakeOptions = "<input type='radio' id='StockTakeOptionPrice' name='StockTakeOption' value='FOLLOW_SETTING' {$StockTakeOptionPriceChecked} /> <label for='StockTakeOptionPrice'>" . $i_InventorySystem['StockTakeOptionPrice'] . "</label>";
                $StockTakeOptions .= "<br /><input type='radio' id='StockTakeOptionCapital' name='StockTakeOption' value='YES' {$StockTakeOptionCapitalChecked} /> <label for='StockTakeOptionCapital'>" . $i_InventorySystem['StockTakeOptionCapital'] . "</label>";
                $StockTakeOptions .= "<br /><input type='radio' id='StockTakeOptionNo' name='StockTakeOption' value='No'  {$StockTakeOptionNoChecked} /> <label for='StockTakeOptionNo'>" . $i_InventorySystem['StockTakeOptionNo'] . "</label>";
                
                $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['StockTake'] . "</td><td class=\"tabletext\" valign=\"top\" colspan=\"3\">$StockTakeOptions</td></tr>\n";
                
                if (! $sys_custom['eInventoryCustForSMC']) {
                    $table_content .= "<tr><td class='field_title'>$i_InventorySystemItemMaintainInfo</td><td class=\"tabletext\" valign=\"top\" colspan=\"3\"><textarea name=\"item_maintain_info\" class=\"textboxtext2\">$item_maintain_info</textarea></td></tr>\n";
                }
                
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Remark</td><td class=\"tabletext\" valign=\"top\" colspan=\"3\"><textarea name=\"item_remark\" class=\"textboxtext2\">$item_remark</textarea></td></tr>\n";
                
                if ($targetCat2 != "") {
                    $sql = "SELECT 
									HasSerialNumber,
									HasSoftwareLicenseModel, 
									HasWarrantyExpiryDate 
							FROM 
									INVENTORY_CATEGORY_LEVEL2 	
							WHERE 
									Category2ID = $targetCat2";
                } else {
                    $sql = "SELECT 
									HasSerialNumber,
									HasSoftwareLicenseModel, 
									HasWarrantyExpiryDate 
							FROM 
									INVENTORY_CATEGORY_LEVEL2 	
							WHERE 
									Category2ID = '".$item_cat2."'";
                }
                $result = $linventory->returnArray($sql, 2);
                if ($result[0][HasSerialNumber] == 1) {
                    $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Serial_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_serial\" type=\"text\" class=\"textboxnum\" value=\"$serial_num\"></td></tr>\n";
                }
                // else
                // {
                // $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Serial_Num</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_serial\" type=\"text\" class=\"textboxnumt\" style=\"background-color:#CCCCCC;\" DISABLED></td></tr>\n";
                // }
                if ($result[0][HasSoftwareLicenseModel] == 1) {
                    $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_License</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_license\" type=\"text\" class=\"textboxnum\" value=\"$license\"></td></tr>\n";
                }
                // else
                // {
                // $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_License</td><td class=\"tabletext\" valign=\"top\"><input name=\"item_license\" type=\"text\" class=\"textboxnumt\" style=\"background-color:#CCCCCC;\" DISABLED></td></tr>\n";
                // }
                if ($result[0][HasWarrantyExpiryDate] == 1) {
                    /*
                     * if($warranty_expiry == "")
                     * $warranty_expiry = date('Y-m-d');
                     * if ($warranty_expiry=="0000-00-00")
                     * $warranty_expiry = "";
                     * $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_WarrantyExpiryDate</td><td class=\"tabletext\" valign=\"top\">".$linterface->GET_DATE_PICKER('item_warranty_expiry_date', $warranty_expiry)."</td></tr>";
                     */
                    $warranty_expiry = $warranty_expiry == "0000-00-00" ? "" : $warranty_expiry;
                    $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_WarrantyExpiryDate</td><td class=\"tabletext\" valign=\"top\">" . $linterface->GET_DATE_PICKER('item_warranty_expiry_date', $DefaultValue = $warranty_expiry, $OtherMember = "", $DateFormat = "yy-mm-dd", $MaskFunction = "", $ExtWarningLayerID = "", $ExtWarningLayerContainer = "", $OnDatePickSelectedFunction = "", $ID = "", $SkipIncludeJS = 0, $CanEmptyField = 1, $Disable = false, $cssClass = "textboxnum") . "</td></tr>";
                }
                // else
                // {
                // $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_WarrantyExpiryDate</td><td class=\"tabletext\" valign=\"top\"><input type=\"text\" name=\"item_warranty_expiry_date\" id=\"item_warranty_expiry_date\" class=\"textboxnum\" maxlength=\"10\" value=\"\" style=\"background-color:#CCCCCC;\" DISABLED></td></tr>";
                // }
                
                // # remove current item photo - under development
                if ($item_photo_id != "") {
                    $delete_photo = "<input type='checkbox' name='delete_photo' value=1> $i_InventorySystem_DeleteItemPhoto <br>";
                }
                
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Photo</td>
										<td class=\"tabletext\" valign=\"top\">$delete_photo<input type=\"file\" name=\"item_photo\" class=\"file\" $disable><input type=\"hidden\" name=\"hidden_item_photo\"><br/><span class=\"tabletextremark\">$i_InventorySystem_PhotoGuide</span></td></tr>\n";
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Attachment</td><td class=\"tabletext\" valign=\"top\">";
                
                $sql = "SELECT AttachmentID, AttachmentPath, Filename FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE ItemID = '".$item_id."'";
                $arr_tmp_exist_attachment = $linventory->returnArray($sql, 2);
                if (sizeof($arr_tmp_exist_attachment) > 0) {
                    $table_content .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
                    for ($i = 0; $i < sizeof($arr_tmp_exist_attachment); $i ++) {
                        list ($file_id, $file_path, $file_name) = $arr_tmp_exist_attachment[$i];
                        // $filename = substr($file_name,1,strlen($file_name));
                        // $attachment = $file_path.$file_name;
                        $table_content .= "<tr><td class=\"tabletext\">$file_name</td><td class=\"tabletext\"><a class=\"tablelink\" href=\"javascript:remove_attachment('$item_id','$file_id');\">$button_remove</a></td></tr>";
                    }
                    $table_content .= "</table>";
                }
                
                $table_content .= "
									<table border=\"0\" id=\"upload_file_list\" cellpadding=\"0\" cellspacing=\"0\" class=\"inside_form_table\">
						
									<script language=\"javascript\">
									for(i=0;i<no_of_upload_file;i++)
									{
										document.writeln('<tr><td><input class=\"file\" type=\"file\" name=\"item_attachment_'+i+'\" size=\"40\">');
									    document.writeln('<input type=\"hidden\" name=\"hidden_item_attachment_'+i+'\"></td></tr>');
									}
									</script>
									</table>
									<input type=button value=\" + \" onClick=\"add_field()\">
								";
                /*
                 * for($i=0;$i<$no_file;$i++)
                 * {
                 * $table_content .= "<input class=\"file\" type=\"file\" name=\"item_attachment_$i\" /><br />";
                 * $table_content .= "<input type=\"hidden\" name=\"hidden_item_attachment_$i\" />";
                 * }
                 */
                $table_content .= "</td></tr>\n";
            }
            
            if ($item_type == 2) {
                $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP";
                $arr_admin_group = $linventory->returnArray($sql, 2);
                
                // ## Get Group IN charge ###
                $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$item_id."' AND LocationID = '".$hiddenLocationID."'";
                $arr_group_in_charge = $linventory->returnVector($sql);
                
                if ($targetGroupInCharge == "") {
                    $targetGroupInCharge = $arr_group_in_charge[0];
                }
                // ## Check user has rights edit the item or not
                // ## Role admin, Group leader <== can edit
                // ## Group member <== can't edit
                if (! ($linventory->IS_GROUP_ADMIN($targetGroupInCharge) || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eInventory"])) {
                    $linventory->NO_ACCESS_RIGHT_REDIRECT();
                    exit();
                }
                
                // $group_in_charge_selection = getSelectByArray($arr_admin_group, " name='targetGroupInCharge' ", $targetGroupInCharge,0,1);
                // $table_content .= "<tr><td class='field_title'>".$i_InventorySystem['Caretaker']." <span class=\"tabletextrequire\">*</span></td>
                // <td colspan=\"3\" class=\"tabletext\" valign=\"top\">$group_in_charge_selection</td></tr>";
                
                /*
                 * ### Get Funding Source ###
                 * $sql = "SELECT FundingSourceID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_FUNDING_SOURCE";
                 * $arr_funding_soruce = $linventory->returnArray($sql,2);
                 *
                 * $sql = "SELECT FundingSourceID FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $hiddenLocationID";
                 * $arr_target_funding = $linventory->returnVector($sql);
                 *
                 * if($targetFundingSource == ""){
                 * $targetFundingSource = $arr_target_funding[0];
                 * }
                 *
                 * $funding_source_selection = getSelectByArray($arr_funding_soruce , " name='targetFundingSource' ", $targetFundingSource,0,1);
                 * $table_content .= "<tr><td class='field_title'>".$i_InventorySystem['FundingSource']." <span class=\"tabletextrequire\">*</span></td>
                 * <td colspan=\"3\" class=\"tabletext\" valign=\"top\">$funding_source_selection</td></tr>";
                 */
                
                // ## Get Bulk Item Variance Admin ###
                $sql = "SELECT BulkItemAdmin from INVENTORY_ITEM_BULK_EXT WHERE ItemID = '".$item_id."'";
                $arr_variance_manager = $linventory->returnVector($sql);
                if (sizeof($arr_variance_manager) > 0) {
                    $variance_manager_id = $arr_variance_manager[0];
                }
                
                $variance_manager_selection = getSelectByArray($arr_admin_group, " name=\"targetVarianceManager\" ", $variance_manager_id);
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Settings_BulkItemAdmin</td>
									<td colspan=\"3\" class=\"tabletext\" valign=\"top\">$variance_manager_selection</td></tr>";
                
                if ($item_StockTakeOption == "YES") {
                    $StockTakeOptionCapitalChecked = "checked='checked'";
                } elseif ($item_StockTakeOption == "NO") {
                    $StockTakeOptionNoChecked = "checked='checked'";
                } else {
                    $StockTakeOptionPriceChecked = "checked='checked'";
                }
                $StockTakeOptions = "<input type='radio' id='StockTakeOptionPrice' name='StockTakeOption' value='FOLLOW_SETTING' {$StockTakeOptionPriceChecked} /> <label for='StockTakeOptionPrice'>" . $i_InventorySystem['StockTakeOptionPrice'] . "</label>";
                $StockTakeOptions .= "<br /><input type='radio' id='StockTakeOptionCapital' name='StockTakeOption' value='YES' {$StockTakeOptionCapitalChecked} /> <label for='StockTakeOptionCapital'>" . $i_InventorySystem['StockTakeOptionCapital'] . "</label>";
                $StockTakeOptions .= "<br /><input type='radio' id='StockTakeOptionNo' name='StockTakeOption' value='No'  {$StockTakeOptionNoChecked} /> <label for='StockTakeOptionNo'>" . $i_InventorySystem['StockTakeOptionNo'] . "</label>";
                
                $table_content .= "<tr><td class='field_title'>" . $i_InventorySystem['StockTake'] . "</td><td class=\"tabletext\" valign=\"top\" colspan=\"3\">$StockTakeOptions</td></tr>\n";
                
                // #
                // # remove current item photo - under development
                if ($item_photo_id != "") {
                    $delete_photo = "<input type='checkbox' name='delete_photo' value=1> $i_InventorySystem_DeleteItemPhoto <br>";
                }
                
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Photo</td>
										<td class=\"tabletext\" valign=\"top\">$delete_photo<input type=\"file\" name=\"item_photo\" class=\"file\" $disable><input type=\"hidden\" name=\"hidden_item_photo\"><br/><span class=\"tabletextremark\">$i_InventorySystem_PhotoGuide</span></td></tr>\n";
                // #
                
                $table_content .= "<tr><td class='field_title'>$i_InventorySystem_Item_Attachment</td><td class=\"tabletext\" valign=\"top\">";
                $sql = "SELECT AttachmentID, AttachmentPath, Filename FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE ItemID = '".$item_id."'";
                $arr_tmp_exist_attachment = $linventory->returnArray($sql, 2);
                if (sizeof($arr_tmp_exist_attachment) > 0) {
                    $table_content .= "<table border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
                    for ($i = 0; $i < sizeof($arr_tmp_exist_attachment); $i ++) {
                        list ($file_id, $file_path, $file_name) = $arr_tmp_exist_attachment[$i];
                        // $filename = substr($file_name,1,strlen($file_name));
                        $attachment = $file_path . $file_name;
                        
                        $table_content .= "<tr><td class=\"tabletext\">$file_name</td><td class=\"tabletext\"><a class=\"tablelink\" href=\"javascript:remove_attachment('$item_id','$file_id');\">$button_remove</a></td></tr>";
                    }
                    $table_content .= "</table>";
                    $table_content .= "<br>";
                }
                
                $table_content .= "<table border=\"0\" id=\"upload_file_list\" cellpadding=\"0\" cellspacing=\"0\" class=\"inside_form_table\">";
                for ($i = 0; $i < $no_file; $i ++) {
                    $table_content .= "<tr><td><input class=\"file\" type=\"file\" name=\"item_attachment_$i\" /><br />";
                    $table_content .= "<input type=\"hidden\" name=\"hidden_item_attachment_$i\" /></td></tr>";
                }
                $table_content .= "</table>";
                $table_content .= "<input type=button value=\" + \" onClick=\"add_field()\">";
                
                $table_content .= "</td></tr>\n";
            }
            
            $table_content .= "<input type=\"hidden\" name=\"item_code\" value=\"$item_code\">\n";
            $table_content .= "<input type=\"hidden\" name=\"item_id\" value=\"$item_id\">\n";
            $table_content .= "<input type=\"hidden\" name=\"item_type\" value=\"$item_type\">\n";
            $table_content .= "<input type=\"hidden\" name=\"item_cat\" value=\"$item_cat\">\n";
            $table_content .= "<input type=\"hidden\" name=\"item_cat2\" value=\"$item_cat2\">\n";
            // $table_content .= "<input type=\"hidden\" name=\"item_photo_id\" value=\"$item_photo_id\">\n";
            
            // $table_content .= "<tr><td colspan=\"4\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\" /></td></tr>";
            // $table_content .= "<tr><td height=\"10px\"></td></tr>";
        }
    }
}

// TAG data
$tagAry = returnModuleAvailableTag($linventory->ModuleName, 1);

$tagNameAry = array_values($tagAry);

$delim = "";
if (sizeof($tagNameAry > 0)) {
    foreach ($tagNameAry as $tag) {
        $availableTagName .= $delim . "\"" . addslashes($tag) . "\"";
        $delim = ", ";
    }
}

$tagDisplay = $linventory->displayTags($item_list);

?>

<script language="javascript">var no_of_upload_file = <?echo $no_file ==""?5:$no_file;?>;</script>
<?//=$js_init;?>
<script language="javascript">
function grapAttach(cform)
{
	if(document.form1.targetItemType == 1)
	{
		var obj = document.form1.item_photo;
		var key;
		var s="";
		key = obj.value;
		if (key!="")
			s += key;
		cform.attachStr.value = s;
	}
}

function add_field()
{
	var table = document.getElementById("upload_file_list");
	var row = table.insertRow(no_of_upload_file);
	//if (document.all)
	if (document.getElementById)
	{
		var cell = row.insertCell(0);
		x= '<input class="file" type="file" name="item_attachment_'+no_of_upload_file+'" size="40">';
		x+='<input type="hidden" name="hidden_item_attachment_'+no_of_upload_file+'">';

		cell.innerHTML = x;
		no_of_upload_file++;
		
		document.form1.attachment_size.value = no_of_upload_file;
	}
}

function Big5FileUploadHandler() {

	 for(i=0;i<=no_of_upload_file;i++)
	 {
		 	objFile = eval('document.form1.item_attachment_'+i);
		 	objUserFile = eval('document.form1.hidden_item_attachment_'+i);
		 	if(objFile!=null && objFile.value!='' && objUserFile!=null)
		 	{
			 	var Ary = objFile.value.split('\\');
			 	objUserFile.value = Ary[Ary.length-1];
			}
	 }
	 
	 objPhoto = eval('document.form1.item_photo');
	 objItemPhoto = eval('document.form1.hidden_item_photo');
	 if(objPhoto!=null && objPhoto.value!='' && objItemPhoto!=null)
	 {
	 	var Ary2 = objPhoto.value.split('\\');
	 	objItemPhoto.value = Ary2[Ary2.length-1];
 	 }
	 return true;
}

function checkForm()
{
	var obj=document.form1;
	var error_no = 0;
	var item_type = <?=$item_type?>;
	var ItemUnitPrice1 = obj.item_unit_price1;
	var ItemUnitPrice2 = obj.item_unit_price2;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	if(document.form1.flag.value == 1)
	{
		// Item Name (Chinese)
		if(!check_text_30(obj.item_chi_name, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_Item_ChineseName; ?>.", "div_ChiName_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "item_chi_name";
		}
		
		// Item Name (English)
		if(!check_text_30(obj.item_eng_name, "<?php echo $i_alert_pleasefillin.$i_InventorySystem_Item_EnglishName; ?>.", "div_EngName_err_msg"))
		{
			error_no++;
			if(focus_field=="")	focus_field = "item_eng_name";
		}
	
		if(item_type==1)
		{
			<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
			// Purchase Price
			if(obj.item_purchase_price.value!='' && !checkUnitPrice(obj.item_purchase_price,obj.item_purchase_price.value))
			{
				document.getElementById('div_PurchasePrice_err_msg').innerHTML = "<font color=red><?=$i_alert_pleasefillin . $i_InventorySystem_Item_Price;?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "item_purchase_price";
			}
			
			//if(obj.item_purchase_price.value!='' && !checkUnitPrice(obj.item_purchase_price,obj.item_purchase_price.value))
			if(!checkUnitPrice(obj.item_purchase_price,obj.item_purchase_price.value))
			{
				document.getElementById('div_PurchasePrice_err_msg').innerHTML = "<font color=red><?=$Lang['eInventory']['InvalidPurchasePrice']?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "item_purchase_price";
			}
			<? } ?>
		
			// Unit Price (1)
			if(ItemUnitPrice1.value!='' && !checkUnitPrice(ItemUnitPrice1,ItemUnitPrice1.value))
			{
				document.getElementById('div_UnitPrice_err_msg1').innerHTML = "<font color=red><?=$Lang['eInventory']['InvalidUnitPrice']?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "item_unit_price1";
			}
			
			// Unit Price (2)
			if(ItemUnitPrice2.value!='' && !checkUnitPrice(ItemUnitPrice2,ItemUnitPrice2.value))
			{
				document.getElementById('div_UnitPrice_err_msg2').innerHTML = "<font color=red><?=$Lang['eInventory']['InvalidUnitPrice']?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "item_unit_price2"; 
			}
			
			// check funding 1 and 2
			if(obj.targetFundingSource.value == obj.targetFundingSource2.value)
			{
				document.getElementById('div_Funding_err_msg').innerHTML = "<br><font color=red><?=$Lang['eInventory']['DuplicateFundingSource']?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "targetFundingSource";  
			}
			
			<? if($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired']) { ?>
			if(obj.item_purchase_date.value=="")
			{
				document.getElementById('div_PurchaseDate_err_msg').innerHTML = "<font color=red><?=$i_alert_pleasefillin . $i_InventorySystem_Item_Purchase_Date;?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "item_purchase_date";
			}
			<? } ?>
			
			// check date format
			if(obj.item_purchase_date.value != "")
			{
				if(!check_date_without_return_msg(obj.item_purchase_date)){
					error_no++;
					if(focus_field=="")	focus_field = "item_purchase_date";
				}
			}

			// check location is null 
			if(obj.targetLocation.value == "")
			{
				document.getElementById('div_Location_err_msg').innerHTML = "<font color=red><?=$i_InventorySystem_Input_Item_LocationSelection_Warning?></font>";
				error_no++;
				if(focus_field=="")	focus_field = "targetLocation";
			}
			
			if(obj.item_warranty_expiry_date != undefined)
			{
				if(obj.item_warranty_expiry_date.value != "")
				{
					if(!check_date_without_return_msg(obj.item_warranty_expiry_date)){
						error_no++;
						if(focus_field=="")	focus_field = "item_warranty_expiry_date";
					}
				}
			}
			
		}
	}
	else
	{
		document.form1.action = "";
		return false;
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
// 		obj.category_id.value = obj.targetCategory.value;
// 		obj.category2_id.value = obj.targetCategory2.value;
// 		obj.purchase_type.value = obj.purchaseType.value;
		
		Big5FileUploadHandler();
		document.form1.action = "category_show_items_edit_update.php";
		return true;
	}
	
}


function reset_innerHtml()
{
	var item_type = <?=$item_type?>;
	
 	document.getElementById('div_EngName_err_msg').innerHTML = "";
  	document.getElementById('div_ChiName_err_msg').innerHTML = "";
  	
  	if(item_type==1)
  	{
 		document.getElementById('div_Funding_err_msg').innerHTML = "";
 		document.getElementById('div_UnitPrice_err_msg1').innerHTML = "";
 		document.getElementById('div_UnitPrice_err_msg2').innerHTML = "";
 		<? if(!$sys_custom['eInventoryCustForSMC']) {?>	
		 	document.getElementById('div_PurchasePrice_err_msg').innerHTML = "";
	 	<? } ?>
 	
	}
//  	document.getElementById('div_Category_err_msg').innerHTML = "";
//  	document.getElementById('div_SubCategory_err_msg').innerHTML = "";
//  	document.getElementById('div_Qty_err_msg').innerHTML = "";
//  	document.getElementById('div_UnitPrice_err_msg').innerHTML = "";
//  	document.getElementById('div_purchase_details_err_msg').innerHTML = "";
//  	document.getElementById('div_existing_item_err_msg').innerHTML = "";
 	
}

function checkUnitPrice(fieldName, fieldValue) {
	decallowed = 2;  // how many decimals are allowed?
	
	if (isNaN(fieldValue) || fieldValue == "") {
		return false;
	}
	else {
		if (fieldValue.indexOf('.') == -1) fieldValue += ".";
		dectext = fieldValue.substring(fieldValue.indexOf('.')+1, fieldValue.length);

		if (dectext.length > decallowed)
		{
			return false;
      	}
		else {
			return true;
      	}
   	}
}

function update_unit_price()
{
	var obj=document.form1;
	var ItemUnitPrice = obj.item_unit_price;
	var ItemUnitPrice1 = obj.item_unit_price1;
	var ItemUnitPrice2 = obj.item_unit_price2;
	var final_item_unit_price = 0;
	
	if(checkUnitPrice(ItemUnitPrice1,ItemUnitPrice1.value))
	{
		final_item_unit_price = final_item_unit_price + parseFloat(ItemUnitPrice1.value);
	}
	
	if(checkUnitPrice(ItemUnitPrice2,ItemUnitPrice2.value))
	{
		final_item_unit_price = final_item_unit_price + parseFloat(ItemUnitPrice2.value);
	}
	
	ItemUnitPrice.value = final_item_unit_price.toFixed(2);
}	


//$(document).ready(function(){ 
//	$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
//	$('#item_purchase_date').datepick({
//	dateFormat: 'yy-mm-dd',
//	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
//	changeFirstDay: false,
//	firstDay: 0
//	});
//	$.datepick.setDefaults({showOn: 'both', buttonImageOnly: true, buttonImage: '<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_calendar_off.gif', buttonText: 'Calendar'});
//	$('#item_warranty_expiry_date').datepick({
//	dateFormat: 'yy-mm-dd',
//	dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
//	changeFirstDay: false,
//	firstDay: 0
//	});
//});

function remove_attachment(ItemID,AttachmentID) {
	var confirm = window.confirm(globalAlertMsg3);
	if (confirm) {
		document.form1.action = "remove_item_attachment.php?ItemID=" + ItemID + "&AttachmentID=" + AttachmentID;
		document.form1.submit();
	}
}
</script>

<br>

<form name="form1" enctype="multipart/form-data" action="" method="post"
	onSubmit="return checkForm();">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
	<?=$infobar1?>
</table>

	<br>

	<table class="form_table_v30">
	<?=$table_content?>
	
	
<tr>
			<td class="field_title"><?=$Lang['eDiscipline']['Tag']?></td>
			<td colspan="3"><input type="text" name="Tags" id="Tags"
				value="<?=intranet_htmlspecialchars($tagDisplay)?>"
				class="textboxtext2" /><br /> <span class="tabletextremark"><?=$Lang['StudentRegistry']['CommaAsSeparator'].", ".$Lang["eInventory"]["Max10Words"]?> </span>
			</td>
		</tr>

	</table>

	<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "grapAttach(this.form)")?>
	<?= $linterface->GET_ACTION_BTN($button_reset, "button", "javascript: reset_innerHtml(); this.form.reset();","reset2")?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='category_show_items_detail.php?type=1&item_id=$item_id'","cancelbtn")?>
</div>

	<input type="hidden" name="flag" value=1> <input type="hidden"
		name="item_list" value="<?=$item_list?>"> <input type="hidden"
		name="attachStr" value="" /> <input type="hidden"
		name="targetItemType" value="<?=$item_type?>"> <input type="hidden"
		name="attachment_size" value="<? echo $no_file==""?5:$no_file;?>"> <input
		type="hidden" name="hiddenLocationID" value="<?=$hiddenLocationID;?>">

	<link rel="stylesheet"
		href="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.ui.all.css"
		type="text/css" media="all" />
	<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.min.js"
		type="text/javascript"></script>
	<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-ui.min.js"
		type="text/javascript"></script>
	<script src="<?=$PATH_WRT_ROOT?>templates/jquery/multicomplete.min.js"
		type="text/javascript"></script>
	<script language="javascript">

$('#Tags').multicomplete({
	source: [<?=$availableTagName?>]
});
$.noConflict(true);
</script>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>