<?php
// using:

// #######################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2019-05-01 Henry
// security issue fix for SQL
//
// Date: 2018-02-21 Henry
// add access right checking [Case#E135442]
//
// Date: 2017-01-13 Cameron
// Do not allow to change location for single item if it's been applied for write-off [case #P104633]
//
// Date: 2016-02-01 Henry
// PHP 5.4 fix: change split to explode
//
// Date: 2015-04-16 Cameron
// - use double quote for string "$image_path/$LAYOUT_SKIN"
// - allow to add multiple location for bulk items
// - show success / fail message after submit record
//
// Date: 2013-04-10 YatWoon
// Improved: set submit button disabled during the form is processing [Case#2013-0403-1210-51156]
//
// Date: 2013-01-30 Rita
// change member update location checking
//
// Date: 2012-10-11 Rita
// revise access right of update location variable, change access right setting
//
// Date: 2012-09-14 Rita
// add customization checking and conditions
//
// Date: 2012-05-25 YatWoon
// cater with group and funding
//
// Date: 2011-06-23 YatWoon
// improved: cannot update the new location same as original location
//
// Date: 2011-05-23 YatWoon
// hidden Resources Mgmt Group selection
//
// #######################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ItemID = IntegerSafe($ItemID, ":");
$item_id = IntegerSafe($item_id);

$llocation_ui = new liblocation_ui();

// Customization
if ($linventory->enableMemberUpdateLocationRight()) {
    
    if (! empty($ItemID) && empty($item_id)) {
        $item_id_temp = explode(":", $ItemID[0]);
        $item_id = $item_id_temp[0];
        $targetLocation = $item_id_temp[1];
        $targetGroupID = $item_id_temp[2];
        $targetFundingID = $item_id_temp[3];
        
        $numOfItemIDArr = count($item_id_temp);
        
        if ($numOfItemIDArr == 4) {
            
            $tempItemType = ITEM_TYPE_BULK;
            $groupInCharge = $targetGroupID;
        } else {
            $tempItemType = ITEM_TYPE_SINGLE;
            $groupInCharge = $linventory->GET_ITEM_GROUP_IN_CHARGE($item_id, $targetLocation);
        }
        
        $groupUserTypeArr = $linventory->getGroupUserType($groupInCharge);
        $groupUserType = $groupUserTypeArr[0]['RecordType'];
        
        if ($groupUserType == 1) {
            // Generate Page Tag #
            // $TAGS_OBJ[] = array($i_InventorySystem_Item_UpdateStatus, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_status_edit.php?item_id=$item_id&targetLocation=$targetOldLocation", 0);
            // $TAGS_OBJ[] = array($i_InventorySystem_Item_UpdateLocation, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_location_edit.php?item_id=$item_id&targetLocation=$targetOldLocation", 1);
            // $TAGS_OBJ[] = array($i_InventorySystem_Item_RequestWriteOff, $PATH_WRT_ROOT."home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_request_write_off.php?item_id=$item_id&targetLocation=$targetOldLocation", 0);
            $TAGS_OBJ[] = array(
                $i_InventorySystem_Item_UpdateStatus,
                "javascript:click_tab('update_status');",
                0
            );
            $TAGS_OBJ[] = array(
                $i_InventorySystem_Item_UpdateLocation,
                "javascript:click_tab('change_location');",
                1
            );
            $TAGS_OBJ[] = array(
                $i_InventorySystem_Item_RequestWriteOff,
                "javascript:click_tab('request_writeoff');",
                0
            );
            // End #
        } else {
            $TAGS_OBJ[] = array(
                $i_InventorySystem_Item_UpdateLocation
            );
        }
    } elseif (empty($ItemID) && ! empty($item_id)) {
        $TAGS_OBJ[] = array(
            $i_InventorySystem_Item_UpdateStatus,
            "javascript:click_tab('update_status');",
            0
        );
        $TAGS_OBJ[] = array(
            $i_InventorySystem_Item_UpdateLocation,
            "javascript:click_tab('change_location');",
            1
        );
        $TAGS_OBJ[] = array(
            $i_InventorySystem_Item_RequestWriteOff,
            "javascript:click_tab('request_writeoff');",
            0
        );
    } else {
        
        header("location: items_full_list.php");
    }
} else {
    $TAGS_OBJ[] = array(
        $i_InventorySystem_Item_UpdateStatus,
        "javascript:click_tab('update_status');",
        0
    );
    $TAGS_OBJ[] = array(
        $i_InventorySystem_Item_UpdateLocation,
        "javascript:click_tab('change_location');",
        1
    );
    $TAGS_OBJ[] = array(
        $i_InventorySystem_Item_RequestWriteOff,
        "javascript:click_tab('request_writeoff');",
        0
    );
}

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

// Generate Page Info #
$sql = "SELECT 
				" . $linventory->getInventoryItemNameByLang("a.") . ",
				" . $linventory->getInventoryItemNameByLang("b.") . ",
				" . $linventory->getInventoryItemNameByLang("c.") . "
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
		WHERE
				a.ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 3);

$temp[] = array(
    $result[0][1],
    ""
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";
$infobar2 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION2($result[0][2] . " > " . $result[0][0] . " > " . $Lang['eInventory']['FieldTitle']['UpdateItemLocation']) . "</td></tr>";
// End #

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$arr_item_type = $linventory->returnVector($sql);
$item_type = $arr_item_type[0];

if ($item_type == ITEM_TYPE_SINGLE) {
    // exclude reject application
    $sql = "SELECT RecordID FROM INVENTORY_ITEM_WRITE_OFF_RECORD where ItemID='$item_id' AND RecordStatus<>2";
    $rs = $linventory->returnResultSet($sql);
    $in_write_off = (count($rs) > 0) ? true : false;
    
    $sql = "SELECT 
					a.LocationID, CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
			FROM 
					INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
					INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS d ON (c.BuildingID = d.BuildingID)
			WHERE 
					a.ItemID = '".$item_id."'
			";
    
    $arr_result = $linventory->returnArray($sql, 2);
    if (sizeof($arr_result) > 0) {
        /*
         * $building_selection = $llocation_ui->Get_Building_Selection($targetBuilding, 'targetBuilding', 'resetLocationSelection(\'Building\'); document.form1.flag.value=0; this.form.submit();', 0, 0, '', 1);
         *
         * if($targetBuilding != "")
         * {
         * $location_selection = $llocation_ui->Get_Floor_Selection($targetBuilding, $targetLocation, 'targetLocation', 'resetLocationSelection(\'Floor\'); document.form1.flag.value=0; this.form.submit();', 0, 0, '');
         * }
         *
         * if(($targetLocation != "") && ($targetBuilding != ""))
         * {
         * $sub_location_selection = $llocation_ui->Get_Room_Selection($targetLocation, $targetSubLocation, 'targetSubLocation', '', 0, '', '');
         * }
         */
        
        $new_sub_location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetSubLocation, "targetSubLocation");
        
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($location_id, $location_name) = $arr_result[$i];
            $targetLocation = $location_id;
            $table_content .= " <tr>
									<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_OriginalLocation</td>
									<td class=\"tabletext\">$location_name</td>
								</tr>";
            if (! $in_write_off) {
                $table_content .= " <tr>
										<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_NewLocation</td>
										<td class=\"tabletext\">$new_sub_location_selection</td>
									</tr>";
            }
            $table_content .= "<input type=\"hidden\" name=\"origianl_location_id\" value=\"$location_id\">";
        }
    }
}
if ($item_type == ITEM_TYPE_BULK) {
    /*
     * $sql = "SELECT
     * DISTINCT b.LocationID,
     * CONCAT(".$linventory->getInventoryNameByLang("d.").",' > ',".$linventory->getInventoryNameByLang("c.").",' > ',".$linventory->getInventoryNameByLang("b.").")
     * FROM
     * INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
     * INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
     * INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
     * INVENTORY_LOCATION_BUILDING AS d ON (c.BuildingID = d.BuildingID)
     * WHERE
     * a.ItemID = $item_id";
     */
    
    if (! $linventory->IS_ADMIN_USER($UserID)) {
        $admin_group_list = $linventory->getInventoryAdminGroup();
        $group_cond = "AND GroupInCharge IN ($admin_group_list)";
    }
    $sql = "SELECT 
					DISTINCT b.LocationID, 
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocFloor.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
			FROM 
					INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN
					INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS LocFloor ON (b.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID)
			WHERE
					a.ItemID = '".$item_id."'
					$group_cond
			order by LocBuilding.DisplayOrder, LocFloor.DisplayOrder, b.DisplayOrder
			";
    $arr_old_location = $linventory->returnArray($sql, 2);
    $old_location_selection = getSelectByArray($arr_old_location, "name=\"targetLocation\" onChange=\"change_filter('location');\"", $targetLocation, 0, 1);
    $table_content .= " <tr>
							<td valign=\"top\" width=\"40%\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_OriginalLocation</td>
							<td class=\"tabletext\" td colspan=2>$old_location_selection</td>
						</tr>";
    
    // group #
    if (! $targetGroupID) {
        // retrieve the first groupid
        $check_own_group = empty($admin_group_list) ? "" : " and GroupInCharge in ($admin_group_list)";
        $sql = "select GroupInCharge from INVENTORY_ITEM_BULK_LOCATION where LocationID = $targetLocation and ItemID = $item_id $check_own_group limit 1";
        $result = $linventory->returnVector($sql);
        $targetGroupID = $result[0];
    }
    $check_own_group = empty($admin_group_list) ? "" : " and c.GroupInCharge in ($admin_group_list)";
    $sql = "SELECT 
				distinct(a.AdminGroupID), 
				" . $linventory->getInventoryNameByLang() . " as GroupName
			FROM 
				INVENTORY_ADMIN_GROUP as a 
				left join INVENTORY_ADMIN_GROUP_MEMBER as b on (b.AdminGroupID=a.AdminGroupID and b.RecordType=1)
				left join INVENTORY_ITEM_BULK_LOCATION as c on (c.GroupInCharge=a.AdminGroupID and c.LocationID = $targetLocation)
			WHERE 
				c.ItemID = '".$item_id."'
				$check_own_group
			 ORDER BY 
			 	a.DisplayOrder
			";
    $group_array = $linventory->returnArray($sql);
    $group_selection = getSelectByArray($group_array, "name=targetGroupID onChange=\"change_filter('group');\" ", $targetGroupID, 0, 1);
    $table_content .= " <tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['Caretaker'] . "</td>
								<td class=\"tabletext\" td colspan=2>$group_selection</td>
							</tr>";
    // end group #
    
    // funding #
    if (! $targetFundingID) {
        // retrieve the first groupid
        $sql = "select 
					a.FundingSourceID 
				from 
					INVENTORY_ITEM_BULK_LOCATION as a
					left join INVENTORY_FUNDING_SOURCE as b on b.FundingSourceID=a.FundingSourceID
				where 
					a.LocationID = '".$targetLocation."' and 
					a.ItemID = '".$item_id."' and 
					a.GroupInCharge='".$targetGroupID."' 
				ORDER BY 
					b.DisplayOrder 
				limit 1
				";
        $result = $linventory->returnVector($sql);
        $targetFundingID = $result[0];
    }
    
    $sql = "SELECT 
				a.FundingSourceID, 
				" . $linventory->getInventoryItemNameByLang() . "
			FROM 
				INVENTORY_FUNDING_SOURCE as a
				left join INVENTORY_ITEM_BULK_LOCATION as b on (b.FundingSourceID=a.FundingSourceID and b.LocationID = $targetLocation and b.GroupInCharge=$targetGroupID)
			where 	
				b.ItemID = ".$item_id."
			order by 
				a.DisplayOrder";
    $arr_funding_source = $linventory->returnArray($sql);
    $funding_selection = getSelectByArray($arr_funding_source, " name=\"targetFundingID\" onChange=\"change_filter('funding');\" ", $targetFundingID, 0, 1);
    $table_content .= " <tr>
								<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">" . $i_InventorySystem['FundingSource'] . "</td>
								<td class=\"tabletext\" td colspan=2>$funding_selection</td>
							</tr>";
    // end funding #
    
    // #### original qty
    $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetLocation and GroupInCharge=$targetGroupID and FundingSourceID=$targetFundingID";
    $arr_old_qty = $linventory->returnVector($sql);
    $table_content .= " <tr>
							<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_OriginalQuantity</td>
							<td class=\"tabletext\" td colspan=2>$arr_old_qty[0]</td>
							<input type=\"hidden\" name=\"bulk_old_qty\" value=\"" . $arr_old_qty[0] . "\">
						</tr>";
    
    // #### new location
    $new_sub_location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetNewSubLocation, "targetNewSubLocation[]");
    $add_location_btn = $linterface->GET_SMALL_BTN("+", "button", "addLocationRow();", "add_location_btn");
    
    $table_content .= " <tr>
							<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">$i_InventorySystem_UpdateItemLocation_NewLocation / $i_InventorySystem_UpdateItemLocation_NewQuantity</td>
							<td td colspan=2>$new_sub_location_selection <input type=\"text\" name=\"bulk_new_qty[]\" class=\"textboxnum\"> </td>
						</tr>";
    $table_content .= " <tr><td></td><td colspan=2>" . $add_location_btn . "</td></tr>";
    
    // $new_sub_location_selection = $llocation_ui->Get_Building_Floor_Room_Selection($targetNewSubLocation, "targetNewSubLocation");
    // $table_content .= " <tr>
    // <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_NewLocation</td>
    // <td class=\"tabletext\">$new_sub_location_selection</td>
    // </tr>";
    // $table_content .= " <tr>
    // <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_NewQuantity</td>
    // <td class=\"tabletext\"><input type=\"text\" name=\"bulk_new_qty\" class=\"textboxnum\"></td>
    // </tr>";
    
    // if(($targetLocation != ""))
    // {
    // $table_content .= " <tr>
    // <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_OriginalQuantity</td>
    // <td class=\"tabletext\">$arr_old_qty[0]</td>
    // <input type=\"hidden\" name=\"bulk_old_qty\" value=\"".$arr_old_qty[0]."\">
    // </tr>";
    // $table_content .= " <tr>
    // <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_NewLocation</td>
    // <td class=\"tabletext\">$new_sub_location_selection</td>
    // </tr>";
    // $table_content .= " <tr>
    // <td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\">$i_InventorySystem_UpdateItemLocation_NewQuantity</td>
    // <td class=\"tabletext\"><input type=\"text\" name=\"bulk_new_qty\" class=\"textboxnum\"></td>
    // </tr>";
    // }
}

$table_content .= "<input type=\"hidden\" name=\"item_type\" value=\"$item_type\">";
$table_content .= "<input type=\"hidden\" name=\"item_id\" value=\"$item_id\">";
$table_content .= "<input type=\"hidden\" name=\"flag\" value=\"1\">";

?>

<script language="javascript">
function addLocationRow()
{
	var btnRow = $('input#add_location_btn').closest('tr');
	var row_html ='<tr><td valign="top" width="40%" nowrap="nowrap" class="formfieldtitle"><?=$i_InventorySystem_UpdateItemLocation_NewLocation?> / <?=$i_InventorySystem_UpdateItemLocation_NewQuantity?></td>';
	row_html += '<td nowrap="nowrap" width="0%"><?=str_replace("'", "\'",str_replace("\n", "",$new_sub_location_selection))?> <input type="text" name="bulk_new_qty[]" class="textboxnum"></td><td align="left" width="100%"><span class="table_row_tool row_content_tool"><a href="javascript:void(0);" class="delete_dim" onclick="removeLocationRow(this);"></a></span></td>';
	btnRow.before(row_html);
}

function removeLocationRow(obj)
{
	var row = $(obj).closest('tr');
	if(row){
		row.remove();
	}
}

function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("targetLocation")){
			document.getElementById("targetLocation").value = "";
		}
		if(document.getElementById("targetSubLocation")){
			document.getElementById("targetSubLocation").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("targetSubLocation")){
			document.getElementById("targetSubLocation").value = "";
		}
	}
}

function checkForm()
{
	var item_type = <?=$item_type?>;
	var tmp_check = 0;
	var ori_location = 0;
	var new_location=0;
	
	if(item_type == 1)
	{
		if(check_select(document.form1.targetSubLocation,"<?=$i_InventorySystem_Input_Item_LocationSelection_Warning;?>",''))	
		{
			if(document.form1.flag.value == 1)
			{
				tmp_check = 1;
			}
			else
			{
				tmp_check = 0;
			}
		}
		
		ori_location = <?=$targetLocation?>;
		new_location = document.form1.targetSubLocation.value;
		// Check same location?
		if(ori_location==new_location)
		{
			alert("<?=$Lang['eInventory']['JSWarning']['NewLocationSameAsOriginalLocation']?>");
			tmp_check = 0;
		}
	}
	
	if(item_type == 2)
	{
		var location_ary = document.getElementsByName('targetNewSubLocation[]');
		var bulk_qty_ary = document.getElementsByName('bulk_new_qty[]');
		var total_qty = 0;
		tmp_check = 1;
		for(var i=0; i<location_ary.length; i++)
		{
			// check location empty or not
			var this_location = $(location_ary[i]);
			if(this_location.val()=="")
			{
				tmp_check = 0;
				alert("<?=$i_InvertorySystem_SelectNewLocation_Warning?>");
				this_location.focus();
				break;
			}
			
			// check the location is same as original or not
			if(ori_location==this_location.val())
			{
				alert("<?=$Lang['eInventory']['JSWarning']['NewLocationSameAsOriginalLocation']?>");
				tmp_check = 0;
				this_location.focus();
				break;
			}
			
			// check bulk qty valid or not
			var this_qty = $(bulk_qty_ary[i]);
			total_qty += parseInt(this_qty.val());
			if(this_qty.val()=="")
			{
				tmp_check = 0;
				alert("<?=$i_InventorySystem_StockCheck_EmptyQuantityWarning?>");
				this_qty.focus();
				break;
			}
			if(!is_positive_int(this_qty.val()) || this_qty.val()<1)
			{
				tmp_check = 0;
				alert("<?=$i_InventorySystem_StockCheck_ValidQuantityWarning?>");
				this_qty.focus();
				break;
			}
		}
		
		// check total relocate bulk item qty <= original qty
		if(total_qty > <?=($arr_old_qty[0] ? $arr_old_qty[0] : 0)?>)
		{
			tmp_check = 0;
			alert("<?=$Lang['eInventory']['NotEnoughQty']?>");
		}

	}
	
	
	if(tmp_check == 1)
	{
		document.getElementById('submit_btn').disabled  = true; 
		document.getElementById('submit_btn').className  = "formbutton_disable print_hide"; 
		document.form1.action = "items_location_edit_update.php";
		return true;
	}
	else
	{
		return false;
	}
}


function change_filter(filter)
{
	var obj = document.form1;
	
	if(filter=="location")
	{
		obj.targetGroupID.selectedIndex=-1; 
		obj.targetFundingID.selectedIndex=-1; 
	}
	
	if(filter=="group")
	{
		obj.targetFundingID.selectedIndex=-1; 
	}
	
	obj.action="items_location_edit.php";
	obj.submit();
}

function click_tab(tab)
{
	var obj = document.form1;
	if(tab=="update_status")
		obj.action="items_status_edit.php";
	if(tab=="change_location")
		obj.action="items_location_edit.php";
	if(tab=="request_writeoff")
		obj.action="items_request_write_off.php";
	obj.submit();
}
</script>

<br>
<form name="form1" action="" method="post" onSubmit="return checkForm()">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
<?=$infobar1?>
</table>

	<table width="96%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<?=$infobar2?>
</table>
	<br>

<? if ( ($item_type == ITEM_TYPE_SINGLE) && $in_write_off):?>
<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Note']?></legend>
		<?=$Lang['eInventory']['WarningArr']['AlreadyAppliedWriteOff']?>
</fieldset>
<? endif;?>

<table width="96%" border="0" cellpadding="5" cellspacing="0">
<?=$table_content?>
</table>

	<table width="96%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td colspan="2" class="dotline"><img
				src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10"
				height="1"></td>
		</tr>
		<tr>
			<td height="10px"></td>
		</tr>
		<tr>
			<td colspan=2 align=center>
<? if ( !(($item_type == ITEM_TYPE_SINGLE) && $in_write_off)):?> 	
		<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit_btn")?> 
		<?=$linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
<? endif;?>
			<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='items_full_list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
	</td>
		</tr>
	</table>
</form>
<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>