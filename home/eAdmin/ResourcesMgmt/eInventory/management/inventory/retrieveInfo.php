<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();
$linventory	= new libinventory();
$item_type = $Type;
$layer_content .= "<table class='form_table_v30'>";

if($Val == "ItemCode")
{
	$sql = "SELECT ".$linventory->getInventoryItemNameByLang().", ItemCode, RecordStatus FROM INVENTORY_ITEM WHERE ItemType = $item_type order by ItemCode";
	$arr_result = $linventory->returnArray($sql);
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_Code."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Item_Name."</td></tr>";
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($item_name, $item_code, $record_status) = $arr_result[$i];
			$layer_content .= "<tr><td nowrap><font color=\"". ($record_status==1?"black":"red")."\">". ($record_status==1?"":"<span class=\"tabletextrequire\">*</span>")."$item_code</font></td>";
			$layer_content .= "<td><font color=\"". ($record_status==1?"black":"red")."\">$item_name</font></td>";
		}
		
		if($item_type == 1)		# single item
			$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\"><font color=\"red\">$i_InventorySystem_Import_CorrectSingleItemCodeReminder</font></td></tr>";
		else					# bulk item
			$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\"><font color=\"red\">$i_InventorySystem_Import_CorrectBulkItemCodeReminder</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><td colspan='2'>$i_no_record_exists_msg</td></tr>";
	}
}

if($Val == "CategoryCode")
{
	$sql = "SELECT Code, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY WHERE RecordStatus=1 order by DisplayOrder";
	$arr_result = $linventory->returnArray($sql);
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_CategoryCode."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Category_Name."</td></tr>";
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($cat_code, $cat_name) = $arr_result[$i];
			$layer_content .= "<tr><td>$cat_code</td>";
			$layer_content .= "<td>$cat_name</td>";
		}
		$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\"><font color=\"red\">$i_InventorySystem_Import_CorrecrCategory2IndexReminder</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><tdcolspan='2'>$i_no_record_exists_msg</td></tr>";
	}
}

if($Val == "SubCategoryCode")
{
	$sql = "SELECT a.Code, ".$linventory->getInventoryNameByLang('a.').", ".$linventory->getInventoryNameByLang('b.')." FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) WHERE a.RecordStatus=1 AND b.RecordStatus=1 order by b.DisplayOrder, b.Code, a.DisplayOrder, a.Code";
	$arr_result = $linventory->returnArray($sql);
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_Category2Code."</td><td class=\"tabletopnolink\">".$i_InventorySystem_SubCategory_Name."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Category_Name."</td></tr>";
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($cat2_code, $cat2_name, $cat_name) = $arr_result[$i];
			
			$layer_content .= "<tr><td>$cat2_code</td>";
			$layer_content .= "<td>$cat2_name</td>";
			$layer_content .= "<td>$cat_name</td>";
		}
		$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\"><font color=\"red\">$i_InventorySystem_Import_CorrecrCategory2IndexReminder</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><td colspan=3>$i_no_record_exists_msg</td></tr>";
	}
}

if($Val=="GroupCode" || $Val=="VarianceManager")
{
	$sql = "SELECT Code, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_ADMIN_GROUP order by DisplayOrder";
	$arr_result = $linventory->returnArray($sql,2);
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_GroupCode."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Group_Name."</td></tr>";
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($group_code, $group_name) = $arr_result[$i];
			
			$layer_content .= "<tr><td>$group_code</td>";
			$layer_content .= "<td>$group_name</td>";
		}
		$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\"><font color=\"red\">$i_InventorySystem_Import_CorrectGroupIndexReminder</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><td colspan=2>$i_no_record_exists_msg</td></tr>";
	}
}

if($Val=="BuildingCode")
{
	include_once($PATH_WRT_ROOT."includes/liblocation.php");
	$llocation = new Building();
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['BuildingCode']."</td><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['BuildingName']."</td></tr>";

	## get all building (have room only) ##
	$arr_result = $llocation->Get_All_Building(0,1);
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			$layer_content .= "<tr><td>".$arr_result[$i]['Code']."</td>";
			if($intranet_session_language == 'b5'){
				$layer_content .= "<td>".$arr_result[$i]['NameChi']."</td>";
			}else{
				$layer_content .= "<td>".$arr_result[$i]['NameEng']."</td>";
			}
		}
		$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\"><font color=\"red\">".$Lang['eInventory']['FieldTitle']['Import']['CorrecrBuildingIndexReminder']."</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><td class=\"tabletex\" colspan=3>$i_no_record_exists_msg</td></tr>";
	}
}

if($Val=="LocationCode")
{
	$sql = "SELECT 
				DISTINCT floor.Code, CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").")
			FROM 
				INVENTORY_LOCATION_BUILDING as building INNER JOIN 
				INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
				INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
			WHERE
				building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1
			Order by 
				building.DisplayOrder, floor.DisplayOrder";
	$arr_result = $linventory->returnArray($sql);
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['FloorCode']."</td><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['FloorName']."</td></tr>";
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($floor_code, $floor_name) = $arr_result[$i];
			
			$layer_content .= "<tr><td>$floor_code</td>";
			$layer_content .= "<td>$floor_name</td>";
		}
		$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\"><font color=\"red\">".$Lang['eInventory']['FieldTitle']['Import']['CorrecrFloorIndexReminder']."</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><td class=\"tabletex\" colspan=2>$i_no_record_exists_msg</td></tr>";
	}
}

if($Val=="SubLocationCode")
{
	$sql = "SELECT 
				DISTINCT room.Code, CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
			FROM 
				INVENTORY_LOCATION_BUILDING as building INNER JOIN 
				INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
				INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
			WHERE
				building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1
			Order by 
				building.DisplayOrder, floor.DisplayOrder, room.DisplayOrder";
	$arr_result = $linventory->returnArray($sql);
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['RoomCode']."</td><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['RoomName']."</td></tr>";
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($room_code, $room_name) = $arr_result[$i];
			
			$layer_content .= "<tr><td>$room_code</td>";
			$layer_content .= "<td>$room_name</td>";
		}
		$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\"><font color=\"red\">".$Lang['eInventory']['FieldTitle']['Import']['CorrecrFloorIndexReminder']."</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><td class=\"tabletex\" colspan=3>$i_no_record_exists_msg</td></tr>";
	}
}

if($Val=="FundingCode")
{
	$sql = "SELECT Code, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_FUNDING_SOURCE WHERE RecordStatus=1 order by DisplayOrder";
	$arr_result = $linventory->returnArray($sql,2);
	
	$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_FundingSourceCode."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Category_Name."</td></tr>";
	
	if(sizeof($arr_result>0))
	{
		for($i=0; $i<sizeof($arr_result); $i++)
		{
			list($funding_code, $funding_name) = $arr_result[$i];
			
			$layer_content .= "<tr><td>$funding_code</td>";
			$layer_content .= "<td>$funding_name</td>";
		}
		$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"2\"><font color=\"red\">$i_InventorySystem_Import_CorrectFundingIndexReminder</font></td></tr>";
	}
	else
	{
		$layer_content .= "<tr><td class=\"tabletex\" colspan=2>$i_no_record_exists_msg</td></tr>";
	}
}


$layer_content .= "</table>";
$response = returnRemarkLayer($layer_content,"hideMenu('ToolMenu2');", 600, $height);
echo $response;
?>