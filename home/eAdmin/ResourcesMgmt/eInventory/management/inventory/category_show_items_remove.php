<?php
# using: 

#####################
#	Date:	2016-02-01 
#			PHP 5.4 fix: change split to explode
#
#	Date	:	2013-05-21 (YatWoon)
#				add remove Tag function [Case#2013-0516-1048-13156]
#
#	Date	:	2012-10-25 (YatWoon)
#				add eBooking integration, send email to eBooking admin 
#
#	Date:	2011-06-04	YatWoon
#			Improved: add deletion log
#
#	Date:	2011-05-18	YAtWoon
#			Fixed: remove location bulk item will remove "ALL" same bulk item
#
#####################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
// include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable.php");
// include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

// $CurrentPageArr['eInventory'] = 1;
// $CurrentPage	= "Management_InventoryList";
// $linterface 	= new interface_html();
$linventory		= new libinventory();

// 	$item_list = implode(",",$ItemID);

/*
if(sizeof($ItemID)>0)
{
	foreach($ItemID as $k=>$d)
	{
		list($item_list[],$location_list[]) = split(":",$d);
	}
}else{
	header("location: items_full_list.php?msg=13");
}
*/

if(sizeof($ItemID)==0)
	header("location: items_full_list.php?msg=13");


$NoMore = 0;

if(!empty($ItemID))
{
	for($i=0;$i<sizeof($ItemID);$i++)
	{
		list($item_id, $targetLocation, $targetGroupID, $targetFundingID) = explode(":",$ItemID[$i]);
	
// 		$ItemID = $item_list[$i];
// 		$LocationID = $location_list[$i];

		### Add deletion log
		$linventory->DeleteLog($item_id, $targetLocation,$targetGroupID,$targetFundingID);
		
		# check is Single or Bulk
		$item_type = $linventory->returnItemType($item_id);
		
		
		# eBooking integration - send email notification [Start]
		if($item_type == 1)
		{
			$linventory->sendEmailNotification_eBooking($item_type,$item_id, $status_remark, 4);
		}
		else
		{
			$this_location_qty = $linventory->returnBulkItemLocationQty($item_id, $targetLocation, $targetGroupID, $targetFundingID);
			$linventory->sendEmailNotification_eBooking($item_type,$item_id, $status_remark, 4, $targetLocation, $targetGroupID, $targetFundingID, $this_location_qty);
		}
		# eBooking integration - send email notification [End]
		
		if($item_type==1)	# Single
		{
			$NoMore = 1;
		}
		else				# Bulk
		{
			$sql = "select count(*) from INVENTORY_ITEM_BULK_LOCATION where ItemID=$item_id";
			$result = $linventory->returnVector($sql);
			$NoMore = $result[0] > 1 ? 0 : 1;
		}
		
		###############################################
		# No More!  Can remove ALL
		###############################################
		if($NoMore)
		{
			$sql = "DELETE FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
			
			$sql = "DELETE FROM INVENTORY_PHOTO_PART WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
			
			$sql = "DELETE FROM INVENTORY_ITEM_BULK_EXT WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
			
			$sql = "DELETE FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
			
			$sql = "DELETE FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
			
			$sql = "DELETE FROM INVENTORY_ITEM_SINGLE_EXT WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
			
			$sql = "DELETE FROM INVENTORY_ITEM_SINGLE_STATUS_LOG WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
		
			$sql = "DELETE FROM INVENTORY_ITEM WHERE ItemID = $item_id";
			$linventory->db_db_query($sql);
			
			$linventory->saveEntryTag($item_id, $Tags);
		}
		else
		{
			# MUST BULK ITEM ONLY
			if($item_type==2)	# double check
			{
// 				$this_location_qty = $linventory->returnBulkItemLocationQty($item_id, $targetLocation, $targetGroupID, $targetFundingID);
				
				# remove INVENTORY_ITEM_BULK_LOCATION
				$sql = "DELETE FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID=$item_id and LocationID=$targetLocation and GroupInCharge=$targetGroupID and FundingSourceID=$targetFundingID";
				$linventory->db_db_query($sql);
			
				# update INVENTORY_ITEM_BULK_EXT --- Quantity
				$sql = "update INVENTORY_ITEM_BULK_EXT set Quantity = Quantity - $this_location_qty where ItemID=$item_id";
				$linventory->db_db_query($sql);
				
				# insert INVENTORY_ITEM_BULK_LOG --- Qty Change
				$sql = "insert into INVENTORY_ITEM_BULK_LOG 
						(ItemID, RecordDate, Action, QtyChange, PersonInCharge, LocationID, GroupInCharge, FundingSource, DateInput)
						values
						($item_id, now(), ". ITEM_ACTION_REMOVE_LOCATION_ITEM .", -$this_location_qty, ". $_SESSION['UserID'].", $targetLocation, $targetGroupID, $targetFundingID, now())";
				$linventory->db_db_query($sql);	
			}
		}
			
	}
	
		header("location: items_full_list.php?msg=3");
		intranet_closedb();
}
else
{
	header("location: items_full_list.php?msg=13");
	intranet_closedb();
}
?>