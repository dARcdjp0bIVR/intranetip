<?php
// modifying :

/**
 * **************************** Change Log [Start] *******************************
 * 2019-05-01 (Henry) security issue fix for SQL
 * 2018-02-21 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log [End] ********************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$ItemID = IntegerSafe($ItemID);
$targetWriteOffReason = IntegerSafe($targetWriteOffReason);

$curr_date = date("Y-m-d");

$re_path = "/file/inventory/write_off";
$path = "$intranet_root$re_path";

if ($ItemType == 1) {
    /*
     * $sql = "LOCK TABLES INVENTORY_ITEM_SINGLE_STATUS_LOG AS WRITE";
     * $linventory->db_db_query($sql);
     *
     * $sql = "INSERT INTO
     * INVENTORY_ITEM_SINGLE_STATUS_LOG
     * (ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
     * VALUES
     * ($ItemID, '$curr_date', 3, $UserID, '', '', '', NOW(), NOW())
     * ";
     *
     * $result['NewInvItemSingleStatusLog'] = $linventory->db_db_query($sql);
     *
     * $sql = "UNLOCK TABLES";
     * $linventory->db_db_query($sql);
     */
    $sql = "SELECT Action FROM INVENTORY_ITEM_SINGLE_STATUS_LOG WHERE ItemID = '".$single_item_id."' AND Action = 3";
    $arr_tmp_result = $linventory->returnVector($sql);
    if (sizeof($arr_tmp_result) == 0) {
        $sql = "UPDATE INVENTORY_ITEM SET RecordStatus = 0 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO 
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($ItemID, '$curr_date', 3, $UserID, 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        // $arr_success_rec[] = $rec_id;
    }
    
    $sql = "SELECT " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = '".$targetWriteOffReason."'";
    $arr_write_off_reason = $linventory->returnVector($sql);
    $write_off_reason = $arr_write_off_reason[0];
    $write_off_reason = addslashes($write_off_reason);
    
    $sql = "INSERT INTO
					INVENTORY_ITEM_WRITE_OFF_RECORD
					(ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, ApproveDate, ApprovePerson, 
					RecordType, RecordStatus, DateInput, DateModified)
			VALUES
					($ItemID, '$curr_date', $ItemLocation, $ItemAdminGroup, 1, '$write_off_reason', '$curr_date', $UserID, '$curr_date', $UserID,
					0, 1, NOW(), NOW())
			";
    
    $result['NewInvItemSingleStatusLog'] = $linventory->db_db_query($sql);
    $write_off_logID = $linventory->db_insert_id();
    
    $file = stripslashes(${"hidden_write_off_attachment"});
    $re_file = stripslashes(${"write_off_attachment"});
    
    if ($re_file == "none" || $re_file == "") {} else {
        $lf = new libfilesystem();
        if (! is_dir($path)) {
            $lf->folder_new($path);
        }
        
        $target = "$path/$file";
        $attachementname = "/$file";
        
        if ($lf->lfs_copy($re_file, $target) == 1) {
            $attachment['FileName'] = $attachementname;
            $attachment['Path'] = $re_path;
            
            $sql = "INSERT INTO 
							INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
							(ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
							($ItemID, $write_off_logID, '$re_path', '$file', NOW(), NOW())
					";
            // echo $sql."<BR><BR>";
            $linventory->db_db_query($sql);
        }
    }
}

if ($ItemType == 2) {
    $sql = "LOCK TABLES
						INVENTORY_ITEM_BULK_LOCATION AS WRITE,
						INVENTORY_ITEM_BULK_EXT AS WRITE,
						INVENTORY_ITEM_BULK_LOG AS WRITE
			";
    $linventory->db_db_query($sql);
    
    $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$ItemID."' AND GroupInCharge = '".$group_id."' AND LocationID = '".$location_id."'";
    $arr_tmp_qty1 = $linventory->returnVector($sql);
    if ($qty <= $arr_tmp_qty1[0]) {
        $tmp_check_1 = true;
    } else {
        $tmp_check_1 = false;
    }
    
    $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_EXT WHERE ItemID = '".$ItemID."'";
    $arr_tmp_qty2 = $linventory->returnVector($sql);
    if ($qty <= $arr_tmp_qty2[0]) {
        $tmp_check_2 = true;
    } else {
        $tmp_check_2 = false;
    }
    
    if ($tmp_check_1 && $tmp_check_2) {
        $sql = "UPDATE INVENTORY_ITEM_BULK_LOCATION SET Quantity = Quantity - $qty WHERE ItemID = '".$ItemID."' AND GroupInCharge = '".$group_id."' AND LocationID = '".$location_id."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE 
						INVENTORY_ITEM_BULK_LOCATION
				SET
						Quantity = Quantity - $item_write_off_qty
				WHERE
						ItemID = '".$ItemID."' AND
						GroupInCharge = '".$ItemAdminGroup."' AND
						LocationID = '".$ItemLocation."'
				";
        
        $result['UpdateInvItemBulkLocation'] = $linventory->db_db_query($sql);
        
        $sql = "UPDATE
						INVENTORY_ITEM_BULK_EXT
				SET
						Quantity = Quantity - $item_write_off_qty
				WHERE
						ItemID = '".$ItemID."'
				";
        
        $result['UpdateInvItemBulkExt'] = $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, PersonInCharge, Remark, RecordType, RecordStatus, 
						DateInput, DateModified, LocationID, GroupInCharge, StockCheckQty)
				VALUES
						($ItemID, '$curr_date', 3, $item_write_off_qty, $UserID, '', '', '', NOW(), NOW(), $ItemLocation,
						$ItemAdminGroup, '')
				";
        
        $result['UpdateInvItemBulkLog'] = $linventory->db_db_query($sql);
        
        $sql = "UNLOCK TABLES";
        $linventory->db_db_query($sql);
    }
    
    $sql = "SELECT " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = '".$targetWriteOffReason."'";
    $arr_write_off_reason = $linventory->returnVector($sql);
    $write_off_reason = $arr_write_off_reason[0];
    $write_off_reason = addslashes($write_off_reason);
    
    $sql = "INSERT INTO
					INVENTORY_ITEM_WRITE_OFF_RECORD
					(ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, ApproveDate, ApprovePerson,
					RecordType, RecordStatus, DateInput, DateModified)
			VALUES
					($ItemID, '$curr_date', $ItemLocation, $ItemAdminGroup, $item_write_off_qty, '$write_off_reason', '$curr_date', $UserID, '$curr_date', $UserID,
					0, 1, NOW(), NOW())
			";
    
    /*
     * $sql = "INSERT INTO
     * INVENTORY_ITEM_WRITE_OFF_RECORD
     * (ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReasonID, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified)
     * VALUES
     * ($ItemID, '$curr_date', $ItemLocation, $ItemAdminGroup, $item_write_off_qty, $targetWriteOffReason, $UserID, 0, 1, NOW(), NOW())
     * ";
     */
    
    $result['UpdateInvItemBulkLog'] = $linventory->db_db_query($sql);
    $write_off_logID = $linventory->db_insert_id();
    
    $file = stripslashes(${"hidden_write_off_attachment"});
    $re_file = stripslashes(${"write_off_attachment"});
    
    if ($re_file == "none" || $re_file == "") {} else {
        $lf = new libfilesystem();
        if (! is_dir($path)) {
            $lf->folder_new($path);
        }
        
        $target = "$path/$file";
        $attachementname = "/$file";
        
        if ($lf->lfs_copy($re_file, $target) == 1) {
            $attachment['FileName'] = $attachementname;
            $attachment['Path'] = $re_path;
            
            $sql = "INSERT INTO 
							INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
							(ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
					VALUES
							($ItemID, $write_off_logID, '$re_path', '$file', NOW(), NOW())
					";
            // echo $sql."<BR><BR>";
            $linventory->db_db_query($sql);
        }
    }
}

if (! in_array(false, $result)) {
    header("location: items_full_list.php?msg=2");
} else {
    header("location: items_full_list.php?msg=14");
}
intranet_closedb();
?>