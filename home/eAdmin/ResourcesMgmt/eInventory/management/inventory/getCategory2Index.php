<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory	= new libinventory();

$layer_content .= "<table border=0 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
if($item_type == 1)
	$layer_content .= "<tr class=tabletop><td colspan=6 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu2')></td></tr>";
else
	$layer_content .= "<tr class=tabletop><td colspan=6 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu3')></td></tr>";
$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$i_InventorySystem_Item_Category2Code."</td><td class=\"tabletopnolink\">".$i_InventorySystem_SubCategory_Name."</td><td class=\"tabletopnolink\">".$i_InventorySystem_Category_Name."</td></tr>";


$sql = "SELECT a.Code, ".$linventory->getInventoryNameByLang('a.').", ".$linventory->getInventoryNameByLang('b.')." FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) WHERE a.RecordStatus=1 AND b.RecordStatus=1";

$arr_result = $linventory->returnArray($sql,3);

if(sizeof($arr_result>0))
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($cat2_code, $cat2_name, $cat_name) = $arr_result[$i];
		
		$layer_content .= "<tr><td class=\"tabletext\">$cat2_code</td>";
		$layer_content .= "<td class=\"tabletext\">$cat2_name</td>";
		$layer_content .= "<td class=\"tabletext\">$cat_name</td>";
	}
}
if(sizeof($arr_result)==0)
{
	$layer_content .= "<tr><td class=\"tabletex\" colspan=3>$i_no_record_exists_msg</td></tr>";
}
			
$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\" class=\"tabletext\"><font color=\"red\">$i_InventorySystem_Import_CorrecrCategory2IndexReminder</font></td></tr>";

$layer_content .= "</table>";

$layer_content = "<br /><br />".$layer_content;

//$response = iconv("Big5","UTF-8",$layer_content);
$response = $layer_content;

echo $response;
?>