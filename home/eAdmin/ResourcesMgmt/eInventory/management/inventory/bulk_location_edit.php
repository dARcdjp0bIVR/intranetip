<?php
// using :

// ################################################
// Date: 2019-05-13 Henry
// Security fix: SQL without quote
//
// Date: 2018-02-07 Henry
// add access right checking [Case#E135442]
//
// Date: 2014-10-09 YatWoon
// Improved: add flag $special_feature['eInventory']['EditLocationBulkQty'], allow client update bulk location qty if necessary [Case#K67403]
// if the flag is on, only can edit qty, disallow move the item to other location
// Deploy: ip.2.5.5.10.1
//
// Date: 2012-07-05 YatWoon
// Improved: display "in use" and original funding
//
// Date: 2012-05-23 YatWoon
// - display qty
//
// ################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();
$llocation_ui = new liblocation_ui();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$targetRecordID = $RecordID[0];

if ($special_feature['eInventory']['EditLocationBulkQty']) {
    $disabled = "disabled";
}

/*
 * $sql = "SELECT
 * a.InvoiceNo, a.PurchaseDate, a.SupplierName, a.PurchasedPrice, a.UnitPrice, a.QtyChange, ".getNameFieldByLang2("b.")."
 * FROM
 * INVENTORY_ITEM_BULK_LOG as a
 * INNER JOIN INTRANET_USER AS b ON (a.PersonInCharge = b.UserID)
 * WHERE
 * RecordID = $targetRecordID";
 * $result = $linventory->returnArray($sql);
 * if(!empty($result))
 * list($org_invoice_no, $org_purchase_date, $org_supplier_name, $org_price, $ori_unitprice, $qty, $pic) = $result[$i];
 */

$sql = "select 
			ItemID, GroupInCharge,FundingSourceID, LocationID, Quantity
		from 
			INVENTORY_ITEM_BULK_LOCATION 
		where 
			RecordID = '".$targetRecordID."'";
$result = $linventory->returnArray($sql);
if (! empty($result))
    list ($this_ItemID, $org_GroupInCharge, $org_FundingSourceID, $this_LocationID, $this_qty) = $result[0];

$sql = "SELECT CONCAT(ItemCode,' - '," . $linventory->getInventoryNameByLang() . ") FROM INVENTORY_ITEM WHERE ItemID = '".$this_ItemID."'";
$ItemName = $linventory->returnVector($sql);
$ItemLocationStr = $linventory->returnLocationStr($this_LocationID);

// ## Funding Source
// $arr_funding_source = $linventory->returnFundingSource();
$namefield = $linventory->getInventoryItemNameByLang();
$sql = "SELECT FundingSourceID, $namefield FROM INVENTORY_FUNDING_SOURCE ";
$sql .= "where RecordStatus=1 or FundingSourceID='".$org_FundingSourceID."' order by DisplayOrder";
$arr_funding_source = $linventory->returnArray($sql, 2);
$funding_selection = getSelectByArray($arr_funding_source, "name=item_funding " . $disabled, $org_FundingSourceID, 0, 1);

if ($special_feature['eInventory']['EditLocationBulkQty']) {
    // Qty
    $this_qty_display = "<input type='text' name='location_qty' value='" . $this_qty . "' class='textboxnum'>";
} else {
    $this_qty_display = $this_qty;
}

// get my resource groups #
$group_array = $linventory->returnOwnGroup();
$group_selection = getSelectByArray($group_array, "name=targetGroup id=targetGroup " . $disabled, $org_GroupInCharge, 0, 1);

// check can right to edit the item
$group_admin_ary = explode(",", $linventory->getInventoryAdminGroup());
if (! ($linventory->IS_ADMIN_USER() || in_array($org_GroupInCharge, $group_admin_ary))) {
    header("location: category_show_items_location.php?type=2&item_id=$this_ItemID&xmsg=NoAccessRightForThisItem");
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);

$PAGE_NAVIGATION[] = array(
    $i_InventorySystem['Location'] . " " . $i_InventorySystem_Search_And2 . " " . $i_InventorySystem['Caretaker'],
    "category_show_items_location.php?type=2&item_id=$this_ItemID"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['Edit']
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>
<?=$linterface->GET_NAVIGATION2($ItemName[0])?>

<form name="form1" action="bulk_location_edit_update.php" method="post">
	<div class="form_table_content">
		<table class="form_table_v30">

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Location_Level?></td>
				<td><?=$ItemLocationStr?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem['Caretaker']?></td>
				<td><?=$group_selection?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Funding?></td>
				<td><?=$funding_selection?></td>
			</tr>

			<tr>
				<td class="field_title"><?=$i_InventorySystem_Item_Qty?></td>
				<td><?=$this_qty_display?></td>
			</tr>





		</table>

		<div class="edit_bottom_v30">
			<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($button_submit, "submit")?> 
<?=$linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='category_show_items_location.php?type=2&item_id=".$this_ItemID."'")?> 
<p class="spacer"></p>
		</div>


	</div>

	<input type='hidden' name='RecordID' value='<?=$targetRecordID?>'> <input
		type='hidden' name='Type' value='2'> <input type='hidden'
		name='ItemID' value='<?=$this_ItemID?>'>
</form>



<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>