<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linventory	= new libinventory();

$layer_content .= "<table border=0 width='100%' bordercolorlight=#FBFDEA bordercolordark=#B3B692 cellpadding=2 cellspacing=0 bgcolor=#FBFDEA>";
if($item_type == 1)
	$layer_content .= "<tr class=tabletop><td colspan=3 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu2')></td></tr>";
else
	$layer_content .= "<tr class=tabletop><td colspan=6 align=right><input type=button value=' X ' onClick=jHIDE_DIV('ToolMenu3')></td></tr>";
$layer_content .= "<tr class=tabletop><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['RoomCode']."</td><td class=\"tabletopnolink\">".$Lang['eInventory']['FieldTitle']['RoomName']."</td></tr>";


$sql = "SELECT 
			DISTINCT room.Code, CONCAT(".$linventory->getInventoryNameByLang("building.").",' > ',".$linventory->getInventoryNameByLang("floor.").",' > ',".$linventory->getInventoryNameByLang("room.").")
		FROM 
			INVENTORY_LOCATION_BUILDING as building INNER JOIN 
			INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
			INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
		WHERE
			building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1
		Order by 
			building.DisplayOrder, floor.DisplayOrder, room.DisplayOrder";
$arr_result = $linventory->returnArray($sql,2);

if(sizeof($arr_result>0))
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($room_code, $room_name) = $arr_result[$i];
		
		$layer_content .= "<tr><td class=\"tabletext\">$room_code</td>";
		$layer_content .= "<td class=\"tabletext\">$room_name</td>";
	}
}
if(sizeof($arr_result)==0)
{
	$layer_content .= "<tr><td class=\"tabletex\" colspan=3>$i_no_record_exists_msg</td></tr>";
}

$layer_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"3\" class=\"tabletext\"><font color=\"red\">".$Lang['eInventory']['FieldTitle']['Import']['CorrecrFloorIndexReminder']."</font></td></tr>";

$layer_content .= "</table>";

$layer_content = "<br /><br />".$layer_content;

//$response = iconv("Big5","UTF-8",$layer_content);
$response = $layer_content;

echo $response;
?>