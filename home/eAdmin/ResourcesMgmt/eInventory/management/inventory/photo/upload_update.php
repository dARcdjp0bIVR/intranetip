<?php
// modifying : 
/****************************** Change Log **************************************************
 * 2018-02-06 (Henry):		resize photo to 1024 x 768 [Case#U131701]
 * 2014-04-23 (YatWoon):	use "+" replace with "/" if item code includes "/" symbol
 * 2011-04-11 (Carlos): rename zip file name since file_unzip() cannot handle chinese name.
 ******************************* Change Log *************************************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/liblocation.php");
include_once($PATH_WRT_ROOT."includes/liblocation_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimage.php");

intranet_auth();
intranet_opendb();

$ldb = new libdb();
$lfs = new libfilesystem();
$linterface = new interface_html();

# upload file handling start
$filename = $_FILES["UploadFile"]["name"];
$tmpfilepath = $_FILES["UploadFile"]["tmp_name"];
$fileext = $lfs->file_ext($filename);
if(strtolower($fileext)==".zip")
{
	// Rename zip file since file_unzip() cannot handle chinese file name
	$filename = md5(time()).$fileext;
}

$tmp_photo_path = $intranet_root."/file/inventory";
if (!is_dir($tmp_photo_path))
{
	$lfs->folder_new($tmp_photo_path);
}
$tmp_photo_path .= "/tmp_photo";
if (!is_dir($tmp_photo_path))
{
	$lfs->folder_new($tmp_photo_path);
}

$base_img_path = "/file/inventory";
$tmpfolder = $tmp_photo_path;
$tmpfile = $tmpfolder."/".$filename;

//debug_pr($tmpfolder);

if(strtolower($fileext)==".zip")
{
	# copy to tmp folder first
	$copy_success = $lfs->lfs_copy($tmpfilepath, $tmpfile);
	
	if($lfs->file_unzip($tmpfile,$tmpfolder))
	{
		$lfs->lfs_remove($tmpfile);
		$tmpfilelist = $lfs->return_folderlist($tmpfolder);
		//debug_pr($tmpfilelist);
	}
}
else if(strtolower($fileext)==".jpg" || strtolower($fileext)==".gif" || strtolower($fileext)==".png")
{
	# copy to tmp folder first
	$lfs->lfs_copy($tmpfilepath, $tmpfile);
	$tmpfilelist = $lfs->return_folderlist($tmpfolder);
}

$target_folder = $intranet_root."/file/photo";
if (!is_dir($target_folder))
{
	$lfs->folder_new($target_folder);
}
$target_folder .= "/inventory";
if (!is_dir($target_folder))
{
	$lfs->folder_new($target_folder);
}

$re_path = "/file/photo/inventory/";

if(count($tmpfilelist)>0)
{	
	$sql = "SELECT 
				i.ItemID,
				i.ItemCode,
				i.NameChi,
				i.NameEng,
				i.CategoryID,
				i.Category2ID,
				IF(p.PartID IS NULL,'',p.PartID) as PartID,
				p.PhotoPath,
				p.PhotoName 
			FROM INVENTORY_ITEM as i 
			LEFT JOIN INVENTORY_PHOTO_PART as p ON p.PartID = i.PhotoLink AND p.PhotoName IS NOT NULL AND p.PhotoName <> '' 
			WHERE 
				i.RecordStatus = '1' 
			ORDER BY 
				i.ItemCode ";
	$items = $ldb->returnArray($sql);
	$items_assoc = array();
	$numOfItems = sizeof($items);
	for($i=0;$i<$numOfItems;$i++){
		$item_code = strtolower($items[$i]['ItemCode']);
		$items_assoc[$item_code] = $items[$i];
	}
	
	$image_obj = new SimpleImage();
	
	$NoOfUploadSuccess = 0;
	for($i=0; $i< count($tmpfilelist); $i++)
	{
		$UploadSuccess = false;
		
		$image_obj->load($tmpfilelist[$i]);
		$image_obj->resizeToMax(1024, 768);
		$image_obj->save($tmpfilelist[$i]);
		
		$ext = $lfs->file_ext($tmpfilelist[$i]); // photo extention
		$extLC = strtolower($ext); // photo extention in lower case
		$basename = basename($tmpfilelist[$i]); // intact photo file name
		$prefixname = substr($basename, 0, strrpos($basename,"."));// photo name without extention
		$prefixnameLC = strtolower($prefixname);
		
		if($special_feature['eInventoryItemFilenameHandling'] || $sys_custom['eInventoryCustForSKH'])
		{
			# replace + to /
			$prefixnameLC_checking = str_replace("+","/",$prefixnameLC);
		}
		else
		{
			$prefixnameLC_checking = $prefixnameLC;
		}
		
		unset($cur_item);
		if($extLC == ".jpg" || $extLC == ".gif" || $extLC == ".png")
		{	
			//if(isset($items_assoc[$prefixnameLC]))
			if(isset($items_assoc[$prefixnameLC_checking]))
			{
				//$cur_item = $items_assoc[$prefixnameLC];
				$cur_item = $items_assoc[$prefixnameLC_checking];
				$target_filename = $cur_item['ItemCode'].$ext;
				
				if($special_feature['eInventoryItemFilenameHandling'] || $sys_custom['eInventoryCustForSKH'])
				{
					# replace / to _
					$target_filename = str_replace("/","_",$prefixnameLC);	
				}
				
				$target_path = $target_folder."/".$target_filename;
				$targetCategory = $cur_item['CategoryID'];
				$targetCategory2 = $cur_item['Category2ID'];
				$item_id = $cur_item['ItemID'];
				
				if($cur_item['PartID']!='')// have photo
				{
					$part_id = $cur_item['PartID'];
					$copy_success = $lfs->lfs_copy($tmpfilelist[$i], $target_path);
					
					if($copy_success)
					{
						if(strcasecmp($target_filename,$cur_item['PhotoName'])!=0){
							$file_to_remove = $intranet_root."/".$cur_item['PhotoPath']."/".$cur_item['PhotoName'];
							$lfs->lfs_remove($file_to_remove);
						}
						$sql = "UPDATE 
									INVENTORY_PHOTO_PART 
								SET
									PhotoPath = '$re_path',
									PhotoName = '".$ldb->Get_Safe_Sql_Query($target_filename)."',
									DateModified = NOW() 
								WHERE
									PartID = $part_id 
								";
						$update_success = $ldb->db_db_query($sql);
						if($update_success){
							$UploadSuccess = true;
							$NoOfUploadSuccess+=1;
						}
					}
				}else // no photo
				{
					$copy_success = $lfs->lfs_copy($tmpfilelist[$i], $target_path);
					if($copy_success)
					{
						$sql = "INSERT INTO INVENTORY_PHOTO_PART 
								(CategoryID, Category2ID, ItemID, PhotoPath, PhotoName, DateInput, DateModified)
								VALUES
								($targetCategory, $targetCategory2, $item_id, '$re_path', '".$ldb->Get_Safe_Sql_Query($target_filename)."', NOW(), NOW())";
						$insert_success = $ldb->db_db_query($sql);
						$tmp_photo_link = $ldb->db_insert_id();
						
						$sql = "UPDATE 
									INVENTORY_ITEM
								SET
									PhotoLink = $tmp_photo_link
								WHERE
									ItemID = $item_id
								";
						$update_success = $ldb->db_db_query($sql);
						if($insert_success && $update_success){
							$UploadSuccess = true;
							$NoOfUploadSuccess+=1;
						}
					}
				}
			}
		} 
		
		$red_css = !$UploadSuccess?" red ":"";

		$rowcss = " class='".($key++%2==0? "tablebluerow2":"tablebluerow1")." $red_css' ";
		
		# gen table row
		$tr.= '<tr '.$rowcss.'>'."\n";
			$tr.= '<td>'.$basename.'</td>'."\n";
			$tr.= '<td>'.($cur_item?$cur_item["NameEng"]:"-").'</td>'."\n";
			$tr.= '<td>'.($cur_item?$cur_item["NameChi"]:"-").'</td>'."\n";
			$tr.= '<td>'.($cur_item?$cur_item["ItemCode"]:"-").'</td>'."\n";
		$tr.= '</tr>'."\n";
	}
	
	$display.= '<table cellpadding=5 cellspacing=0 border=0 width="100%">'."\n";
		$display.= '<tr class="tablebluetop tabletopnolink">'."\n";
			$display.= '<td>'.$Lang['eInventory']['File'].'</td>'."\n";
			$display.= '<td>'.$i_InventorySystem_Item_EnglishName.'</td>'."\n";
			$display.= '<td>'.$i_InventorySystem_Item_ChineseName.'</td>'."\n";
			$display.= '<td>'.$i_InventorySystem_Item_Code.'</td>'."\n";
		$display.= '</tr>'."\n";		
		$display.= $tr;
	$display.= '</table>'."\n";
	
}

$UploadStat.= '<table cellpadding="5" cellspacing="0" border="0" width="100%">'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['eInventory']['PhotoAttempt'].'</td><td class="tabletext">'.count($tmpfilelist).'</td></tr>'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['eInventory']['UploadSuccess'].'</td><td class="tabletext">'.$NoOfUploadSuccess.'</td></tr>'."\n";
	$UploadStat.= '<tr><td class="formfieldtitle" width="30%">'.$Lang['eInventory']['UploadFail'].'</td><td class="tabletext">'.(count($tmpfilelist)-$NoOfUploadSuccess).'</td></tr>'."\n";
$UploadStat.= '</table>'."\n";

$lfs->folder_remove_recursive($tmpfolder);
# upload file handling end

# gen button 
$uploadBtn = $linterface->GET_ACTION_BTN($Lang['eInventory']['UploadMorePhoto'], "button", "window.location.href='./upload.php'", "uploadBtn");
$backBtn = $linterface->GET_ACTION_BTN($button_back, "button", "window.location.href='../items_full_list.php'", "backBtn");


$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

$STEPS_OBJ[] = array($Lang['eInventory']['UploadPhoto']['StepArr'][0], 0);
$STEPS_OBJ[] = array($Lang['eInventory']['UploadPhoto']['StepArr'][1], 1);
$StepObj = $linterface->GET_STEPS($STEPS_OBJ);

# navigation
$PAGE_NAVIGATION[] = array($i_InventorySystem['FullList'], "../items_full_list.php");
$PAGE_NAVIGATION[] = array($Lang['eInventory']['UploadImages'], "");
$PageNavigation = $linterface->GET_NAVIGATION($PAGE_NAVIGATION);

$TAGS_OBJ[] = array($button_upload);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

?>
<br>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="70%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<?=$StepObj?>
					</td>
				</tr>
			</table><br>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<?=$UploadStat?>
					</td>
					<td align="right"></td>
				</tr>
			</table>
			<br>
			<?=$display?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline">
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$uploadBtn?>
			<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="1" height="1">
			<?= $backBtn ?>
		</td>
	</tr>
</table>
<br><br>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>