<?php

// Using:

/**
 * **************************** Change Log [Start] *******************************
 * 2019-05-13 (Henry): Security fix: SQL without quote
 * 2018-05-02 (Henry) disable submit button if clicked [Case#T138878]
 * 2018-02-07 (Henry): add access right checking [Case#E135442]
 * 2016-02-01 (Henry): PHP 5.4 fix: change split to explode
 * 2015-03-16 Cameron: disable submit button after click submit
 * 2013-06-06 Rita cater leader right
 * 2012-10-08 Rita amend js add Single item checking
 * ****************************** Change Log [End] ********************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");

intranet_auth();
intranet_opendb();

$no_of_file = 1;
$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

// Page Title
$TAGS_OBJ[] = array(
    $Lang['eInventory']['FieldTitle']['BulkItemRequestWriteOff']
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

// Obtain ItemID
$ItemID = $_POST['ItemID'];

if (! empty($ItemID)) {
    $ItemID = (array) $ItemID;
    $numOfItemId = count($ItemID);
    $counter = 0;
    $numOfItemWithResourceSetting = 0;
    
    $item_id_Arr = array();
    $itemWithoutRightArr = array();
    
    // Loop each Item Record
    for ($i = 0; $i < $numOfItemId; $i ++) {
        $eachItemID = $ItemID[$i];
        
        // Define Item Type
        if (substr_count($eachItemID, ":") == 1) {
            $item_type = ITEM_TYPE_SINGLE;
            $singleItemType[] = $item_type;
            
            // Obtain Single Item ID
            $item_id_temp = explode(":", $eachItemID);
            $item_id = trim($item_id_temp[0]);
            $targetLocation = $item_id_temp[1];
            
            if ($linventory->IS_ADMIN_USER($UserID)) {
                $item_id_Arr[] = $item_id;
            }            // If User Level is Leader, check if he/she has right to edit this item in this group
            elseif ($linventory->getAccessLevel($UserID) == 2) {
                $groupInCharge = $linventory->GET_ITEM_GROUP_IN_CHARGE($item_id, $targetLocation);
                $groupUserTypeArr = $linventory->getGroupUserType($groupInCharge);
                $groupUserType = $groupUserTypeArr[0]['RecordType'];
                
                if ($groupUserType == 1) {
                    $item_id_Arr[] = $item_id;
                } else {
                    $itemWithoutRightArr[] = $item_id;
                }
            }
        } else {
            $item_type == ITEM_TYPE_BULK;
        }
    }
    
    // ######## Message For Leader Without Right #########
    $noRightWriteOffItemMsg = '';
    
    $numOfItemWithoutRightArr = count($itemWithoutRightArr);
    if ($numOfItemWithoutRightArr > 0) {
        if ($numOfItemWithoutRightArr == (count($item_id_Arr) + count($itemWithoutRightArr))) {
            $noRightWriteOffItemMsg = $Lang['eInventory']['WarningArr']['NoRightWriteOffSelectedItems'];
        } else {
            $noRightWriteOffItemMsg = $Lang['eInventory']['WarningArr']['NoRightWriteOffSomeItems'];
        }
    }
    // ######## Message For Leader Without Right #########
    
    $numOfTotalSingleItemID = sizeof($item_id_Arr);
    for ($i = 0; $i < $numOfTotalSingleItemID; $i ++) {
        $item_id = $item_id_Arr[$i];
        if ($item_type == ITEM_TYPE_SINGLE) {
            
            $sql = "SELECT 
							" . $linventory->getInventoryItemNameByLang("a.") . ",
							c.LocationID,
							CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("LocFloor.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
							d.AdminGroupID,
							" . $linventory->getInventoryNameByLang("d.") . "					
					FROM 
							INVENTORY_ITEM AS a INNER JOIN
							INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
							INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
							INVENTORY_LOCATION_LEVEL AS LocFloor ON (c.LocationLevelID = LocFloor.LocationLevelID) INNER JOIN
							INVENTORY_LOCATION_BUILDING AS LocBuilding ON (LocFloor.BuildingID = LocBuilding.BuildingID) INNER JOIN
							INVENTORY_ADMIN_GROUP AS d ON (b.GroupInCharge = d.AdminGroupID)
					WHERE
							a.ItemID = $item_id
					";
            $arr_result = $linventory->returnArray($sql, 5);
            
            if (sizeof($arr_result) > 0) {
                list ($item_name, $item_location_id, $item_location_name, $item_admin_group_id, $item_admin_group_name) = $arr_result[0];
                $targetLocation = $item_location_id;
                
                $sql = "SELECT ResourceID FROM INTRANET_RESOURCE_INVENTORY_ITEM_LIST WHERE ItemID = '".$item_id."'";
                $arr_is_resource_item = $linventory->returnVector($sql);
                
                if (sizeof($arr_is_resource_item) > 0) {
                    $numOfItemWithResourceSetting ++;
                    // Resource item setting selection (for each item):
                    $arr_resource_item_status = array(
                        array(
                            1,
                            "$i_InventorySystem_RequestWriteOff_ResourceItemStatus1"
                        ),
                        array(
                            2,
                            "$i_InventorySystem_RequestWriteOff_ResourceItemStatus2"
                        ),
                        array(
                            3,
                            "$i_InventorySystem_RequestWriteOff_ResourceItemStatus3"
                        )
                    );
                    $resoruce_item_status_selection = getSelectByArray($arr_resource_item_status, " name=\"resource_item_status_{$item_id}\" class=\"resource_item_status_each\"");
                    $is_resource_item = "<tr><td nowrap class='field_title'>$i_InventorySystem_RequestWriteOff_ResourceItemStatusSelection</td><td>$resoruce_item_status_selection</td></tr>";
                }
            }
            
            // Check is there someone else request write-off already
            $sql = "select 
						RecordID, RequestDate, RequestPerson, " . getNameFieldByLang2("b.") . ", WriteOffQty, WriteOffReason
					from 
						INVENTORY_ITEM_WRITE_OFF_RECORD as a
						left join INTRANET_USER as b on b.UserID=a.RequestPerson
					where 
						ItemID=$item_id and LocationID=$targetLocation and a.RecordStatus=0 ";
            $result = $linventory->returnArray($sql);
            
            $numOfRequestSummitted = '';
            $numOfRequestSummitted = count($result);
            
            if ($numOfRequestSummitted > 0 && ! $xmsg) {
                // Total no. of item already submmited
                $totalItemRequestSummitted += $numOfRequestSummitted;
            } else {
                $counter ++;
                $singleAvaliableItem[] = $item_id;
                
                // Obtain Navigation Information
                $sql = "SELECT 
						" . $linventory->getInventoryItemNameByLang("a.") . ",
						" . $linventory->getInventoryItemNameByLang("b.") . ",
						" . $linventory->getInventoryItemNameByLang("c.") . "
				FROM 
						INVENTORY_ITEM AS a INNER JOIN
						INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
						INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
				WHERE
						a.ItemID = $item_id";
                
                $result = $linventory->returnArray($sql, 3);
                
                // Set Navigation
                $PAGE_NAVIGATION = '';
                $PAGE_NAVIGATION = $counter . '. ' . $result[0][1] . " > " . $result[0][2] . " > " . $result[0][0] . " > " . $Lang['eInventory']['FieldTitle']['ItemRequestWriteOff'];
                
                // Get User Inventory Admin Group
                $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."'";
                $arr_result = $linventory->returnVector($sql);
                if (sizeof($arr_result) > 0) {
                    $inventory_admin_group = implode(",", $arr_result);
                }
                
                // Get Default Write Off Reason
                $sql = "SELECT ReasonTypeID, " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE";
                $arr_write_off_reason = $linventory->returnArray($sql, 2);
                $reason_selection = getSelectByArray($arr_write_off_reason, " name=\"targetWriteOffReason_{$item_id}\" class=\"targetWriteOffReason_each\" id=\"targetWriteOffReason_{$item_id}\" ");
                
                // Set write-off reason display
                $display_request_reason = $this_reason ? $this_reason : $reason_selection;
                
                // ## Display Items without submmited request ###
                // Display Navigation
                $displayTable .= $linterface->GET_NAVIGATION2($PAGE_NAVIGATION);
                
                // Ignore item checkbox
                $displayTable .= "<div class='form_table_content'  id='form_table_content_{$item_id}'>";
                // $displayTable .="<div style='float:left;'><input type='checkbox' id='chk_{$item_id}' onclick='js_ignore_item($item_id);' name='ignoredItem_{$item_id}' value='1' />".$Lang['eInventory']['IgnoreThisItem']."<br /></div>";
                $displayTable .= "<div style='float:left;'>";
                $displayTable .= $linterface->Get_Checkbox("chk_{$item_id}", "ignoredItem_{$item_id}", 1, 0, "", $Lang['eInventory']['IgnoreThisItem'], "js_ignore_item($item_id);", $Disabled = '');
                $displayTable .= "<br /></div>";
                $displayTable .= "<br />";
                
                // # Item Record Information ##
                $displayTable .= "<table class='form_table_v30' id='tableHide_{$item_id}'>";
                
                // Location
                $displayTable .= "<tr valign='top'>";
                $displayTable .= "<td nowrap class='field_title'>" . $i_InventorySystem_Location_Level . "</td>";
                $displayTable .= "<td>" . $item_location_name . "</td>";
                $displayTable .= "</tr>";
                
                // Resource Management Group Name
                $displayTable .= "<tr valign='top'>";
                $displayTable .= "<td nowrap class='field_title'>" . $i_InventorySystem_Group_Name . "</td>";
                $displayTable .= "<td>" . $item_admin_group_name . "</td>";
                $displayTable .= "</tr>";
                
                // Write-off Reason
                $displayTable .= "<tr valign='top'>";
                $displayTable .= "<td nowrap class='field_title'>" . $i_InventorySystem_WriteOffReason_Name . "</td>";
                $displayTable .= "<td>" . $display_request_reason . "</td>";
                $displayTable .= "</tr>";
                
                // Resource item setting
                $displayTable .= $is_resource_item;
                
                // Attachment
                $displayTable .= "<tr valign='top'>";
                $displayTable .= "<td nowrap class='field_title'>" . $i_InventorySystem_Write_Off_Attachment . "</td>";
                $displayTable .= "<td>";
                $displayTable .= "<table border='0' id='upload_file_list_{$item_id}' cellpadding='0' cellspacing='0' class='inside_form_table'>";
                $displayTable .= "<script language='javascript'>
					
					for(i=0;i<no_of_upload_file;i++)
					{			
						document.writeln('<tr><td><input class=\"file\" id=\"eachSubFile_{$item_id}_'+i+'\" type=\"file\" id=\"eachFile_'+i+'\" name=\"item_write_off_attachment_{$item_id}_'+i+'\" size=\"40\">');
						document.writeln('<input type=\"hidden\" id=\"eachSubFileHiddern_{$item_id}_'+i+'\" name=\"hidden_item_write_off_attachment_{$item_id}_'+i+'\"></td></tr>');								
					}			
					</script>";
                
                $displayTable .= "</table>";
                $displayTable .= "<input name='' type='button' value=' + ' class='eachPlusButton' onClick='add_Eachfield($item_id)'>";
                $displayTable .= "<input type='hidden' id='no_of_file_upload_{$item_id}' name='no_of_file_upload_{$item_id}' value=''>";
                
                $displayTable .= "</td>";
                $displayTable .= "</tr>";
                $displayTable .= "</table>";
                
                $displayTable .= "<br /><br />";
                
                // Hidden fields
                $displayTable .= "<input type='hidden' name='flag_{$item_id}' value='1'>";
                $displayTable .= "<input type='hidden' name='item_id[]' value='" . $item_id . "'>";
                $displayTable .= "<input type='hidden' name='ItemType_{$item_id}' value='" . $item_type . "'>";
                $displayTable .= "<input type='hidden' name='ItemAdminGroup_{$item_id}' value='" . $item_admin_group_id . "'>";
                $displayTable .= "<input type='hidden' name='WriteOffRecordID_{$item_id}' value='" . $this_recordid . "'>";
                $displayTable .= "<input type='hidden' name='targetLocation_{$item_id}' value='" . $item_location_id . "'>";
                
                $displayTable .= "</div>";
            } // end of if($numOfRequestSummitted>0 && !$xmsg)
        } // end of if($item_type == ITEM_TYPE_SINGLE)
    } // end of for($i=0;$i<$numOfItemId;$i++)
} // end of if(!empty($ItemID))

?>
<script language="javascript">
<?php if($item_type == ITEM_TYPE_SINGLE){ ?>
	var no_of_upload_file = <?echo $no_file ==""?1:$no_file;?>;
	var itemIDArr = ["<?php echo $singleAvaliableItem ==""? "": join("\", \"", $singleAvaliableItem); ?>"]; 
		
	for(i=0;i<itemIDArr.length;i++){
		itemID = itemIDArr[i];
		var no_of_upload_file_itemID = <?echo $no_file ==""?1:$no_file;?>;
	}
			
	function js_apply_all()
	{
		// Apply to all write-off reasons 
		var selectedVal = $('#targetWriteOffReason :selected').val();
		$('.targetWriteOffReason_each').each( function(){
			$('.targetWriteOffReason_each').val(selectedVal);
		});
		
		<?php if($numOfItemWithResourceSetting >0){ ?>
		// Apply to all resource setting
		var resourceSelectedVal = $('.resource_item_status :selected').val();
		$('.resource_item_status_each').each( function(){
			$('.resource_item_status_each').val(resourceSelectedVal);
		});	
		<? } ?>
	}
	
	function add_Eachfield(itemID)
	{
		var tmpObj = "upload_file_list_" + itemID;
		var table = document.getElementById(tmpObj); 	
		var tmp_val = $('#no_of_file_upload_'+ itemID).val();
		
		if(tmp_val == '')
		{
			tmp_val = 1;
			no_of_upload_file_itemID = 1;
		}
			
		var row = table.insertRow(tmp_val);	
		var cell = row.insertCell(0);
		
		x= '<input class="file" type="file" id="eachSubFile_'+itemID+'_'+tmp_val+'" name="item_write_off_attachment_'+itemID+'_'+tmp_val+'" size="40">';
		x+='<input type="hidden" name="hidden_item_write_off_attachment_'+itemID+'_'+tmp_val+'">';
			
		cell.innerHTML = x;
		no_of_upload_file_itemID++;
			
		$('#no_of_file_upload_'+ itemID).val(no_of_upload_file_itemID);		
			
	}
	
	function checkForm()
	{
		document.getElementById('submit_btn').disabled = true;
		obj = document.form1;
		var itemIDArr = ["<?php echo $singleAvaliableItem == ""? "":join("\", \"", $singleAvaliableItem); ?>"]; 
		var error = 0;
		var totalIgnoredItems = 0;
		var totalItems = 0;
		
		// Check if items have write-off reason
		for(i=0;i<itemIDArr.length;i++){
			var itemID = itemIDArr[i];
			totalItems ++;
	
		 	if(!$('#chk_'+itemID).is(':checked')){
				if(!$('#targetWriteOffReason_'+itemID).val())
				{	
				 error ++;
				}
			}
			else{
				totalIgnoredItems ++;
			}
		}
		
		if(error>0)
		{	
			alert("<?=$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning;?>");
			document.getElementById('submit_btn').disabled = false;
			return false;
		}
		else
		{
			// Check if all items are ignored
			if(totalIgnoredItems == totalItems){
				alert("<?=$Lang['eInventory']['WarningArr']['CannotIgnoreAllItems'];?>");
				document.getElementById('submit_btn').disabled = false;
				return false;
			}
			else{
				document.getElementById('submit_btn').disabled  = true; 
				document.getElementById('submit_btn').className  = "formbutton_disable print_hide"; 
				Big5FileUploadHandler();
				obj.action = "bulk_items_request_write_off_update.php";
				return true;
			}
		}
	}
	
	function Big5FileUploadHandler() 
	{
		
		var itemIDArr = ["<?php echo join("\", \"", $singleAvaliableItem); ?>"]; 
		
		for(i=0;i<itemIDArr.length;i++){
		
			var itemID = itemIDArr[i];
			
			var file_value = eval('document.form1.no_of_file_upload_'+itemID);
			if(file_value != null)
			{
				upload_file_value = file_value.value;
				if(upload_file_value == '') 
					upload_file_value = 0;
			}
			else
			{
				upload_file_value = 0;
			}
			
			for(j=0;j<=no_of_upload_file;j++)
			{	
				objWriteOffFile = eval('document.form1.item_write_off_attachment_'+itemID+'_'+j);
				objHiddenWriteOffFile = eval('document.form1.hidden_item_write_off_attachment_'+itemID+'_'+j);
				if(objWriteOffFile!=null && objWriteOffFile.value!='' && objHiddenWriteOffFile!=null)
				{
					var Ary = objWriteOffFile.value.split('\\');
					objHiddenWriteOffFile.value = Ary[Ary.length-1];
				}
			}
		}
		return true;
	}
	
	function js_ignore_item(itemID){
		if($('#chk_'+itemID).is(':checked') ){
			$('#tableHide_'+itemID).hide();
		}
		else{
			$('#tableHide_'+itemID).show();
		}	
	}

<? } ?>

</script>

<form name='form1' enctype='multipart/form-data'
	action='bulk_items_request_write_off_update.php' method='post'
	onSubmit='return checkForm();'>
	<?if (count($singleItemType)==0){?>

		<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Note'];?></legend>
			- <?=$Lang['eInventory']['WarningArr']['AtLeastChooseOneWriteOffSingleItems'];?>
		</fieldset>

	<div class="edit_bottom_v30">
			<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='items_full_list.php'")?>
		</div>

	<?}else{?>
		<? if($totalItemRequestSummitted>0 && !$xmsg) {?> 
			<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Note']?></legend>
				- <?=$totalItemRequestSummitted. ' ' .$Lang['eInventory']['NoOfSingleItemsWritOffWaitingForApproval']?>
				<?php if($noRightWriteOffItemMsg){echo '<br />- ' . $noRightWriteOffItemMsg;}?>
			</fieldset>
		<? } elseif($noRightWriteOffItemMsg) {?>		
			<fieldset class="instruction_warning_box_v30">
		<legend><?=$Lang['General']['Note']?></legend>			
				 <?php echo '<br />- ' . $noRightWriteOffItemMsg;?>
			</fieldset>		 
		<?}?>
		
			<? if($totalItemRequestSummitted == sizeof($item_id_Arr)) {?>
			<? }else{ ?>	  
			<table class="form_table_v30">
		<tr>
			<td>
				<table align="center" width="100%" border="0" cellpadding="5"
					cellspacing="0" class="tablerow2">
					<tr>
						<td colspan="2" valign="top" nowrap="nowrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="95%"><?=$linterface->GET_NAVIGATION2($Lang['eInventory']['GlobalSettingsToAllSingleItems']);?></td>
									<td align="right"><?= $linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "js_apply_all();");?></td>
								</tr>
							</table>
						</td>
					</tr>
			
						<?php
        // Resource item setting selection (for global setting):
        if ($numOfItemWithResourceSetting > 0) {
            $global_resoruce_item_status_selection = getSelectByArray($arr_resource_item_status, " name=\"resource_item_status\" class=\"resource_item_status\"");
            $global_is_resource_item = "<tr><td nowrap class='field_title'>$i_InventorySystem_RequestWriteOff_ResourceItemStatusSelection</td><td>$global_resoruce_item_status_selection</td></tr>";
        }
        
        // Write-off reason setting selection (for global setting):
        $golbal_reason_selection = getSelectByArray($arr_write_off_reason, " name=\"targetWriteOffReason\" id=\"targetWriteOffReason\" ");
        $golbal_display_request_reason = $this_reason ? $this_reason : $golbal_reason_selection;
        ?>
			
						<tr valign='top'>
						<td nowrap class='field_title'><?=$i_InventorySystem_WriteOffReason_Name?></td>
						<td><?=$golbal_display_request_reason?></td>
					</tr>
						
						<?=$global_is_resource_item?>
						
						<? if($this_recordid) {?>
							<tr valign='top'>
						<td nowrap class='field_title'><?=$i_InventorySystem_RequestPerson?></td>
						<td><?=$this_requestby?></td>
					</tr>

					<tr valign='top'>
						<td nowrap class='field_title'><?=$i_InventorySystem_Write_Off_RequestTime?></td>
						<td><?=$this_requestdate?></td>
					</tr> 
						<? } ?>
						</table>
			</td>
		
		
		<tr>
	
	</table>
			<? } ?>
			
			<br /> <br />

			<?=$displayTable?>

		<div class="edit_bottom_v30">
		<p class="spacer"></p>
			<? if($totalItemRequestSummitted == sizeof($item_id_Arr)) {?>
			<? }else{ ?>	
				<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "", "submit_btn")?> 
				<?=$linterface->GET_ACTION_BTN($button_reset, "reset")?> 
			<? }?>
			<? if($UserID==$this_requestbyID) {?>
				<?=$linterface->GET_ACTION_BTN($Lang['eInventory']['CancelRequest'], "button","cancel_request();","","","","formbutton_alert")?> 
			<? } ?>
			<?=$linterface->GET_ACTION_BTN($button_back, "button", "window.location='items_full_list.php'")?>
			<p class="spacer"></p>
	</div>
		
	<? }?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>