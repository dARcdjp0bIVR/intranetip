<?php

# using: 

#################################################
#
#	Date:	2015-04-16	Cameron
#			can support multiple relocate for bulk item
#
# 	Date:	2012-05-25	YatWoon
#			cater with group and funding
# 
#	Date:	2011-05-30	YatWoon
#			update "Remark" string and support multiple lang (use str_replace function)
#
#################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();

# Generate Page Tag #
$TAGS_OBJ[] = array($i_InventorySystem['FullList'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/items_full_list.php", 1);
$TAGS_OBJ[] = array($i_InventorySystem['Category'], $PATH_WRT_ROOT."home/admin/inventory/stocklist/category.php", 0);
# End #

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$curr_date = date("Y-m-d");


if($item_type == ITEM_TYPE_SINGLE)
{
	$sql = "UPDATE 
				INVENTORY_ITEM_SINGLE_EXT 
			SET
				LocationID = '$targetSubLocation'
			WHERE
				ItemID = '$item_id'
			";
	$result = $linventory->db_db_query($sql);
	
	/*
	### get original location info ###
	$sql = "SELECT 
					CONCAT(".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("a.").")
			FROM
					INVENTORY_LOCATION AS a INNER JOIN
					INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID)
			WHERE 
					a.LocationID = $origianl_location_id";
	$arr_orignial_location = $linventory->returnVector($sql);
	
	### get New location info ###
	$sql = "SELECT 
					CONCAT(".$linventory->getInventoryNameByLang("b.").",' > ',".$linventory->getInventoryNameByLang("a.").")
			FROM
					INVENTORY_LOCATION AS a INNER JOIN
					INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID)
			WHERE 
					a.LocationID = $targetSubLocation";
	$arr_new_location = $linventory->returnVector($sql);
	
	$original_location = $arr_orignial_location[0];
	$new_location = $arr_new_location[0];
	*/
	
// 	$remark = "$i_InventorySystem_From: $original_location<BR>";
// 	$remark .= "$i_InventorySystem_To: $new_location";
	$remark = "[CHANGE_LOCATION]".$origianl_location_id .",".$targetSubLocation;
	$remark = addslashes($remark);
	
	$sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, DateInput, DateModified)
			VALUES
					('$item_id', '$curr_date', '".ITEM_ACTION_CHANGELOCATION."', '$UserID', '$remark', NOW(), NOW())
			";
	$result = $linventory->db_db_query($sql);
	
}
/*
if($item_type == ITEM_TYPE_BULK)
{
	$bulk_new_qty = intval($bulk_new_qty);
	$qty_change = $bulk_new_qty;
	
	### retrieve original location's admin group and funding src
	$sql = "select GroupInCharge, FundingSourceID from INVENTORY_ITEM_BULK_LOCATION where ItemID=$item_id and LocationID = $targetOldLocation";
	$result = $linventory->returnArray($sql);
	$targetOldGroup = $result[0]['GroupInCharge'];
	$OldFundingSource = $result[0]['FundingSourceID'];
	
	### update original location qty
	$sql = "UPDATE 
					INVENTORY_ITEM_BULK_LOCATION 
			SET
					Quantity = Quantity - $qty_change
			WHERE 
					ItemID = $item_id AND
					LocationID = $targetOldLocation AND
					GroupInCharge = $targetOldGroup
			";
	$linventory->db_db_query($sql);
// 	debug_pr("### update original location qty <br>" . $sql);
	
	### Get the destination admin group for the bulk item ###
	$sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetNewSubLocation";
	$arr_destination_admin_group = $linventory->returnVector($sql,1);
	if(sizeof($arr_destination_admin_group)>0)
	{
		$destination_admin_group_id = $arr_destination_admin_group[0];
	}
	$destination_admin_group_id = $destination_admin_group_id ? $destination_admin_group_id : $targetOldGroup; 

	### Get the destination Funding Source ID for the bulk item ###
	$sql = "SELECT FundingSource FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND LocationID = $targetNewSubLocation AND GroupInCharge = $destination_admin_group_id AND Action IN (".ITEM_ACTION_PURCHASE.",".ITEM_ACTION_CHANGELOCATION.")  and FundingSource<>'' ORDER BY DateInput DESC , RecordID DESC LIMIT 0,1";
	$arr_destination_funding_source = $linventory->returnVector($sql);
	if(sizeof($arr_destination_funding_source)>0)
	{
		$destination_funding_source = $arr_destination_funding_source[0];
	}
	$destination_funding_source = $destination_funding_source ? $destination_funding_source : $OldFundingSource; 
	
	### insert / update new location qty
	## check new location already has item or not
	$sql = "select RecordID from INVENTORY_ITEM_BULK_LOCATION where ItemID = $item_id AND LocationID = $targetNewSubLocation";
	$result = $linventory->returnVector($sql);
	if(empty($result))
	{
		$sql = "INSERT INTO
						INVENTORY_ITEM_BULK_LOCATION
						(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
				VALUES
						($item_id, $destination_admin_group_id, $targetNewSubLocation, $destination_funding_source, $qty_change)
				";
	}
	else
	{
		$sql = "UPDATE 
					INVENTORY_ITEM_BULK_LOCATION 
			SET
					Quantity = Quantity + $qty_change 
			WHERE 
					ItemID = $item_id AND
					LocationID = $targetNewSubLocation AND
					GroupInCharge = $destination_admin_group_id
			";
	}
	$linventory->db_db_query($sql) or die(mysql_error());
// 	debug_pr("insert / update new location qty <br>" . $sql);

	### Original Location - change log
	$sql = "INSERT INTO 
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyChange, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
			VALUES
					($item_id, '$curr_date',".ITEM_ACTION_CHANGELOCATION.",-$qty_change,$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
			";
	$linventory->db_db_query($sql) or die(mysql_error());
			
	### New Location - change log
	$sql = "INSERT INTO 
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyChange, FundingSource, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
			VALUES
					($item_id, '$curr_date',".ITEM_ACTION_CHANGELOCATION.",$qty_change,'$destination_funding_source',$targetNewSubLocation,$targetOldGroup,$UserID,NOW(),NOW())
			";
	$linventory->db_db_query($sql) or die(mysql_error());
	
	##################################################################
	### Update Normal, Damage, Repair Qty in the original location ###
	##################################################################
	$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup";
	$arr_tmp_result = $linventory->returnVector($sql);
	$original_location_qty = $arr_tmp_result[0];
// 	debug_pr("original_location_qty: ".$original_location_qty);
	
	$sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup AND Action IN (".ITEM_ACTION_UPDATESTATUS.",".ITEM_ACTION_PURCHASE.") and QtyDamage is not null ORDER BY DateInput DESC , RecordID DESC LIMIT 0,1";
	$arr_tmp_status = $linventory->returnArray($sql,3);
	if(sizeof($arr_tmp_status)>0)
	{
		list($normal_qty, $damage_qty, $repair_qty) = $arr_tmp_status[0];
		## normal qty = retrieve location qty - damage - repair
		$normal_qty = $original_location_qty - $damage_qty - $repair_qty;
	}
	else
	{
		$normal_qty = $original_location_qty;
		$damage_qty = 0;
		$repair_qty = 0;
	}
	$sql = "INSERT INTO 
					INVENTORY_ITEM_BULK_LOG
					(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
			VALUES
					($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$normal_qty',$damage_qty,$repair_qty,$targetOldLocation,$targetOldGroup,$UserID,NOW(),NOW())
			";
	$linventory->db_db_query($sql) or die(mysql_error());
	
	#############################################################
	### Update Normal, Damage, Repair Qty in the NEW location ###
	#############################################################
	# retrieve the new location total for QtyNormal usage (fixed: data in INVENTORY_ITEM_BULK_LOG is incorrect)
	$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetNewSubLocation AND GroupInCharge = $destination_admin_group_id";
	$arr_tmp_result = $linventory->returnVector($sql);
	$new_total = $arr_tmp_result[0];
	
	$sql = "SELECT QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = $item_id AND LocationID = $targetNewSubLocation AND GroupInCharge = $destination_admin_group_id AND Action IN (".ITEM_ACTION_UPDATESTATUS.",".ITEM_ACTION_PURCHASE.") and QtyDamage is not null ORDER BY DateInput DESC , RecordID DESC LIMIT 0,1";
	$arr_tmp_status = $linventory->returnArray($sql);
	if(sizeof($arr_tmp_status)>0)
	{
		list($damage_qty, $repair_qty) = $arr_tmp_status[0];
		$normal_qty = $new_total - $damage_qty - $repair_qty;
	}
	else
	{
		$normal_qty = $new_total;
		$damage_qty = 0;
		$repair_qty = 0;
	}
	
	$sql = "INSERT INTO 
					INVENTORY_ITEM_BULK_LOG	
					(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, PersonInCharge, DateInput, DateModified)
			VALUES
					($item_id, '$curr_date', ".ITEM_ACTION_UPDATESTATUS.",'$normal_qty','$damage_qty','$repair_qty',$targetNewSubLocation,$destination_admin_group_id,$UserID,NOW(),NOW())
			";
	$linventory->db_db_query($sql) or die(mysql_error());
		
	### Check the original location quantity is 0 or not ###
	$sql = "SELECT 
					Quantity 
			FROM 
					INVENTORY_ITEM_BULK_LOCATION 
			WHERE 
					ItemID = $item_id AND
					LocationID = $targetOldLocation AND
					GroupInCharge = $targetOldGroup
			";
	$arr_tmp_exist_qty = $linventory->returnVector($sql);
	if(sizeof($arr_tmp_exist_qty)>0)
	{
		$exist_qty = $arr_tmp_exist_qty[0];
	}
	
	### quantity = 0, delete record ###
	if($exist_qty == 0)
	{
		$sql = "DELETE FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $item_id AND LocationID = $targetOldLocation AND GroupInCharge = $targetOldGroup";
		$linventory->db_db_query($sql);
	}
}
*/
if($item_type == ITEM_TYPE_BULK)
{
	# support multiple location
	$targetNewSubLocation_ary = $targetNewSubLocation;
	$bulk_new_qty_ary = $bulk_new_qty;

	for($i=0,$cnt=sizeof($targetNewSubLocation_ary);$i<$cnt;$i++)
	{
		//$bulk_new_qty = intval($bulk_new_qty);
		//$qty_change = $bulk_new_qty;
		$targetNewSubLocation = $targetNewSubLocation_ary[$i];
		$qty_change = $bulk_new_qty_ary[$i];
		$qty_change = is_numeric($qty_change)?$qty_change:0;

		# new? update? for bluk_location
		$sql = "SELECT 
					RecordID 
				FROM 
					INVENTORY_ITEM_BULK_LOCATION 
				WHERE 
					ItemID = '$item_id' AND 
					LocationID = '$targetNewSubLocation' and
					GroupInCharge = '$targetGroupID' and
					FundingSourceID = '$targetFundingID'
				";
		$arr_rec_id = $linventory->returnVector($sql);
		if(sizeof($arr_rec_id)>0)	# exists
		{
			# exists item + qty
			$rec_id = $arr_rec_id[0];
			$sql = "UPDATE
						INVENTORY_ITEM_BULK_LOCATION
					SET
						Quantity = Quantity + $qty_change
					WHERE
						RecordID = '$rec_id'
					";
			$linventory->db_db_query($sql);
		}
		else
		{
			$sql = "INSERT INTO
							INVENTORY_ITEM_BULK_LOCATION
							(ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
					VALUES
							('$item_id', '$targetGroupID', '$targetNewSubLocation', '$targetFundingID', $qty_change)
					";
			$linventory->db_db_query($sql);
		}
		
		# original item - qty 
		$sql = "UPDATE 
						INVENTORY_ITEM_BULK_LOCATION 
				SET
						Quantity = Quantity - $qty_change
				WHERE 
						ItemID = '$item_id' AND
						LocationID = '$targetLocation' AND
						GroupInCharge = '$targetGroupID' and
						FundingSourceID = '$targetFundingID'
				";
		$linventory->db_db_query($sql);
		
		### Check the original location quantity is 0 or not ###
		$sql = "SELECT 
						Quantity 
				FROM 
						INVENTORY_ITEM_BULK_LOCATION 
				WHERE 
						ItemID = '$item_id' AND
						LocationID = '$targetLocation' AND
						GroupInCharge = '$targetGroupID' and
						FundingSourceID = '$targetFundingID'
				";
		$arr_tmp_exist_qty = $linventory->returnVector($sql);
		if(sizeof($arr_tmp_exist_qty)>0)
		{
			$exist_qty = $arr_tmp_exist_qty[0];
		}
		### quantity = 0, delete record ###
		if($exist_qty == 0)
		{
			$sql = "DELETE FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '$item_id' AND LocationID = '$targetLocation' AND GroupInCharge = '$targetGroupID' and FundingSourceID='$targetFundingID'";
			$linventory->db_db_query($sql);
		}
		
		### Original Location - change log
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, LocationID, GroupInCharge, FundingSource, PersonInCharge, DateInput, DateModified)
				VALUES
						('$item_id', '$curr_date','".ITEM_ACTION_CHANGELOCATION."',-$qty_change,'$targetLocation','$targetGroupID','$targetFundingID','$UserID',NOW(),NOW())
				";
		$linventory->db_db_query($sql);
				
		### New Location - change log
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyChange, LocationID, GroupInCharge, FundingSource, PersonInCharge, DateInput, DateModified)
				VALUES
						('$item_id', '$curr_date','".ITEM_ACTION_CHANGELOCATION."',$qty_change,'$targetNewSubLocation','$targetGroupID','$targetFundingID','$UserID',NOW(),NOW())
				";
		$linventory->db_db_query($sql);
		
		##################################################################
		### Update Normal, Damage, Repair Qty in the original location ###
		##################################################################
		$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '$item_id' AND LocationID = '$targetLocation' AND GroupInCharge = '$targetGroupID' and FundingSourceID='$targetFundingID'";
		$arr_tmp_result = $linventory->returnVector($sql);
		$original_location_qty = $arr_tmp_result[0];
		
		$sql = "SELECT QtyNormal, QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '$item_id' AND LocationID = '$targetLocation' AND GroupInCharge = '$targetGroupID' and FundingSource='$targetFundingID' AND Action IN ('".ITEM_ACTION_UPDATESTATUS."','".ITEM_ACTION_PURCHASE."') and QtyDamage is not null ORDER BY DateInput DESC , RecordID DESC LIMIT 0,1";
		$arr_tmp_status = $linventory->returnArray($sql,3);
		if(sizeof($arr_tmp_status)>0)
		{
			list($normal_qty, $damage_qty, $repair_qty) = $arr_tmp_status[0];
			## normal qty = retrieve location qty - damage - repair
			$normal_qty = $original_location_qty - $damage_qty - $repair_qty;
		}
		else
		{
			$normal_qty = $original_location_qty;
			$damage_qty = 0;
			$repair_qty = 0;
		}
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG
						(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, FundingSource, PersonInCharge, DateInput, DateModified)
				VALUES
						('$item_id', '$curr_date', '".ITEM_ACTION_UPDATESTATUS."','$normal_qty','$damage_qty','$repair_qty','$targetLocation','$targetGroupID', '$targetFundingID','$UserID',NOW(),NOW())
				";
		$linventory->db_db_query($sql);
		
		#############################################################
		### Update Normal, Damage, Repair Qty in the NEW location ###
		#############################################################
		# retrieve the new location total for QtyNormal usage (fixed: data in INVENTORY_ITEM_BULK_LOG is incorrect)
		$sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '$item_id' AND LocationID = '$targetNewSubLocation' AND GroupInCharge = '$targetGroupID' and FundingSourceID='$targetFundingID'";
		$arr_tmp_result = $linventory->returnVector($sql);
		$new_total = $arr_tmp_result[0];
		
		$sql = "SELECT QtyDamage, QtyRepair FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '$item_id' AND LocationID = '$targetNewSubLocation' AND GroupInCharge = '$targetGroupID' and FundingSource='$targetFundingID' AND Action IN ('".ITEM_ACTION_UPDATESTATUS."','".ITEM_ACTION_PURCHASE."') and QtyDamage is not null ORDER BY DateInput DESC , RecordID DESC LIMIT 0,1";
		$arr_tmp_status = $linventory->returnArray($sql);
		if(sizeof($arr_tmp_status)>0)
		{
			list($damage_qty, $repair_qty) = $arr_tmp_status[0];
			$normal_qty = $new_total - $damage_qty - $repair_qty;
		}
		else
		{
			$normal_qty = $new_total;
			$damage_qty = 0;
			$repair_qty = 0;
		}
		
		$sql = "INSERT INTO 
						INVENTORY_ITEM_BULK_LOG	
						(ItemID, RecordDate, Action, QtyNormal, QtyDamage, QtyRepair, LocationID, GroupInCharge, FundingSource, PersonInCharge, DateInput, DateModified)
				VALUES
						('$item_id', '$curr_date', '".ITEM_ACTION_UPDATESTATUS."','$normal_qty','$damage_qty','$repair_qty','$targetNewSubLocation','$targetGroupID','$targetFundingID','$UserID',NOW(),NOW())
				";
		$linventory->db_db_query($sql);
	}	
}

if($exist_qty>0)
	header("location: items_location_edit.php?item_id=$item_id&targetLocation=$targetLocation&targetGroupID=$targetGroupID&targetFundingID=$targetFundingID&xmsg=UpdateSuccess");
else
	header("location: items_full_list.php?msg=2");
	
intranet_closedb();
?>