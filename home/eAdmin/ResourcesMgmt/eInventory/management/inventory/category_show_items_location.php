<?php
// using:

// ################################################
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-07 (Henry)
// add access right checking [Case#E135442]
//
// Date: 2016-02-04 Henry
// php 5.4 issue move set cookies after includes file
//
// Date: 2012-12-03 YatWoon
// Remove "Invoice" link
//
// Date: 2012-03-27 YatWoon
// Improved: allow user edit mgmt group and funding source
//
// ################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

// ## set cookies
if ($page_size_change == 1) {
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
// preserve table view
if ($ck_category_item_browsing_view_location_record_page_number != $pageNo && $pageNo != "") {
    setcookie("ck_category_item_browsing_view_location_record_page_number", $pageNo, 0, "", "", 0);
    $ck_category_item_browsing_view_location_record_page_number = $pageNo;
} else 
    if (! isset($pageNo) && $ck_category_item_browsing_view_location_record_page_number != "") {
        $pageNo = $ck_category_item_browsing_view_location_record_page_number;
    }

if ($ck_category_item_browsing_view_location_record_page_order != $order && $order != "") {
    setcookie("ck_category_item_browsing_view_location_record_page_order", $order, 0, "", "", 0);
    $ck_category_item_browsing_view_location_record_page_order = $order;
} else 
    if (! isset($order) && $ck_category_item_browsing_view_location_record_page_order != "") {
        $order = $ck_category_item_browsing_view_location_record_page_order;
    }

if ($ck_category_item_browsing_view_location_record_page_field != $field && $field != "") {
    setcookie("ck_category_item_browsing_view_location_record_page_field", $field, 0, "", "", 0);
    $ck_category_item_browsing_view_location_record_page_field = $field;
} else 
    if (! isset($field) && $ck_category_item_browsing_view_location_record_page_field != "") {
        $field = $ck_category_item_browsing_view_location_record_page_field;
    }

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);

/*
 * $table_tool .= "<td nowrap=\"nowrap\">
 * <a href=\"javascript:checkRemove(document.form1,'cat2_id[]','category_level2_remove.php')\" class=\"tabletool\">
 * <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
 * $button_remove
 * </a>
 * </td>";
 * $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
 */

// Gen the table tool (edit) #
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'RecordID[]','bulk_location_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";

// get the name of infobar #
$namefield1 = $linventory->getInventoryItemNameByLang("a.");
$namefield2 = $linventory->getInventoryItemNameByLang("b.");
$namefield3 = $linventory->getInventoryItemNameByLang("c.");
$sql = "SELECT 
				a.ItemCode, 
				$namefield1				
		FROM 
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS c ON (a.CategoryID = c.CategoryID AND a.Category2ID = c.Category2ID)
		WHERE
				a.ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 2);

$temp[] = array(
    "<a href=\"items_full_list.php\">$i_InventorySystem_BackToFullList</a>"
);
$infobar1 .= "<tr><td colspan=\"2\" class=\"tabletext\">" . $linterface->GET_NAVIGATION($temp) . "</td></tr>";

$titlebar .= "<tr><td align=\"left\">";
$titlebar .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_section.gif\" width=\"20\" height=\"20\" align=\"absmiddle\" />";
$titlebar .= "<span class=\"sectiontitle\">" . $result[0][0] . " - " . $result[0][1] . "</span>";
$titlebar .= "</td>";
$titlebar .= "<td align=\"right\">$toolbar";
$titlebar .= "</td></tr>";

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$item_id."'";
$result = $linventory->returnArray($sql, 1);

if (sizeof($result) > 0) {
    list ($item_type) = $result[0];
}

$item_namefield1 = $linventory->getInventoryItemNameByLang("a.");
$item_namefield2 = $linventory->getInventoryItemNameByLang("b.");
$item_namefield3 = $linventory->getInventoryItemNameByLang("c.");
$item_namefield4 = $linventory->getInventoryItemNameByLang("d.");
$item_desc_namefield = $linventory->getInventoryDescriptionNameByLang("a.");

if ($item_type == 1) {
    $sql = "SELECT
					$item_namefield3,
					$item_namefield4,
					1,
					CONCAT('<input type=checkbox name=ItemID[] value=', a.ItemID ,'>')
			FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID)
			WHERE
					a.ItemID = '".$item_id."'";
}

if ($item_type == 2) {
    $sql = "SELECT 
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
					" . $linventory->getInventoryNameByLang("e.") . ",
					" . $linventory->getInventoryNameByLang("f.") . ",
					b.Quantity 
					,CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' value=',b.RecordID,'>')
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID) INNER JOIN
					INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
					INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
					INVENTORY_ADMIN_GROUP AS e ON (b.GroupInCharge = e.AdminGroupID)
					inner join INVENTORY_FUNDING_SOURCE as f on f.FundingSourceID=b.FundingSourceID
			WHERE
					a.ItemID = '".$item_id."'";
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$field = $field == "" ? 1 : $field;
if (! isset($order))
    $order = 0;
    
    // TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array(
    "",
    "d.NameEng",
    "e.NameEng",
    "b.Quantity"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = "";
$li->column_array = array(
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0
);
$li->IsColOff = 2;
// echo $li->built_sql();

// TABLE COLUMN
$pos = 1;
$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $i_InventorySystem_Location_Level . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $i_InventorySystem[Caretaker] . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $i_InventorySystem_Item_Funding . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $i_InventorySystem_Item_Qty . "</td>\n";
// $li->column_list .= "<td width='1'>".$li->check("ItemID[]")."</td>\n";
$li->column_list .= "<td width='1'>" . $li->check("RecordID[]") . "</td>\n";

?>

<br>

<form name="form1" enctype="multipart/form-data" action="" method="POST">

	<table id="html_body_frame" width="100%" border="0" cellspacing="0"
		cellpadding="5" align="center">
<?=$infobar1?>
</table>
	<br>
	<br>
	<table width="96%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<?=$titlebar?>
</table>
	<table width="100%" border="0" cellspacing="0" cellpadding="5"
		align="center">
<? if($item_type == 1) { ?>
<tr>
			<td>
				<table border=0 align="center" width="96%">
					<tr class="single">
						<td height="20px" valign="top" nowrap="nowrap" class="tabletext">
							<a
							href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_history.php?type=5&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<? } ?>
<? if($item_type == 2) { ?>
<tr>
			<td>
				<table border=0 align="center" width="96%">
					<tr class="bulk">
						<td height="20px" valign="top" nowrap="nowrap" class="tabletext">
							<a
							href="category_show_items_detail.php?type=1&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==1) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_Detail?><? IF($type==1) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_location.php?type=2&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==2) echo"<B>"; else echo " "; ?><?=$i_InventorySystem['Location']." ".$i_InventorySystem_Search_And2." ".$i_InventorySystem['Caretaker']?><? IF($type==2) echo"</B>"; else echo " "; ?></a>&nbsp;|&nbsp;
							<a
							href="category_show_items_history.php?type=5&item_id=<?=$item_id?>"
							class="tablegreenlink"><? IF($type==5) echo"<B>"; else echo " "; ?><?=$i_InventorySystem_Item_History?><? IF($type==5) echo"</B>"; else echo " "; ?></a>
						</td>
					</tr>
				</table>
			</td>
		</tr>
<? } ?>
</table>

	<table width="90%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr height="3px">
			<td></td>
		</tr>
		<tr>
			<td colspan="" valign="bottom" align="right">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif"
							width="21" height="23"></td>
						<td
							background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
								<?=$table_tool?>
							</tr>
							</table>
						</td>
						<td width="6"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif"
							width="6" height="23"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<?echo $li->display("90%"); ?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"> <input
		type="hidden" name="order" value="<?php echo $li->order; ?>"> <input
		type="hidden" name="field" value="<?php echo $li->field; ?>"> <input
		type="hidden" name="page_size_change" value=""> <input type="hidden"
		name="numPerPage" value="<?=$li->page_size?>"> <input type="hidden"
		name="attachStr" value="" />

</form>

<br>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>