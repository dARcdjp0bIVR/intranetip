<?php
// using:

// #############################################################################
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
//
// Date: 2018-02-21 Henry
// add access right checking [Case#E135442]
//
// Date: 2014-09-15 YatWoon
// fixed: failed to delete the DB record if the attachment is missing [Case#Z67893]
//
// #############################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$item_id = $ItemID;

if ($AttachmentID != "") {
    $sql = "SELECT AttachmentPath, Filename FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE AttachmentID = '".$AttachmentID."'";
    $arr_tmp_attachment = $linventory->returnArray($sql, 2);
    
    if (sizeof($arr_tmp_attachment) > 0) {
        list ($re_path, $file_name) = $arr_tmp_attachment[0];
        
        $path = "$intranet_root$re_path" . "$file_name";
    }
    
    $lf = new libfilesystem();
    
    if (is_file($path)) {
        $lf->file_remove($path);
    }
    $sql = "DELETE FROM INVENTORY_ITEM_ATTACHMENT_PART WHERE AttachmentID = '".$AttachmentID."'";
    $result = $linventory->db_db_query($sql);
}

if ($result == false) {
    header("location: category_show_items_edit.php?item_list=$item_id&msg=12");
} else {
    header("location: category_show_items_edit.php?item_list=$item_id&msg=1");
}
intranet_closedb();
?>