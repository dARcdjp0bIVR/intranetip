<?php
// modifying :

/**
 * **************************** Change Log [Start] *******************************
 * 2020-06-11 (Cameron): fix: should pass advanced search $cond if no record is selected to export so that the export result should be in the searched list but not all [case #N184694]
 * 2019-05-13 (Henry): Security fix: SQL without quote
 * 2019-05-01 (Henry): security issue fix for SQL
 * 2018-02-21 (Henry): add access right checking [Case#E135442]
 * 2016-02-01 (Henry): PHP 5.4 fix: change split to explode
 * 2014-12-04 (Henry): Fixed: Incorrect unit price of bulk items [Case#V72234]
 * 2013-10-18 (YatWoon): Improved: export with simple search result [Case#2013-1017-1352-26170]
 * 2013-05-24 (YatWoon): Fixed: missing to cater Tag data [Case#2013-0524-1245-08054]
 * 2013-04-16 (YatWoon): support search for bulk item barcode
 * Date: 2012-12-12 (Yuen)
 * Support tags
 * 2012-11-02 (YatWoon): Fixed: incorrect query display for QuotationNo [Case#2012-1025-1621-03073]
 * 2012-07-12 (YatWoon): Improved: display 2 funding for single item
 * 2011-12-06 (YatWoon): Fixed: incorrect export result ( " related) [Case#2011-1118-1104-41066]
 * 2011-10-14 (YatWoon): Fixed: incorrect export result [Case#2011-0901-1520-33066]
 * 2011-06-23 (Yuen): correct the initial quantity of bulk item by not counting the pending write-off items
 * 2011-04-19 (YatWoon): add option "display 0 record"
 * ****************************** Change Log [End] ********************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$lexport = new libexporttext();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ItemID = IntegerSafe($ItemID, ":");
$itemIDs = IntegerSafe($ItemIDs);

/*
 * if($BuildingSelected != ""){
 * $cond_Building = " AND building.BuildingID IN ($BuildingSelected) ";
 * }
 *
 * if($FloorSelected != ""){
 * $cond_Location1 = " AND f.LocationLevelID IN ($FloorSelected) ";
 * $cond_Location2 = " AND g.LocationLevelID IN ($FloorSelected) ";
 * }else{
 * $cond_Location1 = "";
 * $cond_Location2 = "";
 * }
 *
 * if($RoomSelected != ""){
 * $cond_SubLocation1 = " AND e.LocationID IN ($RoomSelected) ";
 * $cond_SubLocation2 = " AND f.LocationID IN ($RoomSelected) ";
 * }else{
 * $cond_SubLocation1 = "";
 * $cond_SubLocation2 = "";
 * }
 */

if ($BuildingSelected == "") {
    $sql = "SELECT 
					LocationID
			FROM
					INVENTORY_LOCATION
			WHERE
					RecordStatus = 1
			ORDER BY
					LocationLevelID, LocationID";
    $result = $linventory->returnVector($sql);
} else {
    if ($FloorSelected == "") {
        $sql = "SELECT
						c.LocationID
				FROM
						INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
						INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
				WHERE
						a.BuildingID = $BuildingSelected AND
						a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
        $result = $linventory->returnVector($sql);
    } else {
        if ($RoomSelected == "") {
            $sql = "SELECT
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = $BuildingSelected AND
							b.LocationLevelID = $FloorSelected AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = $BuildingSelected AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        } else {
            
            $sql = "SELECT 
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = $BuildingSelected AND
							b.LocationLevelID = $FloorSelected AND
							c.LocationID = $RoomSelected AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT 
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = $BuildingSelected AND
								b.LocationLevelID = $FloorSelected AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        }
    }
}
if (sizeof($result) > 0) {
    $location_list = implode(",", $result);
    
    $cond_location = " and (d.LocationID IN ($location_list)) ";
}

if ($location_list != "") {
    $location_condition1 = " AND d.LocationID IN ($location_list) ";
    // $location_condition2 = " AND g.LocationID IN ($location_list) ";
}
// ## End Of Location Condition ###

if ($targetGroup != "") {
    $cond_Group1 = " AND g.AdminGroupID IN ($targetGroup) ";
    $cond_Group2 = " AND h.AdminGroupID IN ($targetGroup) ";
} else {
    $cond_Group1 = "";
    $cond_Group2 = "";
}

if ($targetFunding != "") {
    $cond_Funding1 = " AND h.FundingSourceID IN ($targetFunding) ";
    $cond_Funding2 = " AND i.FundingSourceID IN ($targetFunding) ";
} else {
    $cond_Funding1 = "";
    $cond_Funding2 = "";
}

if ($targetCategory != "") {
    $cond_Category1 = " AND b.CategoryID IN ($targetCategory) ";
    $cond_Category2 = " AND b.CategoryID IN ($targetCategory) ";
} else {
    $cond_Category1 = "";
    $cond_Category2 = "";
}

if ($targetSubCategory != "") {
    $cond_SubCategory1 = " AND c.Category2ID IN ($targetSubCategory) ";
    $cond_SubCategory2 = " AND c.Category2ID IN ($targetSubCategory) ";
} else {
    $cond_SubCategory1 = "";
    $cond_SubCategory2 = "";
}

if ($Tag != "") {
    $RecordIDbyTags_Arr = $linventory->getRecordIDsFromTagName($Tag);
    if (is_array($RecordIDbyTags_Arr) && sizeof($RecordIDbyTags_Arr) > 0) {
        $RecordIDbyTags = implode(",", $RecordIDbyTags_Arr);
        $cond_tag = " AND a.ItemID IN ({$RecordIDbyTags}) ";
    }
}

if (sizeof($ItemID) != 0 || $itemIDs != '') { // $itemIDs for search_result.php use
    
    if ($itemIDs != '' && sizeof($ItemID) == 0) {
        $ItemID = explode(',', $itemIDs);
    } else {
        // rebuild $ItemID
        $ItemID_New = array();
        foreach ($ItemID as $k => $d) {
            list ($item_id_temp, $item_locationid_temp) = explode(":", $d);
            $ItemID_New[] = $item_id_temp;
        }
        $ItemID = $ItemID_New;
    }
    
    if ($targetItemID != "") {
        $arr_itemID = explode(",", $targetItemID);
        $temp_arr = array_merge($arr_itemID, $ItemID);
        $targetItems = implode(",", $temp_arr);
        $cond_targetItem = " AND a.ItemID IN ($targetItems) ";
        $cond_targetItem2 = " AND a.ItemID IN ($targetItems) ";
    } else {
        $temp_arr = $ItemID;
        $targetItems = Implode(",", $temp_arr);
        $cond_targetItem = " AND a.ItemID IN ($targetItems) ";
        $cond_targetItem2 = " AND a.ItemID IN ($targetItems) ";
    }
} else {
    // # export all item ##
    if ($cond) {        // from advanced search
        $cond = stripslashes($cond);
        $sql = "SELECT DISTINCT a.ItemID
                FROM
                        INVENTORY_ITEM AS a LEFT OUTER JOIN 
                        INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID ) LEFT OUTER JOIN 
                        INVENTORY_ITEM_BULK_LOG AS c ON (a.ItemID = c.ItemID) LEFT OUTER JOIN
                        INVENTORY_ITEM_BULK_LOCATION AS d ON (a.ItemID = d.ItemID) LEFT OUTER JOIN
                        INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID) 
                WHERE ".$cond;
        $itemIDAry = $linventory->returnVector($sql);
        if (count($itemIDAry)) {
            $cond_targetItem = " AND a.ItemID IN (".implode(",",$itemIDAry).")";
            $cond_targetItem2 = $cond_targetItem;
        }
        else {
            $cond_targetItem = "";
            $cond_targetItem2 = "";
        }
    }
    else {
        $cond_targetItem = "";
        $cond_targetItem2 = "";
    }
}

if ($linventory->getAccessLevel($UserID) == 2) {
    $targetGroup = $linventory->getInventoryAdminGroup();
    $cond_targetGroup = " AND g.AdminGroupID IN ($targetGroup)";
    $cond_targetGroup2 = " AND h.AdminGroupID IN ($targetGroup)";
} else {
    $cond_targetGroup = "";
    $cond_targetGroup2 = "";
}

if ($targetItemType == "") {
    $export_SingleItem = true;
    $export_BulkItem = true;
} else 
    if ($targetItemType == 1) {
        $export_SingleItem = true;
        $export_BulkItem = false;
    } else {
        $export_SingleItem = false;
        $export_BulkItem = true;
    }

$file_content = "";
$keyword_cond = "";

if ($export_SingleItem) {
    // # SingleItem ##
    // # Title Row ##
    // #$file_format = array("Item Code","Barcode","Item Chinese Name","Item English Name","Chinese Description","English Description","Category Code","Sub-category Code","Group Code","Location Code","Sub-Location Code","Funding Source Code","Ownership","Warranty Expiry Date","License","Serial No","Brand","Supplier","Supplier Contact","Supplier Description","Quotation No","Tender No","Invoice No","Purchase Date","Total Purchase Amount","Unit Price","Maintanence Details","Remarks");
    $exportColumn = array(
        $i_InventorySystem_Item_Code_ExportTitle,
        $i_InventorySystem_Item_Barcode_ExportTitle,
        $i_InventorySystem_Item_ChineseName_ExportTitle,
        $i_InventorySystem_Item_EnglishName_ExportTitle,
        $i_InventorySystem_Item_ChineseDescription_ExportTitle,
        $i_InventorySystem_Item_EnglishDescription_ExportTitle,
        $i_InventorySystem_Item_CategoryName_ExportTitle,
        $i_InventorySystem_Item_Category2Name_ExportTitle,
        $i_InventorySystem_Item_GroupName_ExportTitle,
        $Lang['eInventory']['FieldTitle']['Export']['Building'],
        $i_InventorySystem_Item_LocationName_ExportTitle,
        $i_InventorySystem_Item_Location2Name_ExportTitle,
        $i_InventorySystem_Item_FundingSourceName_ExportTitle . " (1)",
        $i_InventorySystem_Unit_Price_ExportTitle . " (1)",
        $i_InventorySystem_Item_FundingSourceName_ExportTitle . " (2)",
        $i_InventorySystem_Unit_Price_ExportTitle . " (2)",
        $i_InventorySystem_Item_Ownership_ExportTitle,
        $i_InventorySystem_Item_Warrany_Expiry_ExportTitle,
        $i_InventorySystem_Item_License_ExportTitle,
        $i_InventorySystem_Item_Serial_Num_ExportTitle,
        $i_InventorySystem_Item_Brand_Name_ExportTitle,
        $i_InventorySystem_Item_Supplier_Name_ExportTitle,
        $i_InventorySystem_Item_Supplier_Contact_ExportTitle,
        $i_InventorySystem_Item_Supplier_Description_ExportTitle,
        $i_InventorySystem_Item_Quot_Num_ExportTitle,
        $i_InventorySystem_Item_Tender_Num_ExportTitle,
        $i_InventorySystem_Item_Invoice_Num_ExportTitle,
        $i_InventorySystem_Item_Purchase_Date_ExportTitle,
        $i_InventorySystem_Item_Price_ExportTitle,
        $i_InventorySystem_Unit_Price_ExportTitle,
        $i_InventorySystemItemMaintainInfo_ExportTitle,
        $i_InventorySystem_Item_Remark_ExportTitle,
        "Stocktake Option",
        "Tags"
    );
    
    /*
     * $file_content = "\"$i_InventorySystem_ItemType_Single_ExportTitle_ExportTitle\"\n";
     * $file_content .= "\"$i_InventorySystem_Item_Code_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Barcode_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_ChineseName_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_EnglishName_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_ChineseDescription_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_EnglishDescription_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_CategoryName_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Category2Name_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_GroupName_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_LocationName_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Location2Name_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_FundingSourceName_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Ownership_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Warrany_Expiry_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_License_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Serial_Num_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Brand_Name_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Supplier_Name_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Supplier_Contact_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Supplier_Description_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Quot_Num_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Tender_Num_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Invoice_Num_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Purchase_Date_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Price_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Unit_Price_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystemItemMaintainInfo_ExportTitle\",";
     * $file_content .= "\"$i_InventorySystem_Item_Remark_ExportTitle\"\n";
     */
    
    // ## Set SQL Condition - Keyword ###
    if ($keyword != "") {
        if ((strpos($keyword, "*") == 0) && (strrpos($keyword, "*") == strlen($keyword) - 1)) {
            $barcode = substr($keyword, 1, strlen($keyword) - 2);
        }
        
        $keyword_cond = " AND ((a.NameChi LIKE '%$keyword%' OR a.NameEng LIKE '%$keyword%') OR
				(a.ItemCode LIKE '%$keyword%') OR
				(b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
				(c.NameChi LIKE '%$keyword%' OR c.NameEng LIKE '%$keyword%') OR
				(d.TagCode LIKE '%$keyword%') OR
				(d.Brand LIKE '%$keyword%') OR
				(d.ItemRemark LIKE '%$keyword%') OR
				(a.DescriptionChi LIKE '%$keyword%') OR
				(a.DescriptionEng LIKE '%$keyword%')
				) ";
    }
    
    $sql = "SELECT 
					a.ItemID,
					IF(a.NameChi = '', '', a.NameChi),
					IF(a.NameEng = '', '', a.NameEng),
					IF(a.DescriptionChi = '', '', a.DescriptionChi),
					IF(a.DescriptionEng = '', '', a.DescriptionEng),
					a.ItemCode,
					CASE a.Ownership 
						WHEN " . ITEM_OWN_BY_SCHOOL . " THEN CONCAT('" . $i_InventorySystem_Ownership_School . "')
						WHEN " . ITEM_OWN_BY_GOVERNMENT . " THEN CONCAT('" . $i_InventorySystem_Ownership_Government . "')
						WHEN " . ITEM_OWN_BY_DONOR . " THEN CONCAT('" . $i_InventorySystem_Ownership_Donor . "')
					END,
					" . $linventory->getInventoryNameByLang("b.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					d.PurchaseDate,
					if(d.PurchasedPrice = '', '0', d.PurchasedPrice),
					if(d.UnitPrice = '', '0', d.UnitPrice),
					IF(d.ItemRemark = '', '', d.ItemRemark),
					IF(d.SupplierName = '', '', d.SupplierName),
					IF(d.SupplierContact = '', '', d.SupplierContact),
					IF(d.SupplierDescription = '', '', d.SupplierDescription),
					IF(d.MaintainInfo = '', '', d.MaintainInfo),
					IF(d.InvoiceNo = '', '', d.InvoiceNo),
					IF(d.QuotationNo= '', '', d.QuotationNo),
					IF(d.TenderNo = '', '', d.TenderNo),
					d.TagCode,
					IF(d.Brand = '', '', d.Brand),
					IF(d.WarrantyExpiryDate = '', '', d.WarrantyExpiryDate),
					IF(d.SoftwareLicenseModel = '', '', d.SoftwareLicenseModel),
					IF(d.SerialNumber = '', '', d.SerialNumber),
					" . $linventory->getInventoryNameByLang("building.") . ",
					" . $linventory->getInventoryNameByLang("f.") . ",
					" . $linventory->getInventoryNameByLang("e.") . ",
					" . $linventory->getInventoryNameByLang("g.") . ",
					" . $linventory->getInventoryNameByLang("h.") . ",
					" . $linventory->getInventoryNameByLang("h2.") . ",
					if(d.UnitPrice1 = '', '0', d.UnitPrice1),
					if(d.UnitPrice2 = '', '', d.UnitPrice2),
					a.StockTakeOption
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = 1 AND a.ItemType = 1) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID AND a.RecordStatus = 1 AND a.ItemType = 1) 
					INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS d ON (a.ItemID = d.ItemID $location_condition1) LEFT OUTER JOIN 
					INVENTORY_LOCATION AS e ON (d.LocationID = e.LocationID) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS f ON (e.LocationLevelID = f.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (building.BuildingID = f.BuildingID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS g ON (d.GroupInCharge = g.AdminGroupID) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS h ON (d.FundingSource = h.FundingSourceID)
					LEFT JOIN INVENTORY_FUNDING_SOURCE AS h2 ON (d.FundingSource2 = h2.FundingSourceID)
			WHERE
					a.ItemType = 1
$cond_location					
$keyword_cond
					$cond_targetItem
					$cond_targetGroup
					$cond_Location1
					$cond_SubLocation1
					$cond_Building
					$cond_Group1
					$cond_Funding1
					$cond_Category1
					$cond_SubCategory1
					$cond_tag
			ORDER BY
					a.ItemCode";
    $arr_result = $linventory->returnArray($sql, 28);
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($item_id, $item_chi_name, $item_eng_name, $item_chi_desc, $item_eng_desc, $item_code, $item_ownership, $item_cat_code, $item_cat2_code, $item_purchase_date, $item_purchased_price, $item_unit_price, $item_remark, $item_supplier_name, $item_supplier_contact, $item_supplier_desc, $item_maintain_info, $item_invoice, $item_quotation, $item_tender, $item_tag_code, $item_brand, $item_warranty_expiry_date, $item_license, $item_serial_num, $item_building_code, $item_location_level_code, $item_location_code, $item_group_code, $item_funding_code, $item_funding_code2, $item_unit_price1, $item_unit_price2, $stocktakeoption) = $arr_result[$i];
            
            if ($item_warranty_expiry_date == "0000-00-00")
                $item_warranty_expiry_date = "";
            if ($item_purchase_date == "0000-00-00")
                $item_purchase_date = "";
            
            $tagsDisplay = $linventory->displayTags($item_id, false);
            
            // # single item - csv content ##
            $temprows = array(
                $item_code,
                $item_tag_code,
                $item_chi_name,
                $item_eng_name,
                $item_chi_desc,
                $item_eng_desc,
                $item_cat_code,
                $item_cat2_code,
                $item_group_code,
                $item_building_code,
                $item_location_level_code,
                $item_location_code,
                $item_funding_code,
                $item_unit_price1,
                $item_funding_code2,
                $item_unit_price2,
                $item_ownership,
                $item_warranty_expiry_date,
                $item_license,
                $item_serial_num,
                $item_brand,
                $item_supplier_name,
                $item_supplier_contact,
                $item_supplier_desc,
                $item_quotation,
                $item_tender,
                $item_invoice,
                $item_purchase_date,
                $item_purchased_price,
                $item_unit_price,
                $item_maintain_info,
                $item_remark,
                $stocktakeoption,
                $tagsDisplay
            );
            $rows[] = $temprows;
            
            /*
             * $file_content .= "\"$item_code\",\"$item_tag_code\",\"$item_chi_name\",\"$item_eng_name\",\"$item_chi_desc\",\"$item_eng_desc\",";
             * $file_content .= "\"$item_cat_code\",\"$item_cat2_code\",\"$item_group_code\",\"$item_location_level_code\",\"$item_location_code\",\"$item_funding_code\",\"$item_ownership\",";
             * $file_content .= "\"$item_warranty_expiry_date\",\"$item_license\",\"$item_serial_num\",\"$item_brand\",\"$item_supplier_name\",\"$item_supplier_contact\",\"$item_supplier_desc\",";
             * $file_content .= "\"$item_quotation\",\"$item_tender\",\"$item_invoice\",\"$item_purchase_date\",\"$item_purchased_price\",\"$item_unit_price\",";
             * $file_content .= "\"$item_maintain_info\",\"$item_remark\"\n";
             */
        }
    }
    if (sizeof($arr_result) == 0) {
        $file_content .= "\"$i_no_record_exists_msg\"\n";
        
        $rows[] = array(
            $i_no_record_exists_msg
        );
    }
    
    $file_content .= "\n";
    $rows[] = array(
        "\n"
    );
}

if ($export_BulkItem) {
    // # Bulk Item ##
    // # Title Row ##
    $file_content .= "\"$i_InventorySystem_ItemType_Bulk_ExportTitle\"\n";
    $file_content .= "\"$i_InventorySystem_Item_Code_ExportTitle\",\"$i_InventorySystem_Item_ChineseName_ExportTitle\",\"$i_InventorySystem_Item_EnglishName_ExportTitle\",";
    $file_content .= "\"$i_InventorySystem_Item_ChineseDescription_ExportTitle\",\"$i_InventorySystem_Item_EnglishDescription_ExportTitle\",\"$i_InventorySystem_Item_CategoryName_ExportTitle\",";
    $file_content .= "\"$i_InventorySystem_Item_Category2Name_ExportTitle\",\"$i_InventorySystem_Item_GroupName_ExportTitle\",\"$i_InventorySystem_Item_LocationName_ExportTitle\",\"$i_InventorySystem_Item_Location2Name_ExportTitle\",";
    $file_content .= "\"$i_InventorySystem_Item_FundingSourceName_ExportTitle\",\"$i_InventorySystem_Item_Ownership_ExportTitle\",\"$i_InventorySystem_Item_BulkItemAdmin\",";
    $file_content .= "\"$i_InventorySystem_Item_Qty\",\"$i_InventorySystem_Item_Supplier_Name_ExportTitle\",\"$i_InventorySystem_Item_Supplier_Contact_ExportTitle\",";
    $file_content .= "\"$i_InventorySystem_Item_Supplier_Description_ExportTitle\",\"$i_InventorySystem_Item_Quot_Num_ExportTitle\",\"$i_InventorySystem_Item_Tender_Num_ExportTitle\",";
    $file_content .= "\"$i_InventorySystem_Item_Invoice_Num_ExportTitle\",\"$i_InventorySystem_Item_Purchase_Date_ExportTitle\",\"$i_InventorySystem_Item_Price_ExportTitle\",";
    $file_content .= "\"$i_InventorySystem_Unit_Price_ExportTitle\",\"$i_InventorySystemItemMaintainInfo_ExportTitle\",\"$i_InventorySystem_Item_Remark_ExportTitle\"\n";
    if ($exportColumn == "") {
        $exportColumn = array(
            $i_InventorySystem_Item_Code_ExportTitle,
            $i_InventorySystem_Item_Barcode_ExportTitle,
            $i_InventorySystem_Item_ChineseName_ExportTitle,
            $i_InventorySystem_Item_EnglishName_ExportTitle,
            $i_InventorySystem_Item_ChineseDescription_ExportTitle,
            $i_InventorySystem_Item_EnglishDescription_ExportTitle,
            $i_InventorySystem_Item_CategoryName_ExportTitle,
            $i_InventorySystem_Item_Category2Name_ExportTitle,
            $i_InventorySystem_Item_GroupName_ExportTitle,
            $Lang['eInventory']['FieldTitle']['Export']['Building'],
            $i_InventorySystem_Item_LocationName_ExportTitle,
            $i_InventorySystem_Item_Location2Name_ExportTitle,
            $i_InventorySystem_Item_FundingSourceName_ExportTitle,
            $i_InventorySystem_Item_Ownership_ExportTitle,
            $i_InventorySystem_Item_BulkItemAdmin_ExportTitle,
            $i_InventorySystem_Item_Qty_ExportTitle,
            $i_InventorySystem_Item_Supplier_Name_ExportTitle,
            $i_InventorySystem_Item_Supplier_Contact_ExportTitle,
            $i_InventorySystem_Item_Supplier_Description_ExportTitle,
            $i_InventorySystem_Item_Quot_Num_ExportTitle,
            $i_InventorySystem_Item_Tender_Num_ExportTitle,
            $i_InventorySystem_Item_Invoice_Num_ExportTitle,
            $i_InventorySystem_Item_Purchase_Date_ExportTitle,
            $i_InventorySystem_Item_Price_ExportTitle,
            $i_InventorySystem_Unit_Price_ExportTitle,
            $i_InventorySystemItemMaintainInfo_ExportTitle,
            $i_InventorySystem_Item_Remark_ExportTitle,
            "Stocktake Option",
            "Tags"
        );
    } else {
        $rows[] = array(
            $i_InventorySystem_Item_Code_ExportTitle,
            $i_InventorySystem_Item_Barcode_ExportTitle,
            $i_InventorySystem_Item_ChineseName_ExportTitle,
            $i_InventorySystem_Item_EnglishName_ExportTitle,
            $i_InventorySystem_Item_ChineseDescription_ExportTitle,
            $i_InventorySystem_Item_EnglishDescription_ExportTitle,
            $i_InventorySystem_Item_CategoryName_ExportTitle,
            $i_InventorySystem_Item_Category2Name_ExportTitle,
            $i_InventorySystem_Item_GroupName_ExportTitle,
            $Lang['eInventory']['FieldTitle']['Export']['Building'],
            $i_InventorySystem_Item_LocationName_ExportTitle,
            $i_InventorySystem_Item_Location2Name_ExportTitle,
            $i_InventorySystem_Item_FundingSourceName_ExportTitle,
            $i_InventorySystem_Item_Ownership_ExportTitle,
            $i_InventorySystem_Item_BulkItemAdmin_ExportTitle,
            $i_InventorySystem_Item_Qty_ExportTitle,
            $i_InventorySystem_Item_Supplier_Name_ExportTitle,
            $i_InventorySystem_Item_Supplier_Contact_ExportTitle,
            $i_InventorySystem_Item_Supplier_Description_ExportTitle,
            $i_InventorySystem_Item_Quot_Num_ExportTitle,
            $i_InventorySystem_Item_Tender_Num_ExportTitle,
            $i_InventorySystem_Item_Invoice_Num_ExportTitle,
            $i_InventorySystem_Item_Purchase_Date_ExportTitle,
            $i_InventorySystem_Item_Price_ExportTitle,
            $i_InventorySystem_Unit_Price_ExportTitle,
            $i_InventorySystemItemMaintainInfo_ExportTitle,
            $i_InventorySystem_Item_Remark_ExportTitle,
            "Stocktake Option",
            "Tags"
        );
    }
    /*
     * $sql = "SELECT
     * a.ItemID,
     * a.ItemCode,
     * a.NameChi,
     * a.NameEng,
     * a.DescriptionChi,
     * a.DescriptionEng,
     * CASE a.Ownership
     * WHEN ".ITEM_OWN_BY_SCHOOL." THEN CONCAT('".$i_InventorySystem_Ownership_School."')
     * WHEN ".ITEM_OWN_BY_GOVERNMENT." THEN CONCAT('".$i_InventorySystem_Ownership_Government."')
     * WHEN ".ITEM_OWN_BY_DONOR." THEN CONCAT('".$i_InventorySystem_Ownership_Donor."')
     * END,
     * ".$linventory->getInventoryNameByLang("b.").",
     * ".$linventory->getInventoryNameByLang("c.").",
     * d.Quantity,
     * ".$linventory->getInventoryNameByLang("g.").",
     * ".$linventory->getInventoryNameByLang("f.").",
     * ".$linventory->getInventoryNameByLang("h.").",
     * ".$linventory->getInventoryNameByLang("i.")."
     * FROM
     * INVENTORY_ITEM AS a INNER JOIN
     * INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = 1 AND a.ItemType = 2) INNER JOIN
     * INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID AND a.RecordStatus = 1 AND a.ItemType = 2) LEFT OUTER JOIN
     * INVENTORY_ITEM_BULK_LOCATION AS d ON (a.ItemID = d.ItemID) LEFT OUTER JOIN
     * TEMP_INVENTORY_ITEM_BULK_FUNDING_LOG AS e ON (a.ItemID = e.ItemID AND d.LocationID = e.LocationID) LEFT OUTER JOIN
     * INVENTORY_LOCATION AS f ON (d.LocationID = f.LocationID) LEFT OUTER JOIN
     * INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
     * INVENTORY_ADMIN_GROUP AS h ON (d.GroupInCharge = h.AdminGroupID) LEFT OUTER JOIN
     * INVENTORY_FUNDING_SOURCE AS i ON (e.FundingSource = i.FundingSourceID)
     * WHERE
     * a.ItemType = 2
     * $cond_targetItem2
     * $cond_targetGroup2
     * $cond_Location2
     * $cond_SubLocation2
     * $cond_Group2
     * $cond_Funding2
     * $cond_Category2
     * $cond_SubCategory2
     * ";
     */
    
    $zero_cond = $display_zero ? "" : "and d.Quantity>0 ";
    
    // ## Set SQL Condition - Keyword ###
    if ($keyword != "") {
        if ((strpos($keyword, "*") == 0) && (strrpos($keyword, "*") == strlen($keyword) - 1)) {
            $barcode = substr($keyword, 1, strlen($keyword) - 2);
        }
        
        $keyword_cond = " AND ((a.NameChi LIKE '%$keyword%' OR a.NameEng LIKE '%$keyword%') OR
				(a.ItemCode LIKE '%$keyword%') OR
				(b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
				(c.NameChi LIKE '%$keyword%' OR c.NameEng LIKE '%$keyword%') OR
				
				(a.DescriptionChi LIKE '%$keyword%') OR
				(a.DescriptionEng LIKE '%$keyword%') OR 
				(bl.Barcode LIKE '%$keyword%') 
				) ";
        // (blog.Remark LIKE '%$keyword%') OR
    }
    
    $sql = "SELECT 
					a.ItemID,
					a.ItemCode,
					a.NameChi,
					a.NameEng,
					a.DescriptionChi,
					a.DescriptionEng,
					CASE a.Ownership 
						WHEN " . ITEM_OWN_BY_SCHOOL . " THEN CONCAT('" . $i_InventorySystem_Ownership_School . "')
						WHEN " . ITEM_OWN_BY_GOVERNMENT . " THEN CONCAT('" . $i_InventorySystem_Ownership_Government . "')
						WHEN " . ITEM_OWN_BY_DONOR . " THEN CONCAT('" . $i_InventorySystem_Ownership_Donor . "')
					END,
					" . $linventory->getInventoryNameByLang("b.") . ",
					" . $linventory->getInventoryNameByLang("c.") . ",
					d.Quantity,
					" . $linventory->getInventoryNameByLang("building.") . ",
					" . $linventory->getInventoryNameByLang("g.") . ",
					" . $linventory->getInventoryNameByLang("f.") . ",
					" . $linventory->getInventoryNameByLang("h.") . ",
					" . $linventory->getInventoryNameByLang("i.") . ",
					a.StockTakeOption,
					bl.Barcode
			FROM
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = 1 AND a.ItemType = 2) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID AND a.RecordStatus = 1 AND a.ItemType = 2) LEFT OUTER JOIN
					INVENTORY_ITEM_BULK_LOCATION AS d ON (a.ItemID = d.ItemID $location_condition1) LEFT OUTER JOIN
					INVENTORY_LOCATION AS f ON (d.LocationID = f.LocationID) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS g ON (f.LocationLevelID = g.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS building ON (building.BuildingID = g.BuildingID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS h ON (d.GroupInCharge = h.AdminGroupID) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS i ON (d.FundingSourceID = i.FundingSourceID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
			WHERE
					a.ItemType = 2
$cond_location					
$keyword_cond
					$cond_targetItem2
					$cond_targetGroup2
					$cond_Location2
					$cond_SubLocation2
					$cond_Building
					$cond_Group2
					$cond_Funding2
					$cond_Category2
					$cond_SubCategory2
					$zero_cond
					$cond_tag
			ORDER BY
					a.ItemCode
			";
    
    $arr_result = $linventory->returnArray($sql, 15);
    
    if (sizeof($arr_result) > 0) {
        for ($i = 0; $i < sizeof($arr_result); $i ++) {
            list ($itemID, $item_code, $item_chi_name, $item_eng_name, $item_chi_desc, $item_eng_desc, $item_ownership, $item_cat_code, $item_cat2_code, $item_quantity, $item_building_code, $item_location_level_code, $item_location_code, $item_group_code, $item_funding_code, $stocktakeoption, $item_barcode) = $arr_result[$i];
            
            // ## GET COST, UNIT PRICE, PURCHASED PRICE ###
            $total_school_cost = 0;
            $total_gov_cost = 0;
            $final_unit_price = 0;
            $final_purchase_price = 0;
            $total_qty = 0;
            $tmp_unit_price = 0;
            $tmp_purchase_price = 0;
            $written_off_qty = 0;
            $location_qty = 0;
            $ini_total_qty = 0;
            
            $sql = "SELECT " . $linventory->getInventoryNameByLang("b.") . " FROM INVENTORY_ITEM_BULK_EXT AS a INNER JOIN INVENTORY_ADMIN_GROUP AS b ON (a.BulkItemAdmin = b.AdminGroupID) WHERE a.ItemID = '".$itemID."'";
            $arr_tmp = $linventory->returnVector($sql, 1);
            $bulk_item_admin = $arr_tmp[0];
            
            // # get Total purchase price ##
            $sql = "SELECT SUM(PurchasedPrice) FROM INVENTORY_ITEM_BULK_LOG WHERE ItemID = '".$itemID."' AND Action = 1";
            $arr_result1 = $linventory->returnArray($sql, 1);
            if (sizeof($arr_result1) > 0) {
                list ($total_purchase_price) = $arr_result1[0];
            }
            
            // # get Total Qty Of the target Item ##
            $sql = "SELECT SUM(Quantity) FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$itemID."'";
            $arr_result1 = $linventory->returnArray($sql, 1);
            if (sizeof($arr_result1) > 0) {
                list ($totalQty) = $arr_result1[0];
            }
            // # get written-off qty ##
            $sql = "SELECT SUM(WriteOffQty) FROM INVENTORY_ITEM_WRITE_OFF_RECORD WHERE ItemID = '".$itemID."' AND RecordStatus=1";
            $tmp_result = $linventory->returnArray($sql, 1);
            if (sizeof($tmp_result) > 0) {
                list ($written_off_qty) = $tmp_result[0];
            }
            
            // # get the qty in the target location ##
            // $sql = "SELECT a.Quantity FROM INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) WHERE a.ItemID = $itemID AND b.Code = '$item_location_code'";
            $sql = "SELECT a.Quantity FROM INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) WHERE a.ItemID = '".$itemID."'";
            $arr_result1 = $linventory->returnArray($sql, 1);
            if (sizeof($arr_result1) > 0) {
                list ($location_qty) = $arr_result1[0];
            }
            
            /*
             * $sql = "SELECT
             * a.FundingSource,
             * b.FundingType
             * FROM
             * INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
             * INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID) INNER JOIN
             * INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID)
             * WHERE
             * a.ItemID = $itemID AND
             * a.Action IN (1,7) AND
             * c.Code = '$item_location_code' AND
             * a.FundingSource IS NOT NULL
             * ";
             */
            $sql = "SELECT 
							a.FundingSource, 
							b.FundingType
					FROM 
							INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
							INVENTORY_FUNDING_SOURCE AS b ON (a.FundingSource = b.FundingSourceID) INNER JOIN
							INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID)
					WHERE 
							a.ItemID = '".$itemID."' AND
							a.Action IN (1,7) AND
							a.FundingSource IS NOT NULL
					";
            $arr_result1 = $linventory->returnArray($sql, 2);
            if (sizeof($arr_result1) > 0) {
                list ($funding_source, $funding_type) = $arr_result1[0];
            }
            
            if ($funding_type == ITEM_SCHOOL_FUNDING) {
                // $final_purchase_price = $total_purchase_price;
                // $final_unit_price = $total_purchase_price/$totalQty;
                // $total_school_cost = $location_qty*($total_purchase_price/$totalQty);
                // $total_gov_cost = 0;
                $final_purchase_price = $total_purchase_price;
                $ini_total_qty = $totalQty + $written_off_qty;
                $final_unit_price = $total_purchase_price / $ini_total_qty;
                $total_school_cost = $ini_total_qty * ($total_purchase_price / $ini_total_qty);
            }
            if ($funding_type == ITEM_GOVERNMENT_FUNDING) {
                // $final_purchase_price = $total_purchase_price;
                // $final_unit_price = $total_purchase_price/$totalQty;
                // $total_gov_cost = $location_qty*($total_purchase_price/$totalQty);
                // $total_school_cost = 0;
                $final_purchase_price = $total_purchase_price;
                $ini_total_qty = $totalQty + $written_off_qty;
                $final_unit_price = $total_purchase_price / $ini_total_qty;
                $total_gov_cost = $ini_total_qty * ($total_purchase_price / $ini_total_qty);
            }
            
            // [case# X71702] [case# V72234]
            $final_purchase_price = $total_purchase_price;
            $ini_total_qty = $totalQty + $written_off_qty;
            $final_unit_price = $total_purchase_price / $ini_total_qty;
            /*
             * $sql = "SELECT
             * a.SupplierName,
             * a.SupplierContact,
             * a.SupplierDescription,
             * a.InvoiceNo,
             * a.QuotationNo,
             * a.TenderNo,
             * a.PurchaseDate,
             * a.MaintainInfo,
             * a.Remark
             * FROM
             * INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
             * INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID)
             * WHERE
             * a.ItemID = $itemID AND
             * a.Action = 1 AND
             * b.Code = '$item_location_code'
             * ";
             */
            $sql = "SELECT 
							a.SupplierName, 
							a.SupplierContact, 
							a.SupplierDescription, 
							a.InvoiceNo, 
							a.QuotationNo, 
							a.TenderNo, 
							a.PurchaseDate, 
							a.MaintainInfo, 
							a.Remark
					FROM
							INVENTORY_ITEM_BULK_LOG AS a INNER JOIN
							INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID)
					WHERE
							a.ItemID = '".$itemID."' AND 
							a.Action = 1
					";
            $arr_result1 = $linventory->returnArray($sql, 9);
            if (sizeof($arr_result1) > 0) {
                for ($j = 0; $j < sizeof($arr_result1); $j ++) {
                    list ($supplier_name, $supplier_contact, $supplier_desc, $invoice_no, $quotation_no, $tender_no, $purchase_date, $maintain_info, $remark) = $arr_result1[0];
                    if ($purchase_date == "0000-00-00")
                        $purchase_date = "";
                }
            } else {
                $supplier_name = "";
                $supplier_contact = "";
                $supplier_desc = "";
                $invoice_no = "";
                $quotation_no = "";
                $tender_no = "";
                $purchase_date = "";
                $maintain_info = "";
                $remark = "";
            }
            
            $tagsDisplay = $linventory->displayTags($itemID, false);
            
            $new_total = $item_quantity * $final_unit_price;
            $temprows = array(
                $item_code,
                $item_barcode,
                $item_chi_name,
                $item_eng_name,
                $item_chi_desc,
                $item_eng_desc,
                $item_cat_code,
                $item_cat2_code,
                $item_group_code,
                $item_building_code,
                $item_location_level_code,
                $item_location_code,
                $item_funding_code,
                $item_ownership,
                $bulk_item_admin,
                $item_quantity,
                $supplier_name,
                $supplier_contact,
                $supplier_desc,
                $quotation_no,
                $tender_no,
                $invoice_no,
                $purchase_date,
                $new_total,
                $final_unit_price,
                $maintain_info,
                $remark,
                $stocktakeoption,
                $tagsDisplay
            );
            $rows[] = $temprows;
            
            $file_content .= "\"$item_code\",\"$item_barcode\",\"$item_chi_name\",\"$item_eng_name\",\"$item_chi_desc\",\"$item_eng_desc\",\"$item_cat_code\",\"$item_cat2_code\",\"$item_group_code\",\"$item_location_level_code\",\"$item_location_code\",\"$item_funding_code\",\"$item_ownership\",\"$bulk_item_admin\",\"$item_quantity\",\"$supplier_name\",\"$supplier_contact\",\"$supplier_desc\",\"$quotation_no\",\"$tender_no\",\"$invoice_no\",\"$purchase_date\",\"$new_total\",\"$final_unit_price\",\"$maintain_info\",\"$remark\"\n";
        }
    }
    
    if (sizeof($arr_result) == 0) {
        $file_content .= "\"$i_no_record_exists_msg\"\n";
        
        $rows[] = array(
            $i_no_record_exists_msg
        );
    }
}
$display = $file_content;

intranet_closedb();

$filename = "item_details.csv";

// if($g_encoding_unicode){
// $export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
// $export_content .= $lexport->GET_EXPORT_TXT($rows, $exportColumn, ",", "\r\n", ",", 0, "11");
// $lexport->EXPORT_FILE($filename, $export_content);
// $export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn, "\t", "\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);
// }else output2browser($display,$filename);

?>