<?php
// modifying : 

/**
 * ******************************** Change Log ********************************************
 * Date : 2019-08-06 (Cameron)
 * - add checking for wrong type (i.e. update Single Item) [case #A162217]
 * - add continue to stop remaining checking once first error is encounter in each loop
 * Date : 2018-06-11 (Henry)
 * add remark for the information that will not update [Case#A138685]
 * Date : 2018-05-18 (Isaac)
 * added str_replace to $item_unit_price and $item_purchase_price to remove ',' from the price parameters. 
 * Date : 2018-05-14 (Henry)
 * auto calcluate the unit price if the unit price is zero [Case#A138685]
 * Date : 2018-02-07 (Henry)
 * add access right checking [Case#E135442]
 * Date : 2016-04-07 (Henry)
 * modified checking the purchase date (SKH)
 * Date : 2016-02-04 (Henry)
 * php 5.4 issue move 'if($format == "" || $format == ITEM_TYPE_BULK)' after includes file
 * Date : 2014-12-23 (Henry) [Case#J73005]
 * $sys_custom['eInventory']['PurchaseDateRequired'] > Purchase date cannot be empty
 *
 * Date : 2014-04-03 (YatWoon)
 * enlarge the temp table's item code length from varchar(25) to varchar(100)
 * $sys_custom['eInventoryCustForSKH'] > Purchase date cannot be empty
 * Date: 2013-09-25 (YatWoon)
 * Fix - no need to check building code for bulk update
 * Date: 2013-06-14 (Carlos)
 * Fix - Miss inserting Barcode to TEMP table
 * Date: 2013-03-25 (Carlos)
 * add Barcode data field
 * Date : 2013-03-22 (YatWoon)
 * support date format YYYY-MM-DD or D/M/YYYY or DD/MM/YYYY [Case#2013-0319-0942-52158]
 * Date : 2013-02-27 (YatWoon)
 * change float to double(20,2) for TEMP TABLE [Case#2013-0226-1009-47073]
 * Date : 2013-02-20 (YatWoon)
 * no need generate item code for bulk item if the data in csv is empty
 * don't display Variance manager if the data in csv is empty [Case#2013-0115-0922-11066]
 * Date : 2012-12-18 (YatWoon)
 * display "-" for purchase date if "0000-00-00"
 * Date : 2012-12-12 (Yuen)
 * added Tag field
 * Date : 2012-06-29 (Henry Chow)
 * allow user to add quantity by "Import > Insert bulk item" [CRM : 2012-0620-1102-42071]
 * Date : 2012-04-02 (Henry Chow)
 * check item in location (with same Funding Source ID & Resource Mgmt Group ID)
 * *****************************************************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");

// this page only handle import of bulk item
if ($format == "" || $format == ITEM_TYPE_BULK) {
    header("Location: import_item.php");
    exit();
}

include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libimporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();
$limport = new libimporttext();
$li = new libdb();
$lo = new libfilesystem();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$filepath = $itemfile;
$filename = $itemfile_name;

// navigation bar
$infobar .= "<tr><td colspan=\"2\" class=\"navigation_v30\">
				<a href=\"import_item.php\">" . $button_import . "</a>
				<span>" . $i_InventorySystem_ItemType_Bulk . " (" . ($actionType == 1 ? $Lang['eInventory']['Insert'] : $Lang['eInventory']['Update']) . ")</span>
			</td></tr>\n";

$file_format = array(
    "Item Code",
    "Barcode",
    "Item Chinese Name",
    "Item English Name",
    "Chinese Description",
    "English Description",
    "Category Code",
    "Sub-category Code",
    "Group Code",
    "Building Code",
    "Location Code",
    "Sub-location Code",
    "Funding Source Code",
    "Ownership",
    "Variance Manager",
    "Quantity",
    "Supplier",
    "Supplier Contact",
    "Supplier Description",
    "Quotation No",
    "Tender No",
    "Invoice No",
    "Purchase Date",
    "Total Purchase Amount",
    "Unit Price",
    "Maintanence Details",
    "Remarks",
    "Stocktake Option",
    "Tags"
);

$sql = "DROP TABLE TEMP_INVENTORY_ITEM";
$li->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS TEMP_INVENTORY_ITEM
		(
		 ItemType int(11),
		 CategoryID int(11),
		 Category2ID int(11),
		 CategoryCode varchar(10),
		 Category2Code varchar(10),
		 NameChi varchar(255),
		 NameEng varchar(255),
		 DescriptionChi text,
		 DescriptionEng text,
		 ItemCode varchar(100),
		 Barcode varchar(100),
		 Ownership int(11),
		 GroupInChargeCode varchar(10),
		 BuildingCode varchar(10),
		 LocationLevelCode varchar(10),
		 LocationCode varchar(10),
		 FundingSourceCode varchar(10),
		 VarianceManager varchar(10),
		 Quantity int(11),
		 ExistItem int(11),
		 SupplierName varchar(255),
		 SupplierContact text,
		 SupplierDescription text,
		 QuotationNo varchar(255),
		 TenderNo varchar(255),
		 InvoiceNo varchar(255),
		 PurchaseDate date,
		 PurchasedPrice double(20,2),
		 UnitPrice double(20,2),
		 MaintainInfo text,
		 Remarks text,
		 StockTakeOption varchar(16),
		 Tags text
	    )ENGINE=InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql);

// Enlarge item code size to varchar(100), same as INVENTORY_ITEM table
$sql = "alter table TEMP_INVENTORY_ITEM change ItemCode ItemCode varchar(100)";
$li->db_db_query($sql);

$ext = strtoupper($lo->file_ext($filename));

if ($ext != ".CSV" && $ext != ".TXT") {
    header("location: import_item.php?msg=15");
    exit();
}

if ($limport->CHECK_FILE_EXT($filename)) {
    // read file into array
    // return 0 if fail, return csv array if success
    $data = $limport->GET_IMPORT_TXT($filepath);
    $col_name = array_shift($data); // drop the title bar
                                    
    // check the csv file's first row is correct or not
    $format_wrong = false;
    for ($i = 0; $i < sizeof($file_format); $i ++) {
        if ($col_name[$i] != $file_format[$i]) {
            $format_wrong = true;
            break;
        }
    }
    if ($format_wrong) {
        header("location: import_item.php?msg=15");
        exit();
    }
    
    // ## Remove Empty Row in CSV File ###
    for ($i = 0; $i < sizeof($data); $i ++) {
        if (sizeof($data[$i]) != 0) {
            $arr_new_data[] = $data[$i];
            $record_row ++;
        } else {
            $empty_row ++;
        }
    }
    
    $file_original_row = sizeof($data);
    $file_new_row = sizeof($arr_new_data);
    
    // Get existing bulk item details
    $sql = "SELECT ItemID, ItemCode	FROM INVENTORY_ITEM WHERE ItemType = '" . ITEM_TYPE_BULK . "'";
    $arr_exist_item = $linventory->returnArray($sql, 2);
    
    for ($i = 0; $i < sizeof($arr_exist_item); $i ++) {
        list ($exist_item_id, $exist_item_code) = $arr_exist_item[$i];
        $exist_item[$exist_item_code]['ItemID'] = $exist_item_id;
    }
    
    // validate the row of records (determinate
    for ($i = 0; $i < sizeof($arr_new_data); $i ++) {
        list ($item_code, $item_barcode, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_categoryCode, $item_category2Code, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode, $item_ownership, $item_variance_manager, $item_qty, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_unit_price, $item_maintain_info, $item_remarks, $stocktakeoption, $tags) = $arr_new_data[$i];
        
        $item_unit_price = str_replace(',','',$item_unit_price);
		$item_purchase_price = str_replace(',','',$item_purchase_price);
        $item_code = trim($item_code);
        $item_barcode = trim($item_barcode);
        $item_chi_name = trim($item_chi_name);
        $item_eng_name = trim($item_eng_name);
        $item_chi_description = trim($item_chi_description);
        $item_eng_description = trim($item_eng_description);
        $item_categoryCode = trim($item_categoryCode);
        $item_category2Code = trim($item_category2Code);
        $item_admin_group_code = trim($item_admin_group_code);
        $item_buildingCode = trim($item_buildingCode);
        $item_locationLevelCode = trim($item_locationLevelCode);
        $item_locationCode = trim($item_locationCode);
        $item_fundingCode = trim($item_fundingCode);
        $item_ownership = trim($item_ownership);
        $item_variance_manager = trim($item_variance_manager);
        $item_qty = trim($item_qty);
        $item_supplier = trim($item_supplier);
        $item_supplier_contact = trim($item_supplier_contact);
        $item_supplier_description = trim($item_supplier_description);
        $item_quotation_no = trim($item_quotation_no);
        $item_tender_no = trim($item_tender_no);
        $item_invoice_no = trim($item_invoice_no);
        $item_purchase_date = trim($item_purchase_date);
        $item_purchase_price = trim($item_purchase_price);
        $item_unit_price = trim($item_unit_price);
        $item_maintain_info = trim($item_maintain_info);
        $item_remarks = trim($item_remarks);
        
        /*
         * if(strpos($item_purchase_date,"/") > 0)
         * {
         * $PurchaseDateArray = Explode("/", $item_purchase_date);
         * $item_purchase_date = date("Y-m-d", mktime(0,0,0,$PurchaseDateArray [1],$PurchaseDateArray [0],$PurchaseDateArray [2]));
         * }
         *
         * if(strpos($item_purchase_date,"/") > 0)
         * {
         * $PurchaseDateArray = Explode("/", $item_purchase_date);
         * $item_purchase_date = date("Y-m-d", mktime(0,0,0,$PurchaseDateArray [1],$PurchaseDateArray [0],$PurchaseDateArray [2]));
         * }
         */
    }
    
    if (sizeof($arr_new_data) > 0) {
        
        foreach ($arr_new_data as $i => $ary) {
            
            list ($item_code, $item_barcode, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_categoryCode, $item_category2Code, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode, $item_ownership, $item_variance_manager, $item_qty, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_unit_price, $item_maintain_info, $item_remarks, $stocktakeoption, $tags) = $ary;
            
            $item_unit_price = str_replace(',','',$item_unit_price);
			$item_purchase_price = str_replace(',','',$item_purchase_price);
            $item_code = trim($item_code);
            $item_barcode = trim($item_barcode);
            $item_chi_name = trim($item_chi_name);
            $item_eng_name = trim($item_eng_name);
            $item_chi_description = trim($item_chi_description);
            $item_eng_description = trim($item_eng_description);
            $item_categoryCode = trim($item_categoryCode);
            $item_category2Code = trim($item_category2Code);
            $item_admin_group_code = trim($item_admin_group_code);
            $item_buildingCode = trim($item_buildingCode);
            $item_locationLevelCode = trim($item_locationLevelCode);
            $item_locationCode = trim($item_locationCode);
            $item_fundingCode = trim($item_fundingCode);
            $item_ownership = trim($item_ownership);
            $item_variance_manager = trim($item_variance_manager);
            $item_qty = trim($item_qty);
            $item_supplier = trim($item_supplier);
            $item_supplier_contact = trim($item_supplier_contact);
            $item_supplier_description = trim($item_supplier_description);
            $item_quotation_no = trim($item_quotation_no);
            $item_tender_no = trim($item_tender_no);
            $item_invoice_no = trim($item_invoice_no);
            $item_purchase_date = trim($item_purchase_date);
            $item_purchase_price = trim($item_purchase_price);
            $item_unit_price = trim($item_unit_price);
            $item_maintain_info = trim($item_maintain_info);
            $item_remarks = trim($item_remarks);
            
            $item_exist = ($actionType == 1) ? 0 : 1;
            
            // action = INSERT, and item code is empty
            if ($actionType == 1 && $item_code == "") {
                // don't generate any item code at the moment
                /*
                 * if(isset($existItemCategory[$item_categoryCode.$item_category2Code])) {
                 * $existItemCategory[$item_categoryCode.$item_category2Code] += 1;
                 * } else {
                 * $existItemCategory[$item_categoryCode.$item_category2Code] = 0;
                 * }
                 *
                 * $item_code = generateItemCode($item_categoryCode,$item_category2Code,$item_purchase_date,$item_admin_group_code,$item_locationCode,$item_fundingCode, $existItemCategory[$item_categoryCode.$item_category2Code]);
                 * $itemCodeAry[$i] = $item_code;
                 */
            } else {
                if ($actionType == 2 && $item_code == "") {
                    $error[$i]['type'] = 2;
                    continue;
                }
                else if ($actionType == 2 && $item_code != "") {
                    $sql = "SELECT i.ItemID FROM INVENTORY_ITEM i WHERE i.ItemCode = '" . $linventory->Get_Safe_Sql_Query($item_code) . "' AND i.ItemType=1";
                    $result = $linventory->returnVector($sql);
                    if (sizeof($result) > 0) {
                        $error[$i]['type'] = 55; // Item exists, but ItemType is Single Item
                        continue;
                    }
                }
            }

            if ($actionType == 1 && $item_barcode != "") {
                $sql = "SELECT Barcode FROM INVENTORY_ITEM_BULK_EXT WHERE Barcode='" . $linventory->Get_Safe_Sql_Query($item_barcode) . "'";
                $result = $linventory->returnVector($sql);
                if (sizeof($result) > 0) {
                    $error[$i]['type'] = 20; // Barcode already used
                    continue;
                }
            }

            if ($actionType == 2 && $item_code != "" && $item_barcode != "") {
                $sql = "SELECT e.Barcode FROM INVENTORY_ITEM_BULK_EXT as e 
						INNER JOIN INVENTORY_ITEM i ON i.ItemID=e.ItemID AND i.ItemCode <> '" . $linventory->Get_Safe_Sql_Query($item_code) . "' 
						WHERE e.Barcode='" . $linventory->Get_Safe_Sql_Query($item_barcode) . "'";
                $result = $linventory->returnVector($sql);
                if (sizeof($result) > 0) {
                    $error[$i]['type'] = 3; // Barcode already exists
                    continue;
                }
            }
            
            if ($actionType == 1) {
                if ($item_chi_name == "") {
                    $error[$i]['type'] = 5; // Item Chinese Name is empty
                    continue;
                }
                if ($item_eng_name == "") {
                    $error[$i]['type'] = 6; // Item English Name is empty
                    continue;
                }
            }
            
            $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "'";
            $tempResult = $linventory->returnVector($sql);
            $thisAdminGroupID = $tempResult[0];
            
            $sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode) . "' AND RecordStatus=1";
            $tempResult = $linventory->returnVector($sql);
            $thisFundingSourceID = $tempResult[0];
            
            $sql = "SELECT room.LocationID FROM INVENTORY_LOCATION_LEVEL as floor INNER JOIN INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID) WHERE floor.RecordStatus = 1 and room.RecordStatus = 1 AND floor.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationLevelCode) . "' and room.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "'";
            $tempResult = $linventory->returnVector($sql);
            $thisLocationID = $tempResult[0];
            
            // ## Check Category Code ###
            if ($actionType == 1) {
                if ($item_categoryCode != "") {
                    if ($item_category2Code != "") {
                        $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_categoryCode) . "' AND RecordStatus=1";
                        $arr_tmp_result = $linventory->returnVector($sql);
                        $cat_id = $arr_tmp_result[0];
                        
                        $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND RecordStatus=1";
                        $arr_tmp_result = $linventory->returnVector($sql);
                        $subcat_id = $arr_tmp_result[0];
                        
                        $sql = "SELECT Count(*) FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $cat_id AND Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND RecordStatus=1";
                        $arr_result = $linventory->returnVector($sql);
                        
                        $matched_row = $arr_result[0];
                        if ($matched_row == 0) {
                            $error[$i]['type'] = 30; // Invalid Category Code / Sub-category Code
                            continue;
                        } else {
                            // commented by Henry Chow on 2012-06-07 (when insert, no need to check existing item)
                            /*
                             * # category is different from existing item
                             * $sql = "SELECT COUNT(*) FROM INVENTORY_ITEM WHERE CategoryID='$cat_id' AND Category2ID='$subcat_id' AND ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."'";
                             * $tempResult = $linventory->returnVector($sql);
                             *
                             * if($tempResult[0]==0) {
                             * $error[$i]['type'] = 40;
                             * }
                             */
                        }
                    } else {
                        $error[$i]['type'] = 8; // Sub-category Code is empty
                        continue;
                    }
                } else {
                    $error[$i]['type'] = 27; // Category Code is empty
                    continue;
                }
            } else {
                if ($item_categoryCode != "") {
                    $sql = "SELECT c.CategoryID FROM INVENTORY_ITEM i INNER JOIN INVENTORY_CATEGORY c ON (i.CategoryID=c.CategoryID AND c.Code='" . $linventory->Get_Safe_Sql_Query($item_categoryCode) . "') WHERE i.ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "' AND c.RecordStatus=1";
                    $result = $linventory->returnVector($sql);
                    
                    if (sizeof($result) == 0) {
                        $error[$i]['type'] = 38; // cannot update Category Code
                        continue;
                    }
                }
            }
            
            if ($actionType == 1) {
                if ($item_admin_group_code != "") {
                    $sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "'";
                    $arr_result = $linventory->returnArray($sql, 1);
                    if (sizeof($arr_result) == 0) {
                        $error[$i]['type'] = 9; // Invalid Group Code
                        continue;
                    } else {
                        if (! $_SESSION['SSV_USER_ACCESS']['eAdmin-eInventory']) {
                            $sql = "SELECT UserID FROM INVENTORY_ADMIN_GROUP g INNER JOIN INVENTORY_ADMIN_GROUP_MEMBER m ON (m.AdminGroupID=g.AdminGroupID AND g.Code='" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "' AND m.UserID='$UserID') WHERE m.RecordType=1";
                            $arr_result = $linventory->returnVector($sql);
                            if (sizeof($arr_result) == 0) {
                                $error[$i]['type'] = 32; // User is not the leader of selected management group
                                continue;
                            }
                        }
                    }
                } else {
                    $error[$i]['type'] = 10; // Group Code is empty
                    continue;
                }
            }
            
            if ($item_buildingCode != "") {
                if ($item_locationLevelCode != "") {
                    if ($item_locationCode != "") {
                        $sql = "SELECT 
									DISTINCT room.Code
								FROM 
									INVENTORY_LOCATION_BUILDING as building INNER JOIN 
									INVENTORY_LOCATION_LEVEL as floor ON (building.BuildingID = floor.BuildingID) INNER JOIN 
									INVENTORY_LOCATION as room ON (floor.LocationLevelID = room.LocationLevelID)
								WHERE
									building.RecordStatus = 1 and floor.RecordStatus = 1 and room.RecordStatus = 1 AND 
									building.Code = '" . $linventory->Get_Safe_Sql_Query($item_buildingCode) . "' and floor.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationLevelCode) . "' and room.Code = '" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "'";
                        $arr_result = $linventory->returnVector($sql);
                        if ($arr_result[0] != $item_locationCode) {
                            $error[$i]['type'] = 11; // Invalid Location Code / Sub-location Code
                            continue;
                        }
                    } else {
                        $error[$i]['type'] = 29; // Sub-location Code is empty
                        continue;
                    }
                } else {
                    $error[$i]['type'] = 12; // Location Code is empty
                    continue;
                }
            } else {
                if ($actionType == 1) {
                    // ## error - Building Code error ###
                    $error[$i]['type'] = 31; // Building Code empty
                    continue;
                }
            }
            
            if ($actionType == 1) {
                if ($item_fundingCode != "") {
                    $sql = "SELECT Code FROM INVENTORY_FUNDING_SOURCE WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode) . "' AND RecordStatus=1";
                    $arr_result = $linventory->returnArray($sql, 1);
                    if (sizeof($arr_result) == 0) {
                        $error[$i]['type'] = 13; // Invalid Funding Source Code
                        continue;
                    }
                } else {
                    $error[$i]['type'] = 14; // Funding Source Code is empty
                    continue;
                }
            }
            
            if ($actionType == 1) {
                if ($item_ownership == "") {
                    $error[$i]['type'] = 15; // Ownership Code is empty
                    continue;
                } else {
                    if ($item_ownership != 1) {
                        if ($item_ownership != 2) {
                            if ($item_ownership != 3) {
                                $error[$i]['type'] = 17; // Invalid Ownership Code
                                continue;
                            }
                        }
                    }
                }
            }
            
            if ($actionType == 1 && $item_variance_manager != "") {
                $sql = "SELECT Code FROM INVENTORY_ADMIN_GROUP WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_variance_manager) . "'";
                
                $arr_result = $linventory->returnArray($sql, 1);
                if (sizeof($arr_result) == 0) {
                    $error[$i]['type'] = 25; // Invalid Variance Manager
                    continue;
                }
            }
            
            if ($actionType == 1 && $item_qty == "") {
                $error[$i]['type'] = 16; // Quantity is empty
                continue;
            } else {
                if ($actionType == 1) {
                    if ($item_qty <= 0) {
                        $error[$i]['type'] = 18; // Invalid Quantity
                        continue;
                    }
                    if (strpos($item_qty, ".") != 0) {
                        $error[$i]['type'] = 18; // Invalid Quantity
                        continue;
                    }
                }
                
                // prepare IDs for further checking on existing item
                //
                $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_admin_group_code) . "'";
                $tempResult = $linventory->returnVector($sql);
                $thisAdminGroupID = $tempResult[0];
                
                $sql = "SELECT FundingSourceID FROM INVENTORY_FUNDING_SOURCE WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_fundingCode) . "' AND RecordStatus=1";
                $tempResult = $linventory->returnVector($sql);
                $thisFundingSourceID = $tempResult[0];
                
                if ($actionType == 1) {
                    // if "INSERT" bulk item, return error msg if item existed in selected location (is item exists in same location with same Funding Source ID and Resources Mgmt Group?)
                    // $sql = "SELECT SUM(bl.Quantity) FROM INVENTORY_ITEM_BULK_LOCATION bl INNER JOIN INVENTORY_ITEM t ON (t.ItemID=bl.ItemID AND t.ItemCode='".$linventory->Get_Safe_Sql_Query($item_code)."') INNER JOIN INVENTORY_LOCATION loc ON (loc.LocationID=bl.LocationID AND loc.Code='".$linventory->Get_Safe_Sql_Query($item_locationCode)."') INNER JOIN INVENTORY_LOCATION_LEVEL lvl ON (loc.LocationLevelID=lvl.LocationLevelID AND lvl.Code='".$linventory->Get_Safe_Sql_Query($item_locationLevelCode)."') INNER JOIN INVENTORY_LOCATION_BUILDING bu ON (bu.BuildingID=lvl.BuildingID AND bu.Code='".$linventory->Get_Safe_Sql_Query($item_buildingCode)."')";
                    
                    $sql = "SELECT SUM(bl.Quantity) FROM INVENTORY_ITEM_BULK_LOCATION bl INNER JOIN INVENTORY_ITEM t ON (t.ItemID=bl.ItemID AND t.ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "' AND bl.GroupInCharge='$thisAdminGroupID' AND bl.FundingSourceID='$thisFundingSourceID') INNER JOIN INVENTORY_LOCATION loc ON (loc.LocationID=bl.LocationID AND loc.Code='" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "') INNER JOIN INVENTORY_LOCATION_LEVEL lvl ON (loc.LocationLevelID=lvl.LocationLevelID AND lvl.Code='" . $linventory->Get_Safe_Sql_Query($item_locationLevelCode) . "') INNER JOIN INVENTORY_LOCATION_BUILDING bu ON (bu.BuildingID=lvl.BuildingID AND bu.Code='" . $linventory->Get_Safe_Sql_Query($item_buildingCode) . "')";
                    $sumQty = $linventory->returnVector($sql);
                    
                    // existing item in selected location (check single item only) since now allow add quantity and invoice data of bulk item by "Insert"
                    /*
                     * if($sumQty[0]>=1) {
                     * $error[$i]['type'] = 34;
                     * $item_exist = 1;
                     * }
                     */
                } else 
                    if ($actionType == 2 && $item_locationCode != "") {
                        // if "UPDATE" bulk item, return error msg if item DOES NOT existed in selected location
                        $sql = "SELECT SUM(bl.Quantity) FROM INVENTORY_ITEM_BULK_LOCATION bl INNER JOIN INVENTORY_ITEM t ON (t.ItemID=bl.ItemID AND t.ItemCode='" . $linventory->Get_Safe_Sql_Query($item_code) . "' AND bl.GroupInCharge='$thisAdminGroupID' AND bl.FundingSourceID='$thisFundingSourceID') INNER JOIN INVENTORY_LOCATION loc ON (loc.LocationID=bl.LocationID AND loc.Code='" . $linventory->Get_Safe_Sql_Query($item_locationCode) . "') INNER JOIN INVENTORY_LOCATION_LEVEL lvl ON (loc.LocationLevelID=lvl.LocationLevelID AND lvl.Code='" . $linventory->Get_Safe_Sql_Query($item_locationLevelCode) . "') INNER JOIN INVENTORY_LOCATION_BUILDING bu ON (bu.BuildingID=lvl.BuildingID AND bu.Code='" . $linventory->Get_Safe_Sql_Query($item_buildingCode) . "')";
                        
                        $sumQty = $linventory->returnVector($sql);
                        // echo $sql.'<br>';
                        if ($sumQty[0] == 0 || $sumQty[0] == "") { // no existing item in selected location
                            $error[$i]['type'] = 33;
                            $item_exist = 0;
                            continue;
                        }
                    }
            }
            
            if ($actionType == 1) {
                // check purchase date (SKH)
                if ($sys_custom['eInventoryCustForSKH'] || $sys_custom['eInventory']['PurchaseDateRequired']) {
                    if ($item_purchase_date == "" || ! checkDateIsValid(getDefaultDateFormat($item_purchase_date))) {
                        $error[$i]['type'] = 53;
                        continue;
                    }
                }
            }
            
            // check purchase price is empty or not #
            if ($item_purchase_price == "") {
                $item_purchase_price = 0;
            } else {
                if ($item_purchase_price < 0) {
                    $error[$i]['type'] = 22; // Invalid Total Purchase Amount
                    continue;
                } else {
                    if (strpos($item_purchase_price, ".") > 0) {
                        $tmp_purchase_price = substr($item_purchase_price, strpos($item_purchase_price, "."));
                        if (strlen($tmp_purchase_price) > 4) {
                            $error[$i]['type'] = 22; // Invalid Total Purchase Amount
                            continue;
                        }
                    }
                }
            }
            
            // check unit price is empty or not #
            if ($item_unit_price == "") {
                $item_unit_price = 0;
            } else {
                if ($item_unit_price < 0) {
                    $error[$i]['type'] = 24; // Invalid Unit Price
                    continue;
                } else {
                    if (strpos($item_unit_price, ".") > 0) {
                        $tmp_unit_price = substr($item_unit_price, strpos($item_unit_price, "."));
                        if (strlen($tmp_unit_price) > 4) {
                            $error[$i]['type'] = 24; // Invalid Unit Price
                            continue;
                        }
                    }
                }
            }
            
            if ($actionType == 1 && $item_unit_price == 0) {
            	$isAutoCalcuateUnitPrice[$i] = true;
            	$item_unit_price = round($item_purchase_price / $item_qty, 2);
            }
            
            $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY WHERE Code = '" . $linventory->Get_Safe_Sql_Query($item_categoryCode) . "' AND RecordStatus=1";
            $tmp_category_id = $linventory->returnVector($sql);
            $category_id = $tmp_category_id[0];
            
            $sql = "SELECT Category2ID FROM INVENTORY_CATEGORY_LEVEL2 AS a INNER JOIN INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND b.CategoryID = $category_id) WHERE a.Code = '" . $linventory->Get_Safe_Sql_Query($item_category2Code) . "' AND a.RecordStatus=1 AND b.RecordStatus=1";
            $tmp_category2_id = $linventory->returnVector($sql);
            $category2_id = $tmp_category2_id[0];
            
            $item_chi_name = intranet_htmlspecialchars(addslashes($item_chi_name));
            $item_eng_name = intranet_htmlspecialchars(addslashes($item_eng_name));
            $item_chi_description = intranet_htmlspecialchars(addslashes($item_chi_description));
            $item_eng_description = intranet_htmlspecialchars(addslashes($item_eng_description));
            // $item_brand = intranet_htmlspecialchars(addslashes($item_brand));
            $item_supplier = intranet_htmlspecialchars(addslashes($item_supplier));
            $item_supplier_contact = intranet_htmlspecialchars(addslashes($item_supplier_contact));
            $item_supplier_description = intranet_htmlspecialchars(addslashes($item_supplier_description));
            $item_maintain_info = intranet_htmlspecialchars(addslashes($item_maintain_info));
            $item_remarks = intranet_htmlspecialchars(addslashes($item_remarks));
            $item_categoryCode = $linventory->Get_Safe_Sql_Query($item_categoryCode);
            $item_category2Code = $linventory->Get_Safe_Sql_Query($item_category2Code);
            $item_code = $linventory->Get_Safe_Sql_Query($item_code);
            $item_barcode = $linventory->Get_Safe_Sql_Query($item_barcode);
            $item_admin_group_code = $linventory->Get_Safe_Sql_Query($item_admin_group_code);
            $item_buildingCode = $linventory->Get_Safe_Sql_Query($item_buildingCode);
            $item_locationLevelCode = $linventory->Get_Safe_Sql_Query($item_locationLevelCode);
            $item_locationCode = $linventory->Get_Safe_Sql_Query($item_locationCode);
            $item_fundingCode = $linventory->Get_Safe_Sql_Query($item_fundingCode);
            $item_variance_manager = $linventory->Get_Safe_Sql_Query($item_variance_manager);
            $item_license = $linventory->Get_Safe_Sql_Query($item_license);
            $item_serial = $linventory->Get_Safe_Sql_Query($item_serial);
            $item_quotation_no = $linventory->Get_Safe_Sql_Query($item_quotation_no);
            $item_tender_no = $linventory->Get_Safe_Sql_Query($item_tender_no);
            $item_invoice_no = $linventory->Get_Safe_Sql_Query($item_invoice_no);
            $stocktakeoption_value = $linventory->parseStockTakeOption($stocktakeoption);
            $tags_value = $linventory->Get_Safe_Sql_Query($tags);
            $item_purchase_date = $linventory->Get_Safe_Sql_Query(getDefaultDateFormat($item_purchase_date));
            
            $sql = "INSERT INTO TEMP_INVENTORY_ITEM
							(ItemType, CategoryID, Category2ID, CategoryCode, Category2Code, NameChi, NameEng, DescriptionChi, DescriptionEng, ItemCode, Barcode, Ownership, GroupInChargeCode, BuildingCode, LocationLevelCode, LocationCode, FundingSourceCode, VarianceManager, Quantity, ExistItem, SupplierName, SupplierContact, SupplierDescription, QuotationNo, TenderNo, InvoiceNo, PurchaseDate, PurchasedPrice, UnitPrice, MaintainInfo, Remarks, StockTakeOption, Tags)
					VALUES 
						('" . ITEM_TYPE_BULK . "','$category_id','$category2_id','$item_categoryCode','$item_category2Code','$item_chi_name','$item_eng_name','$item_chi_description','$item_eng_description','$item_code','$item_barcode','$item_ownership','$item_admin_group_code','$item_buildingCode','$item_locationLevelCode','$item_locationCode','$item_fundingCode', '$item_variance_manager', '$item_qty', '$item_exist','$item_supplier','$item_supplier_contact','$item_supplier_description','$item_quotation_no','$item_tender_no','$item_invoice_no','$item_purchase_date','$item_purchase_price','$item_unit_price','$item_maintain_info','$item_remarks', '$stocktakeoption_value', '$tags_value')";
            $linventory->db_db_query($sql);
            
            if ($item_code) {
                $sql = "SELECT COUNT(ItemCode) FROM TEMP_INVENTORY_ITEM 
				WHERE 
				ItemCode='" . ($item_code) . "' 
				and not (NameChi='" . ($item_chi_name) . "' and NameEng='" . ($item_eng_name) . "')
				";
                $arr_tmp_existing_ItemCode = $linventory->returnVector($sql);
                
                if ($arr_tmp_existing_ItemCode[0] > 0) {
                    $error[$i]['type'] = 19; // Item Code already used
                    continue;
                }
            }
            
            // check same item code, should be same variance group
            // item_variance_manager
            if ($item_code) {
                $sql = "SELECT distinct(VarianceManager) FROM TEMP_INVENTORY_ITEM 
				WHERE 
				ItemCode='" . ($item_code) . "' 
				";
                $arr_tmp_existing_VarianceManager = $linventory->returnVector($sql);
                
                if (sizeof($arr_tmp_existing_VarianceManager) > 1) {
                    $error[$i]['type'] = 25; // Invalid Variance Manager
                    continue;
                }
            }
            
            if ($actionType == 1 && $item_code) {
                // check same item code, should be same variance group (in existings records)
                $sql = "
					select 
						c.Code
					from 
						INVENTORY_ITEM as a
						left join INVENTORY_ITEM_BULK_EXT as b on (b.ItemID=a.ItemID)
						left join INVENTORY_ADMIN_GROUP as c on (c.AdminGroupID = b.BulkItemAdmin)
					where 
						a.ItemCode='" . ($item_code) . "'
				";
                $arr_tmp_existing_VarianceManager = $linventory->returnVector($sql);
                
                if (sizeof($arr_tmp_existing_VarianceManager) > 0) {
                    if ($arr_tmp_existing_VarianceManager[0] != $item_variance_manager) {
                        $error[$i]['type'] = 25; // Invalid Variance Manager
                        continue;
                    }
                }
            }
        }
    }

    // ## Show Import Result ###
    if (sizeof($error) > 0) {
        $a = array_keys($error);
        foreach ($a as $v) {
            $str .= $delim . ($v + 1);
            $delim = ", ";
        }
        $invalidRowMsg = $Lang['eInventory']['RowLine1'] . " " . $str . " " . $Lang['eInventory']['RowLine2'] . $Lang['eInventory']['InvalidRecordRow'];
        $invalidRowMsg = "<font color=\"red\">" . $Lang['eInventory']['InvalidRecord'] . " : $invalidRow ($invalidRowMsg)</font>";
    }
    
    $table_content .= "<tr>";
    $table_content .= "<td class=\"tabletext\" colspan=\"29\">
							$i_InventorySystem_ImportItem_TotalRow : $file_original_row<br>
							" . $Lang['eInventory']['ValidRecord'] . " : " . ($file_original_row - sizeof($error)) . "<br>
							$invalidRowMsg							
						</td>";
    $table_content .= "</tr>\n";
    // ## END ###
    
    $table_content .= "<tr class=\"tabletop\"><td class=\"tabletopnolink\">$i_InventorySystem_Item_Code</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem_Item_Barcode . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseName</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishName</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_ChineseDescription</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_EnglishDescription</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_CategoryCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Category2Code</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_GroupCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['BuildingCode'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['FloorCode'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eInventory']['FieldTitle']['RoomCode'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_FundingSourceCode</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Ownership</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_BulkItemAdmin</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Qty</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Name</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Contact</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Supplier_Description</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Quot_Num</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Tender_Num</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Invoice_Num</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Purchase_Date</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Price</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Unit_Price</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystemItemMaintainInfo</td>";
    $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Item_Remark</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $i_InventorySystem['StockTake'] . "</td>";
    $table_content .= "<td class=\"tabletopnolink\">" . $Lang['eDiscipline']['Tag'] . "</td>";
    
    if (sizeof($error) > 0) {
        $table_content .= "<td class=\"tabletopnolink\">$i_InventorySystem_Error</td>";
    }
    $table_content .= "</tr>\n";
    
    if (sizeof($error) == 0) {
        
        $sql = "SELECT 
					CategoryID, 
					Category2ID, 
					CategoryCode, 
					Category2Code, 
					NameChi, 
					NameEng, 
					IF(DescriptionChi='','-',DescriptionChi),
					IF(DescriptionEng='','-',DescriptionEng),
					ItemCode, 
					Barcode,
					Ownership, 
					GroupInChargeCode, 
					BuildingCode,
					LocationLevelCode,
					LocationCode, 
					FundingSourceCode, 
					VarianceManager,
					Quantity,
					IF(SupplierName='','-',SupplierName),
					IF(SupplierContact='','-',SupplierContact),
					IF(SupplierDescription='','-',SupplierDescription),
					IF(QuotationNo='','-',QuotationNo),
					IF(TenderNo='','-',TenderNo),
					IF(InvoiceNo='','-',InvoiceNo),
					IF(PurchaseDate = '0000-00-00', '-', PurchaseDate), 
					CONCAT('$',PurchasedPrice),
					CONCAT('$',UnitPrice),
					IF(MaintainInfo='','-',MaintainInfo),
					IF(Remarks='','-',Remarks),
					StockTakeOption,
					Tags
				FROM
					TEMP_INVENTORY_ITEM";
        
        $arr_result = $linventory->returnArray($sql, 31);
        
        if (sizeof($arr_result) > 0) {
        	$openFontColorTag = '';
        	$closeFontColorTag = '';
        	if($actionType == 2){
        		$openFontColorTag = '<font color="red">';
        		$closeFontColorTag = '</font>';
        	}
            for ($i = 0; $i < sizeof($arr_result); $i ++) {
                $j = $i + 1;
                if ($j % 2 == 0)
                    $table_row_css = " class=\"tablerow1\" ";
                else
                    $table_row_css = " class=\"tablerow2\" ";
                
                list ($category_id, $category2_id, $item_categoryCode, $item_category2Code, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_code_original, $item_barcode_original, $item_ownership, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode, $item_variance_manager, $item_qty, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_unit_price, $item_maintain_info, $item_remarks, $stocktakeoption, $tags) = $arr_result[$i];
                
                $item_unit_price = str_replace(',','',$item_unit_price);
				$item_purchase_price = str_replace(',','',$item_purchase_price);    
                $table_content .= "<tr $table_row_css><td class=\"tabletext\">".$openFontColorTag . ($item_code_original == "" ? $itemCodeAry[$i] : $item_code_original) . $closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_barcode_original.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_categoryCode.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_admin_group_code.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_buildingCode.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_locationLevelCode.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_locationCode.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_fundingCode.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">" . ($item_ownership ? $item_ownership : "") . "</td>";
                $table_content .= "<td class=\"tabletext\">$item_variance_manager</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_qty.$closeFontColorTag."</td>";
                // $table_content .= "<td class=\"tabletext\">$item_brand</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_supplier.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_supplier_contact.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_supplier_description.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_quotation_no.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_tender_no.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_invoice_no.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_purchase_date.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_purchase_price.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag;
                if ($isAutoCalcuateUnitPrice[$i]){
                	$table_content .= '<span class="tabletextrequire">*</span>';
                }
                $table_content .= $item_unit_price.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_maintain_info.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">".$openFontColorTag.$item_remarks.$closeFontColorTag."</td>";
                $table_content .= "<td class=\"tabletext\">$stocktakeoption</td>";
                $table_content .= "<td class=\"tabletext\">$tags</td>";
                $table_content .= "</tr>\n";
            }
        }
        $table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"29\"></td></tr>\n";
        /*
         * $table_content .= "<tr><td colspan=28 align=right>".
         * $linterface->GET_ACTION_BTN($button_submit, "submit", "")." ".
         * $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
         * "</td></tr>\n";
         */
    }
    
    if (sizeof($error) > 0) {
        
        for ($i = 0; $i < sizeof($arr_new_data); $i ++) {
            
            $j = $i + 1;
            if ($j % 2 == 0)
                $table_row_css = " class=\"tablerow1\" ";
            else
                $table_row_css = " class=\"tablerow2\" ";
            
            list ($item_code_original, $item_barcode_original, $item_chi_name, $item_eng_name, $item_chi_description, $item_eng_description, $item_categoryCode, $item_category2Code, $item_admin_group_code, $item_buildingCode, $item_locationLevelCode, $item_locationCode, $item_fundingCode, $item_ownership, $item_variance_manager, $item_qty, $item_supplier, $item_supplier_contact, $item_supplier_description, $item_quotation_no, $item_tender_no, $item_invoice_no, $item_purchase_date, $item_purchase_price, $item_unit_price, $item_maintain_info, $item_remarks, $stocktakeoption, $tags) = $arr_new_data[$i];
            
            $item_unit_price = str_replace(',','',$item_unit_price);
			$item_purchase_price = str_replace(',','',$item_purchase_price);
            
            if ($error[$i]["type"] == "") {
                
                $table_content .= "<tr $table_row_css><td class=\"tabletext\">" . ($item_code_original == "" ? $itemCodeAry[$i] : $item_code_original) . "</td>";
                $table_content .= "<td class=\"tabletext\">" . $item_barcode_original . "</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
                $table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
                $table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_ownership</td>";
                $table_content .= "<td class=\"tabletext\">$item_variance_manager</td>";
                $table_content .= "<td class=\"tabletext\">$item_qty</td>";
                // $table_content .= "<td class=\"tabletext\">$item_brand</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
                $table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
                $table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
                $table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
                $table_content .= "<td class=\"tabletext\">$item_remarks</td>";
                $table_content .= "<td class=\"tabletext\">$stocktakeoption</td>";
                $table_content .= "<td class=\"tabletext\">$tags</td>";
                $table_content .= "<td class=\"tabletext\"> - </td></tr>\n";
            }
            if ($error[$i]["type"] != "") {
                
                $table_content .= "<tr $table_row_css><td class=\"tabletext\">" . ($item_code_original == "" ? $itemCodeAry[$i] : $item_code_original) . "</td>";
                $table_content .= "<td class=\"tabletext\">" . $item_barcode_original . "</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_name</td>";
                $table_content .= "<td class=\"tabletext\">$item_chi_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_eng_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_categoryCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_category2Code</td>";
                $table_content .= "<td class=\"tabletext\">$item_admin_group_code</td>";
                $table_content .= "<td class=\"tabletext\">$item_buildingCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_locationLevelCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_locationCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_fundingCode</td>";
                $table_content .= "<td class=\"tabletext\">$item_ownership</td>";
                $table_content .= "<td class=\"tabletext\">$item_variance_manager</td>";
                $table_content .= "<td class=\"tabletext\">$item_qty</td>";
                // $table_content .= "<td class=\"tabletext\">$item_brand</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier_contact</td>";
                $table_content .= "<td class=\"tabletext\">$item_supplier_description</td>";
                $table_content .= "<td class=\"tabletext\">$item_quotation_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_tender_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_invoice_no</td>";
                $table_content .= "<td class=\"tabletext\">$item_purchase_date</td>";
                $table_content .= "<td class=\"tabletext\">$item_purchase_price</td>";
                $table_content .= "<td class=\"tabletext\">$item_unit_price</td>";
                $table_content .= "<td class=\"tabletext\">$item_maintain_info</td>";
                $table_content .= "<td class=\"tabletext\">$item_remarks</td>";
                $table_content .= "<td class=\"tabletext\">$stocktakeoption</td>";
                $table_content .= "<td class=\"tabletext\">$tags</td>";
                $table_content .= "<td class=\"tabletext\"><font color=\"red\">" . $i_InventorySystem_ImportError[$error[$i]["type"]] . "</font></td>\n";
            }
        }
        $table_content .= "<tr class=\"tablebottom\" height=\"20px\"><td colspan=\"30\"></td></tr>\n";
        /*
         * $table_content .= "<tr><td colspan=29 align=right>".
         * $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"").
         * "</td></tr>\n";
         */
    }
    
    if (count($isAutoCalcuateUnitPrice) > 0){
    	$table_content .= '<tr><td colspan="30" class="tabletextremark">'.$Lang['eInventory']['AutoCalcuateUnitPriceRemark'].'</td></tr>';
    }
    if (sizeof($error) == 0 && $actionType == 2){
    	$table_content .= '<tr><td colspan="30" class="tabletextremark">'.$Lang['eInventory']['RedColorInfoWillNotBeUpdated'].'</td></tr>';
    }
    
    $table_content .= "<tr><td colspan=30 align=right>";
    if (sizeof($error) == 0)
        $table_content .= $linterface->GET_ACTION_BTN($button_submit, "submit", "");
    $table_content .= " " . $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='import_item.php'", "cancelbtn", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") . "</td></tr>\n";
}
$TAGS_OBJ[] = array(
    $button_import,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<br>

<form name="form1" action="import_item_update.php" method="post">
	<table border="0" width="98%" cellspacing="0" cellpadding="5"
		align="center">
<?=$infobar;?>
</table>
	<br>
	<table border="0" width="96%" cellspacing="0" cellpadding="5"
		align="center">
<?=$table_content;?>
</table>
	<input type="hidden" name="format" id="format" value=<?=$format;?>>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
