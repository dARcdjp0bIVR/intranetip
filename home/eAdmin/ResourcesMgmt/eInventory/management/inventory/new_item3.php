<?php
/*
 * 	Log
 * 
 * 	Date:	2015-04-16 [Cameron] use double quote for string "$image_path/$LAYOUT_SKIN"
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libresource.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libbatch.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory		= new libinventory();
$li 			= new libresource($ResourceID[0]);
$bid 			= $li->TimeSlotBatchID;
$lb 			= new libbatch($li->TimeSlotBatchID);
$lo 			= new libgrouping();


$STEPS_OBJ[] = array($i_InventorySystem_AddNewItem_Step1, 0);
$STEPS_OBJ[] = array($i_InventorySystem_AddNewItem_Step2, 1);

$TAGS_OBJ[] = array($i_InventorySystem_Item_Registration, "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$BarcodeMaxLength = $linventory->getBarcodeMaxLength();
$BarcodeFormat = $linventory->getBarcodeFormat();

# show current title #
$temp[] = array($i_InventorySystem_ResourceItemsSetting,"");
$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$table_content .= "<input type=\"hidden\" name=\"item_total_qty\" value=\"$item_total_qty\">\n"; 
$table_content .= "<input type=\"hidden\" name=\"exist_item_code\" value=\"$exist_item_code\">\n"; 
$table_content .= "<input type=\"hidden\" name=\"purchase_type\" value=\"$purchase_type\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetCategory\" value=\"$targetCategory\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetCategory2\" value=\"$targetCategory2\">\n";
$table_content .= "<input type=\"hidden\" name=\"targetItemType\" value=\"$targetItemType\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_chi_name\" value=\"$item_chi_name\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_eng_name\" value=\"$item_eng_name\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_chi_discription\" value=\"$item_chi_discription\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_eng_discription\" value=\"$item_eng_discription\">\n";
$table_content .= "<input type=\"hidden\" name=\"bulk_item_admin\" value=\"$bulk_item_admin\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_ownership\" value=\"$item_ownership\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_photo\" value=\"$item_photo\">\n";
$table_content .= "<input type=\"hidden\" name=\"hidden_item_photo\" value=\"$hidden_item_photo\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_funding\" value=\"$item_funding\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_brand\" value=\"$item_brand\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_supplier\" value=\"$item_supplier\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_supplier_contact\" value=\"$item_supplier_contact\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_supplier_description\" value=\"$item_supplier_description\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_quotation\" value=\"$item_quotation\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_tender\" value=\"$item_tender\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_invoice\" value=\"$item_invoice\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_purchase_date\" value=\"$item_purchase_date\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_purchase_price\" value=\"$item_purchase_price\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_unit_price\" value=\"$item_unit_price\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_maintain_info\" value=\"$item_maintain_info\">\n";
$table_content .= "<input type=\"hidden\" name=\"item_remark\" value=\"$item_remark\">\n";

for($i=0; $i<$item_total_qty; $i++)
{
	$table_content .= "<input type=\"hidden\" name=\"item_code_$i\" value=\"${"item_code_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"item_barcode_$i\" value=\"".strtoupper(${"item_barcode_$i"})."\">\n";
	$table_content .= "<input type=\"hidden\" name=\"targetLocationLevel_$i\" value=\"${"targetLocationLevel_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"targetLocation_$i\" value=\"${"targetLocation_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"targetGroup_$i\" value=\"${"targetGroup_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"item_serial_$i\" value=\"${"item_serial_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"item_license_$i\" value=\"${"item_license_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"item_warranty_expiry_date_$i\" value=\"${"item_warranty_expiry_date_$i"}\">\n";
	$table_content .= "<input type=\"hidden\" name=\"item_warranty_expiry_date_$i\" value=\"${"item_warranty_expiry_date_$i"}\">\n";
}


if(sizeof($ResourceItem)>0)
{
	$sql = "SELECT 
				CONCAT(".$linventory->getInventoryNameByLang("a.").",' > ',".$linventory->getInventoryNameByLang("b.").") 
			FROM 
				INVENTORY_CATEGORY AS a INNER JOIN 
				INVENTORY_CATEGORY_LEVEL2 AS b ON (a.CategoryID = b.CategoryID)
			WHERE 
				b.Category2ID = $targetCategory2 AND
				b.CategoryID = $targetCategory
			";
	$arr_resource_category = $linventory->returnVector($sql);
	
	if(sizeof($arr_resource_category)>0)
	{
		$ResourceCategory = $arr_resource_category[0];
	}
	
	# table title #
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceCode</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceCategory</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceTitle</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceDaysBefore</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceTimeSlot</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceDescription</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceRemark</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceAutoApprove</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_ResourceRecordStatus</td>";
	$table_content .= "<td class=\"tabletopnolink\">$i_admintitle_group</td>";
	$table_content .= "</tr>";
	
	for($j=0; $j<sizeof($ResourceItem); $j++)
	{	
		/*
		//$table_content .= "<input type=\"hidden\" name=\"InventoryItemID\" value=\"$item_id\">";
		//$table_content .= "<input type=\"hidden\" name=\"ExistingResource\" value=\"0\">";
		*/
		if($j%2 == 0)
			$row_css = " class=\"tablerow1\" ";
		else
			$row_css = " class=\"tablerow2\" ";
			
		$arr_group_selection = $lo->returnSelectedCategoryGroupsFromType($ResourceID=0, "INTRANET_GROUPRESOURCE", "ResourceID", 0);
		if(sizeof($arr_group_selection)>0)
		{
			$group_selection = getSelectByArray($arr_group_selection,"name=\"GroupID_".$j."[]\" MULTIPLE",0,0,1);
		}

		$table_content .= "<tr $row_css>";
		$table_content .= "<td><input type=\"text\" name=\"resource_code_$j\" class=\"textboxtext2\" value=\"${"item_code_$j"}\"></td>\n";
		$table_content .= "<td><input type=\"text\" name=\"resource_category_$j\" class=\"textboxtext2\" value=\"$ResourceCategory\">".$li->displayResourceCategory("resource_category")."</td>\n";
		$table_content .= "<td><input type=\"text\" name=\"resource_name_$j\" class=\"textboxtext2\" value=\"$item_eng_name\"></td>\n";
		$table_content .= "<td><input type=\"text\" class=\"textboxnum\" name=\"day_in_advance_$j\" value=\"0\"></td>\n";
		$table_content .= "<td>".$lb->returnBatchSelect("name=batchID_$j")."<br><span class=\"tabletextremark\">[$i_ResourceViewScheme]</span></td>\n";
		$table_content .= "<td><textarea name=\"resource_description_$j\" class=\"textboxtext2\">$item_eng_discription</textarea></td>\n";
		$table_content .= "<td><textarea name=\"resource_remark_$j\" class=\"textboxtext2\"></textarea></td>\n";
		$table_content .= "<td><input type=\"checkbox\" name=\"auto_approve_$j\" value=\"yes\" CHECKED></td>\n";
		$table_content .= "<td><label class=\"tabletext\"><input type=\"radio\" name=\"resource_status_$j\" value=\"1\" CHECKED>&nbsp;$i_status_publish</label><br><label class=\"tabletext\"><input type=\"radio\" name=\"resource_status_$j\" value=\"0\">&nbsp;$i_status_pending</label></td>\n";
		$table_content .= "<td><label class=\"tabletext\">$group_selection</label></td>\n";
		$table_content .= "</tr>";
		$table_content .= "<input type=\"hidden\" name=\"SetAsResourceItem\" value=\"1\">\n";
		$table_content .= "<input type=\"hidden\" name=\"arrResourceItem[]\" value=\"${"item_code_$j"}\">";
	}
	$table_content .= "<input type=\"hidden\" name=\"no_of_resource_item\" value=\"".sizeof($ResourceItem)."\">\n";
}
		

?>

<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	var total_no = <?=sizeof($ResourceItem);?>;
	var check_code = 0;
	var check_category = 0;
	var check_name = 0;
	
	for(i=0; i<total_no; i++)
	{
		var tmp_1 = eval("document.form1.resource_code_"+i);
		var tmp_2 = eval("document.form1.resource_category_"+i);
		var tmp_3 = eval("document.form1.resource_name_"+i);
		if(tmp_1.value == "")
		{
			check_code++;
			tmp_1.focus();
			alert("Please input resource code");
			return false;
		}
		if(tmp_2.value == "")
		{
			check_category++;
			tmp_2.focus();
			alert("Please input resource category");
			return false;
		}
		if(tmp_3.value == "")
		{
			check_name++;
			tmp_3.focus();
			alert("Please input resource name");
			return false;
		}
	}
	
	if((check_code == 0) && (check_category == 0) && (check_name == 0))
	{
		obj.action = "new_item_update.php";
		return true;
	}
	else
	{
		return false;
	}
}
</script>

<br>
<form name="form1" action="" method="POST" onSubmit="return checkForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<?=$infobar?>
</table>
<br>
<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
<?=$table_content;?>

<tr><td colspan="10" class="dotline"><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10" height="1" /></td></tr>
<tr><td height="10px"></td></tr>
<tr><td colspan="10" align="center">
<?
	echo $linterface->GET_ACTION_BTN($button_submit, "submit")." ";
	echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")." ";
	echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location='items_full_list.php'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
?>
</td></tr>
</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>