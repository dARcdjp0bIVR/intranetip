<?
# using: 
/***************
 * Date: 2019-05-06 (Henry) 
 * Details: change post variable from tempItemID to ItemID
 * 
 * Date: 2016-02-01 (Henry) 
 * Details: PHP 5.4 fix: change split to explode
 * 
 * Date: 2013-01-30 (Rita)
 * Details: change to split
 * 
 **************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

intranet_auth();
intranet_opendb(); 

$linventory = new libinventory();

$tempItemID = $_REQUEST['ItemID'];
$type = $_RESQUEST['type'];

$itemIdArr = explode(':', $tempItemID);
$itemId = $itemIdArr[0];
$itemLocationId = $itemIdArr[1];


$numOfitemIdArr = count($itemIdArr);

if($numOfitemIdArr==4){
	$itemType = ITEM_TYPE_BULK;
	$groupInCharge =  $itemIdArr[2];
}
elseif($numOfitemIdArr==2){
	$itemType = ITEM_TYPE_SINGLE;
	$groupInCharge = $linventory->GET_ITEM_GROUP_IN_CHARGE($itemId, $itemLocationId);
	
}

$groupUserTypeArr = $linventory->getGroupUserType($groupInCharge);

$groupUserType = $groupUserTypeArr[0]['RecordType'];


echo $groupUserType;

intranet_closedb();
?>