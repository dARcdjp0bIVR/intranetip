<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage	= "Management_InventoryList";
$linterface 	= new interface_html();
$linventory	= new libinventory();

$TAGS_OBJ[] = array("View", "", 0);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$arr_report_type = array(array(1,"Category"),array(2,"Location"),array(3,"Group"),array(4,"Funding Source"));

$type_selection = getSelectByArray($arr_report_type, "name=targetType");

echo $type_selection;

?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>