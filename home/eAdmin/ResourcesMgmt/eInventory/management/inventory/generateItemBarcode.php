<?php
// using : 
/*
 * 2013-02-25 (Carlos): Moved generate item barcode function to lib
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$linterface = new interface_html();
$linventory	= new libinventory();

$arr_final_barcode = array();
/*
while(sizeof($arr_final_barcode)!=$total_item)
{
	$tmp_barcode = $linventory->random_barcode();
	$sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '$tmp_barcode'";
	$arr_tmp_barcode = $linventory->returnVector($sql);
	if((sizeof($arr_tmp_barcode)==0) && (!array_search($tmp_barcode,$arr_final_barcode)))
		array_push($arr_final_barcode,$tmp_barcode);
}

if(sizeof($arr_final_barcode)>0)
	$response = implode(",",$arr_final_barcode);

$response = iconv("Big5","UTF-8",$response);
*/
$arr_final_barcode = $linventory->getUniqueBarcode($total_item);
if(sizeof($arr_final_barcode)>0)
	$response = implode(",",$arr_final_barcode);

echo $response;

/*
for($i=0; $i<$total_item; $i++)
{
	while(!array_search($tmp_barcode,$arr_final_barcode)){
			$tmp_barcode = $linventory->random_barcode();
			$sql = "SELECT TagCode FROM INVENTORY_ITEM_SINGLE_EXT WHERE TagCode = '$tmp_barcode'";
			$arr_tmp_barcode = $linventory->returnArray($sql);
			echo $tmp_barcode."<BR>";
			$arr_final_barcode[$i] = $tmp_barcode;
	}
}
print_r($arr_final_barcode);
*
/*
$response = iconv("Big5","UTF-8",$item_barcode);

echo $response;
*/

?>