<?php
// modifying : 

/**
 * **************************** Change Log [Start] *******************************
 * 2020-06-04 (Tommy): fix: table has wrong extra items, modified sql for separating location of single and bulk items[Case#S186232]
 * 2019-05-13 (Henry): Security fix: SQL without quote
 * 2018-05-06 (Henry): change parameter tempItemID to ItemID when posting ajax_check_group_leader_right.php
 * 2018-03-22 (Isaac): add export invoice detail button [Case#Z123012]
 * 2018-02-07 (Henry): add access right checking [Case#E135442]
 * 2017-11-20 (Henry): fix: flash when mouse over image [Case#E131311]
 * 2017-06-20 (Henry): search function fix [Case#E118873]
 * 2014-09-30 (YatWoon): display "Export barcode" button again [H68345]
 * Deploy: IPv10.1
 * 2014-09-05 (YatWoon): hide "Export barcode" button [Case#G45973]
 * Deploy: IPv10.1
 * 2013-06-24 (Rita): change log to allow member edit and write-off [#2013-0617-1522-23073]
 * amend js checkDifferentGroupRightEdit()
 * 2013-06-06 (Rita): change back checkBulkRequestWriteOff to previous one
 * 2013-05-27 (YatWoon): add TrimWriteOffTag() function, avoid display the tag with write-off items only
 * 2013-05-18 (Rita): improve checking for leader to have bulk write-off item access
 * 2013-04-16 (YatWoon): support search for bulk item barcode
 * 2013-01-30 (Rita): Change js checking for customization - member update location
 * 2012-12-12 (Yuen): Support Tag
 * 2012-12-06 (Yuen): Keyword search involves descriptions in Chi & Eng
 * 2012-10-10 (Rita): Revise update location icon name to icon_star_setting_off, change access right setting
 * add checkDifferentGroupRightEdit() JS
 * 2012-09-26 (Rita): Revise access right of update location
 * 2012-09-14 (Rita): Add Update loction customization
 * 2012-09-12 (Rita): Add bulk delete table tool btn, change update status image, return message
 * 2012-07-12 (YatWoon): Improved: display 2 funding for single item
 * 2012-07-05 (YatWoon): Improved: display funding which is using by items
 * 2012-07-03 (YatWoon): Improved: display category/sub-category with distinct from INVENTORY_ITEM
 * 2011-06-04 (YatWoon): Improved: display record date
 * 2011-05-18 (YatWoon): Fixed: remove location bulk item will remove "ALL" same bulk item
 * 2011-04-19 (YatWoon): add option "display 0 record"
 * 2011-03-31 (Carlos) : add upload tool button
 * 2011-01-17 (YatWoon) : add cookies settings
 * ****************************** Change Log [End] ********************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

// #### add cookies settings [Start]
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_page_no",
    "pageNo"
);
$arrCookies[] = array(
    "ck_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_page_field",
    "field"
);

$arrCookies[] = array(
    "ck_eInventory_keyword",
    "keyword"
);
$arrCookies[] = array(
    "ck_eInventory_targetItemType",
    "targetItemType"
);
$arrCookies[] = array(
    "ck_eInventory_targetCategory",
    "targetCategory"
);
$arrCookies[] = array(
    "ck_eInventory_targetSubCategory",
    "targetSubCategory"
);
$arrCookies[] = array(
    "ck_eInventory_BuildingSelected",
    "BuildingSelected"
);
$arrCookies[] = array(
    "ck_eInventory_targetGroup",
    "targetGroup"
);
$arrCookies[] = array(
    "ck_eInventory_targetFunding",
    "targetFunding"
);
$arrCookies[] = array(
    "ck_eInventory_FloorSelected",
    "FloorSelected"
);
$arrCookies[] = array(
    "ck_eInventory_RoomSelected",
    "RoomSelected"
);
$arrCookies[] = array(
    "ck_eInventory_display_photo",
    "display_photo"
);
$arrCookies[] = array(
    "ck_eInventory_display_zero_tmp",
    "display_zero_tmp"
);
$arrCookies[] = array(
    "ck_eInventory_tag",
    "Tag"
);
if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

// #### add cookies settings [End]

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_InventoryList";
$linterface = new interface_html();
$linventory = new libinventory();
$llocation_ui = new liblocation_ui();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$TAGS_OBJ[] = array(
    $i_InventorySystem['FullList'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/items_full_list.php?clearCoo=1",
    1
);
$TAGS_OBJ[] = array(
    $i_InventorySystem['Category'],
    $PATH_WRT_ROOT . "home/eAdmin/ResourcesMgmt/eInventory/management/inventory/category.php",
    0
);

$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

$returnMsgKey = $_GET['returnMsgKey'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

// Generate System Message #
if ($xmsg != "") {
    $SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
    if ($msg == 1)
        $SysMsg = $linterface->GET_SYS_MSG("add");
    if ($msg == 2)
        $SysMsg = $linterface->GET_SYS_MSG("update");
    if ($msg == 3)
        $SysMsg = $linterface->GET_SYS_MSG("delete");
    if ($msg == 12)
        $SysMsg = $linterface->GET_SYS_MSG("add_failed");
    if ($msg == 13)
        $SysMsg = $linterface->GET_SYS_MSG("", "$i_InventorySystem_ItemFullList_DeleteItemFail");
    if ($msg == 14)
        $SysMsg = $linterface->GET_SYS_MSG("update_failed");
    if ($msg == 15)
        $SysMsg = $linterface->GET_SYS_MSG("", "$i_InventorySystem_Warning_SetBulkItemAsResourceItem");
}
// End #
$display_zero = $display_zero_tmp ? $display_zero_tmp : $display_zero;

// Generate an item list which belong to user group#
$sql = "SELECT 
				DISTINCT a.ItemID
		FROM 
				INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) 
		WHERE 
				a.GroupInCharge IN (" . $linventory->getInventoryAdminGroup() . ") AND 
				b.RecordStatus = 1";
$arr_tmp_result1 = $linventory->returnVector($sql);

$sql = "SELECT 
				DISTINCT a.ItemID
		FROM 
				INVENTORY_ITEM_BULK_LOCATION AS a INNER JOIN 
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) 
		WHERE 
				a.GroupInCharge IN (" . $linventory->getInventoryAdminGroup() . ") AND
				b.RecordStatus = 1";
$arr_tmp_result2 = $linventory->returnVector($sql);
$tmp_arr = array_merge($arr_tmp_result1, $arr_tmp_result2);
$targetItemList = implode(",", $tmp_arr);
// end #

// Create Search box #
$searchbox .= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_view.gif\">&nbsp;";
$searchbox .= "<input class=\"textboxnum\" type=\"text\" id=\"temp_keyword\" name=\"temp_keyword\" maxlength=\"50\" value=\"" . intranet_htmlspecialchars(stripslashes($keyword)) . "\" onFocus=\"this.form.pageNo.value=1;\" onKeyPress=\"return submitenter(this,event)\">&nbsp;\n";
// End #

// Generate Search Link #
$search_option .= $searchbox;
$search_option .= "<a class=\"tablelink\" href=\"search.php?targetSearch=2\">{$i_InventorySystem_Avanced_Search}</a>";
// End #

// Generate Toolbar #
if ($linventory->IS_ADMIN_USER($UserID) || $linventory->retriveGroupLeaderAddItemRight()) {
    if (($linventory->getAccessLevel($UserID) == 1) || ($linventory->getAccessLevel($UserID) == 2)) {
        $toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('new_item.php')", "", "", "", "", 0);
        $toolbar .= $linterface->GET_LNK_IMPORT("import_item.php", "", "", "", "", 0);
        $toolbar .= $linterface->GET_LNK_UPLOAD("./photo/upload.php", "", $Lang['eInventory']['UploadImages'], "", "", 0);
    }
}
// End #

// Generate Table Tool Bar - Edit / Detele #
if (($display_photo == "") || ($display_photo == 0)) {
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:showImage(1)\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pic_s.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$i_InventorySystem_DisplayAllImage
						</a>
					</td>";
}
if ($display_photo == 1) {
    $table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:showImage(0)\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pic_s.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_InventorySystem_HideAllImage
					</a>
				</td>";
}

$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkExport(document.form1,'ItemID[]','../barcode_labels/item_details.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$i_InventorySystem_ExportItemBarcode
					</a>
				</td>";



if (($linventory->getAccessLevel($UserID) == 1) || ($linventory->getAccessLevel($UserID) == 2)) {
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
    <a href=\"javascript:checkExportItemDetail(document.form1,'ItemID[]','invoice_detail_export.php')\" class=\"tabletool contenttool\">
    <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
    ".$Lang['eInventory']['ExportInvoice']."
					</a>
				</td>";
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkExportItemDetail(document.form1,'ItemID[]','items_detail_export.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$i_InventorySystem_ExportItemDetails1
						</a>
					</td>";
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkExportItemDetail(document.form1,'ItemID[]','items_detail_export2.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_export.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$i_InventorySystem_ExportItemDetails2
						</a>
					</td>";
}
if ($linventory->IS_ADMIN_USER($UserID)) {
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkRemove(document.form1,'ItemID[]','category_show_items_remove.php?cat_id=$cat_id&cat2_id=$cat2_id')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$button_remove
						</a>
					</td>";
}

if ($linventory->getAccessLevel($UserID) != 3) {
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'ItemID[]','category_show_items_edit.php')\" class=\"tabletool contenttool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
}

// # Update Status Button
// if($linventory->IS_ADMIN_USER($UserID) || $linventory->getAccessLevel($UserID)==2 || $linventory->IS_GROUP_HELPER()==true)
// {

$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkDifferentGroupRightEdit(document.form1,'ItemID[]','items_status_edit.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							$i_InventorySystem_Item_Update_Status
						</a>
					</td>";
// $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
// }

if ($linventory->IS_ADMIN_USER($UserID) || $linventory->getAccessLevel($UserID) == 2) {
    // Bulk Item Request WriteOff btn
    $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
    $table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkBulkRequestWriteOff(document.form1,'ItemID[]','bulk_items_request_write_off.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
							" . $Lang['eInventory']['FieldTitle']['BulkItemRequestWriteOff'] . "
						</a>
					</td>";
}

// Customization
// debug_pr($_SESSION["inventory"]["role"]);
// debug_pr($linventory->getAccessLevel($UserID));
// if ($linventory->enableMemberUpdateLocationRight())
// {
// if($linventory->IS_GROUP_HELPER()==true){
//
// $table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
// $table_tool .= "<td nowrap=\"nowrap\">
// <a href=\"javascript:checkDifferentGroupRightEdit(document.form1,'ItemID[]','items_location_edit.php', 'updateLocation')\" class=\"tabletool contenttool\">
// <img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
// ".$Lang['eInventory']['FieldTitle']['UpdateItemLocation']."
// </a>
// </td>";
// }
// }

$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
// End #

// # Location Selection - Building ##
$opt_LocationBuilding = $llocation_ui->Get_Building_Selection($BuildingSelected, "BuildingSelected", "resetLocationSelection('Building'); this.form.submit();", 0, 0, $Lang['SysMgr']['Location']['All']['Building']);
$location_selection = $opt_LocationBuilding;

// Generate Location Selection Box - Floor #
if ($BuildingSelected != "") {
    $opt_LocationLevel = $llocation_ui->Get_Floor_Selection($BuildingSelected, $FloorSelected, "FloorSelected", "resetLocationSelection('Floor'); this.form.submit();", 0, 0, $Lang['SysMgr']['Location']['All']['Floor']);
    $location_selection .= $opt_LocationLevel;
}
// Generate Location Selection Box - Room #
if (($FloorSelected != "") && ($BuildingSelected != "")) {
    $opt_Location = $llocation_ui->Get_Room_Selection($FloorSelected, $RoomSelected, "RoomSelected", "this.form.submit();", 0, $Lang['SysMgr']['Location']['All']['Room'], "");
    $location_selection .= $opt_Location;
}
// End #

$tagAry = returnModuleAvailableTag($linventory->ModuleName, 1);
$tagAry = $linventory->TrimWriteOffTag($tagAry);

if (is_array($tagAry) && sizeof($tagAry) > 0) {
    foreach ($tagAry as $key => $TagName) {
        $TagName = intranet_htmlspecialchars($TagName);
        $tagNameAry[] = array(
            $TagName,
            $TagName
        );
    }
    $tag_selection = $linterface->GET_SELECTION_BOX($tagNameAry, " name='Tag' id='Tag' onChange='this.form.submit();' ", $Lang["eInventory"]["SelectionAllTags"], intranet_htmlspecialchars(stripslashes($Tag)));
}

// Generate Resource Management Group Selection Box #
if ($linventory->IS_ADMIN_USER($UserID))
    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP ORDER BY DisplayOrder";
else
    $sql = "SELECT AdminGroupID, " . $linventory->getInventoryNameByLang() . " FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID IN (" . $linventory->getInventoryAdminGroup() . ") ORDER BY DisplayOrder";
$arr_group = $linventory->returnArray($sql, 2);

$opt_group = getSelectByArray($arr_group, " name=\"targetGroup\" onChange=\"this.form.submit();\" ", $targetGroup, 1, 0, $Lang['eInventory']['FieldTitle']['AllResourceMgmtGroup']);

$group_selection .= " $opt_group ";
// End #

// Generate Funding Selection Box #
/*
 * $sql = "SELECT
 * FundingSourceID,
 * ".$linventory->getInventoryNameByLang("")."
 * FROM
 * INVENTORY_FUNDING_SOURCE
 * ORDER BY
 * DisplayOrder";
 */
// single item
$sql = "select distinct(a.FundingSource) from INVENTORY_ITEM_SINGLE_EXT as a left join INVENTORY_ITEM as b on b.ItemID=a.ItemID where b.RecordStatus=1";
$single_funding_ids = $linventory->returnVector($sql);
$sql = "select distinct(a.FundingSource2) from INVENTORY_ITEM_SINGLE_EXT as a left join INVENTORY_ITEM as b on b.ItemID=a.ItemID and (a.FundingSource2 is not null && a.FundingSource2!=0) where b.RecordStatus=1";
$single_funding_ids2 = $linventory->returnVector($sql);
if (! empty($single_funding_ids2)) {
    $single_funding_ids = array_unique(array_merge($single_funding_ids, $single_funding_ids2));
}
// bulk item
$sql = "select distinct(FundingSourceID) from INVENTORY_ITEM_BULK_LOCATION ";
if (! $display_zero)
    $sql .= " where Quantity>0";
$bulk_funding_ids = $linventory->returnVector($sql);
$funding_ids = array_unique(array_merge($single_funding_ids, $bulk_funding_ids));
if (! empty($funding_ids)) {
    $funding_ids_str = implode(",", $funding_ids);
} else {
    $funding_ids_str = "-999";
}
$sql = "SELECT 
				FundingSourceID, 
				" . $linventory->getInventoryNameByLang("") . "
		FROM
				INVENTORY_FUNDING_SOURCE
		where 
				FundingSourceID in (" . $funding_ids_str . ")
		ORDER BY
				DisplayOrder";

$arr_funding = $linventory->returnArray($sql, 2);

$opt_funding = getSelectByArray($arr_funding, " name=\"targetFunding\" onChange=\"this.form.submit();\" ", $targetFunding, 1, 0, $Lang['eInventory']['FieldTitle']['AllFundingSource']);

$funding_selection .= " $opt_funding ";
// End #

// Generate Category Selection Box #
// $sql = "SELECT CategoryID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY ORDER BY DisplayOrder";
$sql = "SELECT 
			distinct(a.CategoryID), 
			" . $linventory->getInventoryNameByLang("b.") . " 
		FROM 
			INVENTORY_ITEM as a
			left join INVENTORY_CATEGORY as b on a.CategoryID = b.CategoryID
		ORDER BY b.DisplayOrder";
$arr_category = $linventory->returnArray($sql);
if ($sys_custom['eInventory_DefaultFilterNoAll']) {
    if ($targetCategory == "") {
        $sql = "SELECT CategoryID FROM INVENTORY_CATEGORY ORDER BY DisplayOrder LIMIT 0,1";
        $result = $linventory->returnVector($sql);
        $targetCategory = $result[0];
    }
    $opt_category = getSelectByArray($arr_category, "name=\"targetCategory\" onChange=\"this.form.submit();\"", $targetCategory, 1, 1);
} else {
    $opt_category = getSelectByArray($arr_category, "name=\"targetCategory\" onChange=\"this.form.submit();\"", $targetCategory, 1, 0, $Lang['eInventory']['FieldTitle']['AllCategory']);
}
$category_selection = $opt_category;
if ($targetCategory != "") {
    // $sql = "SELECT Category2ID, ".$linventory->getInventoryNameByLang()." FROM INVENTORY_CATEGORY_LEVEL2 WHERE CategoryID = $targetCategory ORDER BY DisplayOrder";
    $sql = "SELECT 
			distinct(a.Category2ID), 
			" . $linventory->getInventoryNameByLang("b.") . " 
		FROM 
			INVENTORY_ITEM as a
			left join INVENTORY_CATEGORY_LEVEL2 as b on a.Category2ID = b.Category2ID
		 WHERE b.CategoryID = '".$targetCategory."' 
		ORDER BY b.DisplayOrder";
    $arr_sub_category = $linventory->returnArray($sql);
    $opt_sub_category = getSelectByArray($arr_sub_category, "name=\"targetSubCategory\" onChange=\"this.form.submit();\"", $targetSubCategory, 1, 0);
    $category_selection .= $opt_sub_category;
}
// End #

// Genrate Item Type Selection Box #
if ($sys_custom['eInventory_DefaultFilterNoAll']) {
    // ## Custom Filter Bar ###
    if ($targetItemType == "") {
        $targetItemType = 1;
    }
    $item_type_selection = getSelectByArray($i_InventorySystem_ItemType_Array, "name=\"targetItemType\" onChange=\"this.form.submit();\"", $targetItemType, 1, 1);
} else {
    // ## Original Filter Bar ###
    $item_type_selection = getSelectByArray($i_InventorySystem_ItemType_Array, "name=\"targetItemType\" onChange=\"this.form.submit();\"", $targetItemType, 1, 0, $Lang['eInventory']['FieldTitle']['AllType']);
}
// End #

if (! isset($selectItemStatus)) {
    $selectItemStatus = 1;
}
$status_selection = "<SELECT name=\"selectItemStatus\" onChange=\"this.form.submit();\"><OPTION value=\"1\" " . ($selectItemStatus == 1 ? "SELECTED" : "") . ">$i_InventorySystem_ItemStatus_Normal</OPTION>
<OPTION value=\"0\" " . ($selectItemStatus == 0 ? "SELECTED" : "") . ">$i_InventorySystem_ItemStatus_WriteOff</OPTION>
</SELECT>";

$zero_checkbox = "<input type='checkbox' name='display_zero' value='1' id='display_zero' onClick='click_zero();' " . ($display_zero_tmp ? "checked" : "") . "> <label for ='display_zero'>" . $Lang['eInventory']['DisplayZeroRecord'] . "</label>";
$zero_checkbox .= "<input type='hidden' name='display_zero_tmp' value='" . $display_zero_tmp . "'>";

// Create Filter Bar
$filterbar .= $item_type_selection;
$filterbar .= $category_selection;
$filterbar .= $location_selection;
$filterbar .= $tag_selection;
$filterbar .= "<br>";
$filterbar .= $group_selection;
$filterbar .= $funding_selection;
$filterbar .= $zero_checkbox;

// $filterbar .= $status_selection;
// End #

// ## Set SQL Condition - Location ###
if ($BuildingSelected == "") {
    $sql = "SELECT 
					LocationID
			FROM
					INVENTORY_LOCATION
			WHERE
					RecordStatus = 1
			ORDER BY
					LocationLevelID, LocationID";
    $result = $linventory->returnVector($sql);
} else {
    if ($FloorSelected == "") {
        $sql = "SELECT
						c.LocationID
				FROM
						INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
						INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
				WHERE
						a.BuildingID = '".$BuildingSelected."' AND
						a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
        $result = $linventory->returnVector($sql);
    } else {
        if ($RoomSelected == "") {
            $sql = "SELECT
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = '".$BuildingSelected."' AND
							b.LocationLevelID = '".$FloorSelected."' AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = '".$BuildingSelected."' AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        } else {
            
            $sql = "SELECT 
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = '".$BuildingSelected."' AND
							b.LocationLevelID = '".$FloorSelected."' AND
							c.LocationID = '".$RoomSelected."' AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT 
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = '".$BuildingSelected."' AND
								b.LocationLevelID = '".$FloorSelected."' AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        }
    }
}
if (sizeof($result) > 0) {
    $location_list = implode(",", $result);
    
    if ($exist_cond > 0) {
        $cond .= " AND ";
        $cond2 .= " AND ";
    }
    if ($exist_cond == 0) {
        $cond .= " WHERE ";
        $cond2 .= " WHERE ";
    }
    
    $cond .= " (e.LocationID IN ($location_list) OR g.LocationID IN ($location_list)) ";
    // $cond .= " (e.LocationID IN ($location_list)) ";
    // $cond2 .= " (g.LocationID IN ($location_list)) ";
    $exist_cond ++;
}
// ## End Of Location Condition ###

if ($location_list != "") {
    $location_condition1 = " AND e.LocationID IN ($location_list) ";
    $location_condition2 = " AND g.LocationID IN ($location_list) ";
}

if ($linventory->IS_ADMIN_USER($UserID)) {
    $sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP";
    $arr_tmp_admin = $linventory->returnVector($sql);
    $target_admin_group = implode(",", $arr_tmp_admin);
    
    if ($target_admin_group != "") {
        $admin_condition1 = " AND e.GroupInCharge IN (" . $target_admin_group . ") ";
        $admin_condition2 = " AND g.GroupInCharge IN (" . $target_admin_group . ") ";
    } else {
        // $admin_condition1 = " AND e.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
        // $admin_condition2 = " AND g.GroupInCharge IN (".$linventory->getInventoryAdminGroup().") ";
        $admin_condition1 = " ";
        $admin_condition2 = " ";
    }
} else {
    if ($linventory->getInventoryAdminGroup() != "") {
        $admin_condition1 = " AND e.GroupInCharge IN (" . $linventory->getInventoryAdminGroup() . ") ";
        $admin_condition2 = " AND g.GroupInCharge IN (" . $linventory->getInventoryAdminGroup() . ") ";
    }
}

// ## Set SQL Condition - Resource Management Group ###
if ($targetGroup != "") {
    if ($exist_cond > 0) {
        $cond .= " AND ";
        $cond2 .= " AND ";
    }
    if ($exist_cond == 0) {
        $cond .= " WHERE ";
        $cond2 .= " WHERE ";
    }
    $cond .= " (e.GroupInCharge IN ($targetGroup) OR g.GroupInCharge IN ($targetGroup)) ";
    // $cond .= " (e.GroupInCharge IN ($targetGroup)) ";
    // $cond2 .= " (g.GroupInCharge IN ($targetGroup)) ";
    $exist_cond ++;
}
// ## End Of Resource Management Group ###
// ## Set SQL Condition - Funding Source ###
if ($targetFunding != "") {
    // $from_funding_condition = " AND g.FundingSourceID = $targetFunding ";
    
    if ($exist_cond > 0) {
        $cond .= " AND ";
        $cond2 .= " AND ";
    }
    if ($exist_cond == 0) {
        $cond .= " WHERE ";
        $cond2 .= " WHERE ";
    }
    $cond .= " (e.FundingSource IN ($targetFunding) OR e.FundingSource2 IN ($targetFunding) OR g.FundingSourceID IN ($targetFunding)) ";
    // $cond .= " (e.FundingSource IN ($targetFunding)) ";
    // $cond2 .= " (g.FundingSourceID IN ($targetFunding)) ";
    $exist_cond ++;
} else {
    // $from_funding_condition = " AND g.FundingSourceID != '' ";
}
// ## End Of Resource Management Group ###
// ## Set SQL Condition - Category ###
if ($targetCategory == "") {
    $sql = "SELECT 
					Category2ID
			FROM
					INVENTORY_CATEGORY_LEVEL2
			ORDER BY
					Category2ID, CategoryID";
    
    $result = $linventory->returnVector($sql);
} else {
    if ($targetSubCategory == "") {
        $sql = "SELECT 
						Category2ID
				FROM
						INVENTORY_CATEGORY_LEVEL2
				WHERE
						CategoryID = '".$targetCategory."'
				ORDER BY
						Category2ID, CategoryID";
        
        $result = $linventory->returnVector($sql);
    } else {
        $sql = "SELECT
						Category2ID
				FROM
						INVENTORY_CATEGORY_LEVEL2
				WHERE
						Category2ID = '".$targetSubCategory."' AND
						CategoryID = '".$targetCategory."'
				ORDER BY
						Category2ID, CategoryID";
        
        $result = $linventory->returnVector($sql);
        
        if (sizeof($result) == 0) {
            $sql = "SELECT
							Category2ID
					FROM
							INVENTORY_CATEGORY_LEVEL2
					WHERE
							Category2ID = '".$targetSubCategory."'
					ORDER BY
							Category2ID, CategoryID";
            
            $result = $linventory->returnVector($sql);
        }
    }
}
if (sizeof($result) > 0) {
    $category_list = implode(",", $result);
    
    if ($exist_cond > 0)
        $cond .= " AND ";
    if ($exist_cond == 0)
        $cond .= " WHERE ";
    
    if ($category_list != "")
        $cond .= " (a.Category2ID IN ($category_list) OR c.Category2ID IN ($category_list)) ";
    else
        $cond .= " (a.Category2ID IN ('') OR c.Category2ID IN ('')) ";
    $exist_cond ++;
} else {
    if ($exist_cond > 0)
        $cond .= " AND ";
    if ($exist_cond == 0)
        $cond .= " WHERE ";
    
    if ($targetCategory != "")
        $cond .= " (a.CategoryID IN ($targetCategory) OR c.CategoryID IN ($targetCategory)) ";
    else
        $cond .= " (a.CategoryID IN ('') OR c.CategoryID IN ('')) ";
    $exist_cond ++;
}
// ## End Of Category ###
// ## Set SQL Condition - Item Type ###
if ($targetItemType != "") {
    // $item_type_condition = " AND a.ItemType = $targetItemType ";
    $cond .= " AND a.ItemType =  '".$targetItemType."' ";
}
// ## End Of Item Type ###

// ## Set SQL Condition - Keyword ###
if ($keyword != "") {
    if ($exist_cond > 0)
        $cond .= " AND ";
    if ($exist_cond == 0)
        $cond .= " WHERE ";
    
    if ((strpos($keyword, "*") == 0) && (strrpos($keyword, "*") == strlen($keyword) - 1)) {
        $barcode = substr($keyword, 1, strlen($keyword) - 2);
    }
    
    $cond .= " ((a.NameChi LIKE '%$keyword%' OR a.NameEng LIKE '%$keyword%') OR
				(a.ItemCode LIKE '%$keyword%') OR
				(b.NameChi LIKE '%$keyword%' OR b.NameEng LIKE '%$keyword%') OR
				(c.NameChi LIKE '%$keyword%' OR c.NameEng LIKE '%$keyword%') OR
				(e.TagCode LIKE '%$keyword%') OR
				(e.Brand LIKE '%$keyword%') OR
				(e.ItemRemark LIKE '%$keyword%') OR
				(a.DescriptionChi LIKE '%$keyword%') OR
				(a.DescriptionEng LIKE '%$keyword%') OR 
				(bl.Barcode LIKE '%$keyword%') 
				) ";
}

if ($Tag != "") {
    $RecordIDbyTags_Arr = $linventory->getRecordIDsFromTagName($Tag);
    if (is_array($RecordIDbyTags_Arr) && sizeof($RecordIDbyTags_Arr) > 0) {
        $RecordIDbyTags = implode(",", $RecordIDbyTags_Arr);
        $cond .= " AND a.ItemID IN ({$RecordIDbyTags}) ";
    }
}

/*
 * $sql = "DROP TABLE INVENTORY_ITEM_FULL_LIST_VIEW";
 * $linventory->db_db_query($sql);
 *
 * $sql = "CREATE TABLE INVENTORY_ITEM_FULL_LIST_VIEW
 * (
 * ItemCode varchar(100),
 * ItemName text,
 * ItemCategory text,
 * ItemLocation text,
 * ItemGroup text,
 * ItemFundingSource text,
 * ItemQuantity int(11),
 * CheckBox text,
 * Css text
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8";
 * $linventory->db_db_query($sql);
 */
if(!$display_zero){
    $zero_cond .= "and g.Quantity>0 ";
    $cond .= " AND IF(ItemType = 2, g.Quantity>'0', '1') ";
}

if ($display_photo == 1) {
    $sql = "SELECT
					a.ItemCode, 
					IF(
						d.PhotoName != '', CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">'," . stripslashes(intranet_undo_htmlspecialchars($linventory->getInventoryItemNameByLang("a."))) . ",'</a><img src=\"../../../../../..',d.PhotoPath,'/',d.PhotoName,'\" width=\"100px\">'),
						CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">'," . intranet_undo_htmlspecialchars(stripslashes($linventory->getInventoryItemNameByLang("a."))) . ",'</a>')
					  ),
					CONCAT(" . $linventory->getInventoryNameByLang("b.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' , " . $linventory->getInventoryNameByLang("j.") . ",' > '," . $linventory->getInventoryNameByLang("h.") . "),
					" . $linventory->getInventoryNameByLang("i.") . ",
					if(" . $linventory->getInventoryNameByLang("k2.") . ">'', concat(" . $linventory->getInventoryNameByLang("k.") . ",', '," . $linventory->getInventoryNameByLang("k2.") . "), " . $linventory->getInventoryNameByLang("k.") . "),
					IF(ItemType = 2, g.Quantity, ' 1 '),
					a.DateInput,
					IF(ItemType = 2, 
						CONCAT('<input type=\"checkbox\" name=\"ItemID[]\" id=\"ItemID\" class=\"ItemID\" value=',a.ItemID,':',h.LocationID,':',g.GroupInCharge,':',g.FundingSourceID,' onClick=javascript:parseLocation(',h.LocationID,');>'),
						CONCAT('<input type=\"checkbox\" name=\"ItemID[]\" id=\"ItemID\" class=\"ItemID\" value=',a.ItemID,':',h.LocationID,' onClick=javascript:parseLocation(',h.LocationID,');>')),
					IF(a.ItemType=1,CONCAT('single'),CONCAT('bulk'))
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = '$selectItemStatus' $item_type_condition) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID AND a.RecordStatus = '$selectItemStatus' $item_type_condition) LEFT OUTER JOIN
					INVENTORY_PHOTO_PART AS d ON (a.PhotoLink = d.PartID) LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID $location_condition1 $admin_condition1) LEFT OUTER JOIN 
					INVENTORY_ITEM_BULK_LOCATION AS g ON (a.ItemID = g.ItemID $location_condition2 $admin_condition2 $zero_cond) LEFT OUTER JOIN
					INVENTORY_LOCATION AS h ON IF(ItemType = 1,(e.LocationID = h.LocationID), (g.LocationID = h.LocationID)) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS j ON (h.LocationLevelID = j.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (j.BuildingID = LocBuilding.BuildingID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS i ON IF(ItemType = 1,(e.GroupInCharge = i.AdminGroupID), (g.GroupInCharge = i.AdminGroupID)) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS k ON (e.FundingSource = k.FundingSourceID) OR (g.FundingSourceID = k.FundingSourceID)
					LEFT JOIN INVENTORY_FUNDING_SOURCE AS k2 ON (e.FundingSource2 = k2.FundingSourceID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
			$cond
			";
}
if (($display_photo == "") || ($display_photo == 0)) {
    $sql = "SELECT
					a.ItemCode, 
					IF(
						d.PhotoName != '', CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">'," . $linventory->getInventoryItemNameByLang("a.") . ",'</a>&nbsp;<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_pic.gif\" onMouseOver=\"javascript:MM_swapImage(''item_photo_',a.ItemID,'_',h.LocationID,'_',i.AdminGroupID,''','''',''',d.PhotoPath,d.PhotoName,''',1)\" onMouseOut=\"javascript:MM_swapImgRestore()\" id=\"item_photo_',a.ItemID,'_',h.LocationID,'_',i.AdminGroupID,'\" name=\"item_photo_',a.ItemID,'_',h.LocationID,'_',i.AdminGroupID,'\" style=\"position: absolute;top: auto;left: auto;max-height: 250px;max-width: 300px;\">'),
						CONCAT('<a class=\"tablelink\" href=\"category_show_items_detail.php?type=1&item_id=',a.ItemID,'\">'," . intranet_undo_htmlspecialchars(stripslashes($linventory->getInventoryItemNameByLang("a."))) . ",'</a>')
					  ),
					CONCAT(" . $linventory->getInventoryNameByLang("b.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' , " . $linventory->getInventoryNameByLang("j.") . ",' > '," . $linventory->getInventoryNameByLang("h.") . "),
					" . $linventory->getInventoryNameByLang("i.") . ",
					if(" . $linventory->getInventoryNameByLang("k2.") . ">'', concat(" . $linventory->getInventoryNameByLang("k.") . ",', '," . $linventory->getInventoryNameByLang("k2.") . "), " . $linventory->getInventoryNameByLang("k.") . "),
					IF(ItemType = 2, g.Quantity, ' 1 '),
					a.DateInput,
					IF(ItemType = 2, 
						CONCAT('<input type=\"checkbox\" name=\"ItemID[]\" id=\"ItemID\" class=\"ItemID\" value=',a.ItemID,':',h.LocationID,':',g.GroupInCharge,':',g.FundingSourceID,' onClick=javascript:parseLocation(',h.LocationID,');>'),
						CONCAT('<input type=\"checkbox\" name=\"ItemID[]\"  id=\"ItemID\" class=\"ItemID\" value=',a.ItemID,':',h.LocationID,' onClick=javascript:parseLocation(',h.LocationID,');>')),
					IF(a.ItemType=1,CONCAT('single'),CONCAT('bulk'))
			FROM 
					INVENTORY_ITEM AS a INNER JOIN 
					INVENTORY_CATEGORY AS b ON (a.CategoryID = b.CategoryID AND a.RecordStatus = '$selectItemStatus' $item_type_condition) INNER JOIN 
					INVENTORY_CATEGORY_LEVEL2 AS c ON (a.Category2ID = c.Category2ID AND a.RecordStatus = '$selectItemStatus' $item_type_condition) LEFT OUTER JOIN
					INVENTORY_PHOTO_PART AS d ON (a.PhotoLink = d.PartID) LEFT OUTER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID $location_condition1 $admin_condition1) LEFT OUTER JOIN 
					INVENTORY_ITEM_BULK_LOCATION AS g ON (a.ItemID = g.ItemID $location_condition2 $admin_condition2 $zero_cond) LEFT OUTER JOIN
					INVENTORY_LOCATION AS h ON IF(ItemType = 1,(e.LocationID = h.LocationID OR g.LocationID = h.LocationID), (g.LocationID = h.LocationID)) LEFT OUTER JOIN
					INVENTORY_LOCATION_LEVEL AS j ON (h.LocationLevelID = j.LocationLevelID) LEFT OUTER JOIN
					INVENTORY_LOCATION_BUILDING AS LocBuilding ON (j.BuildingID = LocBuilding.BuildingID) LEFT OUTER JOIN
					INVENTORY_ADMIN_GROUP AS i ON IF(ItemType = 1,(e.GroupInCharge = i.AdminGroupID OR g.LocationID = h.LocationID), (g.GroupInCharge = i.AdminGroupID)) LEFT OUTER JOIN
					INVENTORY_FUNDING_SOURCE AS k ON IF(ItemType = 1,(e.FundingSource = k.FundingSourceID OR g.LocationID = h.LocationID), (g.FundingSourceID = k.FundingSourceID))
					LEFT JOIN INVENTORY_FUNDING_SOURCE AS k2 ON (e.FundingSource2 = k2.FundingSourceID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_EXT as bl on (bl.ItemID=a.ItemID)
			$cond
			";
}

// # Gen view for Item Full List
if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$field = $field == "" ? 1 : $field;
// switch($field){
// case 0: $field = 1; break;
// case 1: $field = 1; break;
// case 2: $field = 2; break;
// case 3: $field = 3; break;
// case 4: $field = 4; break;
// case 5: $field = 5; break;
// case 6: $field = 6; break;
// case 7: $field = 7; break;
//
// default: $field = 1;
// }

$order = $order == "" ? 1 : $order;

// TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo, true);
if ($order == 1)
    $li->field_array = array(
        "",
        "a.ItemCode",
        "a.NameEng",
        "c.NameEng ASC, b.NameEng",
        "j.NameEng ASC, h.NameEng",
        "i.NameEng",
        "k.NameEng",
        "g.Quantity",
        "a.DateInput"
    );
else
    $li->field_array = array(
        "",
        "a.ItemCode",
        "a.NameEng",
        "c.NameEng DESC, b.NameEng",
        "j.NameEng DESC, h.NameEng",
        "i.NameEng",
        "k.NameEng",
        "g.Quantity",
        "a.DateInput"
    );
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = "";

$li->wrap_array = array();
$li->IsColOff = "eInventoryFullList";

// TABLE COLUMN
$pos = 1;
$li->column_list .= "<td width='1' class='tabletop tabletopnolink'>#</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Item_Code) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Item_Name) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem['Category']) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Location_Level) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem['Caretaker']) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem['FundingSource']) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $i_InventorySystem_Item_Qty) . "</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>" . $li->column($pos ++, $Lang['General']['RecordDate']) . "</td>\n";
$li->column_list .= "<td width='1'>" . $li->check("ItemID[]") . "</td>\n";
// $li->column_list .= "<td width='1'> </td>\n";

?>

<script language="javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) {x.src=x.oSrc;x.style.zIndex = 0;};
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];x.style.zIndex = 1;}
}
function showImage(val)
{
	document.form1.display_photo.value = val;
	document.form1.pageNo.value = document.form1.current_pageNo.value
	document.form1.submit();
}

function submitenter(myfield,e)
{
var keycode;
if (window.event) keycode = window.event.keyCode;
else if (e) keycode = e.which;
else return true;

if (keycode == 13)
{
document.getElementById("keyword").value = document.getElementById("temp_keyword").value;
myfield.form.submit();
return false;
}
else
return true;
}

function checkExport(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                obj.action=page;
                obj.submit();
        }
}

function checkExportItemDetail(obj,element,page){
        if(countChecked(obj,element)==0)
                if(confirm("<?=$i_InventorySystem_ExportWarning;?>")){
	                obj.action=page;
	                obj.submit();
                }else{
	                obj.action='';
                }
        else{
                obj.action=page;
                obj.submit();
        }
        obj.action="";
}

function checkRemove(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm("<?=$i_InventorySystem_FullList_DeleteItemAlert;?>")){	            
                obj.action=page;                
                obj.method="POST";
                obj.submit();
                }
        }
}

function checkSingleItem(obj,element,page){
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
	        
                obj.action=page;
                obj.submit();
        }
}

function checkEdit(obj,element,page){
		var tmp_item_id;
		var edit_check = 0;
		var is_admin = <?echo $linventory->IS_ADMIN_USER($UserID);?>;
		
        if(countChecked(obj,element)==1) {
	        	tmp_item_id_str = returnChecked(obj,element);
	        	tmp_item_id = tmp_item_id_str.split(":");
	        	<?for($i=0; $i<sizeof($tmp_arr); $i++) {?>
	        		if(tmp_item_id[0] == <?=$tmp_arr[$i]?>) {
	        			edit_check++;
        			}
        		<?}?>	
        		
		        		
		        if((edit_check>0) || (is_admin == 1))
		        {		
                	obj.action=page;
                	obj.submit();
            	}
            	else
            	{
	            	alert("<?=$Lang['eInventory']['NoRightForEditThisItem']?>");
            	}
        } else {
                alert(globalAlertMsg1);
        }
}


function checkDifferentGroupRightEdit(obj,element,page){
		var tmp_item_id;
		var edit_check = 0;
		var is_admin = <?echo $linventory->IS_ADMIN_USER($UserID);?>;
					
        if(countChecked(obj,element)==1) {
	        	tmp_item_id_str = returnChecked(obj,element);
	        	tmp_item_id = tmp_item_id_str.split(":");
	        
	        	<?for($i=0; $i<sizeof($tmp_arr); $i++) {?>
	        		if(tmp_item_id[0] == <?=$tmp_arr[$i]?>) {
	        			edit_check++;
        			}
        		<?}?>	
	        	
	           // var page = "";
         		if (is_admin!=''){	
         			//page = "items_status_edit.php";
         					
				   	obj.action=page;
				   	obj.submit();
				   
				}	
         		else{
         			
		            $.ajax({
					url:      	"ajax_check_group_leader_right.php",
					type:     	"POST",
					data:     	'&ItemID='+tmp_item_id_str,
					async:		false,
					error:    	function(xhr, ajaxOptions, thrownError){
									alert(xhr.responseText);
							  	},
					success: function(data)
							 {		
							 	
								if(data)					
								{			
									if(data ){
									
										//if((edit_check>0) && (data == 1)){
										if((edit_check>0) && (data != 3)){
											page = 'items_status_edit.php';												
										 	obj.action=page;
		                					obj.submit();
										}
										
										<?php
        if ($linventory->enableMemberUpdateLocationRight()) {
            ?>				
											
									    if((edit_check>0) && (data == 3)){	
											page = 'items_location_edit.php';												
										 	obj.action=page;
		                					obj.submit();
										}
										<?php }else{?>
										else{
									 		alert("<?=$Lang['eInventory']['NoRightForEditThisItem']?>");
										 }	
										
										<?} ?>
										
									 }
									 else{
									 	alert("<?=$Lang['eInventory']['NoRightForEditThisItem']?>");
									 }	
									
														 
								}
							}
					 });	
         		}
         
            	
        } else {
                alert(globalAlertMsg1);
        }
}

function parseLocation(locationID){
		document.form1.hiddenLocationID.value = locationID;
}

/* newly add by Ronald - used to reset the location_level and location when change Building or Floor selection */
function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("FloorSelected")){
			document.getElementById("FloorSelected").value = "";
		}
		if(document.getElementById("RoomSelected")){
			document.getElementById("RoomSelected").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("RoomSelected")){
			document.getElementById("RoomSelected").value = "";
		}
	}
}

function click_zero()
{
	var obj=document.form1;
	
	if(obj.display_zero.checked==true)
		obj.display_zero_tmp.value=1;
	else
		obj.display_zero_tmp.value=0;
	obj.submit();	
}


function checkBulkRequestWriteOff(obj,element,page){
	var obj=document.form1;
	
	if(countChecked(obj,element)>0) {
		alert('<?=$Lang['eInventory']['JSWarning']['WriteOffSingleItemsOnly']?>');	
		obj.action="bulk_items_request_write_off.php";
		obj.submit();
	}
	else{
		alert(globalAlertMsg2);
	}
	
}







</script>

<br>

<form name="form1" action="" method="POST">

	<table width="100%" border="0" cellpadding="5" cellspacing="0"
		align="center">
	<?=$infobar1?>
</table>
	<table id="html_body_frame" width="96%" border="0" cellspacing="0"
		cellpadding="5" align="center">
	<?=$infobar2?>
</table>
	<!--
<table width="96%" border="0" cellpadding="0" cellspacing="0">
<?=$search_option;?>
</table>
-->
	<table width="96%" border="0" cellpadding="3" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right" class="tabletext"><?= $SysMsg ?></td>
		</tr>
		<tr height="10px">
			<td colspan="2"></td>
		</tr>
		<tr>
			<td align="left"><?=$toolbar?></td>
			<td align="right"><?=$search_option?></td>
		</tr>
	</table>

	<table width="96%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td align="left">
			<?=$filterbar; ?>
		</td>
		</tr>
		<tr height="3px">
			<td></td>
		</tr>
		<tr class="table-action-bar">
			<td colspan="" valign="bottom" align="right">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif"
							width="21" height="23"></td>
						<td
							background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
								<?=$table_tool?>
							</tr>
							</table>
						</td>
						<td width="6"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif"
							width="6" height="23"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<?php echo $li->display("96%"); ?>

<table border="0" width="96%" cellpadding="5" cellspacing="0">
		<tr>
			<td><table border="0" cellpadding="0">
					<tr>
						<td><table border="0">
								<tr>
									<td width="10" height="10"><table width="100%" border="1"
											cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
											<tr>
												<td class="single"><img
													src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
													width="10" height="10"></td>
											</tr>
										</table></td>
									<td><span class="tabletext"><?=$i_InventorySystem_ItemType_Single?></span></td>
								</tr>
							</table></td>
						<td><table width="100%" border="0">
								<tr>
									<td width="10" height="10"><table width="100%" border="1"
											cellpadding="0" cellspacing="0" bordercolor="#E8E8E8">
											<tr>
												<td class="bulk"><img
													src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif"
													width="10" height="10"></td>
											</tr>
										</table></td>
									<td><span class="tabletext"><?=$i_InventorySystem_ItemType_Bulk?></span></td>
								</tr>
							</table></td>
					</tr>
				</table></td>
		</tr>
	</table>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>">
	<input type="hidden" name="order" value="<?php echo $li->order; ?>"> <input
		type="hidden" name="field" value="<?php echo $li->field; ?>"> <input
		type="hidden" name="page_size_change" value=""> <input type="hidden"
		name="numPerPage" value="<?=$li->page_size?>"> <input type="hidden"
		name="display_photo" value="<?=$display_photo?>"> <input type="hidden"
		name="hiddenLocationID" value=""> <input type="hidden"
		name="current_pageNo" value="<?php echo $li->pageNo; ?>"> <input
		type="hidden" name="clearCoo" value="0"> <input type="hidden"
		name="keyword" id="keyword"
		value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>">
<?
if (sizeof($ItemID) > 0) {
    $selectedItemID = implode(",", $ItemID);
    ?>
	<input type="hidden" name="selectedItemID" value="<?=$selectedItemID?>">
<?}?>


</form>

<br>

<script language="javascript">
document.getElementById("temp_keyword").focus();
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>