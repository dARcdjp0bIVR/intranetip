<?php
// modifying :
/**
 * **************************** Change Log **************************************************
 * 2019-05-13 (Henry): Security fix: SQL without quote
 * 2019-05-01 (Henry): security issue fix for SQL
 * 2018-02-22 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log ************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_StocktakeVarianceHandling";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ItemID = IntegerSafe($ItemID);

$TAGS_OBJ[] = array(
    $i_InventorySystem_StocktakeVarianceHandling,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();

// print_r($arr_surplus_location);

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$ItemID."'";
$arr_item_type = $linventory->returnVector($sql);
$ItemType = $arr_item_type[0];

$curr_date = date("Y-m-d");

if ($ItemType == ITEM_TYPE_SINGLE) {
    if ($variance_action == 1) {
        // Move back #
        $sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($ItemID, '$curr_date, " . ITEM_ACTION_MOVEBACKTOORGINIAL . ", $UserID, '', 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "SELECT
					b.LocationID
				FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
				WHERE
					a.ItemID = '".$ItemID."'
				";
        $arr_new_location_id = $linventory->returnVector($sql);
        $new_location_id = $arr_new_location_id[0];
        
        $sql = "INSERT INTO 
						INVENTORY_VARIANCE_HANDLING_REMINDER
						(HandlerID, SenderID, ItemID, Quantity, OriginalLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($targetHandler, $UserID, $ItemID, 1, $surplus_location_id, $new_location_id, NOW(), 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    if ($variance_action == 2) {
        $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
				FROM 
						INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
						INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
				WHERE
						a.ItemID = '".$ItemID."'
				";
        $arr_original_location = $linventory->returnVector($sql);
        $original_location = $arr_original_location[0];
        
        $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
				FROM
						INVENTORY_LOCATION AS a ON INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID)
				WHERE
						a.LocationID = '".$targetSublocation."'
				";
        $arr_new_location = $linventory->returnVector($sql);
        $new_location = $arr_new_location[0];
        
        $remark = "$i_InventorySystem_From: $original_location<BR>";
        $remark .= "$i_InventorySystem_To: $new_location";
        $remark = addslashes($remark);
        
        // Relocation #
        $sql = "UPDATE 
					INVENTORY_ITEM_SINGLE_EXT
				SET
					LocationID = '".$targetSublocation."'
				WHERE 
					ItemID = '".$ItemID."'
				";
        // echo $sql."<BR>";
        $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($ItemID, '$curr_date', " . ITEM_ACTION_CHANGELOCATION . ", $UserID, '$remark', 0, 0, NOW(), NOW())
				";
        // echo $sql;
        $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO 
						INVENTORY_VARIANCE_HANDLING_REMINDER
						(HandlerID, SenderID, ItemID, quanitiy, OrignialLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($UserID, $targetHandler, $ItemID, 1, $original_location, $targetSublocation, NOW(), 0, 0 , NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    if ($variance_action == 3) {
        // Write-off #
        $sql = "SELECT LocationID, AdminGroupID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$ItemID."'";
        $arr_tmp_result = $linventory->returnArray($sql, 2);
        if (sizeof($arr_tmp_result) > 0) {
            list ($write_off_location, $write_off_admin) = $arr_tmp_result[0];
        }
        $sql = "SELECT " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = '".$targetWriteOffReason."'";
        $arr_write_off_reason = $linventory->returnVector($sql);
        $write_off_reason = addslashes($arr_write_off_reason[0]);
        
        $sql = "INSERT INTO 
					INVENTORY_ITEM_WRITE_OFF_RECORD
					(ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, 
					RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($ItemID, '$curr_date', $write_off_location, $write_off_admin, 1, '$write_off_reason', '$curr_date', $UserID, 
					0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    if ($variance_action == 4) {
        $sql = "UPDATE
					INVENTORY_ITEM_SINGLE_EXT
				SET
					LocationID = '".$surplus_location_id."'
				WHERE
					ItemID = '".$ItemID."'
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO 
						INVENTORY_ITEM_SINGLE_STATUS_LOG
						(ItemID, RecordDate, Action, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($ItemID, '$curr_date', " . ITEM_ACTION_PURCHASE . ", $UserID, 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE
					INVENTORY_ITEM
				SET
					RecordStatus = 1
				WHERE
					ItemID = '".$ItemID."'
				";
        $linventory->db_db_query($sql);
    }
    if ($variance_action == 5) {
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
}

if ($ItemType == ITEM_TYPE_BULK) {
    $arr_surplus_location = explode(",", $surplus_location_list);
    $arr_missing_location = explode(",", $missing_location_list);
    
    // echo $total_surplus_qty."<BR>";
    // echo $total_missing_qty."<BR>";
    
    // Move back to original location #
    for ($i = 0; $i < sizeof($arr_surplus_location); $i ++) {
        $tmp_from_location = $arr_surplus_location[$i];
        for ($j = 0; $j < sizeof($arr_missing_location); $j ++) {
            $tmp_to_location = $arr_missing_location[$j];
            $move_back_qty = ${"move_to_qty_" . $tmp_from_location . "_" . $tmp_to_location};
            if ($move_back_qty != "") {
                // get the most updated surplus qty
                $sql = "SELECT SurplusQty FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_from_location."'";
                $arr_updated_surplus_qty = $linventory->returnVector($sql);
                $updated_surplus_qty = $arr_updated_surplus_qty[0];
                
                // get the most updated missing qty
                $sql = "SELECT MissingQty FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_to_location."'";
                $arr_updated_missing_qty = $linventory->returnVector($sql);
                $updated_missing_qty = $arr_updated_missing_qty[0];
                
                if ($updated_surplus_qty >= $move_back_qty) {
                    // echo $updated_missing_qty." ".$move_back_qty."<BR>";
                    if ($updated_missing_qty >= $move_back_qty) {
                        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - $move_back_qty WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_from_location."'";
                        // echo $sql."<BR>";
                        $linventory->db_db_query($sql);
                        
                        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - $move_back_qty WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_to_location."'";
                        // echo $sql."<BR>";
                        $linventory->db_db_query($sql);
                        
                        $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_to_location."'";
                        $arr_tmp_group = $linventory->returnVector($sql);
                        $tmp_admin_group = $arr_tmp_group[0];
                        $sql = "INSERT INTO 
									INVENTORY_ITEM_BULK_LOG 
									(ItemID, Action, StockCheckQty, PersonInCharge, LocationID, GroupInCharge, DateInput, DateModified)
								VALUES
									($ItemID, " . ITEM_ACTION_CHANGELOCATION . ", $move_back_qty, $UserID, $tmp_to_location, $tmp_admin_group, NOW(), NOW())
								";
                        // echo $sql."<BR><BR>";
                        $linventory->db_db_query($sql);
                        
                        $targetHandler = ${"target_handler_" . $tmp_from_location . "_" . $tmp_to_location};
                        $sql = "INSERT INTO 
									INVENTORY_VARIANCE_HANDLING_REMINDER
									(HandlerID, ItemID, Quantity, OriginalLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified)
								VALUES
									($targetHandler, $ItemID, $move_back_qty, $tmp_from_location, $tmp_to_location, NOW(), 0, 0, NOW(), NOW())
								";
                        $linventory->db_db_query($sql);
                    }
                }
            }
        }
    }
    
    // Relocation #
    for ($i = 0; $i < sizeof($arr_missing_location); $i ++) {
        $tmp_from_location = $arr_missing_location[$i];
        for ($j = 0; $j < sizeof($arr_surplus_location); $j ++) {
            $tmp_to_location = $arr_surplus_location[$j];
            $relocate_qty = ${"relocate_to_qty_" . $tmp_from_location . "_" . $tmp_to_location};
            // echo $relocate_qty."<BR>";
            if ($relocate_qty != "") {
                // get the most updated surplus qty
                $sql = "SELECT SurplusQty FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_to_location."'";
                // echo $sql."<BR>";
                $arr_updated_surplus_qty = $linventory->returnVector($sql);
                $updated_surplus_qty = $arr_updated_surplus_qty[0];
                
                // get the most updated missing qty
                $sql = "SELECT MissingQty FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_from_location."'";
                // echo $sql."<BR>";
                $arr_updated_missing_qty = $linventory->returnVector($sql);
                $updated_missing_qty = $arr_updated_missing_qty[0];
                
                // echo $updated_surplus_qty."<BR>";
                // echo $updated_missing_qty."<BR>";
                
                if ($relocate_qty <= $updated_surplus_qty) {
                    $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - $relocate_qty WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_to_location."'";
                    // echo $sql."<BR>";
                    $linventory->db_db_query($sql);
                }
                if ($relocate_qty <= $updated_missing_qty) {
                    $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - $relocate_qty WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_from_location."'";
                    // echo $sql."<BR>";
                    $linventory->db_db_query($sql);
                }
                
                $targetHandler = ${"target_handler_" . $tmp_from_location . "_" . $tmp_to_location};
                $sql = "INSERT INTO 
							INVENTORY_VARIANCE_HANDLING_REMINDER
							(HandlerID, ItemID, Quantity, OriginalLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified)
						VALUES
							($targetHandler, $ItemID, $relocate_qty, $tmp_from_location, $tmp_to_location, NOW(), 0, 0, NOW(), NOW())
						";
                // echo $sql."<BR>";
                $linventory->db_db_query($sql);
            }
        }
    }
    
    // Write-off handling#
    for ($i = 0; $i < sizeof($arr_missing_location); $i ++) {
        $tmp_from_location = $arr_missing_location[$i];
        $write_off_qty = ${"write_off_qty_$tmp_from_location"};
        
        if ($write_off_qty > 0) {
            $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - $write_off_qty WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_from_location."'";
            // echo $sql."<BR>";
            $linventory->db_db_query($sql);
            
            $sql = "SELECT GroupInCharge FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$ItemID."' AND LocationID = '".$tmp_from_location."'";
            $arr_admin_group_id = $linventory->returnVector($sql);
            $admin_group_id = $arr_admin_group_id[0];
            
            $write_off_reason = addslashes($i_InventorySystem_WriteOff_Reason_LostItem);
            
            $sql = "INSERT INTO 
						INVENTORY_ITEM_WRITE_OFF_RECORD
						(ItemID, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, RecordType, RecordStatus, DateInput, DateModified)
					VALUES
						($ItemID, $tmp_from_location, $admin_group_id, $write_off_qty, '$write_off_reason', '$curr_date', $UserID, 0, 0, NOW(), NOW())
					";
            $linventory->db_db_query($sql);
        }
    }
    
    // Surplus Handling #
    for ($i = 0; $i < sizeof($arr_surplus_location); $i ++) {
        $location_id = $arr_surplus_location[$i];
        $surplus_qty = ${"surplus_qty_$location_id"};
        $ignore_qty = ${"ignory_qty_$location_id"};
        
        // get the most updated surplus qty
        $sql = "SELECT SurplusQty FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$ItemID."' AND LocationID = '".$location_id."'";
        // echo $sql."<BR>";
        $arr_updated_surplus_qty = $linventory->returnVector($sql);
        $updated_surplus_qty = $arr_updated_surplus_qty[0];
        
        if ($surplus_qty > 0) {
            if ($surplus_qty <= $updated_surplus_qty) {
                $sql = "UPDATE 
							INVENTORY_ITEM_BULK_LOCATION
						SET
							Quantity = Quantity + $surplus_qty
						WHERE
							ItemID = '".$ItemID."' AND
							LocationID = '".$location_id."'
						";
                // echo $sql."<BR>";
                $linventory->db_db_query($sql);
                
                $sql = "UPDATE
							INVENTORY_ITEM_BULK_EXT
						SET
							Quantity = Quantity + $surplus_qty
						WHERE
							ItemID = '".$ItemID."'
						";
                $linventory->db_db_query($sql);
                
                $sql = "UPDATE
							INVENTORY_ITEM_SURPLUS_RECORD
						SET
							SurplusQty = SurplusQty - $surplus_qty
						WHERE
							ItemID = '".$ItemID."' AND
							LocationID = '".$location_id."'
						";
                // echo $sql."<BR>";
                $linventory->db_db_query($sql);
            }
        }
        if ($ignore_qty > 0) {
            if ($ignore_qty <= $updated_surplus_qty) {
                $sql = "UPDATE 
							INVENTORY_ITEM_SURPLUS_RECORD
						SET
							SurplusQty = SurplusQty - $ignore_qty
						WHERE
							ItemID = '".$ItemID."' AND
							LocationID = '".$location_id."'
						";
                // echo $sql."<BR>";
                $linventory->db_db_query($sql);
            }
        }
    }
}

$sql = "DELETE FROM INVENTORY_ITEM_MISSING_RECORD WHERE MissingQty = 0";
// echo $sql."<BR>";
$linventory->db_db_query($sql);

$sql = "DELETE FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE SurplusQty = 0";
// echo $sql."<BR>";
$linventory->db_db_query($sql);

intranet_closedb();
header("location: outstanding_items.php?msg=2");
?>