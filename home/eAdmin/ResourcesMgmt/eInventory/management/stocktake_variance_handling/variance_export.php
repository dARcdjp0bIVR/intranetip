<?php
// using: 
/*
 * Date:  2019-10-16 (Tommy)
 *        -fix output
 * 
 * Date:  2019-07-12 (Tommy)
 *        - file created
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_StocktakeVarianceHandling";
$linventory = new libinventory();
$lexport = new libexporttext();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$building = IntegerSafe($BuildingSelected);
$floor = IntegerSafe($FloorSelected);
$room = IntegerSafe($RoomSelected);

if ($building == 0) {
    $sql = "SELECT
					LocationID
			FROM
					INVENTORY_LOCATION
			WHERE
					RecordStatus = 1
			ORDER BY
					LocationLevelID, LocationID";
} else {
    if ($floor == 0) {
        $sql = "SELECT
						c.LocationID
				FROM
						INVENTORY_LOCATION_BUILDING AS a INNER JOIN
						INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
						INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
				WHERE
						a.BuildingID = '".$building."' AND
						a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
        $result = $linventory->returnVector($sql);
    } else {
        if ($room == 0) {
            $sql = "SELECT
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = '".$building."' AND
							b.LocationLevelID = '".$floor."' AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = '".$building."' AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        } else {
            
            $sql = "SELECT
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = '".$building."' AND
							b.LocationLevelID = '".$floor."' AND
							c.LocationID = '".$room."' AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = '".$building."' AND
								b.LocationLevelID = '".$floor."' AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        }
    }
}
if (sizeof($result) > 0) {
    $location_list = implode(",", $result);
    // $cond .= " WHERE (b.LocationID IN ($location_list) OR c.LocationID IN ($location_list)) ";
    $cond .= " and b.LocationID IN ($location_list)";
}

if ($linventory->IS_ADMIN_USER($UserID) == true) {
    /*
     * /// This query will return ALL item records
     * $sql = "SELECT
     * DISTINCT a.ItemID, a.ItemType
     * FROM
     * INVENTORY_ITEM AS a
     * LEFT OUTER JOIN INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID)
     * LEFT OUTER JOIN INVENTORY_ITEM_MISSING_RECORD AS c ON (a.ItemID = c.ItemID)
     * $cond
     * ";
     */
    
    $sql1 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM
				INVENTORY_ITEM_SURPLUS_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
			$cond
			";
			$sql2 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM
				INVENTORY_ITEM_MISSING_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
			$cond
			";
} else {
    $variance_manager_group_list = $linventory->retrieveVarianceManagerGroup();
    /*
     * $sql = "SELECT
     * DISTINCT a.ItemID, a.ItemType
     * FROM
     * INVENTORY_ITEM AS a
     * LEFT OUTER JOIN INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID)
     * LEFT OUTER JOIN INVENTORY_ITEM_MISSING_RECORD AS c ON (a.ItemID = c.ItemID)
     * INNER JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID AND d.BulkItemAdmin IN ($variance_manager_group_list))
     * $cond
     * ";
     *
     *
     * INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID AND e.GroupInCharge IN ($target_group_list))
     * INNER JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID AND (d.BulkItemAdmin IN ($variance_manager_group_list) or d.BulkItemAdmin IN ($target_group_list)))
     *
     */
    
    if ($variance_manager_group_list) {
        $variance_con = "d.BulkItemAdmin IN ($variance_manager_group_list) or";
    }
    
    $sql1 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM
				INVENTORY_ITEM_SURPLUS_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
				LEFT JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID)
				LEFT JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID)
				left join INVENTORY_ITEM_BULK_LOCATION as f on (f.ItemID=b.ItemID and f.LocationID=b.LocationID)
			where
            	(e.GroupInCharge is not null or d.BulkItemAdmin is not null)
            	and ($variance_con f.GroupInCharge IN ($target_group_list) or e.GroupInCharge in ($target_group_list))
			$cond
			";
			$sql2 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM
				INVENTORY_ITEM_MISSING_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
				LEFT JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID)
				LEFT JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID)
				left join INVENTORY_ITEM_BULK_LOCATION as f on (f.ItemID=b.ItemID and f.LocationID=b.LocationID)
			where
            	(e.GroupInCharge is not null or d.BulkItemAdmin is not null)
            	and ($variance_con f.GroupInCharge IN ($target_group_list) or e.GroupInCharge in ($target_group_list))
            	$cond
			";
}
$sql = $sql1 . " union " . $sql2;
//debug_pr($sql);
$arr_target_item_id = $linventory->returnArray($sql);

$data_ary = array();
// # build data array
if (! empty($arr_target_item_id)) {
    foreach ($arr_target_item_id as $k => $d) {
        list ($this_itemid, $this_itemtype, $this_groupid, $this_fundingid) = $d;
        
        // if bulk item, qty need >0
        if ($this_itemtype == 2) {
            $sql = "select Quantity from INVENTORY_ITEM_BULK_EXT where ItemID='".$this_itemid."'";
            $temp_qty = $linventory->returnVector($sql);
            if ($temp_qty[0] <= 0)
                continue;
        }
        $data_ary[$this_itemtype][] = array(
            $this_itemid,
            $this_groupid,
            $this_fundingid
        );
    }
}

$exportColumn = array(
    $i_InventorySystem_StocktakeVarianceHandling,
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    ''
);
$rows[] = array('');

#############################################
#### Single item #####
#############################################

$rows[] = array($i_InventorySystem_ItemType_Single);
$rows[] = array(
    $i_InventorySystem_Item_Code,
    $i_InventorySystem_Item_Name,
    $i_InventorySystem_Item_Barcode,
    $i_InventorySystem['Category'],
    $i_InventorySystem['Location'],
    $i_InventorySystem_Group_Name,
    $i_InventorySystem_Stocktake_VarianceQty,
    $i_InventorySystem_VarianceHandling_CurrentLocation,
    $i_InventorySystem_Stocktake_ExpectedLocation
);

$batch_resume_single_item_exist = false;

$arr_missing_record = array();
if (! empty($data_ary[1])) {
        
        $target_item_list_ary = array();
        foreach ($data_ary[1] as $k => $d1){
            $target_item_list_ary[] = $d1[0];
        }
            $target_item_list = implode(",", $target_item_list_ary);
            
            $sql1 = "SELECT
				DISTINCT a.ItemID,
				b.ItemCode as ItemCode1,
                i.TagCode,
				" . $linventory->getInventoryItemNameByLang("b.") . ",
				CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				" . $linventory->getInventoryNameByLang("e.") . ",
				a.RecordDate,
				" . getNameFieldByLang2("h.") . ",
				CONCAT('+',a.SurplusQty)
			FROM
				INVENTORY_ITEM_SURPLUS_RECORD AS a INNER JOIN
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_CATEGORY AS f ON (b.CategoryID = f.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS g ON (b.Category2ID = g.Category2ID) INNER JOIN
				INTRANET_USER AS h ON (a.PersonInCharge = h.UserID) LEFT OUTER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS i ON (a.ItemID = i.ItemID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (e.AdminGroupID = i.GroupInCharge)
			WHERE
				a.ItemID IN ($target_item_list)
			";
            $sql2 = "SELECT
				DISTINCT a.ItemID,
				b.ItemCode as ItemCode1,
                i.TagCode,
				" . $linventory->getInventoryItemNameByLang("b.") . ",
				CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				" . $linventory->getInventoryNameByLang("e.") . ",
				a.RecordDate,
				" . getNameFieldByLang2("h.") . ",
				CONCAT('-',a.MissingQty)
			FROM
				INVENTORY_ITEM_MISSING_RECORD AS a INNER JOIN
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_CATEGORY AS f ON (b.CategoryID = f.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS g ON (b.Category2ID = g.Category2ID) INNER JOIN
				INTRANET_USER AS h ON (a.PersonInCharge = h.UserID) LEFT OUTER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS i ON (a.ItemID = i.ItemID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (e.AdminGroupID = i.GroupInCharge)
			WHERE
				a.ItemID IN ($target_item_list)
			";
            $sql = $sql1 . " union " . $sql2 . " order by ItemCode1";
            $arr_record = $linventory->returnArray($sql);
            
//             $sql = "SELECT
//                 a.ItemID,
// 				b.LocationID,
// 				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
// 			FROM
// 				INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
// 				INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
// 				INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
// 				inner join INVENTORY_LOCATION_BUILDING as d on (d.BuildingID = c.BuildingID)
// 			WHERE
// 				a.ItemID IN ($target_item_list)
// 			";
//             $arr_original_location = $linventory->returnArray($sql, 2);
            
            if (sizeof($arr_record) > 0) {
                for ($i = 0; $i < sizeof($arr_record); $i ++) {
                    list ($item_id, $item_code, $item_barCode, $item_name, $item_cat, $item_location, $item_admin, $record_date, $user_name, $var_qty) = $arr_record[$i];
                    $sql = "SELECT
                				b.LocationID,
                				CONCAT(" . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ")
                			FROM
                				INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN
                				INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN
                				INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID)
                				inner join INVENTORY_LOCATION_BUILDING as d on (d.BuildingID = c.BuildingID)
                			WHERE
                				a.ItemID IN ($item_id)
                			";
                    $arr_original_location = $linventory->returnArray($sql, 2);
                    for ($j = 0; $j < sizeof($arr_original_location); $j++){
                        list ($original_location_id, $original_location_name) = $arr_original_location[$j];
                        $item_qty = 0;
                        if($var_qty < 0){
                            $item_qty = $var_qty;
                        }else{
                            $item_qty = '+'.$var_qty;
                        }
                        
                        $rows[] = array(
                            $item_code,
                            $item_name,
                            $item_barCode."",
                            $item_cat,
                            $original_location_name,
                            $item_admin,
                            $item_qty,
                            $item_location,
                            $original_location_name
                        );
                    }
                }
            }
            // end single item
} else {
    $rows[] = array(
        $i_no_record_exists_msg
    );
}

#############################################
#### Bulk item #####
#############################################
$rows[] = array(''
    
);
$rows[] = array(''
    
);
$rows[] = array($i_InventorySystem_ItemType_Bulk);

$rows[] = array(
    $i_InventorySystem_Item_Code,
    $i_InventorySystem_Item_Name,
    $i_InventorySystem['Category'],
    $i_InventorySystem_Group_Name,
    $i_InventorySystem['FundingSource'],
    $i_InventorySystem['Location'],
    $i_InventorySystem_Stocktake_VarianceQty,
    $i_InventorySystem_VarianceHandling_Original_Quantity,
    $i_InventorySystem_Stocktake_StocktakeQty
);

$arr_record = array();
if (! empty($data_ary[2])) {
    $target_item_list_ary = array();
    $bulk_item_ary = array();
    foreach ($data_ary[2] as $k => $d1) {
        $target_item_list_ary[] = $d1[0];
        $bulk_item_ary[$d1[0]][] = array(
            $d1[1],
            $d1[2]
        );
    }
    //debug_pr($data_ary[2]);
    $target_item_list = implode(",", $target_item_list_ary);
           
    $sql = "select
				distinct(a.ItemID),
				a.ItemCode as ItemCode1,
				" . $linventory->getInventoryItemNameByLang("a.") . ",
				CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ")
			from
				INVENTORY_ITEM as a
				INNER JOIN INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID)
				INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID)
			where
				a.ItemID IN ($target_item_list)";
    $result = $linventory->returnArray($sql);
    //debug_pr($result);
    
    foreach ($result as $k => $b) {
        list ($this_itemid, $this_itemcode, $this_itemname, $this_cat) = $b;
        
        for ($i = 0; $i < sizeof($bulk_item_ary[$this_itemid]); $i ++) {
            list ($this_groupid, $this_fundingid) = $bulk_item_ary[$this_itemid][$i];
            //debug_pr($this_groupid);
        
            $sql1 = "SELECT
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ") as this_location,
				" . $linventory->getInventoryNameByLang("e.") . ",
				IFNULL(" . getNameFieldByLang2("f.") . ",'" . $Lang['General']['UserAccountNotExists'] . "'),
				b.RecordDate,
				CONCAT('+',b.SurplusQty),
				LocBuilding.DisplayOrder as Building_DisplayOrder,
				d.DisplayOrder as Level_DisplayOrder,
				c.DisplayOrder as Room_DisplayOrder
			FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) LEFT JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
			WHERE
				a.ItemID IN ('".$this_itemid."')
				and b.AdminGroupID='".$this_groupid."' and b.FundingSourceID='".$this_fundingid."'
			";
            
            $sql2 = "SELECT
				b.LocationID,
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				" . $linventory->getInventoryNameByLang("e.") . ",
				IFNULL(" . getNameFieldByLang2("f.") . ",'" . $Lang['General']['UserAccountNotExists'] . "'),
				b.RecordDate,
				CONCAT('-',b.MissingQty),
				LocBuilding.DisplayOrder as Building_DisplayOrder,
				d.DisplayOrder as Level_DisplayOrder,
				c.DisplayOrder as Room_DisplayOrder
		FROM
				INVENTORY_ITEM AS a INNER JOIN
				INVENTORY_ITEM_MISSING_RECORD AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (b.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (b.AdminGroupID = e.AdminGroupID) LEFT JOIN
				INTRANET_USER AS f ON (b.PersonInCharge = f.UserID)
		WHERE
				a.ItemID IN ('".$this_itemid."')
				and b.AdminGroupID=$this_groupid and b.FundingSourceID='".$this_fundingid."'
		";
            $sql = $sql1 . " union " . $sql2 . " order by Building_DisplayOrder, Level_DisplayOrder, Room_DisplayOrder";
            $arr_result = $linventory->returnArray($sql);
            //debug_pr($sql);
            
            $sql = "SELECT
					bl.ItemID, a.ItemCode, " . $linventory->getInventoryItemNameByLang("a.") . ",
                    d.BulkItemAdmin, bl.LocationID,
					CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . "),
					CONCAT(" . $linventory->getInventoryNameByLang("cat.") . ",' > '," . $linventory->getInventoryNameByLang("catLevel.") . "),
                    bl.Quantity
				FROM
                    INVENTORY_ITEM AS a
					LEFT OUTER JOIN INVENTORY_CATEGORY AS cat ON (a.CategoryID = cat.CategoryID)
					LEFT OUTER JOIN INVENTORY_CATEGORY_LEVEL2 AS catLevel ON (a.Category2ID = catLevel.Category2ID)
					LEFT OUTER JOIN INVENTORY_ITEM_BULK_LOCATION AS bl ON (bl.ItemID = a.ItemID)
					INNER JOIN INVENTORY_LOCATION AS b ON (b.LocationID = bl.LocationID)
					INNER JOIN INVENTORY_LOCATION_LEVEL AS c ON (c.LocationLevelID = b.LocationLevelID)
					INNER JOIN INVENTORY_LOCATION_BUILDING AS LocBuilding ON (c.BuildingID = LocBuilding.BuildingID)
					INNER JOIN INVENTORY_ITEM_BULK_EXT as d on (d.ItemID=a.ItemID)
				WHERE
					bl.ItemID = '".$this_itemid."'
					and bl.GroupInCharge='".$this_groupid."' and bl.FundingSourceID='".$this_fundingid."'
				order by LocBuilding.DisplayOrder, c.DisplayOrder, b.DisplayOrder
				";
					// a.LocationID IN ($target_variance)
			$arr_detail_result = $linventory->returnArray($sql, 8);
			$var_qty_ary = array();
            
			if(sizeof($arr_result) > 0){
			    for($i = 0; $i < sizeof($arr_result); $i++){
			        for ($j = 0; $j < sizeof($arr_detail_result); $j++){
                        $this_group_name = $linventory->returnGroupNameByGroupID($this_groupid);
                        $this_funding_name = $linventory->returnFundingSourceByFundingID($this_fundingid);
                        //debug_pr($this_funding_name);
                        
                        list($location_id, $location_name, $item_admin, $user_name, $date, $var_qty) = $arr_result[$i];
                        list($item_id, $item_code, $item_name, $admin_group, $original_location_id, $original_location_name, $item_cat, $original_qty) = $arr_detail_result[$j];
                        $var_qty_ary[$location_id] = $var_qty;
                        $new_qty_default = $original_qty + $var_qty_ary[$location_id];
                        $item_qty = 0;
                        if($var_qty < 0){
                            $item_qty = $var_qty;
                        }else{
                            $item_qty = '+'.$var_qty;
                        }
                        
                        $rows[] = array(
                            $item_code,
                            $item_name,
                            $item_cat,
                            $item_admin,
                            $this_funding_name,
                            $original_location_name,
                            $item_qty,
                            $original_qty,
                            $new_qty_default
                        );
                    }
                }
            }
        }   
    }   
}else {
    $rows[] = array(
        $i_no_record_exists_msg
    );
}

intranet_closedb();

$filename = "stocktake_variance_handing.csv";

$export_content = $lexport->GET_EXPORT_TXT($rows, $exportColumn);
$lexport->EXPORT_FILE($filename, $export_content);

?>