<?php
// using: 

// #################################
//
// Date: 2019-07-15 (Tommy)
// add export function for exporting table
//
// Date: 2019-05-13 (Henry)
// Security fix: SQL without quote
// 
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2015-12-21 Cameron
// add MoveToOriginalLocationByBatch for single item which variance is +1
//
// Date: 2014-12-15 Henry
// display remarks if in stockstake period
//
// Date: 2012-06-01 YatWoon
// hidden "last stocktake" info (display in details)
// display "group" and "funding source" for bulk item
//
// Date: 2011-12-15 YatWoon
// Improved: hide bulk item if qty <=0 [Case#2011-1208-1804-50066]
//
// Date: 2011-06-14 YatWoon
// Fixed: If there is no variance group manager setting for any items, this cause SQL query error [Case#2011-0610-1122-17128]
//
// Date: 2011-06-03 YatWoon
// add checking: can access this page for group leader
//
// Date: 2011-04-19 YatWoon
// fixed: category display order (subcat > cat)
//
// #################################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation.php");
include_once ($PATH_WRT_ROOT . "includes/liblocation_ui.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_StocktakeVarianceHandling";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$llocation_ui = new liblocation_ui();

$curr_date = date("Y-m-d");
if (! ($curr_date > $linventory->getStocktakePeriodEnd())) {
    // header("location: ../inventory/items_full_list.php");
    $TAGS_OBJ[] = array(
        $i_InventorySystem_StocktakeVarianceHandling,
        '',
        1
    );
    $MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
    $linterface->LAYOUT_START();
    
    $x = "<fieldset class='instruction_box_v30'>
		<legend>" . $Lang['General']['Remark'] . "</legend>
			- " . $Lang['eInventory']['StocktakeVarianceHandlingConductedAfterPeriod'] . "<br/>
			- " . $Lang['eInventory']['Report']['StocktakePeriod'] . ": " . $linventory->getStocktakePeriodStart() . " " . $Lang['General']['To'] . " " . $linventory->getStocktakePeriodEnd() . "
		</fieldset>";
    
    echo $x;
    $linterface->LAYOUT_STOP();
    intranet_closedb();
    exit();
}

// get group leader #
$sql = "SELECT AdminGroupID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE UserID = '".$UserID."' AND RecordType = 1";
$arr_admin_group_leader = $linventory->returnVector($sql);

if (sizeof($arr_admin_group_leader) > 0) {
    $target_group_list = implode(",", $arr_admin_group_leader);
}
// end #

if ($linventory->IS_ADMIN_USER($UserID) == false) {
    if ($linventory->retrieveVarianceManagerGroup() == '' && ! $linventory->IS_GROUP_ADMIN()) {
        header("location: ../inventory/items_full_list.php");
        exit();
    }
}

$TAGS_OBJ[] = array(
    $i_InventorySystem_StocktakeVarianceHandling,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

// Generate System Message #
if ($xmsg != "") {
    $SysMsg = $linterface->GET_SYS_MSG("", $xmsg);
} else {
    if ($msg == 1)
        $SysMsg = $linterface->GET_SYS_MSG("add");
    if ($msg == 2)
        $SysMsg = $linterface->GET_SYS_MSG("update");
    if ($msg == 3)
        $SysMsg = $linterface->GET_SYS_MSG("delete");
    if ($msg == 12)
        $SysMsg = $linterface->GET_SYS_MSG("add_failed");
    if ($msg == 13)
        $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
    if ($msg == 14)
        $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
// End #

// Generate Table Tool Bar - Approve / Reject #
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'ItemID[]','outstanding_items_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_update.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_update
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";



// # Location Selection - Building ##
$opt_LocationBuilding = $llocation_ui->Get_Building_Selection($BuildingSelected, "BuildingSelected", "resetLocationSelection('Building'); this.form.submit();", 0, 0, "All Buildings");
$location_selection = $opt_LocationBuilding;

// Generate Location Selection Box - Floor #
if ($BuildingSelected != "") {
    $opt_LocationLevel = $llocation_ui->Get_Floor_Selection($BuildingSelected, $FloorSelected, "FloorSelected", "resetLocationSelection('Floor'); this.form.submit();", 0, 0, "All Floors");
    $location_selection .= $opt_LocationLevel;
}
// Generate Location Selection Box - Room #
if (($FloorSelected != "") && ($BuildingSelected != "")) {
    $opt_Location = $llocation_ui->Get_Room_Selection($FloorSelected, $RoomSelected, "RoomSelected", "this.form.submit();", 0, "All Rooms", "");
    $location_selection .= $opt_Location;
}
// End #

$building = $BuildingSelected;
$floor = $FloorSelected;
$room = $RoomSelected;

if($building == ""){
    $building = 0;
}
if($floor == ""){
    $floor = 0;
}
if($room == ""){
    $room = 0;
}

$toolbar .= "<tr><td class=\"tabletext\">";
$toolbar .= "<div class=\"Conntent_tool\">";
$toolbar .= "<a href=\"javascript:click_export(". $building . "," . $floor . "," . $room .");\" class=\"export\" > " . $Lang['Btn']['Export'] . "</a>";
$toolbar .= "</td></tr>";

// ## Set SQL Condition - Location ###
if ($BuildingSelected == "") {
    $sql = "SELECT 
					LocationID
			FROM
					INVENTORY_LOCATION
			WHERE
					RecordStatus = 1
			ORDER BY
					LocationLevelID, LocationID";
} else {
    if ($FloorSelected == "") {
        $sql = "SELECT
						c.LocationID
				FROM
						INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
						INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
				WHERE
						a.BuildingID = '".$BuildingSelected."' AND
						a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
        $result = $linventory->returnVector($sql);
    } else {
        if ($RoomSelected == "") {
            $sql = "SELECT
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = '".$BuildingSelected."' AND
							b.LocationLevelID = '".$FloorSelected."' AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = '".$BuildingSelected."' AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        } else {
            
            $sql = "SELECT 
							c.LocationID
					FROM
							INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
							INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
							INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
					WHERE
							a.BuildingID = '".$BuildingSelected."' AND
							b.LocationLevelID = '".$FloorSelected."' AND
							c.LocationID = '".$RoomSelected."' AND
							a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
            $result = $linventory->returnVector($sql);
            if (sizeof($result) == 0) {
                $sql = "SELECT 
								c.LocationID
						FROM
								INVENTORY_LOCATION_BUILDING AS a INNER JOIN 
								INVENTORY_LOCATION_LEVEL AS b ON (a.BuildingID = b.BuildingID) INNER JOIN 
								INVENTORY_LOCATION AS c ON (b.LocationLevelID = c.LocationLevelID)
						WHERE
								a.BuildingID = '".$BuildingSelected."' AND
								b.LocationLevelID = '".$FloorSelected."' AND
								a.RecordStatus = 1 AND b.RecordStatus = 1 AND c.RecordStatus = 1";
                $result = $linventory->returnVector($sql);
            }
        }
    }
}
if (sizeof($result) > 0) {
    $location_list = implode(",", $result);
    // $cond .= " WHERE (b.LocationID IN ($location_list) OR c.LocationID IN ($location_list)) ";
    $cond .= " and b.LocationID IN ($location_list)";
}
// ## End Of Location Condition ###

// get item id with variance record #
if ($linventory->IS_ADMIN_USER($UserID) == true) {
    /*
     * /// This query will return ALL item records
     * $sql = "SELECT
     * DISTINCT a.ItemID, a.ItemType
     * FROM
     * INVENTORY_ITEM AS a
     * LEFT OUTER JOIN INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID)
     * LEFT OUTER JOIN INVENTORY_ITEM_MISSING_RECORD AS c ON (a.ItemID = c.ItemID)
     * $cond
     * ";
     */
    
    $sql1 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM 
				INVENTORY_ITEM_SURPLUS_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
			$cond
			";
    $sql2 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM 
				INVENTORY_ITEM_MISSING_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
			$cond
			";
} else {
    $variance_manager_group_list = $linventory->retrieveVarianceManagerGroup();
    /*
     * $sql = "SELECT
     * DISTINCT a.ItemID, a.ItemType
     * FROM
     * INVENTORY_ITEM AS a
     * LEFT OUTER JOIN INVENTORY_ITEM_SURPLUS_RECORD AS b ON (a.ItemID = b.ItemID)
     * LEFT OUTER JOIN INVENTORY_ITEM_MISSING_RECORD AS c ON (a.ItemID = c.ItemID)
     * INNER JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID AND d.BulkItemAdmin IN ($variance_manager_group_list))
     * $cond
     * ";
     *
     *
     * INNER JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID AND e.GroupInCharge IN ($target_group_list))
     * INNER JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID AND (d.BulkItemAdmin IN ($variance_manager_group_list) or d.BulkItemAdmin IN ($target_group_list)))
     *
     */
    
    if ($variance_manager_group_list) {
        $variance_con = "d.BulkItemAdmin IN ($variance_manager_group_list) or";
    }
    
    $sql1 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM 
				INVENTORY_ITEM_SURPLUS_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
				LEFT JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID)
				LEFT JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID)
				left join INVENTORY_ITEM_BULK_LOCATION as f on (f.ItemID=b.ItemID and f.LocationID=b.LocationID)
			where 
            	(e.GroupInCharge is not null or d.BulkItemAdmin is not null)
            	and ($variance_con f.GroupInCharge IN ($target_group_list) or e.GroupInCharge in ($target_group_list))
			$cond
			";
    $sql2 = "SELECT
				DISTINCT a.ItemID, a.ItemType, b.AdminGroupID, b.FundingSourceID
			FROM 
				INVENTORY_ITEM_MISSING_RECORD as b
				inner join INVENTORY_ITEM AS a on (a.ItemID = b.ItemID)
				LEFT JOIN INVENTORY_ITEM_SINGLE_EXT AS e ON (a.ItemID = e.ItemID)
				LEFT JOIN INVENTORY_ITEM_BULK_EXT AS d ON (a.ItemID = d.ItemID)
				left join INVENTORY_ITEM_BULK_LOCATION as f on (f.ItemID=b.ItemID and f.LocationID=b.LocationID)
			where 
            	(e.GroupInCharge is not null or d.BulkItemAdmin is not null)
            	and ($variance_con f.GroupInCharge IN ($target_group_list) or e.GroupInCharge in ($target_group_list))
            	$cond
			";
}
$sql = $sql1 . " union " . $sql2;
$arr_target_item_id = $linventory->returnArray($sql);

$data_ary = array();
// # build data array
if (! empty($arr_target_item_id)) {
    foreach ($arr_target_item_id as $k => $d) {
        list ($this_itemid, $this_itemtype, $this_groupid, $this_fundingid) = $d;
        
        // if bulk item, qty need >0
        if ($this_itemtype == 2) {
            $sql = "select Quantity from INVENTORY_ITEM_BULK_EXT where ItemID='".$this_itemid."'";
            $temp_qty = $linventory->returnVector($sql);
            if ($temp_qty[0] <= 0)
                continue;
        }
        $data_ary[$this_itemtype][] = array(
            $this_itemid,
            $this_groupid,
            $this_fundingid
        );
    }
}

// #############################################
// #### Single item #####
// #############################################
$batch_resume_single_item_exist = false;

$arr_missing_record = array();
if (! empty($data_ary[1])) {
    // $target_item_list = implode(",",$data_ary[1]);
    
    $target_item_list_ary = array();
    foreach ($data_ary[1] as $k => $d1)
        $target_item_list_ary[] = $d1[0];
    $target_item_list = implode(",", $target_item_list_ary);
    
    $sql1 = "SELECT
				DISTINCT a.ItemID,
				b.ItemCode as ItemCode1,
				i.TagCode,
				" . $linventory->getInventoryItemNameByLang("b.") . ",
				CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				" . $linventory->getInventoryNameByLang("e.") . ",
				a.RecordDate,
				" . getNameFieldByLang2("h.") . ",
				CONCAT('+',a.SurplusQty)
			FROM
				INVENTORY_ITEM_SURPLUS_RECORD AS a INNER JOIN
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_CATEGORY AS f ON (b.CategoryID = f.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS g ON (b.Category2ID = g.Category2ID) INNER JOIN
				INTRANET_USER AS h ON (a.PersonInCharge = h.UserID) LEFT OUTER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS i ON (a.ItemID = i.ItemID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (e.AdminGroupID = i.GroupInCharge)
			WHERE
				a.ItemID IN ($target_item_list)
			";
    $sql2 = "SELECT
				DISTINCT a.ItemID,
				b.ItemCode as ItemCode1,
				i.TagCode,
				" . $linventory->getInventoryItemNameByLang("b.") . ",
				CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . "),
				CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ", ' > ' ," . $linventory->getInventoryNameByLang("d.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . "),
				" . $linventory->getInventoryNameByLang("e.") . ",
				a.RecordDate,
				" . getNameFieldByLang2("h.") . ",
				CONCAT('-',a.MissingQty)
			FROM
				INVENTORY_ITEM_MISSING_RECORD AS a INNER JOIN
				INVENTORY_ITEM AS b ON (a.ItemID = b.ItemID) INNER JOIN
				INVENTORY_LOCATION AS c ON (a.LocationID = c.LocationID) INNER JOIN
				INVENTORY_LOCATION_LEVEL AS d ON (c.LocationLevelID = d.LocationLevelID) INNER JOIN
				INVENTORY_LOCATION_BUILDING AS LocBuilding ON (d.BuildingID = LocBuilding.BuildingID) INNER JOIN
				INVENTORY_CATEGORY AS f ON (b.CategoryID = f.CategoryID) INNER JOIN
				INVENTORY_CATEGORY_LEVEL2 AS g ON (b.Category2ID = g.Category2ID) INNER JOIN
				INTRANET_USER AS h ON (a.PersonInCharge = h.UserID) LEFT OUTER JOIN
				INVENTORY_ITEM_SINGLE_EXT AS i ON (a.ItemID = i.ItemID) INNER JOIN
				INVENTORY_ADMIN_GROUP AS e ON (e.AdminGroupID = i.GroupInCharge) 
			WHERE
				a.ItemID IN ($target_item_list)
			";
    $sql = $sql1 . " union " . $sql2 . " order by ItemCode1";
    $arr_missing_record = $linventory->returnArray($sql);
    
    if (sizeof($arr_missing_record) > 0) {
        for ($i = 0; $i < sizeof($arr_missing_record); $i ++) {
            list ($item_id, $item_code, $item_barcode, $item_name, $item_cat, $item_location, $item_admin, $record_date, $user_name, $var_qty) = $arr_missing_record[$i];
            
            $css = " class=\"single\" ";
            
            if ($var_qty == 1) {
                $batch_resume_single_item_exist = true;
                $batch_resume_original_loc = "<td class=\"tabletext\"><input type=\"checkbox\" name=\"ItemID[]\" id=\"ItemID\" value=\"$item_id\"></td>";
            } else {
                $batch_resume_original_loc = "<td class=\"tabletext\">&nbsp;</td>";
            }
            if ($var_qty > 0)
                $var_qty = "<font color=\"green\">$var_qty</font>";
            if ($var_qty < 0)
                $var_qty = "<font color=\"red\">$var_qty</font>";
            
            $table_content .= "<tr $css>";
            $table_content .= "<td class=\"tabletext\">$item_code</td>";
            $table_content .= "<td class=\"tabletext\"><a href=\"variance_edit.php?ItemID=$item_id\" class=\"tablelink\">$item_name</a></td>";
            $table_content .= "<td class=\"tabletext\">$item_barcode</td>";
            $table_content .= "<td class=\"tabletext\">$item_cat</td>";
            $table_content .= "<td class=\"tabletext\">$item_location</td>";
            $table_content .= "<td class=\"tabletext\">$item_admin</td>";
            // $table_content .= "<td class=\"tabletext\">$user_name</td>";
            // $table_content .= "<td class=\"tabletext\">$record_date</td>";
            $table_content .= "<td class=\"tabletext\">$var_qty</td>";
            $table_content .= $batch_resume_original_loc;
            $table_content .= "</tr>";
            $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"8\"></td></tr>";
        }
    }
} else {
    $table_content .= "<tr class=\"tablerow2\">";
    $table_content .= "<td class=\"tabletext\" colspan=\"8\" align=\"center\">$i_no_record_exists_msg</td>";
    $table_content .= "</tr>";
    $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"8\"></td></tr>";
}

// #############################################
// #### Bulk item #####
// #############################################
$arr_missing_record = array();
if (! empty($data_ary[2])) {
    $target_item_list_ary = array();
    $bulk_item_ary = array();
    foreach ($data_ary[2] as $k => $d1) {
        $target_item_list_ary[] = $d1[0];
        $bulk_item_ary[$d1[0]][] = array(
            $d1[1],
            $d1[2]
        );
    }
    $target_item_list = implode(",", $target_item_list_ary);
    // $target_item_list = implode(",",$data_ary[2]);
    
    // retrieve item list first
    $sql = "select 
				distinct(a.ItemID),
				a.ItemCode as ItemCode1,
				" . $linventory->getInventoryItemNameByLang("a.") . ",
				CONCAT(" . $linventory->getInventoryNameByLang("f.") . ",' > '," . $linventory->getInventoryNameByLang("g.") . ")
			from
				INVENTORY_ITEM as a
				INNER JOIN INVENTORY_CATEGORY AS f ON (a.CategoryID = f.CategoryID) 
				INNER JOIN INVENTORY_CATEGORY_LEVEL2 AS g ON (a.Category2ID = g.Category2ID) 
			where
				a.ItemID IN ($target_item_list)";
    $result = $linventory->returnArray($sql);
    foreach ($result as $k => $b) {
        list ($this_itemid, $this_itemcode, $this_itemname, $this_cat) = $b;
        
        for ($i = 0; $i < sizeof($bulk_item_ary[$this_itemid]); $i ++) {
            list ($this_groupid, $this_fundingid) = $bulk_item_ary[$this_itemid][$i];
            
            // retrieve group name
            $this_group_name = $linventory->returnGroupNameByGroupID($this_groupid);
            
            // retrieve funding name
            $this_funding_name = $linventory->returnFundingSourceByFundingID($this_fundingid);
            
            $table_content2 .= "<tr class=\"bulk\">";
            $table_content2 .= "<td class=\"tabletext\">$this_itemcode</td>";
            $table_content2 .= "<td class=\"tabletext\"><a href=\"variance_edit.php?ItemID=$this_itemid&GroupID=$this_groupid&FundingID=$this_fundingid\" class=\"tablelink\">$this_itemname</a></td>";
            $table_content2 .= "<td class=\"tabletext\">" . intranet_htmlspecialchars($this_cat) . "</td>";
            $table_content2 .= "<td class=\"tabletext\">$this_group_name</td>";
            $table_content2 .= "<td class=\"tabletext\">$this_funding_name</td>";
            $table_content2 .= "</tr>";
            $table_content2 .= "<tr class=\"inventory_row_underline\"><td colspan=\"5\"></td></tr>";
        }
    }
} else {
    $table_content2 .= "<tr class=\"tablerow2\">";
    $table_content2 .= "<td class=\"tabletext\" colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td>";
    $table_content2 .= "</tr>";
    $table_content2 .= "<tr class=\"inventory_row_underline\"><td colspan=\"5\"></td></tr>";
}

/*
 * if(($target_item_list == "") || ((sizeof($arr_surplus_record) == 0) && (sizeof($arr_missing_record) == 0)))
 * {
 * $table_content .= "<tr class=\"tablerow2\">";
 * $table_content .= "<td class=\"tabletext\" colspan=\"9\" align=\"center\">$i_no_record_exists_msg</td>";
 * $table_content .= "</tr>";
 * $table_content .= "<tr class=\"inventory_row_underline\"><td colspan=\"9\"></td></tr>";
 * }
 */

?>
<script language="javascript">
/* newly add by Ronald - used to reset the location_level and location when change Building or Floor selection */
function resetLocationSelection(CallFrom){
	if(CallFrom == 'Building'){
		if(document.getElementById("FloorSelected")){
			document.getElementById("FloorSelected").value = "";
		}
		if(document.getElementById("RoomSelected")){
			document.getElementById("RoomSelected").value = "";
		}
	}
	if(CallFrom == 'Floor'){
		if(document.getElementById("RoomSelected")){
			document.getElementById("RoomSelected").value = "";
		}
	}
}

function checkBatchResumeSingleItems(obj,element,page) {
	if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
    else{
        obj.action=page;                
        obj.method="POST";
        obj.submit();
    }
}


function click_export(b, f, r)
{
	window.location="variance_export.php?BuildingSelected=" + b + "&FloorSelected=" + f + "&RoomSelected=" + r;	
}
</script>
<br>

<form name="form1" action="" method="post">
	<table width="100%" border="0" cellpadding="3" cellspacing="0"
		align="center">
		<tr>
			<td colspan="2" align="right" class="tabletext"><?= $SysMsg ?></td>
		</tr>
	</table>

	<table width="100%" border="0" cellpadding="3" cellspacing="0"
		align="center">
		<tr>
			<td align="left"><?=$toolbar?></td>
		</tr>
		<tr>
			<td align="left"><?= $location_selection ?></td>
		</tr>
	</table>

	<!-- Single Item //-->
<?php
$table_tool = "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
						<a href=\"javascript:checkBatchResumeSingleItems(document.form1,'ItemID[]','variance_batch_edit.php')\" class=\"tabletool contenttool\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_common.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">" . $Lang['eInventory']['MoveToOriginalLocationByBatch'] . "
						</a>
					</td>";

if ($batch_resume_single_item_exist) :
    ?>
		<table width="100%" border="0" cellpadding="0" cellspacing="0"
		align="center">
		<tr>
			<td align="left">
					<?=$linterface->GET_NAVIGATION2($i_InventorySystem_ItemType_Single);?>			
				</td>

			<td colspan="" valign="bottom" align="right">
				<table border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="21"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif"
							width="21" height="23"></td>
						<td
							background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
							<table border="0" cellspacing="0" cellpadding="2">
								<tr>
										<?=$table_tool?>
									</tr>
							</table>
						</td>
						<td width="6"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif"
							width="6" height="23"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<?php else: ?>
	<?=$linterface->GET_NAVIGATION2($i_InventorySystem_ItemType_Single);?>	
<?php endif; ?>
		
<table border="0" width="100%" cellpadding="5" cellspacing="0">
		<tr class="tabletop">
			<td class="tabletopnolink"><?=$i_InventorySystem_Item_Code?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem_Item_Name?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem_Item_Barcode?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem['Category']?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem['Location']?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem['Caretaker']?></td>
<?
/*
 * ?>
 * <td class="tabletopnolink"><?=$i_InventorySystem_Stocktake_LastStocktakeBy?></td>
 * <td class="tabletopnolink"><?=$i_InventorySystem_Last_Stock_Take_Time?></td>
 * <?
 */
?>
<td class="tabletopnolink"><?=$i_InventorySystem_Stocktake_VarianceQty?></td>
			<td width="1">
<?=($batch_resume_single_item_exist ? "<input type=\"checkbox\" name=\"checkmaster\" onclick=\"(this.checked)?setChecked(1,this.form,'ItemID[]'):setChecked(0,this.form,'ItemID[]')\">" : "&nbsp;")?>	
</td>

		</tr>
<?=$table_content?>
</table>
	<br>
	<!-- Bulk Item //-->
<?=$linterface->GET_NAVIGATION2($i_InventorySystem_ItemType_Bulk);?>
<table border="0" width="100%" cellpadding="5" cellspacing="0">
		<tr class="tabletop">
			<td class="tabletopnolink"><?=$i_InventorySystem_Item_Code?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem_Item_Name?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem['Category']?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem['Caretaker']?></td>
			<td class="tabletopnolink"><?=$i_InventorySystem['FundingSource']?></td>
		</tr>
<?=$table_content2?>
</table>



	<input type="hidden" name="flag" value="1">

</form>

<br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
