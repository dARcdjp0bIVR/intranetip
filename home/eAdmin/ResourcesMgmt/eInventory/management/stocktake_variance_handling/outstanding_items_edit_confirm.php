<?php
// modifying :
/**
 * **************************** Change Log **************************************************
 * 2019-05-13 (Henry): Security fix: SQL without quote
 * 2019-05-01 (Henry) security issue fix for SQL
 * 2018-02-22 (Henry): add access right checking [Case#E135442]
 * ****************************** Change Log ************************************************
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable.php");
include_once ($PATH_WRT_ROOT . "includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$CurrentPageArr['eInventory'] = 1;
$CurrentPage = "Management_StocktakeVarianceHandling";
$linterface = new interface_html();
$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ItemID = IntegerSafe($ItemID);

$TAGS_OBJ[] = array(
    $i_InventorySystem_StocktakeVarianceHandling,
    "",
    0
);
$MODULE_OBJ = $linventory->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

echo $ItemID;
$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$ItemID."'";
$arr_item_type = $linventory->returnVector($sql);
$item_type = $arr_item_type[0];

if ($item_type == ITEM_TYPE_SINGLE) {}

if ($item_type == ITEM_TYPE_BULK) {
    echo $surplus_location_list;
    echo $missing_location_list;
    
    $arr_surplus_location = explode(",", $surplus_location_list);
    $arr_missing_location = explode(",", $missing_location_list);
    print_r($arr_surplus_location);
    print_r($arr_missing_location);
    
    // echo ${"move_to_qty"};
    // echo ${"relocate_to_qty"};
    // echo ${"write_off_qty"};
}

?>