<?php
// using: Henry

// ##############################
// Date 2019-05-01 (Henry)
// security issue fix for SQL
//
// Date: 2018-02-22 Henry
// add access right checking [Case#E135442]
//
// Date: 2015-06-24 Henry
// handle $variance_action = 8 case
//
// Date: 2015-05-27 Henry
// fix: incorrect write-off reason for bulk item [Case#K78790]
//
// Date: 2015-01-14 Henry
// Improvement: New quantity will not count the writeoff quantity
//
// Date: 2014-11-26 Henry
// Improvement: add writeoff reason
//
// Date: 2011-05-31 YatWoon
// Fixed: incorrect sql query that cannot save "Move Back To Original Location" history for single item
//
// Date: 2011-05-30 YatWoon
// Fixed: [Single] Incorrect Handler and Sender ID
//
// Date: 2011-05-26 YatWoon
// Fixed: [Single] haven't store attachement data
// Fixed: [Single] haven't remove the variance handling record if processed.
// Fixed: [Single] Incorrect sql field name
//
// Date: 2011-05-24 YAtWoon
// Fixed: remove duplicate variance handling notice before insert variance handling notice record
//
// Date: 2011-05-20 YAtWoon
// revised the logic according to the eInventory review meeting
//
// ##############################
$PATH_WRT_ROOT = "../../../../../../";
include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libinventory.php");
include_once ($PATH_WRT_ROOT . "includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linventory = new libinventory();

if (! $linventory->hasAccessRight($_SESSION['UserID'])) {
    include_once ($PATH_WRT_ROOT . "includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit();
}

$ItemID = IntegerSafe($ItemID);
$writeoff_reason = IntegerSafe($writeoff_reason);
$surplus_location_id = IntegerSafe($surplus_location_id);
$targetHandler = IntegerSafe($targetHandler);
$targetSublocation = IntegerSafe($targetSublocation);

$sql = "SELECT ItemType FROM INVENTORY_ITEM WHERE ItemID = '".$ItemID."'";
$arr_item_type = $linventory->returnVector($sql);
$ItemType = $arr_item_type[0];

$curr_date = date("Y-m-d");

if ($ItemType == ITEM_TYPE_SINGLE) {
    // remove the old non-handling reminder records
    $sql = "delete from INVENTORY_VARIANCE_HANDLING_REMINDER where ItemID = '".$ItemID."' and RecordStatus=0";
    $linventory->db_db_query($sql);
    
    if ($variance_action == 1) // Move back #
{
        $sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($ItemID, '$curr_date', " . ITEM_ACTION_MOVEBACKTOORGINIAL . ", $UserID, '', 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "SELECT
					b.LocationID
				FROM
					INVENTORY_ITEM AS a INNER JOIN
					INVENTORY_ITEM_SINGLE_EXT AS b ON (a.ItemID = b.ItemID)
				WHERE
					a.ItemID = $ItemID
				";
        $arr_new_location_id = $linventory->returnVector($sql);
        $new_location_id = $arr_new_location_id[0];
        
        $sql = "INSERT INTO 
						INVENTORY_VARIANCE_HANDLING_REMINDER
						(HandlerID, SenderID, ItemID, Quantity, OriginalLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($targetHandler, $UserID, $ItemID, 1, $surplus_location_id, $new_location_id, NOW(), 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    
    if ($variance_action == 2) // Relocation
{
        // retrieve original location
        $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("c.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . "),
						b.LocationID
				FROM 
						INVENTORY_ITEM_SINGLE_EXT AS a INNER JOIN 
						INVENTORY_LOCATION AS b ON (a.LocationID = b.LocationID) INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS c ON (b.LocationLevelID = c.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS LocBuilding ON (c.BuildingID = LocBuilding.BuildingID)
				WHERE
						a.ItemID = '".$ItemID."'
				";
        $arr_original_location = $linventory->returnArray($sql);
        $original_location = $arr_original_location[0][0];
        // $original_location_id = $arr_original_location[0][1];
        
        // retrieve stocktake location
        $stocktake_location_id = $surplus_location_id ? $surplus_location_id : $missing_location_id;
        
        // new location
        $sql = "SELECT 
						CONCAT(" . $linventory->getInventoryNameByLang("LocBuilding.") . ",' > '," . $linventory->getInventoryNameByLang("b.") . ",' > '," . $linventory->getInventoryNameByLang("a.") . ")
				FROM
						INVENTORY_LOCATION AS a INNER JOIN 
						INVENTORY_LOCATION_LEVEL AS b ON (a.LocationLevelID = b.LocationLevelID) INNER JOIN
						INVENTORY_LOCATION_BUILDING AS LocBuilding ON (b.BuildingID = LocBuilding.BuildingID)
				WHERE
						a.LocationID = '".$targetSublocation."'
				";
        $arr_new_location = $linventory->returnVector($sql);
        $new_location = $arr_new_location[0];
        
        $remark = "$i_InventorySystem_From: $original_location<BR>";
        $remark .= "$i_InventorySystem_To: $new_location";
        $remark = addslashes($remark);
        
        // Relocation #
        $sql = "UPDATE 
					INVENTORY_ITEM_SINGLE_EXT
				SET
					LocationID = '".$targetSublocation."'
				WHERE 
					ItemID = '".$ItemID."'
				";
        $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO
					INVENTORY_ITEM_SINGLE_STATUS_LOG
					(ItemID, RecordDate, Action, PersonInCharge, Remark, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($ItemID, '$curr_date', " . ITEM_ACTION_CHANGELOCATION . ", $UserID, '$remark', 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO 
						INVENTORY_VARIANCE_HANDLING_REMINDER
						(HandlerID, SenderID, ItemID, Quantity, OriginalLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($targetHandler, $UserID, $ItemID, 1, $stocktake_location_id, $targetSublocation, NOW(), 0, 0 , NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    
    if ($variance_action == 3) // # Write off
{
        $sql = "SELECT LocationID, AdminGroupID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$ItemID."'";
        $arr_tmp_result = $linventory->returnArray($sql, 2);
        if (sizeof($arr_tmp_result) > 0) {
            list ($write_off_location, $write_off_admin) = $arr_tmp_result[0];
        }
        
        /*
         * $sql = "SELECT ".$linventory->getInventoryItemWriteOffReason()." FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = $targetWriteOffReason";
         * $arr_write_off_reason = $linventory->returnVector($sql);
         * $write_off_reason = addslashes($arr_write_off_reason[0]);
         */
        
        // ## Check For the write off record is exist or not, exist->delete, then insert ###
        $sql = "SELECT 
					RecordID 
			FROM 
					INVENTORY_ITEM_WRITE_OFF_RECORD 
			WHERE 
					ItemID = '".$ItemID."' AND 
					LocationID = '".$write_off_location."' AND 
					AdminGroupID = '".$write_off_admin."' AND 
					RecordStatus = 0
			";
        $arr_record_exist = $linventory->returnVector($sql);
        if (sizeof($arr_record_exist) > 0) {
            foreach ($arr_record_exist as $k1 => $d1) {
                $exist_record_id = $d1;
                
                // # delete INVENTORY_ITEM_WRITE_OFF_RECORD
                $sql = "delete from INVENTORY_ITEM_WRITE_OFF_RECORD where RecordID = '".$exist_record_id."'";
                $linventory->db_db_query($sql);
                
                // # delete files
                // # implement if necessary
                
                // # delete INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
                $sql = "delete from INVENTORY_ITEM_WRITE_OFF_ATTACHMENT where RequestWriteOffID = '".$exist_record_id."'";
                $linventory->db_db_query($sql);
            }
        }
        // ## [End] Check For the write off record is exist or not, exist->delete, then insert [End] ###
        if (trim($writeoff_reason) == 0)
            $write_off_reason = $i_InventorySystem_Action_Stocktake_Not_Found;
        else {
            $sql = "SELECT " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = '".$writeoff_reason."'";
            $arr_write_off_reason = $linventory->returnVector($sql);
            $write_off_reason = $arr_write_off_reason[0];
            $write_off_reason = addslashes($write_off_reason);
        }
        $sql = "INSERT INTO 
					INVENTORY_ITEM_WRITE_OFF_RECORD
					(ItemID, RecordDate, LocationID, AdminGroupID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, 
					RecordType, RecordStatus, DateInput, DateModified)
				VALUES
					($ItemID, '$curr_date', $write_off_location, $write_off_admin, 1, '$write_off_reason', '$curr_date', $UserID, 
					0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        /*
         * $write_off_logID = $linventory->db_insert_id();
         * $file = $_FILES["write_off_attachment"]["name"];
         * $re_file = stripslashes($write_off_attachment);
         * // $re_file = stripslashes(${"single_item_write_off_attachment_".$single_item_id."_".$j});
         *
         * if($re_file == "none" || $re_file == ""){
         * }
         * else
         * {
         * $re_path = "/file/inventory/write_off";
         * $path = "$intranet_root$re_path";
         *
         * $photo_path = $path . "/".$write_off_logID;
         * $photo_re_path = $re_path . "/".$write_off_logID;
         *
         * $lf = new libfilesystem();
         * if (!is_dir($photo_path))
         * {
         * $lf->folder_new($photo_path);
         * }
         *
         * $target = "$photo_path/$file";
         *
         * if($lf->lfs_copy($re_file, $target) == 1)
         * {
         * $sql = "INSERT INTO
         * INVENTORY_ITEM_WRITE_OFF_ATTACHMENT
         * (ItemID, RequestWriteOffID, PhotoPath, PhotoName, DateInput, DateModified)
         * VALUES
         * ($ItemID, $write_off_logID, '$photo_re_path', '$file', NOW(), NOW())
         * ";
         * $linventory->db_db_query($sql);
         * }
         * }
         */
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    
    if ($variance_action == 4) {
        $sql = "UPDATE
					INVENTORY_ITEM_SINGLE_EXT
				SET
					LocationID = '".$surplus_location_id."'
				WHERE
					ItemID = '".$ItemID."'
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "INSERT INTO 
						INVENTORY_ITEM_SINGLE_STATUS_LOG
						(ItemID, RecordDate, Action, PersonInCharge, RecordType, RecordStatus, DateInput, DateModified)
				VALUES
						($ItemID, '$curr_date', " . ITEM_ACTION_PURCHASE . ", $UserID, 0, 0, NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE
					INVENTORY_ITEM
				SET
					RecordStatus = 1
				WHERE
					ItemID = '".$ItemID."'
				";
        $linventory->db_db_query($sql);
    }
    
    if ($variance_action == 5) {
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    
    if ($variance_action == 6) // found in original place
{
        $sql = "INSERT INTO 
						INVENTORY_ITEM_SINGLE_STATUS_LOG
						(ItemID, RecordDate, Action, PastStatus, NewStatus, PersonInCharge, Remark, RecordType,
						RecordStatus, DateInput, DateModified)
				VALUES
						($ItemID, '$curr_date', " . ITEM_ACTION_STOCKTAKE_ORIGINAL_LOCATION . ", '', '', $UserID, '', '', '', NOW(), NOW())
				";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
    
    if ($variance_action == 8) // remove the variance edit record only
{
        $sql = "UPDATE INVENTORY_ITEM_SURPLUS_RECORD SET SurplusQty = SurplusQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
        
        $sql = "UPDATE INVENTORY_ITEM_MISSING_RECORD SET MissingQty = MissingQty - 1 WHERE ItemID = '".$ItemID."'";
        $linventory->db_db_query($sql);
    }
}

if ($ItemType == ITEM_TYPE_BULK) {
    if ($variance_location_list != "") {
        $arr_variance_location = explode(",", $variance_location_list);
        
        if (sizeof($arr_variance_location) > 0) {
            // ## request write-off
            // remove old request record first
            $sql = "delete from INVENTORY_ITEM_WRITE_OFF_RECORD where ItemID='".$ItemID."' and AdminGroupID='".$GroupID."' and FundingSourceID='".$FundingID."' and RecordStatus=0";
            $linventory->db_db_query($sql);
            
            for ($i = 0; $i < sizeof($arr_variance_location); $i ++) {
                $variance_location_id = $arr_variance_location[$i];
                $this_location_new_normal = ${"new_qty_$variance_location_id"};
                $this_location_new_damage = ${"new_damage_$variance_location_id"};
                $this_location_new_repair = ${"new_repair_$variance_location_id"};
                $this_location_new_write_off = ${"new_write_off_$variance_location_id"};
                $this_location_new_write_off_reason = ${"new_write_off_reason_$variance_location_id"};
                $this_location_remark = ${"remark_$variance_location_id"};
                
                // retrieve original qty
                $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = '".$ItemID."' AND LocationID = '".$variance_location_id."' and GroupInCharge='".$GroupID."' and FundingSourceID='".$FundingID."'";
                $arr_original_qty = $linventory->returnVector($sql);
                $original_qty = $arr_original_qty[0];
                
                // update bulk item qty (location + group + funding)
                $location_new_qty = $this_location_new_normal + $this_location_new_damage + $this_location_new_repair/* + $this_location_new_write_off*/;
                $sql = "UPDATE 
								INVENTORY_ITEM_BULK_LOCATION 
						SET
								Quantity = '".$location_new_qty."'
						WHERE
								ItemID = '".$ItemID."' AND
								LocationID = '".$variance_location_id."'
								and GroupInCharge='".$GroupID."'
								and FundingSourceID = '".$FundingID."'
						";
                $linventory->db_db_query($sql);
                
                // #### update location status
                $sql = "INSERT INTO
							INVENTORY_ITEM_BULK_LOG
							(ItemID, RecordDate, Action, QtyChange, QtyNormal, QtyDamage, QtyRepair, PersonInCharge, LocationID, GroupInCharge, FundingSource, DateInput, DateModified)
						VALUES
							($ItemID, '$curr_date', " . ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING . ", $location_new_qty, $this_location_new_normal, $this_location_new_damage, $this_location_new_repair, $UserID, $variance_location_id, $GroupID, $FundingID, NOW(), NOW())
						";
                $linventory->db_db_query($sql);
                
                // ## Remove duplicate variance handling reminder record
                $sql = "delete from INVENTORY_VARIANCE_HANDLING_REMINDER where NewLocationID = '".$variance_location_id."' and ItemID='".$ItemID."' and RecordStatus=0 and GroupInCharge='".$GroupID."' and FundingSourceID='".$FundingID."'";
                $linventory->db_db_query($sql);
                
                // ## Send variance handling reminder ###
                $handler_id = ${"targetHandler_$variance_location_id"};
                $sql = "INSERT INTO 
								INVENTORY_VARIANCE_HANDLING_REMINDER
								(HandlerID, SenderID, ItemID, OriginalQty, Quantity, OriginalLocationID, NewLocationID, GroupInCharge, FundingSourceID, SendDate, RecordType, RecordStatus, DateInput, DateModified, Remark)
						VALUES
								($handler_id, $UserID, $ItemID, $original_qty, $location_new_qty, $variance_location_id, $variance_location_id, $GroupID, $FundingID, NOW(), 0, 0, NOW(), NOW(),'$this_location_remark')";
                $linventory->db_db_query($sql);
                
                if ($this_location_new_write_off > 0) {
                    if (trim($this_location_new_write_off_reason) == 0)
                        $write_off_reason = $i_InventorySystem_Action_Stocktake_Not_Found;
                    else {
                        $sql = "SELECT " . $linventory->getInventoryItemWriteOffReason() . " FROM INVENTORY_WRITEOFF_REASON_TYPE WHERE ReasonTypeID = '".$this_location_new_write_off_reason."'";
                        $arr_write_off_reason = $linventory->returnVector($sql);
                        $write_off_reason = $arr_write_off_reason[0];
                        $write_off_reason = addslashes($write_off_reason);
                    }
                    $sql = "INSERT INTO 
								INVENTORY_ITEM_WRITE_OFF_RECORD
								(ItemID, RecordDate, LocationID, AdminGroupID, FundingSourceID, WriteOffQty, WriteOffReason, RequestDate, RequestPerson, 
								RecordType, RecordStatus, DateInput, DateModified, ByStocktake)
							VALUES
								($ItemID, '$curr_date', $variance_location_id, $GroupID, $FundingID, $this_location_new_write_off, '$write_off_reason', '$curr_date', $UserID, 
								0, 0, NOW(), NOW(),1)
							";
                    $linventory->db_db_query($sql);
                }
                
                /*
                 * $location_new_qty = $this_location_new_normal + $this_location_new_damage + $this_location_new_repair;
                 * $new_bulk_item_total_qty = $new_bulk_item_total_qty + $location_new_qty;
                 *
                 * ##### update location qty
                 * $sql = "UPDATE
                 * INVENTORY_ITEM_BULK_LOCATION
                 * SET
                 * Quantity = $location_new_qty
                 * WHERE
                 * ItemID = $ItemID AND
                 * LocationID = $variance_location_id
                 * ";
                 * $linventory->db_db_query($sql);
                 *
                 * ##### update location status
                 * $sql = "INSERT INTO
                 * INVENTORY_ITEM_BULK_LOG
                 * (ItemID, RecordDate, Action, QtyChange, QtyNormal, QtyDamage, QtyRepair, PersonInCharge, LocationID, DateInput, DateModified)
                 * VALUES
                 * ($ItemID, '$curr_date', ".ITEM_ACTION_STOCKTAK_VARIANCE_HANDLING.", $location_new_qty, $this_location_new_normal, $this_location_new_damage, $this_location_new_repair, $UserID, $variance_location_id, NOW(), NOW())
                 * ";
                 * $linventory->db_db_query($sql);
                 *
                 * if($location_new_qty > 0)
                 * {
                 * ### Remove duplicate variance handling reminder record
                 * $sql = "delete from INVENTORY_VARIANCE_HANDLING_REMINDER where NewLocationID = $variance_location_id and ItemID=$ItemID and RecordStatus=0";
                 * $linventory->db_db_query($sql);
                 *
                 * ### Send variance handling reminder ###
                 * $handler_id = ${"targetHandler_$variance_location_id"};
                 * $sql = "SELECT Quantity FROM INVENTORY_ITEM_BULK_LOCATION WHERE ItemID = $ItemID AND LocationID = $variance_location_id";
                 * $arr_original_qty = $linventory->returnVector($sql);
                 * $original_qty = $arr_original_qty[0];
                 *
                 * $sql = "INSERT INTO
                 * INVENTORY_VARIANCE_HANDLING_REMINDER
                 * (HandlerID, SenderID, ItemID, OriginalQty, Quantity, OriginalLocationID, NewLocationID, SendDate, RecordType, RecordStatus, DateInput, DateModified, Remark)
                 * VALUES
                 * ($handler_id, $UserID, $ItemID, $original_qty, $location_new_qty, $variance_location_id, $variance_location_id, NOW(), 0, 0, NOW(), NOW(),'$this_location_remark')";
                 * $linventory->db_db_query($sql);
                 * }
                 * else
                 * {
                 * ### Remove bulk location record
                 * $sql = "delete from INVENTORY_ITEM_BULK_LOCATION where ItemID=$ItemID and LocationID=$variance_location_id";
                 * $linventory->db_db_query($sql);
                 * }
                 *
                 * $sql = "SELECT RecordID FROM INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = $ItemID AND LocationID = $variance_location_id";
                 * $arr_surplus_result = $linventory->returnVector($sql);
                 * if(sizeof($arr_surplus_result) > 0)
                 * {
                 * $surplus_record_id = $arr_surplus_result[0];
                 * $sql = "delete from INVENTORY_ITEM_SURPLUS_RECORD WHERE RecordID = $surplus_record_id AND ItemID = $ItemID AND LocationID = $variance_location_id";
                 * $linventory->db_db_query($sql);
                 * }
                 *
                 * $sql = "SELECT RecordID FROM INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = $ItemID AND LocationID = $variance_location_id";
                 * $arr_missing_result = $linventory->returnVector($sql);
                 * if(sizeof($arr_missing_result) > 0)
                 * {
                 * $missing_record_id = $arr_missing_result[0];
                 * $sql = "delete from INVENTORY_ITEM_MISSING_RECORD WHERE RecordID = $missing_record_id AND ItemID = $ItemID AND LocationID = $variance_location_id";
                 * $linventory->db_db_query($sql);
                 * }
                 */
            }
            
            // ## update INVENTORY_ITEM_SURPLUS_RECORD and INVENTORY_ITEM_MISSING_RECORD
            $sql = "delete from INVENTORY_ITEM_SURPLUS_RECORD WHERE ItemID = '".$ItemID."' AND AdminGroupID='".$GroupID."' and FundingSourceID='".$FundingID."'";
            $linventory->db_db_query($sql);
            
            $sql = "delete from INVENTORY_ITEM_MISSING_RECORD WHERE ItemID = '".$ItemID."' AND AdminGroupID='".$GroupID."' and FundingSourceID='".$FundingID."'";
            $linventory->db_db_query($sql);
            
            // #### update bulk item total qty
            $sql = "select sum(Quantity) from INVENTORY_ITEM_BULK_LOCATION where ItemID='".$ItemID."'";
            $result = $linventory->returnVector($sql);
            $new_total_qty = $result[0];
            $sql = "update INVENTORY_ITEM_BULK_EXT set Quantity = '".$new_total_qty."' where ItemID='".$ItemID."'";
            $linventory->db_db_query($sql);
        }
    }
}

// ### Remove qty = 0 record (double check)
$sql = "delete from INVENTORY_ITEM_SURPLUS_RECORD where SurplusQty=0";
$linventory->db_db_query($sql);
$sql = "delete from INVENTORY_ITEM_MISSING_RECORD where MissingQty=0";
$linventory->db_db_query($sql);

intranet_closedb();
header("location: outstanding_items.php?msg=2");
?>