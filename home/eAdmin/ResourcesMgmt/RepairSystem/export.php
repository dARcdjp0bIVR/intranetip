<?php
// Modifying by : 

############ Change Log Start ###############
#
#   Date    :   2020-10-28 (Cameron)
#               modify fitler condition to allow follow-up person (system) to export self record, support showing multiple follow-up person of a repair request record
#
#   Date    :   2020-09-09  (Cameron)
#               Fix: add filter display_own (IP.2.5.11.9.1)
#
#	Date	:	2017-10-11  (Cameron)
#				Fix: show left reporter as -- (case #N128259)
#
#	Date	:	2017-05-31	(Cameron)
#				- Use lang file for $Lang['RepairSystem']['Export']['RefNo'] and $Lang['RepairSystem']['Export']['FollowupPerson']
#				Fix: support special character search like '"<>&
#
#	Date	:	2014-11-06	(Omas)
#				Improved : Add new column - case number for reference
#
#	Date	:	2011-03-28 (Henry Chow)
#				Improved : select "Location" from "School Setting > Campus"
#
#	Date	:	2010-12-23	YAtWoon
#				Fixed: export result missing cater request date and complete date
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();
$lexport = new libexporttext();

# check access right
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# SQL condition
if($CatID!="")
	$conds .= " AND a.CategoryID=$CatID";
/*
if($locID!="")
	$conds .= " AND a.LocationID=$locID";
*/
if($buildingID != "")
	$conds .= " AND bu.BuildingID='$buildingID'";
	
if($Status!="")
	$conds .= " AND a.RecordStatus=$Status";

if($year!="")
	$conds .= " AND a.DateInput LIKE '$year-%'";
	
if($month!="") {
	if($month<=9) $m = '0'.$month;
	$conds .= " AND a.DateInput LIKE '%-$m-%'";
}

if($display_own)
{
    $conds .= " AND a.FollowUpPersonID=$UserID";
}


$followupUserCond = "";
$isAdminUser = $lrepairsystem->IS_ADMIN_USER($UserID);
if(!$isAdminUser){
    $myFollowupRecordIDAry = $lrepairsystem->getMyFollowupRecord($_SESSION['UserID']);
    if (count($myFollowupRecordIDAry)) {
        $followupUserCond = "a.RecordID IN ('" . implode("','", (array)$myFollowupRecordIDAry) . "')";
    }
}

$keyword = standardizeFormGetValue($_GET['keyword']);
if($keyword!="")
	$ukw = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));				// A&<>'"\B ==> A&<>\'\"\\\\B
	$ckw = intranet_htmlspecialchars(str_replace("\\","\\\\",$_GET['keyword']));	// A&<>'"\B ==> A&amp;&lt;&gt;\&#039;&quot;\\\\B
	
	$conds .= " AND (
					b.EnglishName LIKE '%$ukw%' OR
					b.ChineseName LIKE '%$ukw%' OR
					a.Title LIKE '%$ukw%' OR
					a.DetailsLocation LIKE '%$ukw%' OR
					a.Content LIKE '%$ukw%' OR
					a.CaseNumber LIKE '%$ukw%'OR
					a.FollowUpPerson LIKE '%$ukw%' OR
					b.EnglishName LIKE '%$ckw%' OR
					b.ChineseName LIKE '%$ckw%' OR
					a.Title LIKE '%$ckw%' OR
					a.DetailsLocation LIKE '%$ckw%' OR
					a.Content LIKE '%$ckw%' OR
					a.CaseNumber LIKE '%$ckw%'OR
					a.FollowUpPerson LIKE '%$ckw%'
				)";
				
if($requestFlag==1) 
	$conds .= " AND a.DateInput BETWEEN '$requestStart 00:00:00' AND '$requestEnd 23:59:59'";

if($completeFlag==1) 
	$conds .= " AND a.CompleteDate BETWEEN '$completeStart 00:00:00' AND '$completeEnd 23:59:59'";
	
if(!$isAdminUser) {
	$sql = "SELECT CAT.CategoryID FROM REPAIR_SYSTEM_CATEGORY CAT LEFT OUTER JOIN REPAIR_SYSTEM_GROUP_MEMBER GPM ON (GPM.GroupID=CAT.GroupID) WHERE GPM.UserID=$UserID";
	$result = $lrepairsystem->returnVector($sql);
    if(sizeof($result)) {
        if ($followupUserCond) {
            $conds .= " AND (a.CategoryID IN (" . implode(',', $result) . ") OR ".$followupUserCond.")";
        }
        else {
            $conds .= " AND a.CategoryID IN (" . implode(',', $result) . ")";
        }
    }
    else {
        if ($followupUserCond) {
            $cond .= " AND " . $followupUserCond;
        }
        else {
            $conds .= " AND a.CategoryID IN (0)";
        }
    }
}

if($archive==1)
	$conds .= " AND a.ArchivedBy IS NOT NULL";
else
	$conds .= " AND a.ArchivedBy IS NULL";


# request summary
$other_from .= $RequestID ? " and e.RecordID=$RequestID " :"";

# SQL statement
$name_field = getNameFieldByLang("b.");
$buildingName = Get_Lang_Selection("bu.NameChi", "bu.NameEng");
$levelName = Get_Lang_Selection("lv.NameChi", "lv.NameEng");
$LocationName = Get_Lang_Selection("l.NameChi", "l.NameEng");
$FollowUpPersonName= getNameFieldByLang("u.");

$sql = "
		SELECT 
			IF(b.UserLogin IS NULL,'".$Lang['General']['EmptySymbol']."',b.UserLogin) as UserLogin,
			IF($name_field IS NULL,'".$Lang['General']['EmptySymbol']."',$name_field) as name,
			LEFT(a.DateInput,10) as DateInput, 
			IF($buildingName IS NOT NULL, $buildingName, '') as BuildingName, 
			IF($levelName!='', $levelName, '') as LocationLevel, 
			IF($LocationName!='', $LocationName, '') as LocationName, 
			IF(a.LocationBuildingID!=0, a.DetailsLocation, CONCAT(c.LocationName,' &gt; ', a.DetailsLocation)) as DetailsLocation,
			a.Title as title,
			a.Content,
			CAT.Name as catName,
			a.Remark,
			case a.RecordStatus
				when '1' then '". $Lang['RepairSystem']['Processing'] ."'
				when '2' then '". $Lang['General']['Completed'] ."'
				when '3' then '". $i_status_rejected ."'
				when '4' then '". $Lang['RepairSystem']['Pending']."'
				else '". $i_status_cancel."'
			end as status,
			a.CaseNumber,
			IF (
				p.RecordID is null,
					IF(a.FollowUpPerson IS NULL OR a.FollowUpPerson='','".$Lang['General']['EmptySymbol']."',a.FollowUpPerson),
					p.FollowupPerson
			) as FollowUpPerson			
		FROM 
			REPAIR_SYSTEM_RECORDS as a
			LEFT OUTER join INTRANET_USER as b on (b.UserID=a.UserID)
			LEFT JOIN INVENTORY_LOCATION l ON (l.LocationID=a.LocationID)
			LEFT JOIN INVENTORY_LOCATION_LEVEL lv ON ((a.LocationLevelID=lv.LocationLevelID) AND lv.RecordStatus='".REPAIR_SYSTEM_STATUS_APPROVED."')
			LEFT JOIN INVENTORY_LOCATION_BUILDING as bu on ((a.LocationBuildingID=bu.BuildingID) AND bu.RecordStatus='".REPAIR_SYSTEM_STATUS_APPROVED."')
			LEFT join REPAIR_SYSTEM_LOCATION as c on (c.LocationID=a.LocationID)
    		LEFT JOIN (
				SELECT 	p.RecordID, 
						GROUP_CONCAT(DISTINCT ".$FollowUpPersonName." ORDER BY u.EnglishName SEPARATOR ', ') AS FollowupPerson,
						GROUP_CONCAT(DISTINCT u.EnglishName ORDER BY u.EnglishName SEPARATOR ', ') AS SortFollowupPerson
				FROM 
						REPAIR_SYSTEM_FOLLOWUP_PERSON p
				INNER JOIN 
						INTRANET_USER u ON u.UserID=p.PersonID
				GROUP BY p.RecordID
			) AS p ON p.RecordID=a.RecordID			
			INNER JOIN REPAIR_SYSTEM_CATEGORY AS CAT ON (CAT.CategoryID=a.CategoryID)
			INNER JOIN REPAIR_SYSTEM_GROUP AS GP ON (GP.GroupID=CAT.GroupID)
			inner join REPAIR_SYSTEM_REQUEST_SUMMARY as e on (e.Title=a.Title and e.CategoryID=a.CategoryID  $other_from)
		WHERE 
			a.RecordStatus!=".REPAIR_SYSTEM_STATUS_DELETED." 
			and e.RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED."
			$conds
		ORDER BY
			LEFT(a.DateInput,10), a.RecordID
		";	
	//echo $sql;
$result = $lrepairsystem->returnArray($sql);

for($i=0; $i<sizeof($result); $i++)
{
	 	
 	$ExportArr[$i][0] = $result[$i]['UserLogin'];	//user login
 	$ExportArr[$i][1] = $result[$i]['name'];		//name		
	$ExportArr[$i][2] = $result[$i]['DateInput'];			//DateInput
	$ExportArr[$i][3] = $result[$i]['catName'];			//category name
	$ExportArr[$i][4] = $result[$i]['BuildingName'];	//Building
	$ExportArr[$i][5] = $result[$i]['LocationLevel'];	//Location Level
	$ExportArr[$i][6] = $result[$i]['LocationName'];	//Location
	$ExportArr[$i][7] = $result[$i]['DetailsLocation'];	//Location Detail
	$ExportArr[$i][8] = $result[$i]['title'];			//title
	$ExportArr[$i][9] = $result[$i]['Content'];			//Content
	$ExportArr[$i][10] = $result[$i]['Remark'];			//Remark
	$ExportArr[$i][11] = $result[$i]['status'];			//RecordStatus
	$ExportArr[$i][12] = $result[$i]['CaseNumber'];		//case number
	$ExportArr[$i][13] = $result[$i]['FollowUpPerson'];		//FollowUpPerson
}
intranet_closedb();
//debug_pr($ExportArr);
//$exportColumn = array($i_Discipline_Date,$Lang['RepairSystem']['Reporter'],$Lang['RepairSystem']['Location'],$Lang['RepairSystem']['LocationDetail'],$Lang['RepairSystem']['RequestSummary'],$Lang['RepairSystem']['RequestDetails'],$Lang['RepairSystem']['Category'],$i_UserRemark,$i_general_status);

$exportColumn[0] = $Lang['RepairSystem']['ImportField_IP25']['EN'];
$exportColumn[1] = $Lang['RepairSystem']['ImportField_IP25']['B5'];
$exportColumn[0][] = 'Case No.';
$exportColumn[1][] = $Lang['RepairSystem']['Export']['RefNo'];
$exportColumn[0][] = 'Follow-up Person';
$exportColumn[1][] = $Lang['RepairSystem']['Export']['FollowupPerson'];

//$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");
$export_content .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");


$filename = 'RepairSystemRecord.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>