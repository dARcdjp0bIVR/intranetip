<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();
if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

$CurrentPage = "PageRequestStatistics";
$CurrentPageArr['eAdminRepairSystem'] = 1;
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['RepairSystem']['RequestStatistics']);
$linterface->LAYOUT_START();

$category_selection = $lrepairsystem->getCategorySelection($CatID," multiple size=10 ", "", 0, 1, "CatID[]");
$select_all_btn = $linterface->GET_BTN($button_select_all, "button", "SelectAll();");

$form_display = $lrepairsystem->returnRequestStatisticsForm_ui($startdate, $enddate,1, $orderby);

$CatID_str = implode(",",$CatID);
$sql = "select 
			c.Name as CategoryName,
			a.Title as Title,
			count(a.Title) as c
		from
			REPAIR_SYSTEM_RECORDS as a
			left join REPAIR_SYSTEM_REQUEST_SUMMARY as b on (b.Title = a.Title)
			left join REPAIR_SYSTEM_CATEGORY as c on (c.CategoryID=b.CategoryID)
		where 
			left(a.DateInput,10) >='$startdate' and
			left(a.DateInput,10) <='$enddate' and
			b.CategoryID in ($CatID_str)
		Group by 
			c.CategoryID, a.Title
		order by 
			$orderby
";
$result = $lrepairsystem->returnArray($sql);

$display = "<div class='table_board'>";
$display .="<table class='common_table_list_v30 view_table_list_v30'>";
$display .= "<tr>";
$display .= "<th>&nbsp;</th>";
$display .= "<th>". $Lang['RepairSystem']['Category'] ."</th>";
$display .= "<th>". $Lang['RepairSystem']['RequestSummary'] ."</th>";
$display .= "<th>". $Lang['RepairSystem']['NoOfRecords'] ."</th>";
$display .= "</tr>";

for ($i=0; $i<sizeof($result); $i++)
{
	list($thisCatName, $thisTitle, $thisCount) = $result[$i];
	
	$display .= '<tr>';
	$display .= '<td valign="top">'.($i+1).'</td>';
	$display .= '<td valign="top">'.$thisCatName.'</td>';
	$display .= '<td valign="top">'.$thisTitle.'</td>';
	$display .= '<td valign="top">'.$thisCount.'</td>';
	$display .= '</tr>';
	
}

if(sizeof($result)==0)
{
	$display .= '<tr>';
	$display .= '<td valign="top" colspan="4" align="center"><br>'.$i_no_record_exists_msg.'<br><br></td>';
	$display .= '</tr>';
}

$display .= '</table>';
$display .= "</div>"; 

?>

<script language="javascript">
<!--
function hideOption()
{
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOption()
{
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}
function doExport()
{
	window.location='export.php?startdate=<?=$startdate?>&enddate=<?=$enddate?>&CatID_str=<?=$CatID_str?>&orderby=<?=$orderby?>';
}

function doPrint()
{
	newWindow('print.php?startdate=<?=$startdate?>&enddate=<?=$enddate?>&CatID_str=<?=$CatID_str?>&orderby=<?=$orderby?>',10);
}
//-->
</script>

<div id="div_form" class="report_option report_hide_option">
	<span id="spanShowOption" class="spanShowOption">
		<a href="javascript:showOption();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOption();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	<span class="Form_Span" style="display:none"><?=$form_display?></span>
</div>

<?=$display?>

<? if(sizeof($result)) {?>
<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_print, "button", "doPrint();")?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_export, "button", "doExport();")?>&nbsp;
<p class="spacer"></p>
</div>
<? } ?>

<?
$linterface->LAYOUT_STOP();
?>
 