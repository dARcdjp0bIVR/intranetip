<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();
$le = new libexporttext();

$sql = "select 
			c.Name as CategoryName,
			a.Title as Title,
			count(a.Title) as c
		from
			REPAIR_SYSTEM_RECORDS as a
			left join REPAIR_SYSTEM_REQUEST_SUMMARY as b on (b.Title = a.Title)
			left join REPAIR_SYSTEM_CATEGORY as c on (c.CategoryID=b.CategoryID)
		where 
			left(a.DateInput,10) >='$startdate' and
			left(a.DateInput,10) <='$enddate' and
			b.CategoryID in ($CatID_str)
		Group by 
			c.CategoryID, a.Title
		order by 
			$orderby
";

$result = $lrepairsystem->returnArray($sql);

$export_header = array();
$export_ary = array();

$export_header = array(" ", $Lang['RepairSystem']['Category'], $Lang['RepairSystem']['RequestSummary'], $Lang['RepairSystem']['NoOfRecords']);


for ($i=0; $i<sizeof($result); $i++)
{
	list($thisCatName, $thisTitle, $thisCount) = $result[$i];
	
	$export_ary[] = array($i+1, $thisCatName, $thisTitle, $thisCount);
}



$export_text = $le->GET_EXPORT_TXT($export_ary,$export_header);
$filename = "request_statistics.csv";
$le->EXPORT_FILE($filename,$export_text); 
    
intranet_closedb();
?>
