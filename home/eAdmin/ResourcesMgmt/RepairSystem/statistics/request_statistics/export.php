<?php
// Modifying by : 

####### Change log [Start] #######
#
#	Date:	2017-06-02  Cameron
#			should pass parameter $Quoted="11" when export so that special character won't break columns 
#
#	Date:	2017-05-23	Cameron
#			add Location filter
#
#	Date:	2010-12-23	YatWoon
#			add status selection
#
####### Change log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();
$le = new libexporttext();


if(sizeof($_POST)==0 && sizeof($_GET)==0 && $Status=="")
	$Status = 1;
	
if($Status!="")
	$conds .= " AND a.RecordStatus=$Status";

$conds .= $TargetFloor ? " and a.LocationLevelID='".$TargetFloor."'" : ""; 	
$conds .= $TargetRoom ? " and a.LocationID IN ('".str_replace(",","','",$TargetRoom)."')" : "";
	
$sql = "select 
			c.Name as CategoryName,
			a.Title as Title,
			count(a.Title) as c,
			a.CategoryID
		from
			REPAIR_SYSTEM_RECORDS as a
			left join REPAIR_SYSTEM_REQUEST_SUMMARY as b on (b.Title = a.Title)
			left join REPAIR_SYSTEM_CATEGORY as c on (c.CategoryID=b.CategoryID)
		where 
			left(a.DateInput,10) >='$startdate' and
			left(a.DateInput,10) <='$enddate' and
			b.CategoryID in ($CatID_str) and
			b.RecordStatus=1 AND a.RecordStatus != ".REPAIR_SYSTEM_STATUS_DELETED."
			$conds
		Group by 
			c.CategoryID, a.Title
		order by 
			$orderby
";


$result = $lrepairsystem->returnArray($sql);

$export_header = array();
$export_ary = array();

if($withArchive) $text = " (".$Lang['RepairSystem']['NoOfArchivedRecordIncluded'].")";

$export_header = array(" ", $Lang['RepairSystem']['Category'], $Lang['RepairSystem']['RequestSummary'], $Lang['RepairSystem']['NoOfRecords'].$text);


for ($i=0; $i<sizeof($result); $i++)
{
	list($thisCatName, $thisTitle, $thisCount, $thisCategory) = $result[$i];

	if($withArchive) {
		$sql = "SELECT COUNT(RecordID) FROM REPAIR_SYSTEM_RECORDS WHERE (left(DateInput,10) >='$startdate' AND
			left(DateInput,10) <='$enddate') AND CategoryID='$thisCategory' AND Title='$thisTitle' AND ArchivedBy IS NOT NULL AND RecordStatus!=".REPAIR_RECORD_STATUS_DELETED;
		$data = $lrepairsystem->returnVector($sql);
		
	}

	$archivedRecord = (($data[0]==0) ? "" : " (".$data[0].")");
	
	//$thisCount = "aaa\r\nbbb";
	$export_ary[] = array($i+1, $thisCatName, $thisTitle, $thisCount.$archivedRecord);
}


$export_text = $le->GET_EXPORT_TXT($export_ary,$export_header, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11");
//$export_text = $le->GET_EXPORT_TXT($export_ary,$export_header);
$filename = "request_statistics.csv";
$le->EXPORT_FILE($filename,$export_text); 
    
intranet_closedb();
?>
