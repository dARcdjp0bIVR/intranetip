<?
// Modifying by: 

###################
#
#   Date:   2020-08-25 (Cameron)
#           add location filter IsShow=1 when checking location [case #X190073]
#
#   Date:   2020-07-03 (Cameron)
#           Fix: should retrieve category that is approved(not deleted) in REPAIR_SYSTEM_CATEGORY
#
#	Date:	2017-05-31 (Cameron)
#			Fix: support special character in Request Summary like '"<>&
#			- correct word for Request Summary and Request Details
#			- apply Get_Safe_Sql_Query for text field 
#
#	Date:	2014-11-06	(Omas)
#	New Status 'O' -Pending, $status = 4 
#
####################

$PATH_WRT_ROOT="../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();

$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed");
	exit();
}

$file_format = array('User Login','Record Date','Category','Building','Location','Sub-location','Location Detail','Request Summary','Request Details','Remark','Status Code');
$flagAry = array(1,0,1,1,1,1,1,1,1,1,1,1);
$format_wrong = false;
$data = $limport->GET_IMPORT_TXT_WITH_REFERENCE($csvfile,"","",$file_format,$flagAry);
//$data = $limport->GET_IMPORT_TXT($csvfile);

if(is_array($data))
{
	$col_name = array_shift($data);
}

for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header");
	exit();
}
if(sizeof($data)==0)
{
	header("location: import.php?xmsg=import_no_record");
	exit();
}
$lrepairsystem = new librepairsystem();

$linterface = new interface_html();

# menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageList";

# Left menu
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['RepairSystem']['AllRequests']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();


### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";

$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['RepairSystem']['ImportDataCol'][0] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_RecordDate</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['Category']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['Building']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['Location']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_InventorySystem_Location."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['LocationDetail']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['RequestSummary']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['RequestDetails']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserRemark."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_general_status."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>";
$x .= "</tr>";

$sql = "drop table temp_repair_system_record_import";
$lrepairsystem->db_db_query($sql);

$sql = "CREATE TABLE IF NOT EXISTS temp_repair_system_record_import
	(
		UserID int(11),
		RecordDate datetime,
		CategoryID int(11),
		BuildingID int(11),
		LocationLevelID int(11),
		LocationID int(11),
		DetailsLocation text,
		Title varchar(255),
		Content text,
		Remark text,
		RecordStatus tinyint(1)		
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
$lrepairsystem->db_db_query($sql);

$fields = mysql_list_fields($intranet_db, 'temp_repair_system_record_import');
$columns = mysql_num_fields($fields);
for ($i = 0; $i < $columns; $i++) {$field_array[] = mysql_field_name($fields, $i);}

# delete the temp data in temp table 
//$sql = "DELETE FROM temp_repair_system_record_import WHERE UserID=".$_SESSION["UserID"];
$sql = "DELETE FROM temp_repair_system_record_import";
$lrepairsystem->db_db_query($sql) or die(mysql_error());

$error_occured = 0;

for($i=0;$i<sizeof($data);$i++)
{
	$error = array();
	$PicIDArr = array();
	### get data
	list($UserLogin, $RecordDate, $Category, $Building, $LocationLevel, $Location, $DetailsLocation, $Title, $Content, $Remark, $Status) = $data[$i];

	$DetailsLocation = intranet_htmlspecialchars($DetailsLocation);
	$DetailsLocation = intranet_htmlspecialchars($DetailsLocation);
	$Content = intranet_htmlspecialchars($Content);
	$Remark = intranet_htmlspecialchars($Remark);
			
	# User Login
	if(trim($UserLogin)=='')
	{
		$error['UserLogin'] = 'UserLogin cannot be empty';
//		$userid = "";
//		$UserName = "---";
	}
	else
	{
		$name_field = getNameFieldByLang();

		$sql = "SELECT UserID, $name_field as name FROM INTRANET_USER WHERE UserLogin='$UserLogin'";
		$result = $lrepairsystem->returnArray($sql);
		
		if(sizeof($result)==0) {
			$error['UserLogin'] = $Lang['RepairSystem']['UserLoginIncorrect'];					
			$UserName = $UserLogin;
		} else {
			$userid = $result[0][0];
			$UserName = $result[0][1];	
		}
	}

	#RecordDate
	if(trim($RecordDate)=='')
	{
		$error['RecordDate'] = $Lang['RepairSystem']['RecordDateMissing'];
	}
	else
	{
		//list($recorddate_year, $recorddate_month, $recorddate_day) = split('-',$RecordDate);
		if(strtotime($RecordDate)==-1)
		{
			$error['RecordDate'] = $Lang['RepairSystem']['RecordDateNotADate'];						
		}
		else 
		{
			$date_format = false;
			$date_format = $lrepairsystem->checkDateFormat($RecordDate);
			$RecordDate = str_replace('/', '-', $RecordDate);
			if(!$date_format) {	# wrong data format
				$error['RecordDate'] = $iDiscipline['Invalid_Date_Format'];			
			} else {
				
				if($RecordDate>date("Y-m-d")) {
					$error['RecordDate'] = $Lang['RepairSystem']['DateInTheFuture'];			
				}
				
			}
		}
		
	}
	#Category
	if($Category=="") {
		$error['Category'] = $Lang['RepairSystem']['CategoryMissing'];							
	} else {
		$catID = "";
		//$sql = "SELECT CategoryID FROM REPAIR_SYSTEM_CATEGORY WHERE RecordStatus!=-1 AND Name='".intranet_htmlspecialchars($Category)."'";	
		//$result = $lrepairsystem->returnVector($sql);
		
		$sql = "SELECT CategoryID, Name from REPAIR_SYSTEM_CATEGORY WHERE RecordStatus=1";
		$temp = $lrepairsystem->returnArray($sql,2);

		for($k=0; $k<sizeof($temp); $k++) {
			//echo $Category.' ### '.intranet_undo_htmlspecialchars($temp[$k][1]).'<br>';
			if($Category==intranet_undo_htmlspecialchars($temp[$k][1])) {
				$catID = $temp[$k][0];	
				break;
			}
		}
		
		if($catID=="") {
			$error['Category'] = $Lang['RepairSystem']['IncorrectCategory'];							
		} else {
			//$CategoryID = $result[0];	
			$CategoryID = $catID;
		}
	}

	# Location Level
	$BuildingID = "";
	if($Building=="") {
		//$error['Location'] = $Lang['RepairSystem']['LocationMissing'];							
	} else {
		$sql = "SELECT BuildingID from INVENTORY_LOCATION_BUILDING WHERE ".Get_Lang_Selection("NameChi", "NameEng")."='$Building' AND RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED;
		$temp = $lrepairsystem->returnVector($sql);
	
		if(sizeof($temp[0])==0) {
			$error['Building'] = $Lang['RepairSystem']['BuildingIncorrect'];							
		} else {
			$BuildingID = $temp[0];	
		}
	}	

	# Location Level
	$LocationLevelID = "";
	if($LocationLevel=="") {
		//$error['Location'] = $Lang['RepairSystem']['LocationMissing'];							
	} else {
		$sql = "SELECT LocationLevelID from INVENTORY_LOCATION_LEVEL WHERE ".Get_Lang_Selection("NameChi", "NameEng")."='$LocationLevel' AND BuildingID='$BuildingID' AND RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED;
		$temp = $lrepairsystem->returnVector($sql);
	
		if(sizeof($temp[0])==0) {
			$error['LocationLevel'] = $Lang['RepairSystem']['LocationIncorrect'];							
		} else {
			$LocationLevelID = $temp[0];	
		}
	}	
	
	
	# Sub-Location
	$LocationID = "";
	if($Location=="") {
		//$error['Location'] = $Lang['RepairSystem']['LocationMissing'];							
	} else {
		$sql = "SELECT LocationID from INVENTORY_LOCATION WHERE ".Get_Lang_Selection("NameChi", "NameEng")."='$Location' AND LocationLevelID='$LocationLevelID' AND IsShow=1 AND RecordStatus=".REPAIR_SYSTEM_STATUS_APPROVED;
		$temp = $lrepairsystem->returnVector($sql);
				
		if(sizeof($temp[0])==0) {
			$error['Location'] = $Lang['RepairSystem']['StubLocIncorrect'];							
		} else {
			$LocationID = $temp[0];	
		}
	}	
	
	
	#Details Location
	if(trim($DetailsLocation)=='')
	{
		$DetailsLocation = "---";	
	}
	
	#Title
	if(trim($Title)=='')
	{
		$error['Title'] = $Lang['RepairSystem']['TitleMissing'];	
	}
	else
	{
		# need to match with Category
		$title_ck = intranet_htmlspecialchars($Title);
		
		$sql = "select 
					Title
				from
					REPAIR_SYSTEM_REQUEST_SUMMARY 
				where
					RecordStatus = 1 and
					CategoryID = '$CategoryID' and
					Title = '".$lrepairsystem->Get_Safe_Sql_Query($title_ck)."'
				";
		$temp = $lrepairsystem->returnArray($sql);

		if(sizeof($temp)==0)
			$error['Title'] = $Lang['RepairSystem']['MismatchRequestSummary'];
	}
	
	#Content
	if(trim($Content)=='')
	{
		$error['Content'] = $Lang['RepairSystem']['ContentMissing'];	
	}
	
	/*
	#Remark
	if(trim($Remark)=='')
	{
		$error['Remark'] = "Missing Remark";	
	}	
	*/
	
	#Status
	$s = array("P"=>$i_Discipline_System_Discipline_Case_Record_Processing, "C"=>$Lang['StaffAttendance']['SettingCompletedStatus'], "R"=>$i_status_rejected ,"O"=>$Lang['RepairSystem']['Pending']);
	$rs = array("P"=>1, "C"=>2, "R"=>3, "O"=>4);
	
	if($Status=="") {
		//$error['Status'] = $Lang['RepairSystem']['StatusCodeMissing'];
		$Status = "O";
		$RecordStatus = $s[$Status];
	} else {
		if(!in_array($Status, array_keys($s))) {
			$error['Incorrect Status'] = $Lang['RepairSystem']['StatusCodeIncorrect'];
		} else {
			$RecordStatus = $s[$Status];
		}
	}

		
	$css = (sizeof($error)==0) ? "tabletext":"red";

	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">".$UserName."</td>";
	$x .= "<td class=\"$css\">".$RecordDate."</td>";
	$x .= "<td class=\"$css\">".$Category."</td>";
	$x .= "<td class=\"$css\">".$Building."</td>";
	$x .= "<td class=\"$css\">".$LocationLevel."</td>";
	$x .= "<td class=\"$css\">".$Location."</td>";
	$x .= "<td class=\"$css\">".$DetailsLocation."</td>";
	$x .= "<td class=\"$css\">".$Title."</td>";
	$x .= "<td class=\"$css\">".$Content."</td>";
	$x .= "<td class=\"$css\">".$Remark."</td>";
	$x .= "<td class=\"$css\">".$RecordStatus."</td>";
	$x .= "<td class=\"$css\">";
	
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .=$Value.'<br/>';
		}	
	}
	/*
	else
	{	
		$x .= "--";
	}
	*/
	$x.="</td>";
	$x .= "</tr>";

	$sql = "INSERT INTO temp_repair_system_record_import VALUE ('$userid','$RecordDate','$CategoryID', '$BuildingID', '$LocationLevelID', '$LocationID', '".$lrepairsystem->Get_Safe_Sql_Query($DetailsLocation)."','".$lrepairsystem->Get_Safe_Sql_Query($Title)."','".$lrepairsystem->Get_Safe_Sql_Query($Content)."','".$lrepairsystem->Get_Safe_Sql_Query($Remark)."','".$rs[$Status]."')";

	$lrepairsystem->db_db_query($sql);

	if($error)
	{
		$error_occured++;
	}	
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php'");	
}

?>

<br />
<form name="form1" method="post" action="import_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>