<?php
// Modifying by: 
###################
#
#	Date:	2017-05-31 (Cameron)
#		- correct word for Request Summary and Request Details
#
#	Date:	2014-11-06	(Omas)
#	add new column CaseNumbeer - insert case number by calling retrieveNewCaseNumber()
#
####################
$PATH_WRT_ROOT="../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$lrepairsystem = new librepairsystem();

# Check access right
//$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");

$sql = "SELECT * FROM temp_repair_system_record_import";
$recordArr = $lrepairsystem->returnArray($sql);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['RepairSystem']['ImportDataCol'][0] ."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_RecordDate</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['Category']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['Building']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['Location']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_InventorySystem_Location."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['LocationDetail']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['RequestSummary']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['RepairSystem']['RequestDetails']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_UserRemark."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_general_status."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>";
$x .= "</tr>";

for($a=0;$a<sizeof($recordArr);$a++)
{
	$name_field = getNameFieldByLang();
	$sql = "SELECT $name_field as name FROM INTRANET_USER WHERE UserID='".$recordArr[$a]['UserID']."'";
	$result = $lrepairsystem->returnVector($sql);
	$userName = ($result[0]!="") ? $result[0] : "---";
	$catInfo = $lrepairsystem->getCategoryInfo($recordArr[$a]['CategoryID']);
	$catName = $catInfo[0]['Name'];
	$buildingInfo = $lrepairsystem->getBuildingNameByID($recordArr[$a]['BuildingID']);
	$buildingName = $buildingInfo['BuildingName'];
	$locationLevelInfo = $lrepairsystem->getLocationLevelNameByID($recordArr[$a]['LocationLevelID']);
	$locationLevelName = $locationLevelInfo['LocationLevelName'];
	$locationInfo = $lrepairsystem->getLocationNameByID($recordArr[$a]['LocationID']);
	$locationName = $locationInfo['LocationName'];
	
	
	/*
	$LocInfo = $lrepairsystem->getLocationInfo($recordArr[$a]['LocationID']);
	$locName = $LocInfo[0]['LocationName'];
	*/
	$s = array("1"=>$i_Discipline_System_Discipline_Case_Record_Processing, "2"=>$Lang['StaffAttendance']['SettingCompletedStatus'], "3"=>$i_status_rejected, "0"=>$i_status_cancel, "4"=>$Lang['RepairSystem']['Pending']);

	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">". $userName."</td>";
	$x .= "<td class=\"tabletext\">". substr($recordArr[$a]['RecordDate'],0,10) ."</td>";
	$x .= "<td class=\"tabletext\">". $catName ."</td>";
	$x .= "<td class=\"tabletext\">". $buildingName ."</td>";
	$x .= "<td class=\"tabletext\">". $locationLevelName ."</td>";
	$x .= "<td class=\"tabletext\">". $locationName ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['DetailsLocation'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Title'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Content'] ."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Remark'] ."</td>";
	$x .= "<td class=\"tabletext\">". $s[$recordArr[$a]['RecordStatus']] ."</td>";
	$x .= "</tr>";

	$dataAry = array();
		
	$dataAry['CategoryID'] = $recordArr[$a]['CategoryID'];
	$dataAry['LocationBuildingID'] = $recordArr[$a]['BuildingID'];
	$dataAry['LocationLevelID'] = ($recordArr[$a]['LocationLevelID']==0) ? "" : $recordArr[$a]['LocationLevelID'];
	$dataAry['LocationID'] = ($recordArr[$a]['LocationID']==0) ? "" : $recordArr[$a]['LocationID'];
	$dataAry['DetailsLocation'] = ($recordArr[$a]['DetailsLocation']=="---") ? "" : $recordArr[$a]['DetailsLocation'];
	$dataAry['UserID'] = $recordArr[$a]['UserID'];
// 	$dataAry['Title'] = addslashes($recordArr[$a]['Title']);
$dataAry['Title'] =  intranet_htmlspecialchars($recordArr[$a]['Title']);
	$dataAry['Content'] = $recordArr[$a]['Content'];
	//$dataAry['RecordStatus'] = $recordArr[$a]['RecordStatus'];
	$dataAry['Remark'] = $recordArr[$a]['Remark'];
	/*
	$dataAry['DateInput'] = "NOW()";
	$dataAry['DateModified'] = "NOW()";
	$dataAry['LastModifiedBy'] = $UserID;
	*/
	
	
	$dataAry['CaseNumber'] = $lrepairsystem->retrieveNewCaseNumber($recordArr[$a]['CategoryID']);
	$id = $lrepairsystem->insertRepairRecord($dataAry);
	$dataAry2['DateInput'] = $recordArr[$a]['RecordDate'];
	$dataAry2['RecordStatus'] = $recordArr[$a]['RecordStatus'];
	$lrepairsystem->updateRepairRecord($id, $dataAry2);
	
	if($id)
	{
		# do the grouping
		$xmsg2= "<font color=green>".sizeof($recordArr)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
	}
	else
	{
		$xmsg="import_failed";		
	}
}
$x .= "</table>";

/*
$sql = "DELETE FROM temp_repair_system_record_import";
$lrepairsystem->db_db_query($sql);
*/
$linterface = new interface_html();

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php'");

# menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageList";

# Left menu
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['RepairSystem']['List']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
</form>


<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>