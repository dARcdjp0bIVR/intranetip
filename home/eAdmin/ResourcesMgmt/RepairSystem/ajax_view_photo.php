<?php
/*
 * 	modifying:
 * 
 * 	2017-06-19 Cameron 
 * 		- add thickboxContainerDiv & thickboxContentDiv so that it can open in dynamic size thickbox 
 * 
 * 	2017-05-10 Cameron
 * 		- create this file
 * 
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

$CurrentPageArr['eAdminRepairSystem'] = 1;

$rs = $lrepairsystem->getPhotosByPhotoID($PhotoID);

$characterset = ($junior_mck) ? 'big5' : 'utf-8';
header('Content-Type: text/html; charset='.$characterset);

$x = '';
$x .= '<div id="thickboxContainerDiv" class="edit_pop_board">
		<form id="thickboxForm" name="thickboxForm">
			<div id="thickboxContentDiv" class="edit_pop_board_write">';

if (count($rs)) {
	$rs = current($rs);
	$x = '<img src="/file/repair/final/'.$rs['FileHashName'].'" border="0"><br><div class="name">'.$rs['FileName'].'</div>';
}

$x .= '</div></form></div>';

$x .= "<br>";
$x .= '<div id="editBottomDiv" class="edit_bottom_v30">';
$x .= $linterface->GET_ACTION_BTN($Lang['General']['Close'], "button", "js_Hide_ThickBox();", "closeBtn_tb");
$x .= '</div>';
		
echo $x;

intranet_closedb();
?>
