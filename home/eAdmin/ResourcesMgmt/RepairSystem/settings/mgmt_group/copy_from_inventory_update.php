<?php 
/*
 * 	2017-05-25 Cameron
 * 		- create this file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

if(sizeof($GroupID)==0) {
	header("Location: index.php");	
}

$dataAry = array();
$result = array();

$lrepairsystem->Start_Trans();

$sql = "SELECT AdminGroupID, NameChi, NameEng FROM INVENTORY_ADMIN_GROUP WHERE AdminGroupID IN ('".implode("','",$GroupID)."')";
$groupRS = $lrepairsystem->returnResultSet($sql);
if (count($groupRS)) {
	foreach((array)$groupRS as $rs) {		
		$groupID = $rs['AdminGroupID'];
		$nameChi = $rs['NameChi'];
		$nameEng = $rs['NameEng'];
		$groupTitle = Get_Lang_Selection($nameChi,$nameEng);
		$groupDescription = $nameChi == $groupTitle ? $nameEng : $nameChi;		// description use the other language name
		
		$check_sql = "SELECT GroupID FROM REPAIR_SYSTEM_GROUP WHERE (GroupTitle='".$lrepairsystem->Get_Safe_Sql_Query($nameChi).
					 "' OR GroupTitle='".$lrepairsystem->Get_Safe_Sql_Query($nameEng)."') AND RecordStatus<>-1 LIMIT 1";
		$checkRS = $lrepairsystem->returnResultSet($check_sql);
		if (!count($checkRS)) {		// name not exist, allow to copy
			unset($dataAry);
			$dataAry['GroupTitle'] = $lrepairsystem->Get_Safe_Sql_Query($groupTitle);
			$dataAry['GroupDescription'] = $lrepairsystem->Get_Safe_Sql_Query($groupDescription);
			$insert_sql = $lrepairsystem->insertMgmtGroup($dataAry, false);
			$insertResult = $lrepairsystem->db_db_query($insert_sql);
			
			if ($insertResult) {
				$repairGroupID = $lrepairsystem->db_insert_id();
				$result[] = true;
				$sql = "SELECT UserID FROM INVENTORY_ADMIN_GROUP_MEMBER WHERE AdminGroupID='".$groupID."' ORDER BY UserID";
				$memberRS = $lrepairsystem->returnResultSet($sql);
				if (count($memberRS)) {
					foreach((array)$memberRS as $member) {
						$sql = "INSERT INTO REPAIR_SYSTEM_GROUP_MEMBER SET GroupID='".$repairGroupID."', UserID='".$member['UserID']."', DateInput=NOW()";
						$result[] = $lrepairsystem->db_db_query($sql);
					}
				}
			}
			else {
				$result[] = false;
			}
		} 
	}
}

if (!in_array(false,$result)) {
	$lrepairsystem->Commit_Trans();
	$xmsg = "copy";
}
else {
	$lrepairsystem->RollBack_Trans();
	$xmsg = "copy_failed";
}
intranet_closedb();

header("Location: index.php?xmsg=".$xmsg);

?>