<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

	
if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$sql = "SELECT 
				g.NameChi, g.NameEng,
				CONCAT('<a href=\"#TB_inline?height=500&width=800&inlineId=FakeLayer\" onclick=\"view_inventory_group_member(', g.AdminGroupID,')\">', COUNT(gm.UserID),'</a>'), 
				CONCAT('<input type=\"checkbox\" name=\"GroupID[]\" id=\"GroupID[]\" value=\"', g.AdminGroupID,'\">')
		FROM 
				INVENTORY_ADMIN_GROUP g 
		LEFT OUTER JOIN
				INVENTORY_ADMIN_GROUP_MEMBER gm ON gm.AdminGroupID=g.AdminGroupID
		LEFT OUTER JOIN
				REPAIR_SYSTEM_GROUP rg ON ((rg.GroupTitle=g.NameChi OR rg.GroupTitle=g.NameEng) AND rg.RecordStatus<>-1)
		WHERE 
				rg.GroupTitle IS NULL 
		GROUP BY 
				g.AdminGroupID";
       
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("NameChi", "NameEng", "COUNT(gm.UserID)");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = "2";

$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='40%' >".$li->column($pos++, $i_InventorySystem_Setting_ManagementGroup_ChineseName)."</td>\n";
$li->column_list .= "<td width='40%'>".$li->column($pos++, $i_InventorySystem_Setting_ManagementGroup_EnglishName)."</td>\n";
$li->column_list .= "<td width='18%'>".$li->column($pos++, $Lang['RepairSystem']['Member'])."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("GroupID[]")."</td>\n";


# Top menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageMgmtGroupSettings";

$TAGS_OBJ[] = array($Lang['RepairSystem']['MgmtGroup'], "", 0);

# Left menu 
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['GroupList'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['CopyFromInventory'], "");
$msg = '';

# Start layout
$linterface->LAYOUT_START();

echo $linterface->Include_Thickbox_JS_CSS();
?>
<script language="javascript">

function checkCopy(obj,element,page){
    if(countChecked(obj,element)==0)
    	alert(globalAlertMsg2);
    else{
        obj.action=page;
        obj.method="post";
        obj.submit();
    }
}

function view_inventory_group_member(groupID) {
	tb_show('<?=$Lang['RepairSystem']['CopyFromInventory']?>','view_inventory_group_member.php?&GroupID='+groupID+'&height=500&width=800');	
}
	
</script>
<br />
<form name="form1" method="post" action="">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%" class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg, $msg) ?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkCopy(document.form1,'GroupID[]','copy_from_inventory_update.php')" class="tabletool" title="<?= $Lang['Btn']['Copy'] ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_copy.gif" width="12" height="12" border="0" align="absmiddle"> <?=$Lang['Btn']['Copy'] ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
			</td>
		</tr>
	</table>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>