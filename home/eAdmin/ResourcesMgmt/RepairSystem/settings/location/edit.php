<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;

}

$linterface = new interface_html();

$LocID = (is_array($LocID)) ? $LocID[0] : $LocID;

$LocInfo = $lrepairsystem->getLocationInfo($LocID);
$locationName = $LocInfo[0]['LocationName'];


# Top menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageLocationSettings";

$TAGS_OBJ[] = array($Lang['RepairSystem']['MgmtGroup']);

$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['LocationList'], "index.php");
$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['LocationEdit'], "");

# Left menu 
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function checkForm(form1) {
	if(form1.LocationName.value=='')	 {
		alert("<?= $i_alert_pleasefillin.' '.$Lang['RepairSystem']['LocationName'] ?>");	
		form1.LocationName.focus();
		return false;
	} else {
		return true;
	}
}
//-->
</script>
<br />
<form name="form1" method="post" action="edit_update.php" onSubmit="return checkForm(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
										</tr>
									</table>
					</td>
				</tr>
				<tr>
					<td>
						<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['RepairSystem']['LocationName']?><span class="tabletextrequire">*</span></td>
								<td><input name="LocationName" type="text" class="textboxtext" value="<?=$locationName?>"></td>
							</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						</table>
						<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field ?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="10"></td>
				</tr>
				<tr>
					<td height="1"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="1" height="10"></td>
				</tr>
				<tr>
					<td align="center">
					<table width="88%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center">
								<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				</table><br>
				<input type="hidden" name="LocID" id="LocID" value="<?=$LocID?>" />
			</td>
		</tr>
	</table>
</form>
<?php
echo $linterface->FOCUS_ON_LOAD("form1.LocationName"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>