<?php
// using :  
/*
 *  2020-08-22 Cameron
 *      - copy from eBooking and modify
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$lrepairsystem = new librepairsystem();

$Action = trim($Action);
if($_POST['SubLocationID'] != "")
{
	$SubLocationID = $_POST['SubLocationID'];
}

if($Action == "ShowEdit_AllIsShowRoom")
{
    $layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>
						<tbody>
						<tr class='row_approved'>
							<td nowrap style='border-bottom:none' class='tabletext' width='5%'><input type='radio' name='All_IsShowRoom' id='All_IsShowRoom_Yes' value='1'></td>
							<td nowrap style='border-bottom:none' class='tabletext' align='left'><label for='All_IsShowRoom_Yes'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_tick_green.gif' border='0'>".$Lang['General']['Show']."</lable></td>
						</tr>
						<tr class='row_approved'>
							<td nowrap style='border-bottom:none' class='tabletext' width='5%'><input type='radio' name='All_IsShowRoom' id='All_IsShowRoom_No' value='0'></td>
							<td nowrap style='border-bottom:none' class='tabletext' align='left'><label for='All_IsShowRoom_No'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_norecord_bw.gif' border='0'>".$Lang['General']['Hide']."</lable></td>
						</tr>
						<tr height='10px'>
							<td></td>
						</tr>
						<tr>
							<td align='center' colspan='2'>".$linterface->GET_SMALL_BTN($Lang['Btn']['ApplyToAll'],"Button","javascript:ApplyToAll('All_IsShowRoom')").
        "&nbsp;".$linterface->GET_SMALL_BTN($Lang['RepairSystem']['Settings']['ApplyToSelected'],"Button","javascript:ApplyToSelected('SelectedIsShowRoom')").
        "&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>
						</tr>
						</tbody>
					</table>";

}

else if($Action == "ShowEdit_IsShowRoom")
{
	$sql = "SELECT IsShow FROM INVENTORY_LOCATION WHERE LocationID = '$SubLocationID'";
	$arrIsShow = $lrepairsystem->returnVector($sql);
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>"; 
	if($arrIsShow[0] == 1){
		$show_checked = " CHECKED ";
		$hide_checked = " ";
	}else{
		$show_checked = " ";
		$hide_checked = " CHECKED ";
	}
	$layer_content .= "<tr class='row_approved'>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext' width='10%' align='left' ><input type='radio' name='IsShowRoom_".$SubLocationID."' id='IsShowRoom_".$SubLocationID."_Yes' value='1' $show_checked></td>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext'><label for='IsShowRoom_".$SubLocationID."_Yes'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_tick_green.gif' border='0'>".$Lang['General']['Show']."</lable></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr class='row_approved'>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext' width='10%' align='left' ><input type='radio' name='IsShowRoom_".$SubLocationID."' id='All_IsShowRoom_".$SubLocationID."_No' value='0' $hide_checked></td>";
	$layer_content .= "<td nowrap style='border-bottom:none' class='tabletext'><label for='IsShowRoom_".$SubLocationID."_No'><img src='".$PATH_WRT_ROOT.$image_path."/".$LAYOUT_SKIN."/icon_norecord_bw.gif' border='0'>".$Lang['General']['Hide']."</lable></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr height='10px'>";
	$layer_content .= "<td nowrap style='border-bottom:none'></td>";
	$layer_content .= "</tr>";
	$layer_content .= "<tr>";
	$layer_content .= "<td align='center' colspan='2' nowrap style='border-bottom:none'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Apply'],"Button","javascript:ApplyToSelectedLocation('IsShowRoom',$SubLocationID)")."&nbsp;".$linterface->GET_SMALL_BTN($Lang['Btn']['Close'],"Button","js_Hide_Detail_Layer()")."</td>";
	$layer_content .= "</tr>";
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
else if($Action == 'ShowLocationDescription')
{
	$sql = "SELECT Description FROM INVENTORY_LOCATION WHERE LocationID = '$SubLocationID'";
	$arrResult = $lrepairsystem->returnArray($sql);
	
	$layer_content = "<table id='Table_".$SubLocationID."' border='0' cellpadding='3' cellspacing='0' width='100%'>";
	$layer_content .= "<tbody>";
	$layer_content .= "<tr><td align='right'><a href='javascript:js_Hide_Detail_Layer()'><img border='0' src='".$image_path."/".$LAYOUT_SKIN."/ecomm/btn_mini_off.gif'></a></td></tr>";
	for($i=0; $i<sizeof($arrResult); $i++)
	{
		list($description) = $arrResult[$i];
		$layer_content .= "<tr class='row_approved'><td valign='top' nowrap style='border-bottom:none' colspan='3'>$description</td></tr>";
	}
	$layer_content .= "</tbody>";
	$layer_content .= "</table>";
}
echo $layer_content;
?>