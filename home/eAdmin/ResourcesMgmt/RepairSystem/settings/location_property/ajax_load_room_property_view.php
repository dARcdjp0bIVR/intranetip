<?php
// using : 
/*
 *  2020-08-21 Cameron
 *      - copy from eBooking and modify
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

// Get data from POST, not COOKIES
$targetBuilding = !isset($_POST['targetBuilding'])?$_COOKIE['targetBuilding']:$_POST['targetBuilding'];
$targetFloor = !isset($_POST['targetFloor'])?$_COOKIE['targetFloor']:$_POST['targetFloor'];
$targetRoom = !isset($_POST['targetRoom'])?$_COOKIE['targetRoom']:$_POST['targetRoom'];
$pageNo = $_POST['pageNo'];
$numPerPage = $_POST['numPerPage'];

echo $lrepairsystem->getRoomPropertyViewTable($targetBuilding,$targetFloor,$targetRoom,$pageNo,$numPerPage);

intranet_closedb();
?>