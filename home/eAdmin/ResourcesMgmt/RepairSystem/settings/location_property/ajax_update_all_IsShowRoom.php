<?php
/*
 *  2020-08-24 Cameron
 *      - copy from eBooking and modify
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

$lrepairsystem = new librepairsystem();

intranet_auth();
intranet_opendb();

$targetBuilding = (isset($_POST['targetBuilding'])) ? $_POST['targetBuilding'] : $targetBuilding;
$targetFloor 	= (isset($_POST['targetFloor'])) ? $_POST['targetFloor'] : $targetFloor;
$targetRoomID		= (isset($_POST['targetRoomID'])) ? $_POST['targetRoomID'] : $targetRoomID;
$displayedSubLocationIdList	= (isset($_POST['displayedSubLocationIdList'])) ? $_POST['displayedSubLocationIdList'] : $displayedSubLocationIdList;
$targetBuilding = IntegerSafe($targetBuilding);
$targetFloor = IntegerSafe($targetFloor);
$targetRoomID = IntegerSafe($targetRoomID);
$displayedSubLocationIdList = IntegerSafe($displayedSubLocationIdList);
$IsShow = IntegerSafe($_POST['IsShow']);

$lrepairsystem->Start_Trans();

if($targetRoomID == "")
{
	$displayedSubLocationIdAry = explode(',', $displayedSubLocationIdList);
	$sql = "SELECT 
				Room.LocationID 
			FROM 
				INVENTORY_LOCATION_BUILDING AS Building 
				INNER JOIN INVENTORY_LOCATION_LEVEL AS Floor ON (Building.BuildingID = Floor.BuildingID)
				INNER JOIN INVENTORY_LOCATION AS Room ON (Floor.LocationLevelID = Room.LocationLevelID)
			WHERE
				Building.RecordStatus = 1 AND Floor.RecordStatus = 1 AND Room.RecordStatus = 1 
				AND	Building.BuildingID = '$targetBuilding' AND Floor.LocationLevelID = '$targetFloor' AND Room.LocationID IN ('".implode("','", (array)$displayedSubLocationIdAry)."')";
	
	$arrLocation = $lrepairsystem->returnVector($sql);
	if(sizeof($arrLocation)>0)
	{
		$targetRoomID = implode(",",$arrLocation);
	}
}
if ($targetRoomID) {
    $sql = "UPDATE INVENTORY_LOCATION SET IsShow = '$IsShow' WHERE LocationID IN ($targetRoomID) AND RecordStatus = 1";
    $result["UpdateIsShowRoom"] = $lrepairsystem->db_db_query($sql);
}
if (in_array(false,$result)) {
	$lrepairsystem->RollBack_Trans();
	echo 0;
} else {
	$lrepairsystem->Commit_Trans();
	echo 1;
}

intranet_closedb();
?>