<?php
// using: 
/*
 *  2020-08-21 Cameron
 *      - copy from eBooking and modify
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

$ModuleName = $_POST['ModuleName'];
$targetBuilding = !isset($_POST['targetBuilding'])?$_COOKIE['targetBuilding']:$_POST['targetBuilding'];
$targetFloor = !isset($_POST['targetFloor'])?$_COOKIE['targetFloor']:$_POST['targetFloor'];
$targetRoom = !isset($_POST['targetRoom'])?$_COOKIE['targetRoom']:$_POST['targetRoom'];

if($ModuleName == "Floor")
{
	$arrFloor = $lrepairsystem->Get_All_Floor_Array($targetBuilding);

	if($targetBuildiing == "")
	{
		$targetFloor = "";
	}
	
	if($targetFloor == "")
	{
		$targetFloor = $_POST['targetFloor'];
	}
	
	if($disabled)
		$floor_selection = getSelectByArray($arrFloor, " name='targetFloor' id='targetFloor' onChange='javascript: $(\"#pageNo_RepairSystem_Settings_Room_View\").val(\"1\"); Reload_Location_Selection(\"Room\",this.value);' DISABLED",$targetFloor,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Floor']." -- ");
	else
		$floor_selection = getSelectByArray($arrFloor, " name='targetFloor' id='targetFloor' onChange='javascript: $(\"#pageNo_RepairSystem_Settings_Room_View\").val(\"1\"); Reload_Location_Selection(\"Room\",this.value);'",$targetFloor,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Floor']." -- ");
	
	echo $floor_selection;
}
else if($ModuleName == "Room")
{
	$arrRoom = $lrepairsystem->Get_All_Room_Array($targetBuilding,$targetFloor);

	if($disabled)
		$room_selection = getSelectByArray($arrRoom, " name='targetRoom' id='targetRoom' onChange='javascript: reloadMainContent()' DISABLED",$targetRoom,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Room']." -- ");
	else
		$room_selection = getSelectByArray($arrRoom, " name='targetRoom' id='targetRoom' onChange='javascript: reloadMainContent()'",$targetRoom,0,0," -- ".$Lang['SysMgr']['Location']['Select']['Room']." -- ");
	
	echo $room_selection;
}

intranet_closedb();
?>