<?php
# using: 

##### Change Log [Start] #####
#
#	Date:	2017-05-31 [Cameron]
#			Fix: disable button before submit to avoid adding duplicate record
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();


if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

$category_selection = $lrepairsystem->getCategorySelection($CatID,"","","-- ". $Lang['Btn']['Select'] ." --");

# menu highlight setting
$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageRequestSummary";
$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['RepairSystem']['RequestSummary']);
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

$PAGE_NAVIGATION[] = array($Lang['RepairSystem']['NewRequestSummary']);
?>
<script language="javascript">
<!--
function goBack() {
	window.location='index.php';
}

function check_form() 
{
	var obj = document.form1;
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	if(!check_select_30(obj.CatID, "<?php echo $i_alert_pleaseselect.$Lang['RepairSystem']['Category']; ?>.","","div_CatID_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "CatID";  
	}
	
	if(!check_text_30(obj.RequestSummaryTitle, "<?php echo $i_alert_pleasefillin.$Lang['RepairSystem']['RequestSummary']; ?>.", "div_RequestSummaryTitle_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "RequestSummaryTitle";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_RequestSummaryTitle_err_msg').innerHTML = "";
 	document.getElementById('div_CatID_err_msg').innerHTML = "";
}

function reset_form()
{
	reset_innerHtml();
	document.form1.reset();
}

$(document).ready(function(){

	$('#btnSubmit').click(function(e) {
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

		if (check_form()) {
			$('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

});

-->
</script>
 
<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="form1" id="form1" method="POST" action="new_update.php">

<table class="form_table_v30">
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['RepairSystem']['Category']?></td>
	<td><?=$category_selection?><br><span id='div_CatID_err_msg'></span></td>
</tr>

<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['RepairSystem']['RequestSummary']?></td>
	<td><input name="RequestSummaryTitle" type="text" class="textboxtext" value=""><br><span id='div_RequestSummaryTitle_err_msg'></span></td>
</tr>

<tr>
	<td class="field_title"><?=$Lang['General']['Status']?></td>
	<td>
		<input type="radio" name="in_use" value="1" id="in_use1" checked> <label for="in_use1"><?=$Lang['SysMgr']['FormClassMapping']['InUse']?></label>
		<input type="radio" name="in_use" value="0" id="in_use0"> <label for="in_use0"><?=$Lang['SysMgr']['FormClassMapping']['NotInUse']?></label>
	</td>
</tr>

</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($button_submit, "button", "", "btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_reset, "button", "reset_form()", "btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()", "btnBack", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
<p class="spacer"></p>
</div>

</form>

<?
echo $linterface->FOCUS_ON_LOAD("form1.CatID"); 
$linterface->LAYOUT_STOP();
intranet_closedb();

?>