<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

$sql = "select CategoryID, Sequence from REPAIR_SYSTEM_REQUEST_SUMMARY where RecordID=$RecordID";
$result = $lrepairsystem->returnArray($sql);
list($thisCatID, $OriSequence) = $result[0];

if($move=="top")
{
	$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=Sequence+1 where CategoryID=$thisCatID and Sequence<$OriSequence";
	$lrepairsystem->db_db_query($sql);
	
	# set current record seq to 1
	$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=1 where RecordID=$RecordID";
	$lrepairsystem->db_db_query($sql);
}
else if($move=="bottom")
{
	$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=Sequence-1 where CategoryID=$thisCatID and Sequence>$OriSequence";
	$lrepairsystem->db_db_query($sql);
	
	# get max(Sequence)
	$sql2 = "select max(Sequence) from REPAIR_SYSTEM_REQUEST_SUMMARY where CategoryID=$thisCatID and RecordID<>$RecordID";
	$result2 = $lrepairsystem->returnVector($sql2);
	$Sequence = $result2[0]+1;
	
	# set current record seq to max(Sequence)
	$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=$Sequence where RecordID=$RecordID";
	$lrepairsystem->db_db_query($sql);
}
else if($move=="up")
{
	# check seq-1 exists or not
	$sql = "select RecordID from REPAIR_SYSTEM_REQUEST_SUMMARY where CategoryID=$thisCatID and Sequence=". ($OriSequence-1);
	$result2 = $lrepairsystem->returnVector($sql);
	
	if(empty($result2))
	{
		# set current record seq-1 directly
		$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=". ($OriSequence-1) ." where RecordID=$RecordID";
		$lrepairsystem->db_db_query($sql);
	}
	else
	{
		# update seq-1 record to seq
		$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=". $OriSequence ." where RecordID=".$result2[0];
		$lrepairsystem->db_db_query($sql);
		
		# set current record seq-1 
		$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=". ($OriSequence-1) ." where RecordID=$RecordID";
		$lrepairsystem->db_db_query($sql);
	}
}
else if($move=="down")
{
	# check seq+1 exists or not
	$sql = "select RecordID from REPAIR_SYSTEM_REQUEST_SUMMARY where CategoryID=$thisCatID and Sequence=". ($OriSequence+1);
	$result2 = $lrepairsystem->returnVector($sql);
	
	if(empty($result2))
	{
		# set current record seq+1 directly
		$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=". ($OriSequence+1) ." where RecordID=$RecordID";
		$lrepairsystem->db_db_query($sql);
	}
	else
	{
		# update seq+1 record to seq
		$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=". $OriSequence ." where RecordID=".$result2[0];
		$lrepairsystem->db_db_query($sql);
		
		# set current record seq+1 
		$sql = "update REPAIR_SYSTEM_REQUEST_SUMMARY set Sequence=". ($OriSequence+1) ." where RecordID=$RecordID";
		$lrepairsystem->db_db_query($sql);
	}
}


$x = $lrepairsystem->displayRequestSummary_ui($CatID, $in_use, $keyword);

echo $x;
intranet_closedb();
?>

