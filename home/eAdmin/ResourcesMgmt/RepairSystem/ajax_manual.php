<?php
# modifying :
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");

$RecordID = IntegerSafe(standardizeFormPostValue($_POST['RecordID']));

intranet_auth();
intranet_opendb(); 
$lrepairsystem = new librepairsystem();
$linterface = new interface_html();

$htmlAry['textfield'] = '<input id="keyword" name="keyword" type="text" maxlength="32">';
$htmlAry['submitButton'] = $linterface->GET_SMALL_BTN($button_submit, "button", "goGenerateResult()");
$htmlAry['closeBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Close'], "button", "goBackEditpage()");

$x = '';
$x .= '<table><tr>';
$x .= '<td class="Conntent_search" style="float:left">';
$x .= $htmlAry['textfield'];
$x .= '</td>';
$x .= '<td>';
$x .= $htmlAry['submitButton'];
$x .= '</td></tr></table>
		<p class="spacer"></p>
		<div id="ResultDiv" class="edit_pop_board_write" style="height:73%"></div>
		<div id="editBottomDiv" class="edit_bottom_v30">';
$x .= $htmlAry['closeBtn'];
$x .= '<p class="spacer"></p>';
$x .= '</div>';

$htmlAry['resultTable'] = $x;


?>

<script language="javascript">

function goBackEditpage() {

	js_Hide_ThickBox();
}

function goGenerateResult() {

	$('div#ResultDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_advance_search_result_table.php", 
			{ 
				keyword: $('input#keyword').val()
			}
		);
}
</script>

<?php echo $htmlAry['resultTable'] = $x;?>



