<?php
//Modifying by: 

############ Change Log Start ###############
#
#   Date    :   2020-10-28 (Cameron)
#               modify fitler condition to allow follow-up person (system) to view self record
#
#	Date	:	2017-10-10  (Cameron)
#				Fix: use left join INTRANET_USER to include deleted user (case #N128259)
#
#	Date	:	2017-05-31 (Cameron)
#				Fix: support special character search like '"<>&
#
#	Date	:	2014-11-06	(Omas)
#				Improved : Add new column Case Number, new status:Pending, able to search by CaseNumber
#
#	Date	:	2011-03-28 (Henry Chow)
#				Improved : select "Location" from "School Setting > Campus"
#
#	Date	:	2011-01-03	YatWoon
#				add rejected status
#
#	Date	:	2010-12-23	YAtWoon
#				Fixed: export result missing cater request date and complete date
#
#	Date	:	2010-12-15	YatWoon
#				only display in-charged category for mgmt group user (if non-admin) 
#
#	Date	:	2010-11-24	YatWoon
#				- IP25 UI standard
#
############ Change Log End ###############

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/librepairsystem.php");

intranet_auth();
intranet_opendb();

$lrepairsystem = new librepairsystem();

if (!isset($plugin['RepairSystem']) || !$plugin['RepairSystem'] || (!$_SESSION['SSV_USER_ACCESS']['eAdmin-RepairSystem'] && $lrepairsystem->userInMgmtGroup($UserID)==0))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if($ck_right_page_order=="" && $order=="") {
	$order = 1;
} else if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	//$order = $ck_right_page_order;
	$order = 1;		# default in ascending order
} 

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

$CurrentPageArr['eAdminRepairSystem'] = 1;
$CurrentPage = "PageList";

$linterface = new interface_html();

# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 0) ? 0 : 1;
$field = ($field == "") ? 0 : $field;

$li = new libdbtable2007($field, $order, $pageNo);

/*
if(sizeof($_POST)==0 && sizeof($_GET)==0 && $Status=="")
	$Status = 1;
*/
if($CatID2!="") $CatID = $CatID2;


# Del button
$btnOption .= "<a href=\"javascript:removeRecord(document.form1,'RecordID[]','remove.php')\" class=\"tool_delete\">" . $button_delete . "</a>";


## select Year
$currentyear = date('Y');
$array_year = $lrepairsystem->retrieveRecordYears();
if(empty($array_year))	$array_year[] = $currentyear;

$firstname = $Lang['RepairSystem']['AllYears'];
$select_year = getSelectByValueDiffName($array_year,$array_year,"name='year' id='year' onChange='document.form1.submit();'",$year,1,0, $firstname) . "&nbsp;";

## select Month
for ($i=1; $i<=12; $i++)
{
     $array_month[] = $i;
}
$firstname = $Lang['RepairSystem']['AllMonths'];
$select_month = getSelectByValueDiffName($array_month,$array_month,"name='month' id='month' onChange='document.form1.submit();'",$month,1,0,$firstname) . "&nbsp;";


$CategorySelect = $lrepairsystem->getCategorySelection($CatID, " onChange='document.form1.RequestID.selectedIndex=0; document.form1.submit()'", 1, $Lang['RepairSystem']['AllCategory'],"","",1);
$RequestSummarySelection = $lrepairsystem->getRequestSummarySelection($CatID," onChange='document.form1.submit()'","", $Lang['RepairSystem']['AllRequestSummary'],1,$RequestID,1);

## Location
//$LocationSelect = $lrepairsystem->getLocationSelection($locID, " onChange='document.form1.submit()'", 1, $Lang['RepairSystem']['AllLocation']);
$buildingAry = $lrepairsystem->getInventoryBuildingArray();
$buildingSelect = getSelectByArray($buildingAry, 'name="buildingID" id="building" onChange="document.form1.submit()"', $buildingID, 0, 0, "-- ".$Lang['RepairSystem']['Building']." --");

## Status
$StatusSelect = "<SELECT name='Status' id='Status' onChange='document.form1.submit()'>\n";
$StatusSelect.= "<OPTION value='' ".(($Status=='') ? "selected" : "").">".$eDiscipline['Setting_Status_All']."</OPTION>\n";
//$StatusSelect.= "<OPTION value='1' ".(($Status=='1') ? "selected" : "").">".$Lang['RepairSystem']['Processing']."</OPTION>\n";
$StatusSelect.= "<OPTION value='2' ".(($Status=='2') ? "selected" : "").">".$Lang['General']['Completed']."</OPTION>\n";
$StatusSelect.= "<OPTION value='0' ".(($Status=='0') ? "selected" : "").">".$i_status_cancel."</OPTION>\n";
$StatusSelect.= "<OPTION value='3' ".(($Status=='3') ? "selected" : "").">".$i_status_rejected."</OPTION>\n";
$StatusSelect.= "</SELECT>&nbsp;\n";

# Conditions

$conds = "";

if($CatID!="")
	$conds .= " AND a.CategoryID=$CatID";
	

if($buildingID != "")
	$conds .= " AND bu.BuildingID='$buildingID'";


if($year!="")
	$conds .= " AND a.DateInput LIKE '$year-%'";
	
if($month!="") {
	if($month<=9) $m = '0'.$month;
	$conds .= " AND a.DateInput LIKE '%-$m-%'";
}

$followupUserCond = "";
$isAdminUser = $lrepairsystem->IS_ADMIN_USER($UserID);
if(!$isAdminUser){
    $myFollowupRecordIDAry = $lrepairsystem->getMyFollowupRecord($_SESSION['UserID']);
    if (count($myFollowupRecordIDAry)) {
        $followupUserCond = "a.RecordID IN ('" . implode("','", (array)$myFollowupRecordIDAry) . "')";
    }
}

$keyword = standardizeFormPostValue($_POST['keyword']);
if($keyword!="")
	$ukw = mysql_real_escape_string(str_replace("\\","\\\\",$keyword));				// A&<>'"\B ==> A&<>\'\"\\\\B
	$ckw = intranet_htmlspecialchars(str_replace("\\","\\\\",$_POST['keyword']));	// A&<>'"\B ==> A&amp;&lt;&gt;\&#039;&quot;\\\\B
	
	$conds .= " AND (
					b.EnglishName LIKE '%$ukw%' OR
					b.ChineseName LIKE '%$ukw%' OR
					a.Title LIKE '%$ukw%' OR
					a.DetailsLocation LIKE '%$ukw%' OR
					a.Content LIKE '%$ukw%' OR
					a.CaseNumber LIKE '%$ukw%'OR
					b.EnglishName LIKE '%$ckw%' OR
					b.ChineseName LIKE '%$ckw%' OR
					a.Title LIKE '%$ckw%' OR
					a.DetailsLocation LIKE '%$ckw%' OR
					a.Content LIKE '%$ckw%' OR
					a.CaseNumber LIKE '%$ckw%'
				)";

if($requestFlag==1) 
	$conds .= " AND a.DateInput BETWEEN '$requestStart 00:00:00' AND '$requestEnd 23:59:59'";

if($completeFlag==1) 
	$conds .= " AND a.CompleteDate BETWEEN '$completeStart 00:00:00' AND '$completeEnd 23:59:59'";
	
if($Status!="") 
	$conds .= " AND a.RecordStatus='$Status'";

if(!$isAdminUser) {
	$sql = "SELECT CAT.CategoryID FROM REPAIR_SYSTEM_CATEGORY CAT LEFT OUTER JOIN REPAIR_SYSTEM_GROUP_MEMBER GPM ON (GPM.GroupID=CAT.GroupID) WHERE GPM.UserID=$UserID";
	$result = $lrepairsystem->returnVector($sql);

    if(sizeof($result)) {
        if ($followupUserCond) {
            $conds .= " AND (a.CategoryID IN (" . implode(',', $result) . ") OR ".$followupUserCond.")";
        }
        else {
            $conds .= " AND a.CategoryID IN (" . implode(',', $result) . ")";
        }
    }
    else {
        if ($followupUserCond) {
            $cond .= " AND " . $followupUserCond;
        }
        else {
            $conds .= " AND a.CategoryID IN (0)";
        }
    }
}

# request summary
$other_from .= $RequestID ? " and e.RecordID=$RequestID " :"";

# SQL Query
$name_field = getNameFieldByLang("b.");
$buildingName = Get_Lang_Selection("bu.NameChi", "bu.NameEng");
$levelName = Get_Lang_Selection("lv.NameChi", "lv.NameEng");
$LocationName = Get_Lang_Selection("l.NameChi", "l.NameEng");

$sql = "
		SELECT 
			a.CaseNumber,
			LEFT(a.DateInput,10) as DateInput,
			IF($name_field IS NULL,'--',$name_field) as name, 
			IF((a.LocationBuildingID!=0),	
				CONCAT(IF($buildingName IS NOT NULL, $buildingName, '".$Lang['RepairSystem']['MainBuilding']."'), IF($levelName!='', CONCAT(' &gt; ', $levelName), ''), IF($LocationName IS NOT NULL, CONCAT(' &gt; ', $LocationName), ''), IF(a.DetailsLocation!='', CONCAT(' &gt; ', a.DetailsLocation), '')), 
				CONCAT(c.LocationName, IF((a.DetailsLocation IS NOT NULL AND a.DetailsLocation!=''), CONCAT(' &gt; ', a.DetailsLocation), ''))) 
			as LocationName,
			CAT.Name as catName,
			concat('<a href=\'edit.php?RecordID=', a.RecordID ,'&CatID=$CatID&Status=$Status&year=$year&month=$month\'>',IF(a.Title IS NULL Or a.Title='','---', a.Title),'</a>') as title,
			CASE a.RecordStatus
				WHEN '1' THEN '". $Lang['RepairSystem']['Processing'] ."'
				WHEN '2' THEN '". $Lang['General']['Completed'] ."'
				WHEN '3' THEN '". $i_status_rejected ."'
				ELSE '". $i_status_cancel."'
			end as Status,
			CONCAT('<input type=checkbox name=RecordID[] id=RecordID[] value=', a.RecordID ,'>') as checkbox
		FROM 
			REPAIR_SYSTEM_RECORDS as a
			LEFT join INTRANET_USER as b on (b.UserID=a.UserID)
			LEFT JOIN INVENTORY_LOCATION l ON (l.LocationID=a.LocationID)
			LEFT JOIN INVENTORY_LOCATION_LEVEL lv ON (lv.LocationLevelID=a.LocationLevelID)
			LEFT JOIN INVENTORY_LOCATION_BUILDING as bu on (bu.BuildingID=a.LocationBuildingID)
			LEFT JOIN REPAIR_SYSTEM_LOCATION as c on (c.LocationID=a.LocationID)
			INNER JOIN REPAIR_SYSTEM_CATEGORY AS CAT ON (CAT.CategoryID=a.CategoryID)
			INNER JOIN REPAIR_SYSTEM_GROUP AS GP ON (GP.GroupID=CAT.GroupID)
			inner join REPAIR_SYSTEM_REQUEST_SUMMARY as e on (e.Title=a.Title and e.CategoryID=a.CategoryID  $other_from)
		WHERE 
			a.ArchivedBy IS NOT NULL AND a.RecordStatus!=".REPAIR_RECORD_STATUS_DELETED."
			and e.RecordStatus=1
			$conds
		";

$li->sql = $sql;
$li->field_array = array("CaseNumber","DateInput", "name", "LocationName", "catName", "title", "Status");
$li->no_col = sizeof($li->field_array)+2;
$li->IsColOff = "IP25_table";
$li->column_array = array(0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th width='5%' >".$li->column($pos++, $Lang['RepairSystem']['CaseNumber'])."</th>\n";
$li->column_list .= "<th width='9%' >".$li->column($pos++, $i_Discipline_Date)."</th>\n";
$li->column_list .= "<th width='19%' >".$li->column($pos++, $Lang['RepairSystem']['Reporter'])."</th>\n";
$li->column_list .= "<th width='19%' >".$li->column($pos++, $Lang['RepairSystem']['Location'])."</th>\n";
$li->column_list .= "<th width='19%' >".$li->column($pos++, $eDiscipline['Setting_Category'])."</th>\n";
$li->column_list .= "<th width='24%' >".$li->column($pos++, $Lang['RepairSystem']['Request'])."</th>\n";
$li->column_list .= "<th width='4%' >".$li->column($pos++, $i_general_status)."</th>\n";
$li->column_list .= "<th width='1'>".$li->check("RecordID[]")."</th>\n";

$keyword = intranet_htmlspecialchars($keyword);
//$toolbar = $linterface->GET_LNK_IMPORT("import.php",$button_import,"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("export.php?keyword=$keyword&CatID=$CatID&Status=$Status&year=$year&month=$month&requestFlag=$requestFlag&requestStart=$requestStart&requestEnd=$requestEnd&completeFlag=$completeFlag&completeStart=$completeStart&completeEnd=$completeEnd&RequestID=$RequestID&locID=$locID&archive=1",$button_export,"","","",0);

### Title ###
$TAGS_OBJ[] = array($Lang['RepairSystem']['AllRequests'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['RepairSystem']['ArchivedRequests'], "archive.php", 1);

$MODULE_OBJ = $lrepairsystem->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

## search
$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"$keyword\" onKeyUp=\"Check_Go_Search(event)\" />";

?>

<script language="javascript">
<!--
function checkForm() {
	var obj = document.form1;
	if(obj.RequestDate.checked==true && (compareDate(obj.requestEnd.value, obj.requestStart.value)<0)) {
		obj.requestStart.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}
	if(obj.CompleteDate.checked==true && (compareDate(obj.completeEnd.value, obj.completeStart.value)<0)) {
		obj.completeStart.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}
	return true;
}

function removeRecord(obj,element,page){
	
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else{
		if(confirm(alertConfirmRemove)){
			obj.action=page;
			obj.method="post";
			obj.submit();
		}
	}	
}

function Check_Go_Search(evt)
{
	var key = evt.which || evt.charCode || evt.keyCode;
	
	if (key == 13) // enter
		document.form1.submit();
	else
		return false;
}

function showSpan(spanName) {
	document.getElementById(spanName).style.display = 'inline';
}

function hideSpan(spanName) {
	document.getElementById(spanName).style.display = 'none';
}

function checkDisplay(spanName, flagName) {
	
	if(document.getElementById(flagName).value==1) {
		hideSpan(spanName);
		document.getElementById(flagName).value = 0;
	} else {
		showSpan(spanName);
		document.getElementById(flagName).value = 1;
	}
	if(document.getElementById('requestFlag').value==0 && document.getElementById('completeFlag').value==0) {
		$('#submit1').hide();
	}
	else {
		$('#submit1').show();
	}
	
}


-->
</script>

<form name="form1" method="POST" action="archive.php" onSubmit="return checkForm()">

<div class="content_top_tool">
	<?=$toolbar?>
	<div class="Conntent_search"><?=$searchTag?></div>     
	<br style="clear:both" />
</div>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
<td valign="bottom">
	<?=$CategorySelect?><?=$RequestSummarySelection?><?=$buildingSelect?><?/*=$LocationSelect*/?><?=$StatusSelect?>
	<br>
	<input type="checkbox" name="RequestDate" id="RequestDate" value="1" onClick="checkDisplay('spanRequest','requestFlag')" <? if($requestFlag==1) echo "checked";?>><label for="RequestDate"><?=$Lang['RepairSystem']['RequestDate']?></label>
	<div id="spanRequest" style="position:relative;display:<? if($requestFlag==1) {echo "inline";} else {echo "none";}?>"><?=$i_From?> : <?=$linterface->GET_DATE_PICKER("requestStart",$requestStart)?> <?=$i_To ?> <?=$linterface->GET_DATE_PICKER("requestEnd",$requestEnd)?></div>
	 | 
	<input type="checkbox" name="CompleteDate" id="CompleteDate" value="1" onClick="checkDisplay('spanComplete','completeFlag')" <? if($completeFlag==1) echo "checked";?>><label for="CompleteDate"><?=$Lang['RepairSystem']['CompleteDate']?></label>
	<div id="spanComplete" style="position:relative;display:<? if($completeFlag==1) {echo "inline";} else {echo "none";}?>"><?=$i_From?> : <?=$linterface->GET_DATE_PICKER("completeStart",$completeStart)?> <?=$i_To?> <?=$linterface->GET_DATE_PICKER("completeEnd",$completeEnd)?></div>
	<input type="submit" name="submit1" id="submit1" value="<?=$button_submit?>" style="<? if($requestFlag==1 || $completeFlag==1) {echo "display:inline";} else { echo "display:none";}?>">
</td>
<td valign="bottom">
	<div class="common_table_tool">
	<?=$btnOption?>
</td>
</tr>
</table>

<?=$li->display();?>


<br />
<input type="hidden" name="completeFlag" id="completeFlag" value="<?=$completeFlag?>" />
<input type="hidden" name="requestFlag" id="requestFlag" value="<?=$requestFlag?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>" />
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>" />
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="archivePage" id="archivePage" value="1" />


</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>