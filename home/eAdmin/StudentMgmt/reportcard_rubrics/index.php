<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");

intranet_opendb();
$libreportcard = new libreportcardrubrics();

unset($ck_ReportCard_Rubrics_UserType);
unset($IntranetReportCard_Rubrics_UserType);
	
if($from_eAdmin==1 && $_SESSION["SSV_USER_ACCESS"]["eAdmin-eReportCard_Rubrics"]==1)
{
	$IntranetReportCard_Rubrics_UserType = "ADMIN";
}
else
{
	if ($_SESSION['UserType'] == USERTYPE_STAFF)
		$IntranetReportCard_Rubrics_UserType = "TEACHER";
	else if ($_SESSION['UserType'] == USERTYPE_STUDENT)
		$IntranetReportCard_Rubrics_UserType = "STUDENT";
	else if ($_SESSION['UserType'] == USERTYPE_PARENT)
		$IntranetReportCard_Rubrics_UserType = "PARENT";
}

### Update cookies
$arrCookies = array();
$arrCookies[] = array("ck_ReportCard_Rubrics_UserType", "IntranetReportCard_Rubrics_UserType");
$arrCookies[] = array("ck_ReportCard_Rubrics_From_eAdmin", "from_eAdmin");
updateGetCookies($arrCookies);

### Get User Page Access Right from Admin Group
$PageAccessRightArr = $libreportcard->Get_User_Page_Access_Right($_SESSION['UserID'], $FromSession=1);

### check which page to re-direct to
$thisPath = '';
if ($IntranetReportCard_Rubrics_UserType == "ADMIN")
{
	// Admin -> Go to Settings
	$thisPath = $libreportcard->ModuleDocumentRoot.'settings/module/module.php';
}
else if ($IntranetReportCard_Rubrics_UserType == "TEACHER" && $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_subject_teacher"])
{
	// Subject Teacher -> Go to Marksheet
	$thisPath = $libreportcard->ModuleDocumentRoot.'management/marksheet_revision/marksheet_revision.php';
}
else if ($IntranetReportCard_Rubrics_UserType == "TEACHER" && $_SESSION["SSV_PRIVILEGE"]["reportcard_rubrics"]["is_class_teacher"])
{
	// Class Teacher only -> Go to Report
	$thisPath = $libreportcard->ModuleDocumentRoot.'reports/reportcard_generation/index.php';
}
else if (count($PageAccessRightArr) > 0)
{
	// User granted access right in the Admin Group => first page that have access right as the default page
	$LeftMenuArr = $libreportcard->Get_Left_Menu_Structure_Array($CheckAccessRight=1);
	$thisPath = '';
	foreach ((array)$LeftMenuArr as $thisMenu => $thisSubMenuArr)
	{
		foreach ((array)$thisSubMenuArr as $thisSubMenu => $thisSubMenuInfoArr)
		{
			$thisPageCode = $thisMenu.'_'.$thisSubMenu;
			if (in_array($thisPageCode, $PageAccessRightArr))
			{
				$thisPath = $thisSubMenuInfoArr['URL'];
				break(2);
			}
		}
	}
	
	if ($thisPath == '')
		No_Access_Right_Pop_Up();
}
else
{
	// not support student and parent account currently
	No_Access_Right_Pop_Up();
}


### check if the eRC active year is the current academic year
$activeAcademicYearID = $libreportcard->AcademicYearID;
$curAcademicYearID = Get_Current_Academic_Year_ID();

$alertNotCurrentYear = false;
if ($activeAcademicYearID != $curAcademicYearID)
{
	# not viewing current academic year data in eRC => show an alert to tell the user
	$alertNotCurrentYear = true;
	$activeAcademicYearName = $libreportcard->Get_Active_AcademicYearName();
	
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	$ObjAcademicYear = new academic_year($curAcademicYearID);
	$curAcademicYearName = $ObjAcademicYear->Get_Academic_Year_Name();
	
	$alertMsg = $Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearInfo'];
	$alertMsg = str_replace('|||--eRC Active Year--|||', $activeAcademicYearName, $alertMsg);
	$alertMsg = str_replace('|||--Current Academic Year--|||', $curAcademicYearName, $alertMsg);
	
	$alertMsg .= ($IntranetReportCard_Rubrics_UserType=='ADMIN')? $Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearAdmin'] : $Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['NotCurrentAcademicYearTeacher'];
}




?>

<script>

<? if ($alertNotCurrentYear == true) { ?>
	alert("<?=$alertMsg?>");
<? } ?>

window.location = '<?=$thisPath?>';

</script>