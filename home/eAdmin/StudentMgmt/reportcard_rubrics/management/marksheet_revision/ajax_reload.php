<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
$lreportcard_comment = new libreportcardrubrics_comment();
$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == "Student_Marksheet_Table")
{
	$YearTermID = $_REQUEST['YearTermID'];
	$ModuleID = $_REQUEST['ModuleID'];
	$SubjectID = $_REQUEST['SubjectID'];
	$StudentID = $_REQUEST['StudentID'];
	
	$SubjectGroupView = $_REQUEST['SubjectGroupView'];
	$SubjectGroupID = $_REQUEST['SubjectGroupID'];
	
	$FilterTopicInfoText = $_REQUEST['FilterTopicInfoText'];
	
	
	if ($SubjectGroupView == 1)
		echo $lreportcard_ui->Get_Management_MarksheetRevision_SubjectGroupView_Table($YearTermID, $SubjectID, $SubjectGroupID, $ModuleID);
	else
		echo $lreportcard_ui->Get_Management_MarksheetRevision_Table($YearTermID, $SubjectID, $StudentID, $ModuleID,$FilterTopicInfoText);
}
else if ($Action == "Comment_Bank")
{
	$TopicID = $_POST['TopicID'];
	$ModuleID = $_POST['ModuleID'];
	
	echo $lreportcard_ui->Get_CommentTable_for_Marksheet($ModuleID,$TopicID);
}
else if ($Action == "GetCommentByID")
{
	$CommentID = $_POST['CommentID'];
	
//	$thisReturnArray = $lreportcard_comment->Get_Comment_By_ID($ParTopicIDArr='',$CommentID);
//	$thisReturnArray = current($thisReturnArray);
//	$returnComment = $thisReturnArray['Comment'];
//	echo $returnComment;
	
	$CommentInfoArr = $lreportcard_comment->Get_Comment_Bank_Comment($keyword='', $TopicIDArr=0, $CommentID, $returnSubjectComment='');
	echo Get_Lang_Selection($CommentInfoArr[0]['CommentCh'], $CommentInfoArr[0]['CommentEn']);
}

intranet_closedb();
?>