<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - Updated logic for new csv format
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_MarksheetRevision";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_topic = new libreportcardrubrics_topic();

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$limport = new libimporttext();
$lo = new libfilesystem();

// $backPara = "YearTermID=".$YearTermID."&ModuleID=".$ModuleID."&SubjectID=".$SubjectID."&ClassID=".$ClassID;
$backPara = "YearTermID=".$YearTermID."&ModuleID=".$ModuleID."&SubjectID=".$SubjectID."&SubjectGroupID=".$SubjectGroupID;

# Check file format
$name = $_FILES['userfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT"))
{
    intranet_closedb();
    header("location: import.php?'.$backPara.'&ReturnMsgKey=WrongFileFormat");
    exit();
}

# Move CSV file to temp folder
$folder_prefix = $intranet_root."/file/import_temp/reportcard_rubrics/marksheet_input";
if (!file_exists($folder_prefix)) {
    $lo->folder_new($folder_prefix);
}
$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$targetFilePath = stripslashes($folder_prefix."/".$targetFileName);
$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($userfile, $targetFilePath);

# Get Default Cols
$defaultMarksheet_ColumnArr = $lreportcard_topic->Get_Student_Marksheet_Import_Export_Column_Title_Arr();

# Get CSV header & data
$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
$csvHeaderArr = array_shift($csvData);
//$csvStudentHeaderArr = array_shift($csvData);
//$csvHeader2Arr = array_shift($csvData);
$csvDataSize = count($csvData);

# Check 3 CSV header
$csvHeaderWrong = false;
//$DefaultMarksheet_HeaderArr = $defaultMarksheet_ColumnArr[0];
for($i=0; $i<count($defaultMarksheet_ColumnArr); $i++)
{
    if ($csvHeaderArr[$i] != $defaultMarksheet_ColumnArr[$i]) {
        $csvHeaderWrong = true;
        break;
    }
}
//$DefaultMarksheet_Header2Arr = $defaultMarksheet_ColumnArr[1];
//for($i=0; $i<count($DefaultMarksheet_Header2Arr); $i++)
//{
//    if ($csvStudentHeaderArr[$i] != $DefaultMarksheet_Header2Arr[$i]) {
//        $csvHeaderWrong = true;
//        break;
//    }
//}
//$DefaultMarksheet_Header3Arr = $defaultMarksheet_ColumnArr[2];
//for($i=0; $i<count($DefaultMarksheet_Header3Arr); $i++)
//{
//    if ($csvHeader2Arr[$i] != $DefaultMarksheet_Header3Arr[$i]) {
//        $csvHeaderWrong = true;
//        break;
//    }
//}

if($csvHeaderWrong || $csvDataSize == 0)
{
    $ReturnMsgKey = $csvHeaderWrong? 'WrongCSVHeader' : 'CSVFileNoData';
    intranet_closedb();
    
    header('location: import.php?'.$backPara.'&ReturnMsgKey='.$ReturnMsgKey);
    exit();
}

# Thickbox loading message
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
    $ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$csvDataSize, $Lang['General']['ImportArr']['RecordsValidated']);
$ProcessingMsg .= '</span>';

# Tag
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle']);

$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

echo $lreportcard_ui->Get_Management_Marksheet_Import_Step2_UI($targetFilePath, $csvDataSize);
?>

<script language="javascript">
$('document').ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Go_Back()
{
	window.location = 'import.php?<?=$backPara?>';
}

function js_Cancel()
{
	window.location = 'marksheet_revision.php';
}

function js_Continue()
{
	$('form#form1').submit();
}
</script>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>