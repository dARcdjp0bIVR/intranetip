<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - Updated logic for new csv format
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_MarksheetRevision";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_topic = new libreportcardrubrics_topic();

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

# Tag
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle']);

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

echo $lreportcard_ui->Get_Management_Marksheet_Import_UI();
?>

<script type="text/JavaScript" language="JavaScript">
var jsCurYearTermID = '<?=($_POST['YearTermID']? $_POST['YearTermID'] : $_GET['YearTermID'])?>';
var jsCurModuleID = '<?=($_POST['ModuleID']? $_POST['ModuleID'] : $_GET['ModuleID'])?>';
var jsCurSubjectID = '<?=($_POST['SubjectID']? $_POST['SubjectID'] : $_GET['SubjectID'])?>';
var jsCurSubjectGroupID = '<?=($_POST['SubjectGroupID']? $_POST['SubjectGroupID'] : $_GET['SubjectGroupID'])?>';
var jsCurClassID = '<?=($_POST['ClassID']? $_POST['ClassID'] : $_GET['ClassID'])?>';

$(document).ready( function() {
	js_Reload_Module_Selection();
});

function js_Changed_Term_Selection(jsYearTermID)
{
	jsCurYearTermID = jsYearTermID;
	js_Reload_Module_Selection();
}

function js_Changed_Module_Selection(jsModuleID)
{
	jsCurModuleID = jsModuleID;
	js_Reload_Subject_Selection();
}

function js_Changed_Subject_Selection(jsSubjectID)
{
	jsCurSubjectID = jsSubjectID;
    js_Reload_Subject_Group_Selection();
}

// function js_Changed_Class_Selection(jsClassID)
// {
// 	jsCurClassID = jsClassID;
// }

function js_Changed_Subject_Group_Selection(jsSubjectGroupID)
{
	jsCurSubjectGroupID = jsSubjectGroupID;
}

function js_Reload_Module_Selection(ParReload)
{
	$('td#ModuleSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Module_Selection',
			async: false,
			YearTermID: jsCurYearTermID,
			SelectionID: 'ModuleID',
			OnChange: 'js_Changed_Module_Selection(this.value)',
			NoFirst: 1,
			ModuleID: jsCurModuleID,
			IsAll: 0,
			CheckIsWithinDateRange: 1
		},
		function(ReturnData)
		{
			jsCurModuleID = $('select#ModuleID').val();
			js_Reload_Subject_Selection(ParReload);
		}
	);
}

function js_Reload_Subject_Selection(ParReload)
{
	$('td#SubjectSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Subject_Selection',
			async: false,
			YearTermID: jsCurYearTermID,
			ModuleID: jsCurModuleID,
			SubjectID: jsCurSubjectID,
			SelectionID: 'SubjectID',
			OnChange: 'js_Changed_Subject_Selection(this.value)',
			CheckModuleSubject: 1,
			NoFirst: 1,
			AccessibleSubjectOnly: 1
		},
		function(ReturnData)
		{
			jsCurSubjectID = $('select#SubjectID').val();
            js_Reload_Subject_Group_Selection(ParReload);
		}
	);
}

function js_Reload_Subject_Group_Selection(ParReload)
{
    $('td#SubjectGroupSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
        "../../ajax_reload.php",
        {
            Action: 'Subject_Group_Selection',
            async: false,
            YearTermID: jsCurYearTermID,
            SubjectID: jsCurSubjectID,
            SubjectGroupID: jsCurSubjectGroupID,
            SelectionID: 'SubjectGroupID',
            OnChange: 'js_Changed_Subject_Group_Selection(this.value)',
            NoFirst: 0,
            FirstTitle: '<?=$Lang['eRC_Rubrics']['GeneralArr']['SelectTitle']['AllSubjectGroup']?>',
            DisplayTermInfo: 1
        },
        function(ReturnData)
        {
            jsCurSubjectGroupID = $('select#SubjectGroupID').val();
        }
    );
}

function checkForm()
{
	if($('select#ModuleID').val() == null || $('select#ModuleID').val() == '')
	{
        alert('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Module']?>');
        $('select#ModuleID').focus();
        return false;
	}
	
	//if($('select#ClassID').val() == null || $('select#ClassID').val() == '')
	//{
     //   alert('<?//=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Class']?>//');
     //   $('select#ClassID').focus();
     //   return false;
	//}

	if($('select#SubjectID').val() == null || $('select#SubjectID').val() == '')
	{
        alert('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['Subject']?>');
        $('select#SubjectID').focus();
        return false;
	}

    if($('select#SubjectGroupID').val() == null)
    {
        alert('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Select']['SubjectGroup']?>');
        $('select#SubjectGroupID').focus();
        return false;
    }
	
	obj = document.form1;
	if (obj.elements["userfile"].value.Trim() == "")
	{
        alert('<?=$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['AlertSelectFile']?>');
        obj.elements["userfile"].focus();
        return false;
	}
	
	document.form1.action = 'import_step2.php';
	return true;
}

function jGEN_CSV_TEMPLATE()
{
	document.form1.action = 'export.php';
	document.form1.submit();
}

function js_Go_Back()
{
	window.location = 'marksheet_revision.php';
}
</script>
<br />

<?
print $linterface->FOCUS_ON_LOAD("form1.userfile");
$linterface->LAYOUT_STOP();
?>