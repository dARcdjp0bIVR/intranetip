<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_MarksheetRevision";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_MarksheetRevision_SubjectGroupView_UI();
?>

<script language="javascript">
var jsCurYearTermID = '';
var jsCurModuleID = '';
var jsCurSubjectID = '';
var jsCurSubjectGroupID = '';
var jsScreenWidth = parseInt(Get_Screen_Width());
var jsTableWidth = jsScreenWidth - 250;

// The variables for copy and paste are defined in the php function Get_Management_MarksheetRevision_Table()

$(document).ready( function() {	
	jsCurYearTermID = $('select#YearTermID').val();
	js_Reload_Module_Selection();
});

function js_Changed_Term_Selection(jsYearTermID)
{
	jsCurYearTermID = jsYearTermID;
	js_Reload_Module_Selection();
}

function js_Changed_Module_Selection(jsModuleID)
{
	jsCurModuleID = jsModuleID;
	js_Reload_Subject_Selection();
}

function js_Changed_Subject_Selection(jsSubjectID)
{
	jsCurSubjectID = jsSubjectID;
	
	if (jsCurSubjectID == '')
	{
		$('div#SubjectGroupSelectionDiv').html('');
		$('div#StudentSelectionDiv').html('');
		$('div#StudentMarksheetDiv').html('');
	}
	else
	{
		js_Reload_Subject_Group_Selection();
	}
}

function js_Changed_SubjectGroup_Selection(jsSubjectGroupID)
{
	jsCurSubjectGroupID = jsSubjectGroupID;
	js_Reload_Student_Marksheet_Table();
}

function js_Reload_Module_Selection()
{
	$('div#ModuleSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Module_Selection',
			YearTermID: jsCurYearTermID,
			SelectionID: 'ModuleID',
			OnChange: 'js_Changed_Module_Selection(this.value)',
			IsAll: 0,
			ModuleID: jsCurModuleID
		},
		function(ReturnData)
		{
			jsCurModuleID = $('select#ModuleID').val();
			js_Reload_Subject_Selection();
		}
	);
}

function js_Reload_Subject_Selection()
{
	$('div#SubjectSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Selection',
			YearTermID: jsCurYearTermID,
			ModuleID: jsCurModuleID,
			SelectionID: 'SubjectID',
			OnChange: 'js_Changed_Subject_Selection(this.value)',
			SubjectID: jsCurSubjectID,
			CheckModuleSubject: 1,
			FirstTitle: '<?=$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject']?>',
			NoFirst: 0,
			AccessibleSubjectOnly: 1
		},
		function(ReturnData)
		{
			jsCurSubjectID = $('select#SubjectID').val();
			
			if (jsCurSubjectID != '')
				js_Reload_Subject_Group_Selection();
		}
	);
}

function js_Reload_Subject_Group_Selection()
{
	var jsDisplayTermInfo = (jsCurYearTermID=='')? 1 : 0;
	
	$('div#SubjectGroupSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{ 
			Action: 'Subject_Group_Selection',
			SubjectID: jsCurSubjectID,
			YearTermID: jsCurYearTermID,
			SelectionID: 'SubjectGroupID',
			OnChange: 'js_Changed_SubjectGroup_Selection(this.value)',
			DisplayTermInfo: jsDisplayTermInfo,
			SubjectGroupID: jsCurSubjectGroupID
		},
		function(ReturnData)
		{
			// use the second value as the default selection if no specified
			if ($('select#SubjectGroupID').val() == '')
				$('select#SubjectGroupID option:nth-child(2)').attr('selected', true);
			
			jsCurSubjectGroupID = $('select#SubjectGroupID').val();
			js_Reload_Student_Marksheet_Table();
		}
	);
}

function js_Reload_Student_Marksheet_Table()
{
	if (jsCurSubjectGroupID == '' || jsCurSubjectGroupID == null)
	{
		$('div#StudentMarksheetDiv').html('');
	}
	else
	{
		$('div#StudentMarksheetDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"ajax_reload.php", 
			{ 
				Action: 'Student_Marksheet_Table',
				YearTermID: jsCurYearTermID,
				ModuleID: jsCurModuleID,
				SubjectID: jsCurSubjectID,
				SubjectGroupID: jsCurSubjectGroupID
			},
			function(ReturnData)
			{
				// do not use super table if the settings are not completed
				if (document.getElementById('Btn_Save') != null)
					js_Init_SuperTable();
			}
		);
	}
}

function js_Save_Marksheet()
{
	Block_Element('StudentMarksheetDiv');
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(data) {
			if (data=='1')
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				Scroll_To_Top();
				js_Reload_Student_Marksheet_Table();
			}
			else
			{
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>');
				Scroll_To_Top();
			}
			
			UnBlock_Element('StudentMarksheetDiv');
		} 
	});  
	return false;  
}

function js_Apply_Mark_To_All_Topic(jsModuleID)
{
	var jsCounter = 0;
	$('select.GlobalModule_' + jsModuleID).each(function(){
		jsCounter++;
		
		if (jsCounter == 2)	// SuperTable will clone table and therefore there will be more than 1 apply to all selection
			$('select.ChkModuleID_' + jsModuleID + ':enabled').val($(this).val());
	});
}

function js_Update_GlobalModule_Selection(jsID, jsValue)
{
	//alert($('select#' + jsID).val());
}

function js_Init_SuperTable()
{
    $("table#ContentTable").toSuperTable({
    	width: jsTableWidth + "px", 
    	height: "500px", 
    	fixedCols: '<?=$eRC_Rubrics_ConfigArr['MaxLevel']?>',
    	headerRows :3,
    	onFinish: function(){ fixSuperTableHeight('ContentTable', 400);  $(".sSky").css("font-size","0.9em");}
	});
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>