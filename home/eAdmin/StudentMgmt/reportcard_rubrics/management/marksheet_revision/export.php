<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - Updated logic for new csv format
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_MarksheetRevision";

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_rubrics = new libreportcardrubrics_rubrics();

### Class Student
//$StudentArr = $lreportcard_topic->GET_STUDENT_BY_CLASS($ClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=1);
//$StudentIDArr = array_keys($StudentArr);

### Year Term
if($YearTermID == '') {
    $YearTermInfo = getSemesters($lreportcard->Get_Active_AcademicYearID());
    $YearTermIDArr = array_keys((array)$YearTermInfo);
} else {
    $YearTermIDArr = array($YearTermID);
}

### Subject Group
$scm = new subject_class_mapping();
$SubjectGroupInfo = $scm->Get_Subject_Group_List($YearTermIDArr, $SubjectID, $returnAssociate=0, $TeachingOnly);
if($SubjectGroupID == '') {
    $SubjectGroupIDArr = Get_Array_By_Key($SubjectGroupInfo, 'SubjectGroupID');
} else {
    $SubjectGroupIDArr = array($SubjectGroupID);
}
$SubjectGroupInfo = BuildMultiKeyAssoc($SubjectGroupInfo, 'SubjectGroupID');

### Subject Group Student
$StudentIDArr = $scm->Get_Subject_Group_Student_List($SubjectGroupIDArr);
$StudentIDArr = Get_Array_By_Key($StudentIDArr, 'UserID');

$SubjectGroupStudentInfo = array();
foreach($SubjectGroupIDArr as $thisSubjectGroupID) {
    $thisSubjectGroupObj = new subject_term_class($thisSubjectGroupID);
    $SubjectGroupStudentInfo[$thisSubjectGroupID] = $thisSubjectGroupObj->Get_Subject_Group_Student_List($OrderByStudentName=0, $SortingOrder='', $WithStyle=0);
}

### Get Subject Topics Info
$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectID);
$TopicTreeArr = $lreportcard_topic->Get_Student_Study_Topic_Tree($StudentIDArr, $SubjectID, $ModuleID);

### Get Module Info
$ModuleInfoArr = $lreportcard_module->Get_Module_Info($YearTermID, $ModuleID);
$numOfModule = count($ModuleInfoArr);

// $TermModuleCountArr[$YearTermID] = num of Module in the Term
//$TermModuleCountArr = array();
//for ($i=0; $i<$numOfModule; $i++)
//{
//    $thisYearTermID = $ModuleInfoArr[$i]['YearTermID'];
//    $TermModuleCountArr[$thisYearTermID]++;
//}

### Get Student Study Topic Info
// $StudentStudyTopicInfoArr[$StudentID][$ModuleID][$SubjectID][$TopicID] = InfoArr
$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($ModuleID, $SubjectID, $TopicID='', $StudentIDArr);
$StudyingStudentIDArr = array_keys($StudentStudyTopicInfoArr);

### Get Subject Rubrics Info
$StudentClassInfoArr = $lreportcard_topic->Get_Student_Class_Info($StudentIDArr);
$YearID = $StudentClassInfoArr[0]['YearID'];

# Get Subject Rubrics Scheme
// $SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$RubricsItemID] = RubricsItemInfoArr
$SubjectRubricsInfoArr = $lreportcard_topic->Get_Subject_Rubrics_Mapping($SubjectID, $YearID);
if (is_array($SubjectRubricsInfoArr[$YearID])) {
    $tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[$YearID][$SubjectID]);
} else {
    $tmpArrKey = array_keys((array)$SubjectRubricsInfoArr[0][$SubjectID]);
}
$RubricsID = $tmpArrKey[0];
if ($RubricsID != '') {
    $RubricsInfoArr = $lreportcard_rubrics->Get_Rubics_Info($RubricsID);
}

### Get Subject Rubrics Item Info
$SubjectRubricsItemInfoArr = $SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID];

### Get Student Subject Score
// $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$TopicID][$ModuleID] = InfoArr
$StudentSubjectMarksheetScoreArr = $lreportcard_topic->Get_Student_Marksheet_Score($SubjectID, $StudentIDArr);

### CSV Header
$exportArr = array();
$exportArr[] = $lreportcard_topic->Get_Student_Marksheet_Import_Export_Column_Title_Arr();

### CSV Content
$includedStudentIDArr = array();
foreach((array)$SubjectGroupIDArr as $thisSubjectGroupID)
{
    $thisSubjectGroupInfo = $SubjectGroupInfo[$thisSubjectGroupID];
    $thisSubjectGroupCode = $thisSubjectGroupInfo['ClassCode'];
    $thisSubjectGroupName = $thisSubjectGroupInfo['ClassTitleB5'];
    // $thisSubjectGroupName = Get_Lang_Selection($thisSubjectGroupInfo['ClassTitleB5'], $thisSubjectGroupInfo['ClassTitleEN']);

    $thisStudentIDArr = $SubjectGroupStudentInfo[$thisSubjectGroupID];
    foreach((array)$thisStudentIDArr as $thisStudentInfo)
    {
        // skip not studying students
        $thisStudentID = $thisStudentInfo['UserID'];
        if(!in_array($thisStudentID, (array)$StudyingStudentIDArr)){
            continue;
        }

        // skip students already included
        if(in_array($thisStudentID, (array)$includedStudentIDArr)){
            continue;
        }
        $includedStudentIDArr[] = $thisStudentID;

        $thisStudentName = $thisStudentInfo['ChineseName'];
        $thisStudentUserLogin = $thisStudentInfo['UserLogin'];
        // $thisStudentName = Get_Lang_Selection($thisStudentInfo['ChineseName'], $thisStudentInfo['EnglishName']);

        foreach ((array)$TopicTreeArr as $thisTopicID => $thisTopicObjectiveIDArr)
        {
            // skip invalid Learning Topic
            if(!$thisTopicID || !isset($TopicInfoArr[$thisTopicID])) {
                continue;
            }

            foreach ((array)$thisTopicObjectiveIDArr as $thisObjectiveID => $thisObjectiveDetailsIDArr)
            {
                // skip invalid Learning Objective
                if(!$thisObjectiveID || !isset($TopicInfoArr[$thisObjectiveID])) {
                    continue;
                }

                foreach ((array)$thisObjectiveDetailsIDArr as $thisDetailsID => $thisDetailsArr)
                {
                    // skip invalid Learning Details
                    if(!$thisDetailsID || !isset($TopicInfoArr[$thisDetailsID])) {
                        continue;
                    }

                    $exportRowArr = array();
                    $exportRowArr[] = $thisSubjectGroupCode;
                    $exportRowArr[] = $thisSubjectGroupName;
                    $exportRowArr[] = $thisStudentUserLogin;
                    $exportRowArr[] = $thisStudentName;
                    $exportRowArr[] = $TopicInfoArr[$thisTopicID]['TopicCode'];
                    $exportRowArr[] = $TopicInfoArr[$thisTopicID]['TopicNameCh'];
                    $exportRowArr[] = $TopicInfoArr[$thisObjectiveID]['TopicCode'];
                    $exportRowArr[] = $TopicInfoArr[$thisObjectiveID]['TopicNameCh'];
                    $exportRowArr[] = $TopicInfoArr[$thisDetailsID]['TopicCode'];
                    $exportRowArr[] = $TopicInfoArr[$thisDetailsID]['TopicNameCh'];

                    if(!isset($StudentStudyTopicInfoArr[$thisStudentID][$ModuleID][$SubjectID][$thisDetailsID])) {
                        $exportRowArr[] = '--';
                        $exportRowArr[] = '--';
                    }
                    else {
                        $StudentScoreInfo = $StudentSubjectMarksheetScoreArr[$thisStudentID][$SubjectID][$thisDetailsID][$ModuleID];
                        $exportRowArr[] = $SubjectRubricsItemInfoArr[$StudentScoreInfo['RubricsItemID']]['LevelNameEn'];
                        $exportRowArr[] = $StudentScoreInfo['Remarks'];
                    }

                    $exportArr[] = $exportRowArr;
                }
            }
        }
    }
}

/*
// row 1 - Student Name
$exportRowArr = $defaultMarksheet_ColumnArr[0];
foreach((array)$StudentArr as $StudentID => $StudentInfo)
{
    // skip not studying students
    if(!in_array($StudentID, $StudyingStudentIDArr)) {
        continue;
    }
    
    $exportRowArr[] = $StudentInfo['StudentNameEn'].' ('.$StudentInfo['ClassTitleEn'].' - '.$StudentInfo['ClassNumber'].')';
    $exportRowArr[] = '';
}
$exportArr[] = $exportRowArr;

// row 2 - Student UserLogin
$exportRowArr = $defaultMarksheet_ColumnArr[1];
foreach((array)$StudentArr as $StudentID => $StudentInfo)
{
    // skip not studying students
    if(!in_array($StudentID, $StudyingStudentIDArr)) {
        continue;
    }
    
    $exportRowArr[] = $StudentInfo['UserLogin'];
    $exportRowArr[] = '';
}
$exportArr[] = $exportRowArr;

// row 3 - Static
$exportRowArr = $defaultMarksheet_ColumnArr[2];
foreach((array)$StudentArr as $StudentID => $StudentInfo)
{
    // skip not studying students
    if(!in_array($StudentID, $StudyingStudentIDArr)) {
        continue;
    }
    
    $exportRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['Level'];
    $exportRowArr[] = $Lang['eRC_Rubrics']['ManagementArr']['MarksheetRevisionArr']['ImportHeader']['Description'];
}
$exportArr[] = $exportRowArr;

$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel'];
// $numOfModule = count($ModuleInfoArr);

foreach ((array)$TopicTreeArr as $thisTopicID => $thisTopicObjectiveIDArr)
{
    // skip invalid Learning Topic
    if(!$thisTopicID || !isset($TopicInfoArr[$thisTopicID])) {
        continue;
    }
    
    foreach ((array)$thisTopicObjectiveIDArr as $thisObjectiveID => $thisObjectiveDetailsIDArr)
    {
        // skip invalid Learning Objective
        if(!$thisObjectiveID || !isset($TopicInfoArr[$thisObjectiveID])) {
            continue;
        }
        
        foreach ((array)$thisObjectiveDetailsIDArr as $thisDetailsID => $thisDetailsArr)
        {
            // skip invalid Learning Details
            if(!$thisDetailsID || !isset($TopicInfoArr[$thisDetailsID])) {
                continue;
            }
            
            $exportRowArr = array();
            $exportRowArr[] = $TopicInfoArr[$thisTopicID]['TopicCode'];
            $exportRowArr[] = $TopicInfoArr[$thisTopicID]['TopicNameEn'];
            $exportRowArr[] = $TopicInfoArr[$thisObjectiveID]['TopicCode'];
            $exportRowArr[] = $TopicInfoArr[$thisObjectiveID]['TopicNameEn'];
            $exportRowArr[] = $TopicInfoArr[$thisDetailsID]['TopicCode'];
            $exportRowArr[] = $TopicInfoArr[$thisDetailsID]['TopicNameEn'];
            
            foreach((array)$StudentArr as $StudentID => $StudentInfo)
            {
                // skip not studying students
                if(!in_array($StudentID, $StudyingStudentIDArr)){
                    continue;
                }
                
                if(!isset($StudentStudyTopicInfoArr[$StudentID][$ModuleID][$SubjectID][$thisDetailsID])) {
                    $exportRowArr[] = '--';
                    $exportRowArr[] = '--';
                }
                else {
                    
                    $StudentScoreInfo = $StudentSubjectMarksheetScoreArr[$StudentID][$SubjectID][$thisDetailsID][$ModuleID];
                    $exportRowArr[] = $SubjectRubricsItemInfoArr[$StudentScoreInfo['RubricsItemID']]['LevelNameEn'];
                    $exportRowArr[] = $StudentScoreInfo['Remarks'];
                }
            }
            
            $exportArr[] = $exportRowArr;
        }
    }
}
*/

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportArr, array());

intranet_closedb();

# Output the file to user browser
$lexport->EXPORT_FILE('marksheet_import.csv', $export_content);

?>