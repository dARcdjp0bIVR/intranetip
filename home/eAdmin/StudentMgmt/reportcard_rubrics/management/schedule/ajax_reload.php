<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
$lreportcard_ui = new libreportcardrubrics_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);
if ($Action == "Reload_Schedule_Table")
{
	echo $lreportcard_ui->Get_Management_Schedule_Table();
}
else if ($Action == "Reload_Schedule_Edit_Layer")
{
	$ModuleID = stripslashes($_REQUEST['ModuleID']);
	echo $lreportcard_ui->Get_Management_Schedule_Edit_Layer($ModuleID);
}

intranet_closedb();
?>