<?php
// using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Management_Schedule";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ScheduleArr']['MenuTitle']);
$linterface->LAYOUT_START();


echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_Schedule_UI();
?>

<script language="javascript">
$(document).ready( function() {	
	js_Reload_Schedule_Table();
});

function js_Reload_Schedule_Table()
{
	$('div#ScheduleDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php",
		{
			Action:"Reload_Schedule_Table"
		},
		function(ReturnData){
			initThickBox();
		}
	);
}

function js_Reload_Schedule_Edit_Layer(jsModuleID)
{
	$.post(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Schedule_Edit_Layer',
			ModuleID: jsModuleID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
		}
	);
}

function js_Update_Date_Input_Status(jsChecked)
{
	var setDisable = jsChecked? "" : "disabled";
	$("input#MarksheetSubmissionStartDate, input#MarksheetSubmissionEndDate").attr("disabled",setDisable);
	
	if (jsChecked)
		$("img.datepick-trigger").show();
	else
		$("img.datepick-trigger").hide();
}

function js_Update_Schedule()
{
	var jsModuleID = $('input#ModuleID').val();
	var jsApplyDate = $('input#ApplyDate').attr('checked');
	var jsMarksheetSubmissionStartDate = $('input#MarksheetSubmissionStartDate').val();
	var jsMarksheetSubmissionEndDate = $('input#MarksheetSubmissionEndDate').val();
	
	if (jsApplyDate == false)
	{
		jsMarksheetSubmissionStartDate = '';
		jsMarksheetSubmissionEndDate = '';
	}
	else
	{
		if(compareDate(jsMarksheetSubmissionEndDate, jsMarksheetSubmissionStartDate) < 0) {
			$("div#WarnEndDate").html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Date']['EndDateCannotEarlierThanStartDate']?>').show();
			return false;
		}
		else
		{
			$("div#WarnEndDate").html('');
		}
	}
	
	$.post(
		"ajax_update.php", 
		{ 
			Action: "Update_Module_Schedule",
			ModuleID: jsModuleID,
			MarksheetSubmissionStartDate: jsMarksheetSubmissionStartDate,
			MarksheetSubmissionEndDate: jsMarksheetSubmissionEndDate
		},
		function(ReturnData)
		{
			js_Hide_ThickBox();
			js_Reload_Schedule_Table();
			
			// Show system messages
			var jsReturnMsg;
			if (ReturnData == '1')
				jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
			else
				jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
				
			Get_Return_Message(jsReturnMsg);
			Scroll_To_Top();
		}
	);
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>