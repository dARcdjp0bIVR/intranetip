<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_OtherInfo";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_other_info_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_other_info_page_number", "pageNo");
$arrCookies[] = array("ck_other_info_page_order", "order");
$arrCookies[] = array("ck_other_info_page_field", "field");	
$arrCookies[] = array("ck_other_info_reportid", "ReportID");
$arrCookies[] = array("ck_other_info_classlevelid", "ClassLevelID");	
$arrCookies[] = array("ck_other_info_classid", "ClassID");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_settings_other_info_page_size = '';
}
else 
	updateGetCookies($arrCookies);

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

#init $UploadType
if(trim($CategoryID)=='')
{
	$CategoryArr = $lreportcard->Get_Other_Info_Category();
	$CategoryID = 	$CategoryArr[0]['CategoryID'];
}

# tag information
$TAGS_OBJ = $lreportcard_ui->getOtherInfoTabObjArr($CategoryID);

# Return Message
$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];

$linterface->LAYOUT_START($ReturnMsg);

echo $lreportcard_ui->Get_Management_OtherInfo_UI($CategoryID,$ReportID, $ClassLevelID, $ClassID,$field ,$order ,$pageNo);

?>
<script>
function aj_Reload_Class_Selection()
{
	var ClassLevelID = $("select#ClassLevelID").val();
	$.post(
		"../../ajax_reload.php",
		{
			Action	:"Accessible_Class_Selection",
			ClassLevelID	:  ClassLevelID,
			isAll	: 1
		},
		function (ReturnData)
		{
			$("span#ClassSelection").html(ReturnData);
		}
	);
}

function js_Upload()
{
	var ReportID = $("select#ReportID").val();
	var ClassID = $("select#ClassID").val();
	var ClassLevelID = $("select#ClassLevelID").val();
	var CategoryID = $("input#CategoryID").val();
	
	var url = "upload.php?";
//	url += "ReportID="+ReportID;
//	url += "&ClassID="+ClassID;
//	url += "&ClassLevelID="+ClassLevelID;
	url += "&CategoryID="+CategoryID;
	
	window.location.href = url; 
}

</script>

<?

$linterface->LAYOUT_STOP();

intranet_closedb();
?>