<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_OtherInfo";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$OtherInfoItemInfo = $lreportcard->Get_OtherInfo_Item_Info($CategoryID);
$SampleStudentInfo = $lreportcard->Get_OtherInfo_Sample_Student_Info();

$fieldName = array();
$sampleData = array();

list($fieldName,$sampleData) = $SampleStudentInfo; // append student info sample
for($i=0; $i<sizeof($OtherInfoItemInfo); $i++) {
	$fieldName[] = $OtherInfoItemInfo[$i]["ItemNameEn"];
	$sampleData[0][] = "(".$OtherInfoItemInfo[$i]["ItemNameCh"].")";
	$sampleData[1][] = $OtherInfoItemInfo[$i]["Sample1"];
	$sampleData[2][] = $OtherInfoItemInfo[$i]["Sample2"];
	$sampleData[3][] = $OtherInfoItemInfo[$i]["Sample3"];
}
$sampleData[0][] = $Lang['General']['ImportArr']['SecondRowWarn'];// append (do not remove this row)

$filename = $OtherInfoItemInfo[0]['CategoryCode']."_sample.csv";

include_once($PATH_WRT_ROOT."includes/libexporttext.php");
$lexport = new libexporttext();
$exportContent = $lexport->GET_EXPORT_TXT($sampleData, $fieldName);
intranet_closedb();
$lexport->EXPORT_FILE($filename, $exportContent);
	
intranet_closedb();		
?>