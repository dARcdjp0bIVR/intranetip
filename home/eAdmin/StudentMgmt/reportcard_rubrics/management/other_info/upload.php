<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_OtherInfo";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_other_info_reportid", "ReportID");
$arrCookies[] = array("ck_other_info_classlevelid", "ClassLevelID");	
$arrCookies[] = array("ck_other_info_classid", "ClassID");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else 
	updateGetCookies($arrCookies);

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ = $lreportcard_ui->getOtherInfoTabObjArr($CategoryID);

#Return Message
$ReturnMessage = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMessage);

echo $lreportcard_ui->Get_Management_OtherInfo_Import_UI($CategoryID,$ReportID,$ClassLevelID,$ClassID);

?>
<script>
function aj_Reload_Class_Selection(ClassID)
{
	var ClassLevelID = $("select#ClassLevelID").val();
	$.post(
		"../../ajax_reload.php",
		{
			Action	:"Accessible_Class_Selection",
			ClassLevelID	:  ClassLevelID,
			ClassID	:  ClassID,
			isAll	: 1,
			OnChange:"aj_Check_Record_Exist()"
		},
		function (ReturnData)
		{
			$("span#ClassSelection").html(ReturnData);
			aj_Check_Record_Exist();
		}
	);
}
 
var AjaxLoading= false;
var SubmitAfterAjax = false;
var FileExist = false;
function aj_Check_Record_Exist()
{
	AjaxLoading = true;
	var ReportID = $("select#ReportID").val();
	var ClassLevelID = $("select#ClassLevelID").val();
	var ClassID = $("select#ClassID").val();
	var CategoryID = $("input#CategoryID").val();
	
	$.post(
		"check_exist_file.php",
		{
			ReportID:ReportID,
			ClassLevelID:ClassLevelID,
			ClassID:ClassID,
			CategoryID:CategoryID
		},
		function (ReturnData)
		{
			AjaxLoading = false;
			FileExist = ReturnData==1;
			if(SubmitAfterAjax)
			{
				if(checkForm())
					document.form1.submit();
			}
			
		}
	);
}


function checkForm()
{
	if($("input#userfile").val().Trim()=='')
	{
		alert("<?=$Lang['General']['warnSelectcsvFile']?>");
		return false;
	}

	if(AjaxLoading)
	{
		SubmitAfterAjax = true;
		return false;
	}
	
	if(FileExist==true)
	{
		return confirm("<?=$Lang['eRC_Rubrics']['ManagementArr']['OtherInfoArr']['jsConfirmMsg']['OriginalInformationReplace']?>");
	}
		
	return true;	
}

function js_Export(CategoryID)
{
	window.location.href="get_sample_csv.php?CategoryID="+CategoryID;	
}

$().ready(function(){
	aj_Reload_Class_Selection("<?=$ClassID?>");
});
</script>

<?

$linterface->LAYOUT_STOP();

intranet_closedb();
?>