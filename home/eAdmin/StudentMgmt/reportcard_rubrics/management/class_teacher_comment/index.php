<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

### Cookies handling
# set cookies
$arrCookies[] = array("ck_settings_class_teacher_comment_report_id", "ReportID");
if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();

# tag information
if ($sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'])
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Student'], "index.php", 1);
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['Comment'], "index_comment.php", 0);
}
else
{
	$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['MenuTitle'], "", 0);
}

$ReturnMsg = $Lang['General']['ReturnMessage'][$Result];

$linterface->LAYOUT_START($ReturnMsg);

$lreportcard_ui = new libreportcardrubrics_ui();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_Class_Teacher_Comment_UI($ReportID);

?>
<script>
function js_Reload_Comment_Table()
{
	var ReportID = $("select#ReportID").val();
	if(!ReportID)
	{
		$("div#CommentTableDiv").html("");
		return false;
	}

	Block_Element('CommentTableDiv');
	
	$.post(
		"ajax_reload.php",
		{
			ReportID:ReportID,
			Action:"ReloadCommentTable"
		},
		function (ReturnData)
		{
			$("div#CommentTableDiv").html(ReturnData);
			UnBlock_Element('CommentTableDiv');
		}
	);
}

$().ready(function(){
	js_Reload_Comment_Table();
});
</script>
<?
       $linterface->LAYOUT_STOP();
       intranet_closedb();

?>