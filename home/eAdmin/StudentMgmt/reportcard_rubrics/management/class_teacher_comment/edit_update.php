<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!isset($_POST) && (!isset($comment) || !isset($commentID)))
	header("Location:index.php");

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_comment = new libreportcardrubrics_comment();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$ReportID = $_REQUEST['ReportID'];
$NewComment = $_REQUEST['NewComment'];
$Comment = $_REQUEST['Comment'];

if(!empty($NewComment))
	$Success["InsertComment"] = $lreportcard_comment->Insert_Comment($ReportID,$NewComment, 0);
if(!empty($Comment))
	$Success["UpdateComment"] = $lreportcard_comment->Update_Comment_By_CommentID($ReportID,$Comment);

intranet_closedb();

$Result = (!in_array(false,$Success)) ? "UpdateSuccess" : "UpdateUnsuccess";
$parmeters = "ClassID=$ClassID&ReportID=$ReportID";
header("Location:index.php?$parmeters&Result=$Result");
?>

