<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_comment = new libreportcardrubrics_comment();

if (!isset($_GET) || !isset( $ReportID)) {
	header("Location: index.php");
	exit();
}

// Class
if(trim($ClassLevelID)!='')
{
	$ClassArr = $lreportcard->Get_User_Accessible_Class($ClassLevelID);
	$ClassIDArr = Get_Array_By_Key($ClassArr,"ClassID");
}
else
{
	$ClassIDArr[] = $ClassID;
}

# Import Header
$ImportHeader = $Lang['eRC_Rubrics']['ManagementArr']['ClassTeacherCommentArr']['ImportHeader']; 
$ColHeader = array(
	$ImportHeader['ClassName']['En'],
	$ImportHeader['ClassNumber']['En'],
	$ImportHeader['UserLogin']['En'],
	$ImportHeader['WebSamsRegNo']['En'],
	$ImportHeader['StudentName']['En'],
	$ImportHeader['Comment']['En']
);

$ColHeader2 = array(
	$ImportHeader['ClassName']['Ch'],
	$ImportHeader['ClassNumber']['Ch'],
	$ImportHeader['UserLogin']['Ch'],
	$ImportHeader['WebSamsRegNo']['Ch'],
	$ImportHeader['StudentName']['Ch'],
	$ImportHeader['Comment']['Ch'],
	$Lang['General']['ImportArr']['SecondRowWarn']
);

# chinese header
$ExportArr[] = $ColHeader2;

foreach((array) $ClassIDArr as $thisClassID)
{
	$StudentArr = $lreportcard->GET_STUDENT_BY_CLASS($thisClassID);
	
	$CommentArr = $lreportcard_comment->Get_Student_Class_Teacher_Comment($ReportID,'',0);
	$CommentArr = BuildMultiKeyAssoc($CommentArr,"StudentID","Comment",1);
	
	foreach((array)$StudentArr as $thisStudent)
	{
		$StudentID = $thisStudent['UserID'];
		$ClassName = $thisStudent['ClassTitleEn'];
		$ClassNumber = $thisStudent['ClassNumber'];
		$UserLogin = $thisStudent['UserLogin'];
		$WebSAMSRegNo = $thisStudent['WebSAMSRegNo'];
		$StudentName = $thisStudent['StudentName'];
		if(!$is_template)
			$comment = $CommentArr[$StudentID];
			
		$ExportArr[] = array($ClassName,$ClassNumber,$UserLogin,$WebSAMSRegNo,$StudentName,$comment); 
	}
	
}


$lexport = new libexporttext();

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $ColHeader,  "", "\r\n", "", 0, "11",1);

// Output the file to user browser
$ReportInfo = $lreportcard->GetReportTemplateInfo($ReportID);
$ReportName = $ReportInfo[0]['ReportTitleEn'];

if(trim($ClassLevelID)!='')
	$DisplayName = $lreportcard->returnClassLevel($ClassLevelID);
else
	$DisplayName = $ClassName;

$filename = $DisplayName."_".$ReportName."_0.csv";

intranet_closedb();
$filename = str_replace(' ', '_', $filename);
$lexport->EXPORT_FILE($filename, $export_content);

?>
