<?php
// using 
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ClassTeacherComment";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

intranet_auth();
intranet_opendb();

$lreportcard = new libreportcardrubrics_custom();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_comment = new libreportcardrubrics_comment();

$ReportID = $_POST['ReportID'];
$SelectedUserIDArr = $_POST['SelectedUserIDArr'];
$CommentIDArr = unserialize(rawurldecode($CommentIDArr));

//$ReportInfoArr = $lreportcard->returnReportTemplateBasicInfo($ReportID);
$YearID = $ReportInfoArr['ClassLevelID'];
$SelectedUserIDArr = $lreportcard->Get_User_Array_From_Selection($SelectedUserIDArr, $YearID);

$success = $lreportcard_comment->Append_Class_Teacher_Comment($ReportID, $SelectedUserIDArr, $CommentIDArr);

intranet_closedb();

$ReturnMsgKey = ($success)? 'AddCommentSuccess' : 'AddCommentFailed';
header("Location:index_comment.php?ReturnMsgKey=".$ReturnMsgKey."&Keyword=".$Keyword);

?>

