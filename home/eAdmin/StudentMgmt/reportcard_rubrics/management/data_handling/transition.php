<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Management_DataHandling";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();


############## Interface Start ##############

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['MenuTitle']);
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$ReturnMsg = $Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['UpdateArr'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);


$currentAcademicYear = getCurrentAcademicYear('');
$activeAcademicYear = $lreportcard->AcademicYearID;
$activeAcademicYearDisplay = $lreportcard->Get_Active_AcademicYearName();
		
$sql = "SHOW DATABASES LIKE '".$intranet_db."_DB_REPORT_CARD_RUBRICS_%'";
$reportcardDB = $lreportcard->returnVector($sql);
		
### Get Next Academic Year which did not have eRC database yet
$ActiveYear_Obj = new academic_year($activeAcademicYear);
$ActiveYearStart = $ActiveYear_Obj->AcademicYearStart;

$libFCM = new form_class_manage();
$AcademicYearArr = $libFCM->Get_Academic_Year_List($AcademicYearID='', $OrderBySequence=0, $excludeYearIDArr=array(), $NoPastYear=0, $PastAndCurrentYearOnly=0, $ExcludeCurrentYear=0, $ComparePastYearID='', $SortOrder='Asc');
$numOfAcademicYear = count($AcademicYearArr);
for ($i=0; $i<$numOfAcademicYear; $i++)
{
	$thisAcademicYearID = $AcademicYearArr[$i]['AcademicYearID'];
	$thisAcademicYearStart = $AcademicYearArr[$i]['AcademicYearStart'];
	$thisDBName = $intranet_db."_DB_REPORT_CARD_RUBRICS_".$thisAcademicYearID;
	
	if ($thisAcademicYearStart > $ActiveYearStart && !in_array($thisDBName, $reportcardDB))
	{
		$newAcademicYearID = $thisAcademicYearID;
		
		break;
	}
}
$NewYear_Obj = new academic_year($newAcademicYearID);
$newAcademicYearDisplay = $NewYear_Obj->Get_Academic_Year_Name();
		
## Check if the data in the next term is complete or not
$NewYearTermArr = $NewYear_Obj->Get_Term_List(0);

if ($newAcademicYearID=='' || count($NewYearTermArr) == 0)
{
	$noTermNextYear = 1;
	$disableNewYear = 'disabled';
	$checkedNewYear = '';
	$checkedTransistion = 'checked';
	$disableOtherYearSelect = '';
	
	$warning = '<span class="tabletextrequire"><b> *'.$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['NextYearTermNotComplete'].'</b></span>';
}
else
{
	$noTermNextYear = 0;
	$disableNewYear = '';
	$checkedNewYear = 'checked';
	$checkedTransistion = '';
	$disableOtherYearSelect = 'disabled';
	
	$warning = '';
}


$otherYearSelectControl = "";
$numOfDB = count($reportcardDB);
# only let user select previous year if there're other years other than the active one
if ($numOfDB > 1) {
	for($i=0; $i<$numOfDB; $i++) {
		$thisDBName = $reportcardDB[$i];
		$thisDBPieces = explode('_', $thisDBName);
		$thisAcademicYearID = $thisDBPieces[count($thisDBPieces) - 1];
		
		if ($thisAcademicYearID != '' && $thisAcademicYearID != $activeAcademicYear)
		{
			$thisAcademicYearObj = new academic_year($thisAcademicYearID);
			$reportcardDBArr[] = array($thisAcademicYearID, $thisAcademicYearObj->Get_Academic_Year_Name());
		}
	}
	
	if (count($reportcardDBArr) > 0)
	{
		# have other years of eRC => can let the user to switch between years
		$yearRadioDisabled = '';
	}
	else
	{
		# no other years of eRC => disable the year selection
		$yearRadioDisabled = 'disabled';
		$disableOtherYearSelect = 'disabled';
		$checkedNewYear = 'checked';
		$checkedTransistion = '';
	}
	$otherYearSelect = $linterface->GET_SELECTION_BOX($reportcardDBArr, "name='OtherYear' id='OtherYear' class='' $disableOtherYearSelect onchange=''", "", "");
	
	$otherYearSelectControl = "<br />
								<input type='radio' name='transitionAction' id='transitionAction2' value='other' onclick='toggleAction(this.value)' $checkedTransistion $yearRadioDisabled /> 
								<label for='transitionAction2'>".$Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['OtherAcademicYears']."</label> ".$otherYearSelect;
}

$WarningMsg = implode('<br />', $Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['WarningArr']);
$WarningMsgBox = $linterface->Get_Warning_Message_Box('<span class="tabletextrequire">'.$Lang['General']['Warning'].'</span>', $WarningMsg, $others="");

?>


<script type="text/JavaScript" language="JavaScript">
function toggleAction(type) {
	var otherYearSelect = document.getElementById("OtherYear");
	if (type == "other") {
		otherYearSelect.disabled = false;
	} else {
		otherYearSelect.disabled = true;
	}
}

function confirmSubmit() {
	return confirm("<?= $Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['jsWarningArr']['ConfirmTransition'] ?>");
}
</script>


<form name="form1" action="transition_update.php" method="POST" onsubmit="return confirmSubmit()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td align='' colspan='2'><?=$WarningMsgBox?></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $Lang['eRC_Rubrics']['GeneralArr']['ActiveAcademicYear'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $activeAcademicYearDisplay ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $Lang['eRC_Rubrics']['GeneralArr']['CurrentAcademicYear'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<?= $currentAcademicYear ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?= $Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['TransitTo'] ?>
					</td>
					<td width="75%" class='tabletext'>
						<input type="radio" name="transitionAction" id="transitionAction1" value="<?=$newAcademicYearID?>" <?=$checkedNewYear?> onclick="toggleAction(this.value)" <?=$disableNewYear?> /> 
						<label for="transitionAction1"><?= $Lang['eRC_Rubrics']['ManagementArr']['DataHandlingArr']['DataTransitionArr']['NewAcademicYear']." ($newAcademicYearDisplay) $warning" ?></label>
						<?= $otherYearSelectControl ?>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan='2' class="dotline">
						<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
					</td>
				</tr>
				<tr>
					<td colspan='2' align="center">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "") ?>
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='transition.php'") ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>