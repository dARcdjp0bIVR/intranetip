<?php
// Using:
/*
 *  Date :  2019-07-29 (Bill)
 *          - Updated logic for new csv format
 *
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ECAService";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_eca = new libreportcardrubrics_eca();

# Form & Class
$FormArr = BuildMultiKeyAssoc($lreportcard_eca->Get_User_Accessible_Form(), "ClassLevelID");
$ClassArr = BuildMultiKeyAssoc($lreportcard_eca->Get_User_Accessible_Class(), array("ClassLevelID", "ClassID"));

$FormClassSizeArr = array();
foreach((array)$ClassArr as $FormID => $FormClassArr) {
    $FormClassSizeArr[$FormID] = count((array)$FormClassArr);
}
$FormIDArr = array_keys($FormArr);

# Class Student
$StudentArr = array();
foreach((array)$ClassArr as $FormID => $FormClassArr) {
    foreach((array)$FormClassArr as $ClassID => $ClassInfoArr) {
        $StudentArr[$ClassID] = $lreportcard_eca->GET_STUDENT_BY_CLASS($ClassID, $ParStudentIDList="", $isShowBothLangs=0, $withClassNumber=0, $isShowStyle=0, $ReturnAsso=1);
    }
}

$ClassStudentSizeArr = array();
foreach($StudentArr as $ClassID => $ClassStudentArr) {
    $ClassStudentSizeArr[$ClassID] = count((array)$ClassStudentArr);
}

# ECA Category & Item
$ECA_CatArr = BuildMultiKeyAssoc($lreportcard_eca->Get_ECA_Category_Info(), "EcaCategoryID");
$ECA_SubCatArr = BuildMultiKeyAssoc($lreportcard_eca->Get_ECA_SubCategory_Info(), array("EcaCategoryID", "EcaSubCategoryID"));
$ECA_CatItemArr = BuildMultiKeyAssoc($lreportcard_eca->Get_ECA_Category_Item_Info(), array("EcaCategoryID", "EcaSubCategoryID", "EcaItemID"));

# Student Selected ECA Item
$Student_ECARecordArr = $lreportcard_eca->Get_Report_Student_ECA_Record($ReportID);
$Student_ECARecordArr = BuildMultiKeyAssoc($Student_ECARecordArr, array("EcaItemID", "StudentID"));

# Default Cols
$defaultECA_ColumnArr = $lreportcard_eca->Get_Student_ECA_Import_Export_Column_Title_Arr();
list($defaultECA_Column1, $defaultECA_Column2) = $defaultECA_ColumnArr;

foreach((array)$ECA_CatItemArr as $ECA_CatID => $ECA_SubCatItemArr)
{
    // skip invalid ECA Category
    if(!$ECA_CatID || !isset($ECA_CatArr[$ECA_CatID])) {
        continue;
    }

    foreach((array)$ECA_SubCatItemArr as $ECA_SubCatID => $ECA_ItemArr)
    {
        // skip invalid ECA Sub-Category
        if(!$ECA_SubCatID || !isset($ECA_SubCatArr[$ECA_CatID][$ECA_SubCatID])) {
            continue;
        }

        foreach((array)$ECA_ItemArr as $ECA_ItemID => $ECA_ItemInfo)
        {
            // skip invalid ECA Item
            if(!$ECA_ItemID || !isset($ECA_CatItemArr[$ECA_CatID][$ECA_SubCatID][$ECA_ItemID])) {
                continue;
            }

            $defaultECA_Column1[] = $ECA_ItemInfo['EcaItemCode'];
            $defaultECA_Column2[] = $ECA_ItemInfo['EcaItemNameCh'];
        }
    }
}

### CSV Header
$exportArr = array();
$exportArr[] = $defaultECA_Column1;
$exportArr[] = $defaultECA_Column2;

### CSV Content
foreach((array)$ClassArr as $FormID => $FormClassArr)
{
    // skip invalid form
    if(!isset($FormArr[$FormID])) {
        continue;
    }
    
    foreach((array)$FormClassArr as $ClassID => $ClassInfoArr)
    {
        $studentSize = $ClassStudentSizeArr[$ClassID]? $ClassStudentSizeArr[$ClassID] : 0;
        if($studentSize > 0)
        {
            $ClassStudentArr = $StudentArr[$ClassID];
            foreach((array)$ClassStudentArr as $StudentID => $StudentInfoArr)
            {
                $exportRowArr = array();
                $exportRowArr[] = $StudentInfoArr['ClassTitleEn'];
                $exportRowArr[] = $StudentInfoArr['ClassNumber'];
                $exportRowArr[] = $StudentInfoArr['UserLogin'];
                $exportRowArr[] = $StudentInfoArr['StudentNameCh'];

                foreach((array)$ECA_CatItemArr as $ECA_CatID => $ECA_SubCatItemArr)
                {
                    // skip invalid ECA Category
                    if(!$ECA_CatID || !isset($ECA_CatArr[$ECA_CatID])) {
                        continue;
                    }

                    foreach((array)$ECA_SubCatItemArr as $ECA_SubCatID => $ECA_ItemArr)
                    {
                        // skip invalid ECA Subcategory
                        if(!$ECA_SubCatID || !isset($ECA_SubCatArr[$ECA_CatID][$ECA_SubCatID])) {
                            continue;
                        }

                        foreach((array)$ECA_ItemArr as $ECA_ItemID => $ECA_ItemInfo)
                        {
                            // skip invalid ECA Item
                            if(!$ECA_ItemID || !isset($ECA_CatItemArr[$ECA_CatID][$ECA_SubCatID][$ECA_ItemID])) {
                                continue;
                            }

                            $isSelected = isset($Student_ECARecordArr[$ECA_ItemID][$StudentID]);
                            $exportRowArr[] = $isSelected? 'Y' : '';
                        }
                    }
                }

                $exportArr[] = $exportRowArr;
            }
        }
    }
}

/*
// row 1 - Class Name
$exportRowArr = $defaultECA_ColumnArr[0];
foreach((array)$ClassArr as $FormID => $FormClassArr)
{
    // skip invalid form
    if(!isset($FormArr[$FormID])) {
        continue;
    }

    foreach((array)$FormClassArr as $ClassID => $ClassInfoArr) {
        $studentSize = $ClassStudentSizeArr[$ClassID]? $ClassStudentSizeArr[$ClassID] : 0;
        if($studentSize > 0) {
            $exportRowArr[] = $ClassInfoArr['ClassTitleEN'].' ('.$ClassInfoArr['YearName'].')';

            for($i=0; $i<($studentSize - 1); $i++) {
                $exportRowArr[] = '';
            }
        }
    }
}
$exportArr[] = $exportRowArr;

// row 2 - Student Name with Class Name
$exportRowArr = $defaultECA_ColumnArr[1];
foreach((array)$ClassArr as $FormID => $FormClassArr)
{
    // skip invalid form
    if(!isset($FormArr[$FormID])) {
        continue;
    }
    
    foreach((array)$FormClassArr as $ClassID => $ClassInfoArr) {
        $ClassStudentArr = $StudentArr[$ClassID];
        foreach((array)$ClassStudentArr as $StudentID => $StudentInfoArr) {
            $exportRowArr[] = $StudentInfoArr['StudentNameEn'].' ('.$ClassInfoArr['ClassTitleEN'].' - '.$StudentInfoArr['ClassNumber'].')';
        }
    }
}
$exportArr[] = $exportRowArr;

// row 3 - Student UserLogin
$exportRowArr = $defaultECA_ColumnArr[2];
foreach((array)$ClassArr as $FormID => $FormClassArr)
{
    // skip invalid form
    if(!isset($FormArr[$FormID])) {
        continue;
    }
    
    foreach((array)$FormClassArr as $ClassID => $ClassInfoArr) {
        $ClassStudentArr = $StudentArr[$ClassID];
        foreach((array)$ClassStudentArr as $StudentID => $StudentInfoArr) {
            $exportRowArr[] = $StudentInfoArr['UserLogin'];
        }
    }
}
$exportArr[] = $exportRowArr;

### CSV Content
foreach((array)$ECA_CatItemArr as $ECA_CatID => $ECA_SubCatItemArr)
{
    // skip invalid ECA Category
    if(!$ECA_CatID || !isset($ECA_CatArr[$ECA_CatID])) {
        continue;
    }
    $ECA_CatInfo = $ECA_CatArr[$ECA_CatID];
    $ECA_CatCodeDisplay = $ECA_CatInfo['EcaCategoryCode'];
    $ECA_CatNameDisplay = $ECA_CatInfo['EcaCategoryNameEn'];
    
    foreach((array)$ECA_SubCatItemArr as $ECA_SubCatID => $ECA_ItemArr)
    {
        // skip invalid ECA Subcategory
        if(!$ECA_SubCatID || !isset($ECA_SubCatArr[$ECA_CatID][$ECA_SubCatID])) {
            continue;
        }
        $ECA_SubCatInfo = $ECA_SubCatArr[$ECA_CatID][$ECA_SubCatID];
        $ECA_SubCatCodeDisplay = $ECA_SubCatInfo['EcaSubCategoryCode'];
        $ECA_SubCatNameDisplay = $ECA_SubCatInfo['EcaSubCategoryNameEn'];
        
        foreach((array)$ECA_ItemArr as $ECA_ItemID => $ECA_ItemInfo)
        {
            // skip invalid ECA Item
            if(!$ECA_ItemID || !isset($ECA_CatItemArr[$ECA_CatID][$ECA_SubCatID][$ECA_ItemID])) {
                continue;
            }
            $ECA_ItemCodeDisplay = $ECA_ItemInfo['EcaItemCode'];
            $ECA_ItemNameDisplay = $ECA_ItemInfo['EcaItemNameEn'];
            
            $exportRowArr = array($ECA_CatCodeDisplay, $ECA_CatNameDisplay, $ECA_SubCatCodeDisplay, $ECA_SubCatNameDisplay, $ECA_ItemCodeDisplay, $ECA_ItemNameDisplay, '');
            
            foreach((array)$ClassArr as $FormID => $FormClassArr)
            {
                // skip invalid form
                if(!isset($FormArr[$FormID])) {
                    continue;
                }
                
                foreach((array)$FormClassArr as $ClassID => $ClassInfoArr) {
                    $ClassStudentArr = $StudentArr[$ClassID];
                    foreach((array)$ClassStudentArr as $StudentID => $StudentInfoArr) {
                        $isSelected = isset($Student_ECARecordArr[$ECA_ItemID][$StudentID]);
                        $exportRowArr[] = $isSelected? 'Y' : '';
                    }
                }
            }
            $exportArr[] = $exportRowArr;
        }
    }
}
*/

$lexport = new libexporttext();
$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportArr, array());

intranet_closedb();

# Output CSV
$lexport->EXPORT_FILE('eca_import.csv', $export_content);

?>