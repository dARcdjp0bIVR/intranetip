<?php
// Using:
/*
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ECAService";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

# Tag
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle']);

$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

echo $lreportcard_ui->Get_Management_ECA_Import_UI($ReportID, $ClassLevelID);
?>

<script type="text/JavaScript" language="JavaScript">
function checkForm()
{
	obj = document.form1;
	if (obj.elements["userfile"].value.Trim() == "")
	{
        alert('<?=$Lang['eRC_Rubrics']['GeneralArr']['jsWarningArr']['AlertSelectFile']?>');
        obj.elements["userfile"].focus();
        return false;
	}
	
	return true;
}

function jGEN_CSV_TEMPLATE()
{
	var ReportID = $("input#ReportID").val();
	var ClassLevelID = $("select#ClassLevelID").val();
	
	window.location.href = "export.php?ReportID=" + ReportID + "&ClassLevelID=" + ClassLevelID;
}
</script>
<br />

<?
print $linterface->FOCUS_ON_LOAD("form1.userfile");
$linterface->LAYOUT_STOP();
?>