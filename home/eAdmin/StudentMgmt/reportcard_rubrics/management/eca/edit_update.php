<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ECAService";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!isset($_POST) && (!isset($comment) || !isset($commentID)))
	header("Location:index.php");

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_eca = new libreportcardrubrics_eca();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$ReportID = $_REQUEST['ReportID'];
$ClassID = $_REQUEST['ClassID'];
$StudentItemArr = $_REQUEST['StudentItemArr'];

$lreportcard_eca->Start_Trans();

$Success["Delete"] = $lreportcard_eca->Delete_Report_Class_ECA_Record($ReportID,$ClassID);

if($Success["Delete"] && !empty($StudentItemArr))
	$Success["Insert"] = $lreportcard_eca->Insert_Student_ECA_Record($ReportID,$StudentItemArr);

if(in_array(false,$Success))
{
	$lreportcard_eca->RollBack_Trans();
	$Result = "update_failed";
}
else
{
	$lreportcard_eca->Commit_Trans();
	$Result = "update";
}

intranet_closedb();

header("Location:eca.php?ReportID=$ReportID&Result=$Result");
?>

