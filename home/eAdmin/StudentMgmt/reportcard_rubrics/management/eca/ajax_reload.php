<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard_ui = new libreportcardrubrics_ui();
# Get data

switch ($Action)
{
	case "ReloadFormTable":
		$ReportID = stripslashes($_REQUEST['ReportID']);
		echo $lreportcard_ui->Get_Management_ECA_Service_Table($ReportID);
	break;
	case "LoadItemTable":
		$ReportID = stripslashes($_REQUEST['ReportID']);
		$ClassID = stripslashes($_REQUEST['ClassID']);
		$EcaCategoryID = stripslashes($_REQUEST['EcaCategoryID']);
		$EcaSubCategoryID = stripslashes($_REQUEST['EcaSubCategoryID']); 
		echo $lreportcard_ui->Get_Management_ECA_Edit_Table($ReportID,$ClassID,$EcaCategoryID,$EcaSubCategoryID);
	break;
}
intranet_closedb();
?>