<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Management_ECAService";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Management_ECA_Service_UI($ReportID);
//echo $lreportcard_ui->Get_Setting_Module_UI();

?>
<script>
function aj_Reload_Form_Table()
{
	var ReportID = $("select#ReportID").val();
	if(!ReportID)
	{
		$("div#ECATableDiv").html("");
		return false;
	}
	
	Block_Element("ECATableDiv");	
	$.post(
		"ajax_reload.php",
		{
			ReportID : ReportID,
			Action : "ReloadFormTable"
		},
		function(ReturnData)
		{
			$("div#ECATableDiv").html(ReturnData);
			UnBlock_Element();
		}
		
	);
}

$().ready(function(){
	aj_Reload_Form_Table();
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>