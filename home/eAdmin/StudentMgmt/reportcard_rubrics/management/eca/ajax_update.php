<?php
// Using:
/*
 *  Date :  2019-03-21 (Bill)     [2018-1002-1146-08277]
 *          - Create file
 */

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CurriculumPool";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

$lreportcard_eca = new libreportcardrubrics_eca();
$lreportcard_eca->Has_Access_Right($PageRight, $CurrentPage);

$success = array();

# Get data
$action = stripslashes($_REQUEST['action']);
if ($action == 'Import_Student_ECA')
{
    $lreportcard_eca->Start_Trans();
    
    ### Include JS libraries
	$jsInclude = '';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery-1.3.2.min.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/jquery/jquery.blockUI.js"></script>';
	$jsInclude .= '<script type="text/javascript" src="'.$PATH_WRT_ROOT.'templates/script.js"></script>';
	echo $jsInclude;
	
	### Get import temp data
	$TEMP_IMPORT_TABLE = $lreportcard_eca->Get_Table_Name('TEMP_IMPORT_STUDENT_ECA');
	$sql = "SELECT * FROM $TEMP_IMPORT_TABLE WHERE ImportUserID = '".$_SESSION['UserID']."'";
	$ImportDataArr = $lreportcard_eca->returnArray($sql);
	//$numOfImportData = count($ImportDataArr);
	
	$ImportStudentIDArr = array_values(array_filter(array_unique(Get_Array_By_Key($ImportDataArr, "StudentID"))));
	$ImportItemIDArr = array_values(array_filter(array_unique(Get_Array_By_Key($ImportDataArr, "ItemID"))));
	
	### Get Class
	$ClassIDArr = Get_Array_By_Key($lreportcard_eca->Get_User_Accessible_Class(), "ClassID");
	
	### Delete old ECA Student data
	$success["Delete"] = true;
	foreach((array)$ClassIDArr as $thisClassID)
	{
	    $ClassStudentInfoArr = $lreportcard_eca->GET_STUDENT_BY_CLASS($thisClassID);
	    if(empty($ClassStudentInfoArr)) {
	        continue;
	    }
	    $ClassStudentIDArr = Get_Array_By_Key($ClassStudentInfoArr, "UserID");
	    $ClassStudentIDArr = array_intersect($ClassStudentIDArr, $ImportStudentIDArr);
	    
	    if(!empty($ClassStudentIDArr) && !empty($ImportItemIDArr)) {
    	    $result_delete = $lreportcard_eca->Delete_Report_Class_ECA_Record($ReportID, $thisClassID, $ClassStudentIDArr, $ImportItemIDArr);
    	    $success["Delete"] = $success["Delete"] && $result_delete;
	    }
	}
	
	### Insert ECA Student data
	$ImportDataArr = BuildMultiKeyAssoc((array)$ImportDataArr, array('isSelected', 'StudentID'), 'ItemID', 1, 1);
	$ImportDataArr = $ImportDataArr[1];
	if($success["Delete"] && !empty($ImportDataArr)) {
	    $success["Insert"] = $lreportcard_eca->Insert_Student_ECA_Record($ReportID, $ImportDataArr);
	}
	
	### Update Processing Display
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
	   $thisJSUpdate .= 'window.parent.document.getElementById("BlockUI_Processed_Number_Span").innerHTML = "'.$csvDataSize.'";';
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
	
	### Delete temp data
	$success['DeleteTempData'] = $lreportcard_eca->Delete_Import_Temp_Data();
	
	### Roll back data if import failed
	if (in_multi_array(false, $success))
	{
	    $lreportcard_eca->RollBack_Trans();
	    $ImportStatus = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportFail'];
	}
	else
	{
	    $lreportcard_eca->Commit_Trans();
	    $ImportStatus = $Lang['eRC_Rubrics']['ManagementArr']['ECAServiceArr']['ReturnMsgArr']['ImportSuccess'];
	}
	
	### Update Processing Display
	$thisJSUpdate = '';
	$thisJSUpdate .= '<script language="javascript">'."\n";
		$thisJSUpdate .= 'window.parent.document.getElementById("ImportStatusSpan").innerHTML = "'.$ImportStatus.'";';
		$thisJSUpdate .= 'window.parent.UnBlock_Document();'."\n";
	$thisJSUpdate .= '</script>'."\n";
	echo $thisJSUpdate;
}

intranet_closedb();
?>