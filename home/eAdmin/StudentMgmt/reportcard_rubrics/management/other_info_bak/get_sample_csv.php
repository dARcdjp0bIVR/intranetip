<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "TEACHER";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	if ($lreportcard->hasAccessRight()) {
		$otherInfoConfig = $lreportcard->getOtherInfoConfig($UploadType);
		
		if ($otherInfoConfig) {
			for($i=0; $i<sizeof($otherInfoConfig); $i++) {
				$fieldName[] = $otherInfoConfig[$i]["EnglishTitle"];
				$sampleData[0][] = $otherInfoConfig[$i]["ChineseTitle"];
				$sampleData[1][] = $otherInfoConfig[$i]["Sample1"];
				$sampleData[2][] = $otherInfoConfig[$i]["Sample2"];
				$sampleData[3][] = $otherInfoConfig[$i]["Sample3"];
			}
			$filename = $UploadType."_sample.csv";
			
			include_once($PATH_WRT_ROOT."includes/libexporttext.php");
			$lexport = new libexporttext();
			$exportContent = $lexport->GET_EXPORT_TXT($sampleData, $fieldName);
			intranet_closedb();
			$lexport->EXPORT_FILE($filename, $exportContent);
		} else {
			intranet_closedb();
			echo "Config CSV file have not been set properly";
		}
	} else {
?>
You have no priviledge to access this page.
<?
	}
} else {
?>
You have no priviledge to access this page.
<?
}
?>
