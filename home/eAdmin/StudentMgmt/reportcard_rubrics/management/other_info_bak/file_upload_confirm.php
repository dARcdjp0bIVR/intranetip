<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");


intranet_auth();
intranet_opendb();

if ($plugin['ReportCard2008']) {
	include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
	
	# Temp
	//$ReportCardCustomSchoolName = "st_stephen_college";
	
	if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
		include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
		$lreportcard = new libreportcardcustom();
	} else {
		$lreportcard = new libreportcard();
	}
	
	$linterface 	= new interface_html();
	
	$UploadType = $_REQUEST['UploadType'];
	$YearTermID = $_REQUEST['YearTermID'];
	$YearID = $_REQUEST['YearID'];
	$YearClassID = ($_REQUEST['YearClassID']=='')? 0 : $_REQUEST['YearClassID'];
	
	$TeachingClassArr = $lreportcard->GET_TEACHING_CLASS();
	$numOfTeachingClass = count($TeachingClassArr);
	$TeachingYearClassIDArr = array();
	for ($i=0; $i<$numOfTeachingClass; $i++)
		$TeachingYearClassIDArr[] = $TeachingClassArr[$i]['YearClassID'];
		
	$fs = new libfilesystem();
	$limport = new libimporttext();

	$UpdateFail = false;
	
	// check right
	if(!((($ck_ReportCard_UserType=="TEACHER")&&(in_array($YearClassID, $TeachingYearClassIDArr))) || ($ck_ReportCard_UserType=="ADMIN")))
		$UpdateFail = true;
	
	// check $userfile is empty 
	if(!$UpdateFail && !empty($userfile)) 
	{
		$loc = $userfile;
		$filename = $userfile_name;
	}
	else
		$UpdateFail = true;
				
	# check if file name is empty  
	if(!$UpdateFail && trim($filename)!='')
		$ext = $fs->file_ext($filename);
	else 
		$UpdateFail = true;
	
	# check if file ext valid and file exist
	if(!$UpdateFail && ($ext==".csv" || $ext=="txt")&& $loc!="none" && file_exists($loc)) 
		$data = $limport->GET_IMPORT_TXT($loc);
	else
		$UpdateFail = true;
	
	# check if data is empty 
	$data = $limport->GET_IMPORT_TXT($loc);
	if(!$UpdateFail && $data)
		$config = $lreportcard->getOtherInfoConfig($UploadType);
	else
		$UpdateFail = true;
	
	# check if config file is valid
	if(!$UpdateFail && !$config)
		$UpdateFail = true;
	
	if($UpdateFail)
	{
		header("Location: index.php?Result=update_failed&UploadType=$UploadType");
			exit;
	}
		
	# create folder if not exist
//			$fs = new libfilesystem();
	
	$success = 0;	
	for($i=0; $i<sizeof($config); $i++) {
		$fieldName[] = $config[$i]["EnglishTitle"];
	}
	$header_row = array_shift($data);
	$wrong_format = $fs->CHECK_CSV_FILE_FORMAT($header_row, $fieldName);
	
	// Validate the data according to the setting of config file
	// Empty fields will be accepted
	if($wrong_format)
	{
		header("Location: file_upload.php?Result=wrong_csv_header&UploadType=$UploadType");
		exit;
	}
	
	# Get a list of WebSAMSRegNo of all student in the class for validating REGNO
	if ($YearClassID=='')
		$StudentList = $lreportcard->GET_STUDENT_BY_CLASSLEVEL('', $YearID);
	else
		$StudentList = $lreportcard->GET_STUDENT_BY_CLASS($YearClassID);
		
	# $ClassNameNumberAssoc[$ClassName][$ClassNumber] = $UserID
	$ClassNameNumberAssoc = BuildMultiKeyAssoc($StudentList,array("ClassTitleEn","ClassNumber"),"UserID",1);
	
	# $UserLoginAssoc[$UserLogin] = $UserID
	$UserLoginAssoc = BuildMultiKeyAssoc($StudentList,array("UserLogin"),"UserID",1);
	
	# $WebSamsAssoc[$WebSAMSRegNo] = $UserID
	$WebSamsAssoc = BuildMultiKeyAssoc($StudentList,array("WebSAMSRegNo"),"UserID",1);
	
	$wrong_row = array();
	
	// skip $data[0] as $data[0] should be the chinese title
	for($i=1; $i<sizeof($data); $i++) 
	{
		
		list($ClassName, $ClassNumber, $UserLogin, $WebSamsRegNo) = $data[$i];
		
		if(trim($ClassName)!='' xor trim($ClassNumber)!='')
		{
			if(trim($ClassName)=='')
			{
				$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['EmptyClassName'];
				$WarnCss[$i][0] = true;
			}
			else if(trim($ClassNumber)=='')
			{
				$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['EmptyClassNumber'];
				$WarnCss[$i][1] = true;
			}
		}
		else if(trim($ClassName)!='' || trim($ClassNumber)!='')
		{
			if(!$thisStudentID = $ClassNameNumberAssoc[$ClassName][$ClassNumber])
			{
				$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['WrongClassNameNumber'];
				$WarnCss[$i][0] = true;
				$WarnCss[$i][1] = true;
			}
		}
		else if(trim($UserLogin)!='')
		{
			if(!$thisStudentID = $UserLoginAssoc[$UserLogin])
			{
				$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['WrongUserLogin'];
				$WarnCss[$i][2] = true;											
			}
		}
		else if(trim($WebSamsRegNo)!='')
		{
			if(!$thisStudentID = $WebSamsAssoc[$WebSamsRegNo])
			{
				$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['WrongWebSamsRegNo'];
				$WarnCss[$i][3] = true;											
			}
		}
		else
		{
			$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['EmptyStudentData'];
			$WarnCss[$i][0] = true;
			$WarnCss[$i][1] = true;
			$WarnCss[$i][2] = true;
			$WarnCss[$i][3] = true;											
		}
		
		$WrongDataFormat = false;
		#$data[$i][4] is Student Name for Reference only
		for($j=5; $j<sizeof($data[$i]); $j++) {
			
			if ($data[$i][$j] != "") {
				$DataLength = (int)$config[$j]["Length"];
				if ($DataLength != "" && strlen($data[$i][$j]) > $DataLength) {
					$WrongDataFormat = true;
					$WarnCss[$i][$j] = true;
				}
				
				$DataType = $config[$j]["Type"];
				if (strtoupper($DataType) == "NUM" && !is_numeric($data[$i][$j])) {
					$WrongDataFormat = true;
					$WarnCss[$i][$j] = true;
				}
				
				$gradeRegEx = '/^[a-zA-Z][+|-]?$/';
				if (strtoupper($DataType) == "GRADE" && !preg_match($gradeRegEx, $data[$i][$j])) {
					$WrongDataFormat = true;
					$WarnCss[$i][$j] = true;
				}
			}
		}
		if($WrongDataFormat)
			$WarnMsg[$i][] = $eReportCard['ImportWarningArr']['WrongDataFormat']; 
	}
	
	$NoOfFail = count($WarnMsg);
	$NoOfSuccess = sizeof($data)-1-$NoOfFail;
	
	if(count($WarnMsg)>0)
	{
		# build Table 
		$display .= '<table width="95%" border="0" cellpadding="5" cellspacing="0">'."\n";
			$display .= '<tr>'."\n";
				$display .= '<td class="tablebluetop tabletopnolink" width="1%">Row#</td>'."\n";
				$display .= '<td class="tablebluetop tabletopnolink">'.$i_ClassName.'</td>'."\n";
				$display .= '<td class="tablebluetop tabletopnolink">'.$i_ClassNumber.'</td>'."\n";
				$display .= '<td class="tablebluetop tabletopnolink">'.$i_UserLogin.'</td>'."\n";				
				$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['StudentRegistry']['WebSAMSRegNo'].'</td>'."\n";
				for($i=4;$i<sizeof($fieldName);$i++)
				{
					$display .= '<td class="tablebluetop tabletopnolink">'.$fieldName[$i].'</td>'."\n";
				}
				$display .= '<td class="tablebluetop tabletopnolink" width="30%">'.$Lang['General']['Remark'].'</td>'."\n";
			$display .= '</tr>'."\n";
		
			foreach($WarnMsg as $rowno => $Msg)
			{
				$rowcss = " class='".($errctr%2==0? "tablebluerow2":"tablebluerow1")."' ";
				
				$display .= '<tr '.$rowcss.'>'."\n";
					$display .= '<td class="tabletext">'.$rowno.'</td>'."\n";
					for($j=0; $j<sizeof($data[$rowno]); $j++)
					{
						$css = $WarnCss[$rowno][$j]?"red":"";
						$value = $WarnCss[$rowno][$j]&&empty($data[$rowno][$j])?"***":$data[$rowno][$j];
						$display .= '<td class="tabletext '.$css.'">'.$value.'</td>'."\n";
					}
					$display .= '<td class="tabletext">'.implode("<br>",$Msg).'</td>'."\n";
				$display .= '</tr>'."\n";
				
				$errctr++;
			}
		$display .= '</table>'."\n";
	
	}
	else // copy file to a tmp file
	{
		// create csv folder
		$folder_prefix = $intranet_root."/file/reportcard2008/".$lreportcard->schoolYear;
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
			
		// create tmp folder 	
		$folder_prefix .= "/tmp";
		if (!file_exists($folder_prefix))
			$fs->folder_new($folder_prefix);
			
		// create sub folder 
		$folder_prefix .= "/".$UploadType;
		$fs->folder_new($folder_prefix);	
		
		if(strpos($filename, ".")!=0) {
			$filename = $YearTermID."_".$YearID."_".$YearClassID."_unicode.csv";
			$filepath = $folder_prefix."/".$filename;
			$success = $fs->file_copy($loc, stripslashes($filepath));
			
		}
	}

	$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back");
	$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2");
	
	$Btn .= empty($WarnMsg)?$NextBtn."&nbsp;":"";
	$Btn .= $BackBtn;
						
	# Step Obj 
	$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
	$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
	
	# tag information
	$TAGS_OBJ =array();
	$TAGS_OBJ = $lreportcard->getOtherInfoTabObjArr($UploadType);

	# page navigation (leave the array empty if no need)
	$PAGE_NAVIGATION[] = array($button_upload, "");
	
	$CurrentPage = "Management_OtherInfo";
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	$linterface->LAYOUT_START();
							
?>
<form id="form1" name="form1" enctype="multipart/form-data" action="file_upload_update.php" method="post">
<br />
<table id="html_body_frame" width="96%" border="0" cellspacing="0" cellpadding="5">
<tr><td align="right" colspan="<?=$col_size?>"><?= $SysMsg ?></td></tr>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td>
		<table width="30%">
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
				<td class='tabletext'><?=$NoOfSuccess?></td>
			</tr>
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
				<td class='tabletext <?=$NoOfFail>0?"red":""?>'><?=$NoOfFail?></td>
			</tr>
		</table>
	</td>
</tr>'<tr>
	<td>
		<?=$display?>
	</td>
</tr>
<tr>
	<td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$Btn?></td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="UploadType" value="<?=$UploadType?>" />
<input type="hidden" name="filepath" value="<?=$filepath?>" />
<input type="hidden" name="YearID" value="<?=$YearID?>" />
<input type="hidden" name="YearTermID" value="<?=$YearTermID?>" />
<input type="hidden" name="YearClassID" value="<?=$YearClassID?>" />

</form>
<br><br>
<?
$linterface->LAYOUT_STOP();
											

}  else {
	echo "You have no priviledge to access this page.";
}

?>