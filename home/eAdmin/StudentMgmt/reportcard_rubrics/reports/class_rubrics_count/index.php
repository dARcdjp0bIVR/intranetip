<?php
// using:
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_ClassAverageRubricsCount";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ReportsArr']['ClassAverageRubricsCountArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Statistics_General_Setting_UI();

?>
<script>
function js_Changed_Form_Stage_Selection() {
	js_Reload_Form_Selection();
}

function js_Changed_Form_Selection() {
	js_Reload_Class_Selection();
}

function js_Changed_Class_Selection() {
	js_Reload_Student_Selection();
}

function js_Changed_Student_Year_Term_Selection() {
	js_Reload_Student_Subject_Group_Selection();
}

function js_Changed_Student_Subject_Selection() {
	js_Reload_Student_Subject_Group_Selection();
}

function js_Changed_Student_Subject_Group_Selection() {
	js_Reload_Student_Selection();
}

function js_Changed_Student_Extra_Info_Selection() {
	js_Reload_Student_Selection();
}

function js_Reload_Form_Selection()
{
	var jsFormStageIDList = Get_Selection_Value('FormStageIDArr[]' , 'String');
	
	$('span#FormSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Form_Selection',
			SelectionID: 'YearIDArr[]',
			OnChange: 'js_Changed_Form_Selection();',
			FormStageID: jsFormStageIDList,
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('YearIDArr[]', 1);
			js_Reload_Class_Selection();
		}
	);
}

function js_Reload_Class_Selection()
{
	var jsYearIDList = Get_Selection_Value('YearIDArr[]', 'String');
	
	$('span#ClassSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Selection',
			YearID: jsYearIDList,
			SelectionID: 'YearClassIDArr[]',
			OnChange: 'js_Changed_Class_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('YearClassIDArr[]', 1);
			js_Reload_Student_Selection();
		}
	);
}

function js_Reload_Student_Year_Term_Selection()
{
	$('span#StudentYearTermSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Term_Selection',
			ReportID: '',
			SelectionID: 'StudentYearTermIDArr[]',
			OnChange: 'js_Changed_Student_Year_Term_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('StudentYearTermIDArr[]', 1);
			js_Reload_Student_Subject_Group_Selection();
		}
	);
}

function js_Reload_Student_Subject_Group_Selection()
{
	var jsSubjectIDList = Get_Selection_Value('StudentSubjectIDArr[]', 'String');
	var jsYearTermIDList = Get_Selection_Value('StudentYearTermIDArr[]', 'String');
	
	$('span#StudentSubjectGroupSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Subject_Group_Selection',
			SubjectID: jsSubjectIDList,
			YearTermID: jsYearTermIDList,
			SelectionID: 'StudentSubjectGroupIDArr[]',
			OnChange: 'js_Changed_Student_Subject_Group_Selection();',
			IsMultiple: 1,
			NoFirst: 1,
			DisplayTermInfo: 1
		},
		function(ReturnData)
		{
			js_Select_All('StudentSubjectGroupIDArr[]', 1);
			js_Reload_Student_Selection();
		}
	);
}

function js_Reload_Student_Selection()
{
	var jsSelectStudentSource = $("input[name='SelectStudentFrom']:checked").val();
	var jsExtraInfoList = Get_Selection_Value('ExtraItemID[]', 'String');
	
	var jsYearClassIDList = '';
	var jsSubjectGroupIDList = '';
	if (jsSelectStudentSource == 'Class') {
		jsYearClassIDList = Get_Selection_Value('YearClassIDArr[]', 'String');
	}
	else if (jsSelectStudentSource == 'SubjectGroup') {
		jsSubjectGroupIDList = Get_Selection_Value('StudentSubjectGroupIDArr[]', 'String');
	}
					
	$('span#StudentSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Student_Selection',
			YearClassIDList: jsYearClassIDList,
			SubjectGroupIDList: jsSubjectGroupIDList,
			ExtraInfoList: jsExtraInfoList,
			SelectionID: 'StudentIDArr[]',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			js_Select_All('StudentIDArr[]', 1);
		}
	);
}

function js_Changed_Student_Selection_Source(jsTargetSource)
{
	if (jsTargetSource == 'Class') {
		$('tr.Student_FormClassTr').show();
		$('tr.Student_SubjectGroupTr').hide();
	}
	else if (jsTargetSource == 'SubjectGroup') {
		$('tr.Student_FormClassTr').hide();
		$('tr.Student_SubjectGroupTr').show();
	}
	
	js_Reload_Student_Selection();
}

function js_Check_Form()
{
	var jsValid = true;
	
	// Check selected Subject
	if ($('select#SubjectIDArr\\[\\]').val() == null)
	{
		$('div#SelectSubjectWarningDiv').show();
		$('select#SubjectIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectSubjectWarningDiv').hide();
	}
	
	// Check selected Student
	if ($('select#StudentIDArr\\[\\]').val() == null)
	{
		$('div#SelectStudentWarningDiv').show();
		$('select#StudentIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectStudentWarningDiv').hide();
	}
	
	// Check selected Class
	if ($('select#YearClassIDArr\\[\\]').val() == null)
	{
		$('div#SelectClassWarningDiv').show();
		$('select#YearClassIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectClassWarningDiv').hide();
	}
	
	// Check selected Module
	if ($('select#ModuleModuleID').val() == null)
	{
		$('div#ModuleIDWarningDiv').show();
		$('select#ModuleModuleID').focus();
		jsValid = false;
	}
	else
	{
		$('div#ModuleIDWarningDiv').hide();
	}	
	
	if (jsValid == true)
		document.getElementById('form1').submit();
}

$().ready(function (){
	js_Select_All("FormStageIDArr[]", 1);
	js_Select_All_With_OptGroup("StudentSubjectIDArr[]", 1);
	js_Select_All_With_OptGroup("ExtraItemID[]", 1);
	js_Select_All_With_OptGroup("SubjectIDArr[]", 1);
	js_Select_All('ModuleModuleID', 1);
	
	js_Reload_Student_Year_Term_Selection();
	js_Changed_Student_Selection_Source('Class');
	js_Changed_Form_Stage_Selection();
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>