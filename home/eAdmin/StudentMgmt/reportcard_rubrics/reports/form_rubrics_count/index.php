<?php
// using:
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_FormAverageRubricsCount";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ReportsArr']['FormAverageRubricsCountArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Reports_Form_Rubrics_Count_Setting_UI();

?>
<script>


function js_Check_Form()
{
	var jsValid = true;
	
	// Check selected Subject
	if ($('select#SubjectIDArr\\[\\]').val() == null)
	{
		$('div#SelectSubjectWarningDiv').show();
		$('select#SubjectIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectSubjectWarningDiv').hide();
	}
	
	// Check selected Year
	if ($('select#YearIDArr\\[\\]').val() == null)
	{
		$('div#SelectYearIDWarningDiv').show();
		$('select#YearIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectYearIDWarningDiv').hide();
	}
		
	// Check selected Module
	if ($('select#ModuleModuleID').val() == null)
	{
		$('div#ModuleIDWarningDiv').show();
		$('select#ModuleModuleID').focus();
		jsValid = false;
	}
	else
	{
		$('div#ModuleIDWarningDiv').hide();
	}	
	
	if (jsValid == true)
		document.getElementById('form1').submit();
}

$().ready(function (){
	js_Select_All("YearIDArr[]", 1);
	js_Select_All('ModuleModuleID',1);
	js_Select_All_With_OptGroup('SubjectIDArr[]',1)
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>