<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_SubjectTaughtReport";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
//include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");

//$lreportcard = new libreportcardrubrics_custom();
$lreportcard = new libreportcardrubrics();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));

### Print Button
$PrintBtn = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

$TargetLevel = 3;
$Statistic = $lreportcard->Get_Topic_Mark_Statistics($StudentIDArr,$ModuleID,$SubjectIDArr, $TargetLevel);

$TopicInfoArr = $lreportcard_topic->Get_Topic_Info($SubjectIDArr,'',$TargetLevel);
$TopicInfoArr = BuildMultiKeyAssoc($TopicInfoArr, array("SubjectID","TopicID"));

// loop all students selected
foreach($StudentIDArr as $StudentID)
{
	$SubjectTopicArr = $Statistic[$StudentID];
	
	# count Topics in subjects 
	foreach($TopicInfoArr as $SubjectID => $TopicInfo)
	{
		$InvlovedSubject[] = $SubjectID;
		$thisTotal = count($TopicInfo);
		// only taught subject will be included in $SubjectTopicArr
		$SubjectTaughtArr[$StudentID][$SubjectID]['Taught'] = count($SubjectTopicArr[$SubjectID]);
		$SubjectTaughtArr[$StudentID][$SubjectID]['Total'] = $thisTotal;
	}
}

$InvlovedSubject = array_unique($InvlovedSubject);

$SubjectTaughtReport = $lreportcard_ui->Get_Reports_Subject_Taught_Report($SubjectTaughtArr, $InvlovedSubject,$SubjectDisplay);

?>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>
<table border="0" cellpadding="4" width="95%" cellspacing="0">
	<tr><td><?=$SubjectTaughtReport?></td></tr>
</table>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>