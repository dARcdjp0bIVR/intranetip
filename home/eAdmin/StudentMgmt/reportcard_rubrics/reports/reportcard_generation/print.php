<?php
// using: Ivan
@SET_TIME_LIMIT(21600);
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_ReportCardGeneration";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_eca = new libreportcardrubrics_eca();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));
echo $lreportcard_ui->Include_Template_CSS();


### Get Data
$ReportID = $_REQUEST['ReportID'];
$YearIDArr = $_REQUEST['YearIDArr'];
$StudentIDArr = $_REQUEST['StudentIDArr'];

$Global_DisplaySettingsArr['YearIDArr'] = $YearIDArr;

### Prepare Report Data (so that no need to load the data for each student)
# Subject
$Global_DisplaySettingsArr['SubjectIDArr'] = $_REQUEST['SubjectIDArr'];
$Global_DisplaySettingsArr['SubjectDisplay'] = $_REQUEST['SubjectDisplay'];
$Global_DisplaySettingsArr['SubjectGroupDisplay'] = $_REQUEST['SubjectGroupDisplay'];
$Global_DisplaySettingsArr['TopicLevelArr'] = $_REQUEST['TopicLevelArr'];
//$Global_DisplaySettingsArr['TopicLevelArr'][] = $eRC_Rubrics_ConfigArr['MaxLevel'];	// add back the last topic level for the display array
$Global_DisplaySettingsArr['TopicNeedNotStudyDisplay'] = $_REQUEST['TopicNeedNotStudyDisplay'];
$Global_DisplaySettingsArr['MarksheetRemarksDisplay'] = $_REQUEST['MarksheetRemarksDisplay'];

# Mark
$Global_DisplaySettingsArr['MarkDisplayOption'] = $_REQUEST['MarkDisplayOption'];


# Cover Page
$CoverTitleArr = array_remove_empty($CoverTitleArr);
$numOfTitle = count($CoverTitleArr);
for ($i=0; $i<$numOfTitle; $i++)
	$CoverTitleArr[$i] = stripslashes($CoverTitleArr[$i]);
$Global_DisplaySettingsArr['CoverTitleArr'] = $CoverTitleArr;

$Global_DisplaySettingsArr['DisplayCoverPage'] = $_REQUEST['DisplayCoverPage'];
$Global_DisplaySettingsArr['DisplayStudentPhotoFrame'] = $_REQUEST['DisplayStudentPhotoFrame'];


# Report
$Global_DisplaySettingsArr['ReportType'] = $_REQUEST['ReportType'];
$Global_DisplaySettingsArr['SchoolNameDisplay'] = $_REQUEST['SchoolNameDisplay'];

$ReportTitleArr = array_remove_empty($ReportTitleArr);
$numOfTitle = count($ReportTitleArr);
for ($i=0; $i<$numOfTitle; $i++)
	$ReportTitleArr[$i] = stripslashes($ReportTitleArr[$i]);
$Global_DisplaySettingsArr['ReportTitleArr'] = $ReportTitleArr;

$Global_DisplaySettingsArr['HeaderDataFieldArr'] = $_REQUEST['HeaderDataFieldArr'];
$Global_DisplaySettingsArr['NumOfHeaderDataPerRow'] = $_REQUEST['NumOfHeaderDataPerRow'];
$Global_DisplaySettingsArr['ECACategoryIDArr'] = $_REQUEST['ECACategoryIDArr'];
$Global_DisplaySettingsArr['OtherInfoItemIDArr'] = $_REQUEST['OtherInfoItemIDArr'];
$Global_DisplaySettingsArr['DisplayClassTeacherComment'] = $_REQUEST['DisplayClassTeacherComment'];
$Global_DisplaySettingsArr['SignatureFieldArr'] = $_REQUEST['SignatureFieldArr'];



# Get Student Info
$Global_StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($YearIDArr, $ParClassID="", $StudentIDArr, $ReturnAsso=1, $isShowStyle=0, $CheckActiveStudent=0);

# Report Info
$Global_ReportInfoArr['BasicInfo'] = $lreportcard->GetReportTemplateInfo($ReportID);
$Global_ReportInfoArr['ModuleInfo'] = $lreportcard->GetReportTemplateModuleInfo($ReportID);

# Subject Info
// $Global_SubjectInfoArr[$ParentSubjectID] = Info Array
// $Global_SubjectInfoArr[$ParentSubjectID]['CmpSubjectInfoArr'][$CmpSubjectID] = Info Array
$Global_SubjectInfoArr = $lreportcard->Get_Subject_Info();

// $Global_SubjectRubricsInfoArr[$YearID][$SubjectID][$RubricsID][$RubricsItemID] = RubricsItemInfoArr
$Global_SubjectRubricsInfoArr = $lreportcard->Get_Subject_Rubrics_Mapping($Global_DisplaySettingsArr['SubjectIDArr'], $YearID);

# Topic Info
$Global_TopicInfoArr = $lreportcard_topic->Get_Topic_Info();

# Get Subject Topic Tree
$Global_SubjectTopicTreeArr = $lreportcard_topic->Get_Topic_Tree($Global_DisplaySettingsArr['SubjectIDArr']);

# Student Mark
//$Global_StudentMarksheetScoreArr[$StudentID][$SubjectID][$TopicID][$ModuleID] = $MarksheetScoreArr[$i];
$Global_StudentMarksheetScoreArr = $lreportcard->Get_Student_Marksheet_Score($Global_DisplaySettingsArr['SubjectIDArr'], $StudentIDArr);

# Student Mark Statistics
// $Global_TopicStatisticsArr[$StudentID][$SubjectID][$TopicID]['NumOfStudiedModule'] = value;
// $Global_TopicStatisticsArr[$StudentID][$SubjectID][$TopicID]['MarkInfoArr'][$RubricsItemID]['Count'] = value;
$ReportModuleIDArr = Get_Array_By_Key($Global_ReportInfoArr['ModuleInfo'], 'ModuleID');
$Global_TopicStatisticsArr = $lreportcard->Get_Topic_Mark_Statistics($StudentIDArr, $ReportModuleIDArr, $Global_DisplaySettingsArr['SubjectIDArr']);
//$Global_TopicStatisticsArr = $lreportcard->Get_Topic_Mark_Statistics(array(1971), array(2,3,4,5,6,7), array(1));


# ECA Category Info
# Get Cat info
$Global_EcaCategoryInfoArr = $lreportcard_eca->Get_ECA_Category_Info();
$Global_EcaCategoryInfoArr = BuildMultiKeyAssoc($Global_EcaCategoryInfoArr, array("EcaCategoryID"));

# Get SubCat Info
$Global_EcaSubCategoryInfoArr = $lreportcard_eca->Get_ECA_SubCategory_Info();
$Global_EcaSubCategoryInfoArr = BuildMultiKeyAssoc($Global_EcaSubCategoryInfoArr, array("EcaCategoryID", "EcaSubCategoryID"));

# Get Cat, Subcat, Item Mapping (with Item Info)
$RawECACatItemList = $lreportcard_eca->Get_ECA_Category_Item_Info('', '', '', 1);
$Global_EcaCatSubCatItemMappingArr = BuildMultiKeyAssoc($RawECACatItemList, array("EcaCategoryID", "EcaSubCategoryID", "EcaItemID"));

# Get Exist Student Item Mapping
$StudentEcaItemArr = $lreportcard_eca->Get_Report_Student_ECA_Record($ReportID, '', $StudentIDArr);
$Global_StudentEcaMappingArr = BuildMultiKeyAssoc($StudentEcaItemArr , array("StudentID", "EcaItemID"), "EcaItemID", 1);


# Student Other Info
// Category Info
$Global_OtherInfoCategoryArr = $lreportcard->Get_Other_Info_Category();
$Global_OtherInfoCategoryArr = BuildMultiKeyAssoc($Global_OtherInfoCategoryArr, array("CategoryID"));
// Item Info
$Global_OtherInfoItemArr = $lreportcard->Get_OtherInfo_Item_Info();
$Global_OtherInfoItemArr = BuildMultiKeyAssoc($Global_OtherInfoItemArr, array("ItemID"));
// Get Student Other Info Data
$Global_StudentOtherInfoArr = $lreportcard->Get_Student_OtherInfo_Record($ReportID, '', '', '', $StudentIDArr);
$Global_StudentOtherInfoArr = BuildMultiKeyAssoc($Global_StudentOtherInfoArr, array("StudentID", "CategoryID", "ItemID", "RecordID"));


# Student Class Teacher Comment
$Global_StudentClassTeacherCommentArr = $lreportcard->Get_Student_Class_Teacher_Comment($ReportID, $StudentIDArr, 0);
$Global_StudentClassTeacherCommentArr = BuildMultiKeyAssoc($Global_StudentClassTeacherCommentArr, array("StudentID"));



### Print Button
$PrintBtn = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");


?>

<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>


<div style="text-align:center;vertical-align:top;width:100%">
	<?
	$numOfStudent = count($StudentIDArr);
	for($i=0; $i<$numOfStudent; $i++)
	{
		$thisStudentID = $StudentIDArr[$i];
		
		if ($i == $numOfStudent-1)
		{
			// last one
			$thisPageBreakStyle = '';
		}
		else
		{
			$thisPageBreakStyle = 'style="page-break-after:always"';
		}
		
		$x = '';
		$x .= '<div class="container" '.$thisPageBreakStyle.'>'."\n";
			$x .= $lreportcard->Get_Student_Report_Card($ReportID, $thisStudentID);
		$x .= '</div>'."\n";
		
		echo $x;
	}
	?>
</div>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>