<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_ReportCardGeneration";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Add_Settings_Record')
{
	$DataArr = array();
	
	$DataArr['SettingsCode'] 			= trim(urldecode(stripslashes($_POST['Code'])));
	$DataArr['SettingsNameEn']			= trim(urldecode(stripslashes($_POST['NameEn'])));
	$DataArr['SettingsNameCh'] 			= trim(urldecode(stripslashes($_POST['NameCh'])));
	$DataArr['DisplayOrder'] 	= $lreportcard->Get_Report_Template_Settings_Max_Display_Order() + 1;
	
	$SettingsID = $lreportcard->Insert_Report_Template_Settings($DataArr);
	$ReturnString = $SettingsID;
}
else if ($Action == 'Save_Settings')
{
	$SettingsID = $_REQUEST['SettingsID'];
	
	$DataArr = array();
	$DataArr['SettingsValue'] = serialize($_POST);
	$Success = $lreportcard->Update_Report_Template_Settings($SettingsID, $DataArr, $UpdateLastModified=1);	
	
	$ReturnString = ($Success)? '1' : '0';
}
else if ($Action == 'Delete_Settings')
{
	$SettingsID = $_POST['SettingsID'];
	$Success = $lreportcard->Delete_Report_Template_Settings($SettingsID);
	$ReturnString = ($Success)? '1' : '0';
}
else if ($Action == 'Reorder_Settings')
{
	$DisplayOrderString = $_REQUEST['DisplayOrderString'];
	$DisplayOrderSettingsIDArr = explode(',', $DisplayOrderString);
	$numOfSettings = count($DisplayOrderSettingsIDArr);
	$DisplayOrderCount = 1;
	
	$SuccessArr = array();
	for ($i=0; $i<$numOfSettings; $i++)
	{
		$thisSettingsID = $DisplayOrderSettingsIDArr[$i];
		
		if ($thisSettingsID == '' || $thisSettingsID == 0)
			continue;
			
		$DataArr = array();
		$DataArr['DisplayOrder'] = $DisplayOrderCount++;
		$SuccessArr[$thisSettingsID] = $lreportcard->Update_Report_Template_Settings($thisSettingsID, $DataArr, $UpdateLastModified=0);
	}
	
	$ReturnString = (in_array(false, $SuccessArr))? '0' : '1';
}
else if ($Action == 'Edit_Settings_Field')
{
	$SettingsID = $_REQUEST['SettingsID'];
	$DBNameField = trim(urldecode(stripslashes($_REQUEST['DBNameField'])));
	$Value = trim(urldecode(stripslashes($_REQUEST['Value'])));
	
	$DataArr = array();
	$DataArr[$DBNameField] = $Value;
	$Success = $lreportcard->Update_Report_Template_Settings($SettingsID, $DataArr, $UpdateLastModified=1);
	
	$ReturnString = ($Success)? '1' : '0';
}

echo $ReturnString;

intranet_closedb();
?>