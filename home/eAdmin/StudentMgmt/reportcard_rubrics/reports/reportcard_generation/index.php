<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_ReportCardGeneration";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();


############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['MenuTitle']);
$linterface->LAYOUT_START();


$SettingsID = $_REQUEST['SettingsID'];
echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Reports_ReportGeneration_UI($SettingsID);

$btnAdd = str_replace("'", "\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Save_New_Settings();", "NewSubmitBtn"));
$btnCancel = str_replace("'", "\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Settings_Row();", "NewCancelBtn"));

if ($SettingsID != '')
{
	$SettingsInfoArr = $lreportcard->Get_Report_Template_Settings($SettingsID);
	$SettingsValue = $SettingsInfoArr[0]['SettingsValue'];
	$SettingsArr = unserialize($SettingsValue);
	
	### Prepare Arrays for js initialization
	$FormStageIDArr = $SettingsArr['FormStageIDArr'];
	echo ConvertToJSArray($FormStageIDArr, 'jsPresetFormStageIDArr');
	
	$YearIDArr = $SettingsArr['YearIDArr'];
	echo ConvertToJSArray($YearIDArr, 'jsPresetYearIDArr');
	
	$YearClassIDArr = $SettingsArr['YearClassIDArr'];
	echo ConvertToJSArray($YearClassIDArr, 'jsPresetYearClassIDArr');
	
	$StudentYearTermIDArr = $SettingsArr['StudentYearTermIDArr'];
	echo ConvertToJSArray($StudentYearTermIDArr, 'jsPresetStudentYearTermIDArr');
	
	$StudentSubjectIDArr = $SettingsArr['StudentSubjectIDArr'];
	echo ConvertToJSArray($StudentSubjectIDArr, 'jsPresetStudentSubjectIDArr');
	
	$StudentSubjectGroupIDArr = $SettingsArr['StudentSubjectGroupIDArr'];
	echo ConvertToJSArray($StudentSubjectGroupIDArr, 'jsPresetStudentSubjectGroupIDArr');
	
	$ExtraItemIDArr = $SettingsArr['ExtraItemIDArr'];
	echo ConvertToJSArray($ExtraItemIDArr, 'jsPresetExtraItemIDArr');
	
	
	
	$StudentIDArr = $SettingsArr['StudentIDArr'];
	echo ConvertToJSArray($StudentIDArr, 'jsPresetStudentIDArr');
	
	$SubjectIDArr = $SettingsArr['SubjectIDArr'];
	echo ConvertToJSArray($SubjectIDArr, 'jsPresetSubjectIDArr');
	
	$ReportTitleArr = $SettingsArr['CoverTitleArr'];
	echo ConvertToJSArray($ReportTitleArr, 'jsPresetCoverTitleArr');
	
	$ReportTitleArr = $SettingsArr['ReportTitleArr'];
	echo ConvertToJSArray($ReportTitleArr, 'jsPresetReportTitleArr');
}

?>

<script language="javascript">
var jsLastReportTitleSelected;
var jsLastCoverTitleSelected;
var jsGlobalSettingsID = '<?=$SettingsID?>';
var jsMaxTopicLevel = '<?=$eRC_Rubrics_ConfigArr['MaxLevel']?>';
var jsCurReportType;
var jsPresetSelectStudentFrom = '<?=$SettingsArr['SelectStudentFrom']?>';

$(document).ready( function() {	
	// Apply default Report Type Selection Display
	jsLastReportTitleSelected = Trim($("select#ReportID option:selected").text());
	js_Changed_Report_Selection($('select#ReportID').val(), 1);
	
	
	// Initialize the Selections
	if (jsGlobalSettingsID == '') {
		js_Select_All("FormStageIDArr[]", 1);
		js_Select_All_With_OptGroup("StudentSubjectIDArr[]", 1);
		js_Select_All_With_OptGroup("ExtraItemIDArr[]", 1);
		js_Select_All_With_OptGroup("SubjectIDArr[]", 1);
	}
	else {
		js_Preset_Selection('FormStageIDArr[]', jsPresetFormStageIDArr);
		js_Preset_Selection('StudentSubjectIDArr[]', jsPresetStudentSubjectIDArr, 1);
		js_Preset_Selection('ExtraItemIDArr[]', jsPresetExtraItemIDArr, 1);
		js_Preset_Selection('SubjectIDArr[]', jsPresetSubjectIDArr, 1);
	}
	js_Reload_Form_Selection(1);
	
	
	// Initialize the Student Selection Source
	if (jsPresetSelectStudentFrom == '') {
		js_Changed_Student_Selection_Source('Class', 1);
	}
	else {
		js_Changed_Student_Selection_Source(jsPresetSelectStudentFrom, 1);
	}
		
		
	
	// Report Display Title
	if (jsGlobalSettingsID != '')
	{
		// Cover
		var i;
		for (i=0; i<jsPresetCoverTitleArr.length - 1; i++)
		{
			js_Add_More_Title_Row('Cover');
		}
		
		var jsThisIndex;
		for (i=0; i<jsPresetCoverTitleArr.length; i++)
		{
			jsThisIndex = i + 1;
			$('input#CoverTitle_' + jsThisIndex).val(jsPresetCoverTitleArr[i]);
		}
		
		
		// Report
		var i;
		for (i=0; i<jsPresetReportTitleArr.length - 1; i++)
		{
			js_Add_More_Title_Row('Report');
		}
		
		var jsThisIndex;
		for (i=0; i<jsPresetReportTitleArr.length; i++)
		{
			jsThisIndex = i + 1;
			$('input#ReportTitle_' + jsThisIndex).val(jsPresetReportTitleArr[i]);
		}
	}
	
	
	
	// Initialize the Sortable Table Options
	$("ul.sortable").sortable({
		placeholder: 'placeholder',
		update: function(event, ui) {
			var jsListID = $(this).attr('id');
			var jsResult = $(this).sortable('toArray').toString();
			
			if (jsListID == 'HeaderDataFieldSortable')
				$('input#HeaderDataFieldOrderedList').val(jsResult);
			else if (jsListID == 'SignatureFieldSortable')
				$('input#SignatureFieldOrderedList').val(jsResult);
			else
			{
				var jsListIDArr = jsListID.split('_');
				var jsListType = jsListIDArr[0];
				var jsListTypeID = jsListIDArr[1];
				
				if (jsListType == 'OtherInfoCategory')
				{
					$('input#OtherInfoItemOrderedList_' + jsListTypeID).val(jsResult);
				}
			}
		}
	});
	$("ul.sortable").disableSelection();
	
	js_Init_DND_Table();
});

function js_Changed_Report_Selection(jsReportID, jsInitial)
{
	jsInitial = jsInitial || false;

	js_Update_Title_Input();
	js_Reload_Student_Year_Term_Selection(jsInitial);
	
	$.post(
		"../../ajax_validate.php",
		{
			Action: 'ReportCard_Type',
			ReportID : jsReportID
		},
		function(jsReportType)
		{
			if (jsReportType == 'T')
			{
				// Term Report
				jsCurReportType = 'T';
				$('div#MarkOptionDiv').hide();
				
				$('input.MarksheetRemarksDisplay').attr('disabled', '');
			}
			else if (jsReportType == 'W')
			{
				// Consolidate Report
				jsCurReportType = 'W';
				$('div#MarkOptionDiv').show();
				
				$('input.MarksheetRemarksDisplay').attr('disabled', 'disabled');
			}
		}
	);
}

function js_Changed_Form_Stage_Selection() {
	js_Reload_Form_Selection();
}

function js_Changed_Form_Selection() {
	js_Reload_Class_Selection();
}

function js_Changed_Class_Selection() {
	js_Reload_Student_Selection();
}

function js_Changed_Student_Year_Term_Selection() {
	js_Reload_Student_Subject_Group_Selection();
}

function js_Changed_Student_Subject_Selection() {
	js_Reload_Student_Subject_Group_Selection();
}

function js_Changed_Student_Subject_Group_Selection() {
	js_Reload_Student_Selection();
}

function js_Changed_Student_Extra_Info_Selection() {
	js_Reload_Student_Selection();
}




function js_Reload_Form_Selection(jsInitial)
{
	var jsFormStageIDList = Get_Selection_Value('FormStageIDArr[]' , 'String');
	
	$('span#FormSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Form_Selection',
			SelectionID: 'YearIDArr[]',
			OnChange: 'js_Changed_Form_Selection();',
			FormStageID: jsFormStageIDList,
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			// Load preset if initialize and sepecfied the settings
			if (jsInitial == 1 && jsGlobalSettingsID != '')
			{
				js_Preset_Selection('YearIDArr[]', jsPresetYearIDArr);
			}
			else
			{
				js_Select_All('YearIDArr[]', 1);
			}
				
			js_Reload_Class_Selection(jsInitial);
		}
	);
}

function js_Reload_Class_Selection(jsInitial)
{
	var jsYearIDList = Get_Selection_Value('YearIDArr[]', 'String');
	
	$('span#ClassSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Selection',
			YearID: jsYearIDList,
			SelectionID: 'YearClassIDArr[]',
			OnChange: 'js_Changed_Class_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			// Load preset if initialize and sepecfied the settings
			if (jsInitial == 1 && jsGlobalSettingsID != '')
			{
				js_Preset_Selection('YearClassIDArr[]', jsPresetYearClassIDArr, 1);
			}
			else
			{
				js_Select_All('YearClassIDArr[]', 1);
			}
				
			js_Reload_Student_Selection(jsInitial);
		}
	);
}

function js_Reload_Student_Year_Term_Selection(jsInitial)
{
	var jsReportID = $('select#ReportID').val();
	
	$('span#StudentYearTermSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Term_Selection',
			ReportID: jsReportID,
			SelectionID: 'StudentYearTermIDArr[]',
			OnChange: 'js_Changed_Student_Year_Term_Selection();',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			// Load preset if initialize and sepecfied the settings
			if (jsInitial == 1 && jsGlobalSettingsID != '') {
				js_Preset_Selection('StudentYearTermIDArr[]', jsPresetStudentYearTermIDArr);
			}
			else {
				js_Select_All('StudentYearTermIDArr[]', 1);
			}
				
			js_Reload_Student_Subject_Group_Selection(jsInitial);
		}
	);
}

function js_Reload_Student_Subject_Group_Selection(jsInitial)
{
	var jsSubjectIDList = Get_Selection_Value('StudentSubjectIDArr[]', 'String');
	var jsYearTermIDList = Get_Selection_Value('StudentYearTermIDArr[]', 'String');
	
	$('span#StudentSubjectGroupSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Subject_Group_Selection',
			SubjectID: jsSubjectIDList,
			YearTermID: jsYearTermIDList,
			SelectionID: 'StudentSubjectGroupIDArr[]',
			OnChange: 'js_Changed_Student_Subject_Group_Selection();',
			IsMultiple: 1,
			NoFirst: 1,
			DisplayTermInfo: 1
		},
		function(ReturnData)
		{
			// Load preset if initialize and sepecfied the settings
			if (jsInitial == 1 && jsGlobalSettingsID != '') {
				js_Preset_Selection('StudentSubjectGroupIDArr[]', jsPresetStudentSubjectGroupIDArr);
			}
			else {
				js_Select_All('StudentSubjectGroupIDArr[]', 1);
			}
				
			js_Reload_Student_Selection(jsInitial);
		}
	);
}

function js_Reload_Student_Selection(jsInitial)
{
	var jsSelectStudentSource = $("input[name='SelectStudentFrom']:checked").val();
	var jsExtraInfoList = Get_Selection_Value('ExtraItemIDArr[]', 'String');
	
	var jsYearClassIDList = '';
	var jsSubjectGroupIDList = '';
	if (jsSelectStudentSource == 'Class') {
		jsYearClassIDList = Get_Selection_Value('YearClassIDArr[]', 'String');
	}
	else if (jsSelectStudentSource == 'SubjectGroup') {
		jsSubjectGroupIDList = Get_Selection_Value('StudentSubjectGroupIDArr[]', 'String');
	}
					
	$('span#StudentSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Class_Student_Selection',
			YearClassIDList: jsYearClassIDList,
			SubjectGroupIDList: jsSubjectGroupIDList,
			ExtraInfoList: jsExtraInfoList,
			SelectionID: 'StudentIDArr[]',
			IsMultiple: 1,
			NoFirst: 1
		},
		function(ReturnData)
		{
			// Load preset if initialize and sepecfied the settings
			if (jsInitial == 1 && jsGlobalSettingsID != '')
			{
				js_Preset_Selection('StudentIDArr[]', jsPresetStudentIDArr);
			}
			else
			{
				js_Select_All('StudentIDArr[]', 1);
			}
		}
	);
}


function js_Hide_Option_Table(jsTableNumber)
{
	var jsTableID = "OptionTable_" + jsTableNumber;
	var jsShowSpanID = "spanShowOption_" + jsTableNumber;
	var jsHideSpanID = "spanHideOption_" + jsTableNumber;
	
	$('table#' + jsTableID).hide();
	$('span#' + jsShowSpanID).show();
	$('span#' + jsHideSpanID).hide();
}

function js_Show_Option_Table(jsTableNumber)
{
	var jsTableID = "OptionTable_" + jsTableNumber;
	var jsShowSpanID = "spanShowOption_" + jsTableNumber;
	var jsHideSpanID = "spanHideOption_" + jsTableNumber;
	
	$('table#' + jsTableID).show();
	$('span#' + jsShowSpanID).hide();
	$('span#' + jsHideSpanID).show();
}

function js_Update_Title_Input()
{
	var jsCoverTitleTb = Trim($('input#CoverTitle_1').val());
	var jsReportTitleTb = Trim($('input#ReportTitle_1').val());
	var jsCurReportTitleSelected = Trim($("select#ReportID option:selected").text());
		
	if (jsCoverTitleTb=='' || jsCoverTitleTb.toString()==jsLastReportTitleSelected.toString())
		$('input#CoverTitle_1').val(jsCurReportTitleSelected);
		
	if (jsReportTitleTb=='' || jsReportTitleTb.toString()==jsLastReportTitleSelected.toString())
		$('input#ReportTitle_1').val(jsCurReportTitleSelected);
		
	jsLastReportTitleSelected = jsCurReportTitleSelected;
}

function js_Add_More_Title_Row(jsType)
{
	var jsTableID = "OptionTable_" + jsType + "Settings";
	var TableBody = document.getElementById(jsTableID).tBodies[0];
	var RowIndex = document.getElementById("AddMoreRow_" + jsType).rowIndex;
	var NewRow = TableBody.insertRow(RowIndex);
	
	RowIndex--;
	if (jsType == 'Report')
		RowIndex--;
	NewRow.id = jsType + "TitleRow_" + RowIndex;
	$('tr#' + NewRow.id).addClass(jsType + "TitleTr");


	// Code
	var NewCell0 = NewRow.insertCell(0);
	var TmpHTML = '';
	TmpHTML += '<span class="sub_row_content_v30">(<?=$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['Row']?> <span class="RowIndexDisplaySpan_' + jsType + '">' + RowIndex + '</span>)</span>' + "\n";
	TmpHTML += '<span class="row_content_v30">' + "\n";
    	TmpHTML += '<input id="' + jsType + 'Title_' + RowIndex + '" name="' + jsType + 'TitleArr[]" type="text" class="textbox_name ' + jsType + 'TitleTb" />' + "\n";
    	TmpHTML += '&nbsp;&nbsp;' + "\n";
    	TmpHTML += '<a href="javascript:js_Delete_Title_Row(' + RowIndex + ', \'' + jsType + '\');" class="tablelink ' + jsType + 'TitleDeleteIcon">[<?=$Lang['Btn']['Delete']?>]</a>' + "\n";
	TmpHTML += '</span>' + "\n";
	TmpHTML += '<br style="clear:both;" />' + "\n";
	NewCell0.innerHTML = TmpHTML;
	
	var NewRowSpan = parseInt($('td#' + jsType + 'TitleTd').attr("rowspan")) + 1;
	$('td#' + jsType + 'TitleTd').attr("rowspan", NewRowSpan);
	
	document.getElementById('NumOfTitleRow_' + jsType).value++;
}

function js_Delete_Title_Row(jsRowIndex, jsType)
{
	// Remove the Selected Row
	var jsTableID = "OptionTable_" + jsType + "Settings";
	var jsTargetRowSpan = $('td#' + jsType + 'TitleTd').attr('rowspan') - 1;
	document.getElementById(jsTableID).tBodies[0].removeChild(document.getElementById(jsType + "TitleRow_" + jsRowIndex)); 
	document.getElementById('NumOfTitleRow_' + jsType).value--;
	
	// Update the rowspan of the left title
	$('td#' + jsType + 'TitleTd').attr('rowspan', jsTargetRowSpan);
	
	// Update the Tr ID
	var jsCounter = 2;
	var jsTmpID;
	$('tr.' + jsType + 'TitleTr').each( function() {
		jsTmpID = jsType + 'TitleRow_' + jsCounter;
		$(this).attr('id', jsTmpID);
		jsCounter++;
	});
	
	// Update the Row Display
	var jsCounter = 2;
	$('span.RowIndexDisplaySpan_' + jsType).each( function() {
		$(this).html(jsCounter);
		jsCounter++;
	});
	
	// Update the Textbox ID
	var jsCounter = 2;
	var jsTmpID;
	$('input.' + jsType + 'TitleTb').each( function() {
		jsTmpID = jsType + 'Title_' + jsCounter;
		$(this).attr('id', jsTmpID);
		jsCounter++;
	});
	
	// Update the onclick Delete function
	var jsCounter = 2;
	var jsTmpHref;
	$('a.' + jsType + 'TitleDeleteIcon').each( function() {
		jsTmpHref = "javascript:js_Delete_Title_Row(" + jsCounter + ", '" + jsType + "')";
		$(this).attr("href", jsTmpHref);
		jsCounter++;
	});
}

function js_Check_Uncheck_Check_All(jsSelectAllObjID, jsChecked)
{
	if (jsChecked == false)
		$('input#' + jsSelectAllObjID).attr('checked', jsChecked);
}

function js_Check_All_Other_Info_Checkbox(jsChecked)
{
	$('.OtherInfoChk').attr('checked', jsChecked);
}

function js_Init_DND_Table() {
		
	$("table#OtherInfoCatTable").tableDnD({
		onDrop: function(table, DroppedRow) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "" && !isNaN(rows[i].id))
					RecordOrder += rows[i].id+",";
			}
			RecordOrder = RecordOrder.substr(0,RecordOrder.length-1);
			$('input#OtherInfoCategoryOrderedList').val(RecordOrder);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	
	$("table#SettingsTable").tableDnD({
		onDrop: function(table, DroppedRow) {
			var rows = table.tBodies[0].rows;
			var RecordOrder = "";
			for (var i=0; i<rows.length; i++) {
				if (rows[i].id != "" && !isNaN(rows[i].id))
					RecordOrder += rows[i].id+",";
			}
			//alert("RecordOrder = " + RecordOrder);
			
			$.post(
				"ajax_update.php", 
				{ 
					Action: "Reorder_Settings",
					DisplayOrderString: RecordOrder
				},
				function(ReturnData)
				{
					js_Reload_Settings_Layer();
				}
			);
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}

function js_Init_JEdit()
{
	$('div.jEditCode').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsSettingsID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			if (jsInputValue=='' || $(this)[0].revert == jsInputValue || $('#CodeWarningDiv_' + jsSettingsID).css('display') != 'none')
			{
				// Empty or same Code => do nothing
				$('#CodeWarningDiv_' + jsSettingsID).hide();
				$(this)[0].reset();
			}
			else if (jsInputValue.Trim() != '' && !valid_code(jsInputValue))
			{
				$('#CodeWarningDiv_' + jsSettingsID).hide();
				$(this)[0].reset();
			}
			else
			{
				// Check if the code is vaild
				$.post(
					"ajax_validate.php", 
					{
						Action: 'Settings_Code',
						InputValue: jsInputValue,
						ExcludeSettingsID: jsSettingsID
					},
					function(ReturnData)
					{
						if (ReturnData == "1")
						{
							// valid code => Update Value
							$('#CodeWarningDiv_' + jsSettingsID).hide();
							
							// valid
							ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
							$.post(
								"ajax_update.php", 
								{ 
									Action: "Edit_Settings_Field",
									SettingsID: jsSettingsID,
									DBNameField: "SettingsCode",
									Value: jsInputValue
								},
								function(ReturnData)
								{
									var jsReturnMsg = '';
									if (ReturnData == '1')
										jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
									else
										jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
									
									js_Reload_Settings_Layer(0, jsReturnMsg);
								}
							);
						}
						else
						{
							// invalid code => show warning
							ElementObj[0].reset();
							$('div#CodeWarningDiv_' + jsSettingsID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code']?>').show();
						}
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Code']?>",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	);
	// Code Validation
	$('div.jEditCode').keyup( function() {
		var jsInputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var jsSettingsID = idArr[1];
		
		js_Code_Validation(jsInputValue, jsSettingsID, 'CodeWarningDiv_' + jsSettingsID);
	});
	$('div.jEditCode').click( function() {
		var jsInputValue = $('form input').val();
		var idArr = $(this).attr('id').split('_');
		var jsSettingsID = idArr[1];
		
		js_Code_Validation(jsInputValue, jsSettingsID, 'CodeWarningDiv_' + jsSettingsID);
	});
	
	$('div.jEditNameEn').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsSettingsID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			if (jsInputValue=='' || $(this)[0].revert == jsInputValue || $('#NameEnWarningDiv_' + jsSettingsID).css('display') != 'none')
			{
				// Empty or same Code => do nothing
				$('#NameEnWarningDiv_' + jsSettingsID).hide();
				$(this)[0].reset();
			}
			else if (jsInputValue.Trim() != '' && !valid_code(jsInputValue))
			{
				$('#NameEnWarningDiv_' + jsSettingsID).hide();
				$(this)[0].reset();
			}
			else
			{
				ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Edit_Settings_Field",
						SettingsID: jsSettingsID,
						DBNameField: "SettingsNameEn",
						Value: jsInputValue
					},
					function(ReturnData)
					{
						var jsReturnMsg = '';
						if (ReturnData == '1')
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						else
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						
						js_Reload_Settings_Layer(0, jsReturnMsg);
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Title']?>",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	);
	
	$('div.jEditNameCh').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsSettingsID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			if (jsInputValue=='' || $(this)[0].revert == jsInputValue || $('#NameChWarningDiv_' + jsSettingsID).css('display') != 'none')
			{
				// Empty or same Code => do nothing
				$('#NameChWarningDiv_' + jsSettingsID).hide();
				$(this)[0].reset();
			}
			else if (jsInputValue.Trim() != '' && !valid_code(jsInputValue))
			{
				$('#NameChWarningDiv_' + jsSettingsID).hide();
				$(this)[0].reset();
			}
			else
			{
				ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Edit_Settings_Field",
						SettingsID: jsSettingsID,
						DBNameField: "SettingsNameCh",
						Value: jsInputValue
					},
					function(ReturnData)
					{
						var jsReturnMsg = '';
						if (ReturnData == '1')
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						else
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
						
						js_Reload_Settings_Layer(0, jsReturnMsg);
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Title']?>",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	);
}

function js_Check_Form()
{
	var jsValid = true;
	
	// Check in descending order of selection display => focus the problematic element at the most top
	
	// Term Report and Consolidated Report with Module-based Mark display cannot generate without selecting the Last Topic Level
	var jsCheckedLastTopicLevel = $('input#TopicLevel_' + jsMaxTopicLevel).attr('checked');
	if (jsCurReportType == 'T' && jsCheckedLastTopicLevel == false)
	{
		$('div#ModuleReportMustHaveLastTopicLevelWarningDiv').show();
		$('input#SelectAll_TopicLevelChk').focus();
		jsValid = false;
	}
	else if (jsCurReportType == 'W' && $('input#MarkDisplay_ModuleBased').attr('checked') == true && jsCheckedLastTopicLevel == false)
	{
		$('div#ConsolidatedReportMustHaveLastTopicLevelWarningDiv').show();
		$('input#SelectAll_TopicLevelChk').focus();
		jsValid = false;
	}
	else
	{
		$('div#ConsolidatedReportMustHaveLastTopicLevelWarningDiv').hide();
	}
	
	// Check selected Topic Level
	var jsCheckedTopicLevel = false;
	$('input.TopicLevelChk').each( function() {
		if ($(this).attr('checked') == true)
		{
			jsCheckedTopicLevel = true;
			return false;
		}
	})
	if (jsCheckedTopicLevel == false)
	{
		$('div#SelectTopicLevelWarningDiv').show();
		$('input.TopicLevelChk').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectTopicLevelWarningDiv').hide();
	}	
	
	// Check selected Subject
	if ($('select#SubjectIDArr\\[\\]').val() == null)
	{
		$('div#SelectSubjectWarningDiv').show();
		$('select#SubjectIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectSubjectWarningDiv').hide();
	}
	
	// Check selected Student
	if ($('select#StudentIDArr\\[\\]').val() == null)
	{
		$('div#SelectStudentWarningDiv').show();
		$('select#StudentIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectStudentWarningDiv').hide();
	}
	
	// Check selected Class
	if ($('select#YearClassIDArr\\[\\]').val() == null)
	{
		$('div#SelectClassWarningDiv').show();
		$('select#YearClassIDArr\\[\\]').focus();
		jsValid = false;
	}
	else
	{
		$('div#SelectClassWarningDiv').hide();
	}
	
	
	if (jsValid == true)
		document.getElementById('form1').submit();
}



function js_Reload_Settings_Layer(jsWithNewRow, jsReturnMsg)
{
	$.post(
		"ajax_reload.php", 
		{ 
			Action: "Settings_Layer"
		},
		function(ReturnData)
		{
			$('#TB_ajaxContent').html(ReturnData);
			//$('#debugArea').html(ReturnData);
			
			if (jsWithNewRow == 1)
				js_Add_Temp_Settings_Row();
				
			if (jsReturnMsg != null && jsReturnMsg != '')
				Get_Return_Message('<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>');
				
			js_Init_DND_Table();
			js_Init_JEdit();
		}
	);
}

function js_Save_Settings_As()
{
	$('#ShowSettingsLayerLink').click();
	js_Reload_Settings_Layer(1);
}

function js_Add_Temp_Settings_Row()
{
	if (!document.getElementById('GenAddTopicRow')) {
		var TableBody = document.getElementById("SettingsTable").tBodies[0];
		var RowIndex = document.getElementById("AddRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddTopicRow";
		
		// Code
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="Code" id="Code" value="" class="textboxtext" onkeydown="js_Check_Press_Enter_Save_Settings(event);" maxlength="<?=$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Code']?>" />';
			tempInput += '<div id="CodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Title (Eng)
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="NameEn" id="NameEn" value="" class="textboxtext" onkeydown="js_Check_Press_Enter_Save_Settings(event);" maxlength="<?=$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Title']?>" />';
			tempInput += '<div id="NameEnWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell2 = NewRow.insertCell(2);
		var tempInput = '<input type="text" name="NameCh" id="NameCh" value="" class="textboxtext" onkeydown="js_Check_Press_Enter_Save_Settings(event);" maxlength="<?=$eRC_Rubrics_ConfigArr['MaxLength']['TemplateSettings']['Title']?>" />';
			tempInput += '<div id="NameChWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell2.innerHTML = tempInput;
		
		// Empty Cell
		var NewCell3 = NewRow.insertCell(3);
		var temp = '&nbsp;';
		NewCell3.innerHTML = temp;
		
		// Add and Reset Buttons
		var NewCell4 = NewRow.insertCell(4);
		var temp = '<?=$btnAdd?>';
		temp += '&nbsp;';
		temp += '<?=$btnCancel?>';
		NewCell4.innerHTML = temp;
		
		// Focus Code and add Preset Code
		$('input#Code').focus().val();		
		
		// Code validation
		js_Add_KeyUp_Code_Checking("Code", "CodeWarningDiv_New", '');
		js_Add_KeyUp_Blank_Checking('NameEn', 'NameEnWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>');
		js_Add_KeyUp_Blank_Checking('NameCh', 'NameChWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>');
	}
}

function js_Clear_Temp_Settings_Row()
{
	// Delete the Row
	var TableBody = document.getElementById("SettingsTable").tBodies[0];
	var RowIndex = document.getElementById("AddRow").rowIndex - 2;
	TableBody.deleteRow(RowIndex);
}

function js_Check_Press_Enter_Save_Settings(event)
{
	if (Get_KeyNum(event)==13) // keynum==13 => Enter
		js_Save_Settings();
}

function js_Add_KeyUp_Code_Checking(jsListenerObjectID, jsWarningDivID, jsSettingsID)
{
	// Code Validation
	$('#' + jsListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var jsExcludeSettingsID = idArr[1];
		
		if (jsExcludeSettingsID == null)
			jsExcludeSettingsID = '';
			
		if (!isNaN(jsExcludeSettingsID))
			jsExcludeSettingsID = jsSettingsID;
			
		js_Code_Validation(inputValue, jsExcludeSettingsID, jsWarningDivID);
	});
}

function js_Code_Validation(jsInputValue, jsExcludeSettingsID, jsWarningDivID)
{
	if (Trim(jsInputValue) == '')
	{
		$('#' + jsWarningDivID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Code']?>').show();
	}
	else if (!valid_code(jsInputValue))
	{
		$('#' + jsWarningDivID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>').show();
	}
	else
	{
		$('#' + jsWarningDivID).hide();
		
		$.post(
			"ajax_validate.php", 
			{
				Action: 'Settings_Code',
				InputValue: jsInputValue,
				ExcludeSettingsID: jsExcludeSettingsID
			},
			function(ReturnData)
			{
				if (ReturnData == "1")
				{
					// valid code
					$('#' + jsWarningDivID).hide();
				}
				else
				{
					// invalid code => show warning
					$('#' + jsWarningDivID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code']?>').show();
					//$('#debugArea').html('aaa ' + ReturnData);
				}
			}
		);
	}
}

function js_Save_New_Settings()
{
	var jsCode = Trim($('input#Code').val());
	var jsNameEn = Trim($('input#NameEn').val());
	var jsNameCh = Trim($('input#NameCh').val());
	
	if (js_Is_Input_Blank('Code', 'CodeWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Code']?>'))
	{
		$('input#Code').focus();
		return false;
	}
	else if (jsCode != '' && !valid_code(jsCode))
	{
		$('input#Code').focus();
		$('div#CodeWarningDiv_New').html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>').show();
		return false;
	}
	
	if (js_Is_Input_Blank('NameEn', 'NameEnWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>'))
	{
		$('input#NameEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('NameCh', 'NameChWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>'))
	{
		$('input#NameCh').focus();
		return false;
	}
	
	$.post(
		"ajax_validate.php", 
		{
			Action: 'Settings_Code',
			InputValue: jsCode,
			ExcludeSettingsID: ''
		},
		function(ReturnData)
		{
			if (ReturnData == "1")
			{
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Add_Settings_Record",
						Code: jsCode,
						NameEn: jsNameEn,
						NameCh: jsNameCh
					},
					function(ReturnData)
					{
						//$('#debugArea').html(ReturnData);
						
						if (ReturnData != '')
						{
							js_Save_Settings(ReturnData);
						}
						else
						{
							Get_Return_Message('<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>');
						}
						//js_Hide_ThickBox();
					}
				);
				
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv_New').html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code']?>').show();
				$('input#TopicCode').focus();
				return ;
			}
		}
	);
}

function js_Save_Settings(jsSettingsID, jsFromMainPage)
{
	//encodeURIComponent
	Block_Document();
	
	var jsSubmitString = Get_Form_Values(document.getElementById('form1'));
	jsSubmitString += "Action=Save_Settings&SettingsID=" + jsSettingsID;
	
//	$.ajax({  
//		type: "POST",  
//		url: "ajax_update.php",
//		data: jsSubmitString,  
//		success: function(data) 
//		{
//			var jsReturnMsg;
//			if (data=='1')
//			{
//				jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
//			}
//			else
//			{
//				jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
//			}
//			
//			js_Reload_Settings_Layer(0, jsReturnMsg);
//		} 
//	});

	var jsFormValues = $("form#form1").serialize();
	$.post(
		"ajax_update.php?Action=Save_Settings&SettingsID=" + jsSettingsID,
		$("form#form1").serialize(),
		function(ReturnData)
		{
			var jsReturnMsg;
			if (ReturnData=='1')
				jsReturnMsg = '<?=$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReturnMsgArr']['SaveSettingsSuccess']?>';
			else
				jsReturnMsg = '<?=$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['ReturnMsgArr']['SaveSettingsFailed']?>';
			
			UnBlock_Document();
			
			if (jsFromMainPage == 1)
			{
				Get_Return_Message(jsReturnMsg);
				Scroll_To_Top();
			}
			else
			{
				js_Reload_Settings_Layer(0, jsReturnMsg);
			}
		}
	);
	return false;
}

function js_Delete_Settings(jsSettingsID)
{
	if (confirm('<?=$Lang['eRC_Rubrics']['ReportsArr']['ReportCardGenerationArr']['jsWarningArr']['DeleteSettings']?>'))
	{
		$.post(
			"ajax_update.php", 
			{ 
				Action: "Delete_Settings",
				SettingsID: jsSettingsID
			},
			function(ReturnData)
			{
				//$('#debugArea').html(ReturnData);
				
				var jsReturnMsg;
				if (ReturnData == '1')
				{
					jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
				}
				else
				{
					jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
				}
				js_Reload_Settings_Layer(0, jsReturnMsg);
			}
		);
	}
}

function js_Load_Settings(jsSettingsID)
{
	window.location = 'index.php?SettingsID=' + jsSettingsID;  
}

function js_Preset_Selection(jsSelectionObjID, jsPresetArr, jsWithOptGroup)
{
	jsWithOptGroup = jsWithOptGroup || false;
	var jsOptGroupSelector = '';
	
	jsSelectionObjID = jsSelectionObjID.replace('[', '\\[').replace(']', '\\]');
	
	$('select#' + jsSelectionObjID + ' > option').each( function() {
		if (jIN_ARRAY(jsPresetArr, $(this).val()) == true) {
			$(this).attr('selected', true);
		}	
		else {
			$(this).attr('selected', false);
		}	
	});
	
	if (jsWithOptGroup) {
		$('select#' + jsSelectionObjID + ' optgroup > option').each( function() {
			if (jIN_ARRAY(jsPresetArr, $(this).val()) == true) {
				$(this).attr('selected', true);
			}
			else {
				$(this).attr('selected', false);
			}
		});
	}
}

function js_Changed_Student_Selection_Source(jsTargetSource, jsInitial)
{
	if (jsTargetSource == 'Class') {
		$('tr.Student_FormClassTr').show();
		$('tr.Student_SubjectGroupTr').hide();
	}
	else if (jsTargetSource == 'SubjectGroup') {
		$('tr.Student_FormClassTr').hide();
		$('tr.Student_SubjectGroupTr').show();
	}
	
	js_Reload_Student_Selection(jsInitial);
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>