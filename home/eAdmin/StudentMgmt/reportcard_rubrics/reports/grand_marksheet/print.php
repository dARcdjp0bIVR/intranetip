<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Reports_GrandMarksheet";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

$lreportcard = new libreportcardrubrics();

$lreportcardrubrics_topic = new libreportcardrubrics_topic();
$lreportcardrubrics_module = new libreportcardrubrics_module();
$lreportcardrubrics_rubrics = new libreportcardrubrics_rubrics();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();

# prepare Student Arr
$FormClassTitle = $DisplayFormat=="FormSummary"?"YearName":Get_Lang_Selection("ClassTitleCh","ClassTitleEn");
$StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($YearID, $YearClassIDArr);
$StudentInfoAssoc = BuildMultiKeyAssoc($StudentInfoArr,array($FormClassTitle,"UserID"));

$StudentIDArr = Get_Array_By_Key($StudentInfoArr, "UserID");

# Get Student Mark
$StudentMarksheetScoreArr = $lreportcard->Get_Student_Marksheet_Score($SubjectIDArr, $StudentIDArr, $ModuleID);

# Get Module Info
$ModuleInfoArr = $lreportcardrubrics_module->Get_Module_Info();
$ModuleInfoAssoc = BuildMultiKeyAssoc($ModuleInfoArr,"ModuleID");

# Get Subject Info
$SubjectInfoArr = $lreportcard->Get_Subject_Info();
$SubjectInfoAssoc = BuildMultiKeyAssoc($SubjectInfoArr,"SubjectID");

# Get Topic Info  
$TopicTree = $lreportcardrubrics_topic->Get_Topic_Tree($SubjectIDArr);
$TopicInfoArr = $lreportcardrubrics_topic->Get_Topic_Info($SubjectIDArr);
$LevelTopicArr = BuildMultiKeyAssoc($TopicInfoArr, array("Level","TopicID"));

# Get Rubrics Info 
$RubricsItemArr = $lreportcardrubrics_rubrics->Get_Rubics_Item_Info();
$RubricsItemArr =  BuildMultiKeyAssoc($RubricsItemArr, array("RubricsItemID"));
$LevelNameLang = Get_Lang_Selection("LevelNameCh", "LevelNameEn");

if(count($TopicIDArr)==0)
{
	$TopicIDArr = array_keys($LevelTopicArr[3]);
}

$NumOfModule = count($ModuleID);
foreach($TopicIDArr as $TopicID)
{
	$ObjecttiveID = $TopicInfoArr[$TopicID]['PreviousLevelTopicID'];
	$SubjectID = $TopicInfoArr[$TopicID]['SubjectID'];
	$ObjecttiveChild[$ObjecttiveID]+= $NumOfModule;
	$SubjectChild[$SubjectID] += $NumOfModule;
}

foreach($ObjecttiveChild as $ObjecttiveID => $NumOfChild)
{
	$ParentTopicID = $TopicInfoArr[$ObjecttiveID]['PreviousLevelTopicID'];
	$ParentTopicChild[$ParentTopicID]+=$NumOfChild;
}

$HeaderRowSpan = 3;
if($DisplayTopicTitle) $HeaderRowSpan++;
if($DisplayObjectiveTitle) $HeaderRowSpan++;
	
foreach($StudentData as $StudentField)
{
	$StudentInfoHead .= '<td rowspan='.$HeaderRowSpan.' valign="bottom">'.$Lang['eRC_Rubrics']['GeneralArr'][$StudentField].'</td>'."\n";
	
	$StudentInfoHeadExport[] = $Lang['eRC_Rubrics']['GeneralArr'][$StudentField];
	$EmptyStudentInfoHeadExport[] = "";
}

$TopicTitleLang = Get_Lang_Selection("TopicNameCh","TopicNameEn");
$ModuleNameLang = Get_Lang_Selection("ModuleNameCh","ModuleNameEn");
$SubjectTitleLang = Get_Lang_Selection("CH_DES","EN_DES");

foreach($TopicTree as $SubjectID => $TopicArr)
{
	if(!$SubjectChild[$SubjectID]) continue;
	
	$SubjectTitle = $SubjectInfoAssoc[$SubjectID][$SubjectTitleLang];
	$SubjectRow .= '<td colspan="'.$SubjectChild[$SubjectID].'" align="center">'.$SubjectTitle.'</td>';
	
	$SubjectRowExport[] = $SubjectTitle;
	for($i=1; $i<$SubjectChild[$SubjectID]; $i++) $SubjectRowExport[] = "";
	
	foreach($TopicArr as $thisTopicID => $ObjectiveArr)
	{
		if(!$ParentTopicChild[$thisTopicID]) continue;
		
		$TopicName = $LevelTopicArr[1][$thisTopicID][$TopicTitleLang];
		$TopicRow .= '<td colspan="'.$ParentTopicChild[$thisTopicID].'" align="center">'.$TopicName.'</td>';

		$TopicRowExport[] = $TopicName;
		for($i=1; $i<$ParentTopicChild[$thisTopicID]; $i++) $TopicRowExport[] = "";

		foreach($ObjectiveArr as $thisObjectiveID => $Detail)
		{
			if(!$ObjecttiveChild[$thisObjectiveID]) continue;
			
			$ObjectiveName = $LevelTopicArr[2][$thisObjectiveID][$TopicTitleLang];
			$ObjectiveRow .= '<td colspan="'.$ObjecttiveChild[$thisObjectiveID].'" align="center">'.$ObjectiveName.'</td>';	

			$ObjectiveRowExport[] = $ObjectiveName;
			for($i=1; $i<$ObjecttiveChild[$thisObjectiveID]; $i++) $ObjectiveRowExport[] = "";

			foreach(array_keys($Detail) as $DetailID)
			{
				if(!in_array($DetailID, $TopicIDArr)) continue;
				
				$DetailName = $LevelTopicArr[3][$DetailID][$TopicTitleLang];
				$DetailRow .= '<td colspan="'.$NumOfModule.'" align="center">'.$DetailName.'</td>';

				$DetailRowExport[] = $DetailName;
				for($i=1; $i<$NumOfModule; $i++) $DetailRowExport[] = "";

				foreach($ModuleID as $thisModuleID)
				{
					
					$ModuleName = $ModuleInfoAssoc[$thisModuleID][$ModuleNameLang];
					$ModuleRow .= '<td align="center">'.$ModuleName.'</td>';
					$ModuleRowExport[] = $ModuleName;
					
					$StudentRowMarkDetail['SubjectID'] = $SubjectID;
					$StudentRowMarkDetail['ModuleID'] = $thisModuleID;
					$StudentRowMarkDetail['TopicID'] = $DetailID;
					$StudentRowMarkDetailArr[] = $StudentRowMarkDetail;
				}
			}
		}	
	}
}

foreach($StudentInfoAssoc as $FormClassTitle => $thisStudentInfoArr)
{
	$StudentRow = '';
	$StudentRowExportArr = array();
	foreach($thisStudentInfoArr as $StudentID => $thisStudentInfo)
	{
		
		$StudentRow .= '<tr>'."\n";
			$StudentRowExport = array();
			foreach($StudentData as $StudentField)
			{
				switch ($StudentField)
				{
					case "ClassName":		$StudentValue = $thisStudentInfo['ClassTitleCh'];	break;
					case "ClassNumber":		$StudentValue = $thisStudentInfo['ClassNumber'];	break;
					case "EnglishName":		$StudentValue = $thisStudentInfo['StudentNameEn'];	break;
					case "ChineseName":		$StudentValue = $thisStudentInfo['StudentNameCh'];	break;
					case "Gender":			$StudentValue = $thisStudentInfo['Gender'];	break;
				}
				$nowrap = ${"Std".$StudentField."NoWrap"}?"nowrap":"";
				$StudentRow .= '<td '.$nowrap.'>'.$StudentValue.'</td>'."\n";
				$StudentRowExport[] =  $StudentValue;
				
			}
			
			foreach($StudentRowMarkDetailArr as $thisStudentRowMarkDetail)
			{
				$thisSubjectID = $thisStudentRowMarkDetail['SubjectID'];
				$thisModuleID = $thisStudentRowMarkDetail['ModuleID'];
				$thisTopicID = $thisStudentRowMarkDetail['TopicID'];
				
				if(isset($StudentMarksheetScoreArr[$StudentID][$thisSubjectID][$thisTopicID][$thisModuleID]["RubricsItemID"]))
					$thisLevel = $RubricsItemArr[$StudentMarksheetScoreArr[$StudentID][$thisSubjectID][$thisTopicID][$thisModuleID]["RubricsItemID"]][$LevelNameLang];
				else
					$thisLevel = "&nbsp;";
				
				$StudentRow .= '<td>'.$thisLevel.'</td>'."\n";
				$StudentRowExport[] =  $thisLevel;
			}
		$StudentRow .= '</tr>'."\n";
		$StudentRowExportArr[] = $StudentRowExport;
	}
	
	if($ViewFormat == 0)
	{
		$table .= '<div class="grandmarksheet_title">'.$FormClassTitle.'</div>';
		$table .= '<table border=1 class="report_table">'."\n";
			$table .= '<tr>'.$StudentInfoHead.$SubjectRow.'</tr>'."\n";
			if($DisplayTopicTitle)	$table .= '<tr>'.$TopicRow.'</tr>'."\n";
			if($DisplayObjectiveTitle)  $table .= '<tr>'.$ObjectiveRow.'</tr>'."\n";
			$table .= '<tr>'.$DetailRow.'</tr>'."\n";
			$table .= '<tr>'.$ModuleRow.'</tr>'."\n";
			$table .= $StudentRow."\n";
		$table .= '</table>'."\n";
		$table .= '<br>'."\n";
	}
	else
	{
		$ClassExportArr = array();
		$ClassExportArr[] = array($FormClassTitle);
		$ClassExportArr[] = array_merge($EmptyStudentInfoHeadExport,$SubjectRowExport);
		if($DisplayTopicTitle)	$ClassExportArr[] = array_merge($EmptyStudentInfoHeadExport,$TopicRowExport);
		if($DisplayObjectiveTitle)  $ClassExportArr[] = array_merge($EmptyStudentInfoHeadExport,$ObjectiveRowExport);
		$ClassExportArr[] = array_merge($EmptyStudentInfoHeadExport,$DetailRowExport);
		$ClassExportArr[] = array_merge($StudentInfoHeadExport,$ModuleRowExport);
		$ClassExportArr = array_merge($ClassExportArr, $StudentRowExportArr);
		
		$ExportArr = array_merge((array)$ExportArr,$ClassExportArr);
		$ExportArr[] = array("");
	}
}

if($ViewFormat == 0)
{
	include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
	echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));
	
	### Print Button
	$PrintBtn = $lreportcard_ui->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");
	
	?>
	<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
		<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
	</table>
	<table border="0" cellpadding="4" width="95%" cellspacing="0">
		<tr><td><?=$table?></td></tr>
	</table>
	<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
		<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
	</table>
	
	<?
}
else
{
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$ExportTxt = $lexport->GET_EXPORT_TXT($ExportArr, "", "", "\r\n", "", 0, "11");
	$filename = trim($Lang['eRC_Rubrics']['ReportsArr']['GrandMarksheetArr']['MenuTitle']);
	$filename = str_replace('_','%20',$filename);
	$lexport->EXPORT_FILE("$filename.csv", $ExportTxt);
}
intranet_closedb();
?>