<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
//include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$fs = new libfilesystem();
$limport = new libimporttext();
$lreportcard_ui = new libreportcardrubrics_ui();
$lreportcard_topic = new libreportcardrubrics_topic();

$commentType = ($_POST['commentType'])? $_POST['commentType'] : 'class';


$UpdateFail = false;

// check $userfile is empty 
if(!$UpdateFail && !empty($userfile)) 
{
	$loc = $userfile;
	$filename = $userfile_name;
}
else
	$UpdateFail = true;

# check if file name is empty  
if(!$UpdateFail && trim($filename)!='')
	$ext = $fs->file_ext($filename);
else 
	$UpdateFail = true;

# check if file ext valid and file exist
if(!$UpdateFail && ($ext==".csv" || $ext==".txt")&& $loc!="none" && file_exists($loc)) 
	$data = $limport->GET_IMPORT_TXT($loc);
else {
	$UpdateFail = true;
}


# check if data is empty 
$data = $limport->GET_IMPORT_TXT($loc);
if(!$UpdateFail && !$data)
	$UpdateFail = true;

if($UpdateFail)
{
	header("Location: import.php?commentType=$commentType&msg=WrongFileFormat");
		exit;
}

# create folder if not exist
//			$fs = new libfilesystem();

$success = 0;	

$ImportHeader = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']; 
$fieldName = array(
	$ImportHeader['CommentCode']['En'],
	$ImportHeader['CommentCh']['En'],
	$ImportHeader['CommentEn']['En']
);
if($commentType=='subject') {
	$fieldName[] = $ImportHeader['SubjectCode']['En'];
	$fieldName[] = $ImportHeader['SubjectName']['En'];
	
	for ($i=1; $i<=$lreportcard->MaxTopicLevel; $i++) {
		$fieldName[] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i]['En'];
		$fieldName[] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i]['En'];
	}
	
	
	### Get all last level Topic for validation
	$TopicInfoArr = $lreportcard_topic->Get_Topic_Info('', '', $lreportcard->MaxTopicLevel, '', '', '', $ReturnAsso=0);
	$TopicAssoArr = BuildMultiKeyAssoc($TopicInfoArr, 'TopicCode');
	unset($TopicInfoArr);
}
$numOfHeader = count($ImportHeader) + $lreportcard->MaxTopicLevel + 1;

$header_row = array_values(array_remove_empty(array_shift($data)));
$wrong_format = $fs->CHECK_CSV_FILE_FORMAT($header_row, $fieldName);

// Validate the data according to the setting of config file
// Empty fields will be accepted
if($wrong_format) {
	header("Location: import.php?commentType=$commentType&msg=WrongCSVHeader");
	exit;
}

$wrong_row = array();

// skip $data[0] as $data[0] should be the chinese title
$ImportData = array();
for($i=1; $i<sizeof($data); $i++) {
	//list($CommentCode, $CommentEn, $CommentCh) = $data[$i];
	
	$thisColumnCount = 0;
	$thisCommentCode = trim($data[$i][$thisColumnCount++]);
	$thisCommentCh = trim($data[$i][$thisColumnCount++]);
	$thisCommentEn = trim($data[$i][$thisColumnCount++]);
	$thisSubjectCode = trim($data[$i][$thisColumnCount++]);
	$thisSubjectName = trim($data[$i][$thisColumnCount++]);
	
	$thisTopicInfoArr = array();
	$thisLastTopicCodeIndex = 0;
	for ($j=1; $j<=$lreportcard->MaxTopicLevel; $j++) {
		$thisTopicInfoArr[$j]['TopicCode'] = trim($data[$i][$thisColumnCount++]);
		if ($j == $lreportcard->MaxTopicLevel) {
			$thisLastTopicCodeIndex = $thisColumnCount - 1;
		}
		
		$thisTopicInfoArr[$j]['TopicName'] = trim($data[$i][$thisColumnCount++]);
	}
	
	if($thisCommentCode=='') {
		$WarnMsg[$i][] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentCode'];
		$WarnCss[$i][0] = true;
	}
	else if(!valid_code($thisCommentCode)) {
		$WarnMsg[$i][] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['WrongCodeFormat'];
		$WarnCss[$i][0] = true;
	}
	
	if($thisCommentEn=='') {
		$WarnMsg[$i][] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentEn'];
		$WarnCss[$i][1] = true;
	}
	
	if($thisCommentCh=='') {
		$WarnMsg[$i][] = $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['CommentCh'];
		$WarnCss[$i][2] = true;
	}
	
	// Checkings for Subject Details only
	if($commentType=='subject') {
		
		$thisCommentLengthEn = utf8_strlen($thisCommentEn);
		$thisCommentLengthCh = utf8_strlen($thisCommentCh);
		
		if ($thisCommentLengthCh > $eRC_Rubrics_ConfigArr['MaxLength']['Marksheet']['Remarks']) {
			$WarnMsg[$i][] = str_replace('<!--MaxWordCount-->', $eRC_Rubrics_ConfigArr['MaxLength']['Marksheet']['Remarks'], $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['CommentChExceededWordLimit']);
			$WarnCss[$i][1] = true;
		}
		
		if ($thisCommentLengthEn > $eRC_Rubrics_ConfigArr['MaxLength']['Marksheet']['Remarks']) {
			$WarnMsg[$i][] = str_replace('<!--MaxWordCount-->', $eRC_Rubrics_ConfigArr['MaxLength']['Marksheet']['Remarks'], $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['CommentEnExceededWordLimit']);
			$WarnCss[$i][2] = true;
		}
		
		$thisLastTopicCode = $thisTopicInfoArr[$lreportcard->MaxTopicLevel]['TopicCode'];
		if ($thisLastTopicCode == '') {
			$WarnMsg[$i][] = str_replace('<!--TopicLevelName-->', $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$lreportcard->MaxTopicLevel]['Title'], $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['Empty']['TopicCode']);
			$WarnCss[$i][$thisLastTopicCodeIndex] = true;
		}
		else {
			$thisTopicID = $TopicAssoArr[$thisLastTopicCode]['TopicID'];
			if ($thisTopicID == '') {
				$WarnMsg[$i][] = str_replace('<!--TopicLevelName-->', $Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['LevelArr'][$lreportcard->MaxTopicLevel]['Title'], $Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportWarningArr']['WrongTopicCode']);
				$WarnCss[$i][$thisLastTopicCodeIndex] = true;
			}
		}
	}
	
	if(empty($WarnMsg[$i]))	
		$ImportData[] = $data[$i];
}

$NoOfFail = count($WarnMsg);
$NoOfSuccess = sizeof($data)-1-$NoOfFail;

$ChEn = Get_Lang_Selection("Ch","En");
if(count($WarnMsg)>0) {
	# build Table 
	$display .= '<table width="100%" border="0" cellpadding="5" cellspacing="0">'."\n";
		$display .= '<tr>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink" width="1%">Row#</td>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCode'][$ChEn].'</td>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentEn'][$ChEn].'</td>'."\n";
			$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['CommentCh'][$ChEn].'</td>'."\n";
			if($commentType=='subject') {
				$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectCode'][$ChEn].'</td>'."\n";
				$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['SubjectName'][$ChEn].'</td>'."\n";
				
				for ($i=1; $i<=$lreportcard->MaxTopicLevel; $i++) {
					$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicCode'][$i][$ChEn].'</td>'."\n";
					$display .= '<td class="tablebluetop tabletopnolink">'.$Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ImportHeader']['TopicName'][$i][$ChEn].'</td>'."\n";
				}
			}				
			$display .= '<td class="tablebluetop tabletopnolink" width="25%">'.$Lang['General']['Remark'].'</td>'."\n";
		$display .= '</tr>'."\n";
	
		foreach($WarnMsg as $rowno => $Msg)
		{
			$rowcss = " class='".($errctr%2==0? "tablebluerow2":"tablebluerow1")."' ";
			
			$display .= '<tr '.$rowcss.'>'."\n";
				$display .= '<td class="tabletext">'.($rowno+1).'</td>'."\n";
				for($j=0; $j<$numOfHeader; $j++)
				{
					$css = $WarnCss[$rowno][$j]?"red":"";
					$value = $WarnCss[$rowno][$j]&&empty($data[$rowno][$j])?"***":$data[$rowno][$j];
					$display .= '<td class="tabletext '.$css.'">'.$value.'</td>'."\n";
				}
				$display .= '<td class="tabletext">'.implode("<br>",$Msg).'</td>'."\n";
			$display .= '</tr>'."\n";
			
			$errctr++;
		}
	$display .= '</table>'."\n";

}

	$BackBtn = $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()","back");
	$NextBtn = $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2");
	
	$Btn .= empty($WarnMsg)?$NextBtn."&nbsp;":"";
	$Btn .= $BackBtn;
						
	# tag information
	$TAGS_OBJ = $lreportcard_ui->Get_Settings_CommentBank_Tab($commentType);

	# page navigation (leave the array empty if no need)
	if ($commentType == 'class') {
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['ClassTeacherComment'], "index.php");
	}
	else if ($commentType == 'subject') {
		$PAGE_NAVIGATION[] = array($Lang['eRC_Rubrics']['SettingsArr']['CommentBankArr']['SubjectDetailsComment'], "index_subject.php");
	}
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'], "");
	
	$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
	
	$linterface->LAYOUT_START();
?>
<form id="form1" name="form1" enctype="multipart/form-data" action="import_update.php" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?><br/><br/></td>
</tr>
<tr>
	<td><?= $linterface->GET_IMPORT_STEPS(2) ?></td>
</tr>
<tr>
	<td>
		<table width="30%">
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?></td>
				<td class='tabletext'><?=$NoOfSuccess?></td>
			</tr>
			<tr>
				<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?></td>
				<td class='tabletext <?=$NoOfFail>0?"red":""?>'><?=$NoOfFail?></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<?=$display?>
	</td>
</tr>
<tr>
	<td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center"><?=$Btn?></td>
		</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="commentType" value="<?=$commentType?>" />
<input type="hidden" name="ImportData" value="<?=htmlspecialchars(serialize($ImportData))?>" />

</form>
<br><br>
<?
$linterface->LAYOUT_STOP();

?>