<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";
$currentTab = "subject";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");



### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

# tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CommentBank_Tab($currentTab);

# Max Level
$MaxLevel = $eRC_Rubrics_ConfigArr['MaxLevel']+1;

#Return Msg
$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

$Keyword = (isset($_POST['Keyword']) && $_POST['Keyword'] != '')? trim(stripslashes($_POST['Keyword'])) : $Keyword;

echo $lreportcard_ui->Include_JS_CSS(array('rubrics_js', 'cookies'));
echo $lreportcard_ui->Get_Settings_CommentBank_UI($field, $order, $pageNo, $Keyword, $currentTab);
?>

<script language="javascript">
var jsCurSubjectID = '';
var jsCurYearID = '';
var jsMaxTopicLevel = <?=$MaxLevel?>;


var arrCookies = new Array();
arrCookies[arrCookies.length] = "SubjectID";
arrCookies[arrCookies.length] = "YearID";
var jsLevelSelectionStartIndex = arrCookies.length;
for (i=1; i<jsMaxTopicLevel; i++) {
	arrCookies[arrCookies.length] = 'Level' + i + '_TopicID';
}
	
	
$(document).ready( function() {
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	Blind_Cookies_To_Object();
	
	
	// Initialize Subject Selection
	var jsDefaultSubjectID = $.cookies.get(arrCookies[0]);
	if (jsDefaultSubjectID == null || jsDefaultSubjectID == '')
		jsDefaultSubjectID = $('select#SubjectID option:nth-child(1)').val();
	$('select#SubjectID').val(jsDefaultSubjectID);
	jsCurSubjectID = jsDefaultSubjectID;
	
	// Initialize Form Selection
	var jsDefaultYearID = $.cookies.get(arrCookies[1]);
	if (jsDefaultYearID == null || jsDefaultYearID == '')
		jsDefaultYearID = $('select#YearID option:nth-child(1)').val();
	$('select#YearID').val(jsDefaultYearID);
	jsCurYearID = jsDefaultYearID;
	
	// Initialize Topic Filtering
	var jsThisLevel;
	var jsThisCookieIndex;
	var jsThisTopicID;
	var jsThisPreviousLevelTopicID;
	var jsDefaultFilterTopicInfoText = '';

	
	
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Comment_Table();
	});
	
	
	// Reload Subject
	js_Reload_Subject_Selection();
	
	// Reload Table
	js_Reload_Comment_Table(jsDefaultFilterTopicInfoText);
});

function js_Go_Export()
{
	$('input#forImportSample').val(1);
	$('input#FilterTopicString').val(js_Get_Filtering_Info_Text());
	
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'export.php';
	jsFormObj.submit();
	
	jsFormObj.action = 'index.php';
}

function js_Reload_Comment_Table(jsFilterTopicInfoText)
{
	
	jsKeyword = Trim($('input#Keyword').val());
	jsSubjectID = $('select#SubjectID').val();
	jsYearID = $('select#YearID').val();
	
	if(jsSubjectID === undefined) {
		jsSubjectID='';
	}
	
	$('div#CommentTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Comment_Table',
			field: '<?=$field?>',
			order: '<?=$order?>',
			page: '<?=$pageNo?>',
			Keyword: jsKeyword,
			commentType: 'subject',
			subjectID: jsSubjectID,
			FilterTopicStr: js_Get_Filtering_Info_Text()
		},
		function(ReturnData) {
			
		}
	);
}


function js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID)
{
	var jsNextLevel = parseInt(jsLevel) + 1;
	$('div#Level' + jsLevel + '_SelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Reload_Topic_Selection',
			SubjectID: jsCurSubjectID,
			Level: jsLevel,
			PreviousLevelTopicID: jsPreviousLevelTopicID,
			SelectionID: 'Level' + jsLevel + '_TopicID',
			OnChange: 'js_Reload_Topic_Selection(' + jsNextLevel + ', this.value);',
			TopicID: jsTargetTopicID
		},
		function(ReturnData) {	

//			Blind_Cookies_To_Object();
		}
	);
}

function js_Reload_Subject_Selection(ParReload)
{

	$('div#SubjectSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: 'Subject_Selection',
			async: false,
			SelectionID: 'SubjectID',
			OnChange: 'js_Changed_Subject_Selection(this.value)',
			SubjectID: jsCurSubjectID,
			CheckModuleSubject: 1,
			FirstTitle: '<?=$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject']?>',
			NoFirst: 0,
			AccessibleSubjectOnly: 1
		},
		function(ReturnData)
		{
			jsCurSubjectID = $('select#SubjectID').val();
			
			$('div.SubjectSelectionDiv').html(ReturnData);
			
			if (jsCurSubjectID == '') {
				$('div.LevelSelectionDiv').html('');
			}
			else {	
				js_Reload_Topic_Selection(jsLevel=1, '', '', 0);
			}
				
		}
	);
}

function js_Changed_Subject_Selection(jsSubjectID)
{
	jsCurSubjectID = jsSubjectID;
	
	$('div#CommentTableDiv').html('');
	
	if (jsCurSubjectID=='')
	{
		for (i=jsLevel; i<jsMaxTopicLevel; i++)
		{
			$('div#Level' + i + '_SelectionDiv').html('');
		}
		js_Reload_Comment_Table();
	}
	else
	{
		js_Reload_Topic_Selection(jsLevel=1, '', '', 1);	
	}
}

function js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	$('div#CommentTableDiv').html('');

	if (jsTargetTopicID == null)
	{
		jsTargetTopicID = '';
	}	
		
	if (jsReloadTable == null)
	{
		jsReloadTable = 1;
	}
		
		
	// Hide the all Next Level Topic Selection
	var i;
	for (i=jsLevel; i<=jsMaxTopicLevel; i++)
	{
		$('div#Level' + i + '_SelectionDiv').html('');
	}

	if (jsLevel < jsMaxTopicLevel)
	{  
		if (jsLevel==1 || (jsLevel > 1 && jsPreviousLevelTopicID != ''))
		{
			js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID);
		}
		else
		{
		}		
	}

	if (jsReloadTable==1)
	{
		js_Reload_Comment_Table();
	}
		
}
</script>

<br/>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
?>