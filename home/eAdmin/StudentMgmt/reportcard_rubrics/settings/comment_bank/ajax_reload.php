<?php
// using connie
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");	

$lreportcard_comment = new libreportcardrubrics_comment();
$lreportcard_ui = new libreportcardrubrics_ui();

$lreportcard = new libreportcardrubrics_custom();

switch ($Action)
{
	case "Comment_Table":
		$field = trim(urldecode(stripslashes($_REQUEST['field'])));
		$order = trim(urldecode(stripslashes($_REQUEST['order'])));
		$page = trim(urldecode(stripslashes($_REQUEST['page'])));
		$Keyword = trim(urldecode(stripslashes($_REQUEST['Keyword'])));
		$commentType = trim(urldecode(stripslashes($_REQUEST['commentType'])));
		
		$subjectID = trim(stripslashes($_REQUEST['subjectID']));
		$FilterTopicStr = trim(stripslashes($_REQUEST['FilterTopicStr']));

		echo $lreportcard_ui->Get_Settings_CommentBank_DBTable($field, $order, $page, $Keyword,$commentType,$subjectID,$FilterTopicStr);
//		debug_pr(convert_size(memory_get_usage()));
//		debug_pr(convert_size(memory_get_usage(true)));
//		debug_pr('Query Count = '.$GLOBALS[debug][db_query_count]);
//		$lreportcard->db_show_debug_log();
	break;
	
	case "Subject_Selection":
		$SubjectID = $_REQUEST['SubjectID'];
		$SelectionID = $_REQUEST['SelectionID'];
		$OnChange = $_REQUEST['OnChange'];
		$NoFirst = $_REQUEST['NoFirst'];
		$FirstTitle = stripslashes(trim($_REQUEST['FirstTitle']));
		$AccessibleSubjectOnly = $_REQUEST['AccessibleSubjectOnly'];
		
		$NoFirst = ($NoFirst == '')? 1 : $NoFirst;
		
		$libSubject = new subject();
		$SubjectArr = $libSubject->Get_Subject_List();
		$SubjectIDArr = Get_Array_By_Key($SubjectArr, 'RecordID');			
		
		if ($AccessibleSubjectOnly)
		{
			$AccessibleSubjectArr = $lreportcard->Get_User_Accessible_Subject($YearTermID);
			$AccessibleSubjectIDArr = Get_Array_By_Key($AccessibleSubjectArr, 'SubjectID');
		}
		else
		{
			if (!isset($libSubject))
			{
				$libSubject = new subject();
			}				
			$SubjectArr = $libSubject->Get_Subject_List();
			$AccessibleSubjectIDArr = Get_Array_By_Key($SubjectArr, 'RecordID');
		}
		
		$DisplaySubjectID = array_values(array_intersect($SubjectIDArr, $AccessibleSubjectIDArr));
		
		$scm_ui = new subject_class_mapping_ui();
		echo $scm_ui->Get_Subject_Selection($SelectionID, $SubjectID, $OnChange, $NoFirst, $FirstTitle, $YearTermID, $OnFocus='', $FilterSubjectWithoutSG=0, $IsMultiple=0, $DisplaySubjectID);

	break;
	
	case "Reload_Topic_Selection":
		$SubjectID = $_POST['SubjectID'];
		$Level = $_POST['Level'];
		$PreviousLevelTopicID = $_POST['PreviousLevelTopicID'];
		$SelectionID = stripslashes($_POST['SelectionID']);
		$OnChange = stripslashes($_POST['OnChange']);
		$TopicID = $_POST['TopicID'];
		$IsMultiple = $_POST['IsMultiple'];
		$all = $_POST['all'];
		$noFirst = $_POST['noFirst'];
		$hasChooseOption = $_POST['hasChooseOption'];		
		$indicator = $_POST['indicator'];
		
		$SubjectIDArr = explode(",",$SubjectID);
	
		$lreportcard_ui = new libreportcardrubrics_ui();
		echo $lreportcard_ui->Get_Topic_Selection($SubjectIDArr, $Level, $PreviousLevelTopicID, $SelectionID, $TopicID, $OnChange, $all, $noFirst, $IsMultiple,$hasChooseOption,$indicator);		
	break;
	
}


intranet_closedb();
?>