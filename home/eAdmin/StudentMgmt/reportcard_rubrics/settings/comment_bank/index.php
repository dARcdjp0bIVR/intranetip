<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";
$currentTab = 'class';

if ($CommentType == 'subject') {
	header("Location: index_subject.php?clearCoo=1");
}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");


### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_comment_bank_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_comment_bank_page_number", "pageNo");
$arrCookies[] = array("ck_comment_bank_page_order", "order");
$arrCookies[] = array("ck_comment_bank_page_field", "field");	
$arrCookies[] = array("ck_comment_bank_page_keyword", "Keyword");

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

# tag information
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CommentBank_Tab($currentTab);

#Return Msg
$ReturnMsg = $Lang['General']['ReturnMessage'][$msg];
$linterface->LAYOUT_START($ReturnMsg);

$Keyword = (isset($_POST['Keyword']) && $_POST['Keyword'] != '')? trim(urldecode(stripslashes($_POST['Keyword']))) : $Keyword;

echo $lreportcard_ui->Get_Settings_CommentBank_UI($field, $order, $pageNo, $Keyword,'class');
?>

<script language="javascript">

$(document).ready( function() {
	$('input#Keyword').focus().keyup(function(e){
		var keynum = Get_KeyNum(e);
		
		if (keynum==13) // keynum==13 => Enter
			js_Reload_Comment_Table();
	});
});

function js_Go_Export()
{
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'export.php';
	jsFormObj.submit();
	
	jsFormObj.action = 'index.php';
}

function js_Reload_Comment_Table()
{
	jsKeyword = Trim($('input#Keyword').val());

	$('div#CommentTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Comment_Table',
			field: '<?=$field?>',
			order: '<?=$order?>',
			page: '<?=$pageNo?>',
			Keyword: jsKeyword,
			commentType: 'class'
		},
		function(ReturnData)
		{
			
		}
	);
}
</script>

<br/>
<?
        $linterface->LAYOUT_STOP();
		intranet_closedb();
?>