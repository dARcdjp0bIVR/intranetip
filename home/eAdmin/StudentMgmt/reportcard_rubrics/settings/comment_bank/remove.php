<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CommentBank";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_comment.php");

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_comment = new libreportcardrubrics_comment();

$CommentIDArr = (array)$_REQUEST['CommentID'];
$commentType = (array)$_REQUEST['commentType'];
if (is_array($commentType)) {
	$commentType = $commentType[0];
}


$Success = $lreportcard_comment->Remove_Comment_Bank_Comment($CommentIDArr);
$msg = $Success?"DeleteSuccess":"DeleteUnsuccess";

intranet_closedb();

$reutrnFile = '';
if ($commentType == 'subject') {
	$reutrnFile .= 'index_subject.php';
}
else {
	$reutrnFile .= 'index.php';
}
header("Location: ".$reutrnFile."?msg=$msg");
?>

