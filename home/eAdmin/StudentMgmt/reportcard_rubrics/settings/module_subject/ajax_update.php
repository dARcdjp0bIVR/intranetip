<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ModuleSubject";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$lreportcard->Start_Trans();
$ModuleSubjectArr = $_REQUEST['ModuleSubjectArr'];
$SuccessArr['Delete'] = $lreportcard->Delete_Module_Subject();
$SuccessArr['Save'] = $lreportcard->Save_Module_Subject($ModuleSubjectArr);

if (in_array(false, $SuccessArr))
{
	$lreportcard->RollBack_Trans();
	echo '0';
}
else
{
	$lreportcard->Commit_Trans();
	echo '1';
}

intranet_closedb();
?>