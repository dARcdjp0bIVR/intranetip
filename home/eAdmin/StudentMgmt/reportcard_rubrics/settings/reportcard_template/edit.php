<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ReportCardTemplate";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

$ReportID = is_array($ReportID)?$ReportID[0]:$ReportID;

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportCardTemplate']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Get_Setting_Report_Template_Edit_UI($ReportID);

?>
<br><br>
<script>
function CheckForm()
{
	if($("input#ReportTitleEn").val().Trim()=='')
	{
		alert("<?=$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleEn']?>")
		$("input#ReportTitleEn").focus();
		return false;
	}
	
	if($("input#ReportTitleCh").val().Trim()=='')
	{
		alert("<?=$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleCh']?>")
		$("input#ReportTitleCh").focus();
		return false;
	}
	
	return true;
	
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>