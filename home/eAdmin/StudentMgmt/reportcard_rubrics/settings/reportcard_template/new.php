<?php

$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ReportCardTemplate";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['ReportCardTemplate']);
$linterface->LAYOUT_START();

//echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Setting_Report_Template_Edit_UI();

?>
<br><br>
<script>
function ReportTypeChange()
{
	if($("input.ReportTypeRadio:checked").val()=='T')
	{
		$(".ModuleMember").attr("disabled","");
		$(".ConsolidatedMember").attr("disabled","disabled");
	}
	else
	{
		$(".ModuleMember").attr("disabled","disabled");
		$(".ConsolidatedMember").attr("disabled","");
	}
}

function CheckAll(obj)
{
	if(obj.checked)
	{
		$("input.ConsolidatedMember").attr("checked","checked");
	}
	else
	{
		$("input.ConsolidatedMember").attr("checked","");
	}
}

function UncheckCheckAll(obj)
{
	if(!obj.checked)
		$("input#CheckAllModule").attr("checked","");
}

function CheckForm()
{
	if($("input#ReportTitleEn").val().Trim()=='')
	{
		alert("<?=$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleEn']?>")
		$("input#ReportTitleEn").focus();
		return false;
	}
	
	if($("input#ReportTitleCh").val().Trim()=='')
	{
		alert("<?=$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['EmptyArr']['ReportTitleCh']?>")
		$("input#ReportTitleCh").focus();
		return false;
	}
	
	if($("input.ReportTypeRadio:checked").val()=='W' )
	{
		if($("input.ConsolidatedMember:checked").length==0)
		{
			alert("<?=$Lang['eRC_Rubrics']['SettingsArr']['TemplateArr']['jsWarningArr']['SelectModule']?>")
			return false;
		}
	}
	
	return true;
	
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>