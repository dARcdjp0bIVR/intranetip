<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ReportCardTemplate";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$ModuleIDArr = $_REQUEST['ModuleID'];
$ReportTitleEn = trim(stripslashes($_REQUEST['ReportTitleEn']));
$ReportTitleCh = trim(stripslashes($_REQUEST['ReportTitleCh'])); 
$ReportType = $_REQUEST['ReportType']; 

$lreportcard->Start_Trans();

$ReportID = $lreportcard->InsertReportTemplate($ReportTitleEn, $ReportTitleCh, $ReportType);
$Success["InsertTemplate"] = $ReportID?true:false;

$Success["InsertTemplateModuleMap"] = $lreportcard->InsertReportTemplateModuleMapping($ReportID, $ModuleIDArr);

if(in_array(false,$Success))
{
	$lreportcard->RollBack_Trans();	
	$msg = 'AddUnsuccess'; 
}
else
{
	$lreportcard->Commit_Trans();
	$msg = 'AddSuccess'; 	
}

intranet_closedb();

header("location: reportcard_template.php?msg=$msg");
?>