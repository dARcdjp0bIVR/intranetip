<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_AdminGroup";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");


### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle']);

$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$ReturnMsg = $Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

# From Edit
$AdminGroupIDArr = $_REQUEST['AdminGroupIDArr'];
if (is_array($AdminGroupIDArr))
	$AdminGroupID = $AdminGroupIDArr[0];
else
	$AdminGroupID = $AdminGroupIDArr;
	
//echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Setting_Admin_Group_Step1_UI($AdminGroupID);

?>
<br><br>

<script language="javascript">

var jsWarmMsgArr = new Array();
jsWarmMsgArr["AdminGroupNameEn"] = "<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>";
jsWarmMsgArr["AdminGroupNameCh"] = "<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>";
jsWarmMsgArr["AdminGroupCode"] = "<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Code']?>";
jsWarmMsgArr["CodeInUse"] = "<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code']?>";

$(document).ready( function() {	
	
});


function js_Update_Group_Info()
{
	var jsAdminGroupID = $('input#AdminGroupID').val();
	var jsFormValid = true;
	
	
	// check info empty
	$("input.required").each(function(){
		var thisID = $(this).attr("id");
		var WarnMsgDivID = thisID + 'WarningDiv';

		if($(this).val().Trim() == '')
		{
			$("div#" + WarnMsgDivID).html(jsWarmMsgArr[thisID]).show();
			$("input#" + thisID).focus();
			jsFormValid = false;
		}
		else
		{
			$("div#" + WarnMsgDivID).hide();
		}
	});
	
	// ajax check code
	var jsCode = $("input#AdminGroupCode").val().Trim();
	if(jsCode != '')
	{
		if(valid_code(jsCode) == false)
		{
			$("div#AdminGroupCodeWarningDiv").html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>').show();
			$("input#AdminGroupCode").focus();
			jsFormValid = false;
		}
		else
		{
			$.post(
				"ajax_validate.php",
				{
					Action: "AdminGroupCode",
					AdminGroupCode: jsCode,
					AdminGroupID: jsAdminGroupID
				},
				function (ReturnData)
				{
					if(ReturnData==1)
					{
						$("div#AdminGroupCodeWarningDiv").hide();
						$("form#form1").attr('action', 'new_step1_update.php').submit();
					}
					else
					{
						$("div#AdminGroupCodeWarningDiv").html(jsWarmMsgArr["CodeInUse"]).show();
						$("input#AdminGroupCode").focus();
						jsFormValid = false;
					}
				}
			);
		}
	}
}

function js_Back_To_Admin_Group_List()
{
	window.location = 'admin_group.php';
}

function js_Select_All_Checkboxes(jsClass)
{
	$('input.' + jsClass).attr('checked', $('input#' + jsClass + '_Chk').attr('checked'));
}

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>