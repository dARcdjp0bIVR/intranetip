<?php
// using ivan
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$TargetUserType = 1;

# Get data
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$AdminGroupID = $_REQUEST['AdminGroupID'];
$SearchValue = stripslashes(urldecode($_REQUEST['q']));

### Get Select User and exclude them in the search result
$SelectedUserArr = explode(',', $SelectedUserIDList);
$ExcludeUserIDArr = Get_User_Array_From_Common_Choose($SelectedUserArr, $TargetUserType);

### Get User who can be added to the Admin Group
$AvailableUserInfoArr = $lreportcard->Get_Admin_Group_Available_User($AdminGroupID, $SearchValue, $ExcludeUserIDArr, $TargetUserType);
$numOfAvailableUser = count($AvailableUserInfoArr);

$returnString = '';
for ($i=0; $i<$numOfAvailableUser; $i++)
{
	$thisUserID = $AvailableUserInfoArr[$i]['UserID'];
	$thisUserLogin = $AvailableUserInfoArr[$i]['UserLogin'];
	$thisUserName = $AvailableUserInfoArr[$i]['UserName'];
	
	$returnString .= $thisUserName.'|'.$thisUserLogin.'|'.$thisUserID."\n";
}

intranet_closedb();

echo $returnString;
?>