<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_AdminGroup";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");


### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_settings_admin_group_page_size", "numPerPage");
$arrCookies[] = array("ck_settings_admin_group_page_number", "pageNo");
$arrCookies[] = array("ck_settings_admin_group_page_order", "order");
$arrCookies[] = array("ck_settings_admin_group_page_field", "field");
$arrCookies[] = array("ck_settings_admin_group_page_keyword", "Keyword");

$numPerPage = (isset($_POST['numPerPage']))? $_POST['numPerPage'] : $_COOKIE['numPerPage'];
$pageNo = (isset($_POST['pageNo']))? $_POST['pageNo'] : $_COOKIE['pageNo'];
$order = (isset($_POST['order']))? $_POST['order'] : $_COOKIE['order'];
$field = (isset($_POST['field']))? $_POST['field'] : $_COOKIE['field'];
$Keyword = (isset($_POST['Keyword']))? $_POST['Keyword'] : $_COOKIE['Keyword'];

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$ck_settings_admin_group_page_size = '';
	$Keyword = '';
}
else 
	updateGetCookies($arrCookies);

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle']);

#return msg
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$ReturnMsg = $Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

//echo $lreportcard_ui->Include_JS_CSS(array('cookies'));
echo $lreportcard_ui->Get_Setting_Admin_Group_UI($pageNo, $order, $field, $Keyword);

?>
<br><br>

<script language="javascript">

$(document).ready( function() {
	
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>