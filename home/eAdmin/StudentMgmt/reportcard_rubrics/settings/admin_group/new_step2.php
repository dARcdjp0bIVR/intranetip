<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_AdminGroup";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");

### Check Access Right
$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['MenuTitle']);

$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$ReturnMsg = $Lang['eRC_Rubrics']['SettingsArr']['AdminGroupArr']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);

$AdminGroupID = $_REQUEST['AdminGroupID'];
$FromNew = $_REQUEST['FromNew'];


echo $lreportcard_ui->Include_JS_CSS(array('autocomplete'));
echo $lreportcard_ui->Get_Setting_Admin_Group_Step2_UI($AdminGroupID, $FromNew);

?>
<br><br>

<script language="javascript">

var jsAdminGroupID = '';
$(document).ready( function() {	
	// initialize jQuery Auto Complete plugin
	jsAdminGroupID = $('input#AdminGroupID').val();
	Init_JQuery_AutoComplete('UserSearchTb');
	
	$('input#UserSearchTb').focus();
});


var AutoCompleteObj;
function Init_JQuery_AutoComplete(InputID) {
	AutoCompleteObj = $("input#" + InputID).autocomplete("ajax_search_user.php",
		{  			
			onItemSelect: function(li) {
				Add_Selected_User(li.extra[1],li.selectValue);
			},
			formatItem: function(row) {
				return row[0] + " (LoginID: " + row[1] + ")";
			},
			maxItemsToShow: 5,
			minChars: 1,
			delay: 0,
			width: 200
		}
	);
	
	Update_Auto_Complete_Extra_Para();
}

function Add_Selected_User(UserID, UserName) {
	var UserID = UserID || "";
	var UserName = UserName || "";
	var UserSelected = document.getElementById('SelectedUserIDArr[]');

	UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
	Update_Auto_Complete_Extra_Para();
	
	$('input#UserSearchTb').val('').focus();
}

function Update_Auto_Complete_Extra_Para() {
	ExtraPara = new Array();
	ExtraPara['SelectedUserIDList'] = Get_Selection_Value('SelectedUserIDArr[]', 'Array', true);
	ExtraPara['AdminGroupID'] = jsAdminGroupID;
	AutoCompleteObj[0].autocompleter.setExtraParams(ExtraPara);
}

function js_Add_Member()
{
	var objForm = document.getElementById('form1');
	
	// Check if the selections contains current members of this group
	var jsSelectedUserList = Get_Selection_Value('SelectedUserIDArr[]', 'String', true);
	
	$.post(
		"ajax_validate.php", 
		{
			Action: "Validate_Selected_Member", 
			AdminGroupID : jsAdminGroupID,
			SelectedUserList : jsSelectedUserList
		},
		function(ReturnData)
		{
			if (ReturnData == '')
			{
				$('div#MemberSelectionWarningDiv').hide();
				checkOptionAll(objForm.elements["SelectedUserIDArr[]"]);
				objForm.action = 'new_step2_update.php';
				objForm.submit();
			}
			else
			{
				var OptionValueArr = ReturnData.split(',');
				var numOfOption = OptionValueArr.length;
				
				for (i=0; i<numOfOption; i++)
					$("select#SelectedUserIDArr\\[\\] option[value='" + OptionValueArr[i] + "']").attr('selected', 'selected');
					 
				$('div#MemberSelectionWarningDiv').show();
			}
		}
	);
}

function js_Back_To_Admin_Group_List()
{
	window.location = 'admin_group.php';
}

function js_Back_To_Member_List()
{
	var objForm = document.getElementById('form1');
	objForm.action = 'member_list.php';
	objForm.submit();
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>