<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
$lreportcard_ui = new libreportcardrubrics_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Admin_Group_Table')
{
	$pageNo = $_POST['PageNumber'];
	$order = $_POST['Order'];
	$field = $_POST['SortField'];
	$Keyword = trim(urldecode(stripslashes($_POST['Keyword'])));
	
	echo $lreportcard_ui->Get_Setting_Admin_Group_DBTable($pageNo, $order, $field, $Keyword);
}

intranet_closedb();
?>