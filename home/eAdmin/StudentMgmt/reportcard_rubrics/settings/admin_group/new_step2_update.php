<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_AdminGroup";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

# Get data
$AdminGroupID = $_REQUEST['AdminGroupID'];
$SelectedUserIDArr = $_REQUEST['SelectedUserIDArr'];
$FromNew = $_REQUEST['FromNew'];

$Success = $lreportcard->Add_Admin_Group_Member($AdminGroupID, $SelectedUserIDArr);

intranet_closedb();

$NextPage = ($FromNew)? "admin_group.php" : "member_list.php";
$ReturnMsgKey = ($Success)? 'AddMemberSuccess' : 'AddMemberFailed';

$para = "AdminGroupID=$AdminGroupID&ReturnMsgKey=$ReturnMsgKey";
header("Location: ".$NextPage.'?'.$para);
?>