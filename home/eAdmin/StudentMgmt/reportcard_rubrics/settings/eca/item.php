<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ECA";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();
$lreportcard_eca = new libreportcardrubrics_eca();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Setting_ECA_Item_UI($EcaCategoryID, $EcaSubCategoryID);

?>
<br><br>
<script>
function js_Reload_Item_Table()
{
	var EcaCategoryID = $("select#EcaCategoryID").val();
	var EcaSubCategoryID = $("select#EcaSubCategoryID").val();
	$.post(
		"ajax_reload.php",
		{
			Action: "LoadItemTable",
			EcaCategoryID:EcaCategoryID,
			EcaSubCategoryID:EcaSubCategoryID
		},
		function(ReturnData)
		{
			$("td#ItemTableTD").html(ReturnData);
			initThickBox();
			js_Init_DND_Table();
		}
	);
	
}

function js_Add_Item(EcaSubCategoryID,msg)
{
	js_Edit_Item(EcaSubCategoryID,'',msg);
}

function js_Edit_Item(EcaSubCategoryID, EcaItemID,msg)
{
	var EcaItemID = EcaItemID?EcaItemID:'';
	
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadItemEditLayer",
			EcaItemID:EcaItemID,
			EcaSubCategoryID:EcaSubCategoryID
		},
		function (ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			if(msg)
				Get_Return_Message(msg);
		}
		
	)
}

function DeleteItem(EcaItemID)
{
	if(confirm("<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteItem']?>"))
	{
		$.post
		(
			"ajax_update.php",
			{
				Action:"DeleteEcaItem",
				EcaItemID: EcaItemID
			},
			function (ReturnData)
			{
				Get_Return_Message(ReturnData);	
				js_Reload_Item_Table();
			}
		)
	}
}
function CheckTBForm(AddMore)
{
	var FormValid = true;
	
	var WarmMsgArr = new Array();
	WarmMsgArr["EcaItemNameEn"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemNameEn']?>";
	WarmMsgArr["EcaItemNameCh"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemNameCh']?>";
	WarmMsgArr["EcaItemCode"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaItemCode']?>";
	
	// check empty
	$("input.required").each(function(){
		var thisID = $(this).attr("id");
		var WarnMsgDivID = "Warn"+thisID;

		$("div#"+WarnMsgDivID).html("").show()
		if($(this).val().Trim() == '')
		{
			$("div#"+WarnMsgDivID).html(WarmMsgArr[thisID]);
			FormValid = false;
		}
	});
	
	// ajax check rubrics code
	if($("input#EcaItemCode").val().Trim()!='')
	{
		var EcaItemCode = $("input#EcaItemCode").val();
		var EcaItemID = $("input#TBEcaItemID").val();
		
		if(!valid_code(EcaItemCode))
		{
			$("div#WarnEcaItemCode").html("<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>").show();
		}
		else
		{
			$.post
			(
				"ajax_validate.php",
				{
					Action:"ValidateEcaItemCode",
					EcaItemID: EcaItemID,
					EcaItemCode: EcaItemCode
				},
				function (ReturnData)
				{
					if(ReturnData==1)
					{
						
						$("div#WarnEcaItemCode").hide();
						if(FormValid == true)
						{
							SubmitTBForm(AddMore);
						}
					}
					else
					{
						$("div#WarnEcaItemCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['ItemCodeNotAvailible']?>").show();
						FormValid = false;
					}			
				}
			)
		}
	}
	
	return false;
	
}

function SubmitTBForm(AddMore)
{
	var EcaSubCategoryID = $("input#TBEcaSubCategoryID").val();
	var DisplayOrder = parseInt($("input#MaxDisplayOrder_"+EcaSubCategoryID).val())+1;
	var jsSubmitString = Get_Form_Values(document.TBForm);
	jsSubmitString += '&Action=SaveItemInfo';
	jsSubmitString += '&DisplayOrder='+DisplayOrder;
	
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			if(AddMore)
				js_Add_Item(EcaSubCategoryID,ReturnData)
			else
				js_Hide_ThickBox();
			js_Reload_Item_Table();
		} 
	});
}

function updateItemDisplayOrder(tbodyclass)
{
	var jsSubmitString = $("tbody."+tbodyclass).find("input.EcaItemID").serialize()
	jsSubmitString += '&Action=UpdateItemDisplayOrder';

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
			js_Reload_Item_Table();
		} 
	});
}

function js_Reload_Cat_Selection()
{
	var EcaCategoryID = $("select#EcaCategoryID").val();
	$.post
	(
		"../../ajax_reload.php",
		{
			Action:"LoadSubCategorySelection",
			EcaCategoryID: EcaCategoryID
		},
		function (ReturnData)
		{
			$("span#SubCategorySelectionSpan").html(ReturnData);
			js_Reload_Item_Table();
		}
	)
}

// jQuery Plugin
function js_Init_DND_Table() {
		
	var JQueryObj = $("table#ItemTable");
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			var tbodyclass = $(DroppedRow).parent().attr("class")
			updateItemDisplayOrder(tbodyclass);
		},
		onDragStart: function(table, DraggedRow) {
			
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	/*** End of Room Reordering in the index page ***/
}

$().ready(function(){
	js_Reload_Item_Table();
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>