<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_ECA";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_eca.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();
$lreportcard_eca = new libreportcardrubrics_eca();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Setting_ECA_Category_UI();

?>
<br><br>
<script>
function js_Edit_Category(msg)
{
	
	
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadECACategoryEditLayer"
		},
		function (ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			if(msg)
				Get_Return_Message(msg);
			js_Init_DND_Table()
			js_Init_Editable();
		}
	)
}

function js_Add_Category()
{
	if($("tr.new_row").length==0)
	{
		var jsRow = '<?=$lreportcard_ui->Get_Setting_ECA_Category_Edit_Layer_New_Row();?>';
		$("tr.add_row").before(jsRow);
	}
}

function js_Cancel_Add_Category()
{
	$("tr.new_row").remove();
}

function js_Validate_Category()
{
	var FormValid = true;
	$("tr.new_row>td>input.required").each(function()
	{
		$("div#Warn"+$(this).attr("id")).hide()
		if($(this).val().Trim()=='')
		{
			$("div#Warn"+$(this).attr("id")).show()
			FormValid = false;
		}
	});
	
	if($("input#EcaCategoryCode").val().Trim()!='')
	{
		
		var EcaCategoryCode=$("input#EcaCategoryCode").val();
		
		if(!valid_code(EcaCategoryCode))
		{
			$("div#WarnEcaCategoryCode").html("<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>").show()
		}
		else
		{
			$.post(
				"ajax_validate.php",
				{
					Action:"ValidateEcaCategoryCode",
					EcaCategoryCode:EcaCategoryCode
				},
				function (ReturnData)
				{
					if(ReturnData==1)
					{
						if(FormValid)
							js_Save_Category();
					}
					else
						$("div#WarnEcaCategoryCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['ECACodeNotAvailible']?>").show()
				}
			)		
		}	
	}
	
	return false;

}

function js_Save_Category()
{
	var EcaCategoryCode=$("input#EcaCategoryCode").val();
	var EcaCategoryNameEn=$("input#EcaCategoryNameEn").val();
	var EcaCategoryNameCh=$("input#EcaCategoryNameCh").val();
	var DisplayOrder=parseInt($("input#MaxDisplayOrder").val())+1;

	Block_Element("TB_ajaxContent");	
	$.post(
		"ajax_update.php",
		{
			Action:"SaveCategoryInfo",
			EcaCategoryCode:EcaCategoryCode,
			EcaCategoryNameEn:EcaCategoryNameEn,
			EcaCategoryNameCh:EcaCategoryNameCh,
			DisplayOrder:DisplayOrder
		},
		function (ReturnData)
		{
//			js_Hide_ThickBox();
			js_Edit_Category(ReturnData);
			refreshCategoryTable();
			UnBlock_Element();	
		}
	)
}


function DeleteCategory(EcaCategoryID, SubCatExist)
{
	if(SubCatExist==1)
		warnMsg = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteCategoryWithSubCategory']?>"; 
	else
		warnMsg = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteCategory']?>";	
	if(confirm(warnMsg))
	{
		$.post
		(
			"ajax_update.php",
			{
				Action:"DeleteEcaCategory",
				EcaCategoryID: EcaCategoryID
			},
			function (ReturnData)
			{
				Get_Return_Message(ReturnData);
				if(ReturnData.substring(0,1)==1)
				{	
					$("#cat_row_"+EcaCategoryID).remove();
					refreshCategoryTable();
				}
			}
		)
	}
}

function updateCategoryDisplayOrder()
{
	var jsSubmitString = $("input.EcaCategoryID").serialize()
	jsSubmitString += '&Action=UpdateCategoryDisplayOrder';
	
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
		} 
	});
}

// Subcategory
function js_Add_SubCategory(EcaCategoryID,msg)
{
	js_Edit_SubCategory(EcaCategoryID,'',msg);
}

function js_Edit_SubCategory(EcaCategoryID,EcaSubCategoryID,msg)
{
	var EcaSubCategoryID = EcaSubCategoryID?EcaSubCategoryID:'';
	
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadSubCategoryEditLayer",
			EcaCategoryID:EcaCategoryID,
			EcaSubCategoryID:EcaSubCategoryID
		},
		function (ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			if(msg)
				Get_Return_Message(msg);
			js_Init_DND_SubCategoryTable();
		}
		
	)
}

function DeleteSubCategory(EcaSubCategoryID,ItemExist)
{
	if(ItemExist==1)
		warnMsg = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteSubCategoryWithItem']?>"; 
	else
		warnMsg = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsConfirmArr']['DeleteSubCategory']?>";	
	
	if(confirm(warnMsg))
	{
		$.post
		(
			"ajax_update.php",
			{
				Action:"DeleteEcaSubCategory",
				EcaSubCategoryID: EcaSubCategoryID
			},
			function (ReturnData)
			{
				Get_Return_Message(ReturnData);	
				refreshCategoryTable();
			}
		)
	}
}


function CheckTBForm(AddMore)
{
	var FormValid = true;
	
	var WarmMsgArr = new Array();
	WarmMsgArr["EcaCategoryNameEn"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryNameEn']?>";
	WarmMsgArr["EcaCategoryNameCh"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryNameCh']?>";
	WarmMsgArr["EcaCategoryCode"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['EmptyArr']['EcaCategoryCode']?>";
	
	// check empty
	$("input.required").each(function(){
		var thisID = $(this).attr("id");
		var WarnMsgDivID = "Warn"+thisID;

		$("div#"+WarnMsgDivID).html("").show()
		if($(this).val().Trim() == '')
		{
			$("div#"+WarnMsgDivID).html(WarmMsgArr[thisID]);
			FormValid = false;
		}
	});
	
	// ajax check rubrics code
	if($("input#EcaCategoryCode").val().Trim()!='')
	{
		var EcaCategoryCode = $("input#EcaCategoryCode").val();
		var EcaSubCategoryID = $("input#EcaSubCategoryID").val();
		
		$("div#WarnEcaCategoryCode").hide();
		if(!valid_code(EcaCategoryCode))
		{
			$("div#WarnEcaCategoryCode").html("<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>").show()
		}
		else
		{
		
			$.post
			(
				"ajax_validate.php",
				{
					Action:"ValidateEcaSubCategoryCode",
					EcaSubCategoryCode: EcaCategoryCode,
					EcaSubCategoryID: EcaSubCategoryID
				},
				function (ReturnData)
				{
					if(ReturnData==1)
					{
						
						if(FormValid == true)
						{
							SubmitTBForm(AddMore);
						}
					}
					else
					{
						$("div#WarnEcaCategoryCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ECAArr']['jsWarningArr']['ECACodeNotAvailible']?>").show();
						FormValid = false;
					}			
				}
			)
		}
	}
	
	return false;
	
}

function SubmitTBForm(AddMore)
{
	var EcaCategoryID = $("input#EcaCategoryID").val();
	var DisplayOrder = parseInt($("input#MaxDisplayOrder_"+EcaCategoryID).val())+1;
	var jsSubmitString = Get_Form_Values(document.TBForm);
	jsSubmitString += '&Action=SaveSubCategoryInfo';
	jsSubmitString += '&DisplayOrder='+DisplayOrder;
	
	Block_Element("TB_ajaxContent");
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			if(AddMore)
				js_Add_SubCategory(EcaCategoryID,ReturnData)
			else
				js_Hide_ThickBox();

			UnBlock_Element();
			refreshCategoryTable();
		} 
	});
}

function updateSubCategoryDisplayOrder(tbodyclass)
{
	var jsSubmitString = $("tbody."+tbodyclass).find("input.EcaSubCategoryID").serialize()
	jsSubmitString += '&Action=UpdateSubCategoryDisplayOrder';

	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			Get_Return_Message(ReturnData);
		} 
	});
}

function refreshCategoryTable()
{
	$.post
	(
		"ajax_reload.php",
		{
			Action:"LoadECACategoryTable"
		},
		function(ReturnData)
		{
			$("#CategoryTableTD").html(ReturnData);
			initThickBox();
			js_Init_DND_SubCategoryTable();
		}
		
	)
}

// Jquery Plugin
function js_Init_DND_Table() {
		
	var JQueryObj = $("table#CategoryTable");
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			updateCategoryDisplayOrder();
			refreshCategoryTable()
		},
		onDragStart: function(table, DraggedRow) {
			
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	/*** End of Room Reordering in the index page ***/
}

function js_Init_DND_SubCategoryTable() {
		
	
	var JQueryObj = $("table#SubCategoryTable");
	
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			var tbodyclass = $(DroppedRow).parent().attr("class")
			updateSubCategoryDisplayOrder(tbodyclass);
			refreshCategoryTable()
		},
		onDragStart: function(table, DraggedRow) {
			
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	/*** End of Room Reordering in the index page ***/
}

var EditableOriValue='';
function js_Init_Editable()
{
     $('.edit').editable('ajax_update.php?Action=EcaCategoryEditableUpdate', {
        indicator 	:  '<img src="/images/2009a/indicator.gif">',
		tooltip   	: "<?=$Lang['SysMgr']['Location']['ClickToEdit']?>",
		event 		: "click",
		onblur 		: "submit",
		type 		: "text",
		style  		: "display:inline",
		cssclass	: "textboxtext",
		maxlength  	: "255",
		height		: "20px",
		width		: "75px",
		select		: true,
        callback 	: function() {Editable_Callback(this)},
        onedit		: 
        	function() 
        	{
        		var InputValue= $(this).html();
     			EditableOriValue = InputValue;
     		}
     });
     
}

function Editable_Callback(obj)
{
	var returnData = $(obj).html();
	var returnDataAry = returnData.split("|=msg=|");

	Get_Return_Message(returnDataAry[1])

	if(returnDataAry[1].substring(0,1)==1)
	{
		$(obj).html(returnDataAry[0]);
		refreshCategoryTable();
	}else
		$(obj).html(EditableOriValue);
}

function Show_Edit_Background(Obj) {
	Obj.style.backgroundImage = "url(<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_edit_b.gif)";
	Obj.style.backgroundPosition = "center right";
	Obj.style.backgroundRepeat = "no-repeat";
}

function Hide_Edit_Background(Obj) {
	Obj.style.backgroundImage = "";
	Obj.style.backgroundPosition = "";
	Obj.style.backgroundRepeat = "";
}

$().ready(function(){
	js_Init_DND_SubCategoryTable();
});
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>