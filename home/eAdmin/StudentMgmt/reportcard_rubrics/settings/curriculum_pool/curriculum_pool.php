<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CurriculumPool";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();


############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CurriculumPool_Tab_Array('CurriculumPool');
$linterface->LAYOUT_START();

## Add/Edit Building Form Create & Reset Button ##
$btnAddTopic = str_replace("'", "\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button", "js_Insert_Topic();", "NewTopicSubmitBtn"));
$btnCancelTopic = str_replace("'", "\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "js_Clear_Temp_Topic_Info();", "NewTopicCancelBtn"));

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Settings_CurriculumPool_UI();
?>

<script language="javascript">
var jsCurSubjectID = '';
var jsCurYearID = '';
var jsMaxTopicLevel = <?=$eRC_Rubrics_ConfigArr['MaxLevel']?>;
var jsCurLayerTopicLevel = '';

var arrCookies = new Array();
arrCookies[arrCookies.length] = "SubjectID";
arrCookies[arrCookies.length] = "YearID";

var jsLevelSelectionStartIndex = arrCookies.length;
for (i=1; i<jsMaxTopicLevel; i++)
	arrCookies[arrCookies.length] = 'Level' + i + '_TopicID';

$(document).ready( function() {	
	<? if($clearCoo) { ?>
		for(i=0; i<arrCookies.length; i++)
		{
			var obj = arrCookies[i];
			//alert('obj = ' + obj);
			$.cookies.del(obj);
		}
	<? } ?>
	
	Blind_Cookies_To_Object();
	
	// Initialize Subject Selection
	var jsDefaultSubjectID = $.cookies.get(arrCookies[0]);
	if (jsDefaultSubjectID == null || jsDefaultSubjectID == '')
		jsDefaultSubjectID = $('select#SubjectID option:nth-child(1)').val();
	$('select#SubjectID').val(jsDefaultSubjectID);
	jsCurSubjectID = jsDefaultSubjectID;
	
	// Initialize Form Selection
	var jsDefaultYearID = $.cookies.get(arrCookies[1]);
	if (jsDefaultYearID == null || jsDefaultYearID == '')
		jsDefaultYearID = $('select#YearID option:nth-child(1)').val();
	$('select#YearID').val(jsDefaultYearID);
	jsCurYearID = jsDefaultYearID;
	
	// Initialize Topic Filtering
	var jsThisLevel;
	var jsThisCookieIndex;
	var jsThisTopicID;
	var jsThisPreviousLevelTopicID;
	var jsDefaultFilterTopicInfoText = '';
	for (i=1; i<jsMaxTopicLevel; i++)
	{
		jsThisLevel = i;
		jsThisCookieIndex = jsThisLevel + jsLevelSelectionStartIndex - 1;
		jsThisTopicID = $.cookies.get(arrCookies[jsThisCookieIndex]);
		jsThisPreviousLevelTopicID = (jsThisLevel==1)? '' : $.cookies.get(arrCookies[jsThisCookieIndex-1]);
		
		// Skip the initialization of the selection if the previous level selection has not selected before (except the first topic level selection is always loaded)
		if (jsThisPreviousLevelTopicID == null && jsThisLevel > 1)
			continue;
		
		// Reload the Topic selection
		js_Reload_Individual_Topic_Selection(jsThisLevel, jsThisPreviousLevelTopicID, jsThisTopicID);
		
		// Do not record the filtering if not selected before
		if (jsThisTopicID != null)
			jsDefaultFilterTopicInfoText += jsThisLevel + ',' + jsThisTopicID + '::';
	}
	
	// Reload Table
	js_Reload_Subject_Curriculum_Table(jsDefaultFilterTopicInfoText);
});


function js_Changed_Subject_Selection(jsSubjectID)
{
	jsCurSubjectID = jsSubjectID;
	js_Reload_Topic_Selection(jsLevel=1, '');
}

function js_Changed_Form_Selection(jsYearID)
{
	jsCurYearID = jsYearID;
	js_Reload_Subject_Curriculum_Table();
}

function js_Reload_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID, jsReloadTable)
{
	if (jsTargetTopicID == null)
		jsTargetTopicID = '';
		
	if (jsReloadTable == null)
		jsReloadTable = 1;
		
	// Hide the all Next Level Topic Selection
	var i;
	for (i=jsLevel; i<jsMaxTopicLevel; i++)
	{
		$('div#Level' + i + '_SelectionDiv').html('');
	}
	
	if (jsLevel < jsMaxTopicLevel)
	{
		if (jsLevel==1 || (jsLevel > 1 && jsPreviousLevelTopicID != ''))
			js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID);
	}
	
	if (jsReloadTable==1)
		js_Reload_Subject_Curriculum_Table();
}

function js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsTargetTopicID)
{
	var jsNextLevel = parseInt(jsLevel) + 1;
	$('div#Level' + jsLevel + '_SelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../ajax_reload.php", 
		{
			Action: 'Reload_Topic_Selection',
			SubjectID: jsCurSubjectID,
			Level: jsLevel,
			PreviousLevelTopicID: jsPreviousLevelTopicID,
			SelectionID: 'Level' + jsLevel + '_TopicID',
			OnChange: 'js_Reload_Topic_Selection(' + jsNextLevel + ', this.value);',
			TopicID: jsTargetTopicID
		},
		function(ReturnData)
		{
			
			Blind_Cookies_To_Object();
		}
	);
}

function js_Reload_Subject_Curriculum_Table(jsFilterTopicInfoText)
{
	jsFilterTopicInfoText = (jsFilterTopicInfoText == null || jsFilterTopicInfoText == '')? js_Get_Filtering_Info_Text() : jsFilterTopicInfoText;		// jsFilterTopicInfoText = 'Level,TopicID::Level,TopicID::'
	
	$('div#SubjectCurriculumDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Subject_Curricumlum_Table',
			SubjectID: jsCurSubjectID,
			YearID: jsCurYearID,
			FilterTopicInfoText: jsFilterTopicInfoText
		},
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// moved to reportcard_rubrics.js
//function js_Get_Filtering_Info_Text()
//{
//	var jsFilterTopicInfoText = '';		// jsFilterTopicInfoText = 'Level,TopicID::Level,TopicID::'
//	var jsTmpTopicID = '';
//	var jsTmpInfoText = '';
//	for (i=1; i<jsMaxTopicLevel; i++)
//	{
//		jsTmpTopicID = $('select#Level' + i + '_TopicID').val();
//		
//		if (jsTmpTopicID != '' && jsTmpTopicID != null)
//		{
//			jsTmpInfoText = i + ',' + jsTmpTopicID + '::';
//			jsFilterTopicInfoText += jsTmpInfoText;
//		}
//		else
//			break;
//	}
//	
//	return jsFilterTopicInfoText;
//}

function js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, jsReturnMsg, jsReloadTopicSeletcion)
{
	if (jsReloadTopicSeletcion==null)
		jsReloadTopicSeletcion = 0;
		
	jsCurLayerTopicLevel = jsLevel;
		
	$.post(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Topic_Settings_Layer',
			SubjectID: jsCurSubjectID,
			TopicID: jsTopicID,
			Level: jsLevel,
			ParentTopicID: jsParentTopicID,
			PreviousLevelTopicID: jsPreviousLevelTopicID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			js_Init_DND_Table();
			js_Init_JEdit();
			
			if (jsReloadTopicSeletcion == 1)
			{
				var jsLevelTopicSelectionVal = $('select#Level' + jsLevel + '_TopicID').val();
				js_Reload_Individual_Topic_Selection(jsLevel, jsPreviousLevelTopicID, jsLevelTopicSelectionVal, 0);
			}
			
			if (jsReturnMsg != null && jsReturnMsg != '')
				Get_Return_Message(jsReturnMsg);
		}
	);
}


function js_Add_Temp_Topic_Row() {
	if (!document.getElementById('GenAddTopicRow')) {
		var TableBody = document.getElementById("TopicInfoTable").tBodies[0];
		var RowIndex = document.getElementById("AddTopicRow").rowIndex - 1;
		//var RowIndex = 1;
		var NewRow = TableBody.insertRow(RowIndex);
		NewRow.id = "GenAddTopicRow";
		
		// Code
		var NewCell0 = NewRow.insertCell(0);
		var tempInput = '<input type="text" name="TopicCode" id="TopicCode" size="12" value="" class="textbox" onkeydown="js_Check_Press_Enter_Insert_Topic(event);" maxlength="<?=$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['Code']?>" />';
			tempInput += '<div id="CodeWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell0.innerHTML = tempInput;
		
		// Title (Eng)
		var NewCell1 = NewRow.insertCell(1);
		var tempInput = '<input type="text" name="TopicNameEn" id="TopicNameEn" value="" class="textbox" onkeydown="js_Check_Press_Enter_Insert_Topic(event);" maxlength="<?=$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['NameEn']?>" />';
			tempInput += '<div id="NameEnWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell1.innerHTML = tempInput;
		
		// Title (Chi)
		var NewCell2 = NewRow.insertCell(2);
		var tempInput = '<input type="text" name="TopicNameCh" id="TopicNameCh" value="" class="textbox" onkeydown="js_Check_Press_Enter_Insert_Topic(event);" maxlength="<?=$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['NameCh']?>" />';
			tempInput += '<div id="NameChWarningDiv_New" style="display:none;color:red;font-size:10px;">';
			tempInput += '</div>';
		NewCell2.innerHTML = tempInput;
		
		if (jsCurLayerTopicLevel == 1)
		{
			// Remarks
			var NewCell3 = NewRow.insertCell(3);
			var tempInput = '<textarea name="TopicRemarks" id="TopicRemarks" class="tabletext" wrap="virtual"></textarea>';
				tempInput += '<div id="NameChWarningDiv_New" style="display:none;color:red;font-size:10px;">';
				tempInput += '</div>';
			NewCell3.innerHTML = tempInput;
		
			// Empty Cell
			var NewCell4 = NewRow.insertCell(4);
			var temp = '&nbsp;';
			NewCell4.innerHTML = temp;
			
			// Add and Reset Buttons
			var NewCell5 = NewRow.insertCell(5);
			var temp = '<?=$btnAddTopic?>';
			temp += '&nbsp;';
			temp += '<?=$btnCancelTopic?>';
			NewCell5.innerHTML = temp;
		}
		else
		{
			// Empty Cell
			var NewCell3 = NewRow.insertCell(3);
			var temp = '&nbsp;';
			NewCell3.innerHTML = temp;
			
			// Add and Reset Buttons
			var NewCell4 = NewRow.insertCell(4);
			var temp = '<?=$btnAddTopic?>';
			temp += '&nbsp;';
			temp += '<?=$btnCancelTopic?>';
			NewCell4.innerHTML = temp;
		}
		
		
		
		// Focus Code and add Preset Code
		$('input#TopicCode').focus().val($('input#PresetTopicCode').val());		
		
		// Code validation
		js_Add_KeyUp_Code_Checking("TopicCode", "CodeWarningDiv_New", '');
		js_Add_KeyUp_Blank_Checking('TopicNameEn', 'NameEnWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>');
		js_Add_KeyUp_Blank_Checking('TopicNameCh', 'NameChWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>');
	}
}

function js_Check_Press_Enter_Insert_Topic(event)
{
	if (Get_KeyNum(event)==13) // keynum==13 => Enter
		js_Insert_Topic();
}

function js_Clear_Temp_Topic_Info()
{
	// Delete the Row
	var TableBody = document.getElementById("TopicInfoTable").tBodies[0];
	var RowIndex = document.getElementById("GenAddTopicRow").rowIndex-1;
	TableBody.deleteRow(RowIndex);
}

function js_Insert_Topic() {
	var Code = Trim($('input#TopicCode').val());
	var NameEn = Trim($('input#TopicNameEn').val());
	var NameCh = Trim($('input#TopicNameCh').val());
	var TopicRemarks = Trim($('textarea#TopicRemarks').val());
	
	if (js_Is_Input_Blank('TopicCode', 'CodeWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Code']?>'))
	{
		$('input#TopicCode').focus();
		return false;
	}
	else if (Code != '' && !valid_code(Code))
	{
		$('input#TopicCode').focus();
		$('div#CodeWarningDiv_New').html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>').show();
		return false;
	}
	
	if (js_Is_Input_Blank('TopicNameEn', 'NameEnWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>'))
	{
		$('input#TopicNameEn').focus();
		return false;
	}
	if (js_Is_Input_Blank('TopicNameCh', 'NameChWarningDiv_New', '<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Name']?>'))
	{
		$('input#TopicNameCh').focus();
		return false;
	}
	
	$.post(
		"ajax_validate.php", 
		{
			Action: 'Topic_Code',
			InputValue: Code
		},
		function(ReturnData)
		{
			if (ReturnData == "1")
			{
				var jsLevel = $('input#Level').val();
				var jsTopicID = $('input#TopicID').val();
				var jsParentTopicID = $('input#ParentTopicID').val();
				var jsPreviousLevelTopicID = $('input#PreviousLevelTopicID').val();
				
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Add_Topic",
						SubjectID: jsCurSubjectID,
						Level: jsLevel,
						Code: Code,
						NameEn: NameEn,
						NameCh: NameCh,
						Remarks: TopicRemarks,
						ParentTopicID: jsParentTopicID,
						PreviousLevelTopicID: jsTopicID,
						YearID: jsCurYearID
					},
					function(ReturnData)
					{
						//$('#debugArea').html(ReturnData);
						
						if (ReturnData == '1')
						{
							js_Reload_Subject_Curriculum_Table();
							js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>', 1);
						}
						else
						{
							Get_Return_Message('<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>');
						}
						//js_Hide_ThickBox();
					}
				);
			}
			else
			{
				// invalid code => show warning
				$('#CodeWarningDiv_New').html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code']?>').show();
				$('input#TopicCode').focus();
				return ;
				
				//$('#debugArea').html('aaa ' + ReturnData);
			}
		}
	);
}

function js_Delete_Topic(jsTopicID)
{
	if (confirm('<?=$Lang['eRC_Rubrics']['SettingsArr']['CurriculumPoolArr']['jsWarningArr']['Delete']?>'))
	{
		$.post(
			"ajax_update.php", 
			{ 
				Action: "Delete_Topic",
				TopicID: jsTopicID
			},
			function(ReturnData)
			{
				//$('#debugArea').html(ReturnData);
				
				if (ReturnData == '1')
				{
					var jsLevel = $('input#Level').val();
					var jsTopicID = $('input#TopicID').val();
					var jsParentTopicID = $('input#ParentTopicID').val();
					var jsPreviousLevelTopicID = $('input#PreviousLevelTopicID').val();
					
					js_Reload_Subject_Curriculum_Table();
					js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>', 1);
				}
				else
				{
					Get_Return_Message('<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>');
				}
			}
		);
	}
}

function js_Add_KeyUp_Code_Checking(jsListenerObjectID, jsWarningDivID, jsTopicID)
{
	// Code Validation
	$('#' + jsListenerObjectID).keyup( function() {
		var inputValue = $(this).val();
		var idArr = $(this).attr('id').split('_');
		var jsExcludeTopicID = idArr[1];
		
		if (jsExcludeTopicID == null)
			jsExcludeTopicID = '';
			
		if (!isNaN(jsExcludeTopicID))
			jsExcludeTopicID = jsTopicID;
			
		js_Code_Validation(inputValue, jsExcludeTopicID, jsWarningDivID);
	});
}

function js_Code_Validation(jsInputValue, jsExcludeTopicID, jsWarningDivID)
{
	if (Trim(jsInputValue) == '')
	{
		$('#' + jsWarningDivID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['Blank']['Code']?>').show();
	}
	else if (!valid_code(jsInputValue))
	{
		$('#' + jsWarningDivID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>').show();
	}
	else
	{
		$('#' + jsWarningDivID).hide();
		
		$.post(
			"ajax_validate.php", 
			{
				Action: 'Topic_Code',
				InputValue: jsInputValue,
				ExcludeTopicID: jsExcludeTopicID
			},
			function(ReturnData)
			{
				if (ReturnData == "1")
				{
					// valid code
					$('#' + jsWarningDivID).hide();
				}
				else
				{
					// invalid code => show warning
					$('#' + jsWarningDivID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code']?>').show();
					//$('#debugArea').html('aaa ' + ReturnData);
				}
			}
		);
	}
}

function js_Init_DND_Table() {
		
	var JQueryObj = $(".common_table_list_v30");
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			
			// Get the order string
			if(table.id == "TopicInfoTable"){
				/*** Building Edit Form ***/
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "" && !isNaN(rows[i].id))
						RecordOrder += rows[i].id+",";
				}
				//alert("RecordOrder = " + RecordOrder);
				
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Reorder_Topic",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						var jsLevel = $('input#Level').val();
						var jsTopicID = $('input#TopicID').val();
						var jsParentTopicID = $('input#ParentTopicID').val();
						var jsPreviousLevelTopicID = $('input#PreviousLevelTopicID').val();
						js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, '' ,1);
						
						js_Reload_Subject_Curriculum_Table();
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
	/*** End of Room Reordering in the index page ***/
}

function js_Init_JEdit()
{
	$('div.jEditCode').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsTopicID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			if (jsInputValue=='' || $(this)[0].revert == jsInputValue || $('#CodeWarningDiv_' + jsTopicID).css('display') != 'none')
			{
				// Empty or same Code => do nothing
				$('#CodeWarningDiv_' + jsTopicID).hide();
				$(this)[0].reset();
			}
			else if (jsInputValue.Trim() != '' && !valid_code(jsInputValue))
			{
				$('#CodeWarningDiv_' + jsTopicID).hide();
				$(this)[0].reset();
			}
			else
			{
				// Check if the code is vaild
				$.post(
					"ajax_validate.php", 
					{
						Action: 'Topic_Code',
						InputValue: jsInputValue,
						ExcludeTopicID: jsTopicID
					},
					function(ReturnData)
					{
						if (ReturnData == "1")
						{
							// valid code => Update Value
							$('#CodeWarningDiv_' + jsTopicID).hide();
							
							// valid
							ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
							$.post(
								"ajax_update.php", 
								{ 
									Action: "Update_Topic_Field",
									TopicID: jsTopicID,
									DBNameField: "TopicCode",
									Value: jsInputValue
								},
								function(ReturnData)
								{
									var jsReturnMsg = '';
									if (ReturnData == '1')
										jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
									else
										jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
									
									// Reload the layer and the background table
									var jsTopicID = $('input#TopicID').val();
									var jsLevel = $('input#Level').val();
									var jsParentTopicID = $('input#ParentTopicID').val();
									var jsPreviousLevelTopicID = $('input#PreviousLevelTopicID').val();
									
									js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, jsReturnMsg, 1);
									js_Reload_Subject_Curriculum_Table();
								}
							);
						}
						else
						{
							// invalid code => show warning
							$(this)[0].reset();
							$('div#CodeWarningDiv_' + jsTopicID).html('<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['InUse']['Code']?>').show();
						}
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['Code']?>",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	);
	// Code Validation
	$('div.jEditCode').keyup( function() {
		var idArr = $(this).attr('id').split('_');
		var jsTopicID = idArr[1];
		var jsInputValue = $('div#' + $(this).attr("id") + ' > form > input').val();
		
		js_Code_Validation(jsInputValue, jsTopicID, 'CodeWarningDiv_' + jsTopicID);
	});
	$('div.jEditCode').click( function() {
		//var jsInputValue = $('form > input').val();
		var idArr = $(this).attr('id').split('_');
		var jsTopicID = idArr[1];
		var jsInputValue = $('div#' + $(this).attr("id") + ' > form > input').val();
		
		js_Code_Validation(jsInputValue, jsTopicID, 'CodeWarningDiv_' + jsTopicID);
	});
	
	
	$('div.jEditNameEn').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsTopicID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			if (jsInputValue=='' || $(this)[0].revert == jsInputValue)
			{
				// Empty or same Code => do nothing
				$('#NameEnWarningDiv_' + jsTopicID).hide();
				$(this)[0].reset();
			}
			else
			{
				ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Update_Topic_Field",
						TopicID: jsTopicID,
						DBNameField: "TopicNameEn",
						Value: jsInputValue
					},
					function(ReturnData)
					{
						// Show system messages
						var jsReturnMsg = '';
						if (ReturnData == '1')
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						else
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							
						// Reload the layer and the background table
						var jsTopicID = $('input#TopicID').val();
						var jsLevel = $('input#Level').val();
						var jsParentTopicID = $('input#ParentTopicID').val();
						var jsPreviousLevelTopicID = $('input#PreviousLevelTopicID').val();
						js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, jsReturnMsg, 1);
						
						js_Reload_Subject_Curriculum_Table();
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['Code']?>",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	);
	
	
	$('div.jEditNameCh').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsTopicID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			if (jsInputValue=='' || $(this)[0].revert == jsInputValue)
			{
				// Empty or same Code => do nothing
				$('#NameChWarningDiv_' + jsTopicID).hide();
				$(this)[0].reset();
			}
			else
			{
				ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Update_Topic_Field",
						TopicID: jsTopicID,
						DBNameField: "TopicNameCh",
						Value: jsInputValue
					},
					function(ReturnData)
					{
						// Show system messages
						var jsReturnMsg = '';
						if (ReturnData == '1')
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						else
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							
						// Reload the layer and the background table
						var jsTopicID = $('input#TopicID').val();
						var jsLevel = $('input#Level').val();
						var jsParentTopicID = $('input#ParentTopicID').val();
						var jsPreviousLevelTopicID = $('input#PreviousLevelTopicID').val();
						js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, jsReturnMsg, 1);
						js_Reload_Subject_Curriculum_Table();
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "text",
			style  : "display:inline",
			maxlength  : "<?=$eRC_Rubrics_ConfigArr['MaxLength']['Topic']['Code']?>",
			height: "20px",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					}
		}
	);
	
	$('div.jEditRemarks').editable( 
		function(jsInputValue, settings) 
		{			
			var jsDivID = $(this).attr("id");
			var idArr = jsDivID.split('_');
			var jsTopicID = idArr[1];
			
			var ElementObj = $(this);
			jsInputValue = Trim(jsInputValue);
			
			// Remarks is not a required field => can update to empty
			//if (jsInputValue=='' || $(this)[0].revert == jsInputValue)
			if ($(this)[0].revert == jsInputValue)
			{
				// Empty or same Code => do nothing
				$('#NameEnWarningDiv_' + jsTopicID).hide();
				$(this)[0].reset();
			}
			else
			{
				ElementObj.html('<?=$linterface->Get_Ajax_Loading_Image()?>');
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Update_Topic_Field",
						TopicID: jsTopicID,
						DBNameField: "Remarks",
						Value: jsInputValue
					},
					function(ReturnData)
					{
						// Show system messages
						var jsReturnMsg = '';
						if (ReturnData == '1')
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>';
						else
							jsReturnMsg = '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
							
						// Reload the layer
						var jsTopicID = $('input#TopicID').val();
						var jsLevel = $('input#Level').val();
						var jsParentTopicID = $('input#ParentTopicID').val();
						var jsPreviousLevelTopicID = $('input#PreviousLevelTopicID').val();
						js_Get_Topic_Settings_Layer(jsTopicID, jsLevel, jsParentTopicID, jsPreviousLevelTopicID, jsReturnMsg, 1);
						
						//js_Reload_Subject_Curriculum_Table();
					}
				);
			}
		}, 
		{
			tooltip   : "<?=$Lang['General']['ClickToEdit']?>",
			event : "click",
			onblur : "submit",
			type : "textarea",
			rows : 4,
			style  : "display:inline",
			onreset: function() {
						$(this).parent().next().html('');
						$(this).parent().next().hide();
					},
			data: function(value, settings) {
					    /* Convert <br> to newline. */
					    var retval = value.replace(/<br[\s\/]?>/gi, '\n');
					    return retval;
				    }				    
		}
	);
}

function js_Export(jsExportAllSubject)
{
	if (jsExportAllSubject == 1)
	{
		$('input#FilterTopicInfoText').val('');
	}
	else if (jsExportAllSubject == 0)
	{
		$('input#FilterTopicInfoText').val(js_Get_Filtering_Info_Text());
	}
	
	$('input#ExportAllSubject').val(jsExportAllSubject);
	
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'export.php';
	jsFormObj.submit();
}

function js_Import()
{
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'import_step1.php';
	jsFormObj.submit();
}

function js_Get_Applicable_Form_Settings_Layer(jsTopicID, jsReturnMsg)
{
	$.post(
		"ajax_reload.php", 
		{ 
			Action: 'Reload_Topic_Applicable_Form_Settings_Layer',
			TopicID: jsTopicID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			
			if (jsReturnMsg != null && jsReturnMsg != '')
				Get_Return_Message(jsReturnMsg);
		}
	);
}

function js_Save_Topic_Form_Mapping(jsTopicID)
{
	if ($('input.FormChk:checked').size() == 0)
	{
		$('div#SelectApplicableFormWarningDiv').show().focus();
		return false;
	}
	
	var jsCheckedYearID = Array();
	$('input.FormChk:checked').each( function() {
		jsCheckedYearID[jsCheckedYearID.length] = $(this).val();
	})
	
	var jsCheckedYearIDList = jsCheckedYearID.toString();
	$.post(
		"ajax_update.php", 
		{ 
			Action: 'Save_Topic_Form_Mapping',
			TopicID: jsTopicID,
			YearIDList: jsCheckedYearIDList
		},
		function(ReturnData)
		{
			var jsReturnMsg = '';
			if (ReturnData == 1)
				jsReturnMsg = '';
			else 
				jsReturnMsg = '';
				
			js_Get_Applicable_Form_Settings_Layer(jsTopicID);
			js_Reload_Subject_Curriculum_Table();
		}
	);
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>