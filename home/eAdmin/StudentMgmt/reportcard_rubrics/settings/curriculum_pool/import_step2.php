<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CurriculumPool";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();


$lreportcard->Has_Access_Right($PageRight, $CurrentPage);
$SuccessArr = array();


### Validate file extension
$limport = new libimporttext();
$lo = new libfilesystem();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import_step1.php?ReturnMsgKey=WrongFileFormat");
	exit();
}


### move to temp folder first for others validation
$folder_prefix = $intranet_root."/file/import_temp/reportcard_rubrics/topic_info";
if (!file_exists($folder_prefix))
	$lo->folder_new($folder_prefix);

$TargetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$TargetFilePath = stripslashes($folder_prefix."/".$TargetFileName);
$SuccessArr['MoveCsvFileToTempFolder'] = $lo->lfs_move($csvfile, $TargetFilePath);


### Validate file header format
$DefaultCsvHeaderArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Title_Arr($ParLang='En');
$numOfDefaultCsvHeader = count($DefaultCsvHeaderArr);
$ColumnPropertyArr = $lreportcard_topic->Get_Topic_Import_Export_Column_Property($forGetCsvContent=1);

$CsvData = $limport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
$CsvHeaderArr = array_shift($CsvData);
$numOfCsvData = count($CsvData);


$CsvHeaderWrong = false;
for($i=0; $i<$numOfDefaultCsvHeader; $i++)
{
	if ($CsvHeaderArr[$i] != $DefaultCsvHeaderArr[$i])
	{
		$CsvHeaderWrong = true;
		break;
	}
}

if($CsvHeaderWrong || $numOfCsvData==0)
{
	$ReturnMsgKey = ($CsvHeaderWrong)? 'WrongCSVHeader' : 'CSVFileNoData';
	intranet_closedb();
	header("location: import_step1.php?ReturnMsgKey=".$ReturnMsgKey);
	exit();
}


### Thickbox loading message
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
	$ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$numOfCsvData, $Lang['General']['ImportArr']['RecordsValidated']);
$ProcessingMsg .= '</span>';


############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CurriculumPool_Tab_Array('CurriculumPool');
$linterface->LAYOUT_START();


echo $lreportcard_ui->Get_Settings_CurriculumPool_Import_Step2_UI($TargetFilePath, $numOfCsvData);
?>

<script language="javascript">

$('document').ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Go_Back()
{
	window.location = 'import_step1.php';
}

function js_Cancel()
{
	window.location = 'curriculum_pool.php';
}

function js_Continue()
{
	$('form#form1').submit();
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>