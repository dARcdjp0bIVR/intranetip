<?php
// using: Ivan
@SET_TIME_LIMIT(21600);
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CurriculumPool";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$numOfCsvData = $_POST['numOfCsvData'];


### Thickbox loading message					
$ProcessingMsg = '';
$ProcessingMsg .= '<span id="BlockUI_Span">';
	$ProcessingMsg .= str_replace('<!--NumOfRecords-->', '<span id="BlockUI_Processed_Number_Span">0</span> / '.$numOfCsvData, $Lang['General']['ImportArr']['RecordsProcessed']);
$ProcessingMsg .= '</span>';


############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CurriculumPool_Tab_Array('CurriculumPool');
$linterface->LAYOUT_START();


echo $lreportcard_ui->Get_Settings_CurriculumPool_Import_Step3_UI();
?>

<script language="javascript">

$('document').ready(function () {
	Block_Document('<?=$ProcessingMsg?>');
});

function js_Back_To_Curriculum_Pool()
{
	window.location = 'curriculum_pool.php';
}

function js_Back_To_Import_Step1()
{
	window.location = 'import_step1.php';
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>