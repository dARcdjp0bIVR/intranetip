<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
$lreportcard_ui = new libreportcardrubrics_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Reload_Subject_Curricumlum_Table')
{
	include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
	$lreportcard_topic = new libreportcardrubrics_topic();

	$SubjectID = $_POST['SubjectID'];
	$YearID = $_POST['YearID'];
	$FilterTopicInfoText = $_POST['FilterTopicInfoText'];	// $FilterTopicInfoText = 'Level,TopicID::Level,TopicID::'
	$FilterLevelTopicArr = $lreportcard_topic->Get_Topic_FilterArr_From_FilterText($FilterTopicInfoText);
			
	echo $lreportcard_ui->Get_Settings_CurriculumPool_Table($SubjectID, $FilterLevelTopicArr, $YearID);
}
else if ($Action == 'Reload_Topic_Settings_Layer')
{
	$SubjectID = $_POST['SubjectID'];
	$Level = $_POST['Level'];
	$TopicID = $_POST['TopicID'];
	$ParentTopicID = $_POST['ParentTopicID'];
	$PreviousLevelTopicID = $_POST['PreviousLevelTopicID'];
	
	echo $lreportcard_ui->Get_Settings_Topic_Layer($SubjectID, $TopicID, $Level, $ParentTopicID, $PreviousLevelTopicID);
}
else if ($Action == 'Reload_Topic_Applicable_Form_Settings_Layer')
{
	$TopicID = $_POST['TopicID'];
	echo $lreportcard_ui->Get_Settings_Topic_Applicable_Form_Layer($TopicID);
}
# moved to reportcard_rubrcis/ajax_reload.php
//else if ($Action == "Reload_Topic_Selection")
//{
//	$SubjectID = $_REQUEST['SubjectID'];
//	$Level = $_REQUEST['Level'];
//	$PreviousLevelTopicID = $_REQUEST['PreviousLevelTopicID'];
//	$SelectionID = stripslashes($_REQUEST['SelectionID']);
//	$OnChange = stripslashes($_REQUEST['OnChange']);
//	$TopicID = $_REQUEST['TopicID'];
//	
//	echo $lreportcard_ui->Get_Topic_Selection($SubjectID, $Level, $PreviousLevelTopicID, $SelectionID, $TopicID, $OnChange);
//}

intranet_closedb();
?>