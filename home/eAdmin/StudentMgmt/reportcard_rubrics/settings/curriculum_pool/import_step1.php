<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_CurriculumPool";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);


############## Interface Start ##############
$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
### Tag information
$TAGS_OBJ = $lreportcard_ui->Get_Settings_CurriculumPool_Tab_Array('CurriculumPool');

### Return Message
$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];
$linterface->LAYOUT_START($ReturnMsg);


echo $lreportcard_ui->Get_Settings_CurriculumPool_Import_Step1_UI();
?>

<script language="javascript">

function js_Go_Back()
{
	window.location = 'curriculum_pool.php';
}

function js_Go_Download_CSV_Sample()
{
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'export.php';
	jsFormObj.submit();
}

function js_Continue()
{
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'import_step2.php';
	jsFormObj.submit();
}

</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>