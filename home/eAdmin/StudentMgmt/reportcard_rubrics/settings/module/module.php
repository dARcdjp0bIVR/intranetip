<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_Module";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
$lreportcard = new libreportcardrubrics_custom();

$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Setting_Module_UI();
?>
<br><br>
<script>
function js_Edit_Module(TermID,ModuleID,msg)
{
	var ModuleID = ModuleID?ModuleID:'';
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadModuleSettingTBLayer",
			TermID:TermID,
			ModuleID:ModuleID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			if(msg)
				Get_Return_Message(msg);
		}
	);
	
}

function js_Add_Module(TermID,msg)
{
	js_Edit_Module(TermID,'',msg);
}

function DeleteModule(ModuleID)
{
	if(confirm("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsConfirmArr']['RemoveModule']?>"))
	{
		$.post(
			"ajax_update.php",
			{
				Action:"DeleteModule",
				ModuleID:ModuleID
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				refreshModuleTable();
			}
		);
	}
	else
		return false;
}

function CheckModuleCode()
{
	var ModuleID = $("input#ModuleID").val();
	var ModuleCode = $("input#ModuleCode").val();
	
	$.post(
		"ajax_update.php",
		{
			Action:"Module_Code",
			ModuleID:ModuleID,
			InputValue:ModuleCode
		},
		function(ReturnData)
		{
			if(ReturnData == 0)
			{
				$("div#Warn"+objID+":hidden").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['ModuleCodeNotAvailible']?>").show();
				return true;
			}
			else
			{
				$("div#Warn"+objID).hide();
				return false;
			}
		}
	);
}

function SubmitTBForm(AddMore)
{

	var FormValid = true;
	var jsCodeValid = true;
	var WarmMsgArr = new Array();
	$("div[id^='Warn']").hide();
	
	WarmMsgArr["ModuleNameEn"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleNameEn']?>";
	WarmMsgArr["ModuleNameCh"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleNameCh']?>";
	WarmMsgArr["ModuleCode"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['ModuleCode']?>";
	
	// check empty
	$("input.required").each(function(){
		var thisID = $(this).attr("id");
		var WarnMsgDivID = "Warn"+thisID;

		if($(this).val().Trim() == '')
		{
			$("div#"+WarnMsgDivID).html(WarmMsgArr[thisID]).show();
			FormValid = false;
		}

	});
	
	// Check Module Code is start with letter
	var ModuleCode = $("input#ModuleCode").val();

	if(ModuleCode.Trim() != '' && !valid_code(ModuleCode))
	{
		$("div#WarnModuleCode").html("<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>").show();
		jsCodeValid = false;
	}
	
	//compare Date
	var jsDateValid = true;
	if($("#ApplyDate").attr("checked"))
	{
		var StartDate = $("input#StartDate").val();
		var EndDate = $("input#EndDate").val();
		
		var TermStartDate = $("input#TermStartDate").val();
		var TermEndDate = $("input#TermEndDate").val();
		
		var jsDateValid = true;
		var WarnMsg = '';
		
		// Reset Date Warn Div
		$("div#WarnStartDate").html("").show();
		$("div#WarnEndDate").html("").show();
		
		// Start Date out of range
		if(StartDate.Trim() == '')
		{
			$("div#WarnStartDate").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['StartDate']?>").show();
			jsDateValid = false;
		}
		else if(compareDate2(StartDate,TermStartDate)<0 ||compareDate2(StartDate,TermEndDate)>0) 
		{
			$("div#WarnStartDate").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['StartDateOutOfTerm']?>").show();
			jsDateValid = false;
		}
		
		// End Date out of range
		if(EndDate.Trim() == '')
		{
			$("div#WarnEndDate").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EmptyArr']['EndDate']?>").show();
			jsDateValid = false;
		}
		else if(compareDate2(EndDate,TermStartDate)<0 ||compareDate2(EndDate,TermEndDate)>0) 
		{
			$("div#WarnEndDate").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EndDateOutOfTerm']?>").show();
			jsDateValid = false;
		}
		
		// End Date earlier than Start Date
		if(jsDateValid && compareDate2(StartDate,EndDate)>0) 
		{
			$("div#WarnEndDate").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['EndDateLaterThanStartDate']?>").show();
			jsDateValid = false;
		}
		
	}
	
	FormValid = FormValid&&jsDateValid&&jsCodeValid;

	// ajax check
	ajaxSubmit(FormValid, jsDateValid, jsCodeValid,AddMore);
		
}

function ajaxSubmit(FormValid, jsDateValid, jsCodeValid, AddMore)
{
	var ModuleID = $("input#ModuleID").val();
	var ModuleNameEn = $("input#ModuleNameEn").val();
	var ModuleNameCh = $("input#ModuleNameCh").val();
	var ModuleCode = $("input#ModuleCode").val();
	var StartDate = $("#ApplyDate").attr("checked")? $("input#StartDate").val():'';
	var EndDate = $("#ApplyDate").attr("checked")? $("input#EndDate").val():'';
	var YearTermID = $("input#YearTermID").val();
	var FormValid = FormValid?1:0;
	
	$.post(
		"ajax_update.php",
		{
			Action:"ValidateModuleData",
			ModuleID:ModuleID,
			YearTermID:YearTermID,
			ModuleNameEn:ModuleNameEn,
			ModuleNameCh:ModuleNameCh,
			ModuleCode:ModuleCode,
			StartDate:StartDate,
			EndDate:EndDate,
			FormValid:FormValid
		},
		function(ReturnData)
		{
			var DataAry = ReturnData.split("|==|");
			var RecordAdded = DataAry[0];
			var CodeValid = (DataAry[1]==1) && jsCodeValid;
			var DateValid = DataAry[2];
			
			if(RecordAdded.substring(0,1) == 1)
			{
				Get_Return_Message(RecordAdded);
				if(AddMore)
					js_Add_Module(YearTermID,RecordAdded);
				else
					js_Hide_ThickBox();
				refreshModuleTable();
			}
			else
			{
				if(ModuleCode.Trim() != '')
				{
					// code check
					if(CodeValid==0)
					{
						$("div#WarnModuleCode:hidden").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['ModuleCodeNotAvailible']?>").show();
					}
				}
				
				if(jsDateValid)
				{
					if(DateValid==false)
					{
						$("div#WarnEndDate").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsWarningArr']['DateRangeOverlap']?>").show();
					}
				}
			}

		}
	);
		
}

function refreshModuleTable()
{
	$.post(
		"ajax_reload.php",
		{
			Action:"loadModuleSettingTable"
		},
		function(ReturnData){
			$("td#ModuleTableSettingTD").html(ReturnData);
			initThickBox();
		}
	);
	
}

function DisableDate(obj)
{
	var setDisable = obj.checked?"":"disabled";
	$("input#StartDate, input#EndDate").attr("disabled",setDisable);
}
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>