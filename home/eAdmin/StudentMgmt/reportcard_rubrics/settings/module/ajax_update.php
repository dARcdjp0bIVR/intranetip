<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
$lreportcard_module = new libreportcardrubrics_module();


# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Module_Code')
{
	$InputValue = trim(stripslashes($_REQUEST['InputValue']));
	$ExcludeModuleID = $_REQUEST['ExcludeModuleID'];

	$Valid = $lreportcard_module->Is_Module_Code_Valid($InputValue, $ExcludeModuleID);
	echo ($Valid)? '1' : '0';
}
else if($Action == 'ValidateModuleData')
{
	$ModuleID = stripslashes($_REQUEST['ModuleID']);
	$ModuleCode = trim(stripslashes($_REQUEST['ModuleCode']));
	$ModuleNameEn = trim(stripslashes($_REQUEST['ModuleNameEn']));
	$ModuleNameCh = trim(stripslashes($_REQUEST['ModuleNameCh']));
	$StartDate = stripslashes($_REQUEST['StartDate']);
	$EndDate = stripslashes($_REQUEST['EndDate']);
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	$FormValid = stripslashes($_REQUEST['FormValid']);
	
	$CodeValid = $lreportcard_module->Is_Module_Code_Valid($ModuleCode, $ModuleID)?1:0;
	$DateValid = $lreportcard_module->Is_Module_Date_Range_Valid($StartDate, $EndDate, $ModuleID)?1:0;
	
	if($CodeValid && $DateValid && $FormValid)
	{
		$DataArr = array();
		$DataArr["ModuleCode"] = $ModuleCode;
		$DataArr["ModuleNameEn"] = $ModuleNameEn;
		$DataArr["ModuleNameCh"] = $ModuleNameCh;
		$DataArr["StartDate"] = $StartDate;
		$DataArr["EndDate"] = $EndDate;
		$DataArr["YearTermID"] = $YearTermID;
		
		if(trim($ModuleID)=='')
		{
			$Success = $lreportcard_module->InsertModule($DataArr);

			if($Success)
				echo $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['InsertSuccess']."|==|$CodeValid|==|$DateValid";
			else
				echo $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['InsertFail'];
		}
		else
		{
			$Success = $lreportcard_module->UpdateModule($ModuleID,$DataArr);
			if($Success)
				echo $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['UpdateSuccess']."|==|$CodeValid|==|$DateValid";
			else
				echo $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['UpdateFail'];
		}
		//echo "$Success|==|$CodeValid|==|$DateValid";
	}
	else
	{
		echo "0|==|$CodeValid|==|$DateValid";
	}
}
else if($Action == 'DeleteModule')
{
	$ModuleID = stripslashes($_REQUEST['ModuleID']);
	
	$DataArr["RecordStatus"] = 0;
	$Success = $lreportcard_module->UpdateModule($ModuleID,$DataArr);
	
	if($Success)
		echo $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['DeleteSuccess'];
	else
		echo $Lang['eRC_Rubrics']['SettingsArr']['ModuleArr']['jsReturnMsgArr']['DeleteFail'];
	
}

intranet_closedb();
?>
