<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
$lreportcard_ui = new libreportcardrubrics_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);
if ($Action == "Student_Learning_Details_Table")
{
	$YearTermID = $_REQUEST['YearTermID'];
	$SubjectID = $_REQUEST['SubjectID'];
	$SubjectGroupID = $_REQUEST['SubjectGroupID'];
	$StudentID = $_REQUEST['StudentID'];
	$ModuleID = $_REQUEST['ModuleID'];
	$FilterTopicInfoText = $_REQUEST['FilterTopicInfoText'];
	$noStudent =  $_REQUEST['noStudent'];
	
	echo $lreportcard_ui->Get_Settings_StudentLearningDetails_Table($YearTermID, $SubjectID, $SubjectGroupID, $StudentID, $ModuleID,$FilterTopicInfoText,$noStudent);
}

intranet_closedb();
?>