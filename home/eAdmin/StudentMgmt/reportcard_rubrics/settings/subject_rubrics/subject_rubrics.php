<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN");
$CurrentPage = "Settings_SubjectRubrics";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");

$lreportcard = new libreportcardrubrics_custom();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$linterface = new interface_html();

############## Interface Start ##############
$linterface = new interface_html();
$lreportcard_ui = new libreportcardrubrics_ui();
$lreportcard_rubrics = new libreportcardrubrics_rubrics();

$UsedRubricsList = $lreportcard_rubrics->GetUsedRubricList();
if(count($UsedRubricsList>0))
	$JSArrayValues = implode(',',$UsedRubricsList);

# Get Rubrics Item Row
$JSItemRowHtml = $lreportcard_ui->Get_Rubrics_Item_tr_for_JS();

$MODULE_OBJ = $lreportcard->GET_MODULE_OBJ_ARR();
# tag information
$TAGS_OBJ[] = array($Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['MenuTitle']);
$linterface->LAYOUT_START();

echo $lreportcard_ui->Include_JS_CSS();
echo $lreportcard_ui->Get_Setting_Rubrics_UI();

?>
<br><br>
<script>
var UsedRubricsID = [<?=$JSArrayValues?>];
var CurrentEditing = 0;

function js_Edit_Rubrics(SubjectID)
{
	CurrentEditing = SubjectID;
	
	if(!SubjectID)
		var RubricsID = '';
	else
		var RubricsID = $("select#Rubrics_Scheme_"+SubjectID).val();
	//var YearID = $("#YearID").val();
	
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadRubricsSettingTBLayer",
			RubricsID:RubricsID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			refreshBtnAndRemark();
			js_Init_DND_Table();
		}
	);
}

function DeleteRubrics(SubjectID)
{
	
	var RubricsID = $("select#Rubrics_Scheme_"+SubjectID).val();
	if(RubricsID == 0)
	{
		alert("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['SelectRubricsToDelete']?>");
		return false;
	}
	
	var RubricsUsed = false;
	
	for(var i=0; i<UsedRubricsID.length; i++)
	{
		if(UsedRubricsID[i] == RubricsID)
		{
			RubricsUsed = true;
			break;
		}
	}

	var ConfirmDelete = false; 
	if(RubricsUsed)
	{
		ConfirmDelete = confirm("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['DeleteUsedRubrics']?>");
	}	
	else
	{
		ConfirmDelete = confirm("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['DeleteRubrics']?>");
	}
	
	if(ConfirmDelete)
	{
		$.post(
			"ajax_update.php",
			{
				Action:"DeleteRubrics",
				RubricsID:RubricsID
			},
			function(ReturnData){
				Get_Return_Message(ReturnData);
				
				if(ReturnData.substring(0,1) == 1)
				{
					$("select.RubricsSchemeSelect").children("[value='"+RubricsID+"']").remove();
				}	
			}
		);
	}
}

function AddRubricsItem(obj)
{
	var ItemNum = $("a.add_btn").index(obj);
	
	// build row
	var html = '';
//	html += '<tr class="ItemRow">';		
//		html += '<td>';
//			html += '<div style="margin:2px;"><input name="RubricsItem[]" class="RubricsItem" size="20"> <span class="OrderRemark"></span></div>';
//			html += '<div style="margin:2px;"><input name="RubricsItemDesc[]" class="RubricsItemDesc" style="width:100%"></div>';
//			html += '<div style="color:red;" class="ItemWarning"></div>';
//			html += '<input type="hidden" name="RubricsItemID[]" value="new">';
//		html += '</td>';
//		html += '<td class="Draggable">';
//			html += '<div class="table_row_tool row_content_tool">';
//				html += '<a href="javascript:void(0);" class="move_order_dim move_btn"></a>';
//			html +=  '</div>';
//		html +=  '</td>';
//		html += '<td>';
//			html += '<div class="table_row_tool row_content_tool">';
//				html += '<a href="javascript:void(0);" onclick="AddRubricsItem(this)" class="add_dim add_btn"></a>';
//				html += '<a href="javascript:void(0);" onclick="DeleteRubricsItem(this)" class="delete_dim delete_btn"></a>';
//			html +=  '</div>';
//		html +=  '</td>';
//	html +=  '</tr>';
	html += '<?=$JSItemRowHtml?>';
	
	// add row below 
	$("tr.ItemRow:eq("+ItemNum+")").after(html);
	
	refreshBtnAndRemark();
	js_Init_DND_Table();
}

function DeleteRubricsItem(obj)
{
	var ItemNum = $("a.delete_btn").index(obj);
	
	$("tr.ItemRow:eq("+ItemNum+")").remove();
	
	refreshBtnAndRemark();	
}

function refreshBtnAndRemark()
{
	// refresh order remark
	$("span.OrderRemark").html("");
	$("span.OrderRemark:first").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Lowest']?>")
	$("span.OrderRemark:last").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['Highest']?>")
	
	// refresh del btn
	if($("tr.ItemRow").length>2)
		$("a.delete_btn:hidden").show();
	else
		$("a.delete_btn").hide();
}

function refreshRubricsTable()
{
	var YearID = $("#YearID").val();
	
	$.post(
		"ajax_reload.php",
		{
			Action:"loadRubricsSettingTable",
			YearID: YearID
		},
		function(ReturnData){
			$("td#RubricsTableSettingTD").html(ReturnData);
			initThickBox();
		}
	);
	
}

function js_Save(SaveAs)
{
	$("input#SaveAs").val(SaveAs);
	CheckTBForm();
}

function CheckTBForm()
{
	var FormValid = true;
	
	var WarmMsgArr = new Array();
	WarmMsgArr["RubricsEn"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsEn']?>";
	WarmMsgArr["RubricsCh"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsCh']?>";
	WarmMsgArr["RubricsCode"] = "<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsCode']?>";
	
	// check rubrics info empty
	$("input.required").each(function(){
		var thisID = $(this).attr("id");
		var WarnMsgDivID = "Warn"+thisID;

		if($(this).val().Trim() == '')
		{
			$("div#"+WarnMsgDivID).html(WarmMsgArr[thisID]).show();
			FormValid = false;
		}
		else
		{
			$("div#"+WarnMsgDivID).hide();
		}
	});
	
	// check rubrics item level and desc empty
	$("tr.ItemRow").each(function(){
		if($(this).find("input.RubricsItem").val().Trim()=='' || $(this).find("input.RubricsItemDesc").val().Trim()=='')
		{
			$(this).find("div.ItemWarning").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsLevelDesc']?>");
			FormValid = false;
		}
		else
		{
			$(this).find("div.ItemWarning").html("");
		}
	});
	
	// ajax check rubrics code
	if($("input#RubricsCode").val().Trim()!='')
	{
		if(!valid_code($("input#RubricsCode").val()))
		{
			$("div#WarnRubricsCode").html("<?=$Lang['eRC_Rubrics']['GeneralArr']['WarningArr']['CodeMustBeginWithLetter']?>").show();
			FormValid = false
		}
		else
		{ 
			
			var SaveAs = $("input#SaveAs").val();
			var RubricsCode = $("input#RubricsCode").val();
			var RubricsID = SaveAs==1?'':$("input#RubricsID").val();
			
			$.post(
				"ajax_validate.php",
				{
					Action: "ValidateRubricsCode",
					RubricsCode:RubricsCode,
					RubricsID:RubricsID
				},
				function (ReturnData)
				{
					if(ReturnData==1)
					{
						$("div#WarnRubricsCode").hide();
						if(FormValid == true)
						{
							var AnyItemUsed = $("input#AnyItemUsed").val();
							if(AnyItemUsed!=1 || SaveAs==1 || confirm("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsConfirmArr']['EditRubricsWithUsedItem']?>"))
							{
								SubmitTBForm();
							}
						}
					}
					else
					{
						$("div#WarnRubricsCode").html("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['RubricsCodeNotAvailible']?>").show();
						FormValid = false;
					}
				}
			);
		}
	}
	
}

function SubmitTBForm()
{
	var jsSubmitString = Get_Form_Values(document.TBForm);
	jsSubmitString += '&Action=SaveRubricInfo';
	
	$.ajax({  
		type: "POST",  
		url: "ajax_update.php",
		data: jsSubmitString,  
		success: function(ReturnData) {
			
			var DataArr = ReturnData.split("|==|");
			var msg = DataArr[0];
			
			Get_Return_Message(msg);
			js_Hide_ThickBox();
			
			 // for new rubrics record, add option in selection
			 var ActionType = DataArr[1] // Insert or Update

			if(ActionType.Trim() == "Insert") // Insert new rubrics to selections 
			{
				var RubricsID = DataArr[2];
				var RubricsName = DataArr[3];
				var RubricsCode = DataArr[4];
				var optionHTML = '<option value="'+RubricsID+'">'+RubricsName+' ('+RubricsCode+')'+'</oprion>';
				
				$("select#SetAllSchemeSelection").append(optionHTML); //add to SetAllSchemeSelection 
				$("select.RubricsSchemeSelect:gt(0)").each(function(){ $(this).children(":last").before(optionHTML); }); // add to every selection (before "new" option)
				$("select#Rubrics_Scheme_"+CurrentEditing+" option[value='"+RubricsID+"']").attr("selected","selected"); // select newly added option
			}
			else if(ActionType == "Update") // update Rubrics Name
			{
				var RubricsID = DataArr[2];
				var RubricsName = DataArr[3];
				var RubricsCode = DataArr[4];
				$("select.RubricsSchemeSelect").children("[value='"+RubricsID+"']").html(RubricsName+" ("+RubricsCode+")");
			}
		} 
	});  
	
}

function CheckForm()
{
	
	var AllSelected = true;
	$("select.RubricsSchemeSelect:gt(0)").each(function(){
		if($(this).val().Trim() == '')
		{
			AllSelected = false;
		}
	});
	
	if(AllSelected == true)
	{
		var jsSubmitString = Get_Form_Values(document.form1);
		jsSubmitString += '&Action=SaveRubricSubjectMap';
		
		$.ajax({  
			type: "POST",  
			url: "ajax_update.php",
			data: jsSubmitString,  
			success: function(ReturnData) 
			{
				Get_Return_Message(ReturnData);	
				window.location.href='#'
			} 
		}); 
	}
	else
	{
		alert("<?=$Lang['eRC_Rubrics']['SettingsArr']['RubricsArr']['jsWarningArr']['EmptyArr']['RubricsScheme']?>")
	}
}

function js_Reset_Rubrics_Item()
{
	// input#RubricsID is on thickbox
	var RubricsID = $("input#RubricsID").val();
	
	$.post(
		"ajax_reload.php",
		{
			Action:"LoadRubricsSettingTBLayer",
			RubricsID:RubricsID
		},
		function(ReturnData)
		{
			$('div#TB_ajaxContent').html(ReturnData);
			refreshBtnAndRemark();
			js_Init_DND_Table();
		}
	);
}

function js_Reset_Rubrics_Subject_Map()
{
	refreshRubricsTable();
}

function SetAllSelection()
{
	var val = $("select#SetAllSchemeSelection").val();
	if(val)
		$("select.RubricsSchemeSelect").children("[value='"+val+"']").attr("selected","selected");
}

function SchemeSelectChange(obj)
{
	var idx = $("select.RubricsSchemeSelect:gt(0)").index(obj);
	if(obj.value.Trim() == '' && obj.selectedIndex >0)
	{
		$("a.edit_dim:eq("+idx+")").click();
	}
}

function js_Init_DND_Table() {
		
	var JQueryObj = $("#RubricsItemSettingTable");
	JQueryObj.tableDnD({
		onDrop: function(table, DroppedRow) {
			refreshBtnAndRemark();
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);
			
		},
		dragHandle: "Draggable", 
		onDragClass: "move_selected"
	});
	/*** End of Room Reordering in the index page ***/
}

$().ready(function(){
	refreshRubricsTable();
	$("#SaveBtn").attr("disabled","");
});

</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>