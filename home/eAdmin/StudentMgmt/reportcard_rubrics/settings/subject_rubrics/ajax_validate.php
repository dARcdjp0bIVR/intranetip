<?php
// using :
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
$lreportcard_rubrics = new libreportcardrubrics_rubrics();


# Get data
$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'ValidateRubricsCode')
{
	$RubricsID = stripslashes($_REQUEST['RubricsID']);
	$RubricsCode = $_REQUEST['RubricsCode'];

	$Valid = $lreportcard_rubrics->Is_Rubrics_Code_Valid($RubricsCode, $RubricsID);
	echo ($Valid)? '1' : '0';
}



intranet_closedb();
?>