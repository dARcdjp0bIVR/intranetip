<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Statistics_TopicClassPerformance";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
//include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

//$lreportcard = new libreportcardrubrics_custom();
$lreportcard = new libreportcardrubrics();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

### prepare tmp folder
$fs = new libfilesystem();
$folder_prefix = $intranet_root."/file/reportcard_rubrics/tmp_chart/";

### Clear the tmp png folder
if (file_exists($folder_prefix))
	$fs->folder_remove_recursive($folder_prefix);

### Create the tmp png folder if not exist
//if (!file_exists($folder_prefix))
//{
	$fs->folder_new($folder_prefix);
	chmod($folder_prefix, 0777);
//}

### Print Button
$PrintBtn = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

### init libuser in batch
$lu = new libuser('','',$StudentIDArr);

### Gen Flash Chart
$chartWidth = "800";
$chartHeight = "600";

$Statistic = $lreportcard->Get_Topic_Mark_Statistics($StudentIDArr,$ModuleID,$SubjectID,1);

foreach($Statistic as $StudentID => $SubjectTopicArr)
{
	$lu->LoadUserData($StudentID);
	$thisClassName = $lu->ClassName;
	
	foreach($SubjectTopicArr as $SubjectID => $TopicArr)
	{
		foreach($TopicArr as $TopicID =>$TopicInfo)
		{
			if(!in_array($TopicID,(array)$TopicIDArr))
				continue;
			
			if(!in_array($thisClassName,(array)$DataArr[$TopicID]['InvolvedClass']))
				$DataArr[$TopicID]['InvolvedClass'][]= $thisClassName;
			
			foreach($TopicInfo['MarkInfoArr'] as $RubricsID => $RubricsCount)
			{
				if(!in_array($RubricsID,(array)$DataArr[$TopicID]['InvolvedRubricsItems']))
					$DataArr[$TopicID]['InvolvedRubricsItems'][] = $RubricsID;
				$DataArr[$TopicID][$thisClassName]['Total'] += $RubricsCount['Count'];
				$DataArr[$TopicID][$thisClassName][$RubricsID]+= $RubricsCount['Count'];
				
			}
		}
	}
}

$FlashChartArr = $lreportcard_ui->Get_Statistics_TopicClassPerformance_Flash_Chart($SubjectID, $DataArr, $SubjectDisplay, $YearID);

// Added By Philips
include_once('chart.php');
$highChartData = generate_class_subject_chart($DataArr, $SubjectDisplay, $lreportcard->Get_Subject_Info(),$intranet_session_language, $SubjectID, $TopicCode);
$subName = get_SubjectById($SubjectDisplay, $lreportcard->Get_Subject_Info(),$intranet_session_language, $SubjectID);
// Added By Philips

$k=0;
foreach((array)$FlashChartArr as $StudentID => $FlashChart)
{
	$chartArr[] = $FlashChart;
	$FlashDiv .= '<table cellpadding=2 cellspacing=2 align=center>';
		$FlashDiv .= '<tr><td><div id="div'.$k.'parent"><div id="div'.$k.'"></div></div></td></tr>';
		$FlashDiv .= '<tr><td><div id="StatTable'.$k.'" style="text-align:center">'.$StatisticTable[$StudentID].'</div></td></tr>';
	$FlashDiv .= '</table><br><br>';
	$k++;
}

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/swfobject.js"></script>	
<script type="text/javascript">

	function ofc_ready(){
		//alert('ofc_ready');
	}

	function open_flash_chart_data(id){
		return JSON.stringify(dataArr[id]);
	}

	function findSWF(movieName) {
	  if (navigator.appName.indexOf("Microsoft")!= -1) {
		return window[movieName];
	  } else {
		return document[movieName];
	  }
	}

	/*
	function jsonLoaded(id){
		//alert("json loaded and id: "+id);
	}	

	function onAutoReturnImgBinary(bmpBytStr){
		//alert("image binary string is automatically returned: "+bmpBytStr);
	}
	*/

	function onAutoPostImgBinary(id){
		//alert("image binary string is automatically posted and flash id: "+id);
		var swfDIV = document.getElementById("div"+id+"parent");
		//var swfDIV = document.getElementById("img"+id);
		//alert("swfDIV: "+swfDIV+" / width: "+swfDIV.width+" / height: "+swfDIV.height);
		while(swfDIV.hasChildNodes()){
			swfDIV.removeChild(swfDIV.firstChild);
		}
		var img = document.createElement("img");
		img.setAttribute("src", "<?=$intranet_httppath?>/file/reportcard_rubrics/tmp_chart/tmp_chart_png_"+id+".png");
		img.setAttribute("width", "<?=$chartWidth?>");
		img.setAttribute("height", "<?=$chartHeight?>");
		swfDIV.appendChild(img);
	}
	
	var chartNum = <?=count($chartArr)?>;
//	alert("chartNum: "+chartNum);
	
	var dataArr = new Array();
	<?php
		foreach( (array)$chartArr as $chartID => $chartJSON ) {
			echo "dataArr[ ".$chartID." ] = ".$chartJSON->toPrettyString()."\n";
		}
	?>
	
//	alert("dataArr length: "+dataArr.length);

</script>
	
<script type="text/javascript">
	// old version of chart
	//var chartNum = <?=count($chartArr)?>;
	//for(var k=0; k<chartNum; k++){
	//	var flashvars = {id:k, postImgUrl:"../uploadFlashImage.php"};
	//	swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/open-flash-chart.swf", "div"+k, "<?=$chartWidth?>", "<?=$chartHeight?>", "9.0.0", "", flashvars);
	//}//end for
</script>

<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>
<style type='text/css'>
	.report_table {border-collapse:collapse;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<div style="text-align:center;vertical-align:top;width:100%">
	<?//=$FlashDiv?>
</div>
<!-- Start of Philips edited -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<?php for($i=0; $i<count($highChartData);$i++){
	echo "<div id='chart_container$i' style='min-width: 310px; max-width: 800px; height: 600px; margin: 0 auto'></div>";
}?>
<script type="text/javascript">
var dataArray = [];
var nameArray = [];
var topicArray = [];
var cateArray = [];
var colorArray = ['#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414'];
<?php 
$loopCounter = 0;
$Rindex = 0;
foreach($highChartData as $topic => $data){
    echo "topicArray.push('$topic');";
    echo "dataArray[$loopCounter] = [];";
    echo "var temp_name = [];";
    foreach($data as $content){
        echo "var temp_arr = [];";
        echo "temp_name.push('$content[name]');";
        foreach($content['rubrics'] as $k => $v){
            if($k!='total'){
                if($Rindex == 0){
                    echo "cateArray.push('$k');";
                }
                echo "temp_arr.push('$v');";
            }
        }
        $Rindex++;
        echo "dataArray[$loopCounter].push({subject: '$content[name]', data: temp_arr});";
    }
    $loopCounter++;
    echo "nameArray.push(temp_name);";
}
/* For Classes
var classArray = [];
foreach($highChartData as $k => $v){
    echo "nameArray.push('$data[name]');";
    echo "classArray.push('$k');";
    echo "dataArray[$loopCounter] = [];";
    foreach($v['rubrics'] as $key => $value){
        if($k != 'total'){
            echo "dataArray[$loopCounter]['$key'] = [];";
            echo "dataArray[$loopCounter]['$key'].push($value);";
        }
    }
    $loopCounter++;
}
*/
?>
/*
for(var i=0; i<dataArray.length; i++){
	generate_class_subject_chart({
	 id: 'chart_container'+i,
	 title: nameArray[i],
	 subjects: classArray,
	 data: dataArray[i]
	});
}
*/
for(var i=0; i<dataArray.length; i++){
	generate_class_subject_chart({
	 id: 'chart_container'+i,
	 title: topicArray[i]+ ' ' +"<?=$subName?>",
	 subjects: nameArray[i],
	 data: dataArray[i],
	 cate: cateArray
	});
}

function generate_class_subject_chart(obj){
	var series = [];
	var colorIndex = 0;
	for(var cate in obj.cate){
		if(cate!='in_array'){
		 var sdata = {};
		 sdata.name = obj.cate[cate];
		 sdata.data = [];
		 for(var data in obj.data){
			if(data!='in_array')
				sdata.data.push(parseInt(obj.data[data].data[cate]));
		 }
		 sdata.color = colorArray[colorIndex];
		 sdata.legendIndex = 6 - colorIndex;
		 series.push(sdata);
		 colorIndex++;
		}
	}
	Highcharts.chart(obj.id, {
	 chart: {
	  type: 'column'
	 },
	 title: {
	  text: obj.title
	 },
	 xAxis: {
	  categories: obj.subjects,
	  lineWidth: 3,
	  lineColor: 'darkgrey',
	  gridLineWidth: 2
	 },
	 yAxis: {
	  min: 0,
	  title: {
	   text: 'Percentage'
	  },
	  lineWidth: 3,
	  lineColor: 'darkgrey',
	  gridLineWidth: 2,
	  stackLabels: {
	   enabled: true,
	   style: {
	    fontWeight: 'bold',
	    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	   }
	  }
	 },
	 legend: {
	  align: 'center',
	  x: 0,
	  verticalAlign: 'bottom',
	  y: 25,
	  floating: false,
	 },
	 tooltip: {
	  headerFormat: '<b>{point.x}</b><br/>',
	  pointFormat: '{series.name}: {point.percentage:.2f}%<br/>Total: {point.y} / {point.stackTotal}'
	 },
	 plotOptions: {
	  column: {
	   stacking: 'percent',
	   dataLabels: {
	    enabled: true
	   }
	  }
	 },
	 credits:{
	  enabled: false
	 },
	 exporting:{
	   enabled: false
	 },
	 series: series
	});
}
</script>
<!-- End of Philips edited -->
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>