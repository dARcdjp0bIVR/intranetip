<?php

function generate_class_subject_chart($rubricsArr, $SubjectDisplay, $info, $lang, $SubjectID, $TopicCode)
{
    global $PATH_WRT_ROOT, $Lang, $lreportcard;
    include_once ($PATH_WRT_ROOT . "includes/libreportcardrubrics_rubrics.php");
    // Get Rubrics Info
    $lreportcard_rubrics = new libreportcardrubrics_rubrics();
    // $RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
    // $RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
    $RubricsInfo = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping();
    $subjectDisplay = array(
        'Abbr' => 'ABBR',
        'Desc' => 'DES',
        'ShortForm' => 'SNAME'
    );
    $rubricsRef = array(
        7 => "Not Good",
        8 => "Normal",
        9 => "Good",
        10 => "Excellent"
    );
    $thisLang = ($lang == 'en') ? 'EN_' : 'CH_';
    $subjectsRef = array();
    $scoreRef = array();
    
    // From Get_Statistics_Bar_Chart_Topic_Key() in libreportcardrubrics_ui.php
    if (! isset($lreportcard_topic)) {
        include_once ($PATH_WRT_ROOT . "includes/libreportcardrubrics_topic.php");
        $lreportcard_topic = new libreportcardrubrics_topic();
    }
    
    // Get_Topic_Info
    $TopicInfo = $lreportcard_topic->Get_Topic_Info($SubjectID);
    $TopicInfo = BuildMultiKeyAssoc($TopicInfo, "TopicID");
    $TopicDisplayLang = Get_Lang_Selection("TopicNameCh", "TopicNameEn");
    $ItemDisplayLang = Get_Lang_Selection("DescriptionCh", "DescriptionEn");
    /*
     * foreach($rubricsArr as $key => $valArr){
     * if($key != 'InvolvedTopic' && $key != 'InvolvedRubricsItems'){
     * $rubrics = array();
     * foreach($rubricsRef as $rr){
     * $rubrics[$rr] = 0;
     * }
     * foreach($valArr[null] as $k => $v){
     * if($k=='Total')
     * $rubrics['total'] = $v;
     * else
     * $rubrics[$rubricsRef[$k]] = $v;
     * }
     * array_push($scoreRef, array('name' => $TopicInfo[$key][$TopicDisplayLang], 'rubrics' => $rubrics));
     * }
     * }
     */
    echo '<br/>';
    /*
     * Array ( [40] => Array ( [InvolvedClass] => Array ( [0] => F9A ) [InvolvedRubricsItems] => Array ( [0] => 8 )
     * [F9A] => Array ( [Total] => 2 [8] => 2 ) ) )
     */
    foreach ($rubricsArr as $Topic => $valArr) {
        $scoreRef[$TopicInfo[$Topic][$TopicDisplayLang]] = array();
        foreach ($valArr as $class => $arr) {
            if ($class != 'InvolvedClass' && $class != 'InvolvedRubricsItems') {
                $rubrics = array();
                foreach ($RubricsInfo[0][$SubjectID] as $rr) {
                    $rubrics[$rr[$ItemDisplayLang]] = 0;
                }
                foreach ($arr as $key => $v) {
                    if ($key == 'Total')
                        $rubrics['total'] = $v;
                    else {
                        $RubricsName = $RubricsInfo[0][$SubjectID][$key][$ItemDisplayLang];
                        $rubrics[$RubricsName] = $v;
                    }
                }
                array_push($scoreRef[$TopicInfo[$Topic][$TopicDisplayLang]], array(
                    'name' => $class,
                    'rubrics' => $rubrics
                ));
            }
        }
    }
    return $scoreRef;
}

function get_SubjectById($SubjectDisplay, $info, $lang, $SubjectID)
{
    $subjectDisplay = array(
        'Abbr' => 'ABBR',
        'Desc' => 'DES',
        'ShortForm' => 'SNAME'
    );
    $thisLang = ($lang == 'en') ? 'EN_' : 'CH_';
    $subject = $info[$SubjectID][$thisLang . 'DES'];
    return $subject;
}
?>