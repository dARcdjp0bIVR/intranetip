<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Statistics_SubjectClassPerformance";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
//include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

//$lreportcard = new libreportcardrubrics_custom();
$lreportcard = new libreportcardrubrics();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();

### prepare tmp folder
$fs = new libfilesystem();
$folder_prefix = $intranet_root."/file/reportcard_rubrics/tmp_chart/";

### Clear the tmp png folder
if (file_exists($folder_prefix))
	$fs->folder_remove_recursive($folder_prefix);

### Create the tmp png folder if not exist
//if (!file_exists($folder_prefix))
//{
	$fs->folder_new($folder_prefix);
	chmod($folder_prefix, 0777);
//}

### Print Button
$PrintBtn = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

### init libuser in batch
$lu = new libuser('','',$StudentIDArr);

### Gen Flash Chart
$chartWidth = "800";
$chartHeight = "600";

$TargetLevel = 1;
$Statistic = $lreportcard->Get_Topic_Mark_Statistics($StudentIDArr,$ModuleID,$SubjectIDArr, $TargetLevel);

foreach($Statistic as $StudentID => $SubjectTopicArr)
{
	$lu->LoadUserData($StudentID);
	$thisClassName = $lu->ClassName;
	
	foreach($SubjectTopicArr as $SubjectID => $TopicArr)
	{
		if(!in_array($thisClassName,(array)$ClassSubjectRubricsArr[$SubjectID]['InvolvedClass']))
			$ClassSubjectRubricsArr[$SubjectID]['InvolvedClass'][]= $thisClassName;
		foreach($TopicArr as $TopicID =>$TopicInfo)
		{
			foreach($TopicInfo['MarkInfoArr'] as $RubricsID => $RubricsCount)
			{
				if(!in_array($RubricsID,(array)$ClassSubjectRubricsArr[$SubjectID]['InvolvedRubricsItems']))
					$ClassSubjectRubricsArr[$SubjectID]['InvolvedRubricsItems'][] = $RubricsID;
				$ClassSubjectRubricsArr[$SubjectID][$thisClassName]['Total'] += $RubricsCount['Count'];
				$ClassSubjectRubricsArr[$SubjectID][$thisClassName][$RubricsID]+= $RubricsCount['Count'];
			}
		}
	}
}

$FlashChartArr = $lreportcard_ui->Get_Statistics_SubjectPerformance_Flash_Chart($ClassSubjectRubricsArr, $SubjectDisplay, $YearID);
//$StatisticTable = $lreportcard_ui->Get_Statistics_RubricsReport_Statistic_Table($SubjectMarkDisplayArr, $SubjectDisplay);

// Added By Philips
include_once('chart.php');
$highChartData = generate_class_subject_chart($ClassSubjectRubricsArr, $SubjectDisplay, $lreportcard->Get_Subject_Info(),$intranet_session_language);
// Added By Philips

$k=0;
foreach((array)$FlashChartArr as $StudentID => $FlashChart)
{
	$chartArr[] = $FlashChart;
	$FlashDiv .= '<table cellpadding=2 cellspacing=2 align=center>';
		$FlashDiv .= '<tr><td><div id="div'.$k.'parent"><div id="div'.$k.'"></div></div></td></tr>';
		$FlashDiv .= '<tr><td><div id="StatTable'.$k.'" style="text-align:center">'.$StatisticTable[$StudentID].'</div></td></tr>';
	$FlashDiv .= '</table><br><br>';
	$k++;
}
//echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));
//echo $lreportcard_ui->Include_Template_CSS();
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/swfobject.js"></script>	
<script type="text/javascript">

	function ofc_ready(){
		//alert('ofc_ready');
	}

	function open_flash_chart_data(id){
		return JSON.stringify(dataArr[id]);
	}

	function findSWF(movieName) {
	  if (navigator.appName.indexOf("Microsoft")!= -1) {
		return window[movieName];
	  } else {
		return document[movieName];
	  }
	}

	/*
	function jsonLoaded(id){
		//alert("json loaded and id: "+id);
	}	

	function onAutoReturnImgBinary(bmpBytStr){
		//alert("image binary string is automatically returned: "+bmpBytStr);
	}
	*/

	function onAutoPostImgBinary(id){
		//alert("image binary string is automatically posted and flash id: "+id);
		var swfDIV = document.getElementById("div"+id+"parent");
		//var swfDIV = document.getElementById("img"+id);
		//alert("swfDIV: "+swfDIV+" / width: "+swfDIV.width+" / height: "+swfDIV.height);
		while(swfDIV.hasChildNodes()){
			swfDIV.removeChild(swfDIV.firstChild);
		}
		var img = document.createElement("img");
		img.setAttribute("src", "<?=$intranet_httppath?>/file/reportcard_rubrics/tmp_chart/tmp_chart_png_"+id+".png");
		img.setAttribute("width", "<?=$chartWidth?>");
		img.setAttribute("height", "<?=$chartHeight?>");
		swfDIV.appendChild(img);
	}
	
	var chartNum = <?=count($chartArr)?>;
//	alert("chartNum: "+chartNum);
	
	var dataArr = new Array();
	<?php
		foreach( (array)$chartArr as $chartID => $chartJSON ) {
			echo "dataArr[ ".$chartID." ] = ".$chartJSON->toPrettyString()."\n";
		}
	?>
	
//	alert("dataArr length: "+dataArr.length);

</script>
	
<script type="text/javascript">
	//old version of chart
	//var chartNum = <?=count($chartArr)?>;
	//for(var k=0; k<chartNum; k++){
	//	var flashvars = {id:k, postImgUrl:"../uploadFlashImage.php"};
	//	swfobject.embedSWF("<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/open-flash-chart.swf", "div"+k, "<?=$chartWidth?>", "<?=$chartHeight?>", "9.0.0", "", flashvars);
	//}//end for
</script>

<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>
<style type='text/css'>
	.report_table {border-collapse:collapse;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<div style="text-align:center;vertical-align:top;width:100%">
	<?=$FlashDiv?>
</div>
<!-- Start of Philips edited -->

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<?php for($i=0; $i<count($highChartData);$i++){
	echo "<div id='chart_container$i' style='min-width: 310px; max-width: 930px; height: 400px; margin: 0 auto'></div>";
}?>
<script type="text/javascript">
//Array ( [F9A] => Array ( [0] => Array ( [name] => English Language [rubrics] => Array 
		//( [Not Good] => 0 [Normal] => 2 [Good] => 2 [Excellent] => 0 [total] => 4 ) ) ) )
var dataArray = [];
var nameArray = [];
var classArray = [];
var colorArray = ['#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414'];
<?php 
$loopCounter = 0;
/*foreach($highChartData as $data){
    echo "nameArray.push('$data[name]');";
    echo "dataArray[$loopCounter] = [];";
    foreach($data['rubrics'] as $k => $v){
        if($k != 'total'){
            echo "dataArray[$loopCounter]['$k'] = [];";
            echo "dataArray[$loopCounter]['$k'].push($v);";
        }
    }
    $loopCounter++;
}*/
foreach($highChartData as $class => $content){
    foreach($content as $data){
            echo "nameArray.push({topic:'$data[name]', cName: ['$class']});";
            echo "dataArray[$loopCounter] = [];";
            foreach($data['rubrics'] as $k => $v){
                if($k != 'total'){
                    echo "dataArray[$loopCounter]['$k'] = [];";
                    echo "dataArray[$loopCounter]['$k'].push($v);";
                }
            }
    }
    $loopCounter++;
}
?>
for(var i=0; i<dataArray.length; i++){
	generate_class_subject_chart({
	 id: 'chart_container'+i,
	 title: nameArray[i].topic,
	 subjects: nameArray[i].cName,
	 data: dataArray[i]
	});
}

function generate_class_subject_chart(obj){
	var series = [];
	var colorIndex = 0;
	obj.data.reverse();
	for(var data in obj.data){
		if(data!='in_array'){
		 var sdata = {};
		 sdata.name = data;
		 sdata.data = obj.data[data];
		 sdata.color = colorArray[colorIndex];
		 sdata.legendIndex = 4 - colorIndex;
		 series.push(sdata);
		 colorIndex++;
		}
	}
	Highcharts.chart(obj.id, {
	 chart: {
	  type: 'column'
	 },
	 title: {
	  text: obj.title
	 },
	 xAxis: {
	  categories: obj.subjects,
	  lineWidth: 3,
	  lineColor: 'darkgrey',
	  gridLineWidth: 2
	 },
	 yAxis: {
	  min: 0,
	  title: {
	   text: 'Percentage'
	  },
	  lineWidth: 3,
	  lineColor: 'darkgrey',
	  gridLineWidth: 2,
	  stackLabels: {
	   enabled: true,
	   style: {
	    fontWeight: 'bold',
	    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
	   }
	  }
	 },
	 legend: {
	  align: 'center',
	  x: 0,
	  verticalAlign: 'bottom',
	  y: 25,
	  floating: false,
	 },
	 tooltip: {
	  headerFormat: '<b>{point.x}</b><br/>',
	  pointFormat: '{series.name}: {point.percentage:.2f}%<br/>Total: {point.y} / {point.stackTotal}'
	 },
	 plotOptions: {
	  column: {
	   stacking: 'percent',
	   dataLabels: {
	    enabled: true
	   }
	  }
	 },
	 credits:{
	  enabled: false
	 },
	 exporting:{
	   enabled: false
	 },
	 series: series
	});
}
</script>
<!-- End of Philips edited -->
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>