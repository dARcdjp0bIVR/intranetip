<?php

function generate_class_subject_chart($rubricsArr, $SubjectDisplay, $SubjectCode, $info, $lang)
{
    global $PATH_WRT_ROOT, $Lang, $lreportcard;
    include_once ($PATH_WRT_ROOT . "includes/libreportcardrubrics_rubrics.php");
    // Get Rubrics Info
    $lreportcard_rubrics = new libreportcardrubrics_rubrics();
    // $RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
    // $RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
    $RubricsInfo = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping();
    $subjectDisplay = array(
        'Abbr' => 'ABBR',
        'Desc' => 'DES',
        'ShortForm' => 'SNAME'
    );
    $rubricsRef = array(
        7 => "Not Good",
        8 => "Normal",
        9 => "Good",
        10 => "Excellent"
    );
    $thisLang = ($lang == 'en') ? 'EN_' : 'CH_';
    $subjectsRef = array();
    $scoreRef = array();
    // Array ( [F9A] => Array ( [InvolvedSubject] => Array ( [0] => 8 ) [InvolvedRubricsItems] => Array
    // ( [0] => 10 [1] => 9 [2] => 8 ) [8] => Array ( [Total] => 6 [10] => 1 [9] => 3 [8] => 2 ) ) )
    $ItemDisplayLang = Get_Lang_Selection("DescriptionCh","DescriptionEn");
    foreach ($rubricsArr as $class => $content) {
        $scoreRef[$class] = array();
        foreach ($content as $key => $valArr) {
            if ($key != 'InvolvedSubject' && $key != 'InvolvedRubricsItems') {
                $SubjectRubricsID = array_keys((array)$RubricsInfo[0][$key]);
                $rubrics = array();
                foreach($RubricsInfo[0][$key] as $rr){
                    $rubrics[$rr[$ItemDisplayLang]] = 0;
                }
                foreach ($valArr as $k => $v) {
                    if ($k == 'Total')
                        $rubrics['total'] = $v;
                    else {
                        $RubricsName = $RubricsInfo[0][$key][$k][$ItemDisplayLang];
                        $rubrics[$RubricsName] = $v;
                    }
                }
            }
        }
        array_push($scoreRef[$class], array('name' => $info[$key][$thisLang.$subjectDisplay[$SubjectDisplay]], 'rubrics' => $rubrics));
    }
/*    
    foreach ($rubricsArr as $key => $valArr) {
        if ($key != 'InvolvedSubject' && $key != 'InvolvedRubricsItems') {
            $rubrics = array();
            foreach ($rubricsRef as $rr) {
                $rubrics[$rr] = 0;
            }
            foreach ($valArr as $k => $v) {
                if ($k == 'Total')
                    $rubrics['total'] = $v;
                else
                    $rubrics[$rubricsRef[$k]] = $v;
            }
            array_push($scoreRef, array(
                'name' => $info[$key][$thisLang . $subjectDisplay[$SubjectDisplay]],
                'rubrics' => $rubrics
            ));
        }
    }*/
    return $scoreRef;
}
?>