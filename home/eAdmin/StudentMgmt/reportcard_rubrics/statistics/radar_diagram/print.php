<?php
// using: Ivan
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = array("ADMIN", "TEACHER");
$CurrentPage = "Statistics_RadarDiagram";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_module.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_rubrics.php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_topic.php");
include_once($PATH_WRT_ROOT."includes/reportcard_rubrics_custom/".$ReportCard_Rubrics_CustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcardrubrics_ui.php");

include_once('chart.php'); // Added By Phlilips

$lreportcard = new libreportcardrubrics_custom();
$lreportcard_module = new libreportcardrubrics_module();
$lreportcard_topic = new libreportcardrubrics_topic();
$lreportcard->Has_Access_Right($PageRight, $CurrentPage);

$lreportcard_ui = new libreportcardrubrics_ui();
$linterface = new interface_html();
### prepare tmp folder
$fs = new libfilesystem();
$folder_prefix = $intranet_root."/file/reportcard_rubrics/tmp_chart/";

### Clear the tmp png folder
if (file_exists($folder_prefix))
	$fs->folder_remove_recursive($folder_prefix);

### Create the tmp png folder if not exist
$fs->folder_new($folder_prefix);
chmod($folder_prefix, 0777);

### Print Button
$PrintBtn = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();", "submit2");

# Get Rubrics Info
$lreportcard_rubrics = new  libreportcardrubrics_rubrics();
$RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
$RubricsItemLevel = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID","Level",1);

### Prepare Global Variable to use functions in general.php
# Report Display Setting (Pass to general.php) 
$Global_DisplaySettingsArr['SchoolNameDisplay'] = $_REQUEST['SchoolNameDisplay'];
$ReportTitleArr = array_remove_empty($ReportTitleArr);
$numOfTitle = count($ReportTitleArr);
for ($i=0; $i<$numOfTitle; $i++)
	$ReportTitleArr[$i] = stripslashes($ReportTitleArr[$i]);
$Global_DisplaySettingsArr['ReportTitleArr'] = $ReportTitleArr;
$Global_DisplaySettingsArr['HeaderDataFieldArr'] = $_REQUEST['HeaderDataFieldArr'];
$Global_DisplaySettingsArr['NumOfHeaderDataPerRow'] = $_REQUEST['NumOfHeaderDataPerRow'];

# $ReportInfoArr (Pass to general.php)
$Global_ReportInfoArr['ModuleInfo'] = $lreportcard_module->Get_Module_Info("",$ModuleID);

# Get Student Info (Pass to general.php)
$Global_StudentInfoArr = $lreportcard->GET_STUDENT_BY_CLASSLEVEL($YearIDArr, $ParClassID="", $StudentIDArr, $ReturnAsso=1, $isShowStyle=0, $CheckActiveStudent=0);

### Gen Flash Chart
$chartWidth = "450";
$chartHeight = "350";
$StudentMarksheetScoreArr = $lreportcard->Get_Student_Marksheet_Score($SubjectIDArr, $StudentIDArr);

$numOfModule = count($ModuleID);
$numOfSubject = count($SubjectIDArr);
$thisChartSubject = array();
foreach($StudentIDArr as $StudentID)
{
	$StudentInvolvedRubrics = array();
	$SubjectLevelSum =  array();
	$SubjectRubricsTotal =  array();
	
	$RubricsCount = array();
	$StudentStudyTopicInfoArr = $lreportcard_topic->Get_Student_Study_Topic_Info($ModuleID, '', '', $StudentID);
	$thisChartArray = array();
	for ($i=0; $i<$numOfModule; $i++)
	{
		$thisModuleID = $ModuleID[$i];
		
		for ($j=0; $j<$numOfSubject; $j++)
		{
			$thisSubjectID = $SubjectIDArr[$j];
			$thisChartArray[$thisSubjectID] = 0;
			$StudentInvolvedSubject[$thisSubjectID] =  1;
			# Count the Student Studied Topic only
			foreach((array)$StudentStudyTopicInfoArr[$StudentID][$thisModuleID][$thisSubjectID] as $thisTopicID => $thisInfoArr)
			{
				# Record the Mark Count of each Rubrics Items
				$thisRubricsItemID = $StudentMarksheetScoreArr[$StudentID][$thisSubjectID][$thisTopicID][$thisModuleID]['RubricsItemID'];
				if(empty($thisRubricsItemID)) continue; // skip empty mark 
				
				$SubjectLevelSum[$thisSubjectID] += $RubricsItemLevel[$thisRubricsItemID] - 1;
				$SubjectRubricsTotal[$thisSubjectID]++;
//				$RubricsCount[$thisRubricsItemID][$thisSubjectID]++;
//				$SubjectRubricsTotal[$thisSubjectID]++;
//				$SubjectMarkDisplayArr[$StudentID]["RubricsCount"][$thisRubricsItemID][$thisSubjectID]++;
				$StudentInvolvedRubrics[$thisRubricsItemID] = 1;
			}
		}
	}
	foreach($SubjectLevelSum as $thisSubjectID => $thisSubjectLevelSum )
	{
		$thisSubjectRubricsTotal = $SubjectRubricsTotal[$thisSubjectID];
		$thisChartArray[$thisSubjectID] = $thisSubjectRubricsTotal==0?0:($thisSubjectLevelSum/$thisSubjectRubricsTotal);
		$SubjectMarkDisplayArr[$StudentID]["SubjectRubricsAverage"][$thisSubjectID] = $thisSubjectRubricsTotal==0?0:$thisSubjectLevelSum/$thisSubjectRubricsTotal;
	}
	
	array_push($thisChartSubject, generate_radar_highchart_diagram($StudentID, $thisChartArray, $lreportcard->Get_Subject_Info(), $SubjectDisplay, $intranet_session_language));
	
	$SubjectMarkDisplayArr[$StudentID]["InvolvedRubricsItems"] = array_keys($StudentInvolvedRubrics);
	$SubjectMarkDisplayArr[$StudentID]["InvolvedSubject"] = array_keys($StudentInvolvedSubject);
}
$FlashChartArr = $lreportcard_ui->Get_Statistics_RadarDiagram($SubjectMarkDisplayArr, $SubjectDisplay, $YearIDArr);
$k=0;
$NoOfStudent = count($FlashChartArr);
foreach((array)$FlashChartArr as $StudentID => $FlashChart)
{
	$chartArr[] = $FlashChart;
	$pagebreakafter = $k!=$NoOfStudent-1?' style="page-break-after:always;" ':'';
	$FlashDiv .= '<table cellpadding=2 cellspacing=2 align=center '.$pagebreakafter.'>';
		$FlashDiv .= '<tr><td>'.$lreportcard->Get_Title_Table().'</td></tr>';
		$FlashDiv .= '<tr><td>'.$lreportcard->Get_Header_Info_Table($StudentID).'</td></tr>';
		$FlashDiv .= '<tr><td style="text-align:center"><div id="div'.$k.'parent"><div id="div'.$k.'"></div></div></td></tr>';
		// Added by Philips
		$FlashDiv .= '<tr><td style="text-align:center"><div id="rchart_'.$StudentID.'" style="min-width: 400px; max-width: 600px; height: 400px; margin: 0 auto"></div><div></td></tr>';
	$FlashDiv .= '</table><br><br>';
	$k++;
}
//echo $lreportcard_ui->Include_JS_CSS(array('rubrics_css'));
echo $lreportcard_ui->Include_Template_CSS();
include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");
?>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/json/json2.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>includes/flashchart_basic_201301_in_use/js/swfobject.js"></script>	
<script type="text/javascript">

	function ofc_ready(){
		//alert('ofc_ready');
	}

	function open_flash_chart_data(id){
		return JSON.stringify(dataArr[id]);
	}

	function findSWF(movieName) {
	  if (navigator.appName.indexOf("Microsoft")!= -1) {
		return window[movieName];
	  } else {
		return document[movieName];
	  }
	}

	/*
	function jsonLoaded(id){
		//alert("json loaded and id: "+id);
	}	

	function onAutoReturnImgBinary(bmpBytStr){
		//alert("image binary string is automatically returned: "+bmpBytStr);
	}
	*/

	function onAutoPostImgBinary(id){
		//alert("image binary string is automatically posted and flash id: "+id);
		var swfDIV = document.getElementById("div"+id+"parent");
		//var swfDIV = document.getElementById("img"+id);
		//alert("swfDIV: "+swfDIV+" / width: "+swfDIV.width+" / height: "+swfDIV.height);
		while(swfDIV.hasChildNodes()){
			swfDIV.removeChild(swfDIV.firstChild);
		}
		var img = document.createElement("img");
		img.setAttribute("src", "<?=$intranet_httppath?>/file/reportcard_rubrics/tmp_chart/tmp_chart_png_"+id+".png");
		img.setAttribute("width", "<?=$chartWidth?>");
		img.setAttribute("height", "<?=$chartHeight?>");
		swfDIV.appendChild(img);
	}
	
	var chartNum = <?=count($chartArr)?>;
//	alert("chartNum: "+chartNum);
	
	var dataArr = new Array();
	<?php
		foreach( (array)$chartArr as $chartID => $chartJSON ) {
			echo "dataArr[ ".$chartID." ] = ".$chartJSON->toPrettyString()."\n";
		}
	?>
	
//	alert("dataArr length: "+dataArr.length);

</script>
<script type="text/javascript">
//  Flash Version of chart
//	var chartNum = <?//=count($chartArr)?>;
//	for(var k=0; k<chartNum; k++){
//		var flashvars = {id:k, postImgUrl:"../uploadFlashImage.php"};
//		swfobject.embedSWF("<?//=$PATH_WRT_ROOT?>//includes/flashchart_basic_201301_in_use/open-flash-chart.swf", "div"+k, "<?//=$chartWidth?>//", "<?//=$chartHeight?>", "9.0.0", "", flashvars);
//	}//end for
</script>

<style type='text/css' media='print'>
	.print_hide {display:none;}
</style>
<style type='text/css'>
	.report_table {border-collapse:collapse;}
</style>
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<div style="text-align:center;vertical-align:top;width:100%">
	<?=$FlashDiv?>
</div>
<!-- start of philips edited -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<div id="chart_container"></div>
<script type="text/javascript">
  <?php foreach($thisChartSubject as $thisCS){
      $chartSubjects = '';
      $chartScores = '';
      $chartStudent = $Global_StudentInfoArr[$thisCS['id']]['StudentName'];
      foreach($thisCS['name'] as $subjects){
       $chartSubjects .= "'$subjects', ";
      }
      foreach($thisCS['average'] as $scores){
       $chartScores .= "$scores, ";
      }
   ?>
     generate_radar_chart({
		id:'rchart_' + <?=$thisCS['id']?>,
		subjects: [<?=$chartSubjects?>],
		studentName: '<?=$chartStudent?>',
		scores: [<?=$chartScores?>],
		title: ''
     }); 
   <? 
  }?>
  function generate_radar_chart(obj){
  var studentArr = [];
  Highcharts.chart(obj.id, {
    chart: {
        polar: true,
        type: 'line'
    },
    title: {
        text: obj.title,
        x: -80
    },
    pane: {
        size: '80%'
    },
    xAxis: {
        categories: obj.subjects,
        tickmarkPlacement: 'on',
        lineWidth: 0,
        min: 0
    },
    yAxis: {
        gridLineInterpolation: 'polygon',
        lineWidth: 0,
        min: 0,
        max: 5,
        tickInterval: 1,
        gridLineColor: 'grey',
        gridLineWidth: 1
    },
    tooltip: {
        shared: true,
        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
    },

    legend: {
        align: 'right',
        verticalAlign: 'top',
        y: 70,
        layout: 'vertical'
    },
	credits:{
	 enabled: false
	},
	exporting:{
	 enabled: false
	},
    series: [{
        name: obj.studentName,
        data: obj.scores,
        pointPlacement: 'on',
        color: 'orange'
    },]

    });
	}
 </script>
 <!-- end of philips edited -->
<table border="0" cellpadding="4" width="95%" cellspacing="0" class="print_hide">
	<tr><td align="right"><span><?=$PrintBtn?></span></td></tr>
</table>

<?
intranet_closedb();
?>