<?php

function generate_class_subject_chart($rubricsArr, $SubjectDisplay, $info, $lang, $SubjectID, $TopicCode)
{
    // Array ( [F9A] => Array ( [InvolvedTopic] => Array ( [0] => 40 ) [InvolvedRubricsItems] =>
    // Array ( [0] => 10 [1] => 9 [2] => 8 ) [40] => Array ( [Total] => 6 [10] => 1 [9] => 3 [8] => 2 ) ) )
    global $PATH_WRT_ROOT, $Lang, $lreportcard;
    include_once ($PATH_WRT_ROOT . "includes/libreportcardrubrics_rubrics.php");
    // Get Rubrics Info
    $lreportcard_rubrics = new libreportcardrubrics_rubrics();
    // $RubricsInfo = $lreportcard_rubrics->Get_Rubics_Item_Info();
    // $RubricsInfo = BuildMultiKeyAssoc($RubricsInfo, "RubricsItemID");
    $RubricsInfo = $lreportcard_rubrics->Get_Subject_RubricsItem_Mapping();
    $subjectDisplay = array(
        'Abbr' => 'ABBR',
        'Desc' => 'DES',
        'ShortForm' => 'SNAME'
    );
    $rubricsRef = array(
        7 => "Not Good",
        8 => "Normal",
        9 => "Good",
        10 => "Excellent"
    );
    $thisLang = ($lang == 'en') ? 'EN_' : 'CH_';
    $subjectsRef = array();
    $scoreRef = array();
    
    // From Get_Statistics_Bar_Chart_Topic_Key() in libreportcardrubrics_ui.php
    if (! isset($lreportcard_topic)) {
        include_once ($PATH_WRT_ROOT . "includes/libreportcardrubrics_topic.php");
        $lreportcard_topic = new libreportcardrubrics_topic();
    }
    
    // Get_Topic_Info
    $TopicInfo = $lreportcard_topic->Get_Topic_Info($SubjectID);
    $TopicInfo = BuildMultiKeyAssoc($TopicInfo, "TopicID");
    $TopicDisplayLang = Get_Lang_Selection("TopicNameCh", "TopicNameEn");
    $ItemDisplayLang = Get_Lang_Selection("DescriptionCh", "DescriptionEn");
    /*
     * foreach ($rubricsArr['InvolvedTopic'] as $topic) {
     * $rubrics = array();
     * foreach ($rubricsRef as $rr) {
     * $rubrics[$rr] = 0;
     * }
     * foreach ($rubricsArr[$topic] as $k => $v) {
     * if ($k == 'Total')
     * $rubrics['total'] = $v;
     * else
     * $rubrics[$rubricsRef[$k]] = $v;
     * }
     * array_push($scoreRef, array(
     * 'name' => $TopicInfo[$topic][$TopicDisplayLang],
     * 'rubrics' => $rubrics
     * ));
     * }
     */
     echo '<br/>';
    foreach ($rubricsArr as $class => $content) {
        $scoreRef[$class] = array();
        foreach ($content as $title => $data) {
            if ($title != 'InvolvedTopic' && $title != 'InvolvedRubricsItems') {
                $rubrics = array();
                foreach ($RubricsInfo[0][$SubjectID] as $rr) {
                    $rubrics[$rr[$ItemDisplayLang]] = 0;
                }
                foreach ($data as $k => $v) {
                    if ($k == 'Total')
                        $rubrics['total'] = $v;
                    else {
                        $RubricsName = $RubricsInfo[0][$SubjectID][$k][$ItemDisplayLang];
                        $rubrics[$RubricsName] = $v;
                    }
                }
                array_push($scoreRef[$class], array(
                    'name' => $TopicInfo[$title][$TopicDisplayLang],
                    'rubrics' => $rubrics
                ));
            }
        }
    }
    return $scoreRef;
}

function get_SubjectById($SubjectDisplay, $info, $lang, $SubjectID)
{
    $subjectDisplay = array(
        'Abbr' => 'ABBR',
        'Desc' => 'DES',
        'ShortForm' => 'SNAME'
    );
    $thisLang = ($lang == 'en') ? 'EN_' : 'CH_';
    $subject = $info[$SubjectID][$thisLang . 'DES'];
    return $subject;
}
?>