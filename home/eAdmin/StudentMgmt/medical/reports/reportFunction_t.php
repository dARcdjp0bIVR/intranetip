<?php
/*
 * 	updated:	cameron
 * 	Log
 * 	Date:	
 */

// withRemark == "" don't show remark
function getConvulsionReport($startDate, $endDate, $studentList, $bodyParts, $syndrome, 
//							$gender, $sleepOvernight, 
							$outputFormat = 'html', $withRemark=""){
		$css = '<style>
					.medica_align_center{
						text-align: center !important ;
					}
					.boldFont {
						font-weight: bold !important ;
					}
				</style>';


		global $PATH_WRT_ROOT, $medical_cfg, $Lang, $linterface, $intranet_session_language;		
		$objDB = new libdb();
		
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();

		$Rows = array(); // data rows
		$obj = array();

		$startDate = ($startDate) ? $startDate : date("Y-m-d");
		$endDate = ($endDate) ? $endDate : date("Y-m-d");
		$sqlWhere = " AND g.RecordTime BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ";
			
		$bodyPartCond = "";
		if (count($bodyParts) > 0)
			$bodyPartCond =  ' AND v3.Lev3ID In ('. implode(",", (array)$bodyParts ).')';
		$syndromeCond = "";
		if (count($syndrome) > 0)
			$syndromeCond =  ' AND v4.Lev4ID In ('. implode(",", (array)$syndrome ).')';

//		$conds = "";
//		if ($gender)
//		{
//			$conds = " AND u.Gender='" . $gender . "'";
//			$sqlWhere .= $conds;
//		}
//		$sleepCond = ($sleepOvernight == "") ? "" : " AND IUPS.stayOverNight ='$sleepOvernight'";
//		$sqlWhere .=  ' AND g.UserID In (\''. implode("','", (array)$studentList ).'\')';

		$sql = "SELECT v3.Lev3ID, v3.Lev3Name, v4.Lev4ID, v4.Lev4Name 
				FROM MEDICAL_STUDENT_LOG_LEV3 v3 
					INNER JOIN MEDICAL_STUDENT_LOG_LEV4 v4 ON v4.Lev3ID = v3.Lev3ID
					INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID = v3.Lev2ID
					WHERE v2.Lev2Name='". $medical_cfg['studentLog']['level2']['specialStatus']."'"
					. $bodyPartCond . $syndromeCond . " ORDER BY v3.Lev3ID, v4.Lev4ID";
		$header_rs = $objDB->returnResultSet($sql);
		$nrHeader = count($header_rs);
//print $sql ."<br>";
//print "NrHeader=$nrHeader<br>";
//debug_r($header_rs);
//		$sql = 'select '.getNameFieldByLang2('u.').' as UserName, u.UserID from INTRANET_USER u Inner Join INTRANET_USER_PERSONAL_SETTINGS IUPS On IUPS.UserID = u.UserID where u.UserID In (\''. implode("','", (array)$studentList ).'\') '.$sleepCond . $conds;
		$sql = 'select '.getNameFieldByLang2('u.').' as UserName, u.UserID from INTRANET_USER u where u.UserID In (\''. implode("','", (array)$studentList ).'\') ';
		$student_rs = $objDB->returnResultSet($sql);
//print $sql ."<br>";
//debug_r($student_rs);
		$nrStudent = count($student_rs);
		if ($nrStudent > 0)
		{
			for($i = 0; $i < $nrStudent; $i++)
			{
				$studentName 	= stripslashes($student_rs[$i]["UserName"]);
				$studentID 		= $student_rs[$i]["UserID"];
				$obj[$studentID]["StudentName"] = $studentName;
				
				for($k = 0; $k < $nrHeader; $k++)
				{
					$lev3ID = $header_rs[$k]["Lev3ID"];
					$lev4ID = $header_rs[$k]["Lev4ID"];
					$lev4_total[$studentID][$lev3ID][$lev4ID] = 0;	// Initialized number of times per syndrome for a student
					$lev3_total[$studentID][$lev3ID] = 0;			// Initialized number of times per body part for a student
				}


				$sql = "SELECT 	date_format(g.RecordTime,'%Y-%m-%d') as RecordDate
						FROM  MEDICAL_STUDENT_LOG g
						INNER JOIN INTRANET_USER u ON u.UserID=g.UserID
						LEFT JOIN (SELECT v3.Lev3ID, v4.Lev4ID, p.StudentLogID 
									FROM MEDICAL_STUDENT_LOG_PARTS p 
									INNER JOIN MEDICAL_STUDENT_LOG_LEV3 v3 ON v3.Lev3ID = p.Level3ID 
									INNER JOIN MEDICAL_STUDENT_LOG_LEV4 v4 ON v4.Lev3ID = v3.Lev3ID
									INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID = v3.Lev2ID
									WHERE v2.Lev2Name='". $medical_cfg['studentLog']['level2']['specialStatus']."'"
									. $bodyPartCond . $syndromeCond . ") as s
 							ON s.StudentLogID=g.RecordID						 
						INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID = g.BehaviourTwo
						WHERE v2.Lev2Name='". $medical_cfg['studentLog']['level2']['specialStatus']."' 
							AND u.UserID=" .$studentID . $sqlWhere . " GROUP BY date_format(g.RecordTime,'%Y-%m-%d') ORDER BY 1";
				$date_rs = $objDB->returnResultSet($sql);
				$nrDates = count($date_rs);
				if ($nrDates > 0)
				{					
					for($h = 0; $h < $nrDates; $h++)
					{
						$recordDate = $date_rs[$h]['RecordDate'];
						$sql = "SELECT 	g.RecordID, date_format(g.RecordTime,'%H:%i') as RecordTime,g.Duration, g.Remarks
								FROM  MEDICAL_STUDENT_LOG g
								INNER JOIN INTRANET_USER u ON u.UserID=g.UserID
								LEFT JOIN (SELECT v3.Lev3ID, v4.Lev4ID, p.StudentLogID 
											FROM MEDICAL_STUDENT_LOG_PARTS p 
											INNER JOIN MEDICAL_STUDENT_LOG_LEV3 v3 ON v3.Lev3ID = p.Level3ID 
											INNER JOIN MEDICAL_STUDENT_LOG_LEV4 v4 ON v4.Lev3ID = v3.Lev3ID
											INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID = v3.Lev2ID
											WHERE v2.Lev2Name='". $medical_cfg['studentLog']['level2']['specialStatus']."'"
											. $bodyPartCond . $syndromeCond . ") as s
		 							ON s.StudentLogID=g.RecordID						 
								INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID = g.BehaviourTwo
								WHERE v2.Lev2Name='". $medical_cfg['studentLog']['level2']['specialStatus']."'
									AND date_format(g.RecordTime,'%Y-%m-%d')='".$recordDate."' 
									AND u.UserID=" .$studentID . $sqlWhere . " GROUP BY g.RecordID ORDER BY g.RecordTime";
						$rs = $objDB->returnResultSet($sql);
						$nrRow = count($rs);
//print $sql ."<br>";
//debug_r($rs);
				
						if ($nrRow > 0)
						{					
							for($j = 0; $j < $nrRow; $j++)
							{
								$recordID 	= $rs[$j]['RecordID'];
								$recordTime = $rs[$j]['RecordTime'];
								$duration 	= $rs[$j]['Duration'];
								$remarks 	= stripslashes($rs[$j]['Remarks']);
								
								$obj[$studentID]["Log"]["$recordDate"][$j]["RecordID"] 	= $recordID; 
//								$obj[$studentID]["Log"][$j]["RecordDate"] 	= $recordDate;
								$obj[$studentID]["Log"]["$recordDate"][$j]["RecordTime"] 	= $recordTime;
								$obj[$studentID]["Log"]["$recordDate"][$j]["Duration"] 		= $duration;
								$obj[$studentID]["Log"]["$recordDate"][$j]["Remarks"] 		= $remarks;
								
								for($k = 0; $k < $nrHeader; $k++)
								{
									$lev4ID = $header_rs[$k]["Lev4ID"];
									$obj[$studentID]["Log"]["$recordDate"][$j]["Syndrome"][$lev4ID] = 0;	// syndrome not exist by default
								}						
						
//debug_r($obj);
								$sql = "SELECT 	p.StudentLogID, p.Level3ID, p.Level4ID
										FROM  MEDICAL_STUDENT_LOG_PARTS p WHERE StudentLogID=".$recordID .
										str_replace("v3.Lev3ID","p.Level3ID",$bodyPartCond) . 
										str_replace("v4.Lev4ID","p.Level4ID",$syndromeCond) .
										" ORDER BY p.Level3ID, p.Level4ID " ;
		
								$rs2 = $objDB->returnResultSet($sql);
//print $sql ."<br>";
//debug_r($rs2);
						
								$nrSyndrome = count($rs2);
								for($k = 0; $k < $nrSyndrome; $k++)
								{
									$lev3ID = $rs2[$k]["Level3ID"];
									$lev4ID = $rs2[$k]["Level4ID"];
									$obj[$studentID]["Log"]["$recordDate"][$j]["Syndrome"][$lev4ID] = 1;	// syndrome exist, overwirte default value
									$lev4_total[$studentID][$lev3ID][$lev4ID] += 1;
									$lev3_total[$studentID][$lev3ID] += 1;
								}
							}
						}						
					}
				}
				else	// No record for this student
				{
					$obj[$studentID]["Log"] = array();
				}
			}	// End loop for each student
		}		// NrStudent > 0
		else	// No record
		{
			// do nothing
		}
//debug_r($obj);		
	//////////////////////////
	//START HANDLE OUTPUT THE RESULT
	//////////////////////////

	$HeaderRow1 = array();// columns in header row1	
	// HEADER
	$hHeader = '<tr class="tabletop"><th>'.$Lang['medical']['report_studentlog']['tableHeader']['date'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_studentlog']['tableHeader']['time'].'</th>';
	$hHeader .= '<th nowrap>'.$Lang['medical']['report_studentlog']['tableHeader']['duration'].'</th>';
	
	$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['date'];
	$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['time'];
	$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['duration'];

	$lev3Header = array();	
	for($i = 0; $i < $nrHeader; $i++)
	{	
		$lev3ID = $header_rs[$i]["Lev3ID"];
		$lev3Header[$lev3ID]["Name"] = stripslashes($header_rs[$i]["Lev3Name"]);
		$lev3Header[$lev3ID]["Number"] += 1;
	}
//debug_r($lev3Header);	
	foreach($lev3Header as $v)
	{
		$hHeader .= '<th class="medica_align_center" colspan="'.$v["Number"].'">'.$v["Name"].'</th>';
		$HeaderRow1[] = $v["Name"];
		for ($k=0; $k < $v["Number"]-1;$k++)
		{
			$HeaderRow1[] = "";	// pad number of columns
		}
	}
		
	if ($withRemark)
	{		
		$hHeader .= '<th class="medica_align_center">'.$Lang['medical']['report_studentlog']['tableHeader']['remarks'].'</th>';
		$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['remarks'];
	}
	$hHeader .= '</tr>';

	$HeaderRow2 = array();// columns in header row2
	$hHeader .= '<tr class="tabletop">';
	for($i = 0; $i < 3; $i++)
	{	
		$hHeader .= '<th>&nbsp;</th>';
		$HeaderRow2[] = "";
	}
	for($i = 0; $i < $nrHeader; $i++)
	{			
		$lev4Name = stripslashes($header_rs[$i]["Lev4Name"]);

		if ($intranet_session_language == "b5") // Chinese, show in vertical, don't work!
		{
			$chrArray = preg_split('//u',$lev4Name, -1, PREG_SPLIT_NO_EMPTY);
//debug_r($chrArray);			
			$dispLev4Name = implode("<br>",$chrArray); 
//debug_r($dispLev4Name);			
		}
		else
		{
			$dispLev4Name = $lev4Name;
		}
		$hHeader .= '<th class="medica_align_center" valign="top">'.$dispLev4Name.'</th>';		
		$HeaderRow2[] = $lev4Name;				
	}
	if ($withRemark)
	{		
		$hHeader .= '<th class="medica_align_center">&nbsp;</th>';
		$HeaderRow2[] = "";
	}
	$hHeader .= '</tr>';

//debug_r($intranet_session_language);	
//print "<table>".$hHeader."</table>";	



	/////////////////////////////////////////////
	// Output contents
	
	$h = "";
	
	foreach ($obj as $studentID => $student)	// Loop for each students
	{
//debug_r($studentID);
//debug_r($student);
		
		$prevDate = "";		// for comparison to print distinct date only
		 		
		$Rows[] = array($Lang['medical']['studentLog']['report_convulsion']['reportName'].': '.
					$student["StudentName"].' ( '.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.' )');
//debug_r($Rows);					
		
		$h .= '<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
				$Lang['medical']['studentLog']['report_convulsion']['reportName'].': '.
				$student["StudentName"].
				' (&nbsp;'.$startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$endDate.'&nbsp;)'.
			  '</div><br />';
						
		$nrLog = count($student["Log"]);	// Number of log record for the student
//debug_r($nrLog);		
		if ($nrLog > 0)
		{
			$Rows[] =  $HeaderRow1;
			$Rows[] =  $HeaderRow2;
			
//			$exportColumn[0] = $HeaderColumn;
			$exportColumn[0] = "";
			$h .= '<table border = "0" class="common_table_list_v30">';
			$h .= $hHeader;	
			
			
			foreach($student["Log"] as $recordDate => $rs)		 
			{
				$nrRecOfADate = count($rs);
				
				$h .= '<tr>';
				$h .= '<td rowspan="'.$nrRecOfADate.'">'.$recordDate.'</td>';
				
//debug_r($student["Log"][$i]);				
//				$curDate = $student["Log"][$i]["RecordDate"];
//debug_r($curDate);
				
				for ($j = 0; $j < $nrRecOfADate; $j++)
				{
					$Details = array();
					$h .= ($j>0) ? '<tr>' : '';
					$Details[] = ($j>0) ? '' : $recordDate;
					
					$h .= '<td>'.$rs[$j]["RecordTime"].'</td>';
					$Details[] = $rs[$j]["RecordTime"];
					$duration = $rs[$j]["Duration"];
					$dispDuration = "";
//debug_r($h);				
					if (substr($duration,0,2) != "00") 
						$dispDuration .= intval(substr($duration,0,2)) . $Lang['medical']['studentLog']['hour'];
					if (substr($duration,3,2) != "00") 
						$dispDuration .= intval(substr($duration,3,2)) . $Lang['medical']['studentLog']['minute'];
					if (substr($duration,6,2) != "00") 
						$dispDuration .= intval(substr($duration,6,2)) . $Lang['medical']['studentLog']['second'];
					$h .= '<td>'.$dispDuration.'</td>';
					$Details[] = $dispDuration;
				
					// Syndrome
					$nrSyndrome = count($rs[$j]["Syndrome"]);
					foreach($rs[$j]["Syndrome"] as $syndrome)
					{
						$h .= '<td class="medica_align_center">'.(($syndrome == 1)? $linterface->Get_Tick_Image() : "&nbsp;").'</td>';
						$Details[] = $syndrome;
					}
				
					// remarks	
					if ($withRemark)
					{		
						$h .= '<td>'.nl2br($rs[$j]["Remarks"]).'</td>';
						$Details[] = $rs[$j]["Remarks"];
					}	
					$h .= '</tr>';	
					$Rows[] = $Details;
				}

			}
			
			
			// Sub-Total
			$SubTotalRow = array();
			$h .= '<tr class="total_row">';
			$h .= '<td class="boldFont">' . $Lang['medical']['report_studentlog']['subTotal'] . '</td>';
			$SubTotalRow[] = $Lang['medical']['report_studentlog']['subTotal'];
			
			for($i = 0; $i < 2; $i++)
			{	
				$h .= '<td>&nbsp;</td>';
				$SubTotalRow[] = "";
			}
			
			for($i = 0; $i < $nrHeader; $i++)
			{			
				$lev3ID = $header_rs[$i]["Lev3ID"];
				$lev4ID = $header_rs[$i]["Lev4ID"];
				$h .= '<td class="medica_align_center boldFont">'. (($lev4_total[$studentID][$lev3ID][$lev4ID] > 0 ) ? $lev4_total[$studentID][$lev3ID][$lev4ID] : '-') . '</td>';
				$SubTotalRow[] = ($lev4_total[$studentID][$lev3ID][$lev4ID] > 0 ) ? $lev4_total[$studentID][$lev3ID][$lev4ID] : '-' ;				
			}
			if ($withRemark)
			{		
				$h .= '<td>&nbsp;</td>';
				$SubTotalRow[] = "";
			}	
			$h .= '</tr>';			
			$Rows[] = $SubTotalRow;

			// Grand Total
			$GrandTotalRow = array();	
			$h .= '<tr class="tabletop">';
			$h .= '<th class="boldFont">'.$Lang['medical']['report_studentlog']['grandTotal'] . '</th>';
			$GrandTotalRow[] = $Lang['medical']['report_studentlog']['grandTotal'];
			$h .= '<th class="boldFont">' . count($obj[$studentID]["Log"]) . '</th>';
			$GrandTotalRow[] = count($obj[$studentID]["Log"]);
			$h .= '<th>&nbsp;</th>';
			$GrandTotalRow[] = "";
			
			foreach($lev3Header as $lev3ID => $v)
			{
				$h .= '<th class="medica_align_center boldFont" colspan="'.$v["Number"].'">'. (($lev3_total[$studentID][$lev3ID] > 0 ) ? $lev3_total[$studentID][$lev3ID] : '-') . '</th>';
				$GrandTotalRow[] = ($lev3_total[$studentID][$lev3ID] > 0 ) ? $lev3_total[$studentID][$lev3ID] : '-' ;
				for ($k=0; $k < $v["Number"]-1;$k++)
				{
					$GrandTotalRow[] = "";	// pad number of columns
				}
			}
			if ($withRemark)
			{		
				$h .= '<th>&nbsp;</th>';
				$GrandTotalRow[] = "";
			}	
			$h .= '</tr>';
			$Rows[] = $GrandTotalRow;			
			$h .= '</table>';
			$h .= '<br /><br /><br />';
		}
		else	// No record
		{
			$h .= '<h4 class="medica_align_center">' . $Lang['SysMgr']['Homework']['NoRecord'] . "</h4>";
			$h .= '<br>';
			$Rows[] = array($Lang['SysMgr']['Homework']['NoRecord']); 			
		}
		$Rows[] = array("");	// Line break;
	}	// for each student


	switch ($outputFormat)
	{
		case "html":
			return $css.$h;
		break;

		default:
		case "csv":
			$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");						
			$lexport->EXPORT_FILE('export.csv', $exportContent);
			return true;
		break;
	}
} // end of function





function getConvulsionReport2($data, $outputFormat = 'html'){
	$css = '<style>
				.medica_align_center{
					text-align: center !important ;
				}
			</style>';

//debug_r ($_POST);

	global $PATH_WRT_ROOT, $medical_cfg, $Lang, $linterface, $intranet_session_language;		
	$objDB = new libdb();
	
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$Rows = array(); // data rows
	$obj = array();

	//////////////////////////
	//START HANDLE OUTPUT THE RESULT
	//////////////////////////

	$HeaderRow = array();// columns in header row1	
	// HEADER
	$hHeader = '<tr class="tabletop"><th>'.$Lang['medical']['report_general']['tableHeader']['studentName'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['time'].'</th>';
//	$hHeader .= '<th>'.$Lang['medical']['report_studentlog2']['tableHeader']['duration'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_studentlog2']['tableHeader']['type'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['item'].'</th>';
//	$hHeader .= '<th>'.$Lang['medical']['report_studentlog2']['tableHeader']['bodyParts'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['remarks'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['lastUpdatedBy'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['lastUpdatedOn'].'</th>';
	$hHeader .= '</tr>';	
	
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['studentName'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['time'];
//	$HeaderRow[] = $Lang['medical']['report_studentlog2']['tableHeader']['duration'];
	$HeaderRow[] = $Lang['medical']['report_studentlog2']['tableHeader']['type'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['item'];
//	$HeaderRow[] = $Lang['medical']['report_studentlog2']['tableHeader']['bodyParts'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['remarks'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['lastUpdatedBy'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['lastUpdatedOn'];

//print "<table>".$hHeader."</table>";	


	/////////////////////////////////////////////
	// Output contents
	
	$h = "";
	$exportColumn[0] = "";
	if (count($data) > 0)
	{
		$h .= '<table border = "0" class="common_table_list_v30">';
		$h .= $hHeader;			 
		$exportColumn[0] = $HeaderRow;
	
		foreach ($data as $studentID => $log)	// Loop for each students
		{
//debug_r($studentID);
//debug_r($log);
			$studentName = stripslashes($log["Name"]);
									
			$nrLog = count($log["recordDetail"]);	// Number of log record for the student
//debug_r($nrLog);
			$h .= '<tr>';
			$h .= '<td rowspan="'.$nrLog.'">'.$studentName.'</td>';
			$i = 0;
			foreach ($log["recordDetail"] as $k => $v)
			{
//debug_r($v);				
				$Details = array();
				$h .= ($i>0) ? '<tr>' : '';
				$Details[] = ($i>0) ? '' : $studentName;

				$recordTime = substr($v["RecordTime"],11,5);	// Format: Hour:Minute	
				$h .= '<td>'.$recordTime.'</td>';
				$Details[] = $recordTime;

				$h .= '<td>'.($v["Lev1Name"] ? $v["Lev1Name"] : '-').'</td>';
				$Details[] = $v["Lev1Name"] ? $v["Lev1Name"] : '-';

				$h .= '<td>'.($v["Lev2Name"] ? $v["Lev2Name"] : '-').'</td>';
				$Details[] = $v["Lev2Name"] ? $v["Lev2Name"] : '-';

				$h .= '<td>'.($v["Remarks"] ? $v["Remarks"] : '-').'</td>';
				$Details[] = $v["Remarks"] ? $v["Remarks"] : '-';

				$h .= '<td>'.$v["ConfirmedName"].'</td>';
				$Details[] = $v["ConfirmedName"];

				$h .= '<td>'.$v["DateModified"].'</td>';
				$Details[] = $v["DateModified"];
				
				$h .= '</tr>';	
				$Rows[] = $Details;
				
				$i++;						
			}	// loop for log of a student
		}	// loop for number of students
		
		$h .= '</table>';
		$h .= '<br/>';
	}		// record exists
	else	// No record
	{
		$h .= '<h4 class="medica_align_center">' . $Lang['SysMgr']['Homework']['NoRecord'] . "</h4>";
		$h .= '<br/>';
		$Rows[] = array($Lang['SysMgr']['Homework']['NoRecord']); 			
	}


	switch ($outputFormat)
	{
		case "html":
			return $css.$h;
		break;

		default:
		case "csv":
			$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");						
			$lexport->EXPORT_FILE('export.csv', $exportContent);
			return true;
		break;
	}
} // end of function

