<?php

//using: 
/*
 *  2018-05-29 [Cameron]
 *      - fix: apply stripslashes to ReasonName so that it can escape back slashes
 *
 */

$startDate = $date;
$endDate = $date;
$studentDayInfo = $objMedical->getSleepReportData($reasonID='', $userID, $startDate,$endDate, $getReasonName=true, $getReport3Data=true);

list($year,$month,$day) = explode('-', $date);
$month = str_pad($month, 2, "0", STR_PAD_LEFT);
$day = str_pad($day, 2, "0", STR_PAD_LEFT);
$objUser = new libuser($userID);
$disStudentType = $objUser->UserNameLang();
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

$style = <<<HTML
<style>
.thickbox tr td{
	min-height:0px !important;
	height:20px !important;
}
#editBottomDiv{
	position: absolute;
	bottom: 0px;
	width: 95%;
}
</style>
HTML;
echo $style;
$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentSleep']['report3']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$date.'&nbsp;)'.
		'</div>';

$resultHTML = <<<HTML
	<br />
	{$resultHeader}
	<br style="clear:both"/>
	<table class="common_table_list_v30 thickbox">
		<colgroup>
			<col style="width: 5%"/>
			<col style="width: 25%"/>
			<col style="width: 10%"/>
			<col style="width: 60%"/>
		</colgroup>
		<thread>
			<th>#</th>
			<th>{$Lang['medical']['sleep']['tableHeader']['SleepCondition']}</th>
			<th>{$Lang['medical']['sleep']['tableHeader']['Frequency']}</th>
			<th>{$Lang['medical']['sleep']['tableHeader']['Remarks']}</th>
		</thread>
HTML;
echo $resultHTML;
	$i=0;
	if(count($studentDayInfo[$year][$month][$day])>0){
		foreach($studentDayInfo[$year][$month][$day] as $dayDetailList){
			foreach($dayDetailList as $dayDetail){
				++$i;
				$remarks = stripslashes($dayDetail['Remarks']);
				$reasonName = stripslashes($dayDetail['ReasonName']);
				$resultHTML = <<<HTML
					<tr>
						<td>{$i}</td>
						<td>{$reasonName}</td>
						<td>{$dayDetail['Frequency']} </td>
						<td>{$remarks} </td>
					</tr>
HTML;
				echo $resultHTML;
			}		
		}
	}
	else{
		$resultHTML = <<<HTML
		<tr><td colspan="4" align="center">{$Lang['General']['NoRecordAtThisMoment']}</td></tr>
HTML;
		echo $resultHTML;
		
	}	
$resultHTML = <<<HTML
	</table>
HTML;
echo $resultHTML;
?>
<div id="editBottomDiv" class="edit_bottom_v30">
	<?php echo $htmlAry['cancelBtn']; ?>
	<p class="spacer"></p>
</div>
