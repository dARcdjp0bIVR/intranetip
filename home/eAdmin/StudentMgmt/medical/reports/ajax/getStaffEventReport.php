<?php 
// using: 
/*
 * 	Log
 * 
 *  2018-12-31 Cameron
 *      - create this file
 */

//include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");


$fitler = array();
$filter['staffID'] = $_POST['staffID'];
$filter['staffEventTypeID'] = $_POST['eventTypeID'];
$filter['staffEventTypeLev2ID'] = $_POST['eventTypeLev2ID'];
$filter['startDate'] = $_POST['startDate'];
$filter['endDate'] = $_POST['endDate'];
$filter['displayOrder'] = $_POST['displayOrder'];


//$reportName= 'staffEventReport';
//$space = ($intranet_session_language == "en" ) ? "&nbsp;" : "";


switch($action){
	case 'print':
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
			div.reportSection { page-break-before:always !important}
			tr    { page-break-inside:avoid !important; page-break-after:auto !important}
			td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
		}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		</td></tr>
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		<br />
		<br />
		</div>
HTML;
		echo $printHTML;

		echo $objMedical->getStaffEventReportLayout($filter, $action);
		
		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	
	case 'export':
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		$rows = array();  // data rows
		$exportColumn[0] = "";
		$exportContent = '';
		
		$staffEventAry = $objMedical->getStaffEventReport($filter);
		
		if (count($staffEventAry) == 0) {
		    $rows[] = array($Lang['General']['NoRecordAtThisMoment']);
		    $exportContent = $rows;
		}
 		else {
 		    $headerRow = array();
 		    $headerRow[] = $Lang['medical']['staffEvent']['tableHeader']['StaffName'];
 		    $headerRow[] = $Lang['General']['Time'];
	        $headerRow[] = $Lang['medical']['staffEvent']['tableHeader']['EventType'];
	        $headerRow[] = $Lang['medical']['staffEvent']['HasReportedInjury'];
	        $headerRow[] = $Lang['medical']['staffEvent']['DaysOfInjuryLeave'];
	        $headerRow[] = $Lang['medical']['staffEvent']['DaysOfSickLeave'];
	        $headerRow[] = $Lang['medical']['reportStaffEvent']['TotalLeave'];
	        $headerRow[] = $Lang['medical']['staffEvent']['remark'];
	        $headerRow[] = $Lang['medical']['general']['lastModifiedBy'];
	        $headerRow[] = $Lang['medical']['general']['dateModified'];
	        $headerRow[] = $Lang['medical']['event']['Attachment'];
	        $exportColumn[0] = $headerRow;
	        
	        $staffEventAssoc = BuildMultiKeyAssoc($staffEventAry, array('EventDate', 'StaffEventID'));
	        foreach((array)$staffEventAssoc as $eventDate => $_staffEventAssoc) {
	            $rows = array();  // data rows
	            
	            foreach((array)$_staffEventAssoc as $__eventID => $__staffEventAssoc) {
	                $attachment = $__staffEventAssoc['AttachmentStaffEventID'] ? $Lang['General']['Yes2']: '-';
    	            $row = array();
		            $disReportedInjury = $__staffEventAssoc['IsReportedInjury'] ? $Lang['General']['Yes'] : $Lang['General']['No'];
		            $row[] = $__staffEventAssoc['StaffName'];
		            $row[] = $__staffEventAssoc['StartTime'];
		            $row[] = $__staffEventAssoc['EventType'];
		            $row[] = $disReportedInjury;
		            $row[] = $__staffEventAssoc['InjuryLeave'];
		            $row[] = $__staffEventAssoc['SickLeave'];
		            $row[] = $__staffEventAssoc['TotalLeave'];
		            $row[] = $__staffEventAssoc['Remark'];
		            $row[] = $__staffEventAssoc['UpdatedBy'];
		            $row[] = $__staffEventAssoc['DateModified'];
		            $row[] = $attachment;
		            $rows[] = $row;
	            }

	            $exportContent .= $eventDate . "\r\n";
	            $exportContent .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
	            $exportContent .= "\r\n";
	            $exportContent .= "\r\n";
	        }
 		}
 		
 		$lexport->EXPORT_FILE($Lang['medical']['menu']['staffEvent'].'.csv', $exportContent);
 		exit();
		break;
		
	default:
		//////////////// INPUT CHECKING /////////
		echo $linterface->Include_Thickbox_JS_CSS();
		echo '<div class="Conntent_tool">';
		echo '<a class="print tablelink" name="printBtn" id="printBtn" style="float:none;display:inline;">'.$Lang['Btn']['Print'].'</a>&nbsp;';
		echo '<a class="export tablelink" name="exportBtn" id="exportBtn" style="float:none;display:inline;">'.$Lang['Btn']['Export'].'</a>';
		echo '</div>';

		echo "<br/><br/><br/>";
		
		echo $objMedical->getStaffEventReportLayout($filter);
		
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getStaffEventReport&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getStaffEventReport&action=print");
			$('#reportForm').submit();		
		});
		
		$('a.fixedSizeThickboxLink').click( function() {
			Id = $(this).attr('data-Id');
			load_dyn_size_thickbox_ip('{$Lang['medical']['report_general']['attachmentTitle']}', 'onloadThickBox();', inlineID='', defaultHeight=250, defaultWidth=400);
		});	
		function onloadThickBox() {
			$('div#TB_ajaxContent').load(
				"?t=management.ajax.getStaffEventAttachmentList",{ 
					'Id': Id
				},
				function(ReturnData) {
				}
			);
		}
		</script>
HTML;
		echo $script;
	break;
}


?>