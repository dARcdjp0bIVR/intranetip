<?php 
// using: 
/*
 * 	Log
 * 
 *  2019-01-02 Cameron
 *      - create this file
 */

//include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");


$fitler = array();
$filter['studentList'] = $_POST['studentList'];
$filter['eventTypeID'] = $_POST['eventTypeID'];
$filter['startDate'] = $_POST['startDate'];
$filter['endDate'] = $_POST['endDate'];
$filter['displayOrder'] = $_POST['displayOrder'];
//$filter['displayEmptyData'] = $_POST['displayEmptyData'];

//$reportName= 'staffEventReport';
//$space = ($intranet_session_language == "en" ) ? "&nbsp;" : "";


switch($action){
	case 'print':
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
			div.reportSection { page-break-before:always !important}
			tr    { page-break-inside:avoid !important; page-break-after:auto !important}
			td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
		}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		</td></tr>
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		<br />
		<br />
		</div>
HTML;
		echo $printHTML;

		echo $objMedical->getStudentEventReportLayout($filter, $action);
		
		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	
	case 'export':
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();
		$rows = array();  // data rows
		$exportColumn[0] = "";
		$exportContent = '';
		
		$studentEventAry = $objMedical->getStudentEventReport($filter);
		
		if (count($studentEventAry) == 0) {
		    $rows[] = array($Lang['General']['NoRecordAtThisMoment']);
		    $exportContent = $rows;
		}
 		else {
 		    $headerRow = array();
 		    $headerRow[] = $Lang['medical']['event']['tableHeader']['StudentName'];
 		    $headerRow[] = $Lang['medical']['event']['tableHeader']['StartTime'];
 		    $headerRow[] = $Lang['medical']['event']['tableHeader']['EventType'];
 		    $headerRow[] = $Lang['medical']['event']['tableHeader']['Place'];
 		    $headerRow[] = $Lang['medical']['event']['Summary'];
 		    $headerRow[] = $Lang['medical']['event']['tableHeader']['AffectedPerson'];
	        $headerRow[] = $Lang['medical']['general']['lastModifiedBy'];
	        $headerRow[] = $Lang['medical']['general']['dateModified'];
	        $headerRow[] = $Lang['medical']['event']['Attachment'];
	        $exportColumn[0] = $headerRow;
	        
	        $studentEventAssoc = BuildMultiKeyAssoc($studentEventAry, array('EventDate', 'EventID'));
	        foreach((array)$studentEventAssoc as $eventDate => $_studentEventAssoc) {
	            $rows = array();  // data rows
	            
	            foreach((array)$_studentEventAssoc as $__eventID => $__studentEventAssoc) {
	                
	                $attachment = $__studentEventAssoc['AttachmentEventID'] ? $Lang['General']['Yes2']: '-';
    	            $row = array();
		            $row[] = str_replace("<br>","",$__studentEventAssoc['Student']);
		            $row[] = $__studentEventAssoc['StartTime'];
		            $row[] = stripslashes($__studentEventAssoc['EventTypeName']);
		            $row[] = $__studentEventAssoc['Location'];
		            $row[] = $__studentEventAssoc['Summary'];
		            $row[] = str_replace("<br>","",$__studentEventAssoc['AffectedPerson']);
		            $row[] = $__studentEventAssoc['UpdatedBy'];
		            $row[] = $__studentEventAssoc['DateModified'];
		            $row[] = $attachment;
		            $rows[] = $row;
	            }

	            $exportContent .= $eventDate . "\r\n";
	            $exportContent .= $lexport->GET_EXPORT_TXT_WITH_REFERENCE($rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");
	            $exportContent .= "\r\n";
	            $exportContent .= "\r\n";
	        }
 		}
 		
 		$lexport->EXPORT_FILE($Lang['medical']['menu']['event'].'.csv', $exportContent);
 		exit();
		break;
		
	default:
		//////////////// INPUT CHECKING /////////
		echo $linterface->Include_Thickbox_JS_CSS();
		echo '<div class="Conntent_tool">';
		echo '<a class="print tablelink" name="printBtn" id="printBtn" style="float:none;display:inline;">'.$Lang['Btn']['Print'].'</a>&nbsp;';
		echo '<a class="export tablelink" name="exportBtn" id="exportBtn" style="float:none;display:inline;">'.$Lang['Btn']['Export'].'</a>';
		echo '</div>';

		echo "<br/><br/><br/>";
		
		echo $objMedical->getStudentEventReportLayout($filter);
		
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getStudentEventReport&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getStudentEventReport&action=print");
			$('#reportForm').submit();		
		});
		
		$('a.fixedSizeThickboxLink').click( function() {
			Id = $(this).attr('data-Id');
			load_dyn_size_thickbox_ip('{$Lang['medical']['report_general']['attachmentTitle']}', 'onloadThickBox();', inlineID='', defaultHeight=250, defaultWidth=400);
		});	
		function onloadThickBox() {
			$('div#TB_ajaxContent').load(
				"?t=management.ajax.getStudentEventAttachmentList",{ 
					'Id': Id
				},
				function(ReturnData) {
				}
			);
		}
		</script>
HTML;
		echo $script;
	break;
}


?>