<?php 
// using: 
/*
 * 	Log
 * 	Date:	2013-12-17 [Cameron] hide $_POST['gender'] and [$sleepHeader], remove : before these
 */

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_startDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $r_endDate)){
	echo 'Start Report Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

$timePeriodHeader = $_POST['timePeriod']-1;
$timePeriodHeader= ($timePeriodHeader==0)?'-1':$timePeriodHeader;
$sleepHeader = ($_POST['sleep']==2)?0:1;

$genderHeader ='';
if($_POST['gender']==2){
	$genderHeader = 'M';
}
elseif($_POST['gender']==3){
	$genderHeader = 'F';
}
$reportName= 'mealReport2';
switch($action){
	case 'print':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "MEAL_REPORTPRINT"))){
	        header("Location: /");
	        exit();
	    }
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
		}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		</div>
		<br />
		<br />
		
HTML;
		echo $printHTML;

		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_meal']['search']['timePeriodOption'][$timePeriodHeader].
		$Lang['medical']['report_meal']['search']['reportTypeSelect'][$reportName].' '.
//		$Lang['medical']['report_meal']['search']['genderOption'][$_POST['gender']].
//		$Lang['medical']['report_meal']['search']['sleepOption'][$sleepHeader].
		'(&nbsp;'.$r_startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$r_endDate.'&nbsp;)'.
		'</div><br /><br />';

		echo $resultHeader;
		
		echo getMealReport2($_POST['r_startDate'],$_POST['r_endDate'],
						$_POST['studentList'], $_POST['r_status'], 
						$_POST['timePeriod'], //$_POST['gender'], $_POST['sleep'],
						$reportType, $outputFormat = 'print',$_POST['r_withRemarks']);
//		echo '<br /><span class="tabletextremark">'.$Lang['medical']['report_meal']['remarks'].'</span>';
		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	case 'export':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "MEAL_REPORTEXPORT"))){
	        header("Location: /");
	        exit();
	    }
		getMealReport2($_POST['r_startDate'],$_POST['r_endDate'],
						$_POST['studentList'], $_POST['r_status'], 
						$_POST['timePeriod'], //$_POST['gender'], $_POST['sleep'],
							$reportType, $outputFormat = 'csv',$_POST['r_withRemarks']);
	exit();
	break;	
	default:
		//////////////// INPUT CHECKING /////////
		echo '<div class="Conntent_tool">';
		if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "MEAL_REPORTPRINT")))
		  echo '<a class="print tablelink" name="printBtn" id="printBtn" style="float:none;display:inline;">'.$Lang['Btn']['Print'].'</a>&nbsp;';
	    if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "MEAL_REPORTEXPORT")))
		  echo '<a class="export tablelink" name="exportBtn" id="exportBtn" style="float:none;display:inline;">'.$Lang['Btn']['Export'].'</a>';
		echo '</div>';
				$resultHeader = 
		'<br /><br /><br /><div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_meal']['search']['timePeriodOption'][$timePeriodHeader].
		$Lang['medical']['report_meal']['search']['reportTypeSelect'][$reportName].': '.
//		$Lang['medical']['report_meal']['search']['genderOption'][$_POST['gender']].
//		$Lang['medical']['report_meal']['search']['sleepOption'][$sleepHeader].
		'(&nbsp;'.$r_startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$r_endDate.'&nbsp;)'.
		'</div><br /><br />';

		echo $resultHeader;

		echo getMealReport2($_POST['r_startDate'],$_POST['r_endDate'],
						$_POST['studentList'], $_POST['r_status'], 
						$_POST['timePeriod'], //$_POST['gender'],
						$reportType, $outputFormat = 'html',$_POST['r_withRemarks']);
//		echo '<br /><span class="tabletextremark">'.$Lang['medical']['report_meal']['remarks'].'</span>';
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getMealReport2&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getMealReport2&action=print");
			$('#reportForm').submit();		
		});
		</script>
HTML;
		echo $script;
	break;
}
?>