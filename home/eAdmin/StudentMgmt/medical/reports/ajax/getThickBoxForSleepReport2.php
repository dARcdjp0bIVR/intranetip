<?php

//using: 
/*
 *  2018-05-29 [Cameron]
 *      - fix: apply stripslashes to ReasonName so that it can escape back slashes
 *
 */

$startDate = $r_startDate;
$endDate = $r_endDate;
$userID = $r_studentId;
$typeID = $r_typeID;
switch($typeID){
	case $medical_cfg['studentSleep']['sleepWellID']:
		$type = 'sleepWell';
		break;
	case $medical_cfg['studentSleep']['healthProblemID']:
		$type = 'healthProblem';
		break;
	case $medical_cfg['studentSleep']['emotionProblemID']:
		$type = 'emotionProblem';
		break;
	case $medical_cfg['studentSleep']['otherProblemID']:
		$type = 'otherProblem';
		break;
	default:
		exit;
		break;
}

$studentDayInfo = $objMedical->getSleepReportData($reasonID, $userID, $startDate,$endDate, $getReasonName=true, $getReport3Data=true);
//debug_r($studentDayInfo);

/*
 * $data => (
 * 		array => (
 * 			[ReasonName]
 * 			[date] (YYYY-MM-DD)
 * 			[frequency]
 * 			[remarks]
 * 		),
 * 		array => (
 * 			[ReasonName]
 * 			[date] (YYYY-MM-DD)
 * 			[frequency]
 * 			[remarks]
 * 		)
 * )
 */
$data = array();
$totalCount = 0;
foreach ($studentDayInfo as $year => $d1) {
	foreach ($d1 as $month => $d2) {
		foreach ($d2 as $day => $d3) {
			foreach ($d3 as $reasonID => $d4) {
				foreach ($d4 as $reasonList) {
					// Check if the data match the type
					if(strpos($reasonList['ReasonName'],$medical_cfg['studentSleep'][$type])!==false){
						$data[] = array(
							'date' => ($year . '-' . $month . '-' . $day),
						    'reasonName' => stripslashes($reasonList['ReasonName']),
							'frequency' => $reasonList['Frequency'],
							'remarks' => stripslashes($reasonList['Remarks'])
						);
						$totalCount += $reasonList['Frequency'];
					}
				}
			}
		}
	}
}

// Sort the data by date using usort
function sortByDate($a,$b){
	return strcmp($a['date'],$b['date']);
}
usort($data,"sortByDate");

$objUser = new libuser($userID);
$disStudentType = $objUser->UserNameLang();
$htmlAry['cancelBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");

$style = <<<HTML
<style>
.thickbox tr td{
	min-height:0px !important;
	height:20px !important;
}
</style>
HTML;
echo $style;
$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentSleep']['report2']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.'&nbsp;)'.
		'</div>';

$resultHTML = <<<HTML
	<br />
	{$resultHeader}
	<br style="clear:both"/>
	{$Lang['medical']['general']['report']['total']} {$totalCount} {$Lang['medical']['report_sleep']['tableContent']['frequency']}
	<table class="common_table_list_v30 thickbox">
		<colgroup>
			<col style="width: 15%"/>
			<col style="width: 20%"/>
			<col style="width: 5%"/>
			<col style="width: 60%"/>
		</colgroup>
		<thread>
			<th>{$Lang['medical']['report_general']['searchMenu']['date']}</th>
			<th>{$Lang['medical']['sleep']['tableHeader']['Reason']}</th>
			<th>{$Lang['medical']['sleep']['tableHeader']['Frequency']}</th>
			<th>{$Lang['medical']['sleep']['tableHeader']['Remarks']}</th>
		</thread>
HTML;
echo $resultHTML;

foreach ($data as $d){
	$resultHTML = <<<HTML
		<tr>
			<td>{$d['date']}</td>
			<td>{$d['reasonName']}</td>
			<td>{$d['frequency']} </td>
			<td>{$d['remarks']} </td>
		</tr>
HTML;
	echo $resultHTML;
}
				
				
$resultHTML = <<<HTML
	</table>
	<br style="clear:both"/>
HTML;
echo $resultHTML;
?>
<div id="editBottomDiv" class="edit_bottom_v30">
	<?php echo $htmlAry['cancelBtn']; ?>
	<p class="spacer"></p>
</div>
