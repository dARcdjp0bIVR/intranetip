<?php 
// using: pun
/*
 * 	Log
 * 	Date:	2014-01-03 
 */

include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $startDate)){
	echo 'Report Start Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $endDate)){
	echo 'Report End Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

if(!is_numeric($startTimePeriod) || intval($startTimePeriod) < 0 /*|| intval($startTimePeriod) > 47*/){
	echo 'Report Start Time Format Error!';
	return;
}
if(!is_numeric($endTimePeriod) || intval($endTimePeriod) < 0 /*|| intval($endTimePeriod) > 47*/){
	echo 'Report End Time Format Error!';
	return;
}

$startTime = timePeriodToTime(intval($startTimePeriod),true);
$endTime = timePeriodToTime(intval($endTimePeriod),false);

$reportName= 'bowelReport';

$dispGender = "";

//debug_r(returnListOfYearMonth($startDate,$endDate));
//exit();
$bowelStudentData = $objMedical->getBowelReportData($reasonID, $studentList, $startDate,$endDate, $sleep='', $report = 1, $fromThickBox = false,$startTime=$startTime, $endTime=$endTime);
//debug_r($bowelStudentData);
//exit();//

$dispGender = "";
switch($_POST["gender"])
{
	case 2:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['M'];
		break;
	case 3:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['F'];
		break;		
}
	
if ($_POST["sleep2"] == "0")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayOut'];
}
else if ($_POST["sleep2"] == "1")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayIn'];
}
else
{
	$dispSleepType = "";	
} 
$space = ($intranet_session_language == "en" ) ? "&nbsp;" : "";
$disStudentType = $dispGender . $space . $dispSleepType;


function timePeriodToTime($time,$isStartTime){
	global $objMedical;
	$timeFormat = $objMedical->getBowelTimeInterval();
	if($isStartTime){
		return date('H:i:s', mktime(0, 0, 0) + $time * $timeFormat * 60);
	}else{
		return date('H:i:s', mktime(0, 0, 0) + $time * $timeFormat * 60 + ($timeFormat * 60) - 1);
	}
	/* Display peroid 30 minutes
	$time = $time % 48;
	if($isStartTime){
		if($time%2 == 0){
			return sprintf("%02d:00:00",$time/2);
		}else{
			return sprintf("%02d:30:00",$time/2);
		}
	}else{
		if($time%2 == 0){
			return sprintf("%02d:29:59",$time/2);
		}else{
			return sprintf("%02d:59:59",$time/2);
		}
	}
	*/
}

switch($action){
	case 'print':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTPRINT"))){
	        header("Location: /");
	        exit();
	    }
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<link href="{$PATH_WRT_ROOT}home/library_sys/reports/css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="{$PATH_WRT_ROOT}home/library_sys/reports/css/visualize-light.css" type="text/css" rel="stylesheet" />
		<!--[if IE]><script type="text/javascript" src="{$PATH_WRT_ROOT}home/library_sys/reports/js/excanvas.js"></script><![endif]-->
<!--		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script> -->
		<script type="text/javascript" src="{$PATH_WRT_ROOT}home/library_sys/reports/js/visualize.jQuery.js"></script>	
		
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
			table { page-break-after:always !important}
			tr    { page-break-inside:avoid !important; page-break-after:auto !important}
			td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
		}
			.visualize-labels-x{
				text-align:center !important;
			}
			.label{
				font-size: 14px !important;
			}
			.visualize{
				height:250px !important;
			}
			.visualize-title{
				font-size: 14px !important;
				
			}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		</td></tr>
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		</div>
		<br />
		<br />
HTML;
		echo $printHTML;

		
		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentBowel']['report1']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.' )'.
		'</div><br /><br />';

		echo $resultHeader;

		echo $objMedical->getBowelReport1($bowelStudentData, $reasonID, $startTime, $endTime, $outputFormat = 'html',$print=true);

		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	
	case 'export':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTEXPORT"))){
	        header("Location: /");
	        exit();
	    }
		echo $objMedical->getBowelReport1($bowelStudentData, $reasonID, $startTime, $endTime, $outputFormat = 'csv');

	exit();
	break;	
	default:
		//////////////// INPUT CHECKING /////////
		$style = <<<HTML
		<style>
			.visualize-labels-x{
				text-align:center !important;
			}
			.label{
				font-size: 14px !important;
			}
			.visualize{
				height:250px !important;
			}
			.visualize-title{
				font-size: 14px !important;				
			}
		</style>
		<link href="{$PATH_WRT_ROOT}home/library_sys/reports/css/visualize.css" type="text/css" rel="stylesheet" />
		<link href="{$PATH_WRT_ROOT}home/library_sys/reports/css/visualize-light.css" type="text/css" rel="stylesheet" />
HTML;
		echo $style;

		$script= <<<HTML
		<!--[if IE]><script type="text/javascript" src="{$PATH_WRT_ROOT}home/library_sys/reports/js/excanvas.js"></script><![endif]-->
		<script type="text/javascript" src="{$PATH_WRT_ROOT}home/library_sys/reports/js/visualize.jQuery.js"></script>			
HTML;
		echo $script;
		
		
		echo '<div class="Conntent_tool">';
		if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTPRINT")))
		      echo '<a class="print tablelink" name="printBtn" id="printBtn">'.$Lang['Btn']['Print'].'</a>&nbsp;';
	    if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "BOWEL_REPORTEXPORT")))
    		  echo '<a class="export tablelink" name="exportBtn" id="exportBtn">'.$Lang['Btn']['Export'].'</a>';
    		echo '</div>';
    
    		echo "<br/><br/><br/>";

		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentBowel']['report1']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.' )'.
		'</div><br />';

		echo $resultHeader;
		
		echo $objMedical->getBowelReport1($bowelStudentData, $reasonID, $startTime, $endTime, $outputFormat = 'html');
		echo $linterface->Include_Thickbox_JS_CSS();		
		
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getBowelReport1&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getBowelReport1&action=print");
			$('#reportForm').submit();		
		});
		$('a.detailDescription').click(function(){
			$('#period').val( $(this).attr('data-period') );
			$('#reasonIDThickBox').val( $(this).attr('data-reasonID') );
			load_dyn_size_thickbox_ip("{$Lang['medical']['bowel']['tableHeader']['Detail']}", 'onloadThickBox();', inlineID='', defaultHeight=400, defaultWidth=750);
			return false;
		});

		function onloadThickBox() {
			$.ajax({
				url : "?t=reports.ajax.getThickBoxForBowelReport1",
				type : "POST",
				data : $('#reportForm').serialize(),
				success : function(msg) {
					$('div#TB_ajaxContent').html(msg);
				}
			});	
		}
		
		</script>
		<!--[if IE]><script type="text/javascript" src="{$PATH_WRT_ROOT}home/library_sys/reports/js/excanvas.js"></script><![endif]-->
		<script type="text/javascript" src="{$PATH_WRT_ROOT}home/library_sys/reports/js/visualize.jQuery.js"></script>			
HTML;
		echo $script;
	break;
}
?>