<?php 
// using: Adam
/*
 * 	Log
 * 	Date:	2014-01-03 
 */

include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/reports/reportFunction.php");

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $startDate)){
	echo 'Report Start Date Format Error! Please select with format YYYY-MM-DD';
	return;
}
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $endDate)){
	echo 'Report End Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

$reportName= 'sleepReport';

$dispGender = "";
switch($_POST["gender"])
{
	case 2:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['M'];
		break;
	case 3:
		$dispGender = $Lang['medical']['report_general']['search']['genderOption']['F'];
		break;		
}
	
if ($_POST["sleep2"] == "0")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayOut'];
}
else if ($_POST["sleep2"] == "1")
{
	$dispSleepType = $Lang['medical']['report_meal']['sleepOption']['StayIn'];
}
else
{
	$dispSleepType = "";	
}
 
$space = ($intranet_session_language == "en" ) ? "&nbsp;" : "";
$disStudentType = $dispGender . $space . $dispSleepType;


$sleepStudentData = $objMedical->getSleepReportData($reasonID='', $studentList, $startDate,$endDate, $getReasonName=true);
$sleepAttendanceList = $objMedical->getSleepCountDayByStudentID($studentList, $startDate,$endDate);

switch($action){
	case 'print':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTPRINT"))){
	        header("Location: /");
	        exit();
	    }
		include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
		
		$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
		$printHTML = <<<HTML
		<style>
		@media print
		{    
		    .print_hide, .print_hide *
		    {
		        display: none !important;
		    }
			table { page-break-after:always !important}
			tr    { page-break-inside:avoid !important; page-break-after:auto !important}
			td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
		}
		</style>
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		</td></tr>
		<tr><td>
		<div class="print_hide" align="right">
		<br />
		$printButton
		</div>
		<br />
		<br />
HTML;
		echo $printHTML;

		
		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentSleep']['report2']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.'&nbsp;)'.
		'</div><br /><br />';

		echo $resultHeader;

		echo $objMedical->getSleepReport2($sleepStudentData, $sleepAttendanceList, $outputFormat = 'print');	
		echo '<br />';
		echo $Lang['medical']['report_sleep']['reminder3'];
		$printHTML = <<<HTML
		</td></tr>
		</table>
		</body>
		</html>
HTML;
		echo $printHTML;
	break;
	
	case 'export':
	    if($sys_custom['medical']['accessRight']['PrintAndExport'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTEXPORT"))){
	        header("Location: /");
	        exit();
	    }
		echo $objMedical->getSleepReport2($sleepStudentData, $sleepAttendanceList, $outputFormat = 'csv');	
	exit();
	break;	
	default:
		//////////////// INPUT CHECKING /////////
		echo '<div class="Conntent_tool">';
		if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTPRINT")))
		  echo '<a class="print tablelink" name="printBtn" id="printBtn" style="float:none;display:inline;">'.$Lang['Btn']['Print'].'</a>&nbsp;';
	    if(!$sys_custom['medical']['accessRight']['PrintAndExport'] || ($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "SLEEP_REPORTEXPORT")))
		  echo '<a class="export tablelink" name="exportBtn" id="exportBtn" style="float:none;display:inline;">'.$Lang['Btn']['Export'].'</a>';
		echo '</div>';

		echo "<br/><br/><br/>";
		$resultHeader = 
		'<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
		$Lang['medical']['report_studentSleep']['report2']. ':&nbsp;' . 
		$disStudentType.
		'&nbsp;(&nbsp;'.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.'&nbsp;)'.
		'</div><br />';

		echo $resultHeader;

		echo $objMedical->getSleepReport2($sleepStudentData, $sleepAttendanceList, $outputFormat = 'html');	
		echo '<br />';
		echo $Lang['medical']['report_sleep']['reminder3'];
		
		echo $linterface->Include_Thickbox_JS_CSS();
		$script = <<<HTML
		<script>
		$('#exportBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getSleepReport2&action=export");
			$('#reportForm').submit();
		});
		$('#printBtn').click(function(){
			$('#reportForm').attr('action',"?t=reports.ajax.getSleepReport2&action=print");
			$('#reportForm').submit();		
		});
		
		$('a.dateDetail').click(function(){
				
				selectedStartDate = '{$startDate}';
				selectedEndDate = '{$endDate}';
				selectedTypeID = $(this).attr('data-typeID');
				selectedStudentId = $(this).attr('data-studentid');
				load_dyn_size_thickbox_ip("{$Lang['medical']['bowel']['tableHeader']['Detail']}", 'onloadThickBox();', inlineID='', defaultHeight=250, defaultWidth=800);
		});
		function onloadThickBox() {
//			alert(parDate + ' == '+ parReasonList + ' == '+parStudentId);
			$('div#TB_ajaxContent').load(
				"?t=reports.ajax.getThickBoxForSleepReport2",{ 
					'r_studentId': selectedStudentId,
					'r_startDate': selectedStartDate,
					'r_endDate': selectedEndDate,
					'r_typeID': selectedTypeID
				},
				function(ReturnData) {
					
				}
			);
		}
		</script>
HTML;
		echo $script;
	break;
}
?>