<?php 
// using: 
/*
 * 	Log
 *  Date:   20190610 Philips
 */

//////////////// INPUT CHECKING /////////
if(!$sys_custom['medical']['MedicalReport'] || !$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_REPORT') || !$plugin['medical_module']['revisit']){
    header("Location: /");
    exit();
}

// if(sizeof($studentList) > 0){
//     $recordArr = $objMedical->getRevisitReport($studentList);
// }
$reportName= 'revisitReport';
include_once($PATH_WRT_ROOT."/includes/libdb.php");
global $i_no_record_exists_msg;
$li = new libdb();


# Get Student Information
$student_name = getNameFieldByLang2('iu.');
$classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
$classNumber = "ycu.ClassNumber";
$AcademicYearID = Get_Current_Academic_Year_ID();
$cols = "iu.UserID AS UserID,
         $student_name AS StudentName,
         $classNameField As ClassTitle,
         $classNumber AS ClassNumber";
$table = "INTRANET_USER iu";
$joinTable = "INNER JOIN YEAR_CLASS_USER ycu
              ON iu.UserID = ycu.userID
              INNER JOIN YEAR_CLASS yc
              ON ycu.YearClassID = yc.YearClassID
                AND yc.AcademicYearID = '$AcademicYearID'";

$r_startDate = trim($_POST['r_startDate']);
$r_endDate = trim($_POST['r_endDate']);
$statusArr = $_POST['Status'];
$displayOrder = $_POST['displayOrder'];

$optionArr = array();
$optionArr['StartDate'] = $r_startDate;
$optionArr['EndDate'] = $r_endDate;
$optionArr['DisplayOrder'] = $displayOrder;
if(is_array($statusArr))
    $optionArr = array_merge($optionArr, $statusArr);
if($action == '') {
    $commonTable = 'common_table_list_v30';
    $tableTop = 'tabletop';
}
$tableHeaderHTML = <<<TABLE
<table class="reportTable $commonTable">
    <thead>
        <tr class="$tableTop">
            <th width="12%">{$Lang['medical']['revisit']['VisitDate']}</th>
            <th width="20%">{$Lang['medical']['revisit']['VisitHospital']}</th>
            <th width="20%">{$Lang['medical']['revisit']['Diagnosis']}</th>
            <th width="20%">{$Lang['medical']['revisit']['CuringStatus']}</th>
            <th width="16%">{$Lang['medical']['revisit']['IsChangedDrug']}</th>
            <th width="12%">{$Lang['medical']['revisit']['RevisitDate']}</th>
        </tr>
    </thead>
    <tbody>
TABLE;
    
$tableFooterHTML = <<<TABLE
    </tbody>
</table>
TABLE;
    echo $tableHTML;
    
switch($action){

    case 'print':
        
        include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
# Custom Css StyleSheet
$style = <<<style
<style>
@media print
{    
    .print_hide, .print_hide *
    {
        display: none !important;
    }
	table { page-break-after:always !important}
	tr    { page-break-inside:avoid !important; page-break-after:auto !important}
	td    { page-break-inside:avoid !important; page-break-after:auto !important}		    
}
table.reportTable {
    width: 100%;
    border-collapse: collapse;
}
table.reportTable th, table.reportTable tr, table.reportTable td{
    border: 1px black solid;
    padding: 3px;
    vertical-align: top;
}
table.drugTable {
    width: 100%;
}
table.drugTable th, table.drugTable tr, table.drugTable td{
    border: 0px;
    padding: 1px;
}
</style>
style;
echo $style;

$script = <<<script
<script type="text/javascript">
function printAll(){
    var tables = $('table.print_hide');
    tables.removeClass('print_hide');
    window.print();
    tables.addClass('print_hide');
}
function printReport(doc){
    var self = $(doc);
    var table = self.parent().parent().parent().parent().parent();
    table.removeClass('print_hide');
    window.print();
    table.addClass('print_hide');
}
</script>
script;
echo $script;

$printAllButton = $linterface->GET_BTN($Lang['Btn']['PrintAll'], "button", "printAll()","submit2");
$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "printReport(this)","submit2");

echo '<div class="print_hide" align="right"> <br /> ' . $printAllButton . '<br /><br /></div>';
foreach($studentList as $studentID){
    $printHTML = <<<HTML
    <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" class="print_hide">
    <tr><td>
    <div class="print_hide" align="right">
    <br />
    $printButton
    <br />
    <br />
    </div>
    
HTML;
    echo $printHTML;
    $sql = "SELECT $cols FROM $table $joinTable WHERE iu.UserID = '$studentID'";
    $studentInfo = $li->returnArray($sql);
    $studentInfo = $studentInfo[0];
    echo $studentInfo['StudentName'] . " (" . $studentInfo['ClassTitle'] . " - " . $studentInfo['ClassNumber'] . ")";
    echo '<br />';
    $reportList = $objMedical->getStudentRevisitReport($studentID, $optionArr);
    
    echo "<h3 style='text-align:center' width=100% align=center>{$Lang['medical']['menu']['report_revisit']}</h3>";
    
    $tableHTML = $tableHeaderHTML;
    foreach($reportList as $report){
        $tableHTML .= "<tr>";
        $reportID = $report['RevisitID'];
        $drugList = $objMedical->getRevisitDrug($reportID);
        $drugArr = array();
        if(sizeof($drugList) > 0){
            $drugTable = "<table class='drugTable'>";
            $drugTable .= "<col width='50%'>";
            $drugTable .= "<col width='25%'>";
            $drugTable .= "<col width='25%'>";
            foreach($drugList as $drug){
                $drugArr[] = $drug['Drug'] . ": " . $drug['Dosage'] . " ". $drug['Unit'];
                $drugTable .= '<tr>';
                    $drugTable .= '<td>' . $drug['Drug'] . '</td>';
                    $drugTable .= '<td>' . $drug['Dosage'] . '</td>';
                    $drugTable .= '<td>' . $drug['Unit'] . '</td>';
                $drugTable .= '</tr>';
            }
            $drugTable .= "</table>";
        } else {
            $drugTable = '-';
        }
        $tableHTML .= "<td>{$report['VisitDate']}</td>";
        $tableHTML .= "<td>{$report['Hospital']} {$report['Division']}</td>";
        $tableHTML .= "<td>" . ($report['Diagnosis'] != '' ? $report['Diagnosis'] : '-') . "</td>";
        $tableHTML .= "<td>" . ($report['CuringStatus'] != '' ? $report['CuringStatus'] : '-') . "</td>";
        $tableHTML .= "<td>";
            //$tableHTML .= implode('<br />', $drugArr);
            $tableHTML .= $drugTable;
        $tableHTML .= "</td>";
        $tableHTML .= "<td>{$report['RevisitDate']}</td>";
        
        $tableHTML .= "</tr>";
    }
    if(sizeof($reportList) == 0){
        $tableHTML .= "<tr><td colspan='6' align=center>$i_no_record_exists_msg</td></tr>";
    }
    $tableHTML .= $tableFooterHTML;
    echo $tableHTML;
    
    $printHTML = <<<HTML
</td></tr>
</table>
</body>
</html>
HTML;
    echo $printHTML;
}
    break;
    default:
        $style = "
                <style>
                table.drugTable {
                    width: 100%;
                }
                table.drugTable th, table.drugTable tr, table.drugTable td{
                    border: 0px;
                    padding: 1px;
                }
                </style>";
        echo $style;
        echo '<div class="Conntent_tool">';
            echo '<a class="print tablelink" name="printBtn" id="printBtn" onclick="printReport()">'.$Lang['Btn']['Print'].'</a>&nbsp;';
        echo '</div>';
        $resultHeader =
        '<br /><br /><br /><div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
        $Lang['medical']['report_meal']['search']['timePeriodOption'][$timePeriodHeader].
        $Lang['medical']['menu']['report_revisit'].': '.
        '(&nbsp;'.$r_startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$r_endDate.'&nbsp;)'.
        '</div><br /><br />';
        
        echo $resultHeader;
        foreach($studentList as $studentID){
            $sql = "SELECT $cols FROM $table $joinTable WHERE iu.UserID = '$studentID'";
            $studentInfo = $li->returnArray($sql);
            $studentInfo = $studentInfo[0];
            echo $studentInfo['StudentName'] . " (" . $studentInfo['ClassTitle'] . " - " . $studentInfo['ClassNumber'] . ")";
            $reportList = $objMedical->getStudentRevisitReport($studentID, $optionArr);
            $tableHTML = $tableHeaderHTML;
            foreach($reportList as $report){
                $tableHTML .= "<tr>";
                $reportID = $report['RevisitID'];
                $drugList = $objMedical->getRevisitDrug($reportID);
                $drugArr = array();
                if(sizeof($drugList) > 0){
                    $drugTable = "<table class='drugTable'>";
                    $drugTable .= "<col width='50%'>";
                    $drugTable .= "<col width='25%'>";
                    $drugTable .= "<col width='25%'>";
                    foreach($drugList as $drug){
                        $drugArr[] = $drug['Drug'] . ": " . $drug['Dosage'] . " ". $drug['Unit'];
                        $drugTable .= '<tr>';
                        $drugTable .= '<td>' . $drug['Drug'] . '</td>';
                        $drugTable .= '<td>' . $drug['Dosage'] . '</td>';
                        $drugTable .= '<td>' . $drug['Unit'] . '</td>';
                        $drugTable .= '</tr>';
                    }
                    $drugTable .= "</table>";
                } else {
                    $drugTable = '-';
                }
                
                $tableHTML .= "<td>{$report['VisitDate']}</td>";
                $tableHTML .= "<td>{$report['Hospital']} {$report['Division']}</td>";
                $tableHTML .= "<td>" . ($report['Diagnosis'] != '' ? $report['Diagnosis'] : '-') . "</td>";
                $tableHTML .= "<td>" . ($report['CuringStatus'] != '' ? $report['CuringStatus'] : '-') . "</td>";
                $tableHTML .= "<td>";
                //$tableHTML .= implode('<br />', $drugArr);
                $tableHTML .= $drugTable;
                $tableHTML .= "</td>";
                $tableHTML .= "<td>{$report['RevisitDate']}</td>";
                
                $tableHTML .= "</tr>";
            }
            if(sizeof($reportList) == 0){
                $tableHTML .= "<tr><td colspan='6' align=center>$i_no_record_exists_msg</td></tr>";
            }
            
            $tableHTML .= $tableFooterHTML;
            echo $tableHTML;
            echo '<br />';
        }
        break;
}
?>