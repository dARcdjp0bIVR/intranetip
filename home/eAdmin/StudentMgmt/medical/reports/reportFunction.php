<?php
//updated by : 
/* 	
 * 	Log
 * 
 *  Date:   2020-02-14 [Cameron]: fix: check if $PATH_WRT_ROOT exist not not
 *  
 *  Date:   2019-06-04 [Philips]: add access right checking of STUDENTLOG_VIEWALL
 * 
 *  Date:   2018-05-29 [Cameron]: fix: apply stripslashes and htmlspecialchars_decode to Lev1Name, Lev2Name in getConvulsionReport2()
 *   
 * 	Date:	2017-09-18 [Cameron]: add Last revisit date column in getConvulsionReport2()
 * 
 * 	Date:	2017-09-04 [Cameron]: add LastDrug column in getConvulsionReport2()
 * 
 * 	Date:	2015-01-27 [Cameron]: Add $plugin['medical_module']['AlwaysShowBodyPart'] to control whether to show body parts regardless 
 * 				Level2 Name
 * 	Date:	2014-01-21 [Cameorn]: Not need to join LEV2 when retrieve Lev3 items,
 * 									 Assume all items in LEV3 are $medical_cfg['studentLog']['level2']['specialStatus']	
 */

if (!isset($PATH_WRT_ROOT)) {
    header("Location: /");
    exit();
}

// withRemark == "" don't show remark
include_once($PATH_WRT_ROOT."includes/libuser.php");

function getConvulsionReport($startDate, $endDate, $studentList, $bodyParts, $syndrome, 
							$outputFormat = 'html', $withRemark="",$printingWithHeader = 0, $printEmptyData = false,$rowPerPage = 999){

		global $PATH_WRT_ROOT, $medical_cfg, $Lang, $linterface, $intranet_session_language,$plugin, $objMedical, $sys_custom;		
		global $medical_currentUserId;
		$css = '<style>
					.medica_align_center{
						text-align: center !important ;
						padding: 0px !important;
						height: 0.5cm !important;
					}
					.boldFont {
						font-weight: bold !important ;
					}
					@media print
					{
						.line-break {page-break-after:always}
					}
				</style>';

		$break_line = '<br class="line-break"/>'."\n"."\n";
		$div_break = "<div class='line-break'></div>";
		$objDB = new libdb();
		
		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
		$lexport = new libexporttext();

		$Rows = array(); // data rows
		$obj = array();

		$startDate = ($startDate) ? $startDate : date("Y-m-d");
		$endDate = ($endDate) ? $endDate : date("Y-m-d");
		$sqlWhere = " AND g.RecordTime BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ";
			
		$bodyPartCond = "";
		if (count($bodyParts) > 0)
			$bodyPartCond =  ' AND v3.Lev3ID In ('. implode(",", (array)$bodyParts ).')';
		$syndromeCond = "";
		if (count($syndrome) > 0)
			$syndromeCond =  ' AND v4.Lev4ID In ('. implode(",", (array)$syndrome ).')';

//		$conds = "";
//		if ($gender)
//		{
//			$conds = " AND u.Gender='" . $gender . "'";
//			$sqlWhere .= $conds;
//		}
//		$sleepCond = ($sleepOvernight == "") ? "" : " AND IUPS.stayOverNight ='$sleepOvernight'";
//		$sqlWhere .=  ' AND g.UserID In (\''. implode("','", (array)$studentList ).'\')';

//		$sql = "SELECT v3.Lev3ID, v3.Lev3Name, v4.Lev4ID, v4.Lev4Name 
//				FROM MEDICAL_STUDENT_LOG_LEV3 v3 
//					INNER JOIN MEDICAL_STUDENT_LOG_LEV4 v4 ON v4.Lev3ID = v3.Lev3ID
//					INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID = v3.Lev2ID
//					WHERE v2.Lev2Name='". $medical_cfg['studentLog']['level2']['specialStatus']."'"
//					. $bodyPartCond . $syndromeCond . " ORDER BY v3.Lev3ID, v4.Lev4ID";

		##### 2014-01-21: Not need to join LEV2, Assume all items in LEV3 are $medical_cfg['studentLog']['level2']['specialStatus']
		$sql = "SELECT v3.Lev3ID, v3.Lev3Name, v4.Lev4ID, v4.Lev4Name 
				FROM MEDICAL_STUDENT_LOG_LEV3 v3 
					INNER JOIN MEDICAL_STUDENT_LOG_LEV4 v4 ON v4.Lev3ID = v3.Lev3ID
					WHERE 1=1 "
					. $bodyPartCond . $syndromeCond . " ORDER BY v3.Lev3ID, v4.Lev4ID";

		$header_rs = $objDB->returnResultSet($sql);
		$nrHeader = count($header_rs);

//		$sql = 'select '.getNameFieldByLang2('u.').' as UserName, u.UserID from INTRANET_USER u Inner Join INTRANET_USER_PERSONAL_SETTINGS IUPS On IUPS.UserID = u.UserID where u.UserID In (\''. implode("','", (array)$studentList ).'\') '.$sleepCond . $conds;
		$sql = 'select '.getNameFieldByLang2('u.').' as UserName, u.UserID from INTRANET_USER u where u.UserID In (\''. implode("','", (array)$studentList ).'\') ';
		$student_rs = $objDB->returnResultSet($sql);
//print $sql ."<br>";
//debug_r($student_rs);
//error_log('$student_rs -->'.print_r($student_rs,true)."<----\n\t".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");

		$nrStudent = count($student_rs);
		if ($nrStudent > 0)
		{
			for($i = 0; $i < $nrStudent; $i++)
			{
				$studentName 	= stripslashes($student_rs[$i]["UserName"]);
				$studentID 		= $student_rs[$i]["UserID"];
				$obj[$studentID]["StudentName"] = $studentName;
				
				for($k = 0; $k < $nrHeader; $k++)
				{
					$lev3ID = $header_rs[$k]["Lev3ID"];
					$lev4ID = $header_rs[$k]["Lev4ID"];
					$lev4_total[$studentID][$lev3ID][$lev4ID] = 0;	// Initialized number of times per syndrome for a student
					$lev3_total[$studentID][$lev3ID] = 0;			// Initialized number of times per body part for a student
				}

				$sql = "SELECT 	g.RecordID,date_format(g.RecordTime,'%Y-%m-%d') as RecordDate,
								date_format(g.RecordTime,'%H:%i') as RecordTime,g.Duration, g.Remarks
						FROM  MEDICAL_STUDENT_LOG g
						INNER JOIN INTRANET_USER u ON u.UserID=g.UserID
						INNER JOIN MEDICAL_STUDENT_LOG_LEV2 v2 ON v2.Lev2ID = g.BehaviourTwo";
				
				$sqlCond = " WHERE 1 ";		
				if ($plugin['medical_module']['AlwaysShowBodyPart']) {
					$sql .= " INNER JOIN (SELECT v3.Lev3ID, v4.Lev4ID, p.StudentLogID 
							FROM MEDICAL_STUDENT_LOG_PARTS p 
							INNER JOIN MEDICAL_STUDENT_LOG_LEV3 v3 ON v3.Lev3ID = p.Level3ID 
							INNER JOIN MEDICAL_STUDENT_LOG_LEV4 v4 ON v4.Lev3ID = v3.Lev3ID									
							WHERE 1=1 "
							. $bodyPartCond . $syndromeCond . ") as s
 							ON s.StudentLogID=g.RecordID";
				}	
				else {
					$sqlCond .= " AND v2.Lev2Name='". $medical_cfg['studentLog']['level2']['specialStatus']."'";					
				}					
				if($sys_custom['medical']['accessRight']['ViewAll'] && !($objMedical->isSuperAdmin($_SESSION['UserID'])|| $objMedical->checkAccessRight($_SESSION['UserID'], "STUDENTLOG_VIEWALL"))){
				    $sqlCond .= " AND g.InputBy = '{$_SESSION['UserID']}'";
				}
 				$sqlCond .= " AND u.UserID=" .$studentID . $sqlWhere;
				$sql .= $sqlCond . " GROUP BY g.RecordID ORDER BY g.RecordTime";

				$rs = $objDB->returnResultSet($sql);
				
				$nrRow = count($rs);
				
				if ($nrRow > 0)
				{					
					for($j = 0; $j < $nrRow; $j++)
					{
						$recordID 	= $rs[$j]['RecordID'];
						$recordDate = $rs[$j]['RecordDate'];
						$recordTime = $rs[$j]['RecordTime'];
						$duration 	= $rs[$j]['Duration'];
						$remarks 	= stripslashes($rs[$j]['Remarks']);
						
						$obj[$studentID]["Log"][$j]["RecordID"] 	= $recordID; 
						$obj[$studentID]["Log"][$j]["RecordDate"] 	= $recordDate;
						$obj[$studentID]["Log"][$j]["RecordTime"] 	= $recordTime;
						$obj[$studentID]["Log"][$j]["Duration"] 	= $duration;
						$obj[$studentID]["Log"][$j]["Remarks"] 		= $remarks;
						
						for($k = 0; $k < $nrHeader; $k++)
						{
							$lev4ID = $header_rs[$k]["Lev4ID"];
							$obj[$studentID]["Log"][$j]["Syndrome"][$lev4ID] = 0;	// syndrome not exist by default
						}						
						

						$sql = "SELECT 	p.StudentLogID, p.Level3ID, p.Level4ID
								FROM  MEDICAL_STUDENT_LOG_PARTS p WHERE StudentLogID=".$recordID .
								str_replace("v3.Lev3ID","p.Level3ID",$bodyPartCond) . 
								str_replace("v4.Lev4ID","p.Level4ID",$syndromeCond) .
								" ORDER BY p.Level3ID, p.Level4ID " ;

						$rs2 = $objDB->returnResultSet($sql);
						
						$nrSyndrome = count($rs2);
						for($k = 0; $k < $nrSyndrome; $k++)
						{
							$lev3ID = $rs2[$k]["Level3ID"];
							$lev4ID = $rs2[$k]["Level4ID"];
							$obj[$studentID]["Log"][$j]["Syndrome"][$lev4ID] = 1;	// syndrome exist, overwirte default value
							$lev4_total[$studentID][$lev3ID][$lev4ID] += 1;
							$lev3_total[$studentID][$lev3ID] += 1;
						}						
					}
				}
				else	// No record for this student
				{
					$obj[$studentID]["Log"] = array();
				}
			}	// End loop for each student
		}		// NrStudent > 0
		else	// No record
		{
			// do nothing
		}

	//////////////////////////
	//START HANDLE OUTPUT THE RESULT
	//////////////////////////

	$HeaderRow1 = array();// columns in header row1	
	// HEADER
	$hHeader = '<tr class="tabletop"><th>'.$Lang['medical']['report_studentlog']['tableHeader']['date'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_studentlog']['tableHeader']['time'].'</th>';
	$hHeader .= '<th nowrap>'.$Lang['medical']['report_studentlog']['tableHeader']['duration'].'</th>';
	
	$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['date'];
	$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['time'];
	$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['duration'];

	$lev3Header = array();	
	for($i = 0; $i < $nrHeader; $i++)
	{	
		$lev3ID = $header_rs[$i]["Lev3ID"];
		$lev3Header[$lev3ID]["Name"] = stripslashes($header_rs[$i]["Lev3Name"]);
		$lev3Header[$lev3ID]["Number"] += 1;
	}

	foreach($lev3Header as $v)
	{
		$hHeader .= '<th class="medica_align_center" colspan="'.$v["Number"].'">'.$v["Name"].'</th>';
		$HeaderRow1[] = $v["Name"];
		for ($k=0; $k < $v["Number"]-1;$k++)
		{
			$HeaderRow1[] = "";	// pad number of columns
		}
	}
		
	if ($withRemark)
	{		
		$hHeader .= '<th class="medica_align_center">'.$Lang['medical']['report_studentlog']['tableHeader']['remarks'].'</th>';
		$HeaderRow1[] = $Lang['medical']['report_studentlog']['tableHeader']['remarks'];
	}
	$hHeader .= '</tr>';

	$HeaderRow2 = array();// columns in header row2
	$hHeader .= '<tr class="tabletop">';
	for($i = 0; $i < 3; $i++)
	{	
		$hHeader .= '<th>&nbsp;</th>';
		$HeaderRow2[] = "";
	}
	for($i = 0; $i < $nrHeader; $i++)
	{			
		$lev4Name = stripslashes($header_rs[$i]["Lev4Name"]);

		if ($intranet_session_language == "b5") // Chinese, show in vertical, don't work!
		{
			$chrArray = preg_split('//u',$lev4Name, -1, PREG_SPLIT_NO_EMPTY);
			$dispLev4Name = implode("<br>",$chrArray); 
//			$dispLev4Name = "<div style='width:1.5em; float:right;'>{$lev4Name}</div>";
		}
		else
		{
			$dispLev4Name = $lev4Name;
		}
		$hHeader .= '<th class="medica_align_center" valign="top">'.$dispLev4Name.'</th>';		
		$HeaderRow2[] = $lev4Name;				
	}
	if ($withRemark)
	{		
		$hHeader .= '<th class="medica_align_center">&nbsp;</th>';
		$HeaderRow2[] = "";
	}
	$hHeader .= '</tr>';

//debug_r($intranet_session_language);	
//print "<table>".$hHeader."</table>";	



	/////////////////////////////////////////////
	// Output contents
	
	$h = "";
	$p_fontsizeA = 15; //Font size A , may be have font size B
	$p_fontsizeB = 17; 
	$objUser = null;
	if($printingWithHeader){
		$objUser = new libuser($medical_currentUserId);
	}
	
	
	$recordStudentsCount = 0;
	foreach($studentList as $studentID){
		$student = $obj[$studentID];
		if(count($student["Log"])){
			++$recordStudentsCount;
		}
	}

	for ($j = 0, $iMax = count($studentList); $j < $iMax; $j++) {
	
		$studentID = $studentList[$j];
		$student = $obj[$studentID];
		
		$prevDate = "";		// for comparison to print distinct date only
		 		
		$Rows[] = array($Lang['medical']['studentLog']['report_convulsion']['reportName'].': '.
					$student["StudentName"].' ( '.$startDate.' '.$Lang['StaffAttendance']['To'].' '.$endDate.' )');

		if($printingWithHeader){


			$printReportHeader = '<table width="100%" border="0">';
			$printReportHeader .= '<tr>';
			$printReportHeader .= '<td width="30%" align="left">';
			$printReportHeader .= '<span style="font-size:'.$p_fontsizeA.'px"><b>靈實恩光學校</b><br/>';
			$printReportHeader .= '<span style="font-size:'.$p_fontsizeA.'px">學生姓名:<u>'.$student['StudentName'].'</u><br/></span>';
			$printReportHeader .= '<span style="font-size:'.$p_fontsizeA.'px">日期:<u>'.$startDate.' 至 '.$endDate.'</u></span><br/>';
					
			$printReportHeader .= '</td>';
			$printReportHeader .= '<td width="%" align="center"><span style="font-size:'.$p_fontsizeB.'px"><u><b>學生抽搐紀錄表</b></u></span></td>';
			$printReportHeader .= '<td width="30%" align="right">';
			$printReportHeader .= '<span style="font-size:'.$p_fontsizeA.'px">紀錄表列印:<u>'.$objUser->UserName().'</u></span><br/>';
			$printReportHeader .= '<span style="font-size:'.$p_fontsizeA.'px">列印日期:<u>'.date('d/m/Y').'</u></span><br/>';
			$printReportHeader .= '</td>';
			$printReportHeader .= '</tr>';
			$printReportHeader .= '</table>';

			$header = $printReportHeader;
		}else{
			$printReportHeader = '<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">'.
					$Lang['medical']['studentLog']['report_convulsion']['reportName'].': '.
					$student["StudentName"].
					' (&nbsp;'.$startDate.'&nbsp;'.$Lang['StaffAttendance']['To'].'&nbsp;'.$endDate.'&nbsp;)'.
				  '</div><br />';
		}
		$header = $printReportHeader;			
		$nrLog = count($student["Log"]);	// Number of log record for the student
		
		
		if ($nrLog > 0 || $printEmptyData){
			if($printingWithHeader){
				$printReportFooter = '<table width="100%" border="0">';
				$printReportFooter .= '<tr><td align="left">';
	
				$printReportFooter .= '<span style="font-size:12px">香港新界將軍澳安達臣道三０一號<br/></span>';
				$printReportFooter .= '<span style="font-size:12px">電話: (852) 2703 1722　　傳真: (852) 2703 6320<br/></span>';
				$printReportFooter .= '<span style="font-size:12px">電郵: info@sunnyside.edu.hk　網址: www.sunnyside.edu.hk<br/></span>';
	
				$printReportFooter .= '</td>';
				$printReportFooter .= '</table><br/><br/>';
//				$printReportFooter .= $break_line;
//				$h .= $printReportFooter;
			}
		}
		if ($nrLog > 0)
		{
			++$nonEmptyStudent;
			$h .= $header;//
			$Rows[] =  $HeaderRow1;
			$Rows[] =  $HeaderRow2;
			

			$exportColumn[0] = "";
			$h .= '<table border = "0" class="common_table_list_v30">';
			$h .= $hHeader;			 
			for ($i = 0; $i < $nrLog; $i++)
			{
				$Details = array();				

				$curDate = $student["Log"][$i]["RecordDate"];

				$h .= '<tr>';
				$h .= '<td>'.(($curDate != $prevDate)? $curDate : "&nbsp;").'</td>';
				$Details[] = ($curDate != $prevDate)? $curDate : "";
				$prevDate = $curDate;
				$h .= '<td>'.$student["Log"][$i]["RecordTime"].'</td>';
				$Details[] = $student["Log"][$i]["RecordTime"];
				$duration = $student["Log"][$i]["Duration"];
				$dispDuration = "";

				if (substr($duration,0,2) != "00") 
					$dispDuration .= intval(substr($duration,0,2)) . $Lang['medical']['studentLog']['hour'];
				if (substr($duration,3,2) != "00") 
					$dispDuration .= intval(substr($duration,3,2)) . $Lang['medical']['studentLog']['minute'];
				if (substr($duration,6,2) != "00") 
					$dispDuration .= intval(substr($duration,6,2)) . $Lang['medical']['studentLog']['second'];
				$h .= '<td>'.$dispDuration.'&nbsp;</td>';
				$Details[] = $dispDuration;
				
				// Syndrome
				$nrSyndrome = count($student["Log"][$i]["Syndrome"]);
				foreach($student["Log"][$i]["Syndrome"] as $syndrome)
				{
					$h .= '<td class="medica_align_center">'.(($syndrome == 1)? $linterface->Get_Tick_Image() : "&nbsp;").'</td>';
					$Details[] = $syndrome;
				}
				
				// remarks	
				if ($withRemark)
				{		
					$h .= '<td>'.nl2br($student["Log"][$i]["Remarks"]).'&nbsp;</td>';
					$Details[] = $student["Log"][$i]["Remarks"];
				}	
				$h .= '</tr>';	
				$Rows[] = $Details;						
				
				if( ($i+1)%$rowPerPage==0){
					$h .= "</table>";					
					$h .= $printReportFooter;//
					
					if( $i+1<=$nrLog){
						$h .= "<br/>";//
						$h .= $div_break;
						$h .= $header;//
						$h .= '<table border = "0" class="common_table_list_v30">';
						$h .= $hHeader;			 
					}
				}
				
			}
			
			// Sub-Total
			$SubTotalRow = array();
			$h .= '<tr class="total_row">';
			$h .= '<td class="boldFont">' . $Lang['medical']['report_studentlog']['subTotal'] . '</td>';
			$SubTotalRow[] = $Lang['medical']['report_studentlog']['subTotal'];
			
			for($i = 0; $i < 2; $i++)
			{	
				$h .= '<td>&nbsp;</td>';
				$SubTotalRow[] = "";
			}
			
			for($i = 0; $i < $nrHeader; $i++)
			{
				$lev3ID = $header_rs[$i]["Lev3ID"];
				$lev4ID = $header_rs[$i]["Lev4ID"];
				$h .= '<td class="medica_align_center boldFont">'. (($lev4_total[$studentID][$lev3ID][$lev4ID] > 0 ) ? $lev4_total[$studentID][$lev3ID][$lev4ID] : '-') . '</td>';
				$SubTotalRow[] = ($lev4_total[$studentID][$lev3ID][$lev4ID] > 0 ) ? $lev4_total[$studentID][$lev3ID][$lev4ID] : '-' ;				
			}
			if ($withRemark)
			{		
				$h .= '<td>&nbsp;</td>';
				$SubTotalRow[] = "";
			}	
			$h .= '</tr>';			
			$Rows[] = $SubTotalRow;

			// Grand Total
			$GrandTotalRow = array();	
			$h .= '<tr class="tabletop">';
			$h .= '<th class="boldFont">'.$Lang['medical']['report_studentlog']['grandTotal'] . '</th>';
			$GrandTotalRow[] = $Lang['medical']['report_studentlog']['grandTotal'];
			$h .= '<th class="boldFont">' . count($obj[$studentID]["Log"]) . '</th>';
			$GrandTotalRow[] = count($obj[$studentID]["Log"]);
			$h .= '<th>&nbsp;</th>';
			$GrandTotalRow[] = "";
			
			foreach($lev3Header as $lev3ID => $v){
				$h .= '<th class="medica_align_center boldFont" colspan="'.$v["Number"].'">'. (($lev3_total[$studentID][$lev3ID] > 0 ) ? $lev3_total[$studentID][$lev3ID] : '-') . '</th>';
				$GrandTotalRow[] = ($lev3_total[$studentID][$lev3ID] > 0 ) ? $lev3_total[$studentID][$lev3ID] : '-' ;
				for ($k=0; $k < $v["Number"]-1;$k++)
				{
					$GrandTotalRow[] = "";	// pad number of columns
				}
			}
			if ($withRemark){	
				$h .= '<th>&nbsp;</th>';
				$GrandTotalRow[] = "";
			}
			$h .= '</tr>';
			$Rows[] = $GrandTotalRow;			
			$h .= '</table>';
			if(!$printingWithHeader){
				$h .= '<br /><br /><br />';
			}
			$Rows[] = array("");	// Line break;
			
		
			if ( ($nrLog > 0 || $printEmptyData) && $nrLog%$rowPerPage!=0){
				if($printingWithHeader){
					$h .= $printReportFooter;
				}
			}
//			debug_r($recordStudentsCount);
//			debug_r($j);
//			debug_r('testing');
			if($printEmptyData){
				if($j+1<$iMax){
					$h .= $div_break;
				}
			}
			else if($nonEmptyStudent+1<=$recordStudentsCount){
				$h .= $div_break;
			}
		}
		else	// No record
		{
			if($printEmptyData){
				$h .= $header;
				$h .= '<h4 class="medica_align_center">' . $Lang['SysMgr']['Homework']['NoRecord'] . "</h4>";
				$h .= $printReportFooter;
				$h .= '<br>';
				$Rows[] = array($Lang['SysMgr']['Homework']['NoRecord']); 		
				$Rows[] = array("");	// Line break;	
				
				if($j+1<$iMax){
					$h .= $div_break;
				}
			}else{
//				$h = '';
				array_pop($Rows); // Remove the student info
			}
		}

		


		
	}	// for each student


	switch ($outputFormat)
	{
		case "html":
			if(!empty($h)){
				return $css.$h;
			}else{
				return "<div style='text-align: center'>{$Lang['SysMgr']['Homework']['NoRecord']}</div>";
			}
		break;

		default:
		case "csv":
			if(!empty($Rows)){
				$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");						
			}else{
				$exportContent = $Lang['SysMgr']['Homework']['NoRecord'];
			}
			$lexport->EXPORT_FILE('export.csv', $exportContent);
			return true;
		break;
	}
} // end of function





function getConvulsionReport2($data, $outputFormat = 'html',$LastDrug=''){
	$css = '<style>
				.medica_align_center{
					text-align: center !important ;
				}
			</style>';

//debug_r ($_POST);

	global $PATH_WRT_ROOT, $medical_cfg, $Lang, $linterface, $intranet_session_language, $image_path, $plugin, $objMedical;		
	$objDB = new libdb();
	
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();

	$Rows = array(); // data rows
	$obj = array();

	//////////////////////////
	//START HANDLE OUTPUT THE RESULT
	//////////////////////////

	$HeaderRow = array();// columns in header row1	
	// HEADER
	$hHeader = '<tr class="tabletop">';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['studentName'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['time'].'</th>';
//	$hHeader .= '<th>'.$Lang['medical']['report_studentlog2']['tableHeader']['duration'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_studentlog2']['tableHeader']['type'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['item'].'</th>';
//	$hHeader .= '<th>'.$Lang['medical']['report_studentlog2']['tableHeader']['bodyParts'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['remarks'].'</th>';
	
	if($LastDrug && $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='REVISIT_MANAGEMENT') && $plugin['medical_module']['revisit']){
		$hHeader .= '<th>'.$Lang['medical']['revisit']['LastChangedDrug'].'</th>';
	}
		
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['lastUpdatedBy'].'</th>';
	$hHeader .= '<th>'.$Lang['medical']['report_general']['tableHeader']['lastUpdatedOn'].'</th>';
	$hHeader .= '</tr>';	
	
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['studentName'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['time'];
//	$HeaderRow[] = $Lang['medical']['report_studentlog2']['tableHeader']['duration'];
	$HeaderRow[] = $Lang['medical']['report_studentlog2']['tableHeader']['type'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['item'];
//	$HeaderRow[] = $Lang['medical']['report_studentlog2']['tableHeader']['bodyParts'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['remarks'];
	
	if($LastDrug && $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='REVISIT_MANAGEMENT') && $plugin['medical_module']['revisit']){
		$HeaderRow[] = $Lang['medical']['revisit']['LastChangedDrug'];
	}
	
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['lastUpdatedBy'];
	$HeaderRow[] = $Lang['medical']['report_general']['tableHeader']['lastUpdatedOn'];

//print "<table>".$hHeader."</table>";	


	/////////////////////////////////////////////
	// Output contents
	
	$h = "";
	$exportColumn[0] = "";
	if (count($data) > 0)
	{
		$h .= '<table border = "0" class="common_table_list_v30">';
		$h .= $hHeader;			 
		$exportColumn[0] = $HeaderRow;
	
		foreach ($data as $studentID => $log)	// Loop for each students
		{
//debug_r($studentID);
//debug_r($log);
			$studentName = stripslashes($log["Name"]);
									
			$nrLog = count($log["recordDetail"]);	// Number of log record for the student
//debug_r($nrLog);
			$h .= '<tr>';
			$h .= '<td rowspan="'.$nrLog.'" style="width:80px">'.$studentName.'</td>';
			$i = 0;
			foreach ($log["recordDetail"] as $k => $v)
			{
//debug_r($v);				
				$Details = array();
				$h .= ($i>0) ? '<tr>' : '';
				$Details[] = ($i>0) ? '' : $studentName;

				$recordTime = substr($v["RecordTime"],11,5);	// Format: Hour:Minute	
				$h .= '<td style="width:50px">'.$recordTime.'</td>';
				$Details[] = $recordTime;

				$h .= '<td style="width:15%">'.($v["Lev1Name"] ? stripslashes($v["Lev1Name"]) : '-').'</td>';
				$Details[] = $v["Lev1Name"] ? htmlspecialchars_decode(stripslashes($v["Lev1Name"])) : '-';
				
				$attachment = '';
				if( $v['StudentLogID'] && $outputFormat == 'html'){
					$attachment = "<a id='fixedSizeThickboxLink' class='print_hide tablelink fixedSizeThickboxLink' href='javascript:void(0);' data-Id='{$v['StudentLogID']}' title='".$Lang['SysMgr']['Homework']['Attachment']."' ><img src='{$image_path}/icon/attachment_blue.gif' border=0 /></a> ";
				}
				$h .= '<td style="width:15%">'.($v["Lev2Name"] ? stripslashes($v["Lev2Name"]) : '-'). $attachment .'</td>';
				$Details[] = $v["Lev2Name"] ? htmlspecialchars_decode(stripslashes($v["Lev2Name"])) : '-';

				$h .= '<td>'.($v["Remarks"] ? nl2br($v["Remarks"]) : '-').'</td>';
				$Details[] = $v["Remarks"] ? htmlspecialchars_decode($v["Remarks"]) : '-';

				if($LastDrug && $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='REVISIT_MANAGEMENT') && $plugin['medical_module']['revisit']){
					if ($i == 0) {
						$h .= '<td rowspan="'.$nrLog.'">'.($Lang['medical']['revisit']['RevisitDate2'].': '.$log['LastRevisitDate'].'<br>'.($log["LastDrug"] ? nl2br($log["LastDrug"]) : '-')).'</td>';
						$Details[] = $Lang['medical']['revisit']['RevisitDate2'].': '. $log['LastRevisitDate'].' '.($log["LastDrug"] ? $log["LastDrug"] : '-');
					}
				}

				$h .= '<td style="width:70px">'.$v["ConfirmedName"].'</td>';
				$Details[] = $v["ConfirmedName"];

				$h .= '<td style="width:150px">'.$v["DateModified"].'</td>';
				$Details[] = $v["DateModified"];
				
				$h .= '</tr>';	
				$Rows[] = $Details;
				
				$i++;						
			}	// loop for log of a student
		}	// loop for number of students
		
		$h .= '</table>';
		$h .= '<br/>';
	}		// record exists
	else	// No record
	{
		$h .= '<h4 class="medica_align_center">' . $Lang['SysMgr']['Homework']['NoRecord'] . "</h4>";
		$h .= '<br/>';
		$Rows[] = array($Lang['SysMgr']['Homework']['NoRecord']); 			
	}


	switch ($outputFormat)
	{
		case "html":
			return $css.$h;
		break;

		default:
		case "csv":
			$exportContent = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($Rows, $exportColumn, "\t", "\r\n", "\t", 0, "11");						
			return $exportContent;
			//$lexport->EXPORT_FILE('export.csv', $exportContent);
			//return true;
		break;
	}
} // end of function

