<?php 
// Using: 

/**
 * Created by Pun
 * Usage: Send data to ?t=testPostData
 * 			If the data Contains ['_returnData'], it will echo it. Otherwise echo $_POST and $_GET
 */ 
/*
	$.post('?t=testPostData',{Data: 'data'}, function(responseResult){
		alert(responseResult);
	});
*/


	if(isset($_POST['_returnData'])){
		echo $_POST['_returnData'];
	}else if(isset($_GET['_returnData'])){
		echo $_GET['_returnData'];
	}else{
		echo '<h1>$_POST</h1>';
		debug_r($_POST);
		echo '<h1>$_GET</h1>';
		debug_r($_GET);
	}
?>