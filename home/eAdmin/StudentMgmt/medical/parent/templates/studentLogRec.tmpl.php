<?php 
// using: Pun

#### Get YearMonth START ####
$selectedStudent = trim($_REQUEST["Student"]);	// Student UserID
if($selectedStudent == ''){
	$studentList = $objMedical->getStudentInfoByParent($_SESSION['UserID']);
	$selectedStudent = $studentList[0]['UserID'];
}
$yearMonthList = $objMedical->getYearMonthFromStudentLogTableByStudentID($selectedStudent);
#### Get YearMonth END ####

#### Get Year-Month Selection HTML START ####
if( in_array($_REQUEST["SelYear"], array_keys($yearMonthList)) ){
	$defaultYear = $_REQUEST["SelYear"];
	if( in_array($_REQUEST["SelMonth"], $yearMonthList[$defaultYear]) ){
		$defaultMonth = $_REQUEST["SelMonth"];
	}
}else{
	$defaultYear = date("Y");
	$defaultMonth = date("m");
}

$yearHTML = '<select class="" id="FilterYear" name="FilterYear" onchange="">';
foreach (array_keys($yearMonthList) as $year) {
	if($year == $defaultYear){
		$selected = 'selected="selected"';
	}else{
		$selected = '';
	}
	$yearHTML .= "<option value=\"{$year}\" {$selected}>{$year}</option>";
}
$yearHTML .= '</select>';

$monthHTML = '<select class="" id="FilterMonth" name="FilterMonth" onchange="showResultList();">';
foreach ((array)$yearMonthList[$defaultYear] as $month) {
	if($month == $defaultMonth){
		$selected = 'selected="selected"';
	}else{
		$selected = '';
	}
	$monthHTML .= "<option value=\"{$month}\" {$selected}>{$month}</option>";
}
$monthHTML .= '</select>';
#### Get Year-Month Selection HTML END ####

?>
<style type="text/css">
a{
	color:#2286C5;
	cursor:pointer;
}
</style>

<form id="studentLogForm" action="index.php" name="form" method="post">
<div class="table_board">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tbody>
		<tr>
			<td colspan="3">
				<span id="studentArea">
<?
	include_once('parent/ajax/getChildrenList.php');
?>		
				</span>
			</td>
		</tr>
				
		<tr>
			<td class="dotline" colspan="3">
				<img src="<?php echo $image_path."/".$LAYOUT_SKIN.'/10x10.gif'; ?>" width="10" height="1" />
			</td>	
		</tr>
	</tbody>
	<tfoot>
		<!--tr>
			<td align="left">				
				<span class="table_row_tool addEvent">
					<a class="newBtn add" id="addEventImage" title="<?php echo $Lang['medical']['general']['new']; ?>"></a></span>
				<a class="newBtn" id="addRecord" ><?php echo $Lang['medical']['general']['new']; ?></a>
			</td>	
			<td colspan="2"></td>
		</tr-->

		<tr>
			<td align="left">				
<?php
	if(count($yearMonthList) > 0){
		echo $Lang['medical']['bowel']['parent']['month'] . "&nbsp;";
		echo $yearHTML . '-' . $monthHTML;
	}
?>														
			</td>	
			<td colspan="2"></td>
		</tr>
		
	</tfoot>
</table>
</div>
<div id="viewResult">
<?php
	if(count($yearMonthList) == 0)
	{
		echo '<div class=tableContent align=center colspan="6"><br>'.$Lang['SysMgr']['Homework']['NoRecord'].'<br><br></div>';	
	}
?>
</div>

<input type="hidden" name="t" id="t" value="" />
<input type="hidden" name="r_date" id="r_date" value="" />
<input type="hidden" name="action" id="action" value="" />
</form>
	
<script>

	//// Setup year-month list START ////
	var yearMonthList = {}; // yearMonthList[Year] = array(Month,Month...)
	<?php foreach ($yearMonthList as $year=>$month) { ?>
	yearMonthList[<?=$year?>] = [<?=implode(',', $month)?>];
	<?php } ?>
	//// Setup year-month list END ////

	$('#FilterYear').change(function(){
		$('#FilterMonth').empty();
		$.each(yearMonthList[ $('#FilterYear').val() ], function(index, value){
			var month;
			if(value < 10){
				month = '0' + value;
			}else{
				month = value;
			}
			$('#FilterMonth').append('<option value="'+month+'">'+month+'</option>');
		});
		showResultList();
	});
	
	function showResultList() {
		$('#t').val('parent.ajax.getStudentLogList');
		$.ajax({
			url : "index.php",			
			type : "POST",
			data : $('#studentLogForm').serialize(),
			success : function(msg) {
				$('#viewResult').html(msg);
			}
		});	
	}
	
	function getStudentInfo() {
		window.location.href = '?t=parent.studentLogRec&Student='+$('#Student').val();
	}

$(document).ready(function(){

	$('.newBtn').click(function(){
		$('#t').val('parent.studentLogAddItem');
		$('#action').val('new');
		$('#studentLogForm').submit();				
	});

	$('.editBtn').live('click',function(){
		$('#t').val('parent.studentLogAddItem');
		var date = $(this).attr('data-date');		
		$('#r_date').val(date);		
		$('#action').val('edit');
		$('#studentLogForm').submit();				
	});

	showResultList();
});
</script>