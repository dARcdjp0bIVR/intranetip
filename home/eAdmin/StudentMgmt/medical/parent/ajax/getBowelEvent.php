<?php
/*
 * 	Using:
 * 	Log:
 * 	Date:	2014-02-27 [Cameron] Need to check $sid to avoid getting wrong data if the student is not the corresponding child of login parent
 */

	$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$sid);
	if (!$validChild)		// Not child of the parent, redirect to home directory
	{
		exit();
	}

	// An already existed eventID List in hidden form
	// This function needs to wait for the db
	$eventIDArray = array();

	# Object Data
	$objBowelLog = new BowelLog($date);
	$bowelLogObjects = $objBowelLog->getBowelLogByStudentID($sid,$date);

	echo '<span id="dummySpan"></span>';
	if ($bowelLogObjects != false)
	{
		foreach( (array)$bowelLogObjects as $bowelLogObj){

			$objMedical->getBowelLogInputUIByParent($recordID=$bowelLogObj->getRecordID(), $eventDetail = $bowelLogObj);
	
			$eventIDArray[] = $bowelLogObj->getRecordID();
		}
	}
	echo '<span><input type="hidden" id="date" name="date" value="'.$date .'" />					
		  <input type="hidden" id="eventIDArray" name="eventIDArray" value="'.implode(',',$eventIDArray).'" /></span>';
	
	
?>