<?php
/*
 * 	Using:
 * 	Log:
 * 	Date:	2014-02-27 [Cameron] Need to check $sid to avoid getting wrong data if the student is not the corresponding child of login parent
 */

	$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$sid);
	if (!$validChild)		// Not child of the parent, redirect to home directory
	{
		exit();
	}

	
	
# Student Info
$studentInfo = $objMedical->getStudentInfoByID($sid);
//debug_r($studentInfo);

# Object Data
$objLogMapper = new StudentLogMapper();
$studentLogObjAry = $objLogMapper->findByStudentId($sid,$date,$parEndDate = null,$orderBy = 'recordtime Desc');
	
	
	
$countStudentLogObjAry = count($studentLogObjAry);
if($countStudentLogObjAry == 0){
	echo '<div class=tableContent align=center colspan="6"><br>'.$Lang['SysMgr']['Homework']['NoRecord'].'<br><br></div>';
}else{
	foreach( (array)$studentLogObjAry as $index=>$studentLogObj){
		$objMedical->getEventLogInputUI($Id=$studentLogObj->getRecordID(), &$linterface, $eventDetail = &$studentLogObj, $countStudentLogObjAry-$index, $isReadOnly = true);
		$eventIDArray[] = $studentLogObj->getRecordID();
	}
}
?>