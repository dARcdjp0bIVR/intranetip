<?php
/*
 * 	using:
 * 
 * 	Date:	2014-02-27 [Cameron] Need to check $sid to avoid getting wrong data if the student is not the corresponding child of login parent
 */

function showNoData()
{
	global $Lang;
	$x = '<tr>
			<td colspan="8"  class="tableContent" align="center" >
				'.$Lang['General']['NoRecordFound'].'
			</td>
		  </tr>';
	return $x;	
}

//print "FilterStudent=" . $_POST["FilterStudent"] ."<br>";
//print "FilterYear=" . $_POST["FilterYear"] ."<br>";
//print "FilterMonth=" . $_POST["FilterMonth"] ."<br>";
$filterStudent = ($_POST["FilterStudent"])? $_POST["FilterStudent"] : $selectedStudent;
$filterYear = ($_POST["FilterYear"])? $_POST["FilterYear"] : $defaultYear;
$filterMonth = ($_POST["FilterMonth"])? $_POST["FilterMonth"] : $defaultMonth; 
 
$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$filterStudent);
if (!$validChild)		// Not child of the parent, redirect to home directory
{
	exit();
}
 
//print "student: $filterStudent, $filterYear, $filterMonth<br>"; 
$studentDetailList = $objMedical->getBowelLogListByParent($filterStudent, $filterYear, $filterMonth);
//debug_r($studentDetailList);

$objBowelStatus = new bowelStatus();
### Generate Legend HTML Start
$legendHTML ='';
$legendHTML .='<ul class="legend" style="list-style-type: none;">';

$bowelDetail = $objBowelStatus->getActiveStatus($whereCriteria = 'recordstatus = 1',$orderCriteria = '');

$i=0;
foreach( (array)$bowelDetail as $bowelItem){
	$legendHTML .='<li><span class="colorBoxStyle" style="background-color:'.$bowelItem['Color'].';">&nbsp;&nbsp;&nbsp;</span>&nbsp;'. $bowelItem['StatusName'].'</li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	if(++$i%8 == 0){
		$legendHTML .='<br /><br />';
	}
}
$legendHTML .='</ul>';
### Generate Legend HTML End

?>
<style>
.legend li{
	display: inline;
}
.colorBoxStyle{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:17px;
	height:17px;
}
.colorSmallBoxStyle{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:8px;
	height:8px;
}
</style>

<?
echo "<div>" . $legendHTML . "</div>";
?>

<table border="0" cellpadding="5" cellspacing="0" align="center" class="common_table_list" style="width: 95%;">
	<colgroup>
		<col style="width:3%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:20%"/>
		<col style="width:20%"/>
		<col style="width:10%"/>
		<col style="width:12%"/>
	</colgroup>
	<thead>
		<tr class="tabletop">
			<?php foreach( $Lang['medical']['bowel']['parent']['tableHeader'] as $tableHeader): ?>
				<th class="tabletoplink"><?php echo $tableHeader; ?></th>
			<?php endforeach; ?>
		</tr>		
	</thead>
	<tbody>
<?php
	if (($studentDetailList == false) || count($studentDetailList)==0)
	{
		echo showNoData();		
	}
	else
	{
		$i = 1; 
		foreach( $studentDetailList as $recordDate=>$studentDetail):
	
			##### Attendance
			foreach( $studentDetail as $recordTime=>$studentDetailItem)
			{
				$attendance = $studentDetailItem[0]['Attendance'];
				break;			
			}
			$disAttendance = ($attendance==1)? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present']:$Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
	
			$nrTimes = count($studentDetailList[$recordDate]);
			$h = 0;
			$row = '<tr>
						<td rowspan="'.$nrTimes.'">'.$i.'</td>
						<td rowspan="'.$nrTimes.'">'.$recordDate.'</td>
						<td rowspan="'.$nrTimes.'">'.$disAttendance.'</td>';
					
			foreach( $studentDetail as $recordTime=>$studentDetailItem)
			{
				$recordID = $studentDetailItem[0]["RecordID"];
	
				$bowelSyndList ='';
				$disRemark = "";
				$k = 0;			
				foreach($studentDetailItem as $number=>$syndromeDetail)
				{
					$disRemark .= ($k > 0)? "<br>" : "";
					
					$bowelSyndList .='<span class="colorSmallBoxStyle" style="background-color:'.$syndromeDetail['Color'].';">&nbsp;</span>&nbsp;'. $syndromeDetail['StatusName'].'&nbsp;&nbsp;&nbsp;&nbsp;';
					if(++$k%2 == 0){
						$bowelSyndList .='<br />';
					}
						
					$disRemark .= nl2br(stripslashes($syndromeDetail['Remarks']));
				}
	
				$disRemark = ($disRemark == "") ? "-" : $disRemark;
				
				$row .= ($h>0) ? '<tr>' : '';
				list($hour,$minutes) = explode(':',$recordTime);
				if($minutes <= 29){
					$recordTime = $hour.' : 00-29';
				}else{
					$recordTime = $hour.' : 30-59';
				}
				$row .= '<td><a class="editBtn" id="editRecord" data-date="'.$recordDate.'">'. $recordTime .'</a></td>';
				$row .= '<td>'.$bowelSyndList.'</td>';
				$row .= '<td>'.$disRemark.'</td>';
				$row .= '<td>'.$studentDetailItem[0]['LastUpdatedBy'].'</td>';
				$row .= '<td>'.$studentDetailItem[0]['LastUpdatedOn'].'</td>';
				$row .= '</tr>';
				$h++;
			}
			
			print $row;
		$i++; 
		endforeach;
	}
				
?>
	</tbody>

</table>

