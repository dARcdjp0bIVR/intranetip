<?php


$selectedStudent = $_REQUEST["Student"];	// Student UserID

$parentUserID = $_SESSION['UserID'];
//$parentUserID = 2981;	// Multiple Children example
//$parentUserID = 2571;	// Single Child example
//$studentList = $objMedical->getStudentInfoByParent($_SESSION['UserID']);
$studentList = $objMedical->getStudentInfoByParent($parentUserID);
//debug_r($studentList);

$nrStudent = count($studentList); 
if ( $nrStudent > 1)
{
	$valueArray = array();
	foreach($studentList as $k=>$v)
	{
		$valueArray[$v["UserID"]] = $v["Name"];
		if ($v["UserID"] == $selectedStudent)
		{
			$studentSleepType = $v["stayOverNight"];
			$studentClassName = $v["ClassName"];
			$studentClassNumber = $v["ClassNumber"];
		}
	}
	if (!$selectedStudent)
	{
		$studentSleepType = $studentList[0]["stayOverNight"];
		$studentClassName = $studentList[0]["ClassName"];
		$studentClassNumber = $studentList[0]["ClassNumber"];
		$selectedStudent = $studentList[0]["UserID"];			// Use 1st one as default
	}	
	
	$students = getCommonSelectionBox($id="Student", $name="Student", $valueArray, $valueSelected=$selectedStudent, $class="",$otherFeatures="onChange=getStudentInfo();");	
}
else if ($nrStudent == 1)
{
	$students = $studentList[0]["Name"];
	$studentSleepType = $studentList[0]["stayOverNight"];
	$studentClassName = $studentList[0]["ClassName"];
	$studentClassNumber = $studentList[0]["ClassNumber"];
	$selectedStudent = $studentList[0]["UserID"];	
}
else	// No children
{
//	header("Location: /");
	echo "no child info";
	exit();
}

$students .= " (". $studentClassNumber .")";

$disStuentSleepType = $studentSleepType? $Lang['medical']['general']['sleepOption']['StayIn']:$Lang['medical']['general']['sleepOption']['StayOut'];

	$x = '
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['medical']['general']['form']. '</td>
				<td class="formfieldtitle tabletext" width="15%">' . $studentClassName . '</td>
				<td width="70%"></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">' . $Lang['medical']['general']['student']. '</td>
				<td class="formfieldtitle tabletext" width="15%">' . $students. '</td>
				<td width="70%"><input type="hidden" id="FilterStudent" name="FilterStudent" value="'.$selectedStudent.'"></td>
			</tr>		
			<tr>
				<td valign="top" nowrap="nowrap" class="tabletext">' . $Lang['medical']['general']['sleepType']. '</td>
				<td class="tabletext" width="15%">' . $disStuentSleepType. '</td>
				<td width="70%"></td>
			</tr>
		</table>';
		
	echo $x;
?>