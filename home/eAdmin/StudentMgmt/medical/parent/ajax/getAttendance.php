<?php
/*
 * 	Using:
 *	Log:
 *	Date: 2014-02-27 [Cameron] Need to check $sid to avoid getting wrong data if the student is not the corresponding child of login parent
 *		Note!!!	Do not redirect header as it will cause blank page  
 */

$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$sid);
if (!$validChild)		// Not child of the parent, redirect to home directory
{
	exit();
}
 
$attendance = $objMedical->getAttendanceStatusOfUser($sid,$date);
$disAttendance = $attendance ? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present']: $Lang['medical']['bowel']['parent']['AttendanceOption']['Absent'];

echo "(".$Lang['medical']['bowel']['parent']['tableHeader']['AttendanceStatus'] . ":".$disAttendance .")";

?>