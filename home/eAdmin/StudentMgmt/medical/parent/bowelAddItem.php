<?php
// using: Cameron
/*
 * 	Log:
 * 	Date:	2014-02-27 [Cameron] Need to check $sid to avoid getting wrong data if the student is not the corresponding child of login parent
 */

if (!$objMedical->getParentViewAccess('bowel')){
	header("Location: /");
	exit();
}

$sid = trim($_POST["FilterStudent"]);		//Student UserID for particular student
$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$sid);
if (!$validChild)		// Not child of the parent, redirect to home directory
{
	header("Location: /");
	exit();
}

$CurrentPage = "ParentBowelLog";
$TAGS_OBJ[] = array($Lang['medical']['menu']['bowel'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();


//$objFCM_UI = new form_class_manage_ui;
//$menuOption = array();


$date = trim($_POST["r_date"]);
$date = ($date) ? $date : date("Y-m-d");	// default today  

////////////////////////// Object Data ////////////////////////////////////////////////

# Student Info
$studentInfo = $objMedical->getStudentInfoByID($sid);
//debug_r($studentInfo);


$linterface->LAYOUT_START($Msg);
//debug_r("<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n");
include_once('templates/bowelAddItem.tmpl.php');
$linterface->LAYOUT_STOP();
?>