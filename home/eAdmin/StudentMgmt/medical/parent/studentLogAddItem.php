<?php
// using: Pun


if (!$objMedical->getParentViewAccess('studentLog')){
	header("Location: /");
	exit();
}

$sid = IntegerSafe(trim($_POST["FilterStudent"]));		//Student UserID for particular student
$validChild = $objMedical->isChildOfTheParent($_SESSION['UserID'],$sid);
if (!$validChild)		// Not child of the parent, redirect to home directory
{
	header("Location: /");
	exit();
}

$CurrentPage = "ParentStudentLogLog";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentLog'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();


//$objFCM_UI = new form_class_manage_ui;
//$menuOption = array();


$date = trim($_POST["r_date"]);
$date = ($date) ? $date : date("Y-m-d");	// default today  

////////////////////////// Object Data ////////////////////////////////////////////////

# Student Info
$studentInfo = $objMedical->getStudentInfoByID($sid);
//debug_r($studentInfo);

# Object Data
$objLogMapper = new StudentLogMapper();
$studentLogObjAry = $objLogMapper->findByStudentId($sid,$date,$parEndDate = null,$orderBy = 'recordtime Desc');

$linterface->LAYOUT_START($Msg);
//debug_r("<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n");
include_once('templates/studentLogAddItem.tmpl.php');
$linterface->LAYOUT_STOP();
?>