<?php
// using: 
/*
 * 2018-12-27 Cameron
 * - show error message when deleting non-self record
 *  
 * 2018-06-05 Cameron
 * - show created by person [case #F133828]
 * - don't show title for people column (use getNameFieldByLang2 instead of getNameFieldByLang) 
 * 
 * 2018-04-27 Cameron
 * - set 2 to column_array for EventType to strip slashes
 * - apply stripslashes to $eventTypeAry in option list
 *
 * 2018-04-11 Cameron
 * - fix sorting in EventDate column
 *
 * 2018-04-03 Cameron
 * - show EventTypeNotSetup message if there's no event type when click add event button
 *
 * 2018-03-20 Cameron
 * - keyword include AffectedNonAccountHolder [case #F133828]
 * - AffectedPerson column include AffectedNonAccountHolder
 * - add group filter [case #F121742]
 *
 * 2018-02-27 Cameron
 * - add data-deleteAllow attribute to checkbox item for checking if it's allowed to delete [case #F135176]
 * - show DateModified
 *
 * 2018-01-23 Cameron
 * - change default order to descending by date
 *
 * 2017-11-28 Cameron
 * - fix search filter cookie
 *
 * 2017-06-12 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$arrCookies = array();
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_right_page_number",
    "pageNo"
);
$arrCookies[] = array(
    "ck_right_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_right_page_field",
    "field"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventClassName",
    "ClassName"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventGroupID",
    "GroupID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventType",
    "EventType"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventStudent",
    "EventStudent"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventLocationBuildingID",
    "LocationBuildingID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventLocationLevelID",
    "LocationLevelID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventLocationID",
    "LocationID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventAffectedPerson",
    "EventAffectedPerson"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventReporter",
    "EventReporter"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventDate",
    "EventDate"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventDateStart",
    "EventDateStart"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventDateEnd",
    "EventDateEnd"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventTimeStartHour",
    "TimeStartHour"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventTimeStartMin",
    "TimeStartMin"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventTimeEndHour",
    "TimeEndHour"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventTimeEndMin",
    "TimeEndMin"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_EventKeyword",
    "keyword"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order; // default descending
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);

$AcademicYearID = Get_Current_Academic_Year_ID();
$classCond = '';
$cond = '';

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNameEng = $classNameField;
    $joinClass = '';
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "' ";
}

if ($GroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $GroupID . "'";
}

if ($ClassName) {
    $cond .= " AND s.ClassName LIKE '%" . $li->Get_Safe_Sql_Like_Query($ClassName) . "%'";
}

if ($EventStudent) {
    $cond .= " AND s.StudentID LIKE '%" . $li->Get_Safe_Sql_Like_Query('^' . $EventStudent . '~') . "%'";
}

if ($EventType) {
    $cond .= " AND e.EventTypeID='" . $EventType . "'";
}

if ($LocationBuildingID) {
    $cond .= " AND e.LocationBuildingID='" . $LocationBuildingID . "'";
}

if ($LocationLevelID) {
    $cond .= " AND e.LocationLevelID='" . $LocationLevelID . "'";
}

if ($LocationID) {
    $cond .= " AND e.LocationID='" . $LocationID . "'";
}

if ($EventAffectedPerson) {
    $cond .= " AND a.AffectedPersonID LIKE '%" . $li->Get_Safe_Sql_Like_Query('^' . $EventAffectedPerson . '~') . "%'";
    $affectedPersonJoin = " INNER JOIN";
} else {
    $affectedPersonJoin = " LEFT JOIN";
}

if ($EventReporter) {
    $cond .= " AND e.LastModifiedBy='" . $EventReporter . "'";
}

if ($EventDate) {
    if ($EventDateStart) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.StartTime)>='" . $EventDateStart . " " . $TimeStartHour . ":" . $TimeStartMin . ":00'";
    }
    
    if ($EventDateEnd) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.EndTime)<='" . $EventDateEnd . " " . $TimeEndHour . ":" . $TimeEndMin . ":00'";
    }
}

$locationField = Get_Lang_Selection("NameChi", "NameEng");
$student_name = getNameFieldByLang2('u.');
$affected_name = getNameFieldByLang2('u.');
$createdBy = getNameFieldByLang2('cb.');
$updatedBy = getNameFieldByLang2('ub.');

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if ($keyword != "") {
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (s.Student LIKE '%$kw%' OR a.AffectedPerson LIKE '%$kw%' OR e.AffectedNonAccountHolder LIKE '%$kw%')";
    unset($kw);
}

$isRequiredCheckSelfEvent = $objMedical->isRequiredCheckSelfEvent();
$checkSelfDeleteStr = "IF(e.InputBy=" . $_SESSION['UserID'] . ",1,0)";
$dataDeleteAllowValue = $isRequiredCheckSelfEvent ? $checkSelfDeleteStr : '1';

$sql = "SELECT 
				CONCAT('<a href=\"?t=management.event_view&EventID=',e.`EventID`,'\">',e.EventDate,'</a>'),
				s.Student,
				DATE_FORMAT(e.StartTime,'%k:%i'),
				DATE_FORMAT(e.EndTime,'%k:%i'),
				et.EventTypeName,
				CASE 
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN '-'
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN loc." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN lvl." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID<>0 THEN CONCAT(lvl." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN b." . $locationField . "
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN CONCAT(b." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ")
					ELSE CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ",' > ',loc." . $locationField . ")
				END as Location,
				IF(e.AffectedNonAccountHolder IS NULL OR e.AffectedNonAccountHolder='', IF(a.AffectedPerson IS NULL OR a.AffectedPerson='', '-', a.AffectedPerson), CONCAT(IF(a.AffectedPerson IS NULL,'',CONCAT(a.AffectedPerson,',<br>')), e.AffectedNonAccountHolder)) as AffectedPerson, 
				" . $createdBy. " as CreatedBy,
                " . $updatedBy. " as UpdatedBy,
                e.DateModified,
				CONCAT('<input type=\'checkbox\' name=\'EventID[]\' id=\'EventID[]\' data-deleteAllow=\'',$dataDeleteAllowValue,'\' value=\'', e.`EventID`,'\'>'),
                CONCAT(e.EventDate,e.StartTime) as EventDate,
                e.StartTime,
                e.EndTime
		FROM
				MEDICAL_EVENT e
		INNER JOIN (
				SELECT 	EventID, 
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as StudentID,
						GROUP_CONCAT(DISTINCT CONCAT('^',$classNameEng,'~') ORDER BY $classNameEng SEPARATOR ',') as ClassName, 
						GROUP_CONCAT(DISTINCT CONCAT(" . $student_name . ",' ('," . $classNameField . ",IF(p.stayOverNight=1,'" . $Lang['medical']['general']['sleepOption']['StayIn'] . "','" . $Lang['medical']['general']['sleepOption']['StayOut'] . "'),')') ORDER BY u.ClassName, u.ClassNumber SEPARATOR ',<br>') as Student
				FROM 
						MEDICAL_EVENT_STUDENT s
				INNER JOIN 
						INTRANET_USER u ON u.UserID=s.StudentID
				" . $joinClass . "
				LEFT JOIN 
						INTRANET_USER_PERSONAL_SETTINGS p ON p.UserID=u.UserID
				GROUP BY s.EventID
			) AS s ON s.EventID=e.EventID
		LEFT JOIN MEDICAL_EVENT_TYPE et ON et.EventTypeID=e.EventTypeID
		LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=e.LocationBuildingID
		LEFT JOIN INVENTORY_LOCATION_LEVEL lvl ON lvl.LocationLevelID=e.LocationLevelID
		LEFT JOIN INVENTORY_LOCATION loc ON loc.LocationID=e.LocationID
		" . $affectedPersonJoin . " (
				SELECT 	EventID, 
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as AffectedPersonID,
						GROUP_CONCAT(DISTINCT " . $affected_name . " ORDER BY " . $affected_name . " SEPARATOR ',<br>') AS AffectedPerson
				FROM 
						MEDICAL_EVENT_AFFECTED_PERSON p
				INNER JOIN 
						INTRANET_USER u ON u.UserID=p.AffectedPersonID
				GROUP BY EventID
			) AS a ON a.EventID=e.EventID
		LEFT JOIN INTRANET_USER cb ON cb.UserID=e.InputBy
        LEFT JOIN INTRANET_USER ub ON ub.UserID=e.LastModifiedBy
		WHERE 1 " . $cond;
// debug_r($sql);

$li->field_array = array(
    "EventDate",
    "Student",
    "StartTime",
    "EndTime",
    "EventTypeName",
    "Location",
    "AffectedPerson",
    "CreatedBy",
    "UpdatedBy",
    "DateModified"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0,
    0,
    0,
    2,
    0,
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['EventDate']) . "</th>\n";
$li->column_list .= "<th width='16%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['StudentName']) . "</th>\n";
$li->column_list .= "<th width='6%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['StartTime']) . "</th>\n";
$li->column_list .= "<th width='6%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['EndTime']) . "</th>\n";
$li->column_list .= "<th width='19%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['EventType']) . "</th>\n";
$li->column_list .= "<th width='14%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['Place']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['AffectedPerson']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['event']['tableHeader']['ReportPerson']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['general']['lastModifiedBy']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['general']['dateModified']) . "</th>\n";
$li->column_list .= "<th width='1'>" . $li->check("EventID[]") . "</th>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet2(document.form1,'?t=management.new_event')", $button_new, "", "", "", 0);

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='ClassName' id='ClassName'", $ClassName, "", $Lang['medical']['event']['AllClass'], $AcademicYearID);
// Group Filter
$groupFilter = $objMedical->getGroupSelection("GroupID", $GroupID, "", $Lang['medical']['event']['AllGroup']);

// Event Type Filter
$eventTypeAry = $objMedical->getEventType();
foreach ((array) $eventTypeAry as $_idx => $_eventType) {
    $eventTypeAry[$_idx]['EventTypeName'] = stripslashes($_eventType['EventTypeName']);
    $eventTypeAry[$_idx][1] = $eventTypeAry[$_idx]['EventTypeName']; // 2nd column
}
$eventTypeFilter = getSelectByArray($eventTypeAry, "name='EventType' onChange='this.form.submit();'", $EventType, 0, 0, $Lang['medical']['event']['AllCategory']);

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingFilter = getSelectByArray($buildingAry, "Name='LocationBuildingID' ID='LocationBuildingID' onChange='changeBuilding();'", $LocationBuildingID, 0, 0, $Lang['SysMgr']['Location']['All']['Building']);

if ($LocationBuildingID) {
    $levelAry = $objMedical->getInventoryLevelArray($LocationBuildingID);
    $levelFilter = getSelectByArray($levelAry, "Name='LocationLevelID' ID='LocationLevelID' onChange='changeBuildingLevel();'", $LocationLevelID, 0, 0, $Lang['SysMgr']['Location']['All']['Floor']);
}

if ($LocationLevelID) {
    $locationAry = $objMedical->getInventoryLocationArray($LocationLevelID);
    $locationFilter = getSelectByArray($locationAry, "Name='LocationID' ID='LocationID' onChange='this.form.submit();'", $LocationID, 0, 0, $Lang['SysMgr']['Location']['All']['Room']);
}

if (! empty($ClassName) && ! empty($GroupID)) {
    $includeUserIdAry = $objMedical->getMedicalGroupStudent(array(
        $GroupID
    ));
    $includeUserIdAry = BuildMultiKeyAssoc($includeUserIdAry, 'UserID', array(
        'UserID'
    ), $SingleValue = 1);
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($ClassName, $recordstatus = '', $includeUserIdAry);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName, $includeUserIdAry);
    }
} else if (! empty($ClassName) && empty($GroupID)) {
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
    }
} else if (empty($ClassName) && ! empty($GroupID)) {
    $studentAry = $objMedical->getStudentNameListWClassNumberByGroupID($GroupID);
} else {
    $studentAry = $objMedical->getAllStudents($currentAcademicYearID);
}

$studentFilter = getSelectByArray($studentAry, "name='EventStudent' id='EventStudent' onChange='this.form.submit();'", $EventStudent, 0, 0, $Lang['medical']['event']['AllStudent']);

$teacherAry = $objMedical->getTeacher();
$reporterFilter = getSelectByArray($teacherAry, "name='EventReporter' onChange='this.form.submit();'", $EventReporter, 0, 0, $Lang['medical']['case']['AllReporter']);

$affectedPersonAry = array_merge($studentAry, $teacherAry);
$affectedPersonFilter = getSelectByArray($affectedPersonAry, "name='EventAffectedPerson' onChange='this.form.submit();'", $EventAffectedPerson, 0, 0, $Lang['medical']['event']['AllAffectedPerson']);

$EventDateStart = $EventDateStart ? $EventDateStart : substr(getStartDateOfAcademicYear($AcademicYearID), 0, 10);
$EventDateEnd = $EventDateEnd ? $EventDateEnd : substr(getEndDateOfAcademicYear($AcademicYearID), 0, 10);
// ############ end Filters

$CurrentPage = "ManagementEvent";
$CurrentPageName = $Lang['medical']['menu']['event'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['event'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();
if ($returnMsgKey == 'EventTypeNotSetup') {
    $returnMsg = $Lang['medical']['event']['ReturnMessage']['EventTypeNotSetup'];
}elseif ($returnMsgKey == 'DeleteNonSelfRecord') {
    $returnMsg = $Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'];
}else {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

include_once ('templates/event_list.tmpl.php');
$linterface->LAYOUT_STOP();
?>