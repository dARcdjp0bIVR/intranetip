<?php
// using: Adam
/*
 *  Transaction is temporarily disabled
 * 	
 */
//$_POST['date'];
//$_POST['timePeriod'];

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_MANAGEMENT') || !$plugin['medical_module']['meal']){
	header("Location: /");
	exit();
}

$objDB = new libdb();
$objMealDailyLog = new mealDailyLog('1970-01-01');
$result = array();


$objDB->Start_Trans();

# Delete record
if(is_array($_POST['deleteReacordList'])){ // For delete wrong sleep record
	$recordIDArr = $_POST['deleteReacordList'];
	$recordDateArr = $_POST['deleteRecordDate'];
	if(count($recordIDArr)>0){
		foreach($recordIDArr as $index=>$recordID){
			$result[] = $objMealDailyLog->deleteRecord($recordDateArr[$index],$recordID);
		}
	}
}else{
	$recordIDList = trim(trim(htmlentities($_POST['deleteReacordList'], ENT_QUOTES, 'UTF-8')),',');
	if(strlen($recordIDList)>0){
		$recordIDArr = explode(',',$recordIDList);
		foreach($recordIDArr as $recordID){
			$result[] = $objMealDailyLog->deleteRecord($_POST['date'],$recordID);
		}
	}
}

/*$recordList = trim(trim(htmlentities($_POST['deleteReacordList'], ENT_QUOTES, 'UTF-8')),',');
if(strlen($recordList)>0){ // If user has delete record. 
	$recordArr = explode(',',$recordList);
	foreach($recordArr as $recordID){
		$result[] = $objMealDailyLog->deleteRecord($_POST['date'],$recordID);
	}
}*/

# Update record
if(count($_POST['mealRecord'])>0){
	//////////////// INPUT CHECKING /////////
	if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
		echo 'Date Format Error! Please select with format YYYY-MM-DD';
		return;
	}
	foreach($_POST['mealRecord'] as $userID =>$studentMealDetail){
		$objMealDailyLog = new mealDailyLog($_POST['date'], $studentMealDetail['RecordID']);
	
				$objMealDailyLog->setUserID($userID);
				$objMealDailyLog->setMealStatus(trim($studentMealDetail['mealStatus']));
				$objMealDailyLog->setRemarks(trim( htmlentities($studentMealDetail['remarks'], ENT_QUOTES, 'UTF-8')));
				$objMealDailyLog->setPeriodStatus($_POST['timePeriod']);
				$objMealDailyLog->setModifyBy($_SESSION['UserID']);
				$objMealDailyLog->setConfirmedUserID($_SESSION['UserID']);
				$objMealDailyLog->setInputBy($_SESSION['UserID']);
		$result[] = $objMealDailyLog->save();
	}
}
$finalResult = (!in_array( false, (array)$result));

if( $finalResult ){
	//$Msg = 'Save Succeeds';
	$objDB->Commit_Trans();
}
else{
	//$Msg = 'Save Fails';
	$objDB->RollBack_Trans();
}

echo $finalResult;
//header('Location: ?t=management.meal&Msg='.$Msg);
//exit;
?>