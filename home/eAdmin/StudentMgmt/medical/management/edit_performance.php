<?php
// using:
/*
 * 	2017-07-11 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='AWARDSCHEME_MANAGEMENT') || !$plugin['medical_module']['discipline']){
	header("Location: /");
	exit();
}

if (is_array($SchemeID)) {
	$schemeID = (count($SchemeID)==1) ? $SchemeID[0] : '';
}
else {
	$schemeID = $SchemeID;
}

if (!$schemeID) {	
	header("Location: ?t=management.award_list");
}

$groupID = $GroupID;

$rs_scheme = $objMedical->getSchemes($schemeID);
if (count($rs_scheme) == 1) {	
	$rs_scheme = current($rs_scheme);
}

$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['award'], "");
$PAGE_NAVIGATION[] = array($Lang['medical']['award']['Performance'], "");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

?>
<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
.non_recent_col{display:none;}
</style>

<script language="javascript">

$().ready( function(){
	$("#submitBtn_tb").click(function(e){
		
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		
		if (checkForm()) {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: "?t=management.ajax.ajax&m=1&action=savePerformance",
				data : $("#form2").serialize(),		  
				success: function(ajaxReturn){
					if (ajaxReturn != null && ajaxReturn.success){
						js_Hide_ThickBox();				
						$('input.actionBtn').attr('disabled', '');
						
						if (ajaxReturn!=null && ajaxReturn.msg == 'SaveSuccess') {
							Get_Return_Message("<?=$Lang['General']['ReturnMessage']['RecordSaveSuccess']?>");
						}
						else if (ajaxReturn!=null && ajaxReturn.msg == 'SaveUnsuccess') {
							Get_Return_Message("<?=$Lang['General']['ReturnMessage']['RecordSaveUnSuccess']?>");
						}
					}
				},
				error: show_ajax_error
			});
		}
		else {
			$('input.actionBtn').attr('disabled', '');	
		}
		
	});
	
	
	$(".PerfTargetName").each(function(){
			
		if ($.trim($(this).val()).length > 0) {
			$(this).autocomplete(
		      "?t=management.ajax.ajax_get_target_suggestion&ma=1&from=performance&q=",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			onItemSelect: function() { jsShowOrHideTarget($(this).attr('id')); },
		  			formatItem: function(row){ return row[0]; },
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px',
		  			width:'306px'
		  		}
		    );
		}
	});

});

// replace following str: 'New' => '', \[ => \\[, \] => \\]
function str4jquery(str) {
	str = str.replace(/New/,'');
	str = str.replace(/\[/g,'\\[');
	str = str.replace(/\]/g,'\\]');
	return str;
}

function checkForm() {
	var error = 0;
	var focusField = '';
	var id;
	hideError();
	
	$(".PerfTargetName").each(function(){
		if ($.trim($(this).val()) == '') {
			error++;
			if (focusField == '') $(this).focus();
			
			id = $(this).attr('id');
			id = str4jquery(id);

			$('#Err'+id).addClass('error_msg_show').removeClass('error_msg_hide');
		}
	});

	$(".positiveint").each(function(){

		if (($.trim($(this).val()) != '') && ((isNaN($(this).val())) || ($(this).val() < 0) || ($(this).val() > 100) || ($(this).val().indexOf('.') != -1))) {
	
			error++;
			if (focusField == '') $(this).focus();
			
			id = $(this).attr('id');
			id = str4jquery(id);
			$('#Err'+id).addClass('error_msg_show').removeClass('error_msg_hide');
			
		}
	});

	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function jsShowOrHideTarget(id) {
	timerobj = setTimeout(ShowOrHideTargetAction(id),1000);
}

function ShowOrHideTargetAction(id) {
	$("#"+id).focus();
	clearTimeout(timerobj);
}

function add_target(studentID,nrDate,pastIdx,futureIdx) {
	var rowHtml;
	var nr_row=0;
	var nr_new_row = 0;
	var col_class = '';
	var selector = '';
	var src = $('#icon_more').attr('src');
	
	if ($('tr.new_'+studentID).length > 0) {
		nr_new_row += $('tr.new_'+studentID).length;
		nr_row += nr_new_row;
	}
	if ($('tr.edit_'+studentID).length > 0) {
		nr_row += $('tr.edit_'+studentID).length;
	}
	nr_row++;
	nr_new_row++;
	
	$('tr[class $= "_'+studentID+'"]:first td:first').attr('rowspan',nr_row);
	$('tr[class $= "_'+studentID+'"]:first td:eq(1)').attr('rowspan',nr_row);
	

	rowHtml = '<tr class="new_'+studentID+'" id="new_'+studentID+'_'+nr_new_row+'">';
	rowHtml += '<td class="tabletext"><input type="text" name="NewTarget['+studentID+']['+nr_new_row+']" id="NewTarget['+studentID+']['+nr_new_row+']" class="textboxtext PerfTargetName" style="min-width:100px;" value="">';
	rowHtml += '<span class="error_msg_hide" id="ErrTarget['+studentID+']['+nr_new_row+']"><?=$Lang['medical']['award']['Warning']['InputSuggestion']?></span></td>';
	for (var i=0; i<nrDate; i++) {
		if (i<=pastIdx) {
			col_class = (src.indexOf('more_on') == -1) ? '' : ' non_recent_col';
			selector = 'non_recent';
		}
		else if (i>=futureIdx && futureIdx!=-1) {
			col_class = (src.indexOf('more_on') == -1) ? '' : ' non_recent_col';
			selector = 'non_recent';
		}
		else {
			col_class = '';
			selector = 'recent';
		}
		rowHtml += '<td class="tabletext '+selector+col_class+'"><input class="positiveint" type="text" size="4" maxlength="3" name="NewScore['+studentID+']['+nr_new_row+']['+parseInt(i+1)+']" id="NewScore['+studentID+']['+nr_new_row+']['+parseInt(i+1)+']" value="">';
		rowHtml += '<span class="error_msg_hide" id="ErrScore['+studentID+']['+nr_new_row+']['+parseInt(i+1)+']"><?=$Lang['medical']['award']['Warning']['InputInteger']?></span></td>';
	}
	rowHtml += '<td width="1%"><span class="table_row_tool"><a class="delete" title="<?=$Lang['medical']['award']['RemoveTarget']?>" onClick="remove_new_target(\''+studentID+'\',\''+(nr_new_row)+'\')"></a></span></td>';
	rowHtml += '</tr>';
	$('tr[class$="_'+studentID+'"]:last').after(rowHtml);
	
	$('#NewTarget\\['+studentID+'\\]\\['+nr_new_row+'\\]').autocomplete(
      "?t=management.ajax.ajax_get_target_suggestion&ma=1&from=performance&q=",
      {
  			delay:3,
  			minChars:1,
  			matchContains:1,
  			onItemSelect: function() { jsShowOrHideTarget('NewTarget['+studentID+']['+nr_new_row+']'); },
  			formatItem: function(row){ return row[0]; },
  			autoFill:false,
  			overflow_y: 'auto',
  			overflow_x: 'hidden',
  			maxHeight: '200px',
  			width:'306px'
  		}
    );
	
}

function remove_target(studentTargetID) {
	var conf = confirm('<?=$Lang['medical']['award']['Warning']['ConfirmDeleteStudentTarget']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: "?t=management.ajax.ajax&m=1&action=removeStudentTarget",
			data : 	{
						'StudentTargetID': studentTargetID
					},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					var rowObj = $('#Target\\['+studentTargetID+'\\]').parent().parent();
					var id = rowObj.attr('id');
					var res = id.split('_');
					var studentID = res[1];
					var nr_row = $('tr[class $= "_'+studentID+'"]:first td:first').attr('rowspan');
					nr_row = parseInt(nr_row) - 1;
					 
					if (rowObj.attr('id') == $('tr[class $= "_'+studentID+'"]:first').attr('id') && nr_row > 0) {	// record to remove is the first row of a student
						var firstCol = $('tr[class $= "_'+studentID+'"]:first td:first').html();
						var secondCol = $('tr[class $= "_'+studentID+'"]:first td:eq(1)').html();
						$('tr[class $= "_'+studentID+'"]:eq(1) td:first').before('<td>'+secondCol+'</td>');
						$('tr[class $= "_'+studentID+'"]:eq(1) td:first').before('<td>'+firstCol+'</td>');
					}
					
					var student_seq = parseInt($('#'+id+' td:first').html());

					rowObj.remove();
					$('tr[class $= "_'+studentID+'"]:first td:first').attr('rowspan',nr_row);
					$('tr[class $= "_'+studentID+'"]:first td:eq(1)').attr('rowspan',nr_row);
					
					if (nr_row == 0) {	// remove last row of the student, should re-order row number to display
						var cur_student_seq;
						$('.student_seq').each(function(){
							cur_student_seq = parseInt($(this).html()); 
							if (cur_student_seq > student_seq) {
								cur_student_seq--;
								$(this).html(cur_student_seq);
							}
						});
					}
				}
			},
			error: show_ajax_error
		});
	}
}

function remove_new_target(studentID, rowNo) {
	var nr_row = $('tr[class $= "_'+studentID+'"]:first td:first').attr('rowspan');
	nr_row = parseInt(nr_row) - 1;
	var id = $('tr.new_'+studentID+':last').attr('id');	// last new row id of the student
	var j;

	if ($('#new_'+studentID+'_'+rowNo).attr('id') == $('tr[class $= "_'+studentID+'"]:first').attr('id') && nr_row > 0) {	// record to remove is the first row of a student
		var firstCol = $('tr[class $= "_'+studentID+'"]:first td:first').html();
		var secondCol = $('tr[class $= "_'+studentID+'"]:first td:eq(1)').html();
		$('tr[class $= "_'+studentID+'"]:eq(1) td:first').before('<td>'+secondCol+'</td>');
		$('tr[class $= "_'+studentID+'"]:eq(1) td:first').before('<td>'+firstCol+'</td>');
	}
	
	var student_seq = parseInt($('#new_'+studentID+'_'+rowNo+' td:first').html());
	
	$('#new_'+studentID+'_'+rowNo).remove();
	$('tr[class $= "_'+studentID+'"]:first td:first').attr('rowspan',nr_row);
	$('tr[class $= "_'+studentID+'"]:first td:eq(1)').attr('rowspan',nr_row);
	
	var idx = id.lastIndexOf('_');
	if (idx != -1) {
		var max_row = id.substr(idx+1);
		
		if (rowNo != max_row) {	// not removing last row
			var startIdx = parseInt(rowNo) + 1;
			var nrDate = $(':input[name^="NewScore\['+studentID+'\]\['+startIdx+'\]"]').length;
			
			for (var i=startIdx; i<=max_row; i++) {
				j = i - 1;
				$('#new_'+studentID+'_'+i + ' :input[name^="NewTarget"]').attr('name','NewTarget['+studentID+']['+j+']');
				$('#new_'+studentID+'_'+i + ' :input[name^="NewTarget"]').attr('id','NewTarget['+studentID+']['+j+']');
				
				for (var h=1; h<=nrDate; h++) {
					$('#NewScore\\['+studentID+'\\]\\['+i+'\\]\\['+h+'\\]').attr('name', 'NewScore\['+studentID+'\]\['+j+'\]\['+h+'\]');				
					$('#NewScore\\['+studentID+'\\]\\['+i+'\\]\\['+h+'\\]').attr('id', 'NewScore\['+studentID+'\]\['+j+'\]\['+h+'\]');
					$('#ErrScore\\['+studentID+'\\]\\['+i+'\\]\\['+h+'\\]').attr('id', 'ErrScore\['+studentID+'\]\['+j+'\]\['+h+'\]');
				}
				$('#new_'+studentID+'_'+i+' .delete').attr('onClick', 'remove_new_target(\''+studentID+'\',\''+(j)+'\')');
				$('#new_'+studentID+'_'+i).attr('id', 'new_'+studentID+'_'+j);
			}
		}
		else {	// remove last row of the student, should re-order row number to display
			var cur_student_seq;
			$('.student_seq').each(function(){
				cur_student_seq = parseInt($(this).html()); 
				if (cur_student_seq > student_seq) {
					cur_student_seq--;
					$(this).html(cur_student_seq);
				}
			});
		}
	}			

}

function show_ajax_error() {
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function hideError() {
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function toggleColumn() {
	$('.non_recent').toggleClass('non_recent_col');
	var src = $('#icon_more').attr('src');
	if (src.indexOf('more_on') == -1) {
		$('#icon_more').attr('src','/images/2009a/icon_and_more_on.gif');
		$('#icon_more').parent().attr('title','<?=$Lang['medical']['award']['ShowAllDate']?>');
	}
	else {
		$('#icon_more').attr('src','/images/2009a/icon_and_more_off.gif');
		$('#icon_more').parent().attr('title','<?=$Lang['medical']['award']['ShowRecentDate']?>');
	}
}
</script>

<form name="form2" id="form2" method="post" action="">
	<div>
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
			<tr>
				<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			</tr>
		</table>
	</div>
	
	<?=$objMedical->getSchemePerformanceList($schemeID,$groupID)?>
	
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
		<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
	<p class="spacer"></p>
</div>
<?=$linterface->GET_HIDDEN_INPUT('SchemeID', 'SchemeID', $schemeID)?>
<?=$linterface->GET_HIDDEN_INPUT('GroupID', 'GroupID', $groupID)?>
	
</form>
