<?php
// modify : 
/*
 * 	Log
 * 
 * 	2017-07-26 [Cameron]
 * 		- create this file
 * 	
 */

include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentBowelImport.php"); 
include_once($PATH_WRT_ROOT."includes/libuser.php");

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='BOWEL_MANAGEMENT') || !$plugin['medical_module']['bowel']){
	header("Location: /");
	exit();
}

$result = array();
$Msg = '';

$studentLogImport = new libStudentBowelImport();
$data = $studentLogImport->ReadStudentBowelImportData($TargetFilePath);

$nrSuccess = 0;	// number of successfully import record
$nrRec = 0;		// total number of records to import

if ($data != false)
{
	if ($data["HeaderError"] == true)
	{
		$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess_IncorrectHeaderFormat'];
	}	
	else if ($data["NumOfData"] == 0)
	{
		$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess_NoRecord'];
	}
	else	// Import Data exists
	{
		$rs = $data['Data'];
		$nrRec = count($rs);
		
		$objLibMedical = new libMedical();
		
		for ($i = 0; $i < $nrRec; $i++)
		{			
			if ($rs[$i]['pass'])
			{	
				$crs = $rs[$i]['rawData'];
				
				$objBowelLog = new BowelLog($crs['RecordDate'] . ' ' . $crs['RecordTime']);
				$objBowelLog->setUserID($crs['StudentID']);
				$objBowelLog->setBowelID($crs['BowelID']);				
				$objBowelLog->setRecordTime($crs['RecordDate'].' '.$crs['RecordTime']);
				$objBowelLog->setInputBy($_SESSION['UserID']);
				$saveResult = $objBowelLog->save();
				
				if($saveResult){
					$nrSuccess++;
				}
				$result[] = $saveResult;
			}	// pass
		}
	}
}
else
{
	$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
}

if (($Msg =='') && ($nrSuccess > 0) && ($nrRec > 0))
{
	if(!in_array(0, $result)){
		if ($nrSuccess == $nrRec)
		{		
			$Msg = $Lang['General']['ReturnMessage']['ImportSuccess'];
		}		
		else if ($nrSuccess < $nrRec)
		{
			$Msg = $Lang['medical']['ReturnMessage']['ImportPartiallySuccess'];
		}
		else
		{
			$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
		}
	}
	else
	{
		$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];		
	}
}
else
{
	$Msg = $Msg ? $Msg : $Lang['General']['ReturnMessage']['ImportUnsuccess'];
}



$CurrentPage = "ManagementBowel";
$TAGS_OBJ[] = array($Lang['medical']['menu']['bowel'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=3, $CustStepArr='');

$Title = $Lang['medical']['general']['ImportData'];

$PAGE_NAVIGATION[] = array($Title,"");

$linterface->LAYOUT_START($Msg);

$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t=management.bowel'","back_btn"," class='formbutton' ");
?>
<form method="POST" name="frm1" id="frm1" action="?t=management.bowel">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
		</tr>
		<tr>
			<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
			<td><?=$h_stepUI;?></td>
		</tr>
	</table>
			
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr height="20"><td>&nbsp;</td></tr>				
		<tr>
			<td align='center'>
		<?= $nrSuccess ."&nbsp;". $Lang['General']['ImportArr']['RecordsImportedSuccessfully']?>
			</td>
		</tr>
		<tr height="20"><td>&nbsp;</td></tr>
		<tr>
		    <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>

		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		            <tr>
						<td align="center">
						<br/>
						<input type="hidden" name="t" value="management.bowel"/>					
							<?= $BackBtn ?>				
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>

<?php

$linterface->LAYOUT_STOP();
?>