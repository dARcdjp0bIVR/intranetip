<?php
// using:
/*
 * Log
 *
 * 2018-04-03 [Cameron]
 * - store student log event
 *
 * 2018-01-03 [Cameron]
 * - add empty checking before call deleteStudentLogRecords()
 *
 * 2017-09-18 [Cameron]
 * - add send email to pic function
 *
 * 2015-12-04 [Cameron]
 * - get StudentLogID from return value of save method rather than StudentLog->getRecordID for new record
 * because it cannot pass by reference ( support php 5.4)
 *
 * 2015-01-22 [Cameron] Add $plugin['medical_module']['AlwaysShowBodyPart'], save MEDICAL_STUDENT_LOG_PARTS if this is true
 * no matter Lev2Name = $medical_cfg['studentLog']['level2']['specialStatus'] or not
 *
 * !!! Note:
 * 1. body parts (Level 4 and Level 3 items are deleted first, then add them back when update StudentLog)
 *
 */
// /home/web/eclass40/intranetIP25/includes/libUploadFiles.php
include_once ($PATH_WRT_ROOT . "/includes/libUploadFiles.php");
include_once ($PATH_WRT_ROOT . "/includes/libwebmail.php");
include_once ($PATH_WRT_ROOT . "/lang/email.php");

$sid = trim($sid); // student UserID
$r_date = trim($_POST['date']);
$date = trim($_POST['date']);

// ///////////////////////
// IMPORTANCE VARIABLE //
// ///////////////////////
// store the HTML event mapping , ie map n1 (html) to DB table MEDICAL_STUDENT_LOG.RecordID,
// eg $inputIdMapWithDBId['n1'] = 1234; // 1234 is the table MEDICAL_STUDENT_LOG.RecordID , IT IS USED FOR SAVE PART AND DOCUMENT
$inputIdMapWithDBId = array();

$eventIDArray = explode(',', $eventIDArray);
$filesIDDeletedList = array();
$result = array();

foreach ((array) $event as $key => $details) {
    
    $objStudentLogPartsMapper = null;
    // $_recordId = '';
    
    // $objStudendLog = new StudentLog();
    if (strpos($key, 'n') === 0) {
        $objStudendLog = new StudentLog();
        $is_new = true;
    } else {
        $is_new = false;
        if ($key > 0 && is_numeric(intval($key))) {
            // assign the student log recordid for update
            $objStudendLog = new StudentLog($key);
            // $objStudendLog->setRecordID($key);
            
            $eventIDArray = array_diff((array) $eventIDArray, (array) $key);
        } else {
            // not a valid key value , skip this record
            continue;
        }
    }
    
    $objStudendLog->setBehaviourOne($details['level1']);
    $objStudendLog->setBehaviourTwo($details['level2']);
    $objStudendLog->setRemarks(trim(htmlentities($details['remarks'], ENT_QUOTES, 'UTF-8')));
    $objStudendLog->setRecordTime($r_date . ' ' . $details['time_hour'] . ':' . $details['time_min']);
    $objStudendLog->setDuration('00:' . $details['timelasted_min'] . ':' . $details['timelasted_sec']);
    
    $tempList = array();
    for ($i = 0, $count = count($details['PIC']); $i < $count; ++ $i) {
        if (trim($details['PIC'][$i]) != '') {
            $tempList[] = ltrim($details['PIC'][$i], 'U');
        }
    }
    $details['PIC'] = $tempList;
    $objStudendLog->setPIC(implode(',', (array) $details['PIC']));
    $objStudendLog->setInputBy($medical_currentUserId);
    $objStudendLog->setModifyBy($medical_currentUserId);
    $objStudendLog->setUserID($sid);
    
    $objLogMapper = new StudentLogMapper();
    $saveReturn = $objLogMapper->save($objStudendLog);
    
    $_logRecordId = $is_new ? $saveReturn : $objStudendLog->getRecordID();
    unset($objStudendLog);
    unset($objLogMapper);
    $specialStatusIDList = $objMedical->getLev2IDByName($medical_cfg['studentLog']['level2']['specialStatus']);
    
    if ($_logRecordId > 0) {
        $lev3List = array();
        $lev4List = array();
        
        // save this $_logRecordId to the array $inputIdMapWithDBId for upload document
        $inputIdMapWithDBId[$key] = $_logRecordId;
        
        $objStudentLogPartsMapper = new StudentLogPartsMapper($_logRecordId);
        
        if (in_array($details['level2'], $specialStatusIDList) || $plugin['medical_module']['AlwaysShowBodyPart']) {
            foreach ((array) $details['level4'] as $Level3ID => $Level4DetailList) {
                
                foreach ((array) $Level4DetailList as $Level4ID) {
                    $objStudentLogParts = new StudentLogParts();
                    $objStudentLogParts->setLevel3ID($Level3ID);
                    $objStudentLogParts->setLevel4ID($Level4ID);
                    
                    $objStudentLogParts->setDateInput('now()');
                    $objStudentLogParts->setInputBy($medical_currentUserId);
                    $objStudentLogParts->setDateModified('now()');
                    $objStudentLogParts->setModifyBy($medical_currentUserId);
                    
                    $objStudentLogPartsMapper->addParts($objStudentLogParts);
                    
                    if (! isset($lev3List[$Level3ID])) {
                        $lev3Name = $objStudentLogParts->convertLev3ToName($Level3ID);
                        $lev3List[$Level3ID] = $lev3Name[0]['Lev3Name'];
                    }
                    if (! isset($lev4List[$Level3ID][$Level4ID])) {
                        $lev4Name = $objStudentLogParts->convertLev4ToName($Level4ID);
                        $lev4List[$Level3ID][$Level4ID] = $lev4Name[0]['Lev4Name'];
                    }
                }
            }
        }
        $result[] = $objStudentLogPartsMapper->save();
        
        // send email to pic
        if ($details['email2pic']) {
            $lwebmail = new libwebmail();
            $content = array();
            $content['EventDate'] = $date;
            $content['EventTime'] = $details['time_hour'] . ':' . $details['time_min'];
            
            $_lev1 = new studentLogLev1($details['level1']);
            $content['Lev1Name'] = $_lev1->getLev1Name();
            $_lev2 = new studentLogLev2($details['level2']);
            $content['Lev2Name'] = $_lev2->getLev2Name();
            $content['Lev3Name'] = $lev3List;
            $content['Lev4Name'] = $lev4List;
            $content['TimeLasted_min'] = $details['timelasted_min'];
            $content['TimeLasted_sec'] = $details['timelasted_sec'];
            $content['Remarks'] = $details['remarks'];
            $emailContent = $objMedical->getStudentLogEventContent($sid, $content);
            $emailResult = $lwebmail->sendModuleMail($details['PIC'], $Lang['medical']['studentLog']['email']['subject'], $emailContent, 1);
        }
        
        // Student Log - Event (Behaviour and Event) linking
        $oldStudentLogEventAry = $objMedical->getEventStudentLog($eventID = '', $_logRecordId);
        $oldStudentLogEventIDAry = BuildMultiKeyAssoc($oldStudentLogEventAry, 'StudentLogID', $IncludedDBField = array(
            'EventID'
        ), $SingleValue = 1);
        $eventIDToDeleteAry = array_diff((array) $oldStudentLogEventIDAry, (array) $details['event']);
        $eventIDToAddAry = array_diff((array) $details['event'], (array) $oldStudentLogEventIDAry);
        
        // delete EventID
        if (count($eventIDToDeleteAry)) {
            $result[] = $objMedical->deleteStudentLogEvent($eventIDToDeleteAry);
        }
        
        if (count($eventIDToAddAry)) {
            $dataAry = array();
            foreach ((array) $eventIDToAddAry as $_eventID) {
                unset($dataAry);
                $dataAry['StudentLogID'] = $_logRecordId;
                $dataAry['EventID'] = $_eventID;
                $result[] = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STUDENT_LOG', $dataAry, array(), true, true, false); // no DateModified field
            }
        }
    }
    
    // File to be deleted
    foreach ((array) $details['isfileUploadedPresent'] as $key => $preservedFile) {
        if (! $preservedFile) {
            $filesIDDeletedList[] = $key;
        }
    }
}

// return;

// ///////////////////////////////////////
// handle the file upload Data Structure//
// ///////////////////////////////////////
$uploadFileList = array();
foreach ($_FILES as $_file => $fileDetails) {
    foreach ($fileDetails['name'] as $_elementName => $_details) {
        foreach ($_details['fileUploaded'] as $detail) {
            $uploadFileList[$_elementName]['name'][] = $detail;
        }
    }
    $_details = null;
    
    foreach ($fileDetails['type'] as $_elementName => $_details) {
        foreach ($_details['fileUploaded'] as $detail) {
            $uploadFileList[$_elementName]['type'][] = $detail;
        }
    }
    $_details = null;
    
    foreach ($fileDetails['tmp_name'] as $_elementName => $_details) {
        foreach ($_details['fileUploaded'] as $detail) {
            $uploadFileList[$_elementName]['tmp_name'][] = $detail;
        }
    }
    $_details = null;
    
    foreach ($fileDetails['error'] as $_elementName => $_details) {
        foreach ($_details['fileUploaded'] as $detail) {
            $uploadFileList[$_elementName]['error'][] = $detail;
        }
    }
    $_details = null;
    
    foreach ($fileDetails['size'] as $_elementName => $_details) {
        foreach ($_details['fileUploaded'] as $detail) {
            $uploadFileList[$_elementName]['size'][] = $detail;
        }
    }
    $_details = null;
}

$uploadFile_root = $medical_cfg['uploadFile_root'];
if (! file_exists($uploadFile_root)) {
    mkdir($uploadFile_root, 0777);
}
$uploadResultAry = array(); // STORE THE UPLOAD DOCUMENT RESULT
foreach ((array) $uploadFileList as $_uploadInputName => $_details) {
    // must be '==='
    if (strpos($_uploadInputName, 'n' === 0)) {
        // start with "n" , it is a new record
    }
    // $uploadResultAry = NULL;
    for ($i = 0, $iMax = count($_details['name']); $i < $iMax; $i ++) {
        if (trim($_details['tmp_name'][$i]) == '') { // without upload item , skip to next one
            continue;
        }
        $objUploadFiles = new libUploadFiles($uploadFile_root);
        
        $objUploadFiles->setName($_details['name'][$i]);
        $objUploadFiles->setFileType($_details['type'][$i]);
        $objUploadFiles->setTmp_Name($_details['tmp_name'][$i]);
        $objUploadFiles->setError($_details['error'][$i]);
        $objUploadFiles->setSize($_details['size'][$i]);
        $uploadResultAry[$_uploadInputName][] = $objUploadFiles->upload();
    }
}
// SAVE TO DB
foreach ($uploadResultAry as $_uploadInputName => $_uploadResult) {
    $_uploadFailed = 0; // RECORD FOR EACH _uploadInputName
    
    if ($inputIdMapWithDBId[$_uploadInputName] == '') {
        $errMsg = 'Support a value must be find in array \$inputIdMapWithDBId but failed. Value :[' . $_uploadInputName . '] Array Value ' . print_r($rawArray, true) . '"' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
        alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
        
        continue;
    } else {
        for ($i = 0, $iMax = count($_uploadResult); $i < $iMax; $i ++) {
            if (! $_uploadResult[$i]['uploadSuccess']) {
                ++ $_uploadFailed;
                $errMsg = 'Error in upload document. ERROR value :[' . print_r($_uploadResult[$i], true) . ']' . " f:" . __FILE__ . " fun:" . __FUNCTION__ . " line : " . __LINE__ . " HTTP_REFERER :" . $_SERVER['HTTP_REFERER'];
                alert_error_log($projectName = $cfg_medical['module_code'], $errMsg, $errorType = '');
                
                // upload failed , skip to save to DB
                continue;
            }
            
            $objDocument = new StudentLogDocument();
            
            $objDocument->setStudentLogID($inputIdMapWithDBId[$_uploadInputName]);
            $objDocument->setRenameFile($_uploadResult[$i]['renameFile']);
            $objDocument->setOrgFileName($_uploadResult[$i]['name']);
            $objDocument->setFileType($_uploadResult[$i]['type']);
            $objDocument->setFileSize($_uploadResult[$i]['size']);
            $objDocument->setFolderPath($_uploadResult[$i]['destFolder']);
            $objDocument->setDateInput('now()');
            $objDocument->setInputBy($medical_currentUserId);
            $objDocument->save();
        }
    }
}

// Delete Records

if (! empty($filesIDDeletedList)) {
    $result[] = $objMedical->deleteStudentLogAttachment($filesIDDeletedList);
}
if (! empty($eventIDArray)) {
    if ((count($eventIDArray) == 1) && (empty($eventIDArray[0]))) {
        // do nothing
    } else {
        $result[] = $objMedical->deleteStudentLogRecords($eventIDArray);
    }
}
if (in_array(0, $result)) {
    $Msg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
} else {
    $Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}

// Change it to ajaxSave, so no need to redirect
// header('Location: ?t=management.studentLog&Msg='.$Msg.'&sleep='.$sleep.'&classID='.$classID.'&date='.$date.'&gender='.$gender);
?>