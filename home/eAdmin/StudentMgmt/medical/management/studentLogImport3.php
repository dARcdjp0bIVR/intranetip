<?php
// modify :
/*
 * 	Date: 	2016-05-10 [Cameron] replace split with explode to support php 5.4
 * 
 * 	Date:	2015-09-18 [Cameron] 
 * 			- fix bug on showing error message if import file header format is wrong
 * 			- fix bug on not process adding parts if it's empty
 * 
 * 	Date:	2015-01-27 [Cameron] Add $plugin['medical_module']['AlwaysShowBodyPart'], import MEDICAL_STUDENT_LOG_PARTS if this is true
 * 				no matter Lev2Name = $medical_cfg['studentLog']['level2']['specialStatus'] or not
 * 	
 */
//include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/management/studentLogImportCommon.php");
include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentLogImport.php"); 
include_once($PATH_WRT_ROOT."includes/libuser.php");


$result = array();
$Msg = '';

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT') || !$plugin['medical_module']['studentLog']){
	header("Location: /");
	exit();
}

//print "path=".$TargetFilePath . "<br>";

############
## following should be processed in studentLogImport2.php, for temp only
//$name = $_FILES['csvfile']['name'];
//$libimport = new libimporttext();
//$libfs = new libfilesystem();
//if (!$libimport->CHECK_FILE_EXT($name))
//{
//	intranet_closedb();
//	exit();
//}
//$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('reportcard/report_award/', $csvfile, $name);
## end temp
############


//$TargetFilePath = $_POST["TargetFilePath"];
//print "file=$TargetFilePath<br>";

$studentLogImport = new libStudentLogImport();
$data = $studentLogImport->ReadStudentLogImportData($TargetFilePath);

//debug_r($data);
$nrSuccess = 0;	// number of successfully import record
$nrRec = 0;		// total number of records to import

//print "user before <br>";

//print "user after <br>";
if ($data != false)
{
	if ($data["HeaderError"] == true)
	{
//print "Wrong Header<br>";		
		$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess_IncorrectHeaderFormat'];
	}	
	else if ($data["NumOfData"] == 0)
	{
		$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess_NoRecord'];
//print "No record to import<br>";		
	}
	else	// Import Data exists
	{
//print"in data<br>";		
		$rs = $data["Data"];
		$nrRec = count($rs);
		
		$rsReleationLev3_4 = $objMedical->getAllActiveLev4CodeLev3ID();
		foreach($rsReleationLev3_4 as $r)
		{
			$assocLev3_4[$r['Lev4Code']] = $r['Lev3ID']; // Put the releation to a associative-array ( Lev4Code => Lev3ID ).
		}
		
		for ($i = 0; $i < $nrRec; $i++)
		{			
			if ($rs[$i]["pass"])
			{
				$crs = $rs[$i]["rawData"];
//debug_r($crs);				
				$className 	= $crs["ClassName"];
				$classNum	= $crs["ClassNum"];
				$date 		= $crs["Date"];
				$time		= $crs["Time"];
				$behaviour1	= $crs["Behaviour1"];
				$behaviour2	= $crs["Behaviour2"];
				$parts		= $crs["Parts"];
				$duration	= $crs["Duration"];
				$remarks	= $crs["Remarks"];
				$pic		= $crs["Pic"];
				$d = explode(":",$duration);
				if (count($d) == 2)
				{
					$duration = "00:". substr("0".$d[0],-2) . ":" . substr("0".$d[1],-2);
				}
				else
				{
					$duration = "00:00:00";
				}
				
				$userID = $objMedical->getUserIDByClassNameAndNumber($className,$classNum);
				$lev1ID = $objMedical->getLev1IDByCode($behaviour1);
				$lev2ID = $objMedical->getLev2IDByCode($behaviour2,$lev1ID);
//debug_r($lev2ID);				
				$objStudentLogPartsMapper = null;
				$objStudendLog = new StudentLog();
				$objStudendLog->setBehaviourOne($lev1ID);
				$objStudendLog->setBehaviourTwo($lev2ID);
				$objStudendLog->setRemarks(trim( htmlentities($remarks, ENT_QUOTES, 'UTF-8')));
				$objStudendLog->setRecordTime($date.' '.$time);					
				$objStudendLog->setDuration($duration);
				$pic_UserIDs = $objMedical->getUserIDByUserLogin($pic);
//echo '$pic_UserIDs<br>';
//debug_r($pic_UserIDs);
				if($pic_UserIDs) // For empty pic
				{
					$pic_UserID_Array = implode(",",$pic_UserIDs);
				}
				else
				{
					$pic_UserID_Array = '';
				}
//print "pic a = $a <br>";
				$objStudendLog->setPIC($pic_UserID_Array);
//debug_r("'" . implode(",",$pic_UserIDs) . "'");				
				$objStudendLog->setInputBy($medical_currentUserId);
				$objStudendLog->setModifyBy($medical_currentUserId);
				$objStudendLog->setUserID($userID);
		
				$objLogMapper = new StudentLogMapper();
				$studentLogResult = $objLogMapper->save($objStudendLog);
				$result[] = $studentLogResult;
//debug_r($objStudendLog);

				if ($studentLogResult)
				{
					$nrSuccess++;
				}
				$_logRecordId = $objStudendLog->getRecordID();
				
				$specialStatusIDList = $objMedical->getLev2IDByName($medical_cfg['studentLog']['level2']['specialStatus'] );

				
				if(($_logRecordId > 0 ) && (trim($parts) != ''))
				{
					$objStudentLogPartsMapper = new StudentLogPartsMapper($_logRecordId);

					$partsArr = explode(',', $parts);
					
					foreach($partsArr as $part4Code){
						$lev4ID = $objMedical->getLev4IDByCode($part4Code);
						$lev3ID = $assocLev3_4[$part4Code];
						
						if(in_array($lev2ID, $specialStatusIDList) || $plugin['medical_module']['AlwaysShowBodyPart']){	
							$objStudentLogParts = new StudentLogParts();
							$objStudentLogParts->setLevel3ID($lev3ID);
							$objStudentLogParts->setLevel4ID($lev4ID);
				
							$objStudentLogParts->setDateInput('now()');
							$objStudentLogParts->setInputBy($medical_currentUserId);
							$objStudentLogParts->setDateModified('now()');
							$objStudentLogParts->setModifyBy($medical_currentUserId);
				
							$objStudentLogPartsMapper->addParts($objStudentLogParts);
						}
					}
//debug_r($objStudentLogParts);
					$result[] = $objStudentLogPartsMapper->save();
				}
				
			}	// pass
		} 
	}
}
else
{
	$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
//print "False csv data<br>";	
}

//print "num=$nrSuccess and $nrRec";
//debug_r($result);
if (($Msg =='') && ($nrSuccess > 0) && ($nrRec > 0))
{
	if(!in_array(0, $result)){
		if ($nrSuccess == $nrRec)
		{		
			$Msg = $Lang['General']['ReturnMessage']['ImportSuccess'];
		}		
		else if ($nrSuccess < $nrRec)
		{
			$Msg = $Lang['medical']['ReturnMessage']['ImportPartiallySuccess'];
		}
		else
		{
			$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
		}
	}
	else
	{
		$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
	}
}
else
{
	$Msg = $Msg ? $Msg : $Lang['General']['ReturnMessage']['ImportUnsuccess'];
}



$CurrentPage = "ManagementStudentLog";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentLog'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=3, $CustStepArr='');

$Title = $Lang['medical']['general']['ImportData'];

$PAGE_NAVIGATION[] = array($Title,"");

$linterface->LAYOUT_START($Msg);

//debug_r($_FILES);
$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t=management.studentLog'","back_btn"," class='formbutton' ");

?>
<form method="POST" name="frm1" id="frm1" action="?t=management.studentLog">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
		</tr>
		<tr>
			<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
			<td><?=$h_stepUI;?></td>
		</tr>
	</table>
			
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr height="20"><td>&nbsp;</td></tr>				
		<tr>
			<td align='center'>
		<?= $nrSuccess ."&nbsp;". $Lang['General']['ImportArr']['RecordsImportedSuccessfully']?>
			</td>
		</tr>
		<tr height="20"><td>&nbsp;</td></tr>
		<tr>
		    <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>

		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		            <tr>
						<td align="center">
						<br/>
						<input type="hidden" name="t" value="management.studentLog"/>					
							<?= $BackBtn ?>				
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>

<?php

$linterface->LAYOUT_STOP();
?>