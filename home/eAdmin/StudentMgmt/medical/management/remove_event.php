<?php
// using:
/*
 * 2018-12-27 Cameron
 * - fix sql in checking self record
 * 
 * 2018-03-29 Cameron
 * - also delete relevant records in MEDICAL_CASE_EVENT and MEDICAL_EVENT_STUDENT_LOG
 * 
 * 2018-02-27 Cameron
 * - allow to delete self record only for non-super-admin user [case #F135176]
 *
 * 2018-02-01 Cameron
 * - Log delete record for trace purpose
 *
 * 2017-06-22 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$dataAry = array();
$isRequiredCheckSelfEvent = $objMedical->isRequiredCheckSelfEvent();
$returnMsgKey = '';

if (count($EventID) > 0) {
    $isAllowedToDelete = true;
    if ($isRequiredCheckSelfEvent) {
        $sql = "SELECT EventID FROM MEDICAL_EVENT WHERE EventID IN ('" . implode("','", $EventID) . "') AND InputBy<>'" . $_SESSION['UserID'] . "'";
        $checkResult = $ldb->returnResultSet($sql);
        if (count($checkResult)) {
            $isAllowedToDelete = false;
            $returnMsgKey = 'DeleteNonSelfRecord';
        }
    }
    
    if ($isAllowedToDelete) {
        $ldb->Start_Trans();
        
        // log delete record
        foreach ((array) $EventID as $eventID) {
            $sql = "SELECT StudentID FROM MEDICAL_EVENT_STUDENT WHERE EventID='{$eventID}'";
            $rs = $ldb->returnResultSet($sql);
            if (count($rs)) {
                $studentIDAry = BuildMultiKeyAssoc($rs, "StudentID", array(
                    "StudentID"
                ), $SingleValue = 1);
                $studentIDs = implode(',', $studentIDAry);
            } else {
                $studentIDs = '';
            }
            
            $sql = "SELECT AffectedPersonID FROM MEDICAL_EVENT_AFFECTED_PERSON WHERE EventID='{$eventID}'";
            $rs = $ldb->returnResultSet($sql);
            if (count($rs)) {
                $affectedPersonIDAry = BuildMultiKeyAssoc($rs, "AffectedPersonID", array(
                    "AffectedPersonID"
                ), $SingleValue = 1);
                $affectedPersonIDs = implode(',', $affectedPersonIDAry);
            } else {
                $affectedPersonIDs = '';
            }
            $event = $objMedical->getEvents($eventID);
            if (count($event)) {
                $event = current($event);
                
                unset($dataAry);
                $dataAry['TableName'] = 'MEDICAL_EVENT';
                $dataAry['RecordID'] = $eventID;
                $dataAry['StartDate'] = $event['EventDate'];
                $dataAry['RefID'] = $event['EventTypeID'];
                $dataAry['Summary'] = $event['Summary'];
                $dataAry['Detail'] = $event['Cause'];
                $dataAry['StudentID'] = $studentIDs;
                $dataAry['OtherRef'] = $affectedPersonIDs;
                $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
                $result[] = $ldb->db_db_query($sql);
                
                $eventFollowupAry = $objMedical->getEventFollowup($eventID);
                if (count($eventFollowupAry)) {
                    foreach ((array) $eventFollowupAry as $eventFollowup) {
                        unset($dataAry);
                        $dataAry['TableName'] = 'MEDICAL_EVENT_FOLLOWUP';
                        $dataAry['RecordID'] = $eventFollowup['FollowupID'];
                        $dataAry['RefID'] = $eventFollowup['EventID'];
                        $dataAry['Summary'] = $eventFollowup['Handling'];
                        $dataAry['Detail'] = $eventFollowup['StudentResponse'];
                        $dataAry['OtherRef'] = $eventFollowup['FollowupAction'];
                        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                        $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
                        $result[] = $ldb->db_db_query($sql);
                    }
                }
            }
        }
        
        // delete event
        $sql = "DELETE FROM MEDICAL_EVENT WHERE EventID IN ('" . implode("','", $EventID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_EVENT_STUDENT WHERE EventID IN ('" . implode("','", $EventID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_EVENT_AFFECTED_PERSON WHERE EventID IN ('" . implode("','", $EventID) . "')";
        $result[] = $ldb->db_db_query($sql);

        $sql = "DELETE FROM MEDICAL_CASE_EVENT WHERE EventID IN ('" . implode("','", $EventID) . "')";
        $result[] = $ldb->db_db_query($sql);

        $sql = "DELETE FROM MEDICAL_EVENT_STUDENT_LOG WHERE EventID IN ('" . implode("','", $EventID) . "')";
        $result[] = $ldb->db_db_query($sql);
        
        $attachment = $objMedical->getEventAttachment($EventID);
        if (count($attachment)) {
            foreach ((array) $attachment as $rs) {
                $FileID = $rs['FileID'];
                $FolderPath = $rs['FolderPath'];
                $FileHashName = $rs['FileHashName'];
                $file = $file_path . "/file/medical/edisc/" . $FolderPath . "/" . $FileHashName;
                $lfs = new libfilesystem();
                $result[] = $lfs->file_remove($file);
                $result[] = $objMedical->deleteAttachmentByID('MEDICAL_EVENT_ATTACHMENT', $FileID);
            }
        }
        unset($attachment);
        
        $eventFollowup = $objMedical->getEventFollowup($EventID);
        
        if (count($eventFollowup)) {
            foreach ((array) $eventFollowup as $ef) {
                $followupID = $ef['FollowupID'];
                $sql = "DELETE FROM MEDICAL_EVENT_FOLLOWUP WHERE FollowupID='" . $followupID . "'";
                $result[] = $ldb->db_db_query($sql);
                
                $attachment = $objMedical->getEventFollowupAttachment($followupID);
                if (count($attachment)) {
                    foreach ((array) $attachment as $rs) {
                        $FileID = $rs['FileID'];
                        $FolderPath = $rs['FolderPath'];
                        $FileHashName = $rs['FileHashName'];
                        $file = $file_path . "/file/medical/edisc/" . $FolderPath . "/" . $FileHashName;
                        $lfs = new libfilesystem();
                        $result[] = $lfs->file_remove($file);
                        $result[] = $objMedical->deleteAttachmentByID('MEDICAL_EVENT_FOLLOWUP_ATTACHMENT', $FileID);
                    }
                }
            }
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $returnMsgKey = 'DeleteSuccess';
        } else {
            $ldb->RollBack_Trans();
            $returnMsgKey = 'DeleteUnsuccess';
        }
    }
}

header("location: ?t=management.event_list&returnMsgKey=" . $returnMsgKey);

?>