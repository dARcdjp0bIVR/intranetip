<?php
// using:
/*
 * 2018-12-20 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_ADD') || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

$dataAry['StaffID'] = $StaffID;
$dataAry['EventDate'] = $StaffEventDate;
$dataAry['StartTime'] = $StartTimeHour . ":" . $StartTimeMin;
$dataAry['EndTime'] = $EndTimeHour . ":" . $EndTimeMin;
$dataAry['EventTypeID'] = $StaffEventTypeID;
$dataAry['EventTypeLev2ID'] = $StaffEventTypeLev2ID;
$dataAry['LocationBuildingID'] = $StaffEventLocationBuildingID;
$dataAry['LocationLevelID'] = $StaffEventLocationLevelID;
$dataAry['LocationID'] = $StaffEventLocationID;
$dataAry['IsReportedInjury'] = $IsReportedInjury;
$dataAry['InjuryLeave'] = $InjuryLeave;
$dataAry['SickLeave'] = $SickLeave;
$dataAry['Remark'] = standardizeFormPostValue($Remark);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['DateInput'] = 'now()';
$sql = $objMedical->INSERT2TABLE('MEDICAL_STAFF_EVENT', $dataAry, array(), false);

$ldb->Start_Trans();
// # step 1: add to staff event
$res = $ldb->db_db_query($sql);
$result['AddStaffEvent'] = $res;
if ($res) {
    $StaffEventID = $ldb->db_insert_id();
    // # step 2: add student event related to staff event
    foreach ((array) $SelEventID as $eventID) {
        unset($dataAry);
        $dataAry['EventID'] = $eventID;
        $dataAry['StaffEventID'] = $StaffEventID;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STAFF_EVENT', $dataAry, array(), false, false, false); // no DateModified field
        $result['AddStaffEventRelEvent_'.$eventID] = $ldb->db_db_query($sql);
    }
    
    // # step 3: add attachments
    $uploadResult = $objMedical->handleUploadFile($TargetFolder, $StaffEventID, $table = 'staff_event');
    if ($uploadResult != 'AddSuccess') {
        $result['HandleUploadFile'] = false;
    }
    
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
} else {
    $ldb->RollBack_Trans();
    if ($uploadResult == 'FileSizeExceedLimit') {
        $returnMsgKey = 'FileSizeExceedLimit';
    } else {
        $returnMsgKey = 'AddUnsuccess';
    }
}

header("location: ?t=management.staffEventList&returnMsgKey=" . $returnMsgKey);

?>