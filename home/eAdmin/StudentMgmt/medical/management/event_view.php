<?php
// using:
/*
 * 2019-01-04 Cameron
 * - set $affectedPersonInfo to '-' if it's empty
 * - add $relevantStaffEvent
 * 
 * 2018-03-29 Cameron
 * - show relevant student log in list view
 * 
 * 2018-03-26 Cameron
 * - show relevant cases in list view
 *
 * 2018-03-20 Cameron
 * - show AffectedNonAccountHolder in AffectedPersonInfo
 *
 * 2017-06-26 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("location: /");
    exit();
}

if (is_array($EventID)) {
    $eventID = (count($EventID) == 1) ? $EventID[0] : '';
} else {
    $eventID = $EventID;
}

if (! $eventID) {
    header("Location: ?t=management.event_list");
}

$rs_event = $objMedical->getEvents($eventID);
if (count($rs_event) == 1) {
    $rs_event = current($rs_event);
} else {
    header("Location: ?t=management.event_list");
}

$CurrentPage = "ManagementEvent";
$CurrentPageName = $Lang['medical']['menu']['event'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['event'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['event'],
    $ViewMode ? "" : "?t=management.event_list"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['View'],
    ""
);

$eventTypeAry = $objMedical->getEventType($rs_event['EventTypeID']);
$eventTypeName = count($eventTypeAry) == 1 ? $eventTypeAry[0]['EventTypeName'] : '';

$buildingAry = $objMedical->getInventoryBuildingArray($rs_event['LocationBuildingID']);
$buildingName = count($buildingAry) == 1 ? $buildingAry[0]['BuildingName'] : '';

$levelAry = $objMedical->getInventoryLevelArray($rs_event['LocationBuildingID'], $rs_event['LocationLevelID']);
$buildingLevelName = count($levelAry) == 1 ? $levelAry[0]['LevelName'] : '';

$locationAry = $objMedical->getInventoryLocationArray($rs_event['LocationLevelID'], $rs_event['LocationID']);
$locationName = count($locationAry) == 1 ? $locationAry[0]['LocationName'] : '';

if ($buildingName && $buildingLevelName && $locationName) {
    $locationInfo = $buildingName . ' > ' . $buildingLevelName . ' > ' . $locationName;
} else if ($buildingName && $buildingLevelName && $locationName == '') {
    $locationInfo = $buildingName . ' > ' . $buildingLevelName;
} else if ($buildingName && $buildingLevelName == '' && $locationName) {
    $locationInfo = $buildingName;
} else if ($buildingName && $buildingLevelName == '' && $locationName == '') {
    $locationInfo = $buildingName;
} else {
    $locationInfo = '-';
}

$rs_event['Student'] = $objMedical->getEventStudent($eventID);
$studentInfo = $objMedical->getUserNameList($rs_event['Student']);

$rs_event['AffectedPerson'] = $objMedical->getEventAffectedPerson($eventID);
$affectedPersonInfo = $objMedical->getUserNameList($rs_event['AffectedPerson']);
$affectedNonAccountHolder = str_replace(array(
    ',',
    ';'
), '<br>', $rs_event['AffectedNonAccountHolder']);
if (! empty($affectedPersonInfo) && ! empty($affectedNonAccountHolder)) {
    $affectedPersonInfo .= '<br>' . $affectedNonAccountHolder;
} elseif (empty($affectedPersonInfo) && ! empty($affectedNonAccountHolder)) {
    $affectedPersonInfo = $affectedNonAccountHolder;
}
if ($affectedPersonInfo == '') {
    $affectedPersonInfo = '-';
}
$rs_event['Attachment'] = $objMedical->getEventAttachment($eventID);

$relevantCases = $objMedical->getEventCaseList($eventID, $viewMode=true);
$relevantStudentLogs = $objMedical->getEventStudentLogList($eventID, $viewMode=true);

$staffEventViewRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_VIEW');
if ($staffEventViewRight) {
    $relevantStaffEvent = $objMedical->getStaffEventListOfStudentEvent($eventID, $viewMode=true);
}
else {
    $relevantStaffEvent = '';
}

// if ($rs_event['CaseID']) {

// $caseRef = $Lang['medical']['case']['tableHeader']['CaseNo'] . ': ' . $objMedical->getCaseRef4Event($rs_event['CaseID'], $ViewMode);
// } else {
// $caseRef = '';
// }

$followup_list = $objMedical->getEventFollowupList($eventID, $ViewMode);

if (! $ViewMode) {
    $linterface->LAYOUT_START($returnMsg);
    include_once ('templates/event_view.tmpl.php');
    $linterface->LAYOUT_STOP();
} else {
    include_once ('templates/event_view.tmpl.php');
}
?>