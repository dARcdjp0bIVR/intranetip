<?php
// using:
/*
 * 2018-12-20 Cameron
 * - create this file
 */

$viewRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_VIEW');
$editRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_EDIT');
$staffEventID = $_GET['StaffEventID'];
$isStaffEventStaff = $objMedical->isStaffEventStaff($_SESSION['UserID'], $staffEventID);

if ((!$viewRight && !$isStaffEventStaff) || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}

if (is_array($StaffEventID)) {
    $StaffEventID = (count($StaffEventID) == 1) ? $StaffEventID[0] : '';
} else {
    $StaffEventID = $StaffEventID;
}

if (! $StaffEventID) {
    header("Location: ?t=management.staffEventList");
}

$staffEventAry = $objMedical->getStaffEvents($StaffEventID);
if (count($staffEventAry) == 1) {
    $staffEventAry = current($staffEventAry);
} else {
    header("Location: ?t=management.staffEventList");
}

$CurrentPage = "ManagementStaffEvent";
$CurrentPageName = $Lang['medical']['menu']['staffEvent'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['staffEvent'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array(
    $Lang['medical']['menu']['staffEvent'],
    $ViewMode ? "" : "?t=management.staffEventList"
);
$PAGE_NAVIGATION[] = array(
    $Lang['Btn']['View'],
    ""
);

$staffEventTypeAry = $objMedical->getStaffEventType($staffEventAry['EventTypeID']);
$eventTypeName = count($staffEventTypeAry) == 1 ? $staffEventTypeAry[0]['EventTypeName'] : '';

$staffEventTypeLev2Ary = $objMedical->getStaffEventTypeLev2($staffEventAry['EventTypeLev2ID']);
$eventTypeLev2Name = count($staffEventTypeLev2Ary) == 1 ? $staffEventTypeLev2Ary[0]['EventTypeLev2Name'] : '';

if ($eventTypeName && $eventTypeLev2Name) {
    $eventTypeInfo = $eventTypeName . ' > ' . $eventTypeLev2Name;
}
else if ($eventTypeName && $eventTypeLev2Name == '') {
    $eventTypeInfo = $eventTypeName;
}
else {
    $eventTypeInfo = '-';
}

$buildingAry = $objMedical->getInventoryBuildingArray($staffEventAry['LocationBuildingID']);
$buildingName = count($buildingAry) == 1 ? $buildingAry[0]['BuildingName'] : '';

$levelAry = $objMedical->getInventoryLevelArray($staffEventAry['LocationBuildingID'], $staffEventAry['LocationLevelID']);
$buildingLevelName = count($levelAry) == 1 ? $levelAry[0]['LevelName'] : '';

$locationAry = $objMedical->getInventoryLocationArray($staffEventAry['LocationLevelID'], $staffEventAry['LocationID']);
$locationName = count($locationAry) == 1 ? $locationAry[0]['LocationName'] : '';

if ($buildingName && $buildingLevelName && $locationName) {
    $locationInfo = $buildingName . ' > ' . $buildingLevelName . ' > ' . $locationName;
} else if ($buildingName && $buildingLevelName && $locationName == '') {
    $locationInfo = $buildingName . ' > ' . $buildingLevelName;
} else if ($buildingName && $buildingLevelName == '' && $locationName) {
    $locationInfo = $buildingName;
} else if ($buildingName && $buildingLevelName == '' && $locationName == '') {
    $locationInfo = $buildingName;
} else {
    $locationInfo = '-';
}

$staffEventAry['Attachment'] = $objMedical->getStaffEventAttachment($StaffEventID);

if (!$viewRight && $isStaffEventStaff) {
    $linkEventDetails = false;              // avoid unauthorized view the other staff event link in student event
}
else {
    $linkEventDetails = true;
}
$relevantStudentEvents = $objMedical->getStudentEventListOfStaffEvent($StaffEventID, $viewMode=true, $linkEventDetails);

if (! $ViewMode) {
    $linterface->LAYOUT_START($returnMsg);
    include_once ('templates/staff_event_view.tmpl.php');
    $linterface->LAYOUT_STOP();
} else {
    include_once ('templates/staff_event_view.tmpl.php');
}
?>