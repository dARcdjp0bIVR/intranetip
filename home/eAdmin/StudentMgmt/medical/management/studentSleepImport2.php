<?php
// modify : 
/*
 * 	Log
 * 
 * 	Date:	2015-09-18 [Cameron]
 * 			- fix bug on showing error message and exit if file header format is wrong
 */
include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentSleepImport.php");
//include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/management/studentLogImportCommon.php");
//reference to /home/web/eclass40/intranetIP25/home/eAdmin/StudentMgmt/eReportCard/reports/generate_reports/generate_award/import_award_step2.php
//include_once($PATH_WRT_ROOT."includes/libimporttext.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libimport.php");
//include_once($PATH_WRT_ROOT."includes/libftp.php");

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_MANAGEMENT')){
	header("Location: /");
	exit();
}

$libimport = new libimporttext();
//$lo = new libfilesystem();
//$libfs = new libimport();
$libfs = new libfilesystem();

$name = $_FILES['csvfile']['name'];


/*
$ext = strtoupper($libfs->file_ext($name));
if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
//	header("location: import.php?xmsg=import_failed&TabID=$TabID");
	exit();
}
*/
if (!$libimport->CHECK_FILE_EXT($name))
{
	intranet_closedb();
//	header('location: '.$ReturnPath.'&ReturnMsgKey=WrongFileFormat');
	exit();
}

#######################################################
### move to temp folder first for others validation ###
#######################################################
//$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('reportcard/report_award/', $csvfile, $name);
$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('medical/student_sleep/', $csvfile, $name);
//debug_r($csvfile);

##################################
### Validate file header format ##
##################################
//$DefaultCsvHeaderArr = $lreportcard_award->Get_Report_Award_Csv_Header_Title();
//$DefaultCsvHeaderArr = $DefaultCsvHeaderArr['En'];
//$ColumnPropertyArr = $lreportcard_award->Get_Report_Award_Csv_Header_Property($forGetAllCsvContent=1);

//$CsvData = $libimport->GET_IMPORT_TXT_WITH_REFERENCE($TargetFilePath, "", "", $DefaultCsvHeaderArr, $ColumnPropertyArr);
//print "CsvData:<br>";
//debug_r($CsvData);
//$CsvHeaderArr = array_shift($CsvData);
//debug_r($CsvHeaderArr);
//$numOfCsvData = count($CsvData);

$studentLogImport = new libStudentSleepImport();
$data = $studentLogImport->ReadStudentSleepImportData($TargetFilePath);

if ($data['HeaderError']) {
	$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess_IncorrectHeaderFormat'];
	header("location:  index.php?t=management.studentSleepImport1&Msg=$Msg");
	exit();
}

$passCount = 0;
foreach($data['Data'] as $d)
{
	if($d['pass'])
	{
		$passCount++;
	}
}
$failCount = $data['NumOfData'] - $passCount;



$CurrentPage = "ManagementStudentSleep";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentSleep'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=2, $CustStepArr='');

$Title = $Lang['medical']['general']['ImportData'];

$PAGE_NAVIGATION[] = array($Title,"");

$linterface->LAYOUT_START($Msg);

?>
<form method="POST" name="frm1" id="frm1" action="index.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?=$h_stepUI;?></td>
</tr>
<tr>
	<td>	
		<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td class='formfieldtitle'><?=$Lang['Btn']['Import']?>:</td>
			<td class='tabletext'><?=$Lang['medical']['general']['StudentSleepData']?></td>
		</tr>
		<tr>
			<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?>:</td>
			<td class='tabletext'><?=$passCount?></td>
		</tr>
		<tr>
			<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?>:</td>
			<td class='tabletext <?php if($failCount>0) print('red');?>'><?=$failCount?></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%"><?=$Lang['medical']['meal']['tableHeader']['Number']?>&nbsp;</th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['report_general']['searchMenu']['form']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentLog']['importRemarks']['classNum']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentLog']['importTableHeader']['date']?></th>
				<!--td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentSleep']['importRemarks']['time']?>&nbsp;</th-->
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentSleep']['importRemarks']['SleepCondition']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentSleep']['importRemarks']['reason']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentSleep']['importRemarks']['frequency']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentSleep']['importRemarks']['remarks']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentLog']['import']['dataChecking']?></th>
				<?php if($failCount>0) {?>
					<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentLog']['import']['reason']?></th>
				<?php }?>
			</tr>
			
			<?php for($i = 0, $iMax = $data['NumOfData']; $i < $iMax; $i++) {?>
			<tr class='tablebluerow<?=(($i+1)%2+1)?>'>
				<td class='tabletext' valign='top'><?=($i+1)?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['ClassName']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['ClassNum']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Date']?></td>
				<!--td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Time']?></td-->
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Status']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Reason']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Frequency']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Remarks']?></td>
				<td class='tabletext' valign='top'><?=($data['Data'][$i]['pass'])?$linterface->Get_Tick_Image():'<span class="table_row_tool"><a class="delete"/></span>'?></td>
				<?php if($failCount>0) {?>
					<td class='tabletext' valign='top'><?=$data['Data'][$i]["rawData"]["Error"]?></td>
				<?php }?>
			</tr>
			<?php } //end foreach?>
		</table>
	</td>
</tr>
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
		<input type="hidden" name="t" value="management.studentSleepImport3"/>
		<input type="hidden" name="TargetFilePath" value="<?=$TargetFilePath?>"/>	
		<?=($passCount > 0) ? $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") :"" ?>
		&nbsp;
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t=management.studentSleepImport1'","back_btn"," class='formbutton' ") ?>
	</td>
</tr>

</table>
</form>

<?php
$linterface->LAYOUT_STOP();
?>