<?php
// using:
/*
 * 2019-01-04 Cameron
 * - add staff event related to student event
 * 
 * 2018-03-29 Cameron
 * - add event student log
 *
 * 2018-03-23 Cameron
 * - add event case
 * 
 * 2018-03-19 Cameron
 * - add AffectedNonAccountHolder to event
 *
 * 2018-02-27 Cameron
 * - add InputBy and DateInput
 *
 * 2017-06-20 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

$dataAry['EventDate'] = $EventDate;
$dataAry['StartTime'] = $StartTimeHour . ":" . $StartTimeMin;
$dataAry['EndTime'] = $EndTimeHour . ":" . $EndTimeMin;
$dataAry['EventTypeID'] = $EventTypeID;
$dataAry['LocationBuildingID'] = $LocationBuildingID;
$dataAry['LocationLevelID'] = $LocationLevelID;
$dataAry['LocationID'] = $LocationID;
$dataAry['Summary'] = standardizeFormPostValue($Summary);
$dataAry['Cause'] = standardizeFormPostValue($Cause);
$dataAry['AffectedNonAccountHolder'] = standardizeFormPostValue($AffectedNonAccountHolder);
// $dataAry['IsCase'] = $IsCase;
// $dataAry['CaseID'] = $CaseID;
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['DateInput'] = 'now()';
$sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT', $dataAry, array(), false);

$ldb->Start_Trans();
// # step 1: add to event
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
    $EventID = $ldb->db_insert_id();
    // # step 2: add event student
    foreach ((array) $StudentID as $id) {
        unset($dataAry);
        $dataAry['EventID'] = $EventID;
        $dataAry['StudentID'] = $id;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STUDENT', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
    }
    
    // # step 3: add event affected person
    foreach ((array) $AffectedPersonID as $id) {
        unset($dataAry);
        $dataAry['EventID'] = $EventID;
        $dataAry['AffectedPersonID'] = $id;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_AFFECTED_PERSON', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
    }
    
    // # step 4: add event case
    foreach ((array) $SelCaseID as $_caseID) {
        unset($dataAry);
        $dataAry['CaseID'] = $_caseID;
        $dataAry['EventID'] = $EventID;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_EVENT', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
    }
    
    // # step 5: add event student log
    foreach ((array) $selStudentLogID as $_studentLogID) {
        unset($dataAry);
        $dataAry['EventID'] = $EventID;
        $dataAry['StudentLogID'] = $_studentLogID;
        
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STUDENT_LOG', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
    }
    
    // # step 6: add attachments
    $uploadResult = $objMedical->handleUploadFile($TargetFolder, $EventID, $table = 'event');
    if ($uploadResult != 'AddSuccess') {
        $result[] = false;
    }
    
    // # step 7: add staff event related to student event
    foreach ((array) $SelStaffEventID as $staffEventID) {
        unset($dataAry);
        $dataAry['EventID'] = $EventID;
        $dataAry['StaffEventID'] = $staffEventID;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STAFF_EVENT', $dataAry, array(), false, false, false); // no DateModified field
        $result['AddStudentEventRelStaffEvent_'.$staffEventID] = $ldb->db_db_query($sql);
    }
    
//     // # step 6: sync EventID to case
//     if ($IsCase && $CaseID) {
//         $caseEvent = $objMedical->getCaseEvent($CaseID, $EventID);
//         if (count($caseEvent) == 0) { // not exist, need to add
//             unset($dataAry);
//             $dataAry['CaseID'] = $CaseID;
//             $dataAry['EventID'] = $EventID;
//             $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_EVENT', $dataAry, array(), false, false, false); // no DateModified field
//             $result[] = $ldb->db_db_query($sql);
//         }
//     }
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
} else {
    $ldb->RollBack_Trans();
    if ($uploadResult == 'FileSizeExceedLimit') {
        $returnMsgKey = 'FileSizeExceedLimit';
    } else {
        $returnMsgKey = 'AddUnsuccess';
    }
}

header("location: ?t=management.event_list&returnMsgKey=" . $returnMsgKey);

?>