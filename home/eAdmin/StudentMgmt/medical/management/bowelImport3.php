<?php
// modify : 

include_once($PATH_WRT_ROOT."includes/cust/medical/libBowelImport.php"); 
include_once($PATH_WRT_ROOT."includes/libuser.php");

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='BOWEL_MANAGEMENT') || !$plugin['medical_module']['bowel']){
	header("Location: /");
	exit();
}
$error = array();
$result = array();
$failResult = array();


$bowelImport = new libBowelImport();
$data = $bowelImport->ReadBowelImportData($TargetFilePath);
//debug_r($data);
$nrSuccess = 0;	// number of successfully import record
$nrRec = 0;		// total number of records to import
$noDuplicateRecord = true;

//debug_r($data);
//exit();
if ($data != false)
{
	if ($data["NumOfData"] == 0)
	{
		$error[] = "No record to import";
	}
	else	// Import Data exists
	{
		$rs = $data["Data"];
		$nrRec = count($rs);
		
		$objLibMedical = new libMedical();
		
		for ($i = 0; $i < $nrRec; $i++)
		{			
			if ($rs[$i]["pass"])
			{
				$crs 		= $rs[$i]["rawData"];
				$className 	= $crs["ClassName"];
				$classNum	= $crs["ClassNum"];
				$date 		= $crs["Date"];
				$time 		= $crs["Time"];
				$bowelID	= $crs["BowelID"];
				
				$userID = $crs["StudentID"];
				
				$dateTime = str_replace('/','-',$date . ' ' . $time . ':00');
				$objBowelLog = new BowelLog($dateTime);
				$objBowelLog->setUserID($userID);
				$objBowelLog->setRecordTime($dateTime);
				$objBowelLog->setBowelID(trim($bowelID));
				/*$objBowelLog->addRecord(
					$userID,
					$dateTime, 
					$bowelID,
					'', // Remarks
					$medical_currentUserId // input by
				);*/
				$saveResult = $objBowelLog->save();
				if($saveResult){
					$nrSuccess++;
				}else{
					$failResult[] = $i;
					$noDuplicateRecord = false;
				}
				$result[] = $saveResult;
			}	// pass
		}
	}
}
else
{
	$error[] = "False dat data";
//print "False csv data<br>";	
}

//print "num=$nrSuccess and $nrRec";
//debug_r($noDuplicateRecord);

if (($nrSuccess > 0) && ($nrRec > 0))
{
//	if(!in_array(0, $result)){
		if ($nrSuccess == $nrRec && $noDuplicateRecord)
		{		
			$Msg = $Lang['General']['ReturnMessage']['ImportSuccess'];
		}		
		else if ($nrSuccess <= $nrRec)
		{
			$Msg = $Lang['medical']['ReturnMessage']['ImportPartiallySuccess'];
		}
		else
		{
			$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
		}
//	}
//	else
//	{
//		$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];		
//	}
}
else
{
	$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess'];
}



$CurrentPage = "ManagementBowel";
$TAGS_OBJ[] = array($Lang['medical']['menu']['bowel'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=3, $CustStepArr=array_values($Lang['medical']['general']['ImportDATArr']['ImportStepArr']));


$Title = $Lang['medical']['general']['ImportData'];

$PAGE_NAVIGATION[] = array($Title,"");

$linterface->LAYOUT_START($Msg);

//debug_r($_FILES);
$BackBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t=management.bowel'","back_btn"," class='formbutton' ");
?>
<form method="POST" name="frm1" id="frm1" action="?t=management.bowel">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
		</tr>
		<tr>
			<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
		<tr>
			<td><?=$h_stepUI;?></td>
		</tr>
	</table>
			
	<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr height="20"><td>&nbsp;</td></tr>				
		<tr>
			<td align='center'>
		<?= $nrSuccess ."&nbsp;". $Lang['General']['ImportArr']['RecordsImportedSuccessfully']?>
			</td>
		</tr>
		<?php if(count($failResult) > 0) { ?>
			<tr height="20"><td>&nbsp;</td></tr>
			<tr><td align='center'>&nbsp;<?=$Lang['medical']['bowel']['import']['duplicateRecordBelow']?></td></tr>
			<tr>
				<td align='center'>
					<table width="80%" border="0" cellspacing="0" cellpadding="2" align="center">
					<tr>
						<td class='tablebluetop tabletopnolink' width="1%"><?=$Lang['medical']['meal']['tableHeader']['Number']?>&nbsp;</th>
						<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['bowel']['importRemarksTable']['studentID']?></th>
						<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['bowel']['importRemarksTable']['studentName']?></th>
						<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['bowel']['importRemarksTable']['class']?></th>
						<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['bowel']['importRemarksTable']['classNo']?></th>
						<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['bowel']['importRemarksTable']['bowelName']?></th>
						<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['bowel']['importRemarksTable']['bowelDateTime']?></th>
					</tr>
					<?php for($i = 0, $iMax = count($failResult); $i < $iMax; $i++) {?>
					<tr class='tablebluerow<?=(($i+1)%2+1)?>'>
						<td class='tabletext' valign='top'><?=($i+1)?></td>
						<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['StudentBarCode']?></td>
						<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['StudentName']?></td>
						<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['Class']?></td>
						<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['ClassNumber']?></td>
						<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['RecordDataName']?></td>
						<td class='tabletext' valign='top'><?=$data['Data'][$i]["rawData"]["DateTime"]?></td> 
					</tr>
					<?php } //end foreach?>
					</table>
				</td>
			</tr>
		<?php }?>
		<tr height="20"><td>&nbsp;</td></tr>
		<tr>
		    <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>

		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
		            <tr>
						<td align="center">
						<br/>
						<input type="hidden" name="t" value="management.bowel"/>					
							<?= $BackBtn ?>				
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>

<?php
$linterface->LAYOUT_STOP();
?>