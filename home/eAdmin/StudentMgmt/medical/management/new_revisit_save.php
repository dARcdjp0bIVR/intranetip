<?php
// using:
/*
 * 2019-06-05 Philips
 * - add Diagnosis, VisitDate, AccidentInjure,  DischargedAdmittion
 * 
 * 2018-02-27 Cameron
 * - add InputBy and DateInput
 *
 * 2017-08-31 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_MANAGEMENT') || ! $plugin['medical_module']['revisit']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

$dataAry['StudentID'] = $StudentID;
$dataAry['RevisitDate'] = $RevisitDate;
// $dataAry['IsChangedDrug'] = $IsChangedDrug;
$dataAry['CuringStatus'] = standardizeFormPostValue($CuringStatus);
$dataAry['Hospital'] = standardizeFormPostValue($Hospital);
$dataAry['Division'] = standardizeFormPostValue($Division);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['DateInput'] = 'now()';
// new customized columns
if($sys_custom['medical']['MedicalReport']){
    $dataAry['Diagnosis'] = standardizeFormPostValue($Diagnosis);
    $dataAry['VisitDate'] = $VisitDate;
    $dataAry['AccidentInjure'] = $AccidentInjure;
    $dataAry['DischargedAdmittion'] = $DischargedAdmittion;
}

$sql = $objMedical->INSERT2TABLE('MEDICAL_REVISIT', $dataAry, array(), false);

$ldb->Start_Trans();
// # step 1: add to revisit
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
    $RevisitID = $ldb->db_insert_id();
    
    // # step 2: add drugs
    if ($IsChangedDrug) {
        for ($i = 0, $iMax = count($drug_name); $i < $iMax; $i ++) {
            unset($dataAry);
            $dataAry['RevisitID'] = $RevisitID;
            $dataAry['Drug'] = standardizeFormPostValue($drug_name[$i]);
            ;
            $dataAry['Dosage'] = standardizeFormPostValue($dosage[$i]);
            ;
            $dataAry['Unit'] = $drug_unit[$i];
            $sql = $objMedical->INSERT2TABLE('MEDICAL_REVISIT_DRUG', $dataAry, array(), false, false, false); // no DateModified field
            $result[] = $ldb->db_db_query($sql);
        }
    }
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

header("location: ?t=management.revisit_list&returnMsgKey=" . $returnMsgKey);

?>