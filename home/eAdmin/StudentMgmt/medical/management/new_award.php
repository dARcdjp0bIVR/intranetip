<?php
// using:
/*
 * 	2017-06-14 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='AWARDSCHEME_MANAGEMENT') || !$plugin['medical_module']['discipline']){
	header("Location: /");
	exit();
}
$CurrentPage = "ManagementAward";
$CurrentPageName = $Lang['medical']['menu']['award'];
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Scheme'] , "?t=management.award_list", 1);
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Performance'] , "?t=management.student_list_of_performance", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['award'], "?t=management.award_list");
$PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");

$form_action = "?t=management.new_award_save";

$linterface->LAYOUT_START($returnMsg);

include_once('templates/award.tmpl.php');
$linterface->LAYOUT_STOP();
?>