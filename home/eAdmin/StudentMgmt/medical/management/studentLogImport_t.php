<?php
// modify : cameron
//http://192.168.0.146:31002/home/eAdmin/StudentMgmt/medical/?t=management.studentLogImport1
$CurrentPage = "ManagementStudentLog";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentLog'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		


$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=1, $CustStepArr='');

$Title = 'Import Data';

$PAGE_NAVIGATION[] = array($Title,"");

$linterface->LAYOUT_START($Msg);
//echo $h_stepUI;
?>
<form method="POST" name="frm1" id="frm1" action="index.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?=$h_stepUI;?></td>
</tr>

<tr>
	<td>
		<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
		            <tr> 
            	<td><br />


					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "csvfile" name="csvfile"></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['Remarks']?> </td>
							<td class="tabletext"><?=$ImportRemark?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><?=$csvFile?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<span class="tabletextremark"><?=$i_general_required_field?></span>
					<br/><br/>

				</td></tr>
				<tr>
	                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		        </tr>
            <tr>
				<td align="center">
				<br/>
				<input type="hidden" name="t" value="management.studentLogImport3"/>
					<?= $linterface->GET_ACTION_BTN($button_import, "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					&nbsp;
					<?= $BackBtn ?>				
				</td>
			</tr>
		</table>
	</td>
</tr>

</table>



<?php
$linterface->LAYOUT_STOP();
?>