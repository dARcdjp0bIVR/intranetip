<?php
// using:
/*
 * 2018-03-23 Cameron
 * - remove IsCase and CaseID in MEDICAL_EVENT table
 * - one event can link to multiple cases
 *
 * 2018-02-27 Cameron
 * - add InputBy and DateInput
 *
 * 2017-07-03 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = ($_POST['Popup'] ? 'EVENT_MANAGEMENT' : 'CASE_MANAGEMENT')) || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

$caseNo = $objMedical->getNextCaseNo();
$dataAry['CaseNo'] = $caseNo;
$dataAry['StudentID'] = $StudentID;
$dataAry['StartDate'] = $StartDate;
$dataAry['EndDate'] = $EndDate;
$dataAry['Summary'] = standardizeFormPostValue($Summary);
$dataAry['Detail'] = standardizeFormPostValue($Detail);
$dataAry['IsClosed'] = $IsClosed;
$dataAry['ClosedRemark'] = standardizeFormPostValue($ClosedRemark);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['DateInput'] = 'now()';
$sql = $objMedical->INSERT2TABLE('MEDICAL_CASE', $dataAry, array(), false);

$ldb->Start_Trans();
// # step 1: add to case
$res = $ldb->db_db_query($sql);
$result[] = $res;
if ($res) {
    $CaseID = $ldb->db_insert_id();
    
    // # step 2: add case PIC
    foreach ((array) $TeacherID as $id) {
        unset($dataAry);
        $dataAry['CaseID'] = $CaseID;
        $dataAry['TeacherID'] = $id;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_PIC', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
    }
    
    // # step 3: add case event
    foreach ((array) $SelEventID as $id) {
        unset($dataAry);
        $dataAry['CaseID'] = $CaseID;
        $dataAry['EventID'] = $id;
        $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_EVENT', $dataAry, array(), false, false, false); // no DateModified field
        $result[] = $ldb->db_db_query($sql);
        
        // // update CaseID in event
        // $sql = "UPDATE MEDICAL_EVENT SET IsCase=1, CaseID='" . $CaseID . "' WHERE EventID='" . $id . "'";
        // $result[] = $ldb->db_db_query($sql);
    }
    
    // # step 4: update CaseID to case minutes
    foreach ((array) $SelMinuteID as $id) {
        $sql = "UPDATE MEDICAL_CASE_MINUTE SET CaseID='" . $CaseID . "' WHERE MinuteID='" . $id . "'";
        $result[] = $ldb->db_db_query($sql);
    }
    
    // step 5: update next CaseNo
    $academicYearID = Get_Current_Academic_Year_ID();
    $yearPart = substr($caseNo, 0, 7);
    $prefix = substr($caseNo, 8, 1);
    $currentNo = substr($caseNo, - 4);
    $result[] = $objMedical->setNextCaseNo($academicYearID, $yearPart, $currentNo, $prefix);
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
} else {
    $ldb->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

if ($_POST['Popup']) {
    $newRows = $objMedical->getCaseRowForEvent($CaseID);
    $js = "<script>
                var relevantCaseTable = window.opener.$('#RelevantCaseTable').html();
                relevantCaseTable = relevantCaseTable.replace(\"<\/tbody>\",\"\");
                relevantCaseTable += '".$newRows."';
                window.opener.$('#RelevantCaseTable').css('display','');
                window.opener.$('#RelevantCaseTable').html(relevantCaseTable);
                window.close();
            </script>";
    echo $js;
} else {
    header("location: ?t=management.case_list&returnMsgKey=" . $returnMsgKey);
}

?>