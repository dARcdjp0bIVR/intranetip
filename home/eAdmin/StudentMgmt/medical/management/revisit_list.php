<?php
// using:
/*
 * 2019-07-05 Cameron
 * - show curing status column [case #P150622]
 *
 * 2018-12-27 Cameron
 * - show error message when deleting non-self record
 *  
 * 2018-06-05 Cameron
 * - show created by person [case #F133828]
 * - don't show title for people column (use getNameFieldByLang2 instead of getNameFieldByLang) 
 * 
 * 2018-02-27 Cameron
 * - add data-deleteAllow attribute to checkbox item for checking if it's allowed to delete [case #F135176]
 * - show DateModified and LastModifiedBy
 * 
 * 2018-01-23 Cameron
 * - change default order to descending by date
 *
 * 2017-11-27 Cameron
 * - fix: cookie to store AcademicYearID etc.
 *
 * 2017-10-13 Cameron
 * - fix: set AcademicYearID to current academic year if it's not specify
 * - add GroupID filter
 *
 * 2017-08-31 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'REVISIT_MANAGEMENT') || ! $plugin['medical_module']['revisit']) {
    header("Location: /");
    exit();
}

$arrCookies = array();
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_right_page_number",
    "pageNo"
);
$arrCookies[] = array(
    "ck_right_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_right_page_field",
    "field"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitAcademicYearID",
    "AcademicYearID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitClassName",
    "ClassName"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitStudent",
    "RevisitStudent"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitGroupID",
    "GroupID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitHospital",
    "Hospital"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitDivision",
    "Division"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitDateStart",
    "RevisitDateStart"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitDateEnd",
    "RevisitDateEnd"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_RevisitKeyword",
    "keyword"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order; // default descending
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);

$currentAcademicYearID = Get_Current_Academic_Year_ID();

$classCond = '';
$cond = '';

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNumber = "u.ClassNumber";
    $classNameEng = $classNameField;
    $joinClass = '';
    $text_col = 19;
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNumber = "ycu.ClassNumber";
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $currentAcademicYearID . "' ";
    $text_col = 18;
}

if ($ClassName) {
    $cond .= " AND s.ClassName='" . $li->Get_Safe_Sql_Query($ClassName) . "'";
}

if ($RevisitStudent) {
    $cond .= " AND c.StudentID='" . $li->Get_Safe_Sql_Query($RevisitStudent) . "'";
}

if ($Hospital) {
    $cond .= " AND c.Hospital='" . $li->Get_Safe_Sql_Query($Hospital) . "'";
}

if ($Division) {
    $cond .= " AND c.Division='" . $li->Get_Safe_Sql_Query($Division) . "'";
}

if ($GroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $GroupID . "'";
}

if ($RevisitDate) {
    if ($RevisitDateStart) {
        if($sys_custom['medical']['MedicalReport']){
               $cond .= "AND (c.RevisitDate != '0000-00-00') ";
//             $cond .= " AND (CASE WHEN c.RevisitDate = '0000-00-00' THEN c.DateInput ELSE c.RevisitDate END) >='" . $RevisitDateStart . "'";
        } else {
            $cond .= " AND c.RevisitDate>='" . $RevisitDateStart . "'";
        }
    }
    
    if ($RevisitDateEnd) {
        if($sys_custom['medical']['MedicalReport']){
//             $cond .= " AND (CASE WHEN c.RevisitDate = '0000-00-00' THEN c.DateInput ELSE c.RevisitDate END) <='" . $RevisitDateEnd . "'";
        } else {
            $cond .= " AND c.RevisitDate<='" . $RevisitDateEnd . "'";
        }
    }
    $AcademicYearID = 'null';
} else {
    if (! $AcademicYearID) {
        $AcademicYearID = $currentAcademicYearID;
    }
    $startDate = getStartDateOfAcademicYear($AcademicYearID);
    $endDate = getEndDateOfAcademicYear($AcademicYearID);
    if($sys_custom['medical']['MedicalReport']){
        $cond .= " AND (CASE WHEN c.RevisitDate = '0000-00-00' THEN c.DateInput ELSE c.RevisitDate END) >='" . $startDate . "' AND (CASE WHEN c.RevisitDate = '0000-00-00' THEN c.DateInput ELSE c.RevisitDate END) <='" . $endDate . "'";
    } else {
        $cond .= " AND c.RevisitDate>='" . $startDate . "' AND c.RevisitDate<='" . $endDate . "'";
    }
}

$student_name = getNameFieldByLang2('u.');
$createdBy = getNameFieldByLang2('cb.');
$updatedBy = getNameFieldByLang2('ub.');

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if ($keyword != "") {
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (s.Student LIKE '%$kw%')";
    unset($kw);
}

$isRequiredCheckSelfRevisit = $objMedical->isRequiredCheckSelfRevisit();
$checkSelfDeleteStr = "IF(c.InputBy=" . $_SESSION['UserID'] . ",1,0)";
$dataDeleteAllowValue = $isRequiredCheckSelfRevisit ? $checkSelfDeleteStr : '1';
if($sys_custom['medical']['MedicalReport']){
    $dateField = "IF(c.RevisitDate = '0000-00-00', '-', c.RevisitDate)";
} else {
    $dateField = "c.RevisitDate";
}
//$Lang['medical']['revisit']['unstatedDate']
$sql = "SELECT 
				CONCAT('<a href=\"?t=management.revisit_view&RevisitID=',c.`RevisitID`,'\">', $dateField,'</a>'),
				CONCAT(s.Student,' (',s.ClassName,'-',s.ClassNumber,')') as Student,
				c.CuringStatus,
				IF(c.Hospital='' OR c.Hospital is null, '-',c.Hospital) as Hospital,
				IF(c.Division='' OR c.Division is null, '-',c.Division) as Division,
				{$createdBy} AS CreatedBy,
                {$updatedBy} AS UpdatedBy,
                c.DateModified,
				CONCAT('<input type=\'checkbox\' name=\'RevisitID[]\' id=\'RevisitID[]\' data-deleteAllow=\'',$dataDeleteAllowValue,'\' value=\'', c.`RevisitID`,'\'>')
		FROM
				MEDICAL_REVISIT c
		INNER JOIN (
				SELECT 	u.UserID, 
						$classNameEng as ClassName, 
						$classNumber as ClassNumber,
						$student_name as Student
				FROM 
						INTRANET_USER u
				" . $joinClass . "
			) AS s ON s.UserID=c.StudentID
		LEFT JOIN INTRANET_USER cb ON cb.UserID=c.InputBy
        LEFT JOIN INTRANET_USER ub ON ub.UserID=c.LastModifiedBy
		WHERE 1 " . $cond;
// debug_pr($sql);die();

$li->field_array = array(
    "RevisitDate",
    "Student",
    "CuringStatus",
    "Hospital",
    "Division",
    "CreatedBy",
    "UpdatedBy",
    "DateModified"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0,
    $text_col,
    0,
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['revisit']['RevisitDate']) . "</th>\n";
$li->column_list .= "<th width='14%' >" . $li->column($pos ++, $Lang['medical']['revisit']['StudentName']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['revisit']['CuringStatus']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['revisit']['Hospital']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['revisit']['Division']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['inputBy']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['lastModifiedBy']) . "</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['general']['dateModified']) . "</th>\n";
$li->column_list .= "<th width='1'>" . $li->check("RevisitID[]") . "</th>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet2(document.form1,'?t=management.new_revisit')", $button_new, "", "", "", 0);

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters
// Acadermic Year Filter
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="changeAcademicYearFilter();"', 0, 0, $AcademicYearID);

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='ClassName' onChange='this.form.submit();'", $ClassName, "", $Lang['medical']['event']['AllClass'], $currentAcademicYearID);
$groupFilter = $objMedical->getGroupSelection("GroupID", $GroupID, " onChange='this.form.submit();'", $Lang['medical']['revisit']['AllGroup']);

if (! empty($ClassName) && ! empty($GroupID)) {
    $includeUserIdAry = $objMedical->getMedicalGroupStudent(array(
        $GroupID
    ));
    $includeUserIdAry = BuildMultiKeyAssoc($includeUserIdAry, 'UserID', array(
        'UserID'
    ), $SingleValue = 1);
    $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName, $includeUserIdAry);
} else if (! empty($ClassName) && empty($GroupID)) {
    $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
} else if (empty($ClassName) && ! empty($GroupID)) {
    $studentAry = $objMedical->getStudentNameListWClassNumberByGroupID($GroupID);
} else {
    $studentAry = $objMedical->getAllStudents($currentAcademicYearID);
}
$studentFilter = getSelectByArray($studentAry, "name='RevisitStudent' onChange='this.form.submit();'", $RevisitStudent, 0, 0, $Lang['medical']['event']['AllStudent']);

$hospitalAry = array();
$rs = $objMedical->getHospital();
foreach ((array) $rs as $v) {
    $v = $v['Hospital'];
    $hospitalAry[] = array(
        $v,
        $v
    );
}
$hospitalFilter = getSelectByArray($hospitalAry, "name='Hospital' onChange='this.form.submit();'", $Hospital, 0, 0, $Lang['medical']['revisit']['AllHospital']);

$divisionAry = array();
$rs = $objMedical->getDivision();
foreach ((array) $rs as $v) {
    $v = $v['Division'];
    $divisionAry[] = array(
        $v,
        $v
    );
}
$divisionFilter = getSelectByArray($divisionAry, "name='Division' onChange='this.form.submit();'", $Division, 0, 0, $Lang['medical']['revisit']['AllDivision']);

$RevisitDateStart = $RevisitDateStart ? $RevisitDateStart : substr(getStartDateOfAcademicYear($currentAcademicYearID), 0, 10);
$RevisitDateEnd = $RevisitDateEnd ? $RevisitDateEnd : substr(getEndDateOfAcademicYear($currentAcademicYearID), 0, 10);

// ############ end Filters

$CurrentPage = "ManagementRevisit";
$CurrentPageName = $Lang['medical']['menu']['revisit'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['revisit'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

if ($returnMsgKey == 'DeleteNonSelfRecord') {
    $returnMsg = $Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'];
}else {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

include_once ('templates/revisit_list.tmpl.php');
$linterface->LAYOUT_STOP();

?>