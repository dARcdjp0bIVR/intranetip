<?php
// using:
/*
 * 2018-12-21 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_DELETE') || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$lfs = new libfilesystem();
$result = array();
$dataAry = array();
$isRequiredCheckSelfStaffEvent = $objMedical->isRequiredCheckSelfStaffEvent();
$returnMsgKey = '';

if (count($StaffEventID) > 0) {
    $isAllowedToDelete = true;
    if ($isRequiredCheckSelfStaffEvent) {
        $sql = "SELECT StaffEventID FROM MEDICAL_STAFF_EVENT WHERE StaffEventID IN ('" . implode("','", (array)$StaffEventID) . "') AND InputBy<>'" . $_SESSION['UserID'] . "'";
        $checkResult = $ldb->returnResultSet($sql);
        if (count($checkResult)) {
            $isAllowedToDelete = false;
            $returnMsgKey = 'DeleteNonSelfRecord';
        }
    }
    
    if ($isAllowedToDelete) {
        
        $ldb->Start_Trans();
        
        // log delete record
        foreach ((array) $StaffEventID as $_staffEventID) {
            
            $staffEventAry = $objMedical->getStaffEvents($_staffEventID);
            $staffEvent = current($staffEventAry);
            
            unset($dataAry);
            
            $details = $staffEvent['LocationBuildingID'] . '~^' . $staffEvent['LocationLevelID'] . '~^' . $staffEvent['LocationID'];
            $details .= '~^' . $staffEvent['IsReportedInjury'] . '~^' . $staffEvent['InjuryLeave'] . '~^' . $staffEvent['SickLeave'];
            
            $dataAry['TableName'] = 'MEDICAL_STAFF_EVENT';
            $dataAry['RecordID'] = $_staffEventID;
            $dataAry['StartDate'] = $staffEvent['StartDate'];
            $dataAry['EndDate'] = $staffEvent['EndDate'];
            $dataAry['RefID'] = $staffEvent['EventTypeID'];
            $dataAry['Summary'] = $staffEvent['Remark'];
            $dataAry['Detail'] = $details;
            $dataAry['StudentID'] = $staffEvent['StaffID'];
            $dataAry['OtherRef'] = $staffEvent['EventTypeLev2ID'];
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
            $result['AddLog'] = $ldb->db_db_query($sql);
        }
        
        // delete event
        $sql = "DELETE FROM MEDICAL_STAFF_EVENT WHERE StaffEventID IN ('" . implode("','", (array)$StaffEventID) . "')";
        $result['DeleteStaffEvent'] = $ldb->db_db_query($sql);
        
        $sql = "DELETE FROM MEDICAL_EVENT_STAFF_EVENT WHERE StaffEventID IN ('" . implode("','", (array)$StaffEventID) . "')";
        $result['DeleteStaffEventStudentEvent'] = $ldb->db_db_query($sql);
        
        $attachment = $objMedical->getStaffEventAttachment($StaffEventID);
        if (count($attachment)) {
            foreach ((array) $attachment as $rs) {
                $FileID = $rs['FileID'];
                $FolderPath = $rs['FolderPath'];
                $FileHashName = $rs['FileHashName'];
                $file = $file_path . "/file/medical/edisc/" . $FolderPath . "/" . $FileHashName;
                $lfs = new libfilesystem();
                $result['DeletePhysicalFile'] = $lfs->file_remove($file);
                $result['DeleteAttachmentFromDb'] = $objMedical->deleteAttachmentByID('MEDICAL_STAFF_EVENT_ATTACHMENT', $FileID);
            }
        }
        unset($attachment);

        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $returnMsgKey = 'DeleteSuccess';
        } else {
            $ldb->RollBack_Trans();
            $returnMsgKey = 'DeleteUnsuccess';
        }
    }
}

header("location: ?t=management.staffEventList&returnMsgKey=" . $returnMsgKey);

?>