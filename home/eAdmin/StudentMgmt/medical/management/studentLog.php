<?php
// using: 
		
if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT') || !$plugin['medical_module']['studentLog']){
	header("Location: /");
	exit();
}
$CurrentPage = "ManagementStudentLog";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentLog'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
//$MODULE_OBJ['title'] = $Lang['medical']['menu']['meal'];

$objFCM_UI = new form_class_manage_ui;
$valueArray =array();

$valueArray['sleep']['']=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['sleep']['1']=$Lang['medical']['report_meal']['sleepOption']['StayIn'];
$valueArray['sleep']['0']=$Lang['medical']['report_meal']['sleepOption']['StayOut'];

$valueArray['gender']['']=$Lang['medical']['report_meal']['search']['genderOption']['all'];
$valueArray['gender']['M']=$Lang['medical']['report_meal']['search']['genderOption']['M'];
$valueArray['gender']['F']=$Lang['medical']['report_meal']['search']['genderOption']['F'];

if($_POST['action']=='deleteRecord'){
	$result = $objMedical->deleteStudentLogRecords($_POST['recordID']);
	if($result){
		$Msg =$Lang['General']['ReturnMessage']['DeleteSuccess'];
	}
	else{
		$Msg =$Lang['General']['ReturnMessage']['DeleteUnsuccess'];
	}
}

function getSelectionBox($id, $name, $valueArray, $valueSelected="", $class=""){
	$returnStr = '';
	$returnStr .= "<select class='{$class}' id='{$id}' name='{$name}'>";
	foreach( (array)$valueArray as $key=>$valueItem){
			$selected ='';
			if( (string)$key === $valueSelected){
				
				$selected ='selected';
			}
			$returnStr .= "<option $selected value='{$key}'>{$valueItem}</option>";
	}
	$returnStr .= "</select>";
	
	return $returnStr;
	
}

$linterface->LAYOUT_START($Msg);
//debug_r("<----".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n");
include_once('templates/studentLog.tmpl.php');
$linterface->LAYOUT_STOP();
?>