<?php
// using:
/*
 * 	2017-06-30 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess=($popup ? 'EVENT_MANAGEMENT' : 'CASE_MANAGEMENT')) || !$plugin['medical_module']['discipline']){
	header("Location: /");
	exit();
}

if (is_array($MinuteID)) {
	$minuteID = (count($MinuteID)==1) ? $MinuteID[0] : '';
}
else {
	$minuteID = $MinuteID;
}

if (!$minuteID) {	
	header("Location: ?t=management.case_list");
}

$rs_minute = $objMedical->getCaseMinute('',$minuteID);
if (count($rs_minute) == 1) {	
	$rs_minute = current($rs_minute);
	$caseID = $rs_minute['CaseID'];
}


## handle attachment(s)
$ret = $objMedical->handleTempUploadFolder();
$tempFolderPath = $ret['TempFolderPath'];
$use_plupload = $objMedical->is_use_plupload();

$plupload = array();
$plupload['use_plupload'] = $use_plupload;
if($use_plupload) {
	$plupload['pluploadButtonId'] = 'UploadButton3';
	$plupload['pluploadDropTargetId'] = 'DropFileArea3';
	$plupload['pluploadFileListDivId'] = 'FileListDiv3';
	$plupload['pluploadContainerId'] = 'pluploadDiv3';
}

$action = 'editMinuteSave';

include_once('templates/minute.tmpl.php');

if($use_plupload) {
	include_once('plupload_script.php');
}

?>