<?php 
// Using: 

# This file is no longer used.

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTSLEEP_MANAGEMENT')){
	header("Location: /");
	exit();
}

$objStudentSleepLog = new StudentSleepLog();

$recordID = trim(htmlentities($_POST['RecordID'], ENT_QUOTES, 'UTF-8'));

echo $objStudentSleepLog->deleteRecord($recordID);
?>