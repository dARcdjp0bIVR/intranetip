<?php
/*
 *  using:
 *  
 *  2019-01-03 Cameron
 *      - create this file
 * 
 */
 
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STUDENTEVENT_REPORT') || ! $plugin['medical_module']['discipline']) {
    header("Location: /");
    exit();
}

$htmlAry['closeBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$studentEventID = $_POST['Id'];

$attachmentHTML = $objMedical->getCurrentAttachmentLayout('event', $studentEventID, $mode='view');

?>
<style>
a{
	color:#2286C5;
	cursor:pointer;
}
.edit_pop_board_write {
	height: 150px;
}
.edit_bottom_v30{
	height: 30px;
}
</style>
<div id="thickboxContentDiv" class="edit_pop_board_write">
	<?php echo $attachmentHTML; ?>
</div>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $htmlAry['closeBtn']; ?>
</div>