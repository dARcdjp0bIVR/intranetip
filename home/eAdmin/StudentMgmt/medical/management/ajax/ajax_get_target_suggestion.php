<?
/*
 * 	Log
 *  	 
 * 	2017-08-30 [Cameron]
 * 		- add case Hospital, Division & Drug
 * 
 * 	2017-07-11 [Cameron] create this file
 * 
 */


## Get Data
$q = (isset($q) && $q != '') ? substr($q,3) : '';

$x = '';

if($q != ''){
	$ldb = new libdb();
	switch ($from) {
		case 'scheme':
			# search with input keyword
			$cond = ($q != '') ? " AND Target LIKE '%".$ldb->Get_Safe_Sql_Like_Query($q)."%'" : "";
			$sql = "SELECT 
							DISTINCT Target 
					FROM
							MEDICAL_AWARD_SCHEME_TARGET
					WHERE	1
						 	{$cond} 
					ORDER BY
							Target LIMIT 10";
			$result = $ldb->returnResultSet($sql);
			
			if(!empty($result)){
				foreach ($result as $row){
					$x .= $row['Target']."|".$row['Target']."\n";
				}
			}
		break;
		
		case 'performance':
			# search with input keyword
			$cond = ($q != '') ? " AND Target LIKE '%".$ldb->Get_Safe_Sql_Like_Query($q)."%'" : "";
			$sql = "SELECT 
							DISTINCT Target 
					FROM
							MEDICAL_AWARD_SCHEME_STUDENT_TARGET
					WHERE	1
						 	{$cond} 
					ORDER BY
							Target LIMIT 10";
			$result = $ldb->returnResultSet($sql);
			
			if(!empty($result)){
				foreach ($result as $row){
					$x .= $row['Target']."|".$row['Target']."\n";
				}
			}
		
		break;
		
		case 'Hospital':
			# search with input keyword
			$cond = ($q != '') ? " AND Hospital LIKE '%".$ldb->Get_Safe_Sql_Like_Query($q)."%'" : "";
			$sql = "SELECT 
							DISTINCT Hospital 
					FROM
							MEDICAL_REVISIT
					WHERE	1
						 	{$cond} 
					ORDER BY
							Hospital LIMIT 10";
			$result = $ldb->returnResultSet($sql);
			
			if(!empty($result)){
				foreach ($result as $row){
					$x .= $row['Hospital']."|".$row['Hospital']."\n";
				}
			}
		
		break;	

		case 'Division':
			# search with input keyword
			$cond = ($q != '') ? " AND Division LIKE '%".$ldb->Get_Safe_Sql_Like_Query($q)."%'" : "";
			$sql = "SELECT 
							DISTINCT Division 
					FROM
							MEDICAL_REVISIT
					WHERE	1
						 	{$cond} 
					ORDER BY
							Division LIMIT 10";
			$result = $ldb->returnResultSet($sql);
			
			if(!empty($result)){
				foreach ($result as $row){
					$x .= $row['Division']."|".$row['Division']."\n";
				}
			}
		
		break;	

		case 'Drug':
			# search with input keyword
			$cond = ($q != '') ? " AND Drug LIKE '%".$ldb->Get_Safe_Sql_Like_Query($q)."%'" : "";
			$sql = "SELECT 
							DISTINCT Drug 
					FROM
							MEDICAL_REVISIT_DRUG
					WHERE	1
						 	{$cond} 
					ORDER BY
							Drug LIMIT 10";
			$result = $ldb->returnResultSet($sql);
			
			if(!empty($result)){
				foreach ($result as $row){
					$x .= $row['Drug']."|".$row['Drug']."\n";
				}
			}
		
		break;	
			
	}
}

echo $x;
?>