<?php
// using: 
// Breakfast = 0
// Lunch = 1
// Dinner = 2

/*
 *  2018-06-05 [Cameron]
 *      - show created by person [case #F133828]
 *      - show hyphen in eating conditon and remarks column after remove a row 
 * 
 *  2018-05-28 Cameron
 *      - fix: apply stripslashes to $mealItem['StatusName'] and $rs['MealStatus'] so that it can escape back slashes
 *  
 * 	2017-07-27 Cameron
 * 		- add argument $presetRowHeight=15 to GetPresetText() [case #P120778]
 * 		- use div instead of span for showing remarks and AttendanceStatusExplain to avoid overlap problem
 * 		- set row size to show all remark context when edit
 * 		- auto adjust the textarea height when edit remark
 * 
 * 	2017-07-25 Cameron
 * 		- add filter: gender [case #P120778]
 * 		- add argument $append=1 to GetPresetText()	
 */
$objMealStatus = new mealStatus();
$objlibMedical = new libMedical();
$resultList = array();
$studentDetailList = array();

$sleep = $_POST['sleep'];//

//////////////// INPUT CHECKING /////////
if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
	echo 'Date Format Error! Please select with format YYYY-MM-DD';
	return;
}

### Generate Legend HTML Start
$legendHTML ='';
$legendHTML .='<ul class="legend" style="list-style-type: none;">';

$mealDetail = $objMealStatus->getActiveStatus(' StatusCode ASC ');
$i=0;
foreach( (array)$mealDetail as $mealItem){
    $legendHTML .='<li><span class="colorBoxStyle" style="background-color:'.$mealItem['Color'].';">&nbsp;&nbsp;&nbsp;</span>&nbsp;'. stripslashes($mealItem['StatusName']).'</li>&nbsp;&nbsp;';
	if(++$i%6 == 0){
		$legendHTML .='<br /><br />';
	}
}

if(libMedicalShowInput::attendanceDependence('meal')){
	$legendHTML .= '<li><span class="colorBoxStyle" style="background-color:lightgray;">&nbsp;&nbsp;&nbsp;</span>&nbsp;'. $Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent'] . '</li>';
}
$legendHTML .='</ul>';
### Generate Legend HTML End

### Generate Result Table Start
$resultList = $objlibMedical->getMealDailyLogList(
				$_POST['date'], $_POST['timePeriod'], 
				$_POST['sleep'], $_POST['classID'], $_POST['gender'] );

$todayDate = date( 'Y-m-d' );
$nonFutureDate = strtotime($_POST['date']) <= strtotime($todayDate);

$groupStudentList = $objMedical->getMedicalGroupStudent($groupID);
$groupResult = array();
if($groupID != ''){
	foreach($resultList as $rs)
	{
		if(in_array(array('UserID'=>$rs['UserID']),$groupStudentList)){
			$groupResult[] = $rs;
		}
	}
}else{
	$groupResult = $resultList;
}

$i=1;
foreach( $groupResult as $index=>&$rs){
	
	// Always present if the school have not buy attendance module
//	if(!$plugin['attendancestudent']){
	if(!libMedicalShowInput::attendanceDependence('meal')){
		$rs['Present']=1; 
	}
	
	$indexHTML ='';
	$indexHTML .= $i.' ';
	if($rs['RecordID'] !=''){
		$indexHTML .= '<input name="mealRecord['.$rs['UserID'].'][RecordID]" type="hidden" value="'.$rs['RecordID'].'">';
	}
	$studentDetailList[$index][] = $indexHTML;
	
	// Display add button if student is absent and no record in DB and don't show default value
//	if($plugin['medical_module']['AbsentAllowedInput']['meal'] && !libMedicalShowInput::absentAllowedInputShowDefault('meal')){
	if(libMedicalShowInput::absentAllowedInput('meal') && !libMedicalShowInput::absentAllowedInputShowDefault('meal')){
		if( $rs['Present'] == 1){
			$studentDetailList[$index][] = '';
		}else{
			$hidden = '';
			if($rs['MealStatus'] != ''){
				$hidden = 'style="display:none;"';
			}
			$addBtnHTML = '<span class="table_row_tool">';
			$addBtnHTML .= '<a class="newBtn add" data-sid="'.$rs['UserID'].'"  title="'.$Lang['Btn']['Add'].'" '.$hidden.'></a>';
			$addBtnHTML .= '<br /></span>';
			$studentDetailList[$index][] = $addBtnHTML;
		}
	}
	
	$studentDetailList[$index][] = $rs['ClassName'];
	$studentDetailList[$index][] = $rs['Name'];
	$studentDetailList[$index][] = ($rs['Stay'])?$Lang['medical']['meal']['search']['sleepOption'][1]:$Lang['medical']['meal']['search']['sleepOption'][2];
	
	if(!libMedicalShowInput::attendanceDependence('meal')){
		$studentDetailList[$index][] = '<span>--</span>';
	}else if( $rs['Present'] == 1 && $nonFutureDate){
		$studentDetailList[$index][] = $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Present'];
	}else{
		$studentDetailList[$index][] = $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent'];
	}
	
	// Display the record if student is present or allowed absent input
	if( ($rs['Present'] == 1 || (libMedicalShowInput::absentAllowedInput('meal') && $rs['MealStatus'] != '') || (libMedicalShowInput::absentAllowedInput('meal') && libMedicalShowInput::absentAllowedInputShowDefault('meal')) ) && $nonFutureDate){
		
		$remarksHTML = '';
		if($rs['Remarks'] !=''){
			$remarksHTML .='<div class="viewArea">'.stripslashes(nl2br($rs['Remarks'])).'<br /></div>';
			$rows = 'rows="'.((int)substr_count($rs['Remarks'],"\n") + 2).'"';
		}
		else {
			$rows = '';
		}
		$remarksHTML .= '<a class="remarksBtn tablelink" ><div>'.$Lang['medical']['meal']['tableContent'][0].'</div></a>';
		$remarksHTML .= '<textarea name="mealRecord['.$rs['UserID'].'][remarks]" id="mealRecord['.$rs['UserID'].'][remarks]['.$i.']" '.$rows.' onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;">';
		$remarksHTML .= nl2br($rs['Remarks']);
		$remarksHTML .= '</textarea>';
		$remarksHTML .= '<span class="defaultRemarkSpan" style="display:none;">'.GetPresetText("jRemarksArr", $i, "mealRecord[{$rs['UserID']}][remarks][$i]", "", "", "", "1", "15").'</span>';
		
		$studentDetailList[$index][] = $objMealStatus->getHTMLSelection($htmlName = 'mealRecord['.$rs['UserID'].'][mealStatus]',$userSelectedValue = ($rs['MealStatus'] !='')?stripslashes($rs['MealStatus']):'','class="mealStatusSelection"');
		$studentDetailList[$index][] = $remarksHTML;
		$studentDetailList[$index][] = ($rs['CreatedBy'] !='')? $rs['CreatedBy'] : '-' ;
		$studentDetailList[$index][] = ($rs['ConfirmedName'] !='')? $rs['ConfirmedName'] : '-' ;
		$studentDetailList[$index][] = ($rs['DateModified'] !='')? ('<span>'.date( 'Y-m-d H:i', strtotime($rs['DateModified'])).'</span>') : '-' ;
		
		$studentDetailList[$index][] = '';
	}
	else{
		if($rs['RecordID'] == NULL){
			$studentDetailList[$index][] = '-';
			$studentDetailList[$index][] = '-';
			$studentDetailList[$index][] = '-';
			$studentDetailList[$index][] = '-';
			$studentDetailList[$index][] = '-';
			$studentDetailList[$index][] = '';
		}else{			
			$status = $objMealStatus->getAllStatus(' recordstatus = 1 and DeletedFlag = 0 and statusID = '.$rs['MealStatus']);
			$colorBox = '<font color="red">**</font><span style="white-space:nowrap;"><span class="colorBoxStyle"style="background-color: '.$status[0]['Color'].'">&nbsp;&nbsp;&nbsp;</span>&nbsp;';
			$hiddenInput = '<input name="mealRecord['.$rs['UserID'].'][mealStatus]" type="hidden" value="'.$rs['MealStatus'].'">';
			$studentDetailList[$index][] = $colorBox . stripslashes($status[0]['StatusName']) . $hiddenInput;
			
			$remarksHTML = '';
			if($rs['Remarks'] !=''){
				$remarksHTML .='<div class="viewArea">'.stripslashes(nl2br($rs['Remarks'])).'<br /></div>';
				$rows = 'rows="'.((int)substr_count($rs['Remarks'],"\n") + 2).'"';
			}else{
				$remarksHTML = '-';
				$rows = '';
			}
			$remarksHTML .= '<textarea name="mealRecord['.$rs['UserID'].'][remarks]" id="mealRecord['.$rs['UserID'].'][remarks]['.$i.']" '.$rows.' onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;">';
			$remarksHTML .= $rs['Remarks'];
			$remarksHTML .= '</textarea>';	
			$studentDetailList[$index][] = $remarksHTML;
			
			$studentDetailList[$index][] = ($rs['CreatedBy'] !='')?$rs['CreatedBy']:'-';
			$studentDetailList[$index][] = ($rs['ConfirmedName'] !='')?$rs['ConfirmedName']:'-';
			$studentDetailList[$index][] = '<span>'.(($rs['DateModified'] !='')?date( 'Y-m-d H:i', strtotime($rs['DateModified'])):'-').'</span>';
			
			$delBtnHTML = '<span class="table_row_tool">';
			$delBtnHTML .= '<a class="deleteBtn delete deleteWrongData" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$rs['RecordID'].' " data-sid="'.$rs['UserID'].'"  ></a>';
			$delBtnHTML .= '</span>';
			$studentDetailList[$index][] = $delBtnHTML;
		}
	}
	
	 // If allowed absent input but don't show default value, add delete button for absent recorded student
	if(libMedicalShowInput::absentAllowedInput('meal') && !libMedicalShowInput::absentAllowedInputShowDefault('meal')){
		if($rs['Present'] == 0 && $rs['MealStatus'] != ''){
			$delBtnHTML = '<span class="table_row_tool">';
			$delBtnHTML .= '<a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$rs['RecordID'].' " data-sid="'.$rs['UserID'].'"  ></a>';
			$delBtnHTML .= '</span>';
			$studentDetailList[$index][ count($studentDetailList[$index])-1 ] = $delBtnHTML;
		}
	}
	
	++$i;
}
### Generate Result Table End

// Add button if allow absent input but don't show default value
if(libMedicalShowInput::absentAllowedInput('meal') && !libMedicalShowInput::absentAllowedInputShowDefault('meal')){ 
    $showAddButton = true;
}
else {
    $showAddButton = false;
}

#### Get default remarks ####
$defaultRemark = $objlibMedical->getDefaultRemarksArray($medical_cfg['general']['module']['meal']);
//////////////////////////////// Presentation Layer //////////////////////////////////////
echo <<< REMARKS_INIT
	<script>
		var jRemarksCount=$i;
		var jRemarksArr=$defaultRemark;
	</script>
REMARKS_INIT;
?>
<style>
.viewArea{
	-ms-word-break: break-all;
	-webkit-hyphens: auto;
	-moz-hyphens: auto;
	
	word-break: break-all;
	word-break: break-word;
	hyphens: auto;
}
//.tabletoplink{
//	font-weight: bold !important;
//}
.legend li{
	display: inline;
}
.colorBoxStyle{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:17px;
	height:17px;
}
.disabledField td{
	background-color: lightgray !important;
}
.tablelink{
	cursor:pointer;
}
//.tabletoplink{
//	font-weight: bold !important;
//}
#mealForm table td, #mealForm table td * {
    vertical-align: top;
}
.defaultRemarkSpan{
	margin-left:5px;
}
</style>
<br />
<br />

<?php
	echo '<div id="mealStatusTemplate" style="display:none">'. $objMealStatus->getHTMLSelection($htmlName = 'mealRecord['.$rs['UserID'].'][mealStatus]',$userSelectedValue = '','class="mealStatusSelection"') .'</div>';
		
//	$remarksHTML = '<div id="remarksTemplate">';
	$remarksHTML = '<div id="remarksTemplate" style="display:none">';
	$remarksHTML .= '<a class="remarksBtn tablelink" >'.$Lang['medical']['meal']['tableContent'][0].'</a>';
	$remarksHTML .= '<textarea onkeyup="resizeTextArea(this);" style="display:none; overflow:hidden;"></textarea>';
	$remarksHTML .= '<span class="defaultRemarkSpan" style="display:none"></span>';
	$remarksHTML .= '</div>'; 
	echo $remarksHTML;
?>
<?php echo $legendHTML;?>
<br />
<br />

<form method="post" action="?t=management.mealSave" name="mealForm" id="mealForm">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<colgroup>
		<col style="width:2%"/>
		<?php if($showAddButton):?>
			<col style="width:2%"/>
		<?php endif; ?>
		<col style="width:7%"/>
		<col style="width:7%"/>
		<col style="width:6%"/>
		<col style="width:6%"/>
		<col style="width:19%"/>
		<col style="width:19%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<col style="width:10%"/>
		<?php //if(libMedicalShowInput::absentAllowedInput('meal') && !libMedicalShowInput::absentAllowedInputShowDefault('meal')){ // Add button if allow absent input but don't show default value?>
			<col style="width:1%"/>
		<?php //} ?>
	</colgroup>
	<thead>
		<tr class="tabletop">
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['Number']; ?></th>
			<?php if($showAddButton):?>
				<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['Add']; ?></th>
			<?php endif; ?>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['ClassName']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['StudentName']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['StayOrNot']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['AttendanceStatus']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['EatingCondition']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['Remarks']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['general']['inputBy']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['LastPersonConfirmed']; ?></th>
			<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['LastUpdated']; ?></th>
			<?php //if(libMedicalShowInput::absentAllowedInput('meal') && !libMedicalShowInput::absentAllowedInputShowDefault('meal')){ // Delete button if allow absent input but don't show default value?>
				<th class="tabletoplink" nowrap><?php echo $Lang['medical']['meal']['tableHeader']['Delete']; ?></th>
			<?php //} ?>
		</tr>		
	</thead>
	<tbody>
		<?php foreach( $studentDetailList as $index=>$studentDetail): 
				$disabled ='';
//				if( $studentDetail[5] == $Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent'] ){
				if($groupResult[$index]['Present'] == 0){
					$disabled ='class="disabledField"';
				}
		?>
			<tr <?php echo $disabled; ?> class="row_approved">
				<?php foreach( $studentDetail as $studentDetailItem): ?>
					<td><?php echo $studentDetailItem; ?></td>
				<?php endforeach; ?>
			</tr>
		<?php endforeach; ?>
		<?php if(count($studentDetailList)==0): ?>
		<tr>
			<td colspan="<?php echo $showAddButton ? '12' : '11';?>"  class="tableContent" align="center" >
					<?php echo $Lang['General']['NoRecordFound']; ?>
			</td>
		</tr>
		<?php endif; ?>
	</tbody>
	<tfoot>
		<?php if(!libMedicalShowInput::absentAllowedInput('meal')){ ?>
			<tr>
				<td align="center" colspan="12" class="dotline" style="text-align:left">
					<div><?=$Lang['medical']['meal']['AttendanceStatusExplain']?></div>
				</td>
			</tr>
		<?php } ?>
		<?php if(count($studentDetailList)>0): ?>
		<tr>
			<td align="center" colspan="<?php echo $showAddButton ? '12' : '11';?>">
				<!--<input type="submit" id="Save" name="Save" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>"> -->
				<input type="hidden" id="deleteReacordList" name="deleteReacordList" value="">
				<input type="button" id="Save" name="Save" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>">
				<input type="button" id="Reset" name="Reset" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['reset']; ?>">
				<!--<input type="button" id="Cancel" name="Cancel" class="formbutton_v30 print_hide " value="<?php echo $Lang['medical']['meal']['button']['cancel']; ?>">-->
			</td>	
		</tr>
		<?php endif; ?>
	</tfoot>

</table>
<input type="hidden" name="timePeriod" value="<?php echo $_POST['timePeriod'] ?>" />
<input type="hidden" name="date" value="<?php echo $_POST['date'] ?>" />
</form>

<script>
	$('.newBtn').click(function(){
		var userID = $(this).attr('data-sid');
		var trDOM = $(this).closest('tr');
		$(this).hide();

		var mealStatusName = 'mealRecord['+userID+'][mealStatus]';
		var remarksName = 'mealRecord['+userID+'][remarks]';
		
		setElementName($('#mealStatusTemplate').find('select'),mealStatusName);
		setElementName($('#remarksTemplate').find('textarea'),remarksName);

		trDOM.find('td:eq(0)').append('<input name="mealRecord['+userID+'][RecordID]" type="hidden" value="">');
		trDOM.find('td:eq(6)').html('').append($('#mealStatusTemplate').html());
		trDOM.find('td:eq(7)').html('').append($('#remarksTemplate').html());
		trDOM.find('td:eq(11)').append('<span class="table_row_tool"><a class="deleteBtn delete" title="<?=$Lang["Btn"]["Delete"]?>" data-recordID="" data-sid="'+userID+'"  ></a></span>');

		var remarksID = 'mealRecord['+userID+'][remarks][' + jRemarksCount + ']';
		trDOM.find('td:eq(7)').find('textarea').attr('id',remarksID);
		$.ajax({
			url : "?t=management.ajax.getRemarksDefaultList",
			type : "GET",
			data : {
				jArrName: 'jRemarksArr',
				btnID: jRemarksCount,
				targetName: remarksID,
				jOtherFunction: '',
				divIDPrefix: ''
			},
			success : function(result) {
				$('textarea[id="' + remarksID + '"]').next().html(result);
			}
		});

		jRemarksCount++;
	});
/**
 * Specially for IE7, you know it...
 * Reference: http://stackoverflow.com/questions/2094618/changing-name-attr-of-cloned-input-element-in-jquery-doesnt-work-in-ie6-7
 */
function setElementName(elems, name) {
	if ($.browser.msie === true){
		$(elems).each(function() {
			this.mergeAttributes(document.createElement("<input name='" + name + "'/>"), false);
		});
	} else {
		$(elems).attr('name', name);
	}
}
	
	$('.deleteBtn').die('click').live('click', function(){
		var date = '<?=$_POST['date']?>';
		var userID = $(this).attr('data-sid');
		var recordID = $(this).attr('data-recordID');
		var trDOM = $(this).closest('tr');
		
		var isDeleted = confirm('<?php echo $Lang['medical']['report_general']['deleteEvent'] ?>');
		if(!isDeleted){
			return;
		}
		
		trDOM.find('td input').remove();
		
		if(recordID == ''){ // new record (not in DB)
			for(var i=6;i<12;i++){
				trDOM.find('td:eq('+i+') > *').remove();
			}
			for(var i=6;i<11;i++){
				trDOM.find('td:eq('+i+')').html('-');
			}
		}else{ // old record (in DB)
			$("#deleteReacordList").val($("#deleteReacordList").val()+','+recordID);
			for(var i=6;i<12;i++){
				trDOM.find('td:eq('+i+') > *').remove();
			}
			for(var i=6;i<11;i++){
				trDOM.find('td:eq('+i+')').html('-');
			}
		}
		trDOM.find('.newBtn').show();

		if($(this).hasClass('.deleteWrongData')){
			for(var i=5;i<10;i++){
				trDOM.find('td:eq('+i+')').html('-');
			}
		}
	});

	$('.mealStatusSelection').live('change', function(){
		$(this).prev().css('background-color', $(this).find(":selected").attr('data-color'));
	});
	$('.remarksBtn').live('click', function(){
		$(this).hide();
		$(this).prev().hide();
		$(this).next().show();
		$(this).next().next().show();

		$(this).next().focus();
		var psconsole = $(this).next();
    	psconsole.scrollTop(
        psconsole[0].scrollHeight - psconsole.height()
    	);
	});	
	
	// Set Default Color	
	$('.mealStatusSelection').change();
	
	$('#Reset').click(function(){
		$('#mealForm')[0].reset();
		$('.mealStatusSelection').change();
	});
	$('#Save').click(function(){
		$.ajax({
			url : "?t=management.mealSave",
			type : "POST",
			data : $('#mealForm').serialize(),
			success : function(result) {
//				console.log(result);
				if(result){
					msg ='<?php echo $Lang['General']['ReturnMessage']['UpdateSuccess'];?>';
				}
				else{
					msg = '<?php echo $Lang['General']['ReturnMessage']['UpdateUnsuccess'];?>';
				}
				Get_Return_Message(msg, 'system_message_box');
				$("html, body").animate({ scrollTop: 0 }, 0);

//				$('#medical_msg').remove();
//				$('#message_body').append('<span id="medical_msg">'+msg+'</span>');
//				$('#system_message_box').css('visibility','visible');
				$('#Search').click();
			}
		});	
		
	});


</script>