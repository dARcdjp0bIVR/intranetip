<?php
// using: 
/*
 *  2019-01-03 Cameron
 *      - change Lang button from Cancel to Close
 */

$htmlAry['closeBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Close'], "button", "js_Hide_ThickBox();", 'cancelBtn_tb', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
$studentLogID = $_POST['Id'];
$attachmentDetailList = $objMedical->getAttachmentDetail($studentLogID);
$attachmentHTML = $objMedical->getAttachmentHTML($Id, $attachmentDetailList,$deleteBtn=false);
?>
<style>
a{
	color:#2286C5;
	cursor:pointer;
}
.edit_pop_board_write {
	height: 150px;
}
.edit_bottom_v30{
	height: 30px;
}
</style>
<div id="thickboxContentDiv" class="edit_pop_board_write">
	<?php echo $attachmentHTML; ?>
</div>
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
	<?php echo $htmlAry['closeBtn']; ?>
</div>