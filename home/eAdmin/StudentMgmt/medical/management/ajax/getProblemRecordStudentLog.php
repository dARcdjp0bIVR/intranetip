<?php 
// Using:

if(
	( !$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT') || !$plugin['medical_module']['studentLog'])
){
	header("Location: /");
	exit();
}

include_once($PATH_WRT_ROOT."includes/cust/medical/libProblemRecord.php");

$objStudentLogLev1 = new studentLogLev1();
$objStudentLogLev1 = new studentLogLev2();
$objlibMedical = new libMedical();


$problemData = $objMedical->getProblemRecordStudentLog($startdate,$enddate);
usort($problemData, array('libProblemRecordStudentLog','cmp_obj'));

function getBehaviourOneHTML($Lev1ID){
	$objStudentLogLev1 = new studentLogLev1($Lev1ID);
	return $objStudentLogLev1->getLev1Name();
}
function getBehaviourTwoHTML($Lev2ID){
	$objStudentLogLev2 = new studentLogLev2($Lev2ID);
	return $objStudentLogLev2->getLev2Name();
}

if($isPrint){
	include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");
	$printButton = $linterface->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit2");
}
?>

<style>
.legend li{
	display: inline;
}
.colorBoxStyle{
	display: inline-block;
	border: 1px solid #c0c0c0;
	width:17px;
	height:17px;
}
.disabledField td{
	background-color: lightgray !important;
}
.tablelink{
	cursor:pointer;
}
#sleepForm table td, #sleepForm table td * {
    vertical-align: top;
}
.defaultRemarkSpan{
	margin-left:5px;
}
.tabletoplink{
	font-weight: bold !important;
}
<?php if($isPrint){ ?>  
	.print_view, .print_view * {
		display: none !important;
	}
<?php } ?>

@media print
{
	.print_hide, .print_hide * {
		display: none !important;
	}
}
</style>


<br />

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td>

<div align="right" class="print_hide"><?=$printButton?></div>

<div class="Conntent_tool print_view">
	<a class="print tablelink print_view" name="printBtn" id="printBtn" style="float:none;display:inline;"><?=$Lang['Btn']['Print']?></a>
</div>
<br class="print_hide" />
<br class="print_hide" />
<br class="print_hide" />
<div style="width:100%;border-bottom: 2px black solid;font-weight:bold;">
	<?=$Lang['medical']['menu']['problemRecord']?><br />
	<?=$Lang['medical']['menu']['studentLog']?>: 
	( <?=$startdate?> <?=$Lang['StaffAttendance']['To']?> <?=$enddate?> )
</div>
<br />


<form method="post" name="studentLogForm" id="studentLogForm">
<div class="table_board">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="print_view">
			<tr>
				<td valign="bottom">
					<div nowrap="nowrap" class="common_table_tool">
						<a href="#" class="tool_delete" onclick="deleteRecord()"><?=$Lang['Btn']['Delete']?></a>
					</div>
				</td>
			</tr>
		</table>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center" class="common_table_list">
			<colgroup>
				<col style="width:3%"/>
				<?php if($isPrint){ ?>
					<col style="width:8%"/>
				<?php }else{ ?>
					<col style="width:5%"/>
				<?php } ?>
				<col style="width:10%"/>
				<col style="width:8%"/>
				
				<col style="width:8%"/>
				<col style="width:14%"/>
				<col style="width:13%"/>
				<col style="width:11%"/>
				<col style="width:11%"/>
				<col style="width:14%"/>
				<col style="width:3%" class="print_view"/>
			</colgroup>
			<thead>
				<tr class="tabletop">
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Number']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['ClassName']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['StudentName']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Date']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Time']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['StudentLogCondition']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Item']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Remarks']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['LastPersonConfirmed']?></th>
					<th class="tabletoplink" nowrap><?=$Lang['medical']['problemRecord']['studentLog']['tableHeader']['LastUpdated']?></th>
					<th class="tabletoplink print_view" nowrap><input type="checkbox" id="checkALL" /></th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(count($problemData) > 0){
					foreach($problemData as $index=>$data){ 
				?>
						<tr class="tablerow<?=($index%2)+1?>">
							<td valign="top"><?=($index+1)?></td>
							<?php if($data->UserID == $lastStudentID){ ?>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							<?php }else{ ?>
								<td valign="top"><?=$data->ClassName?>&nbsp;</td>
								<td valign="top"><?=$data->StudentName?>&nbsp;</td>
							<?php } ?>
							<td valign="top"><?=substr($data->RecordTime, 0, 10) // Date?>&nbsp;</td>
							<td valign="top"><?=substr($data->RecordTime, 11, 5) // Time?>&nbsp;</td>
							<td valign="top"><?=getBehaviourOneHTML($data->BehaviourOne)?>&nbsp;</td>
							<td valign="top"><?=getBehaviourTwoHTML($data->BehaviourTwo)?>&nbsp;</td>
							<td valign="top"><?=$data->Remarks?>&nbsp;</td>
							<td valign="top"><?=$data->ModifyByName?>&nbsp;</td>
							<td valign="top"><?=$data->DateModified?>&nbsp;</td>
							<td valign="top" class="print_view">
								<input type="checkbox" name="deleteReacordList[]" value="<?=$data->RecordID?>" />&nbsp;
							</td>
						</tr>
				<?php 
						$lastStudentID = $data->UserID;
					}
				}else{?>
					<tr><td colspan="<?=count($Lang['medical']['problemRecord']['studentLog']['tableHeader'])+1?>" align="center"><?=$Lang['SysMgr']['Homework']['NoRecord']?></td></tr>
				<?php 
				} 
				?>
			</tbody>
			<!-- tfoot>
				<!--tr>
					<td align="center" colspan="11" class="dotline" style="text-align:left">
						<span><?=$Lang['medical']['sleep']['AttendanceStatusExplain']?></span>
					</td>
				</tr-- >
				<tr>
					<td align="center" colspan="11">
						<!--<input type="submit" id="Save" name="Save" class="formbutton_v30 print_view " value="<?php echo $Lang['medical']['meal']['button']['save']; ?>"> -- >
						<input type="button" id="Save" name="Save" class="formbutton_v30 print_view " value="<?php echo $Lang['Btn']['Delete']; ?>">
						<input type="reset" id="Reset" name="Reset" class="formbutton_v30 print_view " value="<?php echo $Lang['medical']['meal']['button']['reset']; ?>">
						<!--<input type="button" id="Cancel" name="Cancel" class="formbutton_v30 print_view " value="<?php echo $Lang['medical']['meal']['button']['cancel']; ?>">-- >
					</td>	
				</tr>
			</tfoot-->
		</table>
	</div>
</form>
</td></tr></table>
<script>
	$("#checkALL").click(function(){
		$('input[name="deleteReacordList[]"]').attr('checked',$(this).attr('checked'));
	});

	function deleteRecord(){
		var countCheckedItem = $('input[name="deleteReacordList[]"]:checked').length;
		if( countCheckedItem < 1){
			alert(globalAlertMsg2); // 請最少選擇一個項目。
			return false;
		}
		if(!confirm(globalAlertMsg3)){ // 你是否確定要刪除項目?
			return false;
		}
		
		$.post("?t=management.ajax.studentLogDeleteItem", $('#studentLogForm').serialize(), function(res){
			var msg = '';
			if(res == '1'){
				msg = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
			}else{
				msg = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
			}
			window.location.href = '?t=management.problemRecord&Msg='+msg+'&startDate=<?=$startdate?>&endDate=<?=$enddate?>&type=studentLog';
		});
	}

	$('#printBtn').click(function(){
		window.open("?t=management.ajax.getProblemRecordStudentLog&startdate=<?=$startdate?>&enddate=<?=$enddate?>&isPrint=1");
	});
</script>