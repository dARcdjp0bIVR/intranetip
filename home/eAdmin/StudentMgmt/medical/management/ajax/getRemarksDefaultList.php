<?php 
/*
 * 	2017-07-27 [Cameron]
 * 		- add argument $presetRowHeight=15 to GetPresetText()
 * 
 * 	2017-07-25 [Cameron]
 * 		- add argument $append=1 to GetPresetText()
 *
 */	
	$jArrName = trim($_GET['jArrName']);
	$btnID = (int)$_GET['btnID'];
	$targetName = trim($_GET['targetName']);
	$jOtherFunction = trim($_GET['jOtherFunction']);
	$divIDPrefix = trim($_GET['divIDPrefix']);
	
	
	echo GetPresetText($jArrName,$btnID,$targetName,$jOtherFunction,$divIDPrefix, '', '1', '15');
?>