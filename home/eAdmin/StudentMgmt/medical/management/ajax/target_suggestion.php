<?
	$startDate = $StartDate ? $StartDate : date('Y-m-d');
	$yt = getAcademicYearAndYearTermByDate($startDate);
	$AcademicYearID = count($yt) ? $yt['AcademicYearID'] : Get_Current_Academic_Year_ID();
	$fcm = new form_class_manage();
	$classList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
	
	if ($TargetID) {
		$rs_target = $objMedical->getSchemeTarget('',$TargetID);
		if (count($rs_target)) {
			$rs_target = current($rs_target);
			$schemeID = $rs_target['SchemeID'];
			$selectedClass = $rs_target['Class'];
		}
	}
?>
<script language="javascript">
$().ready( function(){
	$("#submitBtn_tb").click(function(){
		$("input.actionBtn").attr("disabled", "disabled");
		$('#YearClassID option').attr('selected',true);
		
		if ($.trim($('#TargetTitle').val()) == '') {
			alert('<?=$Lang['medical']['award']['Warning']['InputSuggestion']?>');
			$("input.actionBtn").attr("disabled", "");
			return;
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: "?t=management.ajax.ajax&m=1&action=<?=$action?>",
				data : $("#form2").serialize(),		  
				success: update_scheme_target_list,
				error: show_ajax_error
			});
		}
		js_Hide_ThickBox();
	});			
});

function update_scheme_target_list(ajaxReturn){
	if (ajaxReturn != null && ajaxReturn.success){
		<? if ($action=='newTargetSave'):?>
			$('#SchemeTargetTable').append(ajaxReturn.html);
		<? else:?>
			$('#scheme_target_row_<?=$TargetID?>').replaceWith(ajaxReturn.html);
		<? endif;?>
		$('#SchemeTargetTable').css("display","");
	}
	$("input.actionBtn").attr("disabled", "");
}
</script>

<form name="form2" id="form2" method="post" action="">
	<table align="center" class="form_table_v30">
		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['ClassOrientedSuggestion']?><span class="tabletextrequire">*</span></td>
			<td class="tabletext"><input type="text" name="TargetTitle" id="TargetTitle" class="textboxtext" value="<?=intranet_htmlspecialchars($rs_target['TargetTitle'])?>"></td>
		</tr>

		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Header']['Menu']['Class']?></td>
			<td class="tabletext"><?=$objMedical->getClassSelection($selectedClass,$classList)?>
		</tr>
	
	</table>
	
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
	<p class="spacer"></p>
</div>
<?=$linterface->GET_HIDDEN_INPUT('SchemeID', 'SchemeID', $schemeID)?>
<?=$linterface->GET_HIDDEN_INPUT('TargetID', 'TargetID', $TargetID)?>
	
</form>

