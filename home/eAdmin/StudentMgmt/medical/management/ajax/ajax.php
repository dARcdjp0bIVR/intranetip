<?
/*
 * Log
 *
 * Description: output json format data
 *
 * 2019-06-21 [Cameron]
 * - add case getStudentBarcode [case #P162222]
 * - modify checkAddBowel and addBowelByBarcode
 * 
 * 2019-05-27 [Cameron]
 * - add case getBowelStatusByBarcode   [case #P160784]
 * 
 * 2019-01-04 [Cameron]
 * - add case getStaffEvents, getBuildingLevelForStaffEventSelection, getLocationForStaffEventSelection, getSelectedStaffEventsForStudentEvent,
 *      removeStaffEventOfStudentEvent
 *      
 * 2018-12-21 [Cameron]
 * - add case removeStudentEventOfStaffEvent, case staff_event in removeAttachment
 * 
 * 2018-12-19 [Cameron]
 * - add case getStaffList, getStaffEventTypeLev2Selection, getSelectedEventsForStaffEvent, getStaffEventBuildingLevel, getStaffEventLocation
 * 
 * 2018-05-28 [Cameron]
 * - add checking of student barcode, attendance status and duplicate record in checkAddBowel case
 * 
 * 2018-05-10 [Cameron]
 * - add case checkAddBowel, addBowelByBarcode, deleteBowel
 * - fix: add break to removeEventStudentLog()
 * 
 * 2018-04-27 [Cameron]
 * - set $remove_dummy_chars = false in getSelectedEventsForCase, getSelectedEventsForStudentLog
 *
 * 2018-03-29 [Cameron]
 * - add getSelectedStudentLogs, removeEventStudentLog
 *
 * 2018-03-28 [Cameron]
 * - add case getStudentLog, getStudentNameByClassForStudentLog, getStudentNameByGroupIDForStudentLog
 *
 * 2018-03-23 [Cameron]
 * - add case removeEventCase
 *
 * 2018-03-21 [Cameron]
 * - add case getStudentNameByGroupIDForEvent, getStudentNameByGroupIDForCase
 * - modify getStudentNameByClassForEvent and getStudentNameByClassForCase by adding Group filter
 *
 * 2018-03-20 [Cameron]
 * - add case getAffectedPersonNameByClassWithFilter, getAffectedPersonNameByGroupIDWithFilter, getAffectedPersonAvailableTeachingStaff,
 * getAffectedPersonAvailableNonTeachingStaff
 *
 * 2018-03-19 [Cameron]
 * - add case getStudentNameByGroupIDWithFilter
 *
 * 2018-02-27 [Cameron]
 * - add InputBy and DateInput for adding new followup
 * - don't allow to delete event follow-up (removeEventFollowup) if the Login User is not the creator and not super-admin [case #F135176]
 *
 * 2018-02-01 [Cameron] log record for trace when
 * (1) delete event followup in removeEventFollowup
 * (2) delete student performance record in removeStudentTarget
 *
 * 2017-10-13 [Cameron] add case getStudentNameByGroupID()
 *
 * 2017-07-28 [Cameron] set min-width and height for selection list
 *
 * 2017-06-16 [Cameron] create this file
 */
$characterset = 'utf-8';

header('Content-Type: text/html; charset=' . $characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$y = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
$remove_dummy_chars = ($junior_mck) ? false : true; // whether to remove new line, carriage return, tab and back slash

switch ($action) {
    
    case 'getStudentNameByClass':
        $ClassName = $_POST['ClassName'];
        $onChange = $_POST['onChange'] ? " onChange='" . $_POST['onChange'] . "'" : "";
        if (! empty($ClassName)) {
            $data = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
        } else {
            $data = array();
        }
        $x = getSelectByArray($data, "name='StudentID' id='StudentID'" . $onChange);
        $json['success'] = true;
        break;
    
    case 'getStudentNameByGroupID':
        $GroupID = $_POST['GroupID'];
        $onChange = $_POST['onChange'] ? " onChange='" . $_POST['onChange'] . "'" : "";
        if (! empty($GroupID)) {
            $data = $objMedical->getStudentNameListWClassNumberByGroupID($GroupID);
        } else {
            $data = array();
        }
        $x = getSelectByArray($data, "name='StudentID' id='StudentID'" . $onChange);
        $json['success'] = true;
        break;
    
    case 'getStudentNameByGroupIDWithFilter':
        $GroupID = $_POST['GroupID'];
        $excludeUserIdAry = $_POST['ExcludeStudentID'];
        if (! empty($GroupID)) {
            $data = $objMedical->getStudentNameListWClassNumberByGroupID($GroupID, $includeUserIdAry = '', $excludeUserIdAry);
        } else {
            $data = array();
        }
        $x = '<select name=AvailableStudentID[] id=AvailableStudentID style="min-width:200px; height:156px;" multiple>';
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $studentID = $data[$i]['UserID'];
            $studentName = $data[$i]['StudentName'];
            $x .= '<option value="' . $studentID . '">' . $studentName . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getStudentNameByClassForCase':
        $className = $_POST['ClassName'];
        $groupID = $_POST['GroupID'];
        
        if (! empty($className)) {
            if (! empty($groupID)) {
                $groupInfoAry = $objMedical->getStudentNameListWClassNumberByGroupID($groupID);
                $includeUserIdAry = BuildMultiKeyAssoc($groupInfoAry, 'UserID', $IncludedDBField = array(
                    'UserID'
                ), $SingleValue = 1);
            } else {
                $includeUserIdAry = '';
            }
            $data = $objMedical->getStudentNameListWClassNumberByClassName($className, $includeUserIdAry);
        } else {
            if (! empty($groupID)) {
                $data = $objMedical->getStudentNameListWClassNumberByGroupID($groupID);
            } else {
                $data = array();
            }
        }
        $x = getSelectByArray($data, "name='EventStudent' id='EventStudent' onChange='getEvents();'");
        $json['success'] = true;
        break;
    
    case 'getStudentNameByGroupIDForCase':
        $className = $_POST['ClassName'];
        $groupID = $_POST['GroupID'];
        
        if (! empty($groupID)) {
            if (! empty($className)) {
                $classInfoAry = $objMedical->getStudentNameListWClassNumberByClassName($className);
                $includeUserIdAry = BuildMultiKeyAssoc($classInfoAry, 'UserID', $IncludedDBField = array(
                    'UserID'
                ), $SingleValue = 1);
            } else {
                $includeUserIdAry = '';
            }
            $data = $objMedical->getStudentNameListWClassNumberByGroupID($groupID, $includeUserIdAry);
        } else {
            if (! empty($className)) {
                $data = $objMedical->getStudentNameListWClassNumberByClassName($className);
            } else {
                $data = array();
            }
        }
        $x = getSelectByArray($data, "name='EventStudent' id='EventStudent' onChange='getEvents();'");
        $json['success'] = true;
        break;
    
    case 'getStudentNameByClassForEvent':
        $className = $_POST['ClassName'];
        $groupID = $_POST['GroupID'];
        
        if (! empty($className)) {
            if (! empty($groupID)) {
                $groupInfoAry = $objMedical->getStudentNameListWClassNumberByGroupID($groupID);
                $includeUserIdAry = BuildMultiKeyAssoc($groupInfoAry, 'UserID', $IncludedDBField = array(
                    'UserID'
                ), $SingleValue = 1);
            } else {
                $includeUserIdAry = '';
            }
            $data = $objMedical->getStudentNameListWClassNumberByClassName($className, $includeUserIdAry);
        } else {
            if (! empty($groupID)) {
                $data = $objMedical->getStudentNameListWClassNumberByGroupID($groupID);
            } else {
                $data = array();
            }
        }
        $x = getSelectByArray($data, "name='CaseStudent' id='CaseStudent' onChange='getCases();'");
        $json['success'] = true;
        break;
    
    case 'getStudentNameByGroupIDForEvent':
        $className = $_POST['ClassName'];
        $groupID = $_POST['GroupID'];
        
        if (! empty($groupID)) {
            if (! empty($className)) {
                $classInfoAry = $objMedical->getStudentNameListWClassNumberByClassName($className);
                $includeUserIdAry = BuildMultiKeyAssoc($classInfoAry, 'UserID', $IncludedDBField = array(
                    'UserID'
                ), $SingleValue = 1);
            } else {
                $includeUserIdAry = '';
            }
            $data = $objMedical->getStudentNameListWClassNumberByGroupID($groupID, $includeUserIdAry);
        } else {
            if (! empty($className)) {
                $data = $objMedical->getStudentNameListWClassNumberByClassName($className);
            } else {
                $data = array();
            }
        }
        $x = getSelectByArray($data, "name='CaseStudent' id='CaseStudent' onChange='getCases();'");
        $json['success'] = true;
        break;
    
    case 'getStudentNameByClassWithFilter':
        $class_name = $_POST['ClassName'];
        $excludeUserIdAry = $_POST['ExcludeStudentID'];
        $data = $objMedical->getStudentNameByClassWithFilter($class_name, $excludeUserIdAry);
        
        $x = '<select name=AvailableStudentID[] id=AvailableStudentID style="min-width:200px; height:156px;" multiple>';
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $studentID = $data[$i]['UserID'];
            $studentName = $data[$i]['StudentName'];
            $x .= '<option value="' . $studentID . '">' . $studentName . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getAffectedPersonNameByClassWithFilter':
        $affectedPersonClassName = $_POST['AffectedPersonClassName'];
        $excludeUserIdAry = $_POST['ExcludeAffectedPersonIDs'];
        
        $x = '<select name=AvailableAffectedPersonID[] id=AvailableAffectedPersonID style="min-width:200px; height:156px;" multiple>';
        $data = $objMedical->getStudentNameByClassWithFilter($affectedPersonClassName, $excludeUserIdAry);
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $affectedPersonID = $data[$i]['UserID'];
            $affectedPersonName = $data[$i]['StudentName'];
            $x .= '<option value="' . $affectedPersonID . '">' . $affectedPersonName . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getAffectedPersonNameByGroupIDWithFilter':
        $affectedPersonGroupID = $_POST['AffectedPersonGroupID'];
        $excludeUserIdAry = $_POST['ExcludeAffectedPersonIDs'];
        
        if (! empty($affectedPersonGroupID)) {
            $data = $objMedical->getStudentNameListWClassNumberByGroupID($affectedPersonGroupID, $includeUserIdAry = '', $excludeUserIdAry);
        } else {
            $data = array();
        }
        $x = '<select name=AvailableAffectedPersonID[] id=AvailableAffectedPersonID style="min-width:200px; height:156px;" multiple>';
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $studentID = $data[$i]['UserID'];
            $studentName = $data[$i]['StudentName'];
            $x .= '<option value="' . $studentID . '">' . $studentName . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getAffectedPersonAvailableTeachingStaff':
        $excludeUserIdAry = $_POST['ExcludeAffectedPersonIDs'];
        
        $x = '<select name=AvailableAffectedPersonID[] id=AvailableAffectedPersonID style="min-width:200px; height:156px;" multiple>';
        if (count($excludeUserIdAry)) {
            $cond = "AND UserID NOT IN ('" . implode("','", $excludeUserIdAry) . "')";
        } else {
            $cond = '';
        }
        $data = $objMedical->getTeacher($teacherType = 1, $cond);
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $affectedPersonID = $data[$i]['UserID'];
            $affectedPersonName = $data[$i]['Name'];
            $x .= '<option value="' . $affectedPersonID . '">' . $affectedPersonName . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getAffectedPersonAvailableNonTeachingStaff':
        $excludeUserIdAry = $_POST['ExcludeAffectedPersonIDs'];
        
        $x = '<select name=AvailableAffectedPersonID[] id=AvailableAffectedPersonID style="min-width:200px; height:156px;" multiple>';
        if (count($excludeUserIdAry)) {
            $cond = "AND UserID NOT IN ('" . implode("','", (array) $excludeUserIdAry) . "')";
        } else {
            $cond = '';
        }
        
        $data = $objMedical->getTeacher($teacherType = 0, $cond);
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $affectedPersonID = $data[$i]['UserID'];
            $affectedPersonName = $data[$i]['Name'];
            $x .= '<option value="' . $affectedPersonID . '">' . $affectedPersonName . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getTeacherName':
        $teacherType = $_POST['TeacherType'];
        $excludeTeacherIDAry = $_POST['ExcludeTeacherID'];
        if (count($excludeTeacherIDAry)) {
            $cond = "AND UserID NOT IN ('" . implode("','", $excludeTeacherIDAry) . "')";
        } else {
            $cond = "";
        }
        $x = '<select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:156px;" multiple>';
        if ($teacherType == 1) {
            $teacherType = 1;
        } else {
            $teacherType = 0;
        }
        $data = $objMedical->getTeacher($teacherType, $cond);
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $staffID = $data[$i]['UserID'];
            $staffName = $data[$i]['Name'];
            $x .= '<option value="' . $staffID . '">' . $staffName . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getGroupName':
        $groupCategory = $_POST['GroupCategory'];
        $excludeGroupIDAry = $_POST['ExcludeGroupID'];
        
        $x = '<select name=AvailableGroupID[] id=AvailableGroupID style="min-width:200px; height:156px;" multiple>';
        $data = $objMedical->getAvailableGroupByCategory($groupCategory, $excludeGroupIDAry);
        for ($i = 0, $iMax = count($data); $i < $iMax; $i ++) {
            $groupID = $data[$i]['GroupID'];
            $groupTitle = $data[$i]['GroupTitle'];
            $x .= '<option value="' . $groupID . '">' . $groupTitle . '</option>';
        }
        $x .= '</select>';
        
        $json['success'] = true;
        break;
    
    case 'getBuildingLevel':
        $locationBuildingID = $_POST['LocationBuildingID'];
        $levelAry = $objMedical->getInventoryLevelArray($locationBuildingID);
        $firstTitle = ($_POST['FirstTitle'] == 'all') ? $Lang['SysMgr']['Location']['All']['Floor'] : '';
        $x = getSelectByArray($levelAry, 'Name="LocationLevelID" ID="LocationLevelID" onChange="changeLevel()"', '', 0, 0, $firstTitle);
        $json['success'] = true;
        break;
    
    case 'getLocation':
        $locationLevelID = $_POST['LocationLevelID'];
        $onChange = $_POST['onChange'] ? " onChange='" . $_POST['onChange'] . "'" : "";
        $firstTitle = ($_POST['FirstTitle'] == 'all') ? $Lang['SysMgr']['Location']['All']['Room'] : '';
        $locationAry = $objMedical->getInventoryLocationArray($locationLevelID);
        $x = getSelectByArray($locationAry, 'Name="LocationID" ID="LocationID"' . $onChange, '', 0, 0, $firstTitle);
        $json['success'] = true;
        break;
    
    case 'removeAttachment':
        switch ($table) {
            case 'event':
                $db_table = 'MEDICAL_EVENT_ATTACHMENT';
                $refField = 'EventID';
                break;
            case 'event_followup':
                $db_table = 'MEDICAL_EVENT_FOLLOWUP_ATTACHMENT';
                $refField = 'FollowupID';
                break;
            case 'minute':
                $db_table = 'MEDICAL_CASE_MINUTE_ATTACHMENT';
                $refField = 'MinuteID';
                break;
            case 'staff_event':
                $db_table = 'MEDICAL_STAFF_EVENT_ATTACHMENT';
                $refField = 'StaffEventID';
                break;
        }
        
        $result = array();
        if (! empty($table) && ! empty($FileID)) {
            // Get File to delete
            $rs = $objMedical->getAttachmentByID($db_table, $FileID);
            if (sizeof($rs) > 0) {
                $rs = current($rs);
                $FileID = $rs['FileID'];
                $refFieldVal = $rs[$refField];
                $FolderPath = $rs['FolderPath'];
                $FileHashName = $rs['FileHashName'];
                $file = $file_path . "/file/medical/edisc/" . $FolderPath . "/" . $FileHashName;
                $lfs = new libfilesystem();
                $result[] = $lfs->file_remove($file);
                $result[] = $objMedical->deleteAttachmentByID($db_table, $FileID);
                $x = $objMedical->getCurrentAttachmentLayout($table, $refFieldVal, $mode = 'edit');
            }
        }
        
        if (! in_array(false, $result)) {
            $json['success'] = true;
            
            switch ($table) {
                case 'event_followup':
                    $followup = $objMedical->getEventFollowup('', $refFieldVal);
                    if (count($followup) == 1) {
                        $y = $objMedical->getEventFollowupView($followup[0]);
                        $json['FollowupID'] = $refFieldVal;
                    }
                    break;
                
                case 'minute':
                    $minute = $objMedical->getCaseMinute('', $refFieldVal);
                    if (count($minute) == 1) {
                        $y = $objMedical->getCaseMinuteRow($refFieldVal, $rowNo);
                        $json['MinuteID'] = $refFieldVal;
                    }
                    break;
            }
        }
        break;
    
    case 'newFollowupSave':
        $ldb = new libdb();
        $result = array();
        
        $dataAry['EventID'] = $EventID;
        $dataAry['Handling'] = standardizeFormPostValue($Handling);
        $dataAry['StudentResponse'] = standardizeFormPostValue($StudentResponse);
        $dataAry['FollowupAction'] = standardizeFormPostValue($FollowupAction);
        $dataAry['Other'] = standardizeFormPostValue($Other);
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['DateInput'] = 'now()';
        $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_FOLLOWUP', $dataAry, array(), false);
        
        $ldb->Start_Trans();
        // # step 1: add to event followup
        $res = $ldb->db_db_query($sql);
        $result[] = $res;
        if ($res) {
            $followupID = $ldb->db_insert_id();
            
            // # step 2: add attachments
            $uploadResult = $objMedical->handleUploadFile($TargetFolder, $followupID, $table = 'event_followup');
            
            if ($uploadResult != 'AddSuccess') {
                $result[] = false;
            }
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'AddSuccess';
            $json['success'] = true;
            $followup = $objMedical->getEventFollowup($EventID, $followupID);
            if (count($followup) == 1) {
                $x = $objMedical->getEventFollowupView($followup[0]);
            }
        } else {
            $ldb->RollBack_Trans();
            if ($uploadResult == 'FileSizeExceedLimit') {
                $json['msg'] = 'FileSizeExceedLimit';
            } else {
                $json['msg'] = 'AddUnsuccess';
            }
        }
        
        break;
    
    case 'editFollowupSave':
        $ldb = new libdb();
        $result = array();
        
        $dataAry['Handling'] = standardizeFormPostValue($Handling);
        $dataAry['StudentResponse'] = standardizeFormPostValue($StudentResponse);
        $dataAry['FollowupAction'] = standardizeFormPostValue($FollowupAction);
        $dataAry['Other'] = standardizeFormPostValue($Other);
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $objMedical->UPDATE2TABLE('MEDICAL_EVENT_FOLLOWUP', $dataAry, array(
            'FollowupID' => $FollowupID
        ), false);
        
        $ldb->Start_Trans();
        // # step 1: update to event followup
        $result[] = $ldb->db_db_query($sql);
        
        // # step 2: add attachments
        $uploadResult = $objMedical->handleUploadFile($TargetFolder, $FollowupID, $table = 'event_followup');
        
        if ($uploadResult != 'AddSuccess') {
            $result[] = false;
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'UpdateSuccess';
            $json['success'] = true;
            $followup = $objMedical->getEventFollowup('', $FollowupID);
            if (count($followup) == 1) {
                $x = $objMedical->getEventFollowupView($followup[0]);
            }
        } else {
            $ldb->RollBack_Trans();
            if ($uploadResult == 'FileSizeExceedLimit') {
                $json['msg'] = 'FileSizeExceedLimit';
            } else {
                $json['msg'] = 'UpdateUnsuccess';
            }
        }
        
        break;
    
    case 'removeEventFollowup':
        $ldb = new libdb();
        $result = array();
        $dataAry = array();
        
        if ($FollowupID) {
            $isAllowedToDelete = true;
            $ldb->Start_Trans();
            
            $eventFollowupAry = $objMedical->getEventFollowup($eventID = '', $FollowupID);
            if (count($eventFollowupAry)) {
                $eventFollowup = current($eventFollowupAry);
                
                $isRequiredCheckSelfEvent = $objMedical->isRequiredCheckSelfEvent();
                if ($isRequiredCheckSelfEvent) {
                    $inputBy = $eventFollowup['InputBy'];
                    if ($inputBy != $_SESSION['UserID']) {
                        $isAllowedToDelete = false;
                        $result[] = false;
                    }
                }
                
                if ($isAllowedToDelete) {
                    $dataAry['TableName'] = 'MEDICAL_EVENT_FOLLOWUP';
                    $dataAry['RecordID'] = $eventFollowup['FollowupID'];
                    $dataAry['RefID'] = $eventFollowup['EventID'];
                    $dataAry['Summary'] = $eventFollowup['Handling'];
                    $dataAry['Detail'] = $eventFollowup['StudentResponse'];
                    $dataAry['OtherRef'] = $eventFollowup['FollowupAction'];
                    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                    $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
                    $result[] = $ldb->db_db_query($sql);
                }
            }
            
            if ($isAllowedToDelete) {
                $sql = "DELETE FROM MEDICAL_EVENT_FOLLOWUP WHERE FollowupID='" . $FollowupID . "'";
                $result[] = $ldb->db_db_query($sql);
                
                $attachment = $objMedical->getEventFollowupAttachment($FollowupID);
                if (count($attachment)) {
                    foreach ((array) $attachment as $rs) {
                        $FileID = $rs['FileID'];
                        $FolderPath = $rs['FolderPath'];
                        $FileHashName = $rs['FileHashName'];
                        $file = $file_path . "/file/medical/edisc/" . $FolderPath . "/" . $FileHashName;
                        $lfs = new libfilesystem();
                        $result[] = $lfs->file_remove($file);
                        $result[] = $objMedical->deleteAttachmentByID('MEDICAL_EVENT_FOLLOWUP_ATTACHMENT', $FileID);
                    }
                }
            }
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'DeleteSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'DeleteUnsuccess';
        }
        
        break;
    
    case 'getEvents': // get event list to choose for case
        
        include ($intranet_root . '/home/eAdmin/StudentMgmt/medical/management/templates/select_event.tmpl.php');
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
    
    case 'getCases': // get case list to choose for event
        
        include ($intranet_root . '/home/eAdmin/StudentMgmt/medical/management/templates/select_case.tmpl.php');
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
    
    case 'getSelectedEventsForCase':
        $eventID = $_POST['EventID'];
        $x = $objMedical->getEventRowForCase($eventID);
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
    
    case 'removeCaseEvent':
        $ldb = new libdb();
        $result = array();
        
        if ($CaseID && $EventID) {
            $ldb->Start_Trans();
            $sql = "DELETE FROM MEDICAL_CASE_EVENT WHERE CaseID='" . $CaseID . "' AND EventID='" . $EventID . "'";
            $result[] = $ldb->db_db_query($sql);
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'DeleteSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'DeleteUnsuccess';
        }
        
        break;
    
    case 'removeEventCase': // remove all relevant cases for an event
        $ldb = new libdb();
        $result = array();
        $eventID = $_POST['EventID'];
        if ($eventID) {
            $ldb->Start_Trans();
            $sql = "DELETE FROM MEDICAL_CASE_EVENT WHERE EventID='" . $eventID . "'";
            $result[] = $ldb->db_db_query($sql);
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'DeleteSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'DeleteUnsuccess';
        }
        
        break;
    
    case 'newMinuteSave':
        $ldb = new libdb();
        $result = array();
        
        if ($CaseID) {
            $dataAry['CaseID'] = $CaseID;
        }
        $dataAry['MeetingDate'] = $MeetingDate;
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $objMedical->INSERT2TABLE('MEDICAL_CASE_MINUTE', $dataAry, array(), false);
        
        $ldb->Start_Trans();
        // # step 1: add to case minute
        $res = $ldb->db_db_query($sql);
        $result[] = $res;
        if ($res) {
            $minuteID = $ldb->db_insert_id();
            
            // # step 2: add attachments
            $uploadResult = $objMedical->handleUploadFile($TargetFolder, $minuteID, $table = 'minute');
            
            if ($uploadResult != 'AddSuccess') {
                $result[] = false;
            }
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'AddSuccess';
            $json['success'] = true;
            $minute = $objMedical->getCaseMinute($caseID = '', $minuteID);
            
            if (count($minute) == 1) {
                $x = $objMedical->getCaseMinuteRow($minuteID);
            }
        } else {
            $ldb->RollBack_Trans();
            if ($uploadResult == 'FileSizeExceedLimit') {
                $json['msg'] = 'FileSizeExceedLimit';
            } else {
                $json['msg'] = 'AddUnsuccess';
            }
        }
        
        break;
    
    case 'editMinuteSave':
        $ldb = new libdb();
        $result = array();
        
        $dataAry['MeetingDate'] = $MeetingDate;
        $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
        $sql = $objMedical->UPDATE2TABLE('MEDICAL_CASE_MINUTE', $dataAry, array(
            'MinuteID' => $MinuteID
        ), false);
        
        $ldb->Start_Trans();
        // # step 1: update to meeting minutes
        $result[] = $ldb->db_db_query($sql);
        
        // # step 2: add attachments
        $uploadResult = $objMedical->handleUploadFile($TargetFolder, $MinuteID, $table = 'minute');
        
        if ($uploadResult != 'AddSuccess') {
            $result[] = false;
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'UpdateSuccess';
            $json['success'] = true;
            $minute = $objMedical->getCaseMinute($caseID = '', $MinuteID);
            
            if (count($minute) == 1) {
                $x = $objMedical->getCaseMinuteRow($MinuteID);
            }
        } else {
            $ldb->RollBack_Trans();
            if ($uploadResult == 'FileSizeExceedLimit') {
                $json['msg'] = 'FileSizeExceedLimit';
            } else {
                $json['msg'] = 'UpdateUnsuccess';
            }
        }
        
        break;
    
    case 'removeMeetingMinutes':
        $ldb = new libdb();
        $result = array();
        
        if ($MinuteID) {
            $ldb->Start_Trans();
            $sql = "DELETE FROM MEDICAL_CASE_MINUTE WHERE MinuteID='" . $MinuteID . "'";
            $result[] = $ldb->db_db_query($sql);
            
            $attachment = $objMedical->getCaseMinuteAttachment($MinuteID);
            if (count($attachment)) {
                foreach ((array) $attachment as $rs) {
                    $FileID = $rs['FileID'];
                    $FolderPath = $rs['FolderPath'];
                    $FileHashName = $rs['FileHashName'];
                    $file = $file_path . "/file/medical/edisc/" . $FolderPath . "/" . $FileHashName;
                    $lfs = new libfilesystem();
                    $result[] = $lfs->file_remove($file);
                    $result[] = $objMedical->deleteAttachmentByID('MEDICAL_CASE_MINUTE_ATTACHMENT', $FileID);
                }
            }
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'DeleteSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'DeleteUnsuccess';
        }
        
        break;
    
    case 'getSelectedCase':
        $caseID = $_POST['CaseID'];
        $x = $objMedical->getCaseRowForEvent($caseID);
        $json['success'] = true;
        break;
    
    case 'getNewTargetGroup':
        $groupID = $_POST['GroupID'];
        $titleExist = $_GET['TitleExist'];
        $rowID = $_GET['RowID'];
        
        if (! $titleExist) {
            $x .= $objMedical->getTargetGroupTitle($groupID);
        }
        $x .= $objMedical->getTargetGroupRow($groupID, $rowID);
        $json['success'] = true;
        break;
    
    case 'addSchemeGroup':
        $groupID = $_POST['GroupID'];
        if (! is_array($groupID)) {
            $groupID = array(
                $groupID
            );
        }
        $group = $objMedical->getGroupByGroupID($groupID, $orderBy = 'physical');
        
        $titleCol = '';
        for ($i = 0, $iMax = count($group); $i < $iMax; $i ++) {
            $titleCol .= '<th class="Group_' . $group[$i]['GroupID'] . '">' . $group[$i]['GroupTitle'] . '</th>';
        }
        $contentCol = '';
        for ($i = 0, $iMax = count($group); $i < $iMax; $i ++) {
            $contentCol .= '<td class="Group_' . $group[$i]['GroupID'] . '"><input type="checkbox" name="target_group_n[' . $group[$i]['GroupID'] . '][RowIDorTargetID]" value="1"></td>';
        }
        $x = $titleCol;
        $y = $contentCol;
        
        $json['success'] = true;
        break;
    
    case 'removeSchemeGroup':
        $GroupID = $_POST['GroupID'];
        $SchemeID = $_POST['SchemeID'];
        if (! is_array($GroupID)) {
            $GroupID = array(
                $GroupID
            );
        }
        $dataAry = array();
        
        $performanceExist = $objMedical->checkPerformanceExistByGroupID($SchemeID, $GroupID);
        if ($performanceExist) {
            $json['msg'] = 'RecordExist';
        } else {
            $originalScheme = $objMedical->getSchemes($SchemeID);
            if (count($originalScheme) == 1) {
                $originalScheme = current($originalScheme);
                $originalGroupIDAry = explode(',', $originalScheme['GroupID']);
            }
            $grouIDtoKeepAry = array_diff($originalGroupIDAry, $GroupID); // GroupID to keep
            $ldb = new libdb();
            $result = array();
            
            $ldb->Start_Trans();
            
            unset($dataAry);
            $dataAry['GroupID'] = implode(",", (array) $grouIDtoKeepAry);
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            $sql = $objMedical->UPDATE2TABLE('MEDICAL_AWARD_SCHEME', $dataAry, array(
                'SchemeID' => $SchemeID
            ), false);
            $result[] = $ldb->db_db_query($sql);
            
            $schemeTarget = $objMedical->getSchemeTarget($SchemeID);
            
            for ($i = 0, $iMax = count($schemeTarget); $i < $iMax; $i ++) {
                $originalGroupIDAry = explode(',', $schemeTarget[$i]['GroupID']); // selected GroupID for each target
                $grouIDtoKeepAry = array_diff($originalGroupIDAry, $GroupID); // GroupID to keep
                
                unset($dataAry);
                $dataAry['GroupID'] = implode(",", (array) $grouIDtoKeepAry);
                $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                $sql = $objMedical->UPDATE2TABLE('MEDICAL_AWARD_SCHEME_TARGET', $dataAry, array(
                    'TargetID' => $schemeTarget[$i]['TargetID']
                ), false);
                
                $result[] = $ldb->db_db_query($sql);
            }
            
            if (! in_array(false, $result)) {
                $ldb->Commit_Trans();
                $json['msg'] = 'DeleteSuccess';
                $json['success'] = true;
            } else {
                $ldb->RollBack_Trans();
                $json['msg'] = 'DeleteUnsuccess';
            }
        }
        break;
    
    case 'removeSchemeTarget':
        $ldb = new libdb();
        $result = array();
        $target = $objMedical->getSchemeTarget('', $TargetID);
        if (count($target) == 1) {
            $target = current($target);
            $schemeID = $target['SchemeID'];
            $groupID = $target['GroupID'];
            $groupIDAry = explode(',', $groupID);
            $performanceExist = $objMedical->checkPerformanceExistByGroupID($schemeID, $groupIDAry);
            if ($performanceExist) {
                $json['msg'] = 'RecordExist';
            } else {
                $ldb->Start_Trans();
                $sql = "DELETE FROM MEDICAL_AWARD_SCHEME_TARGET WHERE TargetID='" . $TargetID . "'";
                
                $result[] = $ldb->db_db_query($sql);
                
                if (! in_array(false, $result)) {
                    $ldb->Commit_Trans();
                    $json['msg'] = 'DeleteSuccess';
                    $json['success'] = true;
                } else {
                    $ldb->RollBack_Trans();
                    $json['msg'] = 'DeleteUnsuccess';
                }
            }
        }
        
        break;
    
    case 'savePerformance':
        $ldb = new libdb();
        $result = array();
        $dataAry = array();
        $SchemeID = $_POST['SchemeID'];
        $GroupID = $_POST['GroupID'];
        
        $rs_scheme = $objMedical->getSchemes($SchemeID);
        if (count($rs_scheme) == 1) {
            $rs_scheme = current($rs_scheme);
            $schemeDate = $objMedical->getSchemeDateAry($rs_scheme['StartDate'], $rs_scheme['EndDate']);
            $nrDate = count($schemeDate);
        }
        
        $ldb->Start_Trans();
        // # step 1: add new student target
        foreach ((array) $NewTarget as $sid => $target) {
            foreach ((array) $target as $row => $val) {
                unset($dataAry);
                $performance = '';
                foreach ((array) $NewScore[$sid][$row] as $col => $score) {
                    $performance .= ($performance == '') ? '' : '^~'; // column record splitter
                    $performance .= ($schemeDate[$col - 1]) ? $schemeDate[$col - 1] : '';
                    $performance .= ','; // date & score splitter
                    $performance .= ($score === '') ? '' : $score;
                }
                $dataAry['SchemeID'] = $SchemeID;
                $dataAry['StudentID'] = $sid;
                $dataAry['GroupID'] = $GroupID;
                $dataAry['Target'] = standardizeFormPostValue($val);
                $dataAry['Performance'] = $performance;
                $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
                
                $sql = $objMedical->INSERT2TABLE('MEDICAL_AWARD_SCHEME_STUDENT_TARGET', $dataAry, array(), false);
                $result[] = $ldb->db_db_query($sql);
            }
        }
        
        // # step 2: update student target
        foreach ((array) $Target as $StudentTargetID => $val) {
            unset($dataAry);
            $performance = '';
            foreach ((array) $Score[$StudentTargetID] as $col => $score) {
                $performance .= ($performance == '') ? '' : '^~'; // column record splitter
                $performance .= ($schemeDate[$col - 1]) ? $schemeDate[$col - 1] : '';
                $performance .= ','; // date & score splitter
                $performance .= ($score === '') ? '' : $score;
            }
            
            $dataAry['Target'] = standardizeFormPostValue($val);
            $dataAry['Performance'] = $performance;
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            
            $sql = $objMedical->UPDATE2TABLE('MEDICAL_AWARD_SCHEME_STUDENT_TARGET', $dataAry, array(
                'StudentTargetID' => $StudentTargetID
            ), false);
            $result[] = $ldb->db_db_query($sql);
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'SaveSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'SaveUnsuccess';
        }
        
        break;
    
    case 'removeStudentTarget':
        $ldb = new libdb();
        $result = array();
        $dataAry = array();
        $ldb->Start_Trans();
        
        $sql = "SELECT 		t.StudentTargetID,
							t.SchemeID,
							t.StudentID,
							t.GroupID,
							t.Target,
							t.Performance
				FROM 
							MEDICAL_AWARD_SCHEME_STUDENT_TARGET t
				WHERE 
							t.StudentTargetID='" . $StudentTargetID . "'";
        $studentTargetAry = $ldb->returnResultSet($sql);
        
        // log delete record
        if (count($studentTargetAry)) {
            $studentTarget = current($studentTargetAry);
            $dataAry['TableName'] = 'MEDICAL_AWARD_SCHEME_STUDENT_TARGET';
            $dataAry['RecordID'] = $studentTarget['StudentTargetID'];
            $dataAry['RefID'] = $studentTarget['SchemeID'];
            $dataAry['Summary'] = $studentTarget['Target'];
            $dataAry['Detail'] = $studentTarget['Performance'];
            $dataAry['StudentID'] = $studentTarget['StudentID'];
            $dataAry['OtherRef'] = $studentTarget['GroupID'];
            $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
            $sql = $objMedical->INSERT2TABLE('MEDICAL_DELETE_LOG', $dataAry, $condition = array(), $run = false);
            $result[] = $ldb->db_db_query($sql);
        }
        
        $sql = "DELETE FROM MEDICAL_AWARD_SCHEME_STUDENT_TARGET WHERE StudentTargetID='" . $StudentTargetID . "'";
        $result[] = $ldb->db_db_query($sql);
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'DeleteSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'DeleteUnsuccess';
        }
        
        break;
    
    case 'checkSchemeDateRangeChange':
        $ret = $objMedical->checkSchemeDateRangeChange($SchemeID, $_POST['StartDate'], $_POST['EndDate']);
        $x = $ret[0] ? '1' : '0'; // need fillBack
        $y = $ret[1] ? '1' : '0'; // involve delete
        $json['success'] = true;
        break;
    
    case 'getNewDrug':
        $rowID = $_GET['RowID'];
        $x = $objMedical->getDrugRow($rowID);
        $json['success'] = true;
        break;
    
    case 'getLastDrugTable':
        $studentID = $_POST['StudentID'];
        if ($studentID) {
            $x = $objMedical->getLastDrugByStudent($studentID);
        }
        $json['success'] = true;
        break;
    
    case 'getDrugTable': // existing drug by RevisitID
        $revisitID = $_POST['RevisitID'];
        $disabled = $_POST['disabled'];
        if ($revisitID) {
            $revisitDrug = $objMedical->getRevisitDrug($revisitID);
            $x = $objMedical->getDrugTable($revisitDrug, $disabled);
        }
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
    
    case 'getStudentLog':
        include ($intranet_root . '/home/eAdmin/StudentMgmt/medical/management/templates/selectStudentLog.tmpl.php');
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
    
    case 'getStudentNameByClassForStudentLog':
        $className = $_POST['className'];
        $groupID = $_POST['groupID'];
        
        if (! empty($className)) {
            if (! empty($groupID)) {
                $groupInfoAry = $objMedical->getStudentNameListWClassNumberByGroupID($groupID);
                $includeUserIdAry = BuildMultiKeyAssoc($groupInfoAry, 'UserID', $IncludedDBField = array(
                    'UserID'
                ), $SingleValue = 1);
            } else {
                $includeUserIdAry = '';
            }
            $data = $objMedical->getStudentNameListWClassNumberByClassName($className, $includeUserIdAry);
        } else {
            if (! empty($groupID)) {
                $data = $objMedical->getStudentNameListWClassNumberByGroupID($groupID);
            } else {
                $data = array();
            }
        }
        $x = getSelectByArray($data, "name='studentLogStudentID' id='studentLogStudentID' onChange='getStudentLog();'");
        $json['success'] = true;
        break;
    
    case 'getStudentNameByGroupIDForStudentLog':
        $className = $_POST['className'];
        $groupID = $_POST['groupID'];
        
        if (! empty($groupID)) {
            if (! empty($className)) {
                $classInfoAry = $objMedical->getStudentNameListWClassNumberByClassName($className);
                $includeUserIdAry = BuildMultiKeyAssoc($classInfoAry, 'UserID', $IncludedDBField = array(
                    'UserID'
                ), $SingleValue = 1);
            } else {
                $includeUserIdAry = '';
            }
            $data = $objMedical->getStudentNameListWClassNumberByGroupID($groupID, $includeUserIdAry);
        } else {
            if (! empty($className)) {
                $data = $objMedical->getStudentNameListWClassNumberByClassName($className);
            } else {
                $data = array();
            }
        }
        $x = getSelectByArray($data, "name='studentLogStudentID' id='studentLogStudentID' onChange='getStudentLog();'");
        $json['success'] = true;
        break;
    
    case 'getSelectedStudentLogs':
        $studentLogIDAry = $_POST['studentLogID'];
        $eventID = $_POST['EventID'];
        $x = $objMedical->getStudentLogRowForEvent($studentLogIDAry, $eventID);
        $json['success'] = true;
        break;
    
    case 'removeEventStudentLog':
        $ldb = new libdb();
        $result = array();
        
        $studentLogID = $_POST['studentLogID'];
        $eventID = $_POST['eventID'];
        
        if ($studentLogID && $eventID) {
            $ldb->Start_Trans();
            $sql = "DELETE FROM MEDICAL_EVENT_STUDENT_LOG WHERE StudentLogID='" . $studentLogID . "' AND EventID='" . $eventID . "'";
            $result[] = $ldb->db_db_query($sql);
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'DeleteSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'DeleteUnsuccess';
        }
        break;
        
    case 'getSelectedEventsForStudentLog':
        $eventID = $_POST['EventID'];
        $formId = $_GET['FormId'];
        $x = $objMedical->getEventRowForStudentLog($eventID, $formId);
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
        
    // check duplicate record and attendance status        
    case 'checkAddBowel':
        $studentBarcode= $_POST['studentBarcode'];
        $bowelStatusBarcode = $_POST['bowelStatusBarcode'];
        $bowelTime = $objMedical->getBowelTime4Db();
        $objBowelLog= new BowelLog($bowelTime);
        $studentID = $objMedical->getStudentIDByBarcode($studentBarcode);
        if (empty($studentID)) {
            $json['msg'] = 'studentBarcodeNotFound';
        }
        else {
            $libBowelStatus = new BowelStatus();
            $ldb = new libdb();
            $whereCriteria = "BarCode='".$ldb->Get_Safe_Sql_Query($bowelStatusBarcode)."' AND RecordStatus=1 ";
            $bowelStatusAry = $libBowelStatus->getAllStatus($whereCriteria);
            if (count($bowelStatusAry)) {
                $bowelStatusAry = current($bowelStatusAry);
                $statusID = $bowelStatusAry['StatusID'];
                $data['StudentID'] = $studentID;
                $data['BowelID'] = $statusID;
                $data['RecordDate'] = substr($bowelTime,0,10);
                $data['RecordTime'] = substr($bowelTime,11,8);
                $isBowelLogExist = $objBowelLog->isBowelLogExist($data);
                
                $json['BowelStatusID'] = $statusID;
                
                if ($isBowelLogExist) {
                    $json['msg'] = 'BowelLogExist';
                } else {
                    if(libMedicalShowInput::attendanceDependence('bowel')){
                        if (libMedicalShowInput::absentAllowedInput('bowel')) {
                            $json['msg'] = 'passChecking';
                        }
                        else {
                            $present = $objMedical->getAttendanceStatusOfUser($studentID, substr($bowelTime, 0, 10));
                            if ($present) {
                                $json['msg'] = 'passChecking';
                            } else {
                                $json['msg'] = 'absent';
                            }
                        }
                    }
                    else {
                        $json['msg'] = 'passChecking';
                    }
                }
            }
            else {
                $json['msg'] = 'bowelBarcodeNotFound';
            }
        }
        $json['success'] = true;
        break;
        
    case 'addBowelByBarcode':
        $studentBarcode = $_POST['studentBarcode'];
        $bowelStatus = $_POST['bowelStatus'];
        $remarks = $_POST['remarks'];
        $bowelTime = $objMedical->getBowelTime4Db();
        $objBowelLog= new BowelLog($bowelTime);
        
        $tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"].date('Y').'_'.date('m');
        $sql = $objBowelLog->getCreateDatabaseSQL($tableName);
        $ldb = new libdb();
        $ldb->db_db_query($sql);
        
        $studentID = $objMedical->getStudentIDByBarcode($studentBarcode);
        if ($studentID) {
            $objBowelLog->setUserID($studentID);
            $objBowelLog->setRecordTime($bowelTime);
            $objBowelLog->setBowelID($bowelStatus);
            $objBowelLog->setRemarks(intranet_htmlspecialchars(trim(stripslashes($remarks))));
            $objBowelLog->setModifiedBy($_SESSION['UserID']);
            $objBowelLog->setInputBy($_SESSION['UserID']);
            $result = $objBowelLog->save(false);
                
            if ($result) {
                $json['success'] = true;
                
                $bowelLogID = $result;
                unset($objBowelLog);
                $objBowelLog = new BowelLog($bowelTime, $bowelLogID);
                
                $studentInfo = $objMedical->getStudentInfoByID($studentID);
                if (! empty($studentInfo)) {
                    $studentName = $studentInfo['Name'];
                    $className = $studentInfo['ClassName'];
                    $stayOverNight= $studentInfo['stayOverNight'] ? $Lang['medical']['meal']['search']['sleepOption'][1] : $Lang['medical']['meal']['search']['sleepOption'][2];
                }
                else {
                    $studentName = '';
                    $className = '';
                    $stayOverNight = '';
                }
                
                if (! libMedicalShowInput::attendanceDependence('bowel')) {
                    $present = 1; // Always present if the school have not buy attendance module
                } else {
                    $present = $objMedical->getAttendanceStatusOfUser($studentID, substr($bowelTime, 0, 10));
                }
                $showAttendance = ($present == 1) ? $Lang['medical']['bowel']['parent']['AttendanceOption']['Present'] : $Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'];
                
                $objBowelStatus = new bowelStatus($bowelStatus);
                $bowelStatusName = stripslashes($objBowelStatus->getStatusName());
                $bowelStatusColor = $objBowelStatus->getColor();
                $bowelTime2Show = $objMedical->getBowelTime2Show($bowelTime);
                $remarks2Show = empty($remarks) ? '-' : nl2br(stripslashes($remarks));
                $dateModified = $objBowelLog->getDateModified();
                $lastModifiedBy = $objMedical->getUserNameByID($_SESSION['UserID'], $withTitle = false);
                
                $delBtnHTML = '<span class="table_row_tool">';
                $delBtnHTML .= '<a class="deleteBtn delete" title="'.$Lang['Btn']['Delete'].'" data-recordID="'.$bowelLogID.' " data-sid="'.$studentID.'"  ></a>';
                $delBtnHTML .= '</span>';
                
                $x = '<tr>
                        <td><span class="row_id"></span></td>
                        <td>'.$className.'</td>
                        <td>'.$studentName.'</td>
                        <td>'.$stayOverNight.'</td>
                        <td>'.$showAttendance.'</td>
                        <td><span class="colorBoxStyle2" style="background-color:'.$bowelStatusColor.'"></span>&nbsp;'.$bowelStatusName.'</td>
                        <td>'.$bowelTime2Show.'</td>
                        <td>'.$remarks2Show.'</td>
                        <td>'.$lastModifiedBy.'</td>
                        <td>'.$dateModified.'</td>
                        <td>'.$delBtnHTML.'</td>
                      </tr>';
            }
        }
        else {
            $json['msg'] = 'InvalidBarcode';
        }
        $remove_dummy_chars = false;
        break;
   
    case 'deleteBowel':
        $recordID = $_POST['recordID'];
        $recordDate = $_POST['recordDate'];
        $result = $objMedical->deleteBowelLogRecords(array($recordID), $recordDate, $byParent = false);
        $json['success'] = $result ? true : false;
        break;
        
    case 'getStaffList':
        $teacherType = $_POST['teacherType'];
        $staffList = $objMedical->getTeacher($teacherType);
        $x = getSelectByArray($staffList, 'Name="StaffID" ID="StaffID"', $selectedStaffID='');
        $json['success'] = true;
        break;
        
    case 'getStaffEventTypeLev2Selection':
        $staffEventTypeID = $_POST['StaffEventTypeID'];
        $onChange = $_POST['onChange'] ? " onChange='" . $_POST['onChange'] . "'" : "";
        $firstTitle = ($_POST['FirstTitle'] == 'all') ? $Lang['medical']['staffEvent']['AllSubcategory']: '';
        $staffEventTypeLev2List = $objMedical->getStaffEventTypeLev2($eventTypeLev2ID='', $staffEventTypeID,  $idAndNameOnly=true);
        $x = getSelectByArray($staffEventTypeLev2List, 'Name="StaffEventTypeLev2ID" ID="StaffEventTypeLev2ID"' . $onChange, '', 0, 0, $firstTitle);
        $json['success'] = true;
        $remove_dummy_chars = false;
        break;

    case 'getSelectedEventsForStaffEvent':
        $eventID = $_POST['EventID'];
        $x = $objMedical->getEventRowForStaffEvent($eventID);
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
  
    case 'getStaffEventBuildingLevel':
        $locationBuildingID = $_POST['StaffEventLocationBuildingID'];
        $levelAry = $objMedical->getInventoryLevelArray($locationBuildingID);
        $firstTitle = ($_POST['FirstTitle'] == 'all') ? $Lang['SysMgr']['Location']['All']['Floor'] : '';
        $x = getSelectByArray($levelAry, 'Name="StaffEventLocationLevelID" ID="StaffEventLocationLevelID" onChange="changeStaffEventBuildingLevel()"', '', 0, 0, $firstTitle);
        $json['success'] = true;
        break;
        
    case 'getStaffEventLocation':
        $locationLevelID = $_POST['StaffEventLocationLevelID'];
        $onChange = $_POST['onChange'] ? " onChange='" . $_POST['onChange'] . "'" : "";
        $firstTitle = ($_POST['FirstTitle'] == 'all') ? $Lang['SysMgr']['Location']['All']['Room'] : '';
        $locationAry = $objMedical->getInventoryLocationArray($locationLevelID);
        $x = getSelectByArray($locationAry, 'Name="StaffEventLocationID" ID="StaffEventLocationID"' . $onChange, '', 0, 0, $firstTitle);
        $json['success'] = true;
        break;
  
    case 'removeStaffEventOfStudentEvent':
    case 'removeStudentEventOfStaffEvent':
        $ldb = new libdb();
        $result = array();
        $staffEventID = $_POST['StaffEventID'];
        $studentEventID = $_POST['StudentEventID'];
        
        if ($staffEventID && $studentEventID) {
            $ldb->Start_Trans();
            $sql = "DELETE FROM MEDICAL_EVENT_STAFF_EVENT WHERE StaffEventID='" . $staffEventID . "' AND EventID='" . $studentEventID . "'";
            $result[] = $ldb->db_db_query($sql);
        }
        
        if (! in_array(false, $result)) {
            $ldb->Commit_Trans();
            $json['msg'] = 'DeleteSuccess';
            $json['success'] = true;
        } else {
            $ldb->RollBack_Trans();
            $json['msg'] = 'DeleteUnsuccess';
        }
        break;

    case 'getStaffEvents':
        include ($intranet_root . '/home/eAdmin/StudentMgmt/medical/management/templates/selectStaffEvent.tmpl.php');
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
        
    case 'getBuildingLevelForStaffEventSelection':
        $locationBuildingID = $_POST['SrhLocationBuildingID'];
        $levelAry = $objMedical->getInventoryLevelArray($locationBuildingID);
        $firstTitle = ($_POST['FirstTitle'] == 'all') ? $Lang['SysMgr']['Location']['All']['Floor'] : '';
        $x = getSelectByArray($levelAry, 'Name="SrhLocationLevelID" ID="SrhLocationLevelID" onChange="changeSrhBuildingLevel()"', '', 0, 0, $firstTitle);
        $json['success'] = true;
        break;
        
    case 'getLocationForStaffEventSelection':
        $locationLevelID = $_POST['SrhLocationLevelID'];
        $onChange = $_POST['onChange'] ? " onChange='" . $_POST['onChange'] . "'" : "";
        $firstTitle = ($_POST['FirstTitle'] == 'all') ? $Lang['SysMgr']['Location']['All']['Room'] : '';
        $locationAry = $objMedical->getInventoryLocationArray($locationLevelID);
        $x = getSelectByArray($locationAry, 'Name="SrhLocationID" ID="SrhLocationID"' . $onChange, '', 0, 0, $firstTitle);
        $json['success'] = true;
        break;
  
    case 'getSelectedStaffEventsForStudentEvent':
        $staffEventID = $_POST['StaffEventID'];
        $x = $objMedical->getStaffEventRowForStudentEvent($staffEventID);
        $remove_dummy_chars = false;
        $json['success'] = true;
        break;
 
    case 'getBowelStatusByBarcode':
        $barcode = $_POST['barcode'];
        $ldb = new libdb();
        $libBowelStatus = new BowelStatus();
        $whereCriteria = "BarCode='".$ldb->Get_Safe_Sql_Query($barcode)."'";
        $bowelStatusAry = $libBowelStatus->getAllStatus($whereCriteria);
        if (count($bowelStatusAry)) {
            $bowelStatusAry = current($bowelStatusAry);
            $statusID = $bowelStatusAry['StatusID'];
            $statusName = $bowelStatusAry['StatusName'];
            $color = $bowelStatusAry['Color'];
            $x = '<span class="colorBoxStyle" style="background-color:'.$color.';">&nbsp;&nbsp;&nbsp;</span><span>&nbsp;'.  intranet_htmlspecialchars($statusName).'</span>';
            $json['statusID'] = $statusID;
        }
        else {
            $json['msg'] = 'barcodeNotFound';
        }
        $json['success'] = true;
        break;
        
        
    case 'getStudentBarcode':
        $barcode = $_POST['barcode'];
        $ldb = new libdb();
        $sql = "SELECT Barcode FROM INTRANET_USER WHERE Barcode='".$barcode."'";        // verify if the incput barcode exist
        $result = $ldb->returnResultSet($sql);
        if (count($result)) {
            $json['Barcode'] = $result[0]['Barcode'];
        }
        else {
            $json['msg'] = 'barcodeNotFound';
        }
        $json['success'] = true;
        break;
        
}

if ($remove_dummy_chars) {
    $x = remove_dummy_chars_for_json($x);
    $y = remove_dummy_chars_for_json($y);
}

// if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
// $x = convert2unicode($x,true,1);
// }

$json['html'] = $x;
$json['html2'] = $y;
echo $ljson->encode($json);

?>