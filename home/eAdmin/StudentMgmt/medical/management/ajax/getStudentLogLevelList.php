<?php
/*
 * 2018-04-27 [Cameron]
 * - apply stripslashes to Lev2Name in option list
 *
 * 2015-12-04 [Cameron]
 * - remove pass by reference in getEventLogInputUI() to support php 5.4
 */
switch ($action) {
    case 2: // List level 2
        $objLogLev2 = new studentLogLev2();
        $Lev2List = $objLogLev2->getActiveStatus($orderCriteria = 'Lev2Code', $levelOneId = $_POST['data']);
        
        if (count($Lev2List) > 0) {
            foreach ((array) $Lev2List as $Lev2) {
                $selected = '';
                if ($Lev2['IsDefault']) {
                    $selected = 'selected';
                }
                $html .= '<option value="' . $Lev2['Lev2ID'] . '" ' . $selected . '>' . stripslashes($Lev2['Lev2Name']) . '</option>';
            }
        } else {
            $html .= '<option value= "" >' . $Lang['General']['NoRecordFound'] . '</option>' . "\n";
        }
        
        echo $html;
        break;
    case '3': // List level 3
        $Id = $_POST['Id'];
        // $newPartsCount = $_POST['newPartsCount'];
        
        // Level 3 List
        $Lev3List = $objMedical->getLev3List();
        
        $deleteBodyPartsBtn = "<span class='table_row_tool'><a class='deletePartsBtn delete_dim' title='" . $Lang['Btn']['Delete'] . "'></a></span>";
        
        foreach ((array) $Lev3List as $Lev3) {
            $valueArray['lev3List'][$Lev3['Lev3ID']] = $Lev3['Lev3Name'];
        }
        
        $selectedLev3ID = $Lev3List[0]['Lev3ID'];
        
        // Level 4 List
        $Lev4List = $objMedical->getLev4List($selectedLev3ID);
        
        $html = '';
        
        foreach ((array) $Lev4List as $Lev4) {
            // $valueSelected no use
            $Lev4Html .= $objMedical->getCheckBox($id = "event[" . $Id . "][level4][" . $selectedLev3ID . "][" . $Lev4['Lev4ID'] . "]", $name = "event[" . $Id . "][level4][" . $selectedLev3ID . "][" . $Lev4['Lev4ID'] . "]", $name = $Lev4['Lev4Name'], $value = $Lev4['Lev4ID'], $valueSelected = '', // no use
$class = "level4", $OtherMembers = "$selected");
        }
        $html .= $objMedical->getSelectionBox($id = "event[" . $Id . "][level3][]", $name = "event[" . $Id . "][level3][]", $valueArray['lev3List'], $valueSelected = $item['lev3ID'], $class = "level3", $otherMembers = "data-Id='$Id'");
        $html .= "<span class='lev4ListArea'>";
        $html .= <<<HTML
		<script>
			\$('.level3').change(function(){
				var objList = \$(this).next();
				var Id = \$(this).attr('data-Id');
				\$.ajax({
					url : "?t=management.ajax.getStudentLogLevelList",
					type : "POST",
					data : 'data='+\$(this).val()+'&action=4'+'&Id='+Id,
					success : function(msg) {
						objList.html(msg);
					}
				});	
			});
		</script>
HTML;
        $html .= $Lev4Html;
        $html .= "</span>";
        $html .= $deleteBodyPartsBtn;
        $html .= "<br />";
        
        echo $html;
        break;
    case 4: // List level 4
        $selectedLev3ID = $_POST['data'];
        $Lev4List = $objMedical->getLev4List($selectedLev3ID);
        foreach ((array) $Lev4List as $Lev4) {
            // $valueSelected no use
            $html .= $objMedical->getCheckBox($id = "event[" . $Id . "][level4][" . $selectedLev3ID . "][" . $Lev4['Lev4ID'] . "]", $name = "event[" . $Id . "][level4][" . $selectedLev3ID . "][" . $Lev4['Lev4ID'] . "]", $name = $Lev4['Lev4Name'], $value = $Lev4['Lev4ID'], $valueSelected = '', // no use
$class = "level4", $OtherMembers = "$selected");
        }
        echo $html;
        break;
    case 'newField':
        $html = $objMedical->getEventLogInputUI($Id = $_POST['Id'], $linterface, $eventDetail = '');
        // $html .= "<a class='deleteEventBtn'> ".$Lang['Btn']['Delete']."</a>";
        
        echo $html;
        break;
    default:
        break;
}

?>