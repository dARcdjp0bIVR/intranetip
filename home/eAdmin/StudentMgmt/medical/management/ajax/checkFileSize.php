<?php 
// Using:

/*
 * Usage: POST data to 'index.php' with data
 * {
 * 	t: (this file),
 *  fileMaxSize: (the max file size for checking)
 * }
 * 
 * <input type="file" id="event[n1][fileUploaded][]" class="event[n1][fileUploaded][]" name="event[n1][fileUploaded][]">
 */ 

//	debug_r($_FILES);
	$fileMaxSize = $_POST['fileMaxSize'];
	$isPass = true;
	foreach($_FILES as $_file=>$fileDetails){
		foreach($fileDetails['size'] as $_elementName=>$_details){
			foreach($_details['fileUploaded'] as $index=>$size){
				if($size > $fileMaxSize){
					echo $_elementName;
					echo ",";
					echo $index;
					echo "\n";
					$isPass = false;
				}
			}
		}
	}
	
	if($isPass){
		echo '1';
	}else{
//		echo '0';
	}
?>