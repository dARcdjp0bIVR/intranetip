<?php
// using:
/*
 * 2018-12-27 Cameron
 * - add access right checking
 * 
 * 2018-03-27 Cameron
 * - create this file
 */

$viewRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_VIEW');
$editRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_EDIT');
$deleteRight = $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_DELETE');
$isStaffEventStaff = $objMedical->isStaffEventStaff($_SESSION['UserID']);

if ((!$viewRight && !$isStaffEventStaff) || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}

$arrCookies = array();
$arrCookies[] = array(
    "ck_page_size",
    "numPerPage"
);
$arrCookies[] = array(
    "ck_right_page_number",
    "pageNo"
);
$arrCookies[] = array(
    "ck_right_page_order",
    "order"
);
$arrCookies[] = array(
    "ck_right_page_field",
    "field"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventStaffID",
    "StaffID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventTypeID",
    "EventTypeID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventTypeLev2ID",
    "EventTypeLev2ID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventLocationBuildingID",
    "LocationBuildingID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventLocationLevelID",
    "LocationLevelID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventLocationID",
    "LocationID"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventReporter",
    "EventReporter"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventDate",
    "EventDate"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventDateStart",
    "EventDateStart"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventDateEnd",
    "EventDateEnd"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventTimeStartHour",
    "TimeStartHour"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventTimeStartMin",
    "TimeStartMin"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventTimeEndHour",
    "TimeEndHour"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventTimeEndMin",
    "TimeEndMin"
);
$arrCookies[] = array(
    "ck_eAdmin_Medical_StaffEventKeyword",
    "keyword"
);

if (isset($clearCoo) && $clearCoo == 1) {
    clearCookies($arrCookies);
} else {
    updateGetCookies($arrCookies);
}

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order; // default descending
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);

$AcademicYearID = Get_Current_Academic_Year_ID();
$cond = '';

if ($StaffID) {
    $cond .= " AND s.UserID='" . $StaffID . "'";
}

if ($EventTypeID) {
    $cond .= " AND e.EventTypeID='" . $EventTypeID . "'";
}

if ($EventTypeLev2ID) {
    $cond .= " AND e.EventTypeLev2ID='" . $EventTypeLev2ID. "'";
}

if ($LocationBuildingID) {
    $cond .= " AND e.LocationBuildingID='" . $LocationBuildingID . "'";
}

if ($LocationLevelID) {
    $cond .= " AND e.LocationLevelID='" . $LocationLevelID . "'";
}

if ($LocationID) {
    $cond .= " AND e.LocationID='" . $LocationID . "'";
}

if ($EventReporter) {
    $cond .= " AND e.LastModifiedBy='" . $EventReporter . "'";
}

if ($EventDate) {
    if ($EventDateStart) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.StartTime)>='" . $EventDateStart . " " . $TimeStartHour . ":" . $TimeStartMin . ":00'";
    }
    
    if ($EventDateEnd) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.EndTime)<='" . $EventDateEnd . " " . $TimeEndHour . ":" . $TimeEndMin . ":00'";
    }
}

$locationField = Get_Lang_Selection("NameChi", "NameEng");
$staffName = getNameFieldByLang('s.');
$createdBy = getNameFieldByLang2('cb.');
$updatedBy = getNameFieldByLang2('ub.');

$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if ($keyword != "") {
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND {$staffName} LIKE '%$kw%'";
    unset($kw);
}

$isRequiredCheckSelfStaffEvent = $objMedical->isRequiredCheckSelfStaffEvent();
$checkSelfDeleteStr = "IF(e.InputBy=" . $_SESSION['UserID'] . ",1,0)";
$dataDeleteAllowValue = $isRequiredCheckSelfStaffEvent? $checkSelfDeleteStr : '1';

if (!$viewRight && $isStaffEventStaff) {
    $cond .= " AND e.StaffID='".$_SESSION['UserID']."'";
    $checkBoxField = '';
    $extraCol = 1;
}
else if ($viewRight && !$editRight && !$deleteRight) {
    $checkBoxField = '';
    $extraCol = 1;
}
else {
    $checkBoxField = ", CONCAT('<input type=\'checkbox\' name=\'StaffEventID[]\' id=\'StaffEventID[]\' data-deleteAllow=\'',$dataDeleteAllowValue,'\' value=\'', e.`StaffEventID`,'\'>') ";
    $extraCol = 2;
}

$sql = "SELECT 
				CONCAT('<a href=\"?t=management.staff_event_view&StaffEventID=',e.`StaffEventID`,'\">',e.EventDate,'</a>'),
				" . $staffName. " AS StaffName,
				DATE_FORMAT(e.StartTime,'%k:%i') AS StartTime,
				DATE_FORMAT(e.EndTime,'%k:%i') AS EndTime,
                CASE 
                    WHEN e.EventTypeID=0 AND e.EventTypeLev2ID=0 THEN '-'
                    WHEN e.EventTypeID=0 AND e.EventTypeLev2ID>0 THEN et2.EventTypeLev2Name
                    WHEN e.EventTypeID>0 AND e.EventTypeLev2ID=0 THEN et.EventTypeName
                    ELSE CONCAT(et.EventTypeName,' > ', et2.EventTypeLev2Name)
                END AS EventType,
				CASE 
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN '-'
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN loc." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN lvl." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID<>0 THEN CONCAT(lvl." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN b." . $locationField . "
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN CONCAT(b." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ")
					ELSE CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ",' > ',loc." . $locationField . ")
				END AS Location,
				" . $createdBy. " as CreatedBy,
                " . $updatedBy. " as UpdatedBy,
                e.DateModified".$checkBoxField."				
		FROM
				MEDICAL_STAFF_EVENT e
		INNER JOIN 
				INTRANET_USER s ON s.UserID=e.StaffID
		LEFT JOIN MEDICAL_STAFF_EVENT_TYPE et ON et.EventTypeID=e.EventTypeID
        LEFT JOIN MEDICAL_STAFF_EVENT_TYPE_LEV2 et2 ON et2.EventTypeLev2ID=e.EventTypeLev2ID
		LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=e.LocationBuildingID
		LEFT JOIN INVENTORY_LOCATION_LEVEL lvl ON lvl.LocationLevelID=e.LocationLevelID
		LEFT JOIN INVENTORY_LOCATION loc ON loc.LocationID=e.LocationID
        LEFT JOIN INTRANET_USER cb ON cb.UserID=e.InputBy
        LEFT JOIN INTRANET_USER ub ON ub.UserID=e.LastModifiedBy
		WHERE 1 " . $cond;
 //debug_r($sql);

$li->field_array = array(
    "EventDate",
    "StaffName",
    "StartTime",
    "EndTime",
    "EventType",
    "Location",
    "CreatedBy",
    "UpdatedBy",
    "DateModified"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + $extraCol;
$li->title = $eDiscipline["Record"];
$li->column_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->wrap_array = array(
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='10%' >" . $li->column($pos ++, $Lang['medical']['staffEvent']['tableHeader']['EventDate']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['staffEvent']['tableHeader']['StaffName']) . "</th>\n";
$li->column_list .= "<th width='8%' >" . $li->column($pos ++, $Lang['medical']['staffEvent']['tableHeader']['StartTime']) . "</th>\n";
$li->column_list .= "<th width='8%' >" . $li->column($pos ++, $Lang['medical']['staffEvent']['tableHeader']['EndTime']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['staffEvent']['tableHeader']['EventType']) . "</th>\n";
$li->column_list .= "<th width='15%' >" . $li->column($pos ++, $Lang['medical']['staffEvent']['tableHeader']['Place']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['staffEvent']['tableHeader']['ReportPerson']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['general']['lastModifiedBy']) . "</th>\n";
$li->column_list .= "<th width='9%' >" . $li->column($pos ++, $Lang['medical']['general']['dateModified']) . "</th>\n";
if ($editRight || $deleteRight) {
    $li->column_list .= "<th width='1'>" . $li->check("StaffEventID[]") . "</th>\n";
}

if ($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_ADD')) {
    $toolbar = $linterface->GET_LNK_NEW("javascript:checkGet2(document.form1,'?t=management.new_staff_event')", $button_new, "", "", "", 0);
}

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

// ############ start Filters

// Event Type Filter
$eventTypeAry = $objMedical->getStaffEventType();
$eventTypeFilter = getSelectByArray($eventTypeAry, "name='EventTypeID' onChange='this.form.submit();'", $EventTypeID, 0, 0, $Lang['medical']['event']['AllCategory']);

// Event Type Lev2 Filter
if ($EventTypeID) {
    $eventTypeLev2Ary = $objMedical->getStaffEventTypeLev2('',$EventTypeID, true);
    $eventTypeLev2Filter = getSelectByArray($eventTypeLev2Ary, "name='EventTypeLev2ID' id='EventTypeLev2ID' onChange='this.form.submit();'", $EventTypeLev2ID, 0, 0, $Lang['medical']['staffEvent']['AllSubcategory']);
}

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingFilter = getSelectByArray($buildingAry, "Name='LocationBuildingID' ID='LocationBuildingID' onChange='changeBuilding();'", $LocationBuildingID, 0, 0, $Lang['SysMgr']['Location']['All']['Building']);

if ($LocationBuildingID) {
    $levelAry = $objMedical->getInventoryLevelArray($LocationBuildingID);
    $levelFilter = getSelectByArray($levelAry, "Name='LocationLevelID' ID='LocationLevelID' onChange='changeBuildingLevel();'", $LocationLevelID, 0, 0, $Lang['SysMgr']['Location']['All']['Floor']);
}

if ($LocationLevelID) {
    $locationAry = $objMedical->getInventoryLocationArray($LocationLevelID);
    $locationFilter = getSelectByArray($locationAry, "Name='LocationID' ID='LocationID' onChange='this.form.submit();'", $LocationID, 0, 0, $Lang['SysMgr']['Location']['All']['Room']);
}

$staffFilter= $objMedical->getTeacherOptionsByType($StaffID);

$teacherAry = $objMedical->getTeacher();
$reporterFilter = getSelectByArray($teacherAry, "name='EventReporter' onChange='this.form.submit();'", $EventReporter, 0, 0, $Lang['medical']['case']['AllReporter']);

$EventDateStart = $EventDateStart ? $EventDateStart : substr(getStartDateOfAcademicYear($AcademicYearID), 0, 10);
$EventDateEnd = $EventDateEnd ? $EventDateEnd : substr(getEndDateOfAcademicYear($AcademicYearID), 0, 10);
// ############ end Filters

$CurrentPage = "ManagementStaffEvent";
$CurrentPageName = $Lang['medical']['menu']['staffEvent'];
$TAGS_OBJ[] = array(
    $Lang['medical']['menu']['staffEvent'],
    "",
    0
);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();
if ($returnMsgKey == 'EventTypeNotSetup') {
    $returnMsg = $Lang['medical']['staffEvent']['ReturnMessage']['EventTypeNotSetup'];
}elseif ($returnMsgKey == 'DeleteNonSelfRecord') {
    $returnMsg = $Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'];
}else {
    $returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
}
$linterface->LAYOUT_START($returnMsg);

include_once ('templates/staffEventList.tmpl.php');
$linterface->LAYOUT_STOP();
?>