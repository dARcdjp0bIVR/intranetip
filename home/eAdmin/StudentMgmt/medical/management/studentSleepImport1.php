<?php
// modify :  
/*
 *  2019-05-22 Cameron
 *      - security fix: apply getEncryptedText to file and path
 * 
 * 	Date: 2017-07-27 [Cameron]
 * 		- change $plugin['medical_module']['discipline'] to $sys_custom['medical']['swapBowelAndSleep']
 * 
 * 	2017-07-18 [Cameron]
 * 		- add $plugin['medical_module']['discipline'] to change sample file [case #F116540]
 */
//http://192.168.0.146:31002/home/eAdmin/StudentMgmt/medical/?t=management.studentSleepImport1
$CurrentPage = "ManagementStudentSleep";
$TAGS_OBJ[] = array($Lang['medical']['menu']['studentSleep'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_MANAGEMENT')){
	header("Location: /");
	exit();
}
$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=1, $CustStepArr='');

$Title = $Lang['medical']['general']['ImportData'];

$PAGE_NAVIGATION[] = array($Title,"");



$linterface->LAYOUT_START($Msg);
//echo $h_stepUI;

$path = getEncryptedText("home/eAdmin/StudentMgmt/medical/management/CSV/");
$sample_file_name = $sys_custom['medical']['swapBowelAndSleep'] ? 'StudentBowelSample2.csv' : 'StudentSleepSample.csv';
$sample_file_name = getEncryptedText($sample_file_name);
$sample_file = "/home/eAdmin/StudentMgmt/medical/?t=tools.downloadCSV&file=".$sample_file_name."&path=".$path;
$csvFile = "<a class='tablelink' href='". $sample_file ."' target='_blank'>". $Lang['General']['ClickHereToDownloadSample'] ."</a>";

$csv_format = '';
$importRemarks = array_values($Lang['medical']['studentSleep']['importRemarks']);
$delim = '';
$requiredField = array(0,1,2,3,4);
$remarkHint = array(3,4);
for($i=0, $iMax = count($importRemarks); $i < $iMax; $i++)
{
	$csv_format .= $delim . $Lang['SysMgr']['Homework']['Column'] . ' ' . ($i+1) . ' : ';
	if(in_array($i,$requiredField))
	{
		$csv_format .= $linterface->RequiredSymbol();
	}
	$csv_format .= $importRemarks[$i];
	if(in_array($i,$remarkHint)){ 
		$csv_format .= "<a href='javascript:void(0)' class='importHint' data-hintId='{$i}'> [{$Lang['medical']['general']['importHint']['clickToCheck']}{$importRemarks[$i]}]</a>";
	}
	$delim = '<br />';
}

?>
<form method="POST" name="frm1" id="frm1" action="index.php" enctype="multipart/form-data" onsubmit="return CheckForm()">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?=$h_stepUI;?></td>
</tr>

<tr>
	<td>
		<table width="80%" border="0" cellspacing="0" cellpadding="0" align="center">
		            <tr> 
            	<td><br />


					<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">			
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['SourceFile']?> <span class="tabletextremark"><?=$Lang['General']['CSVFileFormat']?></span></td>
							<td class="tabletext"><input class="file" type="file"  id = "csvfile" name="csvfile"></td>
						</tr>
<!--						
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['AccountMgmt']['Remarks']?> </td>
							<td class="tabletext"><?=$ImportRemark?></td>
						</tr>
-->						
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
							<td class="tabletext"><?=$csvFile?></td>
						</tr>
						<tr>
							<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
							<td class="tabletext"><?=$csv_format?></td>
						</tr>
					</table>
					<span class="tabletextremark"><?=$i_general_required_field?></span>
					<br/><br/>

				</td></tr>
				<tr>
	                <td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		        </tr>
            <tr>
				<td align="center">
				<br/>
				<input type="hidden" name="t" value="management.studentSleepImport2"/>
					<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
					&nbsp;
					<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.location='?t=management.sleep'","back_btn"," class='formbutton' ") ?>				
				</td>
			</tr>
		</table>
	</td>
</tr>

</table>


</form>

<div id="importHintResult" style="position: absolute"></div>

<script>
function CheckForm()
{
	var filename = $("#csvfile").val()
	var fileext = filename.substring(filename.lastIndexOf(".")).toLowerCase()
	if(fileext!=".csv" && fileext!=".txt")
	{	
		alert("<?=$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT']?>");
		return false;
	}
	return true;	
}

$('.importHint').click(function(){
	var fieldID = $(this).attr('data-hintId');
	var posTop = $(this).position().top;
	var posLeft = $(this).position().left + $(this).width();
	$.get('?t=management.ajax.getStudentSleepImportHint', {
		'fieldID': fieldID
	}, function(res){
		$('#importHintResult')
			.html(res)
			.show()
			.css({
				top: posTop, 
				left: posLeft
			});
	});
});

function Hide_Window(){
	$('#importHintResult').hide();
}
</script>
<?php

$linterface->LAYOUT_STOP();
?>