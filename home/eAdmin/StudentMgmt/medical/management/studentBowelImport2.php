<?php
// modify : 
/*
 * 	Log
 * 
 * 	2017-07-26 [Cameron]
 * 		- create this file
 */
include_once($PATH_WRT_ROOT."includes/cust/medical/libStudentBowelImport.php");

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='BOWEL_MANAGEMENT') || !$plugin['medical_module']['bowel']){
	header("Location: /");
	exit();
}

$libimport = new libimporttext();
$libfs = new libfilesystem();

$name = $_FILES['csvfile']['name'];


if (!$libimport->CHECK_FILE_EXT($name))
{
	intranet_closedb();
	exit();
}

#######################################################
### move to temp folder first for others validation ###
#######################################################
$TargetFilePath = $libfs->Copy_Import_File_To_Temp_Folder('medical/student_bowel/', $csvfile, $name);

##################################
### Validate file header format ##
##################################

$studentLogImport = new libStudentBowelImport();
$data = $studentLogImport->ReadStudentBowelImportData($TargetFilePath);

if ($data['HeaderError']) {
	$Msg = $Lang['General']['ReturnMessage']['ImportUnsuccess_IncorrectHeaderFormat'];
	header("location:  index.php?t=management.studentBowelImport1&Msg=$Msg");
	exit();
}

$passCount = 0;
foreach($data['Data'] as $d)
{
	if($d['pass'])
	{
		$passCount++;
	}
}
$failCount = $data['NumOfData'] - $passCount;


$CurrentPage = "ManagementBowel";
$TAGS_OBJ[] = array($Lang['medical']['menu']['bowel'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$h_stepUI = $linterface->GET_IMPORT_STEPS($CurrStep=2, $CustStepArr='');

$Title = $Lang['medical']['general']['ImportData'];

$PAGE_NAVIGATION[] = array($Title,"");

$linterface->LAYOUT_START($Msg);

?>
<form method="POST" name="frm1" id="frm1" action="index.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td align='right'><?=$linterface->GET_SYS_MSG("",$xmsg);?></td>
</tr>
<tr>
	<td ><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
</tr>
<tr>
	<td><?=$h_stepUI;?></td>
</tr>
<tr>
	<td>	
		<table width="20%" border="0" cellspacing="0" cellpadding="0" align="center">
		<tr>
			<td class='formfieldtitle'><?=$Lang['Btn']['Import']?>:</td>
			<td class='tabletext'><?=$Lang['medical']['general']['BowelData']?></td>
		</tr>
		<tr>
			<td class='formfieldtitle'><?=$Lang['General']['SuccessfulRecord']?>:</td>
			<td class='tabletext'><?=$passCount?></td>
		</tr>
		<tr>
			<td class='formfieldtitle'><?=$Lang['General']['FailureRecord']?>:</td>
			<td class='tabletext <?php if($failCount>0) print('red');?>'><?=$failCount?></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="90%" border="0" cellspacing="0" cellpadding="2" align="center">
			<tr>
				<td class='tablebluetop tabletopnolink' width="1%"><?=$Lang['medical']['meal']['tableHeader']['Number']?>&nbsp;</th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentBowel']['importRemarks']['StudentBarcode']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentBowel']['importRemarks']['BowelBarcode']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentBowel']['importTableHeader']['RecordDate']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentBowel']['importTableHeader']['RecordTime']?></th>
				<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentLog']['import']['dataChecking']?></th>
				<?php if($failCount>0) {?>
					<td class="tablebluetop tabletopnolink"><?=$Lang['medical']['studentLog']['import']['reason']?></th>
				<?php }?>
			</tr>
			
			<?php for($i = 0, $iMax = $data['NumOfData']; $i < $iMax; $i++) {?>
			<tr class='tablebluerow<?=(($i+1)%2+1)?>'>
				<td class='tabletext' valign='top'><?=($i+1)?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['StudentBarcode']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['BowelBarcode']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['RecordDate']?></td>
				<td class='tabletext' valign='top'><?=$data['Data'][$i]['rawData']['oRecordTime']?></td>
				<td class='tabletext' valign='top'><?=($data['Data'][$i]['pass'])?$linterface->Get_Tick_Image():'<span class="table_row_tool"><a class="delete"/></span>'?></td>
				<?php if($failCount>0) {?>
					<td class='tabletext' valign='top'><?=$data['Data'][$i]["rawData"]["Error"]?></td>
				<?php }?>
			</tr>
			<?php } //end foreach?>
		</table>
	</td>
</tr>
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
		<input type="hidden" name="t" value="management.studentBowelImport3"/>
		<input type="hidden" name="TargetFilePath" value="<?=$TargetFilePath?>"/>	
		<?=($passCount > 0) ? $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "submit", "","submit2"," class='formbutton'  onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") :"" ?>
		&nbsp;
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t=management.studentBowelImport1'","back_btn"," class='formbutton' ") ?>
	</td>
</tr>

</table>
</form>

<?php
$linterface->LAYOUT_STOP();
?>