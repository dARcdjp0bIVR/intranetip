<?php
// using:
/*
 * 	2017-09-01 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='REVISIT_MANAGEMENT') || !$plugin['medical_module']['revisit']){
	header("Location: /");
	exit();
}

if (is_array($RevisitID)) {
	$revisitID = (count($RevisitID)==1) ? $RevisitID[0] : '';
}
else {
	$revisitID = $RevisitID;
}

if (!$revisitID) {	
	header("Location: ?t=management.revisit_list");
}

$rs_revisit = $objMedical->getRevisit($revisitID);
if (count($rs_revisit) == 1) {	
	$rs_revisit = current($rs_revisit);
}

$CurrentPage = "ManagementRevisit";
$CurrentPageName = $Lang['medical']['menu']['revisit'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['revisit'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['revisit'], "?t=management.revisit_list");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$lclass = new libclass();

$rs_class_name = $objMedical->getClassByStudentID($rs_revisit['StudentID']);

if (!empty($rs_class_name)) {	
	$studentInfo = $objMedical->getStudentNameListWClassNumberByClassName($rs_class_name,array($rs_revisit['StudentID']));
	$studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
	
	$revisitDrug = $objMedical->getRevisitDrug($revisitID);
	$rs_revisit['DrugTable'] = $objMedical->getDrugTable($revisitDrug,'disabled');
}
else {
	header("Location: ?t=management.revisit_list");
}	

$linterface->LAYOUT_START($returnMsg);


include_once('templates/revisit_view.tmpl.php');

$linterface->LAYOUT_STOP();
?>