<?php
// using:
/*
 * 	2017-07-04 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess=($from == 'event' ? 'EVENT_MANAGEMENT' : 'CASE_MANAGEMENT')) || !$plugin['medical_module']['discipline']){
	header("location: /");
	exit();
}

if (is_array($CaseID)) {
	$caseID = (count($CaseID)==1) ? $CaseID[0] : '';
}
else {
	$caseID = $CaseID;
}

if (!$caseID) {	
	header("Location: ?t=management.case_list");
}

$rs_case = $objMedical->getCases($caseID);
if (count($rs_case) == 1) {	
	$rs_case = current($rs_case);
}
else {
	header("Location: ?t=management.case_list");
}


$CurrentPage = "ManagementCase";
$CurrentPageName = $Lang['medical']['menu']['case'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['case'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['case'], $ViewMode ? "" : "?t=management.case_list");
$PAGE_NAVIGATION[] = array($Lang['Btn']['View'], "");

$lclass = new libclass();
$rs_class_name = $objMedical->getClassByStudentID($rs_case['StudentID']);
if (!empty($rs_class_name)) {
	$studentInfo = $objMedical->getStudentNameListWClassNumberByClassName($rs_class_name,array($rs_case['StudentID']));
	$studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
}

$rs_case['CasePIC'] = $objMedical->getCasePIC($caseID);
$casePICInfo = $objMedical->getUserNameList($rs_case['CasePIC']);
	
$relevantEvents = $objMedical->getCaseEventList($caseID,'1');
$meetingMinutes = $objMedical->getCaseMinuteList($caseID,'view','1');


if (!$ViewMode) {
	$linterface->LAYOUT_START($returnMsg);
	include_once('templates/case_view.tmpl.php');
	$linterface->LAYOUT_STOP();
}
else {
	include_once('templates/case_view.tmpl.php');
}
?>