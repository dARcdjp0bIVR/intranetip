<?php
// using:
/*
 *  2018-12-27 Cameron
 *      - not allow to change student in edit mode
 *      
 * 	2017-10-13 Cameron
 * 		- add greoupSelection filter
 * 
 * 	2017-08-31 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='REVISIT_MANAGEMENT') || !$plugin['medical_module']['revisit']){
	header("Location: /");
	exit();
}

if (is_array($RevisitID)) {
	$revisitID = (count($RevisitID)==1) ? $RevisitID[0] : '';
}
else {
	$revisitID = $RevisitID;
}

if (!$revisitID) {	
	header("Location: ?t=management.revisit_list");
}

$rs_revisit = $objMedical->getRevisit($revisitID);
if (count($rs_revisit) == 1) {	
	$rs_revisit = current($rs_revisit);
}

$CurrentPage = "ManagementRevisit";
$CurrentPageName = $Lang['medical']['menu']['revisit'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['revisit'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['revisit'], "?t=management.revisit_list");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$lclass = new libclass();

$nrDrugs = 0;
$rs_class_name = $objMedical->getClassByStudentID($rs_revisit['StudentID']);
if (!empty($rs_class_name)) {	
// 	$classSelection = $lclass->getSelectClass("name='ClassName' id='ClassName' ",$rs_class_name);
// 	$groupSelection = $objMedical->getGroupSelection("GroupID","","style=\"display:none\"","-- ".$Lang['Btn']['Select']." --");	
// 	$studentSelection = $objMedical->getStudentNameListWClassNumberByClassName($rs_class_name);
// 	$studentSelection = getSelectByArray($studentSelection, "name='StudentID' id='StudentID' onchange='showLastDrug()'", $rs_revisit['StudentID']);
    $studentInfo = $objMedical->getStudentNameListWClassNumberByClassName($rs_class_name,array($rs_revisit['StudentID']));
    $studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
    
	$revisitDrug = $objMedical->getRevisitDrug($revisitID);
	$nrDrugs = count($revisitDrug);
	$rs_revisit['DrugTable'] = $objMedical->getDrugTable($revisitDrug,'disabled');
	$rs_revisit['LastDrugTable'] = $objMedical->getLastDrugByStudent($rs_revisit['StudentID']);
}
else {
	header("Location: ?t=management.revisit_list");
}	

$linterface->LAYOUT_START($returnMsg);

$form_action = '?t=management.edit_revisit_save';

include_once('templates/revisit.tmpl.php');

$linterface->LAYOUT_STOP();
?>