<?php
// using:
/*
 * 	2017-07-06 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='AWARDSCHEME_MANAGEMENT') || !$plugin['medical_module']['discipline']){
	header("Location: /");
	exit();
}

if (is_array($SchemeID)) {
	$schemeID = (count($SchemeID)==1) ? $SchemeID[0] : '';
}
else {
	$schemeID = $SchemeID;
}

if (!$schemeID) {	
	header("Location: ?t=management.award_list");
}

$rs_scheme = $objMedical->getSchemes($schemeID);
if (count($rs_scheme) == 1) {	
	$rs_scheme = current($rs_scheme);
}

$CurrentPage = "ManagementAward";
$CurrentPageName = $Lang['medical']['menu']['award'];
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Scheme'] , "?t=management.award_list", 1);
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Performance'] , "?t=management.student_list_of_performance", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['award'], "?t=management.award_list");
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");

$rs_scheme['SchemePIC'] = $objMedical->getSchemePIC($schemeID);
$rs_scheme['SchemeTargetGroup'] = $objMedical->getGroupByGroupID(explode(",",$rs_scheme['GroupID']));

$schemeTargets = $objMedical->getSchemeTargetList($schemeID, $mode='edit');

$linterface->LAYOUT_START($returnMsg);

$form_action = '?t=management.edit_award_save';

include_once('templates/award.tmpl.php');
$linterface->LAYOUT_STOP();
?>