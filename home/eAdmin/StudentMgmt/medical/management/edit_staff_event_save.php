<?php
// using:
/*
 * 2018-12-21 Cameron
 * - create this file
 */
if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'STAFFEVENT_EDIT') || ! $plugin['medical_module']['staffEvents']) {
    header("Location: /");
    exit();
}

$ldb = new libdb();
$result = array();

$dataAry['StaffID'] = $StaffID;
$dataAry['EventDate'] = $StaffEventDate;
$dataAry['StartTime'] = $StartTimeHour . ":" . $StartTimeMin;
$dataAry['EndTime'] = $EndTimeHour . ":" . $EndTimeMin;
$dataAry['EventTypeID'] = $StaffEventTypeID;
$dataAry['EventTypeLev2ID'] = $StaffEventTypeLev2ID;
$dataAry['LocationBuildingID'] = $StaffEventLocationBuildingID;
$dataAry['LocationLevelID'] = $StaffEventLocationLevelID;
$dataAry['LocationID'] = $StaffEventLocationID;
$dataAry['IsReportedInjury'] = $IsReportedInjury;
$dataAry['InjuryLeave'] = $InjuryLeave;
$dataAry['SickLeave'] = $SickLeave;
$dataAry['Remark'] = standardizeFormPostValue($Remark);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $objMedical->UPDATE2TABLE('MEDICAL_STAFF_EVENT', $dataAry, array('StaffEventID' => $StaffEventID), false);

$ldb->Start_Trans();
// # step 1: update staff event
$result['UpdateStaffEvent'] = $ldb->db_db_query($sql);

if ($result['UpdateStaffEvent']) {
    
    // # step 2: add student event related to staff event
    $currentStudentEventAry = $objMedical->getStaffEventStudentEvent($StaffEventID);
    $currentStudentEventAry = Get_Array_By_Key($currentStudentEventAry, 'EventID');
    
    foreach ((array) $SelEventID as $_eventID) {
        unset($dataAry);
        $dataAry['EventID'] = $_eventID;
        $dataAry['StaffEventID'] = $StaffEventID;
        if (! in_array($_eventID, (array) $currentStudentEventAry)) {
            $sql = $objMedical->INSERT2TABLE('MEDICAL_EVENT_STAFF_EVENT', $dataAry, array(), false, false, false); // no DateModified field
            $result['AddStaffEventRelEvent_'.$_eventID] = $ldb->db_db_query($sql);
        }
    }
    
    // # step 3: add attachments
    $uploadResult = $objMedical->handleUploadFile($TargetFolder, $StaffEventID, $table = 'staff_event');
    if ($uploadResult != 'AddSuccess') {
        $result['HandleUploadFile'] = false;
    }
    
}

if (! in_array(false, $result)) {
    $ldb->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
} else {
    $ldb->RollBack_Trans();
    if ($uploadResult == 'FileSizeExceedLimit') {
        $returnMsgKey = 'FileSizeExceedLimit';
    } else {
        $returnMsgKey = 'UpdateUnsuccess';
    }
}

header("location: ?t=management.staffEventList&returnMsgKey=" . $returnMsgKey);

?>