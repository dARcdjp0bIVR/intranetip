<?php
// using:
/*
 * 	2017-07-14 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='AWARDSCHEME_MANAGEMENT') || !$plugin['medical_module']['discipline']){
	header("Location: /");
	exit();
}


$lclass = new libclass();
$rs_class_name = $objMedical->getClassByStudentID($StudentID);
if (!empty($rs_class_name)) {
	$studentInfo = $objMedical->getStudentNameListWClassNumberByClassName($rs_class_name,array($StudentID));
	$studentInfo = count($studentInfo) ? $studentInfo[0]['StudentName'] : '';
}

$scheme = $objMedical->getSchemeListByStudent($StudentID);
$schemeCounter = 0; 
$perf = array();

$CurrentPage = "ManagementAward";
$CurrentPageName = $Lang['medical']['menu']['award'];
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Scheme'] , "?t=management.award_list", 0);
$TAGS_OBJ[] = array($Lang['medical']['award']['tab']['Performance'] , "?t=management.student_list_of_performance", 1);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);
?>

<form name="form1" id="form1" method="post">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?></td>
								<td class="tabletext"><?=$studentInfo?></td>
							</tr>
						</table>
					</td>
				</tr>

			<? foreach((array)$scheme as $sch):?>
			<? if ($schemeCounter > 0):?>
				<tr>
					<td>&nbsp;</td>
				</tr>
			<? endif;?>
							
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['Scheme']?>: <?=intranet_htmlspecialchars($sch['SchemeName'])?></td>
							</tr>
						</table>
					</td>
				</tr>
			<? 	$schemeID = $sch['SchemeID'];
				$studentSchemeTarget = $objMedical->getSchemeStudentTarget($schemeID, $groupID='', array($StudentID));
				$schemeTargetCounter = 0;
			?>
				<tr>
					<td>
						<table width="100%" class="common_table_list">
								
				<? foreach((array)$studentSchemeTarget as $sst):
					unset($perf);
					$performance = explode("^~",$sst['Performance']);
					foreach((array)$performance as $val) {
						list($date,$score) = explode(",",$val);
						$perf[$date] = $score;
					}
				?>
					<? if ($schemeTargetCounter == 0):?> 	
							<tr valign="top" class="tabletop">
								<th width="1%">#</th>
								<th width="20%"><?=$Lang['medical']['award']['Target']?></th>
							<? foreach((array)$perf as $k=>$v):?>
								<th><?=($k && $k!='0000-00-00') ? ltrim(substr($k,8,2),'0').'/'.ltrim(substr($k,5,2),'0') : ''?></th>
							<? endforeach;?>
							</tr>
					<? endif;?>
					
							<tr valign="top">
								<td width="1%"><?=++$schemeTargetCounter?></td>
								<td><?=$sst['Target'] ? intranet_htmlspecialchars($sst['Target']) : '-'?></td>
							<? foreach((array)$perf as $k=>$v):?>
								<td><?=$v?></td>
							<? endforeach;?>
							</tr>
							
				<? endforeach;?>	
						</table>
					</td>
				</tr>
			<? $schemeCounter++;?>
			<? endforeach;?>	
			
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
										<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location='?t=management.student_list_of_performance'","btnReturn", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
?>