<?
/*
 * Log
 *
 * 2018-03-21 Cameron
 * - add groupID filter
 *
 * 2017-07-04 Cameron
 * - create this file
 */
$ldb = new libdb();

$currentAcademicYearID = Get_Current_Academic_Year_ID();

$classCond = '';
$cond = '';

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNumber = "u.ClassNumber";
    $classNameEng = $classNameField;
    $joinClass = '';
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNumber = "ycu.ClassNumber";
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $currentAcademicYearID . "' ";
}

if ($SrhGroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $SrhGroupID . "'";
}

if ($SrhClassName) {
    $cond .= " AND s.ClassName='" . $ldb->Get_Safe_Sql_Query($SrhClassName) . "'";
}

if ($CaseStudent) {
    $cond .= " AND c.StudentID='" . $ldb->Get_Safe_Sql_Query($CaseStudent) . "'";
}

switch ($CaseStatus) {
    case 'Closed':
        $statusVal = '1';
        break;
    case 'Processing':
        $statusVal = '0';
        break;
    default:
        $statusVal = '';
}
if ($statusVal != '') {
    $cond .= " AND c.IsClosed='" . $statusVal . "'";
}

if ($CasePIC) {
    $cond .= " AND a.CasePICID LIKE '%" . $ldb->Get_Safe_Sql_Like_Query('^' . $CasePIC . '~') . "%'";
    $casePICJoin = " INNER JOIN";
} else {
    $casePICJoin = " LEFT JOIN";
}

if ($CaseReporter) {
    $cond .= " AND c.LastModifiedBy='" . $CaseReporter . "'";
}

if ($CaseDate) {
    if ($CaseDateStart) {
        $cond .= " AND c.StartDate>='" . $CaseDateStart . "'";
    }
    
    if ($CaseDateEnd) {
        $cond .= " AND c.EndDate<='" . $CaseDateEnd . "'";
    }
    $AcademicYearID = 'null';
} else if ($AcademicYearID) {
    $startDate = getStartDateOfAcademicYear($AcademicYearID);
    $endDate = getEndDateOfAcademicYear($AcademicYearID);
    $cond .= " AND c.StartDate>='" . $startDate . "' AND c.StartDate<='" . $endDate . "'";
}

$student_name = getNameFieldByLang('u.');
$casePIC_name = getNameFieldByLang('u.');

$keyword = standardizeFormPostValue($_POST['keyword']);
if ($keyword != "") {
    $kw = $ldb->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (s.Student LIKE '%$kw%' OR a.CasePICName LIKE '%$kw%')";
    unset($kw);
}

$sql = "SELECT 
				c.CaseNo,
				c.StartDate,
				CONCAT(s.Student,' (',s.ClassName,'-',s.ClassNumber,')') as Student,
				c.Summary,
				IF (c.IsClosed=1,'" . $Lang['medical']['case']['status']['Closed'] . "','" . $Lang['medical']['case']['status']['Processing'] . "') as Status,
				a.CasePICName,
				CONCAT('<input type=\'checkbox\' name=\'CaseID[]\' id=\'CaseID[]\' value=\'', c.`CaseID`,'\'>') as ChkCaseID
		FROM
				MEDICAL_CASE c
		INNER JOIN (
				SELECT 	u.UserID, 
						$classNameEng as ClassName, 
						$classNumber as ClassNumber,
						$student_name as Student
				FROM 
						INTRANET_USER u
				" . $joinClass . "
			) AS s ON s.UserID=c.StudentID
		" . $casePICJoin . " (
				SELECT 	CaseID, 
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as CasePICID,
						GROUP_CONCAT(DISTINCT " . $casePIC_name . " ORDER BY " . $casePIC_name . " SEPARATOR ',<br>') AS CasePICName
				FROM 
						MEDICAL_CASE_PIC p
				INNER JOIN 
						INTRANET_USER u ON u.UserID=p.TeacherID
				GROUP BY CaseID
			) AS a ON a.CaseID=c.CaseID
		LEFT JOIN INTRANET_USER r ON r.UserID=c.LastModifiedBy
		WHERE 1 " . $cond . " ORDER BY c.CaseNo DESC";

if (empty($cond)) {
    $sql .= " LIMIT 10";
}
// debug_r($sql);

$rs = $ldb->returnResultSet($sql);
$x .= '	<table class="common_table_list">
			<th width="1" class="tabletoplink">#</th>
			<th width="16%">' . $Lang["medical"]["case"]["tableHeader"]["CaseNo"] . '</th>
			<th width="16%">' . $Lang["medical"]["case"]["tableHeader"]["StartDate"] . '</th>
			<th width="16%">' . $Lang["medical"]["case"]["tableHeader"]["StudentName"] . '</th>
			<th width="16%">' . $Lang["medical"]["case"]["tableHeader"]["Summary"] . '</th>
			<th width="16%">' . $Lang["medical"]["case"]["tableHeader"]["Status"] . '</th>
			<th width="16%">' . $Lang["medical"]["case"]["tableHeader"]["CasePIC"] . '</th>
			<th width="1">&nbsp;</th>
		';

if (count($rs)) {
    $i = 1;
    foreach ((array) $rs as $r) {
        $x .= '<tr>';
        $x .= '<td class="tableContent">' . $i . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['CaseNo'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['StartDate'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['Student'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . nl2br($r['Summary']) . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['Status'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['CasePICName'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['ChkCaseID'] . '</td>';
        $x .= '</tr>';
        $i ++;
    }
    $y = $i - 1;
} else {
    $x .= '<tr><td colspan="8" align="center">' . $i_no_search_result_msg . '</td></tr>';
    $y = 0;
}
$x .= '</table>';

?>