<?
	echo $linterface->Include_Thickbox_JS_CSS();
?>
<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
</style>

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script type="text/javascript">

var timerobj;

$(document).ready(function(){

	var isLoading = true;
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

	$('#TeacherType').change(function(){
		$('#TeacherID').attr('checked',true);
		isLoading = true;
		$('#TeacherSelectionSpan').html(loadingImg);
		var excludeTeacherID = [];
		var i=0;
		$('#TeacherID option').each(function() {
			if ($.trim($(this).val()) != '') {
				excludeTeacherID[i++] = $(this).val();
			}
		});

		if ($('#TeacherType').val() == '') {
			$('#AvailableTeacherID option').remove();
			isLoading = false;
			var blankOptionList = '<select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:156px;" multiple>';
			blankOptionList += '</select>';
			$('#TeacherSelectionSpan').html(blankOptionList);			
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '?t=management.ajax.ajax&ma=1',
				data : {
					'action': 'getTeacherName',
					'TeacherType': $('#TeacherType').val(),
					'ExcludeTeacherID[]':excludeTeacherID
				},		  
				success: update_available_pic_list,
				error: show_ajax_error
			});
		}		
	});

	$('#GroupCategory').change(function(){
		$('#GroupID').attr('checked',true);
		isLoading = true;
		$('#GroupSelectionSpan').html(loadingImg);
		var excludeGroupID = [];
		var i=0;
		$('#GroupID option').each(function() {
			if ($.trim($(this).val()) != '') {
				excludeGroupID[i++] = $(this).val();
			}
		});

		if ($('#GroupCategory').val() == '') {
			$('#AvailableGroupID option').remove();
			isLoading = false;
			var blankOptionList = '<select name=AvailableGroupID[] id=AvailableGroupID style="min-width:200px; height:156px;" multiple>';
			blankOptionList += '</select>';
			$('#GroupSelectionSpan').html(blankOptionList);			
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '?t=management.ajax.ajax&ma=1',
				data : {
					'action': 'getGroupName',
					'GroupCategory': $('#GroupCategory').val(),
					'ExcludeGroupID[]':excludeGroupID
				},		  
				success: update_available_group_list,
				error: show_ajax_error
			});
		}		
	});

	$('#btnSubmit').click(function(e){
		var conf;
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		$('#TeacherID option').attr('selected',true);
		$('#GroupID option').attr('selected',true);
		
		if (checkForm()) {
		<? if ($schemeID):?>
			if ( ( $('#StartDate').val() != "<?=$rs_scheme['StartDate']?>") || ( $('#EndDate').val() != "<?=$rs_scheme['EndDate']?>") ) {
				$.ajax({
					dataType: "json",
					type: "POST",
					url: "?t=management.ajax.ajax&m=1&action=checkSchemeDateRangeChange",
					data : 	{
								'SchemeID': '<?=$schemeID?>',
								'StartDate': $('#StartDate').val(),
								'EndDate': $('#EndDate').val()
							},		  
					success: function(ajaxReturn){
						if (ajaxReturn != null && ajaxReturn.success){
							if ((ajaxReturn.html == '1') && (ajaxReturn.html2 == '1')) {		// fillback and delete
								conf = confirm('<?=$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedFillBackDelete']?>');
								if (conf) {
									$('#form1').submit();	
								}
								else {
									$('input.actionBtn').attr('disabled', '');
								}								
							}
							else if ((ajaxReturn.html == '1') && (ajaxReturn.html2 == '0')) {	// fillback
								conf = confirm('<?=$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedFillBack']?>');
								if (conf) {
									$('#form1').submit();
								}
								else {
									$('input.actionBtn').attr('disabled', '');
								}
							}
							else if ((ajaxReturn.html == '0') && (ajaxReturn.html2 == '1')) {	// delete
								conf = confirm('<?=$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedDelete']?>');
								if (conf) {
									$('#form1').submit();
								}
								else {
									$('input.actionBtn').attr('disabled', '');
								}								
							}
							else {
								$('#form1').submit();
							}
						}
						else {
							$('input.actionBtn').attr('disabled', '');
						}
					},
					error: show_ajax_error
				});
			}
			else {
				$('#form1').submit();
			} 
		<? else:?>
			$('#form1').submit();
		<? endif;?>			
			
		}
		else {
			$('input.actionBtn').attr('disabled', '');	
		}
	});
	
	$(":input[name='target_name_e\[\]']").each(function(){
		
		if ($.trim($(this).val()).length > 0) {
			$(this).autocomplete(
		      "?t=management.ajax.ajax_get_target_suggestion&ma=1&from=scheme&q=",
		      {
		  			delay:3,
		  			minChars:1,
		  			matchContains:1,
		  			onItemSelect: function() { jsShowOrHideTarget($(this).attr('id')); },
		  			formatItem: function(row){ return row[0]; },
		  			autoFill:false,
		  			overflow_y: 'auto',
		  			overflow_x: 'hidden',
		  			maxHeight: '200px',
		  			width:'306px'
		  		}
		    );
		}
	});
	
});


function jsShowOrHideTarget(id) {
	timerobj = setTimeout(ShowOrHideTargetAction(id),1000);
}

function ShowOrHideTargetAction(id) {
	$("#"+id).focus();
	clearTimeout(timerobj);
}

function checkForm() {
	var error = 0;
	var focusField = '';
	hideError();
	
 	if (!check_date($('#StartDate')[0],"<?=$Lang['General']['InvalidDateFormat']?>")) {
 		error++;
 		if (focusField == '') focusField = 'StartDate';
 	}

 	if (!check_date($('#EndDate')[0],"<?=$Lang['General']['InvalidDateFormat']?>")) {
 		error++;
 		if (focusField == '') focusField = 'EndDate';
 	}
	
	if ( ($('#StartDate').val() != '' ) && ($('#EndDate').val() != '' ) && (compareDate($('#EndDate').val(), $('#StartDate').val()) < 0) ) {
 		error++;
 		if (focusField == '') focusField = 'StartDate';
 		$('#ErrStartDate').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if($('#TeacherID option').length==0) {
 		error++;
 		if (focusField == '') focusField = 'TeacherID';
 		$('#ErrTargetPIC').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	$(":input[name='target_name_e\[\]'], :input[name='target_name_n\[\]']").each(function(){
		if ($.trim($(this).val()) == '') {
			error++;
			if (focusField == '') $(this).focus();
			$(this).parent().find("span[id^='ErrTarget_']").addClass('error_msg_show').removeClass('error_msg_hide');	
		}
	});

	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function show_ajax_error() {
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function hideError() {
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function add_target() {
	if($('#GroupID option').length==0) {
 		$('#ErrGroupSelection').addClass('error_msg_show').removeClass('error_msg_hide');
 		$('#GroupID').focus();
 	}
 	else {
 		$('#ErrGroupSelection').addClass('error_msg_hide').removeClass('error_msg_show');
 		$('#GroupID option').attr('selected',true);
 		var titleExist = $('#SchemeTargetTable th').length ? '1' : '0';
 		var rowID = $("#SchemeTargetTable tr[id^='target_row_n']").length > 0 ? $("#SchemeTargetTable tr[id^='target_row_n']").length + 1 : 1;
		$.ajax({
			dataType: "json",
			async: false,			
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1&action=getNewTargetGroup&TitleExist='+titleExist+'&RowID='+rowID,
			data : $('#form1').serialize(),		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#SchemeTargetTable').append(ajaxReturn.html);
					
					$('#target_id_n'+rowID).autocomplete(
				      "?t=management.ajax.ajax_get_target_suggestion&ma=1&from=scheme&q=",
				      {
				  			delay:3,
				  			minChars:1,
				  			matchContains:1,
				  			onItemSelect: function() { jsShowOrHideTarget('target_id_n'+rowID); },
				  			formatItem: function(row){ return row[0]; },
				  			autoFill:false,
				  			overflow_y: 'auto',
				  			overflow_x: 'hidden',
				  			maxHeight: '200px',
				  			width:'306px'
				  		}
				    );
				}
			},
			error: show_ajax_error
		});
 	}
}

// compare two array a1, a2, return array of elements in a1 but not a2
function getDiffenence(a1,a2) {
	var ret = [];
	for (var i=0; i < a1.length; i++){
		var found = false;
		for (var j=0; j < a2.length; j++){
			if (a2[j] == a1[i]) {
				found = true;
				break;
			} 
		}
		if (found == false) {
			ret.push(a1[i]);
		}
	}
	return ret;
}


function removeTargetGroup() {
<? if ($schemeID):?>
		var conf = confirm('<?=$Lang['medical']['award']['Warning']['ConfirmDeleteGroup']?>');
		if (conf) {
<? endif;?>	
			checkOptionTransfer(form1.elements['GroupID[]'],form1.elements['AvailableGroupID[]']);
			checkTargetGroup();
<? if ($schemeID):?>
		}
<? endif;?>		
}

function checkTargetGroup(){
	var group_class;
	var current_group = [];	
	
	$("th[class^='Group_']").each(function() {
		group_class = $(this).attr('class');
		if (group_class !== null) {
			current_group.push(group_class.replace('Group_',''));
		}
	});
	
	$('#GroupID option').attr('selected',true);
	var selected_group = $('#GroupID').val();
	
	var to_add = [];
	var to_remove = [];
	
	if (selected_group !== null && current_group.length > 0) {
		to_add = getDiffenence(selected_group,current_group);
		to_remove = getDiffenence(current_group, selected_group);
	}
	else if (selected_group === null && current_group.length > 0) {
		to_remove = current_group;
	}
	
	if (to_remove.length > 0) {
<? if ($schemeID):?>
		$.ajax({
			dataType: "json",
			type: "POST",
			url: "?t=management.ajax.ajax&m=1&action=removeSchemeGroup",
			data : {'SchemeID': '<?=$schemeID?>',
					'GroupID[]': to_remove},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					for (var i=0; i<to_remove.length;i++) {
						$('#target_row_title .Group_'+to_remove[i]).remove();
						$('.target_row .Group_'+to_remove[i]).remove();
					}
				}
				else if (ajaxReturn!=null && ajaxReturn.msg == 'RecordExist') {
					Get_Return_Message("0|=|<?=$Lang['medical']['award']['Warning']['PerformanceRecordExist']?>");
					for (var i=0; i<to_remove.length;i++) {
						$('#AvailableGroupID option[value="'+to_remove[i]+'"]').attr('selected',true);
						checkOptionTransfer($('#AvailableGroupID')[0],$('#GroupID')[0]);
					}
					$('#GroupID option').attr('selected',true);
				}
			},
			error: show_ajax_error
		});
<? else:?>
		for (var i=0; i<to_remove.length;i++) {
			$('#target_row_title .Group_'+to_remove[i]).remove();
			$('.target_row .Group_'+to_remove[i]).remove();
		}
<? endif;?>		
	}
	
	if (to_add.length > 0) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: "?t=management.ajax.ajax&m=1&action=addSchemeGroup",
			data : {'GroupID[]': to_add},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					var newRow = 0;
					var targetID = '';
					var addedCol = '';
					$('#target_row_title th:last').before(ajaxReturn.html);
					$('.target_row').each(function() {
						addedCol = ajaxReturn.html2;
						if ($(this).find(":hidden[name='TargetID\[\]']").length > 0) {
							addedCol = addedCol.replace('RowIDorTargetID',$(this).find(":hidden[name='TargetID\[\]']").val());
							addedCol = addedCol.replace('target_group_n','target_group_e');
						}
						else {
							addedCol = addedCol.replace('RowIDorTargetID',++newRow);
						}
						$(this).find('td:last').before(addedCol);
					});					
				}
			},
			error: show_ajax_error
		});
	}
}

function removeTarget(rowID) {

	if (rowID.indexOf('R')!= -1) {	// delete in database
		var conf = confirm('<?=$Lang['medical']['award']['Warning']['ConfirmDeleteSchemeTarget']?>');
		if (conf) {
			var targetID = rowID.replace('R','');
			$.ajax({
				dataType: "json",
				type: "POST",
				url: "?t=management.ajax.ajax&m=1&action=removeSchemeTarget",
				data : {'TargetID': targetID},		  
				success: function(ajaxReturn){
					if (ajaxReturn != null && ajaxReturn.success){
						$('#target_row_e'+targetID).remove();
					}
					else if (ajaxReturn!=null && ajaxReturn.msg == 'RecordExist') {
						Get_Return_Message("<?=$Lang['medical']['award']['Warning']['PerformanceRecordExist']?>");
					}
				},
				error: show_ajax_error
			});
		}
	}
	else {
		var nr_new_rows = $("#SchemeTargetTable tr[id^='target_row_n']").length;	// original number of new rows
		if ((nr_new_rows > 1) && (rowID != nr_new_rows)) {	// not last row
			$('#target_row_n'+rowID).remove();
			var startIdx = parseInt(rowID) + 1;
			for (var i=startIdx; i<=nr_new_rows; i++) {
				var j = i - 1;
				$('#target_row_n'+i + ' span.table_row_tool').html('<a class="delete" title="<?=$Lang['medical']['award']['RemoveTargetSuggestion']?>" onClick="removeTarget(\''+j+'\')"></a>');
				var name = $('#target_row_n'+i + ' :checkbox[name$=\['+i+'\]]').attr('name');
				var idx = name.lastIndexOf('[');
				if (idx != -1) {
					var new_name = name.substr(0,idx);
					new_name = new_name + '['+ j +']';
					$('#target_row_n'+i + ' :checkbox[name$=\['+i+'\]]').attr('name', new_name);
				}			
				$('#target_row_n'+i).attr('id','target_row_n'+j);
				$('#ErrTarget_'+i).attr('id','ErrTarget_'+j);
				$('#target_id_n'+i).attr('id','target_id_n'+j);
			}	
		}
		else {	// last row
			$('#target_row_n'+rowID).remove();
		}
	}		
}


function update_available_pic_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#TeacherSelectionSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}

function update_available_group_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#GroupSelectionSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}


</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['SchemeName']?><span class="tabletextrequire">*</span></td>
								<td class="tabletext"><input type="text" name="SchemeName" id="SchemeName" class="textboxtext" value="<?=intranet_htmlspecialchars($rs_scheme['SchemeName'])?>"></td>
							</tr>
						
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['StartDate']?><span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("StartDate",(empty($rs_scheme['StartDate'])?date('Y-m-d'):$rs_scheme['StartDate']))?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['EndDate']?><span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("EndDate",((empty($rs_scheme['EndDate']) || $rs_scheme['EndDate'] == '0000-00-00')?'':$rs_scheme['EndDate']),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1)?></td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['GroupPIC']?><span class="tabletextrequire">*</span></td>
								<td class="tabletext"><?=$objMedical->getTeacherSelection($rs_scheme['SchemePIC'])?>
								<span class="error_msg_hide" id="ErrTargetPIC"><?=$Lang['medical']['award']['Warning']['SelectGroupPIC']?></span>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['Group']?></td>
								<td class="tabletext"><?=$objMedical->getSchemeGroupSelection($rs_scheme['SchemeTargetGroup'])?>
								<span class="error_msg_hide" id="ErrGroupSelection"><?=$Lang['medical']['award']['Warning']['SelectGroup']?></span>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['award']['ClassOrientedSuggestion']?></td>
								<td class="tabletext">
									<table id="SchemeTargetTable" class="common_table_list">
										<?=$schemeTargets?>
									</table>
									<div>
										<span class="table_row_tool"><a class="newBtn add" onclick="javascript:add_target()" title="<?=$Lang['medical']['award']['AddSuggestion']?>"></a><br></span>
									</div>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['General']['Remark']?></td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("Remark", $rs_scheme['Remark'], 80, 10)?></td>
							</tr>

							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
							
						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.award_list'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="SchemeID" name="SchemeID" value="<?=$schemeID?>">
</form>
