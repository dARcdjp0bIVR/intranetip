<?
/*
 * 2018-03-23 Cameron
 * - show CaseNo
 *
 */
echo $linterface->Include_Thickbox_JS_CSS();
?>

<script type="text/javascript">

function view_event(eventID) {
	tb_show('<?=$Lang['medical']['case']['ViewEvent']?>','?t=management.event_view&EventID='+eventID+'&ViewMode=1&height=500&width=800');
}

</script>

<form name="form1" id="form1" method="post">
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['CaseNo']?></td>
									<td class="tabletext"><?php echo $rs_case['CaseNo'];?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['Identity']['Student']?></td>
									<td class="tabletext"><?=$studentInfo?></td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['General']['StartDate']?></td>
									<td class="tabletext"><?=($rs_case['StartDate'] && $rs_case['StartDate'] != '0000-00-00') ? $rs_case['StartDate'] : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['General']['EndDate']?></td>
									<td class="tabletext"><?=($rs_case['EndDate'] && $rs_case['EndDate'] != '0000-00-00') ? $rs_case['EndDate'] : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['CasePIC']?></td>
									<td class="tabletext"><?=$casePICInfo?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['Summary']?></td>
									<td class="tabletext"><?=$rs_case['Summary'] ? nl2br($rs_case['Summary']) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['Detail']?></td>
									<td class="tabletext"><?=$rs_case['Detail'] ? nl2br($rs_case['Detail']) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['RelevantEvent']?></td>
									<td class="tabletext">
										<table width="100%" id="RelevantEventTable" class="common_table_list" style="display:<?=$relevantEvents ? '':'none'?>">
											<tr>
												<th width="15%"><?=$Lang['medical']['event']['EventDate']?></th>
												<th width="10%"><?=$Lang['medical']['event']['tableHeader']['StartTime']?></th>
												<th width="20%"><?=$Lang['medical']['event']['EventType']?></th>
												<th width="52%"><?=$Lang['medical']['event']['Summary']?></th>
											</tr>
										<?=$relevantEvents?>
									</table>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['Minutes']?></td>
									<td class="tabletext">
										<table width="100%" id="MinuteTable" class="common_table_list" style="display:<?=$meetingMinutes ? '':'none'?>">
											<tr>
												<th width="2%">#</th>
												<th width="10%"><?=$Lang['medical']['case']['meeting']['Date']?></th>
												<th width="85%"><?=$Lang['medical']['case']['meeting']['Record']?></th>
											</tr>
										<?=$meetingMinutes?>
									</table>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['Status']?></td>
									<td class="tabletext"><input type="radio" name="IsClosed"
										id="IsClosedYes" value="1" disabled
										<?=$rs_case['IsClosed']?'checked':''?>><label
										for="IsClosedYes"><?=$Lang['medical']['case']['status']['Closed']?></label>
										<input type="radio" name="IsClosed" id="IsClosedNo" value="0"
										disabled <?=$rs_case['IsClosed']?'':'checked'?>><label
										for="IsClosedNo"><?=$Lang['medical']['case']['status']['Processing']?></label>
									</td>
								</tr>

								<tr valign="top" id="ClosedRemarkRow" style="display:<?=$rs_case['IsClosed']?'':'none'?>">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['ClosedRemark']?></td>
									<td class="tabletext"><?=$rs_case['ClosedRemark'] ? nl2br($rs_case['ClosedRemark']) : '-'?></td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<? if ($ViewMode): ?>
										<?= $linterface->GET_ACTION_BTN($button_close, "button", "js_Hide_ThickBox();","btnClose", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>									
									<? else:?>
										<?= $linterface->GET_ACTION_BTN($button_edit, "button","window.location='?t=management.edit_case&CaseID=$caseID'","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.case_list'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									<? endif;?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br>
			</td>
		</tr>
	</table>
	<input type=hidden id="CaseID" name="CaseID" value="<?=$caseID?>">
</form>
