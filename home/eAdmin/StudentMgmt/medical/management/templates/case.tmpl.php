<?
/*
 * 2018-12-27 Cameron
 * - not allow to change student in edit mode
 *      
 * 2018-04-03 Cameron
 * - change select_event to selectEvent4Case
 *
 * 2018-03-23 Cameron
 * - show CaseNo
 *
 * 2018-03-20 Cameron
 * - add group selection [case #F121742]
 */
echo $linterface->Include_Thickbox_JS_CSS();
?>
<style>
.error_msg_hide {
	display: none;
}

.error_msg_show {
	display: 'block';
	color: #FF0000;
	font-weight: bold;
}
</style>

<script type="text/javascript">
var isLoading = true;
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

$(document).ready(function(){

	$('#ByMethod').change(function(){
		if ($('#ByMethod').val() == 'ByClass') {
			$('#GroupID').css('display','none');
			$('#ClassName').css('display','');
			changeClassName();			
		}
		else {
			$('#GroupID').css('display','');
			$('#ClassName').css('display','none');
			changeGroup();
		}
	});
	
 	$('#ClassName').change(function(){
 		changeClassName();
 	});

 	$('#GroupID').change(function(){
	 	changeGroup();
 	});
	
	$('#TeacherType').change(function(){
		$('#TeacherID').attr('checked',true);
		isLoading = true;
		$('#TeacherSelectionSpan').html(loadingImg);
		var excludeTeacherID = [];
		var i=0;
		$('#TeacherID option').each(function() {
			if ($.trim($(this).val()) != '') {
				excludeTeacherID[i++] = $(this).val();
			}
		});

		if ($('#TeacherType').val() == '') {
			$('#AvailableTeacherID option').remove();
			isLoading = false;
			var blankOptionList = '<select name=AvailableTeacherID[] id=AvailableTeacherID style="min-width:200px; height:156px;" multiple>';
			blankOptionList += '</select>';
			$('#TeacherSelectionSpan').html(blankOptionList);			
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '?t=management.ajax.ajax&ma=1',
				data : {
					'action': 'getTeacherName',
					'TeacherType': $('#TeacherType').val(),
					'ExcludeTeacherID[]':excludeTeacherID
				},		  
				success: update_available_pic_list,
				error: show_ajax_error
			});
		}		
	});
	
	$('#btnSubmit').click(function(e){
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		$('#TeacherID option').attr('selected',true);
		
		if (checkForm()) {
			$('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');	
		}
	});
	
	$("input:radio[name='IsClosed']").change(function(){
		if ($("input:radio[name='IsClosed']:checked").val() == '1') {
			$('#ClosedRemarkRow').css("display","");	
		}
		else {
			$('#ClosedRemarkRow').css("display","none");
		}
	});
	
});

function changeClassName() 
{
    isLoading = true;
	$('#StudentNameSpan').html(loadingImg);
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax',
		data : {
			'action': 'getStudentNameByClass',
			'ClassName': $('#ClassName').val()
		},		  
		success: update_student_list,
		error: show_ajax_error
	});
}

function changeGroup() 
{
    isLoading = true;
	$('#StudentNameSpan').html(loadingImg);
	
	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax',
		data : {
			'action': 'getStudentNameByGroupID',
			'GroupID': $('#GroupID').val()
		},		  
		success: update_student_list,
		error: show_ajax_error
	});
}

function checkForm() {
	var error = 0;
	var focusField = '';
	hideError();
	
 	if (!check_date($('#StartDate')[0],"<?=$Lang['General']['InvalidDateFormat']?>")) {
 		error++;
 		if (focusField == '') focusField = 'StartDate';
 	}
	
	if ($('#StudentID').val() == '') {
 		error++;
 		if (focusField == '') focusField = 'StudentID';
 		$('#ErrStudentID').addClass('error_msg_show').removeClass('error_msg_hide');
	}
	
	if ( ($('#StartDate').val() != '' ) && ($('#EndDate').val() != '' ) && (compareDate($('#EndDate').val(), $('#StartDate').val()) < 0) ) {
 		error++;
 		if (focusField == '') focusField = 'StartDate';
 		$('#ErrStartDate').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
 	
	if($('#TeacherID option').length==0) {    
 		error++;
 		if (focusField == '') focusField = 'TeacherID';
 		$('#ErrCasePIC').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function update_student_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

function show_ajax_error() {
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function hideError() {
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function add_event(){
	if ($('#StudentID').val() == '') {
		alert('<?=$Lang['medical']['case']['Warning']['SelectStudent']?>');	
	}
	else {
		tb_show('<?=$Lang['medical']['case']['SelectEvent']?>','?t=management.selectEventForCase&EventStudent='+$('#StudentID').val()+'&ClassName='+encodeURIComponent($('#ClassName').val())+'&FromCase=1&height=500&width=800');
	}
}

function view_event(eventID) {
	tb_show('<?=$Lang['medical']['case']['ViewEvent']?>','?t=management.event_view&EventID='+eventID+'&ViewMode=1&height=500&width=800');
}

function add_minute(){
	tb_show('<?=$Lang['medical']['case']['NewMinutes']?>','?t=management.new_minute&CaseID=<?=$caseID?>&popup=<?=$popup?>'+'&height=500&width=800');
}

function edit_minute(minuteID){
	tb_show('<?=$Lang['medical']['case']['EditMinutes']?>','?t=management.edit_minute&MinuteID='+minuteID+'&popup=<?=$popup?>&height=500&width=800');
}

function remove_case_event(caseID,eventID){
	conf = confirm('<?=$Lang['medical']['case']['Warning']['ConfirmDeleteCaseEvent']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeCaseEvent',
				'CaseID': caseID,
				'EventID': eventID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#case_event_row_'+eventID).remove();
					if ($('#RelevantEventTable tr').length == 1) {
						$('#RelevantEventTable').css("display","none");
					}
				}
			},
			error: show_ajax_error
		});
	}
}

function removeMinute(minuteID) {
	conf = confirm('<?=$Lang['medical']['case']['Warning']['ConfirmDeleteMeetingMinutes']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeMeetingMinutes',
				'MinuteID': minuteID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#case_minute_row_'+minuteID).remove();
					if ($('#MinuteTable tr').length == 1) {
						$('#MinuteTable').css("display","none");
					}
				}
			},
			error: show_ajax_error
		});
	}
	
}

function FileDeleteSubmit(table, fileID, minuteID) {
	conf = confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
	if (conf) {
		var row_no = $('#case_minute_row_'+minuteID+' td:first').html();
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeAttachment',
				'table': table,
				'FileID': fileID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#current_attachment_' + table + '_edit').replaceWith(ajaxReturn.html);
					if (table == 'minute') {
						$('#case_minute_row_'+minuteID).replaceWith(ajaxReturn.html2);
						$('#case_minute_row_'+minuteID+' td:first').html(row_no);
					}
				}
				
			},
			error: show_ajax_error
		});
	}
}

function update_available_pic_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#TeacherSelectionSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}
</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>"
	enctype="multipart/form-data">
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">
						<?php if ($rs_case['CaseNo']):?>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['CaseNo']?></td>
									<td class="tabletext"><?php echo $rs_case['CaseNo'];?></td>
								</tr>
						<?php endif;?>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['Identity']['Student']?>
										<?php if (!$caseID):?>
											<span class="tabletextrequire">*</span>
										<?php endif;?>
									</td>
									<td class="tabletext">
									<?php if ($caseID):?>
    									<?php echo $studentInfo;?>
    									<input type="hidden" name="StudentID" value="<?php echo $rs_case['StudentID'];?>">
									<?php else:?>
										<span> 
											<select name="ByMethod" id="ByMethod">
												<option value="ByClass"><?php echo $Lang['Header']['Menu']['Class']; ?></option>
												<option value="ByGroup"><?php echo $Lang['medical']['meal']['searchMenu']['group']; ?></option>
											</select>
										</span>&nbsp;<?=$classSelection . $groupSelection?> &nbsp;
										<span><?=$Lang['medical']['case']['StudentName']?></span>&nbsp;
										<span id="StudentNameSpan"><?=$studentSelection?></span> 
										<span class="error_msg_hide" id="ErrStudentID"><?=$Lang['medical']['event']['Warning']['SelectStudent']?></span>
									<?php endif;?>	
									</td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['General']['StartDate']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$linterface->GET_DATE_PICKER("StartDate",(empty($rs_case['StartDate'])?date('Y-m-d'):$rs_case['StartDate']))?>
								<span class="error_msg_hide" id="ErrStartDate"><?=$Lang['medical']['general']['Warning']['StartDateGtEndDate']?></span>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['General']['EndDate']?></td>
									<td class="tabletext"><?=$linterface->GET_DATE_PICKER("EndDate",((empty($rs_case['EndDate']) || $rs_case['EndDate'] == '0000-00-00')?'':$rs_case['EndDate']),$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=1)?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['CasePIC']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$objMedical->getTeacherSelection($rs_case['CasePIC'])?>&nbsp;
									<span class="error_msg_hide" id="ErrCasePIC"><?=$Lang['medical']['case']['Warning']['SelectCasePIC']?></span>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['Summary']?></td>
									<td class="tabletext"><?=$linterface->GET_TEXTAREA("Summary", $rs_case['Summary'], 80, 10)?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['Detail']?></td>
									<td class="tabletext"><?=$linterface->GET_TEXTAREA("Detail", $rs_case['Detail'], 80, 10)?></td>
								</tr>

						<? if (!$popup):?>
							<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['RelevantEvent']?></td>
									<td class="tabletext">
										<table width="100%" id="RelevantEventTable" class="common_table_list" style="display:<?=$relevantEvents ? '':'none'?>">
											<tr>
												<th width="15%"><?=$Lang['medical']['event']['EventDate']?></th>
												<th width="10%"><?=$Lang['medical']['event']['tableHeader']['StartTime']?></th>
												<th width="20%"><?=$Lang['medical']['event']['EventType']?></th>
												<th width="52%"><?=$Lang['medical']['event']['Summary']?></th>
												<th>&nbsp;</th>
											</tr>
										<?=$relevantEvents?>
									</table>

										<div>
											<span class="table_row_tool"><a class="newBtn add"
												onclick="javascript:add_event()"
												title="<?=$Lang['medical']['case']['LinkRelevantEvent']?>"></a></span>
										</div>

									</td>
								</tr>
						<? endif;?>
							
							<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['Minutes']?></td>
									<td class="tabletext">
										<table width="100%" id="MinuteTable" class="common_table_list" style="display:<?=$meetingMinutes ? '':'none'?>">
											<tr>
												<th width="2%">#</th>
												<th width="10%"><?=$Lang['medical']['case']['meeting']['Date']?></th>
												<th width="85%"><?=$Lang['medical']['case']['meeting']['Record']?></th>
												<th>&nbsp;</th>
											</tr>
										<?=$meetingMinutes?>
									</table>
										<div>
											<span class="table_row_tool"><a class="newBtn add"
												onclick="javascript:add_minute()"
												title="<?=$Lang['medical']['case']['AddMeetingMinute']?>"></a></span>
										</div>
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['tableHeader']['Status']?></td>
									<td class="tabletext"><input type="radio" name="IsClosed"
										id="IsClosedYes" value="1"
										<?=$rs_case['IsClosed']?'checked':''?>><label
										for="IsClosedYes"><?=$Lang['medical']['case']['status']['Closed']?></label>
										<input type="radio" name="IsClosed" id="IsClosedNo" value="0"
										<?=$rs_case['IsClosed']?'':'checked'?>><label for="IsClosedNo"><?=$Lang['medical']['case']['status']['Processing']?></label>
									</td>
								</tr>

								<tr valign="top" id="ClosedRemarkRow" style="display:<?=$rs_case['IsClosed']?'':'none'?>">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['case']['ClosedRemark']?></td>
									<td class="tabletext"><?=$linterface->GET_TEXTAREA("ClosedRemark", $rs_case['ClosedRemark'], 80, 10)?></td>
								</tr>

								<tr>
									<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
									<td width="80%">&nbsp;</td>
								</tr>

							</table>

						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", ($popup?"window.close();":"window.location='?t=management.case_list'"),"btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table> <br>
			</td>
		</tr>
	</table>
	<input type=hidden id="CaseID" name="CaseID" value="<?=$caseID?>"> <input
		type=hidden id="Popup" name="Popup" value="<?=$popup?>">
</form>
