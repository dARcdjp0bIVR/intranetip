<?
/*
 * 2018-12-21 Cameron
 * - create this file
 *
 */
echo $linterface->Include_Thickbox_JS_CSS();
echo '<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery-1.8.3.min.js"></script>';
?>

<script type="text/javascript">

function show_ajax_error() {
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function FileDeleteSubmit(table, fileID) {
	conf = confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeAttachment',
				'table': table,
				'FileID': fileID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#current_attachment_' + table + '_edit').replaceWith(ajaxReturn.html);
				}
				
			},
			error: show_ajax_error
		});
	}
}

$(document).ready(function(){

	$(document).on('click', '.viewStudentEvent', function(){
		var studentEventID = $(this).attr('data-StudentEventID');
		tb_show('<?=$Lang['medical']['staffEvent']['ViewEvent']?>','?t=management.event_view&EventID='+studentEventID+'&ViewMode=1&height=500&width=800');
	});
	
});

function view_event(eventID) {
	tb_show('<?=$Lang['medical']['case']['ViewEvent']?>','?t=management.event_view&EventID='+eventID+'&ViewMode=1&height=500&width=800');
}

</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>"
	enctype="multipart/form-data">
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['Identity']['Staff']?></td>
									<td class="tabletext"><?=$staffEventAry['StaffName']?></td>
								</tr>
							
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['EventDate']?></td>
									<td class="tabletext"><?=$staffEventAry['EventDate'] ? $staffEventAry['EventDate'] : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['StartTime']?></td>
									<td class="tabletext"><?=$staffEventAry['StartTime']? substr($staffEventAry['StartTime'],0,5) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['EndTime']?></td>
									<td class="tabletext"><?=$staffEventAry['EndTime']? substr($staffEventAry['EndTime'],0,5) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['EventType']?></td>
									<td class="tabletext"><?=$eventTypeInfo?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['Place']?></td>
									<td class="tabletext"><?=$locationInfo?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['HasReportedInjury']?></td>
									<td class="tabletext"><?=$staffEventAry['IsReportedInjury'] ? $Lang['General']['Yes'] : $Lang['General']['No']?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['DaysOfInjuryLeave']?></td>
									<td class="tabletext"><?=$staffEventAry['InjuryLeave']?></td>
								</tr>
								
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['DaysOfSickLeave']?></td>
									<td class="tabletext"><?=$staffEventAry['SickLeave']?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['remark']?></td>
									<td class="tabletext"><?=$staffEventAry['Remark'] ? nl2br($staffEventAry['Remark']) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['staffEvent']['RelevantEvent']?></td>
									<td class="tabletext">
										<table width="100%" id="relevantStudentEventTable" class="common_table_list" style="display:<?=$relevantStudentEvents? '':'none'?>">
											<tr>
												<th width="15%"><?=$Lang['medical']['event']['EventDate']?></th>
												<th width="10%"><?=$Lang['medical']['event']['tableHeader']['StartTime']?></th>
												<th width="15%"><?=$Lang['medical']['staffEvent']['StudentName']?></th>
												<th width="15%"><?=$Lang['medical']['event']['EventType']?></th>
												<th width="44%"><?=$Lang['medical']['event']['Summary']?></th>
											</tr>
											<?=$relevantStudentEvents?>
										</table>
									</td>
								</tr>

								<!-- upload files -->
								<?=$objMedical->getAttachmentLayout('staff_event', $staffEventAry['StaffEventID'], $mode='view', $plupload)?>
							
							</table>

						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<?php 
									   if ($ViewMode) {
									       echo $linterface->GET_ACTION_BTN($button_close, "button", "js_Hide_ThickBox();","btnClose", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
									   }
									   else {
									       if ((!$viewRight && $isStaffEventStaff) || ($viewRight && !$editRight)) {
									           echo $linterface->GET_ACTION_BTN($button_back, "button", "window.location='?t=management.staffEventList'","btnBack", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
									       }
									       else {
									           echo $linterface->GET_ACTION_BTN($button_edit, "button","window.location='?t=management.edit_staff_event&StaffEventID=".$staffEventAry['StaffEventID']."'","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
									           echo "&nbsp;";
									           echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.staffEventList'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");
									       }
									   }
									?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table> <br>
			</td>
		</tr>
	</table>
	<input type=hidden id="StaffEventID" name="StaffEventID" value="<?=$staffEventAry['StaffEventID']?>">

<? if($use_plupload):?>
	<input type="hidden" name="TargetFolder" id="TargetFolder"
		value="<?=$tempFolderPath?>">
<? else: ?>
	<input type="hidden" name="FileName" id="FileName" value="">
<? endif; ?>

</form>
