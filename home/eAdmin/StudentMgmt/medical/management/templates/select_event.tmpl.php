<?php
/*
 * 
 * 2018-04-27 Cameron
 * - apply stripslashes to EventTypeName
 *
 * 2018-03-21 Cameron
 * - add groupID filter
 *
 * 2018-03-20 Cameron
 * - keyword include AffectedNonAccountHolder
 * - AffectedPerson column include AffectedNonAccountHolder
 */
$ldb = new libdb();

$AcademicYearID = Get_Current_Academic_Year_ID();
$classCond = '';
$cond = '';

if ($junior_mck) {
    $classNameField = "u.ClassName";
    $classNameEng = $classNameField;
    $joinClass = '';
} else {
    $classNameField = Get_Lang_Selection("yc.ClassTitleB5", "yc.ClassTitleEN");
    $classNameEng = "yc.ClassTitleEN";
    $joinClass = " INNER JOIN YEAR_CLASS_USER ycu ON ycu.UserID=u.UserID
					INNER JOIN YEAR_CLASS yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='" . $AcademicYearID . "' ";
}

if ($SrhGroupID) {
    $joinClass .= "	INNER JOIN INTRANET_USERGROUP g ON g.UserID=u.UserID AND g.GroupID='" . $SrhGroupID . "'";
}

if ($SrhClassName) {
    $cond .= " AND s.ClassName LIKE '%" . $ldb->Get_Safe_Sql_Like_Query($SrhClassName) . "%'";
}

if ($EventStudent) {
    $cond .= " AND s.StudentID LIKE '%" . $ldb->Get_Safe_Sql_Like_Query('^' . $EventStudent . '~') . "%'";
}

if ($EventType) {
    $cond .= " AND e.EventTypeID='" . $EventType . "'";
}

if ($LocationBuildingID) {
    $cond .= " AND e.LocationBuildingID='" . $LocationBuildingID . "'";
}

if ($LocationLevelID) {
    $cond .= " AND e.LocationLevelID='" . $LocationLevelID . "'";
}

if ($LocationID) {
    $cond .= " AND e.LocationID='" . $LocationID . "'";
}

if ($EventAffectedPerson) {
    $cond .= " AND a.AffectedPersonID LIKE '%" . $ldb->Get_Safe_Sql_Like_Query('^' . $EventAffectedPerson . '~') . "%'";
    $affectedPersonJoin = " INNER JOIN";
} else {
    $affectedPersonJoin = " LEFT JOIN";
}

if ($EventReporter) {
    $cond .= " AND e.LastModifiedBy='" . $EventReporter . "'";
}

if ($EventDate) {
    if ($EventDateStart) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.StartTime)>='" . $EventDateStart . " " . $TimeStartHour . ":" . $TimeStartMin . ":00'";
    }
    
    if ($EventDateEnd) {
        $cond .= " AND CONCAT(e.EventDate,' ',e.EndTime)<='" . $EventDateEnd . " " . $TimeEndHour . ":" . $TimeEndMin . ":00'";
    }
}

$locationField = Get_Lang_Selection("NameChi", "NameEng");
$student_name = getNameFieldByLang('u.');
$affected_name = getNameFieldByLang('u.');
$reporter_name = getNameFieldByLang('r.');

$keyword = standardizeFormPostValue($_POST['keyword']);
if ($keyword != "") {
    $kw = $ldb->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (s.Student LIKE '%$kw%' OR a.AffectedPerson LIKE '%$kw%' OR e.AffectedNonAccountHolder LIKE '%$kw%')";
    unset($kw);
}

$sql = "SELECT 
				e.EventDate,
				s.Student,
				DATE_FORMAT(e.StartTime,'%k:%i') as StartTime,
				et.EventTypeName,
				CASE 
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN '-'
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN loc." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN lvl." . $locationField . "
					WHEN e.LocationBuildingID=0 AND e.LocationLevelID<>0 AND e.LocationID<>0 THEN CONCAT(lvl." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID=0 THEN b." . $locationField . "
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID=0 AND e.LocationID<>0 THEN CONCAT(b." . $locationField . ",' > ',loc." . $locationField . ")
					WHEN e.LocationBuildingID<>0 AND e.LocationLevelID<>0 AND e.LocationID=0 THEN CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ")
					ELSE CONCAT(b." . $locationField . ",' > ',lvl." . $locationField . ",' > ',loc." . $locationField . ")
				END as Location,
                IF(e.AffectedNonAccountHolder IS NULL OR e.AffectedNonAccountHolder='', a.AffectedPerson, CONCAT(IF(a.AffectedPerson IS NULL,'',CONCAT(a.AffectedPerson,'<br>')), e.AffectedNonAccountHolder)) as AffectedPerson,
				e.Summary,
				CONCAT('<input type=\'checkbox\' name=\'EventID[]\' id=\'EventID[]\' value=\'', e.`EventID`,'\'>') as ChkEventID
		FROM
				MEDICAL_EVENT e
		INNER JOIN (
				SELECT 	EventID,
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as StudentID, 
						GROUP_CONCAT(DISTINCT CONCAT('^',$classNameEng,'~') ORDER BY $classNameEng SEPARATOR ',') as ClassName,
						GROUP_CONCAT(DISTINCT CONCAT(" . $student_name . ",' ('," . $classNameField . ",IF(p.stayOverNight=1,'" . $Lang['medical']['general']['sleepOption']['StayIn'] . "','" . $Lang['medical']['general']['sleepOption']['StayOut'] . "'),')') ORDER BY u.ClassName, u.ClassNumber SEPARATOR ',<br>') as Student
				FROM 
						MEDICAL_EVENT_STUDENT s
				INNER JOIN 
						INTRANET_USER u ON u.UserID=s.StudentID
				" . $joinClass . "
				LEFT JOIN 
						INTRANET_USER_PERSONAL_SETTINGS p ON p.UserID=u.UserID
				GROUP BY s.EventID
			) AS s ON s.EventID=e.EventID
		LEFT JOIN MEDICAL_EVENT_TYPE et ON et.EventTypeID=e.EventTypeID
		LEFT JOIN INVENTORY_LOCATION_BUILDING b ON b.BuildingID=e.LocationBuildingID
		LEFT JOIN INVENTORY_LOCATION_LEVEL lvl ON lvl.LocationLevelID=e.LocationLevelID
		LEFT JOIN INVENTORY_LOCATION loc ON loc.LocationID=e.LocationID
		" . $affectedPersonJoin . " (
				SELECT 	EventID,
						GROUP_CONCAT(DISTINCT CONCAT('^',u.UserID,'~') ORDER BY u.UserID SEPARATOR ',') as AffectedPersonID, 
						GROUP_CONCAT(DISTINCT " . $affected_name . " ORDER BY " . $affected_name . " SEPARATOR ',<br>') AS AffectedPerson
				FROM 
						MEDICAL_EVENT_AFFECTED_PERSON p
				INNER JOIN 
						INTRANET_USER u ON u.UserID=p.AffectedPersonID
				GROUP BY EventID
			) AS a ON a.EventID=e.EventID 
		LEFT JOIN INTRANET_USER r ON r.UserID=e.LastModifiedBy
		WHERE 1 " . $cond . " ORDER BY e.EventDate DESC";
if (empty($cond)) {
    $sql .= " LIMIT 10";
}

$rs = $ldb->returnResultSet($sql);

// $ldb->field_array = array("EventDate", "Student","StartTime","EventTypeName","Location","AffectedPerson", "Summary");
$x .= '	<table class="common_table_list">
			<th width="1" class="tabletoplink">#</th>
			<th width="10%">' . $Lang['medical']['event']['tableHeader']['EventDate'] . '</th>
			<th width="20%">' . $Lang['medical']['event']['tableHeader']['StudentName'] . '</th>
			<th width="10%">' . $Lang['medical']['event']['tableHeader']['StartTime'] . '</th>
			<th width="10%">' . $Lang['medical']['event']['tableHeader']['EventType'] . '</th>
			<th width="20%">' . $Lang['medical']['event']['tableHeader']['Place'] . '</th>
			<th width="10%">' . $Lang['medical']['event']['tableHeader']['AffectedPerson'] . '</th>
			<th width="10%">' . $Lang['medical']['event']['Summary'] . '</th>
			<th width="1"><input type="checkbox" name="checkmaster" onclick="(this.checked)?setChecked(1,this.form,\'EventID[]\'):setChecked(0,this.form,\'EventID[]\')"></th>
		';

if (count($rs)) {
    $i = 1;
    foreach ((array) $rs as $r) {
        $x .= '<tr>';
        $x .= '<td class="tableContent">' . $i . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['EventDate'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['Student'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['StartTime'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . stripslashes($r['EventTypeName']) . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['Location'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['AffectedPerson'] . '</td>';
        $x .= '<td class="tabletext tablerow">' . nl2br($r['Summary']) . '</td>';
        $x .= '<td class="tabletext tablerow">' . $r['ChkEventID'] . '</td>';
        $x .= '</tr>';
        $i ++;
    }
    $y = $i - 1;
} else {
    $x .= '<tr><td colspan="9" align="center">' . $i_no_search_result_msg . '</td></tr>';
    $y = 0;
}
$x .= '</table>';

?>