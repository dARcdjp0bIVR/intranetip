<?
/*
 * 	Log
 * 
 * 	2017-06-30 Cameron
 * 		- create this file
 */
 
	echo $linterface->Include_Thickbox_JS_CSS();
?>

<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
#TB_window a {
    color: #2286C5 !important;
}
#TB_window a:hover {
    color: #ff0000 !important;
}
</style>

<script language="javascript">
$().ready( function(){
	$("#submitBtn_tb").click(function(){
		var empty = true;
		var error = 0;
		$("input.actionBtn").attr("disabled", "disabled");
		if ($("#MeetingDate").val() != ''){
			empty = false;
		}
		
<? if ($use_plupload): ?>	
	if(!jsIsFileUploadCompleted()) {
		error++;
		$('#FileWarnDiv_minute').html('<?=$Lang['medical']['upload']['jsWaitAllFilesUploaded']?>').show();
		setTimeout(hideFileWarningTimerHandler_thickbox,5000);
	}
<? else: ?>
		$('#FileName').val(document.getElementById('File').value);
<? endif;?>
		
		if (empty) {
			alert('<?=$Lang['medical']['case']['Warning']['InputMeetingDate']?>');
			$("input.actionBtn").attr("disabled", "");
			return;
		}
	 	else if (compareDate($('#MeetingDate').val(),$('#StartDate').val())<0) {
	 		alert('<?=$Lang['medical']['case']['Warning']['InvalidDateEarly']?>');
	 		$("input.actionBtn").attr("disabled", "");
	 		return false;
	 	}
		else if (error) {
			$("input.actionBtn").attr("disabled", "");
			return;
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: "?t=management.ajax.ajax&m=1&action=<?=$action?>",
				data : $("#form3").serialize(),		  
				success: <?=(($action=='newMinuteSave') ? 'add_minute_list' : 'update_minute_list')?>,
				error: show_ajax_error
			});
			
		}
		js_Hide_ThickBox();
	});			
});


function add_minute_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#MinuteTable').append(ajaxReturn.html);
		reorderRow();
		$('#MinuteTable').css("display","");
	}
	$("input.actionBtn").attr("disabled", "");
}	

function update_minute_list(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#case_minute_row_<?=$minuteID?>').replaceWith(ajaxReturn.html);
		reorderRow();
		$('#MinuteTable').css("display","");
	}
	$("input.actionBtn").attr("disabled", "");
}	

function hideFileWarningTimerHandler_thickbox() {
	$('#FileWarnDiv_minute').hide();
}

function reorderRow() {
	var i = 1;
	$('.row_num').each(function(){
		$(this).html(i++);		
	});
}

</script>

<form name="form3" id="form3" method="post" enctype="multipart/form-data">
	<table align="center" class="form_table_v30">
	
		<tr valign="top">
			<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['case']['meeting']['Date']?><span class="tabletextrequire">*</span></td>
			<td class="tabletext"><?=$linterface->GET_DATE_PICKER("MeetingDate",(empty($rs_minute['MeetingDate'])?date('Y-m-d'):$rs_minute['MeetingDate']))?></td>
		</tr>
	
		<!-- upload files -->
		<?=$objMedical->getAttachmentLayout('minute', $rs_minute['MinuteID'], $mode='edit', $plupload)?>

	</table>
	
<div id="editBottomDiv" class="edit_bottom_v30">
	<p class="spacer"></p>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "", "submitBtn_tb", "", "0", "", "actionBtn")?>
		<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Hide_ThickBox();", "cancelBtn_tb", "", "0", "", "actionBtn")?>
	<p class="spacer"></p>
</div>
<?=$linterface->GET_HIDDEN_INPUT('CaseID', 'CaseID', $caseID)?>
<?=$linterface->GET_HIDDEN_INPUT('MinuteID', 'MinuteID', $minuteID)?>

<? if($use_plupload):?>
	<input type="hidden" name="TargetFolder" id="TargetFolder" value="<?=$tempFolderPath?>">
<? else: ?>
	<input type="hidden" name="FileName" id="FileName" value="">
<? endif; ?>
	
</form>

