<?php 
/*
 *  2019-06-20 Cameron
 *      - show - for Hospital and Division if they are empty
 */
?>
<style>
.error_msg_hide{display:none;}
.error_msg_show{display:'block';color: #FF0000; font-weight: bold;}
</style>

<form name="form1" id="form1" method="post">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['Identity']['Student']?></td>
								<td class="tabletext"><?=$studentInfo?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['RevisitDate']?></td>
								<td class="tabletext"><?=($rs_revisit['RevisitDate'] && $rs_revisit['RevisitDate'] != '0000-00-00') ? $rs_revisit['RevisitDate'] : '-'?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['ThisRevisitInfo']?></td>
								<td class="tabletext">
									<div id="RevisitDrugDiv">
										<table id="DrugTable" class="common_table_list">
											<tr id="drug_title" class="tabletop">
												<th width="55%"><?=$Lang['medical']['revisit']['Drug']?></th>
												<th width="40%"><?=$Lang['medical']['revisit']['Dosage']?></th>
											</tr>
											<?=$rs_revisit['DrugTable']?>
										</table>									
									</div>
								</td>
							</tr>
							
							<!-- New Customized Columns -->
							<?php if($sys_custom['medical']['MedicalReport']){?>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['VisitDate']?>
								<td><?php echo ((empty($rs_revisit['VisitDate']) || $rs_revisit['VisitDate'] == '0000-00-00')?'-':$rs_revisit['VisitDate'])?></td>
								</td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['AccidentInjure']?></td>
								<td><?php echo ($rs_revisit['AccidentInjure'] ? $linterface->Get_Tick_Image() : '-')?></td>
							</tr>
						 	<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['DischargedAdmittion']?></td>
								<td><?php echo ($rs_revisit['DischargedAdmittion'] ? $linterface->Get_Tick_Image() : '-')?></td>
							</tr>
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['Diagnosis']?></td>
								<td><?php echo $rs_revisit['Diagnosis']?></td>
							</tr>
							<?php }?>
							<!-- End of New Customized Columns -->
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['CuringStatus']?></td>
								<td class="tabletext"><?=$rs_revisit['CuringStatus'] ? nl2br($rs_revisit['CuringStatus']) : '-'?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['Hospital']?></td>
								<td><?php echo $rs_revisit['Hospital'] ? $rs_revisit['Hospital'] : '-';?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?=$Lang['medical']['revisit']['Division']?></td>
								<td><?php echo $rs_revisit['Division'] ? $rs_revisit['Division'] : '-';?></td>
							</tr>

						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
									<?= $linterface->GET_ACTION_BTN($button_edit, "button","window.location='?t=management.edit_revisit&RevisitID=$revisitID'","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.revisit_list'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="RevisitID" name="RevisitID" value="<?=$revisitID?>">

</form>
