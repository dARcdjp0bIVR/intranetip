<?php
/*
 * 2019-01-04 Cameron
 * - add relevantStaffEvent [case#F142033]
 * - add function view_staff_event, remove_staff_event_of_student_event
 * 
 * 2018-12-19 Cameron
 * - fix: should empty LocationID and hide it when change BuildingID (in update_building_level)
 * 
 * 2018-03-29 Cameron
 * - add relevantStudentLogTable
 *
 * 2018-03-23 Cameron
 * - add RelevantCaseTable
 *
 * 2018-03-19 Cameron
 * - add search student by group
 */
echo $linterface->Include_Thickbox_JS_CSS();
echo '<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery-1.8.3.min.js"></script>';
?>

<style>
.error_msg_hide {
	display: none;
}

.error_msg_show {
	display: 'block';
	color: #FF0000;
	font-weight: bold;
}

.followup_list {
	margin: 5px;
	padding: 0.2em;
	border: 1px solid #c6cc42;
	color: #222222;
}

.followup_list_header {
	background: #c6cc42 url(<?=$PATH_WRT_ROOT?>images/space.gif) repeat-x
		scroll 50% 50%;
	border: 1px solid #c6cc42;
	color: #000000;
	font-weight: bold;
	padding: 3px;
}
</style>

<script type="text/javascript">
var isLoading = true;
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image() ?>';

function add_followup() 
{
	tb_show('<?=$Lang['medical']['event']['NewFollowup']?>','?t=management.new_followup&EventID=<?=$eventID?>'+'&height=500&width=800');
}


$(document).ready(function(){

	$('#ByMethod').change(function(){
		if ($('#ByMethod').val() == 'ByClass') {
			$('#GroupID').css('display','none');
			$('#ClassName').css('display','');
			changeClassName();			
		}
		else {
			$('#GroupID').css('display','');
			$('#ClassName').css('display','none');
			changeGroup();
		}
	});
	
 	$('#ClassName').change(function(){
 		changeClassName();
 	});

 	$('#GroupID').change(function(){
	 	changeGroup();
 	});

 	
	$('#AffectedPersonByMethod').change(function(){
		switch($('#AffectedPersonByMethod').val()) {
    		case 'ByClass':
    			$('#AffectedPersonClassName').css('display','');
    			$('#AffectedPersonGroupID').css('display','none');    			
    			changeAffectedPersonClassName();			
    			break;
    		case 'ByGroup':
    			$('#AffectedPersonClassName').css('display','none');
    			$('#AffectedPersonGroupID').css('display','');    			
    			changeAffectedPersonGroup();			
    			break;
    		case 'TeachingStaff':
    			$('#AffectedPersonClassName').css('display','none');
    			$('#AffectedPersonGroupID').css('display','none');
    			getAvailableTeachingStaff();
    			break;
    		case 'NonTeachingStaff':
    			$('#AffectedPersonClassName').css('display','none');
    			$('#AffectedPersonGroupID').css('display','none');
    			getAvailableNonTeachingStaff();
				break;						
		}
	});
	
 	$('#AffectedPersonClassName').change(function(){
 		changeAffectedPersonClassName();
 	});

 	$('#AffectedPersonGroupID').change(function(){
 		changeAffectedPersonGroup();
 	});
 	
	$('#LocationBuildingID').change(function(){
		if ($('#LocationBuildingID').val() == '') {
			$('#LocationLevelID').val('');
			$('#LocationID').val('');
			$('#BuildingLevelSpan').html('');
			$('#LocationSpan').html('');
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '?t=management.ajax.ajax&ma=1',
				data : {
					'action': 'getBuildingLevel',
					'LocationBuildingID': $('#LocationBuildingID').val()
				},		  
				success: update_building_level,
				error: show_ajax_error
			});
		}
	});


	$("input:radio[name='IsCase']").change(function(){
		if ($("input:radio[name='IsCase']:checked").val() == '1') {
			$('#LinkCaseBtn').css("display","");	
		}
		else {
			if ($('.selectCase').length > 0) {
    			var conf = confirm('<?=$Lang['medical']['event']['Warning']['ConfirmDeleteEventCase']?>');
    			if (conf) {
        			if ($('#EventID').val() > 0) {
            			$.ajax({
            				dataType: "json",
            				type: "POST",
            				url: '?t=management.ajax.ajax',
            				data : {
            					'action': 'removeEventCase',
            					'EventID': $('#EventID').val()
            				},		  
            				success: function(ajaxReturn){
            					if (ajaxReturn != null && ajaxReturn.success){
                					removeAllEventCases();
            					}
            				},
            				error: show_ajax_error
            			});
        			}
        			else {
        				removeAllEventCases();
        			}
    			}
    			else {
    				$('#IsCaseYes').prop('checked',true);
    			}
			}
			else {
				$('#LinkCaseBtn').css("display","none");
			}
		}
	});
	
	
	$('#btnSubmit').click(function(e){
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');
		$('#StudentID option').attr('selected',true);
		$('#AffectedPersonID option').attr('selected',true);
		
		if (checkForm()) {
			$('#form1').submit();
		}
		else {
			$('input.actionBtn').prop('disabled', false);	
		}
	});

	$(document).on('click', '.viewCase', function(){
		var caseID = $(this).attr('data-CaseID');
		tb_show('<?=$Lang['medical']['event']['ViewCase']?>','?t=management.case_view&CaseID='+caseID+'&ViewMode=1&height=500&width=800');
	});

	$(document).on('click', '.removeCase', function(){
		var caseID = $(this).attr('data-CaseID');
		var eventID = $(this).attr('data-EventID');
		var conf = confirm('<?=$Lang['medical']['event']['Warning']['ConfirmDeleteEventCase']?>');
		if (conf) {
			if (eventID > 0) {
    			$.ajax({
    				dataType: "json",
    				type: "POST",
    				url: '?t=management.ajax.ajax',
    				data : {
    					'action': 'removeCaseEvent',
    					'CaseID': caseID,
    					'EventID': eventID
    				},		  
    				success: function(ajaxReturn){
    					if (ajaxReturn != null && ajaxReturn.success){
    						removeEventCaseRow(caseID);
    					}
    				},
    				error: show_ajax_error
    			});
			}
			else {
				removeEventCaseRow(caseID);
			}
		}
	});

	$('#addStudentLogBtn').click(function(){
		$('#StudentID option').attr('selected',true);
		tb_show('<?=$Lang['medical']['event']['SelectStudentLog']?>','?t=management.selectStudentLog&StudentID='+$('#StudentID').val()+'&ClassName='+encodeURIComponent($('#ClassName').val())+'&FromEvent=1&height=500&width=800');
	});

	$(document).on('click', '.removeStudentLog', function(){
		var studentLogID = $(this).attr('data-StudentLogID');
		var eventID = $(this).attr('data-EventID');
		var conf = confirm('<?=$Lang['medical']['event']['Warning']['ConfirmDeleteStudentLLog']?>');
		if (conf) {
			if (eventID > 0) {
    			$.ajax({
    				dataType: "json",
    				type: "POST",
    				url: '?t=management.ajax.ajax',
    				data : {
    					'action': 'removeEventStudentLog',
    					'studentLogID': studentLogID,
    					'eventID': eventID
    				},		  
    				success: function(ajaxReturn){
    					if (ajaxReturn != null && ajaxReturn.success){
    						removeEventStudentLogRow(studentLogID);
    					}
    				},
    				error: show_ajax_error
    			});
			}
			else {
				removeEventStudentLogRow(studentLogID);
			}
		}
	});

	$(document).on('click', '.viewStudentLog', function(){
		var studentLogID = $(this).attr('data-StudentLogID');
		tb_show('<?=$Lang['medical']['event']['ViewStudentLog']?>','?t=management.studentLogView&studentLogID='+studentLogID+'&ViewMode=1&height=500&width=800');
	});

	$('#addStaffEventBtn').click(function(){
		$('#StudentID option').attr('selected',true);
		tb_show('<?=$Lang['medical']['event']['SelectStaffEvent']?>','?t=management.selectStaffEventForEvent&height=500&width=800');
	});

	$(document).on('click', '.removeStaffEvent', function(){
		var staffEventID = $(this).attr('data-StaffEventID');
		var eventID = $(this).attr('data-EventID');
		var conf = confirm('<?=$Lang['medical']['event']['Warning']['ConfirmDeleteStaffEvent']?>');
		if (conf) {
			if (staffEventID > 0) {
    			$.ajax({
    				dataType: "json",
    				type: "POST",
    				url: '?t=management.ajax.ajax',
    				data : {
    					'action': 'removeEventStaffEvent',
    					'staffEventID': staffEventID,
    					'eventID': eventID
    				},		  
    				success: function(ajaxReturn){
    					if (ajaxReturn != null && ajaxReturn.success){
    						removeEventStaffEventRow(staffEventID);
    					}
    				},
    				error: show_ajax_error
    			});
			}
			else {
				removeEventStaffEventRow(staffEventID);
			}
		}
	});

	$(document).on('click', '.viewStaffEvent', function(){
		var staffEventID = $(this).attr('data-StaffEventID');
		tb_show('<?=$Lang['medical']['event']['ViewStaffEvent']?>','?t=management.staff_event_view&staffEventID='+staffEventID+'&ViewMode=1&height=500&width=800');
	});
	
});

function removeEventCaseRow(caseID)
{
	$('#event_case_row_'+caseID).remove();
	if ($('#RelevantCaseTable tr').length == 1) {
		$('#RelevantCaseTable').css("display","none");
	}
}

function removeAllEventCases()
{
	$('tr[id^="event_case_row_"]').remove();
	$('#RelevantCaseTable').css("display","none");
	$('#LinkCaseBtn').css("display","none");
}

function removeEventStudentLogRow(studentLogID)
{
	$('#eventStudentLogRow'+studentLogID).remove();
	if ($('#relevantStudentLogTable tr').length == 1) {
		$('#relevantStudentLogTable').css("display","none");
	}
}

function checkForm() 
{
	var error = 0;
	var focusField = '';
	hideError();
 	if (!check_date($('#EventDate')[0],"<?=$Lang['General']['InvalidDateFormat']?>")) {
 		error++;
 		if (focusField == '') focusField = 'EventDate';
 	}
 	
	if($('#StudentID option').length==0) {    
 		error++;
 		if (focusField == '') focusField = 'StudentID';
 		$('#ErrStudentID').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
	if($('#EventTypeID').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'EventTypeID';
 		$('#ErrEventType').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
	if($.trim($('#Summary').val())=='')  {    
 		error++;
 		if (focusField == '') focusField = 'Summary';
 		$('#ErrSummary').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
 	
 	if ((($('#EndTimeHour').val() != '00') || ($('#EndTimeMin').val() != '00')) && (($('#EndTimeHour').val()+$('#EndTimeMin').val()) < ($('#StartTimeHour').val()+$('#StartTimeMin').val()))) {
 		error++;
 		if (focusField == '') focusField = 'EndTimeHour';
 		$('#ErrEndTime').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

//	if (($('#IsCaseYes').is(':checked')) && ($('#SelCaseID\\[\\]').length == 0)){
		if (($('#IsCaseYes').is(':checked')) && ($('.selectCase').length == 0)){
		
 		error++;
 		if (focusField == '') focusField = 'btnLinkCase';
 		$('#ErrCase').addClass('error_msg_show').removeClass('error_msg_hide');
	}

<? if ($objMedical->is_use_plupload()): ?>	
	if(!jsIsFileUploadCompleted()) {
		error++;
		$('#FileWarnDiv_event').html('<?=$Lang['medical']['upload']['jsWaitAllFilesUploaded']?>').show();
		setTimeout(hideFileWarningTimerHandler,5000);
	}
<? else: ?>
		$('#FileName').val(document.getElementById('File').value);
<? endif;?>

	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function hideError() 
{
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

function changeLevel() 
{
	if ($('#LocationLevelID').val() == '') {
		$('#LocationID').val('');
		$('#LocationSpan').html('');
	}
	else {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1',
			data : {
				'action': 'getLocation',
				'LocationLevelID': $('#LocationLevelID').val()
			},		  
			success: update_location,
			error: show_ajax_error
		});
	}
	
}

function update_student_list(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

function update_affected_person_list(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#AffectedPersonNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}	

function update_building_level(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#BuildingLevelSpan').html(ajaxReturn.html);
		$('#LocationID').val('');
		$('#LocationSpan').html('');
	}
}	

function update_location(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#LocationSpan').html(ajaxReturn.html);
	}
}	

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function hideFileWarningTimerHandler() 
{
	$('#FileWarnDiv_event').hide();
}

function FileDeleteSubmit(table, fileID) 
{
	conf = confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeAttachment',
				'table': table,
				'FileID': fileID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#current_attachment_' + table + '_edit').replaceWith(ajaxReturn.html);
					if (table == 'event_followup') {
						$('#ef_'+ajaxReturn.FollowupID).replaceWith(ajaxReturn.html2);
					}
				}
				
			},
			error: show_ajax_error
		});
	}
}

function edit_followup(followup_id) 
{
	tb_show('<?=$Lang['medical']['event']['EditFollowupAction']?>','?t=management.edit_followup&FollowupID='+followup_id+'&height=500&width=800');
}

function delete_followup(followup_id) 
{
	conf = confirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeEventFollowup',
				'FollowupID': followup_id
			},		  
			success: function(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
					$('#ef_'+followup_id).remove();
				} 
			},
			error: show_ajax_error
		});
	}
}

function hideFollowupLayer()
{
	$('#FollowupList').attr('style', 'display: none');
	$('.spanHideFollowup').attr('style', 'display: none');
	$('.spanShowFollowup').attr('style', 'margin-left:5px;');
}

function showFollowupLayer()
{
	$('#FollowupList').attr('style', '');
	$('.spanShowFollowup').attr('style', 'display: none');
	$('.spanHideFollowup').attr('style', 'margin-left:5px;');
}

function select_case()
{
	tb_show('<?=$Lang['medical']['event']['SelectCase']?>','?t=management.select_case&ViewMode=1&height=500&width=800');
}

function new_case() 
{
	newWindow("?t=management.new_case&popup=1",8);
}


function changeClassName() 
{
	var excludeStudentIDs = [];
	excludeStudentIDs = getExcludeStudentIDs();

	if ($('#ClassName').val() == '') {
		emptyStudentList();
	}
	else {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1',
			data : {
				'action': 'getStudentNameByClassWithFilter',
				'ClassName': $('#ClassName').val(),
				'ExcludeStudentID[]':excludeStudentIDs
			},		  
			success: update_student_list,
			error: show_ajax_error
		});
	}		
}

function changeGroup() 
{
	var excludeStudentIDs = [];
	excludeStudentIDs = getExcludeStudentIDs();

	if ($('#GroupID').val() == '') {
		emptyStudentList();
	}
	else {
    	$.ajax({
    		dataType: "json",
    		type: "POST",
    		url: '?t=management.ajax.ajax',
    		data : {
    			'action': 'getStudentNameByGroupIDWithFilter',
    			'GroupID': $('#GroupID').val(),
    			'ExcludeStudentID[]':excludeStudentIDs
    		},		  
    		success: update_student_list,
    		error: show_ajax_error
    	});
	}
}

function getExcludeStudentIDs()
{
	$('#StudentID').attr('checked',true);
	isLoading = true;
	$('#StudentNameSpan').html(loadingImg);
	var excludeStudentIDs = [];
	var i=0;
	$('#StudentID option').each(function() {
		if ($.trim($(this).val()) != '') {
			excludeStudentIDs[i++] = $(this).val();
		}
	});
	return excludeStudentIDs;	
}

function emptyStudentList() 
{
	$('#AvailableStudentID option').remove();
	isLoading = false;
	var blankOptionList = '<select name=AvailableStudentID[] id=AvailableStudentID style="min-width:200px; height:156px;" multiple>';
	blankOptionList += '</select>';
	$('#StudentNameSpan').html(blankOptionList);			
}

function update_student_list(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#StudentNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}

function view_staff_event(staffEventID) {
	tb_show('<?=$Lang['medical']['event']['ViewRelevantStaffEvent']?>','?t=management.staff_event_view&StaffEventID='+staffEventID+'&ViewMode=1&height=500&width=800');
}

function remove_staff_event_of_student_event(studentEventID, staffEventID)
{
	conf = confirm('<?=$Lang['medical']['event']['Warning']['ConfirmDeleteStaffEvent']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeStaffEventOfStudentEvent',
				'StudentEventID': studentEventID,
				'StaffEventID': staffEventID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#staff_event_row_'+staffEventID).remove();
					if ($('#relevantStaffEventTable tr').length == 1) {
						$('#relevantStaffEventTable').css("display","none");
					}
				}
			},
			error: show_ajax_error
		});
	}
}


/////////////////////////////////////////////////
// functions for affected person

function emptyAvailableAffectedPersonList() 
{
	$('#AvailableAffectedPersonID option').remove();
	isLoading = false;
	var blankOptionList = '<select name=AvailableAffectedPersonID[] id=AvailableAffectedPersonID style="min-width:200px; height:156px;" multiple>';
	blankOptionList += '</select>';
	$('#AffectedPersonNameSpan').html(blankOptionList);			
}

function getExcludeAffectedPersonIDs()
{
	$('#AffectedPersonID').attr('checked',true);
	isLoading = true;
	$('#AffectedPersonNameSpan').html(loadingImg);
	var excludeAffectedPersonIDs = [];
	var i=0;
	$('#AffectedPersonID option').each(function() {
		if ($.trim($(this).val()) != '') {
			excludeAffectedPersonIDs[i++] = $(this).val();
		}
	});
	return excludeAffectedPersonIDs;	
}

function changeAffectedPersonClassName() 
{
	var excludeAffectedPersonIDs = getExcludeAffectedPersonIDs();

	if ($('#AffectedPersonClassName').val() == '') {
		emptyAvailableAffectedPersonList();
	}
	else {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1',
			data : {
				'action': 'getAffectedPersonNameByClassWithFilter',
				'AffectedPersonClassName': $('#AffectedPersonClassName').val(),
				'ExcludeAffectedPersonIDs[]':excludeAffectedPersonIDs
			},		  
			success: updateAffectedPersonList,
			error: show_ajax_error
		});
	}		
}

function changeAffectedPersonGroup()
{
	var excludeAffectedPersonIDs = getExcludeAffectedPersonIDs();

	if ($('#AffectedPersonGroupID').val() == '') {
		emptyAvailableAffectedPersonList();
	}
	else {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax&ma=1',
			data : {
				'action': 'getAffectedPersonNameByGroupIDWithFilter',
				'AffectedPersonGroupID': $('#AffectedPersonGroupID').val(),
				'ExcludeAffectedPersonIDs[]':excludeAffectedPersonIDs
			},		  
			success: updateAffectedPersonList,
			error: show_ajax_error
		});
	}		
}

function getAvailableTeachingStaff()
{
	var excludeAffectedPersonIDs = getExcludeAffectedPersonIDs();

	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getAffectedPersonAvailableTeachingStaff',
			'ExcludeAffectedPersonIDs[]':excludeAffectedPersonIDs
		},		  
		success: updateAffectedPersonList,
		error: show_ajax_error
	});
}

function getAvailableNonTeachingStaff()
{
	var excludeAffectedPersonIDs = getExcludeAffectedPersonIDs();

	$.ajax({
		dataType: "json",
		type: "POST",
		url: '?t=management.ajax.ajax&ma=1',
		data : {
			'action': 'getAffectedPersonAvailableNonTeachingStaff',
			'ExcludeAffectedPersonIDs[]':excludeAffectedPersonIDs
		},		  
		success: updateAffectedPersonList,
		error: show_ajax_error
	});
}

function updateAffectedPersonList(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#AffectedPersonNameSpan').html(ajaxReturn.html);
		isLoading = false;
	}
}

</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>"
	enctype="multipart/form-data">
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['Identity']['Student']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$objMedical->getStudentSelection($rs_event['Student'])?><span
										class="error_msg_hide" id="ErrStudentID"><?=$Lang['medical']['event']['Warning']['SelectStudent']?></span></td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['EventDate']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$linterface->GET_DATE_PICKER("EventDate",(empty($rs_event['EventDate'])?date('Y-m-d'):$rs_event['EventDate']))?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['StartTime']?>
									</td>
									<td class="tabletext"><?=getTimeSel('StartTime', empty($rs_event['StartTime'])?0:substr($rs_event['StartTime'],0,2), empty($rs_event['StartTime'])?0:substr($rs_event['StartTime'],3,2), "", $minuteStep=1)?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['EndTime']?>
									</td>
									<td class="tabletext"><?=getTimeSel('EndTime', empty($rs_event['EndTime'])?0:substr($rs_event['EndTime'],0,2), empty($rs_event['EndTime'])?0:substr($rs_event['EndTime'],3,2), "", $minuteStep=1)?>
									<span class="error_msg_hide" id="ErrEndTime"><?=$Lang['medical']['event']['Warning']['StartTimeGtEndTime']?></span></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['AffectedPerson']?></td>
									<td class="tabletext"><?=$objMedical->getAffectedPersonSelection($rs_event['AffectedPerson'])?>
										<?=$linterface->GET_TEXTAREA("AffectedNonAccountHolder", $rs_event['AffectedNonAccountHolder'], 80, 4)?> (<?php echo $Lang['medical']['event']['NonAccountHolder'];?>)
									</td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['EventType']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$eventTypeSelection?><span
										class="error_msg_hide" id="ErrEventType"><?=$Lang['medical']['event']['Warning']['SelectEventType']?></span></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['Place']?></td>
									<td class="tabletext"><?=$buildingSelection?>&nbsp;<span
										id="BuildingLevelSpan"><?=$levelSelection?></span>&nbsp;<span
										id="LocationSpan"><?=$locationSelection?></span></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['Summary']?><span
										class="tabletextrequire">*</span></td>
									<td class="tabletext"><?=$linterface->GET_TEXTAREA("Summary", $rs_event['Summary'], 80, 10)?><span
										class="error_msg_hide" id="ErrSummary"><?=$Lang['medical']['event']['Warning']['InputSummary']?></span></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['Cause']?></td>
									<td class="tabletext"><?=$linterface->GET_TEXTAREA("Cause", $rs_event['Cause'], 80, 10)?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['RelevantCase']?></td>
									<td class="tabletext">
										<table width="100%" id="RelevantCaseTable" class="common_table_list" style="display:<?=$relevantCases ? '':'none'?>">
											<tr>
												<th width="15%"><?=$Lang['medical']['case']['tableHeader']['CaseNo']?></th>
												<th width="19%"><?=$Lang['Identity']['Student']?></th>
												<th width="15%"><?=$Lang['General']['StartDate']?></th>
												<th width="50%"><?=$Lang['medical']['case']['tableHeader']['Summary']?></th>
												<th>&nbsp;</th>
											</tr>
											<?=$relevantCases?>
										</table>
										<div>
											<input type="radio" name="IsCase" id="IsCaseYes" value="1"
												<?=$isCase?'checked':''?>> <label for="IsCaseYes"><?=$Lang['medical']['event']['RelevantCaseYes']?></label>
											<input type="radio" name="IsCase" id="IsCaseNo" value="0"
												<?=$isCase?'':'checked'?>> <label for="IsCaseNo"><?=$Lang['medical']['event']['RelevantCaseNo']?></label>
											<div id="LinkCaseBtn" style="display:<?=$isCase?'':'none'?>">
    										<?= $linterface->GET_ACTION_BTN($Lang['medical']['event']['NewCase'], "button","new_case();","btnNewCase", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;									
    										<?= $linterface->GET_ACTION_BTN($Lang['medical']['event']['SelectCase'], "button","select_case();","btnLinkCase", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
    											<span class="error_msg_hide" id="ErrCase"><?=$Lang['medical']['event']['Warning']['SelectCase']?></span>
											</div>
										</div>
									</td>
								</tr>

<?php if ($plugin['medical_module']['studentLog']):?>								
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['general']['relevantStudentLog']?></td>
									<td class="tabletext">
										<table width="100%" id="relevantStudentLogTable" class="common_table_list" style="display:<?=$relevantStudentLogs ? '':'none'?>">
											<tr>
												<th width="12%"><?=$Lang['General']['Date']?></th>
												<th width="8%"><?=$Lang['medical']['studentLog']['time']?></th>
												<th width="15%"><?=$Lang['Identity']['Student']?></th>
												<th width="15%"><?=$Lang['medical']['studentLog']['studentLog']?></th>
												<th width="15%"><?=$Lang['medical']['studentLog']['case']?></th>
												<th width="33%"><?=$Lang['General']['Remark']?></th>
												<th>&nbsp;</th>
											</tr>
											<?=$relevantStudentLogs?>
										</table>

										<div>
											<span class="table_row_tool"><a class="newBtn add"
												id="addStudentLogBtn"
												title="<?=$Lang['medical']['general']['linkRelevantStudentLog']?>"></a></span>
										</div>

									</td>
								</tr>
<?php endif;?>

<?php if ($plugin['medical_module']['staffEvents'] && $staffEventViewRight):?>								
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['general']['relevantStaffEvent']?></td>
									<td class="tabletext">
										<table width="100%" id="relevantStaffEventTable" class="common_table_list" style="display:<?=$relevantStaffEvent ? '':'none'?>">
											<tr>
												<th width="12%"><?=$Lang['General']['Date']?></th>
												<th width="8%"><?=$Lang['medical']['studentLog']['time']?></th>
												<th width="15%"><?=$Lang['Identity']['Staff']?></th>
												<th width="15%"><?=$Lang['medical']['staffEvent']['EventType']?></th>
												<th width="48%"><?=$Lang['General']['Remark']?></th>
												<th>&nbsp;</th>
											</tr>
											<?=$relevantStaffEvent?>
										</table>

										<div>
											<span class="table_row_tool"><a class="newBtn add"
												id="addStaffEventBtn"
												title="<?=$Lang['medical']['general']['linkRelevantStaffEvent']?>"></a></span>
										</div>

									</td>
								</tr>
<?php endif;?>								
								
								<!-- upload files -->
							<?=$objMedical->getAttachmentLayout('event', $rs_event['EventID'], $mode='edit', $plupload)?>
							
						</table>


							<div>
								<span id="spanShowFollowup" class="spanShowFollowup"
									style="display: none;"> <a
									href="javascript:showFollowupLayer();"><?=$Lang['medical']['event']['ShowFollowupAction']?></a>
								</span> <span id="spanHideFollowup" class="spanHideFollowup"
									style="display:; margin-left: 5px;"> <a
									href="javascript:hideFollowupLayer();"><?=$Lang['medical']['event']['HideFollowupAction']?></a>
								</span>
							</div> <!-- list of followup action -->
							<div id="FollowupList" class="followup_list"><?=$followup_list?></div>

							<table align="center" class="form_table_v30">
						<?if ($eventID):?>							
							<tr>
									<td column="2"><span class="table_row_tool"><a
											class="newBtn add" onclick="javascript:add_followup()"
											title="<?=$Lang['medical']['event']['NewFollowup']?>"></a><br></span>
										<a onclick="javascript:add_followup()"
										onmouseover="javascript:$(this).css('cursor','pointer');"
										onmouseout="javascript:$(this).css('cursor','auto');"><?=$Lang['medical']['event']['NewFollowup']?></a>
									</td>
								</tr>
						<?endif;?>
							<tr>
									<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
									<td width="80%">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<?= $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.event_list'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table> <br>
			</td>
		</tr>
	</table>
	<input type=hidden id="EventID" name="EventID" value="<?=$eventID?>">

<? if($use_plupload):?>
	<input type="hidden" name="TargetFolder" id="TargetFolder"
		value="<?=$tempFolderPath?>">
<? else: ?>
	<input type="hidden" name="FileName" id="FileName" value="">
<? endif; ?>

</form>
