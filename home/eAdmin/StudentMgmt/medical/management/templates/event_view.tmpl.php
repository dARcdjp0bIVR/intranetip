<?
/*
 * 2019-01-04 Cameron
 * - add relevant staff event list view
 * 
 * 2018-04-27 Cameron
 * - apply stripslashes to $eventTypeName
 *
 * 2018-03-29 Cameron
 * - show relevant student log in list view
 *
 * 2018-03-26 Cameron
 * - show relevant cases in list view
 *
 */
echo $linterface->Include_Thickbox_JS_CSS();
echo '<script language="JavaScript" src="' . $PATH_WRT_ROOT . 'templates/jquery/jquery-1.8.3.min.js"></script>';
?>

<style>
.followup_list {
	margin: 5px;
	padding: 0.2em;
	border: 1px solid #c6cc42;
	color: #222222;
}

.followup_list_header {
	background: #c6cc42 url(<?=$PATH_WRT_ROOT?>images/space.gif) repeat-x
		scroll 50% 50%;
	border: 1px solid #c6cc42;
	color: #000000;
	font-weight: bold;
	padding: 3px;
}
</style>

<script type="text/javascript">

function add_followup() {
	tb_show('<?=$Lang['medical']['event']['NewFollowup']?>','?t=management.new_followup&ma=1&EventID=<?=$eventID?>'+'&height=500&width=800');
}


function show_ajax_error() {
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function FileDeleteSubmit(table, fileID) {
	conf = confirm('<?=$Lang['General']['__Warning']['AreYouSureYouWouldLikeToDeleteThisFile']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeAttachment',
				'table': table,
				'FileID': fileID
			},		  
			success: function(ajaxReturn){
				if (ajaxReturn != null && ajaxReturn.success){
					$('#current_attachment_' + table + '_edit').replaceWith(ajaxReturn.html);
					if (table == 'event_followup') {
						$('#ef_'+ajaxReturn.FollowupID).replaceWith(ajaxReturn.html2);
					}
				}
				
			},
			error: show_ajax_error
		});
	}
}

function edit_followup(followup_id) {
	tb_show('<?=$Lang['medical']['event']['EditFollowupAction']?>','?t=management.edit_followup&ma=1&FollowupID='+followup_id+'&height=500&width=800');
}

function delete_followup(followup_id) {
	conf = confirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>');
	if (conf) {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '?t=management.ajax.ajax',
			data : {
				'action': 'removeEventFollowup',
				'FollowupID': followup_id
			},		  
			success: function(ajaxReturn) {
				if (ajaxReturn != null && ajaxReturn.success){
					$('#ef_'+followup_id).remove();
				} 
			},
			error: show_ajax_error
		});
	}
}

function hideFollowupLayer()
{
	$('#FollowupList').attr('style', 'display: none');
	$('.spanHideFollowup').attr('style', 'display: none');
	$('.spanShowFollowup').attr('style', 'margin-left:5px;');
}

function showFollowupLayer()
{
	$('#FollowupList').attr('style', '');
	$('.spanShowFollowup').attr('style', 'display: none');
	$('.spanHideFollowup').attr('style', 'margin-left:5px;');
}

// function view_case(caseID){
//	tb_show('<?=$Lang['medical']['event']['ViewCase']?>','?t=management.case_view&CaseID='+caseID+'&ViewMode=1&from=event&height=500&width=800');
// }

function view_staff_event(staffEventID) {
	tb_show('<?=$Lang['medical']['event']['ViewRelevantStaffEvent']?>','?t=management.staff_event_view&StaffEventID='+staffEventID+'&ViewMode=1&height=500&width=800');
}

$(document).ready(function(){
	$(document).on('click', '.viewCase', function(){
		var caseID = $(this).attr('data-CaseID');
		tb_show('<?=$Lang['medical']['event']['ViewCase']?>','?t=management.case_view&CaseID='+caseID+'&ViewMode=1&height=500&width=800');
	});

	$(document).on('click', '.viewStudentLog', function(){
		var studentLogID = $(this).attr('data-StudentLogID');
		tb_show('<?=$Lang['medical']['event']['ViewStudentLog']?>','?t=management.studentLogView&studentLogID='+studentLogID+'&ViewMode=1&height=500&width=800');
	});
	
});
</script>

<form name="form1" id="form1" method="post" action="<?=$form_action?>"
	enctype="multipart/form-data">
	<table width="98%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="5">
					<tr>
						<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td>
							<table align="center" class="form_table_v30">
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['Identity']['Student']?></td>
									<td class="tabletext"><?=$studentInfo?></td>
								</tr>
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['EventDate']?></td>
									<td class="tabletext"><?=$rs_event['EventDate'] ? $rs_event['EventDate'] : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['StartTime']?></td>
									<td class="tabletext"><?=$rs_event['StartTime']? substr($rs_event['StartTime'],0,5) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['EndTime']?></td>
									<td class="tabletext"><?=$rs_event['EndTime']? substr($rs_event['EndTime'],0,5) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['AffectedPerson']?></td>
									<td class="tabletext"><?=$affectedPersonInfo?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['EventType']?></td>
									<td class="tabletext"><?=$eventTypeName ? stripslashes($eventTypeName) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['Place']?></td>
									<td class="tabletext"><?=$locationInfo?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['Summary']?></td>
									<td class="tabletext"><?=$rs_event['Summary'] ? nl2br($rs_event['Summary']) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['Cause']?></td>
									<td class="tabletext"><?=$rs_event['Cause'] ? nl2br($rs_event['Cause']) : '-'?></td>
								</tr>

								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['event']['RelevantCase']?></td>
									<td class="tabletext">
										<table width="100%" id="RelevantCaseTable" class="common_table_list" style="display:<?=$relevantCases ? '':'none'?>">
											<tr>
												<th width="15%"><?=$Lang['medical']['case']['tableHeader']['CaseNo']?></th>
												<th width="19%"><?=$Lang['Identity']['Student']?></th>
												<th width="15%"><?=$Lang['General']['StartDate']?></th>
												<th width="50%"><?=$Lang['medical']['case']['tableHeader']['Summary']?></th>
											</tr>
										<?=$relevantCases?>
									</table>
										<div>
											<input type="radio" name="IsCase" id="IsCaseYes" value="1"
												<?=$rs_event['IsCase']?'checked':''?> disabled> <label
												for="IsCaseYes"><?=$Lang['medical']['event']['RelevantCaseYes']?></label>
											<input type="radio" name="IsCase" id="IsCaseNo" value="0"
												<?=$rs_event['IsCase']?'':'checked'?> disabled> <label
												for="IsCaseNo"><?=$Lang['medical']['event']['RelevantCaseNo']?></label>
										</div>
									</td>
								</tr>

<?php if ($plugin['medical_module']['studentLog']):?>								
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['general']['relevantStudentLog']?></td>
									<td class="tabletext">
										<table width="100%" id="relevantStudentLogTable" class="common_table_list" style="display:<?=$relevantStudentLogs ? '':'none'?>">
											<tr>
												<th width="12%"><?=$Lang['General']['Date']?></th>
												<th width="8%"><?=$Lang['medical']['studentLog']['time']?></th>
												<th width="15%"><?=$Lang['Identity']['Student']?></th>
												<th width="15%"><?=$Lang['medical']['studentLog']['studentLog']?></th>
												<th width="15%"><?=$Lang['medical']['studentLog']['case']?></th>
												<th width="33%"><?=$Lang['General']['Remark']?></th>
											</tr>
											<?=$relevantStudentLogs?>
										</table>
									</td>
								</tr>
<?php endif;?>								

<?php if ($plugin['medical_module']['staffEvents'] && $staffEventViewRight):?>								
								<tr valign="top">
									<td width="20%" valign="top" nowrap="nowrap"
										class="field_title"><?=$Lang['medical']['general']['relevantStaffEvent']?></td>
									<td class="tabletext">
										<table width="100%" id="relevantStaffEventTable" class="common_table_list" style="display:<?=$relevantStaffEvent ? '':'none'?>">
											<tr>
												<th width="12%"><?=$Lang['General']['Date']?></th>
												<th width="8%"><?=$Lang['medical']['studentLog']['time']?></th>
												<th width="15%"><?=$Lang['Identity']['Staff']?></th>
												<th width="15%"><?=$Lang['medical']['staffEvent']['EventType']?></th>
												<th width="48%"><?=$Lang['General']['Remark']?></th>
												<th>&nbsp;</th>
											</tr>
											<?=$relevantStaffEvent?>
										</table>
									</td>
								</tr>
<?php endif;?>								

								<!-- upload files -->
							<?=$objMedical->getAttachmentLayout('event', $rs_event['EventID'], $mode='view', $plupload)?>
							
						</table>


							<div>
								<span id="spanShowFollowup" class="spanShowFollowup"
									style="display: none;"> <a
									href="javascript:showFollowupLayer();"><?=$Lang['medical']['event']['ShowFollowupAction']?></a>
								</span> <span id="spanHideFollowup" class="spanHideFollowup"
									style="display:; margin-left: 5px;"> <a
									href="javascript:hideFollowupLayer();"><?=$Lang['medical']['event']['HideFollowupAction']?></a>
								</span>
							</div> <!-- list of followup action -->
							<div id="FollowupList" class="followup_list"><?=$followup_list?></div>

							<table align="center" class="form_table_v30">
						<?if ($eventID && !$ViewMode):?>							
							<tr>
									<td column="2"><span class="table_row_tool"><a
											class="newBtn add" onclick="javascript:add_followup()"
											title="<?=$Lang['medical']['event']['NewFollowup']?>"></a><br></span>
										<a onclick="javascript:add_followup()"
										onmouseover="javascript:$(this).css('cursor','pointer');"
										onmouseout="javascript:$(this).css('cursor','auto');"><?=$Lang['medical']['event']['NewFollowup']?></a>
									</td>
								</tr>
						<?endif;?>
						</table>
						</td>
					</tr>
					<tr>
						<td height="1" class="dotline"><img
							src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10"
							height="1"></td>
					</tr>
					<tr>
						<td align="right">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td align="center"><span>
									<? if ($ViewMode): ?>
										<?= $linterface->GET_ACTION_BTN($button_close, "button", "js_Hide_ThickBox();","btnClose", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>									
									<? else:?>
										<?= $linterface->GET_ACTION_BTN($button_edit, "button","window.location='?t=management.edit_event&EventID=$eventID'","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='?t=management.event_list'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
									<? endif;?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
				</table> <br>
			</td>
		</tr>
	</table>
	<input type=hidden id="EventID" name="EventID" value="<?=$eventID?>">

<? if($use_plupload):?>
	<input type="hidden" name="TargetFolder" id="TargetFolder"
		value="<?=$tempFolderPath?>">
<? else: ?>
	<input type="hidden" name="FileName" id="FileName" value="">
<? endif; ?>

</form>
