<form name="form1" id="form1" method="POST" action="?t=management.handover_list">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<colgroup>
    			<col style="width:70%">
    			<col style="width:30%">
    		</colgroup>
            <tr>
                <td colspan="2"><?=$toolbar ?></td>
            </tr>
			<tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">				
						<a href="javascript:editItem();"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:deleteItem();" class="tool_delete"><?php echo $Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</table>
	</div>
	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
                <?php echo $li->display(); ?>
                </td>
            </tr>
        </table>
    </div>
    	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type='hidden' name='action' id='action' value='' />
</form>
<script>
function editItem(){
	var idAry = [];
	$('#RecordID:checked').each(function(index, ele){
		idAry.push(($(ele).val()));
	});
	if(idAry.length == 0 || idAry.length > 1){
		alert(globalAlertMsg1);
	} else {
		var id = idAry.pop();
		window.location.href="?t=management.handover&RecordID=" + id;
	}
}
function deleteItem(){
	var idAry = [];
	$('#RecordID:checked').each(function(index, ele){
		idAry.push(($(ele).val()));
	});
	if(idAry.length > 0){
		cnf = confirm(globalAlertMsg3);
		if(cnf){
    		$('#form1').attr('action', '?t=management.handoverSave');
    		$('#action').val('delete');
    		$('#form1').submit();
		}
	} else {
		alert('<?php echo $Lang['General']['JS_warning']['SelectAtLeastOneRecord'];?>');
	}
}
</script>