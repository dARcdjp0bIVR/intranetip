<?php
// using:
/*
 * 	2017-06-23 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='EVENT_MANAGEMENT') || !$plugin['medical_module']['discipline']){
	header("Location: /");
	exit();
}

## handle attachment(s)
$ret = $objMedical->handleTempUploadFolder();
$tempFolderPath = $ret['TempFolderPath'];
$use_plupload = $objMedical->is_use_plupload();

$plupload = array();
$plupload['use_plupload'] = $use_plupload;
if($use_plupload) {
	$plupload['pluploadButtonId'] = 'UploadButton2';
	$plupload['pluploadDropTargetId'] = 'DropFileArea2';
	$plupload['pluploadFileListDivId'] = 'FileListDiv2';
	$plupload['pluploadContainerId'] = 'pluploadDiv2';
}

$action = 'newFollowupSave';
$eventID = $EventID;

include_once('templates/followup.tmpl.php');

if($use_plupload) {
	include_once('plupload_script.php');
}

?>