<?php 
// Using: 

if(
	( !$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_MANAGEMENT') || !$plugin['medical_module']['meal']) &&
	( !$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_MANAGEMENT') || !$plugin['medical_module']['sleep'])
){
	header("Location: /");
	exit();
}
$CurrentPage = "ManagementProblemRecord";
$TAGS_OBJ[] = array($Lang['medical']['menu']['problemRecord'], "", 0);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		


####### Generate HTML START #######
$startDate = $linterface->GET_DATE_PICKER($Name="startDate",$DefaultValue=$startDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$endDate = $linterface->GET_DATE_PICKER($Name="endDate",$DefaultValue=$endDate,$OtherMember="",$DateFormat="yy-mm-dd",$MaskFunction="",$ExtWarningLayerID="",$ExtWarningLayerContainer="",$OnDatePickSelectedFunction="",$ID="",$SkipIncludeJS=0, $CanEmptyField=0, $Disable=false, $cssClass="textboxnum");
$dateSelectionHTML = $startDate.$Lang['StaffAttendance']['To'].'&nbsp;' .$endDate;


$typeSelectionHTML = '<select id="type">';
if(
	($plugin['medical_module']['meal']) && 
	(!libMedicalShowInput::absentAllowedInput('meal')) && 
	($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='MEAL_MANAGEMENT'))
){
	if($type == 'meal'){
		$selected = 'selected';
	}else{
		$selected = '';
	}
	$typeSelectionHTML .= '<option value="Meal" '.$selected.' >'.$Lang['medical']['menu']['meal'].'</option>';
}
if(
	($plugin['medical_module']['bowel']) && 
	(!libMedicalShowInput::absentAllowedInput('bowel')) &&
	($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='BOWEL_MANAGEMENT'))
){
	if($type == 'bowel'){
		$selected = 'selected';
	}else{
		$selected = '';
	}
	$typeSelectionHTML .= '<option value="Bowel" '.$selected.' >'.$Lang['medical']['menu']['bowel'].'</option>';
}
if(
	($plugin['medical_module']['studentLog']) && 
	(!libMedicalShowInput::absentAllowedInput('studentLog')) &&
	($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_MANAGEMENT'))
){
	if($type == 'studentLog'){
		$selected = 'selected';
	}else{
		$selected = '';
	}
	$typeSelectionHTML .= '<option value="StudentLog" '.$selected.' >'.$Lang['medical']['menu']['studentLog'].'</option>';
}
if(
	($plugin['medical_module']['sleep']) && 
	(!libMedicalShowInput::absentAllowedInput('sleep')) &&
	($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_MANAGEMENT'))
){
	if($type == 'sleep'){
		$selected = 'selected';
	}else{
		$selected = '';
	}
	$typeSelectionHTML .= '<option value="Sleep" '.$selected.' >'.$Lang['medical']['menu']['studentSleep'].'</option>';
}
$typeSelectionHTML .= '</select>';
####### Generate HTML END #######

$linterface->LAYOUT_START($Msg);
include_once('templates/problemRecord.tmpl.php');
$linterface->LAYOUT_STOP();
?>