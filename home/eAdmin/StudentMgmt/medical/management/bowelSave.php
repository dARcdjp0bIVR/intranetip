<?php
//Using: 
/*
 * 	2017-07-17 [Cameron]
 * 		- Fix bug: apply stripslashes to remark field
 */
$objDB = new libdb();
$objBowelLog = new BowelLog($date);



//debug_r($_POST);exit;
$tableName = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_BOWEL_LOG"].date('Y', strtotime($_POST['date'])).'_'.date('m', strtotime($_POST['date']));
//$objBowelLog->createTempTable($tableName,$_POST['date']);

$sql = $objBowelLog->getCreateDatabaseSQL($tableName);
$objDB->db_db_query($sql);

$objDB->Start_Trans();

# Delete record
if(is_array($_POST['deleteReacordList'])){ // For delete wrong sleep record
	$recordIDArr = $_POST['deleteReacordList'];
	$dateArr = $_POST['deleteReacordDate'];
	foreach($recordIDArr as $index => $recordID){
		$date = $dateArr[$index];
		$objBowelLog->deleteRecord($dateArr[$index],$recordID);
	}
}else{
	$recordIDList = trim(trim(htmlentities($_POST['deleteReacordList'], ENT_QUOTES, 'UTF-8')),',');
	if(strlen($recordIDList) > 0){
		$recordIDArr = explode(',',$recordIDList);
		foreach($recordIDArr as $recordID){
			$objBowelLog->deleteRecord($date,$recordID);
		}
	}
}


# Update record
//$objBowelLog->removeDateRecord($tableName,$_POST['date']);
if(count($_POST['bowelRecord'])>0){
	//////////////// INPUT CHECKING /////////
	if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
		echo 'Date Format Error! Please select with format YYYY-MM-DD';
		return;
	}
	foreach($_POST['bowelRecord'] as $userID =>$bowelDetail){
		
		for($i=0,$iMax=count($bowelDetail['RecordID']);$i<$iMax;$i++)
		{
			$RecordTime = $_POST['date'] . ' ' . $bowelDetail['bowelTime']['hour'][$i] . ':' . $bowelDetail['bowelTime']['minute'][$i] . ':00';
			
			$objBowelLog = new BowelLog($RecordTime, $bowelDetail['RecordID'][$i]);
					$objBowelLog->setUserID($userID);
					$objBowelLog->setRecordTime($RecordTime);
					$objBowelLog->setBowelID(trim($bowelDetail['bowelID'][$i]));
//					$objBowelLog->setRemarks(trim( htmlentities($bowelDetail['remarks'][$i], ENT_QUOTES, 'UTF-8')));
					$objBowelLog->setRemarks(intranet_htmlspecialchars(trim(stripslashes($bowelDetail['remarks'][$i]))));
					$objBowelLog->setModifiedBy($_SESSION['UserID']);
					$objBowelLog->setInputBy($_SESSION['UserID']);
/* Unused
			$objBowelLog->addRecord($userID,
										$RecordTime,
										trim($bowelDetail['bowelID'][$i]),
										trim( htmlentities($bowelDetail['remarks'][$i], ENT_QUOTES, 'UTF-8')),
										$_SESSION['UserID']
										 );
			if($bowelDetail['RecordID'][$i] != ''){
				$rs = $objBowelLog->getDataFromTempTable($tableName,$bowelDetail['RecordID'][$i]);
error_log('$rs -->'.print_r($rs,true)."<----\n\t".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/debug_a.txt");

				$objBowelLog->setDateInput($rs[0]['DateInput']);
			}
			$objBowelLog->setInputBy($_SESSION['UserID']);
*/
			$result[] = $objBowelLog->save(false);
		}
	}
}

$finalResult = (!in_array( false, (array)$result));

if( $finalResult ){
	//$Msg = 'Save Succeeds';
	$objDB->Commit_Trans();
}
else{
	//$Msg = 'Save Fails';
	$objDB->RollBack_Trans();
}

//$objBowelLog->dropTempTable($tableName);

echo $finalResult;
//header('Location: ?t=management.bowel&Msg='.$Msg);
//exit;
?>