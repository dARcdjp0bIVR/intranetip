<?php
//Using: 

//$_POST['date'];
//$_POST['timePeriod'];

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SLEEP_MANAGEMENT') || !$plugin['medical_module']['sleep']){
	header("Location: /");
	exit();
}

$objDB = new libdb();
$objStudentSleepLog = new StudentSleepLog();
$result = array();

$objDB->Start_Trans();

# Delete record
if(is_array($_POST['deleteReacordList'])){ // For delete wrong sleep record
	$recordIDArr = $_POST['deleteReacordList'];
}else{
	$recordIDList = trim(trim(htmlentities($_POST['deleteReacordList'], ENT_QUOTES, 'UTF-8')),',');
	if(strlen($recordIDList) > 0){
		$recordIDArr = explode(',',$recordIDList);
	}
}
if(count($recordIDArr)>0){
	foreach($recordIDArr as $recordID){
		$objStudentSleepLog->deleteRecord($recordID);
	}
}

# Update record
if(count($_POST['sleepRecord'])>0){
	//////////////// INPUT CHECKING /////////
	if(!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['date'])){
		echo 'Date Format Error! Please select with format YYYY-MM-DD';
		$objDB->RollBack_Trans();
		return;
	}
	
	//debug_r($_POST['date']);
	
	foreach($_POST['sleepRecord'] as $sleepUserID =>$studentSleepDetail){	
		for($i=0,$iMax=count($studentSleepDetail['RecordID']);$i<$iMax;$i++)
		{
			$objStudentSleepLog = new StudentSleepLog($studentSleepDetail['RecordID'][$i]);
			$objStudentSleepLog->addRecord($sleepUserID, $_POST['date'],
			
										$studentSleepDetail['sleepStatus'][$i],
										$studentSleepDetail['sleepReason'][$i],
										$studentSleepDetail['frequence'][$i],
										trim( htmlentities($studentSleepDetail['remarks'][$i], ENT_QUOTES, 'UTF-8')),
										
										$_SESSION['UserID'],
										$studentSleepDetail['RecordID'][$i]
										 );
			$result[] = $objStudentSleepLog->save();
		}
	}
}

$finalResult = (!in_array( false, (array)$result));

if( $finalResult ){
	//$Msg = 'Save Succeeds';
	$objDB->Commit_Trans();
}
else{
	//$Msg = 'Save Fails';
	$objDB->RollBack_Trans();
}

echo $finalResult;
//header('Location: ?t=management.meal&Msg='.$Msg);
//exit;
?>