<?php
// using:
/*  2019-06-05 Philips
 *      - add Diagnosis, VisitDate, AccidentInjure,  DischargedAdmittion
 * 
 * 	2017-08-31 Cameron
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='REVISIT_MANAGEMENT') || !$plugin['medical_module']['revisit']){
	header("Location: /");
	exit();
}

$ldb = new libdb();
$result = array();

$dataAry['StudentID'] = $StudentID;
$dataAry['RevisitDate'] = $RevisitDate;
$dataAry['CuringStatus'] = standardizeFormPostValue($CuringStatus);
$dataAry['Hospital'] = standardizeFormPostValue($Hospital);
$dataAry['Division'] = standardizeFormPostValue($Division);
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
// new customized columns
if($sys_custom['medical']['MedicalReport']){
    $dataAry['Diagnosis'] = standardizeFormPostValue($Diagnosis);
    $dataAry['VisitDate'] = $VisitDate;
    $dataAry['AccidentInjure'] = $AccidentInjure;
    $dataAry['DischargedAdmittion'] = $DischargedAdmittion;
}
$sql = $objMedical->UPDATE2TABLE('MEDICAL_REVISIT',$dataAry,array('RevisitID'=>$RevisitID),false);

$ldb->Start_Trans();
## step 1: update revisit
$result[] = $ldb->db_db_query($sql);

if ($IsChangedDrug) {
	$originalDrug = $objMedical->getRevisitDrug($RevisitID);
	$originalDrugNameAry = array();
	foreach((array)$originalDrug as $k=>$v) {
		$originalDrugNameAry[] = $v['Drug'];
	}

	$stdDrugNameAry = array();
	foreach((array)$drug_name as $v) {
		$stdDrugNameAry[] = standardizeFormPostValue($v);
	}

	$drug_to_delete = array_diff($originalDrugNameAry,$stdDrugNameAry);
//debug_pr($drug_to_delete);	
//debug_pr($originalDrugNameAry);
//debug_pr($drug_name);
//debug_pr($dosage);
//echo '<br>unit<br>';
//debug_pr($drug_unit);
	
	## step 2: add / update drugs	
	for ($i=0,$iMax=count($drug_name);$i<$iMax;$i++) {
		unset($dataAry);
		if (in_array($stdDrugNameAry[$i],$originalDrugNameAry)) {	// record exist, update
			$dataAry['RevisitID'] = $RevisitID; 
			$dataAry['Drug'] = $stdDrugNameAry[$i];;
			$dataAry['Dosage'] = standardizeFormPostValue($dosage[$i]);;
			$dataAry['Unit'] = $drug_unit[$i];
			$sql = $objMedical->UPDATE2TABLE('MEDICAL_REVISIT_DRUG',$dataAry,array('RevisitID'=>$RevisitID, 'Drug'=>$stdDrugNameAry[$i]),false,false,false);	// no DateModified field
//debug_pr($sql);			
			$result[] = $ldb->db_db_query($sql);
		}
		else {	// insert
			$dataAry['RevisitID'] = $RevisitID; 
			$dataAry['Drug'] = standardizeFormPostValue($drug_name[$i]);;
			$dataAry['Dosage'] = standardizeFormPostValue($dosage[$i]);;
			$dataAry['Unit'] = $drug_unit[$i];
			$sql = $objMedical->INSERT2TABLE('MEDICAL_REVISIT_DRUG',$dataAry,array(),false,false,false);	// no DateModified field
//debug_pr($sql);			
			$result[] = $ldb->db_db_query($sql);
		}
	}

	## step 3: delete drugs
	if (count($drug_to_delete)) {
		foreach($drug_to_delete as $k=>$v) {
			$drug_to_delete[$k] = $ldb->Get_Safe_Sql_Query($v);
		}
		$sql = "DELETE FROM MEDICAL_REVISIT_DRUG WHERE RevisitID='".$RevisitID."' AND Drug IN ('".implode("','",$drug_to_delete)."')";
//debug_pr($sql);		
		$result[] = $ldb->db_db_query($sql);
	}
}

if (!in_array(false,$result)) {
	$ldb->Commit_Trans();
	$returnMsgKey = 'UpdateSuccess';
}
else {
	$ldb->RollBack_Trans();
	$returnMsgKey = 'UpdateUnsuccess';
}

header("location: ?t=management.revisit_list&returnMsgKey=".$returnMsgKey);

?>