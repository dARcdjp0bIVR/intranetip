<?php
// using:
/*
 * 2018-04-04 Cameron
 * - hide filter options only if both student log list and event list are shown in portal
 *
 * 2018-03-28 Cameron
 * - add filter by Group
 *
 * 2018-03-07 Cameron
 * - add hidden field event* and formName to distinguish common fields in listview (e.g. pageNo, order, field and page_size)
 * because there's other listview table in portal page
 * Note: this listview table will use default values when change criteria in another list view table
 *
 * 2018-03-06 Cameron
 * - create this file
 */
include_once ($PATH_WRT_ROOT . "lang/cust/medical_lang." . $intranet_session_language . ".php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/form_class_manage_ui.php");

if ($plugin['medical_module']['homepageHint']['event']) {
    $includeEventPath = $medicalPath . '/homepage/eventList.php';
}

if (! $objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'EVENT_MANAGEMENT') || ! $plugin['medical_module']['discipline'] || ! $plugin['medical_module']['homepageHint']['event']) {
    print $Lang['medical']['general']['AlertMessage']['noAccessRight'];
    exit();
}

// ############ start Filters
$AcademicYearID = Get_Current_Academic_Year_ID();

if ($_POST['EventByMethod'] == 'ByGroup') {
    $selectedByClass = '';
    $selectedByGroup = 'selected';
    $classStyle = 'style="display:none"';
    $groupStyle = '';
} else {
    $selectedByClass = 'selected';
    $selectedByGroup = '';
    $classStyle = '';
    $groupStyle = 'style="display:none"';
}

// Class Filter
$lclass = new libclass();
$classFilter = $lclass->getSelectClass("name='ClassName' id='ClassName' class='selectOption' $classStyle", $ClassName, "", $Lang['medical']['event']['AllClass'], $AcademicYearID);

// Group Filter
$groupFilter = $objMedical->getGroupSelection("EventGroupID", $EventGroupID, $groupStyle." class='selectOption'", $Lang['medical']['event']['AllGroup']);

// Event Type Filter
$eventTypeAry = $objMedical->getEventType();
$eventTypeFilter = getSelectByArray($eventTypeAry, "name='EventType' id='EventType' class='selectOption'", $EventType, 0, 0, $Lang['medical']['event']['AllCategory']);

$buildingAry = $objMedical->getInventoryBuildingArray();
$buildingFilter = getSelectByArray($buildingAry, "Name='LocationBuildingID' ID='LocationBuildingID'", $LocationBuildingID, 0, 0, $Lang['SysMgr']['Location']['All']['Building']);

// if ($LocationBuildingID) {
//     $levelAry = $objMedical->getInventoryLevelArray($LocationBuildingID);
//     $levelFilter = getSelectByArray($levelAry, "Name='LocationLevelID' ID='LocationLevelID' onChange='changeBuildingLevel();'", $LocationLevelID, 0, 0, $Lang['SysMgr']['Location']['All']['Floor']);
// }

// if ($LocationLevelID) {
//     $locationAry = $objMedical->getInventoryLocationArray($LocationLevelID);
//     $locationFilter = getSelectByArray($locationAry, "Name='LocationID' ID='LocationID' class='selectOption'", $LocationID, 0, 0, $Lang['SysMgr']['Location']['All']['Room']);
// }

if (! empty($ClassName)) {
    if ($junior_mck) {
        $studentAry = $lclass->getStudentNameListWClassNumberByClassName($ClassName);
    } else {
        $studentAry = $objMedical->getStudentNameListWClassNumberByClassName($ClassName);
    }
} else {
    $studentAry = $objMedical->getAllStudents($AcademicYearID);
}
// $studentFilter = getSelectByArray($studentAry, "name='EventStudent'", $EventStudent, 0, 0, $Lang['medical']['event']['AllStudent']);

$teacherAry = $objMedical->getTeacher();
$reporterFilter = getSelectByArray($teacherAry, "name='EventReporter' id='EventReporter' class='selectOption'", $EventReporter, 0, 0, $Lang['medical']['case']['AllReporter']);

$affectedPersonAry = array_merge($studentAry, $teacherAry);
$affectedPersonFilter = getSelectByArray($affectedPersonAry, "name='EventAffectedPerson' id ='EventAffectedPerson' class='selectOption'", $EventAffectedPerson, 0, 0, $Lang['medical']['event']['AllAffectedPerson']);

$EventDateStart = $EventDateStart ? $EventDateStart : substr(getStartDateOfAcademicYear($AcademicYearID), 0, 10);
$EventDateEnd = $EventDateEnd ? $EventDateEnd : substr(getEndDateOfAcademicYear($AcademicYearID), 0, 10);
// ############ end Filters

?>

<tr>
	<td>
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td height="24">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="8"><img
								src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_01.gif"
								width="8" height="24"></td>
							<td
								background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_02.gif">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td class="indexnewstitle"><?=$Lang['medical']['menu']['event']?></td>
										<td align="right">&nbsp;</td>
									</tr>
								</table>
							</td>
							<td width="8"><img
								src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_03.gif"
								width="8" height="24"></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<table width="100%" height="100%" border="0" cellpadding="0"
			cellspacing="0">
			<tr>
				<td width="5"
					background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_07.gif"
					width="5" height="5"></td>
				<td valign="top" bgcolor="#FFFFFF" height="100%">
<?php if ($plugin['medical_module']['homepageHint']['studentLog'] && $plugin['medical_module']['homepageHint']['event']):?>
	<?php $filterStyle = 'none'; ?>				
    				<span id="eventSpanShowOption" class="spanShowOption"
					style="display: '';"> <a href="#" id="showEventFilter"><?php echo $Lang['Btn']['ShowOption'];?></a>
				</span> <span id="eventSpanHideOption" class="spanHideOption"
					style="display: none;"> <a href="#" id="hideEventFilter"><?php echo $Lang['Btn']['HideOption'];?></a>
				</span>
<?php else:?>
	<?php $filterStyle = '';?>    				
<?php endif;?>				
					<form name="medicalEventForm" id="medicalEventForm" method="POST"
						action="">
						<div style="margin-top: 3px;display:<?php echo $filterStyle;?>;" id="medicalEventFilter">
							<span> <select name="EventByMethod" id="EventByMethod">
									<option value="ByClass" <?php echo $selectedByClass;?>><?php echo $Lang['Header']['Menu']['Class'];?></option>
									<option value="ByGroup" <?php echo $selectedByGroup;?>><?php echo $Lang['medical']['meal']['searchMenu']['group'];?></option>
							</select>
							</span> <span id="EventByMethodSpan">
						<?=$classFilter.$groupFilter?>
						</span>						
    					<?=$eventTypeFilter?>
    					<?=$buildingFilter?>
    					<?=$levelFilter?>
    					<?=$locationFilter?>&nbsp;<span id="BuildingLevelSpan"></span>&nbsp;<span id="LocationSpan"></span>
    					<?=$affectedPersonFilter?>
    					<?=$reporterFilter?>
    				
						<br style="clear: both" />
							<div>
								<input type="checkbox" name="EventDate" id="EventDate" value="1"
									<?=$EventDate==1 ? "checked" : ""?>><label for="EventDate"><?=$Lang['medical']['filter']['FilterDateRange']?></label>
								<div id="spanRequest" style="position:relative;display:<?=$EventDate==1 ? "inline" : "none"?>">
									<label for="EventDateStart"><?=$i_From?></label> : <?=$linterface->GET_DATE_PICKER("EventDateStart",$EventDateStart)?>
    					 <?=getTimeSel('TimeStart', ($TimeStartHour?$TimeStartHour:0), ($TimeStartMin?$TimeStartMin:0), "", $minuteStep=1)?>
    					<label for="EventDateEnd"><?=$i_To ?></label> <?=$linterface->GET_DATE_PICKER("EventDateEnd",$EventDateEnd)?>
    					 <?=getTimeSel('TimeEnd', ($TimeEndHour?$TimeEndHour:23), ($TimeEndMin?$TimeEndMin:59), "", $minuteStep=1)?></label>
    					 <?=$linterface->GET_ACTION_BTN($Lang['Btn']['View'], 'button',$ParOnClick="", $ParName="eventSearchBtn")?>
							</div>
							</div>
						</div>
						<div id="medicalEventResult">
						<?php include($includeEventPath); ?>
					</div>

					</form>
				</td>
				<td width="5"
					background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_09.gif"
					width="5" height="5"></td>
			</tr>
			<tr>
				<td width="5" height="6"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_10.gif"
					width="5" height="6"></td>
				<td height="6"
					background="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_11.gif"
					width="5" height="6"></td>
				<td width="5" height="6"><img
					src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/index/whatnews/news_12.gif"
					width="5" height="6"></td>
			</tr>
		</table>
	</td>
</tr>

<script>
function checkDateValid(start,end){
	// .replace() is for IE7 
	var _start = new Date(start.replace(/-/g,'/'));
	var _end = new Date(end.replace(/-/g,'/'));
	return start <= end;
}

$(document).ready(function(){
	$('#EventDate').click(function(){
		if ($(this).is(':checked')){
			$('#spanRequest').css('display','inline');
		}
		else {
			$('#spanRequest').css('display','none');
		}
	});

	$('#EventByMethod').change(function(){
		if ($('#EventByMethod').val() == 'ByClass') {
			$('#EventGroupID').css('display','none');
			$('#ClassName').css('display','');
			$('#EventGroupID').val('');
			getEventList();
		}
		else {
			$('#EventGroupID').css('display','');
			$('#ClassName').css('display','none');
			$('#ClassName').val('');
			getEventList();
		}
	});

 	$('.selectOption, #LocationID').change(function(e){
 		e.preventDefault();
 		getEventList();
 	});

 	$('#eventSearchBtn').click(function(e){
 		e.preventDefault();
 		getEventList();
 	});

 	$('#showEventFilter').click(function(e){
 		e.preventDefault();
 		$('#eventSpanShowOption').css('display','none');
 		$('#eventSpanHideOption').css('display','');
 		$('#medicalEventFilter').css('display','');
 	});

 	$('#hideEventFilter').click(function(e){
 		e.preventDefault();
 		$('#eventSpanShowOption').css('display','');
 		$('#eventSpanHideOption').css('display','none');
 		$('#medicalEventFilter').css('display','none');
 	});

	$('#LocationBuildingID').change(function(){
		if ($('#LocationBuildingID').val() == '') {
			$('#LocationLevelID').val('');
			$('#LocationID').val('');
			$('#BuildingLevelSpan').html('');
			$('#LocationSpan').html('');
			getEventList();
		}
		else {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '/home/eAdmin/StudentMgmt/medical/?t=management.ajax.ajax&ma=1',
				data : {
					'action': 'getBuildingLevel',
					'LocationBuildingID': $('#LocationBuildingID').val()
				},		  
				success: update_building_level,
				error: show_ajax_error
			});
		}
	});

	$('#EventDate').click(function(){
		if (!$(this).is(':checked')) {
			getEventList();
		}
	});
});

function getEventList()
{
	if(!checkDateValid($('#EventDateStart').val(),$('#EventDateEnd').val())){
		alert("<?php echo $Lang['medical']['report_bowel']['wrongDatePeriod']?>");
		return false;
	}
	else {	
        $.ajax({
        	dataType: "json",
        	type: "POST",
        	url: '/home/eAdmin/StudentMgmt/medical/?t=homepage.ajax.getEventList&ma=1',
        	data : $('#medicalEventForm').serialize(),		  
        	success: showEventList,
        	error: show_ajax_error
        });
	}
}

function showEventList(ajaxReturn) {
	if (ajaxReturn != null && ajaxReturn.success){
		$('#medicalEventResult').html(ajaxReturn.html);
	}
}	

function show_ajax_error() 
{
	alert('<?=$Lang['medical']['general']['error']['ajax']?>');
}

function update_building_level(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#BuildingLevelSpan').html(ajaxReturn.html);
		getEventList();
	}
}	

function changeLevel() 
{
	if ($('#LocationLevelID').val() == '') {
		$('#LocationID').val('');
		$('#LocationSpan').html('');
		getEventList();
	}
	else {
		$.ajax({
			dataType: "json",
			type: "POST",
			url: '/home/eAdmin/StudentMgmt/medical/?t=management.ajax.ajax&ma=1',
			data : {
				'action': 'getLocation',
				'onChange': 'changeLocationID()',
				'LocationLevelID': $('#LocationLevelID').val()
			},		  
			success: update_location,
			error: show_ajax_error
		});
	}
}

function changeLocationID(){
	getEventList();
}

function update_location(ajaxReturn) 
{
	if (ajaxReturn != null && ajaxReturn.success){
		$('#LocationSpan').html(ajaxReturn.html);
		getEventList();
	}
}	

// function changeBuilding() {
// 	$('#LocationLevelID').val('');
// 	$('#LocationID').val('');
// 	getEventList();
// }

// function changeBuildingLevel() {
// 	$('#LocationID').val('');
// 	getEventList();
// }

</script>
