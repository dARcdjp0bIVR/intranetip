<?php
/*
 * 	Log
 * 
 * 	2018-03-26 Cameron
 * 		- create this file
 */

if( (!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STAFFEVENT_SETTINGS')) || (!$plugin['medical_module']['staffEvents']) ){
    header("Location: /");
    exit();
}

$CurrentPage = "SettingsStaffEventType";

$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_staff_event_type'], "", 0);

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
</style>
<form method="post" name="form1" action="index.php">
<input type="hidden" name="t" value="settings.staffEventTypeSave">
<div id="ContentTableDiv">
<?php
	include_once("staffEventTypeList.php");
?>
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>

function js_show_event_type_setting_add_edit_layer(action) {
	var pass = true;
	var selEventTypeSettingID="";
	switch(action)
	{
		case "add":
			break;
		case "edit":
			if($('input[name="EventTypeSettingID\[\]"]:checked').length == 0)
			{				
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}
			else if($('input[name="EventTypeSettingID\[\]"]:checked').length > 1)
			{
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}		    
		    else
		    {		    	 
		    	selEventTypeSettingID = $('input[name="EventTypeSettingID\[\]"]:checked').val();
		    }
			break;
	}
	if (pass)
	{
		Hide_Return_Message();		
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',350, 650);
		$.post(
			"index.php?t=settings.ajax.ajax_get_staff_event_type_setting_add_edit_layer", 
			{ 
				EventTypeSettingID: selEventTypeSettingID
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
			}
		);
	}
}

function Check_EventTypeSetting_Form() {
	var EventTypeID 	= Trim($('#EventTypeID').val());
	var EventTypeName 	= Trim($('#EventTypeName').val());
	var RecordStatus	= $('input[name=RecordStatus]:checked','#EventTypeSettingForm').val();
	var isDefault 		= 0;
	if ($('#IsDefault').is(":checked"))
		isDefault 	= Trim($('#IsDefault').val());
	
	if (isMandatoryTextPass() && isEmptySelection()) {

		Block_Thickbox();
		
		// Add or Edit
		var action = '';
		var actionScript = '?t=settings.staffEventTypeSave';
		if (EventTypeID == '')
		{			
			action = "new";
		}
		else
		{			
			action = "update";
		}
		
		var checkScript = '?t=settings.ajax.ajax_check_staff_event_type_duplicate';
		$.post(
			checkScript,
			{
				EventTypeID: EventTypeID,
				EventTypeName: EventTypeName
			},
			function(ReturnData)
			{
				if(ReturnData == 'pass')
				{
					$.post(
						actionScript, 
						{ 
							EventTypeName: EventTypeName,				
							RecordStatus: RecordStatus,
							IsDefault: isDefault,
							EventTypeID: EventTypeID
						 },
						function(ReturnData)
						{
							js_Reload_Content_Table();
							
							// Show system messages
							js_Show_System_Message(action, ReturnData);
							
							UnBlock_Thickbox();
							js_Hide_ThickBox();
						}
					);
				}else{
					var errorStr = '';
					UnBlock_Thickbox();
					if(ReturnData & 1)
					{
						errorStr += '<?=$Lang['medical']['general']['duplicateCategory']?>\n'
					}
					alert(errorStr);
				}
			});
	}
}
	
// Reload Content Table
function js_Reload_Content_Table()
{
	$('#ContentTableDiv').load(		
		"?t=settings.ajax.ajax_reload_staff_event_type_setting",
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Load system message
function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "activate":
			returnMessage = (Status)? '<?=$Lang['medical']['general']['ReturnMessage']['Activate']['Success']?>' : '<?=$Lang['medical']['general']['ReturnMessage']['Activate']['Failed']?>';
			break;
		case "suspend":
			returnMessage = (Status)? '<?=$Lang['medical']['general']['ReturnMessage']['Suspend']['Success']?>' : '<?=$Lang['medical']['general']['ReturnMessage']['Suspend']['Failed']?>';
			break;
		case "new":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
			break;
		case "update":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
			break;
		case "delete":
			if (Status == 1) {			
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
			}
			else if (Status == "-1") {
				returnMessage = '<?=$Lang['medical']['general']['ReturnMessage']['Delete']['FailedWithReason']?>';
			}
			else {
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
			}
							
			break;
		
	}
	Get_Return_Message(returnMessage);
}
	
function isMandatoryTextPass(){
	var isFieldValid = true;
	var msg = "";
	if ($.trim($('#EventTypeName').val()) == "") {		
		isFieldValid= false;		
		msg = "<?=$Lang['medical']['general']['tableHeader']['Category']?>";
	}

	if (isFieldValid == false) {
		alert("<?=$Lang['medical']['general']['AlertMessage']['FillText']?>" + ": " + msg);		
	}
	
	return isFieldValid;
}

function isEmptySelection(){
	var isFieldValid = true;
	$('.radiobutton').each(function(){	
		var name = $(this).attr('name');
		if(!$('input[name="'+name+'"]').is(':checked')){
			isFieldValid = false;
		}
	});
	
	if (isFieldValid == false) {	
		alert("<?=$Lang['medical']['general']['AlertMessage']['SelectOption']?>");
	}
	return isFieldValid;
}


function js_handle_event_type_setting(task)
{
	if($('input[name="EventTypeSettingID\[\]"]:checked').length == 0)
	{
		alert(globalAlertMsg2);	// check at least one item	
	}
    else
    {
    	var cnf;
    	
    	switch(task)
    	{
    		case "activate":
    			cnf = confirm("<?=$Lang['medical']['general']['AlertMessage']['Activate']?>");
    			break;
    		case "delete":
    			cnf = confirm(globalAlertMsg3);
    			break;
    		case "suspend":
    			cnf = confirm(globalAlertMsg5);
    			break;
    	}
    	if (cnf)
    	{
		    var ids = [];
		    $('input[name="EventTypeSettingID\[\]"]:checked').each(function() {
		        ids.push($(this).val());	        
		    });
		    ids = ids.join(",");

			Hide_Return_Message();		
			$.post(
				"index.php?t=settings.ajax.ajax_handle_staff_event_type_setting", 
				{ 
					EventTypeID: ids,
					Task: task
				},
				function(ReturnData)
				{
					js_Show_System_Message(task, ReturnData);
					js_Reload_Content_Table();
				}
			);
    	}
    }
}

</script>
