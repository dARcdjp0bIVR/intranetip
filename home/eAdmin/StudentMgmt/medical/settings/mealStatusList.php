<?php
/*
 * 	using:  	
 */
//http://192.168.0.146:31002/home/common_choose/index.php?fieldname=student[]&page_title=SelectMembers&permitted_type=1
//$CurrentPage = "SettingsMealStatus";


//$objlibMedical = new libMedical();
//$mealSetting = $objlibMedical->getMealSetting();
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	

$objMs = new mealStatus();
$filter = "DeletedFlag<>1";	// don't show marked deleted records
$orderBy = "StatusCode";
$mealSetting = $objMs->getAllStatus($filter,$orderBy);
//debug_r($mealSetting);	
//$linterface->LAYOUT_START($Msg);
if (!isset($c_ui))
{
	$c_ui = new common_ui();	
}

?>

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:70%">
			<col style="width:30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool"><br style="clear:both">				
<?php						
	$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "add_dim", 
			$Lang['medical']['meal_setting']['button']['new_category'], "js_show_meal_setting_add_edit_layer('add'); return false;",$InlineID="FakeLayer","&nbsp;".$Lang['Btn']['New']);
	echo $x;
?>	
					</div>												
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">						
						<a href="javascript:js_handle_meal_setting('activate');" class="tool_approve"><?=$Lang['Status']['Activate']?></a>						
						<a href="javascript:js_handle_meal_setting('suspend');" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>						
						<a href="javascript:js_show_meal_setting_add_edit_layer('edit');"><?=$Lang['Btn']['Edit']?></a>
<?php						
//	$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "edit_dim", $Lang['Btn']['Edit'], 
//			"js_show_meal_setting_add_edit_layer('edit'); return false;",$InlineID="FakeLayer","&nbsp;".$Lang['Btn']['Edit']);
//	echo $x;								
?>
						<a href="javascript:js_handle_meal_setting('delete');" class="tool_delete"><?php echo $Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</thead>
	</table>	
	<table width="95%" border="0" cellspacing="0" cellpadding="0" class="common_table_list">
		<colgroup>
			<col style="width:4%">
			<col style="width:30%">
			<col style="width:15%">
			<col style="width:20%">
			<col style="width:15%">
			<col style="width:16%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal_setting']['tableHeader']['Category']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal_setting']['tableHeader']['Code']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal_setting']['tableHeader']['Status']; ?></th>
				<th class="tabletoplink"><?php echo $Lang['medical']['meal_setting']['tableHeader']['Default']; ?></th>
 				<th class="tabletoplink"><input type="checkbox" name="mealSettingsCheckAll" id="mealSettingsCheckAll"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if (count($mealSetting)>0)
			{			
				$i=0;
				foreach( (array)$mealSetting as $rs): ?>
				<tr>
					<td><?php echo ++$i;?></td>
					<td><?php echo '<div class="colorBoxStyle" style="background-color:'. stripslashes($rs['Color']) .'; float: left;">&nbsp;</div>&nbsp;'; echo ($rs['StatusName'] ? stripslashes($rs['StatusName']) : "--"); ?></td>
					<td><?php echo ($rs['StatusCode'] ? stripslashes($rs['StatusCode']) : "--"); ?></td>
					<td><?php echo ($rs['RecordStatus'] ? $Lang['medical']['meal_setting']['StatusOption']['InUse'] : $Lang['medical']['meal_setting']['StatusOption']['Suspend']); ?></td>
					<td><?php echo ($rs['IsDefault'] ? $linterface->Get_Tick_Image() : "&nbsp;"); ?></td>
					<td><input type="checkbox" name="MealSettingID[]" value=<?php echo $rs['StatusID']; ?> class="panelSelection"></td>
				</tr>
		<?php endforeach;
			}
			else
			{?>
				<tr>
					<td class=tableContent align=center colspan="6"><br><?=$Lang['SysMgr']['Homework']['NoRecord']?><br><br></td>
				</tr>
		<?php
			}?>
		</tbody>
	</table>
<?php
	if (count($mealSetting)>0)
	{			
		print ' <span class="form_sep_title">'.
					$Lang['medical']['meal_setting']['tableSorting'].
				'</span>';
	}
?>			
	
</div>
<!--
<script>
//	$('#mealSettingsCheckAll').click(function(){
//		$('.panelSelection').attr('checked', $(this).attr('checked'));
//	});
</script>
-->
<?php
//$linterface->LAYOUT_STOP();
?>