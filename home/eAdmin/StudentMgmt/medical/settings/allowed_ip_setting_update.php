<?php

# using by 
/*
 * 	Log
 * 
 * 	2017-08-29 [Cameron]
 * 		- create this file
 */

$rs = $objMedical->getMedicalGeneralSetting('AllowAccessIP');
$allowedIP = $rs ? $rs['SettingValue'] : '';

$ip_addresses = explode("\n", $allowedIP);
checkCurrentIP($ip_addresses, $Lang['medical']['module']);

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='ALLOWED_IP_SETTING')){
	header("Location: /");
	exit();
}
 

$ldb = new libdb();
$result = array();
$dataAry['Module'] = 'Medical'; 
$dataAry['SettingName'] = 'AllowAccessIP';
$dataAry['SettingValue'] = standardizeFormPostValue($AllowedIP);
$dataAry['ModifiedBy'] = $_SESSION['UserID'];
if ($rs) {	// record exist, update
	$condAry['Module'] = 'Medical';
	$condAry['SettingName'] = 'AllowAccessIP';
	$result[] = $objMedical->UPDATE2TABLE('GENERAL_SETTING',$dataAry,$condAry);
}
else {	// new, insert
	$dataAry['DateInput'] = 'now()';
	$dataAry['InputBy'] = $_SESSION['UserID'];
	$result[] = $objMedical->INSERT2TABLE('GENERAL_SETTING',$dataAry);	
}

$xmsg = in_array(false,$result) ? 'UpdateUnsuccess' : 'UpdateSuccess';

header("Location: ?t=settings.allowed_ip_setting&xmsg=$xmsg");

?>
