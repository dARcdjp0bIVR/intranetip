<?php
//	Using: 

//error_log("\n\nGetPost -->".print_r($_POST,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");

$statusID 		= trim($_POST['StatusID']);
$statusName 	= trim( htmlentities($_POST['StatusName'], ENT_QUOTES, 'UTF-8'));
$statusCode 	= trim( htmlentities($_POST['StatusCode'], ENT_QUOTES, 'UTF-8'));
$statusColor 	= trim( htmlentities($_POST['StatusColor'], ENT_QUOTES, 'UTF-8'));
$recordStatus 	= trim(isset($_POST['RecordStatus'])?$_POST['RecordStatus']:0);
$isDefault 		= trim(isset($_POST['IsDefault'])?$_POST['IsDefault']:0);


$objStudentSleepLev1 = new studentSleepLev1($statusID);
$objStudentSleepLev1->setStatusName($statusName);
$objStudentSleepLev1->setStatusCode($statusCode);
$objStudentSleepLev1->setIsDefault($isDefault);
$objStudentSleepLev1->setColor($statusColor);
$objStudentSleepLev1->setRecordStatus($recordStatus);


if ($statusID)
{
	$objStudentSleepLev1->setLastModifiedBy($_SESSION['UserID']);
}
else
{
	$objStudentSleepLev1->setInputBy($_SESSION['UserID']);
}
//debug_r($objStudentSleepLev1);
$result = $objStudentSleepLev1->save();
echo $result;
?>