<?php

# using by  
/*
 * 	2017-08-29 [Cameron]
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='ALLOWED_IP_SETTING')){
	header("Location: /");
	exit();
}
# Top menu highlight setting
$CurrentPage = "SettingsAllowedIPSetting";
$CurrentPageName = $Lang['medical']['menu']['settings_allowed_ip'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_allowed_ip'], "", 0);

# Left menu
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_allowed_ip'], "?t=settings.allowed_ip_setting");

$linterface->LAYOUT_START($returnMsg);


## retrieve AllowedIP
$rs = $objMedical->getMedicalGeneralSetting('AllowAccessIP');
$allowedIP = $rs ? $rs['SettingValue'] : '';

$x = '';
$current_ip=isset($_SERVER)?$_SERVER['REMOTE_ADDR']:$HTTP_SERVER_VARS['REMOTE_ADDR'];
$txt_ip_list=$linterface->GET_TEXTAREA("AllowedIP", $allowedIP);
$display_value=($allowedIP==''?$Lang['General']['EmptySymbol']:nl2br($allowedIP));
$instruction=$linterface->Get_Warning_Message_Box($Lang['General']['Instruction'],$Lang['Security']['TerminalIPSettingsDescription']);

$x = $instruction.'
<table class="form_table_v30">
	<tr>
		<td class="field_title">
			'.$Lang['Security']['TerminalIPList'].'
		</td>
		<td>
			<span class="tabletextremark">'.$Lang['Security']['TerminalIPInput'].'<br>'.$Lang['Security']['TerminalYourAddress'].': '.$current_ip.'</span><br />
			<div class="EditView" style="display:none">
				'.$txt_ip_list.'
			</div>
			<div class="DisplayView">'.$display_value.'</div>
		</td>
	</tr>
</table>';

$htmlAry['formTable'] =$x;


### buttons
$htmlAry['editBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Edit'], "button", "goEdit()", 'editBtn');
$htmlAry['submitBtn'] = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", 'submitBtn');
$htmlAry['resetBtn'] = $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();",'resetBtn');

?>
<script language="javascript" type="text/javascript">

$( document ).ready(function() {
  $('#submitBtn').hide();
  $('#resetBtn').hide();
});

function checkForm()
{
	var input_ip = document.getElementById('AllowedIP').value.Trim();
	var is_valid = true;
	
	if(input_ip != '') {
		var ip_ary = input_ip.split('\n');
		for(var i=0;i<ip_ary.length;i++) {
			if(!ip_ary[i].match(/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+|\[\d+-\d+\])(\/[0-9]+)*/)){
				is_valid = false;
				break;
			}
		}
	}
	if(!is_valid) {
		alert('<?=$Lang['Security']['WarnMsgArr']['InvalidIPAddress']?>');
		return false;
	}
	$('#form1').submit();
}

function goEdit(){
	$('.EditView').show();
	$('.DisplayView').hide();
	$('#editBtn').hide();
	$('#submitBtn').show();
	$('#resetBtn').show();
}

function goSubmit(){
	checkForm();
}

function ResetInfo()
{
	document.form1.reset();
	$('.EditView').hide();
	$('.DisplayView').show();
	$('#submitBtn').hide();
    $('#resetBtn').hide();
    $('#editBtn').show();
}

</script>

<form name="form1" id="form1" method="post" action="?t=settings.allowed_ip_setting_update">
	<div id="show_div">
	
		<div class="table_board">
			<?=$htmlAry['formTable']?>
			<p class="spacer"></p>
		</div>
		<br style="clear:both;" />
			
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=$htmlAry['editBtn']?>
			<?=$htmlAry['submitBtn']?>
			<?=$htmlAry['resetBtn']?>
			<p class="spacer"></p>
		</div>
	</div>

</form>

<?php
$linterface->LAYOUT_STOP();
?>

