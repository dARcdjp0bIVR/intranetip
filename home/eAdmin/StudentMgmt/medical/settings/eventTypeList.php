<?php
/*
 * 	Log
 * 
 * 	2017-06-15 Cameron
 * 		- create this file
 */
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();	

$objEt = new eventType();
$filter = "DeletedFlag<>1";		// don't show marked deleted records
$orderBy = "EventTypeName";
$eventTypeSetting = $objEt->getAllStatus($filter,$orderBy);

if (!isset($c_ui))
{
	$c_ui = new common_ui();	
}

?>

<div id="viewResult">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:70%">
			<col style="width:30%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">
					<div class="Conntent_tool"><br style="clear:both">				
<?php						
	$x = $linterface->Get_Thickbox_Link($c_ui->getThickBoxHeight(), $c_ui->getThickBoxWidth(), "add_dim", 
			$Lang['medical']['general']['button']['new_category'], "js_show_event_type_setting_add_edit_layer('add'); return false;",$InlineID="FakeLayer","&nbsp;".$Lang['Btn']['New']);
	echo $x;
?>	
					</div>												
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">						
						<a href="javascript:js_handle_event_type_setting('activate');" class="tool_approve"><?=$Lang['Status']['Activate']?></a>						
						<a href="javascript:js_handle_event_type_setting('suspend');" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>						
						<a href="javascript:js_show_event_type_setting_add_edit_layer('edit');"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:js_handle_event_type_setting('delete');" class="tool_delete"><?=$Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</thead>
	</table>	
	<table width="95%" border="0" cellspacing="0" cellpadding="0" class="common_table_list">
		<colgroup>
			<col style="width:4%">
			<col style="width:50%">
			<col style="width:30%">
			<col style="width:16%">
		</colgroup>
		<thead>
			<tr class="tabletop">
				<th class="tabletoplink">#</th>
				<th class="tabletoplink"><?=$Lang['medical']['general']['tableHeader']['Category']?></th>
				<th class="tabletoplink"><?=$Lang['medical']['message']['setting']['recordStatus']?></th>
 				<th class="tabletoplink"><input type="checkbox" name="eventTypeSettingsCheckAll" id="eventTypeSettingsCheckAll" onClick="(this.checked)?setChecked(1,this.form,'EventTypeSettingID[]'):setChecked(0,this.form,'EventTypeSettingID[]')"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if (count($eventTypeSetting)>0)
			{			
				$i=0;
				foreach( (array)$eventTypeSetting as $rs): ?>
				<tr>
					<td><?php echo ++$i;?></td>
					<td><?php echo ($rs['EventTypeName'] ? stripslashes($rs['EventTypeName']) : "--"); ?></td>
					<td><?php echo ($rs['RecordStatus'] ? $Lang['medical']['message']['setting']['StatusOption']['InUse'] : $Lang['medical']['message']['setting']['StatusOption']['Suspend']); ?></td>
					<td><input type="checkbox" name="EventTypeSettingID[]" value=<?php echo $rs['EventTypeID']; ?> class="panelSelection"></td>
				</tr>
		<?php endforeach;
			}
			else
			{?>
				<tr>
					<td class=tableContent align=center colspan="6"><br><?=$Lang['General']['NoRecordAtThisMoment']?><br><br></td>
				</tr>
		<?php
			}?>
		</tbody>
	</table>
	
</div>
