<?php
// using: 
/*
 *  2018-12-27 Cameron
 *      - handle STAFFEVENT_DELETE right [case #F142033]
 */


$userIDList = array();
foreach( (array)$_POST['r_group_user'] as $userID){
	$userIDList[] = ltrim($userID, 'U');
}

$acessRightList = $_POST['r_access_right'];

if ($plugin['medical_module']['staffEvents']) {
    if ($_POST['Delete__STAFFEVENT'] == 'self') {
        $acessRightList[] = 'STAFFEVENT_DELETESELF';
    }
    else if ($_POST['Delete__STAFFEVENT'] == 'all') {
        $acessRightList[] = 'STAFFEVENT_DELETEALL';
    }
}

if($r_gid == ''){
	$result = $objMedical->insertAcessRightGroup( $groupTitle =nl2br(htmlentities( $_POST['r_name'], ENT_QUOTES, 'UTF-8')), $acessRightList, $userIDList);	
	if($result){
		$Msg = $Lang['General']['ReturnMessage']['AddSuccess'];
	}
	else{
		$Msg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
	}
}
else{
	$result = $objMedical->updateAcessRightGroup($groupID = $_POST['r_gid'], $groupTitle = nl2br(htmlentities( $_POST['r_name'], ENT_QUOTES, 'UTF-8')), $acessRightList, $userIDList);
	if($result){
		$Msg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
	}
	else{
		$Msg =$Lang['General']['ReturnMessage']['UpdateUnsuccess'];
	}
}
header("Location: ?t=settings.accessRightList&Msg=".$Msg);
exit();

////////////////////////////////////////////////////////////////////////////
//
//$objDB = new libdb();
//if(is_array($_POST['r_access_right'])){
//	$access_right = implode(';',$_POST['r_access_right']);
//}
//
//$r_group_user = $_POST['r_group_user'];
//$r_name = trim($r_name);
//
//if($r_gid == ''){
//	$sql = 'insert into MEDICAL_ACCESS_GROUP(GroupTitle, GroupAccessRight) values(\''.$objDB->Get_Safe_Sql_Query($r_name).'\',\''.$access_right.'\')';
//
//}else{
//	$sql= 'update MEDICAL_ACCESS_GROUP set GroupTitle = \''.$objDB->Get_Safe_Sql_Query($r_name).'\', GroupAccessRight =\''.$access_right.'\' where GroupID = '.$r_gid;
//}
//
//
//if($sql == ''){
//	$errMsg = 'Suppose SQL cannot empty!'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
//	alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
//	return;
//}
//else{
//
//	$result = $objDB->db_db_query($sql);
//
//	if($result != 1){
//		$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
//		alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
//	}else{
//		if($r_gid == ''){
//			$gid = $objDB->db_insert_id();
//		}
//
//		//handle the user to the group
//		if(is_array($r_group_user)){			
//			$user_sql = array();
//			for($i = 0,$iMax = count($r_group_user);$i < $iMax; $i++){
//				$_userId = $r_group_user[$i];
//				$_userId = ltrim($_userId,'U');
//				$_userId = intval($_userId);
//				debug_r($_userId);
//				if($_userId > 0 )
//				{
//					$user_sql[] .= 		'('.$gid.','.$_userId.',now())';
//				}else{
//					//do nothing
//				}	
//			}
//			if(count($user_sql) >0){
//				$_sql  = implode(',',$user_sql);
//				$insert_sql = 'insert into MEDICAL_ACCESS_GROUP_MEMBER (GroupID,UserID,DateInput) value '.$_sql ;
//				debug_r($insert_sql);
//				$result2 = $objDB->db_db_query($insert_sql);
//				if($result2 != 1){
//					$errMsg = 'SQL Error! '.$sql.' error['.mysql_error().']'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
//					alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
//				}
//			}
//			
//			
//		}
//		
//
////		header("Location: ?t=settings.accessRight&Msg=good");
//		exit();
//	}
//}

?>