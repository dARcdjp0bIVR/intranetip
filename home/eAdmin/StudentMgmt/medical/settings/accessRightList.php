<?php 
// Using: Pun

if(!$objMedical->isSuperAdmin($_SESSION['UserID'])){
	header("Location: /");
	exit();
}

	$CurrentPage = "SettingsAccessRights";
	$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_accessRight'], "", 0);
	if($_POST['action'] =='deleteGroups'){
		$result = $objMedical->deleteAcessRightGroup($_POST['groupIDList']);
		if($result){
			$Msg =$Lang['General']['ReturnMessage']['DeleteSuccess'];
		}
		else{
			$Msg = $Lang['General']['ReturnMessage']['UnDeleteSuccess'];
		}
	};
	
	$groupResult = $objMedical->getAccessRightCount();	
	$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
	$linterface->LAYOUT_START($Msg);

# For testing empty records	
//	$groupResult = array();
?>
<style>
.tablelink{
	cursor:pointer;
}
//.tabletoplink{
//	font-weight: bold !important;
//}
</style>
<form method="post" action="index.php" name="accessRightForm" id="accessRightForm">

<div id="viewResult">
	
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<colgroup>
			<col style="width:4%">
			<col style="width:46%">
			<col style="width:35%">
			<col style="width:15%">
		</colgroup>
		<thead>
			<tr>
				<td valign="bottom" colspan="2">					
					<div class="Conntent_tool"><br style="clear:both">				
						<a name="newGroupBtn" class="tablelink" id="newGroupBtn" href="?t=settings.accessRight&r_action=new">&nbsp;<?php echo $Lang['Btn']['New'];?></a>
					</div>												
				</td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom" colspan="2">
					<div class="common_table_tool">
					<?php if(count($groupResult)>0): ?>
							<a class="tool_edit"  href="#" name="editGroupBtn" id="editGroupBtn"><?php echo $Lang['Btn']['Edit']; ?></a>
							<a class="tool_delete"  href="#" name="deleteGroupBtn" id="deleteGroupBtn"><?php echo $Lang['Btn']['Delete']; ?></a>
					<?php endif; ?>
					</div>
				</td>
			</tr>
		</thead>
	</table>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="common_table_list">
			<colgroup>
				<col style="width:4%">
				<col style="width:46%">
				<col style="width:35%">
				<col style="width:15%">
			</colgroup>
			
			<thead>
				<tr class="tabletop" cellpadding="5">
					<th class="tabletoplink">#</th>
					<th class="tabletoplink"><?php echo $Lang['medical']['accessRightView']['userType']; ?></th>
					<th class="tabletoplink"><?php echo $Lang['medical']['accessRightView']['noOfUsers']; ?></th>
					<th class="tabletoplink"><input type="checkbox" name="groupSettingsCheckAll" id="groupSettingsCheckAll"></th>
				</tr>
			</thead>
			<?php if(count($groupResult)>0): ?>	
			<tbody>
				<?php
					$i=0;
					foreach( $groupResult as $groupDetail): ?>
					<tr class="tableContent">
						<td><?php echo ++$i;?></td>
						<td class="viewArea"><a href="javascript:js_edit('<?="groupIDList_".$i?>');"><?php echo stripslashes($groupDetail['GroupTitle']); ?></a></td>
						<td><?php echo $groupDetail['TotalNum']; ?></td>
						<td><input type="checkbox" name="groupIDList[]" id="groupIDList_<?=$i?>" class="panelSelection" value="<?php echo $groupDetail['GroupID']; ?>"></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
			<?php else: ?>
			<tbody>
				<tr>
				<td colspan="4"  class="tableContent" align="center" >
					<?php echo $Lang['SysMgr']['Homework']['NoRecord']; ?>
				</td>
				</tr>
			</tbody>
			<?php endif; ?>
		</table>
		<input type="hidden" name="action" id="action">
		<input type="hidden" name="t" id="r_t">
	</div>
</form>
<style>
.viewArea{
	-ms-word-break: break-all;
	-webkit-hyphens: auto;
	-moz-hyphens: auto;
	
	word-break: break-all;
	word-break: break-word;
	hyphens: auto;
}	
</style>
<script>
	$('#groupSettingsCheckAll').click(function(){
		$('.panelSelection').attr('checked', $(this).attr('checked'));
	});

	$('#deleteGroupBtn').click(function(){
		$checkBoxCount = $('.panelSelection:checked').length;
		if( $checkBoxCount == 0){
			alert('請選擇下列的用戶類別。');
			return;
		}
		var flag = confirm ('你決定要刪除以下用戶類別嗎?');
		if (flag){
			$('#action').val('deleteGroups');
//			$('#accessRightForm').attr('action', '?t=settings.accessRightList');
			$('#r_t').val('settings.accessRightList');
			$('#accessRightForm').submit();
		}
	});

	$('#editGroupBtn').click(function(){
		$checkBoxCount = $('.panelSelection:checked').length;
		if( $checkBoxCount == 0){
			alert('請選擇下列的用戶類別。');
			return;
		}
		else if ($checkBoxCount >1){
			alert('請單選下列的用戶類別。');
			return;
		}
		//$('#accessRightForm').attr('action', '?t=settings.accessRight');
		$('#r_t').val('settings.accessRight');
		$('#accessRightForm').submit();
	
	});
	/*
	$('#newGroupBtn').click(function(){
		$('#accessRightForm').attr('action', '?t=settings.accessRight&r_action=new');
		$('#accessRightForm').submit();
	});
	*/
	function js_edit(obj)
	{
		$('.panelSelection').attr('checked', false);	// uncheck all check box
		$('#'+obj).attr('checked',true);
		//$('#accessRightForm').attr('action', '?t=settings.accessRight');
		$('#r_t').val('settings.accessRight');
		$('#accessRightForm').submit();
	}
</script>
<?
$linterface->LAYOUT_STOP();
?>
