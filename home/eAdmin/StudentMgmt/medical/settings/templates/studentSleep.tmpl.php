<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 * 
 *  2018-05-29 [Cameron]
 *      - fix: apply stripslashes to StatusName
 *      
 * 	2017-09-19 [Cameron] 
 * 		- add label for radio button for easy choose item
 */
?> 
	<div id="debugArea"></div>
	<div class="edit_pop_board edit_pop_board_reorder">
<?php	$linterface->Get_Thickbox_Return_Message_Layer();?>
		<div class="edit_pop_board_write">
			<form id="StudentSleep<?=$level?>Form" name="StudentSleep<?=$level?>Form" method="post">
				<table class="form_table">
					
					<?php if($level == 'Lev2') { ?>
					<!-- Lev1 Name -->
					<tr>
						<td><?=$Lang['medical']['report_studentlog2']['tableHeader']['type']?></td>
						<td>
							<?php echo stripslashes($StatusName);?>
						</td>
					</tr>
					<?php } ?>
					
					<!-- Category Name -->
					<tr>
						<td><?=$Lang['General']['Name']?><span class="tabletextrequire">*</span></td>
						<td>
							<input id="<?=$level?>Name" name="<?=$level?>Name" type="text" class="textbox" value="<?=$levName?>" />
							<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("NameWarningDiv");?>
						</td>
					<?php if($level == 'Lev1'){?>
						<td>
							<input type="text" id="StatusColor" name="StatusColor" value="<?=$statusColor?>" style="display:none;" />
						</td>
					<?php }?>
					</tr>

					<!-- Code -->
					<tr>
						<td><?=$Lang['General']['Code']?></td>
						<td>
							<input id="<?=$level?>Code" name="<?=$level?>Code" type="text" class="textbox" value="<?=$levCode?>" />
							<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv");?>
						</td>
					</tr>

					<!-- Status -->
					<tr>
						<td><?=$Lang['General']['Status2']?><span class="tabletextrequire">*</span></td>
						<td>
							<input type="radio" value="<?=$medical_cfg['general']['status']['InUse']['value']?>" name="RecordStatus" id='RecordStatusInUse' class="radiobutton" <?php echo $recordStatus == $medical_cfg['general']['status']['InUse']['value'] ? " checked=\"checked\"" : "";?>> <label for="RecordStatusInUse"><?=$medical_cfg['general']['status']['InUse']['lang']?></label>
							<input type="radio" value="<?=$medical_cfg['general']['status']['Suspend']['value']?>" name="RecordStatus" id='RecordStatusSuspend' class="radiobutton" <?php echo $recordStatus == $medical_cfg['general']['status']['Suspend']['value'] ? " checked=\"checked\"" : "";?>> <label for="RecordStatusSuspend"><?=$medical_cfg['general']['status']['Suspend']['lang']?></label>
							<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("StatusWarningDiv");?>
						</td>							
					</tr>

					<!-- Set as default -->
					<tr>
						<td><?=$Lang['medical']['general']['Default']?></td>
						<td>						
							<input id="IsDefault" name="IsDefault" type="checkbox" class="checkbox" value="1" <? echo $disableIsDefault; echo ( ($isDefault == "1") ? " checked" : "");?>/>
								<?php echo $linterface->Get_Thickbox_Warning_Msg_Div("IsDefaultWarningDiv");?>
						</td>
					</tr>

				<!-- Show remind message -->
				<?php
				if (!$enableIsDefault)
				{
				?>
					<tr>
						<td></td>
						<td>						
							<?=$Lang['medical']['general']['RemindMessage']['DefaultHasSet']?>
						</td>
						<td></td>
					</tr>
				<?php
				}
				?>
					
<?php
//debug_r($recordStatus);
//debug_r($medical_cfg['general']['status']['Suspend']['value']);
?>					
				</table>
				<input type="hidden" id="<?=$level?>ID" name="<?=$level?>ID" value="<?=$levID?>">
				
				<table align="center" width="100%" border="0" cellpadding="2" cellspacing="0">
					<tr>
						<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
						<td width="80%">&nbsp;</td>
					</tr>
				</table>				
			</form>
		</div>
		
		<div class="edit_bottom">
			<p class="spacer"></p>
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick="Check_StudentSleepSetting_Form()", $id="Btn_Save")?>&nbsp;
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick="js_Hide_ThickBox()", $id="Btn_Cancel")?>
			<p class="spacer"></p>
		</div>
	</div>

<?php if($level == 'Lev1'){?>
<script>
$(document).ready( function() {
	/*
	$.fn.colorPicker.defaultColors = [ '000000', '993300','333300', '000080', '333399', '333333', '800000', 'FF6600', '808000', '008000', '008080', '0000FF', '666699', '808080', 'FF0000', 'FF9900', '99CC00', '339966', '33CCCC', '3366FF', '800080', '999999', 'FF00FF', 'FFCC00', 'FFFF00', '00FF00', '00FFFF', '00CCFF', '993366', 'C0C0C0', 'FF99CC', 'FFCC99', 'FFFF99' , 'CCFFFF', '99CCFF', 'FFFFFF'];
	*/
	$.fn.colorPicker.defaultColors = [	'ffaaaa', 'ff5656', 'ff0000', 'bf0000', '7f0000', 'ffffff',
										'ffd4aa', 'ffaa56', 'ff7f00', 'bf5f00', '7f3f00', 'e5e5e5',
										'ffffaa', 'ffff56', 'ffff00', 'bfbf00', '7f7f00', 'cccccc',
										'd4ffaa', 'aaff56', '7fff00', '5fbf00', '3f7f00', 'b2b2b2',
										'aaffaa', '56ff56', '00ff00', '00bf00', '007f00', '999999',
										'aaffd4', '56ffaa', '00ff7f', '00bf5f', '007f3f', '7f7f7f',
										'aaffff', '56ffff', '00ffff', '00bfbf', '007f7f', '666666',
										'aad4ff', '56aaff', '007fff', '005fbf', '003f7f', '4c4c4c',
										'aaaaff', '5656ff', '0000ff', '0000bf', '00007f', '333333',
										'd4aaff', 'aa56ff', '7f00ff', '5f00bf', '3f007f', '000000'										
									];

	$('input#StatusColor').colorPicker();

});
</script>
<?php }?>