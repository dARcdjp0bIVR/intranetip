<?php
/*
 * 	using: 
 *  
 *  2018-03-27 Cameron
 *      create this file
 */

if( (!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STAFFEVENT_SETTINGS')) || (!$plugin['medical_module']['staffEvents']) ){
	header("Location: /");
	exit();
}

$eventTypeID = $_GET["EventTypeID"];        // level 1 ID
//debug_pr($eventTypeID);
if (!$eventTypeID)
{
	$errMsg = 'Null StudentSleepStatusID!'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
  	alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
	
	header("Location: /");
	exit();
}
else
{
    $objLev1 = new StaffEventLev1($eventTypeID);
    $eventTypeName = $objLev1->getEventTypeName();
}


$CurrentPage = "SettingsStaffEventType";

$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_staff_event_type'], "", 0);
$PAGE_NAVIGATION[] = array('<span class="tablelink">'.$Lang['medical']['menu']['settings_staff_event_type'].'</span>', "?t=settings.staffEventType");
$PAGE_NAVIGATION[] = array($eventTypeName, "");


$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
</style>
<form method="post" name="form1" action="index.php">
<input type="hidden" name="t" value="settings.studentSleepLev2Save">
	<table width="95%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
		</tr>
	</table>									

<div id="ContentTableDiv">
<?php
    include_once("staffEventTypeLev2List.php");
?>
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>

function jsShowStaffEventTypeLev2SettingAddEditLayer(action) {
	var pass = true;
	var selEventTypeLev2ID="";
	switch(action)
	{
		case "add":
			break;
		case "edit":
			if($('input[name="EventTypeLev2ID\[\]"]:checked').length == 0)
			{				
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}
			else if($('input[name="EventTypeLev2ID\[\]"]:checked').length > 1)
			{
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}		    
		    else
		    {		    	 
		    	selEventTypeLev2ID = $('input[name="EventTypeLev2ID\[\]"]:checked').val();
		    }
			break;
	}
	if (pass)
	{
		Hide_Return_Message();		
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',350, 650);
		$.post(
			"index.php?t=settings.ajax.ajax_get_staff_event_type_lev2_setting_add_edit_layer", 
			{ 
				EventTypeLev2ID: selEventTypeLev2ID,
				EventTypeID: <?=$eventTypeID?>
			},
			function(returnData)
			{
				$('#TB_ajaxContent').html(returnData);
			}
		);
	}
}

function checkStaffEventTypeLev2SettingForm() {
	var eventTypeLev2ID 	= Trim($('#EventTypeLev2ID').val());
	var eventTypeLev2Name 	= Trim($('#EventTypeLev2Name').val());
	var eventTypeID 		= <?=$eventTypeID?>;
	var recordStatus		= $('input[name=RecordStatus]:checked','#StaffEventTypeLev2SettingForm').val();
	var isDefault 			= 0;
	if ($('#IsDefault').is(":checked"))
		isDefault 	= Trim($('#IsDefault').val());

	if (isMandatoryTextPass() && isEmptySelection()) {

		Block_Thickbox();
		
		// Add or Edit
		var action = '';
		var actionScript = '?t=settings.staffEventTypeLev2Save';
		
		if (eventTypeLev2ID == '')
		{			
			action = "new";
		}
		else
		{			
			action = "update";
		}

		var checkScript = '?t=settings.ajax.ajax_check_staff_event_type_lev2_duplicate';
		$.post(
			checkScript,
			{
				EventTypeLev2ID: eventTypeLev2ID,
				EventTypeLev2Name: eventTypeLev2Name,				
				EventTypeID: eventTypeID
			},
			function(returnData)
			{
				if(returnData == 'pass')
				{
					$.post(
						actionScript, 
						{ 
							EventTypeLev2Name: eventTypeLev2Name,				
							RecordStatus: recordStatus,
							IsDefault: isDefault,
							EventTypeLev2ID: eventTypeLev2ID,
							EventTypeID: eventTypeID
						 },
						function(returnData)
						{
							jsReloadContentTable();
							jsShowSystemMessage(action, returnData);
							UnBlock_Thickbox();
							js_Hide_ThickBox();
						}
					);
				}
				else if(returnData == '1')
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory']?>');
				}
				else
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory'] . '\n' . $Lang['medical']['general']['duplicateCode']?>');
				}
			}
		);
	}
}
	
// Reload Content Table
function jsReloadContentTable()
{
	$('#ContentTableDiv').load(		
		"?t=settings.ajax.ajax_reload_staff_event_type_lev2_setting&EventTypeID="+"<?php echo $eventTypeID;?>",
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Load system message
function jsShowSystemMessage(action, status)
{
	var returnMessage = '';
	
	switch(action)
	{
		case "activate":
			returnMessage = (status)? '<?=$Lang['medical']['general']['ReturnMessage']['Activate']['Success']?>' : '<?=$Lang['medical']['general']['ReturnMessage']['Activate']['Failed']?>';
			break;
		case "suspend":
			returnMessage = (status)? '<?=$Lang['medical']['general']['ReturnMessage']['Suspend']['Success']?>' : '<?=$Lang['medical']['general']['ReturnMessage']['Suspend']['Failed']?>';
			break;
		case "new":
			returnMessage = (status)? '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
			break;
		case "update":
			returnMessage = (status)? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
			break;
		case "delete":
			if (status == 1) {			
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
			}
			else if (status == "-1") {
				returnMessage = '<?=$Lang['medical']['general']['ReturnMessage']['Delete']['FailedWithReason']?>';
			}
			else {
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
			}
			break;
		
	}
	Get_Return_Message(returnMessage);
}
	
function isMandatoryTextPass(){
	var isFieldValid = true;
	var msg = "";
	if ($.trim($('#EventTypeLev2Name').val()) == "") {
		isFieldValid= false;		
		msg = "<?=$Lang['General']['Name']?>";
	}
	
	if (isFieldValid == false) {
		alert("<?=$Lang['medical']['general']['AlertMessage']['FillText']?>" + ": " + msg);		
	}
	
	return isFieldValid;
}

function isEmptySelection(){
	var isFieldValid = true;
	$('.radiobutton').each(function(){	
		var name = $(this).attr('name');
		if(!$('input[name="'+name+'"]').is(':checked')){
			isFieldValid = false;
		}
	});
	
	if (isFieldValid == false) {	
		alert("<?=$Lang['medical']['general']['AlertMessage']['SelectOption'].": " .$Lang['General']['Status2']?>");
	}
	return isFieldValid;
}


function jsHandleEventTypeLev2Setting(task)
{
	if($('input[name="EventTypeLev2ID\[\]"]:checked').length == 0)
	{
		alert(globalAlertMsg2);	// check at least one item	
	}
    else
    {
    	var cnf;
    	switch(task)
    	{
    		case "activate":
    			cnf = confirm("<?=$Lang['medical']['general']['AlertMessage']['Activate']?>");
    			break;
    		case "delete":
    			cnf = confirm(globalAlertMsg3);
    			break;
    		case "suspend":
    			cnf = confirm(globalAlertMsg5);
    			break;
    	}
    	if (cnf)
    	{
		    var ids = [];
		    $('input[name="EventTypeLev2ID\[\]"]:checked').each(function() {
		        ids.push($(this).val());	        
		    });
		    ids = ids.join(",");

			Hide_Return_Message();		
			$.post(
				"index.php?t=settings.ajax.ajax_handle_staff_event_type_lev2_setting", 
				{ 
					EventTypeLev2ID: ids,
					Task: task
				},
				function(returnData)
				{
					jsShowSystemMessage(task, returnData);
					jsReloadContentTable();
				}
			);
    	}
    }
}

</script>
