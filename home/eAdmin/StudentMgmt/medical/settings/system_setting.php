<?php

# using by  
/*
 * 	2017-08-29 [Cameron]
 * 		- create this file
 */

if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='SYSTEM_SETTING')){
	header("Location: /");
	exit();
}


$settings = $objMedical->getGeneralSettings();

# Top menu highlight setting
$CurrentPage = "SettingsSystemSetting";
$CurrentPageName = $Lang['medical']['menu']['settings_system_setting'];
$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_system_setting'], "", 0);

# Left menu
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		

$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_system_setting'], "?t=settings.system_setting");

$linterface->LAYOUT_START($returnMsg);

?>
<script language="javascript" type="text/javascript">

function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}	
function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

</script>

<form name="form1" id="form1" method="post" action="?t=settings.system_setting_update">

<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>

<div class="table_board">

	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>
	<p class="spacer"></p>
	
	
	<div class="form_sub_title_v30"><em>- <span class="field_title"> <?=$Lang['medical']['general_setting']['GeneralSetting']?> </span>-</em>
		<p class="spacer"></p>
	</div>
	<table class="form_table_v30">
		<tr valign='top'>
			<td  class="field_title" nowrap><?= str_replace("<!--ModuleName-->", $Lang['medical']['module'], $Lang['Security']['AuthenticationSetting']) ?></td>
			<td>
				<span class="Edit_Hide"><?=($settings['AuthenticationSetting']? $i_general_yes:$i_general_no)?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="AuthenticationSetting" value="1" <?=$settings['AuthenticationSetting'] ? "checked":"" ?> id="AuthenticationSetting1"> <label for="AuthenticationSetting1"><?=$i_general_yes?></label> 
					<input type="radio" name="AuthenticationSetting" value="0" <?=$settings['AuthenticationSetting'] ? "":"checked" ?> id="AuthenticationSetting0"> <label for="AuthenticationSetting0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>
	</table>
</div>
<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
</div>

</form>

<?php
$linterface->LAYOUT_STOP();
?>

