<?php
// using:
/*
 * 2018-12-24 Cameron
 *  - customize staff event access right ($cfg_medical['ACCESS_RIGHT']['STAFFEVENT']) [case #F142033]
 *  
 * 2018-03-02 Cameron
 * - add bracket for DELETESELFRECORD
 */
$CurrentPage = "SettingsAccessRights";

// This page should not involve Msg(return message to User), so set it ''
$Msg = '';

// NO A ADD NEW FUNCTION, GET THE GROUP ID LIST
if ($r_action != 'new') {
    $groupID = is_array($_POST['groupIDList']) ? $_POST['groupIDList'][0] : $_POST['groupIDList'];
}

$PAGE_NAVIGATION[] = array(
    '<span class="tablelink">' . $Lang['medical']['menu']['settings_accessRight'] . '</span>',
    "?t=settings.accessRightList"
);
if (trim($groupID) != '') {
    list ($acessRightGroupDetail, $studentDetailList) = $objMedical->getAcessRightGroupDetail($groupID);
    $PAGE_NAVIGATION[] = array(
        stripslashes($acessRightGroupDetail['GroupTitle']),
        ""
    );
} else {
    $PAGE_NAVIGATION[] = array(
        $i_Discipline_System_Group_Right_Navigation_New_Group,
        ""
    );
}
$groupAccessArray = explode(':', $acessRightGroupDetail['GroupAccessRight']);
$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

// loop for each module
if (count($cfg_medical['ACCESS_RIGHT']) > 0) {
    foreach ($cfg_medical['ACCESS_RIGHT'] as $module => $mod_details) {
        $checkBox = '';
        // loop for each sub module
        foreach ($mod_details as $sub_mod => $sub_mod_details) {
            if ($sub_mod == 'DELETESELF' || $sub_mod == "DELETEALL") {
                continue;
            }
            $selected = '';
            if (in_array($sub_mod_details['RIGHT'], $groupAccessArray)) {
                $selected = 'checked';
            }
            
            if ($sub_mod == 'DELETESELFRECORD') {
                $openBracket = '(';
                $closeBracket = ')';
            } else {
                $openBracket = '';
                $closeBracket = '';
            }
            
            if ($sub_mod == 'DELETE') {
                $selfCheck = in_array($cfg_medical['ACCESS_RIGHT'][$module]['DELETESELF']['RIGHT'], $groupAccessArray) ? ' checked' : '';
                $allCheck = in_array($cfg_medical['ACCESS_RIGHT'][$module]['DELETEALL']['RIGHT'], $groupAccessArray) ? ' checked' : '';
                
                $radioInput = '(';
                $radioInput .= '<input name="Delete__'.$module.'" type="radio" id="DelSelf__'.$module.'" value="self"'.$selfCheck.'><label for="DelSelf__'.$module.'">'.$Lang['medical']['accessRight']['Option']['Self'].'</label>';
                $radioInput .= '<input name="Delete__'.$module.'" type="radio" id="DelAll__'.$module.'" value="all"'.$allCheck.'><label for="DelAll__'.$module.'">'.$Lang['medical']['accessRight']['Option']['All'].'</label>';
                $radioInput .= ')&nbsp;&nbsp;';
            }
            else {
                $radioInput = '';
            }
            
            if ($sub_mod == "REPORTPRINT" && $sys_custom['medical']['accessRight']['PrintAndExport']){
                $checkBox .= " (";
            }
            
            $checkBox .= '<label for="accessRight__' . $sub_mod_details['RIGHT'] . '">' . $openBracket . '<input type="checkbox" name="r_access_right[]" value = "' . $sub_mod_details['RIGHT'] . '" id="accessRight__' . $sub_mod_details['RIGHT'] . '" ' . $selected . '>' . $sub_mod_details['LANG'] . $closeBracket;
            if ($sub_mod == "REPORTEXPORT" && $sys_custom['medical']['accessRight']['PrintAndExport']){
                $checkBox .= ") ";
            }
            $checkBox .= '&nbsp;&nbsp;' . "</label>\n";
            $checkBox .= $radioInput;
        }
        $h .= '<tr><td class="field_title" width="30%">' . $Lang['medical']['accessRight']['selectTitle'][$module] . '</td><td width="70%">' . $checkBox . '</td></tr>' . "\n";
    }
}

$array_student = array();
foreach ((array) $studentDetailList as $studentDetail) {
    $array_student[] = array(
        'U' . $studentDetail['UserID'],
        $studentDetail['Name']
    );
}
$user_selected = $linterface->GET_SELECTION_BOX($array_student, "name='r_group_user[]' id='r_group_user[]' class='select_studentlist' size='15' style='width:150px' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.getElementById('r_group_user[]'))");

$linterface->LAYOUT_START($Msg);
include_once ('templates/accessRight.tmpl.php');
$linterface->LAYOUT_STOP();
?>