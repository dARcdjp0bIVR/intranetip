<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 */

 
if( (!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='STUDENTLOG_SETTINGS')) || (!$plugin['medical_module']['studentLog']) ){
	header("Location: /");
	exit();
}

$CurrentPage = "SettingsStudentLog";


$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_studentlog'], "", 0);
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_studentlog'], "?t=settings.studentLog");
//$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
$linterface->LAYOUT_START($Msg);

$c_ui = new common_ui();
$js_css = $c_ui->Include_JS_CSS();
echo $js_css;
?>
<style type="text/css">
<?php
include_once($PATH_WRT_ROOT."home/eAdmin/StudentMgmt/medical/css/medical.css");
?>
</style>
<form method="post" name="form1" action="index.php">
<input type="hidden" name="t" value="settings.studentLogSave">
<div id="ContentTableDiv">
<?php
	include_once("studentLogList.php");
?>
</div>
</form>

<?php
$linterface->LAYOUT_STOP();
?>
<script>

function js_show_studentlog_setting_add_edit_layer(action) {
	var pass = true;
	var selStudentLogLev1ID="";
	switch(action)
	{
		case "add":
			break;
		case "edit":
			if($('input[name="StudentLogLev1ID\[\]"]:checked').length == 0)
			{				
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}
			else if($('input[name="StudentLogLev1ID\[\]"]:checked').length > 1)
			{
				alert(globalAlertMsg1);	// select only one
			    pass = false;
			}		    
		    else
		    {		    	 
		    	selStudentLogLev1ID = $('input[name="StudentLogLev1ID\[\]"]:checked').val();
		    }
			break;
	}
	if (pass)
	{
		Hide_Return_Message();		
		js_Show_ThickBox('<?=$Lang['Btn']['Edit']?>',350, 650);
		$.post(
			"index.php?t=settings.ajax.ajax_get_studentlog_setting_add_edit_layer", 
			{ 
				StudentLogLev1ID: selStudentLogLev1ID
			},
			function(ReturnData)
			{
				$('#TB_ajaxContent').html(ReturnData);
			}
		);
	}
}

function Check_StudentLogSetting_Form() {
	var Lev1ID 			= Trim($('#Lev1ID').val());
	var Lev1Name 		= Trim($('#Lev1Name').val());
	var Lev1Code 		= Trim($('#Lev1Code').val());
	var RecordStatus	= $('input[name=RecordStatus]:checked','#StudentLogLev1Form').val();
		
	if (isMandatoryTextPass() && isEmptySelection()) {

		Block_Thickbox();
		
		// Add or Edit
		var action = '';
		var actionScript = '?t=settings.studentLogSave';
		if (Lev1ID == '')
		{			
			action = "new";
		}
		else
		{			
			action = "update";
		}

		var checkScript = '?t=settings.ajax.ajax_check_studentloglev_duplicate_NameCode';
		$.post(
			checkScript,
			{
				StudentLogLev: '1',
				StudentLogLevID: Lev1ID,
				StudentLogLevName: Lev1Name,				
				StudentLogLevCode: Lev1Code
			},
			function(ReturnData)
			{
				if(ReturnData == 'pass')
				{
					$.post(
						actionScript, 
						{ 
							Lev1Name: Lev1Name,				
							Lev1Code: Lev1Code,
							RecordStatus: RecordStatus,
							Lev1ID: Lev1ID
						 },
						function(ReturnData)
						{
							js_Reload_Content_Table();
							js_Show_System_Message(action, ReturnData);
							UnBlock_Thickbox();
							js_Hide_ThickBox();
						}
					);
				}
				else if(ReturnData == '1')
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory']?>');
				}
				else if(ReturnData == '2')
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCode']?>');
				}
				else
				{
					UnBlock_Thickbox();
					alert('<?=$Lang['medical']['general']['duplicateCategory'] . '\n' . $Lang['medical']['general']['duplicateCode']?>');
				}
			}
		);
	}
}
	
// Reload Content Table
function js_Reload_Content_Table()
{
	$('#ContentTableDiv').load(		
		"?t=settings.ajax.ajax_reload_studentlog_setting",
		function(ReturnData)
		{
			initThickBox();
		}
	);
}

// Load system message
function js_Show_System_Message(Action, Status)
{
	var returnMessage = '';
	
	switch(Action)
	{
		case "activate":
			returnMessage = (Status)? '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Activate']['Success']?>' : '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Activate']['Failed']?>';
			break;
		case "suspend":
			returnMessage = (Status)? '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Suspend']['Success']?>' : '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Suspend']['Failed']?>';
			break;
		case "new":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['AddSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['AddUnsuccess']?>';
			break;
		case "update":
			returnMessage = (Status)? '<?=$Lang['General']['ReturnMessage']['UpdateSuccess']?>' : '<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>';
			break;
		case "delete":
			if (Status == 1) {			
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteSuccess']?>';
			}
			else if (Status == "-1") {
//				returnMessage = '<?=$Lang['medical']['studentlog_setting']['ReturnMessage']['Delete']['FailedWithReason']?>';
				returnMessage = '<?=$Lang['medical']['general']['ReturnMessage']['Delete']['FailedWithReason']?>';
			}
			else {
				returnMessage = '<?=$Lang['General']['ReturnMessage']['DeleteUnsuccess']?>';
			}
			break;
		
	}
	Get_Return_Message(returnMessage);
}
	
function isMandatoryTextPass(){
	var isFieldValid = true;
	var msg = "";

	if ($.trim($('#Lev1Name').val()) == "") {
		isFieldValid= false;		
		msg = "<?=$Lang['General']['Name']?>";
	}
	
	if (isFieldValid == false) {
		alert("<?=$Lang['medical']['general']['AlertMessage']['FillText']?>" + ": " + msg);		
	}
	
	return isFieldValid;
}

function isEmptySelection(){
	var isFieldValid = true;
	$('.radiobutton').each(function(){	
		var name = $(this).attr('name');
		if(!$('input[name="'+name+'"]').is(':checked')){
			isFieldValid = false;
		}
	});
	
	if (isFieldValid == false) {	
		alert("<?=$Lang['medical']['general']['AlertMessage']['SelectOption'].": " .$Lang['General']['Status2']?>");
	}
	return isFieldValid;
}


function js_handle_studentlog_setting(task)
{
	if($('input[name="StudentLogLev1ID\[\]"]:checked').length == 0)
	{
		alert(globalAlertMsg2);	// check at least one item	
	}
    else
    {
    	var cnf;
    	
    	switch(task)
    	{
    		case "activate":
    			cnf = confirm("<?=$Lang['medical']['general']['AlertMessage']['Activate']?>");
    			break;
    		case "delete":
    			cnf = confirm(globalAlertMsg3);
    			break;
    		case "suspend":
    			cnf = confirm(globalAlertMsg5);
    			break;
    	}
    	if (cnf)
    	{
		    var ids = [];

		    var deleteSuccess = true;
		    $('input[name="StudentLogLev1ID\[\]"]:checked').each(function() {
				if(task == "delete"){
					if($(this).closest('td').prev().prev().html()>0){
						alert('<?=$Lang['medical']['general']['deleteCategoryContainsItem']?>');
						deleteSuccess = false;
						return;
					}
				}
			    
		        ids.push($(this).val());
		    });
		    ids = ids.join(",");

			Hide_Return_Message();	
			if(deleteSuccess){	
				$.post(
					"index.php?t=settings.ajax.ajax_handle_studentlog_setting", 
					{ 
						StudentLogLev1ID: ids,
						Task: task
					},
					function(ReturnData)
					{
						js_Show_System_Message(task, ReturnData);
						js_Reload_Content_Table();
					}
				);
			}else{
				js_Show_System_Message(task, "");
			}
    	}
    }
}


$('#studentLogSettingsCheckAll').live('click',function(){
	$('.panelSelection').attr('checked', $(this).attr('checked'));
});


</script>
