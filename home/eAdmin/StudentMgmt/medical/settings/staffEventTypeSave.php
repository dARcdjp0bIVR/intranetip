<?php
/*
 * Log
 *
 * 2018-03-26 Cameron
 * - create this file
 */
$eventTypeID = trim($_POST['EventTypeID']);
//$eventTypeName = trim(htmlentities($_POST['EventTypeName'], ENT_QUOTES, 'UTF-8'));
$eventTypeName = standardizeFormPostValue(trim($_POST['EventTypeName']));
$recordStatus = trim(isset($_POST['RecordStatus']) ? $_POST['RecordStatus'] : 0);
$isDefault = trim(isset($_POST['IsDefault']) ? $_POST['IsDefault'] : 0);

$objEt = new StaffEventLev1($eventTypeID);

$objEt->setEventTypeName($eventTypeName);
$objEt->setRecordStatus($recordStatus);
$objEt->setIsDefault($isDefault);
if ($eventTypeID) {
    $objEt->setLastModifiedBy($_SESSION['UserID']);
} else {
    $objEt->setInputBy($_SESSION['UserID']);
}

$result = $objEt->save();
echo $result;
?>