<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 */

 
if( !$sys_custom['medical']['Handover'] || !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_SETTINGS'))){
	header("Location: /");
	exit();
}

$CurrentPage = "SettingsHandover";


$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_handover'], "", 0);
//$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

if (isset($ck_page_size) && $ck_page_size != "")
    $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 0 : $order; // default descending
$field = ($field == "") ? 0 : $field;
$li = new libdbtable2007($field, $order, $pageNo);

$cols = "
    CONCAT('<a href=\'?t=settings.handover&ItemID=', ItemID, '\'>', ItemName, '</a>') AS ItemName,
    ItemCode,
    IF(ItemStatus = '1', '{$Lang['medical']['message']['setting']['StatusOption']['InUse']}' ,'{$Lang['medical']['message']['setting']['StatusOption']['Suspend']}') AS ItemStatus,
    CONCAT('<input type=\'checkbox\' name=\'ItemID[]\' id=\'ItemID\' value=\'', ItemID,'\'>')
";

$table = "MEDICAL_HANDOVER_ITEM";

$order = "ItemID ASC";
$cond = "isDeleted = '0'";

$sql = "SELECT
            $cols
        FROM
            $table
        WHERE
            $cond
";
$li->field_array = array(
    "ItemName",
    "ItemCode",
    "ItemStatus"
);
$li->sql = $sql;
$li->no_col = sizeof($li->field_array) + 2;
$li->column_array = array(
    0,
    0,
    0,
);
$li->wrap_array = array(
    0,
    0,
    0
);
$li->IsColOff = "IP25_table";
$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='32%' >" . $li->column($pos ++, $Lang['medical']['handover']['itemName']) . "</th>\n";
$li->column_list .= "<th width='32%' >" . $li->column($pos ++, $Lang['medical']['handover']['itemCode']) . "</th>\n";
$li->column_list .= "<th width='32%' >" . $li->column($pos ++, $Lang['medical']['handover']['itemStatus']) . "</th>\n";
$li->column_list .= "<th width='4%'>" . $li->check("ItemID[]") . "</th>\n";

$toolbar = $linterface->GET_LNK_NEW("?t=settings.handover", $button_new, "", "", "", 0);


$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();		
$linterface->LAYOUT_START();
?>
<form name="form1" id="form1" method="POST" action="?t=settings.handoverList">
	<div class="content_top_tool">

		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<colgroup>
    			<col style="width:70%">
    			<col style="width:30%">
    		</colgroup>
			<tr>
				<td><?=$toolbar ?></td>
            </tr>
            <tr class="table-action-bar">
				<td valign="bottom">
					<div class="common_table_tool">						
						<a href="javascript:activateItem();" class="tool_approve"><?=$Lang['Status']['Activate']?></a>						
						<a href="javascript:suspendItem();" class="tool_reject"><?=$Lang['Status']['Suspend']?></a>						
						<a href="javascript:editItem();"><?=$Lang['Btn']['Edit']?></a>
						<a href="javascript:deleteItem();" class="tool_delete"><?php echo $Lang['Btn']['Delete']; ?></a>
					</div>
				
				</td>
			</tr>
		</table>
	</div>
	<div class="table_board">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="bottom">
                <?php echo $li->display(); ?>
                </td>
            </tr>
        </table>
    </div>
    
	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" /> <input
		type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type='hidden' name='action' id='action' value='' />
</form>
<?php
$linterface->LAYOUT_STOP();
?>
<script>
function activateItem(){
	var idAry = [];
	$('#ItemID:checked').each(function(index, ele){
		idAry.push(($(ele).val()));
	});
	if(idAry.length > 0){
		$('#form1').attr('action', '?t=settings.handoverSave');
		$('#action').val('activate');
		$('#form1').submit();
	} else {
		alert('<?php echo $Lang['General']['JS_warning']['SelectAtLeastOneRecord'];?>');
	}
}
function suspendItem(){
	var idAry = [];
	$('#ItemID:checked').each(function(index, ele){
		idAry.push(($(ele).val()));
	});
	if(idAry.length > 0){
		$('#form1').attr('action', '?t=settings.handoverSave');
		$('#action').val('suspend');
		$('#form1').submit();
	} else {
		alert('<?php echo $Lang['General']['JS_warning']['SelectAtLeastOneRecord'];?>');
	}
}
function editItem(){
	var idAry = [];
	$('#ItemID:checked').each(function(index, ele){
		idAry.push(($(ele).val()));
	});
	if(idAry.length == 0 || idAry.length > 1){
		alert(globalAlertMsg1);
	} else {
		var id = idAry.pop();
		window.location.href="?t=settings.handover&ItemID=" + id;
	}
}
function deleteItem(){
	var idAry = [];
	$('#ItemID:checked').each(function(index, ele){
		idAry.push(($(ele).val()));
	});
	if(idAry.length > 0){
		cnf = confirm(globalAlertMsg3);
		if(cnf){
    		$('#form1').attr('action', '?t=settings.handoverSave');
    		$('#action').val('delete');
    		$('#form1').submit();
		}
	} else {
		alert('<?php echo $Lang['General']['JS_warning']['SelectAtLeastOneRecord'];?>');
	}
}

</script>