<?php

# using by 
/*
 * 	Log
 * 
 * 	2017-08-29 [Cameron]
 * 		- create this file
 */


if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='ALLOWED_IP_SETTING')){
	header("Location: /");
	exit();
}
 

$recordExist = false;
$settings = $objMedical->getGeneralSettings();
if (isset($settings['AuthenticationSetting'])) {
	$recordExist = true;
}

$ldb = new libdb();
$result = array();
$dataAry['Module'] = 'Medical'; 
$dataAry['SettingName'] = 'GeneralSettings';

$settingAry[] = "AuthenticationSetting^:".$AuthenticationSetting;
$str = implode("^~",$settingAry);		// build str in this format: k1^:v1^~k2^:v2^~...kn^:vn
  
$dataAry['SettingValue'] = $str;
$dataAry['ModifiedBy'] = $_SESSION['UserID'];

if ($recordExist) {	// record exist, update
	$condAry['Module'] = 'Medical';
	$condAry['SettingName'] = 'GeneralSettings';
	$result[] = $objMedical->UPDATE2TABLE('GENERAL_SETTING',$dataAry,$condAry);
}
else {	// new, insert
	$dataAry['DateInput'] = 'now()';
	$dataAry['InputBy'] = $_SESSION['UserID'];
	$result[] = $objMedical->INSERT2TABLE('GENERAL_SETTING',$dataAry);	
}

$xmsg = in_array(false,$result) ? 'UpdateUnsuccess' : 'UpdateSuccess';

header("Location: ?t=settings.system_setting&xmsg=$xmsg");

?>
