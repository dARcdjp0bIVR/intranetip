<?php
//Using: 

//error_log("\n\nGetPost -->".print_r($_POST,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
$statusID 		= trim($_POST['StatusID']);
//debug_r($_POST);
if ($statusID)
{
	$reasonID 		= trim($_POST['ReasonID']);
	$reasonName 	= trim( htmlentities($_POST['ReasonName'], ENT_QUOTES, 'UTF-8'));
	$reasonCode 	= trim( htmlentities($_POST['ReasonCode'], ENT_QUOTES, 'UTF-8'));
	$recordStatus 	= trim(isset($_POST['RecordStatus'])?$_POST['RecordStatus']:0);
	$isDefault 		= trim(isset($_POST['IsDefault'])?$_POST['IsDefault']:0);
	
	$objDb = new libdb();
	$objDb->Start_Trans();
//error_log("\n\new db --><---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");	
	$objStudentSleepLev2 = new studentSleepLev2($reasonID);
	$objStudentSleepLev2->setReasonName($reasonName);
	$objStudentSleepLev2->setReasonCode($reasonCode);
	$objStudentSleepLev2->setStatusID($statusID);
	$objStudentSleepLev2->setRecordStatus($recordStatus);
	$objStudentSleepLev2->setIsDefault($isDefault);
	
	if ($reasonID)
	{
		$objStudentSleepLev2->setLastModifiedBy($_SESSION['UserID']);
	}
	else
	{
		$objStudentSleepLev2->setInputBy($_SESSION['UserID']);
	}
//error_log("\n\nbefore save -->".print_r($objStudentSleepLev2,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");	
	$result = $objStudentSleepLev2->save();
	if (!$result)
	{
		$objDb->RollBack_Trans();
	}
	$itemQty = $objStudentSleepLev2->sumItemQty($statusID);
	if ($itemQty !== false)	// zero is allowed
	{
//		$sql = "UPDATE MEDICAL_STUDENT_LOG_LEV1 SET ItemQty=".$itemQty." WHERE Lev1ID=".$statusID;
////error_log("\n\nupdate sql -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");		
//		$result2 = $objDb->db_db_query($sql);
		$objStudentSleepLev1 = new studentSleepLev1($statusID);
		$result2 = $objStudentSleepLev1->updateItemQty($itemQty);
		if (!$result2)
		{
			$objDb->RollBack_Trans();
		}
	}
	
	$objDb->Commit_Trans();
	
	echo $result2;
}
else
{
	echo false;	
}
?>