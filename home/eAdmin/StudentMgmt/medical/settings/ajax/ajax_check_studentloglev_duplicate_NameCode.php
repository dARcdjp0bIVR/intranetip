<?php
// using: 

$StudentLogLev = trim($_POST['StudentLogLev']);
$StudentLogLevID = trim($_POST['StudentLogLevID']);
$StudentLogLevName = trim(htmlentities($_POST['StudentLogLevName'], ENT_QUOTES, 'UTF-8'));
$StudentLogLevCode = trim(htmlentities($_POST['StudentLogLevCode'], ENT_QUOTES, 'UTF-8'));

//debug_r($_POST);
$result = '';
$objStudentLogLev = NULL;
if($StudentLogLev == 1)
{
	$objStudentLogLev = new studentLogLev1($StudentLogLevID);
	$result = $objStudentLogLev->recordDuplicateChecking($StudentLogLevName,$StudentLogLevCode,$StudentLogLevID);
}
else if($StudentLogLev == 2)
{
	$objStudentLogLev = new studentLogLev2($StudentLogLevID);
	$StudentLogLev1ID = trim(htmlentities($_POST['StudentLogLev1ID'], ENT_QUOTES, 'UTF-8'));
	$result = $objStudentLogLev->recordDuplicateChecking($StudentLogLevName,$StudentLogLevCode,$StudentLogLev1ID,$StudentLogLevID);
}
else
{
	echo 'error';
	exit();
}


//$result = 0: pass
//$result = 1: name duplicate
//$result = 2: code duplicate
//$result = 3: both duplicate
if($result == 0)
{
	echo 'pass';
}
else
{
	echo $result;
}
?>