<?php
// using:  
/*
 * 	2017-09-19 [Cameron] 
 * 		- add label for radio button for easy choose item
 */

intranet_opendb();

$BowelSettingID = stripslashes($_REQUEST['BowelSettingID']);

$enableIsDefault = false;
if ($BowelSettingID)
{
	$ms = new bowelStatus($BowelSettingID);
	$statusID 		= $ms->getStatusID();
	$statusName 	= str_replace('"','&quot;',stripslashes($ms->getStatusName()));
	$statusCode 	= str_replace('"','&quot;',stripslashes($ms->getStatusCode()));
	$statusBarCode 	= str_replace('"','&quot;',stripslashes($ms->getBarCode()));
	$statusColor 	= str_replace('"','&quot;',stripslashes($ms->getColor()));
	$isDefault 		= $ms->getIsDefault();
	$recordStatus 	= $ms->getRecordStatus();
	$deletedFlag 	= $ms->getDeletedFlag();
	
	if ($isDefault)	// Current Record is default
	{
		$enableIsDefault = true;
	}	
}
else
{
	$ms = new bowelStatus();
	$statusID 		= "";
	$statusName 	= "";
	$statusCode 	= "";
	$statusBarCode	= "";
	$statusColor	= "#64D3A9";
	$isDefault 		= 0;		// Not default
	$recordStatus 	= "";
	$deletedFlag 	= 0;		// Not deleted	
}

if (!$ms->checkIsDefault())		// All are not default at present
{
	$enableIsDefault = true;		
}
	
$disableIsDefault = $enableIsDefault ? "" : "disabled";

## Interface Start

	$x = '';
	$x .= '<div id="debugArea"></div>';
	$x .= '<div class="edit_pop_board edit_pop_board_reorder">'."\n";
		$x .= $linterface->Get_Thickbox_Return_Message_Layer();
		$x .= '<div class="edit_pop_board_write">'."\n";
			$x .= '<form id="BowelSettingForm" name="BowelSettingForm" method="post">'."\n";
				$x .= '<table class="form_table">'."\n";
//					$x .= '<col class="field_title" />'."\n";
//					$x .= '<col class="field_c" />'."\n";
//					$x .= '<col style="text-align:center" />'."\n";
//					$x .= '<col style="text-align:center; width:280px" />'."\n";
					
					# Bowel Status Name
					$x .= '<tr>'."\n";
						$x .= '<td>'.$Lang['medical']['meal_setting']['Name'].'<span class="tabletextrequire">*</span></td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<input id="StatusName" name="StatusName" type="text" class="textbox" value="'.$statusName.'" maxlength="64"/>'."\n";
							$x .= $linterface->Get_Thickbox_Warning_Msg_Div("NameWarningDiv")."\n";
						$x .= '</td>'."\n";
						$x .= '<td>'."\n";
//							$x .= '<a href="" onClick="js_color_picker"><div class="colorBoxStyle" style="background-color:'. $color .';">&nbsp;</div></a>';
							$x .= '<input type="text" id="StatusColor" name="StatusColor" value="'.$statusColor.'" style="display:none;" />';
//							$x .= '<div class="color_picker" style="background-color: rgb(153, 68, 153);"></div>';
//							$x .= '<input type="hidden" id="StatusColor" name="StatusColor" value="#64D3A9">';

						$x .= '</td>'."\n";							
					$x .= '</tr>'."\n";

					# Bowel Status Code
					$x .= '<tr>'."\n";
						$x .= '<td>'.$Lang['medical']['meal_setting']['Code'].'<span class="tabletextrequire">*</span></td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<input id="StatusCode" name="StatusCode" type="text" class="textbox" value="'.$statusCode.'" maxlength="64"/>'."\n";
							$x .= $linterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv")."\n";
						$x .= '</td>'."\n";
						$x .= '<td></td>'."\n";
					$x .= '</tr>'."\n";
					
					# Bowel Bar Code
					$x .= '<tr>'."\n";
						$x .= '<td>'.$Lang['medical']['bowel_setting']['BarCode'].'</td>'."\n";
						$x .= '<td>'."\n";
							$x .= '<input id="StatusBarCode" name="StatusBarCode" type="text" class="textbox" value="'.$statusBarCode.'" maxlength="64"/>'."\n";
							$x .= $linterface->Get_Thickbox_Warning_Msg_Div("CodeWarningDiv")."\n";
						$x .= '</td>'."\n";
						$x .= '<td></td>'."\n";
					$x .= '</tr>'."\n";

					# Bowel Status Status
					$x .= '<tr>'."\n";
						$x .= '<td>'.$Lang['medical']['meal_setting']['Status'].'<span class="tabletextrequire">*</span></td>'."\n";
						$x .= '<td>'."\n";
							$x .= "<input type='radio' value='1' name='RecordStatus' id='RecordStatusInUse' class='radiobutton' ".($recordStatus == "1"?'checked="checked"':'')."> <label for=\"RecordStatusInUse\">".$Lang['medical']['meal_setting']['StatusOption']['InUse']."</label>";
							$x .= "<input type='radio' value='0' name='RecordStatus' id='RecordStatusSuspend' class='radiobutton' ".($recordStatus == "0"?'checked="checked"':'')."> <label for=\"RecordStatusSuspend\">".$Lang['medical']['meal_setting']['StatusOption']['Suspend']."</label>";
							$x .= $linterface->Get_Thickbox_Warning_Msg_Div("StatusWarningDiv")."\n";
						$x .= '</td>'."\n";							
						$x .= '<td></td>'."\n";							
					$x .= '</tr>'."\n";

					# Set as default
					$x .= '<tr>'."\n";
						$x .= '<td>'.$Lang['medical']['meal_setting']['Default'].'</td>'."\n";
						$x .= '<td>'."\n";						
							$x .= '<input id="IsDefault" name="IsDefault" type="checkbox" class="checkbox" value="1" '.$disableIsDefault. ( ($isDefault == "1") ? " checked" : "").'/>'."\n";
							$x .= $linterface->Get_Thickbox_Warning_Msg_Div("IsDefaultWarningDiv")."\n";
						$x .= '</td>'."\n";
						$x .= '<td></td>'."\n";
					$x .= '</tr>'."\n";

				# Show remind message
				if (!$enableIsDefault)
				{
					$x .= '<tr>'."\n";
						$x .= '<td></td>'."\n";
						$x .= '<td>'."\n";						
							$x .= $Lang['medical']['meal_setting']['RemindMessage']['DefaultHasSet']."\n";
						$x .= '</td>'."\n";
						$x .= '<td></td>'."\n";
					$x .= '</tr>'."\n";
				}
				
				$x .= '</table>'."\n";
				$x .= '<input type="hidden" id="StatusID" name="StatusID" value="'.$statusID.'">';
				
				$x .='<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">' . "\n";
					$x .= '<tr>';
						$x .= '<td valign="top" nowrap="nowrap" class="tabletextremark">' .$i_general_required_field .'</td>' . "\n";
						$x .= '<td width="80%">&nbsp;</td>' . "\n";
					$x .= '</tr>';
				$x .= '</table>' . "\n";
				
			$x .= '</form>'."\n";
		$x .= '</div>'."\n";
		
			
		# Button Save
		$btn_Save = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", 
							$onclick="Check_BowelSetting_Form()", $id="Btn_Save");
		# Button Cancel
		$btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", 
							$onclick="js_Hide_ThickBox()", $id="Btn_Cancel");

		$x .= '<div class="edit_bottom">'."\n";
//			$x .= '<span></span>'."\n";
			$x .= '<p class="spacer"></p>'."\n";
				$x .= $btn_Save."\n";
				$x .= $btn_Cancel."\n";
			$x .= '<p class="spacer"></p>'."\n";
		$x .= '</div>';
	$x .= '</div>';

//	echo $x;
	
## Interface End


intranet_closedb();
?>
<script>
$(document).ready( function() {
	/*
	$.fn.colorPicker.defaultColors = [ '000000', '993300','333300', '000080', '333399', '333333', '800000', 'FF6600', '808000', '008000', '008080', '0000FF', '666699', '808080', 'FF0000', 'FF9900', '99CC00', '339966', '33CCCC', '3366FF', '800080', '999999', 'FF00FF', 'FFCC00', 'FFFF00', '00FF00', '00FFFF', '00CCFF', '993366', 'C0C0C0', 'FF99CC', 'FFCC99', 'FFFF99' , 'CCFFFF', '99CCFF', 'FFFFFF'];
	*/
	$.fn.colorPicker.defaultColors = [	'ffaaaa', 'ff5656', 'ff0000', 'bf0000', '7f0000', 'ffffff',
										'ffd4aa', 'ffaa56', 'ff7f00', 'bf5f00', '7f3f00', 'e5e5e5',
										'ffffaa', 'ffff56', 'ffff00', 'bfbf00', '7f7f00', 'cccccc',
										'd4ffaa', 'aaff56', '7fff00', '5fbf00', '3f7f00', 'b2b2b2',
										'aaffaa', '56ff56', '00ff00', '00bf00', '007f00', '999999',
										'aaffd4', '56ffaa', '00ff7f', '00bf5f', '007f3f', '7f7f7f',
										'aaffff', '56ffff', '00ffff', '00bfbf', '007f7f', '666666',
										'aad4ff', '56aaff', '007fff', '005fbf', '003f7f', '4c4c4c',
										'aaaaff', '5656ff', '0000ff', '0000bf', '00007f', '333333',
										'd4aaff', 'aa56ff', '7f00ff', '5f00bf', '3f007f', '000000'										
									];

	$('input#StatusColor').colorPicker();

});
</script>
<?php
echo  $x;
?>