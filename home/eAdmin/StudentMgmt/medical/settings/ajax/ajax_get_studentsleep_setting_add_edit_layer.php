<?php
// using: 


intranet_opendb();

$StudentSleepStatusID = stripslashes($_REQUEST['StudentSleepStatusID']);
//debug_r($StudentSleepStatusID);
$enableIsDefault = false;
if ($StudentSleepStatusID)
{
	$sLog = new studentSleepLev1($StudentSleepStatusID);
	$statusID 		= $sLog->getStatusID();
	$statusName 	= str_replace('"','&quot;',stripslashes($sLog->getStatusName()));
	$statusCode 	= str_replace('"','&quot;',stripslashes($sLog->getStatusCode()));
	$statusColor 	= str_replace('"','&quot;',stripslashes($sLog->getColor()));
	$isDefault		= $sLog->getIsDefault();	
	$itemQty 		= $sLog->getItemQty();	
	$recordStatus 	= $sLog->getRecordStatus();
}
else
{
	$sLog = new studentSleepLev1();
	$statusID 		= "";
	$statusName 		= "";
	$statusCode 		= "";
	$statusColor	= "#64D3A9";
	$isDefault		= "";
	$itemQty 		= 0;	
	$recordStatus 	= "-1";		// Status not set	
}

$level 		= "Lev1";	// Name & ID prefix
$levID 		= $statusID;
$levName	= $statusName;
$levCode 	= $statusCode;


if (!$sLog->checkIsDefault($statusID))		// All are not default at present
{
	$enableIsDefault = true;		
}
$disableIsDefault = $enableIsDefault ? "" : "disabled";

## Interface Start
include_once($PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/medical/settings/templates/studentSleep.tmpl.php");
## Interface End

intranet_closedb();
?>
