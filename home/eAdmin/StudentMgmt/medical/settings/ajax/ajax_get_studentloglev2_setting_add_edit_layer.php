<?php
// using: 


intranet_opendb();

$StudentLogLev2ID = stripslashes($_REQUEST['StudentLogLev2ID']);
$Lev1ID = stripslashes($_REQUEST['Lev1ID']);

$objLev1 = new studentLogLev1($Lev1ID);
$Lev1Name = $objLev1->getLev1Name();

//debug_r($StudentLogLev2ID);
$enableIsDefault = false;
if ($StudentLogLev2ID)
{
	$sLog = new studentLogLev2($StudentLogLev2ID);
	$lev2ID 		= $sLog->getLev2ID();
	$lev2Name 		= str_replace('"','&quot;',stripslashes($sLog->getLev2Name()));
	$lev2Code 		= str_replace('"','&quot;',stripslashes($sLog->getLev2Code()));
//	$itemQty 		= $sLog->getItemQty();
	$isDefault 		= $sLog->getIsDefault();	
	$recordStatus 	= $sLog->getRecordStatus();

	if ($isDefault)	// Current Record is default
	{
		$enableIsDefault = true;
	}	
}
else
{
	$sLog = new studentLogLev2();
	$lev2ID 		= "";
	$lev2Name 		= "";
	$lev2Code 		= "";
//	$itemQty 		= 0;
	$isDefault 		= 0;		// Not default
	$recordStatus 	= "-1";		// Status not set	
}

if (!$sLog->checkIsDefault($Lev1ID))		// All are not default at present
{
	$enableIsDefault = true;		
}
$disableIsDefault = $enableIsDefault ? "" : "disabled";

$level 		= "Lev2";	// Name & ID prefix
$levID 		= $lev2ID;
$levName	= $lev2Name;
$levCode 	= $lev2Code;


## Interface Start
include_once($PATH_WRT_ROOT."/home/eAdmin/StudentMgmt/medical/settings/templates/studentLog.tmpl.php");
## Interface End

intranet_closedb();
?>
