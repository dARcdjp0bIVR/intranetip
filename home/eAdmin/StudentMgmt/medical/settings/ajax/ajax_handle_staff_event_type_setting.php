<?php
// using
/*
 * 2018-03-26 Cameron create this
 */
intranet_opendb();

$eventTypeID = stripslashes($_POST['EventTypeID']);
$task = stripslashes($_POST['Task']);

$pass = true;
$ret = 1;

$objDb = new libdb();
if ($eventTypeID) {
    switch ($task) {
        case "activate":
            $updatedField = "RecordStatus=1,LastModifiedBy=" . $_SESSION['UserID'] . ",DateModified=NOW()";
            break;
        
        case "delete":
            $sql = "SELECT COUNT(*) FROM MEDICAL_STAFF_EVENT WHERE EventTypeID='$eventTypeID'";
            $rs = $objDb->returnResultSet($sql);
            $isUsed = $rs[0]['COUNT(*)'];
            if ($isUsed) {
                $pass = false;
            }
            $updatedField = "DeletedFlag=1,DeletedBy=" . $_SESSION['UserID'] . ",DeletedDate=NOW()";
            break;
        
        case "suspend":
            $updatedField = "RecordStatus=0,LastModifiedBy=" . $_SESSION['UserID'] . ",DateModified=NOW()";
            break;
    }
    
    if ($pass) {
        $sql = "UPDATE MEDICAL_STAFF_EVENT_TYPE SET $updatedField WHERE EventTypeID IN (" . $eventTypeID . ")";
        $ret = $objDb->db_db_query($sql);
    } else {
        $ret = - 1;
    }
}

intranet_closedb();
echo $ret; // return to refresh page
?>
