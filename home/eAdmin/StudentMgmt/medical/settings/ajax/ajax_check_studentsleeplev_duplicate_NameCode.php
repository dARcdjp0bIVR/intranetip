<?php
// using: 

$StudentSleepLev = trim($_POST['StudentSleepLev']);
$StudentSleepLevID = trim($_POST['StudentSleepLevID']);
$StudentSleepLevName = trim(htmlentities($_POST['StudentSleepLevName'], ENT_QUOTES, 'UTF-8'));
$StudentSleepLevCode = trim(htmlentities($_POST['StudentSleepLevCode'], ENT_QUOTES, 'UTF-8'));

//debug_r($_POST);
$result = '';
$objStudentSleepLev = NULL;
if($StudentSleepLev == 1)
{
	$objStudentSleepLev = new studentSleepLev1($StudentSleepLevID);
	$result = $objStudentSleepLev->recordDuplicateChecking($StudentSleepLevName,$StudentSleepLevCode,$StudentSleepLevID);
}
else if($StudentSleepLev == 2)
{
	$objStudentSleepLev = new studentSleepLev2($StudentSleepLevID);
	$StudentSleepLev1ID = trim(htmlentities($_POST['StudentSleepLev1ID'], ENT_QUOTES, 'UTF-8'));
	$result = $objStudentSleepLev->recordDuplicateChecking($StudentSleepLevName,$StudentSleepLevCode,$StudentSleepLev1ID,$StudentSleepLevID);
}
else
{
	echo 'error';
	exit();
}


//$result = 0: pass
//$result = 1: name duplicate
//$result = 2: code duplicate
//$result = 3: both duplicate
if($result == 0)
{
	echo 'pass';
}
else
{
	echo $result;
}
?>