<?php
// using: 


intranet_opendb();

$StudentSleepStatusID = stripslashes($_REQUEST['StudentSleepStatusID']);
$Task = stripslashes($_REQUEST['Task']);
//debug_r($StudentSleepStatusID);

//// Check if a status has been used in MEDICAL_STUDENT_DAILY_MEAL_LOG* or not
//// Default two years
//function isStatusUsed($statusID, $dateFrom=null, $dateTo=null)
//{
//	global $w2_cfg;
//	$objDb = new libdb();
//	$isUsed = false;
//		
//	if ($dateTo == null)
//	{
//		$dateTo = date("Y-m-d");
//		$dateFrom = date("Y-m-d",strtotime('-24 MONTH', strtotime($dateTo)));	// two years ago
//	}
//
//	$year_month = returnListOfYearMonth($dateFrom, $dateTo);
//	if (is_array($year_month))
//	{
//		foreach($year_month as $k => $v)
//		{
//			$table_name = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . $v;
//			$sql = "show tables like '" . $table_name . "%'";
//			$table_rs = $objDb->returnResultSet($sql);
//			if (count($table_rs)>0)
//			{
//				$sql = "SELECT RecordID FROM " . $table_name . " WHERE MealStatus=".$statusID . " LIMIT 1";
//				$rs = $objDb->returnResultSet($sql);
//				if (count($rs)>0)
//				{
//					$isUsed = true;
//					break;
//				}
//			}
//		}
//	}
//	return $isUsed;
//}
//
//// Assume $mealSettingID is comma seperated string
//function checkStatusUsed($mealSettingID)
//{
//	$isUsed = false;
//	$statusIDs = explode(",",$mealSettingID);
//	if (is_array($statusIDs))
//	{
//		foreach($statusIDs as $k=>$v)
//		{
//			$isUsed = isStatusUsed($v);
//			if ($isUsed)
//				break;
//		}
//	}
//	return $isUsed;
//}

$pass = true;
$ret = 1;

$objDb = new libdb(); 
if ($StudentSleepStatusID)
{
	switch($Task)
	{
		case "activate":
				$updatedField = "RecordStatus=1,LastModifiedBy=". $_SESSION['UserID'] .",DateModified=NOW()";
			break;

		case "delete":
				$sql = "SELECT COUNT(*) FROM MEDICAL_STUDENT_SLEEP WHERE StatusID='$StudentSleepStatusID'";
				$rs = $objDb->returnResultSet($sql);
				$isUsed = $rs[0]['COUNT(*)'];
				if($isUsed > 0){
					$pass = false;
				}
				$updatedField = "DeletedFlag=1,DeletedBy=".$_SESSION['UserID'].",DeletedDate=NOW()";
				
			break;
			
		case "suspend":
				$updatedField = "RecordStatus=0,LastModifiedBy=". $_SESSION['UserID'] .",DateModified=NOW()";
			break;
			
	}
	
	if ($pass)
	{
		$sql = "UPDATE MEDICAL_SLEEP_STATUS SET $updatedField WHERE StatusID IN (" . $StudentSleepStatusID .")";
//debug_r($sql);	
//error_log("\n\nsql -->".print_r($sql,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
		$ret = $objDb->db_db_query($sql);
	}
	else
	{
		$ret = -1;
	}
}
else
{
	$errMsg = 'Null StudentSleepStatusID!'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
  	alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
  	exit;	
}

intranet_closedb();
echo $ret;	// return to refresh page
?>
