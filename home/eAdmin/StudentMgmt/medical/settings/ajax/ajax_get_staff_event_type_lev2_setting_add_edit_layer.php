<?php
// using:
/*
 * 2018-03-27 [Cameron]
 * - create this file
 */
intranet_opendb();

$eventTypeLev2ID = stripslashes($_POST['EventTypeLev2ID']);
$eventTypeID = stripslashes($_POST['EventTypeID']);

$enableIsDefault = false;
if ($eventTypeLev2ID) {
    $et = new StaffEventLev2($eventTypeLev2ID);
    $eventTypeID = $et->getEventTypeID();
//    $eventTypeLev2Name = str_replace('"', '&quot;', stripslashes($et->getEventTypeLev2Name()));
    $eventTypeLev2Name = intranet_htmlspecialchars($et->getEventTypeLev2Name());    
    $isDefault 	= $et->getIsDefault();
    $recordStatus = $et->getRecordStatus();
    $deletedFlag = $et->getDeletedFlag();
    if ($isDefault)	// Current Record is default
    {
        $enableIsDefault = true;
    }    
} else {
    $et = new StaffEventLev2();
    $eventTypeLev2Name= "";
    $isDefault 		= 0;		// Not default
    $recordStatus = "";
    $deletedFlag = 0; // Not deleted
}

if (!$et->checkIsDefault($eventTypeID))		// All are not default at present
{
    $enableIsDefault = true;
}

$disableIsDefault = $enableIsDefault ? "" : "disabled";
$isDefaultChecked = ($isDefault == "1") ? " checked" : "";
    
// # Interface Start

$x = '';
$x .= '<div id="debugArea"></div>';
$x .= '<div class="edit_pop_board edit_pop_board_reorder">' . "\n";
$x .= $linterface->Get_Thickbox_Return_Message_Layer();
$x .= '<div class="edit_pop_board_write">' . "\n";
$x .= '<form id="StaffEventTypeLev2SettingForm" name="StaffEventTypeLev2SettingForm" method="post">' . "\n";
$x .= '<table class="form_table">' . "\n";
// Event Type Name
$x .= '<tr>' . "\n";
$x .= '<td>' . $Lang['medical']['general']['tableHeader']['Subcategory']. '<span class="tabletextrequire">*</span></td>' . "\n";
$x .= '<td>' . "\n";
$x .= '<input id="EventTypeLev2Name" name="EventTypeLev2Name" type="text" class="textbox" value="' . $eventTypeLev2Name. '"/>' . "\n";
$x .= $linterface->Get_Thickbox_Warning_Msg_Div("NameWarningDiv") . "\n";
$x .= '</td>' . "\n";
$x .= '</tr>' . "\n";

// Status
$x .= '<tr>' . "\n";
$x .= '<td>' . $Lang['medical']['message']['setting']['recordStatus'] . '<span class="tabletextrequire">*</span></td>' . "\n";
$x .= '<td>' . "\n";
$x .= "<input type='radio' value='1' name='RecordStatus' id='RecordStatusInUse' class='radiobutton' " . ($recordStatus == "1" ? 'checked="checked"' : '') . "> <label for=\"RecordStatusInUse\">" . $Lang['medical']['general']['StatusOption']['InUse'] . "</label>";
$x .= "<input type='radio' value='0' name='RecordStatus' id='RecordStatusSuspend' class='radiobutton' " . ($recordStatus == "0" ? 'checked="checked"' : '') . "> <label for=\"RecordStatusSuspend\">" . $Lang['medical']['general']['StatusOption']['Suspend'] . "</label>";
$x .= $linterface->Get_Thickbox_Warning_Msg_Div("StatusWarningDiv") . "\n";
$x .= '</td>' . "\n";
$x .= '<td></td>' . "\n";
$x .= '</tr>' . "\n";

// set default item
$x .= '<tr>' . "\n";
$x .= '<td>' . $Lang['medical']['general']['Default']. '</td>' . "\n";
$x .= '<td>' . "\n";
$x .= '<input id="IsDefault" name="IsDefault" type="checkbox" class="checkbox" value="1" '.$disableIsDefault. $isDefaultChecked.'/>';
$x .= $linterface->Get_Thickbox_Warning_Msg_Div("IsDefaultWarningDiv");
$x .= '</td>' . "\n";
$x .= '<td></td>' . "\n";
$x .= '</tr>' . "\n";

$x .= '</table>' . "\n";
$x .= '<input type="hidden" id="EventTypeLev2ID" name="EventTypeLev2ID" value="' . $eventTypeLev2ID. '">';

$x .= '<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">' . "\n";
$x .= '<tr>';
$x .= '<td valign="top" nowrap="nowrap" class="tabletextremark">' . $i_general_required_field . '</td>' . "\n";
$x .= '<td width="80%">&nbsp;</td>' . "\n";
$x .= '</tr>';
$x .= '</table>' . "\n";

$x .= '</form>' . "\n";
$x .= '</div>' . "\n";

// Button Save
$btn_Save = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick = "checkStaffEventTypeLev2SettingForm()", $id = "Btn_Save");
// Button Cancel
$btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick = "js_Hide_ThickBox()", $id = "Btn_Cancel");

$x .= '<div class="edit_bottom">' . "\n";
$x .= '<p class="spacer"></p>' . "\n";
$x .= $btn_Save . "\n";
$x .= $btn_Cancel . "\n";
$x .= '<p class="spacer"></p>' . "\n";
$x .= '</div>';
$x .= '</div>';

// # Interface End

intranet_closedb();

echo $x;
?>