<?php
// using:
/*
 * 2018-03-26 [Cameron]
 * - create this file
 */
intranet_opendb();

$eventTypeSettingID = stripslashes($_POST['EventTypeSettingID']);

$enableIsDefault = false;
if ($eventTypeSettingID) {
    $et = new StaffEventLev1($eventTypeSettingID);
    $eventTypeID = $et->getEventTypeID();
//    $eventTypeName = str_replace('"', '&quot;', stripslashes($et->getEventTypeName()));
    $eventTypeName = intranet_htmlspecialchars($et->getEventTypeName());
    $isDefault 	= $et->getIsDefault();
    $recordStatus = $et->getRecordStatus();
    $deletedFlag = $et->getDeletedFlag();
    if ($isDefault)	// Current Record is default
    {
        $enableIsDefault = true;
    }    
} else {
    $et = new StaffEventLev1();
    $eventTypeID = "";
    $eventTypeName = "";
    $isDefault 		= 0;		// Not default
    $recordStatus = "";
    $deletedFlag = 0; // Not deleted
}
    
if (!$et->checkIsDefault($eventTypeSettingID))		// All are not default at present
{
    $enableIsDefault = true;
}
$disableIsDefault = $enableIsDefault ? "" : "disabled";
$isDefaultChecked = ($isDefault == "1") ? " checked" : "";

// # Interface Start

$x = '';
$x .= '<div id="debugArea"></div>';
$x .= '<div class="edit_pop_board edit_pop_board_reorder">' . "\n";
$x .= $linterface->Get_Thickbox_Return_Message_Layer();
$x .= '<div class="edit_pop_board_write">' . "\n";
$x .= '<form id="EventTypeSettingForm" name="EventTypeSettingForm" method="post">' . "\n";
$x .= '<table class="form_table">' . "\n";
// Event Type Name
$x .= '<tr>' . "\n";
$x .= '<td>' . $Lang['medical']['general']['tableHeader']['Category'] . '<span class="tabletextrequire">*</span></td>' . "\n";
$x .= '<td>' . "\n";
$x .= '<input id="EventTypeName" name="EventTypeName" type="text" class="textbox" value="' . $eventTypeName . '"/>' . "\n";
$x .= $linterface->Get_Thickbox_Warning_Msg_Div("NameWarningDiv") . "\n";
$x .= '</td>' . "\n";
$x .= '</tr>' . "\n";

// Status
$x .= '<tr>' . "\n";
$x .= '<td>' . $Lang['medical']['message']['setting']['recordStatus'] . '<span class="tabletextrequire">*</span></td>' . "\n";
$x .= '<td>' . "\n";
$x .= "<input type='radio' value='1' name='RecordStatus' id='RecordStatusInUse' class='radiobutton' " . ($recordStatus == "1" ? 'checked="checked"' : '') . "> <label for=\"RecordStatusInUse\">" . $Lang['medical']['general']['StatusOption']['InUse'] . "</label>";
$x .= "<input type='radio' value='0' name='RecordStatus' id='RecordStatusSuspend' class='radiobutton' " . ($recordStatus == "0" ? 'checked="checked"' : '') . "> <label for=\"RecordStatusSuspend\">" . $Lang['medical']['general']['StatusOption']['Suspend'] . "</label>";
$x .= $linterface->Get_Thickbox_Warning_Msg_Div("StatusWarningDiv") . "\n";
$x .= '</td>' . "\n";
$x .= '<td></td>' . "\n";
$x .= '</tr>' . "\n";

// set default item
$x .= '<tr>' . "\n";
$x .= '<td>' . $Lang['medical']['general']['Default']. '</td>' . "\n";
$x .= '<td>' . "\n";
$x .= '<input id="IsDefault" name="IsDefault" type="checkbox" class="checkbox" value="1" '.$disableIsDefault. $isDefaultChecked.'/>';
$x .= $linterface->Get_Thickbox_Warning_Msg_Div("IsDefaultWarningDiv");
$x .= '</td>' . "\n";
$x .= '<td></td>' . "\n";
$x .= '</tr>' . "\n";

$x .= '</table>' . "\n";
$x .= '<input type="hidden" id="EventTypeID" name="EventTypeID" value="' . $eventTypeID . '">';

$x .= '<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">' . "\n";
$x .= '<tr>';
$x .= '<td valign="top" nowrap="nowrap" class="tabletextremark">' . $i_general_required_field . '</td>' . "\n";
$x .= '<td width="80%">&nbsp;</td>' . "\n";
$x .= '</tr>';
$x .= '</table>' . "\n";

$x .= '</form>' . "\n";
$x .= '</div>' . "\n";

// Button Save
$btn_Save = $linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "button", $onclick = "Check_EventTypeSetting_Form()", $id = "Btn_Save");
// Button Cancel
$btn_Cancel = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", $onclick = "js_Hide_ThickBox()", $id = "Btn_Cancel");

$x .= '<div class="edit_bottom">' . "\n";
$x .= '<p class="spacer"></p>' . "\n";
$x .= $btn_Save . "\n";
$x .= $btn_Cancel . "\n";
$x .= '<p class="spacer"></p>' . "\n";
$x .= '</div>';
$x .= '</div>';

// # Interface End

intranet_closedb();

echo $x;
?>