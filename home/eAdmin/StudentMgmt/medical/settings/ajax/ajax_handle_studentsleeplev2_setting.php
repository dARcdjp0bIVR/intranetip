<?php
// using: 


intranet_opendb();

$StudentSleepReasonID = stripslashes($_REQUEST['StudentSleepLev2ID']);
$Task = stripslashes($_REQUEST['Task']);
//debug_r($StudentSleepReasonID);

//// Check if a status has been used in MEDICAL_STUDENT_DAILY_MEAL_LOG* or not
//// Default two years
//function isStatusUsed($statusID, $dateFrom=null, $dateTo=null)
//{
//	global $w2_cfg;
//	$objDb = new libdb();
//	$isUsed = false;
//		
//	if ($dateTo == null)
//	{
//		$dateTo = date("Y-m-d");
//		$dateFrom = date("Y-m-d",strtotime('-24 MONTH', strtotime($dateTo)));	// two years ago
//	}
//
//	$year_month = returnListOfYearMonth($dateFrom, $dateTo);
//	if (is_array($year_month))
//	{
//		foreach($year_month as $k => $v)
//		{
//			$table_name = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . $v;
//			$sql = "show tables like '" . $table_name . "%'";
//			$table_rs = $objDb->returnResultSet($sql);
//			if (count($table_rs)>0)
//			{
//				$sql = "SELECT RecordID FROM " . $table_name . " WHERE MealStatus=".$statusID . " LIMIT 1";
//				$rs = $objDb->returnResultSet($sql);
//				if (count($rs)>0)
//				{
//					$isUsed = true;
//					break;
//				}
//			}
//		}
//	}
//	return $isUsed;
//}
//
//// Assume $mealSettingID is comma seperated string
//function checkStatusUsed($mealSettingID)
//{
//	$isUsed = false;
//	$statusIDs = explode(",",$mealSettingID);
//	if (is_array($statusIDs))
//	{
//		foreach($statusIDs as $k=>$v)
//		{
//			$isUsed = isStatusUsed($v);
//			if ($isUsed)
//				break;
//		}
//	}
//	return $isUsed;
//}

$pass = true;
$result2 = 1;
$resultUpdate = array();

$objDb = new libdb(); 
if ($StudentSleepReasonID)
{
	switch($Task)
	{
		case "activate":
				$updatedField = "RecordStatus=1,LastModifiedBy=". $_SESSION['UserID'] .",DateModified=NOW()";
			break;

		case "delete":
				$sql = "SELECT COUNT(*) FROM MEDICAL_STUDENT_SLEEP WHERE ReasonID='$StudentSleepReasonID'";
//debug_r($sql);
				$rs = $objDb->returnResultSet($sql);
//debug_r($rs);
				$isUsed = $rs[0]['COUNT(*)'];
				if($isUsed > 0){
					$pass = false;
				}
				$updatedField = "DeletedFlag=1,DeletedBy=".$_SESSION['UserID'].",DeletedDate=NOW()";
				
			break;
			
		case "suspend":
				$updatedField = "RecordStatus=0,LastModifiedBy=". $_SESSION['UserID'] .",DateModified=NOW()";
			break;
			
	}
	
	if ($pass)
	{
		
		$objDb = new libdb();
		$objDb->Start_Trans();
		
		$sql = "UPDATE MEDICAL_SLEEP_STATUS_REASON SET $updatedField WHERE ReasonID IN (" . $StudentSleepReasonID .")";
//debug_r($sql);	

		$result = $objDb->db_db_query($sql);
		if (!$result)
		{
			$objDb->RollBack_Trans();
		}


		$aryStudentSleepReasonID = explode(',',$StudentSleepReasonID);

		for($i =0,$iMax = count($aryStudentSleepReasonID);$i< $iMax; $i++)
		{

			$objStudentSleepLev2 = new studentSleepLev2($aryStudentSleepReasonID[$i]);
			$statusID = $objStudentSleepLev2->getStatusID();

			$itemQty = $objStudentSleepLev2->sumItemQty($statusID);
			$objStudentSleepLev1 = new studentSleepLev1($statusID);
			$resultUpdate[] = $objStudentSleepLev1->updateItemQty($itemQty);


		}

		if (!in_array(false,$resultUpdate))
		{

			$objDb->Commit_Trans();		

			$result2 = 1;
		}else{

			$objDb->RollBack_Trans();
			$result2 = -1;

		}

		
	}
	else
	{
		$result2 = -1;
	}
}
else
{
	$errMsg = 'Null StudentSleepReasonID!'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
  	alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
  	exit;	
}

intranet_closedb();

echo $result2;	// return to refresh page
?>
