<?php
// using: 


intranet_opendb();

$StudentLogLev2ID = stripslashes($_REQUEST['StudentLogLev2ID']);
$Task = stripslashes($_REQUEST['Task']);
//debug_r($StudentLogLev2ID);

//// Check if a status has been used in MEDICAL_STUDENT_DAILY_MEAL_LOG* or not
//// Default two years
//function isStatusUsed($statusID, $dateFrom=null, $dateTo=null)
//{
//	global $w2_cfg;
//	$objDb = new libdb();
//	$isUsed = false;
//		
//	if ($dateTo == null)
//	{
//		$dateTo = date("Y-m-d");
//		$dateFrom = date("Y-m-d",strtotime('-24 MONTH', strtotime($dateTo)));	// two years ago
//	}
//
//	$year_month = returnListOfYearMonth($dateFrom, $dateTo);
//	if (is_array($year_month))
//	{
//		foreach($year_month as $k => $v)
//		{
//			$table_name = $w2_cfg["PREFIX_MEDICAL_STUDENT_DAILY_MEAL_LOG"] . $v;
//			$sql = "show tables like '" . $table_name . "%'";
//			$table_rs = $objDb->returnResultSet($sql);
//			if (count($table_rs)>0)
//			{
//				$sql = "SELECT RecordID FROM " . $table_name . " WHERE MealStatus=".$statusID . " LIMIT 1";
//				$rs = $objDb->returnResultSet($sql);
//				if (count($rs)>0)
//				{
//					$isUsed = true;
//					break;
//				}
//			}
//		}
//	}
//	return $isUsed;
//}
//
//// Assume $mealSettingID is comma seperated string
//function checkStatusUsed($mealSettingID)
//{
//	$isUsed = false;
//	$statusIDs = explode(",",$mealSettingID);
//	if (is_array($statusIDs))
//	{
//		foreach($statusIDs as $k=>$v)
//		{
//			$isUsed = isStatusUsed($v);
//			if ($isUsed)
//				break;
//		}
//	}
//	return $isUsed;
//}

$pass = true;
$result2 = 1;
$resultUpdate = array();

$objDb = new libdb(); 
if ($StudentLogLev2ID)
{
	switch($Task)
	{
		case "activate":
				$updatedField = "RecordStatus=1,LastModifiedBy=". $_SESSION['UserID'] .",DateModified=NOW()";
			break;

		case "delete":
				$sql = "SELECT COUNT(*) FROM MEDICAL_STUDENT_LOG WHERE BehaviourTwo='$StudentLogLev2ID'";
				$rs = $objDb->returnResultSet($sql);
				$isUsed = $rs[0]['COUNT(*)'];
				if($isUsed > 0){
					$pass = false;
				}
				$updatedField = "DeletedFlag=1,DeletedBy=".$_SESSION['UserID'].",DeletedDate=NOW()";
				
			break;
			
		case "suspend":
				$updatedField = "RecordStatus=0,LastModifiedBy=". $_SESSION['UserID'] .",DateModified=NOW()";
			break;
			
	}
	
	if ($pass)
	{
		
		$objDb = new libdb();
		$objDb->Start_Trans();
		
		$sql = "UPDATE MEDICAL_STUDENT_LOG_LEV2 SET $updatedField WHERE Lev2ID IN (" . $StudentLogLev2ID .")";
//debug_r($sql);	

		$result = $objDb->db_db_query($sql);
		if (!$result)
		{
			$objDb->RollBack_Trans();
		}


		$aryStudentLogLev2ID = explode(',',$StudentLogLev2ID);

		for($i =0,$iMax = count($aryStudentLogLev2ID);$i< $iMax; $i++)
		{

			$objStudentLogLev2 = new studentLogLev2($aryStudentLogLev2ID[$i]);
			$lev1ID = $objStudentLogLev2->getLev1ID();

			$itemQty = $objStudentLogLev2->sumItemQty($lev1ID);
			$objStudentLogLev1 = new studentLogLev1($lev1ID);
			$resultUpdate[] = $objStudentLogLev1->updateItemQty($itemQty);


		}

		if (!in_array(false,$resultUpdate))
		{

			$objDb->Commit_Trans();		

			$result2 = 1;
		}else{

			$objDb->RollBack_Trans();
			$result2 = -1;

		}

		
	}
	else
	{
		$result2 = -1;
	}
}
else
{
	$errMsg = 'Null StudentLogLev2ID!'." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__." HTTP_REFERER :".$_SERVER['HTTP_REFERER'];
  	alert_error_log($projectName=$cfg_medical['module_code'],$errMsg,$errorType='');
  	exit;	
}

intranet_closedb();

echo $result2;	// return to refresh page
?>
