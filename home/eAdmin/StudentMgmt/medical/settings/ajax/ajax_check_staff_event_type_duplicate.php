<?php
// using:
/*
 * Log
 *
 * 2018-03-26 Cameron
 * - create this file
 */
$eventTypeID = trim(htmlentities($_POST['EventTypeID'], ENT_QUOTES, 'UTF-8'));
//$eventTypeName = trim(htmlentities($_POST['EventTypeName'], ENT_QUOTES, 'UTF-8'));
$eventTypeName = standardizeFormPostValue(trim($_POST['EventTypeName']));

$result = '';
$objEt = new StaffEventLev1($eventTypeID);
$result = $objEt->recordDuplicateChecking($eventTypeName, $eventTypeID);

if ($result == 0) {
    echo 'pass';
} else {
    echo $result;
}

?>