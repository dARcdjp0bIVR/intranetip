<?php
// using: 
/*
 * 	Log
 * 
 * 	2017-06-15 Cameron
 * 		- create this file
 */

$EventTypeID = trim(htmlentities($_POST['EventTypeID'], ENT_QUOTES, 'UTF-8'));
$EventTypeName = trim(htmlentities($_POST['EventTypeName'], ENT_QUOTES, 'UTF-8'));


$result = '';
$objEt = new eventType($EventTypeID);
$result = $objEt->recordDuplicateChecking($EventTypeName,$EventTypeID);

if($result == 0)
{
	echo 'pass';
}
else
{
	echo $result;
}
?>