<?php
/*
 * 	using: 
 * 	Log
 * 	Date:
 */

 
if( !$sys_custom['medical']['Handover'] && !($objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess = 'HANDOVER_SETTINGS'))){
	header("Location: /");
	exit();
}

$CurrentPage = "SettingsHandover";


$TAGS_OBJ[] = array($Lang['medical']['menu']['settings_handover'], "", 0);
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

$btnSubmit = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], 'button',$ParOnClick="checkForm()", $ParName="btnSubmit");
$btnBack = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], 'button',$ParOnClick="window.location.href='?t=settings.handoverList'", $ParName="btnBack");

$itemID = $_GET['ItemID'];

if($itemID != ''){
    $item = $objMedical->getHandoverItem($itemID);
    $item = $item[0];
    $checked_activate = $item['ItemStatus'] == '1';
    $checked_suspend = $item['ItemStatus'] == '0';
    $PAGE_NAVIGATION[] = array($Lang['Btn']['Edit'], "");
    $action = 'edit';
} else {
    $PAGE_NAVIGATION[] = array($Lang['Btn']['New'], "");
    $action = 'new';
}
$PAGE_NAVIGATION[] = array($Lang['medical']['menu']['settings_handover'], '');

$MODULE_OBJ = $objMedical->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

include_once('templates/handover.tmpl.php');

$linterface->LAYOUT_STOP();
?>