<?php
// using: 


//if(!$objMedical->checkAccessRight($_SESSION['UserID'], $pageAccess='NOTICE_REMINDER')){
//	header("Location: /");
//	exit();
//}


include_once($PATH_WRT_ROOT."includes/cust/medical/libMedicalMessage.php");

$objMedicalMessage = new MedicalMessage();

$deleteSuccess = false;
if($deleteAction == 'receivedMail'){
	$isReceivedArrValid = $objMedicalMessage->checkOwnReceiveMessage($_SESSION['UserID'],$receiveID);
	if($isReceivedArrValid){
		$deleteSuccess = $objMedicalMessage->deleteReceiveMessage($receiveID);
	}
}else if($deleteAction == 'sentMail'){
	$isSendArrValid = $objMedicalMessage->checkOwnSendMessage($_SESSION['UserID'],$sendID);
	if($isSendArrValid){
		$deleteSuccess = $objMedicalMessage->deleteSendMessage($sendID);
	}
}



if($deleteSuccess){
	echo $Lang['medical']['message']['ReturnMessage']['Delete']['Success'];
}else{	
	echo $Lang['medical']['message']['ReturnMessage']['Delete']['Failed'];
}
?>