<?php
// Editing by 
$StudentID = IntegerSafe($_REQUEST['StudentID']);

$main_content_field = "IF(EnName IS NOT NULL AND EnName !='' AND ChName IS NOT NULL AND ChName !='', IF(IsMain = 1, CONCAT('* ', EnName,' / ',ChName), CONCAT(EnName,' / ',ChName)), IF(EnName IS NOT NULL AND EnName!='',EnName,ChName))";
$main_content_field = "IF($main_content_field='' OR $main_content_field IS NULL,'-',$main_content_field)";

$sql = "SELECT 
				$main_content_field as Name,
				Relation,
				Phone, 
				EmPhone,
				IsMain
		FROM
				$eclass_db.GUARDIAN_STUDENT
		WHERE 
				UserID = '$StudentID'
		ORDER BY 
				IsMain DESC, Relation ASC
		";

$guardians = $indexVar['db']->returnResultSet($sql);
$guardians_count = count($guardians);

$x .= '<table class="common_table_list_v30">';
	$x .= '<tr class="tabletop">
			<th class="num_check">#</th>
			<th>'.$i_UserName.'</th>
			<th>'.$ec_iPortfolio[relation].'</th>
			<th>'.$i_StudentGuardian_Phone.'</th>
			<th>'.$i_StudentGuardian_EMPhone.'</th>
		</tr>'."\n";
	if($guardians_count == 0){
		$x .= '<tr><td colspan="5" align="center">'.$i_no_record_exists_msg.'</td></tr>';
	}
	for($i=0;$i<$guardians_count;$i++)
	{
		$name = $guardians[$i]['Name'];
		$relation = $guardians[$i]['Relation'];
		$phone = $guardians[$i]['Phone'];
		$em_phone = $guardians[$i]['EmPhone'];
		$IsMain = $guardians[$i]['IsMain'];
		$no = $i+1;
		
		$x .= "<tr><td".($IsMain == 1?" style=\"color:green\"":"").">$no</td>";
		$x .= "<td".($IsMain == 1?" style=\"color:green\"":"").">$name</td>";
		$x .= "<td".($IsMain == 1?" style=\"color:green\"":"").">$ec_guardian[$relation]</td>";
		$x .= "<td".($IsMain == 1?" style=\"color:green\"":"").">$phone</td>";
		$x .= "<td".($IsMain == 1?" style=\"color:green\"":"").">$em_phone</td></tr>\n";
	}
	
$x .= '</table>'."\n";

$x .= '<div style="color:green;">* '.$i_StudentGuardian_MainContent.'</div>'."\n";

echo $x;

?>