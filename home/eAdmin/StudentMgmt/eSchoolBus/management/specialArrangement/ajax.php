<?php
// Editing by 

switch($ajax_task)
{
	case 'getStudentSelection':
		
		$selection = $indexVar['linterface']->getStudentSelectionByClass($YearClassID,  ' id="StudentID" name="StudentID" onchange="studentSelectionChanged();" ', $StudentID, $all=0, $noFirst=1, $FirstTitle="");
		echo $selection;
		
	break;
	
	case 'getBusStopsSelection':
		
		$routeCollectionIdAry = IntegerSafe($_POST['RouteCollectionID']);
		//$id = $_POST['Id'];
		$name = $_POST['Name'];
		
		$selection = $indexVar['linterface']->getBusStopsSelectionByRouteCollection($routeCollectionIdAry, ' name="'.$name.'" ', $__selected="", $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --');
		echo $selection;
		
	break;
	
	case 'getSpecialArrangementRow':
		
		if($AutoIncrement == 1){
			$next_date = date("Y-m-d", strtotime($Date) + 86400);
		}else{
			$next_date = date("Y-m-d", strtotime($Date));
		}
		
		$am_uid = uniqid();
		$pm_uid = uniqid();
		$parentSelectionOption = array(array('0',$Lang['eSchoolBus']['Settings']['Student']['ByParent']));
		$x .= '<tr class="special">
				<td>
					'.$indexVar['linterface']->GET_DATE_PICKER("Date[]",$next_date,$__OtherMember='onchange=dateChanged(this);',$__DateFormat="yy-mm-dd",$__MaskFunction="",$__ExtWarningLayerID="",$__ExtWarningLayerContainer="",$__OnDatePickSelectedFunction="dateChanged(this);",$__ID='Date_'.uniqid()).'
				</td>
				<td>
					'.$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID, array('N'), array('AM'),' id="RouteCollectionID_'.$am_uid.'" name="RouteCollectionID[AM][]" onchange="routeSelectionChanged(this, \'AM\');" ', $__selected=$AMRouteCollectionID, $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --', $parentSelectionOption,$next_date).'
					<span'.(in_array($AMRouteCollectionID,array('0',''))?' style="display:none"':'').'>'.$indexVar['linterface']->getBusStopsSelectionByRouteCollection($AMRouteCollectionID, ' name="BusStopsID[AM][]" ', $__selected=$AMBusStopID, $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --').
					$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("RouteCollectionID_".$am_uid."_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'], "WarnMsgDiv").'</span>
				</td>
				<td>
					'.$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID, array('N','S'), array('PM','MON','TUE','WED','THU','FRI'),' id="RouteCollectionID_'.$pm_uid.'" name="RouteCollectionID[PM][]" onchange="routeSelectionChanged(this, \'PM\');" ', $__selected=$PMRouteCollectionID, $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --', $parentSelectionOption,$next_date).'
					<span'.(in_array($PMRouteCollectionID,array('0',''))?' style="display:none"':'').'>'.$indexVar['linterface']->getBusStopsSelectionByRouteCollection($PMRouteCollectionID, ' name="BusStopsID[PM][]" ', $__selected=$PMBusStopID, $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --').
					$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("RouteCollectionID_".$pm_uid."_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'], "WarnMsgDiv").'</span>
				</td>
				<td>'.$indexVar['linterface']->GET_LNK_DELETE('javascript:void(0);', $__ParTitle=$Lang['Btn']['Delete'], $__ParOnClick="deleteRow(this);", $__ParClass="", $__WithSpan=1).'</td>
			</tr>';
		
		echo $x;
		
	break;
}

?>