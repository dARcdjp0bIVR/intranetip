<?php
// Using:
/*
 *  2018-06-07 Cameron
 *      - create this file
 */
$db = $indexVar['db'];
$tableName = 'INTRANET_SCH_BUS_SETTING';

$resultAry = array();

$enableApplyLeaveCutOffTiming = ($_POST['enableApplyLeaveCutOffTiming'])? 1 : 0;
$daysBeforeApplyLeave= trim($_POST['daysBeforeApplyLeave']);

$db->Start_Trans();

$resultAry['enableApplyLeaveCutOffTiming'] = $db->updateSetting('enableApplyLeaveCutOffTiming', $enableApplyLeaveCutOffTiming);
$resultAry['daysBeforeApplyLeave'] = $db->updateSetting('daysBeforeApplyLeave', $daysBeforeApplyLeave);

$classGroupAry = $db->getClassGroup();

foreach((array)$classGroupAry as $classGroupID => $groupName) {
    $gotoSchoolTime =  sprintf("%02d",$_POST['gotoSchoolHour_'.$classGroupID]) . ':' .  sprintf("%02d",$_POST['gotoSchoolMin_'.$classGroupID]);
    $resultAry['gotoSchool_'.$classGroupID] = $db->updateCutoffTimeSetting('gotoSchool_'.$classGroupID, $gotoSchoolTime, '1', $classGroupID);
    
    $leaveSchoolTime =  sprintf("%02d",$_POST['leaveSchoolHour_'.$classGroupID]) . ':' .  sprintf("%02d",$_POST['leaveSchoolMin_'.$classGroupID]);
    $resultAry['leaveSchool_'.$classGroupID] = $db->updateCutoffTimeSetting('leaveSchool_'.$classGroupID, $leaveSchoolTime, '2', $classGroupID);
}

if (in_array(false, $resultAry)) {
    $db->RollBack_Trans();
    $returnMsgKey = 'UpdateUnsuccess';
}
else {
    $db->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
}

$path = "index.php?task=settings/cutoffTime&returnMsgKey=$returnMsgKey";
$indexVar['libSchoolBus']->redirect($path);

?>