<?php
/*
 *  2019-01-31 Cameron
 *      - create this file  
 */

$linterface = $indexVar['linterface'];

if ( $_POST['AddUserID[]']) {
    $excludeTeacherIDAry = $_POST['AddUserID[]'];
}
else {
    if ($_POST['selectedTeacherID']) {
        $teacherID = $_POST['selectedTeacherID'];
        $excludeTeacherIDAry = explode(',',$teacherID);
    }
    else {
        $excludeTeacherIDAry = array();
    }
}
 
$teacherType = $_POST['teacherType'];
$teacherType = ($teacherType == 1) ? 1 : 0;

$db = $indexVar['db'];

$teacherTypeSelection = $linterface->getTeacherTypeSelection($teacherType);


if (count($excludeTeacherIDAry)) {
    $cond = "AND UserID NOT IN ('".implode("','",(array)$excludeTeacherIDAry)."')";
}
else {
    $cond = "";
}

$availableTeacherAry = $db->getTeacher($teacherType,$cond);
$availableTeacherOptions = '';
for($i=0,$iMax=count($availableTeacherAry); $i<$iMax; $i++) {
    $staffID = $availableTeacherAry[$i]['UserID'];
    $staffName = $availableTeacherAry[$i]['Name'];
    $availableTeacherOptions .= '<option value="'.$staffID.'">'.$staffName.'</option>';
}


?>

<script src="/templates/jquery/jquery.autocomplete.js" type="text/javascript" charset="utf-8"></script>
<link href="/templates/jquery/jquery.autocomplete.css" rel="stylesheet" type="text/css">

<script>

var timerobj;
function jsShowOrHideSearchResult()
{
	timerobj = setTimeout(showOrHideSearchResultAction,1000);
}

function showOrHideSearchResultAction()
{
	$("#UserSearch").focus();
	clearTimeout(timerobj);
}

function addSelectedTeacher(name, id)
{
	var existOption = false;
	$("#AddUserID\\[\\] option").each(function(){
		if ($(this).val() == id) {
			existOption = true;
		}
	});

	if (existOption == false) {
    	var to = new Option(name, id, false, false);
    	$("#AddUserID\\[\\]").append(to);
	}
}

function updateAvailableTeacherList(id)
{
	$("#AvalUserList\\[\\] option").each(function(){
		if ($(this).val() == id) {
			$(this).remove();
		}
	});
}

function addSelectedTeacherToRoute()
{
	var html = '';
	var text;
	var value;
	var duplicate;
	
	$('#AddUserID\\[\\] option').attr('selected',true);
	$("#AddUserID\\[\\] option").each(function(){
		text = $(this).text();
		value = $(this).val();
		duplicate = false;
			
		$(':input[name="RouteTeacherID\\[\\]"]').each(function(){
			if ($(this).val() == value) {
				duplicate = true;
			}
		});
		if (duplicate == false) {
    		html += '<div id="TeacherDiv_' + value +'">' + text + '<input type="hidden" name="RouteTeacherID[]" value="' + value + '">&nbsp;';
    		html += '<a href="javascript:deleteRouteTeacher(' + value + ');">x</a>';
    		html += '</div>';
		}		
	});
	
	$('#selectedTeacherSpan').append(html);
	window.top.tb_remove();
}

$(document).ready(function(){
	if($("#UserSearch").length > 0){
		
		$("#UserSearch").autocomplete(		  
	      "index.php?task=settings/route/ajax_search_teacher",
	      {
	  			delay:0,
	  			minChars:1,
	  			matchContains:1,
	  			onItemSelect: function(li) {
		  			jsShowOrHideSearchResult();
		  			addSelectedTeacher(li.selectValue, li.extra[0]);
		  			updateAvailableTeacherList(li.extra[0]);
		  		},
	  			formatItem: function(row){
		  			return row[0]; 
		  		},
	  			maxItemsToShow: 5,
	  			autoFill:false,
	  			overflow_y: 'auto',
	  			overflow_x: 'hidden',
	  			maxHeight: '200px',
	  			width:'280px',
	  			scrollLayer: 'EditLayer'
	  		}
	    );
	}

	$('#TeacherType').change(function(){
		onloadThickBox();
	});
});



</script>


<form name='teacherSelectForm' id='teacherSelectForm'>
<div class="edit_pop_board" style="height:440px;">
	<div class="edit_pop_board_write" id="EditLayer" name="EditLayer" style="height:343px;">
    	<table class="form_table">
			<tr>
		    	<td>
		    		<table width="100%" border="0" cellspacing="0" cellpadding="5">
          				<tr>
          					<td>
          						<table width="100%" border="0" cellspacing="0" cellpadding="5">
									<tr>
										<td width="49%" bgcolor="#EEEEEE">
											<?php echo $Lang['General']['UserType']; ?> :
											<?php echo $teacherTypeSelection;?>
										</td>
										<td width="40"></td>
										<td width="49%" bgcolor="#EFFEE2" class="steptitletext"><?php echo $Lang['eSchoolBus']['Settings']['Route']['SelectTeacher'];?> </td>
									</tr>
									
									<tr>
										<td bgcolor="#EEEEEE" align="center">
                							<div id="AvalUserLayer">
                								<select name="AvalUserList[]" class="AvalUserList" id="AvalUserList[]" size="10" style="width:99%" multiple="true">
                                                	<?php echo $availableTeacherOptions;?>
                                                </select>
    										</div>

											<span class="tabletextremark"><?php echo $Lang['eSchoolBus']['Settings']['Route']['CtrlMultiSelectMessage'];?></span>
										</td>
										
                						<td><input name="AddAll" onclick="checkOptionAll(this.form.elements['AvalUserList[]']);checkOptionTransfer(this.form.elements['AvalUserList[]'],this.form.elements['AddUserID[]']);return false;" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;&gt;" style="width:40px;" title="<?php echo $Lang['Btn']['AddAll']; ?>"/>
                							<br />
                							<input name="Add" onclick="checkOptionTransfer(this.form.elements['AvalUserList[]'],this.form.elements['AddUserID[]']);return false;" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&gt;" style="width:40px;" title="<?php echo $Lang['Btn']['AddSelected']; ?>"/>
                							<br /><br />
                							<input name="Remove" onclick="checkOptionTransfer(this.form.elements['AddUserID[]'],this.form.elements['AvalUserList[]']);return false;" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;" style="width:40px;" title="<?php echo $Lang['Btn']['RemoveSelected']; ?>"/>
                							<br />
                							<input name="RemoveAll" onclick="checkOptionAll(this.form.elements['AddUserID[]']);checkOptionTransfer(this.form.elements['AddUserID[]'],this.form.elements['AvalUserList[]']);return false;" type="button" class="formsmallbutton" onmouseover="this.className='formsmallbuttonon'" onmouseout="this.className='formsmallbutton'" value="&lt;&lt;" style="width:40px;" title="<?php echo $Lang['Btn']['RemoveAll']; ?>"/>
                						</td>
                						
										<td bgcolor="#EFFEE2" id="SelectedUserCell">
            
                                            <!-- selected User list  -->
                                            <select name="AddUserID[]" id="AddUserID[]" class="AddUserID" size="11" style="width:99%" multiple="true">
                                            </select>
    									</td>
    								</tr>
    								
    								<tr>
    									<td width="49%" bgcolor="#EEEEEE">
                                            <?php echo $Lang['General']['Or'];?><br>
                                            <?php echo $Lang['SysMgr']['RoleManagement']['SearchUser'];?><br>
        									<div style="float:left;">
        										<input type="text" id="UserSearch" name="UserSearch" value=""/>
        									</div>
										</td>
										<td>&nbsp;</td>
									</tr>
								</table>
								<p class="spacer"></p>
      						</td>
      					</tr>
					</table>
			  		<br />
				</td>
			</tr>
		</table>
	</div>
  	<div class="edit_bottom">
		<span> </span>
		<p class="spacer"></p>
		<input name="AddTeacherSubmitBtn" id="AddTeacherSubmitBtn" type="button" class="formbutton" onclick="addSelectedTeacherToRoute(); return false;" value="<?php echo $Lang['Btn']['Add'];?>" />
	  	<input name="AddTeacherCancelBtn" id="AddTeacherCancelBtn" type="button" class="formbutton" onclick="window.top.tb_remove(); return false;" value="<?php echo $Lang['Btn']['Cancel'];?>" />
    	<p class="spacer"></p>
	</div>
</div>

</form>