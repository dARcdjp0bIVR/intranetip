<?php
/*
 *  Using:
 *
 *  2019-08-09 Cameron
 *      - pass academicYearID as field rather than use current academicYearID when add new route [case #Y166091]
 *      - now allow to change academicYearID when edit
 *
 *  2019-05-15 Cameron
 *      - use transaction to commit/rollback and display the proper message for result
 *       
 *  2019-01-31 Cameron
 *      - apply standardizeFormPostValue to text field to strip backslash
 *      
 *  2019-01-29 Cameron
 *      - change route teacher to be able to select multiple
 */
$db = $indexVar['db'];
// debug_pr($_POST);
// die();
$routeID = IntegerSafe($_POST['routeID']);
$routeName = standardizeFormPostValue($_POST['RouteName']);
$remarks = standardizeFormPostValue($_POST['remarks']);
$type = $_POST['type'];
$startDate = $_POST['StartDate'];
$endDate = $_POST['EndDate'];
$isActive = IntegerSafe($_POST['status']);
$routeCollectionID = IntegerSafe($_POST['routeCollectionID']);
$academicYearID = IntegerSafe($_POST['academicYearID']);;
$vehicleIDAry = IntegerSafe($_POST['vehicleID']);
$vehicleStartDateAry = $_POST['vehicleStartDate'];
$vehicleEndDateAry = $_POST['vehicleEndDate'];
$vehicleRemarksAry = $_POST['vehicleRemarks'];
//$teacherID = $_POST['chooseIndividual'];
$teacherIDAry = IntegerSafe($_POST['RouteTeacherID']);


if($type=='N'){
	//normal route
	$busStops_N = $_POST['busStops_N'];
	$amTime = array(
		'hour'=>$_POST['Am_Time_hour'],
		'min'=>$_POST['Am_Time_min'],
		'sec'=>$_POST['Am_Time_sec']
	);
	$pmTime = array(
			'hour'=>$_POST['Pm_Time_hour'],
			'min'=>$_POST['Pm_Time_min'],
			'sec'=>$_POST['Pm_Time_sec']
	);
	$amRcID = $routeCollectionID['AM'];
	$pmRcID = $routeCollectionID['PM'];
	$am_arrival_time = str_pad($_POST['Am_Arrive_Leave_Time_hour'],2,'0',STR_PAD_LEFT).':'
			.str_pad($_POST['Am_Arrive_Leave_Time_min'],2,'0',STR_PAD_LEFT).':'
			.str_pad($_POST['Am_Arrive_Leave_Time_sec'],2,'0',STR_PAD_LEFT);
	$pm_leave_time = str_pad($_POST['Pm_Arrive_Leave_Time_hour'],2,'0',STR_PAD_LEFT).':'
			.str_pad($_POST['Pm_Arrive_Leave_Time_min'],2,'0',STR_PAD_LEFT).':'
			.str_pad($_POST['Pm_Arrive_Leave_Time_sec'],2,'0',STR_PAD_LEFT);
}else{
	//special route
	$busStops_S = $_POST['busStops_S'];
	$Time['MON'] = array(
			'hour'=>$_POST['Time_MON_hour'],
			'min'=>$_POST['Time_MON_min'],
			'sec'=>$_POST['Time_MON_sec']
	);
	$Time['TUE'] = array(
			'hour'=>$_POST['Time_TUE_hour'],
			'min'=>$_POST['Time_TUE_min'],
			'sec'=>$_POST['Time_TUE_sec']
	);
	$Time['WED'] = array(
			'hour'=>$_POST['Time_WED_hour'],
			'min'=>$_POST['Time_WED_min'],
			'sec'=>$_POST['Time_WED_sec']
	);
	$Time['THU'] = array(
			'hour'=>$_POST['Time_THU_hour'],
			'min'=>$_POST['Time_THU_min'],
			'sec'=>$_POST['Time_THU_sec']
	);
	$Time['FRI'] = array(
			'hour'=>$_POST['Time_FRI_hour'],
			'min'=>$_POST['Time_FRI_min'],
			'sec'=>$_POST['Time_FRI_sec']
	);
	
	$leave_time['MON'] = str_pad($_POST['MON_Leave_Time_hour'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['MON_Leave_Time_min'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['MON_Leave_Time_sec'],2,'0',STR_PAD_LEFT);
	$leave_time['TUE'] = str_pad($_POST['TUE_Leave_Time_hour'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['TUE_Leave_Time_min'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['TUE_Leave_Time_sec'],2,'0',STR_PAD_LEFT);
	$leave_time['WED'] = str_pad($_POST['WED_Leave_Time_hour'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['WED_Leave_Time_min'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['WED_Leave_Time_sec'],2,'0',STR_PAD_LEFT);
	$leave_time['THU'] = str_pad($_POST['THU_Leave_Time_hour'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['THU_Leave_Time_min'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['THU_Leave_Time_sec'],2,'0',STR_PAD_LEFT);
	$leave_time['FRI'] = str_pad($_POST['FRI_Leave_Time_hour'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['FRI_Leave_Time_min'],2,'0',STR_PAD_LEFT).':'
		.str_pad($_POST['FRI_Leave_Time_sec'],2,'0',STR_PAD_LEFT);
	
}


$result = array();
$db->Start_Trans();

if($routeID){
	//Update
    $originalRouteInfo = $db->getRouteInfo(array($routeID));
    $academicYearID = $originalRouteInfo[0]['AcademicYearID'];
	$changefieldNameArr = array('RouteName','AcademicYearID','Remarks','IsActive','Type','StartDate','EndDate','TeacherID');
	$multiRowValueArr = array(array($routeName,$academicYearID,$remarks,$isActive,$type,$startDate,$endDate,$teacherID));
	$conditions = "RouteID = '$routeID'";
	$result['UpdateRoute'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE', 
			$changefieldNameArr, $multiRowValueArr, $conditions, $excludeCommonField=false);
	$is_edit = true;
}else{
	//Insert
	$fieldNameArr = array('RouteName','AcademicYearID','Remarks','IsActive','Type','StartDate','EndDate','TeacherID');
	$multiRowValueArr = array(array($routeName,$academicYearID,$remarks,$isActive,$type,$startDate,$endDate,$teacherID));
	$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
	if($success){
		$routeID = $db->db_insert_id();
	}
	$result['AddRoute'] = $success;
	$is_edit = false;
}

// update route teacher
$sql = "DELETE FROM INTRANET_SCH_BUS_ROUTE_TEACHER WHERE RouteID='".$routeID."'";
$result['DeleteRouteTeacher'] = $db->db_db_query($sql);

if (count($teacherIDAry)) {
    foreach((array)$teacherIDAry as $_teacherID) {
        $sqlAry[] = "('".$routeID."','".$_teacherID."')";
    }
    $sql = "INSERT INTO INTRANET_SCH_BUS_ROUTE_TEACHER (RouteID, TeacherID) VALUES ". implode(",", $sqlAry);
    $result['AddRouteTeacher'] = $db->db_db_query($sql);
}


if($type=='N'){
	if($amRcID){
		$fieldNameArr = array('Time');
		$multiRowValueArr = array(array($am_arrival_time));
		$condition = "RouteCollectionID = '$amRcID'";
		$result['UpdateRouteCollectionAM'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION',
				$fieldNameArr, $multiRowValueArr, $condition, $excludeCommonField=false);
	}else{
		//insert new am route collection
		$fieldNameArr = array('RouteID','AmPm','Time');
		$multiRowValueArr = array(array($routeID,'AM',$am_arrival_time));
		$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
		if($success){
			$amRcID = $db->db_insert_id();
		}
		$result['AddRouteCollectionAM'] = $success;
	}
	if($pmRcID){
		$fieldNameArr = array('Time');
		$multiRowValueArr = array(array($pm_leave_time));
		$condition = "RouteCollectionID = '$pmRcID'";
		$result['UpdateRouteCollectionPM'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION',
				$fieldNameArr, $multiRowValueArr, $condition, $excludeCommonField=false);
	}else{
		//insert new am route collection
		$fieldNameArr = array('RouteID','AmPm','Time');
		$multiRowValueArr = array(array($routeID,'PM',$pm_leave_time));
		$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
		if($success){
			$pmRcID = $db->db_insert_id();
		}
		$result['AddRouteCollectionPM'] = $success;
	}
}else{

	//special route
	//Check if $busStops_S is empty
	if(isset($busStops_S['MON'])){
		if($routeCollectionID['MON']){
			$fieldNameArr = array('Time');
			$multiRowValueArr = array(array($leave_time['MON']));
			$condition = "RouteCollectionID = '".$routeCollectionID['MON']."'";
			$result['UpdateRouteCollectionMon'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION',
					$fieldNameArr, $multiRowValueArr, $condition, $excludeCommonField=false);
		}else{
			//insert new route collection
			$fieldNameArr = array('RouteID','Weekday','Time');
			$multiRowValueArr = array(array($routeID,'MON',$leave_time['MON']));
			$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
			if($success){
				$routeCollectionID['MON'] = $db->db_insert_id();
			}
			$result['AddRouteCollectionMon'] = $success;
		}
	}
	if(isset($busStops_S['TUE'])){
		if($routeCollectionID['TUE']){
			$fieldNameArr = array('Time');
			$multiRowValueArr = array(array($leave_time['TUE']));
			$condition = "RouteCollectionID = '".$routeCollectionID['TUE']."'";
			$result['UpdateRouteCollectionTue'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION',
					$fieldNameArr, $multiRowValueArr, $condition, $excludeCommonField=false);
		}else{
			//insert new route collection
			$fieldNameArr = array('RouteID','Weekday','Time');
			$multiRowValueArr = array(array($routeID,'TUE',$leave_time['TUE']));
			$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
			if($success){
				$routeCollectionID['TUE'] = $db->db_insert_id();
			}
			$result['AddRouteCollectionTue'] = $success;
		}
	}
	if(isset($busStops_S['WED'])){
		if($routeCollectionID['WED']){
			$fieldNameArr = array('Time');
			$multiRowValueArr = array(array($leave_time['WED']));
			$condition = "RouteCollectionID = '".$routeCollectionID['WED']."'";
			$result['UpdateRouteCollectionWed'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION',
					$fieldNameArr, $multiRowValueArr, $condition, $excludeCommonField=false);
		}else{
			//insert new route collection
			$fieldNameArr = array('RouteID','Weekday','Time');
			$multiRowValueArr = array(array($routeID,'WED',$leave_time['WED']));
			$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
			if($success){
				$routeCollectionID['WED'] = $db->db_insert_id();
			}
			$result['AddRouteCollectionWed'] = $success;
		}
	}
	if(isset($busStops_S['THU'])){
		if($routeCollectionID['THU']){
			$fieldNameArr = array('Time');
			$multiRowValueArr = array(array($leave_time['THU']));
			$condition = "RouteCollectionID = '".$routeCollectionID['THU']."'";
			$result['UpdateRouteCollectionThu'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION',
					$fieldNameArr, $multiRowValueArr, $condition, $excludeCommonField=false);
		}else{
			//insert new route collection
			$fieldNameArr = array('RouteID','Weekday','Time');
			$multiRowValueArr = array(array($routeID,'THU',$leave_time['THU']));
			$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
			if($success){
				$routeCollectionID['THU'] = $db->db_insert_id();
			}
			$result['AddRouteCollectionThu'] = $success;
		}
	}
	if(isset($busStops_S['FRI'])){
		if($routeCollectionID['FRI']){
			$fieldNameArr = array('Time');
			$multiRowValueArr = array(array($leave_time['FRI']));
			$condition = "RouteCollectionID = '".$routeCollectionID['FRI']."'";
			$result['UpdateRouteCollectionFri'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION',
					$fieldNameArr, $multiRowValueArr, $condition, $excludeCommonField=false);
		}else{
			//insert new route collection
			
			$fieldNameArr = array('RouteID','Weekday');
			$multiRowValueArr = array(array($routeID,'FRI',$leave_time['FRI']));
			$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $fieldNameArr, $multiRowValueArr, $excludeCommonField=false);
			if($success){
				$routeCollectionID['FRI'] = $db->db_insert_id();
			}
			$result['AddRouteCollectionFri'] = $success;
		}
	}
}

//Clear empty vlaue in array
$routeCollectionID = array_filter($routeCollectionID);

// Set no stop route to is deleted
$deleteArray = array();
if($type=='S'){
	foreach($routeCollectionID as $weekday=>$_rcID){
		if(empty($busStops_S[$weekday])){
			//These route have no stops => set to is deleted
			$deleteArray[] = $_rcID;
		}
	}
	$result['DeleteRouteCollection'] = $db->deleteData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION', $primaryKeyName='RouteCollectionID', $primaryKeyValArr=$deleteArray);
}


//Delete All existing RouteStops Mapping tuples
$sql = "DELETE FROM INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP WHERE RouteCollectionID IN ('".implode("','",$routeCollectionID)."') ";
// debug_pr($sql);
$result['DeleteRouteCollectionStops'] = $db->db_db_query($sql);

//Update Insert RouteStops
if($type=='N'){
	$countOfBusStops = count($busStops_N);
	$newStopsCounter = 0;
	$oldStopsCounter = 0;
	$fieldNameArr = array('RouteCollectionID','BusStopsID','`Order`','Time');
	$insertMultiRowValueArr = array();
	foreach((array)$busStops_N as $key => $busStopsID){
	// 	debug_pr('$busStopsID'.$busStopsID);
		if($key>=0){
			// Existing route_collection_stops_map
			//AM
			$order = $key+1;	// 		debug_pr('AM $order: '.$order);
			$time = str_pad($Am_Time_hour[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Am_Time_min[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Am_Time_sec[$key],2,'0',STR_PAD_LEFT);
			$insertMultiRowValueArr[] = array($amRcID,$busStopsID,$order,$time);
	
	// 		$db->upsertRouteCollectionStopMap($amRcID,$busStopsID,$order,$time);
	// 		debug_pr('AM $time'.$time);
	
			//PM 
			$order = $countOfBusStops - $key;
			$time = str_pad($Pm_Time_hour[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Pm_Time_min[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Pm_Time_sec[$key],2,'0',STR_PAD_LEFT);
			$insertMultiRowValueArr[] = array($pmRcID,$busStopsID,$order,$time);
	// 		$db->upsertRouteCollectionStopMap($pmRcID,$busStopsID,$order,$time);
			$oldStopsCounter++;
		}else{
			// new stops
			//AM
			$newStopsCounter++;
			$order = $oldStopsCounter + $newStopsCounter;
			$time = str_pad($Am_Time_hour[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Am_Time_min[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Am_Time_sec[$key],2,'0',STR_PAD_LEFT);
			$insertMultiRowValueArr[] = array($amRcID,$busStopsID,$order,$time);
			
			//PM
			$order = $countOfBusStops - ($oldStopsCounter + $newStopsCounter) + 1;
	// 		debug_pr($order);
			$time = str_pad($Pm_Time_hour[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Pm_Time_min[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Pm_Time_sec[$key],2,'0',STR_PAD_LEFT);
			$insertMultiRowValueArr[] = array($pmRcID,$busStopsID,$order,$time);
		}
	}
	//Insert route stops
	$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP', $fieldNameArr, $insertMultiRowValueArr, $excludeCommonField=true);
	$result['InsertRouteStopsMapNormal'] = $success;
}else{
	//Sepcial route
	$insertMultiRowValueArr = array();
	$fieldNameArr = array('RouteCollectionID','BusStopsID','`Order`','Time');
	foreach((array)$busStops_S as $weekday => $busStopsIDAry){
		$countOfBusStops = count($busStopsIDAry);
		$newStopsCounter = 0;
		$oldStopsCounter = 0;
		foreach((array)$busStopsIDAry as $key => $busStopsID){
			if($key>=0){
				//Existing route_collection_stops_map
				$order = $key + 1;
				$time = str_pad($Time[$weekday]['hour'][$key],2,'0',STR_PAD_LEFT).':'.str_pad($Time[$weekday]['min'][$key],2,'0',STR_PAD_LEFT).':'.str_pad($Time[$weekday]['sec'][$key],2,'0',STR_PAD_LEFT);
				$rcID = $routeCollectionID[$weekday];
				$insertMultiRowValueArr[] = array($rcID,$busStopsID,$order,$time);
				//$time = str_pad($Time['MON'][$key],2,'0',STR_PAD_LEFT).':'.str_pad($Am_Time_min[$key],2,'0',STR_PAD_LEFT).':'.str_pad($Am_Time_sec[$key],2,'0',STR_PAD_LEFT);
				$oldStopsCounter++;
			}else{
				$newStopsCounter++;
				$time = str_pad($Time[$weekday]['hour'][$key],2,'0',STR_PAD_LEFT).':'.str_pad($Time[$weekday]['min'][$key],2,'0',STR_PAD_LEFT).':'.str_pad($Time[$weekday]['sec'][$key],2,'0',STR_PAD_LEFT);
				$order = $newStopsCounter + $oldStopsCounter;
				$rcID = $routeCollectionID[$weekday];
				$insertMultiRowValueArr[] = array($rcID,$busStopsID,$order,$time);
			}
		}
	}
	//Insert route stops
	$success = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_COLLECTION_STOPS_MAP', $fieldNameArr, $insertMultiRowValueArr, $excludeCommonField=true);
	$result['InsertRouteStopsMapAsa'] = $success;
}

// Vehicle
//delete
$sql = "DELETE FROM INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP WHERE RouteVehicleID IN ('".implode("','",(array)$deleteExistingVehicleID)."') ";
$result['DeleteVehicle'] = $db->db_db_query($sql);

// $vehicleIDAry ;
// $vehicleStartDateAry;
// $vehicleEndDateAry;
// $vehicleRemarksAry;
$fieldNameArr = array('RouteID','VehicleID','IsActive','StartDate','EndDate','Remarks');
$vehicleInsertMultiRowValueArr = array();
foreach((array)$vehicleIDAry as $key=>$vehicleID){
	if($key>0){
		// existing
	    $vehicleMultiRowValueArr[] = array($routeID,$vehicleID,1,$vehicleStartDateAry[$key],$vehicleEndDateAry[$key],standardizeFormPostValue($vehicleRemarksAry[$key]));
		$conditions = "RouteVehicleID = '$key'";
		$result['UpdateVehicleMap'] = $db->updateData($tableName='INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP',
				$fieldNameArr, $vehicleMultiRowValueArr, $conditions, $excludeCommonField=true);
	}else{
		//add new
	    $vehicleInsertMultiRowValueArr[] = array($routeID,$vehicleID,1,$vehicleStartDateAry[$key],$vehicleEndDateAry[$key],standardizeFormPostValue($vehicleRemarksAry[$key]));
	}
}

if(!empty($vehicleInsertMultiRowValueArr)){
    $result['AddVehicleMap'] = $db->insertData($tableName='INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP', $fieldNameArr, $vehicleInsertMultiRowValueArr, $excludeCommonField=true);
}

//$is_edit = ($routeID)?true:false;
if (!in_array(false,$result)) {
    $db->Commit_Trans();
    $indexVar['libSchoolBus']->setTempSession('SettingsRoute',$is_edit? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['AddSuccess']);
}
else {
    $db->RollBack_Trans();
    $indexVar['libSchoolBus']->setTempSession('SettingsRoute',$is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess']);
}
    
$path = "index.php?task=settings/route&academicYearID=".$academicYearID;
$indexVar['libSchoolBus']->redirect($path);















?>