<?php
// Editing by 

$CurrentPage = 'settings/vehicle/edit';

if(isset($_REQUEST['VehicleID'])){
	$VehicleID = IntegerSafe( is_array($_REQUEST['VehicleID'])? $_REQUEST['VehicleID'][0] : $_REQUEST['VehicleID'] );
}

if($VehicleID !='' && $VehicleID > 0){
	$record = $indexVar['db']->getVehicleRecords(array('VehicleID'=>$VehicleID));
	$hidden_fields = '<input type="hidden" name="VehicleID" id="VehicleID" value="'.$VehicleID.'" />'."\n";
	if(count($record)==0){
		intranet_closedb();
		$indexVar['libSchoolBus']->redirect("index.php?task=settings/vehicle");
	}
}else{
	$record = array();
}

$pages_arr = array(
	array($Lang['eSchoolBus']['SettingsArr']['VehicleArr']['MenuTitle'],'index.php?task=settings/vehicle'),
	array($VehicleID > 0? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);

$indexVar['linterface']->LAYOUT_START($returnMsg);
?>
<?=$indexVar['linterface']->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form name="form1" id="form1" action="index.php?task=settings/vehicle/edit_update" method="post" onsubmit="return false;">
	<?=$hidden_fields?>
	<table width="100%" cellpadding="2" class="form_table_v30">
		<col class="field_title">
		<col class="field_c">
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eSchoolBus']['Settings']['Vehicle']['CarNumber']?></td>
			<td>
				<?=$indexVar['linterface']->GET_TEXTBOX_NAME("CarNum", "CarNum", $record[0]['CarNum'], 'required', array())?>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("CarNum_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>	
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eSchoolBus']['Settings']['Vehicle']['CarPlateNumber']?></td>
			<td>
				<?=$indexVar['linterface']->GET_TEXTBOX_NAME("CarPlateNum", "CarPlateNum", $record[0]['CarPlateNum'], 'required', array())?>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("CarPlateNum_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eSchoolBus']['Settings']['Vehicle']['NumberOfSeat']?></td>
			<td>
				<?=$indexVar['linterface']->GET_TEXTBOX_NUMBER("Seat", "Seat", isset($record[0]['Seat'])? $record[0]['Seat']:60, 'required', array('onchange'=>'restrictPositiveNumber(this,60);'))?>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("Seat_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Vehicle']['Driver']?></td>
			<td>
				<?=$Lang['eSchoolBus']['Settings']['Vehicle']['DriverName']?>
				<?=$indexVar['linterface']->GET_TEXTBOX_NAME("DriverName", "DriverName", $record[0]['DriverName'], '', array())?>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("DriverName_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
				<?=$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber']?>
				<?=$indexVar['linterface']->GET_TEXTBOX_NUMBER("DriverTel", "DriverTel", $record[0]['DriverTel'], '', array())?>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("DriverTel_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Vehicle']['Status']?></td>
			<td>
				<?=$indexVar['linterface']->Get_Radio_Button("RecordStatusY", "RecordStatus", 1, 1, $__Class="", $__Display=$Lang['eSchoolBus']['Settings']['Vehicle']['Active'], $__Onclick="",$__isDisabled=0).'&nbsp;'?>
				<?=$indexVar['linterface']->Get_Radio_Button("RecordStatusN", "RecordStatus", 2, 0, $__Class="", $__Display=$Lang['eSchoolBus']['Settings']['Vehicle']['Inactive'], $__Onclick="",$__isDisabled=0)?>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("RecordStatus_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
</table>
<?=$indexVar['linterface']->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submitBtn').'&nbsp;'?>
<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='index.php?task=settings/vehicle'",'cancelBtn')?>
</div>
</form>
<script type="text/javascript" language="javascript">
function restrictPositiveNumber(obj,defaultVal)
{
	var val = $.trim(obj.value);
	var num = parseInt(val);
	
	if(isNaN(num) || val == ''){
		num = defaultVal;
	}
	if(num < 0){
		num = defaultVal;
	}
	obj.value = num;
}

function checkSubmitForm(formObj)
{
	var valid = true;
	var objs = $('input.required');
	for(var i=0;i<objs.length;i++)
	{
		$('#'+objs[i].name+'_Error').hide();
		if(objs[i].type == 'text' && $.trim(objs[i].value)==''){
			$('#'+objs[i].name+'_Error').html('<?=$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData']?>').show();
			valid = false;
		}else if(objs[i].type=='radio' && $('input[name="'+objs[i].name+'"]:checked').val()==''){
			$('#'+objs[i].name+'_Error').show();
			valid = false;
		}
	}
	
	if(valid){
		$('input[type=button]').attr('disabled',true);
		var params = {'ajax_task':'CheckDuplicatedCar', 'CheckCarNum':$.trim($('#CarNum').val()), 'CheckCarPlateNum':$.trim($('#CarPlateNum').val()) };
		if($('#VehicleID').length > 0){
			params['ExcludeVehicleID'] = $('#VehicleID').val();
		}
		$.post(
			'index.php?task=settings/vehicle/ajax',
			params,
			function(returnCode){
				if(returnCode == ''){
					formObj.submit();
				}else{
					if(returnCode == '3' || returnCode == '1'){
						$('#CarNum_Error').html('<?=$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarNum']?>').show();
					}
					if(returnCode == '3' || returnCode == '2'){
						$('#CarPlateNum_Error').html('<?=$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarPlateNum']?>').show();
					}
					$('input[type=button]').attr('disabled',false);
				}
			}
		);
	}
}
</script>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>