<?php

$is_edit = isset($_POST['VehicleID']) && $_POST['VehicleID']!='' && $_POST['VehicleID']>0;
$success = $indexVar['db']->upsertVehicle($_POST);

if($success){
	$indexVar['libSchoolBus']->setTempSession('SettingsVehicle',$is_edit? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['AddSuccess']);
}else{
	$indexVar['libSchoolBus']->setTempSession('SettingsVehicle',$is_edit? $Lang['General']['ReturnMessage']['UpdateUnsuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess']);
}

intranet_closedb();
$indexVar['libSchoolBus']->redirect("index.php?task=settings/vehicle");
?>