<?php
// Editing by
/*
 *  2019-08-09 Cameron
 *      - pass $AcademicYearID to default $ajax_task
 *
 *  2019-04-01 Cameron
 *      - retrieve $currentRouteCollectionID in countUserRouteRecords()
 *      
 *  2018-11-30 Cameron
 *      - also retrieve nrReserved seats in countUserRouteRecords
 */

switch($ajax_task)
{
	case 'getStudentSelection':
		
		$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID));
		$excludeStudentIdAry = Get_Array_By_Key($records,'UserID');
		
		$selection = $indexVar['linterface']->getStudentSelectionByClass($YearClassID,  ' id="StudentID" name="StudentID" ', $StudentID, $all=0, $noFirst=1, $FirstTitle="",$excludeStudentIdAry);
		echo $selection;
	
	break;
	
	case 'getBusStopsSelection':
		
		$routeCollectionIdAry = IntegerSafe($_POST['RouteCollectionID']);
		$id = $_POST['Id'];
		$name = $_POST['Name'];
		
		$selection = $indexVar['linterface']->getBusStopsSelectionByRouteCollection($routeCollectionIdAry, ' id="'.$id.'" name="'.$name.'" ', $__selected="", $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --');
		echo $selection;
		
	break;
	
	case 'countUserRouteRecords':
		
		$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserID'=>$StudentID));
		$ret = count($records);
		
		$timeSlotCurrentRouteCollectionIDAry = array();
		if ($ret) {
		    foreach((array)$records as $_record) {
	            $timeSlot = ($_record['Type'] == 'N') ? $_record['AmPm'] : $_record['Weekday'];
	            $timeSlotCurrentRouteCollectionIDAry[$timeSlot] = $_record['RouteCollectionID'];
		    }
		}
		
		$routeCollectionIDAry = array();
		$timeSlotAssoc = array();
		foreach((array)$RouteCollectionID as $k=>$v) {
		    if ($v) {
		        $routeCollectionIDAry[$k] = $v;
		        $timeSlotAssoc[$v] = $k;
		    }
		}

		$reservedAry = $indexVar['db']->getRouteBusSeat($routeCollectionIDAry, $DateStart, $DateEnd, $AcademicyearID);
		if (count($reservedAry)) {
		    foreach((array)$reservedAry as $_routeCollectionID => $_reservedAry) {
		        foreach((array)$_reservedAry as $__vehicleID => $__reservedAry) {
		            $timeSlot = $timeSlotAssoc[$_routeCollectionID];
		            $currentRouteCollectionID = isset($timeSlotCurrentRouteCollectionIDAry[$timeSlot]) ? $timeSlotCurrentRouteCollectionIDAry[$timeSlot] : '0';
		            $ret .= '^~^'. $timeSlot . '^#^' . $__reservedAry['NrReserved'] . '^#^' . $__reservedAry['NrSeat'] . '^#^' . $currentRouteCollectionID;
		        }
		    }
		}
		
		echo $ret;
		
	break;
	
	case 'get_asa_activity':
		
		$x .= '<table class="common_table_list_v30">';
		$x .= '<tr class="tabletop">
				<th>'.$Lang['eSchoolBus']['Settings']['Student']['Activity'].'</th>
			</tr>'."\n";
		
		for($i=0;$i<count($schoolBusConfig['ActivityTypes']);$i++){
			$x .= '<tr><td>'.$schoolBusConfig['ActivityTypes'][$i][1].'</td></tr>';
		}
		
		$x .= '</table>';
		
		echo $x;
		
	break;
	
	default: 
		if(strpos($ajax_task,'get_route_')!==false && preg_match('/^get_route_(N|S)_(AM|PM|MON|TUE|WED|THU|FRI)$/',$ajax_task,$matches)){
			$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
			$type = $matches[1];
			$dayType = $matches[2];
			$params = array('AcademicYearID'=>$AcademicYearID,'Type'=>$type,'DayType'=>$dayType,'Status'=>1);
			$route_collections = $indexVar['db']->getRouteCollectionByType($params);
			$route_collections_size = count($route_collections);
			
			$x = '<table class="common_table_list_v30">'."\n";
				$x .= '<tr class="tabletop">
						<th>'.$Lang['eSchoolBus']['Settings']['Route']['RouteName'].'</th>
						<th>'.$Lang['eSchoolBus']['Settings']['Route']['BusStop'].'</th>
						</tr>'."\n";
			$x .= '<tr><td>'.$Lang['eSchoolBus']['AttendanceStatusArr']['Parent'].'</td><td><span class="tabletextremark">[ '.$Lang['eSchoolBus']['Settings']['Student']['LeaveBlank'].' ]</span></td></tr>'."\n";
			for($i=0;$i<$route_collections_size;$i++){
				$stops = $indexVar['db']->getBusStopsByRouteCollection($route_collections[$i]['RouteCollectionID']);
				$stops_size = count($stops);
				$x .= '<tr>';
					$x .= '<td rowspan="'.$stops_size.'">'.$route_collections[$i]['RouteName'].'</td>';
					$x .= '<td>'.$stops[0]['BusStopsName'].'</td>';
				$x .= '</tr>';
				for($j=1;$j<$stops_size;$j++){
					
					$x .= '<tr><td>'.$stops[$j]['BusStopsName'].'</td></tr>';
				}
			}
			//if($route_collections_size == 0){
			//	$x .= '<tr><td colspan="2" align="center">'.$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
			//}
			
			$x .= '</table>'."\n";
		
			echo $x;
		}
	break;
}

?>