<?php
/*
 *  using:
 *  
 *  2020-09-03 Cameron
 *      - create this file
 */

if ($indexVar) {
    $db = $indexVar['db'];
}
else {
    header("location: /");
}

include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();

$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
$FormID = IntegerSafe($_POST['FormID']);
$keyword = trim($_POST['keyword']);

$classNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$busStopField = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');
if ($_SESSION['intranet_session_language'] == 'en') {
    $orderBy = "yc.ClassTitleEN, ycu.ClassNumber + 0 ";
}
else {
    $orderBy = "yc.ClassTitleB5, ycu.ClassNumber + 0 ";
}

$sql = "SELECT 
			CONCAT($classNameField,' (',ycu.ClassNumber,')') as YearClass,
			IF(au.UserID IS NOT NULL,CONCAT(au.EnglishName,'<br>',au.ChineseName),CONCAT(u.EnglishName,'<br>',u.ChineseName)) as StudentName,
			ycu.UserID,
            CONCAT($classNameField,' (',LPAD(ycu.ClassNumber,3,'0'),')') as YearClassSort  
		FROM YEAR_CLASS_USER as ycu 
            INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID' 
            INNER JOIN YEAR as y ON y.YearID=yc.YearID 
            INNER JOIN INTRANET_SCH_BUS_USER_ROUTE as ur ON ur.UserID=ycu.UserID AND ur.AcademicYearID=yc.AcademicYearID 
            LEFT JOIN INTRANET_USER as u ON u.UserID=ycu.UserID 
            LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=ycu.UserID 
		WHERE 
		    yc.AcademicYearID='$AcademicYearID' AND y.YearID='$FormID'";
if($keyword != ''){
    $esc_keyword = $db->Get_Safe_Sql_Like_Query($keyword);
    $sql .= " AND (u.EnglishName LIKE '%$esc_keyword%' OR u.ChineseName LIKE '%$esc_keyword%' 
				OR au.EnglishName LIKE '%$esc_keyword%' OR au.ChineseName LIKE '%$esc_keyword%')";
    $no_msg = $i_no_search_result_msg;
}
else {
    $no_msg = $i_no_record_exists_msg;
}
$sql .= " GROUP BY ycu.UserID ORDER BY {$orderBy}";

$classStudentAry = $db->returnResultSet($sql);
$nrRecord = count($classStudentAry);
$dataAry = array();
$rowCount = 0;
$weekday_ary = array('MON','TUE','WED','THU','FRI');

// Report Name
$dataAry[$rowCount++][0] = $Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'];

// Class
$dataAry[$rowCount][] = $Lang['SysMgr']['FormClassMapping']['Class'];
$dataAry[$rowCount][] = $Lang['Identity']['Student'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Settings']['Route']['NormalRoute'];
for ($i=0;$i<3;$i++) {
    $dataAry[$rowCount][] = '';
}
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Settings']['Route']['SpecialRoute'];
$rowCount++;

for ($i=0;$i<2;$i++) {
    $dataAry[$rowCount][] = '';
}
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Settings']['Student']['AMBus'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Settings']['Student']['BusStop'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Settings']['Student']['PMBus'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Settings']['Student']['BusStop'];
for ($i=1;$i<6;$i++) {
    $dataAry[$rowCount][] = $Lang['SysMgr']['Homework']['WeekDay'][$i];
}
$rowCount++;

if ($nrRecord) {
    $involveStudentIds = Get_Array_By_Key($classStudentAry, 'UserID');
    $records = $db->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserID'=>$involveStudentIds,'IsAssoc'=>1));
    foreach ((array)$classStudentAry as $_classStudentAry) {
        $user_routes = $records[$_classStudentAry['UserID']];
        $dataAry[$rowCount][] = $_classStudentAry['YearClass'];
        $dataAry[$rowCount][] = $_classStudentAry['StudentName'];
        $dataAry[$rowCount][] = (isset($user_routes['N']['AM']['RouteName'])? Get_String_Display( $user_routes['N']['AM']['RouteName'].($user_routes['N']['AM']['Cars']!=''? ' ('.$user_routes['N']['AM']['Cars'].')' : '')) : $Lang['eSchoolBus']['Settings']['Student']['NotTake']);
        $dataAry[$rowCount][] = Get_String_Display( $user_routes['N']['AM']['BusStopsName']);
        $dataAry[$rowCount][] = (isset($user_routes['N']['PM']['RouteName'])? Get_String_Display( $user_routes['N']['PM']['RouteName'].($user_routes['N']['PM']['Cars']!=''? ' ('.$user_routes['N']['PM']['Cars'].')' : '')) : $Lang['eSchoolBus']['Settings']['Student']['NotTake']);
        $dataAry[$rowCount][] = Get_String_Display( $user_routes['N']['PM']['BusStopsName']);

        foreach($weekday_ary as $weekday){
            $display_val = '';
            if($user_routes['S'][$weekday]['ActivityID']!=''){
                $display_val .= $schoolBusConfig['ActivityTypes'][$user_routes['S'][$weekday]['ActivityID']-1][1]."\n";
                if($user_routes['S'][$weekday]['RouteCollectionID'] == '0'){
                    $display_val .= $Lang['eSchoolBus']['Settings']['Student']['ByParent'];
                }else{
                    $display_val .= $user_routes['S'][$weekday]['RouteName'].($user_routes['S'][$weekday]['Cars']!=''? ' ('.$user_routes['S'][$weekday]['Cars'].')' : '')."\n";
                    $display_val .= $user_routes['S'][$weekday]['BusStopsName'];
                }
            }
            $dataAry[$rowCount][] = $display_val;
        }
        $rowCount++;
    }
}
else {
    $dataAry[$rowCount][] = $no_msg;
    $rowCount++;
}

$numOfRow = count($dataAry);
for($i=0;$i<$numOfRow;$i++){
    $numOfColumn = count($dataAry[$i]);
    for($j=0;$j<$numOfColumn;$j++){
        $dataAry[$i][$j]=str_replace("<br>","\n",$dataAry[$i][$j]);
    }
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

$filename = "bus_taking_student.csv";

$lexport->EXPORT_FILE($filename,$export_text);
?>