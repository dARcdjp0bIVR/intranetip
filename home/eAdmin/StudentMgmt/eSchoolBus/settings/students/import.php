<?php
// Editing by
/*
 *      2019-08-09 Cameron
 *          - pass AcademicYearID for import
 */

$returnMsg = $indexVar['libSchoolBus']->getTempSession('SettingsStudentsImport');

include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');

if ($_POST['AcademicYearID']) {
    $AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
}
else if ($_GET['AcademicYearID']) {     // return from clicking Cancel in Step 2
    $AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
}
else {
    $AcademicYearID = Get_Current_Academic_Year_ID();
}

$academicYearObj = new academic_year($AcademicYearID);

$CurrentPage = 'settings/students';

$indexVar['linterface']->LAYOUT_START($returnMsg);

$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'],'index.php?task=settings/students&AcademicYearID='.$AcademicYearID);
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],'');
# step information
$STEPS_OBJ[] = array($Lang['StaffAttendance']['SelectCSVFile'], 1);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['CSVConfirmation'], 0);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['ImportResult'], 0);
?>
<?=$indexVar['linterface']->INCLUDE_COMMON_CSS_JS()?>
<br />
<form name="form1" method="POST" action="index.php?task=settings/students/import_confirm" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$indexVar['linterface']->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td><?=$indexVar['linterface']->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
    <tr>
        <td class="field_title"><?=$Lang['General']['AcademicYear']?></td>
        <td>
            <?=$academicYearObj->Get_Academic_Year_Name()?>
            <input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?php echo $AcademicYearID;?>">
        </td>
    </tr>

	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?=$Lang['Btn']['Select']?>
		</td>
		<td width="70%" class="tabletext">
			<input type="file" class="file" name="upload_file"><br />
			<?=$indexVar['linterface']->GET_IMPORT_CODING_CHKBOX()?>
		</td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?> </td>
		<td width="70%" class="tabletext"><a class="tablelink" href="index.php?task=settings/students/get_csv_sample" target="_blank"><?=$Lang['General']['ClickHereToDownloadSample']?></a></td>
	</tr>
	<tr>
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
		<td width="70%" class="tabletext">
	<?php
	$x = '';
	
	for($i=0;$i<count($Lang['eSchoolBus']['Settings']['Student']['ImportArr']);$i++){
		$x .= $Lang['General']['ImportArr']['Column'].'&nbsp;'.($i+1).'&nbsp;:&nbsp;';
		$x .= $Lang['eSchoolBus']['Settings']['Student']['ImportArr'][$i][0].' - '.$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][$i][1];
		if($Lang['eSchoolBus']['Settings']['Student']['ImportArr'][$i][2] !=''){
			$x .= ' [<a class="tablelink" href="javascript:void(0);" onclick="displayInfoLayer(this,\'InfoLayer\',\''.$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][$i][2].'\');">'.$Lang['Btn']['View'].'</a>]';
		}
		$x .= '<br />';
	}
	
	echo $x;
	?>	
		</td>
	</tr>
	<tr><td colspan="2"><?=$indexVar['linterface']->MandatoryField()?></td></tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
			<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='index.php?task=settings/students&AcademicYearID=".$AcademicYearID."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>
<div id="InfoLayer" class="selectbox_layer selectbox_layer_show_info" style="visibility:visible;display:none;">
	<div style="float:right">
		<a href="javascript:void(0);" onclick="$('#InfoLayer').toggle('fast');">
			<img border="0" src="/images/<?=$LAYOUT_SKIN?>/ecomm/btn_mini_off.gif">
		</a>
	</div>
	<p class="spacer"></p>
	<div class="content" style="min-width:256px;max-height:512px;overflow-y:auto;"></div>
</div>
</form>
<br />
<script type="text/javascript" language="JavaScript">
function displayInfoLayer(btnElement,layerId,task)
{
	var btnObj = $(btnElement);
	var layerObj = $('#'+layerId);
	var posX = btnObj.offset().left;
	var posY = btnObj.offset().top + btnObj.height();
	
	if(layerObj.is(':visible') && task == document.last_task){
		layerObj.toggle('fast');
		document.last_task = task;
		return;
	}
	document.last_task = task;
	$.post(
		'index.php?task=settings/students/ajax',
		{
			'ajax_task': task,
            'AcademicYearID': "<?php echo $AcademicYearID;?>"
		},
		function(data){
			$('#'+layerId+' .content').html(data);
			layerObj.css({'position':'absolute','top':posY+'px','left':posX+'px'});
			layerObj.show('fast');
		}
	);
}
</script>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>