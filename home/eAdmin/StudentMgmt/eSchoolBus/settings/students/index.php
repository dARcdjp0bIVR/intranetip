<?php
// Editing by 
/*
 *  2020-09-03 Cameron
 *      - add export function [case #Y187605]
 *
 *  2019-08-09 Cameron
 *      - change import from hyperlink to function so that it can use post method for passing AcademicYearID
 *
 *  2018-11-02 Cameron
 *      - fix sorting order for Class Column ( ClassName - ClassNumber)
 */

$returnMsg = $indexVar['libSchoolBus']->getTempSession('SettingsStudents');
$PreviousAcademicYearID = $indexVar['libSchoolBus']->getTempSession('SettingsStudents_AcademicYearID');
$PreviousFormID = $indexVar['libSchoolBus']->getTempSession('SettingsStudents_FormID');

$CurrentPage = 'settings/students';

$field = trim(urldecode(stripslashes($_REQUEST['field'])));
$order = trim(urldecode(stripslashes($_REQUEST['order'])));
$pageNo = trim(urldecode(stripslashes($_REQUEST['pageNo'])));
$keyword = trim(urldecode(stripslashes($_REQUEST['keyword'])));
$numPerPage = trim(urldecode(stripslashes($_REQUEST['numPerPage'])));

$arrCookies[] = array("ck_eschoolbus_settings_students_page_size", "numPerPage");
$arrCookies[] = array("ck_eschoolbus_settings_students_page_number", "pageNo");
$arrCookies[] = array("ck_eschoolbus_settings_students_page_order", "order");
$arrCookies[] = array("ck_eschoolbus_settings_students_page_field", "field");	
$arrCookies[] = array("ck_eschoolbus_settings_students_page_keyword", "keyword");
updateGetCookies($arrCookies);

if(!isset($field) || $field == '' || !in_array($field,array(1,2,3,4,5,6))) $field = 0; 
if(!isset($order) || $order == '' || !in_array($order,array(0,1))) $order = 1;
if(!isset($pageNo) || $pageNo == '') $pageNo = 1;
if(!isset($numPerPage) || $numPerPage == '') $numPerPage = 50;

$pageSizeChangeEnabled = true;

if(!isset($AcademicYearID)){
	$AcademicYearID = $PreviousAcademicYearID != '' ? $PreviousAcademicYearID : Get_Current_Academic_Year_ID();
}

if(!isset($FormID) && $PreviousFormID != ''){
	$FormID = $PreviousFormID;
}else if(!isset($FormID) || $PrevAcademicYearID != $AcademicYearID)
{
	include_once($intranet_root.'/includes/form_class_manage.php');
	$fcm = new form_class_manage();
	$form_list = $fcm->Get_Form_List(false, $__ActiveOnly=1, $AcademicYearID, $__YearIDArr='', $__WebSAMSCodeArr='');
	$FormID = $form_list[0]['YearID'];
}

$columns = array(
	array('YearClassSort',$Lang['SysMgr']['FormClassMapping']['Class'],'14%',' rowspan="2" '),
	array('StudentName',$Lang['Identity']['Student'],'14%',' rowspan="2" '),
	array('AMBus',$Lang['eSchoolBus']['Settings']['Student']['AMBus'],'8%',''),
	array('AMStop',$Lang['eSchoolBus']['Settings']['Student']['BusStop'],'8%',''),
	array('PMBus',$Lang['eSchoolBus']['Settings']['Student']['PMBus'],'8%',''),
	array('PMStop',$Lang['eSchoolBus']['Settings']['Student']['BusStop'],'8%',''),
	array('Monday',$Lang['SysMgr']['Homework']['WeekDay'][1],'8%',''),
	array('Tuesday',$Lang['SysMgr']['Homework']['WeekDay'][2],'8%',''),
	array('Wednesday',$Lang['SysMgr']['Homework']['WeekDay'][3],'8%',''),
	array('Thursday',$Lang['SysMgr']['Homework']['WeekDay'][4],'8%',''),
	array('Friday',$Lang['SysMgr']['Homework']['WeekDay'][5],'8%','')
);


$columns_ary = array(18,18,18,18,18,18,18,18,18,18,18);

$classNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$busStopField = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');
/*
$sql = "SELECT 
			CONCAT($classNameField,' (',ycu.ClassNumber,')') as YearClass,
			CONCAT(u.EnglishName,'<br>',u.ChineseName) as StudentName,
			'' as AMBus,
			'' as AMStop,
			'' as PMBus,
			'' as PMStop,
			'' as Mon,
			'' as Tue,
			'' as Wed,
			'' as Thu,
			'' as Friday,
			CONCAT('<input type=\"checkbox\" name=\"StudentID[]\" value=\"',u.UserID,'\" />') as CheckBox,
			u.UserID  
		FROM INTRANET_USER as u 
		INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=u.UserID 
		INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID' 
		INNER JOIN YEAR as y ON y.YearID=yc.YearID 
		INNER JOIN INTRANET_SCH_BUS_USER_ROUTE as ur ON ur.UserID=u.UserID AND ur.AcademicYearID=yc.AcademicYearID 
		WHERE yc.AcademicYearID='$AcademicYearID' AND y.YearID='$FormID' 
		GROUP BY u.UserID ";
*/
$sql = "SELECT 
			CONCAT($classNameField,' (',ycu.ClassNumber,')') as YearClass,
			IF(au.UserID IS NOT NULL,CONCAT(au.EnglishName,'<br>',au.ChineseName),CONCAT(u.EnglishName,'<br>',u.ChineseName)) as StudentName,
			'' as AMBus,
			'' as AMStop,
			'' as PMBus,
			'' as PMStop,
			'' as Mon,
			'' as Tue,
			'' as Wed,
			'' as Thu,
			'' as Friday,
			CONCAT('<input type=\"checkbox\" name=\"StudentID[]\" value=\"',ycu.UserID,'\" />') as CheckBox,
			ycu.UserID,
            CONCAT($classNameField,' (',LPAD(ycu.ClassNumber,3,'0'),')') as YearClassSort  
		FROM YEAR_CLASS_USER as ycu 
		INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID' 
		INNER JOIN YEAR as y ON y.YearID=yc.YearID 
		INNER JOIN INTRANET_SCH_BUS_USER_ROUTE as ur ON ur.UserID=ycu.UserID AND ur.AcademicYearID=yc.AcademicYearID 
		LEFT JOIN INTRANET_USER as u ON u.UserID=ycu.UserID 
		LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=ycu.UserID 
		WHERE yc.AcademicYearID='$AcademicYearID' AND y.YearID='$FormID'"; 

if($keyword != ''){
	$esc_keyword = $indexVar['db']->Get_Safe_Sql_Like_Query($keyword);
	$sql .= " AND (u.EnglishName LIKE '%$esc_keyword%' OR u.ChineseName LIKE '%$esc_keyword%' 
				OR au.EnglishName LIKE '%$esc_keyword%' OR au.ChineseName LIKE '%$esc_keyword%')";
}
$sql .= " GROUP BY ycu.UserID ";

//debug_pr($sql);
//$table_content = $indexVar['linterface']->getDbTable($sql, $columns, $field, $order, $pageNo, $numPerPage, true, $column_ary, 'StudentID[]');

### Stat of customizing dbtable
include_once($intranet_root."/includes/libdbtable.php");
include_once($intranet_root."/includes/libdbtable2007a.php");
		
$li = new libdbtable2007($field,$order,$pageNo);
$li->sql = $sql;
$li->IsColOff = "IP25_table";
$li->field_array = Get_Array_By_Key($columns, '0');
$li->column_array = $columns_ary;
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0);
$li->page_size = $numPerPage;

/*
$pos = 0;
$li->column_list .= "<th width=\"1\" class=\"num_check\" rowspan=\"2\">#</th>\n";
for($i=0;$i<count($columns);$i++){
	$li->column_list .= '<th width="'.$columns[$i][2].'"'.($columns[$i][3]).'>'.$li->column_IP25($pos++,$columns[$i][1])."</th>\n";
}
$li->column_list .= '<th width="1" rowspan="2">'.$li->check("StudentID[]")."</th>\n";
*/

$li->no_col = sizeof($li->field_array)+2;
$sql = $li->built_sql();
$li->rs = $li->db_db_query($sql);
$n_start = $li->n_start;
$x = '';
$x .= "<table class='common_table_list_v30'>\n<thead>\n";
//$x .= $li->displayColumn();
$pos = 0;
$x .= '<tr><th width="1" class="num_check" rowspan="2">#</th>
			<th width="14%" rowspan="2">'.$li->column_IP25($pos++,$columns[0][1]).'</th>
			<th width="14%" rowspan="2">'.$li->column_IP25($pos++,$columns[1][1]).'</th>
			<th width="32%" colspan="4" style="text-align:center;">'.$Lang['eSchoolBus']['Settings']['Route']['NormalRoute'].'</th>
			<th width="40%" colspan="5" style="text-align:center;">'.$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute'].'</th>
			<th width="1" rowspan="2">'.$li->check("StudentID[]")."</th>\n";
$x .= '</tr>';
$x .= '<tr>';
for($i=2;$i<count($columns);$i++){
	$x .= '<th>'.$columns[$i][1].'</th>';
}
$x .= '</tr>'."\n";

$weekday_ary = array('MON','TUE','WED','THU','FRI');
$i = 0;
if ($li->db_num_rows()==0){
		$x .= "<tr><td class=\"tableContent\" align=\"center\" colspan=\"".$li->no_col."\"><br>".$li->no_msg."<br><br></td></tr>\n";
} else {
	$rows = array();
	while($row = $li->db_fetch_array()) {
		$rows[] = $row;
	}
	
	$involveStudentIds = Get_Array_By_Key($rows, 'UserID');
	$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserID'=>$involveStudentIds,'IsAssoc'=>1));
	//$records2 = $indexVar['db']->getSpecialUserRouteRecords(array('UserID'=>$involveStudentIds,'AcademicYearID'=>$AcademicYearID,'IsAssoc'=>1));
	
	//while($row = $li->db_fetch_array()) {
	while($row = array_shift($rows)){
		$i++;
		$css = ($i%2) ? "" : "2";
		if($i > $li->page_size) break;
		
		//$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserID'=>$row['UserID'],'IsAssoc'=>1));
		$user_routes = $records[$row['UserID']];
		/*
		$special_user_routes = $records2[$row['UserID']];
		
		$am_special = '<span class="red">';
		$am_special_stops = '<span class="red">';
		$pm_special = '<span class="red">';
		$pm_special_stops = '<span class="red">';
		if(count($special_user_routes)>0){
			foreach($special_user_routes as $tmp_date => $ary){
				if(isset($ary['AM'])){
					$am_special .= '<br />'.$tmp_date.'<br />'.($ary['AM']['RouteCollectionID']=='0'? $Lang['eSchoolBus']['Settings']['Student']['ByParent'] : $ary['AM']['RouteName']. ($ary['AM']['Cars']!=''? ' ('.$ary['AM']['Cars'].')' : '') );
					$am_special_stops .= '<br />'.$tmp_date.'<br />'.($ary['AM']['RouteCollectionID']=='0'? $Lang['General']['EmptySymbol'] : $ary['AM']['BusStopsName']);
				}
				if(isset($ary['PM'])){
					$pm_special .= '<br />'.$tmp_date.'<br />'.($ary['PM']['RouteCollectionID']=='0'? $Lang['eSchoolBus']['Settings']['Student']['ByParent'] : $ary['PM']['RouteName']. ($ary['PM']['Cars']!=''? ' ('.$ary['PM']['Cars'].')' : '') );
					$pm_special_stops .= '<br />'.$tmp_date.'<br />'.($ary['PM']['RouteCollectionID']=='0'? $Lang['General']['EmptySymbol'] : $ary['PM']['BusStopsName']);
				}
			}
		}
		$am_special .= '</span>';
		$am_special_stops .= '</span>';
		$pm_special .= '</span>';
		$pm_special_stops .= '</span>';
		*/
		$x .= "<tr>\n";
		$x .= "<td class=\"tableContent$css\">".($n_start+$i)."</td>\n";
		//for($j=0; $j<$li->no_col-1; $j++){
		//	$x .= $li->displayCell($j,$row[$j], $css);
		//}
		$x .= $li->displayCell(0,$row[0], $css);
		$x .= $li->displayCell(1,$row[1], $css);
		
		$x .= "<td class=\"".$li->returnCellClassID()."$css\">".(isset($user_routes['N']['AM']['RouteName'])? Get_String_Display( $user_routes['N']['AM']['RouteName'].($user_routes['N']['AM']['Cars']!=''? ' ('.$user_routes['N']['AM']['Cars'].')' : '')) : '<span class="red">'.$Lang['eSchoolBus']['Settings']['Student']['NotTake'].'</span>' ).$am_special."</td>\n";
		$x .= "<td class=\"".$li->returnCellClassID()."$css\">".Get_String_Display( $user_routes['N']['AM']['BusStopsName']).$am_special_stops."</td>\n";
		$x .= "<td class=\"".$li->returnCellClassID()."$css\">".(isset($user_routes['N']['PM']['RouteName'])? Get_String_Display( $user_routes['N']['PM']['RouteName'].($user_routes['N']['PM']['Cars']!=''? ' ('.$user_routes['N']['PM']['Cars'].')' : '')) : '<span class="red">'.$Lang['eSchoolBus']['Settings']['Student']['NotTake'].'</span>' ).$pm_special."</td>\n";
		$x .= "<td class=\"".$li->returnCellClassID()."$css\">".Get_String_Display( $user_routes['N']['PM']['BusStopsName']).$pm_special_stops."</td>\n";
		
		foreach($weekday_ary as $weekday){
			$display_val = '';
			if($user_routes['S'][$weekday]['ActivityID']!=''){
				$display_val .= $schoolBusConfig['ActivityTypes'][$user_routes['S'][$weekday]['ActivityID']-1][1]."\n";
				if($user_routes['S'][$weekday]['RouteCollectionID'] == '0'){
					$display_val .= '<span class="red">'.$Lang['eSchoolBus']['Settings']['Student']['ByParent'].'</span>';
				}else{
					$display_val .= $user_routes['S'][$weekday]['RouteName'].($user_routes['S'][$weekday]['Cars']!=''? ' ('.$user_routes['S'][$weekday]['Cars'].')' : '')."\n";
					$display_val .= $user_routes['S'][$weekday]['BusStopsName'];
				}
			}
			
			$x .= "<td class=\"".$li->returnCellClassID()."$css\">". nl2br($display_val) ."&nbsp;</td>\n";
		}
		
		$x .= $li->displayCell($li->no_col-2,$row[$li->no_col-2], $css);
		
		$x .= "</tr>\n";
	}
}
$x .= "</thead></table>\n";
if($li->db_num_rows()<>0 && $li->no_navigation_row!=true) $x .= $li->navigation_IP25("","");
$li->db_free_result();


### End of customizing dbtable

$tool_buttons = array();
$tool_buttons[] = array('edit','javascript:checkEdit2(document.form1,\'StudentID[]\',\'editStudent(false)\');','','','');
$tool_buttons[] = array('delete','javascript:checkRemove(document.form1,\'StudentID[]\',\'index.php?task=settings/students/delete\')','','');

$indexVar['linterface']->LAYOUT_START($returnMsg);
?>
<br />
<form id="form1" name="form1" method="post" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
			<?=$indexVar['linterface']->GET_LNK_NEW('javascript:editStudent(true);', $Lang['Btn']['New'], $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1).'&nbsp;'?>
			<?=$indexVar['linterface']->GET_LNK_IMPORT('javascript:importStudent();', $Lang['Btn']['Import'], $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1)?>
            <?=$indexVar['linterface']->GET_LNK_EXPORT('javascript:exportStudent();', $Lang['Btn']['Export'], $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=1)?>
		</div>
		<div class="Conntent_search"><input type="text" id="keyword" name="keyword" value="<?=intranet_htmlspecialchars(stripslashes($keyword))?>"></div>
		<br style="clear:both;">
	</div>
	<div class="table_filter">
		<?=$indexVar['linterface']->getAcademicYearSelection(' id="AcademicYearID" name="AcademicYearID" onchange="document.form1.submit();" ', $AcademicYearID, $__all=0, $__noFirst=1, $__FirstTitle="")?>
		<?=$indexVar['linterface']->getFormSelection($AcademicYearID, ' id="FormID" name="FormID" onchange="document.form1.submit();" ', $FormID, $__all=0, $__noFirst=1, $__FirstTitle="")?>
	</div>
	<br style="clear:both">	
	<?=$indexVar['linterface']->Get_DBTable_Action_Button_IP25($tool_buttons)?>
	<br style="clear:both">			
	<?=$x?>
	<input type="hidden" id="PrevAcademicYearID" name="PrevAcademicYearID" value="<?=$AcademicYearID?>" />
	<input type="hidden" id="pageNo" name="pageNo" value="<?=$pageNo?>">
	<input type="hidden" id="order" name="order" value="<?=$order?>">
	<input type="hidden" id="field" name="field" value="<?=$field?>">
	<input type="hidden" id="page_size_change" name="page_size_change" value="">
	<input type="hidden" id="numPerPage" name="numPerPage" value="<?=$numPerPage?>">
</form>	
<br /><br />
<script type="text/javascript" language="javascript">
function editStudent(isNew)
{
	if(isNew){
		setChecked(0,document.form1,'StudentID[]');
	}
	document.form1.action='index.php?task=settings/students/edit';
	document.form1.submit();
}

function importStudent()
{
    document.form1.action='index.php?task=settings/students/import';
    document.form1.submit();
}

function exportStudent()
{
    document.form1.action='index.php?task=settings/students/export';
    document.form1.submit();
    document.form1.action='index.php?task=settings/students/index';     // reset
}

</script>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>