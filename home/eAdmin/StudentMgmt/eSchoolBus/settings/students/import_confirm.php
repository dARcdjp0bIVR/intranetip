<?php
// Editing by 
/*
 *  2019-08-09 Cameron
 *      - allow to import students for non-current academic year
 *
 *  2019-05-07 Cameron
 *      - restrict start date and end date format to standard YYYY-MM-DD format to avoid problem [case #Y160648]
 *      
 *  2019-03-29 Cameron
 *      - add checking for exceeding number of bus seat [case #Y152911]
 */

include_once($intranet_root."/includes/libuser.php");
include_once($intranet_root."/includes/libimporttext.php");
include_once($intranet_root."/includes/libfilesystem.php");

$luser = new libuser();
$limport = new libimporttext();
$lf = new libfilesystem();

//$AcademicYearID = Get_Current_Academic_Year_ID();
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);

$header_array = array('Login ID', 'AM Normal Route', 'AM Normal Route Bus Stop', 'PM Normal Route', 'PM Normal Route Bus Stop', 
					'Monday ASA Route Activity', 'Monday ASA Route', 'Monday ASA Route Bus Stop', 'Tueday ASA Route Activity', 'Tueday ASA Route', 'Tueday ASA Route Bus Stop',	
					'Wednesday ASA Route Activity',	'Wednesday ASA Route', 'Wednesday ASA Route Bus Stop', 'Thursday ASA Route Activity', 'Thursday ASA Route',	'Thursday ASA Route Bus Stop', 
					'Friday ASA Route Activity', 'Friday ASA Route', 'Friday ASA Route Bus Stop', 'Start Date', 'End Date'
);

$filepath = $_FILES["upload_file"]["tmp_name"];
$filename = $_FILES["upload_file"]["name"];

if($filepath=="none" || $filepath == "")
{
	# import failed
    intranet_closedb();
    $indexVar['libSchoolBus']->setTempSession('SettingsStudentsImport',$Lang['General']['ReturnMessage']['ImportUnsuccess']);
    $indexVar['libSchoolBus']->redirect('index.php?task=settings/students/import');
    exit();
}else
{
	$ext = strtoupper($lf->file_ext($filename));
    if($limport->CHECK_FILE_EXT($filename))
    {
        # read file into array
        # return 0 if fail, return csv array if success
        //$data = $lf->file_read_csv($filepath);
        $data = $limport->GET_IMPORT_TXT($filepath);
        if(sizeof($data)>0)
        {
        	$csv_header = array_shift($data);                   # drop the title bar
        	$format_wrong = false;
        	for($i=0; $i<sizeof($header_array); $i++)
			{
				if ($csv_header[$i]!=$header_array[$i])
				{
					$format_wrong = true;
					break;
				}
			}
			if($format_wrong){
				intranet_closedb();
			    $indexVar['libSchoolBus']->setTempSession('SettingsStudentsImport',$Lang['General']['ReturnMessage']['ImportUnsuccess_IncorrectHeaderFormat']);
	    		$indexVar['libSchoolBus']->redirect('index.php?task=settings/students/import');
			    exit();
			}
        }else
        {
		    intranet_closedb();
		    $indexVar['libSchoolBus']->setTempSession('SettingsStudentsImport',$Lang['General']['ReturnMessage']['ImportUnsuccess_NoRecord']);
    		$indexVar['libSchoolBus']->redirect('index.php?task=settings/students/import');
		    exit();
        }
    }
    
    $data_size = count($data);

	$cur_sessionid = session_id();
	$cur_time = date("Y-m-d H:i:s");
	/*
	$sql = "CREATE TABLE IF NOT EXISTS INTRANET_SCH_BUS_IMPORT_STUDENT_ROUTE_SETTINGS (
				RecordID int(11) NOT NULL auto_increment,
				SessionID varchar(300) NOT NULL,
				ImportTime datetime NOT NULL,
				LoginID varchar(50),
				AMRoute varchar(50),
				AMBusStop varchar(50),
				PMRoute varchar(50),
				PMBusStop varchar(50),
				MONASAActivity varchar(50),
				MONASARoute varchar(50),
				MONASABusStop varchar(50),
				TUEASAActivity varchar(50),
				TUEASARoute varchar(50),
				TUEASABusStop varchar(50),
				WEDASAActivity varchar(50),
				WEDASARoute varchar(50),
				WEDASABusStop varchar(50),
				THUASAActivity varchar(50),
				THUASARoute varchar(50),
				THUASABusStop varchar(50),
				FRIASAActivity varchar(50),
				FRIASARoute varchar(50),
				FRIASABusStop varchar(50),
				StartDate date,
				EndDate date,
				PRIMARY KEY(RecordID),
				INDEX IdxSessionID(SessionID),
				INDEX IdxImportTime(ImportTime) 
			)ENGINE=InnoDB DEFAULT CHARSET=utf8";
	*/
	$sql = "CREATE TABLE IF NOT EXISTS INTRANET_SCH_BUS_IMPORT_STUDENT_ROUTE_SETTINGS (
				RecordID int(11) NOT NULL auto_increment,
				SessionID varchar(300) NOT NULL,
				ImportTime datetime NOT NULL,
				StudentID int(11) NOT NULL,
				AM_RouteCollectionID int(11) DEFAULT NULL,
				AM_BusStopsID int(11) DEFAULT NULL,
				PM_RouteCollectionID int(11) DEFAULT NULL,
				PM_BusStopsID int(11) DEFAULT NULL,
				MON_ActivityID int(8) DEFAULT NULL,
				MON_RouteCollectionID int(11) DEFAULT NULL,
				MON_BusStopsID int(11) DEFAULT NULL,
				TUE_ActivityID int(8) DEFAULT NULL,
				TUE_RouteCollectionID int(11) DEFAULT NULL,
				TUE_BusStopsID int(11) DEFAULT NULL,
				WED_ActivityID int(8) DEFAULT NULL,
				WED_RouteCollectionID int(11) DEFAULT NULL,
				WED_BusStopsID int(11) DEFAULT NULL,
				THU_ActivityID int(8) DEFAULT NULL,
				THU_RouteCollectionID int(11) DEFAULT NULL,
				THU_BusStopsID int(11) DEFAULT NULL,
				FRI_ActivityID int(8) DEFAULT NULL,
				FRI_RouteCollectionID int(11) DEFAULT NULL,
				FRI_BusStopsID int(11) DEFAULT NULL,
				DateStart date,
				DateEnd date,
				PRIMARY KEY(RecordID),
				INDEX IdxSessionID(SessionID),
				INDEX IdxImportTime(ImportTime) 
			)ENGINE=InnoDB DEFAULT CHARSET=utf8";
	//debug_pr($sql);
	
	$indexVar['db']->db_db_query($sql);

	$threshold_date = date("Y-m-d H:i:s", strtotime("-1 day"));
	$sql = "DELETE FROM INTRANET_SCH_BUS_IMPORT_STUDENT_ROUTE_SETTINGS WHERE SessionID='$cur_sessionid' OR ImportTime>'$threshold_date'";
	$indexVar['db']->db_db_query($sql);
	//debug_pr($sql);
}

$name_field = getNameFieldWithClassNumberByLang("u.");
$sql = "SELECT u.UserID,$name_field as StudentName,u.UserLogin FROM INTRANET_USER as u WHERE u.RecordType='".USERTYPE_STUDENT."' AND u.RecordStatus='1'";
$students = $indexVar['db']->returnResultSet($sql);
//debug_pr($sql);
//$students = $luser->returnUsersByIdentity(USERTYPE_STUDENT,"", $AcadmicYearID);
$students_size = count($students);
$userloginToStudentAry = array();
for($i=0;$i<$students_size;$i++){
	$userloginToStudentAry[$students[$i]['UserLogin']] = $students[$i];
}
//debug_pr($userloginToStudentAry);
$route_collections = $indexVar['db']->getRouteCollectionByType(array('AcademicYearID'=>$AcademicYearID,'Status'=>1));
$route_collections_size = count($route_collections);

$routeNameToAry = array();
$collectionIdToBusStopAry = array();
$routeCollectionsIdAry = array();
for($i=0;$i<$route_collections_size;$i++){
	if($route_collections[$i]['AmPm'] != ''){
		$routeNameToAry[$route_collections[$i]['AmPm']][$route_collections[$i]['RouteName']] = $route_collections[$i];
	}else{
		$routeNameToAry[$route_collections[$i]['Weekday']][$route_collections[$i]['RouteName']] = $route_collections[$i];
	}
	//$collectionIdToAry[$route_collections[$i]['RouteCollectionID']] = $route_collections[$i];
	$routeCollectionsIdAry[] = $route_collections[$i]['RouteCollectionID'];
}

$busStopNameToAry = array();
$bus_stops = $indexVar['db']->getBusStopsByRouteCollection($routeCollectionsIdAry);
$bus_stops_size = count($bus_stops);
for($i=0;$i<$bus_stops_size;$i++){
	$busStopNameToAry[$bus_stops[$i]['BusStopsName']] = $bus_stops[$i];
	if(!isset($collectionIdToBusStopAry[$bus_stops[$i]['RouteCollectionID']])){
		$collectionIdToBusStopAry[$bus_stops[$i]['RouteCollectionID']] =  array();
	}
	$collectionIdToBusStopAry[$bus_stops[$i]['RouteCollectionID']][] = $bus_stops[$i]['BusStopsName'];
}

$activityNameToAry = array();
for($i=0;$i<count($schoolBusConfig['ActivityTypes']);$i++){
	$activityNameToAry[$schoolBusConfig['ActivityTypes'][$i][1]] = $schoolBusConfig['ActivityTypes'][$i][0];
}


$am_pm_ary = array('AM','PM');
$weekday_ary = array('MON','TUE','WED','THU','FRI');

$errorAry = array();
$valueAry = array();

$routeCollectionSeatAry = array();

for($i=0;$i<$data_size;$i++) {
	
	$tmp_route = array();
	$tmp_busstop = array();
	$tmp_act = array();
	list($login_id, $tmp_route['AM'], $tmp_busstop['AM'], $tmp_route['PM'], $tmp_busstop['PM'], 
		$tmp_act['MON'], $tmp_route['MON'], $tmp_busstop['MON'], 
		$tmp_act['TUE'], $tmp_route['TUE'], $tmp_busstop['TUE'], 
		$tmp_act['WED'], $tmp_route['WED'], $tmp_busstop['WED'], 
		$tmp_act['THU'], $tmp_route['THU'], $tmp_busstop['THU'], 
		$tmp_act['FRI'], $tmp_route['FRI'], $tmp_busstop['FRI'], 
		 $start_date, $end_date) = $data[$i];
	$row_num = $i+2;
	
	$values = '';
	$error_row = array();
	
	if(!isset($userloginToStudentAry[$login_id])){
		$error_row[] = str_replace('<!--TARGET-->',$login_id, $Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
	}
	
	$values = "('".$cur_sessionid."','".$cur_time."','".$userloginToStudentAry[$login_id]['UserID']."'";
	
	if (!intranet_validateDate($start_date)) {
	    $error_row[] = $Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidStartDate'];
	}
	if (!intranet_validateDate($end_date)) {
	    $error_row[] = $Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidEndDate'];
	}
	
	$start_date_val = convertDateTimeToStandardFormat($start_date, false);
	$end_date_val = convertDateTimeToStandardFormat($end_date, false);
	
	foreach($am_pm_ary as $day_type)
	{
		if($tmp_route[$day_type] != '')
		{
		    $_routeCollectionID = $routeNameToAry[$day_type][$tmp_route[$day_type]]['RouteCollectionID'];

            if ($start_date_val != '' && $end_date_val != '') {
                $_routeSeatInfoAry = $indexVar['db']->getRouteBusSeat($_routeCollectionID, $start_date_val, $end_date_val, $AcademicYearID);
            }
		    
		    if (count($_routeSeatInfoAry)) {
    		    $_routeSeatInfoAry = current(current($_routeSeatInfoAry));
    		    $routeCollectionSeatAry[$_routeCollectionID] += 1;
    		    $checkMaxSeat = true;
		    }
		    else {
		        $checkMaxSeat = false;
		    }
		    
			if(!isset($routeNameToAry[$day_type][$tmp_route[$day_type]])){
				$error_row[] = str_replace('<!--TARGET-->',$tmp_route[$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
			}else if($tmp_busstop[$day_type] == ''){
				$error_row[] = str_replace('<!--TARGET-->',$Lang['eSchoolBus']['Settings']['Student'][$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['BusStopIsNotSet']);
			}else if(!isset($busStopNameToAry[$tmp_busstop[$day_type]])){
				$error_row[] = str_replace('<!--TARGET-->',$tmp_busstop[$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
			}else if(!isset($collectionIdToBusStopAry[$busStopNameToAry[$tmp_busstop[$day_type]]['RouteCollectionID']])){
			 	$error_row[] = str_replace('<!--TARGET-->',$tmp_busstop[$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
		 	}else if(!in_array($tmp_busstop[$day_type], $collectionIdToBusStopAry[$busStopNameToAry[$tmp_busstop[$day_type]]['RouteCollectionID']])){
		 		$error_row[] = str_replace('<!--TARGET-->', $tmp_busstop[$day_type], $Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
		 	}else if($checkMaxSeat && ($routeCollectionSeatAry[$_routeCollectionID] + intval($_routeSeatInfoAry['NrReserved']) > $_routeSeatInfoAry['NrSeat'])){
		 	    $error_row[] = $Lang['eSchoolBus']['Settings']['Student']['ImportError']['ExceedBusSeat'].$_routeSeatInfoAry['NrSeat'];
		 	}else{
		 		$values .= ",'".$routeNameToAry[$day_type][$tmp_route[$day_type]]['RouteCollectionID']."','".$busStopNameToAry[$tmp_busstop[$day_type]]['BusStopsID']."'";
		 	}
		}else{
			$values .= ",NULL,NULL";
		}
	}
 	
 	foreach($weekday_ary as $day_type){
 		if($tmp_act[$day_type] != ''){
 		    $_routeCollectionID = $routeNameToAry[$day_type][$tmp_route[$day_type]]['RouteCollectionID'];
 		    if ($start_date_val != '' && $end_date_val != '') {
 		        $_routeSeatInfoAry = $indexVar['db']->getRouteBusSeat($_routeCollectionID, $start_date_val, $end_date_val, $AcademicYearID);
 		    }
 		    if (count($_routeSeatInfoAry)) {
     		    $_routeSeatInfoAry = current(current($_routeSeatInfoAry));
     		    $routeCollectionSeatAry[$_routeCollectionID] += 1;
     		    $checkMaxSeat = true;
 		    }
 		    else {
 		        $checkMaxSeat = false;
 		    }
 		    
 			if(!isset($activityNameToAry[$tmp_act[$day_type]])){
 				$error_row[] = str_replace('<!--TARGET-->',$tmp_act[$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
 			}else if($tmp_route[$day_type] == $Lang['eSchoolBus']['Settings']['Student']['ByParent']){
 				$values .= ",'".$activityNameToAry[$tmp_act[$day_type]]."',0,NULL";
 			}else if($tmp_route[$day_type] == ''){
 				$error_row[] = str_replace('<!--TARGET-->', $Lang['eSchoolBus']['Settings']['Route']['Weekday'][$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['RouteIsNotSet']);
 			}else if(!isset($routeNameToAry[$day_type][$tmp_route[$day_type]])){
 				$error_row[] = str_replace('<!--TARGET-->',$tmp_route[$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
 			}else if($tmp_busstop[$day_type] == ''){
 				$error_row[] = str_replace('<!--TARGET-->', $Lang['eSchoolBus']['Settings']['Route']['Weekday'][$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['BusStopIsNotSet']);
 			}else if(!isset($busStopNameToAry[$tmp_busstop[$day_type]])){
 				$error_row[] = str_replace('<!--TARGET-->', $tmp_busstop[$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
 			}else if(!in_array($tmp_busstop[$day_type], $collectionIdToBusStopAry[$busStopNameToAry[$tmp_busstop[$day_type]]['RouteCollectionID']])){
 				$error_row[] = str_replace('<!--TARGET-->', $tmp_busstop[$day_type],$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound']);
 			}else if($checkMaxSeat && ($routeCollectionSeatAry[$_routeCollectionID] + intval($_routeSeatInfoAry['NrReserved']) > $_routeSeatInfoAry['NrSeat'])){
 			    $error_row[] = $Lang['eSchoolBus']['Settings']['Student']['ImportError']['ExceedBusSeat'].$_routeSeatInfoAry['NrSeat'];
 			}else{
 				$values .= ",'".$activityNameToAry[$tmp_act[$day_type]]."','".$routeNameToAry[$day_type][$tmp_route[$day_type]]['RouteCollectionID']."','".$busStopNameToAry[$tmp_busstop[$day_type]]['BusStopsID']."'";
 			}
 		}else{
 			$values .= ",NULL,NULL,NULL";
 		}
 	}
 	
// 	$start_date_val = convertDateTimeToStandardFormat($start_date, false);
 	if($start_date_val == ''){
// 		$error_row[] = $Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidStartDate'];
 	}else{
 		$values .= ",'$start_date_val'";
 	}
 	
// 	$end_date_val = convertDateTimeToStandardFormat($end_date, false);
 	if($end_date_val == ''){
// 		$error_row[] = $Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidEndDate'];
 	}else{
 		$values .= ",'$end_date_val'";
 	}
 	
 	if($start_date_val != '' && $end_date_val != '' && $start_date_val > $end_date_val){
 		$error_row[] = $Lang['General']['JS_warning']['InvalidDateRange'];
 	}
 	
 	$values .= ")";
 	//debug_pr($values);
 	if(count($error_row)>0){
		$errorAry[$row_num] = $error_row;
	}else{
		$valueAry[] = $values;
	}
}

$x = '<table class="common_table_list_v30">';
$x .= '<thead>
			<tr>
				<th class="num_check">'.$Lang['eSchoolBus']['Settings']['Student']['RowNumber'].'</th>';
	foreach($header_array as $header){
		$x .= '<th>'.$header.'</th>';
	}
		$x .= '<th>'.$Lang['eSchoolBus']['Settings']['Student']['Errors'].'</th>
			</tr>
		</thead>';
$x .= '<tbody>';
for($i=0;$i<$data_size;$i++){
	$row_num = $i+2;
	$display_errors = isset($errorAry[$row_num]) && count($errorAry[$row_num])>0 ? '<span class="red">'. implode('<br />',$errorAry[$row_num]).'</span>' : '&nbsp;';
	$x .= '<tr>';
		$x .= '<td>'.$row_num.'</td>';
		$x .= '<td>'.($data[$i][0]).(isset($userloginToStudentAry[$data[$i][0]])? '<br />('.$userloginToStudentAry[$data[$i][0]]['StudentName'].')' : '').'</td>';
	for($j=1;$j<count($data[$i]);$j++){
		
		$x .= '<td>'.$data[$i][$j].'</td>';
	}
		$x .= '<td>'.$display_errors.'</td>';
	$x .= '</tr>';
}
$x .= '</tbody>';
$x .= '</table>';

if(count($errorAry)==0 && count($valueAry)>0){
	// insert to temp import table
	$sql = "INSERT INTO INTRANET_SCH_BUS_IMPORT_STUDENT_ROUTE_SETTINGS 
			(SessionID,ImportTime,StudentID,AM_RouteCollectionID,AM_BusStopsID,PM_RouteCollectionID,PM_BusStopsID,
			MON_ActivityID,MON_RouteCollectionID,MON_BusStopsID,
			TUE_ActivityID,TUE_RouteCollectionID,TUE_BusStopsID,
			WED_ActivityID,WED_RouteCollectionID,WED_BusStopsID,
			THU_ActivityID,THU_RouteCollectionID,THU_BusStopsID,
			FRI_ActivityID,FRI_RouteCollectionID,FRI_BusStopsID,
			DateStart,DateEnd) VALUES ";
	$chunks = array_chunk($valueAry,100);
	for($i=0;$i<count($chunks);$i++)
	{
		$final_sql = $sql. implode(",",$chunks[$i]);
		$indexVar['db']->db_db_query($final_sql);
		//debug_pr($sql);
	}
}


$CurrentPage = 'settings/students';

$indexVar['linterface']->LAYOUT_START($returnMsg);

//debug_pr($data);
//debug_pr($userloginToNameAry);
//debug_pr($routeNameToAry);
//debug_pr($collectionIdToAry);
//debug_pr($busStopNameToAry);
//debug_pr($activityNameToAry);


$PAGE_NAVIGATION = array();
$PAGE_NAVIGATION[] = array($Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'],'index.php?task=settings/students');
$PAGE_NAVIGATION[] = array($Lang['Btn']['Import'],'');
# step information
$STEPS_OBJ[] = array($Lang['StaffAttendance']['SelectCSVFile'], 0);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['CSVConfirmation'], 1);
$STEPS_OBJ[] = array($Lang['StaffAttendance']['ImportResult'], 0);
?>
<?=$indexVar['linterface']->INCLUDE_COMMON_CSS_JS()?>
<br />
<form name="form1" method="POST" action="index.php?task=settings/students/import_update" >
    <input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?php echo $AcademicYearID;?>">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?=$indexVar['linterface']->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?></td>
	</tr>
	<tr>
		<td>
			&nbsp;
		</td>
	</tr>
	<tr>
		<td><?=$indexVar['linterface']->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
</table>
<?=$x?>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" /></td>
    </tr>
</table>
<table width="96%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
	    <td align="center" colspan="2">
			<?=count($errorAry)>0?'':$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
			<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "javascript:window.location.href='index.php?task=settings/students/import&AcademicYearID=".$AcademicYearID."'","cancelbtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")?>
		</td>
	</tr>
</table>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>