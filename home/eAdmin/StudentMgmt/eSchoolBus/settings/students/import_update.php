<?php
// Editing by 
/*
 *  2019-08-09 Cameron
 *      - allow to import students for non-current academic year
 */
//$AcademicYearID = Get_Current_Academic_Year_ID();
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
$cur_sessionid = session_id();

$sql = "SELECT * FROM INTRANET_SCH_BUS_IMPORT_STUDENT_ROUTE_SETTINGS WHERE SessionID='$cur_sessionid'";
$records = $indexVar['db']->returnResultSet($sql);
$records_size = count($records);

//debug_pr($records);
$resultAry = array();
for($i=0;$i<$records_size;$i++){
	
	$data = array('AcademicYearID'=>$AcademicYearID);
	$keys = array_keys($records[$i]);
	foreach($keys as $key){
		if(strpos($key,'_')>0){
			$parts = explode("_",$key);
			$data[$parts[1]][$parts[0]] = $records[$i][$key];
		}else{
			$data[$key] = $records[$i][$key];
		}
	}
	//debug_pr($data);
	$resultAry['deleteUserRoute'.$records[$i]['StudentID']] = $indexVar['db']->permanentDeleteData('INTRANET_SCH_BUS_USER_ROUTE', 'UserID', $records[$i]['StudentID']," AND AcademicYearID='".$AcademicYearID."' ");
	$resultAry['saveUserRoute'.$records[$i]['StudentID']] = $indexVar['db']->saveUserRoutes($data);
}

$indexVar['libSchoolBus']->setTempSession('SettingsStudentsImport',!in_array(false,$resultAry)? $Lang['General']['ReturnMessage']['ImportSuccess'] : $Lang['General']['ReturnMessage']['ImportUnsuccess']);

intranet_closedb();
$indexVar['libSchoolBus']->redirect("index.php?task=settings/students/import&AcademicYearID=".$AcademicYearID);
?>