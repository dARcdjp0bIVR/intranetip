<?php
// Editing by 
/*
 *  2019-08-09 Cameron
 *      - pass $AcademicYearID to hyperlink of cancel button
 *
 *  2019-04-01 Cameron
 *      - fix: should also empty RouteCollectionID and BusStopID if no activity is chosen
 *      
 *  2019-03-29 Cameron
 *      - fix: retrieve DateStart and DateEnd from saved record rather than those of current academic year when edit
 *      
 *  2018-11-30 Cameron
 *      - check if total number of students that taken the route exceed the bus limit or not
 */
if($AcademicYearID == '' || $FormID == '')
{
	intranet_closedb();
	$indexVar['libSchoolBus']->redirect('index.php?task=settings/students');
	exit;
}

include_once($intranet_root.'/includes/form_class_manage.php');

$fcm = new form_class_manage();

$CurrentPage = 'settings/students/edit';

$academicYearObj = new academic_year($AcademicYearID);
$StartDate = date("Y-m-d", strtotime($academicYearObj->AcademicYearStart));
$EndDate =  date("Y-m-d", strtotime($academicYearObj->AcademicYearEnd));

$is_new = true;

if(isset($StudentID)){
	$is_new = false;
	// Edit
	if(is_array($StudentID)){
		$StudentID = $StudentID[0];
	}
	//$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserRouteID'=>$UserRouteID,'IsAssoc'=>1));
	//foreach($records as $student_id => $val){
	//	$StudentID = $student_id;
	//}
	$YearClassID = $fcm->Get_Student_Studying_Class($StudentID, $AcademicYearID);
	$student_list = $fcm->Get_Student_By_Class(array($YearClassID));
	$student_record = array();
	for($i=0;$i<count($student_list);$i++){
		if($student_list[$i]['UserID'] == $StudentID){
			$student_record = $student_list[$i];
		}
	}
	
	$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserID'=>$StudentID,'IsAssoc'=>1));
	$user_routes = isset($records[$StudentID])? $records[$StudentID] : array();
	
	$hidden_fields .= $indexVar['linterface']->GET_HIDDEN_INPUT('StudentID', 'StudentID', $StudentID);
	
	if (count($user_routes)) {
	    $currUserRouteInfo = current(current($user_routes));
	    $StartDate = $currUserRouteInfo['DateStart'];      // retrieve from saved record, overwrite defaut current academic year start date 
	    $EndDate = $currUserRouteInfo['DateEnd'];
	}
}else{
	// New
	$user_routes = array();
	
	$yearObj = new Year($FormID);
	$class_list = $yearObj->Get_All_Classes(0, $AcademicYearID);
	$YearClassID = $class_list[0]['YearClassID'];
}

$hidden_fields .= $indexVar['linterface']->GET_HIDDEN_INPUT('AcademicYearID', 'AcademicYearID', $AcademicYearID);
$hidden_fields .= $indexVar['linterface']->GET_HIDDEN_INPUT('FormID', 'FormID', $FormID);

$parentSelectionOption = array(array('0',$Lang['eSchoolBus']['Settings']['Student']['ByParent']));

$pages_arr = array(
	array($Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'],'index.php?task=settings/students'),
	array($StudentID > 0? $Lang['Btn']['Edit'] : $Lang['Btn']['New'], '')
);

$indexVar['linterface']->LAYOUT_START($returnMsg);
?>
<?=$indexVar['linterface']->INCLUDE_COMMON_CSS_JS()?>
<?=$indexVar['linterface']->GET_NAVIGATION_IP25($pages_arr)?>
<br />
<form name="form1" id="form1" action="index.php?task=settings/students/edit_update" method="post" onsubmit="return false;">
	<?=$hidden_fields?>
	<table width="100%" cellpadding="2" class="form_table_v30">
		<col class="field_title">
		<col class="field_c">
		<tr>
			<td class="field_title"><?=$Lang['General']['AcademicYear']?></td>
			<td><?=$academicYearObj->Get_Academic_Year_Name()?></td>
		</tr>
	<?php if(isset($StudentID)){ ?>
		<tr>
			<td class="field_title"><?=$Lang['SysMgr']['FormClassMapping']['Class']?></td>
			<td>
				<?=Get_Lang_Selection($student_record['ClassTitleB5'] , $student_record['ClassTitleEN']). ($student_record['ClassNumber']!=''?' ('.$student_record['ClassNumber'].')':'')?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['Identity']['Student']?></td>
			<td>
				<?=$student_record['StudentName']?>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("StudentID_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
	<?php }else{ ?>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['SysMgr']['FormClassMapping']['Class']?></td>
			<td>
				<?=$indexVar['linterface']->getClassSelection($AcademicYearID, ' id="YearClassID" name="YearClassID" onchange="getStudentSelection();" ', $YearClassID, $all=0, $noFirst=1, $FirstTitle="")?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Identity']['Student']?></td>
			<td>
				<span id="StudentSelectionContainer"></span>
				<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("StudentID_Error",$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'], "WarnMsgDiv")?>
			</td>
		</tr>
	<?php } ?>
		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Route']['NormalRoute']?></td>
			<td>
				<p><?=$Lang['eSchoolBus']['Settings']['Student']['AM']?>&nbsp;
				   <?=$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID,'N','AM', ' id="RouteCollectionID[AM]" name="RouteCollectionID[AM]" onchange="routeSelectionChanged(\'AM\')" ',$user_routes['N']['AM']['RouteCollectionID'],$__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --') ?>&nbsp;
				   <?=isset($user_routes['N']['AM']['UserRouteID'])? $indexVar['linterface']->GET_HIDDEN_INPUT('UserRouteID[AM]', 'UserRouteID[AM]', $user_routes['N']['AM']['UserRouteID']) : ''?>
				   <?=$Lang['eSchoolBus']['Settings']['Student']['BusStop']?>&nbsp;
				   <span id="AM_StopContainer"><?=$indexVar['linterface']->getBusStopsSelectionByRouteCollection($user_routes['N']['AM']['RouteCollectionID'],' id="BusStopsID[AM]" name="BusStopsID[AM]" ',$user_routes['N']['AM']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --')?></span>
				   <?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("AM_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'], "WarnMsgDiv")?>
				   <?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("AM_SeatError",$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'], "WarnMsgDiv")?>
				</p>
				<p><?=$Lang['eSchoolBus']['Settings']['Student']['PM']?>&nbsp;
				   <?=$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID,'N','PM', ' id="RouteCollectionID[PM]" name="RouteCollectionID[PM]" onchange="routeSelectionChanged(\'PM\')" ',$user_routes['N']['PM']['RouteCollectionID'],$__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --') ?>&nbsp;
				   <?=isset($user_routes['N']['PM']['UserRouteID'])? $indexVar['linterface']->GET_HIDDEN_INPUT('UserRouteID[PM]', 'UserRouteID[PM]', $user_routes['N']['PM']['UserRouteID']) : ''?>
				   <?=$Lang['eSchoolBus']['Settings']['Student']['BusStop']?>&nbsp;
				   <span id="PM_StopContainer"><?=$indexVar['linterface']->getBusStopsSelectionByRouteCollection($user_routes['N']['PM']['RouteCollectionID'],' id="BusStopsID[PM]" name="BusStopsID[PM]" ',$user_routes['N']['PM']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --')?></span>
				   <?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("PM_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'], "WarnMsgDiv")?>
				   <?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("PM_SeatError",$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'], "WarnMsgDiv")?>
				</p>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute']?></td>
			<td>
				<table>
					<thead>
						<tr>
							<td class="MON"><?=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['MON']?></td>
							<td class="TUE"><?=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['TUE']?></td>
							<td class="WED"><?=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['WED']?></td>
							<td class="THU"><?=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['THU']?></td>
							<td class="FRI"><?=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['FRI']?></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="MON">
								<?=isset($user_routes['S']['MON']['UserRouteID'])? $indexVar['linterface']->GET_HIDDEN_INPUT('UserRouteID[MON]', 'UserRouteID[MON]', $user_routes['S']['MON']['UserRouteID']) : ''?>
								<?=$indexVar['linterface']->getActivitySelection(' id="ActivityID[MON]" name="ActivityID[MON]" onchange="activitySelectionChanged(\'MON\');" ', $user_routes['S']['MON']['ActivityID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'].' --')?>
								<div id="MON_Container" <?=$user_routes['S']['MON']['ActivityID']==''? 'style="display:none;"' :'' ?>>
									<?=$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID,'S','MON',' id="RouteCollectionID[MON]" name="RouteCollectionID[MON]" onchange="routeSelectionChanged(\'MON\',1)" ',$user_routes['S']['MON']['RouteCollectionID'],$__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --',$parentSelectionOption) ?>
									<span id="MON_StopContainer" <?=in_array($user_routes['S']['MON']['RouteCollectionID'],array('0',''))?' style="display:none;"':''?>><?=$indexVar['linterface']->getBusStopsSelectionByRouteCollection($user_routes['S']['MON']['RouteCollectionID'],' id="BusStopsID[MON]" name="BusStopsID[MON]" ',$user_routes['S']['MON']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --')?></span>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("MON_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'], "WarnMsgDiv")?>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("MON_SeatError",$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'], "WarnMsgDiv")?>
								</div>
							</td>
							<td class="TUE">
								<?=isset($user_routes['S']['TUE']['UserRouteID'])? $indexVar['linterface']->GET_HIDDEN_INPUT('UserRouteID[TUE]', 'UserRouteID[TUE]', $user_routes['S']['TUE']['UserRouteID']) : ''?>
								<?=$indexVar['linterface']->getActivitySelection(' id="ActivityID[TUE]" name="ActivityID[TUE]" onchange="activitySelectionChanged(\'TUE\');" ', $user_routes['S']['TUE']['ActivityID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'].' --')?>
								<div id="TUE_Container" <?=$user_routes['S']['TUE']['ActivityID']==''? 'style="display:none;"' :'' ?>>
									<?=$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID,'S','TUE',' id="RouteCollectionID[TUE]" name="RouteCollectionID[TUE]" onchange="routeSelectionChanged(\'TUE\',1)" ',$user_routes['S']['TUE']['RouteCollectionID'],$__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --',$parentSelectionOption) ?>
									<span id="TUE_StopContainer" <?=in_array($user_routes['S']['TUE']['RouteCollectionID'],array('0',''))?' style="display:none;"':''?>><?=$indexVar['linterface']->getBusStopsSelectionByRouteCollection($user_routes['S']['TUE']['RouteCollectionID'],' id="BusStopsID[TUE]" name="BusStopsID[TUE]" ',$user_routes['S']['TUE']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --')?></span>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("TUE_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'], "WarnMsgDiv")?>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("TUE_SeatError",$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'], "WarnMsgDiv")?>
								</div>
							</td>
							<td class="WED">
								<?=isset($user_routes['S']['WED']['UserRouteID'])? $indexVar['linterface']->GET_HIDDEN_INPUT('UserRouteID[WED]', 'UserRouteID[WED]', $user_routes['S']['WED']['UserRouteID']) : ''?>
								<?=$indexVar['linterface']->getActivitySelection(' id="ActivityID[WED]" name="ActivityID[WED]" onchange="activitySelectionChanged(\'WED\');" ', $user_routes['S']['WED']['ActivityID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'].' --')?>
								<div id="WED_Container" <?=$user_routes['S']['WED']['ActivityID']==''? 'style="display:none;"' :'' ?>>
									<?=$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID,'S','WED',' id="RouteCollectionID[WED]" name="RouteCollectionID[WED]" onchange="routeSelectionChanged(\'WED\',1)" ',$user_routes['S']['WED']['RouteCollectionID'],$__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --',$parentSelectionOption) ?>
									<span id="WED_StopContainer" <?=in_array($user_routes['S']['WED']['RouteCollectionID'],array('0',''))?' style="display:none;"':''?>><?=$indexVar['linterface']->getBusStopsSelectionByRouteCollection($user_routes['S']['WED']['RouteCollectionID'],' id="BusStopsID[WED]" name="BusStopsID[WED]" ',$user_routes['S']['WED']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --')?></span>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("WED_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'], "WarnMsgDiv")?>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("WED_SeatError",$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'], "WarnMsgDiv")?>
								</div>
							</td>
							<td class="THU">
								<?=isset($user_routes['S']['THU']['UserRouteID'])? $indexVar['linterface']->GET_HIDDEN_INPUT('UserRouteID[THU]', 'UserRouteID[THU]', $user_routes['S']['THU']['UserRouteID']) : ''?>
								<?=$indexVar['linterface']->getActivitySelection(' id="ActivityID[THU]" name="ActivityID[THU]" onchange="activitySelectionChanged(\'THU\');" ', $user_routes['S']['THU']['ActivityID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'].' --')?>
								<div id="THU_Container" <?=$user_routes['S']['THU']['ActivityID']==''? 'style="display:none;"' :'' ?>>
									<?=$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID,'S','THU',' id="RouteCollectionID[THU]" name="RouteCollectionID[THU]" onchange="routeSelectionChanged(\'THU\',1)" ',$user_routes['S']['THU']['RouteCollectionID'],$__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --',$parentSelectionOption) ?>
									<span id="THU_StopContainer" <?=in_array($user_routes['S']['THU']['RouteCollectionID'],array('0',''))?' style="display:none;"':''?>><?=$indexVar['linterface']->getBusStopsSelectionByRouteCollection($user_routes['S']['THU']['RouteCollectionID'],' id="BusStopsID[THU]" name="BusStopsID[THU]" ',$user_routes['S']['THU']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --')?></span>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("THU_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'], "WarnMsgDiv")?>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("THU_SeatError",$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'], "WarnMsgDiv")?>
								</div>
							</td>
							<td class="FRI">
								<?=isset($user_routes['S']['FRI']['UserRouteID'])? $indexVar['linterface']->GET_HIDDEN_INPUT('UserRouteID[FRI]', 'UserRouteID[FRI]', $user_routes['S']['FRI']['UserRouteID']) : ''?>
								<?=$indexVar['linterface']->getActivitySelection(' id="ActivityID[FRI]" name="ActivityID[FRI]" onchange="activitySelectionChanged(\'FRI\');" ', $user_routes['S']['FRI']['ActivityID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'].' --')?>
								<div id="FRI_Container" <?=$user_routes['S']['FRI']['ActivityID']==''? 'style="display:none;"' :'' ?>>
									<?=$indexVar['linterface']->getRouteCollectionSelection($AcademicYearID,'S','FRI',' id="RouteCollectionID[FRI]" name="RouteCollectionID[FRI]" onchange="routeSelectionChanged(\'FRI\',1)" ',$user_routes['S']['FRI']['RouteCollectionID'],$__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'].' --',$parentSelectionOption) ?>
									<span id="FRI_StopContainer" <?=in_array($user_routes['S']['FRI']['RouteCollectionID'],array('0',''))?' style="display:none;"':''?>><?=$indexVar['linterface']->getBusStopsSelectionByRouteCollection($user_routes['S']['FRI']['RouteCollectionID'],' id="BusStopsID[FRI]" name="BusStopsID[FRI]" ',$user_routes['S']['FRI']['BusStopsID'], $__all=0, $__noFirst=0, $__FirstTitle='-- '.$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'].' --')?></span>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("FRI_Error",$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'], "WarnMsgDiv")?>
									<?=$indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("FRI_SeatError",$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'], "WarnMsgDiv")?>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eSchoolBus']['Settings']['Route']['Dates']?></td>
			<td>
			<?php
				echo $Lang['eSchoolBus']['Settings']['Route']['StartDate'].'&nbsp;';
				echo $indexVar['linterface']->GET_DATE_PICKER('DateStart',$StartDate).'&nbsp;';
				echo $Lang['General']['To'].'&nbsp;';
				echo $Lang['eSchoolBus']['Settings']['Route']['EndDates'].'&nbsp;';
				echo $indexVar['linterface']->GET_DATE_PICKER('DateEnd',$EndDate).'&nbsp;';
				echo $indexVar['linterface']->Get_Thickbox_Warning_Msg_Div("Date_Error",$Lang['General']['JS_warning']['InvalidDateRange'], "WarnMsgDiv");
			?>
			</td>
		</tr>
</table>
<?=$indexVar['linterface']->MandatoryField()?>		
<div class="edit_bottom_v30">
<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submitBtn').'&nbsp;'?>
<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Cancel'],"button","window.location.href='index.php?task=settings/students&AcademicYearID=".$AcademicYearID."'",'cancelBtn')?>
</div>
</form>
<script type="text/javascript" language="javascript">
var loading_img = '<?=$indexVar['linterface']->Get_Ajax_Loading_Image()?>';

function getStudentSelection()
{
	$('#StudentSelectionContainer').html(loading_img);
	$.post(
		'index.php?task=settings/students/ajax',
		{
			'ajax_task': 'getStudentSelection',
			'YearClassID':$('#YearClassID').val(),
			'AcademicYearID': $('#AcademicYearID').val() 
		},function(returnHtml){
			$('#StudentSelectionContainer').html(returnHtml);
		}
	);
}

function routeSelectionChanged(typeName)
{
	var route_collection_id = $('#RouteCollectionID\\['+typeName+'\\]').val();
	if(route_collection_id == '0' || route_collection_id==''){
		$('#'+typeName+'_StopContainer').hide();
	}else{
		$('#'+typeName+'_StopContainer').show();
	}
	
	$.post(
		'index.php?task=settings/students/ajax',
		{
			'ajax_task': 'getBusStopsSelection',
			'RouteCollectionID': route_collection_id,
			'Name': 'BusStopsID['+typeName+']',
			'Id': 'BusStopsID['+typeName+']' 
		},function(returnHtml){
			$('#'+typeName+'_StopContainer').html(returnHtml);
		}
	);
}

function activitySelectionChanged(typeName)
{
	var selected_activity_id = $('#ActivityID\\['+typeName+'\\]').val();
	
	if(selected_activity_id == ''){
		$('#'+typeName+'_Container').hide();
		$('#RouteCollectionID\\['+typeName+'\\]').val('');
		$('#BusStopsID\\['+typeName+'\\]').val('');
	}else{
		$('#'+typeName+'_Container').show();
	}
}

function checkSubmitForm(formObj)
{
	$('input[type=button]').attr('disabled',true);
	var valid = true;
	
	$('.WarnMsgDiv').hide();
	
	var student_id = $('#StudentID').val();
	var am_pm_ary = ['AM','PM'];
	var weekday_ary = ['MON','TUE','WED','THU','FRI'];
	var date_start = $.trim($('#DateStart').val());
	var date_end = $.trim($('#DateEnd').val());
	
	if(student_id == ''){
		$('#StudentID_Error').html('<?=$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData']?>').show();
		valid = false;
	}
	
	for(var i=0;i<am_pm_ary.length;i++){
		if($('#RouteCollectionID\\['+am_pm_ary[i]+'\\]').val()!='' && $('#BusStopsID\\['+am_pm_ary[i]+'\\]').val()==''){
			$('#'+am_pm_ary[i]+'_Error').show();
			valid = false;
		}
	}
	for(var i=0;i<weekday_ary.length;i++){
		if($('#ActivityID\\['+weekday_ary[i]+'\\]').val()!='' && $('#RouteCollectionID\\['+weekday_ary[i]+'\\]').val()!='0' && $('#BusStopsID\\['+weekday_ary[i]+'\\]').val()==''){
			$('#'+weekday_ary[i]+'_Error').show();
			valid = false;
		}
	}
	
	if($('#DPWL-DateStart').html() != ''){
		valid = false;
	}
	if($('#DPWL-DateEnd').html() != ''){
		valid = false;
	}
	if($('#DPWL-DateStart').html() == '' && $('#DPWL-DateEnd').html() == '' && date_start > date_end){
		$('#Date_Error').show();
		valid = false;
	}
	
	if(valid){
<?php if($is_new){ ?>		
		$.post(
			'index.php?task=settings/students/ajax',
			{
				'ajax_task': 'countUserRouteRecords',
				'AcademicYearID': $('#AcademicYearID').val(),
				'StudentID': student_id,
				'DateStart': $('#DateStart').val(),
				'DateEnd': $('#DateEnd').val(),
				'RouteCollectionID[AM]': $('#RouteCollectionID\\[AM\\]').val(),
				'RouteCollectionID[PM]': $('#RouteCollectionID\\[PM\\]').val(),
				'RouteCollectionID[MON]': $('#RouteCollectionID\\[MON\\]').val(),
				'RouteCollectionID[TUE]': $('#RouteCollectionID\\[TUE\\]').val(),
				'RouteCollectionID[WED]': $('#RouteCollectionID\\[WED\\]').val(),
				'RouteCollectionID[THU]': $('#RouteCollectionID\\[THU\\]').val(),
				'RouteCollectionID[FRI]': $('#RouteCollectionID\\[FRI\\]').val()				 
			},
			function(returnStr)
			{
				var ret = returnStr.split('^~^')
				var count = parseInt(ret[0]);
				if(count > 0){
					$('#StudentID_Error').html('<?=$Lang['eSchoolBus']['Settings']['Student']['Warning']['StudentHasBeenSet']?>').show();
					$('input[type=button]').attr('disabled',false);
				}
				else{
					var pass = true;
					for (var i=1; i<ret.length; i++) {
						var seatInfo = ret[i];
						var seatInfoAry = seatInfo.split('^#^');
						var timeSlot = seatInfoAry[0];
						var nrReserved = parseInt(seatInfoAry[1]);
						var nrSeat = parseInt(seatInfoAry[2]);
						if (nrReserved >= nrSeat) {
							$('#'+ timeSlot + '_SeatError').html('<?=$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat']?>' + nrSeat).show();
							$('input[type=button]').attr('disabled',false);
							pass = false;
						}
					}

					if (pass == true) {
						formObj.submit();
					}
				}
			}
		);
<?php }else{ ?>
    	$.post(
    		'index.php?task=settings/students/ajax',
    		{
    			'ajax_task': 'countUserRouteRecords',
    			'AcademicYearID': $('#AcademicYearID').val(),
    			'StudentID': student_id,
    			'DateStart': $('#DateStart').val(),
    			'DateEnd': $('#DateEnd').val(),
    			'RouteCollectionID[AM]': $('#RouteCollectionID\\[AM\\]').val(),
    			'RouteCollectionID[PM]': $('#RouteCollectionID\\[PM\\]').val(),
    			'RouteCollectionID[MON]': $('#RouteCollectionID\\[MON\\]').val(),
    			'RouteCollectionID[TUE]': $('#RouteCollectionID\\[TUE\\]').val(),
    			'RouteCollectionID[WED]': $('#RouteCollectionID\\[WED\\]').val(),
    			'RouteCollectionID[THU]': $('#RouteCollectionID\\[THU\\]').val(),
    			'RouteCollectionID[FRI]': $('#RouteCollectionID\\[FRI\\]').val()				 
    		},
    		function(returnStr)
    		{
    			var ret = returnStr.split('^~^')
    			var pass = true;
    			for (var i=1; i<ret.length; i++) {
    				var seatInfo = ret[i];
    				var seatInfoAry = seatInfo.split('^#^');
    				var timeSlot = seatInfoAry[0];
    				var nrReserved = parseInt(seatInfoAry[1]);
    				var nrSeat = parseInt(seatInfoAry[2]);
    				var currRouteCollectionID = parseInt(seatInfoAry[3]);
    				if (((currRouteCollectionID != $('#RouteCollectionID\\['+timeSlot+'\\]').val()) && (nrReserved >= nrSeat)) || ((currRouteCollectionID == $('#RouteCollectionID\\['+timeSlot+'\\]').val()) && (nrReserved > nrSeat))) {
    					$('#'+ timeSlot + '_SeatError').html('<?=$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat']?>' + nrSeat).show();
    					$('input[type=button]').attr('disabled',false);
    					pass = false;
    				}
    			}
    
    			if (pass == true) {
    				formObj.submit();
    			}
    		}
    	);
<?php } ?>
	}else{
		$('input[type=button]').attr('disabled',false);
	}
}

$(document).ready(function(){
	getStudentSelection();
});
</script>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>