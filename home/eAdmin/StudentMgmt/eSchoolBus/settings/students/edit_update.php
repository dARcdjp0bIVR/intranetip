<?php
// Editing by 

$success = $indexVar['db']->saveUserRoutes($_POST);

$indexVar['libSchoolBus']->setTempSession('SettingsStudents',$success? $Lang['General']['ReturnMessage']['UpdateSuccess'] : $Lang['General']['ReturnMessage']['UpdateUnsuccess']);

$indexVar['libSchoolBus']->setTempSession('SettingsStudents_AcademicYearID', $_POST['AcademicYearID']);
$indexVar['libSchoolBus']->setTempSession('SettingsStudents_FormID', $_POST['FormID']);

intranet_closedb();
$indexVar['libSchoolBus']->redirect("index.php?task=settings/students");
?>