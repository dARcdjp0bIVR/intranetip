<?php
// Editing by 
/*
 * 2019-03-07 Cameron
 * - hide BIBA report title if $sys_custom['eSchoolBus']['report']['showBIBATitle'] is not set ( for general ) [case #A156820]
 * 
 * 2019-01-29 Cameron
 * - modify to support multiple teachers for the same route [case #Y154206]
 */
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');

if($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($AcademicYearID) || !isset($VehicleID)){
	intranet_closedb();
	exit;
}

include_once($intranet_root."/includes/libfilesystem.php");

$lf = new libfilesystem();

$teacher_name_field = getNameFieldByLang2('u.');
$class_name_field = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$bus_stop_name_field = Get_Lang_Selection('bs.BusStopsNameChi','bs.BusStopsName');

$normal_records = array();
$asa_records = array();

for($i=0;$i<count($VehicleID);$i++){
	
	// find regular routes
	$sql = "SELECT 
				v.VehicleID,
				v.CarNum,
				v.DriverName,
				v.DriverTel,
				v.CarPlateNum,
				v.Seat,
				r.RouteID,
				r.RouteName,
				/*$teacher_name_field as TeacherName,
				u.MobileTelNo as TeacherTel,*/
                t.Teacher as TeacherName,
                t.TeacherTel,
				GROUP_CONCAT(rc.RouteCollectionID) as RouteCollectionID, 
				rc.AmPm 
			FROM INTRANET_SCH_BUS_VEHICLE as v
			INNER JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as vm ON vm.VehicleID=v.VehicleID 
			INNER JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=vm.RouteID AND r.IsDeleted='0' 
			INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteID=r.RouteID AND rc.IsDeleted='0'
            INNER JOIN (SELECT 
                                GROUP_CONCAT($teacher_name_field) AS Teacher, 
                                GROUP_CONCAT(u.MobileTelNo) AS TeacherTel,
                                t.RouteID 
                        FROM 
                                INTRANET_SCH_BUS_ROUTE_TEACHER t
                        INNER JOIN INTRANET_USER u ON u.UserID=t.TeacherID
                                GROUP BY t.RouteID
                    ) AS t ON (t.RouteID = r.RouteID)
			/*INNER JOIN INTRANET_USER as u ON u.UserID=r.TeacherID*/ 
			WHERE v.VehicleID='".$VehicleID[$i]."' AND v.RecordStatus='1' AND r.AcademicYearID='$AcademicYearID' AND r.IsActive='1' AND r.Type='N' AND r.IsDeleted='0' 
			GROUP BY r.RouteID 
			ORDER BY r.RouteName,rc.RouteCollectionID ";
	
	$normal_routes = $indexVar['db']->returnResultSet($sql);
	$normal_routes_size = count($normal_routes);
	
	for($j=0;$j<$normal_routes_size;$j++){
		$record = array();
		$record['RouteInfo'] = $normal_routes[$j];
		
		$sql = "SELECT 
					DISTINCT 
					ur.UserID,
					$class_name_field as ClassName,
					ycu.ClassNumber,
					IF(au.UserID IS NOT NULL,CONCAT(au.EnglishName,' / ',au.ChineseName),CONCAT(u.EnglishName,' / ',u.ChineseName)) as StudentName 
				FROM INTRANET_SCH_BUS_USER_ROUTE as ur 
				INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=ur.UserID 
				INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID' 
				INNER JOIN YEAR as y ON y.YearID=yc.YearID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=ur.UserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=ur.UserID 
				INNER JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=ur.BusStopsID AND bs.IsDeleted='0' 
				WHERE ur.AcademicYearID='$AcademicYearID' AND ur.RouteCollectionID IN (".$normal_routes[$j]['RouteCollectionID'].") 
				ORDER BY bs.BusStopsID";
		$students = $indexVar['db']->returnResultSet($sql);
		//debug_pr($students);

		$record['Students'] = $students;
		
		$normal_records[] = $record;
	}
	
	
	// find ASA routes
	$sql = "SELECT 
				v.VehicleID,
				v.CarNum,
				v.DriverName,
				v.DriverTel,
				v.CarPlateNum,
				v.Seat,
				r.RouteID,
				r.RouteName,
				/*$teacher_name_field as TeacherName,
				u.MobileTelNo as TeacherTel,*/
                t.Teacher as TeacherName,
                t.TeacherTel,
				rc.RouteCollectionID, 
				rc.Weekday 
			FROM INTRANET_SCH_BUS_VEHICLE as v
			INNER JOIN INTRANET_SCH_BUS_ROUTE_VEHICLE_MAP as vm ON vm.VehicleID=v.VehicleID 
			INNER JOIN INTRANET_SCH_BUS_ROUTE as r ON r.RouteID=vm.RouteID AND r.IsDeleted='0' 
			INNER JOIN INTRANET_SCH_BUS_ROUTE_COLLECTION as rc ON rc.RouteID=r.RouteID AND rc.IsDeleted='0'
			/*INNER JOIN INTRANET_USER as u ON u.UserID=r.TeacherID*/
            INNER JOIN (SELECT 
                                GROUP_CONCAT($teacher_name_field) AS Teacher, 
                                GROUP_CONCAT(u.MobileTelNo) AS TeacherTel,
                                t.RouteID 
                        FROM 
                                INTRANET_SCH_BUS_ROUTE_TEACHER t
                        INNER JOIN INTRANET_USER u ON u.UserID=t.TeacherID
                                GROUP BY t.RouteID
                    ) AS t ON (t.RouteID = r.RouteID)
			WHERE v.VehicleID='".$VehicleID[$i]."' AND v.RecordStatus='1' AND r.AcademicYearID='$AcademicYearID' AND r.IsActive='1' AND r.Type='S' AND r.IsDeleted='0' 
			ORDER BY r.RouteName,rc.RouteCollectionID ";
	
	$asa_routes = $indexVar['db']->returnResultSet($sql);
	$asa_routes_size = count($asa_routes);
	
	for($j=0;$j<$asa_routes_size;$j++){
		$record = array();
		$record['RouteInfo'] = $asa_routes[$j];
		
		$sql = "SELECT 
					ur.UserID,
					$class_name_field as ClassName,
					ycu.ClassNumber,
					IF(au.UserID IS NOT NULL,CONCAT(au.EnglishName,' / ',au.ChineseName),CONCAT(u.EnglishName,' / ',u.ChineseName)) as StudentName,
					bs.BusStopsID,
					$bus_stop_name_field as BusStopName 
				FROM INTRANET_SCH_BUS_USER_ROUTE as ur 
				INNER JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=ur.UserID 
				INNER JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID='$AcademicYearID' 
				INNER JOIN YEAR as y ON y.YearID=yc.YearID 
				LEFT JOIN INTRANET_USER as u ON u.UserID=ur.UserID 
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON au.UserID=ur.UserID 
				INNER JOIN INTRANET_SCH_BUS_BUS_STOPS as bs ON bs.BusStopsID=ur.BusStopsID AND bs.IsDeleted='0' 
				WHERE ur.AcademicYearID='$AcademicYearID' AND ur.RouteCollectionID='".$asa_routes[$j]['RouteCollectionID']."'
				ORDER BY bs.BusStopsID";
		$students = $indexVar['db']->returnResultSet($sql);
		//debug_pr($students);
		$record['Students'] = $students;
		$asa_records[] = $record;
	}
	
	
}


//debug_pr($normal_records);
//debug_pr($asa_records);

if ($sys_custom['eSchoolBus']['report']['showBIBATitle']) {
    $reportTitle = $Lang['eSchoolBus']['Report']['Pickup']['SchoolBusList'];
}
else {
    $reportTitle = $Lang['eSchoolBus']['Report']['Pickup']['GeneralSchoolBusList'];
}

$normal_records_size = count($normal_records);
$asa_records_size = count($asa_records);

if($Format == 'EXCEL'){
	
	include_once($intranet_root."/includes/phpxls/Classes/PHPExcel/IOFactory.php");
	include_once($intranet_root."/includes/phpxls/Classes/PHPExcel.php");
	
	$objPHPExcel = new PHPExcel();
	// Set properties
	$objPHPExcel->getProperties()->setCreator("eClass")
	->setLastModifiedBy("eClass")
	->setTitle("eClass eSchoolBus - School Bus Student Pickup List")
	->setSubject("eClass eSchoolBus - School Bus Student Pickup List")
	->setDescription("eClass eSchoolBus - School Bus Student Pickup List")
	->setKeywords("eClass eSchoolBus - School Bus Student Pickup List")
	->setCategory("eClass");

	$sheet_index = 0;
	$cur_car_num = '';
	
	$row = 0;

	for($i=0;$i<$normal_records_size;$i++){
		
		$route = $normal_records[$i]['RouteInfo'];
		$students = $normal_records[$i]['Students'];
		
		$students_size = count($students);
		
		$car_num = $route['CarNum'];
		
		if($cur_car_num != $car_num)
		{
			if($sheet_index > 0)
			{
				$objPHPExcel->createSheet();
			}
			$objPHPExcel->setActiveSheetIndex($sheet_index);
			$ActiveSheet[$car_num] = $objPHPExcel->getActiveSheet();
			$ActiveSheet[$car_num]->setTitle($car_num);
			
			$row = 0;
		}
		

		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':H'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$reportTitle);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':H'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['BusNumber'].': '.$route['CarNum'].'    '.$Lang['eSchoolBus']['Report']['Pickup']['Route'].': '.$route['RouteName'].'     '.$Lang['eSchoolBus']['Report']['Pickup']['BusDriver'].': '.$route['DriverName'].'    '.$Lang['eSchoolBus']['Report']['Pickup']['BusTeacher'].': '.$route['TeacherName']);
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':H'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Date'].':         '.$Lang['eSchoolBus']['Report']['Pickup']['Year'].'(Y)     '.$Lang['eSchoolBus']['Report']['Pickup']['Month'].'(M)     '.$Lang['eSchoolBus']['Report']['Pickup']['Day'].'(D)');
		
		$row += 1;
		$table_start_row = $row;
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['ch']);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('B'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Name']['ch']);
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('C'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Class']['ch']);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('D'.$row,$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['ch']);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['ch']);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('F'.$row,$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['ch']);
		$ActiveSheet[$car_num]->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('G'.$row,$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['ch']);
		$ActiveSheet[$car_num]->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('H'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Remark']['ch']);
		$ActiveSheet[$car_num]->getStyle('H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$row += 1;
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['en']);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('B'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Name']['en']);
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('C'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Class']['en']);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('D'.$row,$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['en']);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['en']);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('F'.$row,$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['en']);
		$ActiveSheet[$car_num]->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('G'.$row,$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['en']);
		$ActiveSheet[$car_num]->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('H'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Remark']['en']);
		$ActiveSheet[$car_num]->getStyle('H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		for($j=0;$j<$students_size;$j++){
			$row += 1;
			$ActiveSheet[$car_num]->setCellValue('A'.$row,$j+1);
			$ActiveSheet[$car_num]->setCellValue('B'.$row,$students[$j]['StudentName']);
			$ActiveSheet[$car_num]->setCellValue('C'.$row,$students[$j]['ClassName']);
			$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		
		$row += 3;
		$table_end_row = $row;
		
		$row += 1;
		
		$ActiveSheet[$car_num]->getStyle('A'.$table_start_row.':H'.$table_end_row)->applyFromArray(
		    array(
		        'borders' => array(
		            'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                'color' => array('rgb' => '000000')
		            )
		        )
		    )
		);
		
		if($cur_car_num != $car_num)
		{
			$sheet_index += 1;
		}
		
		$cur_car_num = $route['CarNum'];
	}

	
	for($i=0;$i<$asa_records_size;$i++){
		
		$route = $asa_records[$i]['RouteInfo'];
		$students = $asa_records[$i]['Students'];
		
		$students_size = count($students);
		
		$car_num = $route['CarNum'];
		
		if($cur_car_num != $car_num)
		{
			if($sheet_index > 0)
			{
				$objPHPExcel->createSheet();
			}
			$objPHPExcel->setActiveSheetIndex($sheet_index);
			$ActiveSheet[$car_num] = $objPHPExcel->getActiveSheet();
			$ActiveSheet[$car_num]->setTitle($car_num);
			
			$row = 0;
		}
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':E'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$reportTitle);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getFont()->setBold(true);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':E'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['BusNumber'].': '.$route['CarNum'].'    '.$Lang['eSchoolBus']['Report']['Pickup']['Route'].': '.$route['RouteName'].'    '.$Lang['eSchoolBus']['Report']['Pickup']['BusTeacher'].': '.$route['TeacherName']);
		
		$row += 1;
		$ActiveSheet[$car_num]->mergeCells('A'.$row.':E'.$row);
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Date'].':         '.$Lang['eSchoolBus']['Report']['Pickup']['Year'].'(Y)     '.$Lang['eSchoolBus']['Report']['Pickup']['Month'].'(M)     '.$Lang['eSchoolBus']['Report']['Pickup']['Day'].'(D)     '.$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday'][$route['Weekday']]);
		
		$row += 1;
		$table_start_row = $row;
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['ch']);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('B'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Name']['ch']);
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('C'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Class']['ch']);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('D'.$row,$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['ch']);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['ch']);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$row += 1;
		$ActiveSheet[$car_num]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['en']);
		$ActiveSheet[$car_num]->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('B'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Name']['en']);
		$ActiveSheet[$car_num]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('C'.$row,$Lang['eSchoolBus']['Report']['Pickup']['Class']['en']);
		$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('D'.$row,$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['en']);
		$ActiveSheet[$car_num]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$ActiveSheet[$car_num]->setCellValue('E'.$row,$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['en']);
		$ActiveSheet[$car_num]->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		for($j=0;$j<$students_size;$j++){
			$row += 1;
			$ActiveSheet[$car_num]->setCellValue('A'.$row,$j+1);
			$ActiveSheet[$car_num]->setCellValue('B'.$row,$students[$j]['StudentName']);
			$ActiveSheet[$car_num]->setCellValue('C'.$row,$students[$j]['ClassName']);
			$ActiveSheet[$car_num]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		}
		
		$row += 3;
		$table_end_row = $row;
		
		$row += 1;
		
		$ActiveSheet[$car_num]->getStyle('A'.$table_start_row.':E'.$table_end_row)->applyFromArray(
		    array(
		        'borders' => array(
		            'allborders' => array(
		                'style' => PHPExcel_Style_Border::BORDER_THIN,
		                'color' => array('rgb' => '000000')
		            )
		        )
		    )
		);
		
		if($cur_car_num != $car_num)
		{
			$sheet_index += 1;
		}
		
		$cur_car_num = $route['CarNum'];
	}

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	$tmp_folder = $intranet_root.'/file/eSchoolBus';
	if(!is_dir($tmp_folder)){
		$lf->folder_new($tmp_folder);
	}
	$tmp_file_name = 'tmp'.md5(uniqid()).'.xls';
	$tmp_file = $tmp_folder.'/'.$tmp_file_name;
	
	file_put_contents($tmp_file,'');
	chmod($tmp_file, 0777);
	
	$objWriter->save($tmp_file);
	
	$output_filename = 'school_bus_pickup_list.xls';
	$content = get_file_content($tmp_file);
	$lf->file_remove($tmp_file);
	intranet_closedb();
	output2browser($content, $output_filename);
}
?>