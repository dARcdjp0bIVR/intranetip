<?php
// Editing by 

$AcademicYearID = Get_Current_Academic_Year_ID();

$indexVar['linterface']->LAYOUT_START($returnMsg);
?>
<?=$indexVar['linterface']->INCLUDE_COMMON_CSS_JS()?>
<br />
<form name="form1" id="form1" action="index.php?task=report/pickup/report" method="post" onsubmit="return false;">
	<table width="100%" cellpadding="2" class="form_table_v30">
		<col class="field_title">
		<col class="field_c">
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['General']['AcademicYear']?></td>
			<td>
				<?=$indexVar['linterface']->getAcademicYearSelection(' id="AcademicYearID" name="AcademicYearID" ', $__selected=$AcademicYearID, $__all=0, $__noFirst=1, $__FirstTitle="")?>
				<?=$indexVar['linterface']->Get_Form_Warning_Msg('AcademicYearID_Error', $Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear'], 'Error')?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Bus']?></td>
			<td>
				<span id="BusSelection"><?=$indexVar['linterface']->getVehicleSelection(' id="VehicleID" name="VehicleID[]" multiple="multiple" size="10" ', $__selected="", $__all=0, $__noFirst=1, $__FirstTitle="")?></span>
				<?=$indexVar['linterface']->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('VehicleID')){Select_All_Options('VehicleID', true);}return false;","selectAllTargetBtn",'')?>
				<?=$indexVar['linterface']->Get_Form_Warning_Msg('VehicleID_Error', $Lang['eSchoolBus']['Report']['ASABusStudentContactList']['RequestSelectBus'], 'Error')?>
				<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
			</td>
		</tr>
		<tr style="display:none;">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['General']['ViewFormat']?></td>
			<td>
				<select id="Format" name="Format">
					<option value="HTML">HTML</option>
					<option value="EXCEL" selected="selected">Excel</option>
				</select>
			</td>
		</tr>
	</table>
<?=$indexVar['linterface']->MandatoryField()?>
<div class="edit_bottom_v30">
<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","submitForm();",'submitBtn').'&nbsp;'?>
</div>
</form>
<div id="ReportContainer">
</div>
<script type="text/javascript" language="javascript">
var loading_img = '<?=$indexVar['linterface']->Get_Ajax_Loading_Image()?>';

function submitForm()
{
	$('#submitBtn').attr('disabled',true);
	var valid = true;
	var format = $('#Format').val();
	
	$('.Error').hide();
	if($('#VehicleID option:selected').length == 0){
		valid = false;
		$('#VehicleID_Error').show();
	}
	
	if(!valid){
		$('#submitBtn').attr('disabled',false);
		return;
	}
	
	if(format == 'HTML')
	{
		var url = '';
		var winType = '10';
		var win_name = 'intranet_popup'+winType;
		
		var oldAction = document.form1.action;
		var oldTarget = document.form1.target;
		
		document.form1.target = win_name;
		
		newWindow(url, winType);
		document.form1.submit();
		
		document.form1.action = oldAction;
		document.form1.target = oldTarget;
		$('#submitBtn').attr('disabled',false);
	}else if(format == 'EXCEL'){
		
		var oldAction = document.form1.action;
		var oldTarget = document.form1.target;
		
		document.form1.target = '_self';
		document.form1.submit();
		
		document.form1.action = oldAction;
		document.form1.target = oldTarget;
		$('#submitBtn').attr('disabled',false);
	}
}
</script>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>