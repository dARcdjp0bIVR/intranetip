<?php
// Editing by 
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');

if($_SERVER['REQUEST_METHOD'] != 'POST' || !isset($AcademicYearID) || !isset($YearID)){
	intranet_closedb();
	exit;
}

include_once($intranet_root."/includes/libfilesystem.php");
include_once($intranet_root.'/includes/form_class_manage.php');

$am_pm_ary = array('AM','PM');
$weekday_ary = array('MON','TUE', 'WED','THU','FRI');

$lf = new libfilesystem();
$fcm = new form_class_manage();

$classAry = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID, $YearID, $__TeachingOnly=0, $__YearClassIDArr='');

$students = $fcm->Get_Student_By_Form($AcademicYearID, $YearID);
$studentIdAry = Get_Array_By_Key($students, 'UserID');

//$student_details = $fcm->Get_Active_Student_List($YearID,$__YearClassID='',$AcademicYearID);
$student_details = $fcm->Get_Student_Class_Info_In_AcademicYear($studentIdAry, $AcademicYearID);

$records = $indexVar['db']->getUserRouteRecords(array('AcademicYearID'=>$AcademicYearID,'UserID'=>$studentIdAry,'IsAssoc'=>1));

$studentIdToAry = array();
for($i=0;$i<count($student_details);$i++){
	$studentIdToAry[$student_details[$i]['UserID']] = $student_details[$i];
}

$classIdToStudentIdAry = array();
for($i=0;$i<count($students);$i++){
	if(!isset($classIdToStudentIdAry[$students[$i]['YearClassID']])){
		$classIdToStudentIdAry[$students[$i]['YearClassID']] = array();
	}
	if(isset($records[$students[$i]['UserID']])){
		$classIdToStudentIdAry[$students[$i]['YearClassID']][] = $students[$i]['UserID'];
	}
}

$formToClassAry = array();
$yearClassIdToClassAry = array();
for($i=0;$i<count($classAry);$i++){
	$yearClassIdToClassAry[$classAry[$i]['YearClassID']] = $classAry[$i];
	if(!isset($formToClassAry[$classAry[$i]['YearID']])){
		$formToClassAry[$classAry[$i]['YearID']] = array();
		$formToClassAry[$classAry[$i]['YearID']]['YearID'] = $classAry[$i]['YearID'];
		$formToClassAry[$classAry[$i]['YearID']]['YearName'] = $classAry[$i]['YearName'];
		$formToClassAry[$classAry[$i]['YearID']]['YearClassID'] = array();
	}
	$formToClassAry[$classAry[$i]['YearID']]['YearClassID'][] = $classAry[$i]['YearClassID'];
}

foreach($formToClassAry as $year_id => $year_ary)
{
	for($i=0;$i<count($year_ary['YearClassID']);$i++){
		if(isset($classIdToStudentIdAry[$year_ary['YearClassID'][$i]]) && count($classIdToStudentIdAry[$year_ary['YearClassID'][$i]])>0){
			for($j=0;$j<count($classIdToStudentIdAry[$year_ary['YearClassID'][$i]]);$j++){
				$student_id = $classIdToStudentIdAry[$year_ary['YearClassID'][$i]][$j];
				if(isset($records[$student_id])){
					if(!isset($formToClassAry[$year_id]['StudentID']))
					{
						$formToClassAry[$year_id]['StudentID'] = array();
					}
					$formToClassAry[$year_id]['StudentID'][] = $student_id;
				}
			}
		}
	}
}
/*
if(in_array($Format,array('HTML','PRINT')))
{	

	$x .= '<table width="98%" align="center" class="print_hide" border="0">
				<tr>
					<td align="right">'.$indexVar['linterface']->GET_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","printBtn").'</td>
				</tr>
			</table>';
	$x .= '<div></div>';
	
	$is_print = $Format == "PRINT";
	$header_class = $is_print ? ' class="eSporttdborder eSportprinttabletitle"' :'';
	$header_row_class = $is_print ? ' class="tabletop"' : '';
	$td_class = $is_print ? ' class="eSporttdborder eSportprinttext"' :'';

	foreach($formToClassAry as $year_id => $year_ary)
	{
		$y = '';
		$y.= '<table '.($is_print?'align="center" class="eSporttableborder" border="0" cellpadding="2" cellspacing="0" width="98%"':'class="common_table_list_v30" width="98%" ').'>'."\n";
			$y.='<thead>';
				$y.= '<tr'.$header_row_class.'>';
					$y.= '<th '.$header_class.' rowspan="2" width="1%">&nbsp;</th>';
					$y.='<th '.$header_class.' rowspan="2" style="text-align:center;" width="11%">'.$year_ary['YearName'].'</th>';
					$y.='<th '.$header_class.' colspan="2" style="text-align:center;" width="22%">'.$Lang['eSchoolBus']['Report']['ClassStudentList']['RegularDays'].'</th>';
				foreach($weekday_ary as $weekday){
					$y .= '<th '.$header_class.' rowspan="2" style="text-align:center;" width="11%">'.$Lang['eSchoolBus']['Settings']['Route']['Weekday'][$weekday].'</th>';
				}
					$y .= '<th '.$header_class.' rowspan="2" style="text-align:center;" width="11%">'.$Lang['eSchoolBus']['Management']['Attendance']['Remarks'].'</th>';	
				$y .= '</tr>';
				$y .= '<tr'.$header_row_class.'>';
				foreach($am_pm_ary as $day)
				{
					$y .= '<th '.$header_class.' style="text-align:center;" width="11%">'.$Lang['eSchoolBus']['Settings']['Student'][$day].'</th>';
				}
				$y.= '</tr>';
				
			$y.='</thead>';
			$y.='<tbody>';
		$valid_student_count = 0;
		$row_num = 0;
		for($i=0;$i<count($year_ary['YearClassID']);$i++){
			if(isset($classIdToStudentIdAry[$year_ary['YearClassID'][$i]]) && count($classIdToStudentIdAry[$year_ary['YearClassID'][$i]])>0){
				for($j=0;$j<count($classIdToStudentIdAry[$year_ary['YearClassID'][$i]]);$j++){
					$student_id = $classIdToStudentIdAry[$year_ary['YearClassID'][$i]][$j];
					if(isset($records[$student_id])){
						$user_routes = $records[$student_id];
						$valid_student_count++;
						$row_num+=1;
						$y .= '<tr>';
							$y .= '<td'.$td_class.'>'.$row_num.'</td>';
							$class_info = $studentIdToAry[$student_id]['ClassTitle'].' ('.$studentIdToAry[$student_id]['ClassNumber'].')<br />';
							$y .= '<td'.$td_class.' style="text-align:center;">'.$class_info.$studentIdToAry[$student_id]['NameField'].'</td>';
							foreach($am_pm_ary as $day){
								
								$cell = (isset($user_routes['N'][$day]['RouteName'])? $user_routes['N'][$day]['RouteName'] : $Lang['eSchoolBus']['Settings']['Student']['NotTake'] );
								$cell.= ($user_routes['N'][$day]['Cars']!=''? '<br />'.$user_routes['N'][$day]['Cars'] : '');
								$cell.= '<br />'.$user_routes['N'][$day]['BusStopsName'];
								$y .= '<td'.$td_class.' style="text-align:center;">'.$cell.'</td>';
								
							}
							foreach($weekday_ary as $weekday){
								$cell = '';
								if($user_routes['S'][$weekday]['ActivityID']!=''){
									$cell .= $schoolBusConfig['ActivityTypes'][$user_routes['S'][$weekday]['ActivityID']-1][1]."<br />";
									if($user_routes['S'][$weekday]['RouteCollectionID'] == '0'){
										$cell .= $Lang['eSchoolBus']['Settings']['Student']['ByParent'];
									}else{
										$cell .= $user_routes['S'][$weekday]['RouteName'];
										$cell .= ($user_routes['S'][$weekday]['Cars']!=''? '<br />'.$user_routes['S'][$weekday]['Cars'] : '');
										$cell .= '<br />'.$user_routes['S'][$weekday]['BusStopsName'];
									}
								}
								
								$y .= '<td'.$td_class.' style="text-align:center;">'.$cell.'</td>';
							}
							$y .= '<td'.$td_class.'>&nbsp;</td>';
						$y.= '</tr>';
						
					}
				}
			}
		}
			
			$y .= '</tbody>';
		$y .= '</table>';
		$y .= "<br />\n";
		
		if($valid_student_count > 0){
			$x .= $y;
		}
	}
	
	
	if($Format == 'HTML')
	{
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
		echo $x;
		include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	}else{
		echo $x;
	}
}
*/

if(in_array($Format,array('EXCEL',''))){
	
	include_once($intranet_root."/includes/phpxls/Classes/PHPExcel/IOFactory.php");
	include_once($intranet_root."/includes/phpxls/Classes/PHPExcel.php");
	
	$objPHPExcel = new PHPExcel();
	// Set properties
	$objPHPExcel->getProperties()->setCreator("eClass")
	->setLastModifiedBy("eClass")
	->setTitle("eClass eSchoolBus Class Student List")
	->setSubject("eClass eSchoolBus Class Student List")
	->setDescription("eClass eSchoolBus Class Student List")
	->setKeywords("eClass eSchoolBus Class Student List")
	->setCategory("eClass");
	
	$sheet_index = 0;
	foreach($formToClassAry as $year_id => $year_ary)
	{
		if(count($year_ary['StudentID'])==0) continue;
		
		$year_name = $year_ary['YearName'];
		$student_count = count($year_ary['StudentID']);
		$year_class_id_ary = $year_ary['YearClassID'];
		
		if($sheet_index > 0)
		{
			$objPHPExcel->createSheet();
			
			$row = 0;
		}
			$objPHPExcel->setActiveSheetIndex($sheet_index);
			$ActiveSheet[$year_name] = $objPHPExcel->getActiveSheet();
			$ActiveSheet[$year_name]->setTitle($year_name.' - '.$student_count.$Lang['eSchoolBus']['Report']['ClassStudentList']['People']);
		
		
		for($k=0;$k<count($year_class_id_ary);$k++)
		{
			$student_id_ary = $classIdToStudentIdAry[$year_class_id_ary[$k]];
			$student_count = count($student_id_ary);
			
			if($student_count == 0) continue;
			
			$year_class_name = Get_Lang_Selection($yearClassIdToClassAry[$year_class_id_ary[$k]]['ClassTitleB5'] , $yearClassIdToClassAry[$year_class_id_ary[$k]]['ClassTitleEN']);
			
			$row += 2;
			
			$ActiveSheet[$year_name]->mergeCells('A'.$row.':J'.$row);
			$ActiveSheet[$year_name]->setCellValue('A'.$row,$Lang['eSchoolBus']['Report']['ClassStudentList']['StudentNameList']);
			$ActiveSheet[$year_name]->getStyle('A'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('A'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$row += 1;
			
			$row += 1;
			$table_start_row = $row;
			$ActiveSheet[$year_name]->mergeCells('A'.$row.':A'.($row+1)); // blank
			
			$ActiveSheet[$year_name]->mergeCells('B'.$row.':B'.($row+1)); // form name
			$ActiveSheet[$year_name]->setCellValue('B'.$row,$year_class_name);
			$ActiveSheet[$year_name]->getStyle('B'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('B'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$ActiveSheet[$year_name]->mergeCells('C'.$row.':D'.($row)); // Regular days
			$ActiveSheet[$year_name]->setCellValue('C'.$row,$Lang['eSchoolBus']['Report']['ClassStudentList']['RegularDays']);
			$ActiveSheet[$year_name]->getStyle('C'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$ActiveSheet[$year_name]->getStyle('D'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$ActiveSheet[$year_name]->mergeCells('E'.$row.':E'.($row+1)); // Monday
			$ActiveSheet[$year_name]->setCellValue('E'.$row,$Lang['eSchoolBus']['Settings']['Route']['Weekday']['MON']);
			$ActiveSheet[$year_name]->getStyle('E'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('E'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$ActiveSheet[$year_name]->mergeCells('F'.$row.':F'.($row+1)); // Tuesday
			$ActiveSheet[$year_name]->setCellValue('F'.$row,$Lang['eSchoolBus']['Settings']['Route']['Weekday']['TUE']);
			$ActiveSheet[$year_name]->getStyle('F'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('F'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$ActiveSheet[$year_name]->mergeCells('G'.$row.':G'.($row+1)); // Wednesday
			$ActiveSheet[$year_name]->setCellValue('G'.$row,$Lang['eSchoolBus']['Settings']['Route']['Weekday']['WED']);
			$ActiveSheet[$year_name]->getStyle('G'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('G'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$ActiveSheet[$year_name]->mergeCells('H'.$row.':H'.($row+1)); // Thursday
			$ActiveSheet[$year_name]->setCellValue('H'.$row,$Lang['eSchoolBus']['Settings']['Route']['Weekday']['THU']);
			$ActiveSheet[$year_name]->getStyle('H'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$ActiveSheet[$year_name]->mergeCells('I'.$row.':I'.($row+1)); // Friday
			$ActiveSheet[$year_name]->setCellValue('I'.$row,$Lang['eSchoolBus']['Settings']['Route']['Weekday']['FRI']);
			$ActiveSheet[$year_name]->getStyle('I'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('I'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$ActiveSheet[$year_name]->mergeCells('J'.$row.':J'.($row+1)); // Remark
			$ActiveSheet[$year_name]->setCellValue('J'.$row,$Lang['eSchoolBus']['Management']['Attendance']['Remarks']);
			$ActiveSheet[$year_name]->getStyle('J'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('J'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$row += 1;
			$ActiveSheet[$year_name]->setCellValue('C'.$row,$Lang['eSchoolBus']['Settings']['Student']['AM']);
			$ActiveSheet[$year_name]->setCellValue('D'.$row,$Lang['eSchoolBus']['Settings']['Student']['PM']);
			$ActiveSheet[$year_name]->getStyle('C'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$ActiveSheet[$year_name]->getStyle('D'.$row)->getFont()->setBold(true);
			$ActiveSheet[$year_name]->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			for($i=0;$i<$student_count;$i++){
				$student_id = $student_id_ary[$i];
				$user_routes = $records[$student_id];
				
				//$class_info = $studentIdToAry[$student_id]['ClassTitle'].' ('.$studentIdToAry[$student_id]['ClassNumber'].')'."\n";
				$student_name = $studentIdToAry[$student_id]['EnglishName']."\n".$studentIdToAry[$student_id]['ChineseName'];
				
				$row += 1;
				$row_num = $i+1;
				
				$col = 'A';
				$ActiveSheet[$year_name]->setCellValue($col.$row,$row_num);
				$ActiveSheet[$year_name]->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$col = chr(ord($col)+1);
				$ActiveSheet[$year_name]->setCellValue($col.$row,$student_name);
				$ActiveSheet[$year_name]->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				foreach($am_pm_ary as $day){
					
					$cell = (isset($user_routes['N'][$day]['RouteName'])? $user_routes['N'][$day]['RouteName'] : $Lang['eSchoolBus']['Settings']['Student']['NotTake'] );
					$cell.= ($user_routes['N'][$day]['Cars']!=''? "\n".$user_routes['N'][$day]['Cars'] : '');
					$cell.= "\n".$user_routes['N'][$day]['BusStopsName'];
					
					$col = chr(ord($col)+1);
					$ActiveSheet[$year_name]->setCellValue($col.$row,$cell);
					$ActiveSheet[$year_name]->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
				foreach($weekday_ary as $weekday){
					$cell = '';
					if($user_routes['S'][$weekday]['ActivityID']!=''){
						$cell .= $schoolBusConfig['ActivityTypes'][$user_routes['S'][$weekday]['ActivityID']-1][1]."\n";
						if($user_routes['S'][$weekday]['RouteCollectionID'] == '0'){
							$cell .= $Lang['eSchoolBus']['Settings']['Student']['ByParent'];
						}else{
							$cell .= $user_routes['S'][$weekday]['RouteName'];
							$cell .= ($user_routes['S'][$weekday]['Cars']!=''? "\n".$user_routes['S'][$weekday]['Cars'] : '');
							$cell .= "\n".$user_routes['S'][$weekday]['BusStopsName'];
						}
					}
					
					$col = chr(ord($col)+1);
					$ActiveSheet[$year_name]->setCellValue($col.$row,$cell);
					$ActiveSheet[$year_name]->getStyle($col.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
				$col = chr(ord($col)+1);
				$ActiveSheet[$year_name]->setCellValue($col.$row,'');
			}
			
			$table_end_row = $row;
			
			$row += 1;
			
			$ActiveSheet[$year_name]->getStyle("A".$table_start_row.":J".$table_end_row)->applyFromArray(
			    array(
			        'borders' => array(
			            'allborders' => array(
			                'style' => PHPExcel_Style_Border::BORDER_THIN,
			                'color' => array('rgb' => '000000')
			            )
			        )
			    )
			);
		
		}
		
		$sheet_index+=1;
	}
	
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	
	$tmp_folder = $intranet_root.'/file/eSchoolBus';
	if(!is_dir($tmp_folder)){
		$lf->folder_new($tmp_folder);
	}
	$tmp_file_name = 'tmp'.md5(uniqid()).'.xls';
	$tmp_file = $tmp_folder.'/'.$tmp_file_name;
	
	//shell_exec('touch '.$tmp_file);
	//shell_exec('chmod 777 '.$tmp_file);
	file_put_contents($tmp_file,'');
	chmod($tmp_file, 0777);
	
	$objWriter->save($tmp_file);
	
	$output_filename = 'class_student_name_list.xls';
	$content = get_file_content($tmp_file);
	$lf->file_remove($tmp_file);
	intranet_closedb();
	output2browser($content, $output_filename);
}

?>