<?php
/*
 *  using:
 *  
 *  2019-04-16 [Cameron]
 *      - add filter hasRouteOrNot to getStudentListByClass() [case #Y159784]
 *      - don't show timeSlot column ( There's only "FromSchool" option for Kentville) 
 *      
 *  2019-03-06 [Cameron]
 *      - add ClassGroupID filter [case #Y156987] 
 *      
 *  2018-11-22 Cameron
 *      - create this file
 */
 
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

$lexport = new libexporttext();

$db = $indexVar['db']; 
$linterface = $indexVar['linterface'];

$date = $_POST['SearchDate'];
$timeSlot = $_POST['TimeSlot'];
$hasRouteOrNot = $_POST['HasRouteOrNot'];
$buildingID = $_POST['BuildingID'];
$takeOrNot = $_POST['TakeOrNot'];
$yearClassIDAry = $_POST['YearClassID'];
$classGroupIDAry = $_POST['ClassGroupID'];

$studentAry = $db->getStudentListByClass($date, $timeSlot, $buildingID, $takeOrNot, $hasRouteOrNot, $yearClassIDAry, $classGroupIDAry);

$dataAry = array();
$rowCount = 0;

// Report Name
$dataAry[$rowCount++][0] = $Lang['eSchoolBus']['ReportArr']['StudentListByClassArr']['MenuTitle'];
    
// Date
$dataAry[$rowCount++][0] = $Lang['General']['Date'] . ' : ' . $date;

// Column Title
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['Name'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['StudentNumber'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['Route'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['Class'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['Campus'];
//$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['TimeSlot'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['TakeOrNot'];
$dataAry[$rowCount][] = $Lang['eSchoolBus']['Report']['ByClass']['Title']['Remark'];
$rowCount++;

$numberOfRecord=count($studentAry);
if ($numberOfRecord == 0) {
    $dataAry[$rowCount++][0] = $Lang['General']['NoRecordAtThisMoment'];
}
else {
    for ($i=0; $i<$numberOfRecord; $i++) {
        $_studentAry = $studentAry[$i];
        
        $dataAry[$rowCount][] = strip_tags($_studentAry['StudentName']);
        $dataAry[$rowCount][] = $_studentAry['ClassNumber'];
        $dataAry[$rowCount][] = $_studentAry['RouteName'];
        $dataAry[$rowCount][] = $_studentAry['ClassName'];
        $dataAry[$rowCount][] = $_studentAry['BuildingName'];
//        $dataAry[$rowCount][] = $_studentAry['TimeSlot'];
        $dataAry[$rowCount][] = $_studentAry['TakeOrNot'];
        $dataAry[$rowCount][] = $_studentAry['Remark'];
        $rowCount++;
    }
}

$numOfRow = count($dataAry);
for($i=0;$i<$numOfRow;$i++){
    $numOfColumn = count($dataAry[$i]);
    for($j=0;$j<$numOfColumn;$j++){
        $dataAry[$i][$j]=str_replace($seperator,"\n",$dataAry[$i][$j]);
    }
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

$filename = "bus_taking_list_by_class_at_specific_date.csv";

$lexport->EXPORT_FILE($filename,$export_text);
?>