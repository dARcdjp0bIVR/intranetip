<?php
/*
 *  Editing by
 *  
 *  2019-04-16 Cameron
 *      - remove option TimeSlot (because Kentville concern "From School" only 
 *      - add option HasRouteOrNot [case #Y159784]
 *      
 *  2019-03-06 Cameron
 *      - add ClassGroupID filter [case #Y156987]
 *      
 *  2018-11-21 Cameron
 *      - create this file
 * 
 */
 
include_once ($PATH_WRT_ROOT . "includes/form_class_manage.php");

$linterface = $indexVar['linterface'];
$linterface->LAYOUT_START($returnMsg);
$db = $indexVar['db'];
$currentDate = date('Y-m-d');
$buildingSelection = $linterface->getClassBuilding($currentDate);

$currentAcademicYearID = Get_Current_Academic_Year_ID();
$tags =' id="YearClassID" name="YearClassID[]" multiple="multiple" size="10" ';
$classSelection = $linterface->getClassSelection($currentAcademicYearID, $tags, $selected="", $all=0, $noFirst=0, $FirstTitle="");

$classGroupSelection = $linterface->getClassGroupSelection();

?>
<script>
var loading_img = '<?=$linterface->Get_Ajax_Loading_Image()?>';

function showOption(){
	$('div#reportOptionDiv').show();
	$('#spanShowOption_reportOptionOuterDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').show();
	$('#tdOption').removeClass( "report_show_option" );
}
function hideOption(){
	$('div#reportOptionDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').hide();
	$('#spanShowOption_reportOptionOuterDiv').show();
	$('#tdOption').addClass( "report_show_option" );
}

function getBuildingList()
{
	var date = $('input#SearchDate').val();
	
	$('td#BuildingSelection').html(loading_img);
	$.ajax({
		url: "index.php?task=ajax/ajax",
		type: "POST",
		data: {
			action: 'getClassBuilding',
			date : date			
		},
		dataType: "json",
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('td#BuildingSelection').html(ajaxReturn.html);
				if (date != '' && ajaxReturn.AcademicYearID != <?php echo $currentAcademicYearID;?>) {
					getClassList(ajaxReturn.AcademicYearID);
				}
			}
		},
		error: show_ajax_error
	});
}

function isHoliday()
{
	var ret = false;
	
	$.ajax({
		url: "index.php?task=ajax/ajax",
		type: "POST",
		async: false,
		data : {
			action: 'checkHoliday',
			date: $('input#SearchDate').val()
		},
		dataType: "json",
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				if (ajaxReturn.html == '1') {
					ret = true;					
				}
			}
		},
		error: show_ajax_error
	});
	return ret;
}

function getClassList(academicYearID)
{
	$('td#ClassSelection').html(loading_img);
	$.ajax({
		url: "index.php?task=ajax/ajax",
		type: "POST",
		data: {
			action: 'getClassList',
			academicYearID : academicYearID			
		},
		dataType: "json",
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('td#ClassSelection').html(ajaxReturn.html);
			}
		},
		error: show_ajax_error
	});
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

function submitForm()
{
	$('#submitBtn').attr('disabled',true);
	var valid = true;
	var format = $('#Format').val();
	
	$('.Error').hide();
	if($('#SearchDate').val() == ''){
		valid = false;
		$('#Date_Error').show();
	}

	if (isHoliday()) {
		valid = false;
		$('#Holiday_Error').show();
	}
	
	if($('#YearClassID').val() == null || $('#YearClassID').val() == ''){
		valid = false;
		$('#YearClassID_Error').show();
	}
	
	if(!valid){
		$('#submitBtn').attr('disabled',false);
		return;
	}

	if (Trim($('div#reportOptionOuterDiv').html()) == '') {
		hideOption();
		$('div#showHideOptionDiv').show();
		$('div#contentToolDiv').show();
	}else{
		$('a#spanHideOption_reportOptionOuterDivBtn')[0].click();
	}
	
	getReport();
}

function getReport()
{
	$('div#reportResultDiv').html(loading_img);
	$.ajax({
		url: "index.php?task=ajax/ajax",
		type: "POST",
		data : $('#form1').serialize() + '&action=getStudentListByClass',
		dataType: "json",
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('div#reportResultDiv').html(ajaxReturn.html);
				$('#submitBtn').attr('disabled',false);
			}
		},
		error: show_ajax_error
	});
}

function goExport() {
	$('form#form1').attr('target', '_self').attr('action', 'index.php?task=report/studentListByClass/export').submit();
}

function goPrint() {
	$('form#form1').attr('target', '_blank').attr('action', 'index.php?task=report/studentListByClass/print').submit();
}

$(document).ready(function () {
	$('div#contentToolDiv').hide();
	$('#reportResultDiv').html('');		// clear the result
	$('#YearClassID option').attr('selected',true);
	$('#ClassGroupID option').attr('selected',true);
});

</script>
<style>

</style>

<form name="form1" id="form1" action="index.php?task=report/studentListByClass/report" method="post" onsubmit="return false;">

<div id="showHideOptionDiv" style="display:none">
	<table style="width:100%;">
		<tr>
			<td id="tdOption" class="report_show_option">
				<span id="spanShowOption_reportOptionOuterDiv">'
				<?= $linterface->Get_Show_Option_Link("javascript:showOption();", '', '', 'spanShowOption_reportOptionOuterDivBtn') ?>
				</span>
				<span id="spanHideOption_reportOptionOuterDiv" style="display:none">
				<?= $linterface->Get_Hide_Option_Link("javascript:hideOption();", '', '', 'spanHideOption_reportOptionOuterDivBtn')?>
				</span>
				<div id="reportOptionOuterDiv" style="display:none;">
			</td>
		</tr>
	</table>
</div>
<div id="reportOptionDiv">
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center" class="form_table_v30">
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%"><span class="tabletextrequire">*</span><?=$Lang['General']['Date']?></td>
			<td>
			<?php echo $linterface->GET_DATE_PICKER('SearchDate','','','yy-mm-dd','','','','getBuildingList();'); ?>
			<?php echo $linterface->Get_Form_Warning_Msg('Date_Error', $Lang['eSchoolBus']['Report']['SelectDate'], 'Error'); ?>
			<?php echo $linterface->Get_Form_Warning_Msg('Holiday_Error', $Lang['eSchoolBus']['Report']['IsHolidayWarning'], 'Error'); ?>
			</td>
		</tr>
<!--  		
        <tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Report']['ByClass']['Title']['TimeSlot']?></td>
			<td>
				<select id="TimeSlot" name="TimeSlot">
					<option value="AM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'];?></option>
					<option value="PM"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'];?></option>
					<option value="WD" selected="selected"><?php echo $Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'];?></option>
				</select>
			</td>
		</tr>
-->
        <tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Report']['ByClass']['HasRouteOrNot']?></td>
			<td>
				<select id="HasRouteOrNot" name="HasRouteOrNot">
					<option value="Y" selected="selected"><?php echo $Lang['eSchoolBus']['Report']['ByClass']['HasRoute'];?></option>
					<option value="N"><?php echo $Lang['eSchoolBus']['Report']['ByClass']['NoRoute'];?></option>
					<option value="A"><?php echo $Lang['eSchoolBus']['Report']['ByClass']['All'];?></option>
				</select>
			</td>
		</tr>

		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Report']['ByClass']['TakeOption']?></td>
			<td>
				<select id="TakeOrNot" name="TakeOrNot">
					<option value="0" selected="selected"><?php echo $Lang['eSchoolBus']['Report']['ByClass']['All'];?></option>
					<option value="1"><?php echo $Lang['eSchoolBus']['Report']['ByClass']['Take'];?></option>
					<option value="2"><?php echo $Lang['eSchoolBus']['Report']['ByClass']['NotTake'];?></option>
				</select>
			</td>
		</tr>

		<tr>
			<td valign="top" nowrap="nowrap" class="field_title" width="30%"><?=$Lang['eSchoolBus']['Report']['Campus']?></td>
			<td id="BuildingSelection">
				<?php echo $buildingSelection;?>
			</td>
		</tr>
		
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['Header']['Menu']['Class']?></td>
			<td id="ClassSelection"><?=$classSelection?>
				<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('YearClassID')){Select_All_Options('YearClassID', true);}return false;","selectAllTargetBtn",'')?>
				<?=$linterface->Get_Form_Warning_Msg('YearClassID_Error', $Lang['eSchoolBus']['Report']['ByClass']['RequestSelectClass'], 'Error')?>
				<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
			</td>
		</tr>

		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup']?></td>
			<td id="ClassGroupSelection"><?=$classGroupSelection?>
				<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('ClassGroupID')){Select_All_Options('ClassGroupID', true);}return false;","selectAllTargetBtn",'')?>
				<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
			</td>
		</tr>
		
	</table>
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "submitForm();", 'submitBtn', $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn")?>
		<p class="spacer"></p>
	</div>
	<br>
</div>

<div id="contentToolDiv" class="content_top_tool" style="display:none;">
	<div class="Conntent_tool">
		<?php echo $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');?>
		<?php echo $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');?>
	</div>
</div>

<input type="hidden" name="TimeSlot" value="PM">
</form>

<div id="reportResultDiv">
</div>

<?php
$linterface->LAYOUT_STOP();
 ?>