<?php
/*
 *  2019-03-06 Cameron
 *      - add filterUserID to getRouteCollectionUserCount() and $filterMap to filter selected class group only [case #Y156987]
 */
@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);
ini_set('zend.ze1_compatibility_mode', '0');


$db = $indexVar['db'];
$academicYearID = $_POST['academicYearID'];
$academicYear = getAcademicYearByAcademicYearID($academicYearID);
$type = $_POST['type'];
$weekdayAry = $_POST['weekday'];
$classGroupIDAry = $_POST['ClassGroupID'];

$currentPageAry = array(
		'AM'=>array('A',1),
		'PM'=>array('A',1),
		'MON'=>array('A',1),
		'TUE'=>array('A',1),
		'WED'=>array('A',1),
		'THU'=>array('A',1),
		'FRI'=>array('A',1)
);

//Get All Route of Certain type / weekday of the route

$filterAry = array();
$additionalCond = '';
$filterAry['route.AcademicYearID'] = $academicYearID;
if($type=='S'){
	$additionalCond .= " AND ";
	$additionalCond .= " collection.Weekday IN ('".implode("','",$weekdayAry)."') ";
}
$routeInfo = $db->getRouteInfo($routeIDAry='',$isDeleted=0,$type=$type,$filterAry,$additionalCond);
$routeCollectionIdAry = Get_Array_By_Key($routeInfo, "RouteCollectionID");
$routeCollectionIdAry = array_values(array_unique($routeCollectionIdAry));
$busStopIdAry = Get_Array_By_Key($routeInfo, "BusStopsID");
$busStopIdAry = array_unique($busStopIdAry);

if($type=='N'){
	$routeInfo = BuildMultiKeyAssoc($routeInfo, array('RouteID','AmPm'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
}else{
	$routeInfo = BuildMultiKeyAssoc($routeInfo, array('RouteID','Weekday'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
}
// debug_pr($routeCollectionIdAry);


$filterMap = array('RouteCollectionID'=>$routeCollectionIdAry);

// UserID in selected class group
$filterUserIDAry = array();
$dispClassGroup = $Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup'] . " : ";
$classGroupAry = $db->getClassGroup();
if ($classGroupIDAry != '') {
    if (count($classGroupIDAry) != count($classGroupAry)) {
        $filterUserIDAry = $db->getUserIDByClassGroup($classGroupIDAry, $academicYearID);
        $filterMap['UserID'] = $filterUserIDAry;
        
        $dispClassGroupAry = array();
        foreach((array)$classGroupIDAry as $_classGroupID) {
            $dispClassGroupAry[] = $classGroupAry[$_classGroupID];
        }
        $dispClassGroup .= implode(", ", $dispClassGroupAry);
        unset($dispClassGroupAry);
    }
    else {
        $dispClassGroup .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
    }
}
else {
    $dispClassGroup .= $Lang['eSchoolBus']['Report']['ByClass']['All'];
}


//Get User Count for each route collection and stops
$userCountAry = $db->getRouteCollectionUserCount($routeCollectionIdAry, $filterUserIDAry);
$userCountAry = BuildMultiKeyAssoc($userCountAry, array('RouteCollectionID','BusStopsID'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);
// debug_pr($routeCollectionIdAry);
//Get User Route 
$userRouteAry = $db->getUserRouteRecords($filterMap,$additionalCond='',$includeUserName=true,$includeTime=false);
$userRouteAry = BuildMultiKeyAssoc($userRouteAry, array('RouteCollectionID','BusStopsID'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 1);
// debug_pr($userRouteAry);
//Get BusStops Info
$busStopInfoAry = $db->getBusStopsInfo($busStopIdAry);
$busStopInfoAry = BuildMultiKeyAssoc($busStopInfoAry, array('BusStopsID'), $IncludedDBField = array(), $SingleValue = 0, $BuildNumericArray = 0);


//Generate XLS
/** PHPExcel_IOFactory */
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel/IOFactory.php");
include_once($PATH_WRT_ROOT."includes/phpxls/Classes/PHPExcel.php");
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("eClass")
->setLastModifiedBy("eClass")
->setTitle("eClass eSchoolBus Student List")
->setSubject("eClass eSchoolBus Student List")
->setDescription("eClass eSchoolBus Student List")
->setKeywords("eClass eSchoolBus Student List")
->setCategory("eClass");

$title = $academicYear.' '.$Lang['eSchoolBus']['ReportArr']['StudentListArr']['MenuTitle'];
$title .= ' ('.$dispClassGroup.')';

$heading[0] = $Lang['eSchoolBus']['Report']['StudentList']['CarNum'];
$heading[1] = $Lang['eSchoolBus']['Report']['StudentList']['BusStop'];
$heading[2] = $Lang['eSchoolBus']['Report']['StudentList']['NumberOfStudent'];
$heading[3] = $Lang['eSchoolBus']['Report']['StudentList']['StudentName'];

$objPHPExcel = new PHPExcel();
if($type=='N'){
	//Create xls sheet
    setHeader('AM',0,$title);
    setHeader('PM',1,$title);
}else{
    setHeader('MON',0,$title);
    setHeader('TUE',1,$title);
    setHeader('WED',2,$title);
    setHeader('THU',3,$title);
    setHeader('FRI',4,$title);
}
foreach ((array)$routeInfo as $_routeID => $_routeAryByWeekday){
	$_sumOfCount = 0;
	foreach($_routeAryByWeekday as $__AmPmOrWeekDay => $__stopsAry){
		
		
		foreach ($__stopsAry as $___stopsInfo){
			nextRow($__AmPmOrWeekDay);
			$__thisRowStartNum = $currentPageAry[$__AmPmOrWeekDay][1];
			
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(getCurrentPage($__AmPmOrWeekDay), $___stopsInfo['RouteName']);
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $busStopInfoAry[$___stopsInfo['BusStopsID']][Get_Lang_Selection('BusStopsNameChi','BusStopsName')]);
			$__count = $userCountAry[$___stopsInfo['RouteCollectionID']][$___stopsInfo['BusStopsID']]['CountUser'];
			if(!$__count){
				$__count = 0;
			}
			$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $__count);
			
			$___countOfStudent = 0;
			foreach((array)$userRouteAry[$___stopsInfo['RouteCollectionID']][$___stopsInfo['BusStopsID']] as $____userRouteInfo){
				$___countOfStudent++;
				if($___countOfStudent%5==1 && $___countOfStudent>5){
					nextRow($__AmPmOrWeekDay);
					nextColumn($__AmPmOrWeekDay);
					nextColumn($__AmPmOrWeekDay);
				}
				$ActiveSheet[$__AmPmOrWeekDay]->setCellValue(nextColumn($__AmPmOrWeekDay), $____userRouteInfo['EnglishName'].'/'.$____userRouteInfo['ChineseName']);
			}
			$__thisRowEndNum = $currentPageAry[$__AmPmOrWeekDay][1];
			$ActiveSheet[$__AmPmOrWeekDay]->mergeCells('A'.$__thisRowStartNum.':A'.$__thisRowEndNum);
			$ActiveSheet[$__AmPmOrWeekDay]->mergeCells('B'.$__thisRowStartNum.':B'.$__thisRowEndNum);
			$ActiveSheet[$__AmPmOrWeekDay]->mergeCells('C'.$__thisRowStartNum.':C'.$__thisRowEndNum);
		}
		setBorder($ActiveSheet[$__AmPmOrWeekDay],'A1','H'.$currentPageAry[$__AmPmOrWeekDay][1]);
	}
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

$dirPath = $PATH_WRT_ROOT.'file/eSchoolBus';
if(!is_dir($dirPath)){
	mkdir($dirPath);
}

$fileName = 'eSchoolBus_studentList_'.$type.'_'.$_SESSION['UserID'].'.xls';
$exportFileName = 'eSchoolBus_studentList_'.$type.'.xls';

$path2write = $dirPath.'/'.$fileName;
$successArr[]= $objWriter->save($path2write);

output2browser(get_file_content($path2write), $exportFileName);



function getCurrentPage($key){
	global $currentPageAry;
	return $currentPageAry[$key][0].$currentPageAry[$key][1];
}
function nextColumn($key,$getResultOnly=false){
	global $currentPageAry;
	$alpha = $currentPageAry[$key][0];
	if($alpha=='Z'){
		return 'AA';
	}
	$alpha = chr(ord($alpha)+1) ;
	if($getResultOnly){
		return $alpha.$currentPageAry[$key][1];
	}
	$currentPageAry[$key][0] = $alpha ;
	return getCurrentPage($key);
}
function nextRow($key,$getResultOnly=false){
	global $currentPageAry;
	$num = $currentPageAry[$key][1];
	$num = $num + 1;
	$alpha = 'A';
	if($getResultOnly){
		return $alpha.$num;
	}
	$currentPageAry[$key][1] = $num ;
	$currentPageAry[$key][0] = $alpha;
	return getCurrentPage($key);
}
function setBorder($ActiveSheet,$startBox,$endBox){
	$ActiveSheet->getStyle($startBox.":".$endBox)->applyFromArray(
		array(
				'borders' => array(
						'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('rgb' => '000000')
						)
				)
		)
	);
}
function setHeader($key,$sheetNum,$title=''){
	global $objPHPExcel,$ActiveSheet,$Lang,$academicYear;
	
	if (empty($title)) {
	   $title = $academicYear.' '.$Lang['eSchoolBus']['ReportArr']['StudentListArr']['MenuTitle'];
    }
	
	$heading[0] = $Lang['eSchoolBus']['Report']['StudentList']['CarNum'];
	$heading[1] = $Lang['eSchoolBus']['Report']['StudentList']['BusStop'];
	$heading[2] = $Lang['eSchoolBus']['Report']['StudentList']['NumberOfStudent'];
	$heading[3] = $Lang['eSchoolBus']['Report']['StudentList']['StudentName'];
	
	if($sheetNum!=0){
		$objPHPExcel->createSheet();
	}
	$objPHPExcel->setActiveSheetIndex($sheetNum);
	$ActiveSheet[$key] = $objPHPExcel->getActiveSheet();
	$ActiveSheet[$key]->setTitle($key);
	$ActiveSheet[$key]->setCellValue(getCurrentPage($key), $title);
	$_startMerge = getCurrentPage($key);
	nextColumn($key);
	nextColumn($key);
	nextColumn($key);
	nextColumn($key);
	nextColumn($key);
	nextColumn($key);
	$ActiveSheet[$key]->mergeCells($_startMerge.':'.nextColumn($key));
	$ActiveSheet[$key]->setCellValue(nextRow($key), $heading[0]);
	$ActiveSheet[$key]->setCellValue(nextColumn($key), $heading[1]);
	$ActiveSheet[$key]->setCellValue(nextColumn($key), $heading[2]);
	$ActiveSheet[$key]->setCellValue(nextColumn($key), $heading[3]);
	$_startMerge = getCurrentPage($key);
	nextColumn($key);
	nextColumn($key);
	nextColumn($key);
	$_endMerge = nextColumn($key);
	$ActiveSheet[$key]->mergeCells($_startMerge.':'.$_endMerge);
}
?>