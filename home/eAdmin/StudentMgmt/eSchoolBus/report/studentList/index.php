<?php
// Editing by: 
/*
 *  2019-03-06 Cameron
 *      - add ClassGroupID filter [case #Y156987]
 *      
 */
$linterface = $indexVar['linterface'];
$requiredSym = $linterface->RequiredSymbol();
$linterface->LAYOUT_START($returnMsg);

$academicYearID = ($academicYearID=='')?Get_Current_Academic_Year_ID():$academicYearID;

$classGroupSelection = $linterface->getClassGroupSelection();

?>
<?=$linterface->INCLUDE_COMMON_CSS_JS()?>
<br />
<script>
function selectAllWeekday(value){
	var isChecked = $(value).is(':checked');
	switch(isChecked){
		case true:
			$('input[name="weekday[]"]').each(function(){
				$(this).attr('checked','checked');
			});
		break;
		case false:
			$('input[name="weekday[]"]').each(function(){
				$(this).attr('checked','');
			});
		break;
	}
}
function selectType(value){
	switch(value){
		case 'N':
			$('.asa').hide();
		break;
		case 'S':
			$('.asa').show();
		break;
	}
}
function checkSubmitForm(form){
	$('.WarnMsgDiv').hide();
	var type = $('input[name="type"]:checked').val();
	if(type=='S'){
		var weekdays = $('input[name="weekday[]"]:checked');
		if(weekdays.length==0){
			$('#weekdaysWarning').show();
			return;
		}
	}

	var format = $('#Format').val();
	if(format == 'HTML')
	{
		var url = '';
		var winType = '10';
		var win_name = 'intranet_popup'+winType;
		
		var oldAction = document.form1.action;
		var oldTarget = document.form1.target;
		
		//document.form1.action = 'index.php?task=report/classList/report';
		document.form1.target = win_name;
		
		newWindow(url, winType);
		document.form1.submit();
		
		document.form1.action = oldAction;
		document.form1.target = oldTarget;
		$('#submitBtn').attr('disabled',false);
	}else if(format == 'EXCEL'){
		
		var oldAction = document.form1.action;
		var oldTarget = document.form1.target;
		
		//document.form1.action = 'index.php?task=report/classList/report';
		//document.form1.target = '_blank';
		document.form1.target = '_self';
		document.form1.submit();
		
		document.form1.action = oldAction;
		document.form1.target = oldTarget;
		$('#submitBtn').attr('disabled',false);
	}
}

$(document).ready(function () {
	$('#ClassGroupID option').attr('selected',true);
});

</script>
<form name="form1" id="form1" action="index.php?task=report/studentList/reportGeneration" method="post">
	<table width="100%" cellpadding="2" class="form_table_v30">
		<col class="field_title">
		<col class="field_c">
		<tr>
			<td class="field_title"><?=$requiredSym?><?=$Lang['eSchoolBus']['Report']['RouteArrangement']['Period'] ?></td>
			<td>
				<?= getSelectAcademicYear('academicYearID','onchange="$(\'#form1\').submit();"', 1, 0, $academicYearID); ?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$requiredSym?><?=$Lang['eSchoolBus']['Report']['RouteArrangement']['RouteType'] ?></td>
			<td>
				<?=$linterface->Get_Radio_Button('type_N', 'type', $Value='N', $isChecked=1, $Class="", $Display=$Lang['eSchoolBus']['Settings']['Route']['NormalRoute'], $Onclick="selectType(this.value);",$isDisabled=0) ?>
				&nbsp;
				<?=$linterface->Get_Radio_Button('type_S', 'type', $Value='S', $isChecked=0, $Class="", $Display=$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute'], $Onclick="selectType(this.value);",$isDisabled=0) ?>
			</td>
		</tr>
		<tr class="asa" style="display:none">
			<td class="field_title"><?=$requiredSym?><?=$Lang['eSchoolBus']['Report']['RouteArrangement']['ASAWeekday'] ?></td>
			<td>
				<?= $linterface->Get_Checkbox('weekday_all', '', $Value=0, $isChecked=0, $Class='', $Display=$Lang['eSchoolBus']['Report']['RouteArrangement']['SelectAll'], $Onclick='selectAllWeekday(this)', $Disabled='')?>
				<br>
				&nbsp;<?= $linterface->Get_Checkbox('MON', 'weekday[]', $Value='MON', $isChecked=0, $Class='', $Display=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['MON'], $Onclick='', $Disabled='')?>
				&nbsp;
				&nbsp;<?= $linterface->Get_Checkbox('TUE', 'weekday[]', $Value='TUE', $isChecked=0, $Class='', $Display=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['TUE'], $Onclick='', $Disabled='')?>
				&nbsp;
				&nbsp;<?= $linterface->Get_Checkbox('WED', 'weekday[]', $Value='WED', $isChecked=0, $Class='', $Display=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['WED'], $Onclick='', $Disabled='')?>
				&nbsp;
				&nbsp;<?= $linterface->Get_Checkbox('THU', 'weekday[]', $Value='THU', $isChecked=0, $Class='', $Display=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['THU'], $Onclick='', $Disabled='')?>
				&nbsp;
				&nbsp;<?= $linterface->Get_Checkbox('FRI', 'weekday[]', $Value='FRI', $isChecked=0, $Class='', $Display=$Lang['eSchoolBus']['Settings']['Route']['Weekday']['FRI'], $Onclick='', $Disabled='')?>
				<br>
				<?=$linterface->Get_Thickbox_Warning_Msg_Div("weekdaysWarning",$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty'], "WarnMsgDiv")?>
			</td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup']?></td>
			<td id="ClassGroupSelection"><?=$classGroupSelection?>
				<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('ClassGroupID')){Select_All_Options('ClassGroupID', true);}return false;","selectAllTargetBtn",'')?>
				<div class="tabletextremark"><?=$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage']?></div>
			</td>
		</tr>		
		<tr style="display:none">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['General']['ViewFormat']?></td>
			<td>
				<select id="Format" name="Format">
					<option value="HTML">HTML</option>
					<option value="EXCEL" selected="selected">Excel</option>
				</select>
			</td>
		</tr>
	</table>
<?=$indexVar['linterface']->MandatoryField()?>
<div class="edit_bottom_v30">
<?=$indexVar['linterface']->GET_ACTION_BTN($Lang['Btn']['Submit'],"button","checkSubmitForm(document.form1);",'submitBtn').'&nbsp;'?>
</div>
</form>

<div id="">
</div>
<?php
$indexVar['linterface']->LAYOUT_STOP();
?>