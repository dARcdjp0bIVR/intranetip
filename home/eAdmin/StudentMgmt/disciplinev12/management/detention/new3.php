<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2017-10-30 (Bill)
#				fixed: PHP error when implode empty variables
#
#	Date	:	2017-09-01 (Bill)	[2017-0831-1543-30235]
#				Default uncheck checkbox - Send Notice via eNotice	($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox_Detention'])
#
#	Date	:	2015-04-30 (Bill)	[2015-0429-1005-05207]
#				fixed: disable finish button after click
#
#	Date	:	2015-04-20 (Bill)	[2014-1216-1347-28164]
#				add option to send push message to notify parent
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."/includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();
$linterface = new interface_html();

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser();
	$canSendPushMsg = true;
}

$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1);
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-New");

$CurrentPage = "Management_Detention";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$SysMsg = $linterface->GET_SYS_MSG("$msg");

$PAGE_NAVIGATION[] = array($i_Discipline_Detention_List, "list.php");

if ($from == "auto_arrange")
{
	$PAGE_NAVIGATION[] = array($i_Discipline_Auto_Arrange, "");
	$StepTable = "<br /><br />";
	$navigation = $linterface->GET_NAVIGATION2($i_Discipline_Enotice_Setting);
}
else
{
	$PAGE_NAVIGATION[] = array($i_Discipline_New_Student_Records, "");
	$STEPS_OBJ[] = array($i_Discipline_Select_Student, 0);
	$STEPS_OBJ[] = array($i_Discipline_Add_Record, 0);
	if (!$lnotice->disabled && !empty($NoticeTemplateAva))
		$STEPS_OBJ[] = array($i_Discipline_Enotice_Setting, 1);
	else
		$STEPS_OBJ[] = array($button_finish, 1);
	$StepTable = "<br /><br /><br />".$linterface->GET_STEPS($STEPS_OBJ);
	$navigation = "";
}

//$student = $_POST['student'];
if ($rid == "") $rid = "''";

if (!isset($field)) $field = 4;		// default: sort by request date
if (!isset($order)) $order = 1;		// default: asc order
$pageSizeChangeEnabled = true;
if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("USR.");
$yearID = Get_Current_Academic_Year_ID();
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

$sql = "SELECT 
			CONCAT($clsName, '-', ycu.ClassNumber) AS Class, 
			$name_field AS Student, 
			SES.Reason, 
			SUBSTRING(SES.DateInput, 1, 10) AS DateInput, 
			SES.PICID,
			SES.DetentionID,
			'Period',
			'Location',
			'PIC',
			SES.Remark,
			SES.RecordID
		FROM 
			DISCIPLINE_DETENTION_STUDENT_SESSION SES
			LEFT OUTER JOIN INTRANET_USER USR ON (SES.StudentID = USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 
			SES.RecordID IN ($rid) AND 
			yc.AcademicYearID=$yearID";
$li->field_array = array("Class", $name_field, "Reason", "DateInput", "PICID", "DetentionID", "Period", "Location", "PIC");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
//$li->title = $i_Discipline_Records;
$li->IsColOff = "eDisciplineDetentionNewRecord";
$li->AllFormsData = $i_Discipline_All_Forms;

// Table Column
$pos = 0;
$li->column_list .= "<td width='2%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='6%' nowrap>".$li->column($pos++, $i_Discipline_Class)."</td>\n";
$li->column_list .= "<td width='12%'>".$li->column($pos++, $i_Discipline_Student)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Discipline_Detention_Reason)."</td>\n";
//$li->column_list .= "<td width='15%' nowrap>".$li->column($pos++, $i_Discipline_Remark)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Request_Date)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_PIC)."</td>\n";
$li->column_list .= "<td  nowrap>".$i_Discipline_Arranged_Session."</td>\n";
$li->column_list .= "<td  nowrap>".$iDiscipline['Period']."</td>\n";
$li->column_list .= "<td  nowrap>".$i_Discipline_System_Discipline_Case_Record_Case_Location."</td>\n";
$li->column_list .= "<td  nowrap>".$i_Discipline_System_Discipline_Case_Record_PIC."</td>\n";

$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp)) {
			$catArr[] = array($Key,$Value);
		}
	}
}

$RecordID = explode(",", $rid);
for ($i=0; $i<=sizeof($RecordID); $i++)
{
	${"catSelection".$i} = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID'.$i.'", name="CategoryID'.$i.'" onChange="changeCat(this.value, '.$i.')"', "", $CategoryID);
	if ($i > 0) {
		$resultRecord = $ldiscipline->getDetentionStudentIDByRecordID($RecordID[$i-1]);
		$StudentID = $resultRecord[0];
		$student[] = $StudentID;
		
		$resultStudentName = $ldiscipline->getStudentNameByID($StudentID);
		list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
		
		// [2014-1216-1347-28164]
		$parentCanReceiveMsg = $canSendPushMsg? $luser->getParentUsingParentApp($StudentID, true) : false;
		$TableNotice .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
		$TableNotice .= "<tr>\n";
		$TableNotice .= "<td colspan=\"2\" valign=\"top\" nowrap>\n";
		$TableNotice .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		$TableNotice .= "<tr>\n";
		$TableNotice .= "<td>$i. <img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$TmpClassName-$TmpClassNumber $TmpName</span></td>\n";
		$TableNotice .= "<td align=\"right\"><span class=\"sectiontitle\">\n";
		$TableNotice .= $linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice($i);");
		$TableNotice .= "</span></td>\n";
		$TableNotice .= "</tr>\n";
		$TableNotice .= "</table>\n";
		$TableNotice .= "</td>\n";
		$TableNotice .= "</tr>\n";
		$TableNotice .= "<tr valign=\"top\" class=\"tablerow1\">\n";
		$TableNotice .= "<td width=\"20%\" nowrap class=\"formfieldtitle\"><span class=\"tabletext\">$i_Discipline_Template <span class=\"tabletextrequire\">*</span></span></td>\n";
		$TableNotice .= "<td>\n";
		$TableNotice .= "<input type=\"hidden\" id=\"RecordID$i\" name=\"RecordID$i\" value=\"".$RecordID[$i-1]."\" />";
		$TableNotice .= "<input type=\"hidden\" id=\"StudentID$i\" name=\"StudentID$i\" value=\"".$TmpUserID."\" />";
		$TableNotice .= "<input type=\"hidden\" id=\"StudentName$i\" name=\"StudentName$i\" value=\"".$TmpName."\" />";
		$TableNotice .= ${"catSelection".$i}." <select name=\"SelectNotice$i\" id=\"SelectNotice$i\"><option value=\"0\">-- $button_select --</option></select>\n";
		$TableNotice .= "</td>\n";
		$TableNotice .= "</tr>\n";
		$TableNotice .= "<tr class=\"tablerow1\">\n";
		$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">$i_Discipline_Additional_Info</td>\n";
		$TableNotice .= "<td valign=\"top\"><textarea name=\"TextAdditionalInfo$i\" id=\"TextAdditionalInfo$i\" rows=\"2\" wrap=\"virtual\" class=\"textboxtext\"></textarea></td>\n";
		$TableNotice .= "</tr>\n";
		
		// [2014-1216-1347-28164] display checkbox to send push message if client enables eClass App and student's parents using eClass App
		if($parentCanReceiveMsg){
			$TableNotice .= "<tr class=\"tablerow1\">\n";
			$TableNotice .= "<td valign=\"top\">&nbsp;</td>\n";
			$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"eClassAppNotify$i\" id=\"eClassAppNotify$i\" value='1' checked><label for=\"eClassAppNotify$i\"'>".$Lang['eDiscipline']['Detention']['SendNotification']."</label></td>\n";
			$TableNotice .= "</tr>\n";
		}
		
		$TableNotice .= "<tr class=\"tablerow1\">\n";
		$TableNotice .= "<td valign=\"top\">&nbsp;</td>\n";
		// [2014-1216-1347-28164] checkbox not checked if client enables eClass App and student's parents using eClass App
		//$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"emailNotify$i\" id=\"emailNotify$i\" value='1' checked><label for=\"emailNotify$i\"'>".$Lang['eDiscipline']['EmailNotifyParent']."</label></td>\n";
		$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"emailNotify$i\" id=\"emailNotify$i\" value='1' ".($parentCanReceiveMsg? "" : "checked")."><label for=\"emailNotify$i\"'>".$Lang['eDiscipline']['EmailNotifyParent']."</label></td>\n";
		$TableNotice .= "</tr>\n";
		if ($i != sizeof($RecordID)) {
			$TableNotice .= "<tr>\n";
			$TableNotice .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>\n";
			$TableNotice .= "</tr>\n";
		}
		$TableNotice .= "</table>\n";
	}
}

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	for ($j=0; $j<=sizeof($result); $j++) {
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
		}
		else {
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$result[$j-1][1]."\"\n";
		}
	}
}

// [2017-0831-1543-30235] Default uncheck checkbox - Send Notice via eNotice
$defaultCheckNotice = $sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox_Detention']? "" : " checked";
$NoticeDivDisplay = $sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox_Detention']? "style=\"display:none\"" : "style=\"display:inline\"";

$linterface->LAYOUT_START();
?>

<script language="javascript">

<?=$jTemplateString?>

function changeCat(cat, selectIdx)
{
	var x = document.getElementById("SelectNotice"+selectIdx);
	var listLength = x.length;
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	}
	catch (err) {
		var tmpCatLength = 0;
	}
	
	for (var j=0; j<tmpCatLength; j++) {
		var y=document.createElement('option');
		y.text=eval("jArrayTemplate"+cat)[j];
		y.value=eval("jArrayTemplate"+cat+"Value")[j];
		
		try {
			x.add(y,null); // standards compliant
		}
		catch(ex) {
			x.add(y); // IE only
		}
	}
}

function previewNotice(studentIdx){
	var templateID = document.getElementById("SelectNotice"+studentIdx).value;
	var studentName = '';
	if(studentIdx!=0) {
		studentName = document.getElementById("StudentName"+studentIdx).value;
	}
	var additionInfo = document.getElementById("TextAdditionalInfo"+studentIdx).value;
	newWindow("preview_notice.php?tid="+templateID+"&pv_studentname="+studentName+"&pv_additionalinfo="+additionInfo);
}

function applyToAll(studentCount){
	for(var i=1; i<=studentCount; i++) {
		changeCat(document.getElementById('CategoryID0').value, i);
		document.getElementById('CategoryID'+i).value = document.getElementById('CategoryID0').value;
		document.getElementById('SelectNotice'+i).value = document.getElementById('SelectNotice0').value;
		document.getElementById('TextAdditionalInfo'+i).value = document.getElementById('TextAdditionalInfo0').value;
		
		<?php if($canSendPushMsg){ ?>
			if(document.getElementById('eClassAppNotify'+i)){
				document.getElementById('eClassAppNotify'+i).checked = document.getElementById('eClassAppNotify0').checked;
			}
		<?php } ?>
		
		document.getElementById('emailNotify'+i).checked = document.getElementById('emailNotify0').checked;
	}
}

function showHideNoticeDiv()
{
	if(document.getElementById("CheckENotice").checked)
	{
		showDiv('divENotice');
	}
	else
	{
		hideDiv('divENotice');
	}
}

function showDiv(target_div)
{
	document.getElementById(target_div).style.display="inline";
}

function hideDiv(target_div)
{
	document.getElementById(target_div).style.display="none";
}

function checkForm(){
	if(document.getElementById('CheckENotice'))
	{
		if (document.getElementById('CheckENotice').checked) 
		{
			for(var i=1; i<=<?=sizeof($RecordID)?>; i++){
				if (document.getElementById('SelectNotice'+i).value=='0') {
					document.getElementById('submit_btn').disabled = false;
					document.getElementById('submit_btn').className = "formbutton_v30 print_hide";
					alert('<?=$i_alert_pleaseselect.$i_Discipline_Template?>');
					return false;
				}
			}
			
			var obj = document.form1;
			obj.action = "send_notice.php";
			obj.method = "post";
			obj.submit();
		}
		else 
		{
			//window.location = "student_record.php?rid=<?=$rid?>";
			window.location = "calendar.php?rid=<?=$rid?>";
		}
	}
	else {
		//window.location = "student_record.php?rid=<?=$rid?>";
		window.location = "calendar.php?rid=<?=$rid?>";
	}
}

function retriveAbsentDetails(AbsentRecordID)
{
	$("#AbsentDetails").hide();
	$("#AbsentDetails").html('');
	$.post(
		"ajax_retrive_absent_details.php",
		{
			"RecordID":AbsentRecordID
		},
		function(responseText){
			objAbsentRecord = "AbsentRecord_"+AbsentRecordID;
			var objPos = $("#"+objAbsentRecord).offset();
			$("#AbsentDetails").html(responseText);
			$("#AbsentDetails").css( { "left": (objPos.left + 20) + "px", "top":objPos.top + "px" } );
			$("#AbsentDetails").show();
		}
	);
}

function setClickID(id) {
	document.getElementById('clickID').value = id;	
}

var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_content.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

var callback_homework_type_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function Hide_Window(pos){
	document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	
	return pos_value;
}

function DisplayPosition(){
	document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
	document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
	document.getElementById('ref_list').style.visibility='visible';
}
</script>

<br />
<form name="form1" method="post" action="new3.php">
<table width="97%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation">
			<?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?>
			<?=$StepTable?>
		</td>
	</tr>
</table>

<table width="88%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="left"><?=$navigation?>&nbsp;</td>
				<td align="right"><?=$SysMsg?>&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td>
			<?=$li->display();?>
			<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>
		</td>
	</tr>
<? 	# check template is ava
	if (!$lnotice->disabled && !empty($NoticeTemplateAva))
	{
?>
	<tr>
		<td align="left">
			<input name="CheckENotice" type="checkbox" id="CheckENotice" <?=$defaultCheckNotice?> onClick="javascript:showHideNoticeDiv()" /><label for="CheckENotice"><?=$i_Discipline_Send_Notice_Via_ENotice?></label><br>
			
			<div id="divENotice" <?=$NoticeDivDisplay?>>
			<fieldset class="form_sub_option">
			<?
			if(sizeof($RecordID) > 1) 
			{
			?>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
					<tr>
						<td colspan="2" valign="top" nowrap="nowrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle"><span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span></td>
									<td align="right"><span class="sectiontitle">
										<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyToAll(".sizeof($RecordID).");")?>&nbsp;
										<?=$linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice(0);")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
					
					<tr valign="top">
						<td width="20%" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Template?> <span class="tabletextrequire">*</span></span></td>
						<td>
							<?=$catSelection0?> <select name="SelectNotice0" id="SelectNotice0"><option value="0"><?="-- $button_select --"?></option></select>
						</td>
					</tr>
					
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Additional_Info?></td>
						<td valign="top"><textarea name="TextAdditionalInfo0" id="TextAdditionalInfo0" rows="2" wrap="virtual" class="textboxtext"></textarea></td>
					</tr>
					
					<?php if($canSendPushMsg) { ?>
						<tr valign="top" nowrap="nowrap">
							<td valign="top">&nbsp;</td>
							<td valign="top"><input type="checkbox" name="eClassAppNotify0" id="eClassAppNotify0" value="1" checked><label for="eClassAppNotify0"><?=$Lang['eDiscipline']['Detention']['SendNotification']?></label></td>
						</tr>
					<?php } ?>
					
					<tr>
						<td valign="top" nowrap="nowrap">&nbsp;</td>
						<td valign="top"><input type='checkbox' name='emailNotify0' id='emailNotify0' value='1' checked><label for='emailNotify0'><?=$Lang['eDiscipline']['EmailNotifyParent']?></label></td>
					</tr>
				</table>
			<? } ?>
				<br />
				<?=$TableNotice?>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
					<tr>
						<td valign="top" nowrap="nowrap"><span class="tabletextremark"><?=$i_general_required_field?></span></td>
						<td width="80%">&nbsp;</td>
					</tr>
				</table>
			</fieldset>
			</div>
		</td>
	</tr>
<? } ?>

<? if ($lnotice->disabled) { ?>
	<tr><td><input name="CheckENotice" type="checkbox" id="CheckENotice" disabled><?=$i_Discipline_Send_Notice_Via_ENotice?></td></tr>
	<tr><td>(<?=$eDiscipline["EnoticeDisabledMsg"]?>)</td></tr>
<? } ?>

</table>

<table width="88%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td height="1" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1"></td>
</tr>
<tr>
	<td align="right">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_finish, "button", "this.disabled=true; this.className='formbutton_disable_v30 print_hide'; return checkForm();", "submit_btn")?>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="task" value="" />
<input type="hidden" name="clickID" value="" />
<input type="hidden" name="rid" id="rid" value="<?=$rid?>" />
<input type="hidden" name="student" id="student" value="<?=implode(",", (array)$student)?>" />
<input type="hidden" name="recordCount" id="recordCount" value="<?=sizeof($RecordID)?>" />
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="from" id="from" value="calendar" />
</form>
<br />

<?
if (!$lnotice->disabled && !empty($NoticeTemplateAva))
{
	print $linterface->FOCUS_ON_LOAD("form1.CheckENotice");
}

$linterface->LAYOUT_STOP();
?>