<?php
// Modifying by: 

#########################################
#
# 	Date:	2017-01-09	Bill	[2016-0808-1743-20240]
#			Cust: Handling for RecordID from Calendar
#
#	Date:	2014-02-21	YatWoon
#			Improved: Add a field to store the detention is auto assign or not [CaseP58788]
#
#	Date:	2013-11-01	YatWoon
#			Fixed: Incorrect sesstion full quota checking [Case#2013-1015-1251-10156]
#
#########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
else
{
$SuccessFlag = true;
$StudentNotInSessionFlag = true;
$SessionNotFullFlag = true;
$StudentNotTimeCrashFlag = true;

$allResult = true;
if (is_array($RecordID) && $RecordID[0] <> "") 
{
	for ($i=0; $i<sizeof($RecordID); $i++)
	{
		// [2016-0808-1743-20240] Special Handling - RecordID received from calendar view is StudentSessionID 
		if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'] && $isAssignFromCalendar)
		{
			// Get StudentID using StudentSessionID
			$sql = "SELECT StudentID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '".$RecordID[$i]."' ORDER BY StudentID";
			$StudentID = $ldiscipline->returnVector($sql);
			$StudentID = $StudentID[0];
			if(!$StudentID)		continue;
			
			$TmpSessionCount[0] = 1;
		}
		else
		{
			$StudentID = $RecordID[$i];
			$TmpSessionCount = $ldiscipline->getUnassignSessionCountByStudentID($StudentID);
		}
		
		for ($j=0; $j<$TmpSessionCount[0]; $j++)
		{
			$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $StudentID, "ASC");
			$currentTime = date('Y-m-d H:i:s');
			for ($k=0; $k<sizeof($result); $k++) 
			{
				$StudentNotInSessionFlag = true;
				$SessionNotFullFlag = true;
				$StudentNotTimeCrashFlag = true;
				list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $result[$k];
				
				$detentionSession = $TmpDetentionDate." ".$TmpStartTime;
				if($currentTime<$detentionSession) 
				{
					if ($ldiscipline->checkStudentInSession($StudentID, $TmpDetentionID)) {
						$StudentNotInSessionFlag = false;
					}
					if (is_array($FinalArray) && in_array_2col($StudentID, $TmpDetentionID, $FinalArray)) {
						$StudentNotInSessionFlag = false;
					}
					if($ldiscipline->checkStudentTimeCrash($StudentID, $TmpDetentionID, $TmpDetentionDate, $TmpStartTime, $TmpEndTime)) {
						$StudentNotTimeCrashFlag = false;	
					}
					if(!$TmpAvailable)
						$SessionNotFullFlag = false;
					
					/*
					if (is_array($FinalArray)) {
						$TmpDetentionIDCount = 0;
						for ($l=0; $l<sizeof($FinalArray); $l++) {
							if ($FinalArray[$l][1] == $TmpDetentionID) $TmpDetentionIDCount++;
						}
						if ($TmpAvailable <= $TmpDetentionIDCount) {
							$SessionNotFullFlag = false;
						}
					}
					*/
										
					if ($StudentNotInSessionFlag && $SessionNotFullFlag && $StudentNotTimeCrashFlag) 
					{
						$TmpIndex = sizeof($FinalArray);
						$FinalArray[$TmpIndex][0] = $StudentID;
						$FinalArray[$TmpIndex][1] = $TmpDetentionID;
						
						#####***** added by henry on 20090908 *****######
						unset($DetentionArr);
						
						// [2016-0808-1743-20240] Special Handling - RecordID received from calendar view is StudentSessionID 
						if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'] && $isAssignFromCalendar)
						{
							// Assign to detention session
							$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET DetentionID = '$TmpDetentionID', ArrangedBy = '".$_SESSION['UserID']."', ArrangedDate = NOW(), DateModified = NOW(), isAutoAssign='1', AutoAssignDateTime=NOW() ";
							$sql .= " WHERE RecordID = '".$RecordID[$i]."' AND StudentID = '".$StudentID."' AND DetentionID IS NULL AND RecordStatus = 1 LIMIT 1";
							$ldiscipline->db_db_query($sql);
							
							$sql = "SELECT RecordID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '".$RecordID[$i]."' AND StudentID = '".$StudentID."' AND DetentionID = '".$TmpDetentionID."' AND RecordStatus = 1";
							$result = $ldiscipline->returnVector($sql);
							$result = $result[0];
						}
						else
						{
							$DetentionArr['StudentID'] = $StudentID;
							$DetentionArr['DetentionID'] = $TmpDetentionID;
							$result = $ldiscipline->updateDetention($DetentionArr, 1);
						}
						$DetentionRecordIDArr[] = $result;
						if (!$result) $SuccessFlag = false;
						
						$allResult = $allResult && $SuccessFlag;
						#####***** end of henry's addition  *****######
						
						break;
					}
					else if ($k==sizeof($result)-1) {
						break 3;
					}
				}
			}
		}
	}
}

/* commented by henry on 20090908
$DetentionRecordIDArr = array();
$allResult = true;
if ($StudentNotInSessionFlag && $SessionNotFullFlag && is_array($FinalArray)) {
	for ($i=0; $i<sizeof($FinalArray); $i++) {

		$DetentionArr['StudentID'] = $FinalArray[$i][0];
		$DetentionArr['DetentionID'] = $FinalArray[$i][1];
		$result = $ldiscipline->updateDetention($DetentionArr);
		$DetentionRecordIDArr[] = $result;
		
		if (!$result) $SuccessFlag = false;
		
		$allResult = $allResult && $SuccessFlag;
	}
}
end of comment */

if ($StudentNotInSessionFlag && $SessionNotFullFlag && $StudentNotTimeCrashFlag) {
	/*if (!is_array($FinalArray)) {
		$SysMsg = "update_not_enough_session";
	} else */
	if ($SuccessFlag) {
		if ($allResult) {
			$SysMsg = "add";
		}
		else {
			$SysMsg = "add_failed";
		}
		# Go to step 3 to send eNotice if success
		if(sizeof($DetentionRecordIDArr)>0)
		{
			$RecordIDStr = implode(",", $DetentionRecordIDArr);
			$RedirectPage = "new3.php?msg=$SysMsg&rid=$RecordIDStr&from=auto_arrange";
			intranet_closedb();
			header("Location: $RedirectPage");
			exit();
		}
		else{
			// if there is no more session avaliable, return error msg
			$ReturnPage = "calendar.php";
			$SysMsg = "update_not_enough_session";
			intranet_closedb();
			header("Location: $ReturnPage?msg=$SysMsg");
			exit();
		}			
	}
	else {
		$SysMsg = "update_failed";
	}
}
else if($StudentNotTimeCrashFlag) {
	$SysMsg = "--";
}
else if ($StudentNotInSessionFlag) {
	$SysMsg = "update_not_enough_session";
}
else {
	$SysMsg = "update_already_in_session";
}

intranet_closedb();

$ReturnPage = "calendar.php";
header("Location: $ReturnPage?msg=$SysMsg");
exit();
}
?>