<?php
// Modifying by: henry chow

#################### Change Log #####################
## 
##	Date 	: 	20100202 (Ronald)
##	Details :	Modified the query by using left outer join to inner join when joining INTRANET_USER
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_Student_List, "student_record.php");
$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['AddPunishmentToSelectedStudent'], "");

$name_field = getNameFieldByLang("USR.");

$sql = "
	SELECT 
		RecordID,
		CONCAT(".Get_Lang_Selection("yc.ClassTitleB5","yc.ClassTitleEN").",'-',ycu.ClassNumber) as Class,
		$name_field as name,
		SES.Reason,
		LEFT(SES.DateInput,10) as RequestDate,	
		DET.DetentionDate,
		LEFT(DET.StartTime,5), 
		LEFT(DET.EndTime,5),
		DET.Location,
		DET.Vacancy,
		SES.DetentionID,
		SES.DemeritID,
		SES.StudentID
	FROM
		DISCIPLINE_DETENTION_STUDENT_SESSION SES LEFT OUTER JOIN
		DISCIPLINE_DETENTION_SESSION DET ON (DET.DetentionID=SES.DetentionID) INNER JOIN 
		INTRANET_USER USR ON (USR.UserID=SES.StudentID) INNER JOIN
		YEAR_CLASS_USER ycu ON (ycu.UserID=SES.StudentID) INNER JOIN
		YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)	
	WHERE
		yc.AcademicYearID=".Get_Current_Academic_Year_ID()." AND 
		SES.RecordID IN (".implode(',',$RecordID).")	
		";
$result = $ldiscipline->returnArray($sql,13);

$tableContent = "<table  class='common_table_list'>";
$tableContent .= "<tr class='tabletop'>";
$tableContent .= "<th width='1'>#</th>";
$tableContent .= "<th width='10%'>$i_general_class</th>";
$tableContent .= "<th width='15%'>$i_identity_student</th>";
$tableContent .= "<th width='15%'>$i_Discipline_Detention_Reason</th>";
$tableContent .= "<th width='10%'>$i_Discipline_Request_Date</th>";
$tableContent .= "<th width=''>$i_Discipline_Arranged_Session</th>";
$tableContent .= "<th width=''>".$Lang['General']['Error']."</th>";
$tableContent .= "</tr>";

$studentAry = array();
$html = "";
$count = 0;

for($i=0; $i<sizeof($result); $i++) {
	$error = "";
	
	list($recordID, $class, $name, $reason, $requestDate, $detentionDate, $startTime, $endTime, $location, $vacancy, $detentionID, $demeritID, $studentID) = $result[$i];
	
	if($detentionID!="") {
		$session = $detentionDate." | ".$startTime."-".$endTime." | ".$location." | ".$vacancy;
		$sql = "SELECT UserID FROM DISCIPLINE_DETENTION_SESSION_PIC WHERE DetentionID='$detentionID'";
		$pic = $ldiscipline->returnVector($sql);
		if(sizeof($pic)>0) {
			$picName = $ldiscipline->getPICNameList(implode(',',$pic));
			$session .= " | ".$picName;	
		}
	} else {
		$session = "---";	
	}	

	if($demeritID!="") $error .= "<li>".$Lang['eDiscipline']['DemeritRecordAddedAlready']."</li>";
	if(in_array($studentID, $studentAry))
		$error .= "<li>".$Lang['eDiscipline']['DuplicatedStudent']."</li>";
	else 
		$studentAry[] = $studentID;
	
	$css = ($error!="") ? "status_alert" : "";
	if($error=="") {
		$count++;
		$html .= "<input type='hidden' name='RecordID[]' id='RecordID[]' value='".$recordID."'>\n";
	}
	
	$tableContent .= "<tr class='$css'>";
	$tableContent .= "<td>".($i+1)."</td>";
	$tableContent .= "<td>$class&nbsp;</td>";
	$tableContent .= "<td>$name&nbsp;</td>";
	$tableContent .= "<td>$reason&nbsp;</td>";
	$tableContent .= "<td>$requestDate&nbsp;</td>";
	$tableContent .= "<td>$session</td>";
	$tableContent .= "<td>$error&nbsp;</td>";
	$tableContent .= "</tr>";
}
if(sizeof($result)==0) {
	$tableContent .= "<tr><td colspan='7' height='40' align='center'>$i_no_record_exists_msg</td></tr>";	
}
$tableContent .= "</table>";

$tableContent .= "<div align='center'><p>".$count.$Lang['eDiscipline']['DetentionRecordAddPunishment']."</p></div>";
$tableContent .= "<div class='edit_bottom'>";
if($count>0)
	$tableContent .= $linterface->GET_ACTION_BTN($button_continue,"submit","");
$tableContent .= "&nbsp;".$linterface->GET_ACTION_BTN($button_cancel,"button","self.location.href='student_record.php'");
$tableContent .= "</div>";
					

$linterface->LAYOUT_START();
?>
<script language="javascript">
	
</script>
<form name="form1" method="post" action="add_punishment_selected_student.php">
<table id="html_body_frame" width="98%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td class="navigation">
		<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?>
	</td>
</tr>
</table>
<?=$tableContent?>
<?=$html?>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>