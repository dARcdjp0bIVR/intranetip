<?php
// Modifying by: 

########### Change Log [Start] ###
#	Date:	2017-01-10  Carlos - $sys_custom['eDiscipline']['WFN_Reports'] added print take detention attendance button.
#
#	Date:	2015-09-24	Bill	[2015-0924-1046-51071]
#			added table column - Attendance Status & Attendance Remark
#
#	Date:	2015-04-23	Bill	[2014-1216-1347-28164]
#			add detention session type to Session Info
#
# 	Date:	2014-04-11	YatWoon
#		add ordering checking [Case#P60964]
#
#	Date:	2020-06-03 [YatWoon]
#			add "Craeted by" column
#
########### Change Log [End] ###

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");

$CurrentPage = "Management_Detention";

$linterface = new interface_html();

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_Calendar, "calendar.php");
$PAGE_NAVIGATION[] = array($i_Discipline_Detention_Details, "");

$SysMsg = $linterface->GET_SYS_MSG("$msg");

$student = $_POST['student'];
if (!is_array($student)) $student = explode(",", $student);


if (!isset($field)) $field = 3;		// default: sort by request date
if (!isset($order)) $order = 1;		// default: asc order

$pageSizeChangeEnabled = true;
if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("USR.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

$yearID = Get_Current_Academic_Year_ID();

$sql = "SELECT 
			CONCAT($clsName, '-', ycu.ClassNumber) AS Class, 
			$name_field AS Student, 
			SES.Reason, 
			SUBSTRING(SES.DateInput, 1, 10) AS DateInput,
			SES.PICID,
			SES.CreatedBy,
			'' AS Status, 
			SES.AttendanceStatus, 
			SES.AttendanceRemark,
			if(SES.AttendanceStatus is null,CONCAT('<input type=checkbox name=RecordID[] value=', SES.RecordID ,'>'),'') AS CheckBoxField, 
			SES.StudentID, 
			SES.DetentionID,
			SES.Remark,
			SES.RecordID
		FROM 
			DISCIPLINE_DETENTION_STUDENT_SESSION SES
			LEFT OUTER JOIN INTRANET_USER USR ON (SES.StudentID = USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 
			SES.DetentionID = '$did' AND 
			SES.RecordStatus = 1 AND 
			yc.AcademicYearID=$yearID";
			
if ($targetClass <> "All_Classes" && $targetClass <> "") $sql .= " AND USR.ClassName = '$targetClass'";
//$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", $name_field, "Reason", "DateInput", "Status", "CheckBoxField");
$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", $name_field, "Reason", "DateInput", "Status", "AttendanceStatus", "AttendanceRemark", "CheckBoxField");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $i_Discipline_Records;
$li->IsColOff = "eDisciplineDetentionDetail";
//$li->AllFormsData = $i_Discipline_All_Forms;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='2%' class='tabletoplink'>#</td>\n";
//$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_Discipline_Class)."</td>\n";
//$li->column_list .= "<td width='22%'>".$li->column($pos++, $i_Discipline_Student)."</td>\n";
//$li->column_list .= "<td width='23%'>".$li->column($pos++, $i_Discipline_Detention_Reason)."</td>\n";
//$li->column_list .= "<td width='15%' nowrap>".$li->column($pos++, $i_Discipline_Request_Date)."</td>\n";
//$li->column_list .= "<td width='20%' nowrap>". $i_Discipline_PIC ."</td>\n";
//$li->column_list .= "<td width='20%' nowrap>". $Lang['eDiscipline']['RecordCreatedBy'] ."</td>\n";
//$li->column_list .= "<td width='3%' nowrap>".$li->column($pos++, $i_Discipline_Status)."</td>\n";
$li->column_list .= "<td width='7%' nowrap>".$li->column($pos++, $i_Discipline_Class)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Discipline_Student)."</td>\n";
$li->column_list .= "<td width='20%'>".$li->column($pos++, $i_Discipline_Detention_Reason)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Request_Date)."</td>\n";
$li->column_list .= "<td width='12%' nowrap>". $i_Discipline_PIC ."</td>\n";
$li->column_list .= "<td width='12%' nowrap>". $Lang['eDiscipline']['RecordCreatedBy'] ."</td>\n";
$li->column_list .= "<td width='6%' nowrap>".$li->column($pos++, $i_Discipline_Status)."</td>\n";
$li->column_list .= "<td width='5%' nowrap>".$Lang['eDiscipline']['Detention']['AttendanceStatus']."</td>\n";
$li->column_list .= "<td width='9%' nowrap>".$Lang['eDiscipline']['Detention']['AttendanceRemark']."</td>\n";
$li->column_list .= "<td width='2%'>".$li->check("RecordID[]")."</td>\n";


$TakeAttendanceFlag = $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-TakeAttendance");
if ($TakeAttendanceFlag) {
	$TakeAttendanceStr = "<img width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_takeattendance.gif\" />\n";
	$TakeAttendanceStr .= "<a class=\"contenttool\" href=\"take_attendance.php?did=$did\">$i_Discipline_Take_Attendance</a>\n";
} else {
	$TakeAttendanceStr = "&nbsp;";
}

if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['Detention_Print'])
	$friend_print = "<a class=\"contenttool\" href=\"friendly_print_studentList.php?did=$did\" target=\"_blank\"><img width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" src=\"$image_path/$LAYOUT_SKIN/icon_print.gif\" />$i_PrinterFriendlyPage</a>\n";
	

$linterface->LAYOUT_START();

// [2014-1216-1347-28164] get detention content with session type
$TempList = $ldiscipline->getDetentionListByDetentionID($did, ($sys_custom['eDiscipline']['CreativeSchDetentionCust']?true:false));
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	// [2014-1216-1347-28164] get detention session type
	list($TempType,$TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
} else {
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
}

if ($TmpForm == $TempAllForms) $TmpForm = $ldiscipline->AllFormsData;

$select_class = "<SELECT name=\"targetClass\" id=\"targetClass\" onChange=\"this.form.submit()\">\n";
$select_class .= "<OPTION value='All_Classes' ".($targetClass=="All_Classes"?"SELECTED":"").">$i_Discipline_All_Classes</OPTION>\n";
$resultClass = $ldiscipline->getClassByDetentionID($did);
for ($i=0; $i<sizeof($resultClass); $i++) {
	$ClassName = $resultClass[$i];
	$select_class .= "<OPTION value='$ClassName' ".($targetClass==$ClassName?"SELECTED":"").">$ClassName</OPTION>\n";
}
$select_class .= "</SELECT>\n";


$exportBtn = $linterface->GET_LNK_EXPORT("detail_export.php?did=$did&field=$field&order=$order", $button_export,"","","",0);
?>

<script language="javascript">
function retriveAbsentDetails(AbsentRecordID){
	$("#AbsentDetails").hide();
	$("#AbsentDetails").html('');
	$.post(
		"ajax_retrive_absent_details.php",
		{
			"RecordID":AbsentRecordID
		}
		,function(responseText){
			objAbsentRecord = "AbsentRecord_"+AbsentRecordID;
			var objPos = $("#"+objAbsentRecord).offset();
			$("#AbsentDetails").html(responseText);
			$("#AbsentDetails").css( { "left": (objPos.left + 20) + "px", "top":objPos.top + "px" } );
			$("#AbsentDetails").show();
		}
	);
}
	
function setClickID(id) {
	document.getElementById('clickID').value = id;	
}

var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_content.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

var callback_homework_type_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}


function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
  
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left= (getPosition(document.getElementById(ClickID),'offsetLeft') +10)+'px';
  document.getElementById('ref_list').style.top= getPosition(document.getElementById(ClickID),'offsetTop')+'px';
  document.getElementById('ref_list').style.visibility='visible';
}

function cancelArrangeCheck() {
	if (countChecked(document.form1, 'RecordID[]') == 0) {
		alert(globalAlertMsg2);
		return false;
	} else {
		if(confirm("<?=$i_Discipline_Cancel_Arrange_Alert?>")){
			document.form1.action="cancel_arrange.php";
			document.form1.submit();
		} else {
			return false;
		}
	}
}

</script>

<br />
<form name="form1" method="post" action="detail.php">
<table width="95%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?></td>
	</tr>
</table>
<table width="95%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="2" border="0" width="300">
					<tbody>
						<tr>
							<td>
								<?php
								if($sys_custom['eDiscipline']['WFN_Reports']){
									echo $linterface->GET_LNK_PRINT("print_detention_list.php?did=".$did."&field=".$field."&order=".$order,$Lang['eDiscipline']['TWGHWFNS_StudentDetention']['PrintTakeDetentionAttendanceSheet'],'',' target="_blank" ','',0);
								}
								?>
								<div class="Conntent_tool">
								<a class="print" href="detail_print.php?did=<?=$did?>&field=<?=$field?>&order=<?=$order?>" target="_blank">
									<?=$button_print?>
								</a>
								</div>&nbsp;
								<?=$exportBtn?>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td align="right">&nbsp;</td>
		</tr>
	</tbody>
</table>
<table width="95%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<span class="tabletextremark"><img width="20" height="20" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif"/><span class="sectiontitle"><?=$i_Discipline_Session_Info?></span></span>
			</td>
		</tr>
	</tbody>
</table>
<table width="95%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7" height="7">
				<img width="7" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_01_off.gif"/></td>
			<td height="7" background="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_02_off.gif">
				<img width="7" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_02_off.gif"/></td>
			<td width="7" height="7">
				<img width="9" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_03_off.gif"/></td>
		</tr>
		<tr>
			<td width="7" background="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_04_off.gif">
				<img width="7" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_04_off.gif"/></td>
			<td background="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_05_off.gif" align="right">
				<table width="100%" cellspacing="5" cellpadding="5" border="0">
					<tbody>
					
						<?php if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){ ?>
							<tr>
								<td width="20%" valign="top"><?=$Lang['eDiscipline']['DetentionTypes']['Title']?></td>
								<td valign="top" bgcolor="#ffffff"><?=$TempType?></td>
							</tr>
						<?php } ?>
						
						<tr>
							<td width="20%" valign="top"><?=$i_Discipline_Location?></td>
							<td valign="top" bgcolor="#ffffff"><?=intranet_htmlspecialchars($TmpLocation)?></td>
						</tr>
						<tr>
							<td valign="top"><?=$i_Discipline_Duty_Teacher?></td>
							<td valign="top" bgcolor="#ffffff"><?=$TmpPIC?></td>
						</tr>
						<tr>
							<td valign="top"><?=$i_Discipline_Date?></td>
							<td valign="top" bgcolor="#ffffff"><?=$TmpDetentionDate?></td>
						</tr>
						<tr>
							<td valign="top"><?=$i_Discipline_Time?></td>
							<td valign="top" bgcolor="#ffffff"><?=$TmpPeriod?></td>
						</tr>
						<tr>
							<td valign="top"><?=$i_Discipline_Vacancy?></td>
							<td valign="top" bgcolor="#ffffff"><?=$TmpVacancy?></td>
						</tr>
						<tr>
							<td valign="top"><?=$i_Discipline_Students_Attending?></td>
							<td valign="top" bgcolor="#ffffff"><?=$TmpAssigned?></td>
						</tr>
					</tbody>
				</table>
				<br />
			</td>
			<td width="7" background="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_06_off.gif">
				<img width="9" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_06_off.gif"/></td>
		</tr>
		<tr>
			<td width="7" height="7">
				<img width="7" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_07_off.gif"/></td>
			<td height="7" background="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_08_off.gif">
				<img width="9" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_08_off.gif"/></td>
			<td width="7" height="7">
				<img width="9" height="7" src="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_09_off.gif"/></td>
		</tr>
	</tbody>
</table>
<br />
<table width="95%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<span class="tabletextremark">
					<img width="20" height="20" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif"/>
					<span class="sectiontitle"><?=$i_Discipline_Student_List?></span>
				</span>
			</td>
		</tr>
	</tbody>
</table>
<table width="95%" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr>
							<td>
								<table cellspacing="0" cellpadding="2" border="0">
									<tbody>
										<tr>
											<td>
												<?=$TakeAttendanceStr?>
												&nbsp;<?=$friend_print?>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
							<td align="right"><?=$SysMsg?>&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<table width="100%" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr>
							<td>
								<?=$select_class?>
							</td>
							<td valign="bottom" align="right">
								<table cellspacing="0" cellpadding="0" border="0">
									<tbody>
										<tr>
											<td width="21"><img width="21" height="23" src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_01.gif"/></td>
											<td background="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_02.gif">
												<table cellspacing="0" cellpadding="2" border="0">
													<tbody>
														<tr>
															<td><img width="5" src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif"/></td>
															<td nowrap="">
																<a class="tabletool" href="#" onClick="javascript:cancelArrangeCheck();">
																	<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/><?=$i_Discipline_Cancel_Arrangement?></a>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
											<td width="6"><img width="6" height="23" src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_03.gif"/></td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<?=$li->display();?>
			</td>
		</tr>
	</tbody>
</table>
<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>

<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="clickID" id="clickID" value="" />
<input type="hidden" name="did" id="did" value="<?=$did?>" />
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="cpage" id="cpage" value="detail" />
</form>
<br />
<?
print $linterface->FOCUS_ON_LOAD("form1.DetentionCount0");
$linterface->LAYOUT_STOP();
?>