<?php

##### Change Log [Start] #####
#
#	Date	:	2017-02-15	Bill	[2016-0808-1743-20240]
#				create file		($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'])
#				return remark layer for homework related detention
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if($flag=="record_remark") {
	$tempLayer = $ldiscipline->getDetentionHWRemarkLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");
}
echo $tempLayer;

intranet_closedb();
?>