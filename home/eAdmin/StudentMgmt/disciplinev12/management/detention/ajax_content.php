<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();


if($task == 'remark') {
	
	$sql = "SELECT Remark FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID='$clickID'";
	$thisRemarkArr = $ldiscipline->returnVector($sql);
	$thisRemark = nl2br($thisRemarkArr[0]);
	//$data .= "<div id='show_detail".$id."' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'>";
	$data .= "<table width='300' border='0' cellpadding='0' cellspacing='0' class='layer_table'>";
	$data .= "<tr><td height='19'>";
	$data .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$data .= "<tr>";
	$data .= "<td width='5' height='19'><img src='$image_path/$LAYOUT_SKIN/can_board_01.gif' width='5' height='19'></td>";
	$data .= "<td height='19' valign='middle' background='$image_path/$LAYOUT_SKIN/can_board_02.gif'></td>";
	$data .= "<td width='19' height='19'><a href=\"javascript:Hide_Window('ref_list')\"><img src='$image_path/$LAYOUT_SKIN/can_board_close_off.gif' name='pre_close21' width='19' height='19' border='0' id='pre_close21' onMouseOver=\"MM_swapImage('pre_close21','','$image_path/$LAYOUT_SKIN/can_board_close_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a></td>";
	$data .= "</tr></table></td></tr><tr><td>";
	$data .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$data .= "<tr>";
	$data .= "<td width='5' background='$image_path/$LAYOUT_SKIN/can_board_04.gif'><img src='$image_path/$LAYOUT_SKIN/can_board_04.gif' width='5' height='19'></td>";
	$data .= "<td bgcolor='#FFFFF7'><div style='height:150px; overflow:auto;'>";
	
	$data .= "<table width='98%' border='0' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC' class='layer_table_detail'>";
		$data .= "<tr class='tabletop'>";
		$data .= "<td align='left'>".$iDiscipline['Remarks']."</td>";
		$data .= "</tr>";
	
		$data .= "<tr class='row_approved'>";
		$data .= "<td class='tabletext'>".$thisRemark."</td>";
		$data .= "</tr>";
	$data .= "</table>";
	
	$data .= "</div><br></td>";
	$data .= "<td width='6' background='$image_path/$LAYOUT_SKIN/can_board_06.gif'><img src='$image_path/$LAYOUT_SKIN/can_board_06.gif' width='6' height='6'></td>";
	$data .= "</tr><tr>";
	$data .= "<td width='5' height='6'><img src='$image_path/$LAYOUT_SKIN/can_board_07.gif' width='5' height='6'></td>";
	$data .= "<td height='6' background='$image_path/$LAYOUT_SKIN/can_board_08.gif'><img src='$image_path/$LAYOUT_SKIN/can_board_08.gif' width='5' height='6'></td>";
	$data .= "<td width='6' height='6'><img src='$image_path/$LAYOUT_SKIN/can_board_09.gif' width='6' height='6'></td>";
	$data .= "</tr></table></td></tr></table>";
}
else if($task="demerit") {
	$DemeritType['0'] = $i_Merit_Warning;
	$DemeritType['-1'] = $i_Merit_BlackMark;
	$DemeritType['-2'] = $i_Merit_MinorDemerit;
	$DemeritType['-3'] = $i_Merit_MajorDemerit;
	$DemeritType['-4'] = $i_Merit_SuperDemerit;
	$DemeritType['-5'] = $i_Merit_UltraDemerit;
	
	
	$data .= "<table width='300' border='0' cellpadding='0' cellspacing='0' class='layer_table'>";
	$data .= "<tr><td height='19'>";
	$data .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$data .= "<tr>";
	$data .= "<td width='5' height='19'><img src='$image_path/$LAYOUT_SKIN/can_board_01.gif' width='5' height='19'></td>";
	$data .= "<td height='19' valign='middle' background='$image_path/$LAYOUT_SKIN/can_board_02.gif'></td>";
	$data .= "<td width='19' height='19'><a href=\"javascript:Hide_Window('ref_list')\"><img src='$image_path/$LAYOUT_SKIN/can_board_close_off.gif' name='pre_close21' width='19' height='19' border='0' id='pre_close21' onMouseOver=\"MM_swapImage('pre_close21','','$image_path/$LAYOUT_SKIN/can_board_close_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a></td>";
	$data .= "</tr></table></td></tr><tr><td>";
	$data .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$data .= "<tr>";
	$data .= "<td width='5' background='$image_path/$LAYOUT_SKIN/can_board_04.gif'><img src='$image_path/$LAYOUT_SKIN/can_board_04.gif' width='5' height='19'></td>";
	$data .= "<td bgcolor='#FFFFF7'><div style='height:150px; overflow:auto;'>";
	
	$record = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($DemeritID);
		
	$data .= "<table width='98%' border='0' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC' class='layer_table_detail'>";
		$data .= "<tr class='tabletop'>";
		$data .= "<td align='left'>".$i_Discipline_Reason."</td>";
		$data .= "<td align='left'>".$i_Discipline_Reason2."</td>";
		$data .= "<td align='left'>".$iDiscipline['RecordDate']."</td>";
		$data .= "</tr>";
	
		$data .= "<tr class='row_approved'>";
		$data .= "<td class='tabletext'>".$record[0]['ItemText']."</td>";
		$data .= "<td class='tabletext'>".$ldiscipline->returnDeMeritStringWithNumber($record[0]['ProfileMeritCount'],$DemeritType[$record[0]['ProfileMeritType']])."</td>";
		$data .= "<td class='tabletext'>".$record[0]['RecordDate']."</td>";
		$data .= "</tr>";
	$data .= "</table>";
	
	$data .= "</div><br></td>";
	$data .= "<td width='6' background='$image_path/$LAYOUT_SKIN/can_board_06.gif'><img src='$image_path/$LAYOUT_SKIN/can_board_06.gif' width='6' height='6'></td>";
	$data .= "</tr><tr>";
	$data .= "<td width='5' height='6'><img src='$image_path/$LAYOUT_SKIN/can_board_07.gif' width='5' height='6'></td>";
	$data .= "<td height='6' background='$image_path/$LAYOUT_SKIN/can_board_08.gif'><img src='$image_path/$LAYOUT_SKIN/can_board_08.gif' width='5' height='6'></td>";
	$data .= "<td width='6' height='6'><img src='$image_path/$LAYOUT_SKIN/can_board_09.gif' width='6' height='6'></td>";
	$data .= "</tr></table></td></tr></table>";
	
}


intranet_closedb();
echo $data;
?>