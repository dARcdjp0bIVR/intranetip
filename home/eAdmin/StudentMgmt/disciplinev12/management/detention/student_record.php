<?php
// Modifying by: 

#################### Change Log #####################
## 
##	Date 	: 	2015-04-30 (Bill)	[2014-1216-1347-28164]
##	Details	:	add content tool to send notification for multiple detention records
##
##	Date 	: 	2011-12-19 (Henry Chow)
##	Details :	commented js checking on "more than 10 students"
## 
##	Date 	: 	2011-03-22 (Henry Chow)
##	Details :	add "Academic Year" filter
## 
##	Date 	: 	20100202 (Ronald)
##	Details :	Modified the query by using left outer join to inner join when joining INTRANET_USER
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$SysMsg = $linterface->GET_SYS_MSG("$msg");

if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New")) {
	$menuBar1 = $linterface->GET_LNK_NEW("new.php", $button_new,"","","",0);
	$menuBar2 = $linterface->GET_LNK_IMPORT("import.php", $button_import,"","","",0);
	$menuBar = "<td>{$menuBar1}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
	$menuBar .= "<td>{$menuBar2}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
}

$menuBar3 = $linterface->GET_LNK_EXPORT("student_export.php?AcademicYearID=$AcademicYearID&targetClass=$targetClass&targetStudent=$targetStudent&targetStatus=$targetStatus&searchString=$searchString", $button_export,"","","",0);
$menuBar .= "<td>{$menuBar3}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";

if($sys_custom['Discipline_Detention_Export_Semester'])
{
	$menuBar4 = $linterface->GET_LNK_IMPORT("student_export_semester.php?targetClass=$targetClass&targetStudent=$targetStudent&targetStatus=$targetStatus&searchString=$searchString", $button_export." ".$i_SettingsSemester,"","","",0);
	$menuBar .= "<td>".$menuBar4."</td>";
}

if ($cmonth == "") $cmonth = date("Ym");
$CurrentYear = substr($cmonth, 0, 4);
$CurrentMonth = substr($cmonth, 4, 2);
$NoOfDays = date("t", mktime(0,0,0,$CurrentMonth,1,$CurrentYear));
$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());

// Get Distinct Class
$AllClass = $ldiscipline->getClassWithSession();


if (!isset($field)) $field = 3;		// default: sort by request date
if (!isset($order)) $order = 0;		// default: desc order
$pageSizeChangeEnabled = true;
if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$name_field = getNameFieldByLang("USR.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

$conds = "";

if(isset($AcademicYearID) && $AcademicYearID!="") {
	$startDate = getStartDateOfAcademicYear($AcademicYearID);
	$endDate = getEndDateOfAcademicYear($AcademicYearID);
	$conds .= " AND (SES.DateInput BETWEEN '$startDate' AND '$endDate')";	
}

if ($sid != "") {
	$conds .= " AND SES.StudentID = '$sid'";
}

if ($targetClass != "All_Classes" && $targetClass != "") {
	$conds .= " AND (yc.ClassTitleEN = '$targetClass' OR yc.ClassTitleB5 = '$targetClass')";
}

if ($targetStudent == "Assigned") {
	$conds .= " AND SES.DetentionID IS NOT NULL";
} else if ($targetStudent == "Not_Assigned") {
	$conds .= " AND SES.DetentionID IS NULL";
}

if ($targetStatus == "Detented") {
	$conds .= " AND SES.AttendanceStatus = 'PRE'";
} else if ($targetStatus == "Absent") {
	$conds .= " AND SES.AttendanceStatus = 'ABS'";
}

$yearID = Get_Current_Academic_Year_ID();

$searchString = addslashes(trim($searchString));
if ($searchString != "") {
	$conds .= " AND (CONCAT($clsName, '-', ycu.ClassNumber) LIKE '%$searchString%' OR $name_field LIKE '%$searchString%' OR SES.Reason LIKE '%$searchString%' OR SES.Remark LIKE '%$searchString%')";
}
if($rid!='')
{
	$conds .= " and SES.RecordID in ($rid)";
}
if(isset($targetDemerit) && $targetDemerit=="WithDemerit")
	$conds .= " AND SES.DemeritID IS NOT NULL";
else if(isset($targetDemerit) && $targetDemerit=="WithoutDemerit")
	$conds .= " AND SES.DemeritID IS NULL";

### Note: Changed to use from left outer join to inner join ###
$teacher_name_field = getNameFieldByLang("USR2.");

$sql = "SELECT
			CONCAT($clsName, '-', ycu.ClassNumber) AS Class,
			$name_field AS Student,
			SES.Reason,
			SUBSTRING(SES.DateInput,1,10) AS DateInput,
			SES.PICID,
			SES.CreatedBy,
			SES.DetentionID,
			SES.AttendanceStatus,
			CONCAT('<input type=checkbox name=RecordID[] value=', SES.RecordID ,'>') AS CheckBoxField,
			SES.StudentID,
			CONCAT(D.DetentionDate, D.StartTime, D.EndTime, D.Location) AS TempForSort,
			SES.RecordID,
			SES.RelatedTo,
			SES.Remark,
			SES.DemeritID
		FROM
			DISCIPLINE_DETENTION_STUDENT_SESSION SES
			LEFT JOIN DISCIPLINE_DETENTION_SESSION D ON (D.DetentionID=SES.DetentionID)
			INNER JOIN INTRANET_USER USR ON (SES.StudentID = USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID)
		WHERE 1 AND SES.RecordStatus = 1 AND yc.AcademicYearID=$yearID ".$conds."
		GROUP BY SES.RecordID";


$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", "Student", "Reason", "SES.DateInput", "TempForSort", "AttendanceStatus", "CheckBoxField");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $i_Discipline_Records;
$li->column_array = array(14,14,14,14,14,14,14,14,14);
$li->IsColOff = "eDisciplineDetentionStudentRecord";
$li->AllFormsData = $i_Discipline_All_Forms;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='3%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Discipline_Class)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $i_Discipline_Student)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_Detention_Reason)."</td>\n";
$li->column_list .= "<td width='8%'>".$li->column($pos++, $i_Discipline_Request_Date)."</td>\n";
$li->column_list .= "<td width='9%'>". $i_Discipline_PIC."</td>\n";
$li->column_list .= "<td width='9%'>". $Lang['eDiscipline']['RecordCreatedBy'] ."</td>\n";
$li->column_list .= "<td width='41%'>".$li->column($pos++, $i_Discipline_Arranged_Session)."</td>\n";
$li->column_list .= "<td width='6%'>".$i_Discipline_Attendance."</td>\n";
$li->column_list .= "<td width='3%'>".$li->check("RecordID[]")."</td>\n";


# filters
/*
if(!isset($AcademicYearID) || $$AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();
*/
$allYearsInfo = GetAllAcademicYearInfo();

$years = array();
for($i=0, $i_max=sizeof($allYearsInfo); $i<$i_max; $i++) {
	$years[] = array($allYearsInfo[$i]['AcademicYearID'], Get_Lang_Selection($allYearsInfo[$i]['YearNameB5'],$allYearsInfo[$i]['YearNameEN']));	
}
$select_year = getSelectByArray($years, 'name="AcademicYearID" id="AcademicYearID" onChange="this.form.submit()"', $AcademicYearID, 0, 0, $i_Discipline_System_Award_Punishment_All_School_Year);

$select_class = "<SELECT name=\"targetClass\" id=\"targetClass\" onChange=\"this.form.submit()\">\n";
$select_class .= "<OPTION value='All_Classes' ".($targetClass=="All_Classes"?"SELECTED":"").">$i_Discipline_All_Classes</OPTION>\n";

for ($i=0; $i<sizeof($AllClass); $i++) {
	$ClassName = $AllClass[$i];
	$select_class .= "<OPTION value='$ClassName' ".($targetClass==$ClassName?"SELECTED":"").">$ClassName</OPTION>\n";
}
$select_class .= "</SELECT>\n";

$select_student = "<SELECT name=\"targetStudent\" id=\"targetStudent\" onChange=\"this.form.submit()\">\n";
$select_student .= "<OPTION value='All_Students' ".($targetStudent=="All_Students"?"SELECTED":"").">$i_Discipline_All_Students</OPTION>\n";
$select_student .= "<OPTION value='Assigned' ".($targetStudent=="Assigned"?"SELECTED":"").">$i_Discipline_Assigned</OPTION>\n";
$select_student .= "<OPTION value='Not_Assigned' ".($targetStudent=="Not_Assigned"?"SELECTED":"").">$i_Discipline_Not_Assigned</OPTION>\n";
$select_student .= "</SELECT>\n";

$select_status = "<SELECT name=\"targetStatus\" id=\"targetStatus\" onChange=\"this.form.submit()\">\n";
$select_status .= "<OPTION value='All_Status' ".($targetStatus=="All_Status"?"SELECTED":"").">$i_Discipline_All_Status</OPTION>\n";
$select_status .= "<OPTION value='Detented' ".($targetStatus=="Detented"?"SELECTED":"").">$i_Discipline_Detented</OPTION>\n";
$select_status .= "<OPTION value='Absent' ".($targetStatus=="Absent"?"SELECTED":"").">$i_Discipline_Absent</OPTION>\n";
$select_status .= "</SELECT>\n";

if($targetStatus=="Absent") {
	$select_demerit = "<SELECT name=\"targetDemerit\" id=\"targetDemerit\" onChange=\"this.form.submit()\">\n";	
	$select_demerit .= "<OPTION value='All_Demerit_Status' ".($targetDemerit=="All_Demerit_Status"?"SELECTED":"").">".$Lang['eDiscipline']['AllDemeritStatus']."</OPTION>\n";
	$select_demerit .= "<OPTION value='WithDemerit' ".($targetDemerit=="WithDemerit"?"SELECTED":"").">".$Lang['eDiscipline']['WithDemerit']."</OPTION>\n";
	$select_demerit .= "<OPTION value='WithoutDemerit' ".($targetDemerit=="WithoutDemerit"?"SELECTED":"").">".$Lang['eDiscipline']['WithoutDemerit']."</OPTION>\n";
	$select_demerit .= "</SELECT>\n";
}

$searchbar = "$select_year $select_class $select_student $select_status $select_demerit";

$linterface->LAYOUT_START();
?>

<script language='javascript'>


function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function retriveAbsentDetails(AbsentRecordID){
	$("#AbsentDetails").hide();
	$("#AbsentDetails").html('');
	$.post(
		"ajax_retrive_absent_details.php",
		{
			"RecordID":AbsentRecordID
		}
		,function(responseText){
			objAbsentRecord = "AbsentRecord_"+AbsentRecordID;
			var objPos = $("#"+objAbsentRecord).offset();
			$("#AbsentDetails").html(responseText);
			$("#AbsentDetails").css( { "left": (objPos.left + 20) + "px", "top":objPos.top + "px" } );
			$("#AbsentDetails").show();
		}
	);
}
	
function setClickID(id) {
	document.getElementById('clickID').value = id;	
}

var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition(0,0);
	}
}


function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_content.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

var callback_show_demerit_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition('-200','30');
	}
}


function show_demerit_list(type,click, demeritID)
{
	ClickID = click;
	document.getElementById('task').value = type;
	document.getElementById('DemeritID').value = demeritID;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_content.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_demerit_list);
}


var callback_homework_type_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition(0,0);
	}
}


function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
  
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(le, to){
	var moveLeft = (le != "undefined") ? parseInt(le) : 0;
	var moveTop = (to != "undefined") ? parseInt(to) : 0;

  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10+moveLeft;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop') + moveTop;
  document.getElementById('ref_list').style.visibility='visible';
}	
</script>

<div id="AbsentDetails" style="display:none; position:absolute; z-index:1;"></div>
<br />
<form name="form1" method="post" onSubmit="return checkForm();">

<script type="text/javascript">

	function arrangeCheck() {
		
		var tableObj = document.getElementById('tableStudentRecord');
		var cellContent;

		var len=document.form1.elements.length;
		
		for(var i=0; i<len; i++) {
			
			if (document.form1.elements[i].name=="RecordID[]" && document.form1.elements[i].checked) {

				//cellContent = tableObj.rows[i-6].cells[7].innerHTML;
				cellContent = $("tr#"+document.form1.elements[i].value).attr('class');
				
				//if (cellContent != "--") {
				if((cellContent == "row_suspend") || (cellContent == "row_waiting")){
					// continuous
				}else{
					alert('<?=$i_Discipline_Session_Arranged_Before?>');
					return false;
				}
			}
		}

		if (countChecked(document.form1, 'RecordID[]') == 0) {
			alert(globalAlertMsg2);
			return false;
		} else {
			//if(countChecked(document.form1, 'RecordID[]') > 10)
			//{
			//	alert("<?=$Lang['eDiscipline']['JSWarning']['PleaseDoNotSelectMoreThan10Records'];?>");
			//	return false;
			//} else {
				if(confirm("<?=$i_Discipline_Arrange_Alert?>")){
					document.form1.action="arrange_for_student.php";
					document.form1.submit();
				} else {
					return false;
				}
			//}
		}
	}

	function cancelArrangeCheck() {
		var tableObj = document.getElementById('tableStudentRecord');
		var cellContent;

		var len=document.form1.elements.length;
		for(var i=0; i<len; i++) {
			if (document.form1.elements[i].name=="RecordID[]" && document.form1.elements[i].checked) {
				
				//cellContent = tableObj.rows[i-6].cells[7].innerHTML;
				cellContent = $("#"+document.form1.elements[i].value).attr('class');
				
				//if (cellContent == "--") {
				if (cellContent == "row_waiting") {
					alert('<?=$i_Discipline_Session_Not_Arranged_Before?>');
					return false;
				}else if(cellContent == "row_approved record_Waived") {
					alert("<?=$Lang['eDiscipline']['JSWarning']['DetenedRecordCannotCancel']?>");
					return false;
				}else if(cellContent == "row_suspend") {
					alert("<?=$Lang['eDiscipline']['JSWarning']['AbsentRecordCannotCancel']?>");
					return false;
				}else{
					// continuous
				}
			}
		}

		if (countChecked(document.form1, 'RecordID[]') == 0) {
			alert(globalAlertMsg2);
			return false;
		} else {
			if(confirm("<?=$i_Discipline_Cancel_Arrange_Alert?>")){
				document.form1.action="cancel_arrange.php";
				document.form1.submit();
			} else {
				return false;
			}
		}
	}
	
	<?php if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){ ?>
		function sendMsgCheck(){
			if (countChecked(document.form1, 'RecordID[]') == 0) {
				alert(globalAlertMsg2);
				return false;
			}
			document.form1.action="arrange2.php?from=student_record&cust_arrange=1";
			document.form1.submit();
		}
	<?php } ?>
	
	function checkForm() {
		var tmpObj = document.getElementById('searchString');
		if(tmpObj.value=="") {
			alert("<?=$i_Discipline_System_Award_Punishment_Search_Alert?>");
			tmpObj.focus();
			return false;
		}
		return true;
	}
	
	function addPunishment()
	{
		if (countChecked(document.form1, 'RecordID[]') == 0) {
			alert(globalAlertMsg2);
			return false;
		} else {
			//document.form1.action="add_punishment_selected_student.php";
			document.form1.action="add_punishment_step2.php";
			document.form1.submit();
			
		}
		
		//window.location='../award_punishment/new2.php?student=<?=$AbsentStudentID?>';
// 		document.form1.action="add_punishment_selected_student.php";
// 		document.form1.submit();
	}
		
</script>
		
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr><td align="right" colspan="3"><?=$SysMsg?>&nbsp;</td></tr>
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<?=$menuBar?>
				</tr>
			</table>
		</td>
		<td align="right">
			<input name="searchString" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($searchString))?>" />
			<?=$linterface->GET_BTN($button_search, "submit", "");?>
		</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="28" align="right" valign="bottom" class="tabletextremark">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30" width="75%"><?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Detention_Student_Instruction_Msg) ?></td>
					<td align="right" valign="middle" class="thumb_list"><a href="calendar.php"><?=$i_Discipline_Calendar?></a> | <a href="list.php"><?=$i_Discipline_Session2?></a> | <span><?=$i_Discipline_Student?></span></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0" align="center">
	<tr class="table-action-bar">
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><?= $searchbar; ?></td>
				</tr>
			</table>
		</td>
		<td align="right" valign="bottom">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<!-- Add punishment for selected student(s)  //-->
								<? if ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New")) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:addPunishment();">
										<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/ediscipline/icon_demerit.gif"/> <?=$Lang['eDiscipline']['AddPunishmentToSelectedStudent']?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<? } ?>
															
								<!-- Arrange //-->
								<? if ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:arrangeCheck();">
										<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/> <?=$i_Discipline_Arrange?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<? } ?>
								
								<!-- Cancel Arrangement //-->
								<? if ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:cancelArrangeCheck();">
										<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/> <?=$i_Discipline_Cancel_Arrangement?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<? } ?>
								
								<!-- Send Notification //-->
								<? if ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent") && $plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:sendMsgCheck();">
										<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/> <?=$Lang['eDiscipline']['DetentionMgmt']['SendNotification']?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<? } ?>
								
								<!-- Edit //-->
								<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn")) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:checkEdit(document.form1,'RecordID[]','edit.php')">
										<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit?></a>
								</td>
								<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="5" /></td>
								<? } ?>
								
								<!-- Delete //-->
								<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-DeleteOwn")) { ?>
								<td nowrap>
									<a href="#" class="tabletool" onClick="javascript:checkRemove(document.form1,'RecordID[]','delete_update.php')">
										<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_delete?></a>
								</td>
								<? } ?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div id="ref_list" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>
<?php echo $li->display(); ?>
<br />

<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="clickID" id="clickID" value="" />
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="cpage" id="cpage" value="srec" />
<input type="hidden" name="DemeritID" id="DemeritID" value="" />
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
