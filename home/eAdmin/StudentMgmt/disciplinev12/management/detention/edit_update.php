<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn") && $ldiscipline->isDetentionSessionOwn($RecordID))))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
else
{
$RecordID = $_POST["RecordID"];
$Reason = $_POST["TextReason1"];
$Remark = $_POST["TextRemark1"];
if ($Reason == "") {
	$Reason = 'NULL';
} else {
	$Reason = "'".$Reason."'";
}
if ($Remark == "") {
	$Remark = 'NULL';
} else {
	$Remark = "'".$Remark."'";
}

$SuccessFlag = true;

$DetentionArr['Reason'] = $Reason;
$DetentionArr['Remark'] = $Remark;
$result = $ldiscipline->updateDetentionByRecordID($DetentionArr, $RecordID);
if (!$result) $SuccessFlag = false;

if ($SuccessFlag) {
	$SysMsg = "update";
} else {
	$SysMsg = "update_failed";
}

intranet_closedb();
$ReturnPage = "student_record.php?msg=$SysMsg";
header("Location: $ReturnPage");
}
?>