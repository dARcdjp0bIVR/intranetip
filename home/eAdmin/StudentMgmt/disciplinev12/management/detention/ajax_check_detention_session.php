<?php
// Using :

#################### Change Log #####################
##
##	Date    :	20190513 (Bill)
##	Details	:	Prevent SQL Injection
##
##	Date	:	20190326 (Bill)		[2019-0322-1117-36066]
##	Details	:	Fixed: overlapped sessions checking
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

## Get the current academic year id ##
$CurrAcademicYearID = Get_Current_Academic_Year_ID();

### Handle SQL Injection + XSS [START]
$arrRecordID = explode(",", IntegerSafe($TargetRecordID));
$arrTargetStudnetID = explode(",", IntegerSafe($TargetStudentID));
### Handle SQL Injection + XSS [END]

$ldiscipline->Start_Trans();

if($ldiscipline->MaxDetentionDayBeforeAllow != "" && $ldiscipline->MaxDetentionDayBeforeAllow != 0) {
	$allowedDetentionDate = " (DATE_SUB(CURDATE(), INTERVAL ".$ldiscipline->MaxDetentionDayBeforeAllow." DAY))";
	$conds .= " AND (DetentionDate >= $allowedDetentionDate)";
}
else {
	$conds .= " AND (DetentionDate > CURDATE() OR (DetentionDate = CURDATE()))";
}
$conds .= " AND (e.RecordStatus IS NULL OR e.RecordStatus = 0)";

for($i=0; $i<sizeof($arrTargetStudnetID); $i++)
{
	$SelectedStudentID = $arrTargetStudnetID[$i];
	
	## Get All Possible Avaliable detention session base on User
	$sql = "SELECT 
					a.DetentionID, 
					a.DetentionDate, 
					a.StartTime, 
					a.EndTime, 
					a.Location,
					a.Vacancy,
					e.RecordID
			FROM 
					DISCIPLINE_DETENTION_SESSION AS a INNER JOIN 
					DISCIPLINE_DETENTION_SESSION_CLASSLEVEL AS b ON (a.DetentionID = b.DetentionID) INNER JOIN
					YEAR_CLASS AS c ON (b.ClassLevelID = c.YearID) INNER JOIN
					YEAR_CLASS_USER AS d ON (c.YearClassID = d.YearClassID) LEFT OUTER JOIN
					DISCIPLINE_DETENTION_STUDENT_SESSION AS e ON (a.DetentionID = e.DetentionID AND e.StudentID = '$SelectedStudentID')
			WHERE 
					(e.RecordID IS NULL OR e.RecordStatus=0) AND 
					c.AcademicYearID = '$CurrAcademicYearID' AND 
					d.UserID = '$SelectedStudentID'
					$conds
			ORDER BY
					a.DetentionDate, a.StartTime ASC";
	$arr_avaliable_session = $ldiscipline->returnArray($sql,7);
	//echo $sql;
	//debug_pr($arr_avaliable_session);
	
	$AvaliableSessionID = array();
	if(sizeof($arr_avaliable_session) > 0)
	{
		for($a=0; $a<sizeof($arr_avaliable_session); $a++)
		{
			list($detention_id, $detention_date, $detention_start, $detention_end, $detention_location, $space_avaliable, $detention_record_id) = $arr_avaliable_session[$a];
			$AvaliableSessionID[] = $detention_id;
			
			$AvaliableSessionArray[$detention_id]['Date'] = $detention_date;
			$AvaliableSessionArray[$detention_id]['StartTime'] = $detention_start;
			$AvaliableSessionArray[$detention_id]['EndTime'] = $detention_end;
			$AvaliableSessionArray[$detention_id]['StartTimestamp'] = strtotime($detention_date." ".$detention_start);
			$AvaliableSessionArray[$detention_id]['EndTimestamp'] = strtotime($detention_date." ".$detention_end);
			$AvaliableSessionArray[$detention_id]['Location'] = $detention_location;
			$AvaliableSessionArray[$detention_id]['Vacancy'] = $space_avaliable;
		}
	}
	//debug_pr($AvaliableSessionArray);
	
	## Get assigned detention session based on User
	$sql = "SELECT 
					a.DetentionID,
					a.DetentionDate, 
					a.StartTime, 
					a.EndTime, 
					a.Location 
			FROM 
					DISCIPLINE_DETENTION_SESSION AS a INNER JOIN 
					DISCIPLINE_DETENTION_STUDENT_SESSION AS b ON (a.DetentionID = b.DetentionID) 
			WHERE 
					b.RecordStatus = 1 AND 
					b.StudentID = '$SelectedStudentID' AND 
					(a.DetentionDate > CURDATE() OR (a.DetentionDate = CURDATE() AND a.StartTime > CURTIME()))";
	$arr_assigned_session = $ldiscipline->returnArray($sql,5);
	
	$AssignedSessionID = array();
	if(sizeof($arr_assigned_session) > 0)
	{
		for($a=0; $a<sizeof($arr_assigned_session); $a++)
		{
			list($detention_id, $detention_date, $detention_start, $detention_end, $detention_location) = $arr_assigned_session[$a];
			$AssignedSessionID[] = $detention_id;
			
			$AssignedSessionArray[$detention_id]['Date'] = $detention_date;
			$AssignedSessionArray[$detention_id]['StartTime'] = $detention_start;
			$AssignedSessionArray[$detention_id]['EndTime'] = $detention_end;
			$AssignedSessionArray[$detention_id]['StartTimestamp'] = strtotime($detention_date." ".$detention_start);
			$AssignedSessionArray[$detention_id]['EndTimestamp'] = strtotime($detention_date." ".$detention_end);
			$AssignedSessionArray[$detention_id]['Location'] = $detention_location;
		}
	}
    
	## Final avaliable session for auto assign
	$arr_duplicate_session = array();
	$FinalAvaliableSessionArray = array_diff($AvaliableSessionID, $arr_duplicate_session);
	$tempFinalAvaliableSessionArray = $FinalAvaliableSessionArray;
	
	$duplicate_array = array();
	$AutoAssignedSessionArray[$SelectedStudentID][] = array();
	for($j=1; $j<=${"NumOfRecord_$SelectedStudentID"}; $j++)
	{
		$is_absent_record = ${"AbsentRecord_".$SelectedStudentID."_$j"};
		$recordID =  ${"RecordBelongTo_".$SelectedStudentID."_$j"};
		$SelectedSession = ${"SelectDetentionSession_".$SelectedStudentID."_$j"};
		if(!in_array($SelectedSession, $AutoAssignedSessionArray[$SelectedStudentID]))
		{
			$TargetStudentArray[] = $SelectedStudentID;
			$TargetStudentRecordID[$SelectedStudentID][] = $recordID;
			
			$AutoAssignedSessionArray[$SelectedStudentID][] = $SelectedSession;
			for($k=1; $k<sizeof($AutoAssignedSessionArray[$SelectedStudentID]); $k++)
			{
				if($AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['Date'] == $AvaliableSessionArray[$SelectedSession]['Date'])
				{
					if($AutoAssignedSessionArray[$SelectedStudentID][$k] != $SelectedSession)
					{
						if(($AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['StartTimestamp'] <= $AvaliableSessionArray[$SelectedSession]['StartTimestamp']) && ($AvaliableSessionArray[$SelectedSession]['StartTimestamp'] < $AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['EndTimestamp']))
						{
							$arr_duplicate_session[$SelectedStudentID][] = $SelectedSession;
							$error .= $SelectedStudentID."_".$recordID."_Duplicate|";
							break;
						}
						else if(($AvaliableSessionArray[$SelectedSession]['StartTimestamp'] <= $AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['StartTimestamp']) && ($AvaliableSessionArray[$AutoAssignedSessionArray[$SelectedStudentID][$k]]['StartTimestamp'] < $AvaliableSessionArray[$SelectedSession]['EndTimestamp']))
						{
							$arr_duplicate_session[$SelectedStudentID][] = $SelectedSession;
							$error .= $SelectedStudentID."_".$recordID."_Duplicate|";
							break;
						}
						else
						{
							// continuous
						}
					}
					else
					{
						$FinalAssignedSessionArray[$SelectedStudentID][$recordID] = $SelectedSession;
					}
				}
				else
				{
					$FinalAssignedSessionArray[$SelectedStudentID][$recordID] = $SelectedSession;
				}
			}
		}
		else
		{
			$arr_duplicate_session[$SelectedStudentID][] = $SelectedSession;
			$error .= $SelectedStudentID."_".$recordID."_Duplicate|";
		}
	}
	
	if(sizeof($arr_duplicate_session[$SelectedStudentID]) > 0)
	{
		$AutoAssignedSessionArray[$SelectedStudentID] = array_diff($AutoAssignedSessionArray[$SelectedStudentID], $arr_duplicate_session[$SelectedStudentID]);
	}
	
	for($j=1; $j<sizeof($AutoAssignedSessionArray[$SelectedStudentID]); $j++)
	{
		$SelectedSession = $AutoAssignedSessionArray[$SelectedStudentID][$j];
		$NoOfSpaceNeed[$SelectedSession]++;		
	}
}

for($i=0; $i<sizeof($arrTargetStudnetID); $i++)
{
	$SelectedStudentID = $arrTargetStudnetID[$i];
	for($j=0; $j<sizeof($TargetStudentRecordID[$SelectedStudentID]); $j++)
	{
		$record_id = $TargetStudentRecordID[$SelectedStudentID][$j];
		$SelectedSession = $FinalAssignedSessionArray[$SelectedStudentID][$record_id];
		
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '$SelectedSession' AND RecordStatus = 1";
		//echo $sql."<br>";
		$UsedVacancy = $ldiscipline->returnVector($sql);
		//echo $AvaliableSessionArray[$SelectedSession]['Vacancy'].'/'.$NoOfSpaceNeed[$SelectedSession].'/'.$UsedVacancy[0];
		//echo $AvaliableSessionArray[$SelectedSession]['Vacancy']."#";
		if(($AvaliableSessionArray[$SelectedSession]['Vacancy']-$NoOfSpaceNeed[$SelectedSession]-$UsedVacancy[0]) >= 0)
		{
			// enough quota, do nothing
		}
		else
		{
			$error .= $SelectedStudentID."_".$record_id."_NoSpace|";
		}
	}
}

if($error == "")
{
	echo "pass";
}
else
{
	echo $error;
}

intranet_closedb();
?>