<?php
// Modifying by: henry chow

######################################## Change Log #################################################
#
#	Date	: 	2010-05-07 YatWoon
#				According to the setting $PIC_SelectionDefaultOwn to check PIC default selected value
#
############################################ End ###################################################


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
	
$li = new libdbtable();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");


$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);
$SysMsg = $linterface->GET_SYS_MSG("$msg");

if ($cmonth == "") $cmonth = date("Ym");
$CurrentYear = substr($cmonth, 0, 4);
$CurrentMonth = substr($cmonth, 4, 2);
$NoOfDays = date("t", mktime(0,0,0,$CurrentMonth,1,$CurrentYear));
$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());

if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New")) {
	$menuBar1 = $linterface->GET_LNK_NEW("new.php?cpage=cal&cmonth=$cmonth", $button_new,"","","",0);
	$menuBar2 = $linterface->GET_LNK_IMPORT("import.php?cpage=cal&cmonth=$cmonth", $button_import,"","","",0);
	$menuBar = "<td>{$menuBar1}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
	$menuBar .= "<td>{$menuBar2}<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif'/></td>";
}
//$menuBar3 = $linterface->GET_LNK_EXPORT("export.php?cpage=cal&cmonth=$cmonth&targetDate=$targetDate", $button_export);
$menuBar3 = $linterface->GET_LNK_EXPORT("export.php?targetDate=$targetDate&targetStatus=$targetStatus&targetPeriod=$targetPeriod&targetPIC=$targetPIC", $button_export,"","","",0);
$menuBar .= "<td>{$menuBar3}</td>";

// Get list of available DetentionID and list of full DetentionID
$AvailableDetentionID = $ldiscipline->getDetentionArr("Available");
$FullDetentionID = $ldiscipline->getDetentionArr("Full");

// Get Distinct Period
$AllPeriod = $ldiscipline->getSessionPeriod();

// Get Distinct PIC
$AllPIC = $ldiscipline->getSessionPIC();

// Get Distinct Class
$AllClass = $ldiscipline->getClassWithUnassignStudent();

if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	// Get list of DetentionID with selected PIC
	$result = $ldiscipline->getDetentionIDByPIC($targetPIC);
	for ($i=0; $i<sizeof($result); $i++) {
		list($PICDetentionID[]) = $result[$i];
	}
}

// Get list of unassigned students
$result = $ldiscipline->getUnassignStudentByClass($targetClass);
for ($i=0; $i<sizeof($result); $i++) {
	list($UnassignedStudentID[], $UnassignedStudentCount[]) = $result[$i];
}

$table_unassigned_student = "";
if (sizeof($UnassignedStudentID) == 0) {
	$table_unassigned_student .= "<tr class=\"tablerow1\">";
	$table_unassigned_student .= "<td colspan=\"4\" valign=\"top\" align=\"center\"><br />".$i_no_record_exists_msg."</td></tr>";
} else {
	for ($i=0; $i<sizeof($UnassignedStudentID); $i++) {
		$resultUnassignedStudent = $ldiscipline->getStudentNameByID($UnassignedStudentID[$i]);
		list($TmpStudentID, $TmpStudentName, $TmpClassName, $TmpClassNumber) = $resultUnassignedStudent[0];

		// Preset unassign student detail (for javascript)
		$TmpLayerContent = "";
		$resultUnassignStudentDetail = $ldiscipline->getUnassignStudentDetailByStudentID($TmpStudentID);
		for ($j=0; $j<sizeof($resultUnassignStudentDetail); $j++) {
			list($TmpUnassignReason, $TmpUnassignRemark, $TmpUnassignPICName, $TmpUnassignStudentCount) = $resultUnassignStudentDetail[$j];
			if ($j == 0) {
				$TmpLayerContent .= "<a href=\"student_record.php?sid=".$TmpStudentID."\" class=\"tablelink\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">".$i_Discipline_View_Student_Detention_Arrangement."</a>";
				$TmpLayerContent .= "<table width=\"98%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
				$TmpLayerContent .= "<tr class=\"tabletop\">";
				$TmpLayerContent .= "<td align=\"center\">#</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_Reason."</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_Times."</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_PIC."</td>";
				$TmpLayerContent .= "<td align=\"left\">".$i_Discipline_Remark."</td>";
				$TmpLayerContent .= "</tr>";
			}
			$TmpLayerContent .= "<tr class=\"tablerow1\">";
			$TmpLayerContent .= "<td align=\"center\" class=\"tabletext\">".($j+1)."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".intranet_htmlspecialchars($TmpUnassignReason)."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".$TmpUnassignStudentCount."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".$TmpUnassignPICName."</td>";
			$TmpLayerContent .= "<td align=\"left\" class=\"tabletext\" nowrap>".intranet_htmlspecialchars($TmpUnassignRemark)."</td>";
			$TmpLayerContent .= "</tr>";
			if ($j == sizeof($resultUnassignStudentDetail) - 1) {
				$TmpLayerContent .= "</table>";
			}
		}

		//$jUnassignStudentDetailString .= "var jArrayUnassignStudentDetail".$TmpStudentID." = '".str_replace("\r\n", "<br>", str_replace("'", "&#039;", $TmpLayerContent))."';\n";
		$jUnassignStudentDetailString .= "var jArrayUnassignStudentDetail".$TmpStudentID." = '".nl2br(str_replace("\r\n", "<br>", str_replace("'", "&#039;", $TmpLayerContent)))."';\n";

		$table_unassigned_student .= "<tr class=\"tablerow".((($i%2)==0)?"1":"2")."\">\n";
		$table_unassigned_student .= "<td width=\"12\" valign=\"top\">".($i+1)."</td>\n";
		$table_unassigned_student .= "<td valign=\"top\">".$linterface->GET_PRESET_LINK("jArrayUnassignStudentDetail".$TmpStudentID, ($i+1), $TmpClassName."-".$TmpClassNumber." ".$TmpStudentName, "tablelink", sizeof($resultUnassignStudentDetail))."</td>\n";
		$table_unassigned_student .= "<td valign=\"top\">".$UnassignedStudentCount[$i]."</td>\n";
		$table_unassigned_student .= "<td><input type=\"checkbox\" name=\"RecordID[]\" value=\"".$TmpStudentID."\" /></td></tr>\n";
		if ($UnassignedStudentCount[$i] == 1) {		// Display remark only for one detention left
			$resultRemark = $ldiscipline->getRemarkByStudentID($TmpStudentID);
			$TmpRemark = $resultRemark[0];
			if ($TmpRemark <> "") {
				$table_unassigned_student .= "<tr class=\"tablerow".((($i%2)==0)?"1":"2")."\">\n";
				$table_unassigned_student .= "<td valign=\"top\">&nbsp;</td>\n";
				$table_unassigned_student .= "<td valign=\"top\">".intranet_htmlspecialchars($TmpRemark)."</td>\n";
				$table_unassigned_student .= "<td valign=\"top\">&nbsp;</td><td>&nbsp;</td></tr>\n";
			}
		}
	}
}


if ($targetDate=="") $targetDate = "This_Week";		// default: filter this week
if (!isset($field)) $field = 0;		// default: sort by date
if (!isset($order)) $order = 1;		// default: asc order
$pageSizeChangeEnabled = true;
if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$conds = " WHERE 1=1";
if ($targetDate == "This_Month") {
	$conds .= " AND YEAR(DetentionDate) = ".date("Y")." AND MONTH(DetentionDate) = ".date("n");
} else if ($targetDate == "This_Week") {
	if (date("w") == 0) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-0 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+6 days"))."'";
	} else if (date("w") == 1) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-1 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+5 days"))."'";
	} else if (date("w") == 2) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-2 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+4 days"))."'";
	} else if (date("w") == 3) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-3 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+3 days"))."'";
	} else if (date("w") == 4) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-4 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+2 days"))."'";
	} else if (date("w") == 5) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-5 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+1 days"))."'";
	} else if (date("w") == 6) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-6 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+0 days"))."'";
	}
} else if ($targetDate == "Next_Week") {
	if (date("w") == 0) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+7 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+13 days"))."'";
	} else if (date("w") == 1) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+6 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+12 days"))."'";
	} else if (date("w") == 2) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+5 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+11 days"))."'";
	} else if (date("w") == 3) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+4 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+10 days"))."'";
	} else if (date("w") == 4) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+3 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+9 days"))."'";
	} else if (date("w") == 5) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+2 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+8 days"))."'";
	} else if (date("w") == 6) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+1 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+7 days"))."'";
	}
}

if ($targetStatus == "Available") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DET.DetentionID IN (".implode(', ', $AvailableDetentionID).")";
} else if ($targetStatus == "Full") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DET.DetentionID IN (".implode(', ', $FullDetentionID).")";
} else if ($targetStatus == "Finished") {
	$conds .= " AND (DetentionDate < '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime < '$CurrentTime:00'))";
}

if (($targetPeriod != "All_Time") && ($targetPeriod != "")) {
	$conds .= " AND CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) = '$targetPeriod'";
}

if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	$conds .= " AND DET.DetentionID IN (".implode(', ', $PICDetentionID).")";
}

$sql = "SELECT DET.DetentionID, DetentionDate, CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) as Period, Location
				, 'DUMMY' as Form, 'DUMMY' as PIC, Vacancy, COUNT(SES.DetentionID) as Assigned, 'DUMMY' as Attendance, 'DUMMY' as Status
				, CONCAT('<a href=\"detail.php?did=', DET.DetentionID, '\" class=\"tablelink\"><img src=\"$image_path/$LAYOUT_SKIN/eOffice/icon_classview.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\" title=\"$i_Discipline_Details\"></a>') as Detail
				FROM DISCIPLINE_DETENTION_SESSION DET
				LEFT JOIN DISCIPLINE_DETENTION_STUDENT_SESSION SES ON (SES.DetentionID=DET.DetentionID AND SES.RecordStatus=1)".$conds.
				" GROUP BY DET.DetentionID, DetentionDate, CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)), Location, Vacancy";

$li->field_array = array("DetentionDate", "Period", "Location", "Vacancy", "Assigned");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+6;
$li->title = $i_Discipline_Records;
$li->column_array = array(14,14,14,14,14,14,14,14,14,14,14);
$li->IsColOff = "eDisciplineDetentionList";
$li->AllFormsData = $i_Discipline_All_Forms;

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='2%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='11%' nowrap>".$li->column($pos++, $i_Discipline_Date)."</td>\n";
$li->column_list .= "<td width='11%' nowrap>".$li->column($pos++, $i_Discipline_Time)."</td>\n";
$li->column_list .= "<td width='13%' nowrap>".$li->column($pos++, $i_Discipline_Location)."</td>\n";
$li->column_list .= "<td width='11%'>".$i_Discipline_Form."</td>\n";
$li->column_list .= "<td width='17%'>".$i_Discipline_Duty_Teacher."</td>\n";
$li->column_list .= "<td width='7%'>".$li->column($pos++, $i_Discipline_Vacancy)."</td>\n";
$li->column_list .= "<td width='7%'>".$li->column($pos++, $i_Discipline_Assigned)."</td>\n";
$li->column_list .= "<td width='11%' nowrap>".$i_Discipline_Attendance."</td>\n";
$li->column_list .= "<td width='7%' nowrap>".$i_Discipline_Status."</td>\n";
$li->column_list .= "<td width='3%' nowrap>&nbsp;</td>\n";

# filters
$select_date = "<SELECT name=\"targetDate\" id=\"targetDate\" onChange=\"this.form.submit()\">\n";
$select_date .= "<OPTION value='All_Date' ".($targetDate=="All_Date"?"SELECTED":"").">$i_Discipline_All_Date</OPTION>\n";
$select_date .= "<OPTION value='This_Month' ".($targetDate=="This_Month"?"SELECTED":"").">$i_Discipline_This_Month2</OPTION>\n";
$select_date .= "<OPTION value='This_Week' ".($targetDate=="This_Week"?"SELECTED":"").">$i_Discipline_This_Week2</OPTION>\n";
$select_date .= "<OPTION value='Next_Week' ".($targetDate=="Next_Week"?"SELECTED":"").">$i_Discipline_Next_Week</OPTION>\n";
$select_date .= "</SELECT>\n";

$select_status = "<SELECT name=\"targetStatus\" id=\"targetStatus\" onChange=\"this.form.submit()\">\n";
$select_status .= "<OPTION value='All_Status' ".($targetStatus=="All_Status"?"SELECTED":"").">$i_Discipline_All_Status</OPTION>\n";
$select_status .= "<OPTION value='Available' ".($targetStatus=="Available"?"SELECTED":"").">$i_Discipline_Available</OPTION>\n";
$select_status .= "<OPTION value='Full' ".($targetStatus=="Full"?"SELECTED":"").">$i_Discipline_Full</OPTION>\n";
$select_status .= "<OPTION value='Finished' ".($targetStatus=="Finished"?"SELECTED":"").">$i_Discipline_Finished</OPTION>\n";
$select_status .= "</SELECT>\n";

$select_period = "<SELECT name=\"targetPeriod\" id=\"targetPeriod\" onChange=\"this.form.submit()\">\n";
$select_period .= "<OPTION value='All_Time' ".($targetPeriod=="All_Time"?"SELECTED":"").">$i_Discipline_All_Time</OPTION>\n";
for ($i=0; $i<sizeof($AllPeriod); $i++) {
	list($TempPeriod) = $AllPeriod[$i];
	$select_period .= "<OPTION value='$TempPeriod' ".($targetPeriod==$TempPeriod?"SELECTED":"").">$TempPeriod</OPTION>\n";
}
$select_period .= "</SELECT>\n";

if($ldiscipline->PIC_SelectionDefaultOwn)
{
	if(sizeof($_POST)==0 && sizeof($_GET)==0 && !isset($_POST['targetPIC'])) {		# default display own PIC records
		$flag = 0;
		for($i=0; $i<sizeof($AllPIC); $i++) {
			if($AllPIC[$i][0]==$UserID) {
				$targetPIC = $UserID;		
				$flag = 1;
				break;
			}
		}
	}
}

$select_pic = "<SELECT name=\"targetPIC\" id=\"targetPIC\" onChange=\"this.form.submit()\">\n";
$select_pic .= "<OPTION value='All_Duty' ".($targetPIC=="All_Duty"?"SELECTED":"").">$i_Discipline_Detention_All_Teachers</OPTION>\n";
for ($i=0; $i<sizeof($AllPIC); $i++) {
	list($TempUserID, $TempPIC) = $AllPIC[$i];
	$select_pic .= "<OPTION value='$TempUserID' ".($targetPIC==$TempUserID?"SELECTED":"").">$TempPIC</OPTION>\n";
}
$select_pic .= "</SELECT>\n";

$select_class = "<SELECT name=\"targetClass\" id=\"targetClass\" onChange=\"this.form.submit()\">\n";
$select_class .= "<OPTION value='All_Classes' ".($targetClass=="All_Classes"?"SELECTED":"").">$i_Discipline_All_Classes</OPTION>\n";

for ($i=0; $i<sizeof($AllClass); $i++) {
	$ClassName = $AllClass[$i];
	$select_class .= "<OPTION value='$ClassName' ".($targetClass==$ClassName?"SELECTED":"").">$ClassName</OPTION>\n";
}
$select_class .= "</SELECT>\n";

$searchbar = "$select_date $select_status $select_period $select_pic";

$linterface->LAYOUT_START();
?>

<script language="javascript">
<?=$jUnassignStudentDetailString?>
<?=$jUnassignStudentTitleString?>

	function arrangeCheck() {
		if (countChecked(document.form1, 'RecordID[]') == 0) {
			alert(globalAlertMsg2);
			return false;
		} else {
			if(confirm("<?=$i_Discipline_Arrange_Alert?>")){
				document.form1.action="arrange.php";
				document.form1.submit();
			} else {
				return false;
			}
		}
	}

	function autoArrangeCheck() {
		if (countChecked(document.form1, 'RecordID[]') == 0) {
			alert(globalAlertMsg2);
			return false;
		} else {
			if(confirm("<?=$i_Discipline_Auto_Arrange_Alert?>")){
				document.form1.action="auto_arrange.php";
				document.form1.submit();
			} else {
				return false;
			}
		}
	}

	function checkForm(){
		for(var i=1;i<=<?=sizeof($student)?>;i++){
			if (document.getElementById('TextReason'+i).value=='') {
				alert('<?=$i_alert_pleasefillin.$i_Discipline_Reason?>');
				return false;
			}
		}
		document.getElementById('submit_flag').value='YES';
		return true;
	}
</script>

<br />
<form name="form1" method="post" onsubmit="return checkForm();" action="list.php">
<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<?=$menuBar?>
				</tr>
			</table>
		</td>
		<td align="right"><?=$SysMsg?>&nbsp;</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="28" align="right" valign="bottom" class="tabletextremark">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30" width="75%"><?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Detention_List_Instruction_Msg) ?></td>
					<td align="right" valign="middle" class="thumb_list"><a href="calendar.php"><?=$i_Discipline_Calendar?></a> | <span><?=$i_Discipline_Session2?></span> | <a href="student_record.php"><?=$i_Discipline_Student?></a></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
		</table>
		</td>
	</tr>
</table>


<table width="97%" cellspacing="0" cellpadding="3" border="0">
	<tr>
		<td width="75%" valign="top">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td><?= $searchbar ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
<?=$li->display();?>

		</td>
		<td valign="top" style="border: 1px solid rgb(204, 204, 204);">
			<table width="100%" cellspacing="0" cellpadding="0" border="0">
				<tbody>
					<tr>
						<td valign="top"><span class="indextabon"><?=$i_Discipline_Unassign_Student_List?></span></td>
					</tr>
					<tr>
						<td height="30" align="left"><span class="indextabon"><?=$select_class?></span></td>
					</tr>
					<tr class="table-action-bar">
						<td align="right">
							<table cellspacing="0" cellpadding="0" border="0">
								<tbody>
									<tr>
										<td width="21"><img width="21" height="23" src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_01.gif"/></td>
										<td background="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_02.gif">
											<table cellspacing="0" cellpadding="2" border="0">
												<tbody>
													<tr>
														<td><img width="5" src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif"/></td>
														<td nowrap="">
															<a class="tabletool" href="#" onClick="javascript:autoArrangeCheck();">
																<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/><?=$i_Discipline_Auto_Arrange?></a>
														</td>
														<td><img width="5" src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif"/></td>
														<td nowrap="">
															<a class="tabletool" href="#" onClick="javascript:arrangeCheck();">
																<img width="12" height="12" border="0" align="absmiddle" src="<?="$image_path/$LAYOUT_SKIN"?>/icon_handle.gif"/><?=$i_Discipline_Arrange?></a>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
										<td width="6"><img width="6" height="23" src="<?="$image_path/$LAYOUT_SKIN"?>/table_tool_03.gif"/></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
			<table width="100%" cellspacing="0" cellpadding="4" border="0">
				<tbody>
					<tr class="tabletop">
						<td>#</td>
						<td><?=$i_Discipline_Student?></td>
						<td><?=$i_Discipline_Times_Remain?></td>
						<td width="25"><?=$li->check("RecordID[]")?></td>
					</tr>
<?=$table_unassigned_student?>
				</tbody>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" id="cmonth" name="cmonth" value="<?=$cmonth?>">
<input type="hidden" id="DetentionID" name="DetentionID">
<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
