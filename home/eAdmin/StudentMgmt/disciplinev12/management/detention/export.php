<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");

$ExportArr = array();
if ($targetDate=="") $targetDate = "This_Week";		// default: filter this week


$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());
$conds = " WHERE 1=1";

if($cmonth!='')
{
	$Year = substr($cmonth, 0, 4);
	$Month = substr($cmonth, 4, 2);
	$conds .= " AND YEAR(DetentionDate) = ".$Year." AND MONTH(DetentionDate) = ".$Month;
}

else
{
	if ($targetDate == "This_Month") {
		$conds .= " AND YEAR(DetentionDate) = ".date("Y")." AND MONTH(DetentionDate) = ".date("n");
	} else if ($targetDate == "This_Week") {
		if (date("w") == 0) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-0 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+6 days"))."'";
		} else if (date("w") == 1) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-1 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+5 days"))."'";
		} else if (date("w") == 2) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-2 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+4 days"))."'";
		} else if (date("w") == 3) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-3 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+3 days"))."'";
		} else if (date("w") == 4) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-4 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+2 days"))."'";
		} else if (date("w") == 5) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-5 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+1 days"))."'";
		} else if (date("w") == 6) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-6 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+0 days"))."'";
		}
	} else if ($targetDate == "Next_Week") {
		if (date("w") == 0) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+7 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+13 days"))."'";
		} else if (date("w") == 1) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+6 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+12 days"))."'";
		} else if (date("w") == 2) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+5 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+11 days"))."'";
		} else if (date("w") == 3) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+4 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+10 days"))."'";
		} else if (date("w") == 4) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+3 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+9 days"))."'";
		} else if (date("w") == 5) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+2 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+8 days"))."'";
		} else if (date("w") == 6) {
			$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+1 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+7 days"))."'";
		}
	}
}


if ($targetStatus == "Available") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	if(sizeof($AvailableDetentionID)>0)
	$conds .= " AND DetentionID IN (".implode(', ', $AvailableDetentionID).")";
} else if ($targetStatus == "Full") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	if(sizeof($FullDetentionID)>0)
	$conds .= " AND DetentionID IN (".implode(', ', $FullDetentionID).")";
} else if ($targetStatus == "Finished") {
	$conds .= " AND (DetentionDate < '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime < '$CurrentTime:00'))";
}

if (($targetPeriod != "All_Time") && ($targetPeriod != "")) {
	$conds .= " AND CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) = '$targetPeriod'";
}

if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	
	$conds .= " AND DPIC.UserID = '$targetPIC'";
}

$sql = "SELECT DET.DetentionID, DetentionDate, CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) as Period, Location
				, 'DUMMY' as Form, 'DUMMY' as PIC, Vacancy, COUNT(SES.DetentionID) as Assigned, 'DUMMY' as Attendance, 'DUMMY' as Status
				, CONCAT('$i_Discipline_Details') as Detail
				FROM DISCIPLINE_DETENTION_SESSION DET
				LEFT JOIN DISCIPLINE_DETENTION_STUDENT_SESSION SES ON (SES.DetentionID=DET.DetentionID)
				LEFT JOIN DISCIPLINE_DETENTION_SESSION_PIC DPIC ON (DPIC.DetentionID=DET.DetentionID)
				".$conds.
				" GROUP BY DET.DetentionID, DetentionDate, CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)), Location, Vacancy
				order by DetentionDate";

$row = $ldiscipline->returnArray($sql,9);

// Create data array for export
for($i=0; $i<sizeof($row); $i++)
{
	//Form 	
	$TempAllForms = $ldiscipline->getAllFormString();
	//$tempSQL = "SELECT INTR.LevelName FROM DISCIPLINE_DETENTION_SESSION_CLASSLEVEL LVL, INTRANET_CLASSLEVEL INTR WHERE LVL.ClassLevelID = INTR.ClassLevelID AND LVL.DetentionID = '".$row[$i]['DetentionID']."' ORDER BY INTR.LevelName";
	$tempSQL = "SELECT y.YearName FROM DISCIPLINE_DETENTION_SESSION_CLASSLEVEL LVL, YEAR y WHERE LVL.ClassLevelID = y.YearID AND LVL.DetentionID = '".$row[$i]['DetentionID']."' ORDER BY y.YearName";
	$result = $ldiscipline->returnArray($tempSQL,1);
	$TempClassLevel = "";
	for($k=0;$k<sizeof($result);$k++)
		$TempClassLevel[] = $result[$k][0];
	if (is_array($TempClassLevel)){
		$Form = implode(", ",$TempClassLevel);
	} else {
		$Form = "";
	}
	if($Form==$TempAllForms){
		$Form = $i_Discipline_All_Forms;
	} 
	//PIC
	$name_field = getNameFieldByLang("USR.");
	$tempSQL = "SELECT $name_field FROM DISCIPLINE_DETENTION_SESSION_PIC PIC, INTRANET_USER USR WHERE PIC.UserID = USR.UserID AND PIC.DetentionID = '".$row[$i]['DetentionID']."' ORDER BY $name_field";
	$result = $ldiscipline->returnArray($tempSQL,1);
	$TempPIC = "";
	for($k=0;$k<sizeof($result);$k++)
		$TempPIC[] = $result[$k][0];
	if (is_array($TempPIC)){
		$PIC = implode(", ",$TempPIC);
	} else {
		$PIC = "";
	}
	//status
	/*
	$tempSQL = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '".$row[$i]['DetentionID']."'";
	$result = $ldiscipline->returnArray($tempSQL,1);
	$TempAssigned = $result[0][0];
	*/
	$tempSQL = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '".$row[$i]['DetentionID']."' AND RecordStatus=1";
	$result = $ldiscipline->returnVector($tempSQL);
	$TempAssigned = $result[0];

	/*	
	if ($row[$i]['Vacancy'] <= $TempAssigned) 
	{
		$Status = "$i_Discipline_Full";
	}
	else if (($row[$i]['DetentionDate'] < $CurrentDay) || (($row[$i]['DetentionDate'] == $CurrentDay) && (substr($row[$i]['Period'],0,5) < $CurrentTime))) 
	{
		$TempCSS = "row_approved record_Waived";
		$Status = "$i_Discipline_Finished";
	}
	else
	{
		$Status = "$i_Discipline_Available";
	}
	*/
	if ($row[$i]['Vacancy'] <= $TempAssigned) {
		$Status = "$i_Discipline_Full";
	} else if (($row[$i]['DetentionDate'] < $CurrentDay)) {
		$Status = "$i_Discipline_Finished";
	} else {
		$Status = "$i_Discipline_Available";
	}
						
 	$ExportArr[$i][0] = $row[$i]['DetentionDate'];	//CaseNumber
	$ExportArr[$i][1] = $row[$i]['Period'];			//CaseTitle
	$ExportArr[$i][2] = $row[$i]['Location'];		//EventDate
	$ExportArr[$i][3] = $Form;						//Form
	$ExportArr[$i][4] = $PIC;						//PIC
	$ExportArr[$i][5] = $row[$i]['Vacancy'];		//Vacancy
	$ExportArr[$i][6] = $row[$i]['Assigned'];		//Assigned
	$ExportArr[$i][7] = $Status;		//Status
}



//define column title
$exportColumn = array($i_Discipline_Date,$i_Discipline_Time,$i_Discipline_Location,$i_Discipline_Form,$i_Discipline_Duty_Teacher,$i_Discipline_Vacancy,$i_Discipline_Assigned,$i_Discipline_Status);


$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output the file to user browser
$filename = 'detention.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>
