<?php
// Modifying by: Ronald

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if (is_array($RecordID) && $RecordID[0] <> "") {
	$allSessionOwn = true;
	for ($i=0; $i<sizeof($RecordID); $i++) {
		$rid = $RecordID[$i];
		$allSessionOwn = $allSessionOwn && $ldiscipline->isDetentionSessionOwn($rid);
	}
}

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-DeleteAll") || ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-DeleteOwn") && $allSessionOwn)))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
else
{
$SuccessFlag = true;

if (is_array($RecordID) && $RecordID[0] <> "") {
	for ($i=0; $i<sizeof($RecordID); $i++) {
		$rid = $RecordID[$i];
		$NoticeID = $ldiscipline->getNoticeIDByRecordID($rid);
		//$result = $ldiscipline->deleteDetentionByRecordID($rid);
		$result = $ldiscipline->archiveDetentionByRecordID($rid);
		if (!$result) $SuccessFlag = false;
		if($NoticeID[0] && $result) {
			include_once($intranet_root."/includes/libnotice.php");
			$lnotice = new libnotice($NoticeID[0]);
			$lnotice->deleteNotice($NoticeID[0]);
		}
	}
}

if ($SuccessFlag) {
	$SysMsg = "delete";
} else {
	$SysMsg = "delete_failed";
}

intranet_closedb();
$ReturnPage = "student_record.php";
header("Location: $ReturnPage?msg=$SysMsg");
}
?>