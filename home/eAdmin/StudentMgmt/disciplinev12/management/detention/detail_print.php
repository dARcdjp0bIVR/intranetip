<?php
// Modifying by: 

################################
# Date:	2017-01-09	Bill	[2016-0808-1743-20240]
#		added table column - Remaining Detentions
# Date:	2015-10-05	Bill	[2015-0923-1037-19207]
#		added table column - PIC
# Date:	2015-09-24	Bill	[2015-0924-1046-51071]
#		added table column - Attendance Status & Attendance Remark
# Date:	2015-04-23	Bill	[2014-1216-1347-28164]
#		add detention session type to Session Info
# Date:	2014-11-27	Bill
#		improved: Change the first element in $order_array (Class->ClassSort)
# Date: 2014-09-12	YatWoon
#		improved: separate class and class number [Case#P67603]
#		Deploy: IPv10.1
# Date:	2014-04-11	YatWoon
#		add ordering checking [Case#P60964]
################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/edis_print_header.php");

// [2014-1216-1347-28164] get detention content with session type
$TempList = $ldiscipline->getDetentionListByDetentionID($did, ($sys_custom['eDiscipline']['CreativeSchDetentionCust']?true:false));
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	// [2014-1216-1347-28164] get detention session type
	list($TmpType, $TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
}
else {
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
}

//$order_array = array("concat(TRIM(SUBSTRING_INDEX(Class, '-', 1)), IF(INSTR(Class, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(Class, '-', -1)), 10, '0')))", "Student", "Reason", "DateInput", "Status");
$order_array = array("concat(TRIM(SUBSTRING_INDEX(ClassSort, '-', 1)), IF(INSTR(ClassSort, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassSort, '-', -1)), 10, '0')))", "Student", "Reason", "DateInput", "Status");
$orderby = $order_array[$field] . " " . ($order==1?"asc":"desc");
$classSort = ($field == 0);
$result = $ldiscipline->getDetentionInfoByDetentionID($did, $orderby, $classSort);
for ($i=0; $i<sizeof($result); $i++)
{
	// [2015-0923-1037-19207] Get PICID
	//list($InfoClass[], $InfoClassNumber[], $InfoStudent[], $InfoStudentID[], $InfoDetentionID[], $InfoDemeritID[], $InfoReason[], $InfoRemark[], $InfoAttendanceStatus[], $InfoNoticeID[], $InfoRequestedBy[], $InfoArrangedBy[], $InfoArrangedDate[], $InfoAttendanceRemark[], $InfoAttendanceTakenBy[], $InfoAttendanceTakenDate[], $InfoDateInput[], $InfoDateModified[]) = $result[$i];
	if($classSort)
		list($InfoClass[], $InfoClassNumber[], $InfoStudent[], $InfoStudentID[], $InfoDetentionID[], $InfoDemeritID[], $InfoReason[], $InfoRemark[], $InfoAttendanceStatus[], $InfoNoticeID[], $InfoRequestedBy[], $InfoArrangedBy[], $InfoArrangedDate[], $InfoAttendanceRemark[], $InfoAttendanceTakenBy[], $InfoAttendanceTakenDate[], $InfoDateInput[], $InfoDateModified[], $InfoRecorID[], $InfoClassSort[], $InfoPIC[]) = $result[$i];
	else
		list($InfoClass[], $InfoClassNumber[], $InfoStudent[], $InfoStudentID[], $InfoDetentionID[], $InfoDemeritID[], $InfoReason[], $InfoRemark[], $InfoAttendanceStatus[], $InfoNoticeID[], $InfoRequestedBy[], $InfoArrangedBy[], $InfoArrangedDate[], $InfoAttendanceRemark[], $InfoAttendanceTakenBy[], $InfoAttendanceTakenDate[], $InfoDateInput[], $InfoDateModified[], $InfoRecorID[], $InfoPIC[]) = $result[$i];

	if ($InfoReason[$i] == "") {
		$InfoReason[$i] = "&nbsp;";
	}

	if ($InfoNoticeID[$i] == "") {
		$InfoStatus[$i] = "--";
	}
	else {
		$InfoStatus[$i] = $i_Discipline_Released_To_Student;
	}
	
	if ($InfoAttendanceStatus[$i] == "") {
		$InfoAttendanceStatus[$i] = "&nbsp;";
	}
	else if ($InfoAttendanceStatus[$i] == "PRE") {
		$InfoAttendanceStatus[$i] = $i_Discipline_Present;
	}
	else if ($InfoAttendanceStatus[$i] == "ABS") {
		$InfoAttendanceStatus[$i] = $i_Discipline_Absent;
	}
	
	// [2015-0923-1037-19207] Get PIC Name
	if($InfoPIC[$i] == ""){
		$InfoPIC[$i] = "--";
	}
	else {
		$InfoPIC[$i] = $ldiscipline->getPICNameList($InfoPIC[$i]);
	}
	
	// [2016-0808-1743-20240] Get Remaining Detentions of current student  
	if($sys_custom['eDiscipline']['BWWTC_DetentionPrinAttendance'])
	{
		$remainingDetentionStr = "";
		
		// loop detention sessions
		$remainingDetention = $ldiscipline->getStudentRemainingDetention($result[$i]["StudentID"], "$TmpDetentionDate $TmpStartTime");
		foreach((array)$remainingDetention as $thisDetention)
		{
			$remainingDetentionStr .= $remainingDetentionStr==""? "" : "<br>";
			$remainingDetentionStr .= substr($thisDetention["DetentionStartDate"], 0, 16);
			$remainingDetentionStr .= "~";
			$remainingDetentionStr .= substr($thisDetention["EndTime"], 0, 5);
		}
		
		if($remainingDetentionStr == ""){
			$remainingDetentionStr = "--";
		}
	}
	
	$OutputRow .= "<tr class=\"row_print\">";
	$OutputRow .= "<td valign=\"top\">".($i+1)."</td>";
	$OutputRow .= "<td valign=\"top\">".$InfoClass[$i]."</td>";
	$OutputRow .= "<td valign=\"top\">".$InfoClassNumber[$i]."</td>";
	$OutputRow .= "<td valign=\"top\">".$InfoStudent[$i]."</td>";
	$OutputRow .= "<td valign=\"top\">".str_replace("&amp;", "&", intranet_htmlspecialchars($InfoReason[$i]))."</td>";
	$OutputRow .= "<td valign=\"top\">".str_replace("&amp;", "&", intranet_htmlspecialchars($InfoRemark[$i]))."</td>";
	$OutputRow .= "<td valign=\"top\">".$InfoDateInput[$i]."</td>";
	// [2015-0923-1037-19207] added content
	$OutputRow .= "<td valign=\"top\">".$InfoPIC[$i]."</td>";
	$OutputRow .= "<td valign=\"top\">".$InfoStatus[$i]."</td>";
	$OutputRow .= "<td valign=\"top\">".$InfoAttendanceStatus[$i]."</td>";
	// [2015-0924-1046-51071] added content
	$OutputRow .= "<td valign=\"top\">".$InfoAttendanceRemark[$i]."</td>";
	// [2016-0808-1743-20240] added content - Remaining Detentions 
	if($sys_custom['eDiscipline']['BWWTC_DetentionPrinAttendance'])
		$OutputRow .= "<td valign=\"top\">".$remainingDetentionStr."</td>";
	$OutputRow .= "</tr>";
}
?>

<table width="100%" align="center">
	<tr>
		<td align="right">
			<input name="btn_print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">
		</td>
	</tr>
</table>

<table cellspacing="0" cellpadding="4" width="100%" align="center" border="0">
	<tbody>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="page_title_print"><?=$i_Discipline_Attendance_Sheet?></td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle" />
							<span class="sectiontitle_print"><?=$i_Discipline_Session_Info?></span>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="right" background="<?="$image_path/$LAYOUT_SKIN"?>/iPortfolio/scheme/scheme_board_05_off.gif">
							<table width="100%" border="0" cellspacing="5" cellpadding="5" class="tableborder_print_alone">
								
								<?php if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){ ?>
									<tr>
										<td width="20%" align="left" valign="top"><?=$Lang['eDiscipline']['DetentionTypes']['Title']?></td>
										<td align="left" valign="top" class="row_underline"><?=$TmpType?></td>
									</tr>
								<?php } ?>
								
								<tr>
									<td width="20%" align="left" valign="top"><?=$i_Discipline_Location?></td>
									<td align="left" valign="top" class="row_underline"><?=intranet_htmlspecialchars($TmpLocation)?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Duty_Teacher?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpPIC?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Date?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpDetentionDate?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Time?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpPeriod?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Vacancy?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpVacancy?></td>
								</tr>
								<tr>
									<td align="left" valign="top"><?=$i_Discipline_Students_Attending?></td>
									<td align="left" valign="top" class="row_underline"><?=$TmpAssigned?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br />
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle" />
							<span class="sectiontitle_print"><?=$i_Discipline_Student_List?></span>
						</td>
					</tr>
				</table>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableborder_print">
								<tr class="tabletop_print">
									<td width="15" align="center">#</td>
									<td><?=$i_Discipline_Class?></td>
									<td><?=$Lang['General']['ClassNumber']?></td>
									<td><?=$i_Discipline_Student?></td>
									<td><?=$i_Discipline_Detention_Reason?></td>
									<td><?=$i_Discipline_Remark?></td>
									<td><?=$i_Discipline_Request_Date?></td>
									<td><?=$i_Discipline_PIC?></td>
									<td><?=$i_Discipline_Status?></td>
									<td><?=$Lang['eDiscipline']['Detention']['AttendanceStatus']?></td>
									<td><?=$Lang['eDiscipline']['Detention']['AttendanceRemark']?></td>
								<? if($sys_custom['eDiscipline']['BWWTC_DetentionPrinAttendance']) { ?>
									<td><?=$Lang['eDiscipline']['RemainDetentionRecord']?></td>
								<? } ?>
								</tr>
<?=$OutputRow?>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>

<?
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>