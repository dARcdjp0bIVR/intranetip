<?php
// Modifying by: 

############ Change Log Start #############
#
#	Date	:	2015-04-20	Bill
#				Add detention type field [2014-1216-1347-28164 - Detention customization]
#
#	Date	:	2013-10-25 (YatWoon)
#				Add back $cmonth variable for back to previous calender month [Case#2013-1016-1535-14073]
#
#	Date	:	2011-04-14 (Henry Chow)
#	Detail 	:	allow to edit today's detention session even start time is passed (but cannot edit the Time)
#
#	Date	:	2010-06-25 (Henry)
#	Detail 	:	retrieve Year Name according to the AcademicYearID
#
############ Change Log End #############

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Detention-Access");
$lu = new libuser();

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

if (is_array($RecordID) && $RecordID[0] <> "") $did = $RecordID[0];

$back_par = "targetDate=$targetDate&targetStatus=$targetStatus&targetPeriod=$targetPeriod&targetPIC=$targetPIC";

// 2014-1216-1347-28164 get session type
$sql = "SELECT Year, Semester, DetentionDate, StartTime, EndTime, Location, Vacancy, SessionType FROM DISCIPLINE_DETENTION_SESSION WHERE DetentionID = '$did'";
$result = $ldiscipline->returnArray($sql, 7);
list($OldYear, $OldSemester, $OldDetentionDate, $OldStartTime, $OldEndTime, $OldLocation, $OldVacancy, $OldSessionType) = $result[0];
$OldYear = $ldiscipline->getAcademicYearNameByYearID($ldiscipline->getAcademicYearIDByYearName($OldYear));
$OldStartHour = substr($OldStartTime, 0, 2);
$OldStartMin = substr($OldStartTime, 3, 2);
$OldEndHour = substr($OldEndTime, 0, 2);
$OldEndMin = substr($OldEndTime, 3, 2);

$select_sem_html = "<input type='hidden' name='semester' value=''>";

$sql = "SELECT ClassLevelID FROM DISCIPLINE_DETENTION_SESSION_CLASSLEVEL WHERE DetentionID = '$did'";
$result = $ldiscipline->returnVector($sql);
for ($i=0; $i<sizeof($result); $i++) {
	$OldClassLevelID[] = $result[$i];
}

$sql = "SELECT UserID FROM DISCIPLINE_DETENTION_SESSION_PIC WHERE DetentionID = '$did'";
$result = $ldiscipline->returnVector($sql);
for ($i=0; $i<sizeof($result); $i++) {
	$OldPICID[] = $result[$i];
}

$linterface = new interface_html();
$lclass = new libclass();
$ClassLvlArr = $lclass->getLevelArray();
$lteaching = new libteaching();
$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "../", 0);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 1);
$PAGE_NAVIGATION[] = array($i_Discipline_Session_List, "list.php?$back_par");
$PAGE_NAVIGATION[] = array($i_Discipline_Edit_Session, "");

if ($cpage == "cal") {
	$BackBtnScript = "window.location='calendar.php?cmonth=$cmonth&$back_par'";
	$ActionPage = "edit_update.php?cpage=cal";
} else if ($cpage == "list") {
	$BackBtnScript = "window.location='list.php?$back_par'";
	$ActionPage = "edit_update.php?cpage=list";
}

for ($i=0; $i<=23; $i++) {
	if ($i < 10) {
		$HourArray[] = array("0".$i,"0".$i);
	} else {
		$HourArray[] = array($i,$i);
	}
}
for ($i=0; $i<=55; $i+=5) {
	if ($i < 10) {
		$MinArray[] = array("0".$i,"0".$i);
	} else {
		$MinArray[] = array($i,$i);
	}
}

$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '$did' AND RecordStatus != 0";
$NumOfAssignedStudent = $ldiscipline->returnVector($sql);
$disabled = "";

$js_NumOfAssignedStudent = "var js_NumOfAssignedStudent = ".$NumOfAssignedStudent[0].";";

if($NumOfAssignedStudent[0] > 0){
	$disabled = "disabled";
	$html_show_current_assigned_students = "<span class='tabletextremark'><font color='red'>&nbsp;&nbsp;&nbsp;".$Lang['eDiscipline']['FieldTitle']['NumOfStudentsAssignedInThisSession']." : ".$NumOfAssignedStudent[0]."</font></span>";
}

if($OldDetentionDate==date('Y-m-d') && $OldStartTime<=date('H:i:s')) {
	$disabled = "disabled";	
}

$StartHour = getSelectByArray($HourArray,"name='StartHour' $disabled",$OldStartHour,0,1,"",2);
$StartMin = getSelectByArray($MinArray,"name='StartMin' $disabled",$OldStartMin,0,1,"",2);
$EndHour = getSelectByArray($HourArray,"name='EndHour' $disabled",$OldEndHour,0,1,"",2);
$EndMin = getSelectByArray($MinArray,"name='EndMin' $disabled",$OldEndMin,0,1,"",2);

// [2014-1216-1347-28164] session type selection
if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){
	$emptySessionOption = $OldSessionType? "" : "<option value='0' SELECTED> -- $button_select -- </option>";
	$sessiontype_selection = "
	<select id='sessiontype' name='sessiontype'>
		$emptySessionOption
		<option value='1' ".($OldSessionType=="1"?"SELECTED":"").">".$Lang['eDiscipline']['DetentionTypes']['LunchTime']."</option>
		<option value='2' ".($OldSessionType=="2"?"SELECTED":"").">".$Lang['eDiscipline']['DetentionTypes']['AfterSchool']."</option>
		<option value='3' ".($OldSessionType=="3"?"SELECTED":"").">".$Lang['eDiscipline']['DetentionTypes']['Suspension']."</option>
	</SELECT>";
}

$ClassLvlChk = "<input type=\"checkbox\" $disabled id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.FormEdit, 'classlvl[]') : setChecked(0, document.FormEdit, 'classlvl[]');\"><label for=\"classlvlall\">".$i_Discipline_All_Forms."</label> ";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = "";
	for ($j=0; $j<sizeof($OldClassLevelID); $j++) {
		if ($ClassLvlArr[$i][0] == $OldClassLevelID[$j]) {
			$checked = "checked";
		}
	}
	$ClassLvlChk .= "<input type=\"checkbox\" $disabled $checked id=\"classlvl{$i}\" name=\"classlvl[]\" onClick=\"if(!this.checked){document.FormEdit.classlvlall.checked=false;}\" value=\"".$ClassLvlArr[$i][0]."\"><label for=\"classlvl{$i}\">".$ClassLvlArr[$i][1]."</label> ";
}

for($i = 0; $i < 40; $i++) $space .= "&nbsp;";

$SelectPICList = "";

$staffList = $lu->returnUsersType(1);

for ($i=0; $i<sizeof($staffList); $i++) {
	if (is_array($OldPICID) && in_array($staffList[$i][0],$OldPICID)) {
		$SelectPICList .= "<option value='".$temp [$i][0]."'>".$staffList[$i][1]."</option>";
	}
}
for($a=0;$a<sizeof($staffList);$a++)
{
	if (is_array($OldPICID) && in_array($staffList[$a][0],$OldPICID)) {
		
		$PICUser[] = array($staffList[$a]['UserID'],$staffList[$a][1]);
	}	
}

$selectPIC = $linterface->GET_SELECTION_BOX($PICUser,"id=\"SelectTeacher[]\" name=\"SelectTeacher[]\"  multiple='multiple'","");

$linterface->LAYOUT_START();

?>
<script type="text/javascript">
<?=$js_NumOfAssignedStudent;?>
function deleteAllTeacher() {
	obj = document.getElementById('SelectTeacher[]');
	checkOptionClear(obj);
	checkOptionAdd(obj, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}

function checkForm() 
{	
	var f = document.FormEdit;
	if ((f.StartHour.value+''+f.StartMin.value)>=(f.EndHour.value+''+f.EndMin.value)) {
		alert('<?=$i_Discipline_StartEnd_Time_Alert?>');
		return false;
	}  
	
	<?php if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){ ?>
		if(document.getElementById('sessiontype').value == 0){
			alert('<?=$Lang['eDiscipline']['WarningMsgArr']['SelectDetentionTypes']?>');
			return false;
		}
	<?php } ?>
	
	if (f.TextLocation.value=='') {
		alert('<?=$i_alert_pleasefillin.$i_Discipline_Location?>');
		return false;
	} 
	if ((f.TextVacancy.value+' '!=Math.round(f.TextVacancy.value)+' ') || (!(f.TextVacancy.value>0)) || (f.TextVacancy.value<js_NumOfAssignedStudent)) {
		alert('<?=$i_Discipline_Invalid_Vacancy?>');
		return false;
	}
	
	var Checked = false;
	for (i=0; i< <?=sizeof($ClassLvlArr)?>; i++)
	{
		var classLevelChkBox = document.getElementById('classlvl'+i);
		if (classLevelChkBox.checked)
		{
			Checked = true;
		}
	}
	if (!Checked) {
		alert("<?=$i_alert_pleaseselect.$i_Discipline_Applicable_Form?>");
		return false;
	}
	
	//obj = document.getElementById('SelectTeacher[]');
	obj = document.FormEdit.elements['SelectTeacher[]'];
	
	if (obj.length==0) {
		//checkOptionAdd(obj, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
		alert("<?=$i_alert_pleaseselect.$i_Discipline_Duty_Teacher?>");
		return false;
	}

	checkOptionAll(obj);
	return true;
}
	

</script>

<br />
<form name="FormEdit" method="POST" action="<?=$ActionPage?>" onSubmit="return checkForm();">
<table width="98%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION4($PAGE_NAVIGATION); ?></td>
	</tr>
</table>

<table width="86%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_School_Year?></td>
					<td valign="top"><?=intranet_htmlspecialchars($OldYear)?></td>
				</tr>
<?=$select_sem_html?>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
						<span class="tabletext"><?=$i_Discipline_Date?></span>
					</td>
					<td><?=$OldDetentionDate?></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Time?> <span class="tabletextrequire">*</span></span></td>
					<td><?=$i_From?> <?=$StartHour?> : <?=$StartMin?> <?=$i_To?> <?=$EndHour?> : <?=$EndMin?></td>
				</tr>
				
				<?php if($sys_custom['eDiscipline']['CreativeSchDetentionCust']){ ?>
					<tr valign='top'>
						<td valign="top" nowrap class="formfieldtitle"> <?=$Lang['eDiscipline']['DetentionTypes']['Title']?> <span class="tabletextrequire">*</span></td>
						<td><?=$sessiontype_selection?></td>
					</tr>
				<?php } ?>
				
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Applicable_Form?> <span class="tabletextrequire">*</span></td>
					<td valign="top"><?=$ClassLvlChk?></td>
				</tr>
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Location?> <span class="tabletextrequire">*</span></td>
					<td valign="top"><input name="TextLocation" class="textboxtext" maxLength="255" value="<?=intranet_htmlspecialchars($OldLocation)?>"></td>
				</tr>
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Duty_Teacher?> <span class="tabletextrequire">*</span></td>
					<td>
						<table>
							<tr>
								<td>
									<?=$selectPIC?>

									</select>
								</td>
								<td valign="bottom">
<?=$linterface->GET_BTN($i_Discipline_Select, "button","newWindow('../../choose_pic.php?fieldname=SelectTeacher[]',9)");?><br />
<?=$linterface->GET_BTN($i_Discipline_Remove, "button","checkOptionRemove(document.FormEdit.elements['SelectTeacher[]'])");?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap class="formfieldtitle"><?=$i_Discipline_Vacancy?> <span class="tabletextrequire">*</span></td>
					<td valign="top"><input name="TextVacancy" class="textboxnum" maxLength="4" value="<?=$OldVacancy?>"> <?=$html_show_current_assigned_students;?> </td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td align="left" class="tabletextremark"><?=$i_general_required_field?></td>
				</tr>
				<tr>
					<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="center">
<?= $linterface->GET_ACTION_BTN($button_save, "submit","")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "deleteAllTeacher();")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "$BackBtnScript")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" id="back_par" name="back_par" value="<?=$back_par?>">
<input type="hidden" id="NumOfAssignedStudent" value="<?=$NumOfAssignedStudent[0];?>">
<input type="hidden" id="DetentionID" name="DetentionID" value="<?=$did?>">
<input type="hidden" id="DetentionDate" name="DetentionDate" value="<?=$OldDetentionDate?>">
<input type="hidden" id="disabled" name="disabled" value="<?=$disabled?>">
<input type="hidden" id="cmonth" name="cmonth" value="<?=$cmonth?>">
</form>
<br />
<?
if($NumOfAssignedStudent[0] == 0 && $disabled==""){
	print $linterface->FOCUS_ON_LOAD("FormEdit.StartHour");
}
$linterface->LAYOUT_STOP();
intranet_closedb();
?>