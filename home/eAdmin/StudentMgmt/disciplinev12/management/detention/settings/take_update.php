<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-TakeAttendance")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
else
{
$SuccessFlag = true;

$UserID = $_SESSION["UserID"];
$StudentID = explode(",", $studentid);
for($i=0;$i<sizeof($StudentID);$i++) {
	$TmpRemark = "'".${"textAttendanceRemark".$StudentID[$i]}."'";
	if ($TmpRemark == "''") $TmpRemark = "NULL";
	$DetentionArr['AttendanceStatus'] = "'${"select".$StudentID[$i]}'";
	$DetentionArr['AttendanceRemark'] = $TmpRemark;
	$DetentionArr['AttendanceTakenBy'] = $UserID;
	$DetentionArr['AttendanceTakenDate'] = "NOW()";
	$result = $ldiscipline->updateDetentionByStudentIDDetentionID($DetentionArr, $StudentID[$i], $did);
	if (!$result) $SuccessFlag = false;
}

if ($SuccessFlag) {
	$SysMsg = "update";
} else {
	$SysMsg = "update_failed";
}

intranet_closedb();
$ReturnPage = "take_attendance.php";
header("Location: $ReturnPage?did=$did&msg=$SysMsg");
}
?>