<?php
// Modifying by: yat

######################################## Change Log #################################################
#
#	Date	: 	2010-05-07 YatWoon
#				According to the setting $PIC_SelectionDefaultOwn to check PIC default selected value
#
############################################ End ###################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Detention-Access");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface = new interface_html();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "../", 0);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 1);

if($msg2==11) {
	$xmsg2 = $Lang['eDiscipline']['PIC_Not_Available'];	
}

$SysMsg = $linterface->GET_SYS_MSG($msg, $xmsg2);

$menuBar = $linterface->GET_LNK_NEW("new.php?cpage=list", $button_new,"","","",0);

if ($cmonth == "") $cmonth = date("Ym");
$CurrentYear = substr($cmonth, 0, 4);
$CurrentMonth = substr($cmonth, 4, 2);
$NoOfDays = date("t", mktime(0,0,0,$CurrentMonth,1,$CurrentYear));
$CurrentDay = date("Y-m-d");
$CurrentTime = date("H:i", time());


// Get list of available DetentionID and list of full DetentionID
$sql = "SELECT DetentionID, Vacancy FROM DISCIPLINE_DETENTION_SESSION";
$result = $ldiscipline->returnArray($sql, 2);
for ($i=0; $i<sizeof($result); $i++) {
	list($DetentionIDRec, $VacancyRec) = $result[$i];
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE DetentionID = '".$DetentionIDRec."'";
	$resultCount = $ldiscipline->returnArray($sql, 1);
	if ($VacancyRec > $resultCount[0][0]) {
		$AvailableDetentionID[] = $DetentionIDRec;
	} else {
		$FullDetentionID[] = $DetentionIDRec;
	}
}

// Get Distinct Period
$AllPeriod = $ldiscipline->getSessionPeriod();

// Get Distinct PIC
$AllPIC = $ldiscipline->getSessionPIC();

// Get list of DetentionID with selected PIC
if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	$result = $ldiscipline->getDetentionIDByPIC($targetPIC);
	for ($i=0; $i<sizeof($result); $i++) {
		list($PICDetentionID[]) = $result[$i];
	}
}

if ($targetDate=="") $targetDate = "This_Week";		// default: filter this week
if (!isset($field)) $field = 0;		// default: sort by date
if (!isset($order)) $order = 0;		// default: desc order
$pageSizeChangeEnabled = true;
if ($page_size_change == 1) {
	setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
	$ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$li = new libdbtable2007($field, $order, $pageNo);

$conds = " WHERE 1=1";
if ($targetDate == "This_Month") {
	$conds .= " AND YEAR(DetentionDate) = ".date("Y")." AND MONTH(DetentionDate) = ".date("n");
} else if ($targetDate == "This_Week") {
	if (date("w") == 0) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-0 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+6 days"))."'";
	} else if (date("w") == 1) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-1 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+5 days"))."'";
	} else if (date("w") == 2) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-2 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+4 days"))."'";
	} else if (date("w") == 3) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-3 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+3 days"))."'";
	} else if (date("w") == 4) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-4 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+2 days"))."'";
	} else if (date("w") == 5) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-5 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+1 days"))."'";
	} else if (date("w") == 6) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("-6 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+0 days"))."'";
	}
} else if ($targetDate == "Next_Week") {
	if (date("w") == 0) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+7 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+13 days"))."'";
	} else if (date("w") == 1) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+6 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+12 days"))."'";
	} else if (date("w") == 2) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+5 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+11 days"))."'";
	} else if (date("w") == 3) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+4 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+10 days"))."'";
	} else if (date("w") == 4) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+3 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+9 days"))."'";
	} else if (date("w") == 5) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+2 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+8 days"))."'";
	} else if (date("w") == 6) {
		$conds .= " AND DetentionDate >= '".date('Y-m-d', strtotime("+1 days"))."' AND DetentionDate <= '".date('Y-m-d', strtotime("+7 days"))."'";
	}
}

if ($targetStatus == "Available") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DetentionID IN (".implode(', ', $AvailableDetentionID).")";
} else if ($targetStatus == "Full") {
	$conds .= " AND (DetentionDate > '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime > '$CurrentTime:00'))";
	$conds .= " AND DetentionID IN (".implode(', ', $FullDetentionID).")";
} else if ($targetStatus == "Finished") {
	$conds .= " AND (DetentionDate < '$CurrentDay' OR (DetentionDate = '$CurrentDay' AND StartTime < '$CurrentTime:00'))";
}

if (($targetPeriod != "All_Time") && ($targetPeriod != "")) {
	$conds .= " AND CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) = '$targetPeriod'";
}

if (($targetPIC != "All_Duty") && ($targetPIC != "")) {
	$conds .= " AND DetentionID IN (".implode(', ', $PICDetentionID).")";
}

/*
$sql = "SELECT DetentionID, DetentionDate, CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) as Period, Location
				, 'Dummy' as Form, 'Dummy' as Duty_Teacher, Vacancy, 'Dummy' as Assigned
				, CONCAT('<input type=checkbox name=RecordID[] value=', DetentionID ,'>') as CheckBoxField, StartTime, EndTime
				FROM DISCIPLINE_DETENTION_SESSION".$conds;
*/

$sql = "SELECT 
				DetentionID, 
				DetentionDate, 
				CONCAT(SUBSTRING(StartTime,1,5), '-', SUBSTRING(EndTime,1,5)) as Period, 
				Location, 
				'Dummy' as Form, 
				'Dummy' as Duty_Teacher, 
				Vacancy, 
				'Dummy' as Assigned, 
				CONCAT('<input type=checkbox name=RecordID[] value=', DetentionID ,'>') as CheckBoxField, 
				StartTime, 
				EndTime
		FROM 
				DISCIPLINE_DETENTION_SESSION".$conds;
				
$li->field_array = array("DetentionDate", "Period", "Location", "Form", "Duty_Teacher", "Vacancy", "Assigned");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $i_Discipline_Records;
$li->column_array = array(14,14,14,14,14,14,14,14,14);
$li->IsColOff = "eDisciplineDetentionSetting";
$li->AllFormsData = $i_Discipline_All_Forms;


// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='5%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_Date)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_Time)."</td>\n";
$li->column_list .= "<td width='14%'>".$li->column($pos++, $i_Discipline_Location)."</td>\n";
$li->column_list .= "<td width='14%'>".$li->column($pos++, $i_Discipline_Form)."</td>\n";
$li->column_list .= "<td width='32%'>".$i_Discipline_Duty_Teacher."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Discipline_Vacancy)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_Discipline_Assigned)."</td>\n";
$li->column_list .= "<td width='5%'>".$li->check("RecordID[]")."</td>\n";

# filters
$select_date = "<SELECT name=\"targetDate\" onChange=\"this.form.submit()\">\n";
$select_date .= "<OPTION value='All_Date' ".($targetDate=="All_Date"?"SELECTED":"").">$i_Discipline_All_Week</OPTION>\n";
$select_date .= "<OPTION value='This_Month' ".($targetDate=="This_Month"?"SELECTED":"").">$i_Discipline_This_Month2</OPTION>\n";
$select_date .= "<OPTION value='This_Week' ".($targetDate=="This_Week"?"SELECTED":"").">$i_Discipline_This_Week2</OPTION>\n";
$select_date .= "<OPTION value='Next_Week' ".($targetDate=="Next_Week"?"SELECTED":"").">$i_Discipline_Next_Week</OPTION>\n";
$select_date .= "</SELECT>\n";

$select_status = "<SELECT name=\"targetStatus\" onChange=\"this.form.submit()\">\n";
$select_status .= "<OPTION value='All_Status' ".($targetStatus=="All_Status"?"SELECTED":"").">$i_Discipline_All_Status</OPTION>\n";
$select_status .= "<OPTION value='Available' ".($targetStatus=="Available"?"SELECTED":"").">$i_Discipline_Available</OPTION>\n";
$select_status .= "<OPTION value='Full' ".($targetStatus=="Full"?"SELECTED":"").">$i_Discipline_Full</OPTION>\n";
$select_status .= "<OPTION value='Finished' ".($targetStatus=="Finished"?"SELECTED":"").">$i_Discipline_Finished</OPTION>\n";
$select_status .= "</SELECT>\n";

$select_period = "<SELECT name=\"targetPeriod\" onChange=\"this.form.submit()\">\n";
$select_period .= "<OPTION value='All_Time' ".($targetPeriod=="All_Time"?"SELECTED":"").">$i_Discipline_All_Time</OPTION>\n";
for ($i=0; $i<sizeof($AllPeriod); $i++) {
	list($TempPeriod) = $AllPeriod[$i];
	$select_period .= "<OPTION value='$TempPeriod' ".($targetPeriod==$TempPeriod?"SELECTED":"").">$TempPeriod</OPTION>\n";
}
$select_period .= "</SELECT>\n";

if($ldiscipline->PIC_SelectionDefaultOwn)
{
	if(sizeof($_POST)==0 && sizeof($_GET)==0 && !isset($_POST['targetPIC'])) {		# default display own PIC records
		$flag = 0;
		for($i=0; $i<sizeof($AllPIC); $i++) {
			if($AllPIC[$i][0]==$UserID) {
				$targetPIC = $UserID;		
				$flag = 1;
				break;
			}
		}
	}
}

$select_pic = "<SELECT name=\"targetPIC\" onChange=\"this.form.submit()\">\n";
$select_pic .= "<OPTION value='All_Duty' ".($targetPIC=="All_Duty"?"SELECTED":"").">$i_Discipline_Detention_All_Teachers</OPTION>\n";
for ($i=0; $i<sizeof($AllPIC); $i++) {
	list($TempUserID, $TempPIC) = $AllPIC[$i];
	$select_pic .= "<OPTION value='$TempUserID' ".($targetPIC==$TempUserID?"SELECTED":"").">$TempPIC</OPTION>\n";
}
$select_pic .= "</SELECT>\n";

$searchbar = "$select_date $select_status $select_period $select_pic";

$linterface->LAYOUT_START();
?>
<br />
<form name="form1" method="get">

<script type="text/javascript">
	function deleteSessionAlert(did) {
		if (document.getElementById('assign'+did).innerHTML>0){
			alert("<?=$i_Discipline_Assigned_Session_Cannot_Be_Deleted?>");
			return false;
		} else {
			if(confirm("<?=$i_Discipline_Delete_Session_Alert?>")){
				return true;
			} else {
				return false;
			}
		}
	}

	function checkAssigned(){
		var len=form1.elements.length;
		var tmpDID;
		for(var i=0;i<len;i++){
			if(form1.elements[i].name=='RecordID[]' && form1.elements[i].checked){
				tmpDID=form1.elements[i].value;
				if(document.getElementById('SpanAssigned'+tmpDID).innerHTML>0){
					alert("<?=$i_Discipline_Assigned_Session_Cannot_Be_Deleted?>");
					return false;
				}
  	  }
	  }
	  return true;
	}
</script>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><?= $menuBar ?></td>
				</tr>
			</table>
		</td>
		<td align="right"><?= $SysMsg ?>&nbsp;</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="28" align="right" valign="bottom" class="tabletextremark">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr></tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="30">&nbsp;</td>
					<td align="right" valign="middle" class="thumb_list">
						<a href="calendar.php"><?=$i_Discipline_Calendar?></a> | <span><?=$i_Discipline_Session?></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<table width="97%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table border="0" cellspacing="0" cellpadding="2">
				<tr>
					<td><?= $searchbar; ?></td>
				</tr>
			</table>
		</td>
		<td align="right" valign="bottom">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td nowrap>
									<a href="javascript:if(checkAssigned()){checkRemove(document.form1,'RecordID[]','delete_update.php')}" class="tabletool">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_delete?></a>
								</td>
								<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
								<td nowrap>
									<a href="javascript:checkEdit(document.form1,'RecordID[]','edit.php')" class="tabletool">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit?></a>
								</td>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<?php echo $li->display(); ?>
<br>

<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="cpage" value="list" />
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>