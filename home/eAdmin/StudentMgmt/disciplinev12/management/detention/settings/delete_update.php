<?php
// Modifying by: yat

#############################
#
#	Date:	2013-10-25	YatWoon
#			Add back $cmonth variable for back to previous calender month [Case#2013-1016-1535-14073]
#			
#############################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Detention-Access");

$SuccessFlag = true;

if (is_array($RecordID) && $RecordID[0] <> "") {	// from list.php - can delete more than one record
	for ($i=0; $i<sizeof($RecordID); $i++) {
		$did = $RecordID[$i];

		$result = $ldiscipline->deleteDetentionPICByDetentionID($did);
		if (!$result) $SuccessFlag = false;

		$result = $ldiscipline->deleteDetentionClassLevelByDetentionID($did);
		if (!$result) $SuccessFlag = false;

		$result = $ldiscipline->deleteDetentionSessionByDetentionID($did);
		if (!$result) $SuccessFlag = false;
	}
	$ReturnPage = "list.php";
} else {	// from calendar.php - can delete one record only
	$result = $ldiscipline->deleteDetentionPICByDetentionID($did);
	if (!$result) $SuccessFlag = false;

	$result = $ldiscipline->deleteDetentionClassLevelByDetentionID($did);
	if (!$result) $SuccessFlag = false;

	$result = $ldiscipline->deleteDetentionSessionByDetentionID($did);
	if (!$result) $SuccessFlag = false;

	$ReturnPage = "calendar.php";
}

if ($SuccessFlag) {
	$SysMsg = "delete";
} else {
	$SysMsg = "delete_failed";
}

intranet_closedb();
header("Location: $ReturnPage?cmonth=$cmonth&msg=$SysMsg");
?>