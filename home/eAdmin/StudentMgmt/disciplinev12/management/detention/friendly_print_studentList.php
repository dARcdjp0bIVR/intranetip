<?php
// Modifying by: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-View");

# detention session data
$detentionInfo = $ldiscipline->getDetentionListByDetentionID($did);
list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $detentionInfo[0];
$detentionDate = substr($TmpDetentionDate,0,4)."/".substr($TmpDetentionDate,5,2)."/".substr($TmpDetentionDate,8,2);

# school badge (if any)
if(file_exists($PATH_WRT_ROOT."file/CSWCSS_logo_01.jpg"))
	$schoolBadge = "<td width=\"10%\"><img src='".$PATH_WRT_ROOT."file/CSWCSS_logo_01.jpg"."' border=\"0\" width=\"70\" height=\"90\"></td>";

# header of print out (school name)
$print_header = $Lang['SchoolName']['CSWCCS']['CH']."<br>".$Lang['SchoolName']['CSWCCS']['EN']."<p>".$Lang['eDiscipline']['DetentionClass']." ($detentionDate)";

$name_field = getNameFieldByLang("USR.");
$sql = "SELECT USR.ClassName, USR.ClassNumber, USR.ChineseName, USR.EnglishName, DSS.Reason, PICID FROM DISCIPLINE_DETENTION_STUDENT_SESSION DSS LEFT OUTER JOIN INTRANET_USER USR on (USR.UserID=DSS.StudentID) WHERE DSS.DetentionID=$did";
$result = $ldiscipline->returnArray($sql);

$tableContentPage2 = "";

for($i=0; $i<sizeof($result); $i++) {
	list($clsName, $clsNo, $CH_Name, $EN_Name, $offence, $pic) = $result[$i];
	if($offence=="") $offence = "---";
	$picName = ($ldiscipline->getPICNameList($pic)!="") ? $ldiscipline->getPICNameList($pic) : "---"; 
	
	$tableContent .= "<tr class=\"row_print\">";
	$tableContent .= "<td valign=\"top\">$clsName</td>";
	$tableContent .= "<td valign=\"top\">$clsNo</td>";
	$tableContent .= "<td valign=\"top\">$CH_Name<br>$EN_Name</td>";
	$tableContent .= "<td valign=\"top\">$offence</td>";
	$tableContent .= "<td valign=\"top\">".$picName."</td>";
	$tableContent .= "<td valign=\"top\">&nbsp;</td>";
	$tableContent .= "<td valign=\"top\">&nbsp;</td>";
	$tableContent .= "<td valign=\"top\">&nbsp;</td>";
	$tableContent .= "<td valign=\"top\">&nbsp;</td>";
	$tableContent .= "</tr>";


	$tableContentPage2 .= ($tableContentPage2== "") ? "<div style='page-break-after:always'>&nbsp;</div>" : "";
	$tableContentPage2 .= "<table width='95%'><tr><td><font face='Times New Roman'>".$Lang['eDiscipline']['CSWCSS_DetentionMsg_1'].$clsName." ".$CH_Name." ".$EN_Name." ($clsNo)".$Lang['eDiscipline']['CSWCSS_DetentionMsg_2'].$offence."<br>".$Lang['eDiscipline']['CSWCSS_DetentionMsg_3']." $detentionDate ".$Lang['eDiscipline']['CSWCSS_DetentionMsg_4']."</font></td></tr><tr><td align='right' height='42'>".$Lang['eDiscipline']['CSWCSS_DetentionMsg_5']."</td></tr></table>";


}

if(sizeof($result)==0) {
	$tableContent .= "<tr><td colspan='9' align='center' height='40'>$i_no_record_exists_msg</td></tr>";
}
else {
	for($i=0; $i<40; $i++) 
		$space .= "&nbsp;";
		
	
	$footer = "<div align=\"right\">".$Lang['eDiscipline']['Total']." ".sizeof($result)." ".$Lang['eDiscipline']['Records']."</div>";
	
	$footer .= "<p>&nbsp;</p><p>&nbsp;</p>";
	$footer .= "<table width='95%' border='0' cellspacing='0' cellpadding='0' align='center'>";
	$footer .= "<tr>";
	$footer .= "<td width='50%'>".$Lang['eDiscipline']['PrefectInCharge'].":<u>$space</u></td>";
	$footer .= "<td align='right' width='50%'>".$Lang['eDiscipline']['CheckBy'].":<u>$space</u></td>";
	$footer .= "</tr></table>";
	
	
	//$footer .= "<div style='page-break-after:always'>&nbsp;</div>";
	
	//$footer .= $tableContentPage2;
}

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/edis_print_header.php");

?>
<style type='text/css'>
 td {font-family:"Times New Roman";font-size:12pt}
</style>
<table width="100%" align="center">
	<tr>
		<td align="right" class="print_hide">
			<input name="btn_print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">
		</td>
	</tr>
</table>
<table cellspacing="0" cellpadding="4" width="90%" border="0">
	<tbody>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<?=$schoolBadge?>
						<td class="report_title" align="center"><?=$print_header?></td>
					</tr>
				</table>
				<br>
				<br />
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
					<tr>
						<td valign="top">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="tableborder_print">
								<tr class="tabletop_print">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><?=$i_general_name?></td>
									<td><?=$Lang['eDiscipline']['Offence']?></td>
									<td><?=$Lang['eDiscipline']['SentBy']?></td>
									<td><?=$Lang['eDiscipline']['Attend']?></td>
									<td><?=$Lang['eDiscipline']['ReplySlip']?></td>
									<td><?=$Lang['eDiscipline']['NewDetentionDate']?></td>
									<td><?=$i_UserRemark?></td>
								</tr>
<?=$tableContent?>
							</table>
							<?=$footer?>
							
						</td>
					</tr>
					<tr>
						<td>
							<?=$tableContentPage2?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<br />
<?
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>
