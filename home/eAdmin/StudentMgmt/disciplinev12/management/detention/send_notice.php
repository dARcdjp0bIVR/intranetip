<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#	Date	:	2017-10-30 (Bill)	[2017-1025-1004-03206]
#				fixed: cannot send notice if $emailNotify != 1
#
#	Date	:	2015-04-20 (Bill)	[2014-1216-1347-28164]
#				send push message to parent when $eClassAppNotify = 1
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	$luser = new libuser();
	$leClassApp = new libeClassApp();
	$canSendPushMsg = true;
}

# Check access right
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

// Get submitted data
$submitRecordCount = $_POST["recordCount"];
for ($i=1; $i<=$submitRecordCount; $i++) {
	${"submitRecordID".$i} = $_POST["RecordID".$i];
	${"submitSelectNotice".$i} = $_POST["SelectNotice".$i];
	${"submitTextAdditionalInfo".$i} = stripslashes($_POST["TextAdditionalInfo".$i]);
	${"submitStudentID".$i} = $_POST["StudentID".$i];
	${"submitStudentName".$i} = $_POST["StudentName".$i];
}

$UserID = $_SESSION["UserID"];
$UserName = $ldiscipline->getUserNameByID($UserID);

$lc = new libucc();

$successFlag = true;
for ($i=1; $i<=$submitRecordCount; $i++)
{
		# Send eNotice
		$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO(${"submitSelectNotice".$i});
		if(sizeof($template_info))
		{
			$template_category = $template_info[0]['CategoryID'];

			$SpecialData = array();
			$SpecialData['StudentName'] = '';
			$SpecialData['AdditionalInfo'] = '';
			$SpecialData['DetentionRecordID'] = ${"submitRecordID".$i};
			
			$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($template_category, ${"submitStudentID".$i}, $SpecialData, ${"submitTextAdditionalInfo".$i});
			$Module = $ldiscipline->Module;
			$NoticeNumber = time();
			$TemplateID = ${"submitSelectNotice".$i};
			$Variable = $template_data;
			$IssueUserID = $UserID;
			$IssueUserName = $UserName[0];
			$TargetRecipientType = "U";
			$RecipientID = array(${"submitStudentID".$i});
			$RecordType = NOTICE_TO_SOME_STUDENTS;
			$emailNotify = ${"emailNotify".$i};
			
			// [2014-1216-1347-28164] eClass App Push Message Settings
			$parentCanReceiveMsg = $canSendPushMsg? $luser->getParentUsingParentApp($RecipientID, true) : false;
			if($parentCanReceiveMsg){
				$eClassAppNotify = ${"eClassAppNotify".$i};
				$notifyMessageId = -1;
			}
			
			if (!$lnotice->disabled) 
			{
				$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

                // [2020-1009-1753-46096] delay notice start date & end date
                if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                {
                    $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                    $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                    $lc->setDateStart($DateStart);

                    if ($lc->defaultDisNumDays > 0) {
                        $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                        $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                        $lc->setDateEnd($DateEnd);
                    }
                }

                $emailNotify = $emailNotify? 1 : 0;
				$NoticeID = $lc->sendNotice($emailNotify);
			}
			
			// [2014-1216-1347-28164] Send eClass App Push Message
			if($parentCanReceiveMsg && $eClassAppNotify)
			{
				// Set notice parameters if not set
				if($lnotice->disabled){
					$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);
				}
				// Structure data for sending message
				$lc->restructureData();
				
				// Get ParentID and StudentID Mapping
				$sql = "SELECT 
								ip.ParentID, ip.StudentID
						FROM 
								INTRANET_PARENTRELATION AS ip 
								Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
						WHERE 
								ip.StudentID IN ('".implode("','", (array)$RecipientID)."') 
								AND iu.RecordStatus = 1";
				$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
				
				// Message Title and Content
				$messageInfoAry = array();
				$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
				$messageContent = removeHTMLtags($lc->Description);
				$messageContent = intranet_undo_htmlspecialchars($messageContent);
				$messageContent = str_replace("&nbsp;", "", $messageContent);
				$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $lc->Subject, $messageContent);
			}
			
			// [2014-1216-1347-28164] Success flag handling
			if($canSendPushMsg){
				if($NoticeID != -1) {
					$ldiscipline->updateNoticeID(${"submitRecordID".$i}, $NoticeID);
				}
				if((!$lnotice->disabled && $emailNotify && $NoticeID == -1) || 
					($parentCanReceiveMsg && $eClassAppNotify && ($notifyMessageId == -1 || $notifyMessageId == false))){
					$successFlag = false;
				}
			}
			else {
				if($NoticeID != -1) {
					$ldiscipline->updateNoticeID(${"submitRecordID".$i}, $NoticeID);
				}
				else {
					$successFlag = false;
				}
			}
		}
}

if ($successFlag) {
	$SysMsg = "add";
}
else {
	$SysMsg = "add_failed";
}

intranet_closedb();

$ReturnPage = "student_record.php";
if($from == "calendar"){
	header("Location: calendar.php?msg=$SysMsg&rid=$rid");
}
else{
	header("Location: $ReturnPage?msg=$SysMsg&rid=$rid");
}
?>