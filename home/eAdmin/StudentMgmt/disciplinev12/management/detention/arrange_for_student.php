<?php
# using:

#################################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
	
$ldiscipline = new libdisciplinev12();

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$CurrentPage = "Management_Detention";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Detention_Arrangement'], "", 1);
$TAGS_OBJ[] = array($eDiscipline['Session_Management'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/management/detention/settings/calendar.php", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_Detention_List, "list.php");
$PAGE_NAVIGATION[] = array($i_Discipline_New_Student_Records, "");

$STEPS_OBJ[] = array($i_Discipline_Select_Student, 0);
$STEPS_OBJ[] = array($i_Discipline_Add_Record, 1);
$STEPS_OBJ[] = array($i_Discipline_Enotice_Setting, 0);

$linterface = new interface_html();

$RecordID = IntegerSafe($RecordID);
if($from == 'calendar') {
	$RecordID = explode(",", $RecordID);
}
if(is_array($RecordID)) {
	$targetRecordID = implode(",", $RecordID);
}
else{
	header("location: student_record.php");
}

$sql = "SELECT 
				RecordID, 
				StudentID, 
				DetentionID, 
				IF(Reason = '', ' - ', IF(Reason IS NULL, ' - ', Reason)),
				IF(Remark = '', ' - ', IF(Remark IS NULL, ' - ', Remark)),
				IF(AttendanceStatus = '', ' - ', IF(AttendanceStatus IS NULL, ' - ', AttendanceStatus)),
				IF(AttendanceRemark = '', ' - ', IF(AttendanceRemark IS NULL, ' - ', AttendanceRemark)),
				RelatedTo
		FROM 
				DISCIPLINE_DETENTION_STUDENT_SESSION 
		WHERE 
				RecordID IN ($targetRecordID) AND RecordStatus = 1";
$arr_result = $ldiscipline->returnArray($sql,6);
if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($record_id, $student_id, $detention_id, $reason, $remark, $attendance_status, $attendance_remark, $related_to) = $arr_result[$i];
        
		$resultStudentName = $ldiscipline->getStudentNameByID($student_id);
		list($temp_studentID, $student_name, $class_name, $class_number) = $resultStudentName[0];
		
		$arrTempStudentID[] = $student_id;
		$arrStudentName[$student_id] = $class_name."-".$class_number." ".$student_name;
		$arrStudentRecordID[$student_id][] = $record_id;
		$arrDetentionRecord[$record_id]['DetentionID'] = $detention_id;
		$arrDetentionRecord[$record_id]['Reason'] = $reason;
		$arrDetentionRecord[$record_id]['Remark'] = $remark;
		$arrDetentionRecord[$record_id]['AttendanceStatus'] = $attendance_status;
		$arrDetentionRecord[$record_id]['AttendanceRemark'] = $attendance_remark;
		$arrDetentionRecord[$record_id]['RelatedTo'] = $related_to;
	}
}

//$arrStudentID = array_unique($arrTempStudentID);
$arrTempStudentID = array_unique($arrTempStudentID);
foreach($arrTempStudentID as $key=>$value)
{
	$arrStudentID[] = $value;
}
$targetStudentID = implode(",", $arrStudentID);

$TableResult = "<table border='0' width='100%' cellpadding='3' cellspacing='3'>\n";

if(sizeof($arrStudentID) > 0)
{
	for($i=0; $i<sizeof($arrStudentID); $i++)
	{
		$StudentID = $arrStudentID[$i];
		$StudentName = $arrStudentName[$StudentID];
		$TotalNumOfDetention = sizeof($arrStudentRecordID[$StudentID]);
		
		$TableResult .= "<tr>\n";
		$TableResult .= "<td height=\"10px\" colspan=\"2\" ></td>\n";
		$TableResult .= "</tr>\n";
		$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td colspan=\"2\" valign=\"top\" nowrap=\"nowrap\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$StudentName</span></td></tr>\n";
		$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['DetentionTimes']."</td><td>$TotalNumOfDetention</td></tr>\n";
		
		foreach($arrStudentRecordID[$StudentID] as $key=>$RecordID)
		{
			$DetentionSessionSelection = $ldiscipline->getSelectSessionString_New($i+1, $key+1, $StudentID);
			$DetentionID = $arrDetentionRecord[$RecordID]['DetentionID'];
			$Reason = $arrDetentionRecord[$RecordID]['Reason'];
			$Remark = $arrDetentionRecord[$RecordID]['Remark'];
			$AttendanceStatus = $arrDetentionRecord[$RecordID]['AttendanceStatus'];
			
			$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" colspan=\"2\">\n";
			$TableResult .= "<table width='100%' cellpadding='3' cellspacing='3' style=\"border: 1px 1px 1px 1px dashed;\" >\n";
			if($arrDetentionRecord[$RecordID]['AttendanceStatus'] == "ABS")
			{
				$LastDetentionRecord = $ldiscipline->displayDetentionSessionString($DetentionID);
				
				$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['LastDetentionSession']."</td><td>".$LastDetentionRecord."</td></tr>\n";
				$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['DetentionAbsentRemark']."</td><td>".$arrDetentionRecord[$record_id]['AttendanceRemark']."</td></tr>\n";
				$TableResult .= "<input type='hidden' name='AbsentRecord_".$StudentID."_".($key+1)."' id='AbsentRecord_".$StudentID."_".($key+1)."' value='1'>\n";
			}
			if($arrDetentionRecord[$RecordID]['RelatedTo'] != "")
			{
				$sql = "SELECT DetentionID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordStatus = 1 AND RecordID = '".$arrDetentionRecord[$record_id]['RelatedTo']."'";
				$absent_detention_id = $ldiscipline->returnVector($sql);
				
				$LastDetentionRecord = $ldiscipline->displayDetentionSessionString($absent_detention_id[0]);
				$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['LastDetentionSession']."</td><td>".$LastDetentionRecord."</td></tr>\n";
				$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['DetentionAbsentRemark']."</td><td>".$arrDetentionRecord[$record_id]['AttendanceRemark']."</td></tr>\n";
				$TableResult .= "<input type='hidden' name='AbsentRecord_".$StudentID."_".($key+1)."' id='AbsentRecord_".$StudentID."_".($key+1)."' value='1'>\n";
			}
			$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['DetentionReason']."</td><td>$Reason</td></tr>\n";
			$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['DetentionRemark']."</td><td>$Remark</td></tr>\n";
			$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'>".$Lang['eDiscipline']['FieldTitle']['DetentionSession']."</td><td>$DetentionSessionSelection <span><font id=\"Error_".$RecordID."\" color='red'></font></span></td></tr>\n";
			//$TableResult .= "<tr valign=\"top\" class=\"tablerow1\"><td nowrap=\"nowrap\" class=\"formfieldtitle\" width='40%'></td><td><span id=\"Error_".$RecordID."\" ></span></td></tr>\n";
			$TableResult .= "<input type='hidden' name='RecordBelongTo_".$StudentID."_".($key+1)."' id='RecordBelongTo_".$StudentID."_".($key+1)."' value=$RecordID>\n";
			$TableResult .= "</table>\n";
			$TableResult .= "</td></tr>\n";
		}
		
		$TableResult .= "<tr>\n";
		$TableResult .= "<td height=\"10px\" colspan=\"2\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td>\n";
		$TableResult .= "</tr>\n";
		$TableResult .= "<tr>\n";
		$TableResult .= "<td height=\"10px\" colspan=\"2\" ></td>\n";
		$TableResult .= "</tr>\n";
		$TableResult .= "<input type='hidden' name='NumOfRecord_".$StudentID."' id='NumOfRecord_".$StudentID."' value=$TotalNumOfDetention>\n";
	}
}
$TableResult .= "<input type='hidden' name='TargetRecordID' id='TargetRecordID' value='".$targetRecordID."'>\n";
$TableResult .= "<input type='hidden' name='TargetStudentID' id='TargetStudentID' value='".$targetStudentID."'>\n";
$TableResult .= "<input type='hidden' name='from' id='from' value='".$from."'>\n";
$TableResult .= "</table>\n";

$linterface->Layout_Start();
?>

<script language='javascript'>
function checkForm()
{
	var pass = 0;
	var evalStr = 'var PostVar ={';
	document.form1.action='';
	<? 
	for($i=0; $i<sizeof($arrStudentID); $i++) 
	{
		$StudentID = $arrStudentID[$i];
		foreach($arrStudentRecordID[$StudentID] as $key=>$RecordID)
		{ 
	?>
			var str = "$('#SelectDetentionSession_"+<?=$StudentID;?>+"_"+<?=$key+1?>+"').val()";
			var temp_val = eval(str);
			var str2 = "$('#NumOfRecord_"+<?=$StudentID;?>+"').val()";
			var temp_val2 = eval(str2);
			var str3 = "$('#RecordBelongTo_"+<?=$StudentID;?>+"_"+<?=$key+1?>+"').val()";
			var temp_val3 = eval(str3);
			
			eval("$('#Error_"+<?=$RecordID?>+"').html('');");
			
			evalStr += '"'+'SelectDetentionSession_'+<?=$StudentID;?>+'_'+<?=$key+1?>+'":'+temp_val+',';
			evalStr += '"'+'NumOfRecord_'+<?=$StudentID;?>+'":'+temp_val2+',';
			evalStr += '"'+'RecordBelongTo_'+<?=$StudentID;?>+'_'+<?=$key+1?>+'":'+temp_val3+',';
	<?
		}
	} 
	?>
	evalStr += '"TargetRecordID":'+'"'+$('#TargetRecordID').val()+'"'+',';
	evalStr += '"TargetStudentID":'+'"'+$('#TargetStudentID').val()+'"'+'}';
	
	eval(evalStr);
	
	$.post(
		"ajax_check_detention_session.php",
		PostVar
		,function(responseText){
			if(responseText != 'pass') {
				
				var temp = new Array();
				var temp2 = new Array();
				temp = responseText.split('|');
				for(i=0; i<temp.length; i++)
				{
					temp2 = temp[i].split("_");
					for(j=0; j<temp2.length; j++)
					{
						if(temp2[2] == 'NoSpace'){
							var str = "<?=$Lang['eDiscipline']['JSWarning']['NoMoreVacancy']?>";
							eval("$('#Error_"+temp2[1]+"').html('"+str+"');");								
						}else{
							var str = "<?=$Lang['eDiscipline']['JSWarning']['SessionDuplicate']?>";
							eval("$('#Error_"+temp2[1]+"').html('"+str+"');");
						}
					}
				}
				document.form1.action = "";
			} else {
				document.form1.action = "arrange_for_student_update.php";
				document.form1.submit();
			}
		}
	);
}

function applyToAll(studentCount){
	var obj = document.form1;
	var val = obj.SelectDetentionSession__1.value;
	
	$('select[name^="SelectDetentionSession_"]').val(val);	
	// assign value to all selectors where selector name started with "SelectDetentionSession_1" 
}
</script>

<form name="form1" method="post" action="">

<table width="97%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?=$linterface->GET_NAVIGATION4($PAGE_NAVIGATION)?><br /><br /><br /><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
	</tr>
</table>
<br>

<? if(sizeof($arrTempStudentID)>1) { ?>
	<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">	
		<tr>
			<td colspan="2" valign="top" nowrap="nowrap">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_section.gif" width="20" height="20" align="absmiddle">
							<span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span>
						</td>
						<td align="right">
							<span class="sectiontitle">
	<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyToAll(".sizeof($arrTempStudentID).");")?>
							</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Session?> <span class="tabletextrequire">*</span></td>
			<td valign="top"><?=$ldiscipline->getSelectSessionString_New(0, 1, "")?></td>
		</tr>
	</table>
<?}?>	
	
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<?=$TableResult;?>
		</td>
	</tr>
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_continue, "button", " checkForm() ")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>&nbsp;
						<?
							if($from == 'calendar'){
								echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='calendar.php'");
							}else{
								echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='student_record.php'");
							}
						?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<?
intranet_closedb();
$linterface->Layout_Stop();
?>