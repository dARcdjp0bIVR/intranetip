<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");

$ExportArr = array();


if ($targetClass != "All_Classes" && $targetClass != "") {
	$conds .= " AND USR.ClassName = '$targetClass'";
}

if ($targetStudent == "Assigned") {
	$conds .= " AND SES.DetentionID IS NOT NULL";
} else if ($targetStudent == "Not_Assigned") {
	$conds .= " AND SES.DetentionID IS NULL";
}

if ($targetStatus == "Detented") {
	$conds .= " AND SES.AttendanceStatus = 'PRE'";
} else if ($targetStatus == "Absent") {
	$conds .= " AND SES.AttendanceStatus = 'ABS'";
}

$name_field = getNameFieldByLang("USR.");
if ($searchString != "") {
	$conds .= " AND (CONCAT(USR.ClassName, '-', USR.ClassNumber) LIKE '%$searchString%' OR $name_field LIKE '%$searchString%' OR SES.Reason LIKE '%$searchString%' OR SES.Remark LIKE '%$searchString%')";
}

$sql = "SELECT
					CONCAT(USR.ClassName, '-', USR.ClassNumber) AS Class,
					$name_field AS Student,
					D.Semester,
					SES.Reason,
					IFNULL(SES.Remark, '') AS Remark,
					SUBSTRING(SES.DateInput,1,10) AS DateInput,
					SES.DetentionID,
					SES.AttendanceStatus,
					CONCAT('<input type=checkbox name=RecordID[] value=', SES.RecordID ,'>') AS CheckBoxField,
					SES.StudentID,
					CONCAT(D.DetentionDate, D.StartTime, D.EndTime, D.Location) AS TempForSort
				FROM
					DISCIPLINE_DETENTION_STUDENT_SESSION SES
					LEFT JOIN DISCIPLINE_DETENTION_SESSION D ON (D.DetentionID=SES.DetentionID),
					INTRANET_USER USR
				WHERE
					SES.StudentID = USR.UserID".$conds;
$row = $ldiscipline->returnArray($sql);

// Create data array for export
for($i=0; $i<sizeof($row); $i++)
{
	$TempAllForms = $ldiscipline->getAllFormString();
	$TempList = $ldiscipline->getDetentionListByDetentionID($row[$i]['DetentionID']);
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $TempList[0];
	if ($TmpForm == $TempAllForms) $TmpForm = $i_Discipline_All_Forms;
	if($row[$i]['DetentionID']=='')
	{
		$SessionString='--';
	}
	else
	{
		$SessionString = substr($TmpDetentionDate,2)." (".substr($TmpDetentionDayName,0,3).") | ".$TmpPeriod." | ".intranet_htmlspecialchars($TmpLocation)." | ".$TmpForm." | ".$TmpPIC;
	}
	
	//Attendance 
	if($row[$i]['AttendanceStatus']=="PRE")
	{
		$attendance = $i_Discipline_Attended;
	}
	else if($row[$i]['AttendanceStatus']=="ABS")
	{
		$attendance = $i_Discipline_Absent;
	}
	else
	{
		$attendance = $i_Discipline_Not_Yet;
	}
						
 	$ExportArr[$i][0] = $row[$i]['Class'];		//Class
	$ExportArr[$i][1] = $row[$i]['Student'];	//Student
	$ExportArr[$i][2] = $row[$i]['Semester'];	//Semester
	$ExportArr[$i][3] = $row[$i]['Reason'];		//Reason
	$ExportArr[$i][4] = $row[$i]['Remark'];		//Remark
	$ExportArr[$i][5] = $row[$i]['DateInput'];	//DateInput
	$ExportArr[$i][6] = $SessionString;			//SessionString
	$ExportArr[$i][7] = $attendance;		//Assigned
	$ExportArr[$i][8] = $Status;		//Status
}



//define column title
$exportColumn = array($i_Discipline_Class,$i_Discipline_Student,$i_SettingsSemester,$i_Discipline_Detention_Reason,$i_Discipline_Remark,$i_Discipline_Request_Date,$i_Discipline_Arranged_Session,$i_Discipline_Attendance);


$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output the file to user browser
$filename = 'detention_student.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>
