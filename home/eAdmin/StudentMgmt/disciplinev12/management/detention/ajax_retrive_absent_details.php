<?php
# using:

#################################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$RecordID = IntegerSafe($RecordID);
$result = $ldiscipline->getDetentionListByRecordID($RecordID);

$sql = "SELECT AttendanceRemark FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE RecordID = '$RecordID'";
$ArrAbsentRemark = $ldiscipline->returnVector($sql);

$SelectSession = "<table width=\"98%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#CCCCCC\" class=\"layer_table_detail\">";
$SelectSession .='<tr class="tabletop"><td>'.$Lang['eDiscipline']['FieldTitle']['LastDetentionSession']."</td><td>".$Lang['eDiscipline']['FieldTitle']['DetentionAbsentRemark'].'</td></tr>';

$StrArr = array('Date','Time','Location','PIC','Vacancy');

for ($i=0; $i<sizeof($result); $i++)
{
	list($TmpDetentionID[$i],$TmpDetentionDate[$i],$TmpDetentionDayName[$i],$TmpStartTime[$i],$TmpEndTime[$i],$TmpPeriod[$i],$TmpLocation[$i],$TmpForm[$i],$TmpPIC[$i],$TmpVacancy[$i],$TmpAssigned[$i],$TmpAvailable[$i]) = $result[$i];

	if ($TmpForm[$i] == $TmpAllForms) $TmpForm[$i] = $i_Discipline_All_Forms;
	$StrArr = array(substr($TmpDetentionDate[$i],2)."(".substr($TmpDetentionDayName[$i],0,3).")", $TmpPeriod[$i], $TmpLocation[$i],$TmpPIC[$i],$TmpAvailable[$i]);
	$value = $ldiscipline->displayDetentionSessionString($TmpDetentionID[$i]);
	
	if($ArrAbsentRemark[0] == "") {
		$AbsentRemark = " - ";
	}
	else {
		$AbsentRemark = $ArrAbsentRemark[0];
	}
	
	$SelectSession .= '<tr class="tablerow1"><td>'.$value.'</td>';
	$SelectSession .= '<td>'.$AbsentRemark.'</td></tr>';
}
$SelectSession .= "</table>";

$output = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="20" align="left" valign="top">
						'.$SelectSession.'
						
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';

echo $output;
intranet_closedb();
?>

<script language='javascript'>
	function Hide_Window()
	{
		$("#AbsentDetails").hide();
	}
</script>