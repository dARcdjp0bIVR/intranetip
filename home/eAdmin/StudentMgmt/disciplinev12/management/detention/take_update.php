<?php
// Modifying by:
/*
 * Date: 2020-06-10 (Bill)  [2020-0520-1043-08170]
 *       - display pic name (bilingual) in push msg
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-TakeAttendance")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
else
{

$SuccessFlag = true;

$UserID = $_SESSION["UserID"];
//$StudentID = explode(",", $studentid);
$StudentID = $sid;

$AbsentPushMessageStudentID = array();
for($i=0; $i<sizeof($StudentID); $i++)
{
	$TmpRemark = "'".${"textAttendanceRemark".$StudentID[$i]}."'";
	if ($TmpRemark == "''") {
	    $TmpRemark = "NULL";
    }
	$DetentionArr['AttendanceStatus'] = "'${"select".$StudentID[$i]}'";
	$DetentionArr['AttendanceRemark'] = $TmpRemark;
	$DetentionArr['AttendanceTakenBy'] = $UserID;
	$DetentionArr['AttendanceTakenDate'] = "NOW()";
	if($_POST['pushMessageID'.$StudentID[$i]]) {
		$AbsentPushMessageStudentID[] = $StudentID[$i];	
	}

 	$result = $ldiscipline->updateDetentionByStudentIDDetentionID($DetentionArr, $StudentID[$i], $did);
	if (!$result) {
	    $SuccessFlag = false;
    }
}

if($plugin['eClassApp'] == true)
{
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

	$luser = new libuser();
	$libeClassApp = new libeClassApp();

    // [2020-0520-1043-08170]
    //$detentionInfo = $ldiscipline->getDetentionListByDetentionID($did);
	$detentionInfo = $ldiscipline->getDetentionListByDetentionID($did, false, true);
	$DetentionDate = $detentionInfo[0][1];
	$DetentionStartTime = substr($detentionInfo[0][3],0,-3);
	$DetentionEndTime = substr($detentionInfo[0][4],0,-3);
	$DetentionLocation = $detentionInfo[0][6];
	// [2020-0520-1043-08170]
	//$DetentionPic = $detentionInfo[0][8];
    list($DetentionPicNameCh, $DetentionPicNameEn) = $detentionInfo[0][8];

	$pushMessageTitle = $Lang['eDiscipline']['PushMessage']['DetentionAbsent']['Title'];
	// [2020-0520-1043-08170]
	//$pushMessageContent = $Lang['eDiscipline']['PushMessage']['DetentionAbsent']['Content'];
    $pushMessageContent = $Lang['eDiscipline']['PushMessage']['DetentionAbsent']['Content2'];

 	//$appType = "P";
	$isPublic = "N";
	//debug_pr($luser->getParentStudentMappingInfo($studentIds));
	$parentStudentAssoAry = BuildMultiKeyAssoc($luser->getParentStudentMappingInfo($AbsentPushMessageStudentID), 'ParentID', $IncludedDBField=array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
	$studentWithParentUsingAppAry = $luser->getStudentWithParentUsingParentApp();

	$messageTitle = $pushMessageTitle;
	$individualMessageInfoAry = array();
	for ($i=0; $i<sizeof($AbsentPushMessageStudentID); $i++)
	{
		//list ($studentId, $amount, $balance, $paymentID) = $result2[$i];
		$studentId = $AbsentPushMessageStudentID[$i];
		$StudentName = $ldiscipline->Get_User_Info_By_UseID($studentId);
		$StudentChiName = $StudentName[0]['ChineseName'];
		$StudentEngName = $StudentName[0]['EnglishName'];
		$StudentClassName = $StudentName[0]['ClassName'];
		$StudentClassNo = $StudentName[0]['ClassNumber'];
		
		if(in_array($studentId,$AbsentPushMessageStudentID) && in_array($studentId,$studentWithParentUsingAppAry))
		{
			$thisStudent = new libuser($studentId);
			$appParentIdAry = $luser->getParentUsingParentApp($studentId);
	
			$_individualMessageInfoAry = array();
			foreach ($appParentIdAry as $parentId) {
				$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$studentId;
			}
			$_individualMessageInfoAry['messageTitle'] = $pushMessageTitle;
 			//$_individualMessageInfoAry['messageContent'] = str_replace("[StudentName]", $thisStudent->UserName(), $messageContent);
			
			$messageContent = str_replace("__StudentEngName__", $StudentEngName, $pushMessageContent);
			$messageContent = str_replace("__StudentChiName__", $StudentChiName, $messageContent);
			$messageContent = str_replace("__ClassName__", $StudentClassName, $messageContent);
			$messageContent = str_replace("__ClassNo__", $StudentClassNo, $messageContent);
			$messageContent = str_replace("__Date__", $DetentionDate, $messageContent);
			$messageContent = str_replace("__StartTime__", $DetentionStartTime, $messageContent);
			$messageContent = str_replace("__EndTime__", $DetentionEndTime, $messageContent);
			// [2020-0520-1043-08170]
			//$messageContent = str_replace("__TeacherName__", $DetentionPic, $messageContent);
            $messageContent = str_replace("__TeacherChiName__", $DetentionPicNameCh, $messageContent);
            $messageContent = str_replace("__TeacherEngName__", $DetentionPicNameEn, $messageContent);
			$_individualMessageInfoAry['messageContent'] = $messageContent;
			
			$individualMessageInfoAry[] = $_individualMessageInfoAry;
		}
	}

	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
	//debug_pr($individualMessageInfoAry);

	$msg = ($notifyMessageId > 0) ? $Lang['AppNotifyMessage']['ReturnMsg']['SendSuccess'] : $Lang['AppNotifyMessage']['ReturnMsg']['SendFail'];
}

if ($SuccessFlag) {
	$SysMsg = "update";
} else {
	$SysMsg = "update_failed";
}

intranet_closedb();

$ReturnPage = "take_attendance.php";
header("Location: $ReturnPage?did=$did&msg=$SysMsg");
}
?>