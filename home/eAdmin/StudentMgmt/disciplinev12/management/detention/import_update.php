<?php
// Modifying by: 

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lf = new libfilesystem();
$lu = new libuser();
$filepath = $csvfile;
$filename = $csvfile_name;


$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Detention-New");

$year = getCurrentAcademicYear();
$semester = getCurrentSemester();

$result_ary = array();
$imported_student = array();
$student = array();

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Attendance_Reason</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserRemark</td>";
$x .= "</tr>";
/*
## findout student ary
for($i=0;$i<sizeof($data);$i++)
{
	### get data
	$ClassName = $data[$i][0];
	$ClassNumber = $data[$i][1];

	### checking - valid class & class number (student found)
	$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
	if($StudentID)
		array_push($student, $StudentID);
		
}
*/
# get csv data from db
$sql = "select * from temp_detention_import";
$data = $ldiscipline->returnArray($sql);

if(sizeof($data)==0)
{
	$xmsg="import_failed";
}
else
{
	for($a=0;$a<sizeof($data);$a++)
	{
		$lu = new libuser($data[$a]['StudentID']);
		$thisClassNumber = $lu->ClassNumber;
		$thisClassName = $lu->ClassName;

		### insert into DISCIPLINE_DETENTION_STUDENT_SESSION
		$dataAry = array();
		$dataAry['StudentID'] = $data[$a]['StudentID'];
		$dataAry['Reason'] = "'".addslashes($data[$a]['Reason'])."'";
		$dataAry['Remark'] = "'".addslashes($data[$a]['Remark'])."'";
		$dataAry['RequestedBy'] = $UserID;
		$pic = explode(',', $data[$a]['PICID']);
		for($k=0; $k<sizeof($pic); $k++) {
			$temp = $ldiscipline->getUserInfoByUserLogin(trim($pic[$k]));
			
			$dataAry['PICID'] .= ($dataAry['PICID']=="") ? $temp['UserID'] : ",".$temp['UserID'];
		}
		$dataAry['PICID'] = "'".$dataAry['PICID']."'";
		//debug_pr($dataAry['PICID']); exit;

		$RecordID = $ldiscipline->insertDetention($dataAry);

		if($RecordID)
		{
			$import_success++;
			$RecordIDAry[] = $RecordID;
			
		}
		

		$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
		$x .= "<td class=\"tabletext\">".($a+1)."</td>";
		$x .= "<td class=\"tabletext\">". $thisClassName."-".$thisClassNumber ."</td>";
		$x .= "<td class=\"tabletext\">". $data[$a]['StudentName']."</td>";
		$x .= "<td class=\"tabletext\">". intranet_htmlspecialchars($data[$a]['Reason']) ."</td>";
		$x .= "<td class=\"tabletext\">". $data[$a]['PICID'] ."</td>";
		$x .= "<td class=\"tabletext\">". intranet_htmlspecialchars($data[$a]['Remark']) ."</td>";
		$x .= "</tr>";

	}
	if($import_success>0)
	{
		$xmsg2= "<font color=green>".($import_success)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
	}
	else
	{
		$xmsg="import_failed";	
	}
}
$x .= "</table>";

$linterface = new interface_html();

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php'");

$CurrentPage = "Management_Detention";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Detention']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>	
</table>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
