<?php
// Modifying by: 

##################################################################
#
# 	Date:	2017-01-09	Bill	[2016-0808-1743-20240]
#			Cust: Handling for RecordID from Calendar
#
#	Date:	2014-10-29	YatWoon	[ip.2.5.5.10.1]
#			Fixed: retrieve assigned detention record! 
#
##################################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$targetStudentID = implode(",",$RecordID);

//$sql = "SELECT RecordID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE StudentID IN ($targetStudentID) AND RecordStatus = 1";
//$result = $ldiscipline->returnArray($sql,1);
$sql = "SELECT RecordID FROM DISCIPLINE_DETENTION_STUDENT_SESSION WHERE StudentID IN ($targetStudentID) AND RecordStatus = 1 AND DetentionID is NULL";
$result = $ldiscipline->returnVector($sql);

if(sizeof($result)>0)
{
	/*
	for($i=0;$i<sizeof($result); $i++)
	{
		list($target_record_id) = $result[$i];
		$html .= "<input type='hidden' id='RecordID[]' value=$target_record_id>\n";
	}
	*/
	$targetRecordID = implode(",",$result);
}

// [2016-0808-1743-20240] Special Handling - RecordID received from calendar view is StudentSessionID 
if($sys_custom['eDiscipline']['BWWTC_CalendarWithReason'] && $isAssignFromCalendar)
{
	$targetRecordID = implode(",", $RecordID);
}

header("location: arrange_for_student.php?from=calendar&RecordID=$targetRecordID");
intranet_closedb();
?>