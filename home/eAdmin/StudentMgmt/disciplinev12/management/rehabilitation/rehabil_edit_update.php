<?php
# using: 

########## Change Log ###############
#
# 	Date	:	2015-07-29 (Bill)	[2015-0611-1642-26164]
# 				Create File
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

if(empty($RecordID))	header("Location: index.php");
if(!$sys_custom['eDiscipline']['CSCProbation']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$ldiscipline = new libdisciplinev12();

// Get Post Data
$id = IntegerSafe($RecordID);
$RehabilStatus = IntegerSafe($RehabilStatus);
$reh_response = nl2br($reh_response);

// Update merit record
if($id!="" && $RehabilStatus!="" && $reh_response!= ""){
	$sql = "UPDATE DISCIPLINE_MERIT_RECORD 
				SET RehabilFeedback='$reh_response', RehabilStatus='$RehabilStatus', RehabilLastModify='".$_SESSION['UserID']."', RehabilUpdateDate=now(), ModifiedBy='".$_SESSION['UserID']."', DateModified=now()
			WHERE RecordID='$id'";
	$success = $ldiscipline->db_db_query($sql);
}

# build back link
$back_par = "num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&s=$s&clickID=$clickID&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate";
if($back_page=="detail.php")
	$back_par .= "&SchoolYear2=$SchoolYear2&semester2=$semester2&targetClass2=$targetClass2&MeritType2=$MeritType2&pic=$pic";
else
	$back_par .= "&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&MeritType=$MeritType2&pic=$pic";
header("Location: ". $back_page ."?id=$RecordID&msg=update&$back_par");
		
intranet_closedb();

?>