<?
# using: 

#################################################################
#	Date:	2015-07-06 Shan
#			update UI standard
#
#	Date:	2012-10-19	YatWoon 
#			update ($task=='ItemCode'), missing to display homework not submitted items
#
#################################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

if($task=='ItemCode')
{
	$use_intranet_homework = $ldisciplinev12->accumulativeUseIntranetSubjectList();
	
	if($use_intranet_homework)	# use ASSESSMENT_SUBJECT
	{
		$sql = "SELECT 
					i.Name, i.ItemCode, c.Name as Cat 
				FROM 
					DISCIPLINE_ACCU_CATEGORY_ITEM as i 
					inner join DISCIPLINE_ACCU_CATEGORY c on i.CategoryID = c.CategoryID
				WHERE 
					i.RecordStatus = ". DISCIPLINE_CONDUCT_CATEGORY_ITEM_IN_USE ." and i.CategoryID!=2";
		$CatArr = $ldisciplinev12->returnArray($sql);
		
		# get subject list in ASSESSMENT_SUBJECT
        $CategoryName2 = $ldisciplinev12->RETRIEVE_ACCU_CATEGORY_NAME(2);
		$sql2 = "SELECT ".Get_Lang_Selection("CH_DES","EN_DES").",CODEID FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 ";
		$sql2 .= " and (CMP_CODEID is NULL or CMP_CODEID = '') ";
		$sql2 .=" ORDER BY DisplayOrder";
		$arr_result = $ldisciplinev12->returnArray($sql2);
		if(sizeof($arr_result))
		{
			foreach($arr_result as $k=>$d)
			{
				$CatArr[] = array('Name'=>$d[0],
								'ItemCode'=>$d[1],
								'Cat'=>$CategoryName2);
			}
		}
	}
	else
	{
		$sql = "SELECT i.Name, i.ItemCode, c.Name as Cat FROM DISCIPLINE_ACCU_CATEGORY_ITEM as i 
				inner join DISCIPLINE_ACCU_CATEGORY c on i.CategoryID = c.CategoryID
				WHERE i.RecordStatus = ". DISCIPLINE_CONDUCT_CATEGORY_ITEM_IN_USE;
		$CatArr = $ldisciplinev12->returnArray($sql);
	}
	
	$data = '<table width="98%" cellpadding="3" cellspacing="0" class="common_table_list_v30 view_table_list_v30">';
	 $data .='<tr class="tabletop">
									<th>#</th>
									<th>'.$i_Discipline_System_Discipline_Case_Record_Category.'</th>
									<th>'.$i_Discipline_System_ItemCode.'</th>
									<th>'.$i_Discipline_System_ItemName.'</th></tr>';
	 $data .= '<tbody>';
		for($a=0;$a<sizeof($CatArr);$a++)
		{
			$ItemCode = $CatArr[$a]['ItemCode'];
			$ItemName = $CatArr[$a]['Name'];
			$Category = $CatArr[$a]['Cat'];
			if($ItemName!='' && $ItemCode!='')
			{
				$data .='<tr class="tablerow1">
					 <td>'.($a+1).'</td>
					 <td>'.$Category.'</td>
					 <td>'.$ItemCode.'</td>
					 <td>'.$ItemName.'</td></tr>';
			}
		}
	$data .= '</tbody>';
	$data .="</table>";	
}
else if($task=='SubjCode')
{		
	# homework
	$use_intranet_homework = $ldisciplinev12->accumulativeUseIntranetSubjectList();

	# intranet homework list
	if($use_intranet_homework)
	{
		//$sql = "SELECT CONCAT('SUBJ',SubjectID) as Itemcode, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 ";
		$sql = "SELECT CONCAT('SUBJ',CodeID) as Itemcode, ".Get_Lang_Selection("CH_DES","EN_DES")." as SubjectName FROM ASSESSMENT_SUBJECT";
		$homework_list = $ldisciplinev12->returnArray($sql);
	}
	else
	{
		$sql = "SELECT ItemCode as Itemcode, Name as SubjectName FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID=2 AND RecordStatus <> ".DISCIPLINE_DELETED_RECORD;	
		$homework_list = $ldisciplinev12->returnArray($sql);
	}
		
	$data = '<table width="98%" cellpadding="3" cellspacing="0" class="common_table_list_v30 view_table_list_v30">';
		$data .='<tr class="tabletop">
									<th>#</th>
									<th>'.$i_Discipline_System_ItemCode.'</th>
									<th>'.$i_general_name.'</th>
								</tr>';
			$data .="<tbody>";						
								
		for($a=0;$a<sizeof($homework_list);$a++)
		{
			$ItemCode = $homework_list[$a]['Itemcode'];
			$ItemName = $homework_list[$a]['SubjectName'];
			if($ItemName!='' && $ItemCode!='')
			{
				$data .='<tr class="tablerow1">
					 <td>'.($a+1).'</td>
					 <td>'.$ItemCode.'</td>
					 <td>'.$ItemName.'</td>
					 </tr>';
			}
		}
		$data .="</tbody>";	
	$data .="</table>";	
	
} else if($task=='Receive') {
	
	$data = '<table width="98%" cellpadding="3" cellspacing="0" class="common_table_list_v30 view_table_list_v30">';
		$data .='<tr class="tabletop">
					<th valign="middle">'.$Lang['eDiscipline']['ReceiveTimes'].'</th>
				 </tr>';
		
		$data .='<tr class="tablerow1">
				 <td>0.5</td></tr>';
		$data .='<tr class="tablerow1">
				 <td>1</td></tr>';
		

	$data .="</table>";	
}

# OUTPUT
$output = '<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center" class="common_table_listv30 view_table_list_v30">
		
			
		 <tr> 
		 	<td align="right"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" name="pre_close1"  border="0" id="pre_close1" style="float: right;"></a></td>	 		
		 </tr>
		 <tr>
			  <td>
				<table align="left" width="550" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center">
				'.$data.'						
				</td> 
				 </tr>
				</table>
			</td>
			</tr>
						     		 
        </table>';

//$output = '<table width="100%" border="0" cellpadding="0" cellspacing="0">
//			  <tr>
//				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
//				  <tr>
//					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
//					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
//					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
//				  </tr>
//				</table></td>
//			  </tr>
//			  <tr>
//				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
//				  <tr>
//					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
//					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
//					  
//					  <tr>
//						<td height="150" align="left" valign="top">
//						'.$data.'
//						
//						</td></tr>
//							  <tr>
//						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
//						      <tr>
//							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
//						      </tr>
//						     </table></td>
//						     </tr>
//                 </table></td>
//					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
//				  </tr>
//				  <tr>
//					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
//					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
//					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
//				  </tr>
//					</table></td>
//			  </tr>
//			</table>';
echo $output;
?>
