<?php

/********************** Change Log ***********************/
#	
#	Date	:	2010-05-10	[YatWoon]
#				add "RejectedBy" and "RejectedDate" data 
#
/********************** Change Log [End] ***********************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval");

//$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&approved=$approved&rejected=$rejected&waived=$waived&RecordType=$RecordType&pic=$pic";

$RecordIDAry = array();

if(is_array($RecordID))
	$RecordIDAry = $RecordID;

$all_reject = 1;
foreach($RecordIDAry as $ConductRecordID)
{
	$this_reject = $ldiscipline->REJECT_CONDUCT_RECORD($ConductRecordID);
	$all_reject = $this_reject ? $all_reject : 0;
}


$msg = $all_reject ? "reject":"not_all_reject";
//header("Location: index.php?$return_filter&msg=$msg&fromPPC=$fromPPC");
?>
<form name="form1" id="form1" action="index.php" method="POST">

<input type="hidden" name="SchoolYear" value="<?=$SchoolYear?>">
<input type="hidden" name="semester" value="<?=$semester?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="num_per_page" value="<?=$num_per_page?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>">
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>">
<input type="hidden" name="MeritType" value="<?=$MeritType?>">
<input type="hidden" name="s" value="<?=$s?>">
<input type="hidden" name="clickID" value="<?=$clickID?>">
<input type="hidden" name="approved" value="<?=$approved?>">
<input type="hidden" name="rejected" value="<?=$rejected?>">
<input type="hidden" name="waived" value="<?=$waived?>">
<input type="hidden" name="RecordType" value="<?=$RecordType?>">
<input type="hidden" name="pic" value="<?=$pic?>">
<input type="hidden" name="msg" value="<?=$msg?>">
<input type="hidden" name="fromOverview" value="0">
</form>
<br>
<script language="javascript">
<!--
document.getElementById('form1').submit();
-->
</script>
