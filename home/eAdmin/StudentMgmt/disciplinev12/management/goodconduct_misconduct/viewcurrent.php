<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New", "You have no priviledge to access this page.");

$lu = new libuser();
$linterface = new interface_html("popup.html");

$MODULE_OBJ['title'] = $eDiscipline["AwardPunishmentRecord"];
          
$StudentID = ($_GET['StudentID'] != "") ? $_GET['StudentID'] : $StudentID;

$current_year = getCurrentAcademicYear();
$sql = "SELECT DISTINCT Year FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$StudentID' ORDER BY Year DESC";
$array_years = $ldiscipline->returnVector($sql);

if (sizeof($array_years)==0 || !in_array($current_year, $array_years))
{
    $array_years = array_merge(array($current_year),$array_years);
}
if ($year == "")
{
    $year = getCurrentAcademicYear();
}
if ($semester == "")
{
	$semester = getCurrentSemester();
}

$student = $lu->getNameWithClassNumber($StudentID);
$merit_record = $ldiscipline->retrieveStudentMeritRecordByYearSemester($StudentID, $year, $semester, 1);
$demerit_record = $ldiscipline->retrieveStudentMeritRecordByYearSemester($StudentID, $year, $semester, -1);

$string_type["-1"] = $i_Merit_BlackMark;
$string_type["-2"] = $i_Merit_MinorDemerit;
$string_type["-3"] = $i_Merit_MajorDemerit;
$string_type["-4"] = $i_Merit_SuperDemerit;
$string_type["-5"] = $i_Merit_UltraDemerit;
$string_type["1"] = $i_Merit_Merit;
$string_type["2"] = $i_Merit_MinorCredit;
$string_type["3"] = $i_Merit_MajorCredit;
$string_type["4"] = $i_Merit_SuperCredit;
$string_type["5"] = $i_Merit_UltraCredit;

$col = ($ldiscipline->UseSubScore) ? 7 : 6;
$merit_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
$merit_table .= "<tr><td colspan=\"$col\">". $linterface->GET_NAVIGATION2($eDiscipline["AwardRecord"]) ."</td></tr>";
$merit_table .= "<tr>
				<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>
				<td class=\"tablebluetop tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['EventDate']."</td>
				<td class=\"tablebluetop tabletopnolink\" width=\"200\">".$eDiscipline["Award_Punishment_RecordItem"]."</td>
                <td class=\"tablebluetop tabletopnolink\">$i_general_receive</td>
                <td class=\"tablebluetop tabletopnolink\" width=\"50\">$i_Discipline_System_ConductScore</td>";
$merit_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Subscore1</td>" : "";
$merit_table .= "<td class=\"tablebluetop tabletopnolink\" width=\"150\">$i_Profile_PersonInCharge</td></tr>";

for ($i=0; $i<sizeof($merit_record); $i++)
{
	list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_subscore1, $r_picid, $r_pic_name) = $merit_record[$i];
	$r_item = intranet_htmlspecialchars($r_item);
	
	$merit_table .= "<tr class=\"tablebluerow".($i%2+1)."\">
					<td class=\"tabletext\">".($i+1)."</td>
					 <td class=\"tabletext\">$r_date</td>
					 <td class=\"tabletext\">$r_item</td>
                     <td class=\"tabletext\">$r_profile_count ".$string_type[$r_profile_type]."</td>
                     <td class=\"tabletext\">$r_conduct</td>";
	$merit_table .= ($ldiscipline->use_sub_score1) ? "<td class=\"tabletext\">$r_subscore1</td>" : "";
    $merit_table .= "<td class=\"tabletext\">$r_pic_name</td></tr>";
}
if(sizeof($merit_record) == 0)
	$merit_table .= "<td colspan=$col align=center class=\"tablebluerow2 tabletext\">".$i_no_record_exists_msg."</td>";
$merit_table .= "</table>\n";

$demerit_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"tabletext\">\n";
$demerit_table .= "<tr>
							<td colspan=\"$col\">". $linterface->GET_NAVIGATION2($eDiscipline["PunishmentRecord"]) ."</td>
					</tr>
							<tr>
							<td class=\"tablebluetop tabletopnolink\" width=10>#</td>
							<td class=\"tablebluetop tabletopnolink\" width=80>".$Lang['eDiscipline']['EventDate']."</td>
							<td class=\"tablebluetop tabletopnolink\" width=200>".$eDiscipline["Award_Punishment_RecordItem"]."</td>
                            <td class=\"tablebluetop tabletopnolink\">$i_general_receive</td>
                            <td class=\"tablebluetop tabletopnolink\" width=\"50\">$i_Discipline_System_ConductScore</td>";
$demerit_table .= ($ldiscipline->use_sub_score1) ? "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Subscore1</td>" : "";
$demerit_table .= "<td class=\"tablebluetop tabletopnolink\" width=\"150\">$i_Profile_PersonInCharge</td></tr>";

for ($i=0; $i<sizeof($demerit_record); $i++)
{
	list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
			$r_conduct, $r_subscore1, $r_picid, $r_pic_name) = $demerit_record[$i];
	$r_item = intranet_htmlspecialchars($r_item);
	
	$demerit_table .= "<tr class=\"tablebluerow".($i%2+1)."\"><td class=\"tabletext\">".($i+1)."</td>
								<td class=\"tabletext\">$r_date</td>
                                <td class=\"tabletext\">$r_item</td>
                                <td class=\"tabletext\">$r_profile_count ".$string_type[$r_profile_type]."</td>
                                <td class=\"tabletext\">$r_conduct</td>";
	$demerit_table .= ($ldiscipline->use_sub_score1) ? "<td class=\"tabletext\">$r_subscore1</td>" : "";
    $demerit_table .= "<td class=\"tabletext\">".($r_pic_name==""?"--":$r_pic_name)."</td></tr>";
}
if(sizeof($demerit_record) == 0)
	$demerit_table .= "<td colspan=\"$col\" align=\"center\" class=\"tablebluerow2 tabletext\">".$i_no_record_exists_msg."</td>";
$demerit_table .= "</table>\n";



$linterface->LAYOUT_START();

?>
<form name="form1" action="viewcurrent.php" method="POST">

<!--  START Student Info table //-->
<br />
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
<tr valign="top">
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_UserStudentName?></span></td>
	<td class="tabletext"><?=$student?></td>
</tr>
<tr valign="top">
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_show?></span></td>
	<td class="tabletext">
		<?=getSelectByValue($array_years,"name='year' onchange='document.form1.submit();'",$year, 0, 1)?>
		<?=getSelectSemester("name='semester' onchange='document.form1.submit();'",$semester)?>
	</td>
</tr>
</table>

<!-- END Student Info table //-->

<br />

<?=$merit_table?>

<br/>

<?=$demerit_table?>

<input type='hidden' name="StudentID" value="<?=$StudentID?>" />
<input type='hidden' name="submitFlag" value="1" />
</form>

<table width="95%" border="0" cellpadding="5" cellspacing="0">    
              <tr>
        	<td colspan="3">        
                        <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
                        <tr>
                        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                        </tr>
                        <tr>
        			<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","close_btn") ?>
        			</td>
        		</tr>
                        </table>                                
        	</td>
        </tr>
        </table>
        
<br />
<?php
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
$linterface->LAYOUT_STOP();
?>