<?php
# using: 

/********************** Change Log ***********************/
#
#	Date    :	2019-05-13 (Bill)
#               Prevent SQL Injection
#
#	Date	:	2016-09-29 (Bill)	[2016-0808-1526-52240]
#				- skip conversion period checking	($sys_custom['eDiscipline']['SkipGMPeriodChecking'])
#	
#	Date	:	2010-05-10	[YatWoon]
#				add "ApprovedBy" and "ApprovedDate" data 
#
/********************** Change Log [End] ***********************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval");

//$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&approved=$approved&rejected=$rejected&waived=$waived&RecordType=$RecordType&pic=$pic";

$RecordIDAry = array();
if(is_array($RecordID)) {
	$RecordIDAry = $RecordID;
}
$RecordIDAry = IntegerSafe($RecordIDAry);

$all_approve = 1;
$InDateRange = 0;

foreach($RecordIDAry as $ConductRecordID)
{
	### To-do: Add checking of overlapping periods
	$record_info = $ldiscipline->RETRIEVE_CONDUCT_INFO($ConductRecordID);
	$thisRecordType = $record_info[0]['RecordType'];
	$thisCatID = $record_info[0]['CategoryID'];
	//$StudentID = $record_info[0]['StudentID'];
	$thisRecordDate = substr($record_info[0]['RecordDate'], 0, 10);
	$period_info = $ldiscipline->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($thisRecordDate,$thisRecordType,$thisCatID);
	list($periodType, $period_setting) = $period_info;
	
	# static period
	if($periodType=="static")
	{
		$sql = "SELECT CategoryID, RecordDate FROM DISCIPLINE_ACCU_RECORD WHERE RecordID = '$ConductRecordID'";
		$arr_result = $ldiscipline->returnArray($sql,2);
		if(sizeof($arr_result) > 0){
			list($CategoryID, $RecordDate) = $arr_result[0];
		}
			
		$sql = "SELECT PeriodID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING WHERE CategoryID = '$CategoryID'";
		$arr_PeriodID = $ldiscipline->returnVector($sql);
		if(sizeof($arr_PeriodID) > 0){
			$TargetPeriod = implode(",", $arr_PeriodID);
		}
		
 		$sql = "SELECT SetID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD WHERE CategoryID = '$CategoryID' and RecordStatus=1";
 		$tmp_SetID = $ldiscipline->returnVector($sql);
		$SetID = $tmp_SetID[0] ? $tmp_SetID[0] : 0;	
		
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID IN ($TargetPeriod) AND SetID = '$SetID' AND (DateStart <= '$RecordDate' AND DateEnd >= '$RecordDate')";
		$temp = $ldiscipline->returnVector($sql);
		$InDateRange = sizeof($temp)>0 && $temp[0] > 0 ? 1:0;
	}
	else
	{
		for($i=0;$i<sizeof($period_setting);$i++)
		{
			if($period_setting[$i]['DateStart'] <= $thisRecordDate and $period_setting[$i]['DateEnd'] >= $thisRecordDate)
			{
				$InDateRange = 1;
				break;
			}
		}
	}
	
	// [2016-0808-1526-52240] skip conversion period checking (added $sys_custom['eDiscipline']['SkipGMPeriodChecking'])
	if($InDateRange || $sys_custom['eDiscipline']['SkipGMPeriodChecking']){
		$this_approve = $ldiscipline->APPROVE_CONDUCT_RECORD($ConductRecordID);
		$all_approve = $this_approve ? $all_approve : 0;
	}
	else{
		$all_approve = 0;
	}
}

$msg = $all_approve ? "approve":"not_all_approve";
//header("Location: index.php?$return_filter&msg=$msg");
?>

<form name="form1" id="form1" action="index.php" method="POST">

<input type="hidden" name="SchoolYear" value="<?=$SchoolYear?>">
<input type="hidden" name="semester" value="<?=$semester?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="num_per_page" value="<?=$num_per_page?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>">
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>">
<input type="hidden" name="MeritType" value="<?=$MeritType?>">
<input type="hidden" name="s" value="<?=$s?>">
<input type="hidden" name="clickID" value="<?=$clickID?>">
<input type="hidden" name="approved" value="<?=$approved?>">
<input type="hidden" name="rejected" value="<?=$rejected?>">
<input type="hidden" name="waived" value="<?=$waived?>">
<input type="hidden" name="RecordType" value="<?=$RecordType?>">
<input type="hidden" name="pic" value="<?=$pic?>">
<input type="hidden" name="msg" value="<?=$msg?>">
<input type="hidden" name="fromOverview" value="0">
</form>
<br>

<script language="javascript">
<!--
document.getElementById('form1').submit();
-->
</script>