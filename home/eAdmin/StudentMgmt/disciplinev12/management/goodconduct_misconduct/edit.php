<?
# Using: YW

/********************** Change Log ***********************/
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose)
#
#   Date    :   2019-05-24  Bill
#               apply download_attachment.php
#
#   Date    :   2019-05-01  Bill
#               prevent SQL Injection + Cross-site Scripting
#
#	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#				Handle "Details refer to HOY" logic when edit record ($sys_custom['eDiscipline']['HOY_Access_GM'])
#				- Not allow edit Remarks when	1) Details refer to HOY : true		2) Not HOY Members
#
#	Date	:	2017-02-20 (Bill)
#				set max conduct score deduct selection	(depend on $sys_custom['eDiscipline']['HYK_MaxGMConductMark'])
#
#	Date	:	2016-09-29	(Bill)	[2016-0808-1526-52240]
#				added GMConductScoreChange drop down list for misconduct ($sys_custom['eDiscipline']['GMConductMark'])
#
#	Date	:	2016-06-13 (Anna)	[2016-0526-1139-05206]
#				added creator's information and change CSS of table
#
#	Date	:	2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added prefix style to teacher name of deleted account
#
#	Date	:	2016-03-02 (Bill)	[2016-0224-1423-31073]
#				return PIC name even teacher account is inactive or removed
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				add button to select template for push message
#				display "Send Push Message" in Action
#
#	Date	:	2015-02-13 (Bill) [2015-0123-1536-08071]
#				fixed: display main subject only in drop down list
#
#	Date	:	2014-06-13 (YatWoon)	 - ip.2.5.5.8.1
#				fixed: missing to check eNotice is disabled, cause create discipline error [Case#K63069]
#
#	Date	:	2014-05-28	Carlos
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added ScoreChange selection for misconduct type record
#
#	Date	:	2010-08-23	YatWoon
#				- add ling leung yi man customization: follow-up action ($sys_custom['eDisciplinev12_FollowUp'])
#
#	Date	:	2010-08-18	YatWoon
#				display student internal remark for client reference
#
#	Date	:	2010-06-25 (Henry)
#	Detail 	:	retrieve Year Name according to the AcademicYearID
#
#	Date	:	2010-06-10 (Henry)
#				add attachment to record
#
#	Date	:	2010-04-14 YatWoon
#				check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
#	Date	:	2010-03-17 (Henry)
#	Details :	add Add/Delete Notice button
#
#	Date	:	2010-01-25 (YatWoon)
#	Details :	Display semester name with YearTermID, don't diplasy the field "Semester" which maybe not sync with YearTermID
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$RecordID = IntegerSafe($RecordID);

$SchoolYear = IntegerSafe($SchoolYear);
if($semester != '' && $semester != 'WholeYear') {
    $semester = IntegerSafe($semester);
}
$targetClass = cleanCrossSiteScriptingCode($targetClass);

// $s = cleanCrossSiteScriptingCode($s);
// $clickID = IntegerSafe($clickID);

$RecordType = IntegerSafe($RecordType);
$approved = IntegerSafe($approved);
$rejected = IntegerSafe($rejected);
$waived = IntegerSafe($waived);
$fromPPC = IntegerSafe($fromPPC);
$newleaf = IntegerSafe($newleaf);
$pic = IntegerSafe($pic);
### Handle SQL Injection + XSS [END]

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){
	include_once($PATH_WRT_ROOT."includes/libuser.php");
	$luser = new libuser();
	$canSendPushMsg = true;
}

if(sizeof($RecordID)>0) {
	$RecordID = (is_array($RecordID)) ? implode(",",$RecordID) : $RecordID;
}
else {
	$RecordID = $RecordID;
}

$parameter = "?SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&RecordType=$RecordType&s=$s&approved=$approved&rejected=$rejected&waived=$waived&pic=$pic";
$back_path = "index.php".$parameter;

# Check access right
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || (($ldiscipline->isConductPIC($RecordID) || $ldiscipline->isConductOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"))))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT("","index.php?approved=$approved&rejected=$rejected&waived=$waived&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&s=$s&pageNo=$pageNo&order=$order&field=$field&page_size=$numPerPage&RecordType=$RecordType&s=$s");
}

# Menu Highlight Setting
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# Step Information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 1);

# Navigation Bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], $back_path);
$PAGE_NAVIGATION[] = array($eDiscipline["EditRecord"], "");

$student_namefield = getNamefieldByLang("b.");
$sql = "SELECT 
				CONCAT(b.ClassName,' - ',b.ClassNumber) as ClassNameNum, 
				$student_namefield,
				a.RecordType,
				c.CategoryID,
				c.Name,
				a.ItemID,
				LEFT(a.RecordDate,10) as RecordDate,
				a.Remark,
				a.Year,
				a.Semester,
				a.NoticeID,
				a.WaivedByNewLeaf,
				a.RecordStatus,
				a.PICID,
				a.YearTermID,
				Attachment,
				StudentID, 
				a.AcademicYearID,
				b.Remark,
				a.ActionID,
				a.GMCount, 
				a.CreatedBy,
				a.DateInput ";
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$sql .= " ,a.ScoreChange ";
}
// [2014-1216-1347-28164]
if($canSendPushMsg){
	$sql .= " ,a.PushMessageID ";
}
// [2016-0808-1526-52240]
if($sys_custom['eDiscipline']['GMConductMark']){
	$sql .= " ,a.GMConductScoreChange ";
}
// [2017-0403-1552-54240]
if($sys_custom['eDiscipline']['HOY_Access_GM']){
	$sql .= " ,a.ApplyHOY ";
}
$sql.= "FROM
				DISCIPLINE_ACCU_RECORD as a
				LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON a.CategoryID = c.CategoryID
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
		WHERE 
				a.RecordID = '$RecordID'";
$arr_result = $ldiscipline->returnArray($sql);

if(sizeof($arr_result) > 0)
{
	list($className,$studentName,$recordType,$meritCategoryID,$meritCategoryName,$meritItemID,$recordDate,$remark, $school_year, $semester_name, $noticeID, $isNewLeaf, $status, $pic, $YearTermID, $Attachment, $StudentID, $AcademicYearID, $InternalRemark, $ActionID, $GMCount) = $arr_result[0];
	//$remark = intranet_undo_htmlspecialchars($remark);
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		$score_change = $arr_result[0]['ScoreChange'];
	}
	// [2014-1216-1347-28164]
	if($canSendPushMsg){
		$push_msg_id = $arr_result[0]['PushMessageID'];
	}
	// [2016-0808-1526-52240]
	if($sys_custom['eDiscipline']['GMConductMark']){
		$gm_conduct_score_change = (int)(abs($arr_result[0]['GMConductScoreChange']));
	}
	// [2017-0403-1552-54240]
	if($sys_custom['eDiscipline']['HOY_Access_GM']) {
		$isApplyHOY = $arr_result[0]['ApplyHOY'];
	}
	
	// [2016-0526-1139-05206] Get Creator Info
	$CreatedBy = $arr_result[0]['CreatedBy'];
	if($CreatedBy > 0){
		$userObj = new libuser($CreatedBy);
		$CreatorEnglishName = $userObj->EnglishName;
		$CreatorChineseName = $userObj->ChineseName;
		$nameDisplay = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
	}
	$DateInput = $arr_result[0]['DateInput'];
}

$school_year = $ldiscipline->getAcademicYearNameByYearID($AcademicYearID);

if($Attachment=='') {
	$sessionTime = session_id()."-".(time())."_".$StudentID."tmp";
}
else {
	//$sessionTime = substr($Attachment,0,-3);	
	$sessionTime = $Attachment;
}
//$sessionTime .= "tmp";
//echo $sessionTime;

# Times of GM Count
$GMCountSelection = $ldiscipline->Get_GM_Count_Selection($GMCount);

$path = "$file_path/file/disciplinev12/goodconduct_misconduct/".$sessionTime;
$attachment_list = $ldiscipline->getAttachmentArray($path, $Attachment);
$attSelect = $linterface->GET_SELECTION_BOX($attachment_list,"id=\"Attachment[]\" name=\"Attachment[]\"  multiple='multiple'","");

if(sizeof($attachment_list) > 0)
{
	$attachmentHTML = "";
	for($i=0; $i<sizeof($attachment_list); $i++) {
		list($displayFilename) = $attachment_list[$i];
		// $linkFilename = str_replace(" ","%20", $displayFilename);
		// $displayFilename = "<a href=/file/disciplinev12/goodconduct_misconduct/".$sessionTime."/".$linkFilename." target=\"_blank\">$displayFilename</a>";
		
		$url = $path."/".$displayFilename;
		$displayFilename = "<a href='/home/download_attachment.php?target_e=".getEncryptedText($url)."' target=\"_blank\">$displayFilename</a>";
		$attachmentHTML .= ($attachmentHTML=="") ? $displayFilename : ", ".$displayFilename;
	}
	$attachmentHTML = "[".$attachmentHTML."]";
}

$attachmentHTML .= "<table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$attSelect."
				</td>
				<td valign=\"bottom\">".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."&fieldname=Attachment[]', 9)")."
				<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment()")."
				</td>
				</tr>
				</table>";

$fcm = new academic_year_term($YearTermID);
$semesterName = $fcm->Get_YearNameByID($YearTermID, $intranet_session_language);

if ($pic != '')
{
	$namefield = getNameFieldWithLoginByLang();
	
	// [2016-0224-1423-31073]
	// INTRANET_USER
	//$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($pic) AND RecordStatus = 1 ORDER BY $namefield";
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($pic) ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
	
	// INTRANET_ARCHIVE_USER
	//$sql = "SELECT UserID, $namefield FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($pic) ORDER BY $namefield";
	$sql = "SELECT UserID, CONCAT('<span class=\"tabletextrequire\">*</span>', $namefield) FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($pic) ORDER BY $namefield";
	$array_ArchivePIC = $ldiscipline->returnArray($sql);
	
	// Merge user and archive user
	$array_PIC = array_merge($array_PIC, $array_ArchivePIC);
}
$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");

# Check user can access/edit this item or not
$AccessItem = 0;
if($ldiscipline->IS_ADMIN_USER($UserID) || $ldiscipline->IS_HOY_GROUP_MEMBER())
{
	$AccessItem = 1;
}
else
{
	$ava_item = $ldiscipline->Retrieve_User_Can_Access_GM_Category_Item($UserID, 'item');
	if($ava_item[$meritCategoryID]) {
		$AccessItem = in_array($meritItemID, $ava_item[$meritCategoryID]);
	}
}

# Get Merit Item List 
# Check Category is published or drafted
$sql = "select RecordStatus from DISCIPLINE_ACCU_CATEGORY where CategoryID = '$meritCategoryID'";
$recordstatus = $ldiscipline->returnVector($sql);
if($recordstatus[0]==DISCIPLINE_CONDUCT_CATEGORY_PUBLISHED && $AccessItem)
{
	$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();
	if($use_intranet_homework && $meritCategoryID==PRESET_CATEGORY_HOMEWORK)
	{
		include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");
		$libSubject = new subject();
		
		// only display Main Subject - Added (CMP_CODEID='' OR CMP_CODEID IS NULL) [2015-0123-1536-08071]
		$sql = "SELECT RecordID, ".Get_Lang_Selection("CH_DES","EN_DES")." FROM ASSESSMENT_SUBJECT WHERE RecordStatus=1 AND (CMP_CODEID='' OR CMP_CODEID IS NULL) ORDER BY DisplayOrder";
		$arr_result = $libSubject->returnArray($sql, 2);
		
		$sql = "SELECT ItemID, Name FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID = '$meritCategoryID' and CategoryID <> 2 and RecordStatus = ". DISCIPLINE_CONDUCT_CATEGORY_ITEM_IN_USE;
		$arr_items = $ldiscipline->returnArray($sql,2);
		
		if($arr_result){
			for ($i=0; $i<sizeof($arr_result); $i++) {
				$arr_result_name = intranet_undo_htmlspecialchars($arr_result[$i][1]);
				$arr_items = array_merge($arr_items, array(array($arr_result[$i][0], $arr_result_name)));
			}
		}
		
		$meritItem_selection = getSelectByArray($arr_items," name=\"meritItemID\" onChange=\"changeFollowUp(0, this.value)\" ",$meritItemID,0,0,$iDiscipline['SelectBehaviourItem']);
	}
	else {
		$sql = "SELECT ItemID, Name FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID = '$meritCategoryID' and RecordStatus = ". DISCIPLINE_CONDUCT_CATEGORY_ITEM_IN_USE;
		$arr_items = $ldiscipline->returnArray($sql,2);	
		$meritItem_selection = getSelectByArray($arr_items," name=\"meritItemID\" onChange=\"changeFollowUp(0, this.value)\" ",$meritItemID,0,0,$iDiscipline['SelectBehaviourItem']);
	}
	
	if($meritCategoryID==PRESET_CATEGORY_LATE)
	{
		# no need to display item 
		$merit_display = $meritCategoryName;
		$merit_display .= "<input type='hidden' name='meritItemID' value='0'>";
	}
	else
	{
		# need to select item
		$merit_display = $meritCategoryName ." - ".  $meritItem_selection;	
	}
}
else
{
	$merit_display = $meritCategoryID==1 ? $meritCategoryName : $ldiscipline->RETURN_CONDUCT_REASON($meritItemID);
	$merit_display .= "<input type='hidden' name='meritItemID' value='". $meritItemID ."'>";
}

if($duplicate==1) {
	$merit_display = "<table width='100%' cellpadding='0' cellspacing='0' valign='0'><tr><td>$merit_display</td><td>".$linterface->GET_SYS_MSG("",$Lang['eDiscipline']['ItemAlreadyAdded'])."</td></tr></table>";
}

# Detention
$TempAllForms = $ldiscipline->getAllFormString();
$DetentionList = $ldiscipline->getDetentionListByGMID($RecordID);
//$DetentionPICFlag = false;
$assignedDetentionCount = 0;

for ($i=0; $i<sizeof($DetentionList); $i++) {
	list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $DetentionList[$i];
	if ($TmpForm == $TempAllForms) $TmpForm = $i_Discipline_All_Forms;
	$TmpAttendanceStatus = $ldiscipline->getAttendanceStatusByStudentIDDetentionID($data['StudentID'], $TmpDetentionID);
	if ($TmpAttendanceStatus[0] == "PRE") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_present.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Attended\">";
	}
	else if ($TmpAttendanceStatus[0] == "ABS") {
		$TmpAttendanceStatus = "<img src=\"$image_path/$LAYOUT_SKIN/smartcard/icon_absent.gif\" width=\"20\" height=\"20\" title=\"$i_Discipline_Absent\">";
	}
	else {
		$TmpAttendanceStatus = $i_Discipline_Not_Yet;
	}
	$DetentionOutputRow .= "<tr class=\"row_approved\">\n";
	$DetentionOutputRow .= "<td valign=\"top\">".($i+1)."</td>\n";
	
	if ($TmpDetentionID == "")
	{
		$DetentionOutputRow .= "<td valign=\"top\">".$i_Discipline_System_Not_Yet_Assigned."</td>\n";
	}
	else
	{
		$DetentionOutputRow .= "<td valign=\"top\">$TmpDetentionDate | $TmpStartTime - $TmpEndTime | $TmpLocation | $TmpPIC</td>\n";
	}
	
	$DetentionOutputRow .= "<td valign=\"top\"><span class=\"tabletextremark\">$TmpAttendanceStatus</span></td>\n";
	$DetentionOutputRow .= "</tr>\n";
	
	/*if($ldiscipline->isDetentionSessionPIC($TmpDetentionID))
		$DetentionPICFlag = true;
	*/	
	$assignedDetentionCount += ($TmpDetentionID!='') ? 1 : 0;
}

//if($assignedDetentionCount==0) $DetentionPICFlag = true;
if ($DetentionOutputRow != "") {
	$DetentionOutputPre .= "<tr><td>".$eDiscipline['Detention']."</td></tr>\n";
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
		$DetentionOutputPre .= "<tr><td style=\"border:1px solid #CCCCCC\">\n";
		$DetentionOutputPre .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
		$DetentionOutputPre .= "<tr class=\"tabletop\">\n";
		$DetentionOutputPre .= "<td width=\"15\" align=\"center\">#</td>\n";
		$DetentionOutputPre .= "<td>$i_Discipline_Arranged_Session</td>\n";
		$DetentionOutputPre .= "<td>$i_Discipline_Attendance</td>\n";
		$DetentionOutputPre .= "</tr>\n";
		$DetentionOutputPost = "</table></td></tr>\n";
		$DetentionOutputData = $DetentionOutputPre.$DetentionOutputRow.$DetentionOutputPost;
	}
	else {
		$DetentionOutputData = $DetentionOutputPre.$DetentionOutputPost;
	}
}
if ($DetentionOutputData) {
	$DetentionButtonLabel = $eDiscipline["EditDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Edit';document.form1.submit();";
}
else {
	$DetentionButtonLabel = $eDiscipline["AddDetention"];
	$DetentionButtonAction = "document.form1.action='edit_detention.php';document.form1.DetentionAction.value='Add';document.form1.submit();";
}

/*
# eNotice
$lc = new libucc($noticeID);
$result = $lc->getNoticeDetails();
if ($noticeID) {
	$eNoticePath = "<a href=\"#\" class=\"tabletext\" onClick=\"newWindow('".$result['url']."', 1)\">".$eDiscipline["PreviewNotice"]."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
	$actionContent = $eNoticePath;
}
else if ($DetentionOutputData == ""){
	$actionContent = $i_Discipline_No_Action;
}
*/

# eClass App [2014-1216-1347-28164] 
$msgAvailable = false;	
$msgButtonLabel = "";
if($canSendPushMsg && $recordType==-1 && $luser->getParentUsingParentApp($StudentID, true)){
	$messageButton = $linterface->GET_BTN($Lang['eDiscipline']['DetentionMgmt']['SendPushMessage'], "button", "document.form1.action='notice_new.php?isMsg=1'; document.form1.submit();");
	$msgAvailable = true;
}
if($msgAvailable && $push_msg_id){
	$msgButtonLabel = "<tr><td>".$Lang['eDiscipline']['DetentionMgmt']['SendPushMessage']."</td></tr>";
}

# eNotice
$lc = new libucc($noticeID);
$result = $lc->getNoticeDetails();
$is_disable = $lnotice->disabled ? "1" : "";
if ($noticeID) {
	$eNoticePath = "<a href=\"#\" class=\"tabletext\" onClick=\"newWindow('".$result['url']."', 1)\">".$eDiscipline["PreviewNotice"]."<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"></a>";
	$actionContent = $eNoticePath;
	$noticeButton = $linterface->GET_BTN($Lang['eDiscipline']['DeleteNotice'], "button", "deleteNotice()","","",$is_disable);
}
// [2014-1216-1347-28164] add checking if parent can receive eClass App push message 
else if (($msgAvailable && $DetentionOutputData == "" && $msgButtonLabel == "") || (!$msgAvailable && $DetentionOutputData == "")){
	$actionContent = $i_Discipline_No_Action;
	$noticeButton = $linterface->GET_BTN($Lang['eDiscipline']['AddNotice'], "button", "document.form1.action='notice_new.php';document.form1.submit();","","",$is_disable);
} 
else {
	$noticeButton = $linterface->GET_BTN($Lang['eDiscipline']['AddNotice'], "button", "document.form1.action='notice_new.php';document.form1.submit();","","",$is_disable);
}

$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
var ClickID = '';
var callback_ajaxRemoveAttachment = {
	success: function ( o )
    {
		//var fname = document.getElementById('fieldname').value;
	    var tmp_str = o.responseText;
	    checkOptionRemove(document.form1.elements['Attachment[]']);
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function ajaxRemoveAttachment()
{
	ClickID = "ref_list";
	obj = document.form1;
	//obj.fieldname.value = fname;
	obj.task.value = "remove";
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_attachment.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_ajaxRemoveAttachment);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	
	return pos_value;
}

function DisplayPosition()
{
	var posleft = getPosition(document.getElementById(ClickID),'offsetLeft') +10;
	var postop = getPosition(document.getElementById(ClickID),'offsetTop') +10;
	var divwidth = document.getElementById('ref_list').clientWidth;
	
	if(posleft+divwidth > document.body.clientWidth)
		document.getElementById('ref_list').style.right= "5px";
	else
		document.getElementById('ref_list').style.left= posleft+"px";
		
  	document.getElementById('ref_list').style.top= postop+"px";
  	document.getElementById('ref_list').style.visibility='visible';
}

function checkForm()
{
	var obj = document.form1;
	
	<? if($recordstatus[0]==DISCIPLINE_CONDUCT_CATEGORY_PUBLISHED && $meritCategoryID != PRESET_CATEGORY_LATE) { ?>
	var pic_length = obj.elements['PIC[]'].length;
	var itemSelected = false;
	
	if(pic_length==0) {
		alert("<?=$i_alert_pleaseselect.' '.$i_Discipline_System_Contact_PIC?>");	
		return false;
	}
	
	for(i=0; i<obj.meritItemID.length; i++){
		if(obj.meritItemID.options[i].selected == true && i!=0){
			itemSelected = true;
		}
	}
	if(itemSelected==false) {
		alert('<?=$i_Discipline_Ssytem_Category_Item_Not_Selected?>');
		return false;
	}
	<? } ?>
	
	<? if($sys_custom['eDiscipline_GM_Times']) {?>
	/*
	if(obj.GMCount.selectedIndex==0) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['eDiscipline']['ReceiveTimes']?>");
		return false;
	}
	*/
	<? } ?>
	
	<? if($sys_custom['eDisciplinev12_FollowUp']) {?>
	if(obj.ActionID.value != '<?=$ActionID?>' && obj.ActionID.value !="")
	{
		if(confirm("<?=$Lang['eDiscipline']['ChangeFollowUpActionWarning']?>"))
		{
			obj.resetFollowup.value=1;
			checkOptionAll(document.getElementById('PIC[]'));
			checkOptionAll(document.getElementById('Attachment[]'));
			obj.submit();
		}
		else
		{
			return false;
		}
	}
	else
	{
		checkOptionAll(document.getElementById('PIC[]'));
		checkOptionAll(document.getElementById('Attachment[]'));
		obj.submit();
	}
	<? }
	else { ?>
		checkOptionAll(document.getElementById('PIC[]'));
		checkOptionAll(document.getElementById('Attachment[]'));
		obj.submit();
	<? } ?>
}

function goUnWaive(id) {
	self.location = 'unwaive.php<?=$parameter?>&RecordID=<?=$RecordID?>';
}

function deleteNotice() {
	if(confirm("<?=$Lang['eDiscipline']['DeleteNotice']?>?")) {
		document.form1.action = 'notice_remove.php';
		document.form1.submit();	
	}
}

function changeFollowUp(student_id, itemid)
{
	<? if($sys_custom['eDisciplinev12_FollowUp']) {?>
	obj = document.form1;
	if(student_id>0)
		eval('var cat_select = obj.elements["CatID_'+ student_id +'"];');
	else
		var cat_select = obj.elements["CatID"];
	
	var category_id = cat_select.value; 
	
	$.post(
		'ajax_get_followup.php', 
		{
			CategoryID: category_id,
			ItemID: itemid
		}, 
		function (action_id){
			if(action_id)
			{
				if(student_id>0)
				{
					eval('obj.elements["ActionID_'+ student_id +'"].value=action_id;');
				}
				else
				{
					obj.elements["ActionID"].value=action_id;
				}
			}
			else
			{
				if(student_id>0)
				{
					eval('obj.elements["ActionID_'+ student_id +'"].selectedIndex=0;');
				}
				else
				{
					obj.elements["ActionID"].selectedIndex=0;
				}
			}
		}); 
	<? } ?>
}
-->
</script>

<div id="ref_list"></div>

<form name="form1" method="POST" action="edit_update.php">
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
				<td align='right'><?=$linterface->GET_SYS_MSG($msg)?></td>
			</tr>
		</table>
	</td>
</tr>
</table>

<table class="form_table_v30">
<tr>
	<td class="field_title"><?=$i_general_name;?></td>
	<td class="tabletext" valign="top"><?=$studentName;?></td>
</tr>
<tr>
	<td class="field_title"><?=$i_general_class;?></td>
	<td class="tabletext" valign="top"><?=$className;?></td>
</tr>
<tr>
	<td class="field_title"><?=$eDiscipline["Type"];?></td>
	<td class="tabletext" valign="top">
		<? 
			if($recordType==1){
				echo "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> $i_Discipline_GoodConduct";
			}
			else{
				echo "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> $i_Discipline_Misconduct";
			}
		?>
	</td>
</tr>
<tr>
	<td class="field_title"><?=$iDiscipline['ConductCategoryItem']?> <span class="tabletextrequire">*</span></td>
	<td class="tabletext" valign="top"><?=$merit_display?></td>
</tr>

<?php 
if($sys_custom['eDiscipline']['PooiToMiddleSchool'] && $recordType == -1){ 
	$select_conduct_mark = '<SELECT name="ScoreChange" id="ScoreChange">'."\n";
	$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
	$increment_max = $ldiscipline->MisconductMaxScoreDeduction != ''? $ldiscipline->MisconductMaxScoreDeduction : 3;
	$j = 0;
	$loopConductMark = true;
	while($loopConductMark) {
		$select_conduct_mark .= '<OPTION value="'.$j.'"'.($j==$score_change?' selected':'').'>'.$j.'</OPTION>'."\n";    
		$j = (float)(string)($j + $conductMarkInterval);
		if(floatcmp($j ,">" ,$increment_max)) {
			$loopConductMark = false;
		}
	}
	$select_conduct_mark .= '</SELECT>'."\n";

echo '<tr> 
		<td valign="top" width="25%" nowrap="nowrap" class="field_title">'.$Lang['eDiscipline']['Score'].'</td>
		<td class="tabletext" valign="top">'.$Lang['eDiscipline']['Deduct'].' '.$select_conduct_mark.'<td>
	</tr>';
}
?>

<? if($sys_custom['eDiscipline']['GMConductMark'] && $recordType == -1)
{
	$j = 0;
	$increment_max = $sys_custom['eDiscipline']['HYK_MaxGMConductMark'];
	$conductMarkInterval = 1;
	$loopConductMark = true;
	
	// Conduct Mark drop down list
	$select_conduct_score = '<SELECT name="GMConductScoreChange" id="GMConductScoreChange">'."\n";
	while($loopConductMark) {
		$select_conduct_score .= '<OPTION value="'.$j.'"'.($j==$gm_conduct_score_change?' selected':'').'>'.$j.'</OPTION>'."\n";    
		$j = (float)(string)($j + $conductMarkInterval);
		if(floatcmp($j ,">" ,$increment_max)) {
			$loopConductMark = false;
		}
	}
	$select_conduct_score .= '</SELECT>'."\n";

	// Conduct Mark field
	echo '<tr> 
			<td valign="top" width="25%" nowrap="nowrap" class="field_title">'.$Lang['eDiscipline']['ConductMark'].'</td>
			<td class="tabletext" valign="top">'.$Lang['eDiscipline']['Deduct'].' '.$select_conduct_score.'<td>
		</tr>';
	}
?>

<? if($sys_custom['eDiscipline_GM_Times']) {?>
<tr>
	<td class="field_title"><?=$Lang['eDiscipline']['ReceiveTimes']?> <span class="tabletextrequire">*</span></td>
	<td class="tabletext" valign="top"><?=$GMCountSelection?></td>
</tr>
<? } ?>

<? if($school_year) {?>
<tr>
	<td class="field_title"><?=$i_Discipline_System_Conduct_School_Year?></td>
	<td> <?=$school_year?></td>
</tr>
<? } ?>

<? if($semesterName) {?>
<tr>
	<td class="field_title"><?=$i_Discipline_System_Conduct_Semester?></td>
	<td> <?=$semesterName?></td>
</tr>
<? } ?>

<tr>
	<td class="field_title"><?=$Lang['eDiscipline']['EventDate']?></td>
	<td> <?=$recordDate?></td>
</tr>

<tr>
	<td class="field_title"><?=$i_Discipline_System_Contact_PIC?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td><?=$PIC_selected?></td>
				<td valign="bottom">
					<? if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) { ?>
					<?//=$linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',11)");?>
					<?=$linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=PIC[]&permitted_type=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1', 11)")?><br />
					<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC[]'])");?>
					<? } ?>
				</td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td class="field_title"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td valign="bottom"><?=$attachmentHTML?></td>
			</tr>
		</table>
	</td>
</tr>

<? if($sys_custom['eDiscipline']['HOY_Access_GM'])
{
	$isHOYMember = $ldiscipline->IS_HOY_GROUP_MEMBER();
	if(!$isApplyHOY || ($isApplyHOY && $isHOYMember)) { ?>
		<tr>
			<td class="field_title"><?=$Lang['eDiscipline']['GM_HOYRemarks']?></td>
			<td>
				<?=$linterface->GET_TEXTAREA("remark", stripslashes($remark));?><br/>
				<input type='checkbox' name='ApplyHOY' id='ApplyHOY' value='1' <?=($isApplyHOY? "checked" : "");?>><label for='ApplyHOY'><?=$Lang['eDiscipline']['DetailsReferToHOY']?></label>
			</td>
		</tr>
	<? }
	else { ?>
		<tr>
			<td class="field_title"><?=$i_Discipline_Remark?></td>
			<td>
				<?=$Lang['eDiscipline']['DetailsReferToHOY']?>
				<input type="hidden" name="ApplyHOY" id="ApplyHOY" value="1">
				<input type="hidden" name="SkipRemarkUpdate" id="SkipRemarkUpdate" value="1">
			</td>
		</tr>
	<? }
}
else
{ ?>
	<tr>
		<td class="field_title"><?=$i_Discipline_Remark?></td>
		<td><?=$linterface->GET_TEXTAREA("remark", stripslashes($remark));?></td>
	</tr>
<? } ?>

<? if($sys_custom['eDisciplinev12_FollowUp']) {?>
<tr>
	<td class='field_title'><?=$Lang['eDiscipline']['FollowUp']?></td>
	<td><?=$ldiscipline->getFollowUpSelection(1,$ActionID,"ActionID")?></td>
</tr>
<? } ?>

<? if(trim($InternalRemark)) {?>
<tr>
	<td class="field_title"><?=$Lang['General']['StudentInternalRemark']?></td>
	<td><?=nl2br($InternalRemark)?></td>
</tr>
<? } ?>

<tr>
	<td style="border:none"><em class="form_sep_title"> - <?=$eDiscipline["Actions"]?> -</em></td>
</tr>
<tr>
	<td class="field_title"><?=$iDiscipline['Accumulative_Category_Item']?></td>
	<td>
		<table class="inside_form_table">
			<?=$DetentionOutputData?>
			<tr>
				<td><?=$actionContent?></td>
			</tr>
			<?=$msgButtonLabel?>
		</table>
	</td>
</tr>

<? if($CreatedBy > 0){ ?>
<tr>
	<td style="border:none"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['CreateUserInfo']?> -</em></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['AccountMgmt']['CreateUserName'] ?></td>
	<td colspan="2"><?=$nameDisplay?></td>
</tr>
<tr>
	<td class="field_title"><?=$Lang['AccountMgmt']['CreateUserDate'] ?></td>
	<td colspan="2"><?=$DateInput?></td>
</tr>	
<? } ?>

<tr>
	<td style="border:none; text-align:right" colspan='2' align='right' valign='middle' nowrap='nowrap'>
	<?
		if ($recordType != 1) {
			# check GM access right (edit)
			if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || ($ldiscipline->isConductPIC($RecordID) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"))) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-RearrangeStudent")) {
				# check detention access right (new)
				if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $DetentionOutputData=="") {
					echo $linterface->GET_BTN($DetentionButtonLabel, "button", $DetentionButtonAction);
				} # check detention access right (edit all / edit own)
				//else if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn") && $DetentionPICFlag)) && $DetentionOutputData) {
					else if(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditAll") || ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-EditOwn"))) && $DetentionOutputData) {
					echo $linterface->GET_BTN($DetentionButtonLabel, "button", $DetentionButtonAction);
				}
			}
		}
	?>&nbsp;<?=$noticeButton?><?php if($msgAvailable){ ?>&nbsp;<?=$messageButton?><?php } ?>
	</td>
</tr>
</table>

<table class="inside_form_table">
<tr>
	<td class='formfieldtitle'><img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif' width='10' height='1'></td>
</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "button", "checkForm();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='".$back_path."'")?>
				<? if($isNewLeaf==1) { echo $linterface->GET_ACTION_BTN($button_unwaive, "button", "javascript:goUnWaive($RecordID)"); } ?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">
<input type="hidden" id="DetentionAction" name="DetentionAction" />
<input type="hidden" name="RecordType" value="<?=$RecordType?>">
<input type="hidden" name="SchoolYear" value="<?=$SchoolYear?>">
<input type="hidden" name="semester" value="<?=$semester?>">
<input type="hidden" name="targetClass" value="<?=$targetClass?>">
<input type="hidden" name="approved" value="<?=$approved?>">
<input type="hidden" name="rejected" value="<?=$rejected?>">
<input type="hidden" name="waived" value="<?=$waived?>">
<input type="hidden" name="num_per_page" value="<?=$num_per_page?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>">
<input type="hidden" name="numPerPage" value="<?=$numPerPage?>">
<input type="hidden" name="fromPPC" value="<?=$fromPPC?>">
<input type="hidden" name="newleaf" value="<?=$newleaf?>">
<input type="hidden" name="pic" value="<?=$pic?>">
<input type="hidden" name="ClickID" id="ClickID" value="">
<input type="hidden" name="task" id="task" value="">
<input type="hidden" name="fieldname" id="fieldname" value="Attachment[]">
<input type="hidden" name="FolderLocation" id="FolderLocation" value="<?=$sessionTime."_"?>">
<input type="hidden" name="student[]" id="student[]" value="<?=$StudentID?>">
<input type="hidden" name="resetFollowup" value="0">
<input type="hidden" name="CatID" value="<?=$meritCategoryID?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>