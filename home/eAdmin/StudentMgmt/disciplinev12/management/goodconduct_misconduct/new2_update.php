<?php
# Using: YW

########## Change Log ###############
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose) (replace "U")
#	
#   Date    :   2019-05-14  Bill    [2019-0509-1450-25066]
#               Cast ${"GMCount_".$thisStudentID} to float
#
#   Date    :   2019-05-01  Bill
#               prevent Cross-site Scripting
#
#	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#				Support "Details refer to HOY" Setting ($sys_custom['eDiscipline']['HOY_Access_GM'])
#				Skip all Conversion Period Checking	($sys_custom['eDiscipline']['SkipAllGMPeriodChecking'])
#
# 	Date	:	2017-01-09	Bill	[2016-0808-1743-20240]
#				Auto insert unassigned detention for Not Submitted / Late misconduct	($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'])
#
#	Date	:	2016-09-29 (Bill)	[2016-0808-1526-52240]
#				- skip conversion period checking	($sys_custom['eDiscipline']['SkipGMPeriodChecking'])
#				- update conduct score for misconduct	($sys_custom['eDiscipline']['GMConductMark'])
#
#	Date	:	2015-10-26	(Bill)	[2015-0611-1642-26164] 			[REMOVED]
#				Change logic for CWC Probation Scheme
#				- Approve probation records if upgrade new punishment records
#
#	Date	:	2014-12-23	Bill
#				pass msg to new3.php - display remarks 
#
#	Date	:	2014-06-26 	Carlos
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - skip period settings for misconduct
#				2014-05-28	Carlos
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - update score change for misconduct type
#
#	Date	:	2010-08-20	YatWoon
#				- add ling leung yi man customization: follow-up action ($sys_custom['eDisciplinev12_FollowUp'])
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
foreach((array)$student as $this_key => $thisStudentID) {
    $thisStudentID = IntegerSafe($thisStudentID);
    $student[$this_key] = $thisStudentID;
    
    if(!intranet_validateDate(${"RecordDate_".$thisStudentID})) {
        ${"RecordDate_".$thisStudentID} = date('Y-m-d');
    }
    ${"semester_".$thisStudentID} = cleanCrossSiteScriptingCode(${"semester_".$thisStudentID});
    
    ${"record_type_".$thisStudentID} = IntegerSafe(${"record_type_".$thisStudentID});
    ${"CatID_".$thisStudentID} = IntegerSafe(${"CatID_".$thisStudentID});
    ${"ItemID_".$thisStudentID} = IntegerSafe(${"ItemID_".$thisStudentID});
    
    $thisPICArr = ${"PIC_".$thisStudentID};

    if(is_array($thisPICArr) && sizeof($thisPICArr) > 0) {
        foreach($thisPICArr as $this_key_2 => $thisPICID) {
			$thisPICID = str_replace("U", "", $thisPICID);
            $thisPICArr[$this_key_2] = IntegerSafe($thisPICID);
        }
    }
    else {
        $thisPICArr = IntegerSafe($thisPICArr);
    }
    ${"PIC_".$thisStudentID} = $thisPICArr;
    
    ${"ActionID_".$thisStudentID} = IntegerSafe(${"ActionID_".$thisStudentID});
    ${"GMCount_".$thisStudentID} = (float)(${"GMCount_".$thisStudentID});
    ${"ApplyHOY_".$thisStudentID} = IntegerSafe(${"ApplyHOY_".$thisStudentID});
}

$sessionTime = cleanCrossSiteScriptingCode($sessionTime);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");

//$semester_mode = getSemesterMode();

$isExist = array();
$hiddenField = "";
$success = 1;

# Not allow same student with same item in a same day
if($ldiscipline->NotAllowSameItemInSameDay) {
	foreach($student as $StudentID) {
		$thisCatID = ${"CatID_".$StudentID};
		$thisItemID = ${"ItemID_".$StudentID};
		$thisRecordDate = ${"RecordDate_".$StudentID};
		$isExist[$StudentID] = $ldiscipline->checkSameGMItemInSameDay($StudentID, $thisCatID, $thisItemID, $thisRecordDate);
		if($isExist[$StudentID]==1) $success = 0;
	}
}

if($success)
{
	foreach($student as $StudentID)
	{
		$thisRecordType = ${"record_type_".$StudentID};
		$thisCatID = ${"CatID_".$StudentID};
		$thisItemID = ${"ItemID_".$StudentID};
		$thisRecordDate = ${"RecordDate_".$StudentID};
		$thisPIC = ${"PIC_".$StudentID};
		$thisremark = ${"remark_".$StudentID};
		$thisremark = intranet_htmlspecialchars($thisremark);
		$thisAttachment = $sessionTime."_".$StudentID."tmp";
		$thisActionID = ${"ActionID_".$StudentID};
		$thisGMCount = ${"GMCount_".$StudentID};
		
		# School Year
		$school_year = GET_ACADEMIC_YEAR3($thisRecordDate);
		
		/*
		# Semester
		if($semester_mode==2)
			$thisSemester = retrieveSemester($thisRecordDate);
		else
			$thisSemester = ${"semester_".$StudentID};	
		*/
		$semInfo = getAcademicYearAndYearTermByDate($thisRecordDate);
		$thisSemester = $semInfo[1];
		
		$lu = new libuser($StudentID);
		
		### Check Record Date in one of period (with Conversion Setting)
		$period_info = $ldiscipline->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($thisRecordDate,$thisRecordType,$thisCatID);
		
		// [2017-0403-1552-54240] Skip Conversion Period Checking for all Conduct Type	($sys_custom['eDiscipline']['SkipAllGMPeriodChecking'])
		// [2016-0808-1526-52240] Skip Conversion Period Checking for Misconduct (added $sys_custom['eDiscipline']['SkipGMPeriodChecking'])
		// Have Conversion Period Setting or Pooi To Cust - Skip Conversion Period Checking for Misconduct
		if(sizeof($period_info) || $sys_custom['eDiscipline']['SkipAllGMPeriodChecking'] || (($sys_custom['eDiscipline']['PooiToMiddleSchool'] || $sys_custom['eDiscipline']['SkipGMPeriodChecking']) && $thisRecordType==-1))
		{
			# Insert Into [DISCIPLINE_ACCU_RECORD]
			$dataAry = array();
			$dataAry['CategoryID'] = $thisCatID;
			$dataAry['ItemID'] = $thisItemID;
			$dataAry['RecordDate'] = $thisRecordDate;
			$dataAry['Remark'] = $thisremark;
			$dataAry['PICID'] = (sizeof($thisPIC)>0) ? implode(',',$thisPIC) : $_SESSION['UserID'];
			//$dataAry['PICID'] = $UserID;
			$dataAry['RecordType'] = $thisRecordType;
			$dataAry['Year'] = addslashes($school_year);
			$dataAry['Semester'] = addslashes($thisSemester);
			$dataAry['AcademicYearID'] = $ldiscipline->getAcademicYearIDByYearName($school_year);
			$dataAry['StudentID'] = $StudentID;
			$dataAry['YearTermID'] = $semInfo[0];
			$dataAry['Attachment'] = $thisAttachment;
			$dataAry['ActionID'] = $thisActionID;
			$dataAry['GMCount'] = $sys_custom['eDiscipline_GM_Times'] ? $thisGMCount : 1;
			if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
				//$dataAry['ScoreChange'] = $thisRecordType==-1? $_REQUEST['ScoreChange_'.$StudentID] : 0;
				
			    $studentScoreChange = IntegerSafe($_REQUEST['ScoreChange_'.$StudentID]);
			    $dataAry['ScoreChange'] = $thisRecordType==-1? $studentScoreChange : 0;
			}
			// [2016-0808-1526-52240]
			if($sys_custom['eDiscipline']['GMConductMark']){
				//$dataAry['GMConductScoreChange'] = $thisRecordType==MISCONDUCT? ($_REQUEST['GMConductScoreChange_'.$StudentID] * -1) : 0;
			    
			    $studentGMConductScoreChange = IntegerSafe($_REQUEST['GMConductScoreChange_'.$StudentID]);
			    $dataAry['GMConductScoreChange'] = $thisRecordType==MISCONDUCT? ($studentGMConductScoreChange * -1) : 0;
			}
			// [2017-0403-1552-54240]
			if($sys_custom['eDiscipline']['HOY_Access_GM']) {
				$dataAry['ApplyHOY'] = (${"ApplyHOY_".$StudentID})? 1 : 0;
			}
			
			$accu_record_id = $ldiscipline->INSERT_MISCONDUCT_RECORD($dataAry);
			if($accu_record_id)
			{
				# do the grouping
				// [2015-0611-1642-26164]
//				if($sys_custom['eDiscipline']['CSCProbation']){
//					// approve probation record if update to punishment records
//					$ldiscipline->REGROUP_CONDUCT_RECORDS($accu_record_id, "", 1);
//				}
//				// Normal Case
//				else{
//					$ldiscipline->REGROUP_CONDUCT_RECORDS($accu_record_id);	
//				}
				$ldiscipline->REGROUP_CONDUCT_RECORDS($accu_record_id);
				
				// [2016-0808-1743-20240] Insert Detention (Not Submitted / Late Misconduct)
				if($sys_custom['eDiscipline']['BWWTC_AutoCreateDetention'] && ($thisCatID == $sys_custom['eDiscipline']['BWWTC_WithoutSubmCatID'] || $thisCatID == $sys_custom['eDiscipline']['BWWTC_LateCatID']))
				{
					$ldiscipline->INSERT_LATE_HW_DETNETION_RECORD($accu_record_id);
				}
				
				$HiddenGMID .= '<input type="hidden" name="GMID_'.$StudentID.'" value="'.$accu_record_id.'">';
				
				// [2016-0808-1526-52240] Update Conduct Balance for Misconduct
				if($sys_custom['eDiscipline']['GMConductMark'] && $dataAry['RecordType'] == MISCONDUCT && $dataAry['GMConductScoreChange'] != 0)
				{
					list($school_year_id, $school_year, $semester_id, $semester) = getAcademicYearInfoAndTermInfoByDate($dataAry['RecordDate']);
					$ldiscipline->UPDATE_CONDUCT_BALANCE($dataAry['StudentID'], $school_year, $semester, $dataAry['GMConductScoreChange'], "", DISCIPLINE_CONDUCT_SCORE_CHANGE_TYPE_INPUT, $school_year_id, $semester_id, $accu_record_id);
				}
			}
		}
	}
	
	$path = "$file_path/file/disciplinev12/goodconduct_misconduct/".$sessionTime."tmp"; 
	if(is_dir($path) && $sessionTime!="") {
		$ldiscipline->deleteDirectory($path);
	}
	
	$formAction = "new3.php?msg=add";	
}
else {
	foreach($isExist as $key => $val) {
		if($val==1) {
			$hiddenField .= "<input type='hidden' name='sid[]' id='sid[]' value='$key'>\n";
		}
	}
	$formAction = "new2.php";
}

//header("Location: index.php?msg=add");
?>

<body onLoad="document.form1.submit();">
<form name="form1" method="POST" action="<?=$formAction?>">
<?=$HiddenGMID?>
<?
foreach($_POST as $Key => $Value)
{
	if($Key == 'student')
	{
	    $Value = IntegerSafe($Value);
		echo "<input type=\"hidden\" name=\"$Key\" value=\"".implode(",",$Value)."\">\n";
	}
	else
	{
	    if(substr($Key,0,10) == "RecordDate")
	    {
	        if(!intranet_validateDate($Value)) {
	            $Value = date('Y-m-d');
    	    }
	    }
	    else if(substr($Key,0,8) == "semester")
	    {
	        $Value = cleanCrossSiteScriptingCode($Value);
	    }
	    else if(substr($Key,0,11) == "record_type" || substr($Key,0,5) == "CatID" || substr($Key,0,6) == "ItemID" || substr($Key,0,11) == "ScoreChange" || substr($Key,0,20) == "GMConductScoreChange" || 
	        substr($Key,8) == "ActionID" || substr($Key,8) == "ApplyHOY")
	    {
            $Value = IntegerSafe($Value);
	    }
	    else if(substr($Key,0,7) == "GMCount")
	    {
	        $Value = (float)$Value;
	    }
		echo "<input type=\"hidden\" name=\"$Key\" value=\"$Value\">\n";
	}
}
echo $hiddenField;
?>
</form>
	
<script language="javascript">
	<!--
	document.form1.submit();
	//-->
</script>
</body>

<?
intranet_closedb();
?>