<?php
// Modifying by:   

############## Change Log [Start] ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default not distribute notice to parent	($sys_custom['eDiscipline']['NotReleaseNotice'])
#
#	Date	:	2015-04-30	Bill	[2014-1216-1347-28164]
#				send push message to parent when $eClassAppNotify = 1
#				update DB after send out push message
#
#	Date	:	2010-05-10	YatWoon
#				add PIC data
#
############## Change Log [End] ###############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();
# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");

$lteaching = new libteaching();

$lc = new libucc();
$remark = intranet_htmlspecialchars($remark);

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	$luser = new libuser();
	$leClassApp = new libeClassApp();
	$canSendPushMsg = true;
}

// Get submitted data for eNotice
/*for ($j=1; $j<=sizeof($student); $j++) {
	${"submitSelectNotice".$j} = $_POST["SelectNotice".$j];
	${"submitTextAdditionalInfo".$j} = stripslashes($_POST["TextAdditionalInfo".$j]);
	${"submitStudentID".$j} = $_POST["StudentID".$j];
	${"submitStudentName".$j} = $_POST["StudentName".$j];
}*/

if(!is_array($student)) $student = explode(",",$student);
foreach($_POST as $Key=>$Value)
{
	if(substr($Key,0,5)=="CatID" && strlen($Key)!="5")
	{
		$ID = substr($Key,6);
		$CatArr[$ID] = $Value;
	}
	if(substr($Key,0,6)=="ItemID" && strlen($Key)!="6")
	{
		$ID = substr($Key,7);
		$ItemArr[$ID] = $Value;
	}
	if(substr($Key,0,4)=="GMID")
	{
		$ID = substr($Key,5);
		$GMIDArr[$ID] = $Value;	
	}
}

$ct=1;

$student = array_unique($student);

foreach($student as $k=>$StudentID)
{ 
		# Detention
		if ($detentionFlag == "YES") {
			
			#Reason
			$ConductItem = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT();
			
			for($a=0;$a<sizeof($ConductItem);$a++)
			{
				/*
				if($ConductItem[$a]['ItemID'] == $ItemArr[$StudentID])
				{
					$CatName = $ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME($CatArr[$StudentID]);
					$Reason = $CatName." - ".$ConductItem[$a]['Name'];
				}
				*/
				if($ConductItem[$a][1] == $ItemArr[$StudentID] && $ConductItem[$a][0] == $CatArr[$StudentID])
				{
					$CatName = $ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME($CatArr[$StudentID]);
					$Reason = $CatName." - ".$ConductItem[$a][2];
				}
				
			}
			
			#GM ID
			$GMID = $GMIDArr[$StudentID];
			
			for ($i=0; $i<sizeof($DetentionArr); $i=$i+3) {
				// array sequence: StudentID, DetentionID, Remark
				if ($StudentID == $DetentionArr[$i]) {
					$detentionDataArr = array();
					$detentionDataArr['StudentID'] = "'".$DetentionArr[$i]."'";
					if ($DetentionArr[$i+1] == "") $DetentionArr[$i+1] = "NULL";
					$detentionDataArr['DetentionID'] = $DetentionArr[$i+1];
					$detentionDataArr['Remark'] = "'".stripslashes($DetentionArr[$i+2])."'";
					$detentionDataArr['RequestedBy'] = "'".$UserID."'";
					$detentionDataArr['Reason'] = "'".intranet_htmlspecialchars(addslashes($Reason))."'";
					$detentionDataArr['GMID'] = "'".${"GMID_".$StudentID}."'";
					if ($DetentionArr[$i+1] != "NULL") {
						$detentionDataArr['ArrangedBy'] = "'".$UserID."'";
						$detentionDataArr['ArrangedDate'] = 'NOW()';
					}
					$detentionDataArr['PICID'] = "'".$UserID."'";
					
					$detStuRecordID = $ldiscipline->insertDetention($detentionDataArr);
					
				}
			}
		}
		
		# Save eNotice / Save push message
		// [2014-1216-1347-28164] add eClassApp push message
//		if (!$lnotice->disabled)
		if(!$lnotice->disabled || $canSendPushMsg)
		{
			if ($noticeFlag == "YES") 
			{
				//${"submitTextAdditionalInfo".$i}
				$TemplateID = ${"SelectNotice".$ct};
				$TemplateAddInfo = stripslashes((stripslashes(${"TextAdditionalInfo".$ct})));
				$emailNotify = ${"emailNotify".$ct};
				// [2014-1216-1347-28164]
				$eClassAppNotify = ${"eClassAppNotify".$ct};
				
				$TemplateCategory = ${"CategoryID".$ct};
				$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($TemplateID);
				if(sizeof($template_info))
				{
					$SpecialData = array();
					$SpecialData['ConductRecordID'] = $GMIDArr[$StudentID];
					if($sys_custom['eDisciplinev12_eNoticeTemplate_AccuNumber'])
						$SpecialData['AccuNumber'] = $ldiscipline->RETURN_ACCUMULATED_CONDUCT_NUMBER_FOR_ENOTICE_TEMPLATE($StudentID, $GMIDArr[$StudentID], $CatArr[$StudentID]);
					
					$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($TemplateCategory, $StudentID, $SpecialData, $TemplateAddInfo);
					
					$UserName = $ldiscipline->getUserNameByID($StudentID);
					
					$Module = $ldiscipline->Module;
					$NoticeNumber = time();
					$Variable = $template_data;
					$IssueUserID = $UserID;
					$IssueUserName = $UserName[0];
					$TargetRecipientType = "U";
					$RecipientID = array($StudentID);
					$RecordType = NOTICE_TO_SOME_STUDENTS;
					
					include_once($PATH_WRT_ROOT."includes/libucc.php");
					$lc = new libucc();
					$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);
					
					// [2014-1216-1347-28164] add eNotice checking before sending notice
					if(!$lnotice->disabled)
					{
                        // [2020-1009-1753-46096] delay notice start date & end date
                        if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                        {
                            $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                            $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                            $lc->setDateStart($DateStart);

                            if ($lc->defaultDisNumDays > 0) {
                                $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                                $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                                $lc->setDateEnd($DateEnd);
                            }
                        }

						$NoticeID = $lc->sendNotice($emailNotify);
						$ldiscipline->updateMisconductRecordNoticeID($NoticeID, $GMIDArr[$StudentID]);
						
						// [2016-0823-1255-04240]
						if($NoticeID != -1 && $sys_custom['eDiscipline']['NotReleaseNotice'])
						{
							$lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
						}
					}
					
					// [2014-1216-1347-28164] send eClassApp push message
					if($canSendPushMsg && $luser->getParentUsingParentApp($RecipientID, true) && $eClassAppNotify){
						// structure data for sending message
						$lc->restructureData();
				
						// get ParentID and StudentID mapping
						$sql = "SELECT 
										ip.ParentID, ip.StudentID
								from 
										INTRANET_PARENTRELATION AS ip 
										Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
								WHERE 
										ip.StudentID IN ('".implode("','", (array)$RecipientID)."') 
										AND iu.RecordStatus = 1";
						$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
						
						// prepare push message data
						$messageInfoAry = array();
						$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
						$messageContent = removeHTMLtags($lc->Description);
						$messageContent = intranet_undo_htmlspecialchars($messageContent);
						$messageContent = str_replace("&nbsp;", "", $messageContent);
						$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $lc->Subject, $messageContent);
						
						// update db if $notifyMessageId valid
						if($notifyMessageId){
							// get GM record ID
							$GMID = $GMIDArr[$StudentID];
							$sql = "UPDATE DISCIPLINE_ACCU_RECORD SET PushMessageID='$notifyMessageId' WHERE RecordID='$GMID'";	
							$ldiscipline->db_db_query($sql);
						}
					}
				}
				$ct++;
			}
		}
}

intranet_closedb();
header("Location: index.php?msg=add");

?>