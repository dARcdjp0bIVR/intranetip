<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}


if($RecordID == "") {
	header("Location: index.php");
}

$unwaive = $ldiscipline->UNWAIVE_CONDUCT_RECORD($RecordID);

$msg = $unwaive ? "unwaive" : "no_unwaive_acc_record";
header("Location: index.php?msg=$msg&approved=$approved&rejected=$rejected&waived=$waived&RecordType=$RecordType&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&s=$s&pageNo=$pageNo&order=$order&field=$field&page_size=$numPerPage&RecordType=$RecordType&s=$s&pic=$pic");

intranet_closedb();
?>