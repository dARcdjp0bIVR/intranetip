<?php
// Modifying by: henry

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();


$ldiscipline = new libdisciplinev12();

$datainfo = $ldiscipline->RETRIEVE_CONDUCT_INFO($RecordID);
$data = $datainfo[0];

$NoticeID = $data['NoticeID'];

if(isset($NoticeID) && $NoticeID!="") {

	# delete Notice & Reply Slip
	$ldiscipline->removeNotice($NoticeID);
		
	# update Conduct Record
	$sql = "UPDATE DISCIPLINE_ACCU_RECORD SET NoticeID=NULL WHERE RecordID=$RecordID";
	$ldiscipline->db_db_query($sql);
}
intranet_closedb();

$url = "index.php?msg=update";

header("Location: $url");
?>
