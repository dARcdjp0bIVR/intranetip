<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$RecordIDAry = array();

if(is_array($RecordID))
	$RecordIDAry = $RecordID;
else
	$RecordIDAry[] = $RecordID;
	

$all_update = 1;
foreach($RecordIDAry as $ConductRecordID)
{
	$this_update = $ldiscipline->UPDATE_PPC_STATUS_CONDUCT_RECORD($ConductRecordID);
	$all_update = $this_update ? $all_update : 0;
}


$msg = $all_update ? "update" : "update_failed";
header("Location: index.php?msg=$msg&approved=$approved&rejected=$rejected&waived=$waived&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&s=$s&pageNo=$pageNo&order=$order&field=$field&page_size=$numPerPage&RecordType=$RecordType&s=$s&fromPPC=$fromPPC&pic=$pic");

intranet_closedb();
?>