<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf");

if(!$ldiscipline->use_newleaf) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$RecordIDAry = array();


if(is_array($RecordID))
	$RecordIDAry = $RecordID;
else
	$RecordIDAry[] = $RecordID;
	
$all_waive = 1;

foreach($RecordIDAry as $ConductRecordID)
{
	$this_waive = $ldiscipline->WAIVE_CONDUCT_RECORD($ConductRecordID);
	$all_waive = $this_waive ? $all_waive : 0;
}

$msg = $all_waive ? "waive" : "no_waive_acc_record";
header("Location: index.php?msg=$msg&approved=$approved&rejected=$rejected&waived=$waived&RecordType=$RecordType&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&s=$s&pageNo=$pageNo&order=$order&field=$field&page_size=$numPerPage&RecordType=$RecordType&s=$s&newleaf=$newleaf&pic=$pic");

intranet_closedb();
?>