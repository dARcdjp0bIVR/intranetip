<?php
# Using: YW

####################### Change Log [start] ###########
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose)
#
#   Date    :   2019-05-01  Bill
#               prevent Cross-site Scripting
#
#	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#				added "Details refer to HOY" checkbox ($sys_custom['eDiscipline']['HOY_Access_GM'])
#				modified js function check_period() to skip all Conversion Period Checking	($sys_custom['eDiscipline']['SkipAllGMPeriodChecking'])
#
#	Date	:	2017-10-11 (Bill)	[DM#3270]
#				- return to step 1 if all selected groups without any users
#
#	Date	:	2017-02-20 (Bill)
#				- set max conduct score deduct selection	(depend on $sys_custom['eDiscipline']['HYK_MaxGMConductMark'])
#
#	Date	:	2016-09-29 (Bill)	[2016-0808-1526-52240]
#				- modified js check_period() to skip misconduct period checking	($sys_custom['eDiscipline']['SkipGMPeriodChecking'])
#				- added conduct score deduct selection for misconduct	($sys_custom['eDiscipline']['GMConductMark'])
#
#	Date	:	2016-08-17	Bill	[2016-0726-1631-34073]
#				modified js check_form(), to perform record date checking when Setting : Allow to input discipline records within the last ... day(s) is empty
#
#	Date	:	2016-07-11	Bill
#				change split() to explode(), for PHP 5.4
#
#	Date	:	2014-06-26 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - js skip check_period() for misconduct
#				2014-05-28 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added score change selection for misconduct type
#				
#	Date	:	2012-02-13 (Henry Chow)
#	Detail 	:	disable "Submit" button on form submission
#
#	Date	:	2010-08-20	YatWoon
#				- add ling leung yi man customization: follow-up action ($sys_custom['eDisciplinev12_FollowUp'])
#
#	Date	:	2010-08-18	YatWoon
#				display student internal remark for client reference
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
# 
#	Date:	2010-04-14 YatWoon
#			check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
####################### Change Log [end] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

if(sizeof($student)==0) {
	header("Location: new1.php");	
	exit;
}

$ldiscipline = new libdisciplinev12();
$lgrouping = new libgrouping();

## Get the max. no of record day(s) ##
$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
if($NumOfMaxRecordDays > 0) {
	$EnableMaxRecordDaysChecking = 1;
	$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
	$DateMaxLimit = date("Y-m-d");
}
else {
	$EnableMaxRecordDaysChecking = 0;
}

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New");

$linterface = new interface_html();
$lteaching = new libteaching();

# Menu
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

# Step
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 1);
$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 0);

# Navigation Bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["NewRecord"], "");

# Get data from preious page
if(!is_array($_POST['student'])) {
	$student = explode(',',$student);	
}
else {
	$student = $_POST['student'];
}
if(empty($student)) {
	header("Location: new1.php");
	exit;
}

# Retrieve all students (from U - User, G - Group)
$student2 = array();
$permitted[] = 2;
foreach($student as $k => $d)
{
	$type = substr($d, 0, 1);
	$type_id = substr($d, 1);
	$type_id = IntegerSafe($type_id);
	
	switch($type)
	{
		case "U":
			$student2[] = $type_id;
			break;
		case "G":
			$groupIDAry[] = $type_id;
			$temp = $lgrouping->returnGroupUsersInIdentity($groupIDAry, $permitted);		
			//$temp = $lgrouping->returnGroupUsers($type_id);
			foreach($temp as $k1 => $d1) {
				$student2[] = $d1['UserID'];	
			}
			break;
		case "C":
			$temp = $ldiscipline->getStudentListByClassID($type_id);
			for($i=0; $i<sizeof($temp); $i++) {
				$student2[] = $temp[$i][0];	
			}
			break;
		default:
		    $d = IntegerSafe($d);
			$student2[] = $d;
			break;
	}
}
if(is_array($student2)) {
	$student = array_keys(array_count_values($student2));
}

// [DM#3270]
if(empty($student)) {
	header("Location: new1.php");
	exit;
}

### Handle SQL Injection + XSS [START]
foreach($student as $thisStudentID) {
    $thisStudentID = IntegerSafe($thisStudentID);
    
    if(!intranet_validateDate(${"RecordDate_".$thisStudentID})) {
        ${"RecordDate_".$thisStudentID} = '';
    }
    ${"semester_".$thisStudentID} = cleanCrossSiteScriptingCode(${"semester_".$thisStudentID});
    
    ${"record_type_".$thisStudentID} = IntegerSafe(${"record_type_".$thisStudentID});
    ${"CatID_".$thisStudentID} = IntegerSafe(${"CatID_".$thisStudentID});
    ${"ItemID_".$thisStudentID} = IntegerSafe(${"ItemID_".$thisStudentID});
    
    ${"ApplyHOY_".$thisStudentID} = IntegerSafe(${"ApplyHOY_".$thisStudentID});
}

if(empty($PIC)) {
    $PIC[] = $_SESSION['UserID'];
}
if(is_array($PIC) && sizeof($PIC) > 0) {
    foreach($PIC as $thisKey => $thisPICID) {
        $PIC[$thisKey] = IntegerSafe($thisPICID);
    }
}

// $semester = cleanCrossSiteScriptingCode($semester);
$sessionTime = cleanCrossSiteScriptingCode($sessionTime);
### Handle SQL Injection + XSS [END]

if($sessionTime == "") {
    $sessionTime = session_id()."-".(time());
}

if(!isset($GMCount) || $GMCount=="") $GMCount = "1.00";

// if(empty($PIC)) {
// 	$PIC[] = $_SESSION['UserID'];
// }
if (is_array($PIC) && sizeof($PIC) > 0)
{
	$list = implode(",", $PIC);
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($list) AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
}
$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");

$path = "$file_path/file/disciplinev12/goodconduct_misconduct/$sessionTime";
$attSelect = $linterface->GET_SELECTION_BOX($ldiscipline->getAttachmentArray($path,''),"id=\"Attachment[]\" name=\"Attachment[]\"  multiple='multiple'","");
$attachment = "<table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$attSelect."
				</td>
				<td valign=\"bottom\">".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."tmp&fieldname=Attachment[]', 9)")."
				<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment('Attachment[]','','layerall')")."<div id='layerall'></div>
				</td>
				</tr>
				</table>";

## Build HTML Part
# Individual Student
for($i=0;$i<sizeof($student);$i++)
{
	$StudentID = $student[$i];
	$lu = new libuser($StudentID);
	$student_name = $lu->UserNameLang();
	$class_name = $lu->ClassName;
	$class_no = ($class_name ? "-":"").$lu->ClassNumber;
	/*
	if($semester_mode==1)
	{
		$select_sem = getSelectSemester("name=semester_{$StudentID}", $semester);	
		$this_select_sem_html = "
			<tr>
				<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">{$i_SettingsSemester}</td>
				<td valign=\"top\">{$select_sem}</td>
			</tr>
		";
	}
	else
	{
		$this_select_sem_html = "<input type='hidden' name='semester_{$StudentID}' value=''>";
	}
	*/
	
	if(${"record_type_".$student[$i]}!=1)
	{
		$misconduct_checked = "checked";
	}
	else
	{
		$good_conduct_checked = "checked";
	}
	
	$selected_student_tables .= "
		<br>
		<table align='center' width='90%' border='0' cellpadding='5' cellspacing='0'>
		<tr>
			<td colspan='2' valign='top' nowrap='nowrap' class='sectiontitle'>". ($i+1) ." <img src='".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif' width='20' height='20' align='absmiddle'>{$class_name}{$class_no} {$student_name}</td>
		</tr>
		<tr valign='top'>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$eDiscipline["Type"]}<span class='tabletextrequire'>*</span></td>
			<td width='80%'>
				<input name='record_type_{$StudentID}' type='radio' id='conduct_type_good_{$StudentID}' value='1' onClick='changeConductType({$StudentID})' $good_conduct_checked><img src='".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'> <label for='conduct_type_good_{$StudentID}'>{$eDiscipline['Setting_GoodConduct']}</label>
				<input name='record_type_{$StudentID}' type='radio' id='conduct_type_mis_{$StudentID}' value='-1' onClick='changeConductType({$StudentID})' $misconduct_checked><img src='".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'> <label for='conduct_type_mis_{$StudentID}'>{$eDiscipline['Setting_Misconduct']}</label>
			</td>
		</tr>
		<tr valign='top'>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['ConductCategoryItem']}<span class='tabletextrequire'>*</span></td>
			<td>
				<select name='CatID_{$StudentID}' id='CatID_{$StudentID}' onChange='changeCat({$StudentID}, this.value);'></select> 
				- 
				<SELECT name='ItemID_{$StudentID}' id='ItemID_{$StudentID}' onChange='changeFollowUp({$StudentID}, this.value);'>
					<OPTION value='0' selected>-- ". $eDiscipline["SelectConductItem"] ." --</OPTION>
				</SELECT>
			</td>
			<td>";
	if(sizeof($sid)>0)
		if(in_array($StudentID,$sid)) $selected_student_tables .= $linterface->GET_SYS_MSG("",$Lang['eDiscipline']['ItemAlreadyAdded']);
	$selected_student_tables .=	"
			</td>
		</tr>";
	
	if($sys_custom['eDiscipline']['PooiToMiddleSchool'])
	{
		$select_conduct_mark = '<SELECT name="ScoreChange_'.$StudentID.'" id="ScoreChange_'.$StudentID.'">'."\n";
		$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
		$increment_max = $ldiscipline->MisconductMaxScoreDeduction != ''? $ldiscipline->MisconductMaxScoreDeduction : 3;
		
		$j = 0;
		$loopConductMark = true;
		while($loopConductMark) {
			$select_conduct_mark .= '<OPTION value="'.$j.'">'.$j.'</OPTION>'."\n";
			$j = (float)(string)($j + $conductMarkInterval);
			if(floatcmp($j ,">" ,$increment_max)) {
				$loopConductMark = false;
			}
		}
		$select_conduct_mark .= '</SELECT>'."\n";
		
		$selected_student_tables .= '<tr id="tr_decrement_score_'.$StudentID.'"> 
										<td valign="top" nowrap="nowrap" class="formfieldtitle">'.$Lang['eDiscipline']['Score'].'</td>
										<td><span id="decrement_score_'.$StudentID.'">'.$Lang['eDiscipline']['Deduct'].'</span> '.$select_conduct_mark.'<td>
									</tr>';
	}
	
	// [2016-0808-1526-52240] Conduct Mark drop down list
	if($sys_custom['eDiscipline']['GMConductMark'])
	{
		$j = 0;
		$increment_max = $sys_custom['eDiscipline']['HYK_MaxGMConductMark'];
		$conductMarkInterval = 1;
		$loopConductMark = true;
		
		// Drop Down List
		$select_conduct_score = '<SELECT name="GMConductScoreChange_'.$StudentID.'" id="GMConductScoreChange_'.$StudentID.'">'."\n";
		while($loopConductMark) {
			$select_conduct_score .= '<OPTION value="'.$j.'">'.$j.'</OPTION>'."\n";
			$j = (float)(string)($j + $conductMarkInterval);
			if(floatcmp($j ,">" ,$increment_max)) {
				$loopConductMark = false;
			}
		}
		$select_conduct_score .= '</SELECT>'."\n";
		
		// Conduct Mark field
		$selected_student_tables .= '<tr id="tr_conduct_score_change_'.$StudentID.'"> 
										<td valign="top" nowrap="nowrap" class="formfieldtitle">'.$Lang['eDiscipline']['ConductMark'].'</td>
										<td><span id="conduct_score_change_'.$StudentID.'">'.$Lang['eDiscipline']['Deduct'].'</span> '.$select_conduct_score.'<td>
									</tr>';
	}
	
	if($sys_custom['eDiscipline_GM_Times']) {
		$thisGMCountSelection = $ldiscipline->Get_GM_Count_Selection($thisGMCount="1.00","GMCount_{$StudentID}");
		$selected_student_tables .= "
			<tr valign='top'>
				<td valign='top' nowrap='nowrap' class='formfieldtitle'>".$Lang['eDiscipline']['ReceiveTimes']."<span class='tabletextrequire'>*</span></td>
				<td>$thisGMCountSelection</td>
			</tr> ";
	}
	$selected_student_tables .= $this_select_sem_html;
	
	$this_remark = ${"remark_".$StudentID};
	$this_remark = stripslashes(intranet_htmlspecialchars($this_remark));
	
	$selected_student_tables .= "		
		<tr>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$Lang['eDiscipline']['EventDate']}<span class='tabletextrequire'>* </span></td>
			<td valign='top'>". $linterface->GET_DATE_PICKER("RecordDate_".$StudentID,(${"RecordDate_".$student[$i]} ? ${"RecordDate_".$student[$i]} : date('Y-m-d'))) ."</td>
		</tr>
		
		<tr>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>$i_Discipline_System_Contact_PIC<span class='tabletextrequire'>* </span></td>
			<td>		
				<table>
					<tr>
						<td>".$linterface->GET_SELECTION_BOX($array_PIC, "name='PIC_{$StudentID}[]' id='PIC_{$StudentID}[]' class='select_studentlist' size='6' multiple='multiple'", "")."</td>
						<td valign='bottom'>";
						if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) )
						{
							//$selected_student_tables .= $linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC_{$StudentID}[]',11)")."<br />";
							$selected_student_tables .= $linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=PIC_{$StudentID}[]&permitted_type=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1', 11)")."<br />";
							$selected_student_tables .= $linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC_{$StudentID}[]'])");
						}
	$selected_student_tables .= "<input type='hidden' name='temp' id='temp' value='1'>
						</td>
					</tr>
				</table>
			</td>			
		</tr>
		
		<tr>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>$i_Discipline_System_Discipline_Case_Record_Attachment</td>
			<td>";
			
			$path = "$file_path/file/disciplinev12/goodconduct_misconduct/".$sessionTime."_".$StudentID."tmp";
			$attSelect = $linterface->GET_SELECTION_BOX($ldiscipline->getAttachmentArray($path,''),"id=\"Attachment_{$StudentID}[]\" name=\"Attachment_{$StudentID}[]\"  multiple='multiple'","");
			
	$selected_student_tables .= "<table width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr>
					<td>".$attSelect."</td>
					<td valign=\"bottom\">
						".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."_".$StudentID."tmp&fieldname=Attachment_".$StudentID."[]', 9)")."
						<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment('Attachment_{$StudentID}[]', $StudentID, 'layer_$StudentID')")."<div id=\"layer_$StudentID\"></div>
					</td>
				</tr>
				</table>";
		$selected_student_tables .= "
			</td>
		</tr>";
	
	// [2017-0403-1552-54240]
	if($sys_custom['eDiscipline']['HOY_Access_GM'])
	{
		$thisApplyHOYChecked = ${"ApplyHOY_".$StudentID}? " checked" : "";
		$selected_student_tables .= "
		<tr>
			<td valign='top' class='formfieldtitle'>{$Lang['eDiscipline']['GM_HOYRemarks']}</td>
			<td>
				".$linterface->GET_TEXTAREA("remark_".$StudentID, $remark)."<br/>
				<input type='checkbox' name='ApplyHOY_".$StudentID."' id='ApplyHOY_".$StudentID."' value='1' ".$thisApplyHOYChecked."><label for='ApplyHOY_".$StudentID."'>".$Lang['eDiscipline']['DetailsReferToHOY']."</label>
			</td>
		</tr> ";
	}
	else
	{
		$selected_student_tables .= "
		<tr>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_UserRemark}</td>
			<td>".$linterface->GET_TEXTAREA("remark_".$StudentID, $remark)."</td>
		</tr> ";
	}
	
	##### Follow-up Action
	if($sys_custom['eDisciplinev12_FollowUp'])
	{
		$selected_student_tables .= "
		<tr>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$Lang['eDiscipline']['FollowUp']}</td>
			<td>". $ldiscipline->getFollowUpSelection(1,0,"ActionID_".$StudentID) ."</td>
		</tr> ";
	}
	
	##### Student Internal Remark
	if(trim($lu->Remark)) 
	{
		$selected_student_tables .= "
		<tr>
			<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$Lang['General']['StudentInternalRemark']}</td>
			<td>". nl2br($lu->Remark) ."</td>
		</tr> ";
	}
	
	if($i<sizeof($student)-1) 
	{
		$selected_student_tables .= "
			<tr>
				<td height='1' colspan='2' class='dotline'><img src='".$image_path."/".$LAYOUT_SKIN."/10x10.gif' width='10' height='1'></td>
			</tr> ";
	}
	
	$selected_student_tables .= "</table>";
}

# Get Category Selection
$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1, 1);
$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT();
	
# Build javascript for Category Period Checking
$good_period_list = array();
for($i=0; $i<sizeof($good_cats); $i++) {
	$temp= $ldiscipline->RETRIEVE_CONDUCT_CATEGORY_PERIOD_LIST(1, $good_cats[$i][0]);	
	if(sizeof($temp)>0) $good_period_list = array_merge($temp, $good_period_list);
}
//$good_period_list = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY_PERIOD_LIST(1);
$js="var good_cat_js = new Array();\n";
for($i=0; $i<sizeof($good_period_list); $i++){
	list($a_cat_id,$a_period_id,$a_period_start,$a_period_end)=$good_period_list[$i];
	$js.="if(good_cat_js[$a_cat_id]==null) good_cat_js[$a_cat_id] = new Array();\n";
	$js.="good_cat_js[$a_cat_id].push(new Array('$a_period_start','$a_period_end'));\n";
}

$mis_period_list = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY_PERIOD_LIST(-1);
$js.="var mis_cat_js = new Array();\n";
for($i=0; $i<sizeof($mis_period_list); $i++){
	list($a_cat_id,$a_period_id,$a_period_start,$a_period_end)=$mis_period_list[$i];
	$js.="if(mis_cat_js[$a_cat_id]==null) mis_cat_js[$a_cat_id] = new Array();\n";
	$js.="mis_cat_js[$a_cat_id].push(new Array('$a_period_start','$a_period_end'));\n";
}

# Generate Semester Checking JS
if($semester_mode==2)
{
	$sem_data = getSemesters();
	$sem_js.="var sem_js = new Array();\n";
	for($i=0;$i<sizeof($sem_data);$i++)
	{
		list($sem_name, $sem_type, $sem_start, $sem_end) = explode("::", $sem_data[$i]);
		$sem_js.="sem_js.push(new Array('$sem_start','$sem_end'));\n";
	}
}

# Remark
$remark = stripslashes(intranet_htmlspecialchars($remark));

# Start Layout
$linterface->LAYOUT_START();
?>

<script type="text/javascript" src = "<?=$PATH_WRT_ROOT?>templates/2007a/js/jslb_ajax.js" charset = "utf-8"></script>

<SCRIPT LANGUAGE=Javascript>
<!--
// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectConductCategory"]?> --",0);
<? foreach($good_cats as $k => $d) { ?>
	good_cat_select[<?=$k+1?>] = new Option("<?=str_replace("\"","\\\"",intranet_undo_htmlspecialchars($d['Name']))?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectConductCategory"]?> --",0);
<? foreach($mis_cats as $k => $d) { ?>
	mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace("\"","\\\"",intranet_undo_htmlspecialchars($d['Name']))?>",<?=$d['CategoryID']?>);
<?	} ?>

<?=$js?>
<?=$sem_js?>

var ClickID = '';
var callback_ajaxRemoveAttachment = {
	success: function ( o )
    {
		var fname = document.getElementById('fieldname').value;
	    var tmp_str = o.responseText;
	    checkOptionRemove(document.form1.elements[fname]);
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function ajaxRemoveAttachment(fname, sid, layername)
{
	ClickID = layername;
	obj = document.form1;
	obj.fieldname.value = fname;
	obj.task.value = "remove";
	obj.FolderLocation.value = "<?=$sessionTime?>_"+sid+"tmp";
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_attachment.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_ajaxRemoveAttachment);
}

var callback_ajaxCopyAllAttachment = {
	success: function ( o )
    {
		//var fname = document.getElementById('fieldname').value;
	    var tmp_str = o.responseText;
	    //checkOptionRemove(document.form1.elements[fname]);
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    //DisplayPosition();
	}
}

function ajaxCopyAllAttachment(fname, sid)
{
	//ClickID = layername;
	obj = document.form1;
	obj.fieldname.value = fname;
	obj.task.value = "copyAll";
	obj.FolderLocation.value = "<?=$sessionTime?>_"+sid;
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_attachment.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_ajaxCopyAllAttachment);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	
	return pos_value;
}

function DisplayPosition(){
	
	var posleft = getPosition(document.getElementById(ClickID),'offsetLeft') +10;
	var postop = getPosition(document.getElementById(ClickID),'offsetTop') +10;
	var divwidth = document.getElementById('ref_list').clientWidth;
	
	if(posleft+divwidth > document.body.clientWidth)
		document.getElementById('ref_list').style.right= "5px";
	else
		document.getElementById('ref_list').style.left= posleft+"px";
	
  	document.getElementById('ref_list').style.top= postop+"px";
  	document.getElementById('ref_list').style.visibility='visible';
}

function changeConductType(x) 
{
	var obj = document.form1;
	
	if(x == 0)
	{
		var cat_select = obj.elements["CatID"];
		var item_select = obj.elements["ItemID"];
		var record_type_select = obj.record_type[0].checked;
	}
	else
	{
		eval('var cat_select = obj.elements["CatID_'+ x +'"];');
		eval('var item_select = obj.elements["ItemID_'+ x +'"];');
		eval('var record_type_select = obj.record_type_'+ x +'[0].checked;');
	}
	
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectConductItem"]?> --",0);
	
	if(record_type_select)		// Good Conduct
	{
		cat_select.options.length = good_cat_select.length;
		for(i=0;i<good_cat_select.length;i++)
			cat_select.options[i]=new Option(good_cat_select[i].text,good_cat_select[i].value);	
		
		<?php if($sys_custom['eDiscipline']['PooiToMiddleSchool']){ ?>
			if(x==0){
				$("#decrement_score").html("<?=$Lang['eDiscipline']['Gain']?>");
				$("#tr_decrement_score").hide();
			}
			else{
				$("#decrement_score_"+x).html("<?=$Lang['eDiscipline']['Gain']?>");
				$("#tr_decrement_score_"+x).hide();
			}
		<?php } ?>
		
		<?php if($sys_custom['eDiscipline']['GMConductMark']){ ?>
			// Hide Conduct Mark
			if(x==0){
				$("#tr_conduct_score_change").hide();
			}
			else{
				$("#tr_conduct_score_change_"+x).hide();
			}
		<?php } ?>
	}
	else						// Misconduct
	{
		cat_select.options.length = mis_cat_select.length;
		
		for(i=0;i<mis_cat_select.length;i++)
			cat_select.options[i]=new Option(mis_cat_select[i].text,mis_cat_select[i].value);
		
		<?php if($sys_custom['eDiscipline']['PooiToMiddleSchool']){ ?>
			if(x==0){
				$("#decrement_score").html("<?=$Lang['eDiscipline']['Deduct']?>");
				$("#tr_decrement_score").show();
			}
			else{
				$("#decrement_score_"+x).html("<?=$Lang['eDiscipline']['Deduct']?>");
				$("#tr_decrement_score_"+x).show();
			}
		<?php } ?>
		
		<? if($sys_custom['eDiscipline']['GMConductMark']){ ?>
			// Show Conduct Mark
			if(x==0){
				$("#tr_conduct_score_change").show();
			}
			else{
				$("#tr_conduct_score_change_"+x).show();
			}
		<? } ?>
	}
}

function changeCat(x, value)
{
	if(x == 0)
	{
		var item_select = document.form1.elements["ItemID"];
	}		
	else
	{
		eval('var item_select = document.form1.elements["ItemID_'+ x +'"];');
	}
	
	var selectedCatID = value;
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectConductItem"]?> --",0);
	
	if (selectedCatID == '')
	{
	<?
		$curr_cat_id = "";
		$pos = 1;
		for ($i=0; $i<sizeof($items); $i++)
		{
	    	list($r_catID, $r_itemID, $r_itemName) = $items[$i];
	     	$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
	     	$r_itemName = str_replace('"', '\"', $r_itemName);
	     	$r_itemName = str_replace("'", "\'", $r_itemName);
	     	if ($r_catID != $curr_cat_id)
	     	{
	        	$pos = 1;
	?>
    }
	else if (selectedCatID == "<?=$r_catID?>")
	{
	    <?
	         	$curr_cat_id = $r_catID;
	    }
	    ?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>","<?=$r_itemID?>");
		<?	if($r_itemID == $ItemID) {	?>
			item_select.selectedIndex = <?=$pos-1?>;
		<? } ?>
		<?
	}
	?>
    }
}

function apply_all()
{
	obj = document.form1;
	var record_type_select = obj.record_type[0].checked;
	var cat_seletced = obj.elements["CatID"].options[obj.elements["CatID"].selectedIndex].value;
	var pic_length = obj.elements['PIC[]'].length;
	var att_length = obj.elements['Attachment[]'].length;
	
	if(record_type_select)
		record_type_value = 0;
	else
		record_type_value = 1;
	
	checkOptionAll(obj.elements['Attachment[]']);
	
	<?php 
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		echo 'var inc_de_conduct_score = $("#ScoreChange").val();'."\n";
	}
	if($sys_custom['eDiscipline']['GMConductMark']){
		echo 'var deduct_conduct_score = $("#GMConductScoreChange").val();'."\n";
	}
	?>
	
	<? foreach($student as $StudentID) {?>
		for(i=0; i<obj.elements['PIC_<?=$StudentID?>[]'].length; i++) {
			obj.elements['PIC_<?=$StudentID?>[]'].options[i] = null;
		}
		
		for(j=0; j<obj.elements['Attachment_<?=$StudentID?>[]'].length; j++) {
			obj.elements['Attachment_<?=$StudentID?>[]'].options[j] = null;
		}
		
		eval("obj.record_type_<?=$StudentID?>["+ record_type_value +"].checked = true;");
		changeConductType(<?=$StudentID?>);
		
		obj.elements["CatID_<?=$StudentID?>"].selectedIndex = obj.elements["CatID"].selectedIndex;
		changeCat(<?=$StudentID?>, cat_seletced)
		
		obj.elements["ItemID_<?=$StudentID?>"].selectedIndex = obj.elements["ItemID"].selectedIndex;
		obj.RecordDate_<?=$StudentID?>.value = obj.RecordDate.value;
		obj.remark_<?=$StudentID?>.value = obj.remark.value;
		
		<? if($sys_custom['eDiscipline']['HOY_Access_GM']) {?>
			obj.ApplyHOY_<?=$StudentID?>.checked = obj.ApplyHOY.checked;
		<? } ?>
		<? if($sys_custom['eDiscipline_GM_Times']) {?>
			obj.elements["GMCount_<?=$StudentID?>"].selectedIndex = obj.elements["GMCount"].selectedIndex;
		<? } ?>
		<? if($sys_custom['eDisciplinev12_FollowUp']) {?>
			obj.elements["ActionID_<?=$StudentID?>"].selectedIndex = obj.elements["ActionID"].selectedIndex;
		<? } ?>
		
		for(i=0; i<pic_length; i++) {
			obj.elements['PIC_<?=$StudentID?>[]'].options[i] = new Option(obj.elements['PIC[]'].options[i].text,obj.elements['PIC[]'].options[i].value);
		}
		
		ajaxCopyAllAttachment('Attachment[]', <?=$StudentID?>);
		
		for(j=0; j<att_length; j++) {
			//alert(obj.elements['Attachment[]'].options[j].text+'/'+obj.elements['Attachment[]'].options[j].value);
			if(obj.elements['Attachment[]'].options[j]!=undefined && trim(obj.elements['Attachment[]'].options[j].text)!="") {
				obj.elements['Attachment_<?=$StudentID?>[]'].options[j] = new Option(obj.elements['Attachment[]'].options[j].text,obj.elements['Attachment[]'].options[j].value);
			}
		}
		
		<? if($semester_mode==1) {?>
			obj.elements["semester_<?=$StudentID?>"].selectedIndex = obj.elements["semester"].selectedIndex;
		<? } ?>
		
		<?php
		if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
			echo '$("#ScoreChange_'.$StudentID.'").val(inc_de_conduct_score);'."\n";
		}
		// [2016-0808-1526-52240]
		if($sys_custom['eDiscipline']['GMConductMark']){
			echo '$("#GMConductScoreChange_'.$StudentID.'").val(deduct_conduct_score);'."\n";
		}
		?>
	<? } ?>
}

function ltrim(instr){
	return instr.replace(/^[\s]*/gi,"");
}

function rtrim(instr){
	return instr.replace(/[\s]*$/gi,"");
}

function trim(instr){
	instr = ltrim(instr);
	instr = rtrim(instr);
	return instr;
}

function CompareDates(date_string1, date_string2, EqualDateAllow)
{
	var yr1  = parseInt(date_string1.substring(0,4),10);
	var mon1 = parseInt(date_string1.substring(5,7),10);
	var dt1  = parseInt(date_string1.substring(8,10),10);
	var yr2  = parseInt(date_string2.substring(0,4),10);
	var mon2 = parseInt(date_string2.substring(5,7),10);
	var dt2  = parseInt(date_string2.substring(8,10),10);
	var date1 = new Date(yr1, mon1-1, dt1);
	var date2 = new Date(yr2, mon2-1, dt2);
	
	if(EqualDateAllow == 0)
	{
		if(date2 < date1) {
			return true;
		}
		else{
			return false;
		}
	}
	else{
		if(date2 <= date1) {
			return true;
		}
		else{
			return false;
		}
	}
}

function check_form()
{
	obj = document.form1;

	<? foreach($student as $StudentID) {?>
	var pic_length = obj.elements['PIC_<?=$StudentID?>[]'].length;

	// Record Date
	if(!check_date(obj.RecordDate_<?=$StudentID?>,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
	{
		return false;
	}
	else
	{
		<? if($EnableMaxRecordDaysChecking == 1) { ?>
			// Check Record Date is allow or not
			var DateMinLimit = "<?=$DateMinLimit;?>";
			var DateMaxLimit = "<?=$DateMaxLimit;?>";
			if(!CompareDates(obj.RecordDate_<?=$StudentID?>.value, DateMinLimit, 1)) {
				alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
				return false;
			}
			else {
				if(!CompareDates(DateMaxLimit, obj.RecordDate_<?=$StudentID?>.value, 1)) {
					alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
					return false;
				}
			}
		<? }
		else { ?>
			// Check Record Date is allow or not
			var TodayDate = "<?=date("Y-m-d")?>";
			if(!CompareDates(TodayDate, obj.RecordDate_<?=$StudentID?>.value, 1)) {
				alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
				return false;
			}
		<? } ?>
	}
	
	<? if($sys_custom['eDiscipline_GM_Times']) {?>
		/*
		if(obj.GMCount_<?=$StudentID?>.selectedIndex==0) {
			alert("<?=$i_alert_pleaseselect." ".$Lang['eDiscipline']['ReceiveTimes']?>");
			return false;
		}
		*/
	<?}?>
	
	<? if($semester_mode==2) {?>
		if(!check_semester(obj.RecordDate_<?=$StudentID?>)) return false;
	<? } ?>
	
	// Category / Items
    if(obj.elements["CatID_<?=$StudentID?>"].value == 0 || obj.elements["ItemID_<?=$StudentID?>"].value == 0)
    {
        alert('<?=$i_Discipline_Ssytem_Category_Item_Not_Selected?>');
        return false;
    }
   	
    if(!check_period(obj.record_type_<?=$StudentID?>, obj.RecordDate_<?=$StudentID?>,obj.CatID_<?=$StudentID?>)) return false;
    
	if(pic_length==0) {
		alert("<?=$i_alert_pleaseselect.' '.$i_Discipline_System_Contact_PIC?>");	
		return false;
	}
	<? } ?>
	
	<? foreach($student as $StudentID) {?>
		checkOptionAll(obj.elements['PIC_<?=$StudentID?>[]']);
		checkOptionAll(obj.elements['Attachment_<?=$StudentID?>[]']);
	<? } ?>
	
	$("#submitBtn").attr("disabled", true);
}

function check_period(objRecordType,objDate,objCat)
{
	cat_id = objCat.options[objCat.selectedIndex].value;
	
	<?php if($sys_custom['eDiscipline']['SkipAllGMPeriodChecking']) { ?>
		return true;
	<?php } ?>
	
	if(objRecordType[0].checked){
		periods = good_cat_js[cat_id];
	}
	else{
		<?php if($sys_custom['eDiscipline']['PooiToMiddleSchool'] || $sys_custom['eDiscipline']['SkipGMPeriodChecking']){ // cust no need check period for misconduct ?>
				return true;
		<?php } ?> 
		periods = mis_cat_js[cat_id];
	}
	
	valid = false;
	if(periods!=null){
		for(j=0;j<periods.length;j++){
			dateStart = periods[j][0];
			dateEnd = periods[j][1];
			
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	if(!valid){
		if(objRecordType[0].checked){
			alert('<?=$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning2']?>');
		}
		else{
			alert('<?=$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning']?>');
		}
		
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		return false;
	}
	else{
		objDate.className ='textboxnum';
	}
	
	return true;
}

function check_semester(objDate)
{
	semester_periods = sem_js;
	
	valid = false;
	if(semester_periods!=null)
	{
		for(j=0;j<semester_periods.length;j++){
			dateStart = semester_periods[j][0];
			dateEnd = semester_periods[j][1];
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	
	if(!valid)
	{
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		alert('<?=$eDiscipline["NoSemesterSettings"]?>');
		return false;
	}
	
	return true;
}

function changeFollowUp(student_id, itemid)
{
<? if($sys_custom['eDisciplinev12_FollowUp']) {?>
	obj = document.form1;
	if(student_id>0)
		eval('var cat_select = obj.elements["CatID_'+ student_id +'"];');
	else
		var cat_select = obj.elements["CatID"];
	
	var category_id = cat_select.value; 
	
	$.post(
		'ajax_get_followup.php', 
		{
			CategoryID: category_id,
			ItemID: itemid
		}, 
		function (action_id){
			if(action_id)
			{
				if(student_id>0)
				{
					eval('obj.elements["ActionID_'+ student_id +'"].value=action_id;');
				}
				else
				{
					obj.elements["ActionID"].value=action_id;
				}
			}
			else
			{
				if(student_id>0)
				{
					eval('obj.elements["ActionID_'+ student_id +'"].selectedIndex=0;');
				}
				else
				{
					obj.elements["ActionID"].selectedIndex=0;
				}
			}
	});
<? } ?>
}
//-->
</SCRIPT>

<form name="form1" method="POST" action="new2_update.php" onsubmit="return check_form();">

<br />
<div id="ref_list"></div>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td>
	<? if(sizeof($student) > 1) {?>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
		<tr>
			<td colspan="2" valign="top" nowrap="nowrap"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><?=$linterface->GET_NAVIGATION2($eDiscipline["GlobalSettingsToAllStudent"])?></td>
					<td align="right"><?= $linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "apply_all();")?></td>
				</tr>
			</table></td>
		</tr>
		<tr valign="top">
			<td nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Type"]?><span class="tabletextrequire">*</span></td>
			<td width="80%">
				<input name="record_type" type="radio" id="conduct_type_good" value="1" onClick="changeConductType(0);" > <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <label for="conduct_type_good"><?=$eDiscipline['Setting_GoodConduct']?></label>
				<input name="record_type" type="radio" id="conduct_type_mis" value="-1" onClick="changeConductType(0);" checked> <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <label for="conduct_type_mis"><?=$eDiscipline['Setting_Misconduct']?></label>
			</td>
		</tr>
		<tr valign="top">
			<td nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["Category_Item"]?><span class="tabletextrequire">*</span></td>
			<td>
				<select name="CatID" id="CatID" onChange="changeCat(0, this.value);"></select> 
				- 
				<SELECT name="ItemID" id="ItemID" onChange='changeFollowUp(0, this.value);'>
					<OPTION value="0" selected>-- <?=$eDiscipline["SelectConductItem"]?> --</OPTION>
				</SELECT>
			</td>
		</tr>
		
		<?php if($sys_custom['eDiscipline']['PooiToMiddleSchool'])
		{ 
			$select_conduct_mark = '<SELECT name="ScoreChange" id="ScoreChange">'."\n";
			$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
			$increment_max = $ldiscipline->MisconductMaxScoreDeduction != '' ? $ldiscipline->MisconductMaxScoreDeduction : 3;
			
			$i = 0;
			$loopConductMark = true;
			while($loopConductMark) {
				$select_conduct_mark .= '<OPTION value="'.$i.'">'.$i.'</OPTION>'."\n";    
				$i = (float)(string)($i + $conductMarkInterval);
				if(floatcmp($i ,">" ,$increment_max)) {
					$loopConductMark = false;
				}
			}
			$select_conduct_mark .= '</SELECT>'."\n";
		?>
		
		<tr id="tr_decrement_score"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['Score']?></td>
			<td><span id="decrement_score"><?=$Lang['eDiscipline']['Deduct']?></span> <?=$select_conduct_mark?><td>
		</tr>
		<?php } ?>
		
		<? if($sys_custom['eDiscipline']['GMConductMark'])
		{
			$i = 0;
			$increment_max = $sys_custom['eDiscipline']['HYK_MaxGMConductMark'];
			$conductMarkInterval = 1;
			$loopConductMark = true;
			
			// Conduct Mark drop down list
			$select_conduct_score = '<SELECT name="GMConductScoreChange" id="GMConductScoreChange">'."\n";
			while($loopConductMark) {
				$select_conduct_score .= '<OPTION value="'.$i.'">'.$i.'</OPTION>'."\n";    
				$i = (float)(string)($i + $conductMarkInterval);
				if(floatcmp($i ,">" ,$increment_max)) {
					$loopConductMark = false;
				}
			}
			$select_conduct_score .= '</SELECT>'."\n";
		?>
		
		<tr id="tr_conduct_score_change"> 
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['ConductMark']?></td>
			<td><span id="deduct_conduct_score_span"><?=$Lang['eDiscipline']['Deduct']?></span> <?=$select_conduct_score?><td>
		</tr>
		<? } ?>
		
		<? if($sys_custom['eDiscipline_GM_Times']) { ?>
			<tr valign="top">
				<td nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['ReceiveTimes']?><span class="tabletextrequire">*</span></td>
				<td>
				<?=$GMCountSelection = $ldiscipline->Get_GM_Count_Selection($GMCount);?>
				</td>
			</tr>
		<? } ?>
		
		<?=$select_sem_html?>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['EventDate']?><span class="tabletextrequire">* </span></td>
			<td> <?=$linterface->GET_DATE_PICKER("RecordDate",($RecordDate ? $RecordDate : date('Y-m-d')))?>
			<?/*= $linterface->GET_DATE_FIELD("theRecordDate", "form1", "RecordDate", ($RecordDate ? $RecordDate : date('Y-m-d')), 1)*/?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Contact_PIC?><span class="tabletextrequire">* </span></td>
			<td>		
				<table>
					<tr>
						<td><?=$PIC_selected?></td>
						<td valign="bottom">
						<? if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) { ?>
							<?=$linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',11)");?><br />
							<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC[]'])");?>
						<? } ?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?></td>
			<td><?=$attachment?></td>
		</tr>
		
		<? if($sys_custom['eDiscipline']['HOY_Access_GM']) { ?>
			<tr>
				<td valign="top" class="formfieldtitle"><?=$Lang['eDiscipline']['GM_HOYRemarks']?></td>
				<td>
					<?=$linterface->GET_TEXTAREA("remark", $remark);?><br/>
					<input type='checkbox' name='ApplyHOY' id='ApplyHOY' value='1'><label for='ApplyHOY'><?=$Lang['eDiscipline']['DetailsReferToHOY']?></label>
				</td>
			</tr>
		<? }
		else { ?>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
				<td><?=$linterface->GET_TEXTAREA("remark", $remark);?></td>
			</tr>
		<? } ?>
		
		<? ##### Follow-up Action
		if($sys_custom['eDisciplinev12_FollowUp']) { ?>
			<tr>
				<td valign='top' nowrap='nowrap' class='formfieldtitle'><?=$Lang['eDiscipline']['FollowUp']?></td>
				<td><?=$ldiscipline->getFollowUpSelection(1)?></td>
			</tr>
		<? } ?>
	</table>
	<? } ?>
	
	<?=$selected_student_tables?>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<div id="btnDiv">
					<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "", "submitBtn")?>&nbsp;
					<?= $linterface->GET_ACTION_BTN($button_back, "button", "this.form.action='new1.php';this.form.submit();")?>&nbsp;
					<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
				</div>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<!-- Step 1 data //-->
<? 

if(is_array($student))
{
foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />	
<? }
} ?>

<input type="hidden" id="FolderLocation" name="FolderLocation" value="<?=$sessionTime?>">
<input type="hidden" id="ClickID" name="ClickID" value="">
<input type="hidden" id="fieldname" name="fieldname" value="">
<input type="hidden" id="task" name="task" value="">
<input type="hidden" id="sessionTime" name="sessionTime" value="<?=$sessionTime?>">

</form>

<script language="javascript">
<!--
<? if(sizeof($student)>1) {?>
changeConductType(0);
<? } ?>
<? foreach($student as $StudentID) {?>
	changeConductType(<?=$StudentID?>);
	<? if(${"CatID_".$StudentID}!='')
	{
		echo "document.getElementById('CatID_".$StudentID."').value = ${"CatID_".$StudentID};\n";
	}
	if(${"ItemID_".$StudentID}!='')
	{
		echo "changeCat(".$StudentID.", ".${"CatID_".$StudentID}.");\n";
		echo "document.getElementById('ItemID_".$StudentID."').value = ${"ItemID_".$StudentID};\n";	
	}
	if(${"semester_".$StudentID}!='')
	{
		echo "document.getElementById('semester_".$StudentID."').value = '${"semester_".$StudentID}';\n";	
	}
	if(${"remark_".$StudentID}!='')
	{
		echo "document.getElementById('remark_".$StudentID."').value = '${"remark_".$StudentID}';\n";	
	}
} ?>
//-->
</script>

<?
if(sizeof($student)>1) {
	print $linterface->FOCUS_ON_LOAD("form1.record_type[0]");
}

$linterface->LAYOUT_STOP();
intranet_closedb();
?>