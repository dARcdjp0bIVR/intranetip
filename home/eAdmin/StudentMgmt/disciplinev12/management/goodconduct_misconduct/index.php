<?php
# modifying by : Bill

############ Change Log Start #############################
#
#   Date    :   2019-07-16  Bill    [2019-0712-1439-39207]
#               fixed Year Selection cannot select "Whole Year"
#
#   Date    :   2019-05-01  Bill
#               prevent Cross-site Scripting
#
#	Date	:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#				Support HKUGA Cust - only allow HOY and Class Teacher to view Conduct Records ($sys_custom['eDiscipline']['HOY_Access_GM'])
#
#	Date	:	2017-01-09 (Bill)	[2016-0823-1255-04240]
#				set location of Homework Deletion flag, to display messages related to Not Submitted misconduct ($sys_custom['eDiscipline']['DisplayHomeworkRelatedMsg'])
#
#	Date:		2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added remarks for prefix style of deleted teacher account
#
#	Date	:	2016-04-06 (Bill)	[2015-0807-1048-01073]
#				Cust: redirect to Personal Report when clicking student name ($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport'])
#
#	Date	:	2015-07-31 (Bill)	[DM#2879]
#				fixed cannot search ' and "
#
#	Date	:	2015-07-28 (Evan)
#				fix bug of search bar with extra slashes
#
#	Date	:	2015-07-17 (Evan)
#				Update style of search bar
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				display "Send push message" in column "Action" 
#
#	Date	:	2015-04-29 (Bill)	[2015-0318-1624-34073]
#				add content tool to add notice for multiple GM records
#
#	Date	:	2014-09-19 (YatWoon)
#				Fixed: incorrect search result if same ItemID but diff CategoryID (homework and others) > $check_cat [Case#P68212]
#				deploy: IPv10.1
#
#	Date	:	2014-05-22 (Carlos)
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added a [Score] column
# 
#	Date	:	2012-04-03 (Henry Chow)
#				remove sorting of PIC column
# 
#	Date	:	2011-03-24 (Henry Chow)
#				PIC selection menu only display the staff name of selected semester
# 
#	Date	:	2010-12-29 Henry Chow
#				add "Date Range" option when export
# 
#	Date	:	2010-09-24 Henry Chow
#				Display "Form" in class filter
#
#	Date	:	2010-05-10	YatWoon
#				Display approved by, rejected by.... data in the index page
#
#	Date	: 	2010-05-07 YatWoon
#				According to the setting $PIC_SelectionDefaultOwn to check PIC default selected value
#
#	Date:	2010-02-19	Henry
#			update javascript, allow filter by pulldown and search box at the same time
#
#	Date:	2010-02-18	YatWoon
#			update query for search PIC, use "%,4,%" rather than "%4%" 
#
#	Date:	2010-02-12	YatWoon
#			combine the search with filter option
#
############ Change Log End #############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

//$sys_custom['eDiscipline']['PooiToMiddleSchool'] = false;
$sortEventDate = ($sys_custom['eDiscipline_GM_Times']) ? 5 : 4;			# Event Date is the 4th SQL column
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$sortEventDate += 1;
}

// [2014-1216-1347-28164]
$canSendPushMsg = $plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'];

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}
if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
}
else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}
if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
}
else if (!isset($field) || $ck_approval_page_field == "")
{
	//$field = $ck_right_page_field;
	$field = $sortEventDate;
}

# Temp assign memory of this page
ini_set("memory_limit", "150M");

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

### Handle SQL Injection + XSS [START]
if($SchoolYear != '' && $SchoolYear != '0') {
    $GMSchoolYearArr = $ldiscipline->getGMSchoolYear();
    $GMSchoolYearArr = Get_Array_By_Key((array)$GMSchoolYearArr, 'AcademicYearID');
    if(!in_array($SchoolYear, (array)$GMSchoolYearArr)) {
        $SchoolYear = '0';
    }
}
if($semester != '' && $semester != 'WholeYear') {
    $semester = IntegerSafe($semester);
}
if ($targetClass != '' && $targetClass != '0') {
    if(is_numeric($targetClass)) {
        $sql = " SELECT YearID FROM YEAR ";
        $YearIDArr = $ldiscipline->returnVector($sql);
        if(!in_array($targetClass, (array)$YearIDArr)) {
            $targetClass = '0';
        }
    }
    else {
        $sql = " SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."' ";
        $YearClassIDArr = $ldiscipline->returnVector($sql);
        
        $check_class_id = substr($targetClass, 2);
        if(!in_array($check_class_id, (array)$YearClassIDArr)) {
            $targetClass = '0';
        }
    }
}
$pic = IntegerSafe($pic);
$s = cleanCrossSiteScriptingCode($s);

$RecordType = IntegerSafe($RecordType);
if(isset($approved)) {
    $approved = IntegerSafe($approved);
}
if(isset($rejected)) {
    $rejected = IntegerSafe($rejected);
}
if(isset($waived)) {
    $waived = IntegerSafe($waived);
}
$newleaf = IntegerSafe($newleaf);
if(isset($fromPPC)) {
    $fromPPC = IntegerSafe($fromPPC);
}
### Handle SQL Injection + XSS [END]

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

# Check access right (View page)
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
if($newleaf==1 && (!$ldiscipline->use_newleaf || !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$currentYearID = ($SchoolYear=="") ? Get_Current_Academic_Year_ID() : $SchoolYear;

$choiceType = ($RecordType!="1" && $RecordType != "-1") ? $i_Discipline_System_Award_Punishment_All_Records : "<a href='javascript:reloadForm(0)'>".$i_Discipline_System_Award_Punishment_All_Records."</a>";
$choiceType .= " | ";
$choiceType .= ($RecordType=="1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_GoodConduct}" : "<a href='javascript:reloadForm(1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_GoodConduct}</a>";
$choiceType .= " | ";
$choiceType .= ($RecordType=="-1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_Misconduct}" : "<a href='javascript:reloadForm(-1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'> {$i_Discipline_Misconduct}</a>";

######### Conditions #########
$conds = "";

# Class Condition #
if (!$sys_custom['eDisciplinev12_Munsang_NewLeafScheme'] && $targetClass != '' && $targetClass!="0")
{
    //$conds .= " AND yc.YearClassID=$targetClass";
    if(is_numeric($targetClass)) {
    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
    	$temp = $ldiscipline->returnVector($sql);
    	$conds .= (sizeof($temp)>0) ? " AND yc.YearClassID IN (".implode(',', $temp).")" : "";
	}
    else {
		$conds .= " AND yc.YearClassID='".substr($targetClass,2)."'";
    }
}
else if($newleaf!=1 && $targetClass!="" && $targetClass!="0") {
    if(is_numeric($targetClass)) {
    	$sql = "SELECT YearClassID FROM YEAR_CLASS WHERE YearID='$targetClass'";
    	$temp = $ldiscipline->returnVector($sql);
    	$conds .= (sizeof($temp)>0) ? " AND yc.YearClassID IN (".implode(',', $temp).")" : "";
	}
	else {
		$conds .= " AND yc.YearClassID='".substr($targetClass,2)."'";
	}
}

# RecordType #
if ($RecordType != '' && $RecordType != 0) {
    $conds .= " AND a.RecordType = $RecordType";
}
else {
	$RecordType = 0;
}

# School Year Menu #
$yearSelected = 0;
$currentYearInfo = Get_Current_Academic_Year_ID();
$SchoolYear = ($SchoolYear == '') ? $currentYearInfo : $SchoolYear;

$yearName = $ldiscipline->getAcademicYearNameByYearID($SchoolYear);
$yearArr = $ldiscipline->getGMSchoolYear($SchoolYear);

$selectSchoolYear = "<select name='SchoolYear' onChange='document.form1.semester.value=\"\";reloadForm($MeritType)'>";
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
for($i=0; $i<sizeof($yearArr); $i++) {
	$selectSchoolYear .= "<option value='".$yearArr[$i][0]."'";
	if($SchoolYear==$yearArr[$i][0]) {
		$selectSchoolYear .= " SELECTED";
	}
	if(Get_Current_Academic_Year_ID()==$yearArr[$i][0]) {
		$yearSelected = 1;
	}
	$selectSchoolYear .= ">".$yearArr[$i][1]."</option>";
}
if($yearSelected==0) {
	$selectSchoolYear .= "<option value='".Get_Current_Academic_Year_ID()."'";
	$selectSchoolYear .= (Get_Current_Academic_Year_ID()==$SchoolYear) ? " selected" : "";
	$selectSchoolYear .= ">".$ldiscipline->getAcademicYearNameByYearID($currentYearInfo)."</option>";
}
$selectSchoolYear .= "</select>";

if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID = $SchoolYear";	
}
//$extraConds = " AND a.AcademicYearID = yc.AcademicYearID";

if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme'] && $ldiscipline->use_newleaf && $newleaf==1) {
	$typeSelection .= "<SELECT name='Type' id='Type' onChange='reloadForm($RecordType)'>";
	$typeSelection .= "<option>$i_general_all</option>";
	$typeSelection .= "<option value='A' ".(($Type=='A') ? "selected" : "").">".$Lang['eDiscipline']['NewLeafTypeA']."</option>";
	$typeSelection .= "<option value='B' ".(($Type=='B') ? "selected" : "").">".$Lang['eDiscipline']['NewLeafTypeB']."</option>";
	$typeSelection .= "</SELECT>";
}

# Semester Menu #
$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID=$currentYearID ORDER BY TermStart";
$semResult = $ldiscipline->returnArray($sql, 3);

//echo $sql;
$SemesterMenu = "<select name='semester' onChange='reloadForm($RecordType)'>";
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for($i=0; $i<sizeof($semResult); $i++) {
	list($id, $nameEN, $nameB5) = $semResult[$i];
	$semName = Get_Lang_Selection($nameB5, $nameEN);
	$SemesterMenu .= "<option value='$id'";
	$SemesterMenu .= ($semester==$id) ? " selected" : "";
	$SemesterMenu .= ">$semName</option>";
}
$SemesterMenu .= "</select>";

# Semester Condition #
if($semester != '' && $semester != 'WholeYear') {
	$conds .= " AND (a.YearTermID = $semester)";
}

# Class Menu #
if($ldiscipline->use_newleaf && $sys_custom['eDisciplinev12_Munsang_NewLeafScheme'] && $newleaf==1) {
	if($targetClass=="") $targetClass = 2;
	$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm($RecordType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);
	$stdAry = $ldiscipline->storeStudent('0', $targetClass, $SchoolYear);
}
else {
	//$select_class = $ldiscipline->getSelectClass("name=\"targetClass\" onChange=\"reloadForm($RecordType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);
	$select_class = $lclass->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"reloadForm($RecordType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);
}

# PIC #
if($semester != '' && $semester != 'WholeYear') $thisSemester = $semester; 
$PICArray = $ldiscipline->getPICInArray("DISCIPLINE_ACCU_RECORD", $currentYearID, $thisSemester);
if(sizeof($PICArray)==0) {
	$PICArray = array();
}
if($ldiscipline->PIC_SelectionDefaultOwn)
{
	//if(sizeof($_POST)==0 && sizeof($_GET)==0 && !isset($_POST['pic'])) {		# default display own PIC records
	if(sizeof($_POST)==0 && !isset($_POST['pic'])) {
		$flag = 0;
		for($i=0; $i<sizeof($PICArray); $i++) {
			if($PICArray[$i][0]==$UserID) {
				$pic = $UserID;		
				$flag = 1;
				break;
			}
		}
		/*
		if($flag == 0) {
			$name = $ldiscipline->getUserNameByID($UserID);
			array_push($PICArray, array($UserID, $name[0]));	
			$pic = $UserID;
		}
		*/
	}
}

$picSelection = getSelectByArray($PICArray," name=\"pic\" onChange=\"reloadForm($RecordType)\"",$pic,0,0,"".$i_Discipline_Detention_All_Teachers."");

if($pic != "") {
	$conds .= " AND concat(',',a.PICID,',') LIKE '%,$pic,%'";
	//$conds .= " AND (a.PICID='$pic' OR a.PICID LIKE '$pic,%' OR a.PICID LIKE '%,$pic' OR a.PICID LIKE '%,$pic,%')";
}

// [2017-0403-1552-54240] HKUGA Handling
// 1. HOY Members: View all GM
// 2. Class Teacher: View students' GM only
if($sys_custom['eDiscipline']['HOY_Access_GM'] && !$ldiscipline->IS_HOY_GROUP_MEMBER())
{
	$ClassStudentAry = $ldiscipline->getStudentListByClassTeacherID("", $UserID, $currentYearID);
	$ClassStudentAry = array_unique($ClassStudentAry);
	if(sizeof($ClassStudentAry) > 0) {
		$ClassStudentStr = implode("', '", $ClassStudentAry);
	}
	else { 
		$ClassStudentStr = "";
	}
	$conds .= " AND a.StudentID IN ('".$ClassStudentStr."') ";
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1 || (!isset($waitApproval) && !isset($fromPPC) && !isset($fromOverview))) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;
}

# Approved #  	// released record also be approved
if($approved == 1 || (!isset($waitApproval) && !isset($fromPPC) && !isset($fromOverview))) {
	$approvedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED;
}

# Rejected #
if($rejected == 1 || (!isset($waitApproval) && !isset($fromPPC) && !isset($fromOverview))) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED;
}

# Waived #
if($waived == 1 || (!isset($waitApproval) && !isset($fromPPC) && !isset($fromOverview))) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED;
}

$conds2 .= ($conds2 != "") ? ")" : "";

##############

$i_Merit_MinorCredit = addslashes($i_Merit_MinorCredit);
$i_Merit_MajorCredit = addslashes($i_Merit_MajorCredit);
$i_Merit_SuperCredit = addslashes($i_Merit_SuperCredit);
$i_Merit_UltraCredit = addslashes($i_Merit_UltraCredit);

# $access_level = $ldiscipline->access_level;
$li = new libdbtable2007($field, $order, $pageNo);

$const_status_pending = 0;

$gdConductImg = "<img src=\'{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif\' width=20 height=20 border=0 align=absmiddle title=\'".$i_Discipline_GoodConduct."\'>";
$misconductImg = "<img src=\'{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif\' width=20 height=20 border=0 align=absmiddle title=\'".$i_Discipline_Misconduct."\'>";

$student_namefield = getNamefieldByLang("b.");
$PICID_namefield = getNameFieldWithLoginByLang("d.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$sbjName = ($intranet_session_language=="en") ? "SUB.EN_DES" : "SUB.CH_DES";

$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();

// [2014-1216-1347-28164]
$pushMsgDisplay = $canSendPushMsg? " ,a.PushMessageID" : "";

// [2016-0823-1255-04240]
$homeworkMsgDisplay = $sys_custom['eDiscipline']['DisplayHomeworkRelatedMsg']? " , a.isWaitingForDeletion, a.UpgradedRecordID " : "";

if($fromPPC==1)
{
	$result = $ldiscipline->retrieveGMRecordFromPPC();
	$GMRecordIDFromPPC = implode(',', $result);

	$sql = "SELECT
				CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
				$student_namefield as std_name,
				IF(a.RecordType=1,'$gdConductImg','$misconductImg') as conductImg,";
	if($use_intranet_homework) {
		$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
	}
	else {
		$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
	}
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']) {
		$sql .= " IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange, ";
	}
	if($sys_custom['eDiscipline_GM_Times']) {
		$sql .= " CONCAT(IF(round(a.GMCount)=a.GMCount,floor(a.GMCount),round(a.GMCount,1))) as GMCount,";
	}
	$sql .= "	LEFT(a.RecordDate,10) as RecordDate,
				PICID,
				a.NoticeID as action,
				f.CategoryID as WaiveDay,
				CONCAT('<font title=\'',a.DateModified,'\'>',LEFT(a.DateModified,10),'</font>') as DateModified,
				CONCAT(
					IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$pendingImg',''),
					IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$rejectImg',''),
					IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'$approvedImg',''),
					IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$waivedImg','')
				) as status,
				CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
				a.RecordStatus,
				a.RecordID,
				b.UserID,
				a.Remark,
				LEFT(DATE_ADD(a.RecordDate, INTERVAL f.WaiveDay DAY),10) as waivePassDate
				$pushMsgDisplay
				$homeworkMsgDisplay
			FROM DISCIPLINE_ACCU_RECORD as a
				INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
				LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD as g ON (a.CategoryID=g.CategoryID)
				LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as h ON (f.PeriodID=h.PeriodID)
				LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
				LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID) ";
	if($use_intranet_homework){
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID)";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
	}
	$sql .=	($GMRecordIDFromPPC) ? " WHERE a.RecordID IN ($GMRecordIDFromPPC)" : " WHERE a.RecordDate='9999-99-99'";
	$sql .= " AND yc.AcademicYearID=$currentYearInfo AND b.RecordStatus IN (0,1,2)";
	$sql .= $conds;
	$sql .= $extraConds;
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
}
else if($ldiscipline->use_newleaf && $newleaf==1)	# show "new leaf" only
{
	if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) {
		if(sizeof($stdAry)>0)
			$special = " AND AR.StudentID IN (".implode(',',$stdAry).")";
		
		$result = $ldiscipline->specialNewLeafCount($SchoolYear, $Type, 0, $special);
		$waitForNewLeafID = implode(',', array_keys($result));
	}
	else {
		$result = $ldiscipline->GMWaitForNewLeafCount();
		$waitForNewLeafID = implode(',', $result);		
	}
		
	$sql = "SELECT
				CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
				$student_namefield as std_name,
				IF(a.RecordType=1,'$gdConductImg','$misconductImg') as conductImg,";
	if($use_intranet_homework) {
		$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name))) as itemName,";
	}
	else {
		$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
	}
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']) {
		$sql .= " IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange, ";
	}
	if($sys_custom['eDiscipline_GM_Times']) {
		$sql .= " CONCAT(IF(round(a.GMCount)=a.GMCount,floor(a.GMCount),round(a.GMCount,1))) as GMCount,";
	}
	$sql .= "	LEFT(a.RecordDate,10) as RecordDate,
				PICID,
				a.NoticeID as action,
				f.CategoryID as WaiveDay,
				CONCAT('<font title=\'',a.DateModified,'\'>',LEFT(a.DateModified,10),'</font>') as DateModified,
				CONCAT(
					IF(a.RecordStatus=".DISCIPLINE_STATUS_PENDING.",'$pendingImg',''),
					IF(a.RecordStatus=".DISCIPLINE_STATUS_REJECTED.",'$rejectImg',''),
					IF(a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.",'$approvedImg',''),
					IF(a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.",'$waivedImg','')
				) as status,
				CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
				a.RecordStatus,
				a.RecordID,
				b.UserID,
				a.Remark,";
	if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) {
		$sql .= " LEFT(a.RecordDate, 10) as waivePassDate";
	}
	else {
		$sql .= " LEFT(DATE_ADD(a.RecordDate, INTERVAL f.WaiveDay DAY),10) as waivePassDate";
	}
	$sql .= "	$pushMsgDisplay
				$homeworkMsgDisplay
				FROM DISCIPLINE_ACCU_RECORD as a
					INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
					LEFT OUTER JOIN DISCIPLINE_NEW_LEAF_SCHEME as f ON (f.CategoryID = a.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_PERIOD as g ON (a.CategoryID=g.CategoryID)
					LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as h ON (f.PeriodID=h.PeriodID)
					LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
					LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID) ";
	if($use_intranet_homework){
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID)";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (a.ItemID = SUB.RecordID)";
	}
	$sql .=	($waitForNewLeafID) ? " WHERE a.RecordID IN ($waitForNewLeafID)" : " WHERE a.RecordDate='9999-99-99'";
	$sql .= " AND yc.AcademicYearID=$currentYearInfo AND b.RecordStatus IN (0,1,2)";
	$sql .= $conds;
	$sql .= $extraConds;
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
} 
else
{
	# filter by option & box
	// case 1: quote in string directly stored
	$s = addslashes(trim($s));
	// case 2: quote in string converted to special characters before storage
	$s2 = intranet_htmlspecialchars($s);
	
	$sql = "SELECT
				CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
				$student_namefield as std_name,
				IF(a.RecordType=1,'$gdConductImg','$misconductImg') as conductImg,";
	if($use_intranet_homework) {
		$sql .= " IF(a.CategoryID = 2, CONCAT(c.Name,' - ',$sbjName), IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)))  as itemName,";
		$check_cat = " and a.CategoryID !=2 ";
	}
	else {
		$sql .= " IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,";
		$check_cat = "";
	}
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']) {
		$sql .= " IF(a.ScoreChange=0,'".$Lang['General']['EmptySymbol']."',a.RecordType*a.ScoreChange) as ScoreChange, ";
	}
	if($sys_custom['eDiscipline_GM_Times'])	{
		$sql .= " CONCAT(IF(round(a.GMCount)=a.GMCount,floor(a.GMCount),round(a.GMCount,1))) as GMCount,";
	}
	$sql .= "	LEFT(a.RecordDate,10) as RecordDate,
				PICID,
				a.NoticeID as action,
				a.CategoryID as WaiveDay,
				CONCAT('<font title=\'',a.DateModified,'\'>',LEFT(a.DateModified,10),'</font>') as DateModified,
				a.RecordStatus as status,
				CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
				a.RecordStatus,
				a.RecordID,
				b.UserID,
				a.Remark,
				a.ApprovedBy,
				a.ApprovedDate,
				a.WaivedBy,
				a.WaivedDate,
				a.RejectedBy,
				a.RejectedDate
				$pushMsgDisplay
				$homeworkMsgDisplay
			FROM DISCIPLINE_ACCU_RECORD as a
				INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID AND b.RecordStatus IN (0,1,2))
				INNER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID $check_cat)
				INNER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
				INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID AND yc.AcademicYearID=$currentYearInfo) ";
	if($use_intranet_homework){
		//$sql .= " LEFT OUTER JOIN INTRANET_SUBJECT as e ON (a.ItemID = e.SubjectID) ";
		$sql .= " LEFT OUTER JOIN ASSESSMENT_SUBJECT as SUB ON (SUB.RecordID = a.ItemID) ";
	}
	$sql .=	"WHERE a.DateInput IS NOT NULL
					$conds
					$extraConds
					AND (
							$clsName like '%$s%'
							OR ycu.ClassNumber like '%$s%'
							OR $student_namefield like '%$s%'
							OR c.Name like '%$s%'
							OR d.Name like '%$s%'
							OR a.RecordDate like '%$s%'
							OR c.Name like '%$s2%'
							OR d.Name like '%$s2%'
						) ";
	$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
	$sql .= " GROUP BY a.RecordID";
}

$field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "std_name", "conductImg", "itemName", "RecordDate", "PICID", "action", "WaiveDay", "DateModified", "status");
if($sys_custom['eDiscipline_GM_Times']){
	$field_array = array_merge(array_slice($field_array,0,4),array("GMCount"),array_slice($field_array,4));
}
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$field_array = array_merge(array_slice($field_array,0,4),array("a.ScoreChange"),array_slice($field_array,4));
}
$li->field_array = $field_array;

/*
if($sys_custom['eDiscipline_GM_Times']) {
	$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "std_name", "conductImg", "itemName", "GMCount", "RecordDate", "PICID", "action", "WaiveDay", "DateModified", "status");
} else {
	$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "std_name", "conductImg", "itemName", "RecordDate", "PICID", "action", "WaiveDay", "DateModified", "status");
}
*/

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0,0);
$li->IsColOff = 2;
$li->fieldorder2 = ", a.DateInput desc, ClassNameNum";

// Table Column
$pos = 0;
$li->column_list .= "<td width='1'>#</td>\n";
$li->column_list .= "<td width='8%'>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='1'>".$li->column($pos++, "")."</td>\n";
$li->column_list .= "<td>".$li->column($pos++, $eDiscipline["Category_Item"])."</td>\n";
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$li->column_list .= "<td>".$li->column($pos++, $Lang['eDiscipline']['Score'])."</td>\n";
}
if($sys_custom['eDiscipline_GM_Times']) {
	$li->column_list .= "<td width='5%'>".$li->column($pos++, $Lang['eDiscipline']['ReceiveTimes'])."</td>\n";
}
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $Lang['eDiscipline']['EventDate'])."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$i_Profile_PersonInCharge."</td>\n"; $pos++;
$li->column_list .= "<td width='10%' nowrap>".$eDiscipline["Action"]."</td>\n";$pos++;
if($ldiscipline->use_newleaf && $newleaf==1) {
	$li->column_list .= "<td nowrap>".$i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count."</td>\n";$pos++;
}
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";
$li->column_list .= "<td nowrap>".$li->column($pos++, $i_Discipline_System_Discipline_Status)."</td>\n";
$li->column_list .= "<td>".$li->check("RecordID[]")."</td>\n";

if($sys_custom['eDiscipline_GM_Times'])
{
	$categoryItemLocation = 3;
	$PICLocation = 6;
	$StatusLocation = 12;
	$checkboxLocation = 11;
	$recordIDLocation = 13;
	$actionLocation = 7;		
	$noticeIDLocation = 7;
	$waiveDayLocation = 8;
	$waivedByLocation = 10;	
	$remarkLocation = 15;
	// [2014-1216-1347-28164] location of Push Message ID
	if($canSendPushMsg){
		$pushMsgLocation = 22;
	}
	else {
		$pushMsgLocation = 99;	# no use
	}
	// [2016-0823-1255-04240] location of Homework Deletion flag
	if($sys_custom['eDiscipline']['DisplayHomeworkRelatedMsg'])
	{
		if($canSendPushMsg){
			$homeworkMsgLocation = 23;
			// $homeworkMsgLocation also occupy location = 24
		}
		else {
			$homeworkMsgLocation = 22;
			// $homeworkMsgLocation also occupy location = 23
		}
	}
	else {
		$homeworkMsgLocation = 99;	# no use
	}
	$historyLocation = 99;		# no use
	$caseIDLocation = 99;		# no use
}
else {
	$categoryItemLocation = 3;
	$PICLocation = 5;
	$StatusLocation = 11;
	$checkboxLocation = 10;
	$recordIDLocation = 12;
	$actionLocation = 6;		
	$noticeIDLocation = 6;
	$waiveDayLocation = 7;
	$waivedByLocation = 9;	
	$remarkLocation = 14;	
	// [2014-1216-1347-28164] location of Push Message ID
	if($canSendPushMsg){
		$pushMsgLocation = 21;
	}
	else {
		$pushMsgLocation = 99;	# no use
	}
	// [2016-0823-1255-04240] location of Homework Deletion flag
	if($sys_custom['eDiscipline']['DisplayHomeworkRelatedMsg'])
	{
		if($canSendPushMsg){
			$homeworkMsgLocation = 22;
			// $homeworkMsgLocation also occupy location = 23
		}
		else {
			$homeworkMsgLocation = 21;
			// $homeworkMsgLocation also occupy location =22
		}
	}
	else {
		$homeworkMsgLocation = 99;	# no use
	}
	$historyLocation = 99;		# no use
	$caseIDLocation = 99;		# no use	
}

if($sys_custom['eDiscipline']['PooiToMiddleSchool'])
{
	$PICLocation += 1;
	$StatusLocation += 1;
	$checkboxLocation += 1;
	$recordIDLocation += 1;
	$actionLocation += 1;		
	$noticeIDLocation += 1;
	$waiveDayLocation += 1;
	$waivedByLocation += 1;	
	$remarkLocation += 1;	
	// [2014-1216-1347-28164] location of Push Message ID
	if($canSendPushMsg){
		$pushMsgLocation += 1;
	}
}

if($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport']){
	$nameLocation = 1;
}

######## End of Table Content #########
#######################################

# Access Right Checking
$actionLayer = "";
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval")) {
	$actionLayer = "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','approve_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Approve</a></td>";
	$actionLayer .= "</tr>";
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','reject_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Reject</a></td>";
	$actionLayer .= "</tr><tr>";
}
if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") && $newleaf==1) {
	if($actionLayer != "") {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','newleaf_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> ".$eDiscipline['Waive_By_New_Leaf_Scheme']."</a></td>";
	$actionLayer .= "</tr>";
}
if($fromPPC==1) {
	if($actionLayer != "") {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','cancelPPC_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='12' height='12' border='0' align='absmiddle'> ".$eDiscipline['Setting_Cancel_PPC']."</a></td>";
	$actionLayer .= "</tr>";
}

/*
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Waive")) {
	if($actionLayer != "") {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','waive.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Waive</a></td>";
	$actionLayer .= "</tr>";
}
*/

# Menu Highlight Setting
$CurrentPage = "Management_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct'], "");

$exportLink = "export.php?SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&waived=$waived&s=$s&newleaf=$newleaf&fromPPC=$fromPPC&pic=$pic";

if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-New")) {
	$toolbar = $linterface->GET_LNK_NEW("new1.php",$btn_new,"","","",0);
	$toolbar .= $linterface->GET_LNK_IMPORT("import.php",$btn_import,"","","",0);
}
//$toolbar .= $linterface->GET_LNK_EXPORT($exportLink,$btn_export,"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:showSpan('spanExport')",$btn_export,"","","",0);

// Search Bar
$searchTag = $linterface->Get_Search_Box_Div('text', stripslashes(stripslashes($s)));

# Start layout
$linterface->LAYOUT_START();

//echo $linterface->Include_JS_CSS();
?>

<script language="javascript">
<!--
function showSpan(layername) {
	document.getElementById(layername).style.visibility = "visible";
}

function hideSpan(layername) {
	document.getElementById(layername).style.visibility = "hidden";
}

var xmlHttp
var jsDetention = [];
var jsWaive = [];
var jsHistory = [];
var jsRemark = [];

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3)
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0) {
       alert(globalAlertMsg2);
    }
    else{
        if(confirm(alertConfirmRemove)){
	        obj.action=page;
	        obj.method="post";
	        obj.submit();
    	}
    }
}

function reloadForm(val) {
	document.form1.RecordType.value = val;
	document.form1.s.value = document.form2.text.value;
	document.form1.pageNo.value = 1;
	//document.form1.newleaf.value = "";
	document.form1.submit();
}

//function goSearch(obj) {
//	/*
//	if(document.form2.text.value=="") {
//		alert("<?=$i_Discipline_System_Award_Punishment_Search_Alert?>");
//		document.form2.text.focus();
//		return false;
//	}
//	else {
//	*/
//		document.form1.s.value = document.form2.text.value;
//		
//		document.form1.pageNo.value = 1;
//		document.form1.newleaf.value = "";
//		document.form1.submit();
//		return false;
//	//}
//}

function goSearch(obj) {
	document.form1.s.value = document.form2.text.value;
	<? if($lhomework->exportAllowed) {?>
		document.form1.form1ExportDate.value = document.form2.exportDate.value;
	<?}?>
	document.form1.pageNo.value = 1;
	document.form1.newleaf.value = "";
	document.form1.submit();
	return false;
}

function changeFormAction(obj,element,url) {
	if(countChecked(obj,element)==0) {
		alert(globalAlertMsg2);
	}
	else {
		document.form1.action = url;
		document.form1.submit();
	}
}

function checkCR(evt) {
	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}
}

$(document).ready( function() {
	$('input#text').keydown( function(evt) {
		if (Check_Pressed_Enter(evt)) {
			// pressed enter
			goSearch();
		}
	});
});
//document.onkeypress = checkCR;

/*********************************/
function getObject( obj ) {
	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	}
	else if ( document.all ) {
		obj = document.all.item( obj );
	}
	else {
		obj = null;
	}
	return obj;
}

function moveObject( obj, e, moveX ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;

	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.documentElement.scrollLeft;
		tempY = event.clientY + document.documentElement.scrollTop;
	}
	else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	if(moveX != undefined)
		tempX = tempX - moveX
	else 
		if (tempX < 0){tempX = 0} else {tempX = tempX - 320}

	if (tempY < 0){tempY = 0} else {tempY = tempY}
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {
	obj = getObject( obj );
	if (obj==null) return;
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
}

function hideAllOtherLayer() {
	var layer = "";
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

function stateChanged() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	}
}

function stateChanged2() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	}
}

function stateChanged3() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	}
}

function showResult(str,flag)
{
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	}
	
	var url = "";
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	}
	else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	}
	else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	}
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

$().ready(function () {
	hideAllOtherLayer();
});

function doExport() {
	document.form1.sDate.value = document.form2.startDate.value; 
	document.form1.eDate.value = document.form2.endDate.value; 
	
	document.form1.method = "GET";
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.method = "POST";
	document.form1.action = "";
}

<?php if($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport']){ ?>
	function redirectToReport(TargetClassName, TargetUserID)
	{
		var TargetSchoolYear = document.form1.SchoolYear.value;
		var TargetSemester = document.form1.semester.value;
		
		var parms = '?targetClass='+TargetClassName+'&targetID='+TargetUserID+'&startdate=&enddate=&detailType=10&show_waive_status=&ClickID=&RecordID=';
		parms += '&fromMgmt=1&targetSchoolYear='+TargetSchoolYear+'&targetSemester='+TargetSemester;
		
		var url = '/home/eAdmin/StudentMgmt/disciplinev12/reports/master_report/personal_report/personal_check.php'+parms;
		
		var win = window.open(url, '_blank');
	  	win.focus();
	}
<?php } ?>

//-->
</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<form name="form2" method="post" onSubmit="return goSearch('document.form2')">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table border="0" cellspacing="0" cellpadding="2" width="700">
								<tr>
									<td valign="top">
										<div class="Conntent_tool">
											<?=$toolbar ?>	
											<div class='print_option_layer' id='spanExport' style="width:200px;left:300px">
					                        	<em > - <?=$Lang['General']['ExportOptions']?> -</em>
					                          	<table class='form_table'>
						                            <col class='field_title' />
						                            <col class='field_c' />
						                            <tr>
						                            	<td width='40%'><?=$i_From?></td>
						                              	<td>:</td>
						                              	<td><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
						                            </tr>
						                            <tr>
						                              	<td><?=$i_To?></td>
						                              	<td>:</td>
						                              	<td><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
						                            </tr>
					                          	</table>
					                          	<div class='edit_bottom'>
					                          		<p class='spacer'></p>
						                            <input type="button" name="button1" id="button1" value="<?=$button_submit?>" onClick="doExport()">
						                            <input type="button" name="button2" id="button2" value="<?=$button_cancel?>" onClick="hideSpan('spanExport')">
						                            <p class='spacer'></p>
						                    	</div>
					                        </div>
				                        </div>
									</td>
								</tr>
							</table>
						</td>
						<td align="right">
							<?=$searchTag?>
						</td>
					</tr>
				</table>
			</form>
			
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td height="30">
												<?=$selectSchoolYear?>
												<?=$SemesterMenu?>
												<?=$select_class?>
												<?=$picSelection?>
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$choiceType?></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Award_Punishment_Warning) ?>
											</td>
										</tr>
									</table>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr class="table-action-bar">
											<td width="12%">
												<div class="selectbox_group"> <a href="javascript:;" onClick="MM_showHideLayers('status_option','','show')"><?=$i_Discipline_System_Award_Punishment_Select_Status?></a> </div>
												<br style="clear:both">
												<div id="status_option" class="selectbox_layer">
													<table width="" border="0" cellspacing="0" cellpadding="3">
														<tr>
															<td>
																<!--<input type="checkbox" name="waitApproval" id="waitApproval" value="1" <?=$waitApprovalChecked?>>
																<label for="waitApproval"><?=$i_Discipline_System_Award_Punishment_Pending?></label><br>//-->
																<input type="checkbox" name="approved" id="approved" value="1" <?=$approvedChecked?>>
																<label for="approved"><?=$i_Discipline_System_Award_Punishment_Approved?></label><br>
																<input type="checkbox" name="rejected" id="rejected" value="1" <?=$rejectedChecked?>>
																<label for="rejected"><?=$i_Discipline_System_Award_Punishment_Rejected?></label>
															</td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td>
																<input type="checkbox" name="waived" id="waived" value="1" <?=$waivedChecked?>>
																<label for="waived"><?=$i_Discipline_System_Award_Punishment_Waived?></label>
															</td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td align="center" class="tabletext">
																<?= $linterface->GET_BTN($button_apply, "button", "javascript:reloadForm($RecordType)")?>
																<?= $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');form.reset();")?>
															</td>
														</tr>
													</table>
												</div>
											</td>
											<td width="28%"><?=$typeSelection?>&nbsp;</td>
											<td align="right" valign="bottom" width="60%">
												<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Waive") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")) { ?>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Approval") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-Waive") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")) { ?>
																	<td nowrap><a href="javascript:;" onClick="moveObject('status_list',true, '100');MM_showHideLayers('status_list','','show');" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_update.gif" width="12" height="12" border="0" align="absmiddle"> <?=$i_Discipline_System_Award_Punishment_Change_Status?></a>
																		<div id="status_list" style="position:absolute; width:100px; z-index:1; visibility: hidden;">
																			<table width="100%" border="0" cellpadding="0" cellspacing="0" >
																				<tr>
																					<td height="19">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="5" height="19"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_01.gif" width="5" height="19"></td>
																								<td height="19" valign="middle" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_02.gif">&nbsp;</td>
																								<td width="19" height="19"><a href="javascript:;" onClick="MM_showHideLayers('status_list','','hide')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_off.gif" name="pre_close22" width="19" height="19" border="0" id="pre_close22" onMouseOver="MM_swapImage('pre_close22','','<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
																							</tr>
																						</table>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="5" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif" width="5" height="19"></td>
																								<td bgcolor="#FFFFF7">
																									<table width="98%" border="0" cellpadding="0" cellspacing="3">
																										<?=$actionLayer?>
																									</table>
																								</td>
																								<td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif" width="6" height="6"></td>
																							</tr>
																							<tr>
																								<td width="5" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_07.gif" width="5" height="6"></td>
																								<td height="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif" width="5" height="6"></td>
																								<td width="6" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_09.gif" width="6" height="6"></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																	<?} ?>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn")) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:checkEditMultiple(document.form1,'RecordID[]','batch_notice_new.php?field=<?=$li->field?>&order=<?=$li->order?>&pageNo=<?=$li->pageNo?>&numPerPage=<?=$li->page_size?>&MeritType=<?=$RecordType?>&s=<?=$s?>&SchoolYear2=<?=$SchoolYear?>&semester2=<?=$semester?>&targetClass2=<?=$targetClass?>&approved2=<?=$approved?>&rejected2=<?=$rejected?>&waived2=<?=$waived?>&fromPPC=<?=$fromPPC?>&passedActionDueDate=<?=$passedActionDueDate?>')" class="tabletool" title="<?= $Lang['eDiscipline']['AddNotice'] ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_handle.gif" width="12" height="12" border="0" align="absmiddle"> <?= $Lang['eDiscipline']['AddNotice'] ?> </a></td>
																	<? } ?>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn")) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:checkEdit(document.form1,'RecordID[]','edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<? } ?>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-DeleteOwn")) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:removeCat(document.form1,'RecordID[]','remove_update.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																	<? } ?>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
												<? } ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?=$li->displayFormat_Split_PIC("GoodConduct_Misconduct", $PICLocation, $StatusLocation, $checkboxLocation, $actionLocation, $recordIDLocation, "{$image_path}/{$LAYOUT_SKIN}", $historyLocation, $caseIDLocation, $waivedByLocation, $noticeIDLocation, $waiveDayLocation, $categoryItemLocation, $remarkLocation, $newleaf, "", "", "", $pushMsgLocation, "", $nameLocation, $homeworkMsgLocation)?>
						
					</td>
				</tr>
			</table><br>
			
			<div width="100%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>
			
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="page_size_change" value=""/>
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="RecordType" value="<?=$RecordType ?>">
			<input type="hidden" name="s" value="<?=stripslashes(stripslashes($s))?>"/>
			<input type="hidden" name="newleaf" value="<?=$newleaf?>"/>
			<input type="hidden" name="fromPPC" value="<?=$fromPPC?>"/>
			<input type="hidden" name="clickID" id="clickID" />
			<input type="hidden" name="sDate" id="sDate" value="">
			<input type="hidden" name="eDate" id="eDate" value="">
			</form>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>