<?php 
# using: 

################################
#
#	Date:	2017-10-04	(Bill)
#			Fixed: Whole Year > Score Change Layer show selected student punishment only
#
#	Date:	2012-10-30	YatWoon
#			Improved: display layer for study score details
#
#	Date:	2012-08-27	YatWoon
#			fix incorrect wording display $Lang['eDiscipline']['ComputeStudyScoreGrade']
#
################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

# Change "num per page"
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}
if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
}
else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}
if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
}
else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $sortEventDate;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");

## Conduct Mark Calculation Method
$ConductMarkCalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();

# Year Info for Year Selection
$yearInfo = GetAllAcademicYearInfo();
$yearArr = array();
for($i=0; $i<sizeof($yearInfo); $i++) {
	list($year_id, $yearEN, $yearB5) = $yearInfo[$i];
	$yearArr[$i][] = $year_id;
	$yearArr[$i][] = Get_Lang_Selection($yearB5, $yearEN);
}

# Target Year
if($Year=='')
{
	$Year = Get_Current_Academic_Year_ID();
}
$AcademicYearID = $Year;

# Semester Info for Semester Selection
$semester_data = getSemesters($AcademicYearID);
$number = sizeof($semester_data);

# Study Score Calculation Method
$StudyScoreCalculationMethod = $ldiscipline->retriveStudyScoreCalculationMethod();
if($StudyScoreCalculationMethod==0) {
	// Calculate by Term
	$semArr[] = array('',$ec_iPortfolio['whole_year']);
}
foreach($semester_data as $id=>$name) {
	$semArr[] = array($id, $name);
}

# Target Semester
$YearTermID = $Semester;
if($YearTermID=='' && $YearTermID!='0') 
	$YearTermID = 0;

# Get Initial Score
$sql = "SELECT ConductScore FROM DISCIPLINE_CONDUCT_SETTING WHERE RecordType=0";
$temp = $ldiscipline->returnVector($sql);
$initial_score = $temp[0];

# Year & Semester Selection
$yearSelection = $linterface->GET_SELECTION_BOX($yearArr, 'id="Year", name="Year" onChange="if(this.selectedIndex>0) {document.form1.Semester.value=\'\'; document.form1.ClassID.value=\'\';document.form1.submit();}"', $Lang['eDiscipline']['FieldTitle']['SelectSchoolYear'], $Year);
$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester" name="Semester" onChange="document.form1.submit();"', "", $Semester);

# Set default to the first class
if(empty($ClassID))
{
	$orderBy = " ORDER BY Sequence";
	$sql = "SELECT YearClassID, ClassTitleEN, ClassTitleB5 FROM YEAR_CLASS WHERE AcademicYearID=$AcademicYearID $orderBy limit 1";
	$result = $ldiscipline->returnArray($sql);
	$ClassID = $result[0]['YearClassID'];
}
$ClassID = $ClassID==-1 ? "" : $ClassID;
$classSelection = $ldiscipline->getSelectClass("name=\"ClassID\" onChange=\"document.form1.submit()\"", $ClassID, $i_Discipline_System_Award_Punishment_All_Classes, 1, -1);

/*
$toolbar = "<table width=\"350\" cellspacing=\"0\" cellpadding=\"0\"><tr>";
$toolbar .= "<td>".$linterface->GET_LNK_EXPORT("export.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID&ClassID=$ClassID&searchStr=$searchStr","","","","",0)."</td>";
$toolbar .="<td>".$linterface->GET_LNK_GENERATE("generate.php","","","","",0)."</td>";
$toolbar.="</tr></table>";
*/
$toolbar .= "<div class='Conntent_tool'>";
$toolbar .= $linterface->GET_LNK_EXPORT("export.php?AcademicYearID=$AcademicYearID&YearTermID=$YearTermID&ClassID=$ClassID&searchStr=$searchStr","","","","",0);
$toolbar .= $linterface->GET_LNK_GENERATE("generate.php",$Lang['eDiscipline']['ComputeStudyScoreGrade'],"","","",0);
$toolbar .= "</div>";

$searchTB = "<input type=\"textbox\" class=\"formtextbox\" id=\"searchStr\" name=\"searchStr\" value=\"$searchStr\">&nbsp;".toolBarSpacer().$linterface->GET_BTN($button_find,'button','document.form1.submit()');

# DB Table Settings
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$extraCriteria .= ($ClassID=='') ? "" : " AND ycu.YearClassID=$ClassID";
$extraCriteria .= ($searchStr=='')? "" : " AND (iu.EnglishName like '%".trim($searchStr)."%' or iu.ChineseName like '%".trim($searchStr)."%' )";

$conds_year = ($AcademicYearID=='0')? "": " AND (sub_bal.AcademicYearID='$AcademicYearID')";
$conds_semester = ($YearTermID=='0' || $YearTermID=='')? " AND (sub_bal.IsAnnual = '1')" : " AND (sub_bal.YearTermID='$YearTermID')";
$conds_semester2 = ($YearTermID=='0' || $YearTermID=='')? " " : " AND (m.YearTermID='$YearTermID')";

if ($YearTermID=='0' || $YearTermID=='')
	$YearTermIDStr = 'IsAnnual';
else
	$YearTermIDStr = $YearTermID;

$sql_adj_semester = $ldiscipline->convertSemesterStringToNumSql('sub_bal', 1);
$sql_adj_semester = $ldiscipline->convertSemesterStringToNumSql('sub_bal', 1);
$baseMark = $ldiscipline->getStudyScoreRule('baseMark');

//if($ldiscipline->UseSubScore)
{	
	$studentIDs = $ldiscipline->storeStudent('0','::'.$ClassID);
	$isAnnual = ($YearTermID=="" || $YearTermID=='0') ? 1 : 0;
	$Subscore1Arr = $ldiscipline->retrieveStudentSubscore1($studentIDs, $AcademicYearID, $YearTermID, $isAnnual);
}

### Note: Changed to use from left outer join to inner join ###
$sql = "SELECT 
			concat(".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN').",'-',ycu.ClassNumber) as ClassNameNum, 
			".getNameFieldByLang("iu.")." as UserName,
			IF(sub_bal.SubScore IS NULL, '$baseMark', concat('a',sub_bal.SubScore)) as SubScore,
			IFNULL(sub_bal.GradeChar,'--') as GradeChar,
			IFNULL(sub_bal.DateModified, '--') as DateModified,
			iu.UserID as StudentID,
			count(m.RecordID) as m_count,
			'".$AcademicYearID."' as AcademicYearID, 
			'".$YearTermID."' as YearTermID
		FROM 
			INTRANET_USER iu 
			LEFT OUTER JOIN DISCIPLINE_STUDENT_SUBSCORE_BALANCE sub_bal  ON (iu.UserID = sub_bal.StudentID $conds_semester $conds_year)
			INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID)
			INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
			left join DISCIPLINE_MERIT_RECORD as m on (m.StudentID=iu.UserID and m.SubScore1Change!=0 and m.RecordStatus=1 and m.ReleaseStatus=1 and m.AcademicYearID=$AcademicYearID $conds_semester2)
		WHERE
			 1 $extraCriteria AND iu.RecordType=2 AND iu.RecordStatus IN (1) AND yc.YearClassID != 0 AND yc.AcademicYearID=$AcademicYearID
		GROUP BY iu.UserID";
$SettingName = ($Semester == '')? "study_score_gen_".$Year."_Annual": "study_score_gen_".$Year."_$Semester";

# Init DB Table
if (!isset($order)) $order = 1;
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "iu.EnglishName", "SubScore", "GradeChar","DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0,0);
$li->IsColOff = 2;

# DB Table Header
$pos = 0;
$li->column_list .= "<th width='1%'>#</th>\n";
$li->column_list .= "<th width='20%'>".$li->column_IP25($pos++, $i_general_class)."</th>\n";
$li->column_list .= "<th width='30%'>".$li->column_IP25($pos++, $i_general_name)."</th>\n";
$li->column_list .= "<th width='15%'>".$i_Discipline_System_Subscore1."</th>\n";
$li->column_list .= "<th width='15%'>".$Lang['eDiscipline']['StudyScoreGrade']."</th>\n";
$li->column_list .= "<th width='20%'>$i_Discipline_System_Discipline_Conduct_Last_Updated</th>\n";

# Get last grade generation
$sqlgen = "select DATE_FORMAT(SettingValue,'%Y-%m-%d' ) as LastGen from DISCIPLINE_GENERAL_SETTING
		   where SettingName = '$SettingName' order by SettingValue desc limit 1";
$LastGen = $ldiscipline->returnVector($sqlgen);
if(sizeof($LastGen)==0)
{
	$LastGen[0] = '--';
}

# Top Menu
$CurrentPage = "Management_StudyScore";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Discipline_System_Subscore1);

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
var targetDivID = "";
var clickDivID = "";

function adjust_record(StudentID,Year,Semester)
{
	//checkPost(document.form1, 'adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester);
	window.location='adjust.php?StudentID='+StudentID+'&Year='+Year+'&Semester='+Semester;
}

function Show_Merit_Window(StudentID,Year,Semester){
    obj = document.form1;
    obj.targetDivID.value = StudentID+Year+Semester;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = Year;
    obj.ajaxSemester.value = Semester;
    obj.ajaxType.value = 'Merit';
    
    clickDivID = 'merit_'+StudentID+Year+Semester;
    targetDivID = StudentID+Year+Semester;
    YAHOO.util.Connect.setForm(obj);
    
    var path;
    path = "ajax_subscore.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
}

var callback2 = {
    success: function ( o )
    {
		DisplayDefDetail(o.responseText);
	}
}

function DisplayDefDetail(text){
	document.getElementById('div_form').innerHTML=text;
	DisplayPosition();
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}
	return pos_value;
}

function DisplayPosition(){
	var posleft = getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
	var postop = getPosition(document.getElementById(clickDivID),'offsetTop') +10;

  	//document.getElementById(targetDivID).style.left=getPosition(document.getElementById(clickDivID),'offsetLeft') +10;
  	//document.getElementById(targetDivID).style.top=getPosition(document.getElementById(clickDivID),'offsetTop') +10;
  	document.getElementById(targetDivID).style.left= posleft+"px";
  	document.getElementById(targetDivID).style.top= postop+"px";
  	document.getElementById(targetDivID).style.visibility='visible';
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function confirmDeleteAdjust(RecordID, StudentID, Year, Semester) {
	Remove_Adjust_Window(RecordID, StudentID, Year, Semester);
	//Show_Adj_Window(StudentID,Year,Semester);
}

function Remove_Adjust_Window(RecordID, StudentID, Year, Semester)
{
	if(confirm("<?=$i_Discipline_System_alert_remove_warning_level?>")) {
		clickDivID = 'adj_'+StudentID+Year+Semester;
		
		$.post(
			"ajax_conduct.php", 
			{
				Action: "Edit",
				DivID: encodeURIComponent('adj_'+StudentID+Year+Semester),
				targetDivID: StudentID+Year+Semester,
				ajaxStudentID: StudentID,
				ajaxYear: Year,
				ajaxSemester: Semester,
				ajaxType: 'Remove_Adjust',
				ajaxRecordID: RecordID
			},
			function(ReturnData)
			{
				//alert(ReturnData);
				if(ReturnData==1) {
					self.location.reload();
				} else {
					Get_Return_Message("<?=$Lang['General']['ReturnMessage']['UpdateUnsuccess']?>")
				}
			}
		);
	}
}

function goSubmit(year) {
	$('#Year').val(year);
	document.form1.Semester.value='';
	document.form1.ClassID.value=''
	document.form1.submit();	
}
</script>

<br />
<form name="form1" method="get" action="">
			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td colspan="2" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%"></td>
										</tr>
									</table>
								</td>
								<td align="right"><?=$searchTB ?></td>
							</tr>
							<tr><td><?=$yearSelection?><?=$semSelection?><?=$classSelection?>
							</tr>
						</table>
						<br style="clear:both"></td>
					</td>
				</tr>
				<tr>
					<td><?= $li->displayFormat_Study_Score_Management($Subscore1Arr) ?>
					<div id="div_form"></div>
					</td>
				</tr>
				<tr><td class="tabletextremark">^<?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <?=$LastGen[0]?></td></tr>
				</table><br>
				
				<input type="hidden" name="targetDivID" id="targetDivID" />
				<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
				<input type="hidden" name="ajaxYear" id="ajaxYear" />
				<input type="hidden" name="ajaxSemester" id="ajaxSemester" />
				<input type="hidden" name="ajaxType" id="ajaxType" />
				<input type="hidden" name="ajaxRecordID" id="ajaxRecordID" />
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>