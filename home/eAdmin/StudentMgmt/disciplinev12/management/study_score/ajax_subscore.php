<?
// using : yat

/*************************** Change Log ***************************/
##
##
/************************ End of Change Log ************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

## Get the study score mark calculation method
$ConductMarkCalculationMethod = $ldisciplinev12->retriveStudyScoreCalculationMethod();

if($task=='check_gen_time')
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	
	$OriYearTermID = $ajaxSemester;
	if($ajaxSemester == '0' || $ajaxSemester=='')
	{
		$ajaxSemester = "Annual";
	}
		
		$SettingName = "study_score_gen_".$ajaxYear."_".$ajaxSemester;
		$x = $ldisciplinev12->getLastConductGradeGenDate($SettingName);
		
		if ($x == '')
		{
			$Obj_AcademicYear = new academic_year($ajaxYear);
			$AcademicYearTitleEn = $Obj_AcademicYear->YearNameEN;
			
			$Obj_YearTerm = new academic_year_term($OriYearTermID);
			$YearTermTitleEn = $Obj_YearTerm->YearTermNameEN;
			
			$SettingName = "study_score_gen_".$AcademicYearTitleEn."_".$YearTermTitleEn;
			$x = $ldisciplinev12->getLastConductGradeGenDate($SettingName);
		}
		
		$sql = "select Ratio, AcademicYearID, YearTermID from DISCIPLINE_SEMESTER_RATIO where AcademicYearID ='$ajaxYear' and RecordType='S'";
		$ResultArr = $ldisciplinev12->returnArray($sql);	
		
		if($ajaxSemester=="Annual")
		{
			if($ConductMarkCalculationMethod == 0)
			{
				## only show the ratio table if calculate by term (no accumlate) ##	
				$x.='<table border="0" cellspacing="0" cellpadding="3" class="tablerow2">';																
				$x .='<tr>
						<td><fieldset class="form_sub_option" style="margin:0px;">
							<table border="0" cellspacing="0" cellpadding="3">
							<tr><td colspan="2" class="tablebluebottom"><strong>'.$i_Discipline_System_Discipline_Conduct_Mark_Weighting.'</strong></td></tr>';
				if(sizeof($ResultArr)>0)
				{			
					$x.='<tr><td><u>'.$i_Discipline_System_Discipline_Conduct_Mark_Weight_Semester.'</u></td>
					<td align="center"><u>'.$i_Discipline_System_Discipline_Conduct_Mark_Weight.'</u></td></tr>';
					for($a=0;$a<sizeof($ResultArr);$a++)
					{
						$thisYearTermID = $ResultArr[$a]['YearTermID'];
						$Obj_YearTerm = new academic_year_term($thisYearTermID);
						$thisTermDisplay = $Obj_YearTerm->Get_Year_Term_Name();
						
						$x.='<tr><td>'.$thisTermDisplay.'</td><td align="center">'.$ResultArr[$a]['Ratio'].'</td></tr>';	
						$sum += $ResultArr[$a]['Ratio'];
					}
					$x.='<tr><td align="right" class="tabletextremark" style="border-top:1px solid #CCCCCC">'.$i_Discipline_System_Discipline_Conduct_Mark_Weight_Total.'</td>
						<td align="center" class="tabletextremark" style="border-top:1px solid #CCCCCC">'.$sum.'</td>
					</tr>';
				}
				else
				{
					$x.='<tr><td  align="center" colspan="2">'.$i_Discipline_System_Discipline_Conduct_Mark_No_Ratio_Setting.'</td></tr>';	
				}
				$x.='</table>';
			}
		}
		
		echo $x;
}
else if ($task == 'reload_term_selection')
{
	$semester_data = getSemesters($AcademicYearID);
	
	$number = sizeof($semester_data);
	$semArr[] = array('0',$ec_iPortfolio['whole_year']);
	
	if($number > 0)
	{	
		foreach($semester_data as $id=>$name)
		{
			$semArr[] = array($id, $name);
		}
	}
	$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester" onChange="change_field(\'ajaxSemester\',this.value)"', "", "");

	echo $semSelection;
}
else
{
	$yearID = ($ajaxYear!="") ? $ajaxYear : $Year;
	$YearTermID = $ajaxSemester;
	
	if($ajaxType=='Merit')
	{
		$StudentMeritArr = $ldisciplinev12->retrieveStudentAllMeritRecordByYearSemester($ajaxStudentID, '', '', $yearID, $YearTermID, "studyscore", 1);

		$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td align="center" valign="middle">#</td>
									<td align="left">'.$i_Discipline_Reason.'</td>
									<td align="left">'.$i_Discipline_PIC.'</td>
									<td >'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
									<td >'.$Lang['eDiscipline']['EventDate'].'</td>
					</tr>';
		for($a=0;$a<sizeof($StudentMeritArr);$a++)
		{
			$data .='<tr class="tablerow1"> 
					 <td>'.($a+1).'</td>
					 <td>'.$StudentMeritArr[$a]['Remark'].'</td>
					 <td>'.$StudentMeritArr[$a]['PIC'].'</td>
					 <td>'.$StudentMeritArr[$a]['ScoreChange'].'</td>
					 <td>'.$StudentMeritArr[$a]['RecordDate'].'</td>
					</tr>';
		}
	}

	if($ajaxType!='Remove_Adjust')
		$data .= '	</table>';

	$div_deb = '<div id="'.$targetDivID.'" style="position:absolute; width:380px; z-index:1; ">
		  <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\''.$targetDivID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
						'.$data.'
						
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table></div>';  

	
	if($ajaxType!='Remove_Adjust')
		echo $div_deb;
}
?>
