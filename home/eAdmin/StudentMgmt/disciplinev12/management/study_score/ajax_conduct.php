<?
// using : henry chow

/*************************** Change Log ***************************/
##
##	Date	:	20091208 (Ronald)
##	Details	:	in (ajaxSemester=="Annual") condition, I added a checking to check if now using accumlate method or not.
##				if using accumlate method, then no need to show the ratio table
##
/************************ End of Change Log ************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

## Get the conduct mark calculation method
$ConductMarkCalculationMethod = $ldisciplinev12->retriveConductMarkCalculationMethod();


if($task=='check_gen_time')
{
	include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
	
	$OriYearTermID = $ajaxSemester;
	if($ajaxSemester == '0' || $ajaxSemester=='')
	{
		$ajaxSemester = "Annual";
	}
		
		$SettingName = "study_score_gen_".$ajaxYear."_".$ajaxSemester;
		$x = $ldisciplinev12->getLastConductGradeGenDate($SettingName);
		
		if ($x == '')
		{
			$Obj_AcademicYear = new academic_year($ajaxYear);
			$AcademicYearTitleEn = $Obj_AcademicYear->YearNameEN;
			
			$Obj_YearTerm = new academic_year_term($OriYearTermID);
			$YearTermTitleEn = $Obj_YearTerm->YearTermNameEN;
			
			$SettingName = "study_score_gen_".$AcademicYearTitleEn."_".$YearTermTitleEn;
			$x = $ldisciplinev12->getLastConductGradeGenDate($SettingName);
		}
		
		$sql = "select Ratio, AcademicYearID, YearTermID from DISCIPLINE_SEMESTER_RATIO where AcademicYearID ='$ajaxYear'";
		$ResultArr = $ldisciplinev12->returnArray($sql);	
		
		echo $x;
}
else if ($task == 'reload_term_selection')
{
	$semester_data = getSemesters($AcademicYearID);
	
	$number = sizeof($semester_data);
	$semArr[] = array('0',$ec_iPortfolio['whole_year']);
	
	if($number > 0)
	{	
		//for ($i=0; $i<$number; $i++)
		foreach($semester_data as $id=>$name)
		{
			$semArr[] = array($id, $name);
		}
	}
	$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester" onChange="change_field(\'ajaxSemester\',this.value)"', "", "");

	echo $semSelection;
}
else
{
	
	$yearID = ($ajaxYear!="") ? $ajaxYear : $Year;
	$YearTermID = $ajaxSemester;
	
	$cond = " cond_adj.StudentID = $ajaxStudentID and cond_adj.AcademicYearID = '$yearID' ";
	if($YearTermID=='' || $YearTermID==0)
	{
		$cond.=" and cond_adj.Isannual=1";
	}
	else
	{
		$cond.=" and cond_adj.YearTermID = '$YearTermID'";
	}
	
	
	if($ajaxType=='Remove_Adjust') {
		
		if($ajaxSemester=="") {
			$conds = " AND IsAnnual=1";
		} else {
			$conds = " AND YearTermID='$ajaxSemester'";
		}
		
		$sql = "DELETE FROM DISCIPLINE_CONDUCT_ADJUSTMENT WHERE RecordID=$ajaxRecordID AND StudentID=$ajaxStudentID AND AcademicYearID=$ajaxYear $conds";
		$result = $ldisciplinev12->db_db_query($sql);
		
		if($result)
			echo 1;
	}
	
	if($ajaxType=='Adjust')
	{
		$sql = "select cond_adj.Reason, ".getNameFieldByLang("iu.")." as PIC, cond_adj.AdjustMark, 
			DATE_FORMAT(cond_adj.DateInput,'%Y-%m-%d') as DateInput, cond_adj.RecordID, cond_adj.StudentID
			from DISCIPLINE_CONDUCT_ADJUSTMENT as cond_adj left join INTRANET_USER as iu on iu.UserID = cond_adj.PICID
			where  $cond ORDER BY cond_adj.DateModified DESC";
			
		$resultArr = $ldisciplinev12->returnArray($sql);	
	
		
		$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td align="center" valign="middle">#</td>
									<td >'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
									<td align="left">'.$i_Discipline_PIC.'</td>
									<td >'.$eDiscipline["AdjustmentDate"].'</td>
									<td align="left">'.$i_Discipline_Reason.'</td>';
		if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust")) {
			$data .=				'<td >&nbsp</td>';
		}									
		$data .= 					'</tr>';
		if(sizeof($resultArr)>0)
		{
			for($a=0;$a<sizeof($resultArr);$a++)
			{
				$data .='<tr class="tablerow1">
						 <td>'.($a+1).'</td>
						 <td>'.$resultArr[$a]['AdjustMark'].'</td>
						 <td>'.$resultArr[$a]['PIC'].'</td>
						 <td>'.$resultArr[$a]['DateInput'].'</td>
						 <td>'.$resultArr[$a]['Reason'].'</td>';
				if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust")) 
					$data .= '<td><div class="table_row_tool"><a href="javascript:confirmDeleteAdjust(\''.$resultArr[$a]['RecordID'].'\',\''.$resultArr[$a]['StudentID'].'\',\''.$Year.'\',\''.$Semester.'\')" class="delete_dim">&nbsp;</a></div></td>';
				$data .= '</tr>';
			}
		}else{
			//$data .= "<tr><td colspan='5' align='center'>".$i_no_record_exists_msg."</td></tr>";
			$data .= "<tr><td colspan='5' align='center'>".$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['NoRecordAtThisTerm']."</td></tr>";
		}
	}
	elseif($ajaxType=='Merit')
	{


		$StudentMeritArr = $ldisciplinev12->retrieveStudentAllMeritRecordByYearSemester($ajaxStudentID, '', '', $yearID, $YearTermID);
	
		//$StudentMeritArr = $ldisciplinev12->retrieveStudentAllMeritRecordByYearSemester($ajaxStudentID,$ajaxYear,$Semester);	

		$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td align="center" valign="middle">#</td>
									<td align="left">'.$i_Discipline_Reason.'</td>
									<td align="left">'.$i_Discipline_PIC.'</td>
									<td >'.$i_Discipline_System_Discipline_Conduct_Mark_Adjustment.'</td>
									<td >'.$Lang['eDiscipline']['EventDate'].'</td>
					</tr>';
		for($a=0;$a<sizeof($StudentMeritArr);$a++)
		{
			/*
			$To = $StudentMeritArr[$a]['ToScore'];
			$From = $StudentMeritArr[$a]['FromScore'];
			$AdjustedScore = $To-$From;
			*/
			$AdjustedScore = $StudentMeritArr[$a]['ConductScoreChange'];
			$data .='<tr class="tablerow1">
					 <td>'.($a+1).'</td>
					 <td>'.$StudentMeritArr[$a]['Remark'].'</td>
					 <td>'.$StudentMeritArr[$a]['PIC'].'</td>
					 <td>'.$AdjustedScore.'</td>
					 <td>'.$StudentMeritArr[$a]['RecordDate'].'</td>
					</tr>';
		}
		
		
	}




	if($ajaxType!='Remove_Adjust')
		$data .= '	</table>';

	$div_deb = '<div id="'.$targetDivID.'" style="position:absolute; width:380px; z-index:1; ">
		  <table width="100%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\''.$targetDivID.'\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
						'.$data.'
						
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table></div>';  

	
	if($ajaxType!='Remove_Adjust')
		echo $div_deb;
}
?>
