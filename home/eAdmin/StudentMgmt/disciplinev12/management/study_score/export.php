<?php
// Using:

#################### Change Log #####################
##
##	Date 	: 	20180213 (Bill)		[2018-0208-1510-40235]
##	Details :	Export Study Score Term Balance
##
##	Date 	: 	20100211 (Henry)
##	Details :	Modified the query by using left outer join instead of inner join when joining DISCIPLINE_STUDENT_CONDUCT_BALANCE
## 
##	Date 	: 	20100202 (Ronald)
##	Details :	Modified the query by using from left outer join to inner join when joining INTRANET_USER
##
#####################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");

$extraCriteria .= ($ClassID=="") ? "" : " AND ycu.YearClassID = $ClassID";
$extraCriteria .= ($searchStr=="") ? "" : " AND (iu.EnglishName LIKE '%".trim($searchStr)."%' OR iu.ChineseName LIKE '%".trim($searchStr)."%' )";

$conds_year = ($AcademicYearID=='0')? "" : " AND (sub_bal.AcademicYearID = '$AcademicYearID')";
$conds_semester = ($YearTermID=="0" || $YearTermID=="") ? " AND (sub_bal.IsAnnual = '1') " : " AND (sub_bal.YearTermID = '$YearTermID') ";

$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$baseMark = $ldiscipline->getStudyScoreRule('baseMark');

### Note: Changed to use from left outer join to inner join ###
$sql = "SELECT 
			$clsName as ClassName, 
			ycu.ClassNumber, 
			".getNameFieldByLang("iu.")." as UserName,
			IF(sub_bal.SubScore IS NULL, '$baseMark', sub_bal.SubScore) as SubScore,
			IFNULL(sub_bal.GradeChar,'--') as GradeChar,
			IFNULL(sub_bal.DateModified, '--') as DateModified
		FROM 
			INTRANET_USER iu 
			LEFT JOIN DISCIPLINE_STUDENT_SUBSCORE_BALANCE sub_bal  ON (iu.UserID = sub_bal.StudentID $conds_semester $conds_year)
			INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID)
			INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE
			 1 $extraCriteria AND iu.RecordType = 2 AND iu.RecordStatus IN (1) AND yc.YearClassID != 0 AND yc.AcademicYearID = '$AcademicYearID'
		GROUP BY iu.UserID
		ORDER BY yc.Sequence, ycu.ClassNumber";
$row = $ldiscipline->returnArray($sql,9);

# Create data array for export
$ExportArr = array();
for($i=0; $i<sizeof($row); $i++)
{
	if(sizeof($tmpArr)==0 || $row[$i]['AdjustMark']=="--" || trim($row[$i]['AdjustMark'])=="")
	{
		$LastUpdate = "--";
	}
	else
	{
		$LastUpdate = $ldiscipline->convertDatetoDays($row[$i]['LastUpdate']);
    }
 	
 	$ExportArr[$i][0] = $row[$i]['ClassName'];		// Class Name
 	$ExportArr[$i][1] = $row[$i]['ClassNumber'];	// Class Number
	$ExportArr[$i][2] = $row[$i]['UserName'];		// Student Name
	$ExportArr[$i][3] = $row[$i]['SubScore'];		// Study Score
	$ExportArr[$i][4] = $row[$i]['GradeChar'];		// Score Grade
	$ExportArr[$i][5] = $row[$i]['DateModified'];
}

# Column Title
$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name, "Study Score", $i_Discipline_System_Report_ClassReport_Grade, $i_Discipline_System_Discipline_Conduct_Last_Updated);
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$filename = 'study_score.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>