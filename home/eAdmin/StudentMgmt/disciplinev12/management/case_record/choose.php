<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html("popup.html");

if ($plugin['Disciplinev12'])
{

	$CurrentPage = "Management_AwardInput";
	$CurrentPageArr['eDisciplinev12'] = 1;
	$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
	$MODULE_OBJ['title'] = $eDiscipline['SelectTeacherStaff'];

	$lclass = new libclass();
	$select_class = $lclass->getTeacher(" id=\"targetID[]\" size=\"21\" multiple=\"multiple\"",$targetClass);


$linterface->LAYOUT_START();
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name="form1" action="index.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
			<td valign="top" nowrap="nowrap"><span class="tabletext"><?= $iDiscipline['teacher']?>:</span></td>
			<td width="80%"><?php echo $select_class; ?></td>
		</tr>
		<tr><td colspan="2" height="5"></td></tr>		
		<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td colspan="2" height="5"></td></tr>
		<tr>
			<td valign="top" nowrap="nowrap" ><span class="tabletext"></td>
			<td width="80%" style="align: left">
		       	<table border="0" cellpadding="0" cellspacing="0" align="left">
					<tr> 
						<td>
		       				<?php echo $select_students; ?>
		       			</td>
		       			<td style="vertical-align:bottom">        
					        <table width="100%" border="0" cellspacing="0" cellpadding="6">
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
									</td>
								</tr>
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
									</td>
								</tr>
							</table>
		       			</td>
		       		</tr>
		       	</table>
			</td>
		</tr>
	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
<?

        $linterface->LAYOUT_STOP();
    
    
    
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>