<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
//$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");
if(!(!$ldiscipline->CASE_IS_FINISHED($CaseID) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll")) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


if(!$CaseID)
{
	header("Location: index.php");
	exit;
}

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 0);
$STEPS_OBJ[] = array($eDiscipline["FinishNotification"], 1);

# navigation bar
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_Case_Record, "view.php?CaseID=$CaseID");
$PAGE_NAVIGATION[] = array($eDiscipline["AddStudents"]);

# build html part
$xmsg = sizeof($MeritRecordID) ? "add" : "add_failed";

$record_table = "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
$record_table .= "<tr class='tabletop'>";
$record_table .= "<td width='15' align='center'>#</td>";
$record_table .= "<td>". $i_general_class ."</td>";
$record_table .= "<td>". $i_general_name ."</td>";
$record_table .= "<td>&nbsp;</td>";
$record_table .= "<td>". $eDiscipline["Record"] ."</td>";
$record_table .= "<td>". $eDiscipline["RecordItem"] ."</td>";
$record_table .= "<td width='100'>". $Lang['eDiscipline']['EventDate'] ."</td>";
$record_table .= "<td>". $i_Discipline_PIC."</td>";
$record_table .= "<td align='left'>". $eDiscipline["Action"] ."</td>";
if($sys_custom['eDisciplinev12_add_referenceNo'] || $sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_ReferenceNo']) 
	$record_table .= "<td align='left'>". $iDiscipline["ReferenceNo"] ."</td>";
$record_table .= "</tr>";

$i = 1;
foreach($MeritRecordID as $RecordID)
{
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
	$thisStudentID = $datainfo[0]['StudentID'];
	$lu = new libuser($thisStudentID);
	$thisClass = $lu->ClassName . ($lu->ClassNumber ? " - ". $lu->ClassNumber: "");
	if($datainfo[0]['MeritType']==1 || $datainfo[0]['MeritType']==-1)
	{
		$thisicon = $datainfo[0]['MeritType']==1 ? "icon_merit" : "icon_demerit";
		$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($datainfo[0]['ProfileMeritType']);
	}
	else
	{
		$thisicon = "";
		$thisMeritType = "---";
	}
	$PIC = $datainfo[0]['PICID'];
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
	$PICAry = array();
	foreach($array_PIC as $k=>$d)
		$PICAry[] = $d[1];
	$TemplateID = $datainfo[0]['TemplateID'];
	$action_str = ($TemplateID) ? $eDiscipline["SendNotice"] : "";
		
	# check have detention or not
	$DetentionInfo = $ldiscipline->getDetentionListByDemeritID($RecordID);
	$detention_str = sizeof($DetentionInfo) ? $eDiscipline['Detention'] : "";
	$action_str .= (($action_str && $detention_str)? "<br>" :"") . $detention_str;;
	$ItemText = stripslashes(intranet_htmlspecialchars($datainfo[0]['ItemText']));
	
	$record_table .= "<tr class='row_approved'>";
	$record_table .= "<td valign='top' > ". ($i++) ."</td>";
	$record_table .= "<td valign='top'>". $thisClass ."</td>";
	$record_table .= "<td valign='top'>". $lu->UserName() ."</td>";
	if($thisicon)
	{
		$record_table .= "<td width='20' valign='top'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/{$thisicon}.gif' width='20' height='20' border='0' align='absmiddle'></td>";
		$record_data = $ldiscipline->returnDeMeritStringWithNumber($datainfo[0]['ProfileMeritCount'], $thisMeritType);
		$record_table .= "<td valign='top'>". $record_data ."</td>";
		$record_table .= "<td valign='top'>". $ItemText ."</td>";
	}
	else
	{
		$record_table .= "<td width='20' valign='top'>&nbsp;</td>";
		$record_table .= "<td width='20' valign='top'>--</td>";
		$record_table .= "<td valign='top'>--</td>";
	}
	$record_table .= "<td valign='top'>". $datainfo[0]['RecordDate'] ."</td>";
	$record_table .= "<td valign='top'>". implode("<br>",$PICAry) ."</td>";
	$record_table .= "<td align='left' valign='top'>". $action_str ."&nbsp;</td>";
	if($sys_custom['eDisciplinev12_add_referenceNo'] || $sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_ReferenceNo']) {
		$ref_id = $ldiscipline->RETURN_REFERNCE_NUMBER("DISCIPLINE_MERIT_RECORD", $RecordID);
		$record_table .= "<td valign='top'>". $ref_id ."</td>";
	}
	$record_table .= "</tr>";

}
$record_table .= "</table>";

/*
#############################################################################################################
# A&P record approval/Release is based on Case Finish/Release status, so no need to process
#############################################################################################################
# Status option
if($datainfo[0]['RecordStatus'] == DISCIPLINE_STATUS_APPROVED)
{
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release"))
		$status_option = "<input name='released' type='checkbox' id='released' value='1'><label for='released'>". $i_Discipline_System_Access_Right_Release ."</label>";
}
else
{
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))
	{
			$status_option = "<input name='approved' type='checkbox' id='approved' value='1'><label for='approved'>". $i_status_approve ."</label>";
			
			if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release"))
				$status_option .= "<br> <input name='released' type='checkbox' id='released' value='1'><label for='released'>". $eDiscipline["ApproveRelease"] ."</label>";
	}
}
	
*/
# Start layout
$linterface->LAYOUT_START();

?>

<form name="form1" method="POST" action="student_new4_update.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td align='center'>
		<table align='center' width='90%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td align='right'><?=$linterface->GET_SYS_MSG($xmsg)?></td>
			</tr>
		</table>
	</td>
</tr>

<tr>
	<td align='center'>
		<table align='center' width='90%' border='0' cellpadding='0' cellspacing='0'>
			<tr>
				<td align='right'><?=$record_table?></td>
			</tr>
		</table>
	</td>
</tr>



<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<? /* 
		#############################################################################################################
		# A&P record approval/Release is based on Case Finish/Release status, so no need to process
		#############################################################################################################
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["Status"]?> -</i></td>
		</tr>	
		<tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
                <td><?=$status_option?></td>
		</tr>		
		<? */ ?>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["Notification"]?> -</i></td>
		</tr>
		<tr>
                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline["EmailTo"]?></td>
                <td>
					<input name="email_PIC" type="checkbox" id="email_PIC" value="1"><label for="email_PIC"><?=$i_Discipline_PIC?></label>
					<input name="email_ClassTeacher" type="checkbox" id="email_ClassTeacher" value="1"><label for="email_ClassTeacher"><?=$i_Teaching_ClassTeacher?></label>
					<input name="email_DisciplineAdmin" type="checkbox" id="email_DisciplineAdmin" value="1"><label for="email_DisciplineAdmin"><?=$eDiscipline["DisciplineAdmin"]?></label>
				</td>
		</tr>		
				
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_finish, "submit")?>&nbsp;
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />
<input type="hidden" name="CaseID" value="<?=$CaseID?>" />

<? foreach($MeritRecordID as $RecordID) {?>
	<input type="hidden" name="MeritRecordID[]" value="<?=$RecordID?>">
<? } ?>	
</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>