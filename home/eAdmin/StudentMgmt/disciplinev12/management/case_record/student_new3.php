<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2016-12-05	(Bill)	[2016-1129-1154-50236]
#				fixed: checkbox of Send Notice moved when selected Detention checkbox, set inline style to remove float setting
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

# Check access right
//$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");
if(!(!$ldiscipline->CASE_IS_FINISHED($CaseID) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll")) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New")))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


if(!$CaseID)
{
	header("Location: index.php");
	exit;
}

$isExist = array();
$hiddenField = "";
$success = 1;

if($ldiscipline->NotAllowSameItemInSameDay) {	# Not allow same student with same item in a same day
	foreach($student as $StudentID) {
		$thisCatID = $CatID;
		$thisItemID = $ItemID;
		$thisRecordDate = $RecordDate;
		$isExist[$StudentID] = $ldiscipline->checkSameAPItemInSameDay($StudentID, $thisItemID, $thisRecordDate);
		if($isExist[$StudentID]==1) $success = 0;
		
	}
}

if($success == 0) {	# duplicated AP record with same item on same day

	foreach($isExist as $key=>$val) {
		if($val==1)	# duplicate student ID
			$hiddenField .= "\n<input type='hidden' name='sid[]' id='sid[]' value='$key'>\n";	
	}
	$hiddenField .= "<input type='hidden' name='CaseID' id='CaseID' value='$CaseID'>\n";
	$hiddenField .= "<input type='hidden' name='semester' id='semester' value='$semester'>\n";
	$hiddenField .= "<input type='hidden' name='RecordDate' id='RecordDate' value='$RecordDate'>\n";
	$hiddenField .= "<input type='hidden' name='remark' id='remark' value='$remark'>\n";
	$hiddenField .= "<input type='hidden' name='record_type' id='record_type' value='$record_type'>\n";
	$hiddenField .= "<input type='hidden' name='CatID' id='CatID' value='$CatID'>\n";
	$hiddenField .= "<input type='hidden' name='ItemID' id='ItemID' value='$ItemID'>\n";
	$hiddenField .= "<input type='hidden' name='MeritNum' id='MeritNum' value='$MeritNum'>\n";
	$hiddenField .= "<input type='hidden' name='MeritType' id='MeritType' value='$MeritType'>\n";
	$hiddenField .= "<input type='hidden' name='ConductScore' id='ConductScore' value='$ConductScore'>\n";
	$hiddenField .= "<input type='hidden' name='StudyScore' id='StudyScore' value='$StudyScore'>\n";
	$hiddenField .= "<input type='hidden' name='temp_flag' id='temp_flag' value='$temp_flag'>\n";
	$hiddenField .= "<input type='hidden' name='semester_flag' id='semester_flag' value='$semester_flag'>\n";
	
	foreach($PIC as $p) {
		$hiddenField .= "<input type='hidden' name='PIC[]' id='PIC[]' value='$p'>\n";
	}
	$formAction = "student_new2.php";
	
	foreach($student as $StudentID) {
		$hiddenField .= "<input type='hidden' name='student[]' id='student[]' value='$StudentID'>\n";
	}
	
	echo "
	<body onLoad=\"document.form1.submit();\">
	<form name=\"form1\" method=\"POST\" action=\"$formAction\">";
	
	echo $hiddenField;
	echo "</form>
		
	<script language=\"javascript\">
		document.form1.submit();
	</script>
	</body>	";
	
	exit;
}

$remark = stripslashes(intranet_htmlspecialchars($remark));

# check semester 
//$semester_mode = getSemesterMode();
//if($semester_mode==2)
	$semester = retrieveSemester($RecordDate);
	

if(!$semester)
{
?>
	<form name="form1" method="post" action="student_new2.php">
	<input type="hidden" name="CaseID" value="<?=$CaseID?>" />
	<!-- Step 1 data //-->
	<? foreach($student as $k=>$d) { ?>
		<input type="hidden" name="student[]" value="<?=$d?>" />
	<? } ?>
	
	<!-- Step 2 data //-->
	<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
	<? foreach($PIC as $k=>$d) { ?>
		<input type="hidden" name="PIC[]" value="<?=$d?>" />
	<? } ?>
	<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
	<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
	<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
	<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
	<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
	<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
	<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
	<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
	<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
	<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />
	<input type="hidden" name="xmsg" id="xmsg" value="<?=$eDiscipline['SemesterSettingError']?>" />
	</form>
	
	<script language="javascript">
	<!--
	document.form1.submit();
	//-->
	</script>

<?	
}


$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 1);
$STEPS_OBJ[] = array($eDiscipline["FinishNotification"], 0);

# navigation bar
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_Case_Record, "view.php?CaseID=$CaseID");
$PAGE_NAVIGATION[] = array($eDiscipline["NewRecord"], "");

# get data from preious page
$student = $_POST['student'];
if(empty($student))	header("Location: new1.php");
$PIC = array_unique($PIC);

# build html part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
foreach($student as $k=>$StudentID)
{
	$lu = new libuser($StudentID);
	$student_name = $lu->UserNameLang();
	$class_name = $lu->ClassName;
	$class_no = $lu->ClassNumber;

	$selected_student_table .= "<tr><td width='50%' valign='top'><a href='javascript:viewCurrentRecord($StudentID)' class='tablelink'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle'>";
	$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
	$selected_student_table .= "</a></td></tr>";
}
$selected_student_table .= "</table>";

########## For Detention (Begin) ##########

		if ($_POST["submit_flag"] == "YES") {

			if ($detentionFlag == "YES") {

			// Get submitted data
			$submitStudent = $student;
			$submitDetentionCount0 = $_POST["DetentionCount0"];

			for ($k=1; $k<=$submitDetentionCount0; $k++) {
				${"submitSelectSession0_".$k} = $_POST["SelectSession0_".$k];
			}
			$submitTextRemark0 = $_POST["TextRemark0"];
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				${"submitDetentionCount".$l} = $_POST["DetentionCount".$l];
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					${"submitSelectSession".$l."_".$m} = $_POST["SelectSession".$l."_".$m];
					if (${"submitSelectSession".$l."_".$m} <> "AUTO" && ${"submitSelectSession".$l."_".$m} <> "LATER") {
						$selectedSessionForOneStudent[$l][] = ${"submitSelectSession".$l."_".$m};
						$allSelectedSession[] = ${"submitSelectSession".$l."_".$m};
					}
				}
				${"submitTextRemark".$l} = $_POST["TextRemark".$l];
			}


			// Check past session and available session
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				$assignedSessionForEachStudent = array();
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					if (${"submitSelectSession".$l."_".$m} == "AUTO") {
						$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $submitStudent[$l-1], "ASC");
						$TmpDetentionID = "";
						if (is_array($allSelectedSession)) {
							$TmpSessionArrayCount = array_count_values($allSelectedSession);
						}
						$AutoSuccess = false;
						for ($i=0; $i<sizeof($result); $i++) {
							list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $result[$i];
							if (!(is_array($selectedSessionForOneStudent[$l]) && in_array($TmpDetentionID, $selectedSessionForOneStudent[$l])) && ($TmpSessionArrayCount[$TmpDetentionID] < $TmpAvailable) && !($ldiscipline->checkStudentInSession($submitStudent[$l-1], $TmpDetentionID))) {
								$AutoSuccess = true;
								break;
							}
						}
						if ($TmpDetentionID == "" || $AutoSuccess == false) {
							$AvailableFlag[$l][$m] = "NO";
						} else {
							$AvailableFlag[$l][$m] = "YES";
							$selectedSessionForOneStudent[$l][] = $TmpDetentionID;
							$allSelectedSession[] = $TmpDetentionID;
							$AutoDetentionID[$l][$m] = $TmpDetentionID;
						}
					} else if (${"submitSelectSession".$l."_".$m} != "LATER") {
						$StudentInSessionFlag[$l][$m] = ($ldiscipline->checkStudentInSession($submitStudent[$l-1], ${"submitSelectSession".$l."_".$m}))?"YES":"NO";
						$PastFlag[$l][$m] = ($ldiscipline->checkPastByDetentionID(${"submitSelectSession".$l."_".$m}))?"YES":"NO";
						if (is_array($assignedSession)) {		// array for sessions that are not assigned by AUTO
							$TmpAssignedSessionArrayCount = array_count_values($assignedSession);
						}
						if (in_array(${"submitSelectSession".$l."_".$m}, $assignedSessionForEachStudent)) {
							$StudentInSessionFlag[$l][$m] = "YES";
						}
						$FullFlag[$l][$m] = ($ldiscipline->checkFullByDetentionID(${"submitSelectSession".$l."_".$m}, $TmpAssignedSessionArrayCount[${"submitSelectSession".$l."_".$m}] + 1))?"YES":"NO";
						if ($StudentInSessionFlag[$l][$m] == "NO" && $PastFlag[$l][$m] == "NO" && $FullFlag[$l][$m] == "NO") {
							$assignedSession[] = ${"submitSelectSession".$l."_".$m};
							$assignedSessionForEachStudent[] = ${"submitSelectSession".$l."_".$m};
						}
					}
				}
			}

			$FailFlag = false;

			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
				$allAvailableFlag[] = $AvailableFlag[$l][$m];
				$allStudentInSessionFlag[] = $StudentInSessionFlag[$l][$m];
				$allPastFlag[] = $PastFlag[$l][$m];
				$allFullFlag[] = $FullFlag[$l][$m];
				}
			}
			if (!((is_array($allAvailableFlag) && in_array("NO", $allAvailableFlag)) || (is_array($allStudentInSessionFlag) && in_array("YES", $allStudentInSessionFlag)) || (is_array($allPastFlag) && in_array("YES", $allPastFlag)) || (is_array($allFullFlag) && in_array("YES", $allFullFlag)))) {
				for ($l=1; $l<=sizeof($submitStudent); $l++) {
					for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
						// StudentID
						$DetentionArr[] = $submitStudent[$l-1];
						// DetentionID
						if (${"submitSelectSession".$l."_".$m}=="AUTO") {
							$DetentionArr[] = $AutoDetentionID[$l][$m];
						} else if (${"submitSelectSession".$l."_".$m}=="LATER") {
							$DetentionArr[] = "";
						} else {
							$DetentionArr[] = ${"submitSelectSession".$l."_".$m};
						}
						// Remark
						$DetentionArr[] = ${"submitTextRemark".$l};
					}
				}
				
				

				
?>
<body onLoad="document.form2.submit();">
<form name="form2" method="post" action="student_new3_update.php">
<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>" />

<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />
<? } ?>

<!-- Step 2 data //-->
<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
<? foreach($PIC as $k=>$d) { ?>
	<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
<? } 
?>
<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />

<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<? foreach($DetentionArr as $k=>$d) { ?>
	<input type="hidden" name="DetentionArr[]" id="DetentionArr[]" value="<?=intranet_htmlspecialchars($d)?>" />
<? } ?>
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
<? for ($j=1; $j<=sizeof($student); $j++) { ?>
	<input type="hidden" name="StudentID<?=$j?>" id="StudentID<?=$j?>" value="<?=${"StudentID".$j}?>" />
	<input type="hidden" name="StudentName<?=$j?>" id="StudentName<?=$j?>" value="<?=${"StudentName".$j}?>" />
	<input type="hidden" name="SelectNotice<?=$j?>" id="SelectNotice<?=$j?>" value="<?=${"SelectNotice".$j}?>" />
	<input type="hidden" name="TextAdditionalInfo<?=$j?>" id="TextAdditionalInfo<?=$j?>" value="<?=intranet_htmlspecialchars(${"TextAdditionalInfo".$j})?>" />
<? } ?>

</form>
</body>

<?
			} else {
				$FailFlag = true;
			}

			} else {		// $detentionFlag != "YES"
?>
<body onLoad="document.form3.submit();">
<form name="form3" method="post" action="student_new3_update.php">
<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>" />


<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />
<? } ?>

<!-- Step 2 data //-->
<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
<? foreach($PIC as $k=>$d) { ?>
	<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
<? } ?>
<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />

<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
<? for ($j=1; $j<=sizeof($student); $j++) { ?>
	<input type="hidden" name="StudentID<?=$j?>" id="StudentID<?=$j?>" value="<?=${"StudentID".$j}?>" />
	<input type="hidden" name="StudentName<?=$j?>" id="StudentName<?=$j?>" value="<?=${"StudentName".$j}?>" />
	<input type="hidden" name="SelectNotice<?=$j?>" id="SelectNotice<?=$j?>" value="<?=${"SelectNotice".$j}?>" />
	<input type="hidden" name="TextAdditionalInfo<?=$j?>" id="TextAdditionalInfo<?=$j?>" value="<?=intranet_htmlspecialchars(${"TextAdditionalInfo".$j})?>" />
<? } ?>

</form>
</body>
<?
			}
		}		// end of submit_flag = "YES"
$session_times = $ldiscipline->DetentinoSession_MAX ? $ldiscipline->DetentinoSession_MAX : 10;
for ($i = 1; $i <= $session_times; $i++) $OptionForDetentionCount .= "<option value=\"$i\">$i</option>";

		function getSelectSessionString($parIndex="", $parSessionNumber="", $parUserID="") {
			global $ldiscipline, $i_Discipline_Auto_Assign_Session, $i_Discipline_Put_Into_Unassign_List, $i_Discipline_All_Forms;
			global $i_Discipline_Date, $i_Discipline_Time, $i_Discipline_Location, $i_Discipline_Form, $i_Discipline_Vacancy, $i_Discipline_PIC;
			
			$TmpAllForms = $ldiscipline->getAllFormString();
			if ($parUserID == "") {
				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");
			} else {
				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $parUserID, "ASC");
			}
			$SelectSession = "<table><tr><td>";
			$SelectSession .= "<select name=\"SelectSession".$parIndex."_".$parSessionNumber."\" id=\"SelectSession".$parIndex."_".$parSessionNumber."\" class=\"formtextbox\">";
			if (sizeof($result)>0) {
				$SelectSession .= "<option class=\"row_avaliable\" value=\"AUTO\">".$i_Discipline_Auto_Assign_Session."</option>";
			}
			$SelectSession .= "<option class=\"row_avaliable\" value=\"LATER\" selected>".$i_Discipline_Put_Into_Unassign_List."</option>";
			$label = $ldiscipline->displayDetentionSessionDefaultString();
			$SelectSession .= "<optgroup label=\"$label\">";

			for ($i=0; $i<sizeof($result); $i++) {
				list($TmpDetentionID[$i],$TmpDetentionDate[$i],$TmpDetentionDayName[$i],$TmpStartTime[$i],$TmpEndTime[$i],$TmpPeriod[$i],$TmpLocation[$i],$TmpForm[$i],$TmpPIC[$i],$TmpVacancy[$i],$TmpAssigned[$i],$TmpAvailable[$i]) = $result[$i];

				if ($TmpForm[$i] == $TmpAllForms) $TmpForm[$i] = $i_Discipline_All_Forms;
				$SessionStr = $ldiscipline->displayDetentionSessionString($TmpDetentionID[$i]);
				$SelectSession .= "<option class=\"row_avaliable\" value=\"".$TmpDetentionID[$i]."\">$SessionStr</option>";
			}

			$SelectSession .= "</optgroup>";
			$SelectSession .= "</select>";
			$SelectSession .= "</td><td>";
			$SelectSession .= "<span id=\"SpanSysMsg".$parIndex."_".$parSessionNumber."\"></span>";
			$SelectSession .= "</td></tr></table>";

			return $SelectSession;
		}

		// Loop for individual student
		$TableIndStudent = "";
		for ($i=0; $i<sizeof($student); $i++) {
			$resultStudentName = $ldiscipline->getStudentNameByID($student[$i]);
			list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
			$TableIndStudent .= "<br />";
			$TableIndStudent .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
			$TableIndStudent .= "<tr>";
			$TableIndStudent .= "<td colspan=\"2\" valign=\"top\" nowrap=\"nowrap\">".($i+1).".";
			$TableIndStudent .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
			$TableIndStudent .= "<span class=\"sectiontitle\">".$TmpClassName."-".$TmpClassNumber." ".$TmpName."</span>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr valign=\"top\" class=\"tablerow1\">";
			$TableIndStudent .= "<td nowrap=\"nowrap\" class=\"formfieldtitle\">";
			$TableIndStudent .= "<span class=\"tabletext\">".$i_Discipline_Detention_Times." <span class=\"tabletextrequire\">*</span></span>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "<td width=\"80%\">";
			$TableIndStudent .= "<input type=\"hidden\" id=\"HiddenDetentionCount".($i+1)."\" name=\"HiddenDetentionCount".($i+1)."\" value=\"1\" />";
			$TableIndStudent .= "<select name=\"DetentionCount".($i+1)."\" id=\"DetentionCount".($i+1)."\" onChange=\"javascript:updateSessionCell(".($i+1).");\">".$OptionForDetentionCount."</select>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr class=\"tablerow1\">";
			$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Session." <span class=\"tabletextrequire\">*</span></td>";
			$TableIndStudent .= "<td valign=\"top\"><table name=\"TableSession".($i+1)."\" id=\"TableSession".($i+1)."\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td width=\"15\" nowrap>1.</td><td nowrap>".getSelectSessionString($i+1, 1, $TmpUserID)."</td></tr></table></td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr class=\"tablerow1\">";
			$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Remark."</td>";
			$TableIndStudent .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextRemark".($i+1), "")."</td>";
			$TableIndStudent .= "</tr>";
			if ($i != sizeof($student)-1) {
				$TableIndStudent .= "<tr>";
				$TableIndStudent .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td>";
				$TableIndStudent .= "</tr>";
			}
			$TableIndStudent .= "</table>";
		}

		// Preset detention session (for javascript)
		$jSessionString = "var SelectSessionArr=new Array(".(sizeof($student)+1).");\n";
		for ($k=0; $k<=sizeof($student); $k++) {
			$jSessionString .= "SelectSessionArr[".$k."]=new Array(10);\n";
		}
		for ($i=0; $i<=sizeof($student); $i++) {
			for ($j=0; $j<10; $j++) {
				$jSessionString .= "SelectSessionArr[".$i."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($i, $j+1, ($i==0)?"":$student[$i-1]))."';\n";
			}
		}

		// Initial form after submit fails (for javascript)
		if ($FailFlag) {
			$SessionNAMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_not_available")));
			$SessionPastMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_past")));
			$SessionFullMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_full")));
			$SessionAssignedBeforeMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_assigned_before")));

			$jInitialForm = "";
			for ($l=0; $l<=sizeof($submitStudent); $l++) {
				$jInitialForm .= "if(document.getElementById('DetentionCount".$l."')) document.getElementById('DetentionCount".$l."').value='".${"submitDetentionCount".$l}."';\n";
				$jInitialForm .= "if(document.getElementById('HiddenDetentionCount".$l."')) document.getElementById('HiddenDetentionCount".$l."').value='1';\n";
				$jInitialForm .= "updateSessionCell(".$l.");\n";
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					$jInitialForm .= "if(document.getElementById('SelectSession".$l."_".$m."')) document.getElementById('SelectSession".$l."_".$m."').value='".${"submitSelectSession".$l."_".$m}."';\n";
					if ($AvailableFlag[$l][$m] == "NO") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionNAMsg."';\n";
					} else if ($FullFlag[$l][$m] == "YES") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionFullMsg."';\n";
					} else if ($PastFlag[$l][$m] == "YES") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionPastMsg."';\n";
					} else if ($StudentInSessionFlag[$l][$m] == "YES") {
						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionAssignedBeforeMsg."';\n";
					}
				}
				$jInitialForm .= "if(document.getElementById('TextRemark".$l."')) document.getElementById('TextRemark".$l."').value='".${"submitTextRemark".$l}."';\n";
			}
		}

########## For Detention (End) ##########

########## For eNotice (Begin) ##########
# check template is ava
$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1);
if (!$lnotice->disabled && $record_type!=0 && !empty($NoticeTemplateAva))
	{
		$enotice_checkbox = '<input name="action_notice" type="checkbox" id="action_notice" value="1" onClick="javascript:if(this.checked){document.getElementById(\'noticeFlag\').value=\'YES\';showDiv(\'divNotice\');}else{document.getElementById(\'noticeFlag\').value=\'NO\';hideDiv(\'divNotice\');}" /><label for="action_notice">'.$eDiscipline["SendNoticeWhenReleased"].'</label><br>';
		$catTmpArr = $ldiscipline->TemplateCategory();
		if(is_array($catTmpArr))
		{
			$catArr[0] = array("0","-- $button_select --");
			foreach($catTmpArr as $Key=>$Value)
			{
				# check the Template Catgory has template or not
				$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
				if(!empty($NoticeTemplateAvaTemp))
					$catArr[] = array($Key,$Key);
			}
		}

		for ($i=0; $i<=sizeof($student); $i++) {
			${"catSelection".$i} = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID'.$i.'", name="CategoryID'.$i.'" onChange="changeCat(this.value, '.$i.')"', "", $CategoryID);
			if ($i > 0) {
				$resultStudentName = $ldiscipline->getStudentNameByID($student[$i-1]);
				list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
				$TableNotice .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td colspan=\"2\" valign=\"top\" nowrap>\n";
				$TableNotice .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td>$i. <img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$TmpClassName-$TmpClassNumber $TmpName</span></td>\n";
				$TableNotice .= "<td align=\"right\"><span class=\"sectiontitle\">\n";
				$TableNotice .= $linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice($i);");
				$TableNotice .= "</span></td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "</table>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr valign=\"top\" class=\"tablerow1\">\n";
				$TableNotice .= "<td width=\"20%\" nowrap class=\"formfieldtitle\"><span class=\"tabletext\">$i_Discipline_Usage_Template <span class=\"tabletextrequire\">*</span></span></td>\n";
				$TableNotice .= "<td>\n";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentID$i\" name=\"StudentID$i\" value=\"".$TmpUserID."\" />";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentName$i\" name=\"StudentName$i\" value=\"".$TmpName."\" />";
				$TableNotice .= ${"catSelection".$i}." <select name=\"SelectNotice$i\" id=\"SelectNotice$i\"><option value=\"0\">-- $button_select --</option></select>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr class=\"tablerow1\">\n";
				$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">$i_Discipline_Additional_Info</td>\n";
				$TableNotice .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextAdditionalInfo".($i), "")."</td>";
				$TableNotice .= "</tr>\n";
				if ($i != sizeof($student)) {
					$TableNotice .= "<tr>\n";
					$TableNotice .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>\n";
					$TableNotice .= "</tr>\n";
				}
				$TableNotice .= "</table>\n";
			}
		}


		// Preset template items (for javascript)
		for ($i=0; $i<sizeof($catArr); $i++) {
			$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
			for ($j=0; $j<=sizeof($result); $j++) {
				if ($j==0) {
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
				} else {
					$tempTemplate = $result[$j-1][1];
					$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
			        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
			        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
			        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
        			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);
        
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
				}
			}
		}
	}
	if ($lnotice->disabled)
	{
		//$enotice_checkbox = $eDiscipline["EnoticeDisabledMsg"];	
		$enotice_checkbox = '<input name="action_notice" type="checkbox" id="action_notice" disabled /><label for="action_notice">'.$eDiscipline["SendNoticeWhenReleased"].'</label><br><br>('.$eDiscipline["EnoticeDisabledMsg"].')<br>';
	}

########## For eNotice (End) ##########


if (!($submit_flag == "YES" && $FailFlag == false))
{


# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
<?=$jSessionString?>
<?=$jTemplateString?>

function viewCurrentRecord(id)
{
         newWindow('/home/eAdmin/StudentMgmt/disciplinev12/management/award_punishment/viewcurrent.php?StudentID='+id, 8);
}

function showDiv(div)
{
	document.getElementById(div).style.display="inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display="none";
}

function updateSessionCell(studentIndex){
	if(studentIndex==undefined) studentIndex='0';

	if(document.getElementById('HiddenDetentionCount'+studentIndex))
	{
		var oldCount=parseInt(document.getElementById('HiddenDetentionCount'+studentIndex).value);
		var newCount=parseInt(document.getElementById('DetentionCount'+studentIndex).value);
	}
	
	if(oldCount<newCount){
		var newRow, newCell;
		for(var i=oldCount;i<newCount;i++){
			newRow=document.getElementById('TableSession'+studentIndex).insertRow(document.getElementById('TableSession'+studentIndex).rows.length);
			newCell0=newRow.insertCell(0);
			newCell1=newRow.insertCell(1);
			newCell0.innerHTML=(i+1)+'.';
			newCell1.setAttribute("noWrap", true);
			newCell1.innerHTML=SelectSessionArr[studentIndex][i];
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	} else if(oldCount>newCount){
		for(var i=oldCount;i>newCount;i--){
			document.getElementById('TableSession'+studentIndex).deleteRow(i-1);
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	}
}

function applyToAll(studentCount){
	for(var i=1;i<=studentCount;i++){
		document.getElementById('DetentionCount'+i).value = document.getElementById('DetentionCount0').value;
		updateSessionCell(i);
		for(var j=1;j<=document.getElementById('DetentionCount0').value;j++){
			document.getElementById('SelectSession'+i+'_'+j).value = document.getElementById('SelectSession0_'+j).value;
			if (document.getElementById('SelectSession'+i+'_'+j).value != document.getElementById('SelectSession0_'+j).value){
				document.getElementById('SelectSession'+i+'_'+j).value = 'AUTO';
			}
		}
		document.getElementById('TextRemark'+i).value = document.getElementById('TextRemark0').value;
	}
}

function changeCat(cat, selectIdx){
	var x=document.getElementById("SelectNotice"+selectIdx);

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}

	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}

	for (var j=0; j<tmpCatLength; j++) {
		var y=document.createElement('option');
		y.text=eval("jArrayTemplate"+cat)[j];
		y.value=eval("jArrayTemplate"+cat+"Value")[j];

		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}
}

function previewNotice(studentIdx){
	var templateID = document.getElementById("SelectNotice"+studentIdx).value;
	var studentName = '';
	if(studentIdx!=0) {
		studentName = document.getElementById("StudentName"+studentIdx).value;
	}
	var additionInfo = document.getElementById("TextAdditionalInfo"+studentIdx).value;
	newWindow("preview_notice.php?tid="+templateID+"&pv_studentname="+studentName+"&pv_additionalinfo="+additionInfo);
}

function applyNoticeToAll(studentCount){
	for(var i=1;i<=studentCount;i++){
		changeCat(document.getElementById('CategoryID0').value, i);
		document.getElementById('CategoryID'+i).value = document.getElementById('CategoryID0').value;
		document.getElementById('SelectNotice'+i).value = document.getElementById('SelectNotice0').value;
		document.getElementById('TextAdditionalInfo'+i).value = document.getElementById('TextAdditionalInfo0').value;
	}
}

function checkForm()
{
	<? 
	if (!$lnotice->disabled && $record_type!=0 && !empty($NoticeTemplateAva))
	{?>	
	if (document.getElementById('action_notice').checked) {
		for(var i=1;i<=<?=sizeof($student)?>;i++){
			if (document.getElementById('SelectNotice'+i).value=='0') {
				alert('<?=$i_alert_pleaseselect.$i_Discipline_Template?>');
				return false;
			}
		}
	}
	<? } ?>
	document.getElementById('submit_flag').value='YES';
	return true;
}
//-->
</script>
<form name="form1" method="POST" action="student_new3.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_general_students_selected?></td>
			<td valign="top" class="tablerow2"><?=$selected_student_table?></td>
		</tr>
		<? if($record_type==-1 or ($record_type==1 && $record_type!=0)) {?>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["Action"]?> -</i></td>
		</tr>
		<? } ?>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"></td>
			<td>
<? if($record_type==-1) {?>
				<input name="action_detention" type="checkbox" id="action_detention" value="1" onClick="javascript:if(this.checked){document.getElementById('detentionFlag').value='YES';showDiv('divDetention');}else{document.getElementById('detentionFlag').value='NO';hideDiv('divDetention');}" /><label for="action_detention"><?=$eDiscipline['Detention']?></label><br>
<div id="divDetention" style="display:none">
	<fieldset class="form_sub_option" style="float:none">
		<? if(sizeof($student)>1) {?>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
			<tr>
				<td colspan="2" valign="top" nowrap="nowrap">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_section.gif" width="20" height="20" align="absmiddle">
								<span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span>
							</td>
							<td align="right">
								<span class="sectiontitle">
<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyToAll(".sizeof($student).");")?>
								</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr valign="top">
				<td nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$i_Discipline_Detention_Times?> <span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%">
					<input type="hidden" id="HiddenDetentionCount0" name="HiddenDetentionCount0" value="1" />
					<select name="DetentionCount0" id="DetentionCount0" onChange="javascript:updateSessionCell();"><?=$OptionForDetentionCount?></select>
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Session?> <span class="tabletextrequire">*</span></td>
				<td valign="top"><table name="TableSession0" id="TableSession0" width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td width="15" nowrap>1.</td><td nowrap><?=getSelectSessionString(0, 1, "")?></td></tr></table></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
				<td valign="top"><?=$linterface->GET_TEXTAREA("TextRemark0", "")?></td>
			</tr>
		</table>
		<? } ?>
<?=$TableIndStudent?>
	</fieldset>
</div>
<? } ?>

<?=$enotice_checkbox?>

<div id="divNotice" style="display:none">
	<fieldset class="form_sub_option">
	<? if(sizeof($student)>1) {?>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
					<tr>
						<td colspan="2" valign="top" nowrap="nowrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle"><span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span></td>
									<td align="right"><span class="sectiontitle">
<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyNoticeToAll(".sizeof($student).");")?>&nbsp;
<?=$linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice(0);")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr valign="top">
						<td width="20%" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Template?> <span class="tabletextrequire">*</span></span></td>
						<td>
<?=$catSelection0?> <select name="SelectNotice0" id="SelectNotice0"><option value="0"><?="-- $button_select --"?></option></select>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Additional_Info?></td>
						<td valign="top"><textarea name="TextAdditionalInfo0" rows="2" wrap="virtual" class="textboxtext"></textarea></td>
					</tr>
				</table>
				<br />
				<? } ?>
<?=$TableNotice?>
	</fieldset>
</div>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit", "return checkForm();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "this.form.action='student_new2.php';this.form.submit();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='view_student_involved.php?CaseID=$CaseID'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>" />


<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />
<? } ?>

<!-- Step 2 data //-->
<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
<? foreach($PIC as $k=>$d) { ?>
	<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
<? } ?>
<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />

<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />

</form>
<?
if ($FailFlag) {
?>
<script language="javascript">
<?=$jInitialForm?>
	<? if ($detentionFlag == "YES") { ?>
document.getElementById('action_detention').checked=true;
showDiv('divDetention');
	<? } ?>
	<? if ($noticeFlag == "YES") { ?>
document.getElementById('action_notice').checked=true;
showDiv('divNotice');
		<? for ($j=0; $j<=sizeof($student); $j++) { ?>
changeCat("<?=${"CategoryID".$j}?>", <?=$j?>);
if(document.getElementById('CategoryID<?=$j?>')) document.getElementById('CategoryID<?=$j?>').value = "<?=${"CategoryID".$j}?>";
if(document.getElementById('SelectNotice<?=$j?>')) document.getElementById('SelectNotice<?=$j?>').value = "<?=${"SelectNotice".$j}?>";
if(document.getElementById('TextAdditionalInfo<?=$j?>')) document.getElementById('TextAdditionalInfo<?=$j?>').value = "<?=${"TextAdditionalInfo".$j}?>";
		<? } ?>
	<? } ?>
</script>
<?
}

$linterface->LAYOUT_STOP();

}		// End of !($submit_flag == "YES" && $FailFlag == false)

intranet_closedb();
?>
