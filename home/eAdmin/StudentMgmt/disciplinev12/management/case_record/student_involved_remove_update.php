<?php


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
/*
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteOwn")))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/
if(!(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll"))))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
		exit();
}

$IDs = array();
if(is_array($RecordID))
	$IDs = $RecordID;
else
	$IDs[] = $RecordID;


foreach($IDs as $id)
{
	# check access right
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteAll") || ($ldiscipline->isCasePIC($CaseID) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteOwn")))
	{
		# delete CASE A&P records
		$ldiscipline->DELETE_CASE_AP_RECORD($id, $CaseID);
	}
}

intranet_closedb();
header("Location: view_student_involved.php?CaseID=$CaseID&xmsg=delete");
?>
