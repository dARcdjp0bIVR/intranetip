<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-Finish");


if($CaseID!='' and $status!='')
{
	########################################################################################
	# If case is finished, then all the students' a&p records are approved
	# yat woon @ 20081222
	########################################################################################
	if($status == DISCIPLINE_CASE_RECORD_FINISHED)
		$ldiscipline->SET_CASE_FINISH($CaseID);
	else
		$ldiscipline->SET_CASE_PROCESSING($CaseID);
}

intranet_closedb();
header("Location: view.php?CaseID=$CaseID&xmsg=update");
?>
