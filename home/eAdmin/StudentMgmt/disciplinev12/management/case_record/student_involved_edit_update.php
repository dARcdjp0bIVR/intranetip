<?php
// Modifying by: yat

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
if(!(($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll")) && ($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll")))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

if(!$CaseID)
{
	header("Location: index.php");
	exit;
}

$lteaching = new libteaching();

# check user can access/edit this item or not
$CatID = $ldiscipline->returnCatIDByItemID($ItemID);
$AccessItem = 0;
if($ldiscipline->IS_ADMIN_USER($UserID) || !$ItemID)
{
	$AccessItem = 1;
}
else
{
	$ava_item = $ldiscipline->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
	if($ava_item[$CatID])
		$AccessItem = in_array($ItemID, $ava_item[$CatID]);
}

### retrieve extra data
$yearTermInfo = getAcademicYearInfoAndTermInfoByDate($RecordDate);
list($yearID, $year, $termID, $term) = $yearTermInfo;
/*
# School Year
$school_year = GET_ACADEMIC_YEAR3($RecordDate);

# Semester
$semester_mode = getSemesterMode();
if($semester_mode==2)
	$semester = retrieveSemester($RecordDate);
*/
# Item Text
$ItemInfo = $ldiscipline->returnMeritItemByID($ItemID);
$ItemText = $ItemInfo['ItemName'];
$ItemText = str_replace("'","\'",$ItemText);
$ItemText = str_replace('"','\"',$ItemText);
# ConductScoreChange
$ConductScoreChange = $ConductScore * $record_type;
# SubScoreChange
$StudyScoreChange = $StudyScore * $record_type;

# Subject
if($SubjectID) 
	$SubjectName = $lteaching->returnSubjectName($SubjectID);
# PIC
$PIC = implode(",",$PIC);
$remark = intranet_htmlspecialchars($remark);
$MeritRecordIDAry = array();

$lu = new libuser($StudentID);
$thisClassNumber = $lu->ClassNumber;
$thisClassName = $lu->ClassName;
//$MeritNum = $MeritNum * $record_type;


### insert into DISCIPLINE_MERIT_RECORD 
$dataAry = array();
$dataAry['RecordDate'] = $RecordDate;
$dataAry['Year'] = $year;
$dataAry['AcademicYearID'] = $yearID;
$dataAry['Semester'] = $term;
$dataAry['YearTermID'] = $termID;
$dataAry['RecordID'] = $RecordID;
if($AccessItem) 
{
	$dataAry['ItemID'] = $ItemID;
	$dataAry['ItemText'] = $ItemText;
	$dataAry['MeritType'] = $record_type;
	
	if($record_type!=0)
	{
		$dataAry['ProfileMeritType'] = $MeritType;
		$dataAry['ProfileMeritCount'] = $MeritNum;
	}
	else
	{
		$dataAry['ProfileMeritType'] = "";
		$dataAry['ProfileMeritCount'] = "";
	}
	
	$dataAry['ConductScoreChange'] = $ConductScoreChange;
	$dataAry['SubScore1Change'] = $StudyScoreChange;
}
$dataAry['Remark'] = $remark;
$dataAry['Subject'] = $SubjectName;
$dataAry['SubjectID'] = $SubjectID;
$dataAry['PICID'] = $PIC;
if($action_notice==1) {
	$dataAry['TemplateID'] = $SelectNotice0;
	$dataAry['TemplateOtherInfo'] = $TextAdditionalInfo0;
} else {
	$dataAry['TemplateID'] = "";
	$dataAry['TemplateOtherInfo'] = "";
}

$MeritRecordID = $ldiscipline->UPDATE_MERIT_RECORD($dataAry, $RecordID);

intranet_closedb();

header("Location: view_student_involved.php?msg=update&CaseID=$CaseID");

?>
