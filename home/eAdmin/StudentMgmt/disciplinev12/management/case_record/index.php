<?php
# modifying by : 

############ Change Log Start #############################
#
#   Date    :   2020-11-24 Bill     [IP30 DM#1014]
#               updated semester selection sorting
#
#   Date    :   2020-10-29 (Bill)
#               change to POST method + minor bug fix
#
#	Date    :	2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added remarks for prefix style of deleted teacher account
#
#	Date	:	2015-07-28 (Evan)
#				fix bug of search bar with extra slashes
#
#	Date	:	2015-07-17 (Evan)
#				Update style of search bar
#
#	Date	:	2011-03-24 (Henry Chow)
#				PIC selection menu only display the staff name of selected semester
# 
#	Date	: 	2011-01-27 (Henry Chow)
#				count the "Student Involved" with checking on student status 
# 
#	Date	: 	2011-01-18 (Henry Chow)
#				synchronized with "Overview" page, will not count in "Student Involved" if student is deleted or not assigned to any classes
# 
#	Date	: 	2010-05-07 YatWoon
#				According to the setting $PIC_SelectionDefaultOwn to check PIC default selected value
#
############ Change Log End #############################

if ($page_size_change==1)	# change "num per page"
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}

if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
}
else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}

if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
}
else if (!isset($field) || $ck_approval_page_field=="")
{
	$field = $sortEventDate;
}

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");

# prepare year array for generating year selection box
if($Year == '')
{
    $yearName = getCurrentAcademicYear();
    $Year = Get_Current_Academic_Year_ID();
}

// [IP30 DM#1014]
//$sql = "select distinct AcademicYearID from DISCIPLINE_CASE where trim(Year)!='' and Year is not null AND AcademicYearID!='' AND AcademicYearID is not null";
$sql = "SELECT 
                DISTINCT dc.AcademicYearID 
        FROM 
                DISCIPLINE_CASE dc
                LEFT OUTER JOIN ACADEMIC_YEAR ay ON (ay.AcademicYearID = dc.AcademicYearID)
        WHERE 
                TRIM(dc.Year) != '' AND 
                dc.Year IS NOT NULL AND 
                dc.AcademicYearID != '' AND 
                dc.AcademicYearID IS NOT NULL
        ORDER BY
                ay.Sequence";
$tmpYearArr = $ldisciplinev12->returnVector($sql);
if(is_array($tmpYearArr))
{
    $yearArr[] = array('0',$i_Attendance_AllYear);
    foreach($tmpYearArr as $Key=>$Value)
    {
        $yearArr[] = array($Value, $ldisciplinev12->getAcademicYearNameByYearID($Value));
    }
}

$currentYearExist = 0;
for($i=0; $i<sizeof($yearArr); $i++) {
    if($yearArr[$i][0]==$Year) {
        $currentYearExist = 1;
        break;
    }
}
if($currentYearExist==0) {
    //$yearArr[] = array($ldisciplinev12->getAcademicYearIDByYearName($yearName), getCurrentAcademicYear());
    $yearArr[] = array(Get_Current_Academic_Year_ID(), getCurrentAcademicYear());
}

# prepare semester array for generating semester selection box
/*
if(!isset($Semester))
{
	$Semester = getCurrentSemester();
}
*/
/*
$semester_data = getSemesters();
debug_r($semester_data);
$number = sizeof($semester_data);
$semArr[] = array('',$ec_iPortfolio['whole_year']);
for ($i=0; $i<$number; $i++)
{
	$linedata[]= split("::",$semester_data[$i]);
	$semArr[] = array($linedata[$i][0],$linedata[$i][0]);
}
*/

//$currentYearID = $ldisciplinev12->getAcademicYearIDByYearName($Year);

$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$Year' ORDER BY TermStart";
$semResult = $ldisciplinev12->returnArray($sql, 3);
$semArr[] = array(0,$i_Discipline_System_Award_Punishment_Whole_Year);
for($i=0; $i<sizeof($semResult); $i++) {
    list($id, $nameEN, $nameB5) = $semResult[$i];
    $semName = Get_Lang_Selection($nameB5, $nameEN);
    $semArr[] = array($semResult[$i][0],$semName);
}

# prepare class array for generating class selection box
$sql = "select distinct ClassName from INTRANET_CLASS";
$tmpClassArr = $ldisciplinev12->returnArray($sql);
if(is_array($tmpClassArr))
{
    $classArr[] = array('',$i_general_all_classes);
    foreach($tmpClassArr as $Key=>$Value)
    {
        $classArr[] = array($Value[0],$Value[0]);
    }
}
if($Processing) $ProcessingChecked = "Checked";
if($Finished) $FinishedChecked = "Checked";

$yearSelection = $linterface->GET_SELECTION_BOX($yearArr, 'id="Year", name="Year" onChange="document.form1.Semester.value=\'\';document.form1.submit()"', "", $Year);
$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester" onChange="document.form1.submit()"', "", $Semester);

$statusSelection = '<span id="status_click"><div class="selectbox_group"> <a href="javascript:showStatus();" onClick="MM_showHideLayers(\'status_option\',\'\',\'show\')">'.$i_Discipline_System_Award_Punishment_Select_Status.'</a></div></span>';
$statusDiv = '<div id="status_option" class="selectbox_layer" style="width:37%;visibility: hidden; z-index:1;">
				<table width="100%" border="0" cellspacing="0" cellpadding="3">
					<tr>
						<td>
						    <input type="checkbox" name="Processing" id="Processing" '.$ProcessingChecked.'>
							'.$i_Discipline_System_Discipline_Case_Record_Processing.'<br>
							<input type="checkbox" name="Finished" id="Finished" '.$FinishedChecked.'>
							'.$i_Discipline_System_Discipline_Case_Record_Finished.'
                        </td>
					</tr>
					<tr>
						<td align="left" valign="top" class="dotline" height="2"><img src="{$image_path}/{$LAYOUT_SKIN}/10x10.gif" height="2"></td>
					<tr>
						<td align="center"><span class="tabletext">
							'.$linterface->GET_BTN($button_apply,'submit','MM_showHideLayers(\'status_option\',\'\',\'hide\')').'
							'.$linterface->GET_BTN($button_cancel,'button','MM_showHideLayers(\'status_option\',\'\',\'hide\')').'
						</span></td>
					</tr>
				</table>
			</div>';
$toolbar = "<table width=\"200\" cellspacing=\"0\" cellpadding=\"0\" border=0>";
if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Case_Record-New"))
{
    $toolbar .= "<tr><td>".$linterface->GET_LNK_NEW("new.php","","","","",0)."</td>";
    $toolbar .= "<td>".$linterface->GET_LNK_IMPORT("import.php","","","","",0)."</td>";
}

//$searchStr = trim($_GET['searchStr']);
$searchStr = isset($_GET['searchStr']) ? $_GET['searchStr'] : $_POST['searchStr'];
$searchStr = trim($searchStr);
$toolbar .= "<td>".$linterface->GET_LNK_EXPORT("export.php?Year=$Year&Semester=$Semester&Processing=$Processing&Finished=$Finished&searchStr=".stripslashes(intranet_htmlspecialchars($searchStr))."","","","","",0)."</td>";
$toolbar .= "</tr></table>";
$searchTB = "<div class=\"Conntent_search\"><input type=\"textbox\" class=\"formtextbox\" id=\"searchStr\" name=\"searchStr\" value=\"".stripslashes(intranet_htmlspecialchars($searchStr))."\"></div>";

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$extraCriteria = ($Year=='0')? "" : " AND a.AcademicYearID = '$Year'";
$extraCriteria .= ($Semester=='' || $Semester==0)? "  " : " AND a.YearTermID = '$Semester' ";

# PIC #
$yearName = $ldisciplinev12->getAcademicYearNameByYearID($Year);
if($Semester != '' && $Semester != 0) {
    $thisSemester = $Semester;
}

$PICArray = $ldisciplinev12->getPICInArray("DISCIPLINE_CASE", $Year, $thisSemester);

if($ldisciplinev12->PIC_SelectionDefaultOwn)
{
    //if(sizeof($_POST)==0 && sizeof($_GET)==0 && !isset($_POST['pic'])) {		# default display own PIC records
    if(sizeof($_POST)==0 && !isset($_POST['pic']))
    {
        $flag = 0;
        for($i=0; $i<sizeof($PICArray); $i++) {
            if($PICArray[$i][0]==$UserID) {
                $pic = $UserID;
                $flag = 1;
                break;
            }
        }
        /*
        if($flag == 0) {
            $name = $ldisciplinev12->getUserNameByID($UserID);
            array_push($PICArray, array($UserID, $name[0]));
            $pic = $UserID;
        }
        */
    }
}

$picSelection = getSelectByArray($PICArray," name=\"pic\" onChange=\"document.form1.submit()\"",$pic,0,0,"".$i_Discipline_Detention_All_Teachers."");
if($pic != "") {
    $extraCriteria .= " AND (a.PICID = '$pic' OR a.PICID LIKE '$pic,%' OR a.PICID LIKE '%,$pic' OR a.PICID LIKE '%,$pic,%')";
}

if($Processing && $Finished)
{
    // do nothing
}
else if($Processing)
{
    $extraCriteria .= " AND a.RecordStatus = ".DISCIPLINE_CASE_RECORD_PROCESSING;
}
else if($Finished)
{
    $extraCriteria .= " AND a.RecordStatus = ".DISCIPLINE_CASE_RECORD_FINISHED;
}

$sqlKeyword = $ldisciplinev12->Get_Safe_Sql_Like_Query(stripslashes(intranet_htmlspecialchars($searchStr)));
$extraCriteria .= ($searchStr=='')? "" : " AND (a.CaseTitle LIKE '%".$sqlKeyword."%' OR a.CaseNumber LIKE '%".$sqlKeyword."%' )";
//".stripslashes(intranet_htmlspecialchars($searchStr))."

$sql = "SELECT 
                a.CaseNumber,
                CONCAT('<a href=\"view.php?CaseID=',a.CaseID,'\" class=\"tablelink\">',a.CaseTitle,'</a>') as CaseTitleLink,
                a.EventDate,
                ".getNameFieldByLang('teacher_iu.')." as TeacherName,
                CONCAT('<a href=\"view_student_involved.php?CaseID=',a.CaseID,'\" class=\"tablelink\">',COUNT(merit_rec.RecordID),'</a>') as StudentsInv,
                a.DateModified,
                IF(a.RecordStatus=".DISCIPLINE_CASE_RECORD_PROCESSING.",
                    CONCAT('<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_proccessing.gif\" alt=\"".$i_Discipline_System_Discipline_Case_Record_Processing."\">'),
                    CONCAT('<img src=\"".$image_path."/".$LAYOUT_SKIN."/icon_finished.gif\" alt=\"".$eEnrollment['processed']."\">')
                ) as RecordStatus,
                CONCAT('<input type=\"checkbox\" name=\"CaseID[]\" value=\"',a.CaseID,'\">'),
                a.RecordStatus, 
                a.CaseID,
                a.PICID
		FROM 
                DISCIPLINE_CASE as a
                LEFT JOIN INTRANET_USER as teacher_iu ON (a.PICID = teacher_iu.UserID)
                LEFT JOIN DISCIPLINE_MERIT_RECORD merit_rec ON (merit_rec.CaseID = a.CaseID)
                LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID = teacher_iu.UserID)
                LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID = ycu.YearClassID)
		WHERE 
		        1 
		        $extraCriteria 
		GROUP BY 
		        a.CaseID
		";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("CaseNumber","CaseTitle", "EventDate", "TeacherName","StudentsInv","DateModified","RecordStatus");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->IsColOff = 'eDisciplineCaseRecord';

$pos = 0;
$li->column_list .= "<td width='1'class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_System_Discipline_Case_Record_CaseNumber)."</td>\n";
$li->column_list .= "<td width='18%'>".$li->column($pos++, $i_Discipline_System_Discipline_Case_Record_Case_Title)."</td>\n";
$li->column_list .= "<td width='15%'>".$li->column($pos++, $Lang['eDiscipline']['EventDate'])."</td>\n";
$li->column_list .= "<td width='25%'>".$i_Discipline_System_Discipline_Case_Record_Case_PIC."</td>\n";$pos++;
$li->column_list .= "<td width='12%'>".$i_Discipline_System_Case_Record_Student_Involved."</td>\n";$pos++;
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";
$li->column_list .= "<td width='10%'>".$eDiscipline["Status"]."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("CaseID[]")."</td>\n";

# Top menu highlight setting
$CurrentPage = "Management_CaseRecord";
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldisciplinev12->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

    <script language="javascript">
        var targetDivID = "";
        var clickDivID = "";
        <!--
            function removeCat(obj,element,page){
                var alertConfirmRemove = "<?=$i_Discipline_System_Case_Record_Delete_Msg?>";
                if(countChecked(obj,element)==0) {
                    alert(globalAlertMsg2);
                }
                else {
                    if(confirm(alertConfirmRemove)){
                        obj.action=page;
                        obj.method="post";
                        obj.submit();
                    }
                }
            }
        //-->

        function deleteRecord()
        {
            if(check_checkbox(document.form1,'CaseID[]'))
            {
                if(confirm('<?=$i_Discipline_System_Case_Record_Delete_Msg?>')) {
                    checkPost(document.form1,'remove_update.php');
                }
            }
            else
            {
                alert('<?=$i_Discipline_System_Notice_Warning_Please_Select?>');
            }
        }

        function getPosition(obj, direction)
        {
            var objStr = "obj";
            var pos_value = 0;
            while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
            {
                pos_value += eval(objStr + "." + direction);
                objStr += ".offsetParent";
            }

            return pos_value;
        }

        function Hide_Window(pos)
        {
            document.getElementById(pos).style.visibility='hidden';
        }

        function showStatus()
        {
            document.getElementById('status_option').style.left=getPosition(document.getElementById('status_click'),'offsetLeft');
            document.getElementById('status_option').style.top=getPosition(document.getElementById('status_click'),'offsetTop')+20;
            document.getElementById('status_option').style.visibility='visible';
        }

        function changeClickID(id)
        {
            document.form1.clickID.value = id;
        }
    </script>

    <script language="javascript">
        <!--
        var xmlHttp
        function showResult(str,flag)
        {
            xmlHttp = GetXmlHttpObject()
            if (xmlHttp==null)
            {
                alert ("Browser does not support HTTP Request")
                return
            }

            var url = "";
            url = "get_live.php?flag="+flag;
            url = url + "&RecordID=" + str
            url = url + "&sid=" + Math.random()

            if(flag=='detention') {
                xmlHttp.onreadystatechange = stateChanged
            } else if(flag=='waived') {
                xmlHttp.onreadystatechange = stateChanged2
            }
            xmlHttp.open("GET",url,true)
            xmlHttp.send(null)
        }

        function stateChanged()
        {
            if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
            {
                var id = document.form1.clickID.value;
                document.getElementById("show_detail"+id).style.zIndex = "1";
                document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
                document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
            }
        }

        function stateChanged2()
        {
            if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
            {
                var id = document.form1.clickID.value;
                document.getElementById("show_waive"+id).style.zIndex = "1";
                document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
                document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
            }
        }

        function GetXmlHttpObject()
        {
            var xmlHttp = null;
            try
            {
                // Firefox, Opera 8.0+, Safari
                xmlHttp = new XMLHttpRequest();
            }
            catch (e)
            {
                // Internet Explorer
                try
                {
                    xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e)
                {
                    xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            }
            return xmlHttp;
        }

        /*********************************/
        function getObject(obj)
        {
            if (document.getElementById) {
                obj = document.getElementById(obj);
            } else if (document.all) {
                obj = document.all.item(obj);
            } else {
                obj = null;
            }

            return obj;
        }

        function moveObject(obj, e)
        {
            var tempX = 0;
            var tempY = 0;
            var offset = 5;
            var objHolder = obj;

            obj = getObject(obj);
            if (obj==null) {
                return;
            }

            if (document.all) {
                tempX = event.clientX + document.body.scrollLeft;
                tempY = event.clientY + document.body.scrollTop;
            } else {
                tempX = e.pageX;
                tempY = e.pageY;
            }
            if (tempX < 0){tempX = 0} else {tempX = tempX-276}
            if (tempY < 0){tempY = 0} else {tempY = tempY}

            obj.style.top  = (tempY + offset) + 'px';
            obj.style.left = (tempX + offset) + 'px';
            displayObject( objHolder, true );
        }

        function displayObject(obj, show)
        {
            obj = getObject(obj);
            if (obj==null) {
                return;
            }

            obj.style.display = show ? 'block' : 'none';
            obj.style.visibility = show ? 'visible' : 'hidden';
        }

        var callback_checkAccess = {
            success: function (o)
            {
                var tmp_str = o.responseText;
                var par = "Year=<?=$Year?>&Semester=<?=$Semester?>&pic=<?=$pic?>&Finished=<?=$Finished?>&Processing=<?=$Processing?>";

                if(Trim(tmp_str)=='')
                {
                    alert('<?=$i_general_no_access_right?>');
                }
                else
                {
                    window.location="edit.php?CaseID="+tmp_str+"&"+par;
                }
            }
        }

        function checkAccessEdit()
        {
            if(countChecked(document.form1,'CaseID[]')==1)
            {
                CaseID = returnChecked(document.form1,'CaseID[]');
                obj = document.form1;
                YAHOO.util.Connect.setForm(obj);

                var path = "ajax_check_edit.php?CaseID="+CaseID;
                var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_checkAccess);
            }
            else
            {
                alert('<?=$i_Survey_PleaseSelectType?>');
            }
        }
        //-->
    </script>

    <br />
    <form name="form1" method="post" action="">
        <table width="98%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td align="center">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                    <tr>
                                        <td width="70%"><?=$toolbar ?></td>
                                        <td width="30%"></td>
                                    </tr>
                                </table>
                            </td>
                            <td align="right"><?=$searchTB ?></td>
                        </tr>
                        <tr>
                            <td><?=$yearSelection?><?=$semSelection?><?=$classSelection?><?=$picSelection?></td>
                            <td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <?= $ldisciplinev12->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Award_Punishment_Warning) ?>
                            </td>
                        </tr>
							<tr class="table-action-bar"><td><!--<?=$statusSelection?>//-->
                                <div class="selectbox_group"> <a href="javascript:;" onClick="MM_showHideLayers('status_option','','show')"><?=$i_Discipline_System_Award_Punishment_Select_Status?></a> </div>
                                <br style="clear:both">
                                <?=$statusDiv?>
                            <td height="28" align="right" valign="bottom" >
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="right" valign="bottom">
                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
                                                    <td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
                                                        <table border="0" cellspacing="0" cellpadding="2">
                                                            <tr>
                                                                <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
                                                                <? if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || $ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn")) { ?>
                                                                    <td nowrap><a href="javascript:checkAccessEdit()" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
                                                                <? } ?>
                                                                <? if($ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteAll") || $ldisciplinev12->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteOwn")) { ?>
                                                                    <td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
                                                                    <td nowrap="nowrap"><a href="javascript:deleteRecord()" class="tabletool" title="<?= $button_delete?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_delete?></a></td>
                                                                <? } ?>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td><?= $li->display() ?>
                    <div id="div_form"></div>
                </td>
            </tr>
            <!--<tr><td class="tabletextremark"><?=$i_Discipline_System_Discipline_Case_Record_Message?></td></tr>-->

        </table><br>
        <div width="100%" align="left"><?=$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend']?></div>

        <input type="hidden" name="targetDivID" id="targetDivID" />
        <input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
        <input type="hidden" name="ajaxYear" id="ajaxYear" />
        <input type="hidden" name="ajaxSemester" id="ajaxSemester" />
        <input type="hidden" name="ajaxType" id="ajaxType" />
        <input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
        <input type="hidden" name="order" value="<?php echo $li->order; ?>" />
        <input type="hidden" name="field" value="<?php echo $li->field; ?>" />
        <input type="hidden" name="page_size_change" value="" />
        <input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
        <input type="hidden" name="clickID" value="" />
    </form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>