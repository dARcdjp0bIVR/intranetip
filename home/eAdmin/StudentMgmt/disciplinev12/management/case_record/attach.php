<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();

$MODULE_OBJ['title'] = $i_frontpage_campusmail_attachment;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$no_file = 5;
echo generateFileUploadNameHandler("forma","userfile0","hidden_userfile_name0"
,"userfile1","hidden_userfile_name1"
,"userfile2","hidden_userfile_name2"
,"userfile3","hidden_userfile_name3"
,"userfile4","hidden_userfile_name4"
);
?>
<script LANGUAGE=JAVASCRIPT>
var par = opener.window;
var obj = opener.window.document.form1.elements["Attachment[]"];

function grapAttach(cform)
{
var key;
var s="";
for (i=0; i<obj.length; i++)
{
     key = obj.options[i].value;
     if (key!="")
         s += (key + ',');
}
         cform.attachStr.value = s;
}
</script>

<form name="forma" action="attach_update.php" method="post" enctype="multipart/form-data" onsubmit="return Big5FileUploadHandler();"  >
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">	
	<tr>
		<td align="center" >
		<?php 
		for($i=0;$i<$no_file;$i++)
		{ 
		?>
		<input class="file" type="file" name="userfile<?php echo $i; ?>" size="20" /><br />
		<input type="hidden" name="hidden_userfile_name<?=$i?>" />
		<?php 
		} 
		?>
		</td>
	</tr>
	</table>
	</td>
</tr>

<tr>
	<td>
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_upload, "submit", "grapAttach(this.form)") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.close()") ?>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>

<input type="hidden" name="FolderLocation" value="<?php echo $FolderLocation; ?>">
<input type="hidden" name="attachStr" value="">
</form>

<?php
$linterface->LAYOUT_STOP();
?>