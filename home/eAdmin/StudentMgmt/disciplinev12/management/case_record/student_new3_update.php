<?php
// Modifying by: henry

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();
# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");

if(!$CaseID)
{
	header("Location: index.php");
	exit;
}

$lteaching = new libteaching();

$lc = new libucc();

### retrieve extra data
$yearTermInfo = getAcademicYearInfoAndTermInfoByDate($RecordDate);
list($yearID, $year, $termID, $term) = $yearTermInfo;
/*
# School Year
$school_year = GET_ACADEMIC_YEAR3($RecordDate);
# Semester
$semester_mode = getSemesterMode();
if($semester_mode==2)
	$semester = retrieveSemester($RecordDate);
*/
	
# Item Text
$ItemInfo = $ldiscipline->returnMeritItemByID($ItemID);
$ItemText = ($ItemInfo['ItemName']);
$ItemText = addslashes($ItemText);

# ConductScoreChange
$ConductScoreChange = $ConductScore * $record_type;
# SubScoreChange
$StudyScoreChange = $StudyScore * $record_type;

# Subject
$SubjectName = $lteaching->returnSubjectName($SubjectID);
# PIC
$PIC = implode(",",$PIC);
$remark = intranet_htmlspecialchars($remark);
$MeritRecordIDAry = array();

// Get submitted data for eNotice
for ($j=1; $j<=sizeof($student); $j++) {
	${"submitSelectNotice".$j} = $_POST["SelectNotice".$j];
	${"submitTextAdditionalInfo".$j} = stripslashes($_POST["TextAdditionalInfo".$j]);
	${"submitStudentID".$j} = $_POST["StudentID".$j];
	${"submitStudentName".$j} = $_POST["StudentName".$j];
}
$UserName = $ldiscipline->getUserNameByID($UserID);

foreach($student as $k=>$StudentID)
{
	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName;

	### insert into DISCIPLINE_MERIT_RECORD (status = pending)
	$dataAry = array();
	$dataAry['RecordDate'] = $RecordDate;
	$dataAry['Year'] = $year;
	$dataAry['AcademicYearID'] = $yearID;
	$dataAry['Semester'] = addslashes($term);
	$dataAry['YearTermID'] = $termID;
	$dataAry['StudentID'] = $StudentID;
	$dataAry['ItemID'] = $ItemID;
	$dataAry['ItemText'] = $ItemText;
	$dataAry['Remark'] = $remark;
	
	if($record_type!=0)
	{
		$dataAry['MeritType'] = $record_type;
		$dataAry['ProfileMeritType'] = $MeritType;
		$dataAry['ProfileMeritCount'] = $MeritNum;
		$dataAry['ConductScoreChange'] = $ConductScoreChange;
		$dataAry['SubScore1Change'] = $StudyScoreChange;
	}
	$dataAry['Subject'] = $SubjectName;
	$dataAry['SubjectID'] = $SubjectID;
	$dataAry['PICID'] = $PIC;
	$dataAry['RecordStatus'] = DISCIPLINE_STATUS_PENDING;
	$dataAry['CaseID'] = $CaseID;
		
	# Save eNotice
	if (!$lnotice->disabled && $record_type!=0)
	{
		if ($noticeFlag == "YES") {
			for ($i=1; $i<=sizeof($student); $i++) {
				if ($StudentID == ${"submitStudentID".$i}) {
					$dataAry['TemplateID'] = ${"submitSelectNotice".$i};
					$dataAry['TemplateOtherInfo'] = ${"submitTextAdditionalInfo".$i};
					break;
				}
			}
		}
	}

	$FromCaseRecord = 1;
	$MeritRecordID = $ldiscipline->INSERT_MERIT_RECORD($dataAry, $FromCaseRecord);
	if($MeritRecordID)
	{
		$MeritRecordIDAry[] = $MeritRecordID;
	}
	
	if($MeritRecordID && $record_type!=0)
	{
		# Detention
		if ($detentionFlag == "YES") {
			$Item = $ldiscipline->returnMeritItemByID($ItemID);
			$Reason = $Item['ItemCode']." - ".$Item['ItemName'];
			for ($i=0; $i<sizeof($DetentionArr); $i=$i+3) {
				// array sequence: StudentID, DetentionID, Remark
				if ($StudentID == $DetentionArr[$i]) {
					$detentionDataArr = array();
					$detentionDataArr['StudentID'] = "'".$DetentionArr[$i]."'";
					if ($DetentionArr[$i+1] == "") $DetentionArr[$i+1] = "NULL";
					$detentionDataArr['DetentionID'] = $DetentionArr[$i+1];
					$detentionDataArr['Remark'] = "'".stripslashes($DetentionArr[$i+2])."'";
					$detentionDataArr['DemeritID'] = "'".$MeritRecordID."'";
					$detentionDataArr['RequestedBy'] = "'".$UserID."'";
					$detentionDataArr['Reason'] = "'".intranet_htmlspecialchars(addslashes($Reason))."'";
					if ($DetentionArr[$i+1] != "NULL") {
						$detentionDataArr['ArrangedBy'] = "'".$UserID."'";
						$detentionDataArr['ArrangedDate'] = 'NOW()';
					}
					
					$detStuRecordID = $ldiscipline->insertDetention($detentionDataArr);
					
				}
			}
		}

		/*
		#############################################################################################################
		# A&P record approval is based on Case Finish status, so no need to cater approval action
		#############################################################################################################
		# check need approve or not
		$need_approval = $ldiscipline->CHECK_APPROVAL_REQUIRED($MeritType,$MeritNum);

		if($need_approval)
		{
			# can do nothing
		}
		else
		{
			# set APPROVE
			$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID);
		}
		*/

	}
}

intranet_closedb();

if(sizeof($MeritRecordIDAry))
{
?>
	<body onLoad="document.form1.submit();">
	<form name="form1" method="post" action="student_new4.php">
	<input type="hidden" name="CaseID" value="<?=$CaseID?>" />
	<? foreach($MeritRecordIDAry as $d) {?>
	<input type="hidden" name="MeritRecordID[]" value="<?=$d?>">
	<? } ?>
	</form>
	</body>
<?
}
?>

