<?php
//using: 

/********************** Change Log ***********************/
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
#
#	Date:	2016-06-13 (Anna)	[2016-0526-1139-05206]
#			added creator's information below the webpage, change CSS of the table
#
#	Date:	2016-04-08 (Bill)	[2016-0224-1423-31073]
#			added prefix style to teacher name of deleted account
#
#	Date:	2016-03-02 (Bill)	[2016-0224-1423-31073]
#			return PIC name even teacher account is inactive or removed
#
#	Date:	2010-04-14 YatWoon
#			check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

## Get The max. no of record day(s) ##
$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
if($NumOfMaxRecordDays > 0) {
	$EnableMaxRecordDaysChecking = 1;
	$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
	$DateMaxLimit = date("Y-m-d");
} else {
	$EnableMaxRecordDaysChecking = 0;
}

if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditAll") || (($ldiscipline->isCasePIC($CaseID) || $ldiscipline->isCaseOwn($CaseID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-EditOwn"))))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$back_par = "Year=$Year&Semester=$Semester&pic=$pic&Finished=$Finished&Processing=$Processing";

# get case details
$CaseArr = $ldiscipline->getCaseRecordByCaseID($CaseID);
list($CaseNumber,$CaseTitle,$Year,$Semester,$Location,$Category,$EventDate,$PIC,$Attachment,$RecordStatus,$DateModified,$ModifiedBy,$RecordStatusNum,$ReleaseStatus,$ReleasedDate,$ReleasedBy,$FinishedDate,$FinishedBy,$Remark,$PIC) = $CaseArr[0];
$Remark = $CaseArr[0]['Remark'];
/*
if(is_array($PIC))
{
	$ct = 0;
	foreach($PIC as $Key=>$Value)
	{
		$PICUser[] = array($Key,$Value);
	}
}
*/
if ($PIC != '') {
	$namefield = getNameFieldWithLoginByLang();
	
	//[2016-0224-1423-31073]
	// INTRANET_USER
	//$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) AND RecordStatus = 1 ORDER BY $namefield";
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
	$PICUser = $ldiscipline->returnArray($sql);
				
	// INTRANET_ARCHIVE_USER
	//$sql = "SELECT UserID, $namefield FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
	$sql = "SELECT UserID, CONCAT('<span class=\"tabletextrequire\">*</span>', $namefield) FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($PIC) ORDER BY $namefield";
	$ArchivePIC = $ldiscipline->returnArray($sql);
	
	// Merge user and archive user
	$PICUser = array_merge($PICUser, $ArchivePIC);
}


# prepare semester array for generating semester selection box
/*$semester_data = getSemesters();
$number = sizeof($semester_data);
$semArr[] = array('0',$ec_iPortfolio['whole_year']);
for ($i=0; $i<$number; $i++)
{
	$linedata[]= split("::",$semester_data[$i]);
	$semArr[] = array($linedata[$i][0],$linedata[$i][0]);
}*/

$select_sem_html = "<input type='hidden' name='Semester' value=''>";
/*
$semester_mode = getSemesterMode();
if($semester_mode==1)
{
	$select_sem = getSelectSemester("name=Semester", $Semester);	
	$select_sem_html = '<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle">'.$i_Discipline_System_Discipline_Case_Record_Case_Semester.'
								</td>
								<td class="tabletext">'.$select_sem.'
								</td>
							</tr>
						</table>';
}
else
{
	$select_sem_html = "<input type='hidden' name='Semester' value=''>";
}
*/

if($Attachment=='')
{
	$sessionTime = session_id()."-".(time());
}
else
{
	$sessionTime = $Attachment;	
}
$picSelect = $linterface->GET_SELECTION_BOX($PICUser,"id=\"PIC[]\" name=\"PIC[]\"  multiple='multiple'","");
//$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester", name="Semester"', "", "$Semester");
$PICSelection = "<table class=\"inside_form_table\" width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$picSelect."
				</td>
				<td valign=\"bottom\">";
				
				if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) 
				{ 
					$PICSelection .= $linterface->GET_BTN($button_select, "button", "newWindow('../choose_pic.php?fieldname=PIC[]', 9)");
					$PICSelection .= $linterface->GET_BTN($button_remove_selected, "button", "checkOptionRemove(document.form1.elements['PIC[]'])");
				}
$PICSelection .= "</td>
				</tr>
</table>";


$attSelect = $linterface->GET_SELECTION_BOX(getAttachmentArray($Attachment),"id=\"Attachment[]\" name=\"Attachment[]\"  multiple='multiple'","");
$attachmentHTML = "<table class=\"inside_form_table\" width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$attSelect."
				</td>
				<td valign=\"bottom\">".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."', 9)")."
				<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment()")."
				</td>
				</tr>
				</table>";
($RecordStatusNum==1)?$FinishedChecked  = "checked" : $ProcessingChecked= "checked";

	
$statusHTML = '<input type="radio" id="status" name="status" value="0" '.$ProcessingChecked.'/>'.$i_Discipline_System_Discipline_Case_Record_Processing.
			  '<input type="radio" id="status" name="status" value="1" '.$FinishedChecked.'/>'.$i_Discipline_System_Discipline_Case_Record_Processed;

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Case_Record_Case_List, "index.php?$back_par");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Case_Record_Case_Details);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# generate semester checking js
if($semester_mode==2)
{
	$sem_data = getSemesters();
	$sem_js.="var sem_js = new Array();\n";
	for($i=0;$i<sizeof($sem_data);$i++)
	{
		list($sem_name, $sem_type, $sem_start, $sem_end) = explode("::", $sem_data[$i]);
		$sem_js.="sem_js.push(new Array('$sem_start','$sem_end'));\n";
	}
}
$CaseNumberArr = $ldiscipline->getAllCaseNumber();
$js = "var case_number = new Array();\n";
$js .= "var case_id = new Array();\n";
for($a=0;$a<sizeof($CaseNumberArr);$a++)
{
	$js .="case_number[$a] = '".(($CaseNumberArr[$a]['CaseNumber']))."';\n";
	$js .="case_id[$a]  = '".(($CaseNumberArr[$a]['CaseID']))."';\n";
}
# Start layout
$linterface->LAYOUT_START();

function getAttachmentArray($Attachment)
{
         global $file_path, $image_path, $intranet_httppath;
         $path = "$file_path/file/disciplinev12/case_record/".$Attachment;
         if (!is_dir($path))
		 {
			 $path = $path."tmp";
		 }
         $a = new libfiletable("", $path, 0, 0, "");
         $files = $a->files;
         
         $ct = 0;
         while (list($key, $value) = each($files))
         {
	         
             $returnArr[$ct][0] = $files[$key][0];
             $returnArr[$ct][1] = $files[$key][0];
				$ct++;
         }
         
         return $returnArr;
}


?>
<script type="text/javascript">
<?=$sem_js?>
<?=$js?>
var callback_ajaxRemoveAttachment = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    checkOptionRemove(document.form1.elements['Attachment[]']);
	}
}

function ajaxRemoveAttachment()
{
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_remove_attachment.php";
	
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_ajaxRemoveAttachment);
}
function check_semester(objDate)
{
	semester_periods = sem_js;
	
	valid = false;
	if(semester_periods!=null)
	{
		for(j=0;j<semester_periods.length;j++){
			dateStart = semester_periods[j][0];
			dateEnd = semester_periods[j][1];
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	
	if(!valid)
	{
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		alert('<?=$eDiscipline["NoSemesterSettings"]?>');
		return false;
	}
	
	return true;
}

function CompareDates(date_string1, date_string2, EqualDateAllow)
{
	var yr1  = parseInt(date_string1.substring(0,4),10);
	var mon1 = parseInt(date_string1.substring(5,7),10);
	var dt1  = parseInt(date_string1.substring(8,10),10);
	var yr2  = parseInt(date_string2.substring(0,4),10);
	var mon2 = parseInt(date_string2.substring(5,7),10);
	var dt2  = parseInt(date_string2.substring(8,10),10);
	var date1 = new Date(yr1, mon1-1, dt1);
	var date2 = new Date(yr2, mon2-1, dt2);
	
	if(EqualDateAllow == 0)
	{
		if(date2 < date1) {
			return true;
		}else{
			return false;
		}
	}else{
		if(date2 <= date1) {
			return true;
		}else{
			return false;
		}
	}
}

function checkForm(obj) 
{
	fieldValue = obj.CaseNumber.value.replace(/'/g, "&#039;").replace(/`/g, "&#039;").replace(/"/g, "&quot;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
	for(a=0;a<case_number.length;a++)
	{
		if(fieldValue==case_number[a] && case_id[a]!=<?=$CaseID?>)
		{
			alert('<?=$i_Discipline_System_Discipline_Case_Record_Case_Number_Exists_JS_alert?>');
			return false;
		}
	}
	
	if(!check_text(obj.CaseNumber,'<?=$i_Discipline_System_Discipline_Case_Record_Case_Number_JS_alert?>'))
	{
		return false
	}
	if(!check_text(obj.CaseTitle,'<?=$i_Discipline_System_Discipline_Case_Record_Case_Title_JS_alert?>'))
	{
		return false
	}
	/*if(!check_text(obj.Category,'<?=$i_Discipline_System_Discipline_Case_Record_Category_JS_alert?>'))
	{
		return false
	}*/
	if(!check_text(obj.EventDate,'<?=$i_Discipline_System_Discipline_Case_Record_Event_Date_JS_alert?>'))
	{
		return false
	}else{
		<? if($EnableMaxRecordDaysChecking == 1) { ?>
			// Check Event Date is allow or not
			var DateMinLimit = "<?=$DateMinLimit;?>";
			var DateMaxLimit = "<?=$DateMaxLimit;?>";
			if(!CompareDates(obj.EventDate.value, DateMinLimit, 1))
			{
				alert("<?=$Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'];?>");
				return false;
			}else{
				if(!CompareDates(DateMaxLimit, obj.EventDate.value, 1))
				{
					alert("<?=$Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'];?>");
					return false;
				}
			}
		<? } ?>
	}
	if(!check_text(obj.Location,'<?=$i_Discipline_System_Discipline_Case_Record_Location_JS_alert?>'))
	{
		return false
	}
	var PICobj = document.getElementById('PIC[]');
	if(PICobj.length==0)
    {    
	    alert('<?=$i_Discipline_System_Discipline_Case_Record_PIC_JS_alert?>');
	    return false
 	}
 	if (!check_date(obj.EventDate,"<?php echo $i_invalid_date; ?>")) return false;
 	if (compareDate(obj.EventDate.value, '<?=date('Y-m-d')?>') > 0) 
 	{
	 	alert ("<?php echo $i_invalid_date; ?>");
	 	return false;
	}
	<? if($semester_mode==2) {?>
	if(!check_semester(obj.EventDate))
	{
		return false;
	}
	<? } ?>
	
	checkOptionAll(obj.elements["Attachment[]"]);
    checkOptionAll(obj.elements["PIC[]"]);	
	return true
}
</script>
<form name="form1" method="post" action="edit_update.php" onSubmit="return checkForm(form1)">
<table width="100%">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<label for="grading_passfail" class="tabletext"><em class="form_sep_title"> - <?=$i_Discipline_System_Discipline_Case_Record_Case_Details?>-</em></label>
							</tr>
						</table>
						<table class="form_table_v30">
							<tr valign="top">
								<td class="field_title"><?=$i_Discipline_System_Discipline_Case_Record_Case_Number?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT type="text" name="CaseNumber" id="CaseNumber" class="tabletext" value="<?=$CaseNumber?>" maxlength="50" >
								</td>
							</tr>
							<tr valign="top">
								<td class="field_title"><?=$i_Discipline_System_Discipline_Case_Record_Case_Title?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><input name="CaseTitle" id="CaseTitle" type="text" class="textboxtext" value="<?=$CaseTitle?>">
								</td>
							</tr>
						</table>
						<!--<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Category?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT type="text" name="Category" id="Category" class="tabletext" value="<?=$Category?>">
								</td>
							</tr>
						</table>//-->
						<!--<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Discipline_Case_Record_Case_School_Year?>
								</td>
								<td class="tabletext"><INPUT type="text" name="Year" id="Year" class="tabletext" value="<?=$Year?>">
								</td>
							</tr>
						</table>-->
						
						<?=$select_sem_html?>
								
						<table class="form_table_v30">
							<tr valign="top">
								<td class="field_title"><?=$Lang['eDiscipline']['EventDate']?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><?=$linterface->GET_DATE_PICKER("EventDate",$EventDate)?>
								</td>
							</tr>
							<tr valign="top">
								<td class="field_title"><?=$i_Discipline_System_Discipline_Case_Record_Case_Location?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><input name="Location" id="Location" type="text" class="textboxtext" value="<?=$Location?>">
								</td>
							</tr>
							<tr valign="top">
								<td class="field_title"><?=$i_Discipline_System_Discipline_Case_Record_Case_PIC?>
								<span class="tabletextrequire">* </span></td>
								<td class="tabletext"><?=$PICSelection?>
								</td>
							</tr>
							<tr valign="top">
								<td class="field_title"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?>
								</td>
								<td class="tabletext"><?=$attachmentHTML?>
								</td>
							</tr>
							<tr valign="top">
								<td class="field_title"><?=$i_UserRemark?></td>
								<td class="tabletext"><?=$linterface->GET_TEXTAREA("Remark", $Remark)?></td>
							</tr>
							<tr valign="top">
								<td class="field_title"><?=$i_general_status?>
								</td>
								<td class="tabletext"><?=$statusHTML?>
								</td>
							</tr>
						</table>
						
						<? if($InputBy > 0){ ?>
						<em class="form_sep_title">- <?=$Lang['AccountMgmt']['CreateUserInfo']?> -</em>
						<table class="form_table_v30">
							<tr>
								<td class="field_title"><?=$Lang['AccountMgmt']['CreateUserName'] ?></td>
								<td colspan="2"><?=$nameDisplay?></td>
							</tr>
							<tr>	
								<td class="field_title"><?=$Lang['AccountMgmt']['CreateUserDate'] ?></td>
								<td colspan="2"><?=$dataInput?></td>
			 				</tr>
			 			</table>
		 				<? } ?>
							
						<br>
						<table class="inside_form_table">
							<tr>
								<td class="tabletextremark" style="border:none"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			
			<div class="edit_bottom_v30">
				<p class="spacer"></p>
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit",'','submit')?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?$back_par'")?>
				<p class="spacer"></p>
			</div>
			
		</td>
	</tr>
</table>
<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>"/>
<input type="hidden" name="back_par" id="back_par" value="<?=$back_par?>"/>
<input type="hidden" id="FolderLocation" name="FolderLocation" value="<?=$sessionTime?>">
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>