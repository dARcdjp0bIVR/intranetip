<?php
// Modifying by:

############ Change Log Start #############################
#
#   Date    :   2020-11-24 Bill     [IP30 DM#1014]
#               fixed set incorrect YearTermID to case records
#
############ Change Log End #############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");

$CaseNumber = intranet_htmlspecialchars(trim($CaseNumber));
$CaseTitle = intranet_htmlspecialchars(trim($CaseTitle));
$Category = intranet_htmlspecialchars(trim($Category));
$EventDate = intranet_htmlspecialchars(trim($EventDate));
$Location = intranet_htmlspecialchars(trim($Location));
$Remark = intranet_htmlspecialchars(trim($Remark));

$Year = GET_ACADEMIC_YEAR3($EventDate);
if($Semester != '')
{
	$Semester = intranet_htmlspecialchars(trim($Semester));
}
else
{
	//$Semester = getCurrentSemester();
	$Semester = retrieveSemester($EventDate);
}

$RecordStatus = 0;
$LockStatus = 0;

if(is_array($PIC))
{
	$IDs = implode(",",$PIC);
}

// [IP30 DM#1014]
//$tempYear = GET_ACADEMIC_YEAR3($EventDate);
//$academicYearID = $ldiscipline->getAcademicYearIDByYearName($tempYear);
//$yearTermID = $ldiscipline->getTermIDByTermName($Semester);
$yearAndTermInfo = getAcademicYearInfoAndTermInfoByDate($EventDate);
$academicYearID = $yearAndTermInfo[0];
$yearTermID = $yearAndTermInfo[2];

$fields = "CaseNumber,CaseTitle,Year,Semester,EventDate,Location,Remark,PICID,RecordStatus,LockStatus,Attachment,DateInput,DateModified,modifiedby,CreatedBy,AcademicYearID,YearTermID";
$InsertValue = "'$CaseNumber','$CaseTitle','$Year','$Semester','$EventDate','$Location','$Remark','$IDs',$RecordStatus,$LockStatus,'$FolderLocation',now(),now(),'$UserID','$UserID','$academicYearID','$yearTermID'";

$sql = "INSERT INTO DISCIPLINE_CASE ($fields) VALUES ($InsertValue)";
if($ldiscipline->db_db_query($sql))
{
	$SysMsg = "add";
}
else
{
	$SysMsg = "add_failed";
}	

$ReturnPage = "index.php?xmsg=$SysMsg";

intranet_closedb();
header("Location: $ReturnPage");
?>
