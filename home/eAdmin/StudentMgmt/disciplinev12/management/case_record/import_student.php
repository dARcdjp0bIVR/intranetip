<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");

$linterface = new interface_html();
$lsp = new libstudentprofile();

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$csvFile = ($ldiscipline->use_subject)?"discipline-case-student-import-sample.csv":"discipline-case-student-import-no-subject.csv";

$layer_html = "<div id=\"ref_list\" style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'></div>";

$format_str = $layer_html;
$format_str .= "<p><span id=\"GM__to\"><strong>".$iDiscipline['Import_Instruct_Main']."</strong></span></p>";
$format_str .= "<div class=\"p\"><br/><ol type=\"a\"><li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1']."
				<strong>&lt;<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\" target=\"_blank\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2']."</a>&gt;</strong>".
				$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3']."</li>";
				
$format_str .= "<li>".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1']."</span><br/>";
$format_str .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/discipline/apcasestud_import_illus_$intranet_session_language.png\"/><br/><div class=\"note\">";
$format_str .= "<b>".$iDiscipline['Import_Instruct_Note']."</b><br/>";


$format_str .= "<ul><li>".$Lang['eDiscipline']['Award_Punishment_Import_Instruct_Note_1_1'];
$format_str .= "<strong>&lt;<a href=\"javascript:show_ref_list('ItemCode','ic_click')\" class=\"tablelink\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2']."</a>&gt;</strong><span id=\"ic_click\">&nbsp;</span>";
if($intranet_session_language!='en')
$format_str .= $iDiscipline['Award_Punishment_Import_Instruct_Note_1_2_1'];
$format_str .= "</li>";
if($ldiscipline->use_subject)
{
	$format_str .= "<li>".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_1']."<strong>&lt;<a href=\"javascript:show_ref_list('SubjCode','sc_click')\" class=\"tablelink\">".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_2']."</a>&gt</strong><span id=\"sc_click\">&nbsp;</span>".$iDiscipline['Award_Punishment_Import_Instruct_Note_2_3']."</li>";
}

$format_str .= "<li>".$iDiscipline['Award_Punishment_Import_Instruct_Note_3_1']."</br></br><a href=\"javascript:show_ref_list('MeritCode','mc_click')\" class=\"tablelink\">".$iDiscipline['Award_Punishment_Import_Instruct_Note_3_2']."</a><span id=\"mc_click\">&nbsp;</span></br></br>".$iDiscipline['Award_Punishment_Import_Instruct_Note_3_3']."</li>";

$format_str .= "<li><span id=\"GM__date\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1']."</span></li></ul></div></li><br/>";
$format_str .= "<li><span id=\"GM__savee\">".$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_c_1']."</span></li></ol></div>";

$linterface->LAYOUT_START();
?>
<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "../award_punishment/ajax_ap.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}
function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}
function DisplayPosition(){
	
  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility='visible';
}
</script>
<br />
<form name="form1" method="post" action="import_student_result.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr><td><table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
						<tr><td class="tabletext" align="left" width="30%"><?=$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>: </td><td class="tabletext" width="85%"><input class="file" type="file" name="csvfile"></td></tr>
						<tr><td class="tabletext" align="left" colspan="2"><?=$Lang['eDiscipline']['ImportSourceFileNote']?></td></tr>
						</table>
					</td>	
				</tr>
				<tr>
					<td class="tabletext"><?=$format_str?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='view_student_involved.php?CaseID=".$CaseID."&Page=involved'") ?>
		</td>
	</tr>
</table>
<input type="hidden" id="CaseID" name="CaseID" value="<?=$CaseID?>"/>
<input type="hidden" id="task" name="task"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
