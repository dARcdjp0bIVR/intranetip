<?php
// Modifying by: yat

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lf = new libfilesystem();
$lu = new libuser();
$filepath = $csvfile;
$filename = $csvfile_name;


$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-New");

//$year = getCurrentAcademicYear();
//$semester = getCurrentSemester();

$result_ary = array();
$imported_student = array();
$student = array();

### List out the import result

$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Case_Number</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Case_Title</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Category</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_Case_Location</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Merit_Remark</td>";
$x .= "</tr>";

# get csv data from db
$sql = "select * from temp_case_record_import where UserID=". $_SESSION["UserID"];
$data = $ldiscipline->returnArray($sql);

if(sizeof($data)==0)
{
	$xmsg="import_failed";
}
else
{
	$RecordStatus = 0;
	$LockStatus = 0;
	for($a=0;$a<sizeof($data);$a++)
	{
//		list($CaseNumber,$CaseName,$Category,$EventDate,$Semester,$Location,$PICIDs,$PIC) = $data[$a];
		list($CaseNumber,$CaseName,$EventDate,$Semester,$Location,$PICIDs,$PIC,$Remark) = $data[$a];
				
		$Year = GET_ACADEMIC_YEAR3($EventDate);
		$yearAndTermInfo = getAcademicYearInfoAndTermInfoByDate($EventDate);
		$AcademicYearID = $yearAndTermInfo[0];
		$YearTermID = $yearAndTermInfo[2];
		
		### insert into DISCIPLINE_CASE
//		$fields = "CaseNumber,CaseTitle,Category,Year,Semester,EventDate,Location,PICID,RecordStatus,LockStatus,DateInput,DateModified,modifiedby, CreatedBy";
		$fields = "CaseNumber,CaseTitle,Year,AcademicYearID,Semester,YearTermID,EventDate,Location,PICID,RecordStatus,LockStatus,DateInput,DateModified,modifiedby, CreatedBy, Remark";
//		$InsertValue = "'".addslashes($CaseNumber)."','".addslashes($CaseName)."','".addslashes($Category)."','$Year','".addslashes($Semester)."','$EventDate','".addslashes($Location)."','$PICIDs',$RecordStatus,$LockStatus,now(),now(),'".$UserID."', '".$UserID."'";
		$InsertValue = "'".addslashes($CaseNumber)."','".addslashes($CaseName)."','$Year',$AcademicYearID,'".addslashes($Semester)."',$YearTermID,'$EventDate','".addslashes($Location)."','$PICIDs',$RecordStatus,$LockStatus,now(),now(),'".$UserID."', '".$UserID."','$Remark'";
	
		$sql = "INSERT INTO DISCIPLINE_CASE ($fields) VALUES ($InsertValue)";
		
		if($ldiscipline->db_db_query($sql))
		{
			$import_success++;
		}
		
		$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
		$x .= "<td class=\"tabletext\">".($a+1)."</td>";
		$x .= "<td class=\"tabletext\">". $CaseNumber ."</td>";
		$x .= "<td class=\"tabletext\">". $CaseName ."</td>";
//		$x .= "<td class=\"tabletext\">". $Category ."</td>";
		$x .= "<td class=\"tabletext\">". $EventDate."</td>";
		$x .= "<td class=\"tabletext\">". $Location."</td>";
		$x .= "<td class=\"tabletext\">". $PIC."</td>";
		$x .= "<td class=\"tabletext\">". stripslashes($Remark)."</td>";
		$x .= "</tr>";

	}
	
	if($import_success>0)
	{
		$xmsg2= "<font color=green>".($import_success)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
	}
	else
	{
		$xmsg="import_failed";	
	}
	
}

$x .= "</table>";

$linterface = new interface_html();
$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php'");

$CurrentPage = "Management_CaseRecord";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();

?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
</tr>
<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>	
</table>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
