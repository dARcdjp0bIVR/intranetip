<?php


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteOwn")))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$IDs = array();
if(is_array($CaseID))
	$IDs = $CaseID;
else
	$IDs[] = $CaseID;

foreach($IDs as $cid)
{
	# check access right
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteAll") || (($ldiscipline->isCasePIC($cid) || $ldiscipline->isCaseOwn($cid)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-DeleteOwn")))
	{
		# delete case and related A&P records
		$ldiscipline->DELETE_CASE($cid);
		$msg = 'delete';
	}
	else
	{
		$msg = 'delete_failed';
	}
}

	
intranet_closedb();
header("Location: index.php?xmsg=$msg");
?>
