<?php
# using: henry
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Case_Record-View");

$ExportArr = array();

$conds = "";

if($targetClass != "" && $targetClass != "0") {
    $conds .= " AND (yc.ClassTitleEN = '$targetClass' OR yc.ClassTitleB5 = '$targetClass')";
}

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;	
}

# Approved #  // released record also be approved
if($approved == 1) {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED.")";
	$approvedChecked = "checked";
}

# Rejected #
if($rejected == 1) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED;	
}

# Released #
if($released == 1) {
	$releasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	
}

# Waived #
if($waived == 1) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED;	
}

# Locked #
if($locked == 1) {
	$lockedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.LockStatus = ".DISCIPLINE_STATUS_LOCK;	
}

$conds2 .= ($conds2 != "") ? ")" : "";

$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$student_namefield = getNamefieldByLang("b.");
$yearID = Get_Current_Academic_Year_ID();

$sql = "SELECT ";
//			CONCAT($clsName, ' - ', ycu.ClassNumber) as ClassNameNum,
$sql .= " $clsName as ClassName, ";
$sql .= "
			$student_namefield as std_name,
			IF(a.MeritType=1,'$i_Discipline_System_Merit','$i_Discipline_System_Demerit') as meritImg,
			if(a.MeritType=0,'', CONCAT(a.ProfileMeritCount,' ',a.ProfileMeritType)) as record,
			CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;')) as reason,
			a.TemplateID as action,
			LEFT(a.DateModified,10) as modifiedDate,
			CONCAT('-','-') as status,
			CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'>'),
			a.NoticeID,
			a.RecordID,
			a.RecordStatus,
			a.ProfileMeritType,
			a.ProfileMeritCount,
			b.UserID,ReleaseStatus,
			ycu.ClassNumber
			
			FROM DISCIPLINE_MERIT_RECORD as a
			LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=b.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID) 
			LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
			
			WHERE a.CaseID=$CaseID AND
					yc.AcademicYearID=$yearID
			$conds
			GROUP BY a.RecordID 
			ORDER BY $clsName, ycu.ClassNumber
		";
$sql .= ($conds2 != "") ? " AND ".$conds2 : "";
		
$row = $ldiscipline->returnArray($sql,9);
$meritArr = array();
$meritArr[5] = $i_Merit_Warning;
$meritArr[6] = $i_Merit_Merit;
$meritArr[7] = $i_Merit_MinorCredit;
$meritArr[8] = $i_Merit_MajorCredit;
$meritArr[9] = $i_Merit_SuperCredit;
$meritArr[10] = $i_Merit_UltraCredit;
$meritArr[4] = $i_Merit_BlackMark;
$meritArr[3] = $i_Merit_MinorDemerit;
$meritArr[2] = $i_Merit_MajorDemerit;
$meritArr[1] = $i_Merit_SuperDemerit;
$meritArr[0] = $i_Merit_UltraDemerit;

// Create data array for export
for($i=0; $i<sizeof($row); $i++)
{
	//record
	$merit = $row[$i][12]+5;
	$Record = $row[$i][13]>0 ? $row[$i][13]." ".$meritArr[$merit] : "--";
	
	//status
	$sql = "SELECT RecordStatus, ReleaseStatus FROM DISCIPLINE_MERIT_RECORD WHERE RecordID=".$row[$i]['RecordID'];
	$result = $ldiscipline->returnArray($sql,2);
	$status = '';
	($result[0][0] == DISCIPLINE_STATUS_PENDING) ? $status[] = $i_Discipline_System_Award_Punishment_Pending : "";
	($result[0][0] == DISCIPLINE_STATUS_REJECTED) ? $status[] = $i_Discipline_System_Award_Punishment_Rejected : "";
	($result[0][1] == DISCIPLINE_STATUS_RELEASED) ? $status[] = $i_Discipline_System_Award_Punishment_Released: "";
	($result[0][0] == DISCIPLINE_STATUS_APPROVED) ? $status[] = $i_Discipline_System_Award_Punishment_Approved : "";
	($result[0][0] == DISCIPLINE_STATUS_WAIVED) ? $status[] = $i_Discipline_System_Award_Punishment_Waived : "";
		
	
	 	
 	$ExportArr[$i][0] = $row[$i]['ClassName'];		//Class Name
 	$ExportArr[$i][1] = $row[$i]['ClassNumber'];		//Class Number
	$ExportArr[$i][2] = $row[$i]['std_name'];		//std_name
	$ExportArr[$i][3] = $row[$i][13]>0 ? $row[$i]['meritImg'] : "--";		//merit
	$ExportArr[$i][4] = $Record;		//Record
	$ExportArr[$i][5] = $row[$i]['reason'];		//Reason
	$ExportArr[$i][6] = $row[$i]['modifiedDate'];		//modifiedDate
	$ExportArr[$i][7] = implode(",",$status); //status  
	
}


//define column title
$exportColumn = array($i_ClassName,$i_ClassNumber,$i_general_name,$eDiscipline["AwardPunishmentQty"],$i_general_receive,$eDiscipline["Award_Punishment_RecordItem"],$i_Discipline_Last_Updated,$i_Discipline_System_Discipline_Status);


$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

// Output the file to user browser
$filename = 'case_record_student.csv';
$lexport->EXPORT_FILE($filename, $export_content);

?>
