<?php
// using: 
/*
 * 2019-05-13 (Bill)  : Prevent SQL Injection
 * 2016-04-18 (Bill)  : Add parms passing to index.php	[DM#2953]
 * 2013-06-21 (Carlos): $sys_custom['eDiscipline']['yy3'] - adjust grade according to cust settings adjustment base mark, grade and number of misconduct 
 * 														  - down grade if any demerit item exceed quantity limit setting
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();
$ldisciplinev12->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");

### Handle SQL Injection + XSS [START]
//$Year = intranet_htmlspecialchars($Year);
//$Semester = intranet_htmlspecialchars($Semester);
$AcademicYearID = IntegerSafe($Year);
$YearTermID = IntegerSafe($Semester);
### Handle SQL Injection + XSS [END]

if($sys_custom['eDiscipline']['yy3'])
{
	$downGradeItems = $ldisciplinev12->GetConductDownGradeConditionRecords();
	$downGradeItemIdToQuantity = array();
	for($i=0; $i<count($downGradeItems); $i++) {
		$downGradeItemIdToQuantity[$downGradeItems[$i]['ItemID']] = $downGradeItems[$i]['Quantity'];
	}
	
	$sql = "SELECT MinConductScore, GradeChar FROM DISCIPLINE_CONDUCT_GRADE_RULE ORDER BY MinConductScore DESC";
	$scoreGradeCharAry = $ldisciplinev12->returnResultSet($sql);
	$grade_count = count($scoreGradeCharAry);
	
	// Remark: B- would be the minimum adjustment grade
	$prevGradeCharAry = array(); // gradechar as key, previous higher gradechar as value
	$nextGradeCharAry = array(); // gradechar as key, next lower gradechar as value
	$gradeCharToMarkAry = array(); // gradechar as key, min mark as value
	for($k=0; $k<$grade_count; $k++){
		$prevGradeCharAry[$scoreGradeCharAry[$k]['GradeChar']] = isset($scoreGradeCharAry[$k-1])? $scoreGradeCharAry[$k-1]['GradeChar'] : $scoreGradeCharAry[$k]['GradeChar'];
		$nextGradeCharAry[$scoreGradeCharAry[$k]['GradeChar']] = isset($scoreGradeCharAry[$k+1])? $scoreGradeCharAry[$k+1]['GradeChar'] : $scoreGradeCharAry[$k]['GradeChar'];
		$gradeCharToMarkAry[$scoreGradeCharAry[$k]['GradeChar']] = $scoreGradeCharAry[$k]['MinConductScore'];
	}
	$B_minus_above_mark = $gradeCharToMarkAry[$prevGradeCharAry['B-']]; // get the grade mark of the grade that just above B-
	$custAdjustGrade = count($downGradeItems)>0 && is_numeric($ldisciplinev12->ConductAdjustmentBaseMark);
	//debug_r($downGradeItemIdToQuantity);
}

# Get all students
if($YearTermID=='0')
{
	$IsAnnual = 1;
	$parSemester = null;
	$conds .= " AND IsAnnual = 1";
}
else
{
	$parSemester = $YearTermID;
	//$conds .= " AND Semester = '$YearTermID'";
	$conds .= " AND YearTermID = '$YearTermID'";
}

$sql = "select distinct UserID from INTRANET_USER where RecordType=2 and RecordStatus=1 ";
//$sql = "select distinct UserID from INTRANET_USER where RecordType=2 and RecordStatus=1 and userid = 1844";		# For debug use only
$StudentsArr = $ldisciplinev12->returnArray($sql);
for($a=0; $a<sizeof($StudentsArr); $a++)
{
	$array_students[] = $StudentsArr[$a]['UserID'];
}

if($sys_custom['eDiscipline']['yy3'] && $custAdjustGrade)
{
	// Count number of misconnduct records for each student
	$sql = "SELECT a.StudentID,a.AcademicYearID,a.YearTermID,COUNT(a.RecordID) as NumOfMisconduct
			FROM DISCIPLINE_MERIT_RECORD as a 
			WHERE a.MeritType=-1 AND a.RecordStatus='".DISCIPLINE_STATUS_APPROVED."' 
				AND a.ProfileMeritCount>0 AND a.ProfileMeritType<-1 AND a.ProfileMeritType>=-5 ";
	if($AcademicYearID !='') {
		$sql .= " AND a.AcademicYearID='$AcademicYearID' ";
	}
	if($YearTermID != '' && $YearTermID != '0') {
		$sql .= " AND a.YearTermID='$YearTermID' ";
	}
	$sql .= "GROUP BY a.StudentID,a.AcademicYearID,a.YearTermID";
	$studentMisconductAry = $ldisciplinev12->returnResultSet($sql);
	
	$studentIdToMisconductAry = array();
	for($i=0, $record_count=count($studentMisconductAry); $i<$record_count;$i++)
	{
		$__student_id = $studentMisconductAry[$i]['StudentID'];
		$__academic_year_id = $studentMisconductAry[$i]['AcademicYearID'];
		$__year_term_id = $studentMisconductAry[$i]['YearTermID'];
		$__num_of_misconduct = $studentMisconductAry[$i]['NumOfMisconduct'];
		
		$studentIdToMisconductAry[$__student_id][$__academic_year_id][$__year_term_id] = $__num_of_misconduct;
	}
	//debug_r("Misconduct");debug_r($studentIdToMisconductAry);debug_r($sql);
	
	// Count demerit records for each student
	$sql = "SELECT StudentID, AcademicYearID, YearTermID, ItemID, COUNT(RecordID) as NumOfDemerit FROM DISCIPLINE_MERIT_RECORD WHERE MeritType=-1 AND RecordStatus='".DISCIPLINE_STATUS_APPROVED."' ";
	if($AcademicYearID !='') {
		$sql .= " AND AcademicYearID='$AcademicYearID' ";
	}
	if($YearTermID != '' && $YearTermID != '0') {
		$sql .= " AND YearTermID='$YearTermID' ";
	}
	$sql .= "GROUP BY StudentID,AcademicYearID,YearTermID, ItemID";
	$studentDemeritAry = $ldisciplinev12->returnResultSet($sql);
	
	$studentIdToDemeritAry = array();
	for($i=0, $record_count=count($studentDemeritAry); $i<$record_count;$i++)
	{
		$__student_id = $studentDemeritAry[$i]['StudentID'];
		$__academic_year_id = $studentDemeritAry[$i]['AcademicYearID'];
		$__year_term_id = $studentDemeritAry[$i]['YearTermID'];
		$__item_id = $studentDemeritAry[$i]['ItemID'];
		$__num_of_demerit = $studentDemeritAry[$i]['NumOfDemerit'];
		
		$studentIdToDemeritAry[$__student_id][$__academic_year_id][$__year_term_id][$__item_id] = $__num_of_demerit;
	}
	//debug_r("Demerit");debug_r($studentIdToDemeritAry);
}

# when generate whole year
if($IsAnnual)
{
	if($AcademicYearID != '')
	{
	    # when generate whole year of a selected year
		$student_data = $ldisciplinev12->retrieveAllSemesterConductBalanceGradeForGeneration($array_students,$AcademicYearID);
	}
	else
	{
		# when generate all year, whole year	
		$student_data = $ldisciplinev12->retrieveAllSemesterAllYearConductBalanceGradeForGeneration($array_students,$AcademicYearID);
	}
	
	for ($i=0; $i<sizeof($student_data); $i++)
	{
		$student_id = $student_data[$i]['StudentID'];
		$student_data_annual[$student_id][] = $student_data[$i];
	}
	
	if(is_array($student_data_annual))
	{
		foreach($student_data_annual as $Key=>$Value)
		{
			$total_score = '';
			$ratio = '';
			$total_ratio ='';
			
			# Calculate the conduct score for each semester by multiplying them to the ratio
			for($a=0; $a<sizeof($Value); $a++)
			{
				list($t_student_id, $t_conduct_year ,$t_conduct_semester,$t_conduct_score, $t_grade_char) = $Value[$a];
				$ratio = $ldisciplinev12->getSemesterRatio('', '', $t_conduct_year, $t_conduct_semester);
				
				if($ratio != 0)
				{
					$t_conduct_score *= $ratio;
				    
					# Add all conduct score to total 
					$total_score[$t_conduct_year][] = $t_conduct_score;
					$total_ratio += $ratio;
				}
			}
			
			if(is_array($total_score))
			{
				foreach($total_score as $Key=>$Value)
				{
					# get this student old annual grade
					$t_grade_char_array = $ldisciplinev12->retrieveStudentConductGrade($t_student_id, $Key,'',1);
					$t_grade_char = $t_grade_char_array[$t_student_id];
					
					# add up the conduct score in all semesters
					$sum = 0;
					for($a=0; $a<sizeof($Value); $a++)
					{
						$sum+=$Value[$a];	
					}
					
					# Get the average of the total score by dividing the number of semester
					$pre_adj_total_score_avg = round($sum / $total_ratio);
			        
					# get this student annual conduct score
					$tmp_studentid[0] = $t_student_id;
					$ldisciplinev12_annual_record = $ldisciplinev12->retrieveAnnualConductBalanceGradeForGeneration($tmp_studentid,$Key);
					$ldisciplinev12_annual_record_ConductScore = $ldisciplinev12_annual_record[0]['ConductScore'];
					
					# add up semester conduct score and annual conduct score
					$total_score_avg = $pre_adj_total_score_avg + $ldisciplinev12_annual_record_ConductScore;
					
					# get the grade
					$new_grade_char = $ldisciplinev12->getGradeFromScore($total_score_avg,$Key,$t_conduct_semester);
					$orig_grade_char = $new_grade_char; // grade before any adjustment
					if($sys_custom['eDiscipline']['yy3'] && $custAdjustGrade)
					{
						$is_grade_adjusted = false;
						// if conduct score >= adjust base mark && grade > B- >>>> adjust grade to B-
						if(!$is_grade_adjusted && $total_score_avg >= $ldisciplinev12->ConductAdjustmentBaseMark && ($total_score_avg >= $B_minus_above_mark)
							 && isset($studentIdToMisconductAry[$t_student_id][$Key][$t_conduct_semester])
							&& $studentIdToMisconductAry[$t_student_id][$Key][$t_conduct_semester] > 0)
						{
							$new_grade_char = 'B-';
							$is_grade_adjusted = true;
						}
						
						if(count($downGradeItemIdToQuantity) > 0)
						{
							// check each down grade item setting
							foreach($downGradeItemIdToQuantity as $__item_id => $__quantity) {
								// if number of demerit item >= down grade item quantity
								if(!$is_grade_adjusted && ($total_score_avg >= $B_minus_above_mark) && isset($studentIdToDemeritAry[$t_student_id][$Key][$t_conduct_semester][$__item_id])
									&& $studentIdToDemeritAry[$t_student_id][$Key][$t_conduct_semester][$__item_id] > 0 
									&& $studentIdToDemeritAry[$t_student_id][$Key][$t_conduct_semester][$__item_id] >= $__quantity)
								{	
									// down one grade
									$new_grade_char = $nextGradeCharAry[$orig_grade_char];
									$is_grade_adjusted = true;
								}
							}
						}
					}
					
        			// if ($t_grade_char != $new_grade_char)
					{
						$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
								SET GradeChar = '$new_grade_char', ConductScore = '$pre_adj_total_score_avg' ";
						if($sys_custom['eDiscipline']['yy3']){
							if($custAdjustGrade && $is_grade_adjusted) {
								$sql.= ",AdjustedGradeBefore='$orig_grade_char' ";
							}else{
								$sql.= ",AdjustedGradeBefore=NULL ";
							}
						}
						$sql.= "WHERE StudentID = '$t_student_id'
								AND AcademicYearID = '$Key' AND IsAnnual=1";
						$ldisciplinev12->db_db_query($sql);
					}
				}
			}
		}
	}
}
else
{
	if($AcademicYearID != '')
	{
		if($ldisciplinev12->retriveConductMarkCalculationMethod() == 1)
		{
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."' ORDER BY YearTermID DESC";
			$MaxYearTermID = $ldisciplinev12->returnVector($sql);
			if($MaxYearTermID[0] == $YearTermID)		
			{
				// if the select YearTermID is the Final Year Term, udpate / create a ConductMarkBalance record
				for($i=0; $i<sizeof($array_students); $i++)
				{
					$sql = "SELECT COUNT(*) FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID = '".$AcademicYearID."' AND StudentID = '".$array_students[$i]."' AND IsAnnual = 1";
					$IsAnnualExist = $ldisciplinev12->returnVector($sql);
					
					// Get the Conduct mark for the final year term
					$sql = "SELECT ConductScore FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE AcademicYearID = '".$AcademicYearID."' AND StudentID = '".$array_students[$i]."' AND YearTermID = '".$YearTermID."'";
					$LatestCondcutMark = $ldisciplinev12->returnVector($sql);
					
					if($IsAnnualExist[0] == 0)
					{
						$sql = "INSERT INTO DISCIPLINE_STUDENT_CONDUCT_BALANCE(StudentID, IsAnnual, ConductScore, AcademicYearID, YearTermID) VALUES ('".$array_students[$i]."',1,'".$LatestCondcutMark[0]."','$AcademicYearID','0')";
						$ldisciplinev12->db_db_query($sql);
					}
					else
					{
						$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE SET ConductScore = '".$LatestCondcutMark[0]."' WHERE StudentID = '".$array_students[$i]."' AND AcademicYearID = '".$AcademicYearID."' AND IsAnnual = 1";
						$ldisciplinev12->db_db_query($sql);
					}
				}
			}
		}
		
		$student_data = $ldisciplinev12->retrieveSemesterConductBalanceGradeForGeneration($array_students,$AcademicYearID,$YearTermID);
	}
	else
	{
		$student_data = $ldisciplinev12->retrieveSemesterAllYearConductBalanceGradeForGeneration($array_students,$YearTermID);
	}
	
	for ($i=0; $i<sizeof($student_data); $i++)
	{
		list($t_student_id, $t_conduct_year, $t_conduct_semester,$t_conduct_score, $t_grade_char) = $student_data[$i];
		$new_grade_char = $ldisciplinev12->getGradeFromScore($t_conduct_score,$t_conduct_year);
		$orig_grade_char = $new_grade_char;
		if($sys_custom['eDiscipline']['yy3'] && $custAdjustGrade)
		{
			$is_grade_adjusted = false;
			// if conduct score >= adjust base mark && grade > B- >>>>> adjust grade to B-
			if(!$is_grade_adjusted && $t_conduct_score >= $ldisciplinev12->ConductAdjustmentBaseMark && ($t_conduct_score >= $B_minus_above_mark) 
				&& isset($studentIdToMisconductAry[$t_student_id][$t_conduct_year][$t_conduct_semester])
				&& $studentIdToMisconductAry[$t_student_id][$t_conduct_year][$t_conduct_semester] > 0)
			{
				$new_grade_char = 'B-';
				$is_grade_adjusted = true;
			}
			
			if(count($downGradeItemIdToQuantity) > 0)
			{
				// check each down grade item setting
				foreach($downGradeItemIdToQuantity as $__item_id => $__quantity) {
					// if number of demerit item >= down grade item quantity
					if(!$is_grade_adjusted && ($t_conduct_score >= $B_minus_above_mark) 
						 && isset($studentIdToDemeritAry[$t_student_id][$t_conduct_year][$t_conduct_semester][$__item_id]) 
						 && $studentIdToDemeritAry[$t_student_id][$t_conduct_year][$t_conduct_semester][$__item_id] > 0 
						&& $studentIdToDemeritAry[$t_student_id][$t_conduct_year][$t_conduct_semester][$__item_id] >= $__quantity)
					{
						// down one grade
						$new_grade_char = $nextGradeCharAry[$orig_grade_char];
						$is_grade_adjusted = true;
					}
				}
			}
		}
		
		if($ldisciplinev12->retriveConductMarkCalculationMethod() == 1)
		{
			$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
					SET GradeChar = '$new_grade_char' ";
			if($sys_custom['eDiscipline']['yy3'])
			{
				if($custAdjustGrade && $is_grade_adjusted) {
					$sql.= ",AdjustedGradeBefore='$orig_grade_char' ";
				}
				else{
					$sql.= ",AdjustedGradeBefore=NULL ";
				}
			}		
			$sql.= "WHERE StudentID = '$t_student_id'
					AND AcademicYearID = '$t_conduct_year' AND YearTermID= '$t_conduct_semester'";
			$ldisciplinev12->db_db_query($sql);
				
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '$t_conduct_year' ORDER BY TermStart DESC";
			$LastTermID = $ldisciplinev12->returnVector($sql);
			if($LastTermID[0] == $t_conduct_semester)
			{
				$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
						SET GradeChar = '$new_grade_char' ";
				if($sys_custom['eDiscipline']['yy3']){
					if($custAdjustGrade && $is_grade_adjusted) {
						$sql.= ",AdjustedGradeBefore='$orig_grade_char' ";
					}else{
						$sql.= ",AdjustedGradeBefore=NULL ";
					}
				}
				$sql.= "WHERE StudentID = '$t_student_id'
						AND AcademicYearID = '$t_conduct_year' AND IsAnnual = 1";
				$ldisciplinev12->db_db_query($sql);
			}
		}
		else
		{
			//if ($t_grade_char != $new_grade_char)
			{
				$sql = "UPDATE DISCIPLINE_STUDENT_CONDUCT_BALANCE
						SET GradeChar = '$new_grade_char' ";
				if($sys_custom['eDiscipline']['yy3']){
					if($custAdjustGrade && $is_grade_adjusted) {
						$sql.= ",AdjustedGradeBefore='$orig_grade_char' ";
					}else{
						$sql.= ",AdjustedGradeBefore=NULL ";
					}
				}
				$sql.= "WHERE StudentID = '$t_student_id'
						AND AcademicYearID = '$t_conduct_year' AND YearTermID= '$t_conduct_semester'";
				$ldisciplinev12->db_db_query($sql);
			}
		}
	}
}

# Update generation time
if($YearTermID == '0' || $YearTermID=='')
{
	$redirect_semester = '';
	$gen_semester = "Annual";
}
else
{
	$gen_semester = $YearTermID;
	$redirect_semester = $YearTermID;	
}

if($AcademicYearID == '0')
{
	$gen_year = "Year";
}
else
{
	$gen_year = $AcademicYearID;	
}
$ldisciplinev12->updateGenerateActionRecord($gen_year, $gen_semester);

header("Location: index.php?xmsg=grade_generated&Year=$AcademicYearID&Semester=$redirect_semester&ClassID=$RedirectClassID");

intranet_closedb();
?>