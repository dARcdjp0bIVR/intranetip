<?php
// Using : 

######## Change Log [Start] ################
#
#	Date:	2019-03-12 (Bill)   [2018-0710-1351-39240]
#			- Create file
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12_cust();

# Check access right
if(!($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || $sys_custom['eDiscipline']['GenerateMonthlyNotice'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Year
$AcademicYearID = Get_Current_Academic_Year_ID();
// Year Option
// $wholeYearOption = array();
// $wholeYearOption[0][0] = 0;
// $wholeYearOption[0]['YearTermID'] = 0;
// $wholeYearOption[0][1] = $Lang['General']['WholeYear'];
// $wholeYearOption[0]['TermName'] = $Lang['General']['WholeYear'];
// $wholeYearOption[0]['TermStart'] = getStartDateOfAcademicYear($AcademicYearID);
// $wholeYearOption[0]['TermEnd'] = getEndDateOfAcademicYear($AcademicYearID);

# Semester
$currentSemesterID = getCurrentSemesterID();
$Semesters = getSemesters($AcademicYearID, 0);

# Semester Selection
// $Semesters = array_merge($wholeYearOption, $Semesters);
$selectSemester = $linterface->GET_SELECTION_BOX($Semesters, " name='selectSemester' id='selectSemester' onChange='updateSemesterDisplay()'", "", $currentSemesterID);

# Generate Log
$generateLogArr = $ldiscipline->getGeneratedNoticeLog($AcademicYearID, '', '', '', '', false, false, $lastRecords=10);

# Generate Log Table
$generateLogTable = '';
$generateLogTable .= "<table width='100%' cellpadding='4' cellspacing='0'>";
$generateLogTable .= "<tr>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='12%'>".$Lang['General']['SchoolYear']."</td>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='12%'>".$Lang['General']['Semester']."</td>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='14%'>".$Lang['General']['StartDate']."</td>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='14%'>".$Lang['General']['EndDate']."</td>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='9%'>".$Lang['eDiscipline']['HYKPrintDisciplineNotice']['GeneratedCount']."</td>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='15%'>".$Lang['eDiscipline']['HYKPrintDisciplineNotice']['GenerateDate']."</td>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='18%'>".$Lang['eDiscipline']['HYKPrintDisciplineNotice']['GeneratedBy']."</td>";
    $generateLogTable .= "<td class='tablegreentop tabletopnolink' width='6%'>&nbsp;</td>";
$generateLogTable .= "</tr>";

if(empty($generateLogArr))
{
    // No Record
    $generateLogTable .= "<tr>";
        $generateLogTable .= "<td align='center' colspan='7'>";
            $generateLogTable .= $Lang['General']['NoRecordAtThisMoment'];
        $generateLogTable .= "</td>";
    $generateLogTable .= "</tr>";
}
else
{
    $year_class = new year_class();
    $year_class_ary = array();
    
    $tr_count = 0;
    $user_obj_ary = array();
    foreach((array)$generateLogArr as $thisGenerateLog)
    {
        // Display 10 records only
        if($tr_count > 9) {
            break;
        }
        
        $_tr_css = ($tr_count % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
        $_td_css = ($tr_count % 2 == 1) ? "attendanceouting" : "attendancepresent";
        
        # Year Term Name
        $year_term = getTargetYearSemesterByIDs($thisGenerateLog['AcademicYearID'], $thisGenerateLog['SemesterID']);
        $year_name_display = $year_term['YearName'];
        $term_name_display = $year_term['SemesterName'];
        
        # Notice Count
        $relatedNotice = $ldiscipline->getGeneratedNoticeContent($thisGenerateLog['GenerateLogID']);
        $relatedNoticeCount = count((array)$relatedNotice);
        
        # Input By
        $input_by_user_id = $thisGenerateLog['InputBy'];
        if(!isset($user_obj_ary[$input_by_user_id]))
        {
            $user_obj = new libuser($input_by_user_id);
            $user_obj_ary[$input_by_user_id] = $user_obj->UserName();
        }
        
        $generateLogTable .= "<tr class='".$_tr_css."'>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$year_name_display."</td>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$term_name_display."</td>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$thisGenerateLog['StartDate']."</td>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$thisGenerateLog['EndDate']."</td>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$relatedNoticeCount."</td>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$thisGenerateLog['InputDate']."</td>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$user_obj_ary[$input_by_user_id]."</td>";
            $generateLogTable .= "<td align='left' class='".$_td_css." tabletext'>".$linterface->GET_SMALL_BTN($Lang['Btn']['Delete'], 'button', 'deleteLogRecord('.$thisGenerateLog['GenerateLogID'].')')."</td>";
        $generateLogTable .= "</tr>";
        
        $tr_count++;
    }
}
$generateLogTable .= "</table>";

# Page
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Management_HYKPrintDisciplineNotice";

# Tags
$TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['Single'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['Monthly'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Sub-Tags
$SUB_TAGS_OBJ = array();
$SUB_TAGS_OBJ[] = array($Lang['Btn']['Print'], 'index.php?curSubType=monthly', 0);
$SUB_TAGS_OBJ[] = array($Lang['eDiscipline']['HYKPrintDisciplineNotice']['GenerateMonthlyLetter'], 'javascript: void(0);', 1);
$htmlAry['SUB_TAGS'] = $linterface->GET_SUBTAGS($SUB_TAGS_OBJ);

$message = $msg=='generate'? $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['Success'] : '';
$message = $msg=='deleted'? $Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['Deleted'] : $message;
$linterface->LAYOUT_START($message);
?>

<script type="text/javascript" language="JavaScript">
// Semester Data
var SemesterStartDate = new Array();
var SemesterEndDate = new Array();
<? for($i=0; $i<count((array)$Semesters); $i++){
	$thisSemester = $Semesters[$i];
	echo "SemesterStartDate[".$thisSemester["YearTermID"]."] = '".substr($thisSemester["TermStart"], 0, 10)."'; \r\n";
	echo "SemesterEndDate[".$thisSemester["YearTermID"]."] = '".substr($thisSemester["TermEnd"], 0, 10)."'; \r\n";
} ?>

function showRankTargetResult()
{
	var yearClassId = '';
	
	var url = "../../reports/ajaxGetLive.php?target=class";
	url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	url += "&academicYearId=<?=$AcademicYearID?>";
	
	// get class list
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	
	// Start Date
	var start_date = $.trim($('#StartDate').val());
	if(start_date == ''){
		valid = false;
		$('#StartDate').focus();
	}
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('StartDate'))){
			valid = false;
			$('#StartDate').focus();
		}
	}
	if(!valid) return;
	
	// End Date
	var end_date = $.trim($('#EndDate').val());
	if(end_date == ''){
		valid = false;
		$('#EndDate').focus();
	}
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('EndDate'))){
			valid = false;
			$('#EndDate').focus();
		}
	}
	if(!valid) return;
	
	// Date Range
	if(compareDate(end_date, start_date) < 0){
		valid = false;
		$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
		$('#DateWarnDiv').show();
		$('#EndDate').focus();
	}
	else{
		$('#DateWarnDiv').hide();
	}
	if(!valid) return;
	
	// Issue Date
	var issue_date = $.trim($('#IssueDate').val());
	if(issue_date == '') {
		// do nothing
	}
	else {
		if(!check_date_without_return_msg(document.getElementById('IssueDate'))){
			valid = false;
			$('#IssueDate').focus();
		}
	}
	if(!valid) return;
	
	// Semester
	var semester_id = $('#selectSemester').val();
	var semester_start = SemesterStartDate[semester_id];
	var semester_end = SemesterEndDate[semester_id];
	if(compareDate(start_date, semester_start) < 0 || compareDate(semester_end, end_date) < 0) {
		valid = false;
		$('#DateWarnDiv span').html('<?=$Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['OutOfTermPeriod']?>');
		$('#DateWarnDiv').show();
		$('#textFromDate').focus();
	}
	if(!valid) return;
	
	generateDateChecking();
	
	// Class
// 	if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
// 		valid = false;
//		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
// 		$('#TargetWarnDiv').show();
// 	}
// 	else{
// 		$('#TargetWarnDiv').hide();
// 	}
// 	if(!valid) return;
// 	form_obj.submit();
//	
//	// submit
// 	$('input#format').val(format);
// 	if(format == 'web'){
// 		form_obj.attr('target','');
// 		Block_Element('PageDiv');
//		
// 		$.post(
// 			'generate.php',
// 			$('#form1').serialize(),
// 			function(data){
// 				$('#ReportDiv').html(data);
// 				document.getElementById('div_form').className = 'report_option report_hide_option';
//				
// 				$('.spanShowOption').show();
// 				$('.spanHideOption').hide();
// 				$('.Form_Span').hide();
//				
// 				UnBlock_Element('PageDiv');
// 			}
// 		);
// 	}
// 	else if(format == 'csv' || format == 'word'){
// 		form_obj.attr('target','');
// 		form_obj.submit();
// 	}
// 	else if(format == 'print'){
// 		form_obj.attr('target','_blank');
//		form_obj.submit();
// 	}
}

function updateSemesterDisplay()
{
	var semester_id = $('#selectSemester').val();
	var semester_start = SemesterStartDate[semester_id];
	var semester_end = SemesterEndDate[semester_id];
	
	// Update semester display
	$('#semesterStart').html(semester_start);
	$('#StartDate').val(semester_start);
	$('#EndDate').val(semester_end);
	
	$('#DateWarnDiv').hide();
}

function generateDateChecking()
{
	$.ajax({
		type: "POST",
		url : "ajaxFormChecking.php",
		data:{
			"AcademicYearID": <?=$AcademicYearID?>,
			"SemesterID": $('#selectSemester').val(),
			"StartDate": $('#StartDate').val(),
			"EndDate": $('#EndDate').val()
		},
		success: function(count){
			if(count > 0) {
				var confirmMsg = '<?=$Lang['eDiscipline']['HYKPrintDisciplineNotice']['Message']['ReplaceOverlapLog']?>';
// 				confirmMsg = confirmMsg.replace("<!--COUNT-->", count);
				if(confirm(confirmMsg)) {
					form1.submit();
				}
			}
			else {
				form1.submit();
			}
		}
	});
}

function deleteLogRecord(RecordID)
{
	if(confirm('<?=$Lang['General']['JS_warning']['ConfirmDelete']?>')) {
		Block_Document();
		$('#targetLogID').val(RecordID);
		
		var form_obj = $('#form1');
		form_obj.attr('action', 'remove_update.php');
		form_obj.submit();
	}
}

$(document).ready(function(){
	//showRankTargetResult();
	updateSemesterDisplay();
});
</script>

<div id="div_form">
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="generate_update.php" onsubmit="return false;">
		
		<?= /*$linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['eDiscipline']['GenerateMonthlyNoticeInstruction'], $others="")*/ '' ?>
		<?= $htmlAry['SUB_TAGS'] ?>
		<br/>
		
		<table class="form_table_v30" width="100%" border="0" cellspacing="0" cellpadding="0">
			
			<!-- Year -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
				<td><?=getAYNameByAyId($AcademicYearID)?></td>
			</tr>
			
			<!-- Semester -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['General']['Semester']?></td>
				<td><?=$selectSemester?></td>
			</tr>
			
			<!-- Semester Start Date -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['eDiscipline']['HYKClassSummaryReport']['SemesterStart']?></td>
				<td id="semesterStart"></td>
			</tr>
			
			<!-- Date -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HYKMeritStatisticsReport']['Period']?></td>
				<td><table class="inside_form_table">
						<tr><td>
							<?=$linterface->GET_DATE_PICKER("StartDate", "", "")?>
							<?=$i_To?>
							<?=$linterface->GET_DATE_PICKER("EndDate", "", "")?>
						</td></tr>
				</table>
				<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
				</td>
			</tr>
			
			<!-- Issue Date -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['eDiscipline']['HYKPrintDisciplineNotice']['IssueDate']?></td>
				<td><table class="inside_form_table">
						<tr><td>
							<?=$linterface->GET_DATE_PICKER("IssueDate", date("Y-m-d"), "", "yy-mm-dd", "", "", "", "", "", 0, $CanEmptyField=1)?>
						</td></tr>
				</table>
				<?=$linterface->Get_Form_Warning_Msg("IssueDateWarnDiv","")?>
				</td>
			</tr>
		
			<!-- Target -->
			<!-- 
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_Discipline_Class?></td>
				<td><table class="inside_form_table">
						<tr>
							<td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
							</td>
						</tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
			</td></tr>
			 -->
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "submitForm('print')")?>
			<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="format" id="format" value="generate">
		<input type="hidden" name="YearID" id="YearID" value="<?=$AcademicYearID?>">
		<input type="hidden" name="targetLogID" id="targetLogID" value="">
		</form>
	</span>
</div>

<?= $linterface->GET_NAVIGATION2_IP25($Lang['eDiscipline']['GenerateMonthlyNoticeLatestRecords'])?>
<div><?=$generateLogTable?></div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>