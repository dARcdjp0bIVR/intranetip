<?php
// Using :

######## Change Log [Start] ################
#
#	Date:	2019-03-15 (Bill)   [2018-0710-1351-39240]
#			- Create file
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$targetLogID = $_POST['targetLogID'];

$ldiscipline = new libdisciplinev12_cust();
$targetLogInfo = $ldiscipline->getGeneratedNoticeLog($AcademicYearID='', $SemesterID='', $StartDate='', $EndDate='', $LogID=$targetLogID, $forSelection=false, $checkOverlap=false);

# Delete Generate Log
if(!empty($targetLogInfo))
{
    $targetLogInfo = $targetLogInfo[0];
    
    $dataAry = array();
    $dataAry['YearID'] = $targetLogInfo['AcademicYearID'];
    $dataAry['selectSemester'] = $targetLogInfo['SemesterID'];
    $dataAry['StartDate'] = $targetLogInfo['StartDate'];
    $dataAry['EndDate'] = $targetLogInfo['EndDate'];
    $ldiscipline->removeGeneratedNoticeLog($dataAry);
}

header("Location: generate.php?msg=deleted");

?>