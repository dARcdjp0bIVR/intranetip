<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

$userID = $_SESSION['UserID'];

$yearID = Get_Current_Academic_Year_ID();

# Semester Menu
$semester_data = getSemesters($yearID);
$selectSemesterMenu = "<select name=\"semester\" id=\"semester\">";
$selectSemesterMenu .= "<option value='#'>-- ".$button_select." --</option>";
foreach($semester_data as $key=>$val) {
	$selectSemesterMenu .= "<option value=\"".$key."\"";
	$selectSemesterMenu .= ($semester==$key) ? " selected" : "";
	$selectSemesterMenu .= ">".$val."</option>";
}
$selectSemesterMenu .= "</select>";

$termID = array();
if(sizeof($semester_data)>0) {
	$termID = array_keys($semester_data);
	$termConds = " AND st.YearTermID IN (".implode(',', $termID).")";
}

//$selectSemesterMenu = getSelectSemester2('name="semester" id="semester"', $semester,2, $yearID);

# Form Menu
if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	
	$formAry = $lclass->getLevelArray();
} else {
	/*
	$sql = "SELECT
				y.YearID,
				y.YearName
			FROM
				INTRANET_USER USR INNER JOIN 
				SUBJECT_TERM_CLASS_TEACHER stct ON (stct.UserID=USR.UserID) INNER JOIN 
				SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.SubjectGroupID=stct.SubjectGroupID) INNER JOIN
				SUBJECT_TERM st ON (st.SubjectGroupID=stct.SubjectGroupID $termConds) INNER JOIN
				YEAR_CLASS_TEACHER yct ON (yct.UserID=USR.UserID) INNER JOIN 
				YEAR_CLASS yc ON (yc.YearClassID=yct.YearClassID AND yc.AcademicYearID=$yearID) INNER JOIN  
				YEAR y ON (y.YearID=stcyr.YearID OR y.YearID=yc.YearID)
			WHERE
				USR.UserID=$userID
			GROUP BY 
				y.YearID
			ORDER BY 
				y.Sequence
			";
	*/
	$sql = "SELECT 
				y.YearID, y.YearName 
			FROM 
				INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER yct ON (yct.UserID=USR.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_TEACHER stct ON (stct.UserID=USR.UserID) LEFT OUTER JOIN 
				SUBJECT_TERM st ON (st.SubjectGroupID=stct.SubjectGroupID $termConds) LEFT OUTER JOIN 
				SUBJECT_TERM_CLASS_USER stcu ON (stcu.SubjectGroupID=st.SubjectGroupID) LEFT OUTER JOIN 
				YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) LEFT OUTER JOIN 
				YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID OR yc.YearClassID=yct.YearClassID) LEFT OUTER JOIN 
				YEAR y ON (y.YearID=yc.YearID) 
			WHERE 
				USR.UserID='$UserID' AND yc.AcademicYearID='$yearID' 
			GROUP BY 
				y.YearID 
			ORDER BY 
				y.Sequence";
	$formAry = $ldiscipline->returnArray($sql, 2);
	
}


$selectFormMenu = "<select name='form' id='form'>";
$selectFormMenu .= "<option value='#'>-- ".$button_select." --</option>";
for($i=0; $i<sizeof($formAry); $i++) {
	if($formAry[$i][0]!="") {
		$selectFormMenu .= "<option value='{$formAry[$i][0]}'";
		$selectFormMenu .= ($form==$formAry[$i][0]) ? " selected" : "";
		$selectFormMenu .= ">{$formAry[$i][1]}</option>";
	}
}
$selectFormMenu .= "</select>";

$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 1);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['AbsentInConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absentList.php", 0);

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


?>
<script language="javascript">
<!--
function showResult(semester, form) {
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	document.getElementById("spanStudent").innerHTML = "<?=$i_general_loading?>";
	var url = "";
		
	url = "get_live.php?year="+document.getElementById('year').value;
	url += "&semester=" + document.getElementById('semester').value;
	url += "&form=" + document.getElementById('form').value;

	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanStudent").innerHTML = xmlHttp.responseText;
		document.getElementById("spanStudent").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function checkForm(obj) {
	if(obj.semester.value == "#") {
		alert("<?=$i_general_please_select." ".$i_Discipline_System_Conduct_Semester?>");	
		return false;
	}
	if(obj.form.value == "#") {
		alert("<?=$i_general_please_select." ".$i_Discipline_Form?>");	
		return false;
	}
	showResult(document.getElementById('semester').value, document.getElementById('form').value);
}

function goSubmit() {
	document.form1.submit();	
}
//-->
</script>
<form name="form1" method="POST" action="studentList.php">
<br />
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<? if(sizeof($formAry)!=0) { ?>
	<tr valign="top">
		<td class="tabletext" align="center">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td width="80%" class="tablerow1"><?=$selectSemesterMenu?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Form?></td>
					<td width="80%" class="tablerow1"><?=$selectFormMenu?></td>
				</tr>
				<tr>
					<td width="20%">&nbsp;</td>
					<td width="80%" class="tablerow1"><?= $linterface->GET_ACTION_BTN($button_submit, "button", "return checkForm(document.form1);")?></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<span id="spanStudent"></span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<? } else { ?>
	<tr valign="top" align="center">
		<td class="tabletext"><?=$iDiscipline['NoAssessmentIsNeeded']?></td>
	</tr>	
	<? } ?>
</table>
<br />
<input type="hidden" name="year" id="year" value="<?=Get_Current_Academic_Year_ID()?>">
<input type="hidden" name="viewList" id="viewList" value="<?=$viewList?>">
<input type="hidden" name="class" id="class" value="">
</form>
<script language="javascript">
<!--
if(document.getElementById('viewList').value==1) {
	showResult(document.getElementById('semester').value, document.getElementById('form').value);	
}
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
