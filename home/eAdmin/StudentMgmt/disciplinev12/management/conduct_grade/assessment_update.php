<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$gradeString = array();

if($year && $semester && $form && $uid) {
	$gradeString = "1:$radio_1,2:$radio_2,3:$radio_3,4:$radio_4";

	$ldiscipline->updateConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT", $year, $semester, $form, $class, $uid, $gradeString);
}

intranet_closedb();


if($goNext!='')		# submit & next (student)
	header("Location: studentAssessment.php?year=$year&semester=$semester&form=$form&class=$class&uid=$goNext");
else 				# submit & back to student list
	header("Location: studentList.php?year=$year&semester=$semester&form=$form&class=$class");

?>