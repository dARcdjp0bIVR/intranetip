<?php

#############################################
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if(!isset($class))
	header("Location: index.php");

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
	

//$year = getCurrentAcademicYear();
$year = Get_Current_Academic_Year_ID();

$levelName = $ldiscipline->getFormByLevelID($form);

$stdInfo = $ldiscipline->getStudentNameByClassName($class);

//$APCategory = $ldiscipline->getAPCategoryInfo();
$APTagInfo = $ldiscipline->getAPTagInfo();
$IntegratedGradeArr = $ldiscipline->getStudentConductGrade($year, $semester, $class, "");


$tableTop = "";
$tableContent = "";
$conductGrade = array();

$tableTop = "
	<tr class='tablebluetop'>
		<td class='tabletopnolink'>$i_UserClassNumber</td>
		<td class='tabletopnolink'>$i_UserStudentName</td>";
		
	for($j=0; $j<sizeof($APTagInfo); $j++)	{		
		$tableTop .= "<td class='tabletopnolink'>".$APTagInfo[$j]['TagName']."</td>";
	}	
		
$tableTop .= "<td class='tabletopnolink'>".$iDiscipline['IntegratedConductGrade']."</td>
		<td class='tabletopnolink'>$i_Discipline_System_Discipline_Conduct_Last_Updated</td>
	</tr>";

$k = 0;
for($i=0; $i<sizeof($stdInfo); $i++) {
	list($uid, $name, $clsName, $clsNo) = $stdInfo[$i];
	
	$classTeacherIDs = $ldiscipline->getClassTeacherByStudentID($uid, $year);
	$subjectGroupTeacherIDs = $ldiscipline->getSubjectGroupTeacherList($year, $semester, $uid);

	# teacher either subject teacher or not class teacher of this student
	//if(!in_array($_SESSION['UserID'], $classTeacherIDs) && !in_array($_SESSION['UserID'], $subjectGroupTeacherIDs) && !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) break;
	
	if(in_array($_SESSION['UserID'], $classTeacherIDs) || in_array($_SESSION['UserID'], $subjectGroupTeacherIDs) || $ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
		$css = ($k%2==0) ? 1 : 2;
		$sql = "SELECT conductString, LEFT(DateModified,10) FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE AcademicYearID='$year' AND YearTermID='$semester' AND StudentID=$uid AND UserID=".$_SESSION['UserID'];
		$grade = $ldiscipline->returnArray($sql);
		$grade = $grade[0];
	
		$tempGrade = explode(",",$grade[0]);
		
		$text = "";
		for($j=0; $j<sizeof($tempGrade); $j++) {
			$g = $tempGrade[$j];
			$line = explode(":",$g);
			list ($tagid, $gradeNew) = $line;
			if($tagid!="" && $uid!="")	
				$conductGrade[$uid][$tagid] = $gradeNew;
		}
	
		$tableContent .= "	<tr class='tablebluerow$css'>
								<td class='tabletext'>$clsNo</td>
								<td class='tabletext'><a href='javascript:;' onClick='goSubmit($uid);' class='tablelink'>$name</a></td>";
		for($j=0; $j<sizeof($APTagInfo); $j++)	{
			$tableContent .= "<td class='tabletext'>";
			$tableContent .= (isset($conductGrade[$uid][$APTagInfo[$j]['TagID']])) ? $conductGrade[$uid][$APTagInfo[$j]['TagID']] : "&nbsp;";	
			$tableContent .= "</td>";
		}
			
							
		$tableContent .= (isset($IntegratedGradeArr[$uid])) ? "<td class='tabletext'>$IntegratedGradeArr[$uid]</td>" : "<td>&nbsp;</td>";
								
		$tableContent .= ($grade[1]) ? "<td class='tabletext'>$grade[1]</td>" : "<td>&nbsp;</td>";
		$tableContent .= "</tr>";
		
		$k++;
	}	 
}

$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 1);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['AbsentInConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absentList.php", 0);

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# navigation bar
$PAGE_NAVIGATION[] = array($i_Discipline_Student_List, "index.php?semester=$semester&form=$form&class=$class&viewList=1");
$PAGE_NAVIGATION[] = array($ldiscipline->getClassNameByClassID(substr($class,2)), "");

# Start layout
$linterface->LAYOUT_START();


?>
<script language="javascript">
<!--

function goSubmit(uid) {
	document.getElementById('uid').value = uid;
	document.form1.submit();	
}
//-->
</script>
<form name="form1" method="POST" action="studentAssessment.php">
<br />
<table width="100%">
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
	<tr>
</table>
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td width="80%" class="tablerow1"><?=$ldiscipline->getTermNameByTermID($semester)?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Form?></td>
					<td width="80%" class="tablerow1"><?=$levelName?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Class?></td>
					<td width="80%" class="tablerow1"><?=$ldiscipline->getClassNameByClassID(substr($class,2))?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<?=$tableTop?>
	<?=$tableContent?>
	<tr>
		<td colspan="8" align="center">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="8" align="center"><?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='index.php?semester=$semester&form=$form&viewList=1'")?></td>
	</tr>
</table>
<p>&nbsp</p>
<input type="hidden" name="year" id="year" value="<?=$year?>">
<input type="hidden" name="semester" id="semester" value="<?=$semester?>">
<input type="hidden" name="form" id="form" value="<?=$form?>">
<input type="hidden" name="class" id="class" value="<?=$class?>">
<input type="hidden" name="uid" id="uid" value="">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
