<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();



$ldiscipline = new libdisciplinev12();

$userID = $_SESSION['UserID'];

$yearID = Get_Current_Academic_Year_ID();

$result = $ldiscipline->getTeachingClassByClassLevelID($form);

$table =  "<table width='100%' cellpadding='4' cellspacing='0' border='0'>";
$table .= "		<tr class='tablebluetop'>";
$table .= "			<td class='tabletopnolink'>$i_general_class</td>";
$table .= "			<td class='tabletopnolink'>".$iDiscipline['teacher']."</td>";
$table .= "			<td class='tabletopnolink'>".$iDiscipline['CompleteRatio']."</td>";
$table .= "			<td class='tabletopnolink'>$i_Discipline_System_Discipline_Conduct_Last_Updated</td>";
$table .= "		</tr>";

$k = 0;


for($i=0; $i<sizeof($result); $i++) {
	
	$clsID = $result[$i][0];
	$clsName = $result[$i][1];

	$sql = "SELECT
				CLSTCH.YearClassTeacherID as CT,
				SUBTCH.SubjectClassTeacherID as ST
			FROM
				YEAR_CLASS yc LEFT OUTER JOIN
				YEAR_CLASS_TEACHER CLSTCH ON (yc.YearClassID=CLSTCH.YearClassID AND CLSTCH.UserID=$userID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.YearID=yc.YearID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER SUBTCH ON (SUBTCH.SubjectGroupID=stcyr.SubjectGroupID AND SUBTCH.UserID=$userID) LEFT OUTER JOIN
				INTRANET_USER USR ON (CLSTCH.UserID=USR.UserID OR SUBTCH.UserID=USR.UserID) LEFT OUTER JOIN
				ACADEMIC_YEAR_TERM AYT ON (AYT.AcademicYearID=yc.AcademicYearID)
				
			WHERE 
				USR.UserID=$userID AND 
				(yc.ClassTitleEN='$clsName' OR yc.ClassTitleB5='$clsName') AND
				USR.RecordType=1 AND
				USR.RecordStatus IN (0,1,2) AND
				yc.AcademicYearID = $yearID	AND
				yc.YearID = $form AND
				AYT.YearTermID=$semester
			GROUP BY yc.YearClassID
			";
				
	$detail = $ldiscipline->returnArray($sql);
	$data = $detail[0];
	
	
	$stdid = array();
	$StudentTotal = 0;
	$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($clsName);

	for($j=0; $j<sizeof($stdInfo); $j++) {
		list($clsNo, $name, $sid) = $stdInfo[$j];
		
		$classTeacherIDs = $ldiscipline->getClassTeacherByStudentID($sid, $year);
		$subjectGroupTeacherIDs = $ldiscipline->getSubjectGroupTeacherList($year, $semester, $sid);
	
		# teacher either subject teacher or not class teacher of this student
		//if(!in_array($_SESSION['UserID'], $classTeacherIDs) && !in_array($_SESSION['UserID'], $subjectGroupTeacherIDs) && !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) break;
		if(in_array($_SESSION['UserID'], $classTeacherIDs) || in_array($_SESSION['UserID'], $subjectGroupTeacherIDs) || $ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
			$stdid[] = $sid;
		}
		
	}
	
	if($stdid!="") {
		if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
			$classIDAry[0] = $clsID;
			$studentTotal = sizeof($ldiscipline->getStudentListByClassID($classIDAry));
		} else{ 
			$studentTotal = sizeof($stdid);
		}
	}
	
	$sql = "SELECT COUNT(*), MAX(LEFT(DateModified,10)) FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE AcademicYearID='$year' AND YearTermID='$semester' AND StudentID IN (".implode(',',$stdid).") AND UserID=".$userID;
	$temp = $ldiscipline->returnArray($sql, 2);
	
	$tempData = $temp[0];
	
	$markedStudent = ($tempData[0] != "") ? $tempData[0] : 0;

	if($studentTotal!=0) {
		$css = ($k%2==0) ? 1 : 2;
		$table .= "		<tr class='tablebluerow$css'>";
		$table .= "		<td class='tabletext'><a href='javascript:;' onclick=\"$('#class').val('::{$clsID}');goSubmit();\" class='tablelink'>{$clsName}</a></td>";
		$table .= "		<td class='tabletext'>";
		$table .= ($data[0]!="") ? $i_Teaching_ClassTeacher : (($data[1]!="") ? $i_Teaching_SubjectTeacher : $eDiscipline["AdminUser"]);
		$table .= "		</td>";
		$table .= "		<td class='tabletext'><font color='#FF0000'>$markedStudent</font>/$studentTotal</td>";
		$table .= "		<td class='tabletext'>$tempData[1]</td>";
		$table .= "	</tr>";
	}
}
$table .= "</table>";

intranet_closedb();

echo $table;
?>