<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$student_selected = $linterface->GET_SELECTION_BOX($array, "name='studentID[]' id='studentID[]' class='select_studentlist' size='6' multiple='multiple'", "");

$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['AbsentInConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absentList.php", 0);
$TAGS_OBJ[] = array($ec_iPortfolio['assign_more_student'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/studentList.php", 1);

$PAGE_NAVIGATION[] = array($ec_iPortfolio['assign_more_student']." ".$i_To." ".$iDiscipline['ConductGradeMeeting'], "");

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function checkForm(obj) {
	checkOptionAll(obj.elements['studentID[]']);
	return true;
}
//-->
</script>
<form name="form1" method="POST" action="studentList_update.php" onSubmit="return checkForm(document.form1)">
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="60%"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		<td width="40%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
	</tr>
</table>
<br>
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
	<td class="tabletext" align="center">
		<table width="100%" cellpadding="4" cellspacing="0" border="0">
			<tr>
				<td width="20%" class="formfieldtitle"><?=$i_general_selected_students?></td>
				<td width="80%" class="tablerow1">
					<?=$student_selected?>
					<? echo "<br>".$linterface->GET_BTN($button_select, "button","newWindow('../../choose_student.php?fieldname=studentID[]',11)")."&nbsp;".
					$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.getElementById('studentID[]'))")?>
				</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="80%" class="tablerow1"><?= $linterface->GET_ACTION_BTN($button_submit, "submit")?></td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<br />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
