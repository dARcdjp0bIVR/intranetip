<?php

#############################################
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$li = new libdbtable2007(0,0,0);

$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagID";
$TagAry = $ldiscipline->returnArray($sql, 2);

$form = str_replace("__", " ", $form);
$form = str_replace("_", ".", $form);


$yearID = Get_Current_Academic_Year_ID();

if($statusType==1) {	# whole student list

	if($class!='0') {		# with "class" value
		$classID = $ldiscipline->getClassIDByClassName($class, $yearID);
		$studentIDAry = $ldiscipline->storeStudent('0', '::'.$classID, $yearID);
	} else if($form!='0') {	# with form value
		$levelID = $ldiscipline->getFormIDByFormName($form);
		$studentIDAry = $ldiscipline->storeStudent('0', $levelID, $yearID);
	} else {				# all students
		$studentIDAry = $ldiscipline->storeStudent('0', '0', $yearID);
	}
	
	$i = 0;
	foreach($studentIDAry as $sid) {
		$sql = "SELECT 
					ycu.UserID, 
					".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN')." as ClassName, 
					y.YearName as LevelName, 
					FINAL.isAdjust, 
					yc.YearClassID, 
					y.YearID, 
					FINAL.LockStatus,
					CONCAT('<input type=\"checkbox\" name=\"studentid[]\" value=\"',ycu.UserID,'\">')
				FROM
					YEAR_CLASS_USER ycu LEFT OUTER JOIN
					YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$yearID) LEFT OUTER JOIN
					YEAR y ON (y.YearID=yc.YearID) LEFT OUTER JOIN
					DISCIPLINE_MS_STUDENT_CONDUCT_FINAL FINAL ON (FINAL.StudentID=ycu.UserID AND FINAL.YearTermID=$semester) INNER JOIN
					INTRANET_USER USR ON (USR.UserID=ycu.UserID)
				WHERE
					ycu.UserID=$sid AND yc.AcademicYearID=$yearID
		";

		$temp = $ldiscipline->returnArray($sql, 8);
		$meetingStudentList[$i][0] = $temp[0][0];
		$meetingStudentList[$i][1] = $temp[0][1];
		$meetingStudentList[$i][2] = $temp[0][2];
		$meetingStudentList[$i][3] = $temp[0][3];
		$meetingStudentList[$i][4] = $temp[0][4];
		$meetingStudentList[$i][5] = $temp[0][5];
		$meetingStudentList[$i][6] = $temp[0][6];
		$meetingStudentList[$i][7] = $temp[0][7];
		$i++;
	}
} else {		# student list from conduct grade assessment

	if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
		$meetingStudentList = $ldiscipline->getConductMeetingStudentListByAdmin($year, $semester, $form, $class, $isAdjust);
	} else {
		$meetingStudentList = $ldiscipline->getConductMeetingStudentList($year, $semester, $form, $class, $isAdjust);
	}
}

//debug_pr($meetingStudentList);
//debug_pr($_GET);
?>
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td colspan="13" align="right" valign="top">
			<input type="button" name="toLock" id="toLock" value="Lock" onClick="ChangeLockStatus(1)">&nbsp;<input type="button" name="toUnlock" id="toUnlock" value="Unlock" onClick="ChangeLockStatus(0)">
		</td>
	</tr>
	<tr class="tablebluetop">
		<td class='tabletopnolink'>&nbsp;</td>
		<td class='tabletopnolink'><?=$i_general_class?></td>
		<td class='tabletopnolink'><?=$i_UserClassNumber?></td>
		<td class='tabletopnolink'><?=$i_UserStudentName?></td>
		<?
		for($i=0; $i<sizeof($TagAry); $i++) {
			echo "<td class='tabletopnolink'>".$TagAry[$i][1]."</td>";	
		}
		?>
		<td class='tabletopnolink'><?=$iDiscipline['Overall']?></td>
		<td class='tabletopnolink'><?=$i_Discipline_System_Discipline_Conduct_Last_Updated?></td>
		<td class='tabletopnolink' align='center'><?=$i_general_status?></td>
		<td class='tabletopnolink' align='center'><?=$i_Discipline_System_Award_Punishment_Locked?></td>
		<td class='tabletopnolink' align='center'><?=$li->check("studentid[]")?></td>
	</tr>
	<?
	for($i=0; $i<sizeof($meetingStudentList); $i++) {
		$css = ($i%2==0) ? 1 : 2;
		list($studentid, $className, $formName, $isAdjust, $clsID, $yearID, $lockStatus, $checkbox) = $meetingStudentList[$i];
		$lockText = ($lockStatus) ? $i_general_yes : $i_general_no;
		
		$sql = "SELECT needMeeting, isAdjust FROM DISCIPLINE_MS_STUDENT_CONDUCT_FINAL WHERE StudentID=$studentid AND AcademicYearID=$year AND YearTermID=$semester";
		$tempResult = $ldiscipline->returnArray($sql, 2);
		list($tmpNeedMeeting, $tmpIsAdjust) = $tempResult[0];

		$AdjustStatus = (sizeof($tempResult)==0 || ($tmpNeedMeeting==0 && $tmpIsAdjust!=2)) ? $i_StaffAttendance_Status_Normal : (($isAdjust==1) ? $iDiscipline['Revised'] : (($isAdjust==2) ? $Lang['eDiscipline']['SpecialCaseRevised'] : $iDiscipline['WaitingForReview']));
		
		$stdInfo = $ldiscipline->getStudentNameByID($studentid);
		//debug_pr($stdInfo);
		list($tempID, $tempName, $tempClsName, $tempClsNo) = $stdInfo[0];
	
		$sql = "SELECT conductString, IntegratedConductGrade, LEFT(DateModified,10) FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE AcademicYearID='$year' AND YearTermID='$semester' AND StudentID=$studentid AND UserID=".$_SESSION['UserID'];
		$grade = $ldiscipline->returnArray($sql);
		$grade = $grade[0];
		
		$tempGrade = explode(",",$grade[0]);
		
		$text = "";
		for($j=0; $j<sizeof($tempGrade); $j++) {
			$g = $tempGrade[$j];
			$line = explode(":",$g);
			list ($catid, $gradeNew) = $line;
			if($catid!="" && $studentid!="")	
				$conductGrade[$studentid][$catid] = $gradeNew;
		}
		
		$asterisk = ($statusType==1 && sizeof($tempResult)!=0 && $tmpNeedMeeting!=0) ? "<span class=\"tabletextrequire\">*</span>" : "&nbsp;";
		
		echo "<tr class='tablebluerow$css'>
			<td class='tabletext'>$asterisk</td>
			<td class='tabletext'>$tempClsName</td>
			<td class='tabletext'>$tempClsNo</td>
			<td class='tabletext'><a href='javascript:;' onclick='goSubmit($tempID);' class='tablelink'>$tempName</a></td>";
		
		for($j=0; $j<sizeof($TagAry); $j++) {
			echo "<td class='tabletext'>".$conductGrade[$studentid][$TagAry[$j][0]]."</td>";
		}
		
		
		echo "<td class='tabletext'>$grade[1]</td>
			<td class='tabletext'>$grade[2]</td>
			<td class='tabletext' align='center'>$AdjustStatus</td>
			<td class='tabletext' align='center'>$lockText</td>
			<td class='tabletext' align='center'>$checkbox</td></tr>";
		
	}
	?>
</table>
<?
if($statusType==1) {
	echo "<br>".$Lang['eDiscipline']['NeedToReassessInConductMeeting'];	
}

intranet_closedb();
?>