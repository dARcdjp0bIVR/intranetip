<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!isset($RecordID)) {
	header("Location: absentList.php");	
	exit();
}

$result = true;

# check whether current record is exist
$sql = "SELECT count(*) FROM DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST WHERE RecordID=$RecordID";
$exist = $ldiscipline->returnVector($sql);

if($exist[0]!=0) {
	# check any desired record is already exist
	$sql = "SELECT count(*) FROM DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST WHERE YearID=$form AND UserID=$teacherID AND RecordID!=$RecordID";
	$existNew = $ldiscipline->returnVector($sql);

	if($existNew[0]==0) {
		$sql = "UPDATE DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST SET YearID=$form, RecordType=$markingType WHERE RecordID=$RecordID";
		$ldiscipline->db_db_query($sql);
	} else {
		$result = false;
	}
}
intranet_closedb();

$msgFlag = ($result) ? "update" : "update_failed";

header("Location: absentList.php?RecordType=$RecordType&form=$original_form&xmsg=$msgFlag");
?>
