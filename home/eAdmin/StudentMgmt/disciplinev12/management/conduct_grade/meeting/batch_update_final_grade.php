<?php
# modifying by : 

#############################################
#
#	Date:	2016-07-11	Bill
#			change split() to explode(),  for PHP 5.4
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# select each student
//$sql = "SELECT Distinct StudentID FROM DISCIPLINE_MS_STUDENT_CONDUCT_FINAL WHERE AcademicYearID=2 AND YearTermID=3 AND StudentID=1760";
$sql = "SELECT Distinct StudentID FROM DISCIPLINE_MS_STUDENT_CONDUCT_SEMI WHERE AcademicYearID=3 AND StudentID=19153";
$student = $ldiscipline->returnVector($sql);

# retrieve student grades given by teachers during conduct grade meeting
for($i=0; $i<sizeof($student); $i++) {
	$uid = $student[$i];
	
	//$sql = "SELECT ConductString FROM DISCIPLINE_MS_STUDENT_CONDUCT_SEMI WHERE AcademicYearID=2 AND YearTermID=3 AND StudentID=$uid";	
	$sql = "SELECT ConductString FROM DISCIPLINE_MS_STUDENT_CONDUCT_SEMI WHERE StudentID=$uid";	
	$conductString = $ldiscipline->returnVector($sql);
	
	for($j=0; $j<sizeof($conductString); $j++) {
		$gradeInfo = explode(',', $conductString[$j]);
		for($k=0; $k<sizeof($gradeInfo); $k++) {
			list($tempCatid, $tempGrade) = explode(':',$gradeInfo[$k]);
			if($tempCatid==1)
				$discipline[] = $tempGrade;
			switch($tempCatid) {
				case 1: $teacherCount1++; break;	
				case 2: $teacherCount2++; break;
				case 3: $teacherCount3++; break;
				case 4: $teacherCount4++; break;
				default : break;
			}
		}
	}
	
	$tempLevelID = $ldiscipline->getClassLevelIDByUserID($uid);
	$form = $tempLevelID[0];
	$stdInfo = $ldiscipline->getStudentNameByID($uid, $year);
	$class = $ldiscipline->getClassIDByClassName($stdInfo[0][2]);
	//echo $form.'/'.$class.'/'.$uid.'/'.$teacherCount1.'/'.$teacherCount2.'/'.$teacherCount3.'/'.$teacherCount4.'<br>';

	//$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '2', '3', $form, $class, $uid, '1', $teacherCount1, $discipline);
	//$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '2', '3', $form, $class, $uid, '2', $teacherCount2);
	//$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '2', '3', $form, $class, $uid, '3', $teacherCount3);
	//$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '2', '3', $form, $class, $uid, '4', $teacherCount4);
	$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '3', '5', $form, $class, $uid, '1', $teacherCount1, $discipline);
	$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '3', '5', $form, $class, $uid, '2', $teacherCount2);
	$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '3', '5', $form, $class, $uid, '3', $teacherCount3);
	$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", '3', '5', $form, $class, $uid, '4', $teacherCount4);
	
}

intranet_closedb();
?>
