<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if(!is_array($RecordID) || sizeof($RecordID)==0) {
	header("Location: absentList.php");	
	exit();
}

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$namefield = getNameFieldWithLoginByLang("USR.");
$sql = "SELECT y.YearName, ABL.RecordType, $namefield, ABL.YearID, ABL.UserID FROM DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST ABL LEFT OUTER JOIN INTRANET_USER USR ON (ABL.UserID=USR.UserID) LEFT OUTER JOIN YEAR y ON (y.YearID=ABL.YearID) WHERE ABL.RecordID=".$RecordID[0];
$result = $ldiscipline->returnArray($sql);

list($yearName, $rType, $teacherName, $YearID, $teacherID) = $result[0];

# Form menu
$allForm = $ldiscipline->getAllForm();
$formSelect = getSelectByArray($allForm, ' name="form" id="form"', $YearID, 0, 1);


$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['AbsentList'], "absentList.php?RecordType=$RecordType&form=$form");
$PAGE_NAVIGATION[] = array($button_edit, "");


$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['AbsentInConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absentList.php", 1);


# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


?>
<script language="javascript">
<!--
function checkForm(obj) {
	document.form1.submit();
}
//-->
</script>
<form name="form1" method="POST" action="absent_edit_update.php">
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="60%"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		<td width="40%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
	</tr>
</table>
<br>
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="formfieldtitle"><?=$Lang['eDiscipline']['AbsentList']?></td>
					<td width="80%" class="tablerow1"><?=$teacherName?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Target?></td>
					<td width="80%" class="tablerow1"><?=$formSelect?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$Lang['eDiscipline']['MarkingType']?></td>
					<td width="80%" class="tablerow1">
						<input type="radio" name="markingType" id="markingType1" value="1" <? if($rType==1) echo " checked"; ?>><label for="markingType1"><?=$Lang['eDiscipline']['Marking_NA']?></label>&nbsp;
						<input type="radio" name="markingType" id="markingType2" value="2" <? if($rType==2) echo " checked"; ?>><label for="markingType2"><?=$Lang['eDiscipline']['Marking_OriginalGrade']?></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%">&nbsp;</td>
					<td width="80%" class="tablerow1">
						<?= $linterface->GET_ACTION_BTN($button_submit, "button", "return checkForm(document.form1);")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='absentList.php?RecordType=$RecordType&form=$form'")?>
						</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="hidden" name="teacherID" id="teacherID" value="<?=$teacherID?>">
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID[0]?>">
<input type="hidden" name="original_form" id="original_form" value="<?=$YearID?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
