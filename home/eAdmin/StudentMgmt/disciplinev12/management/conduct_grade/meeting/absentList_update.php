<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$li = new libfilesystem();

intranet_auth();

if(isset($teacherID) && sizeof($teacherID)>0)
	$teacherList = implode(',', $teacherID);
else 
	$teacherList = "";

$li->file_write($teacherList, $intranet_root."/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absent.txt");

header("Location: absentList.php?xmsg=update");
?>