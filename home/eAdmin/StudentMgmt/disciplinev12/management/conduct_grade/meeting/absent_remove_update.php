<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!is_array($RecordID) || sizeof($RecordID)==0) {
	header("Location: absentList.php");	
	exit();
}

$sql = "DELETE FROM DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST WHERE RecordID IN (".implode(',',$RecordID).")";
$result = $ldiscipline->db_db_query($sql);
intranet_closedb();

$msgFlag = ($result) ? "delete" : "delete_failed";

header("Location: absentList.php?RecordType=$RecordType&form=$form&xmsg=$msgFlag");
?>
