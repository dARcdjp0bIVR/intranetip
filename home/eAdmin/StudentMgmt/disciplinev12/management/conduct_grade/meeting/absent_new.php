<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$teacher_selected = $linterface->GET_SELECTION_BOX($array, "name='teacherID[]' ID='teacherID[]' class='select_studentlist' size='6' multiple='multiple'", "");

$allForm = $ldiscipline->getAllForm();
$formSelect = getSelectByArray($allForm, ' name="form" id="form"');

$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['AbsentInConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/absentList.php", 1);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['AbsentList'], "absentList.php?RecordType=$RecordType&form=$form");
$PAGE_NAVIGATION[] = array($button_new, "");

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


?>
<script language="javascript">
<!--
function checkForm(obj) {
	var teacher_length = obj.elements['teacherID[]'].length;
	
	if(teacher_length==0) {
		alert("<?=$i_alert_pleaseselect." ".$i_Teaching_TeacherName?>");
		return false;	
	}
	if(document.form1.form.value=='') {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Target?>");
		return false;	
	}
	checkOptionAll(obj.elements['teacherID[]']);
	document.form1.submit();
}
//-->
</script>
<form name="form1" method="POST" action="absent_new_update.php">
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="60%"><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		<td width="40%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
		</td>
	</tr>
</table>
<br>
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Teaching_TeacherName?></td>
					<td width="80%" class="tablerow1">
						<?=$teacher_selected?><br />
						<?=$linterface->GET_BTN($button_select, "button","newWindow('../../choose_pic.php?fieldname=teacherID[]',11)");?>
						<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['teacherID[]'])");?>

					</td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Target?></td>
					<td width="80%" class="tablerow1"><?=$formSelect?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$Lang['eDiscipline']['MarkingType']?></td>
					<td width="80%" class="tablerow1">
						<input type="radio" name="markingType" id="markingType1" value="1" checked><label for="markingType1"><?=$Lang['eDiscipline']['Marking_NA']?></label>&nbsp;
						<input type="radio" name="markingType" id="markingType2" value="2"><label for="markingType2"><?=$Lang['eDiscipline']['Marking_OriginalGrade']?></label>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td width="20%">&nbsp;</td>
					<td width="80%" class="tablerow1">
						<?= $linterface->GET_ACTION_BTN($button_submit, "button", "return checkForm(document.form1);")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='absentList.php?RecordType=$RecordType&form=$form'")?>
						</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<input type="hidden" name="RecordType" id="RecordType" value="<?=$RecordType?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
