<?php

#############################################
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$ldiscipline = new libdisciplinev12();

$stdInfo = $ldiscipline->getStudentNameByID($uid);

list($tempID, $tempName, $tempClsName, $tempClsNo) = $stdInfo[0];
$stdName = "$tempName ($tempClsName-$tempClsNo)";

//$APCategory = $ldiscipline->getAPCategoryInfo(-1);
$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagID";
$TagAry = $ldiscipline->returnArray($sql, 2);

$sql = "SELECT conductString, UserID FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE AcademicYearID='$year' AND YearTermID='$semester' AND StudentID=$uid";
$data = $ldiscipline->returnArray($sql, 2);

for($i=0; $i<sizeof($data); $i++) {
	$teacherID = $data[$i][1];
	$tempGrade = explode(",",$data[$i][0]);
	for($j=0; $j<sizeof($tempGrade); $j++) {
		$g = $tempGrade[$j];
		$line = explode(":",$g);
		list ($catid, $gradeNew) = $line;
		if($catid!="" && $uid!="")	
			$conductGrade[$teacherID][$catid] = $gradeNew;
	}
}


$MODULE_OBJ['title'] = $iDiscipline['OriginalResult'];

# Start layout
$linterface->LAYOUT_START();

?>
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td colspan="3">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="40%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="60%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td class="tablerow1"><?=$ldiscipline->getTermNameByTermID($semester)?></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_Student?></td>
					<td class="tablerow1"><?=$stdName?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td class='tabletext' colspan="4"></td>
					<td class='tabletext' colspan="4"></td>
				</tr>
				<tr class="tablebluetop">
					<td class='tabletoplink'>&nbsp;</td>
		<?
		for($i=0; $i<sizeof($TagAry); $i++) {
			echo "<td class='tablebluetop tabletoplink'>".intranet_htmlspecialchars($TagAry[$i]['TagName'])."</td>";	
		}
		?>
				</tr>
	<?
	$subjectGroupTeacherIDs = $ldiscipline->getSubjectGroupTeacherList($year, $semester, $uid);
	$stdInfo = $ldiscipline->getStudentNameByID($uid);
	$yearID = Get_Current_Academic_Year_ID();
	$name_field = getNameFieldByLang('USR.');
	
	$conds = (sizeof($subjectGroupTeacherIDs)>0) ? " OR (STCT.UserID IN (".implode(',',$subjectGroupTeacherIDs)."))" : "";
	$sql = "SELECT 
				USR.UserID, $name_field as name 
			FROM 
				INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER CLSTCH ON (USR.UserID=CLSTCH.UserID) LEFT OUTER JOIN 
				YEAR_CLASS CLS ON (CLS.YearClassID=CLSTCH.YearClassID AND CLS.AcademicYearID=$yearID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER STCT ON (STCT.UserID=USR.UserID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_USER STCU ON (STCT.SubjectGroupID=STCU.SubjectGroupID)
				
			WHERE 
				((CLS.ClassTitleEN='".$stdInfo[0]['ClassName']."' OR CLS.ClassTitleB5='".$stdInfo[0]['ClassName']."') $conds) AND 
				USR.RecordType=1 
				
			GROUP BY USR.UserID
			ORDER BY name
			";
	$data = $ldiscipline->returnArray($sql, 2);
	
	$absentAry = $ldiscipline->getAbsentStatusByStudentID($year, $uid);

	for($i=0; $i<sizeof($data); $i++) {
		$css = ($i%2==0) ? 1 : 2;
		$bgClass = (in_array($data[$i][0], $absentAry)) ? "bgcolor='#DCDCDC'" : " class='tablebluerow$css'";
		echo "<tr $bgClass><td class='tabletext'>".$data[$i][1]."</td>";
		for($j=0; $j<sizeof($TagAry); $j++) {
			echo "<td class='tabletext'>".$conductGrade[$data[$i][0]][$TagAry[$j]['TagID']]."</td>";	
		}
		echo "</tr>";	
	}
	?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();")?></td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
