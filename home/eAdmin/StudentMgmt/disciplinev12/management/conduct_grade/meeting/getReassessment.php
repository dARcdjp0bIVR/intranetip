<?php

#############################################
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# remark table
$remarkTable = $ldiscipline->getConductGradeRemarkTable();

$semiGradeString = $ldiscipline->getConductString("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", $year, $semester, $studentid);

$finalGrade = $ldiscipline->getFinalGradeString($year, $semester, $uid);

$lockStatus = $ldiscipline->getLockStatusOfFinalGrade($year, $semester, $uid);
if($lockStatus==0) {	# unlocked
	$buttonSet = $linterface->GET_ACTION_BTN($button_save, "submit", "")."&nbsp;".
					$linterface->GET_ACTION_BTN($button_reset, "reset", "")."&nbsp;".
					$linterface->GET_ACTION_BTN($button_lock, "button", "changeLockStatus(1)")."&nbsp;".
					$linterface->GET_ACTION_BTN($button_back, "button", "goBack()");
} else {				# locked
	$buttonSet = $linterface->GET_ACTION_BTN($button_unlock, "button", "changeLockStatus(0)")."&nbsp;".
					$linterface->GET_ACTION_BTN($button_back, "button", "goBack()");
}

# get Form ID ($lvlID)
$form = str_replace("__", " ", $form);
$form = str_replace("_", ".", $form);
$lvlID = $ldiscipline->getFormIDByFormName($form);

# absent teacher list 
$absentTeacherIDAry = array();
$absentTeacherTypeAry = array();
$sql = "SELECT UserID, RecordType FROM DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST WHERE YearID=$lvlID";
$result = $ldiscipline->returnArray($sql, 2);
//debug_pr($result);
for($i=0; $i<sizeof($result); $i++) {
	list($id, $type) = $result[$i];
	$absentTeacherIDAry[$i] = $id;
	$absentTeacherTypeAry[$id] = $type;
}

# get grade
$gradeInfo = explode("\n",get_file_content("$intranet_root/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/gradeFile.txt"));
for($i=0; $i<sizeof($gradeInfo); $i++) {
	list($tempGrade, $comment, $mark) = explode(":", $gradeInfo[$i]);	
	$grade[] = $tempGrade;				
}

# get category name & $category ID
//$catname = $ldiscipline->getAwardPunishCategoryName($catid);

$sql = "SELECT TagName FROM DISCIPLINE_ITEM_TAG_SETTING WHERE TagID=$tagid";
$result = $ldiscipline->returnVector($sql, 1);
$tagname = $result[0];

# check the best possible conduct grade
$start = "A";

if($tagid == 2) {		# Diligence
	$conds = ($form!=5 && $form!=7) ? " AND Semester='$semester'" : "";		# form name = '5' or '7'
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_RECORD WHERE AcademicYearID='$year' $conds AND CategoryID=2 AND StudentID=$uid AND RecordStatus=1";
	$result = $ldiscipline->returnVector($sql);
	
	if($result[0]>=8) $start = "B";

} else if($tagid == 1) {

	$sql = "SELECT MR.ProfileMeritType, SUM(MR.ProfileMeritCount) as ProfileMeritCount FROM DISCIPLINE_MERIT_RECORD MR
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM MI ON (MI.ItemID=MR.ItemID)
			WHERE MR.AcademicYearID='$year' AND MR.YearTermID='$semester' and MR.StudentID=$uid AND MR.MeritType=-1 AND MR.RecordStatus=1 group by MR.MeritType, MR.ProfileMeritType";	
	$result = $ldiscipline->returnArray($sql, 2);

	for($k=0; $k<sizeof($result); $k++) {
		$tmp = $result[$k];
		$amt = ($form==5 || $form==7) ? $tmp['ProfileMeritCount']/1.5 : $tmp['ProfileMeritCount'];	# form name = '5' or '7'
		switch($tmp['ProfileMeritType']) {
			case 0 :        break;
			case -5:		IF($amt>0) $start = "D"; break;
			case -4:		IF($amt>0) $start = "D"; break;
			case -3:		IF($amt>0) $start = "D"; break;
			case -2:		IF($amt>=1) $start = "D"; break;
			case -1:		IF($amt>=5) $start = "D";
			                ELSE IF($amt>=3) $start = "C-";
			                ELSE IF($amt>=2) $start = "C+";
			default :		break;
		}
	}
}

$stdInfo = $ldiscipline->getStudentNameByID($uid);

//debug_pr($stdInfo);
$subjectGroupTeacherIDs = $ldiscipline->getSubjectGroupTeacherList($year, $semester, $uid);


$yearID = Get_Current_Academic_Year_ID();
$name_field = getNameFieldByLang('USR.');
$conds = (sizeof($subjectGroupTeacherIDs)>0) ? " OR (STCT.UserID IN ('".implode('\',\'',$subjectGroupTeacherIDs)."'))" : "";
$classID = $ldiscipline->getClassIDByClassName($stdInfo[0]['ClassName']);

	$sql = "SELECT 
				USR.UserID, $name_field as name 
			FROM 
				INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER CLSTCH ON (USR.UserID=CLSTCH.UserID) LEFT OUTER JOIN 
				YEAR_CLASS CLS ON (CLS.YearClassID=CLSTCH.YearClassID AND CLS.AcademicYearID=$yearID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER STCT ON (STCT.UserID=USR.UserID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_USER STCU ON (STCT.SubjectGroupID=STCU.SubjectGroupID)
				
			WHERE 
				(CLS.YearClassID=$classID $conds)  AND
				USR.RecordType=1
				
			GROUP BY USR.UserID
			ORDER BY name
			";
$data = $ldiscipline->returnArray($sql, 2);

$totalTeacher = sizeof($data);

$semiConductMarkedTeacherAry = $ldiscipline->getsemiConductMarkedTeacher($year, $semester, $uid);

# teacher absent list
//$absentAry = $ldiscipline->getAbsentStatusByStudentID($year, $uid);

$table = "<table width='100%' cellpadding='4' cellspacing='0' border='0'>";
$table .= "<tr class='tablebluetop'><td class='tabletopnolink' width='40%'>$tagname</td><td class='tabletopnolink'>&nbsp;</td>";
foreach($grade as $val) {
	$table .= "<td class='tabletopnolink' width='10%' align='center'>$val</td>";	
}
	$table .= "<td class='tabletopnolink'>$i_general_na</td>";
	$colspan = 10;
$table .= "</tr>";

$k = 0;
$hiddenField = "";

for($i=0; $i<sizeof($data); $i++) {
	$teacherID[$i] = $data[$i][0];
	$tagGrade = $ldiscipline->getIndividualconductGradeByTag($uid, $year, $semester, $teacherID[$i], $tagid);
	$displayGrade = ($tagGrade=="") ? "-" : $tagGrade;
	
	if(!in_array($teacherID[$i], $absentTeacherIDAry)) {
		$css = ($k%2==0) ? 1 : 2;
		$table .= "<tr class='tablebluerow$css'><td class='tabletext'>".$data[$i][1]."</td><td class='tabletext' nowrap>[$displayGrade]</td>";
		$show = 0;
		foreach($grade as $val) {		# each grade
			if($val==$start) $show = 1;
			$table .= "<td class='tabletopnolink' align='center'>";
			if($show==1) {
				$table .= "<input type='radio' name='radio_".$data[$i][0]."' value='$val'";
				$table .= ($semiConductMarkedTeacherAry[$teacherID[$i]][$tagid]==$val) ? " checked" : "";
				$table .= ">";
			} else {
				$table .= "&nbsp;";	
			}
			$table .= "</td>";
		}
		//if($tagid==1) {
			$table .= "<td><input type='radio' name='radio_".$data[$i][0]."' value='NA'";
			$table .= ($semiConductMarkedTeacherAry[$teacherID[$i]][$tagid]=='NA') ? " checked" : "";
			$table .= "></td>";
		//}
		$table .= "</tr>";	
		$k++;
	} else {
		$value = ($absentTeacherTypeAry[$teacherID[$i]]==1) ? "NA" : $tagGrade;
		$hiddenField .= "<input type='hidden' name='radio_".$data[$i][0]."' id='radio_".$data[$i][0]."' value='$value'>";
		$table .= "<tr bgcolor='#DCDCDC'><td>".$data[$i][1]."</td><td class='tabletext' nowrap>[$displayGrade]</td><td colspan='8'>[".(($value=="") ? $displayGrade : $value)."]</td></tr>";
	}
}
$table .= "	
			<tr class='tablebluetop'>
				<td colspan='$colspan' class='tabletopnolink'>$finalGrade</td>
			</tr>
			<tr>
				<td colspan='$colspan'>
					<table width='100%' cellpadding='1' cellspacing='0' border='0'>
						<tr>
							<td>$buttonSet
							</td>
							<td align='right'>
							".$linterface->GET_BTN($button_view." ".$iDiscipline['OriginalResult'], "button", "showOriginal()")."
							</td>
						</tr>
					</table>					
				</td>
			</tr>
			<tr>
				<td colspan='$colspan'>
					".$ldiscipline->showWarningMsg($i_UserRemark, $remarkTable)."
				</td>
			</tr>
			";
$table .= "</table>";
$table .= "<input type='hidden' name='teacherID' id='teacherID' value='".implode(',', $teacherID)."'>";
$table .= "<input type='hidden' name='totalTeacher' id='totalTeacher' value='$totalTeacher'>";
$table .= $hiddenField;

echo $table;

intranet_closedb();

?>