<?
# using: yat

/********************** Change Log ***********************/

/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(sizeof($RecordID)>0)
{
	$RecordID = (is_array($RecordID)) ? implode(",",$RecordID) : $RecordID;
}else{
	$RecordID = $RecordID;
}

// $parameter = "?SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&RecordType=$RecordType&s=$s&approved=$approved&rejected=$rejected&waived=$waived&pic=$pic";

// $back_path = "index.php".$parameter;

# Check access right
// if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditAll") || (($ldiscipline->isConductPIC($RecordID) || $ldiscipline->isConductOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-EditOwn"))))
// {
// 		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT("","index.php?approved=$approved&rejected=$rejected&waived=$waived&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&RecordType=$RecordType&s=$s&pageNo=$pageNo&order=$order&field=$field&page_size=$numPerPage&RecordType=$RecordType&s=$s");
// }

# menu highlight setting
$CurrentPage = "Management_FollowUp";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($Lang['eDiscipline']['FollowUp']);

# navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "javascript:history.back();");
$PAGE_NAVIGATION[] = array($eDiscipline["EditRecord"], "");
				
$student_namefield = getNamefieldByLang("b.");
$followup_namefield = getNamefieldByLang("e.");

$sql = "SELECT 
				CONCAT(b.ClassName,' - ',b.ClassNumber) as ClassNameNum, 
				$student_namefield,
				a.RecordType,
				c.CategoryID,
				c.Name,
				a.ItemID,
				LEFT(a.RecordDate,10) as RecordDate,
				a.Remark,
				a.PICID,
				b.Remark as InternalRemark,
				a.ActionID, 
				a.ActionTimes,
				a.ActionCompleted,
				a.LastFollowUpDate,
				$followup_namefield
		FROM
				DISCIPLINE_ACCU_RECORD as a
				LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON a.CategoryID = c.CategoryID
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
				LEFT OUTER JOIN INTRANET_USER as e ON (e.UserID = a.LastFollowUpBy)
		WHERE 
				a.RecordID = $RecordID";
				

$arr_result = $ldiscipline->returnArray($sql);

if(sizeof($arr_result)>0){
	list($className,$studentName,$recordType,$meritCategoryID,$meritCategoryName,$meritItemID,$recordDate,$remark, $pic, $InternalRemark, $ActionID, $ActoinTimes, $ActionCompleted, $LastFollowUpDate, $followby) = $arr_result[0];
	$remark = $remark ? nl2br($remark) : "---";
}

$school_year = $ldiscipline->getAcademicYearNameByYearID($AcademicYearID);


$fcm = new academic_year_term($YearTermID);
$semesterName = $fcm->Get_YearNameByID($YearTermID, $intranet_session_language);


if ($pic != '') {
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($pic) AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
}
$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");


# check user can access/edit this item or not
$AccessItem = 0;
if($ldiscipline->IS_ADMIN_USER($UserID))
{
	$AccessItem = 1;
}
else
{
	$ava_item = $ldiscipline->Retrieve_User_Can_Access_GM_Category_Item($UserID, 'item');
	if($ava_item[$meritCategoryID])
		$AccessItem = in_array($meritItemID, $ava_item[$meritCategoryID]);
}

$merit_display = $meritCategoryID==1 ? $meritCategoryName : $ldiscipline->RETURN_CONDUCT_REASON($meritItemID);

$linterface->LAYOUT_START($xmsg);

$followby_str = $followby ?$followby ." (" . $LastFollowUpDate .")" : $Lang['General']['EmptySymbol'];

?>

<script language="javascript">
<!--
function check_form()
{
	var obj = document.form1;
		
	
	if(obj.ActionID.value != <?=$ActionID?> && obj.ActionID.value !="")
	{
		if(confirm("<?=$Lang['eDiscipline']['ChangeFollowUpActionWarning']?>"))
		{
			obj.resetFollowup.value=1;
			obj.submit();
		}
	}
	else
	{
		obj.submit();	
	}
}

function add_followup()
{
	var obj = document.form1;
	
	obj.addFollowUp.value = 1;
	obj.submit();
}
//-->
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>

<form name="form1" method="POST" action="edit_update.php">

<table class="form_table_v30">
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$i_general_name;?></td>
	<td class="tabletext" valign="top"><?=$studentName;?></td>
</tr>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$i_general_class;?></td>
	<td class="tabletext" valign="top"><?=$className;?></td>
</tr>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$eDiscipline["Type"];?></td>
	<td class="tabletext" valign="top">
		<? 
			if($recordType==1){
				echo "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> $i_Discipline_GoodConduct";
			}else{
				echo "<img src=\"{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> $i_Discipline_Misconduct";
			}
		?>
	</td>
</tr>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$iDiscipline['ConductCategoryItem']?></td>
	<td class="tabletext" valign="top"><?=$merit_display?></td>
</tr>
<? if($school_year) {?>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$i_Discipline_System_Conduct_School_Year?></td>
	<td> <?=$school_year?></td>
</tr>
<? } ?>
<? if($semesterName) {?>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$i_Discipline_System_Conduct_Semester?></td>
	<td> <?=$semesterName?></td>
</tr>
<? } ?>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$Lang['eDiscipline']['EventDate']?></td>
	<td> <?=$recordDate?></td>
</tr>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$i_Discipline_System_Contact_PIC?></td>
	<td><?=$ldiscipline->getPICNameList($pic);?></td>
</tr>

<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$i_Discipline_Remark?></td>
	<td><?=$remark?></td>
</tr>

<tr>
	<td valign='top' nowrap='nowrap' class='field_title'><?=$Lang['eDiscipline']['FollowUp']?></td>
	<td><?=$ldiscipline->getFollowUpSelection(1,$ActionID,"ActionID")?></td>
</tr>

<tr>
	<td valign='top' nowrap='nowrap' class='field_title'><?=$Lang['eDiscipline']['FollowUpTimes']?></td>
	<td><?=($ActoinTimes ? $ActoinTimes : 0)?></td>
</tr>

<tr>
	<td valign='top' nowrap='nowrap' class='field_title'><?=$Lang['eDiscipline']['LastFollowUpBy']?></td>
	<td><?=$followby_str?></td>
</tr> 

<? if(trim($InternalRemark)) {?>
<tr>
	<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$Lang['General']['StudentInternalRemark']?></td>
	<td><?=nl2br($InternalRemark)?></td>
</tr>
<? } ?>

<tr>
	<td valign='top' nowrap='nowrap' class='field_title'>Follow-up status</td>
	<td>
		<input type="radio" name="ActionCompleted" id="ActionCompleted1" value=1 <?=($ActionCompleted ? "checked":"") ?>> <label for="ActionCompleted1"><?=$Lang['eDiscipline']['Completed']?></label>
		<input type="radio" name="ActionCompleted" id="ActionCompleted0" value=0 <?=($ActionCompleted ? "":"checked") ?>> <label for="ActionCompleted0"><?=$Lang['eDiscipline']['InProgress']?></label>
	</td>
</tr> 

</table>

<div class="edit_bottom_v30">
<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['AddFollowUp1'], "button", "add_followup();")?>
	<?= $linterface->GET_ACTION_BTN($button_save, "button", "check_form();")?>
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
<p class="spacer"></p>
</div>
 
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>">
<input type="hidden" name="resetFollowup" id="resetFollowup" value="0">
<input type="hidden" name="addFollowUp" id="addFollowUp" value="0">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>