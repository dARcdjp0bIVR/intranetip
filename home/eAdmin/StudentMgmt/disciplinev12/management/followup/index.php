<?

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if (!isset($field) || $ck_page_field == "")
	$field = 3;

$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");
$arrCookies[] = array("ck_status_filter", "AcionCompleted");
$arrCookies[] = array("ck_times_filter", "ActionTimes");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

$page_size = $ck_page_size ? $ck_page_size : $page_size;

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$CurrentPage = "Management_FollowUp";
$CurrentPageArr['eDisciplinev12'] = 1;
$TAGS_OBJ[] = array($Lang['eDiscipline']['FollowUp']);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

##### filters
$status_selection = array();
$status_selection[] = array(-1,$i_status_all);
$status_selection[] = array(0,$Lang['eDiscipline']['InProgress']);
$status_selection[] = array(1,$Lang['eDiscipline']['Completed']);
$AcionCompleted = $AcionCompleted ? $AcionCompleted : 0;
$complete_selection = getSelectByArray($status_selection, " name='AcionCompleted' onChange='document.form1.submit();' ", $AcionCompleted, 0, 1);
switch($AcionCompleted)
{
	case 1:
		$complete_sql = " and (a.ActionCompleted = $AcionCompleted)";
		break;
	case 0:
		$complete_sql = " and (a.ActionCompleted = $AcionCompleted or a.ActionCompleted is NULL)";
		break;
	default:
		$complete_sql = "";
		break;
}

$times_selection_ary = array();
$ActionTimes = isset($ActionTimes) ? $ActionTimes : -1;   
$times_selection_ary[] = array(-1,$Lang['eDiscipline']['AllFollowUpTimes']);
for($i=0;$i<=10;$i++)
{
	$times_selection_ary[] = array($i,$i);
}
$times_selection = getSelectByArray($times_selection_ary, " name='ActionTimes' onChange='document.form1.submit();' ", $ActionTimes, 0, 1);
switch($ActionTimes)
{
	case -1:
		$times_sql = "";
		break;
	default:
		$times_sql = " and a.ActionTimes = $ActionTimes ";
		break;
}
##### Button
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'RecordID[]','edit.php')", '', '', '', '', 0);
$addPunishmentBtn = '<a href="javascript:AddPunishment()" class="tool_other">'. $Lang['eDiscipline']['AddPunishmentForSelectedStudent'] .'</a>';
$addFollowUpBtn = '<a href="javascript:addFollowUp()" class="tool_other">'. $Lang['eDiscipline']['AddFollowUp1'] .'</a>';

##### table list
$student_namefield = getNamefieldByLang("b.");
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$sql = "select 
			CONCAT($clsName,' - ',ycu.ClassNumber) as ClassNameNum,
			$student_namefield as student_name,
			IF(d.Name IS NULL, c.Name, CONCAT(c.Name,' - ',d.Name)) as itemName,
			left(a.RecordDate,10) as RecordDate,
			f.Title,
			if(isnull(a.ActionTimes), 0, a.ActionTimes),
			if(a.ActionCompleted=1, '". $Lang['eDiscipline']['Completed'] ."', '". $Lang['eDiscipline']['InProgress'] ."'),
			CONCAT('<input type=checkbox name=RecordID[] value=', a.RecordID ,'><input type=hidden name=StudentID_', a.RecordID ,' value=', b.UserID ,'>')
		from  
			DISCIPLINE_ACCU_RECORD as a
			INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID)
			LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (a.CategoryID = c.CategoryID)
			LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
			LEFT JOIN DISCIPLINE_FOLLOWUP_ACTION as f ON (f.ActionID = a.ActionID)
			LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (b.UserID=ycu.UserID)
			LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID=yc.YearClassID)
		where 
			(a.ActionID is not NULL and a.ActionID !=0) and
			yc.AcademicYearID=". Get_Current_Academic_Year_ID() ." AND 
			b.RecordStatus IN (0,1,2) 
			$complete_sql
			$times_sql
		";
// debug_pr($sql);


##### Table settings
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", 
						"student_name", "itemName", "RecordDate", "Title", "Times");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;

$li->IsColOff = "GeneralDisplayWithNavigation"; 
$li->fieldorder2 = ", a.DateInput desc, ClassNameNum";
$pos = 0;
$li->column_list .= "<th class='num_check'>#</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $i_general_class)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $i_general_name)."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $eDiscipline["Category_Item"])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['eDiscipline']['EventDate'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['eDiscipline']['FollowUp'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['eDiscipline']['Times2'])."</th>\n";
$li->column_list .= "<th>".$li->column($pos++, $Lang['eDiscipline']['FollowUpStatus'])."</th>\n";
$li->column_list .= "<th>".$li->check("RecordID[]")."</th>\n";

?>
<script language="javascript">
<!--
function AddPunishment()
{
	if(countChecked(document.form1,"RecordID[]")==0)
	{
		alert("<?=$Lang['eDiscipline']['select_student']?>");
	}
	else
	{
		var obj = document.form1;
		var student_str = "";
		var deli = "";
		
		len=obj.elements.length;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="RecordID[]" && obj.elements[i].checked)
                {
	                eval("student_str = student_str + deli + obj.StudentID_"+ obj.elements[i].value +".value;");
	                deli = ",";
                }
        }
		window.location='../award_punishment/new2.php?student=' + student_str;
	}
}

function addFollowUp()
{
	if(countChecked(document.form1,"RecordID[]")==0)
	{
		alert("<?=$Lang['eDiscipline']['select_student']?>");
	}
	else
	{
		var obj = document.form1;
		var record_str = "";
		var deli = "";
		
		len=obj.elements.length;
        for( i=0 ; i<len ; i++) {
                if (obj.elements[i].name=="RecordID[]" && obj.elements[i].checked)
                {
	                record_str = record_str + deli + obj.elements[i].value;
	                deli = ",";
                }
        }
		window.location='edit_update.php?addFollowUp=1&record_str=' + record_str;
	}
}

function print_list()
{
	newWindow('print.php',10);
}
//-->
</script>

<div class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:print_list();" class="print"> <?=$Lang['eDiscipline']['TodayFollowUpList']?></a>
	</div>
<br style="clear:both" />
</div>
    
<form name="form1" method="post" action="index.php"> 
<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr class="table-action-bar">
	<td valign="bottom">
	<div class="table_filter"><?=$complete_selection?><?=$times_selection?></div>
	
	<td valign="bottom">
	<div class="common_table_tool">
		<?=$editBtn?><?=$addPunishmentBtn?><?=$addFollowUpBtn?>
	</div>
	</td>
</tr>
</table>

<?=$li->display('',1);?>


</div>


<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>"/>
<input type="hidden" name="order" value="<?php echo $li->order; ?>"/>
<input type="hidden" name="field" value="<?php echo $li->field; ?>"/>
<input type="hidden" name="page_size_change" value=""/>
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>"/>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>