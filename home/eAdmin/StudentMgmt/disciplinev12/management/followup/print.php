<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$ldiscipline = new libdisciplinev12();

//Print button
$PrintBtn = "
	<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'>".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td>
	</tr>
</table>";

$breakStyle = "style='page-break-after:always'";

$student_namefield = getNamefieldByLang("b.");
$sql = "select 
			b.ClassName,
			b.ClassNumber,
			$student_namefield as student_name,
			c.Title,
			a.RecordDate,
			d.Name
		from 
			DISCIPLINE_ACCU_RECORD as a
			left join INTRANET_USER as b on (b.UserID=a.StudentID) 
			left join DISCIPLINE_FOLLOWUP_ACTION as c on (c.ActionID = a.ActionID)
			LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as d ON (a.ItemID = d.ItemID)
		where 
			a.ActionID>0 and 
			a.ActionCompleted!=1
		order by
			b.ClassName, b.ClassNumber
		";
$result = $ldiscipline->returnArray($sql);

### Build follow-up list record - class name as index
$record_ary = array();
foreach($result as $k=>$d)
{
	$thisClassName = array_shift($d);	# for index = 0
	$thisClassName = array_shift($d);	# for index = ClassName
	$record_ary[$thisClassName][] = $d;
}	

$x = "";
$i = 0;
foreach($record_ary as $thisClassName=>$data)
{
	$i++;
	
	$x .= '<span class="eSportprinttitle"><strong>'. $Lang['eDiscipline']['TodayFollowUpList'] .'</strong></span>';
	$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="4" class="eSporttableborder">';
	$x .= '<tr>';
	$x .= '<td colspan="2" class="eSporttdborder eSportprinttabletitle"><strong>'. $thisClassName.'</strong></td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle">'. $Lang['eDiscipline']['FollowUp'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle">'. $Lang['eDiscipline']['FollowUpRecord'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle">'. $Lang['StudentAttendance']['Absent'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle">'. $Lang['eDiscipline']['FollowUpLetter'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle">'. $Lang['eDiscipline']['DisciplineTeam'].'</td>';
	$x .= '<td class="eSporttdborder eSportprinttabletitle">'. $Lang['eDiscipline']['FollowUpChecked'].'</td>';
	$x .= '</tr>';
	
	foreach($data as $k=>$d)
	{
		list($thisClassNumber, $thisStudent, $thisFollowup, $thisRecordDate, $thisItem) = $d;
		$thisDate = date("d/m", TimeStrToTimeStamp($thisRecordDate));
		
		$x .= '<tr>';
		$x .= '<td class="eSporttdborder eSportprinttext" width=30>'. $thisClassNumber.'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext" width=100>'. $thisStudent.'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext" width=100>'. $thisFollowup.'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext">'. $thisDate . " - " . $thisItem.'</td>';
		$x .= '<td class="eSporttdborder eSportprinttext" width=30>&nbsp;</td>';
		$x .= '<td class="eSporttdborder eSportprinttext" width=30>&nbsp;</td>';
		$x .= '<td class="eSporttdborder eSportprinttext" width=30>&nbsp;</td>';
		$x .= '<td class="eSporttdborder eSportprinttext" width=30>&nbsp;</td>';
		$x .= '</tr>';
	}
	$x .= '</tr>';
	$x .= '</table><br>';
	
	##### Check recent GM records
    $recent_day = 5;
    $sql = "select 
    			b.ClassNumber,
    			$student_namefield as student_name,
    			left(a.RecordDate,10),
    			c.Name,
    			a.ProfileMeritCount, 
    			a.ProfileMeritType,
    			b.NickName
    		 from 
    		 	DISCIPLINE_MERIT_RECORD as a
				left join INTRANET_USER as b on (b.UserID=a.StudentID) 
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as c ON (a.ItemID = c.ItemID)
    		 where 
    		 	a.DateInput>=CURDATE()-$recent_day and
    		 	b.ClassName='$thisClassName'
    		 order by 
    		 	a.RecordDate
    		 ";
	$result = $ldiscipline->returnArray($sql);
	
	if(sizeof($result)) 
	{
		$x .= '<span class="eSportprinttitle"><strong>'. $Lang['eDiscipline']['NewPunishmentRecord'] .'</strong></span>';
		$x .= '<table width="100%" border="0" cellspacing="0" cellpadding="4" class="eSporttableborder">';
		$x .= '<tr>';
		$x .= '<td colspan="7" class="eSporttdborder eSportprinttabletitle"><strong>'. $thisClassName.'</strong></td>';
		$x .= '</tr>';
		
		foreach($result as $k=>$d)
		{
			list($thisClassNumber, $thisStudent, $thisRecordDate, $thisItem, $thisMeritCount, $thisProfileMeritType, $thisNickName ) = $d;
			
			$x .= '<tr>';
			$x .= '<td class="eSporttdborder eSportprinttext" width=30>'. $thisClassNumber.'</td>';
			$x .= '<td class="eSporttdborder eSportprinttext" width=100>'. $thisStudent.'</td>';
			$x .= '<td class="eSporttdborder eSportprinttext" width=100>'. $thisRecordDate.'</td>';
			$x .= '<td class="eSporttdborder eSportprinttext">'. $thisItem.'</td>';
			$x .= '<td class="eSporttdborder eSportprinttext" width=20>'. $ldiscipline->TrimDeMeritNumber_00($thisMeritCount) .'</td>';
			$x .= '<td class="eSporttdborder eSportprinttext" width=50>'. $ldiscipline->RETURN_MERIT_NAME($thisProfileMeritType) .'</td>';
			$x .= '<td class="eSporttdborder eSportprinttext" width=100>'. $thisNickName .'&nbsp;</td>';
			$x .= '</tr>';
			
		}
		$x .= '</tr>';
		$x .= '</table><br>';
	}
	
	if($i < sizeof($record_ary))
		$x .= '<p '. $breakStyle.'>&nbsp;</p>';
	
}

intranet_closedb();
?>

<?=$PrintBtn?>
<?=$x?>

