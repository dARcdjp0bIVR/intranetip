<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

// debug_pr($RecordID);
// debug_pr($ActionID);
// debug_pr($resetFollowup);

$li = new libdb();

if($addFollowUp)
{
	if($record_str)	# from index page > "Add Follow-up by 1 "
	{
		$sql = "update DISCIPLINE_ACCU_RECORD set LastFollowUpdate = now(), LastFollowUpBy = $UserID, ActionTimes=ActionTimes+1 where RecordID in (". $record_str .")";
	}
	else
	{
		$sql = "update DISCIPLINE_ACCU_RECORD set LastFollowUpdate = now(), LastFollowUpBy = $UserID, ActionTimes=ActionTimes+1 where RecordID=$RecordID";
	}
}
else
{
	$ActionID = $ActionID ? $ActionID : 0;
	
	if($resetFollowup && $ActionID)
	{
		$update_times = ", ActionTimes = 0 ";	
	}
	
	$sql = "update DISCIPLINE_ACCU_RECORD set ActionID=$ActionID, LastFollowUpdate = now(), LastFollowUpBy = $UserID, ActionCompleted = $ActionCompleted ". $update_times." where RecordID=$RecordID";
} 
$li->db_db_query($sql);

if($ActionID==0)
	$back_page = "index.php";
else
	$back_page = "edit.php?RecordID=$RecordID&xmsg=". $Lang['General']['ReturnMessage']['UpdateSuccess'];
	
Header("Location: $back_page");

intranet_closedb();
?>