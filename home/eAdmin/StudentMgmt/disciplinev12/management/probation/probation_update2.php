<?php
// Using: Bill

############ Change Log Start #############################
#
#	Date:	2017-10-11	(Bill)
#			skip Approval Checking when approve merit records (as Probation Handling)
#
# 	Date:	2016-06-07 (Bill)	[2016-0329-1757-08164]
#			copy from /award_punishment/approve_update2.php
#			update probation records						
#
############################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
	
# Check access right
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || !$sys_custom['eDiscipline']['CSCProbation'])
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&pic=$pic&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&released=$released&waived=$waived&fromPPC=$fromPPC";

$RecordIDAry = array();
$RecordIDAry = $RecordID;

if($back_page)
	$returnPath = $back_page."?id=$RecordIDAry[0]";
else
	$returnPath = "index.php?$return_filter";

foreach($RecordIDAry as $MeritRecordID)
{
	# Double check user can approve the record or not 
	$can_approve = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($MeritRecordID);
	
	# Set APPROVE
	if($can_approve)
	{
		// Downgrade Merit Type if Confirm Probation
		if($actionType==1)
		{
			$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);
			$data = $datainfo[0];
			
			$dataAry = array();
			$dataAry['ProfileMeritType'] = intval($data['ProfileMeritType']) + 1;
			$ldiscipline->UPDATE_MERIT_RECORD($dataAry, $MeritRecordID);
		}
		$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 0, 1, 1);
	}
}

$msg = "update";
header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>