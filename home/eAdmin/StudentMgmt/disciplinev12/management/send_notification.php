<?php
// Using : 

############ Change Log [Start]
#
#   Date:   2020-11-09 (Bill)   [2020-1009-1753-46096]
#           support scheduled mode  ($sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay'])
#
#	Date:	2016-10-24 [Bill]	[DM#3077]
#			- Update notification message
#
#	Date:	2016-08-17 [Bill]	[2016-0310-0911-58085]
#			Create File
#			- copy logic from notify_parent_via_eClassApp.php
#
############ Change Log [End]

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_auth();
intranet_opendb();

// Get target notice
$NoticeID = $_POST["NoticeIDs"];
$NoticeID = explode(",", $NoticeID);
$NoticeID = array_filter((array)$NoticeID);

// Return error message if no NoticeID available
if (count((array)$NoticeID) == 0)
{
	intranet_closedb();
	
	$returnMsg = "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
	echo $returnMsg;
	
	exit();
}

// Load target notice content
$lnotice = new libnotice("", $NoticeID);

$lu = new libuser($UserID);
$notifyuser = $lu->UserNameLang();

$sendTimeMode = '';
$sendTimeString = '';
$fromModule = 'eNotice';

// [2020-1009-1753-46096] scheduled mode
if(isset($sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay']) && $sys_custom['eDiscipline']['PushMsgReminder_ScheduledDelay'] > 0 && isset($_POST['scheduledDelay']) && $_POST['scheduledDelay'] > 0)
{
    $sendTimeMode = $eclassAppConfig['pushMessageSendMode']['scheduled'];
    $sendTimeString = date("Y-m-d H:i:00", strtotime("+".$_POST['scheduledDelay']." hours"));
}

$leClassApp = new libeClassApp();

$totalCount = 0;
$successCount = 0;
$pendingCount = 0;
$failCount = 0;

// loop notice
$noticeCount = count((array)$lnotice->Notice);
for($i=0; $i<$noticeCount; $i++)
{
	// Notice Info
	$currentNotice = $lnotice->Notice[$i];
	$currentNoticeID = $currentNotice["NoticeID"];
	$NoticeStudentID = str_replace("U", "", $currentNotice["RecipientID"]);
	$NoticeTitle = "".intranet_undo_htmlspecialchars($currentNotice['Title']);
	$NoticeNumber = "".$currentNotice['NoticeNumber'];
	$NoticeStartDate = "".$currentNotice['3'];
	$NoticeEndDate = "".$currentNotice['4'];
	
	// Get Parent Student Mapping
	$sql = "SELECT 
					ip.ParentID, ip.StudentID
			from 
					INTRANET_PARENTRELATION AS ip 
					Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
			WHERE 
					ip.StudentID IN ('".implode("','", (array)$NoticeStudentID)."') 
					AND iu.RecordStatus = 1";
	$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
	
	// Message UserID
	$messageInfoAry = array();
	$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
	
	// Message Title and Content
    $MessageTitle = $Lang['eDiscipline']['AppNotifyMessage']['Subject'];
    $MessageTitle = str_replace("__NOTICENUMBER__", $NoticeNumber, $MessageTitle);
    $MessageContent = str_replace("__TITLE__", $NoticeTitle, $Lang['eDiscipline']['AppNotifyMessage']['Content']);
    $MessageContent = str_replace("__NOTICENUMBER__", $NoticeNumber, $MessageContent);
    $MessageContent = str_replace("__STARTDATE__", $NoticeStartDate, $MessageContent);
    $MessageContent = str_replace("__ENDDATE__", $NoticeEndDate, $MessageContent);

    /*
    if($fromModule == '')
    {
        $currentNoticeID = '';
    }
    */

	// Send Push Message
    // [2020-1009-1753-46096]
	//$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode='', $sendTimeString='', $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule='eNotice', $currentNoticeID);
    $notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=1, $appType='', $sendTimeMode, $sendTimeString, $parNotifyMessageId='', $parNotifyMessageTargetIdAry='', $fromModule, $currentNoticeID);
	
	// Get Message Stat
	$statisticsAry = $leClassApp->getPushMessageStatistics($notifyMessageId);
	$statisticsAry = $statisticsAry[$notifyMessageId];
	if ($notifyMessageId > 0) {
		if($statisticsAry['numOfSendSuccess'] != '') {
			$successCount += $statisticsAry['numOfSendSuccess'];
		}
        if($statisticsAry['numOfWaitingToSend'] != '') {
            $pendingCount += $statisticsAry['numOfWaitingToSend'];
        }
	}
	else if ($statisticsAry['numOfRecepient'] == 0) {
		$failCount++;
	}
	$totalCount += $statisticsAry['numOfRecepient'];
}

intranet_closedb();

// Return Message
$returnMsg = "";
if ($successCount > 0) {
	$returnMsg = "<font color='#DD5555'>".str_replace("[SentTotal]", $successCount, $Lang['AppNotifyMessage']['eNotice']['send_result']). "</font>";
}
// [2020-1009-1753-46096]
else if ($pendingCount > 0) {
    $returnMsg = "<font color='#DD5555'>".str_replace("[SentTotal]", $pendingCount, $Lang['AppNotifyMessage']['eNotice']['send_msg_count']). "</font>";
}

//if($totalCount == 0 || $failCount > 0 || $successCount != $totalCount)
//if ($successCount == 0 || $failCount > 0)
if (($successCount == 0 && $pendingCount == 0) || $failCount > 0)
{
//	$ErrorMsg = $Lang['AppNotifyMessage']['eNotice']['send_failed'];
//	if ($successCount != $totalCount) {
//		$ErrorMsg .= " ".$Lang['AppNotifyMessage']['eNotice']['send_result_no_parent'];
//	}
//	$returnMsg .= $returnMsg==""? "" : "<br>";
//	$returnMsg .= "<font color='red'>".$ErrorMsg."</font> <font color='grey'>(".$totalCount."-".$successCount.")</font>";

	$returnMsg = "<font color='red'>".$Lang['AppNotifyMessage']['eNotice']['send_failed']."</font>";
}

echo $returnMsg;
?>