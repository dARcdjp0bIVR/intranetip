<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if(!isset($uid))
	header("Location: index.php");
	
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$thisStudent = new libuser($uid);

$thisWebSAMSRegNo = $thisStudent->WebSamsRegNo;
list($thisPhotoFilePath, $thisPhotoURL) = $thisStudent->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
if (file_exists($thisPhotoFilePath)) {
	list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
	$photo = "<img src='$thisPhotoURL' width='$originalWidth' height='$originalHeight' border='0'>";	
} 

$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagID";
$tagAry = $ldiscipline->returnArray($sql, 2);
for($i=0; $i<sizeof($tagAry); $i++) {
	list($tid, $tagname) = $tagAry[$i];
	$tagInfo[$i][] = $tid;	
	$tagInfo[$i][] = $tagname;
}

$selectCategory = getSelectByArray($tagInfo, "name='tagid' id='tagid' onChange='showResult(this.value)'", $tagid, 1, 1, "");

/*
$categoryArray = $ldiscipline->getAPCategoryInfo(-1);
for($i=0; $i<sizeof($categoryArray); $i++) {
	list($cid, $catname) = $categoryArray[$i];
	$catInfo[$i][] = $cid;	
	$catInfo[$i][] = $catname;
}

$selectCategory = getSelectByArray($catInfo, "name='catid' onChange='showResult(this.value)'", $catid, 1, 1, "");
*/

$stdInfo = $ldiscipline->getStudentNameByID($uid);

list($tempID, $tempName, $tempClsName, $tempClsNo) = $stdInfo[0];
$stdName = "$tempName ($tempClsName-$tempClsNo)";

$formCurrent = $ldiscipline->getClassLevelNameByUserID($uid);
$formCurrent = $formCurrent[0];
$classCurrent = $tempClsName;

$yearID = Get_Current_Academic_Year_ID();

$show1stSemester = 0;

# check whether show the "View 1st term conduct grade" button 
$semester_data = getSemesters($yearID);
$i = 0;
foreach($semester_data as $key=>$val) {
	if($val==$semester && $i!=0) {
		$view1stSemesterBtn = "<tr><td colspan='2'><span id='view1st'>".$linterface->GET_BTN($iDiscipline['ViewConductGradeOf1stSemester'], "button", "viewAPRecord();", "View1stSemester")."</span></td></tr>";
		$show1stSemester = 1;
		break;
	}
	$i++;
}


$gradeArray = split("\n",get_file_content("$intranet_root/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/gradeFile.txt"));

$remarkTable = $ldiscipline->getConductGradeRemarkTable();

$conductGradeOverview = $ldiscipline->conductGradeOverview($year, $semester, $formCurrent, $classCurrent, $uid);

$semester_data = getSemesters($yearID);
$i = 0;
foreach($semester_data as $key=>$val) {
	if($i==0) {
		$sem_name = $val;
		break;	
	}
}
/*
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($sem_name, $current) = $line;
	if($i==0) {
		break;
	}
}
*/
if($semester==$sem_name) $sem_name = "";

$APTable = $ldiscipline->conductGradeAPTable($year, $semester, $uid, $sem_name);



$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 1);

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# navigation bar
$PAGE_NAVIGATION[] = array($i_Discipline_Student_List, "index.php?semester=$semester&form=$form&class=$class&isAdjust=$isAdjust&viewList=1");
$PAGE_NAVIGATION[] = array($stdName, "");
//$PAGE_NAVIGATION[] = array("Edit", "");

# Start layout
$linterface->LAYOUT_START();

if($submitFlag=="YES")
	$msg = "<td align='right'>".$linterface->GET_SYS_MSG("update")."</td>";

?>
<script language="javascript">
<!--
function goBack() {
	form1.action = "index.php";
	document.getElementById('submitFlag').value = "";
	document.form1.submit();	
}

function viewAPRecord() {
	window.open('../view1stSemesterAPRecord.php?year=<?=$year?>&semester=<?=$semester?>&uid=<?=$uid?>','view','width=400,height=500,scrollbars=yes,resizable=yes');	
}

function showOriginal() {
	window.open('originalResult.php?year=<?=$year?>&semester=<?=$semester?>&uid=<?=$uid?>','result','width=400,height=500,scrollbars=yes,resizable=yes');	
}

function changeText(t) {
	document.getElementById("catName").innerHTML = t;
}

function checkForm(form1) {
	obj = document.form1;
	//obj.
}
//-->
</script>
<script language="javascript">
<!--
function showResult(tagid) {
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "getReassessment.php?year="+document.getElementById('year').value;
	url += "&semester=" + document.getElementById('semester').value;
	url += "&form=" + document.getElementById('form').value;
	url += "&uid=" + document.getElementById('uid').value;
	url += "&tagid=" + tagid;

	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	document.getElementById('submitFlag').value = "";
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanReassessment").innerHTML = xmlHttp.responseText;
		document.getElementById("spanReassessment").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

//-->
</script>
<form name="form1" method="POST" action="getReassessment_update.php" onSubmit="return checkForm(this)">
<br />
<table width="100%">
	
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
		<?=$msg?>
	<tr>
</table>
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td width="80%" class="tablerow1"><?=$semester?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Student?></td>
					<td width="80%" class="tablerow1"><?=$stdName?></td>
				</tr>
			</table>
		</td>
		<td align="right">
			<?=$photo?>
		</td>
	</tr>
	<?=$view1stSemesterBtn?>
</table>
<br />
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td valign="top" width="60%">
				<?=$conductGradeOverview?>
			<br />
				<?=$APTable?>
		</td>
		<? if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) { ?>
		<td valign="top" width="40%">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td>&gt;<?=$iDiscipline['ReAssessmentOfConductGrade']?></td>
					<td align='right'>
						<?=$selectCategory?>
					</td>
				</tr>
			</table>
			<span id="spanReassessment"></span>
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
			</table>
		</td>
		<? } ?>
	</tr>
</table>
<input type="hidden" name="viewList" id="viewList" value="1">
<input type="hidden" name="year" id="year" value="<?=$year?>">
<input type="hidden" name="semester" id="semester" value="<?=$semester?>">
<input type="hidden" name="form" id="form" value="<?=$form?>">
<input type="hidden" name="class" id="class" value="<?=$class?>">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="isAdjust" id="isAdjust" value="<?=$isAdjust?>">
<input type="hidden" name="submitFlag" id="submitFlag" value="">
<p>&nbsp</p>
<script language="javascript">
<!--
<?
if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	if($submitFlag=="YES")
		echo "showResult($tagid);";
	else 
		echo "showResult(1);";
}
	
if($show1stSemester==1) {
	$flag = $ldiscipline->checkConductGrade1stSemester($year, $semester, $form, $class, $uid);
	if($flag==0)
		echo "document.getElementById('view1st').style.display = 'none';";
}
?>
//-->
</script>
</form>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
