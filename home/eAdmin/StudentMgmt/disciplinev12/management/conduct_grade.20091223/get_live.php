<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=BIG5");

$ldiscipline = new libdisciplinev12();

$userID = $_SESSION['UserID'];
$year = getCurrentAcademicYear();
$yearID = Get_Current_Academic_Year_ID();

$result = $ldiscipline->getTeachingClassByClassLevelID($form);
//debug_pr($result);
$table =  "<table width='100%' cellpadding='4' cellspacing='0' border='0'>";
$table .= "		<tr class='tablebluetop'>";
$table .= "			<td class='tabletopnolink'>$i_general_class</td>";
$table .= "			<td class='tabletopnolink'>".$iDiscipline['teacher']."</td>";
$table .= "			<td class='tabletopnolink'>".$iDiscipline['CompleteRatio']."</td>";
$table .= "			<td class='tabletopnolink'>$i_Discipline_System_Discipline_Conduct_Last_Updated</td>";
$table .= "		</tr>";

for($i=0; $i<sizeof($result); $i++) {
	
	$clsID = $result[$i][0];
	$clsName = $result[$i][1];
/*
	$sql = "SELECT 
				CLSTCH.YearClassTeacherID as CT,
				SUBTCH.SubjectClassTeacherID as ST,
				COUNT(*) as totalStd
			FROM
				INTRANET_USER USR LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER SUBTCH ON (USR.UserID=SUBTCH.UserID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_USER stcu ON (SUBTCH.SubjectGroupID=stcu.SubjectGroupID) LEFT OUTER JOIN
				YEAR_CLASS_USER ycu ON (ycu.UserID=stcu.UserID) LEFT OUTER JOIN
				YEAR_CLASS_TEACHER CLSTCH ON (CLSTCH.UserID=USR.UserID) LEFT OUTER JOIN
				YEAR_CLASS CLS ON (CLSTCH.YearClassID=CLS.YearClassID OR CLS.YearClassID=ycu.YearClassID) LEFT OUTER JOIN
				YEAR y ON (CLS.YearID=y.YearID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.SubjectGroupID=SUBTCH.SubjectGroupID AND y.YearID=stcyr.YearID) LEFT OUTER JOIN
				DISCIPLINE_MS_STUDENT_CONDUCT CON ON (USR.UserID=CON.StudentID)
			WHERE
				USR.UserID=$userID AND
				(CLS.ClassTitleEN='$clsName' OR CLS.ClassTitleB5='$clsName') AND
				USR.RecordType=1 AND
				USR.RecordStatus IN (0,1,2) AND
				CLS.AcademicYearID = $yearID AND
				y.YearID = $form
			GROUP BY ycu.ClassNumber
			";	
*/

	$sql = "SELECT
				CLSTCH.YearClassTeacherID as CT,
				SUBTCH.SubjectClassTeacherID as ST
			FROM
				YEAR_CLASS yc LEFT OUTER JOIN
				YEAR_CLASS_TEACHER CLSTCH ON (yc.YearClassID=CLSTCH.YearClassID AND CLSTCH.UserID=$userID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.YearID=yc.YearID) LEFT OUTER JOIN
				SUBJECT_TERM_CLASS_TEACHER SUBTCH ON (SUBTCH.SubjectGroupID=stcyr.SubjectGroupID AND SUBTCH.UserID=$userID) LEFT OUTER JOIN
				INTRANET_USER USR ON (CLSTCH.UserID=USR.UserID OR SUBTCH.UserID=USR.UserID) LEFT OUTER JOIN
				ACADEMIC_YEAR_TERM AYT ON (AYT.AcademicYearID=yc.AcademicYearID)
				
			WHERE 
				USR.UserID=$userID AND 
				(yc.ClassTitleEN='$clsName' OR yc.ClassTitleB5='$clsName') AND
				USR.RecordType=1 AND
				USR.RecordStatus IN (0,1,2) AND
				yc.AcademicYearID = $yearID	AND
				yc.YearID = $form AND
				(AYT.YearTermNameEN='$semester' OR AYT.YearTermNameB5='$semester')
			GROUP BY yc.YearClassID
			";
				
	$detail = $ldiscipline->returnArray($sql);
	$data = $detail[0];
	
	$stdid = "";
	$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($clsName);
	
	for($j=0; $j<sizeof($stdInfo); $j++) {
		list($clsNo, $name, $sid) = $stdInfo[$j];
		$stdid .= ($j!=0) ? ",".$sid : $sid;
	}
	$studentTotal = sizeof(explode(',', $stdid));
	
	$sql = "SELECT COUNT(*), MAX(LEFT(DateModified,10)) FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE Year='$year' AND Semester='$semester' AND StudentID IN ($stdid) AND UserID=".$userID;
	$temp = $ldiscipline->returnArray($sql, 2);
	$tempData = $temp[0];	

	$css = ($i%2==0) ? 1 : 2;
	$table .= "		<tr class='tablebluerow$css'>";
	$table .= "		<td class='tabletext'><a href='javascript:;' onclick=\"document.getElementById('class').value='::{$clsID}';goSubmit();\" class='tablelink'>{$clsName}</a></td>";
	$table .= "		<td class='tabletext'>";
	$table .= ($data[0]!="") ? $i_Teaching_ClassTeacher : $i_Teaching_SubjectTeacher;
	$table .= "		</td>";
	$table .= "		<td class='tabletext'><font color='#FF0000'>$tempData[0]</font>/$studentTotal</td>";
	$table .= "		<td class='tabletext'>$tempData[1]</td>";
	$table .= "	</tr>";
	
}
$table .= "</table>";

intranet_closedb();

echo $table;
?>