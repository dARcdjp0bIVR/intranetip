<?php
// Modifying by : 

##### Change Log [Start] #####
#
#   Date    :   2019-05-31 (Bill)   [2017-0706-1015-04235]
#               improved: allow select all classes + load selected class student selection after submit
#
#	Date	:	2017-03-13 (Bill)	[2017-0123-1653-40066]
#				fixed multiple submit problem
#
#	Date	:	2017-02-14 (Bill)	[2017-0123-1653-40066]
#				disable confirm button to prevent double submit lead to duplicate AP records
#
#	Date	:	2012-04-11 (Henry Chow)
#				add options "Record Type", "Category" and "Conversion Period"
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!$sys_custom['ReCalculateAccumulatedRecord']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eDiscipline']['ReCalculateAccumulatedRecord']);

$lclass = new libclass();
$linterface = new interface_html();

# School year
$selectYear = (!isset($selectYear) || $selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$selectYear = IntegerSafe($selectYear);

# Re-calculate action
if($submit_flag=="YES")
{
    $studentID = IntegerSafe($studentID);
	$thisCategoryId = ($recordType==1) ? $selectGoodCat : $selectMisCat;
	$thisCategoryId = IntegerSafe($thisCategoryId);
	$thisPeriodId = $PeriodID;
	$thisPeriodId = IntegerSafe($thisPeriodId);
	
	# All Classes
	if($targetClass == '#')
	{
	    $result = array();
	    
	    $sql = "SELECT YearID FROM YEAR ORDER BY Sequence";
	    $formIDArr = $ldiscipline->returnVector($sql);
	    for($i=0; $i<sizeof($formIDArr); $i++)
	    {
	        $formID = $formIDArr[$i];
	        
	        $sql2 = "SELECT YearClassID FROM YEAR_CLASS WHERE AcademicYearID = '".Get_Current_Academic_Year_ID()."' AND YearID = '$formID' ORDER BY Sequence, ".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN');
	        $targetClassArr = $ldiscipline->returnVector($sql2);
	        for($j=0; $j<sizeof($targetClassArr); $j++) {
	            $targetClass = '::'.$targetClassArr[$j];
                $result[] = $ldiscipline->ReCalculateAccumulatedRecord($selectYear, $targetClass, $studentID, $thisCategoryId, $thisPeriodId);
            }
        }
        
        $result = !empty($result) && !in_array(false, $result);
        $targetClass = '';
	}
	else
	{
	    $targetClass = str_replace('::', '', $targetClass);
	    $targetClass = '::'.IntegerSafe($targetClass);
	    $result = $ldiscipline->ReCalculateAccumulatedRecord($selectYear, $targetClass, $studentID, $thisCategoryId, $thisPeriodId);
	}
	
	// [2017-0123-1653-40066] fixed multiple submit problem due to page refresh 
	//header("Location: recalculateRecord.php?msg=UpdateSuccess");
	//$msg = "UpdateSuccess";
	header("Location: recalculateRecord.php?targetClass=$targetClass&recordType=$recordType&msg=UpdateSuccess");
	exit();
}

# Target Selection Menu
// $select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, "", $i_general_please_select, "", 0);
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, "", $i_Discipline_System_Award_Punishment_All_Classes, "", 0);

# Good Conduct & Misconduct Categories
$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1);

# Good Conduct Category
$selectGoodCatHTML = "<select name=\"selectGoodCat\" id=\"selectGoodCat\" onChange=\"changeCat(this.value);\">\n";
$selectGoodCatHTML .= "<option value=\"0\"";
$selectGoodCatHTML .= ($submit_flag != "YES")?" selected":"";
$selectGoodCatHTML .= ">-- ".$i_alert_pleaseselect." --</option>";
for ($i = 0; $i < sizeof($good_cats); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$good_cats[$i][0]."\"";
	if ($selectGoodCat == $good_cats[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$good_cats[$i][1]."</option>\n";
}
$selectGoodCatHTML .= "</select>\n";

# Misconduct Category
$selectMisCatHTML = "<select name=\"selectMisCat\" id=\"selectMisCat\" onChange=\"changeCat(this.value);\">\n";
$selectMisCatHTML .= "<option value=\"0\"";
$selectMisCatHTML .= ($submit_flag != "YES")?" selected":"";
$selectMisCatHTML .= ">-- $i_alert_pleaseselect --</option>";
for ($i = 0; $i < sizeof($mis_cats); $i++) {
	$selectMisCatHTML .= "<option value=\"".$mis_cats[$i][0]."\"";
	if ($selectMisCat == $mis_cats[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$mis_cats[$i][1]."</option>\n";
}

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);
?>

<script language="javascript">
var xmlHttp
function showResult(str, studentid)
{
	if (str.length==0)
	{
		document.getElementById("studentID").innerHTML = "";
		document.getElementById("studentID").style.border = "0px";
		return
	}
	
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	
	var url = "";
	url = "../../reports/get_live.php";
	url = url + "?targetClass=" + str
	url = url + "&sid=" + studentid
	
	xmlHttp.onreadystatechange = stateChanged 
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
}

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("studentID").innerHTML = xmlHttp.responseText;
		document.getElementById("studentID").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function goBack() {
	self.location.href = "index.php";	
}

function checkForm(){
	var choice = "";
	var obj = document.form1; 
	
	// if(obj.targetClass.value=="" || obj.targetClass.value=="#") {
	if(obj.targetClass.value=="") {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Target?>");
		return false;	
	}
	else if((obj.recordType[0].checked==true && obj.selectGoodCat.selectedIndex==0) || (obj.recordType[1].checked==true && obj.selectMisCat.selectedIndex==0)) {
		alert("<?=$i_alert_pleaseselect." ".$Lang['eDiscipline']['RecordType']?>");
		return false;
	}
	else if (($("#PeriodID").length==0)) {
		alert("<?=$iDiscipline['AccumulativePunishment_Import_Failed_Reason4']?>");
		return false;
	}
	else {
		if(confirm("<?=$Lang['eDiscipline']['ReCalculateJsAlert']?>")) {
			$("#submitBtn01").attr("disabled", true);
			document.getElementById('submit_flag').value='YES';
			return true;
		}
		else {
			return false;	
		}	
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}
</script>

<form name="form1" method="post" action="recalculateRecord.php" onSubmit="return checkForm()">
<div class="navigation_v30 navigation_2">
	<a href="javascript:goBack();"><?=$eDiscipline["RecordList"]?></a>
</div>
<p class="spacer"></p>
<br style="clear:both">
<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td align="right" colspan="2"><?= $linterface->GET_WARNING_TABLE($Lang['eDiscipline']['ReCalculateAccumulatedRecordWarning']) ?></td>
	</tr>
</table>
<br class="spacer">
<table class="form_table_v30">
	<tr valign="top">
		<td width="10%" valign="top" nowrap="nowrap" class="field_title">
			<?=$i_Discipline_School_Year?>
		</td>
		<td width="90%">
			<?=getCurrentAcademicYear()?>
		</td>
	</tr>
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title" height="">
			<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
		</td>
		<td height="">
			<?=$select_class?>
			<br class="spacer">
			<br class="spacer">
			<div id='studentID' style='position:relative; width:280px; height:20px; z-index:1;'></div>
		</td>
	</tr>
	<tr valign="top">
		<td width="10%" valign="top" nowrap="nowrap" class="field_title">
			<?=$Lang['eDiscipline']['RecordType']?> <span class="tabletextrequire">*</span>
		</td>
		<td width="90%">
			<input name="recordType" type="radio" id="record_type_good" value="1" onClick="hideSpan('MisCatDiv');showSpan('GoodCatDiv');" <? if($recordType=="" || $recordType=="1") echo "checked";?>><label for="record_type_good"><?=$i_Discipline_GoodConduct?></label>
			<input name="recordType" type="radio" id="record_type_mis" value="-1" onClick="hideSpan('GoodCatDiv');showSpan('MisCatDiv');" <? if($recordType=="-1") echo "checked";?>><label for="record_type_mis"><?=$i_Discipline_Misconduct?></label>
			<div id="GoodCatDiv" style="display:<?= ($recordType=="" || $recordType=="1") ? "block" : "none"?>;">
			<?=$selectGoodCatHTML?>
			</div>
			<div id="MisCatDiv" style="display:<?= ($recordType=="-1") ? "block" : "none"?>;">
			<?=$selectMisCatHTML?>
			</div>
		</td>
	</tr>
	<tr valign="top">
		<td width="10%" valign="top" nowrap="nowrap" class="field_title">
			<?=$eDiscipline['Setting_Period']?> <span class="tabletextrequire">*</span>
		</td>
		<td width="90%">
			<div id="periodDiv"><?=$Lang['General']['EmptySymbol']?></div>
		</td>
	</tr>
</table>
	
<span class="tabletextremark"><?=$i_general_required_field?></span>
<div class="edit_bottom_v30">
<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "submitBtn01", "", "", "formbutton_alert")?>
&nbsp;
<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack()", "", "", "")?>
</div>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="selectYear" id="selectYear" value="<?=$selectYear?>">			

</form>
<br />

<script language="Javascript">
function init()
{
	var obj = document.form1;
	<?php if($targetClass != '' && $targetClass != '#') { ?>
	    showResult("<?=$targetClass?>");
	<?php } else { ?>
		showResult("class","");
	<?php } ?>
}

<?
if($submitBtn01 == "") {
	echo "init();\n";
}
else {
	echo "showResult(\"$targetClass\", \"$studentID\");\n";
	if($recordType==1) {
		echo "changeCat($selectGoodCat);\n";
	}
	else {
		echo "changeCat($selectMisCat);\n";
	}
}
?>

function showSpan(layername) {
	$("#"+layername).show();	
	ResetSelection();
}

function hideSpan(layername) {
	$("#"+layername).hide();
}

function ResetSelection() {
	$("#periodDiv").html("<?=$Lang['General']['EmptySymbol']?>");	
	document.getElementById('selectGoodCat').selectedIndex = 0;
	document.getElementById('selectMisCat').selectedIndex = 0;	
}

var js_LAYOUT_SKIN = "<?=$LAYOUT_SKIN?>";
var loading = "<img src='/images/" + js_LAYOUT_SKIN + "/indicator.gif'>";

function changeCat(CatID) {
	$('#periodDiv').html(loading);
	var recordType = $('#record_type_good').is(':checked') ? 1 : -1;
	var thisAcademicYearID = $("#selectYear").val();
	
	var selectedPeriod = '<?=$PeriodID?>';
	
	if(CatID==0) {
		ResetSelection();
	}
	else {
		$.post(
			"get_live.php", 
			{ 
				flag: 'loadConversionPeriod',
				AcademicYearID:thisAcademicYearID,
				recordType: recordType,
				selectedPeriod: selectedPeriod,
				CatID: CatID
			},
			function(ReturnData)
			{
				$('#periodDiv').html(ReturnData);
			}
		);	
	}
}
//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>