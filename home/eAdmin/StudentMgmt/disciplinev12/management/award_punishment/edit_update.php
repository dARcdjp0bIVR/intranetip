<?php
# using: 

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose) (replace "U")
#
#   Date    :   2019-05-31  Bill
#               prevent SQL Injection + Cross-site Scripting + Redirect Problem
#
#   Date    :   2019-05-01  Bill    [DM#1197]
#               Fixed: PHP error msg - implode()
#
#   Date    :   2019-03-15  (Bill)  [2018-0710-1351-39240]
#               auto delete and re-send discipline notice for demerit records ($sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'])
#
#	Date	:	2017-04-05	(Bill)
#				auto convert Conudct Score depends on Merit Type and Number
#				($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore'] and $sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings'])
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				support Activity Score update
#
#	Date	:	2015-06-29 (Bill)	[2015-0609-1638-43222]
#				reset overflow flags, update ConductScoreFakeChange if any overflow flag is set
#
#	Date	: 	2015-06-18 (Carlos): Added $RecordID to be excluded for edit case in $ldiscipline->GetMeritItemTagConductOverflowScoreStudents()
#	Date	:	2013-11-18 (YatWoon)
#				Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-06-21 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] - mark merit item as overflowed and ignore changing conduct mark balance
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

### Handle SQL Injection + XSS [START]
$RecordID = IntegerSafe($RecordID);

if(!intranet_validateDate($RecordDate)) {
    $RecordDate = '';
}
if(isset($RecordInputDate) && !intranet_validateDate($RecordInputDate)) {
    $RecordInputDate = '';
}
if(isset($ActionDueDate) && !intranet_validateDate($ActionDueDate)) {
    $ActionDueDate = '';
}

if(is_array($PIC) && sizeof($PIC) > 0) {
    foreach($PIC as $k => $d) {
		$d = str_replace("U", "", $d);
        $PIC[$k] = IntegerSafe($d);
    }
}
$SubjectID = IntegerSafe($SubjectID);

$ItemID = IntegerSafe($ItemID);
if(isset($MeritNum)) {
    if($ldiscipline->AP_Interval_Value) {
        $MeritNum = (float)$MeritNum;
    }
    else {
        $MeritNum = IntegerSafe($MeritNum);
    }
}
if(isset($MeritType)) {
    $MeritType = IntegerSafe($MeritType);
}
if(isset($ConductScore)) {
    if($sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {
        $ConductScore = (float)$ConductScore;
    }
    else {
        $ConductScore = IntegerSafe($ConductScore);
    }
}
if(isset($StudyScore)) {
    $StudyScore = IntegerSafe($StudyScore);
}
if(isset($ActivityScore)) {
    $ActivityScore = IntegerSafe($ActivityScore);
}

$back_page = cleanCrossSiteScriptingCode($back_page);
if($back_page != 'index.php' && $back_page != 'detail.php') {
    $back_page = 'index.php';
}
### Handle SQL Injection + XSS [END]

if(empty($RecordID))	header("Location: index.php");

$lteaching = new libteaching();

# Check access right
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || (($ldiscipline->isMeritRecordPIC($RecordID) || $ldiscipline->isMeritRecordOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"))))
{
    $ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Original data
$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
$data = $datainfo[0];
$StudentID = $data['StudentID'];
$record_type = $data['MeritType'];
$original_conduct = $data['ConductScoreChange'];
$original_studyScore = $data['SubScore1Change'];
$original_activityScore = $data['SubScore2Change'];
$isAcc = $data['fromConductRecords'];
$isWaived = $data['RecordStatus']==DISCIPLINE_STATUS_WAIVED ? 1 : 0;
$original_notice = $data['NoticeID'];

# Checking on duplicate item on same day (Start)
$isExist = array();
$fail = 0;

# Not allow same student with same item in a same day
if($ldiscipline->NotAllowSameItemInSameDay) {
	$isExist[$StudentID] = $ldiscipline->checkSameAPItemInSameDay($StudentID, $ItemID, $RecordDate, $RecordID);
	if($isExist[$StudentID]==1) $fail = 1;
}

if($fail == 1) { ?>
	<form name="form1" method="POST" action="edit.php">
		<input type="hidden" name="RecordID[]" id="RecordID[]" value="<?=$RecordID?>">
		<input type="hidden" name="StudentID[]" id="StudentID[]" value="<?=$StudentID?>">
		<input type="hidden" name="duplicate" id="duplicate" value="1">
		<input type="hidden" name="back_page" id="back_page" value="<?=$back_page?>">
	</form>
	<script language="javascript">
		document.form1.submit();
	</script>
<? 
	exit();
	# checking on duplicate item on same day (End)
}
else {
	# Item Text
	$ItemInfo = $ldiscipline->returnMeritItemByID($ItemID);
	$ItemText = $ItemInfo['ItemName'];
	$ItemText = addslashes($ItemText);
	
	# Remarks
	$remark = intranet_htmlspecialchars($remark);
	//$ItemText = addslashes(intranet_htmlspecialchars($ItemText));

    # [2020-0824-1151-40308] Follow-up Remark
    $followUpRemark = intranet_htmlspecialchars($followUpRemark);
	
	# PIC
	if(sizeof($PIC) > 0) {
		$PIC = implode(",", (array)$PIC);
	}
	else {
		$PIC = "";	
	}
	
	# Update record info (DateModified, ModifiedBy)
	$dataAry = array();
	if($isAcc || $isWaived)
	{
		$dataAry['Remark'] = $remark;
		$dataAry['PICID'] = $PIC;
	// 	$dataAry['ItemID'] = $ItemID;
	// 	$dataAry['ItemText'] = $ItemText;
	}
	else
	{
		### Retrieve extra data
		# School Year
		$school_year = GET_ACADEMIC_YEAR3($RecordDate);
		
		# Semester
		/*
		$semester_mode = getSemesterMode();
		if($semester_mode==2)	
			$semester = retrieveSemester($RecordDate);
		*/
		$semesterName = retrieveSemester($RecordDate);
		
		$AcademicInfoArray = getAcademicYearInfoAndTermInfoByDate($RecordDate);
		if(sizeof($AcademicInfoArray) > 0){
			$AcademicYearID = $AcademicInfoArray[0];
			$YearName = $AcademicInfoArray[1];
			$YearTermID = $AcademicInfoArray[2];
			$YearTermName = $AcademicInfoArray[3];
			
			$academic_year_id = $AcademicYearID;
			$year_term_id = $YearTermID;
		}
	    
		# Conduct Score
		$ConductScoreChange = $ConductScore * $record_type;
		if($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore'] && !empty($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings']) && $record_type=="-1")
		{
			// Auto Convert Conudct Score depends on Merit Type and Number 
			$MeritTypeConductScore = $sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings'][$MeritType];
			if($MeritTypeConductScore > 0 && $MeritNum > 0) {
				$ConductScoreChange = $MeritTypeConductScore * $MeritNum * $record_type;
			}
			else {
				$ConductScoreChange = 0;
			}
		}
		
		# Study Score
		$StudyScoreChange = $StudyScore * $record_type;
		
		# Activity Score
		$ActivityScoreChange = $ActivityScore * $record_type;
		
		# Subject
		$SubjectName = $lteaching->returnSubjectName($SubjectID);
		
		# Check user can access / edit this item or not
		$CatID = $ldiscipline->returnCatIDByItemID($ItemID);
		$AccessItem = 0;
		if($ldiscipline->IS_ADMIN_USER($UserID))
		{
			$AccessItem = 1;
		}
		else
		{
			$ava_item = $ldiscipline->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
			if($ava_item[$CatID]) {
				$AccessItem = in_array($ItemID, $ava_item[$CatID]);
			}
		}
		
		$dataAry['RecordDate'] = $RecordDate;
		if($sys_custom['eDiscipline']['yy3'])
		{
			$dataAry['RecordInputDate'] = $RecordInputDate;
		}
		
		$dataAry['Year'] = $school_year;
		$dataAry['Semester'] = $semesterName;
		if($academic_year_id != "") {
			$dataAry['AcademicYearID'] = $academic_year_id;
		}
		if($year_term_id != "") {
			$dataAry['YearTermID'] = $year_term_id;
		}
		
		$dataAry['StudentID'] = $StudentID;
		if($AccessItem) 
		{
			$dataAry['ItemID'] = $ItemID;
			$dataAry['ItemText'] = $ItemText;
			
			$dataAry['MeritType'] = $record_type;
			$dataAry['ProfileMeritType'] = $MeritType;
			$dataAry['ProfileMeritCount'] = $MeritNum;
			
			$dataAry['ConductScoreChange'] = $ConductScoreChange;
			$dataAry['SubScore1Change'] = $StudyScoreChange;
			$dataAry['SubScore2Change'] = $ActivityScoreChange;
			
			# [2018-0710-1351-39240] auto delete notice if conduct score changed
			if($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore'] && $sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'] && 
			     $record_type == "-1" && $original_conduct != $ConductScoreChange)
			{
			    include_once($PATH_WRT_ROOT."includes/libnotice.php");
			    $lnotice = new libnotice();
			    
			    # Delete Notice first
			    if($original_notice)
			    {
			        $lnotice->deleteNotice($original_notice);
			        
			        $sql = "UPDATE DISCIPLINE_MERIT_RECORD SET NoticeID = NULL Where RecordID = '$RecordID' ";
			        $lnotice->db_db_query($sql);
			    }
			    
			    # Check if need to sent new notice
			    if(isset($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings'][$MeritType]) && $MeritNum > 0)
			    {
			        $needToResendNotice = true;
			    }
			}
		}
		$dataAry['Subject'] = $SubjectName;
		$dataAry['Remark'] = $remark;
		$dataAry['PICID'] = $PIC;
		$dataAry['SubjectID'] = $SubjectID;
	}
	$dataAry['ActionDueDate'] = $ActionDueDate;
	
	if(sizeof($Attachment) == 0) {
		$dataAry['Attachment'] = "";
	} else {
		$dataAry['Attachment'] = $sessionTime;
	}

	// [2020-0824-1151-40308]
    if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
        $dataAry['FollowUpRemark'] = $followUpRemark;
    }
	
	// [2015-0609-1638-43222] Reset overflow flags
	$dataAry['OverflowMeritItemScore'] = '';
	$dataAry['OverflowConductMark'] = '';
	$dataAry['OverflowTagScore'] = '';
	
	// [2015-0609-1638-43222] Set overflow flags and update ConductScoreFakeChange
	if($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']))
	{
		$overflowMarkInfoAry = $ldiscipline->GetMeritItemTagConductOverflowScoreStudents($StudentID, $academic_year_id, $year_term_id, $ItemID, $record_type, $ConductScore, false, $RecordID);
		if(in_array($StudentID,$overflowMarkInfoAry['MeritScoreOverflowStudentID'])){
			$dataAry['OverflowMeritItemScore'] = '1';
			$dataAry['ConductScoreFakeChange'] = $ConductScoreChange;
		}
		if(in_array($StudentID,$overflowMarkInfoAry['ConductScoreOverflowStudentID'])){
			$dataAry['OverflowConductMark'] = '1';
			$dataAry['ConductScoreAfter'] = $overflowMarkInfoAry['ConductScoreOverflowDetail'][$StudentID]['MarkAfterChange'];
			$dataAry['ConductScoreFakeChange'] = $ConductScoreChange;
		}
		if(in_array($StudentID,$overflowMarkInfoAry['TagScoreOverflowStudentID'])){
			$dataAry['OverflowTagScore'] = '1';
			$dataAry['ConductScoreFakeChange'] = $ConductScoreChange;
		}
	}
	
	if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
	{
		$file_number = intranet_htmlspecialchars($file_number);
		$record_number = intranet_htmlspecialchars($record_number);
		
		$dataAry['FileNumber'] = $file_number;
		$dataAry['RecordNumber'] = $record_number;
	}
	$ldiscipline->UPDATE_MERIT_RECORD($dataAry, $RecordID);
	
	# [2018-0710-1351-39240] auto re-send notice
	if($sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'] && $needToResendNotice)
	{
	    $template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID']);
	    if(sizeof($template_info))
	    {
	        $template_category = $template_info[0]['CategoryID'];
	        $SpecialData = array();
	        $SpecialData['MeritRecordID'] = $RecordID;
	        $TextAdditionalInfo = '';
	        $includeReferenceNo = 1;
	        $template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($template_category, $StudentID, $SpecialData, $TextAdditionalInfo, $includeReferenceNo);
	        
	        $UserName = $ldiscipline->getUserNameByID($UserID);
	        $Module = $ldiscipline->Module;
	        $NoticeNumber = time();
	        $TemplateID = $sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'];
	        $Variable = $template_data;
	        $IssueUserID = $UserID;
	        $IssueUserName = $UserName[0];
	        $TargetRecipientType = "U";
	        $RecipientID = array($StudentID);
	        $RecordType = NOTICE_TO_SOME_STUDENTS;
	        
	        # Send Notice
	        include_once($intranet_root."/includes/libucc.php");
	        $lc = new libucc();
	        $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

            // [2020-1009-1753-46096] delay notice start date & end date
            if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
            {
                $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                $lc->setDateStart($DateStart);

                if ($lc->defaultDisNumDays > 0) {
                    $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                    $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                    $lc->setDateEnd($DateEnd);
                }
            }

	        $NoticeID = $lc->sendNotice($emailNotify=0);
	        
	        # Suspend Notice
	        if($sys_custom['eDiscipline']['NotReleaseNotice'] && $NoticeID != -1) {
	            $lnotice->changeNoticeStatus($NoticeID, SUSPENDED_NOTICE_RECORD);
	        }
	        
	        # Update demerit data
	        if($NoticeID != -1)
	        {
	            $dataAry = array();
	            $dataAry['NoticeID'] = $NoticeID;
	            $dataAry['TemplateID'] = $sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'];
	            $dataAry['TemplateOtherInfo'] = '';
	            $ldiscipline->UPDATE_MERIT_RECORD($dataAry, $RecordID);
	        }
	    }
    }
    
	// # calculate the difference of conduct score
	// # update the balance and log if diff!=0
	// if($ConductScoreChange != $original_conduct)
	// {
	// 	$conduct_diff = $ConductScoreChange - $original_conduct;
	// 	$ldiscipline->UPDATE_CONDUCT_BALANCE($StudentID, $school_year, $semester, $conduct_diff, $RecordID, DISCIPLINE_CONDUCT_SCORE_CHANGE_TYPE_UPDATE);
	// }
	
	// # calculate the difference of sub score
	// # update the balance and log if diff!=0
	
	# Build back link
	$back_par = "num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&s=$s&clickID=$clickID&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate";
	if($back_page=="detail.php") {
		$back_par .= "&SchoolYear2=$SchoolYear2&semester2=$semester2&targetClass2=$targetClass2&MeritType2=$MeritType2&pic=$pic";
	}
	else {
		$back_par .= "&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&MeritType=$MeritType2&pic=$pic";
	}
	header("Location: ". $back_page ."?id=$RecordID&msg=update&$back_par");
	
	intranet_closedb();
}
?>