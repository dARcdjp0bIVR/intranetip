<?php
// Using: 

############ Change Log Start #############################
#
#   Date    :   2020-11-12 (Bill)   [2020-1022-1609-39073]
#               added more return msg if cannot approve all / some APs
#
# 	Date	:	2013-06-21 (Rita)
#				Add email alert for yy3 cust
#
#	Date	:	2013-02-19 (YatWoon)
#				revised the approval checking logic
#
#	Date	:	2012-10-09 (YatWoon)
#				Missing to check the approval group right for approve / reject [Case#2012-1004-1026-21054]
#
############################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
	
# Check access right
// $ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Approval");
//if(!($hasApprovalRight && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")))
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View"))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
# check whether current user has the approval right (according to settings in "Approval Group")
$hasRightToApproveRecordID = array();
if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) {
	$hasRightToApproveRecordID = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED($UserID, "", DISCIPLINE_STATUS_PENDING.",".DISCIPLINE_STATUS_REJECTED.",".DISCIPLINE_STATUS_WAIVED);
	$hasApprovalRight = (sizeof($hasRightToApproveRecordID)>0) ? 1 : 0;	
} else
{
	$hasApprovalRight = 1;
}
*/
/*
$useApprovalGroup = $ldiscipline->useApprovalGroup();
if($useApprovalGroup && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"])
{
	$hasApprovalRight = (sizeof($hasRightToApproveRecordID)>0 && in_array($id,$hasRightToApproveRecordID)) ? 1 : 0;	
}
*/

$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&pic=$pic&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&released=$released&waived=$waived&fromPPC=$fromPPC";

$RecordIDAry = array();
$RecordIDAry = $RecordID;

if($back_page) {
	$returnPath = $back_page."?id=$RecordIDAry[0]";
} else {
	$returnPath = "index.php?$return_filter";
}

$resultAry = array();
foreach((array)$RecordIDAry as $MeritRecordID)
{
	# Double check user can approve the record or not 
	$can_approve = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($MeritRecordID);
	
	# Set APPROVE
	if($can_approve)
	{
        $resultAry[] = $ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID);
		
		# Customization
		if($sys_custom['eDiscipline']['yy3']){
			## Check if Can Send Exceed Max Gain/ Deduct Mark email
			$ldiscipline->SEND_EXCEED_MAX_GAIN_DEDUCT_MARK_EMAIL($MeritRecordID);
		}
	}
}

// [2020-1022-1609-39073]
if(!empty($resultAry) && !in_array(false, (array)$resultAry)) {
    $msg = "approve";
}
else if(!empty($resultAry) && in_array(true, (array)$resultAry)) {
    $msg = "not_all_approve";
}
else {
    $msg = "no_record_approve";
}

header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>