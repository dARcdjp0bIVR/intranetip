<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$tempLayer = $ldiscipline->getWaivedInfo($RecordID, "{$image_path}/{$LAYOUT_SKIN}");

echo $tempLayer;

//$linterface->LAYOUT_STOP();
intranet_closedb();

?>