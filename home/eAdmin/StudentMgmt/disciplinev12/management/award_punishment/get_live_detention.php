<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$tempLayer = $ldiscipline->getDetentionLayer($RecordID, "{$image_path}/{$LAYOUT_SKIN}");

echo $tempLayer;

/*
# Check access right (View page)
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

	//$tempLayer .= "<div id='show_detail".$RecordID."' style='position:absolute; width:280px; height:180px; z-index:1; visibility: hidden;'>";
	$tempLayer .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' class='layer_table'>";
	$tempLayer .= "<tr><td height='19'>";
	$tempLayer .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$tempLayer .= "<tr>";
	$tempLayer .= "<td width='5' height='19'><img src='{$image_path}/{$LAYOUT_SKIN}/can_board_01.gif' width='5' height='19'></td>";
	$tempLayer .= "<td height='19' valign='middle' background='{$image_path}/{$LAYOUT_SKIN}/can_board_02.gif'></td>";
	$tempLayer .= "<td width='19' height='19'><a href='javascript:;' onClick=\"MM_showHideLayers('show_detail".$RecordID."','','hide')\"><img src='{$image_path}/{$LAYOUT_SKIN}/can_board_close_off.gif' name='pre_close21' width='19' height='19' border='0' id='pre_close21' onMouseOver=\"MM_swapImage('pre_close21','','{$image_path}/{$LAYOUT_SKIN}/can_board_close_on.gif',1)\" onMouseOut=\"MM_swapImgRestore()\"></a></td>";
	$tempLayer .= "</tr></table></td></tr><tr><td>";
	$tempLayer .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$tempLayer .= "<tr>";
	$tempLayer .= "<td width='5' background='{$image_path}/{$LAYOUT_SKIN}/can_board_04.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/can_board_04.gif' width='5' height='19'></td>";
	$tempLayer .= "<td bgcolor='#FFFFF7'><div style='height:150px; overflow:auto;'>";
	$tempLayer .= "<table width='98%' border='0' cellpadding='0' cellspacing='0' bgcolor='#CCCCCC' class='layer_table_detail'>";
	$tempLayer .= "<tr class='tabletop'>";
	$tempLayer .= "<td align='center' valign='middle'>#</td>";
	$tempLayer .= "<td align='left'>Date</td>";
	$tempLayer .= "<td align='left'>Session</td>";
	$tempLayer .= "<td align='left'>PIC </td>";
	$tempLayer .= "</tr>";

	$name_field = getNameFieldByLang("c.");
	$sql = "SELECT $name_field, DetentionDate, LEFT(StartTime,5), LEFT(EndTime,5), a.DetentionID
			FROM DISCIPLINE_DETENTION_SESSION as a
				LEFT OUTER JOIN DISCIPLINE_DETENTION_SESSION_PIC as b ON (b.DetentionID = a.DetentionID)
				LEFT OUTER JOIN INTRANET_USER as c ON (c.UserID = b.UserID)
				LEFT OUTER JOIN DISCIPLINE_DETENTION_STUDENT_SESSION as d ON (a.DetentionID=d.DetentionID)
			WHERE c.RecordType = 1
				AND c.Teaching = 1
				AND c.RecordStatus = 1
				AND d.DemeritID = ".$RecordID."
			ORDER BY DetentionDate DESC
			";
	$result = $ldiscipline->returnArray($sql, 4);

	if(sizeof($result)==0) {
		$tempLayer .= "<tr class='row_approved'>";
		$tempLayer .= "<td colspan='4'>Not yet assigned";
		$tempLayer .= "</td></tr>";
	} else {
		for($i=0;$i<sizeof($result);$i++) {
			$k = $i+1;
			$tempLayer .= "<tr class='row_approved'>";
			$tempLayer .= "<td align='center' valign='top'>".$k."</td>";
			$tempLayer .= "<td align='left' valign='top' class='tabletext'>";
			$tempLayer .= ($result[$i][4]=='') ? "Not Confirmed Yet" : $result[$i][1];
			$tempLayer .= "</td><td align='left' valign='top' class='tabletext'>";
			$tempLayer .= ($result[$i][4]=='') ? "---" : ($result[$i][2]." - ".$result[$i][3]);
			$tempLayer .= "</td><td align='left' valign='top' class='tabletext'>";
			$tempLayer .= ($result[$i][4]=='') ? "---" : $result[$i][0];
			$tempLayer .= "</td></tr>";
		}
	}
	$tempLayer .= "</table></div><br></td>";
	$tempLayer .= "<td width='6' background='{$image_path}/{$LAYOUT_SKIN}/can_board_06.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/can_board_06.gif' width='6' height='6'></td>";
	$tempLayer .= "</tr><tr>";
	$tempLayer .= "<td width='5' height='6'><img src='{$image_path}/{$LAYOUT_SKIN}/can_board_07.gif' width='5' height='6'></td>";
	$tempLayer .= "<td height='6' background='{$image_path}/{$LAYOUT_SKIN}/can_board_08.gif'><img src='{$image_path}/{$LAYOUT_SKIN}/can_board_08.gif' width='5' height='6'></td>";
	$tempLayer .= "<td width='6' height='6'><img src='{$image_path}/{$LAYOUT_SKIN}/can_board_09.gif' width='6' height='6'></td>";
	$tempLayer .= "</tr></table></td></tr></table>";
	//$tempLayer .= "</div>";

	//return $tempLayer;
	
	echo $tempLayer;
*/
//$linterface->LAYOUT_STOP();
intranet_closedb();

?>