<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#   Detail	:	Support Follow-up Feedback Import       ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#               Improved: next line inputted in csv file
#
#	Date	:	2020-02-14	(Bill)	[2020-0212-1033-21073]
#	Detail	:	improve performance if import many records (> 100)
#               - apply temp data array to reduce query number
#
#	Date	:	2017-06-30 (Bill)	[2017-0629-1459-19235]
#	Detail	:	Copy auto approval logic from new3_update.php
#
#	Date	:	2017-06-12 (Bill)	[2016-1207-1221-39240]
#	Detail	:	Support Activity Score Import
#
#	Date	:	2015-10-26	(Bill)	[2015-0611-1642-26164]
#	Detail	: 	Change logic for CWC Probation Scheme
#				- Auto Approve AP records
#				- Approve probation records if current record is punishment
#
#	Date	:	2013-11-21 (YatWoon)
#	Detail	: 	Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-06-24 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] - mark merit item as overflowed and ignore changing conduct mark balance
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#
#	Date    :	2011-09-02	YatWoon
#			    improved: add default template selection related coding
#
#	Date    :	2010-05-07	YatWoon
#			    set "Approve by" as auto (-1) if auto approve by system
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$limport = new libimporttext();

$li = new libdb();
$lf = new libfilesystem();
$lu = new libuser();
$lnotice = new libnotice();

$filepath = $csvfile;
$filename = $csvfile_name;

$ldiscipline = new libdisciplinev12();

# Check Access Right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

if($sys_custom['eDiscipline']['yy3'])
{
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

/*
$result_ary = array();
$imported_student = array();
$student = array();

## findout student ary
for($i=0;$i<sizeof($data);$i++)
{
	### get data
	$ClassName = $data[$i][0];
	$ClassNumber = $data[$i][1];

	### checking - valid class & class number (student found)
	$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
	if($StudentID)
		array_push($student, $StudentID);
}
*/

# Get CSV data from temp table
$sql = "SELECT * FROM temp_profile_student_merit_import WHERE UserID = ". $_SESSION["UserID"];
$data = $ldiscipline->returnArray($sql);

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>";
if($sys_custom['eDiscipline']['yy3']){
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['EventRecordDate']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['RecordType']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline["RecordItem"]."</td>";
if ($ldiscipline->use_subject) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_Subject</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Conduct_Mark']."</td>";
if($ldiscipline->UseSubScore) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_Subscore1."</td>";
}
if($ldiscipline->UseActScore) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['ActivityScore']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_receive</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserRemark</td>";
// [2020-0824-1151-40308]
if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
    $x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['FollowUpRemark']."</td>";
}
if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Award_Punishment_Send_Notice</td>";
}
if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eDiscipline']['FileNumber'] ."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">". $Lang['eDiscipline']['RecordNumber'] ."</td>";
}
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

### reduce sql number
// data storage ary
$schoolYearAry = array();
$semInfoAry = array();
$academicYearIDAry = array();
$actionDueDateAry = array();
$studentProbationAry = array();
$needApprovalAry = array();
$templateInfoAry = array();

// static data ary
$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
### reduce sql number

for($a=0; $a<sizeof($data); $a++)
{
	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName;

	### INSERT INTO DISCIPLINE_MERIT_RECORD (status = pending)
	$dataAry = array();
	$dataAry['RecordDate'] = $data[$a]['RecordDate'];
	$dataAry['RecordInputDate'] = $data[$a]['RecordInputDate'];
	$dataAry['Year'] = $data[$a]['Year'];
	$dataAry['Semester'] = addslashes($data[$a]['Semester']);
	$dataAry['StudentID'] = $data[$a]['StudentID'];
	$dataAry['ItemID'] = $data[$a]['ItemID'];
	$dataAry['ItemText'] = addslashes($data[$a]['ItemText']);
	$dataAry['MeritType'] = $data[$a]['MeritType'];
	$dataAry['Remark'] = addslashes($data[$a]['Remark']);
	// [2020-0824-1151-40308]
    $dataAry['FollowUpRemark'] = addslashes($data[$a]['FollowUpRemark']);
	$dataAry['ProfileMeritType'] = $data[$a]['ProfileMeritType'];
	$dataAry['ProfileMeritCount'] = $data[$a]['ProfileMeritCount'];
	$dataAry['ConductScoreChange'] = $data[$a]['ConductScoreChange'];
	$dataAry['SubScore1Change'] = $data[$a]['SubScoreChange'];
	$dataAry['SubScore2Change'] = $data[$a]['ActScoreChange'];
	$dataAry['Subject'] = $data[$a]['Subject'];
	$dataAry['PICID'] = $data[$a]['PICID'];
	$dataAry['RecordStatus'] = DISCIPLINE_STATUS_PENDING;

    //$school_year = GET_ACADEMIC_YEAR3($dataAry['RecordDate']);
    //$semInfo = getAcademicYearAndYearTermByDate($dataAry['RecordDate']);
    //$dataAry['AcademicYearID'] = $ldiscipline->getAcademicYearIDByYearName($school_year);
    //$dataAry['YearTermID'] = $semInfo[0];
    //$dataAry['ActionDueDate'] = $ldiscipline->retrieveWaivePassDay($data[$a]['RecordDate'], $ldiscipline->MaxSchoolDayToCallForAction);
    if(!isset($schoolYearAry[$dataAry['RecordDate']])) {
        $schoolYearAry[$dataAry['RecordDate']] = GET_ACADEMIC_YEAR3($dataAry['RecordDate']);
    }
    $school_year = $schoolYearAry[$dataAry['RecordDate']];
    if(!isset($semInfoAry[$dataAry['RecordDate']])) {
        $semInfoAry[$dataAry['RecordDate']] = getAcademicYearAndYearTermByDate($dataAry['RecordDate']);
    }
    $semInfo = $semInfoAry[$dataAry['RecordDate']];
    if(!isset($academicYearIDAry[$school_year])) {
        $academicYearIDAry[$school_year] = $ldiscipline->getAcademicYearIDByYearName($school_year);
    }
    $dataAry['AcademicYearID'] = $academicYearIDAry[$school_year];
	$dataAry['YearTermID'] = $semInfo[0];
	if(!isset($actionDueDateAry[$data[$a]['RecordDate']])) {
        $actionDueDateAry[$data[$a]['RecordDate']] = $ldiscipline->retrieveWaivePassDay($data[$a]['RecordDate'], $ldiscipline->MaxSchoolDayToCallForAction);
    }
	$dataAry['ActionDueDate'] = $actionDueDateAry[$data[$a]['RecordDate']];

	if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked'])
	{
		if (!$lnotice->disabled) {
			$thisCatID = $ldiscipline->returnCatIDByItemID($dataAry['ItemID']);
			$apCatInfo = $ldiscipline->getAPCategoryInfoBYCatID($thisCatID);
			$TemplateID = $apCatInfo['TemplateID'];
			$TemplateCategoryID = $TemplateID;

			$dataAry['TemplateID'] = $TemplateCategoryID;
			$dataAry['TemplateOtherInfo'] = "";
		}
	}

	if($dataAry['MeritType'] == 1)
	{
		//$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
        $MeritRecordType = $merit_record_type;
	}
	else
	{
		//$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
        $MeritRecordType = $demerit_record_type;
	}
	for($i=1; $i<sizeof($MeritRecordType)+1; $i++)
	{
		if($dataAry['ProfileMeritType'] == $MeritRecordType[$i][0])
		{
			$MeritTypeName = $MeritRecordType[$i][1];
		}
	}

	if($sys_custom['eDiscipline']['yy3'])
	{
		$overflowMarkInfoAry = $ldiscipline->GetMeritItemTagConductOverflowScoreStudents($dataAry['StudentID'], $dataAry['AcademicYearID'], $dataAry['YearTermID'], $dataAry['ItemID'], $dataAry['MeritType'], abs($dataAry['ConductScoreChange']));
		if($sys_custom['eDiscipline']['yy3']) {
			if(in_array($dataAry['StudentID'],$overflowMarkInfoAry['MeritScoreOverflowStudentID'])){
				$dataAry['OverflowMeritItemScore'] = '1';
			}
			if(in_array($dataAry['StudentID'],$overflowMarkInfoAry['ConductScoreOverflowStudentID'])){
				$dataAry['OverflowConductMark'] = '1';
				$dataAry['ConductScoreAfter'] = $overflowMarkInfoAry['ConductScoreOverflowDetail'][$dataAry['StudentID']]['MarkAfterChange'];
			}
			if(in_array($dataAry['StudentID'],$overflowMarkInfoAry['TagScoreOverflowStudentID'])){
				$dataAry['OverflowTagScore'] = '1';
			}
		}
	}

	if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields'])
	{
		$dataAry['FileNumber'] = $data[$a]['FileNumber'];
		$dataAry['RecordNumber'] = $data[$a]['RecordNumber'];
	}

	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['ClassName']."-".$data[$a]['ClassNumber']."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['StudentName'] ."</td>";
	$x .= "<td class=\"tabletext\">". substr($data[$a]['RecordDate'],0,10) ."</td>";
	if($sys_custom['eDiscipline']['yy3']) {
		$x .= "<td class=\"tabletext\">". $data[$a]['RecordInputDate'] ."</td>";
	}
	$x .= "<td class=\"tabletext\">". $data[$a]['Semester'] ."</td>";
	$x .= "<td class=\"tabletext\">".(($data[$a]['MeritType']==1)?$eDiscipline["AwardRecord"]:$i_Discipline_System_Award_Punishment_Punishments)."</td>";
	$x .= "<td class=\"tabletext\">". intranet_htmlspecialchars($data[$a]['ItemText']) ."</td>";
	if ($ldiscipline->use_subject) {
		$x .= "<td class=\"tabletext\">". $data[$a]['Subject'] ."</td>";
	}
	if($sys_custom['eDiscipline']['yy3'] && ($dataAry['OverflowMeritItemScore']=='1' || $dataAry['OverflowConductMark']=='1' || $dataAry['OverflowTagScore']=='1') && $data[$a]['ConductScoreChange']==0){
		$x .= "<td class=\"tabletext\">". $ldiscipline_ui->displayMaskedMark($data[$a]['ConductScoreChange'])."</td>";
	}
	else{
		$x .= "<td class=\"tabletext\">". $data[$a]['ConductScoreChange'] ."</td>";
	}
	if($ldiscipline->UseSubScore) {
		$x .= "<td class=\"tabletext\">". $data[$a]['SubScoreChange'] ."</td>";
	}
	if($ldiscipline->UseActScore) {
		$x .= "<td class=\"tabletext\">". $data[$a]['ActScoreChange'] ."</td>";
	}
	$receive = $ldiscipline->returnDeMeritStringWithNumber($data[$a]['ProfileMeritCount'], $MeritTypeName);
	$x .= "<td class=\"tabletext\">". $receive."</td>";
	$x .= "<td class=\"tabletext\">". $data[$a]['Teacher'] ."</td>";
	//$x .= "<td class=\"tabletext\">". $MeritTypeName ."</td>";
    //$x .= "<td class=\"tabletext\">". $data[$a]['Remark'] ."</td>";
	$x .= "<td class=\"tabletext\">". nl2br($data[$a]['Remark']) ."</td>";
	// [2020-0824-1151-40308]
    if($sys_custom['eDiscipline']['APFollowUpRemarks']){
        $x .= "<td class=\"tabletext\">". nl2br($data[$a]['FollowUpRemark']) ."</td>";
    }
	if($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) {
		$x .= "<td class=\"tabletext\">". $data[$a]['SendNotice'] ."</td>";
    }
	if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {
		$x .= "<td class=\"tabletext\">". $data[$a]['FileNumber'] ."</td>";
		$x .= "<td class=\"tabletext\">". $data[$a]['RecordNumber'] ."</td>";
	}
	$x .= "</tr>";

	$FromCaseRecord = 0;
	$MeritRecordID = $ldiscipline->INSERT_MERIT_RECORD($dataAry, $FromCaseRecord);

	if($MeritRecordID)
	{
		$import_success++;

		$MeritRecordIDAry[] = $MeritRecordID;

		# Detention
		if ($detentionFlag == "YES")
		{
			for ($i=0; $i<sizeof($DetentionArr); $i=$i+3) {
				// array sequence: StudentID, DetentionID, Remark
				if ($StudentID == $DetentionArr[$i]) {
					$detentionDataArr = array();
					$detentionDataArr['StudentID'] = "'".$DetentionArr[$i]."'";
					if ($DetentionArr[$i+1] == "") {
					    $DetentionArr[$i+1] = "NULL";
                    }
					$detentionDataArr['DetentionID'] = $DetentionArr[$i+1];
					$detentionDataArr['Remark'] = "'".stripslashes($DetentionArr[$i+2])."'";
					$detentionDataArr['DemeritID'] = "'".$MeritRecordID."'";
					$detentionDataArr['RequestedBy'] = "'".$UserID."'";
					if ($DetentionArr[$i+1] != "NULL") {
						$detentionDataArr['ArrangedBy'] = "'".$UserID."'";
						$detentionDataArr['ArrangedDate'] = 'NOW()';
					}
					$detStuRecordID = $ldiscipline->insertDetention($detentionDataArr);
				}
			}
		}

		// [2015-0611-1642-26164] CWC Probation Scheme
		if($sys_custom['eDiscipline']['CSCProbation'])
		{
			$approve_probation = false;
			if($dataAry['MeritType'] == -1 && !$studentProbationAry[$dataAry['StudentID']])
			{
				// Get probation details
				list($approve_probation, $probation_records) = $ldiscipline->CHECK_STUDENT_CWC_PROBATION_RECORD($dataAry['StudentID'], $dataAry['YearTermID'], $MeritRecordID);
			}

			// Approve current ap record
			$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1, 1, 1);

			// Approve existing probation records if current record is punishment
			if($dataAry['MeritType'] == -1 && $approve_probation && !$studentProbationAry[$dataAry['StudentID']]){
				foreach((array)$probation_records as $ap_record_id => $ap_record_info){
					$ldiscipline->APPROVE_MERIT_RECORD($ap_record_id, 1, 1, 1);
				}
				$studentProbationAry[$dataAry['StudentID']] = true;
			}
		}
		// Normal case
		else if($ldiscipline->AutoApproveAPRecord) {
			# Check need approval or not
			//$need_approval = $ldiscipline->CHECK_APPROVAL_REQUIRED($dataAry['ProfileMeritType'], $dataAry['ProfileMeritCount']);
            if(!isset($needApprovalAry[$dataAry['ProfileMeritType']])) {
                $needApprovalAry[$dataAry['ProfileMeritType']] = array();
            }
            if(!isset($needApprovalAry[$dataAry['ProfileMeritType']][$dataAry['ProfileMeritCount']])) {
                $needApprovalAry[$dataAry['ProfileMeritType']][$dataAry['ProfileMeritCount']] = $ldiscipline->CHECK_APPROVAL_REQUIRED($dataAry['ProfileMeritType'], $dataAry['ProfileMeritCount']);
            }
            $need_approval = $needApprovalAry[$dataAry['ProfileMeritType']][$dataAry['ProfileMeritCount']];

			// [2017-0629-1459-19235] Copy logic from new3_update.php
			$can_approve = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($MeritRecordID);
			if($need_approval && ($can_approve || $ldiscipline->IS_ADMIN_USER($_SESSION['UserID']))) {
				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1);
			}
			else if(!$need_approval && ($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))) {
				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1);
			}
			/*
			if($need_approval)
			{
				# can do nothing
			}
			else
			{
				# set APPROVE
				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1);
			}
			*/
		}

		# Send Notice
		if($data[$a]['SendNotice'] == "Y")
		{
			    $datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);

				//$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($datainfo[0]['TemplateID']);
                if(!isset($templateInfoAry[$datainfo[0]['TemplateID']])) {
                    $templateInfoAry[$datainfo[0]['TemplateID']] = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($datainfo[0]['TemplateID']);
                }
                $template_info = $templateInfoAry[$datainfo[0]['TemplateID']];

				if(sizeof($template_info))
				{
					//echo 1;
					$template_category = $template_info[0]['CategoryID'];

					$SpecialData = array();
					$SpecialData['MeritRecordID'] = $MeritRecordID;
					$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($template_category, $data[$a]['StudentID'], $SpecialData, $datainfo[0]['TemplateOtherInfo']);
					$UserName = $ldiscipline->getUserNameByID($UserID);

					$Module = $ldiscipline->Module;
					$NoticeNumber = time();
					$TemplateID = $datainfo[0]['TemplateID'];
					$Variable = $template_data;
					$IssueUserID = $UserID;
					$IssueUserName = $UserName[0];
					$TargetRecipientType = "U";
					$RecipientID = array($data[$a]['StudentID']);
					$RecordType = NOTICE_TO_SOME_STUDENTS;

					if (!$lnotice->disabled)
					{
						include_once($PATH_WRT_ROOT."includes/libucc.php");
						$lc = new libucc();
						$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

                        // [2020-1009-1753-46096] delay notice start date & end date
                        if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                        {
                            $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                            $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                            $lc->setDateStart($DateStart);

                            if ($lc->defaultDisNumDays > 0) {
                                $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                                $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                                $lc->setDateEnd($DateEnd);
                            }
                        }

						$NoticeID = $lc->sendNotice();
					}

					if($NoticeID != -1)
					{
						# update [DISCIPLINE_MERIT_RECORD] field NoticeID
// 						$updateDataAry = array();
// 						$updateDataAry['NoticeID'] = $NoticeID;
// 						$ldiscipline->UPDATE_MERIT_RECORD($updateDataAry, $MeritRecordID);

						$sql = "UPDATE DISCIPLINE_MERIT_RECORD SET ";
                            $sql .= "DateModified = now(), ";
                            $sql .= "NoticeID = '$NoticeID', ";
                            $sql .= "ModifiedBy = '$UserID' ";
                        $sql .= "WHERE RecordID = $MeritRecordID";

                        $this->db_db_query($sql);
					}
				}
			//}
		}
	}
}

if($import_success > 0)
{
	$xmsg2= "<font color=green>".($import_success)." ".$iDiscipline['Reord_Import_Successfully']."</font>";
	
	# Delete the temp data in temp table
	$sql = "DELETE FROM temp_profile_student_merit_import WHERE UserID = ".$_SESSION["UserID"];
	$ldiscipline->db_db_query($sql) or die(mysql_error());
}
else
{
	$xmsg = "import_failed";
}

$x .= "</table>";

$linterface = new interface_html();
$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='index.php'")."&nbsp;".$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='import.php'");

$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
$STEPS_OBJ[] = array($i_general_complete, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
    <td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
</tr>
<tr>
    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
    <td><?= $x ?></td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			    <?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>	
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>