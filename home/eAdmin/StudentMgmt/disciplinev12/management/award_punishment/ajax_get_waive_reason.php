<?php
// Using By : 

########## Change Log ###############
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				create for waive / redeem reason
#
#####################################

if (!function_exists('magicQuotes_awStripslashes') && get_magic_quotes_gpc()) {
    function magicQuotes_awStripslashes(&$value, $key) {$value = stripslashes($value);}
    $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
    array_walk_recursive($gpc, "magicQuotes_awStripslashes");
}

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$data = array();

$ldiscipline = new libdisciplinev12();
$reasonList = $ldiscipline->Get_Preset_Reason_List('waive');

$reasonListCount = sizeof($reasonList);
for ($i=0; $i<$reasonListCount; $i++)
{
	$data[] = array('value' => handle_chars($reasonList[$i][1]), 'title' => handle_chars($reasonList[$i][1]));
}

function handle_chars($str){
	$x = '';
	if($str != ''){
		$x = str_replace("\\", "\\\\", $str);
	}
	return $x;
}

header('Content-Type: application/json; charset=utf-8');

$jsonObj = new JSON_obj();
$returnJsonString = $jsonObj->encode($data);

echo $returnJsonString;

intranet_closedb();
?>