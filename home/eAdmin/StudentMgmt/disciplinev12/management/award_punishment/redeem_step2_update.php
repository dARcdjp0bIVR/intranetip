<?php
/* 
 *	Modification Log:
 *  2019-05-21 (Bill)   [2019-0517-1540-40235]
 *      - fixed php error message
 *      - prevent IntegerSafe() - change $MeritID to $MeritIDStr + $DemeritID to $DemeritIDStr
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Waive");

# Back Parameters
$back_para = "pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&pic=$pic";

# Initialization
$MeritIDArr = unserialize(rawurldecode($MeritIDStr));
$DemeritIDArr = unserialize(rawurldecode($DemeritIDStr));
$allRecordIDArr = array_merge((array)$MeritIDArr, (array)$DemeritIDArr);

$success = $ldiscipline->redeemMeritRecords($allRecordIDArr);

if ($success)
{
	header("Location: index.php?msg=redeem_success&$back_para");
}
else
{
	header("Location: index.php?msg=redeem_failed&$back_para");
}

intranet_closedb();
?>