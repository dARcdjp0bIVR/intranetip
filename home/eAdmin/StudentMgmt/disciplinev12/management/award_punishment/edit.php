<?php
# using: YW

################ Change Log Start ######################################
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose)
#
#   Date    :   2019-05-01  Bill
#               prevent SQL Injection + Cross-site Scripting + Redirect Problem
#
#	Date	:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#				support Study Score Limit Checking ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date	:	2017-06-09	(Bill)	[2017-0601-1648-45235]
#				convert "<br/>" in remark to "\n" for edit
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				support Activity Score selection or display
#
#	Date	:	2016-07-11	Bill
#				change split() to explode(), for PHP 5.4
#
#	Date	:	2016-06-13 (Anna)	[2016-0526-1139-05206]
#				added creator's information and change CSS of table
#
#	Date	:	2016-04-08 (Bill)	[2016-0224-1423-31073]
#				added prefix style to teacher name of deleted account
#
#	Date	:	2016-03-02 (Bill)	[2016-0224-1423-31073]
#				return PIC name even teacher account is inactive or removed
#
#	Date	:	2015-06-17 (Carlos) - Modified js checkOverflowConductMark(), added RecordID to be excluded.
#	Date	:	2013-11-18 (YatWoon)
#				Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-06-13 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] 
#				- disable MeritNumber and MeritType selection for non admin users
#				- display warning if score overflow of merit item limit, related tags limit and conduct mark limit
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 - $sys_custom['eDiscipline']['yy3'])
#				display ConductMarkRemark
#
#	Date	:	2013-05-09 (YatWoon)
#	Detail	:	update check_form(), add checking not allow input future record
#
#	Date	:	2011-04-06 (Henry Chow)
#				if merit/demerit is null, no need to change "action"
#
#	Date	:	2010-08-18	YatWoon
#				display student internal remark for client reference
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#	Date:	2010-04-14 YatWoon
#			check setting $ldiscipline->OnlyAdminSelectPIC, only admin can select PIC
#
#	Date:	2010-02-08	YatWoon
#			Add checking to check $ldiscipline->Hidden_ConductMark in javascript
#
#	Date:	2010-01-28	YatWoon
#			Add checking to check $ldiscipline->UseSubScore in javascript
#
################ Change Log End ######################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$RecordID = IntegerSafe($RecordID);

$SchoolYear = IntegerSafe($SchoolYear);
$SchoolYear2 = IntegerSafe($SchoolYear2);
if($semester != '' && $semester != 'WholeYear') {
    $semester = IntegerSafe($semester);
}
if($semester2 != '' && $semester2 != 'WholeYear') {
    $semester2 = IntegerSafe($semester2);
}
$targetClass = cleanCrossSiteScriptingCode($targetClass);
$targetClass2 = cleanCrossSiteScriptingCode($targetClass2);

$s = cleanCrossSiteScriptingCode($s);
$clickID = IntegerSafe($clickID);

$MeritType = IntegerSafe($MeritType);
$MeritType2 = IntegerSafe($MeritType2);
$waitApproval = IntegerSafe($waitApproval);
$waitApproval2 = IntegerSafe($waitApproval2);
$approved = IntegerSafe($approved);
$approved2 = IntegerSafe($approved2);
$rejected = IntegerSafe($rejected);
$rejected2 = IntegerSafe($rejected2);
$released = IntegerSafe($released);
$released2 = IntegerSafe($released2);
$waived = IntegerSafe($waived);
$waived2 = IntegerSafe($waived2);
$fromPPC = IntegerSafe($fromPPC);
$passedActionDueDate = IntegerSafe($passedActionDueDate);

$back_page = cleanCrossSiteScriptingCode($back_page);
if($back_page != 'index.php' && $back_page != 'detail.php') {
    $back_page = 'index.php';
}
### Handle SQL Injection + XSS [END]

if(empty($RecordID))	header("Location: index.php");
if(is_array($RecordID))	$RecordID = $RecordID[0];

$ldiscipline = new libdisciplinev12();

# Check access right
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || (($ldiscipline->isMeritRecordPIC($RecordID) || $ldiscipline->isMeritRecordOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"))))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT("","$back_page?field=$field&order=$order&pageNo=$pageNo&numPerPage=$page_size&MeritType=$MeritType&s=$s&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&waived=$waived&approved=$approved&waitApproval=$waitApproval&released=$released&rejected=$rejected");
}

$linterface = new interface_html();

# Menu highlight
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Navigation Bar
$PAGE_NAVIGATION[] = array($eDiscipline["EditRecord"], "");

# Retrieve data
$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
$data = $datainfo[0];
$student = array($data['StudentID']);
$ItemID = $data['ItemID'];
$CatID = $ldiscipline->returnCatIDByItemID($ItemID);

# check user can access/edit this item or not
$AccessItem = 0;
if($ldiscipline->IS_ADMIN_USER($UserID))
{
	$AccessItem = 1;
}
else
{
	$ava_item = $ldiscipline->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
	if($ava_item[$CatID]) {
		$AccessItem = in_array($ItemID, $ava_item[$CatID]);
	}
}

$RecordType = $data['MeritType'];
$MeritNum = $data['ProfileMeritCount'];
$ConductScore = abs($data['ConductScoreChange']);
$StudyScore = abs($data['SubScore1Change']);
$ActivityScore = abs($data['SubScore2Change']);
$ProfileMeritType = $data['ProfileMeritType'];
$isAcc = $data['fromConductRecords'];
$RecordDate = $data['RecordDate'];
$RecordInputDate = $data['RecordInputDate'];
$ActionDueDate = $data['ActionDueDate'];
//$semester = $data['Semester'];
$thisStatus = $data['RecordStatus'];

$isWaived = $data['RecordStatus']==DISCIPLINE_STATUS_WAIVED ? 1 : 0;

// [2016-0526-1139-05206] Get Creator Info
$CreatorID = $data['CreatedBy'];
if($CreatorID > 0)
{
	$userObj = new libuser($CreatorID);
	$CreatorEnglishName = $userObj->EnglishName;
	$CreatorChineseName = $userObj->ChineseName;
	$nameDisplay = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
}
$CreateDate = $data['DateInput'];

# build html part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
foreach($student as $k=>$StudentID)
{
	$lu = new libuser($StudentID);
	$student_name = $lu->UserNameLang();
	$class_name = $lu->ClassName;
	$class_no = $lu->ClassNumber;
	
	$selected_student_table .= "<tr><td width='50%' valign='top'><a href='javascript:viewCurrentRecord($StudentID)' class='tablelink'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle'>";
	$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
	$selected_student_table .= "</a></td></tr>";
}
$selected_student_table .= "</table>";

# Last PIC selection
$PIC = array($data['PICID']);
if (is_array($PIC) && sizeof($PIC)>0)
{
	$list = implode(",", $PIC);
	// $ldiscipline->getPICNameList($list);
	$namefield = getNameFieldWithLoginByLang();
	
	// [2016-0224-1423-31073]
	// INTRANET_USER
	//$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($list) AND RecordStatus = 1 ORDER BY $namefield";
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN ($list) ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
				
	// INTRANET_ARCHIVE_USER
	//$sql = "SELECT UserID, $namefield FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($list) ORDER BY $namefield";
	$sql = "SELECT UserID, CONCAT('<span class=\"tabletextrequire\">*</span>', $namefield) FROM INTRANET_ARCHIVE_USER WHERE RecordType = 1 AND UserID IN ($list) ORDER BY $namefield";
	$array_ArchivePIC = $ldiscipline->returnArray($sql);
	
	// Merge user and archive user
	$array_PIC = array_merge($array_PIC, $array_ArchivePIC);
	
	if(sizeof($array_PIC)==0) $picName = $data['PICID'];
}

# Get Category selection
$merit_cats = $ldiscipline->returnMeritItemCategoryByType(1);
$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);
$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();
$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);

if($isAcc || $isWaived)
{
	$RecordDateDisplay = $RecordDate;
	if($isAcc || $isWaived) {# user can modify PIC even record comes from GM
		$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");
		$PICDisplay = "<td>". $PIC_selected ."</td>";
	} else {
		$PICAry = array();
		
		foreach($array_PIC as $k=>$d)
			$PICAry[] = $d[1];
		$PICStr = implode("<br>",$PICAry);
		$PICStr = $PICStr ? $PICStr : "---";
		$PICDisplay = "<td>". $PICStr ."</td>";
	}	
	//$subjectDisplay = $data['Subject'] ? $ldiscipline->getCorrectSubjectNameLangBySubjectName($data['Subject']) : "---";
	$subjectDisplay = $data['SubjectID'] ? $ldiscipline->getSubjectNameBySubjectID($data['SubjectID']) : "---";
	
	$CatInfo= $ldiscipline->returnMeritItemCategoryByID($CatID);
	$CatDisplay = $CatInfo['CategoryName'];
	$ItemDisplay = $data['ItemText'];
	/*
	$itemAry = $ldiscipline->getAwardPunishItemByItemID($ItemID);
	$ItemDisplay = $itemAry[0]." - ".$itemAry[1];
	*/
				
// 	if($isAcc)
// 	{
		//$CatDisplay = "<select name='CatID' onChange='changeCat(this.value);'></select>";
// 		$ItemDisplay = "<SELECT name='ItemID'>";
// 		$ItemDisplay .= "<OPTION value='0' selected>-- ". $button_select ." --</OPTION></SELECT>";
	
		$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']);
		$MeritDisplay = $data['ProfileMeritCount'] ." ". $thisMeritType;
		$conduct_markDisplay = $data['ConductScoreChange'];
		$study_markDisplay = $data['SubScore1Change'];
		$activity_markDisplay = $data['SubScore2Change'];
// 	}
// 	else
// 	{
// 		$CatInfo= $ldiscipline->returnMeritItemCategoryByID($CatID);
// 		$CatDisplay = $CatInfo['CategoryName'];
// 		$ItemDisplay = $data['ItemText'];
// 	}
}
else
{
	/*
	$semester_mode = getSemesterMode();
	if($semester_mode==1)
	{
		$select_sem = getSelectSemester("name=semester", $semester);	
		$select_sem_html = "
			<tr>
				<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">{$i_SettingsSemester}</td>
				<td valign=\"top\">{$select_sem}</td>
			</tr>
		";
	}
	else
	{
		$select_sem_html = "<input type='hidden' name='semester' value=''>";
	}
	*/
	$select_sem_html = "<input type='hidden' name='semester' id='semester' value=''>";
	
	$PIC_selected = $linterface->GET_SELECTION_BOX($array_PIC, "name='PIC[]' ID='PIC[]' class='select_studentlist' size='6' multiple='multiple'", "");
	
	$PICDisplay = "<td>". $PIC_selected ."</td>";
	//$PICDisplay .= "<td valign='bottom'>". $linterface->GET_BTN($i_Discipline_Select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',9)") ."<br>". $linterface->GET_BTN($i_Discipline_Remove, "button","checkOptionRemove(document.form1.elements['PIC[]'])") ."</td>";
	
	# Subject Selection
	if ($ldiscipline->use_subject)
	{
		$lteaching = new libteaching();
		$subjectList = $lteaching->returnSubjectList();
	
	
	    $select_subject = "<SELECT name='SubjectID' name='SubjectID'>\n";
	    $select_subject .= "<OPTION value=''>-- $i_notapplicable --</OPTION>\n";
	    foreach ($subjectList AS $k=>$d)
	    {
		    $selected = ($data['SubjectID'] == $d[0]) ? "selected" : "";
	        $select_subject .= "<OPTION value='". $d[0] ."' $selected>". $d[1] ."</OPTION>\n";
	    }
	    $select_subject .= "</SELECT>\n";
	}
	$subjectDisplay = $select_subject;

	if($AccessItem)
	{
		# Category
		$CatDisplay = "<select name='CatID' id='CatID' onChange='changeCat(this.value);'></select>";
		
		# Category Item
		$ItemDisplay = "<SELECT onChange='changeItem(this.value)' name='ItemID' id='ItemID'>";
		$ItemDisplay .= "<OPTION value='0' selected>-- ". $button_select ." --</OPTION></SELECT>";
		
		# Merit Count selection
		$select_merit_num = "<SELECT name='MeritNum' id='MeritNum' ".($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()?"disabled":"").">\n";
		$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
		for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval)
		{
		     $select_merit_num .= "<OPTION value='$i' ". ($MeritNum==$i?"selected":"") .">$i</OPTION>\n";
		}
		$select_merit_num .= "</SELECT>\n";
		$MeritDisplay = $select_merit_num ." <select name='MeritType' id='MeritType' ".($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()?"disabled":"")."></select>";
		
		# Conduct Mark selection
		$select_conduct_mark = "<SELECT name='ConductScore' id='ConductScore'";
		if($sys_custom['eDiscipline']['yy3']){
			$select_conduct_mark .= " onchange=\"checkOverflowConductMark();\" ";
		}
		$select_conduct_mark .= ">\n";
		/*
		for ($i=0; $i<=$ldiscipline->ConductMarkIncrement_MAX; $i++)
		{
		     $select_conduct_mark .= "<OPTION value='$i' ". ($ConductScore==$i?"selected":"") .">$i</OPTION>\n";
		}
		*/
		$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
		$i = 0;
		$loopConductMark = true;
		while($loopConductMark) {
			$select_conduct_mark .= "<OPTION value=$i>$i</OPTION>\n";
			//$i+=$conductMarkInterval;
			$i = (float)(string)($i + $conductMarkInterval);
			if(floatcmp($i ,">" ,$ldiscipline->ConductMarkIncrement_MAX)) {
				$loopConductMark = false;
			}
		}
		$select_conduct_mark .= "</SELECT>\n";
		$conduct_markDisplay = $select_conduct_mark . " " . $ldiscipline->ConductMarkRemark;
	
		# Study Mark selection
		$select_study_mark = "<SELECT name='StudyScore' id='StudyScore'";
		// [2016-1125-0939-00240]
		if($sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
			$select_study_mark .= " onchange='checkOverflowStudyScore();' ";
		}
		$select_study_mark.= ">\n";
		for ($i=0; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++)
		{
		     $select_study_mark .= "<OPTION value='$i' ". ($StudyScore==$i?"selected":"") .">$i</OPTION>\n";
		}
		$select_study_mark .= "</SELECT>\n";
		$study_markDisplay = $select_study_mark;
	
		# Activity Mark selection
		$select_activty_mark = "<SELECT name='ActivityScore' id='ActivityScore'>\n";
		for ($i=0; $i<=$ldiscipline->ActScoreIncrement_MAX; $i++)
		{
		     $select_activty_mark .= "<OPTION value='$i' ". ($ActivityScore==$i?"selected":"") .">$i</OPTION>\n";
		}
		$select_activty_mark .= "</SELECT>\n";
		$activity_markDisplay = $select_activty_mark;
	
		$NumOfMaxRecordDays = $ldiscipline->MaxRecordDayBeforeAllow;
		if($NumOfMaxRecordDays > 0) {
			$EnableMaxRecordDaysChecking = 1;
			$DateMinLimit = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-$NumOfMaxRecordDays, date("Y")));
			$DateMaxLimit = date("Y-m-d");
		} else {
			$EnableMaxRecordDaysChecking = 0;
		}
	}
	else
	{
		# Category
		$CatInfo= $ldiscipline->returnMeritItemCategoryByID($CatID);
		$CatDisplay = $CatInfo['CategoryName'];
		
		# Category Item
		/*
		$itemAry = $ldiscipline->getAwardPunishItemByItemID($ItemID);
		$ItemText = $itemAry[0]." - ".$itemAry[1];
		$ItemDisplay = $ItemText . "<input type='hidden' name='ItemID' value='". $ItemID ."'>";
		*/
		$ItemDisplay = $data['ItemText'];
		
		$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']);
		$MeritDisplay = $ldiscipline->returnDeMeritStringWithNumber($data['ProfileMeritCount'], $thisMeritType);
		
		$conduct_markDisplay = $data['ConductScoreChange'];
		$study_markDisplay = $data['SubScore1Change'];
		$activity_markDisplay = $data['SubScore2Change'];
	}
	
	$RecordDateDisplay = $linterface->GET_DATE_PICKER("RecordDate", ($RecordDate ? $RecordDate : date('Y-m-d')));
	//$RecordDateDisplay = $linterface->GET_DATE_FIELD("theRecordDate", "form1", "RecordDate", ($RecordDate ? $RecordDate : date('Y-m-d')), 1);
	
	if($sys_custom['eDiscipline']['yy3'])
	{
		$RecordInputDateDisplay = $linterface->GET_DATE_PICKER("RecordInputDate", ($RecordInputDate ? $RecordInputDate : date('Y-m-d')));
	}
	
}

if($data['Attachment']=='') {
	$sessionTime = session_id()."-".(time());
} else {
	$sessionTime = $data['Attachment'];	
}

$path = "$file_path/file/disciplinev12/award_punishment/".$sessionTime;
$attachment_list = $ldiscipline->getAttachmentArray($path, $data['Attachment']);
$attSelect = $linterface->GET_SELECTION_BOX($attachment_list,"id=\"Attachment[]\" name=\"Attachment[]\"  multiple='multiple'","");

$attachmentHTML .= "<table class=\"inside_form_table\" width=\"20%\" border=\"0\" cellspacing=\"0\" cellpadding=\"5\">
				<tr><td>".$attSelect."
				</td>
				<td valign=\"bottom\">".$linterface->GET_BTN($button_add, "button", "newWindow('attach.php?FolderLocation=".$sessionTime."&fieldname=Attachment[]', 9)")."
				<br/>".$linterface->GET_BTN($button_remove_selected, "button", "ajaxRemoveAttachment('Attachment[]', 'layer01')")."<div id=\"layer01\"></div>
				</td>
				</tr>
				</table>";	

				
if($duplicate==1) {
	$duplicate_msg = "<td>".$linterface->GET_SYS_MSG("",$Lang['eDiscipline']['ItemAlreadyAdded'])."</td>";
}

# generate semester checking js
if($semester_mode==2)
{
	$sem_data = getSemesters();
	$sem_js.="var sem_js = new Array();\n";
	for($i=0;$i<sizeof($sem_data);$i++)
	{
		list($sem_name, $sem_type, $sem_start, $sem_end) = explode("::", $sem_data[$i]);
		$sem_js.="sem_js.push(new Array('$sem_start','$sem_end'));\n";
	}
}

# Start layout
$linterface->LAYOUT_START();

# build merit/demerit approval setting checking array
$approval_rules = $ldiscipline->RETRIEVE_APPROVAL_NUM();
$approval_rules_js.="var approval_rules_js = new Array();\n";
foreach($approval_rules as $mtype => $num)
{
	$approval_rules_js .= "approval_rules_js[". $mtype ."]='". $num ."';\n";
}
?>

<SCRIPT LANGUAGE=Javascript>
<!--

<?=$sem_js?>
<?=$approval_rules_js?>

var ClickID = '';

var callback_ajaxRemoveAttachment = {
	success: function ( o )
    {
		var fname = document.getElementById('fieldname').value;
	    var tmp_str = o.responseText;
	    checkOptionRemove(document.form1.elements[fname]);
	    //document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function ajaxRemoveAttachment(fname, layername)
{
	ClickID = layername;
	obj = document.form1;
	obj.fieldname.value = fname;
	obj.task.value = "remove";
	obj.FolderLocation.value = "<?=$sessionTime?>";
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_attachment.php";
	var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_ajaxRemoveAttachment);
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
	var posleft = getPosition(document.getElementById(ClickID),'offsetLeft') +10;
	var postop = getPosition(document.getElementById(ClickID),'offsetTop') +10;
	var divwidth = document.getElementById('ref_list').clientWidth;
	
	if(posleft+divwidth > document.body.clientWidth)
		document.getElementById('ref_list').style.right= "5px";
	else
		document.getElementById('ref_list').style.left= posleft+"px";
		
  	document.getElementById('ref_list').style.top= postop+"px";
  	document.getElementById('ref_list').style.visibility='visible';
}


// Generate Merit Category
var merit_cat_select = new Array();
merit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<?	foreach($merit_cats as $k=>$d)	{ ?>
		merit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Demerit Category
var demerit_cat_select = new Array();
demerit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<?	foreach($demerit_cats as $k=>$d)	{ ?>
		demerit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Merit Type
var merit_record_type = new Array();
<?	if(!empty($merit_record_type))
	{
	foreach($merit_record_type as $k=>$d)	{ ?>
		merit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<?	} 
	}?>

// Generate Demerit Type
var demerit_record_type = new Array();
<?	if(!empty($demerit_record_type))
	{
	foreach($demerit_record_type as $k=>$d)	{ ?>
		demerit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<?	} 
	}	?>

function viewCurrentRecord(id)
{
	newWindow('viewcurrent.php?StudentID='+id, 8);
}

function changeMeritType(recordtype) 
{
	var obj = document.form1;
	var cat_select = document.getElementById('CatID');
	var item_select = document.getElementById('ItemID');
	<? if(!$isAcc) {?>
	var meritType = document.getElementById('MeritType');
	<? } ?>
	
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectRecordItem2"]?> --",0);
	
	if(recordtype==1)		// Award
	{
		cat_select.options.length = merit_cat_select.length;
		for(i=0;i<merit_cat_select.length;i++)
		{
			cat_select.options[i]=new Option(merit_cat_select[i].text,merit_cat_select[i].value);	
			<? if($CatID) {?>
			if(merit_cat_select[i].value == <?=$CatID?>) 	cat_select.selectedIndex = i;
			<? } ?>
		}
			
		<? if(!$isAcc) {?>
			meritType.options.length = merit_record_type.length;
			meritType.options[0]=new Option("--", "-999");	
			for(i=1;i<merit_record_type.length;i++)
			{
				meritType.options[i]=new Option(merit_record_type[i].text,merit_record_type[i].value);	
				if(merit_record_type[i].value == <?=$ProfileMeritType?>) 		meritType.selectedIndex = i;
			}
				
			<? if(!$ldiscipline->Hidden_ConductMark) {?>
			document.getElementById('increment_de').innerHTML = "<?=$Lang['eDiscipline']['Gain']?>";
			<? } ?>
			<? if($ldiscipline->UseSubScore) { ?>
			document.getElementById('increment_de2').innerHTML = "<?=$Lang['eDiscipline']['Gain']?>";
			<? } ?>
			<? if($ldiscipline->UseActScore) { ?>
			document.getElementById('increment_de3').innerHTML = "<?=$Lang['eDiscipline']['Gain']?>";
			<? } ?>
			document.getElementById('MeritNum').selectedIndex = <?=$MeritNum?>;
			<? if(!$ldiscipline->Hidden_ConductMark) {?>
			document.getElementById('ConductScore').selectedIndex = <?=$ConductScore?>;
			<? } ?>
			<? if($ldiscipline->UseSubScore) {?>
			document.getElementById('StudyScore').selectedIndex = <?=$StudyScore?>;
			<? } ?>
			<? if($ldiscipline->UseActScore) {?>
			document.getElementById('ActivityScore').selectedIndex = <?=$ActivityScore?>;
			<? } ?>
		<? } ?>
	}
	else								// Punishment
	{
		cat_select.options.length = demerit_cat_select.length;
		
		for(i=0;i<demerit_cat_select.length;i++)
		{
			cat_select.options[i]=new Option(demerit_cat_select[i].text,demerit_cat_select[i].value);
			<? if($CatID) {?>
			if(demerit_cat_select[i].value == <?=$CatID?>) 	cat_select.selectedIndex = i;
			<? } ?>
		}
		<? if(!$isAcc) {?>
			meritType.options.length = demerit_record_type.length;
			meritType.options[0]=new Option("--", "-999");	
			for(i=1;i<demerit_record_type.length;i++)
			{
				meritType.options[i]=new Option(demerit_record_type[i].text,demerit_record_type[i].value);	
				if(demerit_record_type[i].value == <?=$ProfileMeritType?>) 		meritType.selectedIndex = i;
			}
				
			<? if(!$ldiscipline->Hidden_ConductMark) {?>
			document.getElementById('increment_de').innerHTML = "<?=$Lang['eDiscipline']['Deduct']?>";
			<? } ?>
			<? if($ldiscipline->UseSubScore) { ?>
			document.getElementById('increment_de2').innerHTML = "<?=$Lang['eDiscipline']['Deduct']?>";
			<? } ?>
			<? if($ldiscipline->UseActScore) { ?>
			document.getElementById('increment_de3').innerHTML = "<?=$Lang['eDiscipline']['Deduct']?>";
			<? } ?>
			document.getElementById('MeritNum').selectedIndex = <?=$MeritNum?>;
			<? if(!$ldiscipline->Hidden_ConductMark) {?>
			document.getElementById('ConductScore').selectedIndex = <?=$ConductScore?>;
			<? } ?>
			<? if($ldiscipline->UseSubScore) {?>
			document.getElementById('StudyScore').selectedIndex = <?=$StudyScore?>;
			<? } ?>
			<? if($ldiscipline->UseActScore) {?>
			document.getElementById('ActivityScore').selectedIndex = <?=$ActivityScore?>;
			<? } ?>
		<? } ?>
	}
}

function changeCat(value)
{
	var item_select = document.getElementById('ItemID');
	//var selectedCatID = document.getElementById('CatID').value;
	var selectedCatID = value;
	while (item_select.options.length > 0)
	{
	    item_select.options[0] = null;
	}
	
	item_select.options[0] = new Option("-- <?=$eDiscipline["SelectRecordItem2"]?> --",0);
	
	if (selectedCatID == '')
	{
	<?
		$curr_cat_id = "";
		$pos = 1;
		for ($i=0; $i<sizeof($items); $i++)
		{
	     list($r_catID, $r_itemID, $r_itemName) = $items[$i];
	     $r_itemName = intranet_undo_htmlspecialchars($r_itemName);
	     $r_itemName = str_replace('"', '\"', $r_itemName);
	     $r_itemName = str_replace("'", "\'", $r_itemName);
	     if ($r_catID != $curr_cat_id)
	     {
	         $pos = 1;
	?>
     }
	else if (selectedCatID == "<?=$r_catID?>")
	{
	     <?
	         $curr_cat_id = $r_catID;
	     }
	     ?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
			item_select.selectedIndex = <?=$pos-1?>;
		<? } ?>
		<?
	}
	?>
     }
}

function changeItem(value)
{
	var i;
	var meritType = document.getElementById('MeritType');
	var meritNum = document.getElementById('MeritNum');
	<? if(!$ldiscipline->Hidden_ConductMark) {?>
	var conductScore = document.getElementById('ConductScore');
	<? } ?>
	<? if ($ldiscipline->UseSubScore) { ?>
		var subScore1 = document.getElementById('StudyScore');
	<? } ?>
	<? if ($ldiscipline->UseSubScore) { ?>
		var actScore1 = document.getElementById('ActivityScore');
	<? } ?>
	
	<?
	for ($i=0; $i<sizeof($items); $i++)
	{
     list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
     $r_subscore2 = $items[$i]["SubScore2"];
     $r_MaxGainDeductSubScore1 = $items[$i]['MaxGainDeductSubScore1'];
     ?>
     if (value=="<?=$r_itemID?>")
     {
	     
		if(document.form1.temp_flag.value==1)
		{
			check_meritType = <?=$r_meritType?>;
			check_meritNum = <?=$r_meritNum?>;
			check_conductScore = <?=$r_conduct?>;
			check_studyScore = <?=($r_subscore1?$r_subscore1:0)?>;
			check_activityScore = <?=($r_subscore2?$r_subscore2:0)?>;
		}
		else
		{
			check_meritType = <?=($ProfileMeritType?$ProfileMeritType:0) ?>;
			check_meritNum = <?=($MeritNum?$MeritNum:0)?>;
			check_conductScore = <?=($ConductScore?$ConductScore:0)?>;
			check_studyScore = <?=($StudyScore?$StudyScore:0)?>;
			check_activityScore = <?=($ActivityScore?$ActivityScore:0)?>;
		}
		
         // Merit Type
         for (i=0; i<meritType.options.length; i++)
         {
              if (meritType.options[i].value==check_meritType)
              {
                  meritType.selectedIndex = i;
                  break;
              }
         }
         // Merit Num
         for (i=0; i<meritNum.options.length; i++)
         {
              if (meritNum.options[i].value==check_meritNum)
              {
                  meritNum.selectedIndex = i;
                  break;
              }
         }

         // Conduct Score
         <? if(!$ldiscipline->Hidden_ConductMark) {?>
         for (i=0; i<conductScore.options.length; i++)
         {
              if (conductScore.options[i].value==check_conductScore)
              {
                  conductScore.selectedIndex = i;
                  break;
              }
         }
         <? } ?>

          // Sub Score
         <? if ($ldiscipline->UseSubScore) { ?>
          for (i=0; i<subScore1.options.length; i++)
          {
               if (subScore1.options[i].value==check_studyScore)
               {
                   subScore1.selectedIndex = i;
                   break;
               }
          }
         <? } ?>

          // Act Score
         <? if ($ldiscipline->UseActScore) { ?>
          for (i=0; i<actScore1.options.length; i++)
          {
               if (actScore1.options[i].value==check_activityScore)
               {
                   actScore1.selectedIndex = i;
                   break;
               }
          }
         <? } ?>
         
         <? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
        	if(document.form1.maxStudyScore) {
        		document.form1.maxStudyScore.value = "<?=$r_MaxGainDeductSubScore1?>";
        		$('#MaxStudyScoreWarnDiv').html('<?=str_replace("<!--MAX_SCORE-->", $r_MaxGainDeductSubScore1, $Lang['eDiscipline']['StudentExceedMeritItemMaxStudyScore'])?>');
        		$('#MaxStudyScoreWarnDiv').hide();
        		resetOverflowStudyScoreDisable();
        	}
         <? } ?>
     }
        <?
}
?>
document.form1.temp_flag.value = 1;
<?php if($sys_custom['eDiscipline']['yy3']){ ?>
	checkOverflowConductMark();
<?php } ?>
}

function CompareDates(date_string1, date_string2, EqualDateAllow)
{
	var yr1  = parseInt(date_string1.substring(0,4),10);
	var mon1 = parseInt(date_string1.substring(5,7),10);
	var dt1  = parseInt(date_string1.substring(8,10),10);
	var yr2  = parseInt(date_string2.substring(0,4),10);
	var mon2 = parseInt(date_string2.substring(5,7),10);
	var dt2  = parseInt(date_string2.substring(8,10),10);
	var date1 = new Date(yr1, mon1-1, dt1);
	var date2 = new Date(yr2, mon2-1, dt2);
	
	if(EqualDateAllow == 0)
	{
		if(date2 < date1) {
			return true;
		}else{
			return false;
		}
	}else{
		if(date2 <= date1) {
			return true;
		}else{
			return false;
		}
	}
}

function check_form()
{
	obj = document.form1;
	checkOptionAll(document.getElementById('Attachment[]'));
	
	<? if(!$isAcc && !$isWaived)  {?>
	// Record Date
	if(CompareDates(obj.RecordDate.value,"<?=date("Y-m-d")?>", 0))
	{
		alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
			return false;
	}
	
	if(!check_date(obj.RecordDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
	{
		return false;
	}else{
		<? if($EnableMaxRecordDaysChecking == 1) { ?>
			// Check Record Date is allow or not
			var DateMinLimit = "<?=$DateMinLimit;?>";
			var DateMaxLimit = "<?=$DateMaxLimit;?>";
			if(!CompareDates(obj.RecordDate.value, DateMinLimit, 1))
			{
				alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
				return false;
			}else{
				if(!CompareDates(DateMaxLimit, obj.RecordDate.value, 1))
				{
					alert("<?=$eDiscipline['JSWarning']['RecordDateIsNotAllow'];?>");
					return false;
				}
			}
		<? } ?>
	}
	
	<? if($sys_custom['eDiscipline']['yy3']) {?>
		if(!check_date(obj.RecordInputDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
		{
			return false;
		}
		
		if(CompareDates(obj.RecordInputDate.value,"<?=date("Y-m-d")?>", 0))
		{
			alert("<?=$eDiscipline['JSWarning']['RecordInputDateIsNotAllow'];?>");
				return false;
		}
		
		if(CompareDates(obj.RecordDate.value, obj.RecordInputDate.value, 0))
		{
			alert("<?=$eDiscipline['JSWarning']['RecordInputDateIsNotAllow'];?>");
				return false;
		}
	<? } ?>
	
	<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) { ?>
	if(!CompareDates(obj.ActionDueDate.value, obj.RecordDate.value, 1)) {
				alert("<?=$Lang['eDiscipline']['ActionDateCompareWithRecordDate']?>");
				return false;
	}
	<? } ?>
	
 	// Semester
	<? /*if($semester_mode==2) {?>
	if(!check_semester(obj.RecordDate))
	{
		return false;
	}
	<? } */?>

	// Teahcer/Staff
	
	<? if($sys_custom['winglung_ap_report']) { ?>
	if(obj.picType[0].checked==true) {
		objPic = document.getElementById('PIC[]');
		checkOption(objPic);
		if (objPic.length==0) {
			checkOptionAdd(objPic, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
			alert("<?=$i_alert_pleaseselect.$i_Discipline_Duty_Teacher?>");
			return false;
		}
		checkOptionAll(objPic);
	} 
	if(obj.picType[1].checked==true && obj.picName.value=="") {
		alert("<?=$iDiscipline['Assign_Duty_Teacher']?>");	
		return false;
	}
	<? } else { ?>
		objPic = document.getElementById('PIC[]');
		checkOption(objPic);
		if (objPic.length==0) {
			checkOptionAdd(objPic, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
			alert("<?=$i_alert_pleaseselect.$i_Discipline_Duty_Teacher?>");
			return false;
		}
		checkOptionAll(objPic);
	<? } ?>
	
	<? if($AccessItem) {?>
	/* check the merit type 0 - N.A. */
	if(document.getElementById('MeritNum').value==0)	document.getElementById('MeritType').selectedIndex = 0;
	if(document.getElementById('MeritType').value==-999)	document.getElementById('MeritNum').selectedIndex = 0;
	
	// Category / Items
    if(document.getElementById('CatID').value == 0 || document.getElementById('ItemID').value == 0)
    {
            alert('<?=$i_alert_pleaseselect.$i_Discipline_System_general_record?>');
            return false;
    }
    
    // checking on MeritNum, ConductScore & Subscore (if any)
    // NotAllowEmptyInputInAP : <?=$ldiscipline->NotAllowEmptyInputInAP?>
    
    <? if($ldiscipline->NotAllowEmptyInputInAP) { ?>
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
			if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? } else {
			if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			$alertMsg = $i_alert_pleaseselect.$alertMsg;
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    alert("<?=$alertMsg ?>");
        	return false;
	    }
	    <? } ?>
    <? } else { ?>
    	// allow null input
    	
	    <? 
	    if(!$ldiscipline->Hidden_ConductMark) {
			if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit'].", ".$eDiscipline['Conduct_Mark']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$eDiscipline['Conduct_Mark'];
			}
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["ConductScore"].value == 0 && obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            obj.action='edit_update.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? } else { 
			if($ldiscipline->UseActScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0 && obj.elements["ActivityScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1." ".$i_alert_or." ".$Lang['eDiscipline']['ActivityScore'];
			}
			else if($ldiscipline->UseSubScore) {
				$jsScript = ' && obj.elements["StudyScore"].value == 0';	
				$alertMsg = $eDiscipline['MeritDemerit']." ".$i_alert_or." ".$i_Discipline_System_Subscore1;
			}
			else {
				$alertMsg = $eDiscipline['MeritDemerit'];
			}
			if($intranet_session_language=="en")
				$alertMsg = $Lang['eDiscipline']['EmptyInput']." ".$alertMsg." ".$Lang['eDiscipline']['IsSelected'].$button_confirm."?";
			else 
				$alertMsg = $Lang['eDiscipline']['IsSelected'].$alertMsg.$Lang['eDiscipline']['sdbnsm_fullStop'].$button_confirm."?";
		    ?>
	    else if(obj.elements["MeritNum"].value == 0 <?=$jsScript?>)
	    {
		    if (window.confirm('<?=$alertMsg?>'))
	        {
	            //obj.action='new3.php';
	            //return true;
	        }
	        else
	        {
	        	return false;
	    	}
	    }
	    <? } ?>
    <? } ?>
    <? } ?>
    
    /*
    // check merit type
    if(document.getElementById('MeritType').value == -999)
    {
	 	alert("<?=$i_alert_pleaseselect?><?=$i_Discipline_System_Add_Merit_Demerit?>");
	 	return false;   
    }*/
    
    <? if($AccessItem) {?>
    <? if((!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval")) && $thisStatus != DISCIPLINE_STATUS_PENDING) {?>
    // check the record need re-approve or not
    if(parseInt(approval_rules_js[document.getElementById('MeritType').value]) >=0 && parseInt(approval_rules_js[document.getElementById('MeritType').value]) <= parseInt(document.getElementById('MeritNum').value))
    {
	   if(!confirm("<?=$eDiscipline['RecordWillChangeToWaiting_alert']?>"))
	 		return false;
    }
    <? } ?>
    <? } ?>
    
	<? } ?>
	<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) { ?>
	if(!check_date(obj.ActionDueDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) {
			return false;
	}
	<? } ?>
	checkOptionAll(document.getElementById('PIC[]'));
	
<?php if($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER()){ ?>
	$('#MeritNum').after('<input type="hidden" name="MeritNum" value="'+$('#MeritNum').val()+'" />');
	$('#MeritType').after('<input type="hidden" name="MeritType" value="'+$('#MeritType').val()+'" />');
<?php } ?>	
}

function check_semester(objDate)
{
	semester_periods = sem_js;
	
	valid = false;
	if(semester_periods!=null)
	{
		for(j=0;j<semester_periods.length;j++){
			dateStart = semester_periods[j][0];
			dateEnd = semester_periods[j][1];
			if(dateStart <= objDate.value && dateEnd >=objDate.value){
				valid = true;
				break;
			}
		}
	}
	
	if(!valid)
	{
		objDate.focus();
		objDate.className ='textboxnum textboxhighlight';
		alert('<?=$eDiscipline["NoSemesterSettings"]?>');
		return false;
	}
	
	return true;
}

function click_cancel()
{
	<?if($back_page=="index.php") {?>
		window.location="index.php?SchoolYear=<?=$SchoolYear?>&semester=<?=$semester?>&targetClass=<?=$targetClass?>&num_per_page=<?=$num_per_page?>&pageNo=<?=$pageNo?>&order=<?=$order?>&field=<?=$field?>&page_size_change=<?=$page_size_change?>&numPerPage=<?=$numPerPage?>&MeritType=<?=$MeritType?>&s=<?=$s?>&clickID=<?=$clickID?>&field=<?=$field?>&SchoolYear=<?=$SchoolYear2?>&semester=<?=$semester2?>&targetClass=<?=$targetClass2?>&waived=<?=$waived2?>&approved=<?=$approved2?>&waitApproval=<?=$waitApproval2?>&rejected=<?=$rejected2?>&released=<?=$released2?>&fromPPC=<?=$fromPPC?>&passedActionDueDate=<?=$passedActionDueDate?>&pic=<?=$pic?>";
	<? } else {?>
	document.form1.action="<?=$back_page?>";
	document.form1.submit();
	<? } ?>
}

<?php if($sys_custom['eDiscipline']['yy3']){ ?>
function checkOverflowConductMark()
{
	var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image()?>';
	var studentIdAry = [<?=$data['StudentID']?>];
	var recordDate = document.getElementById('RecordDate').value.Trim();
	var recordType = '<?=$data['MeritType']?>';
	var itemId = $('select[name=ItemID]').val();
	var conductScore = $('select[name=ConductScore]').val();
	var recordId = $('#RecordID').val();
	
	if(itemId > 0 && conductScore > 0 && studentIdAry.length > 0 && recordDate != '') {
		$('input#continueBtn').attr('disabled',true);
		$('input#submitBtn').attr('disabled',true);
		
		$('#OverflowMarkWarnDiv').html(loadingImg).load(
			'ajax_check_overflow_mark.php',
			{
				'StudentID[]': studentIdAry,
				'RecordDate':recordDate,
				'RecordType':recordType,
				'ItemID':itemId,
				'ConductScore':conductScore,
				'RecordID':recordId 
			},
			function(data){
				$('input#continueBtn').attr('disabled',false);
				$('input#submitBtn').attr('disabled',false);
			}
		);
	}else{
		$('#OverflowMarkWarnDiv').html('');
	}
}

$(document).ready(function(){
	checkOverflowConductMark();
});
<?php } ?>

<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
function checkOverflowStudyScore()
{
	resetOverflowStudyScoreDisable();
	
	// Exceed Max Study Score (Display Warning & Disable Submit Button)
	if(document.form1.StudyScore && document.form1.maxStudyScore && parseInt(document.form1.maxStudyScore.value) > 0 && parseInt(document.form1.StudyScore.value) > parseInt(document.form1.maxStudyScore.value)) {
		document.form1.StudyScore.focus();
		$('#MaxStudyScoreWarnDiv').show();
		$("input[type=submit]", document.form1).attr('disabled', true);
		$("input[type=submit]", document.form1).attr('class', 'formbutton_disable_v30 print_hide');
	}
}

function resetOverflowStudyScoreDisable()
{
	// Reset
	$('#MaxStudyScoreWarnDiv').hide();
	$("input[type=submit]", document.form1).attr('disabled', false);
	$("input[type=submit]", document.form1).attr('class', 'formbutton_v30 print_hide');
}
<? } ?>
//-->
</SCRIPT>

<div id="ref_list"></div>
<form name="form1" method="POST" action="edit_update.php" onsubmit="return check_form();">

<br/>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<table class="form_table_v30">
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_identity_student?></td>
			<td valign="top" class="tablerow2"><?=$selected_student_table?></td>
		</tr>
		<tr>
			<td style="border:none;" colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title"><i> - <?=$eDiscipline["BasicInfo"]?> -</i></em></td>
		</tr>
		
		<?=$select_sem_html?>
		
		<tr>
                <td class="field_title"><?=$Lang['eDiscipline']['EventDate']?><? if(!$isAcc) {?><span class="tabletextrequire">* </span><? } ?></td>
                <td><?=$RecordDateDisplay?></td>
        </tr>
        
        <? if($sys_custom['eDiscipline']['yy3']) {?>
        <tr>
                <td class="field_title"><?=$Lang['eDiscipline']['EventRecordDate']?> <span class="tabletextrequire">* </span></td>
                <td><?=$RecordInputDateDisplay?></td>
        </tr>
        <? } ?>
        
		<tr>
			<td class="field_title"><?=$i_Discipline_System_Contact_PIC?> <? if(!$isAcc) {?><span class="tabletextrequire">* </span><? } ?></td>
			<td valign="top">
				<table class="inside_form_table">
					<? if($sys_custom['winglung_ap_report']) { ?>
					<tr>
						<td><input type="radio" name="picType" id="picType1" value="1" <? if(sizeof($array_PIC)>0) echo "checked";?>></td>
						<td><?=$PICDisplay?></td>
						<td valign="bottom">
							<?=$linterface->GET_BTN($button_select, "button","form1.picType1.checked=true; newWindow('../choose_pic.php?fieldname=PIC[]',9)");?><br />
							<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC[]']); form1.picType1.checked=true");?>
						</td>
					</tr>
					<tr>
						<td><input type="radio" name="picType" id="picType2" value="2" <? if(sizeof($array_PIC)==0) echo "checked";?>></td>
						<td colspan="2"><input name="picName" type="text" class="tabletext" value="<?=$picName?>" onFocus="form1.picType2.checked=true"/></td>
					</tr>
					<? }
					else { ?>
					<tr>
						<?=$PICDisplay?>
						<td valign='bottom'>
						<? if(!$ldiscipline->OnlyAdminSelectPIC || ($ldiscipline->OnlyAdminSelectPIC && $ldiscipline->IS_ADMIN_USER()) ) { ?>
							<?//=$linterface->GET_BTN($button_select, "button","newWindow('../choose_pic.php?fieldname=PIC[]',9)");?>
							<?=$linterface->GET_BTN($Lang['Btn']['Select'], "button", "newWindow('/home/common_choose/index.php?fieldname=PIC[]&permitted_type=1&DisplayGroupCategory=1&Disable_AddGroup_Button=1', 11)")?><br />
							<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['PIC[]']);");?>
							<? } ?>
						</td>
					</tr>
					<? } ?>
				</table>
			</td>
		</tr>
		<tr>
            <td class="field_title"><?=$i_Discipline_System_Discipline_Case_Record_Attachment?></td>
            <td><?=$attachmentHTML?></td>
        </tr>
		<? if ($ldiscipline->use_subject) {?>
            <tr>
                <td class="field_title"><span class="tabletext"><?=$i_Discipline_Subjects?></span></td>
                <td><?=$subjectDisplay?></td>
            </tr>
        <? } ?>
		<tr>
			<td class="field_title"><?=$i_UserRemark?></td>
			<td><?=$linterface->GET_TEXTAREA("remark", str_replace("<br/>", "\n", $data['Remark']));?></td>
		</tr>

        <? if ($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
            <tr>
                <td class="field_title"><?=$Lang['eDiscipline']['FollowUpRemark']?></td>
                <td><?=$linterface->GET_TEXTAREA("followUpRemark", str_replace("<br/>", "\n", $data['FollowUpRemark']));?></td>
            </tr>
        <? } ?>
		
		<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) {?>
            <tr>
                <td class="field_title"><?=$Lang['eDiscipline']['FileNumber']?></td>
                <td><input type="text" name="file_number" value="<?=$data['FileNumber']?>"></td>
            </tr>

            <tr>
                <td class="field_title"><?=$Lang['eDiscipline']['RecordNumber']?></td>
                <td><input type="text" name="record_number" value="<?=$data['RecordNumber']?>"></td>
            </tr>
		<? } ?>
		
		<? if(trim($lu->Remark)) {?>
            <tr>
                <td class="field_title"><?=$Lang['General']['StudentInternalRemark']?></td>
                <td><?=nl2br($lu->Remark)?></td>
            </tr>
		<? } ?>

		<tr>
			<td style="border:none;" colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title"><i> - <?=$eDiscipline["DisciplineDetails2"]?> -</i></em></td>
		</tr>
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eDiscipline["Type"]?></span></td>
			<td><?=($RecordType==1)? $i_Merit_Award : $i_Merit_Punishment;?></td>
		</tr>
		<tr valign="top">
			<td class="field_title"><span class="tabletext"><?=$eDiscipline["Category_Item"]?> <? if($AccessItem && !$isAcc) {?><span class="tabletextrequire">*</span><? } ?></span></td>
			<td><table class="inside_form_table"><tr><td><?=$CatDisplay?> - <?=$ItemDisplay?></td><?=$duplicate_msg?></tr></table></td>
		</tr>
		
		<tr id="TrMeritRow">
			<td class="field_title"> <?=$i_Discipline_Reason2?></td>
			<td><?=$MeritDisplay?></td>
		</tr>
		<? if(!$ldiscipline->Hidden_ConductMark) { ?>
            <tr>
                <td class="field_title"><?=$eDiscipline['Conduct_Mark']?></td>
                <td><span id="increment_de"><?=$Lang['eDiscipline']['Gain']?></span> <?=$conduct_markDisplay?> <?=($sys_custom['eDiscipline']['yy3']?'<div id="OverflowMarkWarnDiv" style="color:red;"></div>':'')?></td>
            </tr>
		<? } ?>
		
		<? if($ldiscipline->UseSubScore) { ?>
            <tr>
                <td class="field_title"><?=$i_Discipline_System_Subscore1?></td>
                <td>
                    <span id="increment_de2"><?=$Lang['eDiscipline']['Gain']?></span> <?=$study_markDisplay?>
                    <? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
                        <div id="MaxStudyScoreWarnDiv" style="color:red;"></div>
                        <input type="hidden" name="maxStudyScore" value="0" />
                    <? } ?>
                </td>
            </tr>
		<? } ?>
		
		<? if($ldiscipline->UseActScore) { ?>
            <tr>
                <td class="field_title"><?=$Lang['eDiscipline']['ActivityScore']?></td>
                <td><span id="increment_de3"><?=$Lang['eDiscipline']['Gain']?></span> <?=$activity_markDisplay?></td>
            </tr>
		<? } ?>
		
		<? if($CreatorID > 0){ ?>
            <tr>
                <td style="border:none;" colspan="2" valign="middle" nowrap="nowrap"><em class="form_sep_title">- <?=$Lang['AccountMgmt']['CreateUserInfo']?> -</em></td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang['AccountMgmt']['CreateUserName'] ?></td>
                <td colspan="2"><?=$nameDisplay?></td>
            </tr>
            <tr>
                <td class="field_title"><?=$Lang['AccountMgmt']['CreateUserDate'] ?></td>
                <td colspan="2"><?=$CreateDate?></td>
            </tr>
  		<? } ?>
		
		<? if($sys_custom['wscss_disciplinev12_ap_prompt_action']) {
			$maxDate = $ldiscipline->MaxSchoolDayToCallForAction;
			$date = ($ActionDueDate=='' || $ActionDueDate=='0000-00-00') ? $ldiscipline->retrieveWaivePassDay(date('Y-m-d'), $maxDate) : $ActionDueDate;
		?>
            <tr>
                <td class="field_title"><?=$Lang['eDiscipline']['AP_ActionDueDate'] ?></td>
                <td><?=$linterface->GET_DATE_PICKER("ActionDueDate",$date)?></td>
            </tr>
		<? } ?>
	</table>
	
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "click_cancel();")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="semester_flag" id="semester_flag" value="1" />
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>" />

<input type="hidden" name="temp_flag" id="temp_flag" value="0" />

<!-- for back action //-->
<input type="hidden" name="num_per_page" id="num_per_page" value="<?=$num_per_page?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
<input type="hidden" name="s" id="s" value="<?=$s?>" />
<input type="hidden" name="clickID" id="clickID" value="<?=$clickID?>" />
<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
<input type="hidden" name="fieldname" id="fieldname" value="Attachment[]" />
<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="FolderLocation" id="FolderLocation" value="<?=$sessionTime?>" />

<? if($back_page=="index.php") {?>
<input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>" />
<input type="hidden" name="waived" id="waived" value="<?=$waived?>"/>
<input type="hidden" name="approved" id="approved" value="<?=$approved?>"/>
<input type="hidden" name="waitApproval" id="waitApproval" value="<?=$waitApproval?>"/>
<input type="hidden" name="released" id="released" value="<?=$released?>"/>
<input type="hidden" name="rejected" id="rejected" value="<?=$rejected?>"/>
<input type="hidden" name="MeritType2" id="MeritType2" value="<?=$MeritType?>"/>
<input type="hidden" name="pic" id="pic" value="<?=$pic?>"/>
<input type="hidden" name="back_page" id="back_page" value="<?=$back_page?>"/>
<? } else {?>
<input type="hidden" name="id" id="id" value="<?=$RecordID?>" />
<input type="hidden" name="SchoolYear2" id="SchoolYear2" value="<?=$SchoolYear2?>" />
<input type="hidden" name="semester2" id="semester2" value="<?=$semester2?>" />
<input type="hidden" name="targetClass2" id="targetClass2" value="<?=$targetClass2?>" />
<input type="hidden" name="waived2" id="waived2" value="<?=$waived2?>"/>
<input type="hidden" name="approved2" id="approved2" value="<?=$approved2?>"/>
<input type="hidden" name="waitApproval2" id="waitApproval2" value="<?=$waitApproval2?>"/>
<input type="hidden" name="released2" id="released2" value="<?=$released2?>"/>
<input type="hidden" name="rejected2" id="rejected2" value="<?=$rejected2?>"/>
<input type="hidden" name="MeritType2" id="MeritType2" value="<?=$MeritType2?>" />
<!--<input type="hidden" name="pic" value="<?=$pic?>"/>-->
<input type="hidden" name="back_page" id="back_page" value="<?=$back_page?>"/>
<? } ?>
<input type="hidden" name="fromPPC" id="fromPPC" value="<?=$fromPPC?>"/>
<input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="<?=$passedActionDueDate?>"/>
</form>

<? if($isAcc || $isWaived) { } else {?>
<script language="javascript">
<!--
<? if($AccessItem) {?>
changeMeritType(<?=$RecordType?>);
changeCat(<?=$CatID?>);
changeItem(<?=$ItemID?>);
<? } ?>
//-->
</script>

<? 
print $linterface->FOCUS_ON_LOAD("form1.RecordDate");
} ?>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>