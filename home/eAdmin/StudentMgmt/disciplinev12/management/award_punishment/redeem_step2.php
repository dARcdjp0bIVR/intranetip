<?php
# Using:

##### Change Log [Start] #####
#
#   Date    :   2019-05-21 (Bill)   [2019-0517-1540-40235]
#               prevent IntegerSafe() - change $MeritID to $MeritIDStr + $DemeritID to $DemeritIDStr
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Waive");

# Back Parameters
$back_para = "pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&pic=$pic";

# UI settigns
$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_AwardPunishment";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# step information
$STEPS_OBJ[] = array($eDiscipline["Redeem"]["Step1"], 0);
$STEPS_OBJ[] = array($eDiscipline["Redeem"]["Step2"], 1);

# navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php?$back_para");
$PAGE_NAVIGATION[] = array($eDiscipline["Redemption"], "");

$linterface->LAYOUT_START();

# initialization
$MeritIDArr = unserialize(rawurldecode($MeritIDStr));

$DemeritIDArr = array();
if(is_array($DemeritID))
	$DemeritIDArr = $DemeritID;
else
	$DemeritIDArr[] = $DemeritID;
	
//$TableColumnArr = array("Record", "Reason", "EventDate", "PIC", "Reference", "Action", "Status");
$TableColumnArr[] = "Record";
$TableColumnArr[] = "Reason";
if($ldiscipline->Display_ConductMarkInAP) {
	$TableColumnArr[] = "ConductScoreChange";
}
if($ldiscipline->UseSubScore) {
	$TableColumnArr[] = "SubScore1Change";
}
$TableColumnArr[] = "EventDate";
$TableColumnArr[] = "PIC";
$TableColumnArr[] = "Reference";
$TableColumnArr[] = "Action";
$TableColumnArr[] = "Status";

# Get Student Name
$MeritInfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritIDArr[0]);
$StudentID = $MeritInfo[0]['StudentID'];
$lu = new libuser($StudentID);
$student_name = $lu->UserNameLang();
$class_name = $lu->ClassName;
$class_no = $lu->ClassNumber;
$selected_student_table = "<table width='90%' border='0' cellspacing='0' cellpadding='5'>";
$selected_student_table .= "<tr><td width='50%' valign='top' class='sectiontitle'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle' />";
$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
$selected_student_table .= "</td></tr>";
$selected_student_table .= "</table>";

# Merit Table
$meritTable = "";
# Navigation
$meritTable = "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
$meritTable .= "<tr><td width='50%' valign='top' class='tabletext'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'>";
$meritTable .= "<b>".$eDiscipline["AwardRedemption"]."</b>";
$meritTable .= "</td></tr>";
$meritTable .= "</table>";
# Content
$meritTable .= $ldiscipline->generateRedeemInfoTable($MeritIDArr, $TableColumnArr, $width="90%", $showRemark=1);

# Demerit Table
$demeritTable = "";
# Navigation
$demeritTable = "<table width='90%' border='0' cellspacing='0' cellpadding='4'>";
$demeritTable .= "<tr><td width='50%' valign='top' class='tabletext'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'>";
$demeritTable .= "<b>".$eDiscipline["PunishmentRedemption"]."</b>";
$demeritTable .= "</td></tr>";
$demeritTable .= "</table>";
# Content
$demeritTable .= $ldiscipline->generateRedeemInfoTable($DemeritIDArr, $TableColumnArr, $width="90%", $showRemark=1);


?>

<script language="javascript">
<!--

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - 320}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	
}

function checkCR(evt) {

	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	

}

document.onkeypress = checkCR;

/*********************************/
var xmlHttp

function showResult(str,flag)
{
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	} else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged3
	}  else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged4
	}
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history2_"+id).style.zIndex = "1";
		document.getElementById("show_history2_"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history2_"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged4() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
/*********************************/

function jBack()
{
	document.form1.action = 'redeem_step1.php';
	document.form1.submit();
}

function jCancel()
{
	document.form1.action = 'index.php?'+'<?=$back_para?>';
	document.form1.submit();
}

function jSubmit()
{
	document.form1.action = 'redeem_step2_update.php';
	document.form1.submit();
}
//-->
</script>


<form name="form1" method="POST" action="redeem_step1_update.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="navigation">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
					<tr><td>&nbsp;</td></tr>
					<tr><td><?=$linterface->GET_STEPS($STEPS_OBJ)?></td></tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $selected_student_table ?>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $meritTable ?>
			</td>
		</tr>
		<tr>
			<td align="center">
				<?= $demeritTable ?>
			</td>
		</tr>
	</table>
	
	<br />
	
	<table width="90%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="1" class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:jSubmit();", "submitBtn") ?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:jBack();") ?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:jCancel();") ?>&nbsp;
			</td>
		</tr>
	</table>
	
	<br />
	<br />
	
	<input type="hidden" name="pageNo" value="<?php echo $pageNo; ?>"/>
	<input type="hidden" name="order" value="<?php echo $order; ?>"/>
	<input type="hidden" name="field" value="<?php echo $field; ?>"/>
	<input type="hidden" name="page_size_change" value=""/>
	<input type="hidden" name="numPerPage" value="<?=$page_size?>"/>
	<input type="hidden" name="MeritType" value="<?=$MeritType ?>">
	<input type="hidden" name="s" value="<?=$s?>"/>
	<input type="hidden" name="pic" value="<?=$pic?>"/>
	
	<input type="hidden" name="clickID" value=""/>
	<input type="hidden" name="MeritIDStr" value="<?=rawurlencode(serialize($MeritIDArr));?>">
	<input type="hidden" name="DemeritIDStr" value="<?=rawurlencode(serialize($DemeritIDArr));?>">
</form>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
?>