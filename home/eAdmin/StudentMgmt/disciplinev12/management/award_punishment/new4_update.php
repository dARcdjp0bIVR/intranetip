<?php
# using:

########## Change Log ###############
#
#	Date	:	2020-11-13 (Bill)	[2020-1009-1753-46096]
# 				delay notice start date & end date		($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'])
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#   Date    :   2019-02-21  (Bill)  [DM#3538]
#               added record status, study / activity score display in email and push message notification 
#
#   Date    :   2019-02-21  (Bill)  [DM#3539]
#               fixed htmlspecialchars display (AP remarks) in push message notification
#
#   Date    :   2019-01-14  (Bill)  [2018-0417-1222-44170]
#               send push message to target staff
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");
$lclass = new libclass();

$approved = $released ? $released : $approved;

$student_ary = array();
$student_class_ary = array();
$student_ary_msg = array();
$student_class_ary_msg = array();

foreach((array)$MeritRecordID as $RecordID)
{
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
	$thisProfileMeritID = $datainfo[0]['ProfileMeritID'];
	$StudentID = $datainfo[0]['StudentID'];
	
	# approve
	if($approved && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval"))
	{
		if($thisProfileMeritID)
		{
		    # already approved
			# do nothing
		}
		else
		{
			# set APPROVE
			$ldiscipline->APPROVE_MERIT_RECORD($RecordID);
		}
	}
    
	# release
	if($released && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release"))
	{
		# UPDATE [DISCIPLINE_MERIT_RECORD] field ReleasedBy, ReleasedDate, RecordStatus = RELEASE
		if($datainfo[0]['ReleaseStatus'] != DISCIPLINE_STATUS_RELEASED) {
			$ldiscipline->UPDATE_MERIT_RECORD_RELEASE_STATUS($RecordID, DISCIPLINE_STATUS_RELEASED);
			//$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
			$sendNoticeNow = 1;
		}
        
		# Send eNotice
		$template_info = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO($datainfo[0]['TemplateID']);
		if(sizeof($template_info)>0 && $sendNoticeNow==1)
		{
			$template_category = $template_info[0]['CategoryID'];

			$SpecialData = array();
			$SpecialData['MeritRecordID'] = $RecordID;
			$template_data = $ldiscipline->TEMPLATE_VARIABLE_CONVERSION($template_category, $StudentID, $SpecialData, $datainfo[0]['TemplateOtherInfo']);
			
			$UserName = $ldiscipline->getUserNameByID($UserID);
			$Module = $ldiscipline->Module;
			$NoticeNumber = time();
			$TemplateID = $datainfo[0]['TemplateID'];
			$Variable = $template_data;
			$IssueUserID = $UserID;
			$IssueUserName = $UserName[0];
			$TargetRecipientType = "U";
			$RecipientID = array($StudentID);
			$RecordType = NOTICE_TO_SOME_STUDENTS;
            
			if (!$lnotice->disabled)
			{
				include_once($PATH_WRT_ROOT."includes/libucc.php");
				$lc = new libucc();
				$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, $TemplateID, $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);

                // [2020-1009-1753-46096] delay notice start date & end date
                if (isset($sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay']) && $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'] > 0)
                {
                    $delayedHrs = $sys_custom['eDiscipline']['NoticeStartDate_ScheduledDelay'];

                    $DateStart = date('Y-m-d H:i:s', strtotime("+".$delayedHrs." hours"));
                    $lc->setDateStart($DateStart);

                    if ($lc->defaultDisNumDays > 0) {
                        $DateEndTs = time() + ($lc->defaultDisNumDays * 24 * 60 * 60) + ($delayedHrs * 60 * 60);
                        $DateEnd = date('Y-m-d', $DateEndTs).' 23:59:59';
                        $lc->setDateEnd($DateEnd);
                    }
                }
                
				$NoticeID = $lc->sendNotice();
			}
		    
			if($NoticeID != -1)
			{
				# update [DISCIPLINE_MERIT_RECORD] field NoticeID
				$updateDataAry = array();
				$updateDataAry['NoticeID'] = $NoticeID;
				$ldiscipline->UPDATE_MERIT_RECORD($updateDataAry, $RecordID);
			}
		}
	}
	
	# send email (build email content)
	if($email_PIC || $email_ClassTeacher || $email_DisciplineAdmin)
	{
		$lu = new libuser($StudentID);
		$student_name = $lu->UserNameClassNumber();
		$thisClassName = $lu->ClassName;
		
		if($email_PIC || $email_DisciplineAdmin)
		{
			$student_ary[] = $student_name;
		}
		
		if($email_ClassTeacher)
		{
			$student_class_ary[$thisClassName][] = $student_name;
		}
	}
	
	# send push message (build message content)    [2018-0417-1222-44170]
	if($plugin['eClassApp'] && $plugin['eClassTeacherApp'] && ($pushmessagenotify_PIC || $pushmessagenotify_ClassTeacher || $pushmessagenotify_DisciplineAdmin))
	{
	    $lu = new libuser($StudentID);
	    $student_name = $lu->UserNameClassNumber();
	    $thisClassName = $lu->ClassName;
	    
	    if($pushmessagenotify_PIC || $pushmessagenotify_DisciplineAdmin)
	    {
	        $student_ary_msg[] = $student_name;
	    }
	    
	    if($pushmessagenotify_ClassTeacher)
	    {
	        $student_class_ary_msg[$thisClassName][] = $student_name;
	    }
	}
}

# send email
if($email_PIC || $email_ClassTeacher || $email_DisciplineAdmin)
{
    include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	include_once($PATH_WRT_ROOT."lang/email.php");
	$lwebmail = new libwebmail();
	$lcampusmail = new libcampusmail();
	$senderID = $UserID;
	
	$CatID = $ldiscipline->returnCatIDByItemID($datainfo[0]['ItemID']);
	$CatInfo = $ldiscipline->returnMeritItemCategoryByID($CatID);
	if($datainfo[0]['ProfileMeritCount'])
	{
		$thisMeritType = $ldiscipline->RETURN_MERIT_NAME($datainfo[0]['ProfileMeritType']);
		$receive = $ldiscipline->TrimDeMeritNumber_00($datainfo[0]['ProfileMeritCount']) . " x " .$thisMeritType;
	}
	$dataRemark = $datainfo[0]['Remark'];
	// [2020-0824-1151-40308]
    $dataFollowUpRemark = $datainfo[0]['FollowUpRemark'];
	
	// [DM#3538] Get record status
	$dataMeritInfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
	$dataRecordStatus = $dataMeritInfo[0]['RecordStatus'];
	$dataRecordStatusStr = '';
	if($dataRecordStatus == DISCIPLINE_STATUS_APPROVED) {
	    $dataRecordStatusStr = $i_Discipline_System_Award_Punishment_Approved;
	}
	else if ($dataRecordStatus == DISCIPLINE_STATUS_PENDING) {
	    $dataRecordStatusStr = $i_Discipline_System_Award_Punishment_Pending;
	}
	// Get Study / Activity Score
	$dataSubScoreArr = array();
	if($ldiscipline->UseSubScore) {
	    $dataSubScoreArr[1] = $datainfo[0]['SubScore1Change'];
	}
	if($ldiscipline->UseActScore) {
	    $dataSubScoreArr[2] = $datainfo[0]['SubScore2Change'];
	}
	
	if($email_PIC)
	{
		$receiver = array();
		$receiver = explode(",",$datainfo[0]['PICID']);
		$student_list = implode(",",$student_ary);

		// [2020-0824-1151-40308]
        //$email_content_temp = $ldiscipline->EMAIL_CONTENT_AWARD_PUNISHMENT($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr);
		$email_content_temp = $ldiscipline->EMAIL_CONTENT_AWARD_PUNISHMENT($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr, $dataFollowUpRemark);
		list($subject, $content) = $email_content_temp;
		$content = addslashes($content);

		//$lcampusmail->sendMail($receiver, $subject, $content, $senderID);
		$exmail_success = $lwebmail->sendModuleMail($receiver, $subject, $content, 1);
	}
	
	if($email_DisciplineAdmin)
	{
		$receiver = array();
		$DisciplineAdmin = $ldiscipline->GET_ADMIN_USER();
		$receiver = explode(',',$DisciplineAdmin);
		$student_list = implode(",",$student_ary);

		// [2020-0824-1151-40308]
        //$email_content_temp = $ldiscipline->EMAIL_CONTENT_AWARD_PUNISHMENT($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr);
		$email_content_temp = $ldiscipline->EMAIL_CONTENT_AWARD_PUNISHMENT($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr, $dataFollowUpRemark);
		list($subject, $content) = $email_content_temp;
		$content = addslashes($content);

		//$lcampusmail->sendMail($receiver, $subject, $content, $senderID);
		$exmail_success = $lwebmail->sendModuleMail($receiver, $subject, $content, 1);
	}	
	
	if($email_ClassTeacher)
	{
		foreach($student_class_ary as $classname => $stu_ary)
		{
			$receiver = array();
			$ClassTeacherID = $lclass->returnClassTeacherID($classname);
			foreach($ClassTeacherID as $k=>$d)
			{
				$receiver[] = $d['UserID'];
			}
			$receiver = array_unique($receiver);
			$student_list = implode(",",$stu_ary);

            // [2020-0824-1151-40308]
            //$email_content_temp = $ldiscipline->EMAIL_CONTENT_AWARD_PUNISHMENT($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr);
			$email_content_temp = $ldiscipline->EMAIL_CONTENT_AWARD_PUNISHMENT($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr, $dataFollowUpRemark);
			list($subject, $content) = $email_content_temp;
			$content = addslashes($content);

			//$lcampusmail->sendMail($receiver, $subject, $content, $senderID);
			$exmail_success = $lwebmail->sendModuleMail($receiver, $subject, $content, 1);
		}
	}
}

# send push message [2018-0417-1222-44170]
if($plugin['eClassApp'] && $plugin['eClassTeacherApp'] && ($pushmessagenotify_PIC || $pushmessagenotify_ClassTeacher || $pushmessagenotify_DisciplineAdmin))
{
    include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
    $libeClassApp = new libeClassApp();
    
    $isPublic = "N";
    $appType_t = $eclassAppConfig['appType']['Teacher'];
    $senderID = $UserID;
    
    $CatID = $ldiscipline->returnCatIDByItemID($datainfo[0]['ItemID']);
    $CatInfo = $ldiscipline->returnMeritItemCategoryByID($CatID);
    if($datainfo[0]['ProfileMeritCount'])
    {
        $thisMeritType = $ldiscipline->RETURN_MERIT_NAME($datainfo[0]['ProfileMeritType']);
        $receive = $ldiscipline->TrimDeMeritNumber_00($datainfo[0]['ProfileMeritCount']) . " x " .$thisMeritType;
    }
    
    // [DM#3539] Handle htmlspecialchars in remarks for push message display
    $dataRemark = $datainfo[0]['Remark'];
    $dataRemark = str_replace("&#039;", "'", $dataRemark);
    $dataRemark = str_replace("&#", "&amp;#", $dataRemark);
    $dataRemark = htmlspecialchars_decode($dataRemark);

    // [2020-0824-1151-40308]
    $dataFollowUpRemark = $datainfo[0]['FollowUpRemark'];
    $dataFollowUpRemark = str_replace("&#039;", "'", $dataFollowUpRemark);
    $dataFollowUpRemark = str_replace("&#", "&amp;#", $dataFollowUpRemark);
    $dataFollowUpRemark = htmlspecialchars_decode($dataFollowUpRemark);

    // [DM#3538] Get record status
    $dataMeritInfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);
    $dataRecordStatus = $dataMeritInfo[0]['RecordStatus'];
    $dataRecordStatusStr = '';
    if($dataRecordStatus == DISCIPLINE_STATUS_APPROVED) {
        $dataRecordStatusStr = $i_Discipline_System_Award_Punishment_Approved;
    }
    else if ($dataRecordStatus == DISCIPLINE_STATUS_PENDING) {
        $dataRecordStatusStr = $i_Discipline_System_Award_Punishment_Pending;
    }
    // Get Study / Activity Score
    $dataSubScoreArr = array();
    if($ldiscipline->UseSubScore) {
        $dataSubScoreArr[1] = $datainfo[0]['SubScore1Change'];
    }
    if($ldiscipline->UseActScore) {
        $dataSubScoreArr[2] = $datainfo[0]['SubScore2Change'];
    }
    
    if($pushmessagenotify_PIC || $pushmessagenotify_DisciplineAdmin)
    {
        $student_list = implode(",", $student_ary_msg);

        // [2020-0824-1151-40308]
        //list($pushmessage_subject, $pushmessage_body) = $ldiscipline->returnAwardPunishmentPushMessageContent($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr);
        list($pushmessage_subject, $pushmessage_body) = $ldiscipline->returnAwardPunishmentPushMessageContent($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr, $dataFollowUpRemark);
        
        $teacherAssoAry = array();
        if($pushmessagenotify_PIC)
        {
            $target_PIC = explode(",", $datainfo[0]['PICID']);
            if(isset($target_PIC) && sizeof($target_PIC) != 0) {
                foreach($target_PIC as $tPICID) {
                    $teacherAssoAry[$tPICID] = array($tPICID);
                }
            }
        }
        if($pushmessagenotify_DisciplineAdmin)
        {
            $DisciplineAdmin = $ldiscipline->GET_ADMIN_USER();
            $DisciplineAdmin = explode(',', $DisciplineAdmin);
            if(isset($DisciplineAdmin) && sizeof($DisciplineAdmin) != 0) {
                foreach($DisciplineAdmin as $DisAdminID) {
                    $teacherAssoAry[$DisAdminID] = array($DisAdminID);
                }
            }
        }
        $individualMessageInfoAry = array();
        $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
        
        if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
            $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode='', $sendTimeString='', '', '', 'DisciplineDemerit', $RecordID);
        } else {
            $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode='', $sendTimeString='', '', '', 'DisciplineDemerit', $RecordID);
        }
    }
    
    if($pushmessagenotify_ClassTeacher)
    {
        foreach($student_class_ary_msg as $classname => $stu_ary_msg)
        {
            $student_list = implode(",", $stu_ary_msg);

            // [2020-0824-1151-40308]
            //list($pushmessage_subject, $pushmessage_body) = $ldiscipline->returnAwardPunishmentPushMessageContent($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr);
            list($pushmessage_subject, $pushmessage_body) = $ldiscipline->returnAwardPunishmentPushMessageContent($datainfo[0]['MeritType'], $student_list, $CatInfo['CategoryName'], $datainfo[0]['ItemText'], $datainfo[0]['RecordDate'], $receive, $datainfo[0]['ConductScoreChange'], $dataRemark, $dataRecordStatusStr, $dataSubScoreArr, $dataFollowUpRemark);
            
            $teacherAssoAry = array();
            $ClassTeacherID = $lclass->returnClassTeacherID($classname);
            foreach($ClassTeacherID as $cTeacher)
            {
                $teacherAssoAry[$cTeacher['UserID']] = $cTeacher['UserID'];
            }
            $individualMessageInfoAry = array();
            $individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $teacherAssoAry;
            
            if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
                $notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode='', $sendTimeString='', '', '', 'DisciplineDemerit', $RecordID);
            } else {
                $notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $pushmessage_subject, $pushmessage_body, $isPublic, $recordStatus=1, $appType_t, $sendTimeMode='', $sendTimeString='', '', '', 'DisciplineDemerit', $RecordID);
            }
        }
    }
}

header("Location: index.php?msg=add");

intranet_closedb();
?>