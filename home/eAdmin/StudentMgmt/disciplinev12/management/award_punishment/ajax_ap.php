<?
##### Change Log [Start] #####
#
#	Date	:	2015-07-06	Omas
# 				Updated UI 
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

if($task=='ItemCode')
{
		
	$sql = "SELECT i.ItemName, i.ItemCode, c.CategoryName as Cat FROM DISCIPLINE_MERIT_ITEM as i 
			inner join DISCIPLINE_MERIT_ITEM_CATEGORY c on i.CatID = c.CatID
			WHERE (i.RecordStatus IS NULL OR i.RecordStatus=1) AND (c.RecordStatus IS NULL OR c.RecordStatus=1)
			";

	$CatArr = $ldisciplinev12->returnArray($sql);
	
	$data = '<table width="100%" cellpadding="3" cellspacing="0" class="common_table_list_v30 view_table_list_v30">';
	$data .= '<thead>';
		$data .='<tr>
									<th>#</th>
									<th>'.$i_Discipline_System_Discipline_Case_Record_Category.'</th>
									<th>'.$i_Discipline_System_ItemCode.'</th>
									<th>'.$i_Discipline_System_ItemName.'</th></tr>';
	$data .= '</thead>';
	$data .= '<tbody>';
		for($a=0;$a<sizeof($CatArr);$a++)
		{
			$Category = intranet_htmlspecialchars($CatArr[$a]['Cat']);
			$ItemCode = intranet_htmlspecialchars($CatArr[$a]['ItemCode']);
			$ItemName = intranet_htmlspecialchars($CatArr[$a]['ItemName']);
			
			if($ItemName!='' && $ItemCode!='')
			{
				$data .='<tr>
					 <td class="tablerow1">'.($a+1).'</td>
					 <td class="tablerow1">'.$Category.'</td>
					 <td class="tablerow1">'.$ItemCode.'</td>
					 <td class="tablerow1">'.$ItemName.'</td>
					</tr>';
			}
		}
	$data .= '</tbody>';
	$data .="</table>";	
}
else if($task=='SubjCode')
{
	# homework
	$use_intranet_homework = $ldisciplinev12->accumulativeUseIntranetSubjectList();

	# intranet homework list
	//if($use_intranet_homework)
	{
		//$sql = "SELECT CONCAT('SUBJ',SubjectID) as Itemcode, SubjectName FROM INTRANET_SUBJECT WHERE RecordStatus = 1 ";
		$sql = "SELECT CONCAT('SUBJ', RecordID) as Itemcode, ".Get_Lang_Selection('CH_DES','EN_DES')." as SubjectName FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1";
		$homework_list = $ldisciplinev12->returnArray($sql);
	}
	/*else
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID=2 AND RecordStatus <> ".DISCIPLINE_DELETED_RECORD;	
		$homework_list = $ldisciplinev12->returnArray($sql);
	}*/
	
		
	$data = '<table width="100%" border="0"  cellpadding="3" cellspacing="0" class="common_table_list_v30 view_table_list_v30">';
	$data .= '<thead>';
		$data .='<tr class="tabletop">
									<th align="center" valign="middle">#</th>
									<th>'.$Lang['SysMgr']['Homework']['SubjectCode'].'</th>
									<th>'.$i_Discipline_System_ItemName.'</th>
								</tr>';
	$data .= '</thead>';
	$data .= '<tbody>';
		for($a=0;$a<sizeof($homework_list);$a++)
		{
			$ItemCode = $homework_list[$a]['Itemcode'];
			$ItemName = $homework_list[$a]['SubjectName'];
			if($ItemName!='' && $ItemCode!='')
			{
				$data .='<tr>
					 <td>'.($a+1).'</td>
					 <td>'.$ItemCode.'</td>
					 <td>'.$ItemName.'</td>
					 </tr>';
			}
		}
	$data .= '</tbody>';
	$data .="</table>";	
}
else if($task=='MeritCode')
{
	include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
	$lsp = new libstudentprofile();
	
	if (!$lsp->is_merit_disabled)
	{
		$html= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">1</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_Merit</td></tr>";
	}
	if (!$lsp->is_min_merit_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">2</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_MinorCredit</td></tr>";
	}
	if (!$lsp->is_maj_merit_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">3</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_MajorCredit</td></tr>";
	}
	if (!$lsp->is_sup_merit_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">4</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_SuperCredit</td></tr>";
	}
	if (!$lsp->is_ult_merit_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">5</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_UltraCredit</td></tr>";
	}
	if (!$lsp->is_warning_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">0</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_Warning</td></tr>";
	}
	if (!$lsp->is_black_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">-1</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_BlackMark</td></tr>";
	}
	if (!$lsp->is_min_demer_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">-2</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_MinorDemerit</td></tr>";
	}
	if (!$lsp->is_maj_demer_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">-3</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_MajorDemerit</td></tr>";
	}
	if (!$lsp->is_sup_demer_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">-4</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_SuperDemerit</td></tr>";
	}
	if (!$lsp->is_ult_demer_disabled)
	{
		$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">-5</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_Merit_UltraDemerit</td></tr>";
	}
	$html.= "<tr class=\"tablerow1\"><td style=\"border-bottom:1pt solid #D8D8D8;\">-999</td><td style=\"border-bottom:1pt solid #D8D8D8;\">$i_general_na</td></tr>";
	
		
	$data = '<table width="100%" border="0" cellpadding="3" cellspacing="0" class="common_table_list_v30 view_table_list_v30" >';
	$data .= '<thead>';
		$data .='<tr>
									<th align="left" valign="middle">'.$i_SAMS_code.'</th>
									<th align="left" valign="middle">'.$iDiscipline['MeritType'].'</th>
								</tr>';
	  						
	$data .= '</thead>';
	$data .= '<tbody>';
	$data .= $html;
	$data .= '</tbody>';	
	$data .="</table>";	
}
$output = '
		<table>
		
			  <tr>
				 <td align="right"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" name="pre_close1"  border="0" id="pre_close1"></a></td>
				 			
			  </tr>
			  <tr>
			 <td>
				<table align="left" width="540" border="0" cellspacing="0" cellpadding="0">
				<tr>
				<td align="center">
				'.$data.'						
				</td>
				 </tr>
				</table>
			</td>
			</tr>
						     		 
        </table>
                ';
//echo convert2unicode($output,1);
echo $output;
?>
