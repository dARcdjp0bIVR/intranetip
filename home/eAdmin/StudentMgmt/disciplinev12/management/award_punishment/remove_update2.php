<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn")))
{
		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$RecordIDAry = array();

if(is_array($RecordID))
	$RecordIDAry = $RecordID;
else
	$RecordIDAry[] = $RecordID;
	
foreach($RecordIDAry as $MeritRecordID)
{
	if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll") || (($ldiscipline->isMeritRecordPIC($MeritRecordID) || $ldiscipline->isMeritRecordOwn($MeritRecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn")))
	{
		#################################################################################
		# DELETE_MERIT_RECORD include:
		# - delete [PROFILE_STUDENT_MERIT]
		# - delete [DISCIPLINE_MERIT_RECORD] 
		# - if preious status is APPROVED, then update the conduct balance & sub score
		# - (if necessary) delete all detentino records [DISCIPLINE_DETENTION_STUDENT_SESSION] DemeritID = MeritRecordID
		# - (if necessary) delete enotice
		#################################################################################
		$ldiscipline->DELETE_MERIT_RECORD($MeritRecordID);
	}
}

$msg = "delete";
$return_link = "index.php?SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&msg=$msg&waived=$waived&approved=$approved&waitApproval=$waitApproval&released=$released&rejected=$rejected&pic=$pic&fromPPC=$fromPPC&passedActionDueDate=$passedActionDueDate";

header("Location: $return_link");

intranet_closedb();
?>