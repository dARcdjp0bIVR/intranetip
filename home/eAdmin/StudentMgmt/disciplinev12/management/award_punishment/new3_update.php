<?php
// Modifying by : 

########## Change Log ###############
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose) (replace "U")
#
#   Date    :   2019-05-01 (Bill)
#               prevent SQL Injection + Cross-site Scripting
#
#   Date    :   2019-03-15  (Bill)  [2018-0710-1351-39240]
#               auto send discipline notice for demerit records ($sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'])
#
#   Date    :   2018-06-01  (Bill)  [2018-0321-1449-22206]
#               Fixed: cannot upload attachment when "Finish Now" from step 2
#
#	Date	:	2017-11-01	(Bill)	[2017-0403-1552-54240]
#				HKUGA Pastoral email
#				- Get AP Term Count
#				- Send to HOY Group Member for each 6 times (Award / Punishment)
#
#	Date	:	2017-04-05	(Bill)
#				auto convert Conudct Score depends on Merit Type and Number
#				($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore'] and $sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings'])
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				support Activity Score update
#
#	Date	:	2016-06-07	(Bill)	[2016-0329-1757-08164]
#	Detail	:	$sys_custom['eDiscipline']['CSCProbation']
#				- add hidden input fields for probation setting
#				- approve all punishments in probation if current record is punishment
#
#	Date	:	2016-06-01	(Henry HM)	[2016-0524-1502-24235]
#				Change email content for HKUGA Cust
#				- Unescaped the email content ($sys_custom['eDiscipline']['add_AP_records_send_mail_to_classteacher'])
#
#	Date	:	2015-10-26	(Bill)	[2015-0611-1642-26164]
#				Change logic for CWC Probation Scheme
#				- Auto Approve AP records
#				- Approve probation records if current record is punishment
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				Added logic for HKUGA Cust
#				- AP records added by Student Prefect always in Pending ($sys_custom['eDiscipline']['add_AP_records_student_prefect'])
#				- Auto send email to class teacher ($sys_custom['eDiscipline']['add_AP_records_send_mail_to_classteacher'])
#
#	Date	:	2015-07-23	(Bill)	[2015-0611-1642-26164]
#				Added logic for CWC Probation Scheme
#				- Same semester 
#				- 1st Punishment -> Probation (Pending) 
#				- Other Punishments -> Approve current and existing records 
#
#	Date	:	2015-04-30	Bill	[2014-1216-1347-28164]
#				send push message to parent when $eClassAppNotify = 1
#				update DB after send out push message
#
#	Date	:	2013-11-07 (Carlos) - $sys_custom['eDiscipline']['yy3'] ignore conduct score checking for admin user
#
#	Date	:	2013-11-05 (YatWoon)
#				Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-09-04 (YatWoon)
#				fixed: fail to auto approve if user is admin and no need for approval [Case#2013-0904-0922-44140]
#		
#	Date	:	2013-06-21 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] - mark merit item as overflowed and ignore changing conduct mark balance
#												  - add checking of approval group to auto approval 
#												  - add email alert for yy3 cust
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 )
#
#	Date	:	2011-01-13 (Henry Chow)
#	Detail 	:	add hidden field "detentionID" in order to handle the detention ID from "Detention > Student List"
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#	Date:	2010-05-07	YatWoon
#			set "Approve by" as auto (-1) if auto approve by system
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

### Handle SQL Injection + XSS [START]
if(is_array($student) && sizeof($student) > 0) {
    foreach($student as $thisKey => $thisStudentID) {
        $student[$thisKey] = IntegerSafe($thisStudentID);
    }
}

if(!intranet_validateDate($RecordDate)) {
    $RecordDate = date('Y-m-d');
}
if(!intranet_validateDate($RecordInputDate)) {
    $RecordInputDate = '';
}
if(!intranet_validateDate($ActionDueDate)) {
    $ActionDueDate = '';
}
if (is_array($PIC) && sizeof($PIC) > 0) {
    foreach($PIC as $thisKey => $thisPICID) {
		$thisPICID = str_replace("U", "", $thisPICID);
        $PIC[$thisKey] = IntegerSafe($thisPICID);
    }
}
$sessionTime = cleanCrossSiteScriptingCode($sessionTime);

$record_type = IntegerSafe($record_type);
$CatID = IntegerSafe($CatID);
$ItemID = IntegerSafe($ItemID);
if($ldiscipline->AP_Interval_Value) {
    $MeritNum = (float)$MeritNum;
}
else {
    $MeritNum = IntegerSafe($MeritNum);
}
$MeritType = IntegerSafe($MeritType);
if($sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {
    $ConductScore = (float)$ConductScore;
}
else {
    $ConductScore = IntegerSafe($ConductScore);
}
$StudyScore = IntegerSafe($StudyScore);
$ActivityScore = IntegerSafe($ActivityScore);
$semester = cleanCrossSiteScriptingCode($semester);
// $Subject = IntegerSafe($Subject);
$SubjectID = IntegerSafe($SubjectID);
// $TargetTagType = cleanCrossSiteScriptingCode($TargetTagType);
$detentionID = IntegerSafe($detentionID);

$applyProbation = IntegerSafe($applyProbation);
// $maxStudyScore = IntegerSafe($maxStudyScore);

if($detentionFlag != 'YES' && $detentionFlag != 'NO' && $detentionFlag != '') {
    $detentionFlag = '';
}
if($noticeFlag != 'YES' && $noticeFlag != 'NO' && $noticeFlag != '') {
    $noticeFlag = '';
}
### Handle SQL Injection + XSS [END]

$lnotice = new libnotice();

$ls = new libfilesystem();

// [2014-1216-1347-28164]
$canSendPushMsg = false;
if($plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage']){
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
	$leClassApp = new libeClassApp();
	$canSendPushMsg = true;
}

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

if(sizeof($student) <= 0) {
	header("Location: index.php");	
}

# (START) Check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)
$isExist = array();
$hiddenField = "";
$success = 1;

# Not allow same student with same item in a same day
if($ldiscipline->NotAllowSameItemInSameDay) {
	foreach($student as $StudentID) {
		$thisItemID = $ItemID;
		$thisRecordDate = $RecordDate;
		$isExist[$StudentID] = $ldiscipline->checkSameAPItemInSameDay($StudentID, $thisItemID, $thisRecordDate);
		if($isExist[$StudentID]==1) $success = 0;
	}
}

# Existing record of same item on same day (same student)
if($success==0)
{
	$hiddenField = "";
	
	echo "<body onload='document.form5.submit()'>";
	echo "<form name='form5' method='post' action='new2.php'>";
	foreach($student as $StudentID) {
		echo '<input type="hidden" name="student[]" id="student[]" value="'.$StudentID.'" />';
	} 	
	foreach($isExist as $key=>$val) {
		if($val==1)
			$hiddenField .= "<input type='hidden' name='sid[]' id='sid[]' value='$key'>\n";	
	}
	?>
	<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
	<input type="hidden" name="RecordInputDate" id="RecordInputDate" value="<?=$RecordInputDate?>" />
	<input type="hidden" name="ActionDueDate" id="ActionDueDate" value="<?=$ActionDueDate?>" />
		<? if(isset($picType) && $picType==2) { ?>
			<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$picName?>">
		<? }
		else { ?>
			<? foreach($PIC as $k=>$d) { ?>
				<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
			<? } ?>
		<? } ?>
	<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
    <? if($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
        <input type="hidden" name="followUpRemark" id="followUpRemark" value="<?=$followUpRemark?>" />
    <? } ?>
	<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
	<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
	<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
	<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
	<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
	<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
	<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
	<input type="hidden" name="ActivityScore" id="ActivityScore" value="<?=$ActivityScore?>" />
	<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
	<input type="hidden" name="picType" id="picType" value="<?=$picType?>" />
	<input type="hidden" name="picName" id="picName" value="<?=$picName?>" />
	<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />
	<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
	<input type="hidden" name="detentionID" id="detentionID" value="<?=$detentionID?>" />
	
	<? if($sys_custom['eDiscipline']['CSCProbation']) {?>
		<input type="hidden" name="applyProbation" id="applyProbation" value="<?=$applyProbation?>" />
	<? } ?>
	
	<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) 
	{
		$file_number = intranet_htmlspecialchars($file_number);
		$record_number = intranet_htmlspecialchars($record_number);
	?>
	<input type="hidden" name="file_number" id="file_number" value="<?=$file_number?>" />
	<input type="hidden" name="record_number" id="record_number" value="<?=$record_number?>" />
	<? } ?>
	
	<!-- Step 3 data //-->
	<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
	<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
	<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
	<input type="hidden" name="layername" id="layername" value="" />
	<input type="hidden" name="formValid" id="formValid" value="" />
	<?=$hiddenField?>
	
	<script language="javascript">
	<!--
		document.form5.submit();
	-->
	</script>
	</form>
	</body>
<?
exit;
}
# (END) Check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)

$lteaching = new libteaching();
$lc = new libucc();

### Retrieve extra data
# School Year
//$school_year = GET_ACADEMIC_YEAR_WITH_FORMAT($RecordDate);
$school_year = GET_ACADEMIC_YEAR3($RecordDate);
$this_year_id = $ldiscipline->getAcademicYearIDByYearName($school_year);

# Semester
$semInfo = getAcademicYearAndYearTermByDate($RecordDate);
$semester = $semInfo[1];

# Item Text
$ItemInfo = $ldiscipline->returnMeritItemByID($ItemID);
$ItemText = $ItemInfo['ItemName'];
$ItemText = addslashes($ItemText);

# Conduct Score
$ConductScoreChange = $ConductScore * $record_type;
if($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore'] && !empty($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings']) && $record_type=="-1")
{
	// Auto Convert Conudct Score depends on Merit Type and Number 
	$MeritTypeConductScore = $sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings'][$MeritType];
	if($MeritTypeConductScore > 0 && $MeritNum > 0) {
		$ConductScoreChange = $MeritTypeConductScore * $MeritNum * $record_type;
	}
	else {
		$ConductScoreChange = 0;
	}
}

# SubScoreChange
$StudyScoreChange = $StudyScore * $record_type;

# SubScoreChange2
$ActivityScoreChange = $ActivityScore * $record_type;

# Subject
$SubjectName = $lteaching->returnSubjectName($SubjectID);

# PIC
// [2015-0416-1040-06164] get PIC name
if($sys_custom['eDiscipline']['add_AP_records_send_mail_to_classteacher'])
{
	$PIC_Name_List = "";
	$delim = "";
	foreach((array)$PIC as $picIndex => $curPICID)
	{
		$lpic = new libuser($curPICID);
		$thisPICEnglishName = $lpic->EnglishName;
		$PIC_Name_List .= $delim.$thisPICEnglishName;
		$delim = ", ";
	}
	$lpic = "";
}
$PIC = implode(",",$PIC);

# Remark
$remark = intranet_htmlspecialchars($remark);

# [2020-0824-1151-40308] Follow-up Remark
$followUpRemark = stripslashes(intranet_htmlspecialchars($followUpRemark));

$MeritRecordIDAry = array();
$detentionAry = array();

# Records comes from "Detention > Student List"
if($detentionID!="") {
	$sql = "select StudentID, RecordID from DISCIPLINE_DETENTION_STUDENT_SESSION where RecordID in ($detentionID) and (DemeritID='' or DemeritID is NULL)";
	$tempData = $ldiscipline->returnArray($sql);
	for($i=0; $i<sizeof($tempData); $i++) {
		list($sid, $rid) = $tempData[$i];
		$detentionAry[$sid] = $rid;	
	}
}

# Get submitted data for eNotice
for ($j=1; $j<=sizeof($student); $j++) {
    ${"submitSelectNotice".$j} = $_POST["SelectNotice".$j];
	${"submitTextAdditionalInfo".$j} = stripslashes($_POST["TextAdditionalInfo".$j]);
	${"submitStudentID".$j} = $_POST["StudentID".$j];
	${"submitStudentName".$j} = $_POST["StudentName".$j];
	
	${"submitSelectNotice".$j} = IntegerSafe(${"submitSelectNotice".$j});
	${"submitStudentID".$j} = IntegerSafe(${"submitStudentID".$j});
}

$UserName = $ldiscipline->getUserNameByID($UserID);

if ($ldiscipline->UseSubScore) {
    $subscore_1_record = $ldiscipline->retrieveSubscoreBalance($record_type, $student, $school_year, $semester);
}

if ($ldiscipline->UseActScore) {
    $subscore_2_record = $ldiscipline->retrieveActscoreBalance($record_type, $student, $school_year, $semester);
}

// [2018-0321-1449-22206] support upload attachment when "Finish Now" from step 2
if($stop_step2 && sizeof($Attachment) > 0) {
    foreach($student as $k=>$StudentID) {
        # copy attachment
        $originalPath = "$file_path/file/disciplinev12/award_punishment/".$sessionTime;
        $path = "$file_path/file/disciplinev12/award_punishment/".$sessionTime."_".$StudentID."tmp";
        //echo $originalPath.'<br>'.$path.'<br>';
        
        if (is_dir($originalPath) && !is_dir($path))
        {
            $result = $ls->folder_new($path);
            //echo "<ul>#$result<br>$originalPath<br>$path";
            chmod($path, 0777);
        }
        
        //echo $path;
        $a = new libfiletable("", $originalPath, 0, 0, "");
        $files = $a->files;
        
        while (list($key, $value) = each($files)) {
            $file = "$file_path/file/disciplinev12/award_punishment/".$sessionTime."/".$files[$key][0];
            $newfile = "$file_path/file/disciplinev12/award_punishment/".$sessionTime."_".$StudentID."tmp/".$files[$key][0];
            //echo '<li>'.$file.'<br>'.$newfile;
            if(copy($file, $newfile)) {
                chmod("$file_path/file/disciplinev12/award_punishment/".$sessionTime."_".$StudentID."tmp/".$files[$key][0], 0777);
            }
        }
    }
}

if(isset($sessionTime) && $sessionTime!="") {
	$ldiscipline->deleteDirectory("$file_path/file/disciplinev12/award_punishment/".$sessionTime."/");
}

if($sys_custom['eDiscipline']['yy3']){
	$overflowMarkInfoAry = $ldiscipline->GetMeritItemTagConductOverflowScoreStudents($student, $semInfo['AcademicYearID'], $semInfo['YearTermID'], $ItemID, $record_type, $ConductScore);
}

// [2015-0416-1040-06164]
$student_class_ary = array();
foreach($student as $k => $StudentID)
{
	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName;

	### INSERT INTO [DISCIPLINE_MERIT_RECORD] (Status = Pending)
	$dataAry = array();
	$dataAry['RecordDate'] = $RecordDate;
	$dataAry['RecordInputDate'] = $RecordInputDate ? $RecordInputDate : date("Y-m-d");
	$dataAry['ActionDueDate'] = $ActionDueDate;
	$dataAry['Year'] = $school_year;
	$dataAry['Semester'] = addslashes($semester);
	$dataAry['StudentID'] = $StudentID;
	$dataAry['ItemID'] = $ItemID;
	$dataAry['ItemText'] = $ItemText;
	$dataAry['MeritType'] = $record_type;
	$dataAry['Remark'] = $remark;
    // [2020-0824-1151-40308]
    if($sys_custom['eDiscipline']['APFollowUpRemarks']) {
        $dataAry['FollowUpRemark'] = $followUpRemark;
    }
	$dataAry['ProfileMeritType'] = $MeritType;
	$dataAry['ProfileMeritCount'] = $MeritNum;
	$dataAry['ConductScoreChange'] = $ConductScoreChange;
	$dataAry['SubScore1Change'] = $StudyScoreChange;
	$dataAry['SubScore2Change'] = $ActivityScoreChange;
	$dataAry['Subject'] = $SubjectName;
	$dataAry['SubjectID'] = $SubjectID;
	$dataAry['PICID'] = $PIC;
	$dataAry['RecordStatus'] = DISCIPLINE_STATUS_PENDING;
	$dataAry['Attachment'] = $sessionTime."_".$StudentID."tmp";
	$dataAry['AcademicYearID'] = $ldiscipline->getAcademicYearIDByYearName($school_year);
	$dataAry['YearTermID'] = $semInfo[0];	
	
	if($sys_custom['eDiscipline']['yy3'] && !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']))
	{
		$notifyAdmin = false;
		if(in_array($StudentID,$overflowMarkInfoAry['MeritScoreOverflowStudentID'])){
			$dataAry['OverflowMeritItemScore'] = '1';
			$dataAry['ConductScoreChange'] = 0;
			$dataAry['ConductScoreFakeChange'] = $ConductScoreChange;
			$notifyAdmin = true;
		}
		if(in_array($StudentID,$overflowMarkInfoAry['ConductScoreOverflowStudentID'])){
			$dataAry['OverflowConductMark'] = '1';
			//$dataAry['ConductScoreAfter'] = $overflowMarkInfoAry['ConductScoreOverflowDetail'][$StudentID]['MarkAfterChange'];
			$dataAry['ConductScoreChange'] = 0;
			$dataAry['ConductScoreFakeChange'] = $ConductScoreChange;
			$notifyAdmin = true;
		}
		if(in_array($StudentID,$overflowMarkInfoAry['TagScoreOverflowStudentID'])){
			$dataAry['OverflowTagScore'] = '1';
			$dataAry['ConductScoreChange'] = 0;
			$dataAry['ConductScoreFakeChange'] = $ConductScoreChange;
			$notifyAdmin = true;
		}
	}
	
	if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) 
	{ 
		$file_number = intranet_htmlspecialchars($file_number);
		$record_number = intranet_htmlspecialchars($record_number);
		$dataAry['FileNumber'] = $file_number;
		$dataAry['RecordNumber'] = $record_number;
	}
	
	# Save eNotice
	if (!$lnotice->disabled)
	{
		//echo $noticeFlag;
		if ($noticeFlag == "YES") {
			//echo 2;
			for ($i=1; $i<=sizeof($student); $i++) {
				//echo $student[$i];
				if ($StudentID == ${"submitStudentID".$i}) {
					$dataAry['TemplateID'] = ${"submitSelectNotice".$i};
					$dataAry['TemplateOtherInfo'] = ${"submitTextAdditionalInfo".$i};
					$emailNotify = ${"emailNotify".$i};
					break;
				}
			}
		}
		
		# [2018-0710-1351-39240] auto set notice template for demerit records (with conduct score)
		if($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore'] && isset($sys_custom['eDiscipline']['AutoConvertMeritTypeToConductScore_Settings'][$MeritType]))
		{
            if($record_type == "-1" && $MeritNum > 0 && $sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'])
		    {
    		    $dataAry['TemplateID'] = $sys_custom['eDiscipline']['AutoSendDemeritNoticeTemplateID'];
    		    $dataAry['TemplateOtherInfo'] = '';
    		    $emailNotify = 0;
		    }
		}
	}
	
	# eClass App [2014-1216-1347-28164]
	if($canSendPushMsg)
	{
		if ($noticeFlag == "YES") {
			for ($i=1; $i<=sizeof($student); $i++) {
				//echo $student[$i];
				if ($StudentID == ${"submitStudentID".$i}) {
					$eClassAppNotify = ${"eClassAppNotify".$i};
					$eClassAppTemplate = ${"submitSelectNotice".$i};
					$eClassAppAddInfo = ${"submitTextAdditionalInfo".$i};
					break;
				}
			}
		}
	}
	
	$MeritRecordID = $ldiscipline->INSERT_MERIT_RECORD($dataAry);
	if($MeritRecordID)
	{
		$MeritRecordIDAry[] = $MeritRecordID;
		
		# Update Detention record if records from "Detention > Student List"
		if(isset($detentionAry[$StudentID]) && $detentionAry[$StudentID]!="") {
			$sql = "UPDATE DISCIPLINE_DETENTION_STUDENT_SESSION SET DemeritID='$MeritRecordID' WHERE RecordID='".$detentionAry[$StudentID]."'";	
			$ldiscipline->db_db_query($sql);
			//echo $sql.'<br>';
		}
		
		# Detention
		if ($detentionFlag == "YES")
		{
			$Item = $ldiscipline->returnMeritItemByID($ItemID);
			$Reason = $Item['ItemCode']." - ".$Item['ItemName'];
			for ($i=0; $i<sizeof($DetentionArr); $i=$i+3) {
				// array sequence: StudentID, DetentionID, Remark
				if ($StudentID == $DetentionArr[$i]) {
					$detentionDataArr = array();
					$detentionDataArr['StudentID'] = "'".$DetentionArr[$i]."'";
					if ($DetentionArr[$i+1] == "") $DetentionArr[$i+1] = "NULL";
					$detentionDataArr['DetentionID'] = $DetentionArr[$i+1];
					$detentionDataArr['Remark'] = "'".stripslashes($DetentionArr[$i+2])."'";
					$detentionDataArr['DemeritID'] = "'".$MeritRecordID."'";
					$detentionDataArr['RequestedBy'] = "'".$UserID."'";
					$detentionDataArr['Reason'] = "'".intranet_htmlspecialchars(addslashes($Reason))."'";
					if ($DetentionArr[$i+1] != "NULL") {
						$detentionDataArr['ArrangedBy'] = "'".$UserID."'";
						$detentionDataArr['ArrangedDate'] = 'NOW()';
					}
					$detStuRecordID = $ldiscipline->insertDetention($detentionDataArr);
				}
			}
		}
		
		# Customization
		if($sys_custom['eDiscipline']['yy3'] && $notifyAdmin){
			## Check if Can Send Exceed Max Gain/ Deduct Mark email to admin
			$ldiscipline->SEND_EXCEED_MAX_GAIN_DEDUCT_MARK_EMAIL($MeritRecordID);
			continue; // skip approval
		}
		
		// [2015-0611-1642-26164] CWC Probation Scheme
		//echo $ldiscipline->AutoApproveAPRecord.'/';
		if($sys_custom['eDiscipline']['CSCProbation']){
//			// get probation details
//			list($approve_probation, $probation_records) = $ldiscipline->CHECK_STUDENT_CWC_PROBATION_RECORD($StudentID, $semInfo[0], $MeritRecordID);
//
//			// with probation or approved punishment records
//			if($record_type==-1 && $approve_probation){
//				// approve current ap record
//				if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
//				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1, $emailNotify);
//				
//				// approve existing probation ap records
//				foreach((array)$probation_records as $ap_record_id => $ap_record_info){
//					$ldiscipline->APPROVE_MERIT_RECORD($ap_record_id);
//				}
//			}
//			// award records
//			else if($record_type!=-1){
//				// approve current ap record
//				if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
//				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1, $emailNotify);
//			}
			
			// [2016-0329-1757-08164] Check if student with any probation records
			$approve_probation = false;
			if($record_type==-1)
			{
				// Get probation details
				list($approve_probation, $probation_records) = $ldiscipline->CHECK_STUDENT_CWC_PROBATION_RECORD($StudentID, $semInfo[0], $MeritRecordID);
			}
			
			// Approve current AP record if current record not apply Probation OR current student with Probation record
			if(!$applyProbation || $approve_probation)
			{
				if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1, $emailNotify, 1);
			}
	
			// Approve existing Probation records if current record is Punishment
			if($record_type==-1 && $approve_probation){
				foreach((array)$probation_records as $ap_record_id => $ap_record_info){
					$ldiscipline->APPROVE_MERIT_RECORD($ap_record_id, 1, 1, 1);
				}
			}
		}
		// [2015-0416-1040-06164] AP records status added by Student Prefect always pending
		else if($sys_custom['eDiscipline']['add_AP_records_student_prefect'] && $_SESSION['UserType']==USERTYPE_STUDENT){
			// do nothing 
		}
		// Normal Situation
		else if($ldiscipline->AutoApproveAPRecord) {
			# Check need approve or not
			$need_approval = $ldiscipline->CHECK_APPROVAL_REQUIRED($MeritType,$MeritNum);
			//echo $need_approval.'/';
			
			$can_approve = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($MeritRecordID);
			if($need_approval && ($can_approve || $ldiscipline->IS_ADMIN_USER($_SESSION['UserID']))) {
				# Set APPROVE
				if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1, $emailNotify);
			}
			//else if((!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval")) && $need_approval)
			//{
				# can do nothing
			//}
			else if(($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval")) && !$need_approval)
			{
				# Set APPROVE
				if(!isset($emailNotify) || $emailNotify=="") $emailNotify = 0;
				$ldiscipline->APPROVE_MERIT_RECORD($MeritRecordID, 1, $emailNotify);
			}
		}
		
		// [2014-1216-1347-28164] Send Push Message
		if($canSendPushMsg && $record_type==-1 && $eClassAppNotify){
			$messageID = $ldiscipline->SEND_DETENTION_APP_NOTIFICATION($MeritRecordID, $eClassAppTemplate, $eClassAppAddInfo);
			if($messageID){
				$sql = "UPDATE DISCIPLINE_MERIT_RECORD SET PushMessageID='$messageID' WHERE RecordID='$MeritRecordID'";	
				$ldiscipline->db_db_query($sql);
			}
		}
		
		// [2015-0416-1040-06164] Build Mail Content required for HKUGA cust
		if($sys_custom['eDiscipline']['add_AP_records_send_mail_to_classteacher']){
			$StudentNameEN = $lu->EnglishName;
			$StudentNameDisplay = $StudentNameEN." (".$thisClassName."-".$thisClassNumber.")";
			$StudentClassNameNum = $thisClassName."-".$thisClassNumber;
			$student_class_ary[$thisClassName][] = array($StudentNameDisplay, $StudentClassNameNum, $StudentID, $record_type);
		}
	}
}

# [2015-0416-1040-06164] Send Mail to Class Teachers for HKUGA cust
if($sys_custom['eDiscipline']['add_AP_records_send_mail_to_classteacher'])
{
	include_once($PATH_WRT_ROOT."includes/libclass.php");
    include_once($PATH_WRT_ROOT."includes/libwebmail.php");
	include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
	include_once($PATH_WRT_ROOT."lang/email.php");
	
	$lwebmail = new libwebmail();
	$lcampusmail = new libcampusmail();
	
	$lclass = new libclass();
	//$senderID = $UserID;
	
	// AP Item Info
	$CategoryID = $ldiscipline->returnCatIDByItemID($ItemID);
	$CatInfo = $ldiscipline->returnMeritItemCategoryByID($CategoryID);
	
	// AP Count (Term based)
	$studentAPCount = $ldiscipline->getStudentMeritDemeritTotal($student, $this_year_id, $semInfo[0]);
	
	// AP Count (Term based) - for HOY Checking
	$studentAPCount_HOY = $ldiscipline->getStudentMeritDemeritTotal($student, $this_year_id, $semInfo[0], $forApprovedRecords=false);
	
	// HOY Group Member
	$HOYMember = $ldiscipline->GET_HOY_GROUP_MEMBER();
	
	// loop Class
	foreach((array)$student_class_ary as $classname => $stu_ary)
	{
		// Get Receiver (Class Teacher)
		$receiver = array();
		$ClassTeacherID = $lclass->returnClassTeacherID($classname);
		foreach((array)$ClassTeacherID as $k=>$d)
		{
			$receiver[] = $d['UserID'];
		}
		$receiver = array_unique($receiver);
		
		// Send email for each students
		foreach((array)$stu_ary as $stu_index => $cur_student)
		{
			list($studentName, $ClassNameNum, $thisStudentID, $thisRecordType) = $cur_student;
			
			// Get Student Term AP Count
			$postiveNum = $studentAPCount[$thisStudentID][1];
			$negativeNum = $studentAPCount[$thisStudentID][-1];
			
			// Add Receiver (HOY if required)
			$postiveNum_HOY = $studentAPCount_HOY[$thisStudentID][1];
			$negativeNum_HOY = $studentAPCount_HOY[$thisStudentID][-1];
			if(($thisRecordType == 1 && $postiveNum_HOY > 0 && ($postiveNum_HOY % 6 == 0)) || ($thisRecordType == -1 && $negativeNum_HOY > 0 && ($negativeNum_HOY % 6 == 0))) {
				$receiver = array_merge((array)$receiver, (array)$HOYMember);
				$receiver = array_unique($receiver);
			}
			
			// Format Count Number (1 - 9)
			if($postiveNum > 0 && $postiveNum < 10) {
				$postiveNum = "0".$postiveNum;
			}
			if($negativeNum > 0 && $negativeNum < 10) {
				$negativeNum = "0".$negativeNum;
			}
			
			// Get Email Subject and Content
			list($subject, $content) = $ldiscipline->getAutoGenEmail($receiver, $studentName, $ClassNameNum, $RecordDate, $record_type, $ItemText, $PIC_Name_List, $remark, $postiveNum, $negativeNum);
			$exmail_success = $lwebmail->sendModuleMail($receiver, $subject, $content, 1);
		}
	}
}

intranet_closedb();

if($stop_step2)
{
	header("Location: index.php?msg=add");
}
else if(sizeof($MeritRecordIDAry))
{
?>
	<body onLoad="document.form1.submit();">
	<form name="form1" method="post" action="new4.php">
	<? foreach($MeritRecordIDAry as $d) {?>
		<input type="hidden" name="MeritRecordID[]" value="<?=$d?>">
	<? } ?>
	</form>
	</body>
<?
}
?>