<?php
# using: 

############### Change Log Start ################
#
#   Date    :   2019-05-01 (Bill)
#   Detail	:	prevent Cross-site Scripting
#
#	Date	:	2013-06-21 (Carlos)
#	Detail	:	$sys_custom['eDiscipline']['yy3'] - Display conduct score change with masked symbol for those overflow score merit items
#
#################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$RecordID = IntegerSafe($RecordID);

$SchoolYear = IntegerSafe($SchoolYear);
if($semester != '' && $semester != 'WholeYear') {
    $semester = IntegerSafe($semester);
}
$targetClass = cleanCrossSiteScriptingCode($targetClass);

$s = cleanCrossSiteScriptingCode($s);
$clickID = IntegerSafe($clickID);

$MeritType = IntegerSafe($MeritType);
$waitApproval = IntegerSafe($waitApproval);
$approved = IntegerSafe($approved);
$rejected = IntegerSafe($rejected);
$released = IntegerSafe($released);
$waived = IntegerSafe($waived);
$fromPPC = IntegerSafe($fromPPC);
$passedActionDueDate = IntegerSafe($passedActionDueDate);
$pic = IntegerSafe($pic);

$back_page = cleanCrossSiteScriptingCode($back_page);
if($back_page != 'index.php' && $back_page != 'detail.php') {
    $back_page = 'index.php';
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Release");

$RecordIDAry = array();
if(is_array($RecordID)) {
	$RecordIDAry = $RecordID;
}
else {
	$RecordIDAry[] = $RecordID;
}
if(empty($RecordIDAry))	header("Location: index.php");

$linterface = new interface_html();

if($sys_custom['eDiscipline']['yy3']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

# Menu highlight setting
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Navigation bar
//$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php?SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&released=$released&waived=$waived");
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "javascript:click_cancel()");
$PAGE_NAVIGATION[] = array($eDiscipline["UnreleaseRecord"], "");

# Build html part
$i = 0;
$imgPath = "{$image_path}/{$LAYOUT_SKIN}";

foreach($RecordIDAry as $MeritRecordID)
{
	$reference = "";
	$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);
	$data = $datainfo[0];
	$StudentID = $data['StudentID'];
	$thisRemark = $data['Remark'];

	$lu = new libuser($StudentID);
	$thisClassNumber = $lu->ClassNumber;
	$thisClassName = $lu->ClassName.($thisClassNumber ? "-".$thisClassNumber : "");
	$namefield = getNameFieldWithLoginByLang();
	$sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 1 AND UserID IN (". $data['PICID'] .") AND RecordStatus = 1 ORDER BY $namefield";
	$array_PIC = $ldiscipline->returnArray($sql);
	$PICAry = array();
	foreach($array_PIC as $k=>$d) {
		$PICAry[] = $d[1];
	}
	$PICName = empty($PICAry) ? (($data['PICID']!="") ? $data['PICID'] : "---") : implode("<br>",$PICAry);
	
	# if the record is not released, cannot unrelease
	$isReleased = $ldiscipline->RETRIEVE_MERIT_RECORD_RELEASE_STATUS($MeritRecordID);
	$isUnReleased = $isReleased ? 0 : 1;
	
	if(!$isUnReleased)	$i++;
	
	$actionAry = array();
	if($ldiscipline->HAS_DETENTION($MeritRecordID))
	{
		$actionAry[] = "<a href='javascript:;' onClick=\"changeClickID({$MeritRecordID});moveObject('show_detail{$MeritRecordID}', true);showResult({$MeritRecordID},'detention');MM_showHideLayers('show_detail{$MeritRecordID}','','show');\">". $eDiscipline['Detention'] ."</a><div id='show_detail{$MeritRecordID}' style='position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		$y .= "document.onload=moveObject('show_detail{$MeritRecordID}', true);\n";
		$jsDetention .= ($jsDetention!='') ? ','.$MeritRecordID : $MeritRecordID;
	}
	if($y != "") {
		$showY = "<script languahe='javascript'>window.onload = pageOnLoad;function pageOnLoad() {".$y."}</script>";
	}
	if($data['TemplateID'])
	{
		if($data['NoticeID'])
		{
			$lc = new libucc($data['NoticeID']);
			$notice_info = $lc->getNoticeDetails();
			$notice_url = $notice_info['url'];
			$actionAry[] = "<a href='javascript:;' onClick=\"newWindow('{$notice_url}',1)\">". $eDiscipline["SendNotice"] ."</a>";
		}
		else
		{
			$actionAry[] = $eDiscipline["SendNotice"];				
		}
	}
	
	if($data['fromConductRecords']==1) {
		$reference .= $ldiscipline->getAwardPunishHistory($MeritRecordID, $imgPath);	
		$jsHistory .= ($jsHistory!='') ? ','.$MeritRecordID : $MeritRecordID;
	}
	$reference .= ($data['fromConductRecords']==1 && $data['CaseID'] != 0) ? "<br>" : "";
	if($data['CaseID'] != 0) 
	{
		$reference .= "<a href=../case_record/view.php?CaseID=".$data['CaseID'].">". $i_Discipline_System_Discipline_Case_Record_Case ."</a>";	
	}
	$reference .= ($data['fromConductRecords']!=1 && $data['CaseID'] == 0) ? "---" : "";
	//$ItemText = stripslashes(intranet_htmlspecialchars($data['ItemText']));
	$ItemText = ($data['ItemText']!="") ? intranet_htmlspecialchars($data['ItemText']) : "---";

	$pendingImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waiting.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Pending\" border=0>";
	$rejectImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Rejected\" border=0>";
	$releasedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_to_student.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_Released_To_Student\" border=0>";
	$approvedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_approve_b.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Approved\" border=0>";
	$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=\"$i_Discipline_System_Award_Punishment_Waived\" border=0>";
	$remarksImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_remark.gif width=20 height=20 align=absmiddle title=\"".$iDiscipline['Remarks']."\" border=0>";
	
	$status = ($data['RecordStatus']==DISCIPLINE_STATUS_PENDING) ? $pendingImg : "";
	$status .= ($data['ReleaseStatus']==DISCIPLINE_STATUS_RELEASED) ? $releasedImg : "";
	$status .= ($data['RecordStatus']==DISCIPLINE_STATUS_REJECTED) ? $rejectImg : "";
	$status .= ($data['RecordStatus']==DISCIPLINE_STATUS_APPROVED) ? $approvedImg : "";
	if($data['RecordStatus']==DISCIPLINE_STATUS_WAIVED) {
		$status .= "<a href='javascript:;' onClick=\"changeClickID({$data['RecordID']});moveObject('show_waive{$data['RecordID']}', true);showResult({$data['RecordID']},'waived');MM_showHideLayers('show_waive".$data['RecordID']."','','show');\">".$waivedImg."</a><div onload='changeClickID({$data['RecordID']});moveObject('show_waive{$data['RecordID']}', true);' id='show_waive".$data['RecordID']."' style='position:absolute; width:280px; height:180px; z-index:-1;'></div>";
		$jsWaive .= ($jsWaive!='') ? ','.$MeritRecordID : $MeritRecordID;
	}

	$waived_css = $isUnReleased ? "record_waived" : " ";
	
	$row .= "<tr class='row_approved $waived_css'>";
	$row .= "<td valign='top' >". ($isUnReleased ? "&nbsp;" : $i) ."&nbsp;</td>";
	$row .= "<td valign='top'>". $thisClassName ."&nbsp;</td>";
	$row .= "<td valign='top'>". $lu->UserName()."&nbsp;</td>";
	$row .= "<td valign='top'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/". ($data['MeritType']==1? "icon_merit":"icon_demerit").".gif' width='20' height='20' title='".($data['MeritType']==1?$i_Merit_Award:$i_Merit_Punishment)."'>&nbsp;</td>";
	$row .= "<td valign='top' nowrap>". $ldiscipline->returnDeMeritStringWithNumber($data['ProfileMeritCount'], $ldiscipline->RETURN_MERIT_NAME($data['ProfileMeritType']))  ."&nbsp;</td>";
	if($ldiscipline->Display_ConductMarkInAP) {
		$conductMark = ($data['ConductScoreChange']=="") ? 0 : $data['ConductScoreChange'];
		if($sys_custom['eDiscipline']['yy3'] && ($data['OverflowConductMark']=='1' || $data['OverflowMeritItemScore']=='1' || $data['OverflowTagScore']=='1') && $conductMark==0){
			$row .= "<td valign='top' nowrap>".$ldiscipline_ui->displayMaskedMark($conductMark)."</td>";
		}
		else{
			$row .= "<td valign='top' nowrap>".$conductMark."</td>";
		}
	}
	$row .= "<td valign='top'><a href='detail.php?id=$MeritRecordID'>". $ItemText ."</a>";
		if ($thisRemark != "")
		{
			$row .= "&nbsp;";
			$row .= "<a href='javascript:;' onClick=\"changeClickID({$MeritRecordID}); moveObject('show_remark".$MeritRecordID."', true); 
								showResult({$MeritRecordID},'record_remark'); MM_showHideLayers('show_remark".$MeritRecordID."','','show');\">";
				$row .= $remarksImg;
			$row .= "</a>";
			$row .= "<div onload='changeClickID({$MeritRecordID}); moveObject(\'show_remark".$MeritRecordID."}\', true);'
								id='show_remark".$MeritRecordID."' style='position:absolute; width:320px; height:150px; z-index:-1;'></div>";
			
			$jsRemarkAry .= ($jsRemarkAry != '') ? ",".$MeritRecordID : $MeritRecordID;					
		}
	$row .= "</td>";
	$row .= "<td valign='top' nowrap>". $data['RecordDate'] ."&nbsp;</td>";
	$row .= "<td valign='top'>". $PICName ."&nbsp;</td>";
	$row .= "<td valign='top'>".$reference."&nbsp;</td>";
	$row .= "<td valign='top'>".$status."&nbsp;</td>";
	$row .= "<td align='left' valign='top'>". implode("<br>",$actionAry) ."&nbsp;</td>";
	
	if($isUnReleased)
	{
		$row .= "<td valign='top'>". $eDiscipline["RecordIsReleased"] ."&nbsp;</td>";
	}
	else
	{
		$row .= "<td valign='top' nowrap>&nbsp;</td>";
		$row .= "<input type='hidden' name='RecordID[]' value='". $MeritRecordID."' />	";
	}
	$row .= "</tr>";
}

# Start layout
$linterface->LAYOUT_START();

$y = "<script language=javascript>";
$y .= ($jsDetention != '') ? "var jsDetention=[{$jsDetention}];\n" : "var jsDetention=[];\n";
$y .= ($jsWaive != '') ? "var jsWaive=[{$jsWaive}];\n" : "var jsWaive=[];\n";
$y .= ($jsRemarkAry != '') ? "var jsRemark=[{$jsRemarkAry}];\n" : "var jsRemark=[];\n";
$y .= ($jsHistory != '') ? "var jsHistory=[{$jsHistory}];\n" : "var jsHistory=[];\n";
$y .= "</script>";
echo $y;
?>

<script language="javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function click_cancel()
{
	document.form1.action = "index.php";
	document.form1.submit();
	/*
	<?if($back_page) {?>
		window.location= "<?=$back_page?>?id=<?=$RecordIDAry[0]?>";
	<? } else {?>
		window.location="index.php?SchoolYear=<?=$SchoolYear?>&semester=<?=$semester?>&targetClass=<?=$targetClass?>&num_per_page=<?=$num_per_page?>&pageNo=<?=$pageNo?>&order=<?=$order?>&field=<?=$field?>&page_size_change=<?=$page_size_change?>&numPerPage=<?=$numPerPage?>&MeritType=<?=$MeritType?>&s=<?=$s?>&clickID=<?=$clickID?>&waitApproval=<?=$waitApproval?>&approved=<?=$approved?>&rejected=<?=$rejected?>&released=<?=$released?>&waived=<?=$waived?>&pic=<?=$pic?>&fromPPC=<?=$fromPPC?>&passedActionDueDate=<?=$passedActionDueDate?>";
	<? } ?>
	*/
}
//-->
</script>

<script language="javascript">
<!--
var xmlHttp

function showResult(str,flag)
{
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	}

	var url = "";
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()
	
	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	} else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	} else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged4 
	}
		
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged4() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history"+id).style.zIndex = "1";
		document.getElementById("show_history"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
/*********************************/
function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;
	
	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.body.scrollLeft;
		tempY = event.clientY + document.body.scrollTop;
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
	}
	
	if (tempX < 0){tempX = 0} else {tempX = tempX - 300}
	if (tempY < 0){tempY = 0} else {tempY = tempY}
	
	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );
}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

//-->
</script>
<?=$showY?>
<form name="form1" method="POST" action="unrelease_update2.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td align="center">
		<table width="90%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td class="form_sep_title"><i>- <?=$eDiscipline["SelectedRecord"]?> -</i></td>
		</tr>
		<tr>
			<td align="center">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="tabletop">
					<td align="center">#</td>
					<td><?=$i_general_class?></td>
					<td><?=$i_general_name?></td>
					<td>&nbsp;</td>
					<td><?=$i_Discipline_Reason2?></td>
					<?if($ldiscipline->Display_ConductMarkInAP) {?>
						<td><?=$eDiscipline['Conduct_Mark']?></td>
					<?}?>
					<td><?=$i_Discipline_System_Item_Code_Name?></td>
					<td><?=$Lang['eDiscipline']['EventDate']?></td>
					<td><?=$i_Discipline_PIC?></td>
					<td><?=$i_Discipline_System_Award_Punishment_Reference?></td>
					<td><?=$i_Discipline_System_Discipline_Status?></td>
					<td><?=$eDiscipline["Action"]?></td>
					<td>&nbsp;</td>
				</tr>
				
				<?=$row?>
		
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<? if($i) { ?>
				<?= $linterface->GET_ACTION_BTN($button_continue, "submit")?>&nbsp;
				<? } ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "click_cancel();")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>" />
<input type="hidden" name="num_per_page" id="num_per_page" value="<?=$num_per_page?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="s" id="s" value="<?=$s?>" />
<input type="hidden" name="pic" id="pic" value="<?=$pic?>" />
<input type="hidden" name="clickID" id="clickID" value="<?=$clickID?>" />
<input type="hidden" name="back_page" id="back_page" value="<?=$back_page?>" />
<input type="hidden" name="waitApproval" id="waitApproval" value="<?=$waitApproval?>" />
<input type="hidden" name="approved" id="approved" value="<?=$approved?>" />
<input type="hidden" name="rejected" id="rejected" value="<?=$rejected?>" />
<input type="hidden" name="released" id="released" value="<?=$released?>" />
<input type="hidden" name="waived" id="waived" value="<?=$waived?>" />
<input type="hidden" name="fromPPC" id="fromPPC" value="<?=$fromPPC?>" />
<input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="<?=$passedActionDueDate?>" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>