<?php
# using: 

############ Change Log Start #############################
#
#   Date    :   2019-05-01  Bill
#               prevent SQL Injection + Cross-site Scripting + Redirect Problem
# 
#	Date	:	2013-02-19 (YatWoon)
#				revised the approval checking logic
#
#	Date	:	2012-10-09 (YatWoon)
#				Missing to check the approval group right for approve / reject [Case#2012-1004-1026-21054]
#
############################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$RecordID = IntegerSafe($RecordID);

$SchoolYear = IntegerSafe($SchoolYear);
if($semester != '' && $semester != 'WholeYear') {
    $semester = IntegerSafe($semester);
}
$targetClass = cleanCrossSiteScriptingCode($targetClass);

$s = cleanCrossSiteScriptingCode($s);
$clickID = IntegerSafe($clickID);

$MeritType = IntegerSafe($MeritType);
$fromPPC = IntegerSafe($fromPPC);
$passedActionDueDate = IntegerSafe($passedActionDueDate);
$pic = IntegerSafe($pic);

$back_page = cleanCrossSiteScriptingCode($back_page);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();

# Check access right
// $ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Approval");
//if(!($hasApprovalRight && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")))
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View"))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
# check whether current user has the approval right (according to settings in "Approval Group")
$hasRightToRejectRecordID = array();
if(!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"]) {
	$hasRightToRejectRecordID = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED($UserID, "");
	$hasApprovalRight = (sizeof($hasRightToRejectRecordID)>0) ? 1 : 0;
}else
{
	$hasApprovalRight = 1;	
}
*/

/*
$useApprovalGroup = $ldiscipline->useApprovalGroup();
if($useApprovalGroup && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"])
{
	$hasApprovalRight = (sizeof($hasRightToRejectRecordID)>0 && in_array($id,$hasRightToRejectRecordID)) ? 1 : 0;
}
*/

$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&fromPPC=$fromPPC&pic=$pic&passedActionDueDate=$passedActionDueDate";

$RecordIDAry = array();
$RecordIDAry = $RecordID;
if($back_page) {
    if($back_page != 'index.php' && $back_page != 'detail.php') {
        $back_page = 'index.php';
    }
	$returnPath = $back_page."?id=$RecordIDAry[0]";
}
else {
	$returnPath = "index.php?$return_filter";
}

foreach($RecordIDAry as $MeritRecordID)
{
	# double check user can approve the record or not 
	$can_approve = $ldiscipline->RETURN_AP_RECORDID_CAN_BE_APPROVED_BY_USER($MeritRecordID);
	if($can_approve)
	{
		$ldiscipline->REJECT_MERIT_RECORD($MeritRecordID);
	}
}

$msg = "reject";
header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>