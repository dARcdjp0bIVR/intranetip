<?php
// modifying : 
/*
 * 2019-05-13 (Bill)  : Prevent SQL Injection
 * 2013-06-21 (Carlos): $sys_custom['eDiscipline']['yy3'] - Display conduct score change with masked symbol for those overflow score merit items 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New", "You have no priviledge to access this page.");

$lu = new libuser();
$linterface = new interface_html("popup.html");

if($sys_custom['eDiscipline']['yy3']){
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
	$ldiscipline_ui = new libdisciplinev12_ui_cust();
}

$MODULE_OBJ['title'] = $eDiscipline["AwardPunishmentRecord"];

$StudentID = ($_GET['StudentID'] != "") ? $_GET['StudentID'] : $StudentID;
$StudentID = IntegerSafe($StudentID);

$current_year = getCurrentAcademicYear();
$currentAcademicYearID = Get_Current_Academic_Year_ID();

//$sql = "SELECT DISTINCT Year FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$StudentID' ORDER BY Year DESC";
//$array_years = $ldiscipline->returnVector($sql);
$sql = "SELECT DISTINCT AcademicYearID FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$StudentID'";
$APYearID = $ldiscipline->returnVector($sql);

$array_years = array();
for($i=0; $i<sizeof($APYearID); $i++) {
	$sql = "SELECT ".Get_Lang_Selection("YearNameB5","YearNameEN")." FROM ACADEMIC_YEAR WHERE AcademicYearID = '".$APYearID[$i]."'";
	$result = $ldiscipline->returnVector($sql);
	$array_years[] = $result[0];
}

/*
if (sizeof($array_years)==0 || !in_array($current_year, $array_years))
{
    $array_years = array_merge(array($current_year),$array_years);
}
if ($year == "")
{
    $year = getCurrentAcademicYear();
}
*/
$sql = "SELECT DISTINCT AcademicYearID FROM DISCIPLINE_MERIT_RECORD WHERE StudentID = '$StudentID'";
$array_years = $ldiscipline->returnVector($sql);
if (sizeof($array_years)==0 || !in_array($currentAcademicYearID, $array_years))
{
    $array_years = array_merge(array($currentAcademicYearID),$array_years);
}
for($i=0; $i<sizeof($array_years); $i++) {
	$yearAry[$i][] = $array_years[$i]; 
	$yearAry[$i][] = $ldiscipline->getAcademicYearNameByYearID($array_years[$i]);
}
if ($year == "")
{
    $year = Get_Current_Academic_Year_ID();
}

//$yearID = $ldiscipline->getAcademicYearIDByYearName($year);
$yearID = $year;
if ($semester == "")
{
	$sem = getSemesters($yearID);
	foreach($sem as $key=>$val) {
		$semester = $key;
		break;
	}
}
/*
$sql = "SELECT ".Get_Lang_Selection("YearTermNameB5","YearTermNameEN")." FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID=$yearID ORDER BY TermStart";
$array_semester = $ldiscipline->returnVector($sql);
*/

$semAry = getSemesters($yearID);
$i = 0;
foreach($semAry as $key=>$val) {
	$semesterArray[$i][] = $key;
	$semesterArray[$i][] = $val;
	$i++;
}

$student = $lu->getNameWithClassNumber($StudentID);
$merit_record = $ldiscipline->retrieveStudentMeritRecordByYearSemester($StudentID, $year, $semester, 1);
$demerit_record = $ldiscipline->retrieveStudentMeritRecordByYearSemester($StudentID, $year, $semester, -1);

$string_type["-1"] = $i_Merit_BlackMark;
$string_type["-2"] = $i_Merit_MinorDemerit;
$string_type["-3"] = $i_Merit_MajorDemerit;
$string_type["-4"] = $i_Merit_SuperDemerit;
$string_type["-5"] = $i_Merit_UltraDemerit;
$string_type["1"] = $i_Merit_Merit;
$string_type["2"] = $i_Merit_MinorCredit;
$string_type["3"] = $i_Merit_MajorCredit;
$string_type["4"] = $i_Merit_SuperCredit;
$string_type["5"] = $i_Merit_UltraCredit;

$col = ($ldiscipline->UseSubScore) ? 7 : 6;
$merit_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
$merit_table .= "<tr><td colspan=\"$col\">". $linterface->GET_NAVIGATION2($eDiscipline["AwardRecord"]) ."</td></tr>";
$merit_table .= "<tr>
				<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>
				<td class=\"tablebluetop tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['EventDate']."</td>
				<td class=\"tablebluetop tabletopnolink\" width=\"200\">".$eDiscipline["Award_Punishment_RecordItem"]."</td>
                <td class=\"tablebluetop tabletopnolink\">$i_general_receive</td>
                <td class=\"tablebluetop tabletopnolink\" width=\"50\">$i_Discipline_System_ConductScore</td>";
$merit_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Subscore1</td>" : "";
$merit_table .= "<td class=\"tablebluetop tabletopnolink\" width=\"150\">$i_Profile_PersonInCharge</td></tr>";

for ($i=0; $i<sizeof($merit_record); $i++)
{
	list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_subscore1, $r_picid, $r_pic_name) = $merit_record[$i];
	$r_item = intranet_htmlspecialchars($r_item);
	
	$merit_table .= "<tr class=\"tablebluerow".($i%2+1)."\">
					<td class=\"tabletext\">".($i+1)."</td>
					 <td class=\"tabletext\">$r_date</td>
					 <td class=\"tabletext\">$r_item</td>
                     <td class=\"tabletext\">$r_profile_count ".$string_type[$r_profile_type]."</td>";
    if($sys_custom['eDiscipline']['yy3'] && ($merit_record[$i]['OverflowConductMark']=='1'
    	 || $merit_record[$i]['OverflowMeritItemScore']=='1' || $merit_record[$i]['OverflowTagScore']=='1') && $r_conduct==0)
   	{
    	$merit_table .= "<td class=\"tabletext\">".$ldiscipline_ui->displayMaskedMark($r_conduct)."</td>";
    }
    else
    {
    	$merit_table .= "<td class=\"tabletext\">$r_conduct</td>";
   	}
	$merit_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tabletext\">$r_subscore1</td>" : "";
    $merit_table .= "<td class=\"tabletext\">$r_pic_name</td></tr>";
}
if(sizeof($merit_record) == 0) {
	$merit_table .= "<td colspan=$col align=center class=\"tablebluerow2 tabletext\">".$i_no_record_exists_msg."</td>";
}
$merit_table .= "</table>\n";

$demerit_table = "<table width=\"95%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"tabletext\">\n";
$demerit_table .= "<tr>
							<td colspan=\"$col\">". $linterface->GET_NAVIGATION2($eDiscipline["PunishmentRecord"]) ."</td>
					</tr>
							<tr>
							<td class=\"tablebluetop tabletopnolink\" width=10>#</td>
							<td class=\"tablebluetop tabletopnolink\" width=80>".$Lang['eDiscipline']['EventDate']."</td>
							<td class=\"tablebluetop tabletopnolink\" width=200>".$eDiscipline["Award_Punishment_RecordItem"]."</td>
                            <td class=\"tablebluetop tabletopnolink\">$i_general_receive</td>
                            <td class=\"tablebluetop tabletopnolink\" width=\"50\">$i_Discipline_System_ConductScore</td>";
$demerit_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Subscore1</td>" : "";
$demerit_table .= "<td class=\"tablebluetop tabletopnolink\" width=\"150\">$i_Profile_PersonInCharge</td></tr>";

for ($i=0; $i<sizeof($demerit_record); $i++)
{
	list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
			$r_conduct, $r_subscore1, $r_picid, $r_pic_name) = $demerit_record[$i];
	$r_item = intranet_htmlspecialchars($r_item);
	
	$demerit_table .= "<tr class=\"tablebluerow".($i%2+1)."\"><td class=\"tabletext\">".($i+1)."</td>
								<td class=\"tabletext\">$r_date</td>
                                <td class=\"tabletext\">$r_item</td>
                                <td class=\"tabletext\">$r_profile_count ".$string_type[$r_profile_type]."</td>";
    if($sys_custom['eDiscipline']['yy3'] && ($demerit_record[$i]['OverflowConductMark']=='1'
    	 || $demerit_record[$i]['OverflowMeritItemScore']=='1' || $demerit_record[$i]['OverflowTagScore']=='1') && $r_conduct==0)
   	{
    	$demerit_table .= "<td class=\"tabletext\">".$ldiscipline_ui->displayMaskedMark($r_conduct)."</td>";
    }
    else
    {                            
    	$demerit_table .=  "<td class=\"tabletext\">$r_conduct</td>";
   	}
	$demerit_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tabletext\">$r_subscore1</td>" : "";
    $demerit_table .= "<td class=\"tabletext\">".($r_pic_name==""?"--":$r_pic_name)."</td></tr>";
}
if(sizeof($demerit_record) == 0) {
	$demerit_table .= "<td colspan=\"$col\" align=\"center\" class=\"tablebluerow2 tabletext\">".$i_no_record_exists_msg."</td>";
}
$demerit_table .= "</table>\n";

$linterface->LAYOUT_START();
?>

<script language="javascript">
/*
var xmlHttp

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("semester").innerHTML = "";
			document.getElementById("semester").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php";
	url = url + "?year=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("semester").innerHTML = xmlHttp.responseText;
		document.getElementById("semester").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
*/
</script>

<form name="form1" action="viewcurrent.php" method="POST">

<!--  START Student Info table //-->
<br />
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
<tr valign="top">
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_UserStudentName?></span></td>
	<td class="tabletext"><?=$student?></td>
</tr>
<tr valign="top">
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_show?></span></td>
	<td class="tabletext">
		<?=getSelectByArray($yearAry,"name='year' id='year' onchange='document.form1.semester.selectedIndex = -1;document.form1.submit();'",$year, 0, 1)?>
		<?/*=getSelectSemester("name='semester' onchange='document.form1.submit();'",$semester)*/?>
		<?=getSelectByArray($semesterArray, "name='semester' id='semester' onChange='document.form1.submit();'", $semester,0,1)?>
	</td>
</tr>
</table>

<!-- END Student Info table //-->

<br />
<?=$merit_table?>

<br/>
<?=$demerit_table?>

<input type='hidden' name="StudentID" id="StudentID" value="<?=$StudentID?>" />
<input type='hidden' name="submitFlag" id="submitFlag" value="1" />
</form>

<table width="95%" border="0" cellpadding="5" cellspacing="0">    
		<tr>
        	<td colspan="3">
                        <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
                        <tr>
                        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                        </tr>
                        <tr>
                			<td align="center">
        						<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","close_btn") ?>
                			</td>
        				</tr>
                        </table>
        	</td>
        </tr>
        </table>
        
<br />

<?php
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
$linterface->LAYOUT_STOP();
?>