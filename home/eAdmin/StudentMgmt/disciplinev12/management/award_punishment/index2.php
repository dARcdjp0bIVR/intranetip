<?php
# Modifying by : 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if($ldiscipline->Display_ConductMarkInAP) {
	$sortEventDate = 6;			# Event Date is the 6th SQL column
} else {
	$sortEventDate = 5;			# Event Date is the 5th SQL column
}

if ($page_size_change == 1)	# change "num per page"
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}

if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
} else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}

if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
} else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $sortEventDate;
}


if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

# Check access right (View page)
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$currentYearID = ($SchoolYear=="") ? Get_Current_Academic_Year_ID() : $SchoolYear;

$recordType = ($MeritType!="1" && $MeritType != "-1") ? $i_Discipline_System_Award_Punishment_All_Records : "<a href='javascript:reloadForm(0)'>".$i_Discipline_System_Award_Punishment_All_Records."</a>";
$recordType .= " | ";
$recordType .= ($MeritType=="1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards : "<a href='javascript:reloadForm(1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards."</a>";
$recordType .= " | ";
$recordType .= ($MeritType=="-1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments : "<a href='javascript:reloadForm(-1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments."</a>";
	
######### Conditions #########
$conds = "";

# Class  #
if ($targetClass != '')
{
    $conds .= " AND g.YearClassID=$targetClass";
}

# MeritType #
if ($MeritType != '' && $MeritType != 0)
{
    $conds .= " AND a.MeritType = $MeritType";
} else {
	$conds .= " AND (a.MeritType = 1 or a.MeritType=-1 or a.MeritType=0)";
	$MeritType = 0;	
}


$yearName = $ldiscipline->getAcademicYearNameByYearID($SchoolYear);

# Year Menu #
$yearSelected = 0;
$currentYearInfo = Get_Current_Academic_Year_ID();

$SchoolYear = ($SchoolYear == '') ? $currentYearInfo : $SchoolYear;
$yearArr = $ldiscipline->getAPSchoolYear($SchoolYear);

$selectSchoolYear = "<select name='SchoolYear' id='SchoolYear' onChange='document.form1.semester.value=\"\";reloadForm($MeritType)'>";
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
for($i=0; $i<sizeof($yearArr); $i++) {
	$selectSchoolYear .= "<option value='".$yearArr[$i][0]."'";
	if($SchoolYear==$yearArr[$i][0]) {
		$selectSchoolYear .= " SELECTED";
	}
	if(Get_Current_Academic_Year_ID()==$yearArr[$i][0]) {
		$yearSelected = 1;
	}
	$selectSchoolYear .= ">".$yearArr[$i][1]."</option>";
}
if($yearSelected==0) {
	$selectSchoolYear .= "<option value='".Get_Current_Academic_Year_ID()."'";
	$selectSchoolYear .= (Get_Current_Academic_Year_ID()==$SchoolYear) ? " selected" : "";
	$selectSchoolYear .= ">".$ldiscipline->getAcademicYearNameByYearID($currentYearInfo)."</option>";
}

$selectSchoolYear .= "</select>";

if($SchoolYear != '' && $SchoolYear != 0) {
	$conds .= " AND a.AcademicYearID = $SchoolYear";	
}
//$extraConds = " AND g.AcademicYearID = a.AcademicYearID";


# Semester Menu #
$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5 FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID=$currentYearID ORDER BY TermStart";
$semResult = $ldiscipline->returnArray($sql, 3);
$SemesterMenu = "<select name='semester' id='semester' onChange='reloadForm($MeritType)'>";
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for($i=0; $i<sizeof($semResult); $i++) {
	list($id, $nameEN, $nameB5) = $semResult[$i];
	$semName = Get_Lang_Selection($nameB5, $nameEN);
	$SemesterMenu .= "<option value='$id'";
	$SemesterMenu .= ($semester==$id) ? " selected" : "";
	$SemesterMenu .= ">$semName</option>";
}
$SemesterMenu .= "</select>";

if($semester != '' && $semester != 'WholeYear') {
	$conds .= " AND a.YearTermID = $semester";	
}

# Class #
$select_class = $ldiscipline->getSelectClass("name=\"targetClass\" onChange=\"reloadForm($MeritType)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes);

# PIC # 
$PICArray = $ldiscipline->getPICInArray("DISCIPLINE_MERIT_RECORD", $currentYearID);
$picSelection = getSelectByArray($PICArray," name=\"pic\" onChange=\"reloadForm($MeritType)\"",$pic,0,0,"".$i_Discipline_Detention_All_Teachers."");
if($pic != "") 
	$conds .= " AND (a.PICID='$pic' OR a.PICID LIKE '$pic,%' OR a.PICID LIKE '%,$pic' OR a.PICID LIKE '%,$pic,%')";

$conds2 = "";

# Waiting for Approval #
if($waitApproval == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$waitApprovalChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.RecordStatus = ".DISCIPLINE_STATUS_PENDING;	
}
/*
if($unreleased != '') {
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.") AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
}
*/
# unreleased #
if($unreleased == '0' || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$unreleasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.") AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
}

# Approved #  (waived record also be approved)
if($approved == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	if($released == DISCIPLINE_STATUS_UNRELEASED) {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
	} else {
		$conds2 .= ($conds2=="") ? "(" : " OR ";
		$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";
	}
	$approvedChecked = "checked";
}

# Rejected #
if($rejected == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$rejectedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_REJECTED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL OR a.ReleaseStatus=NULL))";	
}

# Released #
if($released == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$releasedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.ReleaseStatus = ".DISCIPLINE_STATUS_RELEASED;	
}

# Waived #
if($waived == 1 || (!isset($released) && !isset($unreleased) && !isset($fromPPC) && !isset($fromOverview))) {
	$waivedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "(a.RecordStatus = ".DISCIPLINE_STATUS_WAIVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL))";	
}
/*
# not use in this phase
# Locked #
if($locked == 1) {
	$lockedChecked = "checked";
	$conds2 .= ($conds2=="") ? "(" : " OR ";
	$conds2 .= "a.LockStatus = ".DISCIPLINE_STATUS_LOCK;	
}
*/
$conds2 .= ($conds2 != "") ? ")" : "";

##############

$i_Merit_Merit = addslashes($i_Merit_Merit);
$i_Merit_MinorCredit = addslashes($i_Merit_MinorCredit);
$i_Merit_MajorCredit = addslashes($i_Merit_MajorCredit);
$i_Merit_SuperCredit = addslashes($i_Merit_SuperCredit);
$i_Merit_UltraCredit = addslashes($i_Merit_UltraCredit);

$li = new libdbtable2007($field, $order, $pageNo);

$const_status_pending = 0;

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Award\'>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Punishment\'>";

$student_namefield = getNamefieldByLang("b.");
$clsName = ($intranet_session_language=="en") ? "g.ClassTitleEN" : "g.ClassTitleB5";

if($intranet_session_language=="en")
	{
		$meritStr = "CONCAT(a.ProfileMeritCount,' ',
					CASE (a.ProfileMeritType)
					WHEN 0 THEN '$i_Merit_Warning'
					WHEN 1 THEN '$i_Merit_Merit'
					WHEN 2 THEN '$i_Merit_MinorCredit'
					WHEN 3 THEN '$i_Merit_MajorCredit'
					WHEN 4 THEN '$i_Merit_SuperCredit'
					WHEN 5 THEN '$i_Merit_UltraCredit'
					WHEN -1 THEN '$i_Merit_BlackMark'
					WHEN -2 THEN '$i_Merit_MinorDemerit'
					WHEN -3 THEN '$i_Merit_MajorDemerit'
					WHEN -4 THEN '$i_Merit_SuperDemerit'
					WHEN -5 THEN '$i_Merit_UltraDemerit'
					ELSE 'Error' END, '(s)')";
	}
	else
	{
		$meritStr = "CONCAT(
					CASE (a.ProfileMeritType)
					WHEN 0 THEN '$i_Merit_Warning'
					WHEN 1 THEN '$i_Merit_Merit'
					WHEN 2 THEN '$i_Merit_MinorCredit'
					WHEN 3 THEN '$i_Merit_MajorCredit'
					WHEN 4 THEN '$i_Merit_SuperCredit'
					WHEN 5 THEN '$i_Merit_UltraCredit'
					WHEN -1 THEN '$i_Merit_BlackMark'
					WHEN -2 THEN '$i_Merit_MinorDemerit'
					WHEN -3 THEN '$i_Merit_MajorDemerit'
					WHEN -4 THEN '$i_Merit_SuperDemerit'
					WHEN -5 THEN '$i_Merit_UltraDemerit'
					ELSE 'Error' END,
					a.ProfileMeritCount,
					'". $Lang['eDiscipline']['Times'] ."'
					)";
	}
	
if($fromPPC==1) {

	$result = $ldiscipline->retrieveMeritRecordFromPPC();
	$MeritRecordIDFromPPC = implode(',', $result);
	# CONCAT(b.ClassName,' - ',b.ClassNumber) as ClassNameNum, 
	
	$sql = "SELECT
			CONCAT($clsName,' - ',f.ClassNumber) as ClassNameNum,
			$student_namefield as EnglishName, 
			IF(a.MeritType>0,'$meritImg','$demeritImg') as meritImg,
			IF(a.ProfileMeritCount=0, '--',
				". $meritStr ."
			) as record,
			a.ConductScoreChange,
			if(a.ItemID=0, 
			CONCAT('<a href=\"javascript:go_detail(',a.RecordID,')\">--</a>'),
			CONCAT('<a title=\"',REPLACE(c.ItemCode,'\"','&quot;'),' - ',REPLACE(c.ItemName,'\"','&quot;'),'\" href=\"javascript:go_detail(',a.RecordID,')\">',c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;'),'</a>')
			) as item,
			a.RecordDate,
			PICID, 
			a.fromConductRecords as reference,
			a.TemplateID as action,
			LEFT(a.DateModified,10),
			a.ReleaseStatus as status,
			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', a.RecordID ,'>'),
			a.WaivedBy, 
			a.RecordStatus,
			a.RecordID,
			a.CaseID,
			a.NoticeID,
			b.UserID,
			a.Remark
				FROM DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
					LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID=f.UserID)
					LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
				WHERE a.DateInput IS NOT NULL AND g.AcademicYearID=$currentYearInfo
					AND a.RecordID IN ($MeritRecordIDFromPPC)
					$conds
					$extraConds					
					";
		$sql .= ($conds2 != '') ? " AND ".$conds2 : "";
		$sql .= " GROUP BY a.RecordID";
		
} else if($s != "") {		# filter by Searching
	$s = addslashes(trim($s));
	$sql = "SELECT
			CONCAT($clsName,' - ',f.ClassNumber) as ClassNameNum,
			$student_namefield as EnglishName, 
			IF(a.MeritType>0,'$meritImg','$demeritImg') as meritImg,
			IF(a.ProfileMeritCount=0, '--',
				". $meritStr ."
			) as record,";
	if($ldiscipline->Display_ConductMarkInAP) {			
			$sql .= "a.ConductScoreChange,";
	}
	$sql .= "
			if(a.ItemID=0, 
			CONCAT('<a href=\"javascript:go_detail(',a.RecordID,')\">--</a>'),
			CONCAT('<a title=\"',REPLACE(c.ItemCode,'\"','&quot;'),' - ',REPLACE(c.ItemName,'\"','&quot;'),'\" href=\"javascript:go_detail(',a.RecordID,')\">',c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;'),'</a>')
			) as item,
			a.RecordDate,
			PICID, 
			a.fromConductRecords as reference,
			a.TemplateID as action,
			LEFT(a.DateModified,10),
			a.ReleaseStatus as status,
			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', a.RecordID ,'>'),
			a.WaivedBy, 
			a.RecordStatus,
			a.RecordID,
			a.CaseID,
			a.NoticeID,
			b.UserID,
			a.Remark
				FROM DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
					LEFT OUTER JOIN YEAR_CLASS_USER as f ON (b.UserID=f.UserID)
					LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
				WHERE a.DateInput IS NOT NULL AND g.AcademicYearID=$currentYearInfo
					AND (
						g.ClassTitleEN like '%$s%' 
						OR g.ClassTitleB5 like '%$s%'
						OR f.ClassNumber like '%$s%'
						OR $student_namefield like '%$s%'
						OR c.ItemCode like '%$s%'
						OR c.ItemName like '%$s%'
						OR a.RecordDate like '%$s%'
					)
					AND (a.MeritType=1 OR a.MeritType=-1 OR a.MeritType=0)
					AND a.AcademicYearID=$SchoolYear
					$extraConds";
		$sql .= " GROUP BY a.RecordID";
	
} 
else {				# filter by option
	
	# CONCAT(b.ClassName,' - ',b.ClassNumber) as ClassNameNum, 
	$sql = "SELECT
			CONCAT($clsName,' - ',f.ClassNumber) as ClassNameNum,
			$student_namefield as EnglishName, 
			IF(a.MeritType>0,'$meritImg','$demeritImg') as meritImg,
			IF(a.ProfileMeritCount=0, '--',
				". $meritStr ."
			) as record,";
	if($ldiscipline->Display_ConductMarkInAP) {			
			$sql .= "a.ConductScoreChange,";
	}
	$sql .= "
			if(a.ItemID=0, 
			CONCAT('<a href=\"javascript:go_detail(',a.RecordID,')\">--</a>'),
			CONCAT('<a title=\"',REPLACE(c.ItemCode,'\"','&quot;'),' - ',REPLACE(c.ItemName,'\"','&quot;'),'\" href=\"javascript:go_detail(',a.RecordID,')\">',c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;'),'</a>')
			) as item,
			a.RecordDate,
			PICID, 
			a.fromConductRecords as reference,
			a.TemplateID as action,
			LEFT(a.DateModified,10),
			a.ReleaseStatus as status,
			CONCAT('<input type=\'checkbox\' name=\'RecordID[]\' id=\'RecordID[]\' value=', a.RecordID ,'>'),
			a.WaivedBy, 
			a.RecordStatus,
			a.RecordID,
			a.CaseID,
			a.NoticeID,
			b.UserID,
			a.Remark
				FROM DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN YEAR_CLASS_USER as f ON (a.StudentID=f.UserID AND f.UserID=a.StudentID)
					LEFT OUTER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID)
					
				WHERE a.DateInput IS NOT NULL AND g.AcademicYearID=$currentYearInfo
					$conds
					$extraConds					
				";
		$sql .= ($conds2 != '') ? " AND ".$conds2 : "";
		$sql .= " GROUP BY a.RecordID";
		
//echo $sql;
}

$MeritCountLocation = 3;
if($ldiscipline->Display_ConductMarkInAP) {	
	$conductMarkLocation = 4;
	$categoryItemLocation = 5;
	$PICLocation = 7;
	$historyLocation = 8;
	$actionLocation = 9;
	$waivedByLocation = 11;
	$checkboxLocation = 12;
	$StatusLocation = 14;
	$recordIDLocation = 15;
	$caseIDLocation = 16;
	$noticeIDLocation = 17;
	$remarkLocation = 19;
} else {
	$categoryItemLocation = 4;
	$PICLocation = 6;
	$historyLocation = 7;
	$actionLocation = 8;
	$waivedByLocation = 10;
	$checkboxLocation = 11;
	$StatusLocation = 13;
	$recordIDLocation = 14;
	$caseIDLocation = 15;
	$noticeIDLocation = 16;
	$remarkLocation = 18;
}
//$li->field_array[] = "ClassNameNum";
$li->field_array[] = "concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))";
$li->field_array[] = "b.EnglishName";
$li->field_array[] = "meritImg";
$li->field_array[] = "record";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->field_array[] = "ConductScoreChange";
}
$li->field_array[] = "item";
$li->field_array[] = "a.RecordDate";
$li->field_array[] = "PICID";
$li->field_array[] = "reference";
$li->field_array[] = "action";
$li->field_array[] = "a.DateModified";
$li->field_array[] = "status";

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_array[] = "0";	
}
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->wrap_array[] = "0";	
}

$li->IsColOff = 2;
//$li->fieldorder2 = ", RecordID desc";
//$li->fieldorder2 = ", ClassNameNum";
$li->fieldorder2 = ", a.DateInput desc, ClassNameNum";

# TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='1'>&nbsp;</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $i_Discipline_Reason2)."</td>\n";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $eDiscipline['Conduct_Mark'])."</td>\n";
}
$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_System_Item_Code_Item_Name)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_RecordDate)."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$i_Profile_PersonInCharge."</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>$i_Discipline_System_Award_Punishment_Reference</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$eDiscipline["Action"]."</td>\n";$pos++;
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";
$li->column_list .= "<td>".$i_Discipline_System_Discipline_Status."</td>\n";
$li->column_list .= "<td>".$li->check("RecordID[]")."</td>\n";


/*
$MeritCountLocation = 3;
$conductMarkLocation = 4;
$categoryItemLocation = 5;
$PICLocation = 7;
$historyLocation = 8;
$actionLocation = 9;
$waivedByLocation = 11;
$checkboxLocation = 12;
$StatusLocation = 14;
$recordIDLocation = 15;
$caseIDLocation = 16;
$noticeIDLocation = 17;
$remarkLocation = 19;
*/

######## end of table content #########
#######################################


# Access Right Checking
$actionLayer = "";

if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval")) {
	$actionLayer = "<tr>";
	$actionLayer .= "<td align='left' valign='top' class='tabletext'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','approve_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_approve.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Approve</a></td>";
	$actionLayer .= "</tr><tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1, 'RecordID[]','reject_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_reject_l.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Reject</a></td>";
	$actionLayer .= "</tr>";
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release")) {
	if($actionLayer != "") {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top' class='tabletext'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','release_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_to_student_s.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Release</a></td>";
	$actionLayer .= "</tr><tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','unrelease_update.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_private.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_UnRelease</a></td>";
	$actionLayer .= "</tr>";
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive")) {
	if($actionLayer != "") {
		$actionLayer .= "<tr>";
		$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
		$actionLayer .= "</tr>";
	}
	
	# waive layer
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','waive.php')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Waive</a></td>\n";
	$actionLayer .= "</tr>";

	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top' class='dotline' height='2'><img src='{$image_path}/{$LAYOUT_SKIN}'/10x10.gif' height='2'></td>";
	$actionLayer .= "</tr>";
	
	# redeem layer
	$actionLayer .= "<tr>";
	$actionLayer .= "<td align='left' valign='top'><a href=\"javascript:changeFormAction(document.form1,'RecordID[]','redeem_step1.php', 'redeem')\" class='tabletool'><img src='{$image_path}/{$LAYOUT_SKIN}/icon_waive.gif' width='12' height='12' border='0' align='absmiddle'> $i_Discipline_System_Award_Punishment_Redeem</a></td>\n";
	$actionLayer .= "</tr>";
}

# menu highlight setting
$CurrentPage = "Management_AwardPunishment";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();


$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment'], "");

$exportLink = "export.php?s=$s&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&MeritType=$MeritType&waitApproval=$waitApproval&approved=$approved&rejected=$rejected&released=$released&unreleased=$unreleased&waived=$waived&locked=$locked&fromPPC=$fromPPC&fromOverview=$fromOverview";

if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-New")) {
	$toolbar = $linterface->GET_LNK_NEW("new1.php",$btn_new,"","","",0)."&nbsp;&nbsp;&nbsp;&nbsp;";
	$toolbar .= $linterface->GET_LNK_IMPORT("import.php",$btn_import,"","","",0)."&nbsp;&nbsp;&nbsp;&nbsp;";
}
$toolbar .= $linterface->GET_LNK_EXPORT($exportLink,$btn_export,"","","",0);

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
    
}

function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}

function reloadForm(val) {

	document.form1.MeritType.value = val;
	document.form1.s.value = "";
	document.form1.pageNo.value = 1;
//	alert(document.form1.approved.value);
	document.form1.submit();	
}

function goSearch(obj) {
	/*
	if(document.form2.text.value=="") {
		alert("<?=$i_Discipline_System_Award_Punishment_Search_Alert?>");	
		document.form2.text.focus();
		return false;
	}
	else {
	*/
	
		document.form1.s.value = document.form2.text.value;
		document.form1.pageNo.value = 1;
		document.form1.submit();
	//}
}


function changeFormAction(obj,element,url,type) {
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else {
		if (type=='redeem')
		{
			redeemCheck();
			return;
		}
		document.form1.action = url;
		document.form1.submit();
	}
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();	
}


function checkCR(evt) {

	var evt  = (evt) ? evt : ((event) ? event : null);
	var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	if ((evt.keyCode == 13) && (node.type=="text")) {return false;}	

}

document.onkeypress = checkCR;

//-->
</script>
<script language="javascript">
<!--
var xmlHttp
var jsDetention = [];
var jsWaive = [];
var jsHistory = [];
var jsRemark = [];

function showResult(str,flag)
{

	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()

	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	} else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	}  else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged4 
	}
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)

} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged4() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}

/*********************************/
function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e, moveX ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;

	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.documentElement.scrollLeft;
		tempY = event.clientY + document.documentElement.scrollTop;
		
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
		
	}
	if(moveX != undefined)
		tempX = tempX - moveX
	else 
		if (tempX < 0){tempX = 0} else {tempX = tempX - 320}
	
	if (tempY < 0){tempY = 0} else {tempY = tempY}

	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );

}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

//-->
</script>

<script language="javascript">
<!--
// Start of AJAX 
var callback = {
    success: function ( o )
    {
		if (o.responseText != "")
		{
			alert(o.responseText);
			return false;
		}
		else
		{
			form1.action = 'redeem_step1.php';
			form1.submit();
		}
	}
}

// Main
function redeemCheck()
{
	obj = document.form1;
    YAHOO.util.Connect.setForm(obj);
	
    var path = "redeem_check_aj.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
}

// End of AJAX
//-->
</script>
<script language="javascript">
<!--
function go_detail(id)
{
	document.form1.id.value = id;
	document.form1.action="detail.php";
	document.form1.submit();
}
$().ready(function (){
	hideAllOtherLayer();
});
//-->
</script>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<form name="form2" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table border="0" cellspacing="0" cellpadding="2">
							<tr><td><?= $toolbar ?></td></tr>
						</table>
					</td>
					<td align="right"><input name="text" id="text" type="text" class="formtextbox" value="<?=stripslashes(stripslashes($s))?>">
						<?=$linterface->GET_BTN($button_find, "button", "javascript:return goSearch('document.form2')");?>
					</td>
				</tr>
			</table>
			</form>
			<form name="form1" method="post" action="">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="5">
										<tr>
											<td height="30">
												<?=$selectSchoolYear?>
												<?=$SemesterMenu?>
												<?=$select_class?>
												<?=$picSelection?>
												
											</td>
											<td align="right" valign="middle" class="thumb_list"><?=$recordType?></td>
										</tr>
										<tr>
											<td colspan="2" align="center">
												<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Award_Punishment_Warning) ?>
											</td>
										</tr>
									</table>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right"><?= $linterface->GET_SYS_MSG($msg) ?>
											</td>
										</tr>
									</table>

									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><div class="selectbox_group"> <a href="javascript:;" onClick="MM_showHideLayers('status_option','','show')"><?=$i_Discipline_System_Award_Punishment_Select_Status?></a> </div>
												<br style="clear:both">
												<div id="status_option" class="selectbox_layer">
													<table width="" border="0" cellspacing="0" cellpadding="3">
														<tr>
															<td><input type="checkbox" name="waitApproval" id="waitApproval" value="1" <?=$waitApprovalChecked?>>
																<label for="waitApproval"><?=$i_Discipline_System_Award_Punishment_Pending?></label><br>
																<input type="checkbox" name="approved" id="approved" value="1" <?=$approvedChecked?>>
																<label for="approved"><?=$i_Discipline_System_Award_Punishment_Approved?></label><br>
																<input type="checkbox" name="rejected" id="rejected" value="1" <?=$rejectedChecked?>>
																<label for="rejected"><?=$i_Discipline_System_Award_Punishment_Rejected?></label>
															</td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="1"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="1"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="released" id="released" value="1" <?=$releasedChecked?>>
																<label for="released"><?=$i_Discipline_System_Award_Punishment_Released?></label><br>
																<input type="checkbox" name="unreleased" id="unreleased" value="0" <?=$unreleasedChecked?>>
																<label for="unreleased"><?=$i_Discipline_System_Award_Punishment_UnReleased?></label></td>
														</tr>
														<tr>
															<td align="left" valign="top" class="dotline" height="1"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="1"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="waived" id="waived" value="1" <?=$waivedChecked?>>
																<label for="waived"><?=$i_Discipline_System_Award_Punishment_Waived?></label></td>
														</tr>
<!--
														<tr>
															<td align="left" valign="top" class="dotline" height="1"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="1"></td>
														</tr>
														<tr>
															<td><input type="checkbox" name="locked"  id="locked" value="1" <?=$lockedChecked?>>
																<label for="locked"><?=$i_Discipline_System_Award_Punishment_Locked?></label> </td>
														</tr>
//-->
														<tr>
															<td align="left" valign="top" class="dotline" height="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" height="2"></td>
														</tr>
														<tr>
															<td align="center" class="tabletext">
																<?= $linterface->GET_BTN($button_view, "submit", "javascript:reloadForm($MeritType)")?>
																<?= $linterface->GET_BTN($button_cancel, "button", "javascript: onClick=MM_showHideLayers('status_option','','hide');form.reset();")?>
															</td>
														</tr>
													</table>
												</div>
											</td>
											<td align="right" valign="bottom">
												<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll")) { ?>
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="1">
																<tr>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Approval") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Release") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-Waive")) { ?>
																	<td nowrap><a href="javascript:;" onClick="MM_showHideLayers('status_list','','show')" class="tabletool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_update.gif" width="12" height="12" border="0" align="absmiddle"> <?=$i_Discipline_System_Award_Punishment_Change_Status?></a>
																		<div id="status_list" style="position:absolute;  width:105px; z-index:1; visibility: hidden;">
																			<table width="100%" border="0" cellpadding="0" cellspacing="0" >
																				<tr>
																					<td height="19">
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="5" height="19"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_01.gif" width="5" height="19"></td>
																								<td height="19" valign="middle" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_02.gif">&nbsp;</td>
																								<td width="19" height="19"><a href="javascript:;" onClick="MM_showHideLayers('status_list','','hide')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_off.gif" name="pre_close22" width="19" height="19" border="0" id="pre_close22" onMouseOver="MM_swapImage('pre_close22','','<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
																							</tr>
																						</table>
																						<table width="100%" border="0" cellspacing="0" cellpadding="0">
																							<tr>
																								<td width="5" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif" width="5" height="19"></td>
																								<td bgcolor="#FFFFF7">
																									<table width="98%" border="0" cellpadding="0" cellspacing="3">
																										<?=$actionLayer?>
																									</table>
																								</td>
																								<td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif" width="6" height="6"></td>
																							</tr>
																							<tr>
																								<td width="5" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_07.gif" width="5" height="6"></td>
																								<td height="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif" width="5" height="6"></td>
																								<td width="6" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_09.gif" width="6" height="6"></td>
																							</tr>
																						</table>
																					</td>
																				</tr>
																			</table>
																		</div>
																		<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5">
																	</td>
																	<? } ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn")) { ?>
																	<td nowrap><a href="javascript:checkEdit(document.form1,'RecordID[]','edit.php?field=<?=$li->field?>&order=<?=$li->order?>&pageNo=<?=$li->pageNo?>&numPerPage=<?=$li->page_size?>&MeritType=<?=$MeritType?>&s=<?=$s?>&SchoolYear2=<?=$SchoolYear?>&semester2=<?=$semester?>&targetClass2=<?=$targetClass?>&waived2=<?=$waived?>&approved2=<?=$approved?>&waitApproval2=<?=$waitApproval?>&released2=<?=$released?>&rejected2=<?=$rejected?>')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<? } ?>
																	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteAll") || $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-DeleteOwn")) { ?>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:removeCat(document.form1,'RecordID[]','remove_update.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																	<? } ?>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
												<? } ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?=$li->displayFormat_Split_PIC("Award_Punishment",$PICLocation,$StatusLocation,$checkboxLocation,$actionLocation,$recordIDLocation,"{$image_path}/{$LAYOUT_SKIN}",$historyLocation, $caseIDLocation, $waivedByLocation, $noticeIDLocation, "", $categoryItemLocation, $remarkLocation, "", $MeritCountLocation, $conductMarkLocation);?>
					</td>
				</tr>
			</table><br>
			<input type="hidden" name="pageNo" id="pageNo" value="<?php echo $li->pageNo; ?>"/>
			<input type="hidden" name="order" id="order" value="<?php echo $li->order; ?>"/>
			<input type="hidden" name="field" id="field" value="<?php echo $li->field; ?>"/>
			<input type="hidden" name="page_size_change" id="page_size_change" value=""/>
			<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>"/>
			<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType ?>">
			<input type="hidden" name="s" id="s" value="<?=stripslashes(stripslashes($s))?>"/>
			<input type="hidden" name="clickID" id="clickID" value=""/>
			<input type="hidden" name="id" id="id" value=""/>
			<input type="hidden" name="fromPPC" id="fromPPC" value="<?=$fromPPC?>"/>

			
			<input type="hidden" name="SchoolYear2" id="SchoolYear2" value="<?=$SchoolYear?>"/>
			<input type="hidden" name="semester2" id="semester2" value="<?=$semester?>"/>
			<input type="hidden" name="targetClass2" id="targetClass2" value="<?=$targetClass?>"/>
			<input type="hidden" name="waived2" id="waived2" value="<?=$waived?>"/>
			<input type="hidden" name="approved2" id="approved2" value="<?=$approved?>"/>
			<input type="hidden" name="waitApproval2" id="waitApproval2" value="<?=$waitApproval?>"/>
			<input type="hidden" name="released2" id="released2" value="<?=$released?>"/>
			<input type="hidden" name="rejected2" id="rejected2" value="<?=$rejected?>"/>
			<input type="hidden" name="MeritType2" id="MeritType2" value="<?=$MeritType ?>">
			
			<input type="hidden" name="back_page" id="back_page" value="index.php"/>
			
			</form>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
