<?
// Modifying by:

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();
$ldiscipline = new libdisciplinev12();
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

if($ext != ".CSV")
{
	header("location: import_student.php?CaseID=$CaseID&xmsg=import_failed");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}
if ($ldiscipline->use_subject)
{
	$file_format = array('Class Name','Class Number','Record Date','Semester','Item Code','Subject','Conduct Mark', 'Quantity','Type', 'PIC','Remark');
}
else
{
	$file_format = array('Class Name','Class Number','Record Date','Semester','Item Code','Conduct Mark', 'Quantity','Type', 'PIC','Remark');
}
$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import_student.php?CaseID=$CaseID&xmsg=wrong_header");
	exit();
}

$lu = new libuser();

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_CaseRecord";

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$CurrentPageArr['eDisciplinev12'] = 1;

# tag information
$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Case_Record);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();


### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_class</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_identity_student</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_record_date</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_SettingsSemester</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['RecordType']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline["RecordItem"]."</td>";
if ($ldiscipline->use_subject)
{
	$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_Subject</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Conduct_Mark']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_general_receive</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_Discipline_System_Discipline_Case_Record_PIC</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">$i_UserRemark</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";



$sql = "drop table temp_profile_student_merit_import";
$ldiscipline->db_db_query($sql);
$sql = "create table temp_profile_student_merit_import
	(
		 ClassName varchar(10),
		 ClassNumber int(3),
		 RecordDate datetime,
		 Year   varchar(100),
		 Semester  varchar(100),
		 StudentID int(11),
		 ItemID int(11),
		 ItemText text,
		 ItemCode text,
		 MeritType int(11),
		 Remark text,
		 ProfileMeritType int(11),
		 ProfileMeritCount int(11),
		 ConductScoreChange int(11),
		 Subject varchar(255),
		 PICID  varchar(255),
		 Teacher varchar(255),
		 Count int(5),
		 Category varchar(255),
		 Type varchar(255),
		 StudentName varchar(255)
	)";
$ldiscipline->db_db_query($sql);

$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current,$fr,$to) = $line;
	$SemesterArr[] = $name;
}

$error_occured = 0;
for($i=0;$i<sizeof($data);$i++)
{
	$error = array();
	$PicIDArr = array();
	$MeritType ='';
	$MeritNum = '';
	$MeritTypeName = '';
	$MeritNum = '';
	$ConductScoreChange = '';
	### get data
	if ($ldiscipline->use_subject)
	{
		
		list($ClassName, $ClassNumber, $RecordDate, $Semester,$ItemCode, $SubjectCode, $ConductScore,  $MeritNum, $MeritType,$PIC, $Remark) = $data[$i];
	}
	else
	{
		list($ClassName, $ClassNumber, $RecordDate, $Semester,$ItemCode, $ConductScore,  $MeritNum, $MeritType,$PIC, $Remark) = $data[$i];
	}
	//$today = date("Y-m-d");
	
	/*if(strtotime($RecordDate)>strtotime($today))
	{
		$error['RecordDate'] = $iDiscipline['Award_Punishment_RecordDate_In_Future'];
	}*/
	
	//if($RecordDate
	$Year = GET_ACADEMIC_YEAR_WITH_FORMAT($RecordDate);
	
	//$Year = GET_ACADEMIC_YEAR_WITH_FORMAT($RecordDate);
	# Semester
	$semester_mode = getSemesterMode();
	if($semester_mode==2)
	{
		$Semester = retrieveSemester($RecordDate);
		if(trim($Semester)=='')
		{
			$error['No_Semester'] = $eDiscipline['SemesterSettingError'];		
		}	
	}
	else
	{
		if(trim($Semester)=='')
		{
			$error['No_Semester'] = $iDiscipline['Award_Punishment_Missing_Semester'];
		}
		else
		{
			if(!in_array($Semester,$SemesterArr) && $Semester!='Whole Year')
			{
				$error['Wrong_Semester'] = "No such semester";
			}
		}
	}
	# MeritType
	
	if($MeritType>0)
	{
		$MeritTypeNum = 1;
		$RecordCat = $eDiscipline["AwardRecord"];
	}
	else if($MeritType<0)
	{
		$MeritTypeNum = -1;	
		$RecordCat = $i_Discipline_System_Award_Punishment_Punishments;
	}
	else if($MeritType=='0')
	{
		$MeritTypeNum = -1;	
		$RecordCat = $i_Discipline_System_Award_Punishment_Punishments;
	}
	else
	{
		$error['Wrong_MeritType']=$iDiscipline['Award_Punishment_Wrong_Merit_Type'];
	}
	
	
	
	#RecordDate
	if(trim($RecordDate)=='')
	{
		$error['RecordDate'] = $iDiscipline['Award_Punishment_Missing_RecordDate'];
	}
	else 
	{
		$date_format = false;
		$date_format = $ldiscipline->checkDateFormat($RecordDate);
		
		if(!$date_format)
		{
			$error['RecordDate'] = $iDiscipline['Invalid_Date_Format'];
		}
	}
	
	
	#Subject
	$SubjectName = '--';
	if ($ldiscipline->use_subject)
	{
		if(trim($SubjectCode)!='')
		{
			//$sql = "select count(*) from INTRANET_SUBJECT where SubjectName = '$SubjectCode'";
			$sql = "select SubjectName from INTRANET_SUBJECT where CONCAT('SUBJ',SubjectID) = '$SubjectCode'";
			$SubjectArr = $ldiscipline->returnVector($sql);
			
			if(!$SubjectArr[0])
			{
				$error['No_Subject'] = $iDiscipline['Award_Punishment_No_Subject'];
			}
			else
			{
				$SubjectName = $SubjectArr[0];
			}
			
		}
	}
	#Merit Num
	if(trim($MeritNum)=='')
	{
		$error ['No_MeritNum'] = $iDiscipline['Award_Punishment_No_Merit_Num'];	
	}
	else
	{
		if(!is_numeric($MeritNum))
		{
			$error ['MeritNum_Numeric'] = $iDiscipline['Award_Punishment_Merit_Numeric'];
		}
		else
		{
			$IsFloat = (strpos($MeritNum,"."));
			if($MeritNum<0)
			{
				$error ['MeritNum_Negative'] = $iDiscipline['Award_Punishment_Merit_Negative'];
			}
			if($MeritNum > $ldiscipline->AwardPunish_MAX || $IsFloat)
			{
				$error['MeritNum'] = $iDiscipline['Case_Student_Import_Quantity_OutRange'];
			}
		} 
	
	}
	
	//if($error['Wrong_MeritType']=='')
	{
		$ItemInfo = $ldiscipline->getAwardPunishItemByItemCode(addslashes($ItemCode));
		
		if(sizeof($ItemInfo)==0)
		{
			$error['No_ItemInfo']=$iDiscipline['Award_Punishment_No_ItemCode'];	
		}
		else
		{
			$ItemText = $ItemInfo['ItemName'];
			$ItemID = $ItemInfo['ItemID'];
			$RecordType = $ItemInfo['RecordType'];
			if($MeritTypeNum!=$RecordType && $error['Wrong_MeritType']=='')
			{
				$error['Wrong_MeritType']=$iDiscipline['Award_Punishment_Wrong_Merit_Type'];
			}
			
		}
	}
	### checking - valid class & class number (student found)
	$StudentID = $lu->returnUserID($ClassName, $ClassNumber, 2, '0,1,2');
	
	$lu = new libuser($StudentID);
	$ClassInfo = $ClassName."-".$ClassNumber;
	$StudentName = ($intranet_session_language=='en')?$lu->EnglishName:$lu->ChineseName;
	
	//if($error['Wrong_MeritType']=='')
	{
		include("$intranet_root/lang/lang.en.php");
		# ProfileMeritType
		if($MeritTypeNum==1)
		{
			$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
		}
		else
		{
			$MeritRecordType = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
		}
		include("$intranet_root/lang/lang.".$intranet_session_language.".php");
		$RecordType='';
		
		for($a=1;$a<sizeof($MeritRecordType)+1;$a++)
		{
			if($MeritType == $MeritRecordType[$a][0] && strlen($MeritType)==strlen($MeritRecordType[$a][0]))
			{
				$RecordType = $MeritRecordType[$a][0];
				$MeritTypeName = $MeritRecordType[$a][1];
				
			}
		}
		
		if(trim($RecordType)=='' && $error['Wrong_MeritType']=='')
		{
			$error['MeritType']=$iDiscipline['Award_Punishment_Wrong_Merit_Type'];
		}
	}	
		
	//if(!$error['Wrong_MeritType'])
	{
		if(trim($ConductScore)=='')
		{
			$error['ConductScore']=$iDiscipline['Good_Conduct_Missing_Conduct_Score'];
		}
		else
		{
			if($MeritTypeNum==-1 && $ConductScore>0)
			{
				$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_Negative'];
			}
			if($MeritTypeNum==1 && $ConductScore<0)
			{
				$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_Positive'];
			}
			$IsFloat = (strpos($ConductScore,"."));
			if(!is_numeric($ConductScore) || $IsFloat || abs($ConductScore) > $ldiscipline->ConductMarkIncrement_MAX)
			{
				$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_OutRange'];
			}
			//$ConductScoreChange = $ConductScore * $MeritTypeNum;
			$ConductScoreChange = $ConductScore;
			if($ConductScoreChange > $ldiscipline->ConductMarkIncrement_MAX)
			{
				$error['ConductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_OutRange'];
			}
		}
	}
	
	# Remark
	$Remark = intranet_htmlspecialchars(addslashes($Remark));
	$PicArr = explode(",",$PIC);
	$PicName = array();
	for($ct=0;$ct<sizeof($PicArr);$ct++)
	{
		$sql = "select UserID as PICID, ".getNamefieldByLang()." as Name from INTRANET_USER where UserLogin = '".$PicArr[$ct]."' and RecordType = 1";
		$tmpResult = $lu->returnArray($sql);
		$PicID = $tmpResult[0]['PICID'];	
		
		if(trim($PicID)=="")
		{
			$error['PIC']=$iDiscipline['Good_Conduct_No_Staff'];
		}
		else
		{
			$PicIDArr[] = $PicID;
			$PicName[] = $tmpResult[0]['Name'];
		}
	}

	if(trim($StudentID)=="")
	{
		$error['No_Student'] = $iDiscipline['Good_Conduct_No_Student'];
	}
	
	$css = (sizeof($error)==0) ? "tabletext":"red";

	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">". $ClassInfo ."</td>";
	$x .= "<td class=\"$css\">". $StudentName ."</td>";
	$x .= "<td class=\"$css\">". $RecordDate ."</td>";
	$x .= "<td class=\"$css\">". $Semester ."</td>";
	$x .= "<td class=\"$css\">". $RecordCat ."</td>";
	$x .= "<td class=\"$css\">". intranet_htmlspecialchars(($ItemText)) ."</td>";
	if ($ldiscipline->use_subject)
	{
		$x .= "<td class=\"$css\">". $SubjectName ."</td>";
	}
	$x .= "<td class=\"$css\">". $ConductScoreChange ."</td>";
	$x .= "<td class=\"$css\">". $MeritNum." ".$MeritTypeName."</td>";
	$x .= "<td class=\"$css\">". implode(",",$PicName) ."</td>";
	$x .= "<td class=\"$css\">". stripslashes($Remark) ."</td>";
	$x .= "<td class=\"$css\">";
	
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .=$Value.'<br/>';
		}	
	}
	else
	{	
		$x .= "--";
	}
	$x.="</td>";
	$x .= "</tr>";
		
	$sql = "insert into temp_profile_student_merit_import values ('$ClassName','$ClassNumber','$RecordDate','$Year','$Semester' ,'$StudentID','$ItemID','".addslashes($ItemText)."','".addslashes($ItemCode)."','$MeritTypeNum','$Remark','$RecordType
	','$MeritNum','$ConductScoreChange','".addslashes(addslashes($SubjectName))."','".implode(',',$PicIDArr)."','".implode(",",$PicName)."',$MeritNum,'$RecordCat','$MeritType','".addslashes($StudentName)."')";
	$ldiscipline->db_db_query($sql);


	if($error)
	{
		$error_occured++;
	}	
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_student.php?CaseID=$CaseID'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")."&nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import_student.php?CaseID=$CaseID'");
}

?>

<br />
<form name="form1" method="post" action="import_student_update.php" enctype="multipart/form-data">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="CaseID" id="CaseID" value="<?=$CaseID?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>