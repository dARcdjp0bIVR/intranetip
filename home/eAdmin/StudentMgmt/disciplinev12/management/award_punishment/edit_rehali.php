<?php
# using: 

################ Change Log Start ######################################
#
# 	Date	:	2015-07-28 (Bill)	[2015-0611-1642-26164]
# 				Copy from detail.php
#
################ Change Log End ######################################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

if(empty($RecordID))	header("Location: index.php");
if(is_array($RecordID))	$RecordID = $RecordID[0];

$ldiscipline = new libdisciplinev12();

//# Check access right
//if(!($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditAll") || (($ldiscipline->isMeritRecordPIC($RecordID) || $ldiscipline->isMeritRecordOwn($RecordID)) && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-EditOwn"))))
//{
//		$ldiscipline->NO_ACCESS_RIGHT_REDIRECT("","$back_page?field=$field&order=$order&pageNo=$pageNo&numPerPage=$page_size&MeritType=$MeritType&s=$s&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&waived=$waived&approved=$approved&waitApproval=$waitApproval&released=$released&rejected=$rejected");
//}

if(!$sys_custom['eDiscipline']['CSCProbation']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT("","$back_page?field=$field&order=$order&pageNo=$pageNo&numPerPage=$page_size&MeritType=$MeritType&s=$s&SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&waived=$waived&approved=$approved&waitApproval=$waitApproval&released=$released&rejected=$rejected");
}

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Management_AwardPunishment";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["RecordDetails"], "");
$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['CWCRehabil']['ResponseToRequest'], "");

# retrieve data
$datainfo = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($RecordID);

$data = $datainfo[0];
$student = array($data['StudentID']);
$ItemID = $data['ItemID'];
$CatID = $ldiscipline->returnCatIDByItemID($ItemID);

# check user can access/edit this item or not
$AccessItem = 1;
//if($ldiscipline->IS_ADMIN_USER($UserID))
//{
//	$AccessItem = 1;
//}
//else
//{
//	$ava_item = $ldiscipline->Retrieve_User_Can_Access_AP_Category_Item($UserID, 'item');
//	if($ava_item[$CatID])
//		$AccessItem = in_array($ItemID, $ava_item[$CatID]);
//}

// Get Post data
$RecordType = $data['MeritType'];
$MeritNum = $data['ProfileMeritCount'];
$ConductScore = abs($data['ConductScoreChange']);
$StudyScore = abs($data['SubScore1Change']);
$ProfileMeritType = $data['ProfileMeritType'];
$isAcc = $data['fromConductRecords'];
$RecordDate = $data['RecordDate'];
$RecordInputDate = $data['RecordInputDate'];
$ActionDueDate = $data['ActionDueDate'];
//$semester = $data['Semester'];
$thisStatus = $data['RecordStatus'];
//$isWaived = $data['RecordStatus']==DISCIPLINE_STATUS_WAIVED ? 1 : 0;

# build html part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
foreach($student as $k=>$StudentID)
{
	$lu = new libuser($StudentID);
	$student_name = $lu->UserNameLang();
	$class_name = $lu->ClassName;
	$class_no = $lu->ClassNumber;
	
	$selected_student_table .= "<tr><td width='50%' valign='top'><a href='javascript:viewCurrentRecord($StudentID)' class='tablelink'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle'>";
	$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
	$selected_student_table .= "</a></td></tr>";
}
$selected_student_table .= "</table>";

// PIC name
if($data['RehabilPIC']){
	// Rehabilitation Teacher PIC
	$lu = new libuser($data['RehabilPIC']);
	$teacher_pic_name = $lu->UserName();
}

// Rehabilitation Status
$status_ary = array();
$status_ary[0] = $Lang['eDiscipline']['CWCRehabil']['Status']['WaitingApproval'];
$status_ary[1] = $Lang['eDiscipline']['CWCRehabil']['Status']['Rejected'];
$status_ary[2] = $Lang['eDiscipline']['CWCRehabil']['Status']['Processing'];
$status_ary[3] = $Lang['eDiscipline']['CWCRehabil']['Status']['Complete'];
$status_ary[4] = $Lang['eDiscipline']['CWCRehabil']['Status']['Incomplete'];
$status_select = getSelectByAssoArray($status_ary, " id='RehabilStatus' name='RehabilStatus'", $data['RehabilStatus'], 0, 1);

// Teacher Response
$response_content = "";
if($data['RehabilFeedback']){
	$response_content = str_replace("<br />","",$data['RehabilFeedback']);
}

# Start layout
$linterface->LAYOUT_START();

?>

<SCRIPT LANGUAGE=Javascript>
<!--
function formSubmit(){
	var reh_response_content = $('#reh_response').val();
	
	if(reh_response_content==""){
		alert('<?=$Lang['eDiscipline']['CWCRehabil']['PleaseFillinReason']?>');
		return false;
	}
	
	document.form1.submit();
}

function click_cancel()
{
	<?if($back_page=="index.php") {?>
		window.location="index.php?SchoolYear=<?=$SchoolYear?>&semester=<?=$semester?>&targetClass=<?=$targetClass?>&num_per_page=<?=$num_per_page?>&pageNo=<?=$pageNo?>&order=<?=$order?>&field=<?=$field?>&page_size_change=<?=$page_size_change?>&numPerPage=<?=$numPerPage?>&MeritType=<?=$MeritType?>&s=<?=$s?>&clickID=<?=$clickID?>&field=<?=$field?>&SchoolYear=<?=$SchoolYear2?>&semester=<?=$semester2?>&targetClass=<?=$targetClass2?>&waived=<?=$waived2?>&approved=<?=$approved2?>&waitApproval=<?=$waitApproval2?>&rejected=<?=$rejected2?>&released=<?=$released2?>&fromPPC=<?=$fromPPC?>&passedActionDueDate=<?=$passedActionDueDate?>&pic=<?=$pic?>";
	<? } else {?>
		document.form1.action="<?=$back_page?>";
		document.form1.submit();
	<? } ?>
}

//-->
</SCRIPT>

<div id="ref_list"></div>

<form name="form1" method="POST" action="edit_rehali_update.php">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_identity_student?></td>
			<td valign="top" class="tablerow2"><?=$selected_student_table?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$Lang['eDiscipline']['CWCRehabil']['ApplicationContent']?> -</i></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['CWCRehabil']['FollowUpTeacher']?></td>
			<td><?=$teacher_pic_name?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['CWCRehabil']['RehabilReason']?></td>
			<td><?=nl2br($data['RehabilReason'])?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire">* </span><?=$Lang['eDiscipline']['CWCRehabil']['Status']['Title']?></td>
			<td><?=$status_select?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['CWCRehabil']['RehabilResponse']?></td>
			<td><textarea id="reh_response" name="reh_response" rows="5" cols="80"><?=$response_content?></textarea></td>
		</tr>
	</table>
	
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "button", "formSubmit()")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "click_cancel();")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<input type="hidden" name="semester_flag" id="semester_flag" value="1" />
<input type="hidden" name="RecordID" id="RecordID" value="<?=$RecordID?>" />

<input type="hidden" name="temp_flag" id="temp_flag" value="0" />

<!-- for back action //-->
<input type="hidden" name="num_per_page" id="num_per_page" value="<?=$num_per_page?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
<input type="hidden" name="s" id="s" value="<?=$s?>" />
<input type="hidden" name="clickID" id="clickID" value="<?=$clickID?>" />
<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
<input type="hidden" name="fieldname" id="fieldname" value="Attachment[]" />
<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="FolderLocation" id="FolderLocation" value="<?=$sessionTime?>" />

<? if($back_page=="index.php") {?>
<input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="targetClass" id="targetClass" value="<?=$targetClass?>" />
<input type="hidden" name="waived" id="waived" value="<?=$waived?>"/>
<input type="hidden" name="approved" id="approved" value="<?=$approved?>"/>
<input type="hidden" name="waitApproval" id="waitApproval" value="<?=$waitApproval?>"/>
<input type="hidden" name="released" id="released" value="<?=$released?>"/>
<input type="hidden" name="rejected" id="rejected" value="<?=$rejected?>"/>
<input type="hidden" name="MeritType2" id="MeritType2" value="<?=$MeritType?>"/>
<input type="hidden" name="pic" id="pic" value="<?=$pic?>"/>
<input type="hidden" name="back_page" id="back_page" value="<?=$back_page?>"/>
<? } else {?>
<input type="hidden" name="id" id="id" value="<?=$RecordID?>" />
<input type="hidden" name="SchoolYear2" id="SchoolYear2" value="<?=$SchoolYear2?>" />
<input type="hidden" name="semester2" id="semester2" value="<?=$semester2?>" />
<input type="hidden" name="targetClass2" id="targetClass2" value="<?=$targetClass2?>" />
<input type="hidden" name="waived2" id="waived2" value="<?=$waived2?>"/>
<input type="hidden" name="approved2" id="approved2" value="<?=$approved2?>"/>
<input type="hidden" name="waitApproval2" id="waitApproval2" value="<?=$waitApproval2?>"/>
<input type="hidden" name="released2" id="released2" value="<?=$released2?>"/>
<input type="hidden" name="rejected2" id="rejected2" value="<?=$rejected2?>"/>
<input type="hidden" name="MeritType2" id="MeritType2" value="<?=$MeritType2?>" />
<!--<input type="hidden" name="pic" value="<?=$pic?>"/>-->
<input type="hidden" name="back_page" id="back_page" value="<?=$back_page?>"/>
<? } ?>
<input type="hidden" name="fromPPC" id="fromPPC" value="<?=$fromPPC?>"/>
<input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="<?=$passedActionDueDate?>"/>
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>