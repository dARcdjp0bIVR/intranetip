<?php
// Modifying by: YW

########## Change Log ###############
#
#   Date    :   2020-11-06  (Bill)  [2020-0824-1151-40308]
#               - Support Follow-up Feedback    ($sys_custom['eDiscipline']['APFollowUpRemarks'])
#
#	Date	:	2020-10-08 (YatWoon) [Case#2020-1007-1112-42170]
#				change PIC selection (use common_choose) (replace "U")
#
#   Date    :   2019-04-30 (Bill)
#               prevent Cross-site Scripting
#
#   Date    :   2019-03-15  (Bill)  [2018-0710-1351-39240]
#               hide notice settings ($sys_custom['eDiscipline']['AP_Step3_HideNoticeSetting'])
#
#	Date	:	2017-10-18	(Bill)	[2017-1003-1124-05235]
#				apply temp array to reduce query number
#
#	Date	:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#				support Study Score Limit Checking ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date	:	2017-06-28 (Bill)	[2015-0120-1200-33164]
#				Support display Merit Item with target Tag Type only
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				support Activity Score
#
#	Date	:	2017-03-02	(Bill)	[2016-0823-1255-04240]
#				default uncheck checkbox "Send email to notify parents"	($sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox'])
#
#	Date	:	2016-12-05	(Bill)	[2016-1129-1154-50236]
#				fixed: checkbox of Send Notice moved when selected Detention checkbox, set inline style to remove float setting
#
#	Date	:	2016-06-07	(Bill)	[2016-0329-1757-08164]
#	Detail	:	$sys_custom['eDiscipline']['CSCProbation']
#				- add hidden input fields for probation setting
#
#	Date	:	2016-04-21 (Bill)	[2016-0405-1012-27236]
#				modified changeCat(), fix js error result in cannot submit AP records
#
#	Date	:	2015-04-30 (Bill)	[2014-1216-1347-28164]
#				add option to send push message to notify parent
#				fixed: disable finish button after click
#
#	Date	:	2015-04-10 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#				auto check "Detention" and/or "Send notice via eNotice" when $DefaultDetention is true and/or $DefaultSendNotice is true
#				improved: display error message using div "show_result"
#
#	Date	:	2015-02-10 (Bill) [2015-0120-1549-57066]
#				modified coding that generate detention session list - to improve performance
#
#	Date	:	2014-12-11 (Henry)
#				disable continue button after user click 
#
#	Date	:	2013-11-18 (YatWoon)
#				Customization: Add 2 extra fields: FileNumber, RecordNumber, $sys_custom['eDiscipline']['MST']['AP_Extra_Fields']	[Case#2013-0520-1008-41072]
#
#	Date	:	2013-05-15 (YatWoon)
#	Datail	:	add RecordInputDate (customization for yy3 )
#
#	Date	:	2011-01-13 (Henry Chow)
#	Detail 	:	if record comes from "Detention > Student List", no need to select detention session
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	select default eNotice Template
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	add attachment to record
#
#####################################
//error_reporting(E_ERROR | E_WARNING | E_PARSE); 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

### Handle SQL Injection + XSS [START]
$student = $_POST['student'];
if(is_array($student) && sizeof($student) > 0) {
    foreach($student as $thisKey => $thisStudentID) {
        $student[$thisKey] = IntegerSafe($thisStudentID);
    }
}

if(!intranet_validateDate($RecordDate)) {
    $RecordDate = date('Y-m-d');
}
if(!intranet_validateDate($RecordInputDate)) {
    $RecordInputDate = '';
}
if(!intranet_validateDate($ActionDueDate)) {
    $ActionDueDate = '';
}
if (is_array($PIC) && sizeof($PIC) > 0) {
    foreach($PIC as $thisKey => $thisPICID) {
		$thisPICID = str_replace("U", "", $thisPICID);
        $PIC[$thisKey] = IntegerSafe($thisPICID);
    }
}
$sessionTime = cleanCrossSiteScriptingCode($sessionTime);

$record_type = IntegerSafe($record_type);
$CatID = IntegerSafe($CatID);
$ItemID = IntegerSafe($ItemID);
if($ldiscipline->AP_Interval_Value) {
    $MeritNum = (float)$MeritNum;
}
else {
    $MeritNum = IntegerSafe($MeritNum);
}
$MeritType = IntegerSafe($MeritType);
if($sys_custom['eDiscipline']['ConductMark1DecimalPlace']) {
    $ConductScore = (float)$ConductScore;
}
else {
    $ConductScore = IntegerSafe($ConductScore);
}
$StudyScore = IntegerSafe($StudyScore);
$ActivityScore = IntegerSafe($ActivityScore);
$semester = cleanCrossSiteScriptingCode($semester);
$Subject = IntegerSafe($Subject);
$SubjectID = IntegerSafe($SubjectID);
$TargetTagType = cleanCrossSiteScriptingCode($TargetTagType);
$detentionID = IntegerSafe($detentionID);

$applyProbation = IntegerSafe($applyProbation);
$maxStudyScore = IntegerSafe($maxStudyScore);

if($detentionFlag != 'YES' && $detentionFlag != 'NO' && $detentionFlag != '') {
    $detentionFlag = '';
}
if($noticeFlag != 'YES' && $noticeFlag != 'NO' && $noticeFlag != '') {
    $noticeFlag = '';
}
### Handle SQL Injection + XSS [END]

$lnotice = new libnotice();
$luser = new libuser();
$ls = new libfilesystem();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New");

# Remark
$remark = stripslashes(intranet_htmlspecialchars($remark));

# [2020-0824-1151-40308] Follow-up Remark
$followUpRemark = stripslashes(intranet_htmlspecialchars($followUpRemark));

# Check semester 
$semInfo = getAcademicYearAndYearTermByDate($RecordDate);
$semester = $semInfo[1];

// [2014-1216-1347-28164]
$canSendPushMsg = $plugin['eClassApp'] && $sys_custom['eDiscipline']['SendDetentionPushMessage'];

$linterface = new interface_html();

# Menu highlight setting
$CurrentPage = "Management_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

# Step information
$STEPS_OBJ[] = array($eDiscipline["SelectStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["AddRecordToStudents"], 0);
$STEPS_OBJ[] = array($eDiscipline["SelectActions"], 1);
$STEPS_OBJ[] = array($eDiscipline["FinishNotification"], 0);

# Navigation bar
$PAGE_NAVIGATION[] = array($eDiscipline['RecordList'], "index.php");
$PAGE_NAVIGATION[] = array($eDiscipline["NewRecord"], "");

# Get data from preious page
// $student = $_POST['student'];
if(empty($student))	header("Location: new1.php");

if(isset($PIC)) {
	$PIC = array_unique($PIC);
}

if(sizeof($Attachment) > 0) {
	foreach($student as $k=>$StudentID) {
		# copy attachment 	
		$originalPath = "$file_path/file/disciplinev12/award_punishment/".$sessionTime;
		$path = "$file_path/file/disciplinev12/award_punishment/".$sessionTime."_".$StudentID."tmp";
		//echo $originalPath.'<br>'.$path.'<br>';
		
		if (is_dir($originalPath) && !is_dir($path))
		{
			$result = $ls->folder_new($path);
			//echo "<ul>#$result<br>$originalPath<br>$path";
			chmod($path, 0777);
		}
		
		//echo $path;
		$a = new libfiletable("", $originalPath, 0, 0, "");
		$files = $a->files;
	
		while (list($key, $value) = each($files)) {
			$file = "$file_path/file/disciplinev12/award_punishment/".$sessionTime."/".$files[$key][0];
			$newfile = "$file_path/file/disciplinev12/award_punishment/".$sessionTime."_".$StudentID."tmp/".$files[$key][0];
			//echo '<li>'.$file.'<br>'.$newfile;
			if(copy($file, $newfile)) {
				chmod("$file_path/file/disciplinev12/award_punishment/".$sessionTime."_".$StudentID."tmp/".$files[$key][0], 0777);
			}
		}
	}
}

// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] - get auto select detention and eNotice option value
$catDefaultOption = $ldiscipline->getAPCategoryInfoBYCatID($CatID);
$DefaultDetention = $catDefaultOption['AutoSelectDetention'];
$DefaultSendNotice = $catDefaultOption['AutoSelecteNotice'];

# Build html part
$selected_student_table = "<table width='100%' border='0' cellspacing='0' cellpadding='5'>";
foreach($student as $k=>$StudentID)
{
	$stu = $ldiscipline->getStudentNameByID($StudentID);
	$student_name = $stu[0][1];
	$class_name = $stu[0][2];
	$class_no = $stu[0][3];
	
	$selected_student_table .= "<tr><td width='50%' valign='top'><a href='javascript:viewCurrentRecord($StudentID)' class='tablelink'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' border='0' align='absmiddle'>";
	$selected_student_table .= $class_name. ($class_no?"-":"") .$class_no." ".$student_name;
	$selected_student_table .= "</a></td></tr>";
}
$selected_student_table .= "</table>";

########## For Detention (Begin) ##########

		if ($_POST["submit_flag"] == "YES")
		{
		    for ($j=1; $j<=sizeof($student); $j++)
		    {
		        ${"StudentID".$j} = IntegerSafe(${"StudentID".$j});
		        //${"StudentName".$j}
		        ${"CategoryID".$j} = IntegerSafe(${"CategoryID".$j});
		        ${"SelectNotice".$j} = IntegerSafe(${"SelectNotice".$j});
		        //intranet_htmlspecialchars(${"TextAdditionalInfo".$j}
		    }
		    
			if ($detentionFlag == "YES")
			{
            
			// Get submitted data
			$submitStudent = $student;
			$submitDetentionCount0 = $_POST["DetentionCount0"];
			$submitDetentionCount0 = IntegerSafe($submitDetentionCount0);

			for ($k=1; $k<=$submitDetentionCount0; $k++) {
			    ${"submitSelectSession0_".$k} = $_POST["SelectSession0_".$k];
			    if (${"submitSelectSession0_".$k} <> "AUTO" && ${"submitSelectSession0_".$k} <> "LATER") {
			        ${"submitSelectSession0_".$k} = IntegerSafe(${"submitSelectSession0_".$k});
			    }
			}
			$submitTextRemark0 = $_POST["TextRemark0"];
			
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
			    ${"submitDetentionCount".$l} = $_POST["DetentionCount".$l];
			    ${"submitDetentionCount".$l} = IntegerSafe(${"submitDetentionCount".$l});
			    
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					${"submitSelectSession".$l."_".$m} = $_POST["SelectSession".$l."_".$m];
					if (${"submitSelectSession".$l."_".$m} <> "AUTO" && ${"submitSelectSession".$l."_".$m} <> "LATER") {
					    ${"submitSelectSession".$l."_".$m} = IntegerSafe(${"submitSelectSession".$l."_".$m});
					    
						$selectedSessionForOneStudent[$l][] = ${"submitSelectSession".$l."_".$m};
						$allSelectedSession[] = ${"submitSelectSession".$l."_".$m};
					}
				}
				${"submitTextRemark".$l} = $_POST["TextRemark".$l];
			}
            
			// Check past session and available session
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				$assignedSessionForEachStudent = array();
				
				// if Auto assign session, get detention list - performance issue [2015-0120-1549-57066]
				$_hasAuto = false;
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					if (${"submitSelectSession".$l."_".$m} == "AUTO") {
						$_hasAuto = true;
						break;
					}
				}
				if ($_hasAuto) {
					$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $submitStudent[$l-1], "ASC");
				}
				
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					if (${"submitSelectSession".$l."_".$m} == "AUTO") {
						// 2015-02-10: commented - performance issue
//						$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $submitStudent[$l-1], "ASC");
						$TmpDetentionID = "";
						if (is_array($allSelectedSession)) {
							$TmpSessionArrayCount = array_count_values($allSelectedSession);
						}
						$AutoSuccess = false;
						$currentTime = date('Y-m-d H:i:s');
						
						for ($i=0; $i<sizeof($result); $i++) {
							list($TmpDetentionID,$TmpDetentionDate,$TmpDetentionDayName,$TmpStartTime,$TmpEndTime,$TmpPeriod,$TmpLocation,$TmpForm,$TmpPIC,$TmpVacancy,$TmpAssigned,$TmpAvailable) = $result[$i];
							
							$detentionSession = $TmpDetentionDate." ".$TmpStartTime;
							if($currentTime<$detentionSession) {
								if (!(is_array($selectedSessionForOneStudent[$l]) && in_array($TmpDetentionID, $selectedSessionForOneStudent[$l])) && ($TmpSessionArrayCount[$TmpDetentionID] < $TmpAvailable) && !($ldiscipline->checkStudentInSession($submitStudent[$l-1], $TmpDetentionID))) {
									$AutoSuccess = true;
									break;
								}
							}
						}
						if ($TmpDetentionID == "" || $AutoSuccess == false) {
							$AvailableFlag[$l][$m] = "NO";
						} else {
							$AvailableFlag[$l][$m] = "YES";
							$selectedSessionForOneStudent[$l][] = $TmpDetentionID;
							$allSelectedSession[] = $TmpDetentionID;
							$AutoDetentionID[$l][$m] = $TmpDetentionID;
						}
					} else if (${"submitSelectSession".$l."_".$m} != "LATER") {
						$StudentInSessionFlag[$l][$m] = ($ldiscipline->checkStudentInSession($submitStudent[$l-1], ${"submitSelectSession".$l."_".$m}))?"YES":"NO";
						$PastFlag[$l][$m] = ($ldiscipline->checkPastByDetentionID(${"submitSelectSession".$l."_".$m}))?"YES":"NO";
						if (is_array($assignedSession)) {		// array for sessions that are not assigned by AUTO
							$TmpAssignedSessionArrayCount = array_count_values($assignedSession);
						}
						if (in_array(${"submitSelectSession".$l."_".$m}, $assignedSessionForEachStudent)) {
							$StudentInSessionFlag[$l][$m] = "YES";
						}
						$FullFlag[$l][$m] = ($ldiscipline->checkFullByDetentionID(${"submitSelectSession".$l."_".$m}, $TmpAssignedSessionArrayCount[${"submitSelectSession".$l."_".$m}] + 1))?"YES":"NO";
						if ($StudentInSessionFlag[$l][$m] == "NO" && $PastFlag[$l][$m] == "NO" && $FullFlag[$l][$m] == "NO") {
							$assignedSession[] = ${"submitSelectSession".$l."_".$m};
							$assignedSessionForEachStudent[] = ${"submitSelectSession".$l."_".$m};
						}
					}
				}
			}

			$FailFlag = false;
			for ($l=1; $l<=sizeof($submitStudent); $l++) {
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
    				$allAvailableFlag[] = $AvailableFlag[$l][$m];
    				$allStudentInSessionFlag[] = $StudentInSessionFlag[$l][$m];
    				$allPastFlag[] = $PastFlag[$l][$m];
    				$allFullFlag[] = $FullFlag[$l][$m];
				}
			}
			if (!((is_array($allAvailableFlag) && in_array("NO", $allAvailableFlag)) || (is_array($allStudentInSessionFlag) && in_array("YES", $allStudentInSessionFlag)) || (is_array($allPastFlag) && in_array("YES", $allPastFlag)) || (is_array($allFullFlag) && in_array("YES", $allFullFlag)))) {
				for ($l=1; $l<=sizeof($submitStudent); $l++) {
					for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
						// StudentID
						$DetentionArr[] = $submitStudent[$l-1];
						// DetentionID
						if (${"submitSelectSession".$l."_".$m}=="AUTO") {
							$DetentionArr[] = $AutoDetentionID[$l][$m];
						} else if (${"submitSelectSession".$l."_".$m}=="LATER") {
							$DetentionArr[] = "";
						} else {
							$DetentionArr[] = ${"submitSelectSession".$l."_".$m};
						}
						// Remark
						$DetentionArr[] = ${"submitTextRemark".$l};
					}
				}
?>

<body onLoad="document.form2.submit();">
<form name="form2" method="post" action="new3_update.php">

<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />
<? } ?>
<input type="hidden" name="TargetTagType" value="<?=$TargetTagType?>" />

<!-- Step 2 data //-->
<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
<input type="hidden" name="RecordInputDate" id="RecordInputDate" value="<?=$RecordInputDate?>" />
<input type="hidden" name="ActionDueDate" id="ActionDueDate" value="<?=$ActionDueDate?>" />
	<? if(isset($picType) && $picType==2) { ?>
		<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$picName?>">
	<? }
	else { ?>
		<? foreach($PIC as $k=>$d) { ?>
			<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
		<? } ?>
	<? } ?>
<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
<? if($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
    <input type="hidden" name="followUpRemark" id="followUpRemark" value="<?=$followUpRemark?>" />
<? } ?>
<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
<input type="hidden" name="ActivityScore" id="ActivityScore" value="<?=$ActivityScore?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="picType" id="picType" value="<?=$picType?>" />
<input type="hidden" name="picName" id="picName" value="<?=$picName?>" />
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
<input type="hidden" name="detentionID" id="detentionID" value="<?=$detentionID?>" />

<? if($sys_custom['eDiscipline']['CSCProbation']) {?>
	<input type="hidden" name="applyProbation" id="applyProbation" value="<?=$applyProbation?>" />
<? } ?>
	
<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
	<input type="hidden" name="maxStudyScore" id="maxStudyScore" value="<?=$maxStudyScore?>" />
<? } ?>

<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) 
{ 
	$file_number = intranet_htmlspecialchars($file_number);
	$record_number = intranet_htmlspecialchars($record_number);
?>
<input type="hidden" name="file_number" id="file_number" value="<?=$file_number?>" />
<input type="hidden" name="record_number" id="record_number" value="<?=$record_number?>" />
<? } ?>
	
<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<? foreach($DetentionArr as $k=>$d) { ?>
	<input type="hidden" name="DetentionArr[]" id="DetentionArr[]" value="<?=intranet_htmlspecialchars($d)?>" />
<? } ?>
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
<? for ($j=1; $j<=sizeof($student); $j++) { ?>
	<input type="hidden" name="StudentID<?=$j?>" id="StudentID<?=$j?>" value="<?=${"StudentID".$j}?>" />
	<input type="hidden" name="StudentName<?=$j?>" id="StudentName<?=$j?>" value="<?=${"StudentName".$j}?>" />
	<input type="hidden" name="SelectNotice<?=$j?>" id="SelectNotice<?=$j?>" value="<?=${"SelectNotice".$j}?>" />
	<input type="hidden" name="TextAdditionalInfo<?=$j?>" id="TextAdditionalInfo<?=$j?>" value="<?=intranet_htmlspecialchars(${"TextAdditionalInfo".$j})?>" />
	
	<?php if($canSendPushMsg){ ?>
		<input type="hidden" name="eClassAppNotify<?=$j?>" id="eClassAppNotify<?=$j?>" value="<?=${"eClassAppNotify".$j}?>" />
	<?php } ?>
	
	<input type="hidden" name="emailNotify<?=$j?>" id="emailNotify<?=$j?>" value="<?=${"emailNotify".$j}?>" />
<? } ?>

</form>
</body>

<?
			}
			else {
				$FailFlag = true;
			}
			
			}
			else {		// Condition ($detentionFlag != "YES")	
?>

<body onLoad="document.form3.submit();">
<form name="form3" method="post" action="new3_update.php">

<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />
<? } ?>
<input type="hidden" name="TargetTagType" value="<?=$TargetTagType?>" />

<!-- Step 2 data //-->
<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
<input type="hidden" name="RecordInputDate" id="RecordInputDate" value="<?=$RecordInputDate?>" />
<input type="hidden" name="ActionDueDate" id="ActionDueDate" value="<?=$ActionDueDate?>" />
	<? if(isset($picType) && $picType==2) { ?>
		<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$picName?>">
	<? } else { ?>
		<? foreach($PIC as $k=>$d) { ?>
			<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
		<? } ?>
	<? } ?>
<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
<? if($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
    <input type="hidden" name="followUpRemark" id="followUpRemark" value="<?=$followUpRemark?>" />
<? } ?>
<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
<input type="hidden" name="ActivityScore" id="ActivityScore" value="<?=$ActivityScore?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="picType" id="picType" value="<?=$picType?>" />
<input type="hidden" name="picName" id="picName" value="<?=$picName?>" />
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
<input type="hidden" name="detentionID" id="detentionID" value="<?=$detentionID?>" />

<? if($sys_custom['eDiscipline']['CSCProbation']) {?>
	<input type="hidden" name="applyProbation" id="applyProbation" value="<?=$applyProbation?>" />
<? } ?>
	
<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
	<input type="hidden" name="maxStudyScore" id="maxStudyScore" value="<?=$maxStudyScore?>" />
<? } ?>

<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) 
{ 
	$file_number = intranet_htmlspecialchars($file_number);
	$record_number = intranet_htmlspecialchars($record_number);
?>
<input type="hidden" name="file_number" id="file_number" value="<?=$file_number?>" />
<input type="hidden" name="record_number" id="record_number" value="<?=$record_number?>" />
<? } ?>

<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
<? for ($j=1; $j<=sizeof($student); $j++) { ?>
	<input type="hidden" name="StudentID<?=$j?>" id="StudentID<?=$j?>" value="<?=${"StudentID".$j}?>" />
	<input type="hidden" name="StudentName<?=$j?>" id="StudentName<?=$j?>" value="<?=${"StudentName".$j}?>" />
	<input type="hidden" name="SelectNotice<?=$j?>" id="SelectNotice<?=$j?>" value="<?=${"SelectNotice".$j}?>" />
	<input type="hidden" name="TextAdditionalInfo<?=$j?>" id="TextAdditionalInfo<?=$j?>" value="<?=intranet_htmlspecialchars(${"TextAdditionalInfo".$j})?>" />
	
	<?php if($canSendPushMsg){ ?>
		<input type="hidden" name="eClassAppNotify<?=$j?>" id="eClassAppNotify<?=$j?>" value="<?=${"eClassAppNotify".$j}?>" />
	<?php } ?>
	
	<input type="hidden" name="emailNotify<?=$j?>" id="emailNotify<?=$j?>" value="<?=${"emailNotify".$j}?>" />
<? } ?>

</form>
</body>
<?
			}
		}		// end of submit_flag = "YES"
		
		$session_times = $ldiscipline->DetentinoSession_MAX ? $ldiscipline->DetentinoSession_MAX : 10;
		for ($i = 1; $i <= $session_times ; $i++) $OptionForDetentionCount .= "<option value=\"$i\">$i</option>";
		
		// 2015-02-10: move out from getSelectSessionString() - to solve the performance issue [2015-0120-1549-57066]
		// Preset String
		$label = $ldiscipline->displayDetentionSessionDefaultString();
		$TmpAllForms = $ldiscipline->getAllFormString();
		
		// 2015-02-10: modified - to solve the performance issue
//		function getSelectSessionString($parIndex="", $parSessionNumber="", $parUserID="") {
		function getSelectSessionString($parIndex="", $parSessionNumber="", $resultCount="", $row_avaliable_option="")
		{
			global $ldiscipline, $i_Discipline_Auto_Assign_Session, $i_Discipline_Put_Into_Unassign_List, $i_Discipline_All_Forms;
			global $i_Discipline_Date, $i_Discipline_Time, $i_Discipline_Location, $i_Discipline_Form, $i_Discipline_Vacancy, $i_Discipline_PIC, $label, $TmpAllForms;
			
			// 2015-02-10: commented - to solve the performance issue
//			$TmpAllForms = $ldiscipline->getAllFormString();
//			if ($parUserID == "") {
//				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");
//			} else {
//				$result = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $parUserID, "ASC");
//			}

			$SelectSession = "<table><tr><td>";
			$SelectSession .= "<select name=\"SelectSession".$parIndex."_".$parSessionNumber."\" id=\"SelectSession".$parIndex."_".$parSessionNumber."\" class=\"formtextbox\">";
			
			// Hide auto assign option if no available session
			$assignLater = "";
			if ($resultCount > 0) {
				$SelectSession .= "<option class=\"row_avaliable\" value=\"AUTO\" selected>".$i_Discipline_Auto_Assign_Session."</option>";
			}
			else {
				$assignLater = " selected";	
			}
			$SelectSession .= "<option class=\"row_avaliable\" value=\"LATER\" $assignLater>".$i_Discipline_Put_Into_Unassign_List."</option>";
			
			// 2015-02-10: commented - to solve the performance issue
//			$label = $ldiscipline->displayDetentionSessionDefaultString();

			$SelectSession .= "<optgroup label=\"$label\">";
			
			// 2015-02-10: commented - to solve the performance issue
//			for ($i=0; $i<sizeof($result); $i++) {
//				list($TmpDetentionID[$i],$TmpDetentionDate[$i],$TmpDetentionDayName[$i],$TmpStartTime[$i],$TmpEndTime[$i],$TmpPeriod[$i],$TmpLocation[$i],$TmpForm[$i],$TmpPIC[$i],$TmpVacancy[$i],$TmpAssigned[$i],$TmpAvailable[$i]) = $result[$i];
//
//				if ($TmpForm[$i] == $TmpAllForms) $TmpForm[$i] = $i_Discipline_All_Forms;
//				$SessionStr = $ldiscipline->displayDetentionSessionString($TmpDetentionID[$i]);
//				$SelectSession .= "<option class=\"row_avaliable\" value=\"".$TmpDetentionID[$i]."\">$SessionStr</option>";
//			}
			$SelectSession .= $row_avaliable_option;
			
			$SelectSession .= "</optgroup>";
			$SelectSession .= "</select>";
			$SelectSession .= "<div id=\"show_result_".$parIndex."_".$parSessionNumber."\" style=\"position:absolute; width:200px; height:20px; z-index:0;\"></div>";
			$SelectSession .= "</td><td>";
			$SelectSession .= "<span id=\"SpanSysMsg".$parIndex."_".$parSessionNumber."\"></span>";
			$SelectSession .= "</td></tr></table>";
			
			return $SelectSession;
		}
		
		// 2015-02-10: move out from getSelectSessionString() - to solve the performance issue [2015-0120-1549-57066]
		$detentionSessionAry = array();
		$detentionSessionCountAry = array();
		$detentionSessionStringAry = array();
		
		// get detention session - apply to All Student
		$detentionList = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "ALL_FORMS", "", "ASC");
		
		// loop Sessions
		$detentionSessionStr = "";
		$detentionSessionNum = count($detentionList);
		for ($d=0; $d<$detentionSessionNum; $d++) {
			$currentDetentionID = $detentionList[$d][0];
			$returnStr = $ldiscipline->displayDetentionSessionString($currentDetentionID);
			$detentionSessionStringAry[$currentDetentionID] = $returnStr;
			
			$detentionSessionStr .= "<option class=\"row_avaliable\" value=\"".$currentDetentionID."\">$returnStr</option>";
		}
		
		// Data for Apply All Detention Settings
		$detentionSessionAry[0] = $detentionSessionStr;
		$detentionSessionCountAry[0] = $detentionSessionNum;
		
		// Loop for individual student
		$TableIndStudent = "";
		for ($i=0; $i<sizeof($student); $i++) {
			
			// 2015-02-10: move out from getSelectSessionString() - to solve the performance issue [2015-0120-1549-57066]
			// get detention session - for each Student
			$detentionList = $ldiscipline->getDetentionList("FUTURE", "AVAILABLE", "THIS_FORM", $student[$i], "ASC");
			
			// loop Sessions
			$detentionSessionStr = "";
			$detentionSessionNum = count($detentionList);
			for ($d=0; $d<$detentionSessionNum; $d++)
			{
				$currentDetentionID = $detentionList[$d][0];
				
				// [2017-1003-1124-05235] apply temp array
				if(isset($detentionSessionStringAry[$currentDetentionID])) {
					$returnStr = $detentionSessionStringAry[$currentDetentionID];
				}
				else {
					$returnStr = $ldiscipline->displayDetentionSessionString($currentDetentionID);
					$detentionSessionStringAry[$currentDetentionID] = $returnStr;
				}
				
				$detentionSessionStr .= "<option class=\"row_avaliable\" value=\"".$currentDetentionID."\">$returnStr</option>";
			}
			
			// Data for Student Detention Settings
			$detentionSessionAry[$i+1] = $detentionSessionStr;
			$detentionSessionCountAry[$i+1] = $detentionSessionNum;
			
			$resultStudentName = $ldiscipline->getStudentNameByID($student[$i]);
			list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
			
			$TableIndStudent .= "<br />";
			$TableIndStudent .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">";
			$TableIndStudent .= "<tr>";
			$TableIndStudent .= "<td colspan=\"2\" valign=\"top\" nowrap=\"nowrap\">".($i+1).".";
			$TableIndStudent .= "<img src=\"".$image_path."/".$LAYOUT_SKIN."/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\">";
			$TableIndStudent .= "<span class=\"sectiontitle\">".$TmpClassName."-".$TmpClassNumber." ".$TmpName."</span>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr valign=\"top\" class=\"tablerow1\">";
			$TableIndStudent .= "<td nowrap=\"nowrap\" class=\"formfieldtitle\">";
			$TableIndStudent .= "<span class=\"tabletext\">".$i_Discipline_Detention_Times." <span class=\"tabletextrequire\">*</span></span>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "<td width=\"80%\">";
			$TableIndStudent .= "<input type=\"hidden\" id=\"HiddenDetentionCount".($i+1)."\" name=\"HiddenDetentionCount".($i+1)."\" value=\"1\" />";
			$TableIndStudent .= "<select name=\"DetentionCount".($i+1)."\" id=\"DetentionCount".($i+1)."\" onChange=\"javascript:updateSessionCell(".($i+1).");\">".$OptionForDetentionCount."</select>";
			$TableIndStudent .= "</td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr class=\"tablerow1\">";
			$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Session." <span class=\"tabletextrequire\">*</span></td>";
			// 2015-02-10: change input parameter to getSelectSessionString() - to solve the performance issue [2015-0120-1549-57066]
//			$TableIndStudent .= "<td valign=\"top\"><table name=\"TableSession".($i+1)."\" id=\"TableSession".($i+1)."\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td width=\"15\" nowrap>1.</td><td nowrap>".getSelectSessionString($i+1, 1, $TmpUserID)."</td></tr></table></td>";
			$TableIndStudent .= "<td valign=\"top\"><table name=\"TableSession".($i+1)."\" id=\"TableSession".($i+1)."\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\"><tr><td width=\"15\" nowrap>1.</td><td nowrap>".getSelectSessionString($i+1, 1, $detentionSessionNum, $detentionSessionStr)."</td></tr></table></td>";
			$TableIndStudent .= "</tr>";
			$TableIndStudent .= "<tr class=\"tablerow1\">";
			$TableIndStudent .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">".$i_Discipline_Remark."</td>";
			$TableIndStudent .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextRemark".($i+1), "")."</td>";
			$TableIndStudent .= "</tr>";
			if ($i != sizeof($student)-1) {
				$TableIndStudent .= "<tr>";
				$TableIndStudent .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td>";
				$TableIndStudent .= "</tr>";
			}
			$TableIndStudent .= "</table>";
		}

		// Preset detention session (for javascript)
		$jSessionString = "var SelectSessionArr=new Array(".(sizeof($student)+1).");\n";
		for ($k=0; $k<=sizeof($student); $k++) {
			$jSessionString .= "SelectSessionArr[".$k."]=new Array(10);\n";
		}

		for ($i=0; $i<=sizeof($student); $i++) {
			for ($j=0; $j < $session_times; $j++) {
				// 2015-02-10: change input parameter to getSelectSessionString() - to solve the performance issue [2015-0120-1549-57066]
//				$jSessionString .= "SelectSessionArr[".$i."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($i, $j+1, ($i==0)?"":$student[$i-1])."';\n";
				$jSessionString .= "SelectSessionArr[".$i."][".$j."]='".str_replace("'", "&#039;", getSelectSessionString($i, $j+1, $detentionSessionCountAry[$i], $detentionSessionAry[$i]))."';\n";
			}
		}

		// Initial form after submit fails (for javascript)
		if ($FailFlag) {
			$SessionNAMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_not_available")));
			$SessionPastMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_past")));
			$SessionFullMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_full")));
			$SessionAssignedBeforeMsg = str_replace("\n", "", str_replace("'", "\"", $linterface->GET_SYS_MSG("session_assigned_before")));

			$jInitialForm = "";
			for ($l=0; $l<=sizeof($submitStudent); $l++) {
				$jInitialForm .= "if(document.getElementById('DetentionCount".$l."')) document.getElementById('DetentionCount".$l."').value='".${"submitDetentionCount".$l}."';\n";
				$jInitialForm .= "if(document.getElementById('HiddenDetentionCount".$l."')) document.getElementById('HiddenDetentionCount".$l."').value='1';\n";
				$jInitialForm .= "updateSessionCell(".$l.");\n";
				for ($m=1; $m<=${"submitDetentionCount".$l}; $m++) {
					$jInitialForm .= "if(document.getElementById('SelectSession".$l."_".$m."')) document.getElementById('SelectSession".$l."_".$m."').value='".${"submitSelectSession".$l."_".$m}."';\n";
					// 2015-04-10: display error message in show_result_$l_$m instead of SpanSysMsg_$l_$m
					if ($AvailableFlag[$l][$m] == "NO") {
//						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionNAMsg."';\n";
						$jInitialForm .= "if(document.getElementById('show_result_".$l."_".$m."')) document.getElementById('show_result_".$l."_".$m."').innerHTML='".substr($i_con_msg_session_not_available,0,-1) ."';\n";
					} else if ($FullFlag[$l][$m] == "YES") {
//						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionFullMsg."';\n";
						$jInitialForm .= "if(document.getElementById('show_result_".$l."_".$m."')) document.getElementById('show_result_".$l."_".$m."').innerHTML='".substr($i_con_msg_session_full,0,-1) ."';\n";
					} else if ($PastFlag[$l][$m] == "YES") {
//						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionPastMsg."';\n";
						$jInitialForm .= "if(document.getElementById('show_result_".$l."_".$m."')) document.getElementById('show_result_".$l."_".$m."').innerHTML='".substr($i_con_msg_session_past,0,-1) ."';\n";
					} else if ($StudentInSessionFlag[$l][$m] == "YES") {
//						$jInitialForm .= "if(document.getElementById('SpanSysMsg".$l."_".$m."')) document.getElementById('SpanSysMsg".$l."_".$m."').innerHTML='".$SessionAssignedBeforeMsg."';\n";
						$jInitialForm .= "if(document.getElementById('show_result_".$l."_".$m."')) document.getElementById('show_result_".$l."_".$m."').innerHTML='".substr($i_con_msg_session_assigned_before,0,-1) ."';\n";
					}
				}
				$jInitialForm .= "if(document.getElementById('TextRemark".$l."')) document.getElementById('TextRemark".$l."').value='".${"submitTextRemark".$l}."';\n";
			}
		}

########## For Detention (End) ##########

########## For eNotice (Begin) ##########
# check template is ava
$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1);

if (!$lnotice->disabled && !empty($NoticeTemplateAva) && !$sys_custom['eDiscipline']['AP_Step3_HideNoticeSetting'])
{
		$checkSelected = ($sys_custom['eDisciplinev12']['CheungShaWanCatholic']['AP_eNoticeChecked']) ? " checked " : "";
		// [2014-1216-1347-28164] display "Send Notification" if system allow send push message for detention records 
		$canSendDetentionMsg = $canSendPushMsg? $record_type==-1 && $detentionID=="" : false;
		//$enoticeCheckboxName = $canSendDetentionMsg? $Lang['eDiscipline']['DetentionMgmt']['SendNotification'] : $eDiscipline["SendNoticeWhenReleased"];
		$enoticeCheckboxName = $eDiscipline["SendNoticeWhenReleased"];
		$enotice_checkbox = '<input name="action_notice" type="checkbox" id="action_notice" value="1" '.$checkSelected.'onClick="javascript:if(this.checked){document.getElementById(\'noticeFlag\').value=\'YES\';showDiv(\'divNotice\');}else{document.getElementById(\'noticeFlag\').value=\'NO\';hideDiv(\'divNotice\');}" /><label for="action_notice">'.$enoticeCheckboxName.'</label><br>';
		
		$catTmpArr = $ldiscipline->TemplateCategory();
		
		if(is_array($catTmpArr))
		{
			$catArr[0] = array("0","-- $button_select --");
			foreach($catTmpArr as $Key=>$Value)
			{
				# check the Template Catgory has template or not
				$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
				if(!empty($NoticeTemplateAvaTemp))
					$catArr[] = array($Key,$Value);
			}
		}
		
		for ($i=0; $i<=sizeof($student); $i++) {
	
			$thisCatID = $CatID;
			$apCatInfo = $ldiscipline->getAPCategoryInfoBYCatID($thisCatID);
			$TemplateID = $apCatInfo['TemplateID'];	
			$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);
			$TemplateCategoryID = $TemplateAry[0]['CategoryID'];

			if($TemplateCategoryID!="0" && $TemplateCategoryID!="") {
				if($i<=sizeof($student) && $i!=0) {
					$k = $i;
					$js .= "changeCat('{$TemplateCategoryID}', $k);itemSelected('$TemplateID', $k, '$TemplateID');\n";
				}
			}
						
			${"catSelection".$i} = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID'.$i.'" name="CategoryID'.$i.'" onChange="changeCat(this.value, '.$i.')"', "", $TemplateCategoryID);
			if ($i > 0) {
				$resultStudentName = $ldiscipline->getStudentNameByID($student[$i-1]);
				list($TmpUserID, $TmpName, $TmpClassName, $TmpClassNumber) = $resultStudentName[0];
				// [2014-1216-1347-28164]
				$parentCanReceiveMsg = $canSendDetentionMsg? $luser->getParentUsingParentApp($student[$i-1], true) : false;
				$TableNotice .= "<table align=\"center\" width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td colspan=\"2\" valign=\"top\" nowrap>\n";
				$TableNotice .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n";
				$TableNotice .= "<tr>\n";
				$TableNotice .= "<td>$i. <img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_stu_ind.gif\" width=\"20\" height=\"20\" align=\"absmiddle\"> <span class=\"sectiontitle\">$TmpClassName-$TmpClassNumber $TmpName</span></td>\n";
				$TableNotice .= "<td align=\"right\"><span class=\"sectiontitle\">\n";
				$TableNotice .= $linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice($i);");
				$TableNotice .= "</span></td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "</table>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr valign=\"top\" class=\"tablerow1\">\n";
				$TableNotice .= "<td width=\"20%\" nowrap class=\"formfieldtitle\"><span class=\"tabletext\">$i_Discipline_Usage_Template <span class=\"tabletextrequire\">*</span></span></td>\n";
				$TableNotice .= "<td>\n";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentID$i\" name=\"StudentID$i\" value=\"".$TmpUserID."\" />";
				$TableNotice .= "<input type=\"hidden\" id=\"StudentName$i\" name=\"StudentName$i\" value=\"".$TmpName."\" />";
				$TableNotice .= ${"catSelection".$i}." <select name=\"SelectNotice$i\" id=\"SelectNotice$i\"><option value=\"0\">-- $button_select --</option></select>\n";
				$TableNotice .= "</td>\n";
				$TableNotice .= "</tr>\n";
				$TableNotice .= "<tr class=\"tablerow1\">\n";
				$TableNotice .= "<td valign=\"top\" nowrap class=\"formfieldtitle\">$i_Discipline_Additional_Info</td>\n";
				$TableNotice .= "<td valign=\"top\">".$linterface->GET_TEXTAREA("TextAdditionalInfo".($i), "")."</td>";
				$TableNotice .= "</tr>\n";
				
				// [2014-1216-1347-28164] display checkbox to send push message if enable eClass App and student's parents using eClass App
				if($parentCanReceiveMsg){
					$TableNotice .= "<tr class=\"tablerow1\">\n";
					$TableNotice .= "<td valign=\"top\">&nbsp;</td>\n";
					$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"eClassAppNotify$i\" id=\"eClassAppNotify$i\" value='1' checked><label for=\"eClassAppNotify$i\"'>".$Lang['eDiscipline']['Detention']['SendNotification']."</label></td>\n";
					$TableNotice .= "</tr>\n";
				}
				
				$TableNotice .= "<tr class=\"tablerow1\">\n";
				$TableNotice .= "<td valign=\"top\">&nbsp;</td>\n";
				// [2014-1216-1347-28164] checkbox not checked if enable eClass App and student's parents using eClass App
				$TableNotice .= "<td valign=\"top\"><input type='checkbox' name=\"emailNotify$i\" id=\"emailNotify$i\" value='1' ".($parentCanReceiveMsg || $sys_custom['eDiscipline']['DefaultUncheckSendEmailCheckbox']? "" : "checked")."><label for=\"emailNotify$i\"'>".$Lang['eDiscipline']['EmailNotifyParent'].($canSendDetentionMsg? $lang['eDiscipline']['DetentionMgmt']['SendNoticeWhenRelease'] : "")."</label></td>\n";
				$TableNotice .= "</tr>\n";
				if ($i != sizeof($student)) {
					$TableNotice .= "<tr>\n";
					$TableNotice .= "<td height=\"1\" colspan=\"2\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td>\n";
					$TableNotice .= "</tr>\n";
				}
				$TableNotice .= "</table>\n";
			}
		}
		//echo $TemplateCategoryID.'/';
		if(sizeof($student)>1 && $TemplateCategoryID!="0" && $TemplateCategoryID!="")
			$js .= "changeCat('{$TemplateCategoryID}', 0);itemSelected('$TemplateID', 0, '$TemplateID');\n";


		// Preset template items (for javascript)
		for ($i=0; $i<sizeof($catArr); $i++) {
			$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
			for ($j=0; $j<=sizeof($result); $j++) {
				if ($j==0) {
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
				} else {
					$tempTemplate = $result[$j-1][1];
					$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
			        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
			        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
			        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
        
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
					$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
				}
			}
		}
	}
	if ($lnotice->disabled && !$sys_custom['eDiscipline']['AP_Step3_HideNoticeSetting'])
	{
		$enotice_checkbox = '<input name="action_notice" type="checkbox" id="action_notice" disabled /><label for="action_notice">'.$eDiscipline["SendNoticeWhenReleased"].'</label><br><br>('.$eDiscipline["EnoticeDisabledMsg"].')<br>';
	}

########## For eNotice (End) ##########


if (!($submit_flag == "YES" && $FailFlag == false))
{

# (START) check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)
$isExist = array();
$hiddenField = "";
$success = 1;

if($ldiscipline->NotAllowSameItemInSameDay) {	# Not allow same student with same item in a same day
	foreach($student as $StudentID) {
		$thisItemID = $ItemID;
		$thisRecordDate = $RecordDate;
		$isExist[$StudentID] = $ldiscipline->checkSameAPItemInSameDay($StudentID, $thisItemID, $thisRecordDate);
		if($isExist[$StudentID]==1) $success = 0;
	}
}


if($success==0) {		# existing record of same item on same day (same student)
	$hiddenField = "";

	echo "<body onload='document.form5.submit()'>";
	echo "<form name='form5' method='post' action='new2.php'>";
	foreach($student as $StudentID) { 
		echo '<input type="hidden" name="student[]" id="student[]" value="'.$StudentID.'" />';
	} 	
	foreach($isExist as $key=>$val) {
		if($val==1)
			$hiddenField .= "<input type='hidden' name='sid[]' id='sid[]' value='$key'>\n";	
	}
	?>
	<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
	<input type="hidden" name="RecordInputDate" id="RecordInputDate" value="<?=$RecordInputDate?>" />
	<input type="hidden" name="ActionDueDate" id="ActionDueDate" value="<?=$ActionDueDate?>" />
		<? if(isset($picType) && $picType==2) { ?>
			<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$picName?>">
		<? } else { ?>
			<? foreach($PIC as $k=>$d) { ?>
				<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
			<? } ?>
		<? } ?>
	<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
    <? if($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
        <input type="hidden" name="followUpRemark" id="followUpRemark" value="<?=$followUpRemark?>" />
    <? } ?>
	<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
	<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
	<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
	<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
	<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
	<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
	<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
	<input type="hidden" name="ActivityScore" id="ActivityScore" value="<?=$ActivityScore?>" />
	<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
	<input type="hidden" name="picType" id="picType" value="<?=$picType?>" />
	<input type="hidden" name="picName" id="picName" value="<?=$picName?>" />
	<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />
	<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
	<input type="hidden" name="detentionID" id="detentionID" value="<?=$detentionID?>" />

	<? if($sys_custom['eDiscipline']['CSCProbation']) {?>
		<input type="hidden" name="applyProbation" id="applyProbation" value="<?=$applyProbation?>" />
	<? } ?>
	
	<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
		<input type="hidden" name="maxStudyScore" id="maxStudyScore" value="<?=$maxStudyScore?>" />
	<? } ?>
	
	<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) 
	{ 
		$file_number = intranet_htmlspecialchars($file_number);
		$record_number = intranet_htmlspecialchars($record_number);
	?>
	<input type="hidden" name="file_number" id="file_number" value="<?=$file_number?>" />
	<input type="hidden" name="record_number" id="record_number" value="<?=$record_number?>" />
	<? } ?>

	<!-- Step 3 data //-->
	<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
	<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
	<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
	<input type="hidden" name="layername" id="layername" value="" />
	<input type="hidden" name="formValid" id="formValid" value="" />
	<?=$hiddenField?>
	
	<script language="javascript">
	<!--
		document.form5.submit();
	-->
	</script>
	</form>
	</body>
<?
exit;
}
# (END) check whether same item already added to student on same day (if $ldiscipline->NotAllowSameItemInSameDay = 1)


# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
<?=$jSessionString?>
<?=$jTemplateString?>

function viewCurrentRecord(id)
{
         newWindow('viewcurrent.php?StudentID='+id, 8);
}

function showDiv(div)
{
	document.getElementById(div).style.display="inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display="none";
}

function updateSessionCell(studentIndex){
	if(studentIndex==undefined) studentIndex='0';

	if(document.getElementById('HiddenDetentionCount'+studentIndex))
	{
		var oldCount=parseInt(document.getElementById('HiddenDetentionCount'+studentIndex).value);
		var newCount=parseInt(document.getElementById('DetentionCount'+studentIndex).value);
	}
	
	if(oldCount<newCount){
		var newRow, newCell;
		for(var i=oldCount;i<newCount;i++){
			newRow=document.getElementById('TableSession'+studentIndex).insertRow(document.getElementById('TableSession'+studentIndex).rows.length);
			newCell0=newRow.insertCell(0);
			newCell1=newRow.insertCell(1);
			newCell0.innerHTML=(i+1)+'.';
			newCell1.setAttribute("noWrap", true);
			newCell1.innerHTML=SelectSessionArr[studentIndex][i];
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	} else if(oldCount>newCount){
		for(var i=oldCount;i>newCount;i--){
			document.getElementById('TableSession'+studentIndex).deleteRow(i-1);
		}
		document.getElementById('HiddenDetentionCount'+studentIndex).value = newCount;
	}
}

function applyToAll(studentCount){
	for(var i=1;i<=studentCount;i++){
		document.getElementById('DetentionCount'+i).value = document.getElementById('DetentionCount0').value;
		updateSessionCell(i);
		for(var j=1;j<=document.getElementById('DetentionCount0').value;j++){
			document.getElementById('SelectSession'+i+'_'+j).value = document.getElementById('SelectSession0_'+j).value;
			if (document.getElementById('SelectSession'+i+'_'+j).value != document.getElementById('SelectSession0_'+j).value){
				document.getElementById('SelectSession'+i+'_'+j).value = 'AUTO';
			}
		}
		document.getElementById('TextRemark'+i).value = document.getElementById('TextRemark0').value;
	}
}

function changeCat(cat, selectIdx){
	var x = document.getElementById("SelectNotice"+selectIdx);

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}

	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
		
		// add empty option to prevent js error when submit form
		if((listLength-1)>=0){
			var y = document.createElement('option');
			y.text = "-- <?=$button_select?> --";
			tempText = "-- <?=$button_select?> --";
			y.value = "0";
			tempValue = "0";
			try {
				x.add(y,null); // standards compliant
				//x.options[x.options.length] = new Option(tempText, tempValue);
			} catch(ex) {
				x.add(y); // IE only
				//x.options[x.options.length] = new Option(tempText, tempValue);
			}
		}
	}
	/*
	var select = document.forms.formName.elements.selectName;
	x.options[x.options.length] = new Option('text', 'value');
	*/
	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		tempText = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];
		tempValue = eval("jArrayTemplate"+cat+"Value")[j];
		try {
			x.add(y,null); // standards compliant
			//x.options[x.options.length] = new Option(tempText, tempValue);
		} catch(ex) {
			x.add(y); // IE only
			//x.options[x.options.length] = new Option(tempText, tempValue);
		}
	}
}

function itemSelected(itemID, selectIdx, templateID) {
	var obj = document.form1;
	//if(templateID!="") {
		var item_length = eval("obj.SelectNotice"+selectIdx+".length");
		for(var i=0; i<item_length; i++) {
			if(obj.elements['SelectNotice'+selectIdx].options[i].value==templateID) {
				obj.elements['SelectNotice'+selectIdx].selectedIndex = i;
				//alert(i);
				break;	
			}
		}
	//}
}

function previewNotice(studentIdx){
	var templateID = document.getElementById("SelectNotice"+studentIdx).value;
	var studentName = '';
	if(studentIdx!=0) {
		studentName = document.getElementById("StudentName"+studentIdx).value;
	}
	var additionInfo = document.getElementById("TextAdditionalInfo"+studentIdx).value;
	newWindow("preview_notice.php?tid="+templateID+"&pv_studentname="+studentName+"&pv_additionalinfo="+additionInfo);
}

function applyNoticeToAll(studentCount){
	for(var i=1;i<=studentCount;i++){
		changeCat(document.getElementById('CategoryID0').value, i);
		document.getElementById('CategoryID'+i).value = document.getElementById('CategoryID0').value;
		document.getElementById('SelectNotice'+i).value = document.getElementById('SelectNotice0').value;
		document.getElementById('TextAdditionalInfo'+i).value = document.getElementById('TextAdditionalInfo0').value;
		
		<?php if($canSendDetentionMsg){ ?>
			if(document.getElementById('eClassAppNotify'+i)){
				document.getElementById('eClassAppNotify'+i).checked = document.getElementById('eClassAppNotify0').checked;
			}
		<?php } ?>
		
		document.getElementById('emailNotify'+i).checked = document.getElementById('emailNotify0').checked;
	}
}

function checkForm()
{

	/*
	if(document.getElementById('formValid').value == 0 || document.getElementById('formValid').value == "") {
		return false;	
	}
	*/
	
	<? if (!$lnotice->disabled && !empty($NoticeTemplateAva) && !$sys_custom['eDiscipline']['AP_Step3_HideNoticeSetting']) {?>	
	if (document.getElementById('action_notice').checked) {
		for(var i=1;i<=<?=sizeof($student)?>;i++){
			if (document.getElementById('SelectNotice'+i).value=='0') {
				document.getElementById('submit_BTN').disabled  = false; 
				document.getElementById('submit_BTN').className  = "formbutton_v30 print_hide"; 
				alert('<?=$i_alert_pleaseselect.$i_Discipline_Template?>');
				return false;
			}
		}
	}
	<? } ?>
	<? if(false && $record_type==-1 && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $detentionID=="") { ?>
	if(document.getElementById('action_detention').checked==false || document.getElementById('formValid').value==1) {
		document.getElementById('submit_flag').value='YES';
		document.form1.submit();
	}
	<? } else { ?>
		document.getElementById('submit_flag').value='YES';
		document.form1.submit();
	<? } ?>
	
/*
	document.getElementById('submit_flag').value='YES';
	return true;
*/
}

function showMsg(msg) {
	var temp;
	var warningMsg = "";
	var layername = "";
	document.getElementById('formValid').value = 1;
	
	<? if(false && $record_type==-1 && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $detentionID=="") { ?>
	if(document.getElementById('action_detention').checked==true) {
		var msgSplit = msg.split(")\n");
		for(i=0;i<msgSplit.length-1;i++) {
			var layer = msgSplit[i].substring(1);
			temp = layer.split(",");
			
			if(temp[1] != 0) {
				document.getElementById('formValid').value = 0;
			}
			
			layername = document.getElementById(temp[0]);
			//layername = eval(temp[0]);
			//alert(temp[0]);
			
			
			switch(temp[1]) {
				case "0" : warningMsg = ""; break;
				case "1" : warningMsg = "<?=substr($i_con_msg_session_not_available,0,-1) ?>"; break;
				case "2" : warningMsg = "<?=substr($i_con_msg_session_full,0,-1) ?>"; break;
				case "3" : warningMsg = "<?=substr($i_con_msg_session_past,0,-1) ?>"; break;
				case "4" : warningMsg = "<?=substr($i_con_msg_session_assigned_before,0,-1) ?>"; break;
				default : warningMsg = ""; break;
			}
			
			layername.innerHTML = warningMsg;
		}
	}
	<? } ?>
	if(document.getElementById('formValid').value==1) {
		checkForm();
	} else {
		document.getElementById('submit_BTN').disabled  = false; 
		document.getElementById('submit_BTN').className  = "formbutton_v30 print_hide"; 
	}
	
}

function defaultClickCheckbox(obj){
	<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $record_type==-1 && $detentionID=="" && $DefaultDetention){ ?>
		if(obj.action_detention){
			document.getElementById('action_detention').checked = true;
			document.getElementById('detentionFlag').value = 'YES';
			showDiv('divDetention');
		}
	<? } ?>
	<? if(!$lnotice->disabled && !empty($NoticeTemplateAva) && !$sys_custom['eDiscipline']['AP_Step3_HideNoticeSetting'] && $DefaultSendNotice) {?>
		if(obj.action_notice){
			document.getElementById('action_notice').checked = true;
			document.getElementById('noticeFlag').value = 'YES';
			showDiv('divNotice');
		}
	<? } ?>
}

//-->
</script>
<script type="text/javascript" language="javascript">
var http_request = false;

function makeRequest(url, parameters) {

	http_request = GetXmlHttpObject()
	
	if (!http_request) {
		alert('Cannot create XMLHTTP instance');
		return false;
	}
	http_request.onreadystatechange = alertContents;
	http_request.open('GET', url + parameters, true);
	http_request.send(null);
}

function alertContents() {
	layername = document.getElementById('layername').value;
	if (http_request.readyState==4 || http_request.readyState=="complete") {
		//document.getElementById(layername).innerHTML = http_request.responseText;  
		//document.getElementById(layername).style.border = "0px solid #A5ACB2";          
		showMsg(http_request.responseText);
	}
}

function get(obj, layername) {
	if(layername != undefined)
		document.getElementById('layername').value = layername;
	
	var getstr = "?";

	for (i=0; i<obj.elements.length; i++) {
		if (obj.elements[i].tagName == "SELECT") {
			var sel = obj.elements[i];
			getstr += sel.name + "=" + sel.options[sel.selectedIndex].value + "&";
		}
		if (obj.elements[i].tagName == "INPUT") {
			var inTemp = obj.elements[i];
			getstr += inTemp.name + "=" + inTemp.value + "&";
		}
		if (obj.elements[i].tagName == "TEXTAREA") {
			var inTemp = obj.elements[i];
			getstr += inTemp.name + "=" + inTemp.value + "&";
		}
	}
	makeRequest('validate.php', getstr);
}

function GetXmlHttpObject()
{
	var http_request = null;
	try {
		// Firefox, Opera 8.0+, Safari
		http_request = new XMLHttpRequest();
	} catch (e) {
		// Internet Explorer
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			http_request = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return http_request;
}
</script>
<form name="form1" method="POST" action="new3.php">
<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="navigation">
		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td></tr>
		</table>
	</td>
</tr>
<tr>
	<td align="center"><?=$linterface->GET_STEPS($STEPS_OBJ)?></td>
</tr>

<tr>
	<td>
		<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td valign="top" nowrap="nowrap" class="tablerow2"><?=$i_general_students_selected?></td>
			<td valign="top" class="tablerow2"><?=$selected_student_table?></td>
		</tr>
		<tr>
			<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$eDiscipline["Action"]?> -</i></td>
		</tr>
		<tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"></td>
			<td>
<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-New") && $record_type==-1 && $detentionID=="") {?>
				<input name="action_detention" type="checkbox" id="action_detention" value="1" onClick="javascript:if(this.checked){document.getElementById('detentionFlag').value='YES';showDiv('divDetention');}else{document.getElementById('detentionFlag').value='NO';hideDiv('divDetention');}" /><label for="action_detention"><?=$eDiscipline['Detention']?></label><br>
<div id="divDetention" style="display:none">
	<fieldset class="form_sub_option" style="float:none">
		<? if(sizeof($student)>1) {?>
		<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
			<tr>
				<td colspan="2" valign="top" nowrap="nowrap">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_section.gif" width="20" height="20" align="absmiddle">
								<span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span>
							</td>
							<td align="right">
								<span class="sectiontitle">
<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyToAll(".sizeof($student).");")?>
								</span>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr valign="top">
				<td nowrap="nowrap" class="formfieldtitle">
					<span class="tabletext"><?=$i_Discipline_Detention_Times?> <span class="tabletextrequire">*</span></span>
				</td>
				<td width="80%">
					<input type="hidden" id="HiddenDetentionCount0" name="HiddenDetentionCount0" value="1" />
					<select name="DetentionCount0" id="DetentionCount0" onChange="javascript:updateSessionCell();"><?=$OptionForDetentionCount?></select>
				</td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Session?> <span class="tabletextrequire">*</span></td>
				<td valign="top"><table name="TableSession0" id="TableSession0" width="100%" border="0" cellpadding="5" cellspacing="0"><tr><td width="15" nowrap>1.</td><td nowrap><?=getSelectSessionString(0, 1, $detentionSessionCountAry[0], $detentionSessionAry[0])?><!--<?=getSelectSessionString(0, 1, "")?>--></td></tr></table></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_UserRemark?></td>
				<td valign="top"><?=$linterface->GET_TEXTAREA("TextRemark0", "")?></td>
			</tr>
		</table>
		<? } ?>
<?=$TableIndStudent?>
	</fieldset>
</div>
<? } ?>


<?=$enotice_checkbox?>

<div id="divNotice" style="display:none">
	<fieldset class="form_sub_option">
	<? if(sizeof($student)>1) {?>
				<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0" class="tablerow2">
					<tr>
						<td colspan="2" valign="top" nowrap="nowrap">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle"><span class="sectiontitle"><?=$eDiscipline["GlobalSettingsToAllStudent"]?></span></td>
									<td align="right"><span class="sectiontitle">
<?=$linterface->GET_BTN($i_Discipline_Apply_To_All, "button", "javascript:applyNoticeToAll(".sizeof($student).");")?>&nbsp;
<?=$linterface->GET_BTN($button_view_template, "button", "javascript:previewNotice(0);")?>
									</span></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr valign="top">
						<td width="20%" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_Template?> <span class="tabletextrequire">*</span></span></td>
						<td>
<?=$catSelection0?> <select name="SelectNotice0" id="SelectNotice0"><option value="0"><?="-- $button_select --"?></option></select>
						</td>
					</tr>
					<tr>
						<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Additional_Info?></td>
						<td valign="top"><textarea name="TextAdditionalInfo0" id="TextAdditionalInfo0" rows="2" wrap="virtual" class="textboxtext"></textarea></td>
					</tr>
					
					<?php if($canSendDetentionMsg){ ?>
						<tr>
							<td valign="top" nowrap="nowrap">&nbsp;</td>
							<td valign="top"><input type='checkbox' name='eClassAppNotify0' id='eClassAppNotify0' value='1' checked><label for='eClassAppNotify0'><?=$Lang['eDiscipline']['Detention']['SendNotification']?></label></td>
						</tr>
					<?php } ?>
					
					<tr>
						<td valign="top" nowrap="nowrap">&nbsp;</td>
						<td valign="top"><input type='checkbox' name='emailNotify0' id='emailNotify0' value='1' checked><label for='emailNotify0'><?=$Lang['eDiscipline']['EmailNotifyParent'].($canSendDetentionMsg? $Lang['eDiscipline']['DetentionMgmt']['SendNoticeWhenRelease'] : "")?></label></td>
					</tr>
				</table>
				<br />
				<? } ?>
<?=$TableNotice?>
	</fieldset>
</div>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_continue, "button", "this.disabled=true; this.className='formbutton_disable_v30 print_hide'; get(this.form, 'show_result_".$parIndex."_".$parSessionNumber."');", "submit_BTN")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "this.form.action='new2.php';this.form.submit();")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'")?>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>

<br />

<!-- Step 1 data //-->
<? foreach($student as $k=>$d) { ?>
	<input type="hidden" name="student[]" id="student[]" value="<?=$d?>" />
<? } ?>
<input type="hidden" name="TargetTagType" value="<?=$TargetTagType?>" />

<!-- Step 2 data //-->
<input type="hidden" name="RecordDate" id="RecordDate" value="<?=$RecordDate?>" />
<input type="hidden" name="RecordInputDate" id="RecordInputDate" value="<?=$RecordInputDate?>" />
<input type="hidden" name="ActionDueDate" id="ActionDueDate" value="<?=$ActionDueDate?>" />
	<? if(isset($picType) && $picType==2) { ?>
		<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$picName?>">
	<? } else { ?>
		<? foreach($PIC as $k=>$d) { ?>
			<input type="hidden" name="PIC[]" id="PIC[]" value="<?=$d?>" />
		<? } ?>
	<? } ?>
<input type="hidden" name="remark" id="remark" value="<?=$remark?>" />
    <? if($sys_custom['eDiscipline']['APFollowUpRemarks']) {?>
        <input type="hidden" name="followUpRemark" id="followUpRemark" value="<?=$followUpRemark?>" />
    <? } ?>
<input type="hidden" name="record_type" id="record_type" value="<?=$record_type?>" />
<input type="hidden" name="CatID" id="CatID" value="<?=$CatID?>" />
<input type="hidden" name="ItemID" id="ItemID" value="<?=$ItemID?>" />
<input type="hidden" name="MeritNum" id="MeritNum" value="<?=$MeritNum?>" />
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>" />
<input type="hidden" name="ConductScore" id="ConductScore" value="<?=$ConductScore?>" />
<input type="hidden" name="StudyScore" id="StudyScore" value="<?=$StudyScore?>" />
<input type="hidden" name="ActivityScore" id="ActivityScore" value="<?=$ActivityScore?>" />
<input type="hidden" name="semester" id="semester" value="<?=$semester?>" />
<input type="hidden" name="picType" id="picType" value="<?=$picType?>" />
<input type="hidden" name="picName" id="picName" value="<?=$picName?>" />
<input type="hidden" name="SubjectID" id="SubjectID" value="<?=$SubjectID?>" />
<input type="hidden" name="sessionTime" id="sessionTime" value="<?=$sessionTime?>" />
<input type="hidden" name="detentionID" id="detentionID" value="<?=$detentionID?>" />

<? if($sys_custom['eDiscipline']['CSCProbation']) {?>
	<input type="hidden" name="applyProbation" id="applyProbation" value="<?=$applyProbation?>" />
<? } ?>
	
<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
	<input type="hidden" name="maxStudyScore" id="maxStudyScore" value="<?=$maxStudyScore?>" />
<? } ?>

<? if($sys_custom['eDiscipline']['MST']['AP_Extra_Fields']) 
{ 
	$file_number = intranet_htmlspecialchars($file_number);
	$record_number = intranet_htmlspecialchars($record_number);
?>
<input type="hidden" name="file_number" id="file_number" value="<?=$file_number?>" />
<input type="hidden" name="record_number" id="record_number" value="<?=$record_number?>" />
<? } ?>

<!-- Step 3 data //-->
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="detentionFlag" id="detentionFlag" value="<?=$detentionFlag?>" />
<input type="hidden" name="noticeFlag" id="noticeFlag" value="<?=$noticeFlag?>" />
<input type="hidden" name="layername" id="layername" value="" />
<input type="hidden" name="formValid" id="formValid" value="" />

</form>
<?
if ($FailFlag) {
?>
<script language="javascript">
<?=$jInitialForm?>
	<? if ($detentionFlag == "YES") { ?>
document.getElementById('action_detention').checked=true;
showDiv('divDetention');
	<? } ?>
	<? if ($noticeFlag == "YES" && !$sys_custom['eDiscipline']['AP_Step3_HideNoticeSetting']) { ?>
document.getElementById('action_notice').checked=true;
showDiv('divNotice');
		<? for ($j=0; $j<=sizeof($student); $j++) { ?>
changeCat("<?=${"CategoryID".$j}?>", <?=$j?>);
if(document.getElementById('CategoryID<?=$j?>')) document.getElementById('CategoryID<?=$j?>').value = "<?=${"CategoryID".$j}?>";
if(document.getElementById('SelectNotice<?=$j?>')) document.getElementById('SelectNotice<?=$j?>').value = "<?=${"SelectNotice".$j}?>";
if(document.getElementById('TextAdditionalInfo<?=$j?>')) document.getElementById('TextAdditionalInfo<?=$j?>').value = "<?=${"TextAdditionalInfo".$j}?>";
		<? } ?>
	<? } ?>
</script>
<? } else { ?>
<script language="javascript">
	defaultClickCheckbox(document.form1);
</script>
<? }
echo "<script language='javascript'>$js</script>";
if($checkSelected!="")
	echo "<script language='javascript'>showDiv('divNotice'); document.getElementById('noticeFlag').value='YES';</script>";

$linterface->LAYOUT_STOP();

}		

intranet_closedb();
?>