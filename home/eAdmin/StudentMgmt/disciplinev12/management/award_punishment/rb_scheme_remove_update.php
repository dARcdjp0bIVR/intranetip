<?php
# using: 

############ Change Log Start #############################
#
#	Date	:	2018-03-02 (Bill)	[2017-0317-1025-03225]
#				Creete file
#
############################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
if(!$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['TKP_RainbowScheme'])
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// Please don't change to "\n" or "<br/>" - otherwise cannot remove "參加彩虹計劃" in Remarks
$nextLine = '
';

$MeritRecordID = $_POST['RecordID'];
if($MeritRecordID)
{
    $recordData = $ldiscipline->RETRIEVE_MERIT_RECORD_INFO_BY_ID($MeritRecordID);
    $recordData = $recordData[0];
    $recordMeritType = $recordData['ProfileMeritType'];
    $recordIsJoined  = $recordData['RehabilStatus'];
    $recordRemarks   = $recordData['Remark'];
    
    if($recordIsJoined == "1" && ($recordMeritType == "-2" || $recordMeritType == "-3"))
    {
        $recordRemarks = str_replace($nextLine.$Lang['eDiscipline']['TKP']['JoinRainbowSchemeCH'], '', $recordRemarks);
        $recordRemarks = str_replace($Lang['eDiscipline']['TKP']['JoinRainbowSchemeCH'], '', $recordRemarks);
        
        $dataAry = array();
        $dataAry['ProfileMeritType'] = intval($recordMeritType) - 1;
        $dataAry['Remark'] = addslashes($recordRemarks);
        $dataAry['RehabilLastModify'] = $UserID;
        $ldiscipline->UPDATE_MERIT_RECORD($dataAry, $MeritRecordID);
        
        $sql = "UPDATE DISCIPLINE_MERIT_RECORD SET RehabilStatus = NULL, RehabilUpdateDate = NOW(), DateModified = NOW() WHERE RecordID = '$MeritRecordID'";
        $ldiscipline->db_db_query($sql);
    }
}

$returnPath = $back_page."?id=$MeritRecordID";
$return_filter = "SchoolYear=$SchoolYear&semester=$semester&targetClass=$targetClass&num_per_page=$num_per_page&pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&fromPPC=$fromPPC&pic=$pic&passedActionDueDate=$passedActionDueDate";
$msg = "update";
header("Location: ".$returnPath."&".$return_filter."&msg=".$msg);

intranet_closedb();
?>