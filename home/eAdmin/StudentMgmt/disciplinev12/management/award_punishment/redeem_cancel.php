<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-Waive");

# Back Parameters
$back_para = "pageNo=$pageNo&order=$order&field=$field&page_size_change=$page_size_change&numPerPage=$numPerPage&MeritType=$MeritType&s=$s&clickID=$clickID&pic=$pic&fromPPC=$fromPPC&passedActionDueDate=$passActionDueDate";

# initialization
$RedeemID = $_GET['RedeemID'];
$RecordID = $_GET['RecordID'];

$success = $ldiscipline->unredeemMeritRecords($RedeemID);

if ($success)
{
	header("Location: index.php?msg=cancel_redeem_success");
}
else
{
	header("Location: index.php?msg=cancel_redeem_failed");
}

intranet_closedb();
?>