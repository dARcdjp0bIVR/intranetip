<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup4.html");
$ldiscipline = new libdisciplinev12();

$linterface->LAYOUT_START();

$conductGrade = array();

//$APCategory = $ldiscipline->getAPCategoryInfo(-1);
$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagID";
$APTagID = $ldiscipline->returnArray($sql, 2);

$sql = "SELECT conductString, UserID FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE Year='$year' AND Semester='$semester' AND StudentID=$uid";
$data = $ldiscipline->returnArray($sql, 2);

for($i=0; $i<sizeof($data); $i++) {
	$teacherID = $data[$i][1];
	$tempGrade = split(",",$data[$i][0]);
	for($j=0; $j<sizeof($tempGrade); $j++) {
		$g = $tempGrade[$j];
		$line = split(":",$g);
		list ($catid, $gradeNew) = $line;
		if($catid!="" && $uid!="")	
			$conductGrade[$teacherID][$catid] = $gradeNew;
	}
}

?>

<table width="100%" cellpadding="3" cellspacing="0" border="0">
	<tr>
		<td class='tablebluetop tabletoplink'>&nbsp;</td>
		<?
		for($i=0; $i<sizeof($APTagID); $i++) {
			echo "<td class='tablebluetop tabletopnolink'>".intranet_htmlspecialchars($APTagID[$i]['TagName'])."</td>";	
		}
		?>
	</tr>
	<?
	$stdInfo = $ldiscipline->getStudentNameByID($uid);

	$yearID = Get_Current_Academic_Year_ID();
	$name_field = getNameFieldByLang('USR.');
	/*
	$sql = "SELECT USR.UserID, $name_field as name FROM INTRANET_USER USR LEFT OUTER JOIN 
				INTRANET_CLASSTEACHER CLSTCH ON (USR.UserID=CLSTCH.UserID) LEFT OUTER JOIN 
				INTRANET_SUBJECT_TEACHER SUBTCH ON (USR.UserID=SUBTCH.UserID) LEFT OUTER JOIN 
				INTRANET_CLASS CLS ON ((CLSTCH.ClassID=CLS.ClassID OR SUBTCH.ClassID=CLS.ClassID) AND (CLSTCH.UserID=USR.UserID OR SUBTCH.UserID=USR.UserID))
			WHERE 
				CLS.ClassName='".$stdInfo[0]['ClassName']."' AND 
				USR.RecordType=1 AND
				USR.UserID!=".$_SESSION['UserID']."
			GROUP BY USR.UserID
			ORDER BY name
			";
	*/		
	
if($ldiscipline->displayOtherTeacherGradeInConductGrade) {
	$extraTable = "LEFT OUTER JOIN SUBJECT_TERM_CLASS_TEACHER SUBTCH ON (USR.UserID=SUBTCH.UserID) LEFT OUTER JOIN 
					SUBJECT_TERM_CLASS_YEAR_RELATION stcyr ON (stcyr.SubjectGroupID=SUBTCH.SubjectGroupID) LEFT OUTER JOIN
					YEAR y ON (CLS.YearID=y.YearID OR stcyr.YearID=y.YearID)
					";
}	
	$sql = "SELECT USR.UserID, $name_field as name FROM INTRANET_USER USR LEFT OUTER JOIN 
				YEAR_CLASS_TEACHER CLSTCH ON (USR.UserID=CLSTCH.UserID) LEFT OUTER JOIN 
				YEAR_CLASS CLS ON (CLS.YearClassID=CLSTCH.YearClassID AND CLS.AcademicYearID=$yearID)
				$extraTable
			WHERE 
				(CLS.ClassTitleEN='".$stdInfo[0]['ClassName']."' OR CLS.ClassTitleB5='".$stdInfo[0]['ClassName']."') AND 
				USR.RecordType=1 AND
				USR.UserID!=".$_SESSION['UserID']."
			GROUP BY USR.UserID
			ORDER BY name
			";
	$data = $ldiscipline->returnArray($sql, 2);
//echo $sql;
	for($i=0; $i<sizeof($data); $i++) {
		$css = ($i%2==0) ? 1 : 2;
		echo "<tr class='tablebluerow$css'><td class='tabletext'>".$data[$i][1]."</td>";
		for($j=0; $j<sizeof($APTagID); $j++) {
			echo "<td class='tabletext'>".$conductGrade[$data[$i][0]][$APTagID[$j]['TagID']]."</td>";	
		}
		echo "</tr>";	
	}
	?>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>