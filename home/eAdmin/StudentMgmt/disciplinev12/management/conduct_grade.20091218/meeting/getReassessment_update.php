<?php

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$teacherID = explode(',', $teacherID);

for($i=0; $i<sizeof($teacherID); $i++) {
	$newGrade = ${"radio_".$teacherID[$i]};	
	
	if($newGrade != "") {	# with new mark, insert/update to "DISCIPLINE_MS_STUDENT_CONDUCT_SEMI"
	
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_MS_STUDENT_CONDUCT_SEMI WHERE Year='$year' AND Semester='$semester' AND StudentID=$uid AND UserID=".$teacherID[$i];
		$count = $ldiscipline->returnVector($sql);
		
		if($count[0]==0) {
			# insert	
			$ldiscipline->insertSemiConductGrade($year, $semester, $form, $class, $uid, $teacherID[$i], $tagid, $newGrade);
		} else {
			# update
			$ldiscipline->updateSemiConductGrade($year, $semester, $form, $class, $uid, $teacherID[$i], $tagid, $newGrade);
		}
		
	} else {			# no new mark, them retrieve the old one to insert/update to "DISCIPLINE_MS_STUDENT_CONDUCT_SEMI"
		$sql = "SELECT conductString FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE Year='$year' AND Semester='$semester' AND StudentID=$uid AND UserID=".$teacherID[$i];
		$result = $ldiscipline->returnVector($sql);
		
		if($result[0]) {	# with existing record in "DISCIPLINE_MS_STUDENT_CONDUCT"
			$gradeInfo = split(',', $result[0]);
			
			for($j=0; $j<sizeof($gradeInfo); $j++) {
				$data = split(':', $gradeInfo[$j]);	
				if($tagid==$data[0]) $newGrade = $data[1];
			}
			
			$sql = "SELECT COUNT(*) FROM DISCIPLINE_MS_STUDENT_CONDUCT_SEMI WHERE Year='$year' AND Semester='$semester' AND StudentID=$uid AND UserID=".$teacherID[$i];
			$count = $ldiscipline->returnVector($sql);
			if($count[0]==0) {
				# insert	
				$ldiscipline->insertSemiConductGrade($year, $semester, $form, $class, $uid, $teacherID[$i], $tagid, $newGrade);
			} else {
				# update
				$ldiscipline->updateSemiConductGrade($year, $semester, $form, $class, $uid, $teacherID[$i], $tagid, $newGrade);
			}
		}
	}
}
$ldiscipline->updateFinalConductGrade("DISCIPLINE_MS_STUDENT_CONDUCT_SEMI", $year, $semester, $form, $class, $uid);

intranet_closedb();
?>
<body onLoad="document.form1.submit();">
<form name="form1" method="post" action="reAssessment.php">
	<input type="hidden" name="year" id="year" value="<?=$year?>">
	<input type="hidden" name="semester" id="semester" value="<?=$semester?>">
	<input type="hidden" name="form" id="form" value="<?=$form?>">
	<input type="hidden" name="class" id="class" value="<?=$class?>">
	<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
	<input type="hidden" name="tagid" id="tagid" value="<?=$tagid?>">
	<input type="hidden" name="submitFlag" id="submitFlag" value="YES">
</form>
<script language="javascript">
<!--
	document.form1.submit();
//-->
</script>
</body>