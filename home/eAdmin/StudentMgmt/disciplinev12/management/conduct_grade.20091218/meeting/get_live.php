<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=BIG5");

$ldiscipline = new libdisciplinev12();

//$APCategory = $ldiscipline->getAPCategoryInfo(-1);
$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagID";
$TagAry = $ldiscipline->returnArray($sql, 2);

$form = str_replace("__", " ", $form);
$form = str_replace("_", ".", $form);

if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$meetingStudentList = $ldiscipline->getConductMeetingStudentListByAdmin($year, $semester, $form, $class, $isAdjust);
} else {
	$meetingStudentList = $ldiscipline->getConductMeetingStudentList($year, $semester, $form, $class, $isAdjust);
}

?>
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr class="tablebluetop">
		<td class='tabletopnolink'><?=$i_general_class?></td>
		<td class='tabletopnolink'><?=$i_UserClassNumber?></td>
		<td class='tabletopnolink'><?=$i_UserStudentName?></td>
		<?
		for($i=0; $i<sizeof($TagAry); $i++) {
			echo "<td class='tabletopnolink'>".$TagAry[$i][1]."</td>";	
		}
		?>
		<td class='tabletopnolink'><?=$iDiscipline['Overall']?></td>
		<td class='tabletopnolink'><?=$i_Discipline_System_Discipline_Conduct_Last_Updated?></td>
		<td class='tabletopnolink' align='center'><?=$i_general_status?></td>
	</tr>
	<?
	for($i=0; $i<sizeof($meetingStudentList); $i++) {
		$css = ($i%2==0) ? 1 : 2;
		list($studentid, $className, $formName, $isAdjust) = $meetingStudentList[$i];
		$status = ($isAdjust==1) ? $iDiscipline['Revised'] : $iDiscipline['WaitingForReview'];
		
		$stdInfo = $ldiscipline->getStudentNameByID($studentid);
		list($tempID, $tempName, $tempClsName, $tempClsNo) = $stdInfo[0];
	
		$sql = "SELECT conductString, IntegratedConductGrade, LEFT(DateModified,10) FROM DISCIPLINE_MS_STUDENT_CONDUCT WHERE Year='$year' AND Semester='$semester' AND StudentID=$studentid AND UserID=".$_SESSION['UserID'];
		$grade = $ldiscipline->returnArray($sql);
		$grade = $grade[0];
		
		$tempGrade = split(",",$grade[0]);
		
		$text = "";
		for($j=0; $j<sizeof($tempGrade); $j++) {
			$g = $tempGrade[$j];
			$line = split(":",$g);
			list ($catid, $gradeNew) = $line;
			if($catid!="" && $studentid!="")	
				$conductGrade[$studentid][$catid] = $gradeNew;
		}
		
		echo "<tr class='tablebluerow$css'>
			<td class='tabletext'>$tempClsName</td>
			<td class='tabletext'>$tempClsNo</td>
			<td class='tabletext'><a href='javascript:;' onclick='goSubmit($tempID);' class='tablelink'>$tempName</a></td>";
		
		for($j=0; $j<sizeof($TagAry); $j++) {
			echo "<td class='tabletext'>".$conductGrade[$studentid][$TagAry[$j][0]]."</td>";
		}
		
		
		echo "<td class='tabletext'>$grade[1]</td>
			<td class='tabletext'>$grade[2]</td>
			<td class='tabletext' align='center'>$status</td>
		</tr>";

	}
	?>
</table>
<?
intranet_closedb();
?>