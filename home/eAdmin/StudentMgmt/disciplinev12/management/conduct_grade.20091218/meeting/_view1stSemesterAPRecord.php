<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$ldiscipline = new libdisciplinev12();

$MODULE_OBJ['title'] = $eDiscipline['Award_and_Punishment'];

# Start layout
$linterface->LAYOUT_START();

?>
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td colspan="3">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="40%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="60%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td class="tablerow1">�W�Ǵ�</td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_Student?></td>
					<td class="tablerow1">Chan Tai Man (1A-01)</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr class="tablebluetop">
		<td class='tabletoplink' width="60%"><?=$i_Merit_Punishment?></td>
		<td class='tabletoplink' width="20%"><?=$i_Discipline_Times?></td>
		<td class='tabletoplink' width="20%"><?=$iDiscipline['Percentage']?></td>
	</tr>
	<tr class='tablebluerow1'>
		<td class='tabletext'>Hair too long</td>
		<td class='tabletext'>5</td>
		<td class='tabletext'>31.25%</td>
	</tr>
	<tr class='tablebluerow2'>
		<td class='tabletext'>Skirt Too short</td>
		<td class='tabletext'>10</td>
		<td class='tabletext'>62.5%</td>
	</tr>
	<tr class='tablebluerow1'>
		<td class='formfieldtitle'>Late</td>
		<td class='formfieldtitle'>1</td>
		<td class='formfieldtitle'>6.25%</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr class="tablebottom">
		<td colspan="3">[<?=$eDiscipline["PunishmentRecord"]?>]</td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="70%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="70%"><?=$iDiscipline['TotalPunishment']?> :</td>
					<td width="30%">16</td>
				</tr>
				<tr>
					<td width="70%">Warning :</td>
					<td width="30%">1</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br />
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr class="tablebluetop">
		<td class='tabletoplink' width="60%"><?=$i_Merit_Award?></td>
		<td class='tabletoplink' width="20%"><?=$i_Discipline_Times?></td>
		<td class='tabletoplink' width="20%"><?=$iDiscipline['Percentage']?></td>
	</tr>
	<tr class='tablebluerow1'>
		<td class='formfieldtitle'>Event Helper</td>
		<td class='formfieldtitle'>3</td>
		<td class='formfieldtitle'>21.43%</td>
	</tr>
	<tr class='tablebluerow2'>
		<td class='tabletext'>Services</td>
		<td class='tabletext'>11</td>
		<td class='tabletext'>78.57%</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr class="tablebottom">
		<td colspan="3">[<?=$eDiscipline["AwardRecord"]?>]</td>
	</tr>
	<tr>
		<td colspan="3">
			<table width="70%" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td width="70%"><?=$iDiscipline['TotalAward']?> :</td>
					<td width="30%">29</td>
				</tr>
				<tr>
					<td width="70%">Merit :</td>
					<td width="30%">3</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3" align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();")?></td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
