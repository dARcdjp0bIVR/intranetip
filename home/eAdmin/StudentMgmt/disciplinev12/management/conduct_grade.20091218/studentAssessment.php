<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if(!isset($uid))
	header("Location: index.php");
	
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$thisStudent = new libuser($uid);

$thisWebSAMSRegNo = $thisStudent->WebSamsRegNo;
list($thisPhotoFilePath, $thisPhotoURL) = $thisStudent->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
if (file_exists($thisPhotoFilePath)) {
	list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
	$photo = "<img src='$thisPhotoURL' width='$originalWidth' height='$originalHeight' border='0'>";	
} 

$stdInfo = $ldiscipline->getStudentNameByID($uid);

list($tempID, $tempName, $tempClsName, $tempClsNo) = $stdInfo[0];
$stdName = "$tempName ($tempClsName-$tempClsNo)";

$APTable = $ldiscipline->conductGradeAPTable($year, $semester, $uid, "");

# check whether show the "View 1st term conduct grade" button 
//$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$yearID = Get_Current_Academic_Year_ID();
$semester_data = getSemesters($yearID);
//debug_pr($semester_data);

$i = 0;
foreach($semester_data as $key=>$val)
{
	if($val==$semester && $i!=0) {
		$view1stSemesterBtn = "<tr><td colspan='2'>".$linterface->GET_BTN($iDiscipline['ViewConductGradeOf1stSemester'], "button", "viewAPRecord();")."</td></tr>";
		break;
	}
	$i++;
}

$gradeArray = split("\n",get_file_content("$intranet_root/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/gradeFile.txt"));

# remark table
$remarkTable = $ldiscipline->getConductGradeRemarkTable();

$TAGS_OBJ[] = array($iDiscipline['ConductGradeAssessment'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/index.php", 1);
$TAGS_OBJ[] = array($iDiscipline['ConductGradeMeeting'], "/home/eAdmin/StudentMgmt/disciplinev12/management/conduct_grade/meeting/index.php", 0);

# menu highlight setting
$CurrentPage = "Management_ConductGrade";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# navigation bar
$PAGE_NAVIGATION[] = array($i_Discipline_Student_List, "index.php?semester=$semester&form=$form&class=$class&viewList=1");
$PAGE_NAVIGATION[] = array($ldiscipline->getClassNameByClassID(substr($class,2)), "studentList.php?semester=$semester&form=$form&class=$class&viewList=1");
$PAGE_NAVIGATION[] = array($button_edit, "");

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--

function viewAPRecord() {
	window.open('view1stSemesterAPRecord.php?year=<?=$year?>&semester=<?=$semester?>&uid=<?=$uid?>','view','width=400,height=500,scrollbars=yes,resizable=yes');	
}

function checkForm(obj) {
	start = 1;
	end = 4;
	total = 0;
	grade = <?= sizeof($gradeArray)?>;
	
	for(i=start; i<=end; i++) {
		for(j=0; j<grade; j++) {
			radioChoice = eval("obj.radio_" +i+ "[" +j+ "]");		
			if(typeof(radioChoice)!="undefined" && radioChoice.checked==true) 
				total += 1;
		}
	}

	if(total<4) {
		alert("<?=$i_general_please_select.' '.$iDiscipline['Grade']?>");
		return false;	
	}
}
//-->
</script>
<form name="form1" method="POST" action="assessment_update.php" onSubmit="return checkForm(document.form1)">
<br />
<table width="100%">
	<tr>
		<td><?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION)?></td>
	<tr>
</table>
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
	<tr valign="top">
		<td class="tabletext" align="center">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="80%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td width="80%" class="tablerow1"><?=$semester?></td>
				</tr>
				<tr>
					<td width="20%" class="formfieldtitle"><?=$i_Discipline_Student?></td>
					<td width="80%" class="tablerow1"><?=$stdName?></td>
				</tr>
			</table>
		</td>
		<td align="right">
			<?=$photo?>
		</td>
	</tr>
	<?=$view1stSemesterBtn?>
</table>
<br />
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td valign="top">
			<?=$APTable?>
		</td>
		<td valign="top" width="40%">
		&gt;<?=$iDiscipline['Assessment']?>
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
			<?=$ldiscipline->getConductGradeMarkingTable($year, $semester, $form, $uid);?>
				<tr>
					<td colspan="8">
						<?= $ldiscipline->showWarningMsg($i_UserRemark, $remarkTable) ?>
					</td>
				</tr>
				<tr>
					<td colspan="8">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['SubmitAndNext'], "submit", "document.getElementById('goNext').value=1", "submitNext")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='studentList.php?year=$year&semester=$semester&form=$form&class=$class'")?>
					</td>
				</tr>
			</table>
			<table width="100%" cellpadding="0" cellspacing="0" border="1">
				<tr>
					<td height="100">
						<iframe src="referenceTable.php?year=<?=$year?>&semester=<?=$semester?>&uid=<?=$uid?>" width="100%" height="300" frameborder="0"></iframe>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="year" id="year" value="<?=$year?>">
<input type="hidden" name="semester" id="semester" value="<?=$semester?>">
<input type="hidden" name="form" id="form" value="<?=$form?>">
<input type="hidden" name="class" id="class" value="<?=$class?>">
<input type="hidden" name="uid" id="uid" value="<?=$uid?>">
<input type="hidden" name="goNext" id="goNext" value="">
<p>&nbsp</p>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
