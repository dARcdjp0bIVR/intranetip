<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html("popup.html");
$ldiscipline = new libdisciplinev12();

$MODULE_OBJ['title'] = $eDiscipline['Award_and_Punishment'];

# Start layout
$linterface->LAYOUT_START();

$stdInfo = $ldiscipline->getStudentNameByID($uid);
list($tempID, $tempName, $tempClsName, $tempClsNo) = $stdInfo[0];
$stdName = "$tempName ($tempClsName-$tempClsNo)";

$yearID = Get_Current_Academic_Year_ID();

$semester_data = getSemesters($yearID);
$i = 0;
foreach($semester_data as $key=>$val) {
	$id = $key;
	$name = $val;
	if($i==0)	break;
}
/*
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	if($i==0) {
		break;
	}
}
*/
$APTable = $ldiscipline->conductGradeAPTable($year, $name, $uid);

?>
<table width="100%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td colspan="3">
			<table width="100%" cellpadding="4" cellspacing="0" border="0">
				<tr>
					<td width="40%" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
					<td width="60%" class="tablerow1"><?=getCurrentAcademicYear()?></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_System_Conduct_Semester?></td>
					<td class="tablerow1"><?=$name?></td>
				</tr>
				<tr>
					<td class="formfieldtitle"><?=$i_Discipline_Student?></td>
					<td class="tablerow1"><?=$stdName?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">
			<?=$APTable?>
		</td>
	</tr>
	<tr>
		<td colspan="3" align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?></td>
	</tr>
</table>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
