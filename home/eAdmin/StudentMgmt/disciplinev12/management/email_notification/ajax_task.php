<?php
// Editing by 
/*
 * 2015-06-03 (Carlos): Added global flag $globalDoNotSendToAlternativeEmail to disable sending to user alternative emails.
 * 2015-06-01 (Carlos): Modified to cater more than one class teachers
 * 2014-06-11 (Carlos): Created for customization $sys_custom['eDiscipline']['PooiToMiddleSchool']
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
intranet_auth();
intranet_opendb();

// disable sending to user alternative emails
$globalDoNotSendToAlternativeEmail = true;

$ldiscipline = new libdisciplinev12_cust();

if(
	!($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View") || !$sys_custom['eDiscipline']['PooiToMiddleSchool'])
) 
{
	//$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	intranet_closedb();
	exit;
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

switch($task)
{
	case "get_record_list":
		echo $ldiscipline_ui->getEmailNotificationRecordList($_REQUEST);
	break;
	
	case "send_email":
		$fcm = new form_class_manage();
		$lwebmail = new libwebmail();
		
		$template_type = $_REQUEST['TemplateType'];
		$send_status = $_REQUEST['SendStatus'];
		
		$options = array('TemplateType'=>$template_type,'SendStatus'=>$send_status);
		$RecordID = $_REQUEST['RecordID'];
		$RecordID_count = count($RecordID);
		$recordIdAry = array();
		for($i=0;$i<$RecordID_count;$i++){
			$ary = explode(",",$RecordID[$i]);
			$recordIdAry = array_merge($recordIdAry,$ary);
		}
		$options['RecordID'] = $recordIdAry;
		//debug_r($options);
		
		$records = $ldiscipline->getMisconductAndPunishmentNotificationRecords($options);
		$record_count = count($records);
		
		$sendResult = array();
		for($i=0;$i<$record_count;$i++){
			$year_class_id = $fcm->Get_Student_Studying_Class($records[$i]['UserID']);
			$year_class = new year_class($year_class_id);
			$year_class->Get_Class_Teacher_List(false);
			$class_teacher_list = $year_class->ClassTeacherList;
			$class_teacher_name_ary = Get_Array_By_Key($class_teacher_list, 'TeacherName');
			$class_teacher_id_ary = Get_Array_By_Key($class_teacher_list, 'UserID');
			
			//$class_teacher_name = $class_teacher_list[0]['TeacherName'];
			//$class_teacher_id = $class_teacher_list[0]['UserID'];
			$class_teacher_name = implode(", ", $class_teacher_name_ary);
			$records[$i]['ClassTeacherName'] = $class_teacher_name;
			
			if(count($class_teacher_id_ary) > 0){
				$Subject = $records[$i]['ClassName'].'('.$records[$i]['ClassNumber'].') '.$records[$i]['StudentName'];
				$Subject .= $Lang['eDiscipline']['EmailNotifictionCust']['EmailTitleArr'][$TemplateType];
				$Message = $ldiscipline_ui->getEmailNotificationDiv($TemplateType, $records[$i]);
				$ToArray = $class_teacher_id_ary;
				
				$sendResult[$i] = $lwebmail->sendModuleMail($ToArray,$Subject,$Message);
				
				if($sendResult[$i]){
					if($send_status == '1'){ // sent records are resend
						$sql = "UPDATE DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD SET DateModified=NOW(),ModifiedBy='".$_SESSION['UserID']."' WHERE RecordID='".$records[$i]['RecordID']."'";
						$ldiscipline->db_db_query($sql);
					}else{ // records are first time send
						$record_type = $TemplateType == 5? '2':'1';
						$rids = explode(",",$records[$i]['RecordID']);
						sort($rids,SORT_NUMERIC);
						$rids_count = count($rids);
						for($j=0;$j<$rids_count;$j++){
							$data = array('StudentID'=>$records[$i]['UserID'],'RelatedRecordID'=>$rids[$j],'SendStatus'=>'1','RecordType'=>$record_type);
							$ldiscipline->updateEmailNotificationRecord($data);
						}
						
						$sql = "SELECT RecordID FROM DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD WHERE StudentID='".$records[$i]['UserID']."' AND AcademicYearID='".$records[$i]['AcademicYearID']."' AND TemplateType='".$template_type."' AND RelatedRecordID='".implode(',',$rids)."'";
						$rs = $ldiscipline->returnVector($sql);
						if(count($rs)>0 && $rs[0] > 0){
							$sql = "UPDATE DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD SET DateModified=NOW(),ModifiedBy='".$_SESSION['UserID']."' WHERE RecordID='".$rs[0]."'";
							$ldiscipline->db_db_query($sql);
						}else{ // insert new sent records
							$record_ary_text = '';
							$delim = '';
							for($j=0;$j<count($records[$i]['RecordAry']);$j++){
								$record_ary_text .= $delim.$records[$i]['RecordAry'][$j][0].'<br>'.$records[$i]['RecordAry'][$j][1];
								$delim = '<br>';
							}
							$sql = "INSERT INTO DISCIPLINE_EMAIL_NOTIFICATION_SEND_RECORD 
									(StudentID,AcademicYearID,TemplateType,RelatedRecordID,RecordDetail,RecordAry,DateInput,DateModified,InputBy,ModifiedBy) VALUES 
									('".$records[$i]['UserID']."','".$records[$i]['AcademicYearID']."','$template_type','".implode(',',$rids)."','".$ldiscipline->Get_Safe_Sql_Query($records[$i]['RecordDetail'])."','".$ldiscipline->Get_Safe_Sql_Query($record_ary_text)."',NOW(),NOW(),'".$_SESSION['UserID']."','".$_SESSION['UserID']."')";
							$ldiscipline->db_db_query($sql);
							
						}
					}
				}
			}
		}
		
		echo in_array(false,$sendResult)? '0':'1';
	break;

}

intranet_closedb();
?>