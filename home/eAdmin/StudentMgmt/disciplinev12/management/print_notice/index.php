<?php
//Modifying by: 

######## Change Log [Start] ################
#
# - 2011-11-10 [Henry Chow]
#	passed $targetClass to function, in order to display "Class" filter
#
# - 2010-07-29 [YatWoon]
#	Move entire UI into libnotice_ui->Get_Print_Module_Notice_UI(); for later easier development
#
# - 2009-12-30 [YatWoon]
#	Add Title filter
#
######## Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libnotice_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$ldiscipline = new libdisciplinev12();

$linterface         = new interface_html();
$NoticeUI = new libnotice_ui();
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Management_PrintNotice";

# Check access right (View page)
if(
	!($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View") || 
	$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View"))
) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

### Title ###
$TAGS_OBJ[] = array($Lang['eDiscipline']['PrintNotice']);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$status = $_REQUEST['status'];
$sign_status = $_REQUEST['sign_status'];
$print_status = $_REQUEST['print_status'];
$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$keyword = $_REQUEST['keyword'];
$title = $_REQUEST['title'];
$targetClass = $_REQUEST['targetClass'];

//echo $linterface->Include_JS_CSS();

echo $NoticeUI->Get_Print_Module_Notice_UI($ldiscipline->Module,$title,$status,$sign_status,$print_status,$StartDate,$EndDate,$keyword, $submitOptionFlag, $updateOption, $option, $targetClass);

intranet_closedb();
$linterface->LAYOUT_STOP();
?>

