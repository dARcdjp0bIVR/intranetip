<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lu = new libuser();
$lclass = new libclass();

# Check access right
//$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Award_Punishment-New", "You have no priviledge to access this page.");

$MODULE_OBJ['title'] = $eDiscipline["TeacherStaffList"];

$teacherTypeSelection = $lclass->getTeacherTypeSelection("onChange='document.FormChoosePIC.submit();'", $teacherType);


$teacherList = $lu->returnUsersType2(1, $teacherType);
$SelectPIC = "<select name=ChooseUserID[] size=23 multiple>\n";
for($i=0; $i<sizeof($teacherList); $i++)
	$SelectPIC .= "<option value=".$teacherList[$i][0].">".$teacherList[$i][1]."</option>\n";
$SelectPIC .= "</select>\n";

/*
if($teacherType!="") {
	if($sys_custom['wscss_disciplinev12_student_access_cust']) {
		$select_teacher = $lclass->getTeacherAndStudent("size=\"21\" multiple=\"multiple\" name=\"targetID[]\"", $teacherType, $conds);
	} else {
		$select_teacher = $lclass->getTeacher("size=\"21\" multiple=\"multiple\" name=\"targetID[]\"", $teacherType, $conds);
	}
}
*/

$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>
<script language="javascript">
function AddOptions(obj){
	par = opener.window;
	parObj = opener.window.document.getElementById("<?=$fieldname?>");
	checkOption(obj);
	par.checkOption(parObj);
	i = obj.selectedIndex;
	while(i!=-1){
		par.checkOptionAdd(parObj, obj.options[i].text, obj.options[i].value);
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
	//par.checkOptionAdd(parObj, "<? for($i = 0; $i < 40; $i++) echo " "; ?>", "");
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}
</script>
<form name="FormChoosePIC" action="choose_pic.php" method="post">
	<table id="html_body_frame" width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td>
				<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
					<tr>
						<td valign="top" nowrap="nowrap" width="30%" class='formfieldtitle'>
							<span class="tabletext">
								<?=$eDiscipline["SelectTeacherStaff"]?>
							</span>
						</td>
						</tr>
		<tr>
			<td valign="top" nowrap="nowrap" colspan="2"><span class="tabletext"> <?=$Lang['eDiscipline']['TeacherType']?>: </span><?=$teacherTypeSelection ?></td>
		</tr>		
		<? if($teacherType!="") { ?>
					<tr>
						<td>
							<table border='0' cellspacing='1' cellpadding='1'>
								<tr>
									<td><?=$SelectPIC?></td>
									<td>
										<?= $linterface->GET_BTN($button_add, "button","checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") . "<br>"; ?>
										<?= $linterface->GET_BTN($button_select_all, "button","SelectAll(this.form.elements['ChooseUserID[]']); return false;") . "<br>"; ?>
									</td>
								</tr>
							</table>
						</td>
					</tr>
		<? } ?>
					<tr>
						<td class="dotline" colspan="2">
							<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
					</tr>
					<tr>
						<td align="center" colspan="2">
							<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();") ?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input type=hidden name=fieldname value="<?=$fieldname?>">
</form>
<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>
