<?php
// Using :

##############################################
#
#	Date:   2019-05-02    Bill
#           - prevent Command Injection
#
##############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

# Check connection of the WebSAMS host entered by the user
# Return "Pass" if the host can be connected, otherwise, return "Fail"

# initialization
$ldiscipline = new libdisciplinev12();
$url = $_POST['url'];
$url = OsCommandSafe($url);

# Drop "http://" and Drop the last "/"
$pingPath = str_replace("http://", "", $url);
$pingPath = substr($pingPath, 0, strlen($pingPath)-1);

$pingResultText = system("ping -w 4 $pingPath");
system("killall ping");

//debug_r('pingPath = '.$pingPath);
//debug_r('pingResultText = '.$pingResultText);

$returnString = "";
if ($pingResultText != "")
{
	$returnString = "Pass";
}
else
{
	$returnString = "Fail";
}

echo convert2unicode($returnString, 1);

intranet_closedb();
?>