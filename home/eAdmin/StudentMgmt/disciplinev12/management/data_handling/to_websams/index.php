<?php
# modifying : henry chow

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($intranet_session_language=="en")
{
	$meritStr = "CONCAT(a.ProfileMeritCount,' ',
				CASE (a.ProfileMeritType)
				WHEN 0 THEN '$i_Merit_Warning'
				WHEN 1 THEN '$i_Merit_Merit'
				WHEN 2 THEN '$i_Merit_MinorCredit'
				WHEN 3 THEN '$i_Merit_MajorCredit'
				WHEN 4 THEN '$i_Merit_SuperCredit'
				WHEN 5 THEN '$i_Merit_UltraCredit'
				WHEN -1 THEN '$i_Merit_BlackMark'
				WHEN -2 THEN '$i_Merit_MinorDemerit'
				WHEN -3 THEN '$i_Merit_MajorDemerit'
				WHEN -4 THEN '$i_Merit_SuperDemerit'
				WHEN -5 THEN '$i_Merit_UltraDemerit'
				ELSE 'Error' END, '(s)')";
}
else
{
	$meritStr = "CONCAT(
				CASE (a.ProfileMeritType)
				WHEN 0 THEN '$i_Merit_Warning'
				WHEN 1 THEN '$i_Merit_Merit'
				WHEN 2 THEN '$i_Merit_MinorCredit'
				WHEN 3 THEN '$i_Merit_MajorCredit'
				WHEN 4 THEN '$i_Merit_SuperCredit'
				WHEN 5 THEN '$i_Merit_UltraCredit'
				WHEN -1 THEN '$i_Merit_BlackMark'
				WHEN -2 THEN '$i_Merit_MinorDemerit'
				WHEN -3 THEN '$i_Merit_MajorDemerit'
				WHEN -4 THEN '$i_Merit_SuperDemerit'
				WHEN -5 THEN '$i_Merit_UltraDemerit'
				ELSE 'Error' END, 
				cast(a.ProfileMeritCount as char), '". $Lang['eDiscipline']['Times'] ."', ' '
				)";
}

$student_namefield = getNamefieldByLang("b.");
$clsName = ($intranet_session_language=="en") ? "g.ClassTitleEN" : "g.ClassTitleB5";
$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Award\'>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0 title=\'$i_Merit_Punishment\'>";


#################
### Condition ###
#################

# Academic Year 
//$thisAcademicYearID = (!isset($AcademicYearID) || $AcademicYearID=="") ? Get_Current_Academic_Year_ID() : $AcademicYearID;
$thisAcademicYearID = Get_Current_Academic_Year_ID();
$conds .= ($thisAcademicYearID==0) ? "" : " AND a.AcademicYearID='$thisAcademicYearID'";

# Year Term
$thisSemester = (isset($YearTermID) && $YearTermID!="") ? $YearTermID : getCurrentSemesterID();
if($thisSemester!="" && $thisSemester!="0") {
	$conds .= " AND a.YearTermID='$thisSemester'";
}

# Class
if($ClassID!="0" && $ClassID!="") {	# not "All Classes"
	if(is_numeric($ClassID)) {	# whole form
		//$conds .= " AND g.YearID='$ClassID'";
		$clsConds .= " AND g.YearID='$ClassID'";
	} else {
		$cId = substr($ClassID,2);
		//$conds .= " AND g.YearClassID='$cId'";	
		$clsConds .= " AND g.YearClassID='$cId'";	
	}
}

# PIC
if(isset($PICID) && $PICID!="") {
	$conds .= " AND CONCAT(',',a.PICID,',') LIKE ('%$PICID%')";
}

# Record Type
if(isset($RecordType) && in_array($RecordType,array(GOOD_CONDUCT,MISCONDUCT))) {
	//$conds .= " AND a.RecordType='$RecordType'";
	$typeConds .= " AND a.MeritType='$RecordType'";
}

# Search Str 
if($s!="") {
	//$s = addslashes(trim($s));
	$searchStrConds = "AND (
					g.ClassTitleEN like '%$s%' 
					OR g.ClassTitleB5 like '%$s%'
					OR f.ClassNumber like '%$s%'
					OR $student_namefield like '%$s%'
					OR c.ItemCode like '%$s%'
					OR c.ItemName like '%$s%'
					OR a.RecordDate like '%$s%')";
}

######################
### Selection menu ###
######################
/*
# Academic Year
$yearArr = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYear = getSelectByArray($yearArr, 'name="AcademicYearID" id="AcademicYearID" onChange="reloadForm()"', $thisAcademicYearID, 0, 1);
*/

# Year Term (Semester)
$yearTermTempAry = getSemesters($thisAcademicYearID);
$yearTermIDAry = array_keys($yearTermTempAry);
$yearTermNameAry = array_values($yearTermTempAry);
$yearTermAry = array();
for($i=0, $i_max=sizeof($yearTermIDAry); $i<$i_max; $i++) {
	$yearTermAry[] = array($yearTermIDAry[$i], $yearTermNameAry[$i]);	
}
$yearTermAry = array_merge(array(array("0", $i_Discipline_System_Award_Punishment_Whole_Year)), $yearTermAry);

$selectSemester = getSelectByArray($yearTermAry, 'name="YearTermID" id="YearTermID" onChange="reloadForm()"', $thisSemester, 0, 1);

# Class 
$selectClass = $lclass->getSelectClassWithWholeForm("name=\"ClassID\" id=\"ClassID\" onChange=\"reloadForm()\"", $ClassID, $i_Discipline_System_Award_Punishment_All_Classes);

# PIC
$picAry = $ldiscipline->getPICInArray("DISCIPLINE_MERIT_RECORD", $thisAcademicYear, $thisSemester);
$selectPIC = getSelectByArray($picAry, 'name="PICID" id="PICID" onChange="reloadForm()"', $PICID, 0, 0, $i_Discipline_Detention_All_Teachers);

# Merit Type
$recordType = ($RecordType!="1" && $RecordType != "-1") ? $i_Discipline_System_Award_Punishment_All_Records : "<a href='javascript:reloadForm(0)'>".$i_Discipline_System_Award_Punishment_All_Records."</a>";
$recordType .= " | ";
$recordType .= ($RecordType=="1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards : "<a href='javascript:reloadForm(1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Awards."</a>";
$recordType .= " | ";
$recordType .= ($RecordType=="-1") ? "<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments : "<a href='javascript:reloadForm(-1)'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' border='0' align='absmiddle'> ".$i_Discipline_System_Award_Punishment_Punishments."</a>";



$s = addslashes(trim($s));
$sql = "SELECT
			CONCAT($clsName,' - ',f.ClassNumber) as ClassNameNum,
			$student_namefield as StudentName, 
			IF(a.MeritType>0,'$meritImg','$demeritImg') as meritImg,
			IF(a.ProfileMeritCount=0, '--',
				". $meritStr ."
			) as record,";
if($ldiscipline->Display_ConductMarkInAP) {			
	$sql .= "a.ConductScoreChange,";
}
	
	$sql .= "
			IF(a.ItemID=0, '--', CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;'))) as item,
			a.RecordDate,
			PICID, 
			a.RecordID as reference,
			a.RecordID as action,
			LEFT(a.DateModified,10) as DateModified		
		FROM DISCIPLINE_MERIT_RECORD as a
			INNER JOIN INTRANET_USER as b ON (a.StudentID = b.UserID AND b.RecordStatus IN (0,1,2) AND a.AcademicYearID='$thisAcademicYearID' AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND a.HasTransferredToWebSAMS=0 $typeConds)
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
			LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
			INNER JOIN YEAR_CLASS_USER as f ON (a.StudentID=f.UserID AND f.UserID=a.StudentID)
			INNER JOIN YEAR_CLASS as g ON (f.YearClassID=g.YearClassID AND g.AcademicYearID='$thisAcademicYearID' $clsConds)
			
		WHERE a.DateInput IS NOT NULL 
		$conds
		$searchStrConds
	";


$li = new libdbtable2007($field, $order, $pageNo);

$li->field_array[] = "concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))";
$li->field_array[] = "StudentName";
$li->field_array[] = "meritImg";
$li->field_array[] = "record";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->field_array[] = "ConductScoreChange";
}
$li->field_array[] = "item";
$li->field_array[] = "RecordDate";
$li->field_array[] = "PICID";
$li->field_array[] = "reference";
$li->field_array[] = "action";
$li->field_array[] = "DateModified";

$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_array[] = "0";	
}
$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0);
if($ldiscipline->Display_ConductMarkInAP) {
	$li->wrap_array[] = "0";	
}

$li->IsColOff = "eDiscipline_WebSAMS_NotTransferredIndex";
$li->fieldorder2 = ", a.DateInput desc, ClassNameNum";

# TABLE COLUMN
$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td width='8%' nowrap>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='1'>&nbsp;</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $i_Discipline_Reason2)."</td>\n";
if($ldiscipline->Display_ConductMarkInAP) {
	$li->column_list .= "<td width='80' nowrap>".$li->column($pos++, $eDiscipline['Conduct_Mark'])."</td>\n";
}
$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_System_Item_Code_Item_Name)."</td>\n";
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $Lang['eDiscipline']['EventDate'])."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$i_Profile_PersonInCharge."</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>$i_Discipline_System_Award_Punishment_Reference</td>\n";$pos++;
$li->column_list .= "<td width='80' nowrap>".$eDiscipline["Action"]."</td>\n";$pos++;
$li->column_list .= "<td width='10%' nowrap>".$li->column($pos++, $i_Discipline_Last_Updated)."</td>\n";


$CurrentPage = "Management_DataHandling_ToWebsams";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['eDiscipline']['RecordsNotTransferred'], "index.php", 1);
$TAGS_OBJ[] = array($eDiscipline['Websams_Transition'], "disclaimer.php", 0);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$filters = $selectSemester." ".$selectClass." ".$selectPIC;
?>

<script language="javascript">
function reloadForm(val) {
	if(val!=undefined) {
		$('#RecordType').val(val);	
	}
	document.form1.submit();	
}



var xmlHttp
var jsDetention = [];
var jsWaive = [];
var jsHistory = [];
var jsRemark = [];

function showResult(str,flag)
{

	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "../../award_punishment/get_live.php?flag="+flag;
	url = url + "&RecordID=" + str
	url = url + "&sid=" + Math.random()

	if(flag=='detention') {
		xmlHttp.onreadystatechange = stateChanged 
	} else if(flag=='waived') {
		xmlHttp.onreadystatechange = stateChanged2 
	} else if(flag=='record_remark') {
		xmlHttp.onreadystatechange = stateChanged3 
	}  else if(flag=='history') {
		xmlHttp.onreadystatechange = stateChanged4 
	}
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)

} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_detail"+id).style.zIndex = "1";
		document.getElementById("show_detail"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_detail"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged2() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_waive"+id).style.zIndex = "1";
		document.getElementById("show_waive"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_waive"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_remark"+id).style.zIndex = "1";
		document.getElementById("show_remark"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_remark"+id).style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged4() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		var id = document.form1.clickID.value;
		document.getElementById("show_history"+id).style.zIndex = "1";
		document.getElementById("show_history"+id).innerHTML = xmlHttp.responseText;
		document.getElementById("show_history"+id).style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function hideAllOtherLayer() {
	var layer = "";
	
	if(jsDetention.length != 0) {
		for(i=0;i<jsDetention.length;i++) {
			layer = "show_detail" + jsDetention[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsWaive.length != 0) {
		for(i=0;i<jsWaive.length;i++) {
			layer = "show_waive" + jsWaive[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsRemark.length != 0) {
		for(i=0;i<jsRemark.length;i++) {
			layer = "show_remark" + jsRemark[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
	if(jsHistory.length != 0) {
		for(i=0;i<jsHistory.length;i++) {
			layer = "show_history" + jsHistory[i];
			MM_showHideLayers(layer,'','hide');	
		}
	}
}


function getObject( obj ) {

	if ( document.getElementById ) {
		obj = document.getElementById( obj );
	
	} else if ( document.all ) {
		obj = document.all.item( obj );
	
	} else {
		obj = null;
	}
	
	return obj;
}

function moveObject( obj, e, moveX ) {
	var tempX = 0;
	var tempY = 0;
	var offset = 5;
	var objHolder = obj;

	obj = getObject( obj );
	if (obj==null) {return;}
	
	if (document.all) {
		tempX = event.clientX + document.documentElement.scrollLeft;
		tempY = event.clientY + document.documentElement.scrollTop;
		
	} else {
		tempX = e.pageX;
		tempY = e.pageY;
		
	}
	if(moveX != undefined)
		tempX = tempX - moveX
	else 
		if (tempX < 0){tempX = 0} else {tempX = tempX - 320}
	
	if (tempY < 0){tempY = 0} else {tempY = tempY}

	obj.style.top  = (tempY + offset) + 'px';
	obj.style.left = (tempX + offset) + 'px';
	
	displayObject( objHolder, true );

}

function displayObject( obj, show ) {

	obj = getObject( obj );
	if (obj==null) return;
	
	obj.style.display = show ? 'block' : 'none';
	obj.style.visibility = show ? 'visible' : 'hidden';
}

function changeClickID(id) {
	document.form1.clickID.value = id;
	hideAllOtherLayer();	
}
</script>

<form name="form1" id="form1" method="post" action="">
<br "style=clear:both" />
<div class="content_top_tool">
	<div class="Conntent_search"><input type="text" id="s" name="s" value="<?=stripslashes(stripslashes($s))?>" /></div>
</div>
<br style="clear:both" />
<table width="100%">
<tr>
<td>
<?=$filters?>
</td>
<td align="right">
<?=$recordType?>
</td>
</tr>
</table>
<br "style=clear:both" />
<br "style=clear:both" />
<?=$li->display()?>


<input type="hidden" name="clickID" id="clickID" value="">
<input type="hidden" name="RecordType" id="RecordType" value="">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
