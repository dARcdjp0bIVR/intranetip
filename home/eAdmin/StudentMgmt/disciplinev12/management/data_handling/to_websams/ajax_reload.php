<?php

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$Action = stripslashes($_REQUEST['Action']);

if ($Action == 'Reload_Step2_Table')
{
	$AcademicYearID = $_REQUEST['AcademicYearID'];
	$FileName = $_REQUEST['FileName'];
	$RecordTypeStr = $_REQUEST['RecordTypeStr'];
	
	echo $ldiscipline->Get_WebSAMS_Step2_Table($FileName, $AcademicYearID, $RecordTypeStr);
}

intranet_closedb();

?>