<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$AgreedDisclaimer = $_POST['AgreedDisclaimer'];
if ($AgreedDisclaimer != 1 && $Result=='') {	// $Result=='' means not from step1_update.php
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT($Lang['eDiscipline']['NotYetAgreeDisclaimer'], 'disclaimer.php');
}
else {
	$AgreedDisclaimer = 1;
}

# menu highlight setting
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Management_DataHandling_ToWebsams";

$TAGS_OBJ[] = array($Lang['eDiscipline']['RecordsNotTransferred'], "index.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Websams_Transition'], "step1.php", 1);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Steps table
$STEPS_OBJ = $ldiscipline->getWebSAMSTransitionStepsArr($currentStep=1);

# Start layout
$linterface->LAYOUT_START();

##########

# Initialization
# If the user has entered the URL before, use the entered one
# else if the user is "back" from other pages, retrieve the url from the POST variable
# else if the user enter this page for the first time, display "http://"
$savedURL = get_file_content($ldiscipline->WebSAMS_FilesPath."/url.txt");
if ($savedURL != "")
{
	$URL = $savedURL;
}
else
{
	$URL = (isset($url) && $url!="")? $url : "http://";
}

# Instruction Row
$instructionHTML = "";
$instruction = $eDiscipline['Websams_Transition_Step1_Instruction4']."<br /><br />".$eDiscipline['Websams_Transition_Step1_Instruction2']."<br />".$eDiscipline['Websams_Transition_Step1_Instruction3'];
$instructionHTML .= $ldiscipline->showWarningMsg($eDiscipline['Instruction'], $instruction);
$instructionHTML .= "<br />";
$warning = $eDiscipline['Websams_Transition_Step1_Instruction1'];
$instructionHTML .= $ldiscipline->showWarningMsg("<font color='red'>".$eComm['Warning']."</font>", $warning);

# File Upload Row
$SampleCSV = "websams_and_studentID_sample.csv";
$UploadTableRow .= "<tr>";
	$UploadTableRow .= "<td valgin='top' class='formfieldtitle tabletext' width='30%'>";
		$UploadTableRow .= "<br />";
		$UploadTableRow .= $eDiscipline["StudentMappingCSVFile"];
		$UploadTableRow .= "<span class=\"tabletextrequire\">*</span>";
		$UploadTableRow .= "<br />";
		$UploadTableRow .= "<span class=\"tabletextremark\" valign=\"middle\">".$eDiscipline['Websams_Transition_CSV_File_Remarks']."</span>";
		$UploadTableRow .= "<br />";
		$UploadTableRow .= "<span>";
		$UploadTableRow .= "<a target='_blank' href='". GET_CSV($SampleCSV) ."' class='tablelink'>
								<img src='".$PATH_WRT_ROOT."/images/2009a/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>".
								$i_general_clickheredownloadsample."
							</a>";
		$UploadTableRow .= "</span>";
	$UploadTableRow .= "</td>";
	$UploadTableRow .= "<td valgin='top'><input class=\"textboxtext\" type='file' name='userfile' id='userfile' size=\"25\"  maxlength=\"255\">";
$UploadTableRow .= "</tr>";

# Teacher mapping Upload Row
$TeacherSampleCSV = "teacher_id_mapping_sample.csv";
$TeacherMappingRow = '';
$TeacherMappingRow .= "<tr>";
	$TeacherMappingRow .= "<td valgin='top' class='formfieldtitle tabletext' width='30%'>";
	$TeacherMappingRow .= "<br />";
		$TeacherMappingRow .= $eDiscipline["TeacherMappingCSVFile"];
		$TeacherMappingRow .= "<br />";
		$TeacherMappingRow .= "<span class=\"tabletextremark\" valign=\"middle\">".$eDiscipline["Websams_Transition_Teacher_CSV_File_Remarks"]."</span>";
		$TeacherMappingRow .= "<br />";
		$TeacherMappingRow .= "<span>";
		$TeacherMappingRow .= "<a target='_blank' href='get_teacher_id_csv.php' class='tablelink'>
								<img src='".$PATH_WRT_ROOT."/images/2009a/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>".
								$eDiscipline["Download_Teacher_CSV"]."
							</a>";
		$TeacherMappingRow .= "</span>";
		
			
		
		$TeacherMappingRow .= "<br>";
		$TeacherMappingRow .= "<span>";
		$TeacherMappingRow .= "<a target='_blank' href='". GET_CSV($TeacherSampleCSV) ."' class='tablelink'>
								<img src='".$PATH_WRT_ROOT."/images/2009a/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'>".
								$i_general_clickheredownloadsample."
							</a>";
		$TeacherMappingRow .= "</span>";
	$TeacherMappingRow .= "</td>";
	$TeacherMappingRow .= "<td valgin='top'><input class=\"textboxtext\" type='file' name='teacherfile' id='teacherfile' size=\"25\"  maxlength=\"255\">";
$TeacherMappingRow .= "</tr>";

$LastTransferredDate = $ldiscipline->Get_Last_Transfer_Date_to_WebSAMS();
//$LastTransferredDate = ($LastTransferredDate)? $LastTransferredDate : "-";
$LastTransferredDateHTML = $eDiscipline['LastTransferringDate'].": ".$LastTransferredDate;

					
# Button Row
$ButtonHTML = $linterface->GET_ACTION_BTN($button_next, "button", "javascript:checkForm()", "btnSubmit");


# System Message
$SysMsgHTML = "";
if (isset($Result) && $Result!="")
	$SysMsgHTML = $linterface->GET_SYS_MSG("", $eDiscipline["WebSAMS_Warning_".$Result]);
	

?>

<form name="form1" method="post" enctype="multipart/form-data">
<br />
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<?= $linterface->GET_STEPS($STEPS_OBJ) ?>
		</td>
	</tr>
	
	<tr>
		<td>
			<table width="90%" cellpadding="3" cellspacing="0" border="0" align="center">
				<tr>
					<td colspan="2" align="right">
						<?= $SysMsgHTML ?>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" align="center">
						<?= $instructionHTML ?>
						<br />
						<br />
					</td>
				</tr>
				
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$eDiscipline['WebSAMS_URL']?><span class="tabletextrequire">*</span> 
						<br />
						<span class="tabletextremark" valign="middle"><?=$eDiscipline['Websams_Transition_Step1_url_example']?></span>
					</td>
					<td class="tabletext">
						<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
							<tr>
								<td width="70%">
									<input class="textboxtext" type="text" id="url" name="url" size="50" maxlength="100" value="<?= $URL ?>">
								</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<!--
								<td width="15%" valign="middle" align="center"><a class="tablelink" href="javascript: checkConnection()"><?=$eDiscipline['CheckConnection']?><a/></td>
								<td width="15%" valign="middle" align="center"><span class="tabletext" id="connectionStatus" name="connectionStatus"></span></td>
								-->
							</tr>
							
						</table>
					</td>
				</tr>
				
				<?= $UploadTableRow ?>
				<?= $TeacherMappingRow ?>
				
				<tr>
					<td colspan="2" class="tabletextremark">
						<br />
						<?= $LastTransferredDateHTML ?>
					</td>
				</tr>
				
			</table>
		</td>
	</tr>
	
	<tr>
		<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center"><?=$ButtonHTML?></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<input type="hidden" id="AgreedDisclaimer" name="AgreedDisclaimer" value="<?=$AgreedDisclaimer?>" />

<br />
</form>

<script language="javascript">
<!--
function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	var obj = document.form1;
	
	// Validate the URL
	var urlObj = document.getElementById("url");
	urlObj.value = trim(urlObj.value);
	
	if (!validateURL(urlObj,"<?=$eDiscipline['jsWarning']['InvalidURL']?>"))
	{
		return false;
	}
	
	// insert missing "/" at the back of the url
	urlObj.value = complete_url(urlObj.value);
	
	// Check if user has selected any files
	var fileObj = document.getElementById("userfile");
	if (trim(fileObj.value) == "") {
		 alert('<?=$eDiscipline['jsWarning']['SelectFileFirst']?>');
		 fileObj.focus();
		 return false;
	}
	
	obj.action = "step1_update.php";
	obj.submit();
}

function complete_url(urlStr)
{
	// insert missing "/" at the back of the url
	if (urlStr.lastIndexOf("/") != (urlStr.length - 1))
	{
		urlStr += "/";
	}
	
	return urlStr;
}
//-->
</script>

<script language="javascript">
<!-- 
//Check connection AJAX
var connectionStatusSpan = document.getElementById("connectionStatus");

var callback = {
	success: function ( o )
	{
		//connectionStatusSpan.innerHTML = o.responseText;
		var len = o.responseText.length;
		var result = o.responseText.substr(len-4, 4);
		
		//alert('o.responseText = ' + o.responseText);
		
		if (result == "Pass")
		{
			connectionStatusSpan.innerHTML = "<font color='blue'><b><?=$eDiscipline['Successful']?></b></font>";
		}
		else if (result == "Fail")
		{
			connectionStatusSpan.innerHTML = "<font color='red'><b><?=$eDiscipline['Failed']?></b></font>";
		}
		else
		{
			connectionStatusSpan.innerHTML = "<font color='red'><b><?=$iDiscipline['AccumulativePunishment_Import_Failed_Reason']?></b></font>";
		}
		
	}
}

function checkConnection() {
	// Validate the URL
	var urlObj = document.getElementById("url");
	urlObj.value = trim(urlObj.value);
	
	if (!validateURL(urlObj,"<?=$eDiscipline['jsWarning']['InvalidURL']?>"))
	{
		return;
	}
	
	// insert missing "/" at the back of the url
	urlObj.value = complete_url(urlObj.value);
	
	
	connectionStatusSpan.innerHTML = "<?=$eDiscipline['Connecting']?>" + "...";
	
	formObj = document.form1;
    YAHOO.util.Connect.setForm(formObj);
	
    var path = "check_connection_aj.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
	
}

// End of AJAX
//-->
</script>


<?
$linterface->FOCUS_ON_LOAD("url");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
