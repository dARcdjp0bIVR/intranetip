<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$AgreedDisclaimer = $_POST['AgreedDisclaimer'];
if ($AgreedDisclaimer != 1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT($Lang['eDiscipline']['NotYetAgreeDisclaimer'], 'disclaimer.php');
}

# menu highlight setting
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Management_DataHandling_ToWebsams";

$TAGS_OBJ[] = array($Lang['eDiscipline']['RecordsNotTransferred'], "index.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Websams_Transition'], "step1.php", 1);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Steps table
$STEPS_OBJ = $ldiscipline->getWebSAMSTransitionStepsArr($currentStep=2);

# Start layout
$linterface->LAYOUT_START();

# Initialize variable
$url = $_POST['url'];
$filename = $_POST['filename'];
$numWebSAMSRegNoInCSV = $_POST['numWebSAMSRegNoInCSV'];

##########

## Instruction Row
$instructionHTML = "";
$instructionHTML .= $ldiscipline->showWarningMsg($eComm['Warning'], $eDiscipline['Websams_Transition_Step2_Instruction1']);


## Get merit data
# Get current school year
// $currentYear = date("Y");
// $currentMonth = date("m");
// // $academic_year_start_month & $academic_year_start_day are set in settings.php
// $acadermicMonth = $academic_year_start_month;
// $acadermicDay = $academic_year_start_day;
// if ($currentMonth < $academic_year_start_month)
// {
// 	# 2009-01-01 => search for records after 2008-09-01 also
// 	$acadermicYear = $currentYear - 1;
// }
// else
// {
// 	# 2008-12-01 => search for records before 2008-09-01 only
// 	$acadermicYear = $currentYear;
// }

# Get current school year
$AcademicYearID = ($AcademicYearID=='')? Get_Current_Academic_Year_ID() : $AcademicYearID;

# Construct Navigation
$DisciplineRecordsTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n";
	$DisciplineRecordsTable .= "<tr><td align=\"left\">\n";
	$DisciplineRecordsTable .= $linterface->GET_NAVIGATION2($eDiscipline['RecordsInDiscipline']);
	$DisciplineRecordsTable .= "</td></tr>\n";
$DisciplineRecordsTable .= "</table>\n";

## School Year Selection
$selectSchoolYear = "<select name='AcademicYearID' id='AcademicYearID' onchange='js_Changed_Academic_Year_Selection(this.value);'>";
	$yearArr = $ldiscipline->getAPSchoolYear();
	for($i=0; $i<sizeof($yearArr); $i++) {
		$selectSchoolYear .= "<option value='".$yearArr[$i]['AcademicYearID']."'";
		if($AcademicYearID==$yearArr[$i]['AcademicYearID']) {
			$selectSchoolYear .= " SELECTED";
		}
		$selectSchoolYear .= ">".$yearArr[$i][1]."</option>";
	}
$selectSchoolYear .= "</select>";


# check disable button
$Disable_Next_Step = '';
if ($url == '' || $numWebSAMSRegNoInCSV == 0 || ($NumOfAwardsNotYetTransferred == 0 && $NumOfPunishmentsNotYetTransferred == 0))
	$Disable_Next_Step = 'DISABLED';

## Button Row
$ButtonHTML = "";
$ButtonHTML .= $linterface->GET_ACTION_BTN($button_next, "button", "javascript:checkForm()", "btnNext");
$ButtonHTML .= "&nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "jsBack()");

?>

<script language="javascript">
var submitWindow;
var jsNumOfAwardsNotYetTransferred;
var jsNumOfPunishmentsNotYetTransferred;
var jsSelectedAcademicYearID;
$(document).ready( function() {	
	jsSelectedAcademicYearID = $('select#AcademicYearID').val();
	js_Reload_Record_Table();
});

function js_Changed_Academic_Year_Selection(jsYearID)
{
	jsSelectedAcademicYearID = jsYearID;
	js_Reload_Record_Table();
}

function trim(str) {
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	
	if(countChecked(obj,'RecordType[]')==0)
	{
    	alert("<?=$eDiscipline['jsWarning']['NothingSelected']?>");
	}
    else
    {
//	    var chkAward = document.getElementById("chkAward");
//	    if(chkAward.checked && jsNumOfAwardsNotYetTransferred == 0)
//	    {
//			alert("<?=$eDiscipline['jsWarning']['AllAwardsTransferrred']?>");
//			return false;
//	    }
//	    
//	    var chkPunishment = document.getElementById("chkPunishment");
//	    if(chkPunishment.checked && jsNumOfPunishmentsNotYetTransferred == 0)
//	    {
//			alert("<?=$eDiscipline['jsWarning']['AllPunishmentsTransferrred']?>");
//			return false;
//	    }
	    
	    if(confirm(globalAlertMsgTransferToWebSAMS))
		{
			obj.action = "step3.php";
			obj.submit();
			return;
		}
	}
}

function jsBack() {
	obj = document.form1;
	
	obj.action = "step1.php";
	obj.submit();
}

function js_Reload_Record_Table()
{
	$('div#RecordTableDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Reload_Step2_Table',
			AcademicYearID: jsSelectedAcademicYearID,
			FileName: '<?=$filename?>',
			RecordTypeStr: '<?=$RecordTypeStr?>'
		},
		function(ReturnData)
		{
			
		}
	);
}

</script>



<form name="form1" method="post" action="">
<br />
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<?= $linterface->GET_STEPS($STEPS_OBJ) ?>
		</td>
	</tr>
	
	<tr><td colspan="2" align="center"><?=$instructionHTML?></td></tr>
	
	<tr><td colspan="2" align="center">&nbsp;</td></tr>
	
	<tr>
		<td>
			<table width="90%" cellpadding="3" cellspacing="0" border="0" align="center">
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$eDiscipline['CurrentSchoolYear']?>
					</td>
					<td class="tabletext">
						<?= getCurrentAcademicYear('') ?>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$eDiscipline["TargetSchoolYear"]?>
					</td>
					<td class="tabletext">
						<?= $selectSchoolYear ?>
					</td>
				</tr>
				
				<tr>
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$eDiscipline['WebSAMS_URL']?>
					</td>
					<td class="tabletext">
						<?= $url ?>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$eDiscipline['NumOfWebSAMSRegNoInCSV']?>
					</td>
					<td class="tabletext">
						<?= $numWebSAMSRegNoInCSV ?>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$eDiscipline['ReportCardPrintIndicate']?>
					</td>
					<td class="tabletext">
						<input type="checkbox" name="chkPrintInd" checked>
					</td>
				</tr>
				
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
						<?=$Lang['eDiscipline']['SLPReadIndicate']?>
					</td>
					<td class="tabletext">
						<input type="checkbox" name="chkSLPReadInd" checked>
					</td>
				</tr>
				
				<tr><td colspan="2">&nbsp;</td></tr>
				
				<tr><td colspan="2">
					<?= $DisciplineRecordsTable ?>
					<div id="RecordTableDiv"></div>
				</td></tr>
				
			</table>
		</td>
	</tr>
	
	<tr>
		<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center"><?=$ButtonHTML?></td>
			</tr>
		</table>
		</td>
	</tr>
</table>

<input type="hidden" name="url" value="<?=$url?>" />
<input type="hidden" name="filename" value="<?=$filename?>" />
<input type="hidden" name="teacher_filename" value="<?=$teacher_filename?>" />
<input type="hidden" name="numWebSAMSRegNoInCSV" value="<?=$numWebSAMSRegNoInCSV?>" />
<input type="hidden" id="AgreedDisclaimer" name="AgreedDisclaimer" value="<?=$AgreedDisclaimer?>" />

<br />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
