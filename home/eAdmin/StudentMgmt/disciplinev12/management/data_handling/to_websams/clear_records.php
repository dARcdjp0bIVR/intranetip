<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$sql = "UPDATE DISCIPLINE_MERIT_RECORD SET DateOfTransferToWebSAMS = NULL, HasTransferredToWebSAMS = '0' WHERE RecordStatus = 1";
$ldiscipline->db_db_query($sql);

intranet_closedb();
?>

	