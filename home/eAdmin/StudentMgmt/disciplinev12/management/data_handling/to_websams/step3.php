<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$AgreedDisclaimer = $_POST['AgreedDisclaimer'];
if ($AgreedDisclaimer != 1) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT($Lang['eDiscipline']['NotYetAgreeDisclaimer'], 'disclaimer.php');
}

# menu highlight setting
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Management_DataHandling_ToWebsams";

$TAGS_OBJ[] = array($Lang['eDiscipline']['RecordsNotTransferred'], "index.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Websams_Transition'], "step1.php", 1);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Steps table
$STEPS_OBJ = $ldiscipline->getWebSAMSTransitionStepsArr($currentStep=3);

# Start layout
$linterface->LAYOUT_START();

#####

# Initialization
$url = $_POST['url'];
$filename = $_POST['filename'];
$AcademicYearID = $_POST['AcademicYearID'];
$chkPrintInd = $_POST['chkPrintInd'];
$chkSLPReadInd = $_POST['chkSLPReadInd'];
$RecordType = $_POST['RecordType'];
$numWebSAMSRegNoInCSV = $_POST['numWebSAMSRegNoInCSV'];

if(empty($RecordType))
	$RecordType = array();
else	
	$RecordTypeStr = implode(",", $RecordType);

# Reset all HasTransferredToWebSAMS to 0
/////$ldiscipline->Reset_All_HasTransferredToWebSAMS($acadermicYear);

# Instruction Row
$instructionHTML = "";
$instructionHTML .= $ldiscipline->showWarningMsg($eDiscipline['Instruction'], $eDiscipline['Websams_Transition_Step3_Instruction2']);
$instructionHTML .= $ldiscipline->showWarningMsg("<font color='red'>".$eComm['Warning']."</font>", $eDiscipline['Websams_Transition_Step3_Instruction1']);

## Build associative array of WebSAMSRegNo and StuID in WebSAMS
$WebSAMS_eClassStuID_MappingArr = $ldiscipline->Get_eClassStuID_WebSAMSRegNo_Mapping($filename, 1);

# Calculate number of records that have to transfer
$NumOfRecordsToBeTransferred = 0;
//$NumOfAwardsNotYetTransferredArr = $ldiscipline->Get_Info_of_AP_Records("Award", DISCIPLINE_STATUS_APPROVED, 0, $AcademicYearID, "COUNT(*)", 102006);
$NumOfAwardsNotYetTransferredArr = $ldiscipline->Get_Info_of_AP_Records("Award", DISCIPLINE_STATUS_APPROVED, 0, $AcademicYearID, "COUNT(*)", $WebSAMS_eClassStuID_MappingArr);
$NumOfAwardsNotYetTransferred = $NumOfAwardsNotYetTransferredArr[0][0];
//$NumOfPunishmentsNotYetTransferredArr = $ldiscipline->Get_Info_of_AP_Records("Punishment", DISCIPLINE_STATUS_APPROVED, 0, $AcademicYearID, "COUNT(*)", 102006);
$NumOfPunishmentsNotYetTransferredArr = $ldiscipline->Get_Info_of_AP_Records("Punishment", DISCIPLINE_STATUS_APPROVED, 0, $AcademicYearID, "COUNT(*)", $WebSAMS_eClassStuID_MappingArr);
$NumOfPunishmentsNotYetTransferred = $NumOfPunishmentsNotYetTransferredArr[0][0];

if (in_array("Award", $RecordType))
{
	$NumOfRecordsToBeTransferred += $NumOfAwardsNotYetTransferred;
}
if (in_array("Punishment", $RecordType))
{
	$NumOfRecordsToBeTransferred += $NumOfPunishmentsNotYetTransferred;
}

## Button Row
$ButtonHTML .= "&nbsp;".$linterface->GET_ACTION_BTN($button_back, "button", "jsBack()", "BackBtn");
$ButtonHTML .= "&nbsp;".$linterface->GET_ACTION_BTN($button_cancel, "button", "jsCancel()", "CancelBtn", "style='display:inline'");

## Build associative array of WebSAMSRegNo and StuID in WebSAMS
$WebSAMS_StuID_MappingArr = $ldiscipline->Get_WebSAMSStuID_WebSAMSRegNo_Mapping($filename);

## Build associative array of TeacherID in WebSAMS and eClass
$WebSAMS_TeacherID_MappingArr = $ldiscipline->Get_TeacherID_eClass_WebSAMS_Mapping($teacher_filename);

?>


<form name="form1" method="post" action="">
<br />
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<?= $linterface->GET_STEPS($STEPS_OBJ) ?>
		</td>
	</tr>
	
	<tr><td align="center"><?=$instructionHTML?></td></tr>
	<tr><td align="center">&nbsp;</td></tr>
	
	<tr>
		<td>
			<table width="90%" cellpadding="3" cellspacing="0" border="0" align="center">
				<tr>
					<td align="center" class="tabletext">
						<div id="process_status" style="display:none">
							<?=$eDiscipline["Transferring"]?>
							<br />
							<div id="processed_num"></div>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
	
	<tr>
		<td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center"><?=$ButtonHTML?></td>
			</tr>
		</table>
		</td>
	</tr>
	
	<tr><td>&nbsp;</td></tr>
	
</table>
<input type="hidden" name="url" value="<?=$url?>" />
<input type="hidden" name="filename" value="<?=$filename?>" />
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>" />
<input type="hidden" name="numWebSAMSRegNoInCSV" value="<?=$numWebSAMSRegNoInCSV?>" />
<input type="hidden" name="RecordTypeStr" value="<?=$RecordTypeStr?>" />
<input type="hidden" name="chkPrintInd" value="<?=$chkPrintInd?>" />
<input type="hidden" name="chkSLPReadInd" value="<?=$chkSLPReadInd?>" />
<input type="hidden" name="WebSAMS_StuID_MappingArr" value="<?=rawurlencode(serialize($WebSAMS_StuID_MappingArr));?>">
<input type="hidden" name="WebSAMS_TeacherID_MappingArr" value="<?=rawurlencode(serialize($WebSAMS_TeacherID_MappingArr));?>">
<input type="hidden" name="WebSAMS_eClassStuID_MappingArr" value="<?=rawurlencode(serialize($WebSAMS_eClassStuID_MappingArr));?>">
<input type="hidden" name="isLast" id="isLast" value="" />
<input type="hidden" name="transferCount" id="transferCount" value="" />
<input type="hidden" id="AgreedDisclaimer" name="AgreedDisclaimer" value="<?=$AgreedDisclaimer?>" />
<br />
</form>

<div id="websams_form_div" style="display:none;"><div>

<!-- JS from WebSAMS Form -->
<script language="JavaScript">
<!--
	var ratingArray = new Array();
	var aRatingBean;

function ANPRatingBean(code,level1,level2,level3,level4,level5,conductMark,prefix,position,awardCat,awardFrom,rptCrdPrtSeq)
{
	this.code = code;
	this.level1 = level1;
	this.level2 = level2;
	this.level3 = level3;
	this.level4 = level4;
	this.level5 = level5;
	this.conductMark = conductMark;
	this.prefix = prefix;
	this.position = position;
	this.awardCat = awardCat;
	this.awardFrom = awardFrom;
	this.rptCrdPrtSeq = rptCrdPrtSeq;
}

function eventChanged()
{
	var fm = document.forms[0];
	if(!fm.descriptionCode || fm.descriptionCode.value=="" || !fm.rdDescription[0].checked)
	{
		return;
	}
	var code = fm.descriptionCode.value;
	aRatingBean = getRatingBean(code);
	if(fm.lvl1)
	{
		fm.lvl1.value = aRatingBean.level1;
	}
	if(fm.lvl2)
	{
		fm.lvl2.value = aRatingBean.level2;
	}
	if(fm.lvl3)
	{
		fm.lvl3.value = aRatingBean.level3;
	}
	if(fm.lvl4)
	{
		fm.lvl4.value = aRatingBean.level4;
	}
	if(fm.lvl5)
	{
		fm.lvl5.value = aRatingBean.level5;
	}
	if(fm.conductMark)
	{
		fm.conductMark.value = aRatingBean.conductMark;
	}
	if(fm.prefix)
	{
		if(aRatingBean.prefix==null)
			document.getElementById('prefix').innerText = '';
		else
			fm.prefix.value = aRatingBean.prefix;
	}
	if(fm.position)
	{
		if(aRatingBean.position==null)
			document.getElementById('position').innerText = '';
		else		
		fm.position.value = aRatingBean.position;
	}
	if(fm.awardCat)
	{
		fm.awardCat.value = aRatingBean.awardCat;
	}
	if(fm.awardFrom)
	{
		fm.awardFrom.value = aRatingBean.awardFrom;
	}
	if(fm.rptCrdPrtSeq)
	{
		fm.rptCrdPrtSeq.value = aRatingBean.rptCrdPrtSeq;
	}				
}

function getRatingBean(code)
{
	var len = ratingArray.length;
	for(var i=0;i<len;i++)
	{
		aRatingBean = ratingArray[i];
		if(aRatingBean.code==code)
		{
			return aRatingBean;
		}
	}
	return new ANPRatingBean(0,0,0,0,0,0,0,null,null,null,null);
}

function initPunishRatingArray()
{

}

function doSave(actionType, formName)
{
	var f = document.getElementById(formName);

	var actionType;
	if(f.rdDescription)
	{
		if(f.rdDescription[0].checked)
		{
			f.descriptionStr.value="";
		}else{
			f.descriptionCode.value="";
		}
	}
	if(f.chkPrintInd.checked)
	{
	   f.printInd.value="true";
	}
	else
	{
	   f.printInd.value="false";
	}
	
	if(f.chkSLPReadInd.checked)
	{
	   f.SLPReadInd.value="true";
	}else
	{
	   f.SLPReadInd.value="false";
	}
	    
	//Set STA search Details		
	
	f.action = "<?=$url?>" + "stu/anp/ANPMaintStuRec.do";
	//f.action = "http://192.168.0.245:52001/home/admin/reportcard2008/management/websams_transition/import_result.php";
	
	f.actionType.value=actionType;
	f.submit();	
}

function doDelete()
{
	if(!confirm("�T�w�R������?"))
	{
		return;
	}
	
	//Set STA search Details		
	
		document.forms[0].action="/stu/anp/ANPMaintStuRec.do";
	
	document.forms[0].actionType.value="Delete";
	//document.forms[0].submit();
}

function actionTakenChanged()
{
    //radiochange(2);
	initRadio();
	if(!document.forms[0].actionTaken)return;

    if(document.forms[0].actionTaken.value=='01')
    {
        setDisable(false);
    }else
    {
        setDisable(true);
    }
		
}

function initRadio(){
	if(document.forms[0].descriptionStr.value != "")
	{
		document.forms[0].descriptionCode.disabled=true;
		document.forms[0].descriptionStr.disabled=false;
        if (document.forms[0].rdDescription)
            document.forms[0].rdDescription[1].checked = true;
        if (document.forms[0].rdDescription)
            document.forms[0].rdDescription[0].checked = false;
    }
	else if(document.forms[0].descriptionCode.value != "")
	{
		document.forms[0].descriptionCode.disabled=false;
		document.forms[0].descriptionStr.disabled=true;
        if (document.forms[0].rdDescription)
            document.forms[0].rdDescription[0].checked = true;
        if (document.forms[0].rdDescription)
            document.forms[0].rdDescription[1].checked = false;
	}
}

function setDisable(blnFlag)
{
	var blnFlag;
    var f = document.forms[0];
    if(!f.fromDate || !f.toDate)
    {
        return;
    }
	document.forms[0].fromDate.disabled = blnFlag;
	document.forms[0].toDate.disabled = blnFlag;
    if(document.forms[0].fromDateCl)
    {
        document.forms[0].fromDateCl.disabled = blnFlag;
    }
    if(document.forms[0].toDateCl)
    {
        document.forms[0].toDateCl.disabled = blnFlag;
    }

    
	document.forms[0].includeInClass.disabled = blnFlag;
    
}

function goBack(url)
{	
	var fm = document.forms[0];
	//Set ANPPunishByAttdForm	
	
		var fm = document.forms[0];
		fm.action = url;
	    fm.anpEditedRecID.value=0;
		fm.actionType.value="Search";
		//fm.submit();
	
}

function MM_callJS(jsStr) { //v2.0
  return eval(jsStr)
}

function showCal(anchor, dest)
{
    var f = document.forms[0];
    var obj = eval("document.forms[0]." + dest);
    if(!obj.disabled)
    {
        cal.showCalendar(anchor, 'document.forms[0].'+ dest + ".value");
    }
}


function radiochange(index, formName)
{
	var thisForm = document.getElementById(formName);

	if(index==1)
	{
		thisForm.descriptionCode.disabled=true;
		thisForm.descriptionStr.disabled=false;
	}
	else
	{
		thisForm.descriptionCode.disabled=false;
		thisForm.descriptionStr.disabled=true;
	}
}

//BEGIN 20060901 UCS-ANP02 Maintain Student Award And Punishment Record (Add Quick Search)
function key(e) {
	return (navigator.appName=="Microsoft Internet Explorer")?window.event.keyCode:e.which;
}

function mapComment(kc){
	if (kc != 13) {
 		return;
	}
	else{
		writeComment();
	}
}

function writeComment() {
	obj = document.getElementById('descriptionCode');
	obj.value = obj.value.toUpperCase();
	var code = trim(obj.value);
	//obj.value = "";

	if( code == "" ){
		if(document.getElementById('codeDesc')!=null)
			document.getElementById('codeDesc').innerText = '';
		return;
	}

	var strEng = "";
	var strChi = "";
	
	switch( code ) {
	
		
		default: obj.focus(); document.getElementById('codeDesc').innerText = '';
						 alert('�N�X�L��');
						 return;
	}
	eventChanged();
	
	
	
		document.getElementById('codeDesc').innerText = strChi;
			
}

function writeCommentNoAlert() {
	obj = document.getElementById('descriptionCode');
	obj.value = obj.value.toUpperCase();
	var code = trim(obj.value);
	//obj.value = "";

	if( code == "" ){
		if(document.getElementById('codeDesc')!=null)
			document.getElementById('codeDesc').innerText = '';
		return;
	}

	var strEng = "";
	var strChi = "";
	
	switch( code ) {
		default: obj.focus(); 
		document.getElementById('codeDesc').innerText = '';
		return;
	}
	
	document.getElementById('codeDesc').innerText = strChi;
	
}

function showTable(){
	
	path = "/stu/anp/ANPCodeTable.do?doWhat=init&ANPEvent=A";
	
	var newwin;
	//newwin = window.open(path, "anpCodeTable_"+"A", "menu,scrollbars=yes,status,resizable=yes, width=630, height=500", true);
	newwin = window.open("", "anpCodeTable_"+"A", "menu,scrollbars=yes,status,resizable=yes, width=630, height=500", true);
	
	//document.forms[1].action = path;
	document.forms[1].target = "anpCodeTable_"+"A"
	//document.forms[1].submit();
	newwin.focus(); 
}

function init(){
	actionTakenChanged();
	writeCommentNoAlert();
	
	initStaffCode();
}

function initStaffCode(){
	//if the user is a staff
	if('Others'=='Staff'){
		staffCode = document.getElementById("staffCode");
		if(staffCode !=null && staffCode.length>0){
			len = staffCode.options.length;
			for(i=0;i<len;i++){
				if(staffCode.options[i].value ==' '){
					staffCode.selectedIndex = i;
					break;
				}
			}
		}
	}
}

//END 20060901
//-->
</script>



<script type="text/javascript">
var jsSubmitCounter = 0;
var jsIntervalID = 0;
var submitWindow;
var processed_num_div = document.getElementById("processed_num");
var process_status_div = document.getElementById("process_status");
var websams_form_div = document.getElementById("websams_form_div");
var jsTimeInterval = 1500;
var jsNumOfRecords = <?=$NumOfRecordsToBeTransferred?>;
var jsSuccessNum = 0;
var jsFailNum = 0;
var BackBtn = document.getElementById("BackBtn");
var CancelBtn = document.getElementById("CancelBtn");


function OpenNewWindow()
{
	// pop up the window on the top-right hand corner
	//var topPos = 20;
	//var pageBody = document.getElementsByTagName("body")[0];
	//var leftPos = pageBody.clientWidth;
	
	//submitWindow = window.open ("step3_popup.php", "submitWindow", "resizable,scrollbars=1,top=" + topPos + ",left=" + leftPos + ",width=560,height=410");
	submitWindow = window.open ("step3_popup.php", "submitWindow", "resizable,scrollbars=1,width=560,height=410");
}

function TriggerFormSubmit()
{
	process_status_div.style.display = "block";
	BackBtn.disabled = true;
	CancelBtn.disabled = true;
	jsIntervalID = setInterval("FormSubmit()", jsTimeInterval);
}

function FormSubmit()
{
	if (jsSubmitCounter < jsNumOfRecords)
	{
		processed_num_div.innerHTML = (jsSubmitCounter+1) + " / " + jsNumOfRecords;
		
		document.getElementById("transferCount").value = jsSubmitCounter + 1;
		
		if (jsSubmitCounter == (jsNumOfRecords - 1))
		{
			document.getElementById("isLast").value = 1;
		}
		
		obj = document.form1;
	    YAHOO.util.Connect.setForm(obj);
		
	    var path = "build_form_aj.php";
	    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback);
		
	}
	else
	{
		clearInterval(jsIntervalID);
		submitWindow.close();
		processed_num_div.style.display = "none";
		process_status_div.innerHTML = "<?=$eDiscipline['DataTransferComplete']?>";
		process_status_div.innerHTML += "<br />";
		process_status_div.innerHTML += "<?=$eDiscipline['RecordsTransferred1']?> " + jsSuccessNum + " <?=$eDiscipline['RecordsTransferred2']?>";
		
		
		
		process_status_div.innerHTML += "<br />";
		
		CancelBtn.value = "<?=$button_finish?>";
		CancelBtn.disabled = false;
		
		BackBtn.style.display = "none";
	}
}

OpenNewWindow();





function jsBack() {
	obj = document.form1;
	
	obj.action = "step2.php";
	obj.submit();
}

function jsCancel() {
	obj = document.form1;
	
	obj.action = "step1.php";
	obj.submit();
}

</script>

<script language="javascript">
<!--
var thisFormName = "websams_form";
var thisForm;

// Start of AJAX 
var callback = {
	success: function ( o )
	{
		if (o.responseText != "invalid")
		{
			websams_form_div.innerHTML = o.responseText;
		
			thisForm = document.getElementById(thisFormName);
			
			//radiochange(1, thisFormName);
			thisForm.target = "submitWindow";
			doSave("Add", thisFormName);
			
			jsSuccessNum++;
		}
		else
		{
			jsFailNum++;
		}
		
	    jsSubmitCounter++;
	    
	}
}

// End of AJAX
//-->
</script>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>