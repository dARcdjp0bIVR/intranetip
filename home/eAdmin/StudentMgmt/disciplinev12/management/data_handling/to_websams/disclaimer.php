<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Management_DataHandling_ToWebsams";

$TAGS_OBJ[] = array($Lang['eDiscipline']['RecordsNotTransferred'], "index.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Websams_Transition'], "disclaimer.php", 1);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Steps table
//$STEPS_OBJ = $ldiscipline->getWebSAMSTransitionStepsArr($currentStep=1);

# Start layout
$linterface->LAYOUT_START();

##########
$DisclaimerTable = '';
$DisclaimerTable .= '<table width="100%" border="0" cellpadding="0" cellspacing="0">'."\n";
	$numOfDisclaimer = count($Lang['eDiscipline']['DisclaimerArr']);
	for ($i=0; $i<$numOfDisclaimer; $i++)
	{
		$thisRow = $i + 1;
		$thisDisplay = $Lang['eDiscipline']['DisclaimerArr'][$i];
		
		$DisclaimerTable .= '<tr>'."\n";
			$DisclaimerTable .= '<td width="10" valign="top">'.$thisRow.'.&nbsp;&nbsp;</td>'."\n";
			$DisclaimerTable .= '<td>'.$thisDisplay.'</td>'."\n";
		$DisclaimerTable .= '</tr>'."\n";
		
		if ($i < $numOfDisclaimer - 1)
			$DisclaimerTable .= '<tr><td>&nbsp;</td></tr>'."\n";
	}
$DisclaimerTable .= '</table>'."\n";
$DisclaimerHTML = $ldiscipline->showWarningMsg("<font color='red'>".$Lang['eDiscipline']['Disclaimer']."</font>", $DisclaimerTable);

### I Agree checkbox
$AgreeCheckbox = $linterface->Get_Checkbox('AgreeChk', 'AgreeChk', $Value=1, $isChecked=0, $Class='', $Lang['eDiscipline']['AgreeToUseDataTransfer'], 'js_Clicked_Agree_Status(this.checked);', $Disabled='');
					
# Button Row
$ButtonHTML = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript:checkForm()", "btnSubmit", '' , 1, '');


# System Message
$SysMsgHTML = "";
if (isset($Result) && $Result!="")
	$SysMsgHTML = $linterface->GET_SYS_MSG("", $eDiscipline["WebSAMS_Warning_".$Result]);
	

?>
<script>
function js_Clicked_Agree_Status(jsChecked)
{
	if (jsChecked) {
		js_Enable_Button();
	}
	else {
		js_Disable_Button();
	}
}

function checkForm()
{
	js_Disable_Button();
	
	$.post(
		"ajax_update.php", 
		{ 
			Action: 'Log_Agreement'
		},
		function(ReturnData)
		{
			if (ReturnData == '1') {
				$('input#AgreedDisclaimer').val(1);
				$('form#form1').attr('action', 'step1.php').submit();
			}
			else {
				js_Enable_Button();
				Get_Return_Message('<?=$Lang['eDiscipline']['FailedToAgreeDisclaimer']?>');
			}
		}
	);
}

function js_Disable_Button()
{
	$('input#btnSubmit').attr("disabled", true).addClass("formbutton_disable").removeClass("formbutton");
}

function js_Enable_Button()
{
	$('input#btnSubmit').attr("disabled", false).removeClass("formbutton_disable").addClass("formbutton");
}
</script>
<form id="form1" name="form1" method="post">
<br />
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<table width="98%" cellpadding="3" cellspacing="0" border="0" align="center">
				<tr>
					<td colspan="2" align="right">
						<?= $SysMsgHTML ?>
					</td>
				</tr>
				
				<tr>
					<td colspan="2">
						<?= $DisclaimerHTML ?>
						<?= $AgreeCheckbox ?>
						<br />
						<br />
					</td>
				</tr>
			</table>
		</td>
	</tr>
	
	<tr>
		<td><table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center"><?=$ButtonHTML?></td>
			</tr>
		</table>
		</td>
	</tr>
</table>
<input type="hidden" id="AgreedDisclaimer" name="AgreedDisclaimer" value="" />
<br />
</form>
<?
$linterface->FOCUS_ON_LOAD("url");
$linterface->LAYOUT_STOP();
intranet_closedb();
?>