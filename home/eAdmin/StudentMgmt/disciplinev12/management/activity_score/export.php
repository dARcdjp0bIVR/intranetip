<?php
// using: 

#############################################
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				create File
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
 
# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");

$ExportArr = array();
$extraCriteria .= ($ClassID=="") ? "" : " AND ycu.YearClassID = $ClassID";
$extraCriteria .= ($searchStr=="")? "" : " AND (iu.EnglishName LIKE '%".trim($searchStr)."%' or iu.ChineseName LIKE '%".trim($searchStr)."%' )";

$conds_year = ($AcademicYearID=="0")? "": " AND (act_bal.AcademicYearID = '$AcademicYearID')";
$conds_semester = ($YearTermID=="0" || $YearTermID=="")? " AND (act_bal.IsAnnual = '1')" : " AND (act_bal.YearTermID = '$YearTermID')";

$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
$baseMark = $ldiscipline->getActivityScoreRule("baseMark");

$sql = "SELECT 
			$clsName as ClassName, 
			ycu.ClassNumber, 
			".getNameFieldByLang("iu.")." as UserName,
			IF(act_bal.SubScore IS NULL, '$baseMark', act_bal.SubScore) as SubScore,
			IFNULL(act_bal.GradeChar, '--') as GradeChar,
			IFNULL(act_bal.DateModified, '--') as DateModified
		FROM 
			INTRANET_USER iu 
			LEFT JOIN DISCIPLINE_STUDENT_ACTSCORE_BALANCE act_bal  ON (iu.UserID = act_bal.StudentID $conds_semester $conds_year)
			INNER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID)
			INNER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE
			 1 $extraCriteria AND iu.RecordType=2 AND iu.RecordStatus IN (1) AND yc.YearClassID != 0 AND yc.AcademicYearID='$AcademicYearID'
		GROUP BY iu.UserID 
		ORDER BY yc.Sequence, ycu.ClassNumber";
$row = $ldiscipline->returnArray($sql, 9);

# Create data array for export
for($i=0; $i<sizeof($row); $i++)
{
	if(sizeof($tmpArr)==0 || $row[$i]['AdjustMark']=="--" || trim($row[$i]['AdjustMark']==""))
	{
		$LastUpdate='--';
	}
	else
	{
		$LastUpdate = $ldiscipline->convertDatetoDays($row[$i]['LastUpdate']);
    }
 	
 	$ExportArr[$i][0] = $row[$i]['ClassName'];		// Class Name
 	$ExportArr[$i][1] = $row[$i]['ClassNumber'];	// Class Number
	$ExportArr[$i][2] = $row[$i]['UserName'];		// Student Name
	$ExportArr[$i][3] = $row[$i]['SubScore'];		// Activity Score
	$ExportArr[$i][4] = $row[$i]['GradeChar'];		// Adjustment
	$ExportArr[$i][5] = $row[$i]['DateModified'];
}

# Define column title
$exportColumn = array($i_ClassName, $i_ClassNumber, $i_general_name, "Activity Score", $i_Discipline_System_Report_ClassReport_Grade, $i_Discipline_System_Discipline_Conduct_Last_Updated);
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

# Output the file to user browser
$filename = "activity_score.csv";
$lexport->EXPORT_FILE($filename, $export_content);

?>