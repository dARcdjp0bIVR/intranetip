<?php 
# using: henry
if ($page_size_change == 1)	# change "num per page"
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
# preserve table view
if ($ck_approval_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_approval_page_number", $pageNo, 0, "", "", 0);
	$ck_approval_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_approval_page_number!="")
{
	$pageNo = $ck_approval_page_number;
}

if ($ck_approval_page_order!=$order && $order!="")
{
	setcookie("ck_approval_page_order", $order, 0, "", "", 0);
	$ck_approval_page_order = $order;
} else if (!isset($order) && $ck_approval_page_order!="")
{
	$order = $ck_approval_page_order;
}

if ($ck_approval_page_field!=$field && $field!="")
{
	setcookie("ck_approval_page_field", $field, 0, "", "", 0);
	$ck_approval_page_field = $field;
} else if (!isset($field) || $ck_approval_page_field == "")
{
	$field = $sortEventDate;
}
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-View");


# prepare year array for generating year selection box
/*
$sql = "SELECT 
				distinct(dscb.AcademicYearID), ".Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN')."
		FROM 
				DISCIPLINE_STUDENT_CONDUCT_BALANCE AS dscb
				INNER JOIN
				ACADEMIC_YEAR AS ay
				ON (dscb.AcademicYearID = ay.AcademicYearID)
		WHERE 
				dscb.AcademicYearID != ''
		ORDER BY ay.Sequence
		";
$tmpYearArr = $ldiscipline->returnArray($sql);

if(is_array($tmpYearArr))
{
	foreach($tmpYearArr as $Key => $ValueArr)
	{
		$thisAcademicYearID = $ValueArr['AcademicYearID'];
		$thisAcademicYearDisplay = Get_Lang_Selection($ValueArr['YearNameB5'], $ValueArr['YearNameEN']);
		$yearArr[]= array($thisAcademicYearID, $thisAcademicYearDisplay);
	}
}
*/
$yearArr = $ldiscipline->returnAllYearsSelectionArray();

if($Year=='')
{
	$Year = Get_Current_Academic_Year_ID();
}
$AcademicYearID = $Year;

# prepare semester array for generating semester selection box
$semester_data = getSemesters($AcademicYearID);
$number = sizeof($semester_data);
$semArr[] = array('',$ec_iPortfolio['whole_year']);

if($number>0) {
	foreach($semester_data as $id=>$name) {
		$semArr[] = array($id, $name);
	}
}

if(!isset($Semester))
{
	$Semester = getCurrentSemesterID();
}

if(!isset($Semester))
{
	$Semester = $semArr[0][0];
}

$YearTermID = $Semester;
if($YearTermID=='' &&$YearTermID!='0') 
	$YearTermID = 0;

	
$yearSelection = $linterface->GET_SELECTION_BOX($yearArr, 'id="Year", name="Year" onChange="document.form1.Semester.value=\'\';document.form1.ClassID.value=\'\';document.form1.submit();"', "", $Year);
$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester" name="Semester" onChange="document.form1.submit()"', "", $Semester);

# set default to the first class
/*
if(empty($ClassID))
{
	$sql = "SELECT yc.YearClassID, yc.ClassTitleEN, yc.ClassTitleB5 FROM YEAR_CLASS yc INNER JOIN YEAR y ON (y.YearID=yc.YearID) WHERE yc.AcademicYearID=$AcademicYearID ORDER BY y.Sequence, yc.Sequence limit 1";
	$result = $ldiscipline->returnArray($sql);
	$ClassID = $result[0]['YearClassID'];
}
$ClassID = $ClassID==-1 ? "" : $ClassID;
*/
$classSelection = $lclass->getSelectClass("name=\"ClassID\" id=\"ClassID\" onChange=\"document.form1.submit()\"", $ClassID, "", $i_general_all_classes, $AcademicYearID);

$toolbar = "<table cellspacing=\"0\" cellpadding=\"0\"><tr>";
$toolbar .="<td>".$linterface->GET_LNK_EXPORT("javascript:exportTo()","","","","",0)."</td>";
$toolbar .="<td>".$linterface->GET_LNK_GENERATE("generate.php","","","","",0)."</td>";
$toolbar.="</tr></table>";
$searchTB = "<input type=\"textbox\" class=\"formtextbox\" id=\"searchStr\" name=\"searchStr\" value=\"$searchStr\">".toolBarSpacer().$linterface->GET_BTN($button_find,'button','document.form1.submit()');

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$YearClassID = $lclass->getClassID($ClassID, $AcademicYearID);
$extraCriteria .= ($ClassID=='') ? "" : " AND ycu.YearClassID=$YearClassID";
$extraCriteria .= ($searchStr=='')? "" : " AND (iu.EnglishName like '%".trim($searchStr)."%' or iu.ChineseName like '%".trim($searchStr)."%' )";

$conds_year = ($AcademicYearID=='0')? "": " AND (conGrade.AcademicYearID=$AcademicYearID)";
$conds_semester = ($YearTermID=='0' || $YearTermID=='')? " AND (conGrade.IsAnnual = 1)" : " AND (conGrade.YearTermID = $YearTermID)";

$name_field = getNameFieldByLang("iu.");
$sql = "SELECT 
			CONCAT(".Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN').",' - ',ycu.ClassNumber) as ClassNameNum,
			$name_field as name,
			IF(GradeChar IS NULL, '--',GradeChar) as GradeChar
		FROM 
			INTRANET_USER iu
			LEFT JOIN DISCIPLINE_STUDENT_CONDUCT_GRADE conGrade ON (iu.UserID = conGrade.StudentID $conds_semester $conds_year)
			LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=iu.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
		WHERE 1 $extraCriteria and iu.RecordType=2 AND iu.RecordStatus IN (0,1,2) AND yc.AcademicYearID=$AcademicYearID
		GROUP BY iu.UserID";

$SettingName = ($Semester == '')? "conduct_grade_gen_".$Year."_Annual": "conduct_grade_gen_".$Year."_$Semester";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("concat(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', 1)), IF(INSTR(ClassNameNum, '-') = 0, NULL, LPAD(TRIM(SUBSTRING_INDEX(ClassNameNum, '-', -1)), 10, '0')))", "name", "GradeChar");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+1;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='1%'class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='5%'>".$li->column($pos++, $i_general_class)."</td>\n";
$li->column_list .= "<td width='10%'>".$li->column($pos++, $i_general_name)."</td>\n";
$li->column_list .= "<td width='10%'>$i_Discipline_System_Report_ClassReport_Grade</td>\n";
//$li->column_list .= "<td width='10%'>$i_Discipline_System_Discipline_Conduct_Last_Updated</td>\n";

# Get last grade generation
$sqlgen = "select DATE_FORMAT(SettingValue,'%Y-%m-%d' ) as LastGen from DISCIPLINE_GENERAL_SETTING
		   where SettingName = '$SettingName' order by SettingValue desc limit 1";
$LastGen = $ldiscipline->returnVector($sqlgen);
if(sizeof($LastGen)==0)
{
	$LastGen[0] = '--';
}

# Top menu highlight setting
$CurrentPage = "Management_UCCKE_ConductGrade";

$TAGS_OBJ[] = array($eDiscipline['Conduct_Grade']);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--

function exportTo() {
	var obj	= document.form1;
	obj.action = "export.php";
	obj.target = "_blank";
	obj.submit();
	obj.action = "";
	obj.target = "_self";
}

-->
</script>
<br />
<form name="form1" method="POST" action="">

			<table width="98%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td colspan="2" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%"></td>
										</tr>
									</table>
								</td>
								<td align="right"><?=$searchTB ?></td>
							</tr>
							<tr><td><?=$yearSelection?><?=$semSelection?><?=$classSelection?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					<div id="div_form"></div>
					</td>
				</tr>
				<tr><td class="tabletextremark">^<?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <?=$LastGen[0]?></td></tr>
				</table><br>
				
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>