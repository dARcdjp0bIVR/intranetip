<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-MGMT-Conduct_Mark-Adjust");

# prepare year array for generating year selection box
/*
$sql = "SELECT 
				distinct(dscb.AcademicYearID), ".Get_Lang_Selection('ay.YearNameB5', 'ay.YearNameEN')."
		FROM 
				DISCIPLINE_STUDENT_CONDUCT_BALANCE AS dscb
				INNER JOIN
				ACADEMIC_YEAR AS ay
				ON (dscb.AcademicYearID = ay.AcademicYearID)
		WHERE 
				dscb.AcademicYearID != ''
		ORDER BY ay.Sequence
		";
$tmpYearArr = $ldiscipline->returnArray($sql);

if(is_array($tmpYearArr))
{
	foreach($tmpYearArr as $Key => $ValueArr)
	{
		$thisAcademicYearID = $ValueArr['AcademicYearID'];
		$thisAcademicYearDisplay = Get_Lang_Selection($ValueArr['YearNameB5'], $ValueArr['YearNameEN']);
		$yearArr[]= array($thisAcademicYearID, $thisAcademicYearDisplay);
	}
}
*/
$yearArr = $ldiscipline->returnAllYearsSelectionArray();


if($Year=='')
{
	$Year = Get_Current_Academic_Year_ID();
}
$AcademicYearID = $Year;
//echo $Year.'/'.$AcademicYearID;


$yearSelection = "<select id=\"Year\", name=\"Year\" onChange=\"changeTerm(this.value);getGenerateDate();getClass()\">";
for($a=0;$a<sizeof($yearArr);$a++)
{
	$default = ($yearArr[$a][0]==$AcademicYearID)?"selected":"";
	$yearSelection .="<option value=\"".$yearArr[$a][0]."\" $default>".$yearArr[$a][1]."</option>";
}
$yearSelection .= "</select>";

$yearID = $Year;

# get semester data from file, prepare semester conversation from number to text. ie: 1 -> First semester
$semester_data = getSemesters($yearID);

$number = sizeof($semester_data);
$semArr[] = array('0',$ec_iPortfolio['whole_year']);
if($number>0)
{	
	foreach($semester_data as $id=>$name)
	{
		$semArr[] = array($id, $name);
	}
}
$semSelection = $linterface->GET_SELECTION_BOX($semArr, 'id="Semester" name="Semester" onChange="getGenerateDate()"', "", $Semester);

$warningContent = $Lang['eDiscipline']['uccke_ConductGrade_Instruction']."<br><span class='tabletextrequire'>$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction3</span>";

# Top menu highlight setting
$CurrentPage = "Management_UCCKE_ConductGrade";

$TAGS_OBJ[] = array($eDiscipline['Conduct_Grade']);

$PAGE_NAVIGATION[] = array($eDiscipline["RecordList"], "index.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Conduct_Mark_Generate_Conduct_Grade);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
var xmlHttp3
var xmlHttp2
var xmlHttp

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "ajax.php";
	url += "?flag=getSemester"
	url += "&year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function getGenerateDate() {
	document.getElementById("LastGenDiv").innerHTML = "";
	document.getElementById("LastGenDiv").style.border = "0px";

		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "ajax.php";
	url += "?flag=getLastGenDate";
	url += "&Year="+document.getElementById('Year').value;
	url += "&Semester="+document.getElementById('Semester').value;
	
	xmlHttp.onreadystatechange = stateChanged 
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("LastGenDiv").innerHTML = xmlHttp.responseText;
		document.getElementById("LastGenDiv").style.border = "0px solid #A5ACB2";
	} 
}

function getClass() {
	document.getElementById("spanClass").innerHTML = "";
	document.getElementById("spanClass").style.border = "0px";

		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "ajax.php";
	url += "?flag=getClass";
	url += "&AcademicYearID="+document.getElementById('Year').value;
	
	xmlHttp2.onreadystatechange = stateChanged2 
	xmlHttp2.open("GET",url,true)
	xmlHttp2.send(null)
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanClass").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanClass").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{

	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function checkForm(form1) {
	if(document.getElementById('targetClass')==undefined || document.getElementById('targetClass').value=='' || document.getElementById('targetClass').value=='#') {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Class?>");
		return false;	
	}
}
</script>
<br />
<form name="form1" method="post" action="generate_update.php" onSubmit="return checkForm(this.form)">

			<table width="98%" border="0" cellspacing="0" cellpadding="1">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
						<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr><td align="center"><?=$ldiscipline->showWarningMsg($i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction1,$warningContent)?></td></tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr>
					<td align="center">
						<table width="80%" border="0" cellspacing="5" cellpadding="5" align="center">
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Profile_Year?><span class="tabletextrequire">*</span></td>
								<td width="80%"><?=$yearSelection?></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Profile_Semester?><span class="tabletextrequire">*</span></td>
								<td width="80%" class="tabletextremark"><span id='spanSemester'><?=$semSelection?></span></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_general_class?><span class="tabletextrequire">*</span></td>
								<td width="80%" class="tabletextremark"><span id='spanClass'><?=$select_class?></span></td>
							</tr>
							<tr valign="top">
								<td>&nbsp;</td>
								<td width="80%" class="tabletextremark"><?=$i_Discipline_System_Discipline_Conduct_Last_Generated?> : <span id="LastGenDiv"><?=$LastGen[0]?></div<</td>
							</tr>
							<tr><td colspan="2"><span class="tabletextremark"><?=$i_general_required_field?></td></tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td height="10px"></td>
				</tr>
				<tr><td colspan="2" align="center">
					<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
					<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
					<? echo $linterface->GET_ACTION_BTN($button_back, "button", "","back"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\" onClick=\"history.back()\""); ?>
				</td></tr>
				</table><br>
</form>
<script language="javascript">
changeTerm(<?=$AcademicYearID?>);
getGenerateDate();
getClass();
</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>