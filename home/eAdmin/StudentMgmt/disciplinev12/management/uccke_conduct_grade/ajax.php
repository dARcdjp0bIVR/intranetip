<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if($flag=='getLastGenDate') {			# get Last Generate Date of conduct grade
	$SettingName = ($Semester == '' || $Semester=='0')? "conduct_grade_gen_".$Year."_Annual": "conduct_grade_gen_".$Year."_$Semester";
	
	# Get last grade generation
	$LastGen = $ldiscipline->getLastConductGradeGenDate($SettingName);
	
	if(sizeof($LastGen)==0)
	{
		$LastGen = '--';
	}
	
	echo "<input type='hidden' name='SettingName' id='SettingName' value='$SettingName'>";
	echo $LastGen;
}
else if($flag=='getSemester') {			# get semester according to the Year
	$yearID = $year;
	$result = getSemesters($yearID);
	
	$temp = "<select name='$field' id='$field' class='formtextbox' onChange='getGenerateDate()'>";
	$temp .= "<option value='0'";
	$temp .= ($year=='0') ? " selected" : "";
	$temp .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
	
	if($year!=0 and $result) {
	
		foreach($result as $termID=>$termName) {
			if($field=='semester1') {
				$selected = ($termID==$term1) ? " selected" : "";
			} else if($field=='semester2') {
				$selected = ($termID==$term2) ? " selected" : "";	
			} else {
				$selected = ($termID==$term) ? " selected" : "";	
			}
			
			$temp .= "<option value='$termID' $selected>$termName</option>";
		}
	}
		
	$temp .= "</select>";
	
	echo $temp;
}
else if($flag=='getClass') {		# get class menu according to the Year
	$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" id=\"targetClass\"", $targetClass, "", $i_general_please_select, $AcademicYearID);
	echo $select_class;
	
}

intranet_closedb();

?>