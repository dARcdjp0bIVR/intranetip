<?php
// Modifying by: 

###########################################################
#
#	Date:	2016-04-15	Bill	[DM#2968]
#			fixed: cannot display stack bar chart, use open-flash-chart-develop.swf for stack bar chart
#
###########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-STAT-Detention-View");

$CurrentPage = "Statistics_Detention";
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['Detention_Statistics'], "", 1);

$linterface = new interface_html();

//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/edis_print_header.php");


$selectBy = $submitSelectBy;
$parPeriodField1 = $PeriodField1;
$parPeriodField2 = $PeriodField2;
$parByFieldArr = explode(",", $ByField);
$parStatsFieldArr = explode("|||", $StatsField);
$itemNameColourMappingArr = unserialize(rawurldecode($itemNameColourMappingArr));
$tmpReasonIDArr = explode("|||", $newItemID);
sort($tmpReasonIDArr);

$tmpDetentionReason = $ldiscipline->getAllDistinctDetentionReasonWithRecordID();
$k = 0;

for($i=0;$i<sizeof($tmpReasonIDArr);$i++) {
	for($j=0;$j<sizeof($tmpDetentionReason);$j++) {
		if($tmpReasonIDArr[$i]==$tmpDetentionReason[$j][1]) {
			$ReasonArr[$k] = intranet_htmlspecialchars($tmpDetentionReason[$j][0]);	
			$k++;
			break;
		}
	}
}

if($ReasonArr) {
	sort($ReasonArr);
	$chartDataArr = $ldiscipline->getDetentionStatisticsAll($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $ReasonArr);
}
//debug_r($chartDataArr);
##############################################################################################################################
############################################### flash chart ##################################################################
##############################################################################################################################
# flash chart

$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');

$chart = new open_flash_chart();

$tooltip = new tooltip();
$tooltip->set_hover();
$chart->set_tooltip($tooltip);

$key = new key_legend();
$key->set_selectable(false);

$maxValue = 0;
$i = 0;

if ($StatType != "REASON") {
	
	$valueArr = array();
	foreach ($parByFieldArr as $byValue) {
		$byValue = stripslashes($byValue);
		$tmpValue = (int)$chartDataArr["$byValue"];
		$valueArr[] = $tmpValue;
		if($display=="horizontal") {
			if ($tmpValue > $maxValue) $maxValue = $tmpValue;
		}
		else {
			$tempMax[$byValue] += $tmpValue;
		}
	}

	if($display=="horizontal")
		$bar = new bar_glass();
	else 
		$bar = new bar_stack_group();
			
	$bar->set_values($valueArr);
	$bar->set_colour("$colourArr[0]");
	$bar->set_tooltip($i_Discipline_No_Of_Detentions.":#val#");
	$bar->set_key($i_Discipline_No_Of_Detentions, "12");
	$chart->add_element($bar);
	
} else if($ReasonArr) {
	
	foreach ($ReasonArr as $statsValue) {	# selected detention reason
		//$statsUnicodeValue = intranet_undo_htmlspecialchars(stripslashes(Big5ToUnicode($statsValue)));
		$statsUnicodeValue = intranet_undo_htmlspecialchars(stripslashes($statsValue));
		$valueArr = array();
		
		if($display=="horizontal")
			$bar = new bar_glass();
		else 
			$bar = new bar_stack_group();
			
		$bar->set_expandable( false ); 
		
		# follow the colour from the index page for printing
		$thisColour = $itemNameColourMappingArr[intranet_undo_htmlspecialchars($statsValue)];
		$bar->set_colour($thisColour);
			
		$j = 0;
		foreach ($parByFieldArr as $byValue) {
			$byValue = stripslashes($byValue);
			$tmpValue = (int)$chartDataArr[$byValue][intranet_undo_htmlspecialchars($statsValue)];
			$valueArr[] = $tmpValue;
			
			if($display=="horizontal") {
				if ($tmpValue > $maxValue) $maxValue = $tmpValue;
			}
			else {
				$tempMax[$byValue] += $tmpValue;
			}
			
			$bar->set_values($valueArr);
		}
	
		$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#");	# x-axis item 
		$bar->set_key("$statsUnicodeValue", "12");
		$bar->set_id( $allDetentionReasonID[$i] ); # ID
		$flag = 0;
		$bar->set_visible( true );
		$chart->add_element($bar);
		$i++;
	}
	
}
if($display=="stack") {
	foreach($tempMax as $val) {
		if ($val > $maxValue) $maxValue = $val;
	}
}


if ($Period == "YEAR") {
	$titleString = $ldiscipline->getAcademicYearNameByYearID($selectYear)." ";
	$titleString .= ($selectSemester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($selectSemester)." ";
} else {
	$titleString = $i_From." ".$textFromDate." $i_To ".$textToDate." ";
}

$titleString .= $eDiscipline['Detention_Statistics'];
$title = new title($titleString);
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

if ($selectBy=="FORM") {
	$x_legend = new x_legend($i_Discipline_Form);
} else if ($selectBy=="CLASS") {
	$x_legend = new x_legend($i_Discipline_Class);
}
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$parByFieldArr = explode(',',stripslashes(implode(',',$parByFieldArr)));	# remove the slashes in the forms / classes
$x->set_labels_from_array($parByFieldArr);

$y_legend = new y_legend($i_Discipline_Quantity);
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );

// $maxY = 5, 10, 20, 30...
if ($maxValue <= 5) {
	$maxY = 5;
} else {
	if (substr($maxValue, strlen($maxValue)-1, 1) == "0") {		// Last digit = 0
		$maxY = $maxValue;
	} else {
		$maxY = (substr($maxValue, 0, strlen($maxValue)-1) + 1)."0";
	}
}

$y->set_range( 0, $maxY, $maxY / 5 );
$y->set_offset(false);

$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->set_key_legend( $key );

##############################################################################################################################
############################################ END - flash chart ###############################################################
##############################################################################################################################
$height = 300 + (sizeof($parStatsFieldArr)/2) * 20;
//$height = 350;
$fixedWidthForAcceptableItem = 5;
$maxWidth = 5000;
if ($StatType != "REASON") {
	$width = 850 + (sizeof($parByFieldArr)) * 5;
} else {	# detention reason
	$width = 850 + ((sizeof($parByFieldArr)+sizeof($ReasonArr))) * 5;
}
$width = ($width>$maxWidth) ? $maxWidth : $width;

	$chartContent = "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"result_box\">";
	$chartContent .= "<tr><td align=\"center\">";

	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################### flash chart ##################################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n"; 
	$chartContent .= "<div id=\"my_chart\">no flash?</div>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/json/json2.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/swfobject.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	
	if($display=="horizontal") {
		$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart.swf\", \"my_chart\", \"".$width."\", \"".$height."\", \"9.0.0\", \"expressInstall.swf\");\n";
	}
	// [DM#2968] use open-flash-chart-develop.swf as open-flash-chart.swf not work for stack bar chart
	else {
		$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart-develop.swf\", \"my_chart\", \"".$width."\", \"".($height+150)."\", \"9.0.0\", \"expressInstall.swf\");\n";
	}
	
	$chartContent .= "</script>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	$chartContent .= "function ofc_ready(){\n";
	$chartContent .= "	//alert('ofc_ready');\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function open_flash_chart_data(){\n";
	$chartContent .= "	//alert( 'reading data' );\n";
	$chartContent .= "	return JSON.stringify(data);\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function findSWF(movieName) {\n";
	$chartContent .= "  if (navigator.appName.indexOf(\"Microsoft\")!= -1) {\n";
	$chartContent .= "	return window[movieName];\n";
	$chartContent .= "  } else {\n";
	$chartContent .= "	return document[movieName];\n";
	$chartContent .= "  }\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "var data = ".$chart->toPrettyString().";\n";
	$chartContent .= "</script>\n";
	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################ END - flash chart ###############################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n";

	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";

?>

<table width="100%" align="center" class="print_hide">
	<tr>
		<td align="right">
			<input name="btn_print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">
		</td>
	</tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<?=$chartContent?>
			<br />
		</td>
	</tr>
</table>
<?
intranet_closedb();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>
