<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Temp Assign memory of this page
ini_set("memory_limit", "150M");

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$filename = "goodconduct_misconduct_stats.csv";
$categoryID = ($record_type==1) ? $selectGoodCat : $selectMisCat;

//debug_pr($_POST);
$selectBy = $submitSelectBy;
$parPeriodField1 = $PeriodField1;
$parPeriodField2 = $PeriodField2;
$parByFieldArr = explode(",", intranet_undo_htmlspecialchars($ByField));
$parStatsFieldArr = explode(",", $StatsField);
$parStatsFieldTextArr = explode(",", str_replace("'","&#039;",(stripslashes($StatsFieldText))));
$newItemID = explode(",",$newItemID);


// Data array for export
$chartDataArr = $ldiscipline->getGoodConductMisconductStatisticsAll($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $selectShowOnlyType, $selectShowOnlyNum, $newItemID, $categoryID);

$ExportArr = array();
$i = 0;
$ExportArr[$i][0] = $iDiscipline["Period"].":";
if ($Period == "YEAR") {
	$ExportArr[$i][1] = $ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." (";
	$ExportArr[$i][1] .= ($parPeriodField2==0) ? $i_Discipline_System_Award_Punishment_Whole_Year.")" : $ldiscipline->getTermNameByTermID($parPeriodField2).")";
} else {
	$ExportArr[$i][1] = $i_From." ".$parPeriodField1." ".$i_To." ".$parPeriodField2;
}
$i++;
$ExportArr[$i][0] = $eDiscipline["Type"].":";
if ($StatType == "GM_TITLES") {
	$ExportArr[$i][1] = $i_Discipline_GoodConductMisconduct_Titles;
} else {
	$ExportArr[$i][1] = $i_Discipline_No_Of_GoodConductsMisconducts;
}
$i++;
$ExportArr[$i][0] = "";		// a blank line
$i++;
if ($selectBy == "FORM") {
	$ExportArr[$i][0] = $i_Discipline_Form;
} else if ($selectBy == "CLASS") {
	$ExportArr[$i][0] = $i_Discipline_Class;
}

$ExportArr[$i][1] = $i_Discipline_GoodConductMisconduct_Titles;
$i++;
$ExportArr[$i][0] = "";


$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE ItemID IN (".implode(",",$newItemID).")";
$all_item_name = $ldiscipline->returnVector($sql);
/*
if($categoryID==1) {
	$all_item_name = $parStatsFieldTextArr;
} else if($categoryID==2) {
	
	$sql2 = "SELECT ".Get_Lang_Selection('CH_DES','EN_DES')." as SubjectName FROM ASSESSMENT_SUBJECT WHERE RecordID IN (".implode(",", $newItemID).")";
	$temp = $ldiscipline->returnVector($sql2);
	$all_item_name = $temp;	
	*/
if($StatType=="GM_TITLES")	 {
	$all_item_name = $parStatsFieldTextArr;
} else if($StatType=="NO_OF_GM") {
	for($i=0;$i<sizeof($newItemID);$i++) {
		if($newItemID[$i]==0) {
			$all_item_name[$i] = $i_Discipline_Misconduct;
		} else {
			$all_item_name[$i] = $i_Discipline_GoodConduct;
		}
	}
}

for ($j=0; $j<sizeof($all_item_name); $j++) {
	//$ExportArr[$i][$j+1] = stripslashes($all_item_name[$j]);
	$ExportArr[$i][$j+1] = (($all_item_name[$j]));
} 

$i++;
if($radioType=="GM_TITLES") {
	
	foreach ($parByFieldArr as $byValue) {	# selected class/form
		$byValue = stripslashes($byValue);
		$k = 0;
		$ExportArr[$i][$k] = $byValue;
		$k++;
	//	foreach ($parStatsFieldTextArr as $statsValue) 
		foreach ($all_item_name as $statsValue) 	# selected item
		{
	//		$ExportArr[$i][$k] = $chartDataArr[$byValue][stripslashes($statsValue)];
	//		$ExportArr[$i][$k] = (isset($chartDataArr[$byValue][stripslashes($statsValue)])) ? $chartDataArr[$byValue][stripslashes($statsValue)] : 0;
			$ExportArr[$i][$k] = (isset($chartDataArr[$byValue][($statsValue)])) ? $chartDataArr[$byValue][($statsValue)] : 0;
			$k++;
		}
		$i++;
	}
} else {
	
	foreach ($parByFieldArr as $byValue) {	# selected class/form
		$byValue = stripslashes($byValue);
		$k = 0;
		$ExportArr[$i][$k] = $byValue;
		$k++;
		//foreach ($parStatsFieldTextArr as $statsValue) 
		foreach ($all_item_name as $statsValue) 
		{
	//		$ExportArr[$i][$k] = $chartDataArr[$byValue][stripslashes($statsValue)];
			$ExportArr[$i][$k] = (isset($chartDataArr[$byValue][stripslashes($statsValue)])) ? $chartDataArr[$byValue][stripslashes($statsValue)] : 0;
	
			$k++;
		}
		$i++;
	}
}

if($categoryID==1 || $categoryID==2) {
	$numOfColumn = sizeof($parStatsFieldTextArr) + 2;
} else {
	$numOfColumn = sizeof($all_item_name) + 2;
}

$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "", "\r\n", "", $numOfColumn, "11");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
