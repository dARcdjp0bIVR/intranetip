<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/edis_print_header.php");
/*
if($radioType=="GM_TITLES") {
//	$all_item = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID,'id');
	$all_item = explode(",", $newItemID);
}

if($categoryID==1 || $categoryID==2) {
	$sql2 = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID=1";
	$result = $ldiscipline->returnVector($sql2);
	$all_item_name[0] = $result[0];
} else {
	$all_item_name = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID, 'name');
}
*/
/*
if($selectClass) {
	$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID IN (".implode(',',$selectClass).")";
	$temp = $ldiscipline->returnVector($sql);
	
	for($i=0;$i<sizeof($temp);$i++) {
		$className .= ($className!="") ? "','".$temp[$i] : $temp[$i];
	}
	$className = "'".$className."'";
}
*/

$radioPeriod = $radioPeriod;	# Year/Date
$selectBy = $submitSelectBy;
$parPeriodField1 = $PeriodField1;
$parPeriodField2 = $PeriodField2;
$parByFieldArr = explode(",", intranet_undo_htmlspecialchars($ByField));
$parStatsFieldArr = explode(",", $StatsField);
$parStatsFieldTextArr = explode(",", stripslashes($StatsFieldText));

$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";


if ($submitSelectBy == "FORM") {
	$sql = "SELECT y.YearName, ";
} else {
	$sql = "SELECT $clsName, ";
}

$sql .= " COUNT(AR.StudentID) AS StudentCount,";

$sql .= " ACC.Name";

$sql .= " FROM DISCIPLINE_ACCU_RECORD AR";

$sql .= " LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM ACI ON (AR.ItemID=ACI.ItemID)
		  LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY ACC ON (ACC.CategoryID=AR.CategoryID)
		  INNER JOIN INTRANET_USER USR ON (AR.StudentID=USR.UserID)";

$sql .= " LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID)
			LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
			LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)";		  

$sql .= " WHERE AR.RecordType=";
			
$sql .= ($id==0) ? "-1" : "1";

$sql .= " AND AR.RecordStatus=1 AND AR.DateInput IS NOT NULL";

if ($submitSelectBy == "FORM") {
	$sql .= " AND y.YearID IN ($selectedTarget)";
} else {
	$sql .= " AND yc.YearClassID IN ($selectedTarget)";
}
$sql .= " AND yc.AcademicYearID=$selectYear";

if ($Period == "YEAR") {
	if ($parPeriodField1 != $i_Discipline_System_Award_Punishment_All_School_Year) {
		$sql .= " AND AR.AcademicYearID=$selectYear";
	}
	if ($parPeriodField2 != $i_Discipline_System_Award_Punishment_Whole_Year && $parPeriodField2!=0) {
		$sql .= " AND AR.YearTermID = '$selectSemester'";
	}
} else {
	$sql .= " AND UNIX_TIMESTAMP(AR.RecordDate) BETWEEN UNIX_TIMESTAMP('$textFromDate') AND UNIX_TIMESTAMP('$textToDate')";
}

if ($submitSelectBy == "FORM") {
	$sql .= " GROUP BY y.YearID, AR.ItemID";
} else {
	$sql .= " GROUP BY $clsName, AR.ItemID";
}
$sql .= " ORDER BY ACI.Name";



//echo $sql;
$tmpResult = $ldiscipline->returnArray($sql);	

for ($i=0; $i<sizeof($tmpResult); $i++) {
	list($TmpBy, $TmpStudentCount, $RecordType) = $tmpResult[$i];
	$chartDataArr[$TmpBy][$RecordType] += $TmpStudentCount;
}
if($id==0) {
	$currentMeritType = $i_Discipline_Misconduct;
	$meritType = -1;
} else {
	$currentMeritType = $i_Discipline_GoodConduct;
	$meritType = 1;
}

//$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE MeritType=$meritType AND RecordStatus = 1";
$conds = ($meritType==1) ? " MeritType=1" : " MeritType=-1 OR MeritType IS NULL";
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE $conds";
$result = $ldiscipline->returnVector($sql);

sort($result);

##############################################################################################################################
############################################### flash chart ##################################################################
##############################################################################################################################
# flash chart

$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');

$chart = new open_flash_chart();

$tooltip = new tooltip();
$tooltip->set_hover();
$chart->set_tooltip($tooltip);

$key = new key_legend();
$key->set_selectable(false);

$maxValue = 0;
$i = 0;

//debug_r($chartDataArr);
//foreach ($parStatsFieldTextArr as $statsValue) 
foreach ($result as $statsValue) 	# merit/demerit Item
{
	$statsValue = addslashes($statsValue);
//	if($statsValue==$currentMeritType) {
		//$bar = new bar_stack();
		$bar = new bar_glass();
		$statsUnicodeValue = intranet_undo_htmlspecialchars(stripslashes($statsValue));
		$valueArr = array();
	
		for($j=0;$j<sizeof($valueArr);$j++) {
			$valueArr[$j] = "";
		}
	
		$j = 0;
		foreach ($parByFieldArr as $byValue) {		# selected class/form
			$byValue = stripslashes($byValue);
			$tmpValue = (int)$chartDataArr[$byValue][stripslashes($statsValue)];
			
			//echo $byValue."/".stripslashes($statsValue)."/".$tmpValue."<br>";
			
			$valueArr[] = $tmpValue;
			if ($tmpValue > $maxValue) $maxValue = $tmpValue;	
			//$bar->append_stack( $valueArr );
			$bar->set_values( $valueArr);
			//$j++;
		}
	
	//	$bar = new bar();
//		$bar->set_oneColour( true );
		$bar->set_expandable( false );
	//	$bar->set_values($valueArr);
		$bar->set_colour("$colourArr[$i]");
		$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#");
	//	$bar->set_id( $i );
	//	$bar->set_visible( true );
		$bar->set_id( $all_item[$i] );
		$bar->set_visible( true );
		$bar->set_key("$statsUnicodeValue", "12");
		$chart->add_element($bar);
		$i++;
//	}
}


if ($Period == "YEAR") {
	$titleString = $selectYear." ";
	$titleString .= ($selectSemester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year." " : $selectSemester." ";
} else {
	$titleString = $i_From." ".$textFromDate." ".$i_To." ".$textToDate." ";
}
$titleString .= $eDiscipline['Good_Conduct_and_Misconduct_Statistics'];
$title = new title($titleString);
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

if ($selectBy=="FORM") {
	$x_legend = new x_legend($i_Discipline_Form);
} else if ($selectBy=="CLASS") {
	$x_legend = new x_legend($i_Discipline_Class);
}
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$parByFieldArr = explode(',',stripslashes(implode(',',$parByFieldArr)));	# remove the slashes in the forms / classes
$x->set_labels_from_array($parByFieldArr);

$y_legend = new y_legend($i_Discipline_Quantity);
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );

// $maxY = 5, 10, 20, 30...
if ($maxValue <= 5) {
	$maxY = 5;
} else {
	if (substr($maxValue, strlen($maxValue)-1, 1) == "0") {		// Last digit = 0
		$maxY = $maxValue;
	} else {
		$maxY = (substr($maxValue, 0, strlen($maxValue)-1) + 1)."0";
	}
}

$y->set_range( 0, $maxY, $maxY / 5 );
$y->set_offset(false);

$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->set_key_legend( $key );

##############################################################################################################################
############################################ END - flash chart ###############################################################
##############################################################################################################################

# Adjust the width of flash depends on the no. of class/form
$fixedWidthForAcceptableItem = 5;
$height = 550 + ((sizeof($result)/3)) * 10;
$width = 850 + ((sizeof($parByFieldArr)+sizeof($result))-$fixedWidthForAcceptableItem) * 5;	


	$chartContent = "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"result_box\">";
	$chartContent .= "<tr><td align=\"center\">";

	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################### flash chart ##################################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n";
	$chartContent .= "<div id=\"my_chart\">no flash?</div>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/json/json2.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/swfobject.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart.swf\", \"my_chart\", \"$width\", \"$height\", \"9.0.0\", \"expressInstall.swf\");\n";
	$chartContent .= "</script>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	$chartContent .= "function ofc_ready(){\n";
	$chartContent .= "	//alert('ofc_ready');\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function open_flash_chart_data(){\n";
	$chartContent .= "	//alert( 'reading data' );\n";
	$chartContent .= "	return JSON.stringify(data);\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function findSWF(movieName) {\n";
	$chartContent .= "  if (navigator.appName.indexOf(\"Microsoft\")!= -1) {\n";
	$chartContent .= "	return window[movieName];\n";
	$chartContent .= "  } else {\n";
	$chartContent .= "	return document[movieName];\n";
	$chartContent .= "  }\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "var data = ".$chart->toPrettyString().";\n";
	$chartContent .= "</script>\n";
	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################ END - flash chart ###############################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n";

	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";
?>

<table width="100%" align="center">
	<tr>
		<td align="right">
			<input name="btn_print" type="button" class="printbutton" value="<?=$button_print?>" onClick="window.print(); return false;" onMouseOver="this.className='printbuttonon'" onMouseOut="this.className='printbutton'">
		</td>
	</tr>
</table>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<?=$chartContent?>
			<br />
		</td>
	</tr>
</table>
<?
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>
