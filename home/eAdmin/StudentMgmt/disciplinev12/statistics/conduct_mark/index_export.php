<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Temp Assign memory of this page
ini_set("memory_limit", "150M");

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-STAT-Conduct_Mark-View");

$filename = "conduct_mark_stats.csv";

$selectBy = $submitSelectBy;
$parPeriodField1 = $PeriodField1;
$parPeriodField2 = $PeriodField2;
$parByFieldArr = explode(",", intranet_undo_htmlspecialchars($ByField));

$gradeArr = $ldiscipline->getAllGrade($newItemID);
for($i=0; $i<sizeof($gradeArr); $i++) {
	list($t_score, $t_grade) = $gradeArr[$i];
	$gradeStatsArr[] = $t_grade;
}

// Data array for export
$chartDataArr = $ldiscipline->getConductMarkStatistics($parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr);

$ExportArr = array();
$i = 0;
$ExportArr[$i][0] = $iDiscipline["Period"].":";
$ExportArr[$i][1] = $ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." (";
$ExportArr[$i][1] .= ($parPeriodField2==0) ? $i_Discipline_System_Award_Punishment_Whole_Year.")" : $ldiscipline->getTermNameByTermID($parPeriodField2).")";
$i++;
$ExportArr[$i][0] = $eDiscipline["Type"].":";
$ExportArr[$i][1] = $i_Discipline_Conduct_Grade;
$i++;
$ExportArr[$i][0] = "";		// a blank line
$i++;
if ($selectBy == "FORM") {
	$ExportArr[$i][0] = $i_Discipline_Form;
} else if ($selectBy == "CLASS") {
	$ExportArr[$i][0] = $i_Discipline_Class;
}
$ExportArr[$i][1] = $i_Discipline_Conduct_Grade;
$i++;
$ExportArr[$i][0] = "";
for ($j=0; $j<sizeof($gradeStatsArr); $j++) {
	$ExportArr[$i][$j+1] = $gradeStatsArr[$j];
}
$i++;

	foreach ($parByFieldArr as $byValue) {
		$byValue = stripslashes($byValue);
		$k = 0;
		$ExportArr[$i][$k] = $byValue;
		$k++;
		foreach ($gradeStatsArr as $statsValue) {
	//		$ExportArr[$i][$k] = $chartDataArr[$byValue][stripslashes($statsValue)];
			$ExportArr[$i][$k] = $chartDataArr[$byValue][($statsValue)];
			$k++;
		}
		$i++;
	}


$numOfColumn = sizeof($gradeStatsArr) + 1;
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "", "\r\n", "", $numOfColumn, "11");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
