<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// Temp Assign memory of this page
ini_set("memory_limit", "150M");

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$ldiscipline->CONTROL_ACCESS("Discipline-STAT-Award_Punishment-View");

$filename = "award_punish_stats.csv";

$selectBy = $submitSelectBy;
$parPeriodField1 = $PeriodField1;
$parPeriodField2 = $PeriodField2;
$parByFieldArr = explode(",", $ByField);
$parStatsFieldArr = explode(",", $StatsField);
$parStatsFieldTextArr = explode(",", stripslashes($StatsFieldText));

$newItemID = explode(",", $newItemID);

// Data array for export
//$chartDataArr = $ldiscipline->getAwardPunishStatistics($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $selectShowOnlyType, $selectShowOnlyNum);
$chartDataArr = $ldiscipline->getAwardPunishStatisticsAll($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $selectShowOnlyType, $selectShowOnlyNum, $newItemID);

$ExportArr = array();
$i = 0;
$ExportArr[$i][0] = $iDiscipline["Period"].":";
if ($Period == "YEAR") {
	$ExportArr[$i][1] = $parPeriodField1." (".$parPeriodField2.")";
} else {
	$ExportArr[$i][1] = $i_From." ".$parPeriodField1." ".$i_To." ".$parPeriodField2;
}
$i++;
$ExportArr[$i][0] = $eDiscipline["Type"].":";
if ($StatType == "NO_OF_MD") {
	$ExportArr[$i][1] = $i_Discipline_No_Of_Merits_Demerits;
} else {
	$ExportArr[$i][1] = $i_Discipline_Award_Punishment_Titles;
}
$i++;
$ExportArr[$i][0] = "";		// a blank line
$i++;
if ($selectBy == "FORM") {
	$ExportArr[$i][0] = $i_Discipline_Form;
} else if ($selectBy == "CLASS") {
	$ExportArr[$i][0] = $i_Discipline_Class;
}
//if ($StatType == "REASON") {
if ($StatType == "NO_OF_MD") {
	$ExportArr[$i][1] = $i_Discipline_No_Of_Merits_Demerits;
} else {
	$ExportArr[$i][1] = $i_Discipline_Award_Punishment_Titles;
}
	$i++;
	$ExportArr[$i][0] = "";
	
	$sql = "SELECT CONCAT(ItemCode,' - ',ItemName) FROM DISCIPLINE_MERIT_ITEM WHERE ItemID IN (".implode(",",$newItemID).")";
	$all_item_name = $ldiscipline->returnVector($sql);
	
	for ($j=0; $j<sizeof($all_item_name); $j++) {
//		$ExportArr[$i][$j+1] = stripslashes($parStatsFieldTextArr[$j]);
		$ExportArr[$i][$j+1] = stripslashes($all_item_name[$j]);
	}
//} else {
//	$ExportArr[$i][1] = $i_Discipline_Quantity;
//}
$i++;
//if ($StatType != "REASON") {
//	foreach ($parByFieldArr as $byValue) {
//		$k = 0;
//		$ExportArr[$i][$k] = $byValue;
//		$k++;
//		$ExportArr[$i][$k] = $chartDataArr["$byValue"];
//		$k++;
//		$i++;
//	}
//} else {

	if ($StatType == "NO_OF_MD") {
		
		$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
		$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
		$MeritDemeritArr = array_merge($merit_record_type, $demerit_record_type);
			
		$j = 0;
		$selectedMeritDemerit = array();
		for($m=0;$m<sizeof($MeritDemeritArr);$m++) {
			for($k=0;$k<sizeof($newItemID);$k++) {
				if($MeritDemeritArr[$m][0]==$newItemID[$k]) {
					$selectedMeritDemerit[$j] = $MeritDemeritArr[$m][1];
					$j++;
				}
			}
		}
		
		for ($j=0; $j<sizeof($selectedMeritDemerit); $j++) {
			$ExportArr[$i][$j+1] = stripslashes($selectedMeritDemerit[$j]);
		}
		
	}
	$i++;

	foreach ($parByFieldArr as $byValue) {	# selected class/form
		$k = 0;
		$ExportArr[$i][$k] = $byValue;
		$k++;
		if ($StatType == "NO_OF_MD") {
			foreach ($selectedMeritDemerit as $statsValue) {
	//			$ExportArr[$i][$k] = $chartDataArr[$byValue][stripslashes($statsValue)];
				$ExportArr[$i][$k] = (isset($chartDataArr[$byValue][stripslashes($statsValue)])) ? $chartDataArr[$byValue][stripslashes($statsValue)] : 0;
				$k++;
			}
		} else {
			foreach ($all_item_name as $statsValue) {	# selected item name
	//			$ExportArr[$i][$k] = $chartDataArr[$byValue][stripslashes($statsValue)];
				$ExportArr[$i][$k] = (isset($chartDataArr[$byValue][stripslashes($statsValue)])) ? $chartDataArr[$byValue][stripslashes($statsValue)] : 0;
				$k++;
			}
		}
		$i++;
		
	}
//}

//debug_r($ExportArr);
//$numOfColumn = sizeof($parStatsFieldArr) + 1;
if($StatType=="NO_OF_MD") {
	$numOfColumn = sizeof($selectedMeritDemerit) + 1;
} else {
	$numOfColumn = sizeof($all_item_name) + 1;
}
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "", "\r\n", "", $numOfColumn, "11");

intranet_closedb();

// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);

?>
