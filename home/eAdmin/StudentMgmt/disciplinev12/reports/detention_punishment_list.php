<?php
// Editing by 
/* 
 * Change Log
 * 
  * 2018-02-07 (Isaac): added $NewPagePerClass; 
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

// General deploy this report [2015-0128-1121-06170]
//if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['DetentionPunishmentList']) {
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

$StartDate = $_REQUEST['StartDate'];
$EndDate = $_REQUEST['EndDate'];
$TargetType = $_REQUEST['TargetType'];
$TargetIdAry = $_REQUEST['TargetID'];
$AttendanceStatusAry = $_REQUEST['AttendanceStatus'];
$Times = $_REQUEST['Times'];
$Format = strtoupper($_REQUEST['Format']);
$PrintNewPagePerClass = $_REQUEST['SeperateByClass'];

if($Format == 'CSV'){
	$ldiscipline_ui->getDetentionPunishmentListTable($StartDate, $EndDate, $TargetType, $TargetIdAry, $AttendanceStatusAry, $Times, $Format);
}else{
?>	

<style type='text/css'>

html, body { margin: 0px; padding: 0px; } 


@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
}

.print_table {
	border-collapse: collapse;
	margin: 0;
	border-spacing: 0;
}

.print_table th {
    border: 1px solid #000000;
    text-align: left;
    padding: 5px 3px;
    vertical-align: top;
}

.print_table td {
    border: 1px solid #000000;
    text-align: left;
    padding: 5px 3px;
    vertical-align: top;
}

@media print
{
  table { page-break-after:auto;  }
  tr    { page-break-inside:avoid; page-break-after:auto; }
  td    { page-break-inside:avoid; page-break-after:auto; }
  thead { display:table-header-group }
}

</style>

<?php	
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
// 	$sys_custom['eDiscipline']['DetentionPunishmentList']? $NewPagePerClass = true : $NewPagePerClass = false;
	echo $ldiscipline_ui->getDetentionPunishmentListTable($StartDate, $EndDate, $TargetType, $TargetIdAry, $AttendanceStatusAry, $Times, $Format, $PrintNewPagePerClass);
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}
intranet_closedb();
?>