<?php
// modifying by: 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-MonthlyReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Class #
$select_class = $ldiscipline->getSelectClass("name=\"targetClass\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, false);


function RepQuotes($strx) {
	$patterns = array("/\"/", "/&quot;/");
	$replacements = "/&quot;&quot;/";
	return preg_replace($patterns, $replacements, $strx);
}

# menu highlight setting
$TAGS_OBJ[] = array($eDiscipline['UniformRecords'], "");
$CurrentPage = "UniformRecords";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


echo '<form name="form1" method="post" action="">';

// Get Student's Information
$name = $lclass ->getStudentNameByStudentID($studentID);
$classNo = $lclass ->getClassNoByStudentID($studentID);
$class = $lclass ->getClassByStudentID($studentID);
$gender = $lclass ->getGenderByStudentID($studentID);

$sql = "SELECT PhotoLink FROM INTRANET_USER WHERE UserID=$studentID";
$photo = $lclass->returnVector($sql);
echo '<img src="'.$photo[0].'" width=100 height=130 border=1><br>';
// Display Student's Information
echo '<br>';
echo '<table border=0>';
echo '<tr>';
echo '<td>Student ID</td>';
echo '<td align=right> : </td>';
echo '<td align=left>'.$studentID.'</td>';
echo '</tr>';
echo '<tr>';
echo '<td>Name</td>';
echo '<td align=right> : </td>';
echo '<td align=left>'.$name[0][0].'('.$name[0][1].')</td>';
echo '</tr>';
echo '<tr>';
echo '<td>Class</td>';
echo '<td align=right> : </td>';
echo '<td align=left>'.$class[0].'('.$classNo[0].')</td>';
echo '</tr>';
echo '<tr>';
echo '<td>Gender</td>';
echo '<td align=right> : </td>';
echo '<td align=left>'.$gender[0].'</td>';
echo '</tr>';
echo '</table>';

echo '<input type="hidden" name="content" value="'.urlencode($detail_table).'" />';
echo '<input type="hidden" name="exportContent" value="'.$export_content.'" />';
echo "<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>".$detail_table."</td></tr></table>";
echo "<br>";
echo $linterface->GET_ACTION_BTN("Back", "button", "history.go(-1);return true;");
echo '&nbsp;&nbsp;';
echo $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint()");
echo '&nbsp;&nbsp;';
echo $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()");
echo '</form>';
?>
<?php
  $linterface->LAYOUT_STOP();
  intranet_closedb();
?>
