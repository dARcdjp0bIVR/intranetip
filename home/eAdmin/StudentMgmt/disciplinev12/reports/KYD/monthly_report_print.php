<?php
// modifying by: YAT

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_kyd.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12_kyd();
$lclass = new libclass();

$thisClassNameEn = $lclass->getClassName($targetClass);
$thisClassName = $lclass->getClassNameByLang($targetClass);

$sql = "select 
			a.UserID,
        	CategoryID,
			count(b.CategoryID)
		from 
			INTRANET_USER as a
			left join DISCIPLINE_ACCU_RECORD as b on (b.StudentID=a.UserID)
		where
			a.ClassName = '$thisClassNameEn'
			and b.RecordDate >='$Section2Fr' and b.RecordDate <='$Section2To'
			and CategoryID<=4
    	group by 
    		a.UserID, b.CategoryID
";
$result = $ldiscipline->returnArray($sql);

##### Build data array
$data = array();
if(!empty($result))
{
	foreach($result as $k=>$d)
	{
		list($thisUserID, $thisCategoryID, $thisCount) = $d;
		$data[$thisUserID][$thisCategoryID] = $thisCount;
	}
}

$student_ary = $lclass->getStudentNameListByClassName($thisClassName);
$studentIDAry = Get_Array_By_Key($student_ary, 'UserID');
$absentData = retrieveAttendanceData($studentIDAry,$Section2Fr,$Section2To);
//Print and Close button able
$displayTable = "
	<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'>".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td>
	</tr>
</table>";

?>

<?=$displayTable?>

<table class="form_table_v30">
<tr>
	<td class='field_title'><?=$i_Discipline_Class?></td>
	<td><?=$thisClassName?></td>
</tr>
<tr> 
	<td class='field_title'><?=$i_Discipline_System_Reports_Report_Period?></td>
	<td><?=$Section2Fr?> <?=$i_To?> <?=$Section2To?></td>
</tr>
</table>

<table class="common_table_list view_table_list_v30">
<td class="tabletop_print"><?=$i_ClassNumber?></td>
<td class="tabletop_print"><?=$i_UserStudentName?></td>
<td class="tabletop_print"><?=$ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME(1)?></td>
<td class="tabletop_print"><?=$ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME(2)?></td>
<td class="tabletop_print"><?=$ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME(3)?></td>
<td class="tabletop_print"><?=$ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME(4)?></td>
<td class="tabletop_print"><?=$i_Discipline_Absent?></td>
<td class="tabletop_print"><?=$i_UserRemark?></td>

<? foreach($student_ary as $k => $d) 
{
	list($thisUserID, $thisName, $thisClassNumber) = $d;
?>
<tr>
	<td width="20"><?=$thisClassNumber?></td>
	<td width="200"><?=$thisName?></td>
	<td width="50"><?=$data[$thisUserID][1]>=5?"<u>":""?><?=$data[$thisUserID][1] ? $data[$thisUserID][1] : 0 ?><?=$data[$thisUserID][1]>=5?"</u>":""?></td>
	<td width="50"><?=$data[$thisUserID][2]>=5?"<u>":""?><?=$data[$thisUserID][2] ? $data[$thisUserID][2] : 0 ?><?=$data[$thisUserID][2]>=5?"</u>":""?></td>
	<td width="50"><?=$data[$thisUserID][3]>=5?"<u>":""?><?=$data[$thisUserID][3] ? $data[$thisUserID][3] : 0 ?><?=$data[$thisUserID][3]>=5?"</u>":""?></td>
	<td width="50"><?=$data[$thisUserID][4]>=5?"<u>":""?><?=$data[$thisUserID][4] ? $data[$thisUserID][4] : 0 ?><?=$data[$thisUserID][4]>=5?"</u>":""?></td>
	<td width="50"><?=$absentData[$thisUserID]['Absent']>=5?"<u>":""?><?=($absentData[$thisUserID]['Absent'] ? $absentData[$thisUserID]['Absent'] : 0)?><?=$absentData[$thisUserID]['Absent']?"</u>":""?></td>
	<td>&nbsp;</td>
</tr>
<? } ?>
</table>

<?
intranet_closedb();

function retrieveAttendanceData($UserArray, $startDate, $endDate) {
	global $PATH_WRT_ROOT;
	// eAttendance define
	//		define("PROFILE_DAY_TYPE_WD",1);
	//		define("PROFILE_DAY_TYPE_AM",2);
	//		define("PROFILE_DAY_TYPE_PM",3);
	//		define("PROFILE_TYPE_ABSENT",1);
	include_once($PATH_WRT_ROOT.'includes/libdb.php');
	$ldb = new libdb();
	$month_year_Ary = array();
	$result = array();
	include_once ($PATH_WRT_ROOT . "includes/libgeneralsettings.php");
	# Get Profile Count for Late/Absent/Earlyleave
	$GeneralSetting = new libgeneralsettings();
	$SettingList = array (
			"'ProfileAttendCount'"
	);
	$Settings = $GeneralSetting->Get_General_Setting('StudentAttendance', $SettingList);
	$count_value = $Settings['ProfileAttendCount'] == 1 ? 0.5 : 1;
	// count (0.5/1) - according to eAttendance settings
	$eachCount = $count_value;
	
	// UserID
	$UserList = "-1";
	if(count($UserArray) > 0){
		$UserList = "";
		$delim = "";
		for($i=0; $i<count($UserArray); $i++){
			$UserList .= $delim."'".$UserArray[$i]."'";
			$delim = ", ";
		}
	}
	else {
		return "";
	}
	list($start_year,$start_month,$start_date) = explode("-",$startDate);
	list($end_year,$end_month,$end_date) = explode("-",$endDate);
	
	// build array for query data in CARD_STUDENT_DAILY_LOG_y_m
	// year
	while("$start_year" <= "$end_year"){
		$month_year_Ary[$start_year] = array();
		
		// month
		$check_month = "$start_year"=="$end_year"? $end_month : "12";
		while("$start_year$start_month" <= "$start_year$check_month"){
			$month_year_Ary[$start_year][] = $start_month;
			
			$start_month++;
			$start_month = $start_month<10? "0$start_month" : $start_month;
			if(count($month_year_Ary[$start_year]) > 12)
				break;
		}
		$start_month = "01";
		$start_year++;
	}
	
	if(count($month_year_Ary) > 0){
		foreach($month_year_Ary as $year => $month_Ary){
			foreach($month_Ary as $month){
				// Table
				$card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
				// Log date in (Y-m-d)
				$log_date_field = "CONCAT('".$year."','-','".$month."','-',IF(b.DayNumber<10, CONCAT('0',b.DayNumber), b.DayNumber))";
				
				$sql = "SELECT
				b.UserID,
				IF(b.DayNumber<10, CONCAT('0',b.DayNumber), b.DayNumber) AS DayNumber,
				$log_date_field AS LogDate,
				b.AMStatus,
				b.PMStatus,
				b.LeaveStatus,
				c.RecordStatus as am_late_waive,
				d.RecordStatus as pm_late_waive,
				e.RecordStatus as am_absent_waive,
				f.RecordStatus as pm_absent_waive
				FROM
				$card_log_table_name as b ";
				$sql .= "			LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as c
										ON
											c.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber))
											AND c.StudentID = b.UserID
											AND c.DayType = '2' AND c.RecordType = '2'
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as d
										ON
											d.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber))
											AND d.StudentID = b.UserID
											AND d.DayType = '3' AND d.RecordType = '2'
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as e
										ON
											e.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber))
											AND e.StudentID = b.UserID
											AND e.DayType = '2' AND e.RecordType = '1'
										LEFT JOIN CARD_STUDENT_PROFILE_RECORD_REASON as f
										ON
											f.RecordDate = CONCAT('".$year."-".$month."-',IF(b.DayNumber < 10,CONCAT('0',b.DayNumber),b.DayNumber))
											AND f.StudentID = b.UserID
											AND f.DayType = '3' AND f.RecordType = '1'
								where
									b.UserID in (".$UserList.") AND $log_date_field >= '".$startDate."' AND $log_date_field <= '".$endDate."'
								ORDER BY b.DayNumber";
				// 				debug_pr($sql);
				$log_entries = $ldb->returnArray($sql);
				
				for($j=0; $j<count($log_entries); $j++){
					$log_records = $log_entries[$j];
					if(!isset($result[$log_records['UserID']])){
						$result[$log_records['UserID']]["Total"] = 0;
						// 						$result[$log_records['UserID']]["Present"] = 0;
						$result[$log_records['UserID']]["Absent"] = 0;
						// [2015-1013-1434-16164]
						// 						$result[$log_records['UserID']]["Late"] = 0;
					}
					### 2020-01-17 Philips - Avoid Not Taken Status
					if($log_records["AMStatus"]=='-1' || $log_records["PMStatus"]=='-1') continue;
					
					$result[$log_records['UserID']]["Total"] += $eachCount;
					// Present (On Time, Absent with reason, Late, Outgoing)
					// 					if(isset($log_records["AMStatus"]) && ($log_records["AMStatus"]==0 ||
					// 					$log_records["AMStatus"]==2 || $log_records["AMStatus"]==3)){
					// 						$result[$log_records['UserID']]["Present"] += $eachCount;
					// 					}
					// 					if(isset($log_records["PMStatus"]) && ($log_records["PMStatus"]==0 ||
					// 							$log_records["PMStatus"]==2 || $log_records["PMStatus"]==3)){
					// 								$result[$log_records['UserID']]["Present"] += $eachCount;
					// 					}
					
					// Absent (Absent without reason)
					if($log_records["AMStatus"]==1 ){
						$result[$log_records['UserID']]["Absent"] += $eachCount;
					}
					if($log_records["PMStatus"]==1){
						$result[$log_records['UserID']]["Absent"] += $eachCount;
					}
					// 					if($log_records["AMStatus"]==1 || $log_records["PMStatus"]==1){
					// 						$result[$log_records['UserID']]["Absent"] += $eachCount;
					// 					}
					
					// 					if($log_records["AMStatus"]==2 && $log_records['am_late_waive'] != '1'){
					// 						$result[$log_records['UserID']]["Late"] += $eachCount;
					// 					}
					// 					if($log_records["PMStatus"]==2 && $log_records['pm_late_waive'] != '1'){
					// 						$result[$log_records['UserID']]["Late"] += $eachCount;
					// 					}
					}
			}
		}
	}
	return $result;
}
?>