<?php
# using:  
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Ranking_Detail_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


# School Year Menu #
$SchoolYear = $SchoolYear =="" ? Get_Current_Academic_Year_ID() : $SchoolYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange=\"changeTerm(this.value);showResult(document.getElementById('rankTarget').value)\"", "", $SchoolYear);


$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";


# Class #
$award_category = $ldiscipline->getAPCategoryByType(1);
$punishment_category = $ldiscipline->getAPCategoryByType(-1);
$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCatInStatistics();
$award_items = $ldiscipline->getAPItemsByType(1);
$punishment_items = $ldiscipline->getAPItemsByType(-1);


// Good Conduct Category
$selectGoodCatHTML = "<select name=\"selectGoodCat\" id=\"selectGoodCat\" onChange=\"changeCat(this.value);\">\n";
$selectGoodCatHTML .= "<option value=\"0\"";
$selectGoodCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectGoodCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Awards." --</option>";
for ($i = 0; $i < sizeof($award_category); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$award_category[$i][0]."\"";
	if ($selectGoodCat == $award_category[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$award_category[$i][1]."</option>\n";
}
$selectGoodCatHTML .= "</select>\n";

// Misconduct Category
$selectMisCatHTML = "<select name=\"selectMisCat\" id=\"selectMisCat\" onChange=\"changeCat(this.value);\">\n";
$selectMisCatHTML .= "<option value=\"0\"";
$selectMisCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
//$selectMisCatHTML .= ">-- ".$eDiscipline["SelectRecordCategory"]." --</option>";
$selectMisCatHTML .= ">-- $i_Discipline_System_Reports_All_Punishment --</option>";
for ($i = 0; $i < sizeof($punishment_category); $i++) {
	$selectMisCatHTML .= "<option value=\"".$punishment_category[$i][0]."\"";
	if ($selectMisCat == $punishment_category[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$punishment_category[$i][1]."</option>\n";
}
$selectMisCatHTML .= "</select>\n";

// Items
if($record_type==1) {
	$selectedItem = $award_items;	
} else {
	$selectedItem = $punishment_items;	
}

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);


$selectItemIDHTML = "<select name=\"ItemID[]\" id=\"ItemID[]\" multiple size=\"5\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($selectedItem); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $selectedItem[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			if (in_array($r_itemID, $ItemID)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".$r_itemName."</option>\n";
		}
	}
}
$selectItemIDHTML .= "</select>\n";


# menu highlight setting
$TAGS_OBJ[] = array($Lang['eDiscipline']['AwardPunishment_Detail_Ranking_Report'], "");

$CurrentPage = "APRankingDetail";

$warningMsg = $Lang['eDiscipline']['AwardPunishmentItemReportMsg'];

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";
	var misItem = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}
	if(form1.rankTarget.value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}	 
	
	if(document.getElementById('record_type').value==1) {
		if(document.getElementById('selectGoodCat').value!=1) {
			goodItem = document.getElementById('ItemID[]');
			choiceSelected = 0;
			for(i=0;i<goodItem.options.length;i++) {
				if(goodItem.options[i].selected==true) 
					choiceSelected = 1;
			}
			if(choiceSelected==0) {
				alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['GoodConductItem']?>");
				return false;
			}	 
		}
	} else {
		if(document.getElementById('selectMisCat').value!=1) {
			misItem = document.getElementById('ItemID[]');
			choiceSelected = 0;
			for(i=0;i<misItem.options.length;i++) {
				if(misItem.options[i].selected==true) 
					choiceSelected = 1;
			}
			if(choiceSelected==0) {
				alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['MisconductItem']?>");
				return false;
			}	 
		}
	}	

	if(form1.listBy[0].checked==false && form1.listBy[1].checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['DisplayIn']?>");	
		return false;
	}
	if(form1.listBy[1].checked==true && (isNaN(form1.times.value) || form1.times.value=='' || form1.times.value<0)) {
		alert("<?=$i_alert_pleasefillin." ".$i_Discipline_Times?>");
		return false;	
	}
	if(form1.sortBy[0].checked==false && form1.sortBy[1].checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['DisplayIn']?>");	
		return false;
	}
	
	getSelectedTextString(document.getElementById('ItemID[]'), 'ItemText2');
	document.getElementById("SelectedItemText").value += document.getElementById('ItemText2').value;
}


function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";

	if(span == 'spanStudent') {
		document.getElementById('rankTargetDetail[]').multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
		

	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		SelectAll(document.getElementById('rankTargetDetail[]'));
	}
	
	if(span == 'spanStudent') {
		document.getElementById('rankTargetDetail[]').multiple = true;
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function getSelectedTextString(obj, targetObj) {
	var tmpString = '';
	for (i=0; i<obj.length; i++) {
		if (obj.options[i].selected == true) {
			tmpString += obj.options[i].text + "','";
		}
	}
	if (tmpString.length > 0) {
		document.getElementById(targetObj).value = "'"+tmpString.substr(0, tmpString.length-3)+"'";
	} else {
		document.getElementById(targetObj).value = "'"+tmpString+"'";
	}
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
//-->
</script>
<script language="javascript">
var xmlHttp
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str)
{

	if(str != "student2ndLayer") 
		document.getElementById("studentFlag").value = 0;
	
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()

	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

		
	var url = "";
	url = "../get_live2.php";
	url += "?target=" + str;
	url += "&year=" + document.getElementById('SchoolYear').value;
	url += (document.getElementById('studentFlag').value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged() 
{ 
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
		if(document.getElementById("studentFlag").value == 0)
			if(document.getElementById('rankTarget').value != 'student') {
				SelectAll(document.getElementById('rankTargetDetail[]'));
			}
	} 
}

var xmlHttp2

function changeClassForm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&level=" + document.getElementById('level').value;
	
	xmlHttp.onreadystatechange = stateChanged2
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{

	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}


// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<?	foreach($award_category as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($punishment_category as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>


function changeCat(value)
{
	var item_select = document.getElementById('ItemID[]');
	var selectedCatID = value;
	var record_type_good = document.getElementById('record_type_good');

	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	
	if (selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>
		if(document.form1.record_type_good.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($award_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $award_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($punishment_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $punishment_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		}
	}
	else if (selectedCatID == '')
	{

	<? 
		$curr_cat_id = "";
		$pos = 0;
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
			$r_itemName = str_replace("'", "\'", $r_itemName);
			if ($r_catID != $curr_cat_id)
			{
				$pos = 0;
	?>
	}
	else if (selectedCatID == "<?=$r_catID?>")
	{
	<?
				$curr_cat_id = $r_catID;
			}
	?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
		item_select.selectedIndex = <?=$pos-1?>;
		<? 	} 
		}
	?>
	}
	SelectAllItem(document.getElementById('ItemID[]'), true);
}

</script>
<form name="form1" method="post" action="report_view.php" onSubmit="return goCheck(this)">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td align="center" bgcolor="#FFFFFF">
	      <table width="98%" border="0" cellspacing="0" cellpadding="5">
	          <tr> 
	            <td align="center">
		            <table width="88%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td height="20" class="tabletextremark"><i><?=$i_Discipline_System_Reports_Report_Option?></i></td>
									</tr>
								</table>
								<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $warningMsg) ?>
								<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
									<tr valign="top">
										<td height="57" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['Period']?><span class="tabletextrequire">*</span></td>
										<td>
											<table border="0" cellspacing="0" cellpadding="3">
												<tr>
													<td height="30" colspan="6" onClick="document.form1.dateChoice[0].checked=true">
														<input name="dateChoice" type="radio" id="dateChoice[0]" value="1" checked>
														<?=$i_Discipline_School_Year?>
														<?=$selectSchoolYearHTML?>
														<!--
														<select name="SchoolYear" id="SchoolYear" onFocus="document.form1.dateChoice[0].checked=true" onchange="changeTerm(this.value)">
															<?=$selectSchoolYear?>
														</select>//-->
														<?=$i_Discipline_Semester?> 
														<span id="spanSemester"><select name="semester" id="semester" onFocus="document.form1.dateChoice[0].checked=true" >
															<?=$SemesterMenu?>
														</select></span>
													</td>
												</tr>
												<tr>
													<td><input name="dateChoice" type="radio" id="dateChoice[1]" value="2">
														<?=$iDiscipline['Period_Start']?></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
													<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RankingTarget']?><span class="tabletextrequire">*</span></td>
										<td width="80%">
											<table width="0%" border="0" cellspacing="2" cellpadding="0">
												<tr>
													<td valign="top">
														<table border="0">
															<tr>
																<td valign="top">
																	<select name="rankTarget" id="rankTarget" onChange="showResult(this.value);if(this.value=='student') {showSpan('spanStudent');document.form1.selectAllBtn01.style} else {hideSpan('spanStudent');}">
																		<option value="#" selected>-- <?=$i_general_please_select?> --</option>
																		<option value="form"><?=$i_Discipline_Form?></option>
																		<option value="class"><?=$i_Discipline_Class?></option>
																		<option value="student"><?=$i_UserStudentName?></option>
																	</select>
																</td>
																<td>
																	<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
																	<select name="rankTargetDetail[]"  size="5" id="rankTargetDetail[]"></select>
																	<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
																</td>
																<td>
																	<div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:1;display:none;'>
																		<select name="studentID[]" multiple size="5" id="studentID[]"></select>
																		<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
																	</div>
																</td>
															</tr>
															<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
														</table>
													</td>
													<td valign="top"><br></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle">
											<span><?=$iDiscipline['RecordType2']?> <span class="tabletextrequire">*</span></span>
										</td>
										<td width="85%">
											<table width="0%" border="0" cellspacing="2" cellpadding="0">
												<tr>
													<td valign="top">
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td>
																	<input name="record_type" type="radio" id="record_type_good" value="1" <?=($record_type==1 || $record_type=="")?"checked":""?> onClick="hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';"><label for="record_type_good"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Award?></label>
																	<input name="record_type" type="radio" id="record_type_mis" value="-1" <?=($record_type==-1)?"checked":""?> onClick="hideSpan('spanGoodCat');showSpan('spanMisCat');changeCat('0');document.getElementById('selectMisCat').value='0';"><label for="record_type_mis"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Punishment?></label><br />
																	<span id="spanGoodCat"<? echo ($record_type==1 || $record_type=="")?"":" style=\"display:none\"" ?>>
																		<?=$selectGoodCatHTML?>
																	</span>
																	<span id="spanMisCat"<? echo ($record_type==-1)?"":" style=\"display:none\"" ?>>
																		<?=$selectMisCatHTML?>
																	</span>
																</td>
															</tr>
															<tr>
																<td>
																	<?=$selectItemIDHTML?>
																	<?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('ItemID[]'), true); return false;") ?>
																</td>
															</tr>
															<tr>
																<td>
																	<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
											<input type="hidden" name="selectShowOnlyType" id="selectShowOnlyType" value="ALL">
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['DisplayIn']?><span class="tabletextrequire">*</span>
										</td>
										<td><input name="listBy" type="radio" id="listBy[0]" value="byClass" onClick="javascript:hideSpan('spanIndividual');" checked>
											<label for="listBy[0]"><?=$i_general_class?></label>
											<input name="listBy" type="radio" id="listBy[1]" value="byIndividual" onClick="javascript:showSpan('spanIndividual');">
											<label for="listBy[1]"><?=$Lang['eDiscipline']['Individual']?></label>
										</td>
									</tr>
									<tr>
										<!--<td valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>-->
										<td colspan="2">
											<span id="spanIndividual" style="display:none">
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td width="100%">
															<table width="100%" border="0" cellspacing="0" cellpadding="0">
																<tr>
																	<td width="20%" class="formfieldtitle"><?=$i_Discipline_Times?><span class="tabletextrequire">*</span></td>
																	<td valign="top" width="80%">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td>
																					<input type="text" name="times" id="times" value="" size="6" maxlength="6"> 
																					<input type="checkbox" name="onBtn" id="onBtn" value="1">
																					<label for="onBtn"><?=$Lang['eDiscipline']['OnOrAbove']?></label>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<input type="hidden" name="selectShowOnlyType" id="selectShowOnlyType" value="ALL">
												</table>
											</span>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$list_sortby ?><span class="tabletextrequire">*</span></td>
										<td><input name="sortBy" type="radio" id="sortBy[0]" value="byTimes" checked>
											<label for="sortBy[0]"><?=$i_Discipline_Times?></label>
											<input name="sortBy" type="radio" id="sortBy[1]" value="byClass">
											<label for="sortBy[1]"><?=$i_general_class?></label>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
										<td>&nbsp;</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td align="right">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center">
												<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.action='report_view.php'", "submitBtn01")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
			            </td>
			          </tr>
			        </table>
		        </td>
		    </tr>
			</table><br>
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
<input type="hidden" name="ItemText2" id="ItemText2" />
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<input type="hidden" name="SelectedItemText" id="SelectedItemText" />

</form>

<script>
<!--
function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form");
	changeCat('0');
	changeTerm('<?=$SchoolYear?>');
}

init();


//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
