<?php
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Ranking_Detail_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


if($rankTarget == '') {
	header("Location: index.php");	
}


# School Year Menu #
$SchoolYear = $SchoolYear =="" ? Get_Current_Academic_Year_ID() : $SchoolYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYear = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange=\"changeTerm(this.value);showResult(document.getElementById('rankTarget').value)\"'", "", $SchoolYear);

//$conds .= " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;


$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($semester);

# Semester Menu #
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
	$dateAry['dateChoice'] = 2;
	$dateAry['date1'] = $ldiscipline->getAcademicYearIDByYearName(GET_ACADEMIC_YEAR3($startDate));
	$dateAry['date2'] = "";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $ldiscipline->getAcademicYearNameByYearID($SchoolYear)." ".$semesterText;
		list($startYear, $endYear) = split('-',$SchoolYear);
		//$conds .= " AND a.AcademicYearID=$SchoolYear";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
	}
	//$conds .= ($semester != 'WholeYear' && $semester!=0) ? " AND a.YearTermID='$semester'" : "";
	$dateAry['dateChoice'] = 1;
	$dateAry['date1'] = $SchoolYear;
	$dateAry['date2'] = $semester;
	
}

$award_category = $ldiscipline->getAPCategoryByType(1);
$punishment_category = $ldiscipline->getAPCategoryByType(-1);
$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();
$award_items = $ldiscipline->getAPItemsByType(1);
$punishment_items = $ldiscipline->getAPItemsByType(-1);

if($record_type==1) {
	$selectedCategoryItem = $ldiscipline->getGMItemByCategoryID($selectGoodCat);
	$selectedItem = $award_items;	
	$selectedCat = $selectGoodCat;
} else {
	$selectedCategoryItem = $ldiscipline->getGMItemByCategoryID($selectMisCat);
	$selectedItem = $punishment_items;
	$selectedCat = $selectMisCat;
}


# Good Conduct Category
$selectGoodCatHTML = "<select name=\"selectGoodCat\" id=\"selectGoodCat\" onChange=\"changeCat(this.value);SelectAllItem(document.getElementById('ItemID[]'), true);\">\n";
$selectGoodCatHTML .= "<option value=\"0\"";
$selectGoodCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectGoodCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Awards." --</option>";
for ($i = 0; $i < sizeof($award_category); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$award_category[$i][0]."\"";
	if ($selectGoodCat == $award_category[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$award_category[$i][1]."</option>\n";
}
$selectGoodCatHTML .= "</select>\n";


$selectMisCatHTML = "<select name=\"selectMisCat\" id=\"selectMisCat\" onChange=\"changeCat(this.value);SelectAllItem(document.getElementById('ItemID[]'), true);\">\n";
$selectMisCatHTML .= "<option value=\"0\"";
$selectMisCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectMisCatHTML .= ">-- $i_Discipline_System_Reports_All_Punishment --</option>";
for ($i = 0; $i < sizeof($punishment_category); $i++) {
	$selectMisCatHTML .= "<option value=\"".$punishment_category[$i][0]."\"";
	if ($selectMisCat == $punishment_category[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$punishment_category[$i][1]."</option>\n";
}
$selectMisCatHTML .= "</select>\n";

$selectItemIDHTML = "<select name=\"ItemID[]\" id=\"ItemID[]\" multiple size=\"5\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($selectedItem); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $selectedItem[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			if (in_array($r_itemID, $ItemID)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".$r_itemName."</option>\n";
		}
	}
}
$selectItemIDHTML .= "</select>\n";

if(sizeof($ItemID)>0) {
	if($selectGoodCat != '0') {			# select from 1 good conduct category
		//$conds .= " AND c.CatID=$selectGoodCat";	
		$additionConds = " AND c.CatID=$selectGoodCat";	
		$catConds = " AND c.CatID=$selectGoodCat";
	} else if($selectMisCat != '0') {	# select from 1 misconduct category
		//$conds .= " AND c.CatID=$selectMisCat";	
		$additionConds .= " AND c.CatID=$selectMisCat";	
		$catConds = " AND c.CatID=$selectMisCat";	
	} else {					# select from "ALL"
		$SelectedItemText = substr($SelectedItemText,2,-2);
		$newItemText = "'".str_replace("\',\'","','",$SelectedItemText)."'";
		//$conds .= " AND a.ItemID IN (".implode(',',$ItemID).")";
		$additionConds .= " AND a.ItemID IN (".implode(',',$ItemID).")";
	}
	$itemConds = " AND a.ItemID IN (".implode(',',$ItemID).")";
}

$groupBy = "";

if($listBy == "byClass") {
	$groupBy .= " yc.YearClassID";
}
else if($listBy == "byIndividual") {
	$having .= ($onBtn==1) ? " HAVING totalCount>=$times" : " HAVING totalCount=$times";
	$groupBy .= " ycu.UserID";
}

if($sortBy == "byTimes")
	$sortByConds = " totalCount DESC,  y.Sequence, yc.Sequence, ycu.ClassNumber";
else if($sortBy == "byClass")
	$sortByConds = " y.Sequence, yc.Sequence, ycu.ClassNumber";


# Rank Target
$rankTargetMenu .= "<select name='rankTarget' id='rankTarget' onChange=\"showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');} \">";
$rankTargetMenu .= "<option value='#'>-- $i_general_please_select --</option>";
$rankTargetMenu .= "<option value='form'";
$rankTargetMenu .= ($rankTarget=='form') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Form</option>";
$rankTargetMenu .= "<option value='class'";
$rankTargetMenu .= ($rankTarget=='class') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Class</option>";
$rankTargetMenu .= "<option value='student'";
$rankTargetMenu .= ($rankTarget=='student') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Student</option>";
$rankTargetMenu .= "</select>";

# form / class

if(is_array($rankTargetDetail)) {
	$rankTargetDetail = $rankTargetDetail;
}
else {
	$rankTargetDetail[0] = $rankTargetDetail;
}

$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= "y.YearID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= " yc.YearClassID=$rankTargetDetail[$i]";
	}
} else {
	$tempConds .= " a.StudentID IN (".implode(',',$studentID).")";	
}
$conds .= " AND ($tempConds)";


$jsRankTargetDetail = implode(',', $rankTargetDetail);


$result = $ldiscipline->getAPItemDetailReport($conds, $groupBy, $sortByConds, $having, $dateAry, $times, $onBtn, $rankTarget, $jsRankTargetDetail, $studentID, $itemConds, $catConds, $additionConds);

# Title of the report
$categoryName = ($record_type==1 && $selectGoodCat=='0') ? $i_Discipline_System_Reports_All_Awards : (($record_type==-1 && $selectMisCat=='0') ? $i_Discipline_System_Reports_All_Punishment : $ldiscipline->getAwardPunishCategoryName($selectedCat));
if($intranet_session_language=="en") {
	$title = $date."<br>";
	$title .= $Lang['eDiscipline']['ListOf']." ".$categoryName;
	$title .= ($listBy=='byIndividual') ? " ".$times." ".$Lang['eDiscipline']['Times'] : "";
	if($onBtn==1)	
		$title .= " ".$Lang['eDiscipline']['OnOrAbove'];
	
	$export_title .= $date."\n".$Lang['eDiscipline']['ListOf']." ".$categoryName;
	$export_title .= ($listBy=='byIndividual') ? " ".$times." ".$Lang['eDiscipline']['Times'] : "";
	if($onBtn==1)
		$export_title .= " ".$Lang['eDiscipline']['OnOrAbove'];
} else {
	$title = $date."<br>";
	$title .= $categoryName;
	$title .= ($listBy=='byIndividual') ? " ".$times." ".$Lang['eDiscipline']['Times'] : "";
	if($onBtn==1)
		$title .= $Lang['eDiscipline']['OnOrAbove'];
	$title .= $Lang['eDiscipline']['ListOf2'];
	
	$export_title .= $date."\n".$categoryName;
	$export_title .= ($listBy=='byIndividual') ? " ".$times." ".$Lang['eDiscipline']['Times'] : "";
	if($onBtn==1)
		$export_title .= $Lang['eDiscipline']['OnOrAbove'];
	$export_title .= $Lang['eDiscipline']['ListOf2'];
}

$export_content = "";
$export_detention = "";

# table Top
$tableTop .= "<table width='100%' border='0' cellpadding='";
$tableTop .= ($listBy=='byClass') ? 2 : 4;
$tableTop .= "' cellspacing='1' bgcolor='#CCCCCC'>";
$tableTop .= "<tr class='tablebluetop'>";
if($listBy=='byClass') {	
	$tableTop .= "<td width='40%' class='tabletopnolink'>$i_ClassName</td>";
	$tableTop .= "<td width='40%' class='tabletopnolink'>{$Lang['eDiscipline']['Times']}</td>";
	
	$export_top .= "&quot;".$i_ClassName."&quot;\t";
	$export_top .= "&quot;".$Lang['eDiscipline']['Times']."&quot;\t";
} else {
	$tableTop .= "<td width='10%' class='tabletopnolink'>$i_ClassName</td>";
	$tableTop .= "<td width='10%' class='tabletopnolink'>{$iDiscipline['ClassNo']}</td>";
	$tableTop .= "<td width='40%' class='tabletopnolink'>$i_UserStudentName</td>";
	$tableTop .= "<td width='40%' class='tabletopnolink'>{$Lang['eDiscipline']['Times']}</td>";
	
	$export_top .= "&quot;".$i_ClassName."&quot;\t";
	$export_top .= "&quot;".$iDiscipline['ClassNo']."&quot;\t";
	$export_top .= "&quot;".$i_UserStudentName."&quot;\t";
	$export_top .= "&quot;".$Lang['eDiscipline']['Times']."&quot;";
}
$tableTop .= "</tr>";

$export_detention .= "&quot;Class Name&quot;\t";
$export_detention .= "&quot;Class Number&quot;\t";
$export_detention .= "&quot;Reason&quot;\t";
$export_detention .= "&quot;Remark&quot;";
$export_detention .= "\n";


# table bottom
$tableBottom .= "</table>";

$tableContent = "";

if(sizeof($result)==0) {	# table no record
	
	$tableContent .= $tableTop;
	$tableContent .= "<tr class='tablebluerow1'>";
	$tableContent .= "<td class='tabletext' colspan='4' height='40' align='center'>{$i_no_record_exists_msg}</td>";
	$tableContent .= "</tr>";
	$tableContent .= $tableBottom;
	
	$export_content .= $export_title;
	$export_content .= "\n\n";
	$export_content .= $export_top;
	$export_content .= "\n";
	$export_content .= "&quot;".$i_no_record_exists_msg."&quot;";
} else {					# with data
	$tableContent .= $tableTop;
	
	$export_content .= $export_title;
	$export_content .= "\n\n";
	$export_content .= $export_top;
	$export_content .= "\n";
	
	for($i=0; $i<sizeof($result); $i++) {
		$css = ($i%2==1) ? 2 : 1;
		list($clsName, $clsNo, $stdName, $total, $reason, $remark) = $result[$i];
		
		# print view
		$tableContent .= "<tr class='tablebluerow$css'>";
		$tableContent .= "	<td class='tabletext'>$clsName</td>";
		if($listBy=='byIndividual') {	
			$tableContent .= "	<td class='tabletext'>$clsNo</td>";
			$tableContent .= "	<td class='tabletext'>$stdName</td>";
		}
		$tableContent .= "	<td class='tabletext'>$total</td>";
		$tableContent .= "</tr>";
		
		# export view
		$export_content .= "&quot;".$clsName."&quot;\t";
		if($listBy=='byIndividual') {	
			$export_content .= "&quot;".$clsNo."&quot;\t";
			$export_content .= "&quot;".$stdName."&quot;\t";
		}
		$export_content .= "&quot;".$total."&quot;";
		$export_content .= "\n";
		
		# detention export view
		$export_detention .= "&quot;".$clsName."&quot;\t";
		$export_detention .= "&quot;".$clsNo."&quot;\t";
		$export_detention .= "&quot;".$reason." ".$times." ".$Lang['eDiscipline']['Times']." ".$Lang['eDiscipline']['OnOrAbove']."&quot;\t";
		$export_detention .= "&quot;".$remark."&quot;";
		$export_detention .= "\n";
	}
	$tableContent .= $tableBottom;
	
}

# Report Option Layer
$reportOptionContent .= "<table width='88%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr><td>";
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td height='30' colspan='6' onFocus=\"document.form1.dateChoice[0].checked=true\">";
$reportOptionContent .= "<input name='dateChoice' type='radio' id='dateChoice[0]' value='1'";
$reportOptionContent .= ($dateChoice==1) ? " checked" : "";
$reportOptionContent .= ">$i_Discipline_School_Year";
$reportOptionContent .= $selectSchoolYear;
$reportOptionContent .= "$i_Discipline_Semester ";
$reportOptionContent .= "<span id='spanSemester'><select name='semester' id='semester' onFocus=\"document.form1.dateChoice[0].checked=true\" >";
$reportOptionContent .= $SemesterMenu;
$reportOptionContent .= "</select></span></td></tr>";
$reportOptionContent .= "<tr><td><input name='dateChoice' type='radio' id='dateChoice[1]' value='2'";
$reportOptionContent .= ($dateChoice==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true' onFocus='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("startDate",$startDate);
$reportOptionContent .= "</td><td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true' onFocus='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("endDate",$endDate);
$reportOptionContent .= "</td></tr></table></td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingTarget']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='0%' border='0' cellspacing='2' cellpadding='0'>";
$reportOptionContent .= "<tr><td valign='top'>";
$reportOptionContent .= $rankTargetMenu;
$reportOptionContent .= "</td><td><div id='rankTargetDetail' style='position:absolute; width:280px; height:100px; z-index:0;'></div>";
$reportOptionContent .= "<select name='rankTargetDetail[]' id='rankTargetDetail[]' multiple size='5'></select><br>";
$reportOptionContent .= $linterface->GET_BTN($button_select_all, 'button', "javascript:SelectAll(this.form.elements['rankTargetDetail[]']);return false;", "selectAllBtn01");
$reportOptionContent .= "<td><div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:0;display:";
$reportOptionContent .= ($rankTarget=='student') ? "inline" : "none";
$reportOptionContent .= ";'><select name='studentID[]' id='studentID[]' multiple size='5'></select><br>";
$reportOptionContent .= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(this.form.elements['studentID[]'], true);return false;");
$reportOptionContent .= "</div></td>";
$reportOptionContent .= "</tr><tr><td colspan='3' class='tabletextremark'>($i_Discipline_Press_Ctrl_Key)</td></tr></table></td></tr>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "	<td valign='top' nowrap='nowrap' class='formfieldtitle'>";
$reportOptionContent .= "	<span>".$iDiscipline['RecordType2']." <span class='tabletextrequire'>*</span></span>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "	<td width='85%'>";
$reportOptionContent .= "		<table width='0%' border='0' cellspacing='2' cellpadding='0'>";
$reportOptionContent .= "			<tr><td valign='top'>";
$reportOptionContent .= "				<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
$reportOptionContent .= "					<tr><td>";
$reportOptionContent .= "						<input name='record_type' type='radio' id='record_type_good' value='1' ".(($record_type==1 || $record_type=='')?"checked":"")." onClick=\"hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';\"><label for='record_type_good'><img src='$image_path/$LAYOUT_SKIN/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'> $i_Merit_Award</label>";
$reportOptionContent .= "						<input name='record_type' type='radio' id='record_type_mis' value='-1' ".(($record_type==-1)?"checked":"")." onClick=\"hideSpan('spanGoodCat');showSpan('spanMisCat');changeCat('0');document.getElementById('selectMisCat').value='0';\"><label for='record_type_mis'><img src='$image_path/$LAYOUT_SKIN/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'> $i_Merit_Punishment</label><br />";
$reportOptionContent .= "						<span id='spanGoodCat'".(($record_type==1 || $record_type=='') ? "" : " style='display:none'") .">";
$reportOptionContent .= 							$selectGoodCatHTML;
$reportOptionContent .= "						</span>";
$reportOptionContent .= "						<span id='spanMisCat'".(($record_type==-1) ? "" : " style='display:none'") .">";
$reportOptionContent .= 							$selectMisCatHTML;
$reportOptionContent .= "						</span>";
$reportOptionContent .= "					</td></tr>";
$reportOptionContent .= "					<tr><td>";
$reportOptionContent .= 						$selectItemIDHTML;
$reportOptionContent .= 						$linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('ItemID[]'), true); return false;");
$reportOptionContent .= "					</td></tr>";
$reportOptionContent .= "					<tr><td>";
$reportOptionContent .= "						<span class='tabletextremark'>($i_Discipline_Press_Ctrl_Key)</span>";
$reportOptionContent .= "					</td></tr>";
$reportOptionContent .= "				</table>";
$reportOptionContent .= "			</td></tr>";
$reportOptionContent .= "		</table>";
$reportOptionContent .= "		<input type='hidden' name='selectShowOnlyType' id='selectShowOnlyType' value='ALL'>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "</tr>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "	<td valign='top' nowrap='nowrap' class='formfieldtitle'>".$Lang['eDiscipline']['DisplayIn']."<span class='tabletextrequire'>*</span>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "	<td><input name='listBy' type='radio' id='listBy[0]' value='byClass' onClick=\"javascript:hideSpan('spanIndividual');\"";
$reportOptionContent .= ($listBy=='byClass') ? " checked" : "";
$reportOptionContent .= "	>";
$reportOptionContent .= "		<label for='listBy[0]'>".$i_general_class."</label>";
$reportOptionContent .= "		<input name='listBy' type='radio' id='listBy[1]' value='byIndividual' onClick=\"javascript:showSpan('spanIndividual');\"";
$reportOptionContent .= ($listBy=='byIndividual') ? " checked" : "";
$reportOptionContent .= "		>";
$reportOptionContent .= "		<label for='listBy[1]'>".$Lang['eDiscipline']['Individual']."</label>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "</tr>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "	<td colspan='2'>";
$reportOptionContent .= "		<span id='spanIndividual' style='display:";
$reportOptionContent .= ($listBy=='byIndividual') ? " inline" : " none";
$reportOptionContent .= "		'>";
$reportOptionContent .= "		<table width='100%' cellspacing='0' cellpadding='0' border='0'>";
$reportOptionContent .= "			<tr>";
$reportOptionContent .= "				<td width='100%'>";
$reportOptionContent .= "					<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
$reportOptionContent .= "						<tr>";
$reportOptionContent .= "							<td width='20%' class='formfieldtitle'>".$Lang['eDiscipline']['Times']."<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "							<td valign='top' width='80%'>";
$reportOptionContent .= "								<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
$reportOptionContent .= "									<tr>";
$reportOptionContent .= "										<td>";
$reportOptionContent .= "											<input type='text' name='times' id='times' value='$times' size='6' maxlength='6'> 
																	<input type='checkbox' name='onBtn' id='onBtn' value='1'";
$reportOptionContent .= ($onBtn!='') ? " checked" : "";
$reportOptionContent .= "											><label for='onBtn'>".$Lang['eDiscipline']['OnOrAbove']."</label>";
$reportOptionContent .= "										</td>";
$reportOptionContent .= "									</tr>";
$reportOptionContent .= "								</table>";
$reportOptionContent .= "							</td>";
$reportOptionContent .= "						</tr>";
$reportOptionContent .= "					</table>";
$reportOptionContent .= "				</td>";
$reportOptionContent .= "			</tr>";
$reportOptionContent .= "			<input type='hidden' name='selectShowOnlyType' id='selectShowOnlyType' value='ALL'>";
$reportOptionContent .= "		</table>";
$reportOptionContent .= "		</span>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "</tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "	<td valign='top' nowrap='nowrap' class='formfieldtitle'>$list_sortby<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "	<td><input name='sortBy' type='radio' id='sortBy[0]' value='byTimes'";
$reportOptionContent .= ($sortBy=="byTimes") ? " checked" : "";
$reportOptionContent .= "		>";
$reportOptionContent .= "		<label for='sortBy[0]'>{$Lang['eDiscipline']['Times']}</label>";
$reportOptionContent .= "		<input name='sortBy' type='radio' id='sortBy[1]' value='byClass'";
$reportOptionContent .= ($sortBy=="byClass") ? " checked" : "";
$reportOptionContent .= "		>";
$reportOptionContent .= "		<label for='sortBy[1]'>$i_general_class</label>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "</tr>";
$reportOptionContent .= "<tr><td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td align='center'>";
$reportOptionContent .= "</td></tr><tr><td align='center' colspan='2'>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.target='';document.form1.action='report_view.php'");
$reportOptionContent .= "</td></tr></table>";

# menu highlight setting
$TAGS_OBJ[] = array($Lang['eDiscipline']['AwardPunishment_Detail_Ranking_Report'], "");

$CurrentPage = "APRankingDetail";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function SelectAllItem(obj, flag){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
		form1.elements['rankTargetDetail[]'].multiple = true;
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.getElementById('showReportOption').value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('showReportOption').value = '0';
}

function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";
	var misItem = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}
	if(form1.rankTarget.value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}	 
	
	if(document.getElementById('record_type').value==1) {
		if(document.getElementById('selectGoodCat').value!=1) {
			goodItem = document.getElementById('ItemID[]');
			choiceSelected = 0;
			for(i=0;i<goodItem.options.length;i++) {
				if(goodItem.options[i].selected==true) 
					choiceSelected = 1;
			}
			if(choiceSelected==0) {
				alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['GoodConductItem']?>");
				return false;
			}	 
		}
	} else {
		if(document.getElementById('selectMisCat').value!=1) {
			misItem = document.getElementById('ItemID[]');
			choiceSelected = 0;
			for(i=0;i<misItem.options.length;i++) {
				if(misItem.options[i].selected==true) 
					choiceSelected = 1;
			}
			if(choiceSelected==0) {
				alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['MisconductItem']?>");
				return false;
			}	 
		}
	}	

	if(form1.listBy[0].checked==false && form1.listBy[1].checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['DisplayIn']?>");	
		return false;
	}
	if(form1.listBy[1].checked==true && (isNaN(form1.times.value) || form1.times.value=='' || form1.times.value<0)) {
		alert("<?=$i_alert_pleasefillin." ".$i_Discipline_Times?>");
		return false;	
	}
	if(form1.sortBy[0].checked==false && form1.sortBy[1].checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$Lang['eDiscipline']['DisplayIn']?>");	
		return false;
	}
	
	getSelectedTextString(document.getElementById('ItemID[]'), 'ItemText2');
	document.getElementById("SelectedItemText").value += document.getElementById('ItemText2').value;
}


function showDiv(div)
{
	document.getElementById(div).style.display = "inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display = "none";
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + "','";
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = "'"+tmpString.substr(0, tmpString.length-3)+"'";
	} else {
		document.getElementById(targetObj).value = "'"+tmpString+"'";
	}
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
//-->
</script>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp
var xmlHttp2

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "../get_live2.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+document.getElementById('SchoolYear').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
	url += (document.getElementById("studentFlag").value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";

	xmlHttp.onreadystatechange = stateChanged 
	
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	//SelectAll(form1.elements['rankTargetDetail[]']);
} 



function stateChanged() 
{ 

	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}

// Generate Award Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory2"]?> --",0);
<?	foreach($award_category as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Punishment Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($punishment_category as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>


function changeCat(value)
{
	var item_select = document.getElementById('ItemID[]');
	var selectedCatID = value;
	var record_type_good = document.getElementById('record_type_good');

	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	
	if (selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>
		if(document.form1.record_type_good.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($award_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $award_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($punishment_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $punishment_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		}
	}
	else if (selectedCatID == '')
	{

	<? 
		$curr_cat_id = "";
		$pos = 0;
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
			$r_itemName = str_replace("'", "\'", $r_itemName);
			if ($r_catID != $curr_cat_id)
			{
				$pos = 0;
	?>
	}
	else if (selectedCatID == "<?=$r_catID?>")
	{
	<?
				$curr_cat_id = $r_catID;
			}
	?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
		item_select.selectedIndex = <?=$pos-1?>;
		<? 	} 
		}
	?>
	}
	SelectAllItem(document.getElementById('ItemID[]'), true);
}



function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="post" action="report_view.php" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td>
						<table border="0" cellspacing="5" cellpadding="5" align="center">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$title?></td>
							</tr>
						</table>
						<br>
						<?=$tableContent?>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.target='_self';document.form1.action='report_export.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_print, "submit", "document.form1.target='_blank';document.form1.action='report_print.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>
<input type="hidden" name="showReportOption" id="showReportOption" value="0">
<input type="hidden" name="studentFlag" id="studentFlag" value="<? if(sizeof($studentID)>0){echo "1";} else { echo "0";} ?>">			
<input type="hidden" name="ItemText1" id="ItemText1" />
<input type="hidden" name="ItemText2" id="ItemText2" />
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<input type="hidden" name="SelectedItemText" id="SelectedItemText" />
<input type="hidden" name="content" value='<?=urlencode($tableContent)?>' />
<input type="hidden" name="title" value='<?=urlencode($title)?>' />
<input type="hidden" name="exportContent" value='<?=intranet_htmlspecialchars($export_content)?>' />
</form>
<script language="javascript">
<!--
showResult("<?=$rankTarget?>", "<?=$jsRankTargetDetail?>");

<?
if(sizeof($studentID)>0) 
	echo "initialStudent()\n";	
?>

function initialStudent() {
	xmlHttp2 = GetXmlHttpObject()
	url = "../get_live2.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID)?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
	} 
}

changeTerm('<?=$SchoolYear?>');
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
