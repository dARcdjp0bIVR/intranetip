<?php
$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!sizeof($_POST)) {
	header("Location: index.php");	
}

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>

<table width="98%" border="0" cellpadding="4" cellspacing="0" align="center">
	<tr>
		<td align="right" class='print_hide'>
			<?= $linterface->GET_BTN($button_print, "submit", "window.print(); return false;"); ?>
		</td>
	</tr>
	<tr class="tabletext" height='40'>
		<td align="center" class="tabletext">
			<?= urldecode($title)?>
		</td>
	</tr>
	<tr class="tabletext">
		<td>
			<?= urldecode($content);?>
		</td>
	</tr>
</table>
<?

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>