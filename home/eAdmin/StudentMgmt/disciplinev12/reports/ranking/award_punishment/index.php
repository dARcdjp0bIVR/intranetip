<?php
# using:  
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Ranking_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


# School Year Menu #
$SchoolYear = $SchoolYear =="" ? Get_Current_Academic_Year_ID() : $SchoolYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange='changeTerm(this.value)'", "", $SchoolYear);

# Semester Menu #
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";


# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==10) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);


# menu highlight setting
$TAGS_OBJ[] = array($eDiscipline['AwardPunishmentRanking'], "");
$CurrentPage = "AwardPunishmentRanking";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if(document.getElementById('rankTarget').value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	var choiceSelected = 0;
	var choice = document.getElementById('rankTargetDetail[]');

	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";

	if(span == 'spanStudent') {
		form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
		

	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		SelectAll(document.getElementById('rankTargetDetail[]'));
	}
	
	if(span == 'spanStudent') {
		form1.elements['rankTargetDetail[]'].multiple = true;
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function getSelectedTextString(obj, targetObj) {
	var tmpString = '';
	for (i=0; i<obj.length; i++) {
		if (obj.options[i].selected == true) {
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0) {
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}
//-->
</script>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;

	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

		
	var url = "";
		
	url = "get_live2.php";
	url = url + "?target=" + str
	url += "&year="+document.getElementById('SchoolYear').value;
	url += (document.getElementById("studentFlag").value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged() 
{ 
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
		if(document.getElementById("studentFlag").value == 0)
			if(document.getElementById('rankTarget').value != 'student') {
				SelectAll(form1.elements['rankTargetDetail[]']);
			}
	} 
	//document.getElementById("studentFlag").value = 0;
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td align="center" bgcolor="#FFFFFF">
	      <table width="98%" border="0" cellspacing="0" cellpadding="5">
	          <tr> 
	            <td align="center">
		            <table width="88%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td height="20" class="tabletextremark"><i><?=$i_Discipline_System_Reports_Report_Option?></i></td>
									</tr>
								</table>
								<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Ranking_Report_Msg) ?>
								<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
									<tr valign="top">
										<td height="57" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['Period']?><span class="tabletextrequire">*</span></td>
										<td>
											<table border="0" cellspacing="0" cellpadding="3">
												<tr>
													<td height="30" colspan="6" onClick="document.form1.dateChoice[0].checked=true">
														<input name="dateChoice" type="radio" id="dateChoice[0]" value="1" checked>
														<?=$i_Discipline_School_Year?>
														<?=$selectSchoolYearHTML?>
														<!--
														<select name="SchoolYear" id="SchoolYear" onFocus="document.form1.dateChoice[0].checked=true" onChange="changeTerm(this.value)">
															<?=$selectSchoolYear?>
														</select>
														//-->
														<?=$i_Discipline_Semester?> 
														<span id="spanSemester"><select name="semester" id="semester" onFocus="document.form1.dateChoice[0].checked=true" >
															<?=$SemesterMenu?>
														</select></span>
													</td>
												</tr>
												<tr>
													<td><input name="dateChoice" type="radio" id="dateChoice[1]" value="2">
														<?=$iDiscipline['Period_Start']?></td>
													<td><!--<input name="startDate" type="text" class="tabletext" onFocus="document.form1.dateChoice[1].checked=true" />--></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
													<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
													<td><!--<input name="endDate" type="text" class="tabletext" onFocus="document.form1.dateChoice[1].checked=true" />--></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RankingTarget']?><span class="tabletextrequire">*</span></td>
										<td width="80%">
											<table width="0%" border="0" cellspacing="2" cellpadding="0">
												<tr>
													<td valign="top">
														<select name="rankTarget" id="rankTarget" onChange="showResult(this.value);if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}">
															<option value="#" selected>-- <?=$i_general_please_select?> --</option>
															<option value="form"><?=$i_Discipline_Form?></option>
															<option value="class"><?=$i_Discipline_Class?></option>
															<option value="student"><?=$i_UserStudentName?></option>
														</select>
													</td>
													<td>
														<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
														<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
														<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAll(this.form.elements['rankTargetDetail[]']);return false;", "selectAllBtn01")?>
													</td>
													<td>
														<div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:1;display:none;'>
															<select name="studentID[]" id="studentID[]" multiple size="5"></select>
															<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(this.form.elements['studentID[]'], true);return false;")?>
														</div>
													</td>
												</tr>
												<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
											</table>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RankingRange']?><span class="tabletextrequire">*</span></td>
										<td><?=$i_general_highest?> 
											<select name="rankRange" id="rankRange">
												<?=$rankRangeMenu?>
											</select>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RecordType']?><span class="tabletextrequire">*</span>
										</td>
										<td><input name="meritType" type="radio" id="meritType[0]" value="1" checked>
											<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_merit.gif" width="20" height="20" border="0" align="absmiddle"> <label for="meritType[0]"><?=$i_Merit_Award?></label>
											<input name="meritType" type="radio" id="meritType[1]" value="-1" >
											<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_demerit.gif" width="20" height="20" border="0" align="absmiddle"> <label for="meritType[1]"><?=$i_Merit_Punishment?> </label>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
										<td>&nbsp;</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td align="right">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center">
												<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.action='ranking_report_view.php'")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
			            </td>
			          </tr>
			        </table>
		        </td>
		    </tr>
			</table><br>
			
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
</form>

<script>
<!--
function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form");
	
	changeTerm('<?=$SchoolYear?>');
}

init();
//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
