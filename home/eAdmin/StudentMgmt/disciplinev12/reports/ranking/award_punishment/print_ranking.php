<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Ranking_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$academicYearID = Get_Current_Academic_Year_ID();

# School Year Menu #

$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear .= $ldiscipline->getConductSchoolYear($SchoolYear);

$conds .= " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;

if($meritType==1) {
	$meritTitle = $iDiscipline['Awarded'];	
	$conds .= " AND meritType=1";
} else {
	$meritTitle = $iDiscipline['Punished'];	
	$conds .= " AND meritType=-1";
}

$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($semester);

# Semester Menu #
if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $ldiscipline->getAcademicYearNameByYearID($SchoolYear)." ".$semesterText;
		list($startYear, $endYear) = split('-',$SchoolYear);
		//$conds .= " AND a.Year='$SchoolYear'";
		$conds .= " AND a.AcademicYearID=$SchoolYear";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
	}
	$conds .= ($semester != 'WholeYear' && $semester!=0) ? " AND a.Semester='$semester'" : "";
}

	
# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==$rankRange) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

//$criteria = ($rankTarget=='class') ? $i_Discipline_Class : $i_Discipline_Form;
switch($rankTarget) {
	case("class") : $criteria = $i_Discipline_Class; break;
	case("form") : $criteria = $i_Discipline_Form; break;
	case("student") : $criteria = $i_Discipline_Student; break;
	default: break;
}

# form / class
if(is_array($rankTargetDetail)) {
	$rankTargetDetail = $rankTargetDetail;
}
else {
	$rankTargetDetail[0] = $rankTargetDetail;
}
$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
//		$tempConds .= "b.ClassName LIKE '$rankTargetDetail[$i]%'";
		//$tempConds .= " d.ClassLevelID=$rankTargetDetail[$i]";
		$tempConds .= "y.YearID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		//$tempConds .= "b.ClassName='$rankTargetDetail[$i]'";
		$tempConds .= "yc.YearClassID=$rankTargetDetail[$i]";
	}
} else {
	$tempConds .= " a.StudentID IN (".implode(',',$studentID).")";	
}
$conds .= " AND ($tempConds)";


# table content
$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";

if($rankTarget=='form') {
	$sql = "SELECT
				y.YearName as form,
				COUNT(*) as total,
				$clsName as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				INNER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
				LEFT OUTER JOIN
					INTRANET_CLASS as c ON (b.ClassName=c.ClassName)
				LEFT OUTER JOIN 
					INTRANET_CLASSLEVEL as d ON (c.ClassLevelID=d.ClassLevelID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=b.UserID)
				LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$academicYearID)
				LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY form
				ORDER BY total DESC
				LIMIT $rankRange
			";
} else if($rankTarget=='class') {
	$name_field = getNameFieldByLang('b.');
	$sql = "SELECT
				$clsName,
				COUNT(*) as total,
				$name_field as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				INNER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
				LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=b.UserID)
				LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=$academicYearID)
				LEFT OUTER JOIN YEAR y ON (y.YearID=yc.YearID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY $clsName
				ORDER BY total DESC
				LIMIT $rankRange
			";
} else {	# rankTarget == 'student'
	$name_field = getNameFieldByLang('b.');
	$sql = "SELECT
				$name_field as name,
				COUNT(*) as total,
				b.ClassName
			FROM
				DISCIPLINE_MERIT_RECORD as a
				INNER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY b.UserID
				ORDER BY total DESC
				LIMIT $rankRange
			";
}
//echo $sql;

$result = $ldiscipline->returnArray($sql,3);

/*
$tableContent = "<html><head>";
$tableContent .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
$tableContent .= "<title></title>";
//$tableContent .= "<link href='{$PATH_WRT_ROOT}/templates/{$LAYOUT_SKIN}/css/print.css' rel='stylesheet' type='text/css'>";
$tableContent .= "<style type='text/css'>";
$tableContent .= "<!--";
$tableContent .= ".print_hide {display:none;}";
$tableContent .= "-->";
$tableContent .= "</style>";
$tableContent .= "</head><body>";
*/
$tableContent .= "<table width='100%' align='center' class='print_hide'><tr>";
$tableContent .= "<td align='right'>";
$tableContent .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$tableContent .= "</td></tr></table>";
$tableContent .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='result_box'>";
$tableContent .= "<tr><td valign='top'>";
$tableContent .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
$tableContent .= "<tr><td class='tabletext'>{$date} {$i_general_most} {$meritTitle} {$criteria}</td></tr>";

$tableContent .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' class='tableborder_print'>";
$tableContent .= "<tr class='tabletop_print tabletext'>";
$tableContent .= "<td>".$iDiscipline['Rank']."</td>";
$tableContent .= "<td>".$i_Discipline_Class."</td>";
$tableContent .= "<td>".$i_Discipline_No_of_Records."</td>";
$tableContent .= "</tr>";

if(sizeof($result)==0) {
	$tableContent .= "<tr class='row_print'>";
	$tableContent .= "<td class='tabletext row_print' colspan='3' align='center' height='40'>{$i_no_record_exists_msg}</td>";
	$tableContent .= "</tr>";
}

for($i=0;$i<sizeof($result);$i++) {
	$k = $i+1;
	$css = ($i%2)+1;
	$tableContent .= "<tr class='row_print'>";
	$tableContent .= "<td class='tabletext row_print'>{$k}</td>";
	$tableContent .= "<td class='tabletext row_print'>";
	//$tableContent .= ($rankTarget=='form') ? "Form " : "";
	$tableContent .= "{$result[$i][0]}</td>";
	$tableContent .= "<td class='tabletext row_print'>{$result[$i][1]}</td>";
	$tableContent .= "</tr>";
}
$tableContent .= "</table>";
$tableContent .= "</td>";
$tableContent .= "</tr>";
$tableContent .= "</table>";
$tableContent .= "</body></html>";

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();

echo $tableContent;

?>
