<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

if($rankTarget == '') {
	header("Location: index.php");	
}

# School Year Menu #
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange='changeTerm(this.value)'", "", $SchoolYear);


$conds .= " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;


$meritTitle = $i_Discipline_GoodConduct." & ".$i_Discipline_Misconduct;

$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($semester);

# Semester Menu #
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $ldiscipline->getAcademicYearNameByYearID($SchoolYear)." ".$semesterText;
		list($startYear, $endYear) = split('-',$SchoolYear);
		//$conds .= " AND a.Year='$SchoolYear'";
		$conds .= " AND a.AcademicYearID=$SchoolYear";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
	}
	$conds .= ($semester != 'WholeYear' && $semester!=0) ? " AND a.Semester='$semester'" : "";
}
	
$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1);
$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT('',1);
$good_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(1,0); # "Homework" subjects not included
$mis_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1,1);


for ($i = 0; $i < sizeof($good_cats); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$good_cats[$i][0]."\"";
	if($typeBy=='byCategory' && sizeof($selectGoodCat)>0) {
		if(in_array($good_cats[$i][0], $selectGoodCat)) {
			$selectGoodCatHTML .= " selected";
		}
	}
	$selectGoodCatHTML .= ">".$good_cats[$i][1]."</option>\n";
}

for ($i = 0; $i < sizeof($mis_cats); $i++) {
	$selectMisCatHTML .= "<option value=\"".$mis_cats[$i][0]."\"";
	if($typeBy=='byCategory' && sizeof($selectMisCat)>0) {
		if(in_array($mis_cats[$i][0], $selectMisCat)) {
			$selectMisCatHTML .= " selected";
		}
	}
	$selectMisCatHTML .= ">".$mis_cats[$i][1]."</option>\n";
}



# Good Conduct Item
$selectGoodItemIDHTML = "<select name=\"ItemID1[]\" multiple size=\"5\" id=\"selectGoodItem\">\n";
for ($i = 0; $i < sizeof($good_items); $i++) {
	list($r_catID, $r_itemID, $r_itemName) = $good_items[$i];
		$selectGoodItemIDHTML .= "<option value=\"".$r_itemID."\"";
		if($typeBy=='byItem' && sizeof($ItemID1)>0) {
			if (in_array($r_itemID, $ItemID1)) {
				$selectGoodItemIDHTML .= " selected";
			}
		}
		$selectGoodItemIDHTML .= ">".$r_itemName."</option>\n";
}
$selectGoodItemIDHTML .= "</select>\n";

# Misconduct Item
$selectMisItemIDHTML = "<select name=\"ItemID2[]\" multiple size=\"5\" id=\"selectMisItem\">\n";
for ($i = 0; $i < sizeof($mis_items); $i++) {
	list($r_catID, $r_itemID, $r_itemName) = $mis_items[$i];
		$selectMisItemIDHTML .= "<option value=\"".$r_itemID."\"";
		if($typeBy=='byItem' && sizeof($ItemID2)>0) {
			if (in_array($r_itemID, $ItemID2)) {
				$selectMisItemIDHTML .= " selected";
			}
		}
		$selectMisItemIDHTML .= ">".$r_itemName."</option>\n";
}
$selectMisItemIDHTML .= "</select>\n";

# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==$rankRange) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

# Rank Target
$rankTargetMenu .= "<select name='rankTarget' id='rankTarget' onChange=\"showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');} \">";
$rankTargetMenu .= "<option value='#'>-- $i_general_please_select --</option>";
$rankTargetMenu .= "<option value='form'";
$rankTargetMenu .= ($rankTarget=='form') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Form</option>";
$rankTargetMenu .= "<option value='class'";
$rankTargetMenu .= ($rankTarget=='class') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Class</option>";
$rankTargetMenu .= "<option value='student'";
$rankTargetMenu .= ($rankTarget=='student') ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Student</option>";
$rankTargetMenu .= "</select>";

# form / class

if(is_array($rankTargetDetail)) {
	$rankTargetDetail = $rankTargetDetail;
}
else {
	$rankTargetDetail[0] = $rankTargetDetail;
}

$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= "y.YearID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= " yc.YearClassID=$rankTargetDetail[$i]";
	}
} else {
	$tempConds .= " a.StudentID IN (".implode(',',$studentID).")";	
}
$conds .= " AND ($tempConds)";


if($typeBy=='byCategory') {
	if(is_array($selectGoodCat) && is_array($selectMisCat))
		$categoryID = array_merge($selectGoodCat, $selectMisCat);
	else if(is_array($selectGoodCat))
		$categoryID = $selectGoodCat;
	else 
		$categoryID = $selectMisCat;
}
if($typeBy=='byItem') {
	if(is_array($ItemID1) && is_array($ItemID2))
		$ItemID = array_merge($ItemID1, $ItemID2);
	else if(is_array($ItemID1))
		$ItemID = $ItemID1;
	else 
		$ItemID = $ItemID2;
}


if($typeBy=='byCategory' && sizeof($categoryID)>0)
	$conds .= " AND a.CategoryID IN (".implode(',',$categoryID).")";
	
$selectedItemTextAry = explode(',',$SelectedItemText);

if($typeBy=='byItem' && sizeof($ItemID)>0) {
	
	for($i=0; $i<sizeof($ItemID); $i++) {
		$itemData = $ldiscipline->getConductItemDataByID($ItemID[$i], $selectedItemTextAry[$i]);
		$tempConds .= ($tempConds!='') ? " OR (a.ItemID='".$ItemID[$i]."' AND a.CategoryID='".$itemData['ID']."')" : " (a.ItemID='".$ItemID[$i]."' AND a.CategoryID='".$itemData['ID']."')";
		
	}
	
	if($tempConds!="")
		$conds .= " AND (".$tempConds.")";
	
}
$jsRankTargetDetail = implode(',', $rankTargetDetail);

# Group By
if($rankTarget=="form") 
	$groupBy = "form";
else if($rankTarget=="class")
	$groupBy = "name";
else 
	$groupBy = "b.UserID";
	
if($typeBy=='byType')
	$groupBy .= "";
else if($typeBy=='byCategory')	
	$groupBy .= ", a.CategoryID ";
else 
	$groupBy .= ", a.ItemID ";
	

switch($rankTarget) {
	case("class") : $criteria = $i_Discipline_Class; break;
	case("form") : $criteria = $i_Discipline_Form; break;
	case("student") : $criteria = $i_Discipline_Student; break;
	default: break;
}	


# table Top
$tableTop .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$tableTop .= "<tr class='tablebluetop'>";
$tableTop .= "<td width='20%' class='tabletopnolink'>{$iDiscipline['Rank']}</td>";
$tableTop .= "<td width='40%' class='tabletopnolink'>{$criteria} </td>";
$tableTop .= "<td width='40%' class='tabletopnolink'>{$i_Discipline_No_of_Records}</td>";
$tableTop .= "</tr>";

# table bottom
$tableBottom .= "</table>";

# table no record
$tableNoContent .= $tableTop;
$tableNoContent .= "<tr class='tablebluerow1'>";
$tableNoContent .= "<td class='tabletext' colspan='3' height='40' align='center'>{$i_no_record_exists_msg}</td>";
$tableNoContent .= "</tr>";
$tableNoContent .= $tableBottom;

if($typeBy=='byType') {
	
	for($i=0; $i<sizeof($record_type); $i++) {
	
		$tempConds = ($record_type[$i]==1) ? " AND a.RecordType=1" : " AND (a.RecordType=-1 OR a.RecordType IS NULL)";

		$temp = $ldiscipline->getGMRankingData('type', $record_type[$i], $conds.$tempConds, $groupBy);
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		$tableContent .= ($record_type[$i]==1) ? "<br>".$i_Discipline_GoodConduct : "<br>".$i_Discipline_Misconduct;
		
		if(sizeof($temp)>0) {
			$tableContent .= $tableTop;
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				$k = $j+1;
				$css = ($j%2)+1;
				$tableContent .= "<tr class='tablebluerow{$css}'>";
				$tableContent .= "<td class='tabletext' width='20%'>{$k}</td>";
				$tableContent .= "<td width='40%'>";
				$tableContent .= "{$showName}</td>";
				$tableContent .= "<td width='40%'>{$temp[$j][1]}</td>";
				$tableContent .= "</tr>";
			}
			$tableContent .= $tableBottom;
		} else {
			$tableContent .= $tableNoContent;	
		}
	}
	
} else if($typeBy=='byCategory'){
	
	//$selectedCategory = array_merge($selectGoodCat, $selectMisCat);
	if(is_array($selectGoodCat) && is_array($selectMisCat))
		$selectedCategory = array_merge($selectGoodCat, $selectMisCat);
	else if(is_array($selectGoodCat))
		$selectedCategory = $selectGoodCat;
	else 
		$selectedCategory = $selectMisCat;
	
	for($i=0; $i<sizeof($selectedCategory); $i++) {
		$temp = $ldiscipline->getGMRankingData('category', $selectedCategory[$i], $conds, $groupBy);
		
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		
		$categoryData = $ldiscipline->getGMCategoryInfoByCategoryID($selectedCategory[$i]);
		$meritTypeByCat = $categoryData['MeritType'];
		//debug_pr($categoryData);
		
		$tableContent .= ($meritTypeByCat==1) ? "<br>".$i_Discipline_GoodConduct : "<br>".$i_Discipline_Misconduct;
		
		$GMCategory = $ldiscipline->getGMCategoryInfoByCategoryID($selectedCategory[$i]);
		
		$tableContent .= " > ".$GMCategory['Name']."<br>";
		
		if(sizeof($temp)>0) {
			$tableContent .= $tableTop;
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				
				$k = $j+1;
				$css = ($j%2)+1;
				$tableContent .= "<tr class='tablebluerow{$css}'>";
				$tableContent .= "<td class='tabletext' width='20%'>{$k}</td>";
				$tableContent .= "<td width='40%'>";
				$tableContent .= "{$showName}</td>";
				$tableContent .= "<td width='40%'>{$temp[$j][1]}</td>";
				$tableContent .= "</tr>";
			}
			$tableContent .= $tableBottom;
		} else {
			$tableContent .= $tableNoContent;	
		}
	}		
	
} else if($typeBy=='byItem'){
	
	if(sizeof($ItemID1)==0) $ItemID1 = array();
	if(sizeof($ItemID2)==0) $ItemID2 = array();
	
	$selectedItem = array_merge($ItemID1, $ItemID2);
	//$selectedItemText = explode(',',$SelectedItemText);
	
	
	for($i=0; $i<sizeof($selectedItem); $i++) {
		$temp = $ldiscipline->getGMRankingData('item', $selectedItem[$i], $conds, $groupBy);
		
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[0];
		
		$itemData = $ldiscipline->getConductItemDataByID($selectedItem[$i], $selectedItemTextAry[$i]);		
		$meritTypeByItem = $itemData['RecordType'];
		
		$tableContent .= ($meritTypeByItem==1) ? "<br>".$i_Discipline_GoodConduct : "<br>".$i_Discipline_Misconduct;
			
		$GMItem = $ldiscipline->getGMItemInfoByCategoryID($selectedItem[$i], $selectedItemTextAry[$i]);
		
        if(sizeof($GMItem)==0) {
                $subjectName = " > ".$ldiscipline->RETURN_CONDUCT_REASON($selectedItem[$i], 2);
        }
        $tableContent .= ($subjectName) ? $subjectName."<br>" : (" > ".$GMItem['CategoryName']." > ".$GMItem['ItemName']."<br>");

		
		if(sizeof($temp)>0) {
			$tableContent .= $tableTop;
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				
				$k = $j+1;
				$css = ($j%2)+1;
				$tableContent .= "<tr class='tablebluerow{$css}'>";
				$tableContent .= "<td class='tabletext' width='20%'>{$k}</td>";
				$tableContent .= "<td width='40%'>";
				$tableContent .= "{$showName}</td>";
				$tableContent .= "<td width='40%'>{$temp[$j][1]}</td>";
				$tableContent .= "</tr>";
			}
			$tableContent .= $tableBottom;
		} else {
			$tableContent .= $tableNoContent;	
		}
		
	}	
		
}



# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);

$reportOptionContent .= "<table width='88%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr><td>";
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td height='30' colspan='6'>";
$reportOptionContent .= "<input name='dateChoice' type='radio' id='dateChoice[0]' value='1'";
$reportOptionContent .= ($dateChoice==1) ? " checked" : "";
$reportOptionContent .= ">$i_Discipline_School_Year";
//$reportOptionContent .= "<select name='SchoolYear' id='SchoolYear' onFocus=\"document.form1.dateChoice[0].checked=true\" onChange=\"changeTerm(this.value)\">";
$reportOptionContent .= $selectSchoolYearHTML;
//$reportOptionContent .= "</select>";
$reportOptionContent .= $i_Discipline_Semester;
$reportOptionContent .= "<span id='spanSemester'><select name='semester' id='semester' onFocus=\"document.form1.dateChoice[0].checked=true\" >";
$reportOptionContent .= $SemesterMenu;
$reportOptionContent .= "</select></span></td></tr>";
$reportOptionContent .= "<tr><td><input name='dateChoice' type='radio' id='dateChoice[1]' value='2'";
$reportOptionContent .= ($dateChoice==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td><!--<input name='startDate' type='text' class='tabletext' value='$startDate' onFocus=\"document.form1.dateChoice[1].checked=true\" />--></td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true' onFocus='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("startDate",$startDate);
$reportOptionContent .= "</td><td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td><!--<input name='endDate' type='text' class='tabletext' value='$endDate' onFocus=\"document.form1.dateChoice[1].checked=true\" />--></td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true' onFocus='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("endDate",$endDate);
$reportOptionContent .= "</td></tr></table></td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingTarget']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='0%' border='0' cellspacing='2' cellpadding='0'>";
$reportOptionContent .= "<tr><td valign='top'>";
$reportOptionContent .= $rankTargetMenu;
$reportOptionContent .= "</td><td><div id='rankTargetDetail' style='position:absolute; width:280px; height:100px; z-index:0;'></div>";
$reportOptionContent .= "<select name='rankTargetDetail[]' id='rankTargetDetail[]' multiple size='5'></select><br>";
$reportOptionContent .= $linterface->GET_BTN($button_select_all, 'button', "javascript:SelectAll(this.form.elements['rankTargetDetail[]']);return false;", "selectAllBtn01");
$reportOptionContent .= "<td><div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:0;display:";
$reportOptionContent .= ($rankTarget=='student') ? "inline" : "none";
$reportOptionContent .= ";'><select name='studentID[]' id='studentID[]' multiple size='5'>
								</select>
									<br>".$linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(this.form.elements['studentID[]'], true);return false;")."
								</div>
							</td>";
$reportOptionContent .= "</tr><tr><td colspan='3' class='tabletextremark'>({$i_Discipline_Press_Ctrl_Key})</td></tr></table></td></tr>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RecordType']}<span class='tabletextrequire'>*</span>";
$reportOptionContent .= "</td>";
$reportOptionContent .= "<td><input name='typeBy' type='radio' id='typeBy[0]' value='byType'";
$reportOptionContent .= ($typeBy=='byType') ? " checked" : "";
$reportOptionContent .= " onClick=\"javascript:hideSpan('spanCategory');hideSpan('spanItem');showSpan('spanRecordType');document.getElementById('record_type_goodconduct').checked=true;\"";
$reportOptionContent .= "><label for='typeBy[0]'>".$iDiscipline['ReceiveType']."</label>";
$reportOptionContent .= "<input name='typeBy' type='radio' id='typeBy[1]' value='byCategory'";
$reportOptionContent .= ($typeBy=='byCategory') ? " checked" : "";
$reportOptionContent .= " onClick=\"javascript:hideSpan('spanItem');hideSpan('spanRecordType');showSpan('spanCategory');document.getElementById('record_type_good').checked=true;document.getElementById('record_type_mis').checked=true;changeCat('0');SelectAllItem(this.form.elements['selectGoodCat'], true);SelectAllItem(this.form.elements['selectMisCat'], true);\"><label for='typeBy[1]'>".$iDiscipline['byCategory']."</label>";
$reportOptionContent .= "<input name='typeBy' type='radio' id='typeBy[2]' value='byItem'";
$reportOptionContent .= ($typeBy=='byItem') ? " checked" : "";
$reportOptionContent .= " onClick=\"javascript:hideSpan('spanCategory');hideSpan('spanRecordType');showSpan('spanItem');document.getElementById('record_type_good2').checked=true;document.getElementById('record_type_mis2').checked=true;changeCat('0');SelectAllItem(this.form.elements['selectGoodItem'], true);SelectAllItem(this.form.elements['selectMisItem'], true);\"";
$reportOptionContent .= "><label for='typeBy[2]'>".$iDiscipline['byItem']."</label>";
$reportOptionContent .= "</td></tr>";
$reportOptionContent .= "<tr>
						<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle\">&nbsp;</td>
						<td>
							<span id=\"spanRecordType\" style=\"display:";
$reportOptionContent .= ($typeBy=='byType') ? "inline" : "none";
$reportOptionContent .= "\">
								<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
									<tr>
										<td width=\"95%\">
											<table width=\"0%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
												<tr>
													<td valign=\"top\">
														<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
															<tr>
																<td>
																	<input name=\"record_type[]\" type=\"checkbox\" id=\"record_type_goodconduct\" value=\"1\" ";
$reportOptionContent .= ($typeBy=='byType' && in_array(1,$record_type)) ? " checked " : "";
$reportOptionContent .= "><label for=\"record_type_goodconduct\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_gd_conduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_GoodConduct</label>
																	<input name=\"record_type[]\" type=\"checkbox\" id=\"record_type_misconduct\" value=\"-1\" ";
$reportOptionContent .= ($typeBy=='byType' && in_array(-1,$record_type)) ? " checked " : "";
$reportOptionContent .= "><label for=\"record_type_misconduct\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_misconduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Misconduct</label>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<input type=\"hidden\" name=\"selectShowOnlyType\" id=\"selectShowOnlyType\" value=\"ALL\">
								</table>
							</span>
							<span id=\"spanCategory\" style=\"display:";
$reportOptionContent .= ($typeBy=='byCategory') ? "inline" : "none";
$reportOptionContent .= "\">
								<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
									<tr>
										<td width=\"95%\">
											<table width=\"0%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
												<tr>
													<td valign=\"top\">
														<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
															<tr>
																<td>
																	<table>
																		<tr>
																			<td>
																				<input name=\"record_type[]\" type=\"checkbox\" id=\"record_type_good\" value=\"1\" ";
$reportOptionContent .= ($typeBy=='byCategory' && in_array(1,$record_type)) ? " checked " : "";
$reportOptionContent .= "onClick=\"showSpan('spanMisCat');showSpan('spanGoodCat');if(document.getElementById('record_type_good').checked==true){SelectAllItem(this.form.elements['selectGoodCat'], true);} else {SelectAllItem(this.form.elements['selectGoodCat'], false);} \"><label for=\"record_type_good\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_gd_conduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_GoodConduct</label>
																			</td>
																			<td>
																				<input name=\"record_type[]\" type=\"checkbox\" id=\"record_type_mis\" value=\"-1\" ";
$reportOptionContent .= ($typeBy=='byCategory' && in_array(-1,$record_type)) ? " checked " : "";
$reportOptionContent .= "onClick=\"showSpan('spanGoodCat');showSpan('spanMisCat');if(document.getElementById('record_type_mis').checked==true){SelectAllItem(this.form.elements['selectMisCat'], true);} else {SelectAllItem(this.form.elements['selectMisCat'], false);} \"><label for=\"record_type_mis\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_misconduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Misconduct</label><br />
																			</td>
																			</tr><tr>
																			<td>
																				<span id=\"spanGoodCat\" display:none\"\">
																					<select name=\"selectGoodCat[]\" multiple onChange=\"changeCat(this.value);\" id=\"selectGoodCat\">
																					$selectGoodCatHTML
																					</select>
																				</span>
																			</td><td>
																				<span id=\"spanMisCat\" \"display:none\"\">
																					<select name=\"selectMisCat[]\" multiple onChange=\"changeCat(this.value);\" id=\"selectMisCat\">
																					$selectMisCatHTML
																					</select>
																				</span>
																			</td>
																		</tr>
																		<tr>
																			<td>".$linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectGoodCat'], true); document.getElementById('record_type_good').checked=true;return false;")."</td>
																			<td>".$linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectMisCat'], true); document.getElementById('record_type_mis').checked=true;return false;")."</td>
																		</tr>
																		<tr><td colspan=\"2\" class=\"tabletextremark\">($i_Discipline_Press_Ctrl_Key)</td></tr>
																	</table>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<input type=\"hidden\" name=\"selectShowOnlyType\" id=\"selectShowOnlyType\" value=\"ALL\">
								</table>
							</span>
							<span id=\"spanItem\" style=\"display:";
$reportOptionContent .= ($typeBy=='byItem') ? "inline" : "none";
$reportOptionContent .= "\"><table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
									<tr>
										<td width=\"95%\">
											<table width=\"0%\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">
												<tr>
													<td valign=\"top\">
														<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
															<tr>
																<td>
																	<input name=\"record_type[]\" type=\"checkbox\" id=\"record_type_good2\" value=\"1\" ";
$reportOptionContent .= ($typeBy=='byItem' && in_array(1,$record_type)) ? " checked " : "";
$reportOptionContent .= " onClick=\"if(document.getElementById('record_type_good2').checked){SelectAllItem(this.form.elements['selectGoodItem'], true);} else {SelectAllItem(this.form.elements['selectGoodItem'], false);}\"><label for=\"record_type_good2\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_gd_conduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> $i_Discipline_GoodConduct</label>
																</td>
																<td>
																	<input name=\"record_type[]\" type=\"checkbox\" id=\"record_type_mis2\" value=\"-1\" ";
$reportOptionContent .= ($typeBy=='byItem' && in_array(-1,$record_type)) ? " checked " : "";
$reportOptionContent .= "onClick=\"if(document.getElementById('record_type_mis2').checked){SelectAllItem(this.form.elements['selectMisItem'], true);} else {SelectAllItem(this.form.elements['selectMisItem'], false);}\"><label for=\"record_type_mis2\"><img src=\"$image_path/$LAYOUT_SKIN/ediscipline/icon_misconduct.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\"> $i_Discipline_Misconduct</label>
																</td>
															</tr>
															<tr>
																<td>
																	$selectGoodItemIDHTML
																</td>
																<td>
																	$selectMisItemIDHTML
																</td>
															</tr>
															<tr>
																<td>".$linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectGoodItem'], true); document.getElementById('record_type_good2').checked=true;return false;")."</td>
																<td>".$linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectMisItem'], true); document.getElementById('record_type_mis2').checked=true; return false;")."</td>
															</tr>
															<tr>
																<td colspan=\"2\">
																	<span class=\"tabletextremark\">($i_Discipline_Press_Ctrl_Key)</span>
																</td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<input type=\"hidden\" name=\"selectShowOnlyType\" id=\"selectShowOnlyType\" value=\"ALL\">
								</table>
							</span>
						</td>
					</tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingRange']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td>{$i_general_highest} <select name='rankRange' id='rankRange'>";
$reportOptionContent .= $rankRangeMenu;
$reportOptionContent .= "</select></td></tr>";
$reportOptionContent .= "<tr><td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td align='center'>";
$reportOptionContent .= "</td></tr><tr><td align='center' colspan='2'>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.target='';document.form1.action='ranking_report_view.php'");
$reportOptionContent .= "</td></tr></table>";


# menu highlight setting
$TAGS_OBJ[] = array($eDiscipline['GoodconductMisconductRanking'], "");

$CurrentPageArr['eDisciplinev12'] = 1;

$CurrentPage = "GoodconductMisconductRanking";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$CurrentPageArr['eDisciplinev12'] = 1;

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function SelectAllItem(obj, flag){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function showSpan(span){
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
		form1.elements['rankTargetDetail[]'].multiple = true;
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.getElementById('showReportOption').value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('showReportOption').value = '0';
}

function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}
	if(form1.rankTarget.value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;

	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}	 
	
	if(form1.typeBy[0].checked==true && form1.record_type_goodconduct.checked==false && form1.record_type_misconduct.checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["SelectRecordCategory"]?>");	
		return false;
	}
	if(form1.typeBy[1].checked==true && form1.record_type_good.checked==false && form1.record_type_mis.checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["SelectRecordCategory"]?>");	
		return false;
	}
	if(form1.typeBy[2].checked==true) {
		if(form1.record_type_good2.checked==false && form1.record_type_mis2.checked==false) {
			alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["SelectRecordCategory"]?>");		
			return false;
		}
		var item1Length = document.getElementById('selectGoodItem');
		var item2Length = document.getElementById('selectMisItem');
		
		choiceSelected = 0;
		for(i=0;i<item1Length.length;i++) {
			if(item1Length.options[i].selected==true)	
				choiceSelected = 1;
		}
		for(i=0;i<item2Length.length;i++) {
			if(item2Length.options[i].selected==true)	
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["Award_Punishment_RecordItem"]?>");
			return false;
		}
	}
	
	getSelectedTextString(document.getElementById('selectGoodItem'), 'ItemText1');
	getSelectedTextString(document.getElementById('selectMisItem'), 'ItemText2');
	document.getElementById("SelectedItemText").value = document.getElementById('ItemText1').value;
	document.getElementById("SelectedItemText").value += (document.getElementById('ItemText1').value && document.getElementById('ItemText2').value) ? "," : "";
	document.getElementById("SelectedItemText").value += document.getElementById('ItemText2').value;

}


/*
function goCheck(form1) {
	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}

	var choiceSelected = 0;
	var choice = document.getElementById('rankTargetDetail[]');

	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}
	
	if(form1.meritType[0].checked==false && form1.meritType[1].checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RecordType']?>");
		return false;
	}
	getSelectedTextString(document.getElementById('selectGoodItem'), 'ItemText1');
	getSelectedTextString(document.getElementById('selectMisItem'), 'ItemText2');
	alert(ItemID1);
	alert(ItemID2);
	//document.getElementById("selectedItemText").value = document.getElementById("");
	//reture true;

}
*/
function showDiv(div)
{
	document.getElementById(div).style.display = "inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display = "none";
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
//-->
</script>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp
var xmlHttp2

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "get_live2.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+document.getElementById('SchoolYear').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
	url += (document.getElementById("studentFlag").value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";
	
	xmlHttp.onreadystatechange = stateChanged 
	
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	//SelectAll(form1.elements['rankTargetDetail[]']);
} 



function stateChanged() 
{ 

	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
		if(document.getElementById("studentFlag").value == 0)
			if(document.getElementById('rankTarget').value != 'student') {
				//SelectAll(form1.elements['rankTargetDetail[]']);
				//document.getELementById('selectAllBtn01').display:none;
			}
	} 
	//document.getElementById("studentFlag").value = 0;
}


// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($good_cats as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($mis_cats as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

function changeCat(value)
{
	<?
		$curr_cat_id = "";
		$pos = 0;
			for ($i=0; $i<sizeof($good_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $good_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "document.getElementById('selectGoodItem').options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
			
		$pos = 0;
			for ($i=0; $i<sizeof($mis_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $mis_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "document.getElementById('selectMisItem').options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td>
						<table border="0" cellspacing="5" cellpadding="5" align="center">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$date?> <?=$i_general_most?> <?=$meritTitle?> <?=$criteria?></td>
							</tr>
						</table>
						<br>
						<?=$tableContent?>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.target='_self';document.form1.action='export_ranking.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_print, "submit", "document.form1.target='_blank';document.form1.action='print_ranking.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>
<input type="hidden" name="showReportOption" id="showReportOption" value="0">
<input type="hidden" name="studentFlag" id="studentFlag" value="<? if(sizeof($studentID)>0){echo "1";} else { echo "0";} ?>">			
<input type="hidden" name="ItemText1" id="ItemText1" />
<input type="hidden" name="ItemText2" id="ItemText2" />
<input type="hidden" name="SelectedItemText" id="SelectedItemText" />
</form>
<script language="javascript">
<!--
showResult("<?=$rankTarget?>", "<?=$jsRankTargetDetail?>");

<?
if(sizeof($studentID)>0) 
	echo "initialStudent()\n";	
?>

function initialStudent() {
	//document.getElementById("studentFlag").value = 1;
	xmlHttp2 = GetXmlHttpObject()
	url = "get_live2.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID)?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
		//SelectAll(form1.elements['studentID[]']);
	} 
}

changeTerm('<?=$SchoolYear?>');
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
