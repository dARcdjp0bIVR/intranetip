<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-STAT-Award_Punishment-View");

$CurrentPage = "Statistics_AwardPunishment";
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment_Statistics'], "", 1);

$lclass = new libclass();
$linterface = new interface_html();

// School year
$selectYear = ($selectYear == '') ? getCurrentAcademicYear() : $selectYear;
$selectSchoolYearHTML = "<select name=\"selectYear\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSchoolYearHTML .= "<option value=\"$i_Discipline_System_Award_Punishment_All_School_Year\"";
$selectSchoolYearHTML .= ($selectYear!=$i_Discipline_System_Award_Punishment_All_School_Year) ? "" : " selected";
$selectSchoolYearHTML .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYearHTML .= $ldiscipline->getConductSchoolYear($selectYear);
$selectSchoolYearHTML .= "</select>";


$meritDemerit[] = "";


// Semester
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "<option value=\"$i_Discipline_System_Award_Punishment_Whole_Year\"";
$selectSemesterHTML .= ($selectSemester!=$i_Discipline_System_Award_Punishment_Whole_Year) ? "" : " selected";
$selectSemesterHTML .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++) {
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$selectSemesterHTML .= "<option value=\"".$name."\"";
	if ($selectSemester == "") {		// first time
		if($name==$currentSemester) { $selectSemesterHTML .= " selected"; }
	} else {		// submit from form
		if($name==$selectSemester) { $selectSemesterHTML .= " selected"; }
	}
	$selectSemesterHTML .= ">".$name."</option>";
}
$selectSemesterHTML .= "</select>";

// Form
$ClassLvlArr = $lclass->getLevelArray();
$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",", $SelectedFormText);
$selectFormHTML = "<select name=\"selectForm[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$selectFormHTML .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassLvlArr[$i][0], $SelectedFormArr)) {
		$selectFormHTML .= " selected";
	}
	$selectFormHTML .= ">".$ClassLvlArr[$i][1]."</option>\n";
}
$selectFormHTML .= "</select>\n";

// Class
$ClassListArr = $lclass->getClassList();
$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);
$selectClassHTML = "<select name=\"selectClass[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($ClassListArr); $i++) {
	$selectClassHTML .= "<option value=\"".$ClassListArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($ClassListArr[$i][0], $SelectedClassArr)) {
		$selectClassHTML .= " selected";
	}
	$selectClassHTML .= ">".$ClassListArr[$i][1]."</option>\n";
}
$selectClassHTML .= "</select>\n";

// No. of Merits & Demerits
$merit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(1);
$demerit_record_type = $ldiscipline->returnValidMeritTypeFieldWithOrder(-1);
$MeritDemeritArr = array_merge($merit_record_type, $demerit_record_type);
$SelectedMeritDemeritArr = explode(",", $SelectedMeritDemerit);
$SelectedMeritDemeritTextArr = explode(",", $SelectedMeritDemeritText);
$selectMeritDemeritHTML = "<select name=\"selectMeritDemerit[]\" multiple size=\"5\">\n";
for ($i = 0; $i < sizeof($MeritDemeritArr); $i++) {
	$selectMeritDemeritHTML .= "<option value=\"".$MeritDemeritArr[$i][0]."\"";
	if ($_POST["submit_flag"] != "YES" || in_array($MeritDemeritArr[$i][0], $SelectedMeritDemeritArr)) {
		$selectMeritDemeritHTML .= " selected";
	}
	$selectMeritDemeritHTML .= ">".$MeritDemeritArr[$i][1]."</option>\n";
}
$selectMeritDemeritHTML .= "</select>\n";

// Get Category selection
$merit_cats = $ldiscipline->returnMeritItemCategoryByType(1);
$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);
$merit_item = $ldiscipline->returnMeritItemByType(1);
$demerit_item = $ldiscipline->returnMeritItemByType(-1);

$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat($record_type);

// Award Category
$selectMeritCatHTML = "<select name=\"selectMeritCat\" onChange=\"changeCat(this.value);\">\n";
$selectMeritCatHTML .= "<option value=\"0\"";
$selectMeritCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectMeritCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Awards." --</option>";
for ($i = 0; $i < sizeof($merit_cats); $i++) {
	$selectMeritCatHTML .= "<option value=\"".$merit_cats[$i][0]."\"";
	if ($selectMeritCat == $merit_cats[$i][0]) {
		$selectMeritCatHTML .= " selected";
	}
	$selectMeritCatHTML .= ">".$merit_cats[$i][1]."</option>\n";
}
$selectMeritCatHTML .= "</select>\n";

// Punishment Category
$selectDemeritCatHTML = "<select name=\"selectDemeritCat\" onChange=\"changeCat(this.value);\">\n";
$selectDemeritCatHTML .= "<option value=\"0\"";
$selectDemeritCatHTML .= ($_POST["submit_flag"] != "YES")?" selected":"";
$selectDemeritCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Punishment." --</option>";
for ($i = 0; $i < sizeof($demerit_cats); $i++) {
	$selectDemeritCatHTML .= "<option value=\"".$demerit_cats[$i][0]."\"";
	if ($selectDemeritCat == $demerit_cats[$i][0]) {
		$selectDemeritCatHTML .= " selected";
	}
	$selectDemeritCatHTML .= ">".$demerit_cats[$i][1]."</option>\n";
}
$selectDemeritCatHTML .= "</select>\n";

// Items
$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);
$selectItemIDHTML = "<select name=\"ItemID[]\" multiple size=\"5\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($items); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $items[$i];
		$r_itemName = intranet_htmlspecialchars($r_itemName);
		$r_itemName = str_replace('"', '\"', $r_itemName);
		$r_itemName = str_replace("'", "\'", $r_itemName);
		//$r_itemName = str_replace('"', '&quot;', $r_itemName);
		
		if (($record_type == 1 && $selectMeritCat == $r_catID) || ($record_type == -1 && $selectDemeritCat == $r_catID) || ($selectMeritCat==0 && $selectDemeritCat==0)) {
		//if (($record_type == 1 && $selectMeritCat == $r_catID) || ($record_type == -1 && $selectDemeritCat == $r_catID) || ($categoryID==0)) {

			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			
			if (in_array($r_itemID, $SelectedItemIDArr)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".str_replace("\'","'",str_replace('\"','"',$r_itemName))."</option>\n";
		} 
	}
}
$selectItemIDHTML .= "</select>\n";


// Submit from form
if ($_POST["submit_flag"] == "YES") {
	
	if($radioType=="AP_TITLES" && $record_type=="1") {
		$categoryID = $selectMeritCat;
	} else if($radioType=="AP_TITLES" && $record_type=="-1") {
		$categoryID = $selectDemeritCat;
	} 
	
	$optionString = "<td id=\"tdOption\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";
	if ($Period == "YEAR") {
		$parPeriodField1 = $selectYear;
		$parPeriodField2 = $selectSemester;
	} else {
		$parPeriodField1 = $textFromDate;
		$parPeriodField2 = $textToDate;
	}

	if ($selectBy == "FORM") {
		$parByFieldArr = $SelectedFormTextArr;
	} else {
		$parByFieldArr = $SelectedClassTextArr;
	}

	if ($StatType == "NO_OF_MD") {
		//$parStatsFieldArr = $SelectedMeritDemeritArr;
		//$parStatsFieldTextArr = $SelectedMeritDemeritTextArr;
		for($i=0;$i<sizeof($MeritDemeritArr);$i++) {
			$all_meritDemerit[$i] = $MeritDemeritArr[$i][0];
			$all_meritDemerit_name[$i] = $MeritDemeritArr[$i][1];
		}
		//$parStatsFieldArr = $all_meritDemerit;				# all merit/demerit
		//$parStatsFieldTextArr = $all_meritDemerit_name;
		$parStatsFieldArr = $SelectedMeritDemeritArr;			# selected merit/demerit
		$parStatsFieldTextArr = $SelectedMeritDemeritTextArr;
	} else {
		$parStatsFieldArr = $SelectedItemIDArr;
		$parStatsFieldTextArr = $SelectedItemIDTextArr;
		
	}
	# all merit/demerit
	//$all_item = $ldiscipline->retrieveAllAwardPunishmentItem($categoryID,'id',$record_type);
	//$all_item_name = $ldiscipline->retrieveAllAwardPunishmentItem($categoryID,'name',$record_type);
	
	# selected merit/demerit
	$all_item = $SelectedItemIDArr;
	$all_item_name = $parStatsFieldTextArr;
	
//	$chartDataArr = $ldiscipline->getAwardPunishStatistics($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $selectShowOnlyType, $selectShowOnlyNum);
	$chartDataArr = $ldiscipline->getAwardPunishStatisticsAll($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $selectShowOnlyType, $selectShowOnlyNum, $all_item, 'view', $MeritDemeritArr);
	
##############################################################################################################################
############################################### flash chart ##################################################################
##############################################################################################################################
# flash chart

$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');

$chart = new open_flash_chart();

$tooltip = new tooltip();
$tooltip->set_hover();
$chart->set_tooltip($tooltip);

$maxValue = 0;

$i = 0;

# Item-Colour Mapping - $itemNameColourMappingArr[#ItemName] = #colour
$itemNameColourMappingArr = array();

if($StatType=="NO_OF_MD") {
	foreach ($parStatsFieldTextArr as $statsValue) {	# merit/demerit name
		$statsUnicodeValue = stripslashes(Big5ToUnicode($statsValue));
		$statsValue = stripslashes($statsValue);
		$valueArr = array();
		for($j=0;$j<sizeof($valueArr);$j++) {
			$valueArr[$j] = "";
		}
	
		//$bar = new bar_stack();
		//$bar->set_oneColour( true ); 
		$bar = new bar_glass();
		
//		$bar->set_expandable( false ); 
		$bar->set_colour("$colourArr[$i]");
		# remember the column colour for printing
		$itemNameColourMappingArr[$statsValue] = $colourArr[$i];
		
		$j = 0;
		foreach ($parByFieldArr as $byValue) {		# selected class/form
			$tmpValue = (int)$chartDataArr[$byValue][$statsValue];
			//$valueArr[$j] = $tmpValue;
			$valueArr[] = $tmpValue;
			if ($tmpValue > $maxValue) $maxValue = $tmpValue;
			//$bar->append_stack( $valueArr );
			$bar->set_values( $valueArr );
		}
		
		
		//$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#total#");
		$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#");
		$bar->set_key("$statsUnicodeValue", "12");
		$bar->set_id( $parStatsFieldArr[$i] );
		$flag = 0;
		
		foreach ($SelectedMeritDemeritTextArr as $statsValue2) {
			$statsValue2 = intranet_undo_htmlspecialchars(stripslashes($statsValue2));
			if($flag==0) {
				//echo $statsValue."/".$statsValue2."<br>";
				if($statsValue==$statsValue2) {
					$bar->set_visible( true );
					$flag = 1;
				} else {
					$bar->set_visible( false );
				}
			}
		}
		$chart->add_element($bar);
		$i++;
	}
} else {
//	foreach ($all_item_name as $statsValue) {	# selected item name
	foreach ($parStatsFieldTextArr as $statsValue) {	# selected item name
		$statsUnicodeValue = intranet_undo_htmlspecialchars(stripslashes(Big5ToUnicode($statsValue)));
		$statsValue = stripslashes($statsValue);
		
		$valueArr = array();
		for($j=0;$j<sizeof($valueArr);$j++) {
			$valueArr[$j] = "";
		}
	
		//$bar = new bar_stack();
		//$bar->set_oneColour( true ); 
		$bar = new bar_glass();
		
		$bar->set_expandable( false ); 
		$bar->set_colour("$colourArr[$i]");
		# remember the column colour for printing
		$itemNameColourMappingArr[$statsValue] = $colourArr[$i];
		
		$j = 0;
		foreach ($parByFieldArr as $byValue) {
			$tmpValue = (int)$chartDataArr[$byValue][$statsValue];
			//$valueArr[$j] = $tmpValue;
			$valueArr[] = $tmpValue;
			if ($tmpValue > $maxValue) $maxValue = $tmpValue;
			//$bar->append_stack( $valueArr );
			$bar->set_values( $valueArr );
			
		}
		
		//$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#total#");	# x-axis item 
		$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#");	# x-axis item 
		$bar->set_key("$statsUnicodeValue", "12");
		$bar->set_id( $all_item[$i] );
		$flag = 0;
		foreach ($parStatsFieldTextArr as $statsValue2) {
			$statsValue2 = stripslashes($statsValue2);
			//$statsValue2 = intranet_undo_htmlspecialchars(stripslashes($statsValue2));	# for all AP
			if($flag==0) {
				//if(intranet_undo_htmlspecialchars($statsValue)==$statsValue2) {			# for all AP
				if($statsValue==$statsValue2) {		# for selected AP
					$bar->set_visible( true );
					$flag = 1;
				} else {
					$bar->set_visible( false );
				}
			}
		}
		$chart->add_element($bar);
		$i++;
	}
}

if ($Period == "YEAR") {
	$titleString = $selectYear." ".$selectSemester." ";
} else {
	$titleString = $i_From." ".$textFromDate." ".$i_To." ".$textToDate." ";
}
$titleString .= $eDiscipline['Award_and_Punishment_Statistics'];
$title = new title(Big5ToUnicode($titleString));
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

if ($selectBy=="FORM") {
	$x_legend = new x_legend(Big5ToUnicode($i_Discipline_Form));
} else if ($selectBy=="CLASS") {
	$x_legend = new x_legend(Big5ToUnicode($i_Discipline_Class));
}
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array($parByFieldArr);

$y_legend = new y_legend(Big5ToUnicode($i_Discipline_Quantity));
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );

// $maxY = 5, 10, 20, 30...
if ($maxValue <= 5) {
	$maxY = 5;
} else {
	if (substr($maxValue, strlen($maxValue)-1, 1) == "0") {		// Last digit = 0
		$maxY = $maxValue;
	} else {
		$maxY = (substr($maxValue, 0, strlen($maxValue)-1) + 1)."0";
	}
}

$y->set_range( 0, $maxY, $maxY / 5 );
$y->set_offset(false);

$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );

##############################################################################################################################
############################################ END - flash chart ###############################################################
##############################################################################################################################
/*
# Adjust the height of flash depends on the no. of items
if($StatType=="NO_OF_MD") {
	$height = 320;
} else {
	$height = 300 + (sizeof($all_item)/2) * 20;	
}
*/
$height = 350;

if($StatType=="NO_OF_MD") {
	$width = 850 + (sizeof($parByFieldArr)) * 10;
} else {
	$width = 850 + (sizeof($parStatsFieldTextArr) +sizeof($parByFieldArr)) * 5; 
}

	$chartContent = "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"result_box\">";
	$chartContent .= "<tr><td align=\"center\">";

	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################### flash chart ##################################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n";
	$chartContent .= "<div id=\"my_chart\">no flash?</div>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/json/json2.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/swfobject.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
//	$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart-develop.swf\", \"my_chart\", \"850\", \"350\", \"9.0.0\", \"expressInstall.swf\");\n";
	$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart.swf\", \"my_chart\", \"".$width."\", \"".$height."\", \"9.0.0\", \"expressInstall.swf\");\n";
	$chartContent .= "</script>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	$chartContent .= "function ofc_ready(){\n";
	$chartContent .= "	//alert('ofc_ready');\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function open_flash_chart_data(){\n";
	$chartContent .= "	//alert( 'reading data' );\n";
	$chartContent .= "	return JSON.stringify(data);\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function findSWF(movieName) {\n";
	$chartContent .= "  if (navigator.appName.indexOf(\"Microsoft\")!= -1) {\n";
	$chartContent .= "	return window[movieName];\n";
	$chartContent .= "  } else {\n";
	$chartContent .= "	return document[movieName];\n";
	$chartContent .= "  }\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";

	$chartContent .= "var itemID = new Array(); var newItemID = new Array();\n";
	if($StatType=="NO_OF_MD") {
		$chartContent .= "itemID = [".implode(",",$SelectedMeritDemeritArr)."];\n\n";	
	} else {
		$chartContent .= "itemID = [".implode(",", $parStatsFieldArr)."];\n\n";	
	}
	$chartContent .= "function setChart(id, display) {\n";
	$chartContent .= "var j = 0;\n";
	$chartContent .= "var itemIDList = \"\";\n";
	$chartContent .= "newItemID = [];\n";	# initiate newItemID[]
	$chartContent .= "for(var i=0;i<itemID.length;i++) {\n";
	$chartContent .= "	if(display==true) { \n";
	$chartContent .= "		if(i==0) { newItemID[j] = id;\n j++;\n}\n";
	$chartContent .= "			newItemID[j] = itemID[i];\n j++; \n";
	$chartContent .= "	} else {\n";
	$chartContent .= "		if(id != itemID[i])	{\n";
	$chartContent .= "			newItemID[j] = itemID[i];\n j++;\n";
	$chartContent .= "		}\n";
	$chartContent .= "	}\n";
	$chartContent .= "}\n";
	$chartContent .= "if(itemID.length==0){newItemID[j] = id;\n j++;\n} \n";	
	$chartContent .= "itemID = [];\n";
	$chartContent .= "var j = 0;\n";
	$chartContent .= "for(i=0;i<newItemID.length;i++) {\n";
	$chartContent .= "//	if(newItemID[i] != \"\" && newItemID[i] != \"undefined\") {\n";
	$chartContent .= "		itemID[i] = newItemID[i];\n";
	$chartContent .= "		itemIDList += (itemIDList != \"\") ? \",\" + itemID[i] : itemID[j];\n j++;";
	$chartContent .= "	/*}*/}\n";
	$chartContent .= "document.getElementById('newItemID').value = itemIDList;\n";
	//$chartContent .= "alert(document.getElementById('newItemID').value);\n";	
	$chartContent .= "}\n";
	
	$chartContent .= "var data = ".$chart->toPrettyString().";\n";
	$chartContent .= "</script>\n";
	$chartContent .= "<!--\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "############################################ END - flash chart ###############################################################\n";
	$chartContent .= "##############################################################################################################################\n";
	$chartContent .= "-->\n";

	$chartContent .= "</td></tr>";
	$chartContent .= "<tr><td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
	$chartContent .= "<tr><td align=\"right\">";
	$chartContent .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	$chartContent .= "<tr><td align=\"center\">";
	$chartContent .= $linterface->GET_ACTION_BTN($button_export, "button", "exportStats();")."&nbsp;";
	$chartContent .= $linterface->GET_ACTION_BTN($button_print, "button", "printStats();");
	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";
	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";

	$initialString = "<script language=\"javascript\">hideOption();</script>\n";
} else {
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
	$initialString = "<script language=\"javascript\">jPeriod='YEAR';jStatType='NO_OF_MD';</script>\n";
}


$linterface->LAYOUT_START();

?>

<script language="javascript">
var jPeriod="<?=$Period?>";
var jStatType="<?=$StatType?>";

// Generate Merit Category
var merit_cat_select = new Array();
merit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($merit_cats as $k=>$d)	{ ?>
		merit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Demerit Category
var demerit_cat_select = new Array();
demerit_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($demerit_cats as $k=>$d)	{ ?>
		demerit_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['CategoryName'])?>",<?=$d['CatID']?>);
<?	} ?>

// Generate Merit Type
var merit_record_type = new Array();
<?	foreach($merit_record_type as $k=>$d)	{ ?>
		merit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<?	} ?>

// Generate Demerit Type
var demerit_record_type = new Array();
<?	foreach($demerit_record_type as $k=>$d)	{ ?>
		demerit_record_type[<?=$k?>] = new Option("<?=$d[1]?>",<?=$d[0]?>);
<?	} ?>

function changeCat(value)
{
	var item_select = document.getElementById('ItemID[]');
	var selectedCatID = value;
	
	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	
	if(selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>

		if(document.frmStats.record_type_award.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($merit_item); $i++)
			{
				list($r_itemID, $r_itemName) = $merit_item[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				//$r_itemName = str_replace('"', '&quot;', $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($demerit_item); $i++)
			{
				list($r_itemID, $r_itemName) = $demerit_item[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				//$r_itemName = str_replace('"', '&quot;', $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		}
	}
	else if (selectedCatID == '')
	{
	<?
		$curr_cat_id = "";
		$pos = 0;
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
			$r_itemName = str_replace("'", "\'", $r_itemName);
			//$r_itemName = str_replace('"', '&quot;', $r_itemName);
			if ($r_catID != $curr_cat_id)
			{
				$pos = 0;
	?>
			}
			else if (selectedCatID == "<?=$r_catID?>")
			{
	<?
				$curr_cat_id = $r_catID;
			}
	?>
			item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?		if($r_itemID == $ItemID) {	?>
					item_select.selectedIndex = <?=$pos-1?>;
		<? 		} ?>
		<?
		}
	?>
	}
	SelectAll(frmStats.elements['ItemID[]']);
}

function showSpan(span){
	document.getElementById(span).style.display="inline";
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className='';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className='report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function getSelectedString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].value + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function exportStats(){
	if(document.getElementById('newItemID').value != '') {
		document.frmStats.action = "index_export.php";
		document.frmStats.submit();
		document.frmStats.action = "";
	} else {
		alert('<?=$msg_check_at_least_one_item?>');	
	}
}

function printStats(){
	if(document.getElementById('newItemID').value != '') {
		document.frmStats.action = "index_print.php";
		document.frmStats.target = "_blank";
		document.frmStats.submit();
		document.frmStats.action = "";
		document.frmStats.target = "_self";
	} else {
		alert('<?=$msg_check_at_least_one_item?>');	
	}
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function checkForm(){
	if (jPeriod=='DATE' && document.getElementById('textFromDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('textToDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textFromDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textToDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.frmStats.textFromDate.value > document.frmStats.textToDate.value)) {
		alert("<?=$i_invalid_date?>");
		return false;	
	}
	if (document.getElementById('selectBy').value=='FORM' && countOption(document.getElementById('selectForm[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('selectBy').value=='CLASS' && countOption(document.getElementById('selectClass[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if (jStatType=='NO_OF_MD' && countOption(document.getElementById('selectMeritDemerit[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_No_Of_Merits_Demerits?>');
		return false;
	}
	if (jStatType=='AP_TITLES' && countOption(document.getElementById('ItemID[]'))<1) {
		alert('<?=$i_Discipline_Award_Punishment_Titles?>');
		return false;
	}
	document.getElementById('Period').value = jPeriod;
	document.getElementById('StatType').value = jStatType;
	getSelectedString(document.getElementById('selectForm[]'), 'SelectedForm');
	getSelectedTextString(document.getElementById('selectForm[]'), 'SelectedFormText');
	getSelectedString(document.getElementById('selectClass[]'), 'SelectedClass');
	getSelectedTextString(document.getElementById('selectClass[]'), 'SelectedClassText');
	getSelectedString(document.getElementById('selectMeritDemerit[]'), 'SelectedMeritDemerit');
	getSelectedTextString(document.getElementById('selectMeritDemerit[]'), 'SelectedMeritDemeritText');
	getSelectedString(document.getElementById('ItemID[]'), 'SelectedItemID');
	getSelectedTextString(document.getElementById('ItemID[]'), 'SelectedItemIDText');
	document.getElementById('submit_flag').value='YES';
<? if ($_POST["submit_flag"] == "YES") {
	echo "document.getElementById('newItemID').value = newItemID;";
}
?>
//	document.getElementById('textToDate').value='';
//	document.getElementById('textFromDate').value='';
	return true;
}

</script>
<br />
<form name="frmStats" method="post" action="index2.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<?=$optionString?>
							</tr>
						</table>
						<br />
						<span id="spanOptionContent">
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td height="57" width="150" valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span>
								</td>
								<td width="95%">
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td height="30" colspan="6">
												<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" onClick="javascript:jPeriod=this.value"<? echo ($Period!="DATE")?" checked":"" ?>>
												<?=$i_Discipline_School_Year?>
												<?=$selectSchoolYearHTML?>&nbsp;
												<?=$i_Discipline_Semester?>
												<?=$selectSemesterHTML?>
											</td>
										</tr>
										<tr>
											<td><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" onClick="javascript:jPeriod=this.value"<? echo ($Period=="DATE")?" checked":"" ?>> <?=$i_From?> </td>
											<td><?=$linterface->GET_DATE_FIELD2("textFromDateSpan", "frmStats", "textFromDate", $textFromDate, 1, "textFromDate", "onclick=\"changeRadioSelection('DATE')\"")?>&nbsp;</td>
											<td> <?=$i_To?> </td>
											<td><?=$linterface->GET_DATE_FIELD2("textToDateSpan", "frmStats", "textToDate", $textToDate, 1, "textToDate", "onclick=\"changeRadioSelection('DATE')\"")?>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<select name="selectBy" onChange="javascript:if(this.value=='FORM'){hideSpan('spanClass');showSpan('spanForm');}else{hideSpan('spanForm');showSpan('spanClass');}">
													<option value="FORM"<? echo ($selectBy!="CLASS")?" selected":"" ?>><?=$i_Discipline_Form?></option>
													<option value="CLASS"<? echo ($selectBy=="CLASS")?" selected":"" ?>><?=$i_Discipline_Class?></option>
												</select>
												<br />
												<span id="spanForm"<? echo ($selectBy!="CLASS")?"":" style=\"display:none\"" ?>>
													<?=$selectFormHTML?>
													<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['selectForm[]']); return false;") ?>
												</span>
												<span id="spanClass"<? echo ($selectBy=="CLASS")?"":" style=\"display:none\"" ?>>
													<?=$selectClassHTML?>
													<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['selectClass[]']); return false;") ?>
												</span>
												<br />
												<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
											</td>
											<td valign="top"><br /></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<span><?=$eDiscipline["CompareShow"]?> <span class="tabletextrequire">*</span></span>
								</td>
								<td width="80%">
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<input name="radioType" type="radio" id="radioType1" value="NO_OF_MD" onClick="javascript:jStatType=this.value;hideSpan('spanAPTitles');showSpan('spanNoOfMD');"<? echo ($StatType!="AP_TITLES")?" checked":"" ?> /><label for="radioType1"><?=$i_Discipline_No_Of_Merits_Demerits?></label>
												<input name="radioType" type="radio" id="radioType2" value="AP_TITLES" onClick="javascript:jStatType=this.value;hideSpan('spanNoOfMD');showSpan('spanAPTitles');changeCat('0');"<? echo ($StatType=="AP_TITLES")?" checked":"" ?> /><label for="radioType2"><?=$i_Discipline_Award_Punishment_Titles?></label>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- ############### //-->
							<tr>
								<td colspan="2">
									<span id="spanNoOfMD"<? echo ($StatType!="AP_TITLES")?"":" style=\"display:none\"" ?>>
									<table width="100%" cellspacing="0" cellpadding="0">
										<tr>
											<td valign="top" nowrap="nowrap" class="formfieldtitle" width="22%">
												<?=$iDiscipline['RecordType2']?> <span class="tabletextrequire">*</span>
											</td>
											<td width="78%">
												<table width="0%" border="0" cellspacing="2" cellpadding="0">
													<tr>
														<td valign="top">
															<?=$selectMeritDemeritHTML?>
															<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['selectMeritDemerit[]']); return false;") ?>
															<br />
															<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									</span>
									<span id="spanAPTitles"<? echo ($StatType=="AP_TITLES")?"":" style=\"display:none\"" ?>>
										<table width="100%" cellspacing="0" cellpadding="0">
											<tr>
												<td width="150" valign="top" nowrap="nowrap" class="formfieldtitle">
													<?=$iDiscipline['RecordType2']?> <span class="tabletextrequire">*</span>
												</td>
												<td width="95%">
													<table width="0%" border="0" cellspacing="2" cellpadding="0">
														<tr>
															<td valign="top">
																<table width="100%" border="0" cellspacing="0" cellpadding="0">
																	<tr>
																		<td>
																			<input name="record_type" type="radio" id="record_type_award" value="1" <?=($record_type==1 || $record_type=="")?"checked":""?> onClick="hideSpan('spanDemeritCat');showSpan('spanMeritCat');changeCat('0');document.getElementById('selectMeritCat').value='0';"><label for="record_type_award"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_merit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Award?></label>
																			<input name="record_type" type="radio" id="record_type_punishment" value="-1" <?=($record_type==-1)?"checked":""?> onClick="hideSpan('spanMeritCat');showSpan('spanDemeritCat');changeCat('0');document.getElementById('selectDemeritCat').value='0';"><label for="record_type_punishment"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_demerit.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Merit_Punishment?></label><br />
																			<span id="spanMeritCat"<? echo ($record_type==1 || $record_type=="")?"":" style=\"display:none\"" ?>>
																				<?=$selectMeritCatHTML?>
																			</span>
																			<span id="spanDemeritCat"<? echo ($record_type==-1)?"":" style=\"display:none\"" ?>>
																				<?=$selectDemeritCatHTML?>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<?=$selectItemIDHTML?>
																			<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ItemID[]']); return false;") ?>
																		</td>
																	</tr>
																	<tr>
																		<td>
																			<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr><!--
											<tr>
												<td class="formfieldtitle">
													<?=$i_Discipline_Show_Only?> <span class="tabletextrequire">*</span></td>
												</td>
												<td>
													<select name="selectShowOnlyType" onChange="if(this.value=='ALL'){document.getElementById('selectShowOnlyNum').disabled=true;}else{{document.getElementById('selectShowOnlyNum').disabled=false;}}">
														<option value="ALL"<? echo ($selectShowOnlyType!="TOP")?" selected":"" ?>><?=$i_general_all?></option>
														<option value="TOP"<? echo ($selectShowOnlyType=="TOP")?" selected":"" ?>><?=$i_Discipline_Top?></option>
													</select>
													<select name="selectShowOnlyNum"<? echo ($selectShowOnlyType!="TOP")?" disabled":"" ?>>
														<option value="5"<? echo ($selectShowOnlyNum!="10")?" selected":"" ?>>5</option>
														<option value="10"<? echo ($selectShowOnlyNum=="10")?" selected":"" ?>>10</option>
													</select>
												</td>
											</tr>//-->
											<input type="hidden" name="selectShowOnlyType" value="ALL">
										</table>
									</span>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap">
									<span class="tabletextremark"><?=$i_general_required_field?></span>
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td height="1" class="dotline" colspan="2">
									<img src="<?="$image_path/$LAYOUT_SKIN"?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td>
									<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit", "document.frmStats.action='index2.php';document.frmStats.target='_self';return checkForm();")?>
								</td>
							</tr>
						</table>
						</span>
					</td>
				</tr>
			</table>
			<br />
				<?=$chartContent?>
			<br />
		</td>
	</tr>
</table>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="Period" id="Period" value="<?=$Period?>" />
<input type="hidden" name="submitSelectBy" id="submitSelectBy" value="<?=$selectBy?>" />
<input type="hidden" name="StatType" id="StatType" value="<?=$StatType?>" />
<input type="hidden" name="PeriodField1" id="PeriodField1" value="<?=$parPeriodField1?>" />
<input type="hidden" name="PeriodField2" id="PeriodField2" value="<?=$parPeriodField2?>" />
<input type="hidden" name="ByField" id="ByField" value="<?=implode(",", $parByFieldArr)?>" />
<input type="hidden" name="StatsField" id="StatsField" value="<?=implode(",", $parStatsFieldArr)?>" />
<input type="hidden" name="StatsFieldText" id="StatsFieldText" value="<?=intranet_htmlspecialchars(implode(",", $parStatsFieldTextArr))?>" />
<input type="hidden" name="SelectedForm" id="SelectedForm" />
<input type="hidden" name="SelectedFormText" id="SelectedFormText" />
<input type="hidden" name="SelectedClass" id="SelectedClass" />
<input type="hidden" name="SelectedClassText" id="SelectedClassText" />
<input type="hidden" name="SelectedMeritDemerit" id="SelectedMeritDemerit" />
<input type="hidden" name="SelectedMeritDemeritText" id="SelectedMeritDemeritText" />
<input type="hidden" name="SelectedItemID" id="SelectedItemID" />
<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" />
<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">

<?/*
if ($_POST["submit_flag"] == "YES") {
	echo "<input type=\"hidden\" name=\"newItemID\" id=\"newItemID\" value=\"".implode(",", $all_item)."\">";
}
*/?>
<? if ($_POST["submit_flag"] == "YES") {
	if($StatType=="NO_OF_MD") {
		echo "<input type=\"hidden\" name=\"newItemID\" id=\"newItemID\" value=\"".implode(",",$SelectedMeritDemeritArr)."\">";
	} else {
		echo "<input type=\"hidden\" name=\"newItemID\" id=\"newItemID\" value=\"".implode(",", $parStatsFieldArr)."\">";
	}
}
?>

</form>
<br />
<?=$initialString?>
<?
$linterface->LAYOUT_STOP();
?>
