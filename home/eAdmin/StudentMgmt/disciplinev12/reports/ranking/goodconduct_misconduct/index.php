<?php
# using:  YAT
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


# School Year Menu #
$SchoolYear = $SchoolYear =="" ? Get_Current_Academic_Year_ID() : $SchoolYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange='changeTerm(this.value)'", "", $SchoolYear);


# Semester Menu #
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==10) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

# Class #

$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1);
$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT('',1);
$good_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(1,0); # "Homework" subjects not included
$mis_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1,1);


for ($i = 0; $i < sizeof($good_cats); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$good_cats[$i][0]."\"";
	if ($selectGoodCat == $good_cats[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$good_cats[$i][1]."</option>\n";
}

for ($i = 0; $i < sizeof($mis_cats); $i++) {
	$selectMisCatHTML .= "<option value=\"".$mis_cats[$i][0]."\"";
	if ($selectMisCat == $mis_cats[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$mis_cats[$i][1]."</option>\n";
}

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);
# Good Conduct Item
$selectGoodItemIDHTML = "<select name=\"ItemID1[]\" multiple size=\"5\" id=\"selectGoodItem\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($good_items); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $good_items[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectGoodItemIDHTML .= "<option value=\"".$r_itemID."\">".$r_itemName."</option>\n";
		}
	}
}
$selectGoodItemIDHTML .= "</select>\n";

# Misconduct Item
$selectMisItemIDHTML = "<select name=\"ItemID2[]\" multiple size=\"5\" id=\"selectMisItem\">\n";
if ($_POST["submit_flag"] == "YES") {
	for ($i = 0; $i < sizeof($mis_items); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $mis_items[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectMisItemIDHTML .= "<option value=\"".$r_itemID."\">".$r_itemName."</option>\n";
		}
	}
}
$selectMisItemIDHTML .= "</select>\n";

# menu highlight setting
$TAGS_OBJ[] = array($eDiscipline['GoodconductMisconductRanking'], "");
$CurrentPage = "GoodconductMisconductRanking";

$warningMsg = $i_Discipline_System_Ranking_Goodconduct_Misconduct_Report_Msg;

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');
		//var choice = obj;

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}
	if(form1.rankTarget.value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}	 
	
	if(form1.typeBy[0].checked==true && form1.record_type_goodconduct.checked==false && form1.record_type_misconduct.checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["SelectRecordCategory"]?>");	
		return false;
	}
	if(form1.typeBy[1].checked==true && form1.record_type_good.checked==false && form1.record_type_mis.checked==false) {
		alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["SelectRecordCategory"]?>");	
		return false;
	}
	if(form1.typeBy[2].checked==true) {
		if(form1.record_type_good2.checked==false && form1.record_type_mis2.checked==false) {
			alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["SelectRecordCategory"]?>");		
			return false;
		}
		var item1Length = document.getElementById('selectGoodItem');
		var item2Length = document.getElementById('selectMisItem');
		
		choiceSelected = 0;
		for(i=0;i<item1Length.length;i++) {
			if(item1Length.options[i].selected==true)	
				choiceSelected = 1;
		}
		for(i=0;i<item2Length.length;i++) {
			if(item2Length.options[i].selected==true)	
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect?><?=$eDiscipline["Award_Punishment_RecordItem"]?>");
			return false;
		}
	}
	
	getSelectedTextString(document.getElementById('selectGoodItem'), 'ItemText1');
	getSelectedTextString(document.getElementById('selectMisItem'), 'ItemText2');
	document.getElementById("SelectedItemText").value = document.getElementById('ItemText1').value;
	document.getElementById("SelectedItemText").value += (document.getElementById('ItemText1').value && document.getElementById('ItemText2').value) ? "," : "";
	document.getElementById("SelectedItemText").value += document.getElementById('ItemText2').value;
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";

	if(span == 'spanStudent') {
		form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
		

	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		SelectAll(document.getElementById('rankTargetDetail[]'));
	}
	
	if(span == 'spanStudent') {
		form1.elements['rankTargetDetail[]'].multiple = true;
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function getSelectedTextString(obj, targetObj) {
	var tmpString = '';
	for (i=0; i<obj.length; i++) {
		if (obj.options[i].selected == true) {
			tmpString += obj.options[i].text + ',';
		}
	}
	if (tmpString.length > 0) {
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
//-->
</script>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str)
{

	if(str != "student2ndLayer") 
		document.getElementById("studentFlag").value = 0;
	
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()

	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

		
	var url = "";
		
	url = "get_live2.php";
	url = url + "?target=" + str
	url += "&year="+document.getElementById('SchoolYear').value;
	url += (document.getElementById("studentFlag").value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged() 
{ 
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
		if(document.getElementById("studentFlag").value == 0)
			if(document.getElementById('rankTarget').value != 'student') {
				SelectAll(form1.elements['rankTargetDetail[]']);
			}
	} 
	//document.getElementById("studentFlag").value = 0;
}


function GetXmlHttpObject()
{

	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}



// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($good_cats as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($mis_cats as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

function changeCat(value)
{
	/*
	var item_select = "";
	item_select = document.getElementById('ItemID[]');
		
	var selectedCatID = value;
	
	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	*/
	//if (selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>
		//if(document.form1.record_type_good.checked || document.form1.record_type_good2.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($good_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $good_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "document.getElementById('selectGoodItem').options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		//} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($mis_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $mis_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "document.getElementById('selectMisItem').options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		//}
	//}
}

</script>
<form name="form1" method="post" action="ranking_report_view.php" onSubmit="return goCheck(this)">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td align="center" bgcolor="#FFFFFF">
	      <table width="98%" border="0" cellspacing="0" cellpadding="5">
	          <tr> 
	            <td align="center">
		            <table width="88%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="3">
									<tr>
										<td height="20" class="tabletextremark"><i><?=$i_Discipline_System_Reports_Report_Option?></i></td>
									</tr>
								</table>
								<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $warningMsg) ?>
								<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
									<tr valign="top">
										<td height="57" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['Period']?><span class="tabletextrequire">*</span></td>
										<td>
											<table border="0" cellspacing="0" cellpadding="3">
												<tr>
													<td height="30" colspan="6" onClick="document.form1.dateChoice[0].checked=true">
														<input name="dateChoice" type="radio" id="dateChoice[0]" value="1" checked>
														<?=$i_Discipline_School_Year?>
														<?=$selectSchoolYearHTML?>
														<!--
														<select name="SchoolYear" id="SchoolYear" onFocus="document.form1.dateChoice[0].checked=true" onchange="changeTerm(this.value)">
															<?=$selectSchoolYear?>
														</select>//-->
														<?=$i_Discipline_Semester?> 
														<span id="spanSemester"><select name="semester" id="semester" onFocus="document.form1.dateChoice[0].checked=true" >
															<?=$SemesterMenu?>
														</select></span>
													</td>
												</tr>
												<tr>
													<td><input name="dateChoice" type="radio" id="dateChoice[1]" value="2">
														<?=$iDiscipline['Period_Start']?></td>
													<td><!--<input name="startDate" type="text" class="tabletext" onFocus="document.form1.dateChoice[1].checked=true" />--></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
													<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
													<td><!--<input name="endDate" type="text" class="tabletext" onFocus="document.form1.dateChoice[1].checked=true" />--></td>
													<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RankingTarget']?><span class="tabletextrequire">*</span></td>
										<td width="80%">
											<table width="0%" border="0" cellspacing="2" cellpadding="0">
												<tr>
													<td valign="top">
														<table border="0">
															<tr>
																<td valign="top">
																	<select name="rankTarget" id="rankTarget" onChange="showResult(this.value);if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}">
																		<option value="#" selected>-- <?=$i_general_please_select?> --</option>
																		<option value="form"><?=$i_Discipline_Form?></option>
																		<option value="class"><?=$i_Discipline_Class?></option>
																		<option value="student"><?=$i_UserStudentName?></option>
																	</select>
																</td>
																<td>
																	<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
																	<select name="rankTargetDetail[]"  size="5" id="rankTargetDetail[]"></select>
																	<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAll(this.form.elements['rankTargetDetail[]']);return false;", "selectAllBtn01")?>
																</td>
																<td>
																	<div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:1;display:none;'>
																		<select name="studentID[]" multiple size="5" id="studentID[]"></select>
																		<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(this.form.elements['studentID[]'], true);return false;")?>
																	</div>
																</td>
															</tr>
															<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
														</table>
													</td>
													<td valign="top"><br></td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RecordType']?><span class="tabletextrequire">*</span>
										</td>
										<td><input name="typeBy" type="radio" id="typeBy[0]" value="byType" onClick="javascript:hideSpan('spanCategory');hideSpan('spanItem');showSpan('spanRecordType');document.getElementById('record_type_goodconduct').checked=true;" checked>
											<label for="typeBy[0]"><?=$iDiscipline['ReceiveType']?></label>
											<input name="typeBy" type="radio" id="typeBy[1]" value="byCategory" onClick="javascript:hideSpan('spanRecordType');hideSpan('spanItem');showSpan('spanCategory');showSpan('spanMisCat');showSpan('spanGoodCat');document.getElementById('record_type_good').checked=true;document.getElementById('record_type_mis').checked=true;document.getElementById('selectGoodCat').value='0';SelectAllItem(this.form.elements['selectGoodCat'], true);SelectAllItem(this.form.elements['selectMisCat'], true);">
											<label for="typeBy[1]"><?=$iDiscipline['byCategory']?></label>
											<input name="typeBy" type="radio" id="typeBy[2]" value="byItem" onClick="javascript:hideSpan('spanCategory');hideSpan('spanRecordType');showSpan('spanItem');document.getElementById('record_type_good2').checked=true;document.getElementById('record_type_mis2').checked=true;changeCat('0');SelectAllItem(this.form.elements['selectGoodItem'], true);SelectAllItem(this.form.elements['selectMisItem'], true);">
											<label for="typeBy[2]"><?=$iDiscipline['byItem']?></label>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="formfieldtitle">&nbsp;</td>
										<td>
											<span id="spanRecordType" style="display:inline">
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td width="95%">
															<table width="0%" border="0" cellspacing="2" cellpadding="0">
																<tr>
																	<td valign="top">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td>
																					<input name="record_type[]" type="checkbox" id="record_type_goodconduct" value="1" checked><label for="record_type_goodconduct"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_GoodConduct?></label>
																					<input name="record_type[]" type="checkbox" id="record_type_misconduct" value="-1" checked><label for="record_type_misconduct"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_Misconduct?></label>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<input type="hidden" name="selectShowOnlyType" id="selectShowOnlyType" value="ALL">
												</table>
											</span>
											<span id="spanCategory" style="display:none">
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td width="95%">
															<table width="0%" border="0" cellspacing="2" cellpadding="0">
																<tr>
																	<td valign="top">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td>
																					<table>
																						<tr>
																							<td>
																								<input name="record_type[]" type="checkbox" id="record_type_good" value="1" onClick="showSpan('spanMisCat');showSpan('spanGoodCat');if(document.getElementById('record_type_good').checked==true){SelectAllItem(this.form.elements['selectGoodCat'], true);} else {SelectAllItem(this.form.elements['selectGoodCat'], false);} "><label for="record_type_good"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_GoodConduct?></label>
																							</td>
																							<td>
																								<input name="record_type[]" type="checkbox" id="record_type_mis" value="-1" onClick="showSpan('spanGoodCat');showSpan('spanMisCat');if(document.getElementById('record_type_mis').checked==true){SelectAllItem(this.form.elements['selectMisCat'], true);} else {SelectAllItem(this.form.elements['selectMisCat'], false);} "><label for="record_type_mis"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_Misconduct?></label><br />
																							</td>
																							</tr><tr>
																							<td>
																								<span id="spanGoodCat"<? echo ($record_type==1 || $record_type=="")?"":" style=\"display:none\"" ?>>
																									<select name="selectGoodCat[]" multiple onChange="changeCat(this.value);" id="selectGoodCat">
																									<?=$selectGoodCatHTML?>
																									</select>
																								</span>
																							</td><td>
																								<span id="spanMisCat"<? echo ($record_type==-1)?"":" style=\"display:none\"" ?>>
																									<select name="selectMisCat[]" multiple onChange="changeCat(this.value);" id="selectMisCat">
																									<?=$selectMisCatHTML?>
																									</select>
																								</span>
																							</td>
																						</tr>
																						<tr>
																							<td><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectGoodCat'], true); document.getElementById('record_type_good').checked=true;return false;") ?></td>
																							<td><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectMisCat'], true); document.getElementById('record_type_mis').checked=true;return false;") ?></td>
																						</tr>
																						<tr><td colspan="2" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
																					</table>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<input type="hidden" name="selectShowOnlyType" id="selectShowOnlyType" value="ALL">
												</table>
											</span>
											<span id="spanItem" style="display:none">
												<table width="100%" cellspacing="0" cellpadding="0" border="0">
													<tr>
														<td width="95%">
															<table width="0%" border="0" cellspacing="2" cellpadding="0">
																<tr>
																	<td valign="top">
																		<table width="100%" border="0" cellspacing="0" cellpadding="0">
																			<tr>
																				<td>
																					<input name="record_type[]" type="checkbox" id="record_type_good2" value="1" onClick="if(document.getElementById('record_type_good2').checked){SelectAllItem(this.form.elements['selectGoodItem'], true);} else {SelectAllItem(this.form.elements['selectGoodItem'], false);}"><label for="record_type_good2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_GoodConduct?></label>
																				</td>
																				<td>
																					<input name="record_type[]" type="checkbox" id="record_type_mis2" value="-1" onClick="if(document.getElementById('record_type_mis2').checked){SelectAllItem(this.form.elements['selectMisItem'], true);} else {SelectAllItem(this.form.elements['selectMisItem'], false);}"><label for="record_type_mis2"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_Misconduct?></label>
																				</td>
																			</tr>
																			<tr>
																				<td>
																					<?=$selectGoodItemIDHTML?>
																				</td>
																				<td>
																					<?=$selectMisItemIDHTML?>
																				</td>
																			</tr>
																			<tr>
																				<td><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectGoodItem'], true); document.getElementById('record_type_good2').checked=true;return false;") ?></td>
																				<td><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(this.form.elements['selectMisItem'], true); document.getElementById('record_type_mis2').checked=true; return false;") ?></td>
																			</tr>
																			<tr>
																				<td colspan="2">
																					<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
													<input type="hidden" name="selectShowOnlyType" id="selectShowOnlyType" value="ALL">
												</table>
											</span>
										</td>
									</tr>
									<tr valign="top">
										<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$iDiscipline['RankingRange']?><span class="tabletextrequire">*</span></td>
										<td><?=$i_general_highest?> 
											<select name="rankRange" id="rankRange">
												<?=$rankRangeMenu?>
											</select>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
										<td>&nbsp;</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
							</tr>
							<tr>
								<td align="right">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="center">
												<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.action='ranking_report_view.php'", "submitBtn01")?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
			            </td>
			          </tr>
			        </table>
		        </td>
		    </tr>
			</table><br>

<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
<input type="hidden" name="ItemText1" id="ItemText1" />
<input type="hidden" name="ItemText2" id="ItemText2" />
<input type="hidden" name="SelectedItemText" id="SelectedItemText" />
</form>

<script>
<!--
function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form");
	
	changeTerm('<?=$SchoolYear?>');
}

init();


//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
