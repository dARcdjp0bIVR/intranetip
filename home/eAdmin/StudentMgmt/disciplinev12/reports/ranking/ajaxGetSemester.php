<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");

$ldiscipline = new libdisciplinev12();
$yearID = $ldiscipline->getAcademicYearIDByYearName($year);
$result = getSemesters($yearID);

$temp = "<select name='$field' id='$field' class='formtextbox' onFocus=\"document.form1.dateChoice[0].checked=true\">";
$temp .= "<option value='0'";
$temp .= ($year=='0') ? " selected" : "";
$temp .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

if($year!=0) {

	foreach($result as $termID=>$termName) {
		if($field=='semester1') {
			$selected = ($termName==$term1) ? " selected" : "";
		} else if($field=='semester2') {
			$selected = ($termName==$term2) ? " selected" : "";	
		} else {
			$selected = ($termName==$term) ? " selected" : "";	
		}
		
		$temp .= "<option value='$termName' $selected>$termName</option>";
	}
}
	
$temp .= "</select>";

echo $temp;

intranet_closedb();

?>