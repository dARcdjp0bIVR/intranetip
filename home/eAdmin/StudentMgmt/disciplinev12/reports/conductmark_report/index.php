<?
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-ConductMarkReport-View"))
{
	$url = "./conduct_mark_report/";
}
else
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
				
header("Location: $url");		
						

intranet_closedb();
?>