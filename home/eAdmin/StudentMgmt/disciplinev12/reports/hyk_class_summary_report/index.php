<?php
// Editing by 

#################### Change Log [start] ###########
#
#   Date:   2019-03-12 (Bill)   [2018-0710-1351-39240]
#           - Support Export as Word Document
#
#	Date:	2016-09-29 (Bill)	[2016-0808-1526-52240]
#			- Create file
#
#################### Change Log [end] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right
$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HYKClassSummaryReport']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Current Page
$CurrentPage = "Reports_HYK_Class_Summary_Report";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['HYKClassSummaryReport']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Year
//$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
$AcademicYearID = Get_Current_Academic_Year_ID();

// Year Option
$wholeYearOption = array();
$wholeYearOption[0][0] = 0;
$wholeYearOption[0]['YearTermID'] = 0;
$wholeYearOption[0][1] = $Lang['General']['WholeYear'];
$wholeYearOption[0]['TermName'] = $Lang['General']['WholeYear'];
$wholeYearOption[0]['TermStart'] = getStartDateOfAcademicYear($AcademicYearID);
$wholeYearOption[0]['TermEnd'] = getEndDateOfAcademicYear($AcademicYearID);

# Semester
$Semesters = getSemesters($AcademicYearID, 0);
$Semesters = array_merge($wholeYearOption, $Semesters);
$selectSemester = $linterface->GET_SELECTION_BOX($Semesters, " name='selectSemester' id='selectSemester' onChange='updateSemesterDisplay()'", "", getCurrentSemesterID());

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="JavaScript">
// Semester Data
var SemesterStartDate = new Array();
var SemesterEndDate = new Array();
<? for($i=0; $i<count((array)$Semesters); $i++){
	$thisSemester = $Semesters[$i];
	echo "SemesterStartDate[".$thisSemester["YearTermID"]."] = '".substr($thisSemester["TermStart"], 0, 10)."'; \r\n";
	echo "SemesterEndDate[".$thisSemester["YearTermID"]."] = '".substr($thisSemester["TermEnd"], 0, 10)."'; \r\n";
} ?>

function showRankTargetResult()
{
	var yearClassId = '';
	
	var url = "../ajaxGetLive.php?target=class";
	url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	url += "&academicYearId=<?=$AcademicYearID?>";
	
	// get class list
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function updateSemesterDisplay()
{
	var semester_id = $('#selectSemester').val();
	var semester_start = SemesterStartDate[semester_id];
	var semester_end = SemesterEndDate[semester_id];
	
	// Update semester display
	$('#semesterStart').html(semester_start);
	$('#StartDate').val(semester_start);
	$('#EndDate').val(semester_end);
	
	$('#DateWarnDiv').hide();
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	
	// Start Date
	var start_date = $.trim($('#StartDate').val());
	if(start_date == ''){
		valid = false;
		$('#StartDate').focus();
	}
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('StartDate'))){
			valid = false;
			$('#StartDate').focus();
		}
	}
	if(!valid) return;
	
	// End Date
	var end_date = $.trim($('#EndDate').val());
	if(end_date == ''){
		valid = false;
		$('#EndDate').focus();
	}
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('EndDate'))){
			valid = false;
			$('#EndDate').focus();
		}
	}
	if(!valid) return;
	
	// Date Range
	if(compareDate(end_date, start_date) < 0){
		valid = false;
		$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
		$('#DateWarnDiv').show();
		$('#EndDate').focus();
	}
	else{
		$('#DateWarnDiv').hide();
	}
	if(!valid) return;
	
	// Class
	if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
		valid = false;
		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
		$('#TargetWarnDiv').show();
	}
	else{
		$('#TargetWarnDiv').hide();
	}
	if(!valid) return;
	
	// submit
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target','');
		Block_Element('PageDiv');
		
		$.post(
			'report.php',
			$('#form1').serialize(),
			function(data){
				$('#ReportDiv').html(data);
				document.getElementById('div_form').className = 'report_option report_hide_option';
				
				$('.spanShowOption').show();
				$('.spanHideOption').hide();
				$('.Form_Span').hide();
				
				UnBlock_Element('PageDiv');
			}
		);
	}
	else if(format == 'csv' || format == 'word'){
		form_obj.attr('target','');
		form_obj.submit();
	}
	else if(format == 'print'){
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

$(document).ready(function(){
	showRankTargetResult();
	updateSemesterDisplay();
});
</script>

<div id="PageDiv">

<div id="div_form">
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php" onsubmit="return false;">
		<table class="form_table_v30">
			
			<!-- Year -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
				<td><?=getAYNameByAyId($AcademicYearID)?></td>
			</tr>
			
			<!-- Semester -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['General']['Semester']?></td>
				<td><?=$selectSemester?></td>
			</tr>
			
			<!-- Semester Start Date -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['eDiscipline']['HYKClassSummaryReport']['SemesterStart']?></td>
				<td id="semesterStart"></td>
			</tr>
			
			<!-- Date -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HYKClassSummaryReport']['RecordInputDate']?></td>
				<td><table class="inside_form_table">
						<tr><td>
							<?=$linterface->GET_DATE_PICKER("StartDate", "", "")?>
							<?=$i_To?>
							<?=$linterface->GET_DATE_PICKER("EndDate", "", "")?>
						</td></tr>
				</table>
				<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
				</td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_Discipline_Class?></td>
				<td><table class="inside_form_table">
						<tr><td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
						</td></tr>
						<tr>
							<td colspan="3">
								<input type='checkbox' name='ShowRecordStudentOnly' id='ShowRecordStudentOnly' value='1'><label for='ShowRecordStudentOnly'><?=$Lang['eDiscipline']['HYKClassSummaryReport']['ShowRecordStudentOnly']?></label>
							</td>
						</tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
			</td></tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "submitForm('print')")?>
			&nbsp;
			<?= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['HYKClassSummaryReport']['ExportToWord'], "button", "submitForm('word')")?>
			<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="format" id="format" value="print">
		<input type="hidden" name="YearID" id="YearID" value="<?=$AcademicYearID?>">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>