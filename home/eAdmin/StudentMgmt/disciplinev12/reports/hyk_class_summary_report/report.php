<?php
// Editing by 

#################### Change Log [start] ###########
#
#   Date:   2019-03-12 (Bill)   [2018-0710-1351-39240]
#           - Support Export as Word Document
#
#	Date:	2016-09-29 (Bill)	[2016-0808-1526-52240]
#			- Create file
#
#################### Change Log [end] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right
$ldiscipline = new libdisciplinev12_cust();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HYKClassSummaryReport']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();
$report_content = $ldiscipline_ui->getHYKClassSummaryReport($_POST);

# Print - HTML
if($_POST['format'] == 'print')
{
    include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
    echo $report_content;
    include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}
# Export - MS Word
else if($_POST['format'] == 'word')
{
    $ContentType = "application/x-msword";
    $filename = "Class_Monthly_Summary_".date("ymd").".doc";
    
    $tags_to_strip = Array("A", "a", "HTML", "html", "BODY", "body");
    foreach ($tags_to_strip as $tag)
    {
        $report_content = preg_replace("/<\/?" . $tag . "(.|\s)*?>/", "", $report_content);
    }
    
    $output = '';
    $output .= "<HTML xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
    $output .= "<HEAD>\n";
        $output .= returnHtmlMETA();
        $output .= "<style>\n";
            $output .= "div.Section1 { page: Section1; }\n";
            $output .= "@page Section1 { margin: 36.0pt 36.0pt 36.0pt 36.0pt; }\n";
            $output .= ".main_container { display: block; width: 713px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }
                        .main_container .report_header td { font-size: 10.5pt; font-family: \"新細明體\", \"Arial\", \"Lucida Console\"; padding: 0; }
                        .main_container .result_table { border-collapse: collapse; }
                        .main_container .result_table td { font-size: 10.5pt; font-family: \"新細明體\", \"Arial\", \"Lucida Console\"; text-align: center; }";
        $output .= "</style>\n";
    $output .= "</HEAD>\n";
    
    $output .= "<BODY LANG=\"zh-HK\">\n";
        $output .= "<div class=\"Section1\">\n";
            $output .= $report_content;
        $output .= "</div>\n";
    $output .= "</BODY>\n";
    $output .= "</HTML>\n";
    
    header('Pragma: public');
    header('Expires: 0');
    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    
    header('Content-type: '.$ContentType);
    header('Content-Length: '.strlen($output));
    
    header('Content-Disposition: attachment; filename="'.$filename.'";');
    echo $output;
}

intranet_closedb();
?>

<?php if($_POST['format'] == 'print') { ?>
<style type="text/css">
@charset "utf-8";
/* CSS Document */
.main_container { display: block; width: 713px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }
.main_container .report_header td { font-size: 14px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding: 0; }
.main_container .result_table { border-collapse: collapse; }
.main_container .result_table td { font-size: 14px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: center; }
.page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }
</style>
<?php } ?>