<?php
// Editing by 

#################### Change Log [start] ###########
#
#	Date:	2016-10-27 (Bill)	[2016-0808-1526-52240]
#			- Create file
#
#################### Change Log [end] ###########

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right
$ldiscipline = new libdisciplinev12_cust();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HYKMeritStatisticsReport']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$ldiscipline_ui = new libdisciplinev12_ui_cust();
echo $ldiscipline_ui->getHYKMeritStatisticsReport($_POST);

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>
<style type="text/css">
@charset "utf-8";
/* CSS Document */
.main_container { display: block; width: 713px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }
.main_container .report_header { padding-top:40px; }
.main_container .report_header td { font-size: 14px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding: 0; }
.main_container .result_table { border-collapse: collapse; }
.main_container .result_table td { font-size: 14px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: center; }
.page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }
</style>