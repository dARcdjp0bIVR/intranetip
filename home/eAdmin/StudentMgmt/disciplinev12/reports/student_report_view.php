<?php 
//using : 

$perPage = 10;

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

if($ck_page_size=='') {
	$ck_page_size = $perPage;	
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo))
{
	$pageNo = 1;
}
/*} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}*/

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}


$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# check parameter exist
if($studentID=='' || $targetClass == '') {
	header("Location: student_report.php");	
}


# Temp Assign memory of this page
ini_set("memory_limit", "200M"); 

if (!$lstudentprofile->is_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='1' id='merit1' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==1) ? " checked" : "";
	}
	$meritType .= "><label for='merit1'>".$i_Merit_Merit."</label>";
}
if (!$lstudentprofile->is_min_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='2' id='merit2' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==2) ? " checked" : "";
	}
	$meritType .= "><label for='merit2'>".$i_Merit_MinorCredit."</label>";
}
if (!$lstudentprofile->is_maj_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='3' id='merit3' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==3) ? " checked" : "";
	}
	$meritType .= "><label for='merit3'>".$i_Merit_MajorCredit."</label>";
}
if (!$lstudentprofile->is_sup_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='4' id='merit4' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==4) ? " checked" : "";
	}
	$meritType .= "><label for='merit4'>".$i_Merit_SuperCredit."</label>";
}
if (!$lstudentprofile->is_ult_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='5' id='merit5' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==5) ? " checked" : "";
	}
	$meritType .= "><label for='merit5'>".$i_Merit_UltraCredit."</label>";
}

$demeritType .= "<input name='demerit[]' type='checkbox' value='0' id='demerit0' onClick=\"checkClickBox(this,'all_punishment')\"";
for($i=0;$i<sizeof($demerit);$i++) {
	$demeritType .= ($demerit[$i]==0) ? " checked" : "";
}
$demeritType .= "><label for='demerit0'>".$i_Merit_Warning."</label>";
if (!$lstudentprofile->is_black_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-1' id='demerit1' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-1) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit1'>".$i_Merit_BlackMark."</label>";
}
if (!$lstudentprofile->is_min_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-2' id='demerit2' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-2) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit2'>".$i_Merit_MinorDemerit."</label>";
}
if (!$lstudentprofile->is_maj_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-3' id='demerit3' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-3) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit3'>".$i_Merit_MajorDemerit."</label>";
}
if (!$lstudentprofile->is_sup_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-4' id='demerit4' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-4) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit4'>".$i_Merit_SuperDemerit."</label>";
}
if (!$lstudentprofile->is_ult_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-5' id='demerit5' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-5) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit5'>".$i_Merit_UltraDemerit."</label>";
}

$meritDemerit[5] = $i_Merit_Warning;
$meritDemerit[6] = $i_Merit_Merit;
$meritDemerit[7] = $i_Merit_MinorCredit;
$meritDemerit[8] = $i_Merit_MajorCredit;
$meritDemerit[9] = $i_Merit_SuperCredit;
$meritDemerit[10] = $i_Merit_UltraCredit;
$meritDemerit[4] = $i_Merit_BlackMark;
$meritDemerit[3] = $i_Merit_MinorDemerit;
$meritDemerit[2] = $i_Merit_MajorDemerit;
$meritDemerit[1] = $i_Merit_SuperDemerit;
$meritDemerit[0] = $i_Merit_UltraDemerit;

$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYear1Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear1ID' id='SchoolYear1ID' onChange='changeTerm(this.value, \"semester1\")'", "", $SchoolYear1ID);
$selectSchoolYear2Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear2ID' id='SchoolYear2ID' onChange='changeTerm(this.value, \"semester2\")'", "", $SchoolYear2ID);

$SchoolYear1Text = $ldiscipline->getAcademicYearNameByYearID($SchoolYear1ID);
$SchoolYear2Text = $ldiscipline->getAcademicYearNameByYearID($SchoolYear2ID);

$act = new academic_year_term();
$semester1Text = $act->Get_YearNameByID($semester1);
$semester2Text = $act->Get_YearNameByID($semester2);

$year1Semesters = getSemesters($SchoolYear1ID);
$year2Semesters = getSemesters($SchoolYear2ID);

# Semester Menu #
$SemesterMenu1 .= "<option value='WholeYear'";
$SemesterMenu1 .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu1 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
foreach($year1Semesters as $id=>$name) {
	$SemesterMenu1 .= "<option value='$id'";
	$SemesterMenu1 .= ($semester1 != $id) ? "" : " selected";
	$SemesterMenu1 .= ">".$name."</option>";
}

$SemesterMenu2 .= "<option value='WholeYear'";
$SemesterMenu2.= ($semester2 != 'WholeYear') ? "" : " selected";
$SemesterMenu2 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
foreach($year2Semesters as $id=>$name) {
	$SemesterMenu2 .= "<option value='$id'";
	$SemesterMenu2 .= ($semester2 != $id) ? "" : " selected";
	$SemesterMenu2 .= ">".$name."</option>";
}

# Class #

$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value,'')\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_alert_pleaseselect);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$classAry = array();
$studentAry = array();

$studentAry = $ldiscipline->storeStudent($studentID, $targetClass);

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";

$semester1Text = ($semester1=='WholeYear' || $semester1==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester1Text;
$semester2Text = ($semester2=='WholeYear' || $semester2==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester2Text;


# SQL conditions (A&P) #

if($award_punish_period == 1) {		# choose "School Year" & "Semester"
	if($SchoolYear1ID != '0') {		# specific school year
		$date1 = $SchoolYear1Text." ".$semester1Text;
		$conds .= " AND a.AcademicYearID='$SchoolYear1ID'";
	} else {						# all school year
		$date1 = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester1Text;
		$academicYear = $ldiscipline->generateAllSchoolYear();
	}
	
	if($semester1!='WholeYear' && $semester1!=0) {	# specific semester
		$conds .= " AND a.YearTermID='$semester1'";
	}
} else {		# choose "Specific date range"
	$date1 = $Section1Fr." To ".$Section1To;
	$conds .= " AND (a.RecordDate BETWEEN '$Section1Fr' AND '$Section1To')";
}


if($waive_record1 == 1) {	# with waived record
	$conds .= " AND ((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.") OR (a.RecordStatus=".DISCIPLINE_STATUS_WAIVED."))";
} else { 					# only approved record
	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.")";	
}

# Award & Punishment option #
if($merit != '') {
	$meritChoice = implode(",",$merit);
}
if($demerit != '') {
	$demeritChoice = implode(",",$demerit);
}

if($all_award != '' && $all_punishment != '') {				# award + punishment
	$conds .= " AND (a.MeritType=1 OR a.MeritType=-1)";
} else if($all_award != '' && $demerit != '') {			# award + demerit
	$conds .= " AND (a.MeritType=1 OR a.ProfileMeritType IN ($demeritChoice))";
} else if($all_punishment != '' && $merit != '') {		# punish + merit
	$conds .= " AND (a.MeritType=-1 OR a.ProfileMeritType IN ($meritChoice))";
} else if($merit != '' && $demerit != '') {				# merit + demerit
	$conds .= " AND (a.ProfileMeritType IN ($meritChoice) or a.ProfileMeritType IN ($demeritChoice))";
} else if($all_award != '')	{							# only award
	$conds .= " AND a.MeritType=1";
} else if($all_punishment != '') {						# only punishment
	$conds .= " AND a.MeritType=-1";
} else if($merit != '')	 {								# merit
	$conds .= " AND a.ProfileMeritType IN ($meritChoice)";
} else if($demerit != '') {								# demerit
	$conds .= " AND a.ProfileMeritType IN ($demeritChoice)";
}

# end of SQL conditions

# loop of table content #
$displayContent = "";
$tempContent = "";

for($i=0;$i<sizeof($studentAry);$i++) {
	# student info
	$stdName = $ldiscipline->getStudentNameByID($studentAry[$i]);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	
	$classDisplay = $className;
	$classDisplay .= ($classNumber != '') ? ' - '.$classNumber : $classNumber;

	# --------- Table A&P ---------- 
	if($userID != '') {
		
		if($award_punishment_flag==1) {
			$result = $ldiscipline->awardPunishmentStudentReport($userID, $conds, $waivedImg, $meritImg, $demeritImg);
			
			$tempContent = "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
			$tempContent .= "<tr class='tabletop'>";
			$tempContent .= "<td width='1' class='tabletoplink'>#</td>";
			$tempContent .= "<td width='15%'>{$Lang['eDiscipline']['EventDate']}</td>";
			$tempContent .= "<td width='1'>&nbsp;</td>";
			$tempContent .= "<td width='1'>&nbsp;</td>";
			$tempContent .= "<td width='15%'>{$i_Discipline_Reason2}</td>";
			$tempContent .= "<td width='35%'>{$eDiscipline['Category_Item2']}</td>";
			if($show_conduct_mark==1)
				$tempContent .= "<td width='10%'>{$eDiscipline['Conduct_Mark']}</td>";
			$tempContent .= "<td width='30%'>{$i_Profile_PersonInCharge}</td>";
			if($sys_custom['sdbnsm_disciplinev12_student_report_cust']) {
				$tempContent .= "<td width='30%'>{$i_Discipline_Remark}</td>";
			}
			$tempContent .= "</tr>";
			
			if(sizeof($result)==0) {
				$colspan = ($sys_custom['sdbnsm_disciplinev12_student_report_cust']) ? 8 : 7;
				$tempContent .= "<tr class='row_approved'>";
				$tempContent .= "<td class='tabletext' height='40' colspan='$colspan' align='center'>{$i_no_record_exists_msg}</td>";
				$tempContent .= "</tr>";
			}
			
			for($m=0;$m<sizeof($result);$m++) {
				$k = ($m%2)+1;
				$j = $m+1;
				$temp = $result[$m][4] + 5;
				$tempContent .= "<tr class='row_approved'>";
				$tempContent .= "<td class='tabletext'>{$j}</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][0]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][1]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][2]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>". $ldiscipline->returnDeMeritStringWithNumber($result[$m][3], $meritDemerit[$temp]) ."&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>{$result[$m][5]}&nbsp;</td>";
				if($show_conduct_mark==1)
					$tempContent .= "<td class='tabletext tablerow' align='center'>{$result[$m][8]}&nbsp;</td>";
				$tempContent .= "<td class='tabletext tablerow'>";
				
				$name_field = getNameFieldByLang();
                if($result[$m][6] != '' && $result[$m][6] != 0) {
                	$sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ({$result[$m][6]})";
                	$temp = $ldiscipline->returnArray($sql,1);
                	if(sizeof($temp)!=0) {
                    	for($p=0;$p<sizeof($temp);$p++) {
                        	$tempContent .= $temp[$p][0];
                        	$tempContent .= ($p != (sizeof($temp)-1)) ? ", " : "";
                    	}
            		} else {
                		$tempContent .= "---";
            		}
        		} else {
            		$tempContent .= "---";	
        		}

				$tempContent .= "&nbsp;</td>";
				if($sys_custom['sdbnsm_disciplinev12_student_report_cust']) {
					$tempContent .= ($result[$m][7]) ? "<td class='tabletext tablerow'>{$result[$m][7]}&nbsp;</td>" : "<td class='tabletext tablerow'>---</td>";
				}
				$tempContent .= "</tr>";
			}
			$tempContent .= "</table>";
			
		}
		
		# --------- Good Conduct & Misconduct Table --------------- #
		# SQL conditions (G&M) #
		
		if($gdConduct_miscondict_period == 1) {
			$date2 = ($SchoolYear2ID != '0') ? $SchoolYear2Text." ".$semester2Text : $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester2Text;
			$dateConds = ($SchoolYear2ID != '0') ? " AND b.AcademicYearID='$SchoolYear2ID'" : "";
			$dateConds .= ($semester2 != 'WholeYear' && $semester2!='0') ? " AND b.YearTermID='$semester2'" : "";
		
		} else {
			$date2 = $Section2Fr." To ".$Section2To;
			$dateConds = " AND (b.RecordDate BETWEEN '$Section2Fr' AND '$Section2To')";
		}
		
		# GoodConduct & Misconduct Option #

		if($displayMode==1) {	# Summary Mode
			if($goodconductid != '') {
	//			$gdConductChoice = implode(",", $gdConductCategory);	
				$gdConductChoice = implode(",", $goodconductid);
			}
			if($misconductid != '') {
	//			$misConductChoice = implode(",", $misConductCategory);	
				$misConductChoice = implode(",", $misconductid);	
			}		
			$gdConductContent = $ldiscipline->getGdConductContent($userID, $waive_record2, $all_gd_conduct, $gdConductChoice, $SchoolYear2ID, $semester2,$intranet_root, $i_no_record_exists_msg,'row_approved', $dateConds);
			$misConductContent = $ldiscipline->getMisConductContent($userID, $waive_record2, $all_misconduct, $misConductChoice, $SchoolYear2ID, $semester2,$intranet_root, $i_no_record_exists_msg,'row_approved', $dateConds);
		} else {				# Detail Mode
			$printExport = 0;	# view
			//$Choice = ($goodconductid!='' && $misconductid!='') ? array_merge($goodconductid, $misconductid) : ($goodconductid ? $goodconductid : $misconductid);
			if(sizeof($goodconductid)>0) {
				$goodContent = $ldiscipline->getGoodMisReportInStudentReport($userID, $waive_record2, $goodconductid, $SchoolYear2ID, $semester2, $dateConds, 'row_approved', $printExport);
			}

			if(sizeof($misconductid)>0) {
				$misContent = $ldiscipline->getGoodMisReportInStudentReport($userID, $waive_record2, $misconductid, $SchoolYear2ID, $semester2, $dateConds, 'row_approved', $printExport);
			}
		}
		
		# --------- End of Good Conduct & Misconduct Table --------- #
		
		# display content #
		if($i != 0) {
			$displayContent .= "<tr class='row_approved'><td height='1'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";	
		}
		
		$displayContent .= "
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td align='left' class='tablegreenrow2'>
					<table border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_stu_ind.gif' width='20' height='20' align='absmiddle'>{$i_Discipline_Student} : <span class='sectiontitle'>{$std_name}</span> </td>
							<td>{$i_Discipline_Class} : <span class='sectiontitle'>{$classDisplay}</span></td>
						</tr>
					</table>
				</td>
			</tr>
		";
		
		if($award_punishment_flag==1) {
			$displayContent .= "
				<tr>
					<td>
						<table width='100%' border='0' cellspacing='0' cellpadding='0'>
							<tr>
								<td align='left' class='form_sep_title'><i> - {$eDiscipline['Award_and_Punishment']} -</i></td>
								<td align='right'>{$date1}</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align='left' class='tabletextremark'>
						{$tempContent}								
						<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_waived.gif' width='20' height='20' align='absmiddle'>= {$i_Discipline_System_WaiveStatus_Waived}
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			";
		}
		if($displayMode==1) {
			if($gdConduct==1 || $misconduct==1) {
				$displayContent .= "
					<tr>
						<td>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='left' class='form_sep_title'><i> - {$eDiscipline['Good_Conduct_and_Misconduct']} -</i></td>
									<td align='right'>{$date2}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align='left'>
							<table width='100%' border='0' cellspacing='0' cellpadding='3'>
								<tr>
				";
				if($gdConduct==1) {
					$displayContent .= "	
						<td width='50%' valign='top'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr class='tablebottom'>
									<td valign='top'>
										<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'>{$eDiscipline['Setting_GoodConduct']} </td>
									<td align='center' valign='middle'>{$eDiscipline['Setting_AccumulatedTimes']}</td>
								</tr>
								{$gdConductContent}
							</table>
						</td>
					";
				}
				if($misconduct==1) {
					$displayContent .= "
						<td width='50%' valign='top'>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr class='tablebottom'>
									<td valign='top'>
										<img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'>{$eDiscipline['Setting_Misconduct']}</td>
									<td align='center' valign='middle'>{$eDiscipline['Setting_AccumulatedTimes']}</td>
								</tr>
								{$misConductContent}
							</table>
						</td>
					";
				}
	
				$displayContent .= "</tr></table></td></tr>";
				/*$displayContent .= "<tr><td>
					<table width='100%' border='0' cellspacing='0' cellpadding='0'>
					<tr><td align='left' class='tabletextremark'>() = {$i_Discipline_System_WaiveStatus_Waived}</td></tr>
					</table></td></tr>";*/
			}
		} else {
			if($gdConduct==1) {
				$displayContent .= "
					<tr>
						<td>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='left' class='form_sep_title'><i> - {$eDiscipline['Setting_GoodConduct']} -</i></td>
									<td align='right'>{$date2}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							{$goodContent}								
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				";
			}
			if($misconduct==1) {
				$displayContent .= "
					<tr>
						<td>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='left' class='form_sep_title'><i> - {$eDiscipline['Setting_Misconduct']} -</i></td>
									<td align='right'>{$date2}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							{$misContent}								
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				";
			}
		}
	}

	# end of display content #
		
	#--------- End Table ------
}


# Report Option #
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "<td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i> - {$i_Discipline_Student} -</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_Discipline_Class}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td class='navigation'>";
$reportOptionContent .= "{$select_class}";
$reportOptionContent .= "</td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap'>&nbsp;</td>";
$reportOptionContent .= "<td><div id='studentID' style='position:absolute; width:280px; height:20px; z-index:0;'></div>";
$reportOptionContent .= "<select name='studentID' class='formtextbox'></select>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i>{$i_Discipline_System_Reports_First_Section}</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_general_show} <font color='green'>#</font></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_award' id='all_award' onClick=\"resetOption(this,'merit[]')\"";
$reportOptionContent .= ($all_award != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif' width='20' height='20' align='absmiddle'> <label for='all_award'>{$i_Discipline_System_Reports_All_Awards}</label> ";
$reportOptionContent .= "</td><td>{$meritType}</td></tr>";
$reportOptionContent .= "<tr><td valign='top'><input type='checkbox' name='all_punishment' id='all_punishment' onClick=\"resetOption(this,'demerit[]')\"";
$reportOptionContent .= ($all_punishment != '') ? " checked" : "";
$reportOptionContent .= "><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif' width='20' height='20' align='absmiddle'><label for='all_punishment'>{$i_Discipline_System_Reports_All_Punishment}</label> </td>";
$reportOptionContent .= "<td>{$demeritType}</td></tr></table><br>";
$reportOptionContent .= "<input type='checkbox' name='waive_record1' id='waive_record1' value='1'";
$reportOptionContent .= ($waive_record1==1) ? " checked" : "";
$reportOptionContent .= "><label for='waive_record1'>{$i_Discipline_System_Reports_Include_Waived_Record}</label><br>";
$reportOptionContent .= "<input type='checkbox' name='show_conduct_mark' id='show_conduct_mark' value='1'";
$reportOptionContent .= ($show_conduct_mark==1) ? " checked" : "";
$reportOptionContent .= "><label for='show_conduct_mark'>".$Lang['eDiscipline']['displayConductMark']."</label>";
$reportOptionContent .= "</td></tr><tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}</td>";
$reportOptionContent .= "<td>";
$reportOptionContent .= "<table border='0' cellspacing='0' cellpadding='3'><tr>";
$reportOptionContent .= "<td height='30' colspan='6' onClick=\"form1.award_punish_period[0].checked=true;\" onFocus=\"form1.award_punish_period[0].checked=true;\">";
$reportOptionContent .= "<input name='award_punish_period' type='radio' id='award_punish_period' value='1'";
$reportOptionContent .= ($award_punish_period==1) ? " checked" : "";
//$reportOptionContent .= ">{$i_Discipline_School_Year} <select name='SchoolYear1' onFocus=\"form1.award_punish_period[0].checked=true;\" onChange=\"changeTerm(this.value, 'semester1')\">{$selectSchoolYear1}</select>";
$reportOptionContent .= ">{$i_Discipline_School_Year} {$selectSchoolYear1Selection}";
$reportOptionContent .= "{$i_Discipline_Semester} <span id='spanSemester1'><select name='semester1' onFocus=\"form1.award_punish_period[0].checked=true;\">{$SemesterMenu1}</select></span>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td><input name='award_punish_period' type='radio' id='award_punish_period' value='2'";
$reportOptionContent .= ($award_punish_period==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td>".$linterface->GET_DATE_PICKER("Section1Fr",$Section1Fr)."</td>";
$reportOptionContent .= "<td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td>".$linterface->GET_DATE_PICKER("Section1To",$Section1To)."</td>";
$reportOptionContent .= "</tr></table></td></tr>";
$reportOptionContent .= "<tr><td colspan='2' valign='middle' nowrap='nowrap' class='form_sep_title'><i> {$i_Discipline_System_Reports_Second_Section}</i></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>".$Lang['eDiscipline']['DisplayMode']."</td><td><input type='radio' name='displayMode' id='displayMode1' value='1'";
$reportOptionContent .= ($displayMode==1) ? " checked" : "";
$reportOptionContent .= "><label for='displayMode1'>".$Lang['eDiscipline']['DisplayInSummary']."</label> <input type='radio' name='displayMode' id='displayMode2' value='2'";
$reportOptionContent .= ($displayMode==2) ? " checked" : "";
$reportOptionContent .= "><label for='displayMode2'>".$Lang['eDiscipline']['DisplayInDetails']."</label></td>";
$reportOptionContent .= "</tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$i_general_show} <font color='green'>#</font></td>";
$reportOptionContent .= "<td>";
$reportOptionContent .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td valign='top'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_gd_conduct.gif' width='20' height='20' border='0' align='absmiddle'>{$i_Discipline_GoodConduct}</td>";
$reportOptionContent .= "<td align='left'><img src='{$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_misconduct.gif' width='20' height='20' border='0' align='absmiddle'>{$i_Discipline_Misconduct}</td></tr>";
$reportOptionContent .= "<tr><td valign='top'>".$ldiscipline->showGoodconductList($goodconductid)."<br>".$linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['goodconductid[]']); return false;");
$reportOptionContent .= "&nbsp;".$linterface->GET_BTN($Lang['eDiscipline']['ButtonUnselectAll'], "button", "DeSelectAll(this.form.elements['goodconductid[]']); return false;")."<br><span class=\"tabletextremark\">(".$i_Discipline_Press_Ctrl_Key.")</span></td><td align='left'>";
$reportOptionContent .= $ldiscipline->showMisconductList($misconductid);
$reportOptionContent .= "<br>".$linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['misconductid[]']); return false;");
$reportOptionContent .= "&nbsp;".$linterface->GET_BTN($Lang['eDiscipline']['ButtonUnselectAll'], "button", "DeSelectAll(this.form.elements['misconductid[]']); return false;")."<br><span class=\"tabletextremark\">(".$i_Discipline_Press_Ctrl_Key.")</span></td></tr></table><br>";
$reportOptionContent .= "<input type='checkbox' name='waive_record2' id='waive_record2' value='2'";
$reportOptionContent .= ($waive_record2==2) ? "checked" : "";
$reportOptionContent .= "><label for='waive_record2'>{$i_Discipline_System_Reports_Include_Waived_Record}</label>";
$reportOptionContent .= "</td></tr><tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}</td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'><tr>";
$reportOptionContent .= "<td height='30' colspan='6' onClick=\"form1.gdConduct_miscondict_period[0].checked=true\"; onFocus=\"form1.gdConduct_miscondict_period[0].checked=true;\"><input name='gdConduct_miscondict_period' type='radio' id='radio' value='1' checked>";
//$reportOptionContent .= "{$i_Discipline_School_Year} <select name='SchoolYear2' onFocus=\"form1.gdConduct_miscondict_period[0].checked=true;\" onChange=\"changeTerm(this.value, 'semester2')\">{$selectSchoolYear2}</select>";
$reportOptionContent .= "{$i_Discipline_School_Year} {$selectSchoolYear2Selection}";

$reportOptionContent .= "{$i_Discipline_Semester} <span id='spanSemester2'><select name='semester2' onFocus=\"form1.gdConduct_miscondict_period[0].checked=true;\">{$SemesterMenu2}</select></span>";
$reportOptionContent .= "</td></tr><tr>";
$reportOptionContent .= "<td><input name='gdConduct_miscondict_period' type='radio' id='gdConduct_miscondict_period' value='2'";
$reportOptionContent .= ($gdConduct_miscondict_period==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td>".$linterface->GET_DATE_PICKER("Section2Fr",$Section2Fr)."</td>";
$reportOptionContent .= "<td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td>".$linterface->GET_DATE_PICKER("Section2To",$Section2To)."</td>";
$reportOptionContent .= "</tr></table></td></tr><tr>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td>&nbsp;</td></tr><tr><td colspan='2' align='center'>";
//$reportOptionContent .= "<input type='hidden' name='gdConduct' value=''><input type='hidden' name='misconduct' value=''><input type='hidden' name='award_punishment_flag' value=''>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, 'submit','document.form1.target=\'\';document.form1.action=\'student_report_view.php\'')."</td></tr></table>";

# End of Report Option #

$TAGS_OBJ[] = array($eDiscipline['StudentReport'], "");

# menu highlight setting
$CurrentPage = "StudentReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$i = 1;
?>
<script language="javascript">
function showSpan(span){
	document.getElementById(span).style.display="inline";
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.getElementById('showReportOption').value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('showReportOption').value = '0';
}

function checkClickBox(obj, ref) {
	var field = "";
	if(obj.checked==false) {
		field = eval("form1." + ref);
		field.checked = false;
	}
}

function resetOption(obj, element_name) {
	var field = "";
	var len = document.form1.elements.length;
	if(obj.checked == true)	 {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) 
				document.form1.elements[i].checked = true;
		}
	} else {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) 
				document.form1.elements[i].checked = false;
		}
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function DeSelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = false;
	}
}

function goCheck(form1) {
	var flag = 0;
	var temp = "";
	var region1 = 0;
	var region2 = 0;
	var region3 = 0;
	var region4 = 0;
	var choiceSelected = 0;
	var choice = "";
	form1.award_punishment_flag.value = "";
	form1.gdConduct.value = "";
	form1.misconduct.value = "";
	
	// region 1
	if(form1.all_award.checked==true || form1.all_punishment.checked==true) {
		region1 = 1;
		form1.award_punishment_flag.value = 1;
	}
	
	var len = form1.elements.length;
	for(i=0;i<len;i++) {
		//region2 = (form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) ? 1 : 0;
		//region2 = (form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) ? 1 : 0;
		if(form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}
		if(form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}/*
		if(form1.elements[i].name=='gdConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			form1.gdConduct.value = 1;
		}
		if(form1.elements[i].name=='misConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			form1.misconduct.value = 1;
		}*/
	}
	
	// region 2
	/*
	for(i=0;i<form1.merit.length;i++) {
		temp = eval("form1.merit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
	for(i=0;i<form1.demerit.length;i++) {
		temp = eval("form1.demerit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
*/
	// region 4
	choiceSelected = 0;
	choice = document.getElementById('goodconductid[]');
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true) {
			region4 = 1;
			form1.gdConduct.value = 1;
		}
	}
	choiceSelected = 0;
	choice = document.getElementById('misconductid[]');
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true) {
			region4 = 1;
			form1.misconduct.value = 1;
		}
	}
	
	if(form1.targetClass.value == '#') {
		alert("<?=$i_alert_pleaseselect?><?=$i_Discipline_Class?>");	
		return false;
	}
	
	if(region1==0 && region2==0 && region3==0 && region4==0) {
		alert("<?=$i_Discipline_System_Please_Select_Record?>");	
		return false;
	}

	if(region1!=0 || region2!=0){
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value=='' && form1.Section1To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.award_punish_period[1].checked==true && ((!check_date(form1.Section1Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section1To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value > form1.Section1To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
	if(region3!=0 || region4!=0){
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value=='' && form1.Section2To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.gdConduct_miscondict_period[1].checked==true && ((!check_date(form1.Section2Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section2To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value > form1.Section2To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
/*	
	if(form1.all_gd_conduct.checked==true) {
		form1.gdConduct.value = 1;	
	}
	if(form1.all_misconduct.checked==true) {
		form1.misconduct.value = 1;	
	}
*/
//	alert(form1.award_punishment_flag.value + "/" + form1.gdConduct.value + "/" + form1.misconduct.value);
	return true;
}


function showDiv(div)
{
	document.getElementById(div).style.display = "inline";
}

function hideDiv(div)
{
	document.getElementById(div).style.display = "none";
}

//window.onunload = goCheck(document.form1);
</script>
<script language="javascript">
var xmlHttp2
var xmlHttp3
function changeTerm(val, field) {
/*
	if (val.length==0)
	{ 
		document.getElementById("spanSemester1").innerHTML = "";
		document.getElementById("spanSemester1").style.border = "0px";
		document.getElementById("spanSemester2").innerHTML = "";
		document.getElementById("spanSemester2").style.border = "0px";
		return
	}
*/		
	xmlHttp2 = GetXmlHttpObject()
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp2==null || xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&field=" + field;
	url += "&term1=<?=$semester1?>";
	url += "&term2=<?=$semester2?>";
	
	if(field=='semester1') {
		xmlHttp2.onreadystatechange = stateChanged2 
		xmlHttp2.open("GET",url,true)
		xmlHttp2.send(null)
	} else {
		xmlHttp3.onreadystatechange = stateChanged3 
		xmlHttp3.open("GET",url,true)
		xmlHttp3.send(null)
	}
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanSemester1").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester1").style.border = "0px solid #A5ACB2";
	} 
}

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester2").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester2").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

showResult('<?=$targetClass?>','<?=$studentID?>');
changeTerm('<?=$SchoolYear1ID?>','semester1');
setTimeout("changeTerm('<?=$SchoolYear2ID?>', 'semester2')",1000);

function showResult(str,std)
{
	if (str.length==0)
		{ 
			document.getElementById("studentID").innerHTML = "";
			document.getElementById("studentID").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live.php";
	url = url + "?targetClass=" + str + "&sid=" + std
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("studentID").innerHTML = xmlHttp.responseText;
		document.getElementById("studentID").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="POST" action="" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<!--<a href="javascript:;" class="contenttool" onClick="javascript:if(document.getElementById('showReportOption').value=='0'){document.getElementById('showReportOption').value='1';showDiv('divReportOption');}else{document.getElementById('showReportOption').value='0';hideDiv('divReportOption');}"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_System_Reports_Show_Reports_Option?></a>//-->
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<br>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td align="center">
						<table border="0" cellspacing="5" cellpadding="5">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$i_Discipline_System_Reports_Discipline_report?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<?=$displayContent?>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit","document.form1.target='';document.form1.action='export_student.php'")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_print, "submit","document.form1.target='_blank';document.form1.action='print_student.php'")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_back, "button","javascript:self.location.href='student_report.php'")?>&nbsp;
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?= $li->pageNo; ?>"/>
<input type="hidden" name="order" value="<?= $li->order; ?>"/>
<input type="hidden" name="field" value="<?= $li->field; ?>"/>
<input type="hidden" name="page_size_change" value=""/>
<input type="hidden" name="numPerPage" value="<?= $li->page_size?>"/>
<input type="hidden" name="showReportOption" value="0">
<input type='hidden' name='gdConduct' value='<?=$gdConduct?>'>
<input type='hidden' name='misconduct' value='<?=$misconduct?>'>
<input type='hidden' name='award_punishment_flag' value='<?=$award_punishment_flag?>'>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
