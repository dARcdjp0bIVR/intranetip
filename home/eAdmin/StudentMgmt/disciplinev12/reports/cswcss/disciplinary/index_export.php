<?php
// Modifying by : Bill

########## Change Log ###############
#
#	Date	:	2016-12-09	(Bill)	[2016-1103-1545-24206]
#				support export alumni disciplinary record
#
##################################### 

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
//include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();
//$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$filename = "individual_student_disciplinary_record.csv";	

//$lclass = new libclass();
//$linterface = new interface_html();

### Table Data ###

if ($radioPeriod == "YEAR") {
	$parPeriodField1 = $selectYear;
	$parPeriodField2 = $selectSemester;
}
else {
	$parPeriodField1 = $textFromDate;
	$parPeriodField2 = $textToDate;
}

// Current Student	
$isAlumni = false;
if($studentTarget=="current") {
	$parByFieldArr = $rankTargetDetail;
}
// Alumni
else {
	$isAlumni = true;
	$parByFieldArr = $AlumniStudentID;
}

$exportContent = $ldiscipline->retrieve_CSWCSS_DisciplinaryRecord($radioPeriod, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $studentID, $display=3, $isAlumni);

################################################
############ Report Table Content ##############

/*
$exportArr = array();

# header of report table
$p = 0;

if($rankTarget=="form") {
	$exportArr[$p][] = $i_Discipline_Form;
	//$exportArr[$p][] = $i_Discipline_Class;
} else if($rankTarget=="class") {
	$exportArr[$p][] = $i_Discipline_Class;
} else {
	$exportArr[$p][] = $i_general_name;
	$exportArr[$p][] = $i_ClassNumber;
}


foreach($parStatsFieldTextArr as $tempValue) {
	$exportArr[$p][] = intranet_htmlspecialchars(stripslashes($tempValue));
}

$exportArr[$p][] = "";

foreach($i_MeritDemerit as $tempMeritDemerit) {
	$exportArr[$p][] = $tempMeritDemerit;
}
$p++;


$exportArr[$p][] = $i_Discipline_System_Report_Class_Total;
if($rankTarget=="student")	
	$exportArr[$p][] = "";

for($k=0; $k<sizeof($parStatsFieldTextArr); $k++) {
	$exportArr[$p][] = (isset($itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))])) ? $itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))] : "0";
}

$exportArr[$p][] = "";

if($record_type==1) {
	$m = 1;
} else {
	$m = 0;
}

foreach($i_MeritDemerit as $columnNo) {
	$exportArr[$p][] .= $columnMeritDemeritCount[$m];
	
	if($record_type==1) {
		$m++;
	} else {
		$m--;
	}
}
*/
############ End of Report Table ###############
################################################

if ($g_encoding_unicode)
{
	$Temp = explode("\n", urldecode($exportContent));
	$exportColumn = explode("\t", trim($Temp[0], "\r"));
	for ($i = 1; $i < sizeof($Temp); $i++)
	{
		$result[] = explode("\t", trim($Temp[$i], "\r"));
	}
	$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
	
}
else {
	$export_content .= stripslashes($exportContent);	
	$lexport->EXPORT_FILE($filename, $export_content);
}

intranet_closedb();

//$lexport->EXPORT_FILE($filename, $export_content);

?>