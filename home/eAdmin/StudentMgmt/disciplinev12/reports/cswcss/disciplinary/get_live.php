<?php
// Modifying by : Bill

########## Change Log ###############
#
#	Date	:	2016-12-09	(Bill)	[2016-1103-1545-24206]
#				added target: matched alumni
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");
$linterface = new interface_html();

$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

// [2016-1103-1545-24206]
if($target=='alumni')
{
	$AlumniName = trim($AlumniName);
	$ArchiveNameCond = ($AlumniName == "")? "" : " AND (iau.EnglishName LIKE '%".$AlumniName."%' or iau.ChineseName LIKE '%".$AlumniName."%') ";
	
	$AlumniWebsams = trim($AlumniWebsams);
	$ArchiveWebsamsCond = ($AlumniWebsams == "")? "" : " AND iau.WebSAMSRegNo LIKE '%".$AlumniWebsams."%' ";
	
	// Get alumni with discipline related records
	$sql = "SELECT
				iau.UserID as AlumniUserID,
				".Get_Lang_Selection("iau.ChineseName", "iau.EnglishName")." as StudentName,
				iau.UserLogin as UserLogin,
				iau.WebSAMSRegNo as WebSAMSRegNo,
				iau.YearOfLeft,
				CONCAT('<input type=\"checkbox\" name=\"AlumniStudentID[]\" id=\"AlumniStudentArr_', iau.UserID, '\" value=\"', iau.UserID ,'\">') as Checkbox
			FROM 
				INTRANET_ARCHIVE_USER as iau
			LEFT JOIN
				DISCIPLINE_MERIT_RECORD as mr ON (iau.UserID = mr.StudentID)
			LEFT JOIN
				DISCIPLINE_DETENTION_STUDENT_SESSION as dss ON (iau.UserID = dss.StudentID)
			LEFT JOIN
				PROFILE_ARCHIVE_ATTENDANCE as ara ON (iau.UserID = ara.UserID)
			WHERE
				(
					(mr.StudentID IS NOT NULL AND mr.StudentID = iau.UserID) OR 
					(dss.StudentID IS NOT NULL AND dss.StudentID = iau.UserID) OR 
					(ara.UserID IS NOT NULL AND ara.UserID = iau.UserID)
				)
				AND iau.RecordType = 2 ".$ArchiveNameCond.$ArchiveWebsamsCond."
			GROUP BY
				iau.UserID
			ORDER BY
				iau.YearOfLeft DESC,
				StudentName";
	$rs = $ldiscipline->returnResultSet($sql);
	$noOfRs = count($rs);
	
	$h_class = 'class="tabletext tablerowtablelink"';
	$temp = '<table class="common_table_list_v30" align="left" width="100%">';
		$temp .= '<thead>';
		$temp .= '<tr class="tabletop">';
			$temp .= '<th>#</th>';
			$temp .= '<th>'.$i_UserStudentName.'</th>';
			$temp .= '<th>'.$i_UserLogin.'</th>';
			$temp .= '<th>'.$i_WebSAMS_Registration_No.'</th>';
			$temp .= '<th>'.$Lang['AccountMgmt']['YearOfLeft'].'</th>';
			$temp .= '<th><input type ="checkbox" name="checkAllAlumni" value="1" onclick="checkAllAlumniCheckBox(this.checked)">&nbsp;</th>';
		$temp .= '</tr>';
		$temp .= '</thead>';
	
	if($noOfRs == 0)
	{
		$temp .= '<tr><td '.$h_class.' colspan="100%" align="center">'.$Lang['General']['NoRecordFound'].'</td></tr>';
	}
	else
	{
		// loop matched alumni
		for($i=0; $i<$noOfRs; $i++)
		{
			// alumni info
			$AlumniStudentName = trim($rs[$i]['StudentName']) == ''? '--' :$rs[$i]['StudentName'];
			$AlumniUserLogin = trim($rs[$i]['UserLogin']) == ''? '--' :$rs[$i]['UserLogin'];
			$AlumniWebSAMSRegNo = trim($rs[$i]['WebSAMSRegNo']) == ''? '--' :$rs[$i]['WebSAMSRegNo'];
			$AlumniYearOfLeft = trim($rs[$i]['YearOfLeft']) == ''? '--' :$rs[$i]['YearOfLeft'];
			$AlumniCheckbox = $rs[$i]['Checkbox'];
			
			$temp .= '<tr>';
				$temp .= '<td '.$h_class.'>'.intval($i+1).'</td>';
				$temp .= '<td '.$h_class.'>'.$AlumniStudentName.'</td>';
				$temp .= '<td '.$h_class.'>'.$AlumniUserLogin.'</td>';
				$temp .= '<td '.$h_class.'>'.$AlumniWebSAMSRegNo.'</td>';
				$temp .= '<td '.$h_class.'>'.$AlumniYearOfLeft.'</td>';
				$temp .= '<td '.$h_class.' width="30">'.$AlumniCheckbox.'</td>';
			$temp .= '</tr>';
		}
	}
	$temp .= '</table>';
}
else
{
	$result = $ldiscipline->getRankTarget($target);
	$RTDetail = explode(",", $rankTargetDetail);
	
	if($target=='form')
	{
		$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='5' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;} \">";
		$ClassLvlArr = $lclass->getLevelArray();

		$SelectedFormTextArr = explode(",", $SelectedFormText);

		$isEmpty = $ldiscipline->checkIsEmptyArray($RTDetail);
		
		for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
			$temp .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
			$temp .= (in_array($ClassLvlArr[$i][0], $RTDetail) || $isEmpty) ? " selected" : "";
			$temp .= ">".$ClassLvlArr[$i][1]."</option>\n";
		}
		$temp .= "</select>\n";
	}
	
	if($target=="class")
	{
		$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='5' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;}\">";
		$classResult = $ldiscipline->getRankClassList("",$year);
		$isEmpty = $ldiscipline->checkIsEmptyArray($RTDetail);
		
		for($k=0;$k<sizeof($classResult);$k++) {
			$temp .= "<option value='{$classResult[$k][1]}'";
			for($j=0;$j<sizeof($RTDetail);$j++) {
				$temp .= ($RTDetail[$j]==$classResult[$k][1] || $isEmpty) ? " selected" : "";	
			}
			$temp .= ">{$classResult[$k][0]}</option>";
		}
		$temp .= "</select>";
	}
	
	if($target=="student")
	{
		$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='5' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;} \">";
		$classResult = $ldiscipline->getRankClassList("", $year);
				
		for($k=0;$k<sizeof($classResult);$k++) {
			$temp .= "<option value='{$classResult[$k][1]}'";
			for($j=0;$j<sizeof($RTDetail);$j++) {
				$temp .= ($RTDetail[$j]==$classResult[$k][1]) ? " selected" : "";	
			}
			$temp .= ">{$classResult[$k][0]}</option>";
		}
		$temp .= "</select>";
	}
	
	$name_field = getNameFieldByLang("USR.");
	if($target=="student2ndLayer")
	{
		$studentIDAry = explode(',',$studentid);
		$temp = "<select name='studentID[]' id='studentID[]' class='formtextbox' multiple size='5'>";
		if($value != "") {
			$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
			//$sql = "SELECT ClassName, ClassNumber, $name_field, UserID FROM INTRANET_USER WHERE ClassName='$value' AND RecordType=2 ORDER BY ClassNumber";
			$sql = "SELECT $clsName, ycu.ClassNumber, $name_field, USR.UserID FROM INTRANET_USER USR
					LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) 
					LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
					WHERE yc.YearClassID=$value AND USR.RecordType=2 
					ORDER BY ycu.ClassNumber
					";
			
			$studentResult = $ldiscipline->returnArray($sql, 3);
	
			for($k=0;$k<sizeof($studentResult);$k++) {
				$temp .= "<option value={$studentResult[$k][3]}";
				for($j=0;$j<sizeof($RTDetail);$j++) {
					$temp .= (in_array($studentResult[$k][3], $studentIDAry)) ? " selected" : "";
				}
				$temp .= ">{$studentResult[$k][0]}-{$studentResult[$k][1]} {$studentResult[$k][2]}</option>";
			}
		}
		$temp .= "</select>";
		//$temp .= "<br>";
		
		$temp .= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(document.getElementById('studentID[]'), true);return false;");
	}	
}

echo $temp;

intranet_closedb();

?>