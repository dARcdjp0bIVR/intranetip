<?php

$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $button_select.($intranet_session_language=="en"?" ":"").$i_CampusMail_New_ListAlias;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

$permitted = array(1);

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
     if($CatID==-5)
     	$chooseGroupID[0]=1;
}

$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();


$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=0></option>\n";
$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_teachingStaff</option>\n";
$x1 .= "<option value=-5 ".(($CatID==-5)?"SELECTED":"").">$i_nonteachingStaff</option>\n";
$x1 .= "</select>";

if($CatID < 0){
   # Return users with identity chosen
   $selectedUserType = 0-$CatID;
   $c_teaching="";
   if($selectedUserType==5){
	   $c_user_type = 1;
	   $c_teaching = 0;
   }else if($selectedUserType==1){
	   $c_user_type = $selectedUserType;
	   $c_teaching =1;
   }else $c_user_type = $selectedUserType;

   //$row = $li->returnUserForType($selectedUserType);
   $lu = new libuser();
   $row = $lu->returnUsersByIdentity($c_user_type,$c_teaching);

     $x3  = "<select name=ChooseUserID[] size=10 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
     $row = $li->returnGroupUsersInIdentity($ChooseGroupID, $permitted);
     $x3  = "<select name=ChooseUserID[] size=10 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}

$suf_parent = $i_general_targetParent;

?>

<script language="javascript">
function AddOptions(obj)
{
	par = opener.window;
	parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];
	
	checkOption(obj);
	par.checkOption(parObj);
	i = obj.selectedIndex;
	
	while(i!=-1)
	{
		addtext = obj.options[i].text;
		
		par.checkOptionAdd(parObj, addtext, obj.options[i].value);
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
	par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
	par.form1.flag.value = 0;
	//par.generalFormSubmitCheck(par.form1);

}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name="form1" action="index2.php" method="post">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td>
	<br />
	<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td>
		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="tabletext" >
		<tr>
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_GroupCategorySettings?>:</span>
			</td>
			<td >
			<?=$x1?>
			</td>
		</tr>

		<?php
		if(isset($ChooseGroupID))
		{
		?>
		<tr>
			<td height="1" colspan="2" class="dotline">
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>

		<tr>
			<td height="5" colspan="2"  >
			<img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1" />
			</td>
		</tr>

		<tr >
			<td valign="top" nowrap="nowrap" width="30%" >
			<span class="tabletext"><?=$i_CampusMail_New_AddressBook_ByUser?>:</span>
			</td>
			<td >
			<table border="0" cellpadding="0" cellspacing="0" align="left">
			<tr >
				<td >
				<?=$x3?>
				</td>
				<td valign="bottom" >
				<table cellpadding="0" cellspacing="6" >


				<tr >
					<td >
					<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])") ?>
				</tr >
				<tr >
					<td >
					<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['ChooseUserID[]']); return false;") ?>
					</td >
				</tr >
				

				</table>
				</td>
			</tr>
			</table>
			</td>
		</tr>											
		<?php 
		} 
		?>
						
		</table>
		<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />

		</td>
	</tr>

	<tr>
		<td>
		<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		<tr>
			<td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close()") ?>
			</td>
		</tr>
		</table>
		</td>
	</tr>

	</table>

	</td>
</tr>
</table>

</form>

<?php
	intranet_closedb();
	$linterface->LAYOUT_STOP();
?>