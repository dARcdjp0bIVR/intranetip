<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
        
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();
$lc = new libclass();

if (!($plugin['Disciplinev12'] && $sys_custom['ediscipline_case_report']))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "WahYan_CaseReport";

$TAGS_OBJ[] = array($iDiscipline['CaseReport'], "index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['CreateReport'], "new.php", 1);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($iDiscipline['SelectCaseReport'], $CaseType?0:1 );
$STEPS_OBJ[] = array($iDiscipline['select_students'], $CaseType?1:0 );
$STEPS_OBJ[] = array($iDiscipline['ReportInfo'], 0);

# Last selection
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    # sort by class and class number
    $sql = "SELECT UserID, $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list) ORDER BY ClassName ASC, ClassNumber ASC";
    $array_students = $lc->returnArray($sql,2);
}

$student_selected = $linterface->GET_SELECTION_BOX($array_students, "name='student[]' class='select_studentlist' size='15' multiple='multiple'", "");
$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");
            
### case selection
if(!$CaseType)
{
	$CaseReportList = $ldiscipline->returnCaseReportList();
	$DisplayCase = getSelectByArray($CaseReportList,"name='CaseType' onChange=\"this.form.submit()\"");
}
else
{
	$DisplayCase = $ldiscipline->returnCaseName($CaseType) . "<input type='hidden' name='CaseType' value='$CaseType'>";	
}

### start for searching ###
#get required data
$data_ary = array();
// $sql = "SELECT ClassID,ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
// $result = $lc->returnArray($sql,2);
$sql = "select ClassTitleEN from YEAR_CLASS where AcademicYearID=". Get_Current_Academic_Year_ID()." order by Sequence";
$result = $lc->returnVector($sql);
for ($i = 0; $i < sizeof($result); $i++) 
{
	//list($this_classid, $this_classname) = $result[$i];
	$this_classname = $result[$i];
	
	$name_field = getNameFieldByLang();
	$sql1 = "SELECT UserID, $name_field, ClassNumber,UserLogin FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '".$this_classname."' ORDER BY ClassNumber";
	$result1 = $lc->returnArray($sql1,4);
	for ($j = 0; $j < sizeof($result1); $j++) 
	{
		list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
		$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
	}
}

#define yui array (Search by input format )
for($i=0;$i<sizeof($data_ary);$i++)
{
	list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

	$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
	if($this_class_number)
		$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
	else
		$temp_str2 = $this_stu_name;
	
	$liArr .= "[\"". $temp_str ."\", \"". $temp_str2 ."\", \"". $this_userid ."\"]";
	($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
}

foreach ($data_ary as $key => $row) 
{
	$field1[$key] = $row[0];	//user id
	$field2[$key] = $row[1];	//class name
	$field3[$key] = $row[2];	//class number
	$field4[$key] = $row[3];	//stu name
	$field5[$key] = $row[4];	//login id
}

array_multisort($field5, SORT_ASC, $data_ary);

#define yui array (Search by login id )
for($i=0;$i<sizeof($data_ary);$i++)
{
	list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];

	if($this_class_number)
		$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
	else
		$temp_str2 = $this_stu_name;
	
	$liArr2 .= "[\"". $this_userlogin ."\", \"". $temp_str2 ."\", \"". $this_userid ."\"]";
	($i == (sizeof($result)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
}
### end for searching

?>

<link type="text/css" rel="stylesheet" href="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}
    
    
	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>

<script language="javascript">
<!--
function finishSelection()
{
         obj = document.form1;
         obj.action = 'new2.php';
         checkOptionAll(obj.elements["student[]"]);
         obj.submit();
         return true;
}

function checkForm()
{
    obj = document.form1;

    if(obj.elements["student[]"].length != 0)
            return finishSelection();
    else
    {
            alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
            return false;
    }
}

//-->
</script>

<form name="form1" onsubmit="return checkForm();" method="POST">

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
        <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>

<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
					<?=$iDiscipline['SelectedCaseReport']?>
				</td>
				<td class="tabletext"><?=$DisplayCase?></td>
			</tr>
			</table>
		</td>
</tr>

<? if($CaseType) {?>
<tr>
        <td>
      	  <table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
      	  <tr>
      	  	<td class="tabletext" width="40%"><?=$i_general_choose_student?></td>
      	  	<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
      	  	<td class="tabletext" width="60%"><?=$i_general_selected_students; ?> <span class="tabletextrequire">*</span></td>
      	  </tr>
      	  
      	  <tr>
			<td class="tablerow2" valign="top">
				<table width="100%" border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td class="tabletext"><?=$i_general_from_class_group?></td>
				</tr>
				<tr>
					<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('choose/index.php?fieldname=student[]', 9)")?></td>
				</tr>
				<tr>
					<td class="tabletext"><i><?=$i_general_or?></i></td>
				</tr>
				<tr>
					<td class="tabletext">
					<?=$i_general_search_by_inputformat?><br \>
					<div id="statesautocomplete">
						<input type="text" class="tabletext" name="search1">
						<div id="statescontainer">
							<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
								<div style="display: none;" class="yui-ac-hd"></div>
								<div class="yui-ac-bd"></div>
								<div style="display: none;" class="yui-ac-ft"></div>
							</div>
							<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
						</div>
					</div>
					</td>
				</tr>	
				
				<tr>
					<td class="tabletext"><i><?=$i_general_or?></i></td>
				</tr>
				<tr>
					<td class="tabletext">
					<?=$i_general_search_by_loginid?><br \>
					<div id="statesautocomplete">
						<input type="text" class="tabletext" name="search2">
						<div id="statescontainerCC">
							<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
								<div style="display: none;" class="yui-ac-hd"></div>
								<div class="yui-ac-bd"></div>
								<div style="display: none;" class="yui-ac-ft"></div>
							</div>
							<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
						</div>
					</div>
					</td>
				</tr>	
				
				</table>	
			</td>
			<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
			<td align="left" valign="top">
				<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td align="left"><?= $student_selected ?></td>
				</tr>
				<tr>
					<td align="right"><?=$button_remove_html?></td>
				</tr>
				</table>
			</td>
		</tr>
                </table>
        </td>
</tr>
<? } ?>			

<? 
if($CaseType) {?>
<tr>
        <td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
                <tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
                <tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
                <tr>
                        <td align="center"><?= $linterface->GET_ACTION_BTN($button_continue, "submit", "this.form.flag.value=3") ?>
                        </td>
                </tr>
        </table>
        </td>
</tr>
<? } ?>
</table>

<br />
<input type="hidden" name="flag" value="">
</form>

<?
        $linterface->LAYOUT_STOP();
?>


<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<!-- In-memory JS array begins-->
<script type="text/javascript">
var statesArray = [
    <?= $liArr?>
];

var loginidArray = [
    <?= $liArr2?>
];

var delimArray = [
    ";"
];
</script>
<!-- In-memory JS array ends-->


<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
    var oACDS, oAutoComp;
    return {
        init: function() {

            // Instantiate first JS Array DataSource
            oACDS = new YAHOO.widget.DS_JSArray(statesArray);

            // Instantiate first AutoComplete            
            oAutoComp = new YAHOO.widget.AutoComplete('search1','student[]', 'statescontainer', oACDS);
            oAutoComp.queryDelay = 0;
            oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp.useShadow = true;
            oAutoComp.minQueryLength = 0;
            
            oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
            oAutoComp2 = new YAHOO.widget.AutoComplete('search2','student[]', 'statescontainerCC', oACDS2);
            oAutoComp2.queryDelay = 0;
            oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp2.useShadow = true;
            oAutoComp2.minQueryLength = 0;
        },

        validateForm: function() {
            // Validate form inputs here
            return false;
        }
    };
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>