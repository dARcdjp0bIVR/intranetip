<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
        
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if (!($plugin['Disciplinev12'] && $sys_custom['ediscipline_case_report']))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "WahYan_CaseReport";

$TAGS_OBJ[] = array($iDiscipline['CaseReport'], "index.php", $msg=="add"?0:1);
$TAGS_OBJ[] = array($iDiscipline['CreateReport'], "new.php", $msg=="add"?1:0);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($iDiscipline['PreviewReport'], "");

if($msg=="add")		$buttonDisplay = $linterface->GET_ACTION_BTN($i_Discipline_System_msg_AddAnotherRecord, "button", "self.location='new.php'");
if($msg=="update")	$buttonDisplay = $linterface->GET_ACTION_BTN($i_Discipline_System_msg_ModifyOtherRecord, "button", "ModifyOtherNotice();");

?>

<script language="javascript">
<!--
function showNotice()
{
         checkPost(document.form1, 'shownotice.php');
}
function ModifyOtherNotice()
{
	with(document.form1)
	{
		target = "_self";
		CaseType.value = "";
		action = "list.php";
		submit();	
	}
}
//-->
</script>
<br />

<form name="form1" method="post" target="_blank">
<input type="hidden" name="CaseType" value="<?=$CaseType?>">
<input type="hidden" name="startdate" value="<?=$startdate?>">
<input type="hidden" name="enddate" value="<?=$enddate?>">
<input type="hidden" name="targetID" value="<?=$targetID?>">
<?
	for($i=0;$i<sizeof($id_ary);$i++)
		echo "<input type=hidden name='id_ary[]' value='".$id_ary[$i]."'>";
?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<? if($print_letter) {?>
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($msg);?></td>
</tr>
<? } else {?>
<tr>
	<td align='center'><?=$linterface->GET_SYS_MSG($msg);?></td>
</tr>
<? } ?>

<? if($print_letter) {
	// <option value="1"> $i_general_Format_word </option>
	?>
<tr>
	<td align="center" colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5">
        <tr>
			<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_general_Format?>: </span></td>
			<td>
				<select name="format">
                	<option value="0"><?=$i_general_Format_web?></option>
                	
				</select>
				<?= $linterface->GET_BTN($button_view, "button", "javascript:showNotice();")?>
			</td>
		</tr>
		
		</table>
    </td>
</tr>
<? } ?>
<tr>
        <td align="center" colspan="2">
        <table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
                <tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
                <tr>
                        <td align="center"><?=$buttonDisplay?></td>
                </tr>
        </table>
        </td>
</tr>

</table>
<br />
</form>

<?
$linterface->LAYOUT_STOP();
?>