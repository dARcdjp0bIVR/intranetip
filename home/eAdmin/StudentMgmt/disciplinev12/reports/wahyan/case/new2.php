<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
        
intranet_auth();
intranet_opendb();
$lc = new libclass();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if (!($plugin['Disciplinev12'] && $sys_custom['ediscipline_case_report']))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "WahYan_CaseReport";

$TAGS_OBJ[] = array($iDiscipline['CaseReport'], "index.php", 0);
$TAGS_OBJ[] = array($iDiscipline['CreateReport'], "new.php", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$STEPS_OBJ[] = array($iDiscipline['SelectCaseReport'], 0 );
$STEPS_OBJ[] = array($iDiscipline['select_students'], 0 );
$STEPS_OBJ[] = array($iDiscipline['ReportInfo'], 1);

### case selection
$DisplayCase = $ldiscipline->returnCaseName($CaseType);	

### Selected students
if (is_array($student) && sizeof($student)>0)
{
    $list = implode(",", $student);
    $namefield = getNameFieldWithClassNumberByLang();
    $sql = "SELECT UserID , $namefield FROM INTRANET_USER WHERE RecordType = 2 AND UserID IN ($list)";
    $array_students = $ldiscipline->returnArray($sql,2);
    
    for($i=0;$i<sizeof($array_students);$i++)
    {
	 	   list($id, $name) = $array_students[$i];
	 	   $DisplaySelectedStudent .= $name . "<br>";
    }
}

##################################################################
## Case
##################################################################
## common fields
switch($CaseType)
{
	case "3":		//Case Record (form1)
		$DisplayStudentsInvolved = true;
		$DisplayTeachersInvolved = true;
		$DisplayDetails = true;
		$DetailsBlank = true;
		break;
	case "4":		//Case Record (form4)
		$DisplayVenue = true;	
		$DisplayStudentsInvolved = true;
		$DisplayTeachersInvolved = true;
		$DisplayDetails = true;
		$DisplayRemarks = true;
		break;
	case "5":		//Warning Letter
		$DisplayDetails = true;
		break;
}


## extra fields

$extra = "";

switch($CaseType)
{
	case "1":		//Violation of Mobile Phone Regulations
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['Reason_s'] ." <span class=\"tabletextrequire\">*</span></td>";
		$extra .= "<td class=\"tabletext\">";
		$MobileReasons = $ldiscipline->returnMobileReasons();
		for($i=0;$i<sizeof($MobileReasons);$i++)
		{
			list($id, $r) = $MobileReasons[$i];
			$extra .= "<input type='checkbox' name='reason[]' value='{$id}' id='r". $i."'> <label for='r". $i."'>". $r . "</label><br>";
		}
		$extra .= "<input type='checkbox' name='other_r' value='1' id='r'> <label for='r'>". $iDiscipline['Others'] . "</label>: <input type='text' name='other_reason' class='textboxtext3'><br>";
		$extra .= "</td>";
		$extra .= "</tr>";
		break;
		
	case "2":		//Violation of School Regulations
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['DatesTimes'] ." <span class=\"tabletextrequire\">*</span></td>";
		$extra .= "<td class=\"tabletext\">". $linterface->GET_TEXTAREA("DatesTimes","") ."</td>";
		$extra .= "</tr>";
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['Details'] ." <span class=\"tabletextrequire\">*</span></td>";
		$extra .= "<td class=\"tabletext\">". $linterface->GET_TEXTAREA("Details","") ."</td>";
		$extra .= "</tr>";
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['Punishment'] ." <span class=\"tabletextrequire\">*</span></td>";
		$extra .= "<td class=\"tabletext\">". $linterface->GET_TEXTAREA("Punishment","") ."</td>";
		$extra .= "</tr>";
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['RecordInBlackBook'] ." </td>";
		$extra .= "<td class=\"tabletext\">
			<input type='radio' name='RecordInBlackBook' value='1' id='RecordInBlackBook1' checked> <label for='RecordInBlackBook1'>{$i_general_yes}</label> 
			<input type='radio' name='RecordInBlackBook' value='0' id='RecordInBlackBook0'> <label for='RecordInBlackBook0'>{$i_general_no}</label>
		</td>";
		$extra .= "</tr>";
		break;
	case "3":		//Case Record (form1)
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['ReferenceNo'] ." </td>";
		$extra .= "<td class=\"tabletext\"><input type='text' name='RefNo' class='textboxtext'></td>";
		$extra .= "</tr>";
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['CaseHandledby'] ." </td>";
		$extra .= "<td class=\"tabletext\"><input type='text' name='CaseHandledby' class='textboxtext'></td>";
		$extra .= "</tr>";
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['FollowUp'] ." </td>";
		$extra .= "<td class=\"tabletext\"><input type='text' name='FollowUp' class='textboxtext'></td>";
		$extra .= "</tr>";
		break;
	case "4":		//Case Record (form4)
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['CourseOfAction'] ." <span class=\"tabletextrequire\">*</span></td>";
		$extra .= "<td class=\"tabletext\">";
		$CommonAction = $ldiscipline->returnCommonAction();
		for($i=0;$i<sizeof($CommonAction);$i++)
		{
			list($id, $a) = $CommonAction[$i];
			$extra .= "<input type='radio' name='ActionTaken' value='{$id}' id='a". $i."'> <label for='a". $i."'>". $a . "</label><br>";
		}
		$extra .= "<input type='radio' name='ActionTaken' value='0' id='a00'> <label for='a00'>". $iDiscipline['Others'] . "</label>: <input type='text' name='other_action' class='textboxtext3'><br>";
		$extra .= "</td>";
		$extra .= "</tr>";
		
		$extra .= "<input type='hidden' name='VenueFields' value=''>";
		break;
	case "6":		//Improper School Uniform
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['UniformProblems'] ." <span class=\"tabletextrequire\">*</span></td>";
		$extra .= "<td class=\"tabletext\">";
		$UniformProblems = $ldiscipline->returnUniformProblem();
		for($i=0;$i<sizeof($UniformProblems);$i++)
		{
			list($id, $r) = $UniformProblems[$i];
			$extra .= "<input type='checkbox' name='problems[]' value='{$id}' id='r". $i."'> <label for='r". $i."'>". $r . "</label><br>";
		}
		$extra .= "<input type='checkbox' name='other_p' value='1' id='other_p'> <label for='other_p'>". $iDiscipline['Others'] . "</label>: <input type='text' name='other_problem' class='textboxtext3'><br>";
		$extra .= "</td>";
		$extra .= "</tr>";
		
		$extra .= "<tr>";
		$extra .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['CourseOfAction'] ." <span class=\"tabletextrequire\">*</span></td>";
		$extra .= "<td class=\"tabletext\">";
		$UniformAction = $ldiscipline->returnUniformAction();
		for($i=0;$i<sizeof($UniformAction);$i++)
		{
			list($id, $p) = $UniformAction[$i];
			$extra .= "<input type='radio' name='ActionTaken' value='{$id}' id='p". $i."'> <label for='p". $i."'>". $p . "</label><br>";
		}
		$extra .= "<input type='radio' name='ActionTaken' value='0' id='p00'> <label for='p00'>". $iDiscipline['Others'] . "</label>: <input type='text' name='other_action' class='textboxtext3'><br>";
		$extra .= "</td>";
		$extra .= "</tr>";
		
		$extra .= "<input type='hidden' name='ActionFields' value=''>";
		
		break;
	
}
##################################################################

$VenueOption = "<tr>";
$VenueOption .= "<td valign=\"top\" nowrap=\"nowrap\" class=\"formfieldtitle tabletext\" width=\"30%\">". $iDiscipline['Venue'] ." <span class=\"tabletextrequire\">*</span></td>";
$VenueOption .= "<td class=\"tabletext\">";
$VenueChoices = $ldiscipline->returnVenue();
for($i=0;$i<sizeof($VenueChoices);$i++)
{
	list($id, $v) = $VenueChoices[$i];
	$VenueOption .= "<input type='radio' name='Venue' value='{$id}' id='v". $i."'> <label for='v". $i."'>". $v . "</label><br>";
}
$VenueOption .= "<input type='radio' name='Venue' value='0' id='v00'> <label for='v00'>". $iDiscipline['Others'] . "</label>: <input type='text' name='other_venue' class='textboxtext3'><br>";
$VenueOption .= "</td>";
$VenueOption .= "</tr>";

if($DisplayStudentsInvolved)
{
	### start for searching ###
	#get required data
	$data_ary = array();
	//$sql = "SELECT ClassID,ClassName FROM INTRANET_CLASS WHERE RecordStatus = 1 ORDER BY ClassName";
	//$result = $lc->returnArray($sql,2);
	$sql = "select ClassTitleEN from YEAR_CLASS where AcademicYearID=". Get_Current_Academic_Year_ID()." order by Sequence";
	$result = $lc->returnVector($sql);

	for ($i = 0; $i < sizeof($result); $i++) 
	{
		//list($this_classid, $this_classname) = $result[$i];
		$this_classname = $result[$i];
		
		$name_field = getNameFieldByLang();
		$sql1 = "SELECT UserID, $name_field, ClassNumber,UserLogin FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '".$this_classname."' ORDER BY ClassNumber";
		$result1 = $lc->returnArray($sql1,4);
		for ($j = 0; $j < sizeof($result1); $j++) 
		{
			list($this_userid, $this_stu_name, $this_class_number, $this_userlogin) = $result1[$j];
			$data_ary[] = array($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin);
		}
	}

	#define yui array (Search by input format )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
	
		$temp_str = $this_classname . $this_class_number. " ". $this_stu_name;
		if($this_class_number)
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		else
			$temp_str2 = $this_stu_name;
		
		//($i == 0) ? $liList = "<li class=\"\" style=\"display: none;\">". $temp_str ."</li>\n" : $liList = "<li style=\"display: none;\">". $temp_str ."</li>\n";
		$liArr .= "[\"". $temp_str ."\", \"". $temp_str2 ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr .= "" : $liArr .= ",\n";
	}
	
	foreach ($data_ary as $key => $row) 
	{
		$field1[$key] = $row[0];	//user id
		$field2[$key] = $row[1];	//class name
		$field3[$key] = $row[2];	//class number
		$field4[$key] = $row[3];	//stu name
		$field5[$key] = $row[4];	//login id
	}
	array_multisort($field5, SORT_ASC, $data_ary);
	
	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary);$i++)
	{
		list($this_userid, $this_classname, $this_class_number, $this_stu_name, $this_userlogin) = $data_ary[$i];
	
		if($this_class_number)
			$temp_str2 = $this_stu_name . " (". $this_classname ."-". $this_class_number .")";
		else
			$temp_str2 = $this_stu_name;
		
		$liArr2 .= "[\"". $this_userlogin ."\", \"". $temp_str2 ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr2 .= "" : $liArr2 .= ",\n";
	}
	### end for searching

	$select_studentinvolved = $linterface->GET_SELECTION_BOX($array_studentinvolved, "name='studentinvolved[]' class='select_studentlist' size='10' multiple='multiple'", "");
	$button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['studentinvolved[]'])");	
}

if($DisplayTeachersInvolved)
{
	### start for searching ###
	#get required data
	$data_ary2 = array();
	$name_field = getNameFieldByLang();
	$sql1 = "SELECT UserID, $name_field, UserLogin FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus = 1 order by EnglishName";
	$result1 = $lc->returnArray($sql1);
	for ($j = 0; $j < sizeof($result1); $j++) 
	{
		list($this_userid, $this_name, $this_userlogin) = $result1[$j];
		$data_ary2[] = array($this_userid, $this_name, $this_userlogin);
	}

	#define yui array (Search by login id )
	for($i=0;$i<sizeof($data_ary2);$i++)
	{
		list($this_userid, $this_name, $this_userlogin) = $data_ary2[$i];
	
		$temp_str2 = $this_name;
		
		$liArr3 .= "[\"". $this_userlogin ."\", \"". $temp_str2 ."\", \"". $this_userid ."\"]";
		($i == (sizeof($result)-1)) ? $liArr3 .= "" : $liArr3 .= ",\n";
	}
	### end for searching

	$select_teacherinvolved = $linterface->GET_SELECTION_BOX($array_teacherinvolved, "name='teacherinvolved[]' class='select_studentlist' size='10' multiple='multiple'", "");
	$button_remove_html2 = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['teacherinvolved[]'])");	
}
?>

<? if($DisplayStudentsInvolved || $DisplayTeachersInvolved) {?>
<link type="text/css" rel="stylesheet" href="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.css">

<style type="text/css">
    #statesmod {position:relative;}
    #statesautocomplete {position:relative;width:22em;margin-bottom:1em;}/* set width of widget here*/
    #statesautocomplete {z-index:9000} /* for IE z-index of absolute divs inside relative divs issue */
    #statesinput {_position:absolute;width:100%;height:1.4em;z-index:0;} /* abs for ie quirks */
    #statescontainer, #statescontainerCC, #statescontainerBCC {position:absolute;top:0.3em;width:100%}
    #statescontainer .yui-ac-content, #statescontainerCC .yui-ac-content, #statescontainerBCC .yui-ac-content {position:absolute;width:100%;border:1px solid #404040;background:#eeeeee;overflow:hidden;z-index:9050;}
    #statescontainer .yui-ac-shadow, #statescontainerCC .yui-ac-shadow, #statescontainerBCC .yui-ac-shadow {position:absolute;margin:.3em;width:100%;background:#a0a0a0;z-index:9049;}
    #statescontainer ul, #statescontainerCC ul, #statescontainerBCC ul {padding:5px 0;width:100%;}
    #statescontainer li, #statescontainerCC li, #statescontainerBCC li {padding:0 5px;cursor:default;white-space:nowrap;}
    #statescontainer li.yui-ac-highlight, #statescontainerCC li.yui-ac-highlight, #statescontainerBCC li.yui-ac-highlight {background:#bbbbbb;}
    #statescontainer li.yui-ac-prehighlight, #statescontainerCC li.yui-ac-prehighlight, #statescontainerBCC li.yui-ac-prehighlight {background:#FFFFFF;}
    
    
	#statesmod div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,form,fieldset,input,textarea,p,blockquote{margin:0;padding:0;}
	#statesmod table{border-collapse:collapse;border-spacing:0;}
	#statesmod fieldset,img{border:0;}
	#statesmod address,caption,cite,code,dfn,em,strong,th,var{font-style:normal;font-weight:normal;}
	#statesmod ol,ul {list-style:none;}
	#statesmod caption,th {text-align:left;}
	#statesmod h1,h2,h3,h4,h5,h6{font-size:100%;font-weight:normal;}
	#statesmod q:before,q:after{content:'';}
	#statesmod abbr,acronym {border:0;}
	#statesmod {font:13px arial,helvetica,clean,sans-serif;*font-size:small;*font:x-small;}
</style>
<? } ?>

<script language="javascript">
<!--
function checkForm()
{
	var flag=0;
	var obj = document.form1;
	
	if(!check_date(obj.RecordDate, "<?php echo $i_invalid_date; ?>.")) return false;
	<?
		switch($CaseType)
		{
			case "1": ?>			
				flag = countChecked(obj, 'reason[]');
				
				if(obj.other_r.checked)
				{
					if(obj.other_reason.value=='')	
					{
						alert("<?php echo $i_alert_pleasefillin.$iDiscipline['Others']; ?>.");
						return false;
					}
					
					flag = 1;
				}
				
				if(!flag) alert("<?=$iDiscipline['NoReasonSelected']?>");
			<?	break;
			case "2": ?>
				if(!check_text(obj.DatesTimes, "<?php echo $i_alert_pleasefillin.$iDiscipline['DatesTimes']; ?>.")) return false;
				if(!check_text(obj.Details, "<?php echo $i_alert_pleasefillin.$iDiscipline['Details']; ?>.")) return false;
				if(!check_text(obj.Punishment, "<?php echo $i_alert_pleasefillin.$iDiscipline['Punishment']; ?>.")) return false;
				
				flag = 1;
			<?	break; 
			case "3": ?>
				checkOptionAll(obj.elements["studentinvolved[]"]);
				checkOptionAll(obj.elements["teacherinvolved[]"]);
				flag = 1;
			<?	break; 
			case "4": ?>
				//if(!check_time2(obj.RecordTime, "<?php echo $i_SMS_InvalidTime; ?>.")) return false;
				venue_select = returnChecked(obj,'Venue');
				if(!venue_select)
				{
					alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Venue']?>");
					obj.Venue[0].focus();
					return false;
				}
				if(venue_select==0)
				{
					if(!check_text(obj.other_venue, "<?php echo $i_alert_pleasefillin.$iDiscipline['Others']; ?>.")) return false;
				}
				switch(venue_select)
				{
					case "1":
						if(!check_text(obj.Room_1, "<?php echo $i_Homework_Error_Empty; ?>.")) return false;
						obj.VenueFields.value = obj.VenueFields.value + '[#Room#]'+ obj.Room_1.value;
						break;
				}
				
				if(!check_text(obj.Details, "<?php echo $i_alert_pleasefillin.$iDiscipline['DetailsDescription']; ?>.")) return false;
				
				action_select = returnChecked(obj,'ActionTaken');
				if(!action_select)
				{
					alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['CourseOfAction']?>");
					obj.ActionTaken[0].focus();
					return false;
				}
				if(action_select==0)
				{
					if(!check_text(obj.other_action, "<?php echo $i_alert_pleasefillin.$iDiscipline['Others']; ?>.")) return false;
				}
				
				checkOptionAll(obj.elements["studentinvolved[]"]);
				checkOptionAll(obj.elements["teacherinvolved[]"]);
				flag = 1;
			<?	break;
			case "5": ?>
				if(!check_text(obj.Details, "<?php echo $i_alert_pleasefillin.$iDiscipline['DetailsOffence']; ?>.")) return false;
				flag = 1;
			<?	break;
			case "6": ?>
				flag = countChecked(obj, 'problems[]');
				
				if(obj.other_p.checked)
				{
					if(obj.other_problem.value=='')	
					{
						if(!check_text(obj.other_problem, "<?php echo $i_alert_pleasefillin.$iDiscipline['Others']; ?>.")) return false;
					}
					flag = 1;
				}
				
				if(!flag) 
				{
					alert("<?=$iDiscipline['NoProblemSelected']?>");
					return false;
				}
				
				/* check school action */
				flag = 0;
				action_select = returnChecked(obj,'ActionTaken');
				if(!action_select)
				{
					alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['CourseOfAction']?>");
					return false;
				}
				
				switch(action_select)
				{
					case "0":
						if(!check_text(obj.other_action, "<?php echo $i_alert_pleasefillin.$iDiscipline['Others']; ?>.")) return false;
						break;
					case "1":
						if(!check_text(obj.ActionTeacherName_1, "<?php echo $i_Homework_Error_Empty; ?>.")) return false;
						if(!check_date(obj.ActionDate_1, "<?php echo $i_invalid_date; ?>.")) return false;
						if(!check_time2(obj.ActionTime_1, "<?php echo $i_SMS_InvalidTime; ?>.")) return false;
						
						obj.ActionFields.value = '';
						obj.ActionFields.value = obj.ActionFields.value + '[#ActionTeacherName#]'+ obj.ActionTeacherName_1.value;
						obj.ActionFields.value = obj.ActionFields.value + '[#ActionDate#]'+ obj.ActionDate_1.value;
						obj.ActionFields.value = obj.ActionFields.value + '[#ActionTime#]'+ obj.ActionTime_1.value;
						obj.ActionFields.value = obj.ActionFields.value + '[#ActionAPM#]'+ obj.ActionAPM_1.value;
						
						obj.other_action.value='';
						break;
					case "3":
						if(!check_date(obj.ActionDate_3, "<?php echo $i_invalid_date; ?>.")) return false;
						
						obj.ActionFields.value = '';
						obj.ActionFields.value = obj.ActionFields.value + '[#ActionDate#]'+ obj.ActionDate_3.value;
						
						obj.other_action.value='';
						break;
				}
								
				flag = 1;
			<?	break;
			
			
		}		
		
	?>
	
	if(!flag)	return false;
}		
//-->
</script>

<form name="form1" action="new_update.php" onsubmit="return checkForm();" method="POST">
<input type="hidden" name="CaseType" value="<?=$CaseType?>" />
<?
for ($i=0; $i<sizeof($array_students); $i++)
{
     list($id, $name) = $array_students[$i];
?>
<input type="hidden" name="student[]" value="<?=$id?>" />
<?
}
?>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
        <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>

<tr>
	<td>
		<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$iDiscipline['SelectCaseReport']?></td>
				<td class="tabletext"><?=$DisplayCase?></td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%"><?=$i_general_students_selected?></td>
				<td class="tabletext"><?=$DisplaySelectedStudent?></td>
			</tr>
			
			<?// if($DisplayRecordDate) {?>
			<tr valign="top">
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iDiscipline['RecordDate']?> <span class='tabletextrequire'>*</span></span></td>
				<td><?=$linterface->GET_DATE_PICKER("RecordDate",$RecordDate)?></td>
			</tr>
			<?// } ?>
			
			<?// if($DisplayRecordTime) {?>
			<tr valign="top">
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iDiscipline['RecordTime']?></span></td>
				<td><input name="RecordTime" type="text" class="textboxnum" maxlength="5" value="<?php echo date("H:i"); ?>"/></td>
			</tr>
			<?// } ?>
			
			<? if($DisplayVenue) {?>
			<?= $VenueOption?>
			<? } ?>
			
			<? if($DisplayStudentsInvolved) {?>
			<!-- Student Involved //-->
			<tr>
		        <td colspan="2">
		      	  <table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		      	  <tr>
		      	  	<td class="tabletext" width="40%"><?=$iDiscipline['StudentInvolved']?></td>
		      	  	<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		      	  	<td class="tabletext" width="60%"><?=$i_general_selected_students; ?></td>
		      	  </tr>
		      	  
		      	  <tr>
					<td class="tablerow2" valign="top">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
						<tr>
							<td class="tabletext"><?=$i_general_from_class_group?></td>
						</tr>
						<tr>
							<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('choose/index.php?fieldname=studentinvolved[]', 9)")?></td>
						</tr>
						<tr>
							<td class="tabletext"><i><?=$i_general_or?></i></td>
						</tr>
						<tr>
							<td class="tabletext">
							<?=$i_general_search_by_inputformat?><br \>
							<div id="statesautocomplete">
								<input type="text" class="tabletext" name="search1">
								<div id="statescontainer">
									<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
										<div style="display: none;" class="yui-ac-hd"></div>
										<div class="yui-ac-bd"></div>
										<div style="display: none;" class="yui-ac-ft"></div>
									</div>
									<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
								</div>
							</div>
							</td>
						</tr>	
						
						<tr>
							<td class="tabletext"><i><?=$i_general_or?></i></td>
						</tr>
						<tr>
							<td class="tabletext">
							<?=$i_general_search_by_loginid?><br \>
							<div id="statesautocomplete">
								<input type="text" class="tabletext" name="search2">
								<div id="statescontainerCC">
									<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
										<div style="display: none;" class="yui-ac-hd"></div>
										<div class="yui-ac-bd"></div>
										<div style="display: none;" class="yui-ac-ft"></div>
									</div>
									<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
								</div>
							</div>
							</td>
						</tr>	
						
						</table>	
					</td>
					<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
					<td align="left" valign="top">
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td align="left"><?= $select_studentinvolved ?></td>
						</tr>
						<tr>
							<td align="right"><?=$button_remove_html?></td>
						</tr>
						</table>
					</td>
				</tr>
		                </table>
		        </td>
			</tr>
			<? } ?>
			
			<? if($DisplayTeachersInvolved) {?>
			<!-- Teachers Involved //-->
			<tr>
		        <td colspan="2">
		      	  <table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
		      	  <tr>
		      	  	<td class="tabletext" width="40%"><?=$iDiscipline['TearchersInvolved']?></td>
		      	  	<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		      	  	<td class="tabletext" width="60%"><?=$i_general_selected_teachers; ?></td>
		      	  </tr>
		      	  
		      	  <tr>
					<td class="tablerow2" valign="top">
						<table width="100%" border="0" cellpadding="3" cellspacing="0">
						<tr>
							<td class="tabletext"><?=$iDiscipline['fromStaffList']?></td>
						</tr>
						<tr>
							<td class="tabletext"><?= $linterface->GET_BTN($button_select, "button", "javascript:newWindow('choose/index2.php?fieldname=teacherinvolved[]', 9)")?></td>
						</tr>
												
						<tr>
							<td class="tabletext"><i><?=$i_general_or?></i></td>
						</tr>
						<tr>
							<td class="tabletext">
							<?=$i_general_search_by_loginid?><br \>
							<div id="statesautocomplete">
								<input type="text" class="tabletext" name="search3">
								<div id="statescontainerBCC">
									<div style="display: none; width: 199px; height: 0px;" class="yui-ac-content">
										<div style="display: none;" class="yui-ac-hd"></div>
										<div class="yui-ac-bd"></div>
										<div style="display: none;" class="yui-ac-ft"></div>
									</div>
									<div style="width: 0pt; height: 0pt;" class="yui-ac-shadow"></div>
								</div>
							</div>
							</td>
						</tr>	
						
						</table>	
					</td>
					<td class="tabletext" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
					<td align="left" valign="top">
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td align="left"><?= $select_teacherinvolved ?></td>
						</tr>
						<tr>
							<td align="right"><?=$button_remove_html2?></td>
						</tr>
						</table>
					</td>
				</tr>
		                </table>
		        </td>
			</tr>
			<? } ?>
			
			
			<? if($DisplayDetails) {?>
			<tr valign="top">
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iDiscipline['DetailsDescription']?> <?if(!$DetailsBlank) {?><span class='tabletextrequire'>*</span><? } ?></span></td>
				<td><?=$linterface->GET_TEXTAREA("Details","")?></td>
			</tr>
			<? } ?>
			
			<!-- Start Form Content //-->		
			
			<?=$extra?>
			
			<!-- End Form Content //-->
			
			<? if($DisplayRemarks) {?>
			<tr valign="top">
				<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iDiscipline['Remarks']?> </span></td>
				<td><?=$linterface->GET_TEXTAREA("Remark","")?></td>
			</tr>
			<? } ?>
			
			
			<tr>
                    <td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$iDiscipline['PreviewReport']?></span></td>
                    <td><input type=checkbox name='print_letter' value='1'>
                    </td>
            </tr>
            
			</table>
		</td>
</tr>

<tr>
        <td><table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
                <tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field2?></td></tr>
                <tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
                <tr>
                        <td align="center">
                        <?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>
                        <?= $linterface->GET_ACTION_BTN($button_back, "button", "form1.action='new.php'; form1.submit()")?>
                        </td>
                </tr>
        </table>
        </td>
</tr>

</table>

<br />
<input type="hidden" name="flag" value="">
</form>

<?
        $linterface->LAYOUT_STOP();
?>

<? if($DisplayStudentsInvolved || $DisplayTeachersInvolved) {?>
<!-- Libary begins -->
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/yahoo.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dom.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/event-debug.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/animation.js"></script>
<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/autocomplete-debug-ip20.js"></script>
<!-- Library ends -->

<!-- In-memory JS array begins-->
<script type="text/javascript">
var statesArray = [
    <?= $liArr?>
];

var loginidArray = [
    <?= $liArr2?>
];

var teacherstatesArray = [
    <?= $liArr3?>
];

var delimArray = [
    ";"
];
</script>
<!-- In-memory JS array ends-->


<script type="text/javascript">
YAHOO.example.ACJSArray = function() {
    var oACDS, oAutoComp;
    return {
        init: function() {

            // Instantiate first JS Array DataSource
            oACDS = new YAHOO.widget.DS_JSArray(statesArray);
            // Instantiate first AutoComplete            
            oAutoComp = new YAHOO.widget.AutoComplete('search1','studentinvolved[]', 'statescontainer', oACDS);
            oAutoComp.queryDelay = 0;
            oAutoComp.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp.useShadow = true;
            oAutoComp.minQueryLength = 0;
            
            oACDS2 = new YAHOO.widget.DS_JSArray(loginidArray);
            oAutoComp2 = new YAHOO.widget.AutoComplete('search2','studentinvolved[]', 'statescontainerCC', oACDS2);
            oAutoComp2.queryDelay = 0;
            oAutoComp2.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp2.useShadow = true;
            oAutoComp2.minQueryLength = 0;
            
            
            oACDS3 = new YAHOO.widget.DS_JSArray(teacherstatesArray);
            oAutoComp3 = new YAHOO.widget.AutoComplete('search3','teacherinvolved[]', 'statescontainerBCC', oACDS3);
            oAutoComp3.queryDelay = 0;
            oAutoComp3.prehighlightClassName = "yui-ac-prehighlight";
            oAutoComp3.useShadow = true;
            oAutoComp3.minQueryLength = 0;
            
            
        },

        validateForm: function() {
            // Validate form inputs here
            return false;
        }
    };
}();

YAHOO.util.Event.addListener(this,'load',YAHOO.example.ACJSArray.init);
</script>

<script type="text/javascript" src="<?= $PATH_WRT_ROOT?>templates/yui/autocomplete_files/dpSyntaxHighlighter.js"></script>
<script type="text/javascript">
dp.SyntaxHighlighter.HighlightAll('code');
</script>
<? } ?>