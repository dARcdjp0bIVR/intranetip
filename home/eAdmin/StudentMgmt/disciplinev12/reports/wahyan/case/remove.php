<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$list = is_array($RecordID) ? implode(",",$RecordID) : $RecordID;

if($list!=""){
	$sql = "DELETE FROM DISCIPLINE_CASE_REPORT WHERE RecordID IN ($list)";
	$li->db_db_query($sql);
}

intranet_closedb();
?>

<form name="form1" action="list.php" method="POST">
<input type="hidden" name="CaseType" value="<?=$CaseType?>">
<input type="hidden" name="targetID" value="<?=$targetID?>">
<input type="hidden" name="startdate" value="<?=$startdate?>">
<input type="hidden" name="enddate" value="<?=$enddate?>">
<input type="hidden" name="msg" value="delete">
</form>

<script language="Javascript">
<!--
document.form1.submit();
//-->
</script>