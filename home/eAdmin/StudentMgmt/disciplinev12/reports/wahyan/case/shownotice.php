<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
        
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if (!($plugin['Disciplinev12'] && $sys_custom['ediscipline_case_report']))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

#################################
### Define MsWord file name
#################################
switch($CaseType)
{
	case "1":	$wordFilename = "mobile_notice.doc";		break;
	case "2":	$wordFilename = "school_reg_notice.doc";	break;
	case "3":	$wordFilename = "case_record_form1.doc";	break;
	case "4":	$wordFilename = "case_record_form4.doc";	break;
	case "5":	$wordFilename = "warning_letter.doc";		break;
	case "6":	$wordFilename = "uniform_notice.doc";		break;
}
#################################
#################################

$template = $ldiscipline->loadCaseReportTemplate($CaseType);
$template_header = $template['Head'];
$template_content = $template['Content'];
$template_footer = $template['Foot'];

$report_content = "";
//$page_breaker = "<P CLASS='breakhere'>";
//$page_breaker = "<P style='page-break-after:always'>";
$page_breaker = "<br clear=all style='page-break-before:always'>";

for($i=0;$i<sizeof($id_ary);$i++)
{
	$RecordID = $id_ary[$i];
	$sql = "select * from DISCIPLINE_CASE_REPORT where RecordID=$RecordID";
	$info_ary = $ldiscipline->returnArray($sql);
	
	list($RecordID, $CaseType, $RefNo, $RecordDate, $RecordTime, $StudentID, $ClassName, $ClassNumber, $UserID, $Reasons, $OthersReason, $DatesTimes, $Details, $Punishment, $RecordInBlackBook, $Remark, $Problems, $OthersProblem, $ActionTaken, $ActionFields, $OtherAction, $Venue, $VenueFields, $OtherVenue, $StudentsInvolved, $TeachersInvolved, $CaseHandledby, $FollowUp, $DateInput, $DateModified) = $info_ary[0];

	$name_field = getNameFieldByLang();
	$sql1 = "SELECT $name_field, EnglishName, ChineseName FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND UserID = '".$StudentID."'";
	$student_info = $ldiscipline->returnArray($sql1);
	
	$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus = 1 AND UserID = '".$UserID."'";
	$teacher_info = $ldiscipline->returnVector($sql2);
	
	## Start Report data
	$imgfile = get_file_content($intranet_root."/file/schoolbadge.txt");
	$SCHOOL_BADGE = ($imgfile != "") ? "<img src=\"[#ServerAddress#]/file/{$imgfile}\" width=120 height=60>\n" : "";
	
	$StudentName = $student_info[0][0];
	$StudentEngName = $student_info[0][1];
	$StudentChiName = $student_info[0][2];
	$TeacherName = $teacher_info[0];
	$DateStr = substr($RecordDate, 0, 10);
	$CurrentDateStr = date("Y-m-d");
	$RecordInBlackBookStr = $RecordInBlackBook ? "will be" : "will not be";
	$AcademicYear = getCurrentAcademicYear();
	$RefNo = trim($RefNo) ? $RefNo : "__________";
	$CaseHandledby = trim($CaseHandledby) ? $CaseHandledby : "__________________________________________________________";
	$FollowUp = trim($FollowUp) ? $FollowUp : "_______________________________________________________________";
	
	//-- Start Venue --//
	$Venue = $ldiscipline->returnVenue($Venue);
	if($VenueFields)
	{
		$FieldsAry = explode("[#", $VenueFields);
		for($f=0;$f<sizeof($FieldsAry);$f++)
		{
			if($FieldsAry[$f]=="")	continue;
			list($AF, $AR) = explode("#]", $FieldsAry[$f]);
			$Venue = str_replace("[#".$AF."#]", $AR, $Venue);
		}
	}
	$Venue = $OtherVenue ? "Others: ".$OtherVenue : $Venue;
	//-- End Venue --//
	
	//-- Start Involved --//
	$InvolvedPeople = "";
	if($StudentsInvolved)
	{
		$StudentsInvolved_ary = explode(",", $StudentsInvolved);
		for($si=0;$si<sizeof($StudentsInvolved_ary);$si++)
		{
			$temp_s = $StudentsInvolved_ary[$si];
			$sql1 = "SELECT $name_field FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND UserID = '".$temp_s."'";
			$temp = $ldiscipline->returnVector($sql1);
			$InvolvedPeople .= $temp[0] .", ";
		}
	}
	if($TeachersInvolved)
	{
		$TeachersInvolved_ary = explode(",", $TeachersInvolved);
		for($si=0;$si<sizeof($TeachersInvolved_ary);$si++)
		{
			$temp_t = $TeachersInvolved_ary[$si];
			$sql1 = "SELECT $name_field FROM INTRANET_USER WHERE RecordType = 1 AND RecordStatus = 1 AND UserID = '".$temp_t."'";
			$temp = $ldiscipline->returnVector($sql1);
			$InvolvedPeople .= $temp[0] .", ";
		}
	}
	$InvolvedPeople = substr($InvolvedPeople, 0, -2);
	//-- Start Involved --//
	
	switch($CaseType)
	{
		case 1:
			//---- mobile reasons ----//
			$ReasonsStr = "";
			$Reasons_ary = explode(",", $Reasons);
			$MobileReasons = $ldiscipline->returnMobileReasons();
			for($r=0;$r<sizeof($MobileReasons);$r++)
			{
				list($id,$name) = $MobileReasons[$r];
				if(in_array($id, $Reasons_ary))
					$ReasonsStr .= "- ". $name . "<br>";
			}
			if($OthersReason)	
				$ReasonsStr .= "- Others: ". $OthersReason . "<br>";
			break;	
		case 3:
			if(!trim($Details))
			{
				for($dd=0;$dd<38;$dd++)
					$Details.= "<p class=MsoNormal><span lang=EN-US style='font-family:\"Times New Roman\"'>__________________________________________________________________________<o:p></o:p></span></p><p class=MsoNormal><span lang=EN-US style='font-family:\"Times New Roman\"'><![if !supportEmptyParas]>&nbsp;<![endif]><o:p></o:p></span></p>";
			}
			else
				$Details = "<p class=MsoNormal><span lang=EN-US style='font-family:\"Times New Roman\"'>".$Details."<o:p></o:p></span></p>";
			break;
		case 4:
			$Action = $ldiscipline->returnCommonAction($ActionTaken);
			if($ActionFields)
			{
				$FieldsAry = explode("[#", $ActionFields);
				for($f=0;$f<sizeof($FieldsAry);$f++)
				{
					if($FieldsAry[$f]=="")	continue;
					list($AF, $AR) = explode("#]", $FieldsAry[$f]);
					if($AF=="ActionAPM")	// a.m. / p.m.
					{
						if($AR==1)	$Action = str_replace("[#".$AF."#]", "a.m.", $Action);
						if($AR==2)	$Action = str_replace("[#".$AF."#]", "p.m.", $Action);
					}
					else
					{
						$Action = str_replace("[#".$AF."#]", $AR, $Action);
					}
				}
			}
			$Action = $OtherAction ? $OtherAction : $Action;
			break;
		case 6:
			//---- uniform reasons ----//
			$Problems_ary = explode(",", $Problems);
			if(in_array("1", $Problems_ary))	$Belt = "v";
			if(in_array("2", $Problems_ary))	$Tie = "v";
			if(in_array("3", $Problems_ary))	$Shoes = "v";
			if(in_array("4", $Problems_ary))	$Coloured = "v";
			if(in_array("5", $Problems_ary))	$Ring = "v";
			if(in_array("6", $Problems_ary))	$Sweater = "v";
			if(in_array("7", $Problems_ary))	$Hair = "v";
			if(in_array("8", $Problems_ary))	$Blazer = "v";
			if(in_array("9", $Problems_ary))	$Trousers = "v";
			if(in_array("10", $Problems_ary))	$Jacket = "v";
			if(in_array("11", $Problems_ary))	$Shirt = "v";
			if($OthersProblem) 					$Others = "v";
			$OthersProblem = $OthersProblem ? $OthersProblem : "&nbsp;";
			
			$Action = $ldiscipline->returnUniformAction($ActionTaken);
			if($ActionFields)
			{
				$FieldsAry = explode("[#", $ActionFields);
				for($f=0;$f<sizeof($FieldsAry);$f++)
				{
					if($FieldsAry[$f]=="")	continue;
					list($AF, $AR) = explode("#]", $FieldsAry[$f]);
					if($AF=="ActionAPM")	// a.m. / p.m.
					{
						if($AR==1)	$Action = str_replace("[#".$AF."#]", "a.m.", $Action);
						if($AR==2)	$Action = str_replace("[#".$AF."#]", "p.m.", $Action);
					}
					else
					{
						$Action = str_replace("[#".$AF."#]", $AR, $Action);
					}
				}
			}
			$Action = $OtherAction ? $OtherAction : $Action;
			break;
	}
		
	## End Report data
	
	$report_entry = $template_content;
	# Replace data to template
	$report_entry = str_replace('[#SCHOOL_BADGE#]',	$SCHOOL_BADGE,	$report_entry);
	$report_entry = str_replace('[#StudentName#]',	$StudentName,	$report_entry);
	$report_entry = str_replace('[#ClassName#]',	$ClassName,		$report_entry);
	$report_entry = str_replace('[#ClassNumber#]',	$ClassNumber,	$report_entry);
	$report_entry = str_replace('[#ReasonsStr#]',	$ReasonsStr,	$report_entry);
	$report_entry = str_replace('[#TeacherName#]',	$TeacherName,	$report_entry);
	$report_entry = str_replace('[#DateStr#]',		$DateStr,		$report_entry);
	$report_entry = str_replace('[#CurrentDateStr#]',$CurrentDateStr,$report_entry);
	$report_entry = str_replace('[#DatesTimesStr#]',nl2br($DatesTimes),	$report_entry);
	$report_entry = str_replace('[#DetailsStr#]',	nl2br($Details),$report_entry);
	$report_entry = str_replace('[#PunishmentStr#]',nl2br($Punishment),	$report_entry);
	$report_entry = str_replace('[#RecordInBlackBookStr#]',	$RecordInBlackBookStr,	$report_entry);
	$report_entry = str_replace('[#RecordDate#]',	$RecordDate,	$report_entry);
	$report_entry = str_replace('[#RecordTime#]',	trim($RecordTime) ? $RecordTime : "---",	$report_entry);
	
	$report_entry = str_replace('[#Belt#]',			$Belt,			$report_entry);
	$report_entry = str_replace('[#Tie#]',			$Tie,			$report_entry);
	$report_entry = str_replace('[#Shoes#]',		$Shoes,			$report_entry);
	$report_entry = str_replace('[#Coloured#]',		$Coloured,		$report_entry);
	$report_entry = str_replace('[#Ring#]',			$Ring,			$report_entry);
	$report_entry = str_replace('[#Sweater#]',		$Sweater,		$report_entry);
	$report_entry = str_replace('[#Hair#]',			$Hair,			$report_entry);
	$report_entry = str_replace('[#Blazer#]',		$Blazer,		$report_entry);
	$report_entry = str_replace('[#Trousers#]',		$Trousers,		$report_entry);
	$report_entry = str_replace('[#Jacket#]',		$Jacket,		$report_entry);
	$report_entry = str_replace('[#Shirt#]',		$Shirt,			$report_entry);
	$report_entry = str_replace('[#Others#]',		$Others,		$report_entry);	
	$report_entry = str_replace('[#OthersProblem#]',$OthersProblem,	$report_entry);	
	$report_entry = str_replace('[#Action#]',		$Action,		$report_entry);	
	
	$report_entry = str_replace('[#StudentEngName#]',$StudentEngName, $report_entry);	
	$report_entry = str_replace('[#StudentChiName#]',$StudentChiName, $report_entry);	
	$report_entry = str_replace('[#Venue#]',		$Venue, 		$report_entry);	
	$report_entry = str_replace('[#Remarks#]',		trim($Remark) ? nl2br($Remark) : "---", 		$report_entry);	
	$report_entry = str_replace('[#InvolvedPeople#]', trim($InvolvedPeople) ? $InvolvedPeople : "---" , $report_entry);	
	$report_entry = str_replace('[#AcademicYear#]',		$AcademicYear, 		$report_entry);	
	$report_entry = str_replace('[#RefNo#]',		$RefNo,			$report_entry);	 
	$report_entry = str_replace('[#CaseHandledby#]',$CaseHandledby,	$report_entry);	
	$report_entry = str_replace('[#FollowUp#]',		$FollowUp, 		$report_entry);	
	 
	$report_content.=$report_entry;
	
	# Page break
	if ($i != sizeof($id_ary)-1){
	  	$report_content .= "\n".$page_breaker."\n";
	}
}

############################
### Start Report
############################
$response_content = "<center>";
$response_content .= $template_header;
/*
$response_content .= "
<STYLE TYPE=\"text/css\">
     P.breakhere {page-break-before: always}
</STYLE>
";
*/
$response_content .= $report_content;
$response_content .= $template_footer;
$response_content .= "</center>";

############################
### End Report
############################


if ($format == 1)    # Word
{
	$website = get_file_content($intranet_root."/file/email_website.txt");
    $website = ($website=="") ? "http://".$HTTP_SERVER_VARS["HTTP_HOST"] : $website;
    
    # Handle offline format
    $response_content = str_replace('[#ServerAddress#]',$website,$response_content);
    
    $output_filename = $wordFilename;
    output2browser($response_content,$output_filename);
    flush();
}
else        # Web
{
    $response_content = str_replace('[#ServerAddress#]',"",$response_content);
    echo $response_content;
}

intranet_closedb();
?>