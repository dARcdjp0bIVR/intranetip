<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "WahYan_CaseReport";

if (!($plugin['Disciplinev12'] && $sys_custom['ediscipline_case_report']))
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();

############################################################################################################

// $ldiscipline->authAccessReport();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);
if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"name=\"targetID\"");

############################################################################################################

$ts = time();
if ($startdate == "")
{
    $startdate = date('Y-m-d',getStartOfAcademicYear($ts));
}
if ($enddate == "")
{
    $enddate = date('Y-m-d',getEndOfAcademicYear($ts));
}

$CaseReportList = $ldiscipline->returnCaseReportList();
$DisplayCase = getSelectByArray($CaseReportList,"name='CaseType'", $CaseType);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($iDiscipline['CaseReport'], "index.php", 1);
$TAGS_OBJ[] = array($iDiscipline['CreateReport'], "new.php", 0);

$linterface->LAYOUT_START();

?>
<SCRIPT LANGUAGE=Javascript>
function checkForm()
{
	obj = document.form1;

	var flag= false;
	
	if(obj.CaseType.value != "")	flag = true;
	if(obj.targetClass.value != "")	flag = true;
// 	if(obj.targetID.value != "")	flag = true;
	if(obj.targetLogin.value != "")	flag = true;
	if(!(obj.startdate.value == "" || obj.startdate.value=="yyyy-mm-dd" ))	flag = true;
	if(!(obj.enddate.value == "" || obj.enddate.value=="yyyy-mm-dd" ))		flag = true;

	if(!flag)
	{
		alert("<?=$iDiscipline['PleaseSelectOption']?>");
		return false;	
	}
}
</SCRIPT>

<br />
<form name="form1" action="list.php" method="get" onsubmit="return checkForm();">
<table border="0" cellpadding="5" cellspacing="0" width="90%">
  <tr>
    <td class="tabletext formfieldtitle" valign="top" width="30%"><?=$i_ClassNameNumber?> </td>
    <td><?=$select_class?><?=$select_students?></td>
  </tr>

	<tr><td>&nbsp;</td><td class="tablerow2"><span class="tabletextremark">(<?=$i_general_alternative?>)</span>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?= $i_UserLogin; ?><br />
			<input type="text" name="targetLogin" class="textboxnum">
		</td></tr>
		</table>
		</td>
	</tr>
	
	<tr>
		<td class="tabletext formfieldtitle" valign="top" width="30%"><?=$iDiscipline['CaseReport']?> </td>
		<td><?=$DisplayCase?></td>
	</tr>
	
	<tr>
		<td class="tabletext formfieldtitle" valign="top"><?=$i_general_startdate?></td>
		<td class="tabletext"><?= $linterface->GET_DATE_FIELD("theStartDate", "form1", "startdate", $startdate, 1)?></td>
	</tr>
	
	<tr>
		<td class="tabletext formfieldtitle" valign="top"><?=$i_general_enddate?></td>
		<td class="tabletext"><?= $linterface->GET_DATE_FIELD("theEndDate", "form1", "enddate", $enddate, 1)?></td>
	</tr>

</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($button_view, "submit"); ?>
		</td>
	</tr>
</table>

</form>
<?
$linterface->LAYOUT_STOP();
?>