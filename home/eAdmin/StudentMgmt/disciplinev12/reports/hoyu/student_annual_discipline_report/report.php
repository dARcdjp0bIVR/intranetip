<?php
// Editing by

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12_cust();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HoYuPrimarySchCust']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}

echo $ldiscipline_ui->getHoYuAnnualDisciplineReport($_REQUEST);

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>

<style type="text/css">
@charset "utf-8";

/* CSS Document */
body { font-family: "Times New Roman, 新細明體"; padding:0; margin:0;}
html, body { margin: 0px; padding: 0px; }

.main_container { display:block; width:680px; height:880px; margin:auto; /*border:1px solid #DDD;*/ padding:30px 20px; position:relative; padding-bottom:90px; }
.second_page_header { min-height:80px; }
.page_break { page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0; }

.report_sch_header { clear:both; position:relative; vertical-align:center; }
.report_sch_header .sch_logo_td { vertical-align:top; align:center;}
.report_sch_header .sch_logo_td .hy_sch_logo { height:110px; width:110px; }
.report_sch_header h2 { text-align:center; min-height:22px; font-family:"Times New Roman, 新細明體"; font-size:20px; font-weight:bold; position:relative; padding-top: 2px; margin-bottom: 4px;}
.report_sch_header .chi_sch_name { letter-spacing: 6px; }

.report_title_header { clear:both; position:relative; vertical-align:center; }
.report_title_header h2 { text-align:center; font-family:"Times New Roman, 新細明體"; font-size:20px; font-weight:bold; position:relative; padding-top: 1px; margin-bottom: 1px; }

.student_info { float:left; padding-bottom:40px; }
.student_info td { font-family: "Times New Roman, 新細明體"; font-size:16px; align:left }

.record_title { font-family:"Times New Roman, 新細明體"; font-size:16px; }
.record_table { width:60%; margin-left: 30px; margin-bottom:40px; } 
.record_table td { font-family: "Times New Roman, 新細明體"; font-size:16px; }
/*.record_table .record_table_content { padding-left:20px; }*/
.record_table .record_table_content { text-align: center; vertical-align: center; }

.page_no { display:block; width:100%; bottom:-60px; padding:90px 0 0 0; text-align: center; position: absolute }
.page_no span { font-family: "Times New Roman, 新細明體"; font-size:16px; }
.page_no .class_teacher_field { position:absolute; left:30px; bottom:25px; border-top:1px solid #000; width:200px; padding-top:3px}
.page_no .principal_field { position:absolute; right:30px; bottom:25px; border-top:1px solid #000; width:200px; padding-top:3px}

</style>

