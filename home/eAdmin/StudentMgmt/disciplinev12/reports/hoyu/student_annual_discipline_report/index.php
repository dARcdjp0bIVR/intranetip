<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# user access right checking
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['HoYuPrimarySchCust']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Setting of Current Page
$CurrentPage = "Reports_Student_Annual_Discipline_Report";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['StudentAnnualDisciplineReport']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# School Year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onChange='changeTerm(this.value); showRankTargetResult(\"student\");' ", "", $selectYear);
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 

# Semester
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\">";
$selectSemesterHTML .= "</select>";

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">

function showResult(str)
{
	var yearClassId = '';
	if(str == 'student2ndLayer'){
		var options = $('select#rankTargetDetail\\[\\] option:selected');
		var delim = '';
		for(var i=0;i<options.length;i++){
			yearClassId += delim + options.get(i).value;
			delim = ',';
		}
	}
	
	var url = "../../ajaxGetLive.php?target=" + str;
	if(str == 'student'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&onchange="+encodeURIComponent("showRankTargetResult('student2ndLayer','');");
	}else if(str == 'student2ndLayer'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&studentFieldId=studentID[]&studentFieldName=studentID[]&YearClassID="+yearClassId;
	}
	url += "&academicYearId="+$('#selectYear').val();
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			if(str == 'student2ndLayer'){
				showIn = "spanStudent";	
			}
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
			
			if(str == 'student'){
				showRankTargetResult('student2ndLayer');
			}
		}
	);
}

function showRankTargetResult(val) {
	showResult(val);
}

function hideOptionLayer()
{
	$('.Form_Span').hide();
	$('.spanHideOption').hide();
	$('.spanShowOption').show();
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('.Form_Span').show();
	$('.spanHideOption').show();
	$('.spanShowOption').hide();
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	
	if($('select#studentID\\[\\] option:selected').length == 0){
		valid = false;
		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
		$('#TargetWarnDiv').show();
	}else{
		$('#TargetWarnDiv').hide();
	}
	
	if(!valid) return;
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target','');
		Block_Element('PageDiv');
		
		$.post(
			'report.php',
			$('#form1').serialize(),
			function(data){
				$('#ReportDiv').html(data);
				document.getElementById('div_form').className = 'report_option report_hide_option';
				$('.spanShowOption').show();
				$('.spanHideOption').hide();
				$('.Form_Span').hide();
				UnBlock_Element('PageDiv');
			}
		);
	} else if(format == 'print'){
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

function changeTerm(val) {
	var url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	
	$.get(
		url,
		{},
		function(responseText){
			$('#spanSemester').html(responseText);
			document.getElementById('selectSemester').style.border = "1px solid #A5ACB2";
		}
	);
}

$(document).ready(function(){
	changeTerm('<?=$selectYear?>');
	showRankTargetResult('student');
});

</script>

<div id="PageDiv">
<div id="div_form">
	<span id="spanShowOption" class="spanShowOption" style="display:none">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption']?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php" onsubmit="submitForm('print'); return false;">
		<table class="form_table_v30">
		
			<!-- Period -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HoYuPrimarySchCustReport']['SchoolYear']?></td>
				<td><table class="inside_form_table">
						<tr><td colspan="6"><?=$selectSchoolYearHTML?>&nbsp;<span id="spanSemester"><?=$selectSemesterHTML?></span></td></tr>
				</table></td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_UserStudentName?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td valign="top" nowrap>
								<!-- Class -->
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
									
								<!-- Student //-->
								<span id='spanStudent' style='display:inline;'>
									<select name="studentID[]" multiple size="5" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
								</span>
							</td>
						</tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['HoYuPrimarySchCustReport']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
		<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")?>
		<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="format" id="format" value="print">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
