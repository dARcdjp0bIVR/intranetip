<?php
// Editing by 
/*
 * Change Log
 *
 * 2018-10-04 (Bill): support access right settings [2018-0920-1312-34207]
 * 2018-02-07 (Isaac): added $NewPagePerClass;
 *
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

// General deploy this report [2015-0128-1121-06170]
//if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['DetentionPunishmentList']) {
if(!($ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || $ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Detention_Punishment_List-View"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Reports_Detention_Punishment_List";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($Lang['eDiscipline']['DetentionPunishmentList'], "", 1);

$lclass = new libclass();
$linterface = new interface_html();

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="JavaScript">
var loadingImg = '<?=$linterface->Get_Ajax_Loading_Image();?>';
var academicYearId = '<?=Get_Current_Academic_Year_ID()?>';

function checkViewFomat(id){
	if(id == "FormatHTML"){
		$("#printSeperateClass").show();
	} else {
		$("#printSeperateClass").hide();
	}
}

function checkForm(formObj)
{
	var isStartDateValid  = check_date_without_return_msg(document.getElementById('StartDate'));
	var isEndDateValid  = check_date_without_return_msg(document.getElementById('EndDate'));
	var isDatePeriodValid = true;
	var targetType = document.getElementById('TargetType').value;
	var targetIdObj = $('#TargetID option:selected');
	var statusSelected = 0;
	var isValid = true;
	
	Date_Picker_Check_Date_Format(document.getElementById('StartDate'),'DPWL-StartDate','');
	Date_Picker_Check_Date_Format(document.getElementById('EndDate'),'DPWL-EndDate','');
	if(isStartDateValid && isEndDateValid) {
		if(document.getElementById('StartDate').value.Trim() > document.getElementById('EndDate').value.Trim()) {
			isDateperiodValid = false;
			$('#WarnDatePeriod').show();
		}else{
			$('#WarnDatePeriod').hide();
		}
	}
	
	if(!isStartDateValid || !isEndDateValid || !isDatePeriodValid) {
		isValid = false;
	}
	
	if(targetIdObj.length == 0){
		$('#WarnTargetID').show();
		isValid = false;
	}else{
		$('#WarnTargetID').hide();
	}
	
	var statusObjs = document.getElementsByName('AttendanceStatus[]');
	for(var i=0;i<statusObjs.length;i++) {
		if(statusObjs[i].checked) {
			statusSelected++;
		}
	}
	if(statusSelected == 0){
		$('#WarnAttendanceStatus').show();
		isValid = false;	
	}else{
		$('#WarnAttendanceStatus').hide();
	}
	
	if(isValid) {
		formObj.submit();
	}
}

function targetTypeChanged(obj)
{
	var selectedTargetType = obj.value;
	if(selectedTargetType == 'form' || selectedTargetType == 'class'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'get_live.php',
			{
				'target':selectedTargetType,
				'academicYearId':academicYearId,
				'fieldId':'TargetID',
				'fieldName':'TargetID[]'
			},
			function(data){
				$('#selectAllTargetBtn').show();
				$('#DivSelectAllRemark').show();
			}
		);
	}else if(selectedTargetType == 'student'){
		$('#DivRankTargetDetail').html(loadingImg).load(
			'get_live.php',
			{
				'target':'student',
				'academicYearId':academicYearId,
				'fieldId':'studentTargetID',
				'fieldName':'studentTargetID[]',
				'onchange':'getStudentSelection()',
				'divStudentSelection':'DivStudentSelection'
			},
			function(data){
				$('#selectAllTargetBtn').hide();
				$('#DivSelectAllRemark').show();
				getStudentSelection();
			}
		);
	}else{
		$('#DivRankTargetDetail').html('');
		$('#selectAllTargetBtn').hide();
		$('#DivSelectAllRemark').hide();
	}
}

function getStudentSelection()
{
	var selectedYearClassId = [];
	var yearClassObj = document.getElementById('studentTargetID');
	for(var i=0;i<yearClassObj.options.length;i++) {
		if(yearClassObj.options[i].selected) {
			selectedYearClassId.push(yearClassObj.options[i].value);
		}
	}
	
	$('#DivStudentSelection').html(loadingImg);
	$.post(
		'get_live.php',
		{
			'target':'student2ndLayer',
			'academicYearId':academicYearId,
			'fieldId':'studentTargetID',
			'fieldName':'studentTargetID[]',
			'studentFieldName':'TargetID[]',
			'studentFieldId':'TargetID',
			'YearClassID[]':selectedYearClassId 
		},
		function(data){
			$('#DivStudentSelection').html(data);
		}
	);
}

function timesChanged(obj)
{
	var num = parseInt(obj.value);
	if(isNaN(num) || num < 0 || num == '') {
		obj.value = 0;
	}
}
</script>

<form name="form1" method="post" action="detention_punishment_list.php" target="_blank" onsubmit="checkForm(this);return false;">
<table width="100%">
	<tr>
		<td>
			<table width="96%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr align="left" class="report_show_option">
								<td height="20" class="tabletextremark"><em>- <?=$i_Discipline_Statistics_Options?> -</em></td>
							</tr>
						</table>
						<table  class="form_table_v30">
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['SysMgr']['Periods']['FieldTitle']['PeriodRange']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$Lang['General']['From'].'&nbsp;'.$linterface->GET_DATE_PICKER("StartDate",date("Y-m-d")).$Lang['General']['To'].'&nbsp;'.$linterface->GET_DATE_PICKER("EndDate",date("Y-m-d"))?>
									<?=$linterface->Get_Form_Warning_Msg('WarnDatePeriod', $Lang['General']['JS_warning']['InvalidDateRange'], 'WarnDatePeriod', false);?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="field_title">
									<?=$i_Discipline_Target?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<table border="0">
										<tr>
											<td valign="top">
												<select name="TargetType" id="TargetType" onchange="targetTypeChanged(this);">
													<option value="#" selected="selected">-- <?=$i_general_please_select?> --</option>
													<option value="form"><?=$i_Discipline_Form?></option>
													<option value="class"><?=$i_Discipline_Class?></option>
													<option value="student"><?=$i_UserStudentName?></option>
												</select>
											</td>
											<td>
												<div id='DivRankTargetDetail'></div>
												<?=$linterface->GET_BTN($Lang['Btn']['SelectAll'], "button", "if(document.getElementById('rankTargetDetail[]')){Select_All_Options('rankTargetDetail[]', true);}return false;","selectAllTargetBtn",' style="display:none;"')?>
											</td>
										</tr>
									</table>
									<?=$linterface->Get_Form_Warning_Msg('WarnTargetID', $Lang['eDiscipline']['WarningMsgArr']['SelectTarget'], 'WarnTargetID', false);?>
									<div class="tabletextremark" id="DivSelectAllRemark" style="display:none;">(<?=$i_Discipline_Press_Ctrl_Key?>)</div>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['eDiscipline']['DetentionStatus']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$linterface->Get_Checkbox('AttendanceStatusABS', 'AttendanceStatus[]', 'ABS', $isChecked=1, $Class='', $Lang['eDiscipline']['DetentionAbsent'], $Onclick='', $Disabled='')?>
									<?=$linterface->Get_Checkbox('AttendanceStatusPRE', 'AttendanceStatus[]', 'PRE', $isChecked=1, $Class='', $Lang['eDiscipline']['DetentionPresent'], $Onclick='', $Disabled='')?>
									<?=$linterface->Get_Checkbox('AttendanceStatusNTA', 'AttendanceStatus[]', 'NTA', $isChecked=1, $Class='', $Lang['eDiscipline']['DetentionNotTakeAttandance'], $Onclick='', $Disabled='')?>
									<?=$linterface->Get_Form_Warning_Msg('WarnAttendanceStatus', $Lang['eDiscipline']['WarningMsgArr']['SelectAttendanceStatus'], 'WarnAttendanceStatus', false);?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['eDiscipline']['DetentionTimes']?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$linterface->GET_TEXTBOX_NUMBER("Times", "Times", '0', $OtherClass='', array('onchange'=>'timesChanged(this);','size'=>'5')).'&nbsp;'.$Lang['eDiscipline']['DetentionTimesAbove']?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['General']['ViewFormat']?>
								</td>
								<td>
									<?=$linterface->Get_Radio_Button("FormatHTML", "Format", $Lang['General']['HTML'], 1, "", $Lang['General']['HTML'], "checkViewFomat(this.id)",0)?>
									<?=$linterface->Get_Radio_Button("FormatCSV", "Format", $Lang['General']['CSV'], 0, "", $Lang['General']['CSV'], "checkViewFomat(this.id)",0)?>
								</td>
							</tr>
							<tr id="printSeperateClass">
							<td valign="top" nowrap="nowrap"  class="field_title">
									<?=$Lang['eDiscipline']['PrintNewPagePerClass']?>
								</td>
								<td>
									<?=$linterface->Get_Radio_Button("SeperateByClassYes", "SeperateByClass", true, 0, "", $Lang['General']['Yes'], "",0)?>
									<?=$linterface->Get_Radio_Button("SeperateByClassNo", "SeperateByClass", false, 1, "", $Lang['General']['No'], "",0)?>
								</td>
							</tr>
						</table>
						<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
							<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
							<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "", "submitBtn")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>