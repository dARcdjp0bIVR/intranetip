<?php
// modifying by: 

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-MonthlyReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Class #
$select_class = $ldiscipline->getSelectClass("name=\"targetClass\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, false);


function RepQuotes($strx) {
	$patterns = array("/\"/", "/&quot;/");
	$replacements = "/&quot;&quot;/";
	return preg_replace($patterns, $replacements, $strx);
}

$TAGS_OBJ[] = array($eDiscipline['MonthlyReport'], "");

# menu highlight setting
$CurrentPage = "MonthlyReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">

function doExport()
{
	document.form1.action = "monthly_report_export.php";
	document.form1.submit();
	document.form1.action = "";
}

function doPrint()
{
	document.form1.action = "monthly_report_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "_self";
}

</script>

<br>

<?php
echo '<form name="form1" method="post" action="">';
$studentID = $lclass ->getClassStudentListOrderByClassNo($targetClass);

# Table Header
$detail_table =  "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">\n";
$detail_table .= "<tr>";
$detail_table .= "";
$detail_table .= "<td colspan=\"2\"><font size=\"4\">".$i_general_class.": ".$targetClass."</font></td>";
$detail_table .= "<td colspan=\"5\"><font size=\"3\">( ".$Section2Fr."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; to &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$Section2To." )</font></td>";
$detail_table .= "";
$detail_table .= "</tr>";
$detail_table .= "<tr class=\"tablebluetop\">";
$detail_table .= "<td class=\"tabletoplink\" width=\"8.5%\">".$i_UserClassNumber."</td>";
$detail_table .= "<td class=\"tabletoplink\" width=\"33%\">".$i_UserStudentName."</td>";
$detail_table .= "<td class=\"tabletoplink\" width=\"10%\">".$i_Profile_Late."</td>";
$detail_table .= "<td class=\"tabletoplink\" width=\"10%\">Homework</td>";
$detail_table .= "<td class=\"tabletoplink\" width=\"10%\">Uniform</td>";
$detail_table .= "<td class=\"tabletoplink\" width=\"10%\">I.D.</td>";
$detail_table .= "<td class=\"tabletoplink\" width=\"18.5%\">Remarks</td>";
$detail_table .= "</tr>";

$export_content = "&quot;&quot;,";
$export_content .= "&quot;".$i_general_class.": ".$targetClass."&quot;,";
$export_content .= "&quot;".$Section2Fr."      to      ".$Section2To."&quot;,";
$export_content .= "\n";
$export_content .= "&quot;".$i_UserClassNumber."&quot;,";
$export_content .= "&quot;".$i_UserStudentName."&quot;,";
$export_content .= "&quot;".$i_Profile_Late."&quot;,";
$export_content .= "&quot;Homework&quot;,";
$export_content .= "&quot;Uniform&quot;,";
$export_content .= "&quot;I.D.&quot;,";
$export_content .= "&quot;Remarks&quot;,";
$export_content .= "\n";


	
for ($i=0; $i<sizeof($studentID); $i++)
  {
    $css = ($i%2?"2":"1");
		$detail_table .= "<tr class=\"tablebluerow$css tabletext\">";
    
    # Fatch Class No. form datbase
    $result = $lclass->getClassNoByStudentID($studentID[$i]);
    $detail_table .= "<td>".$result[0]."</td>";
    $export_content .= "&quot;".RepQuotes($result[0])."&quot;,";
    
    # Fatch English Name form datbase
    $result = $lclass->getStudentNameByStudentID($studentID[$i]);
    $detail_table .= "<td>".$result[0][0]." (".$result[0][1].")</td>";
    $export_content .= "&quot;".RepQuotes($result[0][0])." (".RepQuotes($result[0][1]).")&quot;,";

    # count misconduct record for Late records
    $CategoryID=1;
    $result = $ldiscipline->countMisconductInPeriod($studentID[$i],$Section2Fr,$Section2To,$CategoryID);
    //$result[0] = $result[0]*4;
    if ($result[0] > 4){
	    $detail_table .= "<td><u>".$result[0]."</u></td>";
    	$export_content .= "&quot;(".RepQuotes($result[0]).")&quot;,";
	}   
	else{ 
    	$detail_table .= "<td>".$result[0]."</td>";
    	$export_content .= "&quot;".RepQuotes($result[0])."&quot;,";
	}
    
    # Homework
    $CategoryID=2;
    $result = $ldiscipline->countMisconductInPeriod($studentID[$i],$Section2Fr,$Section2To,$CategoryID);
    //$result[0] = $result[0]*4;
    if ($result[0] > 4){
	    $detail_table .= "<td><u>".$result[0]."</u></td>";
    	$export_content .= "&quot;(".RepQuotes($result[0]).")&quot;,";
	}   
	else{ 
    	$detail_table .= "<td>".$result[0]."</td>";
    	$export_content .= "&quot;".RepQuotes($result[0])."&quot;,";
	}
    
    # Uniform
    $CategoryID=3;
    $result = $ldiscipline->countMisconductInPeriod($studentID[$i],$Section2Fr,$Section2To,$CategoryID);
    //$result[0] = $result[0]*4;
    if ($result[0] > 4){
	    $detail_table .= "<td><u>".$result[0]."</u></td>";
    	$export_content .= "&quot;(".RepQuotes($result[0]).")&quot;,";
	}   
	else{ 
    	$detail_table .= "<td>".$result[0]."</td>";
    	$export_content .= "&quot;".RepQuotes($result[0])."&quot;,";
	}
	    
    # I.D.
    $CategoryID=12;
    $result = $ldiscipline->countMisconductInPeriod($studentID[$i],$Section2Fr,$Section2To,$CategoryID);
    //$result[0] = $result[0]*4;
    if ($result[0] > 4){
	    $detail_table .= "<td><u>".$result[0]."</u></td>";
    	$export_content .= "&quot;(".RepQuotes($result[0]).")&quot;,";	
	}   
	else{ 
    	$detail_table .= "<td>".$result[0]."</td>";
    	$export_content .= "&quot;".RepQuotes($result[0])."&quot;,";
	}
	    
    $detail_table .= "<td></td>";
    $detail_table .= "</tr>";
    $export_content .= "&quot;&quot;,";	
    $export_content .= "\n";   
 
  }
$detail_table .= "</table>";
echo '<input type="hidden" name="content" value="'.urlencode($detail_table).'" />';
echo '<input type="hidden" name="exportContent" value="'.$export_content.'" />';
echo "<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>".$detail_table."</td></tr></table>";
echo "<br>";
echo $linterface->GET_ACTION_BTN("Back", "button", "history.go(-1);return true;");
echo '&nbsp;&nbsp;';
echo $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint()");
echo '&nbsp;&nbsp;';
echo $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()");
echo '</form>';
?>
<?php
  $linterface->LAYOUT_STOP();
  intranet_closedb();
?>
