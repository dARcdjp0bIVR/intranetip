<?php
$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

ini_set("memory_limit", "150M"); 

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ExportArr = array();

if (!$lstudentprofile->is_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='1' id='merit1' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==1) ? " checked" : "";
	}
	$meritType .= "><label for='merit1'>".$i_Merit_Merit."</label>";
}
if (!$lstudentprofile->is_min_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='2' id='merit2' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==2) ? " checked" : "";
	}
	$meritType .= "><label for='merit2'>".$i_Merit_MinorCredit."</label>";
}
if (!$lstudentprofile->is_maj_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='3' id='merit3' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==3) ? " checked" : "";
	}
	$meritType .= "><label for='merit3'>".$i_Merit_MajorCredit."</label>";
}
if (!$lstudentprofile->is_sup_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='4' id='merit4' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==4) ? " checked" : "";
	}
	$meritType .= "><label for='merit4'>".$i_Merit_SuperCredit."</label>";
}
if (!$lstudentprofile->is_ult_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='5' id='merit5' onClick=\"checkClickBox(this,'all_award')\"";
	for($i=0;$i<sizeof($merit);$i++) {
		$meritType .= ($merit[$i]==5) ? " checked" : "";
	}
	$meritType .= "><label for='merit5'>".$i_Merit_UltraCredit."</label>";
}

if (!$lstudentprofile->is_black_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-1' id='demerit1' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-1) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit1'>".$i_Merit_BlackMark."</label>";
}
if (!$lstudentprofile->is_min_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-2' id='demerit2' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-2) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit2'>".$i_Merit_MinorDemerit."</label>";
}
if (!$lstudentprofile->is_maj_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-3' id='demerit3' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-3) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit3'>".$i_Merit_MajorDemerit."</label>";
}
if (!$lstudentprofile->is_sup_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-4' id='demerit4' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-4) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit4'>".$i_Merit_SuperDemerit."</label>";
}
if (!$lstudentprofile->is_ult_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-5' id='demerit5' onClick=\"checkClickBox(this,'all_punishment')\"";
	for($i=0;$i<sizeof($demerit);$i++) {
		$demeritType .= ($demerit[$i]==-5) ? " checked" : "";
	}
	$demeritType .= "><label for='demerit5'>".$i_Merit_UltraDemerit."</label>";
}

$meritDemerit[5] = $i_Merit_Warning;
$meritDemerit[6] = $i_Merit_Merit;
$meritDemerit[7] = $i_Merit_MinorCredit;
$meritDemerit[8] = $i_Merit_MajorCredit;
$meritDemerit[9] = $i_Merit_SuperCredit;
$meritDemerit[10] = $i_Merit_UltraCredit;
$meritDemerit[4] = $i_Merit_BlackMark;
$meritDemerit[3] = $i_Merit_MinorDemerit;
$meritDemerit[2] = $i_Merit_MajorDemerit;
$meritDemerit[1] = $i_Merit_SuperDemerit;
$meritDemerit[0] = $i_Merit_UltraDemerit;

// $SchoolYear = getCurrentAcademicYear();
// $selectSchoolYear1 .= "<option value='0'";
// $selectSchoolYear1 .= ($SchoolYear1==0) ? " selected" : "";
// $selectSchoolYear1 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
// $selectSchoolYear1 .= $ldiscipline->getConductSchoolYear($SchoolYear1);

// $selectSchoolYear2 .= "<option value='0'";
// $selectSchoolYear2 .= ($SchoolYear2==0) ? " selected" : "";
// $selectSchoolYear2 .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
// $selectSchoolYear2 .= $ldiscipline->getConductSchoolYear($SchoolYear2);

# Semester Menu #
$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SemesterMenu1 .= "<option value='WholeYear'";
$SemesterMenu1 .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu1 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu1 .= "<option value='".$name."'";
	if($name==$semester1) { $SemesterMenu1 .= " selected"; }
	$SemesterMenu1 .= ">".$name."</option>";
}

$SemesterMenu2 .= "<option value='WholeYear'";
$SemesterMenu2.= ($semester2 != 'WholeYear') ? "" : " selected";
$SemesterMenu2 .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$SemesterMenu2 .= "<option value='".$name."'";
	if($name==$semester2) { $SemesterMenu2 .= " selected"; }
	$SemesterMenu2 .= ">".$name."</option>";
}
# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value,'')\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_alert_pleaseselect);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$classAry = array();
$studentAry = array();

$studentAry = $ldiscipline->storeStudent($studentID, $targetClass);

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";
$SchoolYear1Text = $ldiscipline->getAcademicYearNameByYearID($SchoolYear1ID);
$SchoolYear2Text = $ldiscipline->getAcademicYearNameByYearID($SchoolYear2ID);

$act = new academic_year_term();
$semester1Text = $act->Get_YearNameByID($semester1);
$semester2Text = $act->Get_YearNameByID($semester2);

$semester1Text = ($semester1=='WholeYear' || $semester1==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester1Text;
$semester2Text = ($semester2=='WholeYear' || $semester2==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester2Text;

# SQL conditions (A&P) #
if($award_punish_period == 1) {
	if($SchoolYear1ID != '0') {		# specific school year
		$date1 = $SchoolYear1Text." ".$semester1Text;
		$conds .= " AND a.AcademicYearID='$SchoolYear1ID'";
	} else {						# all school year
		$date1 = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester1Text;
		$academicYear = $ldiscipline->generateAllSchoolYear();
	}
	
	if($semester1!='WholeYear' && $semester1!=0) {	# specific semester
		$conds .= " AND a.YearTermID='$semester1'";
	}
} else {		# choose "Specific date range"
	$date1 = $Section1Fr." To ".$Section1To;
	$conds .= " AND (a.RecordDate BETWEEN '$Section1Fr' AND '$Section1To')";
}

if($waive_record1 == 1) {	# with waived record
//	$conds .= " AND ((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL)) OR (a.RecordStatus=".DISCIPLINE_STATUS_WAIVED." AND (a.ReleaseStatus=".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL)))";
	$conds .= " AND ((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.") OR (a.RecordStatus=".DISCIPLINE_STATUS_WAIVED."))";
} else { 					# only approved record
//	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND (a.ReleaseStatus = ".DISCIPLINE_STATUS_UNRELEASED." OR a.ReleaseStatus IS NULL))";	
	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.")";	
}

# Award & Punishment option #
if($merit != '') {
	$meritChoice = implode(",",$merit);
}
if($demerit != '') {
	$demeritChoice = implode(",",$demerit);
}

if($all_award != '' && $all_punishment != '') {				# award + punishment
	$conds .= " AND (a.MeritType=1 OR a.MeritType=-1)";
} else if($all_award != '' && $demerit != '') {			# award + demerit
	$conds .= " AND (a.MeritType=1 OR a.ProfileMeritType IN ($demeritChoice))";
} else if($all_punishment != '' && $merit != '') {		# punish + merit
	$conds .= " AND (a.MeritType=-1 OR a.ProfileMeritType IN ($meritChoice))";
} else if($merit != '' && $demerit != '') {				# merit + demerit
	$conds .= " AND (a.ProfileMeritType IN ($meritChoice) or a.ProfileMeritType IN ($demeritChoice))";
} else if($all_award != '')	{							# only award
	$conds .= " AND a.MeritType=1";
} else if($all_punishment != '') {						# only punishment
	$conds .= " AND a.MeritType=-1";
} else if($merit != '')	 {								# merit
	$conds .= " AND a.ProfileMeritType IN ($meritChoice)";
} else if($demerit != '') {								# demerit
	$conds .= " AND a.ProfileMeritType IN ($demeritChoice)";
}

if($goodconductid != '') {
	$gdConductChoice = implode(",", $goodconductid);
}
if($misconductid != '') {
	$misConductChoice = implode(",", $misconductid);	
}		

$meritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_merit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$demeritImg = "<img src={$image_path}/{$LAYOUT_SKIN}/ediscipline/icon_demerit.gif width=20 height=20 border=0 align=absmiddle border=0>";
$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";

/*
$export_content .= "<html><head>";
$export_content .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
$export_content .= "<link href='/templates/{$LAYOUT_SKIN}/css/print.css' rel='stylesheet' type='text/css'>";
$export_content .= "<style type='text/css'>";
$export_content .= "<!--";
$export_content .= ".print_hide {display:none;}";
$export_content .= "-->";
$export_content .= "</style>";
$export_content .= "</head><body>";
*/
$export_content .= "<table width='100%' align='center' class='print_hide'><tr>";
$export_content .= "<td align='right'>";
$export_content .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$export_content .= "</td></tr></table>";
$export_content .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='result_box'>";
$export_content .= "<tr><td valign='top'>";
$export_content .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";


for($i=0;$i<sizeof($studentAry);$i++) {

	# student info
	$stdName = $ldiscipline->getStudentNameByID($studentAry[$i]);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	
	$export_content .= "<tr><td colspan='2' valign='top'>";	
	$export_content .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$export_content .= "<tr><td class='page_title_print tabletext'>{$i_Discipline_System_Reports_Discipline_report}</td>";
	$export_content .= "</tr></table>";
	$export_content .= "</td></tr>";	

	$export_content .= "<tr><td align='left' colspan='2' valign='top'>";
	$export_content .= "<table border='0' cellspacing='0' cellpadding='5'>";
	$export_content .= "<tr class='tabletext'><td>{$i_Discipline_Student} : <span class='sectiontitle_print'>{$std_name}</span> </td>";
	$export_content .= "<td> {$i_Discipline_Class} : <span class='sectiontitle_print'>{$className}-{$classNumber}</span></td>";
	$export_content .= "</tr></table></td></tr>";


	# --------- Table A&P ---------- 

	
	if($userID != '') {
		if($award_punishment_flag==1) {
			$sql = "
				SELECT 
					a.RecordDate as RecordDate, 
					CONCAT(
						IF(a.RecordStatus=2,'$waivedImg',''),
						IF(a.ProfileMeritType>0,'$meritImg','$demeritImg')
					) as img,
					a.ProfileMeritCount as meritCount, 
					a.ProfileMeritType as meritType,
					CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;')) as item,
					PICID,
					IF(a.RecordStatus=2,'Yes','') as waivedRecord,
					a.Remark,
					a.ConductScoreChange
			
				FROM DISCIPLINE_MERIT_RECORD as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
					LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
					LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
					LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
				WHERE a.DateInput IS NOT NULL
					AND b.UserID = $userID
					AND a.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED."
					$conds
					ORDER BY a.RecordDate DESC
			";
			
			$row = $ldiscipline->returnArray($sql,5);
			// Create data array for export
			$ExportArr = array();
			
			$export_content .= "<tr><td align='left' class='form_sep_title' valign='top'><i> - {$eDiscipline['Award_and_Punishment']} -</i></td><td align='right' class='tabletext'> {$date1} </td></tr>";
			$export_content .= "<tr><td align='left' colspan='2' valign='top'>";
			$export_content .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' class='tableborder_print'>";
			$export_content .= "<tr class='tabletop_print tabletext'>";
			$export_content .= "<td width='15' align='center'>#</td>";
			$export_content .= "<td width='100'>".$Lang['eDiscipline']['EventDate']."</td>";
			$export_content .= "<td width='40'>&nbsp;</td>";
			$export_content .= "<td>".$eDiscipline["Record"]."</td>";
			$export_content .= "<td>".$i_Discipline_Reason."</td>";
			if($show_conduct_mark==1)
				$export_content .= "<td align='center'>{$eDiscipline['Conduct_Mark']}</td>";
			$export_content .= "<td>".$i_Discipline_PIC."</td>";
			if($sys_custom['sdbnsm_disciplinev12_student_report_cust']) {
				$export_content .= "<td width='30%'>{$i_Discipline_Remark}</td>";
			}
			$export_content .= "</tr>";
				
			if(sizeof($row)==0) {
				$colspan = ($sys_custom['sdbnsm_disciplinev12_student_report_cust']) ? 7 : 6;
				$export_content .= "<tr><td colspan='$colspan' align='center' height='80' class='row_print tabletext'>".$i_no_record_exists_msg."</td></tr>";
			} 
			else {
				
				for($j=0; $j<sizeof($row); $j++){
					$n = $j+1;
					$temp = $row[$j][3] + 5;
					$export_content .= "<tr class='row_print'>";
					$export_content .= "<td valign='top' class='tabletext row_print'>{$n}</td>";
					$export_content .= "<td valign='top' class='tabletext row_print'>{$row[$j][0]}</td>";
					$export_content .= "<td align='right' valign='top' class='tabletext row_print'>{$row[$j][1]}</td>";
					$export_content .= "<td valign='top' class='tabletext row_print'>". $ldiscipline->returnDeMeritStringWithNumber($row[$j][2], $meritDemerit[$temp]) ."&nbsp;</td>";
					$export_content .= "<td valign='top' class='tabletext row_print'>{$row[$j][4]}&nbsp;</td>";
					if($show_conduct_mark==1)
						$export_content .= "<td  valign='top' class='tabletext row_print' align='center'>{$row[$j][8]}&nbsp;</td>";
					$export_content .= "<td align='left' valign='top' class='tabletext row_print'>";

					$name_field = getNameFieldByLang();		//PIC
					if($row[$j][5] != '') {
						$pic = $row[$j][5];
	
						$sql2 = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($pic)";
						$temp = $ldiscipline->returnArray($sql2,1);
						
						if(sizeof($temp)!=0) {
					    	for($k=0; $k<sizeof($temp); $k++) {
					        	$export_content .= $temp[$k][0];
					        	$export_content .= ($k != (sizeof($temp)-1)) ? ", " : "";
					    	}
						} else {
							$export_content .= "---";
						}
					} else {
						$export_content .= "---";	
					}
					$export_content .= "</td>";
					if($sys_custom['sdbnsm_disciplinev12_student_report_cust']) {
						$export_content .= ($row[$j][7]) ? "<td class='tabletext row_print'>{$row[$j][7]}&nbsp;</td>" : "<td class='tabletext row_print'>---</td>";
					}
					
					$export_content .= "</tr>";
				}
			}
			$export_content .= "</table>";
			$export_content .= "{$waivedImg}<span class='tabletextremark tabletext'>= {$i_Discipline_System_Award_Punishment_Waived}</span>";
			$export_content .= "</td></tr><tr><td colspan='2'>&nbsp;</td></tr>";

		}
		if($displayMode==1) {
			if($gdConduct==1 || $misconduct==1) {
				# GoodConduct & Misconduct Option #
				if($gdConduct_miscondict_period == 1) {
					$date2 = ($SchoolYear2ID != '0') ? $SchoolYear2Text." ".$semester2Text : $i_Discipline_System_Award_Punishment_All_School_Year." ".$semester2Text;
					$dateConds = ($SchoolYear2ID != '0') ? " AND b.AcademicYearID='$SchoolYear2ID'" : "";
					$dateConds .= ($semester2 != 'WholeYear' && $semester2!=0) ? " AND b.YearTermID='$semester2'" : "";
				
				} else {
					$date2 = $Section2Fr." To ".$Section2To;
					$dateConds = " AND (b.RecordDate BETWEEN '$Section2Fr' AND '$Section2To')";
				}
		
				$export_content .= "<tr><td align='left' colspan='2' valign='top' class='tabletext'>";
				$export_content .= "<table width='100%' border='0' cellspacing='0' cellpadding='3'><tr>";
				$export_content .= "<td class='form_sep_title_print tabletext'><i>- {$eDiscipline['Good_Conduct_and_Misconduct']} -</i></td>";
				$export_content .= "<td align='right' class='tabletext'>{$date2}</td></tr><tr>";
		
				
				if($gdConduct==1) {
					$export_content .= "<td width='50%' valign='top' class='tabletextremark tabletext'>";
					$gdConductContent = $ldiscipline->getGdConductContent($userID, $waive_record2, $all_gd_conduct, $gdConductChoice, $SchoolYear2ID, $semester2, $intranet_root, $i_no_record_exists_msg, 'row_print', $dateConds);
					
					if(sizeof($gdConductContent) != 0) {
						$export_content .= "<table border='0' width='100%' cellspacing='0' cellpadding='3' class='tableborder_print'>";
						$export_content .= "<tr class='tabletop_print tabletext'><td>{$meritImg} {$i_Discipline_GoodConduct}</td><td align='center'>{$eDiscipline['Setting_AccumulatedTimes']}</td></tr>";
						$export_content .= $gdConductContent;
						$export_content .= "</table></td>";
					} 
					
				}
				
				if($misconduct==1) {
					$export_content .= "<td width='50%' valign='top' class='tabletextremark tabletext'>";
					$misConductContent = $ldiscipline->getMisConductContent($userID, $waive_record2, $all_misconduct, $misConductChoice, $SchoolYear2ID, $semester2, $intranet_root, $i_no_record_exists_msg, 'row_print', $dateConds);
					
					if(sizeof($misConductContent) != 0) {
						$export_content .= "<table border='0' width='100%' cellspacing='0' cellpadding='3' class='tableborder_print'>";
						$export_content .= "<tr class='tabletop_print tabletext'><td>{$demeritImg} {$i_Discipline_Misconduct}</td><td align='center'>{$eDiscipline['Setting_AccumulatedTimes']}</td></tr>";
						$export_content .= $misConductContent;
						$export_content .= "</table></td>";
					}
				}
	//			$export_content .= "</tr></table>( ) = {$i_Discipline_System_Award_Punishment_Waived}</td></tr>";
				$export_content .= "</tr></table></td></tr>";
			}
		} else {
			$printExport = 1;	# print
			//$Choice = ($goodconductid!='' && $misconductid!='') ? array_merge($goodconductid, $misconductid) : ($goodconductid ? $goodconductid : $misconductid);
			//$gmContent = $ldiscipline->getGoodMisReportInStudentReport($userID, $waive_record2, $Choice, $SchoolYear2ID, $semester2, $dateConds, 'row_approved', $printExport);	
			if(sizeof($goodconductid)>0) {
				$goodContent = $ldiscipline->getGoodMisReportInStudentReport($userID, $waive_record2, $goodconductid, $SchoolYear2ID, $semester2, $dateConds, 'row_approved', $printExport);
				$export_content .= "
					<tr>
						<td>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='left' class='form_sep_title'><i> - {$eDiscipline['Setting_GoodConduct']} -</i></td>
									<td align='right'>{$date2}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							{$goodContent}								
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				";
			}
			if(sizeof($misconductid)>0) {
				$misContent = $ldiscipline->getGoodMisReportInStudentReport($userID, $waive_record2, $misconductid, $SchoolYear2ID, $semester2, $dateConds, 'row_approved', $printExport);
				$export_content .= "
					<tr>
						<td>
							<table width='100%' border='0' cellspacing='0' cellpadding='0'>
								<tr>
									<td align='left' class='form_sep_title'><i> - {$eDiscipline['Setting_Misconduct']} -</i></td>
									<td align='right'>{$date2}</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							{$misContent}								
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				";
			}
		}
	}
	$export_content .= "<tr><td colspan='2' style='page-break-after:always'>&nbsp;</td></tr>";
}
		
$export_content .= "</table>";
$export_content .= "</td>";
$export_content .= "</tr>";
$export_content .= "</table>";

intranet_closedb();

echo $export_content;
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>
