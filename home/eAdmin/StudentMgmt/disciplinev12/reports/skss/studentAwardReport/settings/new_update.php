<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();


$ldiscipline = new libdisciplinev12();
# Check access right
//$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");

//$qStr = htmlspecialchars(trim($qStr));
$Title = htmlspecialchars(trim($Title));
$Content = htmlspecialchars(trim($Content));
//$Subject = htmlspecialchars(trim($Subject));
$TempVar = $ldiscipline->TemplateVariable_SKSS();
$Content = str_replace("http://".$_SERVER['HTTP_HOST'],'',$Content);

//$cStr = htmlspecialchars(trim($cStr));
//$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);

$Module = "Discipline_StudentAwardReport";

$fields = "Module,CategoryID,Content,Title,RecordStatus,DateInput,DateModified";
$InsertValue = "'$Module', '','$Content','$Title','$RecordStatus',now(),now()";
$sql = "INSERT INTO INTRANET_NOTICE_MODULE_TEMPLATE ($fields) VALUES ($InsertValue)";

if($ldiscipline->db_db_query($sql))
{
	$SysMsg = $Lang['General']['ReturnMessage']['AddSuccess'];
}
else
{
	$SysMsg = $Lang['General']['ReturnMessage']['AddUnsuccess'];
}	
		
$ReturnPage = "index.php?ReturnMsg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
