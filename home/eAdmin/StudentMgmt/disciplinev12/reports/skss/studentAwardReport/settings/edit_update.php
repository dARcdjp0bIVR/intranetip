<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

if (!$plugin['Disciplinev12'])
{
	header("Location: ./");
	exit;
}

$ldiscipline = new libdisciplinev12();
# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");

$Title = htmlspecialchars(trim($Title));
$Content = htmlspecialchars(trim($Content));
//$Subject = htmlspecialchars(trim($Subject));
//$qStr = htmlspecialchars(trim($qStr));
//$cStr = htmlspecialchars(trim($cStr));

$TempVar = $ldiscipline->TemplateVariable_SKSS();
$Content = str_replace("http://".$_SERVER['HTTP_HOST'],'',$Content);
//$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);


$sql = "UPDATE INTRANET_NOTICE_MODULE_TEMPLATE 
		set
		Title = '$Title',
		Content = '$Content',
		RecordStatus = '$RecordStatus',
		DateModified = now()
		WHERE TemplateID = '$TemplateID'
		";

if($ldiscipline->db_db_query($sql))
{
	$SysMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];
}
else
{
	$SysMsg = $Lang['General']['ReturnMessage']['UpdateUnsuccess'];
}	
		
$ReturnPage = "index.php?ReturnMsg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
