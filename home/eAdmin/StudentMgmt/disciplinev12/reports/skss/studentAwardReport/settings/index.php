<?php 
# using: 
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
} else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
} else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
} else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();
//$ldisciplinev12->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$RecordStatus = ($RecordStatus=='')? "0" : $RecordStatus;
//$CategoryID = ($CategoryID=='')? "0" : $CategoryID;

//$extraCriteria = ($CategoryID=='0')? "" : " AND CategoryID= '$CategoryID' ";
$extraCriteria .= ($RecordStatus=='0')? "" : " AND RecordStatus = '".($RecordStatus-1)."' ";

$Module = "Discipline_StudentAwardReport";

$sql  = "SELECT
            CONCAT('<a class=\'tablelink\' href=\'edit.php?TemplateID=', TemplateID, '\'>', Title, '</a>') as Title,               			IF(RecordStatus=1,CONCAT('$i_Discipline_System_Discipline_Template_Published'),CONCAT('$i_Discipline_System_Discipline_Template_Draft')) as Status,
            CONCAT('<input type=\'checkbox\' name=\'TemplateID[]\' value=\'', TemplateID,'\'>')
         FROM 
			INTRANET_NOTICE_MODULE_TEMPLATE
         WHERE 
			Module='$Module' $extraCriteria ";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Title", "Status");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='60%' >".$li->column($pos++, $i_Discipline_System_Discipline_Template_Title)."</td>\n";
//$li->column_list .= "<td width='30%'>".$li->column($pos++, $i_Discipline_System_Discipline_Situation)."</td>\n";
$li->column_list .= "<td width='40%'>".$li->column($pos++, $i_Discipline_System_Discipline_Status)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("TemplateID[]")."</td>\n";

/*
$catTmpArr = $ldisciplinev12->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array('0',$i_ResourceSelectAllSituations);
	
	foreach($catTmpArr as $Key=>$Value)
	{
		$catArr[] = array($Key,$Value);
	} 
}
*/
$statusArr[0] = array(0,$i_Discipline_System_Discipline_Template_All_Status);
$statusArr[1] = array(1,$i_Discipline_System_Discipline_Template_Draft);
$statusArr[2] = array(2,$i_Discipline_System_Discipline_Template_Published);

$statusSelection = $linterface->GET_SELECTION_BOX($statusArr, 'id="RecordStatus", name="RecordStatus" onChange="document.form1.submit()"', "", $RecordStatus);
/*
$catSelection = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID", name="CategoryID" onChange="document.form1.submit()"', "", $CategoryID);
*/
$toolbar = $linterface->GET_LNK_NEW("new.php",$button_new,"","","",0);

# Top menu highlight setting
$CurrentPage = "StudentAwardReport";

$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['StudentAwardReport'],"/home/eAdmin/StudentMgmt/disciplinev12/reports/skss/studentAwardReport/index.php",0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['TemplateManagement'],"/home/eAdmin/StudentMgmt/disciplinev12/reports/skss/studentAwardReport/settings/index.php",1);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldisciplinev12->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START($ReturnMsg);

?>
<script language="javascript">
<!--
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}
//-->
</script>
<br />
<form name="form1" method="post" action="">
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
										</tr>
									</table>
								</td>
								<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
							</tr>
							<tr><td><?=$catSelection?><?=$statusSelection?></td>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'TemplateID[]','edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:removeCat(document.form1,'TemplateID[]','remove_update.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
	</table><br>
				<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
				<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
				<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
				<input type="hidden" name="page_size_change" value="" />
				<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>