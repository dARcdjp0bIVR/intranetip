<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-StudentAttendance"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['attendancestudent']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}


$ldiscipline = new libdisciplinev12();
$IDs = implode(",",$TemplateID);
$sql = "DELETE FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE TemplateID in ($IDs) ";

if($ldiscipline->db_db_query($sql))
{
	$SysMsg = $Lang['General']['ReturnMessage']['DeleteSuccess'];
}
else
{
	$SysMsg = $Lang['General']['ReturnMessage']['DeleteUnsuccess'];
}	
		
$ReturnPage = "index.php?ReturnMsg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
