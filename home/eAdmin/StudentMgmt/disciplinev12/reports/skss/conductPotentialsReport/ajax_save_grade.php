<?
// Modifying by : henry

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


intranet_auth();
intranet_opendb();
//debug_pr($_POST);
$ldiscipline = new libdisciplinev12();

foreach($sid as $studentID) {
	for($i=1; $i<sizeof($Lang['eDiscipline']['SKSS']['Items']); $i++) {
		$dataAry[$studentID][$i] = ${"student_".$studentID."_".$i};	
	}
	
}

$success = $ldiscipline->update_SKSS_Conduct_Potentials_Grade($dataAry);

$returnMsg = ($success) ? $Lang['SysMgr']['eDiscipline']['SKSS']['SaveSuccess'] :  $Lang['SysMgr']['eDiscipline']['SKSS']['SaveFail'];

intranet_closedb();

echo $returnMsg;
?>