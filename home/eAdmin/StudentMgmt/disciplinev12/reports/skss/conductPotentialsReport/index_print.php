<?php

#####################################
#
#	Date:	2013-06-06	YatWoon
#			Redirect the cust report generation to libdisciplinev12_cust.php
#
#####################################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if(!sizeof($_POST)) {
	header("Location: index.php");	
}
intranet_auth();
intranet_opendb();
$linterface = new interface_html("print_header_no25css.php");
//$ldiscipline = new libdisciplinev12();
$ldiscipline = new libdisciplinev12_cust();

$lnotice = new libnotice();
// include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
$noticeAry = explode(',',$noticeList);
$noticeData = $lnotice->returnRecord("",$noticeAry);
$tableContent = $ldiscipline->retrieveConductPotentialsReport($classID, $issueDate, $option);
?>
<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>
<table width="98%" border="0" cellpadding="0" cellspacing="0" align="center" class='print_hide'>
	<tr>
		<td align="right">
			<?= $linterface->GET_BTN($button_print, "submit", "window.print(); return false;"); ?>
		</td>
	</tr>
</table>
<?= $tableContent?>
<?
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>