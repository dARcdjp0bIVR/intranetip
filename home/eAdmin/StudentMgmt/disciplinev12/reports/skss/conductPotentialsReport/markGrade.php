<?php
// Modifying by : henry


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
/*
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}
*/
$CurrentPage = "ConductPotentialsReport";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['ConductPotentialsAssessmentReport']['Name'],"");

$lclass = new libclass();
$linterface = new interface_html();

# Academic Year ID
$academicYearID = Get_Current_Academic_Year_ID();

# School year
$selectSchoolYearHTML = getAYNameByAyId();

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";

$classAry = $lclass->getSelectClassWithoutWholeForm($academicYearID);
$clsSelectionMenu = getSelectByArray($classAry, ' name="classID" id="classID" onChange="Get_Supplementary()"', $classID); 

$signature = "<table>";
foreach($Lang['eDiscipline']['SKSS']['ConductPotentialSignature'] as $id=>$name) {
	$signatureDisplay = Get_Lang_Selection($name[1],$name[0]);
	$signature .= '<tr><td><input type="checkbox" name="option[]" id="option'.($id).'" value="'.($id).'" '.(sizeof($option)>0 && in_array(($id), $option) ? "checked" : "").'><label for="option'.($id).'">'.$signatureDisplay.'</td></tr>';
}
$signature .= "</table>";

# Submit from form
if ($_POST["submit_flag"] == "YES") {
	$optionString = "<td id=\"tdOption\" class=\"tabletextremark\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";

	$jsRankTargetDetail = implode(',', $rankTargetDetail);

	if ($radioPeriod == "YEAR") {
		$parPeriodField1 = $selectYear;
		$parPeriodField2 = $selectSemester;
	} else {
		$parPeriodField1 = $textFromDate;
		$parPeriodField2 = $textToDate;
	}

	$parByFieldArr = $rankTargetDetail;

	### Table Data ###
	
	if($submit_flag=="YES") {
		if($rankTarget!="student")
			unset($studentID);

		//$tableContent = $ldiscipline->retrieveConductPotentialsReport($radioPeriod, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $studentID, $issueDate, $option, $display=1);
		
		$tableButton .= "<table width='90%' border='0' align='center'><tr><td align='center'>"
							.$linterface->GET_ACTION_BTN($button_print, "button", "doPrint();")."&nbsp;"
							.$linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'")."&nbsp;</td></tr></table>";									
	}

	
	############ End of Report Table ###############
	################################################
	
	
	$initialString = "<script language=\"javascript\">hideOption();</script>\n";
} else {
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
	$initialString = "<script language=\"javascript\">jPeriod='YEAR';</script>\n";
}

$linterface->LAYOUT_START();
echo $linterface->Include_JS_CSS();

?>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
//	alert("This is the url -> "+url);
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str, choice)
{
	
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "../get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+document.getElementById('selectYear').value;
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
//alert(url);		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
</script>

<script language="javascript">
var jPeriod="<?=$radioPeriod?>";

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function doPrint()
{
	document.form1.action = "index_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "index.php";
	document.form1.target = "_self";
}

function doExport()
{
	document.form1.action = "index_export.php";
	//document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "index.php";
	//document.form1.target = "_self";
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function checkForm(){
	var choiceSelected;
	var choice = "";
	
	if (jPeriod=='DATE' && document.getElementById('textFromDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('textToDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textFromDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textToDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.form1.textFromDate.value > document.form1.textToDate.value)) {
		alert("<?=$i_invalid_date?>");
		return false;	
	}
	if (document.getElementById('rankTarget').value=='#') {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
		return false;
	}	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");
		return false;
	}	 	
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if(document.getElementById('rankTarget').value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	/*
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	*/
	document.getElementById('submit_flag').value='YES';
	return true;
}

function Get_Supplementary()
{
	var PostVar = {
			searchText: encodeURIComponent($('Input#searchText').val()),
			AcademicYearID: encodeURIComponent($('#AcademicYearID').val()),
			classID: encodeURIComponent($('#classID').val()),
			selectSemester: encodeURIComponent($('#selectSemester').val()),
			issueDate: encodeURIComponent($('#issueDate').val()),
			startDate: encodeURIComponent($('#startDate').val()),
			endDate: encodeURIComponent($('#endDate').val())
			}

	Block_Element("spanOptionContent");
	$.post('ajax_get_supplementary.php',PostVar,
		function(data){
			//alert(data);
			if (data == "die") 
				window.top.location = '/';
			else {
				$('div#spanOptionContent').html(data);
				Thick_Box_Init();
				UnBlock_Element("spanOptionContent");
			}
		});
}

function doSave(print)
{
	Block_Thickbox();
	var PostString = Get_Form_Values(document.getElementById("form1"));
	

	var PostVar = PostString;
	//alert(PostVar);
	
	//Block_Element("spanOptionContent");
	$.post('ajax_save_grade.php',PostVar,
		function(data){
			//alert(data);
			if (data == "") 
				window.top.location = '/';
			else {
				Get_Return_Message(data);
			}
		});
	
	if(print==1) {
		doPrint();
	}
}

{
//on page load call tb_init
function Thick_Box_Init()
{   
	tb_init('a.thickbox, area.thickbox, input.thickbox');//pass where to apply thickbox
}
}
</script>

<form name="form1" method="post" action="markGrade.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td width="50">&nbsp;</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr align="left">
								<?=$optionString?>
							</tr>
						</table>
						
						<table width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="10%" valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$Lang['SysMgr']['Homework']['AcademicYear']?>
								</td>
								<td width="90%">												
									<?=$selectSchoolYearHTML?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="formfieldtitle">
									<?=$i_ClassName?>
								</td>
								<td>
									<?=$clsSelectionMenu?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="formfieldtitle">
									<?=$Lang['eDiscipline']['SKSS']['IssueDate']?>
								</td>
								<td>
									<?=$linterface->GET_DATE_PICKER("issueDate",$issueDate)?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="formfieldtitle">
									<?=$Lang['eDiscipline']['SKSS']['Signature']?>
								</td>
								<td>
									<?=$signature?>
								</td>
							</tr>
							<tr valign="top">
								<td colspan="2">&nbsp;</td>
							</tr>
							<tr valign="top">
								<td colspan="2">
									
								</td>
							</tr>
						</table>
						<div id="spanOptionContent">
							<input type="hidden" name="studentSize" id="studentSize" value="0">
						</div>
					</td>
				</tr>
			</table>
			<br />
				<?=$tableContent?>
			<br />
			
		</td>
	</tr>

</table>
	<?=$tableButton?>
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
<input type="hidden" name="academicYearID" id="academicYearID" value="<?=$academicYearID?>">
</form>
<br />
<?=$initialString?>
<script language="javascript">
<!--

function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form","");
}
<?
if($submitBtn01 == "")
	//echo "init();\n";
?>

//-->
</script>
<script language="javascript">
<!--
<?
if($submitBtn01 != "") {
	echo "showResult(\"$rankTarget\", \"$jsRankTargetDetail\");";
}
?>
<? if(sizeof($studentID)>0) { ?>
var xmlHttp2;

//initialStudent();	
	
function initialStudent() {
	xmlHttp2 = GetXmlHttpObject()
	url = "../get_live.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID);?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}
<? } ?>

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
		//SelectAll(form1.elements['studentID[]']);
	} 
}

//changeTerm('<?=$selectYear?>');

//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
 