<?php
// Modifying by : henry


$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
/*
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}
*/
$CurrentPage = "ConductPotentialsReport";

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($Lang['eDiscipline']['SKSS']['ConductPotentialsAssessmentReport']['Name'],"");

$lclass = new libclass();
$linterface = new interface_html();

# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='hideSpan(\"spanStudent\"); changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); document.form1.rankTarget.selectedIndex=1; showResult(\"form\",\"\")'", "", $selectYear);


# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";

$classAry = $lclass->getSelectClassWithoutWholeForm(Get_Current_Academic_Year_ID());
$clsSelectionMenu = getSelectByArray($classAry, ' name="classID" id="classID"', $classID); 

$signature = "<table>";
foreach($Lang['eDiscipline']['SKSS']['ConductPotentialSignature'] as $id=>$name) {
	$signatureDisplay = Get_Lang_Selection($name[1],$name[0]);
	$signature .= '<tr><td><input type="checkbox" name="option[]" id="option'.($id).'" value="'.($id).'" '.(sizeof($option)>0 && in_array(($id), $option) ? "checked" : "").'><label for="option'.($id).'">'.$signatureDisplay.'</td></tr>';
}
$signature .= "</table>";

# Submit from form
if ($_POST["submit_flag"] == "YES") {
	$optionString = "<td id=\"tdOption\" class=\"tabletextremark\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";

	$jsRankTargetDetail = implode(',', $rankTargetDetail);

	if ($radioPeriod == "YEAR") {
		$parPeriodField1 = $selectYear;
		$parPeriodField2 = $selectSemester;
	} else {
		$parPeriodField1 = $textFromDate;
		$parPeriodField2 = $textToDate;
	}

	$parByFieldArr = $rankTargetDetail;

	### Table Data ###
	
	if($submit_flag=="YES") {
		if($rankTarget!="student")
			unset($studentID);

		//$tableContent = $ldiscipline->retrieveConductPotentialsReport($radioPeriod, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $studentID, $issueDate, $option, $display=1);
		
		$tableButton .= "<table width='90%' border='0' align='center'><tr><td align='center'>"
							.$linterface->GET_ACTION_BTN($button_print, "button", "doPrint();")."&nbsp;"
							.$linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'")."&nbsp;</td></tr></table>";									
	}

	
	############ End of Report Table ###############
	################################################
	
	
	$initialString = "<script language=\"javascript\">hideOption();</script>\n";
} else {
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
	$initialString = "<script language=\"javascript\">jPeriod='YEAR';</script>\n";
}

$linterface->LAYOUT_START();


?>
<script language="javascript">
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
//	alert("This is the url -> "+url);
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str, choice)
{
	
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "../get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+document.getElementById('selectYear').value;
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
//alert(url);		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}
</script>

<script language="javascript">
var jPeriod="<?=$radioPeriod?>";

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function doPrint()
{
	document.form1.action = "index_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "index.php";
	document.form1.target = "_self";
}

function doExport()
{
	document.form1.action = "index_export.php";
	//document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "index.php";
	//document.form1.target = "_self";
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
		jPeriod = type;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
		jPeriod = type;
	}
}

function checkForm(){
	var choiceSelected;
	var choice = "";
	
	if (jPeriod=='DATE' && document.getElementById('textFromDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_startdate?>');
		return false;
	}
	if (jPeriod=='DATE' && document.getElementById('textToDate').value=='') {
		alert('<?=$i_alert_pleasefillin.$i_general_enddate?>');
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textFromDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && !check_date(document.getElementById('textToDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(jPeriod=='DATE' && (document.form1.textFromDate.value > document.form1.textToDate.value)) {
		alert("<?=$i_invalid_date?>");
		return false;	
	}
	if (document.getElementById('rankTarget').value=='#') {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
		return false;
	}	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");
		return false;
	}	 	
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if(document.getElementById('rankTarget').value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	/*
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	*/
	document.getElementById('submit_flag').value='YES';
	return true;
}


</script>

<form name="form1" method="post" action="index.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr align="left">
					<td width="50">&nbsp;</td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr align="left" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
								<?=$optionString?>
							</tr>
						</table>
						<span id="spanOptionContent">
						<table width="100%" border="0" cellpadding="5" cellspacing="0" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
							<tr>
								<td valign="top" nowrap="nowrap"  class="formfieldtitle">
									<?=$Lang['eDiscipline']['SKSS']['IssueDate']?>
								</td>
								<td>
									<?
									if(!isset($issueDate))
										$issueDate = date("Y-m-d");
									
									echo $linterface->GET_DATE_PICKER("issueDate",$issueDate);
									?>
								</td>
							</tr>
							<tr valign="top">
								<td height="57" width="10%" valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span>
								</td>
								<td width="90%">
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td height="30" colspan="6" onClick="document.form1.radioPeriod_Year.checked=true">
												<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" onClick="javascript:jPeriod=this.value"<? echo ($radioPeriod!="DATE")?" checked":"" ?>>
												<?=$i_Discipline_School_Year?>
												<?=$selectSchoolYearHTML?>&nbsp;
												<?=$i_Discipline_Semester?>
												<span id="spanSemester"><?=$selectSemesterHTML?></span>
											</td>
										</tr>
										<tr>
											<td><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" onClick="javascript:jPeriod=this.value"<? echo ($radioPeriod=="DATE")?" checked":"" ?>> <?=$i_From?> </td>
											<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate)?>&nbsp;</td>
											<td> <?=$i_To?> </td>
											<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate)?>&nbsp;</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle">
									<?=$i_ClassName?> <span class="tabletextrequire">*</span>
								</td>
								<td>
									<?=$clsSelectionMenu?>
									<!--
									<table width="0%" border="0" cellspacing="2" cellpadding="0">
										<tr>
											<td valign="top">
												<table border="0">
													<tr>
														<td valign="top">
															<select name="rankTarget" id="rankTarget" onChange="showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}">
																<option value="#">-- <?=$i_general_please_select?> --</option>
																<option value="form" <? if($rankTarget=="form") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
																<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
																<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
															</select>
														</td>
														<td>
															<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
															<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5" id="rankTargetDetail[]"></select>
															<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
															<br>
															<div id='spanStudent' style='position:relative; width:280px; height:80px; z-index:1;<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
																<select name="studentID[]" multiple size="5" id="studentID[]"></select>
																<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
															</div>
														</td>
													</tr>
													<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
												</table>
											</td>
											<td valign="top"><br /></td>
										</tr>
									</table>
									-->
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap"  class="formfieldtitle">
									<?=$Lang['eDiscipline']['SKSS']['Signature']?>
								</td>
								<td>
									<?=$signature?>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap">
									<span class="tabletextremark"><?=$i_general_required_field?></span>
								</td>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
								<td><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit", "document.form1.action='index.php';document.form1.target='_self';return checkForm();", "submitBtn01")?>
								</td>
							</tr>
						</table>
						</span>
					</td>
				</tr>
			</table>
			<br />
				<?=$tableContent?>
			<br />
			
		</td>
	</tr>

</table>
	<?=$tableButton?>
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			

</form>
<br />
<?=$initialString?>
<script language="javascript">
<!--

function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	showResult("form","");
}
<?
if($submitBtn01 == "")
	//echo "init();\n";
?>

//-->
</script>
<script language="javascript">
<!--
<?
if($submitBtn01 != "") {
	echo "showResult(\"$rankTarget\", \"$jsRankTargetDetail\");";
}
?>
<? if(sizeof($studentID)>0) { ?>
var xmlHttp2;

//initialStudent();	
	
function initialStudent() {
	xmlHttp2 = GetXmlHttpObject()
	url = "../get_live.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID);?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}
<? } ?>

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
		//SelectAll(form1.elements['studentID[]']);
	} 
}

changeTerm('<?=$selectYear?>');

//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>
 