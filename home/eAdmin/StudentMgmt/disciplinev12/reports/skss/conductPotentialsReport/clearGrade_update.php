<?php
// Modifying by : henry

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# remove existing records
$sql = "DELETE FROM DISCIPLINE_SKSS_CONDUCT_POTENTIALS_REPORT";
$result = $ldiscipline->db_db_query($sql);

# deletion log
$detail = "Clear assessment grade (SKSS)";
$sql = "INSERT INTO MODULE_RECORD_DELETE_LOG (Module, Section, RecordDetail, DelTableName, LogDate, LogBy) VALUES
		('".$ldiscipline->Module."', 'Conduct_Potentials_Report', '$detail', 'DISCIPLINE_SKSS_CONDUCT_POTENTIALS_REPORT', NOW(), '$UserID')";
$ldiscipline->db_db_query($sql);

intranet_closedb();


$returnMsg = $result ? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess'];

echo $returnMsg;
?>