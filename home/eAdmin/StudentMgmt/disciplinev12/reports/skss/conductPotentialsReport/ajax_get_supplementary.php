<?php
# modifying : yat

#################################
#
#	Date:	2012-10-29	YatWoon
#			Updated: set default as "B+" [Case#2012-0717-1649-14073]
#
#################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$lclass = new libclass();
$ldiscipline = new libdisciplinev12();

$academicYearID = Get_Current_Academic_Year_ID();

$classAry = $lclass->getSelectClassWithoutWholeForm($academicYearID);
$clsSelectionMenu = getSelectByArray($classAry, ' name="classID" id="classID" onChange="Get_Supplementary()"', $classID);

$selectSchoolYearHTML = $ldiscipline->getAcademicYearNameByYearID($academicYearID);

/*
# Semester
$currentSemesterAry = getSemesters($academicYearID);
foreach($currentSemesterAry as $id=>$name)
	$semesterAry[] = array($id, $name);
	
$selectSemesterHTML = getSelectByArray($semesterAry, ' name="selectSemester" id="selectSemester"', $selectSemester, 0, 0, $i_Discipline_System_Award_Punishment_Whole_Year);
*/

if(!isset($issueDate) || $issueDate=="undefined")
	$issueDate = date("Y-m-d");
if(!isset($startDate) || $startDate=="undefined")
	$startDate = date("Y-m-d");
if(!isset($endDate) || $endDate=="undefined")
	$endDate = date("Y-m-d");

$studentIDAry = $ldiscipline->storeStudent(0,"::".$classID, $academicYearID);
$grade = array();
$grade = $ldiscipline->get_SKSS_Assessment_Item_Grade($studentIDAry, $academicYearID);

function get_SKSS_Grade_Selection($gradeSelected="", $tag="") {
	global $Lang;
	
	$menu = "<SELECT $tag>";
	for($i=1; $i<sizeof($Lang['eDiscipline']['SKSS']['GradeOption']); $i++) {
		//$selected = ($Lang['eDiscipline']['SKSS']['GradeOption'][$i]==$gradeSelected) ? " selected" : "";
		$selected = ($Lang['eDiscipline']['SKSS']['GradeOption'][$i]==$gradeSelected || ($gradeSelected=="" && $i==4)) ? " selected" : "";	# set default as B+
		$menu .= "<option value=\"".$i."\" $selected>".$Lang['eDiscipline']['SKSS']['GradeOption'][$i]."</option>";
	}
	$menu .= "</SELECT>";
	return $menu;
}

$table = '
<table class="common_table_list">
	<tr>
<th>'.$i_ClassNumber.'</th>
<th>'.$i_general_name.'</th>';
$pos = ($intranet_session_language=="en") ? 0 : 1;

# Junior & Senior forms display different item names 
$FormLevel = $ldiscipline->getLevelInfoByClassID($classID);

$juniorForm = array(1,2,3);
if(in_array($FormLevel[0]['YearID'],$juniorForm)) {	# Junior Form
	$ItemNameAry = $Lang['eDiscipline']['SKSS']['Items_Junior'];
} else {												# Senior Form
	$ItemNameAry = $Lang['eDiscipline']['SKSS']['Items'];
}

for($i=1; $i<sizeof($ItemNameAry); $i++) {
	$table .= '<th>'.$ItemNameAry[$i][$pos].'</th>';	
}
$table .= '	</tr>';

$hiddenField = "";

# display student
foreach($studentIDAry as $sid) {
	$stdInfo = $ldiscipline->retrieveStudentInfoByID2($sid, $academicYearID);
	list($engname, $chiname, $clsName, $clsNo) = $stdInfo[0];
		 
	$table .= '
		<tr>
		<td>'.$clsNo.'</td>
		<td>'.Get_Lang_Selection($chiname, $engname).'</td>';
		
	for($i=1; $i<sizeof($ItemNameAry); $i++) {
		$table .= '<td>'.get_SKSS_Grade_Selection($grade[$sid][$i], ' name="student_'.$sid.'_'.$i.'" id="student_'.$sid.'_'.$i.'"').'</td>';
	}
	$table .= '</tr>';
	
	$hiddenField .= '<input type="hidden" name="sid[]" id="sid[]" value="'.$sid.'">';
} 
if(sizeof($studentIDAry)==0) {
	$colspan = sizeof($ItemNameAry) + 2;
	$table .= "<tr><td align='center' height='40' colspan='$colspan' valign='absmiddle'>$i_no_record_exists_msg</td></tr>";
}
$table .= '</table><br>';

$table .= '<div align="center">';
$table .= $linterface->GET_ACTION_BTN($button_save, "button", "doSave(0);")."&nbsp;";
$table .= $linterface->GET_ACTION_BTN($Lang['eDiscipline']['SKSS']['SaveAndPrint'], "button", "doSave(1);")."&nbsp;";
$table .= $linterface->GET_ACTION_BTN($button_cancel, "button", "resetSpan()");
$table .= '<input type="hidden" name="studentSize" id="studentSize" value="'.sizeof($studentIDAry).'">';
$table .= $hiddenField;

$table .= '</div>';

echo $table;

?>