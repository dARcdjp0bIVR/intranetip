<?php
# using:  
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$sys_custom['eDis_AP_export_WEBSAMS_format']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


# School Year Menu #
$SchoolYear = $SchoolYear =="" ? Get_Current_Academic_Year_ID() : $SchoolYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange=\"changeTerm(this.value);showResult(document.getElementById('rankTarget').value)\"", "", $SchoolYear);

/*
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
*/

$TAGS_OBJ[] = array($Lang['eDiscipline']['AwardPunishmentInterfaceFile']);
$CurrentPage = "AP_InterfaceFile";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--
function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";
	var misItem = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	
	if(form1.rankTarget.value == "#") {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
		return false;
	}
	if(form1.rankTarget.value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['RankingTarget']?>");
		return false;
	}
}


function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";

	if(span == 'spanStudent') {
		document.getElementById('rankTargetDetail[]').multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
		

	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		SelectAll(document.getElementById('rankTargetDetail[]'));
	}
	
	if(span == 'spanStudent') {
		document.getElementById('rankTargetDetail[]').multiple = true;
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

//-->
</script>
<script language="javascript">
var xmlHttp
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str)
{

	if(str != "student2ndLayer") 
		document.getElementById("studentFlag").value = 0;
	
	
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()

	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

		
	var url = "";
	url = "../APReport/ranking/get_live2.php";
	url += "?target=" + str;
	url += "&year=" + document.getElementById('SchoolYear').value;
	url += (document.getElementById('studentFlag').value==1) ? "&student=1&value="+document.getElementById('rankTargetDetail[]').value : "";
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged() 
{ 
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
		if(document.getElementById("studentFlag").value == 0)
			if(document.getElementById('rankTarget').value != 'student') {
				SelectAll(document.getElementById('rankTargetDetail[]'));
			}
	} 
}

var xmlHttp2

function changeClassForm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&level=" + document.getElementById('level').value;
	
	xmlHttp.onreadystatechange = stateChanged2
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{

	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}


</script>
<form name="form1" method="post" action="export.php" onSubmit="return goCheck(this)">

<table class="form_table_v30">
	<tr valign="top">
		<td height="57" valign="top" nowrap="nowrap" class="field_title"><?=$iDiscipline['Period']?><span class="tabletextrequire">*</span></td>
		<td>
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td height="30" colspan="6" onClick="document.form1.dateChoice[0].checked=true">
						<input name="dateChoice" type="radio" id="dateChoice[0]" value="1" checked>
						<?=$i_Discipline_School_Year?>
						<?=$selectSchoolYearHTML?>
						<?=$i_Discipline_Semester?> 
						<span id="spanSemester"><select name="semester" id="semester" onFocus="document.form1.dateChoice[0].checked=true" >
							<?=$SemesterMenu?>
						</select></span>
					</td>
				</tr>
				<tr>
					<td><input name="dateChoice" type="radio" id="dateChoice[1]" value="2">
						<?=$iDiscipline['Period_Start']?></td>
					<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("startDate",$startDate)?></td>
					<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
					<td align="center" onClick="form1.dateChoice[1].checked=true" onFocus="form1.dateChoice[1].checked=true"><?=$linterface->GET_DATE_PICKER("endDate",$endDate)?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr valign="top">
		<td valign="top" nowrap="nowrap" class="field_title"><?=$iDiscipline['RankingTarget']?><span class="tabletextrequire">*</span></td>
		<td width="80%">
			<table border="0">
				<tr>
					<td valign="top">
						<select name="rankTarget" id="rankTarget" onChange="showResult(this.value);if(this.value=='student') {showSpan('spanStudent');document.form1.selectAllBtn01.style} else {hideSpan('spanStudent');}">
							<option value="#" selected>-- <?=$i_general_please_select?> --</option>
							<option value="form"><?=$i_Discipline_Form?></option>
							<option value="class"><?=$i_Discipline_Class?></option>
							<option value="student"><?=$i_UserStudentName?></option>
						</select>
					</td>
					<td>
						<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px;'></div>
						<select name="rankTargetDetail[]"  size="5" id="rankTargetDetail[]"></select>
						<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
					</td>
					<td>
						<div id='spanStudent' style='position:relative; width:280px; height:80px;display:none;'>
							<select name="studentID[]" multiple size="5" id="studentID[]"></select>
							<br><?= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
						</div>
					</td>
				</tr>
				<tr><td colspan="3" class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</td></tr>
			</table>
		</td>
	</tr>
</table>
	            
<span class="tabletextremark"><?=$i_general_required_field?></span>
<div class="edit_bottom_v30">
<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "submit", "")?>
</div>
<br>
<input type="hidden" name="studentFlag" id="studentFlag" value="0">			
<input type="hidden" name="ItemText2" id="ItemText2" />
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<input type="hidden" name="SelectedItemText" id="SelectedItemText" />

</form>

<script>
<!--
$(document).ready(function() {
	document.form1.rankTarget.selectedIndex=1;		// Form
	showResult("form");
	changeTerm('<?=$SchoolYear?>');
})



//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
