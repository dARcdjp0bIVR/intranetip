<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$sys_custom['eDis_AP_export_WEBSAMS_format']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lexport = new libexporttext();

$filename = "interfaceFile.xls";


if($dateChoice==1) {	# by Year / Semester
	if($semester == "0") $semester = "";
	$startDate = substr(getStartDateOfAcademicYear($SchoolYear, $semester),0,10);
	$endDate = substr(getEndDateOfAcademicYear($SchoolYear, $semester),0,10);
} else {
	// do nothing
}


$exportContent = $ldiscipline->Get_AP_Interface_File($startDate, $endDate, $rankTarget, $rankTargetDetail, $studentID);

//$export_content = $lexport->GET_EXPORT_TXT($exportContent, $exportColumn);

$export_content .= stripslashes($exportContent);	
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>