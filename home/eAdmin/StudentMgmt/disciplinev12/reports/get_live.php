<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$academicYearID = Get_Current_Academic_Year_ID();

$result = $ldiscipline->getStudentNameByClassName($targetClass, $academicYearID);

$temp = "<select name='studentID' id='studentID' class='formtextbox'>";
$temp .= "<option value='0'";
$temp .= ($targetClass=='0') ? " selected" : "";
$temp .= ">".$i_Discipline_All_Students."</option>";

for($i=0;$i<sizeof($result);$i++) {
	list($user_id, $name, $className, $classNo) = $result[$i];
	$temp .= "<option value='{$user_id}'";
	$temp .= ($user_id==$sid) ? " selected" : "";
	$temp .= ">{$className} - {$classNo} {$name}</option>";
}

$temp .= "</select>";

echo $temp;

intranet_closedb();

?>