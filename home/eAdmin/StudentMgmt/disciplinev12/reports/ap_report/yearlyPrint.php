<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$li = new libfilesystem();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html();

	$dataAry['StartDate'] = $StartDate;
	$dataAry['EndDate'] = $EndDate;
	$dataAry['rankTarget'] = $rankTarget;
	$dataAry['rankTargetDetail'] = $rankTargetDetail;
	$dataAry['studentID'] = $studentID;
	
	$print = 1;
	
	$tableContent = $ldiscipline->getMeritDemeritYearlyRecordTable($dataAry, $print);
	

?>

<p align='right' class='print_hide'><?=$linterface->GET_BTN($button_print, "button", "window.print()"); ?></p>

<?=$tableContent?>


<? intranet_closedb(); 
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>
