<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$ldiscipline = new libdisciplinev12();
$lclass = new libclass();
$result = $ldiscipline->getRankTarget($target);

$year = getCurrentAcademicYear();

$RTDetail = explode(",", $rankTargetDetail);
//debug_pr($rankTargetDetail);
//debug_pr($RTDetail);

	if($target=='form') {
		
		$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='5' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;} \">";
		$ClassLvlArr = $lclass->getLevelArray();

		$SelectedFormTextArr = explode(",", $SelectedFormText);

		$isEmpty = $ldiscipline->checkIsEmptyArray($RTDetail);
		
		for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
			$temp .= "<option value=\"".$ClassLvlArr[$i][0]."\"";
			$temp .= (in_array($ClassLvlArr[$i][0], $RTDetail) || $isEmpty) ? " selected" : "";
			$temp .= ">".$ClassLvlArr[$i][1]."</option>\n";
		}
		$temp .= "</select>\n";
	}
	
	if($target=="class") {
		
		$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='5' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;}\">";
		$classResult = $ldiscipline->getRankClassList($year);
		
		$isEmpty = $ldiscipline->checkIsEmptyArray($RTDetail);
		
		for($k=0;$k<sizeof($classResult);$k++) {
			$temp .= "<option value='{$classResult[$k][1]}'";
			for($j=0;$j<sizeof($RTDetail);$j++) {
				$temp .= ($RTDetail[$j]==$classResult[$k][1] || $isEmpty) ? " selected" : "";	
			}
			$temp .= ">{$classResult[$k][0]}</option>";
		}
		$temp .= "</select>";
	}
	
	if($target=="student") {
		
		$temp = "<select name='rankTargetDetail[]' id='rankTargetDetail[]' class='formtextbox' multiple size='5' onChange=\"javascript:if(document.form1.rankTarget.value=='student') { showResult('student2ndLayer'); studentSelection(this.value); document.form1.studentFlag.value=1;} \">";
		$classResult = $ldiscipline->getRankClassList($year);
		
				
		for($k=0;$k<sizeof($classResult);$k++) {
			$temp .= "<option value='{$classResult[$k][1]}'";
			for($j=0;$j<sizeof($RTDetail);$j++) {
				$temp .= ($RTDetail[$j]==$classResult[$k][1]) ? " selected" : "";	
			}
			$temp .= ">{$classResult[$k][0]}</option>";
		}
		$temp .= "</select>";
	}
	
	$name_field = getNameFieldByLang("USR.");
	if($target=="student2ndLayer") {
		
		$studentIDAry = explode(',',$studentid);
		$temp = "<select name='studentID[]' id='studentID[]' class='formtextbox' multiple size='5'>";
		if($value != "") {
			$clsName = ($intranet_session_language=="en") ? "yc.ClassTitleEN" : "yc.ClassTitleB5";
			//$sql = "SELECT ClassName, ClassNumber, $name_field, UserID FROM INTRANET_USER WHERE ClassName='$value' AND RecordType=2 ORDER BY ClassNumber";
			$sql = "SELECT $clsName, ycu.ClassNumber, $name_field, USR.UserID FROM INTRANET_USER USR
					LEFT OUTER JOIN YEAR_CLASS_USER ycu ON (ycu.UserID=USR.UserID) 
					LEFT OUTER JOIN YEAR_CLASS yc ON (yc.YearClassID=ycu.YearClassID)
					WHERE yc.YearClassID=$value AND USR.RecordType=2 
					ORDER BY ycu.ClassNumber
					";
			
			$studentResult = $ldiscipline->returnArray($sql, 3);
	
			for($k=0;$k<sizeof($studentResult);$k++) {
				$temp .= "<option value={$studentResult[$k][3]}";
				for($j=0;$j<sizeof($RTDetail);$j++) {
					$temp .= (in_array($studentResult[$k][3], $studentIDAry)) ? " selected" : "";
				}
				$temp .= ">{$studentResult[$k][0]}-{$studentResult[$k][1]} {$studentResult[$k][2]}</option>";
			}
		}
		$temp .= "</select>";
		$temp .= "<br>";
		
		$temp .= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAllItem(document.getElementById('studentID[]'), true);return false;");
	}	


echo $temp;

intranet_closedb();

?>