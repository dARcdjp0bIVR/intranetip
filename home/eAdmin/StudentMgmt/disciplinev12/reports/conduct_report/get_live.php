<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$stdAry = explode(',', $studentID);

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();
$stdList = array();

$selectedStudent = explode(',', $studentID);

$select = "<select name='studentID[]' id='studentID[]' multiple size='5'>";

$classAry[0] = $class;
$studentAry = $ldiscipline->getStudentListByClassID($classAry);

for($i=0; $i<sizeof($studentAry); $i++) {
	list($id, $name) = $studentAry[$i];	
	$select .= "<option value='$id'";
	$select .= (in_array($id, $selectedStudent)) ? " selected" : "";
	$select .= ">".$studentAry[$i][1]."</option>";
}
$select .= "</select>";
$select .= $linterface->GET_BTN($button_select_all, "button", "onClick=SelectAll(document.form1.elements['studentID[]'])");
intranet_closedb();

echo $select;
?>