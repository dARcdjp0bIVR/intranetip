<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$linterface = new interface_html("popup.html");
$ldiscipline = new libdisciplinev12();

$result = "";

$li = new libfilesystem();

//$result .= "<div align='right'>".$linterface->GET_BTN($button_print, "button", "javascript:window.print()")."</div>";
$result .= "
		<table width='100%' align='center' class='print_hide' border='0'>
		<tr>
			<td align='right'>". $linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2") ."</td>
		</tr>
		</table>";

$studentID2 = explode(',', $studentID2);
$title2 = urldecode($title2);

$dataAry['displayWarning'] = $displayWarning;
$dataAry['displayWaived'] = $displayWaived;

$result .= $ldiscipline->getConductReportByID($periodType2, $SchoolYear2, $semester2, $SectionFr2, $SectionTo2, $class2, $studentID2, $print, $title2, $displayLogo2, $showDate2, $dataAry);

$name = $ldiscipline->getUserNameByID($_SESSION['UserID']);
$printedBy = $iDiscipline['PrintedBy']." : ".$name[0];
	
# Start layout
?>
<style type='text/css'>
<!--
print_hide {display:none;}
-->
</style>

<br />
	<?=$result?>

<input type="hidden" name="year" id="year" value="<?=Get_Current_Academic_Year_ID()?>">
</form>
<?
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

intranet_closedb();
?>
