<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ConductMarkCalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();


# class menu
//$select_class = $ldiscipline->getSelectClass("name='class' id='class' onChange=\"if(this.value!='') {showResult(this.value);} else { ClearStudent();}\"", $class, "-- ".$button_select." --", true);
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"class\" id\"class\" onChange=\"showResult(this.value)\"", $class, "-- ".$i_alert_pleaseselect." --", "");

if($SchoolYear == "")
	$SchoolYear = Get_Current_Academic_Year_ID();
	
if($showDate=="")
	$showDate = Date("Y-m-d");

# School Year Menu
$selectSchoolYear .= $ldiscipline->getConductSchoolYear($SchoolYear);

if($submitBtn != "") {
	
	$studentAry = $ldiscipline->storeStudent($studentID, $class);
	
	$dataAry['displayWarning'] = $displayWarning;
	$dataAry['displayWaived'] = $displayWaived;
	//$result .= $ldiscipline->getConductReportByID($periodType, $SchoolYear, $semester, $SectionFr, $SectionTo, $class, $studentID, $print, $title, $displayLogo, "", $dataAry);
	$result .= $ldiscipline->getConductReportByID($periodType, $SchoolYear, $semester, $SectionFr, $SectionTo, $class, $studentAry, $print, $title, $displayLogo, "", $dataAry);
	$result .= "<br />";
	$result .= "<div align='center'>".$linterface->GET_ACTION_BTN($button_print, "button", "goPrint()", "")."</div>";
}


# menu highlight setting
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "ConductReport";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eDiscipline['Student_Conduct_Report'], "/home/admin/disciplinev12/reports/conduct_report/index.php");

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
var xmlHttp

function showResult(str)
{
	
	<? 
	if($submitBtn!="") {
		//echo "var std = '".implode(',',$studentID)."';";
		echo "var std = '$studentID'";	
	} else { 
		echo "var std = '';";
	}
	?>
	
	if (str.length==0 || str==0)
		{ 
			document.getElementById("studentID").innerHTML = "";
			document.getElementById("studentID").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "../get_live.php";
	//url += "?class=" + str
	url += "?targetClass=" + str;
	url += "&sid=" + std;

	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanStudent").innerHTML = xmlHttp.responseText;
		document.getElementById("spanStudent").style.border = "0px solid #A5ACB2";
		
	} 
}

var xmlHttp2

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	url += "&hideWholeYear=<?=$ConductMarkCalculationMethod?>";
	
	xmlHttp2.onreadystatechange = stateChanged2 
	xmlHttp2.open("GET",url,true)
	xmlHttp2.send(null)
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function checkForm() {
	/*
	if(document.getElementById('showDate').value=="") {
		alert("<?=$i_alert_pleasefillin." ".$eDiscipline['Date']?>");	
		return false;
	}
	if(!check_date(document.getElementById('showDate'),'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))
	{
		return false;
	}
	*/	
	
	if(document.getElementById('class').value=="") {
		alert("<?=$i_alert_pleaseselect." ".$i_general_class?>");
		return false;	
	}
	/*
	var choice = document.getElementById('studentID[]');
	var total = 0;
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true) {
			total += 1;
		}
	}
	if(total==0) {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Student?>");	
		return false;
	}
	*/
	
	if(document.getElementById('periodType1').checked==true && ((!check_date(document.getElementById('SectionFr'),'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(document.getElementById('SectionTo'),'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	
	if(document.getElementById('periodType1').checked==true && (document.getElementById('SectionFr').value > document.getElementById('SectionTo').value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}

	document.getElementById('print').value = "";	
	document.form1.action = "index.php";
	document.form1.target = "_self";
}

function goPrint() {
	document.getElementById('print').value = 1;	
	document.form1.action = "print.php";
	document.form1.target = "_blank";
	document.form1.submit();
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function ClearStudent(obj) {
	document.getElementById("spanStudent").innerHTML = "<select name='studentID[]' id='studentID[]' class='formtextbox' multiple size='5'></select>";

}
</script>

<br />
<form name="form1" method="post" action="">
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td width="20%" class="formfieldtitle"><?=$eDiscipline['Date']?><span class="tabletextrequire">* </span></td>
		<td width="80%">
			<?=$linterface->GET_DATE_PICKER("showDate",$showDate)?>
		</td>
	</tr>
	<tr>
		<td width="20%" class="formfieldtitle" valign="top"><?=$i_general_class?><span class="tabletextrequire">* </span></td>
		<td width="80%" class="tablerow1">
			<table border="0" cellpadding="3" cellspacing="0">
				<tr>
					<td valign="top"><?=$select_class?></td>
				</tr>
				<tr>
					<td><div id='spanStudent'>
						<select name="studentID" id="studentID" class="formtextbox"></select>
						</div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" class="formfieldtitle" valign="top"><?=$iDiscipline['Period']?><span class="tabletextrequire">* </span></td>
		<td width="80%" class="tablerow1">
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td height="30" colspan="6" onClick="form1.periodType0.checked=true;">
						<input name="periodType" type="radio" id="periodType0" value="1" <? if($periodType=="" || $periodType==1) echo "checked";?>>
						<?=$i_Discipline_School_Year?> <select name="SchoolYear" id="SchoolYear" onFocus="form1.periodType0.checked=true;" onChange="changeTerm(this.value)"><?=$selectSchoolYear?></select>
						<?=$i_Discipline_Semester?> <span id="spanSemester"><select name='semester' id='semester' onFocus="form1.periodType0.checked=true;"><?=$SemesterMenu?></select></span>
					</td>
				</tr>
				<tr>
					<td colspan="4" onClick="form1.periodType1.checked=true;"><input name="periodType" type="radio" id="periodType1" value="2" <? if($periodType==2) echo "checked";?>><?=$iDiscipline['Period_Start']?> &nbsp;
					<?=$linterface->GET_DATE_PICKER("SectionFr",$SectionFr)?>&nbsp;
					<?=$i_To?>&nbsp;
					<?=$linterface->GET_DATE_PICKER("SectionTo",$SectionTo)?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" class="formfieldtitle" valign="top"><?=$i_general_title?></td>
		<td width="80%" class="tablerow1"><?=$linterface->GET_TEXTAREA('title', stripslashes($title));?></td>
	</tr>
	<tr>
		<td width="20%" class="formfieldtitle" valign="top"><?=$Lang['eDiscipline']['DisplayWaivedRecord']?></td>
		<td width="80%" class="tablerow1"><input type="radio" name="displayWaived" id="displayWaived1" value="1" <? if($displayWaived==1 || $displayWaived=="") {echo "checked";} ?>><label for="displayWaived1"><?=$i_general_yes?></label><input type="radio" name="displayWaived" id="displayWaived2" value="0" <? if($displayWaived=='0') {echo "checked";} ?>><label for="displayWaived2"><?=$i_general_no?></label></td>
	</tr>
	<tr>
		<td width="20%" class="formfieldtitle" valign="top"><?=$Lang['eDiscipline']['DisplayWarningRecord']?></td>
		<td width="80%" class="tablerow1"><input type="radio" name="displayWarning" id="displayWarning1" value="1" <? if($displayWarning==1 || $displayWarning=="") {echo "checked";} ?>><label for="displayWarning1"><?=$i_general_yes?></label><input type="radio" name="displayWarning" id="displayWarning2" value="0" <? if($displayWarning=='0') {echo "checked";} ?>><label for="displayWarning2"><?=$i_general_no?></label></td>
	</tr>
	<tr>
		<td width="20%" class="formfieldtitle" valign="top"><?=$Lang['eDiscipline']['DisplaySchoolLogo']?></td>
		<td width="80%" class="tablerow1"><input type="radio" name="displayLogo" id="displayLogo1" value="1" <? if($displayLogo==1 || $displayLogo=="") {echo "checked";} ?>><label for="displayLogo1"><?=$i_general_yes?></label><input type="radio" name="displayLogo" id="displayLogo2" value="0" <? if($displayLogo=='0') {echo "checked";} ?>><label for="displayLogo2"><?=$i_general_no?></label></td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td width="80%" class="tablerow1"><?= $linterface->GET_ACTION_BTN($button_submit, "submit", "return checkForm()", "submitBtn")?></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2"><?=$result?></td>
	</tr>
</table>

<input type="hidden" name="content" value='<?=urlencode($result)?>' />
<input type="hidden" name="year" id="year" value="<?=getCurrentAcademicYear()?>">
<input type="hidden" name="print" id="print" value="<?=$print?>">
<input type="hidden" name="showDate2" id="showDate2" value="<?=$showDate?>">
<input type="hidden" name="class2" id="class2" value="<?=$class?>">
<input type="hidden" name="studentID2" id="studentID2" value="<? if(sizeof($studentAry)>0) echo implode(',', $studentAry) ?>">
<input type="hidden" name="periodType2" id="periodType2" value="<?=$periodType?>">
<input type="hidden" name="SchoolYear2" id="SchoolYear2" value="<?=$SchoolYear?>">
<input type="hidden" name="semester2" id="semester2" value="<?=$semester?>">
<input type="hidden" name="SectionFr2" id="SectionFr2" value="<?=$SectionFr?>">
<input type="hidden" name="SectionTo2" id="SectionTo2" value="<?=$SectionTo?>">
<input type="hidden" name="title2" id="title2" value="<?=urlencode($title)?>">
<input type="hidden" name="displayLogo2" id="displayLogo2" value="<?=$displayLogo?>">
<input type="hidden" name="selectedStudentList" id="selectedStudentList" value="<?=$selectedStudentList?>">
</form>
<script language="javascript">
<!--
<? if($submitBtn != "") { 
	echo "showResult('$class');";
	echo "changeTerm('$SchoolYear');\n";
} else {
	echo "changeTerm(document.getElementById('SchoolYear').value);\n";
}
?>
//-->
</script>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
