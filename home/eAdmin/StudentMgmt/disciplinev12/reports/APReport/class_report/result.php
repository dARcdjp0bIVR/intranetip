<?php
# modifying : yat

################################################
#
#	Date:	2014-11-20	Bill
#			Improved: display both record data and modified date when sort by modified date
#
#	Date:	2014-11-10	Bill
#			Added: display modified date - $sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']
#				
#	Date:	2011-12-08	YatWoon
#			Improved: display Merit/Demerit Record(s) [Case#2011-1110-1418-20066]
#
################################################
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Class_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();
$CurrentPage = "Reports_APReport";
$CurrentPageArr['eDisciplinev12'] = 1;
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $ldiscipline->return_APReport_TAGS(4);
$linterface->LAYOUT_START();
$fc = new form_class_manage();

$startdate = $startdate==""?date('Y-m-d'):$startdate;
$enddate = $enddate==""?date('Y-m-d'):$enddate;

$pos = strpos($targetClass,"::");
if($pos !== false)
{
	# select single class 
	$selectedClass = str_replace("::","",$targetClass);
}
else
{
	$TempClassInfo = $fc->Get_Class_List_By_YearID($targetClass);
	if(!empty($TempClassInfo))
	{
		foreach($TempClassInfo as $k=>$d)
		$selectedClass.= $d['YearClassID'] . ",";
	}
	$selectedClass = substr($selectedClass, 0, strlen($selectedClass)-1);
}

if(!empty($targetClass))
{
	$class_sql = ' and d.YearClassID in ('. $selectedClass .')';
}

$statusConds = ($include_waive) ? " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED." OR a.RecordStatus=".DISCIPLINE_STATUS_WAIVED.")" : " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;
$statusConds .= ($include_unreleased) ? "" : " AND a.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED;

# Fat Ho Cust
$dateSql = 'a.RecordDate';
$display_DateModified = false;
if($sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']){
	$dateSql = $dateSearchType? 'DATE_FORMAT(a.DateModified, \'%Y-%m-%d\')' : $dateSql;
	$display_DateModified = $dateSearchType? true : false;
}
 
$namefield = getNameFieldByLang("b.");
$pic_namefield = getNameFieldByLang("e.");
$Current_Academic_Year_ID = Get_Current_Academic_Year_ID();
$sql = '
		select 
			b.ClassName,
			b.ClassNumber,
			'. $namefield .' as studentname,
			a.RecordDate, 
			a.ItemText, 
			a.Remark, 
			a.ProfileMeritType, 
			a.ProfileMeritCount, 
			a.ConductScoreChange, 
			a.PICID,
			a.SubScore1Change,
			DATE_FORMAT(a.DateModified, \'%Y-%m-%d\')
		from 
			DISCIPLINE_MERIT_RECORD as a
			inner join INTRANET_USER as b on (b.UserID=a.StudentID)
			inner join YEAR_CLASS_USER as c on (c.UserID = b.UserID)
			inner join YEAR_CLASS as d on (d.YearClassID = c.YearClassID and d.AcademicYearID='. $Current_Academic_Year_ID .' '. $class_sql .')
			inner join YEAR as f on (f.YearID = d.YearID)
		where 
			a.MeritType = '. $meritType .' and 
			'.$dateSql.' >= "'. $startdate .'" and '.$dateSql.'<="'. $enddate .'" and
			b.ClassNumber > 0
			'.$statusConds.'
		order by 
			f.Sequence, d.Sequence, b.ClassName, b.ClassNumber,'.$dateSql.'
';
$result = $ldiscipline->returnArray($sql);

### build data array
$DataAry = array();
$display = "";
if(!empty($result))
{
	foreach($result as $k=>$d)
	{
		list($this_classname,$this_classnumber,$this_name,$this_date,$this_item,$this_remark,$this_merittype,$this_meritcount,$this_conduct,$this_pic, $this_study_score, $this_modified) = $d;
		
		$DataAry[$this_classname][$this_classnumber][] = array($this_name,$this_date,$this_item,$this_remark,$this_merittype,$this_meritcount,$this_conduct,$this_pic, $this_study_score, $this_modified);
	}
}
if(!empty($DataAry))
{
	### build display
	foreach($DataAry as $this_classname=>$d)
	{
		$display .= $linterface->GET_NAVIGATION2($this_classname);
		
		$class_conduct_total = 0;
		$class_study_total = 0;
		
		$display.= '<div class="table_board">';
		$display.= '<table class="common_table_list_v30 view_table_list_v30">';
		$display.= '<tr>
					<th width="5%">'. $i_ClassNumber.'</th>
					<th width="15%">'. $i_UserStudentName.'</th>
					<th width="10%" class="sub_row_top">'. $i_general_record_date .'</th>';
		if($display_DateModified) {
		$display.= '<th width="8%" class="sub_row_top">'. $Lang['eDiscipline']['APReportCust']['ModifiedDate'].'</th>';
		}
		$display.= '<th width="25%" class="sub_row_top">'. $i_Discipline_System_general_record.'</th>
					<th width="15%" class="sub_row_top">'. $i_Discipline_System_general_remark.'</th>
					<th width="10%" class="sub_row_top">'. $eDiscipline["MeritDemeritContent"].'</th>
					<th width="5%" class="sub_row_top">'. $i_Discipline_System_ConductScore.'</th>';
		if($ldiscipline->UseSubScore) {
			$display.= '<th width="5%" class="sub_row_top">'. $i_Discipline_System_Subscore1.'</th>';
		}
		$display.='
					<th width="15%" class="sub_row_top">'. $eDiscipline['PICTeacher'].'</th>
					</tr>';
					
		foreach($d as $this_classnumber=>$d2)
		{
			$i=0;
			foreach($d2 as $k=>$d3)
			{
				list($this_name,$this_date,$this_item,$this_remark,$this_merittype,$this_meritcount,$this_conduct,$this_pic, $this_study_score, $this_modified) = $d3;
				$display .= '<tr class="sub_row">';
				if($i==0)
				{
					$display .= '<td rowspan='. sizeof($d2) .' class="sub_row_group">'. $this_classnumber .'</td>';
					$display .= '<td rowspan='. sizeof($d2) .' class="sub_row_group">'. $this_name .'</td>';
				}
				$display .= '<td>'. $this_date .'</td>';
				if($display_DateModified) {
					$display.= '<td>'. $this_modified.'</td>';
				}
				$display .= '<td>'. $this_item .'</td>';
				$display .= '<td>'. ($this_remark ? $this_remark : $Lang['General']['EmptySymbol']) .'</td>';
				
				$this_merit_str = $ldiscipline->returnDeMeritStringWithNumber($this_meritcount, $ldiscipline->RETURN_MERIT_NAME($this_merittype));
				$display .= '<td>'. $this_merit_str .'</td>'; 
				
				$display .= '<td>'. $this_conduct .'</td>';
				if($ldiscipline->UseSubScore) {
					$display .= '<td>'. $this_study_score .'</td>';	
				}
				$display .= '<td>'. ($this_pic ? $ldiscipline->getPICNameList($this_pic) : $Lang['General']['EmptySymbol']) .'</td>';
				$display .= '</tr>';
				$i++;	
				$class_conduct_total += $this_conduct;
				$class_study_total += $this_study_score;
			}		
		}
		
		$colspan = ($ldiscipline->UseSubScore) ? 3 : 2; 
		$colspan = $display_DateModified? ++$colspan : $colspan;
		
		# display couduct total
		$display .= '<tr>';
		$display .= '<td colspan=6 align="right">'. $Lang['eDiscipline']['TotalConductScore'] .'</td>';
		$display .= '<td colspan='.$colspan.'>'. $class_conduct_total .'</td>';
		$display .= '</tr>';

		if($ldiscipline->UseSubScore) {
			# display study score total
			$display .= '<tr>';
			$display .= '<td colspan=6 align="right">'. $Lang['eDiscipline']['TotalSubScore'] .'</td>';
			$display .= '<td colspan='.$colspan.'>'. $class_study_total .'</td>';
			$display .= '</tr>';
		}

		
		# display student total
		$display .= '<tr>';
		$display .= '<td colspan=6 align="right">'. $Lang['eDiscipline']['TotalNoStudent'] .'</td>';
		$display .= '<td colspan='.$colspan.'>'. sizeof($d) .'</td>';
		$display .= '</tr>';
		
		$display.= "</table><br>";				
	}		
}
else
{
	$display .= "<div class='no_record_find_short_v30'>". $Lang['General']['NoRecordFound'] ."</div>";
	$no_record = 1;
}
?>

<script language="javascript">
<!--
function hideOption()
{
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOption()
{
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function doPrint()
{
	newWindow('print.php?startdate=<?=$startdate?>&enddate=<?=$enddate?>&meritType=<?=$meritType?>&targetClass=<?=$targetClass?>&include_waive=<?=$include_waive?>&include_unreleased=<?=$include_unreleased?>&dateSearchType=<?=$dateSearchType?>',10);
}

function doExport()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "";
}

//-->
</script>


<div id="div_form" class="report_option report_hide_option">
	<span id="spanShowOption" class="spanShowOption">
		<a href="javascript:showOption();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOption();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	<span class="Form_Span" style="display:none"><?=$ldiscipline_ui->AP_Class_Report_Form($startdate, $enddate, $meritType, $targetClass, $include_waive, $include_unreleased, $dateSearchType)?></span>
</div>


<?=$display?>


<div class="edit_bottom_v30">
<p class="spacer"></p>
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "doPrint();")?> 
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "doExport();")?>
<p class="spacer"></p>
</div>

<?
intranet_closedb();
 $linterface->LAYOUT_STOP();
?>