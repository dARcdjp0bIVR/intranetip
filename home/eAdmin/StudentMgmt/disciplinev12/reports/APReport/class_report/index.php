<?php
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Class_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

$CurrentPage = "Reports_APReport";
$CurrentPageArr['eDisciplinev12'] = 1;
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$TAGS_OBJ = $ldiscipline->return_APReport_TAGS(4);
$linterface->LAYOUT_START();

$startdate = $startdate==""?date('Y-m-d'):$startdate;
$enddate = $enddate==""?date('Y-m-d'):$enddate;


?>

<?=$ldiscipline_ui->AP_Class_Report_Form($startdate, $enddate);?>



<?
intranet_closedb();
 $linterface->LAYOUT_STOP();
?>