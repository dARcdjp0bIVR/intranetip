<?php
// Modifying by : 

############ Change Log Start #############################
#
#	Date	:	2020-09-21	(Bill)	[2020-0921-0935-24066]
#				fixed cannot display correct gen report date
#
#	Date	:	2017-11-17	(Bill)	[2017-0926-1041-46054]
#				Hide received AP Count	($sys_custom['eDiscipline']['HYKHideAPReport'])
#
############ Change Log End #############################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$filename = "award_punishment_report.csv";	

$lclass = new libclass();
$linterface = new interface_html();

$selectYear = ($selectYear == '') ? getCurrentAcademicYear() : $selectYear;

//$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode("||", $SelectedFormText);

$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode("||", $SelectedClassText);

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode("||", $SelectedItemIDText);

if ($Period == "YEAR") {
	$parPeriodField1 = $selectYear;
	$parPeriodField2 = $selectSemester;
	$yearID = $selectYear;
}
else {
	$parPeriodField1 = $textFromDate;
	$parPeriodField2 = $textToDate;
	$temp = getAcademicYearInfoAndTermInfoByDate($textFromDate);
	$yearID = $temp[0];
}

if ($rankTarget == "form") {
	$parByFieldArr = $SelectedFormTextArr;
}
else {
	$parByFieldArr = $SelectedClassTextArr;
}

if($categoryID==1) {			# "Late"
	$parStatsFieldArr[0] = 0;
	$parStatsFieldTextArr[0] = 'Late';
}
else if($categoryID==2) {		# "Homework not submitted"
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
}
else {
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
}

### Table Data ###
$data = $ldiscipline->retrieveAwardPunishReport($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID, $include_waive, $include_unreleased);

################################################
############ Report Table Content ##############

// [2017-0926-1041-46054] 
if($sys_custom['eDiscipline']['HYKHideAPReport']) {
	$i_MeritDemerit = array();
}
else {
	if($record_type == 1) {
		$i_MeritDemerit[1] = $i_Merit_Merit;
		$i_MeritDemerit[2] = $i_Merit_MinorCredit;
		$i_MeritDemerit[3] = $i_Merit_MajorCredit;
		$i_MeritDemerit[4] = $i_Merit_SuperCredit;
		$i_MeritDemerit[5] = $i_Merit_UltraCredit;
	}
	else if($record_type == -1) {
		$i_MeritDemerit[0] = $i_Merit_Warning;
		$i_MeritDemerit[-1] = $i_Merit_BlackMark;
		$i_MeritDemerit[-2] = $i_Merit_MinorDemerit;
		$i_MeritDemerit[-3] = $i_Merit_MajorDemerit;
		$i_MeritDemerit[-4] = $i_Merit_SuperDemerit;
		$i_MeritDemerit[-5] = $i_Merit_UltraDemerit;
	}
}
	
$itemTotal = array();
$columnMeritDemeritCount = array();

$exportArr = array();

# Header of Report Table
$p = 0;
if($rankTarget=="form") {
	$exportArr[$p][] = $i_Discipline_Form;
	//$exportArr[$p][] = $i_Discipline_Class;
}
else if($rankTarget=="class") {
	$exportArr[$p][] = $i_Discipline_Class;
}
else {
	$exportArr[$p][] = $i_general_name;
	$exportArr[$p][] = $i_ClassNumber;
}

foreach($parStatsFieldTextArr as $tempValue) {
	$exportArr[$p][] = intranet_htmlspecialchars(stripslashes($tempValue));
}

$exportArr[$p][] = "";
foreach($i_MeritDemerit as $tempMeritDemerit) {
	$exportArr[$p][] = $tempMeritDemerit;
}
$p++;

# Content of Report Table
//debug_r($data);
/*
if ($rankTarget == "form") {
	$sql = "SELECT CLS.ClassName FROM INTRANET_CLASS CLS LEFT OUTER JOIN INTRANET_CLASSLEVEL LVL ON (CLS.ClassLevelID=LVL.ClassLevelID) WHERE LVL.LevelName IN ('".implode('\',\'',$parByFieldArr)."')";
	$temp = $ldiscipline->returnVector($sql);
	$parByFieldArr = $temp;
}
*/

if($rankTarget != "student")
{
	for($i=0; $i<sizeof($parByFieldArr); $i++) {					# Class / Form
		if($rankTarget=="form")
		{
			$w = 0;
			$exportArr[$p][$w] = stripslashes($parByFieldArr[$i]);
			$w++;
			
			for($j=0; $j<sizeof($parStatsFieldArr); $j++) {			# for each item
				$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][stripslashes($parByFieldArr[$i])] : 0;
				$w++;
				$itemTotal[$parStatsFieldArr[$j]] += addslashes($data[$parStatsFieldArr[$j]][stripslashes($parByFieldArr[$i])]);
			}
			
			# (YEAR/DATE , Period1, Period2, Class/Form, ClassName/FormName, SelectedItemID, SelectedItemName)
			$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromAP($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "", $include_waive, $include_unreleased);
			$exportArr[$p][$w] = "";
			$w++;
			
			if($record_type==1) {
				$m = 1;
			}
			else {
				$m = 0;
			}
			foreach($i_MeritDemerit as $columnNo) {
				$tempCount = 0;
				for($n=0; $n<sizeof($meritDemeritCount); $n++) {
					if($meritDemeritCount[$n]['ProfileMeritType']==$m)
						$tempCount += $meritDemeritCount[$n]['countTotal'];
				}
				$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
				$w++;
				$columnMeritDemeritCount[$m] += $tempCount;
				
				if($record_type==1) {
					$m++;
				}
				else {
					$m--;
				}
			}
			$p++;
		}
		else {
			//$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($parByFieldArr[$i]);
			//for($k=0; $k<sizeof($stdInfo); $k++) {
				$w = 0;
				$exportArr[$p][$w] = $parByFieldArr[$i];
				$w++;
				//$exportArr[$p][$w] = $parByFieldArr[$i]."-".$stdInfo[$k][0];
				//$w++;
				
				for($j=0; $j<sizeof($parStatsFieldArr); $j++) {		# for each item
					$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][$parByFieldArr[$i]])) ? $data[$parStatsFieldArr[$j]][$parByFieldArr[$i]] : "0";
					$w++;
					$itemTotal[$parStatsFieldArr[$j]] += $data[$parStatsFieldArr[$j]][$parByFieldArr[$i]];
				}
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromAP($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "", $include_waive, $include_unreleased);
				$exportArr[$p][$w] = "";
				$w++;
				//debug_r($meritDemeritCount);
				if($record_type==1) {
					$m = 1;
				}
				else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
					$w++;
					$columnMeritDemeritCount[$m] += $tempCount;
					
					if($record_type==1) {
						$m++;
					}
					else {
						$m--;
					}
				}
				$p++;
			//}
		}
	}
}
else
{
			for($k=0; $k<sizeof($studentID); $k++) {				# Student
				$stdInfo = $ldiscipline->getStudentNameByID($studentID[$k], $yearID);
				$w = 0;
				$exportArr[$p][$w] = $stdInfo[0][1];
				$w++;
				$exportArr[$p][$w] = $stdInfo[0][2]."-".$stdInfo[0][3];
				$w++;
				
				for($j=0; $j<sizeof($parStatsFieldArr); $j++) {		# for each item
					$exportArr[$p][$w] = (isset($data[(stripslashes($parStatsFieldArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? $data[(stripslashes($parStatsFieldArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]] : "0";
					$w++;
					$itemTotal[$parStatsFieldArr[$j]] += $data[$parStatsFieldArr[$j]][$stdInfo[0][2]][$stdInfo[0][3]];
				}
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromAP($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k], $include_waive, $include_unreleased);
				$exportArr[$p][$w] = "";
				$w++;
				
				if($record_type==1) {
					$m = 1;
				}
				else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
					$w++;
					$columnMeritDemeritCount[$m] += $tempCount;
					
					if($record_type==1) {
						$m++;
					}
					else {
						$m--;
					}
				}
				$p++;
			}
}

$exportArr[$p][] = $i_Discipline_System_Report_Class_Total;
if($rankTarget=="student")	
	$exportArr[$p][] = "";

for($k=0; $k<sizeof($parStatsFieldArr); $k++) {
	$exportArr[$p][] = (isset($itemTotal[stripslashes($parStatsFieldArr[$k])])) ? $itemTotal[stripslashes($parStatsFieldArr[$k])] : "0";
}

$exportArr[$p][] = "";
if($record_type==1) {
	$m = 1;
}
else {
	$m = 0;
}
foreach($i_MeritDemerit as $columnNo) {
	$exportArr[$p][] .= $columnMeritDemeritCount[$m];
	
	if($record_type==1) {
		$m++;
	}
	else {
		$m--;
	}
}

############ End of Report Table ###############
################################################

$numOfColumn = sizeof($parStatsFieldTextArr) + sizeof($i_MeritDemerit) + 2;

$export_content = $eDiscipline['AwardPunishmentReport'];
if($Period=="YEAR") {
	$termName = ($parPeriodField2==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($parPeriodField2);
	$export_content .= " (".$ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." ".$termName.")\n";
}
else {
	$export_content .= " (".$iDiscipline['Period_Start']." ".$parPeriodField1." ".$iDiscipline['Period_End']." ".$parPeriodField2.")\n";
}

$generationDate = getdate();
// [2020-0921-0935-24066]
//$export_content .= $eDiscipline['ReportGeneratedDate']." ".$generationDate['year']."/".$generationDate['mon']."/".$generationDate['wday']." ".$generationDate['hours'].":".$generationDate['minutes'].":".$generationDate['seconds']."\n\n";
$export_content .= $eDiscipline['ReportGeneratedDate']." ".$generationDate['year']."/".$generationDate['mon']."/".$generationDate['mday']." ".$generationDate['hours'].":".$generationDate['minutes'].":".$generationDate['seconds']."\n\n";

$export_content .= $lexport->GET_EXPORT_TXT($exportArr, "", "\t", "\r\n", "\t", $numOfColumn, "11");

intranet_closedb();

$lexport->EXPORT_FILE($filename, $export_content);
?>