<?
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Report-View"))
{
	$url = "./award_punishment/";
}
else if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Ranking_Report-View"))
{
	$url = "./ranking/";
}
else if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Ranking_Detail_Report-View"))
{
	$url = "./ranking_detail/";
}
else if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-Award_Punishment_Class_Report-View"))
{
	$url = "./class_report/";
}
else
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
				
header("Location: $url");		
						

intranet_closedb();
?>