<?php
# using: henry

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lsp = new libstudentprofile();

$lexport = new libexporttext();

# Retrieve Student Name
$lu_student = new libuser($targetID);
$student_name = $lu_student->UserName();
$class_name = $lu_student->ClassName;
$class_num = $lu_student->ClassNumber;

$year = $ldiscipline->getAcademicYearNameByYearID($yearID);

# Check display Reamrk field or not (customization for fyk 20080424)
$show_remark_in_personal_report = $sys_custom['Discipline_ShowRemarkInPersonalReport'];

$year_info = "\"".$i_Profile_Year."\"\t\"".str_replace('"','""',$year)."\"\n";
$utf_year_info = $i_Profile_Year."\t".$year."\t\r\n";


if(($selectSemester=="")||($selectSemester==0))
	$str_semester = $i_Discipline_System_Award_Punishment_Whole_Year;
	
else $str_semester = $ldiscipline->getTermNameByTermID($selectSemester);

$semester_info = "\"".$i_SettingsSemester."\"\t\"".str_replace('"','""',$str_semester)."\"\n";
$utf_semester_info = $i_SettingsSemester."\t".$str_semester."\t\r\n";


$header  = "\"Student Name\"\t\"".str_replace('"','""',$student_name)."(".str_replace('"','""',$class_name)." - ".str_replace('"','""',$class_num).")\"\n";
$header .= "\"".$i_general_startdate."\"\t\"".str_replace('"','""',$startdate)."\"\n";
$header .= "\"".$i_general_enddate."\"\t\"".str_replace('"','""',$enddate)."\"\n";
$header .= $year_info;
$header .= $semester_info."\n";

$preheader = "Student Name\t".$student_name."(".$class_name." - ".$class_num.")\t\r\n";
$preheader .= $i_general_startdate.":\t".$startdate."\t\r\n";
$preheader .= $i_general_enddate.":\t".$enddate."\t\r\n";
$preheader .= $utf_year_info;
$preheader .= $utf_semester_info."\n";
//$preheader = iconv('BIG5', 'UTF-16LE', $preheader);

$content = "";

switch($detailType)
{
        case 1:	# Punihsment Record
                $string_type["0"] = $i_Merit_Warning;
                $string_type["-1"] = $i_Merit_BlackMark;
                $string_type["-2"] = $i_Merit_MinorDemerit;
                $string_type["-3"] = $i_Merit_MajorDemerit;
                $string_type["-4"] = $i_Merit_SuperDemerit;
                $string_type["-5"] = $i_Merit_UltraDemerit;

                // Retrieve Demerit Record
                if($selectSemester == 0){
                	$selectSemester = "";
                }
                $result = $ldiscipline->retrieveStudentDemeritRecord($targetID, $startdate, $enddate, $yearID, $selectSemester,$show_waive_status);

                // Build Table Header
                $header .= "\"".$i_Discipline_System_general_date_demerit."\",\"".$i_Discipline_System_general_record."\",\"".$i_Discipline_System_Add_Demerit."\"";
                $exportColumn = array($i_Discipline_System_general_date_demerit, $i_Discipline_System_general_record, $i_Discipline_System_Add_Demerit);
                
                if(!$ldiscipline->Hidden_ConductMark)
                {
                	$header .=",\"".$i_Discipline_System_Conduct_ScoreDifference."\"";
                	$exportColumn[] = $i_Discipline_System_Conduct_ScoreDifference;
            	}
                
                if ($ldiscipline->UseSubScore) {
	                $header .= ",\"".$i_Discipline_System_Subscore1_ScoreDifference."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$i_Discipline_System_Subscore1_UpdatedScore\"" : "");
	                $exportColumn[] = $i_Discipline_System_Subscore1_ScoreDifference;
                }
                if ($ldiscipline->use_subject) {
	                $header .= ",\"".$i_Subject_name."\"";
	                $exportColumn[] = $i_Subject_name;
                }
                $header .= ",\"".$i_Profile_PersonInCharge."\"";
                $exportColumn[] = $i_Profile_PersonInCharge;
				if($show_waive_status==1) {
					$header.=",\"".$i_Discipline_System_WaiveStatus."\"";
					 $exportColumn[] = $i_Discipline_System_WaiveStatus;
				}
				if($show_remark_in_personal_report)
				{
					$header.=",\"".$i_Discipline_System_general_remark."\"";
					 $exportColumn[] = $i_Discipline_System_general_remark;
				}
								
				$header.="\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
					$r = 3;
                        list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, $r_picid, $r_pic_name,$record_status, $remark, $fromConductRecord) = $result[$i];
                        $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                        
                        $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
                        	
                        if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
                        $content .= "\"".str_replace('"','""',$r_date)."\",\"".str_replace('"','""',$r_item)."\",\"".$record_data."\"";
                        $row = array($r_date, $r_item, $record_data);
                        
                        if (!$ldiscipline->Hidden_ConductMark) {
	                        $content .= ",\"".str_replace('"','""',$r_conduct)."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$t_subscore1_after\"" : "");
	                        $row[$r] = $r_conduct;
							$r++;
                        }
                        
                        if ($ldiscipline->UseSubScore) {
	                        $content .= ",\"".str_replace('"','""',$t_subscore1)."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$t_subscore1_after\"" : "");
	                        $row[$r] = $t_subscore1;
							$r++;
                        }
                        if ($ldiscipline->use_subject) {
	                        $content .= ",\"".str_replace('"','""',$t_subject)."\"";
	                        $row[$r] = $t_subject;
							$r++;
                        }
                        $content .= ",\"".str_replace('"','""',$r_pic_name)."\"";
                        $row[$r] = $r_pic_name;
						$r++;
                        if($show_waive_status==1) {
	                        $content.=",\"".str_replace('"','""',$str_waive_status)."\"";
	                        $row[$r] = $str_waive_status;
							$r++;
                        }
                        if($show_remark_in_personal_report)
                        {
	                        $content.=",\"".str_replace('"','""',$remark)."\"";
	                        $row[$r] = $remark;
							if($fromConductRecord) {
								$row[$r] .= $ldiscipline->displayGMDetailByAPRecordID($r_id, 1);
							}
							$r++;
                        }
                  		
                        $row_result[] = $row;
                        unset($row);
                        $content.="\n";
                }

                $filename = "punishment_record_".$targetID.".csv";
                break;
        case 2:	# Award Record
                $string_type["1"] = $i_Merit_Merit;
                $string_type["2"] = $i_Merit_MinorCredit;
                $string_type["3"] = $i_Merit_MajorCredit;
                $string_type["4"] = $i_Merit_SuperCredit;
                $string_type["5"] = $i_Merit_UltraCredit;

                // Retrieve Merit Record
                if($selectSemester == 0){
                	$selectSemester = "";
                }
                $result = $ldiscipline->retrieveStudentMeritRecord($targetID, $startdate, $enddate,$yearID,$selectSemester,$show_waive_status);

                // Build Table Header
                $header .= "\"".$i_Discipline_System_general_date_merit."\",\"".$i_Discipline_System_general_record."\",\"".$i_Discipline_System_Add_Merit."\"";
                $exportColumn = array($i_Discipline_System_general_date_merit, $i_Discipline_System_general_record, $i_Discipline_System_Add_Merit);
                
                if (!$ldiscipline->Hidden_ConductMark) {
	                $header .= ",\"".$i_Discipline_System_Conduct_ScoreDifference."\""; 
	                $exportColumn[] = $i_Discipline_System_Conduct_ScoreDifference;
                }
                
                if ($ldiscipline->UseSubScore) {
	                $header .= ",\"".$i_Discipline_System_Subscore1_ScoreDifference."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$i_Discipline_System_Subscore1_UpdatedScore\"" : "");
	                $exportColumn[] = $i_Discipline_System_Subscore1_ScoreDifference;
                }
                $header .= ",\"".$i_Profile_PersonInCharge."\"";
                $exportColumn[] = $i_Profile_PersonInCharge;
                
                if($show_waive_status==1) {
					$header.=",\"".$i_Discipline_System_WaiveStatus."\"";
					 $exportColumn[] = $i_Discipline_System_WaiveStatus;
				}
                
                if($show_remark_in_personal_report)
                {
					$header.=",\"".$i_Discipline_System_general_remark."\"";
					$exportColumn[] = $i_Discipline_System_general_remark;
                }

                $header.="\n";
                for ($i=0; $i<sizeof($result); $i++)
                {
				$r = 3;
                        list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $r_picid, $r_pic_name, $remark, $record_status, $fromConductRecord) = $result[$i];
                        $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                        
                        if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
                        $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
                        $content .= "\"".str_replace('"','""',$r_date)."\",\"".str_replace('"','""',$r_item)."\",\"". $record_data ."\"";
                        $row = array($r_date, $r_item, $record_data);
                        
                        if (!$ldiscipline->Hidden_ConductMark) {
	                        $content .= ",\"".str_replace('"','""',$r_conduct)."\""; 
	                        $row[$r] = $r_conduct;
							$r++;
                        }
                        
                        if ($ldiscipline->UseSubScore) {
	                        $content .= ",\"".str_replace('"','""',$t_subscore1)."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$t_subscore1_after\"" : "");
	                        $row[$r] = $t_subscore1;
							$r++;
                        }
                        $content .= ",\"".str_replace('"','""',$r_pic_name)."\"";
                        $row[$r] = $r_pic_name;
						$r++;
                        
                        if($show_waive_status==1) {
	                        $content.=",\"".str_replace('"','""',$str_waive_status)."\"";
	                        $row[$r] = $str_waive_status;
							$r++;
                        }
                        
                        if($show_remark_in_personal_report)
                        {
	                        $content.=",\"".str_replace('"','""',$remark)."\"";
	                        $row[$r] = $remark;
							if($fromConductRecord) {
								$row[$r] .= $ldiscipline->displayGMDetailByAPRecordID($r_id, 1);
							}							
							$r++;
                        }
                        
                        $content .= "\n";
                        
                        $row_result[] = $row;
                        unset($row);
                }
                $filename = "award_record_".$targetID.".csv";
                break;
        case 4:	# Award/Punihsment Record
                $string_type["0"] = $i_Merit_Warning;
                $string_type["-1"] = $i_Merit_BlackMark;
                $string_type["-2"] = $i_Merit_MinorDemerit;
                $string_type["-3"] = $i_Merit_MajorDemerit;
                $string_type["-4"] = $i_Merit_SuperDemerit;
                $string_type["-5"] = $i_Merit_UltraDemerit;
                $string_type["1"] = $i_Merit_Merit;
                $string_type["2"] = $i_Merit_MinorCredit;
                $string_type["3"] = $i_Merit_MajorCredit;
                $string_type["4"] = $i_Merit_SuperCredit;
                $string_type["5"] = $i_Merit_UltraCredit;

                // Retrieve Merit/Demerit Record
                if($selectSemester == 0){
                	$selectSemester = "";
                }
                $result = $ldiscipline->retrieveStudentProfileRecord($targetID, $startdate, $enddate, $yearID, $selectSemester,$show_waive_status);

                // Build Table Header
                if ($sys_custom['Discipline_ShowCategoryInPersonalReport']) 
                {
                	$header .= "\"".$i_general_record_date."\",\"".$i_Discipline_System_CategoryName."\",\"".$i_Discipline_System_general_record."\",\"".$i_Discipline_System_Add_Merit_Demerit."\"";
                	$exportColumn = array($i_general_record_date, $i_Discipline_System_CategoryName, $i_Discipline_System_general_record, $i_Discipline_System_Add_Merit_Demerit);
            	}
            	else
            	{
	            	$header .= "\"".$i_general_record_date."\",\"".$i_Discipline_System_general_record."\",\"".$i_Discipline_System_Add_Merit_Demerit."\"";
                	$exportColumn = array($i_general_record_date, $i_Discipline_System_general_record, $i_Discipline_System_Add_Merit_Demerit);	
            	}
            	
            	if (!$ldiscipline->Hidden_ConductMark) {
	                $header .= ",\"".$i_Discipline_System_Conduct_ScoreDifference."\""; 
	                $exportColumn[] = $i_Discipline_System_Conduct_ScoreDifference;
                }
                
                if ($ldiscipline->UseSubScore) {
	                $header .= ",\"".$i_Discipline_System_Subscore1_ScoreDifference."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$i_Discipline_System_Subscore1_UpdatedScore\"" : "");
	                $exportColumn[] = $i_Discipline_System_Subscore1_ScoreDifference;
                }
                $header .= ($ldiscipline->use_subject) ? ",\"".$i_Subject_name."\"" : "";
                if ($ldiscipline->use_subject) {
	                $exportColumn[] = $i_Subject_name;
                } else {
	                //$exportColumn[] = "";
                }
                $header .= ",\"".$i_Profile_PersonInCharge."\"";
                $exportColumn[] = $i_Profile_PersonInCharge;
                if($show_waive_status==1) {
                	$header.=",\"".$i_Discipline_System_WaiveStatus."\"";
                	$exportColumn[] = $i_Discipline_System_WaiveStatus;
            	}
            	if($show_remark_in_personal_report)
				{
					$header.=",\"".$i_Discipline_System_general_remark."\"";
					 $exportColumn[] = $i_Discipline_System_general_remark;
				}
                $header.="\n";

                for ($i=0; $i<sizeof($result); $i++)
                {
					$r = 3;
                        list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count, $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, $r_picid, $r_pic_name,$record_status, $MeritType, $remark, $cat_name, $fromConductRecord) = $result[$i];
                       
                        $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                        
                        $content .= "\"".str_replace('"','""',$r_date)."\",";
                        
                        if($sys_custom['Discipline_ShowCategoryInPersonalReport'])
                        	$content .= "\"".str_replace('"','""',$cat_name)."\",";
                        	
                        $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
                        	
                        if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
						{
							if($r_item=="其他記過事項" || $r_item=="其他記功事項")
								$r_item = $remark;
						}
					
                        $content .="\"".str_replace('"','""',$r_item)."\",\"".$record_data."\"";
                        //$row = array($r_date, $r_item, $r_profile_count.'yyy '.$string_type[$r_profile_type]);
						$row = array($r_date, $r_item, $record_data);
                        
                        if (!$ldiscipline->Hidden_ConductMark) {
	                        $content .= ",\"".str_replace('"','""',$r_conduct)."\""; 
	                        $row[$r] = $r_conduct;
							$r++;
                        }
                        
                        if ($ldiscipline->UseSubScore) {
	                        $content .= ",\"".str_replace('"','""',$t_subscore1)."\""; #.(($sys_custom['DisciplineShowScoreAfter']) ? ",\"$t_subscore1_after\"" : "");
	                        $row[$r] = $t_subscore1;
							$r++;
                        }
                        
                        if ($ldiscipline->use_subject) {
	                        $content .= ",\"".str_replace('"','""',$t_subject)."\"";
	                        $row[$r] = $t_subject;
							$r++;
                        }
                        $content .= ",\"".str_replace('"','""',$r_pic_name)."\"";
                        $row[$r] = $r_pic_name;
						$r++;
						
                        if($show_waive_status==1) {
                        	$content.=",\"".str_replace('"','""',$str_waive_status)."\"";
                        	$row[$r] = $str_waive_status;
							$r++;
                    	}
                    	if($show_remark_in_personal_report)
                        {
	                        $content.=",\"".str_replace('"','""',$remark)."\"";
	                        $row[$r] = $remark;
							if($fromConductRecord) {
								$row[$r] .= $ldiscipline->displayGMDetailByAPRecordID($r_id, 1);
							}							
							$r++;
                        }
                    	$row_result[] = $row;
                    	unset($row);
                        $content.="\n";
                }
                $filename = "award_punishment_record_".$targetID.".csv";
                break;
             case 6: # Conduct Mark Change Log
             		if($selectSemester == 0){
             			$selectSemester = "";
             		}
             		$result = $ldiscipline->retrieveConductScoreChangeLogRecord($targetID,$startdate,$enddate,$yearID,$selectSemester);
             		$header .= "\"".$i_general_record_date."\",\"".$i_Merit_Type."\",\"".$i_Discipline_System_general_record."\",\"".$i_Discipline_System_FromScore."\",";
                    $header .= "\"".$i_Discipline_System_ToScore."\",\"".$i_Profile_PersonInCharge."\"";
                    
                    if($show_remark_in_personal_report)
                    {
	                    $header .= ",\"".$i_Discipline_System_general_remark."\"";
                    }
                    $header .= "\n";
                    
                    $exportColumn = array($i_general_record_date, $i_Merit_Type, $i_Discipline_System_general_record, $i_Discipline_System_FromScore, $i_Discipline_System_ToScore, $i_Profile_PersonInCharge);
                    if($show_remark_in_personal_report)
                    {
	                    $exportColumn[] .= $i_Discipline_System_general_remark;
                    }
					
		             for ($i=0; $i<sizeof($result); $i++)
		             {
		                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
		
		                  switch($r_action_type){
			                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
			                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
			                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
			                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
			                  case 5 : $action = $i_Discipline_System_System_Revised; break;
			              }
			              if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
			              if($r_item =="--" && $r_action_type!=5){
				              $r_item = "$i_Discipline_System_Record_Already_Removed";
				          }
		                  
				          if(!$show_remark_in_personal_report)
				          {
								$content .= "\"".str_replace('"','""',$r_date)."\",\"".str_replace('"','""',$action)."\",\"".str_replace('"','""',$r_item)."\",\"".str_replace('"','""',$r_fromscore)."\",\"".str_replace('"','""',$r_toscore)."\",\"".str_replace('"','""',$r_pic_name)."\"\n";
								$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name);
				          }
				          else
				          {
				          		$content .= "\"".str_replace('"','""',$r_date)."\",\"".str_replace('"','""',$action)."\",\"".str_replace('"','""',$r_item)."\",\"".str_replace('"','""',$r_fromscore)."\",\"".str_replace('"','""',$r_toscore)."\",\"".str_replace('"','""',$r_pic_name)."\",\"".str_replace('"','""',$remark)."\"\n";
								$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name, $remark);
					  		}
					  	unset($row);
		             }
                  	$filename = "conduct_score_change_log_".$targetID.".csv";
             		break;
             case 7: # Subscore Change Log
             		if($selectSemester == 0){
             			$selectSemester = "";
             		}
                    $result = $ldiscipline->retrieveSubscore1ChangeLogRecord($targetID,$startdate,$enddate,$yearID,$selectSemester);
                    $header .= "\"".$i_general_record_date."\",\"".$i_Merit_Type."\",\"".$i_Discipline_System_general_record."\",\"".$i_Discipline_System_FromScore."\",";
                    $header .="\"".$i_Discipline_System_ToScore."\",\"".$i_Profile_PersonInCharge."\"";
                    if($show_remark_in_personal_report)
                    {
	                    $header .= ",\"".$i_Discipline_System_general_remark."\"";
                    }
                    $header .= "\n";
                    
                    $exportColumn = array($i_general_record_date, $i_Merit_Type, $i_Discipline_System_general_record, $i_Discipline_System_FromScore, $i_Discipline_System_ToScore, $i_Profile_PersonInCharge);
					if($show_remark_in_personal_report)
					{
						$exportColumn[] = $i_Discipline_System_general_remark;
						
					}
		             for ($i=0; $i<sizeof($result); $i++)
		             {
		                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
		
		                  switch($r_action_type){
			                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
			                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
			                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
			                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
			              }
			              if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
			              if($r_item =="--"){
				              $r_item = "$i_Discipline_System_Record_Already_Removed";
				          }
				          
				          if(!$show_remark_in_personal_report)
				          {
					        $header .="\"".str_replace('"','""',$r_date)."\",\"".str_replace('"','""',$action)."\",\"".str_replace('"','""',$r_item)."\",\"".str_replace('"','""',$r_fromscore)."\",\"".str_replace('"','""',$r_toscore)."\",\"".str_replace('"','""',$r_pic_name)."\"\n";
						  	$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name, $remark);  
				          }
				          else
				          {
				          	$header .="\"".str_replace('"','""',$r_date)."\",\"".str_replace('"','""',$action)."\",\"".str_replace('"','""',$r_item)."\",\"".str_replace('"','""',$r_fromscore)."\",\"".str_replace('"','""',$r_toscore)."\",\"".str_replace('"','""',$r_pic_name)."\",\"".str_replace('"','""',$remark)."\"\n";
						  	$row_result[] = array($r_date, $action, $r_item, $r_fromscore, $r_toscore, $r_pic_name, $remark);
					  	}
		             }
                  	$filename = "study_score_change_log_".$targetID.".csv";             
             		break; 
         case 8:	# Misconduct Record
				$header.="\"".$i_Discipline_System_general_date_demerit."\",\"".$iDiscipline['Accumulative_Category_Name']."\",";
				$header.="\"".$iDiscipline['Accumulative_Category_Item_Name']."\",";
				$header.="\"". $i_Profile_PersonInCharge ."\",";
				$header.="\"".$i_Discipline_System_general_remark."\"";
				
				if($show_waive_status==1) {
					$header.=",\"".$i_Discipline_System_WaiveStatus."\"";
				}
				
				$header .= "\n";
				
				$exportColumn = array($i_Discipline_System_general_date_demerit,$iDiscipline['Accumulative_Category_Name'], $iDiscipline['Accumulative_Category_Item_Name'], $i_Profile_PersonInCharge, $i_Discipline_System_general_remark);
				if($show_waive_status==1) {
					$exportColumn[] = $i_Discipline_System_WaiveStatus;
				}
				
				if($selectSemester == 0){
					$selectSemester = "";
				}
				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester, "-1", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				                 
				  $content.="\"".str_replace('"','""',$record_date)."\",\"".str_replace('"','""',$cat_name)."\",\"".str_replace('"','""',$item_name)."\",\"".str_replace('"','""',$r_pic_name)."\",\"".str_replace('"','""',$remark)."\"";
				  
				  if($show_waive_status==1){
      				    $content .= ",\"".str_replace('"','""',$str_waive_status)."\"";
	      				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark, $str_waive_status);

			      	 }
			      	 else {
      	 				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark);
				     }
				     $content .= "\n";

				}
				$filename = "misconduct_record".$targetID.".csv";
             break;     
		case 9:	# Good Conduct Record
				$header.="\"".$i_Discipline_System_general_date_demerit."\",\"".$iDiscipline['Accumulative_Category_Name']."\",";
				$header.="\"".$iDiscipline['Accumulative_Category_Item_Name']."\",";
				$header.="\"". $i_Profile_PersonInCharge ."\",";
				$header.="\"".$i_Discipline_System_general_remark."\"";
				
				if($show_waive_status==1) {
					$header.=",\"".$i_Discipline_System_WaiveStatus."\"";
				}
				
				$header .= "\n";
				
              	$exportColumn = array($i_Discipline_System_general_date_demerit,$iDiscipline['Accumulative_Category_Name'], $iDiscipline['Accumulative_Category_Item_Name'], $i_Profile_PersonInCharge, $i_Discipline_System_general_remark);
				if($show_waive_status==1) {
					$exportColumn[] = $i_Discipline_System_WaiveStatus;
				}
				
				if($selectSemester == 0){
					$selectSemester = "";
				}
				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester, "1", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				                 
				  $content.="\"".str_replace('"','""',$record_date)."\",\"".str_replace('"','""',$cat_name)."\",\"".str_replace('"','""',$item_name)."\",\"".str_replace('"','""',$r_pic_name)."\",\"".str_replace('"','""',$remark)."\"";
				  
				  if($show_waive_status==1){
      				    $content .= ",\"".str_replace('"','""',$str_waive_status)."\"";
	      				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark, $str_waive_status);

			      	 }
			      	 else {
      	 				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark);
				     }
				     $content .= "\n";

				}
				$filename = "goodconduct_record".$targetID.".csv";
             break;  
		case 10:	# Good /Mic Conduct Record
				$header.="\"".$i_Discipline_System_general_date_demerit."\"\t\"".$iDiscipline['Accumulative_Category_Name']."\"\t";
				$header.="\"".$iDiscipline['Accumulative_Category_Item_Name']."\"\t";
				$header.="\"". $i_Profile_PersonInCharge ."\"\t";
				$header.="\"".$i_Discipline_System_general_remark."\"";
				
				if($show_waive_status==1) {
					$header.="\t\"".$i_Discipline_System_WaiveStatus."\"";
				}
				
				$header .= "\n";
				
               	$exportColumn = array($i_Discipline_System_general_date_demerit,$iDiscipline['Accumulative_Category_Name'], $iDiscipline['Accumulative_Category_Item_Name'], $i_Profile_PersonInCharge, $i_Discipline_System_general_remark);
				if($show_waive_status==1) {
					$exportColumn[] = $i_Discipline_System_WaiveStatus;
				}

				if($selectSemester == 0){
					$selectSemester = "";
				}
				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester, "", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				                 
				  $content.="\"".str_replace('"','""',$record_date)."\"\t\"".str_replace('"','""',$cat_name)."\"\t\"".str_replace('"','""',$item_name)."\"\t\"".str_replace('"','""',$r_pic_name)."\"\t\"".str_replace('"','""',$remark)."\"";
				  
				 	 if($show_waive_status==1){
      				    $content .= "\t\"".str_replace('"','""',$str_waive_status)."\"";
	      				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark, $str_waive_status);

			      	 }
			      	 else {
      	 				$row_result[] = array($record_date, $cat_name, $item_name, $r_pic_name, $remark);
				     }
				     $content .= "\n";

				}
				$filename = "good_misconduct_record".$targetID.".csv";
             break;  
             
}
//debug_pr($result);
/*if (!$g_encoding_unicode) {
	$export_content = $header.$content;
	//output2browser($export_content,$filename);
	$lexport->EXPORT_FILE($filename, $export_content);
} else {*/
	$export_content = $preheader.$lexport->GET_EXPORT_TXT($row_result, $exportColumn, "\t", "\r\n", "\t", 0, "11");
	$lexport->EXPORT_FILE($filename, $export_content);
//}

/*
$export_content = $header.$content;
output2browser($export_content,$filename);
*/

/*
$export_content = $header.$content;

// Export Content
header("Content-type: application/octet-stream");
header("Content-Length: ".strlen($export_content) );
header("Content-Disposition: attachment; filename=\"".$filename."\"");
echo $export_content;
*/

?>
