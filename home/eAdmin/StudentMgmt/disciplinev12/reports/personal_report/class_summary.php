<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lsp = new libstudentprofile();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-ClassSummary-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

########################################################

# Show selection controls
if ($level == 1) # Class Level
{
    $nav1_name = $i_Discipline_System_Report_AwardPunish_ClassLevel;
    # Get selection controls
}
else
{
    $nav1_name = $i_Discipline_System_Report_AwardPunish_ClassRecord;
}

if ($flag == 1)
{
    if (!isset($target) || sizeof($target)==0)
    {
         $flag = 0;
    }

}
else
{
}

$ts = time();
if ($startdate == "")
{
    $startdate = date('Y-m-d',getStartOfAcademicYear($ts));
}
if ($enddate == "")
{
    $enddate = date('Y-m-d',getEndOfAcademicYear($ts));
}

# Get Levels and Classes
$levels = $lclass->getLevelArray();
$classes = $lclass->getClassList();

$select_list = "<SELECT name=\"target[]\" MULTIPLE SIZE=\"5\">";
if ($level==1)
{
    for ($i=0; $i<sizeof($levels); $i++)
    {
         list($id,$name) = $levels[$i];

                $selected_tag = (is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>".$name."</OPTION>";
    }
}
else
{
    for ($i=0; $i<sizeof($classes); $i++)
    {
         list($id, $name, $lvl) = $classes[$i];

                $selected_tag = ( is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>". $name."</OPTION>";
    }
}
$select_list .= "</SELECT>";

# Get semester selection list
//$select_sem = getSelectSemester("name='semester'", $semester);

$semester_data = getSemesters();
 $x = "<SELECT name='semester'>\n";
 $x.="<OPTION VALUE=''>$i_Discipline_System_Award_Punishment_Whole_Year</OPTION>\n";
/*
  for ($i=0; $i<sizeof($semester_data); $i++)
 {
      $line = split("::",$semester_data[$i]);
      list ($name,$current) = $line;
      $selected_str = ($semester==$name )? "SELECTED":"";
      $x .= "<OPTION value='$name' $selected_str>$name</OPTION>\n";
 }
*/
 $x .= "</SELECT>\n";
$select_sem = $x;
 
if ($year=="")
{
	$year = getCurrentAcademicYear();
}
/*$sql = "SELECT DISTINCT Year FROM DISCIPLINE_STUDENT_CONDUCT_BALANCE WHERE Year != '' ORDER BY Year";
$years = $ldiscipline->returnVector($sql);
$select_year .= $linterface->CONVERT_TO_JS_ARRAY($years, "jArrayYears", 1);
$select_year .= $linterface->GET_PRESET_LIST("jArrayYears", 0, "year");*/
# Year Menu #


$select_year = "<select name='year' onChange='changeTerm(this.value)'>";
$select_year .= "<option value='0'";
$select_year .= ($year==0) ? " selected" : "";
$select_year .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$select_year .= $ldiscipline->getConductSchoolYear($year);;
$select_year .= "</select>";

function RepQuotes($strx) {
	$patterns = array("/\"/", "/&quot;/");
	$replacements = "/&quot;&quot;/";
	return preg_replace($patterns, $replacements, $strx);
}

########################################################

$TAGS_OBJ[] = array($eDiscipline['ClassSummary']);

# menu highlight setting
$CurrentPage = "ClassSummary";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();


?>

<script language="javascript">
//SelectAll(form1.elements['target[]']);
function changeType(form_obj,lvl_value)
{
         obj = form_obj.elements["target[]"]
         <? # Clear existing options
         ?>
         while (obj.options.length > 0)
         {
                obj.options[0] = null;
         }
         if (lvl_value==1)
         {
             <?
             for ($i=0; $i<sizeof($levels); $i++)
             {
                  list($id,$name) = $levels[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
                  <?

             }
             ?>
         }
         else
         {
             <?
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  list($id,$name, $lvl_id) = $classes[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
                  <?

             }
             ?>
         }
//	SelectAll(form1.elements['target[]']);

}
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
function doPrint()
{
	document.form1.action = "class_summary_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "_self";
}
function doExport()
{
	document.form1.action = "class_summary_export.php";
	document.form1.submit();
	document.form1.action = "";
}
function view()
{
	if (checkform())
	{
		document.form1.action = "";
		document.form1.submit();
	}
}
function checkform()
{
	var obj = document.form1;
	var select_obj = obj.elements['target[]'];

	if (!check_text(document.form1.year, "<?=$i_Discipline_System_alert_SchoolYear?>"))
	{
		return false;
	}

	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}

	alert('<?=$i_Discipline_System_alert_PleaseSelectClassLevel?>');
	return false;
}
</script>
<script language="javascript">
var xmlHttp2

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp2 = GetXmlHttpObject()
	
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp2.onreadystatechange = stateChanged2 
	xmlHttp2.open("GET",url,true)
	xmlHttp2.send(null)
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "get_live2.php";
	url = url + "?target=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<br/>
<form name="form1" action="" method="POST">
<table width="90%" border="0" cellpadding="5" cellspacing="0">
<tr class="tabletext">
	<td width="30%" class="formfieldtitle" valign="top"><?=$i_Profile_Year?> <span class="tabletextrequire">*</span></td>
	<td><!--<input type="text" name="year" maxlength="100" value="<?=$year?>" class="textboxnum">--> <?=$select_year?></td>
</tr>
<tr class="tabletext">
	<td class="formfieldtitle" valign="top"><?=$i_SettingsSemester ?></td>
	<td><div id="spanSemester"><?=$select_sem?></div></td>
</tr>
<tr class="tabletext">
	<td class="formfieldtitle" valign="top"><?="$i_Discipline_Class/$i_Discipline_Form"?></td>
	<td>
		<SELECT name="level" onChange="changeType(this.form,this.value)">
			<OPTION value="0" <?=$level!=1?"SELECTED":""?>><?=$i_Discipline_Class?></OPTION>
			<OPTION value="1" <?=$level==1?"SELECTED":""?>><?=$i_Discipline_Form?></OPTION>
		</select>
	</td>
</tr>
<tr class="tabletext">
	<td class="formfieldtitle" valign="top"><?=($level==1?$i_ClassLevel:$i_ClassName)?> <span class="tabletextrequire">*</span></td>
	<td><?=$select_list?>
		<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.form1.elements['target[]'])"); ?>
	</td>
</tr>
<!--
<tr class="tabletext">
	<td class="formfieldtitle" valign="top"><?=$i_Discipline_System_general_SortField?></td>
	<td>
		<SELECT name="sortfield" onChange="document.form1.submit();">
			<OPTION value="0" <?=($sortfield==0?"SELECTED":"")?> ><?=$i_ClassNameNumber?></OPTION>
		</SELECT>
	</td>
</tr>
-->
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "button", "javascript:view()")?>&nbsp;
		</td>
	</tr>
</table>
<br>
<?


if ($flag==1) {
$isAnnual = $semester=="" ? 1 : 0;
	# Get Student List
	$studentIDs = $ldiscipline->RETRIEVEsTUDENTidSbYcLASSorlEVEL($level, $target);
	
	$studentName = $ldiscipline->retrieveStudentInfoByClassORLevel($level, $target);
	
	# Get Merit Records
	$MeritDetails = $ldiscipline->retrieveStudentMeritTypeRecord($studentIDs, $year, $semester);

	if(!$ldiscipline->Hidden_ConductMark) 
	{
		# Get Conduct Scores and grade rules
		if($year!=0) // when not select 'all school years'
		{
			$ConductScores = $ldiscipline->retrieveStudentConductScore($studentIDs, $year, $semester,$isAnnual);
		}
	
		# Get Adjustment
		$ConductScoresAdjustment = $ldiscipline->retrieveStudentConductScoreAdjustment($studentIDs, $year, $semester,$isAnnual);
		
		# Retrieve Conduct Grade
		$ConductGrades = $ldiscipline->retrieveStudentConductGrade($studentIDs, $year, $semester,$isAnnual);
	}
		
	# If Subscore 1 is used, get Subscore1
	if($ldiscipline->UseSubScore)
	{
		
		$Subscore1Arr = $ldiscipline->retrieveStudentSubscore1($studentIDs, $year, $semester,$isAnnual);
	}

	# Retrieve Count of Late
	$LateCount = $ldiscipline->retrieveStudentCountOfLate($studentIDs, $year, $semester,$isAnnual);
	# Retrieve Count of Absent
	$AbsentCount = $ldiscipline->retrieveStudentCountOfAbsent($studentIDs, $year, $semester,$isAnnual);

	$getGrade = "N/A";
	$typeCount = 0;
	$detail_table =  "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">\n";
	$detail_table .= "<tr class=\"tablebluetop\">";
	
	$detail_table .= "<td class=\"tabletoplink\">".$i_UserStudentName."</td>";
	$export_content .= "&quot;".$i_UserStudentName."&quot;,";
	
	$detail_table .= "<td class=\"tabletoplink\">".$i_SmartCard_ClassName."</td>";
	$export_content .= "&quot;".$i_SmartCard_ClassName."&quot;,";
	
	$detail_table .= "<td class=\"tabletoplink\">".$i_UserClassNumber."</td>";
	$export_content .= "&quot;".$i_UserClassNumber."&quot;,";
	
		if (!$lsp->is_ult_merit_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_UltraCredit."</td>";
		$export_content .= "&quot;".$i_Merit_UltraCredit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_sup_merit_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_SuperCredit."</td>";
		$export_content .= "&quot;".$i_Merit_SuperCredit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_maj_merit_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_MajorCredit."</td>";
		$export_content .= "&quot;".$i_Merit_MajorCredit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_min_merit_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_MinorCredit."</td>";
		$export_content .= "&quot;".$i_Merit_MinorCredit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_merit_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_Merit."</td>";
		$export_content .= "&quot;".$i_Merit_Merit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_ult_demer_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_UltraDemerit."</td>";
		$export_content .= "&quot;".$i_Merit_UltraDemerit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_sup_demer_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_SuperDemerit."</td>";
		$export_content .= "&quot;".$i_Merit_SuperDemerit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_maj_demer_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_MajorDemerit."</td>";
		$export_content .= "&quot;".$i_Merit_MajorDemerit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_min_demer_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_MinorDemerit."</td>";
		$export_content .= "&quot;".$i_Merit_MinorDemerit."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_black_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_BlackMark."</td>";
		$export_content .= "&quot;".$i_Merit_BlackMark."&quot;,";
		$typeCount++;
	}
	if (!$lsp->is_warning_disabled)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Merit_Warning."</td>";
		$export_content .= "&quot;".$i_Merit_Warning."&quot;,";
		$typeCount++;
	}

	$detail_table .= "<td class=\"tabletoplink\">".$i_Profile_Late."</td>";
	$detail_table .= "<td class=\"tabletoplink\">".$i_Profile_Absent."</td>";
	$export_content .= "&quot;".$i_Profile_Late."&quot;,&quot;".$i_Profile_Absent."&quot;";
	
	if(!$ldiscipline->Hidden_ConductMark)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Discipline_System_Conduct_CurrentScore."</td>";
		$detail_table .= "<td class=\"tabletoplink\">".$i_Discipline_System_Report_ClassReport_Grade."</td>";
		
		$export_content .= ",&quot;".$i_Discipline_System_Conduct_CurrentScore."&quot;,&quot;".$i_Discipline_System_Report_ClassReport_Grade."&quot;";
	}
	
	if($ldiscipline->UseSubScore)
	{
		$detail_table .= "<td class=\"tabletoplink\">".$i_Discipline_System_Subscore1_CurrentScore."</td>";
		$export_content .= ",&quot;".$i_Discipline_System_Subscore1_CurrentScore."&quot;";
	}
	$detail_table .= "</tr>";
	$export_content .= "\n";

	$cols = $typeCount+3;
	for($i=0; $i<sizeof($studentIDs); $i++)
	{
		$css = ($i%2?"2":"1");
		$detail_table .= "<tr class=\"tablebluerow$css tabletext\">";
		$detail_table .= "<td>".$studentName[$i]['studentname']."</td>";
		$export_content .= "&quot;".RepQuotes($studentName[$i]['studentname'])."&quot;,";
		
		$detail_table .= "<td>".$studentName[$i]['ClassName']."</td>";
		$export_content .= "&quot;".RepQuotes($studentName[$i]['ClassName'])."&quot;,";
		
		$detail_table .= "<td>".$studentName[$i]['ClassNumber']."</td>";
		$export_content .= "&quot;".RepQuotes($studentName[$i]['ClassNumber'])."&quot;,";
		

		$student_id = $studentIDs[$i];
		$record = $MeritDetails[$student_id];

		if (!$lsp->is_ult_merit_disabled)
		{
			$detail_table .= "<td>".($record['5']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['5']+0))."&quot;,";
		}
		if (!$lsp->is_sup_merit_disabled)
		{
			$detail_table .= "<td>".($record['4']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['4']+0))."&quot;,";
		}
		if (!$lsp->is_maj_merit_disabled)
		{
			$detail_table .= "<td>".($record['3']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['3']+0))."&quot;,";
		}
		if (!$lsp->is_min_merit_disabled)
		{
			$detail_table .= "<td>".($record['2']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['2']+0))."&quot;,";
		}
		if (!$lsp->is_merit_disabled)
		{
			$detail_table .= "<td>".($record['1']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['1']+0))."&quot;,";
		}
		if (!$lsp->is_ult_demer_disabled)
		{
			$detail_table .= "<td>".($record['-5']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['-5']+0))."&quot;,";
		}
		if (!$lsp->is_sup_demer_disabled)
		{
			$detail_table .= "<td>".($record['-4']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['-4']+0))."&quot;,";
		}
		if (!$lsp->is_maj_demer_disabled)
		{
			$detail_table .= "<td>".($record['-3']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['-3']+0))."&quot;,";
		}
		if (!$lsp->is_min_demer_disabled)
		{
			$detail_table .= "<td>".($record['-2']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['-2']+0))."&quot;,";
		}
		if (!$lsp->is_black_disabled)
		{
			$detail_table .= "<td>".($record['-1']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['-1']+0))."&quot;,";
		}
		if (!$lsp->is_warning_disabled)
		{
			$detail_table .= "<td>".($record['0']+0)."</td>";
			$export_content .= "&quot;".RepQuotes(($record['0']+0))."&quot;,";
		}

		$detail_table .= "<td>".($LateCount[$student_id]==""?0:$LateCount[$student_id])."</td>";
		$detail_table .= "<td>".($AbsentCount[$student_id]==""?0:$AbsentCount[$student_id])."</td>";
		$export_content .= ($LateCount[$student_id] == "") ? "&quot;0&quot;," : "&quot;".RepQuotes($LateCount[$student_id])."&quot;,";
		$export_content .= ($AbsentCount[$student_id] == "") ? "&quot;0&quot;," : "&quot;".RepQuotes($AbsentCount[$student_id])."&quot;,";

		if(!$ldiscipline->Hidden_ConductMark)
		{
			if($ConductScores[$student_id]=="" || $ConductScores[$student_id]==NULL)
			{
				if($year==0)
				{
					$detail_table .= "<td>".$getGrade."</td>";
				}
				else
				{
					$i_conduct_score = $ldiscipline->getConductMarkRule('baseMark');
					$currConductScore = $i_conduct_score;
					$detail_table .= "<td>".$currConductScore."</td>";
				}
			}
			else
			{
				if($year==0)
				{
					$detail_table .= "<td>".$getGrade."</td>";	
				}
				else
				{
					$currConductScore = $ConductScores[$student_id] + $ConductScoresAdjustment[$student_id];
					$detail_table .= "<td>".$currConductScore."</td>";
				}
			}
	
			if($year==0)
			{
				$export_content .= "&quot;".RepQuotes($getGrade)."&quot;,";
			}
			else
			{
				$export_content .= "&quot;".RepQuotes($currConductScore)."&quot;,";
			}
			if($year==0)
			{
				$detail_table .= "<td>".$getGrade."</td>";
				$export_content .= "&quot;".RepQuotes($getGrade)."&quot;,";
			}
			else
			{
				$detail_table .= "<td>".(($ConductGrades[$student_id]=="" || $ConductGrades[$student_id]==NULL)?$getGrade:$ConductGrades[$student_id])."</td>";
				$export_content .= (($ConductGrades[$student_id]=="" || $ConductGrades[$student_id]==NULL) ? "&quot;".RepQuotes($getGrade)."&quot;" : "&quot;".RepQuotes($ConductGrades[$student_id])."&quot;");
			}
		}

		if($ldiscipline->UseSubScore)
		{
			if($Subscore1Arr[$student_id]=="" || $Subscore1Arr[$student_id]==NULL)
			{
				$i_subscore = $ldiscipline->conduct_initial_score;
				$sql = "INSERT INTO DISCIPLINE_STUDENT_CONDUCT_BALANCE (StudentID, ConductScore) VALUES ($student_id, $i_subscore)";
				$ldiscipline->db_db_query($sql);
				$currSubscore = $i_subscore;
				$detail_table .= "<td>".$currSubscore."</td>";
			}
			else
			{
				$currSubscore = $Subscore1Arr[$student_id];
				$detail_table .= "<td>".$currSubscore."</td>";
			}
			$export_content .= ",&quot;".RepQuotes($currSubscore)."&quot;";
		}

		$export_content .= "\n";
		$detail_table .= "</tr>\n";
	}

	$detail_table .= "</table>";
	$detail_table .= "<br>";
}
echo "<table width=\"96%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>".$detail_table."</td></tr></table>";
?>
<input type="hidden" name="flag" value="1" />
<?
	if ($flag == 1)
	{
		?>
		<input type="hidden" name="content" value='<?=urlencode($detail_table)?>' />
		<input type="hidden" name="exportContent" value='<?=$export_content?>' />

		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">

			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()");?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint()");?>
				</td>
			</tr>
		</table>

		</p>
		<?
	}
?>
</form>
<script language="javascript">
<?
if($flag==1) {
	echo "changeTerm('$year');";
} else {
	echo "changeTerm(document.getElementById('year').value);";
}
?>
</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
