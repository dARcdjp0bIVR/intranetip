<?php
# using: henry

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/fileheader.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lsp = new libstudentprofile();

$linterface = new interface_html();


# Retrieve Student Name
$lu_student = new libuser($targetID);
$student_name = $lu_student->UserName();
$class_name = $lu_student->ClassName;
$class_num = $lu_student->ClassNumber;

$content = "";

$year = $ldiscipline->getAcademicYearNameByYearID($yearID);
# Check display Reamrk field or not (customization for fyk 20080424)
$show_remark_in_personal_report = $sys_custom['Discipline_ShowRemarkInPersonalReport'];

switch($detailType)
{
        case 1:
             $string_type["0"] = $i_Merit_Warning;
             $string_type["-1"] = $i_Merit_BlackMark;
             $string_type["-2"] = $i_Merit_MinorDemerit;
             $string_type["-3"] = $i_Merit_MajorDemerit;
             $string_type["-4"] = $i_Merit_SuperDemerit;
             $string_type["-5"] = $i_Merit_UltraDemerit;
             $report_name = $eDiscipline["PunishmentRecord"];

             # Demerit
             if($selectSemester == 0){
             	$selectSemester = "";
             }
             $result = $ldiscipline->retrieveStudentDemeritRecord($targetID, $startdate, $enddate, $yearID, $selectSemester,$show_waive_status);
             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             $detail_table .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\">#</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_general_date_demerit</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_Add_Demerit</td>";
			if(!$ldiscipline->Hidden_ConductMark)
			{
				$detail_table .= "<td class=\"tabletoplink\">$i_Discipline_System_Conduct_ScoreDifference</td>";
			}
            
            $detail_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tabletoplink\">$i_Discipline_System_Subscore1_ScoreDifference</td>" : "";
            $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tabletoplink\">$i_Subject_name</td>" : "";
            $detail_table .= "<td class=\"tabletoplink\">$i_Profile_PersonInCharge</td>";
            $detail_table .=$show_waive_status? "<td class=\"tabletoplink\">$i_Discipline_System_WaiveStatus</td>":"";

            $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletoplink\">$i_Discipline_System_general_remark</td>":"";
            
            $detail_table.="  </tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, 
                        $r_picid, $r_pic_name,$record_status, $remark, $fromConductRecord) = $result[$i];
					
                        if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
                  $css = ($i%2?"2":"1");
                  $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
                  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                  $detail_table .= "<tr class=\"tablebluerow$css\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>". intranet_htmlspecialchars($r_item) ."</td>
                                     <td>$record_data</td>";
				if(!$ldiscipline->Hidden_ConductMark) 	
					$detail_table .= "<td>$r_conduct</td>";
				$detail_table .= ($ldiscipline->UseSubScore) ? "<td>$t_subscore1</td>"  : "";
                $detail_table .= ($ldiscipline->use_subject) ? "<td>$t_subject</td>" : "";
                  $detail_table .= "<td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
                  if($show_waive_status==1)
                  	$detail_table .="<Td>$str_waive_status</td>";
                  if($show_remark_in_personal_report) {
                  		$detail_table .="<Td>". nl2br($remark) ;
						if($fromConductRecord) {
							$detail_table .= $ldiscipline->displayGMDetailByAPRecordID($r_id);
						}
						$detail_table .="</td>";
					}
                  $detail_table.=" </tr> ";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";

             break;
        case 2:
             $string_type["1"] = $i_Merit_Merit;
             $string_type["2"] = $i_Merit_MinorCredit;
             $string_type["3"] = $i_Merit_MajorCredit;
             $string_type["4"] = $i_Merit_SuperCredit;
             $string_type["5"] = $i_Merit_UltraCredit;
             $report_name = $eDiscipline["AwardRecord"];

             # Merit
             if($selectSemester == 0){
             	$selectSemester = "";
             }
             $result = $ldiscipline->retrieveStudentMeritRecord($targetID, $startdate, $enddate,$yearID,$selectSemester);
             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             $detail_table .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\">#</td>
                                                                <td class=\"tabletoplink\">$i_Discipline_System_general_date_merit</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_Add_Merit</td>";
                                 
			if(!$ldiscipline->Hidden_ConductMark)
             	$detail_table .= " <td class=\"tabletoplink\">$i_Discipline_System_Conduct_ScoreDifference</td>";
             $detail_table .= ($ldiscipline->UseSubScore) ? "
                                             <td class=\"tabletoplink\">$i_Discipline_System_Subscore1_ScoreDifference</td>
                                                                 " : "";
             # <td class=tabletoplink>$i_Discipline_System_Subscore1_UpdatedScore</td>
            $detail_table .= "<td class=\"tabletoplink\">$i_Profile_PersonInCharge</td>";
			$detail_table .= $show_remark_in_personal_report? "<td class=\"tabletoplink\">$i_Discipline_System_general_remark</td>":"";
			
			$detail_table .= "</tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $r_picid, $r_pic_name, $remark, $status, $fromConductRecord) = $result[$i];
						
                  if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
                        $css = ($i%2?"2":"1");
                  $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
                  $detail_table .= "<tr class=\"tablebluerow$css\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>". intranet_htmlspecialchars($r_item) ."</td>
                                     <td>$record_data</td>";
				if(!$ldiscipline->Hidden_ConductMark)
					$detail_table .= "<td>$r_conduct</td>";
                  $detail_table .= ($ldiscipline->UseSubScore) ? "<td>$t_subscore1</td>" : "";
                  # <td>$t_subscore1_after</td>
                  $detail_table .= "<td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
					if($show_remark_in_personal_report) {
                  		$detail_table .="<Td>".nl2br($remark);
						if($fromConductRecord) {
							$detail_table .= $ldiscipline->displayGMDetailByAPRecordID($r_id);
						}						
						$detail_table .="</td>";
					}
				$detail_table .= "</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";
             break;
                case 4:
                         $string_type["0"] = $i_Merit_Warning;
			             $string_type["-1"] = $i_Merit_BlackMark;
			             $string_type["-2"] = $i_Merit_MinorDemerit;
			             $string_type["-3"] = $i_Merit_MajorDemerit;
			             $string_type["-4"] = $i_Merit_SuperDemerit;
			             $string_type["-5"] = $i_Merit_UltraDemerit;
			             $string_type["1"] = $i_Merit_Merit;
			             $string_type["2"] = $i_Merit_MinorCredit;
			             $string_type["3"] = $i_Merit_MajorCredit;
			             $string_type["4"] = $i_Merit_SuperCredit;
			             $string_type["5"] = $i_Merit_UltraCredit;
			             $report_name = $eDiscipline["Personal_Report_AwardPunishmentRecord"];

             # Merit / Demerit
             if($selectSemester == 0){
             	$selectSemester = "";
             }
             $result = $ldiscipline->retrieveStudentProfileRecord($targetID, $startdate, $enddate,$yearID,$selectSemester,$show_waive_status);

             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             $detail_table .= "<tr class=\"tablebluetop\"><td class=\"tabletoplink\">#</td>
             					<td class=\"tabletoplink\">$i_general_record_date</td>";
             if($sys_custom['Discipline_ShowCategoryInPersonalReport'])                                 
				$detail_table .= "<td class=\"tabletoplink\">$i_Discipline_System_CategoryName</td>";
             $detail_table .= "<td class=\"tabletoplink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_Add_Merit_Demerit</td>";
                                 
			if(!$ldiscipline->Hidden_ConductMark)
				$detail_table .= "<td class=\"tabletoplink\">$i_Discipline_System_Conduct_ScoreDifference</td>";
             
             $detail_table .= ($ldiscipline->UseSubScore) ? "
                                 <td class=\"tabletoplink\">$i_Discipline_System_Subscore1_ScoreDifference</td>
                                                                " : "";
             # <td class=tabletoplink>$i_Discipline_System_Subscore1_UpdatedScore</td>
             $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tabletoplink\">$i_Subject_name</td>" : "";
            $detail_table .= "<td class=\"tabletoplink\">$i_Profile_PersonInCharge</td>";
                        $detail_table .=$show_waive_status? "<td class=\"tabletoplink\">$i_Discipline_System_WaiveStatus</td>":"";
                        
			$detail_table .= $show_remark_in_personal_report? "<td class=\"tabletoplink\">$i_Discipline_System_general_remark</td>":"";

            $detail_table.="</tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, $r_picid, $r_pic_name,$record_status, $MeritType, $remark, $cat_name, $fromConductRecord) = $result[$i];
                  $cat_name = intranet_htmlspecialchars($cat_name);
                  $r_item = intranet_htmlspecialchars($r_item);
                  $remark = intranet_htmlspecialchars($remark);
                  
                  if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
                  
                  $css = ($i%2?"2":"1");
                  
                  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                  /*
                                   switch($waive_status){
	                  case "--": $str_waive_status = $waive_status; break;
	                  case 0 : $str_waive_status = $i_Discipline_System_WaiveStatus_NoWaived;break;
	                  case 1 : $str_waive_status = $i_Discipline_System_WaiveStatus_InProgress;break;
	                  case 2 : $str_waive_status = $i_Discipline_System_WaiveStatus_Success; break;
	                  case 3 : $str_waive_status = $i_Discipline_System_WaiveStatus_Fail; break;
	                  default : $str_waive_status = $i_Discipline_System_WaiveStatus_NoWaived;
	              }
	              */
                  
                  $detail_table .= "<tr class=\"tablebluerow$css\"><td>".($i+1)."</td>
                                     <td>$r_date</td>";
                                     
				$record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);                                 
                  if($sys_custom['Discipline_ShowCategoryInPersonalReport'])
						$detail_table .= "<td>$cat_name</td>";                   
                  $detail_table .= "<td>$r_item</td>
                                     <td>$record_data</td>";
				if(!$ldiscipline->Hidden_ConductMark)
                 $detail_table .= "<td>$r_conduct</td>";
                  
                  $detail_table .= ($ldiscipline->UseSubScore) ? "<td>$t_subscore1</td>" : "";
                  $detail_table .= ($ldiscipline->use_subject) ? "<td>$t_subject</td>" : "";
                  $detail_table .= "<td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
                                  if($show_waive_status==1)
                  	$detail_table .="<Td>$str_waive_status</td>";
                  	if($show_remark_in_personal_report) {
                  		$detail_table .="<td>". nl2br($remark);
						if($fromConductRecord) {
							$detail_table .= $ldiscipline->displayGMDetailByAPRecordID($r_id);
						}
						$detail_table.="</td>";
					}
                               $detail_table.="</tr>  ";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";
             break;
                             
             case 6: 
             		$report_name = $i_Discipline_System_Conduct_Change_Log;
             		if($selectSemester == 0){
						$selectSemester = "";
					}
             		$result = $ldiscipline->retrieveConductScoreChangeLogRecord($targetID,$startdate,$enddate,$yearID,$selectSemester);
  		            $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             		$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletoplink\">#</td>
                                 <td class=\"tabletoplink\">$i_general_record_date</td>
                                 <td class=\"tabletoplink\">$i_Merit_Type</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_FromScore</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_ToScore</td>
            					 <td class=\"tabletoplink\">$i_Profile_PersonInCharge</td>
                                 ";
					$detail_table .= $show_remark_in_personal_report? "<td class=\"tabletoplink\">$i_Discipline_System_general_remark</td>":"";
					$detail_table .= "</tr>";
					
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
                  $css = ($i%2?"2":"1");

                  //$r_tool_link = $linterface->GET_BTN($button_edit, "button", 'self.location="personal_check_merit_edit.php?RecordID=$r_id&targetType=$targetType&targetID=$targetID&detailType=$detailType"');
                  //$r_tool_link = $linterface->GET_BTN($button_remove, "button", "javascript:confirmRemoveRecord($detailType, $r_id, $targetID)");

                  switch($r_action_type){
	                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
	                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
	                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
	                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
	                  case 5 : $action = $i_Discipline_System_System_Revised; break;
	              }
	              
	              if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
	              if($r_item =="--" && $r_action_type!=5){
		              $r_item = "<i>$i_Discipline_System_Record_Already_Removed</i>";
		          }
				  else
					$r_item = intranet_htmlspecialchars($r_item);
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>$action</td>
                                     <td>". $r_item ."</td>
                                     <td>$r_fromscore</td>
                                     <td>$r_toscore</td>
                  					 <td>".($r_pic_name==""?"--":$r_pic_name)."</td>
                                     ";
					if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";
                  	$detail_table .="</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";

             		break;
             case 7: 
             		$report_name = $i_Discipline_System_SubScore1_Change_Log;
             		if($selectSemester == 0){
						$selectSemester = "";
					}
                    $result = $ldiscipline->retrieveSubscore1ChangeLogRecord($targetID,$startdate,$enddate,$yearID,$selectSemester);
  		            $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             		$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletoplink\">#</td>
                                 <td class=\"tabletoplink\">$i_general_record_date</td>
                                 <td class=\"tabletoplink\">$i_Merit_Type</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_FromScore</td>
                                 <td class=\"tabletoplink\">$i_Discipline_System_ToScore</td>
            					 <td class=\"tabletoplink\">$i_Profile_PersonInCharge</td>
                                 ";
                 $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletoplink\">$i_Discipline_System_general_remark</td>":"";
                    $detail_table .= "</tr>";
                    
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
                  $css = ($i%2?"2":"1");

                  //$r_tool_link = $linterface->GET_BTN($button_edit, "button", 'self.location="personal_check_merit_edit.php?RecordID=$r_id&targetType=$targetType&targetID=$targetID&detailType=$detailType"');
                  //$r_tool_link = $linterface->GET_BTN($button_remove, "button", "javascript:confirmRemoveRecord($detailType, $r_id, $targetID)");

                  switch($r_action_type){
	                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
	                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
	                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
	                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
	              }
	              
	              if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}

	              if($r_item =="--"){
		              $r_item = "<i>$i_Discipline_System_Record_Already_Removed</i>";
		          }
				  else
				  {
					$r_item = intranet_htmlspecialchars($r_item);
				  }
				  
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>$action</td>
                                     <td>". $r_item ."</td>
                                     <td>$r_fromscore</td>
                                     <td>$r_toscore</td>
                  					 <td>".($r_pic_name==""?"--":$r_pic_name)."</td>
                                     ";
                 if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";
                $detail_table .= "</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";
             
             
             	break;
        case 8:
        		$report_name = $eDiscipline['Personal_Report_Misconduct_Record'];
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletoplink\" width=\"5\">#</td>
				                     <td class=\"tabletoplink\" width=\"80\">$i_Discipline_System_general_date_demerit</td>
				                     <td class=\"tabletoplink\" width=\"80\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"80\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"150\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletoplink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletoplink\">$i_Discipline_System_WaiveStatus</td>":"";
				 
				 $detail_table .="<td>&nbsp;</td></tr>";
				
				if($selectSemester == 0){
					$selectSemester = "";
				}
				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester, "-1", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				  $css = ($i%2?"2":"1");
						              
				      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
				                         <td>$record_date</td>
				                         <td>$cat_name</td>
				                         <Td>$item_name</td>
				                         <Td>$r_pic_name</td>
				                         <Td>". intranet_htmlspecialchars($remark) ."</td>
				                        ";
				      if($show_waive_status==1)
				      	$detail_table .="<Td nowrap>$str_waive_status</td>";
				      
				      $detail_table.="</tr>";
				
				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";

             break;  
		case 9:
        		$report_name = $eDiscipline['Personal_Report_Goodconduct_Record'];
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletoplink\" width=\"5\">#</td>
				                     <td class=\"tabletoplink\" width=\"80\">$i_Discipline_System_general_date_demerit</td>
				                     <td class=\"tabletoplink\" width=\"80\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"80\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"150\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletoplink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletoplink\">$i_Discipline_System_WaiveStatus</td>":"";
				 
				 $detail_table .="<td>&nbsp;</td></tr>";
				
				if($selectSemester == 0){
					$selectSemester = "";
				}
				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester, "1", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				  $css = ($i%2?"2":"1");
						              
				      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
				                         <td>$record_date</td>
				                         <td>$cat_name</td>
				                         <Td>$item_name</td>
				                         <Td>$r_pic_name</td>
				                         <Td>". intranet_htmlspecialchars($remark) ."</td>
				                        ";
				      if($show_waive_status==1)
				      	$detail_table .="<Td nowrap>$str_waive_status</td>";
				      
				      $detail_table.="</tr>";
				
				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";

             break; 
             case 10:
        		$report_name = $eDiscipline['Personal_Report_Good_Misconduct_Record'];
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletoplink\" width=\"5\">#</td>
				                     <td class=\"tabletoplink\" width=\"80\">$i_Discipline_System_general_date_demerit</td>
				                     <td class=\"tabletoplink\" width=\"80\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"80\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>
				                     <td class=\"tabletoplink\" width=\"150\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletoplink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletoplink\">$i_Discipline_System_WaiveStatus</td>":"";
				 
				 $detail_table .="<td>&nbsp;</td></tr>";
				
				if($selectSemester == 0){
					$selectSemester = "";
				}
				$result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester, "", $show_waive_status);
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				  $css = ($i%2?"2":"1");
						              
				      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
				                         <td>$record_date</td>
				                         <td>$cat_name</td>
				                         <Td>$item_name</td>
				                         <Td>$r_pic_name</td>
				                         <Td>". intranet_htmlspecialchars($remark) ."</td>
				                        ";
				      if($show_waive_status==1)
				      	$detail_table .="<Td nowrap>$str_waive_status</td>";
				      
				      $detail_table.="</tr>";
				
				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";

             break; 
             	             
}

$year_info = "<tr><td align=right nowrap>$i_Profile_Year:</td><td>$year</td></tr>";
if(($selectSemester=="")||($selectSemester==0))
	$str_semester = $i_Discipline_System_Award_Punishment_Whole_Year;
else $str_semester = $ldiscipline->getTermNameByTermID($selectSemester);
$semester_info = "<tr><td align=right nowrap>$i_SettingsSemester:</td><td>$str_semester</td></tr>";

$functionbar  = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
$functionbar .= "<tr><td colspan='2' nowrap>$student_name ($class_name - $class_num)</td></tr>";
$functionbar .= "<tr><Td align=right width=10% nowrap>$i_general_startdate:</td><td>$startdate</td></tr>";
$functionbar .= "<tr><td align=right width=10% nowrap>$i_general_enddate:</td><td>$enddate</td></tr>";
$functionbar .= $year_info;
$functionbar .= $semester_info;
$functionbar .= "</table>\n";

//echo $header.$content;
?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>

<form name="form1" method="get">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
<tr><td><font size='+0'><u><?=$i_Discipline_System_Report_Personal_PersonalRecord."(".$report_name.")" ?></u></font></td></tr>
<tr><td><?= $functionbar ?></td></tr>
<tr><td class=tableContent><br><br>
<?=$detail_table?>
</td></tr>
</table>

<br>
<table border="0" width="100%">
<tr><td align="center">
<?= $linterface->GET_ACTION_BTN($button_print, "submit", "window.print(); return false;"); ?>
</td></tr>
</table>

</form>

<?php
include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();

?>
