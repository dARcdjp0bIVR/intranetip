<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


$linterface = new interface_html();

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);
if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"name=\"targetID\" onChange=\"this.form.targetType.value=1\" ");

# tag information
$CurrentPage = "Reports_PersonalReport";

$TAGS_OBJ[] = array($iDiscipline['Reports_Child_PersonalReport']);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>
<SCRIPT LANGUAGE=Javascript>

function checkForm()
{
	obj = document.form1;

	if(obj.targetClass.value == "" && obj.targetLogin.value == "")
	{
		alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
		return false;
	}
	else if(obj.targetClass.value != "" && obj.targetLogin.value == "")
	{
		if(obj.targetID.value != "")
		{
			obj.action = "personal_check.php";
			return true;
		}
		else
		{
			alert('<?=$i_Discipline_System_alert_PleaseSelectStudent?>');
			return false;
		}
	}
	
	obj.action = "personal_check.php";
	return true;
}
</SCRIPT>


<br />
<form name="form1" method"=POST" onsubmit="return checkForm();">
<table border="0" cellpadding="5" cellspacing="0" width="90%">
  <tr>
    <td class="tabletext formfieldtitle" valign="top" width="30%"><?=$i_ClassNameNumber?> <span class="tabletextrequire">*</span></td>
    <td><?=$select_class?><?=$select_students?></td>
  </tr>

	<tr><td>&nbsp;</td><td class="tablerow2"><span class="tabletextremark">(<?=$i_general_alternative?>)</span>
		<table cellpadding="0" cellspacing="0" border="0">
		<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext">
			<?= $i_UserLogin; ?><br />
			<!--<input onFocus="this.form.flag.value=1" type="text" name="student_login" maxlength="100" class="textboxnum" />-->
			<input type="text" name="targetLogin" class="textboxnum" onFocus="this.form.targetType.value=2">
		</td></tr>
		</table>
		</td>
	</tr>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($button_view, "submit"); ?>
		</td>
	</tr>
</table>

<input type="hidden" name="targetType" value="2" />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>