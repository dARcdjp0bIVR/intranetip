<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lsp = new libstudentprofile();
$lclass = new libclass();

if(!is_array($target)) {
	header("Location: index.php");	
}

$rankTargetMenu = "<SELECT name=\"level\" onChange=\"changeClassForm(document.getElementById('SchoolYear').value)\">";
$rankTargetMenu .= "	<OPTION value=\"0\"";
$rankTargetMenu .= ($level==0) ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Class</OPTION>";
$rankTargetMenu .= "	<OPTION value=\"1\"";
$rankTargetMenu .= ($level==1) ? " selected" : "";
$rankTargetMenu .= ">$i_Discipline_Form</OPTION>";
$rankTargetMenu .= "</select>";


# Get Levels and Classes
$levels = $lclass->getLevelArray();
//$classes = $ldiscipline->getRankClassList("",Get_Current_Academic_Year_ID());
$classes = $ldiscipline->getRankClassList("",$SchoolYear);

$classSelected = array();

$select_list = "<SELECT name=\"target[]\" id=\"target[]\" MULTIPLE SIZE=\"5\">";
if ($level==1)		# "form"
{
    for ($i=0; $i<sizeof($levels); $i++)
    {
         list($id,$name) = $levels[$i];

                $selected_tag = (is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>".$name."</OPTION>";
    }
    $classSelected = $ldiscipline->getClassIDByLevelID($target, $SchoolYear);
}
else				# "class"
{
    for ($i=0; $i<sizeof($classes); $i++)
    {
         //list($id, $name, $lvl) = $classes[$i];
         list($name, $id) = $classes[$i];

                $selected_tag = ( is_array($target) && in_array($id,$target) )?"SELECTED":"";
                $select_list .= "<OPTION value='".$id."' $selected_tag>". $name."</OPTION>";
    }
    $classSelected = $target;
}
$select_list .= "</SELECT>";

//$conds .= (sizeof($classDisplay)>0) ? " AND yc.YearClassID IN (".implode(',',$classDisplay).")" : "";

$dataAry = array();

# School Year Menu #
$SchoolYear = $SchoolYear =="" ? Get_Current_Academic_Year_ID() : $SchoolYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYear = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear' id='SchoolYear' onFocus='document.form1.dateChoice[0].checked=true' onChange='changeTerm(this.value);changeClassForm(this.value)'", "", $SchoolYear);

$conds .= " AND MR.RecordStatus=".DISCIPLINE_STATUS_APPROVED;


$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($semester);

# Semester Menu #
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester1 != 'WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	
	$conds .= " AND (MR.RecordDate BETWEEN '$startDate' AND '$endDate')";
	$yearInfo = getAcademicYearInfoAndTermInfoByDate($startDate);
	$dataAry['Year'] = $yearInfo[0];
} else {
	$date = $ldiscipline->getAcademicYearNameByYearID($SchoolYear)." ".$semesterText;
	
	$conds .= " AND yc.AcademicYearID=$SchoolYear AND MR.AcademicYearID=$SchoolYear";
	$conds .= ($semester != 'WholeYear' && $semester!=0) ? " AND MR.YearTermID=$semester" : "";	
	$dataAry['Year'] = $SchoolYear;
}

$dataAry['target'] = $classSelected;
$dataAry['top'] = $top;
	
$interval = 5;
$topBoundary = 50;
$meritTypeInUse = 0;
$demeritTypeInUse = 0;

$tempConds = "";
$merit = array();
$demerit = array();

$topSelectMenu = "<select name='top' id='top'>";
for($i=1;$i<=$topBoundary;$i++) {
	if($i%5==0) {
		$topSelectMenu .= "<option value='$i'";
		$topSelectMenu .= ($i==$top) ? " selected" : "";
		$topSelectMenu .= ">$i</option>";
	}
}
$topSelectMenu .= "</select>";

$meritScore = "<table width='100%' cellpadding='0' cellspacing='0' border='0'>";
$meritScore .= "	<tr>";
$meritScore .= "		<td width='50%' valign='top'>";
$meritScore .= "			<table width='90%' ceppadding='2' cellspacing='0' border='0'>";
if (!$lsp->is_merit_disabled) {
	$merit['1'] = $merit1;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=1" : " MR.ProfileMeritType=1";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_Merit</td><td width='40%'><input type='text' name='merit1' id='merit1' value='".$merit1."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_min_merit_disabled) {
	$merit['2'] = $merit2;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=2" : " MR.ProfileMeritType=2";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MinorCredit</td><td width='40%'><input type='text' name='merit2' id='merit2' value='".$merit2."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_maj_merit_disabled) {
	$merit['3'] = $merit3;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=3" : " MR.ProfileMeritType=3";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MajorCredit</td><td width='40%'><input type='text' name='merit3' id='merit3' value='".$merit3."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_sup_merit_disabled) {
	$merit['4'] = $merit4;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=4" : " MR.ProfileMeritType=4";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_SuperCredit</td><td width='40%'><input type='text' name='merit4' id='merit4' value='".$merit4."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_ult_merit_disabled) {
	$merit['5'] = $merit5;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=5" : " MR.ProfileMeritType=4";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_UltraCredit</td><td width='40%'><input type='text' name='merit5' id='merit5' value='".$merit5."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if(sizeof($merit)==0) {
	$meritScore .= "			<tr><td width='100%'>".$Lang['eDiscipline']['NoMeritTypeInUse']."</td></tr>";	
}
$meritScore .= "			</table>";
$meritScore .= "		<td>";
$meritScore .= "		<td width='50%' valign='top'>";
$meritScore .= "			<table width='90%' ceppadding='2' cellspacing='0' border='0'>";
if (!$lsp->is_warning_disabled) {
	$demerit['0'] = $demerit0;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=0" : " MR.ProfileMeritType=0";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_Warning</td><td width='40%'><input type='text' name='demerit0' id='demerit0' value='".$demerit0."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_black_disabled) {
	$demerit['-1'] = $demerit1;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=-1" : " MR.ProfileMeritType=-1";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_BlackMark</td><td width='40%'><input type='text' name='demerit1' id='demerit1' value='".$demerit1."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_min_demer_disabled) {
	$demerit['-2'] = $demerit2;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=-2" : " MR.ProfileMeritType=-2";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MinorDemerit</td><td width='40%'><input type='text' name='demerit2' id='demerit2' value='".$demerit2."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_maj_demer_disabled) {
	$demerit['-3'] = $demerit3;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=-3" : " MR.ProfileMeritType=-3";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_MajorDemerit</td><td width='40%'><input type='text' name='demerit3' id='demerit3' value='".$demerit3."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_sup_demer_disabled) {
	$demerit['-4'] = $demerit4;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=-4" : " MR.ProfileMeritType=-4";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_SuperDemerit</td><td width='40%'><input type='text' name='demerit4' id='demerit4' value='".$demerit4."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if (!$lsp->is_ult_demer_disabled) {
	$demeritTypeInUse += 1;
	$demerit['-5'] = $demerit5;
	$tempConds .= ($tempConds!="") ? " OR MR.ProfileMeritType=-5" : " MR.ProfileMeritType=-5";
	$meritScore .= "			<tr><td width='60%'>$i_Merit_UltraDemerit</td><td width='40%'><input type='text' name='demerit5' id='demerit5' value='".$demerit5."' size='4' maxlength='5' onBlur=\"if(this.value=='') this.value=0\"></td></tr>";	
}
if(sizeof($demerit)==0) {
	$meritScore .= "			<tr><td width='100%'>".$Lang['eDiscipline']['NoDemeritTypeInUse']."</td></tr>";	
}
$meritScore .= "			</table>";
$meritScore .= "		</td>";
$meritScore .= "	</tr>";
$meritScore .= "</table>";

if($tempConds != "") {
	$conds .= " AND ($tempConds)";	
}

# Title of the report
if($intranet_session_language=="en") {
	$title = $date;
	
	$export_title .= $date."\n";
} else {
	$title = $date;
	
	$export_title .= $date."\n";
}

$export_content = "";

# table Top
$tableTop .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' bgcolor='#CCCCCC'>";
$tableTop .= "	<tr class='tablebluetop'>";
$tableTop .= "		<td class='tabletop'>$i_ClassName</td>";
if (!$lsp->is_merit_disabled && $merit1!=0) 
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_Merit</td>";
if (!$lsp->is_min_merit_disabled && $merit2!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_MinorCredit</td>";
if (!$lsp->is_maj_merit_disabled && $merit3!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_MajorCredit</td>";
if (!$lsp->is_sup_merit_disabled && $merit4!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_SuperCredit</td>";
if (!$lsp->is_ult_merit_disabled && $merit5!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_UltraCredit</td>";
if (!$lsp->is_warning_disabled && $demerit0!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_Warning</td>";
if (!$lsp->is_black_disabled && $demerit1!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_BlackMark</td>";
if (!$lsp->is_min_demer_disabled && $demerit2!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_MinorDemerit</td>";
if (!$lsp->is_maj_demer_disabled && $demerit3!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_MajorDemerit</td>";
if (!$lsp->is_sup_demer_disabled && $demerit4!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_SuperDemerit</td>";
if (!$lsp->is_ult_demer_disabled && $demerit5!=0)
	$tableTop .= "	<td class='tabletopnolink' align='center'>$i_Merit_UltraDemerit</td>";
$tableTop .= "		<td class='tabletop' align='center'>$i_Sports_field_Total_Score</td>";
$tableTop .= "		<td class='tabletop' align='center'>{$Lang['eDiscipline']['totalStudents']}</td>";
$tableTop .= "		<td class='tabletopnolink' align='center'>{$Lang['eDiscipline']['AverageScore']}</td>";
$tableTop .= "	</tr>";

$export_top .= "&quot;".$i_ClassName."&quot;\t";
if (!$lsp->is_merit_disabled && $merit1!=0)
	$export_top .= "&quot;".$i_Merit_Merit."&quot;\t";
if (!$lsp->is_min_merit_disabled && $merit2!=0)
	$export_top .= "&quot;".$i_Merit_MinorCredit."&quot;\t";
if (!$lsp->is_maj_merit_disabled && $merit3!=0)
	$export_top .= "&quot;".$i_Merit_MajorCredit."&quot;\t";
if (!$lsp->is_sup_merit_disabled && $merit4!=0)
	$export_top .= "&quot;".$i_Merit_SuperCredit."&quot;\t";
if (!$lsp->is_ult_merit_disabled && $merit5!=0)
	$export_top .= "&quot;".$i_Merit_UltraCredit."&quot;\t";
if (!$lsp->is_warning_disabled && $demerit0!=0)
	$export_top .= "&quot;".$i_Merit_Warning."&quot;\t";
if (!$lsp->is_black_disabled && $demerit1!=0)
	$export_top .= "&quot;".$i_Merit_BlackMark."&quot;\t";
if (!$lsp->is_min_demer_disabled && $demerit2!=0)
	$export_top .= "&quot;".$i_Merit_MinorDemerit."&quot;\t";
if (!$lsp->is_maj_demer_disabled && $demerit3!=0)
	$export_top .= "&quot;".$i_Merit_MajorDemerit."&quot;\t";
if (!$lsp->is_sup_demer_disabled && $demerit4!=0)
	$export_top .= "&quot;".$i_Merit_SuperDemerit."&quot;\t";
if (!$lsp->is_ult_demer_disabled && $demerit5!=0)
	$export_top .= "&quot;".$i_Merit_UltraDemerit."&quot;\t";
	
$export_top .= "&quot;".$i_Sports_field_Total_Score."&quot;\t";
$export_top .= "&quot;".$Lang['eDiscipline']['totalStudents']."&quot;\t";
$export_top .= "&quot;".$Lang['eDiscipline']['AverageScore']."&quot;";


# table bottom
$tableBottom .= "</table>";


$result = $ldiscipline->getTermAwardScheme($conds, $dataAry, $merit, $demerit);

$tableContent = "";

# table content 
if(sizeof($result)==0) {	# table no record

	$colspan = 1 + sizeof($merit) + sizeof($demerit) + 2;	

	$tableContent .= $tableTop;
	$tableContent .= "<tr class='tablebluerow1'>";
	$tableContent .= "<td class='tabletext' colspan='$colspan' height='40' align='center'>{$i_no_record_exists_msg}</td>";
	$tableContent .= "</tr>";
	$tableContent .= $tableBottom;
	
	$export_content .= $export_title;
	$export_content .= "\n\n";
	$export_content .= $export_top;
	$export_content .= "\n";
	$export_content .= "&quot;".$i_no_record_exists_msg."&quot;";
	
} else {					# with data

	$tableContent .= $tableTop;
	
	$export_content .= $export_title;
	$export_content .= "\n\n";
	$export_content .= $export_top;
	$export_content .= "\n";
	
	$associatedScore = array();
	
	foreach($result as $key=>$ary) {
		foreach($ary as $key2 => $val) {
			$associatedScore[$key] += ($key2>0) ? $val * $merit[$key2] : $val * $demerit[$key2];
			$tempID[0] = $key;
			$totalStudent[$key] = (sizeof($ldiscipline->getStudentIDByTarget('class', $tempID)) != "") ? sizeof($ldiscipline->getStudentIDByTarget('class', $tempID)) : 0;
			$avgAry[$key] = ($totalStudent==0) ? 0 : round(($associatedScore[$key]/$totalStudent[$key]),2);
		}
	}

	if($sortBy=='asc')
		asort($avgAry);
	else 
		arsort($avgAry);
	
	$i = 0;
	
	foreach($avgAry as $key=>$val) {
		$css = ($i%2==1) ? 2 : 1;
		$css2 = ($i%2==1) ? "2" : "";
		
		$score = 0;
		
		# print view
		$tableContent .= "<tr class='tablebluerow$css'>";
		$tableContent .= "<td class='tablerow$css'>".$ldiscipline->getClassNameByClassID($key)."</td>";
		if (!$lsp->is_merit_disabled && $merit1!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['1']!="") ? $result[$key]['1'] : 0)."</td>";
			$score += ($result[$key]['1']!="") ? $result[$key]['1']*$merit['1'] : 0;
		}
		if (!$lsp->is_min_merit_disabled && $merit2!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['2']!="") ? $result[$key]['2'] : 0)."</td>";
			$score += ($result[$key]['2']!="") ? $result[$key]['2']*$merit['2'] : 0;
		}
		if (!$lsp->is_maj_merit_disabled && $merit3!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['3']!="") ? $result[$key]['3'] : 0)."</td>";
			$score += ($result[$key]['3']!="") ? $result[$key]['3']*$merit['3'] : 0;
		}
		if (!$lsp->is_sup_merit_disabled && $merit4!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['4']!="") ? $result[$key]['4'] : 0)."</td>";
			$score += ($result[$key]['4']!="") ? $result[$key]['4']*$merit['4'] : 0;
		}
		if (!$lsp->is_ult_merit_disabled && $merit5!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['5']!="") ? $result[$key]['5'] : 0)."</td>";
			$score += ($result[$key]['5']!="") ? $result[$key]['5']*$merit['5'] : 0;
		}
		if (!$lsp->is_warning_disabled && $demerit0!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['0']!="") ? $result[$key]['0'] : 0)."</td>";
			$score += ($result[$key]['0']!="") ? $result[$key]['0']*$demerit['0'] : 0;
		}
		if (!$lsp->is_black_disabled && $demerit1!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['-1']!="") ? $result[$key]['-1'] : 0)."</td>";
			$score += ($result[$key]['-1']!="") ? $result[$key]['-1']*$demerit['-1'] : 0;
		}
		if (!$lsp->is_min_demer_disabled && $demerit2!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['-2']!="") ? $result[$key]['-2'] : 0)."</td>";
			$score += ($result[$key]['-2']!="") ? $result[$key]['-2']*$demerit['-2'] : 0;
		}
		if (!$lsp->is_maj_demer_disabled && $demerit3!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['-3']!="") ? $result[$key]['-3'] : 0)."</td>";
			$score += ($result[$key]['-3']!="") ? $result[$key]['-3']*$demerit['-3'] : 0;
		}
		if (!$lsp->is_sup_demer_disabled && $demerit4!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['-4']!="") ? $result[$key]['-4'] : 0)."</td>";
			$score += ($result[$key]['-4']!="") ? $result[$key]['-4']*$demerit['-4'] : 0;
		}
		if (!$lsp->is_ult_demer_disabled && $demerit5!=0) {
			$tableContent .= "	<td class='tablerow_total$css2' align='center'>".(($result[$key]['-5']!="") ? $result[$key]['-5'] : 0)."</td>";
			$score += ($result[$key]['-5']!="") ? $result[$key]['-5']*$demerit['-5'] : 0;
		}
		
		
		$tableContent .= "<td class='tabletext tablerow$css' align='center'>".$associatedScore[$key]."</td>";
		$tableContent .= "<td class='tabletext tablerow$css' align='center'>".$totalStudent[$key]."</td>";
		$tableContent .= "<td class='tablerow_total$css2' align='center'>".$avgAry[$key]."</td>";
		$tableContent .= "</tr>";
		
		# export view
		$export_content .= "&quot;".$ldiscipline->getClassNameByClassID($key)."&quot;\t";
		if (!$lsp->is_merit_disabled && $merit1!=0)
			$export_content .= "&quot;".(($result[$key]['1']!="") ? $result[$key]['1'] : 0)."&quot;\t";
		if (!$lsp->is_min_merit_disabled && $merit2!=0)
			$export_content .= "&quot;".(($result[$key]['2']!="") ? $result[$key]['2'] : 0)."&quot;\t";
		if (!$lsp->is_maj_merit_disabled && $merit3!=0)
			$export_content .= "&quot;".(($result[$key]['3']!="") ? $result[$key]['3'] : 0)."&quot;\t";
		if (!$lsp->is_sup_merit_disabled && $merit4!=0)
			$export_content .= "&quot;".(($result[$key]['4']!="") ? $result[$key]['4'] : 0)."&quot;\t";
		if (!$lsp->is_ult_merit_disabled && $merit5!=0)
			$export_content .= "&quot;".(($result[$key]['5']!="") ? $result[$key]['5'] : 0)."&quot;\t";
		if (!$lsp->is_warning_disabled && $demerit0!=0)
			$export_content .= "&quot;".(($result[$key]['0']!="") ? $result[$key]['0'] : 0)."&quot;\t";
		if (!$lsp->is_black_disabled && $demerit1!=0)
			$export_content .= "&quot;".(($result[$key]['-1']!="") ? $result[$key]['-1'] : 0)."&quot;\t";
		if (!$lsp->is_min_demer_disabled && $demerit2!=0)
			$export_content .= "&quot;".(($result[$key]['-2']!="") ? $result[$key]['-2'] : 0)."&quot;\t";
		if (!$lsp->is_maj_demer_disabled && $demerit3!=0)
			$export_content .= "&quot;".(($result[$key]['-3']!="") ? $result[$key]['-3'] : 0)."&quot;\t";
		if (!$lsp->is_sup_demer_disabled && $demerit4!=0)
			$export_content .= "&quot;".(($result[$key]['-4']!="") ? $result[$key]['-4'] : 0)."&quot;\t";
		if (!$lsp->is_ult_demer_disabled && $demerit5!=0)
			$export_content .= "&quot;".(($result[$key]['-5']!="") ? $result[$key]['-5'] : 0)."&quot;\t";
		$export_content .= "&quot;".$associatedScore[$key]."&quot;\t";
		$export_content .= "&quot;".$totalStudent[$key]."&quot;\t";
		$export_content .= "&quot;".$avgAry[$key]."&quot;\t";
		$export_content .= "\n";
		
		$i++;
		if($i>=$top) break;
	}
	$tableContent .= $tableBottom;
	
}

# Report Option Layer
$reportOptionContent .= "<table width='88%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr><td>";
$reportOptionContent = "<table align='center' width='100%' border='0' cellpadding='5' cellspacing='0'>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td height='57' valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['Period']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td><table border='0' cellspacing='0' cellpadding='3'>";
$reportOptionContent .= "<tr><td height='30' colspan='6' onClick=\"document.form1.dateChoice[0].checked=true\">";
$reportOptionContent .= "<input name='dateChoice' type='radio' id='dateChoice[0]' value='1'";
$reportOptionContent .= ($dateChoice==1) ? " checked" : "";
$reportOptionContent .= ">$i_Discipline_School_Year";
$reportOptionContent .= $selectSchoolYear;
$reportOptionContent .= "$i_Discipline_Semester ";
$reportOptionContent .= "<span id='spanSemester'><select name='semester' id='semester' >";
$reportOptionContent .= $SemesterMenu;
$reportOptionContent .= "</select></span></td></tr>";
$reportOptionContent .= "<tr><td><input name='dateChoice' type='radio' id='dateChoice[1]' value='2'";
$reportOptionContent .= ($dateChoice==2) ? " checked" : "";
$reportOptionContent .= ">{$iDiscipline['Period_Start']}</td>";
$reportOptionContent .= "<td><!--<input name='startDate' type='text' class='tabletext' value='$startDate' onFocus=\"document.form1.dateChoice[1].checked=true\" />--></td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true' onFocus='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("startDate",$startDate);
$reportOptionContent .= "</td><td width='25' align='center'>{$iDiscipline['Period_End']}</td>";
$reportOptionContent .= "<td><!--<input name='endDate' type='text' class='tabletext' value='$endDate' onFocus=\"document.form1.dateChoice[1].checked=true\" />--></td>";
$reportOptionContent .= "<td align='center' onClick='form1.dateChoice[1].checked=true' onFocus='form1.dateChoice[1].checked=true'>";
$reportOptionContent .= $linterface->GET_DATE_PICKER("endDate",$endDate);
$reportOptionContent .= "</td></tr></table></td></tr>";
$reportOptionContent .= "<tr valign='top'>";
$reportOptionContent .= "<td valign='top' nowrap='nowrap' class='formfieldtitle'>{$iDiscipline['RankingTarget']}<span class='tabletextrequire'>*</span></td>";
$reportOptionContent .= "<td width='80%'>";
$reportOptionContent .= "<table width='0%' border='0' cellspacing='2' cellpadding='0'>";
$reportOptionContent .= "<tr><td valign='top'>";
$reportOptionContent .= $rankTargetMenu."<br><span id='spanTarget'>".$select_list."</span>";
$reportOptionContent .= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAllItem(document.getElementById('target[]'), true)");
$reportOptionContent .= "</td></tr>";
$reportOptionContent .= "<tr><td colspan='3' class='tabletextremark'>({$i_Discipline_Press_Ctrl_Key})</td></tr></table></td></tr>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "	<td valign='top' nowrap='nowrap' class='formfieldtitle'>".$Lang['eDiscipline']['uccke_MeritScore']."<span class='tabletextrequire'>*</span>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "	<td>";
$reportOptionContent .=  		$meritScore;
$reportOptionContent .= "	</td>";
$reportOptionContent .= "</tr>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "	<td valign='top' nowrap='nowrap' class='formfieldtitle'>$i_Discipline_Top<span class='tabletextrequire'>*</span>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "	<td>";
$reportOptionContent .= 		$topSelectMenu;
$reportOptionContent .= "	</td>";
$reportOptionContent .= "</tr>";
$reportOptionContent .= "<tr>";
$reportOptionContent .= "	<td valign='top' nowrap='nowrap' class='formfieldtitle'>$list_sortby<span class='tabletextrequire'>*</span>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "	<td>";
$reportOptionContent .= "		<input type='radio' name='sortBy' id='sortBy1' value='asc'";
$reportOptionContent .= ($sortBy=='asc') ? " checked" : "";
$reportOptionContent .= "> <label for='sortBy1'>$list_asc </label>";
$reportOptionContent .= "		<input type='radio' name='sortBy' id='sortBy2' value='desc'";
$reportOptionContent .= ($sortBy=='desc') ? " checked" : "";
$reportOptionContent .= "> <label for='sortBy2'>$list_desc </label>";
$reportOptionContent .= "	</td>";
$reportOptionContent .= "</tr>";
$reportOptionContent .= "<tr><td valign='top' nowrap='nowrap' class='tabletextremark'>{$i_general_required_field}</td>";
$reportOptionContent .= "<td align='center'>";
$reportOptionContent .= "</td></tr><tr><td align='center' colspan='2'>";
$reportOptionContent .= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "document.form1.target='';document.form1.action='report_view.php'");
$reportOptionContent .= "</td></tr></table>";

# menu highlight setting
$TAGS_OBJ[] = array($Lang['eDiscipline']['TermAwardScheme'], "");

$CurrentPageArr['eDisciplinev12'] = 1;

$CurrentPage = "UCCKE_TermAwardScheme";

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();


# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function SelectAllItem(obj, flag){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}
/*
function SelectAll(obj)
{
		var choice = document.getElementById('rankTargetDetail[]');

         for (var i=0; i<choice.options.length; i++)
         {
              choice.options[i].selected = true;
         }
}
*/
function showSpan(span){
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span){
	document.getElementById(span).style.display="none";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
		//form1.elements['rankTargetDetail[]'].multiple = true;
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('divReportOption');
	document.getElementById('showReportOption').value = '1';
}

function hideOption(){
	hideSpan('divReportOption');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('showReportOption').value = '0';
}

function goCheck(form1) {
	var choiceSelected;
	var choice = "";

	if(form1.dateChoice[0].checked==false && form1.dateChoice[1].checked==false)	 {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	
	if(form1.dateChoice[1].checked==true && (form1.startDate.value == "" || form1.endDate.value == "")) {
		alert("<?=$i_alert_pleaseselect?><?=$iDiscipline['Period']?>");	
		return false;
	}
	
	if(form1.dateChoice[1].checked==true && ((!check_date(form1.startDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.endDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
	{
		return false;
	}
	
	if(form1.dateChoice[1].checked==true && (form1.startDate.value > form1.endDate.value)) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(form1.level.value=='') {
		alert("");	
		return false;
	}
	choice = document.getElementById('target[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect." ".$i_Discipline_Class."/".$i_Discipline_Form?>");
		return false;
	}	 
		
}
//-->
</script>
<script language="javascript">
function changeType(form_obj,lvl_value)
{
     obj = form_obj.elements["target[]"];
     <? # Clear existing options
     ?>
     while (obj.options.length > 0)
     {
            obj.options[0] = null;
     }
     if (lvl_value==1)
     {
         <?
         for ($i=0; $i<sizeof($levels); $i++)
         {
              list($id,$name) = $levels[$i];
              ?>
              obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
              <?

         }
         ?>
     }
     else
     {
         <?
         for ($i=0; $i<sizeof($classes); $i++)
         {
              //list($id,$name, $lvl_id) = $classes[$i];
              list($name, $id) = $classes[$i];
              ?>
              obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
              <?

         }
         ?>
     }
     SelectAllItem(document.getElementById('target[]'), true)
}

var xmlHttp
var xmlHttp3

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$semester?>";
	url += "&field=semester";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function changeClassForm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanTarget").innerHTML = "";
		document.getElementById("spanTarget").style.border = "0px";
		return
	}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxChangeClass.php";
	url += "?year=" + val;
	url += "&level=" + document.getElementById('level').value;
	
	xmlHttp.onreadystatechange = stateChanged 
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
	
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}
</script>
<form name="form1" method="post" action="report_view.php" onSubmit="return goCheck(this)">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td class="report_show_option">
									<span id="spanShowOption"><a href="javascript:showOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_show_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Show_Statistics_Option?></a></span>
									<span id="spanHideOption" style="display:none"><a href="javascript:hideOption();" class="contenttool"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_hide_option.gif" width="20" height="20" border="0" align="absmiddle"><?=$i_Discipline_Hide_Statistics_Option?></a></span>
									<div id='divReportOption' style='display:none'><?=$reportOptionContent?></div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table width="88%" border="0" cellpadding="5" cellspacing="0" class="result_box">
				<tr>
					<td>
						<table border="0" cellspacing="5" cellpadding="5" align="center">
							<tr>
								<td align="center" valign="middle" class="Chart_title"><?=$title?></td>
							</tr>
						</table>
						<br>
						<?=$tableContent?>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_export, "submit", "document.form1.target='_self';document.form1.action='report_export.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_print, "submit", "document.form1.target='_blank';document.form1.action='report_print.php'")?>
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location.href='index.php'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>
<input type="hidden" name="showReportOption" id="showReportOption" value="0">
<input type="hidden" name="studentFlag" id="studentFlag" value="<? if(sizeof($studentID)>0){echo "1";} else { echo "0";} ?>">			
<input type="hidden" name="ItemText1" id="ItemText1" />
<input type="hidden" name="ItemText2" id="ItemText2" />
<input type="hidden" name="submit_flag" id="submit_flag" value="YES" />
<input type="hidden" name="SelectedItemText" id="SelectedItemText" />
<input type="hidden" name="content" value='<?=urlencode($tableContent)?>' />
<input type="hidden" name="title" value='<?=urlencode($title)?>' />
<input type="hidden" name="exportContent" value='<?=$export_content?>' />
</form>

<script language="javascript">
	changeTerm('<?=$SchoolYear?>');
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
