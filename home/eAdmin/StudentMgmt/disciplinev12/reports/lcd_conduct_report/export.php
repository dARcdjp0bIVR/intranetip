<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$sys_custom['lcd_ap_report']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lexport = new libexporttext();

$filename = "conduct_report.csv";

$exportContent = $ldiscipline->Get_LCD_AP_Report($AcademicYearID, $level, $target, $include_waive);

//$export_content = $lexport->GET_EXPORT_TXT($exportContent, $exportColumn);

$export_content .= stripslashes($exportContent);	
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>