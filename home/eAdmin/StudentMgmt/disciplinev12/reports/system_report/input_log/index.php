<?php
// Using:
/*
 *  Date: 2018-03-21 (Bill)
 *          Fixed: CONCAT() in SQL when PHP Version < 5.4
 *  
 * 	Date: 2018-03-19 (Bill)     [2018-0202-1046-39164]
 *          Create File (Copy logic from Deletion Log)   ($sys_custom['eDiscipline']['EntryRecordLog'])
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

# Current Page
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Reports_SystemReport";

if (!$sys_custom['eDiscipline']['EntryRecordLog'] || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Date range
$StartDate = $StartDate ? $StartDate : date("Y-m-d");
$EndDate = $EndDate ? $EndDate : date("Y-m-d");

$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();
$currentYearID = Get_Current_Academic_Year_ID();

# Table Info
$pageSizeChangeEnabled = true;
if($field == "") $field = 0;
$li = new libdbtable2007($field, $order, $pageNo);

$studentNameField = getNameFieldWithClassNumberByLang("iu.");
$inputByNameField = getNamefieldByLang("iu2.");

// Conduct details field
$subjectNameField = ($intranet_session_language=="en") ? "sub.EN_DES" : "sub.CH_DES";
if($use_intranet_homework)
{
    $conductItemNameField = " IF(ar.CategoryID = 2, CONCAT(ac.Name, ' - ', $subjectNameField), IF(aci.Name IS NULL, ac.Name, CONCAT(ac.Name, ' - ', aci.Name)))";
    $itemCategoryCond = " AND ar.CategoryID != 2 ";
}
else
{
    $conductItemNameField = " IF(aci.Name IS NULL, ac.Name, CONCAT(ac.Name, ' - ', aci.Name))";
    $itemCategoryCond = "";
}
$conductDetailsField = "
CONCAT(
    'Record Date: ', LEFT(ar.RecordDate, 10), '<br/>',
    'Student: ', ".$studentNameField.", '<br/>',
    'Record: ', ".$conductItemNameField."
)";

// Merit details field
if($intranet_session_language == "en")
{
    $meritStr = "CONCAT(
                    mr.ProfileMeritCount, ' ',
                    CASE (mr.ProfileMeritType)
                        WHEN 0 THEN '$i_Merit_Warning'
                        WHEN 1 THEN '$i_Merit_Merit'
                        WHEN 2 THEN '$i_Merit_MinorCredit'
                        WHEN 3 THEN '$i_Merit_MajorCredit'
                        WHEN 4 THEN '$i_Merit_SuperCredit'
                        WHEN 5 THEN '$i_Merit_UltraCredit'
                        WHEN -1 THEN '$i_Merit_BlackMark'
                        WHEN -2 THEN '$i_Merit_MinorDemerit'
                        WHEN -3 THEN '$i_Merit_MajorDemerit'
                        WHEN -4 THEN '$i_Merit_SuperDemerit'
                        WHEN -5 THEN '$i_Merit_UltraDemerit'
                    ELSE '---' END, '(s)'
                )";
}
else
{
    $meritStr = "CONCAT(
                    CASE (mr.ProfileMeritType)
                        WHEN 0 THEN '$i_Merit_Warning'
                        WHEN 1 THEN '$i_Merit_Merit'
                        WHEN 2 THEN '$i_Merit_MinorCredit'
                        WHEN 3 THEN '$i_Merit_MajorCredit'
                        WHEN 4 THEN '$i_Merit_SuperCredit'
                        WHEN 5 THEN '$i_Merit_UltraCredit'
                        WHEN -1 THEN '$i_Merit_BlackMark'
                        WHEN -2 THEN '$i_Merit_MinorDemerit'
                        WHEN -3 THEN '$i_Merit_MajorDemerit'
                        WHEN -4 THEN '$i_Merit_SuperDemerit'
                        WHEN -5 THEN '$i_Merit_UltraDemerit'
                    ELSE '---' END,
                    CAST(mr.ProfileMeritCount as char),
                    '". $Lang['eDiscipline']['Times'] ."'
				)";
}
$meritDetailsField = "
CONCAT(
    'Record Date: ', mr.RecordDate, '<br/>',
    'Student: ', ".$studentNameField.", '<br/>',
    'Record: ', mr.ItemText,
                ' (',
                    IF(mr.ProfileMeritType = -999, '', ".$meritStr."),
                    IF((mr.ConductScoreChange != 0 OR mr.ConductScoreChange IS NOT NULL),  
                        CONCAT(
                            IF(mr.ProfileMeritType = -999, '', ' , '), 
                            'Conduct Mark: ', IF(mr.ConductScoreChange > 0, '+', ''), mr.ConductScoreChange
                        ), ''
                    ),
                ')'
)";

$subjectTable = "";
if($use_intranet_homework) {
    $subjectTable = "  LEFT JOIN ASSESSMENT_SUBJECT as sub ON (ar.ItemID = sub.RecordID)";
}

$sql = "SELECT
    		LEFT(ar.DateInput, 10) as InputDate,
    		".$inputByNameField." as InputBy,
    		'".$eDiscipline['Good_Conduct_and_Misconduct']."' as RecordType,
            ".$conductDetailsField." as RecordDetails,
            ar.RecordID
		FROM
			DISCIPLINE_ACCU_RECORD as ar
			INNER JOIN DISCIPLINE_ACCU_CATEGORY as ac ON (ar.CategoryID = ac.CategoryID)
			LEFT JOIN DISCIPLINE_ACCU_CATEGORY_ITEM as aci ON (ar.ItemID = aci.ItemID ".$itemCategoryCond.")
            ".$subjectTable."
            INNER JOIN INTRANET_USER as iu ON (ar.StudentID = iu.UserID AND iu.RecordStatus IN (0, 1, 2))
			INNER JOIN YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
			INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$currentYearID."')
			INNER JOIN INTRANET_USER as iu2 ON (ar.CreatedBy = iu2.UserID AND iu2.RecordStatus IN (0, 1, 2))
		WHERE
            ar.DateInput >= '".$StartDate." 00:00:00' AND ar.DateInput <= '".$EndDate." 23:59:59' AND 
            ar.DateInput IS NOT NULL
        GROUP BY
            ar.RecordID
        UNION
        SELECT
    		LEFT(mr.DateInput, 10) as InputDate,
    		".$inputByNameField." as InputBy,
    		'".$eDiscipline['Award_and_Punishment']."' as RecordType,
            ".$meritDetailsField." as RecordDetails,
            mr.RecordID
		FROM
			DISCIPLINE_MERIT_RECORD as mr
		    INNER JOIN DISCIPLINE_MERIT_ITEM as mi ON (mi.ItemID = mr.ItemID)
            INNER JOIN INTRANET_USER as iu ON (mr.StudentID = iu.UserID AND iu.RecordStatus IN (0, 1, 2))
            INNER JOIN YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
            INNER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '".$currentYearID."')
            INNER JOIN INTRANET_USER as iu2 ON (mr.CreatedBy = iu2.UserID AND iu2.RecordStatus IN (0, 1, 2))
        WHERE
            mr.DateInput >= '".$StartDate." 00:00:00' AND mr.DateInput <= '".$EndDate." 23:59:59' AND 
            mr.DateInput IS NOT NULL
        GROUP BY
            mr.RecordID";
$li->sql = $sql;

$li->no_col = 5;
$li->IsColOff = "IP25_table";
$li->field_array = array("InputDate");
$li->count_mode = 1;

// Table Column
$li->column_list .= "<th width='2%'>#</th>\n";
$li->column_list .= "<th>".$Lang['eDiscipline']['InputLog']['InputDate']."</th>\n";
$li->column_list .= "<th>".$Lang['eDiscipline']['InputLog']['InputBy']."</th>\n";
$li->column_list .= "<th>".$Lang['eDiscipline']['RecordType']."</th>\n";
$li->column_list .= "<th>".$Lang['eDiscipline']['RecordInfo']."</th>\n";

### Filter - Date Range
$date_select = "";
$date_select .= $eNotice['Period_Start'] .": ";
$date_select .= $linterface->GET_DATE_PICKER("StartDate", $StartDate);
$date_select .= $eNotice['Period_End']."&nbsp;";
$date_select .= $linterface->GET_DATE_PICKER("EndDate", $EndDate);
$date_select .= $linterface->GET_BTN($button_submit, "submit");

### Title ###
$TAGS_OBJ[] = array($Lang['eDiscipline']['DeleteLog'], "../del_log/");
$TAGS_OBJ[] = array($Lang['eDiscipline']['InputLog']['Title'], "../input_log/", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<SCRIPT LANGUAGE=Javascript>
<!--//
function check_form()
{
	obj = document.form1;
	
	if(!check_date(obj.StartDate,"<?=$i_invalid_date?>"))
	{
		obj.StartDate.focus();
		return false;
	}
	if(!check_date(obj.EndDate,"<?=$i_invalid_date?>"))
	{
		obj.EndDate.focus();
		return false;
	}
	if(compareDate(obj.EndDate.value, obj.StartDate.value)<0) {
		obj.StartDate.focus();
		alert("<?=$i_con_msg_date_startend_wrong_alert?>");
		return false;
	}	
	
	return true;
}
//-->
</SCRIPT>

<form name="form1" method="get" action="index.php" onSubmit="return check_form();">

<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<?=$date_select?>
	</td>
	<td valign="bottom">
    	<div class="common_table_tool">
    		<?=$delBtn?>
    	</div>
	</td>
</tr>
</table>
</div>

<?=$li->display();?>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>