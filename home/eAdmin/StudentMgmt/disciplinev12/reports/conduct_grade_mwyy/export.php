<?php
# using: henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$sys_custom['eDiscipline']['Conduct_Grade']['MWYY']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}


$lexport = new libexporttext();
$ay = new academic_year($year);


# Class
$targetClass = $target[0];
//$ClassName = $ldiscipline->getClassNameBYClassID($targetClass);


$studentAry = $ldiscipline->storeStudent(0,$targetClass, $AcademicYearID=$year);
$totalStudent = count($studentAry);

# File Name
$filename = "conduct_grade_report.csv";


$data = array();

if($totalStudent>0) {
	if($semester!=0 && $semester!="") $conds = " AND r.YearTermID='$semester'";
	
	# Prepare Data [Start]
	$sql = "SELECT r.StudentID, SUM(r.ConductScoreChange) as TotalDeducted, COUNT(r.RecordID) as TotalRecord FROM DISCIPLINE_MERIT_RECORD r WHERE r.AcademicYearID='$year' $conds AND (r.MeritType=-1 OR r.MeritType IS NULL) AND r.StudentID IN (".implode(',',$studentAry).") AND r.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND r.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED." GROUP BY r.StudentID";
	$result = $ldiscipline->returnArray($sql);
	$totalResult = count($result);
	for($i=0; $i<$totalResult; $i++) {
		list($studentid, $totalDeducted, $totalRecord) = $result[$i];
		$data[$studentid]['TotalDeducted'] = ($totalDeducted) ? abs($totalDeducted) : "0";
		$data[$studentid]['TotalRecord'] = $totalRecord;
		$data[$studentid]['Demerit'] = floor($data[$studentid]['TotalDeducted']/5);
	}
	
	$sql = "SELECT StudentID, SUM(ConductScoreChange) as TotalNonHwDeducted FROM DISCIPLINE_MERIT_RECORD r WHERE AcademicYearID='$year' $conds AND (MeritType=-1 OR MeritType IS NULL) AND StudentID IN (".implode(',',$studentAry).") AND RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED." AND ItemID!=93 GROUP BY StudentID";
	$result = $ldiscipline->returnArray($sql);
	$totalResult = count($result);
	for($i=0; $i<$totalResult; $i++) {
		list($studentid, $totalNonHwDeducted) = $result[$i];
		$data[$studentid]['TotalNonHwDeducted'] = abs($totalNonHwDeducted);
	}
	
	$sql = "SELECT StudentID, TagName, COUNT(RecordID) as totalRecord FROM DISCIPLINE_MERIT_RECORD r INNER JOIN DISCIPLINE_AP_ITEM_TAG ait ON (ait.APItemID=r.ItemID) INNER JOIN DISCIPLINE_ITEM_TAG_SETTING t ON (t.TagID=ait.TagID) WHERE r.AcademicYearID='$year' $conds AND (r.MeritType=-1 OR r.MeritType IS NULL) AND r.StudentID IN (".implode(',',$studentAry).") AND r.RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND r.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED."  GROUP BY r.StudentID, t.TagID";
	$result = $ldiscipline->returnArray($sql);
	$totalResult = count($result);
	for($i=0; $i<$totalResult; $i++) {
		list($studentid, $tagname, $totalRecord) = $result[$i];
		if(strlen($tagname)>2) {
			if($totalRecord!=0 && $tagname!="") {
				$data[$studentid]['Reasons'] .= $tagname.$totalRecord." ";	
			}
		} else {
			if((!isset($data[$studentid]['MP']) || $data[$studentid]['MP']=="") && substr($tagname,0,2)=="MP" && $totalRecord>0) {
				$data[$studentid]['MP'] = "X";
			}
			if((!isset($data[$studentid]['MU']) || $data[$studentid]['MU']=="") && substr($tagname,0,2)=="MU" && $totalRecord>0) {
				$data[$studentid]['MU'] = "X";
			}
			if((!isset($data[$studentid]['MH']) || $data[$studentid]['MH']=="") && substr($tagname,0,2)=="MH" && $totalRecord>0) {
				$data[$studentid]['MH'] = "X";
			}	
		}
	}
	# Prepare Data [End]
	
	# Prepare Export Content [Start]
	$exportContent = Get_Lang_Selection($ay->YearNameB5, $ay->YearNameEN);
	if(isset($semester) && $semester!=0) {
		$TermName = $ldiscipline->getTermNameByTermID($semester);
		if($TermName!="") $exportContent .= " ($TermName)";	
	}
	$exportContent .= "\n";
	
	$exportContent .= $Lang['General']['Class']."\t";
	$exportContent .= $Lang['General']['ClassNo_ShortForm']."\t";
	$exportContent .= $Lang['General']['EnglishName']."\t";
	$exportContent .= $Lang['General']['ChineseName']."\t";
	$exportContent .= $Lang['General']['Sex']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['ConductGrade']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['MarksDeductedNonHW']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['TotalMarksDeducted']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['Reasons']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['MP']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['MU']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['MH']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['Demerits']."\t";
	$exportContent .= $Lang['eDiscipline']['MWYY']['Remarks']."\n";
	
	
	for($a=0; $a<$totalStudent; $a++) {
		$studentid = $studentAry[$a];
		$thisUser = new libuser($studentid);
		//debug_pr($thisUser);
		$tempClassName = $thisUser->GET_USER_BY_CLASS_CLASSNO($thisUser->ClassName,$thisUser->ClassNumber,$year);
		$thisClassName = Get_Lang_Selection($tempClassName[0]['ClassTitleB5'], $tempClassName[0]['ClassTitleEN']);
		$exportContent .= "\"".$thisClassName."\"\t";
		$exportContent .= "\"".$thisUser->ClassNumber."\"\t";
		$exportContent .= "\"".$thisUser->EnglishName."\"\t";
		$exportContent .= "\"".$thisUser->ChineseName."\"\t";
		$exportContent .= "\"".$thisUser->Gender."\"\t";
		$exportContent .= "\"\"\t";
		$exportContent .= "\"".($data[$studentid]['TotalNonHwDeducted']?$data[$studentid]['TotalNonHwDeducted']:"0")."\"\t";
		$exportContent .= "\"".($data[$studentid]['TotalDeducted']?$data[$studentid]['TotalDeducted']:"0")."\"\t";
		$exportContent .= "\"".$data[$studentid]['Reasons']."\"\t";
		$exportContent .= "\"".$data[$studentid]['MP']."\"\t"; 
		$exportContent .= "\"".$data[$studentid]['MU']."\"\t"; 
		$exportContent .= "\"".$data[$studentid]['MH']."\"\t"; 
		$exportContent .= "\"".($data[$studentid]['Demerit']?$data[$studentid]['Demerit']:"0")."\"\t";
		$exportContent .= "\"\"\n";
	}
	
}

$export_content .= stripslashes($exportContent);	
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>