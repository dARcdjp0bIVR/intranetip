<?php
// Editing by 

/***************
 * 
 * Date:	2017-01-16	Bill	[2016-0825-1456-31225]
 * 			- Create file
 * 
 ***************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

# user access right checking
if(!$sys_custom['eDiscipline']['CustLateDemeritReport'] || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$ldiscipline_ui = new libdisciplinev12_ui_cust();
echo $ldiscipline_ui->getStudentLateMeritReport($_POST);

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>

<style type="text/css">
@charset "utf-8";
/* CSS Document */
.main_container { display: block; width: 713px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }
.main_container .report_header td { font-size: 20px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: center; padding-top: 5px; }
.main_container .report_table { padding-top: 13px; }
.main_container .report_table td { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding-bottom: 1px; }
.main_container .report_table td.data_td { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; font-weight: bold; text-align: center; border-bottom: 1px solid black; }
.main_container .report_static { padding-top: 14px }
.main_container .report_static td { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding-bottom: 10px; }
.main_container .report_static2 { padding-top: 24px }
.main_container .report_static2 td { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding-bottom: 15px; }
.main_container .report_static td.remainder, .main_container .report_static2 td.remainder { font-size: 18px; padding-bottom: 13px; }
.page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }
</style>