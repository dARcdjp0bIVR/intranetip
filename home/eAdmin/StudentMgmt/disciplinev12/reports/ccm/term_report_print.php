<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();


$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

ini_set("memory_limit", "150M"); 

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$classAry = array();
$studentAry = array();


$meritDemerit[5] = $i_Merit_Warning;
$meritDemerit[6] = $i_Merit_Merit;
$meritDemerit[7] = $i_Merit_MinorCredit;
$meritDemerit[8] = $i_Merit_MajorCredit;
$meritDemerit[9] = $i_Merit_SuperCredit;
$meritDemerit[10] = $i_Merit_UltraCredit;
$meritDemerit[4] = $i_Merit_BlackMark;
$meritDemerit[3] = $i_Merit_MinorDemerit;
$meritDemerit[2] = $i_Merit_MajorDemerit;
$meritDemerit[1] = $i_Merit_SuperDemerit;
$meritDemerit[0] = $i_Merit_UltraDemerit;

$studentAry = $ldiscipline->storeStudent($studentID, $targetClass);


//$SchoolYearText = $ldiscipline->getAcademicYearNameByYearID($SchoolYearID);

$act = new academic_year_term();
$semesterText = $act->Get_YearNameByID($semester);

//$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $semesterText;

$school_name = $_SESSION["SSV_PRIVILEGE"]["school"]["name"];

//$print_date = date("d/m/Y");

$periodType = $_POST['PeriodType'];

# SQL conditions (A&P) #

if($periodType=='0'){ # Year Term
	if($SchoolYearID != '0') {		# specific school year
		$conds .= " AND a.AcademicYearID='$SchoolYearID'";
	} else {						# all school year
		$academicYear = $ldiscipline->generateAllSchoolYear();
	}
	
	if($semester!='WholeYear' && $semester!=0) {	# specific semester
		$conds .= " AND a.YearTermID='$semester'";
	}
}elseif($periodType=='1'){ # Date Range
	$startDate = $_POST['textFromDate'];
	$endDate = $_POST['textToDate'];
	$conds .= " AND UNIX_TIMESTAMP(a.RecordDate) BETWEEN UNIX_TIMESTAMP('$startDate') AND UNIX_TIMESTAMP('$endDate') "; 
		
}

if($waive_record1 == 1) {	# with waived record
	$conds .= " AND ((a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.") OR (a.RecordStatus=".DISCIPLINE_STATUS_WAIVED."))";
} else { 					# only approved record

	$conds .= " AND (a.RecordStatus=".DISCIPLINE_STATUS_APPROVED.")";	
}

$export_content .= "<table width='100%' align='center' class='print_hide'><tr>";
$export_content .= "<td align='right'>";
$export_content .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$export_content .= "</td></tr></table>";
$export_content .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' >";
$export_content .= "<tr><td valign='top'>";

$waivedImg = "<img src={$image_path}/{$LAYOUT_SKIN}/icon_waived.gif width=20 height=20 align=absmiddle title=Waived border=0>";


for($i=0;$i<sizeof($studentAry);$i++)
{

	# student info
	$stdName = $ldiscipline->getStudentNameByID($studentAry[$i]);
	list($userID, $std_name, $className, $classNumber) = $stdName[0];
	$user_number = str_replace("S", "", str_replace("s", "", $stdName[0]["UserLogin"]));
	$user_ChineseName = $stdName[0]["ChineseName"];
	$user_EnglishName = $stdName[0]["EnglishName"];
	
	// fix: award display in punishment records if Recieve = "--" as ProfileMeritType = -999 [2015-0204-1028-53066]
	// SQL chnage: IF(a.ProfileMeritType>0, 1, 0) AS MeritSection -> IF(a.MeritType = 1, 1, 0) AS MeritSection
	$sql = "
			SELECT 
				a.RecordDate as RecordDate, 
				IF(a.RecordStatus=2,'".$Lang['StudentAttendance']['Waived']."','') as Waive,
				a.ProfileMeritCount as meritCount, 
				a.ProfileMeritType as meritType,
				CONCAT(c.ItemCode,' - ',REPLACE(c.ItemName,'<','&lt;')) as item,
				a.Remark,
				a.ConductScoreChange,
				a.SubScore1Change,
				IF(a.MeritType = 1, 1, 0) AS MeritSection
		
			FROM DISCIPLINE_MERIT_RECORD as a
				LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as c ON a.ItemID = c.ItemID
				LEFT OUTER JOIN DISCIPLINE_MERIT_TYPE_SETTING as d ON a.ProfileMeritType = d.MeritType
				LEFT OUTER JOIN INTRANET_USER as e ON a.PICID = e.UserID
			WHERE a.DateInput IS NOT NULL
				AND b.UserID = $userID
				AND a.ReleaseStatus=".DISCIPLINE_STATUS_RELEASED."
				$conds
				ORDER BY MeritSection ASC, a.RecordDate ASC
		";
		
	$row = $ldiscipline->returnArray($sql,5);
	
	if (sizeof($row)<=0 && $NotShowStudentWithoutRecord)
	{
		continue;
	}

	$export_content_student = '<table width="100%" border="0" cellpadding="0" cellspacing="0" class="print_hide"><tr><td style="border-bottom:dashed orange 2px;">&nbsp;</td></table>';
	$export_content_student .= "<table width='100%' border='0' cellspacing='0' cellpadding='0' style='page-break-after: always;'>";
	$export_content_student .= "<tr><td colspan='2' valign='top'>";
	
	$export_content_student .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	$export_content_student .= "<tr><td width='20%' valign='top'><img src='school_logo.gif' width='60' border='0' /></td><td align='center' width='60%'><br /><br /><br /><span style='font-size:22px;'>{$school_name}</span></td><td width='20%' align='right' valign='top'>" .
					"<table width='150' border='0'><tr><td align='right' nowrap='nowrap'><font size='2'>".$Lang['eNotice']['PrintDate']." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'><font size='2'>{$PrintDate}</td></tr><tr><td align='right' nowrap='nowrap'><font size='2'>".$Lang['eDiscipline']['ccm_term_report_print_term']." &nbsp; &nbsp; &nbsp;</td><td nowrap='nowrap'><font size='2'>{$PrintTerm}</td></tr></table>" .
					"</td></tr>";
	$export_content_student .= "</table>";
	
	
	$export_content_student .= "</td></tr>";
	$export_content_student .= '<tr><td colspan="2" height="12"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="border-top:solid black 2px;"><img src="/images/space.gif" height="1" border="0" /></td></table></td></tr>';
	
	$inner_width = "96%";

	$export_content_student .= "<tr><td align='center' colspan='2'>";
	$export_content_student .= "<table width='".$inner_width."' border='0' cellspacing='0' cellpadding='5' ><tr><td><table width='100%' border='0' cellspacing='0' cellpadding='2' >";
	$export_content_student .= "<tr><td nowrap='nowrap'><font size='2'>".$Lang['eDiscipline']['ccm_term_report_student_no'].": &nbsp; &nbsp; &nbsp;</td><td width='90%'><font size='2'>{$user_number}</td></tr>";
	$export_content_student .= "<tr><td nowrap='nowrap'><font size='2'>".$Lang['StudentRegistry']['ChineseName'].": &nbsp; &nbsp; &nbsp;</td><td><font size='2'>{$user_ChineseName}</td></tr>";
	$export_content_student .= "<tr><td nowrap='nowrap'><font size='2'>".$Lang['StudentRegistry']['EnglishName'].": &nbsp; &nbsp; &nbsp;</td><td><font size='2'>{$user_EnglishName}</td></tr>";
	$export_content_student .= "<tr><td nowrap='nowrap'><font size='2'>".$Lang['SysMgr']['FormClassMapping']['Class'].": &nbsp; &nbsp; &nbsp;</td><td><font size='2'>{$className}</td></tr>";

	$export_content_student .= "</table></td></tr></table></td></tr>";


	$export_content_student .= '<tr><td colspan="2" align="center" valign="top"><table width="'.$inner_width.'" border="0" cellpadding="0" cellspacing="0"><tr><td style="border-bottom:solid black 1px;"><img src="/images/space.gif" height="1" border="0" /></td></table></td></tr>';
	
	
	# --------- Remark for teacher to writ ---------- 
	$export_content .= $export_content_student;
	$export_content .= "<tr><td colspan='2' align='center'><table width='".$inner_width."' border='0' cellspacing='0' cellpadding='5' >";
	$export_content .= "<tr><td height='180' valign='top'><font size='2'>".$Lang['General']['Remark']." :</font> &nbsp;</td></tr>";
	$export_content .= "</table></td></tr>";
	
	
	
	$export_content .= '<tr><td colspan="2" height="12" align="center"><table width="'.$inner_width.'" border="0" cellpadding="0" cellspacing="0"><tr><td style="border-top:solid black 1px;"><img src="/images/space.gif" height="1" border="0" /></td></table></td></tr>';
	
	# --------- Table A&P ---------- 
	
	$row_awards = array();
	$row_punishments = array();
	
	for($j=0; $j<sizeof($row); $j++)
	{
		if ($row[$j]["MeritSection"]==0)
		{
			$row_punishments[] = $row[$j];
			//$row_punishments[] = $row[$j];
		} else
		{
			$row_awards[] = $row[$j];
			//$row_awards[] = $row[$j];
			//$row_awards[] = $row[$j];
			//$row_awards[] = $row[$j];
		}
	}
	
	$two_page_mode = (sizeof($row_punishments) + sizeof($row_awards)>=20);
	$ap_box_height_p1 = ($two_page_mode) ? 100 : 575;
	$export_content .= "<tr><td align='center' colspan='2' valign='top' height='{$ap_box_height_p1}'>";
	
	# --------- Table awards ---------- 
	
	//$export_content .= "<tr><td align='center' colspan='2'>";
	$export_content .= "<table width='".$inner_width."' border='0' cellspacing='0' cellpadding='1' >";	
	$export_content .= "<tr><td width='15%'><font size='2'>".$Lang['eDiscipline']['ccm_term_report_award'].":</td><td width='1%'>&nbsp;</td><td width='2%'>&nbsp;</td><td colspan='5'>&nbsp;</td></tr>";
	
	$export_content .= "<tr><td colspan='3'>&nbsp;</td><td width='15%' align='right'>".$Lang['eDiscipline']['EventDate']." &nbsp; &nbsp;</td><td width='32%'>".$Lang['eDiscipline']['ccm_term_report_award_reason'] ."</td><td width='20%'>".$Lang['eInventory']['Details']."</td><td width='5%'>&nbsp;</td><td width='2%'>&nbsp;</td></tr>";
	$export_content .= '<tr><td colspan="2"><img src="/images/space.gif" height="1" border="0" /></td><td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="border-top:solid black 2px;"><img src="/images/space.gif" height="1" border="0" /></td></table></td><td width="2%"><img src="/images/space.gif" height="1" border="0" /></td></tr>';
		
	if (sizeof($row_awards)<=0)
	{
		$export_content .= '<tr><td colspan="2">&nbsp;</td><td colspan="5">'.$Lang['eDiscipline']['ccm_term_report_no_record'].'</td><td width="2%">&nbsp;</td></tr>';
	} else
	{
		
		for ($j=0; $j<sizeof($row_awards); $j++)
		{
			$item_details = "";
			$record_obj = $row_awards[$j];

			$conduct_mark_change = ($record_obj["ConductScoreChange"]==(int)$record_obj["ConductScoreChange"]) ? (int)$record_obj["ConductScoreChange"] : $record_obj["ConductScoreChange"];
			
			$event_date = date("d/m/Y", strtotime($record_obj["RecordDate"]));
			$merite_title = $meritDemerit[$record_obj["meritType"] + 5];

			if ($conduct_mark_change!=0)
			{
				$item_details = $conduct_mark_change." ".$Lang['eDiscipline']['ConductMark'];
			}
			if ($merite_title!="" && $record_obj["meritCount"]>0)
			{
				$meritCount = ($record_obj["meritCount"]==(int)$record_obj["meritCount"]) ? (int)$record_obj["meritCount"] : $record_obj["meritCount"];
				$item_details .= (($item_details!="") ? ", ":""). $meritCount." ".$merite_title;
			}
			$export_content .= "<tr><td colspan='3' height='24'>&nbsp;</td><td align='right' nowrap='nowrap'>".$event_date." &nbsp; &nbsp;</td><td >".$record_obj["item"]."</td><td >{$item_details}</td><td >".$record_obj["Waive"]."</td><td >&nbsp;</td></tr>";
		}
	}
	$export_content .= "</table>";
	
	if ($two_page_mode)
	{
		$export_content .= "</td></tr>"; 
		$export_content .= $export_content_student;
		$ap_box_height_p2 = ($two_page_mode) ? 770 : 0;
		$export_content .= "<tr><td align='center' colspan='2' valign='top' height='{$ap_box_height_p2}'>";
		
	} else
	{
		$export_content .= "<br /><br />";
	}

	
	# --------- Table punishments ---------- 
	
	
	$export_content .= "<table width='".$inner_width."' border='0' cellspacing='0' cellpadding='1' >";	
	$export_content .= "<tr><td width='15%'><font size='2'>".$Lang['eDiscipline']['ccm_term_report_punishment'].":</td><td width='1%'>&nbsp;</td><td width='2%'>&nbsp;</td><td colspan='5'>&nbsp;</td></tr>";
	
	$export_content .= "<tr><td colspan='3'>&nbsp;</td><td width='15%' align='right'>".$Lang['eDiscipline']['EventDate']." &nbsp; &nbsp;</td><td width='32%'>".$Lang['eDiscipline']['ccm_term_report_punish_reason']."</td><td width='20%'>".$Lang['eInventory']['Details']."</td><td width='5%'>&nbsp;</td><td width='2%'>&nbsp;</td></tr>";
	$export_content .= '<tr><td colspan="2"><img src="/images/space.gif" height="1" border="0" /></td><td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td style="border-top:solid black 2px;"><img src="/images/space.gif" height="1" border="0" /></td></table></td><td width="2%"><img src="/images/space.gif" height="1" border="0" /></td></tr>';
		
	if (sizeof($row_punishments)<=0)
	{
		$export_content .= '<tr><td colspan="2">&nbsp;</td><td colspan="5">'.$Lang['eDiscipline']['ccm_term_report_no_record'].'</td><td width="2%">&nbsp;</td></tr>';
	} else
	{
		for ($j=0; $j<sizeof($row_punishments); $j++)
		{
			$item_details = "";
			$record_obj = $row_punishments[$j];
			$conduct_mark_change = ($record_obj["ConductScoreChange"]==(int)$record_obj["ConductScoreChange"]) ? (int)$record_obj["ConductScoreChange"] : $record_obj["ConductScoreChange"];
		
			$event_date = date("d/m/Y", strtotime($record_obj["RecordDate"]));
			$merite_title = $meritDemerit[$record_obj["meritType"] + 5];
			if ($conduct_mark_change!=0)
			{
				$item_details = $conduct_mark_change." ".$Lang['eDiscipline']['ConductMark'];
			}
			if ($merite_title!="" && $record_obj["meritCount"]>0)
			{
				$meritCount = ($record_obj["meritCount"]==(int)$record_obj["meritCount"]) ? (int)$record_obj["meritCount"] : $record_obj["meritCount"];
				$item_details .= (($item_details!="") ? ", ":""). $meritCount." ".$merite_title;
			}
			$export_content .= "<tr><td colspan='3' height='24'>&nbsp;</td><td align='right' nowrap='nowrap'>".$event_date." &nbsp; &nbsp;</td><td >".$record_obj["item"]."</td><td >{$item_details}</td><td >".$record_obj["Waive"]."</td><td >&nbsp;</td></tr>";
		}
	}
	$export_content .= "</table>";
	

	$export_content .= "</td></tr>";
	
	
	$export_content .= "<tr><td >";
	$export_content .= "<table width='270' border='0' cellpadding='0' cellspacing='5'><tr><td align='right' nowrap='nowrap'><font size='2'>".$Lang['eDiscipline']['sdbnsm_classTeacher_signature']."</td><td width='200' style='border-bottom:1px black solid;'>&nbsp;</td></tr>" .
						//"<tr><td colspan='2' height='3'><img src=''/images/space.gif' height='1'  width='1' border='0' /></td></tr>" .
						"<tr><td align='right' nowrap='nowrap' height='30' valign='bottom'><font size='2'>".$Lang['eDiscipline']['MunSangDate']."</td><td width='170' style='border-bottom:1px black solid;' align='center'>$TeacherSignDate</td></tr></table>";
	$export_content .= "</td><td align='right'>" .
			"<table width='270' border='0' cellpadding='0' cellspacing='5'><tr><td align='right' nowrap='nowrap'><font size='2'>".$Lang['eDiscipline']['MunSangSignOfParent']."</td><td width='200' style='border-bottom:1px black solid;'>&nbsp;</td></tr>" .
						//"<tr><td colspan='2' height='3'><img src=''/images/space.gif' height='1' width='1' border='0' /></td></tr>" .
						"<tr><td align='right' nowrap='nowrap' height='30' valign='bottom'><font size='2'>".$Lang['eDiscipline']['MunSangDate']."</td><td width='170' style='border-bottom:1px black solid;'>&nbsp;</td></tr></table>" .
			"</td></tr>";
	
	$export_content .= "</table>";
}
		
$export_content .= "</td>";
$export_content .= "</tr>";
$export_content .= "</table>";

intranet_closedb();

?>
<style type='text/css' media='print'>
 div#container {
	margin: 0 auto;
	width: 780px;
	height: 1120px
	}
</style>


<?php

echo $export_content;
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>