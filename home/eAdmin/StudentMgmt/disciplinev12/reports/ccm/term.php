<?php
// modifying by: Rita

/***************
 * Date: 	2013-01-14 (Rita)
 * Details:	add Date range
 * 
 ***************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# School Year Menu #
$SchoolYearID = Get_Current_Academic_Year_ID();

$years = $ldiscipline->returnAllYearsSelectionArray();
//$selectSchoolYear1Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear1ID' id='SchoolYear1ID' onChange='changeTerm(this.value, \"semester\")", "", $SchoolYearID);
$selectSchoolYear2Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYearID' id='SchoolYearID' onChange='changeTerm(this.value, \"semester\")'", "", $SchoolYearID);

//$selectSchoolYear1Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear1ID' onFocus='form1.award_punish_period[0].checked=true;' onChange='changeTerm(this.value, \"semester1\")'", $i_Discipline_System_Award_Punishment_All_School_Year, $SchoolYearID);
//$selectSchoolYear2Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear2ID' onFocus='form1.gdConduct_miscondict_period[0].checked=true;' onChange='changeTerm(this.value, \"semester2\")'", $i_Discipline_System_Award_Punishment_All_School_Year, $SchoolYearID);

# Semester Menu #
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);


# menu highlight setting
$CurrentPage = "Reports_CCM_TERM_REPORT";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($Lang['eDiscipline']['ccm_term_report'] );

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$i = 1;
?>
<script language="javascript">
function checkClickBox(obj, ref) {
	var field = "";
	if(obj.checked==false) {
		field = eval("form1." + ref);
		field.checked = false;
	}
}

function resetOption(obj, element_name) {
	var field = "";
	var len = document.form1.elements.length;
	if(obj.checked == true)	 {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) 
				document.form1.elements[i].checked = true;
		}
	} else {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) 
				document.form1.elements[i].checked = false;
		}
	}
}


var xmlHttp2
var xmlHttp3
function changeTerm(val, field) {
	xmlHttp2 = GetXmlHttpObject();
	if (xmlHttp2==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&field=" + field;
	
	if(field=='semester') {
		xmlHttp2.onreadystatechange = stateChanged2 
		xmlHttp2.open("GET",url,true)
		xmlHttp2.send(null)
	}
	
} 

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}


function goCheck(form1) {
	var flag = 0;
	var temp = "";
	var region1 = 0;
	var region2 = 0;
	var region3 = 0;
	var region4 = 0;
	var choiceSelected = 0;
	var choice = "";
	
	// region 1
	/*
	if(form1.all_award.checked==true || form1.all_punishment.checked==true) {
		region1 = 1;
		form1.award_punishment_flag.value = 1;
	}
	*/
	
	var len = form1.elements.length;
	for(i=0;i<len;i++) {
		//region2 = (form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) ? 1 : 0;
		//region2 = (form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) ? 1 : 0;
		if(form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}
		if(form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}/*
		if(form1.elements[i].name=='gdConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			form1.gdConduct.value = 1;
		}
		if(form1.elements[i].name=='misConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			form1.misconduct.value = 1;
		}*/
	}
	
	// region 2
	/*
	for(i=0;i<form1.merit.length;i++) {
		temp = eval("form1.merit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
	for(i=0;i<form1.demerit.length;i++) {
		temp = eval("form1.demerit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
*/
/*
	// region 4
	choiceSelected = 0;
	//choice = document.getElementById('goodconductid[]');
	choice = $('[name="goodconductid[]"]').get()[0];
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true) {
			region4 = 1;
			form1.gdConduct.value = 1;
		}
	}
	choiceSelected = 0;
	//choice = document.getElementById('misconductid[]');
	choice = $('[name="misconductid[]"]').get()[0];
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true) {
			region4 = 1;
			form1.misconduct.value = 1;
		}
	}
	*/
	
	if(form1.targetClass.value == '#') {
		alert("<?=$i_alert_pleaseselect?><?=$i_Discipline_Class?>");	
		return false;
	}
	
	/*
	if(region1==0 && region2==0 && region3==0 && region4==0) {
		alert("<?=$i_Discipline_System_Please_Select_Record?>");	
		return false;
	}
*/

	if(region1!=0 || region2!=0){
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value=='' && form1.Section1To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.award_punish_period[1].checked==true && ((!check_date(form1.Section1Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section1To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value > form1.Section1To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
	if(region3!=0 || region4!=0){
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value=='' && form1.Section2To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.gdConduct_miscondict_period[1].checked==true && ((!check_date(form1.Section2Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section2To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value > form1.Section2To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
/*	
	if(form1.all_gd_conduct.checked==true) {
		form1.gdConduct.value = 1;	
	}
	if(form1.all_misconduct.checked==true) {
		form1.misconduct.value = 1;	
	}
*/
//	alert(form1.award_punishment_flag.value + "/" + form1.gdConduct.value + "/" + form1.misconduct.value);
	return true;
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function DeSelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = false;
	}
}

</script>
<script language="javascript">
var xmlHttp

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("studentID").innerHTML = "";
			document.getElementById("studentID").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "../get_live.php";
	url = url + "?targetClass=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("studentID").innerHTML = xmlHttp.responseText;
		document.getElementById("studentID").style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<form name="form1" method="post" action="term_report_print.php" onSubmit="return goCheck(this)" target="eDis_term_report">
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<?= $ldiscipline->showWarningMsg($Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['Remark'], $Lang['eDiscipline']['ccm_term_report_desc']) ?>
			<table class="form_table_v30">
				<tr>
					<td valign="top" rowspan="2" nowrap="nowrap" class="field_title"><?=$i_Discipline_Class?> <span class="tabletextrequire">*</span></td>
					<td class="navigation">
						<?=$select_class?>
					</td>
				</tr>
				<tr valign="top">
					
					<td><div id='studentID' style='position:absolute; width:280px; height:20px; z-index:0;'></div>
						<select name="studentID" class="formtextbox"></select>
					</td>
				</tr>
				<tr valign="top">
					<td height="57" valign="top" nowrap="nowrap" class="field_title"><?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span></td>
					<td>
						<table border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td>
								<?=$linterface->Get_Radio_Button('PeriodType_YearTerm', 'PeriodType', '0', $isChecked=1);?>
								<?=$i_Discipline_School_Year?><?=$selectSchoolYear2Selection?>
								<?=$i_Discipline_Semester?> <span id="spanSemester"><select name='semester' onFocus="form1.gdConduct_miscondict_period[0].checked=true;"><?=$SemesterMenu?></select></span></td>
							</tr>
							<tr>
								<td>
								<?=$linterface->Get_Radio_Button('PeriodType_DateRange', 'PeriodType', '1', $isChecked=0);?>	
								<?=$i_From?> 
								<?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate)?>
								<?=$i_To?>
								<?=$linterface->GET_DATE_PICKER("textToDate",$textToDate)?>
								</td>
							</tr>
						</table>					
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$i_general_show?></td>
					<td width="80%">
						
						<input type="checkbox" name="NotShowStudentWithoutRecord" id="NotShowStudentWithoutRecord" value="1">
							<label for="NotShowStudentWithoutRecord"><?=$Lang['eDiscipline']['NotToShowStudentWithoutRecord']?></label>
						<br />
						<input type="checkbox" name="waive_record1" id="waive_record1" value="1">
							<label for="waive_record1"><?=$i_Discipline_System_Reports_Include_Waived_Record?></label>
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eNotice']['PrintDate']?></td>
					<td width="80%">
						
						<input type="text" name="PrintDate" id="PrintDate" value="<?=date("d/m/Y")?>" />
					
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eDiscipline']['ccm_term_report_print_term']?></td>
					<td width="80%">
						
						<input type="text" name="PrintTerm" id="PrintTerm" value="" />
					
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eDiscipline']['sdbnsm_classTeacher_signature']. $Lang['eDiscipline']['MunSangDate']?></td>
					<td width="80%">
						
						<input type="text" name="TeacherSignDate" id="TeacherSignDate" value="<?=date("d/m/Y")?>" />
					
					</td>
				</tr>
			</table>
		</td>
	</tr>

</table>
<div class="tabletextremark"><?=$i_general_required_field?></div>

<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit")?>
</div>

<input type="hidden" name="award_punishment_flag" value="">
<input type="hidden" name="gdConduct" value="">
<input type="hidden" name="misconduct" value="">
</form>
<script>
<!--

$('document').ready(function(){
	$('#textFromDate').click(function(){	
		$('#PeriodType_DateRange').attr('checked', 'checked');		
	});
	
	$('#textToDate').click(function(){		
		$('#PeriodType_DateRange').attr('checked', 'checked');	
	});

	$('#SchoolYearID').click(function(){		
		$('#PeriodType_YearTerm').attr('checked', 'checked');	
	});
	
	$('#semester').click(function(){		
		$('#PeriodType_YearTerm').attr('checked', 'checked');	
	});

});


function init()
{
	var obj = document.form1;
	
	// Class
	obj.targetClass.selectedIndex=3;	// First Class as default
	tempClass=obj.targetClass[obj.targetClass.selectedIndex].value;
	showResult(tempClass);
	
	// Semester
	changeTerm('<?=$SchoolYearID?>', 'semester');
	//setTimeout("changeTerm('<?=$SchoolYearID?>', 'semester2')",1000);

}


init();
//-->
</script>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
