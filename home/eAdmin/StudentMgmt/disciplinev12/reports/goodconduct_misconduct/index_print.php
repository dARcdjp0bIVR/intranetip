<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
//$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$lclass = new libclass();
$linterface = new interface_html();

$selectYear = ($selectYear == '') ? getCurrentAcademicYear() : $selectYear;

$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",", $SelectedFormText);

$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);


if ($Period == "YEAR") {
	$parPeriodField1 = $selectYear;
	$parPeriodField2 = $selectSemester;
} else {
	$parPeriodField1 = $textFromDate;
	$parPeriodField2 = $textToDate;
}

if ($rankTarget == "form") {
	$parByFieldArr = $SelectedFormTextArr;
} else {
	$parByFieldArr = $SelectedClassTextArr;
}

if($categoryID==1) {		# "Late"
	$parStatsFieldArr[0] = 0;
	//$parStatsFieldTextArr[0] = 'Late';
	$parStatsFieldTextArr[0] = $ldiscipline->getGMCatNameByCatID(1);
} else if($categoryID==2) {	# "Homework not submitted"
	
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
	
} else {
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
}

if($categoryID!=1 && $categoryID!=2) { 	
	$all_item_name = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID, 'name', $record_type);
}

### Table Data ###
$data = $ldiscipline->retrieveGoodMisReport($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID);
//debug_pr($data);

################################################
############ Report Table Content ##############

if($record_type == 1) {
	$i_MeritDemerit[1] = $i_Merit_Merit;
	$i_MeritDemerit[2] = $i_Merit_MinorCredit;
	$i_MeritDemerit[3] = $i_Merit_MajorCredit;
	$i_MeritDemerit[4] = $i_Merit_SuperCredit;
	$i_MeritDemerit[5] = $i_Merit_UltraCredit;
} else if($record_type == -1) {
	$i_MeritDemerit[0] = $i_Merit_Warning;
	$i_MeritDemerit[-1] = $i_Merit_BlackMark;
	$i_MeritDemerit[-2] = $i_Merit_MinorDemerit;
	$i_MeritDemerit[-3] = $i_Merit_MajorDemerit;
	$i_MeritDemerit[-4] = $i_Merit_SuperDemerit;
	$i_MeritDemerit[-5] = $i_Merit_UltraDemerit;
}
	
$itemTotal = array();
$columnMeritDemeritCount = array();
/*
$detail_table .= "<head>";
$detail_table .= "<link href='{$PATH_WRT_ROOT}/templates/{$LAYOUT_SKIN}/css/print.css' rel='stylesheet' type='text/css'>";
$detail_table .= "<style type='text/css'>";
$detail_table .= "<!--";
$detail_table .= ".print_hide {display:none;}";
$detail_table .= "-->";
$detail_table .= "</style>";
$detail_table .= "</head>";
*/
$detail_table .=  "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\" class='tableborder_print'>\n";
$detail_table .= "<tr class=\"tabletext row_print\">";
$detail_table .= "<td class=\"tabletext row_print\" align=\"right\" colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\">";
if($Period=="YEAR") {
	$detail_table .= $ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." ".$ldiscipline->getTermNameByTermID($parPeriodField2)."\n\n";
} else {
	$detail_table .= $iDiscipline['Period_Start']." ".$parPeriodField1." ".$iDiscipline['Period_End']." ".$parPeriodField2."\n\n";
}
//$generationDate = getdate();
$generationDate = date("Y/m/d H:i:s");
$detail_table .= "<br>".$eDiscipline['ReportGeneratedDate']." ".$generationDate;

$detail_table .= "</td></tr>";	
$detail_table .= "<tr class=\"tabletop_print tabletext\">";
	if($rankTarget=="form") {
		$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Form."</td>";	
	} else if($rankTarget=="class") {
		$detail_table .= "<td class=\"tabletop\">".$i_Discipline_Class."</td>";	
	} else {
		$detail_table .= "<td class=\"tabletop\">".$i_general_name."</td>";	
		$detail_table .= "<td class=\"tabletop\">".$i_ClassNumber."</td>";	
	}

# header of report table
for($i=0; $i<sizeof($parStatsFieldTextArr); $i++) {
	# style=\"writing-mode: tb-rl\"
	$css = ($i==0) ? "border_left" : "";
	$detail_table .= "<td class=\"$css\" align=\"center\">".intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))."</td>";	
}
$i = 0;
foreach($i_MeritDemerit as $tempMeritDemerit) {
	# style=\"writing-mode: tb-rl\"
	$css = ($i==0) ? "border_left" : "";
	$detail_table .= "<td align=\"center\" class=\"$css\">".$tempMeritDemerit."</td>";	
	$i++;
}
$detail_table .= "</tr>";
		/*
		if ($rankTarget == "form") {
			$sql = "SELECT CLS.ClassName FROM INTRANET_CLASS CLS LEFT OUTER JOIN INTRANET_CLASSLEVEL LVL ON (CLS.ClassLevelID=LVL.ClassLevelID) WHERE LVL.LevelName IN ('".implode('\',\'',$parByFieldArr)."')";
			$temp = $ldiscipline->returnVector($sql);
			$parByFieldArr = $temp;
		}
		*/

#content of report table
if($rankTarget != "student") {
	for($i=0; $i<sizeof($parByFieldArr); $i++) {						# for each class/form
		if($rankTarget=="form") {
			
			$css = ($i%2) ? "2" : "1";
			$detail_table .= "<tr class=\"row_print\">";
			
			$detail_table .= "<td class='tabletext row_print'>".stripslashes($parByFieldArr[$i])."</td>";
			
			for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {			# for each item
				$css = ($j==0) ? "border_left" : "";
				$detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class='tabletext row_print $css'>".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class='tabletext row_print $css'>0</td>";
				$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
			}
																				# (YEAR/DATE , Period1, Period2, Class/Form, ClassName/FormName, SelectedItemID, SelectedItemName)
			$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);
			
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				
				$k = 0;
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$css = ($k==0) ? "border_left" : "";
					$detail_table .= "<td align=\"center\" class='tabletext row_print $css'>";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
					$k++;
				}
				
			$detail_table .= "</tr>";
			
		} else {		# rankTarget = class
			
			//$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($parByFieldArr[$i]);
			
			//for($k=0; $k<sizeof($stdInfo); $k++) {
				$css = ($k%2) ? "2" : "1";
				$detail_table .= "<tr class=\"row_print\">";
				$detail_table .= "<td class='tabletext row_print'>".$parByFieldArr[$i]."</td>";
				//$detail_table .= "<td class='tabletext row_print'>".$parByFieldArr[$i]." - ".$stdInfo[$k][0]."</td>";
				
				for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {		# for each item
					$css2 = ($j==0) ? "border_left" : "";
					$detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? "<td align=\"center\" class='tabletext row_print $css2'>".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]."</td>" : "<td align=\"center\" class='tabletext row_print $css2 '>0</td>";
					$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
				}
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);
				
			
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				
				$a = 0;
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$css2 = ($a==0) ? "border_left" : "";
					$detail_table .= "<td align=\"center\" class='tabletext row_print $css2'>";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
					$a++;
				}
	
				$detail_table .= "</tr>";
			//}
			
		}
		
	}
} else {	# rankTarget = student
	
			for($k=0; $k<sizeof($studentID); $k++) {
				$stdInfo = $ldiscipline->getStudentNameByID($studentID[$k], $selectYear);
				//debug_r($stdInfo);
				$css = ($p%2) ? "2" : "1";
				$detail_table .= "<tr class=\"tabletext row_print\">";
				$detail_table .= "<td class=\"tabletext row_print\">".$stdInfo[0][1]."</td>";
				$detail_table .= "<td class=\"tabletext row_print\">".$stdInfo[0][2]." - ".$stdInfo[0][3]."</td>";
				
				for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {		# for each item
					$css = ($p%2==0) ? "" : "2";
					$css2 = ($j==0) ? "border_left" : "";
					
					$detail_table .= (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">".$data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]]."</td>" : "<td align=\"center\" class=\"tabletext tablerow_total".$css." ".$css2."\">0</td>";
					$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
				}
				$p++;
//				$detail_table .= "<td width=\"1\" class=\"tablebluetop\"><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"1\" height=\"1\"></td>";
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k]);
			
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				
				$a = 0;
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$css2 = ($a==0) ? "border_left" : "";
					$detail_table .= "<td align=\"center\" class='tabletext row_print $css2'>";
					$detail_table .= ($tempCount != 0) ? $tempCount : "0";
					$columnMeritDemeritCount[$m] += $tempCount;
					$detail_table .= "</td>";
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
					$a++;
				}

				$detail_table .= "</tr>";
			}
}

$detail_table .= "</tr>";

$detail_table .= "<tr colspan='tabletoplink'><td colspan=\"".(sizeof($parStatsFieldTextArr)+sizeof($i_MeritDemerit)+2)."\" class='tabletext row_print'><table cellspacing=0 cellpadding=0 width=100%><tr><td class='tabletext row_print'><img src=\"{$image_path}/{$LAYOUT_SKIN}\"/10x10.gif\" width=\"100%\" height=\"1\"></td></tr></table></td>";
$detail_table .= "<tr class='row_print'><td class='tabletext row_print'>".$i_Discipline_System_Report_Class_Total."</td>";
if($rankTarget=="student") {
	$detail_table .= "<td class='tabletext row_print'>&nbsp;</td>";
}
for($i=0; $i<sizeof($parStatsFieldTextArr); $i++) {
	$css = ($i==0) ? "border_left" : "";
	$detail_table .= (isset($itemTotal[$parStatsFieldArr[$i]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))])) ? "<td align=\"center\" class='tabletext row_print $css'>".$itemTotal[$parStatsFieldArr[$i]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$i]))]."</td>" : "<td align=\"center\" class='tabletext row_print $css'>0</td>";
}

if($record_type==1) {
	$m = 1;
} else {
	$m = 0;
}
$k = 0;
foreach($i_MeritDemerit as $columnNo) {
	$css = ($k==0) ? "border_left" : "";
	$detail_table .= "<td align=\"center\" class='tabletext row_print $css'>";
	$detail_table .= $columnMeritDemeritCount[$m];
	
	if($record_type==1) {
		$m++;
	} else {
		$m--;
	}
	$detail_table .= "</td>";
	$k++;
}


$detail_table .= "</tr>";

$detail_table .=  "</table>\n";

############ End of Report Table ###############
################################################
	
?>


<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" class='print_hide'>
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
	<tr>
		<td class='tabletext'>
			<?=$eDiscipline['GoodConductMisconductReport']?>
		</td>
	</tr>
	<tr>
		<td>
			<?=$detail_table?>
		</td>
	</tr>
</table>
<?
intranet_closedb();

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

?>