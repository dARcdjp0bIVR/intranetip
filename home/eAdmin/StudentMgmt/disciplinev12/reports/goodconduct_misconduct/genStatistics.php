<?php

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/flashchart_basic/php-ofc-library/open-flash-chart.php");

header("Content-Type:text/html;charset=BIG5");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# raw data
	$parStatsFieldArr = explode(',', $parStatsFieldArr);
	$Period;
	$parByFieldArr = explode(',', $parByFieldArr);
	$parPeriodField1;
	$parPeriodField2;
	$selectBy;
	$categoryID;
	$parStatsFieldTextArr = explode(',', $parStatsFieldTextArr);

# chart data
	$data = $ldiscipline->retrieveGoodMisReport($Period, $parPeriodField1, $parPeriodField2, $selectBy, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);

	$chart = new open_flash_chart();
	$maxValue = 0;
	$i = 0;
	
$title = new title( "2007-2008 No. of Merits and Demerits of Form 1 Statistics" );
$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );

$x_legend = new x_legend( 'class' );
$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$x = new x_axis();
$x->set_stroke( 2 );
$x->set_tick_height( 2 );
$x->set_colour( '#999999' );
$x->set_grid_colour( '#CCCCCC' );
$x->set_labels_from_array( array('1A','1B','1C','1D','1E','1F') );

$y_legend = new y_legend( 'Quantity' );
$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );

$y = new y_axis();
$y->set_stroke( 2 );
$y->set_tick_length( 2 );
$y->set_colour( '#999999' );
$y->set_grid_colour( '#CCCCCC' );
$y->set_range( 0, 100, 5 );
$y->set_offset(true);

$bar0 = new bar_stack_group();
$bar0->set_values( array(12,16,7,10,5,18) );
$bar0->set_colour( '#72a9db' );
$bar0->set_tooltip( '123 : #val#' );
$bar0->set_key( '123' , '12' );
$bar0->set_id( 0 );
$bar0->set_visible( true );

$bar1 = new bar_stack_group();
$bar1->set_values( array(5,19,12,16,9,3) );
$bar1->set_colour( '#eb593b' );
$bar1->set_tooltip( 'Minar Merit:#val#' );
$bar1->set_key( 'Minar Merit', '12' );
$bar1->set_id( 1 );
$bar1->set_visible( true );

$bar2 = new bar_stack_group();
$bar2->set_values( array(18,4,16,6,16,5) );
$bar2->set_colour( '#aaaaaa' );
$bar2->set_tooltip( 'Major Merit:#val#' );
$bar2->set_key( 'Major Merit', '12' );
$bar2->set_id( 2 );
$bar2->set_visible( false );

$bar3 = new bar_stack_group();
$bar3->set_values( array(14,8,11,13,7,14) );
$bar3->set_colour( '#92d24f' );
$bar3->set_tooltip( 'Black Point:#val#' );
$bar3->set_key( 'Black Point', '12' );
$bar3->set_id( 3 );
$bar3->set_visible( true );

$bar4 = new bar_stack_group();
$bar4->set_values( array(16,7,5,11,17,12) );
$bar4->set_colour( '#eaa325' );
$bar4->set_tooltip( 'Minar Demerit:#val#' );
$bar4->set_key( 'Minar Demerit', '12' );
$bar4->set_id( 4 );
$bar4->set_visible( false );

$bar5 = new bar_stack_group();
$bar5->set_values( array(11,8,13,11,16,5) );
$bar5->set_colour( '#f0e414' );
$bar5->set_tooltip( 'Major Demerit:#val#' );
$bar5->set_key( 'Major Demerit', '12' );
$bar5->set_id( 5 );
$bar5->set_visible( true );

$tooltip = new tooltip();
$tooltip->set_hover();

$chart = new open_flash_chart();
$chart->set_bg_colour( '#FFFFFF' );
$chart->set_title( $title );
$chart->set_x_legend( $x_legend );
$chart->set_x_axis( $x );
$chart->set_y_legend( $y_legend );
$chart->set_y_axis( $y );
$chart->add_element( $bar0 );
$chart->add_element( $bar1 );
$chart->add_element( $bar2 );
$chart->add_element( $bar3 );
$chart->add_element( $bar4 );
$chart->add_element( $bar5 );
$chart->set_tooltip( $tooltip );

# prepare the bars
/*
	foreach ($parStatsFieldTextArr as $statsValue) 	# selected ItemID	-- original
	{
		$statsUnicodeValue = intranet_undo_htmlspecialchars(stripslashes(Big5ToUnicode($statsValue)));
		$statsValue = stripslashes($statsValue);
		
		$valueArr = array();
		for($j=0;$j<sizeof($valueArr);$j++) {
			$valueArr[$j] = "";
		}
		
		$bar = new bar_stack_group();
		$bar->set_colour("$colourArr[$i]");
		# remember the column colour for printing
		$itemNameColourMappingArr[$statsValue] = $colourArr[$i];
		
		$j = 0;
		foreach ($parByFieldArr as $byValue) {		# selected class/form
			$tmpValue = (int)$chartDataArr[$byValue][$statsValue];
			
			$valueArr[] = $tmpValue;
			if ($tmpValue > $maxValue) $maxValue = $tmpValue;	
			$bar->set_values($valueArr);
			$j++;
		}
		
		$bar->set_tooltip(intranet_htmlspecialchars($statsUnicodeValue).":#val#");	# x-axis item 
		$bar->set_key("$statsUnicodeValue", "12");
		$bar->set_id( $parStatsFieldArr[$i] );
		$flag = 0;
		
		if($categoryID==1 || $categoryID==2) {
			$bar->set_visible( true );
		} else {
			foreach ($parStatsFieldTextArr as $statsValue2) {
				$statsValue2 = stripslashes($statsValue2);				
				if($flag==0) {
					if($statsValue==$statsValue2) {
						$bar->set_visible( true );
						$flag = 1;
					} else {
						$bar->set_visible( false );
					}
				}
			}
		}
		$chart->add_element($bar);
		$i++;
	}
# chart properties #
	
	if ($Period == "YEAR") {
		$titleString = $selectYear." ".$selectSemester." ";
	} else {
		$titleString = $i_From." ".$textFromDate." ".$i_To." ".$textToDate." ";
	}
	$titleString .= $eDiscipline['Good_Conduct_and_Misconduct_Statistics'];
	$title = new title( Big5ToUnicode($titleString) );
	$title->set_style( "{font-size: 16px; font-family: _sans; color: #333333; font-weight:bold; text-align:center;}" );
	
	$x_legend = new x_legend( 'class' );
	$x_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
	
	$x = new x_axis();
	$x->set_stroke( 2 );
	$x->set_tick_height( 2 );
	$x->set_colour( '#999999' );
	$x->set_grid_colour( '#CCCCCC' );
	$x->set_labels_from_array($parByFieldArr);
	
	$y_legend = new y_legend( Big5ToUnicode($i_Discipline_Quantity) );
	$y_legend->set_style( '{font-size: 12px; font-weight: bold; color: #1e9dff}' );
	
	$y = new y_axis();
	$y->set_stroke( 2 );
	$y->set_tick_length( 2 );
	$y->set_colour( '#999999' );
	$y->set_grid_colour( '#CCCCCC' );
	$y->set_range( 0, 100, 5 );
	$y->set_offset(true);	# true / false
	
	$tooltip = new tooltip();
	$tooltip->set_hover();
	
	########
	$colourArr = array('#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414', '#72a9db', '#eb593b', '#aaaaaa', '#92d24f', '#eaa325', '#f0e414');
	
	$chart->set_tooltip($tooltip);
	
	$key = new key_legend();
	$key->set_selectable(true);
	
	########
	
	$chart->set_bg_colour( '#FFFFFF' );
	$chart->set_title( $title );
	$chart->set_x_legend( $x_legend );
	$chart->set_x_axis( $x );
	$chart->set_y_legend( $y_legend );
	$chart->set_y_axis( $y );
	
	$chart->add_element( $bar0 );
	$chart->add_element( $bar0 );
	$chart->add_element( $bar1 );
	$chart->add_element( $bar2 );
	$chart->add_element( $bar3 );
	$chart->add_element( $bar4 );
	$chart->add_element( $bar5 );
	
	$chart->set_tooltip( $tooltip );
	$chart->set_key_legend( $key );
*/
# chart content (generate content)
	# Adjust the height of flash depends on the no. of items
	$height = 350;
	
	# Adjust the width of flash depends on the no. of class/form
	$fixedWidthForAcceptableItem = 5;
	$width = 850 + ((sizeof($parByFieldArr)+sizeof($parStatsFieldTextArr))-$fixedWidthForAcceptableItem) * 5;	
	
	$chartContent = "<table width=\"88%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\" class=\"result_box\">";
	$chartContent .= "<tr><td align=\"center\">";

	$chartContent .= "<!--\n";
	$chartContent .= "############################################### flash chart ##################################################################\n";
	$chartContent .= "-->\n";
	$chartContent .= "<div id=\"my_chart\">no flash?</div>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/json/json2.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\" src=\"".$PATH_WRT_ROOT."includes/flashchart_basic/js/swfobject.js\"></script>\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	$chartContent .= "	swfobject.embedSWF(\"".$PATH_WRT_ROOT."includes/flashchart_basic/open-flash-chart.swf\", \"my_chart\", \"".$width."\", \"".$height."\", \"9.0.0\", \"expressInstall.swf\");\n";
	$chartContent .= "</script>\n";
	$chartContent .= "\n";
	$chartContent .= "<script type=\"text/javascript\">\n";
	$chartContent .= "function ofc_ready(){\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function open_flash_chart_data(){\n";
	$chartContent .= "	//alert( 'reading data' );\n";
	$chartContent .= "	return JSON.stringify(data);\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	$chartContent .= "function findSWF(movieName) {\n";
	$chartContent .= "  if (navigator.appName.indexOf(\"Microsoft\")!= -1) {\n";
	$chartContent .= "	return window[movieName];\n";
	$chartContent .= "  } else {\n";
	$chartContent .= "	return document[movieName];\n";
	$chartContent .= "  }\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	/*
	$chartContent .= "function expandSubLevel(id){\n";
	$chartContent .= "var popup = document.getElementById(\"testPopup\");\n";
	$chartContent .= "while(popup.hasChildNodes()){\n";
	$chartContent .= "popup.removeChild(popup.firstChild);\n";
	$chartContent .= "}\n";
	$chartContent .= "popupDetail(id);\n";
	$chartContent .= "}\n";
	$chartContent .= "\n";
	
	$chartContent .= "function popupDetail(id) {\n";
	$chartContent .= "document.frmStats.id.value = id;";
	$chartContent .= "document.frmStats.action = 'popupDetail.php';";
	$chartContent .= "document.frmStats.target = '_blank';";
	$chartContent .= "document.frmStats.submit();";
	$chartContent .= "document.frmStats.action = '';";
	$chartContent .= "document.frmStats.target = '';";
	$chartContent .= "}\n\n";
	*/
	$chartContent .= "var itemID = new Array(); var newItemID = new Array();\n";
	$chartContent .= "itemID = [".implode(",", $parStatsFieldArr)."];\n\n";	
	
	$chartContent .= "function setChart(id, display) {\n";
	$chartContent .= "var j = 0;\n";
	$chartContent .= "var itemIDList = \"\";\n";
	$chartContent .= "newItemID = [];\n";	# initiate newItemID[]
	$chartContent .= "for(var i=0;i<itemID.length;i++) {\n";
	$chartContent .= "	if(display==true) {\n";
	$chartContent .= "		if(i==0) { newItemID[j] = id;\n j++;\n}\n";
	$chartContent .= "		newItemID[j] = itemID[i];\n j++;\n";			
	$chartContent .= "	} else {\n";
	$chartContent .= "		if(id != itemID[i])	{\n";
	$chartContent .= "			newItemID[j] = itemID[i];\n j++;\n";
	$chartContent .= "		}\n";
	$chartContent .= "	}\n";
	$chartContent .= "}\n";
	$chartContent .= "if(itemID.length==0){newItemID[j] = id;\n j++;\n} \n";	
	$chartContent .= "itemID = [];\n";
	$chartContent .= "for(i=0;i<newItemID.length;i++) {\n";
	$chartContent .= "		itemID[i] = newItemID[i];\n";
	$chartContent .= "		itemIDList += (itemIDList != \"\") ? \",\" + newItemID[i] : newItemID[i];\n";
	$chartContent .= "}\n";
	$chartContent .= "document.getElementById('newItemID').value = itemIDList;\n";
	
	$chartContent .= "}\n";
	
	$chartContent .= "\n";
	$chartContent .= "var data = ".$chart->toPrettyString().";\n";
	$chartContent .= "</script>\n";
	$chartContent .= "<!--\n";
	$chartContent .= "############################################ END - flash chart ###############################################################\n";
	$chartContent .= "-->\n";

	$chartContent .= "</td></tr>";
	$chartContent .= "<tr><td height=\"1\" class=\"dotline\"><img src=\"$image_path/$LAYOUT_SKIN/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
	$chartContent .= "<tr><td align=\"right\">";
	$chartContent .= "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
	$chartContent .= "<tr><td align=\"center\">";
	$chartContent .= $linterface->GET_ACTION_BTN($button_print_statistics, "button", "printStats();");
	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";
	$chartContent .= "</td></tr>";
	$chartContent .= "</table>";


intranet_closedb();

echo $chartContent;
?>