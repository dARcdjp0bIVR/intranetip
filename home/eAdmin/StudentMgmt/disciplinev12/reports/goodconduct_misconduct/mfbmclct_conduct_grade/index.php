<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# Year Menu 
$currentAcademicYearID = Get_Current_Academic_Year_ID();

########################################################

if($flag == 1) {
	$optionString = "<td id=\"tdOption\" class=\"tabletextremark\"><span id=\"spanShowOption\"><a href=\"javascript:showOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_show_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Show_Statistics_Option</a></span>";
	$optionString .= "<span id=\"spanHideOption\" style=\"display:none\"><a href=\"javascript:hideOption();\" class=\"contenttool\"><img src=\"$image_path/$LAYOUT_SKIN/icon_hide_option.gif\" width=\"20\" height=\"20\" border=\"0\" align=\"absmiddle\">$i_Discipline_Hide_Statistics_Option</a></span></td>";
	$initialString = "<script language=\"javascript\">hideOption();</script>\n";

	$studentIDAry = array();
	foreach($target as $val) {
		if($level == "0") {	# class
			$tempStudentAry = $ldiscipline->storeStudent(0,'::'.$val);	
		} else {
			$tempStudentAry = $ldiscipline->storeStudent(0,$val);	
		}
		if(sizeof($tempStudentAry)>0) $studentIDAry = array_merge($studentIDAry, $tempStudentAry);
	}
	
	$exportContent = "";
	
	$rawDataAry = array();
	$rawDataAry['startDate1'] = $startDate1;
	$rawDataAry['endDate1'] = $endDate1;
	$rawDataAry['startDate2'] = $startDate2;
	$rawDataAry['endDate2'] = $endDate2;
	$rawDataAry['startDate3'] = $startDate3;
	$rawDataAry['endDate3'] = $endDate3;
	$rawDataAry['level'] = $level;
	$rawDataAry['target'] = $target;
	
	$tableContent = $ldiscipline->getMisconductSpecificItemReport($studentIDAry, $rawDataAry);
	
} else {
	$optionString = "<td height=\"20\" class=\"tabletextremark\"><em>- $i_Discipline_Statistics_Options -</em></td>";
}
########################################################

$TAGS_OBJ[] = array($Lang['eDiscipline']['MFBMCLCT_ConductGradeReport'],"");

# menu highlight setting
$CurrentPage = "MFBMCLCT_ConductGradeReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>

<script language="javascript">
function changeType(form_obj,lvl_value)
{
         obj = form_obj.elements["target[]"]
         <? # Clear existing options
         ?>
         while (obj.options.length > 0)
         {
                obj.options[0] = null;
         }
         if (lvl_value==1)
         {
             <?
             for ($i=0; $i<sizeof($levels); $i++)
             {
                  list($id,$name) = $levels[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
                  <?

             }
             ?>
         }
         else
         {
             <?
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  list($id,$name, $lvl_id) = $classes[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
                  <?

             }
             ?>
         }
//	SelectAll(form1.elements['target[]']);

}
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}


function doExport()
{
	document.form1.action = "export.php";
	document.form1.submit();
	document.form1.action = "";
}
function doPrint()
{
	document.form1.action = "print.php";
	document.form1.target = "_blank";
	document.form1.submit();
	document.form1.action = "";
	document.form1.target = "";
}
function view()
{
	if (checkform())
	{
		document.form1.action = "";
		document.form1.submit();
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}


function checkform()
{
	var select_obj = document.getElementById('target[]');

	if ((document.getElementById('startDate1').value=='' && document.getElementById('endDate1').value!='') || (document.getElementById('startDate1').value!='' && document.getElementById('endDate1').value=='')) {
		alert("<?=$i_alert_pleasefillin.$Lang['eDiscipline']['1stPeriod'].' '.$i_general_startdate.'/'.$i_general_enddate?>");
		return false;
	}
	if ((document.getElementById('startDate2').value=='' && document.getElementById('endDate2').value!='') || (document.getElementById('startDate2').value!='' && document.getElementById('endDate2').value=='')) {
		alert("<?=$i_alert_pleasefillin.$Lang['eDiscipline']['2ndPeriod'].' '.$i_general_startdate.'/'.$i_general_enddate?>");
		return false;
	}
	if ((document.getElementById('startDate3').value=='' && document.getElementById('endDate3').value!='') || (document.getElementById('startDate3').value!='' && document.getElementById('endDate3').value=='')) {
		alert("<?=$i_alert_pleasefillin.$Lang['eDiscipline']['3rdPeriod'].' '.$i_general_startdate.'/'.$i_general_enddate?>");
		return false;
	}
	if(!check_date_allow_null(document.getElementById('startDate1'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(!check_date_allow_null(document.getElementById('endDate1'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(document.form1.startDate1.value > document.form1.endDate1.value) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(!check_date_allow_null(document.getElementById('startDate2'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(!check_date_allow_null(document.getElementById('endDate2'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(document.form1.startDate2.value > document.form1.endDate2.value) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	if(!check_date_allow_null(document.getElementById('startDate3'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(!check_date_allow_null(document.getElementById('endDate3'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(document.form1.startDate3.value > document.form1.endDate3.value) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}

	alert('<?=$i_Discipline_System_alert_PleaseSelectClassLevel?>');
	return false;
}
</script>
<script language="javascript">

var xmlHttp

function showResult(str)
{
	if (str.length==0)
		{ 
			document.getElementById("rankTargetDetail").innerHTML = "";
			document.getElementById("rankTargetDetail").style.border = "0px";
			return
		}
		
	xmlHttp = GetXmlHttpObject()
	
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 

	var url = "";
		
	url = "../../get_live2.php";
	url = url + "?target=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById("rankTargetDetail").innerHTML = xmlHttp.responseText;
		document.getElementById("rankTargetDetail").style.border = "0px solid #A5ACB2";
	} 
}

var xmlHttp3

function changeClassForm() {
	
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	var url = "";
		
	url = "../../ajaxChangeClass.php";
	url += "?year=" + <?=$currentAcademicYearID?>;
	url += "&selectedTarget="+document.getElementById('selectedTarget').value;
	url += "&level=" + document.getElementById('level').value;
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
	
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanTarget").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanTarget").style.border = "0px solid #A5ACB2";
	} 
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

</script>
<br/>
<form name="form1" action="" method="POST">
<table width="90%" border="0" cellspacing="0" cellpadding="3" align="center">
	<tr align="left" <? if($_POST["submit_flag"]=="YES") echo "class=\"report_show_option\""; ?>>
		<?=$optionString?>
	</tr>
</table>
<br>
<span id="spanOptionContent">
<table width="90%" border="0" cellpadding="4" cellspacing="0">
	<tr class="tabletext">
		<td width="20%" class="formfieldtitle" valign="top"><?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span></td>
		<td>
			<table border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td><?=$Lang['eDiscipline']['1stPeriod']." : "?></td><td><?=$linterface->GET_DATE_PICKER("startDate1",$startDate1)."&nbsp;".$i_To."&nbsp;".$linterface->GET_DATE_PICKER("endDate1",$endDate1)?></td>
				</tr>
				<tr>
					<td><?=$Lang['eDiscipline']['2ndPeriod']." : "?></td><td><?=$linterface->GET_DATE_PICKER("startDate2",$startDate2)."&nbsp;".$i_To."&nbsp;".$linterface->GET_DATE_PICKER("endDate2",$endDate2)?></td>
				</tr>
				<tr>
					<td><?=$Lang['eDiscipline']['3rdPeriod']." : "?></td><td><?=$linterface->GET_DATE_PICKER("startDate3",$startDate3)."&nbsp;".$i_To."&nbsp;".$linterface->GET_DATE_PICKER("endDate3",$endDate3)?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?="$i_Discipline_Class/$i_Discipline_Form"?></td>
		<td>
			<SELECT name="level" id='level' onChange="changeType(this.form,this.value);changeClassForm()">
				<OPTION value="0" <?=$level!=1?"SELECTED":""?>><?=$i_Discipline_Class?></OPTION>
				<OPTION value="1" <?=$level==1?"SELECTED":""?>><?=$i_Discipline_Form?></OPTION>
			</select>
			<br><br>
			<span id='spanTarget'></span>
			<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('target[]'))"); ?>
		</td>
	</tr>
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "button", "javascript:view()")?>&nbsp;
		</td>
	</tr>
</table>
</span>
<?=$initialString?>
<br>
<input type="hidden" name="flag" value="1" />
<input type="hidden" name="targetDivID" id="targetDivID" />
<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
<input type="hidden" name="ajaxPeriod" id="ajaxPeriod" />
<input type="hidden" name="ajaxDate1" id="ajaxDate1" />
<input type="hidden" name="ajaxDate2" id="ajaxDate2" />
<input type="hidden" name="ajaxLevel" id="ajaxLevel" />
<input type="hidden" name="ajaxTarget" id="ajaxTarget" />
<input type="hidden" name="ajaxRecordType" id="ajaxRecordType" />
<input type="hidden" name="ajaxType" id="ajaxType" />
<input type="hidden" name="selectedTarget" id="selectedTarget" value="<? if(sizeof($target)>0) { echo implode(',',$target); }?>">
<input type="hidden" name="title" value="<?=urlencode($Lang['eDiscipline']['MFBMCLCT_ConductGradeReport'])?>" />
<input type="hidden" name="content" value='<?=urlencode($tableContent)?>' />
<?
	if ($flag == 1)
	{
		?>
		<?=$tableContent?>

		<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">

			<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr>
				<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()");?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:doPrint()");?>
				<div id="div_form"></div>
				</td>
			</tr>
		</table>

		</p>
		<?
	}
?>
<script language="javascript">
changeClassForm();
<? if($flag==1) { ?>	
	checkDateNull(document.getElementById('startDate1'), '<?=$startDate1?>');
	checkDateNull(document.getElementById('startDate2'), '<?=$startDate2?>');
	checkDateNull(document.getElementById('startDate3'), '<?=$startDate3?>');
	checkDateNull(document.getElementById('endDate1'), '<?=$endDate1?>');
	checkDateNull(document.getElementById('endDate2'), '<?=$endDate2?>');
	checkDateNull(document.getElementById('endDate3'), '<?=$endDate3?>');
<? } ?>

function checkDateNull(f, val) {
	if(val=='')	{
		f.value = "";	
	}
}
</script>
</form>

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
