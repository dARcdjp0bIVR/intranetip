<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$lexport = new libexporttext();

$filename = "weekly_report_report.csv";

if ($g_encoding_unicode) {
	
	$Temp = explode("\n", urldecode($exportContent));
	$exportColumn = explode("\t", trim($Temp[0], "\r"));
	for ($i = 1; $i < sizeof($Temp); $i++) {
		$result[] = explode("\t", trim($Temp[$i], "\r"));
	}
	
	$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn);
	$lexport->EXPORT_FILE($filename, $export_content);
	
} else {
	$export_content .= stripslashes($exportContent);	
	$lexport->EXPORT_FILE($filename, $export_content);
}

intranet_closedb();
?>