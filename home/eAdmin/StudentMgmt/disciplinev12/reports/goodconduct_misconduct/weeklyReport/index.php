<?php
# using: henry
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

/*
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

$currentAcademicYearID = Get_Current_Academic_Year_ID();


$interval = ($interval=='') ? 5 : $interval;
$intervalMenu = "<select name='interval' id='interval'>";
for($i=1; $i<=60; $i++) {
	$intervalMenu .= "<option value='$i'";
	$intervalMenu .= ($i==$interval) ? " selected" : "";
	$intervalMenu .= ">$i</option>";	
}
$intervalMenu .= "</select>";

######## Category & Item selection menu #############
$good_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(-1);
$items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT('',1);
$good_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(1,0);
$mis_items = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(-1,1);

$all_item = array();

$categoryID = ($record_type==1) ? $selectGoodCat : $selectMisCat;

if($categoryID==1) {	 # "Late"
	$all_item[0] = 0;	
} else {
	$all_item = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM($categoryID,'id', $record_type);
}

# Good Conduct Category
$selectGoodCatHTML = "<select name=\"selectGoodCat\" id=\"selectGoodCat\" onChange=\"changeCat(this.value);\">\n";
$selectGoodCatHTML .= "<option value=\"0\"";
$selectGoodCatHTML .= ($submitBtn != "")?" selected":"";
$selectGoodCatHTML .= ">-- ".$i_Discipline_System_Reports_All_Good_Conducts." --</option>";
for ($i = 0; $i < sizeof($good_cats); $i++) {
	$selectGoodCatHTML .= "<option value=\"".$good_cats[$i][0]."\"";
	if ($selectGoodCat == $good_cats[$i][0]) {
		$selectGoodCatHTML .= " selected";
	}
	$selectGoodCatHTML .= ">".$good_cats[$i][1]."</option>\n";
}
$selectGoodCatHTML .= "</select>\n";

# Misconduct Category
$selectMisCatHTML = "<select name=\"selectMisCat\" id=\"selectMisCat\" onChange=\"changeCat(this.value);\">\n";
$selectMisCatHTML .= "<option value=\"0\"";
$selectMisCatHTML .= ($submitBtn != "")?" selected":"";
$selectMisCatHTML .= ">-- $i_Discipline_System_Reports_All_Misconduct --</option>";
for ($i = 0; $i < sizeof($mis_cats); $i++) {
	$selectMisCatHTML .= "<option value=\"".$mis_cats[$i][0]."\"";
	if ($selectMisCat == $mis_cats[$i][0]) {
		$selectMisCatHTML .= " selected";
	}
	$selectMisCatHTML .= ">".$mis_cats[$i][1]."</option>\n";
}
$selectMisCatHTML .= "</select>\n";

# Items
if($record_type==1) {
	$selectedCat = $selectGoodCat;
	$selectedItem = $good_items;	
} else {
	$selectedCat = $selectMisCat;
	$selectedItem = $mis_items;	
}

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode("\',\'", substr($SelectedItemIDText,2,-2));


$selectItemIDHTML = "<select name=\"ItemID[]\" id=\"ItemID[]\" multiple size=\"5\">\n";
if ($submitBtn == "") {
	for ($i = 0; $i < sizeof($selectedItem); $i++) {
		list($r_catID, $r_itemID, $r_itemName) = $selectedItem[$i];
		if (($record_type == 1 && $selectGoodCat == $r_catID) || ($record_type == -1 && $selectMisCat == $r_catID) || ($selectGoodCat==0 && $selectMisCat==0)) {
			$selectItemIDHTML .= "<option value=\"".$r_itemID."\"";
			if (in_array(addslashes(intranet_undo_htmlspecialchars($r_itemName)), $SelectedItemIDTextArr) && in_array(addslashes(intranet_undo_htmlspecialchars($r_itemID)), $SelectedItemIDArr)) {
				$selectItemIDHTML .= " selected";
			}
			$selectItemIDHTML .= ">".$r_itemName."</option>\n";
		}
	}
}
$selectItemIDHTML .= "</select>\n";
######## Category & Item selection menu (End) #############

########################################################
# Get Student List
if($flag==1) {

	$result = $ldiscipline->retrieveGMWeeklyReportIntervalBase($startDate, $endDate, $record_type, $interval, $selectedCat, $SelectedItemIDArr, $SelectedItemIDTextArr);
	
	$tableContent = "";
	$export_content = "";
	
	# table header
	$tableContent .= "<table width='98%' cellpadding='4' cellspacing='0' border='0'>";
	$tableContent .= "	<tr class='tablebluetop'>";
	$tableContent .= "		<td class='tabletopnolink' width='30%'>$i_general_class</td>";
	$tableContent .= "		<td class='tabletopnolink' width='20%'>$i_general_name</td>";
	$tableContent .= "		<td class='tabletopnolink' width='20%' align='center'>".$eDiscipline["RecordDetails"]."</td>";
	$tableContent .= "		<td class='tabletopnolink' width='15%' align='center'>".$Lang['eDiscipline']['MeetInterval']." (".$Lang['eDiscipline']['TotalTimes'].")</td>";
	$tableContent .= "		<td class='tabletopnolink' width='15%' align='center'>$i_Discipline_System_Conduct_CurrentScore</td>";
	$tableContent .= "	</tr>";
	
	# export header
	$export_content .= "&quot;$i_ClassName&quot;\t";
	$export_content .= "&quot;$i_ClassNumber&quot;\t";
	$export_content .= "&quot;$i_general_name&quot;\t";
	$export_content .= "&quot;".$Lang['eDiscipline']['MeetInterval']."&quot;\t";
	$export_content .= "&quot;(".$Lang['eDiscipline']['TotalTimes'].")&quot;\t";
	$export_content .= "&quot;$i_Discipline_System_Conduct_CurrentScore&quot;\t";
	$export_content .= "&quot;".$eDiscipline['RecordDetails']." ($i_Discipline_Reason)&quot;\t";
	$export_content .= "&quot;($i_Discipline_Reason)&quot;\t";
	$export_content .= "&quot;($i_EventDate)&quot;\n";
	
	if(sizeof($result)==0) {
		$tableContent .= "	<tr><td colspan='5' class='tabletext' align='center' height='80'>$i_no_record_exists_msg</td></tr>";
		
		$export_content .= "&quot;$i_no_record_exists_msg&quot;\n";
	} else {
		for($i=0; $i<sizeof($result); $i++)	 {
			list($sid, $name, $cls, $clsNo, $date, $total) = $result[$i];
			$cs = $ldiscipline->retrieveConductScoreBalance($sid);
			$conductScore = ($cs) ? $cs : $ldiscipline->getConductMarkRule('baseMark');
			$remainder = $total % $interval;
			$meetIntervalValue = $total - $remainder;
			
			
			# need RELEASED?
			$sql = "SELECT SUM(ConductScoreChange) FROM DISCIPLINE_MERIT_RECORD WHERE StudentID=$sid AND RecordStatus=".DISCIPLINE_STATUS_APPROVED." AND ReleaseStatus=".DISCIPLINE_STATUS_RELEASED." AND RecordDate BETWEEN '$startDate' AND '$endDate'";	
			$change = $ldiscipline->returnVector($sql);
			
			$conductScoreChange = $conductScore + $change[0];
			
			# table content 
			$css = ($i%2==0) ? 1 : 2;
			$tableContent .= "<tr class='tablebluerow$css'>";
			$tableContent .= "<td class='tabletext'>$cls - $clsNo</td>";	
			$tableContent .= "<td class='tabletext'>$name</td>";	
			$tableContent .= "<td class='tabletext' align='center'><a href=\"javascript:Show_Detail_Window('$sid','$currentAcademicYearID','$startDate','$endDate', $record_type)\" class='tablelink'>
								<div id='RecordDateDetail_$sid'>".$iDiscipline['Details']."</div></a></td>";	
			$tableContent .= "<td class='tabletext' align='center'>$meetIntervalValue ($total)</td>";	
			$tableContent .= "<td class='tabletext' align='center'>$conductScoreChange</td>";	
			$tableContent .= "</tr>";

			
			# export content
			$tempDate = $ldiscipline->getRecordInfoInLayerByDateRange($sid, $currentAcademicYearID, $startDate, $endDate, $record_type, $selectedCat, $SelectedItemIDArr, $SelectedItemIDTextArr);
			
			$temp_export_content = "";

			$export_content .= "&quot;$cls&quot;\t";
			$export_content .= "&quot;$clsNo&quot;\t";
			$export_content .= "&quot;".intranet_htmlspecialchars($name)."&quot;\t";
			$export_content .= "&quot;$meetIntervalValue&quot;\t";
			$export_content .= "&quot;$total&quot;\t";
			$export_content .= "&quot;$conductScoreChange&quot;\t";
			for($k=0;$k<sizeof($tempDate);$k++) { 
				if($k!=0) {
					$temp_export_content .= "&quot;&quot;\t";
					$temp_export_content .= "&quot;&quot;\t";
					$temp_export_content .= "&quot;&quot;\t";
					$temp_export_content .= "&quot;&quot;\t";
					$temp_export_content .= "&quot;&quot;\t";
					$temp_export_content .= "&quot;&quot;\t";
				}
				$temp_export_content .= "&quot;".$tempDate[$k]['Item']."&quot;\t";
				$temp_export_content .= "&quot;".$tempDate[$k]['PIC']."&quot;\t";
				$temp_export_content .= "&quot;".substr($tempDate[$k]['RecordDate'],0,10)."&quot;";
				if($k!=(sizeof($tempDate)-1)) $temp_export_content .= "\r";
			}
			
			$export_content .= $temp_export_content."\n";
			
		}
	}
	$tableContent .= "</table>";
}


########################################################

$TAGS_OBJ[] = array($Lang['eDiscipline']['GM_WeeklyReport'],"");

# menu highlight setting
$CurrentPage = "GM_WeeklyReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
//debug_pr($_POST);
?>

<script language="javascript">
function changeType(form_obj,lvl_value)
{
        obj = form_obj.elements["target[]"]
         <? # Clear existing options
         ?>
         while (obj.options.length > 0)
         {
                obj.options[0] = null;
         }
         if (lvl_value==1)
         {
             <?
             for ($i=0; $i<sizeof($levels); $i++)
             {
                  list($id,$name) = $levels[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=intranet_htmlspecialchars($name)?>',<?=$id?>);
                  <?

             }
             ?>
         }
         else
         {
             <?
             for ($i=0; $i<sizeof($classes); $i++)
             {
                  list($id,$name, $lvl_id) = $classes[$i];
                  ?>
                  obj.options[<?=$i?>] = new Option('<?=$name?>',<?=$id?>);
                  <?

             }
             ?>
         }

}
function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function doExport()
{
	document.form1.action = "weeklyReport_export.php";
	document.form1.submit();
	document.form1.action = "";
}

function getSelectedString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].value + ',';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = tmpString.substr(0, tmpString.length-1);
	} else {
		document.getElementById(targetObj).value = tmpString;
	}
}

function getSelectedTextString(obj, targetObj){
	var tmpString = '';
	for (i=0; i<obj.length; i++){
		if (obj.options[i].selected == true){
			tmpString += obj.options[i].text + '\',\'';
		}
	}
	if (tmpString.length > 0){
		document.getElementById(targetObj).value = "'"+tmpString.substr(0, tmpString.length-3)+"'";
	} else {
		document.getElementById(targetObj).value = "'"+tmpString+"'";
	}
}

function checkForm()
{

 	if(!check_date(document.getElementById('startDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(!check_date(document.getElementById('endDate'), '<?=$i_invalid_date?>')) {
		return false;
	}
	if(document.getElementById('startDate').value > document.getElementById('endDate').value) {
		alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
		return false;	
	}
	
	/*
	var select_obj = document.getElementById('target[]');

	for (var i=0; i<select_obj.length; i++)
	{
		if (select_obj[i].selected)
		{
			return true;
		}
	}
	alert('<?=$i_Discipline_System_alert_PleaseSelectClassLevel?>');
	
	return false;
	*/
	getSelectedString(document.getElementById('ItemID[]'), 'SelectedItemID');
	getSelectedTextString(document.getElementById('ItemID[]'), 'SelectedItemIDText');
}

// Generate Good Conduct Category
var good_cat_select = new Array();
good_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($good_cats as $k=>$d)	{ ?>
		good_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

// Generate Misconduct Category
var mis_cat_select = new Array();
mis_cat_select[0] = new Option("-- <?=$eDiscipline["SelectRecordCategory"]?> --",0);
<?	foreach($mis_cats as $k=>$d)	{ ?>
		mis_cat_select[<?=$k+1?>] = new Option("<?=str_replace('"', '\"', $d['Name'])?>",<?=$d['CategoryID']?>);
<?	} ?>

function changeCat(value)
{
	var item_select = document.getElementById('ItemID[]');
	var selectedCatID = value;
	var record_type_good = document.getElementById('record_type_good');

	while (item_select.options.length > 0)
	{
		item_select.options[0] = null;
	}
	
	if (selectedCatID == 0) {
	<?
		$curr_cat_id = "";
	?>
		if(document.form1.record_type_good.checked) {
	<?
		$pos = 0;
			for ($i=0; $i<sizeof($good_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $good_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		} else {
	<?
			$pos = 0;
			for ($i=0; $i<sizeof($mis_items); $i++)
			{
				list($r_catID, $r_itemID, $r_itemName) = $mis_items[$i];
				$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
				$r_itemName = str_replace('"', '\"', $r_itemName);
				$r_itemName = str_replace("'", "\'", $r_itemName);
				echo "item_select.options[".$pos++."] = new Option(\"".$r_itemName."\",".$r_itemID.");\n";
			}
	?>	
		}
	}
	else if (selectedCatID == '')
	{

	<? 
		$curr_cat_id = "";
		$pos = 0;
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
			$r_itemName = str_replace("'", "\'", $r_itemName);
			if ($r_catID != $curr_cat_id)
			{
				$pos = 0;
	?>
	}
	else if (selectedCatID == "<?=$r_catID?>")
	{
	<?
				$curr_cat_id = $r_catID;
			}
	?>
		item_select.options[<?=$pos++?>] = new Option("<?=$r_itemName?>",<?=$r_itemID?>);
		<?	if($r_itemID == $ItemID) {	?>
		item_select.selectedIndex = <?=$pos-1?>;
		<? 	} 
		}
	?>
	}
	SelectAll(document.form1.elements['ItemID[]']);
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	//document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	//document.getElementById('tdOption').className = 'report_show_option';
}


</script>
<script language="javascript">

var targetDivID = "";
var clickDivID = "";


function Show_Detail_Window(StudentID, academicYearID, startDate, endDate, recordType){
    obj = document.form1;
    obj.targetDivID.value = StudentID;
    obj.ajaxStudentID.value = StudentID;
    obj.ajaxYear.value = academicYearID;
    obj.ajaxStartDate.value = startDate;
    obj.ajaxEndDate.value = endDate;
    obj.ajaxType.value = 'recordDate';
    obj.ajaxRecordType.value = recordType;
    clickDivID = 'RecordDateDetail_'+StudentID;
    targetDivID = StudentID;
    YAHOO.util.Connect.setForm(obj);	
    var path;
    path = "ajaxRecord.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback2);
    
}
var callback2 = {
    success: function ( o )
    {       
    	
		  DisplayDefDetail(o.responseText);
	}
}
function DisplayDefDetail(text){
	document.getElementById('div_form').innerHTML = text;
	DisplayPosition();
  
}
function getPosition(obj, direction)
{
	
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}
function DisplayPosition() {
	var posleft = getPosition(document.getElementById(clickDivID),'offsetLeft') + 100;
	var postop = getPosition(document.getElementById(clickDivID),'offsetTop') + 10;

	document.getElementById(targetDivID).style.left = posleft+"px";
	document.getElementById(targetDivID).style.top = postop+"px";
	document.getElementById(targetDivID).style.visibility = 'visible';
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility = 'hidden';
}
</script>
<br/>
<form name="form1" action="" method="POST" onSubmit="return checkForm()">
<table width="90%" border="0" cellpadding="5" cellspacing="0">
	<tr class="tabletext">
		<td width="30%" class="formfieldtitle" valign="top"><?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span></td>
		<td>
			<?=$linterface->GET_DATE_PICKER("startDate",$startDate)." ".$i_To." ".$linterface->GET_DATE_PICKER("endDate",$endDate)?>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?=$iDiscipline['RecordType2'] ?><span class="tabletextrequire">*</span></td>
		<td>
			<input name="record_type" type="radio" id="record_type_good" value="1" <?=($record_type==1)?"checked":""?> onClick="hideSpan('spanMisCat');showSpan('spanGoodCat');changeCat('0');document.getElementById('selectGoodCat').value='0';"><label for="record_type_good"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_GoodConduct?></label>
			<input name="record_type" type="radio" id="record_type_mis" value="-1" <?=($record_type==-1 || $record_type=="")?"checked":""?> onClick="hideSpan('spanGoodCat');showSpan('spanMisCat');changeCat('0');document.getElementById('selectMisCat').value='0';"><label for="record_type_mis"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"> <?=$i_Discipline_Misconduct?></label><br />
			<span id="spanGoodCat"<? echo ($record_type==1 || $record_type=="")?"":" style=\"display:none\"" ?>>
				<?=$selectGoodCatHTML?>
			</span>
			<span id="spanMisCat"<? echo ($record_type==-1)?"":" style=\"display:none\"" ?>>
				<?=$selectMisCatHTML?>
			</span>
			<br>
			<?=$selectItemIDHTML?>
			<?= $linterface->GET_BTN($button_select_all, "button", "javascript:SelectAll(document.getElementById('ItemID[]'))"); ?>
		</td>
	</tr>
	<tr class="tabletext">
		<td class="formfieldtitle" valign="top"><?=$Lang['eDiscipline']['Interval'] ?><span class="tabletextrequire">*</span></td>
		<td>
			<?/*=$eDiscipline["Every"]." ".$intervalMenu." ".$Lang['eDiscipline']['OnOrAbove']*/?>
			<?=$eDiscipline["Every"]." ".$intervalMenu." ".$i_Discipline_Times?>
		</td>
	</tr>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit", "", "submitBtn01")?>&nbsp;
		</td>
	</tr>
</table>
<br>

<? if ($flag == 1) { ?>
<input type="hidden" name="exportContent" value='<?=$export_content?>' />

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">

	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
		<?=$tableContent?><br>
		<?=$linterface->GET_ACTION_BTN($button_export, "button", "javascript:doExport()")?>
		<div id="div_form"></div>
		</td>
	</tr>
</table>
</p>
<? } ?>
<input type="hidden" name="flag" id="flag" value="1" />
<input type="hidden" name="targetDivID" id="targetDivID" />
<input type="hidden" name="ajaxStudentID" id="ajaxStudentID" />
<input type="hidden" name="ajaxYear" id="ajaxYear" />
<input type="hidden" name="ajaxStartDate" id="ajaxStartDate" />
<input type="hidden" name="ajaxEndDate" id="ajaxEndDate" />
<input type="hidden" name="ajaxType" id="ajaxType" />
<input type="hidden" name="ajaxRecordType" id="ajaxRecordType" />
<input type="hidden" name="SelectedItemID" id="SelectedItemID" value="<?=$SelectedItemID?>"/>
<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" value="<?=intranet_htmlspecialchars($SelectedItemIDText)?>"/>
</form>
<script language="javascript">
<? if($submitBtn01!="") { ?>

<? } else { ?>
	hideSpan('spanGoodCat');
	showSpan('spanMisCat');
	changeCat('0');
	document.getElementById('selectMisCat').value='0';

<? } ?>
</script>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
