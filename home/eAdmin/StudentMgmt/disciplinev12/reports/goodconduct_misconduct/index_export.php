<?php
// Modifying by : 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();
//$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$filename = "goodconduct_misconduct_report.csv";	

$lclass = new libclass();
$linterface = new interface_html();

$selectYear = ($selectYear == '') ? getCurrentAcademicYear() : $selectYear;

//$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",", $SelectedFormText);

$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",", $SelectedClassText);

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",", $SelectedItemIDText);


if ($Period == "YEAR") {
	$parPeriodField1 = $selectYear;
	$parPeriodField2 = $selectSemester;
} else {
	$parPeriodField1 = $textFromDate;
	$parPeriodField2 = $textToDate;
}

if ($rankTarget == "form") {
	$parByFieldArr = $SelectedFormTextArr;
} else {
	$parByFieldArr = $SelectedClassTextArr;
}

if($categoryID==1) {		# "Late"
	$parStatsFieldArr[0] = 0;
	//$parStatsFieldTextArr[0] = 'Late';
	$parStatsFieldTextArr[0] = $ldiscipline->getGMCatNameByCatID(1);
} else if($categoryID==2) {	# "Homework not submitted"
	
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
	
} else {
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
}

### Table Data ###
$data = $ldiscipline->retrieveGoodMisReport($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID);

################################################
############ Report Table Content ##############

if($record_type == 1) {
	$i_MeritDemerit[1] = $i_Merit_Merit;
	$i_MeritDemerit[2] = $i_Merit_MinorCredit;
	$i_MeritDemerit[3] = $i_Merit_MajorCredit;
	$i_MeritDemerit[4] = $i_Merit_SuperCredit;
	$i_MeritDemerit[5] = $i_Merit_UltraCredit;
} else if($record_type == -1) {
	$i_MeritDemerit[0] = $i_Merit_Warning;
	$i_MeritDemerit[-1] = $i_Merit_BlackMark;
	$i_MeritDemerit[-2] = $i_Merit_MinorDemerit;
	$i_MeritDemerit[-3] = $i_Merit_MajorDemerit;
	$i_MeritDemerit[-4] = $i_Merit_SuperDemerit;
	$i_MeritDemerit[-5] = $i_Merit_UltraDemerit;
}
	
$itemTotal = array();
$columnMeritDemeritCount = array();

$exportArr = array();

# header of report table
$p = 0;

if($rankTarget=="form") {
	$exportArr[$p][] = $i_Discipline_Form;
	//$exportArr[$p][] = $i_Discipline_Class;
} else if($rankTarget=="class") {
	$exportArr[$p][] = $i_Discipline_Class;
} else {
	$exportArr[$p][] = $i_general_name;
	$exportArr[$p][] = $i_ClassNumber;
}


foreach($parStatsFieldTextArr as $tempValue) {
	$exportArr[$p][] = intranet_htmlspecialchars(stripslashes($tempValue));
}

$exportArr[$p][] = "";

foreach($i_MeritDemerit as $tempMeritDemerit) {
	$exportArr[$p][] = $tempMeritDemerit;
}
$p++;
#content of report table
//debug_r($data);
		/*
		if ($rankTarget == "form") {
			$sql = "SELECT CLS.ClassName FROM INTRANET_CLASS CLS LEFT OUTER JOIN INTRANET_CLASSLEVEL LVL ON (CLS.ClassLevelID=LVL.ClassLevelID) WHERE LVL.LevelName IN ('".implode('\',\'',$parByFieldArr)."')";
			$temp = $ldiscipline->returnVector($sql);
			$parByFieldArr = $temp;
		}
		*/

if($rankTarget != "student") {
	for($i=0; $i<sizeof($parByFieldArr); $i++) {						# for each class/form
		if($rankTarget=="form") {
			$w = 0;
					
			$exportArr[$p][$w] = stripslashes($parByFieldArr[$i]);
			$w++;
			
			for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {			# for each item
				$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : 0;
				$w++;
				$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += addslashes($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]);
			}
																				# (YEAR/DATE , Period1, Period2, Class/Form, ClassName/FormName, SelectedItemID, SelectedItemName)
			$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);
			$exportArr[$p][$w] = "";
			$w++;
			
			if($record_type==1) {
				$m = 1;
			} else {
				$m = 0;
			}
			foreach($i_MeritDemerit as $columnNo) {
				$tempCount = 0;
				for($n=0; $n<sizeof($meritDemeritCount); $n++) {
					if($meritDemeritCount[$n]['ProfileMeritType']==$m)
						$tempCount += $meritDemeritCount[$n]['countTotal'];
				}
				$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
				$w++;
				$columnMeritDemeritCount[$m] += $tempCount;
				
				if($record_type==1) {
					$m++;
				} else {
					$m--;
				}
			}
			$p++;
						
		} else {	# rankTarget = class
			
			//$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($parByFieldArr[$i]);
			
			//for($k=0; $k<sizeof($stdInfo); $k++) {
				$w = 0;
				$exportArr[$p][$w] = $parByFieldArr[$i];
				$w++;
				//$exportArr[$p][$w] = $parByFieldArr[$i]."-".$stdInfo[$k][0];
				//$w++;
				
				for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {		# for each item
					$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : "0";
					$w++;
					$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
				}
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID);
				$exportArr[$p][$w] = "";
				$w++;
				//debug_r($meritDemeritCount);
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
					$w++;
					$columnMeritDemeritCount[$m] += $tempCount;
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
				}
				$p++;
							
			//}
			
		}
		
	}
} else {		# rankTarget = student
			
			for($k=0; $k<sizeof($studentID); $k++) {
				
				$stdInfo = $ldiscipline->getStudentNameByID($studentID[$k], $selectYear);
				$w = 0;
				$exportArr[$p][$w] = $stdInfo[0][1];
				$w++;
				$exportArr[$p][$w] = $stdInfo[0][2]."-".$stdInfo[0][3];
				$w++;
				
				for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {		# for each item
					$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]] : "0";
					$w++;
					$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
				}
				
				$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k]);
				$exportArr[$p][$w] = "";
				$w++;
				
				if($record_type==1) {
					$m = 1;
				} else {
					$m = 0;
				}
				foreach($i_MeritDemerit as $columnNo) {
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++) {
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
					$w++;
					$columnMeritDemeritCount[$m] += $tempCount;
					
					if($record_type==1) {
						$m++;
					} else {
						$m--;
					}
				}
				$p++;
							
			}
			
}

$exportArr[$p][] = $i_Discipline_System_Report_Class_Total;
if($rankTarget=="student")	
	$exportArr[$p][] = "";

for($k=0; $k<sizeof($parStatsFieldTextArr); $k++) {
	$exportArr[$p][] = (isset($itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))])) ? $itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))] : "0";
}

$exportArr[$p][] = "";

if($record_type==1) {
	$m = 1;
} else {
	$m = 0;
}
foreach($i_MeritDemerit as $columnNo) {
	$exportArr[$p][] .= $columnMeritDemeritCount[$m];
	
	if($record_type==1) {
		$m++;
	} else {
		$m--;
	}
}

############ End of Report Table ###############
################################################

$numOfColumn = sizeof($parStatsFieldTextArr) + sizeof($i_MeritDemerit) + 2;

$export_content = $eDiscipline['GoodConductMisconductReport'];
if($Period=="YEAR") {
	$export_content .= " (".$ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." ".$ldiscipline->getTermNameByTermID($parPeriodField2).")\n";
} else {
	$export_content .= " (".$iDiscipline['Period_Start']." ".$parPeriodField1." ".$iDiscipline['Period_End']." ".$parPeriodField2.")\n";
}
//$generationDate = getdate();
$generationDate = date("Y/m/d H:i:s");
$export_content .= $eDiscipline['ReportGeneratedDate']." ".$generationDate."\n\n";

$export_content .= $lexport->GET_EXPORT_TXT($exportArr, "", "\t", "\r\n", "\t", $numOfColumn, "11");

intranet_closedb();

$lexport->EXPORT_FILE($filename, $export_content);

?>