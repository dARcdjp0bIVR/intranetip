<?
# using: yat

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Report-View"))
{
	$url = "./goodconduct_misconduct/";
}
else if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Report-View"))
{
	$url = "./ranking/";
}
else if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Detail_Report-View"))
{
	$url = "./ranking_detail/";
}
else if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Daily_Report-View"))
{
	$url = "./daily_report/";
}
else
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
				
header("Location: $url");		
						

intranet_closedb();
?>