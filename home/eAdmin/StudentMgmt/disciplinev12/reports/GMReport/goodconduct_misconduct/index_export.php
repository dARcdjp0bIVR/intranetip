<?php
// Modifying by : 

##### Change Log [Start] #####
#
#	Date	:	2017-11-30 (Bill)	[2017-1121-0914-46066]
#				- Fixed: Display Mode Setting (Vertical) cannot export last item
#
#	Date	:	2017-09-27 (Bill)	[2017-0927-1530-03066]
#				- Use Display Mode Setting to control export format instead of $sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout']
#
#	Date	:	2016-11-01 (Bill)	[2016-0808-1526-52240]
#				- Change layout 	X-Axis: GM Item		Y-Axis: Target
#				- if use old layout, set $sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout'] = true
#
#	Date	:	2012-11-15 (YatWoon)
#				Cater with subject with ",", use ",," as separator [Case#2012-1005-1150-06132] [Case#2012-1114-0941-57156]
#
#	Date	:	2012-03-21 (Henry Chow)
#				revise calculation of displaying "Total Not Submit Times" [CRM : 2012-0117-1703-41042]
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
	$defaultSecondCategory = 7;
	$defaultSecondItem = 2;	
}

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();
//$ldiscipline->CONTROL_ACCESS("Discipline-STAT-GoodConduct_Misconduct-View");

$filename = "goodconduct_misconduct_report.csv";	

$lclass = new libclass();
$linterface = new interface_html();

$selectYear = ($selectYear == '') ? getCurrentAcademicYear() : $selectYear;

//$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));

$SelectedFormArr = explode(",", $SelectedForm);
$SelectedFormTextArr = explode(",,", $SelectedFormText);

$SelectedClassArr = explode(",", $SelectedClass);
$SelectedClassTextArr = explode(",,", $SelectedClassText);

$SelectedItemIDArr = explode(",", $SelectedItemID);
$SelectedItemIDTextArr = explode(",,", $SelectedItemIDText);

if ($Period == "YEAR") {
	$parPeriodField1 = $selectYear;
	$parPeriodField2 = $selectSemester;
}
else {
	$parPeriodField1 = $textFromDate;
	$parPeriodField2 = $textToDate;
}

if ($rankTarget == "form") {
	$parByFieldArr = $SelectedFormTextArr;
}
else {
	$parByFieldArr = $SelectedClassTextArr;
}

if($categoryID==1) {		# "Late"
	$parStatsFieldArr[0] = 0;
	//$parStatsFieldTextArr[0] = 'Late';
	$parStatsFieldTextArr[0] = $ldiscipline->getGMCatNameByCatID(1);
}
else if($categoryID==2) {	# "Homework not submitted"
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
}
else {
	$parStatsFieldArr = $SelectedItemIDArr;
	$parStatsFieldTextArr = $SelectedItemIDTextArr;
}

### Table Data ###
$data = $ldiscipline->retrieveGoodMisReport($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr, $StatType, $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID, $include_waive);

################################################
############ Report Table Content ##############

if($record_type == 1) {
	$i_MeritDemerit[1] = $i_Merit_Merit;
	$i_MeritDemerit[2] = $i_Merit_MinorCredit;
	$i_MeritDemerit[3] = $i_Merit_MajorCredit;
	$i_MeritDemerit[4] = $i_Merit_SuperCredit;
	$i_MeritDemerit[5] = $i_Merit_UltraCredit;
}
else if($record_type == -1) {
	$i_MeritDemerit[0] = $i_Merit_Warning;
	$i_MeritDemerit[-1] = $i_Merit_BlackMark;
	$i_MeritDemerit[-2] = $i_Merit_MinorDemerit;
	$i_MeritDemerit[-3] = $i_Merit_MajorDemerit;
	$i_MeritDemerit[-4] = $i_Merit_SuperDemerit;
	$i_MeritDemerit[-5] = $i_Merit_UltraDemerit;
}

$itemTotal = array();
$columnMeritDemeritCount = array();

$exportArr = array();
$p = 0;

// [2016-0808-1526-52240] new layout
//if(!$sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout'])
if($display_hori)
{
	// Target: Form / Class
	if($rankTarget=="form" || $rankTarget=="class") 
	{
		// Initial
		$p1 = 0;
		$export2Arr = array();
		$subtotal = array();
		$targetMeritAry = array();
		$ItemCategoryAry = array();
		
		# Table Header
		$exportArr[$p][] = ($record_type==1? $i_Discipline_GoodConduct : $i_Discipline_Misconduct);
		$exportArr[$p][] = ($rankTarget=="form"? $Lang['eDiscipline']['WholeSchoolTotal'] : $i_Discipline_System_Report_Class_Total);
		for($i=0; $i<sizeof($parByFieldArr); $i++)
		{
			$exportArr[$p][] = stripslashes($parByFieldArr[$i]);
		}
		$p++;
		
		// loop items
		for($j=0; $j<sizeof($parStatsFieldTextArr); $j++)
		{
			// Item name
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$exportArr[$p][0] = intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]));
			}
			else {
				$export2Arr[$p1][0] = intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]));
			}
			
			// loop targets
			$w = 2;
			$finalTotal = 0;
			for($i=0; $i<sizeof($parByFieldArr); $i++)
			{
				// Target Count
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
					$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : "0";
				}
				else {
					$export2Arr[$p1][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : "0";
				}
				// add total count
				$finalTotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
				
				// handle Total Not Submit Times
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
				{
					// get category count
					if(!isset($ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))]))
					{
						$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
						$ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] = $ldiscipline->returnVector($sql);
					}
					$tempResult = $ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))];
					
					// add to target count (Total Not Submit Times)
					if($tempResult[0]>0) {
						$subtotal[stripslashes($parByFieldArr[$i])] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
					}
				}
				$w++;
			}
			
			// Total Count
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$exportArr[$p][1] = (isset($finalTotal)) ? $finalTotal : "0";
				$p++;
			}
			else {
				$export2Arr[$p1][1] = (isset($finalTotal)) ? $finalTotal : "0";
				$p1++;
			}
		}
		unset($ItemCategoryAry);
		
		// Total Not Submit Times Column
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
		{
			$exportArr[$p][0] = $Lang['eDiscipline']['TotalNotSubmitTimes'];
			
			// loop targets
			$w = 2;
			$finalTotal = 0;
			for($i=0; $i<sizeof($parByFieldArr); $i++)
			{
				$exportArr[$p][$w] = (isset($subtotal[stripslashes($parByFieldArr[$i])])) ? $subtotal[stripslashes($parByFieldArr[$i])] : "0";
				$finalTotal += (isset($subtotal[stripslashes($parByFieldArr[$i])])) ? $subtotal[stripslashes($parByFieldArr[$i])] : 0;
				$w++;
			}
			
			// Total Count (Total Not Submit Times)
			$exportArr[$p][1] = (isset($finalTotal)) ? $finalTotal : "0";
			$p++;
		}
		
		// Merge exportArr
		$exportArr = array_merge($exportArr, $export2Arr);
		if(!empty($export2Arr))
			$p += $p1;
		
		// Merit / Demerit
		if($include_merit)
		{
			$a = 0;
			if($record_type==1)
				$m = 1;
			else
				$m = 0;
				
			$exportArr[$p] = array("");
			$p++;
				
			// loop merit type
			foreach($i_MeritDemerit as $tempMeritDemerit)
			{
				$exportArr[$p][0] = $tempMeritDemerit;
				
				// loop target
				$w = 2;
				$finalTotal = 0;
				for($i=0; $i<sizeof($parByFieldArr); $i++)
				{
					$css = ($i==0) ? "border_left" : "";
				
					// Get Merit / Demerit
					if(!isset($targetMeritAry[stripslashes($parByFieldArr[$i])]))
						$targetMeritAry[stripslashes($parByFieldArr[$i])] = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "",  $include_waive, $include_unreleased);
					$meritDemeritCount = $targetMeritAry[stripslashes($parByFieldArr[$i])];
					
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++)
					{
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					
					// Merit / Demerit Count
					$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
					$finalTotal += $tempCount;
					$w++;
				}
				
				// Total Count (Merit / Demerit) 
				$exportArr[$p][1] = (isset($finalTotal)) ? $finalTotal : "0";
				
				// update $m and $a
				if($record_type==1)
					$m++;
				else
					$m--;
				$a++;
				$p++;
			}
			unset($targetMeritAry);
		}
	}
	// Target: Student
	else
	{
		// Initial
		$p1 = 0;
		$export2Arr = array();
		$subtotal = array();
		$stutentAry = array();
		$studentMeritAry = array();
		$ItemCategoryAry = array();
		
		# Table Header
		$exportArr[$p][] = ($record_type==1? $i_Discipline_GoodConduct : $i_Discipline_Misconduct);
		$exportArr[$p][] = $Lang['eDiscipline']['StudentTotal'];
		for($k=0; $k<sizeof($studentID); $k++)
		{
			// store student info
			$stutentAry[$studentID[$k]] = $ldiscipline->getStudentNameByID($studentID[$k], $selectYear); 
			$stdInfo = $stutentAry[$studentID[$k]];
			$exportArr[$p][] = $stdInfo[0][1]." (".$stdInfo[0][2]." - ".$stdInfo[0][3].")";
		}
		$p++;
		
		// loop item
		for($j=0; $j<sizeof($parStatsFieldTextArr); $j++)
		{
			// Item Name
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$exportArr[$p][0] = intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]));
			}
			else {
				$export2Arr[$p1][0] = intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]));
			}
		
			// loop students
			$w = 2;
			$finalTotal = 0;
			for($k=0; $k<sizeof($studentID); $k++)
			{
				$stdInfo = $stutentAry[$studentID[$k]];
				
				// Student Count
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
					$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]] : "0";
				}
				else {
					$export2Arr[$p1][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]] : "0";
				}
				// add total count
				$finalTotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
				
				// handle Total Not Submit Times
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
				{
					// get category count
					if(!isset($ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))]))
					{
						$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
						$ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] = $ldiscipline->returnVector($sql);
					}
					$tempResult = $ItemCategoryAry[intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))];
					
					// add to target count (Total Not Submit Times)
					if($tempResult[0]>0) {
						$subtotal[$stdInfo[0][2]][$stdInfo[0][3]] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
					}
				}
				$w++;
			}
			
			// Total Count
			if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0, $defaultSecondItem))) {
				$exportArr[$p][1] = (isset($finalTotal)) ? $finalTotal : "0";
				$p++;
			}
			else {
				$export2Arr[$p1][1] = (isset($finalTotal)) ? $finalTotal : "0";
				$p1++;
			}
		}
		unset($ItemCategoryAry);
		
		// Total Not Submit Times Column
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'])
		{
			$exportArr[$p][0] = $Lang['eDiscipline']['TotalNotSubmitTimes'];
			
			// loop students
			$w = 2;
			$finalTotal = 0;
			for($k=0; $k<sizeof($studentID); $k++)
			{
				$stdInfo = $stutentAry[$studentID[$k]];	
				$exportArr[$p][$w] = (isset($subtotal[$stdInfo[0][2]][$stdInfo[0][3]])) ? $subtotal[$stdInfo[0][2]][$stdInfo[0][3]] : "0";
				$finalTotal += (isset($subtotal[$stdInfo[0][2]][$stdInfo[0][3]])) ? $subtotal[$stdInfo[0][2]][$stdInfo[0][3]] : 0;
				$w++;
			}
			
			// Total Count (Total Not Submit Times)
			$exportArr[$p][1] = (isset($finalTotal)) ? $finalTotal : "0";
			$p++;
		}
		
		// Merge exportArr
		$exportArr = array_merge($exportArr, $export2Arr);
		if(!empty($export2Arr))
			$p += $p1;
		
		// Merit / Demerit
		if($include_merit)
		{
			$a = 0;
			if($record_type==1)
				$m = 1;
			else
				$m = 0;
				
			$exportArr[$p] = array("");
			$p++;
			
			// loop merit type
			foreach($i_MeritDemerit as $tempMeritDemerit)
			{
				$exportArr[$p][0] = $tempMeritDemerit;
				
				// loop students
				$w = 2;
				$finalTotal = 0;
				for($k=0; $k<sizeof($studentID); $k++)
				{
					// Get Merit / Demerit
					$stdInfo = $stutentAry[$studentID[$k]];
					if(!isset($studentMeritAry[$studentID[$k]]))
						$studentMeritAry[$studentID[$k]] = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k], $include_waive, $include_unreleased);
					$meritDemeritCount = $studentMeritAry[$studentID[$k]];
					
					$tempCount = 0;
					for($n=0; $n<sizeof($meritDemeritCount); $n++)
					{
						if($meritDemeritCount[$n]['ProfileMeritType']==$m)
							$tempCount += $meritDemeritCount[$n]['countTotal'];
					}
					
					// Merit / Demerit Count
					$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
					$finalTotal += $tempCount;
					$w++;
				}
				
				// Total Count (Merit / Demerit) 
				$exportArr[$p][1] = (isset($finalTotal)) ? $finalTotal : "0";
				
				// update $m and $a
				if($record_type==1)
					$m++;
				else
					$m--;
				$a++;
				$p++;
			}
			unset($studentMeritAry);
		}
	}
}
else
{
	if($rankTarget=="form") {
		$exportArr[$p][] = $i_Discipline_Form;
		//$exportArr[$p][] = $i_Discipline_Class;
		$c = 1;
	}
	else if($rankTarget=="class") {
		$exportArr[$p][] = $i_Discipline_Class;
		$c = 1;
	}
	else {
		$exportArr[$p][] = $i_general_name;
		$exportArr[$p][] = $i_ClassNumber;
		$c = 2;
	}
	$start = $c;
	$c2 = $c;
	
	$i = 0;
	foreach($parStatsFieldTextArr as $tempValue) {
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$i], array(0,$defaultSecondItem))) {
			$exportArr[$p][$c] = intranet_htmlspecialchars(stripslashes($tempValue));
			$c2++;
		}
		else {
			// [2017-1121-0914-46066]
			if($c == $start) {
				$exportArr[$p][$c] = "";
			}
			$exportArr[$p][$c+1] = intranet_htmlspecialchars(stripslashes($tempValue));
		}
		$c++;
		$i++;
	}
	
	if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
		$exportArr[$p][$c2] = $Lang['eDiscipline']['TotalNotSubmitTimes'];	
	}
	$exportArr[$p][] = "";
	
	// Merit / Demerit
	if($include_merit)
	{
		foreach($i_MeritDemerit as $tempMeritDemerit) {
			$exportArr[$p][] = $tempMeritDemerit;
		}
	}
	$p++;
	$finalTotal = 0;
	
	#content of report table
	//debug_r($data);
	/*
	if ($rankTarget == "form") {
		$sql = "SELECT CLS.ClassName FROM INTRANET_CLASS CLS LEFT OUTER JOIN INTRANET_CLASSLEVEL LVL ON (CLS.ClassLevelID=LVL.ClassLevelID) WHERE LVL.LevelName IN ('".implode('\',\'',$parByFieldArr)."')";
		$temp = $ldiscipline->returnVector($sql);
		$parByFieldArr = $temp;
	}
	*/
	
	// Target: Form / Class
	if($rankTarget != "student")
	{
		for($i=0; $i<sizeof($parByFieldArr); $i++) {
			if($rankTarget=="form")
			{
				$w = 0;
				$exportArr[$p][$w] = stripslashes($parByFieldArr[$i]);
				$w++;
				
				$subtotal = 0;
				// [2017-1121-0914-46066]
				if(!$sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
					$exportArr[$p][$w] = "";
					$w++;
				}
				$temp_w = $w;
				
				// loop items
				for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0,$defaultSecondItem))) {
						$this_w = $w;
						$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : 0;
						$temp_w++;
					}
					else {
						$this_w = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) ? $w+1 : $w;
						$exportArr[$p][$this_w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : 0;
					}
					$w++;
					
					$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += addslashes($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])]);
					
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
						$tempResult = $ldiscipline->returnVector($sql);
						
						if($tempResult[0]>0) {
							$subtotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
						}
					}
				}
				
				if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
					$exportArr[$p][$temp_w] = $subtotal;
					$finalTotal += $subtotal;
					$w++;
				}
				
				// Merit / Demerit
				if($include_merit)
				{
					# (YEAR/DATE , Period1, Period2, Class/Form, ClassName/FormName, SelectedItemID, SelectedItemName)
					$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "", $include_waive, $include_unreleased);
					$exportArr[$p][$w] = "";
					$w++;
					
					if($record_type==1) {
						$m = 1;
					}
					else {
						$m = 0;
					}
					
					foreach($i_MeritDemerit as $columnNo) {
						$tempCount = 0;
						for($n=0; $n<sizeof($meritDemeritCount); $n++) {
							if($meritDemeritCount[$n]['ProfileMeritType']==$m)
								$tempCount += $meritDemeritCount[$n]['countTotal'];
						}
						$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
						$w++;
						$columnMeritDemeritCount[$m] += $tempCount;
						
						if($record_type==1) {
							$m++;
						}
						else {
							$m--;
						}
					}
				}
				$p++;
			}
			else
			{
				//$stdInfo = $ldiscipline->retrieveNameClassNoByClassName($parByFieldArr[$i]);
				//for($k=0; $k<sizeof($stdInfo); $k++) {
					$w = 0;
					$exportArr[$p][$w] = $parByFieldArr[$i];
					$w++;
					//$exportArr[$p][$w] = $parByFieldArr[$i]."-".$stdInfo[$k][0];
					//$w++;
					
					$subtotal = 0;
					// [2017-1121-0914-46066]
					if(!$sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$exportArr[$p][$w] = "";
						$w++;
					}
					$temp_w = $w;
					
					// loop items
					for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0,$defaultSecondItem))) {
							$this_w = $w;
							$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : "0";
							$temp_w++;
						}
						else {
							$this_w = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) ? $w+1 : $w;
							$exportArr[$p][$this_w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])] : "0";
						}
						$w++;
						
						$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
						
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
							$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
							$tempResult = $ldiscipline->returnVector($sql);
							
							if($tempResult[0]>0) {
								$subtotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][stripslashes($parByFieldArr[$i])];
							}
						}
					}
					
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$exportArr[$p][$temp_w] = $subtotal;
						$finalTotal += $subtotal;
						$w++;
					}
					
					// Merit / Demerit
					if($include_merit)
					{
						$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $parByFieldArr[$i], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, "", $include_waive, $include_unreleased);
						$exportArr[$p][$w] = "";
						$w++;
						//debug_r($meritDemeritCount);
						
						if($record_type==1) {
							$m = 1;
						}
						else {
							$m = 0;
						}
						
						foreach($i_MeritDemerit as $columnNo) {
							$tempCount = 0;
							for($n=0; $n<sizeof($meritDemeritCount); $n++) {
								if($meritDemeritCount[$n]['ProfileMeritType']==$m)
									$tempCount += $meritDemeritCount[$n]['countTotal'];
							}
							$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
							$w++;
							$columnMeritDemeritCount[$m] += $tempCount;
							
							if($record_type==1) {
								$m++;
							}
							else {
								$m--;
							}
						}
					}
					$p++;
				//}
			}
		}
	}
	// Target: Student
	else
	{
				for($k=0; $k<sizeof($studentID); $k++) {
					$stdInfo = $ldiscipline->getStudentNameByID($studentID[$k], $selectYear);
					
					$w = 0;
					$exportArr[$p][$w] = $stdInfo[0][1];
					$w++;
					$exportArr[$p][$w] = $stdInfo[0][2]."-".$stdInfo[0][3];
					$w++;
					
					$subtotal = 0;
					// [2017-1121-0914-46066]
					if(!$sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$exportArr[$p][$w] = "";
						$w++;
					}
					$temp_w = $w;
					
					// loop items
					for($j=0; $j<sizeof($parStatsFieldTextArr); $j++) {
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$j], array(0,$defaultSecondItem))) {
							$exportArr[$p][$w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]] : "0";
							$temp_w++;
						}
						else {
							$this_w = ($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) ? $w+1 : $w;
							$exportArr[$p][$this_w] = (isset($data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]])) ? $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]] : "0";
						}
						$w++;
						$itemTotal[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))] += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
						
						if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
							$sql = "SELECT COUNT(c.CategoryID) FROM DISCIPLINE_ACCU_CATEGORY c INNER JOIN DISCIPLINE_ACCU_CATEGORY_ITEM i ON (c.CategoryID=i.CategoryID AND i.ItemID='".$parStatsFieldArr[$j]."' AND c.Name LIKE '%".$Lang['eDiscipline']['NotSubmit']."%')";
							$tempResult = $ldiscipline->returnVector($sql);
							
							if($tempResult[0]>0) {
								$subtotal += $data[$parStatsFieldArr[$j]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$j]))][$stdInfo[0][2]][$stdInfo[0][3]];
							}
						}
					}
					
					if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
						$exportArr[$p][$temp_w] = $subtotal;
						$finalTotal += $subtotal;
						$w++;
					}
					
					// Merit / Demerit
					if($include_merit)
					{
						$meritDemeritCount = $ldiscipline->retrieveMeritDemeritCountFromGM($Period, $parPeriodField1, $parPeriodField2, $rankTarget, $stdInfo[$k][2], $parStatsFieldArr, $parStatsFieldTextArr, $categoryID, $studentID[$k], $include_waive, $include_unreleased);
						$exportArr[$p][$w] = "";
						$w++;
						
						if($record_type==1) {
							$m = 1;
						}
						else {
							$m = 0;
						}
						
						foreach($i_MeritDemerit as $columnNo) {
							$tempCount = 0;
							for($n=0; $n<sizeof($meritDemeritCount); $n++) {
								if($meritDemeritCount[$n]['ProfileMeritType']==$m)
									$tempCount += $meritDemeritCount[$n]['countTotal'];
							}
							$exportArr[$p][$w] = ($tempCount != 0) ? $tempCount : "0";
							$w++;
							$columnMeritDemeritCount[$m] += $tempCount;
							
							if($record_type==1) {
								$m++;
							}
							else {
								$m--;
							}
						}
					}
					$p++;
				}
	}
	
	$exportArr[$p][] = $i_Discipline_System_Report_Class_Total;
	if($rankTarget=="student")	
		$exportArr[$p][] = "";
	
	//$finalTotal = 0;
	
	$temp_k = $start;
	for($k=0; $k<sizeof($parStatsFieldTextArr); $k++) {
		if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal'] && in_array($parStatsFieldArr[$k], array(0,$defaultSecondItem))) {
			$exportArr[$p][$start] = (isset($itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))])) ? $itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))] : "0";
			$temp_k++;
		}
		else {
			// [2017-1121-0914-46066]
			if($start == $temp_k) {
				$exportArr[$p][$start] = "";
			}
			$exportArr[$p][$start+1] = (isset($itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))])) ? $itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))] : "0";
		}
		$start++;
		//$finalTotal += $itemTotal[$parStatsFieldArr[$k]][intranet_htmlspecialchars(stripslashes($parStatsFieldTextArr[$k]))];
	}
	
	if($sys_custom['eDiscipline']['GM_Report_Display_SubTotal']) {
		$exportArr[$p][$temp_k] = 	$finalTotal.'.';
	}
	
	// Merit / Demerit
	if($include_merit)
	{
		$exportArr[$p][] = "";
		
		if($record_type==1) {
			$m = 1;
		}
		else {
			$m = 0;
		}
		
		foreach($i_MeritDemerit as $columnNo) {
			$exportArr[$p][] .= $columnMeritDemeritCount[$m];
			
			if($record_type==1) {
				$m++;
			}
			else {
				$m--;
			}
		}
	}
}

############ End of Report Table ###############
################################################

$numOfColumn = sizeof($parStatsFieldTextArr) + sizeof($i_MeritDemerit) + 2;

$export_content = $eDiscipline['GoodConductMisconductReport'];
if($Period=="YEAR") {
	$export_content .= " (".$ldiscipline->getAcademicYearNameByYearID($parPeriodField1)." ".$ldiscipline->getTermNameByTermID($parPeriodField2).")\n";
}
else {
	$export_content .= " (".$iDiscipline['Period_Start']." ".$parPeriodField1." ".$iDiscipline['Period_End']." ".$parPeriodField2.")\n";
}

//if(!$sys_custom['eDiscipline']['GM_Report_Display_OriginalLayout'])
if($display_hori) 
{
	$targetDisplay = "";
	if($rankTarget=="form")
		$targetDisplay = $i_Discipline_Form;
	else if($rankTarget=="class")
		$targetDisplay = $i_Discipline_Class;
	else if($rankTarget=="student")
		$targetDisplay = $i_Discipline_Student;
	$export_content .= $i_Discipline_Target." : ".$targetDisplay."\n";
}

//$generationDate = getdate();
$generationDate = date("Y/m/d H:i:s");
$export_content .= $eDiscipline['ReportGeneratedDate']." ".$generationDate."\n\n";
$export_content .= $lexport->GET_EXPORT_TXT($exportArr, "", "\t", "\r\n", "\t", $numOfColumn, "11");

intranet_closedb();

$lexport->EXPORT_FILE($filename, $export_content);
?>