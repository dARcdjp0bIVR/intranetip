<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

$CategoryID = $misconductid[0];

$use_intranet_subject = $ldiscipline->accumulativeUseIntranetSubjectList();

$item_list = array();

if($CategoryID==2 && $use_intranet_subject){ # Homework
	$sql = "SELECT RecordID, ".Get_Lang_Selection('CH_DES','EN_DES')." as SubjectName FROM ASSESSMENT_SUBJECT WHERE RecordStatus = 1 order by DisplayOrder";
}
else{ # Others
	$sql="SELECT ItemID,Name FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID='$CategoryID' AND RecordStatus=1 ORDER BY DisplayOrder";
}
$temp = $ldiscipline->returnArray($sql,2);
$item_order = array();

for($i=0;$i<sizeof($temp);$i++){
	list($item_id,$item_name) = $temp[$i];
	$item_list[$item_id]['name'] = $item_name;
	$item_list[$item_id]['records']="";
	$item_order[]= $item_id;
}	

$namefield = getNameFieldByLang("b.");

## Get ACCU. punish Record
$conds .= ($include_waive) ? " AND a.RecordStatus IN (".DISCIPLINE_STATUS_APPROVED.",".DISCIPLINE_STATUS_WAIVED.")" : "AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;

$sql="SELECT a.StudentID,$namefield,a.ItemID,b.ClassName,DATE_FORMAT(a.RecordDate,'%Y-%m-%d'),b.ClassNumber 
FROM DISCIPLINE_ACCU_RECORD AS a 
LEFT OUTER JOIN INTRANET_USER AS b ON (a.StudentID = b.UserID) 
WHERE a.CategoryID='$CategoryID' AND a.RecordDate>='$startdate' AND a.RecordDate<='$enddate' $conds
ORDER BY b.ClassName,b.ClassNumber,a.RecordDate";
$temp = $ldiscipline->returnArray($sql);

## build result array
for($i=0;$i<sizeof($temp);$i++){
	list($student_id,$student_name,$item_id,$class_name,$record_date,$class_number)=$temp[$i];
	if($CategoryID==1)
		$item_list[1]['records'][$class_name][] = array($student_id,$student_name,$record_date,$class_number);
	else 
		$item_list[$item_id]['records'][$class_name][]= array($student_id,$student_name,$record_date,$class_number);
}

$cat_name = $ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME($CategoryID);

$Export_Ary = array();

$Export_Ary[] = array($Lang['eDiscipline']['GMDailyReport']);
$Export_Ary[] = array($i_general_startdate, $startdate);
$Export_Ary[] = array($i_general_enddate, $enddate);
$Export_Ary[] = array($iDiscipline['Accumulative_Category'], $cat_name);

$display = "";
if(!empty($item_list))
{
	foreach($item_list as $k=>$d)
	{
		$this_item_name = $d['name'];
		$this_records_ary = $d['records'];
		
		$Export_Ary[] = array();
		
		if(!empty($this_item_name))
			$Export_Ary[] = array($this_item_name);
		
		if(!empty($this_records_ary))
		{
			foreach($this_records_ary as $this_classname=>$d1)
			{
				$i=0;
				foreach($d1 as $k2 => $this_record)
				{
					list($this_student_id, $this_student_name, $this_record_date, $this_class_number) = $this_record;
					$display .= '<tr class="sub_row">';
					if($i==0)
						$Export_Ary[] = array($this_classname);

					$Export_Ary[] = array($this_class_number,$this_student_name,$this_record_date);
					$i++;
				}	
			}
		}	
		else
		{
			$Export_Ary[] = array($Lang['General']['NoRecordFound']);
		}
		
	}
}
	
$utf_content = "";
foreach($Export_Ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();
	
$filename = "GM_daily_report.csv";
$lexport->EXPORT_FILE($filename, $utf_content); 

?>