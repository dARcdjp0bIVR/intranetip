<?php
# using:  

##### Change Log [Start] #####
#
#	Date	:	2016-04-11	(Bill)
#				Fixed php error when pass empty string to array_merge()
#
#	Date	:	2016-04-08	(Bill)
#				Replace deprecated split() by explode() for PHP 5.4
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

//header("Content-Type:text/html;charset=utf-8");

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-GoodConduct_Misconduct_Ranking_Report-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# School Year Menu #

$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear .= $ldiscipline->getConductSchoolYear($SchoolYear);

$conds .= ($include_waive) ? " AND a.RecordStatus IN (".DISCIPLINE_STATUS_APPROVED.",".DISCIPLINE_STATUS_WAIVED.")" : "AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;

/*
if($meritType==1) {
	$meritTitle = $i_Discipline_GoodConduct;	
	$conds .= " AND meritType=1";
} else {
	$meritTitle = $i_Discipline_Misconduct;	
	$conds .= " AND meritType=-1";
}
*/
$meritTitle = $i_Discipline_GoodConduct." & ".$i_Discipline_Misconduct;

$semesterText = ($semester=='WholeYear' || $semester==0) ? $i_Discipline_System_Award_Punishment_Whole_Year : $ldiscipline->getTermNameByTermID($semester);

# Semester Menu #
if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $ldiscipline->getAcademicYearNameByYearID($SchoolYear)." ".$semesterText;
		// replace split() by explode() - for PHP 5.4
		//list($startYear, $endYear) = split('-',$SchoolYear);
		list($startYear, $endYear) = explode('-',$SchoolYear);
		//$conds .= " AND a.Year='$SchoolYear'";
		$conds .= " AND a.AcademicYearID=$SchoolYear";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
	}
	$conds .= ($semester != 'WholeYear' && $semester!=0) ? " AND a.Semester='$semester'" : "";
}

	/*
# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==$rankRange) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}
*/


if($typeBy=='byCategory') {
	if(is_array($selectGoodCat) && is_array($selectMisCat))
		$categoryID = array_merge($selectGoodCat, $selectMisCat);
	else if(is_array($selectGoodCat))
		$categoryID = $selectGoodCat;
	else 
		$categoryID = $selectMisCat;
}
if($typeBy=='byItem') {
	if(is_array($ItemID1) && is_array($ItemID2))
		$ItemID = array_merge($ItemID1, $ItemID2);
	else if(is_array($ItemID1))
		$ItemID = $ItemID1;
	else 
		$ItemID = $ItemID2;
}

$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		//$tempConds .= " d.ClassLevelID=$rankTargetDetail[$i]";
		$tempConds .= " y.YearID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		//$tempConds .= " b.ClassName='$rankTargetDetail[$i]'";
		$tempConds .= " yc.YearClassID=$rankTargetDetail[$i]";
	}
} else {
	$tempConds .= " a.StudentID IN (".implode(',',$studentID).")";	
}
$conds .= " AND ($tempConds)";

	
if($typeBy=='byCategory' && sizeof($categoryID)>0)
	$conds .= " AND f.CategoryID IN (".implode(',',$categoryID).")";
	
$selectedItemTextAry = explode(',',$SelectedItemText);

if($typeBy=='byItem' && sizeof($ItemID)>0) {
	
	for($i=0; $i<sizeof($ItemID); $i++) {
		$itemData = $ldiscipline->getConductItemDataByID($ItemID[$i], $selectedItemTextAry[$i]);
		$tempConds .= ($tempConds!='') ? " OR (a.ItemID='".$ItemID[$i]."' AND a.CategoryID='".$itemData['ID']."')" : " (a.ItemID='".$ItemID[$i]."' AND a.CategoryID='".$itemData['ID']."')";
		
	}
	
	if($tempConds!="")
		$conds .= " AND (".$tempConds.")";
	
}

$jsRankTargetDetail = implode(',', $rankTargetDetail);

# Group By
if($rankTarget=="form") 
	$groupBy = "form";
else if($rankTarget=="class")
	$groupBy = "name";
else 
	$groupBy = "b.UserID";
	
if($typeBy=='byType')
	//$groupBy .= ", a.RecordType ";
	$groupBy .= "";
else if($typeBy=='byCategory')	
	$groupBy .= ", e.CategoryID ";
else 
	$groupBy .= ", a.ItemID ";

	
switch($rankTarget) {
	case("class") : $criteria = $i_Discipline_Class; break;
	case("form") : $criteria = $i_Discipline_Form; break;
	case("student") : $criteria = $i_Discipline_Student; break;
	default: break;
}

# table Top
$tableTop .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' class='tableborder_print'>";
$tableTop .= "<tr class='tabletop_print'>";
$tableTop .= "<td width='20%'><a class=''>{$iDiscipline['Rank']}</a></td>";
$tableTop .= "<td width='40%'><a class=''>{$criteria} </a></span></td>";
$tableTop .= "<td width='40%'><a class=''>{$i_Discipline_No_of_Records}</a></td>";
$tableTop .= "</tr>";

# table bottom
$tableBottom .= "</table>";

# table no record
$tableNoContent .= $tableTop;
$tableNoContent .= "<tr class='row_print'>";
$tableNoContent .= "<td class='tabletext row_print' colspan='3' height='40' align='center'>{$i_no_record_exists_msg}</td>";
$tableNoContent .= "</tr>";
$tableNoContent .= $tableBottom;

# table content 
if($typeBy=='byType') {
	
	for($i=0; $i<sizeof($record_type); $i++) {
		
		$tempConds = ($record_type[$i]==1) ? " AND a.RecordType=1" : " AND (a.RecordType=-1 OR a.RecordType IS NULL)";

		$temp = $ldiscipline->getGMRankingData('type', $record_type[$i], $conds.$tempConds, $groupBy);
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		$tempTable .= ($record_type[$i]==1) ? "<br><span class='tabletext row_print'>".$i_Discipline_GoodConduct."</span>" : "<br><span class='tabletext row_print'>".$i_Discipline_Misconduct."</span>";
		
		if(sizeof($temp)>0) {
			$tempTable .= $tableTop;
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				$k = $j+1;
				$css = ($j%2)+1;
				$tempTable .= "<tr class='row_print'>";
				$tempTable .= "<td class='tabletext row_print' width='20%'>{$k}</td>";
				$tempTable .= "<td class='tabletext row_print' width='40%'>";
				$tempTable .= "{$showName}</td>";
				$tempTable .= "<td class='tabletext row_print' width='40%'>{$temp[$j][1]}</td>";
				$tempTable .= "</tr>";
			}
			$tempTable .= $tableBottom;
		} else {
			$tempTable .= $tableNoContent;	
		}
	}
	
} else if($typeBy=='byCategory'){
	//$selectedCategory = array_merge($selectGoodCat, $selectMisCat);
	$selectedCategory = array_merge((array)$selectGoodCat, (array)$selectMisCat);
	
	for($i=0; $i<sizeof($selectedCategory); $i++) {
		$temp = $ldiscipline->getGMRankingData('category', $selectedCategory[$i], $conds, $groupBy);
		
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		
		$categoryData = $ldiscipline->getGMCategoryInfoByCategoryID($selectedCategory[$i]);
		$meritTypeByCat = $categoryData['MeritType'];
		
		$tempTable .= ($meritTypeByCat==1) ? "<br><span class='tabletext row_print'>".$i_Discipline_GoodConduct : "<br><span class='tabletext row_print'>".$i_Discipline_Misconduct;
		
		$GMCategory = $ldiscipline->getGMCategoryInfoByCategoryID($selectedCategory[$i]);
		
		$tempTable .= " > ".$GMCategory['Name']."</span><br>";
		
		if(sizeof($temp)>0) {
			$tempTable .= $tableTop;
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				
				$k = $j+1;
				$css = ($j%2)+1;
				$tempTable .= "<tr class='row_print'>";
				$tempTable .= "<td class='tabletext row_print' width='20%'>{$k}</td>";
				$tempTable .= "<td class='tabletext row_print' width='40%'>";
				$tempTable .= "{$showName}</td>";
				$tempTable .= "<td class='tabletext row_print' width='40%'>{$temp[$j][1]}</td>";
				$tempTable .= "</tr>";
			}
			$tempTable .= $tableBottom;
		} else {
			$tempTable .= $tableNoContent;	
		}
	}		
	
} else if($typeBy=='byItem'){
	//$selectedItem = array_merge($ItemID1, $ItemID2);
	$selectedItem = array_merge((array)$ItemID1, (array)$ItemID2);
	$selectedItemText = explode(',',$SelectedItemText);
	
	for($i=0; $i<sizeof($selectedItem); $i++) {
		$temp = $ldiscipline->getGMRankingData('item', $selectedItem[$i], $conds, $groupBy);
		
		
		list($lvlName, $countTotal, $className, $catid, $catname, $itemid, $itemname, $meritType, $stdname) = $temp[$i];
		
		$itemData = $ldiscipline->getConductItemDataByID($selectedItem[$i], $selectedItemTextAry[$i]);		
		$meritTypeByItem = $itemData['RecordType'];
		
		$tempTable .= ($meritTypeByItem==1) ? "<br><span class='tabletext row_print'>".$i_Discipline_GoodConduct : "<br><span class='tabletext row_print'>".$i_Discipline_Misconduct;
			
		$GMItem = $ldiscipline->getGMItemInfoByCategoryID($selectedItem[$i], $selectedItemText[$i]);
        if(sizeof($GMItem)==0) {
                $subjectName = " > ".$ldiscipline->RETURN_CONDUCT_REASON($selectedItem[$i], 2);
        }
        $tempTable .= ($subjectName) ? $subjectName."<br>" : (" > ".$GMItem['CategoryName']." > ".$GMItem['ItemName']."<br>");
		
		if(sizeof($temp)>0) {
			$tempTable .= $tableTop;
			for($j=0; $j<sizeof($temp) && $j<$rankRange; $j++) {
				
				switch($rankTarget) {
					case("class") : $showName = $temp[$j][2]; break;
					case("form") : $showName = $temp[$j][0]; break;
					case("student") : $showName = $temp[$j][8]; break;
					default: break;
				}	
				
				$k = $j+1;
				$css = ($j%2)+1;
				$tempTable .= "<tr class='row_print'>";
				$tempTable .= "<td class='tabletext row_print' width='20%'>{$k}</td>";
				$tempTable .= "<td class='tabletext row_print' width='40%'>";
				$tempTable .= "{$showName}</td>";
				$tempTable .= "<td class='tabletext row_print' width='40%'>{$temp[$j][1]}</td>";
				$tempTable .= "</tr>";
			}
			$tempTable .= $tableBottom;
		} else {
			$tempTable .= $tableNoContent;	
		}
		
	}	
		
}
# end of table content
/*
$tableContent = "<html><head>";
$tableContent .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
$tableContent .= "<title></title>";
$tableContent .= "<link href='{$PATH_WRT_ROOT}/templates/{$LAYOUT_SKIN}/css/print.css' rel='stylesheet' type='text/css'>";
$tableContent .= "</head><body>";
*/
$tableContent .= "<table width='100%' align='center' class='print_hide'><tr>";
$tableContent .= "<td align='right'>";
$tableContent .= $linterface->GET_BTN($button_print, "button","javascript:window.print()");
$tableContent .= "</td></tr></table>";
$tableContent .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$tableContent .= "<tr><td valign='top'>";
$tableContent .= "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
$tableContent .= "<tr><td class='tabletext'>{$date} {$i_general_most} {$meritTitle} {$criteria}</td></tr><tr><td>";
/*
$tableContent .= "<table width='100%' border='0' cellpadding='4' cellspacing='1' class='tableborder_print'>";
$tableContent .= "<tr class='tabletop_print'>";
$tableContent .= "<td>".$iDiscipline['Rank']."</td>";
$tableContent .= "<td>".$i_Discipline_Class."</td>";
$tableContent .= "<td>".$i_Discipline_No_of_Records."</td>";
$tableContent .= "</tr>";


if(sizeof($result)==0) {
	$tableContent .= "<tr class='row_print'>";
	$tableContent .= "<td class='tabletext row_print' colspan='3' align='center' height='40'>{$i_no_record_exists_msg}</td>";
	$tableContent .= "</tr>";
}

for($i=0;$i<sizeof($result);$i++) {
	$k = $i+1;
	$css = ($i%2)+1;
	$tableContent .= "<tr class='row_print'>";
	$tableContent .= "<td class='tabletext row_print'>{$k}</td>";
	$tableContent .= "<td class='tabletext row_print'>";
	$tableContent .= ($rankTarget=='form') ? "Form " : "";
	$tableContent .= "{$result[$i][0]}</td>";
	$tableContent .= "<td class='tabletext row_print'>{$result[$i][1]}</td>";
	$tableContent .= "</tr>";
}
$tableContent .= "</table>";
*/
$tableContent .= $tempTable;
$tableContent .= "</td>";
$tableContent .= "</tr>";
$tableContent .= "</table>";
$tableContent .= "</td>";
$tableContent .= "</tr>";
$tableContent .= "</table>";
//$tableContent .= "</body></html>";

intranet_closedb();

echo $tableContent;
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

?>
