<?php
# using:
/*
 * 2019-05-13  (Bill)
 * - Prevent SQL Injection
 */

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$year = IntegerSafe($year);
$semester = IntegerSafe($semester);
$target = IntegerSafe($target);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-ClassSummary-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html();

for($i=0; $i<sizeof($target); $i++)
{
	if ($level == 1)
	{
		$sql = "SELECT YearName FROM YEAR WHERE YearID = '".$target[$i]."'";
		$result = $ldiscipline->returnVector($sql);
		$target[$i] = $result[0];
	}
	else
	{
		$target[$i] = $lclass->getClassName($target[$i]);
	}
}

if($radioPeriod=="YEAR")
{
	if($semester=='' || $semester==0)
	{
		$display = $i_status_all;
	}
	else
	{
		$display = $ldiscipline->getTermNameByTermID($semester);	
	}
}
else
{
	$display = $textFromDate.' '.$i_To.' '.$textToDate;
}

if($year =='' || $year =='0')
{
	$display_yr = $i_status_all;
}
else
{
	$display_yr = $ldiscipline->getAcademicYearNameByYearID($year);	
}

$functionbar = "&nbsp; $i_Profile_Year: $display_yr";
$functionbar .= "<br>&nbsp; $i_SettingsSemester: $display";
$functionbar .= "<br>&nbsp; ".(($level == 1) ? $i_ClassLevel : $i_ClassName).": ".implode(",", $target);

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
?>

<style type='text/css' media='print'>
 .print_hide {display:none;}
</style>

<br>
<table border="0" width="100%">
	<tr class='print_hide'>
		<td align="right">
			<?=$linterface->GET_BTN($button_print, "button","javascript:window.print()")?>
		</td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="tabletext"><td><font size='+0'><u><?=$i_Discipline_System_Report_Type_Class."(".(($level == 1) ? $i_Discipline_System_Report_AwardPunish_ClassLevel : $i_Discipline_System_Report_AwardPunish_ClassRecord).")" ?></u></font></td></tr>
	<tr class="tabletext"><td><?= $functionbar ?><p></td></tr>
	<tr class="tabletext"><td><?= urldecode($content); ?></td></tr>
</table>
<br />
<?

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>