<?php
// modifying by: Bill 

############### Change Log [Start]
#
#	Date:	2017-11-17	(Bill)	[2017-0926-1041-46054]
#			Hide Class Summary Tab	($sys_custom['eDiscipline']['HYKHideAPReport'])
#
#	Date:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#			Hide options related GM	($sys_custom['eDiscipline']['HideAllGMReport'])
#
#	Date:	2015-07-17	Bill	[2014-1103-1422-59164]
#			Added Display Remarks option for both GM and AP record table
#
###############################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

if (!$lstudentprofile->is_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='1' id='merit1' onClick=\"checkClickBox(this,'all_award')\"><label for='merit1'>".$i_Merit_Merit."</label>";
}
if (!$lstudentprofile->is_min_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='2' id='merit2' onClick=\"checkClickBox(this,'all_award')\"><label for='merit2'>".$i_Merit_MinorCredit."</label>";
}
if (!$lstudentprofile->is_maj_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='3' id='merit3' onClick=\"checkClickBox(this,'all_award')\"><label for='merit3'>".$i_Merit_MajorCredit."</label>";
}
if (!$lstudentprofile->is_sup_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='4' id='merit4' onClick=\"checkClickBox(this,'all_award')\"><label for='merit4'>".$i_Merit_SuperCredit."</label>";
}
if (!$lstudentprofile->is_ult_merit_disabled) {	
	$meritType .= "<input name='merit[]' type='checkbox' value='5' id='merit5' onClick=\"checkClickBox(this,'all_award')\"><label for='merit5'>".$i_Merit_UltraCredit."</label>";
}

$demeritType .= "<input name='demerit[]' type='checkbox' value='0' id='demerit0' onClick=\"checkClickBox(this,'all_punishment')\"><label for='demerit0'>".$i_Merit_Warning."</label>";
if (!$lstudentprofile->is_black_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-1' id='demerit1' onClick=\"checkClickBox(this,'all_punishment')\"><label for='demerit1'>".$i_Merit_BlackMark."</label>";
}
if (!$lstudentprofile->is_min_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-2' id='demerit2' onClick=\"checkClickBox(this,'all_punishment')\"><label for='demerit2'>".$i_Merit_MinorDemerit."</label>";
}
if (!$lstudentprofile->is_maj_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-3' id='demerit3' onClick=\"checkClickBox(this,'all_punishment')\"><label for='demerit3'>".$i_Merit_MajorDemerit."</label>";
}
if (!$lstudentprofile->is_sup_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-4' id='demerit4' onClick=\"checkClickBox(this,'all_punishment')\"><label for='demerit4'>".$i_Merit_SuperDemerit."</label>";
}
if (!$lstudentprofile->is_ult_demer_disabled) {	
	$demeritType .= "<input name='demerit[]' type='checkbox' value='-5' id='demerit5' onClick=\"checkClickBox(this,'all_punishment')\"><label for='demerit5'>".$i_Merit_UltraDemerit."</label>";
}

# School Year Menu #
$SchoolYearID = Get_Current_Academic_Year_ID();

$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYear1Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear1ID' id='SchoolYear1ID'", "", $SchoolYear1ID);
$selectSchoolYear2Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear2ID' id='SchoolYear2ID'", "", $SchoolYear2ID);

$selectSchoolYear1Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear1ID' onFocus='form1.award_punish_period[0].checked=true;' onChange='changeTerm(this.value, \"semester1\")'", $i_Discipline_System_Award_Punishment_All_School_Year, $SchoolYearID);
$selectSchoolYear2Selection = $linterface->GET_SELECTION_BOX($years, "name='SchoolYear2ID' onFocus='form1.gdConduct_miscondict_period[0].checked=true;' onChange='changeTerm(this.value, \"semester2\")'", $i_Discipline_System_Award_Punishment_All_School_Year, $SchoolYearID);

# Semester Menu #
$SemesterMenu .= "<option value='WholeYear'";
$SemesterMenu .= ($semester!='WholeYear') ? "" : " selected";
$SemesterMenu .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";

# Class #
$select_class = $ldiscipline->getSelectClassWithWholeForm("name=\"targetClass\" onChange=\"showResult(this.value)\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, $i_general_please_select);

// $TAGS_OBJ[] = array($eDiscipline['StudentReport'], "");
if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View"))
{
	$TAGS_OBJ[] = array($iDiscipline['Reports_Child_PersonalReport'],"../personal_report/");
	$TAGS_OBJ[] = array($eDiscipline['StudentReport'],"../student_report/",1);
}
if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-ClassSummary-View") && !$sys_custom['eDiscipline']['HYKHideAPReport'])
{
	$TAGS_OBJ[] = array($eDiscipline['ClassSummary'],"../class_summary/class_summary.php");
	if($sys_custom['Discipline_show_AP_converted_data'])	
		$TAGS_OBJ[] = array($Lang['eDiscipline']['ClassSummary_ConvertedData'],"../class_summary/class_summary_converted.php");
}

# Menu Highlight Setting
$CurrentPage = "Reports_MasterReport";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();

$i = 1;
?>

<script language="javascript">
function checkClickBox(obj, ref) {
	var field = "";
	if(obj.checked==false) {
		field = eval("form1." + ref);
		field.checked = false;
	}
}

function resetOption(obj, element_name) {
	var field = "";
	var len = document.form1.elements.length;
	if(obj.checked == true)	 {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) {
				document.form1.elements[i].checked = true;
			}
		}
	}
	else {
		for(i=0;i<len;i++) {
			if(document.form1.elements[i].name==element_name) {
				document.form1.elements[i].checked = false;
			}
		}
	}
}

var xmlHttp2
var xmlHttp3
function changeTerm(val, field) {
	/*
	if (val.length==0)
	{
		document.getElementById("spanSemester1").innerHTML = "";
		document.getElementById("spanSemester1").style.border = "0px";
		document.getElementById("spanSemester2").innerHTML = "";
		document.getElementById("spanSemester2").style.border = "0px";
		return
	}
	*/
	
	xmlHttp2 = GetXmlHttpObject()
	xmlHttp3 = GetXmlHttpObject()
	if (xmlHttp2==null || xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	
	var url = "";
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&field=" + field;
	
	if(field=='semester1') {
		xmlHttp2.onreadystatechange = stateChanged2 
		xmlHttp2.open("GET",url,true)
		xmlHttp2.send(null)
	}
	else {
		xmlHttp3.onreadystatechange = stateChanged3 
		xmlHttp3.open("GET",url,true)
		xmlHttp3.send(null)
	}
}

function stateChanged2() 
{
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{
		document.getElementById("spanSemester1").innerHTML = xmlHttp2.responseText;
		document.getElementById("spanSemester1").style.border = "0px solid #A5ACB2";
	}
}

function stateChanged3()
{
	<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{
		document.getElementById("spanSemester2").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester2").style.border = "0px solid #A5ACB2";
	}
	<? } ?>
}

function goCheck(form1) {
	var flag = 0;
	var temp = "";
	var region1 = 0;
	var region2 = 0;
	var region3 = 0;
	var region4 = 0;
	var choiceSelected = 0;
	var choice = "";
	
	// region 1
	if(form1.all_award.checked==true || form1.all_punishment.checked==true) {
		region1 = 1;
		form1.award_punishment_flag.value = 1;
	}
	
	var len = form1.elements.length;
	for(i=0;i<len;i++) {
		//region2 = (form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) ? 1 : 0;
		//region2 = (form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) ? 1 : 0;
		if(form1.elements[i].name=='merit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}
		if(form1.elements[i].name=='demerit[]' && form1.elements[i].checked==true) {
			region2 = 1;	
			form1.award_punishment_flag.value = 1;
		}
		/*
		if(form1.elements[i].name=='gdConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			form1.gdConduct.value = 1;
		}
		if(form1.elements[i].name=='misConductCategory[]' && form1.elements[i].checked==true) {
			region4 = 1;
			form1.misconduct.value = 1;
		}
		*/
	}
	
	// region 2
	/*
	for(i=0;i<form1.merit.length;i++) {
		temp = eval("form1.merit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
	for(i=0;i<form1.demerit.length;i++) {
		temp = eval("form1.demerit");
		if(temp[i].checked==true) {
			region2 = 1;	
		}
	}
	*/
	
	<?
	if(!$sys_custom['eDiscipline']['HideAllGMReport'])
	{ ?>
	// region 4
	choiceSelected = 0;
	//choice = document.getElementById('goodconductid[]');
	choice = $('[name="goodconductid[]"]').get()[0];
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true) {
			region4 = 1;
			form1.gdConduct.value = 1;
		}
	}
	choiceSelected = 0;
	//choice = document.getElementById('misconductid[]');
	choice = $('[name="misconductid[]"]').get()[0];
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true) {
			region4 = 1;
			form1.misconduct.value = 1;
		}
	}
	<? } ?>
	
	if(form1.targetClass.value == '#') {
		alert("<?=$i_alert_pleaseselect?><?=$i_Discipline_Class?>");	
		return false;
	}
	
	if(region1==0 && region2==0 && region3==0 && region4==0) {
		alert("<?=$i_Discipline_System_Please_Select_Record?>");	
		return false;
	}
	
	if(region1!=0 || region2!=0){
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value=='' && form1.Section1To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.award_punish_period[1].checked==true && ((!check_date(form1.Section1Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section1To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.award_punish_period[1].checked==true && (form1.Section1Fr.value > form1.Section1To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
	
	<?
	if(!$sys_custom['eDiscipline']['HideAllGMReport'])
	{ ?>
	if(region3!=0 || region4!=0){
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value=='' && form1.Section2To.value=='')) {
			alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
			return false;
		}	
		if(form1.gdConduct_miscondict_period[1].checked==true && ((!check_date(form1.Section2Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section2To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>'))))
		{
			return false;
		}
		if(form1.gdConduct_miscondict_period[1].checked==true && (form1.Section2Fr.value > form1.Section2To.value)) {
			alert("<?=$i_Discipline_System_Reports_Invalid_Date_Compare?>");
			return false;	
		}
	}
	<? } ?>
	
	/*	
	if(form1.all_gd_conduct.checked==true) {
		form1.gdConduct.value = 1;	
	}
	if(form1.all_misconduct.checked==true) {
		form1.misconduct.value = 1;	
	}
	*/
	//alert(form1.award_punishment_flag.value + "/" + form1.gdConduct.value + "/" + form1.misconduct.value);
	return true;
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function DeSelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = false;
	}
}
</script>

<script language="javascript">
var xmlHttp
function showResult(str)
{
	if (str.length==0)
	{
		document.getElementById("studentID").innerHTML = "";
		document.getElementById("studentID").style.border = "0px";
		return
	}
	
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	}
	
	var url = "";
	url = "../../get_live.php";
	url = url + "?targetClass=" + str
	
	xmlHttp.onreadystatechange = stateChanged 
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{
		document.getElementById("studentID").innerHTML = xmlHttp.responseText;
		document.getElementById("studentID").style.border = "0px solid #A5ACB2";
	}
}

function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function setRadioSelection(f) {
	if(f==0)
		document.form1.award_punish_period[1].checked==true;
	if(f==1)
		document.form1.gdConduct_miscondict_period[1].checked==true;
}
</script>

<form name="form1" method="post" action="student_report_view.php" onSubmit="return goCheck(this)">
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
				<tr>
					<td height="20" class="tabletextremark"><i><?=$i_Discipline_System_Reports_Report_Option?></i></td>
				</tr>
			</table>
			
			<?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Reports_Instruction_Msg) ?>
			
			<table class="form_table_v30">
				<tr>
					<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> - <?=$i_Discipline_Student?> -</i></td>
				</tr>
				<tr>
					<td valign="top" rowspan="2" nowrap="nowrap" class="field_title"><?=$i_Discipline_Class?><span class="tabletextrequire">*</span></td>
					<td class="navigation">
						<?=$select_class?>
					</td>
				</tr>
				<tr valign="top">
					<td><div id='studentID' style='position:absolute; width:280px; height:20px; z-index:0;'></div>
						<select name="studentID" class="formtextbox"></select>
					</td>
				</tr>
				<tr valign="top">
					<td colspan="2">
						<input type="checkbox" name="NotShowStudentWithoutRecord" id="NotShowStudentWithoutRecord" value="1">
						<label for="NotShowStudentWithoutRecord"><?=$Lang['eDiscipline']['NotToShowStudentWithoutRecord']?></label>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i><?=$i_Discipline_System_Reports_First_Section?></i></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$i_general_show?> <font color="green">#</font></td>
					<td width="80%">
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td valign="top"><input type="checkbox" name="all_award" id="all_award" onClick="resetOption(this,'merit[]')">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_merit.gif" width="20" height="20" align="absmiddle"> <label for="all_award"><?=$i_Discipline_System_Reports_All_Awards?></label> 
								</td>
								<td>
									<?=$meritType?>
								</td>
							</tr>
							<tr>
								<td valign="top"><input type="checkbox" name="all_punishment" id="all_punishment" onClick="resetOption(this,'demerit[]')">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_demerit.gif" width="20" height="20" align="absmiddle"><label for="all_punishment"><?=$i_Discipline_System_Reports_All_Punishment?></label> </td>
								<td>
									<?=$demeritType?>
								</td>
							</tr>
						</table>
						<br>
						<input type="checkbox" name="waive_record1" id="waive_record1" value="1"><label for="waive_record1"><?=$i_Discipline_System_Reports_Include_Waived_Record?></label>
						<br>
						<input type="checkbox" name="show_conduct_mark" id="show_conduct_mark" value="1"><label for="show_conduct_mark"><?= ($ldiscipline->UseSubScore) ? $Lang['eDiscipline']['displayConductMarkAndSubScore'] : $Lang['eDiscipline']['displayConductMark']?></label>
						<?php if(!$sys_custom['sdbnsm_disciplinev12_student_report_cust']) { ?>
							<br>
							<input type="checkbox" name="show_remarks1" id="show_remarks1" value="1"><label for="show_remarks1"><?= $Lang['eDiscipline']['displayRecordRemarks'] ?></label>
						<?php } ?>
					</td>
				</tr>
				<tr valign="top">
					<td height="57" valign="top" nowrap="nowrap" class="field_title"><?=$iDiscipline['Period']?></td>
					<td>
						<table border="0" cellspacing="0" cellpadding="3" >
							<tr>
								<td height="30" colspan="6" onFocus="form1.award_punish_period[0].checked=true;" onClick="form1.award_punish_period[0].checked=true;">
									<input name="award_punish_period" type="radio" id="award_punish_period[0]" value="1" checked>
									<?=$i_Discipline_School_Year?>
									<!--<select name="SchoolYear1" onFocus="form1.award_punish_period[0].checked=true;" onChange="changeTerm(this.value, 'semester1')"><?=$selectSchoolYear?></select>//-->
									<?=$selectSchoolYear1Selection?>
									<?=$i_Discipline_Semester?> <span id="spanSemester1"><select name='semester1' onFocus="form1.award_punish_period[0].checked=true;"><?=$SemesterMenu?></select></span>
								</td>
							</tr>
							<tr>
								<td><input name="award_punish_period" type="radio" id="award_punish_period[1]" value="2"><?=$iDiscipline['Period_Start']?></td>
								<td><!--<input name="Section1Fr" type="text" class="tabletext" onFocus="form1.award_punish_period[1].checked=true;"/>--></td>
								<td align="center" onClick="form1.award_punish_period[1].checked=true;" onFocus="form1.award_punish_period[1].checked=true;"><?=$linterface->GET_DATE_PICKER("Section1Fr",$Section1Fr)?><?/*=$linterface->GET_CALENDAR("form1", "Section1Fr")*/?></td>
								<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
								<td><!--<input name="Section1To" type="text" class="tabletext" onFocus="form1.award_punish_period[1].checked=true;"/>--></td>
								<td align="center" onClick="form1.award_punish_period[1].checked=true;" onFocus="form1.award_punish_period[1].checked=true;"><?=$linterface->GET_DATE_PICKER("Section1To",$Section1To)?><?/*=$linterface->GET_CALENDAR("form1", "Section1To")*/?></td>
							</tr>
						</table>
					</td>
				</tr>
				<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
				<tr>
					<td colspan="2" valign="middle" nowrap="nowrap" class="form_sep_title"><i> <?=$i_Discipline_System_Reports_Second_Section?></i></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eDiscipline']['DisplayMode']?></td>
					<td>
						<input type="radio" name="displayMode" id="displayMode1" value="1" onclick="$('#show_remarks2')[0].disabled=true;" checked><label for="displayMode1"><?=$Lang['eDiscipline']['DisplayInSummary']?></label> 
						<input type="radio" name="displayMode" id="displayMode2" value="2" onclick="$('#show_remarks2')[0].disabled='';" ><label for="displayMode2"><?=$Lang['eDiscipline']['DisplayInDetails']?></label></td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="field_title"><?=$i_general_show?> <font color="green">#</font></td>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="3">
							<!--
							<tr>
								<td valign="top"><input type="checkbox" name="all_gd_conduct" id="all_gd_conduct" onClick="resetOption(this, 'gdConductCategory[]')">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"><label for="all_gd_conduct"><?=$i_Discipline_System_Reports_All_Good_Conducts?></label>
								</td>
								<td align="left">
									<?= $ldiscipline->showMeritOption()?>
								</td>
							</tr>
							<tr>
								<td valign="top"><input type="checkbox" name="all_misconduct" id="all_misconduct" onClick="resetOption(this, 'misConductCategory[]')">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"><label for="all_misconduct"><?=$i_Discipline_System_Reports_All_Misconduct?></label><br>
								</td>
								<td align="left">
									<?= $ldiscipline->showDemeritOption()?>
								</td>
							</tr>
							//-->
							<tr>
								<td valign="top" width="50%">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_gd_conduct.gif" width="20" height="20" border="0" align="absmiddle"><label for="all_gd_conduct"><?=$i_Discipline_GoodConduct?></label>
								</td>
								<td align="left">
									<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/ediscipline/icon_misconduct.gif" width="20" height="20" border="0" align="absmiddle"><label for="all_misconduct"><?=$i_Discipline_Misconduct?></label>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<!--<?= $ldiscipline->showMeritOption()?></td>//-->
									<?= $ldiscipline->showGoodconductList()?><br>
									<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['goodconductid[]']); return false;") ?>&nbsp;
									<?= $linterface->GET_BTN($Lang['eDiscipline']['ButtonUnselectAll'], "button", "DeSelectAll(this.form.elements['goodconductid[]']); return false;") ?><br>
									<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
								<td align="left">
									<!--<?= $ldiscipline->showDemeritOption()?>//-->
									<?= $ldiscipline->showMisconductList()?><br>
									<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['misconductid[]']); return false;") ?>&nbsp;
									<?= $linterface->GET_BTN($Lang['eDiscipline']['ButtonUnselectAll'], "button", "DeSelectAll(this.form.elements['misconductid[]']); return false;") ?><br>
									<span class="tabletextremark">(<?=$i_Discipline_Press_Ctrl_Key?>)</span>
								</td>
							</tr>
						</table>
						<br>
						<input type="checkbox" name="waive_record2" id="waive_record2" value="2"><label for="waive_record2"><?=$i_Discipline_System_Reports_Include_Waived_Record?></label>
						<br>
						<input type="checkbox" name="show_remarks2" id="show_remarks2" value="1" disabled><label for="show_remarks2"><?= $Lang['eDiscipline']['displayRecordRemarks'] ?></label>
					</td>
				</tr>
				<tr valign="top">
					<td height="57" valign="top" nowrap="nowrap" class="field_title"><?=$iDiscipline['Period']?></td>
					<td>
						<table border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td height="30" colspan="6" onClick="form1.gdConduct_miscondict_period[0].checked=true;" onFocus="form1.gdConduct_miscondict_period[0].checked=true;"><input name="gdConduct_miscondict_period" type="radio" id="gdConduct_miscondict_period[0]" value="1" checked>
									<?=$i_Discipline_School_Year?>
									<!--<select name="SchoolYear2" onFocus="form1.gdConduct_miscondict_period[0].checked=true;" onChange="changeTerm(this.value, 'semester2')"><?=$selectSchoolYear?></select>//-->
									<?=$selectSchoolYear2Selection?>
									<?=$i_Discipline_Semester?> <span id="spanSemester2"><select name='semester2' onFocus="form1.gdConduct_miscondict_period[0].checked=true;"><?=$SemesterMenu?></select></span>
								</td>
							</tr>
							<tr>
								<td><input name="gdConduct_miscondict_period" type="radio" id="gdConduct_miscondict_period[1]" value="2"><?=$iDiscipline['Period_Start']?></td>
								<td><!--<input name="Section2Fr" type="text" class="tabletext"  onFocus="form1.gdConduct_miscondict_period[1].checked=true;"/>--></td>
								<td align="center" onClick="form1.gdConduct_miscondict_period[1].checked=true;" onFocus="form1.gdConduct_miscondict_period[1].checked=true;"><?=$linterface->GET_DATE_PICKER("Section2Fr",$Section2Fr)?><?/*=$linterface->GET_CALENDAR("form1", "Section2Fr")*/?></td>
								<td width="25" align="center"><?=$iDiscipline['Period_End']?></td>
								<td><!--<input name="Section2To" type="text" class="tabletext"  onFocus="form1.gdConduct_miscondict_period[1].checked=true;"/>--></td>
								<td align="center" onClick="form1.gdConduct_miscondict_period[1].checked=true;" onFocus="form1.gdConduct_miscondict_period[1].checked=true;"><?=$linterface->GET_DATE_PICKER("Section2To",$Section2To)?><?/*=$linterface->GET_CALENDAR("form1", "Section2To")*/?></td>
							</tr>
						</table>					
					</td>
				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
</table>

<div class="tabletextremark"><?=$i_general_required_field?></div>
<div class="edit_bottom_v30">
	<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Report, "submit")?>
</div>

<input type="hidden" name="award_punishment_flag" value="">
<input type="hidden" name="gdConduct" value="">
<input type="hidden" name="misconduct" value="">
</form>

<script>
<!--
function init()
{
	var obj = document.form1;
	
	// Class
	obj.targetClass.selectedIndex=3;	// First Class as default
	tempClass=obj.targetClass[obj.targetClass.selectedIndex].value;
	showResult(tempClass);
	
	// AP
	obj.all_award.checked = true;
	resetOption(obj.all_award,"merit[]");
	obj.all_award.checked = true;
	resetOption(obj.all_award,"merit[]");
	obj.all_punishment.checked = true;
	resetOption(obj.all_punishment,"demerit[]");
	obj.waive_record1.checked = true;
	
	<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
		// GM
		SelectAll(obj.elements['goodconductid[]']);
		SelectAll(obj.elements['misconductid[]']);
		obj.waive_record2.checked = true;
	<? } ?>
	
	// Semester
	changeTerm('<?=$SchoolYearID?>', 'semester1');
	setTimeout("changeTerm('<?=$SchoolYearID?>', 'semester2')",1000);
}

init();
//-->
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>