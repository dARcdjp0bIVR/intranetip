<?php
// Using: Bill

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !!!	pls use utf-8 editor		!!!
# !!!	其他記過事項, 其他記功事項	!!!
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# Personal Report
# - show number of merit, demerit pts

##########################################################################
# Report type:
# 1.Punhishment Record
# 2.Award Record
# 3.Detention Attendance Record		<== not support in v12
# 4.Award/Punhishment Record
# 5.Detention Punishment Record		<== not support in v12
# 6.Conduct Mark Change Log			<== not in IP25
# 7.Subscore Change Log			
# 8.Misconduct Record
# 9.Good conduct Record				<== new in v12
# 10.Good/Misconduct Record			<== new in v12
##########################################################################

#######################
#
#	Date:	2017-11-17	(Bill)	[2017-0926-1041-46054]
#		- Hide Class Summary Tab	($sys_custom['eDiscipline']['HYKHideAPReport'])
#		- Hide AP Count Summary Table
#
#	Date:	2017-10-31 (Bill)	[2017-0403-1552-54240]
#		- hide GM related options ($sys_custom['eDiscipline']['HideAllGMReport'])
#
#	Date:	2016-11-01 (Bill)	[2016-0715-0934-05096]		($sys_custom['eDiscipline']['PersonalReportCommonOverallConversion'])
#		- add Total no. (Overall Records After Conversion) row under Comparison in Different Periods section
#			(only for Conversion Method : Common settings for all categories)
#
#	Date:	2016-08-08 (Bill)	[2016-0729-1028-19073]
#		- add accumulated conduct score column
#
#	Date:	2016-07-04 (Bill)	[2016-0526-1018-39096]
#		- add drop down list to select GM/AP item category
#
#	Date:	2016-05-24 (Bill)	[2016-0519-1111-59096]
#		- added drop down list to set date range according to semester
#		- display accumulated AP records count same as Yellow Paper
#		  ($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
#
#	Date:	2016-04-06 (Bill)	[2015-0807-1048-01073]
#		Cust: auto set Start Date and End Date ($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport'])
#
#	Date:	2015-04-09 Bill
#		change table Punhishment Record & Award Record header "Add" -> "Added" [2015-0330-1119-38207]
#
#	Date:	2013-08-20 Carlos
#		$sys_custom['eDiscipline']['yy3'] - display overflow conduct remark for overflowwed award/punishment items
#
#	Date:	2013-05	YatWoon
#		implement yy3 customization ($sys_custom['eDiscipline']['yy3'])
#
#	Date:	2013-05-16	YatWoon
#		remove "Year" selection (due to misconfuse with Date range)
#
#######################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();
$lclass = new libclass();
$lsp = new libstudentprofile();

if ($targetID == "")
{
    if ($targetType==1)
    {
        $response = "$targetClass - $targetID";
    }
    else if ($targetType==2)
    {
         $sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = '$targetLogin' AND RecordType=2 ";
         $temp = $ldiscipline->returnVector($sql);
         $targetID = $temp[0];
         if ($targetID == "")
         {
             header("Location: personal.php");
             exit();
         }
         $response =  $targetLogin;
    }
    else
    {
	    if($targetClass)
	    {
		    # Retrieve the fist student
		    $stu_list = $lclass->getClassStudentListOrderByClassNo($targetClass);
		    if(sizeof($stu_list)) {
		    	$targetID = $stu_list[0];
		    }
	    }
	    else
	    {
        	header("Location: personal.php");
        	exit();
    	}
    }
}

$CurrentPage = "Reports_MasterReport";
$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
//$TAGS_OBJ[] = array($iDiscipline['Reports_Child_PersonalReport']);
if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View"))
{
	$TAGS_OBJ[] = array($iDiscipline['Reports_Child_PersonalReport'],"../personal_report/",1);
	$TAGS_OBJ[] = array($eDiscipline['StudentReport'],"../student_report/");
}
if($ldiscipline->CHECK_ACCESS("Discipline-REPORTS-ClassSummary-View") && !$sys_custom['eDiscipline']['HYKHideAPReport'])
{
	$TAGS_OBJ[] = array($eDiscipline['ClassSummary'],"../class_summary/class_summary.php");
	if($sys_custom['Discipline_show_AP_converted_data']) {
		$TAGS_OBJ[] = array($Lang['eDiscipline']['ClassSummary_ConvertedData'],"../class_summary/class_summary_converted.php");
	}
}

$linterface->LAYOUT_START();

# Retrieve Student Name
$lu_student = new libuser($targetID);
$student_name = $lu_student->UserName();

$class_name = $lu_student->ClassName;
$class_num = $lu_student->ClassNumber;
if($class_name) {
	$targetClass = $class_name;
}

# Get Year
if ($yearID=="")
	$yearID = Get_Current_Academic_Year_ID();

$year = $ldiscipline->getAcademicYearNameByYearID($yearID);
/*
$APyear = $ldiscipline->getAPSchoolYear();
$GMyear = $ldiscipline->getGMSchoolYear();
$years = array();
for($i=0; $i<sizeof($APyear); $i++) {
	$years[$i] = array($APyear[$i][0], $APyear[$i][1]);
}

$pos = sizeof($years);
for($i=0; $i<sizeof($GMyear); $i++) {
	$yearInList = false;
	for($j=0; $j<sizeof($APyear); $j++) {
		if($APyear[$j][0]==$GMyear[$i][0])
			$yearInList = true;
	}
	if($yearInList==false) {
		//$years[$pos] = $GMyear[$i][1];
		$years[$pos] = array($GMyear[$i][0], $GMyear[$i][1]);
		$pos++;
	}
}
*/
$years = $ldiscipline->returnAllYearsSelectionArray();
$yearMenu = $linterface->GET_SELECTION_BOX($years, "name='yearID' id='yearID' onChange='changeTerm(this.value)'", "", $yearID);

//$yearMenu = $linterface->GET_SELECTION_BOX($years, "name='yearID'", "", $yearID);
	/*
# Year Menu
$yearMenu = "<select name='year' onChange=\"this.form.action='personal_check.php';this.form.submit()\">";
foreach($years as $y) {
	$yearMenu .= "<option value='$y'";
	$yearMenu .= ($y==$year) ? " selected" : "";
	$yearMenu .= ">".$y."</option>";
}
$yearMenu .= "</select>";
*/
	
# Get Semester Selection
//$yearID = $ldiscipline->getAcademicYearIDByYearName($year);
$sql = "SELECT YearTermID, YearTermNameEN, YearTermNameB5, DATE_FORMAT(TermStart,'%Y-%m-%d'), DATE_FORMAT(TermEnd,'%Y-%m-%d') FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID=$yearID ORDER BY TermStart";
$semResult = $ldiscipline->returnArray($sql, 3);

$select_sem = "<select name='semester' onChange=\"this.form.action='personal_check.php';this.form.submit()\">";
$select_sem .= "<option value=''";
$select_sem .= ($semester!='') ? "" : " selected";
$select_sem .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
for($i=0; $i<sizeof($semResult); $i++) {
	list($id, $nameEN, $nameB5) = $semResult[$i];
	$semName = Get_Lang_Selection($nameB5, $nameEN);
	$select_sem .= "<option value='$id'";
	$select_sem .= ($semester==$id) ? " selected" : "";
	$select_sem .= ">$semName</option>";
}
$select_sem .= "</select>";

# Date Range
$ts = time();

// [2015-0807-1048-01073] auto set Start Date and End Date from Management page
if($sys_custom['eDiscipline']['MoPuiChingRedirToPersonalReport'] && $fromMgmt==1)
{
	if($targetSemester && $targetSemester=="WholeYear"){
		$targetSemester = "";
	}
	$targetSemesterRange = $targetSemester;
	
	$startdate = substr(getStartDateOfAcademicYear($targetSchoolYear, $targetSemester), 0, 10);
	$enddate = substr(getEndDateOfAcademicYear($targetSchoolYear, $targetSemester), 0, 10);
	
	unset($_GET["fromMgmt"]);
	unset($_GET["targetSchoolYear"]);
	unset($_GET["targetSemester"]);
}

//if ($startdate == "")
{
    //$startdate = date('Y-m-d',getStartOfAcademicYear($ts));
    //$startdate = date('Y-m-d',getStartDateOfAcademicYear($yearID));
    //$startdate = ($startdate=="")  ? substr(getStartDateOfAcademicYear($yearID, $semester),0,10) : $startdate;
    $startdate = ($startdate=="")  ? substr(getStartDateOfAcademicYear($yearID, $selectSemester),0,10) : $startdate;
    
}
//if ($enddate == "")
{
    //$enddate = date('Y-m-d',getEndOfAcademicYear($ts));
    //$enddate = date('Y-m-d',getEndDateOfAcademicYear($yearID));
    $enddate = ($enddate=="") ? substr(getEndDateOfAcademicYear($yearID, $selectSemester),0,10) : $enddate;

}

$label_year = "$i_Profile_Year ($year) ";
$label_semester = ($selectSemester!='') ? "$i_SettingsSemester (".$ldiscipline->getTermNameByTermID($selectSemester).") " : $i_SettingsSemester;

# Get data from DB
$date_count = $ldiscipline->retrieveStudentMeritTypeCountByDate($targetID,$startdate,$enddate);
$year_count = $ldiscipline->retrieveStudentMeritTypeByYearSemester($targetID, $yearID, "");
$sem_count  = $ldiscipline->retrieveStudentMeritTypeByYearSemester($targetID, $yearID, $selectSemester);

# Build Next and Prev Record links
$student_name_field = getNameFieldWithClassNumberByLang();
$sql = "SELECT UserID, $student_name_field FROM INTRANET_USER WHERE RecordType = 2 AND RecordStatus = 1 AND ClassName = '$class_name' ORDER BY ClassNumber";
$result = $ldiscipline->returnArray($sql, 2);
for ($i=0; $i<sizeof($result); $i++)
{
    list($t_id, $t_sname) = $result[$i];
    $ClassList[$i] = $t_id;
    $StudentName[$t_id] = $t_sname;
}
if($targetID)
{
	$curr_index = array_search($targetID, $ClassList);
	$next_index = ($curr_index+1 > sizeof($ClassList)-1) ? 0 : $curr_index+1;
	$prev_index = ($curr_index-1 < 0) ? sizeof($ClassList)-1 : $curr_index-1;
}
$select_class = $lclass->getSelectClass("name=\"targetClass\" id=\"targetClass\" onChange=\"document.form1.targetID.selectedIndex = -1; this.form.submit();\"",$targetClass);

if ($targetClass != "" && $targetID) {
    $select_students = $lclass->getStudentSelectByClass($targetClass,"name=\"targetID\"  id=\"targetID\" onChange=\"this.form.action='personal_check.php';this.form.submit()\" ", $targetID);
}
else {
	$select_students =  "<SELECT name=\"targetID\" id=\"targetID\"  onChange=\"this.form.action='personal_check.php';this.form.submit()\" >
                		<OPTION value='' $empty_selected> -- $button_select -- </OPTION>
                		</SELECT>\n";
}

// [2016-0519-1111-59096] add drop down list to set date range according to semester
if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport']){
	$select_semRange = "<select name='targetSemesterRange' onChange=\"updateDateRange(this.value);\">";
	$select_semRange .= "<option value=''";
	$select_semRange .= ($targetSemesterRange=='') ? " selected" : "";
	$select_semRange .= ">".$i_Discipline_System_Award_Punishment_Whole_Year."</option>";
	for($i=0; $i<sizeof($semResult); $i++) {
		list($id, $nameEN, $nameB5) = $semResult[$i];
		$semName = Get_Lang_Selection($nameB5, $nameEN);
		$select_semRange .= "<option value='$id'";
		$select_semRange .= ($targetSemesterRange==$id) ? " selected" : "";
		$select_semRange .= ">$semName</option>";
	}
	$select_semRange .= "</select>";
}
?>

<br/>
<form name="form1" action="personal_check.php" method="GET">
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
		<td align=left>
		<? if($ClassList[$prev_index]) {?>
			<a href="javascript:nextRecord('<?=$ClassList[$prev_index]?>')" class="tablebottomlink" onMouseOver="MM_swapImage('prevp','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_on.gif',1)" onMouseOut="MM_swapImgRestore()"title="<?=$StudentName[$ClassList[$prev_index]]?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_prev_off.gif" name="prevp" width="11" height="10" border="0" align="absmiddle" id="prevp"> <?=$i_Discipline_System_Previous_Student?></a>
		<? } ?>
		</td>
		<td align=right>
		<? if($ClassList[$next_index]) {?>
			<a href="javascript:nextRecord('<?=$ClassList[$next_index]?>')" class="tablebottomlink" onMouseOver="MM_swapImage('nextp','','<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_on.gif',1)" onMouseOut="MM_swapImgRestore()"title="<?=$StudentName[$ClassList[$next_index]]?>"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_next_off.gif" name="nextp" width="11" height="10" border="0" align="absmiddle" id="nextp"> <?=$i_Discipline_System_Next_Student?></a>
		<? } ?>
		</td>
	</tr>
</table>
<br><br>

<table border="0" cellpadding="2" cellspacing="0" width="90%">
<tr>
	<td class="tabletext formfieldtitle" valign="top" width="30%"><?=$i_ClassNameNumber?> <span class="tabletextrequire">*</span></td>
	<td class="tabletext"><?=$select_class?><?=$select_students?></td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top"> <?=$i_general_startdate?> <span class="tabletextrequire">*</span></td>
	<td class="tabletext">
		<?=$linterface->GET_DATE_PICKER("startdate",$startdate)?><?/*= $linterface->GET_DATE_FIELD("theStartDate", "form1", "startdate", $startdate, 1)*/?>
	</td>
</tr>
<tr>
	<td class="tabletext formfieldtitle" valign="top"><?=$i_general_enddate?> <span class="tabletextrequire">*</span></td>
	<td class="tabletext">
		<?=$linterface->GET_DATE_PICKER("enddate",$enddate)?><?/*= $linterface->GET_DATE_FIELD("theEndDate", "form1", "enddate", $enddate, 1)*/?>
	</td>
</tr>
<? /* ?>
<tr class="tabletext">
	<td width="30%" class="formfieldtitle" valign="top"><?=$i_Profile_Year?> <span class="tabletextrequire">*</span></td>
	<td><?=$yearMenu?></td>
</tr>
<tr class="tabletext">
	<td class="formfieldtitle" valign="top"><?=$i_SettingsSemester ?></td>
	<td><span id="spanSemester"><?=$select_sem?></span></td>
</tr>
</table>
<? */ ?>

<? if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport']){ ?>
	<tr>
		<td class="tabletext formfieldtitle" valign="top"><?=$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['Semester']?></td>
		<td class="tabletext"><?=$select_semRange?></td>
	</tr>
<? } ?>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_view, "button", "this.form.action='personal_check.php'; this.form.submit()");?>
		</td>
	</tr>
</table>

<br/>

<?
if(!$sys_custom['eDiscipline']['HYKHideAPReport'])
{
?>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr><td class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
	<span class="sectiontitle"><?= $iDiscipline['compare_different_periods']?></span>
</td></tr>
</table>

<table border="0" cellpadding="2" cellspacing="0" width="96%">
<tr><td>
  <!-- Merit Count --->
  <table width="96%" border="0" cellpadding="2" cellspacing="0">
    <tr class="tablebluetop tabletopnolink">
      <td>&nbsp;</td>
    <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".$i_Merit_UltraCredit."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".$i_Merit_SuperCredit."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".$i_Merit_MajorCredit."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".$i_Merit_MinorCredit."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".$i_Merit_Merit."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".$i_Merit_UltraDemerit."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".$i_Merit_SuperDemerit."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".$i_Merit_MajorDemerit."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".$i_Merit_MinorDemerit."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".$i_Merit_BlackMark."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".$i_Merit_Warning."</td>";
    }
    ?>
    </tr>
    <tr class="tabletext tablebluerow1">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['eDiscipline']['TotalNo']?></td>
    <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".($date_count['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".($date_count['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".($date_count['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".($date_count['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".($date_count['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".($date_count['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".($date_count['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".($date_count['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".($date_count['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".($date_count['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".($date_count['0']+0)."</td>";
    }
    ?>
    </tr>
	<? /* hide academic year info  ?>
    <tr class="tabletext">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$label_year?></td>
          <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($year_count['0']+0)."</td>";
    }
    ?>
    </tr>
	
<?php if(($selectSemester!="")&&($selectSemester!=0)){?>
    <tr class="tabletext tablebluerow1">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$label_semester?></td>
           <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".($sem_count['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".($sem_count['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".($sem_count['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".($sem_count['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".($sem_count['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".($sem_count['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".($sem_count['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".($sem_count['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".($sem_count['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".($sem_count['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".($sem_count['0']+0)."</td>";
    }
    ?>
    </tr>
<?php } ?>
	<? */ ?>
	
	<? ######### Display Converted Data [Start] #################
	if($sys_custom['Discipline_show_AP_converted_data'])
	{
		$data = $ldiscipline->retrieveStudentConvertedMeritTypeCountByDate($targetID, $startdate, $enddate);
	?>
	<tr class="tabletext tablebluerow2">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['eDiscipline']['TotalNoConverted']?></td>
	<?
	    if (!$lsp->is_ult_merit_disabled)
	    {
	         echo "<td>".($data['5']+0)."</td>";
	    }
	    if (!$lsp->is_sup_merit_disabled)
	    {
	         echo "<td>".($data['4']+0)."</td>";
	    }
	    if (!$lsp->is_maj_merit_disabled)
	    {
	         echo "<td>".($data['3']+0)."</td>";
	    }
	    if (!$lsp->is_min_merit_disabled)
	    {
	         echo "<td>".($data['2']+0)."</td>";
	    }
	    if (!$lsp->is_merit_disabled)
	    {
	         echo "<td>".($data['1']+0)."</td>";
	    }
	    if (!$lsp->is_ult_demer_disabled)
	    {
	         echo "<td>".($data['-5']+0)."</td>";
	    }
	    if (!$lsp->is_sup_demer_disabled)
	    {
	         echo "<td>".($data['-4']+0)."</td>";
	    }
	    if (!$lsp->is_maj_demer_disabled)
	    {
	         echo "<td>".($data['-3']+0)."</td>";
	    }
	    if (!$lsp->is_min_demer_disabled)
	    {
	         echo "<td>".($data['-2']+0)."</td>";
	    }
	    if (!$lsp->is_black_disabled)
	    {
	         echo "<td>".($data['-1']+0)."</td>";
	    }
	    if (!$lsp->is_warning_disabled)
	    {
	         echo "<td>".($data['0']+0)."</td>";
	    }
    ?>
    </tr>
	<? }
	   ######### Display Converted Data [End] #################
	
	   ######### Display Converted Overall Data [Start] #################
	if($sys_custom['eDiscipline']['PersonalReportCommonOverallConversion'])	// [2016-0715-0934-05096] 
	{
		$overall_data = $ldiscipline->retrieveStudentConvertedMeritTypeCountByDate($targetID, $startdate, $enddate, 0, 1);
	?>
	<tr class="tabletext tablebluerow">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$Lang['eDiscipline']['TotalNoOverallConverted']?></td>
	<?
	    if (!$lsp->is_ult_merit_disabled)
	    {
	         echo "<td>".($overall_data['5']+0)."</td>";
	    }
	    if (!$lsp->is_sup_merit_disabled)
	    {
	         echo "<td>".($overall_data['4']+0)."</td>";
	    }
	    if (!$lsp->is_maj_merit_disabled)
	    {
	         echo "<td>".($overall_data['3']+0)."</td>";
	    }
	    if (!$lsp->is_min_merit_disabled)
	    {
	         echo "<td>".($overall_data['2']+0)."</td>";
	    }
	    if (!$lsp->is_merit_disabled)
	    {
	         echo "<td>".($overall_data['1']+0)."</td>";
	    }
	    if (!$lsp->is_ult_demer_disabled)
	    {
	         echo "<td>".($overall_data['-5']+0)."</td>";
	    }
	    if (!$lsp->is_sup_demer_disabled)
	    {
	         echo "<td>".($overall_data['-4']+0)."</td>";
	    }
	    if (!$lsp->is_maj_demer_disabled)
	    {
	         echo "<td>".($overall_data['-3']+0)."</td>";
	    }
	    if (!$lsp->is_min_demer_disabled)
	    {
	         echo "<td>".($overall_data['-2']+0)."</td>";
	    }
	    if (!$lsp->is_black_disabled)
	    {
	         echo "<td>".($overall_data['-1']+0)."</td>";
	    }
	    if (!$lsp->is_warning_disabled)
	    {
	         echo "<td>".($overall_data['0']+0)."</td>";
	    }
    ?>
    </tr>
	<? }
	   ######### Display Converted Overall Data [End] #################
	?>
	
    <tr><td>&nbsp;</td></tr>
  </table>
</td></tr>
</table>
<?
}
?>

<? 
/* ### moved to the 1st table ###
### Converted Data (customization report) ###
if($sys_custom['Discipline_show_AP_converted_data']) { ?>
<table border="0" cellpadding="2" cellspacing="0" width="100%">
<tr><td class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
	<span class="sectiontitle"><?= $Lang['eDiscipline']['compare_different_periods_convertedDate']?></span>
</td></tr>
</table>
<table border="0" cellpadding="2" cellspacing="0" width="96%">
<tr><td>
  <table width="96%" border="0" cellpadding="2" cellspacing="0">
    <tr class="tablebluetop tabletopnolink">
      <td>&nbsp;</td>
    <?
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".$i_Merit_UltraCredit."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".$i_Merit_SuperCredit."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".$i_Merit_MajorCredit."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".$i_Merit_MinorCredit."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".$i_Merit_Merit."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".$i_Merit_UltraDemerit."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".$i_Merit_SuperDemerit."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".$i_Merit_MajorDemerit."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".$i_Merit_MinorDemerit."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".$i_Merit_BlackMark."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".$i_Merit_Warning."</td>";
    }
    ?>
    </tr>
    <tr class="tabletext tablebluerow1">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$i_Discipline_System_Report_DateSelected?></td>
	<?	
	$data = $ldiscipline->retrieveStudentConvertedMeritTypeCountByDate($targetID, $startdate, $enddate);
	
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".($data['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".($data['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".($data['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".($data['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".($data['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".($data['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".($data['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".($data['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".($data['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".($data['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".($data['0']+0)."</td>";
    }
    ?>
    </tr>
	
	
    <tr class="tabletext">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$label_year?></td>
	<?
	$AcademicYearID = ($yearID!='') ? $yearID : Get_Current_Academic_Year_ID();
	$yearStartDate = getStartDateOfAcademicYear($AcademicYearID);
	$yearEndDate = getEndDateOfAcademicYear($AcademicYearID);
	$yearData = $ldiscipline->retrieveStudentConvertedMeritTypeCountByDate($targetID, $yearStartDate,$yearEndDate);
	
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td class=\"tablebluerow2\">".($yearData['0']+0)."</td>";
    }
    ?>
    </tr>
<?php if(($selectSemester!="")&&($selectSemester!=0)){?>
    <tr class="tabletext tablebluerow1">
      <td class="tabletext formfieldtitle" width="30%" valign="top"><?=$label_semester?></td>
    <?
	$semStart = getStartDateOfAcademicYear($yearID, $selectSemester);
	$semEnd = getEndDateOfAcademicYear($yearID, $selectSemester);
	$semesterCount = $ldiscipline->retrieveStudentConvertedMeritTypeCountByDate($targetID, $semStart, $semEnd);
	
    if (!$lsp->is_ult_merit_disabled)
    {
         echo "<td>".($semesterCount['5']+0)."</td>";
    }
    if (!$lsp->is_sup_merit_disabled)
    {
         echo "<td>".($semesterCount['4']+0)."</td>";
    }
    if (!$lsp->is_maj_merit_disabled)
    {
         echo "<td>".($semesterCount['3']+0)."</td>";
    }
    if (!$lsp->is_min_merit_disabled)
    {
         echo "<td>".($semesterCount['2']+0)."</td>";
    }
    if (!$lsp->is_merit_disabled)
    {
         echo "<td>".($semesterCount['1']+0)."</td>";
    }
    if (!$lsp->is_ult_demer_disabled)
    {
         echo "<td>".($semesterCount['-5']+0)."</td>";
    }
    if (!$lsp->is_sup_demer_disabled)
    {
         echo "<td>".($semesterCount['-4']+0)."</td>";
    }
    if (!$lsp->is_maj_demer_disabled)
    {
         echo "<td>".($semesterCount['-3']+0)."</td>";
    }
    if (!$lsp->is_min_demer_disabled)
    {
         echo "<td>".($semesterCount['-2']+0)."</td>";
    }
    if (!$lsp->is_black_disabled)
    {
         echo "<td>".($semesterCount['-1']+0)."</td>";
    }
    if (!$lsp->is_warning_disabled)
    {
         echo "<td>".($semesterCount['0']+0)."</td>";
    }
    ?>
    </tr>
<?php } ?>
  </table>
</td></tr>
</table>
<? }
### end of Converted Data (customization) ###
*/ 
?>

<? if($sys_custom['eDiscipline']['yy3']) 
{
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
	$ldiscipline_cust = new libdisciplinev12_cust();
	$current_conduct = $ldiscipline->retrieveConductScoreBalance($targetID);
	$conduct_summary = $ldiscipline_cust->retrieveStudentConductMarkSummary($targetID, $startdate, $enddate);
?>
	<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr><td class="tabletext"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/icon_section.gif" width="20" height="20" align="absmiddle">
		<span class="sectiontitle"><?=$Lang['eDiscipline']['ConductMarkSummary']?></span>
	</td></tr>
	</table>
	
	<table border="0" cellpadding="2" cellspacing="0" width="96%">
	<tr><td>
	  <table width="96%" border="0" cellpadding="2" cellspacing="0">
	    <tr class="tablebluetop tabletopnolink">
	      <td width="16.6%" align="center"><?=$Lang['eDiscipline']['Academic_AccumulativeConduct']?></td>
		  <td width="16.6%" align="center"><?=$Lang['eDiscipline']['Behavioural_AccumulativeConduct']?></td>
		  <td width="16.6%" align="center"><?=$Lang['eDiscipline']['ECA_AccumulativeConduct']?></td>
		  <td width="16.6%" align="center"><?=$Lang['eDiscipline']['TotalConductMarkAddition']?></td>
		  <td width="16.6%" align="center"><?=$Lang['eDiscipline']['TotalConductMarkDeduction']?></td>
		  <td width="16.6%" align="center"><?=$Lang['eDiscipline']['CurrentConductMark']?></td>
	    </tr>
	    <tr class="tabletext tablebluerow1">
	      <td align="center"><?=$conduct_summary['Tag'][1] ? $conduct_summary['Tag'][1] : 0?></td>
		  <td align="center"><?=$conduct_summary['Tag'][2] ? $conduct_summary['Tag'][2] : 0?></td>
		  <td align="center"><?=$conduct_summary['Tag'][3] ? $conduct_summary['Tag'][3] : 0?></td>
		  <td align="center"><?=$conduct_summary['A']?></td>
		  <td align="center"><?=$conduct_summary['S']?></td>
		  <td align="center"><?=$current_conduct?></td>
	    </tr>
	  </table>
	</td></tr>
	</table>
<? } ?>

<br>
<?
if (!isset($detailType))
{
     $detailType = 4;
}
if(!($detailType == 6 || $detailType == 7))
{
	if($show_waive_status==1){
		$show_waive_status_link = "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" name=\"statusp\"  border=\"0\" align=\"absmiddle\" id=\"statusp\"><a href='javascript:showWaiveStatus(0)'  class='tablebottomlink'>$i_Discipline_System_No_WaiveStatus</a>";
	}
	else{
		$show_waive_status_link = "<img src=\"$image_path/$LAYOUT_SKIN/icon_view.gif\" name=\"statusp\"  border=\"0\" align=\"absmiddle\" id=\"statusp\"><a href='javascript:showWaiveStatus(1)'  class='tablebottomlink'>$i_Discipline_System_Show_WaiveStatus</a>";

	}
}
$detail_table = "";

$edit_image= "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" alt=\"$button_edit\" border=\"0\">";
$delete_image="<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" alt=\"$button_remove\" border=\"0\">";

# Check display Reamrk field or not (customization for fyk 20080424)
$show_remark_in_personal_report = $sys_custom['Discipline_ShowRemarkInPersonalReport'];
if($sys_custom['eDiscipline']['yy3']) {
	$conductMarkExceedLimitRemark = $ldiscipline->ConductMarkExceedLimitRemark;
}

$CategorySelection = "";
switch ($detailType)
{
        case 1:
             $string_type["0"] = $i_Merit_Warning;
             $string_type["-1"] = $i_Merit_BlackMark;
             $string_type["-2"] = $i_Merit_MinorDemerit;
             $string_type["-3"] = $i_Merit_MajorDemerit;
             $string_type["-4"] = $i_Merit_SuperDemerit;
             $string_type["-5"] = $i_Merit_UltraDemerit;

             # get list of merit record id that is upgraded from ACCU punishment
             $sql="SELECT DISTINCT UpgradedRecordID FROM DISCIPLINE_ACCU_RECORD WHERE UpgradedRecordID>0";
             $upgraded_list = $ldiscipline->returnVector($sql);

             # Demerit
             if($selectSemester == 0){
             	$selectSemester = "";
             }
             
			 # [2016-0526-1018-39096] Item Category
			 $APCategoryArr = $ldiscipline->getAPCategoryByType($mtype=-1);
			 $CategorySelection = "&nbsp;".$linterface->GET_SELECTION_BOX($APCategoryArr, " id='PunishmentCatID' name='PunishmentCatID' onchange=\"this.form.action='personal_check.php'; this.form.submit()\"", "- $i_Discipline_System_Reports_All_Punishment -", $PunishmentCatID);
			 
			 $cond = !empty($PunishmentCatID) && $PunishmentCatID > 0? " AND b.CatID = '$PunishmentCatID'" : "";
           	 $result = $ldiscipline->retrieveStudentDemeritRecord($targetID, $startdate, $enddate, $yearID, $selectSemester, $show_waive_status, $cond);
			 
             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             // [2015-0330-1119-38207] replace $i_Discipline_System_Add_Demerit by $Lang['eDiscipline']['AddedDemeritRecord']
             $detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
                                 	<td class=\"tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['EventDate']."</td>
                                 	<td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>";
                                 	
//           // [2016-0519-1111-59096]
//           if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
//             	$detail_table .= "	<td class=\"tabletopnolink\" width=\"60\">".$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['AccumulateCount']."</td>";
             
             $detail_table .= "		<td class=\"tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['AddedDemeritRecord']."</td>";
             
			 if(!$ldiscipline->Hidden_ConductMark){
				$detail_table .= "	<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_Conduct_ScoreDifference</td>";
				$detail_table .= "	<td class=\"tabletopnolink\" width=\"60\">".$Lang['eDiscipline']['AccumulatedConductScore']."</td>";
			 }
			 
			 $detail_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tabletopnolink\">$i_Discipline_System_Subscore1_ScoreDifference</td>" : "";
			 $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tabletopnolink\" width=\"10%\">$i_Subject_name</td>" : "";
             $detail_table .= "<td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>";
             $detail_table .= $show_waive_status? "<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_WaiveStatus</td>":"";
             
             # Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report ? "<td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>":"";

             $detail_table .="</tr>";

			 $accumulatedScore = 0;
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, 
                        $r_picid, $r_pic_name,$record_status,$remark, $fromConductRecord, $overflowConductMark, $overflowMeritItemScore,$overflowTagScore) = $result[$i];
                  
                  $css = ($i%2?"1":"2");
                  
                  if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
				  {
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
				  }
                  
//                  // [2016-0519-1111-59096]
//                  $floatCount = "--";
//                  if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'] && $fromConductRecord)
//                  {
//                  		$floatCount = $ldiscipline->returnAPRecordFloatUpgradeCount($r_id);
//						$floatCount = $floatCount && $floatCount>0? $floatCount : "--"; 
//                  }

				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
          
				  $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     	<td>$r_date</td>
                                     	<td>". intranet_htmlspecialchars($r_item) ."</td>";
                                     
//	              // [2016-0519-1111-59096]
//	              if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
//	             		$detail_table .= "<td>".$floatCount."</td>";
	             		
                  $detail_table .= "	<td>$record_data</td>";
                  
				  if(!$ldiscipline->Hidden_ConductMark){
						if($sys_custom['eDiscipline']['yy3'] && ($overflowConductMark==1 || $overflowMeritItemScore==1 || $overflowTagScore==1) && $r_conduct==0) {
							$detail_table .= "<td>$conductMarkExceedLimitRemark</td>";
						} else {
							$detail_table .= "<td>$r_conduct</td>";
				  			$accumulatedScore += $r_conduct;
						}
						$detail_table .= "<td>$accumulatedScore</td>";
				  }
				  
                  $detail_table .= ($ldiscipline->UseSubScore) ? "<td>$t_subscore1</td>"  : "";
                  $detail_table .= ($ldiscipline->use_subject) ? "<td>$t_subject</td>" : "";
                  $detail_table .= "<td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
                  if($show_waive_status==1)
              			$detail_table .="<Td>$str_waive_status</td>";

          		  if($show_remark_in_personal_report) {
          				$detail_table .="<Td>". nl2br($remark);
					
					  	if($fromConductRecord) {
							//$detail_table .= $ldiscipline->displayGMDetailByAPRecordID($r_id);
							$detail_table .= "<a href=\"javascript:show_remark($r_id, 'show_history$r_id')\" class='tablelink'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' align='absmiddle'></a><div id=\"show_history$r_id\">&nbsp;</div>";
					  	}
						
					  	$detail_table .= "</td>";
				  }

                  $detail_table.="</tr>";

             }
             if (sizeof($result) == 0)
             	  $detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";

             break;
        case 2:
             $string_type["1"] = $i_Merit_Merit;
             $string_type["2"] = $i_Merit_MinorCredit;
             $string_type["3"] = $i_Merit_MajorCredit;
             $string_type["4"] = $i_Merit_SuperCredit;
             $string_type["5"] = $i_Merit_UltraCredit;

             # Merit
             if($selectSemester == 0){
             	$selectSemester = "";
             }
             
			 # [2016-0526-1018-39096] Item Category
			 $APCategoryArr = $ldiscipline->getAPCategoryByType($mtype=1);
			 $CategorySelection = "&nbsp;".$linterface->GET_SELECTION_BOX($APCategoryArr, " id='AwardCatID' name='AwardCatID' onchange=\"this.form.action='personal_check.php'; this.form.submit()\"", "- $i_Discipline_System_Reports_All_Awards -", $AwardCatID);
			 
			 $cond = !empty($AwardCatID) && $AwardCatID > 0? " AND b.CatID = '$AwardCatID'" : "";
             $result = $ldiscipline->retrieveStudentMeritRecord($targetID, $startdate, $enddate, $yearID, $selectSemester, $show_waive_status, $cond);
			 
             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             // [2015-0330-1119-38207] replace $i_Discipline_System_Add_Merit by $Lang['eDiscipline']['AddedMeritRecord']
             $detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
                                 	<td class=\"tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['EventDate']."</td>
                                 	<td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>";
                                 
//             // [2016-0519-1111-59096]
//             if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
//             	$detail_table .= "	<td class=\"tabletopnolink\" width=\"60\">".$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['AccumulateCount']."</td>";
             	
             $detail_table .= "		<td class=\"tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['AddedMeritRecord']."</td>";
             
			 if(!$ldiscipline->Hidden_ConductMark){
				$detail_table .= "<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_Conduct_ScoreDifference</td>";
				$detail_table .= "<td class=\"tabletopnolink\" width=\"60\">".$Lang['eDiscipline']['AccumulatedConductScore']."</td>";
			 }
			 
			 $detail_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tabletopnolink\">$i_Discipline_System_Subscore1_ScoreDifference</td>" : "";
             $detail_table .= "<td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>";
             $detail_table .= $show_waive_status? "<td class=\"tabletopnolink\" width=\"60\">$i_Discipline_System_WaiveStatus</td>":"";
            
            # Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>":"";
            
             $detail_table .= "</tr>";
              
			 $accumulatedScore = 0;
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $r_picid, 
                        $r_pic_name, $remark, $record_status, $fromConductRecord, $overflowConductMark, $overflowMeritItemScore, $overflowTagScore) = $result[$i];
                        
                  $css = ($i%2?"2":"");
                  
                  if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
				  {
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
				  }
                  
//                  // [2016-0519-1111-59096]
//                  $floatCount = "--";
//                  if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'] && $fromConductRecord)
//                  {
//                  		$floatCount = $ldiscipline->returnAPRecordFloatUpgradeCount($r_id);
//						$floatCount = $floatCount && $floatCount>0? $floatCount : "--";
//                  }
					
                  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                  
                  $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
						
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     	<td>$r_date</td>
                                     	<td>". intranet_htmlspecialchars($r_item) ."</td>";
                                     
//	              // [2016-0519-1111-59096]
//	              if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
//	             		$detail_table .= "<td>".$floatCount."</td>";
	             		
                  $detail_table .= "   <td>$record_data</td>";
                  
				  if(!$ldiscipline->Hidden_ConductMark) {
				 		if($sys_custom['eDiscipline']['yy3'] && ($overflowConductMark==1 || $overflowMeritItemScore==1 || $overflowTagScore==1) && $r_conduct==0) {
							$detail_table .= "<td>$conductMarkExceedLimitRemark</td>";
						}else{
							$detail_table .= "<td>$r_conduct</td>";
				  			$accumulatedScore += $r_conduct;
						}
						$detail_table .= "<td>$accumulatedScore</td>";
				  }
				  
                  $detail_table .= ($ldiscipline->UseSubScore) ? "<td>$t_subscore1</td>" : "";
                  $detail_table .= "<td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
                  if($show_waive_status==1)		
                  		$detail_table .="<Td>$str_waive_status</td>";
                  if($show_remark_in_personal_report) {
                  		$detail_table .="<Td>". nl2br($remark) ;
						if($fromConductRecord) {
							//$detail_table .= $ldiscipline->displayGMDetailByAPRecordID($r_id);
							$detail_table .= "<a href=\"javascript:show_remark($r_id, 'show_history$r_id')\" class='tablelink'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' align='absmiddle'></a><div id=\"show_history$r_id\">&nbsp;</div>";
						}
						$detail_table .="</td>";
					}
                  		
                  $detail_table .= "</tr>";
             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";
             
             break;
         case 4:
             $string_type["0"] = $i_Merit_Warning;
             $string_type["-1"] = $i_Merit_BlackMark;
             $string_type["-2"] = $i_Merit_MinorDemerit;
             $string_type["-3"] = $i_Merit_MajorDemerit;
             $string_type["-4"] = $i_Merit_SuperDemerit;
             $string_type["-5"] = $i_Merit_UltraDemerit;
             $string_type["1"] = $i_Merit_Merit;
             $string_type["2"] = $i_Merit_MinorCredit;
             $string_type["3"] = $i_Merit_MajorCredit;
             $string_type["4"] = $i_Merit_SuperCredit;
             $string_type["5"] = $i_Merit_UltraCredit;

             # get list of merit record id that is upgraded from ACCU punishment
             $sql="SELECT DISTINCT UpgradedRecordID FROM DISCIPLINE_ACCU_RECORD WHERE UpgradedRecordID>0";
             $upgraded_list = $ldiscipline->returnVector($sql);

             # Merit / Demerit
             if($selectSemester == 0){
             	$selectSemester = "";
             }
             
			 # [2016-0526-1018-39096] Item Category
			 $APCategory1Arr = $ldiscipline->getAPCategoryByType($mtype=1);
			 $APCategory2Arr = $ldiscipline->getAPCategoryByType($mtype=-1);
			 $APCategoryArr = array_merge($APCategory1Arr, $APCategory2Arr);
			 $CategorySelection = "&nbsp;".$linterface->GET_SELECTION_BOX($APCategoryArr, " id='APCatID' name='APCatID' onchange=\"this.form.action='personal_check.php'; this.form.submit()\"", "- ".$Lang['eDiscipline']['AllAwardPunishment']." -", $APCatID);
			 
			 $cond = !empty($APCatID) && $APCatID > 0? " AND b.CatID = '$APCatID'" : "";
             $result = $ldiscipline->retrieveStudentProfileRecord($targetID, $startdate, $enddate, $yearID, $selectSemester, $show_waive_status, $cond);

             $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             $detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\">#</td>
                                 	<td class=\"tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>";
			 if($sys_custom['Discipline_ShowCategoryInPersonalReport'])                                 
				$detail_table .= "	<td class=\"tabletopnolink\">$i_Discipline_System_CategoryName</td>";
			 $detail_table .= "		<td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>";
			 
//			 // [2016-0519-1111-59096]
//			 if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
//			   	$detail_table .= "	<td class=\"tabletopnolink\">".$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['AccumulateCount']."</td>";
			   	
             $detail_table .= "	 	<td class=\"tabletopnolink\">$i_Discipline_System_Add_Merit_Demerit</td>";
			 
			 if(!$ldiscipline->Hidden_ConductMark){
            	$detail_table .= "	<td class=\"tabletopnolink\">$i_Discipline_System_Conduct_ScoreDifference</td>";
				$detail_table .= "	<td class=\"tabletopnolink\">".$Lang['eDiscipline']['AccumulatedConductScore']."</td>";
			 }
			 
             $detail_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tabletopnolink\">$i_Discipline_System_Subscore1_ScoreDifference</td>" : "";
             $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tabletopnolink\">$i_Subject_name</td>" : "";
             $detail_table .= "<td class=\"tabletopnolink\">$i_Profile_PersonInCharge</td>";
             $detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
            
             # Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\">$i_Discipline_System_general_remark</td>":"";
            
             $detail_table.="</tr>";
             
			 $accumulatedScore = 0;
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_id, $r_date, $r_item, $r_profile_type, $r_profile_count,
                        $r_conduct, $r_conduct_after, $t_subscore1, $t_subscore1_after, $t_subject, 
                        $r_picid, $r_pic_name,$record_status, $MeritType, $remark, $cat_name, $fromConductRecord, $overflowConductMark, $overflowMeritItemScore, $overflowTagScore) = $result[$i];
				  $cat_name = intranet_htmlspecialchars($cat_name);
				  $r_item = intranet_htmlspecialchars($r_item);
				  $remark = intranet_htmlspecialchars($remark);
					
                  $css = ($i%2?"2":"1");

				  if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
				  {
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
				  }
				  
//				  // [2016-0519-1111-59096]
//                  $floatCount = "--";
//                  if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'] && $fromConductRecord)
//                  {
//                  		$floatCount = $ldiscipline->returnAPRecordFloatUpgradeCount($r_id);
//						$floatCount = $floatCount && $floatCount>0? $floatCount : "--";
//                  }
					
                  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
                  /*
                  switch($waive_status){
   	                  case "--": $str_waive_status = $waive_status; break;
	                  case 0 : $str_waive_status = $i_Discipline_System_WaiveStatus_NoWaived;break;
	                  case 1 : $str_waive_status = $i_Discipline_System_WaiveStatus_InProgress;break;
	                  case 2 : $str_waive_status = $i_Discipline_System_WaiveStatus_Success; break;
	                  case 3 : $str_waive_status = $i_Discipline_System_WaiveStatus_Fail; break;
	                  default : $str_waive_status = $i_Discipline_System_WaiveStatus_NoWaived;
	              }
	              */

                  $css = $record_status==DISCIPLINE_STATUS_WAIVED ? 3 : $css;
				  $record_data = $ldiscipline->returnDeMeritStringWithNumber($r_profile_count, $string_type[$r_profile_type]);
                  $detail_table .= "<tr><td class=\"tablegreenrow$css tabletext\">". ($i+1)."</td>
                                     	<td class=\"tablegreenrow$css tabletext\">$r_date</td>";
                  if($sys_custom['Discipline_ShowCategoryInPersonalReport'])
						$detail_table .= "<td class=\"tablegreenrow$css tabletext\">$cat_name</td>";
						
                  $detail_table .= "	<td class=\"tablegreenrow$css tabletext\">$r_item</td>";
                  
//	              // [2016-0519-1111-59096]
//	              if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
//	             		$detail_table .= "<td class=\"tablegreenrow$css tabletext\">".$floatCount."</td>";
	             		
                  $detail_table .= "   	<td class=\"tablegreenrow$css tabletext\">$record_data</td>";
				  
				  if(!$ldiscipline->Hidden_ConductMark) {
						if($sys_custom['eDiscipline']['yy3'] && ($overflowConductMark==1 || $overflowMeritItemScore==1 || $overflowTagScore==1) && $r_conduct==0) {
							$detail_table .= "<td>$conductMarkExceedLimitRemark</td>";
						}else{
							$detail_table .= "<td class=\"tablegreenrow$css tabletext\">$r_conduct</td>";
				  			$accumulatedScore += $r_conduct;
						}
						$detail_table .= "<td class=\"tablegreenrow$css tabletext\">$accumulatedScore</td>";
				  }
				  
                  $detail_table .= ($ldiscipline->UseSubScore) ? "<td class=\"tablegreenrow$css tabletext\">$t_subscore1</td>" : "";
                  $detail_table .= ($ldiscipline->use_subject) ? "<td class=\"tablegreenrow$css tabletext\">$t_subject&nbsp;</td>" : "";
                  $detail_table .= "<td class=\"tablegreenrow$css tabletext\">".($r_pic_name==""?"--":$r_pic_name)."</td>";
                  if($show_waive_status==1)
                		$detail_table .="<Td class=\"tablegreenrow$css tabletext\">$str_waive_status</td>";
                  if($show_remark_in_personal_report) {
                  		$detail_table .="<td class=\"tablegreenrow$css tabletext\">". nl2br($remark);
						if($fromConductRecord) {
							//$detail_table .= $ldiscipline->displayGMDetailByAPRecordID($r_id);
							$detail_table .= "<a href=\"javascript:show_remark($r_id, 'show_history$r_id')\" class='tablelink'><img src='$image_path/$LAYOUT_SKIN/icon_remark.gif' border='0' align='absmiddle'></a><div id=\"show_history$r_id\">&nbsp;</div>";
						}
						$detail_table .="</td>";
                  }
                  $detail_table.= "</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";
			
             break;
       case 6:
					if($selectSemester == 0){
						$selectSemester = "";
					}
             		$result = $ldiscipline->retrieveConductScoreChangeLogRecord($targetID,$startdate,$enddate,$yearID,$selectSemester);
  		            $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             		$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\">#</td>
                                 <td class=\"tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>
                                 <td class=\"tabletopnolink\">$i_Merit_Type</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_FromScore</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_ToScore</td>
            					 <td class=\"tabletopnolink\">$i_Profile_PersonInCharge</td>";
            		# Yat Woon, customization for fyk.edu.hk 20080424
             $detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\">$i_Discipline_System_general_remark</td>":"";
             			 
                   $detail_table .= "</tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
                  $css = ($i%2?"2":"1");

                  switch($r_action_type){
	                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
	                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
	                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
	                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
	                  case 5 : $action = $i_Discipline_System_System_Revised; break;
	              }
	              $r_item = intranet_htmlspecialchars($r_item);
	              if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
	              if($r_item =="--" && $r_action_type!=5){
		              $r_item = "<i>$i_Discipline_System_Record_Already_Removed</i>";
		          }
				  
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>$action</td>
                                     <td>$r_item</td>
                                     <td>$r_fromscore</td>
                                     <td>$r_toscore</td>
                  					 <td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
				if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";
                  $detail_table .="</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";

             		break;
      case 7:
					if($selectSemester == 0){
						$selectSemester = "";
					}
                    $result = $ldiscipline->retrieveSubscore1ChangeLogRecord($targetID,$startdate,$enddate,$yearID,$selectSemester);
  		            $detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
             		$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\">#</td>
                                 <td class=\"tabletopnolink\">".$Lang['eDiscipline']['EventDate']."</td>
                                 <td class=\"tabletopnolink\">$i_Merit_Type</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_general_record</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_FromScore</td>
                                 <td class=\"tabletopnolink\">$i_Discipline_System_ToScore</td>
            					 <td class=\"tabletopnolink\">$i_Profile_PersonInCharge</td>";
            		# Yat Woon, customization for fyk.edu.hk 20080424
             		$detail_table .= $show_remark_in_personal_report? "<td class=\"tabletopnolink\">$i_Discipline_System_general_remark</td>":"";
                    $detail_table .= "</tr>";
             for ($i=0; $i<sizeof($result); $i++)
             {
                  list ($r_date, $r_action_type, $r_item, $r_fromscore, $r_toscore,$r_pic_name, $remark) = $result[$i];
                  $css = ($i%2?"2":"1");

                  switch($r_action_type){
	                  case 1 : $action = $i_Discipline_System_New_MeritDemerit; break;
	                  case 2 : $action = $i_Discipline_System_Edit_MeritDemerit; break;
	                  case 3 : $action = $i_Discipline_System_Remove_MeritDemerit; break;
	                  case 4 : $action = $i_Discipline_System_Manual_Adjustment; break;
	              }
	              $r_item = intranet_htmlspecialchars($r_item);
	              if($sys_custom['sdbnsm_disciplinev12_personal_report_cust'])
					{
						if($r_item=="其他記過事項" || $r_item=="其他記功事項")
							$r_item = $remark;
					}
					
	              if($r_item =="--"){
		              $r_item = "<i>$i_Discipline_System_Record_Already_Removed</i>";
		          }
                  $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
                                     <td>$r_date</td>
                                     <td>$action</td>
                                     <td>$r_item</td>
                                     <td>$r_fromscore</td>
                                     <td>$r_toscore</td>
                  					 <td>".($r_pic_name==""?"--":$r_pic_name)."</td>";
				 if($show_remark_in_personal_report)
                  		$detail_table .="<Td>". nl2br($remark) ."</td>";
                $detail_table .= "</tr>";

             }
             if (sizeof($result) == 0)
             	$detail_table .= "<tr><td class='tabletext' colspan='7' align='center'>{$i_no_record_exists_msg}</td></tr>";
             $detail_table .= "</table>\n";


             	break;

        case 8:
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
				                     <td class=\"tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['EventDate']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>";
			 
			 	// [2016-0519-1111-59096]
			 	if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
			   		$detail_table .= "<td class=\"tabletopnolink\">".$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['AccumulateCount']."</td>";
			   	
             	$detail_table .= "	 <td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
				$detail_table .="</tr>";

				if($selectSemester == 0){
					$selectSemester = "";
				}
             
				 # [2016-0526-1018-39096] Item Category
				 $GMCategoryArr = $ldiscipline->getAllGmCategory(" AND MeritType = -1");
				 $CategorySelection = "&nbsp;".$linterface->GET_SELECTION_BOX($GMCategoryArr, " id='MisconductCatID' name='MisconductCatID' onchange=\"this.form.action='personal_check.php'; this.form.submit()\"", "- $i_Discipline_System_Reports_All_Misconduct -", $MisconductCatID);
				 
				 $cond = !empty($MisconductCatID) && $MisconductCatID > 0? " AND a.CategoryID = '$MisconductCatID'" : "";				
				 $result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester,"-1",$show_waive_status,$cond);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  
				  // [2016-0519-1111-59096]
                  $floatCount = "--";
                  if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
                  {
                  		$floatCount = $ldiscipline->returnAPRecordFloatUpgradeCount("", $record_id);
						$floatCount = $floatCount && $floatCount>0? $floatCount : "--";
                  }
                  
				  $css = ($i%2?"2":"1");
				  
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				 				  
			      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
			                         <td>$record_date</td>
			                         <td>$cat_name</td>
			                         <Td>$item_name</td>";
                  
	              // [2016-0519-1111-59096]
	              if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
	             		$detail_table .= "<td>".$floatCount."</td>";
	             		
                  $detail_table .= " <td>".($r_pic_name==""?"--":$r_pic_name)."</td>
			                         <Td>". intranet_htmlspecialchars($remark) ."</td>
			                        ";
			      if($show_waive_status==1)
			      	$detail_table .="<Td>$str_waive_status</td>";

			      $detail_table.="</tr>";

				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";

             break;
		case 9:
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
				                     <td class=\"tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['EventDate']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>";
			 
			 	// [2016-0519-1111-59096]
			 	if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
			   		$detail_table .= "<td class=\"tabletopnolink\">".$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['AccumulateCount']."</td>";
			   	
             	$detail_table .= "	 <td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
				 $detail_table .="</tr>";
				
				if($selectSemester == 0){
					$selectSemester = "";
				}
             
				 # [2016-0526-1018-39096] Item Category
				 $GMCategoryArr = $ldiscipline->getAllGmCategory(" AND MeritType = 1");
				 $CategorySelection = "&nbsp;".$linterface->GET_SELECTION_BOX($GMCategoryArr, " id='GoodConductCatID' name='GoodConductCatID' onchange=\"this.form.action='personal_check.php'; this.form.submit()\"", "- $i_Discipline_System_Reports_All_Good_Conducts -", $GoodConductCatID);
				 
				 $cond = !empty($GoodConductCatID) && $GoodConductCatID > 0? " AND a.CategoryID = '$GoodConductCatID'" : "";
				 $result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester,"1",$show_waive_status,$cond);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  
				  // [2016-0519-1111-59096]
                  $floatCount = "--";
                  if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
                  {
                  		$floatCount = $ldiscipline->returnAPRecordFloatUpgradeCount("", $record_id);
						$floatCount = $floatCount && $floatCount>0? $floatCount : "--";
                  }
                  
				  $css = ($i%2?"2":"1");

				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				  
			      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
			                         <td>$record_date</td>
			                         <td>$cat_name</td>
			                         <Td>$item_name</td>";
                  
	              // [2016-0519-1111-59096]
	              if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
	             		$detail_table .= "<td>".$floatCount."</td>";
	             		
                  $detail_table .= " <Td>$r_pic_name</td>
			                         <Td>". intranet_htmlspecialchars($remark) ."</td>
			                        ";
			      if($show_waive_status==1)
			      	$detail_table .="<Td>$str_waive_status</td>";

			      $detail_table.="</tr>";

				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";
				
             break;
		case 10:
				$detail_table="";
				$detail_table = "<table width=\"100%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\">\n";
				$detail_table .= "<tr class=\"tablegreentop\"><td class=\"tabletopnolink\" width=\"5\">#</td>
				                     <td class=\"tabletopnolink\" width=\"80\">".$Lang['eDiscipline']['EventDate']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Name']."</td>
				                     <td class=\"tabletopnolink\" width=\"30%\">".$iDiscipline['Accumulative_Category_Item_Name']."</td>";
			 
			 	// [2016-0519-1111-59096]
			 	if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
			   		$detail_table .= "<td class=\"tabletopnolink\">".$Lang['eDiscipline']['MoPuiChingCustomizePersonalReport']['AccumulateCount']."</td>";
			   	
             	$detail_table .= "	 <td class=\"tabletopnolink\" width=\"10%\">$i_Profile_PersonInCharge</td>
				                     <td class=\"tabletopnolink\" width=\"150\">$i_Discipline_System_general_remark</td>";
				$detail_table .=$show_waive_status? "<td class=\"tabletopnolink\">$i_Discipline_System_WaiveStatus</td>":"";
				 $detail_table .="</tr>";

				if($selectSemester == 0){
					$selectSemester = "";
				}
				
				 # [2016-0526-1018-39096] Item Category
				 $GMCategoryArr = $ldiscipline->getAllGmCategory("");
				 $CategorySelection = "&nbsp;".$linterface->GET_SELECTION_BOX($GMCategoryArr, " id='GMCatID' name='GMCatID' onchange=\"this.form.action='personal_check.php'; this.form.submit()\"", "- ".$Lang['eDiscipline']['AllGoodMisConduct']." -", $GMCatID);
				 
				 $cond = !empty($GMCatID) && $GMCatID > 0? " AND a.CategoryID = '$GMCatID'" : "";
				 $result = $ldiscipline->retrieveStudentConductRecord($targetID,$startdate,$enddate,$yearID,$selectSemester,"",$show_waive_status,$cond);
				
				for ($i=0; $i<sizeof($result); $i++)
				{
				  list ($record_id,$record_date,$cat_name,$item_name,$remark,$upgrade_record_id,$record_status,$r_pic_name)= $result[$i];
				  
				  // [2016-0519-1111-59096]
                  $floatCount = "--";
                  if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
                  {
                  		$floatCount = $ldiscipline->returnAPRecordFloatUpgradeCount("", $record_id);
						$floatCount = $floatCount && $floatCount>0? $floatCount : "--";
                  }
                  
				  $css = ($i%2?"2":"1");
				  
				  $str_waive_status = $record_status==DISCIPLINE_STATUS_WAIVED ? $i_general_yes : $i_general_no;
				 				  
			      $detail_table .= "<tr class=\"tablegreenrow$css tabletext\"><td>".($i+1)."</td>
			                         <td>$record_date</td>
			                         <td>$cat_name</td>
			                         <Td>$item_name</td>";
                  
	              // [2016-0519-1111-59096]
	              if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport'])
	             		$detail_table .= "<td>".$floatCount."</td>";
	             		
                  $detail_table .= " <td>".($r_pic_name==""?"--":$r_pic_name)."</td>
			                         <Td>". intranet_htmlspecialchars($remark) ."</td>
			                        ";
			      if($show_waive_status==1)
			      	$detail_table .="<Td>$str_waive_status</td>";

			      $detail_table.="</tr>";

				}
				$cols = $show_waive_status==1?8:7;
				if (sizeof($result) == 0)
					$detail_table .= "<tr><td class='tabletext' colspan='$cols' align='center'>{$i_no_record_exists_msg}</td></tr>";
				$detail_table .= "</table>\n";

             break;
}
?>

<SCRIPT language=Javascript>

<? if($sys_custom['eDiscipline']['MoPuiChingCustomizePersonalReport']){
	echo "SchoolTeamRange = {};\n";
	echo "SchoolTeamRange[0] = new Array('".(substr(getStartDateOfAcademicYear($yearID), 0, 10))."', '".(substr(getEndDateOfAcademicYear($yearID), 0, 10))."');\n";
	for($i=0; $i<sizeof($semResult); $i++) {
		list($id, $nameEN, $nameB5, $sem_start, $sem_end) = $semResult[$i];
		echo "SchoolTeamRange[".$id."] = new Array('".$sem_start."', '".$sem_end."');\n";
	}
?>

	function updateDateRange(semid)
	{
		semid = semid || 0;
		var semRange = SchoolTeamRange[semid]; 
		document.getElementById('startdate').value = semRange[0];
		document.getElementById('enddate').value = semRange[1];
		document.form1.submit();
	}
<? } ?>

function confirmRemoveRecord(detailType, id, targetID)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
        if (detailType==1 || detailType==2 || detailType == 4)
        {
        	location.href = 'personal_check_merit_remove.php?RecordID='+id+'&targetID='+targetID+'&detailType='+detailType+'&show_waive_status='+document.form1.show_waive_status.value;
        }
        else if (detailType==3)
        {
            location.href = 'personal_check_detention_remove.php?RecordID='+id+'&targetID='+targetID+'&detailType='+detailType;
		}
	}
}

function confirmRemoveAccumulativeRecord(detailType,id, targetID)
{
	if (confirm('<?=$i_Usage_RemoveConfirm?>'))
	{
     	location.href = 'accumulative/accu_personal_check_remove.php?RecordID='+id+'&targetID='+targetID+'&detailType='+detailType+'&show_waive_status='+document.form1.show_waive_status.value;
	}
}

function exportRecord()
{
	form = document.form1;
    form.action = "personal_check_export.php";
    form.submit();
}

function printRecord()
{
    document.form1.action = "personal_check_print.php";
    document.form1.target = "_blank";
    document.form1.submit();
    document.form1.target = "_self";
    document.form1.action = "";
}

function nextRecord(id)
{
    form = document.form1;
    form.targetID.value = id;
    form.action = '';
    form.submit();
}

function showWaiveStatus(v){
	obj = document.form1.show_waive_status;
	document.form1.action="personal_check.php";
	obj.value=v;
	document.form1.submit();
}

var xmlHttp3
function changeTerm(val) {
	if (val.length==0)
	{
		if(document.getElementById("spanSemester"))
		{
			document.getElementById("spanSemester").innerHTML = "";
			document.getElementById("spanSemester").style.border = "0px";
		}
		return
	}
	
	xmlHttp3 = GetXmlHttpObject()
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	
	var url = "";
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	url += "&action=OnChange";
	url += "&action_value=this.form.action='personal_check.php';this.form.submit()";
	
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{
		if(document.getElementById("spanSemester"))
		{
			document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
			document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
		}
	}
}

function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) {
	with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { 
		v=args[i+2];
		document.getElementById('ref_list').style.visibility='hidden';
		if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
		obj.visibility=v;		
	}
  }
}
</SCRIPT>

<script language="JavaScript">
var ClickID = '';
var callback_show_remark = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_remark(id, click)
{
	ClickID = click;
	document.getElementById('RecordID').value = id;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	
	var path = "ajax_gm.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_remark);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility='visible';
}
</script>

<table border="0" cellpadding="2" cellspacing="0" width="96%">
<tr><td>
<SELECT name="detailType" onChange="this.form.action='personal_check.php'; this.form.submit()">
<OPTION value="1" <?=($detailType==1?"SELECTED":"")?>><?=$eDiscipline["PunishmentRecord"]?></OPTION>
<OPTION value="2" <?=($detailType==2?"SELECTED":"")?>><?=$eDiscipline["AwardRecord"]?></OPTION>
<OPTION value="4" <?=($detailType==4?"SELECTED":"")?>><?=$eDiscipline["Personal_Report_AwardPunishmentRecord"]?></OPTION>
<? if(!$sys_custom['eDiscipline']['HideAllGMReport']) { ?>
	<OPTION value="8" <?=($detailType==8?"SELECTED":"")?>><?=$eDiscipline['Personal_Report_Misconduct_Record']?></OPTION>
	<OPTION value="9" <?=($detailType==9?"SELECTED":"")?>><?=$eDiscipline['Personal_Report_Goodconduct_Record']?></OPTION>
	<OPTION value="10" <?=($detailType==10?"SELECTED":"")?>><?=$eDiscipline['Personal_Report_Good_Misconduct_Record']?></OPTION>
<? } ?>
<?/* # hidden in IP25, since will mess with Conduct Adjustment
 if(!$ldiscipline->Hidden_ConductMark) {?>
<OPTION value="6" <?=($detailType==6?"SELECTED":"")?>><?=$i_Discipline_System_Conduct_Change_Log?></OPTION>
<? } */ ?>

<?php
if($ldiscipline->UseSubScore){?>
	<OPTION value="7" <?=($detailType==7?"SELECTED":"")?>><?=$i_Discipline_System_SubScore1_Change_Log?></OPTION>
<?php } ?>
</SELECT>

<?=$CategorySelection?>

&nbsp;
<?=$show_waive_status_link?>
</td>
<td></td>
</tr>
</table>

<table border="0" cellpadding="2" cellspacing="0" width="96%">
<tr><td>
<?=$detail_table?>
<div id="ref_list" style='position:absolute; height:200px; z-index:1; visibility: hidden;'></div>
</td></tr>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_export, "button", "javascript:exportRecord()") ?>&nbsp;
		<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:printRecord()") ?>
		</td>
	</tr>
</table>

<input type='hidden' name='show_waive_status' value='<?=$show_waive_status?>'>
<input type='hidden' name='ClickID' id='ClickID' value=''>
<input type='hidden' name='RecordID' id='RecordID' value=''>
</form>

<script language="javascript">
changeTerm(<?=$yearID?>);
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>