<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/cust/buddhist_fat_ho_edis.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

# user access right checking
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

# Export
if($format == 'csv'){
	$ldiscipline_ui->getIndividualDisciplineRecord($_REQUEST);
}
# Print
else
{
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	echo $ldiscipline_ui->getIndividualDisciplineRecord($_REQUEST);
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>

<? if($format == 'print'){ ?>
	
<style type="text/css">
@charset "utf-8";

/* CSS Document */
body { font-family: "Times New Roman, 新細明體"; padding:0; margin:0;}
html, body { margin: 0px; padding: 0px; } 

.main_container { display:block; width:680px;  height:880px; margin:auto; /*border:1px solid #DDD;*/ padding:30px 20px; position:relative; padding-bottom:90px; }
.second_page_header { min-height:40px; }
.page_break { page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0; }

.table_header { min-height:100px; clear:both; position:relative; vertical-align:center; }
.table_header h2 { text-align:center; min-height:30px; font-family: "Times New Roman, 新細明體"; font-size:21px; font-weight:bold; position:relative; padding-top: 5px; margin-bottom: 11px;}

.student_info { float:left; }
.student_info td { font-family: "Times New Roman, 新細明體"; font-size:16px; }
.record_table td{ font-family: "Times New Roman, 新細明體"; font-size:16px; }
.record_table .record_table_header td { font-size:18px; }

</style>

<? } ?>