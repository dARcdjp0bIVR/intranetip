<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['BFHMC_IndividualDisciplineRecord']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Setting of Current Page
$CurrentPage = "Reports_Individual_Discipline_Record";
$CurrentPageArr['eDisciplinev12'] = 1;
# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['IndividualDisciplineRecord']['Title'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['IndividualDisciplineRecord']['LangSettings'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# School year
$selectYear = ($targetYearID == '') ? Get_Current_Academic_Year_ID() : $targetYearID;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear'", "", $selectYear);

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="JavaScript">
function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//document.getElementById('selectAllBtn01').disabled = true;
		//document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		$('#selectAllBtn01').hide();
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		//document.getElementById('selectAllBtn01').disabled = false;
		//document.getElementById('selectAllBtn01').style.visibility = 'visible';
		$('#selectAllBtn01').show();
	}
}

function showResult(str, choice)
{
//	if(str != "student2ndLayer")
//		document.getElementById("studentFlag").value = 0;
//	else
//		document.getElementById("studentFlag").value = 1;
	
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
	
	var yearClassId = '';
	if(str == 'student2ndLayer'){
		var options = $('select#rankTargetDetail\\[\\] option:selected');
		var delim = '';
		for(var i=0;i<options.length;i++){
			yearClassId += delim + options.get(i).value;
			delim = ',';
		}
	}
	
	var url = "../ajaxGetLive.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	if(str == 'form'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	}else if(str == 'class'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	}else if(str == 'student'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&onchange="+encodeURIComponent("showRankTargetResult('student2ndLayer','');");
	}else if(str == 'student2ndLayer'){
		url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&studentFieldId=studentID[]&studentFieldName=studentID[]&YearClassID="+yearClassId;
	}
	url += "&academicYearId="+$('#selectYear').val();
	//url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	//url += "&student=1&value="+choice;
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			//if(document.getElementById("studentFlag").value == 0) {
			if(str == 'student2ndLayer'){
				showIn = "spanStudent";	
			}
			//document.getElementById(showIn).innerHTML = responseText;
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
			
			if(str == 'student'){
				showRankTargetResult('student2ndLayer','');
			}
		}
	);
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);
	if (val=='student2ndLayer') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function studentSelection(val) {
/*
	var rank = document.getElementById('rankTargetDetail[]');
	for(var i=0; i<rank.options.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
*/
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function submitForm()
{
	var valid = true;
	var form_obj = $('#form1');
	var rank_target = $('#rankTarget').val();
	
	if(rank_target == 'form' || rank_target == 'class'){
		if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}else if(rank_target == 'student'){
		if($('select#studentID\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}
	
	if(valid) {
		form_obj.submit();
	}
}

$(document).ready(function(){
	showRankTargetResult($("#rankTarget").val(), '');
});
</script>

<div id="PageDiv">
<div id="div_form">
	
	<p class="spacer"></p> 
	<span class="Form_Span">
	
		<form id="form1" name="form1" method="post" action="add_student_update.php" onSubmit="return false;">
		<table class="form_table_v30">
			<!-- Period -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['IndividualDisciplineRecord']['SchoolYear']?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td colspan="6" onClick="showRankTargetResult($('#rankTarget').val(),'');">
								<?=$selectSchoolYearHTML?>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['IndividualDisciplineRecord']['Target']?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td valign="top">
								<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
									<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
									<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
									<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
								</select>
							</td>
							<td valign="top" nowrap>
								<!-- Form / Class //-->
								<span id='rankTargetDetail'>
								<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
									
								<!-- Student //-->
								<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
									<select name="studentID[]" multiple size="5" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
								</span>
								
							</td>
						</tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['IndividualDisciplineRecord']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "submitForm();")?>
		<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="targetLang" id="targetLang" value="en">
		</form>
	</span>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
