<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$li = new libfilesystem();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html();

$jsRankTargetDetail = implode(',', $rankTargetDetail);

$studentIDAry = array();

if($rankTarget!="student")
	$studentIDAry = $ldiscipline->getStudentIDByTarget($rankTarget, $rankTargetDetail);
else 
	$studentIDAry = $studentID;
	
/*
$tableContent .= "<head>";
$tableContent .= "<link href='{$PATH_WRT_ROOT}/templates/2009a/css/print.css' rel='stylesheet' type='text/css'>";
$tableContent .= "<meta http-equiv='content-type' content='text/html; charset=utf-8'>";
$tableContent .= "<style type='text/css'>";
$tableContent .= "<!--";
$tableContent .= ".print_hide {display:none;}";
$tableContent .= "-->";
$tableContent .= "</style>";
$tableContent .= "</head>";
*/	
$print = 1;
if(sizeof($studentIDAry)>0)
	$tableContent .= $ldiscipline->getMonthlyAPReport($studentIDAry, $SchoolYear, $month, $print);
else 
	$tableContent .= "<table width='100%' cellpadding='2' cellspacing='0' border='0'><tr><td height='30'>$i_no_record_exists_msg</td></tr></table>";
?>

<p align='right' class='print_hide'><?=$linterface->GET_BTN($button_print, "button", "window.print()"); ?></p>

<?=$tableContent?>


<? intranet_closedb(); 
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>
