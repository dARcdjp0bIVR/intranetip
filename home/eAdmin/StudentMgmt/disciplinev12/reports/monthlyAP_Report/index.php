<?php
# using: yat


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
/*
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-StudentReport-View")) 
{
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

$linterface = new interface_html();
$limport = new libimporttext();
$lo = new libfilesystem();


$SchoolYear = ($SchoolYear == '') ? Get_Current_Academic_Year_ID() : $SchoolYear;
$allYears = $ldiscipline->returnAllYearsSelectionArray();


$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($allYears, "name='SchoolYear' id='SchoolYear' onChange='init()'","",$SchoolYear);

$selectMonth = "<select name='month'>";
$currentMonth = ($month=='') ? date(n) : $month+1;

for($i=0; $i<12; $i++) {
	$selected = ($i==($currentMonth-1)) ? " selected" : "";
	$selectMonth .= "<option value='$i' $selected>".$i_general_MonthShortForm[$i]."</option>";
}
$selectMonth .= "</select>";

if($submitBtn01) {
	if(sizeof($_POST)<=0)
		header("Location: index.php");
		
	$sql = "drop table temp_monthly_ap_import";
	$ldiscipline->db_db_query($sql);
	
	if($csvfile) {
		# import file checking
		$name = $_FILES['csvfile']['name'];
		$ext = strtoupper($lo->file_ext($name));
		
		if($ext != ".CSV")
		{
			header("location: index.php?xmsg=import_failed");
			exit();
		}
		$data = $limport->GET_IMPORT_TXT($csvfile);
		
		if(is_array($data))
		{
			$col_name = array_shift($data);
		}
		
		$file_format = array('Class Name','Class Number','Absent','Late','Early Leave','Comment');
		$format_wrong = false;
		for($i=0; $i<sizeof($file_format); $i++)
		{
			if ($col_name[$i]!=$file_format[$i])
			{
				$format_wrong = true;
				break;
			}
		}
		if($format_wrong)
		{
			header("location: index.php?xmsg=wrong_header");
			exit();
		}
		# create temp table for import data
		$sql = "create table temp_monthly_ap_import
			(
				ClassName varchar(10),
				ClassNumber int(3),
				Absent decimal(4,1),
				Late int(3),
				EarlyLeave int(3),
				Comment varchar(255)		 
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";
		$ldiscipline->db_db_query($sql);
		
		# insert into temp table	
		for($i=0;$i<sizeof($data);$i++) {
			list($clsName, $clsNo, $absent, $late, $earlyLeave, $comment) = $data[$i];
			$sql = "INSERT INTO temp_monthly_ap_import VALUES (\"$clsName\", $clsNo, $absent, $late, $earlyLeave, \"$comment\")";
			$ldiscipline->db_db_query($sql);
		}
	}
	
	$jsRankTargetDetail = implode(',', $rankTargetDetail);

	$studentIDAry = array();
	
	if($rankTarget!="student")
		$studentIDAry = $ldiscipline->getStudentIDByTarget($rankTarget, $rankTargetDetail, $SchoolYear);
	else 
		$studentIDAry = $studentID;
		

	$print = 0;
	
	if(sizeof($studentIDAry)>0)
		$tableContent = $ldiscipline->getMonthlyAPReport($studentIDAry, $SchoolYear, $month, $print);
	else 
		$tableContent = "<table width='100%' cellpadding='2' cellspacing='0' border='0'><tr><td height='40'>$i_no_record_exists_msg</td></tr></table>";
		
	$extraButton = $linterface->GET_ACTION_BTN($button_export, "button", "goExport()")."&nbsp;";
	$extraButton .= $linterface->GET_ACTION_BTN($Lang['Btn']['Export_without_ap'], "button", "goExport2()")."&nbsp;";
	$extraButton .= $linterface->GET_ACTION_BTN($button_print, "button", "goPrint()")."&nbsp;";
	$extraButton .= $linterface->GET_ACTION_BTN($button_cancel, "button", "goBack()");
}
	//$Lang['eDiscipline']['MonthlyAwardPunishmentReport'] = "Monthly Award & Punishment Report";

# tag information
$CurrentPage = "MonthlyAwardPunishmentReport";

$TAGS_OBJ[] = array($Lang['eDiscipline']['MonthlyAwardPunishmentReport']);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$csvFile = "discipline-monthly-ap-sample.csv";

$linterface->LAYOUT_START();


?>

<SCRIPT LANGUAGE=Javascript>

function checkForm()
{
	var choiceSelected;
	var choice = "";
	obj = document.form1;
	
	if(document.getElementById('SchoolYear').value=="#") {
		alert('<?=$i_alert_pleaseselect.$iDiscipline['Period']?>');	
		return false;
	}
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Form?>');
		return false;
	}
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) {
		alert('<?=$i_alert_pleaseselect.$i_Discipline_Class?>');
		return false;
	}
	if(document.getElementById('rankTarget').value == "student") {
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		if(choiceSelected==0) {
			alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
			return false;
		}
	}

	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	if(choiceSelected==0) {
		alert("<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>");	
		return false;
	}
	
	obj.action = 'index.php';	
	obj.method = 'POST';
	obj.target = "_self";
	obj.submit();

}

var xmlHttp

function showResult(str, choice)
{
	obj = document.form1;
	
	if(str != "student2ndLayer")
		obj.studentFlag.value = 0;
	
	if (str.length==0)
	{ 
		obj.rankTargetDetail.innerHTML = "";
		obj.rankTargetDetail.style.border = "0px";
		return
	}
	
	xmlHttp = GetXmlHttpObject()

	if (xmlHttp==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 
	
	url = "get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year="+obj.SchoolYear.value;
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
				
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	obj = document.form1;
	
	var showIn = "";
	if(obj.studentFlag.value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
	} 
}


function GetXmlHttpObject()
{
	var xmlHttp = null;
	try
	{
		// Firefox, Opera 8.0+, Safari
		xmlHttp = new XMLHttpRequest();
	}
	catch (e)
	{
		// Internet Explorer
		try
		{
			xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e)
		{
			xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
	}
	return xmlHttp;
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//form1.elements['rankTargetDetail[]'].multiple = false;
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		//SelectAll(document.getElementById('rankTargetDetail[]'));
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function goExport() {
	document.form1.action = 'export.php';	
	document.form1.method = 'POST';
	document.form1.target = "_self";
	document.form1.submit();
}

function goExport2() {
	document.form1.action = 'export2.php';	
	document.form1.method = 'POST';
	document.form1.target = "_self";
	document.form1.submit();
}

function goPrint() {
	document.form1.action = 'print.php';	
	document.form1.method = 'POST';
	document.form1.target = "_blank";
	document.form1.submit();
}

function goBack() {
	self.location.href = 'index.php';
}
</SCRIPT>


<br />
<form name="form1" method"=POST" onsubmit="return checkForm();" enctype="multipart/form-data">
<table border="0" cellpadding="5" cellspacing="0" width="90%">
  <tr>
  	<td align="right" colspan="2"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
  </tr>
  <tr>
    <td class="tabletext formfieldtitle" valign="top" width="20%"><?=$iDiscipline['Period']?> <span class="tabletextrequire">*</span></td>
    <td><?=$selectSchoolYearHTML?>&nbsp;<?=$selectMonth?></td>
  </tr>
  <tr>
    <td class="tabletext formfieldtitle" valign="top" width="20%"><?=$i_general_target?> <span class="tabletextrequire">*</span></td>
    <td>
		<table border="0">
			<tr>
				<td valign="top">
					<select name="rankTarget" id="rankTarget" onChange="showResult(this.value,'');if(this.value=='student') {showSpan('spanStudent');} else {hideSpan('spanStudent');}">
						<option value="#">-- <?=$i_general_please_select?> --</option>
						<option value="form" <? if($rankTarget=="form") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
						<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
						<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
					</select>
				</td>
				<td>
					<div id='rankTargetDetail' style='position:absolute; width:280px; height:0px; z-index:0;'></div>
					<select name="rankTargetDetail[]"  id="rankTargetDetail[]" size="5" id="rankTargetDetail[]"></select>
					<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
				</td>
				<td>
					<div id='spanStudent' style='position:relative; left:60px; width:280px; height:80px; z-index:1;<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
						<select name="studentID[]" id="studentID[]" multiple size="5" id="studentID[]"></select>
						<br><?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
					</div>
				</td>
			</tr>
		</table>
		
    </td>
  </tr>
  <tr>
    <td class="tabletext formfieldtitle" valign="top" width="20%"><?=$iDiscipline['Import_Source_File']?> <span class="tabletextrequire">*</span></td>
    <td>
    	<input class="file" type="file" name="csvfile" id="csvfile" value="<?=$name?>">
    	<br>
    	<?=$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1']."<strong>&lt;<a class=\"tablelink\" href=\"". GET_CSV($csvFile) ."\" target=\"_blank\">".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2']."</a>&gt;</strong> ".$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3']?>
    </td>
  </tr>
  
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center"><?= $linterface->GET_ACTION_BTN($button_view, "submit", "", "submitBtn01"); ?></td>
	</tr>
	<tr>
		<td align="center">&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><?=$tableContent?></td>
	</tr>
	<tr>
		<td align="center"><?=$extraButton?></td>
	</tr>
	
</table>

<input type="hidden" name="targetType" value="2" />
<input type="hidden" name="studentFlag" value="0">	
</form>
<script language="javascript">
<!--
function init()
{
	var obj = document.form1;
	
	obj.rankTarget.selectedIndex=1;		// Form
	hideSpan('spanStudent');
	showResult("form","");
}
<?
if($submitBtn01 == "")
	echo "init();\n";
?>

<?
if($submitBtn01 != "") {
	echo "showResult(\"$rankTarget\", \"$jsRankTargetDetail\");";
}
?>
<? if(sizeof($studentID)>0) { ?>
var xmlHttp2;

initialStudent();	
	
function initialStudent() {
	xmlHttp2 = GetXmlHttpObject()
	url = "get_live.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID);?>";
	xmlHttp2.onreadystatechange = stateChanged2
	xmlHttp2.open('GET',url,true);
	xmlHttp2.send(null);
	
	document.getElementById('selectAllBtn01').disabled = true;
	document.getElementById('selectAllBtn01').style.visibility = 'hidden';
}
<? } ?>

function stateChanged2() 
{ 
	if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
	{ 
		document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
		document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
		
		//SelectAll(form1.elements['studentID[]']);
	} 
}
//-->
</script>
<?
        $linterface->LAYOUT_STOP();
        intranet_closedb();

?>