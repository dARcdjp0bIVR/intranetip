<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");


intranet_auth();
intranet_opendb();

$lclass = new libclass();
$linterface = new interface_html("popup.html");
$ldiscipline = new libdisciplinev12();

$result = $ldiscipline->getClassConductGradeReport($year, $semester, $class, $print);


$name = $ldiscipline->getUserNameByID($_SESSION['UserID']);
$printedBy = $iDiscipline['PrintedBy']." : ".$name[0];
# Start layout
?>
<link href='<?=$PATH_WRT_ROOT?>/templates/2007a/css/print.css' rel='stylesheet' type='text/css'>
<style type='text/css'>
<!--
print_hide {display:none;}
-->
</style>

<br />
<form name="form1" method="post" action="">
<table width="95%" cellpadding="4" cellspacing="0" border="0">
	<tr>
		<td class='tabletext'><?=$i_general_class." : ". $ldiscipline->getClassNameByClassID($class)?><br /><?=$i_general_print_date." : ".(Date("d-M-y"))?><br /><?=$printedBy?></td>
		<td valign='top' class='print_hide'><?=$linterface->GET_BTN($button_print, "button", "javascript:window.print()")?></td>
	<tr>
	<tr>
		<td><?=$result?></td>
	<tr>
</table>

<input type="hidden" name="year" id="year" value="<?=getCurrentAcademicYear()?>">
</form>
<?
intranet_closedb();
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");

?>
