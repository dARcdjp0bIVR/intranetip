<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-RankingReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

ini_set("memory_limit", "150M"); 
$filename = "ranking_report.csv";	


# School Year Menu #

//$selectSchoolYear .= "<select name='SchoolYear'>";
$selectSchoolYear .= "<option value='0'";
$selectSchoolYear .= ($SchoolYear==0) ? " selected" : "";
$selectSchoolYear .= ">".$i_Discipline_System_Award_Punishment_All_School_Year."</option>";
$selectSchoolYear .= $ldiscipline->getConductSchoolYear($SchoolYear);
//$selectSchoolYear .= "</select>";

$conds .= " AND a.RecordStatus=".DISCIPLINE_STATUS_APPROVED;

if($meritType==1) {
	$meritTitle = $iDiscipline['Awarded'];	
	$conds .= " AND meritType=1";
} else {
	$meritTitle = $iDiscipline['Punished'];	
	$conds .= " AND meritType=-1";
}

$semesterText = ($semester=='WholeYear') ? $i_Discipline_System_Award_Punishment_Whole_Year : $semester;

# Semester Menu #
if($dateChoice==2) {
	$date = $startDate ." {$i_To} ".$endDate;
	$conds .= " AND a.RecordDate BETWEEN '$startDate' AND '$endDate'";
} else {
	if($SchoolYear != '0') {		# specific school year (not "All School Year")
		$date = $SchoolYear." ".$semesterText;
		list($startYear, $endYear) = split('-',$SchoolYear);
		$conds .= " AND a.Year='$SchoolYear'";
	} else {						# all school year
		$date = $i_Discipline_System_Award_Punishment_All_School_Year." ".$semesterText;
		$academicYear = $ldiscipline->generateAllSchoolYear();
		$tempConds = "";
	}
	$conds .= ($semester != 'WholeYear') ? " AND a.Semester='$semester'" : "";
}

# Ranking Range
for($i=1;$i<=20;$i++) {
	$rankRangeMenu .= "<option value=$i";
	$rankRangeMenu .= ($i==$rankRange) ? " SELECTED" : "";
	$rankRangeMenu .= ">$i</option>";
}

# form / class
if(is_array($rankTargetDetail)) {
	$rankTargetDetail = $rankTargetDetail;
}
else {
	$rankTargetDetail[0] = $rankTargetDetail;
}
$tempConds = "";
if($rankTarget=='form') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
//		$tempConds .= "b.ClassName LIKE '$rankTargetDetail[$i]%'";
		$tempConds .= " d.ClassLevelID=$rankTargetDetail[$i]";
	}
} else if($rankTarget=='class') {
	for($i=0;$i<sizeof($rankTargetDetail);$i++) {
		$tempConds .= ($tempConds!="") ? " OR " : "";
		$tempConds .= "b.ClassName='$rankTargetDetail[$i]'";
	}
}
$conds .= " AND ($tempConds)";


# table content
if($rankTarget=='form') {
	$sql = "SELECT
				LEFT(b.ClassName,1) as form,
				COUNT(*) as total,
				b.ClassName as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				LEFT OUTER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
				LEFT OUTER JOIN
					INTRANET_CLASS as c ON (b.ClassName=c.ClassName)
				LEFT OUTER JOIN 
					INTRANET_CLASSLEVEL as d ON (c.ClassLevelID=d.ClassLevelID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY form
				ORDER BY total DESC
				LIMIT $rankRange
			";
} else {
	$name_field = getNameFieldByLang('b.');
	$sql = "SELECT
				b.ClassName,
				COUNT(*) as total,
				$name_field as name
			FROM
				DISCIPLINE_MERIT_RECORD as a
				LEFT OUTER JOIN
					INTRANET_USER as b ON (a.StudentID=b.UserID)
			WHERE
				a.DateInput IS NOT NULL
				$conds
				GROUP BY b.ClassName
				ORDER BY total DESC
				LIMIT $rankRange
			";
}
$result = $ldiscipline->returnArray($sql,3);

$exportArr = array();
$exportColumn = array($iDiscipline['Rank'], $i_Discipline_Class, $i_Discipline_No_of_Records);

if(sizeof($result)==0) {
	$exportArr[0][0] = $i_no_record_exists_msg;	
}

for($i=0;$i<sizeof($result);$i++) {
	$k = $i+1;
	$exportArr[$i][0] = $k;
	$exportArr[$i][1] = ($rankTarget=='form') ? "Form ".$result[$i][0] : $result[$i][0];
	$exportArr[$i][2] = $result[$i][1];
}

$criteria = ($rankTarget=='class') ? $i_Discipline_Class : $i_Discipline_Form;

$export_content = $date." ".$i_general_most." ".$meritTitle." ".$criteria."\n\n";
$export_content .= $lexport->GET_EXPORT_TXT($exportArr, $exportColumn, "\t", "\r\n", "\t", 0, "11");

intranet_closedb();

$lexport->EXPORT_FILE($filename, $export_content);

?>
