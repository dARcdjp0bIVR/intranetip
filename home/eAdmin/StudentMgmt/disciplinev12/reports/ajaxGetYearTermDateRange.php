<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$json = new JSON_obj();

$AcademicYearID = IntegerSafe($AcademicYearID);
$YearTermID = IntegerSafe($YearTermID);

$ary = getPeriodOfAcademicYear($AcademicYearID, $YearTermID);
$ary['StartDate'] = date("Y-m-d", strtotime($ary['StartDate']));
$ary['EndDate'] = date("Y-m-d", strtotime($ary['EndDate']));

header('Content-type: application/json');
echo $json->encode($ary);

intranet_closedb();
?>