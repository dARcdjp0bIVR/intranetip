<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12_cust();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['MoPuiChingDailyReport']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}

$reqData = $_REQUEST;

$reqData["UserIDs"] = Get_User_Array_From_Common_Choose($reqData["Recipient"]);
$reqData["UserIDs"] = array_filter($reqData["UserIDs"]);

echo $ldiscipline_ui->getMoPuiChingDailyReport($reqData);

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>
<? if($format == 'print'){
	// $fontstyle = 'font-family: "Arial Unicode MS", Helvetica, Arial, 新細明體, "細明體_HKSCS", sans-serif;';
	$fontstyle = 'font-family: "Arial", "Helvetica", "新細明體", "細明體_HKSCS", "sans-serif"; line-height: 24px;';
?>
<style type="text/css">
@charset "utf-8";

/* CSS Document */
/*
.main_container { display:block; width:713px; height:1000px; margin:auto; / *border:1px solid #DDD;* / padding:30px 20px; position:relative; }
.main_container .report_header { font-size:15pt; margin-bottom:12px; font-family: "Times New Roman", "Arial", "Lucida Console"; }
.main_container .report_content	{ border-top:1px solid black; padding: 12px 0; font-family: "Times New Roman", "Arial", "Lucida Console"; }
.main_container .report_content	.report_data { font-size:12pt; font-family: "Times New Roman", "Arial", "Lucida Console"; }
*/

.main_container { width:713px; /*width:100%; max-width:958px; height:1000px; */ display:block; margin:0px auto; padding:20px 30px; position:relative; }
.main_container.student_mode { width:713px; /*width:100%; max-width:958px;*/ height:auto; padding:0px; padding-top:0px; padding-bottom:0px; margin-bottom:100px; page-break-inside: avoid; }
.main_container.student_mode span.classname { padding-right:30px; font-size:21px; }

.main_container table { font-size:16px; <?php echo $fontstyle; ?> width:96%!important; margin:0px auto; }
.main_container table table { width:100%!important; }
.main_container table td.thtitle { font-size:14px; }
.main_container .report_header { font-size:21px; margin-bottom:12px; <?php echo $fontstyle; ?> }
.main_container .report_content	{ border-top:1px solid black; padding: 12px 0; <?php echo $fontstyle; ?> }


.main_container .report_content	td { vertical-align: top; }
.page_break { page-break-after:always; margin:0; padding:0; line-height:0; font-size:0; height:0; }

.main_container .class_info { font-size: 1.5em; font-weight: bold; }

@media print {
	.main_container { width:713px; }
	.main_container.student_mode { font-size:12px; width:713px; }
	.main_container table { width:100%!important; }
}
</style>

<? } ?>