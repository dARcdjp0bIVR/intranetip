<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# user access right checking
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['MoPuiChingDailyReport']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Setting of Current Page
$CurrentPage = "Reports_MoPuiChing_Daily_Report";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['MoPuiChingDailyReport']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# School Year
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();

$commonChooseLink = '../../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectRecipients&permitted_type=2&excluded_type=4&DisplayGroupCategory=1&Disable_AddGroup_Button=1&filterECAActivity=1&custFollowUpJs=1&logToDB=1&includeNoPushMessageUser=1';

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">

function showResult()
{
	var yearClassId = '';
	
	var url = "../ajaxGetLive.php?target=class";
	url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	url += "&academicYearId=<?=$AcademicYearID?>";
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function showRankTargetResult() {
	showResult();
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function submitForm(format)
{
	MoPuiChingAppJS.func.formSubmitHandle(format);
	return false;
	var valid = true;
	var form_obj = $('#form1');
	
	var current_date = $.trim($('#targetDate').val());
	if(current_date == ''){
		valid = false;
		$('#targetDate').focus();
	}
	
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('targetDate'))){
			valid = false;
			$('#targetDate').focus();
		}
	}
		
	if(valid){
		$('#DateWarnDiv span').html('');
		$('#DateWarnDiv').hide();
	}
	
	if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
		valid = false;
		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
		$('#TargetWarnDiv').show();
	}else{
		$('#TargetWarnDiv').hide();
	}
	
	if(!valid) return;
	
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target','');
		Block_Element('PageDiv');
		
		$.post(
			'report.php',
			$('#form1').serialize(),
			function(data){
				$('#ReportDiv').html(data);
				document.getElementById('div_form').className = 'report_option report_hide_option';
				$('.spanShowOption').show();
				$('.spanHideOption').hide();
				$('.Form_Span').hide();
				UnBlock_Element('PageDiv');
			}
		);
	}else if(format == 'csv'){
		form_obj.attr('target','');
		form_obj.submit();
	}
	else if(format == 'print'){
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

var MoPuiChingAppJS = {
	vars: {
		valid: true
	},
	listeners: {
		changeOptHandler: function(e) {
			$("table.reportTypeOption").hide();
			var selectedType = $(this).attr('id'); 
			$("table." + selectedType).show();
			$('div.submitAction').show();
		}
	},
	func: {
		init: function() {
			$("input[name='reportType']").bind('click', MoPuiChingAppJS.listeners.changeOptHandler);

			var selectedType = $("input[name='reportType']:checked").attr('id');
			$("table." + selectedType).show();
			$('div.submitAction').show();
		},
		checkDate: function(date_field) {
			var form_date = $.trim($('#' + date_field).val());
			if(form_date == ''){
				MoPuiChingAppJS.vars.valid = false;
				$('#' + date_field).focus();

				if (MoPuiChingAppJS.vars.valid) {
					if(!check_date_without_return_msg(document.getElementById('#' + date_field))){
						MoPuiChingAppJS.vars.valid = false;
						$('#' + date_field).focus();
					}
				}
			}
		},
		checkSelectBox: function(selectName, warningTarget, mustSelected) {
			var selectedOpt = true;
			if (typeof mustSelected != "undefined") {
				selectedOpt = mustSelected;
			}
			var optStr = "option";
			if (selectedOpt) {
				optStr = "option:selected";
			}
			
			var selectedClass = $("select[name='" + selectName + "']").eq(0).find(optStr).length;
			
			if ( typeof selectedClass == "undefined" || selectedClass <= 0 ) {
				MoPuiChingAppJS.vars.valid = false;
				var msg = '<?php echo $i_alert_pleaseselect.$iDiscipline['RankingTarget']; ?>';
				switch (warningTarget) {
					case "RecipientWarnDiv":
						msg = '<?php echo $i_alert_pleaseselect.$Lang['eDiscipline']['MoPuiChingDailyReport']['Student']; ?>';
						break; 
				}
				$('#' + warningTarget + ' span').html(msg);
				$('#' + warningTarget).show();
			} else {
				$('#' + warningTarget).hide();
			}
		},
		formSubmitHandle: function(format) {
			try {
				MoPuiChingAppJS.vars.valid = true;
				var form_obj = $('#form1');
				var reportType = $('input[name="reportType"]:checked').val();
				
				switch (reportType) {
					case "student":
						MoPuiChingAppJS.func.checkDate("textFromDate");
						if (MoPuiChingAppJS.vars.valid) {
							MoPuiChingAppJS.func.checkDate("textToDate");
							if (MoPuiChingAppJS.vars.valid) {
								var FromDate = new Date($('#textFromDate').val());
								var ToDate = new Date($('#textToDate').val());
								var diff = ToDate.getTime() - FromDate.getTime(); 
								if (diff < 0 || diff == "Invalid Date") {
									valid = false;
									$('#textToDate').focus();
								}
							}
						}
						if (MoPuiChingAppJS.vars.valid) {
							MoPuiChingAppJS.func.checkSelectBox("Recipient[]", "RecipientWarnDiv", false);
						}
						break;
					default:
						MoPuiChingAppJS.func.checkDate("targetDate");
						if (MoPuiChingAppJS.vars.valid) {
							MoPuiChingAppJS.func.checkSelectBox("rankTargetDetail[]", "TargetWarnDiv");
						}
						break;
				}
				
				if (MoPuiChingAppJS.vars.valid) {
					if (reportType == "student") {
						$('#userSelector').find('option').each(function() {
							$(this).attr('selected', 'selected'); 
						});
						SelectAll(document.getElementById("userSelector"));
					}
					
					form_obj.attr('target','_blank');
					form_obj.submit();
				}
			} catch (err) {
				console.log(err);
			}
		}
	}
};

$(document).ready(function(){
	MoPuiChingAppJS.func.init();
	showRankTargetResult();
});

</script>

<div id="PageDiv">

<div id="div_form">
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php" onsubmit="submitForm('print'); return false;">
		<table class="form_table_v30">
			
			<!-- Year -->
			<tr valign="top">
				<td class="field_title"><?=$Lang['General']['SchoolYear']?></td>
				<td><?=getAYNameByAyId($AcademicYearID)?></td>
			</tr>
			
			<!-- Option -->
			<tr valign="top">
				<td class="field_title"><?php echo $Lang['eDiscipline']['MoPuiChingDailyReport']['PleaseSelectOption']; ?></td>
				<td>
					<input type="radio" name="reportType" value="class" id="reportType_class" checked><label for="reportType_class"><?php echo $Lang['eDiscipline']['MoPuiChingDailyReport']['Class']; ?></label>&nbsp;&nbsp;
					<input type="radio" name="reportType" value="student" id="reportType_student"><label for="reportType_student"><?php echo $Lang['eDiscipline']['MoPuiChingDailyReport']['Student']; ?></label>
				</td>
			</tr>
		</table>
		<table class="form_table_v30 reportTypeOption reportType_class" style="display:none">
			<!-- Date -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['MoPuiChingDailyReport']['Date']?></td>
				<td><table class="inside_form_table">
						<tr><td>
							<?=$linterface->GET_DATE_PICKER("targetDate",$targetDate,"")?>
							<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
						</td></tr>
				</table></td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_Discipline_Class?></td>
				<td><table class="inside_form_table">
						<tr><td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
						</td></tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['MoPuiChingDailyReport']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
			</td></tr>
		</table>
		
		<table class="form_table_v30 reportTypeOption reportType_student" style="display:none">
			<!-- Date -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['MoPuiChingDailyReport']['Date']?></td>
				<td><table class="inside_form_table">
					<tr>
						<td><?=$i_From?> </td>
						<td><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate)?><?/*=$linterface->GET_DATE_FIELD2("textFromDateSpan", "form1", "textFromDate", $textFromDate, 1, "textFromDate", "onclick=\"changeRadioSelection('DATE')\"")*/?>&nbsp;</td>
						<td> <?=$i_To?> </td>
						<td><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate)?><?/*=$linterface->GET_DATE_FIELD2("textToDateSpan", "form1", "textToDate", $textToDate, 1, "textToDate", "onclick=\"changeRadioSelection('DATE')\"")*/?>&nbsp;</td>
					</tr>
				</table></td>
			</tr>		
			<!-- Date -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['MoPuiChingDailyReport']['Date']?></td>
				<td><table border="0">
				<tr>
					<td><select id="userSelector" name="Recipient[]" size="10" multiple><option><?php for($i = 0; $i < 40; $i++) echo "&nbsp;"; ?></option></select></td>
					<td width="50%"">
					<? if ($plugin['eClassApp'] || $plugin['eClassTeacherApp']) {
						// select student 
					?>
						<?= $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "newWindow('".$commonChooseLink."',3)") ?>
					<? } else { 
						// select parent directly	
					?>
						<?= $linterface->GET_BTN($Lang['General']['ChooseUser'], "button", "newWindow('../../../../common_choose/index.php?fieldname=Recipient[]&page_title=SelectRecipients&permitted_type=2&excluded_type=4&DisplayGroupCategory=1&Disable_AddGroup_Button=1&filterECAActivity=1',3)") ?>
					<? } ?>
					
					<br /> <br />
					<?= $linterface->GET_BTN($Lang['Btn']['RemoveSelected'], "button", "checkOptionRemove(document.form1.elements['Recipient[]']);commonChooseFollowUpAction()") ?>
					<br /> <br />
					<span style="display:none" id="outerspan"><span id="numOfUser" name="numOfUser" >x</span> <?=$Lang['MessageCenter']['UserSelected']?></span>
					
					<?=$linterface->Get_Form_Warning_Msg("RecipientWarnDiv","")?>
					</td>
				</tr>
				</table></td>
			</tr>		
		</table>
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30 submitAction">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")?>
			<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="format" id="format" value="print">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
