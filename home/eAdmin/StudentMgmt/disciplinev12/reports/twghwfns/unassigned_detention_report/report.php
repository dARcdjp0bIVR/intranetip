<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['WFN_Reports']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

/*
$format = $_POST['format'];
$rankTarget = $_POST['rankTarget'];
$radioPeriod = $_POST['radioPeriod'];
$selectYear = $_POST['selectYear']; 
$selectSemester = $_POST['selectSemester'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];
*/

# Condition
if($rankTarget == 'form'){
	$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}
else if($rankTarget == 'class'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}
else if($rankTarget == 'student'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
	$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$studentID)."')";
}

# Get Term Period
$TermPeriod = getPeriodOfAcademicYear($selectYear, ($selectSemester? $selectSemester : ""));
$TermStartDate = date("Y-m-d", strtotime($TermPeriod['StartDate']));
$TermEndDate = date("Y-m-d", strtotime($TermPeriod['EndDate']));

# Style
$EmptySymbol = $Lang['General']['EmptySymbol'];
$StylePrefix = '<span class="tabletextrequire">';
$StyleSuffix = '</span>';
if($format == 'csv'){
	$StylePrefix = '';
	$StyleSuffix = '';
}

# Fields
$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$StudentNameField = getNameFieldByLang("iu.");
$clsName = $YearClassNameField;

# Get student in today detention
$sql = "SELECT
			r.StudentID,
			$YearClassNameField as ClassName,
			ycu.ClassNumber,
			IF(iu.UserID IS NOT NULL, iu.ChineseName, au.ChineseName) as ChineseName,
			IF(iu.UserID IS NOT NULL, iu.EnglishName, au.EnglishName) as EnglishName
		FROM
			DISCIPLINE_DETENTION_STUDENT_SESSION as r
			INNER JOIN DISCIPLINE_DETENTION_SESSION ds ON r.DetentionID = ds.DetentionID 
			LEFT JOIN INTRANET_USER as iu ON r.StudentID = iu.UserID 
			LEFT JOIN INTRANET_ARCHIVE_USER as au ON r.StudentID = au.UserID 
			LEFT JOIN YEAR_CLASS_USER as ycu ON (r.StudentID = ycu.UserID OR au.UserID = ycu.UserID) 
			LEFT JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID AND yc.AcademicYearID = '$selectYear') 
			LEFT JOIN YEAR as y ON yc.YearID = y.YearID 
		WHERE
			r.DetentionID IS NOT NULL AND r.RecordStatus='1' AND 
			yc.AcademicYearID = '".$selectYear."' AND ds.DetentionDate = '$textTargetDate'
			$yearIdCond $classIdCond $studentIdCond 
		GROUP BY
			r.StudentID 
		ORDER BY
			y.Sequence, yc.Sequence, ycu.ClassNumber+0";
$studentRecords = $ldiscipline->returnArray($sql);
$studentIDAry = Get_Array_By_Key($studentRecords, "StudentID");
$student_size = count($studentRecords);

# Get student remaining detention
$sql = "SELECT
			r.RecordID, r.StudentID, r.Reason 
		FROM
			DISCIPLINE_DETENTION_STUDENT_SESSION as r 
		WHERE
			(r.DetentionID IS NULL OR r.AttendanceTakenBy IS NULL OR r.AttendanceStatus IS NULL OR r.AttendanceStatus = 'ABS') AND r.RecordStatus='1' 
			AND r.StudentID IN ('".implode("', '", (array)$studentIDAry)."')
			AND SUBSTR(r.Reason, 1, 10) >= '$TermStartDate' AND SUBSTR(r.Reason, 1, 10) <= '$textTargetDate'
		ORDER BY
			r.Reason";
$detentionRecords = $ldiscipline->returnArray($sql);
$detentionRecords = BuildMultiKeyAssoc((array)$detentionRecords, array("StudentID", "RecordID"));

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}

$x .= '<style type="text/css" media="print"> .print-style { page-break-inside: avoid;}</style>';
$x .= '<style type="text/css">body {font-size:1em;}</style>';
if($format == 'web'){
	$x .= '<div class="content_top_tool">
			<div class="Conntent_tool">
				<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
			</div>
			<br style="clear:both" />
		  </div><br />';
}
if($format == 'print'){
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
			</tr>
			</table>';
			
	$school_name = GET_SCHOOL_NAME();
	$x .= '<h2 style="text-align:center;">'.$school_name.'</h2>';
}

$x .= '<h3>'.str_replace('<!--DATE-->',date('d/m/Y',strtotime($textTargetDate)),'截至 <!--DATE--> 留堂餘數紀錄').'</h3>';

if($student_size > 0)
{
	// loop Student
	for($i=0; $i<$student_size; $i++)
	{
		# Student Info
		$thisStudentInfo = $studentRecords[$i];
		$thisStudentUserID = $thisStudentInfo["StudentID"];
		$thisStudentName = Get_Lang_Selection($thisStudentInfo["ChineseName"], $thisStudentInfo["EnglishName"]);
		$thisStudentClassName = $thisStudentInfo["ClassName"];
		$thisStudentClassNumber = $thisStudentInfo["ClassNumber"];
		
		# Student Detention
		$thisStudentDetention = (array)$detentionRecords[$thisStudentUserID];
		$detention_count = count($thisStudentDetention);
		
		$y = '<div class="print-style">';
			$y .= '<table align="center" style="width:95%;border-collapse:collapse;border:3px solid black;font-family:標楷體,sans-serif;">';
				$y .= '<tr>';
					$y .= '<th width="10%" style="border-bottom:3px solid black;padding:8px 4px 4px 4px;font-weight:bold;">班別</th><th width="10%" style="border-bottom:3px solid black;padding:8px 4px 4px 4px;text-align:right;font-weight:bold;">學號</th><th width="30%" style="border-bottom:3px solid black;padding:8px 4px 4px 4px;text-align:center;font-weight:bold;">學生姓名</th><th width="50%">&nbsp;</th>';
				$y .= '</tr>';
				$y.='<tr">';
					$y.='<td style="padding:4px;vertical-align:top;">'.$thisStudentClassName.'</td><td style="padding:4px;text-align:right;vertical-align:top;">'.$thisStudentClassNumber.'</td><td style="padding:4px;text-align:center;vertical-align:top;">'.$thisStudentName.'</td>';
					$y.='<td>';
					$y.='<table style="width:100%;border-collapse:collapse;border:3px solid black;font-family:標楷體,sans-serif;text-align:center;">';
						$y .= '<tr style="border-bottom:3px solid black;">';
							$y .= '<td style="padding:4px;font-weight:bold;">功課名稱</td><td style="padding:4px;font-weight:bold;">日期</td><td style="padding:4px;font-weight:bold;">科目</td>';
						$y .= '</tr>';
					
					if($detention_count > 0) {
						// loop Detention
						foreach($thisStudentDetention as $thisDetentionInfo)
						{
							# Detention Info
							$thisDetentionReason = $thisDetentionInfo["Reason"];
							$reason_date = date("d/m/Y", strtotime(substr($thisDetentionReason, 0, 10)));
							$parts = explode("-", substr($thisDetentionReason, 11));
							$reason_number = trim($parts[1]);
							$reason_subject = trim($parts[0]);
							
							$y .= '<tr>';
								$y .= '<td style="padding:4px;">'.$reason_number.'</td><td style="padding:4px;">'.$reason_date.'</td><td style="padding:4px;">'.$reason_subject.'</td>';
							$y .= '</tr>';
						}
					}
					else {
						$y .= '<tr>';
							$y .= '<td colspan="3">---</td>';
						$y .= '</tr>';
					}
					
					$y.='</table>';
					$y.='</td>';
				$y.='</tr>';	
			$y .= '</table><br />';
		$y .= '</div>';
		$x .= $y;
	}
}

echo $x;

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>