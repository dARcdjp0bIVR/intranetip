<?php
// Editing by Bill

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# setting the current page
$CurrentPage = "Reports_TWGHWFNS_Cancel_Misconduct_Report";
$CurrentPageArr['eDisciplinev12'] = 1;
$TAGS_OBJ[] = array($Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# School year
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\" onchange=\"changeYearTermDateRange();\">";
$selectSemesterHTML .= "</select>";

# Date
$textFromDate = date("Y-m-d", strtotime(getStartDateOfAcademicYear($selectYear, '')));
$textToDate = date("Y-m-d", strtotime(getEndDateOfAcademicYear($selectYear, '')));

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="JavaScript">
function changeTerm(val) {
	if (val.length==0) {
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
	
	var currentDateOnly = false;
	if(val==0) {
		val = $("#selectYear").val();
		var currentDateOnly = true;
	}
	
	$.get(
		'../../ajaxGetSemester.php?year='+val+'&term=<?=$selectSemester?>'+'&field=selectSemester&onchange=changeYearTermDateRange()',
		{},
		function(responseText){
			document.getElementById("spanSemester").innerHTML = responseText;
			document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
			
			if(currentDateOnly) {
				$('#textFromDate').val("<?=date("Y-m-d")?>");
				$('#textToDate').val("<?=date("Y-m-d")?>");
			}
			else {
				changeYearTermDateRange();
			}
		}
	);
}

function changeRadioSelection(type)
{
	/*
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
	*/
	showRankTargetResult($('#rankTarget').val(),'');
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		//document.getElementById('selectAllBtn01').disabled = true;
		//document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		$('#selectAllBtn01').hide();
		var temp = document.getElementById('studentID[]');
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		//document.getElementById('selectAllBtn01').disabled = false;
		//document.getElementById('selectAllBtn01').style.visibility = 'visible';
		$('#selectAllBtn01').show();
	}
}

function showResult(str, choice)
{
//	if(str != "student2ndLayer")
//		document.getElementById("studentFlag").value = 0;
//	else
//		document.getElementById("studentFlag").value = 1;
	
	if (str.length==0) {
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
	
	var yearClassId = '';
	if(str == 'student2ndLayer'){
		var options = $('select#rankTargetDetail\\[\\] option:selected');
		var delim = '';
		for(var i=0;i<options.length;i++){
			yearClassId += delim + options.get(i).value;
			delim = ',';
		}
	}
	
	var url = "../../ajaxGetLive.php";
	var data = {"target":str,"rankTargetDetail":choice,"selectSize":8};
	//url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	if(str == 'form'){
		//url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
	}
	else if(str == 'class'){
		//url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
	}
	else if(str == 'student'){
		//url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&onchange="+encodeURIComponent("showRankTargetResult('student2ndLayer','');");
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
		data["onchange"] = encodeURIComponent("showRankTargetResult('student2ndLayer','');");
	}
	else if(str == 'student2ndLayer'){
		//url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]&studentFieldId=studentID[]&studentFieldName=studentID[]&YearClassID="+yearClassId;
		data["fieldId"] = "rankTargetDetail[]";
		data["fieldName"] = "rankTargetDetail[]";
		data["studentFieldId"] = "studentID[]";
		data["studentFieldName"] = "studentID[]";
		data["YearClassID"] = yearClassId;
	}
	//if($('input[name=radioPeriod]:checked').val()=='YEAR'){
		//url += "&academicYearId="+$('#selectYear').val();
		data["academicYearId"] = $('#selectYear').val();
	//}else{
		//url += "&academicYearId=<?=$AcademicYearID?>";
		//data["academicYearId"] = "<?=$AcademicYearID?>";
	//}
	//url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	//url += "&student=1&value="+choice;
	
	$.post(
		url,
		data,
		function(responseText){
			var showIn = "rankTargetDetail";
			//if(document.getElementById("studentFlag").value == 0) {
			if(str == 'student2ndLayer'){
				showIn = "spanStudent";	
			}
			//document.getElementById(showIn).innerHTML = responseText;
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
			
			if(str == 'student'){
				showRankTargetResult('student2ndLayer','');
			}
		}
	);
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);
	if (val=='student2ndLayer') {
		showSpan('spanStudent');
	}
	else {
		hideSpan('spanStudent');
	}
}

function hideOptionLayer()
{
	//$('.Form_Span').attr('style', 'display: none');
	$('.Form_Span').hide();
	$('.spanHideOption').hide();
	$('.spanShowOption').show();
	//$('.spanHideOption').attr('style', 'display: none');
	//$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	//$('.Form_Span').attr('style', '');
	$('.Form_Span').show();
	$('.spanHideOption').show();
	$('.spanShowOption').hide();
	//$('.spanShowOption').attr('style', 'display: none');
	//$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	var radio_period = $('input[name="radioPeriod"]:checked').val();
	var rank_target = $('#rankTarget').val();
	
	if(radio_period == 'DATE'){
		var from_date = $.trim($('#textFromDate').val());
		var to_date = $.trim($('#textToDate').val());
		
		if(from_date == '' || to_date == ''){
			valid = false;
			$('#textFromDate').focus();
		}
		if(valid){
			if(!check_date_without_return_msg(document.getElementById('textFromDate'))){
				valid = false;
				$('#textFromDate').focus();
			}
			if(!check_date_without_return_msg(document.getElementById('textToDate'))){
				valid = false;
				$('#textToDate').focus();
			}
		}
		if(valid){
			if(from_date > to_date){
				valid = false;
				$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
				$('#DateWarnDiv').show();
				$('#textFromDate').focus();
			}
		}
		if(valid){
			$('#DateWarnDiv span').html('');
			$('#DateWarnDiv').hide();
		}
	}
	
	if(rank_target == 'form' || rank_target == 'class'){
		if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
			$('#TargetWarnDiv').show();
		}
		else{
			$('#TargetWarnDiv').hide();
		}
	}
	else if(rank_target == 'student'){
		if($('select#studentID\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
			$('#TargetWarnDiv').show();
		}
		else{
			$('#TargetWarnDiv').hide();
		}
	}
	if(!valid) return;
	
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target','');
		Block_Element('PageDiv');
		
		$.post(
			'report.php',
			$('#form1').serialize(),
			function(data){
				$('#ReportDiv').html(data);
				document.getElementById('div_form').className = 'report_option report_hide_option';
				$('.spanShowOption').show();
				$('.spanHideOption').hide();
				$('.Form_Span').hide();
				UnBlock_Element('PageDiv');
			}
		);
	}
	else if(format == 'csv'){
		form_obj.attr('target','');
		form_obj.submit();
	}
	else if(format == 'print'){
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

function changeYearTermDateRange()
{
	var yearTermId = $("#selectSemester").val() == '0'?'':$("#selectSemester").val();
	$.post(
		"../../ajaxGetYearTermDateRange.php",
		{
			"AcademicYearID": $('#selectYear').val(),
			"YearTermID":yearTermId
		},
		function(returnData)
		{
			var obj = JSON.parse(returnData);
			$('#textFromDate').val(obj['StartDate']);
			$('#textToDate').val(obj['EndDate']);
		}
	);
}

$(document).ready(function(){
	changeTerm(0);
	showRankTargetResult($("#rankTarget").val(), '');
});
</script>

<div id="PageDiv">
<div id="div_form">
	<span id="spanShowOption" class="spanShowOption" style="display:none">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php" onsubmit="submitForm('web');return false;">
		<table class="form_table_v30">
			<!-- Type -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Type']?></td>
				<td>
					<input name="reportType" type="radio" id="reportTypeHomework" value="H" <? echo ($reportType=="" || $reportType=="H")?" checked":"" ?>>
					<label for='reportTypeHomework'><?=$Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Homework']?></label>
					&nbsp;&nbsp;
					<input name="reportType" type="radio" id="reportTypeLate" value="L" <? echo ($reportType=="L")?" checked":"" ?>>
					<label for='reportTypeLate'><?=$Lang['eDiscipline']['TWGHWFNS_Cancel_Misconduct_Report']['Late']?></label>
				</td>
			</tr>
								
			<!-- Period -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td>
								<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
								<?=$selectSchoolYearHTML?>&nbsp;
								<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
								<span id="spanSemester"><?=$selectSemesterHTML?></span>
							</td>
						</tr>
						<tr>
							<td>
								<?=$Lang['General']['From']?>
								<?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
								<?=$Lang['General']['To']?>
								<?=$linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
								<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"]?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td valign="top">
								<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
									<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
									<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
									<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
								</select>
							</td>
							<td valign="top" nowrap>
								<!-- Form / Class //-->
								<span id='rankTargetDetail'>
								<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
									
								<!-- Student //-->
								<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
									<select name="studentID[]" multiple size="5" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
								</span>
								
							</td>
						</tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit")?>
		<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="format" id="format" value="web">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>