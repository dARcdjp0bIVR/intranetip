<?php
// Editing by Bill

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Access right checking
$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

// Condition
if($rankTarget == 'form'){
	$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}
else if($rankTarget == 'class'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
}
else if($rankTarget == 'student'){
	$classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$rankTargetDetail)."') ";
	$studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$studentID)."')";
}

// Misconduct Category
if($reportType=="H") {
	$thisCategoryID = $sys_custom['eDiscipline']['BWWTC_WithoutSubmCatID'];
}
else if($reportType=="L") {
	$thisCategoryID = PRESET_CATEGORY_LATE;
}
else {
	$thisCategoryID = "";
}

// Style
$EmptySymbol = $Lang['General']['EmptySymbol'];
$StylePrefix = '<span class="tabletextrequire">';
$StyleSuffix = '</span>';
if($format == 'csv'){
	$StylePrefix = '';
	$StyleSuffix = '';
}

// Get Upgraded Records
$sql = "SELECT 
			mr.RecordID,
			mr.StudentID,
			yc.YearClassID,
			yc.ClassTitleB5 as ClassName,
			ycu.ClassNumber,
			IF(iu.UserID IS NOT NULL, iu.ChineseName, au.ChineseName) as StudentName,
			mr.RecordDate,
			mr.ProfileMeritType,
			mr.ProfileMeritCount,
			ar.CountNo,
			mr.DateInput
		FROM
			DISCIPLINE_MERIT_RECORD as mr 
			INNER JOIN DISCIPLINE_ACCU_RECORD as ar ON (ar.UpgradedRecordID = mr.RecordID)
			LEFT JOIN INTRANET_USER as iu ON (iu.UserID = mr.StudentID) 
			LEFT JOIN INTRANET_ARCHIVE_USER as au ON (au.UserID = mr.StudentID) 
			LEFT JOIN YEAR_CLASS_USER as ycu ON (ycu.UserID = mr.StudentID OR ycu.UserID = au.UserID) 
			LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear') 
			LEFT JOIN YEAR as y ON (y.YearID = yc.YearID) 
		WHERE
			mr.AcademicYearID = '$selectYear' AND (mr.RecordDate BETWEEN '$textFromDate' AND '$textToDate') AND 
			ar.CategoryID = '".$thisCategoryID."' AND mr.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND mr.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'
			$yearIdCond $classIdCond $studentIdCond 
		GROUP BY
			mr.RecordID 
		ORDER BY
			mr.RecordDate DESC, y.Sequence ASC, yc.Sequence ASC, ycu.ClassNumber+0 ASC";
$records = $ldiscipline->returnResultSet($sql);

// Report Header
if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");
}
$x .= "<style type='text/css' media='print'> .print-style { page-break-inside: avoid; } </style>";
$x .= "<style type='text/css'>";
	$x .= ".main_container { display: block; width: 713px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }";
	$x .= ".print-style table { padding: 4px; } ";
	$x .= ".print-style td { font-size: 19px; padding: 2px; text-align: left; } ";
	$x .= ".print-style td.report_main_content { font-size: 21px; } ";
	$x .= ".print-style table.report_header td { padding-bottom: 7px; } ";
	$x .= ".print-style table.report_result td.report_main_content { padding: 6px 2px 6px 2px; } ";
	$x .= ".print-style table.report_footer { padding-bottom: 30px; } ";
	$x .= ".page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }";
$x .= "</style>";

if($format == 'web'){
	$x .= '<div class="content_top_tool">
			<div class="Conntent_tool">
				<!--<a href="javascript:submitForm(\'csv\');" class="export">'.$Lang['Btn']['Export'].'</a>-->
				<a href="javascript:submitForm(\'print\')" class="print">'.$Lang['Btn']['Print'].'</a>
			</div>
			<br style="clear:both" />
		  </div><br />';
}
if($format == 'print'){
	$x .= '<table width="100%" align="center" class="print_hide" border="0">
			<tr>
				<td align="right">'.$linterface->GET_SMALL_BTN($Lang['Btn']['Print'], "button", "javascript:window.print();","submit_print").'</td>
			</tr>
			</table>';
}

$record_size = count($records);
if($record_size > 0) {
	$YearClassIDAry = Get_Array_By_Key((array)$records, "YearClassID");
	$YearClassIDAry = array_unique((array)$YearClassIDAry);
	
	$sql = "Select 
				yct.YearClassID, IF(iu.UserID IS NOT NULL, iu.ChineseName, au.ChineseName) as TeacherName
			From
				YEAR_CLASS_TEACHER as yct 
				LEFT JOIN INTRANET_USER as iu ON (yct.UserID = iu.UserID)
				LEFT JOIN INTRANET_ARCHIVE_USER as au ON (yct.UserID = au.UserID) 
			Where 
				yct.YearClassID IN ('".implode("', '", (array)$YearClassIDAry)."')";
	$teacherAry = $ldiscipline->returnResultSet($sql);
	$teacherAry = BuildMultiKeyAssoc((array)$teacherAry, array("YearClassID", "TeacherName"));
	
	$x .= "<div class='main_container'>";
	
	// loop records
	$records = BuildMultiKeyAssoc((array)$records, array("RecordDate", "YearClassID", "ProfileMeritType", "ProfileMeritCount", "RecordID"));
	foreach((array)$records as $date_records) {
		foreach((array)$date_records as $class_records) {
			foreach((array)$class_records as $merit_records) {
				foreach((array)$merit_records as $merit_count_records)
				{
					$isFirstRecords = true;
					foreach((array)$merit_count_records as $upgradedInfo)
					{
						// Record Grid Header
						if($isFirstRecords)
						{
							// Merit Name
							$thisMeritName = "";
							if($reportType=="H") {
								$thisMeritName = "欠交功課";
							}
							else if($reportType=="L") {
								$thisMeritName = "遲到";
							}
					
							// Merit Type
							$thisMeritType = $upgradedInfo["ProfileMeritType"];
							$thisMeritTypeName = $Lang['eDiscipline']['TWGHWFNS_ConductPunishmentReport']['PunishmentAry'][$thisMeritType];
							$thisMeritTypeName = $thisMeritTypeName==""? "" : $thisMeritTypeName;
							
							// Merit Number
							$thisMeritNumber = (int)$upgradedInfo["ProfileMeritCount"];
							$thisMeritNumber = $thisMeritNumber > 0? $thisMeritNumber : "---";
							
							// Class Teacher
							$thisClassTeacherDisplay = "";
							$thisClassTeacher = $teacherAry[$upgradedInfo["YearClassID"]];
							if(!empty($thisClassTeacher)) {
								$thisClassTeacher = array_keys((array)$thisClassTeacher);
								$thisClassTeacherDisplay = implode('、', (array)$thisClassTeacher);
							}
							
							$y = "";
							$y .= "<div class='print-style'>";
								$y .= "<table class='report_header' style='width:100%; border:0px; text-align:center;'>";
									$y .= "<tr><td colspan='5' class='report_main_content' style='text-align: center'>TWGHs Wong Fut Nam College</td></tr>";
									$y .= "<tr>";
										$y .= "<td width='2%'>&nbsp;</td>";
										$y .= "<td width='24%'>".$upgradedInfo["ClassName"]."</td>";
										$y .= "<td width='50%'>班主任: ".$thisClassTeacherDisplay."</td>";
										$y .= "<td width='20%' style='text-align: right'>".$upgradedInfo["RecordDate"]."</td>";
										$y .= "<td width='2%'>&nbsp;</td>";
									$y .= "</tr>";
								$y .= "</table>";
								$y .= "<table class='report_result' style='width:100%; border:4px solid black; text-align:center;'>";
									$y .= "<tr>";
										$y .= "<td colspan='2'>&nbsp;</td>";
										$y .= "<td style='border-bottom: 1px solid black;'>&nbsp;欠交功課／遲到總數</td>";
									$y .= "</tr>";
									$y .= "<tr>";
											$y .= "<td colspan='3' class='report_main_content'>下到學生因".$thisMeritName."，記".$thisMeritTypeName.$thisMeritNumber."次</td>";
									$y .= "</tr>";
							
							$isFirstRecords = false;
						}
						
						// Get Conversion Period Settings
						$period_info = $ldiscipline->RETRIEVE_TARGET_CATEGORY_PERIOD_SETTING($upgradedInfo['RecordDate'], MISCONDUCT, $thisCategoryID);
						list($periodType, $period_setting) = $period_info;
						list($SettingID, $upgrade_item_id, $upgrade_item_code, $upgrade_item_name, $upgrade_count, $upgrade_demerit_type, $upgrade_demerit_count, $change_conduct_score, $UpgradeDeductSubScore1, $period_date_start, $period_date_end, $TargetYear, $TargetSemester, $PeriodID, $TemplateID, $sendNoticeAction) = $period_setting[0];
						
						// Get Misconduct Upgrade Count
						$sql = "SELECT 
									COUNT(ar.RecordID) 
								FROM 
									DISCIPLINE_ACCU_RECORD ar
									INNER JOIN DISCIPLINE_MERIT_RECORD mr ON (ar.UpgradedRecordID = mr.RecordID)
								WHERE 
									ar.StudentID = '".$upgradedInfo['StudentID']."' AND ar.CategoryID = '".$thisCategoryID."' AND ar.RecordType = '".MISCONDUCT."' AND
									ar.AcademicYearID = '".$selectYear."' AND ar.RecordDate >= '$period_date_start 00:00:00' AND ar.RecordDate <= '$period_date_end 23:59:59' AND 
									ar.UpgradedRecordID > 0 AND mr.RecordID <= '".$upgradedInfo['RecordID']."'";
						$upgrade_count = $ldiscipline->returnVector($sql);
						$upgrade_count = $upgrade_count[0];
						$upgrade_count = $upgrade_count > 0? $upgrade_count : 0;
					
						// Grid Content Row 
						$y .= "<tr class='data_tr'>";
							$y .= "<td width='20%'>&nbsp;</td>";
							$y .= "<td width='52%'>".$upgradedInfo["ClassNumber"]."&nbsp;&nbsp;".$upgradedInfo["StudentName"]."</td>";
							$y .= "<td width='28%' style='text-align:center;'>".$upgrade_count."</td>";
						$y .= "</tr>";
					}
					
					// Record Grid Footer
					if(!$isFirstRecords)
					{
								$y .= "<tr>";
									$y .= "<td colspan='3' style='border-top: 1px solid black; font-size: 18px;'>&nbsp;</td>";
								$y .= "</tr>";
							$y .= "</table>";
							$y .= "<table class='report_footer' style='width:100%; border:0px; text-align:center;'>";
								$y .= "<tr>";
									$y .= "<td width='80%' align='left'>*請班主任通知學生</td>";
									$y .= "<td width='20%' align='right'>(班主任保存)</td>";
								$y .= "</tr>";
							$y .= "</table>";
						$y .= "</div>";
						$x .= $y;
					}
				}
			}
		}
	}
	$x .= "</div>";
}

//if($format == 'csv'){
//	$lexport->EXPORT_FILE("eDiscipline_Accumulated_Late_Report.csv", $exportContent);
//}else{
	echo $x;
//}

if($format == 'print'){
	include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
}

intranet_closedb();
?>