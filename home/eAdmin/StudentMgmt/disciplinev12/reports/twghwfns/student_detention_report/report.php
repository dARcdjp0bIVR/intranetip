<?php
// Editing by 

/***************
 * 
 ***************/
 
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12_cust();

# user access right checking
if(!$sys_custom['eDiscipline']['WFN_Reports'] || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_header.php");

$ldiscipline_ui = new libdisciplinev12_ui_cust();
echo $ldiscipline_ui->getWFNDetentionReport($_POST);

include_once($PATH_WRT_ROOT."/templates/". $LAYOUT_SKIN ."/layout/print_footer.php");

intranet_closedb();
?>

<style type="text/css">
@charset "utf-8";
/* CSS Document */
.main_container { display: block; width: 713px; height: 920px; margin: auto; padding: 30px 20px; position: relative; }
.main_container .report_header .header_title_1 { font-size: 20px; font-family: "Arial", "PMingLiU", "Lucida Console"; text-align: center;  padding-top: 10px; }
.main_container .report_header .header_title_2 { font-size: 20px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding-top: 10px; }
.main_container .report_table th { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; font-weight: bold; padding-bottom: 4px; }
.main_container .report_table td { font-size: 16px; font-family: "Arial", "PMingLiU", "Lucida Console"; padding-bottom: 4px; }
.page_break { page-break-after: always; margin: 0; padding: 0; line-height: 0; font-size: 0; height: 0; }
</style>