<?php
// modifying by: 
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();
$lclass = new libclass();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-MonthlyReport-View")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Class #
$select_class = $ldiscipline->getSelectClass("name=\"targetClass\"", $targetClass, $i_Discipline_System_Award_Punishment_All_Classes, false);

$TAGS_OBJ[] = array($eDiscipline['MonthlyReport'], "");

# menu highlight setting
$CurrentPage = "MonthlyReport";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$i = 1;

Function CurrentMonthLastDay()
{
	global $day;
	
	switch (date("m")) {
		case 1:
			$day = 31;
			break;
		case 2:
			if (date("Y")%4==0)
				$day = 29;
			else
				$day = 28;
			break;
		case 3:
			$day = 31;
			break;
		case 4:
			$day = 30;
			break;
		case 5:
			$day = 31;
			break;
		case 6:
			$day = 30;
			break;
		case 7:
			$day = 31;
			break;
		case 8:
			$day = 31;
			break;
		case 9:
			$day = 30;
			break;
		case 10:
			$day = 31;
			break;
		case 11:
			$day = 30;
			break;
		case 12:
			$day = 31;
			break;
	}	
	return $day;       
}

$LastDay=CurrentMonthLastDay();	

?>
<script language="javascript">

function goCheck(form1) 
{
	var flag = 0;
	var temp = "";

	if(form1.targetClass.value == '#') 
  {
		alert("<?=$i_alert_pleaseselect?><?=$i_Discipline_Class?>");	
		return false;
	}
	
	if(form1.Section2Fr.value=='' && form1.Section2To.value=='') 
  {
    alert("<?=$i_alert_pleasefillin?><?=$i_Discipline_System_Reports_Report_Period?>");	
		return false;
	}
  	
	if((!check_date(form1.Section2Fr,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) || (!check_date(form1.Section2To,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')))
	{
		return false;
	}
	
	if(form1.Section2Fr.value > form1.Section2To.value)
	{
    alert("<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>");	
		return false;
  }	
	
	return true;
}
</script>

<form name="form1" method="post" action="monthly_report_view.php" onSubmit="return goCheck(this)">
<br>
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Class?><span class="tabletextrequire">*</span></td>
					<td class="navigation">
						<?=$select_class?>
					</td>
				</tr>
				<tr valign="top">
					<td height="57" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Reports_Report_Period?><span class="tabletextrequire">*</span></td>
					<td>
						<table border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td><?=$linterface->GET_DATE_FIELD2("Section2FrSpan", "form1", "Section2Fr", date("Y-m")."-01", 1, "Section2Fr", "onclick=\"changeRadioSelection('DATE')\"")?>&nbsp;</td>
								<td> <?=$i_To?> &nbsp;&nbsp;&nbsp;</td>
								<td><?=$linterface->GET_DATE_FIELD2("Section2ToSpan", "form1", "Section2To", date("Y-m")."-".$LastDay, 1, "Section2To", "onclick=\"changeRadioSelection('DATE')\"")?>&nbsp;</td>
							</tr>
						</table>					
					</td>
				</tr>
				<tr>
					<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<?= $linterface->GET_ACTION_BTN($button_view, "submit")?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
  $linterface->LAYOUT_STOP();
  intranet_closedb();
?>
