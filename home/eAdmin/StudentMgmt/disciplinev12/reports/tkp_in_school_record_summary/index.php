<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# user access right checking
//if(!$sys_custom['eDiscipline']['TKPSchoolRecordSummary'] && !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])
if(!$sys_custom['eDiscipline']['TKPSchoolRecordSummary'] || (!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-TKP_School_Record_Summary-View"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Setting of Current Page
$CurrentPage = "Reports_TKP_School_Record_Summary";
$CurrentPageArr['eDisciplinev12'] = 1;

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['TKPSchoolRecordSummary']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# School Year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='showResult()' ", "", $selectYear);
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 

$textFromDate = date("Y-m-d", strtotime(getStartDateOfAcademicYear($selectYear, '')));
$textToDate = date("Y-m-d", strtotime(getEndDateOfAcademicYear($selectYear, '')));

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">

function showResult()
{
	var yearClassId = '';
	
	var url = "../ajaxGetLive.php?target=class";
	url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	url += "&academicYearId="+$('#selectYear').val();
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function hideOptionLayer()
{
	$('.Form_Span').hide();
	$('.spanHideOption').hide();
	$('.spanShowOption').show();
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('.Form_Span').show();
	$('.spanHideOption').show();
	$('.spanShowOption').hide();
	document.getElementById('div_form').className = 'report_option report_show_option';

}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	
	var from_date = $.trim($('#textFromDate').val());
	var to_date = $.trim($('#textToDate').val());
	if(from_date == '' || to_date == ''){
		valid = false;
		$('#textFromDate').focus();
	}
	if(valid){
		if(!check_date_without_return_msg(document.getElementById('textFromDate'))){
			valid = false;
			$('#textFromDate').focus();
		}
		if(!check_date_without_return_msg(document.getElementById('textToDate'))){
			valid = false;
			$('#textToDate').focus();
		}
	}
	if(valid){
		if(from_date > to_date){
			valid = false;
			$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			$('#DateWarnDiv').show();
			$('#textFromDate').focus();
		}
	}
	if(valid){
		$('#DateWarnDiv span').html('');
		$('#DateWarnDiv').hide();
	}
	
	if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
		valid = false;
		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
		$('#TargetWarnDiv').show();
	}else{
		$('#TargetWarnDiv').hide();
	}
	
	if(!valid) return;
	
	$('input#format').val(format);
	
	if(format == 'web'){
		form_obj.attr('target','');
		Block_Element('PageDiv');
		
		$.post(
			'report.php',
			$('#form1').serialize(),
			function(data){
				$('#ReportDiv').html(data);
				document.getElementById('div_form').className = 'report_option report_hide_option';
				$('.spanShowOption').show();
				$('.spanHideOption').hide();
				$('.Form_Span').hide();
				UnBlock_Element('PageDiv');
			}
		);
	}else if(format == 'csv'){
		form_obj.attr('target','');
		form_obj.submit();
	}
	else if(format == 'pdf'){
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

$(document).ready(function(){
	showResult();
});

</script>

<div id="PageDiv">

<div id="div_form">
	<span id="spanShowOption" class="spanShowOption" style="display:none">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption']?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php">
		<table class="form_table_v30">
		
			<!-- Year -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['TKPSchoolRecordSummary']['SchoolYear']?></td>
				<td><table class="inside_form_table">
						<tr><td colspan="6"">
							<?=$selectSchoolYearHTML?>
						</td></tr>
				</table></td>
			</tr>
			
			<!-- Date Range -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['TKPClassDemeritRecord']['Period']?></td>
				<td><table class="inside_form_table">
						<tr><td >
								<td><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
								<br><span id='div_DateEnd_err_msg'></span></td>
								<td><?=$i_To?> </td>
								<td><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
								<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
						</td></tr>
				</table></td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$i_Discipline_Class?></td>
				<td><table class="inside_form_table">
						<tr><td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
						</td></tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['TKPSchoolRecordSummary']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
			</td></tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm('pdf');")?>
			&nbsp;
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "submitForm('csv');")?>
		</div>
		
		<input type="hidden" name="format" id="format" value="web">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
