<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_cust.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$ldiscipline = new libdisciplinev12_cust();

//if(!$sys_custom['eDiscipline']['TKPSchoolRecordSummary'] || !$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])
if(!$sys_custom['eDiscipline']['TKPSchoolRecordSummary'] || (!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-REPORTS-TKP_School_Record_Summary-View"))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ldiscipline_ui = new libdisciplinev12_ui_cust();

# POST data
$format = $_POST['format'];
$selectYear = $_POST['selectYear'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];

# Year - date range
$SQL_startdate = getStartDateOfAcademicYear($selectYear, '');
$SQL_enddate = getEndDateOfAcademicYear($selectYear, '');
$yearFromDate = date("Y-m-d", strtotime($SQL_startdate));
$yearToDate = date("Y-m-d", strtotime($SQL_enddate));

list($start_year, $start_month, $start_day) = explode("-", $textFromDate);
list($end_year, $end_month, $end_day) = explode("-", $textToDate);
$date_range = "$start_year 年  $start_month 月 $start_day 日 - $end_year 年  $end_month 月 $end_day 日";

# Semester
$semester = (array)getSemesters($selectYear);
$semester = array_keys($semester);

# Rank Target
$classIdCond = " AND yc.YearClassID IN ('".implode("','", (array)$_POST['rankTargetDetail'])."') ";

# Style
if($format == 'pdf')
{
	$StylePrefix = '<span class="tabletextrequire">';
	$StyleSuffix = '</span>';
}

# Get Target Students
$clsName = "yc.ClassTitleEN";	
$sql = "SELECT 
			iu.UserID,
			yc.YearClassID,
			yc.ClassTitleEN as ClassName,
			ycu.ClassNumber,
			CONCAT(
				IF($clsName IS NULL OR $clsName='' OR ycu.ClassNumber IS NULL OR ycu.ClassNumber='' Or iu.RecordStatus != 1, '".$StylePrefix."^".$StyleSuffix."', ''),
				iu.ChineseName
			) as StudentName
		FROM INTRANET_USER as iu 
		LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID = iu.UserID
		LEFT JOIN YEAR_CLASS as yc ON (yc.YearClassID = ycu.YearClassID AND yc.AcademicYearID = '$selectYear')
		LEFT JOIN YEAR as y ON y.YearID = yc.YearID
		WHERE yc.AcademicYearID = '".$selectYear."' $classIdCond 
		GROUP BY iu.UserID 
		ORDER BY y.Sequence, yc.Sequence, ycu.ClassNumber";
$students = $ldiscipline->returnResultSet($sql);
$student_ids = Get_Array_By_Key($students, "UserID");
$student_ary = BuildMultiKeyAssoc((array)$students, array("YearClassID", "UserID"));
$student_count = count($students);

# Good Conduct / Misconduct - Approved
$sql = "SELECT
			r.RecordID,
			r.StudentID,
			cat.CategoryID,
			cat.Name,
			r.RecordType,
			DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
			r.YearTermID,
			SUM(1) AS GMRecordCount
		FROM 
			DISCIPLINE_ACCU_RECORD as r
		INNER JOIN 
			DISCIPLINE_ACCU_CATEGORY as cat ON cat.CategoryID = r.CategoryID
		WHERE 
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND r.AcademicYearID = '$selectYear' AND 
 			r.RecordDate >= '$textFromDate 00:00:00' AND r.RecordDate <= '$textToDate 23:59:59' AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'
		GROUP BY 
			r.StudentID, r.RecordType, r.CategoryID, r.YearTermID";
$gm_records = $ldiscipline->returnResultSet($sql);
$gm_records = BuildMultiKeyAssoc((array)$gm_records, array("StudentID", "CategoryID", "YearTermID"));

# Awards / Punishment - Approved and Released
$sql = "SELECT 
			r.RecordID,
			r.StudentID,
			m.ItemCode,
			m.ItemName,
			r.ProfileMeritType,
			DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
			r.YearTermID,
			SUM(ROUND(r.ProfileMeritCount)) AS MeritRecordCount
		FROM 
			DISCIPLINE_MERIT_RECORD as r
		INNER JOIN 
			DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
		WHERE 
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND r.AcademicYearID = '$selectYear' AND 
			r.RecordDate >= '$textFromDate' AND r.RecordDate <= '$textToDate' AND 
			r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'
		GROUP BY 
			r.StudentID, r.ProfileMeritType, r.YearTermID";
$ap_records = $ldiscipline->returnResultSet($sql);
$ap_records = BuildMultiKeyAssoc((array)$ap_records, array("StudentID", "ProfileMeritType", "YearTermID"));

# Awards / Punishment - Waived and Redeemed
// $sql = "SELECT 
// 			r.RecordID,
// 			r.StudentID,
// 			m.ItemCode,
// 			m.ItemName,
// 			r.ProfileMeritType,
// 			DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
// 			r.YearTermID,
// 			SUM(ROUND(r.ProfileMeritCount)) AS MeritRecordCount
// 		FROM 
// 			DISCIPLINE_MERIT_RECORD as r
// 		INNER JOIN 
// 			DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
// 		WHERE 
// 			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND r.AcademicYearID = '$selectYear' AND 
// 			r.RecordDate >= '$textFromDate' AND r.RecordDate <= '$textToDate' AND 
// 			r.RecordStatus = '".DISCIPLINE_STATUS_WAIVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."'
// 		GROUP BY 
// 			r.StudentID, r.ProfileMeritType, r.YearTermID";
// $ap_waived_records = $ldiscipline->returnResultSet($sql);
// $ap_waived_records = BuildMultiKeyAssoc((array)$ap_waived_records, array("StudentID","ProfileMeritType","YearTermID"));

# Awards / Punishment - Joined Rainbow Scheme
$sql = "SELECT
			r.RecordID,
			r.StudentID,
			m.ItemCode,
			m.ItemName,
			(r.ProfileMeritType - 1) as RSMeritType,
			DATE_FORMAT(r.RecordDate,'%d/%m/%Y') as RecordDate,
			r.YearTermID,
			SUM(ROUND(r.ProfileMeritCount)) AS MeritRecordCount
		FROM
			DISCIPLINE_MERIT_RECORD as r
		INNER JOIN
			DISCIPLINE_MERIT_ITEM as m ON m.ItemID = r.ItemID
		WHERE
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND r.AcademicYearID = '$selectYear' AND
			r.RecordDate >= '$textFromDate' AND r.RecordDate <= '$textToDate' AND
			r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."' AND r.ReleaseStatus = '".DISCIPLINE_STATUS_RELEASED."' AND
			(r.ProfileMeritType = -2 OR r.ProfileMeritType = -3) AND r.RehabilStatus = '1'
		GROUP BY
			r.StudentID, RSMeritType, r.YearTermID";
$ap_rb_scheme_records = $ldiscipline->returnResultSet($sql);
$ap_rb_scheme_records = BuildMultiKeyAssoc((array)$ap_rb_scheme_records, array("StudentID", "RSMeritType", "YearTermID"));

# Detention - Set Session
$sql = "SELECT 
			r.RecordID,
			r.StudentID,
			DATE_FORMAT(r.ArrangedDate,'%d/%m/%Y') as RecordDate,
			SUM(1) AS DetentionTypeRecordCount
		FROM 
			DISCIPLINE_DETENTION_STUDENT_SESSION as r
		WHERE 
			r.StudentID IN ('".implode("','", (array)$student_ids)."') AND 
			r.ArrangedDate >= '$yearFromDate 00:00:00' AND r.ArrangedDate <= '$yearToDate 23:59:59' AND 
			r.ArrangedDate >= '$textFromDate 00:00:00' AND r.ArrangedDate <= '$textToDate 23:59:59' AND 
			r.DetentionID IS NOT NULL AND r.RecordStatus = '".DISCIPLINE_STATUS_APPROVED."'
		GROUP BY
			r.StudentID";
$detention_records = $ldiscipline->returnResultSet($sql);
$detention_records = BuildMultiKeyAssoc((array)$detention_records, array("StudentID"));
	
# Report Format - CSV
if($format == 'csv')
{
	# Initiate Export Object
	include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	$lexport = new libexporttext();
	$exportContent = '';
	
	# CSV Header
	$header = array("田家炳中學", "輔委會學生校內生活紀錄總表(".getAYNameByAyId($selectYear,"b5").")", "$date_range");
	$rows = array();
	
	$level_num = count((array)$_POST['rankTargetDetail']);
	if($level_num)
	{
		foreach((array)$_POST['rankTargetDetail'] as $yearclass_id)
		{
		    $rows = $ldiscipline_ui->getTKPSchoolRecordSummary($format, $yearclass_id, $student_ary, $semester, $gm_records, $ap_records, $ap_rb_scheme_records, $detention_records, false, $rows);
		}
	}
	
	$exportContent .= $lexport->GET_EXPORT_TXT($rows, $header);
	$lexport->EXPORT_FILE("StudentRecordTable.csv", $exportContent);
}
# Report Format - PDF
else if($format == 'pdf')
{
	# Initiate mPDF Object
	require_once($PATH_WRT_ROOT."includes/mpdf/mpdf.php");
	$pdf_obj = "";
		
	// Create mPDF object
	$pdf_obj = new mPDF('','A4',0,'',10,10,10,10,5,0);
	
	// mPDF Setting
	$pdf_obj->setAutoTopMargin = 'stretch';
	//split 1 table into 2 pages add border at the bottom
	$pdf_obj->splitTableBorderWidth = 0.1; 

	# Report Header
	$html = "<table class='report_header' cellspacing='0' cellpadding='0' border='0' width='100%'>";
		$html .= "<tr><td align='center'>田家炳中學</td></tr>";
		$html .= "<tr><td align='center'>輔委會學生校內生活紀錄總表(".getAYNameByAyId($selectYear,"b5").")</td></tr>";
		$html .= "<tr><td align='right' style='font-size: 8.5pt;'>$date_range</td></tr>";
	$html .= "</table>";
	$pdf_obj->DefHTMLHeaderByName("titleHeader", $html);
	$pdf_obj->SetHTMLHeaderByName("titleHeader");
	
	# CSS
	$pdf_obj->writeHTML("<head>");
	$html = "<style TYPE='text/css'>
				body 
				{
					font-family: msjh; 
					font-size: 8.5pt; 
				}
				.report_header
				{
					font-size: 11pt;	
				}
				.result_table 
				{
					border:0.1mm solid black; 
				}
				table.result_table th, table.result_table td 
				{
					border: 0.1mm solid black;
					border-right: 0mm solid black;
					text-align: center;
					padding: 1px;
				}
				table.result_table th
				{
					border-top: 0mm solid black;
					font-weight: normal;
				}
			</style>";
	$pdf_obj->writeHTML($html);
	$pdf_obj->writeHTML("</head>");
	
	$level_count = 0;
	$level_num = count((array)$_POST['rankTargetDetail']);
	
	# Report Table
	$pdf_obj->writeHTML("<body>");
	if($level_num)
	{
		foreach((array)$_POST['rankTargetDetail'] as $yearclass_id)
		{
		    $html = $ldiscipline_ui->getTKPSchoolRecordSummary($format, $yearclass_id, $student_ary, $semester, $gm_records, $ap_records, $ap_rb_scheme_records, $detention_records, ($level_count==$level_num-1));
			$pdf_obj->writeHTML($html);
			
			$level_count++;
		}
	}
	$pdf_obj->writeHTML("</body>");
	
	# Export PDF file
	$pdf_obj->Output('StudentRecordTable.pdf', 'I');
} 
else 
{
	echo "Not support";
}

intranet_closedb();
?>
