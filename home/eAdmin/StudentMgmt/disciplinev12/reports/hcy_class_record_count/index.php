<?php
// Using: Bill

############# Change Log [Start] ################
#
#   Date:   2018-12-18 (Bill)   [2018-0627-1537-01277]
#           Create file
#
############# Change Log [End] ################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Access right checking
$teachingClasses = $ldiscipline->getTeachingClassesByTeacherID();
if(!$sys_custom['eDiscipline']['HCY_ClassRecordCountReport'] || (!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && empty($teachingClasses))) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$linterface = new interface_html();

# Current Page
$CurrentPage = "Reports_HCY_ClassRecordCountReport";
if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
    $CurrentPageArr['eDisciplinev12'] = 1;
} else {
    $CurrentPageArr['eServiceeDisciplinev12'] = 1;
}

# Tab
$TAGS_OBJ[] = array($Lang['eDiscipline']['HCY_ClassRecordCountReport']['Title'], "", 1);
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Year - Date Range
$selectYear = $selectYear == ''? Get_Current_Academic_Year_ID() : $selectYear;
$textFromDate = date("Y-m-d", strtotime(getStartDateOfAcademicYear($selectYear, '')));
$textToDate = date("Y-m-d", strtotime(getEndDateOfAcademicYear($selectYear, '')));

# Year Selection
$year_ary = $ldiscipline->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($year_ary, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\");' ", "", $selectYear);

$linterface->LAYOUT_START();
?>

<script type="text/javascript" language="JavaScript">
function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
	
	showRankTargetResult($('#rankTarget').val());
}

// function changeYearTermDateRange()
// {
// 	$.post(
// 		"../ajaxGetYearTermDateRange.php",
// 		{
// 			"AcademicYearID": $('#selectYear').val()
// 		},
// 		function(returnData)
// 		{
// 			var obj = JSON.parse(returnData);
// 			$('#textFromDate').val(obj['StartDate']);
// 			$('#textToDate').val(obj['EndDate']);
// 		}
// 	);
// }

function showRankTargetResult(str)
{
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}

	var targetYearId = $('#selectYear').val();
	if(document.getElementById("radioPeriod_Date").checked) {
		targetYearId = '<?=Get_Current_Academic_Year_ID()?>';
	}
	
	var url = "../ajaxGetLive.php";
	url += "?target=" + str;
	url += "&fieldId=rankTargetDetail[]&fieldName=rankTargetDetail[]";
	url += "&academicYearId="+targetYearId;
	<?php if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) { ?>
		url += "&teachingClassOnly=1";
	<?php } ?>
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "rankTargetDetail";
			$('#'+showIn).html(responseText);
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	
	var from_date = $.trim($('#textFromDate').val());
	var to_date = $.trim($('#textToDate').val());
	if(from_date == '' || to_date == '') {
		valid = false;
		$('#textFromDate').focus();
	}
	
	if(valid)
	{
		if(!check_date_without_return_msg(document.getElementById('textFromDate'))) {
			valid = false;
			$('#textFromDate').focus();
		}
		if(!check_date_without_return_msg(document.getElementById('textToDate'))) {
			valid = false;
			$('#textToDate').focus();
		}
	}
	if(valid)
	{
		if(from_date > to_date) {
			valid = false;
			$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
			$('#DateWarnDiv').show();
			$('#textFromDate').focus();
		}
	}
	if(valid)
	{
		$('#DateWarnDiv span').html('');
		$('#DateWarnDiv').hide();
	}
	
	if($('select#rankTargetDetail\\[\\] option:selected').length == 0) {
		valid = false;
		$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
		$('#TargetWarnDiv').show();
	}
	else {
		$('#TargetWarnDiv').hide();
	}
	if(!valid) return;
	
	$('input#format').val(format);
// 	if(format == 'web')
// 	{
// 		form_obj.attr('target','');
// 		Block_Element('PageDiv');
		
// 		$.post(
// 			'report.php',
// 			$('#form1').serialize(),
// 			function(data){
// 				$('#ReportDiv').html(data);
// 				document.getElementById('div_form').className = 'report_option report_hide_option';
// 				$('.spanShowOption').show();
// 				$('.spanHideOption').hide();
// 				$('.Form_Span').hide();
// 				UnBlock_Element('PageDiv');
// 			}
// 		);
// 	}else
	if(format == 'csv') {
		form_obj.attr('target','');
		form_obj.submit();
	}
	else if(format == 'print') {
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

function SelectAll(obj)
{
	for (i=0; i<obj.length; i++) {
		obj.options[i].selected = true;
	}
}

$(document).ready(function() {
	showRankTargetResult($("#rankTarget").val());
});
</script>

<div id="PageDiv">

<div id="div_form">
	<p class="spacer"></p> 
	
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php">
		<table class="form_table_v30">
			<!-- Year -->
			<!-- 
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HCY_ClassRecordCountReport']['SchoolYear']?></td>
				<td><table class="inside_form_table">
						<tr>
							<td colspan="6""><?=$selectSchoolYearHTML?></td>
						</tr>
				</table></td>
			</tr>
			-->
			
			<!-- Date Range -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HCY_ClassRecordCountReport']['Period']?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td colspan="4">
								<input type="radio" name="radioPeriod" id="radioPeriod_Year" value="YEAR" onclick="changeRadioSelection('YEAR');" checked>
								<label for="radioPeriod_Year"><?=$Lang['eDiscipline']['HCY_ClassRecordCountReport']['SchoolYear']?></label>
								<span id="spanSemester"><?=$selectSchoolYearHTML?></span>
							</td>
						</tr>
						<tr>
							<td>
								<input type="radio" name="radioPeriod" id="radioPeriod_Date" value="DATE" onclick="changeRadioSelection('DATE');">
								<label for="radioPeriod_Date"><?=$i_From?></label>
							</td>
							<td onclick="changeRadioSelection('DATE')" onfocus="changeRadioSelection('DATE')">
								<?=$linterface->GET_DATE_PICKER("textFromDate", $textFromDate, "")?>
							</td>
							<td>
								<label for="radioPeriod_Date"><?=$i_To?></label>
							</td>
							<td onclick="changeRadioSelection('DATE')" onfocus="changeRadioSelection('DATE')">
								<?=$linterface->GET_DATE_PICKER("textToDate", $textToDate, "")?>
								<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eDiscipline']['HCY_ClassRecordCountReport']['Target']?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td valign="top">
								<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value)">
									<?php if($ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) { ?>
										<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
									<?php } ?>
									<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
								</select>
							</td>
							<td valign="top" nowrap>
								<span id='rankTargetDetail'>
									<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="5"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
							</td>
						</tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang['eDiscipline']['HCY_ClassRecordCountReport']['PressCtrlKey']?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "submitForm('print');")?>
			&nbsp;
			<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Export'], "button", "submitForm('csv');")?>
		</div>
		
		<input type="hidden" name="format" id="format" value="web">
		</form>
	</span>
</div>

<div id="ReportDiv"></div>
</div>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>