<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lclass = new libclass();

if($single!=1) {
	$tag = "MULTIPLE SIZE=\"5\"";	
}

if($withWholeForm==1) {
	
	$select_list = $ldiscipline->getSelectClassWithWholeForm("name=\"target[]\" id=\"target[]\" $tag", $target, $firstValue="", $pleaseSelect="", Get_Current_Academic_Year_ID(),1);
	
	
} else {
	$levels = $lclass->getLevelArray();
	$classes = $ldiscipline->getRankClassList("",$year);
	
	if($selectedTarget!="" && !isset($target))
		$target = explode(',',$selectedTarget);
	
	
	$select_list = "<SELECT name=\"target[]\" id=\"target[]\" $tag>";
	
	if ($level==1)
	{
	    for ($i=0; $i<sizeof($levels); $i++)
	    {
	         list($id,$name) = $levels[$i];
	
	         $selected_tag = (is_array($target) && in_array($id,$target) )?"SELECTED":"";
	         $select_list .= "<OPTION value='".$id."' $selected_tag>".$name."</OPTION>";
	    }
	}
	else
	{
	    for ($i=0; $i<sizeof($classes); $i++)
	    {
	         //list($id, $name, $lvl) = $classes[$i];
	         list($name, $id) = $classes[$i];
	
			if($single!=1 || count($target)>0) {
	        	$selected_tag = ((is_array($target) && in_array($id,$target)) || $selectedTarget=="")?"SELECTED":"";
			}
	         $select_list .= "<OPTION value='".$id."' $selected_tag>". $name."</OPTION>";
	    }
	}
	$select_list .= "</SELECT>";
}

if(sizeof($$target)==0) {
	$select_list .= "\n<script language='javascript'>\nSelectAll(document.getElementById('target[]'));\n</script>";	
}
intranet_closedb();

echo $select_list;
?>