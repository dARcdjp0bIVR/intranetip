<?php
# using: henry

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"] && sizeof($_SESSION['SESSION_ACCESS_RIGHT'])==0) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();


$yearArr = $ldiscipline->returnAllYearsSelectionArray();

$SchoolYear = ($SchoolYear=="") ? Get_Current_Academic_Year_ID() : $SchoolYear;
$yearSelect = $linterface->GET_SELECTION_BOX($yearArr, 'id="SchoolYear" name="SchoolYear" onChange="goURL(this.value)"', "", $SchoolYear);

$TAGS_OBJ[] = array($eDiscipline['Overview'], "");

# menu highlight setting
$CurrentPage = "Overview";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$i = 1;

if($ldiscipline->use_newleaf)
	$nl = ($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) ? $ldiscipline->specialNewLeafCount($SchoolYear,"", 1) : $ldiscipline->GMWaitForNewLeafCount($SchoolYear);


?>
<script language="javascript">
function goURL(url) {
	self.location.href = "index.php?SchoolYear=" + url;	
}
</script>

<?
# Bubble Message (display eClass Update message)

if($_SESSION['Is_First_Login']) 		# only display the bubble when first login
{
	
	//intranet_auth();
	//intranet_opendb();

	$libdb = new libdb();
	
	$version = Get_IP_Version();	

	$display = displayBubbleMessage($version, $UserID);
	
	
	if($display) {			# display the bubble if user did not read it / choose "remind me later" before
		# get admin user
		$sql = "SELECT * FROM GENERAL_SETTING where Module='SchoolSettings'";
		$result = $libdb->returnArray($sql);
		$AdminUser = $result[0]['SettingValue'];
		$AdminUserAry = explode(',',$AdminUser);
		
		$url = "";
		$bubble = "";
		
		if(in_array($UserID, $AdminUserAry)) {	# system admin
			if($intranet_session_language=="en")
				$url = "http://support.broadlearning.com/doc/help/central/BubbleNews.html#CurrentNewsAdminEN";
			else 
				$url = "http://support.broadlearning.com/doc/help/central/BubbleNews.html#CurrentNewsAdminHK";	
		} else if($_SESSION['UserType']==USERTYPE_STAFF) {	# teacher
			if($intranet_session_language=="en")
				$url = "http://support.broadlearning.com/doc/help/central/BubbleNews.html#CurrentNewsUserEN";
			else
				$url = "http://support.broadlearning.com/doc/help/central/BubbleNews.html#CurrentNewsUserHK";
		}
		
		if($url!="") {			# only system admin / teacher can see this bubble
			$bubble = '
				<script language="javascript">
					function hideSpan(layername) {
						document.getElementById(layername).style.visibility = "hidden";
					}

					function goCancel() {
						
						cancelAjax = GetXmlHttpObject();
						   
						if (cancelAjax == null)
						{
							alert (errAjax);
							return;
						} 
					
						var url = \'ajax_cancel_bubble.php\';
						var PostValue = "version='.$version.'";
						cancelAjax.onreadystatechange = function() {
							if (cancelAjax.readyState == 4) {
								//alert(cancelAjax.responseText);
							}
							
						};
						cancelAjax.open("POST", url, true);
						cancelAjax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
						cancelAjax.send(PostValue);

						hideSpan(\'sub_layer_eclass_update_alert_v30\');
					}
					
					function GetXmlHttpObject()
					{
						var xmlHttp = null;
						try
						{
							// Firefox, Opera 8.0+, Safari
							xmlHttp = new XMLHttpRequest();
						}
						catch (e)
						{
							// Internet Explorer
							try
							{
								xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
							}
							catch (e)
							{
								xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
							}
						}
						return xmlHttp;
					}

				</script>
			';
			
			$bubble .= '
				<div class="sub_layer_board_v30" id="sub_layer_eclass_update_alert_v30" style="visibility: visible; z-index:9999999999;top:55px;left:15px;">
					<span class="bubble_board_01_v30">
						<span class="bubble_board_02_v30">
							<em><img src="/images/'.$LAYOUT_SKIN.'/addon_tools/sub_layer_arrow.png" width="16" height="12"></em>
						</span>
					</span>
					<span class="bubble_board_03_v30">
						<span class="bubble_board_04_v30">
							<iframe width="100%" height="280" frameborder="0" src="'.$url.'"></iframe>
							<p>
							<div class="edit_bottom">
								<input type="button" class="formsmallbutton" onClick="javascript:goCancel();" name="closeBtn" id="closeBtn" value="'.$Lang['General']['Close'].'"   onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>
								
								<input type="button" class="formsmallbutton" onClick="javascript:hideSpan(\'sub_layer_eclass_update_alert_v30\')" name="remindBtn" id="remindBtn" value="'.$Lang['General']['RemindMeLater'].'"   onMouseOver="this.className=\'formsmallbuttonon\'" onMouseOut="this.className=\'formsmallbutton\'"/>
							</div>
						</span>
					</span>
					<span class="bubble_board_05_v30"><span class="bubble_board_06_v30"></span></span>									
				</div>
			';

			echo $bubble;	
		}		
	}	
	
	//intranet_closedb();
	
}

?>

<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						<?=$linterface->GET_NAVIGATION2($i_Discipline_Pending_Event);?>
					</td>
				</tr>
				<tr>
					<td align="left" class="sectiontitle">
						<?= $yearSelect?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="50" align="right" class="tabletextremark">
									<table width="100%" border="0" cellspacing="0" cellpadding="4">
										<tr class="tabletop">
											<td><?=$i_Discipline_Event_Type?></td>
											<td><?=$i_Discipline_Status?></td>
											<td><?=$i_Discipline_No_of_Records?></td>
											<td width="25"><?=$button_view?></td>
										</tr>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")) { ?>
										<? if($ldiscipline->awardPunishmentRecordCount("RecordStatus",DISCIPLINE_STATUS_PENDING, $SchoolYear)>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$eDiscipline['Award_and_Punishment']?></td>
											<td class="tabletext"><?=$i_Discipline_System_Award_Punishment_Pending?></td>
											<td class="tabletext"><?= $ldiscipline->awardPunishmentRecordCount("RecordStatus",DISCIPLINE_STATUS_PENDING, $SchoolYear)?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="waitApproval" id="waitApproval" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($ldiscipline->awardPunishmentRecordCount("ReleaseStatus",DISCIPLINE_STATUS_APPROVED, $SchoolYear)>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><? if($ldiscipline->awardPunishmentRecordCount("RecordStatus",DISCIPLINE_STATUS_PENDING, $SchoolYear)==0) { echo $eDiscipline['Award_and_Punishment'];} ?></td>
											<td class="tabletext"><?=$i_Discipline_System_Award_Punishment_Wait_For_Release?></td>
											<td class="tabletext"><?= $ldiscipline->awardPunishmentRecordCount("ReleaseStatus",DISCIPLINE_STATUS_APPROVED, $SchoolYear)?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="unreleased" id="unreleased" value="<?=DISCIPLINE_STATUS_UNRELEASED?>"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($plugin['ppc_disciplinev12'] && sizeof($ldiscipline->retrieveMeritRecordFromPPC($SchoolYear))>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td >&nbsp;</td>
											<td class="tabletext"><?=$i_Discipline_System_Insert_From_PPC?></td>
											<td class="tabletext"><?= sizeof($ldiscipline->retrieveMeritRecordFromPPC($SchoolYear))?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="fromPPC" id="fromPPC" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($sys_custom['wscss_disciplinev12_ap_prompt_action'] && sizeof($ldiscipline->getPassedActionDueDateAPRecord($SchoolYear))>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td >&nbsp;</td>
											<td class="tabletext"><?=$Lang['eDiscipline']['PassedActionDueDate']?></td>
											<td class="tabletext"><?=sizeof($ldiscipline->getPassedActionDueDateAPRecord($SchoolYear))?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($ldiscipline->awardPunishmentRecordCount("RecordStatus",DISCIPLINE_STATUS_PENDING, $SchoolYear)>0 || $ldiscipline->awardPunishmentRecordCount("ReleaseStatus",DISCIPLINE_STATUS_APPROVED)>0 || (($plugin['ppc_disciplinev12'] && sizeof($ldiscipline->retrieveMeritRecordFromPPC())>0))) $i++; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View")) { ?>
										<? $showGM = 0; ?>
										<? if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") && sizeof($nl)>0) { ?>
										<form action="../management/goodconduct_misconduct/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$eDiscipline['Good_Conduct_and_Misconduct']?></td>
											<td class="tabletext"><?=$i_Discipline_System_GoodConduct_Misconduct_Wait_For_NewLeaf?></td>
											<td class="tabletext"><? if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) { echo $i_general_yes; } else { echo sizeof($ldiscipline->GMWaitForNewLeafCount($SchoolYear));} ?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="newleaf" id="newleaf" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $showGM = 1; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View") && sizeof($ldiscipline->retrieveGMRecordFromPPC($SchoolYear))>0) { ?>
										<? if($plugin['ppc_disciplinev12'] && sizeof($ldiscipline->retrieveGMRecordFromPPC($SchoolYear))>0) { ?>
										<form action="../management/goodconduct_misconduct/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><? if(!$ldiscipline->use_newleaf || !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") || sizeof($nl)==0) { echo $eDiscipline['Good_Conduct_and_Misconduct'];} ?></td>
											<td class="tabletext"><?=$i_Discipline_System_Insert_From_PPC?></td>
											<td class="tabletext"><?=sizeof($ldiscipline->retrieveGMRecordFromPPC($SchoolYear))?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="fromPPC" id="fromPPC" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $showGM = 1; }} if($showGM==1) $i++; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-View") && $ldiscipline->caseRecordProcessingCount(DISCIPLINE_CASE_RECORD_PROCESSING, $SchoolYear)>0) { ?>
										<form action="../management/case_record/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$i_Discipline_System_Discipline_Case_Record_Case?></td>
											<td class="tabletext"><?=$i_Discipline_System_Discipline_Case_Record_Processing?></td>
											<td class="tabletext"><?= $ldiscipline->caseRecordProcessingCount(DISCIPLINE_CASE_RECORD_PROCESSING, $SchoolYear) ?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="Processing" id="Processing" value="on"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $i++; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) { ?>
										<? if($ldiscipline->detentionWaitForArrangeCount($SchoolYear)>0) { ?>
										<form action="../management/detention/student_record.php" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$eDiscipline['Detention']?></td>
											<td class="tabletext"><?=$i_Discipline_Waiting_For_Arrangement?></td>
											<td class="tabletext"><?=$ldiscipline->detentionWaitForArrangeCount($SchoolYear)?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="targetStudent" id="targetStudent" value="Not_Assigned"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $i++; } ?>
										<? if($ldiscipline->detentionAbsentCount($SchoolYear)>0) { ?>
										<form action="../management/detention/student_record.php" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><? if($ldiscipline->detentionWaitForArrangeCount($SchoolYear)==0) { echo $eDiscipline['Detention']; }?></td>
											<td class="tabletext"><?=$i_Discipline_Absent?></td>
											<td class="tabletext"><?= $ldiscipline->detentionAbsentCount($SchoolYear)?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" width="20" height="20" border="0"><input type="hidden" name="targetStatus" id="targetStatus" value="Absent"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $i++; } ?>
										<? } ?>
										
										<? if($i==1) {?>
										<tr>
										<td align="center" class="tablerow2 tabletext" colspan="4"><br><?=$i_no_record_exists_msg?><br><br></td>
									</tr>
									<? } ?>
				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				
				
				<tr>
					<td align="right" class="tablebottom">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
