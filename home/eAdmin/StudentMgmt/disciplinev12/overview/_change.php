<?
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Award & Punishment Record (eDisciplinev12)
$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_MERIT_RECORD');

if(!is_numeric($leftRecord)) {		# already transferred
	echo "eDisciplinev12...Award & Punishment Records already transferred<br>";
}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
	echo "eDisciplinev12...Transfer Award & Punishment Records to IP25...Done<br>";
} else {							# all Award & Punishment data transfer to IP25 successfully
	echo "eDisciplinev12...Transfer Award & Punishment Records to IP25...$leftRecord record(s) cannot be transferred to IP25<br>";
}

# Good Conduct & Misconduct Record (eDisciplinev12)
$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_ACCU_RECORD');

if(!is_numeric($leftRecord)) {		# already transferred
	echo "eDisciplinev12...Good Conduct & Misconduct Records already transferred<br>";
}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
	echo "eDisciplinev12...Transfer Good Conduct & Misconduct Records to IP25...Done<br>";
} else {							# all Award & Punishment data transfer to IP25 successfully
	echo "eDisciplinev12...Transfer Good Conduct & Misconduct Records to IP25...$leftRecord record(s) cannot be transferred to IP25<br>";
}

# Case Record (eDisciplinev12)
$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_CASE');

if(!is_numeric($leftRecord)) {		# already transferred
	echo "eDisciplinev12...Case Records already transferred<br>";
}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
	echo "eDisciplinev12...Transfer Case Records to IP25...Done<br>";
} else {							# all Award & Punishment data transfer to IP25 successfully
	echo "eDisciplinev12...Transfer Case Records to IP25...$leftRecord record(s) cannot be transferred to IP25<br>";
}

intranet_closedb();
?>