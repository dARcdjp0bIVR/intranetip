<?php
# using: 

##### Change Log [Start] #####
#
#	Date	:	2015-11-04 (Bill)	[2015-0416-1040-06164]
#				Redirect to AP Page for Student Prefect - HKUGA Cust
#	
#	Date	:	2015-09-11 (Bill)	[2015-0909-1241-20222]
#				seperate record count from html - improve performance
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"] && sizeof($_SESSION['SESSION_ACCESS_RIGHT'])==0) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

// if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View") && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-View") && !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
// 	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
// }

// [2015-0416-1040-06164] Student Prefect access will be redirected to AP page
if($sys_custom['eDiscipline']['add_AP_records_student_prefect'] && $ldiscipline->isStudentPrefect()){
	header("Location: ../management/award_punishment/?clearCoo=1");
	exit();
}

$yearArr = $ldiscipline->returnAllYearsSelectionArray();

$SchoolYear = ($SchoolYear=="") ? Get_Current_Academic_Year_ID() : $SchoolYear;
$yearSelect = $linterface->GET_SELECTION_BOX($yearArr, 'id="SchoolYear" name="SchoolYear" onChange="goURL(this.value)"', "", $SchoolYear);

$TAGS_OBJ[] = array($eDiscipline['Overview'], "");

# menu highlight setting
$CurrentPage = "Overview";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$i = 1;

if($ldiscipline->use_newleaf){
	$nl = ($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) ? $ldiscipline->specialNewLeafCount($SchoolYear,"", 1) : $ldiscipline->GMWaitForNewLeafCount($SchoolYear);
	$nl_count = sizeof($nl);
}

// [2015-0909-1241-20222] seperate record count from html - improve performance
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")) {
	$AP_pending_count = $ldiscipline->awardPunishmentRecordCount("RecordStatus",DISCIPLINE_STATUS_PENDING, $SchoolYear);
	$AP_release_count = $ldiscipline->awardPunishmentRecordCount("ReleaseStatus",DISCIPLINE_STATUS_APPROVED, $SchoolYear);
//	$all_AP_release_count = $ldiscipline->awardPunishmentRecordCount("ReleaseStatus",DISCIPLINE_STATUS_APPROVED);
	
	if($plugin['ppc_disciplinev12']){
		$AP_PPC_MeritCount = sizeof($ldiscipline->retrieveMeritRecordFromPPC($SchoolYear));
//		$all_AP_PPC_MeritCount = sizeof($ldiscipline->retrieveMeritRecordFromPPC());
	}
	
	if($sys_custom['wscss_disciplinev12_ap_prompt_action']){
		$AP_WSCSS_passedRecord = sizeof($ldiscipline->getPassedActionDueDateAPRecord($SchoolYear));
	}
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View") && $plugin['ppc_disciplinev12']){
	$PPC_GMCount = sizeof($ldiscipline->retrieveGMRecordFromPPC($SchoolYear));
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-View")){
	$CaseRecordCount = $ldiscipline->caseRecordProcessingCount(DISCIPLINE_CASE_RECORD_PROCESSING, $SchoolYear);
}
if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) {
	$detention_waiting_count = $ldiscipline->detentionWaitForArrangeCount($SchoolYear);
	$detention_absent_count = $ldiscipline->detentionAbsentCount($SchoolYear);
}

?>
<script language="javascript">
function goURL(url) {
	self.location.href = "index.php?SchoolYear=" + url;	
}
</script>
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="left" class="sectiontitle">
						<?=$linterface->GET_NAVIGATION2($i_Discipline_Pending_Event);?>
					</td>
				</tr>
				<tr>
					<td align="left" class="sectiontitle">
						<?= $yearSelect?>
					</td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="50" align="right" class="tabletextremark">
									<table width="100%" border="0" cellspacing="0" cellpadding="4">
										<tr class="tabletop">
											<td><?=$i_Discipline_Event_Type?></td>
											<td><?=$i_Discipline_Status?></td>
											<td><?=$i_Discipline_No_of_Records?></td>
											<td width="35"><?=$button_view?></td>
										</tr>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Award_Punishment-View")) { ?>
										<? if($AP_pending_count>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$eDiscipline['Award_and_Punishment']?></td>
											<td class="tabletext"><?=$i_Discipline_System_Award_Punishment_Pending?></td>
											<td class="tabletext"><?= $AP_pending_count?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="waitApproval" id="waitApproval" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($AP_release_count>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><? if($AP_pending_count==0) { echo $eDiscipline['Award_and_Punishment'];} ?></td>
											<td class="tabletext"><?=$i_Discipline_System_Award_Punishment_Wait_For_Release?></td>
											<td class="tabletext"><?= $AP_release_count?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="unreleased" id="unreleased" value="<?=DISCIPLINE_STATUS_UNRELEASED?>"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($plugin['ppc_disciplinev12'] && $AP_PPC_MeritCount>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td >&nbsp;</td>
											<td class="tabletext"><?=$i_Discipline_System_Insert_From_PPC?></td>
											<td class="tabletext"><?= $AP_PPC_MeritCount?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="fromPPC" id="fromPPC" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($sys_custom['wscss_disciplinev12_ap_prompt_action'] && $AP_WSCSS_passedRecord>0) { ?>
										<form action="../management/award_punishment/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td >&nbsp;</td>
											<td class="tabletext"><?=$Lang['eDiscipline']['PassedActionDueDate']?></td>
											<td class="tabletext"><?=$AP_WSCSS_passedRecord?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="passedActionDueDate" id="passedActionDueDate" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? } ?>
										<? if($AP_pending_count>0 || $AP_release_count>0 || (($plugin['ppc_disciplinev12'] && $AP_PPC_MeritCount>0))) $i++; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View")) { ?>
										<? $showGM = 0; ?>
										<? if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") && $nl_count>0) { ?>
										<form action="../management/goodconduct_misconduct/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$eDiscipline['Good_Conduct_and_Misconduct']?></td>
											<td class="tabletext"><?=$i_Discipline_System_GoodConduct_Misconduct_Wait_For_NewLeaf?></td>
											<td class="tabletext"><? if($sys_custom['eDisciplinev12_Munsang_NewLeafScheme']) { echo $i_general_yes; } else { echo $nl_count;} ?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="newleaf" id="newleaf" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $showGM = 1; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-View") && $PPC_GMCount>0) { ?>
										<? if($plugin['ppc_disciplinev12'] && $PPC_GMCount>0) { ?>
										<form action="../management/goodconduct_misconduct/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><? if(!$ldiscipline->use_newleaf || !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") || $nl_count==0) { echo $eDiscipline['Good_Conduct_and_Misconduct'];} ?></td>
											<td class="tabletext"><?=$i_Discipline_System_Insert_From_PPC?></td>
											<td class="tabletext"><?=$PPC_GMCount?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="fromPPC" id="fromPPC" value="1"><input type="hidden" name="fromOverview" id="fromOverview" value="1"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $showGM = 1; }} if($showGM==1) $i++; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Case_Record-View") && $CaseRecordCount>0) { ?>
										<form action="../management/case_record/" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$i_Discipline_System_Discipline_Case_Record_Case?></td>
											<td class="tabletext"><?=$i_Discipline_System_Discipline_Case_Record_Processing?></td>
											<td class="tabletext"><?= $CaseRecordCount ?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="Processing" id="Processing" value="on"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $i++; } ?>
										<? if($ldiscipline->CHECK_ACCESS("Discipline-MGMT-Detention-View")) { ?>
										<? if($detention_waiting_count>0) { ?>
										<form action="../management/detention/student_record.php" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><?=$eDiscipline['Detention']?></td>
											<td class="tabletext"><?=$i_Discipline_Waiting_For_Arrangement?></td>
											<td class="tabletext"><?=$detention_waiting_count?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="targetStudent" id="targetStudent" value="Not_Assigned"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $i++; } ?>
										<? if($detention_absent_count>0) { ?>
										<form action="../management/detention/student_record.php" method="post">
										<tr class="tablerow<?= ($i%2==1)?1:2?>">
											<td class="tabletext"><? if($detention_waiting_count==0) { echo $eDiscipline['Detention']; }?></td>
											<td class="tabletext"><?=$i_Discipline_Absent?></td>
											<td class="tabletext"><?= $detention_absent_count?></td>
											<td><input type="image" src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_view.gif" class="image_no_border" width="20" height="20" border="0"><input type="hidden" name="targetStatus" id="targetStatus" value="Absent"><input type="hidden" name="SchoolYear" id="SchoolYear" value="<?=$SchoolYear?>"></td>
										</tr>
										</form>
										<? $i++; } ?>
										<? } ?>
										
										<? if($i==1) {?>
										<tr>
										<td align="center" class="tablerow2 tabletext" colspan="4"><br><?=$i_no_record_exists_msg?><br><br></td>
									</tr>
									<? } ?>
				
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				
				
				<tr>
					<td align="right" class="tablebottom">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
