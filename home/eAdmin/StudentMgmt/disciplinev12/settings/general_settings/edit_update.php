<?php
# using: 

########## Change Log [Start] ###########
#
#	Date	:	2017-07-07 (Anna)
#				added AuthenticationSetting
#
#	Date	:	2017-03-16	(Bill)	[2016-1207-1221-39240]
#				added Activty Score settings
#
#	Date	:	2015-08-24	Bill
#				added Redeem setting - Award must be earlier than Punishment
#
#	Date	:	2014-05-22	Carlos
#				$sys_custom['eDiscipline']['PooiToMiddleSchool'] - added misconduct to punishment item setting
#																 - added late to punishment item setting
#																 - added misconduct max score deduction setting
#
#	Date	:	2013-06-13	Carlos
#				$sys_custom['eDiscipline']['yy3'] - add Customized Settings > Conduct Adjustment Base Mark 
#												  							> Conduct Mark Warning Limit
#																			> Conduct Mark Exceed Limit Remark
#	
#	Date	:	2011-04-19	YatWoon
#				add Max. Study Score Increment/Decrement setting
#
#	Date	: 	2010-08-24 (Henry Chow)
#				Add settings "Display eDis icon in portal page"
#	
#	Date	: 	2010-05-07 YatWoon
#				Add settings "Auto-release Award/Punishment Records generated from accumulative Good Conduct/Misconduct Records"
#				Add settings "PIC selection default own"
#
#	Date	: 	2010-04-14 YatWoon
#				Add settings "Only allow admin to select record's PIC"
#
# - 2010-02-24 (Henry)
#	Add option "Not allow same student with same 'Award & Punishment' or 'Good Conduct & Misconduct' item in a same day"
#
# - 2009-12-30 YatWoon
#	Add option "Send email to class teacher when student trigger warning reminder point"
#
########## Change Log [Ebd] ###########

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$data = array();
$data['SemesterRatio_MAX'] = $SemesterRatio_MAX;
$data['ConductMarkIncrement_MAX'] = $ConductMarkIncrement_MAX;
$data['SubScoreIncrement_MAX'] = $SubScoreIncrement_MAX;
$data['ActScoreIncrement_MAX'] = $ActScoreIncrement_MAX;
$data['AwardPunish_MAX'] = $AwardPunish_MAX;
$data['DetentinoSession_MAX'] = $DetentinoSession_MAX;
$data['AccumulativeTimes_MAX'] = $AccumulativeTimes_MAX;
$data['ApprovalLevel_MAX'] = $ApprovalLevel_MAX;
$data['use_subject'] = $use_subject;
$data['AP_Conversion_MAX'] = $AP_Conversion_MAX;
$data['use_newleaf'] = $use_newleaf;
$data['Detention_Sat'] = $Detention_Sat;
$data['Hidden_ConductMark'] = $Hidden_ConductMark;
$data['Display_ConductMarkInAP'] = $Display_ConductMarkInAP;
$data['AP_Interval_Value'] = $AP_Interval_Value;
$data['MaxRecordDayBeforeAllow'] = ($MaxRecordDayBeforeAllow==0) ? "" : $MaxRecordDayBeforeAllow;
$data['AutoReleaseForAPRecord'] = $AutoReleaseForAPRecord;
$data['AutoReleaseAGM2AP'] = $AutoReleaseAGM2AP;
$data['RejectRecordEmailNotification'] = $RejectRecordEmailNotification;
$data['displayOtherTeacherGradeInConductGrade'] = $displayOtherTeacherGradeInConductGrade;
$data['WarningReminderPointEmailNotification'] = $WarningReminderPointEmailNotification;
$data['WarningReminderPointEmailNotificationToDisciplineAdmin'] = $WarningReminderPointEmailNotificationToDisciplineAdmin;
$data['SubScoreWarningReminderPointEmailNotification'] = $SubScoreWarningReminderPointEmailNotification;
$data['SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin'] = $SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin;
$data['ActScoreWarningReminderPointEmailNotification'] = $ActScoreWarningReminderPointEmailNotification;
$data['ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin'] = $ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin;
$data['MaxSchoolDayToCallForAction'] = $MaxSchoolDayToCallForAction;
$data['NotAllowSameItemInSameDay'] = $NotAllowSameItemInSameDay;
$data['NotAllowEmptyInputInAP'] = $NotAllowEmptyInputInAP;
$data['OnlyAdminSelectPIC'] = $OnlyAdminSelectPIC;
$data['PIC_SelectionDefaultOwn'] = $PIC_SelectionDefaultOwn;
$data['DisplayIconInPortal'] = $DisplayIconInPortal;
$data['AutoApproveAPRecord'] = $AutoApproveAPRecord;
$data['AllowToAssignStudentToAccessRightGroup'] = $AllowToAssignStudentToAccessRightGroup;
$data['MaxDetentionDayBeforeAllow'] = $MaxDetentionDayBeforeAllow;
$data['Notice_Copies'] = $Notice_Copies;
$data['NoticeRemark'] = $NoticeRemark;
$data['ConductMarkRemark'] = $ConductMarkRemark;
$data['ConductAdjustmentBaseMark'] = $ConductAdjustmentBaseMark;
$data['ConductAdjustmentBaseMarkGrade'] = $ConductAdjustmentBaseMarkGrade;
$data['NumOfMisconductToAdjustGrade'] = $NumOfMisconductToAdjustGrade;
$data['ConductMarkExceedLimitRemark'] = $ConductMarkExceedLimitRemark;
$data['ConductMarkWarningLimit'] = $ConductMarkWarningLimit;
$data['NoLimitOnAwardPunishmentDate'] = $NoLimitOnAwardPunishmentDate;
$data['AuthenticationSetting'] = $AuthenticationSetting;

if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$data['MisconductToPunishmentItem'] = $MisconductToPunishmentItem;
	$data['LateToPunishmentItem'] = $LateToPunishmentItem;
	$data['MisconductMaxScoreDeduction'] = $MisconductMaxScoreDeduction;
}

# store in DB
$success = $ldiscipline->updateGeneralSettings($data);

if($success >0)
{
	# set $this->
	$ldiscipline->SemesterRatio_MAX = $SemesterRatio_MAX;			
	$ldiscipline->ConductMarkIncrement_MAX = $ConductMarkIncrement_MAX;	
	$ldiscipline->SubScoreIncrement_MAX = $SubScoreIncrement_MAX;
	$ldiscipline->ActScoreIncrement_MAX = $ActScoreIncrement_MAX;
	$ldiscipline->AwardPunish_MAX = $AwardPunish_MAX;				
	$ldiscipline->AccumulativeTimes_MAX = $AccumulativeTimes_MAX;		
	$ldiscipline->DetentinoSession_MAX = $DetentinoSession_MAX;		
	$ldiscipline->ApprovalLevel_MAX = $ApprovalLevel_MAX;			
	$ldiscipline->use_subject = $use_subject;			
	$ldiscipline->use_newleaf = $use_newleaf;			
	$ldiscipline->Detention_Sat = $Detention_Sat;			
	$ldiscipline->Hidden_ConductMark = $Hidden_ConductMark;			
	$ldiscipline->Display_ConductMarkInAP = $Display_ConductMarkInAP;			
	$ldiscipline->AP_Interval_Value = $AP_Interval_Value;			
	$ldiscipline->MaxRecordDayBeforeAllow = $MaxRecordDayBeforeAllow;			
	$ldiscipline->AutoReleaseForAPRecord = $AutoReleaseForAPRecord;	
	$ldiscipline->AutoReleaseAGM2AP = $AutoReleaseAGM2AP;	
	$ldiscipline->RejectRecordEmailNotification = $RejectRecordEmailNotification;			
	$ldiscipline->displayOtherTeacherGradeInConductGrade = $displayOtherTeacherGradeInConductGrade;			
	$ldiscipline->WarningReminderPointEmailNotification = $WarningReminderPointEmailNotification;			
	$ldiscipline->WarningReminderPointEmailNotificationToDisciplineAdmin = $WarningReminderPointEmailNotificationToDisciplineAdmin;	
	$ldiscipline->SubScoreWarningReminderPointEmailNotification = $SubScoreWarningReminderPointEmailNotification;			
	$ldiscipline->SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin = $SubScoreWarningReminderPointEmailNotificationToDisciplineAdmin;	
	$ldiscipline->ActScoreWarningReminderPointEmailNotification = $ActScoreWarningReminderPointEmailNotification;
	$ldiscipline->ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin = $ActScoreWarningReminderPointEmailNotificationToDisciplineAdmin;
	$ldiscipline->MaxSchoolDayToCallForAction = $MaxSchoolDayToCallForAction;			
	$ldiscipline->NotAllowSameItemInSameDay = $NotAllowSameItemInSameDay;			
	$ldiscipline->NotAllowEmptyInputInAP = $NotAllowEmptyInputInAP;			
	$ldiscipline->OnlyAdminSelectPIC = $OnlyAdminSelectPIC;	
	$ldiscipline->PIC_SelectionDefaultOwn = $PIC_SelectionDefaultOwn;	
	$ldiscipline->DisplayIconInPortal = $$DisplayIconInPortal;
	$ldiscipline->AutoApproveAPRecord = $AutoApproveAPRecord;	
	$ldiscipline->AllowToAssignStudentToAccessRightGroup = $AllowToAssignStudentToAccessRightGroup;
	$ldiscipline->MaxDetentionDayBeforeAllow = $MaxDetentionDayBeforeAllow;		
	$ldiscipline->Notice_Copies = $Notice_Copies;
	$ldiscipline->NoticeRemark = $NoticeRemark;
	$ldiscipline->ConductMarkRemark = $ConductMarkRemark;
	$ldiscipline->ConductAdjustmentBaseMark = $ConductAdjustmentBaseMark;
	$ldiscipline->ConductAdjustmentBaseMarkGrade = $ConductAdjustmentBaseMarkGrade;
	$ldiscipline->NumOfMisconductToAdjustGrade = $NumOfMisconductToAdjustGrade;
	$ldiscipline->ConductMarkExceedLimitRemark = $ConductMarkExceedLimitRemark;
	$ldiscipline->ConductMarkWarningLimit = $ConductMarkWarningLimit;
	$ldiscipline->NoLimitOnAwardPunishmentDate = $NoLimitOnAwardPunishmentDate;
	
	if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
		$ldiscipline->MisconductToPunishmentItem = $MisconductToPunishmentItem;
		$ldiscipline->LateToPunishmentItem = $LateToPunishmentItem;
		$ldiscipline->MisconductMaxScoreDeduction = $MisconductMaxScoreDeduction;
	}
	$ldiscipline->AuthenticationSetting = $AuthenticationSetting;	
}

header("Location: index.php");

intranet_closedb();
?>