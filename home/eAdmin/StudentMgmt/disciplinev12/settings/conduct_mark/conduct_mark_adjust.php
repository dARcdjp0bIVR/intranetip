<?php
##################################################
# This page should be implement as: Check user access right and redirect to the corresponding page
# temp for "Overview"
##################################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldisciplinev12 = new libdisciplinev12();

$TAGS_OBJ[] = array($i_Discipline_System_Access_Right_Conduct_Mark, "", 1);

# menu highlight setting
$CurrentPage = "Settings_ConductMark";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldisciplinev12->GET_MODULE_OBJ_ARR();

$PAGE_NAVIGATION[] = array($i_Discipline_System_Conduct_Mark_Student_List, "conduct_mark.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Conduct_Mark_Adjust_Conduct_Mark, "");

# Start layout
$linterface->LAYOUT_START();

?>
<form name="form1" method="post" action="">
<table width="99%" border="0" cellspacing="0" cellpadding="0">
	<tr> 
		<td align="center" bgcolor="#FFFFFF">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="98%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="4">
							<tr class="tablegreentop">
								<td><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0211','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()">#</a></td>
								<td><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0211','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?= $i_general_class ?> <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_off.gif" name="sort0211" width="13" height="13" border="0" align="absmiddle" id="sort0211"></a></td>
								<td width="20%"><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0112','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_a_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?= $i_general_name ?></a></td>
								<td><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0212','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?= $i_Discipline_System_Conduct_Mark_Current_Conduct_Mark?></a></td>
								<td><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0312','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?=$i_Discipline_System_Conduct_Mark_Previous_Adjustment?></a></td>
								<td><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0212','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?=$i_Discipline_System_Conduct_Mark_Manually_Adjustment?></a></td>
								<td><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0212','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?=$i_Discipline_System_Conduct_Mark_Reversed_Conduct_Mark?></a></td>
								<td><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0212','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?=$i_Discipline_System_Conduct_Mark_Reversed_Conduct_Grade?></a></td>
								<td width="20%"><a href="#" class="tabletoplink" onMouseOver="MM_swapImage('sort0212','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_sort_d_on.gif',1)" onMouseOut="MM_swapImgRestore()"><?=$i_Discipline_System_Conduct_Mark_Reason?></a></td>
							</tr>
							<tr class="tablebottom">
								<td class="tabletext">&nbsp;</td>
								<td class="tabletext">&nbsp;</td>
								<td >&nbsp;</td>
								<td class="tabletext">&nbsp;</td>
								<td class="tabletext">&nbsp;</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="2" class="layer_table">
										<tr>
											<td><input name="textfield" type="text" class="formtextbox" size="3"></td>
											<td width="25" align="center"><a href="#"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_assign.gif" width="12" height="12" border="0"></a><a href="#" onMouseOver="MM_swapImage('preset22','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"></a> </td>
										</tr>
									</table>
								</td>
								<td class="tabletext">&nbsp;</td>
								<td class="tabletext">&nbsp;</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="2" class="layer_table">
										<tr>
											<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
											<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset221','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset221" width="18" height="18" border="0" id="preset221"></a> </td>
											<td width="25" align="center"><a href="#"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_assign.gif" width="12" height="12" border="0"></a><a href="#" onMouseOver="MM_swapImage('preset22','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"></a> </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="tablerow1">
								<td class="tabletext"> 1</td>
								<td class="tabletext"> 1A-01</td>
								<td ><a href="#" class="tablelink">Chan Tai Ming</a></td>
								<td class="tabletext"><a href="#" onClick="MM_showHideLayers('show_detail','','show')" class="tablelink">73</a>
										<div id="show_detail" style="position:absolute; width:350px; height:180px; z-index:1; visibility: hidden;">
											<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
												<tr>
													<td height="19">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td width="5" height="19"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_01.gif" width="5" height="19"></td>
																<td height="19" valign="middle" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_02.gif">&nbsp;</td>
																<td width="19" height="19"><a href="#" onClick="MM_showHideLayers('show_detail','','hide')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1" onMouseOver="MM_swapImage('pre_close1','','<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
															</tr>
													</table>
													</td>
												</tr>
												<tr>
													<td>
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr>
																<td width="5" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif" width="5" height="19"></td>
																<td bgcolor="#FFFFF7"><div style="height:150px; overflow:auto;">
																		<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
																			<tr class="tabletop">
																				<td align="center" valign="middle">#</td>
																				<td align="left">Reason</td>
																				<td align="left">PIC</td>
																				<td >Conduct Mark</td>
																				<td >Event Date</td>
																			</tr>
																			<tr class="row_approved">
																				<td align="center" valign="top">1</td>
																				<td align="left" valign="top" class="tabletext">AT- 出貓</td>
																				<td align="left" valign="top" class="tabletext">Miss Chan</td>
																				<td align="left" valign="top">-2</td>
																				<td align="left" valign="top">2008-10-10</td>
																			</tr>
																			<tr class="row_approved">
																				<td align="center" valign="top">2</td>
																				<td align="left" valign="top">AA02-舉報罪行</td>
																				<td align="left" valign="top">Miss Chan</td>
																				<td align="left" valign="top">+1</td>
																				<td align="left" valign="top">2008-10-10</td>
																			</tr>
																			<tr class="tablerow1">
																				<td align="center" valign="top" class="tabletext">3</td>
																				<td align="left" valign="top" class="tabletext">DD-打架</td>
																				<td align="left" valign="top" class="tabletext">Miss Chan</td>
																				<td align="left" valign="top">-2</td>
																				<td align="left" valign="top">2008-10-10</td>
																			</tr>
																			<tr class="tablerow1">
																				<td align="center" valign="top" class="tabletext">4</td>
																				<td align="left" valign="top" class="tabletext">AA01-路不拾遺</td>
																				<td align="left" valign="top" class="tabletext">Miss Chan</td>
																				<td align="left" valign="top">+1</td>
																				<td align="left" valign="top">2008-10-10</td>
																			</tr>
																		</table>
																</div>
																		<br></td>
																<td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif" width="6" height="6"></td>
															</tr>
															<tr>
																<td width="5" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_07.gif" width="5" height="6"></td>
																<td height="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif" width="5" height="6"></td>
																<td width="6" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_09.gif" width="6" height="6"></td>
															</tr>
														</table>
													</td>
												</tr>
											</table>
										</div>
									<a href="#" class="tablelink"></a>
								</td>
								<td class="tabletext"><a href="#" onClick="MM_showHideLayers('show_detail2','','show')" class="tablelink">-5</a>
									<div id="show_detail2" style="position:absolute; width:350px; height:180px; z-index:1; visibility: hidden;">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
											<tr>
												<td height="19">
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="5" height="19"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_01.gif" width="5" height="19"></td>
															<td height="19" valign="middle" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_02.gif">&nbsp;</td>
															<td width="19" height="19"><a href="#" onClick="MM_showHideLayers('show_detail2','','hide')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_off.gif" name="pre_close111" width="19" height="19" border="0" id="pre_close111" onMouseOver="MM_swapImage('pre_close111','','<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_close_on.gif',1)" onMouseOut="MM_swapImgRestore()"></a></td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td width="5" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_04.gif" width="5" height="19"></td>
															<td bgcolor="#FFFFF7"><div style="height:150px; overflow:auto;">
																<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">
																	<tr class="tabletop">
																		<td align="center" valign="middle">#</td>
																		<td align="left">Adjustment</td>
																		<td align="left">PIC</td>
																		<td > Date </td>
																		<td >Reason</td>
																	</tr>
																	<tr class="row_approved">
																		<td align="center" valign="top">1</td>
																		<td align="left" valign="top" class="tabletext">-2</td>
																		<td align="left" valign="top" class="tabletext">Miss Chan</td>
																		<td align="left" valign="top">2008-10-10</td>
																		<td align="left" valign="top">No Reason</td>
																	</tr>
																	<tr class="row_approved">
																		<td align="center" valign="top">2</td>
																		<td align="left" valign="top">-3</td>
																		<td align="left" valign="top">Miss Chan</td>
																		<td align="left" valign="top">2008-10-10</td>
																		<td align="left" valign="top">No Reason</td>
																	</tr>
																</table>
																</div>
																<br>
															</td>
															<td width="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_06.gif" width="6" height="6"></td>
														</tr>
														<tr>
															<td width="5" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_07.gif" width="5" height="6"></td>
															<td height="6" background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_08.gif" width="5" height="6"></td>
															<td width="6" height="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/can_board_09.gif" width="6" height="6"></td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</div>
								<a href="#" class="tablelink"></a>
							</td>
							<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
							<td class="tabletext"><span title="By Miss Chan">68</span></td>
							<td class="tabletext"><span title="By Miss Chan">C</span></td>
							<td class="tabletext">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
									<tr>
										<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
										<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset26','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset26" width="18" height="18" border="0" id="preset26"></a> </td>
									</tr>
								</table>
							</td>
						</tr>
						<tr class="tablegreenrow2">
							<td class="tabletext"> 2</td>
							<td class="tabletext">1A-02</td>
							<td><a href="#" class="tablelink">Chan Ka Wah</a></td>
							<td class="tabletext"> 100</td>
							<td class="tabletext">--</td>
							<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
							<td class="tabletext"> 100</td>
							<td class="tabletext"> A</td>
							<td class="tabletext">
								<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
									<tr>
										<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
										<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset1','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset1" width="18" height="18" border="0" id="preset1"></a> </td>
									</tr>
								</table>
							</td>
						</tr>
							<tr class="tablerow1">
								<td class="tabletext"> 3</td>
								<td class="tabletext">1A-03</td>
								<td ><a href="#" class="tablelink">Chan Tai Keung</a></td>
								<td class="tabletext"><a href="#" class="tablelink">98</a></td>
								<td class="tabletext"><a href="#" class="tablelink">--</a></td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
								<td class="tabletext">98</td>
								<td class="tabletext">A</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
											<tr>
												<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
												<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset261','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset261" width="18" height="18" border="0" id="preset261"></a> </td>
											</tr>
									</table>
								</td>
							</tr>
							<tr class="tablegreenrow2">
								<td class="tabletext"> 4</td>
								<td class="tabletext">1A-04</td>
								<td><a href="#" class="tablelink">Lee Ka Yan</a></td>
								<td class="tabletext">100</td>
								<td class="tabletext">--</td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
								<td class="tabletext"> 100</td>
								<td class="tabletext"> A</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
											<tr>
												<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
												<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset11','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset11" width="18" height="18" border="0" id="preset11"></a> </td>
											</tr>
									</table>
								</td>
							</tr>
							<tr class="tablerow1">
								<td class="tabletext"> 5</td>
								<td class="tabletext">1A-05</td>
								<td ><a href="#" class="tablelink">Lee Han Kim</a></td>
								<td class="tabletext">100</td>
								<td class="tabletext">--</td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
								<td class="tabletext"> 100</td>
								<td class="tabletext"> A</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
											<tr>
												<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
												<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset262','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset262" width="18" height="18" border="0" id="preset262"></a> </td>
											</tr>
									</table>
								</td>
							</tr>
							<tr class="tablegreenrow2">
								<td class="tabletext"> 6</td>
								<td class="tabletext">1A-06</td>
								<td><a href="#" class="tablelink">Kwok Sing Sing</a></td>
								<td class="tabletext"><a href="#" class="tablelink">90</a></td>
								<td class="tabletext"><a href="#" class="tablelink">-10</a></td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
								<td class="tabletext"> 80</td>
								<td class="tabletext"> B</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
											<tr>
												<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
												<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset12','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset12" width="18" height="18" border="0" id="preset12"></a> </td>
											</tr>
									</table>
								</td>
							</tr>
							<tr class="tablerow1">
								<td class="tabletext"> 7</td>
								<td class="tabletext">1A-05</td>
								<td ><a href="#" class="tablelink">Lee Han Kim</a></td>
								<td class="tabletext">100</td>
								<td class="tabletext">--</td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
								<td class="tabletext"> 100</td>
								<td class="tabletext"> A</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
										<tr>
											<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
											<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset263','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset263" width="18" height="18" border="0" id="preset263"></a> </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr class="tablegreenrow2">
								<td class="tabletext"> 8</td>
								<td class="tabletext">1A-06</td>
								<td><a href="#" class="tablelink">Kwok Sing Sing</a></td>
								<td class="tabletext">100</td>
								<td class="tabletext">--</td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
								<td class="tabletext"> 100</td>
								<td class="tabletext"> A</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
											<tr>
												<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
												<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset13','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset13" width="18" height="18" border="0" id="preset13"></a> </td>
											</tr>
									</table>
								</td>
							</tr>
							<tr class="tablerow1">
								<td class="tabletext"> 9</td>
								<td class="tabletext">1A-05</td>
								<td ><a href="#" class="tablelink">Lee Han Kim</a></td>
								<td class="tabletext">100</td>
								<td class="tabletext">--</td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3">											</td>
								<td class="tabletext"> 100</td>
								<td class="tabletext"> A</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
											<tr>
												<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
												<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset264','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset264" width="18" height="18" border="0" id="preset264"></a> </td>
											</tr>
									</table>
								</td>
							</tr>
							<tr class="tablegreenrow2">
								<td class="tabletext"> 10</td>
								<td class="tabletext">1A-06</td>
								<td><a href="#" class="tablelink">Kwok Sing Sing</a></td>
								<td class="tabletext">100</td>
								<td class="tabletext">--</td>
								<td class="tabletext"><input name="textfield" type="text" class="formtextbox" size="3"></td>
								<td class="tabletext"> 100</td>
								<td class="tabletext"> A</td>
								<td class="tabletext">
									<table width="100%" border="0" cellpadding="0" cellspacing="0" class="layer_table">
										<tr>
											<td><input name="text2" type="text" class="textboxtext" value="No Reason" ></td>
											<td width="25" align="center"><a href="#" onMouseOver="MM_swapImage('preset14','','/<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_on.gif',1)" onMouseOut="MM_swapImgRestore()"  onClick="MM_showHideLayers('presetlist','','show')"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_pre-set_off.gif" alt="Pre-set List" name="preset14" width="18" height="18" border="0" id="preset14"></a> </td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location=''")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();

?>

