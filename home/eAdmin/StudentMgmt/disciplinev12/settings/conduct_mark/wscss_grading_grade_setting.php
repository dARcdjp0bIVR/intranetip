<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();

// check access right on this customization
if(!$sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

// check access right on Conduct Mark setting
if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_ConductMark";

$CalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();



$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Base_Mark, "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
if($CalculationMethod == 0) {
	$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);
}
$TAGS_OBJ[] = array($Lang['eDiscipline']['ConductGradeSetting'], "wscss_grading_grade_setting.php", 1);		


$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# thickbox css / js
$includeJs = $ldiscipline_ui->Include_JS_CSS();

# UI form
$html_form = $ldiscipline_ui->Get_WSCSS_Conduct_Grade_Setting_Form($isViewOnly=1);


# Start layout
$linterface->LAYOUT_START();

?>
<?=$includeJs?>
<script language="javascript">
<!--
initThickBox();

function RemoveSetting(id) {
	if(confirm(globalAlertMsg3)) {
		$.post(
			"ajax_delete_year_grade_setting_update.php", 
			{ 
				YearGradeID: id
			},
			function(ReturnData)
			{
				//$("#Main_HTML_Div").html(ReturnData);
				Get_Return_Message(ReturnData);
				Reload_Year_Grade_Setting();	
			}
		);	
	}
}

function AddSetting(yearid) {
	initThickBox();
	$.post(
		"ajax_new_year_grade_setting.php", 
		{ 
			YearID: yearid
		},
		function(ReturnData)
		{
			//initThickBox();
			$('#TB_ajaxContent').html(ReturnData);				
		}
	);	
}

function EditSetting(year_grade_id) {
	initThickBox();
	$.post(
		"ajax_edit_year_grade_setting.php", 
		{ 
			YearGradeID: year_grade_id
		},
		function(ReturnData)
		{
			//initThickBox();
			$('#TB_ajaxContent').html(ReturnData);				
		}
	);	
	
}

function Reload_Year_Grade_Setting() {
	initThickBox();
	Block_Element("Main_HTML_Div");
	
	$.post(
		"ajax_reload_year_grade_setting.php","",
		function(ReturnData)
		{
			$("#Main_HTML_Div").html(ReturnData);
		}
	);
	
	UnBlock_Element("Main_HTML_Div");
	
	initThickBox();
}


function displayFormSetting(YearID) {
	
	var divName = "FormDiv_"+YearID;
	var divPreloadName = "FormPreloadDiv_"+YearID;
	
	if(document.getElementById(divName).style.display=="none") {
		$('#Link_'+YearID).attr("class","zoom_out");
		$("#"+divName).show("slow");
		$("#"+divPreloadName).hide();
	} else {
		$('#Link_'+YearID).attr("class","zoom_in");
		$("#"+divName).hide("hide");
		$("#"+divPreloadName).show();
	}
	
}

function DisplayOption(thisChecked, layername) {
	if(thisChecked==true || document.getElementById(layername).style.display=="none") {
		$("#"+layername).show();
	} else {
		$("#"+layername).hide();
	}
}
//-->
</script>
<div id='tempdiv'></div>
<form name="form1" method="post" action="">
<br />

<div id="Main_HTML_Div">

<?=$html_form?>

</div>

</form>


<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
