<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();


//$returnTable = $ldiscipline_ui->Get_WSCSS_Form_Setting($thisYearID="",$isNewSetting=1);
$GradeData = $ldiscipline->Get_WSCSS_Form_Setting_By_YearID($YearID="", $YearGradeID);
$_GradeChar = $GradeData[0]['GradeChar'];
$_Sequence = $GradeData[0]['Sequence'];
$_YearID = $GradeData[0]['YearID'];


$thisSetting = $ldiscipline_ui->Get_WSCSS_Individual_Form($YearGradeID);
$returnTable .= '<table class="common_table_list_v30 edit_table_list_v30">';
			
$returnTable .= '<tr class="edit_table_head_bulk">';
$returnTable .= '	<th>'.$Lang['General']['Grade'];
$returnTable .= ' <input type="text" name="grade" id="grade" value="'.$_GradeChar.'" size="4">';
$returnTable .= '&nbsp;&nbsp;|&nbsp;&nbsp;'.$Lang['General']['Sequence'];
$returnTable .= ' <input type="text" name="sequence" id="sequence" value="'.$_Sequence.'" size="4">';
$returnTable .= '</th>';
$returnTable .= '</tr>';

for($b=0, $b_max=count($thisSetting); $b<$b_max; $b++) {

	$returnTable .= '<tr>';
	$returnTable .= '	<td colspan="100%">'.$thisSetting[$b].'</td>';
	$returnTable .= '</tr>';
	
}

$returnTable .= '</table>';	
$returnTable .= '<br />';
?>

<script language="javascript">

function GoBack() {
	window.tb_remove();
}

function EditSubmit() {
	
	var obj = document.formEdit;
	
	var thisFormChecked = false;
	
	if($('#grade').val() == "") {
		alert("<?=$Lang['General']['PleaseFillIn']." ".$Lang['General']['Grade']?>");
		return false;	
	}
	
	if($('#sequence').val() == "") {
		alert("<?=$Lang['General']['PleaseFillIn']." ".$Lang['General']['Sequence']?>");
		return false;	
	}
	
	for(var i=1; i<=<?=count($Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'])?>; i++) {
		if(obj.elements['optionInUse['+i+']'].checked==true) {
			thisFormChecked = true;
			
			
			if(document.formEdit.elements['CriteriaValue['+i+']'].value=="" || isNaN(document.formEdit.elements['CriteriaValue['+i+']'].value)) {
				alert("<?=$Lang['eDiscipline']['WSCSS_ConductMarkMustBeNumber']?>");
				return false;
				break;
			}
			
			if(i>2) {
				var optCount = countOption(document.formEdit.elements['ItemID['+i+'][]']);
				if(optCount==0) {
					alert("<?=$Lang['eDiscipline']['WSCSS_PleaseSelectAwardItem']?>");
					return false;
					break;
				}
			}
			
		}
	}
	
	if(thisFormChecked==false) {
		alert("<?=$Lang['eDiscipline']['WSCSS_PleaseSelect_Setting']?>");
		return false;
	}
	
	var PostVar = Get_Form_Values(document.getElementById("formEdit"));
	
	
	$.post(
		"ajax_edit_year_grade_setting_update.php", 
		PostVar,
		function(ReturnData)
		{
			//$("#tempdiv").html(ReturnData);
			Get_Return_Message(ReturnData);
			Reload_Year_Grade_Setting(<?=$_YearID?>);
			window.tb_remove();		
		}
	);
}

</script>
<br>
<form name="formEdit" id="formEdit" method="post" action="">

<?=$returnTable?>

<div class="edit_bottom_v30">

<br />
<input type="hidden" name="YearGradeID" id="YearGradeID" value="<?=$YearGradeID?>">

<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "EditSubmit();");?>
&nbsp;
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "GoBack();");?>

</div>
</form>

<?
intranet_closedb();
?>