<?php
# using: 

###############################
#
#	Date:	2016-10-27 (Bill)	[2016-0707-1429-22240]
#			Add Reminder Target
#
#	Date:	2011-08-30	YatWoon
#			improved: add default template selection
#
###############################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$new = $reminderPoint;
$pre = $previousPoint;

$status = $ldiscipline->editWarningRule("edit", $pre, $new, $SelectNotice, $target); 

intranet_closedb();

if($status==false) {		# record exists
	$url = "warning_rule_edit.php?PointID=$pre&xmsg2=$i_Discipline_System_alert_warning_level_already_exists";
} else {
	$url = "warning_rule.php?xmsg=update";
}

header("Location: $url");
?>