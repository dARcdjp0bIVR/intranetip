<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

//echo "1|=|Added Successfully.";

if(!isset($YearID) || $YearID=="") {
	
	echo $Lang['General']['ReturnMessage']['AddUnsuccess'];
	exit;	
	
} else {
	
	#### Year Grade Setting Array [Start]
	
	$settingAry['YearID'] = $YearID;
	$settingAry['GradeChar'] = $grade;
	$settingAry['Sequence'] = $sequence;
	
	#### Year Grade Setting Array [End]
	
	
	#### Setting Detail Array [Start]
	
	for($a=1, $a_max=count($Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting']); $a<=$a_max; $a++) {
		$pos = $a;
			
			
		if($pos==1 || $pos==2) {
			$thisValue = $CriteriaValue[$pos];	
		} else {
			$thisItem = count($ItemID[$pos])>0 ? implode(',',$ItemID[$pos]) : "";
			$thisValue = ($thisItem=="") ? "#".$CriteriaValue[$pos] : $thisItem."#".$CriteriaValue[$pos];	
		}
		$thisRecordStatus = (isset($optionInUse[$a])) ? 1 : 0;
		
		$settingDetailAry[] = array(
									'CriteriaValue' => $thisValue,
									'RecordStatus' => $thisRecordStatus,
									'Sequence' => $a
								);	
	}
	/*
	debug_pr($settingAry);
	debug_pr($settingDetailAry);
	exit;
	*/
	
	#### Setting Detail Array [End]

	$result = $ldiscipline->Create_Year_Grade_Setting($settingAry, $settingDetailAry);
	
	echo ($result ? $Lang['General']['ReturnMessage']['AddSuccess'] : $Lang['General']['ReturnMessage']['AddUnsuccess']); 
	
}


?>