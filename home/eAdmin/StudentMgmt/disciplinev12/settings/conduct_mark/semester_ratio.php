<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_ConductMark";

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Base_Mark, "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 1);
if($sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ConductGradeSetting'], "wscss_grading_grade_setting.php", 0);		
}

//$schoolYearSelected = ($schoolYear=='') ? getCurrentAcademicYear() : $schoolYear;
$schoolYearSelected = ($schoolYear=='') ? Get_Current_Academic_Year_ID() : $schoolYear;

$years = $ldiscipline->returnAllYearsSelectionArray();
$selectYearMenu = "<select name='schoolYear' onChange='reloadSchoolYear(this.value)'>";
for($i=0; $i<sizeof($years); $i++)
{
	list($yearID, $yearName) = $years[$i];

	$selectYearMenu .= "<option value='$yearID'";
	if($yearID==$schoolYearSelected) {
		$selectYearMenu .= " SELECTED";
	}
	$selectYearMenu .= ">".$yearName."</option>";
}
//$selectYearMenu .= $ldiscipline->getConductSchoolYear($schoolYearSelected);
$selectYearMenu .= "</select>";

############
# Get Year Selection
//$yearID = $ldiscipline->getAcademicYearIDByYearName($schoolYearSelected);
$yearID = $schoolYearSelected;

//$semester_data = split("\n",get_file_content("$intranet_root/file/semester.txt"));
$semester_data = getSemesters($yearID);

$total = 0;

foreach($semester_data as $id=>$name) {
	$select .= "<tr><td>";
	$select .= $name;
	$select .= "</td><td align=center>";
	//$temp = $ldiscipline->getSemesterRatio($schoolYearSelected, $name);
	$temp = $ldiscipline->getSemesterRatio('', '', $yearID, $id);
	$select .= ($temp=="") ? "0" : $temp;
	$select .= "</td></tr>";
	$total += $temp;
}
/*
for ($i=0; $i<sizeof($semester_data); $i++)
{
	$target_sem = $semester_data[$i];
	$line = split("::",$target_sem);
	list ($name,$current) = $line;
	$select .= "<tr><td>";
	$select .= $name;
	$select .= "</td><td align=center>";
	$temp = $ldiscipline->getSemesterRatio($schoolYearSelected, $name);
	$select .= ($temp=="") ? "0" : $temp;
	$select .= "</td></tr>";
	$total += $temp;

}
*/
#######
/*
$sem_ratio = array();
$sem_array = $ldiscipline->getSemeterRatio("2008-2009");

debug_r($sem_array);
*/
if($xmsg != '') {
	$showmsg = "<table width=80% cellspacing=0 cellpadding=2 border=0>";	
	$showmsg .= "<tr><td align=center>";
	$showmsg .= $linterface->GET_SYS_MSG($xmsg);
	$showmsg .= "</td></tr>";
	$showmsg .= "</table>";
}

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function reloadSchoolYear(year) {
	self.location.href = 'semester_ratio.php?schoolYear='+year;
}
//-->
</script>
<form name="form1" method="post" action="semester_ratio_edit.php">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td align="right">
						<?=$showmsg ?>
					</td>
				</tr>
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
								<td>
									<?=$selectYearMenu ?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_System_Conduct_Ratio2?></span></td>
								<td><label for="grading_honor" class="tabletext"></label>
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td><U><?=$i_Discipline_System_Conduct_Semester?></U></td>
											<td align="center"><u><?=$i_Discipline_System_Conduct_Ratio?></u></td>
										</tr>
											<?=$select ?>
										<tr>
											<td align="right" class="tabletextremark" style="border-top:1px solid #CCCCCC"><?=$i_Discipline_System_Conduct_Total?></td>
											<td align="center" class="tabletextremark" style="border-top:1px solid #CCCCCC"><?=$total ?></td>
										</tr>
									</table>
									<label for="grading_passfail" class="tabletext"></label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" alt="" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center"><?= $linterface->GET_ACTION_BTN($button_edit, "submit")?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
