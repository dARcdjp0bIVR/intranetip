<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_ConductMark";

if(is_array($GradeID) || $GradeID[0] != '') {
	$score = $GradeID[0];
}

$Grade = $ldiscipline->getGradeRuleGrade($score);

$CalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Base_Mark, "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 1);
if($CalculationMethod == 0) {
	$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);
}
if($sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ConductGradeSetting'], "wscss_grading_grade_setting.php", 0);		
}

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

?>
<script language="javascript">
<!--
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
                }
        }
}

function checkForm(form1) {
	if(form1.conductScore.value=="")	 {
		alert("<?=$i_Discipline_System_Conduct_alert_min_conduct_mark ?>");	
		form1.conductScore.focus();
		return false;
	}
	if(isNaN(form1.conductScore.value)) {
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");	
		form1.conductScore.focus();
		return false;
	}
	/*
	if(!isInteger(document.getElementById('conductScore').value))
	{
		alert("<?=$i_Discipline_System_Discipline_Conduct_Mark_Score_Integer_JS_alert?>");
		form1.conductScore.focus();
		return false;
	}
	*/
	if(form1.calculatedGrade.value=="")	 {
		alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade?>");	
		form1.calculatedGrade.focus();
		return false;
	}
	/*if(!IsAlphabetic(document.getElementById('calculatedGrade').value))
	{
		alert("<?=$i_Discipline_System_Discipline_Conduct_Mark_Grade_JS_alert?>");	
		
	    form1.calculatedGrade.focus();
		return false;	
	}*/
	return true;
}
function IsAlphabetic(sText)
{
	var ValidChars = "abcdefghijklmnopqrstuvwxyz-+";
	var Char;

	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
            return false;
        }
    }
    return true;

}
function isInteger(sText)
{
	var ValidChars = "0123456789";
	var Char;

	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
	        return false;
        }
    }
    return true;

}
//-->
</script>
<br/>
<form name="form1" action="grading_scheme_add_update.php" method="POST" ONSUBMIT="return checkForm(this)">
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td width="100%" align="right" colspan="2"><?= $linterface->GET_SYS_MSG('',$xmsg2) ?></td>
</tr>
<tr>
	<td colspan="2"><?= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Conduct_Instruction_Recalculate_Msg) ?><br /></td>
</tr>
<tr>
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
		<span class="tabletext"><?=$i_Discipline_System_Conduct_ConductGradeRule_MinScore?></span> <span class="tabletextrequire">*</span>
	</td>
	<td>
		<input type="text" name="conductScore" value="" class="textboxnum">
	</td>
</tr>
<tr>
	<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle">
		<span class="tabletext"><?=$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade?></span> <span class="tabletextrequire">*</span>
	</td>
	<td>
		<input type="text" name="calculatedGrade" value="" class="textboxnum">
	</td>
</tr>
</table>

<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field ?></td></tr>
	<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr>
		<td align="center">
			<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='grading_scheme.php'")?>
		</td>
	</tr>
</table>

<input type=hidden name=previousScore value="<?= $score ?>">
</form>

<?
echo $linterface->FOCUS_ON_LOAD("form1.conductScore"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>
