<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();

$result = $ldiscipline->Remove_Year_Grade_Setting($YearGradeID);

echo ($result ? $Lang['General']['ReturnMessage']['DeleteSuccess'] : $Lang['General']['ReturnMessage']['DeleteUnsuccess']); 

intranet_closedb();
?>