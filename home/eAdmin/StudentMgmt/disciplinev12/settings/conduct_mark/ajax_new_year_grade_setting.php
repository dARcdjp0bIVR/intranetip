<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();


$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline_ui = new libdisciplinev12_ui();

// display blank form
$returnTable = $ldiscipline_ui->Get_WSCSS_Form_Setting($thisYearID="",$isNewSetting=1);

?>

<script language="javascript">

function GoBack() {
	window.tb_remove();
}

function NewSubmit() {
	
	var obj = document.formNew;
	
	var thisFormChecked = false;
	var thisValuefulfil = true;
	
	if($('#grade').val() == "") {
		alert("<?=$Lang['General']['PleaseFillIn']." ".$Lang['General']['Grade']?>");
		return false;	
	}
	
	for(var i=1; i<=<?=count($Lang['eDiscipline']['WSCSS_Conduct_Grade_Setting'])?>; i++) {
		if(document.getElementById('optionInUse_'+i).checked==true) {
			thisFormChecked = true;
			
			if(document.formNew.elements['CriteriaValue['+i+']'].value=="" || isNaN(document.formNew.elements['CriteriaValue['+i+']'].value)) {
				alert("<?=$Lang['eDiscipline']['WSCSS_ConductMarkMustBeNumber']?>");
				return false;
				break;
			}
			
			if(i>2) {
				var optCount = countOption(document.formNew.elements['ItemID['+i+'][]']);
				if(optCount==0) {
					alert("<?=$Lang['eDiscipline']['WSCSS_PleaseSelectAwardItem']?>");
					return false;
					break;
				}
			}
		}
	}
	
	if(thisFormChecked==false) {
		alert("<?=$Lang['eDiscipline']['WSCSS_PleaseSelect_Setting']?>");
		return false;
	}
	
	var PostVar = Get_Form_Values(document.getElementById("formNew"));
	
	
	
	$.post(
		"ajax_new_year_grade_setting_update.php", 
		PostVar,
		function(ReturnData)
		{
			//$("#tempdiv").html(ReturnData);
			Get_Return_Message(ReturnData);
			Reload_Year_Grade_Setting();
			window.tb_remove();		
		}
	);
}

</script>

<form name="formNew" id="formNew" method="post" action="">

<?=$returnTable?>

<div class="edit_bottom_v30">

<br />
<input type="hidden" name="YearID" id="YearID" value="<?=$YearID?>">

<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "NewSubmit();");?>
&nbsp;
<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "GoBack();");?>

</div>
</form>

<?
intranet_closedb();
?>