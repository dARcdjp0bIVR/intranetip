<?php
# using: 

###############################
#
#	Date:	2016-10-27 (Bill)	[2016-0707-1429-22240]
#			Allow set Reminder Target	 ($sys_custom['eDiscipline']['ConductNoticeSetTarget'])
#
#	Date:	2011-08-30	YatWoon
#			improved: add default template selection
#
###############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_ConductMark";

$CalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();

# eNotice Template
$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		# check the Template Catgory has template or not
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp))
			$catArr[] = array($Key,$Key);
	}
}
$templateCat = $linterface->GET_SELECTION_BOX($catArr, 'id="TemplateCategoryID", name="TemplateCategoryID" onChange="changeCat(this.value)"', "", $TemplateCategoryID);

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	for ($j=0; $j<=sizeof($result); $j++) {
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
			//$js .= "obj.options[$j] = new Option('-- $button_select --',0);\n";
		} else {
			$tempTemplate = $result[$j-1][1];
			$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
	        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
	        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
	        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);

			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
			//$js .= "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
		}
	}
}

# [2016-0707-1429-22240] Reminder Target
if($sys_custom['eDiscipline']['ConductNoticeSetTarget'])
	$target_selected = $linterface->GET_SELECTION_BOX(array(), "name='target[]' ID='target[]' class='select_studentlist' size='6' multiple='multiple'", "");

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Base_Mark, "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
if($CalculationMethod == 0) {
	$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);
}
if($sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ConductGradeSetting'], "wscss_grading_grade_setting.php", 0);		
}

$PAGE_NAVIGATION[] = array($i_Discipline_System_Conduct_Warning_Rules_List, "warning_rule.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_New, "");
$CurrentPageArr['eDisciplinev12'] = 1;
# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
# Start layout
$linterface->LAYOUT_START();


?>
<script language="javascript">
<?=$jTemplateString?>
<!--
function checkForm(form1) {
	if(form1.reminderPoint.value=="" || form1.reminderPoint.value==" ")	 {
		alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_Conduct_Warning_Reminder_Point?>");	
		form1.reminderPoint.focus();
		return false;
	}
	if(isNaN(form1.reminderPoint.value)) {
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");	
		form1.reminderPoint.focus();
		return false;
	}
	
	<? if($sys_custom['eDiscipline']['ConductNoticeSetTarget']){ ?>
		objTarget = document.getElementById('target[]');
		checkOption(objTarget);
		checkOptionAll(objTarget);
	<? } ?>
	
	/*
	if(!isInteger(document.getElementById('reminderPoint').value))
	{
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");	
		form1.reminderPoint.focus();
		return false;
	}
	*/
	return true;
}
function isInteger(sText)
{
	var ValidChars = "0123456789";
	var Char;

	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
	        return false;
        }
    }
    return true;

}

function changeCat(cat){

	var x=document.getElementById("SelectNotice");

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}


	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];

		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}

	/****************
	obj = x;
	<?
	
	for ($i=0; $i<sizeof($catArr); $i++) {
		
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);?>
		
		if(cat=="<?=$catArr[$i][0]?>") {
			<?
			for ($j=0; $j<=sizeof($result); $j++) { 
				if($result[$j][0]==0) {
					echo "obj.options[0] = new Option('-- $button_select --',0);\n";	
				} else {
					$tempTemplate = $result[$j-1][1];
					echo "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
				}
			}
			
		?> } <?
	}
	
	?>
	****************/
}
//-->
</script>
<form name="form1" method="post" action="warning_rule_add_update.php" onSubmit="return checkForm(document.form1)">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					<td width="20%" align="right"><?= $linterface->GET_SYS_MSG('',$xmsg2) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_System_Conduct_Warning_Reminder_Point?> <span class="tabletextrequire">*</span></span></td>
								<td><input name="reminderPoint" type="text" class="formtextbox" value=""></td>
							</tr>
							
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['eNoticeTemplate']?></td>
								<td class="tabletext">
									<?=$templateCat?>
									<select name="SelectNotice" id="SelectNotice"><option value="0">-- <?=$button_select?> --</option></select>
								</td>
							</tr>
							
							<? if($sys_custom['eDiscipline']['ConductNoticeSetTarget']){ ?>
								<tr valign="top">
									<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['ConductNoticeTarget']?></td>
									<td>
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr valign="top">
												<td width='4%'><?=$target_selected?></td>
												<td width='1'>&nbsp;</td>
												<td valign="bottom">
													<?=$linterface->GET_BTN($button_select, "button","newWindow('/home/common_choose/index.php?fieldname=target[]&page_title=SelectAudience&permitted_type=1&excluded_type=4&DisplayGroupCategory=0&UserRecordStatus=0,1',16)");?><br />
													<?=$linterface->GET_BTN($button_remove, "button","checkOptionRemove(document.form1.elements['target[]'])");?>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							<? } ?>
							
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="tabletextrequire"><?=$i_general_required_field ?></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button","location.href='warning_rule.php'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
</form>
<?
echo $linterface->FOCUS_ON_LOAD("form1.reminderPoint"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
