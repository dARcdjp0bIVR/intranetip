<?php
//using : Ronald
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_ConductMark";

$CalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Base_Mark, "base_mark.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
if($CalculationMethod == 0) {
	$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);
}
if($sys_custom['eDiscipline']['WSCSS_Conduct_Grade']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ConductGradeSetting'], "wscss_grading_grade_setting.php", 0);		
}

$baseMark = $ldiscipline->getConductMarkRule('baseMark');

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$arrCalculationMethod = array(array(0,$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByTerm']),array(1,$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByAcademicYear']));
$CalculationMethodSelection = getSelectByArray($arrCalculationMethod, ' name="CalculationMethod" ',$CalculationMethod,0,1);

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function checkForm(obj) {
//	var alertConfirmEdit = "<?=$i_Discipline_System_Conduct_Instruction_Msg_Content?>\n<?= $i_general_are_you_sure?>";
	var alertConfirmEdit = "<?=$i_Discipline_System_Conduct_Conduct_Balance_Auto_Update?>\n<?= $i_general_are_you_sure?>";
	if(obj.baseMark.value=="" || obj.baseMark.value==" ") {
		alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_Conduct_Base_Mark?>");	
		obj.baseMark.focus();
		return false;
	}
	/*else if(isNaN(obj.baseMark.value)) {
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");
		obj.baseMark.focus();
		return false;
	}*/
	else if(!isInteger(document.getElementById('baseMark').value))
	{
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");	
		form1.baseMark.focus();
		return false;
	}
	else {
	    if(confirm(alertConfirmEdit)){
			return true;
	    } else 
	    	return false;
	}
}
function isInteger(sText)
{
	var ValidChars = "0123456789-";
	var Char;

	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
	        return false;
        }
    }
    return true;

}
//-->
</script>

<form name="form1" method="post" action="base_mark_edit_update.php" onSubmit="return checkForm(document.form1)">
<br />
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FieldTitle']['CalculationMethod'];?></td>
					<td><?=$CalculationMethodSelection;?></td>
				</tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_Base_Mark?> <span class="tabletextrequire">*^</span></td>
					<td><input name="baseMark" type="text" class="formtextbox" value="<?=$baseMark ?>"></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field ?></td>
					<td>&nbsp;</td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="tabletextremark" colspan="2"><span class="tabletextrequire">^</span><?=$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['OnlyAffectTheCurrentRecord'];?></td>
				</tr>
			</table>													
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<span class="dotline">
						<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='base_mark.php'")?>
						</span>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="right">&nbsp;</td>
	</tr>
</table>
</form>
<?
echo $linterface->FOCUS_ON_LOAD("form1.baseMark"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
