<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$x = "-1";
if($ActionType=="add_title")
{
	# check duplicate
	$sql = "select count(*) from DISCIPLINE_FOLLOWUP_ACTION where Title='$fname'";
	$c = $li->returnVector($sql);
	
	if(!$c[0])
	{
		# found out the max sequence
		$sql = "select max(Sequence) from DISCIPLINE_FOLLOWUP_ACTION";
		$result = $li->returnVector($sql);
		$s = $result[0]+1;
		
		$sql = "insert into DISCIPLINE_FOLLOWUP_ACTION (Title, Sequence, RecordStatus, DateInput, InputBy) values ('$fname', $s, 1, now(), $UserID)";
		$li->db_db_query($sql);
		$x = "1";
	} 
}
else if($ActionType=="update_title")
{
	# check duplicate
	$sql = "select count(*) from DISCIPLINE_FOLLOWUP_ACTION where Title='$fname' and ActionID<>$ExcludeActionID";
	$c = $li->returnVector($sql);
	
	if(!$c[0])
	{
		$sql = "update DISCIPLINE_FOLLOWUP_ACTION set Title='$fname' where ActionID=$ExcludeActionID";
		$li->db_db_query($sql);
		$x = "1";
	} 
}
else if($ActionType=="order")
{
	$DisplayOrderString = $_REQUEST['DisplayOrderString'];
	
	$DisplayOrderActionIDArr = explode(',', $DisplayOrderString);
	$DisplayOrderCount = 1;
	
	foreach($DisplayOrderActionIDArr as $index=>$ActionID)
	{
		if ($ActionID == '' || $ActionID == 0)
			continue;
		
		$sql = "update DISCIPLINE_FOLLOWUP_ACTION set Sequence=$index+1 where ActionID=$ActionID";
		$li->db_db_query($sql);
	}
	
	$x = "1";
}
else if($ActionType=="delete")
{
	$sql = "delete from DISCIPLINE_FOLLOWUP_ACTION where ActionID=$ActionID";
	$li->db_db_query($sql);
	
	$x = "1";
}
else if($ActionType=="toggle")
{
	$sql = "update DISCIPLINE_FOLLOWUP_ACTION set RecordStatus=(RecordStatus * -1) where ActionID=$ActionID";
	$li->db_db_query($sql);
	$x = "1";
}

intranet_closedb();

echo $x;
?>