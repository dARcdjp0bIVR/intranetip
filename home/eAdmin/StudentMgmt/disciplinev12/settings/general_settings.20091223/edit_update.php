<?php
# using: yat
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

$data = array();
$data['SemesterRatio_MAX'] = $SemesterRatio_MAX;
$data['ConductMarkIncrement_MAX'] = $ConductMarkIncrement_MAX;
$data['AwardPunish_MAX'] = $AwardPunish_MAX;
$data['DetentinoSession_MAX'] = $DetentinoSession_MAX;
$data['AccumulativeTimes_MAX'] = $AccumulativeTimes_MAX;
$data['ApprovalLevel_MAX'] = $ApprovalLevel_MAX;
$data['use_subject'] = $use_subject;
$data['AP_Conversion_MAX'] = $AP_Conversion_MAX;
$data['use_newleaf'] = $use_newleaf;
$data['Detention_Sat'] = $Detention_Sat;
$data['Hidden_ConductMark'] = $Hidden_ConductMark;
$data['Display_ConductMarkInAP'] = $Display_ConductMarkInAP;
$data['AP_Interval_Value'] = $AP_Interval_Value;
$data['MaxRecordDayBeforeAllow'] = $MaxRecordDayBeforeAllow;
$data['AutoReleaseForAPRecord'] = $AutoReleaseForAPRecord;
$data['RejectRecordEmailNotification'] = $RejectRecordEmailNotification;

# store in DB
$success = $ldiscipline->updateGeneralSettings($data);

if($success >0)
{
	# set $this->
	$ldiscipline->SemesterRatio_MAX = $SemesterRatio_MAX;			
	$ldiscipline->ConductMarkIncrement_MAX = $ConductMarkIncrement_MAX;		
	$ldiscipline->AwardPunish_MAX = $AwardPunish_MAX;				
	$ldiscipline->AccumulativeTimes_MAX = $AccumulativeTimes_MAX;		
	$ldiscipline->DetentinoSession_MAX = $DetentinoSession_MAX;		
	$ldiscipline->ApprovalLevel_MAX = $ApprovalLevel_MAX;			
	$ldiscipline->use_subject = $use_subject;			
	$ldiscipline->use_newleaf = $use_newleaf;			
	$ldiscipline->Detention_Sat = $Detention_Sat;			
	$ldiscipline->Hidden_ConductMark = $Hidden_ConductMark;			
	$ldiscipline->Display_ConductMarkInAP = $Display_ConductMarkInAP;			
	$ldiscipline->AP_Interval_Value = $AP_Interval_Value;			
	$ldiscipline->MaxRecordDayBeforeAllow = $MaxRecordDayBeforeAllow;			
	$ldiscipline->AutoReleaseForAPRecord = $AutoReleaseForAPRecord;			
	$ldiscipline->RejectRecordEmailNotification = $RejectRecordEmailNotification;			
}

header("Location: index.php");

intranet_closedb();
?>
