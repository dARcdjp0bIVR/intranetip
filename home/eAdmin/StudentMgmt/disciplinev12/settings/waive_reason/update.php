<?php
// Using By : 

########## Change Log ###############
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				create for waive / redeem reason
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['PresetWaiveReason']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# POST data
$ReasonID = $_POST['ReasonID'];
$ReasonContent = standardizeFormPostValue($_POST['WaiveReason']);

$sql = "";
$returnMsgKey = "";
if($ReasonID){
	$sql .= "Update DISCIPLINE_PRESET_REASON Set ReasonText='".$ldiscipline->Get_Safe_Sql_Query($ReasonContent)."', DateModified=now() WHERE ReasonType='waive' AND ReasonID='$ReasonID'";
	$returnMsgKey = "Update";
}
else{
	$sql .= "INSERT INTO DISCIPLINE_PRESET_REASON (ReasonType, ReasonText, DateInput, DateModified)
				VALUES ('waive', '".$ldiscipline->Get_Safe_Sql_Query($ReasonContent)."', now(), now())";
	$returnMsgKey = "Add";
}
$resultId = $ldiscipline->db_db_query($sql);

if ($resultId > 0)
	$returnMsgKey .= "Success";
else
	$returnMsgKey .= "Unsuccess";

intranet_closedb();

header("location:index.php?returnMsgKey=$returnMsgKey");
?>