<?php
// Using By : 

########## Change Log ###############
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				create for waive / redeem reason
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$ldiscipline_cust = new libdisciplinev12_ui_cust();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['PresetWaiveReason']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# GET data
$returnMsgKey = $_GET["returnMsgKey"];
$ReturnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];

# Tag
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eDiscipline']['PresetWaiveReason']['Title'], "", 0);

# Current Page
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_Preset_Waive_Reason";
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Toolbar
$btnAry = array();
$btnAry[] = array("new", "javascript: goNew();");
$toolbar = $linterface->Get_Content_Tool_By_Array_v30($btnAry);

# Button
$actionBtnAry = array();
$actionBtnAry[] = array("edit", "javascript:checkEdit(document.form1, 'ReasonIDAry[]', 'edit.php')");
$actionBtnAry[] = array("delete", "javascript:checkRemove2(document.form1,'ReasonIDAry[]','goDelete();')");
$actionBtn = $linterface->Get_DBTable_Action_Button_IP25($actionBtnAry);

# Content Table
$presetReasonList = $ldiscipline->Get_Preset_Reason_List('waive');
$dataTable = $ldiscipline_cust->getPresetReasonTableHtml($presetReasonList);

$linterface->LAYOUT_START($ReturnMsg);
?>

<script type="text/javascript">
function goNew() {
	$('form#form1').attr('action', 'edit.php').submit();
}

function goDelete() {
	$('form#form1').attr('action', 'delete_update.php').submit();
}
</script>

<form id="form1" name="form1" method="POST" action="">
	<div class="content_top_tool">
		<div class="Conntent_tool">
	       <?=$toolbar?>
		</div>
	</div>
	
	<div class="table_board">
		<?=$actionBtn?>
		<div id="dataTableDiv"><?=$dataTable?></div>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>