<?php
// Using By : 

########## Change Log ###############
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				create for waive / redeem reason
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['PresetWaiveReason']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# POST data
$ReasonIDAry = $_POST["ReasonIDAry"];

$sql = "DELETE FROM DISCIPLINE_PRESET_REASON WHERE ReasonID IN ('".implode("','",(array)$ReasonIDAry)."')";
$resultId = $ldiscipline->db_db_query($sql);

if ($resultId > 0)
	$returnMsgKey = 'DeleteSuccess';
else
	$returnMsgKey = 'DeleteUnsuccess';

intranet_closedb();

header("location:index.php?returnMsgKey=.$returnMsgKey");
?>