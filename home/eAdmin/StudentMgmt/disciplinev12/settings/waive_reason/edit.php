<?php
// Using By : 

########## Change Log ###############
#
#	Date	:	2015-09-25	(Bill)	[2015-0416-1040-06164]
#				create for waive / redeem reason
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID']) || !$sys_custom['eDiscipline']['PresetWaiveReason']) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# POST data
$ReasonID = $_POST["ReasonIDAry"];
$ReasonID = $ReasonID[0];

# Get Preset Reason - not new Reason
if($ReasonID){
	list($reason_id, $reason_content) = $ldiscipline->Get_Preset_Reason_List("waive", $ReasonID);
}

# Tag
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eDiscipline']['PresetWaiveReason']['Title'], "", 0);

# Current Page
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_Preset_Waive_Reason";
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Navigation
$actionDisplay = $ReasonID==""? $Lang['Btn']['New'] : $Lang['Btn']['Edit'];
$navigationAry[] = array($Lang['eDiscipline']['PresetWaiveReason']['Title'], "javascript: goBack();");
$navigationAry[] = array($actionDisplay);
$navigation = $linterface->GET_NAVIGATION_IP25($navigationAry);

# Edit Table
$x = "";
$x .= "<table class='form_table_v30'>\r\n";
	$x .= "<tbody>\r\n"; 
		$x .= "<tr>\r\n";
			$x .= "<td class='field_title'>".$linterface->RequiredSymbol().$Lang['eDiscipline']['PresetWaiveReason']['Reason']."</td>\r\n";
			$x .= "<td>\r\n";
				$x .= $linterface->GET_TEXTBOX_NAME("WaiveReason", "WaiveReason", $reason_content, $OtherClass="requiredField", $OtherPar=array());
				$x .= $linterface->Get_Form_Warning_Msg("EmptyWarnDiv", $Lang['eDiscipline']['JS_warning']['InputReason'], $Class="warnMsgDiv")."\n";
			$x .= "</td>\r\n";
		$x .= "</tr>\r\n";
	$x .= "</tbody>\r\n";
$x .= "</table>\r\n";
$formTable = $x;

# Button
$submitBtn = $linterface->Get_Action_Btn($Lang['Btn']['Submit'], "button", "goSubmit()", "submitBtn");
$backBtn = $linterface->Get_Action_Btn($Lang['Btn']['Back'], "button", "goBack()", "backBtn");

# Hidden fields
$reasonIDHiddenField = $linterface->GET_HIDDEN_INPUT('ReasonID', 'ReasonID', $reason_id);

$linterface->LAYOUT_START();
?>

<script type="text/javascript">
$(document).ready( function() {
	$('input#WaiveReason').keypress(function(e) {
		if (e.which == 13) {
			goSubmit();
		    return false;
		}
	});
	$('input#WaiveReason').focus();
});

function goBack() {
	window.location = 'index.php';
}

function goSubmit() {
	var canSubmit = true;
	var isFocused = false;
	
	$('div.warnMsgDiv').hide();
	
	// disable action button to prevent double submission
	$('input#submitBtn').attr('disabled', 'disabled');
	
	// check waive reason fields
	if ($.trim($('input#WaiveReason').val())=="") {
		canSubmit = false;
			
		$('div#EmptyWarnDiv').show();
		$('input#WaiveReason').focus();
	}
	
	if (canSubmit) {
		$("#form1").submit();
	}
	else {
		// enable the action buttons if not allowed to submit
		$('input#WaiveReason').attr('disabled', '');
	}
}
</script>
<form name="form1" id="form1" method="POST" action="update.php">
	<?=$navigation?>
	<br style="clear:both;" />
	
	<div class="table_board">
		<p class="spacer"></p>
		<?=$formTable?>
		<?=$reasonIDHiddenField?>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$submitBtn?>
		<?=$backBtn?>
		<p class="spacer"></p>
	</div>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>