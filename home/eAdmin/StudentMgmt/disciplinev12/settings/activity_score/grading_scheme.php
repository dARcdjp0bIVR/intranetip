<?php

#############################################
#
#	Date	:	2017-10-13 (Bill)	[DM#3273]
#				fixed Session Problem in PHP 5.4
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				create File
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve table view
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
}
else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
}
else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$sql  = "SELECT
				MinActScore,
				GradeChar,
				CONCAT('<input type=\'checkbox\' name=\'GradeID[]\' value=\'', MinActScore ,'\'>')
                FROM
					DISCIPLINE_ACTSCORE_GRADE_RULE ";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("MinActScore", "GradeChar");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0);
$li->wrap_array = array(0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='3%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='57%' >".$Lang['eDiscipline']['ActivityScoreLowerBoundary']."*</td>\n";
$li->column_list .= "<td width='40%'>".$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("GradeID[]")."</td>\n";

# Menu highlight setting
$CurrentPage = "Settings_ActivityScore";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ActivityScoreBaseMark'], "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);
$toolbar = $linterface->GET_LNK_NEW("grading_scheme_add.php", $button_new, "", "", "", 0);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# System Msg
$regenerateWarningRowHTML = "";
$regenerateWarningRowHTML .= "<tr><td colspan='2' align='center'>";
	$regenerateWarningRowHTML .= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $Lang['eDiscipline']['GenerateGradeOfActivityScoreInstruction']);
$regenerateWarningRowHTML .= "</td></tr>";
?>

<script language="javascript">
<!--
function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0)
        alert(globalAlertMsg2);
    else{
        if(confirm(alertConfirmRemove)){
            obj.action=page;
            obj.method="post";
            obj.submit();
        }
    }
}
//-->
</script>

<form name="form1" method="post" action="">
<br />
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="80%"><?=$toolbar ?></td>
		<td width="20%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
	</tr>
	<?= $regenerateWarningRowHTML ?>
</table>
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="right">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="28" align="right" valign="bottom" >
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>&nbsp;</td>
									<td align="right" valign="bottom">
										<table border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
													<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
														<table border="0" cellspacing="0" cellpadding="2">
															<tr>
																<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'GradeID[]','grading_scheme_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																<td nowrap="nowrap"><a href="javascript:removeCat(document.form1,'GradeID[]','grading_scheme_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
															</tr>
														</table>
													</td>
													<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
												</tr>
										</table>
									</td>
								</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<?= $li->display() ?>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>