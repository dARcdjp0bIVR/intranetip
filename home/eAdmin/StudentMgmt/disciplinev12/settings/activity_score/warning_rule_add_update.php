<?php

#############################################
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				create File
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$new = $reminderPoint;

$status = $ldiscipline->editActivityScoreWarningRule("add", "", $new); 

intranet_closedb();

# Record exists
if($status==false) { 
	$url = "warning_rule_add.php?xmsg2=$i_Discipline_System_alert_warning_level_already_exists";	
}
else {
	$url = "warning_rule.php?xmsg=add";
}

header("Location: $url");
?>