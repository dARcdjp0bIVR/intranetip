<?php

#############################################
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#				create File
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Menu highlight setting
$CurrentPage = "Settings_ActivityScore";

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 1);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ActivityScoreBaseMark'], "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Conduct_Warning_Rules_List, "warning_rule.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_New, "");

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function checkForm(form1) {
	if(form1.reminderPoint.value=="" || form1.reminderPoint.value==" ")	 {
		alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_Conduct_Warning_Reminder_Point?>");	
		form1.reminderPoint.focus();
		return false;
	}
	if(isNaN(form1.reminderPoint.value)) {
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");	
		form1.reminderPoint.focus();
		return false;
	}
	/*
	if(!isInteger(document.getElementById('reminderPoint').value))
	{
		alert("<?=$i_ServiceMgmt_System_Warning_Numeric?>");	
		form1.reminderPoint.focus();
		return false;
	}
	*/
	return true;
}
function isInteger(sText)
{
	var ValidChars = "0123456789";
	var Char;

	sText = sText.toLowerCase();
	for (i = 0; i < sText.length; i++)
    {
        Char = sText.charAt(i);
        if (ValidChars.indexOf(Char) == -1)
        {
	        return false;
        }
    }
    return true;

}
//-->
</script>

<form name="form1" method="post" action="warning_rule_add_update.php" onSubmit="return checkForm(document.form1)">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					<td width="20%" align="right"><?= $linterface->GET_SYS_MSG('',$xmsg2) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Discipline_System_Conduct_Warning_Reminder_Point?> <span class="tabletextrequire">*</span></span></td>
								<td><input name="reminderPoint" type="text" class="formtextbox" value=""></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="tabletextrequire"><?=$i_general_required_field ?></td>
								<td>&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button","location.href='warning_rule.php'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
</form>

<?
echo $linterface->FOCUS_ON_LOAD("form1.reminderPoint"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>