<?php
// using : 

############# Change Log (start) #################
#
#	Date:	2017-07-06 (Anna)
#			create file for input Allow Access IP - copy from EJ
#
############## Change Log (end) ##################

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Left Menu
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_AllowAccessIP";

# Tag information
$TAGS_OBJ[] = array($Lang['Security']['AllowAccessIP'], "");
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

// get current IP settings
$AllowAccessIP = $ldiscipline->getDisciplineGeneralSetting("AllowAccessIP");

?>
<script type="text/javascript" language="Javascript">
function checkForm(obj)
{
	var input_ip = document.getElementById('AllowAccessIP').value.Trim();
	var is_valid = true;
	
	if(input_ip != '') {
		var ip_ary = input_ip.split('\n');
		for(var i=0;i<ip_ary.length;i++) {
			if(!ip_ary[i].match(/([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+|\[\d+-\d+\])(\/[0-9]+)*/)){
				is_valid = false;
				break;
			}
		}
	}
	if(!is_valid) {
		alert('<?=$Lang['Security']['WarnMsgArr']['InvalidIPAddress']?>');
		return false;
	}
	obj.submit();
}
</script>
<form name="form1" action="update.php" method="post" onsubmit="checkForm(this); return false;">
	<table align="center" width="90%" border="0" cellpadding="5" cellspacing="0" >
		<tr>
			<td>&nbsp;</td>
			<td align="right"><?=$linterface->GET_SYS_MSG($msg);?></td>
		</tr>
		<tr>
			<td colspan="2" align="right">&nbsp;</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="tabletext">
				<?=$Lang['Security']['TerminalIPList']?>
			</td>
			<td class="tabletext" width="70%">
				<span class="tabletextremark"><?=$Lang['Security']['TerminalIPInput']?><br />
				<?=$Lang['Security']['TerminalYourAddress'].':'.getRemoteIpAddress()?></span><br />
				<?=$linterface->GET_TEXTAREA("AllowAccessIP", $AllowAccessIP)?>
			</td>
		</tr>
		<tr>
			<td colspan="2" class="formfieldtitle tabletext">
				<br /><?=$Lang['Security']['TerminalIPSettingsDescription']?>
			</td>
		</tr>
	</table>
	<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0" >
		<tr>
			<td height="1" class="dotline" colspan="2">
				<img src="<?=$PATH_WRT_ROOT.'images/'.$LAYOUT_SKIN?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		<tr>
			<td align="center" valign="bottom" colspan="2">
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Save'], "submit")?>
				<?=$linterface->GET_ACTION_BTN($Lang['Btn']['Reset'], "reset")?>
			</td>
		</tr>
	</table>
</form>
<br>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>