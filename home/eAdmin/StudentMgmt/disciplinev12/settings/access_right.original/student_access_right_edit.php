<?php 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 0);
//$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 1);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# TABLE COLUMN 

$table .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$table .= "<tr class='tabletop'>";
$table .= "<td width='20%'>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$table .= "<td width='20%'>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
//$table .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$table .= "<td width='20%'>".$i_Discipline_System_Access_Right_Detention."</td>";
$table .= "<td width='20%'>".$eDiscipline['StudentReport'] ."</td>";
$table .= "<td width='20%'>".$iDiscipline['Reports_Child_PersonalReport'] ."</td>";
$table .= "</tr><tr class='tablerow1'>";


$GroupID = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('S','');
# VIEW_ACCESS_RIGHT($GroupID, $Module, $Section, $Function)

$tempResult = array();
$tdPreSet = "";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','GoodConduct_Misconduct','','');
$table .= "<td class='";
if($tempResult[0][1]=='View') {$table .= "tablegreenrow3"; $tdPreSet .= "document.getElementById('td1').className = 'tablegreenrow3';";} else {$table .= "tabletextremark"; $tdPreSet .= "document.getElementById('td1').className = 'tabletextremark';";}
$table .= "' id='td1'><input type='checkbox' name='GoodConduct_Misconduct' id='GoodConduct_Misconduct' value='View' onclick=\"checkBoxOnClick(this)\"";
if($tempResult[0][1]=='View') {$table .= " checked";}
$table .= "> <label for='GoodConduct_Misconduct'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$table .= "<td class='";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','Award_PunishMent','','');
if($tempResult[0][1]=='View') {$table .= "tablegreenrow3"; $tdPreSet .= "document.getElementById('td2').className = 'tablegreenrow3';";} else {$table .= "tabletextremark"; $tdPreSet .= "document.getElementById('td2').className = 'tabletextremark';";}
$table .= "' id='td2'><input type='checkbox' name='Award_Punishment' id='Award_Punishment' value='View' onclick=\"checkBoxOnClick(this)\"";
if($tempResult[0][1]=='View') {$table .= " checked";}
$table .= "> <label for='Award_Punishment'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','Detention','','');
$table .= "<td class='";
if($tempResult[0][1]=='View') {$table .= "tablegreenrow3"; $tdPreSet .= "document.getElementById('td3').className = 'tablegreenrow3';";} else {$table .= "tabletextremark"; $tdPreSet .= "document.getElementById('td3').className = 'tabletextremark';";}
$table .= "' id='td3'><input type='checkbox' name='Detention' id='Detention' value='View' onclick=\"checkBoxOnClick(this)\"";
if($tempResult[0][1]=='View') {$table .= " checked";}
$table .= "> <label for='Detention'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','StudentReport','','');
$table .= "<td class='";
if($tempResult[0][1]=='View') {$table .= "tablegreenrow3"; $tdPreSet .= "document.getElementById('td4').className = 'tablegreenrow3';";} else {$table .= "tabletextremark"; $tdPreSet .= "document.getElementById('td4').className = 'tabletextremark';";}
$table .= "' id='td4'><input type='checkbox' name='StudentReport' id='StudentReport' value='View' onclick=\"checkBoxOnClick(this)\"";
if($tempResult[0][1]=='View') {$table .= " checked";}
$table .= "> <label for='StudentReport'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','PersonalReport','','');
$table .= "<td class='";
if($tempResult[0][1]=='View') {$table .= "tablegreenrow3"; $tdPreSet .= "document.getElementById('td5').className = 'tablegreenrow3';";} else {$table .= "tabletextremark"; $tdPreSet .= "document.getElementById('td5').className = 'tabletextremark';";}
$table .= "' id='td5'><input type='checkbox' name='PersonalReport' id='PersonalReport' value='View' onclick=\"checkBoxOnClick(this)\"";
if($tempResult[0][1]=='View') {$table .= " checked";}
$table .= "> <label for='PersonalReport'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$table .= "</tr>";
$table .= "</table>";

$tempResult = array();

$table2 .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$table2 .= "<tr class='tabletop'>";
$table2 .= "<td width='25%'>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$table2 .= "<td width='25%'>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$table2 .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','RANKING','GoodConduct_Ranking','','');
$table2 .= "<td class='";
if($tempResult[0][1]=='View') {$table2 .= "tablegreenrow3"; $tdPreSet .= "document.getElementById('td5').className = 'tablegreenrow3';";} else {$table2 .= "tabletextremark"; $tdPreSet .= "document.getElementById('td5').className = 'tabletextremark';";}
$table2 .= "' id='td5'><input type='checkbox' name='GoodConduct_Ranking' id='GoodConduct_Ranking' value='View' onclick=\"checkBoxOnClick(this)\"";
if($tempResult[0][1]=='View') {$table2 .= " checked";}
$table2 .= "> <label for='GoodConduct_Ranking'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','RANKING','Award_Ranking','','');
$table2 .= "<td class='";
if($tempResult[0][1]=='View') {$table2 .= "tablegreenrow3"; $tdPreSet .= "document.getElementById('td6').className = 'tablegreenrow3';";} else {$table2 .= "tabletextremark"; $tdPreSet .= "document.getElementById('td6').className = 'tabletextremark';";}
$table2 .= "' id='td6'><input type='checkbox' name='Award_Ranking' id='Award_Ranking' value='View' onclick=\"checkBoxOnClick(this)\"";
if($tempResult[0][1]=='View') {$table2 .= " checked";}
$table2 .= "> <label for='Award_Ranking'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$table2 .= "</tr>";
$table2 .= "</table>";
# Start layout
$linterface->LAYOUT_START();

# no navigation in this page
$PAGE_NAVIGATION[] = array($i_Discipline_System_Student_Right_Navigation_Edit_Right, "");
?>

<br />
<script language="javascript">
<!--
function checkBoxOnClick(elementRef)
{
	elementRef.parentNode.className = (elementRef.checked) ? 'tablegreenrow3' : 'tabletextremark';
}

function resetField() {
	<?= $tdPreSet ?>
}
//-->
</script>
<form name="form1" method="post" action="student_access_right_edit_update.php">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td class="form_sep_title"> <i><?= $eDiscipline['Viewing'] ?></i>
									<?= $table ?>
								</td>
							</tr>
							<tr>
								<td class="form_sep_title"> <i><?= $eDiscipline['RankingReport'] ?></i>
									<?= $table2 ?>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>									
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="88%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_save, "button", "form1.submit()")?>&nbsp;<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "javacript:resetField()")?>&nbsp;<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:location.href='student_access_right.php'")?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>