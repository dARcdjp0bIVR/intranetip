<?php 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

#####
$tableMGMT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableMGMT .= "<tr class='tabletop'>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Award_Punish ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Case_Record ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Conduct_Mark ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Detention_Management ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Detention_Session_Arrangement ."</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td1'><input name='MGMT_Award_Punishment_View' type='checkbox' id='f1' value='View' onclick='checkBoxOnClick(this)'>";
$tableMGMT .= "<label for='f1'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td2'><input name='MGMT_GoodConduct_Misconduct_View' type='checkbox' id='f2' value='View' onclick='checkBoxOnClick(this)'>";
$tableMGMT .= "<label for='f2'>".$i_Discipline_System_Access_Right_View."</label></td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td3'><input name='MGMT_Case_Record_View' type='checkbox' id='f3' value='View' onclick='checkBoxOnClick(this)'>";
$tableMGMT .= "<label for='f3'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td4'><input name='MGMT_Conduct_Mark_View' type='checkbox' id='f4' value='View' onclick='checkBoxOnClick(this)'>";
$tableMGMT .= "<label for='f4'>".$i_Discipline_System_Access_Right_View."</label></td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td5'><input name='MGMT_Detention_View' type='checkbox' id='f5' value='View' onclick='checkBoxOnClick(this)'>";
$tableMGMT .= "<label for='f5'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td6'><input name='MGMT_Award_Punishment_New' type='checkbox' id='f6' value='New' onclick='checkBoxOnClick(this)'><label for='f6'>".$i_Discipline_System_Access_Right_New."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td7'><input name='MGMT_GoodConduct_Misconduct_New' type='checkbox' id='f7' value='New' onclick='checkBoxOnClick(this)'><label for='f7'>".$i_Discipline_System_Access_Right_New."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td8'><input name='MGMT_Case_Record_New' type='checkbox' id='f8' value='New' onclick='checkBoxOnClick(this)'><label for='f8'>".$i_Discipline_System_Access_Right_New."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td9'><input name='MGMT_Detention_New' type='checkbox' id='f9' value='New' onclick='checkBoxOnClick(this)'><label for='f9'>".$i_Discipline_System_Access_Right_New."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td10'><input name='MGMT_Award_Punishment_Edit' type='checkbox' id='f10' value='Edit' onclick='if(this.checked==false){form1.f10_OwnAll[0].checked=false; form1.f10_OwnAll[1].checked=false;} else{form1.f10_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f10'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f10_OwnAll' type='radio' id='f10_OwnAll[0]' value='Own' onclick='form1.MGMT_Award_Punishment_Edit.checked=true;checkBoxOnClick(this)'>";
$tableMGMT .= "<label for='f10_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f10_OwnAll' type='radio' id='f10_OwnAll[1]' value='All' onclick='form1.MGMT_Award_Punishment_Edit.checked=true;checkBoxOnClick(this)'>";
$tableMGMT .= "<label for='f10_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td11'><input name='MGMT_GoodConduct_Misconduct_Edit' type='checkbox' id='f11' value='Edit' onclick='if(this.checked==false){form1.f11_OwnAll[0].checked=false; form1.f11_OwnAll[1].checked=false;} else{form1.f11_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f11'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f11_OwnAll' type='radio' id='f11_OwnAll[0]' value='Own' onclick='form1.MGMT_GoodConduct_Misconduct_Edit.checked=true;checkBoxOnClick(this)'><label for='f11_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f11_OwnAll' type='radio' id='f11_OwnAll[1]' value='All' onclick='form1.MGMT_GoodConduct_Misconduct_Edit.checked=true;checkBoxOnClick(this)'><label for='f11_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td12'>";
$tableMGMT .= "<input name='MGMT_Case_Record_Edit' type='checkbox' id='f12' value='Edit' onclick='if(this.checked==false){form1.f12_OwnAll[0].checked=false; form1.f12_OwnAll[1].checked=false;} else{form1.f12_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f12'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f12_OwnAll' type='radio' id='f12_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_Edit.checked=true;checkBoxOnClick(this)'><label for='f12_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f12_OwnAll' type='radio' id='f12_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_Edit.checked=true;checkBoxOnClick(this)'><label for='f12_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td13'>";
$tableMGMT .= "<input name='MGMT_Detention_Edit' type='checkbox' id='f13' value='Edit' onclick='if(this.checked==false){form1.f13_OwnAll[0].checked=false; form1.f13_OwnAll[1].checked=false;} else{form1.f13_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f13'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f13_OwnAll' type='radio' id='f13_OwnAll[0]' value='Own' onclick='form1.MGMT_Detention_Edit.checked=true;checkBoxOnClick(this)'><label for='f13_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f13_OwnAll' type='radio' id='f13_OwnAll[1]' value='All' onclick='form1.MGMT_Detention_Edit.checked=true;checkBoxOnClick(this)'><label for='f13_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td14'><input name='MGMT_Award_Punishment_Delete' type='checkbox' id='f14' value='Delete' onclick='if(this.checked==false){form1.f14_OwnAll[0].checked=false; form1.f14_OwnAll[1].checked=false;} else{form1.f14_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f14'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f14_OwnAll' type='radio' id='f14_OwnAll[0]' value='Own' onclick='form1.MGMT_Award_Punishment_Delete.checked=true;checkBoxOnClick(this)'><label for='f14_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f14_OwnAll' type='radio' id='f14_OwnAll[1]' value='All' onclick='form1.MGMT_Award_Punishment_Delete.checked=true;checkBoxOnClick(this)'><label for='f14_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td15'><input name='MGMT_GoodConduct_Misconduct_Delete' type='checkbox' id='f15' value='Delete' onclick='if(this.checked==false){form1.f15_OwnAll[0].checked=false; form1.f15_OwnAll[1].checked=false;} else{form1.f15_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f15'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f15_OwnAll' type='radio' id='f15_OwnAll[0]' value='Own' onclick='form1.MGMT_GoodConduct_Misconduct_Delete.checked=true;checkBoxOnClick(this)'><label for='f15_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f15_OwnAll' type='radio' id='f15_OwnAll[1]' value='All' onclick='form1.MGMT_GoodConduct_Misconduct_Delete.checked=true;checkBoxOnClick(this)'><label for='f15_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td16'><input name='MGMT_Case_Record_Delete' type='checkbox' id='f16' value='Delete' onclick='if(this.checked==false){form1.f16_OwnAll[0].checked=false; form1.f16_OwnAll[1].checked=false;} else{form1.f16_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f16'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f16_OwnAll' type='radio' id='f16_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_Delete.checked=true;checkBoxOnClick(this)'><label for='f16_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f16_OwnAll' type='radio' id='f16_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_Delete.checked=true;checkBoxOnClick(this)'><label for='f16_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td17'><input name='MGMT_Detention_Delete' type='checkbox' id='f17' value='Delete' onclick='if(this.checked==false){form1.f17_OwnAll[0].checked=false; form1.f17_OwnAll[1].checked=false;} else{form1.f17_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f17'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f17_OwnAll' type='radio' id='f17_OwnAll[0]' value='Own' onclick='form1.MGMT_Detention_Delete.checked=true;checkBoxOnClick(this)'><label for='f17_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f17_OwnAll' type='radio' id='f17_OwnAll[1]' value='All' onclick='form1.MGMT_Detention_Delete.checked=true;checkBoxOnClick(this)'><label for='f17_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td18'><input name='MGMT_Award_Punishment_Waive' type='checkbox' id='f18' value='Waive' onclick='checkBoxOnClick(this)'><label for='f18'>".$i_Discipline_System_Access_Right_Waive."</label> </td>";
if($ldiscipline->use_newleaf) {
	$tableMGMT .= "<td valign='top' class='tabletextremark' id='td48'><input name='MGMT_GoodConduct_Misconduct_NewLeaf' type='checkbox' id='f48' value='NewLeaf' onclick='checkBoxOnClick(this)'><label for='f48'>".$i_Discipline_System_Access_Right_NewLeaf."</label> </td>";
} else {
	$tableMGMT .= "<td valign='top'>&nbsp;</td>";
}
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td19'><input name='MGMT_Award_Punishment_Release' type='checkbox' id='f19' value='Release' onclick='checkBoxOnClick(this)'><label for='f19'>".$i_Discipline_System_Access_Right_Release."</label></td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td46'><input name='MGMT_Case_Record_Release' type='checkbox' id='f46' value='Release' onclick='checkBoxOnClick(this)'><label for='f46'>".$i_Discipline_System_Access_Right_Release."</label></td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td20'><input name='MGMT_Award_Punishment_Approval' type='checkbox' id='f20' value='Approval' onclick='checkBoxOnClick(this)'><label for='f20'>".$i_Discipline_System_Access_Right_Approval."</label></td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td21'><input name='MGMT_GoodConduct_Misconduct_Approval' type='checkbox' id='f21' value='Approval' onclick='checkBoxOnClick(this)'><label for='f21'>".$i_Discipline_System_Access_Right_Approval."</label></td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td47'><input name='MGMT_Case_Record_Finish' type='checkbox' id='f47' value='Finish' onclick='checkBoxOnClick(this)'><label for='f47'>".$i_Discipline_System_Access_Right_Finish."</label></td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td22'>";
$tableMGMT .= "<!--<input name='MGMT_Award_Punishment_Lock' type='checkbox' id='f22' value='Lock' onclick='checkBoxOnClick(this)'> <label for='f22'>".$i_Discipline_System_Access_Right_Lock."</label>--></td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td23'><!--<input name='MGMT_Case_Record_Lock' type='checkbox' id='f23' value='Lock' onclick='checkBoxOnClick(this)'><label for='f23'>".$i_Discipline_System_Access_Right_Lock."</label>--></td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td24'><input name='MGMT_Conduct_Mark_Adjust' type='checkbox' id='f24' value='Adjust' onclick='checkBoxOnClick(this)'><label for='f24'>".$i_Discipline_System_Access_Right_Adjust."</label></td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td25'><input name='MGMT_Detention_Take_Attendance' type='checkbox' id='f25' value='TakeAttendance' onclick='checkBoxOnClick(this)'><label for='f25'>".$i_Discipline_System_Access_Right_Take_Attendance."</label></td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td43'><input name='SETTINGS_Detention_Access' type='checkbox' id='f43' value='Access' onclick='checkBoxOnClick(this)'><label for='f43'>".$i_Discipline_System_Access_Right_Access."</label></td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td26'><!--<input name='MGMT_Case_Record_NewNotes' type='checkbox' id='f26' value='NewNotes' onclick='checkBoxOnClick(this)'><label for='f26'>".$i_Discipline_System_Access_Right_New_Notes."</label>--></td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td27'><input name='MGMT_Detention_RearrangeStudent' type='checkbox' id='f27' value='RearrangeStudent' onclick='checkBoxOnClick(this)'><label for='f27'>".$i_Discipline_System_Access_Right_Rearrange_Student."</label></td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><!--<tr class='tablerow2'>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td28'><input name='MGMT_Case_Record_EditNotes' type='checkbox' id='f28' value='EditNotes' onclick='if(this.checked==false){form1.f28_OwnAll[0].checked=false; form1.f28_OwnAll[1].checked=false;} else{form1.f28_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f28'>".$i_Discipline_System_Access_Right_Edit_Notes."</label><br>";
$tableMGMT .= "<input name='f28_OwnAll' type='radio' id='f28_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_EditNotes.checked=true;checkBoxOnClick(this)'><label for='f28_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f28_OwnAll' type='radio' id='f28_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_EditNotes.checked=true;checkBoxOnClick(this)'><label for='f28_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top' class='tabletextremark' id='td29'><input name='MGMT_Case_Record_DeleteNotes' type='checkbox' id='f29' value='DeleteNotes' onclick='if(this.checked==false){form1.f29_OwnAll[0].checked=false; form1.f29_OwnAll[1].checked=false;} else{form1.f29_OwnAll[0].checked=true;};checkBoxOnClick(this)'><label for='f29'>".$i_Discipline_System_Access_Right_Delete_Notes."</label><br>";
$tableMGMT .= "<input name='f29_OwnAll' type='radio' id='f29_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_DeleteNotes.checked=true;checkBoxOnClick(this)'><label for='f29_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f29_OwnAll' type='radio' id='f29_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_DeleteNotes.checked=true;checkBoxOnClick(this)'><label for='f29_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr>-->";
$tableMGMT .= "</table>";

$tableSTAT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSTAT .= "<tr class='tabletop'>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSTAT .= "</tr><tr class='tablerow1'>";
$tableSTAT .= "<td class='tabletextremark' id='td30'><input type='checkbox' name='STAT_Award_Punishment_View' id='f30' value='View' onclick='checkBoxOnClick(this)'><label for='f30'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableSTAT .= "<td class='tabletextremark' id='td31'><input type='checkbox' name='STAT_GoodConduct_Misconduct_View' id='f31' value='View' onclick='checkBoxOnClick(this)'><label for='f31'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableSTAT .= "<td class='tabletextremark' id='td32'><input type='checkbox' name='STAT_Conduct_Mark_View' id='f32' value='View' onclick='checkBoxOnClick(this)'><label for='f32'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableSTAT .= "<td class='tabletextremark' id='td33'><input type='checkbox' name='STAT_Detention_View' id='f33' value='View' onclick='checkBoxOnClick(this)'><label for='f33'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableSTAT .= "</tr></table>";

$tableREPORTS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableREPORTS .= "<tr class='tabletop'>";
$tableREPORTS .= "<td>".$i_Discipline_System_Report_Type_Personal." / ".$eDiscipline['StudentReport']."</td>";
$tableREPORTS .= "<td>".$eDiscipline['RankingReport']."</td>";
$tableREPORTS .= "<td>".$eDiscipline['ClassSummary']."</td>";
$tableREPORTS .= "<td>".$eDiscipline['MonthlyReport']."</td>";
$tableREPORTS .= "</tr><tr class='tablerow1'>";
$tableREPORTS .= "<td class='tabletextremark' id='td36'><input type='checkbox' name='REPORTS_StudentReport_View' id='f36' value='View' onclick='checkBoxOnClick(this)'><label for='f36'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableREPORTS .= "<td class='tabletextremark' id='td37'><input type='checkbox' name='REPORTS_RankingReport_View' id='f37' value='View' onclick='checkBoxOnClick(this)'><label for='f37'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableREPORTS .= "<td class='tabletextremark' id='td38'><input type='checkbox' name='REPORTS_ClassSummary_View' id='f38' value='View' onclick='checkBoxOnClick(this)'><label for='f38'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableREPORTS .= "<td class='tabletextremark' id='td39'><input type='checkbox' name='REPORTS_MonthlyReport_View' id='f39' value='View' onclick='checkBoxOnClick(this)'><label for='f39'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableREPORTS .= "</tr></table>";

$tableSETTINGS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSETTINGS .= "<tr class='tabletop'>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
//$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_eNotice_Template."</td>";
//$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Letter."</td>";
$tableSETTINGS .= "</tr><tr class='tablerow1'>";
$tableSETTINGS .= "<td class='tabletextremark' id='td40'><input type='checkbox' name='SETTINGS_Award_Punishment_Access' id='f40' value='Access' onclick='checkBoxOnClick(this)'><label for='f40'>".$i_Discipline_System_Access_Right_Access."</label></td>";
$tableSETTINGS .= "<td class='tabletextremark' id='td41'><input type='checkbox' name='SETTINGS_GoodConduct_Misconduct_Access' id='f41' value='Access' onclick='checkBoxOnClick(this)'><label for='f41'>".$i_Discipline_System_Access_Right_Access."</label></td>";
$tableSETTINGS .= "<td class='tabletextremark' id='td42'><input type='checkbox' name='SETTINGS_Conduct_Mark_Access' id='f42' value='Access' onclick='checkBoxOnClick(this)'><label for='f42'>".$i_Discipline_System_Access_Right_Access."</label></td>";
//$tableSETTINGS .= "<td class='tabletextremark' id='td43'><input type='checkbox' name='SETTINGS_Detention_Access' id='f43' value='Access' onclick='checkBoxOnClick(this)'><label for='f43'>".$i_Discipline_System_Access_Right_Access."</label></td>";
$tableSETTINGS .= "<td class='tabletextremark' id='td44'><input type='checkbox' name='SETTINGS_eNoticeTemplate_Access' id='f44' value='Access' onclick='checkBoxOnClick(this)'><label for='f44'>".$i_Discipline_System_Access_Right_Access."</label></td>";
//$tableSETTINGS .= "<td class='tabletextremark' id='td45'><input type='checkbox' name='SETTINGS_Letter_Access' id='f45' value='Access' onclick='checkBoxOnClick(this)'><label for='f45'>".$i_Discipline_System_Access_Right_Access."</label></td>";
$tableSETTINGS .= "</tr></table>";

#####

# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 1);
//$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
//$TAGS_OBJ[]$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_Group_List, "group_access_right.php");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_New_Group, "");

?>

<br />
<script language="javascript">
<!--
var CatArr = new Array();
CatArr[0] = 'MGMT_Award_Punishment';
CatArr[1] = 'MGMT_GoodConduct_Misconduct';
CatArr[2] = 'MGMT_Case_Record';
CatArr[3] = 'MGMT_Conduct_Mark';
CatArr[4] = 'MGMT_Detention';

var CatTaskArr = new Array();
CatTaskArr[0] = '_New';
CatTaskArr[1] = '_Edit';
CatTaskArr[2] = '_Delete';
CatTaskArr[3] = '_Release';
CatTaskArr[4] = '_Finish';
CatTaskArr[5] = '_Waive';
CatTaskArr[6] = '_Approval';
CatTaskArr[7] = '_Adjust';
CatTaskArr[8] = '_Take_Attendance';
CatTaskArr[9] = '_RearrangeStudent';
CatTaskArr[10] = '_NewLeaf';

var radioArr = new Array();
radioArr[0] = 'f10_OwnAll[0]';
radioArr[1] = 'f10_OwnAll[1]';
radioArr[2] = 'f11_OwnAll[0]';
radioArr[3] = 'f11_OwnAll[1]';
radioArr[4] = 'f12_OwnAll[0]';
radioArr[5] = 'f12_OwnAll[1]';
radioArr[6] = 'f13_OwnAll[0]';
radioArr[7] = 'f13_OwnAll[1]';
radioArr[8] = 'f14_OwnAll[0]';
radioArr[9] = 'f14_OwnAll[1]';
radioArr[10] = 'f15_OwnAll[0]';
radioArr[11] = 'f15_OwnAll[1]';
radioArr[12] = 'f16_OwnAll[0]';
radioArr[13] = 'f16_OwnAll[1]';
radioArr[14] = 'f17_OwnAll[0]';
radioArr[15] = 'f17_OwnAll[1]';

function initialDisableCheckbox()
{
	//disable all radio buttons expect those that were checked
	for(a=0;a<radioArr.length;a++)
	{
		obj = document.getElementById(radioArr[a]);
		if(obj)
		{
			if(obj.checked==false)
			{
				if(radioArr[a+1]==obj.id.substring(0,10)+'[1]' && document.getElementById(obj.id.substring(0,10)+'[1]').checked==true)
				{
					obj.disabled=false;
				}
				else if(radioArr[a-1]==obj.id.substring(0,10)+'[0]' && document.getElementById(obj.id.substring(0,10)+'[0]').checked==true)
				{
					obj.disabled=false;
				}
				else
				{
					obj.disabled=true;
				}
			}
		}
	}
	//disable all checkboxes expect those that were checked
	for(a=0;a<CatArr.length;a++)
	{
		for(b=0;b<CatTaskArr.length;b++)
		{
			name = CatArr[a]+CatTaskArr[b];
			//alert(name);
			obj = document.getElementById(name);
			if(obj)
			{
				if(obj.checked==false)
				{
					obj.disabled=true;
				}
			}
		}
	}
	
}

function checkBoxOnClick(elementRef)
{
	elementRef.parentNode.className = (elementRef.checked) ? 'tablegreenrow3' : 'tabletextremark';
	var PostFixLength = elementRef.name.length-5
	var AccessType = elementRef.name.substring(0,PostFixLength);
	
	PostFix = elementRef.name.substring(PostFixLength);
	if(PostFix=='_View')
	{
		if (elementRef.checked==false)
		{
			//disable and uncheck checkboxes
			for(a=0;a<CatTaskArr.length;a++)
			{
				name = AccessType+CatTaskArr[a];
				obj = document.getElementById(name);
				if(obj)
				{
					obj.disabled=true;
					obj.checked=false;
					obj.parentNode.className = 'tabletextremark';
				}
			}
			
			//disable own radio buttons
			switch(AccessType)
			{
				case CatArr[0]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[0]);
				objArr[1] = document.getElementById(radioArr[1]);
				objArr[2] = document.getElementById(radioArr[8]);
				objArr[3] = document.getElementById(radioArr[9]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
				case CatArr[1]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[2]);
				objArr[1] = document.getElementById(radioArr[3]);
				objArr[2] = document.getElementById(radioArr[10]);
				objArr[3] = document.getElementById(radioArr[11]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
				case CatArr[2]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[4]);
				objArr[1] = document.getElementById(radioArr[5]);
				objArr[2] = document.getElementById(radioArr[12]);
				objArr[3] = document.getElementById(radioArr[13]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
				case CatArr[4]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[6]);
				objArr[1] = document.getElementById(radioArr[7]);
				objArr[2] = document.getElementById(radioArr[14]);
				objArr[3] = document.getElementById(radioArr[15]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
				
				
			}

		}
		else if (elementRef.checked==true)
		{
			//enable own checkboxes
			for(a=0;a<CatTaskArr.length;a++)
			{
				name = AccessType+CatTaskArr[a];
				obj = document.getElementById(name);
				if(obj)
				{
					obj.disabled=false;
				}
			}
			//enable own radio buttons
			switch(AccessType)
			{
				case CatArr[0]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[0]);
				objArr[1] = document.getElementById(radioArr[1]);
				objArr[2] = document.getElementById(radioArr[8]);
				objArr[3] = document.getElementById(radioArr[9]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
				case CatArr[1]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[2]);
				objArr[1] = document.getElementById(radioArr[3]);
				objArr[2] = document.getElementById(radioArr[10]);
				objArr[3] = document.getElementById(radioArr[11]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
				case CatArr[2]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[4]);
				objArr[1] = document.getElementById(radioArr[5]);
				objArr[2] = document.getElementById(radioArr[12]);
				objArr[3] = document.getElementById(radioArr[13]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
				case CatArr[4]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[6]);
				objArr[1] = document.getElementById(radioArr[7]);
				objArr[2] = document.getElementById(radioArr[14]);
				objArr[3] = document.getElementById(radioArr[15]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
				
				
			}
		}
	}
}

function resetTD() {
	var tdName = "";
	for(var i=1;i<=48;i++) {
		tdName = eval("document.getElementById('td" + i + "')");
		if(tdName != null) {
			tdName.className = 'tabletextremark';
		}
	}	
}

function checkForm(form1) {
	if(form1.GroupTitle.value=='')	 {
		alert("<?= $i_Discipline_System_alert_require_group_title ?>");	
		form1.GroupTitle.focus();
		return false;
	} else {
		return true;
	}
}
//-->
</script>
<form name="form1" method="post" action="group_access_right_new_update.php" onSubmit="return checkForm(this)">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>									
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="100%" cellpadding="5" cellspacing="0">
							<tr>
								<td width="60%"><?=$linterface->GET_NAVIGATION2($i_Discipline_System_Group_Right_Navigation_Group_Info);?></td>
								<td width="40%" align="right"><?=$linterface->GET_SYS_MSG('',$xmsg2) ?></td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Title?><span class="tabletextrequire">*</span></td>
								<td><input name="GroupTitle" type="text" class="textboxtext" value=""></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Description?></td>
								<td class="formfieldtitle"><?=$linterface->GET_TEXTAREA('GroupDescription', "");?></td>
							</tr>
						</table>
						<table width="88%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><?=$linterface->GET_NAVIGATION2($i_Discipline_System_Group_Right_Navigation_Group_Access_Right);?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Management ?></i>
								<?= $tableMGMT ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?= $i_Discipline_System_Teacher_Right_Statistics ?></i>
								<?= $tableSTAT ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Reports ?></i>
								<?= $tableREPORTS ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Settings ?></i>
								<?= $tableSETTINGS ?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field ?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="center">
					<table width="88%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center">
								<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_reset, "reset","javascript:resetTD()")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='group_access_right.php'")?>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<script>
initialDisableCheckbox();
</script>
</form>

<?php
echo $linterface->FOCUS_ON_LOAD("form1.GroupTitle"); 

$linterface->LAYOUT_STOP();
intranet_closedb();

?>