<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$GroupID = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('T','');
$show = 0;
$showTDContent = array();
$showTDContent[0][0] = "tabletextremark";
$showTDContent[0][1] = "";
$showTDContent[1][0] = "tablegreenrow3";
$showTDContent[1][1] = " checked";
$RestoreTD = array();

### TABLE CONTENT ###
$tableMGMT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableMGMT .= "<tr class='tabletop'>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
#$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Case_Record."</td>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableMGMT .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

// Return Function & Action
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[0]=1;";} else { $show = 0;  $restoreField .= "arrayInt[0]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td1'><input type='checkbox' name='MGMT_Award_Punishment_View' value='View' id='f1' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f1'> ".$i_Discipline_System_Access_Right_View."</label> </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[1]=1;";} else { $show = 0;  $restoreField .= "arrayInt[1]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td2'><input type='checkbox' name='MGMT_GoodConduct_Misconduct_View' value='View' id='f2' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f2'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[3]=1;";} else { $show = 0;  $restoreField .= "arrayInt[3]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td4'><input type='checkbox' name='MGMT_Conduct_Mark_View' value='View' id='f4' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f4'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[4]=1;";} else { $show = 0;  $restoreField .= "arrayInt[4]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td5'><input type='checkbox' name='MGMT_Detention_View' value='View' id='f5' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f5'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','New','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[5]=1;"; } else { $show = 0;  $restoreField .= "arrayInt[5]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td6'><input type='checkbox' name='MGMT_Award_Punishment_New' value='New' id='f6' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f6'> ".$i_Discipline_System_Access_Right_New."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','New','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[6]=1;";} else { $show = 0;  $restoreField .= "arrayInt[6]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td7'><input type='checkbox' name='MGMT_GoodConduct_Misconduct_New' value='New' id='f7' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f7'> ".$i_Discipline_System_Access_Right_New."</label> </td>";

$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[8]=1;";} else { $show = 0;  $restoreField .= "arrayInt[8]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td9'><input type='checkbox' name='MGMT_Detention_Edit' value='Edit' id='f9' onclick='if(this.checked==false){form1.f9_OwnAll[0].checked=false;form1.f9_OwnAll[1].checked=false;} else {form1.f9_OwnAll[0].checked=true;} ;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll' || $tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .="> <label for='f9'>".$i_Discipline_System_Access_Right_Edit."</label><br> <input type='radio' name='f9_OwnAll' id='f9_OwnAll[0]' value='Own' onclick='if(this.checked==true){form1.MGMT_Detention_Edit.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f9_OwnAll[0]'> ".$i_Discipline_System_Access_Right_Own." </label><input type='radio' name='f9_OwnAll' id='f9_OwnAll[1]' value='All' onclick='if(this.checked==true){form1.MGMT_Detention_Edit.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f9_OwnAll[1]'> ".$i_Discipline_System_Access_Right_All." </label></td>";

$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[9]=1;";} else { $show = 0;  $restoreField .= "arrayInt[9]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td10'><input type='checkbox' name='MGMT_Award_Punishment_Edit' value='Edit' id='f10' onclick='if(this.checked==false){form1.f10_OwnAll[0].checked=false;form1.f10_OwnAll[1].checked=false;} else {form1.f10_OwnAll[0].checked=true;} ;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll' || $tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .="> <label for='f10'>".$i_Discipline_System_Access_Right_Edit."</label><br> <input type='radio' name='f10_OwnAll' id='f10_OwnAll[0]' value='Own' onclick='if(this.checked==true){form1.MGMT_Award_Punishment_Edit.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f10_OwnAll[0]'> ".$i_Discipline_System_Access_Right_Own." </label><input type='radio' name='f10_OwnAll' id='f10_OwnAll[1]' value='All' onclick='if(this.checked==true){form1.MGMT_Award_Punishment_Edit.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f10_OwnAll[1]'> ".$i_Discipline_System_Access_Right_All." </label></td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[10]=1;";} else { $show = 0;  $restoreField .= "arrayInt[10]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td11'><input type='checkbox' name='MGMT_GoodConduct_Misconduct_Edit' value='Edit' id='f11' onclick='if(this.checked==false){form1.f11_OwnAll[0].checked=false;form1.f11_OwnAll[1].checked=false;} else {form1.f11_OwnAll[0].checked=true;} ;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll' || $tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .="> <label for='f11'>".$i_Discipline_System_Access_Right_Edit."</label><br> <input type='radio' name='f11_OwnAll' id='f11_OwnAll[0]' value='Own' onclick='if(this.checked==true){form1.MGMT_GoodConduct_Misconduct_Edit.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f11_OwnAll[0]'> ".$i_Discipline_System_Access_Right_Own." </label><input type='radio' name='f11_OwnAll' id='f11_OwnAll[1]' value='All' onclick='if(this.checked==true){form1.MGMT_GoodConduct_Misconduct_Edit.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f11_OwnAll[1]'> ".$i_Discipline_System_Access_Right_All." </label></td>";

$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','New','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[12]=1;";} else { $show = 0;  $restoreField .= "arrayInt[12]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td13'><input type='checkbox' name='MGMT_Detention_New' value='New' id='f13' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f13'> ".$i_Discipline_System_Access_Right_New."</label> </td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[13]=1;";} else { $show = 0;  $restoreField .= "arrayInt[13]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td14'><input type='checkbox' name='MGMT_Award_Punishment_Delete' value='Delete' id='f14' onclick='if(this.checked==false){form1.f14_OwnAll[0].checked=false;form1.f14_OwnAll[1].checked=false;} else {form1.f14_OwnAll[0].checked=true;} ;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll' || $tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked";}
$tableMGMT .="> <label for='f14'>".$i_Discipline_System_Access_Right_Delete."</label><br> <input type='radio' name='f14_OwnAll' id='f14_OwnAll[0]' value='Own' onclick='if(this.checked==true){form1.MGMT_Award_Punishment_Delete.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f14_OwnAll[0]'> ".$i_Discipline_System_Access_Right_Own." </label><input type='radio' name='f14_OwnAll' id='f14_OwnAll[1]' value='All' onclick='if(this.checked==true){form1.MGMT_Award_Punishment_Delete.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f14_OwnAll[1]'> ".$i_Discipline_System_Access_Right_All." </label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[14]=1;"; } else { $show = 0;  $restoreField .= "arrayInt[14]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td15'><input type='checkbox' name='MGMT_GoodConduct_Misconduct_Delete' value='Delete' id='f15' onclick='if(this.checked==false){form1.f15_OwnAll[0].checked=false;form1.f15_OwnAll[1].checked=false;} else {form1.f15_OwnAll[0].checked=true;} ;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll' || $tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked";}
$tableMGMT .="> <label for='f15'>".$i_Discipline_System_Access_Right_Delete."</label><br> <input type='radio' name='f15_OwnAll' id='f15_OwnAll[0]' value='Own' onclick='if(this.checked==true){form1.MGMT_GoodConduct_Misconduct_Delete.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f15_OwnAll[0]'> ".$i_Discipline_System_Access_Right_Own." </label><input type='radio' name='f15_OwnAll' id='f15_OwnAll[1]' value='All' onclick='if(this.checked==true){form1.MGMT_GoodConduct_Misconduct_Delete.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f15_OwnAll[1]'> ".$i_Discipline_System_Access_Right_All." </label></td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Waive','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[16]=1;";} else { $show = 0;  $restoreField .= "arrayInt[16]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td17'><input type='checkbox' name='MGMT_Award_Punishment_Waive' value='Waive' id='f17' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f17'> ".$i_Discipline_System_Access_Right_Waive."</label> </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','Waive','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[39]=1;";} else { $show = 0;  $restoreField .= "arrayInt[39]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td40'><input type='checkbox' name='MGMT_GoodConduct_Misconduct_Waive' value='Waive' id='f40' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f40'> ".$i_Discipline_System_Access_Right_Waive."</label> </td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[17]=1;";} else { $show = 0;  $restoreField .= "arrayInt[17]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td18'><input type='checkbox' name='MGMT_Detention_Delete' value='Delete' id='f18' onclick='if(this.checked==false){form1.f18_OwnAll[0].checked=false;form1.f18_OwnAll[1].checked=false;} else {form1.f18_OwnAll[0].checked=true;} ;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll' || $tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked";}
$tableMGMT .="> <label for='f18'>".$i_Discipline_System_Access_Right_Delete."</label><br> <input type='radio' name='f18_OwnAll' id='f18_OwnAll[0]' value='Own' onclick='if(this.checked==true){form1.MGMT_Detention_Delete.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f18_OwnAll[0]'> ".$i_Discipline_System_Access_Right_Own." </label><input type='radio' name='f18_OwnAll' id='f18_OwnAll[1]' value='All' onclick='if(this.checked==true){form1.MGMT_Detention_Delete.checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f18_OwnAll[1]'> ".$i_Discipline_System_Access_Right_All." </label></td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Release','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[18]=1;";} else { $show = 0;  $restoreField .= "arrayInt[18]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td19'><input type='checkbox' name='MGMT_Award_Punishment_Release' value='Release' id='f19' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f19'> ".$i_Discipline_System_Access_Right_Release."</label></td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Approval','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[19]=1;";} else { $show = 0;  $restoreField .= "arrayInt[19]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td20'><input type='checkbox' name='MGMT_Award_Punishment_Approval' value='Approval' id='f20' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f20'> ".$i_Discipline_System_Access_Right_Approval."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','Approval','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[20]=1;";} else { $show = 0;  $restoreField .= "arrayInt[20]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td21'><input type='checkbox' name='MGMT_GoodConduct_Misconduct_Approval' value='Approval' id='f21' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f21'> ".$i_Discipline_System_Access_Right_Approval."</label></td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Lock','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[21]=1;";} else { $show = 0;  $restoreField .= "arrayInt[21]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td22'><input type='checkbox' name='MGMT_Award_Punishment_Lock' value='Lock' id='f22' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f22'> ".$i_Discipline_System_Access_Right_Lock."</label></td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Conduct_Mark','Adjust','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[23]=1;";} else { $show = 0;  $restoreField .= "arrayInt[23]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td24'><input type='checkbox' name='MGMT_Conduct_Mark_Adjust' value='Adjust' id='f24' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f24'> ".$i_Discipline_System_Access_Right_Adjust."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','TakeAttendance','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[24]=1;";} else { $show = 0;  $restoreField .= "arrayInt[24]=0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td25'><input type='checkbox' name='MGMT_Detention_Take_Attendance' value='TakeAttendance' id='f25' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f25'> ".$i_Discipline_System_Access_Right_Take_Attendance."</label></td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tableMGMT .= "<td>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','RearrangeStudent','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[26]=1;";} else { $show = 0;  $restoreField .= "arrayInt[26]=0;"; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td27'><input type='checkbox' name='MGMT_Detention_RearrangeStudent' value='RearrangeStudent' id='f27' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f27'> ".$i_Discipline_System_Access_Right_Rearrange_Student."</label></td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";

$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "<td>&nbsp;</td>";
$tableMGMT .= "</tr></table>";

$tableSTAT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSTAT .= "<tr class='tabletop'>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSTAT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[29]=1;";} else { $show = 0;  $restoreField .= "arrayInt[29]=0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td30'><input type='checkbox' name='STAT_Award_Punishment_View' value='View' id='f30' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f30'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[30]=1;";} else { $show = 0;  $restoreField .= "arrayInt[30]=0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td31'><input type='checkbox' name='STAT_GoodConduct_Misconduct_View' value='View' id='f31' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f31'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[31]=1;";} else { $show = 0;  $restoreField .= "arrayInt[31]=0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td32'><input type='checkbox' name='STAT_Conduct_Mark_View' value='View' id='f32' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f32'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[32]=1;";} else { $show = 0;  $restoreField .= "arrayInt[32]=0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td33'><input type='checkbox' name='STAT_Detention_View' value='View' id='f33' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f33'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tableSTAT .= "</tr></table>";

$tableREPORTS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableREPORTS .= "<tr class='tabletop'>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Top10."</td>";
$tableREPORTS .= "</tr><tr class='tablerow1'>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[33]=1;";} else { $show = 0;  $restoreField .= "arrayInt[33]=0;"; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td34'><input type='checkbox' name='REPORTS_Award_Punishment_View' value='View' id='f34' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f34'> ".$i_Discipline_System_Access_Right_View."</label> </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[34]=1;";} else { $show = 0;  $restoreField .= "arrayInt[34]=0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td35'><input type='checkbox' name='REPORTS_GoodConduct_Misconduct_View' value='View' id='f35' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f35'> ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[36]=1;";} else { $show = 0;  $restoreField .= "arrayInt[36]=0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td37'><input type='checkbox' name='REPORTS_Conduct_Mark_View' value='View' id='f37' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f37'> ".$i_Discipline_System_Access_Right_View."</label> </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Detention','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[37]=1;";} else { $show = 0;  $restoreField .= "arrayInt[37]=0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td38'><input type='checkbox' name='REPORTS_Detention_View' value='View' id='f38' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f38'> ".$i_Discipline_System_Access_Right_View."</label> </td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Top10','View','');
if($tempResult[0][1]!='') { $show = 1;  $restoreField .= "arrayInt[38]=1;";} else { $show = 0;  $restoreField .= "arrayInt[38]=0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td39'><input type='checkbox' name='REPORTS_Top10_View' value='View' id='f39' onclick='checkBoxOnClick(this)'".$showTDContent[$show][1]."><label for='f39'> ".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableREPORTS .= "</tr></table>";


$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Discipline_System_Teacher_Right_Navigation_Edit_Right, "");

?>

<script language="javascript">
function checkBoxOnClick(elementRef)
{
	elementRef.parentNode.className = (elementRef.checked) ? 'tablegreenrow3' : 'tabletextremark';
}

function resetField() {
	var tdClassName = "";
	var arrayInt = new Array();
	<?= $restoreField ?>
	var tt = "";
	for(var i=1;i<=45;i++) {
		tdClassName = eval("document.getElementById('td" + i + "')");
		if(tdClassName != null) {
			if(arrayInt[i-1]==1) {
				tdClassName.className = 'tablegreenrow3';	
			} else {
				tdClassName.className = 'tabletextremark';	
			}
		}
	}
}
</script>
<br />
<form name="form1" method="post" action="teacher_access_right_edit_update.php">
<table width="95%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table width="88%" border="0" cellspacing="0" cellpadding="5"><tr><td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="form_sep_title"> <i><?= $i_Discipline_System_Teacher_Right_Management ?></i>
				<?= $tableMGMT ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="form_sep_title"><i> <?= $i_Discipline_System_Teacher_Right_Statistics ?></i>
				<?= $tableSTAT ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="form_sep_title"><i><?= $i_Discipline_System_Teacher_Right_Reports ?></i>
				<?= $tableREPORTS ?>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>
				<table width="88%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center">
							<?= $linterface->GET_ACTION_BTN($button_save, "button", "form1.submit()")?>&nbsp;<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "javascript:resetField()")?>&nbsp;<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:location.href='teacher_access_right.php'")?></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
</td></tr></table>
</form>


<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>