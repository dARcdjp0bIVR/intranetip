<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$ldiscipline->DELETE_ACCESS_RIGHT_MEMBER($GroupID, 'A', $userID); 

header("Location: group_access_right_member_list.php?xmsg=delete&GroupID=$GroupID&msg=3");
intranet_closedb();
?>