<?php
# using: 

###############################
#
#	Date:	2018-01-05	Bill	[2017-1030-1357-13206]
#			Add default template selection
#
###############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$new = $reminderPoint;
$status = $ldiscipline->editStudyScoreWarningRule("add", "", $new, $SelectNotice); 

intranet_closedb();

if($status==false) {	# record exists 
	$url = "warning_rule_add.php?xmsg2=$i_Discipline_System_alert_warning_level_already_exists";	
}
else {
	$url = "warning_rule.php?xmsg=add";
}

header("Location: $url");
?>