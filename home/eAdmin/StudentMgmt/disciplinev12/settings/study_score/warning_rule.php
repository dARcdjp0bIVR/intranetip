<?php
# using: 

#############################################
#
#	Date:	2018-01-05	Bill	[2017-1030-1357-13206]
#			Add default template selection
#
#	Date:	2017-10-13 (Bill)	[DM#3273]
#			fixed Session Problem in PHP 5.4
#
#	Date:	2017-01-20	Bill	[2015-0120-1200-33164]
#			Update Tab wording
#
#	Date:	2016-07-11	Bill
#			change split() to explode(), for PHP 5.4
#
#	Date:	2012-11-09	YatWoon
#			Add "Semester Ratio" tab
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
}
else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
}
else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."/includes/libnotice.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lnotice = new libnotice();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Current Page
$CurrentPage = "Settings_StudyScore";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 1);
$TAGS_OBJ[] = array($Lang['eDiscipline']['SubScoreBaseMark'], "base_mark.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

$sql = "SELECT
				a.InitialScore,
				IFNULL(CONCAT(b.CategoryID, ' > ', b.Title), '<i>".$Lang['eDiscipline']['UseDefaultSettings']."</i>'),
				CONCAT('<input type=\'checkbox\' name=\'PointID[]\' id=\'PointID[]\' value=\'', a.InitialScore ,'\'>')
    	FROM
				DISCIPLINE_SUBSCORE_SETTING as a
				LEFT JOIN INTRANET_NOTICE_MODULE_TEMPLATE as b ON (b.TemplateID = a.TemplateID)
		WHERE
				a.RecordType = 1 ";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("InitialScore");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0);
$li->wrap_array = array(0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list = "<td width='3%' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='46%'>".$i_Discipline_System_Conduct_Warning_Point."</td>\n";
$li->column_list .= "<td width='50%'>".$i_Discipline_Template."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("PointID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("warning_rule_add.php", $button_new, "", "", "", 0);

if($xmsg != '')
{
	$showmsg = "<table width=95% cellspacing=0 cellpadding=2 border=0>";	
	$showmsg .= "<tr><td align=right>";
	$showmsg .= $linterface->GET_SYS_MSG($xmsg);
	$showmsg .= "</td></tr>";
	$showmsg .= "</table>";
}

########## For eNotice (Begin) ##########
# Check Notice Template is ava
$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("", 1);
if (!$lnotice->disabled && !empty($NoticeTemplateAva))
{
		$catTmpArr = $ldiscipline->TemplateCategory();
		if(is_array($catTmpArr))
		{
			$catArr[0] = array('0', "--".$button_select_situation."--");
			foreach($catTmpArr as $Key => $Value)
			{
				# Check Notice Template Catgory has template or not
				$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
				if(!empty($NoticeTemplateAvaTemp)) {
					$catArr[] = array($Key, $Key);
				}
			}
		}
		
		$template = $ldiscipline->getDisciplineGeneralSetting("StudyScoreWarningRuleTemplate");
		$line = explode("::",$template);
		list ($eNoticeCategory, $eNoticeSubCategory, $additionInfo) = $line;
		$eNoticeSubCategory = ($eNoticeSubCategory == '') ? '0' : $eNoticeSubCategory;
		
		//$eNoticeCategory = "Warning";
		//$eNoticeSubCategory = "10";
		
		$catSelection0 = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID0", name="CategoryID0" onChange="changeCat(this.value, 0)"', "", $eNoticeCategory);
		//$SelectNotice0 = $linterface->GET_SELECTION_BOX();
		
		// Preset template items (for javascript)
		for ($i=0; $i<sizeof($catArr); $i++)
		{
			$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
			array_splice($result, 0, 0, array(array(0=>'0', 1=>"--".$button_select_template."--")));
			for ($j=0; $j<sizeof($result); $j++)
			{
				if($catArr[$i][0]==$eNoticeCategory) {
					$optionSubCategory .= "<option value='{$result[$j][0]}'";
					$optionSubCategory .= ($eNoticeSubCategory==$result[$j][0]) ? " selected" : "";
					$optionSubCategory .= ">{$result[$j][1]}</option>";
				}
				
				if ($j==0) {
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".sizeof($result).");\n";
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".sizeof($result).");\n";
					$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Selected = new Array(".sizeof($result).");\n";
				}
				
				$tempTemplate = $result[$j][1];
				$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
		        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
		        $tempTemplate=str_replace("&#039;", "'", $tempTemplate);
		        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
			    
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j][0]."\"\n";
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
				$jTemplateString .= ($eNoticeSubCategory==$result[$j][0]) ? ("jArrayTemplate".$catArr[$i][0]."Selected[$j] = \"1\"\n") : ("jArrayTemplate".$catArr[$i][0]."Selected[$j] = \"0\"\n");
			}
		}
}
########## For eNotice (End) ##########

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
<?=$jTemplateString?>
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0) {
            alert(globalAlertMsg2);
        }
        else{
            if(confirm(alertConfirmRemove)){
                obj.action=page;
                obj.method="post";
                obj.submit();
            }
		}
}

function changeCat(cat, selectIdx){
	<? if (!$lnotice->disabled && !empty($NoticeTemplateAva)) {?>
	var x = document.getElementById("SelectNotice"+selectIdx);
	var listLength = x.length;
	for (var i = listLength-1; i>=0; i--) {
		x.remove(i);
	}
	
	if (cat != 0) {
		for (var j=0; j<eval("jArrayTemplate"+cat).length; j++) {
			var y = document.createElement('option');
			y.text = eval("jArrayTemplate"+cat)[j];
			y.value = eval("jArrayTemplate"+cat+"Value")[j];
			if(<?=$eNoticeSubCategory?>==y.value) {
				y.selected = "selected";
			}

			try {
				x.add(y,null); // standards compliant
			}
			catch(ex) {
				x.add(y); // IE only
			}
		}
	}
	<? } ?>
}

function checkForm(form1) 
{
	<? if (!$lnotice->disabled && !empty($NoticeTemplateAva)) { ?>
	if(form1.CategoryID0.value==0) {
		alert("<?=$i_alert_pleaseselect?>");	
		form1.CategoryID0.focus();
		return false;
	}
	else if(form1.SelectNotice0.value==0) {
		alert("<?=$i_alert_pleaseselect?>");	
		form1.SelectNotice0.focus();
		return false;
	}
	else {
		form1.action = "warning_rule_eNotice_update.php";	
		form1.submit();
	}
	<? }
	else { ?>
	form1.action = "warning_rule_eNotice_update.php";	
	form1.submit();
	<? } ?>
}

function goreset() {
	form1.reset();
	<? if (!$lnotice->disabled && !empty($NoticeTemplateAva)) {?>
	changeCat(document.form1.CategoryID0.value, 0)	
	<? } ?>
}
//-->
</script>

<form name="form1" method="post" action="" onSubmit="return checkForm(this)">
<br />
<table width="95%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="70%"><?=$toolbar?></td>
								<td width="30%" align="right"><?=$showmsg ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="right">
								</td>
							</tr>
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>&nbsp;</td>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap><a href="javascript:checkEdit(document.form1,'PointID[]','warning_rule_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap><a href="javascript:removeCat(document.form1,'PointID[]','warning_rule_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<?= $li->display();?>
					</td>
				</tr>
				<? if (!$lnotice->disabled && !empty($NoticeTemplateAva)) {?>
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td colspan="2" valign="top" nowrap="nowrap">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="sectiontitle"><img src="<?="$image_path/$LAYOUT_SKIN"?>/icon_section.gif" width="20" height="20" align="absmiddle"><?=$eDiscipline['eNoticeTemplate']?></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr valign="top">
								<td width="20%" nowrap="nowrap" class="tabletext"><?=$i_Discipline_Template?> <span class="tabletextrequire">*</span></td>
								<td>
									<?=$catSelection0?> 
									<select name="SelectNotice0" id="SelectNotice0"><?=$optionSubCategory?></select>
								</td>
							</tr>
							<tr>
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_Additional_Info?></td>
								<td valign="top"><?=$linterface->GET_TEXTAREA('TextAdditionalInfo0', $additionInfo);?></td>
							</tr>
							<tr>
								<td colspan="2" valign="top" align="center">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="2" valign="top" nowrap="nowrap" align="center">
									<?=$linterface->GET_ACTION_BTN($button_submit, "submit", "")?>
									<?=$linterface->GET_ACTION_BTN($button_reset, "button", "javascript:goreset()")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<? } ?>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>