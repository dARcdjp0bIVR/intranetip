<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$semester_data = getSemesters($yearID);

$total_ratio = 0;

$dataAry = array();
$i = 0;

foreach($semester_data as $id=>$name) {
     $s_ratio = ${"ratio$i"};
     $dataAry[$i][] = $id;
     $dataAry[$i][] = $s_ratio;
     $i++;
}

$ldiscipline->editSemesterRatio($yearID, $dataAry, 'S');

header("Location: semester_ratio.php?xmsg=update&schoolYear=$yearID");

intranet_closedb();
?>