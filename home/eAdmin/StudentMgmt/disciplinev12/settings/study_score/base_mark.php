<?php

#############################################
#
#	Date:	2017-01-20	Bill	[2015-0120-1200-33164]
#			Update Tab wording
#
#	Date:	2012-11-09	YatWoon
#			Add "Semester Ratio" tab
#
#############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_StudyScore";

# Conduct Mark calculation method
$CalculationMethod = $ldiscipline->retriveConductMarkCalculationMethod();

# Study Score calculation method
$StudyScoreCalculationMethod = $ldiscipline->retriveStudyScoreCalculationMethod();

$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Warning_Rules, "warning_rule.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['SubScoreBaseMark'], "base_mark.php", 1);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Grading_Scheme, "grading_scheme.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester_Ratio, "semester_ratio.php", 0);

$baseMark = $ldiscipline->getStudyScoreRule('baseMark');
$baseMark = ($baseMark=='') ? "N/A" : $baseMark;

if($xmsg != '') {
	$showmsg = "<table width=90% cellspacing=0 cellpadding=5 border=0 align='right'>";	
	$showmsg .= "<tr><td align=right>";
	$showmsg .= $linterface->GET_SYS_MSG($xmsg);
	$showmsg .= "</td></tr>";
	$showmsg .= "</table>";
}

if($msg==1) {
	$ReturnMsg = $Lang['General']['ReturnMessage']['UpdateSuccess'];	
}

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();


if($StudyScoreCalculationMethod != 0){
	$str_CalculationMethod = $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByAcademicYear'];
}else{
	$str_CalculationMethod = $Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByTerm'];
}

# selection menu of method type
$arrCalculationMethod = array(array(0,$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByTerm']),array(1,$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByAcademicYear']));
$CalculationMethodSelection = getSelectByArray($arrCalculationMethod, ' name="CalculationMethod" ',$StudyScoreCalculationMethod,0,1);


# Start layout
$linterface->LAYOUT_START($ReturnMsg);
?>
<script language="javascript">
<!--
var alertMsg = "<?=$Lang['eDiscipline']['jsStudyScoreEditWarning']?>";	

function showAlertMsg() {
    if(confirm(alertMsg)){
		self.location.href = 'base_mark_edit.php';
    } else 
    	return false;
	
}

function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

function checkForm() {
	var obj = document.form1;
	if(obj.baseMark.value=="" || isNaN(obj.baseMark.value) || obj.baseMark.value<0) {
		alert("<?=$Lang['General']['JS_warning']['InputPositiveInteger']?>");
		return false;
	} else {
		if(confirm(alertMsg)) {
			return true;
		} else {
			return false;
		}
	}
}
//-->
</script>
<form name="form1" method="post" action="base_mark_edit_update.php" onSubmit="return checkForm()">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><?=$showmsg ?><br style="clear:both">
			<?/*= $ldiscipline->showWarningMsg($i_Discipline_System_Conduct_Instruction, $i_Discipline_System_Conduct_Instruction_Msg_Content,"class='tabletextrequire'") */ ?>
			
			<div class="table_row_tool row_content_tool">
				<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
			</div>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FieldTitle']['StudyScoreCalculationMethod']?></td>
								<td>
									<span class="Edit_Hide">
										<?=$str_CalculationMethod;?>
									</span>
									<span class="Edit_Show" style="display:none">
										<?=$CalculationMethodSelection?>
									</span>
								</td>
							</tr>
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['FieldTitle']['StudyScoreBaseMark']?></td>
								<td>
									<span class="Edit_Hide">
										<?=$baseMark ?> 
									</span>
									<span class="Edit_Show" style="display:none">
										<input type="text" name="baseMark" id="baseMark" value="<?=($baseMark=="N/A" ? "" : $baseMark)?>" maxlength="3" >
									</span>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				
			</table>
			<div class="edit_bottom_v30">
				<p class="spacer"></p>
				<?=$linterface->GET_ACTION_BTN($button_submit,"submit", "","UpdateBtn", " style='display:none'")?>
				<?=$linterface->GET_ACTION_BTN($button_cancel,"button", "ResetInfo()","cancelbtn", " style='display:none'")?>
				</p>
			</div>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
