<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Conduct_Mark-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$previousScore = $_POST['previousScore'];
$newScore = $_POST['studyScore'];
$grade = intranet_htmlspecialchars($_POST['calculatedGrade']);

$status = $ldiscipline->setStudyScoreGradeRuleGrade($previousScore, $newScore, $grade);

intranet_closedb();

if($status==false) {
	$url = "grading_scheme_edit.php?xmsg2=$i_Discipline_System_alert_grading_scheme_already_exists&score=$previousScore";
}	
else {
	$url = "grading_scheme.php?xmsg=update";
}
header("Location: $url");
?>
