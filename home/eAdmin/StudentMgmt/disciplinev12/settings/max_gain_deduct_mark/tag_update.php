<?php
# using: 

/*************************************************
 *	Date: 2013-06-14 (Rita) 
 *  Details: create this page for yy3 cust
 *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$MaxGainMarkArr = $_POST['MaxGainMark'];
$MaxDeductMarkArr = $_POST['MaxDeductMark'];
$result = array();
foreach ((array)$MaxGainMarkArr as $thisTagID=>$thisMaxGainMark)
{
	$thisMaxDeductMark = $MaxDeductMarkArr[$thisTagID];
	$result[] = $ldiscipline->UpdateTagMaxGainDeductMark($thisTagID,$thisMaxGainMark,$thisMaxDeductMark);
}

if(!in_array(false,$result)){
	$xmsg = 'UpdateSuccess';
}else{
	$xmsg = 'UpdateUnsuccess';
}

header("Location: tag.php?xmsg=$xmsg");

intranet_closedb();

?>