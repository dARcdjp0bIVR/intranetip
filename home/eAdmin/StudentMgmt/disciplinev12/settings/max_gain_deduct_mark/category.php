<?php

// Using: rita
/*************************************************
 *  Date: 2013-07-16 (Rita)
 *  Details: amend to show sub categories in no.
 * 
 *	Date: 2013-06-14 (Rita) 
 *  Details: create this page for yy3 cust
 *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
	
# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}
	
# Page Setting
$PageSettings = 1;
$CurrentPage = "Settings_Max_Gain_Deduct_Mark";
$CurrentPageArr['eDisciplinev12'] = 1;
			
$Merit = $_GET['Merit'];
if($Merit==''){
	$Merit = 1;	
}			
		
# Tag 
$awardTag = 0;
$punishTag = 0;

if($Merit==1)// Award
{
	$awardTag = 1;
}
elseif($Merit==-1)// Punishment
{
	$punishTag = 1;
}

$TAGS_OBJ[] = array($i_Merit_Award, "category.php?Merit=1", $awardTag);
$TAGS_OBJ[] = array($i_Merit_Punishment, "category.php?Merit=-1", $punishTag);
$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester, "semester.php", 0);

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$xmsg = $_GET['xmsg'];
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

# Get Merit Category
if($Merit==1) // Award
{
	$meritCatArr = $ldiscipline->GetMeritCat();
}
elseif($Merit==-1) // Punishment
{
	$meritCatArr = $ldiscipline->GetDemeritCat();
}

$numOfMeritCatArr = count($meritCatArr);

$displayTable = '';
$displayTable = '<form name="form1" id="form1" method="post" action="category_item_edit.php">';

//	# Edit Btn
//	$displayTable .='<div class="common_table_tool">';
//	$displayTable .='<a href="javascript:submitForm(document.form1,\'CatIDItemID[]\',\'category_item_edit.php\')" class="tool_edit">'.$Lang['Button']['Edit'].'</a>';
//	$displayTable .='</div>';
	
	$displayTable .= '<table class="common_table_list_v30" width="100%" align="center" border="0" cellspacing="0" cellpadding="4">';
	$displayTable .= '<thead>';
		$displayTable .= '<tr>';
		$displayTable .= '<th width="5" class="tabletop tabletopnolink">#</th>';
		$displayTable .= '<th width="55%" class="tabletop tabletopnolink">'.$i_Discipline_System_CategoryName.'</th>';
		$displayTable .= '<th width="40%" class="tabletop tabletopnolink">'.$Lang['eDiscipline']['sdbnsm_ap_item'].'</th>';

	 	$displayTable .= '</tr>';
	$displayTable .= '</thead>';
	
	$displayTable .= '<tbody>';
	
	
	for($i=0;$i<$numOfMeritCatArr;$i++){
		$thisMeritCatId = $meritCatArr[$i]['CatID'];
		$thisMeritCatName = $meritCatArr[$i]['CategoryName'];
		
		if($Merit==1)
		{
			$thisCatItemArr = $ldiscipline->GetMeritCatItem($thisMeritCatId);
		}
		elseif($Merit==-1)
		{
			$thisCatItemArr = $ldiscipline->GetDemeritCatItem($thisMeritCatId);
		}
		
		$numOfThisCatItemArr = count($thisCatItemArr);
		
		$displayTable .= '<tr>';
		$displayTable .= '<td>' . ($i+1) . '</td>';
		$displayTable .= '<td>' . $thisMeritCatName . '</td>';
		
		$displayTable .= '<td>';
		$displayTable .= '<a href="category_item.php?Merit='.$Merit.'&id=' .$thisMeritCatId. '">' . $numOfThisCatItemArr .'</a>';	
		 
		$displayTable .='</td>';
		
	}
	
	
	$displayTable .= '</tbody>';
	$displayTable .= '</table>';
	$displayTable .= '<input type="hidden" name="Merit" value="'.$Merit.'"></input>';
$displayTable .= '</form>';

echo $displayTable;

?>
<script language="javascript">

function submitForm(obj,page,action)
{
	if($('.CatIDItemID:checkbox:checked').length==0){
		alert(globalAlertMsg2);
	}else{
		obj.submit();
	}
}

function js_Select_All_Item(){
	if($('.CatIDItemID:checkbox:checked').length==0){
		$('.CatIDItemID').attr('checked', 'checked');
	}else{
		$('.CatIDItemID').attr('checked', '');
	}	
}

</script>

<?
echo $linterface->FOCUS_ON_LOAD("form1.TagName"); 
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
