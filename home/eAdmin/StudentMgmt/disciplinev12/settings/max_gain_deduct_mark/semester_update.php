<?php
# Editing by 

/*************************************************
 *	Date: 2013-06-18
 *  Details: create this page for yy3 cust
 *************************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$yearID = $_POST['yearID'];
$MaxConductMarkArr = $_POST['MaxConductMark'];
$MinConductMarkArr = $_POST['MinConductMark'];
$result = array();
foreach ((array)$MaxConductMarkArr as $thisYearTermID=>$thisMaxConductMark)
{
	$thisMinConductMark = $MinConductMarkArr[$thisYearTermID];
	$result[] = $ldiscipline->UpdateSemesterMaxMinConductMark($yearID,$thisYearTermID,$thisMaxConductMark,$thisMinConductMark);
}

if(!in_array(false,$result)){
	$xmsg = 'UpdateSuccess';
}else{
	$xmsg = 'UpdateUnsuccess';
}

header("Location: semester.php?schoolYear=$yearID&xmsg=$xmsg");

intranet_closedb();
?>