<?php
// Editing by 
/*************************************************
 *	Date: 2013-06-18
 *  Details: create this page for yy3 cust
 *************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# Page Setting
$PageSettings = 1;
$CurrentPage = "Settings_Max_Gain_Deduct_Mark";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Merit_Award, "category.php?Merit=1", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "category.php?Merit=-1", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Conduct_Semester, "semester.php", 1);

$selectedYear = $_POST['schoolYear'];

$yearID = $selectedYear;
$schoolYearName = $ldiscipline->getAcademicYearNameByYearID($yearID);

############
# Get Year Selection

$semester_data = getSemesters($yearID);

$conductMarkRecords = $ldiscipline->GetSemesterMaxMinConductMark($yearID);

foreach($semester_data as $id=>$name) {
	$select .= "<tr><td>";
	$select .= $name;
	$select .= "</td><td align=center>";
	$select .= $linterface->GET_TEXTBOX_NUMBER("MaxConductMark[$id]", "MaxConductMark[$id]", $conductMarkRecords[$id]['MaxConductMark'], $OtherClass='ConductMark', $OtherPar=array('onchange'=>'forceInputNumber(this);'));
	$select .= "</td>";
	$select .= "<td align=center>";
	$select .= $linterface->GET_TEXTBOX_NUMBER("MinConductMark[$id]", "MinConductMark[$id]", $conductMarkRecords[$id]['MinConductMark'], $OtherClass='ConductMark', $OtherPar=array('onchange'=>'forceInputNumber(this);'));
	$select .= "</td>";
	$select .= "</tr>";
}


# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$xmsg = $_GET['xmsg'];
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>
<script language="javascript">
<!--
function forceInputNumber(obj)
{
	var num = obj.value.Trim();
	if(isNaN(num) || !parseFloat(num)) {
		obj.value = '';
	}else{
		obj.value = parseFloat(num);
	}
}

function checkForm(obj)
{
	obj.submit();
}
//-->
</script>
<form name="form1" method="post" action="semester_update.php" onsubmit="checkForm(this);return false;">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td align="right">
						<?=$showmsg ?>
					</td>
				</tr>
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Conduct_School_Year?></td>
								<td>
									<?=$schoolYearName ?>
								</td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=""?></span></td>
								<td><label for="grading_honor" class="tabletext"></label>
									<table border="0" cellspacing="0" cellpadding="3">
										<tr>
											<td><U><?=$i_Discipline_System_Conduct_Semester?></U></td>
											<td align="center"><u><?=$Lang['eDiscipline']['MaxConductMark']?></u></td>
											<td align="center"><u><?=$Lang['eDiscipline']['MinConductMark']?></u></td>
										</tr>
										<?=$select ?>
									</table>
									<label for="grading_passfail" class="tabletext"></label>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" alt="" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button","javascript:location.href='semester.php?schoolYear=$yearID'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="yearID" value="<?=$yearID?>" />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
