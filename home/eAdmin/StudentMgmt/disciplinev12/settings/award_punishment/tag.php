<?php
// Using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
}
else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
} 

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
}
else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eDiscipline"] && !$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
/*
if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
*/

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

/*
$order = $order ? $order : 1;
$field = ($field == "") ? 0 : $field;
*/
$order = ($order == '' || $order != 0) ? 1 : 0;
//$field = ($field == "") ? 0 : $field;
$field = 2;

$mtype = 1;

$sql = "SELECT
			CONCAT('<a href=\"tag_edit.php?tagID=',TagID,'\">',TagName,'</a>') as TagName,
			CONCAT('<input type=\'checkbox\' name=\'tagID[]\' value=\'', TagID ,'\'>')
		FROM
			DISCIPLINE_ITEM_TAG_SETTING
		";
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("TagName");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='5' class='tabletopnolink'>#</td>\n";
$li->column_list .= "<td width='90%'>".$Lang['eDiscipline']['TagName']."</td>\n";$pos++;
$li->column_list .= "<td width='1'>".$li->check("tagID[]")."</td>\n";

# Menu
$CurrentPage = "Settings_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 1);
}

$toolbar = $linterface->GET_LNK_NEW("tag_new.php",$i_Discipline_System_Access_Right_New,"","","",0);

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0) {
    	alert(globalAlertMsg2);
    }
    else{
        if(confirm(alertConfirmRemove)){
            obj.action = page;
            obj.method = "post";
            obj.submit();
        }
    }
}
//-->
</script>

<form name="form1" method="post" action="">
<br />
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="80%"><?= $toolbar ?></td>
								<td width="20%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>&nbsp;</td>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'tagID[]','tag_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:removeCat(document.form1,'tagID[]','tag_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?=$li->display();?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="merit" value="<?=$mtype?>">
<br />
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>