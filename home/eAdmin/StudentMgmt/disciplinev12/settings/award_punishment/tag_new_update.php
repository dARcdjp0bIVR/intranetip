<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$dataAry = array();

$dataAry['MeritType'] = $merit;
$dataAry['TagName'] = $_POST['TagName'];

$ldiscipline->editAwardPunishTag("add", $dataAry);

intranet_closedb();

header("Location: tag.php?xmsg=add");

?>
