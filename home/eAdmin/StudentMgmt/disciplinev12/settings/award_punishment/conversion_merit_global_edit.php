<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

if($merit==1) {
	$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 1);
	$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php?MeritType=1", 0);
	if($sys_custom['Disciplinev12_show_AP_tag']) {
		$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
	}
	$categoryItemURL = "index.php";
	$tagURL = "award_tag.php";
	$approvalURL = "award_approval.php";
	$backURL = "award_promotion.php";
	
	$meritDemerit1 = $i_Merit_Merit;
	$meritDemerit2 = $i_Merit_MinorCredit;
	$meritDemerit3 = $i_Merit_MajorCredit;
	$meritDemerit4 = $i_Merit_SuperCredit;
	$meritDemerit5 = $i_Merit_UltraCredit;
	
	if(!$lstudentprofile->is_min_merit_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', 2);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 2);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit1."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td align='center' class='formfieldtitle'>1 ".$meritDemerit2." </td></tr>";		
	}
	if(!$lstudentprofile->is_maj_merit_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', 3);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 3);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit2."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td class='formfieldtitle' align='center'>1 ".$meritDemerit3." </td></tr>";		
	}
	if(!$lstudentprofile->is_sup_merit_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', 4);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 4);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit3."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td class='formfieldtitle' align='center'>1 ".$meritDemerit4." </td></tr>";		
	}
	if(!$lstudentprofile->is_ult_merit_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', 5);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 5);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit4."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td class='formfieldtitle' align='center'>1 ".$meritDemerit5." </td></tr>";		
	}
	
	
} else {
	$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 0);
	$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 1);
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
	if($sys_custom['Disciplinev12_show_AP_tag']) {
		$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
	}
	$categoryItemURL = "punish_category_item.php";
	$tagURL = "punish_tag.php";
	$approvalURL = "punish_approval.php";
	$backURL = "punish_promotion.php";
	
	$meritDemerit1 = $i_Merit_BlackMark;
	$meritDemerit2 = $i_Merit_MinorDemerit;
	$meritDemerit3 = $i_Merit_MajorDemerit;
	$meritDemerit4 = $i_Merit_SuperDemerit;
	$meritDemerit5 = $i_Merit_UltraDemerit;

	if(!$lstudentprofile->is_min_demer_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', -2);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 2);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td width='30%' valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit1."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td class='formfieldtitle' align='center'>1 ".$meritDemerit2." </td></tr>";		
	}
	if(!$lstudentprofile->is_maj_demer_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', -3);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 3);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit2."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td class='formfieldtitle' align='center'>1 ".$meritDemerit3." </td></tr>";		
	}
	if(!$lstudentprofile->is_sup_demer_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', -4);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 4);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit3."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td class='formfieldtitle' align='center'>1 ".$meritDemerit4." </td></tr>";		
	}
	if(!$lstudentprofile->is_ult_demer_disabled) {
		$upgradeNum = $ldiscipline->getMeritUpgradeNum('', -5);
		$conductScoreMenu = $ldiscipline->selectEquivalentMerit($upgradeNum, 5);
		
		$text .= "<tr valign='top'>";
		$text .= "<td class='formfieldtitle'>".$conductScoreMenu."</td>";
		$text .= "<td valign='top' nowrap='nowrap' class='formfieldtitle' align='center'>".$meritDemerit4."(s)</td>";
		$text .= "<td valign='top' align='center' nowrap='nowrap' class='formfieldtitle'>".$i_Discipline_System_Award_Punishment_Convert_To."</td>";
		$text .= "<td class='formfieldtitle' align='center'>1 ".$meritDemerit5." </td></tr>";
	}
	
}

$CatID = (is_array($CatID)) ? $CatID[0] : $CatID;

$CatName = $ldiscipline->getAwardPunishCategoryName($CatID);

$PeriodType =  $ldiscipline->getConversionPeriod('',$merit);
$PeriodSelection = $ldiscipline->getPeriodTypeSelection("PeriodType", $PeriodType["global"]);


$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, $categoryItemURL, 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, $approvalURL, 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, $backURL, 1);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Award_Punishment_Common_Setting, $backURL);
$PAGE_NAVIGATION[] = array($CatName, "");


 
$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<form name="form1" method="post" action="conversion_merit_global_edit_update.php">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table><!--
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
								<?= $text ?>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
										<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='$backURL'")?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>//-->
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="60%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td colspan=4><?=$PeriodSelection?></td>
							</tr>
							<tr valign="top" class="tabletop">
								<td align='center'><?=$i_general_no_of ?></td>
								<td align='center'><?=$i_Discipline_System_Award_Punishment_Lower_Level?></td>
								<td align='center'><?=$i_Discipline_System_Award_Punishment_Convert_To?></td>
								<td align='center'><?=$i_Discipline_System_Award_Punishment_Higher_Level?></td>
							</tr>
								<?= $text ?>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right"><table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
										<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
										<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='$backURL'")?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="meritType" value="<?=$merit?>">
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
