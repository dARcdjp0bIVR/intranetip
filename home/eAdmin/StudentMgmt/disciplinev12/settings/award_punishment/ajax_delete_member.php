<?php
// editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libaccessright.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12_ui.php");

intranet_auth();
intranet_opendb();
/*
$laccessright = new libaccessright();

if(!$plugin['AccountMgmt_StudentRegistry'] || (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-AccountMgmt_StudentRegistry"] && !$laccessright->CHECK_ACCESS("StudentRegistry-SETTINGS-AccessRight-Access"))) {
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
*/
$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

$ldiscipline ->Start_Trans();
if ($ldiscipline->Approval_Group_Remove_Member($GroupID,$StaffID)) {
	$ldiscipline->Commit_Trans();
	echo $Lang['StaffAttendance']['DeleteMemberSuccess'];
}
else {
	$ldiscipline->RollBack_Trans();
	echo $Lang['StaffAttendance']['DeleteMemberFail'];
}

intranet_closedb();
?>