<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$ldiscipline->updateAwardPunishCategoryPriority($merit,$old,$new);

intranet_closedb();

if($merit == 1) {
	header("Location: index.php?xmsg=update");
} else {
	header("Location: punish_category_item.php?xmsg=update");
}

?>
