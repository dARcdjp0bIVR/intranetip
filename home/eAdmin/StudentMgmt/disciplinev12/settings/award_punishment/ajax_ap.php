<?
# using: 

########## Change Log ###############
#
#	Date	:	2015-04-13 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	display reference popup for auto select detention and eNotice option
#
#	Date	:	2010-06-14 (Henry)
#	Detail 	:	import of Category
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if($task=='status')
{				
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Setting_Status'].'</td>
								</tr>';
				$data .='<tr class="tablerow1">
					 <td>1</td>
					 <td>'.$eDiscipline['Setting_Status_Using'].'</td>
					 </tr>
					 <tr class="tablerow1">
					 <td>2</td>
					 <td>'.$eDiscipline['Setting_Status_NonUsing'].'</td>
					 </tr>';
	$data .="</table>";	
}
else if($task=='templateID')
{			
	$sql = "SELECT CategoryID, TemplateID, Title FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE Module='".$ldiscipline->Module."' AND RecordStatus=1 ORDER BY CategoryID, Title";
	$result = $ldiscipline->returnArray($sql, 2);
		
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$i_Discipline_System_Discipline_Reason_For_Issue.'</td>
									<td>'.$eDiscipline['Subject'].'</td>
								</tr>';
		for($i=0; $i<sizeof($result); $i++) {
				$data .='<tr class="tablerow1">
					 <tr class="tablerow1">
					 <td>'.$result[$i][1].'</td>
					 <td>'.$result[$i][0].'</td>
					 <td>'.$result[$i][2].'</td>
					 </tr>';
		}
	$data .="</table>";	
}
else if($task=='category')
{			
	$meritAry = array("1"=>$i_Discipline_System_Award_Punishment_Awards, "-1"=>$i_Discipline_System_Award_Punishment_Punishments);
	$sql = "SELECT CatID, CategoryName, RecordType FROM DISCIPLINE_MERIT_ITEM_CATEGORY WHERE (RecordStatus IS NULL OR RecordStatus!=-1) ORDER BY RecordType DESC, CategoryName";
	$result = $ldiscipline->returnArray($sql, 2);
		
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Setting_CategoryName'].'</td>
									<td>'.$eDiscipline['Type'].'</td>
								</tr>';
		for($i=0; $i<sizeof($result); $i++) {
				$data .='<tr class="tablerow1">
					 <tr class="tablerow1">
					 <td>'.$result[$i][0].'</td>
					 <td>'.$result[$i][1].'</td>
					 <td>'.$meritAry[$result[$i][2]].'</td>
					 </tr>';
		}
	$data .="</table>";	
}
else if($task=='type') {
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Type'].'</td>
								</tr>';
			$data .='<tr class="tablerow1">
				 <tr class="tablerow1">
				 <td>1</td>
				 <td>'.$i_Discipline_System_Award_Punishment_Awards.'</td>
				 </tr>
				 <tr class="tablerow1">
				 <td>-1</td>
				 <td>'.$i_Discipline_System_Award_Punishment_Punishments.'</td>
				 </tr>
				 ';
	$data .="</table>";	
}
else if($task=='meritType') {
	//if($MeritType==1) {
		if (!$lsp->is_merit_disabled)
		{
			$mType['1'] = $i_Merit_Merit;
		}
		if (!$lsp->is_min_merit_disabled)
		{
			$mType['2'] = $i_Merit_MinorCredit;
		}
		if (!$lsp->is_maj_merit_disabled)
		{
			$mType['3'] = $i_Merit_MajorCredit;
		}
		if (!$lsp->is_sup_merit_disabled)
		{
			$mType['4'] = $i_Merit_SuperCredit;
		}
		if (!$lsp->is_ult_merit_disabled)
		{
			$mType['5'] = $i_Merit_UltraCredit;
		}
	//} else {
		if (!$lsp->is_warning_disabled)
		{
			$mType['0'] = $i_Merit_Warning;
		}
		if (!$lsp->is_black_disabled)
		{
			$mType['-1'] = $i_Merit_BlackMark;
		}
		if (!$lsp->is_min_demer_disabled)
		{
			$mType['-2'] = $i_Merit_MinorDemerit;
		}
		if (!$lsp->is_maj_demer_disabled)
		{
			$mType['-3'] = $i_Merit_MajorDemerit;
		}
		if (!$lsp->is_sup_demer_disabled)
		{
			$mType['-4'] = $i_Merit_SuperDemerit;
		}
		if (!$lsp->is_ult_demer_disabled)
		{
			$mType['-5'] = $i_Merit_UltraDemerit;
		}
	//}
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Type'].'</td>
								</tr>';
		foreach($mType as $key=>$val) {
				$data .='<tr class="tablerow1">
					 <tr class="tablerow1">
					 <td>'.$key.'</td>
					 <td>'.$val.'</td>
					 </tr>';
		}
		$data .= '<tr class="tablerow1">
					<td>-999</td>
					<td>'.$i_general_na.'</td>
				';
	$data .="</table>";	
}
else if($task=='tag') {
	$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING ORDER BY TagName";
	$result = $ldiscipline->returnArray($sql, 2);
	
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Type'].'</td>
								</tr>';
		for($i=0; $i<sizeof($result); $i++) {
				$data .='<tr class="tablerow1">
					 <tr class="tablerow1">
					 <td>'.$result[$i][0].'</td>
					 <td>'.$result[$i][1].'</td>
					 </tr>';
		}
	$data .="</table>";	
}
// [2015-0128-1044-22170] / [2015-0313-1420-56054]
else if($task=='defaultDetention'){	
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$Lang['eDiscipline']['DefaultSetDetention'].'</td></tr>';
				$data .='<tr class="tablerow1">
					 <td>0</td>
					 <td>'.$i_general_no.'</td></tr>
					 <tr class="tablerow1">
					 <td>1</td>
					 <td>'.$i_general_yes.'</td></tr>';
	$data .= "</table>";	
} 
// [2015-0127-1101-48073]
else if($task=='defaultNotice'){	
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$Lang['eDiscipline']['DefaultSendNotice'].'</td></tr>';
				$data .='<tr class="tablerow1">
					 <td>0</td>
					 <td>'.$i_general_no.'</td></tr>
					 <tr class="tablerow1">
					 <td>1</td>
					 <td>'.$i_general_yes.'</td></tr>';
	$data .= "</table>";	
}

$output = '<table width="70%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
						'.$data.'
						
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';
echo $output;
?>
