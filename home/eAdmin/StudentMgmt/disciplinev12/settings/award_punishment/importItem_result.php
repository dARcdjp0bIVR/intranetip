<?
// Modifying by: 

#############################################
#
#	Date	:	2017-07-13	(Bill)	[DM#3233]
#	Detail 	:	add N.A. import handling (Set number to 0 if merit type = -999)
#
#	Date	:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#	Detail 	:	support import items with Study Score Limit ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#	Detail 	:	support import item with Activity Score
#
#	Date	:	2016-03-09 (Kenneth)
#	Detail 	:	add update import
#
#	Date	:	2010-06-14 (Henry)
#	Detail 	:	import of Item
#
#####################################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$limport = new libimporttext();
$lo = new libfilesystem();
$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Award_Punishment-Access");
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));

$importMode = $_POST['importMode'];

if(!isset($MeritType) || $MeritType=="") $MeritType = 1;

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: importItem.php?xmsg=import_failed&MeritType=$MeritType");
	exit();
}

$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}

if($importMode=='new'){
	$file_format = array('Item Code', 'Item Name', 'Category ID', 'Conduct Mark');
}
else if($importMode=='update'){
	$file_format = array('Existing Item Code','Item Code', 'Item Name', 'Category ID', 'Conduct Mark');
}
// [2016-1125-0939-00240]
if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$file_format[] = "Point";
	$file_format[] = "Point Limit";
}
else if($ldiscipline->UseSubScore) {
	$file_format[] = "Study Score";
}
if($ldiscipline->UseActScore) {
	$file_format[] = "Activity Score";
}
$file_format[] = "Quantity";
$file_format[] = "Type";
$file_format[] = "Status";
if($sys_custom['Discipline_AP_Item_Tag']) {
	$file_format[] = "Tag ID";
}

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i]) {
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: importItem.php?xmsg=wrong_header&MeritType=$MeritType");
	exit();
}

$ldiscipline = new libdisciplinev12();
$lu = new libuser();

$linterface = new interface_html();

# Menu Highlight
$CurrentPage = "Settings_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);
$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportItem']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
if($importMode=='update'){
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import']['ExistingItemCode'] ."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_ItemCode."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_ItemName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['Accumulative_Category']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Conduct_Mark']."</td>";
if($ldiscipline->UseSubScore) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_Subscore1."</td>";
}
// [2016-1125-0939-00240]
if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['MeritItemMaxStudyScore']."</td>";
}
if($ldiscipline->UseActScore) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['ActivityScore']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Type']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_Quantity."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['MeritType']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_Status']."</td>";
if($sys_custom['Discipline_AP_Item_Tag']) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Tag']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

$sql = "create table IF NOT EXISTS temp_award_punishment_settings_item_import
		(
			 ItemCode varchar(255),
			 ItemName mediumtext,
			 CategoryID int(11),
			 ConductMark int(11),
			 StudyScore int(11),
			 Quantity float(8,2),
			 Type int(11),
			 Status int(11),
			 TagID varchar(100),
			 UserID int(11),
			 DateInput datetime
	    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";	    
$ldiscipline->db_db_query($sql);

$sql = "ALTER TABLE temp_award_punishment_settings_item_import Add ExistingItemCode varchar(255) default NULL";
$ldiscipline->db_db_query($sql);

$sql = "ALTER TABLE temp_award_punishment_settings_item_import Add ActivityScore int(11)";
$ldiscipline->db_db_query($sql);

// [2016-1125-0939-00240]
$sql = "ALTER TABLE temp_award_punishment_settings_item_import Add MaxGainDeductSubScore1 int(11)";
$ldiscipline->db_db_query($sql);

$fields = mysql_list_fields($intranet_db, 'temp_award_punishment_settings_item_import');

# delete the temp data in temp table 
$sql = "delete from temp_award_punishment_settings_item_import where UserID=$UserID";
$ldiscipline->db_db_query($sql) or die(mysql_error());

if (!$lsp->is_merit_disabled)
{
	$mType['1'] = $i_Merit_Merit;
}
if (!$lsp->is_min_merit_disabled)
{
	$mType['2'] = $i_Merit_MinorCredit;
}
if (!$lsp->is_maj_merit_disabled)
{
	$mType['3'] = $i_Merit_MajorCredit;
}
if (!$lsp->is_sup_merit_disabled)
{
	$mType['4'] = $i_Merit_SuperCredit;
}
if (!$lsp->is_ult_merit_disabled)
{
	$mType['5'] = $i_Merit_UltraCredit;
}
if (!$lsp->is_warning_disabled)
{
	$mType['0'] = $i_Merit_Warning;
}
if (!$lsp->is_black_disabled)
{
	$mType['-1'] = $i_Merit_BlackMark;
}
if (!$lsp->is_min_demer_disabled)
{
	$mType['-2'] = $i_Merit_MinorDemerit;
}
if (!$lsp->is_maj_demer_disabled)
{
	$mType['-3'] = $i_Merit_MajorDemerit;
}
if (!$lsp->is_sup_demer_disabled)
{
	$mType['-4'] = $i_Merit_SuperDemerit;
}
if (!$lsp->is_ult_demer_disabled)
{
	$mType['-5'] = $i_Merit_UltraDemerit;
}

$error_occured = 0;
$space = ($intranet_session_language=="en") ? " ":"";
$typeAry = array('1'=>$i_Discipline_System_Award_Punishment_Awards, '-1'=>$i_Discipline_System_Award_Punishment_Punishments);
$statusAry = array(1=>$eDiscipline['Setting_Status_Using'], 2=>$eDiscipline['Setting_Status_NonUsing']);

$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING";
$result = $ldiscipline->returnArray($sql);
$tagAry = build_assoc_array($result);

for($i=0; $i<sizeof($data); $i++)
{
	$error = array();
	
	### get data
	if($ldiscipline->UseActScore) {
		if($importMode=='new'){
			list($itemCode, $itemName, $catID, $conductMark, $studyScore, $activityScore, $qty, $type, $status, $tagid) = $data[$i];
		}
		else if($importMode=='update') {
			list($existingItemCode, $itemCode, $itemName, $catID, $conductMark, $studyScore, $activityScore, $qty, $type, $status, $tagid) = $data[$i];
		}
	}
	// [2016-1125-0939-00240]
	else if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
		if($importMode=='new'){
			list($itemCode, $itemName, $catID, $conductMark, $studyScore, $maxStudyScore, $qty, $type, $status, $tagid) = $data[$i];
		}
		else if($importMode=='update') {
			list($existingItemCode, $itemCode, $itemName, $catID, $conductMark, $studyScore, $maxStudyScore, $qty, $type, $status, $tagid) = $data[$i];
		}
	}
	else if($ldiscipline->UseSubScore) {
		if($importMode=='new'){
			list($itemCode, $itemName, $catID, $conductMark, $studyScore, $qty, $type, $status, $tagid) = $data[$i];
		}
		else if($importMode=='update') {
			list($existingItemCode, $itemCode, $itemName, $catID, $conductMark, $studyScore, $qty, $type, $status, $tagid) = $data[$i];
		}
	}
	else {
		if($importMode=='new'){
			list($itemCode, $itemName, $catID, $conductMark, $qty, $type, $status, $tagid) = $data[$i];
		}
		else if($importMode=='update') {
			list($existingItemCode, $itemCode, $itemName, $catID, $conductMark, $qty, $type, $status, $tagid) = $data[$i];
		}
	}
	$existingItemCode = intranet_htmlspecialchars(trim($existingItemCode));
	$itemCode = intranet_htmlspecialchars(trim($itemCode));
	$itemName = intranet_htmlspecialchars(trim($itemName));
	$catID = trim($catID);
	$conductMark = trim($conductMark);
	$studyScore = trim($studyScore);
	$maxStudyScore = trim($maxStudyScore);
	$activityScore = trim($activityScore);
	$qty = trim($qty);
	$type = trim($type);
	$status = trim($status);
	$tagid = trim($tagid);
	$catInfo = $ldiscipline->getAPCategoryInfoBYCatID($catID);
	
	#### Check is Item Code exist
	if($importMode=='update'){
		if(trim($existingItemCode)=='') {
			$error['existingItemCode'] = $Lang['eDiscipline']['Import']['ExistingItemCode'].$space.$ec_html_editor['missing'];	
		}
		else {
			// check existing 
			if($ldiscipline->checkDuplicateItemCode($existingItemCode,'')==0) {
				$error['existingItemCode'] = $Lang['eDiscipline']['Import']['ExistingItemCode'].$space.$ec_html_editor['incorrect'];	
			}
		} 
	}
	
	# Item Code
	if(trim($itemCode)=='') {
		$error['itemCode'] = $i_Discipline_System_ItemCode.$space.$ec_html_editor['missing'];	
	}
	if($importMode=='new'){
		if($ldiscipline->checkDuplicateItemCode($itemCode,'') != 0) {
			# Duplicate item code with existing items
			$error['itemCode'] = $i_Discipline_System_ItemCode.$space.$Lang['eDiscipline']['Import_AlreadyExist'];	
		}
	}
	else if($importMode=='update'){
		$itemInfo = $ldiscipline->getAwardPunishItemByItemCode($existingItemCode);
		$itemID = $itemInfo['ItemID'];
		$countOfDuplicates = $ldiscipline->checkDuplicateItemCode($itemCode,$itemID);
		if($countOfDuplicates>0){
			$error['itemCode'] = $i_Discipline_System_ItemCode.$space.$Lang['eDiscipline']['Import_AlreadyExist'];	
		}
	}
	# Check if any duplicate Item Code within CSV
	for($a=0, $a_max=$i-1; $a<=$a_max; $a++) {
		$otherItemCode = $data[$a][0];
		//echo $i.'/'.$a.'/'.$itemCode.'/'.$otherItemCode.'<br>';
		if($itemCode==$otherItemCode) {	
			$error['itemCode'] = $i_Discipline_System_ItemCode.$space.$Lang['eDiscipline']['Import_AlreadyExist'].'|';
			break;	
		}
	}
	
	# Item Name
	if(trim($itemName)=='')
	{
		$error['itemName'] = $eDiscipline['Setting_ItemName'].$space.$ec_html_editor['missing'];	
	}
	
	# Category Name
	$displayCat = $catID;
	if(trim($catID)=='') {
		$error['catID'] = $iDiscipline['Accumulative_Category'].$space.$ec_html_editor['missing'];
	}
	else {
		if(sizeof($catInfo)==0) {
			$error['catID'] = $iDiscipline['Accumulative_Category'].$space.$ec_html_editor['incorrect'];
		}
		else {
			$displayCat = intranet_htmlspecialchars($catInfo['CategoryName']);
		}
	}	
	
	# Conduct Mark
	$displayMark = $conductMark;
	if($conductMark>0 && $catInfo['RecordType']==-1) {
		$error['conductMark'] = $iDiscipline['Case_Student_Import_Conduct_Score_Negative'];
	}
	else if($conductMark<0 && $catInfo['RecordType']==1) {
		$error['conductScore'] = $iDiscipline['Case_Student_Import_Conduct_Score_Positive'];	
	}
	else {
		$displayMark = $conductMark;	
	}
	
	# Study Score
	$displayStudyScore = $studyScore;
	if($ldiscipline->UseSubScore) {
		if($studyScore > 0 && $catInfo['RecordType']==-1) {
			$error['studyScore'] = $i_Discipline_System_Subscore1;
		}
		else if($studyScore < 0 && $catInfo['RecordType']==1) {
			$error['studyScore'] = $i_Discipline_System_Subscore1;	
		}
		else {
			// [2016-1125-0939-00240]
			if($sys_custom['eDiscipline']['ApplyMaxStudyScore'] && $catInfo['RecordType']==1 && $maxStudyScore > 0 && $studyScore > $maxStudyScore) {
				$error['studyScore'] = $Lang['eDiscipline']['ExceedStudyScoreMaxGain'];
			}
			else if($sys_custom['eDiscipline']['ApplyMaxStudyScore'] && $catInfo['RecordType']==-1 && $maxStudyScore < 0 && $studyScore < $maxStudyScore) {
				$error['studyScore'] = $Lang['eDiscipline']['ExceedStudyScoreMaxDeduct'];
			}
			else {
				$displayStudyScore = $studyScore;
			}
		}
	}
	
	# [2016-1125-0939-00240] Study Score Limit
	$displayMaxStudyScore = $maxStudyScore;
	if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
		if($maxStudyScore > 0 && $catInfo['RecordType']==-1) {
			$error['maxStudyScore'] = $Lang['eDiscipline']['MeritItemMaxStudyScore'];
		}
		else if($maxStudyScore < 0 && $catInfo['RecordType']==1) {
			$error['maxStudyScore'] = $Lang['eDiscipline']['MeritItemMaxStudyScore'];
		}
		else if ($maxStudyScore==0) {
			$displayMaxStudyScore = $Lang['General']['NotApplicable'];
		}
		else {
			$displayMaxStudyScore = $maxStudyScore;
		}
	}
	
	# Activity Score
	$displayActivityScore = $activityScore;
	if($ldiscipline->UseActScore) {
		if($activityScore>0 && $catInfo['RecordType']==-1) {
			$error['activityScore'] = $Lang['eDiscipline']['ActivityScore'];
		}
		else if($activityScore<0 && $catInfo['RecordType']==1) {
			$error['activityScore'] = $Lang['eDiscipline']['ActivityScore'];	
		}
		else {
			$displayActivityScore = $activityScore;
		}
	}
	
	# Merit Type (Award/Punishment)
	$displayMeritType = $typeAry[$catInfo['RecordType']];
	if($displayMeritType=="") $displayMeritType = $type;
	
	# Merit Quantity
	$displayQty = ($qty=="0") ? "---" : $qty;
	/*
	if(trim($qty)=='') {
		$error['meritType'] = $i_Discipline_System_Quantity.$space.$ec_html_editor['missing'];
	} else {
	*/
		if($qty<0) {
			$error['meritType'] = $i_Discipline_System_Quantity.$space.$ec_html_editor['incorrect'];
		}
		else {
			//$displayQty = $qty;	
		}
	//}	
	
	# Type
	$displayType = $type;
	if(trim($type)=='') {
		$error['type'] = $eDiscipline['Type'].$space.$ec_html_editor['missing'];
	}
	// [DM#3233] N.A. Hnadling
	if($type=="-999") {
		$displayType = "---";
		$displayQty = "---";
		$qty = "0";
	}
	else if($qty==0) {
		$displayType = "---";
		$type = "-999";
	} 
	else if(($catInfo['RecordType']=="-1" && $type>0) || ($catInfo['RecordType']=="1" && $type<=0)) {
		$error['type'] = $iDiscipline['Award_Punishment_Wrong_Merit_Type'];
	}
	else {
		if(!array_key_exists($type, $mType)) {
			$error['type'] = $iDiscipline['Type'].$space.$ec_html_editor['incorrect'];
		}
		else {
			$displayType = $mType[$type];
		}
	}
	
	# Record Status
	$displayStatus = $status;
	if(trim($status)=='') {
		$error['status'] = $eDiscipline['Setting_Status'].$space.$ec_html_editor['missing'];
	}
	else {
		if(!array_key_exists($status, $statusAry)) {
			$error['status'] = $eDiscipline['Setting_Status'].$space.$ec_html_editor['incorrect'];
		}
		else {
			$displayStatus = $statusAry[$status];
		}
	}
	
	# AP Tag 
	$tagName = "";
	if($sys_custom['Discipline_AP_Item_Tag']) {
		if(trim($tagid)!="" && trim($tagid)!="0") {
			$importedTagAry = explode(',', $tagid);
			for($j=0; $j<sizeof($importedTagAry); $j++) {
				$importedTagID = $importedTagAry[$j];
				
				if(!array_key_exists($importedTagID, $tagAry)) {
					$error['tagid'] = $Lang['eDiscipline']['Import_AP_Item_Col'][8].$space.$ec_html_editor['incorrect'];
					$tagName .= ($tagName!="") ? ", ".$importedTagID : $importedTagID;
				}
				else {
					$tagName .= ($tagName!="") ? ", ".$tagAry[$importedTagID] : $tagAry[$importedTagID];
				}
			}
		}
		else {
			$tagName = "---";
		}	
	}

	$css = (sizeof($error)==0) ? "tabletext":"red";
	
	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	if($importMode=='update'){
		$x .= "<td class=\"$css\">". $existingItemCode ."</td>";
	}
	$x .= "<td class=\"$css\">". $itemCode ."</td>";
	$x .= "<td class=\"$css\">". $itemName ."</td>";
	$x .= "<td class=\"$css\">". $displayCat ."</td>";
	$x .= "<td class=\"$css\">". $displayMark ."</td>";
	if($ldiscipline->UseSubScore) {
		$x .= "<td class=\"$css\">". $displayStudyScore."</td>";
	}
	// [2016-1125-0939-00240]
	if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
		$x .= "<td class=\"$css\">". $displayMaxStudyScore."</td>";
	}
	if($ldiscipline->UseActScore) {
		$x .= "<td class=\"$css\">". $displayActivityScore."</td>";
	}
	$x .= "<td class=\"$css\">". $displayMeritType ."</td>";
	$x .= "<td class=\"$css\">". $displayQty ."</td>";
	$x .= "<td class=\"$css\">". $displayType ."</td>";
	$x .= "<td class=\"$css\">". $displayStatus ."</td>";
	if($sys_custom['Discipline_AP_Item_Tag']) {
		$x .= "<td class=\"$css\">". $tagName ."</td>";
	}
	$x .= "<td class=\"$css\">";
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value) {
			$x .=$Value.'<br/>';
		}	
	}
	else
	{	
		$x .= "--";
	}
	$x.="</td>";
	$x .= "</tr>";
		
	if(sizeof($error)>0)
	{
		$error_occured++;
	}
	else {
		$sql_fields = "(ItemCode, ItemName, categoryID, ConductMark, StudyScore, ActivityScore, MaxGainDeductSubScore1, Quantity, Type, Status, TagID, UserID, DateInput, ExistingItemCode)";
		$sql_values = "('$itemCode', '$itemName', '$catID', '$conductMark', '$studyScore', '$activityScore', '$maxStudyScore', '$qty', '$type', '$status', '$tagid', '$UserID', NOW(),'$existingItemCode')";
		
		$sql = "insert into temp_award_punishment_settings_item_import $sql_fields values $sql_values ";
		$ldiscipline->db_db_query($sql);
	}
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='importItem.php?MeritType=$MeritType'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".
	$linterface->GET_ACTION_BTN($button_back, "button", "window.location='importItem.php?MeritType=$MeritType'");	
}
?>

<br />
<form name="form1" method="post" action="importItem_update.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>

<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>">
<input type="hidden" name="importMode" id="importMode" value="<?=$importMode?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>