<?
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2015-04-13 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	logic for auto select detention and eNotice option checking
#				fixed - not allow import category with existing category name
#
#	Date	:	2010-06-14 (Henry)
#	Detail 	:	import of Category
#
#####################################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();


$limport = new libimporttext();
$lo = new libfilesystem();
$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Award_Punishment-Access");


$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!isset($MeritType) || $MeritType=="") $MeritType = 1;

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: import.php?xmsg=import_failed&MeritType=$MeritType");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}

// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add auto select detention and eNotice option to file format
//$file_format = array('Category Name','Merit Type','Status','Notice Template');
$file_format = array('Category Name','Merit Type','Status','Default auto select Detention option','Default auto select Send eNotice option','Notice Template');

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: import.php?xmsg=wrong_header&MeritType=$MeritType");
	exit();
}

$ldiscipline = new libdisciplinev12();
$lu = new libuser();

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportCategory']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_CategoryName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Type']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_Status']."</td>";
// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add table header - auto select detention and eNotice option
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['DefaultSetDetention']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['DefaultSendNotice']."</td>";
//$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import_GM_Col'][4]."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import_GM_Col'][6]."</td>";
if($MeritType==-1 &&  $sys_custom['Discipline_SeriousLate']){
// 	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import_GM_Col'][5]."</td>";
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import_GM_Col'][7]."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add auto select detention and eNotice option column field
$sql = "create table IF NOT EXISTS temp_award_punishment_settings_import
		(
			 CategoryName varchar(255),
			 RecordStatus int(11),
			 TemplateID int(11),
			 MeritType int(11),
			 UserID int(11),
			 DateInput datetime,
     		 AutoSelectDetention int(8),
     		 AutoSelecteNotice int(8)
	    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";	    
$ldiscipline->db_db_query($sql);

$fields = mysql_list_fields($intranet_db, 'temp_award_punishment_settings_import');

# delete the temp data in temp table 
$sql = "delete from temp_award_punishment_settings_import where UserID=$UserID";
$ldiscipline->db_db_query($sql) or die(mysql_error());

$error_occured = 0;
$space = ($intranet_session_language=="en") ? " ":"";
$typeAry = array('1'=>$i_Discipline_System_Award_Punishment_Awards,'-1'=>$i_Discipline_System_Award_Punishment_Punishments);
$statusAry = array(1=>$eDiscipline['Setting_Status_Using'],2=>$eDiscipline['Setting_Status_NonUsing']);
// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add $defaultOptionAry
$defaultOptionAry = array(0=>$i_general_no,1=>$i_general_yes);

for($i=0; $i<sizeof($data); $i++)
{
	$error = array();

	### get data
	list($catName,$mType,$status,$defaultDetention,$defaultSendNotice,$templateID) = $data[$i];
	$catName = intranet_htmlspecialchars(trim($catName));
	$mType = trim($mType);
	$status = trim($status);
	// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] get auto select detention and eNotice option value
	$defaultDetention = trim($defaultDetention);
	$defaultSendNotice = trim($defaultSendNotice);
	$templateID = trim($templateID);
	
	# Category Name
	if(trim($catName)=='')
	{
		$error['catName'] = $eDiscipline['Setting_CategoryName'].$space.$ec_html_editor['missing'];	
	}	
	else
	{
		// fixed: check existing category name from correct table
//		$sql = "SELECT COUNT(*) FROM DISCIPLINE_MERIT_CATEGORY WHERE Name='$catName'";
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_MERIT_ITEM_CATEGORY WHERE CategoryName='$catName'";
		$result = $ldiscipline->returnVector($sql);
		
			if($result[0] != 0){
				$error['catName'] = $eDiscipline['Setting_CategoryName'].$space.$Lang['eDiscipline']['Import_AlreadyExist'];
			} else {
				for($a=0, $a_max=$i; $a<$a_max; $a++) {
					if($catName==$data[$a][0]) {
						$error['catName'] = $eDiscipline['Setting_CategoryName'].$space.$Lang['eDiscipline']['Import_AlreadyExist'];
						break;	
					}	
				}
			}
	}
	
	# Merit Type
	$displayType = $mType;
	if(trim($mType)=='') {
		$error['mType'] = $eDiscipline['Type'].$space.$ec_html_editor['missing'];
	} else {
		if(!array_key_exists($mType, $typeAry)) {
			$error['mType'] = $eDiscipline['Type'].$space.$ec_html_editor['incorrect'];
		} else {
			$displayType = $typeAry[$mType];
		}
	}	
	
	# Record Status
	$displayStatus = $status;
	if(trim($status)=='') {
		$error['status'] = $eDiscipline['Setting_Status'].$space.$ec_html_editor['missing'];
	} else {
		if(!array_key_exists($status, $statusAry)) {
			$error['status'] = $eDiscipline['Setting_Status'].$space.$ec_html_editor['incorrect'];
		} else {
			$displayStatus = $statusAry[$status];
		}
	}
	
	# [2015-0128-1044-22170] / [2015-0313-1420-56054] Default auto select Detention option
	if(trim($defaultDetention)!="") {
		if(!array_key_exists($defaultDetention, $defaultOptionAry)) {
			$error['defaultDetention'] = $Lang['eDiscipline']['DefaultSetDetention'].$space.$ec_html_editor['incorrect'];
			$defaultDetentionOption = "---";
		} else {
			$defaultDetentionOption = $defaultOptionAry[$defaultDetention];
		}
	} else {
		$defaultDetentionOption = $i_general_no;
	}
	
	# [2015-0127-1101-48073] Default auto select Send eNotice option
	if(trim($defaultSendNotice)!="") {
		if(!array_key_exists($defaultSendNotice, $defaultOptionAry)) {
			$error['defaultSendNotice'] = $Lang['eDiscipline']['DefaultSendNotice'].$space.$ec_html_editor['incorrect'];
			$defaultSendNoticeOption = "---";
		} else {
			$defaultSendNoticeOption = $defaultOptionAry[$defaultSendNotice];
		}
	} else {
		$defaultSendNoticeOption = $i_general_no;
	}
	
	# eNotice Template 
	if(trim($templateID)!="") {
		$sql = "SELECT TemplateID FROM INTRANET_NOTICE_MODULE_TEMPLATE where TemplateID=$templateID";
		$result = $ldiscipline->returnVector($sql);
		if(!in_array($templateID, $result)) {
			// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
//			$error['templateID'] = $Lang['eDiscipline']['Import_GM_Col'][4].$space.$ec_html_editor['incorrect'];
			$error['templateID'] = $Lang['eDiscipline']['Import_GM_Col'][6].$space.$ec_html_editor['incorrect'];
			$templateName = $templateID;
		} else {
			$templateInfo = $ldiscipline->retrieveTemplateDetails($templateID);
			$templateName = $templateInfo[0]['Title'];
		}
	} else {
		$templateName = "---";
	}	
	

	$css = (sizeof($error)==0) ? "tabletext":"red";
	
	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">". $catName ."</td>";
	$x .= "<td class=\"$css\">". $displayType ."</td>";
	$x .= "<td class=\"$css\">". $displayStatus ."</td>";
	// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] display auto select detention and eNotice option value
	$x .= "<td class=\"$css\">". $defaultDetentionOption ."</td>";
	$x .= "<td class=\"$css\">". $defaultSendNoticeOption ."</td>";
	$x .= "<td class=\"$css\">". $templateName ."</td>";
	$x .= "<td class=\"$css\">";
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .=$Value.'<br/>';
		}	
	}
	else
	{	
		$x .= "--";
	}
	$x.="</td>";
	$x .= "</tr>";
		
	if(sizeof($error)>0)
	{
		$error_occured++;
	} else {
		// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] insert value into temp table
//		$sql_fields = "(CategoryName, RecordStatus, TemplateID, MeritType, UserID, DateInput)";
//		$sql_values = "('$catName', '$status', '$templateID', '$mType', '$UserID', NOW())";
		$sql_fields = "(CategoryName, RecordStatus, AutoSelectDetention, AutoSelecteNotice, TemplateID, MeritType, UserID, DateInput)";
		$sql_values = "('$catName', '$status', '$defaultDetention', '$defaultSendNotice', '$templateID', '$mType', '$UserID', NOW())";
		
		$sql = "insert into temp_award_punishment_settings_import $sql_fields values $sql_values ";
		$ldiscipline->db_db_query($sql);
			
	}
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?MeritType=$MeritType'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".
	$linterface->GET_ACTION_BTN($button_back, "button", "window.location='import.php?MeritType=$MeritType'");	
}


?>

<br />
<form name="form1" method="post" action="import_update.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>