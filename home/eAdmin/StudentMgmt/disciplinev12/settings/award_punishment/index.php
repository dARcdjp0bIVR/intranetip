<?php
// Modifying by : 

########## Change Log ###############
#
#	Date	:	2017-10-13 (Bill)	[DM#3273]
#	Details :	fixed Session Problem in PHP 5.4
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
}
else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
} 

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
}
else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if (!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

/*
$order = $order ? $order : 1;
$field = ($field == "") ? 0 : $field;
*/
$order = ($order == '' || $order != 0) ? 1 : 0;
//$field = ($field == "") ? 0 : $field;
$field = 2;

$maxOrder = $ldiscipline->getAwardPunishCategoryMaxOrder(1);
if($maxOrder=="" || $maxOrder=="0") {
	$maxOrder = "999999999";	
}

$mtype = 1;

$sql  = "SELECT
			CONCAT(
				'<a class=tablelink href=award_category_item_edit.php?merit=1&CatID=',
				a.CatID,
				'>',
				REPLACE(a.CategoryName,'<','&lt;'),'</a>'";
	if($sys_custom['mansung_disciplinev12_conduct_grade']) {
		$sql .= ",IF(a.catID>=24 AND a.CatID<=31,'<span class=\'tabletextrequire\'>*</span>','')";
	}
			$sql .= ") as CatName,
			CONCAT('<a class=tablelink href=award_category_item_item_list.php?CatID=',a.CatID,'>',COUNT(b.ItemID),'</a>') as itemCount,
			CASE(a.DisplayOrder)
				WHEN $maxOrder THEN CONCAT('<a href=category_item_set_priority.php?merit=1&old=',a.DisplayOrder,'&new=1 class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Top\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif border=0></a> <a href=category_item_set_priority.php?merit=1&old=',a.DisplayOrder,'&new=',a.DisplayOrder-1,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Up\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif border=0></a>')
				WHEN 1 THEN CONCAT('<img src={$image_path}/{$LAYOUT_SKIN}/10x10.gif width=32 height=2><a href=category_item_set_priority.php?merit=1&old=1&new=2 class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Down\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif border=0></a> <a href=category_item_set_priority.php?merit=1&old=1&new=',$maxOrder,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Bottom\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif border=0></a>')
				ELSE CONCAT('<a href=category_item_set_priority.php?merit=1&old=',a.DisplayOrder,'&new=1 class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Top\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif border=0></a> <a href=category_item_set_priority.php?merit=1&old=',a.DisplayOrder,'&new=',a.DisplayOrder-1,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Up\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif border=0></a> <a href=category_item_set_priority.php?merit=1&old=',a.DisplayOrder,'&new=',a.DisplayOrder+1,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Down\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif border=0></a> <a href=category_item_set_priority.php?merit=1&old=',a.DisplayOrder,'&new=',$maxOrder,' class=tablelink title=\'$i_Discipline_System_Award_Punishment_Move_Bottom\'><img src={$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif border=0></a>') END,
			IF(a.RecordStatus=2,'".$eDiscipline['Setting_Status_Drafted']."','".$eDiscipline['Setting_Status_Published']."') as RecordStatus,
			CONCAT('<input type=\'checkbox\' name=\'CatID[]\' value=\'', a.CatID ,'\'>'),
			a.CatID
			FROM
				DISCIPLINE_MERIT_ITEM_CATEGORY as a
			LEFT OUTER JOIN DISCIPLINE_MERIT_ITEM as b ON (a.CatID=b.CatID)
			WHERE a.RecordType=1 AND (a.RecordStatus IS NULL || a.RecordStatus!=-1)
			GROUP BY a.CatID
			";

$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("CatName","itemCount","a.DisplayOrder","RecordStatus");
$li->sql = $sql;
//$li->no_col = 5;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
$li->IsColOff = 2;


$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='50%' >$i_Discipline_System_CategoryName</td>\n";$pos++;
$li->column_list .= "<td width='20%'>".$i_Discipline_System_Award_Punishment_No_of_Item."</td>\n";$pos++;
$li->column_list .= "<td width='20%'>$i_general_DisplayOrder</td>\n";
$li->column_list .= "<td width='10%'>".$eDiscipline['Setting_Status']."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("CatID[]")."</td>\n";

if($xmsg2==11)
	$message = $Lang['eDiscipline']['ChangeAPItemStatusWarning'];

# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 1);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}

$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, "index.php", 1);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, "award_approval.php", 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, "award_promotion.php", 0);

$toolbar = $linterface->GET_LNK_NEW("category_item_new.php?merit=1",$i_Discipline_System_Access_Right_New,"","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("import.php?MeritType=1",$Lang['eDiscipline']['ImportCategory'],"","","",0);
$toolbar .= $linterface->GET_LNK_IMPORT("importItem.php?MeritType=1",$Lang['eDiscipline']['ImportItem'],"","","",0);
$toolbar .= $linterface->GET_LNK_EXPORT("export.php?MeritType=1", $Lang['Btn']['Export'], $ParOnClick="", $ParOthers="", $ParClass="", $useThickBox=0);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
<!--
function removeCat(obj,element,page){
		var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
        if(countChecked(obj,element)==0)
                alert(globalAlertMsg2);
        else{
                if(confirm(alertConfirmRemove)){
	            obj.action = page;
                obj.method = "post";
                obj.submit();
                }
        }
}
//-->
</script>
<form name="form1" method="post" action="">
<br />
<table width="98%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="80%"><?= $toolbar ?></td>
								<td width="20%" align="right"><?= $linterface->GET_SYS_MSG($xmsg, $message) ?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="table-action-bar">
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td>&nbsp;</td>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'CatID[]','award_category_item_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:removeCat(document.form1,'CatID[]','category_item_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?=$li->displayFormat_Number_Item($mtype);?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="merit" value="<?=$mtype?>">
<br />
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
