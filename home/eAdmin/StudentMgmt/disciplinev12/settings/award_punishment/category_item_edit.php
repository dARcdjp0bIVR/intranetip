<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(is_array($CatID)) {
	$CatID = $CatID[0];	
} else {
	$CatID = $CatID;	
}

$catName = intranet_htmlspecialchars($ldiscipline->getAwardPunishCategoryName($CatID));

# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

if($merit==1) {
	$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 1);
	$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
	if($sys_custom['Disciplinev12_show_AP_tag']) {
		$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
	}
	$backURL = "index.php";
	$tagURL = "award_tag.php";
	$approvalURL = "award_approval.php";
	$promotionURL = "award_promotion.php";
} else {
	$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 0);
	$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 1);
	$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
	if($sys_custom['Disciplinev12_show_AP_tag']) {
		$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
	}
	$backURL = "punish_category_item.php";
	$tagURL = "punish_tag.php";
	$approvalURL = "punish_approval.php";
	$promotionURL = "punish_promotion.php";
}

$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, $backURL, 1);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, $approvalURL, 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, $promotionURL, 0);

$PAGE_NAVIGATION[] = array($i_Discipline_System_CategoryList, $backURL);
$PAGE_NAVIGATION[] = array($eDiscipline_Settings_GoodConductMisconuct_NAV_EditCategory, "");

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
function checkForm(obj) {
<!--
if(obj.ItemCode.value=="" || obj.ItemCode.value==" ") {
	alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_CategoryName?>");
	obj.ItemCode.focus();
	return false;
}
//-->	
}
</script>
<form name="form1" method="post" action="category_item_edit_update.php" onSubmit="return checkForm(form1)">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_CategoryName?><span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT maxLength="80" name="ItemCode" class="tabletext" value="<?=$catName?>">
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='$backURL'")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="merit" value="<?= $merit?>">
<input type="hidden" name="CatID" value="<?= $CatID?>">
</form>
<?
echo $linterface->FOCUS_ON_LOAD("form1.ItemCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
