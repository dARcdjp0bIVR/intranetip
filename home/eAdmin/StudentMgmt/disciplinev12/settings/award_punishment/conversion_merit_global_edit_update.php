<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$dataAry = array();

	$dataAry[1][] = $ConductScore1;
	$dataAry[2][] = $ConductScore2;
	$dataAry[3][] = $ConductScore3;
	$dataAry[4][] = $ConductScore4;
	$dataAry[5][] = $ConductScore5;
	
if($meritType==1) {
	$dataAry[1][] = '1';
	$dataAry[2][] = '2';
	$dataAry[3][] = '3';
	$dataAry[4][] = '4';
	$dataAry[5][] = '5';
}
else {
	$dataAry[1][] = '-1';
	$dataAry[2][] = '-2';
	$dataAry[3][] = '-3';
	$dataAry[4][] = '-4';
	$dataAry[5][] = '-5';
}

$ldiscipline->updateAwardPunishUpgradeNum($meritType, '', $dataAry);
$ldiscipline->updateConversionPeriod('',$PeriodType, $meritType);


intranet_closedb();

if($meritType=='1') {
	header("Location: award_promotion.php?xmsg=update");
} else {
	header("Location: punish_promotion.php?xmsg=update");
}

?>
