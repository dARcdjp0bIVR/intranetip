<?php
// Using: 

######################################################
#
#	Date:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#			support Study Score Limit update ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date:	2017-03-16	Bill	[2016-1207-1221-39240]
#			support preset Activity Score update ($sys_custom['ActScore'])
#
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$ItemCode = $_POST['ItemCode'];
$ItemName = $_POST['ItemName'];
$MeritType = $_POST['meritType'];
$ConductScore = $_POST['ConductScore'];
$StudyScore = $_POST['StudyScore'];
$ActivityScore = $_POST['ActivityScore'];
$MeritNum = $_POST['MeritNum'];
$mtype = $_POST['merit'];
$CatID = $_POST['CatID'];
$TagID = $_POST['TagID'];
$Status = $_POST['Status'];
$MaxStudyScore = $_POST['MaxStudyScore'];

if ($mtype!=-1) $mtype=1;

### Check if Item Code duplicate
$dup_result = $ldiscipline->checkDuplicateItemCode($ItemCode, '');
if($dup_result==0)
{
	$ldiscipline->editCategoryItem('', $CatID, $ItemCode, $ItemName, $ConductScore, $MeritNum, $MeritType, $mtype, $StudyScore, $TagID, $Status, "", $ActivityScore, $MaxStudyScore);
	header("Location: punish_category_item_item_list.php?xmsg=add&CatID=$CatID");
}
else
{
	header("Location: punish_category_item_item_new.php?xmsg2=duplicate Item code&CatID=$CatID");
}

intranet_closedb();
?>