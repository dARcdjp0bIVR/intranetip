<?php
// Modifying by : 

/********************************************
 *  modification log
 * 	20171013 Bill:	[DM#3273]
 * 		- fixed Session Problem in PHP 5.4
 * 	20100127 Marcus:
 * 		- modified coding to show conversion period on table
 * ******************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
}
else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
}
else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();
$lstudentprofile = new libstudentprofile();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;

# what method (global or category) is using for "Conversion"
$method = $ldiscipline->getPromotionMethod(1);

$type1 = (!$lstudentprofile->is_merit_disabled) ? "1" : "0";
$type2 = (!$lstudentprofile->is_min_merit_disabled) ? "1" : "0";
$type3 = (!$lstudentprofile->is_maj_merit_disabled) ? "1" : "0";
$type4 = (!$lstudentprofile->is_sup_merit_disabled) ? "1" : "0";
$type5 = (!$lstudentprofile->is_ult_merit_disabled) ? "1" : "0";

$type[] = $type1;
$type[] = $type2;
$type[] = $type3;
$type[] = $type4;
$type[] = $type5;

$ldiscipline->checkMeritTypeExist();

$li = new libdbtable2007($field, $order, $pageNo);

if($method=="category") {

	$editURL = "conversion_merit_edit.php";
	
	$sql  = "SELECT	CONCAT('<a class=tablelink href=conversion_merit_edit.php?merit=1&CatID=', a.CatID, '>', REPLACE(a.CategoryName,'<','&lt;'), '</a>') as CatName,
			";
	if($type2) {$sql .= "a.CatID as num2,"; }
	if($type3) {$sql .= "a.CatID as num3,"; }
	if($type4) {$sql .= "a.CatID as num4,"; }
	if($type5) {$sql .= "a.CatID as num5,"; }
	$sql .= " 
		Case a.ConversionPeriod
			WHEN 0 THEN '".$Lang["eDiscipline"]["ByYear"]."'
			WHEN 1 THEN '".$Lang["eDiscipline"]["BySemester"]."'
			WHEN 2 THEN '".$Lang["eDiscipline"]["ByMonth"]."'
			WHEN 3 THEN '".$Lang["eDiscipline"]["BySeparatedMonth"]."' 
		END as ConversionPeriod,
	";
	$sql .= "CONCAT('<input type=\'checkbox\' name=\'CatID[]\' value=\'', a.CatID ,'\'>')
				FROM
					DISCIPLINE_MERIT_ITEM_CATEGORY as a
				LEFT OUTER JOIN DISCIPLINE_MERIT_PROMOTION_BY_CATEGORY as b ON (a.CatID=b.CatID)
				WHERE a.RecordType=1 AND (a.RecordStatus IS NULL OR a.RecordStatus!=-1)
				GROUP BY a.CatID
				";
	$li->field_array[] = "CatName";
	$li->column_array[] = "0";
	$li->wrap_array[] = "0";
	$li->column_list .= "<td width='3%' class='tabletoplink'>#</td>\n";
	$li->column_list .= "<td>".$li->column($pos++, $i_Discipline_System_CategoryName)."</td>\n";

}
else {
	
	$editURL = "conversion_merit_global_edit.php";
	
	$sql  = "SELECT ";
	if($type2) {$sql .= "MeritType as num2,"; }
	if($type3) {$sql .= "MeritType as num3,"; }
	if($type4) {$sql .= "MeritType as num4,"; }
	if($type5) {$sql .= "MeritType as num5,"; }
	$sql .= "	
		Case b.SettingValue
			WHEN 0 THEN '".$Lang["eDiscipline"]["ByYear"]."'
			WHEN 1 THEN '".$Lang["eDiscipline"]["BySemester"]."'
			WHEN 2 THEN '".$Lang["eDiscipline"]["ByMonth"]."'
			WHEN 3 THEN '".$Lang["eDiscipline"]["BySeparatedMonth"]."' 
		END as ConversionPeriod, ";
	$sql .= "CONCAT('<input type=\'checkbox\' name=\'CatID[]\' value=\'', MeritType ,'\'>')
				FROM
					DISCIPLINE_MERIT_TYPE_SETTING,
					DISCIPLINE_GENERAL_SETTING b
				WHERE
					MeritType=0
					AND b.SettingName = 'AwardConversionPeriod'
				";
			
	$li->column_list .= "<td width='3%' class='tabletoplink'>#</td>\n";
}

if($type2) {
	$li->field_array[] = "num2";
	$li->column_array[] = "0";
	$li->wrap_array[] = "0";
}
if($type3) {
	$li->field_array[] = "num3";
	$li->column_array[] = "0";
	$li->wrap_array[] = "0";
}
if($type4) {
	$li->field_array[] = "num4";
	$li->column_array[] = "0";
	$li->wrap_array[] = "0";
}
if($type5) {
	$li->field_array[] = "num5";
	$li->column_array[] = "0";
	$li->wrap_array[] = "0";
}

$li->field_array[] = "ConversionPeriod";
$li->column_array[] = "0";
$li->wrap_array[] = "0";


$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->IsColOff = 2;

$pos = 0;
if($type2) {
	$li->column_list .= "<td>{$i_general_no_of}{$i_Merit_Merit}<br><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\"> {$i_Merit_MinorCredit}</td>\n";
}
if($type3) {
	$li->column_list .= "<td>{$i_general_no_of}{$i_Merit_MinorCredit}<br><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\"> {$i_Merit_MajorCredit}</td>\n";
}
if($type4) {
	$li->column_list .= "<td>{$i_general_no_of}{$i_Merit_MajorCredit}<br><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\"> {$i_Merit_SuperCredit}</td>\n";
}
if($type5) {
	$li->column_list .= "<td>{$i_general_no_of}{$i_Merit_SuperCredit}<br><img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_next_off.gif\"> {$i_Merit_UltraCredit}</td>\n";
}
$li->column_list .= "<td>{$Lang["eDiscipline"]["ConversionPeriod"]}</td>\n";
$li->column_list .= "<td width='1'>".$li->check("CatID[]")."</td>\n";

//echo $sql;

if(is_array($CatID)) {
	$CatID = $CatID[0];	
} else {
	$CatID = $CatID;	
}

$catName = $ldiscipline->getAwardPunishCategoryName($CatID);

$method = $ldiscipline->getPromotionMethod(1);
if($method == 'category') {		# category
	$conversionMethodText = $i_Discipline_System_Award_Punishment_Setting_Category;
} else {						# global
	$conversionMethodText = $i_Discipline_System_Award_Punishment_Setting_Global;
}


# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 1);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}

$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, "index.php", 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, "award_approval.php", 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, "award_promotion.php", 1);

$PAGE_NAVIGATION[] = array($i_Discipline_System_CategoryList, $backURL);
$PAGE_NAVIGATION[] = array($i_Discipline_System_NewCategory, "");

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
function checkForm(obj) {
<!--
if(obj.ItemCode.value=="" || obj.ItemCode.value==" ") {
	alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_CategoryName?>");
	obj.ItemCode.focus();
	return false;
}
//-->	
}
</script>
<form name="form1" method="post" action="">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
				<tr>
					<td align="right"><?= $linterface->GET_SYS_MSG($xmsg, $xmsg2) ?>
					</td>
				</tr>
			</table>
			<br />
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="table-action-bar">
					<td class="tabletext"><?=$i_Discipline_System_Award_Punishment_Conversion_Method?> : <span class="tabletextrequire2"><?= $conversionMethodText?><a href="conversion_method_edit.php?merit=1" class="tabletool"> <img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit_b.gif" width="20" height="20" border="0" align="absmiddle"></a></span></td>
					<td align="right" valign="bottom">
						<table border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
								<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
									<table border="0" cellspacing="0" cellpadding="2">
										<tr>
											<td nowrap>
												<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'CatID[]','<?=$editURL?>')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a>
											</td>
										</tr>
									</table>
								</td>
								<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td><?= $li->displayFormat_AwardPunishPromotion(sizeof($li->field_array), $type, 1)?>
		</td>
	</tr>
	<tr>
		<td align="left" class="tabletextremark">&nbsp;</td>
	</tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
<input type="hidden" name="merit" value="1">
</form>
<?

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
