<?php
// Modifying by: 

#############################################
#
#	Date	:	2017-07-13	(Bill)	[DM#3233]
#	Detail 	:	display "---" for merit type = -999
#
#	Date	:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#	Detail 	:	support import items with Study Score Limit ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date	:	2017-03-17	(Bill)	[2016-1207-1221-39240]
#	Detail 	:	support import item with Activity Score
#
#	Date	:	2016-03-09 (Kenneth)
#	Detail 	:	add update import
#
#	Date	:	2010-06-14 (Henry)
#	Detail 	:	import of Item
#
#############################################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-Award_Punishment-Access");

$sql = "select * from temp_award_punishment_settings_item_import where UserID=$UserID";
$recordArr = $ldiscipline->returnArray($sql);

if (!$lsp->is_merit_disabled)
{
	$mType['1'] = $i_Merit_Merit;
}
if (!$lsp->is_min_merit_disabled)
{
	$mType['2'] = $i_Merit_MinorCredit;
}
if (!$lsp->is_maj_merit_disabled)
{
	$mType['3'] = $i_Merit_MajorCredit;
}
if (!$lsp->is_sup_merit_disabled)
{
	$mType['4'] = $i_Merit_SuperCredit;
}
if (!$lsp->is_ult_merit_disabled)
{
	$mType['5'] = $i_Merit_UltraCredit;
}
if (!$lsp->is_warning_disabled)
{
	$mType['0'] = $i_Merit_Warning;
}
if (!$lsp->is_black_disabled)
{
	$mType['-1'] = $i_Merit_BlackMark;
}
if (!$lsp->is_min_demer_disabled)
{
	$mType['-2'] = $i_Merit_MinorDemerit;
}
if (!$lsp->is_maj_demer_disabled)
{
	$mType['-3'] = $i_Merit_MajorDemerit;
}
if (!$lsp->is_sup_demer_disabled)
{
	$mType['-4'] = $i_Merit_SuperDemerit;
}
if (!$lsp->is_ult_demer_disabled)
{
	$mType['-5'] = $i_Merit_UltraDemerit;
}

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
if($importMode=='update'){
$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Import']['ExistingItemCode']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_ItemCode."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_ItemName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['Accumulative_Category']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Conduct_Mark']."</td>";
if($ldiscipline->UseSubScore) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_Subscore1."</td>";
}
// [2016-1125-0939-00240]
if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['MeritItemMaxStudyScore']."</td>";
}
if($ldiscipline->UseActScore) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['ActivityScore']."</td>";
}
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Type']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_Quantity."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['MeritType']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_Status']."</td>";
if($sys_custom['Discipline_AP_Item_Tag']) {
	$x .= "<td class=\"tablebluetop tabletopnolink\">".$Lang['eDiscipline']['Tag']."</td>";
}
$x .= "</tr>";

$space = ($intranet_session_language=="en") ? " ":"";
$typeAry = array('1'=>$i_Discipline_System_Award_Punishment_Awards, '-1'=>$i_Discipline_System_Award_Punishment_Punishments);
$statusAry = array(1=>$eDiscipline['Setting_Status_Using'], 2=>$eDiscipline['Setting_Status_NonUsing']);

$sql = "SELECT TagID, TagName FROM DISCIPLINE_ITEM_TAG_SETTING";
$result = $ldiscipline->returnArray($sql);
$tagAry = build_assoc_array($result);

for($a=0;$a<sizeof($recordArr);$a++)
{
	unset($dataAry);
	
	$tagName = "";
	$catInfo = $ldiscipline->getAPCategoryInfoBYCatID($recordArr[$a]['CategoryID']);
	
	$importedTagAry = explode(',', $recordArr[$a]['TagID']);
	for($j=0; $j<sizeof($importedTagAry); $j++) {
		$importedTagID = $importedTagAry[$j];
		
		if(!array_key_exists($importedTagID, $tagAry)) {
			$error['tagid'] = $Lang['eDiscipline']['Import_AP_Item_Col'][9].$space.$ec_html_editor['incorrect'];
			$tagName .= ($tagName!="") ? ", ".$importedTagID : $importedTagID;
		}
		else {
			$tagName .= ($tagName!="") ? ", ".$tagAry[$importedTagID] : $tagAry[$importedTagID];
		}
	}
	
	$x .= "<tr class=\"tablebluerow".($a%2+1)."\">";
	$x .= "<td class=\"tabletext\">".($a+1)."</td>";
	if($importMode=='update'){
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['ExistingItemCode']."</td>";
	}
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ItemCode']."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ItemName']."</td>";
	$x .= "<td class=\"tabletext\">". $catInfo['CategoryName']."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['ConductMark']."</td>";
	if($ldiscipline->UseSubScore) {
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['StudyScore']."</td>";
	}
	// [2016-1125-0939-00240]
	if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['MaxGainDeductSubScore1']."</td>";
	}
	if($ldiscipline->UseActScore) {
		$x .= "<td class=\"tabletext\">". $recordArr[$a]['ActivityScore']."</td>";
	}
	$x .= "<td class=\"tabletext\">". $typeAry[$catInfo['RecordType']]."</td>";
	$x .= "<td class=\"tabletext\">". $recordArr[$a]['Quantity']."</td>";
	$x .= "<td class=\"tabletext\">". ($recordArr[$a]['Type']=="-999"? "---" : $mType[$recordArr[$a]['Type']]) ."</td>";
	$x .= "<td class=\"tabletext\">". $statusAry[$recordArr[$a]['Status']] ."</td>";
	if($sys_custom['Discipline_AP_Item_Tag']) {
		$x .= "<td class=\"tabletext\">". $tagName ."</td>";
	}
	$x .= "</tr>";

	$meritType = ($type>0) ? "1" : "-1";
	
	$insertTagAry = explode(',', $recordArr[$a]['TagID']);
	if($importMode=='new'){
		$ldiscipline->editCategoryItem('', $recordArr[$a]['CategoryID'], $recordArr[$a]['ItemCode'], $recordArr[$a]['ItemName'], $recordArr[$a]['ConductMark'], $recordArr[$a]['Quantity'], $recordArr[$a]['Type'], $catInfo['RecordType'], $recordArr[$a]['StudyScore'], $insertTagAry, $recordArr[$a]['Status'], "", $recordArr[$a]['ActivityScore'], $recordArr[$a]['MaxGainDeductSubScore1']);	
	}
	else if($importMode=='update'){
		$itemInfo = $ldiscipline->getAwardPunishItemByItemCode($recordArr[$a]['ExistingItemCode']);
		$itemID = $itemInfo['ItemID'];
		$ldiscipline->editCategoryItem($itemID, $recordArr[$a]['CategoryID'], $recordArr[$a]['ItemCode'], $recordArr[$a]['ItemName'], $recordArr[$a]['ConductMark'], $recordArr[$a]['Quantity'], $recordArr[$a]['Type'], $catInfo['RecordType'], $recordArr[$a]['StudyScore'], $insertTagAry, $recordArr[$a]['Status'], "", $recordArr[$a]['ActivityScore'], $recordArr[$a]['MaxGainDeductSubScore1']);
	}
}

$x .= "</table>";

$xmsg="import_success";

$linterface = new interface_html();

$url = ($MeritType==1) ? "index.php" : "punish_category_item.php";

$import_button = $linterface->GET_ACTION_BTN($button_finish, "button","self.location='$url'")."&nbsp;".
					$linterface->GET_ACTION_BTN($iDiscipline['ImportOtherRecords'], "button", "window.location='importItem.php?MeritType=$MeritType'");

# Menu Highlight
$CurrentPage = "Settings_AwardPunishment";
$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Tag Information
$TAGS_OBJ[] = array($eDiscipline['Award_and_Punishment']);
$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportItem']);

# Step Information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 1);

$linterface->LAYOUT_START();
?>

<form name="form1" method="post">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg,$xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td><?= $x ?></td>
	</tr>
	<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
	</tr>
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>