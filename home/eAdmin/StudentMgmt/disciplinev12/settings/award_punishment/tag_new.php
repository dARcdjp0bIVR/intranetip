<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}


# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 0);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 1);
}

if($merit==1) {
	$backURL = "tag.php";
	$tagURL = "tag.php";
	$approvalURL = "award_approval.php";
	$promotionURL = "award_promotion.php";
} else {
	$backURL = "tag.php";
	$tagURL = "tag.php";
	$approvalURL = "punish_approval.php";
	$promotionURL = "punish_promotion.php";
}


$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['TagList'], $backURL);
$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['NewTag'], "");

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
function checkForm(obj) {
<!--
if(obj.TagName.value=="" || obj.TagName.value==" ") {
	alert("<?=$i_alert_pleasefillin ?><?=$Lang['eDiscipline']['TagName']?>");
	obj.TagName.focus();
	return false;
}
//-->	
}
</script>
<form name="form1" method="post" action="tag_new_update.php" onSubmit="return checkForm(form1)">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['TagName']?><span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT maxLength="80" name="TagName" class="tabletext">
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='$backURL'")?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="merit" value="<?= $merit?>">
</form>
<?
echo $linterface->FOCUS_ON_LOAD("form1.TagName"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
