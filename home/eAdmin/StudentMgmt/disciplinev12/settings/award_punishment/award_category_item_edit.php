<?php
# using : 

########## Change Log ###############
#
#	Date	:	2015-04-10 (Bill)	[2015-0127-1101-48073]
#	Detail	:	add auto select eNotice option
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	can choose default eNtotice Template
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(is_array($CatID)) {
	$CatID = $CatID[0];	
} else {
	$CatID = $CatID;	
}
if($CatID == "") {
	header("Location: index.php");
}

$catName = $ldiscipline->getAwardPunishCategoryName($CatID);
$catName = str_replace('"','&quot;',$catName);

$catInfo = $ldiscipline->getAPCategoryInfoBYCatID($CatID);
$Status = $catInfo['RecordStatus'];

$catData = $ldiscipline->getAPCategoryInfoBYCatID($CatID);
$TemplateID = $catData['TemplateID'];
if($TemplateID=="") $TemplateID = 0;
$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);
$TemplateCategoryID = $TemplateAry[0]['CategoryID'];

// [2015-0127-1101-48073] - get auto select eNotice option value
$catDefaultOption = $ldiscipline->getAPCategoryInfoBYCatID($CatID);
$DefaultSendNotice = $catDefaultOption['AutoSelecteNotice'];

# eNotice Template
$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		# check the Template Catgory has template or not
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp))
			$catArr[] = array($Key,$Key);
	}
}
$templateCat = $linterface->GET_SELECTION_BOX($catArr, 'id="TemplateCategoryID", name="TemplateCategoryID" onChange="changeCat(this.value)"', "", $TemplateCategoryID);

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	for ($j=0; $j<=sizeof($result); $j++) {
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
			//$js .= "obj.options[$j] = new Option('-- $button_select --',0);\n";
		} else {
			$tempTemplate = $result[$j-1][1];
			$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
	        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
	        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
	        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);

			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
			//$js .= "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
		}
	}
}


# menu highlight setting
$CurrentPage = "Settings_AwardPunishment";

$TAGS_OBJ[] = array($i_Merit_Award, "index.php", 1);
$TAGS_OBJ[] = array($i_Merit_Punishment, "punish_category_item.php", 0);
$TAGS_OBJ[] = array($Lang['eDiscipline']['ApprovalGroup'], "approvalGroup.php", 0);
if($sys_custom['Disciplinev12_show_AP_tag']) {
	$TAGS_OBJ[] = array($Lang['eDiscipline']['Tag'], "tag.php", 0);
}

$backURL = "index.php";
$tagURL = "award_tag.php";
$approvalURL = "award_approval.php";
$promotionURL = "award_promotion.php";

$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Category_Item, $backURL, 1);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Approval, $approvalURL, 0);
$subTag[] = array($i_Discipline_System_Award_Punishment_Submenu_Promotion, $promotionURL, 0);

$PAGE_NAVIGATION[] = array($i_Discipline_System_CategoryList, $backURL);
$PAGE_NAVIGATION[] = array($eDiscipline_Settings_GoodConductMisconuct_NAV_EditCategory, "");

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();
?>
<script language="javascript">
<?=$jTemplateString?>

<!--
function checkForm(obj) {
	if(obj.ItemCode.value=="" || obj.ItemCode.value==" ") {
		alert("<?=$i_alert_pleasefillin ?><?=$i_Discipline_System_CategoryName?>");
		obj.ItemCode.focus();
		return false;
	}
	if((obj.TemplateCategoryID.value==0 && obj.SelectNotice.value!=0) || (obj.TemplateCategoryID.value!=0 && obj.SelectNotice.value==0)) {
		alert("<?=$i_alert_pleaseselect." ".$eDiscipline['eNoticeTemplate']?>");
		return false;	
	}
}

function changeCat(cat){

	var x=document.getElementById("SelectNotice");

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}


	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];

		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}

	/****************
	obj = x;
	<?
	
	for ($i=0; $i<sizeof($catArr); $i++) {
		
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);?>
		
		if(cat=="<?=$catArr[$i][0]?>") {
			<?
			for ($j=0; $j<=sizeof($result); $j++) { 
				if($result[$j][0]==0) {
					echo "obj.options[0] = new Option('-- $button_select --',0);\n";	
				} else {
					$tempTemplate = $result[$j-1][1];
					echo "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
				}
			}
			
		?> } <?
	}
	
	?>
	****************/
}

function itemSelected(itemID) {
	var obj = document.form1;
	var item_length = obj.SelectNotice.length;
	for(var i=0; i<item_length; i++) {
		if(obj.elements['SelectNotice'].options[i].value==<?=$TemplateID?>) {
			obj.elements['SelectNotice'].selectedIndex = i;
			//alert(i);
			break;	
		}
	}
}

//-->	
</script>
<form name="form1" method="post" action="category_item_edit_update.php" onSubmit="return checkForm(form1)">
<br />
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr> 
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_CategoryName?><span class="tabletextrequire">* </span></td>
								<td class="tabletext"><INPUT maxLength="80" name="ItemCode" class="tabletext" value="<?=$catName?>">
								</td>
							</tr>
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Setting_Status']?><span class="tabletextrequire">* </span></td>
								<td class="tabletext">
									<input type="radio" name="Status" value="2" id='Status2' <? if($Status==2) echo " checked";?>><label for="Status2"><?=$eDiscipline['Setting_Status_Drafted'];?></label>
									<input type="radio" name="Status" value="1" id='Status1' <? if($Status==1 || $Status=="") echo " checked";?>><label for="Status1"><?=$eDiscipline['Setting_Status_Published'];?></label>
								</td>
							</tr>
							
							<!-- Default Set Send eNotice -->
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['DefaultSendNotice']?></td>
								<td class="tabletext">
									<input type="radio" name="DefaultSendNotice" value="0" id='DefaultSendNotice0' <? if(!$DefaultSendNotice) echo " checked";?>><label for="DefaultSendNotice0"><?=$Lang['General']['No'];?></label>
									<input type="radio" name="DefaultSendNotice" value="1" id='DefaultSendNotice1' <? if($DefaultSendNotice) echo " checked";?>><label for="DefaultSendNotice1"><?=$Lang['General']['Yes'];?></label>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['eNoticeTemplate']?></td>
								<td class="tabletext">
									<?=$templateCat?>
									<select name="SelectNotice" id="SelectNotice"><option value="0">-- <?=$button_select?> --</option></select>
								</td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span class="dotline">
									<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
									<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='$backURL'")?>
									</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type="hidden" name="merit" value="<?= $merit?>">
<input type="hidden" name="CatID" value="<?= $CatID?>">
</form>
<?
if($TemplateCategoryID!="0" && $TemplateCategoryID!="") {
	echo "<script language='javascript'>changeCat('$TemplateCategoryID');itemSelected('$TemplateID')</script>";
}


echo $linterface->FOCUS_ON_LOAD("form1.ItemCode"); 

$linterface->LAYOUT_STOP();
intranet_closedb();
?>
