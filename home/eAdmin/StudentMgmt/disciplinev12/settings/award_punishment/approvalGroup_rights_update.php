<?php
// editing by : henry chow

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

//$result = $ldiscipline->Approval_Group_Add_Member($GroupID, $student);

if(!isset($GroupID) || $GroupID=="") {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();		
}

$APCategoryRight = array();
if(isset($meritCategory) && sizeof($meritCategory)>0) {
	$APCategoryRight = array_merge($APCategoryRight, $meritCategory);	
}
if(isset($demeritCategory) && sizeof($demeritCategory)>0) {
	$APCategoryRight = array_merge($APCategoryRight, $demeritCategory);	
}

$ldiscipline->Start_Trans();
$result = array();

$result[] = $ldiscipline->DELETE_APPROVAL_GROUP_RIGHT($GroupID);
if(sizeof($APCategoryRight) > 0) {
	$result[] = $ldiscipline->ASSIGN_APPROVAL_GROUP_RIGHT($GroupID, $APCategoryRight);
}

if(in_array(false, $result)) {
	$ldiscipline->RollBack_Trans();
	$msg = "UpdateUnsuccess";	
} else {
	$ldiscipline->Commit_Trans();
	$msg = "UpdateSuccess";	
}

intranet_closedb();

header("Location: approvalGroup_rights.php?GroupID=$GroupID&msg=$msg");
?>
