<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}


if(($sys_custom['sdbnsm_disciplinev12_monthly_ap_report_cust'] && (in_array('3', $CatID) || in_array('20', $CatID))) || ($sys_custom['tsk_disciplinev12_class_summary'] && (in_array('20', $CatID) || in_array('21', $CatID) || in_array('22', $CatID) || in_array('13', $CatID) || in_array('14', $CatID) || in_array('15', $CatID) || in_array('17', $CatID) || in_array('18', $CatID)))) {	# these categories cannot be deleted	
	if($merit == 1) {
		header("Location: index.php?xmsg=delete_failed");
	} else {
		header("Location: punish_category_item.php?xmsg=delete_failed");
	}
	exit();
}

$ldiscipline->editAwardPunishCategory("delete", $CatID);

intranet_closedb();


if($merit == 1) {
	header("Location: index.php?xmsg=delete");
} else {
	header("Location: punish_category_item.php?xmsg=delete");
}

?>
