<?php
// Modifying by: 

######################################################
#
#	Date:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#			export Study Score Limit ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
#
#	Date:	2017-03-16	Bill	[2016-1207-1221-39240]
#			support preset Activity Score export ($sys_custom['ActScore'])
#
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$lexport = new libexporttext();

if (!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-Award_Punishment-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$returnArr = $ldiscipline->getAwardPunishmentItemInfo($recordType=$MeritType);
$resultAssoArr = BuildMultiKeyAssoc($returnArr, array('CatID','ItemID'), $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);

$headerArr='';
$headerArr[]='Item Code';
$headerArr[]='Item Name';
$headerArr[]='Category ID';
$headerArr[]='Conduct Mark';
// [2016-1125-0939-00240]
if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$headerArr[]='Point';
	$headerArr[]='Point Limit';
}
else {
	($ldiscipline->UseSubScore)?$headerArr[]='Study Score':'';
}
($ldiscipline->UseActScore)?$headerArr[]='Activity Score':'';
$headerArr[]='Quantity';
$headerArr[]='Type';
$headerArr[]='Status';
($sys_custom['Discipline_AP_Item_Tag'])?$headerArr[]='Tag ID':'';

$counter = 0;
foreach((array)$resultAssoArr as $_catID => $dataArrInSameCat){
	//debug_pr($_catID);
	foreach((array)$dataArrInSameCat as $__ItemID => $dataArrInSameItem){
		//debug_pr($__ItemID);
		
		$exportArr[$counter][] = $dataArrInSameItem[0]['ItemCode'];
		$exportArr[$counter][] = $dataArrInSameItem[0]['ItemName'];
		$exportArr[$counter][] = $dataArrInSameItem[0]['CatID'];
		$exportArr[$counter][] = $dataArrInSameItem[0]['ConductScore'];
		($ldiscipline->UseSubScore)?$exportArr[$counter][] = $dataArrInSameItem[0]['SubScore1']:'';
		// [2016-1125-0939-00240]
		($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore'])?$exportArr[$counter][] = $dataArrInSameItem[0]['MaxGainDeductSubScore1']:'';
		($ldiscipline->UseActScore)?$exportArr[$counter][] = $dataArrInSameItem[0]['SubScore2']:'';
		$exportArr[$counter][] = $dataArrInSameItem[0]['NumOfMerit'];
		$exportArr[$counter][] = $dataArrInSameItem[0]['RelatedMeritType'];
		$exportArr[$counter][] = $dataArrInSameItem[0]['RecordStatus'];
		if($sys_custom['Discipline_AP_Item_Tag']){
			$tagInSingleLine = '';
			$numOfTag = count($dataArrInSameItem);
			for($i=0;$i<$numOfTag;$i++){
				//debug_pr($dataArrInSameItem[$i]['TagID']);
				if($i!=0){
					$tagInSingleLine .= ',';
				}
				$tagInSingleLine .=$dataArrInSameItem[$i]['TagID'];
			}
			$exportArr[$counter][] = $tagInSingleLine;
		}
		$counter++;
	}
}

$export_text = $lexport->GET_EXPORT_TXT($exportArr, $headerArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "export.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);

?>