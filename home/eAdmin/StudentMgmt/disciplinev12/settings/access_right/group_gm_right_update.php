<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$GroupID) {
	header("Location: group_access_right.php");
}

$good_cats = $ldiscipline->RETRIEVE_ALL_CONDUCT_CATEGORY(1);
$mis_cats = $ldiscipline->RETRIEVE_ALL_CONDUCT_CATEGORY(-1, 1);

# good conduct 
for($i=0; $i<sizeof($good_cats); $i++) {
	$items = $ldiscipline->getGMItemByCategoryIDWithCheckHomework($good_cats[$i][0]);
	for($j=0; $j<sizeof($items); $j++) {
		$itemName = ${"good_cat_".$good_cats[$i][0]."_".$items[$j][1]};
		if($itemName) {
			$dataAry[$good_cats[$i][0]][] = $items[$j][1];	
		}
	}
}

# late
if($mis_cat_1) {
	$dataAry[1][] = 0;	
}

# misconduct 
for($i=0; $i<sizeof($mis_cats); $i++) {
	$items = $ldiscipline->getGMItemByCategoryIDWithCheckHomework($mis_cats[$i][0]);
	for($j=0; $j<sizeof($items); $j++) {
		$itemName = ${"mis_cat_".$mis_cats[$i][0]."_".$items[$j][1]};
		if($itemName) {
			$dataAry[$mis_cats[$i][0]][] = $items[$j][1];	
		}
	}
}

$ldiscipline->updateGMAccessRight($GroupID, $dataAry);

intranet_closedb();

header("Location: group_gm_right.php?GroupID=$GroupID&xmsg=update");
?>