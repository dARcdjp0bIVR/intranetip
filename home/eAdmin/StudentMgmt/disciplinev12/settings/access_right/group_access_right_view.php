<?php 
# using: 

/********************** Change Log ***********************
#
#	Date	:	2018-10-04 [Bill]   [2018-0920-1312-34207]
#				- add new AP report > Detention Punishment List
#
#	Date	:	2016-12-20 [Bill]	[2016-1117-1427-18066]
#				- add new AP report > CWC 2 Cust Reports
#
#	Date	:	2015-08-25 [Bill]	[2015-0820-1038-41206]
#				- add new AP report > TKP 2 Cust Reports
#
#	Date	:	2010-12-28 [YatWoon]
#				- add new AP report > AP Class Report
#
#	Date	:	2010-12-23 [YatWoon]
#				- add new GM report > Daily Accumulative Misconduct Report (from eDis1.0)
#
******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}
$GroupID = $_GET['GroupID'];
$thisModule = 'Discipline';

$show = 0;
$showTDContent = array();
$showTDContent[0][0] = "tabletextremark";
$showTDContent[0][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' align='absmiddle'>";
$showTDContent[1][0] = "tablegreenrow3";
$showTDContent[1][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif' width='20' height='20' align='absmiddle'>";

$GroupDetails = $ldiscipline->getAccessRightGroupInfo($GroupID);
list($GroupTitle, $GroupDescription, $no_of_members) = $GroupDetails[0];
$GroupTitle = str_replace("<","&lt;",$GroupTitle);
$GroupDescription = str_replace("<","&lt;",$GroupDescription);
$GroupDescription = nl2br($GroupDescription);

#####
$tableMGMT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableMGMT .= "<tr class='tabletop'>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Award_Punish ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Case_Record ."</td>";
$tableMGMT .= "<td valign='top'>".(($ldiscipline->UseSubScore) ? $Lang['eDiscipline']['ConductMarkStudyScore'] : $i_Discipline_System_Access_Right_Conduct_Mark) ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Detention_Management ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Detention_Session_Arrangement ."</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td1'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td2'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td3'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td4'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td5'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','New','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td6'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','New','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td7'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','New','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td8'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New."</td>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','New','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0;}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td9'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New."</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td10'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit." ";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='EditAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td11'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit." ";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='EditAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td12'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit." ";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='EditAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}

$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','EditAll','EditOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td13'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit." ";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='EditAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td14'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete." ";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td15'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete." ";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td16'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete." ";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','DeleteAll','DeleteOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td17'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete." ";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Waive','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td18'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Waive."</td>";

if($ldiscipline->use_newleaf) {
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','NewLeaf','');
	if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
	$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td48'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_NewLeaf."</td>";
} else {
	$tableMGMT .= "<td valign='top'>&nbsp;</td>";
}

$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Release','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td19'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Release."</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','Release','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='46'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Release."</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Approval','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td20'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Approval."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','GoodConduct_Misconduct','Approval','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td21'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Approval."</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','Finish','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td47'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Finish."</td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Award_Punishment','Lock','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td22'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Lock."</td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','Lock','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td23'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Lock."</td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Conduct_Mark','Adjust','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td24'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Adjust."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','TakeAttendance','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td25'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Take_Attendance."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','SETTINGS','Detention','Access','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td43'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Access."</td>";

$tableMGMT .= "</tr><tr class='tablerow1'>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','NewNotes','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td26'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_New_Notes."</td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Detention','RearrangeStudent','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td27'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Rearrange_Student."</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','EditNotesAll','EditNotesOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td28'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Edit_Notes." ";
if($tempResult[0][1]=='EditNotesOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='EditNotesAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow1'>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','MGMT','Case_Record','DeleteNotesAll','DeleteNotesOwn');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td29'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Delete_Notes." ";
if($tempResult[0][1]=='DeleteNotesOwn') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_Own.")</td>";} else if($tempResult[0][1]=='DeleteNotesAll') { $tableMGMT .= "(".$i_Discipline_System_Access_Right_All.")</td>";}
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr>";
$tableMGMT .= "</table>";

$tableSTAT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSTAT .= "<tr class='tabletop'>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSTAT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td30'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td31'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td32'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','STAT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td33'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tableSTAT .= "</tr></table>";

$tableREPORTS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableREPORTS .= "<tr class='tabletop'>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Report_Type_Personal." /<br> ".$eDiscipline['StudentReport']."</td>";
$tableREPORTS .= "<td>".$eDiscipline['ClassSummary']."</td>";
$tableREPORTS .= "<td>".$Lang['eDiscipline']['ConductMarkReport']."</td>";
$tableREPORTS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Award_Punishment_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['Report']." - $i_Discipline_System_Access_Right_View</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','GoodConduct_Misconduct_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['Report']." - $i_Discipline_System_Access_Right_View</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','StudentReport','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td36'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','ClassSummary','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td38'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','ConductMarkReport','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td55'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tableREPORTS .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Award_Punishment_Ranking_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['RankingReport']." - $i_Discipline_System_Access_Right_View</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','GoodConduct_Misconduct_Ranking_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['RankingReport']." - $i_Discipline_System_Access_Right_View</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

$tableREPORTS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Award_Punishment_Ranking_Detail_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['RankingDetailReport']." - $i_Discipline_System_Access_Right_View</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','GoodConduct_Misconduct_Ranking_Detail_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['RankingDetailReport']." - $i_Discipline_System_Access_Right_View</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

//$tableREPORTS .= "</tr>";

$tableREPORTS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','Award_Punishment_Class_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['AP_Class_Report_short']." - $i_Discipline_System_Access_Right_View</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','GoodConduct_Misconduct_Daily_Report','View','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['GMDailyReport']." - $i_Discipline_System_Access_Right_View</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

$tableREPORTS .= "</tr>";

// [2015-0820-1038-41206] TKP cust report access right
if($sys_custom['eDiscipline']['TKPSchoolRecordSummary']){
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','TKP_School_Record_Summary','View','');
	if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['TKPSchoolRecordSummary']['Title']." - $i_Discipline_System_Access_Right_View</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
}
if($sys_custom['eDiscipline']['TKPClassDemeritRecord']){
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','REPORTS','TKP_Class_Demerit_Record','View','');
	if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['TKPClassDemeritRecord']['Title']." - $i_Discipline_System_Access_Right_View</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
}

// [2016-1117-1427-18066] CWC Cust Report access right
if($sys_custom['eDiscipline']['CSCProbation']){
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID, 'Discipline', 'REPORTS', 'CWC_AP_Records', 'View', '');
	if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['CWC']['StudentAPRecord']." - $i_Discipline_System_Access_Right_View</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
	
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID, 'Discipline', 'REPORTS', 'CWC_Punishment_Count', 'View', '');
	if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['CWC']['ClassAPStatTable']." - $i_Discipline_System_Access_Right_View</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
}

// [2018-0920-1312-34207] Detention Punishment List access right
$tableREPORTS .= "<tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID, 'Discipline', 'REPORTS', 'Detention_Punishment_List', 'View', '');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$Lang['eDiscipline']['DetentionPunishmentList']." - $i_Discipline_System_Access_Right_View</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

$tableREPORTS .= "</tr>";

$tableREPORTS .= "</table>";

$tableSETTINGS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSETTINGS .= "<tr class='tabletop'>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSETTINGS .= "<td>".(($ldiscipline->UseSubScore) ? $Lang['eDiscipline']['ConductMarkStudyScore'] : $i_Discipline_System_Access_Right_Conduct_Mark)."</td>";
//$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_eNotice_Template."</td>";
//$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Letter."</td>";
$tableSETTINGS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','SETTINGS','Award_Punishment','Access','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td40'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Access."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','SETTINGS','GoodConduct_Misconduct','Access','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td41'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Access."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','SETTINGS','Conduct_Mark','Access','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td42'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Access."</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','SETTINGS','Detention','Access','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td43'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Access."</td>";
*/
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','SETTINGS','eNoticeTemplate','Access','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td44'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Access."</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','SETTINGS','Letter','Access','');
if($tempResult[0][1]!='') { $show = 1; } else { $show = 0; }
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td45'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_Access."</td>";
*/
$tableSETTINGS .= "</tr></table>";


#####

# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 1);
//$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

$subTag[] = array($Lang['eDiscipline']['AccessRight_General_Right'], "group_access_right_view.php?GroupID=$GroupID", 1);
$subTag[] = array($Lang['eDiscipline']['AccessRight_GM_Right'], "group_gm_right.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_AP_Right'], "group_ap_right.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_UserList'], "group_access_right_member_list.php?GroupID=$GroupID", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Group_Access_Rights, "group_access_right.php");
$PAGE_NAVIGATION[] = array($GroupTitle, "");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Access_Right_View, "");
?>

<br />
<form name="form1" method="post" action="group_access_right_new_update.php">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
					</td>
				</tr>
			</table>
			<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr><td colspan="2" align="right"><?=$linterface->GET_SYS_MSG($xmsg);?></td></tr>
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Title?></td>
					<td><?=$GroupTitle ?></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Description?></td>
					<td><?=$GroupDescription ?></td>
				</tr>
				<!--
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_No_Of_Members?></td>
					<td><a class="tablelink" href="group_access_right_member_list.php?GroupID=<?=$GroupID?>"><?=$no_of_members ?></a></td>
				</tr>
				-->
			</table>
			<br />
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>

			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td>
						<table width="95%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><?=$linterface->GET_NAVIGATION2($i_Discipline_System_Group_Right_Navigation_Group_Access_Right);?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Management ?></i>
								<?= $tableMGMT ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?= $i_Discipline_System_Teacher_Right_Statistics ?></i>
								<?= $tableSTAT ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Reports ?></i>
								<?= $tableREPORTS ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Settings ?></i>
								<?= $tableSETTINGS ?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="center">
					<table width="88%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center">
								<?= $linterface->GET_ACTION_BTN($button_edit, "button", "window.location='group_access_right_edit.php?GroupID=$GroupID'")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_back, "button", "window.location='group_access_right.php'")?>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<br>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>