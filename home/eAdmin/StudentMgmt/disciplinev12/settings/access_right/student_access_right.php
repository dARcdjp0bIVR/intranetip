<?php 
#modifing by yat

############ Change Log [Start] #############
# 
#	Date:	2010-03-10 YatWoon
#			Add "overview" access right for Student/Parent mode
#
############ Change Log [End] #############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 0);
//$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 1);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# TABLE COLUMN 

####################################
# Discipline Records 
####################################
$table .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$table .= "<tr class='tabletop'>";
$table .= "<td width='20%'>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$table .= "<td width='20%'>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
//$table .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$table .= "<td width='20%'>".$i_Discipline_System_Access_Right_Detention."</td>";
$table .= "<td width='20%'>".$eDiscipline['StudentReport'] ."</td>";
$table .= "<td width='20%'>".$iDiscipline['Reports_Child_PersonalReport'] ."</td>";
$table .= "</tr><tr class='tablerow1'>";

$showTDContent = array();
$showTDContent[0][0] = "tabletextremark";
$showTDContent[0][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10'>";
$showTDContent[1][0] = "tablegreenrow3";
$showTDContent[1][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif' width='20' height='20'>";
$show = 0;

$GroupID = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('S','');
# VIEW_ACCESS_RIGHT($GroupID, $Module, $Section, $Function)

$tempResult = array();

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','GoodConduct_Misconduct','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','Award_PunishMent','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','Detention','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','StudentReport','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','PersonalReport','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$table .= "</tr>";
$table .= "</table>";


####################################
# Ranking Report 
####################################
$table2 = "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$table2 .= "<tr class='tabletop'>";
$table2 .= "<td width='50%'>$i_Discipline_System_Access_Right_Good_Conduct_Misconduct</td>";
$table2 .= "<td width='50%'>$i_Discipline_System_Access_Right_Award_Punish</td>";
$table2 .= "</tr><tr class='tablerow1'>";

$show = 0;

$tempResult = array();

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','RANKING','GoodConduct_Ranking','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table2 .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','RANKING','Award_Ranking','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table2 .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$table2 .= "</tr>";
$table2 .= "</table>";


####################################
# Overview
####################################
$table3 = "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$table3 .= "<tr class='tabletop'>";
$table3 .= "<td width='50%'>". $eDiscipline['Overview'] ."</td>";
$table3 .= "</tr><tr class='tablerow1'>";

$show = 0;

$tempResult = array();

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,'Discipline','VIEWING','Overview','','');
if($tempResult[0][1]=='View') {$show = 1;} else {$show = 0;}
$table3 .= "<td class='".$showTDContent[$show][0]."'>".$showTDContent[$show][1]." ".$i_Discipline_System_Access_Right_View."</td>";

$table3 .= "</tr>";
$table3 .= "</table>";

# Start layout
$linterface->LAYOUT_START();

# no navigation in this page
$PAGE_NAVIGATION[] = array($i_Discipline_System_Student_Rights, "");
?>

<br />
<form name="form1" method="post" action="">
<table width="88%" border="0" cellpadding="5" cellspacing="0">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="5">
							<tr>
								<td align="right"><?=$linterface->GET_SYS_MSG($xmsg);?>
								</td>
							</tr>
							<tr>
								<td class="form_sep_title"> <i><?= $eDiscipline['Overview'] ?></i>
									<?= $table3 ?>
								</td>
							</tr>
							<tr>
								<td class="form_sep_title"> <i><?= $eDiscipline['Viewing'] ?></i>
									<?= $table ?>
								</td>
							</tr>
							<tr>
								<td class="form_sep_title"> <i><?= $eDiscipline['RankingReport'] ?></i>
									<?= $table2 ?>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						</table>									
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="1" class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td>
			<table width="88%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
					<?= $linterface->GET_ACTION_BTN($button_edit, "button", "javascript:location.href='student_access_right_edit.php'")?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form>
<?php

$linterface->LAYOUT_STOP();
intranet_closedb();

?>