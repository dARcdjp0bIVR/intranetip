<?php 
# Using: 

######################################################
#
#	Date:	2017-08-24	Bill	[2017-0822-0918-30207]
#			Fixed: too many POST fields > cannot update Access Right
#
#	Date:	2016-03-20	Bill	[2016-1207-1221-39240]
#			support Activity Score ($sys_custom['ActScore'])
#
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$GroupDetails = $ldiscipline->getAccessRightGroupInfo($GroupID);
list($GroupTitle, $GroupDescription, $no_of_members) = $GroupDetails[0];
$GroupTitle = str_replace("<","&lt;",$GroupTitle);
$GroupDescription = str_replace("<","&lt;",$GroupDescription);
$GroupDescription = nl2br($GroupDescription);

# Conduct Mark Selection
function getConductMenu($conduct, $max) {
	for ($i=0; $i<=$max; $i++)
	{
		$selected = ($i==$conduct) ? " selected" : "";
	    $select_conduct_mark .= "<OPTION value='$i' $selected>$i</OPTION>\n";
	}
	return $select_conduct_mark;
}

# Study Score Selection
function getSubscoreMenu($subscore, $max) {
	for ($i=0; $i<=$max; $i++)
	{
		$selected = ($i==$subscore) ? " selected" : "";
	    $select_study_score .= "<OPTION value='$i' $selected>$i</OPTION>\n";
	}
	return $select_study_score;
}

# Activity Score Selection
function getActscoreMenu($actscore, $max) {
	for($i=0; $i<=$max; $i++)
	{
		$selected = ($i==$actscore) ? " selected" : "";
	    $select_activity_score .= "<OPTION value='$i' $selected>$i</OPTION>\n";
	}
	return $select_activity_score;
}

# Merit Count Selection
$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
function getMeritCountMenu($meritCount, $max, $interval) {
	for ($i=0; $i<=$max; $i=$i+$interval)
	{
		$selected = ($i==$meritCount) ? " selected" : "";
	    $select_merit_count .= "<OPTION value='$i' $selected>$i</OPTION>\n";
	}
	return $select_merit_count;
}

# Merit Type (Award) Selection
function getAwardMenu($type) {
	global $i_Merit_NoAwardPunishment, $i_Merit_Merit, $i_Merit_MinorCredit, $i_Merit_MajorCredit, $i_Merit_SuperCredit, $i_Merit_UltraCredit;
	$selectAwardMenu .= "<option value='-999'".($type=='-999' ? " selected" : "").">$i_Merit_NoAwardPunishment</option>";
	$selectAwardMenu .= ($lstudentprofile->is_merit_disabled) ? "" : "<option value='1'".($type=='1' ? " selected" : "").">$i_Merit_Merit</option>";
	$selectAwardMenu .= ($lstudentprofile->is_min_merit_disabled) ? "" : "<option value='2'".($type=='2' ? " selected" : "").">$i_Merit_MinorCredit</option>";
	$selectAwardMenu .= ($lstudentprofile->is_maj_merit_disabled) ? "" : "<option value='3'".($type=='3' ? " selected" : "").">$i_Merit_MajorCredit</option>";
	$selectAwardMenu .= ($lstudentprofile->is_sup_merit_disabled) ? "" : "<option value='4'".($type=='4' ? " selected" : "").">$i_Merit_SuperCredit</option>";
	$selectAwardMenu .= ($lstudentprofile->is_ult_merit_disabled) ? "" : "<option value='5'".($type=='5' ? " selected" : "").">$i_Merit_UltraCredit</option>";
	return $selectAwardMenu;
}

# Merit Type (Punishment) Selection
function getPunishMenu($type) {
	global $i_Merit_NoAwardPunishment, $i_Merit_Warning, $i_Merit_BlackMark, $i_Merit_MinorDemerit, $i_Merit_MajorDemerit, $i_Merit_SuperDemerit, $i_Merit_UltraDemerit;
	$selectPunishMenu .= "<option value='-999'".($type=='-999' ? " selected" : "").">$i_Merit_NoAwardPunishment</option>";
	$selectPunishMenu .= "<option value='0'".($type=='0' ? " selected" : "").">$i_Merit_Warning</option>";
	$selectPunishMenu .= ($lstudentprofile->is_black_disabled) ? "" : "<option value='-1'".($type=='-1' ? " selected" : "").">$i_Merit_BlackMark</option>";
	$selectPunishMenu .= ($lstudentprofile->is_min_demer_disabled) ? "" : "<option value='-2'".($type=='-2' ? " selected" : "").">$i_Merit_MinorDemerit</option>";
	$selectPunishMenu .= ($lstudentprofile->is_maj_demer_disabled) ? "" : "<option value='-3'".($type=='-3' ? " selected" : "").">$i_Merit_MajorDemerit</option>";
	$selectPunishMenu .= ($lstudentprofile->is_sup_demer_disabled) ? "" : "<option value='-4'".($type=='-4' ? " selected" : "").">$i_Merit_SuperDemerit</option>";
	$selectPunishMenu .= ($lstudentprofile->is_ult_demer_disabled) ? "" : "<option value='-5'".($type=='-5' ? " selected" : "").">$i_Merit_UltraDemerit</option>";
	return $selectPunishMenu;
}

# Get All Items Settings
$allMeritItemInfo = $ldiscipline->getAllAPItemInfo();
for($i=0; $i<sizeof($allMeritItemInfo); $i++) {
	$defaultValue['ConductScore'][$allMeritItemInfo[$i]['ItemID']] = $allMeritItemInfo[$i]['ConductScore'];
	$defaultValue['Subscore'][$allMeritItemInfo[$i]['ItemID']] = $allMeritItemInfo[$i]['SubScore1'];
	$defaultValue['Actscore'][$allMeritItemInfo[$i]['ItemID']] = $allMeritItemInfo[$i]['SubScore2'];
	$defaultValue['MeritNum'][$allMeritItemInfo[$i]['ItemID']] = $allMeritItemInfo[$i]['NumOfMerit'];
	$defaultValue['MeritType'][$allMeritItemInfo[$i]['ItemID']] = $allMeritItemInfo[$i]['RelatedMeritType'];
}

# Get Group Access Items
$canAccessItem = $ldiscipline->getAccessGroupAccessAPItem($GroupID);

# Award / Punishment Category
$award_category = $ldiscipline->getAPCategoryByType(1);
$punishment_category = $ldiscipline->getAPCategoryByType(-1);

$colspan = ($ldiscipline->UseSubScore) ? 7 : 6;
$colspan = ($ldiscipline->UseSubScore && $ldiscipline->UseActScore) ? 8 : $colspan;

# Table Content (Award Header)
$tableContent = "";
$tableContent .= "<div class='table_board'>";
$tableContent .= "<table class='common_table_list rights_table_list' id='myTable'>";
$tableContent .= "	<tr class='seperate_row'>";
$tableContent .= "		<th width='100%' colspan='$colspan' class='sub_row_top'>$i_Merit_Award</td>";
$tableContent .= "	</tr>";
$tableContent .= "	<tr class='tabletop'>";
$tableContent .= "		<th width='1%' class='num_check' rowspan='2'>#</th>";
$tableContent .= "		<th width='19%' class='row_content' rowspan='2'>".$eDiscipline['Setting_Category']."</th>";
$tableContent .= "		<th width='80%' colspan='".($colspan-3)."'><span class='row_content'>".$Lang['eDiscipline']['Items']."</span>";
$tableContent .= "		<div align='right'><a href=\"javascript:goSelectAll('award', true)\">".$Lang['eDiscipline']['SelectAll']."</a>&nbsp;|&nbsp;<a href=\"javascript:goSelectAll('award', false)\">".$Lang['eDiscipline']['UnselectAll']."</a></div></th>";
$tableContent .= "	</tr>";
$tableContent .= "	<tr class='tabletop'>";
$tableContent .= "		<th width='30%'><span class='row_content'>&nbsp;</span>";
$tableContent .= "		<th width='13%'><span class='row_content'>$i_Discipline_System_ConductScore <br>($i_Discipline_System_general_increment)</span>";
if($ldiscipline->UseSubScore) 
	$tableContent .= "	<th width='13%'><span class='row_content'>$i_Discipline_System_Subscore1 <br>($i_Discipline_System_general_increment)</span>";
if($ldiscipline->UseActScore) 
	$tableContent .= "	<th width='13%'><span class='row_content'>".$Lang['eDiscipline']['ActivityScore']." <br>($i_Discipline_System_general_increment)</span>";
$tableContent .= "		<th width='25%'><span class='row_content'>$i_Merit_Award</span>";
$tableContent .= "	</tr>";

# Award Table Content 
for($i=0; $i<sizeof($award_category); $i++) {
	$itemsID = $ldiscipline->retrieveAllAwardPunishmentItem($award_category[$i][0], 'id',1);
	$itemsName = $ldiscipline->retrieveAllAwardPunishmentItem($award_category[$i][0], 'name',1);

	for($j=0; $j<sizeof($itemsID); $j++) {
		$tableContent .= "	<tr class='tabletext'>";
		if($j==0) {
			$tableContent .= "		<td width='1%' valgin='top' rowspan='".sizeof($itemsID)."'>".($i+1)."</td>";
			$tableContent .= "		<td width='19%' valgin='top' rowspan='".sizeof($itemsID)."'><input type='checkbox' name='award_cat[".$award_category[$i][0]."]' id='award_cat_".$award_category[$i][0]."' value='1' onclick=\"goCheckSelection('category', this)\"><label for='award_cat_".$award_category[$i][0]."'>".intranet_htmlspecialchars($award_category[$i][1])."</label></td>";
		}
		if($canAccessItem['Item'][$award_category[$i][0]][$itemsID[$j]]) {
			$defaultCheck = " checked";
			$defaultClassName = "rights_selected";
			$conductShow = getConductMenu($canAccessItem['ConductScore'][$award_category[$i][0]][$itemsID[$j]], $ldiscipline->ConductMarkIncrement_MAX);
			$subscoreShow = getSubscoreMenu($canAccessItem['Subscore'][$award_category[$i][0]][$itemsID[$j]], $ldiscipline->SubScoreIncrement_MAX);
			$actscoreShow = getActscoreMenu($canAccessItem['Actscore'][$award_category[$i][0]][$itemsID[$j]], $ldiscipline->ActScoreIncrement_MAX);
			$meritNumShow = getMeritCountMenu($canAccessItem['MeritCount'][$award_category[$i][0]][$itemsID[$j]], $ldiscipline->AwardPunish_MAX, $AP_Interval);
			$meritTypeShow = getAwardMenu($canAccessItem['MeritType'][$award_category[$i][0]][$itemsID[$j]]);
		}
		else {
			$defaultCheck = "";
			$defaultClassName = "";
			$conductShow = getConductMenu($defaultValue['ConductScore'][$itemsID[$j]], $ldiscipline->ConductMarkIncrement_MAX);
			$subscoreShow = getSubscoreMenu($defaultValue['Subscore'][$itemsID[$j]], $ldiscipline->SubScoreIncrement_MAX);
			$actscoreShow = getActscoreMenu($defaultValue['Actscore'][$itemsID[$j]], $ldiscipline->ActScoreIncrement_MAX);
			$meritNumShow = getMeritCountMenu($defaultValue['MeritNum'][$itemsID[$j]], $ldiscipline->AwardPunish_MAX, $AP_Interval);
			$meritTypeShow = getAwardMenu($defaultValue['MeritType'][$itemsID[$j]]);
		}
		$tableContent .= "			<td class='$defaultClassName'><input type='checkbox' name='award_cat[".$award_category[$i][0]."][".$itemsID[$j]."]' id='award_cat_".$award_category[$i][0]."_".$itemsID[$j]."' value='1' onclick=\"if(this.checked==false){document.getElementById('award_cat_".$award_category[$i][0]."').checked=false;}; goCheckSelection('item',this)\" $defaultCheck><label for='award_cat_".$award_category[$i][0]."_".$itemsID[$j]."'>".intranet_htmlspecialchars($itemsName[$j])."</label></td>";
		$tableContent .= "			<td class='$defaultClassName'><SELECT name='conductScore[".$itemsID[$j]."]' id='conductScore_".$itemsID[$j]."'>".$conductShow."</SELECT></td>";
		if($ldiscipline->UseSubScore) 
			$tableContent .= "		<td class='$defaultClassName'><SELECT name='subscore[".$itemsID[$j]."]' id='subscore_".$itemsID[$j]."'>$subscoreShow</SELECT></td>";
		if($ldiscipline->UseActScore) 
			$tableContent .= "		<td class='$defaultClassName'><SELECT name='actscore[".$itemsID[$j]."]' id='actscore_".$itemsID[$j]."'>$actscoreShow</SELECT></td>";
		$tableContent .= "			<td class='$defaultClassName'>
										<SELECT name='meritCount[".$itemsID[$j]."]' id='meritCount_".$itemsID[$j]."'>$meritNumShow</SELECT> 
										<SELECT name='meritType[".$itemsID[$j]."]' id='meritType_".$itemsID[$j]."'>$meritTypeShow</SELECT>
									</td>";
		$tableContent .= "	</tr>";
	}
}

$tableContent .= "<tr><td colspan='$colspan'>&nbsp;</td></tr>";

# Table Content (Punishment Header)
$tableContent .= "	<tr class='seperate_row'>";
$tableContent .= "		<th width='100%' colspan='$colspan' class='sub_row_top'>$i_Merit_Punishment</th>";
$tableContent .= "	</tr>";
$tableContent .= "	<tr class='tabletop'>";
$tableContent .= "		<th width='1%' rowspan='2'>#</th>";
$tableContent .= "		<th width='19%' class='num_check' rowspan='2'>".$eDiscipline['Setting_Category']."</th>";
$tableContent .= "		<th width='80%' colspan='".($colspan-3)."'><span class='row_content'>".$Lang['eDiscipline']['Items']."</span>";
$tableContent .= "		<div align='right'><a href=\"javascript:goSelectAll('punishment', true)\">".$Lang['eDiscipline']['SelectAll']."</a>&nbsp;|&nbsp;<a href=\"javascript:goSelectAll('punishment', false)\">".$Lang['eDiscipline']['UnselectAll']."</a></div></th>";
$tableContent .= "	</tr>";
$tableContent .= "	<tr class='tabletop'>";
$tableContent .= "		<th width='30%'><span class='row_content'>&nbsp;</span>";
$tableContent .= "		<th width='13%'><span class='row_content'>$i_Discipline_System_ConductScore <br>($i_Discipline_System_general_decrement)</span>";
if($ldiscipline->UseSubScore) 
	$tableContent .= "	<th width='13%'><span class='row_content'>$i_Discipline_System_Subscore1 <br>($i_Discipline_System_general_decrement)</span>";
if($ldiscipline->UseActScore) 
	$tableContent .= "	<th width='13%'><span class='row_content'>".$Lang['eDiscipline']['ActivityScore']." <br>($i_Discipline_System_general_decrement)</span>";
$tableContent .= "		<th width='25%'><span class='row_content'>$i_Merit_Punishment</span>";
$tableContent .= "	</tr>";

# Punishment Table Content 
for($i=0; $i<sizeof($punishment_category); $i++) {
	$itemsID = $ldiscipline->retrieveAllAwardPunishmentItem($punishment_category[$i][0], 'id',-1);
	$itemsName = $ldiscipline->retrieveAllAwardPunishmentItem($punishment_category[$i][0], 'name',-1);
	
	for($j=0; $j<sizeof($itemsID); $j++) {
		$tableContent .= "	<tr class='tabletext'>";
		if($j==0) {
			$tableContent .= "		<td valgin='top' rowspan='".sizeof($itemsID)."'>".($i+1)."</td>";
			$tableContent .= "		<td valgin='top' rowspan='".sizeof($itemsID)."'><input type='checkbox' name='punishment_cat[".$punishment_category[$i][0]."]' id='punishment_cat_".$punishment_category[$i][0]."' value='1' onclick=\"goCheckSelection('category', this)\"><label for='punishment_cat_".$punishment_category[$i][0]."'>".intranet_htmlspecialchars($punishment_category[$i][1])."</label></td>";
		}
		if($canAccessItem['Item'][$punishment_category[$i][0]][$itemsID[$j]]) {
			$defaultCheck = " checked";
			$defaultClassName = "rights_selected";
			$conductShow = getConductMenu($canAccessItem['ConductScore'][$punishment_category[$i][0]][$itemsID[$j]], $ldiscipline->ConductMarkIncrement_MAX);
			$subscoreShow = getSubscoreMenu($canAccessItem['Subscore'][$punishment_category[$i][0]][$itemsID[$j]] ,$ldiscipline->SubScoreIncrement_MAX);
			$actscoreShow = getActscoreMenu($canAccessItem['Actscore'][$punishment_category[$i][0]][$itemsID[$j]] ,$ldiscipline->ActScoreIncrement_MAX);
			$meritNumShow = getMeritCountMenu($canAccessItem['MeritCount'][$punishment_category[$i][0]][$itemsID[$j]], $ldiscipline->AwardPunish_MAX, $AP_Interval);
			$meritTypeShow = getPunishMenu($canAccessItem['MeritType'][$punishment_category[$i][0]][$itemsID[$j]]);
		}
		else {
			$defaultCheck = "";
			$defaultClassName = "";
			$conductShow = getConductMenu($defaultValue['ConductScore'][$itemsID[$j]], $ldiscipline->ConductMarkIncrement_MAX);
			$subscoreShow = getSubscoreMenu($defaultValue['Subscore'][$itemsID[$j]], $ldiscipline->SubScoreIncrement_MAX);
			$actscoreShow = getActscoreMenu($defaultValue['Actscore'][$itemsID[$j]], $ldiscipline->ActScoreIncrement_MAX);
			$meritNumShow = getMeritCountMenu($defaultValue['MeritNum'][$itemsID[$j]], $ldiscipline->AwardPunish_MAX, $AP_Interval);
			$meritTypeShow = getPunishMenu($defaultValue['MeritType'][$itemsID[$j]]);
		}
		$tableContent .= "			<td class='$defaultClassName'><input type='checkbox' name='punishment_cat[".$punishment_category[$i][0]."][".$itemsID[$j]."]' id='punishment_cat_".$punishment_category[$i][0]."_".$itemsID[$j]."' value='1' onclick=\"if(this.checked==false){document.getElementById('punishment_cat_".$punishment_category[$i][0]."').checked=false;}; goCheckSelection('item', this)\" $defaultCheck><label for='punishment_cat_".$punishment_category[$i][0]."_".$itemsID[$j]."'>".intranet_htmlspecialchars($itemsName[$j])."</label></td>";
		$tableContent .= "			<td class='$defaultClassName'><SELECT name='conductScore[".$itemsID[$j]."]' id='conductScore_".$itemsID[$j]."'>$conductShow</SELECT></td>";
		if($ldiscipline->UseSubScore) 
			$tableContent .= "		<td class='$defaultClassName'><SELECT name='subscore[".$itemsID[$j]."]' id='subscore_".$itemsID[$j]."'>$subscoreShow</SELECT></td>";
		if($ldiscipline->UseActScore) 
			$tableContent .= "		<td class='$defaultClassName'><SELECT name='actscore[".$itemsID[$j]."]' id='actscore_".$itemsID[$j]."'>$actscoreShow</SELECT></td>";
		$tableContent .= "			<td class='$defaultClassName'>
										<SELECT name='meritCount[".$itemsID[$j]."]' id='meritCount_".$itemsID[$j]."'>$meritNumShow</SELECT> 
										<SELECT name='meritType[".$itemsID[$j]."]' id='meritType_".$itemsID[$j]."'>$meritTypeShow</SELECT>
									</td>";
		$tableContent .= "	</tr>";
	}
}
$tableContent .= "</table>";
$tableContent .= "</div>";

$colNo = ($ldiscipline->UseSubScore) ? 3 : 2;
$colNo = ($ldiscipline->UseSubScore && $ldiscipline->UseActScore) ? 4 : $colNo;

# Top Menu Highlight
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 1);
//TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

$subTag[] = array($Lang['eDiscipline']['AccessRight_General_Right'], "group_access_right_view.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_GM_Right'], "group_gm_right.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_AP_Right'], "group_ap_right.php?GroupID=$GroupID", 1);
$subTag[] = array($Lang['eDiscipline']['AccessRight_UserList'], "group_access_right_member_list.php?GroupID=$GroupID", 0);

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Group_Access_Rights, "group_access_right.php");
$PAGE_NAVIGATION[] = array($GroupTitle, "group_access_right_view.php?GroupID=$GroupID");
//$PAGE_NAVIGATION[] = array($button_edit, "");

$CurrentPageArr['eDisciplinev12'] = 1;

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();
?>

<script language="javascript">
<!--
function goCheckSelection(elementType, elementRef) {
	if(elementType=='item') {
		elementRef.parentNode.className = (elementRef.checked) ? 'rights_selected' : '';
		var tds = document.getElementsByTagName('td');
		for (var i=0 ; i<tds.length ; i++ ) {
			if(tds[i]==elementRef.parentNode) {
				for(j=1; j<=<?=$colNo?>; j++) {
					tds[i+j].className = (elementRef.checked) ? 'rights_selected' : '';
				}
			}
		}
	}
	else {
		var len = document.form1.elements.length;
		var catNameLen = elementRef.id.toString().length;
		for(var i=0; i<len; i++) {
			if(document.form1.elements[i].id.substring(0,catNameLen+1) == (elementRef.id+"_")) {
				if(elementRef.checked) {
					document.form1.elements[i].checked = true;	
				}
				else {
					document.form1.elements[i].checked = false;	
				}
				goCheckSelection('item', document.form1.elements[i]);
			}
		}
	}
}

function goSelectAll(name, flag) {
	var len = document.form1.elements.length;
	for(var i=0; i<len; i++) {
		if(document.form1.elements[i].id.substring(0,5)==name || document.form1.elements[i].id.substring(0,10)==name) {
			document.form1.elements[i].checked = flag;
			if((document.form1.elements[i].id.split("_").length - 1) > 2)
				goCheckSelection('item', document.form1.elements[i]);
		}
	}
}

function goReset() {
	document.form1.reset();
	var len = document.form1.elements.length;
	for(var i=0; i<len; i++) {
		if(document.form1.elements[i].checked==true && (document.form1.elements[i].id.split("_").length - 1) > 2) {
			goCheckSelection('item', document.form1.elements[i]);
			i = i+4;
		}
		else {
			document.form1.elements[i].parentNode.className = "";
		}
	}
}
//-->
</script>

<br />
<form name="form1" method="post" action="group_ap_right_update.php">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="40%"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
					<td align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
				</tr>
			</table>
			<br />
			<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
				<tr valign="top">
					<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Title?></td>
					<td><?=$GroupTitle ?></td>
				</tr>
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Description?></td>
					<td><?=$GroupDescription ?></td>
				</tr>
				<!--
				<tr valign="top">
					<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_No_Of_Members?></td>
					<td><a class="tablelink" href="group_access_right_member_list.php?GroupID=<?=$GroupID?>"><?=$no_of_members ?></a></td>
				</tr>
				-->
			</table>
			<br />
			<table width="95%" border="0" cellspacing="0" cellpadding="0" align="center">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
			<br style="clear:both" />
			<?=$tableContent?>							
			<br>
			<input type="hidden" name="GroupID" value="<?= $GroupID ?>">
			<div align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "goReset()")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='group_access_right.php'")?>
			</div>
		</td>
	</tr>
</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>