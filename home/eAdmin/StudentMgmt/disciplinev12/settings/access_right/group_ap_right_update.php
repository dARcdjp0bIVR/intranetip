<?php 
# using: 

######################################################
#
#	Date:	2017-08-24	Bill	[2017-0822-0918-30207]
#			Fixed: too many POST fields > Get Access Right Settings from array
#
#	Date:	2016-03-20	Bill	[2016-1207-1221-39240]
#			support Activity Score ($sys_custom['ActScore'])
#
######################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if(!$GroupID) {
	header("Location: group_access_right.php");
}

$award_cats = $ldiscipline->getAPCategoryByType(1);
$punishment_cats = $ldiscipline->getAPCategoryByType(-1);

# Award 
for($i=0; $i<sizeof($award_cats); $i++)
{
	$itemsID = $ldiscipline->retrieveAllAwardPunishmentItem($award_cats[$i][0], 'id', 1);
	for($j=0; $j<sizeof($itemsID); $j++) {
		$itemName = $_POST["award_cat"][$award_cats[$i][0]][$itemsID[$j]];
		
		# Selected Item
		if($itemName) {
			$dataAry['Item'][$award_cats[$i][0]][$itemsID[$j]] = 1;	
			$dataAry['conductScore'][$award_cats[$i][0]][$itemsID[$j]] = $_POST["conductScore"][$itemsID[$j]];
			$dataAry['subscore'][$award_cats[$i][0]][$itemsID[$j]] = $_POST["subscore"][$itemsID[$j]];
			$dataAry['actscore'][$award_cats[$i][0]][$itemsID[$j]] = $_POST["actscore"][$itemsID[$j]];
			$dataAry['meritCount'][$award_cats[$i][0]][$itemsID[$j]] = $_POST["meritCount"][$itemsID[$j]];
			$dataAry['meritType'][$award_cats[$i][0]][$itemsID[$j]] = $_POST["meritType"][$itemsID[$j]];
		}
	}
}

# Punishment 
for($i=0; $i<sizeof($punishment_cats); $i++)
{
	$itemsID = $ldiscipline->retrieveAllAwardPunishmentItem($punishment_cats[$i][0], 'id', 1);
	for($j=0; $j<sizeof($itemsID); $j++) {
		$itemName = $_POST["punishment_cat"][$punishment_cats[$i][0]][$itemsID[$j]];
		
		# Selected Item
		if($itemName) {
			$dataAry['Item'][$punishment_cats[$i][0]][$itemsID[$j]] = 1;	
			$dataAry['conductScore'][$punishment_cats[$i][0]][$itemsID[$j]] = $_POST["conductScore"][$itemsID[$j]];
			$dataAry['subscore'][$punishment_cats[$i][0]][$itemsID[$j]] = $_POST["subscore"][$itemsID[$j]];
			$dataAry['actscore'][$punishment_cats[$i][0]][$itemsID[$j]] = $_POST["actscore"][$itemsID[$j]];
			$dataAry['meritCount'][$punishment_cats[$i][0]][$itemsID[$j]] = $_POST["meritCount"][$itemsID[$j]];
			$dataAry['meritType'][$punishment_cats[$i][0]][$itemsID[$j]] = $_POST["meritType"][$itemsID[$j]];
		}
	}
}

# Update Access Right
$ldiscipline->updateAPAccessRight($GroupID, $dataAry);

intranet_closedb();

header("Location: group_ap_right.php?GroupID=$GroupID&xmsg=update");
?>