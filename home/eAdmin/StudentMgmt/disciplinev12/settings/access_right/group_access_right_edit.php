<?php 
# using: 

/********************** Change Log ***********************
#
#	Date	:	2018-10-04 [Bill]   [2018-0920-1312-34207]
#				- add new AP report > Detention Punishment List [td64]
#
#	Date	:	2017-10-09 [Bill]	[2017-0915-1351-05054]	
#				- Management : Uncheck "View" > Uncheck all related Rights (e.g. Create / Edit / Delete / ...)
#
#	Date	:	2016-12-20 [Bill]	[2016-1117-1427-18066]
#				- add new AP report > CWC 2 Cust Reports [td62] [td63]
#
#	Date	:	2015-08-25 [Bill]	[2015-0820-1038-41206]
#				- add new AP report > TKP 2 Cust Reports [td60] [td61]
#				- modified js function initialDisableCheckboxAndRadio(), fix js error
#
#	Date	:	2010-12-28 [YatWoon]
#				- add new AP report > AP Class Report [td57]
#
#	Date	:	2010-12-23 [YatWoon]
#				- add new GM report > Daily Accumulative Misconduct Report (from eDis1.0) [td56]
#
******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$GroupID = is_array($GroupID)? $GroupID[0] : $GroupID;

$thisModule = 'Discipline';

$sql = "SELECT GroupTitle, GroupDescription FROM ACCESS_RIGHT_GROUP WHERE GroupID=$GroupID";
$GroupDetails = $ldiscipline->returnArray($sql,2);
list($GroupTitle, $GroupDescription) = $GroupDetails[0];
$GroupTitle = str_replace("<","&lt;",$GroupTitle);
$GroupTitle = Str_replace("\"","&quot;",$GroupTitle);
$GroupDescription = str_replace("<","&lt;",$GroupDescription);

$show = 0;
$showTDContent = array();

$showTDContent[0][0] = "tabletextremark";
$showTDContent[0][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' align='absmiddle'>";
$showTDContent[1][0] = "tablegreenrow3";
$showTDContent[1][1] = "<img src='{$image_path}/{$LAYOUT_SKIN}/icon_tick_green.gif' width='20' height='20' align='absmiddle'>";

#####
$tableMGMT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableMGMT .= "<tr class='tabletop'>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Award_Punish ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Case_Record ."</td>";
$tableMGMT .= "<td valign='top'>".(($ldiscipline->UseSubScore) ? $Lang['eDiscipline']['ConductMarkStudyScore'] : $i_Discipline_System_Access_Right_Conduct_Mark) ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Detention_Management ."</td>";
$tableMGMT .= "<td valign='top'>".$i_Discipline_System_Access_Right_Detention_Session_Arrangement ."</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[0]=1;"; } else { $show = 0; $restoreField .= "arrayInt[0] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td1'><input name='MGMT_Award_Punishment_View' type='checkbox' id='f1' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f1'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[1]=1;"; } else { $show = 0; $restoreField .= "arrayInt[1] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td2'><input name='MGMT_GoodConduct_Misconduct_View' type='checkbox' id='f2' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f2'>".$i_Discipline_System_Access_Right_View."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[2]=1;"; } else { $show = 0; $restoreField .= "arrayInt[2] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td3'><input name='MGMT_Case_Record_View' type='checkbox' id='f3' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f3'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[3]=1;"; } else { $show = 0; $restoreField .= "arrayInt[3] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td4'><input name='MGMT_Conduct_Mark_View' type='checkbox' id='f4' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f4'>".$i_Discipline_System_Access_Right_View."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[4]=1;"; } else { $show = 0; $restoreField .= "arrayInt[4] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td5'><input name='MGMT_Detention_View' type='checkbox' id='f5' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f5'>".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','New','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[5]=1;"; } else { $show = 0; $restoreField .= "arrayInt[5] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td6'><input name='MGMT_Award_Punishment_New' type='checkbox' id='f6' value='New' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f6'>".$i_Discipline_System_Access_Right_New."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','New','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[6]=1;"; } else { $show = 0; $restoreField .= "arrayInt[6] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td7'><input name='MGMT_GoodConduct_Misconduct_New' type='checkbox' id='f7' value='New' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f7'>".$i_Discipline_System_Access_Right_New."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','New','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[7]=1;"; } else { $show = 0; $restoreField .= "arrayInt[7] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td8'><input name='MGMT_Case_Record_New' type='checkbox' id='f8' value='New' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f8'>".$i_Discipline_System_Access_Right_New."</label> </td>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','New','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[8]=1;"; } else { $show = 0; $restoreField .= "arrayInt[8] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td9'><input name='MGMT_Detention_New' type='checkbox' id='f9' value='New' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f9'>".$i_Discipline_System_Access_Right_New."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','EditOwn','EditAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[9]=1;"; } else { $show = 0; $restoreField .= "arrayInt[9] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td10'><input name='MGMT_Award_Punishment_Edit' type='checkbox' id='f10' value='Edit' onclick='if(this.checked==false){form1.f10_OwnAll[0].checked=false; form1.f10_OwnAll[1].checked=false;} else{form1.f10_OwnAll[0].disabled=false;form1.f10_OwnAll[1].disabled=false;form1.f10_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn' || $tempResult[0][1]=='EditAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f10'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f10_OwnAll' type='radio' id='f10_OwnAll[0]' value='Own' onclick='form1.MGMT_Award_Punishment_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f10_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f10_OwnAll' type='radio' id='f10_OwnAll[1]' value='All' onclick='form1.MGMT_Award_Punishment_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f10_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','EditOwn','EditAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[10]=1;"; } else { $show = 0; $restoreField .= "arrayInt[10] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td11'><input name='MGMT_GoodConduct_Misconduct_Edit' type='checkbox' id='f11' value='Edit' onclick='if(this.checked==false){form1.f11_OwnAll[0].checked=false; form1.f11_OwnAll[1].checked=false;} else{form1.f11_OwnAll[0].disabled=false;form1.f11_OwnAll[1].disabled=false;form1.f11_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn' || $tempResult[0][1]=='EditAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f11'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f11_OwnAll' type='radio' id='f11_OwnAll[0]' value='Own' onclick='form1.MGMT_GoodConduct_Misconduct_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f11_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f11_OwnAll' type='radio' id='f11_OwnAll[1]' value='All' onclick='form1.MGMT_GoodConduct_Misconduct_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f11_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','EditOwn','EditAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[11]=1;"; } else { $show = 0; $restoreField .= "arrayInt[11] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td12'>";
$tableMGMT .= "<input name='MGMT_Case_Record_Edit' type='checkbox' id='f12' value='Edit' onclick='if(this.checked==false){form1.f12_OwnAll[0].checked=false; form1.f12_OwnAll[1].checked=false;} else{form1.f12_OwnAll[0].disabled=false;form1.f12_OwnAll[1].disabled=false;form1.f12_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn' || $tempResult[0][1]=='EditAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f12'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f12_OwnAll' type='radio' id='f12_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f12_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f12_OwnAll' type='radio' id='f12_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f12_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','EditOwn','EditAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[12]=1;"; } else { $show = 0; $restoreField .= "arrayInt[12] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td13'>";
$tableMGMT .= "<input name='MGMT_Detention_Edit' type='checkbox' id='f13' value='Edit' onclick='if(this.checked==false){form1.f13_OwnAll[0].checked=false; form1.f13_OwnAll[1].checked=false;} else{form1.f13_OwnAll[0].disabled=false;form1.f13_OwnAll[1].disabled=false;form1.f13_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn' || $tempResult[0][1]=='EditAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f13'>".$i_Discipline_System_Access_Right_Edit."</label> <br>";
$tableMGMT .= "<input name='f13_OwnAll' type='radio' id='f13_OwnAll[0]' value='Own' onclick='form1.MGMT_Detention_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditOwn') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f13_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f13_OwnAll' type='radio' id='f13_OwnAll[1]' value='All' onclick='form1.MGMT_Detention_Edit.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditAll') { $tableMGMT .= " checked";}
$tableMGMT .="><label for='f13_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','DeleteOwn','DeleteAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[13]=1;"; } else { $show = 0; $restoreField .= "arrayInt[13] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td14'><input name='MGMT_Award_Punishment_Delete' type='checkbox' id='f14' value='Delete' onclick='if(this.checked==false){form1.f14_OwnAll[0].checked=false; form1.f14_OwnAll[1].checked=false;} else{form1.f14_OwnAll[0].disabled=false;form1.f14_OwnAll[1].disabled=false;form1.f14_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn' || $tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f14'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f14_OwnAll' type='radio' id='f14_OwnAll[0]' value='Own' onclick='form1.MGMT_Award_Punishment_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f14_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f14_OwnAll' type='radio' id='f14_OwnAll[1]' value='All' onclick='form1.MGMT_Award_Punishment_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f14_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','DeleteOwn','DeleteAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[14]=1;"; } else { $show = 0; $restoreField .= "arrayInt[14] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td15'><input name='MGMT_GoodConduct_Misconduct_Delete' type='checkbox' id='f15' value='Delete' onclick='if(this.checked==false){form1.f15_OwnAll[0].checked=false; form1.f15_OwnAll[1].checked=false;} else{form1.f15_OwnAll[0].disabled=false;form1.f15_OwnAll[1].disabled=false;form1.f15_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn' || $tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f15'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f15_OwnAll' type='radio' id='f15_OwnAll[0]' value='Own' onclick='form1.MGMT_GoodConduct_Misconduct_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f15_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f15_OwnAll' type='radio' id='f15_OwnAll[1]' value='All' onclick='form1.MGMT_GoodConduct_Misconduct_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f15_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','DeleteOwn','DeleteAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[15]=1;"; } else { $show = 0; $restoreField .= "arrayInt[15] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td16'><input name='MGMT_Case_Record_Delete' type='checkbox' id='f16' value='Delete' onclick='if(this.checked==false){form1.f16_OwnAll[0].checked=false; form1.f16_OwnAll[1].checked=false;} else{form1.f16_OwnAll[0].disabled=false;form1.f16_OwnAll[1].disabled=false;form1.f16_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn' || $tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f16'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f16_OwnAll' type='radio' id='f16_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f16_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f16_OwnAll' type='radio' id='f16_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f16_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','DeleteOwn','DeleteAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[16]=1;"; } else { $show = 0; $restoreField .= "arrayInt[16] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td17'><input name='MGMT_Detention_Delete' type='checkbox' id='f17' value='Delete' onclick='if(this.checked==false){form1.f17_OwnAll[0].checked=false; form1.f17_OwnAll[1].checked=false;} else{form1.f17_OwnAll[0].disabled=false;form1.f17_OwnAll[1].disabled=false;form1.f17_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn' || $tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f17'>".$i_Discipline_System_Access_Right_Delete."</label><br>";
$tableMGMT .= "<input name='f17_OwnAll' type='radio' id='f17_OwnAll[0]' value='Own' onclick='form1.MGMT_Detention_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteOwn') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f17_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f17_OwnAll' type='radio' id='f17_OwnAll[1]' value='All' onclick='form1.MGMT_Detention_Delete.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f17_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','Waive','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[17]=1;"; } else { $show = 0; $restoreField .= "arrayInt[17] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td18'><input name='MGMT_Award_Punishment_Waive' type='checkbox' id='f18' value='Waive' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f18'>".$i_Discipline_System_Access_Right_Waive."</label> </td>";

if($ldiscipline->use_newleaf) {
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','NewLeaf','');
	if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[47]=1;"; } else { $show = 0; $restoreField .= "arrayInt[47] = 0;";}
	$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td48'><input name='MGMT_GoodConduct_Misconduct_NewLeaf' type='checkbox' id='f48' value='NewLeaf' onclick='checkBoxOnClick(this)'";
	if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
	$tableMGMT .= "><label for='f48'>".$i_Discipline_System_Access_Right_NewLeaf."</label> </td>";
}
else {
	$tableMGMT .= "<td valign='top'>&nbsp;</td>";
}

$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','Release','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[18]=1;"; } else { $show = 0; $restoreField .= "arrayInt[18] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td19'><input name='MGMT_Award_Punishment_Release' type='checkbox' id='f19' value='Release' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f19'>".$i_Discipline_System_Access_Right_Release."</label></td>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','Release','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[45]=1;"; } else { $show = 0; $restoreField .= "arrayInt[45] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td46'><input name='MGMT_Case_Record_Release' type='checkbox' id='f46' value='Release' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f46'>".$i_Discipline_System_Access_Right_Release."</label></td>";

$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','Approval','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[19]=1;"; } else { $show = 0; $restoreField .= "arrayInt[19] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td20'><input name='MGMT_Award_Punishment_Approval' type='checkbox' id='f20' value='Approval' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f20'>".$i_Discipline_System_Access_Right_Approval."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','GoodConduct_Misconduct','Approval','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[20]=1;"; } else { $show = 0; $restoreField .= "arrayInt[20] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td21'><input name='MGMT_GoodConduct_Misconduct_Approval' type='checkbox' id='f21' value='Approval' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f21'>".$i_Discipline_System_Access_Right_Approval."</label></td>";

/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','Finish','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[46]=1;"; } else { $show = 0; $restoreField .= "arrayInt[46] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td47'><input name='MGMT_Case_Record_Finish' type='checkbox' id='f47' value='Finish' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f47'>".$i_Discipline_System_Access_Right_Finish."</label></td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow1'>";

/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Award_Punishment','Lock','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[21]=1;"; } else { $show = 0; $restoreField .= "arrayInt[21] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td22'><!--<input name='MGMT_Award_Punishment_Lock' type='checkbox' id='f22' value='Lock' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f22'>".$i_Discipline_System_Access_Right_Lock."</label>--></td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','Lock','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[22]=1;"; } else { $show = 0; $restoreField .= "arrayInt[22] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td23'><!--<input name='MGMT_Case_Record_Lock' type='checkbox' id='f23' value='Lock' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f23'>".$i_Discipline_System_Access_Right_Lock."</label>--></td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Conduct_Mark','Adjust','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[23]=1;"; } else { $show = 0; $restoreField .= "arrayInt[23] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td24'><input name='MGMT_Conduct_Mark_Adjust' type='checkbox' id='f24' value='Adjust' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f24'>".$i_Discipline_System_Access_Right_Adjust."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','TakeAttendance','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[24]=1;"; } else { $show = 0; $restoreField .= "arrayInt[24] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td25'><input name='MGMT_Detention_Take_Attendance' type='checkbox' id='f25' value='TakeAttendance' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f25'>".$i_Discipline_System_Access_Right_Take_Attendance."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'SETTINGS','Detention','Access','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[42]=1;"; } else { $show = 0; $restoreField .= "arrayInt[42] = 0;";}
$tableMGMT .= "<td class='".$showTDContent[$show][0]."' id='td43'><input type='checkbox' name='SETTINGS_Detention_Access' id='f43' value='Access' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f43'>".$i_Discipline_System_Access_Right_Access."</label></td>";

$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','NewNotes','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[25]=1;"; } else { $show = 0; $restoreField .= "arrayInt[25] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td26'><!--<input name='MGMT_Case_Record_NewNotes' type='checkbox' id='f26' value='NewNotes' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f26'>".$i_Discipline_System_Access_Right_New_Notes."</label>--></td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Detention','RearrangeStudent','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[26]=1;"; } else { $show = 0; $restoreField .= "arrayInt[26] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td27'><input name='MGMT_Detention_RearrangeStudent' type='checkbox' id='f27' value='RearrangeStudent' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableMGMT .= " checked";}
$tableMGMT .= "><label for='f27'>".$i_Discipline_System_Access_Right_Rearrange_Student."</label></td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";

$tableMGMT .= "</tr><!--<tr class='tablerow1'>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','EditNotesOwn','EditNotesAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[27]=1;"; } else { $show = 0; $restoreField .= "arrayInt[27] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td28'><input name='MGMT_Case_Record_EditNotes' type='checkbox' id='f28' value='EditNotes' onclick='if(this.checked==false){form1.f28_OwnAll[0].checked=false; form1.f28_OwnAll[1].checked=false;} else{form1.f28_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditNotesOwn' || $tempResult[0][1]=='EditNotesAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f28'>".$i_Discipline_System_Access_Right_Edit_Notes."</label><br>";
$tableMGMT .= "<input name='f28_OwnAll' type='radio' id='f28_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_EditNotes.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditNotesOwn') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f28_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label>";
$tableMGMT .= "<input name='f28_OwnAll' type='radio' id='f28_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_EditNotes.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='EditNotesAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f28_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr><tr class='tablerow2'>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'MGMT','Case_Record','DeleteNotesOwn','DeleteNotesAll');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[28]=1;"; } else { $show = 0; $restoreField .= "arrayInt[28] = 0;";}
$tableMGMT .= "<td valign='top' class='".$showTDContent[$show][0]."' id='td29'><input name='MGMT_Case_Record_DeleteNotes' type='checkbox' id='f29' value='DeleteNotes' onclick='if(this.checked==false){form1.f29_OwnAll[0].checked=false; form1.f29_OwnAll[1].checked=false;} else{form1.f29_OwnAll[0].checked=true;};checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteNotesOwn' || $tempResult[0][1]=='DeleteNotesAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f29'>".$i_Discipline_System_Access_Right_Delete_Notes."</label><br>";
$tableMGMT .= "<input name='f29_OwnAll' type='radio' id='f29_OwnAll[0]' value='Own' onclick='form1.MGMT_Case_Record_DeleteNotes.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteNotesOwn') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f29_OwnAll[0]'>".$i_Discipline_System_Access_Right_Own."</label> ";
$tableMGMT .= "<input name='f29_OwnAll' type='radio' id='f29_OwnAll[1]' value='All' onclick='form1.MGMT_Case_Record_DeleteNotes.checked=true;checkBoxOnClick(this)'";
if($tempResult[0][1]=='DeleteNotesAll') { $tableMGMT .= " checked"; }
$tableMGMT .= "><label for='f29_OwnAll[1]'>".$i_Discipline_System_Access_Right_All."</label> </td>";
*/
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "<td valign='top'>&nbsp;</td>";
$tableMGMT .= "</tr>-->";
$tableMGMT .= "</table>";

$tableSTAT .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSTAT .= "<tr class='tabletop'>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Conduct_Mark."</td>";
$tableSTAT .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSTAT .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'STAT','Award_Punishment','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[29]=1;"; } else { $show = 0; $restoreField .= "arrayInt[29] = 0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td30'><input type='checkbox' name='STAT_Award_Punishment_View' id='f30' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSTAT .= " checked";}
$tableSTAT .= "><label for='f30'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'STAT','GoodConduct_Misconduct','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[30]=1;"; } else { $show = 0; $restoreField .= "arrayInt[30] = 0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td31'><input type='checkbox' name='STAT_GoodConduct_Misconduct_View' id='f31' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSTAT .= " checked";}
$tableSTAT .= "><label for='f31'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'STAT','Conduct_Mark','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[31]=1;"; } else { $show = 0; $restoreField .= "arrayInt[31] = 0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td32'><input type='checkbox' name='STAT_Conduct_Mark_View' id='f32' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSTAT .= " checked";}
$tableSTAT .= "><label for='f32'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'STAT','Detention','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[32]=1;"; } else { $show = 0; $restoreField .= "arrayInt[32] = 0;";}
$tableSTAT .= "<td class='".$showTDContent[$show][0]."' id='td33'><input type='checkbox' name='STAT_Detention_View' id='f33' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSTAT .= " checked";}
$tableSTAT .= "><label for='f33'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tableSTAT .= "</tr></table>";

$tableREPORTS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableREPORTS .= "<tr class='tabletop'>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableREPORTS .= "<td>".$i_Discipline_System_Report_Type_Personal." /<br> ".$eDiscipline['StudentReport']."</td>";
$tableREPORTS .= "<td>".$eDiscipline['ClassSummary']."</td>";
$tableREPORTS .= "<td>".$Lang['eDiscipline']['ConductMarkReport']."</td>";
$tableREPORTS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','Award_Punishment_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[48]=1;"; } else { $show = 0; $restoreField .= "arrayInt[48] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td49'><input type='checkbox' name='REPORTS_Award_Punishment_Report_View' id='f49' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f49'>".$Lang['eDiscipline']['Report']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','GoodConduct_Misconduct_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[49]=1;"; } else { $show = 0; $restoreField .= "arrayInt[49] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td50'><input type='checkbox' name='REPORTS_GoodConduct_Misconduct_Report_View' id='f50' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f50'>".$Lang['eDiscipline']['Report']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','StudentReport','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[35]=1;"; } else { $show = 0; $restoreField .= "arrayInt[35] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td36'><input type='checkbox' name='REPORTS_StudentReport_View' id='f36' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f36'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','ClassSummary','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[37]=1;"; } else { $show = 0; $restoreField .= "arrayInt[37] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td38'><input type='checkbox' name='REPORTS_ClassSummary_View' id='f38' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f38'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','ConductMarkReport','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[54]=1;"; } else { $show = 0; $restoreField .= "arrayInt[54] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td55'><input type='checkbox' name='REPORTS_ConductMarkReport_View' id='f55' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f55'>".$i_Discipline_System_Access_Right_View."</label> </td>";

$tableREPORTS .= "</tr><tr class='tablerow2'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','Award_Punishment_Ranking_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[50]=1;"; } else { $show = 0; $restoreField .= "arrayInt[50] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td51'><input type='checkbox' name='REPORTS_Award_Punishment_Ranking_Report_View' id='f51' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f51'>".$Lang['eDiscipline']['RankingReport']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','GoodConduct_Misconduct_Ranking_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[51]=1;"; } else { $show = 0; $restoreField .= "arrayInt[51] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td52'><input type='checkbox' name='REPORTS_GoodConduct_Misconduct_Ranking_Report_View' id='f52' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f52'>".$Lang['eDiscipline']['RankingReport']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

$tableREPORTS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','Award_Punishment_Ranking_Detail_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[52]=1;"; } else { $show = 0; $restoreField .= "arrayInt[52] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td53'><input type='checkbox' name='REPORTS_Award_Punishment_Ranking_Detail_Report_View' id='f53' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f53'>".$Lang['eDiscipline']['RankingDetailReport']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','GoodConduct_Misconduct_Ranking_Detail_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[53]=1;"; } else { $show = 0; $restoreField .= "arrayInt[53] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td54'><input type='checkbox' name='REPORTS_GoodConduct_Misconduct_Ranking_Detail_Report_View' id='f54' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f54'>".$Lang['eDiscipline']['RankingDetailReport']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

//$tableREPORTS .= "</tr>";

$tableREPORTS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','Award_Punishment_Class_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[56]=1;"; } else { $show = 0; $restoreField .= "arrayInt[56] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td57'><input type='checkbox' name='REPORTS_Award_Punishment_Class_Report_View' id='f57' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f57'>".$Lang['eDiscipline']['AP_Class_Report_short']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','GoodConduct_Misconduct_Daily_Report','View','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[55]=1;"; } else { $show = 0; $restoreField .= "arrayInt[55] = 0;";}
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td56'><input type='checkbox' name='REPORTS_GoodConduct_Misconduct_Daily_Report_View' id='f56' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
$tableREPORTS .= "><label for='f56'>".$Lang['eDiscipline']['GMDailyReport']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";

$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

$tableREPORTS .= "</tr>";

// [2015-0820-1038-41206] TKP cust report access right
if($sys_custom['eDiscipline']['TKPSchoolRecordSummary']){
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','TKP_School_Record_Summary','View','');
	if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[59]=1;"; } else { $show = 0; $restoreField .= "arrayInt[59] = 0;";}
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td60'><input type='checkbox' name='REPORTS_TKP_School_Record_Summary' id='f60' value='View' onclick='checkBoxOnClick(this)'";
	if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
	$tableREPORTS .= "><label for='f60'>".$Lang['eDiscipline']['TKPSchoolRecordSummary']['Title']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
}
if($sys_custom['eDiscipline']['TKPClassDemeritRecord']){
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'REPORTS','TKP_Class_Demerit_Record','View','');
	if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[60]=1;"; } else { $show = 0; $restoreField .= "arrayInt[60] = 0;";}
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td61'><input type='checkbox' name='REPORTS_TKP_Class_Demerit_Record' id='f61' value='View' onclick='checkBoxOnClick(this)'";
	if($tempResult[0][1]!='') { $tableREPORTS .= " checked";}
	$tableREPORTS .= "><label for='f61'>".$Lang['eDiscipline']['TKPClassDemeritRecord']['Title']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
}

// [2016-1117-1427-18066] CWC Cust Report access right
if($sys_custom['eDiscipline']['CSCProbation']){	
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID, 'Discipline', 'REPORTS', 'CWC_AP_Records', 'View', '');
	if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[61]=1;"; } else { $show = 0; $restoreField .= "arrayInt[61] = 0;"; }
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td62'><input type='checkbox' name='REPORTS_CWC_AP_Records' id='f62' value='View' onclick='checkBoxOnClick(this)'";
	if($tempResult[0][1]!='') { $tableREPORTS .= " checked"; }
	$tableREPORTS .= "><label for='f62'>".$Lang['eDiscipline']['CWC']['StudentAPRecord']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
	
	$tableREPORTS .= "<tr class='tablerow1'>";
	
	$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID, 'Discipline', 'REPORTS', 'CWC_Punishment_Count', 'View', '');
	if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[62]=1;"; } else { $show = 0; $restoreField .= "arrayInt[62] = 0;"; }
	$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td63'><input type='checkbox' name='REPORTS_CWC_Punishment_Count' id='f63' value='View' onclick='checkBoxOnClick(this)'";
	if($tempResult[0][1]!='') { $tableREPORTS .= " checked"; }
	$tableREPORTS .= "><label for='f63'>".$Lang['eDiscipline']['CWC']['ClassAPStatTable']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
	
	$tableREPORTS .= "</tr>";
}

// [2018-0920-1312-34207] Detention Punishment List access right
$tableREPORTS .= "<tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID, 'Discipline', 'REPORTS', 'Detention_Punishment_List', 'View', '');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[63]=1;"; } else { $show = 0; $restoreField .= "arrayInt[63] = 0;"; }
$tableREPORTS .= "<td class='".$showTDContent[$show][0]."' id='td64'><input type='checkbox' name='REPORTS_Detention_Punishment_List' id='f64' value='View' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableREPORTS .= " checked"; }
$tableREPORTS .= "><label for='f64'>".$Lang['eDiscipline']['DetentionPunishmentList']." - ".$i_Discipline_System_Access_Right_View."</label> </td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";
$tableREPORTS .= "<td valign='top'>&nbsp;</td>";

$tableREPORTS .= "</tr>";

$tableREPORTS .= "</table>";

$tableSETTINGS .= "<table width='100%' border='0' cellpadding='0' cellspacing='1' bgcolor='#A6A6A6'>";
$tableSETTINGS .= "<tr class='tabletop'>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Award_Punish."</td>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Good_Conduct_Misconduct."</td>";
$tableSETTINGS .= "<td>".(($ldiscipline->UseSubScore) ? $Lang['eDiscipline']['ConductMarkStudyScore'] : $i_Discipline_System_Access_Right_Conduct_Mark)."</td>";
//$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Detention."</td>";
$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_eNotice_Template."</td>";
//$tableSETTINGS .= "<td>".$i_Discipline_System_Access_Right_Letter."</td>";
$tableSETTINGS .= "</tr><tr class='tablerow1'>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'SETTINGS','Award_Punishment','Access','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[39]=1;"; } else { $show = 0; $restoreField .= "arrayInt[39] = 0;";}
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td40'><input type='checkbox' name='SETTINGS_Award_Punishment_Access' id='f40' value='Access' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSETTINGS .= " checked";}
$tableSETTINGS .= "><label for='f40'>".$i_Discipline_System_Access_Right_Access."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'SETTINGS','GoodConduct_Misconduct','Access','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[40]=1;"; } else { $show = 0; $restoreField .= "arrayInt[40] = 0;";}
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td41'><input type='checkbox' name='SETTINGS_GoodConduct_Misconduct_Access' id='f41' value='Access' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSETTINGS .= " checked";}
$tableSETTINGS .= "><label for='f41'>".$i_Discipline_System_Access_Right_Access."</label></td>";

$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'SETTINGS','Conduct_Mark','Access','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[41]=1;"; } else { $show = 0; $restoreField .= "arrayInt[41] = 0;";}
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td42'><input type='checkbox' name='SETTINGS_Conduct_Mark_Access' id='f42' value='Access' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSETTINGS .= " checked";}
$tableSETTINGS .= "><label for='f42'>".$i_Discipline_System_Access_Right_Access."</label></td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'SETTINGS','Detention','Access','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[42]=1;"; } else { $show = 0; $restoreField .= "arrayInt[42] = 0;";}
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td43'><input type='checkbox' name='SETTINGS_Detention_Access' id='f43' value='Access' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSETTINGS .= " checked";}
$tableSETTINGS .= "><label for='f43'>".$i_Discipline_System_Access_Right_Access."</label></td>";
*/
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'SETTINGS','eNoticeTemplate','Access','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[43]=1;"; } else { $show = 0; $restoreField .= "arrayInt[43] = 0;";}
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td44'><input type='checkbox' name='SETTINGS_eNoticeTemplate_Access' id='f44' value='Access' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSETTINGS .= " checked";}
$tableSETTINGS .= "><label for='f44'>".$i_Discipline_System_Access_Right_Access."</label></td>";
/*
$tempResult = $ldiscipline->VIEW_ACCESS_RIGHT($GroupID,$thisModule,'SETTINGS','Letter','Access','');
if($tempResult[0][1]!='') { $show = 1; $restoreField .= "arrayInt[44]=1;"; } else { $show = 0; $restoreField .= "arrayInt[44] = 0;";}
$tableSETTINGS .= "<td class='".$showTDContent[$show][0]."' id='td45'><input type='checkbox' name='SETTINGS_Letter_Access' id='f45' value='Access' onclick='checkBoxOnClick(this)'";
if($tempResult[0][1]!='') { $tableSETTINGS .= " checked";}
$tableSETTINGS .= "><label for='f45'>".$i_Discipline_System_Access_Right_Access."</label></td>";
*/
$tableSETTINGS .= "</tr></table>";


#####

# Top menu highlight setting
$CurrentPage = "Settings_AccessRight";

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 1);
//$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

$subTag[] = array($Lang['eDiscipline']['AccessRight_General_Right'], "group_access_right_view.php?GroupID=$GroupID", 1);
$subTag[] = array($Lang['eDiscipline']['AccessRight_GM_Right'], "group_gm_right.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_AP_Right'], "group_ap_right.php?GroupID=$GroupID", 0);
$subTag[] = array($Lang['eDiscipline']['AccessRight_UserList'], "group_access_right_member_list.php?GroupID=$GroupID", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($i_Discipline_System_Discipline_Group_Access_Rights, "group_access_right.php");
$PAGE_NAVIGATION[] = array($GroupTitle, "group_access_right_view.php?GroupID=$GroupID");
$PAGE_NAVIGATION[] = array($i_Discipline_System_Group_Right_Navigation_Edit_Group, "");
?>

<br />
<script language="javascript">
<!--
var CatArr = new Array();
CatArr[0] = 'MGMT_Award_Punishment';
CatArr[1] = 'MGMT_GoodConduct_Misconduct';
CatArr[2] = 'MGMT_Case_Record';
CatArr[3] = 'MGMT_Conduct_Mark';
CatArr[4] = 'MGMT_Detention';

var CatTaskArr = new Array();
CatTaskArr[0] = '_New';
CatTaskArr[1] = '_Edit';
CatTaskArr[2] = '_Delete';
CatTaskArr[3] = '_Release';
CatTaskArr[4] = '_Finish';
CatTaskArr[5] = '_Waive';
CatTaskArr[6] = '_Approval';
CatTaskArr[7] = '_Adjust';
CatTaskArr[8] = '_Take_Attendance';
CatTaskArr[9] = '_RearrangeStudent';
CatTaskArr[10] = '_NewLeaf';

var radioArr = new Array();
radioArr[0] = 'f10_OwnAll[0]';
radioArr[1] = 'f10_OwnAll[1]';
radioArr[2] = 'f11_OwnAll[0]';
radioArr[3] = 'f11_OwnAll[1]';
radioArr[4] = 'f12_OwnAll[0]';
radioArr[5] = 'f12_OwnAll[1]';
radioArr[6] = 'f13_OwnAll[0]';
radioArr[7] = 'f13_OwnAll[1]';
radioArr[8] = 'f14_OwnAll[0]';
radioArr[9] = 'f14_OwnAll[1]';
radioArr[10] = 'f15_OwnAll[0]';
radioArr[11] = 'f15_OwnAll[1]';
radioArr[12] = 'f16_OwnAll[0]';
radioArr[13] = 'f16_OwnAll[1]';
radioArr[14] = 'f17_OwnAll[0]';
radioArr[15] = 'f17_OwnAll[1]';

function initialDisableCheckboxAndRadio()
{
	// disable all radio buttons expect those that were checked
	for(a=0;a<radioArr.length;a++)
	{
		obj = document.getElementById(radioArr[a]);
		if(obj)
		{
			if(obj.checked==false)
			{
				if(radioArr[a+1]==obj.id.substring(0,10)+'[1]' && document.getElementById(obj.id.substring(0,10)+'[1]').checked==true)
				{
					obj.disabled=false;
				}
				else if(radioArr[a-1]==obj.id.substring(0,10)+'[0]' && document.getElementById(obj.id.substring(0,10)+'[0]').checked==true)
				{
					obj.disabled=false;
				}
				else
				{
					/*
					for(x=0;x<CatArr.length;x++)
					{
						if(document.getElementById(CatArr[x]+'_View').checked==false)
						{
								
						}
					*/
					obj.disabled=true;
					//	}
				}
			}
		}
	}
	
	// disable all checkboxes expect those that were checked
	for(a=0;a<CatArr.length;a++)
	{
		for(b=0;b<CatTaskArr.length;b++)
		{
			//if(document.getElementById(CatArr[a]+'_View').checked==false)
			if($('input[name='+CatArr[a]+'_View]')[0].checked==false)
			{
				// obj = document.getElementById(name);
				name = CatArr[a]+CatTaskArr[b];
				obj = $('[name="'+name+'"]')[0];
				if(obj)
				{
					if(obj.checked==false)
					{
						obj.disabled=true;
					}
				}
			}
		}
	}
}

function checkBoxOnClick(elementRef)
{
	elementRef.parentNode.className = (elementRef.checked) ? 'tablegreenrow3' : 'tabletextremark';
	var PostFixLength = elementRef.name.length-5
	var AccessType = elementRef.name.substring(0,PostFixLength);
	
	PostFix = elementRef.name.substring(PostFixLength);
	if(PostFix=='_View')
	{
		if (elementRef.checked==false)
		{
			// disable and uncheck checkboxes
			for(a=0;a<CatTaskArr.length;a++)
			{
				// obj = document.getElementById(name);
				name = AccessType+CatTaskArr[a];
				obj = $('[name="'+name+'"]')[0];
				if(obj)
				{
					obj.disabled=true;
					obj.checked=false;
					obj.parentNode.className = 'tabletextremark';
				}
			}
			
			// disable own radio buttons
			switch(AccessType)
			{
				case CatArr[0]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[0]);
				objArr[1] = document.getElementById(radioArr[1]);
				objArr[2] = document.getElementById(radioArr[8]);
				objArr[3] = document.getElementById(radioArr[9]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
				
				case CatArr[1]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[2]);
				objArr[1] = document.getElementById(radioArr[3]);
				objArr[2] = document.getElementById(radioArr[10]);
				objArr[3] = document.getElementById(radioArr[11]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
				
				case CatArr[2]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[4]);
				objArr[1] = document.getElementById(radioArr[5]);
				objArr[2] = document.getElementById(radioArr[12]);
				objArr[3] = document.getElementById(radioArr[13]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
				
				case CatArr[4]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[6]);
				objArr[1] = document.getElementById(radioArr[7]);
				objArr[2] = document.getElementById(radioArr[14]);
				objArr[3] = document.getElementById(radioArr[15]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].checked=false;
						objArr[a].disabled=true;
					}	
				}
				break;
			}
		}
		else if (elementRef.checked==true)
		{
			// enable own checkboxes
			for(a=0;a<CatTaskArr.length;a++)
			{
				// obj = document.getElementById(name);
				name = AccessType+CatTaskArr[a];
				obj = $('[name="'+name+'"]')[0];
				if(obj)
				{
					obj.disabled=false;
				}
			}
			
			// enable own radio buttons
			switch(AccessType)
			{
				case CatArr[0]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[0]);
				objArr[1] = document.getElementById(radioArr[1]);
				objArr[2] = document.getElementById(radioArr[8]);
				objArr[3] = document.getElementById(radioArr[9]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
				
				case CatArr[1]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[2]);
				objArr[1] = document.getElementById(radioArr[3]);
				objArr[2] = document.getElementById(radioArr[10]);
				objArr[3] = document.getElementById(radioArr[11]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
				
				case CatArr[2]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[4]);
				objArr[1] = document.getElementById(radioArr[5]);
				objArr[2] = document.getElementById(radioArr[12]);
				objArr[3] = document.getElementById(radioArr[13]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
				
				case CatArr[4]:
				var objArr = new Array();
				objArr[0] = document.getElementById(radioArr[6]);
				objArr[1] = document.getElementById(radioArr[7]);
				objArr[2] = document.getElementById(radioArr[14]);
				objArr[3] = document.getElementById(radioArr[15]);
				for(a=0;a<objArr.length;a++)
				{
					if(objArr[a])
					{
						objArr[a].disabled=false;
					}	
				}
				break;
			}
		}
	}
}

function resetTD() {
	var tdClassName = "";
	var arrayInt = new Array();
	<?= $restoreField ?>
	var tt = "";
	for(var i=1;i<=55;i++) {
		tdClassName = eval("document.getElementById('td" + i + "')");
		if(tdClassName != null) {
			tt += i + ". " + tdClassName + "\n";
			if(arrayInt[i-1]==1) {
				tdClassName.className = 'tablegreenrow3';	
			}
			else {
				tdClassName.className = 'tabletextremark';	
			}	
		}
	}
}

function checkForm(form1) {
	if(form1.GroupTitle.value=='')	 {
		alert("<?= $i_Discipline_System_alert_require_group_title ?>");	
		form1.GroupTitle.focus();
		return false;
	}
	else {
		return true;
	}
}
//-->
</script>

<form name="form1" method="post" action="group_access_right_edit_update.php" onSubmit="return checkForm(this)">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
				</tr>
			</table>		
			<br />					
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="tab_underline">
						<div class="shadetabs">
							<ul>
								<?=$ldiscipline->getSubTag($subTag)?>
							</ul>
						</div>
					</td>
				</tr>
			</table>
					
			<table width="88%" border="0" cellpadding="5" cellspacing="0">
				<tr>
					<td align="left">
						<table width="100%" cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td width="60%"><?=$linterface->GET_NAVIGATION2($i_Discipline_System_Group_Right_Navigation_Group_Info);?></td>
								<td width="40%" align="right"><?=$linterface->GET_SYS_MSG('',$xmsg2) ?></td>
							</tr>
						</table>
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr valign="top">
								<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Title?><span class="tabletextrequire">*</span></td>
								<td><input name="GroupTitle" type="text" class="textboxtext" value="<?= $GroupTitle ?>"></td>
							</tr>
							<tr valign="top">
								<td valign="top" nowrap="nowrap" class="formfieldtitle"><?=$i_Discipline_System_Group_Right_Navigation_Group_Description?></td>
								<td class="formfieldtitle"><?=$linterface->GET_TEXTAREA('GroupDescription', $GroupDescription);?></td>
							</tr>
						</table>
						<table width="95%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td><?=$linterface->GET_NAVIGATION2($i_Discipline_System_Group_Right_Navigation_Group_Access_Right);?></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Management ?></i>
								<?= $tableMGMT ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?= $i_Discipline_System_Teacher_Right_Statistics ?></i>
								<?= $tableSTAT ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Reports ?></i>
								<?= $tableREPORTS ?>
							</td>
						</tr>
						<tr><td>&nbsp;</td></tr>
						<tr>
							<td class="form_sep_title"><i><?=$i_Discipline_System_Teacher_Right_Settings ?></i>
								<?= $tableSETTINGS ?>
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						</table>
						
						<!--
						<table width="100%" cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td colspan="2"><?=$linterface->GET_NAVIGATION2("GM item");?></td>
							</tr>
							
							<tr>
								<td colspan="2">&nbsp;</td>
							</tr>
							
							
						</table>
						//-->
						
						<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?=$i_general_required_field ?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="center">
					<table width="88%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td align="center">
								<input type="hidden" name="GroupID" value="<?= $GroupID ?>">
								<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_reset, "reset","javascript:resetTD()")?>&nbsp;
								<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='group_access_right_view.php?GroupID=$GroupID'")?>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			<br>
		</td>
	</tr>
</table>

<script>
initialDisableCheckboxAndRadio();
</script>

</form>

<?php
echo $linterface->FOCUS_ON_LOAD("form1.GroupTitle"); 

temp_function();

$linterface->LAYOUT_STOP();
intranet_closedb();
?>

<?
function temp_function()
{
    //global $i_GroupRole, $i_admintitle_group, $button_add, $button_remove, $intranet_session_language, $linterface, $i_eNews_AddTo, $i_eNews_DeleteTo, $PATH_WRT_ROOT;
    global $ldiscipline;
    
    $t = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT();
//  debug_pr($t);
//	$t2 = $ldiscipline->RETRIEVE_ALL_CONDUCT_ITEM();
    
    # retieve All Misconduct Category
//  $mc_categoryid = $ldiscipline->RETRIEVE_CONDUCT_CATEGORY(MISCONDUCT);
//  debug_pr($mc_categoryid);
    $All_Misconduct_Items_Temp = $ldiscipline->RETRIEVE_CONDUCT_ITEM_GROUPBY_CAT(MISCONDUCT);
    foreach($All_Misconduct_Items_Temp as $k=>$d)
    {
	    list($thisCategoryID, $thisItemID, $thisItemName) = $d;
	    $All_Misconduct_Items[] = array($thisCategoryID, $thisItemID, $thisItemName);
// 	    $thisCategoryName = $ldiscipline->RETRIEVE_ACCU_CATEGORY_NAME($thisCategoryID);
// 	 	debug_pr($thisCategoryName ." - " . $thisItemName);
    }
//  debug_pr($All_Misconduct_Items);

	$sql = "SELECT 
        		CategoryID, 
        		ItemID
    		FROM 
    			DISCIPLINE_GROUP_ACCESS_GM_ITEM
    		WHERE 
    			GroupID = $GroupID
    		";
	$AllowMisconductItems = $ldiscipline->returnArray($sql);
//  debug_pr($AllowMisconductItems);
    
	$NotAllowMisconductItems = array_diff_assoc($All_Misconduct_Items, $All_Misconduct_Items);
//	debug_pr($NotAllowMisconductItems);
	
    /*
    $AllAllow = 0;
    $GroupsAry = $ldiscipline->GET_ACCESS_RIGHT_GROUP();
    
    $sql = "
    		SELECT 
        		a.GroupID, 
        		b.GroupTitle
    		FROM 
    			DISCIPLINE_CATEGORY_GROUP_ACCESSRIGHT a
    			left join ACCESS_RIGHT_GROUP as b on (b.GroupID = a.GroupID)
    		WHERE 
    			a.CategoryID = $CategoryID
    		";
	$AllowGroups = $ldiscipline->returnArray($sql);
	if(empty($AllowGroups))
	{
		$AllowGroups = $ldiscipline->GET_ACCESS_RIGHT_GROUP();
	}	

	$NotAllowGroups = array_diff_assoc($GroupsAry, $AllowGroups);
	
    $x .= "<table border=0 cellpadding=5 cellspacing=0>\n";
    $x .= "<tr>\n";
    $x .= "<td>Allow Group(s):</td>\n";
    $x .= "<td>&nbsp;</td>\n";
    $x .= "<td>Not Allow Group(s):</td>\n";
    $x .= "</tr>\n";
    $x .= "<tr>\n";
    
    ##### Left selection
    $x .= "<td class=tableContent>\n";
    $x .= "<select name=AllowGroupID[] id=AllowGroupID size=10 multiple>\n";
	foreach($AllowGroups as $k1=>$d1)
	{
            $GroupID = $d1['GroupID'];
            $Title = $d1['GroupTitle'];
            $x .= "<option value=$GroupID> $Title</option>\n";
    }
    $x .= "</select>\n";
    $x .= "</td>\n";
            
    ##### Middle button
    
    include_once("libinterface.php");
    $linterface 	= new interface_html();
	$x .= "<td class=tableContent align=center>\n";
    $x .= $linterface->GET_BTN("<< ".$i_eNews_AddTo, "submit", "checkOptionTransfer(this.form.elements['NotAllowGroupID[]'],this.form.elements['AllowGroupID[]']);return false;", "submit2") . "<br /><br />";
    $x .= $linterface->GET_BTN($i_eNews_DeleteTo . " >>", "submit", "checkOptionTransfer(this.form.elements['AllowGroupID[]'],this.form.elements['NotAllowGroupID[]']);return false;", "submit23");
	$x .= "</td>\n";
            
	##### Right selection
	$x .= "<td class=tableContent>\n";
    $x .= "<select name=NotAllowGroupID[] id=NotAllowGroupID size=10 multiple>\n";
	foreach($NotAllowGroups as $k2=>$d2)
	{
            $GroupID = $d2['GroupID'];
            $Title = $d2['GroupTitle'];
            $x .= "<option value=$GroupID> $Title</option>\n";
    }
    $x .= "</select>\n";
    $x .= "</td>\n";
    $x .= "</tr>\n";
	$x .= "</table>\n";
    return $x;
    */
}
?>