<?php
#modifing by yat

############ Change Log [Start] #############
# 
#	Date:	2010-03-10 YatWoon
#			Add "overview" access right for Student/Parent mode
#
############ Change Log [End] #############

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

$viewItem = array();
$viewValue = array();
$dataAry = array();

$thisModule = "Discipline";

$dataAry['VIEWING']['GoodConduct_Misconduct'][] = $_POST['GoodConduct_Misconduct'];
$dataAry['VIEWING']['Award_Punishment'][] = $_POST['Award_Punishment'];
$dataAry['VIEWING']['Detention'][] = $_POST['Detention'];
$dataAry['VIEWING']['StudentReport'][] = $_POST['StudentReport'];
$dataAry['VIEWING']['PersonalReport'][] = $_POST['PersonalReport'];
$dataAry['RANKING']['GoodConduct_Ranking'][] = $_POST['GoodConduct_Ranking'];
$dataAry['RANKING']['Award_Ranking'][] = $_POST['Award_Ranking'];
$dataAry['VIEWING']['Overview'][] = $_POST['Overview'];

$viewItem[0] = 'Award_Punishment';
$viewItem[1] = 'GoodConduct_Misconduct';
$viewItem[2] = 'Detention';
$viewItem[3] = 'StudentReport';
$viewItem[4] = 'PersonalReport';
$viewItem[5] = 'Award_Ranking';
$viewItem[6] = 'GoodConduct_Ranking';
$viewItem[7] = 'Overview';

$viewValue[0] = $_POST['Award_Punishment'];
$viewValue[1] = $_POST['GoodConduct_Misconduct'];
$viewValue[2] = $_POST['Detention'];
$viewValue[3] = $_POST['StudentReport'];
$viewValue[4] = $_POST['PersonalReport'];
$viewValue[5] = $_POST['Award_Ranking'];
$viewValue[6] = $_POST['GoodConduct_Ranking'];
$viewValue[7] = $_POST['Overview'];

$GroupID = $ldiscipline->GET_ACCESS_RIGHT_GROUP_ID('S','');
if(empty($GroupID))
{
	$GroupID = $ldiscipline->NEW_ACCESS_RIGHT_GROUP('S','','');
}

$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'S', $thisModule, 'VIEWING','',''); 
$ldiscipline->DELETE_ACCESS_RIGHT($GroupID, 'S', $thisModule, 'RANKING','',''); 


$ldiscipline->INSERT_ACCESS_RIGHT($GroupID, $thisModule, $dataAry);

header("Location: student_access_right.php?xmsg=update");

intranet_closedb();

?>