<?php 
// Modifying by : 

########## Change Log ###############
#
#	Date	:	2017-10-13 (Bill)	[DM#3273]
#	Details :	fixed Session Problem in PHP 5.4
#
#	Date	:	2016-06-21 (Anna)
#	Derails	:	add searchbox in table
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_right_page_number!=$pageNo && $pageNo!="")
{
	setcookie("ck_right_page_number", $pageNo, 0, "", "", 0);
	$ck_right_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_right_page_number!="")
{
	$pageNo = $ck_right_page_number;
}

if ($ck_right_page_order!=$order && $order!="")
{
	setcookie("ck_right_page_order", $order, 0, "", "", 0);
	$ck_right_page_order = $order;
}
else if (!isset($order) && $ck_right_page_order!="")
{
	$order = $ck_right_page_order;
}

if ($ck_right_page_field!=$field && $field!="")
{
	setcookie("ck_right_page_field", $field, 0, "", "", 0);
	$ck_right_page_field = $field;
}
else if (!isset($field) && $ck_right_page_field!="")
{
	$field = $ck_right_page_field;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if(!$ldiscipline->IS_ADMIN_USER($_SESSION['UserID'])) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
}

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$order = ($order == 1) ? 1 : 0;
$field = ($field == "") ? 0 : $field;
$GroupType = 'A';

$keyword = standardizeFormPostValue($_POST['keyword']);
if ($keyword != '') {
	$Keyword_cond = " and (GroupTitle LIKE '%".$ldiscipline->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($keyword))."%')" ;
}

$sql  = "SELECT
               	CONCAT('<a class=\'tablelink\' href=\'group_access_right_view.php?GroupID=', a.GroupID, '\'>', REPLACE(GroupTitle,'<','&lt;'), '</a>'),
               	REPLACE(GroupDescription,'<','&lt;'),
               	CONCAT('<a class=\'tablelink\' href=\'group_access_right_member_list.php?GroupID=',a.GroupID,'\'>',COUNT(b.UserID),'</a>') as countUser,
               	CONCAT('<input type=\'checkbox\' name=\'GroupID[]\' value=\'', a.GroupID ,'\'>')
         FROM
                ACCESS_RIGHT_GROUP as a 
                LEFT OUTER JOIN ACCESS_RIGHT_GROUP_MEMBER as b ON a.GroupID=b.GroupID
         WHERE 
         		GroupType = '$GroupType' 
         		and a.Module='". $ldiscipline->Module."' 
         		$Keyword_cond
         GROUP BY 
         		a.GroupID ";
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("GroupTitle", "GroupDescription", "countUser");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0);
$li->wrap_array = array(0,0,0);
$li->IsColOff = 2;

$pos = 0;
$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='40%' >".$li->column($pos++, $i_Discipline_System_Group_Right_Group_Name)."</td>\n";
$li->column_list .= "<td width='30%'>".$li->column($pos++, $i_Discipline_System_Group_Right_Description)."</td>\n";
$li->column_list .= "<td width='30%'>".$li->column($pos++, $i_Discipline_System_Group_Right_No_Of_Members)."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("GroupID[]")."</td>\n";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'group_access_right_new.php')",$button_new,"","","",0);

# Top Menu
$CurrentPage = "Settings_AccessRight";
$CurrentPageArr['eDisciplinev12'] = 1;

$TAGS_OBJ[] = array($i_Discipline_System_Discipline_Members_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/group_access_right.php", 1);
//$TAGS_OBJ[] = array($i_Discipline_System_Teacher_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/teacher_access_right.php", 0);
$TAGS_OBJ[] = array($i_Discipline_System_Student_Rights, $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/access_right/student_access_right.php", 0);

# Left Menu 
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# Start Layout
$linterface->LAYOUT_START();

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
?>

<script language="javascript">
<!--
function removeCat(obj,element,page){
	var alertConfirmRemove = "<?=$i_Discipline_System_alert_remove_record?>";
    if(countChecked(obj,element)==0) {
        alert(globalAlertMsg2);
    }
    else{
        if(confirm(alertConfirmRemove)){
            obj.action=page;
            obj.method="post";
            obj.submit();
        }
    }
}
//-->
</script>

<br />
<form name="form1" method="post" action="">
<table width="98%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="center">
			<table width="95%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td align="center">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="70%"><?=$toolbar ?></td>
											<td width="30%" align="right"><?= $linterface->GET_SYS_MSG($xmsg) ?></td>
											<td><?=$htmlAry['searchBox']?></td>	
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="28" align="right" valign="bottom" >
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td align="right" valign="bottom">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
														<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
															<table border="0" cellspacing="0" cellpadding="2">
																<tr>
																	<td nowrap="nowrap"><a href="javascript:checkEdit(document.form1,'GroupID[]','group_access_right_edit.php')" class="tabletool" title="<?= $button_edit ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_edit.gif" width="12" height="12" border="0" align="absmiddle"> <?=$button_edit ?></a></td>
																	<td><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="5"></td>
																	<td nowrap="nowrap"><a href="javascript:removeCat(document.form1,'GroupID[]','group_access_right_remove.php')" class="tabletool" title="<?= $button_delete ?>"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_delete.gif" width="12" height="12" border="0" align="absmiddle"> <?= $button_delete ?></a></td>
																</tr>
															</table>
														</td>
														<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?= $li->display() ?>
					</td>
				</tr>
			</table>
			<br>
			
			<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
			<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
			<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
			<input type="hidden" name="page_size_change" value="" />
			<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
		
		</td>
	</tr>
</table>
</form>

<?php
$linterface->LAYOUT_STOP();
intranet_closedb();
?>