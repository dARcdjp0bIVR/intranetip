<?php

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

//	include_once($PATH_WRT_ROOT."includes/libdiscipline.php");
$ldiscipline = new libdisciplinev12();
	
$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_AccessRight";
	
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$MODULE_OBJ['title'] = $iDiscipline['select_members'];
	
$linterface = new interface_html("popup.html");

$lclass = new libclass();

$teacherTypeSelection = $lclass->getTeacherTypeSelection("onChange='document.form1.submit();'", $teacherType);

/* 
# original
if($sys_custom['wscss_disciplinev12_student_access_cust']) {
	$select_teacher = $lclass->getTeacherAndStudent("size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
} else {
	$select_teacher = $lclass->getTeacher("size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
}
*/

if($teacherType!="") {
	if($sys_custom['wscss_disciplinev12_student_access_cust']) {
		$select_teacher = $lclass->getTeacherAndStudent("size=\"21\" multiple=\"multiple\" name=\"targetID[]\"", $teacherType);
	} else {
		$select_teacher = $lclass->getTeacher("size=\"21\" multiple=\"multiple\" name=\"targetID[]\"", $teacherType);
	}
} 
  
################################################################

$linterface->LAYOUT_START();
?>

<script language="javascript">
function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.form1.flag.value = 0;

     //par.generalFormSubmitCheck(par.form1);
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}
</script>

<form name="form1" action="index.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
		<tr><td colspan="2" height="5"></td></tr>		
		<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td colspan="2" height="5"></td></tr>
		<tr>
			<td valign="top" nowrap="nowrap" ><span class="tabletext"><?=$Lang['eDiscipline']['TeacherType']?>: </span></td>
			<td width="80%" style="align: left">
		       	<table border="0" cellpadding="0" cellspacing="0" align="left">
		       		<tr>
		       			<td colspan="2"><?=$teacherTypeSelection?></td>
		       		</tr>
		       		<? if($teacherType!="") { ?>
					<tr> 
						<td>
		       				<?=$select_teacher ?>
		       			</td>
		       			<td style="vertical-align:bottom">        
					        <table width="100%" border="0" cellspacing="0" cellpadding="6">
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_BTN($button_add, "button", "checkOption(this.form.elements['targetID[]']);AddOptions(this.form.elements['targetID[]'])")?>
									</td>
								</tr>
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(this.form.elements['targetID[]']); return false;")?>
									</td>
								</tr>
							</table>
		       			</td>
		       		</tr>
		       		<? } ?>
		       	</table>
			</td>
		</tr>
	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
</form>
<?
$linterface->LAYOUT_STOP();
?>