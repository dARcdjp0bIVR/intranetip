<?php
// Modifying by:

####################################
#
#   Date:   2019-12-02 (Bill)   [2019-1127-1117-54066]
#           fixed cannot set HTTP host hyperlink
#
#   Date:   2019-05-01 (Bill)
#           Category Selection > Change 'CategoryID' to 'CategoryType' to prevent IntegerSafe()    [DM#1189]
#
####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

if (!$plugin['Disciplinev12'])
{
	header("Location: ./");
	exit;
}

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");

$Title = htmlspecialchars(trim($Title));
$Content = htmlspecialchars(trim($Content));
$Subject = htmlspecialchars(trim($Subject));
$qStr = htmlspecialchars(trim($qStr));
$cStr = htmlspecialchars(trim($cStr));
$sendReplySlip = (isset($ReplySlip) && $ReplySlip==1) ? 1 : 0;

$TempVar = $ldiscipline->TemplateVariable($CategoryType);

// [2019-1127-1117-54066]
//$Content = str_replace("http://".$_SERVER['HTTP_HOST'],'',$Content);
//$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);
$Content = str_replace(array("http://".$_SERVER['HTTP_HOST']."/", "https://".$_SERVER['HTTP_HOST']."/"),"/", $Content);
$cStr = str_replace(array("http://".$_SERVER['HTTP_HOST']."/", "https://".$_SERVER['HTTP_HOST']."/"),"/", $cStr);

$sql = "UPDATE
            INTRANET_NOTICE_MODULE_TEMPLATE 
		SET
    		CategoryID = '$CategoryType',
    		Title = '$Title',
    		Content = '$Content',
    		Subject = '$Subject',
    		ReplySlip = '$qStr',
    		ReplySlipContent = '$cStr',
    		RecordStatus = '$RecordStatus',
    		SendReplySlip = '$sendReplySlip',
    		DateModified = now()
		WHERE 
		    TemplateID = '$TemplateID' ";
if($ldiscipline->db_db_query($sql))
{
	$SysMsg = "update";
}
else
{
	$SysMsg = "update_failed";
}	

$ReturnPage = "index.php?xmsg=$SysMsg";

intranet_closedb();
header("Location: $ReturnPage");
?>