<?php
// modifying : 

#####################################
#	Date:	2013-05-16	YatWoon
#			remove eNotice disable checking [Case#2013-0516-1112-37073 ]
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();

// if (!$lnotice->disabled)
// {
    $lf = new libform();
    
    $MODULE_OBJ['title'] = $i_Notice_ReplyContent;
$linterface = new interface_html("popup.html");
$linterface->LAYOUT_START();

?>


<script language="javascript" src="/templates/forms/form_edit.js"></script>
<script language="javascript" src="/templates/forms/layer.js"></script>

<br />


<form name="ansForm" action="">
<input type="hidden" name="qStr" value="">
<input type="hidden" name="aStr" value="">

<script language="Javascript">
// Grab values from
s = new String(window.opener.document.form1.qStr.value);
s = s.replace(/"/g, '&quot;');
document.ansForm.qStr.value = s;
document.ansForm.aStr.value = window.opener.document.form1.aStr.value;
function copyback()
{
         finish();
         window.opener.document.form1.qStr.value = document.ansForm.qStr.value;
         window.opener.document.form1.aStr.value = document.ansForm.aStr.value;
         self.close();
}
<?=$lf->getWordsInJS4eDiscipline()?>

var answer_sheet	= '<?=$i_Notice_ReplyContent?>';
var space10 	= '<img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" />';
var add_btn 	= '<?=str_replace("'", "\'",$linterface->GET_BTN($button_add, 'button', 'retainValues(); if (appendTxt(this.form.name)) {writetolayer(\'blockInput\',sheet.writeSheet()); this.form.reset(); this.form.secDesc.focus();}','submit2'))?>';
var order_name	= '<?=$i_Discipline_System_Discipline_Template_Topic_Title?>';
<? $PAGE_NAVIGATION1[] = array($eDiscipline['Content']); ?>
var Part1 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION1))?>'
<? $PAGE_NAVIGATION2[] = array($i_general_DisplayOrder); ?>
var Part2 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION2))?>'
<? $PAGE_NAVIGATION3[] = array($i_QB_Question . ($intranet_session_language=="en"?" ":"") . $i_menu_setting); ?>
var Part3 = '<?=str_replace("'","\'",$linterface->GET_NAVIGATION($PAGE_NAVIGATION3))?>'
replyslip = '<?=$i_Notice_ReplySlip?>';
var pls_fill_in_title = "<?=$i_alert_pleasefillin." ".$i_general_title?>";
var MoveUpBtn = "<?=$Lang['Button']['MoveUp']?>";
var MoveDownBtn = "<?=$Lang['Button']['MoveDown']?>";
var DeleteBtn = "<?=$Lang['Button']['Delete']?>";
var EditBtn = "<?=$Lang['Button']['Edit']?>";

var form_templates = new Array();     // template not in this moment
background_image = "";

var sheet= new Answersheet();
// attention: MUST replace '"' to '&quot;'
sheet.qString = document.ansForm.qStr.value;
sheet.mode=0;        // 0:edit 1:fill in application
sheet.answer=sheet.sheetArr();
sheet.templates=form_templates;
document.write(editPaneleDiscipline());
</script>



<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "copyback(); return false;","submit2") ?>
				<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
	</td>
</tr>
</table>                   

</form>
<?php
// }
// else
// {
//     header ("Location: /");
// }

intranet_closedb();
$linterface->LAYOUT_STOP();
?>