<?php
# using: yat

###### Change Log [Start] ######
#
#	Date:	2013-05-16	YatWoon
#			remove eNotice disable checking [Case#2013-0516-1112-37073 ]
#
#	Date	:	2013-02-04 (YatWoon)
#				Fixed: display auto fill-in selection
#
#	Date	:	2011-09-20 (Henry Chow)
#				allow empty content in "Content Base Reply Slip" 
#
####### Change Log [End] #######
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$lnotice = new libnotice();
$ldiscipline = new libdisciplinev12();

// if (!$lnotice->disabled)
// {
    $MODULE_OBJ['title'] = $i_Notice_ReplyContent;
	$linterface = new interface_html("popup.html");
	$linterface->LAYOUT_START();
	
	if($TemplateID)
	{
		$tempDetails = $ldiscipline->retrieveTemplateDetails($TemplateID);
		list($Module,$CategoryID,$Title,$Subject,$Content,$ReplySlip,$RecordType,$RecordStatus,$ReplySlipContent) = $tempDetails[0];
	}
		
	if($cStr)
	{
		$cStr = stripslashes($cStr);
		$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);	
		$ReplySlipContent = $cStr;
	}
	
?>

<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_yahoo.js"></script>
<script language="JavaScript" src="<?=$PATH_WRT_ROOT?>templates/ajax_connection.js"></script>

<script type="text/javascript">
var callback_changeGenVariables = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('genVariableDiv').innerHTML = tmp_str;
	}
}

function changeGenVariables()
{
	obj = document.ansForm;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_genvariable.php";
	
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_changeGenVariables);
}

function copyback()
{
	var field = FCKeditorAPI.GetInstance('Content');
	var content_var = field.GetHTML(true);
	/*
	if(content_var=="")
	{
		// commented by Henry on 20110920, allow empty (synchronized with question base reply slip)
		//alert('<?=$i_Form_pls_fill_in?>');
	}
	else
	{
	*/
 		window.opener.document.form1.cStr.value = content_var;
 		self.close();
	//}
}
</script>

<br /> 
 
<form name="ansForm" action="">
<table align="center" width="95%" border="0" cellpadding="5" cellspacing="0">
<tr valign="top">
	<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eNotice['ReplySlipType']?></td>
	<td class="tabletext" colspan="2"><?=$eNotice['ContentBase']?></td>
</tr>

<tr valign="top">
	<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$eDiscipline['Content']?></td>
	<td><div id="genVariableDiv"><?=$select?></div></td>
</tr>

<tr valign="top">
	<td align="center" colspan="3">
		<?
		/*
			$msg_box_width = "100%";
			$msg_box_height = 200;
			$obj_name = "Content";
			$editor_width = $msg_box_width;
			$editor_height = $msg_box_height;
			$init_html_content = $ReplySlipContent;
			include($PATH_WRT_ROOT."includes/html_editor_embed_discipline.php");
		*/
		
		include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
		$objHtmlEditor = new FCKeditor ( 'Content' , "100%", "250", "","", $ReplySlipContent);
		$objHtmlEditor->Create();
		?>
	</td>
	
</tr>
<? /*?>
	<tr valign="top">
		<td width="20%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['AutoFillIn']?></td>
		<td class="tabletext"><div id="genVariableDiv"><?=$select?></div></td>
		<td width="100%"><input type="button" onClick="FillIn()" value="<?=$Lang['eDiscipline']['Insert']?>"></td>
	</tr>
						<? */ ?>			
			
	<script language="javascript">
	function FillIn()
	{
		var field = FCKeditorAPI.GetInstance('Content');
		//alert(field.GetHTML(true));
		field.InsertHtml(document.ansForm.genVariable.value);
	}
	</script>
<tr>
	<td class="dotline" colspan="3"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center" colspan="3">
				<?= $linterface->GET_ACTION_BTN($button_save, "submit", "copyback(); return false;","submit2") ?>
				<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
	</td>
</tr>
</table>                   

<input type="hidden" name="CategoryID" value="<?=$CategoryID?>">
</form>
<script>changeGenVariables()</script>

<?php
// }
// else
// {
//     header ("Location: /");
// }

intranet_closedb();
$linterface->LAYOUT_STOP();
?>