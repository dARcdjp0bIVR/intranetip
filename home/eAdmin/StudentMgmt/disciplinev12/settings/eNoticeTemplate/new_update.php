<?php
# using:

####################################
#
#   Date:   2019-12-02 (Bill)   [2019-1127-1117-54066]
#           fixed cannot set HTTP host hyperlink
#
#   Date:   2019-05-01 (Bill)
#           Category Selection > Change 'CategoryID' to 'CategoryType' to prevent IntegerSafe()    [DM#1189]
#
####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");

$qStr = htmlspecialchars(trim($qStr));
$Title = htmlspecialchars(trim($Title));
$Content = htmlspecialchars(trim($Content));
$Subject = htmlspecialchars(trim($Subject));
$TempVar = $ldiscipline->TemplateVariable($CategoryType);

// [2019-1127-1117-54066]
//$Content = str_replace("http://".$_SERVER['HTTP_HOST'],'',$Content);
$Content = str_replace(array("http://".$_SERVER['HTTP_HOST']."/", "https://".$_SERVER['HTTP_HOST']."/"),"/", $Content);

// [2019-1127-1117-54066]
$cStr = htmlspecialchars(trim($cStr));
//$cStr = str_replace("http://".$_SERVER['HTTP_HOST'],'',$cStr);
$cStr = str_replace(array("http://".$_SERVER['HTTP_HOST']."/", "https://".$_SERVER['HTTP_HOST']."/"),"/", $cStr);

$sendReplySlip = (isset($ReplySlip) && $ReplySlip==1) ? 1 : 0;

$fields = "Module, CategoryID, Title, Content, Subject, RecordStatus, ReplySlip, DateInput, DateModified, ReplySlipContent, SendReplySlip";
$InsertValue = "'DISCIPLINE', '$CategoryType', '$Title', '$Content', '$Subject', '$RecordStatus', '$qStr', now(), now(), '$cStr', '$sendReplySlip'";
$sql = "INSERT INTO INTRANET_NOTICE_MODULE_TEMPLATE ($fields) VALUES ($InsertValue)";
if($ldiscipline->db_db_query($sql))
{
	$SysMsg = "add";
}
else
{
	$SysMsg = "add_failed";
}	

$ReturnPage = "index.php?xmsg=$SysMsg";

intranet_closedb();
header("Location: $ReturnPage");
?>