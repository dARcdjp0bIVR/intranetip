<?php
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
intranet_auth();
intranet_opendb();


$ldiscipline = new libdisciplinev12();
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-eNoticeTemplate-Access");
$IDs = implode(",",$TemplateID);
$sql = "DELETE FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE TemplateID in ($IDs) ";

if($ldiscipline->db_db_query($sql))
{
	$SysMsg = "delete";
}
else
{
	$SysMsg = "delete_failed";
}	
		
$ReturnPage = "index.php?xmsg=$SysMsg";


intranet_closedb();
header("Location: $ReturnPage");
?>
