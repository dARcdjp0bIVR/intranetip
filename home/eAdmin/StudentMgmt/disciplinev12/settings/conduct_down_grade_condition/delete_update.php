<?php
// Editing by Carlos
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Access Right 
if(!$sys_custom['eDiscipline']['yy3']){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$ItemID = $_REQUEST['ItemID'];

$sql = "DELETE FROM DISCIPLINE_CONDUCT_DOWN_GRADE_CONDITION WHERE ItemID='$ItemID'";
$result = $ldiscipline->db_db_query($sql);

if($result){
	$xmsg = 'DeleteSuccess';
}else{
	$xmsg = 'DeleteUnsuccess';
}

header("Location: index.php?xmsg=$xmsg");
intranet_closedb();
?>