<?php
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2015-04-13 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	add reference for auto select detention and eNotice option
#
#	Date	:	2010-06-11 (Henry)
#	Detail 	:	import of Category
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access");

$linterface = new interface_html();
$lsp = new libstudentprofile();

# menu highlight setting
$CurrentPage = "Settings_GoodConductMisconduct";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportCategory']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 0);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

if(!isset($MeritType) || $MeritType=="") $MeritType = 1;

if($MeritType==-1 && $sys_custom['Discipline_SeriousLate']) {
	$sample_file = "goodconduct_misconduct_category_withSeriousLate_settings_unicode.csv";
} else {
	$sample_file = "goodconduct_misconduct_category_settings_unicode.csv";
}

$csvFile = "<a class=\"tablelink\" href=\"/templates/get_sample_csv.php?file=$sample_file\" target=\"_self\">".$Lang['General']['ClickHereToDownloadSample']."</a>";

$csv_format = "";
$delim = "<br>";
for($i=0; $i<sizeof($Lang['eDiscipline']['Import_GM_Col']); $i++) {
	if($i!=0) $csv_format .= $delim;
	// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
//	if($i!=5 || ($MeritType==-1 && $sys_custom['Discipline_SeriousLate'])) {
	if($i!=7 || ($MeritType==-1 && $sys_custom['Discipline_SeriousLate'])) {
		$csv_format .= $Lang['SysMgr']['Homework']['Column']." ".($i+1)." : ".$Lang['eDiscipline']['Import_GM_Col'][$i];
	}
}

$reference_str = "<div id=\"ref_list\" style='position:absolute; height:150px; z-index:1; visibility: hidden;'></div>";
// [2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073] add reference for auto select detention and eNotice option
$reference_str .= "<a class=\"tablelink\" href=javascript:show_ref_list('conversionPeriod','cc_click')>".$eDiscipline['Setting_Period']."</a>,<span id=\"cc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('meritType','mc_click')>".$iDiscipline['MeritType']."</a>,<span id=\"mc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('status','sc_click')>".$eDiscipline['Setting_Status']."</a>,<span id=\"sc_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('defaultDetention','dt_click')>".$Lang['eDiscipline']['DefaultSetDetention']."</a>,<span id=\"dt_click\">&nbsp;</span>
				<a class=\"tablelink\" href=javascript:show_ref_list('defaultNotice','dn_click')>".$Lang['eDiscipline']['DefaultSendNotice']."</a>,<span id=\"dn_click\">&nbsp;</span>";
//				<a class=\"tablelink\" href=javascript:show_ref_list('templateID','tc_click')>".$Lang['eDiscipline']['Import_GM_Col'][4]."</a><span id=\"tc_click\">&nbsp;</span>
$reference_str .= "<a class=\"tablelink\" href=javascript:show_ref_list('templateID','tc_click')>".$Lang['eDiscipline']['Import_GM_Col'][6]."</a><span id=\"tc_click\">&nbsp;</span>
				</p>";	

$linterface->LAYOUT_START();

$url = ($MeritType==1) ? "goodconduct/category.php" : "misconduct/category.php";


?>
<script language="JavaScript">
var ClickID = '';
var callback_show_ref_list = {
	success: function ( o )
    {
	    var tmp_str = o.responseText;
	    document.getElementById('ref_list').innerHTML = tmp_str;
	    DisplayPosition();
	}
}

function show_ref_list(type,click)
{
	ClickID = click;
	document.getElementById('task').value = type;
	obj = document.form1;
	YAHOO.util.Connect.setForm(obj);
	var path = "ajax_gm.php";
    var connectionObject = YAHOO.util.Connect.asyncRequest("POST", path, callback_show_ref_list);
}

function Hide_Window(pos){
  document.getElementById(pos).style.visibility='hidden';
}

function getPosition(obj, direction)
{
	var objStr = "obj";
	var pos_value = 0;
	while (typeof(eval(objStr))!="undefined" && eval(objStr + ".tagName")!="BODY")
	{
		pos_value += eval(objStr + "." + direction);
		objStr += ".offsetParent";
	}

	return pos_value;
}

function DisplayPosition(){
	
  document.getElementById('ref_list').style.left=getPosition(document.getElementById(ClickID),'offsetLeft') +10;
  document.getElementById('ref_list').style.top=getPosition(document.getElementById(ClickID),'offsetTop');
  document.getElementById('ref_list').style.visibility='visible';
}
</script>
<br />
<form name="form1" method="post" action="import_result.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td align="right" colspan="2"><?=$linterface->GET_SYS_MSG($xmsg, $xmsg2);?></td>
	</tr>
	<tr>
		<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="90%" border="0" cellpadding="5" cellspacing="0" align="center">
				<tr>
					<td class="formfieldtitle" align="left" width="30%"><?=$Lang['General']['SourceFile']." <span class='tabletextremark'>".$Lang['General']['CSVFileFormat']."</span>"?>
					</td>
					<td class="tabletext" width="70%">
						<input class="file" type="file" name="csvfile">
					</td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['General']['CSVSample']?></td>
					<td class="tabletext"><?=$csvFile?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn']?></td>
					<td class="tabletext"><?=$csv_format?></td>
				</tr>
				<tr>
					<td class="formfieldtitle" align="left" height="30"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference']?></td>
					<td class="tabletext"><?=$reference_str?></td>
				</tr>
				<tr>
					<td class="tabletextremark" colspan="2"><?=$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField']?></td>
				</tr>
				
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
			<?= $linterface->GET_ACTION_BTN($button_continue, "submit") ?>&nbsp;
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='$url'") ?>
		</td>
	</tr>
</table>
<input type="hidden" id="task" name="task"/>
<input type="hidden" id="MeritType" name="MeritType" value="<?=$MeritType?>"/>
</form>
<br />
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
