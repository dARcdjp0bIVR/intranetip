<?
// Using:

#################################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$PeriodID = IntegerSafe($PeriodID);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf"))
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_sub_tag = $ldiscipline->returnVector($sql);
if(sizeof($arr_sub_tag) > 0){
	$sub_tag = $arr_sub_tag[0];
}

if(is_array($PeriodID)) {
	$PeriodID = $PeriodID[0];
}

$sql = "SELECT CONCAT(DateStart,' ".$i_To." ',DateEnd) FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID = '$PeriodID'";
$tmp_periodRange = $ldiscipline->returnVector($sql);
if(sizeof($tmp_periodRange) > 0){
	$PeriodRange = $tmp_periodRange[0];
}

$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType"),array("$PeriodRange",""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step1'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step2'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step3'], 1);

if ($msg != "")
{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('category_period_new3.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=1')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'RuleID[]','category_period_rule_copy.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_copy.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						".$eDiscipline['Setting_Duplicate']."
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'RuleID[]','category_period_rule_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'RuleID[]','category_period_rule_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
# End #	

$sql = "SELECT 
				RuleID, NextNumLate, ProfileMeritNum, ProfileMeritType, 
				ReasonItemID, ConductScore, SubScore1, DetentionMinutes 
		FROM
				DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE
		WHERE 
				CategoryID = '$CategoryID' AND PeriodID = '$PeriodID'
		ORDER BY
				RuleOrder";
$arr_result = $ldiscipline->returnArray($sql,7);

if(sizeof($arr_result) > 0)
{
	$table_content .= "<table border=\"0\" cellpadding=\"5\" cellspacing=\"0\" width=\"100%\" class=\"dotline\">";
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td>#</td>";
	$table_content .= "<td>".$eDiscipline['Setting_AccumulatedTimes']."</td>";
	$table_content .= "<td>".$eDiscipline['Setting_AddDemeritRecord']."</td>";
	if($ldiscipline->Display_ConductMarkInAP) 
	{
		$table_content .= "<td>".$eDiscipline['Conduct_Mark']."</td>";
	}
	
	$table_content .= "<td>".$eDiscipline['Setting_ItemName']."</td>";
	//$table_content .= "<td>$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_DetentionSession_Title</td>";
	##$table_content .= "<td>$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_Reminder_Title</td>";
	$table_content .= "<td><input type=\"checkbox\" onClick=\"(this.checked)?setChecked(1,this.form,'RuleID[]'):setChecked(0,this.form,'RuleID[]')\"></td>";
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($ruleID, $accumulatedCount, $meritNum, $meritItem, $itemName, $conductScore, $studyScore, $detentionSession) = $arr_result[$i];
		
		if($i%2==0)
			$row_css = " CLASS=\"tablerow1\" ";
		else
			$row_css = " CLASS=\"tablerow2\" ";
			
		$row = $i+1;
		$arr_meritItem = $ldiscipline->returnMeritItemByID($itemName);
		$item_name = $arr_meritItem['ItemName'];
		
		$table_content .= "<tr $row_css>";
		$table_content .= "<td>$row	</td>";
		$table_content .= "<td>$accumulatedCount</td>";
		$merit_record = $ldiscipline->returnDeMeritStringWithNumber($meritNum, $ldiscipline->RETURN_MERIT_NAME($meritItem));
		$table_content .= "<td>$merit_record</td>";
		if($ldiscipline->Display_ConductMarkInAP) 
		{
			$table_content .= "<td>". ($conductScore * -1) ."</td>";
		}
		$table_content .= "<td>$item_name</td>";
		##$table_content .= "<td></td>";	## for Reminder
		$table_content .= "<td><input type=\"checkbox\" name=\"RuleID[]\" value=\"$ruleID\"></td>";	
		$table_content .= "</tr>";
	}
	$table_content .= ($msg==1) ? "<tr><td colspan=\"6\" align=\"center\">".$linterface->GET_ACTION_BTN($button_finish, "button", "self.location.href='category.php';")."</td></tr>" : "";
	$table_content .= "<tr><td colspan=\"6\"></td></tr>";
	$table_content .= "</table>";
}
	
//print_r($arr_result);
?>

<form name="form1" action="" method="POST">
<?=$subtags_table;?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr height="5px"><td></td></tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CalculationSchema'];?></td>
		<td><?=$eDiscipline['Setting_CalculationSchema_Floating'];?></td>
	</tr>
	<tr>
		<td colspan="2" align="right"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td colspan="2" align="left"><?=$toolbar?></td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>	
		<td class="tabletextremark"><i><?=$eDiscipline['Setting_CalculationDetails'];?></i></td>
		<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><?=$table_content;?></td></tr>
	<input type="hidden" name="CategoryID" value="<?=$CategoryID?>">
	<input type="hidden" name="PeriodType" value="<?=$PeriodType?>">
	<input type="hidden" name="PeriodID" value="<?=$PeriodID?>">
	<input type="hidden" name="Scheme" value="<?=$Scheme?>">
</table>
<br>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>