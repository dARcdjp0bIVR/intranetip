<?
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$result = $ldiscipline->removeCategoryCalculationSchema($RuleID);

if($result){
	header("Location: category_period_rule.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=$Scheme&msg=3");
	exit();
}else{
	header("Location: category_period_rule.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=$Scheme&msg=13");
	exit();
}


?>