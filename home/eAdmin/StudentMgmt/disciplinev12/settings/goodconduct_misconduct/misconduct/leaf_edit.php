<?
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access") || !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

if(is_array($CategoryID)){
	$category_id = $CategoryID[0];
}else{
	$category_id = $CategoryID;
}

if(!$category_id) {
	header("Location: newleaf.php");	
}

$CurrentPage = "Settings_GoodConductMisconduct";


### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);


$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 0);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 1);


$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$temp = array(array($eDiscipline['Setting_NewLeaf'],"newleaf.php"),array($button_edit,""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

if($category_id==1) {
	$defaultSpecify = 0;	
} else {
	$defaultSpecify = $ldiscipline->retrieveDefaultSpecify($category_id);
}

$periodSet = $ldiscipline->retrievePeriodIDSet($category_id, $defaultSpecify);

$cat_detail = $ldiscipline->retrieveCategoryNameAndType($category_id);
list($showCatName, $schemeType) = $cat_detail;

$result = $ldiscipline->retrieveNewLeafDetail($category_id, $periodSet);

	

$table_content .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' align='center'>";
$table_content .= "<tr class='tabletop'>";
$table_content .= "<td width='40%'>".$iDiscipline['Period']."</td>";
$table_content .= "<td width='30%'>".$eDiscipline['Setting_Days']."</td>";
$table_content .= "<td width='30%'>".$eDiscipline['Setting_WaiveFirst']."</td>";
$table_content .= "<tr>";

for($i=0; $i<sizeof($result); $i++) {
	list($catName, $schemeType, $dateStart, $dateEnd, $waiveDay, $waiveFirst) = $result[$i];
	$waiveDay = ($waiveDay=="") ? 0 : $waiveDay;
	$waiveFirst = ($waiveFirst=="") ? 0 : $waiveFirst;
	$table_content .= "<tr class='tablerow'>";
	$table_content .= "<td width='40%'>".$dateStart." ".$i_To." ".$dateEnd."</td>";
	$table_content .= "<td width='30%'><input type='text' name='day[]' class='formtextbox' value='".$waiveDay."' size='10' onBlur='returnValue(this)'></td>";
	$table_content .= "<td width='30%'><input type='text' name='first[]' class='formtextbox' value='".$waiveFirst."' size='10' onBlur='returnValue(this)'></td>";
	$table_content .= "<tr>";
		
}

if(sizeof($result)==0) {
	
	$table_content .= "<tr>";
	$table_content .= "<td colspan='3' align='center' height='30'>".$i_Discipline['Warning_Need_Setting']."</td>";
	$table_content .= "<tr>";
	
	$button .= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:self.location='newleaf.php'");
	
} else {
	
	$button .= $linterface->GET_ACTION_BTN($button_submit, "submit")."&nbsp;";
	$button .= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"")."&nbsp;";
	$button .= $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:self.location='newleaf.php'","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");	
	
}
$table_content .= "</table>";



//$i_Discipline['Warning_Integer_Input']
?>

<script language="javascript">
function returnValue(obj) {
	if(obj.value=='')	
		obj.value = 0;
}

function checkForm(){
	var obj = document.form1;

	for(var i=0; i<obj.elements.length; i++) {
		if(obj.elements[i].type=='text') {
			if(isNaN(obj.elements[i].value) || obj.elements[i].value<0 || !isInteger(obj.elements[i].value)) {
				alert("<?=$eDiscipline['Setting_Days']." / ".$eDiscipline['Setting_WaiveFirst'].$i_Discipline['Warning_Integer_Input']?>");	
				return false;					
			}
			if(obj.elements[i].name=='first[]')  {
				if(obj.elements[i].value!=0 && obj.elements[i-1].value==0) {
					alert("<?=$i_Discipline['Warning_Input_WaiveFirst']?>");
					return false;
				}
			}
		}
	}
	return true;
}

function isInteger(s){
 var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}
</script>

<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
</table>

<form name="form1" action="leaf_edit_update.php" method="POST" onSubmit="return checkForm();">
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CategoryName']?></td>
		<td class="tabletext" valign="top"><?=$showCatName?></td>
	</tr>
	<!--
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_HeldEach']?></td>
		<td class="tabletext" valign="top">
			<?=$schemeType?>
		</td>
	</tr>
	//-->
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<?=$table_content?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<?=$button?>
		</td>
	</tr>
	<input type="hidden" name="category_id" value="<?=$category_id;?>">
	<input type="hidden" name="periodSetID" value="<?= implode(',',$periodSet) ?>">
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>