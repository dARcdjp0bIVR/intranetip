<?
// Using: Bill

#########################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
# 	Date:	2016-02-26	Bill	[2015-0807-1048-01073]
#			- support copy from existing period setting
#
#########################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
if(isset($PeriodID)) {
    $PeriodID = IntegerSafe($PeriodID);
}

if((isset($StartDate) && !intranet_validateDate($StartDate)) || (isset($EndDate) && !intranet_validateDate($EndDate)))
{
    header("Location: category_period_new.php?msg=14&CategoryID=$CategoryID&PeriodType=".CATEGORY_PERIOD_CONDITION_SPECIFY);
    exit();
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";

### To-do: Add checking of overlapping periods
if(isset($StartDate) && isset($EndDate))
{
	$PeriodSet = $ldiscipline->returnPeriodSetID($CategoryID);
	
	if ($MeritType == 1)
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND RecordType = 1 AND SetID = '$PeriodSet'";
	}
	else
	{
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND (RecordType = -1 OR RecordType IS NULL) AND SetID = '$PeriodSet'";
	}
}
$temp = $ldiscipline->returnVector($sql);
$overlapped = sizeof($temp) > 0 && $temp[0] > 0? 1 : 0;
if($overlapped)
{
	header("Location: category_period_new.php?error=1&CategoryID=$CategoryID&PeriodType=".CATEGORY_PERIOD_CONDITION_SPECIFY."&StartDate=$StartDate&EndDate=$EndDate");
	exit();
}

### Check across different school terms
$sql = "SELECT LEFT(TermEnd,10) FROM ACADEMIC_YEAR_TERM WHERE ('$StartDate' BETWEEN TermStart AND TermEnd)";
$temp = $ldiscipline->returnVector($sql);

$outBound = (sizeof($temp)==0)? 3 : ((sizeof($temp) > 0 && $temp[0] < $EndDate)? 2 : 0);
# outBound==2: across different school terms
# outBound==3: school term not yet defined
//echo $outBound;
if(!isset($PeriodID) && $outBound!=0)
{
	header("Location: category_period_new.php?error=$outBound&StartDate=$StartDate&EndDate=$EndDate");
	exit();
}
### End Checking ###

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")) {
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);
}

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_sub_tag = $ldiscipline->returnVector($sql);
if(sizeof($arr_sub_tag) > 0){
	$sub_tag = $arr_sub_tag[0];
}

$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step1'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step2'], 1);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step3'], 0);

if ($msg != "")
{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #

if(is_array($PeriodID)){
	$PeriodID = $PeriodID[0];
}
else{
	$PeriodID = $PeriodID;
}

$sql = "SELECT DateStart, DateEnd FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID = '$PeriodID'";
$arr_result = $ldiscipline->returnArray($sql,2);
if(sizeof($arr_result) > 0){
	list($StartDate, $EndDate) = $arr_result[0];
}
$PeriodRange = $StartDate." ".$i_To." ".$EndDate;

$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType"),array("$PeriodRange",""), array($eDiscipline['Setting_NAV_NewPeriod'],""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

// [2015-0807-1048-01073]
### Existing Period Drop Down List
// Get existing period settings
$sql = "	SELECT
					DISTINCT a.PeriodID, a.DateStart, a.DateEnd,
					/*
					If(c.RuleID IS NOT NULL, '".$eDiscipline['Setting_CalculationSchema_Floating']."', IF(b.SettingID IS NOT NULL, '".$defined_field_link."', 'No Setting'))
					*/					
					If(c.RuleID IS NOT NULL, 1, IF(b.SettingID IS NOT NULL, 0, 2)) As UpgradeType
			FROM 
					DISCIPLINE_ACCU_PERIOD AS a LEFT OUTER JOIN 
					DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING AS b ON (a.PeriodID = b.PeriodID AND b.CategoryID = '$CategoryID') LEFT OUTER JOIN 
					DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE AS c ON (c.CategoryID = '$CategoryID' AND c.PeriodID = a.PeriodID ) 
			WHERE
					a.RecordType = '".MISCONDUCT."' AND
					a.SetID = 0 AND 
					(c.RuleID IS NOT NULL OR b.SettingID IS NOT NULL)";
$periodSettings = $ldiscipline->returnArray($sql,2);
$periodSettings = BuildMultiKeyAssoc($periodSettings, array("UpgradeType", "PeriodID"), array(), 0, $BuildNumericArray=1);

// Build Drop Down List for Static Increment Scheme
$staticSettings = $periodSettings[0];
if(count($staticSettings) > 0)
{
	$staticSettingsList = "<select id='staticPeriodList' name='staticPeriodList'>";
	$staticSettingsList .= "<option value='0'>".$Lang['Btn']['NotSet']."</option>";
	foreach ($staticSettings as $settingInfo)
	{
		$settingInfo = $settingInfo[0];
		$staticSettingsList .= "<option value='".$settingInfo["PeriodID"]."'>".$settingInfo["DateStart"]." $i_To ".$settingInfo["DateEnd"]."</option>";
	}
	$staticSettingsList .= "</select>";
}
else
{
	$staticSettingsList = "---";
}

// Build Drop Down List for Floating Increment Scheme
$floatSettings = $periodSettings[1];
if(count($floatSettings) > 0)
{
	$floatSettingsList = "<select id='floatSettingsList' name='floatSettingsList'>";
	$floatSettingsList .= "<option value='0'>".$Lang['Btn']['NotSet']."</option>";
	foreach ($floatSettings as $settingInfo)
	{
		$settingInfo = $settingInfo[0];
		$floatSettingsList .= "<option value='".$settingInfo["PeriodID"]."'>".$settingInfo["DateStart"]." $i_To ".$settingInfo["DateEnd"]."</option>";
	}
	$floatSettingsList .= "</select>";
}
else
{
	$floatSettingsList = "---";
}
?>
<script language="javascript">

<?php if($NewPeriodRange==""){ ?>
$(document).ready(function() {
	var SchemeValue = document.form1.Scheme.value;
	if(SchemeValue == 0 || SchemeValue == 1){
    	changeSetting(SchemeValue);
	}
});
<?php } ?>

function changeSetting(val){
	<?php if($NewPeriodRange==""){ ?>
	// show static list
	if(val == 0){
		document.getElementById('staticDiv').style.display="";
		document.getElementById('floatingDiv').style.display="none";
	}
	// show floating list
	else if(val == 1){
		document.getElementById('staticDiv').style.display="none";
		document.getElementById('floatingDiv').style.display="";
	}
	<?php } ?>
}

function checkForm(obj){
	//var schemeType = obj.Scheme.value;
	var schemeType = $('input:radio[name=Scheme]:checked').val();
	
	// selected valid static period
	if(schemeType == 0 && obj.staticPeriodList){
		if(obj.staticPeriodList.value!=0){
			// copy settings if confirm
			if(confirm('<?=$Lang['eDiscipline']['ConfirmToCopyPeriodSetting']?>')){
				obj.action = "category_period_existing_copy.php";
			}
			else{
				return false;
			}
		}
	}
	// selected valid floating period
	else if(schemeType == 1 && obj.floatSettingsList){
		if(obj.floatSettingsList.value!=0){
			// copy settings if confirm
			if(confirm('<?=$Lang['eDiscipline']['ConfirmToCopyPeriodSetting']?>')){
				obj.action = "category_period_existing_copy.php";
			}
			else{
				return false;
			}
		}
	}
	else{
		obj.action = "category_period_new3.php";
	}
	
	return true;
}

</script>

<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>
<form name="form1" action="category_period_new3.php" method="POST" onsubmit="return checkForm(this);">
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Period'];?></td>
		<td class="tabletext" valign="top"><?=$PeriodRange;?></td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_ConversionScheme'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<input type="radio" name="Scheme" value="0" id="Scheme0" onclick="changeSetting(0)" CHECKED><label for="Scheme0"><?=$eDiscipline['Setting_CalculationSchema_Static'];?></label>
			<input type="radio" name="Scheme" value="1" id="Scheme1" onclick="changeSetting(1)"><label for="Scheme1"><?=$eDiscipline['Setting_CalculationSchema_Floating'];?></label>
		</td>
	</tr>
	<?php if($NewPeriodRange==""){ ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$Lang['eDiscipline']['CopyGMPeriodSetting']?><span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<div id='staticDiv'><?=$staticSettingsList?></div>
			<div id='floatingDiv' style='display:none'><?=$floatSettingsList?></div>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_continue, "submit"); ?>
			<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location.href='category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType'","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
		</td>
	</tr>
	<input type="hidden" name="NewPeriodRange" value="<?=$NewPeriodRange?>">
	<input type="hidden" name="CategoryID" value="<?=$CategoryID;?>">
	<input type="hidden" name="PeriodType" value="<?=$PeriodType;?>">
	<input type="hidden" name="PeriodID" value="<?=$PeriodID;?>">
	<input type="hidden" name="StartDate" value="<?=$StartDate;?>">
	<input type="hidden" name="EndDate" value="<?=$EndDate;?>">
	<input type="hidden" name="TargetSemester" value="<?=$semester;?>">
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>