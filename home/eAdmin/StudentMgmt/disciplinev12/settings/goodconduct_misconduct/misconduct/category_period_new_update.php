<?
# using: Bill

#################################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#	Date:	2017-09-21	(Bill)	[2017-0710-1151-50054]
#			update settings - Email to PIC / Class Teacher / eDiscipline Administrator
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$PeriodID = IntegerSafe($PeriodID);

if(!intranet_validateDate($StartDate)) {
    $StartDate = '';
}
if(!intranet_validateDate($EndDate)) {
    $EndDate = '';
}
$TargetSemester = intranet_htmlspecialchars($TargetSemester);

$accumlativeTimes = IntegerSafe($accumlativeTimes);
$meritType = IntegerSafe($meritType);
$meritNum = IntegerSafe($meritNum);
$meritItemID = IntegerSafe($meritItemID);
if($conductMark == ""){
    $conductMark = IntegerSafe($conductMark);
}
if($studyMark == ""){
    $studyMark = IntegerSafe($studyMark);
}

$SelectNotice0 = IntegerSafe($SelectNotice0);
$sendNoticeAction = IntegerSafe($sendNoticeAction);
$SendEmailToPIC = IntegerSafe($SendEmailToPIC);
$SendEmailToClassTeacher = IntegerSafe($SendEmailToClassTeacher);
$SendEmailToAdmin = IntegerSafe($SendEmailToAdmin);

if($waiveDay != 0 || $waiveDay != '') {
    $waiveDay = IntegerSafe($waiveDay);
}
if($waiveFirst != 0 || $waiveFirst != '') {
    $waiveFirst = IntegerSafe($waiveFirst);
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

## Static Increment
if($Scheme == CALCULATION_SCHEME_STATIC)
{
	$result = $ldiscipline->insertCategoryPeriodCalculationScheme($NewPeriodRange, $StartDate, $EndDate, $TargetSemester, CALCULATION_SCHEME_STATIC, $CategoryID, $PeriodID, $accumlativeTimes, $meritType, $meritNum, $meritItemID, $conductMark, $studyMark, MISCONDUCT, $waiveDay, $waiveFirst, $updateNewLeaf, $SelectNotice0, $sendNoticeAction, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin);
	if($result){
		header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=1");
		exit();
	}
	else{
		header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=12");
		exit();
	}
	intranet_closedb();
}
## Floating Increment
else
{
	$result = $ldiscipline->insertCategoryPeriodCalculationScheme($NewPeriodRange, $StartDate, $EndDate, $TargetSemester, CALCULATION_SCHEME_FLOATING, $CategoryID, $PeriodID, $accumlativeTimes, $meritType, $meritNum, $meritItemID, $conductMark, $studyMark, MISCONDUCT, $waiveDay, $waiveFirst, $updateNewLeaf, $SelectNotice0, $sendNoticeAction, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin);
	
	if($NewPeriodRange == 1)
	{
		$sql = "SELECT SetID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD WHERE CategoryID = '$CategoryID'";
		$tmp_setID = $ldiscipline->returnVector($sql);
		$SetID = $tmp_setID[0];
		
		if($TargetSemester == ""){
			$TargetSemester = "NULL";
		}
		
		## $sql = "SELECT PeriodID FROM DISCIPLINE_ACCU_PERIOD WHERE SetID = '$SetID'";
		$sql = "SELECT PeriodID FROM DISCIPLINE_ACCU_PERIOD WHERE SetID = '$SetID' AND DateStart = '$StartDate' AND DateEnd = '$EndDate'";
		$tmp_PeriodID = $ldiscipline->returnVector($sql);
		$PeriodID = $tmp_PeriodID[0];
	}
	
	if($result){
		// header("Location: category_period_rule_summary.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=$Scheme");
		header("Location: category_period_rule.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=$Scheme&msg=1");
		exit();
	}
	else{
		header("Location: category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&msg=12");
		exit();
	}
	intranet_closedb();
}
?>