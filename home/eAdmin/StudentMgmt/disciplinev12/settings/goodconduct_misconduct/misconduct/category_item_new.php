<?
# using: 

############## Change Log
#
#	2019-05-13 Bill
#		- Prevent SQL Injection
#
#	2014-06-27 Carlos
#		- $sys_custom['eDiscipline']['PooiToMiddleSchool'] hide Accumulation Period
#
#	2010-08-20 YatWoon
#		- change to IP25 standard
#		- add ling leung yi man customization: follow-up action ($sys_custom['eDisciplinev12_FollowUp'])
#
#####################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
if(!$sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
	if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")) {
		$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);
	}
}
$subtags_table = $linterface->GET_SUBTAGS($SUBTAGS_OBJ);

### Navigation ###
$CategoryID = IntegerSafe($CategoryID);
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_sub_tag = $ldiscipline->returnVector($sql);
if(sizeof($arr_sub_tag) > 0){
	$sub_tag = $arr_sub_tag[0];
}

$PAGE_NAVIGATION = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_item.php?CategoryID=$CategoryID"),array($eDiscipline['Setting_NAV_NewCategoryItem'],""));
$infobar1 = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

// if ($msg != "") {
// 	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
// 	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
// 	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
// 	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
// 	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
// 	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
// }
# End #

$CatItem = $ldiscipline->getAllCategoryItem();
$js = "var ItemCode = new Array();\n";
for($a=0;$a<sizeof($CatItem);$a++)
{
	$js .="ItemCode[".$a."] = '".addslashes($CatItem[$a]['ItemCode'])."';\n";	
}
?>

<script language="javascript">
<?=$js?>
function checkForm()
{
	var obj = document.form1;
	
	if(check_text(obj.ItemName,"<?=$eDiscipline['Setting_JSWarning_CategoryItemNameEmpty'];?>") &&
	check_text(obj.ItemCode,"<?=$eDiscipline['Setting_JSWarning_CategoryItemCodeEmpty'];?>")){
		
		for(a=0;a<ItemCode.length;a++)
		{
			if(obj.ItemCode.value==ItemCode[a])
			{
				alert("<?=$iDiscipline['Category_Item_Code_Warning']?>");
				return false;
			}
		}
		
		return true;
	}
	
	return false;
	
}
</script>

<?=$subtags_table;?>
<?=$infobar1?>


<form name="form1" action="category_item_new_update.php" method="POST" onSubmit="return checkForm();">
<div class="table_board">
	<table class="form_table_v30">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span><?=$eDiscipline['Setting_ItemName'];?></td>
		<td class="tabletext" valign="top"><input class="textboxtext" type="text" name="ItemName" maxlength="220" value=""></td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span><?=$i_Discipline_System_ItemCode;?></td>
		<td class="tabletext" valign="top"><input class="formtextbox" type="text" name="ItemCode" maxlength="80" value=""></td> 
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span><?=$eDiscipline['Setting_Status'];?></td>
		<td class="tabletext" valign="top">
			<input type="radio" name="Status" id="Status2" value="2" CHECKED><label for="Status2"><?=$eDiscipline['Setting_Status_NonUsing'];?></label>
			<input type="radio" name="Status" id="Status1" value="1"><label for="Status1"><?=$eDiscipline['Setting_Status_Using'];?></label>
		</td>
	</tr>
	
	<? if($sys_custom['eDisciplinev12_FollowUp']) { ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="field_title"><?=$Lang['eDiscipline']['DefaultFollowUp'];?></td>
		<td class="tabletext" valign="top"><?=$ldiscipline->getFollowUpSelection(1);?></td> 
	</tr>
	<? } ?>
	</table>
<?=$linterface->MandatoryField();?>
</div>

<div class="edit_bottom_v30">
	<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
	<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
	<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
</div>
 
<input type="hidden" name="CategoryID" value="<?=$CategoryID;?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>