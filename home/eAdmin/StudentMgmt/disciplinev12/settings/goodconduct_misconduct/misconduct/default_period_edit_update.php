<?
// Using:

#############################################
#
#	Date	:	2019-05-13	(Bill)
#				Prevent SQL Injection
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$period_id = IntegerSafe($period_id);
if(!intranet_validateDate($StartDate) || !intranet_validateDate($EndDate))
{
    header("Location: default_period_edit.php?msg=14&PeriodID=$period_id");
    exit();
}

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

### To-do: Add checking of overlapping periods
if ($meritType == 1)
{
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND RecordType = 1 AND PeriodID NOT IN ('$period_id') AND SetID = 0";
}
else
{
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND RecordType = -1 AND PeriodID NOT IN ('$period_id') AND SetID = 0";
}
$temp = $ldiscipline->returnVector($sql);
$overlapped = sizeof($temp) > 0 && $temp[0] > 0? 1 : 0;
if($overlapped)
{
	header("Location: default_period_edit.php?error=1&StartDate=$StartDate&EndDate=$EndDate&PeriodID=$period_id");
	exit();
}

### Check across different school terms
$sql = "SELECT LEFT(TermEnd,10) FROM ACADEMIC_YEAR_TERM WHERE ('$StartDate' BETWEEN TermStart AND TermEnd)";
$temp = $ldiscipline->returnVector($sql);

# outBound==2: across different school terms
# outBound==3: school term not yet defined
//$outBound = (sizeof($temp)==0) ? 3 : ((sizeof($temp)>0 && $temp[0] < $EndDate) ? 2:0);
$outBound = (sizeof($temp)==0) ? 3 : 0;

$sql = "SELECT COUNT(DISTINCT AcademicYearID) FROM ACADEMIC_YEAR_TERM WHERE ('$StartDate' BETWEEN TermStart AND TermEnd) OR ('$EndDate' BETWEEN TermStart AND TermEnd)";
$result = $ldiscipline->returnVector($sql);
if($result[0] != 1)
{
	$outBound = 4;
}

if($outBound){
	header("Location: default_period_edit.php?error=$outBound&StartDate=$StartDate&EndDate=$EndDate&PeriodID=$period_id");
	exit();
}

# Check any record using this default period
if(($StartDate != $original_start_date || $EndDate != $original_end_date) || (isset($original_semester) && $original_semester != '' && $original_semester != 'NULL'))
{
	$sql = "SELECT DateStart, DateEnd FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID = '$period_id'";
	$result = $ldiscipline->returnArray($sql, 2);
	list($dateStart, $dateEnd) = $result[0];
	/*
	$sql = "SELECT 
				a.CategoryID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD as a 
				LEFT OUTER JOIN DISCIPLINE_ACCU_PERIOD as b ON (a.SetID=b.SetID)
				LEFT OUTER JOIN DISCIPLINE_ACCU_CATEGORY as c ON (c.CategoryID=a.CategoryID)
			WHERE
				b.SetID=0 AND 
				b.PeriodID=$period_id AND
				(a.RecordType=-1 OR a.RecordType IS NULL) AND 
				a.RecordStatus=0 AND 
				a.DateInput IS NOT NULL
			";
	*/
	
	# commented by Henry on 20090828 (only need to count the category id which using "Default Period Setting")
	$sql = "SELECT 
				CategoryID 
			FROM 
				DISCIPLINE_ACCU_CATEGORY_PERIOD
			WHERE
				(RecordType=-1 OR RecordType IS NULL) AND 
				RecordStatus=0 AND
				DateInput IS NOT NULL";
	$tempResult = $ldiscipline->returnVector($sql);
	$CatID = implode(",", $tempResult);
	
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_RECORD WHERE CategoryID IN ($CatID) AND (RecordDate >= '$dateStart' AND RecordDate <= '$dateEnd') ";
	$result = $ldiscipline->returnVector($sql);
	if($result[0] > 0){
		header("Location: default_period.php?msg=15");
		exit();
	}
}

$curr_school_year = getCurrentAcademicYear();
$semester = isset($semester) ? $semester : intranet_htmlspecialchars($original_semester);
if($semester == "") {
	$semester = "NULL";
}
$sql = "UPDATE DISCIPLINE_ACCU_PERIOD SET DateStart = '$StartDate', DateEnd = '$EndDate', TargetYear = '$curr_school_year', TargetSemester = '$semester', DateModified = NOW() WHERE PeriodID = '$period_id'";
$result = $ldiscipline->db_db_query($sql);

intranet_closedb();

if($result){
	header("Location: default_period.php?msg=2");
	exit();
}
else{
	header("Location: default_period.php?msg=14");
	exit();
}
?>