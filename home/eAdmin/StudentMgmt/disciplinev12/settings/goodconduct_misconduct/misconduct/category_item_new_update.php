<?
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

$result = $ldiscipline->insertCategoryItem($CategoryID,$ItemName,$Status,MISCONDUCT,$ItemCode, $ActionID);

intranet_closedb();

if($result){
	header("Location: category_item.php?CategoryID=".$CategoryID."&xmsg=".$Lang['General']['ReturnMessage']['AddSuccess']);
	exit();
}else{
	header("Location: category_item.php?CategoryID=".$CategoryID."&xmsg=".$Lang['eDiscipline']['ReturnMessage']['DuplicateItemName']);
	exit();
}

?>