<?
// Using:

#############################################
#
#	Date	:	2019-05-13	(Bill)
#				Prevent SQL Injection
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!intranet_validateDate($StartDate) || !intranet_validateDate($EndDate))
{
    header("Location: default_period_new.php?msg=14");
    exit();
}

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

### To-do: Add checking of overlapping periods
if ($meritType == 1)
{
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND RecordType = 1 AND SetID = 0";
}
else
{
	$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_PERIOD WHERE DateStart <= '$EndDate' AND DateEnd >= '$StartDate' AND (RecordType = -1 OR RecordType IS NULL) AND SetID = 0";
}
$temp = $ldiscipline->returnVector($sql);
$overlapped = sizeof($temp) > 0 && $temp[0] > 0? 1 : 0;
if($overlapped)
{
	header("Location: default_period_new.php?error=1&StartDate=$StartDate&EndDate=$EndDate");
	exit();
}

### Check across different school terms
$sql = "SELECT LEFT(TermEnd,10) FROM ACADEMIC_YEAR_TERM WHERE ('$StartDate' BETWEEN TermStart AND TermEnd)";
$temp = $ldiscipline->returnVector($sql);

# outBound==2: across different school terms
# outBound==3: school term not yet defined
//$outBound = (sizeof($temp)==0) ? 3 : ((sizeof($temp)>0 && $temp[0] < $EndDate) ? 2:0);		# cannot set period over 2 terms
$outBound = (sizeof($temp)==0) ? 3 : 0;

$sql = "SELECT COUNT(DISTINCT AcademicYearID) FROM ACADEMIC_YEAR_TERM WHERE ('$StartDate' BETWEEN TermStart AND TermEnd) OR ('$EndDate' BETWEEN TermStart AND TermEnd)";
$result = $ldiscipline->returnVector($sql);
if($result[0] != 1)
{
	$outBound = 4;
}

if($outBound){
	header("Location: default_period_new.php?error=$outBound&StartDate=$StartDate&EndDate=$EndDate");
	exit();
}

$curr_school_year = getCurrentAcademicYear();
if($semester == "") {
	$semester = "NULL";
}

$sql = "INSERT INTO DISCIPLINE_ACCU_PERIOD (DateStart,DateEnd,TargetYear,TargetSemester,RecordType,DateInput,DateModified) VALUES ('$StartDate','$EndDate','$curr_school_year','$semester','-1',NOW(),NOW())";
$result = $ldiscipline->db_db_query($sql);
if($result){
	header("Location: default_period.php?msg=1");
	exit();
}
else{
	header("Location: default_period.php?msg=12");
	exit();
}
?>