<?
// Using : 

########## Change Log ###############
#
#	Date	:	2015-04-10 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	add auto select detention and eNotice option
#
#	2014-06-26 (Carlos): $sys_custom['eDiscipline']['PooiToMiddleSchool'] - hide period type
#
#	Date	:	2010-06-10 (Henry)
#	Detail 	:	can choose default eNtotice Template
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";


### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);


$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
if(!$sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
	if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf"))
		$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);
}
						
$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

# eNotice Template
$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		# check the Template Catgory has template or not
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp))
			$catArr[] = array($Key,$Key);
	}
}
$templateCat = $linterface->GET_SELECTION_BOX($catArr, 'id="TemplateCategoryID", name="TemplateCategoryID" onChange="changeCat(this.value)"', "", $TemplateCategoryID);

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	for ($j=0; $j<=sizeof($result); $j++) {
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
			//$js .= "obj.options[$j] = new Option('-- $button_select --',0);\n";
		} else {
			$tempTemplate = $result[$j-1][1];
			$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
	        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
	        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
	        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);

			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
			//$js .= "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
		}
	}
}

### Navigation ###
$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array($eDiscipline['Setting_NAV_NewCategory'],""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 


if ($msg != "") {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #				
?>

<script language="javascript">
<?=$jTemplateString?>

function checkForm()
{
	var obj = document.form1;
	var LateMinCheck = 1;
	<? if($sys_custom['Discipline_SeriousLate']) { ?>
		if(obj.SeriousLate[0].checked) {
			if(check_positive_int(obj.LateMinutes,"Invalid Mins",0,0)){
				LateMinCheck = 1;
			}else{
				LateMinCheck = 0;
			}
		}
	<? } ?>
	
	if(check_text(obj.CategoryName,"<?=$eDiscipline['Setting_Warning_InputCategoryName'];?>")){
		if(LateMinCheck == 1){
			return true;
		}
	}
	
	return false;
}

function showLateMinsDiv(val)
{
	if(val == 1){
		$("#LateMinutesDiv1").show();
		$("#LateMinutesDiv2").show();
		$("#LateMinutesTD").attr("class","formfieldtitle tabletext");
	}else{
		$("#LateMinutesDiv1").hide();
		$("#LateMinutesDiv2").hide();
		$("#LateMinutesTD").attr("class","");
	}
}

function changeCat(cat){

	var x=document.getElementById("SelectNotice");

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}


	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];

		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}

	/****************
	obj = x;
	<?
	
	for ($i=0; $i<sizeof($catArr); $i++) {
		
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);?>
		
		if(cat=="<?=$catArr[$i][0]?>") {
			<?
			for ($j=0; $j<=sizeof($result); $j++) { 
				if($result[$j][0]==0) {
					echo "obj.options[0] = new Option('-- $button_select --',0);\n";	
				} else {
					$tempTemplate = $result[$j-1][1];
					echo "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
				}
			}
			
		?> } <?
	}
	?>
	****************/
}
</script>

<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>
<form name="form1" action="category_new_update.php" method="POST" onSubmit="return checkForm();">
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CategoryName'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><input class="formtextbox" type="text" name="CategoryName" maxlength="80" value="<?=$CatName;?>"></td>
	</tr>
<?php if(!$sys_custom['eDiscipline']['PooiToMiddleSchool']){ ?>	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Period'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<input type="radio" name="Period" value="0" CHECKED><?=$eDiscipline['Setting_Period_Default_Setting'];?>
			<input type="radio" name="Period" value="1"><?=$eDiscipline['Setting_Period_Specify_Setting'];?>
		</td>
	</tr>
<? } ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Status'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<input type="radio" name="Status" value="2" CHECKED><?=$eDiscipline['Setting_Status_Drafted'];?>
			<input type="radio" name="Status" value="1"><?=$eDiscipline['Setting_Status_Published'];?>
		</td>
	</tr>
							
	<!-- Default Set Detention -->
	<tr valign="top">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['DefaultSetDetention']?></td>
		<td class="tabletext">
			<input type="radio" name="DefaultDetention" value="0" id='DefaultDetention0' CHECKED><label for="DefaultDetention0"><?=$Lang['General']['No'];?></label>
			<input type="radio" name="DefaultDetention" value="1" id='DefaultDetention1'><label for="DefaultDetention1"><?=$Lang['General']['Yes'];?></label>
		</td>
	</tr>
							
	<!-- Default Set Send eNotice -->
	<tr valign="top">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['DefaultSendNotice']?></td>
		<td class="tabletext">
			<input type="radio" name="DefaultSendNotice" value="0" id='DefaultSendNotice0' CHECKED><label for="DefaultSendNotice0"><?=$Lang['General']['No'];?></label>
			<input type="radio" name="DefaultSendNotice" value="1" id='DefaultSendNotice1'><label for="DefaultSendNotice1"><?=$Lang['General']['Yes'];?></label>
		</td>
	</tr>
	
	<? if($sys_custom['Discipline_SeriousLate']) { ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"> <?=$Lang['eDiscipline']['FieldTitle']['SeriousLate']?><span class="tabletextrequire">*</span></td>
		<td>
			<input type="radio" name="SeriousLate" id="SeriousLate" value="1" onClick="showLateMinsDiv(this.value)"> <?=$Lang['General']['Yes']?>
			<input type="radio" name="SeriousLate" id="SeriousLate" value="2" onClick="showLateMinsDiv(this.value)" CHECKED> <?=$Lang['General']['No']?>
		</td>
	</tr>
	<tr>
		<td id="LateMinutesTD" valign="top" width="25%" nowrap="nowrap" >
			<div id="LateMinutesDiv1" style="display:none;">
				<?=$Lang['eDiscipline']['FieldTitle']['LateMinutes']?><span class="tabletextrequire">*</span>
			</div>
		</td>
		<td>
			<div id="LateMinutesDiv2" style="display:none;">
				<input type="text" name="LateMinutes" id="LateMinutes">
			</div>
		</td>
	</tr>
	<? } ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['eNoticeTemplate']?></td>
		<td class="tabletext" valign="top">
			<?=$templateCat?>
			<select name="SelectNotice" id="SelectNotice"><option value="0">-- <?=$button_select?> --</option></select>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
			<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
		</td>
	</tr>

</table>
</form>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>