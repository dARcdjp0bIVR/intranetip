<?
# using: Bill

########## Change Log ###############
#
#	Date	:	2017-10-11 (Bill)	[DM#3265]
#	Details :	fixed Session Problem in PHP 5.4
#
#	2016-08-25	Bill	[2016-0824-0934-33235]		(not applied)
#		- apply LateMinutes checking only if $sys_custom['Discipline_SeriousLate'] = true
#
#	2016-06-21 Anna
#	add searchbox in table 
#
#	2014-06-26 (Carlos): $sys_custom['eDiscipline']['PooiToMiddleSchool'] - Hide Import category and items, hide period setting
#
#	Date	:	2010-06-11 (Henry)
#	Detail 	:	import of Category/Item
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_category_page_number!=$pageNo && $pageNo!="")
{
        setcookie("ck_category_page_number", $pageNo, 0, "", "", 0);
        $ck_category_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_category_page_number!="")
{
        $pageNo = $ck_category_page_number;
}
if ($ck_category_page_order!=$order && $order!="")
{
        setcookie("ck_category_page_order", $order, 0, "", "", 0);
        $ck_category_page_order = $order;
}
else if (!isset($order) && $ck_category_page_order!="")
{
        $order = $ck_category_page_order;
}
if ($ck_category_page_field!=$field && $field!="")
{
        setcookie("ck_category_page_field", $field, 0, "", "", 0);
        $ck_category_page_field = $field;
}
else if (!isset($field) && $ck_category_page_field!="")
{
        $field = $ck_category_page_field;
}

if (!isset($meritType) || $meritType == "")
{
	$meritType = -1;	
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# Preset Category
$ldiscipline->presetAccumulativeCategory();

$keyword = standardizeFormPostValue($_POST['keyword']);

$CurrentPageArr['eDisciplinev12'] = 1;
$CurrentPage = "Settings_GoodConductMisconduct";

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
if(!$sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
	if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf"))
		$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);
}

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('category_new.php')","","","","",0);
if(!$sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('../import.php?MeritType=-1')",$Lang['eDiscipline']['ImportCategory'],"","","",0);
	$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('../importItem.php?MeritType=-1')",$Lang['eDiscipline']['ImportItem'],"","","",0);
}

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'CategoryID[]','category_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'CategoryID[]','category_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";

# Generate System Message #
if($error == 1){
	$SysMsg = $SysMsg = $linterface->GET_SYS_MSG("",$eDiscipline['Accumulative_Period_InUse_Warning']);
}
else{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #

# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;
if (!isset($field)) $field = 0;
if (!isset($order)) $order = 1;
//switch ($field){
//        case 0: $field = 0; break;
//        case 1: $field = 1; break;
//        default: $field = 1; break;
//}
$order = ($order == 1) ? 1 : 0;

if(!isset($Status)){
	$Status = 0;
}
if($Status == 0){
	$cond .= " (a.MeritType = ".MISCONDUCT." OR a.MeritType IS NULL) ";
}
else{
	$cond .= " (a.MeritType = ".MISCONDUCT." OR a.MeritType IS NULL) AND a.RecordStatus = $Status ";
}
if ($keyword != '') {
	$Keyword_cond = " and (a.Name LIKE '%".$ldiscipline->Get_Safe_Sql_Like_Query(intranet_htmlspecialchars($keyword))."%')" ;
}

// [2016-0824-0934-33235] only apply LateMinutes checking when $sys_custom['Discipline_SeriousLate'] = true
//$CatItemDisplaySQL = "CONCAT('<a class=\"tablelink\" href=\"category_item.php?CategoryID=',a.CategoryID,'\">',COUNT(b.Name),'</a>')";
//if($sys_custom['Discipline_SeriousLate'])
//$CatItemDisplaySQL = "IF(a.LateMinutes IS NOT NULL,' - ',CONCAT('<a class=\"tablelink\" href=\"category_item.php?CategoryID=',a.CategoryID,'\">',COUNT(b.Name),'</a>'))";

$sql  = "SELECT 
				IF(a.CategoryID = 1 OR a.CategoryID = 2 OR a.CategoryID = 3 ,CONCAT('<a class=\"tablelink\" href=\"category_edit.php?CategoryID=',a.CategoryID,'\">',a.Name,'</a><span class=\"tabletextrequire\">*</span>'),CONCAT('<a class=\"tablelink\" href=\"category_edit.php?CategoryID=',a.CategoryID,'\">',".intranet_undo_htmlspecialchars('a.Name').",'</a>')),
				IF(a.CategoryID = 1, ' - ', IF(a.LateMinutes IS NOT NULL,' - ',CONCAT('<a class=\"tablelink\" href=\"category_item.php?CategoryID=',a.CategoryID,'\">',COUNT(b.Name),'</a>'))) as NumOfItem,
				CONCAT('<a class=\"tablelink\" href=\"category_period.php?CategoryID=',a.CategoryID,'&PeriodType=',IF(c.RecordStatus = '' OR c.RecordStatus IS NULL,0,c.RecordStatus),'\" >',IF(c.RecordStatus = 1,'".intranet_htmlspecialchars($eDiscipline[Setting_Period_Use_Specify_Periods])."',IF(c.RecordStatus = 0,'".intranet_htmlspecialchars($eDiscipline[Setting_Period_Use_Default_Periods])."','".intranet_htmlspecialchars($eDiscipline[Setting_Period_Use_Default_Periods])."')),'</a>'),
				IF(a.RecordStatus = 1,'".$eDiscipline['Setting_Status_Published']."',IF(a.RecordStatus = 2,'".$eDiscipline['Setting_Status_Drafted']."',' -- ')),
				CONCAT('<input type=\"checkbox\" name=\"CategoryID[]\" value=\"',a.CategoryID,'\">'),
				a.CategoryID,
				a.RecordStatus
		 FROM
		 		DISCIPLINE_ACCU_CATEGORY AS a LEFT OUTER JOIN
		 		DISCIPLINE_ACCU_CATEGORY_ITEM AS b ON (a.CategoryID = b.CategoryID AND b.RecordStatus != ". DISCIPLINE_DELETED_RECORD .") LEFT OUTER JOIN
		 		DISCIPLINE_ACCU_CATEGORY_PERIOD AS c ON (a.CategoryID = c.CategoryID)
		 WHERE
		 		$cond
		 		$Keyword_cond
		 GROUP BY
		 		a.CategoryID ";

# DB Table
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("a.Name","c.RecordStatus","a.RecordStatus");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+3;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0);
$li->wrap_array = array(0,0,0,0);
//$li->IsColOff = 2;
$li->IsColOff = "eDisciplineGMMisCatSetting";
//echo $li->built_sql();

// Table Column
$pos = 0;
$width_percent = array();
$width_percent[0] = '30%';
$width_percent[1] = '20%';
$width_percent[2] = '40%';
$width_percent[3] = '30%';
if($sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$width_percent[0] = '50%';
	$width_percent[1] = '20%';
	$width_percent[2] = '0%';
	$width_percent[3] = '30%';
}

$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='".$width_percent[0]."' >".$li->column($pos++, $eDiscipline['Setting_CategoryName'])."</td>\n";
$li->column_list .= "<td width='".$width_percent[1]."' >".$eDiscipline['Setting_NumOfItem']."</td>\n";
if(!$sys_custom['eDiscipline']['PooiToMiddleSchool']){
	$li->column_list .= "<td width='".$width_percent[2]."' >".$li->column($pos++, $eDiscipline['Setting_Period'])."</td>\n";
}
$li->column_list .= "<td width='".$width_percent[3]."' >".$li->column($pos++, $eDiscipline['Setting_Status'])."</td>\n";
$li->column_list .= "<td width='1'>".$li->check("CategoryID[]")."</td>\n";

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);
?>

<script language="javascript">
<!--
function checkRemove(obj,element,page)
{
	var rejectRemove = 0;
	if(countChecked(obj,element)==0) {
		alert(globalAlertMsg2);
	}
	else{
		var val = returnChecked(obj,element);
		for(i=0; i<obj.elements.length; i++){
			if (obj.elements[i].name==element && obj.elements[i].checked)
			{
				if(obj.elements[i].value == 1 || obj.elements[i].value == 2 || obj.elements[i].value == 3){
	            	rejectRemove = rejectRemove + 1;
            	}
			}
		}
		if(rejectRemove > 0){
			alert("<?=$eDiscipline['Setting_JSWarning_RemoveCategory'];?>");
		}
		else{
    		if(confirm(globalAlertMsg3)){	            
				obj.action=page;
				obj.method="POST";
				obj.submit();
			}
		}
	}
}
-->
</script>

<form name="form1" action="" method="POST">
<?=$subtags_table;?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
		<td><?=$htmlAry['searchBox']?></td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td>
			<?
				$arr_status = array(array(0,$eDiscipline['Setting_Status_All']),array(1,$eDiscipline['Setting_Status_Published']),array(2,$eDiscipline['Setting_Status_Drafted']));
				$status_selection = getSelectByArray($arr_status," name=\"Status\" onChange=\"document.form1.submit();\" ", $Status=($Status==""?0:$Status),0,1);
				echo $status_selection;
			?>
		</td>
		<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><?= $li->display() ?></td></tr>
</table>

<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>