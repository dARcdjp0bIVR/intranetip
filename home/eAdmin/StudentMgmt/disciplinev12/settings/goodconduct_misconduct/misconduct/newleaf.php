<?
// Modifying by : 

########## Change Log ###############
#
#	Date	:	2017-10-13 (Bill)	[DM#3273]
#	Details :	fixed Session Problem in PHP 5.4
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

# Preserve Table View
if ($ck_category_page_number!=$pageNo && $pageNo!="")
{
        setcookie("ck_category_page_number", $pageNo, 0, "", "", 0);
        $ck_category_page_number = $pageNo;
}
else if (!isset($pageNo) && $ck_category_page_number!="")
{
        $pageNo = $ck_category_page_number;
}

if ($ck_category_page_order!=$order && $order!="")
{
        setcookie("ck_category_page_order", $order, 0, "", "", 0);
        $ck_category_page_order = $order;
}
else if (!isset($order) && $ck_category_page_order!="")
{
        $order = $ck_category_page_order;
}

if ($ck_category_page_field!=$field && $field!="")
{
        setcookie("ck_category_page_field", $field, 0, "", "", 0);
        $ck_category_page_field = $field;
}
else if (!isset($field) && $ck_category_page_field!="")
{
        $field = $ck_category_page_field;
}

if (!isset($meritType) || $meritType == "")
{
	$meritType = -1;	
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access") || !$ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

# Preset
$ldiscipline->presetNewLeafSetting();

$CurrentPage = "Settings_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 0);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 1);

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

//$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('leaf_new.php')");
/*
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'CategoryID[]','leaf_remove.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
*/
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'CategoryID[]','leaf_edit.php')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
				
# Generate System Message #
if($error == 1){
	$SysMsg = $SysMsg = $linterface->GET_SYS_MSG("",$eDiscipline['Accumulative_Period_InUse_Warning']);
}else{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #				
				
# TABLE SQL
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if (!isset($field)) $field = 0;
if (!isset($order)) $order = 1;
$order = ($order == 1) ? 1 : 0;

$sql = "SELECT 
			b.Name,
			a.CategoryID,
			IF(WaiveDay!='',WaiveDay,'---'),
			IF(WaiveFirst!='',WaiveFirst,'---'),
			CONCAT('<input type=\"checkbox\" name=\"CategoryID[]\" value=\"',a.CategoryID,'\">')
		FROM
			DISCIPLINE_NEW_LEAF_SCHEME AS a LEFT OUTER JOIN
			DISCIPLINE_ACCU_CATEGORY AS b ON (a.CategoryID=b.CategoryID)
		WHERE
			b.MeritType=-1 OR b.MeritType IS NULL
		GROUP BY a.CategoryID
		
		";

$li = new libdbtable2007($field, $order, $pageNo);
//$li->field_array = array("b.Name", "a.SchemeType", "a.CategoryID", "WaiveDay", "WaiveFirst");
$li->field_array = array("b.Name", "a.CategoryID", "WaiveDay", "WaiveFirst");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = $eDiscipline["Record"];
$li->column_array = array(0,0,0,0,0);
$li->wrap_array = array(0,0,0,0,0);
//$li->IsColOff = 2;
$li->IsColOff = "eDisciplineGMMisNewLeafSetting";


// TABLE COLUMN
$pos = 0;

$li->column_list .= "<td width='1' class='tabletoplink'>#</td>\n";
$li->column_list .= "<td width='35%' >".$li->column($pos++, $eDiscipline['Setting_CategoryName'])."</td>\n";
$li->column_list .= "<td width='35%' >".$eDiscipline['Setting_Rehabilitation_Period']."</td>\n";$pos++;
$li->column_list .= "<td width='15%' >".$li->column($pos++, $eDiscipline['Setting_Days'])."</td>\n";
$li->column_list .= "<td width='15%' nowrap>".$li->column($pos++, $eDiscipline['Setting_WaiveFirst'])."</td>\n";
$li->column_list .= "<td width='1'>&nbsp;</td>\n";

?>

<script language="javascript">
<!--
function checkRemove(obj,element,page)
{
	var rejectRemove = 0;
	if(countChecked(obj,element)==0)
		alert(globalAlertMsg2);
	else{
		var val = returnChecked(obj,element);
		for(i=0; i<obj.elements.length; i++){
			if (obj.elements[i].name==element && obj.elements[i].checked)
			{
				if(obj.elements[i].value == 1 || obj.elements[i].value == 2 || obj.elements[i].value == 3){
	            	rejectRemove = rejectRemove + 1;
            	}
			}
		}
		if(rejectRemove > 0){
			alert("<?=$eDiscipline['Setting_JSWarning_RemoveCategory'];?>");
		}else{
    		if(confirm(globalAlertMsg3)){	            
				obj.action=page;
				obj.method="POST";
				obj.submit();
			}
		}
	}
}
-->
</script>

<form name="form1" action="" method="POST">
<?=$subtags_table;?>
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
	</tr>
</table>

<table width="96%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>	
		<td align="right">
			<table border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="21"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_01.gif" width="21" height="23"></td>
					<td background="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_02.gif">
						<table border="0" cellspacing="0" cellpadding="2">
							<tr>
								<?=$table_tool?>
							</tr>
						</table>
					</td>
					<td width="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/table_tool_03.gif" width="6" height="23"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2"><?= $li->display() ?></td></tr>
</table>
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
</form>

<br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>