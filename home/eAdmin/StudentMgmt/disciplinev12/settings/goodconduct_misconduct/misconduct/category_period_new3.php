<?
# using: 

#################################################
#
#   Date:   2019-10-14  (Bill)  [DM#3661]
#           Hide Email to PIC option - except $sys_custom['eDiscipline']['GMConversionStepShowEmailToPIC'] = true
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#	Date:	2017-09-21	(Bill)	[2017-0710-1151-50054]
#			added options - Email to PIC / Class Teacher / eDiscipline Administrator
#
#	Date:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#			support Study Score Limit ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
# 
#	Date:	2013-02-27	YatWoon
#			check with $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] [Case#2013-0225-1322-36071]
#			Need alter tabel change int(11) to float(8,1) for DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE and DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING 
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
if($PeriodID != '') {
    $PeriodID = IntegerSafe($PeriodID);
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 0);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/default_period.php", 0);
if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf")) {
	$SUBTAGS_OBJ[] = array($eDiscipline['Setting_NewLeaf'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/newleaf.php", 0);
}

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_sub_tag = $ldiscipline->returnVector($sql);
if(sizeof($arr_sub_tag) > 0){
	$sub_tag = $arr_sub_tag[0];
}

$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step1'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step2'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step3'], 1);

if ($msg != "")
{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #				

if($StartDate != "" && $EndDate != ""){
	$PeriodRange = $StartDate." ".$i_To." ".$EndDate;
}
else{
	if($PeriodID != "") {
		$sql = "SELECT CONCAT(DateStart,' ".$i_To." ',DateEnd) FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID = '$PeriodID'";
		$tmp_periodRange = $ldiscipline->returnVector($sql);
		if(sizeof($tmp_periodRange) > 0){
			$PeriodRange = $tmp_periodRange[0];
		}
	}
}

if($Scheme != "")
{
	if($Scheme == CALCULATION_SCHEME_STATIC){
		$SchemeTitle = $eDiscipline['Setting_CalculationSchema_Static'];
	}
	if($Scheme == CALCULATION_SCHEME_FLOATING){
		$SchemeTitle = $eDiscipline['Setting_CalculationSchema_Floating'];
	}
}

if($Scheme == CALCULATION_SCHEME_FLOATING AND $PeriodID != "")
{
	$sql = "SELECT 
					NextNumLate, ProfileMeritNum, ProfileMeritType, ConductScore, SubScore1,
					ReasonItemID, DetentionMinutes 
			FROM 
					DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE 
			WHERE 
					CategoryID = '$CategoryID' AND PeriodID = '$PeriodID'
			ORDER BY 
					RuleOrder";
	$arr_existing_result = $ldiscipline->returnArray($sql,5);
	if(sizeof($arr_existing_result) > 0)
	{
		$existing_cal = 1;
		
		$calculation_details .= "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"3\" >";
		$calculation_details .= "<tr class=\"tabletop\">";
		$calculation_details .= "<td>#</td>";
		$calculation_details .= "<td>".$eDiscipline['Setting_AccumulatedTimes']."</td>";
		$calculation_details .= "<td>".$eDiscipline['Setting_AddMeritRecord']."</td>";
		$calculation_details .= "<td>".$eDiscipline['Setting_ItemName']."</td>";
		//$calculation_details .= "<td>$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_DetentionSession_Title</td>";
		##$calculation_details .= "<td>$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_Reminder_Title</td>";
		$calculation_details .= "<td></td>";
		$calculation_details .= "</tr>";
		
		for($i=0; $i<sizeof($arr_existing_result); $i++)
		{
			list($accumulated_count, $meritNum, $meritItem, $conduct_mark, $study_score, $itemName, $detentionSession) = $arr_existing_result[$i];
			
			$arr_meritItem = $ldiscipline->returnMeritItemByID($itemName);
			$item_name = $arr_meritItem['ItemName'];
			$item_cat_id = $ldiscipline->returnCatIDByItemID($itemName);
			
			if($i%2==0)
				$row_css = " CLASS=\"tablerow1\" ";
			else
				$row_css = " CLASS=\"tablerow2\" ";
			
			$row = $i+1;
			$calculation_details .= "<tr $row_css>";
			$calculation_details .= "<td>$row</td>";
			$calculation_details .= "<td>$accumulated_count</td>";
			$calculation_details .= "<td>$meritNum ".$ldiscipline->RETURN_MERIT_NAME($meritItem)."</td>";
			$calculation_details .= "<td>$item_name</td>";
			##$calculation_details .= "<td></td>";		## For Reminder
			//$calculation_details .= "<td><input class=\"formsubbutton\" type=\"button\" name=\"Copy\" value=\"Copy\" onClick=\"javascript:copyCurrentSetting($accumulated_count,$meritNum,$meritItem,$item_cat_id,$conduct_mark,$study_score,$itemName,$detentionSession);\" ></td>";
			if($sys_custom['UseSubScore1']){
				$calculation_details .= "<td>".$linterface->GET_BTN($eDiscipline['Setting_Duplicate'], "button", "javascript:copyCurrentSetting($accumulated_count,$meritNum,$meritItem,$item_cat_id,$conduct_mark,$study_score,$itemName,$detentionSession);", "", "class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"")."</td>";
			}
			else{
				$calculation_details .= "<td>".$linterface->GET_BTN($eDiscipline['Setting_Duplicate'], "button", "javascript:copyCurrentSetting($accumulated_count,$meritNum,$meritItem,$item_cat_id,$conduct_mark,'',$itemName,$detentionSession);", "", "class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"")."</td>";
			}
			$calculation_details .= "</tr>";
		}
		$calculation_details .= "</table>";
	}
}

for($i=1; $i<=$ldiscipline->AccumulativeTimes_MAX; $i++){
	$arr_AccumulativeTimes[] = $i;
}
$accumlative_selection = getSelectByValue($arr_AccumulativeTimes," name=\"accumlativeTimes\" ","",0,1);

$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
for($i=0; $i<=$ldiscipline->ConductMarkIncrement_MAX; $i=$i+$conductMarkInterval){
	$arr_ConductMarkAdjustment[] = round($i,2);
}
$conductmark_selection = getSelectByValue($arr_ConductMarkAdjustment," name=\"conductMark\" ","",0,1);

for($i=0; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++){
	$arr_StudyScoreAdjustment[] = $i;
}
$select_tag = " name=\"studyMark\" ";
if($sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$select_tag .= " onchange=\"checkOverflowStudyScore();\" ";
}
$studymark_selection = getSelectByValue($arr_StudyScoreAdjustment, $select_tag, "", 0, 1);

$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval) {
	$arr_AddMeritRecord[] = $i;
}
$meritrecord_selection = getSelectByValue($arr_AddMeritRecord," name=\"meritNum\" ","",0,1);

$meritType_selection = $ldiscipline->getMeritDemeritTypeSelection(-1, "meritType");

$sql = "SELECT CatID, CategoryName FROM DISCIPLINE_MERIT_ITEM_CATEGORY WHERE (RecordType = '".MISCONDUCT."' OR RecordType IS NULL) AND RecordStatus IS NULL";
$arr_MeritCategory = $ldiscipline->returnArray($sql,2);
$demerit_cats = $ldiscipline->returnMeritItemCategoryByType(-1);
$meritCategory_selection = getSelectByArray($demerit_cats," name=\"meritCategoryID\" onChange=\"changeCat(this.value);\" ","",0,0,$eDiscipline["SelectRecordCategory2"]);

$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();

for($i=1; $i<=$ldiscipline->ReminderLevel_MAX; $i++){
	$arr_ReminderLevel[] = $i;
}
$reminderLevel_selection = getSelectByValue($arr_ReminderLevel," name=\"ReminderLevel\" ","",0,1);

$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("", 1);
if (!$lnotice->disabled && !empty($NoticeTemplateAva))
{
	$enotice_checkbox = '<input name="action_notice" type="checkbox" id="action_notice" value="1" onClick="javascript:if(this.checked){document.getElementById(\'noticeFlag\').value=\'YES\';showDiv(\'divNotice\');}else{document.getElementById(\'noticeFlag\').value=\'NO\';hideDiv(\'divNotice\');}" /><label for="action_notice">'.$eDiscipline["SendNoticeWhenReleased"].'</label><br>';
	$catTmpArr = $ldiscipline->TemplateCategory();
	if(is_array($catTmpArr))
	{
		$catArr[0] = array("0","-- $button_select --");
		foreach($catTmpArr as $Key=>$Value)
		{
			# check the Template Catgory has template or not
			$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("", 1, $Key);
			if(!empty($NoticeTemplateAvaTemp)) {
				$catArr[] = array($Key, $Value);
			}
		}
	}
	$catSelection0 = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID0" name="CategoryID0" onChange="changeTemplate(this.value, 0)"', "", "");
	
	// Preset template items (for javascript)
	for ($i=0; $i<sizeof($catArr); $i++) {
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
		for ($j=0; $j<=sizeof($result); $j++) {
			if ($j==0) {
				$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
				$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
			}
			else {
				$tempTemplate = $result[$j-1][1];
				$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
		        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
		        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
		        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
    
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
			}
		}
	}
}

$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType"),array("$PeriodRange",""),array($eDiscipline['Setting_NAV_NewPeriod'],""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 
?>

<script language="javascript">
<!--
<?=$jTemplateString?>

function changeCat(val)
{
	document.form1.meritItemID.length = 1;		// reset the drop down meun length
	<?
		for($i=0; $i<sizeof($items); $i++){
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
	     	$r_itemName = str_replace("'", "\'", $r_itemName);
	?>
			if(val == <?=$r_catID?>){
				var x = document.form1.meritItemID.length; 
				//var x = 1+<?=$i?>;
				//document.form1.meritItemID[x] = new Option("<?=intranet_htmlspecialchars($r_itemName)?>","<?=intranet_htmlspecialchars($r_itemID)?>");
				document.form1.meritItemID[x] = new Option("<?=$r_itemName?>","<?=intranet_htmlspecialchars($r_itemID)?>");
			}
	<?
		}
	?>
}

function changeItem(value) {
	var i;
	var meritType = document.form1.elements["meritType"];
	var meritNum = document.form1.elements["meritNum"];
	var conductScore = document.form1.elements["conductMark"];
	var studyMark = document.form1.elements["studyMark"];
	
	<? if ($ldiscipline->UseSubScore) { ?>
		var subScore1 = document.form1.elements["studyMark"];
	<? } ?>
	
	<?
	for ($i=0; $i<sizeof($items); $i++)
	{
		list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
	    $r_subscore2 = $items[$i]["SubScore2"];
	    $r_MaxGainDeductSubScore1 = $items[$i]['MaxGainDeductSubScore1'];
	?>
		if (value=="<?=$r_itemID?>")
		{
			if(document.form1.temp_flag.value==1)
			{
				check_meritType = <?=$r_meritType?>;
				check_meritNum = <?=$r_meritNum?>;
				check_conductScore = <?=($r_conduct?$r_conduct:0)?>;
			}
			else
			{
				check_meritType = <?=($r_meritType?$r_meritType:0) ?>;
				check_meritNum = <?=($r_meritNum?$r_meritNum:0)?>;
				check_conductScore = <?=($r_conduct?$r_conduct:0)?>;
			}
			
			// Merit Type
			for (i=0; i<meritType.options.length; i++)
			{
				if (meritType.options[i].value==check_meritType)
				{
					meritType.selectedIndex = i;
					break;
				}
			}
			
			// Merit Num
			for (i=0; i<meritNum.options.length; i++)
			{
				if (meritNum.options[i].value==check_meritNum)
				{
					meritNum.selectedIndex = i;
					break;
				}
			}
			
			// Conduct Score
			for (i=0; i<conductScore.options.length; i++)
			{
				if (conductScore.options[i].value==check_conductScore)
				{
					conductScore.selectedIndex = i;
					break;
				}
			}
			
			// Sub Score
			<? if ($ldiscipline->UseSubScore) { ?>
			for (i=0; i<studyMark.options.length; i++)
			{
				if (studyMark.options[i].value=="<?=$r_subscore1?>")
				{
					studyMark.selectedIndex = i;
					break;
				}
			}
			<? } ?>
         	
	       	<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
	        	if(document.form1.maxStudyScore) {
	        		document.form1.maxStudyScore.value = "<?=$r_MaxGainDeductSubScore1?>";
	        		$('#MaxStudyScoreWarnDiv').html('<?=str_replace("<!--MAX_SCORE-->", $r_MaxGainDeductSubScore1, $Lang['eDiscipline']['StudentExceedMeritItemMaxStudyScore'])?>');
	        		$('#MaxStudyScoreWarnDiv').hide();
	        		resetOverflowStudyScoreDisable();
	        	}
	       	<? } ?>
		}
	<?
	}
	?>
}

function changeTemplate(cat, selectIdx){
	var x = document.getElementById("SelectNotice"+selectIdx);
	if(x) {
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}
	
	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		tempText = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];
		tempValue = eval("jArrayTemplate"+cat+"Value")[j];
		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}
}

function copyCurrentSetting(accumulated_count,meritNum,meritItem,item_cat_id,conduct_mark,study_score,itemName,detentionSession)
{
	<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
		// for Study Score Limit
		changeItem(itemName);
	<? } ?>
	
	var obj = document.form1;
	
	for(i=0; i<obj.accumlativeTimes.length; i++){
		if(obj.accumlativeTimes.options[i].value == accumulated_count){
			obj.accumlativeTimes.options[i].selected = true;
		}
	}
	
	changeCat(item_cat_id);
	
	for(i=0; i<obj.meritCategoryID.length; i++){
		if(obj.meritCategoryID.options[i].value == item_cat_id){
			obj.meritCategoryID.options[i].selected = true;
		}
	}
	
	for(i=0; i<obj.meritItemID.length; i++){
		if(obj.meritItemID.options[i].value == itemName){
			obj.meritItemID.options[i].selected = true;
		}
	}
	
	for(i=0; i<obj.conductMark.length; i++){
		if(obj.conductMark.options[i].value == conduct_mark){
			obj.conductMark.options[i].selected = true;
		}
	}
	
	<? if($sys_custom['UseSubScore1']){ ?>
	for(i=0; i<obj.studyMark.length; i++){
		if(obj.studyMark.options[i].value == study_score){
			obj.studyMark.options[i].selected = true;
		}
	}
	<?}?>
	
	for(i=0; i<obj.meritNum.length; i++){
		if(obj.meritNum.options[i].value == meritNum){
			obj.meritNum.options[i].selected = true;
		}
	}
	
	for(i=0; i<obj.meritType.length; i++){
		if(obj.meritType.options[i].value == meritItem){
			obj.meritType.options[i].selected = true;
		}
	}
}

function returnValue(obj) {
	if(obj.value=='')	
		obj.value = 0;
}

function checkForm()
{
	var obj = document.form1;
	var category_check = 0;
	var item_check = 0;
	var merit_type_check = 0;
	
	for(i=0; i<obj.meritCategoryID.length; i++){
		if(obj.meritCategoryID.options[i].selected == true && i != 0){
			category_check = 1;
		}
	}
	for(i=0; i<obj.meritItemID.length; i++){
		if(obj.meritItemID.options[i].selected == true && i != 0){
			item_check = 1;
		}
	}
	for(i=0; i<obj.meritType.length; i++){
		if(obj.meritType.options[i].selected == true){
			merit_type_check = 1;
		}
	}	
	
	/* check the merit type 0 - N.A. */
	if(obj.meritNum.value==0)	obj.meritType.selectedIndex = 0;
	if(obj.meritType.value==-999)	obj.meritNum.selectedIndex = 0;
	
	<? if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") && !$existing_cal) { ?>
	if(!isInteger(obj.waiveDay.value) || !isInteger(obj.waiveFirst.value)) {
		alert("<?=$eDiscipline['Setting_Days']." / ".$eDiscipline['Setting_WaiveFirst'].$i_Discipline['Warning_Integer_Input']?>");
		return false;	
	}
	if(obj.waiveDay.value=='0' && obj.waiveFirst.value!='0') {
		alert("<?=$i_Discipline['Warning_Input_WaiveFirst']?>");
		return false;	
	}
	<? } ?>
	
	if(category_check == 1 && item_check == 1 && merit_type_check == 1){
		obj.action = "category_period_new_update.php";
		return true;
	}
	else{
		if(category_check == 0){
			alert("Please select a category");
			return false;
		}
		if(item_check == 0){
			alert("Please select a item");
			return false;
		}
		if(merit_type_check == 0){
			alert("Please select a merit type");
			return false;
		}
	}
}

function isInteger(s){
 var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function checkNoticeAction(val) {
	if(val==1) {
		document.getElementById('CategoryID0').disabled = false;
		document.getElementById('SelectNotice0').disabled = false;
	}
	else {
		document.getElementById('CategoryID0').disabled = true;
		document.getElementById('SelectNotice0').disabled = true;
	}
}

<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
function checkOverflowStudyScore()
{
	resetOverflowStudyScoreDisable();
	
	// Exceed Max Study Score (Display Warning & Disable Submit Button)
	if(document.form1.studyMark && document.form1.maxStudyScore && parseInt(document.form1.maxStudyScore.value) > 0 && parseInt(document.form1.studyMark.value) > parseInt(document.form1.maxStudyScore.value)) {
		document.form1.studyMark.focus();
		$('#MaxStudyScoreWarnDiv').show();
		$("input[type=submit]", document.form1).attr('disabled', true);
		$("input[type=submit]", document.form1).attr('class', 'formbutton_disable');
	}
}

function resetOverflowStudyScoreDisable()
{
	// Reset
	$('#MaxStudyScoreWarnDiv').hide();
	$("input[type=submit]", document.form1).attr('disabled', false);
	$("input[type=submit]", document.form1).attr('class', 'formbutton');
}
<? } ?>
-->
</script>

<form name="form1" action="" method="POST" onSubmit="return checkForm();">
<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Period']?></td>
		<td class="tabletext" valign="top"><?=$PeriodRange;?></td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CalculationSchema']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=$SchemeTitle;?></td>
	</tr>
	
	<?
		if($Scheme == CALCULATION_SCHEME_FLOATING && $existing_cal==1){
	?>
	<tr height="10px"><td></td></tr>
	<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_CalculationDetails']?></i></td></tr>
	<tr height="10px"><td></td></tr>
	<tr>
		<td colspan="2">
			<?=$calculation_details;?>
		</td>
	</tr>
	<?
		}
	?>
	
	<tr height="10px"><td></td></tr>
	<?
	if($Scheme == CALCULATION_SCHEME_FLOATING){
		if($existing_cal==1){
	?>
			<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_FutherCalculationInfo']?></i></td></tr>
	<?
		}
		else{
	?>
			<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_FirstCalculationSchema']?></i></td></tr>
	<?
		}
	}
	else{
	?>
		<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_CalculationInfo']?></i></td></tr>
	<?
	}
	?>
	<tr height="10px"><td></td></tr>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Condition']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<?=($Scheme == CALCULATION_SCHEME_FLOATING) ? (($existing_cal==1)? $i_general_another : $i_general_first) : $i_general_every ?><?=$accumlative_selection;?><?=$i_general_instance_of?><b><u><?=$sub_tag;?></u></b>, <?=$i_general_will_be_count_as?> <br>
			<?=$meritCategory_selection;?> - <?=$meritItem_selection;?>
			<SELECT name="meritItemID" onChange="changeItem(this.value)">
					<OPTION value="0" selected>-- <?=$eDiscipline["SelectRecordItem"]?> --</OPTION>
			</SELECT>
		</td>
	</tr>
	
	<? if(!$ldiscipline->Hidden_ConductMark) {?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_ConductMark_Decrease']?> <span class="tabletextrequire">*</span> <span class="tabletextrequire2">#</span></td>
		<td class="tabletext" valign="top">
			<?=$conductmark_selection;?>
		</td>
	</tr>
	<? } ?>
	
	<? if($sys_custom['UseSubScore1']){ ?>
		<tr>
			<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_StudyMark_Decrease']?> <span class="tabletextrequire">*</span> <span class="tabletextrequire2">#</span></td>
			<td class="tabletext" valign="top">
				<?=$studymark_selection;?>
				<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
					<div id="MaxStudyScoreWarnDiv" style="color:red;"></div>
					<input type="hidden" name="maxStudyScore" value="0" />
		        <? } ?>
			</td>
		</tr>
	<? } ?>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_AddDemeritRecord']?> <span class="tabletextrequire">*</span> <span class="tabletextrequire2">#</span></td>
		<td class="tabletext" valign="top">
			<?=$meritrecord_selection;?><?=$meritType_selection;?>
		</td>
	</tr>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['eNoticeTemplate']?></td>
		<td class="tabletext" valign="top">
			<input type="checkbox" name="sendNoticeAction" id="sendNoticeAction" value="1" <?if($sendNoticeAction) echo "checked";?> onClick="checkNoticeAction(this.checked)"><label for="sendNoticeAction"><?=$Lang['eDiscipline']['SendNoticeActionMsg']?></label><br>
			<?=$catSelection0?><select name="SelectNotice0" id="SelectNotice0"><option value="0"><?="-- $button_select --"?></option></select>
		</td>
	</tr>
	
	<? if($ldiscipline->use_newleaf && $ldiscipline->CHECK_ACCESS("Discipline-MGMT-GoodConduct_Misconduct-NewLeaf") && !$existing_cal) { ?>
	<tr height="10px"><td>&nbsp;</td></tr>
	<tr><td class="tabletextremark"><i>- <?=$eDiscipline['Setting_NewLeaf']?> -</i></td></tr>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Days']?></td>
		<td class="tabletext" valign="top">
			<input type="text" class="formtextbox" name="waiveDay" value="0" size="10" onBlur='returnValue(this)'> 
		</td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_WaiveFirst']?></td>
		<td class="tabletext" valign="top">
			<input type="text" class="formtextbox" name="waiveFirst" value="0" size="10" onBlur='returnValue(this)'> 
		</td>
	</tr>
	<? } ?>
	
	<tr height="10px"><td></td></tr>
	<tr><td class="tabletextremark"><i> - <?=$eDiscipline["Notification"]?> -</i></td></tr>
	<tr height="10px"><td></td></tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline["EmailTo"]?></td>
        <td class="tabletext" valign="top">
            <?php if ($sys_custom['eDiscipline']['GMConversionStepShowEmailToPIC']) { ?>
			    <input name="SendEmailToPIC" type="checkbox" id="SendEmailToPIC" value="1"><label for="SendEmailToPIC"><?=$i_Discipline_PIC?></label>
            <?php } ?>
			<input name="SendEmailToClassTeacher" type="checkbox" id="SendEmailToClassTeacher" value="1"><label for="SendEmailToClassTeacher"><?=$i_Teaching_ClassTeacher?></label>
			<input name="SendEmailToAdmin" type="checkbox" id="SendEmailToAdmin" value="1"><label for="SendEmailToAdmin"><?=$eDiscipline["DisciplineAdmin2"]?></label>
		</td>
	</tr>
	
	<tr height="10px"><td>&nbsp;</td></tr>
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
	</tr>
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><span class="tabletextrequire2">#</span> <?=$eDiscipline['Setting_Warning_PleaseSelectAtLeaseOnePunlishment']?></td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
			<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']? "resetOverflowStudyScoreDisable()" : ""), "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location.href='category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=$Scheme&meritType=-1'","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
		</td>
	</tr>
	
	<input type="hidden" name="NewPeriodRange" value="<?=$NewPeriodRange?>">
	<input type="hidden" name="CategoryID" value="<?=$CategoryID;?>">
	<input type="hidden" name="PeriodType" value="<?=$PeriodType;?>">
	<input type="hidden" name="PeriodID" value="<?=$PeriodID;?>">
	<input type="hidden" name="StartDate" value="<?=$StartDate;?>">
	<input type="hidden" name="EndDate" value="<?=$EndDate;?>">
	<input type="hidden" name="TargetSemester" value="<?=$TargetSemester;?>">
	<input type="hidden" name="Scheme" value="<?=$Scheme;?>">	
	<input type="hidden" name="flag" value="1">
	<input type="hidden" name="updateNewLeaf" value="<?=!$existing_cal?>">	
	<input type="hidden" name="temp_flag" value="0" />
</table>
</form>

<script language="javascript">
checkNoticeAction(0);
</script>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>