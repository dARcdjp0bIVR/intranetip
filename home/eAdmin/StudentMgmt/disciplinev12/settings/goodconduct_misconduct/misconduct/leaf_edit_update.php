<?
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$category_id) {
	header("Location: newleaf.php");	
}

$periodSet = explode(',', $periodSetID);


$result = $ldiscipline->updateNewLeafSetting($category_id, $periodSet, $day, $first);

intranet_closedb();

if($result){
	header("Location: newleaf.php?msg=2");
	exit();
}else{
	header("Location: newleaf.php?msg=14");
	exit();
}

?>