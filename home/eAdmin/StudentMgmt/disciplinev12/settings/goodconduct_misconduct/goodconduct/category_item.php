<?
// Using:

#############################################
#
#	Date	:	2019-05-13	(Bill)
#				Prevent SQL Injection
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if(!isset($CategoryID)) {
	header("category.php");
	intranet_closedb();
	exit();
}

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$targetItemID = IntegerSafe($targetItemID);
if(isset($Status)) {
    $Status = IntegerSafe($Status);
}
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START($xmsg);

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/default_period.php", 0);

$subtags_table = $linterface->GET_SUBTAGS($SUBTAGS_OBJ);

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_cat_name = $ldiscipline->returnVector($sql);
$CatName = $arr_cat_name[0];

$PAGE_NAVIGATION = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$CatName",""),array($eDisicpline['GoodConduct_Misconduct_View'],""));
//$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>";

$infobar1 = $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);

$addBtn = $linterface->GET_LNK_NEW("javascript:checkNew('category_item_new.php?CategoryID=$CategoryID')","","","","",0);
$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEdit(document.form1,'ItemID[]','category_item_edit.php?CategoryID=$CategoryID')","","","","",0);
$delBtn = $linterface->GET_LNK_REMOVE("javascript:checkRemove(document.form1,'ItemID[]','category_item_remove.php?CategoryID=$CategoryID')");

/*
$toolbar = $linterface->GET_LNK_NEW("javascript:checkNew('category_item_new.php?CategoryID=$CategoryID')","","","","",0);

$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkEdit(document.form1,'ItemID[]','category_item_edit.php?CategoryID=$CategoryID')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_edit.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_edit
					</a>
				</td>";
$table_tool .= "<td><img src=\"{$image_path}/{$LAYOUT_SKIN}/10x10.gif\" width=\"5\"></td>";
$table_tool .= "<td nowrap=\"nowrap\">
					<a href=\"javascript:checkRemove(document.form1,'ItemID[]','category_item_remove.php?CategoryID=$CategoryID')\" class=\"tabletool\">
						<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_delete.gif\" width=\"12\" height=\"12\" border=\"0\" align=\"absmiddle\">
						$button_remove
					</a>
				</td>";
*/

# Generate System Message #
if($error == 1)
{
	$SysMsg = $linterface->GET_SYS_MSG("",$eDiscipline['Accumulative_Period_InUse_Warning']);
}
else
{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("duplicate_item");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #

### Update Display Order ###
if($targetItemID != "" && $sortingMethod != "")
{
	$sql = "SELECT ItemID, DisplayOrder FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE CategoryID = '".$CategoryID."' and RecordStatus != '". DISCIPLINE_DELETED_RECORD ."' ORDER BY DisplayOrder, ItemID";
	$arr_old_order = $ldiscipline->returnArray($sql,2);
	if(sizeof($arr_old_order) > 0)
	{
		for($j=0; $j<sizeof($arr_old_order); $j++){
			list($tmp_item_id, $tmp_order) = $arr_old_order[$j];
			$arr_orignalOrder[$j] = $tmp_item_id;
		}
	}

	if($sortingMethod == REORDER_TOP)
	{
		for($j=0; $j<sizeof($arr_orignalOrder); $j++)
		{
			if($targetItemID == $arr_orignalOrder[$j])
			{
				$sql = "UPDATE DISCIPLINE_ACCU_CATEGORY_ITEM SET DisplayOrder = 1 WHERE ItemID = '$targetItemID'";
				//echo $sql."<BR>";
				$ldiscipline->db_db_query($sql);
				continue;
			}
            
			if ($j==0){
				$pos = $j + 2;
			}
			else if ($j==sizeof($arr_orignalOrder)-1){
				$pos = $j + 1;
			}
			else{
				$pos = $j + 2;
			}
            
			$sql = "UPDATE DISCIPLINE_ACCU_CATEGORY_ITEM SET DisplayOrder = '".$pos."' WHERE ItemID = '".$arr_orignalOrder[$j]."' ";
			//echo $sql."<BR>";
			$ldiscipline->db_db_query($sql);
		}
	}
	if($sortingMethod == REORDER_UP)
	{
		$new_pos = "";
		for($j=0; $j<sizeof($arr_orignalOrder); $j++)
		{
			if($targetItemID == $arr_orignalOrder[$j]){
				$curr_pos = $j+1;
				$new_pos = $curr_pos-1;
			}
		}
        
		for($j=0; $j<sizeof($arr_orignalOrder); $j++)
		{
			if($targetItemID != $arr_orignalOrder[$j]){
				if($j+1 == $new_pos)
					//echo $arr_orignalOrder[$j]." ".$curr_pos."<BR>";
					$final_pos[$arr_orignalOrder[$j]] = $curr_pos;
				else
					//echo $arr_orignalOrder[$j]." ".($j+1)."<BR>";
					$final_pos[$arr_orignalOrder[$j]] = $j+1;
			}
			else{
				//echo $arr_orignalOrder[$j]." ".$new_pos."<BR>";
				$final_pos[$arr_orignalOrder[$j]] = $new_pos;
			}
		}
		
		foreach($arr_orignalOrder as $val){
			//echo $val." ".$final_pos[$val]."<BR>";
			$sql = "UPDATE DISCIPLINE_ACCU_CATEGORY_ITEM SET DisplayOrder = '".$final_pos[$val]."' WHERE ItemID = '$val'";
			//echo $sql."<BR>";
			$ldiscipline->db_db_query($sql);
		}
	}
	if($sortingMethod == REORDER_DOWN)
	{
		$new_pos = "";
		for($j=0; $j<sizeof($arr_orignalOrder); $j++)
		{
			if($targetItemID == $arr_orignalOrder[$j]){
				$curr_pos = $j+1;
				$new_pos = $curr_pos+1;
				//echo $curr_pos;
			}
		}
        
		for($j=0; $j<sizeof($arr_orignalOrder); $j++)
		{
			if($targetItemID != $arr_orignalOrder[$j]){
				if($j+1 == $new_pos)
					//echo $arr_orignalOrder[$j]." ".$curr_pos."<BR>";
					$final_pos[$arr_orignalOrder[$j]] = $curr_pos;
				else
					//echo $arr_orignalOrder[$j]." ".($j+1)."<BR>";
					$final_pos[$arr_orignalOrder[$j]] = $j+1;
			}
			else{
				//echo $arr_orignalOrder[$j]." ".$new_pos."<BR>";
				$final_pos[$arr_orignalOrder[$j]] = $new_pos;
			}
		}
		
		foreach($arr_orignalOrder as $val){
			//echo $val." ".$final_pos[$val]."<BR>";
			$sql = "UPDATE DISCIPLINE_ACCU_CATEGORY_ITEM SET DisplayOrder = '".$final_pos[$val]."' WHERE ItemID = '$val' ";
			//echo $sql."<BR>";
			$ldiscipline->db_db_query($sql);
		}
	}
	if($sortingMethod == REORDER_BOTTOM)
	{
		for($j=0; $j<sizeof($arr_orignalOrder); $j++)
		{
			if($targetItemID == $arr_orignalOrder[$j])
			{
				$sql = "UPDATE DISCIPLINE_ACCU_CATEGORY_ITEM SET DisplayOrder = '".sizeof($arr_orignalOrder)."' WHERE ItemID = '$targetItemID'";
				//echo $sql."<BR>";
				$ldiscipline->db_db_query($sql);
				continue;
			}
			
			if ($j==0)
				$pos = $j + 1;
			else
				$pos = $j;
			
			$sql = "UPDATE DISCIPLINE_ACCU_CATEGORY_ITEM SET DisplayOrder = '".$pos."' WHERE ItemID = '".$arr_orignalOrder[$j]."' ";
			//echo $sql."<BR>";
			$ldiscipline->db_db_query($sql);
		}
	}
}
//print_r($arr_orignalOrder);

if(!isset($Status)){
	$Status = 0;
}
if($Status == 0){
	$cond .= " a.RecordType = '".GOOD_CONDUCT."'";
}
else{
	$cond .= " a.RecordType = '".GOOD_CONDUCT."' AND a.RecordStatus = '$Status' ";
}

### Table SQL ###
$sql = "SELECT
				CONCAT(IF(a.CompulsoryInReport=1,'<font color=red>*</font>',''),a.Name) as Name,
				IF(a.RecordStatus = 1, '".$eDiscipline['Setting_Status_Published']."', IF(a.RecordStatus = 2, '".$eDiscipline['Setting_Status_Drafted']."', ' -- ')),
				IF(a.DisplayOrder = 0, ItemID, ''),
				a.ItemID, a.ItemCode, b.Title
		FROM
				DISCIPLINE_ACCU_CATEGORY_ITEM a
				left join DISCIPLINE_FOLLOWUP_ACTION as b on (a.ActionID=b.ActionID and b.RecordStatus=1)
		WHERE
				a.CategoryID = '$CategoryID' AND
				$cond
				and a.RecordStatus != '". DISCIPLINE_DELETED_RECORD ."'
		ORDER BY
				a.DisplayOrder, a.ItemID ";
$arr_result = $ldiscipline->returnArray($sql,4);

$table_content .= "<tr>
						<th width='24%'>".$eDiscipline['Setting_ItemName']."</th>
						<th width='24%' >".$i_Discipline_System_ItemCode."</th>
							<th width='15%' >".$eDiscipline['Setting_Status']."</th>";
	if($sys_custom['eDisciplinev12_FollowUp'])
	{
		//$table_content .= "		<th width='24%' >".$Lang['eDiscipline']['FollowUp']."</th>";
	}				
	$table_content .= "		<th width='24%' >".$eDiscipline['Setting_Reorder']."</th>
						<th width='1%' ><input type=\"checkbox\" onClick=(this.checked)?setChecked(1,this.form,'ItemID[]'):setChecked(0,this.form,'ItemID[]')></th>
					</tr>";

if(sizeof($arr_result) > 0)
{
	for($i=0; $i<sizeof($arr_result); $i++)
	{
		list($item_name, $item_status, $item_order, $item_id,$item_code, $title) = $arr_result[$i];

		if($i%2==0)
			$table_css = " tablerow1 ";
		else
			$table_css = " tablerow2 ";
        
		if($i == 0){
			$img_order = "<img src={$image_path}/{$LAYOUT_SKIN}/10x10.gif width=30 height=2>
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_DOWN.");\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_BOTTOM.");\">";
		}
		else if($i == sizeof($arr_result)-1){
			$img_order = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_TOP.");\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_UP.");\">";
		}
		else{
			$img_order = "<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_top_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_TOP.");\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_a_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_UP.");\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_sort_d_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_DOWN.");\">
							<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_bottom_off.gif\" onClick=\"changeSortingOrder($item_id,".REORDER_BOTTOM.");\">";
		}
        
		$table_content .= "<tr class=\"$table_css\">
								<td>$item_name</td>
								<td>$item_code</td>
								<td>$item_status</td>";
		if($sys_custom['eDisciplinev12_FollowUp'])
		{
			//$table_content .= "		<td>$title &nbsp;</td>";
		}
		
		$table_content .= "		<td>$img_order</td>
								<td><input type=\"checkbox\" name=\"ItemID[]\" value=\"$item_id\"></td>
							</tr>";
	}
}
else
{
	$table_content .= "<tr class=\"tablerow2\" height=\"80px\"><td colspan=\"5\" align=\"center\">$i_no_record_exists_msg</td></tr>";
}
//$table_content .= "<tr class=\"tablebottom\" height=\"25px\"><td colspan=\"5\"></td></tr>";
$table_content .= "<input type=\"hidden\" name=\"targetItemID\" value=\"\">";
$table_content .= "<input type=\"hidden\" name=\"sortingMethod\" value=\"\">";
?>

<script language="javascript">
function changeSortingOrder(ItemID, Method)
{
	var obj = document.form1;
	if(ItemID != '')
	{
		obj.targetItemID.value = ItemID;
	}
	if(Method != '')
	{
		obj.sortingMethod.value = Method;
	}
	obj.submit();
}
</script>

<form name="form1" action="" method="POST">

<?=$subtags_table;?>
<?=$infobar1?>
<!--
<table width="96%" border="0" cellpadding="3" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr>
		<td align="right"><?=$SysMsg?></td>
	</tr>
	<tr>
		<td align="left"><?=$toolbar?></td>
	</tr>
</table>
-->
<div class="content_top_tool">
        <?=$addBtn?>
<br style="clear:both" />
</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr class="table-action-bar">
		<td>
			<?
				$arr_status = array(array(0,$eDiscipline['Setting_Status_All']),array(1,$eDiscipline['Setting_Status_Published']),array(2,$eDiscipline['Setting_Status_Drafted']));

				$status_selection = getSelectByArray($arr_status," name=\"Status\" onChange=\"document.form1.submit();\" ", $Status=($Status==""?0:$Status),0,1);
				echo $status_selection;
			?>

		</td>
		<td align="right">
			<div class="common_table_tool">
			<?=$editBtn?><?=$delBtn?>
			</div>
		</td>
	</tr>
</table>
<table class="common_table_list">
<?=$table_content;?>
</table>
<? if($sys_custom['skss']['StudentAwardReport']) { ?>
<span class="tabletextremark">
	<span class='tabletextrequire'>*</span> <?=$Lang['eDiscipline']['SKSS']['CompulsoryInReport']?>
</span>	
<? } ?>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>