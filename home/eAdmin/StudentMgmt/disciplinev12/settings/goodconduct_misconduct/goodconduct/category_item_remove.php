<?
// Modifying by:

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(is_array($ItemID)){
	$item_id = implode(",",$ItemID);
}else{
	$item_id = $ItemID;
}

$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_RECORD WHERE ItemID IN ($item_id) AND CategoryID = '$CategoryID'";
$tmp_existing_record = $ldiscipline->returnVector($sql);
$existingRecord = $tmp_existing_record[0];

if($existingRecord > 0){
	header("Location: category_item.php?CategoryID=".$CategoryID."&error=1");
	exit();
}

$result = $ldiscipline->removeCategoryItem($ItemID);

intranet_closedb();

if($result){
	header("Location: category_item.php?CategoryID=".$CategoryID."&msg=3");
	exit();
}else{
	header("Location: category_item.php?CategoryID=".$CategoryID."&msg=13");
	exit();
}

?>
