<?
// Using:

#############################################
#
#	Date	:	2019-05-13	(Bill)
#				Prevent SQL Injection
#
#############################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$PeriodID = IntegerSafe($PeriodID);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/default_period.php", 0);

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_sub_tag = $ldiscipline->returnVector($sql);
if(sizeof($arr_sub_tag) > 0){
	$sub_tag = $arr_sub_tag[0];
}

$sql = "SELECT CONCAT(DateStart,' ".$i_To." ',DateEnd) FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID = '$PeriodID'";
$tmp_periodRange = $ldiscipline->returnVector($sql);
if(sizeof($tmp_periodRange) > 0){
	$PeriodRange = $tmp_periodRange[0];
}

$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType"),array("$PeriodRange",""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step1'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step2'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step3'], 0);
if($Scheme == CALCULATION_SCHEME_FLOATING) {
	$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step4'], 1);
}

if($Scheme != "")
{
	if($Scheme == CALCULATION_SCHEME_STATIC){
		$SchemeTitle = $eDiscipline['Setting_CalculationSchema_Static'];
	}
	if($Scheme == CALCULATION_SCHEME_FLOATING){
		$SchemeTitle = $eDiscipline['Setting_CalculationSchema_Floating'];
	}
}

$sql = "SELECT 
				RuleID, NextNumLate, ProfileMeritNum, ProfileMeritType, ConductScore, SubScore1,
				ReasonItemID, DetentionMinutes 
		FROM 
				DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE 
		WHERE 
				CategoryID = '$CategoryID' AND PeriodID = '$PeriodID'
		ORDER BY 
				RuleOrder";
$arr_existing_result = $ldiscipline->returnArray($sql,5);
if(sizeof($arr_existing_result) > 0)
{
	$existing_cal = 1;
	
	$table_content .= "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"3\" >";
	$table_content .= "<tr class=\"tabletop\">";
	$table_content .= "<td>#</td>";
	$table_content .= "<td>".$eDiscipline['Setting_AccumulatedTimes']."</td>";
	$table_content .= "<td>".$eDiscipline['Setting_AddMeritRecord']."</td>";
	$table_content .= "<td>".$eDiscipline['Setting_ItemName']."</td>";
	//$table_content .= "<td>$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_DetentionSession_Title</td>";
	##$table_content .= "<td>$eDiscipline_Settings_GoodConductMisconuct_NewPeriod_Reminder_Title</td>";
	$table_content .= "<td></td>";
	$table_content .= "</tr>";
	
	for($i=0; $i<sizeof($arr_existing_result); $i++)
	{
		list($rule_id, $accumulated_count, $meritNum, $meritItem, $conduct_mark, $study_score, $itemName, $detentionSession) = $arr_existing_result[$i];
		
		$arr_meritItem = $ldiscipline->returnMeritItemByID($itemName);
		$item_name = $arr_meritItem['ItemName'];
		$item_cat_id = $ldiscipline->returnCatIDByItemID($itemName);
			
		if($i%2==0)
			$row_css = " CLASS=\"tablerow1\" ";
		else
			$row_css = " CLASS=\"tablerow2\" ";
		
		$row = $i + 1;
		$table_content .= "<tr $row_css>";
		$table_content .= "<td>$row</td>";
		$table_content .= "<td>$accumulated_count</td>";
		$table_content .= "<td>$meritNum ".$ldiscipline->RETURN_MERIT_NAME($meritItem)."</td>";
		$table_content .= "<td>$item_name</td>";
		##$table_content .= "<td></td>";		## For Reminder
		//$table_content .= "<td><input class=\"formsubbutton\" type=\"button\" name=\"Duplicate\" value=\"Duplicate\" onClick=\"javascript:copyCurrentSetting($accumulated_count,$meritNum,$meritItem,$item_cat_id,$conduct_mark,$study_score,$itemName,$detentionSession);\" ></td>";
		$table_content .= "<td>".$linterface->GET_BTN($eDiscipline['Setting_Duplicate'], "button", "javascript:copyScheme($rule_id)", "", "class='formsubbutton' onMouseOver=\"this.className='formsubbuttonon'\" onMouseOut=\"this.className='formsubbutton'\"")."</td>";
		$table_content .= "</tr>";
	}
	$table_content .= "</table>";
}
?>

<script language="javascript">
<!--
function copyScheme(RuleID)
{
	var obj = document.form1;
	var cat_id = obj.CategoryID.value;
	var period_type = obj.PeriodType.value;
	var period_id = obj.PeriodID.value;
	var scheme_id = obj.Scheme.value;
	
	var url = "category_period_rule_copy.php?RuleID="+RuleID+"&CategoryID="+cat_id+"&PeriodType="+period_type+"&PeriodID="+period_id+"&Scheme="+scheme_id;
	
	window.location = url;
}
-->
</script>

<form name="form1" action="" method="POST" onSubmit="checkForm();">
<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Period'];?></td>
		<td class="tabletext" valign="top"><?=$PeriodRange;?></td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CalculationSchema'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=$SchemeTitle;?></td>
	</tr>
	<tr height="10px"><td></td></tr>
	<tr>
		<td class="tabletextremark"><i><?=$eDiscipline['Setting_CalculationDetails']?></i></td>
	</tr>
	<tr>
		<td colspan="2">
			<?=$table_content;?>
		</td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN("Add another calculation details", "button", "javacript:window.location = 'category_period_new3.php?CategoryID=$CategoryID&PeriodType=$PeriodType&PeriodID=$PeriodID&Scheme=$Scheme'"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:window.location = 'category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType' ",""," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
		</td>
	</tr>
	<input type="hidden" name="CategoryID" value="<?=$CategoryID?>">
	<input type="hidden" name="PeriodType" value="<?=$PeriodType?>">
	<input type="hidden" name="PeriodID" value="<?=$PeriodID?>">
	<input type="hidden" name="Scheme" value="<?=$Scheme?>">
</table>
<br>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>