<?
### This page is only for static calcuation use ###

# using: 

#################################################
#
#   Date:   2019-10-14  (Bill)  [DM#3661]
#           Hide Email to PIC option - except $sys_custom['eDiscipline']['GMConversionStepShowEmailToPIC'] = true
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#	Date:	2017-09-21	(Bill)	[2017-0710-1151-50054]
#			added options - Email to PIC / Class Teacher / eDiscipline Administrator
#
#	Date:	2017-07-11	(Bill)	[2016-1125-0939-00240]
#			support Study Score Limit ($sys_custom['eDiscipline']['ApplyMaxStudyScore'])
# 
#	Date:	2016-04-14	Bill	[DM#2971]
#			Correct Remarks
#
#	Date:	2013-02-27	YatWoon
#			check with $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] [Case#2013-0225-1322-36071]
#			Need alter tabel change int(11) to float(8,1) for DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE and DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING 
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$PeriodID = IntegerSafe($PeriodID);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")) {
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";
$CurrentPageArr['eDisciplinev12'] = 1;

### Default warning message when user access into this page ###
$warning_msg = $linterface->GET_SYS_MSG("",$Lang['eDiscipline']['Accumulative_Period_In_Use_Warning_GoodConduct']);

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 0);

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/default_period.php", 0);

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_sub_tag = $ldiscipline->returnVector($sql);
if(sizeof($arr_sub_tag) > 0){
	$sub_tag = $arr_sub_tag[0];
}

if(is_array($PeriodID)){
	$PeriodID = $PeriodID[0];
}
else{
	$PeriodID = $PeriodID;
}
$sql = "SELECT DateStart, DateEnd FROM DISCIPLINE_ACCU_PERIOD WHERE PeriodID = '$PeriodID'";
$arr_result = $ldiscipline->returnArray($sql,2);
if(sizeof($arr_result) > 0){
	list($StartDate, $EndDate) = $arr_result[0];
}
$PeriodRange = $StartDate." ".$i_To." ".$EndDate;

//$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType"),array("$PeriodRange","category_period_rule.php?meritType=1&CategoryID=$CategoryID&Scheme=$Scheme&PeriodType=$PeriodType&PeriodID=$PeriodID"), array($eDiscipline['Setting_NAV_EditPeriod'],""));
$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType"),array("$PeriodRange",""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

if ($msg != "")
{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #				

if($Scheme != "")
{
	if($Scheme == CALCULATION_SCHEME_STATIC){
		$SchemeTitle = $eDiscipline_Settings_GoodConductMisconuct_NewPeriod_CalScheme_Static;
	}
	if($Scheme == CALCULATION_SCHEME_FLOATING){
		$SchemeTitle = $eDiscipline_Settings_GoodConductMisconuct_NewPeriod_CalScheme_Floating;
	}
}

### Get The period calculation setting ###
if(is_array($PeriodID)){
	$PeriodID = $PeriodID[0];
}
else{
	$PeriodID = $PeriodID;
}
$sql = "SELECT 
				SettingID,
				PeriodID, 
				UpgradeCount,
				UpgradeDemeritType,
				UpgradeDemeritCount,
				UpgradeToItemID,
				UpgradeDeductConductScore,
				UpgradeDeductSubScore1,
				TemplateID,
				SendNoticeAction,
				SendEmailToPIC,
				SendEmailToClassTeacher,
				SendEmailToAdmin
		FROM 
				DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING 
		WHERE 
				PeriodID = '$PeriodID' AND CategoryID = '$CategoryID'";
$arr_result = $ldiscipline->returnArray($sql,6);
if(sizeof($arr_result) > 0) {
	list($setting_id, $period_id, $accumlativeTimes, $meritType, $meritNum, $itemName, $conductScore, $studyScore, $TemplateID, $sendNoticeAction, $SendEmailToPIC, $SendEmailToClassTeacher, $SendEmailToAdmin) = $arr_result[0];
}

for($i=1; $i<=$ldiscipline->AccumulativeTimes_MAX; $i++){
	$arr_AccumulativeTimes[] = $i;
}
$accumlative_selection = getSelectByValue($arr_AccumulativeTimes," name=\"accumlativeTimes\" ",$accumlativeTimes,0,1);

$conductMarkInterval = $sys_custom['eDiscipline']['ConductMark1DecimalPlace'] ? 0.1 : 1;
for($i=0; $i<=$ldiscipline->ConductMarkIncrement_MAX; $i=$i+$conductMarkInterval){
	$arr_ConductMarkAdjustment[] = round($i,2);
}
$conductmark_selection = getSelectByValue($arr_ConductMarkAdjustment," name=\"conductMark\" ",$conductScore,0,1);

for($i=0; $i<=$ldiscipline->SubScoreIncrement_MAX; $i++){
	$arr_StudyScoreAdjustment[] = $i;
}
// [2016-1125-0939-00240]
$select_tag = " name=\"studyMark\" ";
if($sys_custom['eDiscipline']['ApplyMaxStudyScore']) {
	$select_tag .= " onchange=\"checkOverflowStudyScore();\" ";
}
$studymark_selection = getSelectByValue($arr_StudyScoreAdjustment, $select_tag, $studyScore, 0, 1);

$AP_Interval = $ldiscipline->AP_Interval_Value ? $ldiscipline->AP_Interval_Value : 1;
for ($i=0; $i<=$ldiscipline->AwardPunish_MAX; $i=$i+$AP_Interval) {
	$arr_AddMeritRecord[] = $i;
}
$meritrecord_selection = getSelectByValue($arr_AddMeritRecord," name=\"meritNum\" ",$meritNum,0,1);

$meritType_selection = $ldiscipline->getMeritDemeritTypeSelection(1, "meritType",$meritType);

$merit_cats = $ldiscipline->returnMeritItemCategoryByType(1);
$meritCategory_selection = getSelectByArray($merit_cats," name=\"meritCategoryID\" onChange=\"changeCat(this.value);\" ","",0,0,"Select A Category");

$items = $ldiscipline->retrieveMeritItemswithCodeGroupByCat();

for($i=1; $i<=$ldiscipline->ReminderLevel_MAX; $i++){
	$arr_ReminderLevel[] = $i;
}
$reminderLevel_selection = getSelectByValue($arr_ReminderLevel," name=\"ReminderLevel\" ","",0,1);

$tmp_cat_id = $ldiscipline->returnCatIDByItemID($itemName);

$NoticeTemplateAva = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("", 1);
if (!$lnotice->disabled && !empty($NoticeTemplateAva))
{
	$catTmpArr = $ldiscipline->TemplateCategory();
	if(is_array($catTmpArr))
	{
		$catArr[0] = array("0","-- $button_select --");
		foreach($catTmpArr as $Key=>$Value)
		{
			# check the Template Catgory has template or not
			$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("", 1, $Key);
			if(!empty($NoticeTemplateAvaTemp)) {
				$catArr[] = array($Key, $Value);
			}
		}
	}
	
	$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);
	$TemplateCategoryID = $TemplateAry[0]['CategoryID'];	
	if($TemplateCategoryID != "0" && $TemplateCategoryID != "") {
		$js .= "changeTemplate('{$TemplateCategoryID}', 0);itemSelected('$TemplateID', 0, '$TemplateID');\n";
	}
	$catSelection0 = $linterface->GET_SELECTION_BOX($catArr, 'id="CategoryID0" name="CategoryID0" onChange="changeTemplate(this.value, 0)"', "", "$TemplateCategoryID");
	
	// Preset template items (for javascript)
	for ($i=0; $i<sizeof($catArr); $i++)
	{
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
		for ($j=0; $j<=sizeof($result); $j++) {
			if ($j==0) {
				$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
				$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
			}
			else {
				$tempTemplate = $result[$j-1][1];
				$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
		        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
		        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
		        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
    
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
				$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
			}
		}
	}
}
?>

<script language="javascript">
<!--
<?=$jTemplateString?>

function changeCat(val)
{
	document.form1.meritItemID.length = 1;		// reset the drop down meun length
	<?
		for($i=0; $i<sizeof($items); $i++){
			list($r_catID, $r_itemID, $r_itemName) = $items[$i];
			$r_itemName = intranet_undo_htmlspecialchars($r_itemName);
			$r_itemName = str_replace('"', '\"', $r_itemName);
	     	$r_itemName = str_replace("'", "\'", $r_itemName);
	?>
			if(val == <?=$r_catID?>){
				var x = document.form1.meritItemID.length; 
				//var x = 1+<?=$i?>;
				//document.form1.meritItemID[x] = new Option("<?=intranet_htmlspecialchars($r_itemName)?>","<?=intranet_htmlspecialchars($r_itemID)?>");
				document.form1.meritItemID[x] = new Option("<?=$r_itemName?>","<?=intranet_htmlspecialchars($r_itemID)?>");
			}
	<?
		}
	?>
}	

function changeItem(value) {
	var i;
	var meritType = document.form1.elements["meritType"];
	var meritNum = document.form1.elements["meritNum"];
	var conductScore = document.form1.elements["conductMark"];

	<? if ($ldiscipline->UseSubScore) { ?>
		var subScore1 = document.form1.elements["studyMark"];
	<? } ?>
	<?
	for ($i=0; $i<sizeof($items); $i++)
	{
		list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
	    $r_subscore2 = $items[$i]["SubScore2"];
	    $r_MaxGainDeductSubScore1 = $items[$i]['MaxGainDeductSubScore1'];
	?>
		if (value=="<?=$r_itemID?>")
		{
			if(document.form1.temp_flag.value==1)
			{
				check_meritType = <?=$r_meritType?>;
				check_meritNum = <?=$r_meritNum?>;
				check_conductScore = <?=$r_conduct?>;
			}
			else
			{
				check_meritType = <?=($r_meritType?$r_meritType:0) ?>;
				check_meritNum = <?=($r_meritNum?$r_meritNum:0)?>;
				check_conductScore = <?=($r_conduct?$r_conduct:0)?>;
			}
			
			// Merit Type
			for (i=0; i<meritType.options.length; i++)
			{
				if (meritType.options[i].value==check_meritType)
				{
					meritType.selectedIndex = i;
					break;
				}
			}
			
			// Merit Num
			for (i=0; i<meritNum.options.length; i++)
			{
				if (meritNum.options[i].value==check_meritNum)
				{
					meritNum.selectedIndex = i;
					break;
				}
			}
			
			// Conduct Score
			for (i=0; i<conductScore.options.length; i++)
			{
				if (conductScore.options[i].value==check_conductScore)
				{
					conductScore.selectedIndex = i;
					break;
				}
			}
			
			// Sub Score
			<? if ($ldiscipline->UseSubScore) { ?>
			for (i=0; i<subScore1.options.length; i++)
			{
				if (subScore1.options[i].value=="<?=$r_subscore1?>")
				{
					subScore1.selectedIndex = i;
					break;
				}
			}
			<? } ?>
         	
	       	<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
	        	if(document.form1.maxStudyScore) {
	        		document.form1.maxStudyScore.value = "<?=$r_MaxGainDeductSubScore1?>";
	        		$('#MaxStudyScoreWarnDiv').html('<?=str_replace("<!--MAX_SCORE-->", $r_MaxGainDeductSubScore1, $Lang['eDiscipline']['StudentExceedMeritItemMaxStudyScore'])?>');
	        		$('#MaxStudyScoreWarnDiv').hide();
	        		resetOverflowStudyScoreDisable();
	        	}
	       	<? } ?>
		}
	<?
	}
	?>
}

function changeTemplate(cat, selectIdx){
	var x = document.getElementById("SelectNotice"+selectIdx);
	if(x) {
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}

	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}
	
	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		tempText = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];
		tempValue = eval("jArrayTemplate"+cat+"Value")[j];
		
		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}
}

function itemSelected(itemID, selectIdx, templateID) {
	var obj = document.form1;
	//if(templateID!="") {
		var item_length = eval("obj.SelectNotice"+selectIdx+".length");
		for(var i=0; i<item_length; i++) {
			if(obj.elements['SelectNotice'+selectIdx].options[i].value==templateID) {
				obj.elements['SelectNotice'+selectIdx].selectedIndex = i;
				//alert(i);
				break;	
			}
		}
	//}
}

function checkForm()
{
	var obj = document.form1;
	var category_check = 0;
	var item_check = 0;
	var merit_type_check = 0;
	
	for(i=0; i<obj.meritCategoryID.length; i++){
		if(obj.meritCategoryID.options[i].selected == true && i != 0){
			category_check = 1;
		}
	}
	for(i=0; i<obj.meritItemID.length; i++){
		if(obj.meritItemID.options[i].selected == true && i != 0){
			item_check = 1;
		}
	}
	for(i=0; i<obj.meritType.length; i++){
		if(obj.meritType.options[i].selected == true){
			merit_type_check = 1;
		}
	}	
	
	/* check the merit type 0 - N.A. */
	if(obj.meritNum.value==0)	obj.meritType.selectedIndex = 0;
	if(obj.meritType.value==-999)	obj.meritNum.selectedIndex = 0;
	
	if(category_check == 1 && item_check == 1 && merit_type_check == 1){
		obj.action = "category_period_edit_update.php";
		return true;
	}
	else{
		if(category_check == 0){
			alert("Please select a category");
			return false;
		}
		if(item_check == 0){
			alert("Please select an item");
			return false;
		}
		if(merit_type_check == 0){
			alert("Please select a merit type");
			return false;
		}
	}
}

function checkNoticeAction(val) {
	if(val==1) {
		document.getElementById('CategoryID0').disabled = false;
		document.getElementById('SelectNotice0').disabled = false;
	}
	else {
		document.getElementById('CategoryID0').disabled = true;
		document.getElementById('SelectNotice0').disabled = true;
	}
}

<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
function checkOverflowStudyScore()
{
	resetOverflowStudyScoreDisable();
	
	// Exceed Max Study Score (Display Warning & Disable Submit Button)
	if(document.form1.studyMark && document.form1.maxStudyScore && parseInt(document.form1.maxStudyScore.value) > 0 && parseInt(document.form1.studyMark.value) > parseInt(document.form1.maxStudyScore.value)) {
		document.form1.studyMark.focus();
		$('#MaxStudyScoreWarnDiv').show();
		$("input[type=submit]", document.form1).attr('disabled', true);
		$("input[type=submit]", document.form1).attr('class', 'formbutton_disable');
	}
}

function resetOverflowStudyScoreDisable()
{
	// Reset
	$('#MaxStudyScoreWarnDiv').hide();
	$("input[type=submit]", document.form1).attr('disabled', false);
	$("input[type=submit]", document.form1).attr('class', 'formbutton');
}
<? } ?>
-->
</script>

<form name="form1" action="" method="POST" onSubmit="return checkForm();">
<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr><td colspan='2'><center><?=$warning_msg?></center></td></tr>	
	<?=$infobar1?>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Period'];?></td>
		<td class="tabletext" valign="top"><?=$PeriodRange;?></td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CalculationScheme'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=$SchemeTitle;?></td>
	</tr>
	
	<?
		if($Scheme == CALCULATION_SCHEME_FLOATING && $existing_cal==1){
	?>
	<tr height="10px"><td></td></tr>
	<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_CalculationSettings'];?></i></td></tr>
	<tr height="10px"><td></td></tr>
	<tr>
		<td colspan="2">
			<?=$calculation_details;?>
		</td>
	</tr>
	<?
		}
	?>
	
	<tr height="10px"><td></td></tr>
	<?
	if($Scheme == CALCULATION_SCHEME_FLOATING){
		if($existing_cal==1){
	?>
			<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_FutherCalculationInfo'];?></i></td></tr>
	<?
		}
		else{
	?>
			<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_FirstCalculationSchema'];?></i></td></tr>
	<?
		}
	}
	else{
	?>
		<tr><td class="tabletextremark"><i><?=$eDiscipline['Setting_CalculationInfo'];?></i></td></tr>
	<?
	}
	?>
	<tr height="10px"><td></td></tr>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Condition']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<?=$i_Discipline_System_GoodConduct_Misconduct_Every?> <?=$accumlative_selection;?> <?=$i_Discipline_System_GoodConduct_Misconduct_Instance_Of?> <b><u><?=$sub_tag;?></u></b> <?=$i_Discipline_System_GoodConduct_Misconduct_Will_Be_Count_As?> <br>
			<?=$meritCategory_selection;?> <?=$i_Discipline_System_GoodConduct_Misconduct_Category_To_Subcategory?> <?=$meritItem_selection;?>
			<SELECT name="meritItemID" onChange="changeItem(this.value)">
					<OPTION value="0" selected>-- <?=$button_select?> --</OPTION>
			</SELECT>
		</td>
	</tr>
	
	<? if(!$ldiscipline->Hidden_ConductMark) {?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_ConductMark_Increase'];?> <span class="tabletextrequire">*</span> <span class="tabletextrequire2">#</span></td>
		<td class="tabletext" valign="top">
			<?=$conductmark_selection;?>
		</td>
	</tr>
	<? } ?>
	
	<? if($sys_custom['UseSubScore1']){ ?>
		<tr>
			<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_StudyMark_Increase'];?> <span class="tabletextrequire">*</span> <span class="tabletextrequire2">#</span></td>
			<td class="tabletext" valign="top">
				<?=$studymark_selection;?>
				<? if($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']) { ?>
					<div id="MaxStudyScoreWarnDiv" style="color:red;"></div>
					<input type="hidden" name="maxStudyScore" value="0" />
		        <? } ?>
			</td>
		</tr>
	<? } ?>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_AddMeritRecord'];?> <span class="tabletextrequire">*</span> <span class="tabletextrequire2">#</span></td>
		<td class="tabletext" valign="top">
			<?=$meritrecord_selection;?><?=$meritType_selection;?>
		</td>
	</tr>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['eNoticeTemplate']?></td>
		<td class="tabletext" valign="top">
			<input type="checkbox" name="sendNoticeAction" id="sendNoticeAction" value="1" <?if($sendNoticeAction) echo "checked";?> onClick="checkNoticeAction(this.checked)"><label for="sendNoticeAction"><?=$Lang['eDiscipline']['SendNoticeActionMsg']?></label><br>
			<?=$catSelection0?><select name="SelectNotice0" id="SelectNotice0"><option value="0"><?="-- $button_select --"?></option></select>
		</td>
	</tr>
	
	<tr height="10px"><td></td></tr>
	<tr><td class="tabletextremark"><i> - <?=$eDiscipline["Notification"]?> -</i></td></tr>
	<tr height="10px"><td></td></tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline["EmailTo"]?></td>
        <td class="tabletext" valign="top">
            <?php if ($sys_custom['eDiscipline']['GMConversionStepShowEmailToPIC']) { ?>
			    <input name="SendEmailToPIC" type="checkbox" id="SendEmailToPIC" value="1" <? if($SendEmailToPIC) echo "checked"; ?>><label for="SendEmailToPIC"><?=$i_Discipline_PIC?></label>
            <?php } ?>
			<input name="SendEmailToClassTeacher" type="checkbox" id="SendEmailToClassTeacher" value="1" <? if($SendEmailToClassTeacher) echo "checked"; ?>><label for="SendEmailToClassTeacher"><?=$i_Teaching_ClassTeacher?></label>
			<input name="SendEmailToAdmin" type="checkbox" id="SendEmailToAdmin" value="1" <? if($SendEmailToAdmin) echo "checked"; ?>><label for="SendEmailToAdmin"><?=$eDiscipline["DisciplineAdmin2"]?></label>
		</td>
	</tr>
	
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
	</tr>
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><span class="tabletextrequire2">#</span> <?=$Lang['eDiscipline']['PleaseSelectAtLeaseOneAward'];?></td>
	</tr>
	
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
			<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore']? "resetOverflowStudyScoreDisable()" : ""), "reset2", " class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
		</td>
	</tr>
	
	<input type="hidden" name="CategoryID" value="<?=$CategoryID;?>">
	<input type="hidden" name="PeriodType" value="<?=$PeriodType;?>">
	<input type="hidden" name="PeriodID" value="<?=$PeriodID;?>">
	<input type="hidden" name="StartDate" value="<?=$StartDate;?>">
	<input type="hidden" name="EndDate" value="<?=$EndDate;?>">
	<input type="hidden" name="Scheme" value="<?=$Scheme;?>">
	<input type="hidden" name="SettingID" value="<?=$setting_id?>">
	<input type="hidden" name="flag" value="1">
	<input type="hidden" name="temp_flag" value="0" />
</table>
</form>

<script language="javascript">
<!--
if(<?=$itemName;?> != ""){
	var obj = document.form1;
	changeCat(<?=$tmp_cat_id;?>);
	
	for(i=0; i<obj.meritCategoryID.length; i++){
		if(obj.meritCategoryID.options[i].value == <?=$tmp_cat_id;?>){
			obj.meritCategoryID.options[i].selected = true;
		}
	}
	for(i=0; i<obj.meritItemID.length; i++){
		if(obj.meritItemID.options[i].value == <?=$itemName;?>){
			obj.meritItemID.options[i].selected = true;
		}
	}
	
	<? if ($ldiscipline->UseSubScore && $sys_custom['eDiscipline']['ApplyMaxStudyScore'] && $itemName!="") {
		for ($i=0; $i<sizeof($items); $i++)
		{
			list($r_catID, $r_itemID, $r_itemName, $r_meritType, $r_meritNum, $r_conduct, $r_punish, $r_detention, $r_subscore1) = $items[$i];
		    $thisMaxGainDeductSubScore1 = $items[$i]['MaxGainDeductSubScore1'];
		    
		    if($itemName != $r_itemID)	continue;
	?>
			// for Study Score Limit of saved Item 
		    if(document.form1.maxStudyScore) {
	    		document.form1.maxStudyScore.value = "<?=$thisMaxGainDeductSubScore1?>";
	    		$('#MaxStudyScoreWarnDiv').html('<?=str_replace("<!--MAX_SCORE-->", $thisMaxGainDeductSubScore1, $Lang['eDiscipline']['StudentExceedMeritItemMaxStudyScore'])?>');
	    		$('#MaxStudyScoreWarnDiv').hide();
    		}
	<?		
			break;
		}
	} ?>
-->
}
</script>

<?
echo "<script language='javascript'>
$js
checkNoticeAction($sendNoticeAction);
</script>";

intranet_closedb();
$linterface->LAYOUT_STOP();
?>