<?
$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

$CurrentPage = "Settings_GoodConductMisconduct";

if(sizeof($PeriodID)>0){
	$targetPeriod = implode(",",$PeriodID);
}else{
	$targetPeriod = $PeriodID;
}
	
$sql = "SELECT SettingID FROM DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING WHERE PeriodID IN ($targetPeriod)";
$temp = $ldiscipline->returnVector($sql);

if($temp[0]+0>0){
	header("Location: default_period.php?error=1");
	exit();
}

$result = $ldiscipline->removeDefaultPeriod($PeriodID);

intranet_closedb();
	
if($result){
	header("Location: default_period.php?msg=3");
	exit();
}else{
	header("Location: default_period.php?msg=13");
	exit();
}

?>