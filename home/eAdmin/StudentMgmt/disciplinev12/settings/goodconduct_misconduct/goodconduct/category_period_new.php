<?
// Using:

#################################################
#
#	Date:	2019-05-13	(Bill)
#			Prevent SQL Injection
#
#################################################

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

### Handle SQL Injection + XSS [START]
$CategoryID = IntegerSafe($CategoryID);
$PeriodID = IntegerSafe($PeriodID);
### Handle SQL Injection + XSS [END]

$ldiscipline = new libdisciplinev12();
$lfile = new libfilesystem();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

### Check the semester mode - auto change / manual change ###
$semester_mode = $lfile->file_read($intranet_root."/file/semester_mode.txt");

$CurrentPage = "Settings_GoodConductMisconduct";

### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 0);

$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/default_period.php", 0);

$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}
	else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";

### Navigation ###
$sql = "SELECT Name FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID = '$CategoryID'";
$arr_sub_tag = $ldiscipline->returnVector($sql);
if(sizeof($arr_sub_tag) > 0){
	$sub_tag = $arr_sub_tag[0];
}

if(is_array($PeriodID)) {
	$PeriodID = $PeriodID[0];
}

$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array("$sub_tag","category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType"),array($eDiscipline['Setting_NAV_NewPeriod'],""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step1'], 1);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step2'], 0);
$STEPS_OBJ[] = array($eDiscipline['Setting_NewPeriod_Step3'], 0);

# Generate System Message #
if ($error != "")
{
	if($error == 1) $SysMsg = $linterface->GET_SYS_MSG("", $eDiscipline['Setting_DefaultPeriodOverlapWarning']);
	if($error == 2) $SysMsg = $linterface->GET_SYS_MSG("", $Lang['eDisicpline']['PeriodCannotAcrossSchoolTerm']);
	if($error == 3) $SysMsg = $linterface->GET_SYS_MSG("", $Lang['eDiscipline']['SchoolTermNotYetSet']);
}
else
{
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #
?>

<script language="javascript">
function checkForm()
{
	var obj = document.form1;
	if(check_text(obj.StartDate,"<?=$eDiscipline['Setting_JSWarning_PeriodStartDateEmpty'];?>")){
		if(check_text(obj.EndDate,"<?=$eDiscipline['Setting_JSWarning_PeriodEndDateEmpty'];?>")){
			if(compareDate(obj.EndDate.value,obj.StartDate.value) == true){
				return true;
			}else{
				alert("<?=$eDiscipline['Setting_JSWarning_StartDateLargerThanEndDate'];?>");
			}
				
		}
	}
	return false;
}
</script>

<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>
<form name="form1" action="category_period_new2.php" method="POST" onSubmit="return checkForm();">
<table border="0" width="88%">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_From?><span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=$linterface->GET_DATE_PICKER("StartDate",$StartDate)?><!--<input class="formtextbox" type="text" name="StartDate" maxlength="80" value="<?=$StartDate?>"><?echo $linterface->GET_CALENDAR(form1,StartDate);?>//--></td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_To?><span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=$linterface->GET_DATE_PICKER("EndDate",$EndDate)?><!--<input class="formtextbox" type="text" name="EndDate" maxlength="80" value="<?=$EndDate?>"><?echo $linterface->GET_CALENDAR(form1,EndDate);?>//--></td>
	</tr>
	<? if($semester_mode == SEMESTER_MODE_MANUALLY){ ?>
	<!--<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_Discipline_Semester;?><span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=getSelectSemester("name=semester");?></td>
	</tr>//-->
	<? } ?>
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_continue, "submit"); ?>
			<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location.href='category_period.php?CategoryID=$CategoryID&PeriodType=$PeriodType'","cancel"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\""); ?>
		</td>
	</tr>
	<input type="hidden" name="CategoryID" value="<?=$CategoryID;?>">
	<input type="hidden" name="PeriodType" value="<?=$PeriodType;?>">
	<input type="hidden" name="PeriodID" value="<?=$PeriodID;?>">
	<input type="hidden" name="NewPeriodRange" value="1">
	<input type="hidden" name="MeritType" value="<?=GOOD_CONDUCT?>">
</table>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>