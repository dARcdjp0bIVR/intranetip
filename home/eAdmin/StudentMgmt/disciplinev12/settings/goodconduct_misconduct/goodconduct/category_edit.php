<?
# using: 

########## Change Log ###############
#
#	Date	:	2015-04-10 (Bill)	[2015-0127-1101-48073]
#	Detail	:	add auto select eNotice option
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

if(!$ldiscipline->CHECK_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access")){
	$ldiscipline->NO_ACCESS_RIGHT_REDIRECT();
	exit();
}

$CurrentPage = "Settings_GoodConductMisconduct";


### Tag Object ###
$TAGS_OBJ[] = array($eDiscipline['Setting_GoodConduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$TAGS_OBJ[] = array($eDiscipline['Setting_Misconduct'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/misconduct/category.php", 0);


$CurrentPageArr['eDisciplinev12'] = 1;

$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$SUBTAGS_OBJ[] = array($eDiscipline['Setting_CategorySettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/category.php", 1);
$SUBTAGS_OBJ[] = array($eDiscipline['Setting_DefaultPeriodSettings'], $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/disciplinev12/settings/goodconduct_misconduct/goodconduct/default_period.php", 0);

						
$subtags_table .= "<table width=\"96%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
$subtags_table .= "<tr height=\"5px\"><td></td></tr>";
$subtags_table .= "<tr>
					<td class=\"tab_underline\"><div class=\"shadetabs\">
						<ul>
					";
for($i=0; $i<sizeof($SUBTAGS_OBJ); $i++){
	if($SUBTAGS_OBJ[$i][2] == 1){
		$subtags_table .= "<li class=\"selected\"><a href=\"".$SUBTAGS_OBJ[$i][1]."\"><strong>".$SUBTAGS_OBJ[$i][0]."</strong></a></li>";
	}else{
		$subtags_table .= "<li><a href=\"".$SUBTAGS_OBJ[$i][1]."\">".$SUBTAGS_OBJ[$i][0]."</a></li>";
	}
}
$subtags_table .= "</ul></td></tr>";
$subtags_table .= "</table>";


### Navigation ###
$temp = array(array($eDiscipline['Setting_NAV_CategoryList'],"category.php"),array($eDiscipline['Setting_NAV_EditCategory'],""));
$infobar1 .= "<tr><td colspan=\"2\" class=\"navigation\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

# Generate System Message #
if ($msg != "") {
	if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
	if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
	if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
	if ($msg == 12) $SysMsg = $linterface->GET_SYS_MSG("add_failed");
	if ($msg == 13) $SysMsg = $linterface->GET_SYS_MSG("delete_failed");
	if ($msg == 14) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
}
# End #

if(is_array($CategoryID)){
	$category_id = $CategoryID[0];
}else{
	$category_id = $CategoryID;
}

$sql = "SELECT 
				a.Name, a.RecordStatus, b.RecordStatus, a.TemplateID, a.AutoSelecteNotice
		FROM 
				DISCIPLINE_ACCU_CATEGORY AS a INNER JOIN 
				DISCIPLINE_ACCU_CATEGORY_PERIOD AS b ON (a.CategoryID = b.CategoryID)
		WHERE 
				a.CategoryID = $category_id";

$arr_Category = $ldiscipline->returnArray($sql);

if(sizeof($arr_Category)>0){
	// [2015-0127-1101-48073] - also get auto select eNotice option value
	list($cat_name, $cat_status, $period, $TemplateID, $DefaultSendNotice) = $arr_Category[0];
}
if($TemplateID=="")
	$TemplateID = 0;

$TemplateAry = $ldiscipline->retrieveTemplateDetails($TemplateID);

$TemplateCategoryID = $TemplateAry[0]['CategoryID'];
$AccessGroupRightSelection = $ldiscipline->CategoryAccessGroupSelection($CategoryID, 'GM');

# eNotice Template
$catTmpArr = $ldiscipline->TemplateCategory();
if(is_array($catTmpArr))
{
	$catArr[0] = array("0","-- $button_select --");
	foreach($catTmpArr as $Key=>$Value)
	{
		# check the Template Catgory has template or not
		$NoticeTemplateAvaTemp = $ldiscipline->RETRIEVE_NOTICE_TEMPLATE_INFO("",1, $Key);
		if(!empty($NoticeTemplateAvaTemp))
			$catArr[] = array($Key,$Key);
	}
}
$templateCat = $linterface->GET_SELECTION_BOX($catArr, 'id="TemplateCategoryID", name="TemplateCategoryID" onChange="changeCat(this.value)"', "", $TemplateCategoryID);

// Preset template items (for javascript)
for ($i=0; $i<sizeof($catArr); $i++) {
	$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);
	for ($j=0; $j<=sizeof($result); $j++) {
		if ($j==0) {
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]."Value = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "var jArrayTemplate".$catArr[$i][0]." = new Array(".(sizeof($result)+1).");\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"0\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"-- $button_select --\"\n";
			//$js .= "obj.options[$j] = new Option('-- $button_select --',0);\n";
		} else {
			$tempTemplate = $result[$j-1][1];
			$tempTemplate=str_replace("&lt;", "<", $tempTemplate);
	        $tempTemplate=str_replace("&gt;", ">", $tempTemplate);
	        $tempTemplate=str_replace("&#039;","'",$tempTemplate);
	        $tempTemplate=str_replace("&amp;", "&", $tempTemplate);
			//$tempTemplate=str_replace("&quot;", "\"", $tempTemplate);

			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."Value[$j] = \"".$result[$j-1][0]."\"\n";
			$jTemplateString .= "jArrayTemplate".$catArr[$i][0]."[$j] = \"".$tempTemplate."\"\n";
			//$js .= "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
		}
	}
}

?>

<script language="javascript">
<?=$jTemplateString?>


function checkForm(){
	var obj = document.form1;
	
	if(!check_text(obj.CategoryName,"<?=$eDiscipline['Setting_Warning_InputCategoryName'];?>"))
	{
		return false;
	}
	if((obj.TemplateCategoryID.value==0 && obj.SelectNotice.value!=0) || (obj.TemplateCategoryID.value!=0 && obj.SelectNotice.value==0)) {
		alert("<?=$i_alert_pleaseselect." ".$eDiscipline['eNoticeTemplate']?>");
		return false;	
	}

}

function changeCat(cat){

	var x=document.getElementById("SelectNotice");

	if(x)
	{
		var listLength = x.length;
	}
	for (var i=listLength-1; i>=0; i--) {
		x.remove(i);
	}
	try {
		var tmpCatLength = eval("jArrayTemplate"+cat).length;
	} catch (err) {
		var tmpCatLength = 0;
	}


	for (var j=0; j<tmpCatLength; j++) {
		var y = document.createElement('option');
		y.text = eval("jArrayTemplate"+cat)[j];
		y.value = eval("jArrayTemplate"+cat+"Value")[j];

		try {
			x.add(y,null); // standards compliant
		} catch(ex) {
			x.add(y); // IE only
		}
	}

	/****************
	obj = x;
	<?
	
	for ($i=0; $i<sizeof($catArr); $i++) {
		
		$result = $ldiscipline->getDetentionENoticeByCategoryID("DISCIPLINE", $catArr[$i][0]);?>
		
		if(cat=="<?=$catArr[$i][0]?>") {
			<?
			for ($j=0; $j<=sizeof($result); $j++) { 
				if($result[$j][0]==0) {
					echo "obj.options[0] = new Option('-- $button_select --',0);\n";	
				} else {
					$tempTemplate = $result[$j-1][1];
					echo "obj.options[$j] = new Option('$tempTemplate',".$result[$j-1][0].");\n";
				}
			}
			
		?> } <?
	}
	
	?>
	****************/
}

function itemSelected(itemID) {
	var obj = document.form1;
	var item_length = obj.SelectNotice.length;
	for(var i=0; i<item_length; i++) {
		if(obj.elements['SelectNotice'].options[i].value==<?=$TemplateID?>) {
			obj.elements['SelectNotice'].selectedIndex = i;
			//alert(i);
			break;	
		}
	}
}
</script>

<?=$subtags_table;?>
<table width="98%" border="0" cellpadding="5" cellspacing="0" align="center">
	<?=$infobar1?>
	<tr><td align="right"><?=$SysMsg;?></td></tr>
</table>

<form name="form1" action="category_edit_update.php" method="POST" onSubmit="return checkForm();">
<table border="0" width="96%" border="0" cellpadding="4" cellspacing="0" align="center">
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_CategoryName']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><input class="textboxtext" type="text" name="CategoryName" maxlength="80" value="<?=$cat_name;?>"></td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Period'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
		<?
			
			if($period == 0 || $period == ""){
				$period_default_checked = " CHECKED ";
				$period_specify_checked = " ";
			}else{
				$period_default_checked = " ";
				$period_specify_checked = " CHECKED ";
			}
		?>
			<input type="radio" name="Period" value="0" <?=$period_default_checked;?>  id='Period0'><label for="Period0"><?=$eDiscipline['Setting_Period_Default_Setting'];?></label>
			<input type="radio" name="Period" value="1" <?=$period_specify_checked;?>  id='Period1'><label for="Period1"><?=$eDiscipline['Setting_Period_Specify_Setting'];?></label>
		</td>
	</tr>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['Setting_Status'];?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top">
			<?
				if($cat_status == 1){
					$publish_selected = " CHECKED ";
					$draft_selected = "";
				}
				if($cat_status == 2){
					$publish_selected = "";
					$draft_selected = " CHECKED ";
				}
			?>
			<input type="radio" name="Status" value="2" <?=$draft_selected;?>  id='Status2'><label for="Status2"><?=$eDiscipline['Setting_Status_Drafted'];?></label>
			<input type="radio" name="Status" value="1" <?=$publish_selected;?>  id='Status1'><label for="Status1"><?=$eDiscipline['Setting_Status_Published'];?></label>
		</td>
	</tr>
							
	<!-- Default Set Send eNotice -->
	<tr valign="top">
		<td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><?=$Lang['eDiscipline']['DefaultSendNotice']?></td>
		<td class="tabletext">
			<input type="radio" name="DefaultSendNotice" value="0" id='DefaultSendNotice0' <? if(!$DefaultSendNotice) echo " checked";?>><label for="DefaultSendNotice0"><?=$Lang['General']['No'];?></label>
			<input type="radio" name="DefaultSendNotice" value="1" id='DefaultSendNotice1' <? if($DefaultSendNotice) echo " checked";?>><label for="DefaultSendNotice1"><?=$Lang['General']['Yes'];?></label>
		</td>
	</tr>
	
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$eDiscipline['eNoticeTemplate']?></td>
		<td class="tabletext" valign="top">
			<?=$templateCat?>
			<select name="SelectNotice" id="SelectNotice"><option value="0">-- <?=$button_select?> --</option></select>
		</td>
	</tr>
	<? /* ?>
	<tr>
		<td valign="top" width="25%" nowrap="nowrap" class="formfieldtitle tabletext">Group Access right <span class="tabletextrequire">*</span></td>
		<td class="tabletext" valign="top"><?=$AccessGroupRightSelection?></td>
	</tr>
	<? */ ?>
	
	<tr>
		<td colspan="2" align="left" class="tabletextremark"><?=$i_general_required_field2?></td>
	</tr>
	<tr>
		<td colspan="2" class="dotline"><img src="<?=$image_path;?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
	</tr>
	<tr>
		<td height="10px"></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<? echo $linterface->GET_ACTION_BTN($button_submit, "submit"); ?>
			<? echo $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"ldiscipline.className='formbuttonon'\" onMouseOut=\"ldiscipline.className='formbutton'\""); ?>
			<? echo $linterface->GET_ACTION_BTN($button_cancel, "button", "javascript:history.back();","cancel"," class='formbutton' onMouseOver=\"ldiscipline.className='formbuttonon'\" onMouseOut=\"ldiscipline.className='formbutton'\""); ?>
		</td>
	</tr>
	<input type="hidden" name="CategoryID" value="<?=$category_id;?>">
</table>
</form>

<?
if($TemplateCategoryID!="0" && $TemplateCategoryID!="") {
	echo "<script language='javascript'>changeCat('$TemplateCategoryID');itemSelected('$TemplateID')</script>";
}


intranet_closedb();
$linterface->LAYOUT_STOP();
?>