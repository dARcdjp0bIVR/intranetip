<?
# using : 

########## Change Log ###############
#
#	Date	:	2015-04-10 (Bill)	[2015-0127-1101-48073]
#	Detail	:	update auto select eNotice option value
#
#####################################

$PATH_WRT_ROOT = "../../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$ldiscipline = new libdisciplinev12();
$linterface = new interface_html();

// [2015-0127-1101-48073] pass auto select eNotice option value
//$result = $ldiscipline->updateCategory($CategoryID,$CategoryName,$Status,$Period,GOOD_CONDUCT, 'GM', $AllowGroupID);
$result = $ldiscipline->updateCategory($CategoryID, $CategoryName, $Status, $Period, GOOD_CONDUCT, "", $SelectNotice, '', 0, ($DefaultSendNotice? 1 : 0));

intranet_closedb();

if($result){
	header("Location: category.php?msg=2");
	exit();
}else{
	header("Location: category.php?msg=14");
	exit();
}

?>