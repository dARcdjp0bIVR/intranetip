<?
// Modifying by: 

########## Change Log ###############
#
#	Date	:	2015-05-22 (Bill)	[2015-0518-1013-22207] 
#				display error message if category is default category	
#
#	Date	:	2012-06-26 (YatWoon)
#				add recordstatus checking 
#
#	Date	:	2010-06-11 (Henry)
#	Detail 	:	import of Item
#
#####################################

$PATH_WRT_ROOT="../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_opendb();


$limport = new libimporttext();
$lo = new libfilesystem();
$ldiscipline = new libdisciplinev12();

# Check access right
$ldiscipline->CONTROL_ACCESS("Discipline-SETTINGS-GoodConduct_Misconduct-Access");


$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lo->file_ext($name));
if(!isset($MeritType) || $MeritType=="") $MeritType = 1;

if($ext != ".CSV" && $ext != ".TXT")
{
	header("location: importItem.php?xmsg=import_failed&MeritType=$MeritType");
	exit();
}
$data = $limport->GET_IMPORT_TXT($csvfile);
if(is_array($data))
{
	$col_name = array_shift($data);
}

$file_format = array('Item Code','Item Name','Category ID','Status');

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}
if($format_wrong)
{
	header("location: importItem.php?xmsg=wrong_header&MeritType=$MeritType");
	exit();
}

$ldiscipline = new libdisciplinev12();
$lu = new libuser();

$linterface = new interface_html();

# menu highlight setting
$CurrentPage = "Settings_GoodConductMisconduct";

$CurrentPageArr['eDisciplinev12'] = 1;

# Left menu
$MODULE_OBJ = $ldiscipline->GET_MODULE_OBJ_ARR();

# tag information
$TAGS_OBJ[] = array($eDiscipline['Good_Conduct_and_Misconduct']);

$PAGE_NAVIGATION[] = array($Lang['eDiscipline']['ImportItem']);

# step information
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($iDiscipline['Confirmation'], 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();

### List out the import result
$x .= "<table width=\"100%\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";
$x .= "<tr>";
$x .= "<td class=\"tablebluetop tabletopnolink\" width=\"10\">#</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$i_Discipline_System_ItemCode."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_ItemName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_CategoryName']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline["Type"]."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$eDiscipline['Setting_Status']."</td>";
$x .= "<td class=\"tablebluetop tabletopnolink\">".$iDiscipline['AccumulativePunishment_Import_Failed_Reason']."</td>";
$x .= "</tr>";

$sql = "create table IF NOT EXISTS temp_goodconduct_misconduct_settings_item_import
		(
			 ItemCode varchar(100),
			 ItemName varchar(255),
			 CategoryID int(11),
			 RecordStatus int(11),
			 RecordType int(11),
			 UserID int(11),
			 DateInput datetime
	    ) ENGINE=InnoDB DEFAULT CHARSET=utf8";	    
$ldiscipline->db_db_query($sql);

$fields = mysql_list_fields($intranet_db, 'temp_goodconduct_misconduct_settings_item_import');

# delete the temp data in temp table 
$sql = "delete from temp_goodconduct_misconduct_settings_item_import where UserID=$UserID";
$ldiscipline->db_db_query($sql) or die(mysql_error());

$error_occured = 0;
$space = ($intranet_session_language=="en") ? " ":"";
$typeAry = array('1'=>$eDiscipline['Setting_GoodConduct'],'-1'=>$eDiscipline['Setting_Misconduct']);
$statusAry = array(1=>$eDiscipline['Setting_Status_Using'],2=>$eDiscipline['Setting_Status_NonUsing']);

for($i=0; $i<sizeof($data); $i++)
{
	$error = array();

	### get data
	list($itemCode, $itemName,$catID,$status) = $data[$i];
	$itemCode = intranet_htmlspecialchars(trim($itemCode));
	$itemName = intranet_htmlspecialchars(trim($itemName));
	$catID = trim($catID);
	$status = trim($status);
	
	# Item Code
	if(trim($itemCode)=='')
	{
		$error['itemCode'] = $i_Discipline_System_ItemCode.$space.$ec_html_editor['missing'];	
	}	
	else
	{
		$catName = intranet_htmlspecialchars($catName);
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_CATEGORY_ITEM WHERE ItemCode='$itemCode' and RecordStatus=1";
		$result = $ldiscipline->returnVector($sql);
		
			if($result[0] != 0)
			{
				$error['itemCode'] = $i_Discipline_System_ItemCode.$space.$Lang['eDiscipline']['Import_AlreadyExist'];
			}
	}
		
	# Item Name
	if(trim($itemName)=='')
	{
		$error['itemName'] = $eDiscipline['Setting_ItemName'].$space.$ec_html_editor['missing'];	
	}	
	else
	{
		$catName = intranet_htmlspecialchars($catName);
		$sql = "SELECT COUNT(*) FROM DISCIPLINE_ACCU_CATEGORY WHERE Name='$catName'";
		$result = $ldiscipline->returnVector($sql);
		
			if($result[0] != 0)
			{
				$error['itemName'] = $eDiscipline['Setting_ItemName'].$space.$Lang['eDiscipline']['Import_AlreadyExist'];
			}
	}
	
	# Category 
	$displayCatName = $catID;
	if(trim($catID)=='')
	{
		$error['catID'] = $eDiscipline['Setting_CategoryName'].$space.$ec_html_editor['missing'];
	} 
	else
	{
		$sql = "SELECT CategoryID FROM DISCIPLINE_ACCU_CATEGORY WHERE RecordStatus=1";
		$result = $ldiscipline->returnVector($sql);
		if(!in_array($catID, $result)) {
			$error['catID'] = $eDiscipline['Setting_CategoryName'].$space.$Lang['eDiscipline']['Import_DoesNotExist'];
		} else {
			$displayCatName = intranet_htmlspecialchars($ldiscipline->getGMCatNameByCatID($catID));
			// [2015-0518-1013-22207] display error message if category is default category	
			//if(in_array(trim($catID), array(1,2,3)))
			if(in_array(trim($catID), array(1,2)))
			{
				$error['catID'] = $Lang['eDiscipline']['Import_NoDefaultCat'];
			}
		}
	}
	
	# Merit Type
	$sql = "SELECT MeritType FROM DISCIPLINE_ACCU_CATEGORY WHERE CategoryID=$catID";
	$result = $ldiscipline->returnVector($sql);
	$mType = $result[0];
	$displayType = "---";
	if(trim($mType)=='') {
		//$error['mType'] = $eDiscipline['Type'].$space.$ec_html_editor['missing'];
	} else {
		if(!array_key_exists($mType, $typeAry)) {
			$error['mType'] = $eDiscipline['Type'].$space.$ec_html_editor['incorrect'];
		} else {
			$displayType = $typeAry[$mType];
		}
	}	
	
	# Record Status
	$displayStatus = $status;
	if(trim($status)=='') {
		$error['status'] = $eDiscipline['Setting_Status'].$space.$ec_html_editor['missing'];
	} else {
		if(!array_key_exists($status, $statusAry)) {
			$error['status'] = $eDiscipline['Setting_Status'].$space.$ec_html_editor['incorrect'];
		} else {
			$displayStatus = $statusAry[$status];
		}
	}
	

	$css = (sizeof($error)==0) ? "tabletext":"red";
	
	$x .= "<tr class=\"tablebluerow".($i%2+1)."\">";
	$x .= "<td class=\"$css\">".($i+1)."</td>";
	$x .= "<td class=\"$css\">".$itemCode."</td>";
	$x .= "<td class=\"$css\">". $itemName ."</td>";
	$x .= "<td class=\"$css\">". $displayCatName ."</td>";
	$x .= "<td class=\"$css\">". $displayType ."</td>";
	$x .= "<td class=\"$css\">". $displayStatus ."</td>";
	$x .= "<td class=\"$css\">";
	if(sizeof($error)>0)
	{
		foreach($error as $Key=>$Value)
		{
			$x .=$Value.'<br/>';
		}	
	}
	else
	{	
		$x .= "--";
	}
	$x.="</td>";
	$x .= "</tr>";
		
	if(sizeof($error)>0)
	{
		$error_occured++;
	} else {
		$sql_fields = "(ItemCode, ItemName, CategoryID, RecordStatus, RecordType, UserID, DateInput)";
		$sql_values = "('$itemCode', '$itemName', '$catID', '$status', '$mType', '$UserID', NOW())";
		
		$sql = "insert into temp_goodconduct_misconduct_settings_item_import $sql_fields values $sql_values ";
		$ldiscipline->db_db_query($sql);
		
	}
}
$x .= "</table>";

if($error_occured)
{
	$import_button = $linterface->GET_ACTION_BTN($button_back, "button", "window.location='importItem.php?MeritType=$MeritType'");
}
else
{	
	$import_button = $linterface->GET_ACTION_BTN($button_import, "submit")." &nbsp;".
	$linterface->GET_ACTION_BTN($button_back, "button", "window.location='importItem.php?MeritType=$MeritType'");	
}


?>

<br />
<form name="form1" method="post" action="importItem_update.php" enctype="multipart/form-data">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td class="navigation"><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	</tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td>
</tr>
<tr>
	<td align="center">
		<table width="96%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td colspan="2"><?=$x?></td>
				</tr>
				</table>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr>
			<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
		</tr>
		<tr>
			<td align="center" colspan="2">
			<?=$import_button?>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<input type="hidden" name="MeritType" id="MeritType" value="<?=$MeritType?>">
</form>
<br />

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>