<?
# using: 

########## Change Log ###############
#
#	Date	:	2015-05-22 (Bill)	[2015-0518-1013-22207]  
#				filter default categories if task is "category2"
#
#	Date	:	2015-04-13 (Bill)	[2015-0128-1044-22170] / [2015-0313-1420-56054] / [2015-0127-1101-48073]
#	Detail	:	display reference popup for auto select detention and eNotice option
#
#	Date	:	2010-06-11 (Henry)
#	Detail 	:	import of Category/Item
#
#####################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$ldiscipline = new libdisciplinev12();

if($task=='conversionPeriod')
{	
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Setting_Period'].'</td></tr>';
				$data .='<tr class="tablerow1">
					 <td>0</td>
					 <td>'.$eDiscipline['Setting_Period_Use_Default'].'</td></tr>
					 <tr class="tablerow1">
					 <td>1</td>
					 <td>'.$eDiscipline['Setting_Period_Specify_Setting'].'</td></tr>';
	$data .= "</table>";	
}
else if($task=='meritType')
{				
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Type'].'</td>
								</tr>';
				$data .='<tr class="tablerow1">
					 <td>1</td>
					 <td>'.$eDiscipline['Setting_GoodConduct'].'</td>
					 </tr>
					 <tr class="tablerow1">
					 <td>-1</td>
					 <td>'.$eDiscipline['Setting_Misconduct'].'</td>
					 </tr>';
	$data .="</table>";	
}else if($task=='status')
{				
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Setting_Status'].'</td>
								</tr>';
				$data .='<tr class="tablerow1">
					 <td>1</td>
					 <td>'.$eDiscipline['Setting_Status_Using'].'</td>
					 </tr>
					 <tr class="tablerow1">
					 <td>2</td>
					 <td>'.$eDiscipline['Setting_Status_NonUsing'].'</td>
					 </tr>';
	$data .="</table>";	
}
else if($task=='templateID')
{			
	$sql = "SELECT CategoryID, TemplateID, Title FROM INTRANET_NOTICE_MODULE_TEMPLATE WHERE Module='".$ldiscipline->Module."' AND RecordStatus=1 ORDER BY CategoryID, Title";
	$result = $ldiscipline->returnArray($sql, 2);
		
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$i_Discipline_System_Discipline_Reason_For_Issue.'</td>
									<td>'.$eDiscipline['Subject'].'</td>
								</tr>';
		for($i=0; $i<sizeof($result); $i++) {
				$data .='<tr class="tablerow1">
					 <tr class="tablerow1">
					 <td>'.$result[$i][1].'</td>
					 <td>'.$result[$i][0].'</td>
					 <td>'.$result[$i][2].'</td>
					 </tr>';
		}
	$data .="</table>";	
}
else if($task=='category' || $task=='category2')
{			
	// [2015-0518-1013-22207] add condition to filter default categories 
	//$default_cat_cond = $task=='category2'? " AND CategoryID NOT IN (1, 2, 3) ": "";
	$default_cat_cond = $task=='category2'? " AND CategoryID NOT IN (1, 2) ": "";
	
	$sql = "SELECT CategoryID, Name FROM DISCIPLINE_ACCU_CATEGORY WHERE RecordStatus=1 $default_cat_cond ORDER BY Name";
	$result = $ldiscipline->returnArray($sql, 2);
		
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$eDiscipline['Setting_CategoryName'].'</td>
								</tr>';
		for($i=0; $i<sizeof($result); $i++) {
				$data .='<tr class="tablerow1">
					 <tr class="tablerow1">
					 <td>'.$result[$i][0].'</td>
					 <td>'.$result[$i][1].'</td>
					 </tr>';
		}
	$data .="</table>";	
}
// [2015-0128-1044-22170] / [2015-0313-1420-56054]
else if($task=='defaultDetention'){	
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$Lang['eDiscipline']['DefaultSetDetention'].'</td></tr>';
				$data .='<tr class="tablerow1">
					 <td>0</td>
					 <td>'.$i_general_no.'</td></tr>
					 <tr class="tablerow1">
					 <td>1</td>
					 <td>'.$i_general_yes.'</td></tr>';
	$data .= "</table>";	
} 
// [2015-0127-1101-48073]
else if($task=='defaultNotice'){	
	$data = '<table width="98%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
		$data .='<tr class="tabletop">
									<td>#</td>
									<td>'.$Lang['eDiscipline']['DefaultSendNotice'].'</td></tr>';
				$data .='<tr class="tablerow1">
					 <td>0</td>
					 <td>'.$i_general_no.'</td></tr>
					 <tr class="tablerow1">
					 <td>1</td>
					 <td>'.$i_general_yes.'</td></tr>';
	$data .= "</table>";	
}

$output = '<table width="70%" border="0" cellpadding="0" cellspacing="0">
			  <tr>
				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
					<td width="19" height="19"><a href="javascript:Hide_Window(\'ref_list\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
				  </tr>
				</table></td>
			  </tr>
			  <tr>
				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
					  
					  <tr>
						<td height="150" align="left" valign="top">
						'.$data.'
						
						</td></tr>
							  <tr>
						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
						      <tr>
							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
						      </tr>
						     </table></td>
						     </tr>
                 </table></td>
					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
				  </tr>
				  <tr>
					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
				  </tr>
					</table></td>
			  </tr>
			</table>';
echo $output;
?>
