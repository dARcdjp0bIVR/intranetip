<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libucc.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

//$NoticeNumber = standardizeFormPostValue($_POST['TextNumber']);
$TextTitle =  standardizeFormPostValue($_POST['TextTitle']);
$TextContent = standardizeFormPostValue($_POST['TextContent']);

// $StudentIDAry = explode(",",$_POST['StudentID']);
// $SizeofStudent = count($StudentIDAry);

$libuser = new libuser($UserID);

$lc = new libucc();
$lnotice = new libnotice();

$Module = $libenroll->ModuleTitle;
$NoticeNumber = time();

$Variable = '';
$IssueUserID = $UserID;
$IssueUserName = $libuser->StandardDisplayName;
$TargetRecipientType = "U";

$Subject = $lc->setSubject($TextTitle);
$Content = $lc->setDescription($TextContent);

$emailNotify = '0';

// $ct = 1;
//foreach($StudentIDAry as $k=>$StudentID){
//	$TemplateID = ${"SelectNotice".$ct};

	$RecipientID = $_POST['StudentID'];
	$RecordType = NOTICE_TO_SOME_STUDENTS;
	$TargetType = "S";

	$ReturnArr = $lc->setNoticeParameter($Module, $NoticeNumber, '', $Variable, $IssueUserID, $IssueUserName, $TargetRecipientType, $RecipientID, $RecordType);	

	
	$NoticeID = $lc->sendNotice($emailNotify,'S');
	
//	$ct++;
//}

$xmsg = $Lang['eEnrolment']['SysRport']['NoticeSendSuccess'];
echo "<script>alert (\"".$xmsg." \"); js_Hide_ThickBox(); </script>";
intranet_closedb();
?>
