<?php
//using: 
##### Change Log [Start] #####
#
#	Date	:	2019-08-28  Tommy
#	- change $report_content
#
#	Date	:	2015-05-18	Omas
#	- Create this page for $sys_custom['eEnrolment']['kyc']['notificationLetter']	
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

$releaseDate = $_REQUEST['releaseDate'];
$teacherName = $_REQUEST['teacherName'];
$replyDate = $_REQUEST['replyDate'];




$time=strtotime($releaseDate);
$releaseDay=date("j",$time);
$releaseMonth=date("n",$time);
$releaseYear=date("Y",$time);

$replyTime=strtotime($replyDate);
$replyDay=date("j",$replyTime);
$replyMonth=date("n",$replyTime);


intranet_opendb();

$libenroll = new libclubsenrol();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);
$UID = IntegerSafe($UID);

$memberList = $UID;

$page_breaker = "<P CLASS='breakhere'>";

//get group/activity title
if (substr($typeText, 0, 4) == "club")
{
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)) && (!$ligroup->hasAdminBasicInfo($UserID))  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	{
		//header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
		No_Access_Right_Pop_Up();
		exit;
	}
	
	$sql = "SELECT TitleChinese FROM INTRANET_GROUP WHERE GroupID = '$GroupID'";
	$result = $libenroll->returnVector($sql);
	$title = $result[0];
	
	//$LetterContent = str_replace('<!--Type-->', $Lang['eEnrolment']['Club'], $Lang['eEnrolment']['NotificationLetter']['Content']);
}
else
{
	$title = $libenroll->GET_EVENT_TITLE($EnrolEventID);	
}


//get student info
$namefield = getNameFieldByLang();
if(sizeof($memberList)>0){
	$id_list = implode(",",$memberList);
	$sql = "SELECT EnglishName, ChineseName, ClassName, ClassNumber FROM INTRANET_USER WHERE UserID IN ($id_list) ORDER BY ClassName, ClassNumber";
	$memberName = $libenroll->returnArray($sql,4);
}

//get school data
//$school_data = split("\n",get_file_content("$intranet_root/file/school_data.txt"));
$school_name = '救恩書院';

//construct table content
$report_content = "";
for($i=0;$i<sizeof($memberList);$i++){
	$LetterContent = '　　本年度　貴子弟 ( '.$memberName[$i]['ClassName'].' '.$memberName[$i]['ChineseName'].' ) 已入選參加'.$title.'。家長及學生可參閱日前派發的「聯課活動資料一覽表」，以檢視該學會的詳情。為鼓勵同學積極及依時出席參與聯課活動，請家長及學生注意以下事項：';
	
		$report_content .= "<table align='center' width='700' style='page-break-after: always;'>";
		$report_content .= "<tr>";
		$report_content .= "<td>";
	// Letter Content
	$report_content .= "<table class='header' border='0' width='700' cellspacing='0' align='center'>";
		$report_content .= "<tr><th align='center'><p style='font-size: 18pt;'>$school_name</p></th></tr>";
		$report_content .= "<tr><th align='center'></th></tr>";
	$report_content .= "</table>";
	$report_content .= "<table class=\"content\" border=\"0\" width=\"100%\" cellspacing=\"3\" align=\"center\">";
		$report_content .= "<tr><td colspan='2'>敬啟者：</td></tr>";	
		$report_content .= "<tr><td colspan='2'>".$LetterContent."</td></tr>";
		//$report_content .= "<tr><td align='right' style='white-space:nowrap;vertical-align:top;'>1.</td><td width='90%'>如同學全年出席率達 80% (請事假或病假當缺席)，本會將送贈禮物給同學以茲鼓勵。各聯課活動小組老師亦會於年終推薦一名最有責任感的同學成為本校的「責任之星」，獎狀會於結業禮頒發嘉許。</td></tr>";
		$report_content .= "<tr><td align='right' style='white-space:nowrap;vertical-align:top;'>1.</td><td width='90%'>如同學全年出席率達 80% (請事假或病假當缺席)，本會將送贈禮物給同學以茲鼓勵。各聯課活動小組老師亦會於年終推薦一名表現最好的同學獲獎，獎狀會於結業禮頒發嘉許。</td></tr>";
		//$report_content .= "<tr><td align='right' style='white-space:nowrap;vertical-align:top;'>2.</td><td width='90%'>學生必須依時出席有關活動，如學生因需出席學科測驗／留堂／補課／其他活動而要請假，須填寫「學生課業活動請假紙」，並於活動前將請假紙交予負責老師。如學生因病假或其他事故而未能於當天請假，學生須於三個上課日內親自向負責老師詳述因由。如學生沒有辦理以上請假手續，會視作「無故缺席」，校方會紀錄在案，「紀律」評級將會被降級。</td></tr>";
		$report_content .= "<tr><td align='right' style='white-space:nowrap;vertical-align:top;'>2.</td><td width='90%'>學生必須依時出席有關活動，如學生因需出席學科測驗／留堂／補課／其他活動而要請假，須填寫「學生課業活動請假紙」，並於活動前將請假紙交予負責老師。如學生因病假或其他事故而未能於當天請假，學生須於三個上課日內親自向負責老師詳述因由。如學生沒有辦理以上請假手續，會視作「無故缺席」，校方會紀錄在案，聯課活動小組評級將會被降級。</td></tr>";
		$report_content .= "</table>";
	$report_content .= "<table class=\"reply\" border=\"0\" width=\"100%\" cellspacing=\"3\" align=\"center\">";		
		$report_content .= "<tr><td colspan='2'>　　此致</td></tr>";
		$report_content .= "<tr><td colspan='2'>貴家長</td></tr>";
		$report_content .= "<tr><td colspan='2'align='right'>校長＿＿＿＿＿＿＿＿謹啟</td></tr>";
		$report_content .= "<tr><td colspan='2'align='right'><span style='text-decoration: underline'>".$releaseYear."</span>年<span style='text-decoration: underline'>".$releaseMonth."</span>月<span style='text-decoration: underline'>".$releaseDay."</span>日</td></tr>";
		$report_content .= "<tr><td colspan='2'align='center'>--------------------------------------------------------------------------------------------------------------</td></tr>";
		$report_content .= "<tr><td colspan='2'align='center'>回條</td></tr>";
		$report_content .= "<tr><td colspan='2'align='center'></td></tr>";
// 		$report_content .= "<tr><td colspan='2'>敬覆者：台函奉悉，本人 * 同意/不同意敝子弟 __＿＿＿＿＿＿__ (S ＿＿__ 班) 參加 __＿＿＿＿＿＿＿ ，並著其積極參與及依時出席有關活動。如敝子弟未能出席有關活動，必會提交「學生課業活動請假紙」給有關老師，謹此奉覆。</td></tr>";
		$report_content .= "<tr><td colspan='2'>敬覆者：台函奉悉，本人 * 同意/不同意敝子弟 <span style='text-decoration: underline'>".$memberName[$i]['ChineseName']."</span> (S <span style='text-decoration: underline'>".$memberName[$i]['ClassName']."</span> 班) 參加<span style='text-decoration: underline'> ".$title."</span>，並著其積極參與及依時出席有關活動。如敝子弟未能出席有關活動，必會提交「學生課業活動請假紙」給有關老師，謹此奉覆。</td></tr>";
		$report_content .= "<tr><td colspan='2' align='left'>　　此覆</td></tr>";
		$report_content .= "<tr><td colspan='2' align='left'>救恩書院校長</td></tr>";
		$report_content .= "<tr><td colspan='2' align='right'>學生家長/監護人簽署：＿＿＿＿＿＿＿＿</td></tr>";
		$report_content .= "<tr><td colspan='2' align='right'>學生簽署：＿＿＿＿＿＿＿＿</td></tr>";
		$report_content .= "<tr><td colspan='2' align='right'>＿＿＿＿年＿＿月＿＿日</td></tr>";
// 		$report_content .= "<tr><td align='left' border='1' nowrap style='border-top:1px solid;border-left:1px solid;border-right:1px solid;border-bottom:1px solid;'>請於__月__日前交_______老師</td><td width='600'></td></tr>";
		$report_content .= "<tr><td align='left' border='1' nowrap style='border-top:1px solid;border-left:1px solid;border-right:1px solid;border-bottom:1px solid;'>請於<span style='text-decoration: underline'>".$replyMonth."</span>月<span style='text-decoration: underline'>".$replyDay."</span>日前交<span style='text-decoration: underline'>".$teacherName."</span>老師</td><td width='600'></td></tr>";
		$report_content .= "<tr><td colspan='2' align='left' style='line-height:300%;'>*請刪去不適用者</td></tr>";
	$report_content .= "</table>";
	
	$report_content .= "</td>";
	$report_content .= "</tr>";
	$report_content .= "</table>";
}

intranet_closedb();

?>
<html>
<head>
<meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<title><?=$school_name?></title>
<style>
<!--

html, body { margin: 0px; padding: 0px; } 

@media print and (width: 21cm) and (height: 29.7cm) {
    @page {
       margin: 3cm;
    }
 }

table.content td{
	line-height:33px;
}
table.reply td{
	line-height:28px;
}

*{  font-size:14pt;
	font-family: "Times New Roman","DFKai-sb","標楷體";
 }
-->
</style>
</head>

<body>
<?= $report_content ?>

</body>

</html>