<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_opendb();

$linterface = new interface_html();
$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();

$today = date("Y-m-d");

$datetime_count = count($TargetDatetimes);

$arrDateAvailable = array();
$AvailableDateArr = array();
$arrNotAvailableDate = array();
$arrNotAvailableDateRemark = array();
for($j=0;$j<$datetime_count;$j++){
	
	$datetime_parts = explode(",",$TargetDatetimes[$j]);
	$targetDate = $datetime_parts[0];
	$StartTime = $datetime_parts[1];
	$EndTime = $datetime_parts[2];
	
	//if($targetDate < $today) continue;
	
	$display_time = $targetDate."&nbsp;".$StartTime."&nbsp;".$Lang['General']['To']."&nbsp;".$EndTime;
	
	if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $FacilityID, $_SESSION['UserID'], $targetDate))
	{
		### Available
				
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $targetDate, $targetDate, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $FacilityID);
		$numOfPeningRecord = count($curPendingRecordAry);
		$havePendingRecord = false;
		for ($i=0; $i<$numOfPeningRecord; $i++) {
			$_pendingRecordStartTime = $curPendingRecordAry[$i]['StartTime'];
			$_pendingRecordEndTime = $curPendingRecordAry[$i]['EndTime'];
			
			if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
				$havePendingRecord = true;
				break;
			}
		}
		
		if ($havePendingRecord) {
			$arrNotAvailableDate[] = $display_time;
			$arrNotAvailableDateRemark[$display_time] = $Lang['eBooking']['MakePendingBookingAlready']; 
		}
		else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$FacilityID,$targetDate,$StartTime,$EndTime))
		{
			$tempArr[0]["StartTime"] = $StartTime;
			$tempArr[0]["EndTime"] = $EndTime;
			$arrDateAvailable[$targetDate][] = $tempArr[0];
			
			$AvailableDateArr[] = $display_time;
		}else{
			$arrNotAvailableDate[] = $display_time;
		}
		
	}else{
		### Not Available
		$arrNotAvailableDate[] = $display_time;
	}
}

if(is_array($SingleItemID)){
	$SingleItemIDArr = $SingleItemID;
}else if($SingleItemID != "") {
	$SingleItemIDArr = explode(",",$SingleItemID);
} else {
	$SingleItemIDArr = array();
}

$BookingRemarks = "";

$ResponsibleUID = $_SESSION['UserID'];

//echo $RepeatSelect." - ".$RepeatTimes." - ".$RepeatValue." - ".$RepeatStart." - ".$RepeatEnd;
//debug_r($DateTimeArr);


$SettingsArr['CancelDayBookingIfOneItemIsNA'] = $CancelDayBookingIfOneItemIsNA;
$SettingsArr['FromModule'] = "eEnrolment";

if(count($arrDateAvailable)>0){
	$result = $lebooking_ui->Create_Temp_Booking_Record_iCal($FacilityID, $arrDateAvailable, $StartTime_TimeSlotID_Mapping_Arr, $SingleItemIDArr, $BookingRemarks, $ResponsibleUID, $SettingsArr);
}

intranet_closedb();
?>