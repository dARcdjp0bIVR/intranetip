<?php
// Editing by 
/*
 * 2017-09-15 (Anna): add webSAMS type and code
 * 2017-06-13 (Omas): force ApplyUserType to be uppercase -#W118563
 * 2016-07-19 (Omas): replace split to explode for PHP5.4
 * 2015-12-29 (Omas):	add support to ole default setting import if have iPortfolio
 * 2014-11-21 (Omas):	Insert Record by Change_Club_Event_GroupMapping() -  - 'A' for no limit (if A no record will add for house mapping)
 * 2014-07-09 (Carlos): $sys_custom['eEnrolment']['TWGHCYMA'] - use INTRANET_GROUP.GroupCode to get EnrolGroupOID
 * 2014-04-10 (Carlos): Added $sys_custom['eEnrolment']['TWGHCYMA'] TWGHs C Y MA MEMORIAL COLLEGE customization
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

if(!isset($_SESSION['eEnrolmentImportdata']) || count($_SESSION['eEnrolmentImportdata'])==0) {
	header("Location: import_activity_info.php?msg=ImportUnsuccess_NoRecord");
	exit;
}


$data = $_SESSION['eEnrolmentImportdata'];

$NoOfData = count($data);

for($i=0; $i<$NoOfData; $i++) {
	
	if($plugin['iPortfolio']) {
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $volunteerService, $volunteerServiceHour, $activityLocation, $schoolLocation, $externalLocation, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$intExt , $titleLang , $category , $ole_component ,$organization ,$details , $SchoolRemarks,$websamscode,$websamstype) = $data[$i];
		}else{
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$intExt , $titleLang , $category , $ole_component ,$organization ,$details , $SchoolRemarks,$websamscode,$websamstype) = $data[$i];
		}
	}
	else{
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $volunteerService, $volunteerServiceHour, $activityLocation, $schoolLocation, $externalLocation, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$websamscode,$websamstype) = $data[$i];
		}else{
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$websamscode,$websamstype) = $data[$i];
		}		
	}
	
	$EnrolGroupIDArr = array();
	
	//echo "<br><br>############### ROW $i ###################<br><br>";
	
	# No need to delete
	//$libenroll->DEL_EVENTSTUDENT($EnrolEventID);
	
	// insert / update ACTIVITY
	if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
	{
		if ($Description==strip_tags($Description))
		{
			$Description = nl2br($Description);
		}
	}
	$EventArr['Description'] = trim(stripslashes($content));
	if(STRTOUPPER($age)=="A") {
		$EventArr['LowerAge'] = 0;
		$EventArr['UpperAge'] = 0;
	} else {
		$ageArange = explode('-', $age);
		$EventArr['LowerAge'] = ($age) ? $ageArange[0] : "";
		$EventArr['UpperAge'] = ($age) ? $ageArange[1] : "";
	}
	$EventArr['Gender'] = $gender;
	$EventArr['Quota'] = $quota;
	$EventArr['EventTitle'] = $activityName;
	
	if($sys_custom['eEnrolment']['TWGHCYMA']){
		
		$EventArr['EventCategory'] = 0;
		$EventArr['IsVolunteerService'] = strtoupper($volunteerService) == 'YES' ? '1':'0';
		if($EventArr['IsVolunteerService'] == '1'){
			$EventArr['VolunteerServiceHour'] = $volunteerServiceHour;
		}
		
		$EventArr['Nature'] = $activityLocation;
		$EventArr['ActivityExtLocation'] = $externalLocation;
		
		$activityCategoryAry = explode(",",trim($activityCategory));
		$categoryIdAry = array();
		for($j=0;$j<count($activityCategoryAry);$j++) 
		{
			$sql = "SELECT CategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$libenroll->Get_Safe_Sql_Query(trim($activityCategoryAry[$j]))."'";
			$catInfo = $libenroll->returnVector($sql);
			if(count($catInfo)>0){
				$categoryIdAry[] = $catInfo[0];
			}
		}
		$EventArr['ActivityCategory'] = $categoryIdAry;
		
		$activityNatureAry = explode(",",trim($nature));
		$natureIdAry = array();
		for($j=0;$j<count($activityNatureAry);$j++) 
		{
			$sql = "SELECT NatureID FROM INTRANET_ENROL_ACTIVITY_NATURE WHERE Nature='".$libenroll->Get_Safe_Sql_Query(trim($activityNatureAry[$j]))."'";
			$natureInfo = $libenroll->returnVector($sql);
			if(count($natureInfo)>0) {
				$natureIdAry[] = $natureInfo[0];
			}
		}
		$EventArr['ActivityNature'] = $natureIdAry;
		
	}else{
		$sql = "SELECT CategoryID, OleCategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$libenroll->Get_Safe_Sql_Query(trim($activityCategory))."'";
		$catInfo = $libenroll->returnResultSet($sql);
		
		$EventArr['EventCategory'] = $catInfo[0]['CategoryID'];
		$EventArr['Nature'] = $nature;
	}
	
	$EventArr['ApplyStartTime'] = $applyPeriodStart.":00";
	$EventArr['ApplyEndTime'] = $applyPeriodEnd.":00";
	$EventArr['ApplyMethod'] = $enrolmentMethod;
	
	//2014-0612-1657-46183
	//$EventArr['EnrolGroupID'] = $EnrolGroupID;
	if (strtolower(trim($belongToClub)) == '[na]') {
		$EventArr['EnrolGroupID'] = '';
	}
	else {
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			$sql = "SELECT i.EnrolGroupID FROM INTRANET_GROUP g INNER JOIN INTRANET_ENROL_GROUPINFO i ON (i.GroupID=g.GroupID and g.GroupCode='$belongToClub') WHERE g.GroupCode='$belongToClub'";
			$rs = $libenroll->returnVector($sql);
			$enrol_group_id = $rs[0];
			$EventArr['EnrolGroupID'] = $enrol_group_id;
		}else{
			$EventArr['EnrolGroupID'] = $belongToClub;
		}
	}
	
	if(STRTOUPPER($fee)=="TBC") {
		$EventArr['PaymentAmount'] = 0;
		$EventArr['isAmountTBC'] = 1; 
	} else {
		$EventArr['PaymentAmount'] = $fee;
		$EventArr['isAmountTBC'] = 0; 
	}
	
	$EventArr['GroupID'] = "";
	$EventArr['AcademicYearID'] = $AcademicYearID;
	$EventArr['ActivityCode'] = trim($activityCode);
	
	$EventArr['WebSAMSSTACode'] = $websamscode;
	$EventArr['WebSAMSSTAType'] = $websamstype;

	/*	
	if ($libenroll->IS_EVENTINFO_EXISTS($EnrolEventID)) {
		$EnrolEventID = $libenroll->EDIT_EVENTINFO($EventArr);
	} else {
	*/
		$EnrolEventID = $libenroll->ADD_EVENTINFO($EventArr);
	//}
	
	// insert Class Level
	$libenroll->DEL_EVENTCLASSLEVEL($EnrolEventID);
	
	if($targetForm =="A"){
		$libenrolllass = new libclass();
		$ClassLvltempArr = $libenrolllass->getLevelArray();
		foreach($ClassLvltempArr as $temp){
			$ClasslvlArr['ClassLevelID'] = trim($temp[0]);
			$ClasslvlArr['EnrolEventID'] = $EnrolEventID;
			$libenroll->ADD_EVENTCLASSLEVEL($ClasslvlArr);
		}
		
	}
	else{
		$ClasslvlArr['EnrolEventID'] = $EnrolEventID;
		$targetFormAry = explode(',',$targetForm);
		for ($a = 0; $a < sizeof($targetFormAry); $a++) {
			$sql = "SELECT YearID FROM YEAR WHERE YearName='".$libenroll->Get_Safe_Sql_Query(trim($targetFormAry[$a]))."'";
			$yearId = $libenroll->returnVector($sql);
					
			$ClasslvlArr['ClassLevelID'] = $yearId[0];
			$libenroll->ADD_EVENTCLASSLEVEL($ClasslvlArr);
		}
	}
	
	#Add house mapping
	if($targetHouse != "A"){
		$targetHouseAry = explode(',',$targetHouse);
		$targetHouseAry = array_map('trim', $targetHouseAry);
		$libenroll->Change_Club_Event_GroupMapping($enrolConfigAry['Activity'],$EnrolEventID, $targetHouseAry);
	}
	
	/**************** assign staff ******************/
	$libenroll->DEL_EVENTSTAFF($EnrolEventID);
	
	$PICArr['EnrolEventID'] = $EnrolEventID;
	$PICArr['StaffType'] = "PIC";
	$picAry = explode(',',$pic);
	for ($a = 0; $a < sizeof($picAry); $a++) {
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$libenroll->Get_Safe_Sql_Query(trim($picAry[$a]))."'";
		$uid = $libenroll->returnVector($sql);
		$PICArr['UserID'] = $uid[0];
		$libenroll->ADD_EVENTSTAFF($PICArr);
	}
	
	$PICArr['StaffType'] = "HELPER";
	$HelperAry = explode(',',$helper);
	for ($a = 0; $a < sizeof($HelperAry); $a++) {
		$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".$libenroll->Get_Safe_Sql_Query(trim($HelperAry[$a]))."'";
		$uid = $libenroll->returnVector($sql);
		$PICArr['UserID'] = trim($uid[$a]);
		$libenroll->ADD_EVENTSTAFF($PICArr);
	}
	
	/******************** Set Right *********************/
	
	$StudentArr['EnrolEventID'] = $EnrolEventID;
	$StudentArr['ApplyUserType'] = strtoupper($setting);
	$libenroll->UPDATE_APPLY_USERTYPE($StudentArr);
		
	
	/******************* Set Schedule *******************/
	// assign schedule
	
	if(trim($schedule)!="") {
		$dataAry = array();
		$scheduleAry = explode(',',$schedule);
		for($a=0; $a<count($scheduleAry); $a++) {
			unset($dataAry);
			$dataAry['EnrolEventID'] = $EnrolEventID;
			$dataAry['ActivityDateStart'] = substr(trim($scheduleAry[$a]),0,16).":00";
			$dataAry['ActivityDateEnd'] = substr(trim($scheduleAry[$a]),0,11).substr(trim($scheduleAry[$a]),17).":00";
			//debug_pr($dataAry);
			$SuccessArr['Assign_Club_Schedule'][] = $libenroll->ADD_ENROL_EVENT_DATE($dataAry);
		}
	}	
	
//	if($plugin['iPortfolio'] && $catInfo[0]['OleCategoryID'] != 0) {
//		$newOleSetting = array();
//		$newOleSetting['CategoryID'] = $catInfo[0]['OleCategoryID'];
//		$SuccessArr['DefaultOleSettings'][] = $libenroll->Update_Enrolment_Default_OLE_Setting('activity', $EnrolEventID, $newOleSetting);		
//	}
	if ($plugin['iPortfolio']) {
		$newOleSetting = array();
		$newOleSetting['IntExt'] = $intExt;
		$newOleSetting['TitleLang'] = $titleLang;
		if($category == '' && $catInfo[0]['OleCategoryID'] != 0){
			$category = $catInfo[0]['OleCategoryID'];
		}
		$newOleSetting['CategoryID'] = $category;
		
		if($intExt=='EXT'){
			$newOleSetting['OLE_Component'] = '';
		}
		else{
			$newOleSetting['OLE_Component'] = $ole_component;
		}
		$newOleSetting['Organization'] = trim(stripslashes($organization));
		$newOleSetting['Details'] = trim(stripslashes($details));
		$newOleSetting['SchoolRemark'] = trim(stripslashes($SchoolRemarks));
		
		$libenroll->Update_Enrolment_Default_OLE_Setting('activity', $EnrolEventID, $newOleSetting);
	}
}

unset($_SESSION['eEnrolmentImportdata']);

header("Location: import_activity_info.php?msg=ImportSuccess");

?>
