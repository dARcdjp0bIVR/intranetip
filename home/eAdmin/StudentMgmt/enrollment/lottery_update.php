<?php
## Using By: 
######################################
##	Modification Log:
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition
######################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();
global $debug_sql;
if ($plugin['eEnrollment'])
{

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$MODULE_OBJ['title'] = $eEnrollment['drawing'];
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    $linterface = new interface_html("popup.html");

	    ################################################################

if ($libenroll->hasAccessRight($_SESSION['UserID']))
{

}
else
{
    echo "You have no priviledge to access this page.";
    exit();
}

# Grab min requirement to temp table
$minmax = $libenroll->getFullMinMax();

$lclass = new libclass();

if($libenroll->UseCategorySetting)
{
	$CategoryIDArr = $libenroll->GET_CATEGORY_LIST();
	$CategoryIDArr = Get_Array_By_Key($CategoryIDArr, "CategoryID");
}
else
	$CategoryIDArr = array(0); // means do not use cateory setting

$CategorySetting = $libenroll->Get_Category_Enrol_Setting($CategoryIDArr);

$sql = "CREATE TEMPORARY TABLE TEMP_ENROL_REQUIRE(
         StudentID int(11),
         Min int(11)
         )";
$libenroll->db_db_query($sql);


foreach($CategoryIDArr as $thisCategoryID)
{
	// clear for each category
	$sql = "DELETE FROM TEMP_ENROL_REQUIRE";
	$libenroll->db_db_query($sql);
	
	//$overallMax = $libenroll->defaultMax;
	$overallMax = $CategorySetting[$thisCategoryID]['Club']['DefaultMax'];
	for ($i=0; $i<count($minmax); $i++)
	{
	     list($id,$min,$max) = $minmax[$i];
	     if ($max == "") continue;
	     if ($max > $overallMax) $overallMax = $max;
	     // - if ($max < $overallMax) $overallMax = $max;
	     
	     # Grab classname for this form
	     $classes = $lclass->returnClassListByLevel($id);
	     if(count($classes))
	     {
		     $delimiter = "";
		     $list = "";
		     for ($j=0; $j<count($classes); $j++)
		     {
		          list($id,$name) = $classes[$j];
		          $list .= "$delimiter '$name'";
		          $delimiter = ",";
		     }
		     $sql = "INSERT IGNORE INTO TEMP_ENROL_REQUIRE (StudentID,Min)
		             SELECT UserID, $min FROM INTRANET_USER WHERE RecordType = 2 AND ClassName IN ($list) AND ClassName != ''";
		     $libenroll->db_db_query($sql);//20110823
	     }
	}
	
	if ($overallMax == 0)
	{
		if($thisCategoryID==0)
	    	$sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT"; 
	    else
	    	$sql = "SELECT MAX(Max) FROM INTRANET_ENROL_STUDENT_CATEGORY_ENROL WHERE CategoryID = '$thisCategoryID' ";
	    $result = $libenroll->returnVector($sql);
	    if ($result[0]!=0)
	    {
	        $overallMax = $result[0];
	    }
	}
	
	# Grab unsatisfied school requirement 
	$unsatSchool = $libenroll->Get_Student_Unsatisfy_School_Requirement($thisCategoryID);
	
	
	$i = 0;
	
	while (count($unsatSchool)>0 && $i<$overallMax)
	{	   
		
			$i++;

	       if($WithPriority)
	       {
	       		$m = 1; 
	       		$Loop = $overallMax;
	       }
	       else
	       {
	       		$m = 0;
	       		$Loop = 0;
	       }
			
			for($m; $m<=$Loop; $m++) {	# check priority first
				
				# Retrieve Groups with spaces
				$groups = $libenroll->Retrieve_Groups_With_Spaces_For_Lottery($thisCategoryID, $m);
				
				$libenroll->Lottery_Arrange_Students_To_Groups($groups, $unsatSchool, $overallMax, $i, $thisCategoryID, $SuccessArr);
				
			}
			# Grab unsatisfied school requirement again
			$unsatSchool = $libenroll->Get_Student_Unsatisfy_School_Requirement($thisCategoryID);
			
	}
	
	
	
	# Now have to match students' own requirement
	$unsatOwn = $libenroll->Get_Student_Unsatisfy_Max_Want($thisCategoryID);
	
	$i = 0;
	
	while (count($unsatOwn)>0 && $i<$overallMax)
	{
	       $i++;
	       
	       if($WithPriority)
	       {
	       		$m = 1; 
	       		$Loop = $overallMax;
	       }
	       else // 
	       {
	       		$m = 0;
	       		$Loop = 0;
	       }
	       
	       for($m; $m<=$Loop; $m++) {	# check priority first
		       
				# Retrieve Groups with spaces
				$groups = $libenroll->Retrieve_Groups_With_Spaces_For_Lottery($thisCategoryID, $m);
				
				$libenroll->Lottery_Arrange_Students_To_Groups($groups, $unsatOwn, $overallMax, $i, $thisCategoryID, $SuccessArr);
	
			}
			# Grab unsatisfied own requirement again
			$unsatOwn = $libenroll->Get_Student_Unsatisfy_Max_Want($thisCategoryID);
	}
}

$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 1 WHERE RecordStatus = 0";
$libenroll->db_db_query($sql);

// send email
$laccess = new libaccess();
if ($laccess->retrieveAccessCampusmailForType(2) && $libenroll->onceEmail == 1)     # if can access campusmail
{	
	//get all groups
	$groupArr = $libenroll->getGroupInfoList($isAll=1, $Semester='', $EnrolGroupIDArr='', $getRegOnly=1);
	$numOfClub = count($groupArr);
	
	// loop through all groups
	for ($j=0; $j<$numOfClub; $j++)
	{
		$thisEnrolGroupID = $groupArr[$j]['EnrolGroupID'];
		$thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
						
		//check if the club is using the enrolment system
		/* checked in function getGroupInfoList() already
		$sql = "SELECT Quota FROM INTRANET_ENROL_GROUPINFO WHERE GroupID = '$thisGroupID'";
		$result = $libenroll->returnArray($sql,1);
		if ($result[0][0] == "")
		{
			//ignore clubs that are nor using enrolment system
			continue;
		}
		*/
				
		//send email to all students who have applied for this club
		$sql = "SELECT StudentID, RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$thisEnrolGroupID'";
		$students = $libenroll->returnArray($sql,1);
		$numOfClubMember = count($students);	
	
		for ($i=0; $i<$numOfClubMember; $i++)
		{
			$thisStudentID = $students[$i]['StudentID'];
			$thisRecordStatus = $students[$i]['RecordStatus'];
			
			# define record type
			$RecordType = "club";
			
			# get student list
			$userArray = array();			
			$userArray[] = $students[$i][0];
			
			# get success result
			$successResult = false;
			
			// commented by Ivan 20100226 - Select the RecordStatus in previous SQL so that less SQL is run
			//$sql = "SELECT RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE GroupID = '$GroupID' AND StudentID = '".$students[$i][0]."'";
			//$choices = $libenroll->returnArray($sql,1);
			
			if ($thisRecordStatus == 2) {
				$successResult = true;
			} else if ($thisRecordStatus == 1){
				$successResult = false;
			} else {
				// do nothing
			}
			
			$libenroll->Send_Enrolment_Result_Email($userArray, $thisGroupID, $RecordType, $successResult);
		}
		//debug_r($recipient);
		
	}
}


intranet_closedb();


################################################################

$linterface->LAYOUT_START();

?>

<div style="height: 400px;">
<br/><br/><br/><br/>
<?=$i_ClubsEnrollment_LotteryFinished?><br/><br/>
<?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
</div>
<script type="text/javascript">
	window.opener.location.reload();
</script>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>