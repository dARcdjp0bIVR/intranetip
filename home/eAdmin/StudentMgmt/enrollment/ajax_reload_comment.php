<?php
// using : 

/***********************************************
 *	Date:	2014-11-13 (Omas)
 *		Comment Bank all move to front-end and move into DB
 *		modified to reload comment from DB and new type of Teacher comments
 * 
 * Date: 	2013-04-17 (Rita)
 * Details: Create this page for loading comment 
 ***********************************************/
 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_opendb();

$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();

$type = $_POST['type'];
$commentId = $_POST['commentId'];


if($type == 'PerformanceComment')
{
//	include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
//	$lword = new libwordtemplates();
//	$words = $lword->getWordList(8);
//	$thisComment = $words[$commentId];
	$getPerformanceSQL= $libenroll->Get_Achievement_Comment_Sql($commentId);
	$PerformanceArr = $libenroll->returnArray($getPerformanceSQL);
	$thisComment = $PerformanceArr[0]['Comment'];
	
}
elseif($type == 'AchievementComment')
{
//	$achievementDataSql = $libenroll->Get_Achievement_Comment_Sql($commentId);
//	$achievementCommentDataArray = $libenroll->returnArray($achievementDataSql);		
//	$thisComment = $achievementCommentDataArray[0]['Comment'];
	$achievementDataSql = $libenroll->Get_Achievement_Comment_Sql($commentId);
	$achievementCommentDataArray = $libenroll->returnArray($achievementDataSql);
	$thisComment = $achievementCommentDataArray[0]['Comment'];
	
}
elseif($type == 'commentComment'){
	$commentDataSql = $libenroll->Get_Achievement_Comment_Sql($commentId);
	$commentCommentDataArray = $libenroll->returnArray($commentDataSql);	
	$thisComment = $commentCommentDataArray[0]['Comment'];
}



		

		


	
echo $thisComment;

intranet_closedb();

?>