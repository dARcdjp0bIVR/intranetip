<?php
//using: Rita

/*******************************************
 * Date:	2013-02-08 (Rita)
 * Details: add three columns - English Name, Chinese Name, and Student ID
 * 
 * Date:	2012-12-13 (Rita)
 * Details:	Copy from eSurvey, and amend for activity survey customization
 *******************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libsurvey.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# Temp Assign memory of this page
ini_set("memory_limit", "150M"); 

$lexport = new libexporttext();
$libenroll = new libclubsenrol($AcademicYearID);



if($_POST['EnrolEventID']){
	$eEnrolRecordID = $_POST['EnrolEventID'];
	$filter = $_POST['filter'];
	$keyword = $_POST['keyword'];
	if ($keyword == $eEnrollment['enter_student_name'] || $keyword == $eEnrollment['enter_name']) $keyword = "";

	
	# Survey Question 
	$surveyQuestionInfoArr = $libenroll->GET_EVENTINFO($eEnrolRecordID);
	
	# Student Info
	$enrollEventInfoArrSql = $libenroll->Get_Management_Activity_Participant_Enrollment_Sql(array($eEnrolRecordID), array($filter), $keyword, $ForExport=1);
	$enrollEventInfoArr = $libenroll->returnArray($enrollEventInfoArrSql);

	$numOfStudent = count($enrollEventInfoArr);

	$thisStudentIDArr = array();
	for($i=0;$i<$numOfStudent;$i++){
		$thisStudentID = $enrollEventInfoArr[$i]['StudentID'];
		$thisStudentIDArr[]= $thisStudentID;
	}
	
}else{
	// for club
}	



$lsurvey = new libsurvey($SurveyID);

$exportColumn = array();
$ExportArr = array();
$thisRow = array();

$queString = $surveyQuestionInfoArr['Question'];
$question_array = $lsurvey->splitQuestion($queString);
$answers = $libenroll->returnUserSurveyAnswerInfo($thisStudentIDArr, $eEnrolRecordID);
 
$surveyUserIDArr = array_keys(BuildMultiKeyAssoc($answers, 'UserID'));

$libuser = new libuser();
$userInfoArr = $libuser->returnUser('', '', $surveyUserIDArr);
$userInforAssoArr = BuildMultiKeyAssoc($userInfoArr, 'UserID');


### Export Column ###
$exportColumn[] = "English Name";
$exportColumn[] = "Chinese Name";
$exportColumn[] = "Student ID";
$exportColumn[] = "Class";
$exportColumn[] = "Class Number";

# Loop Each Question
for ($j=0; $j<sizeof($question_array); $j++)
{
      $exportColumn[] = str_replace("\r\n"," ",intranet_undo_htmlspecialchars($lsurvey->question_array[$j][1]));
}
$exportColumn[] = "Date of filling";
    
    
### Export Row ###
$numOfAnswers = count($answers);
if($numOfAnswers > 0){
	for ($i=0; $i<$numOfAnswers; $i++)
	{
		$thisRow = array();     
		$thisUserId =  $answers[$i]['UserID'];
		
		$thisUserLogin = $userInforAssoArr[$thisUserId]['UserLogin'];
		$thisEnglishName = $userInforAssoArr[$thisUserId]['EnglishName'];
		$thisChineseName = $userInforAssoArr[$thisUserId]['ChineseName'];
		
		$name = $answers[$i]['UserName'];
		$class = $answers[$i]['ClassName'];
		$classnumber = $answers[$i]['ClassNumber'];
		$astr = $answers[$i]['Answer'];
		$fillTime = $answers[$i]['DateModified'];
		$recordType = $answers[$i]['RecordType'];
		 
		$parsed = $lsurvey->parseAnswerStr($astr);
	  
	  	# Data Row
		$thisRow[] = $thisEnglishName;
		$thisRow[] = $thisChineseName;
		$thisRow[] = $thisUserLogin;
		$thisRow[] = $class;
		$thisRow[] = $classnumber;
		  
		for ($j=0; $j<sizeof($question_array); $j++)
		{
			$thisRow[] = $parsed[$j];
		}
		
		$thisRow[] = $fillTime;
		$ExportArr[] = $thisRow;
	}
}else{
	$thisRow[] = $Lang['General']['NoRecordFound'] ;
}
  
  
$export_content .= $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, "\t", "\r\n", "\t", 0);

intranet_closedb();
      
$filename = "surveyresult_$SurveyID.csv";
$lexport->EXPORT_FILE($filename, $export_content);
     
?>