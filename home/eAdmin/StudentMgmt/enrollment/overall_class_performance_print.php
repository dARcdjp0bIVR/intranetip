<?php
# using: yat

############################################
#
#
############################################
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

$linterface = new interface_html();
$lclass = new libclass();

//check if the user is a class teacher if the user is not a enrol admin/master
if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$libTeaching = new libteaching($_SESSION['UserID']);
	$result = $libTeaching->returnTeacherClass($_SESSION['UserID'], Get_Current_Academic_Year_ID());
	
	if (count($result)==0)
	{
		$isClassTeacher = false;
	}
	else
	{
		$isClassTeacher = true;
		$targetClassID = $result[0][0];
	}
}
if ($isClassTeacher)
{
	$TitleField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEn');
	//get the class name
	$sql = "SELECT 
					yc.$TitleField, ycu.ClassNumber 
			FROM 
					YEAR_CLASS as yc
					INNER JOIN
					YEAR_CLASS_USER as ycu
					On (yc.YearClassID = ycu.YearClassID)
			WHERE 
					yc.YearClassID = '$targetClassID'
			Order By 
					ycu.ClassNumber
			";
	$result = $libenroll->returnArray($sql,1);
	$classSelection = $result[0][0];
	$targetClass = $result[0][0];
}
else
{ 
	$FormSelection = $lclass->getSelectLevel("name=\"targetFormID\" onChange='document.form1.submit();'", $targetFormID, 1, $Lang['SysMgr']['FormClassMapping']['All']['Form'], Get_Current_Academic_Year_ID());
}

##### retrieve class list
if(empty($targetFormID))
{
	$this_class_list = $lclass->getClassList();
	$FormDisplayName = $Lang['SysMgr']['FormClassMapping']['All']['Form'];
}
else
{
	$ThisFormClassAry = $lclass->returnClassListByLevel($targetFormID);
	if(!empty($ThisFormClassAry))
	{
		for($i=0;$i<sizeof($ThisFormClassAry);$i++)
		{
			list($tmp_YearClassID, $tmp_ClassTitleEn, $tmp_ClassTitleB5) = $ThisFormClassAry[$i];
			
			$this_class_list[$i]['ClassID'] = $tmp_YearClassID;
			$this_class_list[$i]['ClassName'] = $tmp_ClassTitleEn;
		}
	}
	$form_con = " d.YearID = '". $targetFormID."' and ";
	$FormDisplayName = $lclass->getLevelName($targetFormID);
}
$class_performance_ary = array();
# club performance
$club_sql = "SELECT 
				d.YearClassID, sum(a.Performance)
			FROM 
				INTRANET_USERGROUP as a 
				LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
				left join INTRANET_USER as c on (c.UserID=a.UserID)
				left join YEAR_CLASS as d on (c.ClassName=d.ClassTitleEN and d.AcademicYearID='".Get_Current_Academic_Year_ID()."')
			WHERE  
				$form_con
				c.ClassName <> '' and 
				b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			group by 
				d.YearClassID
			";
$club_result = $libenroll->returnArray($club_sql);
if(!empty($club_result))
{
	foreach($club_result as $k=>$d)
	{
		$class_performance_ary[$d[0]] += $d[1];
	}
}
# act performance
$act_sql = "SELECT 
			d.YearClassID, sum(a.Performance)
		FROM 
			INTRANET_ENROL_EVENTSTUDENT as a 
			left join INTRANET_USER as c on (c.UserID=a.StudentID)
			left join YEAR_CLASS as d on (c.ClassName=d.ClassTitleEN and d.AcademicYearID='".Get_Current_Academic_Year_ID()."')
		where 
			$form_con
			c.ClassName <> '' 
		group by 
			d.YearClassID
";
$act_result = $libenroll->returnArray($act_sql);
if(!empty($act_result))
{
	foreach($act_result as $k=>$d)
	{
		$class_performance_ary[$d[0]] += $d[1];
	}
}

$display = "<div class='table_board'>";
$display .="<table class='common_table_list_v30 view_table_list_v30'>";
$display .= "<tr>";
$display .= "<th>".$i_UserClassName."</th>";
$display .= "<th style=\"text-align:center\">".$Lang['eEnrolment']['perform_score_comment']."</th>";
$display .= "</tr>";

$SumOver = 0;
for ($i=0; $i<sizeof($this_class_list); $i++)
{
	$this_pf = $class_performance_ary[$this_class_list[$i]['ClassID']] ? $class_performance_ary[$this_class_list[$i]['ClassID']] : 0;
	$display .= '<tr>';
	$display .= '<td valign="top">'.$this_class_list[$i]['ClassName'].'</td>';
	$link = "overall_performance_report.php?showTarget=class&targetClassID=".$this_class_list[$i]['ClassID'];
	$display .= '<td align="center" valign="top">'. $this_pf .'</td>';
	$display .= '</tr>';
	
	$SumOver += $this_pf;
}

// Display Sum Overall Performance
$display .= '<tr>';
$display .= '<td align=right>'. $Lang['eEnrolment']['total_overall_perform'] .'</td>';
$display .= '<td align="center" valign="top"><b>'.$SumOver.'</b></td>';
$display .= '</tr>';

$display .= '</table>';
$display .= "</div>"; 				
				
?>


<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
	
<table class="form_table_v30">
	<tr>
		<td nowrap="nowrap" class="field_title"><?=$Lang['AccountMgmt']['Form']?></td>
		<td><?= $FormDisplayName ?></td>
	</tr>
</table>
    
<?=$display?>
		

<?
intranet_closedb();
?>