<?php
# using:   
/*
 * Change Log:
 * 2019-10-28 Henry 
 *  - bug fix for Club PICs fail to set schedule of activities [Case#E174088]
 * 2019-02-22 (Cameron)
 *      - pass MeetingDateType to UpdateEnrolEbookingRelation
 * 2019-02-21 (Cameron)
 *      - delete eBooking if $isEnableEnroleBooking
 * 2017-05-10 (Villa) #K108095 
 * -	 Add enrol-eBooking Relatied
 * 2017-04-27 (Villa)
 * -	Add $sys_custom['eEnrolment']['EnableLocation_tlgcCust'] to save Location field
 * 2016-07-26 (Cara)
 * -	Save change log in MODULE_RECORD_DELETE_LOG 
 * 2014-07-22 (Bill)
 * -	Continue to member settting for new group/activity setting
 * 2014-04-09 (Carlos)
 * -	 $sys_custom['eEnrolment']['TWGHCYMA'] for TWGHs C Y MA MEMORIAL COLLEGE customization - added location booking function
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");
include_once($PATH_WRT_ROOT."includes/liblog.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
$_POST['Semester'] = str_replace(array('<', '>', ';', 'javascript:', '"', '\''),'',$_POST['Semester']);
$Semester = trim(intranet_undo_htmlspecialchars(stripslashes($_POST['Semester'])));

$recordId = IntegerSafe($_POST['recordId']);
$recordType = $_POST['recordType'];
$pic_view = $_POST['pic_view'];
$dateInfoAry = $_POST['dateInfoAry'];
$TimeDescription = trim(stripslashes($_POST['TimeDescription']));
$Module= 'eEnrolment';
$Section= "Save_Meeting_Date_$recordType";
$RecordTitle = $_SESSION['RecordTitle'];
$Action = Update;


$LibUser = new libuser($_SESSION['UserID']);
$libenroll = new libclubsenrol($AcademicYearID);
$liblog = new liblog();
$json =  new JSON_obj();


####################################################
########## Access Right Checking [Start] ###########
####################################################
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$isPic = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $recordId, $recordType);
$isEnableEnroleBooking = $libenroll->CheckEnableEnroleBooking();

$canAccess = true;
if ($recordType == $enrolConfigAry['Club']) {
	$disableUpdateSettings = $libenroll->disableUpdate;
	$TableName='INTRANET_ENROL_GROUP_DATE';
	
	if (
			(!$plugin['eEnrollment'] && !$isEnrolAdmin && !$isEnrolMaster && !$isPic)
			|| $disableUpdateSettings
			|| !$libenroll->AllowToEditPreviousYearData
	) {
		$canAccess = false;
	}
	
	### Get Club specific data
	$originalMeetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($recordId);
	$dateIdKey = 'GroupDateID';
	$backUrl = 'group.php';
}
else if ($recordType == $enrolConfigAry['Activity']) {
	$disableUpdateSettings = $libenroll->Event_DisableUpdate;
	$TableName='INTRANET_ENROL_EVENT_DATE';
	
	$activityInfoAry = $libenroll->GET_EVENTINFO($recordId);
	$activityEnrolGroupID = $activityInfoAry['EnrolGroupID'];
	$isActivityClubPic = $libenroll->IS_CLUB_PIC($activityEnrolGroupID);
	if (
			(!$isEnrolAdmin && !$isEnrolMaster && !$isActivityClubPic && !$isPic)
			|| $disableUpdateSettings
			|| !$libenroll->AllowToEditPreviousYearData
	) {
		$canAccess = false;
	}
	### Get Activity specific data
	$originalMeetingDateInfoAry = $libenroll->GET_ENROL_EVENT_DATE($recordId);
	$dateIdKey = 'EventDateID';
	$backUrl = 'event.php';
}

$originalMeetingDateRecordIdAry = Get_Array_By_Key($originalMeetingDateInfoAry, $dateIdKey);

unset($meetingDateInfoAry);


if (!$canAccess) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
####################################################
########### Access Right Checking [End] ############
####################################################


####################################################
######## Update Club / Activity Info [Start] #######
####################################################

### Inactivate deleted date
//$originalMeetingDateRecordIdAry
$newMeetingDateRecordIdAry = array();

foreach ((array)$dateInfoAry as $_rowNumber => $_dateInfoAry) {
	$_dateRecordId = $_dateInfoAry['dateRecordId'];
	if ($_dateRecordId != '') {
		$newMeetingDateRecordIdAry[] = $_dateRecordId;
	}
}
$deleteDateRecordIdAry = array_values(array_diff((array)$originalMeetingDateRecordIdAry, (array)$newMeetingDateRecordIdAry));
$numOfDeletedDate = count($deleteDateRecordIdAry);
### Insert newly added date 
foreach ((array)$dateInfoAry as $_rowNumber => $_dateInfoAry) {
	$_dateRecordId = $_dateInfoAry['dateRecordId'];
	$_dateText = $_dateInfoAry['dateText'];
	$_hourStart = str_pad($_dateInfoAry['hourStart'], 2, 0, STR_PAD_LEFT);
	$_minStart = str_pad($_dateInfoAry['minStart'], 2, 0, STR_PAD_LEFT);
	$_hourEnd = str_pad($_dateInfoAry['hourEnd'], 2, 0, STR_PAD_LEFT);
	$_minEnd = str_pad($_dateInfoAry['minEnd'], 2, 0, STR_PAD_LEFT);
	
	$_dateTimeStart = $_dateText.' '.$_hourStart.':'.$_minStart.':00';
	$_dateTimeEnd = $_dateText.' '.$_hourEnd.':'.$_minEnd.':00';
	$_infoAry = array();
	$infoAry['dateRecordId'] = $_dateRecordId;
	$infoAry['ActivityDateStart'] = $_dateTimeStart;
	$infoAry['ActivityDateEnd'] = $_dateTimeEnd;
	##Location #W107649 
	if($sys_custom['eEnrolment']['EnableLocation_tlgcCust']){
		$infoAry['Location'] = $_dateInfoAry['Location'];
	}
 	$TimeDisplay[] = $_dateTimeStart.' to '.$_dateTimeEnd;
 	$rs = $libenroll->Save_Meeting_Date($recordType, $recordId, $infoAry);
 	$successAry['Save_Meeting_Date_'.$_rowNumber] = $rs['Success'];
 	
    if($isEnableEnroleBooking && $rs['Success'] && $_dateInfoAry['BookingID']){
        $_meetingDateType = $recordType == $enrolConfigAry['Club'] ? ENROL_TYPE_CLUB : ENROL_TYPE_ACTIVITY;
        $libenroll->UpdateEnrolEbookingRelation($rs['MeetingDateID'],$_dateInfoAry['BookingID'], $_meetingDateType);
 	}
 	
// 	$successAry['Save_Meeting_Date_'.$_rowNumber] = $libenroll->Save_Meeting_Date($recordType, $recordId, $infoAry);
}
$jsonTimeDisplay = $json->encode($TimeDisplay);


### Update DB record
if ($recordType == $enrolConfigAry['Club']) {
	if ($numOfDeletedDate > 0) {
		$successAry['Inactivate_Deleted_Date'] = $libenroll->INACTIVATE_ENROL_GROUP_DATE($deleteDateRecordIdAry);
		$Action = 'Delete';
		if ($isEnableEnroleBooking) {
		    $libenroll->deleteEBooking($deleteDateRecordIdAry,ENROL_TYPE_CLUB);
		}
	} else if ($_dateRecordId == '') {
		$Action = 'Create';
	} else {	
		$Action = 'Update';
	}
	$successAry['Update_TimeDesc'] = $libenroll->UPDATE_GROUPINFO_TIMEDESCRIPTION($recordId, $TimeDescription);

}
else if ($recordType == $enrolConfigAry['Activity']) {
	if ($numOfDeletedDate > 0) {
		$successAry['Inactivate_Deleted_Date'] = $libenroll->INACTIVATE_ENROL_EVENT_DATE($deleteDateRecordIdAry);
		$Action = 'Delete';
		if ($isEnableEnroleBooking) {
		    $libenroll->deleteEBooking($deleteDateRecordIdAry,ENROL_TYPE_ACTIVITY);
		}
	} else if ($_dateRecordId == '') {
		$Action = 'Create';
	} else {	
		$Action = 'Update';
	}
	$successAry['Update_TimeDesc'] = $libenroll->UPDATE_EVENTINFO_TIMEDESCRIPTION($recordId, $TimeDescription);
	
	
	if($sys_custom['eEnrolment']['TWGHCYMA'] && $plugin['eBooking'] && in_array($activityInfoAry['SchoolActivity'],array(1,2))){
		$oldBookingIds = $libenroll->Get_Activity_Location_Booking_Records($recordId);
		$newBookingIds = explode(",",trim($hiddenBookingIDs));
		$toRemoveBookingIds = array_values(array_diff($oldBookingIds,$newBookingIds));
		$toUpdateBookingIds = array_values(array_diff($newBookingIds,$toRemoveBookingIds));
		if(count($toRemoveBookingIds)>0){
			$libenroll->Delete_Activity_Location_Booking_Record($toRemoveBookingIds);
		}
		if(count($toUpdateBookingIds)>0){
			$libenroll->Update_Activity_Location_Booking_Record($recordId, $toUpdateBookingIds);
		}
	}
}
####################################################
######### Update Club / Activity Info [End] ########
####################################################


$returnMsgKey = (in_array(false, $successAry))? 'UpdateUnsuccess' : 'UpdateSuccess';

$para = 'AcademicYearID='.$AcademicYearID.'&Semester='.$Semester.'&pic_view='.$pic_view.'&returnMsgKey='.$returnMsgKey;
$lurlparahandler = new liburlparahandler(liburlparahandler::actionEncrypt, $para, $enrolConfigAry['encryptionKey']);
$paraEncrypted = $lurlparahandler->getParaEncrypted();

// Redirect to related member setting page
if(IntegerSafe($DirectToMemberSettingGroup)){
	// Redirect to member setting for new group/activity setting
	$_para = 'AcademicYearID='.$AcademicYearID.'&EnrolGroupID='.$recordId.'&Semester='.$Semester;
	$link = "member_index.php?$_para";
	header('Location: '.$link);
} else if(IntegerSafe($DirectToMemberSettingActivity)){
	// Redirect to member setting for new group/activity setting
	$_para = 'AcademicYearID='.$AcademicYearID.'&EnrolEventID='.$recordId;
	$link = "event_member_index.php?$_para";
	header('Location: '.$link);
} else {
	header('Location: '.$backUrl.'?p='.$paraEncrypted);
}

$RecordDetail= "$recordType Name: $RecordTitle<br>Action: $Action<br>Details: $jsonTimeDisplay";
$liblog->INSERT_LOG($Module, $Section, $RecordDetail, $TableName, $recordId);
