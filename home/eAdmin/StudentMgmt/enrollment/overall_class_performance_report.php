<?php
# using: yat

############################################
#
#   Date    :   2018-03-19 Philips
#               Add HighCharts
#
#	Date	:	2011-09-08	YatWoon
# 				lite version should not access this page 
#
############################################
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
// include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
// $libenroll_ui = new libclubsenrol_ui();
// $lg = new libgroup();

if ($plugin['eEnrollmentLite'] || ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$isClassTeacher) && (!$libenroll->IS_CLUB_PIC())))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

// include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
// include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


$linterface = new interface_html();
$lclass = new libclass();

$CurrentPage = "PageClassPerformanceReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['overall_class_perform'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


//check if the user is a class teacher if the user is not a enrol admin/master
if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$libTeaching = new libteaching($_SESSION['UserID']);
	$result = $libTeaching->returnTeacherClass($_SESSION['UserID'], Get_Current_Academic_Year_ID());
	
	if (count($result)==0)
	{
		$isClassTeacher = false;
	}
	else
	{
		$isClassTeacher = true;
		$targetClassID = $result[0][0];
	}
}
// $LibUser = new libuser($UserID);
if ($isClassTeacher)
{
	$TitleField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEn');
	//get the class name
	$sql = "SELECT 
					yc.$TitleField, ycu.ClassNumber 
			FROM 
					YEAR_CLASS as yc
					INNER JOIN
					YEAR_CLASS_USER as ycu
					On (yc.YearClassID = ycu.YearClassID)
			WHERE 
					yc.YearClassID = '$targetClassID'
			Order By 
					ycu.ClassNumber
			";
	$result = $libenroll->returnArray($sql,1);
	$classSelection = $result[0][0];
	$targetClass = $result[0][0];
}
else
{ 
	$FormSelection = $lclass->getSelectLevel("name=\"targetFormID\" onChange='document.form1.submit();'", $targetFormID, 1, $Lang['SysMgr']['FormClassMapping']['All']['Form'], Get_Current_Academic_Year_ID());
}

##### retrieve class list
if(empty($targetFormID))
{
	$this_class_list = $lclass->getClassList();
}
else
{
	$ThisFormClassAry = $lclass->returnClassListByLevel($targetFormID);
	if(!empty($ThisFormClassAry))
	{
		for($i=0;$i<sizeof($ThisFormClassAry);$i++)
		{
			list($tmp_YearClassID, $tmp_ClassTitleEn, $tmp_ClassTitleB5) = $ThisFormClassAry[$i];
			
			$this_class_list[$i]['ClassID'] = $tmp_YearClassID;
			$this_class_list[$i]['ClassName'] = $tmp_ClassTitleEn;
		}
	}
	$form_con = " d.YearID = '". $targetFormID."' and ";
}

$class_performance_ary = array();
# club performance
$club_sql = "SELECT 
				d.YearClassID, sum(a.Performance)
			FROM 
				INTRANET_USERGROUP as a 
				LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
				left join INTRANET_USER as c on (c.UserID=a.UserID)
				left join YEAR_CLASS as d on (c.ClassName=d.ClassTitleEN and d.AcademicYearID='".Get_Current_Academic_Year_ID()."')
			WHERE  
				$form_con
				c.ClassName <> '' and 
				b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			group by 
				d.YearClassID
			";
			
$club_result = $libenroll->returnArray($club_sql);
if(!empty($club_result))
{
	foreach($club_result as $k=>$d)
	{
		$class_performance_ary[$d[0]] += $d[1];
	}
}
# act performance
$act_sql = "SELECT 
			d.YearClassID, sum(a.Performance)
		FROM 
			INTRANET_ENROL_EVENTSTUDENT as a 
			left join INTRANET_USER as c on (c.UserID=a.StudentID)
			left join YEAR_CLASS as d on (c.ClassName=d.ClassTitleEN and d.AcademicYearID='".Get_Current_Academic_Year_ID()."')
		where 
			$form_con
			c.ClassName <> '' 
		group by 
			d.YearClassID
";
$act_result = $libenroll->returnArray($act_sql);
if(!empty($act_result))
{
	foreach($act_result as $k=>$d)
	{
		$class_performance_ary[$d[0]] += $d[1];
	}
}

$display = "<div class='table_board'>";
$display .="<table class='common_table_list_v30 view_table_list_v30'>";
$display .= "<tr>";
$display .= "<th>".$i_UserClassName."</th>";
$display .= "<th style=\"text-align:center\">".$Lang['eEnrolment']['perform_score_comment']."</th>";
$display .= "</tr>";

$SumOver = 0;
for ($i=0; $i<sizeof($this_class_list); $i++)
{
	$this_pf = $class_performance_ary[$this_class_list[$i]['ClassID']] ? $class_performance_ary[$this_class_list[$i]['ClassID']] : 0;
	$display .= '<tr>';
	$display .= '<td valign="top">'.$this_class_list[$i]['ClassName'].'</td>';
	$link = "overall_performance_report.php?showTarget=class&targetClassID=".$this_class_list[$i]['ClassID'];
	$display .= '<td align="center" valign="top"><a href="'.$link.'">'. $this_pf .'</a></td>';
	$display .= '</tr>';
	
	$SumOver += $this_pf;
}

// Display Sum Overall Performance
$display .= '<tr>';
$display .= '<td align=right>'. $Lang['eEnrolment']['total_overall_perform'] .'</td>';
$display .= '<td align="center" valign="top"><b>'.$SumOver.'</b></td>';
$display .= '</tr>';

$display .= '</table>';
$display .= "</div>"; 				
				
?>

<script language="javascript">
<!--
function print_report()
{
	document.form1.action="overall_class_performance_print.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action="overall_class_performance_report.php";
	document.form1.target="_self";
}
function export_report()
{
	document.form1.action="overall_class_performance_export.php";
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action="overall_class_performance_report.php";
	document.form1.target="_self";
}
//-->
</script>

<form name="form1" action="overall_class_performance_report.php" method="get">
<table class="form_table_v30">
	<tr>
		<td nowrap="nowrap" class="field_title"><?=$Lang['AccountMgmt']['Form']?></td>
		<td><?= $FormSelection ?></td>
	</tr>
</table>
		
<div class="content_top_tool">
	<div class="Conntent_tool">
		<a href="javascript:export_report()" class="export"> <?=$Lang['Btn']['Export']?></a>
		<a href="javascript:print_report()" class="print"> <?=$Lang['Btn']['Print']?></a>
	</div>
<br style="clear:both" />
</div>
    
<?=$display?>

<!-- Start of Philips Edited -->
<!-- Keep a blank space between the tabel and the chart -->
<!--
<div class="chart_blank" style="height: 100px;"></div> -->
<!-- Keep a blank space between the tabel and the chart -->
<div class="chart_board" style="width: 100%; height: 100%;margin: 0 auto;">
<div id="chart_example" style="width: 100%; height: <?=sizeof($this_class_list)*26?>px; margin: 0 auto;clear: left; float: left;">
</div>
</div>
<?php 
$this_class_list_str = "";
$class_performance_ary_str = "";

// initialize the string for the chart
for ($i=0; $i<sizeof($this_class_list); $i++)
{
	$this_pf = $class_performance_ary[$this_class_list[$i]['ClassID']] ? $class_performance_ary[$this_class_list[$i]['ClassID']] : 0;
	$this_class_list_str .= '\'' . $this_class_list[$i]['ClassName'] . '\',';
	$class_performance_ary_str .= $this_pf . ',';
}

?>
<!-- script of the Highchart -->
<script type="text/javascript" src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="https://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" >
var chart = Highcharts.chart('chart_example', {
 chart: {
	 type: 'bar'
 },
 title: {
	 text: 'Overall Class Performance'
 },
 xAxis:{
	categories: [<?=$this_class_list_str?>],
	title:{
		text: null
	}
 },
 yAxis: {
	min: 0,
	title: {
		text: 'Performace (Score/Comment)',
		align: 'high'
	},
 },
 tooltip: {
	 formatter: function(){
		return this.x + '<br /><b>' + this.y + '</b> pt';
	 }
 },
 plotOptions: {
	 bar: {
		 dataLabels: {
			 enabled: true
		 }
	 }
 },
 legend: {
	 enabled: false
 },
 credits: {
	 enabled: false
 },
 series: [
	{name: '',
	data: [<?=$class_performance_ary_str?>]}
 ]
});
</script>
<!-- script of the Highchart -->


<!-- End of Philips Edited -->
		
</form>

<?
intranet_closedb();
echo $linterface->FOCUS_ON_LOAD("form1.targetFormID");
$linterface->LAYOUT_STOP();
?>