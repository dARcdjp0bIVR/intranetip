<?php
#Using by:
##########################
#   Date: 2019-04-01 Anna
#       Fixed case [#158932]
#
#	Date: 2017/09/15 Anna
#		add webSAMS code and type
#
#	Date: 2015/12/29 Omas
#		 add support to ole default setting import if have iPortfolio 
#
#	Date: 2015/10/27 Kenneth
#		Remarks table update
#
#	Date: 2015/10/20 (Omas)
#		Improved: Import description can support remarks link+ remarks 
#
#	Date: 2015/06/09
#		UI update
#
#	Date: 2015/06/03 (Evan)
#		Update the style according to UI standard
#
#	Date: 2014/11/21 (Omas)
#		Import new field column I for target House - 'A' for no limit 
#
#	Date: 2014/11/06 (Omas)
#		Import data column 1,2,3 -> A,B,C by function Get_Import_Page_Column_Display()
#
#
##########################


$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

if (!$plugin['eEnrollment']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
	# Assing Academic Year if not exist
	if(!isset($AcademicYearID) || $AcademicYearID=="") $AcademicYearID = Get_Current_Academic_Year_ID();

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	
	$libenroll = new libclubsenrol();
	$linterface = new interface_html();

	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID']))) {
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	}
	
	
//	if($libenroll->AllowToEditPreviousYearData) {
//		$yearFilter = getSelectAcademicYear("AcademicYearID", '', 1, 0, $AcademicYearID);
//	} else {
//		$yearFilter = getCurrentAcademicYear($intranet_session_language)."<input type='hidden' name='AcademicYearID' id='AcademicYearID' value='$AcademicYearID'>";
//	}
	$noPastYear = ($libenroll->AllowToEditPreviousYearData)? false : true;
	$yearFilter = getSelectAcademicYear('AcademicYearID', '', $noFirst=1, $noPastYear, $AcademicYearID);
	

	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	# tags
    $tab_type = "club";
    $current_tab = 1;
    
    $cancel_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&field=$field&order=$order&filter=$filter&keyword=$keyword";
	
	# navigation
	$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['eEnrolment']['Club'], "");
	
	if($plugin['iPortfolio']) {
		$ipo_sample = '_ipo';
	}
	else{
		$ipo_sample = '';
	}
	$CsvFileName = 'import_club_sample'.$ipo_sample.'.csv';
	
	$format_str .= "<a class=\"tablelink\" href=\"". GET_CSV($CsvFileName) ."\">[". $i_general_clickheredownloadsample ."]</a><br>";
	
	$format_str .= "<br>";
	
	# column description
//	$compulsoryColumn = array(1,2,3,4,5,6,7,8,9,10,11,12,13);
//	$FileDescription = "";
	$delim = "";
//	for($i=0; $i<count($Lang['eEnrolment']['Club_Import_FileDescription']); $i++) {
//		$FileDescription .= $delim.$Lang['General']['ImportColumn']." ".($i+1)." : ";
//		$FileDescription .= (in_array($i, $compulsoryColumn)) ? "<span class='tabletextrequire'>*</span>" : "";
//		$FileDescription .= $Lang['eEnrolment']['Club_Import_FileDescription'][$i];
//		if($Lang['eEnrolment']['Club_Import_ReferenceLink'][$i]!="") {
//			$FileDescription .= " [<a href='javascript:Load_Reference(\"".$Lang['eEnrolment']['Club_Import_ReferenceLink'][$i][0]."\")' class='tablelink' id='".$Lang['eEnrolment']['Club_Import_ReferenceLink'][$i][0]."'>".$Lang['eEnrolment']['Club_Import_ReferenceLink'][$i][1]."</a>]";	
//		}
//		if($Lang['eEnrolment']['Club_Import_Remark'][$i]!="") {
//			$FileDescription .= " <span class='tabletextremark'>(".$Lang['eEnrolment']['Club_Import_Remark'][$i].")</span>";	
//		}
//		$delim = "<br>";
//	}
//
//	$FileDescription .= " <br /><br /><span class='tabletextremark'>(".$Lang['eEnrolment']['Activity_Import_Remark'][18].")</span>";	
//	
//	$format_str .= $FileDescription;

	$ColumnTitleArr = $Lang['eEnrolment']['Club_Import_FileDescription'];
	if($plugin['iPortfolio']) {
		$ColumnTitleArr[] = str_replace('<!--DataName-->',$Lang['eEnrolment']['RecordType'],$Lang['eEnrolment']['Import']['DefaultOLE']);
		$ColumnTitleArr[] = str_replace('<!--DataName-->',$Lang['eEnrolment']['TargetNameLang'],$Lang['eEnrolment']['Import']['DefaultOLE']) ;
		$ColumnTitleArr[] = str_replace('<!--DataName-->',$eEnrollment['OLE_Category'],$Lang['eEnrolment']['Import']['DefaultOLE']);
		$ColumnTitleArr[] = str_replace('<!--DataName-->',$eEnrollment['OLE_Components'],$Lang['eEnrolment']['Import']['DefaultOLE']);
		$ColumnTitleArr[] = str_replace('<!--DataName-->',$ec_iPortfolio['organization'],$Lang['eEnrolment']['Import']['DefaultOLE']);
		$ColumnTitleArr[] = str_replace('<!--DataName-->',$ec_iPortfolio['details'],$Lang['eEnrolment']['Import']['DefaultOLE']);
		$ColumnTitleArr[] = str_replace('<!--DataName-->',$ec_iPortfolio['school_remarks'],$Lang['eEnrolment']['Import']['DefaultOLE']);
	}
	$ColumnTitleArr[] = $eEnrollment['WebSAMSSTACode'];
	$ColumnTitleArr[] = $eEnrollment['WebSAMSSTAType'];
	
	$ColumnPropertyArr = array(3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3);
	$RemarksArr = array();	
		
	for($i=0; $i<count($Lang['eEnrolment']['Club_Import_FileDescription']); $i++) {
		if($Lang['eEnrolment']['Club_Import_ReferenceLink'][$i]!="") {
			$RemarksArr[$i] = " [<a href='javascript:Load_Reference(\"".$Lang['eEnrolment']['Club_Import_ReferenceLink'][$i][0]."\")' class='tablelink' id='".$Lang['eEnrolment']['Club_Import_ReferenceLink'][$i][0]."'>".$Lang['eEnrolment']['Club_Import_ReferenceLink'][$i][1]."</a>]";	
		}
		if($Lang['eEnrolment']['Club_Import_Remark'][$i]!="") {
			$RemarksArr[$i] .= " <span class='tabletextremark'>(".$Lang['eEnrolment']['Club_Import_Remark'][$i].")</span>";	
		}
		$delim = "<br>";
	}
	
	if($plugin['iPortfolio']) {
		$RemarksArr[17] = '<a id="ole_type" class="tablelink" href="javascript:Load_Reference(\'ole_type\');">['.$Lang['eEnrolment']['Import']['OLESettingBtn1'].']</a>';
		$RemarksArr[18] = '<a id="ole_lang" class="tablelink" href="javascript:Load_Reference(\'ole_lang\');">['.$Lang['eEnrolment']['Import']['OLESettingBtn2'].']</a>';
		$RemarksArr[19] = '<a id="ole_category" class="tablelink" href="javascript:Load_Reference(\'ole_category\');">['.$Lang['eEnrolment']['Import']['OLESettingBtn3'].']</a>';
		$RemarksArr[20] = '<a id="ole_component" class="tablelink" href="javascript:Load_Reference(\'ole_component\');">['.$Lang['eEnrolment']['Import']['OLESettingBtn4'].']</a>&nbsp;'.'<span class="tabletextremark">('.$Lang['eEnrolment']['Import']['OLESettingRemark1'].')</span>';
	}
	
	$RemarksArr[25] = '<a id="WebSAMSSTAType" class="tablelink" href="javascript:Load_Reference(\'WebSAMSSTAType\');">['.$Lang['eEnrolment']['Import']['WebSAMS']['CheckType'].']</a>';
	$format_str.= $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
	
	$STEPS_OBJ[] = array($i_general_select_csv_file, 1);
	$STEPS_OBJ[] = array($i_general_confirm_import_data, 0);
	$STEPS_OBJ[] = array($i_general_imported_result, 0);
	
	
    include_once("management_tabs.php");

	if($msg!="") {
		$displayMsg = $Lang['General']['ReturnMessage'][$msg];	
	}

	$linterface->LAYOUT_START($displayMsg);
		
?>

<script type="text/JavaScript" language="JavaScript">

function trim(str){
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm() {
	obj = document.form1;
	if (trim(obj.elements["userfile"].value) == "") {
		 alert('<?=$eEnrolment['AlertSelectFile']?>');
		 obj.elements["userfile"].focus();
		 return false;
	}
	
	obj.action = "import_club_result.php";
		
	obj.submit();
}



function Load_Reference(ref) {
	
	$('#remarkDiv_type').html('');
	var AcademicYearID = $('#AcademicYearID').val();
	
	$.post(
		"ajax_reload.php",
		{ 
			flag: ref,
			AcademicYearID: AcademicYearID
		},
		function(ReturnData)
		{
			$('#remarkDiv_type').html(ReturnData);
		}
	);
	
	displayReference(ref);
}


function displayReference(ref) {
	var p = $("#"+ref);
	var position = p.position();
	
	var t = position.top + 15;
	var l = position.left + p.width()+5;
	$("#remarkDiv_type").css({ "position": "absolute", "visibility": "visible", "top": t+"px", "left": l+"px" });
}

function hideAllRemarkLayer() {
	$('div.selectbox_layer').each( function() {
		MM_showHideLayers($(this).attr('id'), '', 'hide');
	});
}
function hideRemarkLayer(remarkType) {
	var remarkDivId = getRemarkDivIdByRemarkType(remarkType);
	MM_showHideLayers(remarkDivId, '', 'hide');
}

function getRemarkDivIdByRemarkType(remarkType) {
	return 'remarkDiv_' + remarkType;
}

function Hide_Window(ref) {
	$("#"+ref).css({ "visibility": "hidden" });
}
</script>
<br />
<form name="form1" method="POST" onsubmit="return checkForm();" enctype="multipart/form-data">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result);?></td>
		</tr>
	</table>
		<table id="html_body_frame" width="100%" border="0" cellspacing="2" cellpadding="4">
		
		<tr><td><?= $linterface->GET_STEPS($STEPS_OBJ) ?></td></tr>
		<tr>
	    	<td colspan="2">
	    	<table width="90%" align="center">
				<tr>
					<td>
		    		<br>
			    		<table class="form_table_v30">
							<tr>
								<td class="field_title">
									<?= $linterface->RequiredSymbol().$Lang['General']['AcademicYear'] ?>
								</td>
								<td class="tabletext"><?=$yearFilter?><br>
								
								</td>
							</tr>
							<tr>
								<td class="field_title">
									<?= $linterface->RequiredSymbol().$i_select_file ?>
								</td>
								<td class="tabletext"><input class="file" type="file" name="userfile"><br>
								
								</td>
							</tr>
							<tr>
								<td class="field_title"><?= $i_general_Format ?></td>
								<td class="tabletext"><?=$format_str?><div id="remarkDiv_type" class="selectbox_layer" style="width:400px"></div></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		
		<br />
		
		<tr>
    		<td colspan="2">
		    	<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
		    		<tr>
                		<td colspan="3" class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
                	</tr>
		    	</table>
	    	</td>
		</tr>
		
		<br />
		
		<tr>
			<td colspan="2" align="center">
			<br />
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit", "")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'group.php?AcademicYearID=$AcademicYearID&Semester=$Semester'")?>&nbsp;
				
			</td>
		</tr>

			
		
	</table>
	
	<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="<?=$EnrolGroupID?>"/>
	<input type="hidden" name="EnrolEventID" id="EnrolEventID" value="<?=$EnrolEventID?>"/>
	<input type="hidden" name="type" id="type" value="<?=$type?>"/>
	<input type="hidden" name="Semester" id="Semester" value="<?=$Semester?>"/>
	
	
</form>
<?
	print $linterface->FOCUS_ON_LOAD("form1.userfile");
  	$linterface->LAYOUT_STOP();
?>