<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

$EnrolGroupID = $_REQUEST['EnrolGroupID'];
$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club")))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		
$li = new libfilesystem();

$GroupArr['GroupID'] = $GroupID;
$GroupArr['EnrolGroupID'] = $EnrolGroupID;
$GroupArr['Discription'] = $Discription;
if ($set_age) $GroupArr['UpperAge'] = $UpperAge;
if ($set_age) $GroupArr['LowerAge'] = $LowerAge;
$GroupArr['Gender'] = $gender;
$GroupArr['attach'] = $_FILES['attach'];
$GroupArr['GroupCategory'] = $sel_category;
$GroupArr['PaymentAmount'] = $PaymentAmount;
$GroupArr['Quota'] = $Quota;
$GroupArr['MinQuota'] = $MinQuota;
$GroupArr['isAmountTBC'] = $isAmountTBC;


if ($attachname0 != "") $GroupArr['attach']['name'][0] = $attachname0;
if ($attachname1 != "") $GroupArr['attach']['name'][1] = $attachname1;
if ($attachname2 != "") $GroupArr['attach']['name'][2] = $attachname2;
if ($attachname3 != "") $GroupArr['attach']['name'][3] = $attachname3;
if ($attachname4 != "") $GroupArr['attach']['name'][4] = $attachname4;

if ($libenroll->IS_GROUPINFO_EXISTS($EnrolGroupID)) {
	$EnrolGroupID = $libenroll->EDIT_GROUPINFO($GroupArr);
} else {
	// Must have INRANET_ENROL_GROUPINFO for eEnrolment1.2
	//$EnrolGroupID = $libenroll->ADD_GROUPINFO($GroupArr);
}

//$libenroll->DEL_GROUPSTUDENT($GroupID);

$libenroll->DEL_GROUPCLASSLEVEL($EnrolGroupID);

$ClasslvlArr['EnrolGroupID'] = $EnrolGroupID;
for ($i = 0; $i < sizeof($classlvl); $i++) {
	$ClasslvlArr['ClassLevelID'] = $classlvl[$i];
	$libenroll->ADD_GROUPCLASSLEVEL($ClasslvlArr);
}

intranet_closedb();
header("Location: group_new1b.php?EnrolGroupID=$EnrolGroupID&Semester=$Semester");

?>