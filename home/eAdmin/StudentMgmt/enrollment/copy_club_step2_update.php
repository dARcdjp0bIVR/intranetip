<?php
// Using: 

########Change Log#########
#
#	2015-10-30	Kenneth
#		- Reset Enrollment Data ONLY when Copy Member is selected
#
##############################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->hasAccessRight($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$LibUser = new libuser($UserID);

$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);

if (!$isEnrolAdmin)
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");

$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
$ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
$TermMappingArr = unserialize(rawurldecode($_REQUEST['TermMappingArr']));
$CopyClubArr = unserialize(rawurldecode($_REQUEST['CopyClubArr']));
$CopyMemberArr = unserialize(rawurldecode($_REQUEST['CopyMemberArr']));
$CopyActivityArr = unserialize(rawurldecode($_REQUEST['CopyActivityArr']));

//Kenneth - 2015-10-30	Reset Enrollment Data ONLY when Copy Member is selected
if(sizeof($CopyMemberArr)!=0){
	$SuccessArr['Reset_Enrollment_Data'] = $libenroll->Reset_Enrollment_Data();
}

$SuccessArr = array();
$GroupIDMappingArr = array(); 			// $GroupIDMappingArr[$FromGroupID] = $ToGroupID
$EnrolGroupIDMappingArr = array();		// $EnrolGroupIDMappingArr[$FromEnrolGroupID] = $ToEnrolGroupID
$failedCount = 0;
$successCount = 0;

$libenroll->Start_Trans();
foreach ($CopyClubArr as $thisEnrolGroupID => $thisCopy)
{
	$CopyMember = $CopyMemberArr[$thisEnrolGroupID];
	$CopyActivity = $CopyActivityArr[$thisEnrolGroupID];
	
	$thisGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
	$ReturnInfoArr = $libenroll->Copy_Group_And_Club($thisGroupID, $thisEnrolGroupID, $ToAcademicYearID, $TermMappingArr, $GroupIDMappingArr);
	
	$NewEnrolGroupID = $ReturnInfoArr['EnrolGroupID'];
	$NewGroupID = $ReturnInfoArr['GroupID'];
	$EnrolGroupIDMappingArr[$thisEnrolGroupID] = $NewEnrolGroupID;
	$GroupIDMappingArr[$thisGroupID] = $NewGroupID;
	
	if ($NewEnrolGroupID==0 || $NewEnrolGroupID=='')
		$SuccessArr[$thisEnrolGroupID]['Copy_GroupAndClub'] = false;
	else
	{
		$SuccessArr[$thisEnrolGroupID]['Copy_GroupAndClub'] = true;
		
		if ($CopyMember){
			$SuccessArr[$thisEnrolGroupID]['Copy_Member'] = $libenroll->Copy_Club_Member($thisEnrolGroupID, $NewEnrolGroupID);
		}
		if ($CopyActivity){
			$SuccessArr[$thisEnrolGroupID]['Copy_Activity'] = $libenroll->Copy_Activity_Of_Club($thisEnrolGroupID, $NewEnrolGroupID);
		}
	}
	
	if (in_array(false, $SuccessArr[$thisEnrolGroupID]))
		$failedCount++;
	else
		$successCount++;
}

if (in_array(false, $SuccessArr))
{
	$libenroll->RollBack_Trans();
}
else
{
	$libenroll->Commit_Trans();
	$libenroll->Add_Copy_Log($FromAcademicYearID, $ToAcademicYearID, $TermMappingArr, $CopyClubArr, $CopyMemberArr, $CopyActivityArr, $EnrolGroupIDMappingArr);
}

intranet_closedb();

header('Location: copy_club_step3.php?SuccessCount='.$successCount);
?>