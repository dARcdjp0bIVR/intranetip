<?php 
/*
 * Using:
 *
 * Change Log:
 * Date:	2017-05-04	Villa
 * 			-Open the File
 */
##Modify Form Serlize to Arr
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libebooking_api = new libebooking_api();
$libenroll = new libclubsenrol($AcademicYearID);
if($dateInfoAry){
	//OnClickCancel
	foreach ((array)$dateInfoAry as $key=>$_dateInfoAry){
		$BookingID = $_dateInfoAry['BookingID'];
		if($BookingID){
			$libebooking_api->RequestDeleteBooking($BookingID);
		}
	}
}else{
	//Delete From the ThickBox
	if($BookingID){
		$libebooking_api->RequestDeleteBooking($BookingID);
		$libenroll->DelEnrolEBookingRelation($BookingID);
	}
}

intranet_closedb();
?>