<?php
//Using: 
/******************************************
 * Modification Log:
 *      2020-01-17 (Tommy)
 *      - added adjustedReason for $sys_custom['eEnrolment']['Refresh_Performance']
 * 
 *      2018-08-27 (Ivan) [X145096] [ip.2.5.9.10.1]
 *      - support $sys_custom['eEnrolment']['TermBasedPerformanceForClub'] to show term-based performance data
 *      
 *      2018-08-21 (Anna)
 *      - Change navigation name to $Lang['eEnrolment']['MemberList']
 *      
 *      2018-06-14 (Philips)
 *      - Hide parent filter selection
 * 
 * 		2017-09-27 (Anna)
 * 		- Modified access right to club pic	
 * 
 * 		2017-08-31 (Anna)
 * 		- Add print_eca_attendace_report
 * 
 * 		2017-08-07 (Simon)
 * 		- Add pop up panel for extra curricular activities form
 * 
 * 		2017-06-20 (Omas)		
 * 		- Changed $eEnrollment['comment'] to $eEnrollment['teachers_comment'] 
 * 
 * 		2015-07-22 (Evan)
 * 		-Modified link of Add & Assign button, passes GroupID and filter as well
 * 
 * 		2015-07-10 (Shan)
 * 		-Searchbar style updated
 * 
 *      2015-06-02 (Shan)
 *      -UI style updated        
 *
 * 		2015-05-18 (Omas)
 * 		-Add $sys_custom['eEnrolment']['kyc']['notificationLetter'] for cust notification letter
 * 
 *  	2013-05-22 (Rita)
 *  	amend club merit
 * 
 * 		2013-03-01 Rita
 * 		- add Disallow PIC to add/remove members control, $showBtnforPIC variable 
 * 
 * 		2012-12-28 Rita
 * 		- add club merit customization
 * 
 * 		2012-03-30 Ivan
 * 		- add achivement field display
 *
 *		2011-10-31 YatWoon
 *		- add customization access right checking Customization_HeungToChecking() # For Heung To only: only admin can add/edit/delete member (for club only)
 *
 *		2011-09-07 YatWoon
 *		- lite version implementation
 *
 *		20110604 YatWoon
 *			- add "Print" function (case#2011-0526-1607-35071)
 *
 *		20110524 YatWoon
 *			- add column "Attendance"
 *
 * 		20100802 Thomas
 * 			- Check the data is freezed or not
 *			  Show alert message when adding / updating the data
 * 
 * 		20100409 Marcu
 *			- modified getGroupMember
 *  		- hide "non-exist" user , e.g. User not approved or who left school already.
 * 
 * 		20100409 Marcus:
 * 			 - add "AND a.EnrolGroupID = '$EnrolGroupID'" condition to sql 
 * ****************************************/

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

// if(!$plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]  || !($libenroll->IS_CLUB_PIC()))
if(!$plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] && !$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
//	debug_pr( !$_SESSION["SSV_PRIVILEGE"]["enrollment"]["is_club_pic"] );
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

$LibUser = new libuser($UserID);

$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
if ($_POST['AcademicYearID'] != '') {
	$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);
}
else if ($_GET['AcademicYearID'] != '') {
	$AcademicYearID = IntegerSafe($_GET['AcademicYearID']);
}
if ($_POST['Semester'] != '') {
	$Semester = IntegerSafe($_POST['Semester']);
}
else if ($_GET['Semester'] != '') {
	$Semester = IntegerSafe($_GET['Semester']);
}

$libenroll = new libclubsenrol();	
$libenroll_ui = new libclubsenrol_ui();
$lstudentprofile = new libstudentprofile();

$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);

// edited by Ivan(17 July 2008) set default sorting field as class(class number) in asc order
if ($field=="" || !isset($field))
{
	$field = 0;
}
if ($order=="" || !isset($order))
{
	$order = 1;
}
if($filter=="" || !isset($filter)){
	$filter = 2;
}

$filter_array = array (
			   //array (3,$i_identity_parent),
               array (2,$i_identity_student),
               array (1,$i_identity_teachstaff),
               );
               
if($UID_list!="")
{
	$UID = explode(",", $UID_list);
}

if ($plugin['eEnrollment'])
{
	$linterface = new interface_html();
    $ligroup = new libgroup($GroupID);
        
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)) && (!$ligroup->hasAdminBasicInfo($UserID))  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	{
		//header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
		No_Access_Right_Pop_Up();
		exit;
	}
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

		/*
        $linterface = new interface_html();
        $ligroup = new libgroup($GroupID);
        */
        //$TAGS_OBJ[] = array($ligroup->Title." ".$eEnrollment['groupmates_list'], "", 1);
        
        # tags 
        $tab_type = "club";
        $current_tab = 1;
        include_once("management_tabs.php");        
        # navigation
        $title_display = $intranet_session_language=="en" ? $ligroup->Title : $ligroup->TitleChinese;
        $PAGE_NAVIGATION[] = array($title_display." ".$Lang['eEnrolment']['MemberList'], "");

        # Setting - >Disallow PIC to add/remove members Control
		$showBtnforPIC = true;
		if($libenroll->disallowPICtoAddOrRemoveMembers){
			if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])){
				if(($libenroll->IS_CLUB_PIC($EnrolGroupID))){
					$showBtnforPIC = false;
				}
			}
		}
        
  
        # Construc information above the table
        $quotaDisplay = "<table cellspacing=\"0\" cellpadding=\"4\" border=\"0\" width=\"100%\" class =\"form_table_v30\">";
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">";
		$quotaDisplay .= "$i_ClubsEnrollment_GroupName </td><td>".$title_display."</td></tr>";

		$groupQuota = $libenroll->getQuota($EnrolGroupID);
		$ClubInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID);
		
		$memberCount = count($ClubInfoArr);
		
		//$memberCount = $libenroll->GET_NUMBER_OF_MEMBER($EnrolGroupID, "club");
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">$i_ClubsEnrollment_GroupQuota1 </td><td>$groupQuota</td></tr>";
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"field_title\" width=\"30%\">$i_ClubsEnrollment_CurrentSize </td><td>$memberCount</td></tr>";
		$quotaDisplay .= "</table>";
		
		//$searchbar = $libenroll->GET_STUDENT_NAME_SERACH_BOX($keyword, "keyword", $eEnrollment['enter_name']);
		//$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "javascript:document.form1.keyword.value = Trim(document.form1.keyword.value); document.form1.submit()");
		//$searchbar = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"". stripslashes($keyword) ."\" onkeyup=\"Check_Go_Search(event);\"/>";
		### search box
		$sqlkeyword = stripslashes($keyword);
		$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $sqlkeyword);
		$keyword = intranet_htmlspecialchars($keyword);
		//do not search for the initial textbox text
		if ($keyword == $eEnrollment['enter_name']) $keyword = "";
        
        //added by Ivan (17 July 2008) for export file
        $type = 0;
		$add_member_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=$type&field=$field&order=$order&filter=$filter&keyword=$keyword";
		$typeImport = "clubPerformance";
		$import_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=$typeImport&field=$field&order=$order&filter=$filter&keyword=$keyword";
		$typeClub = "club";
		$active_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=$typeClub&field=$field&order=$order&filter=$filter&keyword=$keyword";
		$notification_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=$typeClub&field=$field&order=$order&filter=$filter&keyword=$keyword";
		$typeImportMember = "clubMember";
		$importMember_parameters = "AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&type=$typeImportMember&field=$field&order=$order&filter=$filter&keyword=$keyword";
	
		//for selection of Student/Teacher/Staff/All	
        $filterbar = "";         
		$filterbar .= getSelectByArray($filter_array,"name=filter onChange=\"document.form1.target='_self'; document.form1.action='member_index.php'; document.form1.submit();\"",$filter, 0, 1);
		if ($filter == 1 || $filter == 2 || $filter == 3){
			$conds = "a.RecordType = $filter";
		}
		else{
			$conds = "a.RecordType = 2";
		}
		
		if ($sys_custom['eEnrolment']['ClubMemberListApprovedByUserFilter'] && $filter == 2) {
			$filterbar .= $libenroll_ui->Get_Member_Approve_By_Selection($enrolConfigAry['Club'], 'approvedByType', 'approvedByType', $approvedByType, "changedApprovedByFilterSelection();");
		}
       
	    ################################################################

		// edited by Ivan (16 July 2008) to retrieve the number of students only, excluding the teachers and staffs
		$GroupUsers = $ligroup->getNumberGroupUsers( $conds);
		//$GroupUsers = $ligroup->getNumberGroupUsers();		
		
		/*	
		if ($ligroup->RecordType != -1) {                
		    $toolbar .= "<a class=iconLink href=javascript:newWindow('add.php?GroupID=".$ligroup->GroupID."',2)>".importIcon()."$button_import</a>\n".toolBarSpacer();
		}
		*/
		

		# Construct toolbars
		$toolbar1 = "";
		$toolbar2 = "";
		
		# Email link
		if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) || ($libenroll->IS_CLUB_PIC($EnrolGroupID))) {
			$toolbar1 .= '<div style="float:left; padding: 3px 6px 5px 2px; display: block;">';
			$toolbar1 .= ($GroupUsers == 0) ? "<img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/iMail/icon_compose.gif\" border=\"0\" align=\"absmiddle\" style='padding: 0px 2px;'><span class='contenttool'>$button_email</span>" : "<a class='contenttool' href=\"javascript:checkRole(document.form1, 'UID[]','member_email.php')\"><img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/iMail/icon_compose.gif\" border=\"0\" align=\"absmiddle\" style='padding: 0px 2px;'>$button_email</a>\n";
			$toolbar1 .= '</div>';
			$toolbar1 .= toolBarSpacer();
		}
		
		# Add member
		if ((!$libenroll->disableStatus || $libenroll->ALLOW_PAYMENT($GroupID)) && $libenroll->Customization_HeungToChecking())
		{
			# Add member link
			//$toolbar1 .= $linterface->GET_LNK_ADD("javascript:submitAdd(document.form1, 'add_member.php?')");
//			$toolbar1 .= "<a class=\"contenttool\" href=\"";
//			$toolbar1 .= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitAdd(document.form1, 'add_member.php')";
//			$toolbar1 .= "\">";
//			$toolbar1 .= "<img src=\"".$PATH_WRT_ROOT."/images/{$LAYOUT_SKIN}/icon_add.gif\" width=\"18\" height=\"18\" border=\"0\" align=\"absmiddle\"> ";
//			$toolbar1 .= $eEnrollment['button']['add&assign'];
//			$toolbar1 .= "</a>";

			$onclick = ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:submitAdd(document.form1, 'add_member.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&filter=$filter')";
			
			if($showBtnforPIC==true){
				$toolbar1 .= $linterface->GET_LNK_ADD($onclick, $eEnrollment['button']['add&assign'], '', '', '', 0);
			}
		}
		
		
		# Notification Letter link (students only)
		if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) || ($libenroll->IS_CLUB_PIC($EnrolGroupID))) {
			if ($filter == 2)
			{
				if($plugin['eEnrollmentLite'])
				{
					$toolbar1 .= "<div class='Conntent_tool'><i class='print'>".$eEnrollment['button']['notification_letter']."</i></div>";
				}
				else
				{
					if($sys_custom['eEnrolment']['kyc']['notificationLetter']){
						$onclick = "javascript: document.form1.target='blank'; newCheckRole(document.form1, 'UID[]', 'notification_letter_view_kyc.php?$notification_parameters')";
					}
					else{
						$onclick = "javascript: document.form1.target='blank'; checkRole(document.form1, 'UID[]', 'notification_letter_view.php?$notification_parameters')";	
					}
					$toolbar1 .= $linterface->GET_LNK_PRINT($onclick, $eEnrollment['button']['notification_letter'], '', '', '', 0);
				}
			}
		}
		
		//second row of toolbar
		# Import Member link
		if($libenroll->AllowToEditPreviousYearData && $libenroll->Customization_HeungToChecking()) {
			if($showBtnforPIC==true){
				$toolbar2 .= $linterface->GET_LNK_IMPORT(($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" :"import.php?$importMember_parameters", '', '', '', '', 0);
				$toolbar2 .= toolBarSpacer();
			}
		}
		# Export link
		$toolbar2 .= $linterface->GET_LNK_EXPORT("javascript:js_Go_Export();", '', '', '', '', 0);
		$toolbar2 .= toolBarSpacer();
		
		# Print member list link
		$toolbar2 .= $linterface->GET_LNK_PRINT_IP25("javascript:print_member_list();");
		$toolbar2 .= toolBarSpacer();
		
		
		if($sys_custom['eEnrolment']['UnitRolePoint'] == true){
			$toolbar2.= $linterface->GET_LNK_PRINT("javascript:print_eca_attendace_report();", $Lang['eEnrolment']['SysRport']['button']['AttendanceReport'], '', '', '', 0);
			
		}
		# Import Perfermance and Comment link
		//$toolbar2 .= $linterface->GET_LNK_IMPORT("import.php?$import_parameters", $eEnrollment['import_performance_comment']);
		//$toolbar2 .= toolBarSpacer();
		
		$toolbar3 = "";
		# Active Member link
		if($plugin['eEnrollmentLite']){
			$toolbar3 .= "<div class='Conntent_tool'><i class='import'>". $eEnrollment['button']['active_member'] ."</i></div>";
		}else{
			$toolbar3 .= $linterface->GET_LNK_IMPORT(($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" :"active_member.php?$active_parameters", $eEnrollment['button']['active_member'], '', '', '', 0);
		}
		
		
		# show role selection button and update button if student is chosen
		if ($filter == 2)
		{
			$row = $ligroup->returnRoleType();
			if (!isset($RoleID)){
	        	$RoleID = '';
	        	$selected = (isset($RoleID))? "" : "SELECTED";
		    }
			$functionbar = "<select name='RoleID'>\n";
			$functionbar .= "<option value='' $selected>".$eEnrollment['select_role']."</option>\n";
			for($i=0; $i<sizeof($row); $i++)
				$functionbar .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
			$functionbar .= "</select>\n";
			
			$functionbar .= $linterface->GET_SMALL_BTN($eEnrollment['button']['update_role'], "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRole(document.form1,'UID[]','member_role.php')")."&nbsp;";
		}
		
		# Remove member
		if ((!$libenroll->disableStatus || $libenroll->ALLOW_PAYMENT($GroupID)) && $libenroll->Customization_HeungToChecking())
		{
		
			if($showBtnforPIC == true){
				$functionbar .= ($ligroup->RecordType == -1) ? "&nbsp;" : $linterface->GET_SMALL_BTN($Lang['Btn']['Delete'], "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRemove(document.form1,'UID[]','member_delete.php')");
			}
		}
		
		$groupadminfunctionbar = "";
		
		$subleader = get_file_content("$intranet_root/file/homework_leader.txt");
		
		if ($subleader == 1 && $ligroup->RecordType == 3) {                # Only Class has
		    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UID[]','performance.php')\">";
		    $groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UID[]','subjectleader.php')\"><img src='/images/admin/button/t_btn_assignsubjectleader_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_assignsubjectleader'></a>&nbsp;";
		}
		
		# if only have access right Group Member Mgmt, cannot see these options
		if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) || ($libenroll->IS_CLUB_PIC($EnrolGroupID))) {
			$groupadminfunctionbar .= "<input type='hidden' name='adminflag' value='0'>";
			$groupadminfunctionbar .= $linterface->GET_SMALL_BTN($eEnrollment['set_admin'], "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:document.form1.adminflag.value='A'; checkEdit(document.form1, 'UID[]','member_assignadmin.php')")."&nbsp;";
			$groupadminfunctionbar .= $linterface->GET_SMALL_BTN($eEnrollment['unset_admin'], "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:document.form1.adminflag.value='0'; checkRole(document.form1, 'UID[]','member_setadmin.php')")."&nbsp;";
		
		
			if ($ligroup->RecordType == 5) {                
				# Only ECA has
			    #$groupadminfunctionbar .= "<input class=button type=button value=\"$button_updateperformance\" onClick=\"checkEdit(this.form, 'UID[]','performance.php')\">";
			    //$groupadminfunctionbar .= "<a href=\"javascript:checkEdit(document.form1, 'UID[]','performance.php')\"><img src='/images/admin/button/t_btn_update_performance_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_updateperformance'></a>&nbsp;";
			    //"<a href=\"\"><img src='/images/admin/button/t_btn_update_performance_$intranet_session_language.gif' border='0' align='absmiddle' alt='$button_updateperformance'></a>&nbsp;";
			    if(!$plugin['eEnrollmentLite'])
			    	$groupadminfunctionbar .= $linterface->GET_SMALL_BTN($Lang['eEnrolment']['UpdatePerformanceAndPerformance'], "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkRestore(document.form1, 'UID[]','member_comment_perform.php')");
			    else
			    	$groupadminfunctionbar .= $linterface->GET_SMALL_BTN($Lang['eEnrolment']['UpdatePerformanceAndPerformance'], "button","",""," disabled ");
			}
		}
		
		
		# TABLE INFO
		$li = new libdbtable2007($field, $order, $pageNo);	
		
		$sql = $libenroll->Get_Management_Club_Member_Sql($GroupID, $EnrolGroupID, $filter, $sqlkeyword, $ForExport=0, $AcademicYearID, $approvedByType);
// debug_pr($sql);
		if ($filter == 2)
		{
// 			if ($libenroll->enableUserJoinDateRange()) {
// 				$li->field_array = array("iu.ClassName", "iu.ClassNumber", "StudentName");
// 				if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
// 					$li->field_array[] ="iu.NickName";
// 				}
// 				$li->field_array[] ="ir.Title";
// 				//
// 				$li->field_array[] ="EnrolAvailiableDateStart";
// 				$li->field_array[] ="EnrolAvailiableDateEnd";
// 				$li->field_array[] ="Performance";
// 				$li->field_array[] ="Achievement";
// 				$li->field_array[] ="Comment";
// 				$li->field_array[] ="ActiveMemberStatus";
// 			//	$li->field_array[] ="iu.UserID";
// 			} else {
// 				$li->field_array = array("iu.ClassName", "iu.ClassNumber", "StudentName");
				
// 				if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
// 					$li->field_array[] ="iu.NickName";
// 				}
// 				$li->field_array[]= "ir.Title";
// 				$li->field_array[]= "Performance";
// 				$li->field_array[]= "Achievement";
// 				$li->field_array[]= "Comment";
// 				$li->field_array[]= "ActiveMemberStatus";
// 			//
// 			}
			
			$li->field_array = array("iu.ClassName", "iu.ClassNumber", "StudentName");
			if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
			    $li->field_array[] ="iu.NickName";
			}
			$li->field_array[] ="ir.Title";
			if ($libenroll->enableUserJoinDateRange()) {
    			$li->field_array[] ="EnrolAvailiableDateStart";
    			$li->field_array[] ="EnrolAvailiableDateEnd";
			}
			if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
			    $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
			    
			    $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
			    $termAry = $libenroll->returnResultSet($sqlTemp);
			    $numOfTerm = count($termAry);
			    
			    for ($i=0; $i<$numOfTerm; $i++) {
			        $_yearTermId = $termAry[$i]['YearTermID'];
			        $li->field_array[] = 'TermBasedPer'.$_yearTermId;
			    }
			}
			$li->field_array[] ="Performance";
			if($sys_custom['eEnrolment']['Refresh_Performance']){
			    $li->field_array[] = "AdjustReason";
			}
			$li->field_array[] ="Achievement";
			$li->field_array[] ="Comment";
			$li->field_array[] ="ActiveMemberStatus";
			
			$li->field_array[]= "iu.UserID";
				
			$li->fieldorder2 = ", iu.ClassNumber";
		//	$li->field_array[] = ", iu.ClassNumber";
		}
		
		else
		{
			$li->field_array = array("StudentName", "iegs.StaffType", "iu.UserID");
			$li->fieldorder2 = ", iu.ClassNumber";
		//	$li->field_array[] = ", iu.ClassNumber";
		}
		
		$li->field_array[] = "";
		### Record the sql for export first	
		//$li->sql = $libenroll->Get_Management_Club_Member_Sql($GroupID, $EnrolGroupID, $filter, $keyword, $ForExport=1, $AcademicYearID, $approvedByType);
		//echo $li->sql.'<br>';
		//$ExportSQLTemp = $li->built_sql();
		//$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
		//$ExportSQL = substr($ExportSQLTemp, 0, $LimitIndex);
		
		### Build Page SQL now
		$li->sql = $sql;
		
		$li->title = "";
		
//		if($libenroll->enableClubRecordMeritInfoRight()){	
//			$li->column_array = array(0,0,0,0,0,0,18,0,0,0,0,0,18);	
//			$li->wrap_array = array(0,0,0,0,0,0,0,0,0,0,0);
//		}
//		else{
//			$li->column_array = array(0,0,0,0,0,0,18,18);
//			$li->wrap_array = array(0,0,0,0,0,0);
//		}

		
		$li->column_array = array();
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
			$li->column_array[] = 0;
		}
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		$li->column_array[] = 0;
		if ($libenroll->enableUserJoinDateRange()) {
			$li->column_array[] = 0;
			$li->column_array[] = 0;
		}
		if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
		    for ($i=0; $i<$numOfTerm; $i++) {
		        $li->column_array[] = 0;
		    }
		}
		if($sys_custom['eEnrolment']['Refresh_Performance']){
		    $li->column_array[] = 0;
		}
		$li->column_array[] = 18;
		$li->column_array[] = 18;
		if ($libenroll->enableClubRecordMeritInfoRight()) {
			if (!$lstudentprofile->is_merit_disabled) {
				$li->column_array[] = 0;
			}
			if (!$lstudentprofile->is_min_merit_disabled) {
				$li->column_array[] = 0;
			}
			if (!$lstudentprofile->is_maj_merit_disabled) {
				$li->column_array[] = 0;
			}
			if (!$lstudentprofile->is_sup_merit_disabled) {
				$li->column_array[] = 0;
			}
			if (!$lstudentprofile->is_ult_merit_disabled) {
				$li->column_array[] = 0;
			}
		//	$li->column_array[] = 0;
		}
	//	$li->column_array[] = 0; 
// 		$li->IsColOff = 12;
		$li->IsColOff = "Enrolment_Display_Club";
		
		//edited by Ivan (17 July 2008) deleting the loginID and showing the class number first
		// TABLE COLUMN
		if ($filter == 2)
		{
			$col = 0;
			$li->column_list .= "<th width=\"1\" nowrap><span class=\"tabletoplink\">#</span></th>\n";
			$li->column_list .= "<th width=\"5%\" nowrap>".$li->column($col++, $i_general_class)."</th>\n";
			$li->column_list .= "<th width=\"5%\" nowrap>".$li->column($col++, $i_ClassNumber)."</th>\n";
			$li->column_list .= "<th width=\"10%\" nowrap>".$li->column($col++, $i_UserStudentName)."</th>\n";
			if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
				$li->column_list .= "<th width=\"10%\" nowrap>".$li->column($col++, $i_UserNickName)."</th>\n";
			}
		//	$li->column_list .= "<th width=\"5%\">". $li->column($col++, $eEnrollment['attendence'] )."</th>\n";
			$li->column_list .= "<th width=\"10%\" nowrap>".$li->column($col++, $i_admintitle_role)."</th>\n";
			$li->column_list .= "<th width=\"5%\" nowrap>". $li->column($col++, $eEnrollment['attendence'] )."</th>\n";
			
			if ($libenroll->enableUserJoinDateRange()) {
				$li->column_list .= "<th width=\"6%\">".$li->column($col++, $Lang['eEnrolment']['AvailiableDateStart'])."</th>\n";
				$li->column_list .= "<th width=\"6%\">".$li->column($col++, $Lang['eEnrolment']['AvailiableDateEnd'])."</th>\n";
				$li->no_col = 13;
			} else {
				$li->no_col = 11;
			}
			if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
				$li->no_col++;
			}
			
			if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
			    for ($i=0; $i<$numOfTerm; $i++) {
			        $_termName = $termAry[$i]['TermName'];
			        
			        $li->column_list .= "<th width=\"6%\">".$li->column($col++, $_termName.' '.$eEnrollment['performance'])."</th>\n";
			        $li->no_col++;
			    }
			}
			$li->column_list .= "<th width=\"15%\">".$li->column($col++, $eEnrollment['performance'])."</th>\n";
			if($sys_custom['eEnrolment']['Refresh_Performance']){
			    $li->column_list .= "<th width=\"6%\">".$li->column($col++, $Lang['eEnrolment']['Refresh_Performance']['AdjustedReason'])."</th>\n";
			    $li->no_col++;
			}
			
			$li->column_list .= "<th width=\"15%\">".$li->column($col++, $Lang['eEnrolment']['Achievement'])."</th>\n";
			$li->column_list .= "<th width=\"15%\">".$li->column($col++, $eEnrollment['teachers_comment'])."</th>\n";
			$li->column_list .= "<th width=\"10%\" nowrap>".$li->column($col++, $eEnrollment['active']."/".$eEnrollment['inactive'])."</th>\n";
			
			if($libenroll->enableClubRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_Merit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_MinorCredit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_MajorCredit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_SuperCredit)."</th>\n";
					$li->no_col++;
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$li->column_list .= "<th>".$li->column($col++, $i_Merit_UltraCredit)."</th>\n";
					$li->no_col++;
				}
			}
		//	$li->column_list .= "<th>".$li->check("UID[]")."</th>\n";
			
		}
		else
		{
			$li->column_list .= "<th width=\"1\" ><span class=\"tabletoplink\">#</span></th>\n";
			$li->column_list .= "<th width=\"49%\" >".$li->column(0, $i_UserName)."</th>\n";
			$li->column_list .= "<th width=\"50%\" >".$li->column(1, $i_admintitle_role)."</th>\n";
			$li->no_col = 4;
		//	$li->column_list .= "<th>".$li->check("UID[]")."</th>\n";
		}
		
		
		
		$li->column_list .= "<th width=\"1\">".$li->check("UID[]")."</td>\n";
				
		################################################################
		
		# record added
		if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
		# record updated
		if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
		# record deleted
		if ($msg == 3) $SysMsg = $linterface->GET_SYS_MSG("delete");
		# email sent
		if ($msg == 4) $SysMsg = $linterface->GET_SYS_MSG("",$i_con_msg_email);
		# Update Fail (2013/1/03 added)
		if ($msg == 5) $SysMsg = $linterface->GET_SYS_MSG("update_failed");
		
		$linterface->LAYOUT_START();
		
		$ClubInfoArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
		$Semester = $ClubInfoArr['Semester'];
		
		$button_back_para = "AcademicYearID=$AcademicYearID&Semester=$Semester";
		if (($LibUser->isStudent()) || ($LibUser->isParent())) {
			$button_back_para .= '&pic_view=1';
		}
		
		$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks();
		
//		if($libenroll->enableClubRecordMeritInfoRight()){
//				$htmlAry['meritSubmitBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "javascript:submitMeritRecord()");							
//		}

		?>
		
		<div id="extraCurricularActivitiesPopUp" class="selectbox_layer" style="width:400px;">
			<table cellspacing="0" cellpadding="3" border="0" width="100%">
				<tr>
					<td colspan="2" style="text-align: right; padding: .2em; cursor:pointer;" onclick="closeExtraCurricularActivitiesPopUp()">X</td>
				</tr>
				<tr>
					<td><?php echo $Lang['General']['Date'] ?></td>
					<td><?=$linterface->GET_DATE_PICKER("releaseDate")?></td>
				</tr>
				<tr>
					<td><?php echo $eEnrollment['replySlip']['replySlip'].$Lang['General']['Date'] ?></td>
					<td><?=$linterface->GET_DATE_PICKER("replyDate")?></td>
				</tr>
				<tr>
					<td><?php echo $Lang['General']['Teacher'].$Lang['General']['Name'] ?></td>
					<td><input type="text" name="teacherName" id="teacherName" /></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center">
						<input type="button" value="<?php echo $Lang['Btn']['Submit'] ?>" onclick="extraCurricularActivitiesBtnSubmit()" />
					</td>
				</tr>
			</table>
		</div>
	    <!--script src="<?php echo $PATH_WRT_ROOT?>test/marcoyu/xlsx.core.min.js"></script>
	    <script src="<?php echo $PATH_WRT_ROOT?>test/marcoyu/filesave.js"></script>
	    <script src="<?php echo $PATH_WRT_ROOT?>test/marcoyu/tableExport.js"></script-->
	    <script language="javascript">
		// newCheckRole function
		function newCheckRole(obj,element,page){
			if(countChecked(obj,element)==0){
		        alert(globalAlertMsg2);
			}else{
				showExtraCurricularActivitiesPopUp();
			}
		}
		

		$(function(){
			$('div#extraCurricularActivitiesPopUp').hide();
		})

		// call for ExtraCurricularActivitiesPopUp
		function showExtraCurricularActivitiesPopUp() {
			var topAdjustment = 350;
			var leftAdjustment = $('div#extraCurricularActivitiesPopUp').width();

			$('div#extraCurricularActivitiesPopUp')
				.css({"visibility":"visible",
					"left":leftAdjustment,
					"top":topAdjustment})
				.toggle();
		}

		function extraCurricularActivitiesBtnSubmit() {
			var releaseDateVal = $('#releaseDate').val();
			$('#releaseDateId').val(releaseDateVal);
			var replyDateVal = $('#replyDate').val();
			$('#replyDateId').val(replyDateVal);
			var teacherNameVal = $('#teacherName').val();
			$('#teacherNameId').val(teacherNameVal);
			obj=document.form1;
			page='notification_letter_view_kyc.php?' + '<?php echo $notification_parameters ?>';
			obj.action=page;
            obj.target="_blank";
			obj.submit();
			obj.action="";
		}

		// close the extra curricular activities pop up
		function closeExtraCurricularActivitiesPopUp(){
			$('div#extraCurricularActivitiesPopUp').hide();
		}
		
		function submitAdd(obj, page){
	        obj.action=page;
	        obj.submit();
		}
		
		function js_Go_Export()
		{
			document.getElementById('type').value = 0;
			
			var jsObjForm = document.getElementById('form1');
			jsObjForm.action = 'export.php';
	        jsObjForm.submit();
		}
		
		function print_member_list()
		{
			newWindow('print_member_list.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$EnrolGroupID?>&Semester=<?=$Semester?>&filter=<?=$filter?>',10);
		}

		<?php if($sys_custom['eEnrolment']['UnitRolePoint']){?>
			function print_eca_attendace_report(){
				newWindow('print_eca_attendace_report.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$EnrolGroupID?>&Semester=<?=$Semester?>&filter=<?=$filter?>',10);
				
			}
		<?php }?>
		
		<?php if($libenroll->enableActivitySurveyFormRight()){ ?>
		    function view_merit(EnrolGroupID,StudentID)
			{
				newWindow('edit_merit_record.php?EnrolGroupID='+EnrolGroupID+'&StudentID='+StudentID,1);
			}
		
		<? }?>
		
		
		<?php// if($libenroll->enableClubRecordMeritInfoRight()){ ?>
			//function submitMeritRecord(){
			//	$('#form1').attr('action','merit_record_update.php');
			//	$('#form1').submit();
			//}
		<? //}?>
		
		function changedApprovedByFilterSelection() {
			document.form1.target = '_self';
			document.form1.action = 'member_index.php';
			document.form1.submit();
		}	      
		</script>
		
		<br />
		
		<table width="100%" border="0" cellspacing="4" cellpadding="4">
			<tr><td>
				<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)?>
			</td></tr>
		</table>
				
		<div class="table_board">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr><td><?=$quotaDisplay?></td></tr>
				<tr><td height="5"></td></tr>
				<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			</table>
			<br />
	
			<form id="form1" name="form1" method="post">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
					<tr>
						<td align="left">	
							<?if($libenroll->AllowToEditPreviousYearData) {echo $toolbar1;} ?>	
						</td>
						<td align="right">
							<?= $SysMsg ?>
						</td>
					</tr>
					<?if (($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) || ($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) || ($libenroll->IS_CLUB_PIC($EnrolGroupID))) {?>
					<tr>
						<td align="left">	
							<?= $toolbar2 ?>	
						</td>
						<td align="right">
							&nbsp;	<?=$htmlAry['searchBox']?>
						</td>
					</tr>
					<?}?>
					<tr>
						<td align="left">
							<?= $toolbar3 ?>
							
						</td>
						<td align="right">
						    <?if($libenroll->AllowToEditPreviousYearData) { echo $functionbar;}?>
						</td>
					</tr>
					<tr>
						<td align="left">
						<?=$filterbar?>	
					
						</td>
						<td align="right"><?if($libenroll->AllowToEditPreviousYearData) { echo $groupadminfunctionbar;}?></td>
					</tr>
	
				</table>
				<?=$li->display("100%","","",$filter)?>
				
			    <?=$RemarksTable?>
				
				<input type="hidden" name="GroupID" value=<?= $GroupID ?>>
				<input type="hidden" name="EnrolGroupID" value=<?= $EnrolGroupID ?>>
				<input type="hidden" name="field" value="<?=$field?>">
				<input type="hidden" name="order" value="<?=$order?>">
				<input type="hidden" name="page_size_change" value="">
				<input type="hidden" name="pageNo" value="<?=$pageNo?>">
				<input type="hidden" name="numPerPage" value="<?=$ligroup->page_size?>">
				<input type="hidden" id="type" name="type" value="<?=$type?>">
				<input type="hidden" name="typeText" value="<?=$typeClub?>">
				<input type="hidden" name="Semester" value="<?=$Semester?>">
				<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">
				<input type="hidden" name="releaseDate" id="releaseDateId" value="<?=$releaseDate?>">
				<input type="hidden" name="replyDate" id="replyDateId" value="<?=$replyDate?>">
				<input type="hidden" name="teacherName" id="teacherNameId" value="<?=$teacherName?>">
				
				
				<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
					<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td align="center" colspan="6">
						<div style="padding-top: 5px">
							<?=$htmlAry['meritSubmitBtn']?>	
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='group.php?".$button_back_para."'")?>
																
						</div>
					</td></tr>
				</table>
			    <!--script>
			    var RowColSpan1 = $('table.common_table_list_v30');
			    new TableExport(RowColSpan1, {
			        formats: ['xlsx'],
			        IgnoreCols: 0
			    });
			    </script--> 
			</form>
		</div>

	<?

		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>