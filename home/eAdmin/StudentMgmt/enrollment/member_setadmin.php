<?php

$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$li = new libdb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$UID = IntegerSafe($UID);

if ($adminflag == "0" || $adminflag == "")
{
    $adminflag = "NULL";
    $rights_string = "NULL";
    $user_list = implode(",",$UID);
}
else
{
    $adminflag = "'$adminflag'";
    $user_list = "$UID";
    if ($alltools == 1)
    {
        $rights_string = 'NULL';
    }
    else
    {
        # Change the tool rights to integer
        $sum = 0;
        for ($i=0; $i<sizeof($grouprights); $i++)
        {
             $target = intval($grouprights[$i]);
             $sum += pow(2,$target);
        }
        $rights_string = $sum;
    }
}

$sql = "UPDATE INTRANET_USERGROUP SET
        RecordType = $adminflag
        ,AdminAccessRight = $rights_string
        WHERE GroupID = '$GroupID' AND UserID IN ($user_list)";
$li->db_db_query($sql);

intranet_closedb();
header("Location: member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&filter=$filter&msg=2");
?>
