<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/libwebmail.php');

intranet_auth();
intranet_opendb();

// $EnrolEventID = IntegerSafe($_POST['EnrolEventID']);
$EnrolGroupID = IntegerSafe($_POST['EnrolGroupID']);
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);


# students in the email list
$UID = unserialize(rawurldecode($UID));


$mailSubject = stripslashes(intranet_htmlspecialchars(addslashes($subject)));
$mailBody = stripslashes(intranet_htmlspecialchars(addslashes($message)));



$lwebmail = new libwebmail();
$lwebmail->sendModuleMail($UID, $mailSubject, $mailBody);


header("Location: committee_member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&filter=$filter&msg=4");
?>
