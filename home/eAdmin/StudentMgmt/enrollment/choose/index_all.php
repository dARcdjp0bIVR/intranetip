<?php

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_opendb();

if ($plugin['eEnrollment'])
{

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$MODULE_OBJ['title'] = $button_select." ".$eEnrollment['select'][$ppl_type];
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    $linterface = new interface_html("popup.html");

	    ################################################################

if ($libenroll->hasAccessRight($_SESSION['UserID']))
{

}
else
{
    echo "You have no priviledge to access this page.";
    exit();
}

$lclass = new libclass();
$select_class = $lclass->getSelectClass("name=\"targetClass\" onChange=\"this.form.action='';this.form.submit()\"",$targetClass);

if ($targetClass != "")
    $select_students = $lclass->getStudentSelectByClass($targetClass,"size=\"21\" multiple=\"multiple\" name=\"targetID[]\"");
    
################################################################

$linterface->LAYOUT_START();

# retrieve role type

$lo = new libgroup($GroupID);

$row = $lo->returnGroupRoleType();
$RoleOptions .= "<select name=RoleID>\n";
for($i=0; $i<sizeof($row); $i++)
$RoleOptions .= (Isset($RoleID)) ? "<option value=".$row[$i][0]." ".(($row[$i][0]==$RoleID)?"SELECTED":"").">".$row[$i][1]."</option>\n" : "<option value=".$row[$i][0]." ".(($row[$i][2]==1)?"SELECTED":"").">".$row[$i][1]."</option>\n";
$RoleOptions .= "</select>\n";

# retrieve group category

$li = new libgrouping();

if($CatID < 0){
     unset($ChooseGroupID);
     $ChooseGroupID[0] = 0-$CatID;
}
$lgroupcat = new libgroupcategory();
$cats = $lgroupcat->returnAllCat();

$x1  = ($CatID!=0 && $CatID > 0) ? "<select name=CatID onChange=checkOptionNone(this.form.elements[\"ChooseGroupID[]\"]);this.form.submit()>\n" : "<select name=CatID onChange=this.form.submit()>\n";
$x1 .= "<option value=''>-- $button_select --</option>\n";
$x1 .= "<option value=-1 ".(($CatID==-1)?"SELECTED":"").">$i_identity_teachstaff</option>\n";
	$x1 .= "<option value=-2 ".(($CatID==-2)?"SELECTED":"").">$i_identity_student</option>\n";
if ($ppl_type != "pic") {
	$x1 .= "<option value=-3 ".(($CatID==-3)?"SELECTED":"").">$i_identity_parent</option>\n";
}
$x1 .= "</select>";

if($CatID < 0){
   # Return users with identity chosen
   $selectedUserType = 0-$CatID;

	$username_field = getNameFieldWithClassNumberByLang("");
	$sql = "SELECT UserID,$username_field FROM INTRANET_USER WHERE RecordType = $selectedUserType And RecordStatus = 1
			ORDER BY ClassName, ClassNumber, TRIM(EnglishName)";
	$row = $li->returnArray($sql,2);
   
   //$row = $li->returnUserForTypeExcludeGroup($selectedUserType,$GroupID);

     $x3  = "<select name=ChooseUserID[] size=21 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
else if(isset($ChooseGroupID)) {
	
	$username_field = getNameFieldWithClassNumberByLang("");
	$sql = "SELECT UserID,$username_field FROM INTRANET_USER WHERE RecordType = $selectedUserType And RecordStatus = 1
			ORDER BY ClassName, ClassNumber, TRIM(EnglishName)";
	$row = $li->returnArray($sql,2);
	
     //$row = $li->returnGroupUsersExcludeGroup($ChooseGroupID, $GroupID);
     $x3  = "<select name=ChooseUserID[] size=21 multiple>\n";
     for($i=0; $i<sizeof($row); $i++)
     $x3 .= "<option value=".$row[$i][0].">".$row[$i][1]."</option>\n";
     $x3 .= "<option>";
     for($i = 0; $i < 40; $i++)
     $x3 .= "&nbsp;";
     $x3 .= "</option>\n";
     $x3 .= "</select>\n";
}
?>
<script language="JavaScript1.2">
function add_role(obj){
     role_name = prompt("", "New_Role");
     if(role_name!=null && Trim(role_name)!=""){
          obj.role.value = role_name;
          obj.action = "add_role.php";
          obj.submit();
     }
}
function add_user(obj){
     obj.action = "add_user.php";
     obj.submit();
}

function import_update(){
        var obj = document.form1;
        checkOption(obj.elements["ChooseUserID[]"]);
        obj.action = "add_user.php";
        obj.submit();
}

function SelectAll(obj)
{
         for (i=0; i<obj.length; i++)
         {
              obj.options[i].selected = true;
         }
}

function expandGroup()
{
        var obj = document.form1;
        checkOption(obj.elements['ChooseGroupID[]']);
        obj.submit();
}

function AddOptions(obj){
     par = opener.window;
     parObj = opener.window.document.form1.elements["<?php echo $fieldname; ?>"];

     checkOption(obj);
     par.checkOption(parObj);
     i = obj.selectedIndex;

     while(i!=-1)
        {
         addtext = obj.options[i].text;

          par.checkOptionAdd(parObj, addtext, obj.options[i].value);
          obj.options[i] = null;
          i = obj.selectedIndex;
     }
     par.checkOptionAdd(parObj, "<?php for($i = 0; $i < 40; $i++) echo " "; ?>", "");
     par.form1.flag.value = 0;
     //par.generalFormSubmitCheck(par.form1);
}

</script>

<form name="form1" action="index_all.php" method="post">
<table border="0" cellpadding="0" cellspacing="0" align="center" width="90%">
<tr>
<td align="center">

<table width="95%" border="0" cellpadding="5" cellspacing="1">
<tr>
	<td>
	<table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
			<td valign="top" nowrap="nowrap"><span class="tabletext"><?= $i_frontpage_campusmail_select_category?> : </span></td>
			<td width="70%"><?php echo $x1; ?></td>
		</tr>
		<tr><td colspan="2" height="5"></td></tr>		
		<?php if(isset($ChooseGroupID))
		{ ?>
		<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td colspan="2" height="5"></td></tr>
		<tr>
			<td valign="top" nowrap="nowrap" ><span class="tabletext"><?= $i_frontpage_campusmail_select_user?>: </span></td>
			<td width="80%" style="align: left">
		       	<table border="0" cellpadding="0" cellspacing="0" align="left">
					<tr> 
						<td>
		       				<?php echo $x3 ?>
		       			</td>
		       			<td style="vertical-align:bottom">        
					        <table width="100%" border="0" cellspacing="0" cellpadding="6">
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_SMALL_BTN($button_add, "button", "checkOption(this.form.elements['ChooseUserID[]']);AddOptions(this.form.elements['ChooseUserID[]'])")?>
										<!--
										<a href="javascript:import_update()"><img src="/images/admin/button/s_btn_add_<?=$intranet_session_language?>.gif" border="0"></a>
										-->
									</td>
								</tr>
								<tr> 
									<td align="left"> 
										<?= $linterface->GET_SMALL_BTN($button_select_all, "button", "SelectAll(this.form.elements['ChooseUserID[]']); return false;")?>
									</td>
								</tr>
							</table>
		       			</td>
		       		</tr>
		       	</table>
			</td>
		</tr>
		<?php
		} ?>
	</table>
</tr>

</table>


</td>
</tr>

<tr><td>

<table width="100%" border="0" cellpadding="5" cellspacing="0" align="center">
<tr><td class="dotline"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr>
	<td align="center"><?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
	</td>
</tr>
</table>
<br/>

</td></tr>

</table>
<input type="hidden" name="GroupID" value="<?php echo $GroupID; ?>">
<input type="hidden" name="role">
<input type="hidden" name="fieldname" value="<?php echo $fieldname; ?>" />
<input type="hidden" name="ppl_type" value="<?php echo $ppl_type; ?>" />
</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>