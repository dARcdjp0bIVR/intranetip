<?php
# using: yat

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

	$libenroll = new libclubsenrol();
	$lg = new libgroup();
	$lexport = new libexporttext();
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	$TargetArr = array();
	$HeaderArr = array();
	$DataArr = array();
	$ExportArr = array();
	
	if ($type=="classPerform")
	{
		//construct table title
		$HeaderArr[] = array("#",$i_UserClassName,$i_UserClassNumber,$i_UserStudentName,$Lang["eEnrolment"]["NoOfEmptyClubPerformance"],$Lang["eEnrolment"]["NoOfEmptyActPerformance"],$Lang['eEnrolment']['perform_score_comment']);
		
		//get name of the student
    	$name_field = getNameFieldByLang('USR.');
    	if($showTarget=="class")
    	{
	       	$YearClassObj = new year_class($class,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
	       	$ClassName = $YearClassObj->Get_Class_Name();
	       	
        	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
        	$studentNameArr = $YearClassObj->ClassStudentList;
        	
        	$TargetArr[] = array($i_general_class, $ClassName);
    	}
	    else if($showTarget=="club")
	    {
    		$studentNameArr = $libenroll->Get_Club_Member_Info(array($club), $PersonTypeArr=array(2), Get_Current_Academic_Year_ID(), $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
    		$EnrolGroupInfo = $libenroll->GET_GROUPINFO($club);
    		$TitleDisplay = $intranet_session_language=="en" ? $EnrolGroupInfo['Title'] : $EnrolGroupInfo['TitleChinese'];
    		$TargetArr[] = array($eEnrollmentMenu['club'], $TitleDisplay);
    	} 
    	else if($showTarget=="house") 
    	{
	       	$temp_stuInfo = $lg->returnGroupUser4Display($house, 2);
	       	for($i=0; $i<sizeof($temp_stuInfo); $i++) {
				$studentIDArr[] = $temp_stuInfo[$i]['UserID'];
				$studentNameArr[$i]['UserID'] = $temp_stuInfo[$i]['UserID'];
				
				$studentNameArr[$i]['StudentName'] = (empty($temp_stuInfo[$i]['ClassName']) ? "^":"") . $temp_stuInfo[$i][3];
				$studentNameArr[$i]['ClassName'] = $temp_stuInfo[$i]['ClassName'];
				$studentNameArr[$i]['ClassNumber'] = $temp_stuInfo[$i]['ClassNumber'];
	     	}
	     	$HouseInfo = $libenroll->getGroupInfo($house);
    		$TitleDisplay = $intranet_session_language=="en" ? $HouseInfo[0]['Title'] : $HouseInfo[0]['TitleChinese'];
	     	$TargetArr[] = array($i_House, $TitleDisplay);
       	}
       	
	 	$SumOver = 0;
	 	//loop through all students to get performance and construct table contents
	 	
		if (empty($targetCategory)){
			$conds_category_group ='';
			$conds_category_event ='';
		}else{
			$conds_category_group = " AND d.GroupCategory IN ( ".$targetCategory." ) ";
			$specialJoinGroup = ' INNER JOIN INTRANET_ENROL_GROUPINFO as d ON  (a.GroupID = d.GroupID) ';
			$conds_category_event = " AND d.EventCategory IN ( ".$targetCategory." ) ";
			$specialJoinEvent = ' INNER JOIN INTRANET_ENROL_EVENTINFO as d ON  (a.EnrolEventID = d.EnrolEventID) ';
		}
	 	
	 	for ($i=0; $i<sizeOf($studentNameArr); $i++)
	 	{
	     	$studentID = $studentNameArr[$i]['UserID'];
	     	$studentName = $studentNameArr[$i]['StudentName'];
	     	$className = ($studentNameArr[$i]['ClassName'] == "")? $Lang['General']['EmptySymbol'] : $studentNameArr[$i]['ClassName'];
			$classNumber = ($studentNameArr[$i]['ClassNumber'] == "")? $Lang['General']['EmptySymbol'] : $studentNameArr[$i]['ClassNumber'];
			
	     	//get club performance
	     	$sql = "SELECT  a.Performance
	     			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID ".$specialJoinGroup."
	     			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'".$conds_category_group;
	     	$clubPerformArr = $libenroll->returnArray($sql,1);
	     	
	     	/**
	     	 * Add Category Filter [Kenneth]
	     	 */
	     	
	     	//get activity performance
	     	$sql = "SELECT a.Performance
	     			FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID ".$specialJoinEvent."
	     			WHERE a.StudentID = '$studentID'  AND a.RecordStatus = 2".$conds_category_event;
	     	$actPerformArr = $libenroll->returnArray($sql,1);
	     	
	     	//loop through all performances to get the total mark
	     	$totalMark =  $emptyActPerformance = $emptyClubPerformance = 0;
	     	for ($j=0; $j<sizeof($clubPerformArr); $j++)
	     	{
		     	if (is_numeric($clubPerformArr[$j][0]))
		     	{
			     	$totalMark += $clubPerformArr[$j][0];
		     	}
		     	else if (empty($clubPerformArr[$j][0]))
		     	{
			     	$emptyClubPerformance ++;
		     	}
	     	}
	     	for ($j=0; $j<sizeof($actPerformArr); $j++)
	     	{
		     	if (is_numeric($actPerformArr[$j][0]))
		     	{
			     	$totalMark += $actPerformArr[$j][0];
		     	}	
		     	else if (empty($actPerformArr[$j][0]))
		     	{
			     	$emptyActPerformance ++;
		     	}
	     	}
	     	
	     	$SumOver += $totalMark;
	     	
	     	$count = $i+1;
			$DataArr[] = array($count,$className,$classNumber,$studentName,$emptyClubPerformance,$emptyActPerformance,$totalMark);
	 	}
	 	
	 	if (sizeof($studentNameArr)==0)
	    {
	        $DataArr[] = array($i_no_record_exists_msg);
	    }
	}
	
	$ExportArr = array_merge($TargetArr, $HeaderArr,$DataArr);
	
	$utf_content = "";
	foreach($ExportArr as $k=>$d)
	{
		$utf_content .= implode("\t", $d);
		$utf_content .= "\r\n";
	}
	
	intranet_closedb();
	
	$filename = "overall_performance.csv";
	$lexport->EXPORT_FILE($filename, $utf_content); 
?>