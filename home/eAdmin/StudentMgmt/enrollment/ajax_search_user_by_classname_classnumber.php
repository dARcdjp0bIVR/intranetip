<?php
// using
################## Change Log [Start] ##############
#	Date:	2017-03-20 	Villa
#			Create this File with the reference of eNotice
################## Change Log [End] ################
$PATH_WRT_ROOT = "../../../../";


include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

# Get data
$YearID = $_REQUEST['YearID'];
//$SearchValue = stripslashes(urldecode($_REQUEST['q']));
$SelectedUserIDList = $_REQUEST['SelectedUserIDList'];
$SelectedUserIDList = str_replace('U','',$SelectedUserIDList);
$SelectedUserIDListArr = explode(',',$SelectedUserIDList);
$SelectedUserIDsStr = implode("','",$SelectedUserIDListArr);

$libdb = new libdb();

$currAcademicYearID = Get_Current_Academic_Year_ID();

if (isset($_REQUEST["searchfrom"]) && $_REQUEST["searchfrom"] == "textarea") {
	$textarea_q = explode("||", $_GET['q']);
	if (count($textarea_q) > 0) {
		$removeChar = array( "\t", " " );
		foreach ($textarea_q as $kk => $vv) {
			$textarea_q[$kk] = trim(str_replace($removeChar, "", $vv));
		}
	}
	$textarea_q = array_unique($textarea_q);
	$returnString = '';
	if (count($textarea_q) > 0) {
		foreach ($textarea_q as $kk => $val) {
			$SearchValue = trim($libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($val)))));
			
			if (!empty($SearchValue)) {
				if ($_REQUEST["nt_userType"] == "pic") {
					$sql = "SELECT
					UserID, UserLogin,
						".getNameFieldByLang()." as Name,
						ClassName,
						ClassNumber
					FROM
						INTRANET_USER
					WHERE
						RecordStatus = 1
						AND RecordType = 1
						AND UserID NOT IN ('".$SelectedUserIDsStr."')
						AND UserLogin like '".$SearchValue."'
					Order by ClassName asc,ClassNumber asc
					Limit 1";
				} else {
					$sql = "SELECT
					iu.UserID, iu.UserLogin,
						".getNameFieldByLang()." as Name,
						iu.ClassName,
						iu.ClassNumber
					FROM
						INTRANET_USER AS iu
					LEFT JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID=iu.UserID)
					LEFT JOIN YEAR_CLASS AS yc ON (ycu.YearClassID=yc.YearClassID and AcademicYearID='" . $currAcademicYearID . "')
					WHERE
						iu.RecordStatus = 1
						AND iu.RecordType = 2
						AND iu.UserID NOT IN ('".$SelectedUserIDsStr."')
						AND (
							Concat(lower(iu.ClassName),iu.classNumber ) like '".$SearchValue."'
							OR Concat(lower(yc.ClassTitleEN),ycu.classNumber ) like '".$SearchValue."'
							OR Concat(lower(yc.ClassTitleB5),ycu.classNumber ) like '".$SearchValue."'
							OR iu.UserLogin like '".$SearchValue."'
						)
					Group by iu.UserID 
					Order by iu.ClassName asc,iu.ClassNumber asc
					Limit 1";
				}
				$result = $libdb->returnResultSet($sql);
				if (count($result) > 0) {
					foreach((array)$result as $_StudentInfoArr){
						$_UserID = $_StudentInfoArr['UserID'];
						$_Name = $_StudentInfoArr['Name'];
						$_ClassName = $_StudentInfoArr['ClassName'];
						$_ClassNumber = $_StudentInfoArr['ClassNumber'];
						$UserLogin = $_StudentInfoArr['UserLogin'];
						if (empty($_ClassName) || $_REQUEST["nt_userType"] == "pic") {
							$_thisUserNameWithClassAndClassNumber = $_Name.'';
						} else {
							$_thisUserNameWithClassAndClassNumber = $_Name.' ('.$_ClassName.'-'.$_ClassNumber.')';
						}
						$returnString .= $_thisUserNameWithClassAndClassNumber. "||" . $_UserID . "\n";
					}
				} else {
					$returnString .= '||NOT_FOUND||' . $val . "\n";
				}
			}
		}
	}
} else {
	
	$sql = "SELECT
				iu.UserID,
				".getNameFieldByLang()." as Name,
				iu.ClassName,
				iu.ClassNumber
			FROM
				INTRANET_USER AS iu
			LEFT JOIN YEAR_CLASS_USER AS ycu ON (ycu.UserID=iu.UserID)
			LEFT JOIN YEAR_CLASS AS yc ON (ycu.YearClassID=yc.YearClassID and AcademicYearID='" . $currAcademicYearID . "')";
	$sql .= " WHERE iu.RecordStatus = 1 AND iu.RecordType = 2 AND iu.USERID NOT IN ('".$SelectedUserIDsStr."') AND (";
	
	$pos = strpos($_REQUEST['q'], " ");
	if ($pos !== false) {
		$tmp = explode(" ", $libdb->Get_Safe_Sql_Like_Query((urldecode($_REQUEST['q']))));
		$SearchValues[0] = strtolower(trim($tmp[0]));
		$SearchValues[1] = trim(str_replace($tmp[0], "", $libdb->Get_Safe_Sql_Like_Query((urldecode($_REQUEST['q'])))));
	} else {
		$SearchValue = trim($libdb->Get_Safe_Sql_Like_Query(strtolower((urldecode($_REQUEST['q'])))));
	}
	if ($SearchValues > 1) {
		$sql .= " ( Concat(lower(iu.ClassName),iu.classNumber ) like '%".$SearchValues[0]."%'
			OR Concat(lower(yc.ClassTitleEN),ycu.classNumber ) like '%".$SearchValues[0]."%'
			OR Concat(lower(yc.ClassTitleB5),ycu.classNumber ) like '%".$SearchValues[0]."%'
		)
		AND ( iu.ChineseName like'%".$SearchValues[1]."%'
		or iu.EnglishName like '%".$SearchValues[1]."%' )";
	} else {
		$sql .= " Concat(lower(iu.ClassName),iu.classNumber ) like '%".$SearchValue."%'
				OR Concat(lower(yc.ClassTitleEN),ycu.classNumber ) like '%".$SearchValue."%'
				OR Concat(lower(yc.ClassTitleB5),ycu.classNumber ) like '%".$SearchValue."%'
			  	OR iu.ChineseName like'%".$SearchValue."%'
			  	OR iu.EnglishName like '%".$SearchValue."%'";
	}
	$sql .= " ) Group by iu.UserID Order by iu.ClassName asc,iu.ClassNumber asc Limit 10";
	
	$result = $libdb->returnResultSet($sql);

	$returnString = '';
	foreach((array)$result as $_StudentInfoArr){
		$_UserID = $_StudentInfoArr['UserID'];
		$_Name = $_StudentInfoArr['Name'];
		$_ClassName = $_StudentInfoArr['ClassName'];
		$_ClassNumber = $_StudentInfoArr['ClassNumber'];
		if (empty($_ClassName)) {
			$_thisUserNameWithClassAndClassNumber = $_Name.'';
		} else {
			$_thisUserNameWithClassAndClassNumber = $_Name.' ('.$_ClassName.'-'.$_ClassNumber.')';
		}
// 		$_thisUserNameWithClassAndClassNumber = convert2Big5($_thisUserNameWithClassAndClassNumber);
		

		$returnString .= $_thisUserNameWithClassAndClassNumber.'|'.'|'.$_UserID."\n";
// 		$returnString = convert2Big5($returnString);
	}
}
intranet_closedb();
echo $returnString;
?>