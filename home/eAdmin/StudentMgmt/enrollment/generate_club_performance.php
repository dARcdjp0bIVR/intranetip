<?php
// Using: yat

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!($plugin['eEnrollment'] && $sys_custom['YeoCheiMan_generate_performance'] && $libenroll->hasAccessRight($_SESSION['UserID'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$lg = new libgeneralsettings();
$linterface = new interface_html();

# tags 
$tab_type = "club";
$current_tab = 1;
include_once("management_tabs.php");
        
$CurrentPage = "PageMgtClub";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['GeneratePerformance']);

$dataAry = array();
$dataAry[] = "'Club_Generate_Performance_DateTime'";
$dataAry[] = "'Club_Generate_Performance_By'";
$gen_data = $lg->Get_General_Setting("eEnrolment", $dataAry);
$lastGenBy_tmp = $libenroll->returnUserName($gen_data['Club_Generate_Performance_By']);
$lastGenBy = $lastGenBy_tmp[0];


?>

<script language="javascript">
<!--
function click_back()
{
	window.location="index.php";	
}

function click_gen()
{
	document.getElementById('div_btns').innerHTML = "<br><?=$Lang['General']['Loading']?>";
	
	$('#div_btns').load(
		'ajax_generate_performance.php', 
		{type: 'club'}, 
		function (data){
		}); 
}	
//-->
</script>

<?=$linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION);?>

<fieldset class="instruction_warning_box_v30">
<legend><?=$Lang['General']['Warning']?></legend>
<?=$Lang['eEnrolment']['GeneratePerformanceWarning']?>
</fieldset>


<div class="edit_bottom" id="div_btns">
	<? if($lastGenBy) {?>
	<span><?=$Lang['eEnrolment']['LastGenerated']?>: <?=$gen_data['Club_Generate_Performance_DateTime']?> (<?=$lastGenBy?>)</span>
	<? } ?>
	<p class="spacer"></p>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Generate'], "button", "click_gen();") ?>
	<?= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "click_back();") ?>
<p class="spacer"></p>
</div>    

<?

intranet_closedb();
$linterface->LAYOUT_STOP();

?>