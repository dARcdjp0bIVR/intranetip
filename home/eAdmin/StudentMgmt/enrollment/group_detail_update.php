<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
$lf = new libfilesystem();

$AppStart = intranet_htmlspecialchars($AppStart);
$AppEnd = intranet_htmlspecialchars($AppEnd);
$GpStart = intranet_htmlspecialchars($GpStart);
$GpEnd = intranet_htmlspecialchars($GpEnd);
$Description = intranet_htmlspecialchars($Description);

$setting_file = "$intranet_root/file/clubenroll_setting.txt";
$desp_file = "$intranet_root/file/clubenroll_desp.txt";
$file_content = $lf->file_read($setting_file);
$lines = explode("\n",$file_content);
$lines[0] = $basicmode;
switch ($basicmode)
{
        case 0:           # Disabled
             break;
        case 1:           # Simple
             $lines[1] = $AppStart." ".$enrolStartHour.":".$enrolStartMin.":00";
             $lines[2] = $AppEnd." ".$enrolEndHour.":".$enrolEndMin.":00";
             $lines[3] = $GpStart;
             $lines[4] = $GpEnd;
             $lines[5] = $defaultMin;
             $lines[6] = $defaultMax;
             $lines[8] = $EnrollMax;
             $lines[9] = $enrollPersontype;
             break;
        case 2:           # Advanced
             $lines[1] = $AppStart." ".$enrolStartHour.":".$enrolStartMin.":00";
             $lines[2] = $AppEnd." ".$enrolEndHour.":".$enrolEndMin.":00";
             $lines[3] = $GpStart;
             $lines[4] = $GpEnd;
             $lines[5] = $defaultMin;
             $lines[6] = $defaultMax;
             $lines[7] = $tiebreak;
             $lines[8] = $EnrollMax;
             $lines[9] = $enrollPersontype;
             break;
}

$updatedcontent = implode("\n",$lines);
$lf->file_write($updatedcontent,$setting_file);
$lf->file_write($Description, $desp_file);
if ($clearRecord==1)
{
    intranet_opendb();
    $li = new libdb();
    $sql = "DELETE FROM INTRANET_ENROL_STUDENT";
    $li->db_db_query($sql);
    $sql = "DELETE FROM INTRANET_ENROL_GROUPSTUDENT";
    $li->db_db_query($sql);
    $sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Approved = 0";
    $li->db_db_query($sql);
}
header("Location: basic.php?msg=2");
?>