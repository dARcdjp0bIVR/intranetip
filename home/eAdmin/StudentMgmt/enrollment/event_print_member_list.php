<?php
#using : 


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

if(!$plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]  && (!$libenroll->IS_CLUB_PIC()))
{
	echo "You have no priviledge to access this page.";
	exit;
}

if ($plugin['eEnrollment'])
{
	
    if ($libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
        
        ($EventInfoArr['AddToPayment']==1)? $allowPayment=false : $allowPayment=true;

        if($filter=="" || !isset($filter)){
			$filter = 2;
		}	
        if (!isset($field))
        {
	        $field = ($filter==2)? 0 : 2;
        }

		switch ($field){
			case 0: $field = 0; break;
			case 1: $field = 1; break;
			case 2: $field = 2; break;
			case 3: $field = 3; break;
			case 4: $field = 4; break;
			case 5: $field = 5; break;
			default: $field = 0; break;
		}
		if ($order=="" || !isset($order))
		{
			$order = ($order == 0) ? 1 : 0;
		}
		
        $quotaDisplay = "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"4\" border=\"0\">";
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\" width=\"30%\">";	
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\">{$eEnrollment['act_name']}</td><td>".$EventInfoArr[3]."</td></tr>";
// 		if ($EventInfoArr[1] == 0)
// 		{
// 			
// 			$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\">$i_ClubsEnrollment_ActivityQuota1 </td><td>".$i_ClubsEnrollment_NoLimit."</td></tr>";
// 		}
// 		else
// 		{
// 			$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\">$i_ClubsEnrollment_ActivityQuota1 </td><td>".$EventInfoArr[1]."</td></tr>";
// 		}
// 		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\">$i_ClubsEnrollment_GroupApprovedCount </td><td>".$EventInfoArr[2]."</td></tr>";
		$quotaDisplay .= "</table>";
        
		//for selection of Student/Teacher/Staff/All
        $filterbar = "";
        $filter_array = array (
			   array (3,$i_identity_parent),
               array (2,$i_identity_student),
               array (1,$i_identity_teachstaff),
               );
		$filterbar .= getSelectByArray($filter_array,"name=filter onChange=\"document.form1.target='_self'; document.form1.action='event_member_index.php'; document.form1.submit();\"",$filter, 0, 1);
		
        $searchbar = $libenroll->GET_STUDENT_NAME_SERACH_BOX($keyword, "keyword", $eEnrollment['enter_name']);
		$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "javascript:document.form1.keyword.value = Trim(document.form1.keyword.value); document.form1.submit()");
		
		//do not search for the initial textbox text
		if ($keyword == $eEnrollment['enter_name']) $keyword = "";
		
		if (isset($keyword))
		{
			$conds = "AND (a.EnglishName LIKE '%{$keyword}%' OR a.ChineseName LIKE '%{$keyword}%')";
		}
		
		$EventStudents = sizeof($libenroll->GET_ENROLLED_EVENTSTUDENT($EnrolEventID));
		
		# TABLE INFO
		$name_field = getNameFieldByLang2("a.");
		
		if ($filter == 2)
		{
			$li->field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance","Achievement","Comment","ActiveMemberStatus","iu.UserID");
			$li->fieldorder2 = ",ClassName, ClassNumber, iees.EventStudentID";
		}
		else
		{
			$li->field_array = array("StudentName", "RoleTitle", "iu.UserID");
			$li->fieldorder2 = ", StudentName";
		}

		
		$display = "<div class='table_board'>";
		$display .="<table class='common_table_list_v30 view_table_list_v30'>";
		if ($filter == 2)
		{
			$display .= "<tr>";
			$display .= "<th>#</th>";
			$display .= "<th>". $i_general_class ."</th>";
			$display .= "<th>". $i_ClassNumber ."</th>";
			$display .= "<th>". $i_UserStudentName ."</th>";
			$display .= "<th>". $i_admintitle_role ."</th>";
			$display .= "<th>". $eEnrollment['attendence'] ."</th>";
			$display .= "<th>". $eEnrollment['performance'] ."</th>";
			$display .= "<th>". $Lang['eEnrolment']['Achievement'] ."</th>";
			$display .= "<th>". $eEnrollment['comment'] ."</th>";
			$display .= "<th>". $eEnrollment['active']."/".$eEnrollment['inactive'] ."</th>";
			$display .= "</tr>";
		}
		else
		{
			$display .= "<tr>";
			$display .= "<th>#</th>";
			$display .= "<th>". $i_UserName ."</th>";
			$display .= "<th>". $i_admintitle_role ."</th>";
			$display .= "</tr>";
		}
		
		
		$sql = $libenroll->Get_Management_Activity_Participant_Sql(array($EnrolEventID), array(2), $filter, $keyword, $ForExport=1);
		if ($filter == 2)
		{	
			$sql .= " order by iu.ClassName, iu.ClassNumber";
		}
		else
		{
			$sql .= " order by StudentName, iu.ClassNumber";
		}
		$result = $libenroll->returnArray($sql);
		
		if(empty($result))
		{
			$display .= '<tr>';
			$display .= '<td valign="top" colspan="9" align="center">'. $Lang['General']['NoRecordAtThisMoment'] .'</td>';
			$display .= '</tr>';	
		}
		
		for($i=0;$i<sizeof($result);$i++)
		{
			$display .= '<tr>';
			$display .= '<td valign="top">'.($i+1).'</td>';
			
			if ($filter == 2)
			{
				//list($thisClassName, $thisClassNumber, $thisStudentName, $thisTitle, $thisPerformance, $thisComment, $thisActiveMemberStatus, $thisUserID) = $result[$i];
//				debug_pr($result);
				$thisUserID = $result[$i]['UserID'];
				$thisClassName = $result[$i]['ClassName'];
				$thisClassNumber = $result[$i]['ClassNumber'];
				$thisStudentName = $result[$i]['StudentName'];
				$thisTitle = $result[$i]['RoleTitle'];
				$thisPerformance = $result[$i]['Performance'];
				$thisAchievement = nl2br($result[$i]['Achievement']);
				$thisComment = nl2br($result[$i]['Comment']);
				$thisActiveMemberStatus = $result[$i]['ActiveMemberStatus'];
				
				$DataArr['StudentID'] = $thisUserID;
				$DataArr['EnrolEventID'] = $EnrolEventID;
				$this_attendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);
				
				$display .= '<td valign="top">'.$thisClassName.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisClassNumber.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisStudentName.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisTitle.'&nbsp;</td>';
				$display .= '<td valign="top">'.$this_attendance.'%&nbsp;</td>';
				$display .= '<td valign="top">'.$thisPerformance.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisAchievement.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisComment.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisActiveMemberStatus.'&nbsp;</td>';
			}
			else
			{
				list($thisName, $thisTitle) = $result[$i];
				$display .= '<td valign="top">'.$thisName.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisTitle.'&nbsp;</td>';
			}
			
			$display .= '</tr>';	
		}
		
		$display .= '</table>';
		$display .= "</div>"; 	
		
		$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'),1);
?>

<div class="table_board">
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr><td><?=$quotaDisplay?></td></tr>
</table>
<br />
	<?=$display?>
	<?=$RemarksTable?>
	
</div>


<?
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>