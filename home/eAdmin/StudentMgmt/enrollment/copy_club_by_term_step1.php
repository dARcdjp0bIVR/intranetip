<?php
// Using: 

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
if (!$libenroll->hasAccessRight($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");


$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
	if (!$isEnrolAdmin)
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$linterface = new interface_html();
	
	$AcademicYearID = $_POST['AcademicYearID'];
	$Semester = $_POST['Semester'];
        
    # tags 
    $tab_type = "club";
    $current_tab = 1;
    include_once("management_tabs.php");
  
    $linterface->LAYOUT_START();
    
    
	echo $libenroll_ui->Copy_Club_By_Term_Step1_UI($AcademicYearID, $Semester);
?>

<script language="javascript">
$(document).ready( function() {
	js_Changed_From_Term_Selection();
});

function js_Changed_From_Term_Selection() {
	var excludeYearTermID = $('select#FromTermID').val();
	var defaultToYearTermID = $('select#ToTermID').val();
	
	$('div#ToTermSelDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Term_Selection',
			SelectionID: 'ToTermID',
			AcademicYearID: '<?=$AcademicYearID?>',
			YearTermID:	defaultToYearTermID,
			NoFirst: 1,
			ExcludeYearTermIDList: excludeYearTermID,
			OnChange: 'js_Reload_Club_Info_Table();'
		},
		function(ReturnData) {
			js_Reload_Club_Info_Table();
		}
	);
}

function js_Reload_Club_Info_Table() {
	$('div#ClubInfoDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"ajax_reload.php", 
		{
			Action: 'Copy_Club_By_Term_Club_Selection_Table',
			AcademicYearID: '<?=$AcademicYearID?>',
			FromYearTermID:	$('select#FromTermID').val(),
			ToYearTermID:	$('select#ToTermID').val()
		},
		function(ReturnData) {
			
		}
	);
}

function js_Change_Checkbox_Status(jsChecked, jsCheckBoxClass)
{
	$('input.' + jsCheckBoxClass + ':enabled').each( function() {
		$(this).attr('checked', jsChecked);
	})
	
	// enable / disable the "Copy Member" and "Copy Activity" checkboxes if the Copy Club option is changed
	if (jsCheckBoxClass=='CopyClubChk')
	{
		var jsDisabled = (jsChecked==true)? false : true;
		
		$('input.CopyMemberChk').each( function() {
			$(this).attr('disabled', jsDisabled);
		})
		
		$('input.CopyActivityChk').each( function() {
			$(this).attr('disabled', jsDisabled);
		})
	}
}

function js_Check_Uncheck_Global_Selection(jsChecked, jsGlobalID, jsEnrolGroupID)
{
	if (jsChecked == false)
		$('input#' + jsGlobalID).attr('checked', jsChecked);
		
	if (jsGlobalID == 'CopyClub_Global')
	{
		var jsDisabled = (jsChecked==true)? false : true;
		
		$('input#CopyMemberChk_' + jsEnrolGroupID).attr('disabled', jsDisabled);
		$('input#CopyActivityChk_' + jsEnrolGroupID).attr('disabled', jsDisabled);
	}
}

function js_Go_Back() {
	window.location = "group.php";
}

function js_Submit_Form() {

	if ($('input.CopyClubChk:checked').length == 0)
	{
		alert('<?=$Lang['eEnrolment']['CopyClub']['jsWarningArr']['SelectAtLeastOneClub']?>');
		return false;
	}
	
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'copy_club_by_term_step2.php';
	ObjForm.submit();
}
</script>

<?
	$linterface->LAYOUT_STOP();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>