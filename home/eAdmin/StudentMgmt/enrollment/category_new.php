<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);	

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageCategorySetting";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['mgt_cat'], "", 1);
        
        $linterface->LAYOUT_START();

        
$CategoryArr = $lc->GET_CATEGORYINFO($CategoryID);

# page navigation (leave the array empty if no need)
if ($CategoryID != "") {
	$PAGE_NAVIGATION[] = array($button_edit." ".$eEnrollment['add_activity']['act_category'], "");
	$button_title = $button_save;
} else {
	$PAGE_NAVIGATION[] = array($button_new." ".$eEnrollment['add_activity']['act_category'], "");
	$button_title = $button_submit;
}


?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	if(!check_text(obj.CategoryName, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) return false;
	if(!check_text(obj.DisplayOrder, "<?php echo $i_alert_pleasefillin.$i_general_DisplayOrder; ?>.")) return false;

	obj.submit();
}
</SCRIPT>
<form name="form1" action="category_update.php" method="POST" enctype="multipart/form-data">
<br/>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
<tr><td>
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="25%"><?= $eEnrollmentMenu['cat_title']?> <span class="tabletextrequire">*</span></td>
		<td><input type="text" id="CategoryName" name="CategoryName" value="<?= htmlspecialchars($CategoryArr[1])?>" class="textboxtext"></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $i_general_DisplayOrder?> <span class="tabletextrequire">*</span></td>
		<td><input type="text" id="DisplayOrder" name="DisplayOrder" value="<?= $CategoryArr[2]?>" class="textboxnum"></td>
	</tr>
</table>

</td></tr>

</table>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_title, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='category.php'")?>
</div>
</td></tr>
</table>
<br/>
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="CategoryID" id="CategoryID" value="<?= $CategoryID?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />

</form>
<?= $linterface->FOCUS_ON_LOAD("form1.CategoryName") ?>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>