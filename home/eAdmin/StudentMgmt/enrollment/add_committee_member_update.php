<?
// using: anna
#########################################################
#
##########################################################

$PATH_WRT_ROOT="../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
// include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");


intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);

# all selected students for adding
// $data = unserialize(rawurldecode($data));
# error code for rejection
$result_ary = unserialize(rawurldecode($result_ary));

$libenroll = new libclubsenrol();

// $linterface = new interface_html();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($EnrolGroupID) && !$libenroll->IS_EVENT_PIC($EnrolEventID) && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
{
	//header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageMgtClub";
$tab_type = "club";

$current_tab = 3;


include_once("management_tabs.php");
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

// $EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);

$PAGE_NAVIGATION[] = array($eEnrollment['button']['add'], "");

$STEPS_OBJ[] = array($eEnrollment['select_student'], 0);
$STEPS_OBJ[] = array($eEnrollment['lookup_for_clashes'], 1);

// $linterface->LAYOUT_START();

$EnrolGroupID= $_POST['EnrolGroupID'];
$StudentIDArr = $_POST['student'];
$AcademicYearID = $_POST['AcademicYearID'];
$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
$thisEnrolGroupTitle = $libenroll->getClubInfoByEnrolGroupID($EnrolGroupID);
foreach($StudentIDArr as $StudentID){
    
    $sql = "SELECT max(Priority)
            FROM INTRANET_ENROL_COMMITTEE_STUDENT
            WHERE StudentID = '".$StudentID."' AND AcademicYearID = '$AcademicYearID'";
    $PriorityResult = $libenroll->returnVector($sql);   
    

    $Priority= $PriorityResult[0];
    
    $sql = "SELECT EnrolGroupID
            FROM INTRANET_ENROL_COMMITTEE_STUDENT
            WHERE StudentID = '".$StudentID."' and EnrolGroupID = '".$EnrolGroupID."'";
    $haveErnolResult = $libenroll->returnVector($sql);
    
    
    $enroledBefore = $haveErnolResult[0];

    if($enroledBefore){
        $sql = "UPDATE
                     INTRANET_ENROL_COMMITTEE_STUDENT
                SET
                    Priority ='".($Priority+1)."',
                    ApplyStatus ='1',
                    DateModified = now(),
                    StatusChangedBy = '".$_SESSION['UserID']."',
                    RecordStatus = '1'
                WHERE
                    EnrolGroupID = '".$EnrolGroupID."'
                    AND StudentID = '".$StudentID."'";
//         $ErrorAry[$StudentID] = 1;
        $SuccessAry[] = $libenroll->db_db_query($sql);	
        
    }else{
        
        $sql = "INSERT INTO INTRANET_ENROL_COMMITTEE_STUDENT
                (StudentID, GroupID, EnrolGroupID ,Priority, ApplyStatus ,RecordStatus,DateInput ,DateModified ,StatusChangedBy,AcademicYearID)
                Values
                ('".$StudentID."','".$GroupID."','".$EnrolGroupID."','".($Priority+1)."','1','1',now(),now(),'".$_SESSION['UserID']."', '".$AcademicYearID."') ";
     
        $SuccessAry[] = $libenroll->db_db_query($sql);	
    }     
}
header("Location: committee_member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&GroupID=$GroupID&field=$field&order=$order&filter=$filter&keyword=$keyword");
?>
