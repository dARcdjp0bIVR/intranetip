<?php
# using: 

############################################
#	Date:	2018-03-28 Pun
#			-  Fixed cannot display house name in drop down list
#
#	Date:	2017-02-20 Omas
#			-  fixing click std name -> back -> std name cannot show report problem.
#
#	Date:	2015-12-18 Kenneth
#			- $sys_custom['eEnrolment']['OverallPerformanceReport_showUserLogin'] flag to enable user Login display
#
#	Date:	2015-12-01 Kenneth
#			- Add Category Filter
#			- Change seperated sql to local function getStudentFromAllGroup(), getStudentFromAllEvent()
#
#
#	Date:	2015-06-02 Evan
#			- Replace the old style with UI standard
#
#	Date:	2010-11-22	YatWoon
#			- Add "House" Option
#			- change to IP25 layout standard
#			- add "Total Overal Performance"
#
############################################
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();




function getStudentFromAllGroup($userId, $targetCategory) {
	
	//debug_pr($targetCategory);
	#New filtering by Category
	if (empty($targetCategory)){
		$conds_category_group ='';
	}else{
		$conds_category_group = " AND d.GroupCategory IN ( '".implode("','", (array)$targetCategory)."' ) ";
		$specialJoinGroup = ' INNER JOIN INTRANET_ENROL_GROUPINFO as d ON  (a.GroupID = d.GroupID) ';
	}
	$sql = "SELECT a.Performance, b.Title, b.TitleChinese
	     			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID $specialJoinGroup
	     			WHERE a.UserID = '$userId' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."' $conds_category_group"; 

	return $sql;
}

function getStudentFromAllEvent($userId, $targetCategory){

	#New filtering by Category
	if (empty($targetCategory)){
		$conds_category_event ='';
	}else{
		$conds_category_event = " AND d.EventCategory IN ( '".implode("','", (array)$targetCategory)."' ) ";
		$specialJoinEvent = ' INNER JOIN INTRANET_ENROL_EVENTINFO as d ON  (a.EnrolEventID = d.EnrolEventID) ';
	}
	$sql = "SELECT a.Performance, b.EventTitle
	     	FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID $specialJoinEvent
	     	WHERE a.StudentID = '$userId'  AND a.RecordStatus = 2 $conds_category_event";
	
	return $sql;
}



$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$lg = new libgroup();

// Omas 2017-02-20 fixing click std name -> back -> std name cannot show report problem.
//if(!is_null($returnTargetCategory)){
if(!empty($TargetCategory)){
	//$targetCategory = explode(',',$returnTargetCategory);
	$targetCategory = explode(',',$TargetCategory);
}

//check if the user is a class teacher if the user is not a enrol admin/master
if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libteaching.php");
	$libTeaching = new libteaching($_SESSION['UserID']);
	$result = $libTeaching->returnTeacherClass($_SESSION['UserID'], Get_Current_Academic_Year_ID());
	
	if (count($result)==0)
	{
		$isClassTeacher = false;
	}
	else
	{
		$isClassTeacher = true;
		$targetClassID = $result[0][0];
	}
}
	
if ($plugin['eEnrollmentLite'] || (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$isClassTeacher) && (!$libenroll->IS_CLUB_PIC())))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

$LibUser = new libuser($UserID);



if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

if ($plugin['eEnrollment'])
{
	$lclass = new libclass();
	
	
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$isClassTeacher))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        //include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        
        $CurrentPage = "PagePerformanceReport";
		$CurrentPageArr['eEnrolment'] = 1;
       	$TAGS_OBJ[] = array($Lang['eEnrolment']['overall_perform'], "", 1);
       	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
       	
       	if (isset($studentID))
       	{
	     	//student view
	     	$studentView = true;
	     	
	     	//$classSelection = $targetClass;
	     	//$clubSelection = $targetClub;
	     	$sql = "SELECT ClassName, ClassNumber FROM INTRANET_USER WHERE UserID='$studentID'";
	     	$temp = $libenroll->returnArray($sql,2);
	     	$classSelection = $temp[0][0].' - '.$temp[0][1];
	     	
	     	$clubSelection = $libenroll->getClubInfoByEnrolGroupID($targetClub);
	     	$temp = $lg->returnGroup($targetHouse);
	     	$houseSelection = $temp[0]['Title'];
	     	
	     	//get name of the student
	     	$name_field = getNameFieldByLang();
	     	$sql = "SELECT ".$name_field." FROM INTRANET_USER WHERE UserID = '$studentID'";
	     	$result = $libenroll->returnArray($sql,1);
	     	$studentNameDisplay = $result[0][0];
	     	
	     	################################## Club Table ##################################
	     	//get all groups of student
//	     	$sql = "SELECT b.Title, a.Performance, b.TitleChinese
//	     			FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
//	     			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
	     	
	     
	     	//  	debug_pr($targetCategory);
	     	if(!empty($targetCategory)){
	     		$targetCategoryArr = explode(',',$targetCategory);
	     	}
	     	//debug_pr($targetCategoryArr);
	     	
	     	$sql = getStudentFromAllGroup($studentID,$targetCategoryArr);
	     	$clubPerformArr = $libenroll->returnArray($sql,2);
	     	//debug_pr($clubPerformArr);
	     	
	     	//construct club table title
	     	$clubDisplay = "<table width=\"96%\" cellpadding=\"5\" cellspacing=\"0\">";
			$clubDisplay .= "<tr><td>";
			$clubDisplay .= $linterface->GET_NAVIGATION2($eEnrollment['club']);
			$clubDisplay .= "</td></tr>";
			$clubDisplay .= "<tr>";
			$clubDisplay .= "<td width=\"50%\" class=\"tablebluetop tabletopnolink\">".$eEnrollment['club_name']."</td>
					  <td width=\"50%\" align=\"center\" class=\"tablebluetop tabletopnolink\">".$eEnrollment['performance']."</td>";
			         
		    $clubDisplay .= "</tr>";
		    
		    $total_col = 2;
		   	$numOfClub = sizeof($clubPerformArr);
		   	
		   	//construct table content
			$clubDisplay .= ($numOfClub==0) ? "<tr class='tablerow2'><td align='center' colspan='$total_col' class='tabletext' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
	     	
	     	//loop through each club record to construct table content
	     	$totalPerform = 0;
	     	for ($i=0; $i<$numOfClub; $i++)
	     	{
	     		$tr_css = ($i % 2 == 1) ? "tablebluerow2" : "tablebluerow1";
	     		
	     		if(empty($clubPerformArr[$i]['Performance']))
	     		{
	     			$curPerform = "&nbsp;";
	     		}
	     		else if (is_numeric($clubPerformArr[$i]['Performance']))
		     	{
			     	$curPerform = $clubPerformArr[$i]['Performance'];
			     	$totalPerform += $curPerform;
		     	}
		     	else
		     	{
			     	$curPerform = $clubPerformArr[$i]['Performance'];
		     	}
			 				
				$clubDisplay .= '<tr class="'.$tr_css.'">';
				$clubDisplay .= '<td valign="top" class="tabletext">'. ($intranet_session_language=="en"?$clubPerformArr[$i]['Title']:$clubPerformArr[$i]['TitleChinese']).'</td>
			          	  <td class="tabletext" align="center" valign="top">'.$curPerform.'</td>';
			    $clubDisplay .= '</tr>';
	     	}
	     	
	     	//display the total score of club performance
	     	$clubDisplay .= "<tr class=\"tablebluebottom\">";
			$clubDisplay .= "<td width=\"50%\" class=\"tabletext\">".$eEnrollment['total_club_performance']."</td>
					  <td width=\"50%\" align=\"center\" class=\"tabletext\">".$totalPerform."</td>";
			         
		    $clubDisplay .= "</tr>";	     	
     		$clubDisplay .= '</table>';
     		################################## Enf of Club Table ##################################
     		
     		################################## Activity Table #####################################
     		//debug_pr($targetCategoryArr);
     		
     		//get all activities of student
//	     	$sql = "SELECT b.EventTitle, a.Performance
//	     			FROM INTRANET_ENROL_EVENTSTUDENT as a LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID
//	     			WHERE a.StudentID = '$studentID'  AND a.RecordStatus = 2";
	     	$sql = getStudentFromAllEvent($studentID, $targetCategoryArr);
	     		     	$actPerformArr = $libenroll->returnArray($sql,2);
	     	
	     	//construct club table title
	     	$actDisplay .= "<table width=\"96%\" cellpadding=\"5\" cellspacing=\"0\">";
			$actDisplay .= "<tr><td>";
			$actDisplay .= $linterface->GET_NAVIGATION2($eEnrollment['activity']);
			$actDisplay .= "</td></tr>";
			$actDisplay .= "<tr>";
			$actDisplay .= "<td width=\"50%\" class=\"tablebluetop tabletopnolink\">".$eEnrollment['act_name']."</td>
					  <td width=\"50%\" align=\"center\" class=\"tablebluetop tabletopnolink\">".$eEnrollment['performance']."</td>";
			         
		    $actDisplay .= "</tr>";
		    
		    $total_col = 2;
		   	$numOfAct = sizeof($actPerformArr);
		   	
		   	//construct table content
			$actDisplay .= ($numOfAct==0) ? "<tr class='tablerow2'><td align='center' colspan='$total_col' class='tabletext' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
	     	
	     	//loop through each club record to construct table content
	     	$totalPerform = 0;
	     	for ($i=0; $i<$numOfAct; $i++)
	     	{
	     		$tr_css = ($i % 2 == 1) ? "tablebluerow2" : "tablebluerow1";
	     		
	     		if(empty($actPerformArr[$i]['Performance']))
	     		{
	     			$curPerform = "&nbsp;";
	     		}
	     		else if (is_numeric($actPerformArr[$i]['Performance']))
		     	{
			     	$curPerform = $actPerformArr[$i]['Performance'];
			     	$totalPerform += $curPerform;
		     	}
		     	else
		     	{
			     	$curPerform = $actPerformArr[$i]['Performance'];
		     	}
			 				
				$actDisplay .= '<tr class="'.$tr_css.'">';
				$actDisplay .= '<td valign="top">'.$actPerformArr[$i]['EventTitle'].'</td>
			          	  <td class="tabletext" align="center" valign="top">'.$curPerform.'</td>';
			    $actDisplay .= '</tr>';
	     	}
	     	
	     	//display the total score of club performance
	     	$actDisplay .= "<tr>";
			$actDisplay .= "<td width=\"50%\" class=\"tablebluebottom tabletext\">".$eEnrollment['total_act_performance']."</td>
					  <td width=\"50%\" align=\"center\" class=\"tablebluebottom tabletext\">".$totalPerform."</td>";
			         
		    $actDisplay .= "</tr>";	     	
     		$actDisplay .= '</table>';
     		
     		################################## End of Activity Table ##############################
       	}
       	else
       	{
	       	//class view
	       	$classView = true;
	       	if ($isClassTeacher)
	       	{
		       	$TitleField = Get_Lang_Selection('ClassTitleB5', 'ClassTitleEn');
		       	//get the class name
		       	$sql = "SELECT 
		       					yc.$TitleField, ycu.ClassNumber 
		       			FROM 
		       					YEAR_CLASS as yc
		       					INNER JOIN
		       					YEAR_CLASS_USER as ycu
		       					On (yc.YearClassID = ycu.YearClassID)
		       			WHERE 
		       					yc.YearClassID = '$targetClassID'
		       			Order By 
		       					ycu.ClassNumber
		       			";
		       	$result = $libenroll->returnArray($sql,1);
		       	$classSelection = $result[0][0];
		       	//$targetClass = ($showTarget=='club') ? $result[0][0]." - ".$result[0][1] : $result[0][0];
		       	$targetClass = $result[0][0];
	       	}
	       	else
	       	{
		       	//class-based selection drop-down list
				$classSelection = $lclass->getSelectClassID("name=\"targetClassID\"", $targetClassID);
				
				$tempClub = $libenroll->GET_GROUPINFO('', Get_Current_Academic_Year_ID(), $ClubType='') ;
		       	$clubInfoArray = array();
		       	$p = 0;
		       	
		       	for($i=0; $i<sizeof($tempClub); $i++) {
			     	$clubInfoArray[$p][] = $tempClub[$i]['EnrolGroupID']; 	
			     	$clubInfoArray[$p][] = $intranet_session_language=="en" ? $tempClub[$i]['TitleWithSemester'] : $tempClub[$i]['TitleChineseWithSemester'];
			     	$p++;
		       	}
				
		       	$clubSelection = getSelectByArray($clubInfoArray," name=\"targetClub\"",$targetClub);
	       	}
	       	
	       	##### House Group Info 
		    $temp_HouseGroup = $lg->returnGroupsByCategory(4, Get_Current_Academic_Year_ID());
		    $p = 0;
	       	for($i=0; $i<sizeof($temp_HouseGroup); $i++) {
			     	$HouseInfoArray[$p][] = $temp_HouseGroup[$i]['GroupID']; 	
			     	$HouseInfoArray[$p][] = Get_Lang_Selection($temp_HouseGroup[$i]['TitleChinese'],$temp_HouseGroup[$i]['TitleEn']);
			     	$p++;
		       	}
	       	$houseSelection = getSelectByArray($HouseInfoArray," name=\"targetHouse\"",$targetHouse);
	       	       		
	       	//show all student if a target class is set
	       	if($showTarget=="class" && isset($targetClassID) && $targetClassID!="")
	       	{
	        	//$studentNameArr = $lclass->getStudentNameListByClassName($targetClass);	//UserID, $name_field, ClassNumber
	        	$YearClassObj = new year_class($targetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);	
	        	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
	        	
	        	$studentNameArr = $YearClassObj->ClassStudentList;
	        	//debug_pr($studentNameArr);
	       	} else if($showTarget=="club" && isset($targetClub) && $targetClub!="") {
		     	$studentIDArr = array();
		     	$studentNameArr = array();
		     	
		     	/*
		     	$name_field = getNameFieldByLang('USR.');
		     	$sql = "SELECT 
								USR.UserID, USR.ClassName, USR.ClassNumber, $name_field as StudentName
						FROM 
								INTRANET_USERGROUP USRGP 
		     					LEFT OUTER JOIN 
								INTRANET_USER USR ON (USRGP.UserID=USR.UserID) 
								LEFT OUTER JOIN 
								INTRANET_GROUP GP ON (GP.GroupID=USRGP.GroupID)
		     					LEFT OUTER JOIN 
								INTRANET_ENROL_GROUPINFO iegi ON (iegi.EnrolGroupID = USRGP.EnrolGroupID)
		     			WHERE 
								iegi.EnrolGroupID = '$targetClub' And GP.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
								AND
								USR.RecordType = 2
		     			ORDER BY USR.ClassName, USR.ClassNumber";
		     	$studentInfo = $libenroll->returnArray($sql);
		     	
		     	for($i=0; $i<sizeof($studentInfo); $i++) {
					list($uID, $clsName, $clsNo, $name) = $studentInfo[$i];
					$studentIDArr[] = $uID;
					$studentNameArr[$i]['UserID'] = $uID;
					$studentNameArr[$i]['StudentName'] = $name;
					$studentNameArr[$i]['ClassNumber'] = $clsName." - ".$clsNo;
					
		     	}
		     	*/
		     	$studentNameArr = $libenroll->Get_Club_Member_Info(array($targetClub), $PersonTypeArr=array(2), Get_Current_Academic_Year_ID(), $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
	       		
	       	} else if($showTarget=="house" && isset($targetHouse) && $targetHouse!="") {
		       	$IndicatorPrefix = '<span class="tabletextrequire">';
				$IndicatorSuffix = "</span>";
					
		       	$temp_stuInfo = $lg->returnGroupUser4Display($targetHouse, 2);
		       	for($i=0; $i<sizeof($temp_stuInfo); $i++) {
					$studentIDArr[] = $temp_stuInfo[$i]['UserID'];
					$studentNameArr[$i]['UserID'] = $temp_stuInfo[$i]['UserID'];
					
					$studentNameArr[$i]['StudentName'] = (empty($temp_stuInfo[$i]['ClassName']) ? $IndicatorPrefix."^".$IndicatorSuffix:"") . $temp_stuInfo[$i][3];
					$studentNameArr[$i]['ClassName'] = $temp_stuInfo[$i]['ClassName'];
					$studentNameArr[$i]['ClassNumber'] = $temp_stuInfo[$i]['ClassNumber'];
		     	}
		     	
		     	
	       	}
	       	
	       	
	       	
	       	$SumOver = 0;
		     	//loop through all students to get performance and construct studentInfoArr
		     	
		     	for ($i=0; $i<sizeOf($studentNameArr); $i++)
		     	{
			     	$studentID = $studentNameArr[$i]['UserID'];
			     	$studentInfoArr[$i]['StudentID'] = $studentNameArr[$i]['UserID'];
			     	$studentInfoArr[$i]['UserLogin'] = $studentNameArr[$i]['UserLogin'];
			     	$studentInfoArr[$i]['StudentName'] = $studentNameArr[$i]['StudentName'];
			     	$studentInfoArr[$i]['ClassName'] = ($studentNameArr[$i]['ClassName'] == "")? $Lang['General']['EmptySymbol'] : $studentNameArr[$i]['ClassName'];
			     	$studentInfoArr[$i]['ClassNumber'] = ($studentNameArr[$i]['ClassNumber'] == "")? $Lang['General']['EmptySymbol'] : $studentNameArr[$i]['ClassNumber'];
			     	
			     	
			     	//get club performance
			     	//$targetCategory = $_GET['targetCategory'];
			     	$sql = getStudentFromAllGroup($studentID, $targetCategory);
			     	$clubPerformArr = $libenroll->returnArray($sql,1);
			     	 
			   		
			     	 
			     	//get activity performance
			     	$sql = getStudentFromAllEvent($studentID, $targetCategory);
			     	$actPerformArr = $libenroll->returnArray($sql,1);
			     	
			     	
			     	//loop through all performances to get the total mark
			     	$totalMark = 0;
			     	$emptyClubPerform=0;
			     	for ($j=0; $j<sizeof($clubPerformArr); $j++)
			     	{
				     	if (is_numeric($clubPerformArr[$j][0]))
				     	{
					     	$totalMark += $clubPerformArr[$j][0];
				     	}
				     	else if(empty($clubPerformArr[$j][0]))
				     	{
				     		$emptyClubPerform++;
				     	}
			     	}
			     	$emptyActPerform=0;
			     	for ($j=0; $j<sizeof($actPerformArr); $j++)
			     	{
				     	if (is_numeric($actPerformArr[$j][0]))
				     	{
					     	$totalMark += $actPerformArr[$j][0];
				     	}
				     	else if(empty($actPerformArr[$j][0]))
				     	{
				     		$emptyActPerform++;
				     	}
			     	}
			     	$studentInfoArr[$i]['EmptyClubPerform'] = $emptyClubPerform;
			     	$studentInfoArr[$i]['EmptyActPerform'] = $emptyActPerform;
			     	$studentInfoArr[$i]['TotalPerformance'] = $totalMark;
			     	$SumOver += $totalMark;
		     	}
		     	
		     	//construct table title
// 		     	$display = "<table width=\"96%\" cellpadding=\"5\" cellspacing=\"0\" border=1>";
     			$display = "<div class='table_board'>";
		     	$display .="<table class='common_table_list_v30 view_table_list_v30'>";
				$display .= "<tr>";
					$display .= "<th width=\"10%\" warp=\"nowrap\">".$i_UserClassName."</th>";
					$display .= "<th style=\"text-align:center\" width=\"10%\" warp=\"nowrap\">".$i_UserClassNumber."</th>";
					if($sys_custom['eEnrolment']['OverallPerformanceReport_showUserLogin']){
						$display .= "<th style=\"text-align:center\">".$Lang['General']['UserLogin']."</th>";
					}
					$display .= "<th>".$i_UserStudentName."</th>";
					$display .= "<th width=\"20%\" style=\"text-align:center\">".$Lang["eEnrolment"]["NoOfEmptyClubPerformance"]."</th>";
					$display .= "<th width=\"20%\" style=\"text-align:center\">".$Lang["eEnrolment"]["NoOfEmptyActPerformance"]."</th>";
					$display .= "<th width=\"20%\" style=\"text-align:center\">".$Lang['eEnrolment']['perform_score_comment']."</th>";
				$display .= "</tr>";
			    
			    $total_col = 5;
			   	$StudentSize = sizeof($studentInfoArr);
			   	//debug_pr($studentInfoArr);
			   	################################
			   	#	Contruct Get Parameter String for target Category
			   	################################
			   	$targetCategoryGetString = '';
			   	$targetCategoryGetString .= '&targetCategory=';
			   	$count = sizeOf($targetCategory);
			   	for($i=0;$i<$count;$i++){
			   		$targetCategoryGetString .= $targetCategory[$i];
			   		if($i!=$count-1){
			   			$targetCategoryGetString .= ',';
			   		}
			   	}
			   	//debug_pr($targetCategoryGetString);
			   	//construct table content
			   	$display .= ($StudentSize==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
	        	    
			    for ($i=0; $i<$StudentSize; $i++)
			    {
					$display .= '<tr>';
						$display .= '<td class="tabletext" valign="top">'.$studentInfoArr[$i]['ClassName'].'</td>';
						$display .= '<td align="center" class="tabletext" valign="top">'.$studentInfoArr[$i]['ClassNumber'].'</td>';
						if($sys_custom['eEnrolment']['OverallPerformanceReport_showUserLogin']){
							$display .= '<td align="center" class="tabletext" valign="top">'.$studentInfoArr[$i]['UserLogin'].'</td>';
						}
						$display .= '<td valign="top"><a class="tablelink" href=overall_performance_report.php?showTarget='.$showTarget.'&studentID='.$studentInfoArr[$i]['StudentID'].'&targetClassID='.$targetClassID.'&targetClub='.$targetClub.'&targetHouse='.$targetHouse.$targetCategoryGetString.'>'.$studentInfoArr[$i]['StudentName'].'</a></td>';
					    $display .= '<td class="tabletext" align="center" valign="top"><a class="tablelink" href=overall_performance_report.php?showTarget='.$showTarget.'&studentID='.$studentInfoArr[$i]['StudentID'].'&targetClassID='.$targetClassID.'&targetClub='.$targetClub.'&targetHouse='.$targetHouse.$targetCategoryGetString.'>'.$studentInfoArr[$i]['EmptyClubPerform'].'</a></td>';
					    $display .= '<td class="tabletext" align="center" valign="top"><a class="tablelink" href=overall_performance_report.php?showTarget='.$showTarget.'&studentID='.$studentInfoArr[$i]['StudentID'].'&targetClassID='.$targetClassID.'&targetClub='.$targetClub.'&targetHouse='.$targetHouse.$targetCategoryGetString.'>'.$studentInfoArr[$i]['EmptyActPerform'].'</a></td>';
					    $display .= '<td class="tabletext" align="center" valign="top">'.$studentInfoArr[$i]['TotalPerformance'].'</td>';
			    	$display .= '</tr>';
				}
				
				// Display Sum Overall Performance
				$display .= '<tr>';
						$display .= '<td colspan='. $total_col .' align=right>'. $Lang['eEnrolment']['total_overall_perform'] .'</td>';
					    $display .= '<td align="center" valign="top"><b>'.$SumOver.'</b></td>';
			    	$display .= '</tr>';
				
			    $display .= '</table>';
			    $display .= "</div>"; 
	       	
       	}
    }      

	$linterface->LAYOUT_START();
	$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
	
	
	### Club Category Selection
	$ClubCategoryArr = $libenroll->GET_CATEGORY_LIST();
//	debug_pr($ClubCategoryArr);

	if(count($targetCategory)==0){
		$targetCategory = Get_Array_By_Key($ClubCategoryArr, 'CategoryID');
	}
	
	$CategorySelection = getSelectByArray($ClubCategoryArr," id=\"targetCategory\" name=\"targetCategory[]\" multiple size=10 ",$targetCategory,0,1,"- ".$i_general_please_select." -");
	$CategorySelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetCategory',1)");
	
//	if(count($_GET['targetCategory'])==0){
//		$CategorySelection .= "<script>js_Select_All_With_OptGroup('targetCategory',1)</script>";	//Defalut select all
//	}
	
	$CategorySelection .= $linterface->spacer();  	 
	$CategorySelection .= $linterface->MultiSelectionRemark(); 
	
	$categoryRowHTML = '';
	if (!$studentView){
		$categoryRowHTML .= "<tr>";
		$categoryRowHTML .= "<td class='field_title'>".$linterface->RequiredSymbol().$eEnrollment['category']."</td>";
		$categoryRowHTML .= "<td valign='top' class='tabletext''>";
			$categoryRowHTML .= $CategorySelection;	//Club selection list
		$categoryRowHTML .= "</td>";
		$categoryRowHTML .= "</tr>";
	}else if($studentView && $useCategory) {
		
	}else{
		//debug_pr($targetCategoryArr);
		$categoryRowHTML .= "<tr>";
		$categoryRowHTML .= "<td class='field_title'>".$eEnrollment['category']."</td>";
		$categoryRowHTML .= "<td valign='top' class='tabletext''>";
		$count = sizeOf($targetCategoryArr);
		//debug_pr($count);
		for($i=0;$i<$count;$i++){
			$catInfo = $libenroll->GET_CATEGORYINFO($targetCategoryArr[$i]);
			//debug_pr($catInfo);
			$categoryRowHTML .= $catInfo['CategoryName'];
			$categoryRowHTML .= '</br>';
		}
		
		$categoryRowHTML .= "</td>";
		$categoryRowHTML .= "</tr>";
	}
	
	
	
?>
<script language="javascript">
function showSpan(span) {
	document.getElementById(span).style.display="inline";
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
}
function checkForm(){
	var checked ='';
	if($('input#target01').is(':checked')){
		checked = 'class';
	}else if($('input#target02').is(':checked')){
		checked = 'club';
	}else{
		checked = 'house';
	}
	var selectorClass = $('#spanClass > select').val();
	var selectorClub = $('#spanClub > select').val();
	var selectorHouse = $('#spanHouse > select').val();
	if(selectorClass==''&&checked=='class'){
		alert("Please enter target class");
		return;
	}else if(selectorClub==''&&checked=='club'){
		alert("Please enter target club");
		return;
	}else if(selectorHouse==''&&checked=='house'){
		alert("Please enter target house");
		return;
	}
	var selectedValue = $('#targetCategory').val();
	if(selectedValue == null){
		alert("<?= $Lang['eEnrolment']['Warning']['SelectCategory'] ?>");
	}else{
		form1.submit();
	}
}
</script>
	<br>
	<form name="form1" action="overall_performance_report.php" method="get">
		<table class="form_table_v30">
			<tr>
				<td class="field_title"><?=$linterface->RequiredSymbol().$i_Discipline_Target?></td>
				<td valign="top" class="tabletext">
					<? if(($isClassTeacher && !($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) || ($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']))) {?>
						<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target01" value="class" <? if($showTarget=="" || $showTarget=="class") echo "checked"; ?> onClick="showSpan('spanClass'); hideSpan('spanClub');hideSpan('spanHouse');"><? } ?><? if((!$studentView) || $showTarget=="class") { ?><label for="target01"><?=$i_general_class?></label><? } ?>
					<? } ?>
					<? if($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) {?>
						<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target02" value="club" <? if($showTarget=="club") echo "checked";?> onClick="showSpan('spanClub'); hideSpan('spanClass');hideSpan('spanHouse');"><? } ?><? if((!$studentView) || $showTarget=="club") { ?><label for="target02"><?=$eEnrollmentMenu['club']?></label><? } ?>
						<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target03" value="house" <? if($showTarget=="house") echo "checked";?> onClick="showSpan('spanHouse'); hideSpan('spanClass');hideSpan('spanClub');"><? } ?><? if((!$studentView) || $showTarget=="house") { ?><label for="target03"><?=$i_House?></label><? } ?>
					<? } ?>
					<br>
					<div id="spanClass" style="display:<? if($showTarget=="" || $showTarget=="class") {echo "inline";} else {echo "none";} ?>">
						<?= $classSelection ?>
					</div>
					<div id="spanClub" style="display:<? if($showTarget=="club") {echo "inline";} else {echo "none";} ?>">
						<?=$clubSelection?>
					</div>
					<div id="spanHouse" style="display:<? if($showTarget=="house") {echo "inline";} else {echo "none";} ?>">
						<?=$houseSelection?>
					</div>
				</td>
			</tr>
			
			<?=$categoryRowHTML?>
			<tr><td style="border-bottom:0px;">
				<?= $linterface->MandatoryField()?>
			</td></tr>
			<?
			if ($studentView)
			{
			?>
			<tr>
				<td width="10%" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_UserStudentName?></td>
				<td valign="top" class="tabletext">
					<?= $studentNameDisplay ?>
				</td>
			</tr>
			<?
			} else {
			?>
				
			<tr>
				<td colspan="2" valign="top" style="border-bottom: 0px;">
					<div class="edit_bottom_v30">
						<p class="spacer"></p>
							<?= $linterface->GET_ACTION_BTN($eEnrollment['generate_or_update'], "button", "checkForm();")?>&nbsp;	
						<p class="spacer"></p>
					</div>
				</td>
			</tr>
			
			
			<? }
			if ($classView && ((isset($targetClassID) && $targetClassID!="") || (isset($targetClub) && $targetClub!="") || (isset($targetHouse) && $targetHouse!="")))
			{
			?>
				<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr>
					<td colspan="2" valign="top">
						<?= $linterface->GET_LNK_EXPORT_IP25("overall_performance_export.php?showTarget=$showTarget&type=classPerform&class=$targetClassID&club=$targetClub&house=$targetHouse&targetCategory=".implode(',',$targetCategory)) ?>
						<?= $linterface->GET_LNK_PRINT_IP25("javascript:newWindow('print.php?showTarget=$showTarget&type=classPerform&class=$targetClassID&club=$targetClub&house=$targetHouse&targetCategory=".implode(',',$targetCategory)."',4);") ?>
						<?= toolBarSpacer() ?>
						<?
						if($showTarget=="class")
							$showPrintText = $eEnrollment['print_class_enrolment_result'];
						else if($showTarget=="club")
							$showPrintText = $eEnrollment['print_club_enrolment_result'];
						else if($showTarget=="house")
							$showPrintText = $Lang['eEnrolment']['print_house_enrolment_result'];
						?>
						<?= $linterface->GET_LNK_PRINT_IP25("javascript:newWindow('print.php?showTarget=$showTarget&type=classEnrol&class=$targetClassID&club=$targetClub&house=$targetHouse&targetCategory=".implode(',',$targetCategory)."',4);", $showPrintText) ?>
					</td>
				</tr>
			<?
			}
			?>
		</table>
		<?
		if ($classView && isset($showTarget))
		{
		?>
			<?=$display?>
			<br />
			<?=$RemarksTable?>
				
		<?
		}
		else if ($studentView)
		{
		?>
			<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr><td class="dotline" colspan="2"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr><td valign="top">
				
					<?= $linterface->GET_LNK_PRINT("javascript:newWindow('print.php?showTarget=$showTarget&type=student&studentID=$studentID&club=$targetClub&targetCategory=".$targetCategory."',4);", '', '', '', '', 0)?>
				</td></tr>
			</table>
			<?=$clubDisplay?>
			<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			</table>
			<br />
			<?=$actDisplay?>
			<br />
			<?=$RemarksTable?>
		<?
		}
		?>
		
		<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
			<tr><td colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
			<tr><td align="center" colspan="6">
				<?
				if ($classView)
				{
				?>

				<?
				}
				else if ($studentView)
				{
				?>
					<input type="hidden" name="showTarget" value="<?=$showTarget?>">
					
					<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='overall_performance_report.php?showTarget=$showTarget&targetClassID=$targetClassID&targetClub=$targetClub&TargetCategory=".$targetCategory."'")?>&nbsp;
				<?
				}
				?>
			</td></tr>
		</table>
	</form>
	
	<br>
	
	
<?
	intranet_closedb();
	if($showTarget == "class")
		echo $linterface->FOCUS_ON_LOAD("form1.targetClassID");
	else if($showTarget == "club")
		echo $linterface->FOCUS_ON_LOAD("form1.targetClub");
	else if($showTarget == "house")
		echo $linterface->FOCUS_ON_LOAD("form1.targetHouse");
    $linterface->LAYOUT_STOP();
}
else
{
	intranet_closedb();
?>
	You have no priviledge to access this page.
<?
}
?>