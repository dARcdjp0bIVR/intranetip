<?php
# Modify :

/********************************************************
 * Modification Log
 * 
 * 2018-04-30 Anna
 * Created this page
 * 
 ********************************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT.'includes/liblog.php');


intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lgs = new libgeneralsettings();
$json = new JSON_obj();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || !$sys_custom['eEnrolment']['CommitteeRecruitment'])
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$SendEmail = $SendEmail=='1'?'1':'0';

$SettingArr = array();
$SettingArr['Committee_Recruitment_AcademicYearID'] = NULL;
$SettingArr['Committee_Recruitment_Start'] = NULL;
$SettingArr['Committee_Recruitment_End'] = NULL;
$SettingArr['Committee_Recruitment_Send_Email'] = NULL;
$SettingArr['Committee_Apply_Max'] = NULL;


$SettingArr['Committee_Recruitment_AcademicYearID'] = $AcademicYearID;
$SettingArr['Committee_Recruitment_Start']= $CommitteeRecruitmentStart." ".$CommitteeRecruitmentStartHour.":".$CommitteeRecruitmentStartMin.":00";
$SettingArr['Committee_Recruitment_End'] = $CommitteeRecruitmentEnd." ".$CommitteeRecruitmentEndHour.":".$CommitteeRecruitmentEndMin.":00";
$SettingArr['Committee_Recruitment_Send_Email'] = $SendEmail;
$SettingArr['Committee_Apply_Max'] = $CommitteeApplyMax; // 0 unlimited 

$Success['EditSettings'] = $lgs->Save_General_Setting($libenroll->ModuleTitle, $SettingArr);


$SettingArr['ModifiedBy'] = $_SESSION['UserID'];
$jsonSettingArr = $json->encode($SettingArr);


# insert log
$liblog = new liblog();
$SuccessArr['Log_Change'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Change_Committee_Recruitment_Setting', $jsonSettingArr, 'GENERAL_SETTING');

header("Location: committee_recruitment_setting.php?msg=2");
?>