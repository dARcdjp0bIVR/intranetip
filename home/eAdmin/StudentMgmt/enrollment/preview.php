<?php

# using: rita

#################################
#	Date:   2012-12-04 Rita
#			copy from eSurvey for customization
#
#	Date:	2011-02-23	YatWoon
#			Add "Display question number"
#################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();
$lform = new libform();


$libenroll = new libclubsenrol($AcademicYearID);
$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	if (($LibUser->isStudent()) || ($LibUser->isParent())) {
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	elseif ($libenroll->hasAccessRight($_SESSION['UserID']))
	{    
	  	if($libenroll->enableActivitySurveyFormRight()){
			$MODULE_OBJ['title'] = $Lang['eSurvey']['SurveyPreview'];
			$linterface = new interface_html("popup.html");
			$linterface->LAYOUT_START();
	  	}
	}    
}


?>
<br />
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="javascript" src="/templates/forms/form_edit.js"></script>

<script language="Javascript">
var replyslip = '<?=$Lang['eSurvey']['ReplySlip']?>';
</script>

<table width="80%" align="center" border="0">
<form name="ansForm" method="post" action="update.php">
        <input type="hidden" name="qStr" value="">
        <input type="hidden" name="aStr" value="">
</form>
<tr>
	<td align="left">
		<script language="Javascript">	
		<?=$lform->getWordsInJS()?>
		
		var DisplayQuestionNumber = 0;
        if(window.opener.document.form1.DisplayQuestionNumber.checked)
        	DisplayQuestionNumber = 1;
                	
		var sheet= new Answersheet();
		// attention: MUST replace '"' to '&quot;'
		sheet.qString=window.opener.document.form1.qStr.value;
		sheet.aString="";
		//edit submitted application
		sheet.mode=1;
		sheet.answer=sheet.sheetArr();
		//sheet.templates=form_templates;
		document.write(editPanel());	
		</script>
	</td>
</tr>
<tr>
	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
</tr>
<tr>
	<td align="center">
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "window.close(); return false;","cancelbtn") ?>
	</td>
</tr>
</table>
<br />

<?php
$linterface->LAYOUT_STOP();
?>
