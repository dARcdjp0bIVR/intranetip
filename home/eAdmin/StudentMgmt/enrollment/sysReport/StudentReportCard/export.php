<?php
# using:
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$classIdAry = $_POST['classIdAry'];
$selectSemester = $_POST['Semester'];
$seperator = "<br />";
$AcademicYearID = Get_Current_Academic_Year_ID();

$contentArr = array();
if(isset($selectSemester))
{
	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $Semester);
	
	$term_group_list = array();
	if(!empty($term_group_list_temp))
	{
		foreach($term_group_list_temp as $k=>$d)
			$term_group_list[] = $d['EnrolGroupID'];
	}
}

$HeaderArr = array();
$HeaderArr = array($Lang['eEnrolment']['SysRport']['AttendanceReport']['Class'],$Lang['eEnrolment']['SysRport']['AttendanceReport']['Number'],$Lang['eEnrolment']['SysRport']['SchoolECA']['Unit'],$Lang['eEnrolment']['SysRport']['SchoolECA']['Role']);


$studentNameArr = array();
foreach($classIdAry as $thistargetClassID)
{
	$YearClassObj = new year_class($thistargetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
	$studentNameArr =  array_merge($studentNameArr,$YearClassObj->ClassStudentList);
	$GroupStudentAssoc[$thistargetClassID] = Get_Array_By_Key($YearClassObj->ClassStudentList, 'UserID');
}

$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');

$StudentInUsed = $libenroll->Get_Student_Info($studentIDArr);
// /$studentIDArr = Get_Array_By_Key($StudentInUsed, 'UserID');

$studentClubArr = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIDArr,'');



$studentInfoArr = array();
foreach ($studentNameArr as $key => $studentDataArr)
{
	$thisUserID = $studentDataArr['UserID'];
	$studentInfoArr[$thisUserID]['Status'] = $studentDataArr['ArchiveUser'];
	$studentInfoArr[$thisUserID]['ClassName'] = $studentDataArr['ClassName'];
	$studentInfoArr[$thisUserID]['ClassNumber'] = $studentDataArr['ClassNumber'];
}


foreach ($studentClubArr as $key => $clubDataArr)
{
	
	list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
	$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
	//	$thisTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
	$roleTitle = ($roleTitle!="") ? $roleTitle : "--";
	# filter the group if the group do not have gathering within the selected period
	if ($usePeriod && !in_array($thisEnrolGroupID, $clubWithinPeriodArr))
		continue;
		
		if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
		{
			continue;
		}
		
		if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
		{
			$studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
		}
		

		
		$studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle, $roleTitle);
}

foreach((array)$GroupStudentAssoc as $ClassClubID => $StudentIDArr)
{

	$StudentIDArr = array_values((array)$StudentIDArr);
	
	
		// 		$total_col = 5;
	$StudentSize = sizeof($StudentIDArr);
	
	
	for ($i=0; $i<$StudentSize; $i++)
	{
		$thisStudentID = $StudentIDArr[$i];
		
		$thisClassName = $studentInfoArr[$thisStudentID]['ClassName'];
		$thisClassNumber = $studentInfoArr[$thisStudentID]['ClassNumber'];
		
		$thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
		
		# build clubs link
		$sizeClubArr = count($thisClubArr);
	
		if($studentInfoArr[$thisUserID]['Status'] != '1'){
		    for ($j=0; $j<$sizeClubArr; $j++)
		    {
		        list($thisEnrolGroupID, $thisTitle,$thisRoleTitle) = $thisClubArr[$j];
		        $contentArr[] = array($thisClassName,$thisClassNumber,$thisTitle,$thisRoleTitle);
		    }
        }
		
	}
}

foreach($HeaderArr as &$header){
	$header = standardizeFormPostValue($header);
}
foreach($contentArr as &$contentRow){
	foreach($contentRow as &$data){
		$data = standardizeFormPostValue($data);
	}
}


$numOfRow = count($contentArr);
for($i=0;$i<$numOfRow;$i++){
	$numOfColumn = count($contentArr[$i]);
	for($j=0;$j<$numOfColumn;$j++){
		$contentArr[$i][$j]=str_replace($seperator,"\n",$contentArr[$i][$j]);
	}
}

$export_text = $lexport->GET_EXPORT_TXT($contentArr, $HeaderArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "student_report_card.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>