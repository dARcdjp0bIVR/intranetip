<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$linterface = new interface_html();

# setting the current page
$CurrentPage = "IndividualECAReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['SysRport']['IndividualECAReport'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();



if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$semester_data = getSemesters($AcademicYearID);
	
if(!empty($_POST['Semester'])){
	$Semester = $_POST['Semester'];
}else{
	$Semester = array();
	$Semester [] = 0;
	foreach((array)$semester_data as $_value => $_semesterName){
		$Semester[] = $_value;
	}
}
$SemesterFilter = $libenroll->Get_Term_Selection_CheckBox('Semester', $AcademicYearID, $Semester);


// Option Div after result table created
$divName = 'reportOptionOuterDiv';
$tdId = 'tdOption';
$x = '';
$x .= '<div id="showHideOptionDiv" style="display:none;">'."\n";
	$x .= '<table style="width:100%;">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td id="tdOption" class="report_show_option">'."\n";
				$x .= '<span id="spanShowOption_'.$divName.'">'."\n";
					$x .= $linterface->Get_Show_Option_Link("javascript:showOption();", '', '', 'spanShowOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<span id="spanHideOption_'.$divName.'" style="display:none">'."\n";
					$x .= $linterface->Get_Hide_Option_Link("javascript:hideOption();", '', '', 'spanHideOption_reportOptionOuterDivBtn');
				$x .= '</span>'."\n";
				$x .= '<div id="'.$divName.'" style="display:none;">'."\n";
				$x .= '</div>'."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";
$x .= '</div>'."\n";
$htmlAry['reportOptionOuterDiv'] = $x;


// Option Div
$divName = 'reportOptionDiv';
$x = '';
	$x .= '<div id="'.$divName.'">'."\n";
		$x .= '<div>'."\n";
			$x .= '<table id="filterOptionTable" class="form_table_v30">'."\n";

			# Class
				$libclass = new libclass();
				$studentSource_classSel = $libclass->getSelectClassID('id="classSel" name="classIdAry[]" multiple="multiple" size="10" onchange="changedClassSelection();"', $selected="", $DisplaySelect=2);
				$studentSource_classSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('classSel', 1, 'changedClassSelection();');");
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['General']['Class'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($studentSource_classSel, $studentSource_classSelectAllBtn);
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";

				
				
			#Student 
				$studentSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('studentSel', 1);");
				$x .= '<tr id="studentTr">'."\n";
					$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['student'].'</td>'."\n";
					$x .= '<td>'."\n";
						$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div('', $studentSelectAllBtn, 'studentSelectionSpan');
					$x .= '</td>'."\n";
				$x .= '</tr>'."\n";
				
				
				# Period
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['ClubType'].'</td>'."\n";
					$x .= '<td valign="top" class="tabletext">'.$SemesterFilter.'<span class="tabletextremark"></span></td>'."\n";
				$x .= '</tr>'."\n";

				#Upper Limit
				$x .= '<tr>'."\n";
					$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['SysRport']['IndividualECA']['StudentType'].'</td>'."\n";
					$x .='<td valign="top" class="tabletext">';
						$x .=$linterface->Get_Radio_Button("UpperLimit", "StudentType", 'UpperLimit', true, "", $Lang['eEnrolment']['SysRport']['IndividualECA']['UpperLimit']);
						$x .=$linterface->Get_Radio_Button("AllStudent", "StudentType", 'AllStudent', false, "", $Lang['eEnrolment']['SysRport']['IndividualECA']['AllStudent']);
					$x .='</td>';
				$x .= '</tr>'."\n";
			$x .= '</table>'."\n";
		$x .= '</div>'."\n";
		$x .= '<br style="clear:both;" />'."\n";

		$x .= '<div style="text-align:left;">'."\n";
			$x .= $linterface->MandatoryField();
		$x .= '</div>'."\n";
		$x .= '<br style="clear:both;" />'."\n";

		$x .= '<div class="edit_bottom_v30">'."\n";
			$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "refreshReport();");
		$x .= '</div>'."\n";
	$x .= '</div>'."\n";
		$htmlAry['reportOptionDiv'] = $x;

// Tool bar
$x = '';
$x .= '<div id="contentToolDiv" class="content_top_tool" style="display:none;">'."\n";
	$x .= '<div class="Conntent_tool">'."\n";
		$x .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
		$x .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['contentToolDiv'] = $x;

// Result Div
$htmlAry['reportResultDiv'] = '<div id="reportResultDiv"></div>';

$linterface->LAYOUT_START();
?>
<script type="text/javascript">
$(document).ready(function () {
// 	changeTerm($("#selectYear").val());
	js_Select_All('classSel', 1, '');
	document.getElementById("termCheckBox_global").checked = true;
	changedClassSelection();
});

function changedClassSelection(){
	
	var selectionId = 'classSel';
	var selectedIdAry = new Array();
	$('select#' + selectionId + ' option:selected').each( function() {
		selectedIdAry[selectedIdAry.length] = $(this).val();
	});
	var selectedIdList = selectedIdAry.join(',');
	$('span#studentSelectionSpan').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"../ajax_reload.php", 
			{ 
				Action: 'studentSelection',
				SelectedIdList: selectedIdList,
				studentSelId: 'studentSel',
				studentSelName: 'studentIdAry[]'
			},
			function(ReturnData) {
				js_Select_All('studentSel', 1);
			}
	);
}

function checkForm(){
	var classArr = [];
	$('#classSel :selected').each(function(i, selected){ 
	  classArr[i] = $(selected).val(); 
	});	
	//check class
	if(classArr==''){
		alert("Please select at least one class");
		return false;
	}

	var check;
	check = false;

	$('.termCheckBox').each(function(){
		if($(this).attr('checked')){
			check = true;
		}
	});

	if(check){
		return true;
	}else{
		alert("<?=$Lang['eEnrolment']['jsWarning']['NoClubTypeIsSelected'] ?>");
		return false;
	}
}


function refreshReport() {
	
	var isValid = checkForm();

	if(isValid){
		Block_Document();
		if (Trim($('div#reportOptionOuterDiv').html()) == '') {
			hideOption();
			$('div#showHideOptionDiv').show();
			$('div#contentToolDiv').show();
		}else{
			$('a#spanHideOption_reportOptionOuterDivBtn')[0].click();
		}
		$('div#reportResultDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		$.ajax({
			url:      	"ajax_reload.php",
			type:     	"POST",
			data:     	$("#form1").serialize() + '&RecordType=reportResult',
			success:  	function(xml) {
							$('div#reportResultDiv').html(xml);
							Scroll_To_Top();
							UnBlock_Document();
			}
		});
	}
}
function showOption(){
	$('div#reportOptionDiv').show();
	$('#spanShowOption_reportOptionOuterDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').show();
	$('#tdOption').removeClass( "report_show_option" );
}
function hideOption(){
	$('div#reportOptionDiv').hide();
	$('#spanHideOption_reportOptionOuterDiv').hide();
	$('#spanShowOption_reportOptionOuterDiv').show();
	$('#tdOption').addClass( "report_show_option" );
}
function goExport() {
	$('form#form1').attr('target', '_self').attr('action', 'export.php').submit();
}

function goPrint() {
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
}
</script>
<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['reportOptionOuterDiv']?>
	
	
	<?=$htmlAry['reportOptionDiv']?>
	<br />
	<?=$htmlAry['contentToolDiv']?>
	<br />
	<?=$htmlAry['reportResultDiv']?>
	<br />
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>