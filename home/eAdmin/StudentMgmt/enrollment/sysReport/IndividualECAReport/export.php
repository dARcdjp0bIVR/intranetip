<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');

intranet_auth();
intranet_opendb();

$libclass = new libclass();
$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$studentIdAry = $_POST['studentIdAry'];
$classIdAry = $_POST['classIdAry'];
$selectSemesterAry = $_POST['Semester'];
$StudentType = $_POST['StudentType'];

if(isset($selectSemesterAry))
{
	//villa B111852
	//$term_group_list_temp = $libenroll->getGroupInfoList(0, $Semester, $EnrolGroupIDArr='', $getRegOnly=0, $ParCond="", $AcademicYearID);
	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $selectSemesterAry);
	
	$term_group_list = array();
	if(!empty($term_group_list_temp))
	{
		foreach($term_group_list_temp as $k=>$d)
			$term_group_list[] = $d['EnrolGroupID'];
	}
}

$HeaderArr = array();
$HeaderArr = array($Lang['eEnrolment']['SysRport']['IndividualECA']['ClassNumber'],$Lang['eEnrolment']['SysRport']['SchoolECA']['EnglishName'],$Lang['eEnrolment']['SysRport']['SchoolECA']['ChineseName'],$Lang['eEnrolment']['SysRport']['SchoolECA']['Unit'],$Lang['eEnrolment']['SysRport']['SchoolECA']['Role'],$Lang['eEnrolment']['UnitRolePoint']['Point']);

$AcademicYearID = Get_Current_Academic_Year_ID();
$fcm = new form_class_manage();
$FormList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);
$FormListAry = BuildMultiKeyAssoc($FormList,'YearClassID');

$ClassSize = sizeof($classIdAry);
$contentArr= array();
for($k=0;$k<$ClassSize;$k++){
	
	
	$YearClassID = $classIdAry[$k];
	$WebSAMSCode = $FormListAry[$YearClassID]['Form_WebSAMSCode'];
	
// 	$FormNumber = preg_replace('/\D/s', '', $WebSAMSCode);
// 	$SuggestUpperAry[1] = 24;
// 	$SuggestUpperAry[2] = 24;
// 	$SuggestUpperAry[3] = 30;
// 	$SuggestUpperAry[4] = 36;
// 	$SuggestUpperAry[5] = 36;
// 	$SuggestUpperAry[6] = 20;
// 	$SuggestUpper = $SuggestUpperAry[$FormNumber];
	
	$SuggestUpper = $libenroll->GetFormExceedPoint($AcademicYearID,$WebSAMSCode);
	
	$studentInfoAry = $libclass->getStudentByClassId($YearClassID);
	$thisClassStudentIDAry = Get_Array_By_Key($studentInfoAry,'UserID');
	
	$studentClubArr= $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIdAry,$targetCategory);
	//	$studentClubArr = BuildMultiKeyAssoc($studentClubArr,'UserID');
	
	$studentInfoArr = array();
	
	
	foreach ($studentClubArr as $key => $clubDataArr)
	{
		
		list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
		$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
		$roleTitle = ($roleTitle!="") ? $roleTitle : "--";
		
		if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
		{
			continue;
		}
		
		if(in_array($thisUserID,$thisClassStudentIDAry)){
    		
    		if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
    		{
    			$studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
    		}
    		$rolePointArr = $libenroll->Get_Role_Point_By_Role_Title($thisEnrolGroupID,$roleTitle);
    		$Point = "";
    		if($rolePointArr[0]['Point'] != null){
    			$Point = $rolePointArr[0]['Point'];
    		}
		    $studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle, $roleTitle,$Point);
	
		}
	}


	

	for($i=0;$i<sizeof($studentIdAry);$i++){
		$thisStudentID = $studentIdAry[$i];
		
		if(in_array($thisStudentID,$thisClassStudentIDAry)){
		 
    		$libuser =new libuser($thisStudentID);
    		$StudentEnglishName = $libuser->EnglishName;
    		$StudentChineseName = $libuser->ChineseName;
    		$ClassName = $libuser->ClassName;
    		$ClassNumber = $libuser->ClassNumber;
    		
    		
    		$thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
    		
    		
    		$sizeClubArr = sizeof($thisClubArr);
    		
    		if($sizeClubArr != 0){
    		    
    		
        		$PointTotal = '';
        	//	$RecordArr = '';
        		for ($j=0; $j<$sizeClubArr; $j++)
        		{
        			$thisRolePoint = $thisClubArr[$j][3];
        		//	list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
        			$PointTotal+= $thisRolePoint;
        		}
        		
        		if($StudentType=='UpperLimit' && $PointTotal <= $SuggestUpper){
        		    
//         		    $contentArr[] = array($ClassName.$ClassNumber,$StudentEnglishName,$StudentChineseName,'','','');
//         		    $contentArr[] = array($i_no_record_exists_msg,'','','','','');
        		}
        		else{
        		 
        			for ($j=0; $j<$sizeClubArr; $j++)
        			{	
        				list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];

        				
        				if($j==0){
        					$contentArr[] = array($ClassName.$ClassNumber,$StudentEnglishName,$StudentChineseName,'','','');
        					$contentArr[] =array('','','',$thisTitle,$thisRoleTitle,$thisRolePoint);       					
        				
        				}
        				
        				else if($j == $sizeClubArr-1){       				   
        					$contentArr[] =array('','','',$thisTitle,$thisRoleTitle,$thisRolePoint);
        					$contentArr[] =array('','','','',$Lang['eEnrolment']['SysRport']['IndividualECA']['TotalPoint'],$PointTotal);
        					$contentArr[] =array('','','','',$Lang['eEnrolment']['SysRport']['IndividualECA']['SuggestPoint'],$SuggestUpper);			
        				}
        				else{
        					$contentArr[] =array('','','',$thisTitle,$thisRoleTitle,$thisRolePoint);	
        				}
        				
        				if($sizeClubArr == 1){
        				    $contentArr[] =array('','','','',$Lang['eEnrolment']['SysRport']['IndividualECA']['TotalPoint'],$PointTotal);
        				    $contentArr[] =array('','','','',$Lang['eEnrolment']['SysRport']['IndividualECA']['SuggestPoint'],$SuggestUpper);
        				}
				
        			}
        		}
//     		}
    		}
    		
    	
	   }
	}
}


foreach($HeaderArr as &$header){
	$header = standardizeFormPostValue($header);
}
foreach($contentArr as &$contentRow){
	foreach($contentRow as &$data){
		$data = standardizeFormPostValue($data);
	}
}


$numOfRow = count($contentArr);
for($i=0;$i<$numOfRow;$i++){
	$numOfColumn = count($contentArr[$i]);
	for($j=0;$j<$numOfColumn;$j++){
		$contentArr[$i][$j]=str_replace($seperator,"\n",$contentArr[$i][$j]);
	}
}

$export_text = $lexport->GET_EXPORT_TXT($contentArr, $HeaderArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "individual_eca_report.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>