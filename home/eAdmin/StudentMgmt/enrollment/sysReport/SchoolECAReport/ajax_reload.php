<?php 


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$linterface = new interface_html();

$RecordType = $_POST['RecordType'];
$classIdAry = $_POST['classIdAry'];

$selectSemester = $_POST['Semester'];


$AcademicYearID = Get_Current_Academic_Year_ID();

$AcademicYearObj = new academic_year($AcademicYearID);
$AcademicYearName = $AcademicYearObj->Get_Academic_Year_Name();

if ($RecordType == 'reportResult') {
	
// 	$numOfClassId = count($classIdAry);
	
	
	if(isset($selectSemester))
	{
		//villa B111852
		//$term_group_list_temp = $libenroll->getGroupInfoList(0, $Semester, $EnrolGroupIDArr='', $getRegOnly=0, $ParCond="", $AcademicYearID);
		$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $Semester);
		
		$term_group_list = array();
		if(!empty($term_group_list_temp))
		{
			foreach($term_group_list_temp as $k=>$d)
				$term_group_list[] = $d['EnrolGroupID'];
		}
	}
	
	$studentNameArr = array();
	foreach($classIdAry as $thistargetClassID)
	{
		$YearClassObj = new year_class($thistargetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
		$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
		$studentNameArr =  array_merge($studentNameArr,$YearClassObj->ClassStudentList);
		$GroupStudentAssoc[$thistargetClassID] = Get_Array_By_Key($YearClassObj->ClassStudentList, 'UserID');
	}
	
	$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
// 	if (!isset($useCategory)){
// 		unset($targetCategory);
// 	}
// 	if($targetCategory==''&&$useCategory=='on'){
// 		$studentClubArr = array();
// 	}else{
		$studentClubArr = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIDArr,'');
//	}
	$studentInfoArr = array();
	foreach ($studentNameArr as $key => $studentDataArr)
	{
		$thisUserID = $studentDataArr['UserID'];
		$studentInfoArr[$thisUserID]['StudentName'] = $studentDataArr['StudentName'];
		$studentInfoArr[$thisUserID]['ClassName'] = $studentDataArr['ClassName'];
		$studentInfoArr[$thisUserID]['ClassNumber'] = $studentDataArr['ClassNumber'];
	}
	
	foreach ($studentClubArr as $key => $clubDataArr)
	{
		
		list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
		$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
	//	$thisTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
		$roleTitle = ($roleTitle!="") ? $roleTitle : "--";
			
			//						if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
			if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
			{
				continue;
			}
			
			if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
			{
				$studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
			}
			$rolePointArr = $libenroll->Get_Role_Point_By_Role_Title($thisEnrolGroupID,$roleTitle);
			$Point = "";
			if($rolePointArr[0]['Point'] != null){
				$Point = $rolePointArr[0]['Point'];
			}
		
			
			$studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle, $roleTitle,$Point);
	}
	
	foreach((array)$GroupStudentAssoc as $ClassClubID => $StudentIDArr)
	{
	
		$ObjClass = new year_class($ClassClubID);
		$GroupName = $ObjClass->Get_Class_Name();

		$StudentIDArr = array_values((array)$StudentIDArr);
		
		##### Header
		$display .= "<br><center>
					<table width='1000px' style='" . $pageBreakCss . " border:0px; margin: 0 auto; font-size: 22px; '>";
		# Table Name
						$display .= "<tr>";
							$display .= "<td colspan='2' style='line-height:24px;' align='center'>";
								$display .= GET_SCHOOL_NAME()." ECA Record of $GroupName (". $AcademicYearName.")<br/>";
							$display .= "</td>";
						$display .= "</tr>";
					
						$display .= "<tr>";
							$display .= "<td>";
								$display .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
									$display .= "<thead><tr>";
									$display .= "
													<th>".$Lang['eEnrolment']['SysRport']['SchoolECA']['StudentCode'] ."</td>
													<th>".$Lang['eEnrolment']['SysRport']['SchoolECA']['EnglishName']."</td>
													<th>".$Lang['eEnrolment']['SysRport']['SchoolECA']['ChineseName']."</td>
													<th>".$Lang['eEnrolment']['SysRport']['SchoolECA']['Unit']."</td>
													<th>".$Lang['eEnrolment']['SysRport']['SchoolECA']['Role']."</td>
													<th>".$Lang['eEnrolment']['UnitRolePoint']['Point']."</td>
													";
				
						$display .= "</tr></thead>";
			
		#### Header End ########

						$StudentSize = sizeof($StudentIDArr);
		
						if($StudentSize ==0){
							$display .= '<tr>';
								$display .= '<td class="tabletext" valign="top" colspan="6">'.$i_no_record_exists_msg.'</td>
											 ';
							$display .= '</tr>';
						}
		

						for ($i=0; $i<$StudentSize; $i++)
						{
							$thisStudentID = $StudentIDArr[$i];
							$libuser =new libuser($thisStudentID);
							$StudentEnglishName = $libuser->EnglishName;
							$StudentChineseName = $libuser->ChineseName;
						//	$thisStudentName = $studentInfoArr[$thisStudentID]['StudentName'];
							$thisClassName = $studentInfoArr[$thisStudentID]['ClassName'];
							$thisClassNumber = $studentInfoArr[$thisStudentID]['ClassNumber'];
							
							$thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
			
		
			
							$display .='<tr>';
								$display .='<td class="tabletext" valign="top" colspan="6">'.$thisClassName.$thisClassNumber.'</td>';
							$display .='</tr>';
							
							$display .= '<tr>';
								$display .= '<td> </td>
											 <td class="tabletext" valign="top">'.$StudentEnglishName.'</td>
											 <td valign="top">'.$StudentChineseName.'</td>';
									          	  	
			
							# build clubs link
							$sizeClubArr = count($thisClubArr);
							if($sizeClubArr  == 0){
								$display .= '<td class="tabletext" valign="top" colspan="3">'.$i_no_record_exists_msg.'</td>
											 ';
								$display .= '</tr>';
							}else{
								$thisClubLink = "";
								$RoleTitle = "";
								$RolePoint = "";
								$ClubRolePointAry = array();
								for ($j=0; $j<$sizeClubArr; $j++)
								{
									list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
									$thisClubLink .= $thisTitle."\n";
									if ($j < $sizeClubArr-1 )
									{
										$thisClubLink .= "<br />\n";
									}
									
									$RoleTitle .=$thisRoleTitle."\n";
									if ($j < $sizeClubArr-1 )
									{
										$RoleTitle.= "<br />\n";
									}
									
									$RolePoint .=$thisRolePoint;
									if ($j < $sizeClubArr-1 )
									{
										$RolePoint.= "<br />\n";
									}
								}
			
							
								$display .= '<td class="tabletext" valign="top">'.$thisClubLink.'</td>
											<td class="tabletext"  valign="top">'.$RoleTitle.'</td>
											<td class="tabletext"  valign="top">'.$RolePoint.'</td>';
						
								$display .= '</tr>';
						}
					}
						$display .= '</table>';
				$display .= "</td>";
			$display .= "</tr>";
	$display .= '</table></center>';
	}
	echo $display;
}
?>