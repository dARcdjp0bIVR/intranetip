<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$classIdAry = $_POST['classIdAry'];
$selectSemester = $_POST['Semester'];
$seperator = "<br />";

$contentArr = '';
	if(isset($selectSemester))
	{
		//villa B111852
		//$term_group_list_temp = $libenroll->getGroupInfoList(0, $Semester, $EnrolGroupIDArr='', $getRegOnly=0, $ParCond="", $AcademicYearID);
		$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $Semester);
		
		$term_group_list = array();
		if(!empty($term_group_list_temp))
		{
			foreach($term_group_list_temp as $k=>$d)
				$term_group_list[] = $d['EnrolGroupID'];
		}
	}
	
	$studentNameArr = array();
	foreach($classIdAry as $thistargetClassID)
	{
		$YearClassObj = new year_class($thistargetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
		$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
		$studentNameArr =  array_merge($studentNameArr,$YearClassObj->ClassStudentList);
		$GroupStudentAssoc[$thistargetClassID] = Get_Array_By_Key($YearClassObj->ClassStudentList, 'UserID');
	}
	
	$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
	
// 	if (!isset($useCategory)){
// 		unset($targetCategory);
// 	}
// 	if($targetCategory==''&&$useCategory=='on'){
// 		$studentClubArr = array();
// 	}else{
		$studentClubArr = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIDArr,'');
// 	}
	
	$studentInfoArr = array();
	foreach ($studentNameArr as $key => $studentDataArr)
	{
		$thisUserID = $studentDataArr['UserID'];
// 		$studentInfoArr[$thisUserID]['StudentName'] = $studentDataArr['StudentName'];
		$studentInfoArr[$thisUserID]['ClassName'] = $studentDataArr['ClassName'];
		$studentInfoArr[$thisUserID]['ClassNumber'] = $studentDataArr['ClassNumber'];
	}
	
	foreach ($studentClubArr as $key => $clubDataArr)
	{
		
		list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
		$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
		//	$thisTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
		$roleTitle = ($roleTitle!="") ? $roleTitle : "--";
		# filter the group if the group do not have gathering within the selected period
		if ($usePeriod && !in_array($thisEnrolGroupID, $clubWithinPeriodArr))
			continue;
			
			//						if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
			if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
			{
				continue;
			}
			
			if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
			{
				$studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
			}
			
			$rolePointArr = $libenroll->Get_Role_Point_By_Role_Title($thisEnrolGroupID,$roleTitle);
			
			$Point = "";
			if($rolePointArr[0]['Point'] != null){
				$Point = $rolePointArr[0]['Point'];
			}
			
			
			$studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle, $roleTitle,$Point);
	}
	$HeaderArr = array();
	$HeaderArr = array($Lang['eEnrolment']['SysRport']['SchoolECA']['StudentCode'] ,$Lang['eEnrolment']['SysRport']['SchoolECA']['EnglishName'],$Lang['eEnrolment']['SysRport']['SchoolECA']['ChineseName'],$Lang['eEnrolment']['SysRport']['SchoolECA']['Unit'],$Lang['eEnrolment']['SysRport']['SchoolECA']['Role'],$Lang['eEnrolment']['UnitRolePoint']['Point']);
	foreach((array)$GroupStudentAssoc as $ClassClubID => $StudentIDArr)
	{
		
		$ObjClass = new year_class($ClassClubID);
		$GroupName = $ObjClass->Get_Class_Name();
		
		$StudentIDArr = array_values((array)$StudentIDArr);

		
// 		$total_col = 5;
		$StudentSize = sizeof($StudentIDArr);
		
	
		for ($i=0; $i<$StudentSize; $i++)
		{
			$thisStudentID = $StudentIDArr[$i];
			
			$libuser =new libuser($thisStudentID);
			$StudentEnglishName = $libuser->EnglishName;
			$StudentChineseName = $libuser->ChineseName;
			
			$thisClassName = $studentInfoArr[$thisStudentID]['ClassName'];
			$thisClassNumber = $studentInfoArr[$thisStudentID]['ClassNumber'];
			
			$thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
			

			
			
			# build clubs link
			$sizeClubArr = count($thisClubArr);
			$thisClubLink = "";
			$RoleTitle = "";
			$RolePoint = "";
			$ClubRolePointAry = array();
			for ($j=0; $j<$sizeClubArr; $j++)
			{
				list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
				
				if($j==0){
					$contentArr[] = array($thisClassName.$thisClassNumber,'','','','','');
					$contentArr[] = array('',$StudentEnglishName,$StudentChineseName,$thisTitle,$thisRoleTitle,$thisRolePoint);
				}else{
					$contentArr[] = array('',$StudentEnglishName,$StudentChineseName,$thisTitle,$thisRoleTitle,$thisRolePoint);
				}
			}
			
		}
		
	}
	

foreach($HeaderArr as &$header){
	$header = standardizeFormPostValue($header);
}
foreach($contentArr as &$contentRow){
	foreach($contentRow as &$data){
		$data = standardizeFormPostValue($data);
	}
}


$numOfRow = count($contentArr);
for($i=0;$i<$numOfRow;$i++){
	$numOfColumn = count($contentArr[$i]);
	for($j=0;$j<$numOfColumn;$j++){
		$contentArr[$i][$j]=str_replace($seperator,"\n",$contentArr[$i][$j]);
	}
}

$export_text = $lexport->GET_EXPORT_TXT($contentArr, $HeaderArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "school_eca_report.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>