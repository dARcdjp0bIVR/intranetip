<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');

intranet_auth();
intranet_opendb();


$libclass = new libclass();
$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');



$RecordType = $_POST['RecordType'];
$studentIdAry = $_POST['studentIdAry'];
$classIdAry = $_POST['classIdAry'];
$selectSemesterAry = $_POST['Semester'];
// 	$StudentType = $_POST['StudentType'];

if(isset($selectSemesterAry))
{
	$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $selectSemesterAry);
	
	$term_group_list = array();
	if(!empty($term_group_list_temp))
	{
		foreach($term_group_list_temp as $k=>$d)
			$term_group_list[] = $d['EnrolGroupID'];
	}
}


$HeaderArr = array();
$HeaderArr = array($Lang['eEnrolment']['SysRport']['SchoolECA']['StudentCode'],
					$Lang['eEnrolment']['SysRport']['AttendanceReport']['Class'],
					$Lang['eEnrolment']['SysRport']['SchoolECA']['EnglishName'],
					$Lang['eEnrolment']['SysRport']['SchoolECA']['ChineseName'],
					$Lang['eEnrolment']['SysRport']['ECAPoint'],
					$Lang['eEnrolment']['SysRport']['UpperLimit'],
					$Lang['eEnrolment']['SysRport']['ExceededBy'],
// 					$Lang['eEnrolment']['SysRport']['FollowUpBy'] ,
// 					$Lang['eEnrolment']['SysRport']['Result'],
					$Lang['eEnrolment']['SysRport']['Consequence'],
					$Lang['eEnrolment']['SysRport']['NewECAPoints'],
					$Lang['eEnrolment']['SysRport']['ExceededBy']);


$AcademicYearID = Get_Current_Academic_Year_ID();
$fcm = new form_class_manage();
$FormList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);



$FormListAry = BuildMultiKeyAssoc($FormList,'YearClassID');
$SuggestUpper = array();

$ResultInfo = '';
$ClassSize = sizeof($classIdAry);
for($k=0;$k<$ClassSize;$k++){
	$YearClassID = $classIdAry[$k];
	$WebSAMSCode = $FormListAry[$YearClassID]['Form_WebSAMSCode'];
	
	$SuggestUpper = $libenroll->GetFormExceedPoint($AcademicYearID,$WebSAMSCode);
	$studentInfoAry = $libclass->getStudentByClassId($YearClassID);
	
	$thisClassStudentIDAry = Get_Array_By_Key($studentInfoAry,'UserID');
	
	$studentClubArr= $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIdAry,$targetCategory);
	
	$studentInfoArr = array();
	
	
	foreach ($studentClubArr as $key => $clubDataArr)
	{
		
		list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
		$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
		$roleTitle = ($roleTitle!="") ? $roleTitle : "--";
		
		if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
		{
			continue;
		}
		if(in_array($thisUserID,$thisClassStudentIDAry)){
    		if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
    		{
    			$studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
    		}
    		$rolePointArr = $libenroll->Get_Role_Point_By_Role_Title($thisEnrolGroupID,$roleTitle);
    		$Point = "";
    		if($rolePointArr[0]['Point'] != null){
    			$Point = $rolePointArr[0]['Point'];
    		}
	
    		$studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle, $roleTitle,$Point);
	    }
	}
	

// /	$contentArr = '';
    for($i=0;$i<sizeof($studentIdAry);$i++){
    		
    		
    		$NoResult = array();
    		$thisStudentID = $studentIdAry[$i];
    		if(in_array($thisStudentID,$thisClassStudentIDAry)){
    		    
    		
    		$libuser =new libuser($thisStudentID);
    		$StudentEnglishName = $libuser->EnglishName;
    		$StudentChineseName = $libuser->ChineseName;
    		$ClassName = $libuser->ClassName;
    		$ClassNumber = $libuser->ClassNumber;
    		
    		
    		$thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
    		
    		
    		$sizeClubArr = sizeof($thisClubArr);
    		$thisClubLink = '';
    		$RoleTitle = '';
    		$RolePoint='';
    		$PointTotal = '';
    		for ($j=0; $j<$sizeClubArr; $j++)
    		{
    			list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
    			$PointTotal += $thisRolePoint;
    			
    		}
    		$ExceededPoint = $PointTotal-$SuggestUpper;
    		
    		if($sizeClubArr==0){
    			$NoResult[] =array($i_no_record_exists_msg,'','','','','','','','','');
    	//		$NoResult ="<tr><td align='center' colspan='12' class='tabletext tablerow2' ><br>$i_no_record_exists_msg<br><br></td></tr>\n";
    		}
    		else if($PointTotal < $SuggestUpper){
    	//		$NoResult="<tr><td align='center' colspan='12' class='tabletext tablerow2' ><br>$i_no_record_exists_msg<br><br></td></tr>\n";
    			$NoResult[] =array($i_no_record_exists_msg,'','','','','','','','','');
    		}
    		else{
    			$QuitClubAry = array();
    			$Consequence = 0;
    			$new = 0;
    			$newExceed = 0;
    			for ($j=0; $j<$sizeClubArr; $j++)
    			{
    				list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
    				
    				$GroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
    				$MemberAry = $libenroll->Get_Club_Student_Attendance_Report_Info($GroupID,'5',$AcademicYearID,'2',$thisEnrolGroupID);
    				$MemberAry= BuildMultiKeyAssoc($MemberAry,'UserID');
    				
    				
    				$ActiveMemberStatus = $MemberAry[$thisStudentID]['ActiveMemberStatus'];
    				if($ActiveMemberStatus == 'UnQualified'){
    					$QuitClubAry[] = $thisTitle;
    					$Consequence = 0-$thisRolePoint;
    					$new =$PointTotal + $Consequence;
    					$newExceed = $new -$SuggestUpper;
    					if($newExceed < 0){
    						$newExceed = 0;
    					}
    				}
    			}
    			$Result = '';
    			if(sizeof($QuitClubAry)>0){
    				$Result  = 'Quit '.implode(';',$QuitClubAry);
    			}
    			
    			$ResultInfo[] =array($ClassName.$ClassNumber,$ClassName,$StudentEnglishName,$StudentChineseName,$PointTotal,$SuggestUpper,$ExceededPoint,$Consequence,$new,$newExceed);
    			
    		}
    	}	
   }
	
}

if($ResultInfo == ''){
	$contentArr = $NoResult;
}
else{
	$contentArr = $ResultInfo;
}


foreach($HeaderArr as &$header){
	$header = standardizeFormPostValue($header);
}
foreach($contentArr as &$contentRow){
	foreach($contentRow as &$data){
		$data = standardizeFormPostValue($data);
	}
}


$numOfRow = count($contentArr);
for($i=0;$i<$numOfRow;$i++){
	$numOfColumn = count($contentArr[$i]);
	for($j=0;$j<$numOfColumn;$j++){
		$contentArr[$i][$j]=str_replace($seperator,"\n",$contentArr[$i][$j]);
	}
}

$export_text = $lexport->GET_EXPORT_TXT($contentArr, $HeaderArr, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "individual_eca_report.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>