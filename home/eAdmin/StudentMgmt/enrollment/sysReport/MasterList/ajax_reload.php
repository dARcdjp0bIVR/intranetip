<?php 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
// include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$libclass = new libclass();
# user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$RecordType = $_POST['RecordType'];


$studentIdAry = $_POST['studentIdAry'];
$classIdAry = $_POST['classIdAry'];
$selectSemesterAry = $_POST['Semester'];
// 	$StudentType = $_POST['StudentType'];

if(isset($selectSemesterAry))
{
    $term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $selectSemesterAry);
    
    $term_group_list = array();
    if(!empty($term_group_list_temp))
    {
        foreach($term_group_list_temp as $k=>$d)
            $term_group_list[] = $d['EnrolGroupID'];
    }
}

$AcademicYearID = Get_Current_Academic_Year_ID();
$fcm = new form_class_manage();
$FormList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);

$FormListAry = BuildMultiKeyAssoc($FormList,'YearClassID');

$ClassSize = sizeof($classIdAry);



for($k=0;$k<$ClassSize;$k++){
    $display = '';
    $ResultInfo = '';
    $YearClassID = $classIdAry[$k];
    $WebSAMSCode = $FormListAry[$YearClassID]['Form_WebSAMSCode'];
    
    
    $SuggestUpper = $libenroll->GetFormExceedPoint($AcademicYearID,$WebSAMSCode);
    $studentInfoAry = $libclass->getStudentByClassId($YearClassID);
    
    $thisClassStudentIDAry = Get_Array_By_Key($studentInfoAry,'UserID');
// 		$FormNumber = preg_replace('/\D/s', '', $WebSAMSCode);
		
// 		$SuggestUpperAry[1] = 24;
// 		$SuggestUpperAry[2] = 24;
// 		$SuggestUpperAry[3] = 30;
// 		$SuggestUpperAry[4] = 36;
// 		$SuggestUpperAry[5] = 36;
// 		$SuggestUpperAry[6] = 20;
// 		$SuggestUpper = $SuggestUpperAry[$FormNumber];
		
    $studentClubArr= $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIdAry,$targetCategory);
    //	$studentClubArr = BuildMultiKeyAssoc($studentClubArr,'UserID');
    
    $studentInfoArr = array();
    
    foreach ($studentClubArr as $key => $clubDataArr)
    {
        
        list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
        $thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
        $roleTitle = ($roleTitle!="") ? $roleTitle : "--";
        
        if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
        {
            continue;
        }
        
        if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
        {
            $studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
        }
        $rolePointArr = $libenroll->Get_Role_Point_By_Role_Title($thisEnrolGroupID,$roleTitle);
        $Point = "";
        if($rolePointArr[0]['Point'] != null){
            $Point = $rolePointArr[0]['Point'];
        }
        
        
        $studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle, $roleTitle,$Point);
    }
		
		##### Header
		$display .="<br>";
		$display .= "<center>";
			$display .= "<table width='1000px'  border:0px; margin: 0 auto; font-size: 30px; '>";
		# Table Name
				$display .= "<tr>";
					$display .= "<td colspan='2' style='line-height:30px;' align='center'>";
				//	$display .= GET_SCHOOL_NAME()." ECA Record (". $AcademicYearName.")<br/>";
						$display .="Excessive Participation Record<br/>";
					$display .= "</td>";
				$display .= "</tr>";
				
				#### Header End ########
				##### Content #####
				$display .= "<tr>";
					$display .= "<td align='center'>";
						$display .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
						$display .= "<thead>";
							$display .= "<tr>";
								$display .= "<td  colspan='12'>";
									$display .= "ECA: Excessive Participation Record (Student whose ECA points exceed the upper limit)";
								$display .= "</td>";
							$display .= "</tr>";
							$display .= "<tr>";
								$display .= "<th width='8%'>".$Lang['eEnrolment']['SysRport']['SchoolECA']['StudentCode']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['Class']."</th>
											<th width='28%'>".$Lang['eEnrolment']['SysRport']['SchoolECA']['EnglishName']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['SchoolECA']['ChineseName']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['ECAPoint']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['UpperLimit']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['ExceededBy']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['Consequence']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['NewECAPoints']."</th>
											<th width='8%'>".$Lang['eEnrolment']['SysRport']['ExceededBy']."</th>";
		
		
				$display .= "</tr></thead>";
				# Display Result
				
				for($i=0;$i<sizeof($studentIdAry);$i++){
					$thisStudentID = $studentIdAry[$i];
					if(in_array($thisStudentID,$thisClassStudentIDAry)){
					   
    					$libuser =new libuser($thisStudentID);
    					$StudentEnglishName = $libuser->EnglishName;
    					$StudentChineseName = $libuser->ChineseName;
    					$ClassName = $libuser->ClassName;
    					$ClassNumber = $libuser->ClassNumber;
    					
    					$thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
    					
    					$sizeClubArr = sizeof($thisClubArr);
    					$thisClubLink = '';
    					$RoleTitle = '';
    					$RolePoint='';
    					$PointTotal = '';
    					for ($j=0; $j<$sizeClubArr; $j++)
    					{
    						list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
    						$PointTotal += $thisRolePoint;
    	
    					}
    					$ExceededPoint = $PointTotal-$SuggestUpper;
    					
    					if($sizeClubArr==0){
    // 						$display .="<tr>
    // 										<td class='tabletext' valign=top'>$ClassName.$ClassNumber</td>
    // 									  	<td class='tabletext'  valign='top'>$ClassName</td>
    // 										<td class='tabletext'  valign='top'>$StudentEnglishName</td>
    // 										<td class='tabletext'  valign='top'>$StudentChineseName</td>
    // 										<td class='tabletext'  valign='top'>$PointTotal</td>
    // 										<td  colspan='8' class='tabletext' valign='top' ><br>$i_no_record_exists_msg<br><br></td>
    // 									</tr>\n";
    						$NoResult="<tr><td align='center' colspan='10' class='tabletext tablerow2' ><br>$i_no_record_exists_msg<br><br></td></tr>\n";
    						
    					}
    					else if($PointTotal < $SuggestUpper){
    // 						$display .="<tr>
    // 										<td class='tabletext' valign=top'>$ClassName.$ClassNumber</td>
    // 										<td class='tabletext'  valign='top'>$ClassName</td>
    // 										<td class='tabletext'  valign='top'>$StudentEnglishName</td>
    // 										<td class='tabletext'  valign='top'>$StudentChineseName</td>
    // 										<td class='tabletext'  valign='top'>$PointTotal</td>
    // 										<td colspan='8' class='tabletext' valign='top' >$i_no_record_exists_msg<br><br></td>
    // 									</tr>\n";
    						$NoResult="<tr><td align='center' colspan='10' class='tabletext tablerow2' ><br>$i_no_record_exists_msg<br><br></td></tr>\n";
    						
    					}
    					else{
    						$QuitClubAry = array();
    						$Consequence = 0;
    						$new = 0;
    						$newExceed = 0;
    						for ($j=0; $j<$sizeClubArr; $j++)
    						{
    							list($thisEnrolGroupID, $thisTitle,$thisRoleTitle,$thisRolePoint) = $thisClubArr[$j];
    										
    							$GroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
    							$MemberAry = $libenroll->Get_Club_Student_Attendance_Report_Info($GroupID,'5',$AcademicYearID,'2',$thisEnrolGroupID);
    							$MemberAry= BuildMultiKeyAssoc($MemberAry,'UserID');
    										
    							$ActiveMemberStatus = $MemberAry[$thisStudentID]['ActiveMemberStatus'];
    							if($ActiveMemberStatus == 'UnQualified'){
    								$QuitClubAry[] = $thisTitle;
    								$Consequence = 0-$thisRolePoint;
    								$new =$PointTotal + $Consequence;
    								$newExceed = $new -$SuggestUpper;
    								if($newExceed < 0){
    									$newExceed = 0;
    								}
    							}
    						}
    									
    						$Result = '';
    						if(sizeof($QuitClubAry)>0){
    						$Result  = 'Quit '.implode(';',$QuitClubAry);
    						}
    						$ResultInfo.= '<tr>';
    							$ResultInfo.= '<td class="tabletext" valign="top">'.$ClassName.$ClassNumber.'</td>
    									  	   <td class="tabletext"  valign="top">'.$ClassName.'</td>
    									 	   <td class="tabletext"  valign="top">'.$StudentEnglishName.'</td>
    									 	   <td class="tabletext"  valign="top">'.$StudentChineseName.'</td>
    										   <td class="tabletext"  valign="top">'.$PointTotal.'</td>
    										   <td class="tabletext"  valign="top">'.$SuggestUpper.'</td>
    										   <td class="tabletext"  valign="top">'.$ExceededPoint.'</td>
    										   <td class="tabletext"  valign="top">'.$Consequence.'</td>
    										   <td class="tabletext"  valign="top">'.$new.'</td>
    										   <td class="tabletext"  valign="top">'.$newExceed.'</td>
    										';
    						$ResultInfo.= '</tr>';
    						
    					}
    					
				    }
				}
			
				
				if($ResultInfo == ''){
					$display .= $NoResult;
				}
				else{
					$display .= $ResultInfo;
				}
				$display .= '</table>';
			$display .= "</td>";
		$display .= "</tr>";
	$display .= '</table></center>';

	echo $display;
}
?>