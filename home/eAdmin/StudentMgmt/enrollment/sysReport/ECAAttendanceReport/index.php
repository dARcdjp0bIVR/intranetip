<?
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$linterface = new interface_html();

# setting the current page
$CurrentPage = "ECAAttendanceReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['SysRport']['ECAAttendanceReport'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();
	
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onChange='changeYear(this.value);' ", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onChange=\"changeTerm(this.value);\">";
$selectSemesterHTML .= "</select>";


$activityCategoryHTMLBtn .= " ".$linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('activityCategory'));return false;", "selectAllBtnActCat");
$linterface->LAYOUT_START();
?>
<script type="text/javascript">
$(document).ready(function () {
	changeYear($("#selectYear").val());
});

function changeYear(val) {
	$('#selectSemester').load(
		'../../ajaxGetSemester.php', 
		{ 
			year: val,
			field: 'selectSemester'
		},
		function(ReturnData) {
			document.getElementById("selectSemester").innerHTML = ReturnData;
		}
	);		
	changeTerm();
} 

function changeTerm(){
	getClubSelectionBox();
	
}


function getClubSelectionBox() { 
	
	var yearID = document.form1.selectYear.value;
	var semester = document.form1.selectSemester.value;

	$('#ClubElements').load(
		'../ajax_reload.php', 
		{ 
			Action: 'clubSelection',
			AcademicYearID: yearID,
			Semester: semester,
			clubSelID: 'selectClub',
			clubSelName : 'selectClub'
		//	studentSelName: 'studentIdAry[]'
		},
		function(ReturnData) {
			js_Select_All('selectClub', 1);
		}
	);
}


function checkForm(){
		$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

</script>
<br />
<form id="form1" name="form1" method="post">
<table class="form_table_v30">
	<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td colspan="6">					
					<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
					<?=$selectSchoolYearHTML?>&nbsp;
					<span <?=$hideSemester?>>
						<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
						<span id="spanSemester"><?=$selectSemesterHTML?></span>
					</span>
				</td>
			</tr>			
		</table>
	</td>
</tr>

<!-- Category -->
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$eEnrollment['add_activity']['act_category']?></td>
	<td>
		<table class="inside_form_table" width="100%">
			<tr>
				<td>
					<span id="ClubElements"></span> <?=$activityCategoryHTMLBtn?>
				</td>
			</tr>
		</table>
		<span id='div_Category_err_msg'></span>
	</td>
</tr>



</table>
	
<div class="edit_bottom_v30">
<?php echo $linterface->GET_ACTION_BTN($Lang['Btn']['Print'], "button", "javascript:checkForm();");?>
</div>	
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>