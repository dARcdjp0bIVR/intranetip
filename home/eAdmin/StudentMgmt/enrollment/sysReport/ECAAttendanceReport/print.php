<?php
## Modifying By: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');


$AcademicYearID =$_POST['selectYear'];
// $SelectSemester = $_POST['selectSemester'];
$SelectedClubIDAry = $_POST['selectClub'];

$CurrentTime = getdate();
$date = $CurrentTime['year'].'/'.$CurrentTime['mon'].'/'.$CurrentTime['mday'].' '.$CurrentTime['hours'].':'.$CurrentTime['minutes'].':'.$CurrentTime['seconds'];



$pageBreakCss = "page-break-after: always;";
foreach($SelectedClubIDAry as $EnrolGroupID){
    // debug_pr($date);
    $GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
    
    $MemberAry = $libenroll->Get_Club_Student_Attendance_Report_Info($GroupID,'5',$AcademicYearID,'2',$EnrolGroupID);
    $AttendanceInfoAry = $libenroll->Get_Student_Attendance_Arr($EnrolGroupID,'club');
    
    //$MemberAry= BuildMultiKeyAssoc($MemberAry,'UserID');
    $ClubName = $libenroll->getClubInfoByEnrolGroupID($EnrolGroupID);
    
    ##### Table Header
    $display .= "<br><center><table width='1000px' style='" . $pageBreakCss . "' border:0px; margin: 0 auto; font-size: 22px; '>";
//     # Print button
//     $display .="<tr>";
//         $display .="<td>";
//             $display .= "<table width='100%' align='center' class='print_hide' >";
//                 $display .="<tr>";
//                     $display .="<td align='right'>";
//                      $display .= $linterface->GET_SMALL_BTN('Print', "button", "javascript:window.print();","submit2");
//                     $display .="</td>";
//                 $display .="</tr>";
//             $display .="</table>";
//         $display .="</td>";
//     $display .="</tr>";
    
    
    # Table Name
    $display .= "<tr>";
        $display .= "<td colspan='2' style='line-height:24px;' align='center'>";
            $display .= GET_SCHOOL_NAME()." ECA Attendance Report<br/>";
        $display .= "</td>";
    $display .= "</tr>";
    # Student Information
    $display .= "<tr>";
            $display .= "<td colspan='2' align='left' style='line-height:25px; padding-right: 30px;'>";
            $display .= "<span style='display: table-cell; width:800px; '>".$ClubName."</span>";
        $display .= "</td>";
    $display .= "</tr>";
    #### Header End ########
    #######
    $display .= "<tr>";
    $display .= "<td align='center'>";
    $display .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
    $display .= "<thead><tr>";
    $display .= "<th width='20%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['Student Name']."</td>
								 <th width='10%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['Class']."</td>
								 <th width='10%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['Number']."</td>
								 <th width='20%'>".$Lang['eEnrolment']['SysRport']['SchoolECA']['Role']."</td>
								 <th width='10%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['RoleCode']."</td>
 								 <th width='10%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['Qualified']."</td>
								 <th width='10%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['Attendance']."</td>
								 <th width='10%'>".$Lang['eEnrolment']['SysRport']['AttendanceReport']['Unsatisfactoryperformance']."</td>";
    $display .= "</tr></thead>";
    

    $StudentSize = sizeof($MemberAry);
    for ($i=0; $i<$StudentSize; $i++){
        $_UserID = $MemberAry[$i]['UserID'];
        $_ClassName = $MemberAry[$i]['ClassName'];
        $_ClassNumber= $MemberAry[$i]['ClassNumber'];
        $_StudentName= $MemberAry[$i]['StudentName'];
        $_RoleTitle= $MemberAry[$i]['Title'];
        $_WebSAMSCode= $MemberAry[$i]['WebSAMSCode'];
        $_ActiveMemberStatus = $MemberAry[$i]['ActiveMemberStatus'];
        $_Attendance = $AttendanceInfoAry[$_UserID];
        
        $QualifiedCheckbox = '';
        $AttendanceCheckbox = '';
        if($_ActiveMemberStatus == 'Qualified'){
            $QualifiedCheckbox = 'checked';
            
        }
        if($_Attendance<70){
            $AttendanceCheckbox = 'checked';
        }
        
        $display .= '<tr>';
        $display .= '<td class="tabletext" valign="top">'.$_StudentName.'</td>
							 <td class="tabletext"  valign="top">'.$_ClassName.'</td>
							 <td class="tabletext"  valign="top">'.$_ClassNumber.'</td>
							 <td class="tabletext"  valign="top">'.$_RoleTitle.'</td>
							 <td class="tabletext"  valign="top">'.$_WebSAMSCode.'</td>
							 <td class="tabletext"  valign="top"><input type="checkbox"'.$QualifiedCheckbox.'></td>
							 <td class="tabletext"  valign="top"><input type="checkbox"'.$AttendanceCheckbox.'></td>
							 <td class="tabletext"  valign="top"><input type="checkbox"></td>
									';
        $display .= '</tr>';
    }
    $display .= '</table>';
    $display .= '</td>';
    $display .= '</tr>';
    
    $display .="<tr>";
    $display .="<td class='tabletext'>";
    $display.= " Generated : ".$date;
    $display .= "</td>";
    $display .= "</tr>";
    
    $display .= '</table></center>';
    
    
}

?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN('Print', "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
<?
echo $display;

intranet_closedb();
?>