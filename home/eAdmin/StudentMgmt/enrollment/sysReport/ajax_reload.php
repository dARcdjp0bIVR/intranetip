<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");


intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();

$Action = trim(stripslashes($_POST['Action']));	

if ($Action == 'studentSelection') {
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	$libclass = new libclass();
	
	$selectedIdList = $_POST['SelectedIdList'];
	$studentSelId = $_POST['studentSelId'];
	$studentSelName = $_POST['studentSelName'];
	
	$selectedIdAry = explode(',', $selectedIdList);
	
	$studentIdAry = array();
	$studentInfoAry = $libclass->getStudentByClassId($selectedIdAry);

	
	$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
	$studentInfoAry = $libenroll->Get_Student_Info($studentIdAry);
	
	$numOfStudent = count($studentInfoAry);
	
	$selectAry = array();
	for ($i=0; $i<$numOfStudent; $i++) {
		$_studentId = $studentInfoAry[$i]['UserID'];
		$_studentName = $studentInfoAry[$i]['StudentName'];
		
		$selectAry[$_studentId] = $_studentName;
	}
	
	echo getSelectByAssoArray($selectAry, 'id="'.$studentSelId.'" name="'.$studentSelName.'" multiple="multiple" size="10"', $selected="", $all=0, $noFirst=1);
}

if ($Action == 'clubSelection') {
//     include_once($PATH_WRT_ROOT."includes/libclass.php");
//     $libclass = new libclass();
    
    $AcademicYearID= $_POST['AcademicYearID'];
    $Semester= $_POST['Semester'];
    $clubSelName = $_POST['clubSelName'];
    $clubSelID = $_POST['clubSelID'];

    $ClubInfoAry = $libenroll->getGroupsForEnrolSetByConditions($AcademicYearID,"","",$Semester);
    $numOfClub = count($ClubInfoAry);
    
    $selectAry = array();
    for ($i=0; $i<$numOfClub; $i++) {
        $_enrolGroupId = $ClubInfoAry[$i]['EnrolGroupID'];
        $_enrolGroupName = $ClubInfoAry[$i]['Title'];
        $selectAry[$_enrolGroupId] = $_enrolGroupName;
    }
    $multipleSelectionBox = $linterface->Get_Multiple_Selection_Box($selectAry, "selectClub", "selectClub[]", 10,0, explode(",",$selectedClubs));
    
  //echo getSelectByAssoArray($selectAry, 'id="'.$clubSelID.'" name="'.$clubSelName.'" multiple="multiple" size="10"', $selected="", $all=0, $noFirst=1);
    echo $multipleSelectionBox;
}
?>