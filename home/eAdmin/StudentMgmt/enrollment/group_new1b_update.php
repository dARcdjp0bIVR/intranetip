<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$GroupID = $_REQUEST['GroupID'];
$EnrolGroupID = $_REQUEST['EnrolGroupID'];
//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
	
if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club")))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

//record all UserID of staff before updating for deletion purpose
//otherwise, we do not know which staff has been removed since $pic and $helper only includes the new staff list
$sql = "
			SELECT
						UserID
			FROM
						INTRANET_ENROL_GROUPSTAFF
			WHERE
						EnrolGroupID = '".$EnrolGroupID."'
		";
$oldStaffArr = $libenroll->returnArray($sql,1);

$libenroll->DEL_GROUPSTAFF($EnrolGroupID);

$PICArr['EnrolGroupID'] = $EnrolGroupID;
$PICArr['StaffType'] = "PIC";

for ($i = 0; $i < sizeof($pic); $i++) {
	$PICArr['UserID'] = $pic[$i];
	$libenroll->ADD_GROUPSTAFF($PICArr);
	
	//check if the staff is removed (for deletion later)
	for ($j = 0; $j < sizeof($oldStaffArr); $j++){
		if ($PICArr['UserID'] == $oldStaffArr[$j]['UserID']){
			$oldStaffArr['notRemoved'][$j][0] = true;
		}
	}
		
	//check if the satff is already a member
	$sql = "SELECT COUNT(DISTINCT a.UserID) FROM INTRANET_USERGROUP WHERE ((EnrolGroupID = '".$EnrolGroupID."') AND (UserID = '".$PICArr['UserID']."'))";
	$result = $libenroll->returnArray($sql);	
	
	// add staff to the member list if the staff is not in the group
	if ($result[0][0]==0){
		$sql = "INSERT INTO INTRANET_USERGROUP
					(GroupID, EnrolGroupID, UserID, RecordType, DateInput, DateModified, ApprovedBy)
				VALUES
					('$GroupID', '".$EnrolGroupID."', '".$PICArr['UserID']."', 'A', now(), now(), '".$_SESSION['UserID']."')
				";	
		$libenroll->db_db_query($sql);
	}

}

$PICArr['StaffType'] = "HELPER";
for ($i = 0; $i < sizeof($helper); $i++) {
	$PICArr['UserID'] = $helper[$i];
	$libenroll->ADD_GROUPSTAFF($PICArr);
	
	# if is Student => do not add to member list
	# otherwise, add to member list
	$sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '".$PICArr['UserID']."'";
	$personTypeArr = $libenroll->returnVector($sql);
	$personType = $personTypeArr[0];
	
	//check if the staff is removed for deletion later
	for ($j = 0; $j < sizeof($oldStaffArr); $j++){
		if ($PICArr['UserID'] == $oldStaffArr[$j]['UserID']){
			$oldStaffArr['notRemoved'][$j][0] = true;
		}
	}
	
	if ($personType != 2)
	{
		//check if the staff is already a member
		$sql = "SELECT COUNT(DISTINCT a.UserID) FROM INTRANET_USERGROUP WHERE ((EnrolGroupID = '".$EnrolGroupID."') AND (UserID = '".$PICArr['UserID']."'))";
		$result = $libenroll->returnArray($sql);
		
		// add staff to the member list if the staff is not in the group
		if ($result[0][0]==0){
			$sql = "INSERT INTO INTRANET_USERGROUP
						(GroupID, EnrolGroupID, UserID, RecordType, DateInput, DateModified, ApprovedBy)
					VALUES
						('$GroupID', '".$EnrolGroupID."', '".$PICArr['UserID']."', 'A', now(), now(), '".$_SESSION['UserID']."')
					";	
			$libenroll->db_db_query($sql);
		}
	}
}

//delete staff from member list if is removed by the user
for ($i = 0; $i < sizeof($oldStaffArr); $i++) {
	$sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '".$oldStaffArr[$i]['UserID']."'";
	$personTypeArr = $libenroll->returnVector($sql);
	$personType = $personTypeArr[0];
	
	if (!$oldStaffArr['notRemoved'][$i][0] && $personType != 2){
		$sql = "DELETE FROM INTRANET_USERGROUP WHERE ((EnrolGroupID = '".$EnrolGroupID."') AND (UserID = '".$oldStaffArr[$i]['UserID']."'))";
		$libenroll->db_db_query($sql);
	}	
}

intranet_closedb();
header("Location: group_new2.php?EnrolGroupID=$EnrolGroupID&Semester=$Semester");
?>