<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$LibUser = new libuser();
$linterface = new interface_html();

if ($plugin['eEnrollment'])
{
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	if ($EnrolGroupID != "") {
		$CurrentPage = "PageMgtClub";
		$TempArr = $libenroll->getGroupInfo($GroupID);
		# tags 
	    $tab_type = "club";
	    $current_tab = 2;
	}
	if ($EnrolEventID != "") {
		$CurrentPage = "PageMgtActivity";
		$TempArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		# tags 
	    $tab_type = "activity";
	    $current_tab = 2;
	}
	# tags 
    include_once("management_tabs.php");
	
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
}

$linterface->LAYOUT_START();

# show current location #
$temp[] = array("".$eEnrollment['payment_summary']."","");
$infobar .= "<tr><td colspan=\"2\" class=\"tabletext\">".$linterface->GET_NAVIGATION($temp)."</td></tr>"; 

$arr_success = explode(",",$success_list);
$arr_fail = explode(",",$fail_list);

for($i=0; $i<sizeof($arr_success); $i++) {
	
	$user_id = $arr_success[$i];
		
	$sql = "SELECT 
					a.UserID, a.ChineseName, a.EnglishName, a.ClassName, a.ClassNumber, CONCAT('$',FORMAT(b.Balance,2))
			FROM 
					INTRANET_USER AS a INNER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE 
					a.UserID = '$user_id'";
	
	$arr_temp = $libenroll->returnArray($sql,6);
	
	if(sizeof($arr_temp)>0) {
		list($user_id, $chinese_name, $english_name, $class_name, $class_num, $acc_balance) = $arr_temp[0];
		
		$arr_payment_success_record[] = $user_id;
		$arr_payment_success_detail[$user_id]['ChiName'] = $chinese_name;
		$arr_payment_success_detail[$user_id]['EngName'] = $english_name;
		$arr_payment_success_detail[$user_id]['ClassName'] = $class_name;
		$arr_payment_success_detail[$user_id]['ClassNum'] = $class_num;
		$arr_payment_success_detail[$user_id]['AccBalance'] = $acc_balance;
		$arr_payment_success_detail[$user_id]['Status'] = 1;
	}
}

for($i=0; $i<sizeof($arr_fail); $i++) {
	
	$user_id = $arr_fail[$i];
		
	$sql = "SELECT 
					a.UserID, a.ChineseName, a.EnglishName, a.ClassName, a.ClassNumber, CONCAT('$',FORMAT(b.Balance,2))
			FROM 
					INTRANET_USER AS a INNER JOIN PAYMENT_ACCOUNT AS b ON (a.UserID = b.StudentID)
			WHERE 
					a.UserID = '$user_id'";
	
	$arr_temp = $libenroll->returnArray($sql,6);
	
	if(sizeof($arr_temp)>0) {
		list($user_id, $chinese_name, $english_name, $class_name, $class_num, $acc_balance) = $arr_temp[0];
		
		$arr_payment_fail_record[] = $user_id;
		$arr_payment_fail_detail[$user_id]['ChiName'] = $chinese_name;
		$arr_payment_fail_detail[$user_id]['EngName'] = $english_name;
		$arr_payment_fail_detail[$user_id]['ClassName'] = $class_name;
		$arr_payment_fail_detail[$user_id]['ClassNum'] = $class_num;
		$arr_payment_fail_detail[$user_id]['AccBalance'] = $acc_balance;
		$arr_payment_fail_detail[$user_id]['Status'] = 0;
	}
}

$success_content .= "<tr><td class=\"tablename\" colspan=\"5\"><font color=\"green\">".$eEnrollment['payment_success_list']."</font></td></tr>";
$success_content .= "<tr><td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_UserChineseName</td>";
$success_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_UserEnglishName</td>";
$success_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_ClassNameNumber</td>";
$success_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_Payment_Field_BalanceAfterPay</td>";
$success_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_Payment_PaymentRecord</td>";
$success_content .= "</tr>";

if(sizeof($arr_payment_success_record)>0) {
	
	foreach($arr_payment_success_record as $key=>$user_id)
	{
		$chinese_name = $arr_payment_success_detail[$user_id]['ChiName'];
		$english_name = $arr_payment_success_detail[$user_id]['EngName'];
		$class_name = $arr_payment_success_detail[$user_id]['ClassName'];
		$class_num = $arr_payment_success_detail[$user_id]['ClassNum'];
		$acc_balance = $arr_payment_success_detail[$user_id]['AccBalance'];
		$payment_status = $arr_payment_success_detail[$user_id]['Status'];
		
		if($payment_status == 1) {
			$txt_payment_status = $eEnrollment['payment_status_success'];
		}
		if($payment_status == 0) {
			$txt_payment_status = $eEnrollment['payment_status_fail'];
		}
		
		if($key%2 == 0)
			$css = " class=\"tablerow1\" ";
		else
			$css = " class=\"tablerow2\" ";
		
		$success_content .= "<tr $css>";
		$success_content .= "<td class=\"tabletext\">$chinese_name</td>";
		$success_content .= "<td class=\"tabletext\">$english_name</td>";
		$success_content .= "<td class=\"tabletext\">$class_name ($class_num)</td>";
		$success_content .= "<td class=\"tabletext\">$acc_balance</td>";
		$success_content .= "<td class=\"tabletext\">$txt_payment_status</td>";
		$success_content .= "</tr>";
	}	
} else {
	$success_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"5\">$i_no_record_exists_msg</td></tr>";
}
$success_content .= "<tr height=\"20px\"><td class=\"tableorangebottom\" colspan=\"5\"></td></tr>";


$fail_content .= "<tr><td class=\"tablename\" colspan=\"5\"><font color=\"red\">".$eEnrollment['payment_fail_list']."</font></td></tr>";
$fail_content .= "<tr><td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_UserChineseName</td>";
$fail_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_UserEnglishName</td>";
$fail_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_ClassNameNumber</td>";
$fail_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_Payment_Field_CurrentBalance</td>";
$fail_content .= "<td class=\"tableorangetop tabletopnolink\" width=\"20%\">$i_Payment_PaymentRecord</td>";
$fail_content .= "</tr>";

if(sizeof($arr_payment_fail_record)>0) {
	
	foreach($arr_payment_fail_record as $key=>$user_id)
	{
		$chinese_name = $arr_payment_fail_detail[$user_id]['ChiName'];
		$english_name = $arr_payment_fail_detail[$user_id]['EngName'];
		$class_name = $arr_payment_fail_detail[$user_id]['ClassName'];
		$class_num = $arr_payment_fail_detail[$user_id]['ClassNum'];
		$acc_balance = $arr_payment_fail_detail[$user_id]['AccBalance'];
		$payment_status = $arr_payment_fail_detail[$user_id]['Status'];
		
		if($payment_status == 1) {
			$txt_payment_status = $eEnrollment['payment_status_success'];
		}
		if($payment_status == 0) {
			$txt_payment_status = $eEnrollment['payment_status_fail'];
		}
		
		if($key%2 == 0)
			$css = " class=\"tablerow1\" ";
		else
			$css = " class=\"tablerow2\" ";
		
		$fail_content .= "<tr $css>";
		$fail_content .= "<td class=\"tabletext\">$chinese_name</td>";
		$fail_content .= "<td class=\"tabletext\">$english_name</td>";
		$fail_content .= "<td class=\"tabletext\">$class_name ($class_num)</td>";
		$fail_content .= "<td class=\"tabletext\">$acc_balance</td>";
		$fail_content .= "<td class=\"tabletext\">$txt_payment_status</td>";
		$fail_content .= "</tr>";
	}	
} else {
	$fail_content .= "<tr class=\"tablerow2\"><td class=\"tabletext\" colspan=\"5\">$i_no_record_exists_msg</td></tr>";
}
$fail_content .= "<tr height=\"20px\"><td class=\"tableorangebottom\" colspan=\"5\"></td></tr>";

?>
<br>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td align="left">
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right"><?= $SysMsg ?></td>
</tr>
</table>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<?=$infobar?>
</table>
<br>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
<?=$success_content ;?>
</table>
<br>
<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
<?=$fail_content ;?>
</table>

<table width="96%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr><td align="center" colspan="6">
		<div style="padding-top: 5px">
			<?
			if ($EnrolGroupID!="")
			{
			?>
				<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_club_enrolment'], "button", "self.location='club_status.php'")?>
			<?
			}
			if ($EnrolEventID!="")
			{
			?>
				<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_act_enrolment'], "button", "self.location='event_status.php'")?>
			<?
			}
			?>
			
		</div>
	</td></tr>
</table>
<br />

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>