<?php
## Using By: Rita

######################################
##	Modification Log:
##	2012-12-24: Rita 
##	Add reject reason for customization [#2012-0803-1543-49054]
##
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition
##
## 2010-01-08 Max (201001071635)
## - Case the email send by "EventOnceEmail"
########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolEventID = IntegerSafe($EnrolEventID);

if (isset($EnrolEventID) && $EnrolEventID!="")
{
	$StuArr['EnrolEventID'] = $EnrolEventID;
}
else
{
	$StuArr['EnrolEventID'] = $EventEnrolID;
}

$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $StuArr['EnrolEventID']);


# Reject Reason Customization 
if($libenroll->enableActivityFillInEnrolReasonRight()){
	$libdb = new libdb();
	$userIDArr = $_POST['uid'];
	$rejectReason = $_POST['rejectReason'];
	$UserArr = unserialize(rawurldecode($userIDArr));
	
}else{
	$UserArr = $uid;
}


$numOfUserArr = count($UserArr);

for ($i = 0; $i<$numOfUserArr; $i++) {
	$StuArr['StudentID'] = $UserArr[$i];
	
	//check if student is rejected already
	$sql = "SELECT RecordStatus FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '".$UserArr[$i]."'";
	$result = $libenroll->returnArray($sql,1);
	if ($result[0][0]==1)
	{
		//ignore rejected student
		continue;	
	}
	
	$libenroll->DEL_EVENT_STU($StuArr);
	$updatedStuList[] = $UserArr[$i];

}


$canSendEmail = true;

# Reject Reason Customization 
if($libenroll->enableActivityFillInEnrolReasonRight()){
	$SuccessArr = array();
	for ($i = 0; $i<$numOfUserArr; $i++) {
		$thisReason='';
		$thisStudentID = $UserArr[$i];
		$thisReason = standardizeFormPostValue($rejectReason[$i]);	
		$SuccessArr['INTRANET_ENROL_REASON'][] = $libenroll->Update_Event_Enrol_Reason($EnrolEventID,$thisStudentID,$thisReason);				
	}
		
	if (in_array(false, $SuccessArr))
	{
		$libenroll->RollBack_Trans();
		$response_msg = '5';
		$canSendEmail = false;
		
	}
	else
	{
		$libenroll->Commit_Trans();			
		$response_msg = '2';
//		# Send email to all student 
//		for ($i = 0; $i<$numOfUserArr; $i++) {
//			$thisReason='';
//			$RecordType = "activity";
//			$successResult = false;
//			$thisStudentID = $UserArr[$i];
//			$thisReason = standardizeFormPostValue($rejectReason[$i]);
//			$libenroll->Send_Enrolment_Result_Email(array($thisStudentID), $EnrolEventID, $RecordType, $successResult, $thisReason);
//		}
	
	}
	header("Location: event_member.php?EnrolEventID=$EnrolEventID&msg=$response_msg&filter=$filter");
}

 if($canSendEmail){
 	
	// send email to all student 
	$RecordType = "activity";
	$successResult = false;
	$libenroll->Send_Enrolment_Result_Email($updatedStuList, $EnrolEventID, $RecordType, $successResult);
	header("Location: event_member.php?EnrolEventID=$EnrolEventID&msg=2&filter=$filter");
	
 }

intranet_closedb();

?>