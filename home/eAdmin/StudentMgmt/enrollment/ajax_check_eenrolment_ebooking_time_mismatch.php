<?php
/*
 * Using:
 * 
 *  2019-01-11 Cameron
 *      - create this file
 */
##Modify Form Serlize to Arr
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();


$recordType = $_POST['recordType'];
$dateInfoAry = $_POST['dateInfoAry'];
$recordId = IntegerSafe($_POST['recordId']);
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);

if ($junior_mck) {
    if (phpversion_compare('5.2') != 'ELDER') {
        $characterset = 'utf-8';
    }
    else {
        $characterset = 'big5';
    }
}
else {
    $characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;

$libenroll = new libclubsenrol($AcademicYearID);

$result = $libenroll->checkeEnrolmenteBookingTimeMismatch($recordType, $recordId, $dateInfoAry);

if ($result['success']) {
    $json['success'] = true;
}

echo $ljson->encode($json);

intranet_closedb();

?>