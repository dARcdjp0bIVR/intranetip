<?php
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && (!$libenroll->IS_CLUB_PIC())
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
	//	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
		
		$button_cancel_para = '?pic_view=1';
		$picView_HiddenField = "<input type='hidden' name='pic_view' value='1' />";
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        //$TAGS_OBJ[] = array($eEnrollmentMenu['mgt_event'], "", 1);
        # tags 
        $tab_type = "activity";
        $current_tab = 1;
        include_once("management_tabs.php");
        # navigation
        if ($isNew == 1)
        {
	        $PAGE_NAVIGATION[] = array($eEnrollment['new_activity'], "");
        }
        else
        {
	        $PAGE_NAVIGATION[] = array($eEnrollment['edit_activity'], "");
        }
        
        $STEPS_OBJ[] = array($eEnrollment['add_activity']['step_1'], 0);
        $STEPS_OBJ[] = array($eEnrollment['add_activity']['step_1b'], 0);
		$STEPS_OBJ[] = array($eEnrollment['add_activity']['step_2'], 0);
		$STEPS_OBJ[] = array($eEnrollment['add_activity']['step_3'], 1);
        		
        $linterface->LAYOUT_START();


$EnrollEventArr = $lc->GET_EVENTINFO($EnrolEventID);

# Last selection - PIC
###############################################################################################
$StudentArr = $lc->GET_EVENTSTUDENT($EnrolEventID);
//$StudentSel = "<select name='student[]' size='6' multiple='multiple' OnFocus='alert(this.length)'>";
$StudentSel = "<select name='student[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($StudentArr); $i++) {
	$StudentSel .= "<option value=\"".$StudentArr[$i][1]."\">".$LibUser->getNameWithClassNumber($StudentArr[$i][1])."</option>";
}
$StudentSel .= "</select>";
//if (sizeof($StudentArr)>0)
//{
	$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");
//}
###############################################################################################
        
?>
<script language="javascript">
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["student[]"]);
         //obj.elements["student[]"].focus();
         //obj.submit();
}

function FormSubmitCheck(obj)
{
	/* student selection is removed in v1.2
	if ((obj.ApplyUserType[2].checked)&&(document.getElementById('student[]').length == 0)) {
		alert('<?= $eEnrollment['js_sel_student']?>');
		return false;
	}
		
	if ((document.getElementById('student[]').length > <?= $EnrollEventArr[1]?>) && (<?= $EnrollEventArr[1]?> != 0)) {
		var temp = '<?= str_replace("<!-- NoOfStudent-->", $EnrollEventArr[1], $eEnrollment['js_quota_exists']) ?>';
		alert(temp.replace('<!-- NoOfSelectedStudent-->', document.getElementById('student[]').length));
		return false;
	}
	
	generalFormSubmitCheck(obj);
	*/
	
	/* will not remove student record in v1.2
	if (confirm("<?= $eEnrollment['js_del_warning']?>")) {
		obj.submit();
	} else {
		return false;
	}
	*/
	
	obj.submit();
}

</SCRIPT>
<form name="form1" action="event_new3_update.php" method="POST">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr><td class="tabletext" align="center">
<!-- <?= $eEnrollment['del_warning'] ?> --><br/><br/>
</td></tr>
<tr><td>
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['apply_ppl_type']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" name="ApplyUserType" id="student" value="S" checked> <label for="student"><?= $eEnrollment['add_activity']['student_enroll']?></label>
			<input type="radio" name="ApplyUserType" id="parent" value="P" <? if ($EnrollEventArr[15] == "P") print "checked"; ?> > <label for="parent"><?= $eEnrollment['add_activity']['parent_enroll']?></label><br/>
			
			<? /* hide the student selection in v1.2 */ 
			/*
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr>
					<td valign="top" nowrap="nowrap" class="tabletext<? if ($EnrollEventArr[15] == "T") print " formfieldtitle"; ?>" width="25%" id="teacher_td">
						<input type="radio" name="ApplyUserType" id="teacher" value="T" <? if ($EnrollEventArr[15] == "T") print "checked"; ?> onClick="document.getElementById('select_student').style.display = 'block'; document.getElementById('teacher_td').className = 'formfieldtitle tabletext';"> <label for="teacher"><?= $eEnrollment['add_activity']['pickup_ppl']?></label>
					</td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td class="tablerow2" rowspan="2">
						<table border="0" cellpadding="0" cellspacing="0" id="select_student" style="display: <? ($EnrollEventArr[15] != "T") ? print "'none'":print "'block'"; ?>">
							<tr>
								<td><?= $StudentSel?></td>
								<td valign="bottom">
									&nbsp;<input type="button" class="formsubbutton" onClick="javascript:newWindow('choose/index.php?fieldname=student[]&type=1&EnrolEventID=<?=$EnrolEventID?>', 9)"  value="<?= $button_select?>"   onMouseOver="this.className='formsubbuttonon'" onMouseOut="this.className='formsubbutton'"/><br />
									&nbsp;<?= $button_remove_html?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>	
			*/
			?>		
		</td>
	</tr>
</table>

</td></tr>

<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<!--
<?= $linterface->GET_ACTION_BTN($button_save, "submit", "javascript: generalFormSubmitCheck(document.form1);")?>&nbsp;
-->
<?= $linterface->GET_ACTION_BTN($button_save, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='event_new2.php?EnrolEventID=$EnrolEventID'")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_event_index'], "button", "self.location='event.php".$button_cancel_para."'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>

</table>
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="EnrolEventID" id="EnrolEventID" value="<?= $EnrolEventID?>" />
<?=$picView_HiddenField?>
</form>


    <?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>