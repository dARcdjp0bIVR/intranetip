<?php
# using: 

######### Change Log Start ################
#	Date:	2020-06-19 Philips
#			- add SDAS_Category,SDAS_AllowSync,ActivityTarget,Guest,ParticipatedSchool,TotalParticipant
#   Date:   2018-08-08 Cameron
#           - add returnMsgKey to show action result
#
#	Date:	2017-09-28 Anna
#			- add new event status about reply slip
#
#	Date:	2017-09-15 Anna
#			- added webSAMS code and type
#
#	Date:	2017-06-19 Villa
#			- not create Reply slip if no question is set
#
#	Date:	2017-04-24 Villa
#	`		- add Reply Slip Related 
#
#	Date:	2017-03-17 Villa
#			- add checking for $PICArr['StaffType'] = "PIC"/ "helper" - prevent inserting record when UserID=0
#
##	Date:	2016-06-22 Henry HM
##			Add SPTSS Cust //$sys_custom['eEnrolment']['ActivityIncludeInReports']
##
#	Date:	2014-11-21 Omas
#			Update target house by Change_Club_Event_GroupMapping()
#
#	Date:	2014-07-23 Carlos
#			Customization $sys_custom['eEnrolment']['TWGHCYMA'] - added RelatedGroupID and other Joint Activity fields
#
#	Date:	2014-06-19 Bill
#			Customization $sys_custom['eEnrolment']['ActivityBudgetField'] - added budget field
#
#	Date:	2014-04-29 Carlos
#			Customization $sys_custom['eEnrolment']['TWGHCYMA'] - added INTRANET_GROUP relation
#
#	Date:	2014-04-25 Ivan
#			added GatheringLocation and DismissLocation field
#
#	Date:	2014-04-07 Carlos
#			- Added customization $sys_custom['eEnrolment']['TWGHCYMA'] for TWGHs C Y MA MEMORIAL COLLEGE
#			- 1) Multiple categories; 2) Volunteer Service & Service Hour; 3) Activity Location; 4) Activity Natures
#
#	Date:	2013-05-08 Rita
#			add transport, organizer, rewardOrCert, location
#
#	Date: 	2012-12-04	Rita
#			add survey form variable for customization [Case#2012-0803-1543-49054]
#
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
#	Date:	2010-01-28	YatWoon
#			Add "School Activity" option
#
######### Change Log Start ################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolEventID = IntegerSafe($EnrolEventID);

$isNew = IntegerSafe($isNew);

//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && (!$libenroll->IS_CLUB_PIC())
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
	

if ($EnrolGroupID > 0) {
	$GroupInfoArr = array();
	
	if ($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	{
		$GroupInfoArr = $libenroll->getGroupInfoList(1,"","","","",$AcademicYearID);
	}
	else
	{
		$PIC_EnrolGroupIDArr = $libenroll->Get_PIC_Club($_SESSION['UserID'], $AcademicYearID);
		$numOfPICClub = count($PIC_EnrolGroupIDArr);
		if ($numOfPICClub > 0)
		{
			# Club PIC => can select pic clubs and current activity's club
			if ($EnrollEventArr['EnrolGroupID'] != '')
				$PIC_EnrolGroupIDArr[] = $EnrollEventArr['EnrolGroupID'];
				
			$GroupInfoArr = $libenroll->getGroupInfoList($isAll=1, '', $PIC_EnrolGroupIDArr,"","",$AcademicYearID);
		}
		else
		{
			# Only allowed to select the current activity's club
			if ($EnrollEventArr['EnrolGroupID'] != '')
				$DisplayGroupIDArr = array($EnrollEventArr['EnrolGroupID']);
			if(sizeof($DisplayGroupIDArr)>0)
				$GroupInfoArr = $libenroll->getGroupInfoList($isAll=1, '', $DisplayGroupIDArr,"","",$AcademicYearID);
		}
	}
	
	$enrolGroupIdAry = Get_Array_By_Key($GroupInfoArr, 'EnrolGroupID');
	if (!in_array($EnrolGroupID, $enrolGroupIdAry)) {
		No_Access_Right_Pop_Up();
	}
}
	

$TempEventArr = $libenroll->GET_EVENTINFO($EnrolEventID);

# No need to delete
//$libenroll->DEL_EVENTSTUDENT($EnrolEventID);
$EventArr['EnrolEventID'] = $EnrolEventID;
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($Description==strip_tags($Description))
	{
		$Description = nl2br($Description);
	}
}
$EventArr['Description'] = trim(stripslashes($Description));
if ($set_age) $EventArr['UpperAge'] = IntegerSafe($UpperAge);
if ($set_age) $EventArr['LowerAge'] = IntegerSafe($LowerAge);
$EventArr['Gender'] = $gender;
$EventArr['attach'] = $_FILES['attach'];
$EventArr['Quota'] = IntegerSafe($Quota);
$EventArr['EventTitle'] = trim(stripslashes(cleanHtmlJavascript($act_title)));
$EventArr['EventCategory'] = IntegerSafe($sel_category);
$EventArr['ApplyStartTime'] = $ApplyStartTime." ".IntegerSafe($ApplyStartHour).":".IntegerSafe($ApplyStartMin).":00";
$EventArr['ApplyEndTime'] = $ApplyEndTime." ".IntegerSafe($ApplyEndHour).":".IntegerSafe($ApplyEndMin).":00";
$EventArr['ApplyMethod'] = $tiebreak;
$EventArr['EnrolGroupID'] = $EnrolGroupID;
$EventArr['PaymentAmount'] = cleanHtmlJavascript($PaymentAmount);
$EventArr['isAmountTBC'] = $isAmountTBC;
$EventArr['Nature'] = IntegerSafe($nature);
$EventArr['GroupID'] = $TempEventArr['GroupID'];
$EventArr['AcademicYearID'] = $AcademicYearID;
$EventArr['ActivityCode'] = trim(cleanHtmlJavascript($ActivityCode));
$EventArr['CurrentUser'] = $_SESSION['UserID'];


//debug_pr($_POST);die();
## 2012-12-04 added for Survey Form customization
if($libenroll->enableActivitySurveyFormRight()){	
	$qStr2 = $qStr;
	$qStr = intranet_htmlspecialchars(trim($qStr));
	$DisplayQuestionNumber = $DisplayQuestionNumber==1 ? 1 : 0;
	$AllFieldsReq = $AllFieldsReq==1 ? 1 : 0;
	
	$EventArr['Question'] = $qStr;
	$EventArr['DisplayQuestionNumber'] = $DisplayQuestionNumber;
	$EventArr['AllFieldsReq'] = $AllFieldsReq;
}

# Cutomization fields
if($libenroll->enableActivityLocationField()){	
	$EventArr['location'] = $location; 
}
if($libenroll->enableActivityTransportationField()){	
	$EventArr['transportation'] = $transportation; 
}

if($libenroll->enableActivityOrganizerField()){	
	$EventArr['organizer'] = $organizer; 	
}

if($libenroll->enableActivityRewardOrCertificationField()){	
	$EventArr['rewardOrCert'] = $rewardOrCert; 
}

if($libenroll->enableActivityGatheringLocationField()){
	$EventArr['gatheringLocation'] = trim(stripslashes($gatheringLocation));
}

if($libenroll->enableActivityDismissLocationField()){
	$EventArr['dismissLocation'] = trim(stripslashes($dismissLocation));
}

if($sys_custom['eEnrolment']['TWGHCYMA']){
	$EventArr['RelatedGroupID'] = $RelatedGroupID; // related INTRANET_GROUP
	$EventArr['EventCategory'] = 0; // set original category id to 0
	$EventArr['ActivityCategory'] = IntegerSafe($sel_category); // array
	$EventArr['IsVolunteerService'] = $IsVolunteerService; // 1 or 0
	$EventArr['VolunteerServiceHour'] = $VolunteerServiceHour; // float
	$EventArr['ActivityExtLocation'] = trim(stripslashes($ActivityExtLocation)); // string
	$EventArr['ActivityNature'] = $activity_nature; // array
	
	$EventArr['JointAct'] = $JointAct;
	$EventArr['JointActOrganiser'] = $JointAct=='1'? trim(stripslashes($JointActOrganiser)) : '';
	$EventArr['JointActOrganisedGroup'] = $JointAct=='1'? trim(stripslashes($JointActOrganisedGroup)) : '';
	$EventArr['JointActAwarded'] = $JointActAwarded;
	$EventArr['JointActAwardType'] = $JointActAwarded=='1'? trim(stripslashes($JointActAwardType)) : '';
	$EventArr['JointActChiAuthority'] = $JointActAwarded=='1'? trim(stripslashes($JointActChiAuthority)) : '';
	$EventArr['JointActEngAuthority'] = $JointActAwarded=='1'? trim(stripslashes($JointActEngAuthority)) : '';
	$EventArr['JointActChiAwardName'] = $JointActAwarded=='1'? trim(stripslashes($JointActChiAwardName)) : '';
	$EventArr['JointActEngAwardName'] = $JointActAwarded=='1'? trim(stripslashes($JointActEngAwardName)) : '';
}

if($sys_custom['eEnrolment']['ActivityBudgetField']){
	$EventArr['BudgetAmount'] = $BudgetAmount;
}

if($sys_custom['eEnrolment']['ActivityIncludeInReports']){
	$EventArr['IncludeInReports'] = $IncludeInReports;
}
$EventArr['WebSAMSSTACode'] = $_POST['WebSAMSSTACode'];
$EventArr['WebSAMSSTAType'] = $_POST['WebSAMSSTAType'];

// 2020-06-01 (Philips) - CEES KG
if($plugin['SDAS_module']['KISMode']){
	$EventArr['SDAS_Category'] = $_POST['SDAS_Category'];
	$EventArr['SDAS_AllowSync'] = implode(',',(array)$_POST['SDAS_AllowSync']);
	$EventArr['ActivityTarget'] = $_POST['ActivityTarget'];
	$EventArr['Guest'] = $_POST['Guest'];
	$EventArr['ParticipatedSchool'] = $_POST['ParticipatedSchool'];
	$EventArr['TotalParticipant'] = $_POST['TotalParticipant'];
}

//if ($attachname0 != "") $EventArr['attach']['name'][0] = $attachname0;
//if ($attachname1 != "") $EventArr['attach']['name'][1] = $attachname1;
//if ($attachname2 != "") $EventArr['attach']['name'][2] = $attachname2;
//if ($attachname3 != "") $EventArr['attach']['name'][3] = $attachname3;
//if ($attachname4 != "") $EventArr['attach']['name'][4] = $attachname4;

$result = array();
if ($libenroll->IS_EVENTINFO_EXISTS($EnrolEventID)) {
	$EnrolEventID = $libenroll->EDIT_EVENTINFO($EventArr);
	$result['edit'] = $EnrolEventID;
	
	if($sys_custom['eEnrolment']['TWGHCYMA'] && $plugin['eBooking'] && !in_array($EventArr['Nature'],array(1,2))){
		$locationBookingIdAry = $libenroll->Get_Activity_Location_Booking_Records($EnrolEventID);
		$libenroll->Delete_Activity_Location_Booking_Record($locationBookingIdAry);
		$libenroll->Remove_Non_Existing_Activity_Location_Booking_Record($EnrolEventID);
	}
} else {
	$EnrolEventID = $libenroll->ADD_EVENTINFO($EventArr);
	$result['add'] = $EnrolEventID;
}


if($sys_custom['eEnrolment']['ReplySlip'] &&  $_POST['qStr']){

	
	$Detail = $_POST['qStr'];
	// 	$Detail= preg_replace('#\s+#','<br>',trim($Detail));
	$DisplayQuestionNumber = $_POST['DisplayQuestionNumber'];
	$AllFieldsReq = $_POST['AllFieldsReq'];
	$ReplySlipTitle = $_POST['ReplySlipTitle'];
	$ReplySlipInstruction = $_POST['ReplySlipInstruction'];
	$ReplySlipID = $_POST['ReplySlipID'];
	$ReplySlipID = $libenroll->saveReplySlip($ReplySlipID,$ReplySlipTitle,$Detail,$DisplayQuestionNumber,$AllFieldsReq,$IsTemplate ='0',$IsDeleted='0',$ReplySlipInstruction);
	$libenroll->saveReplySlipRelation($ReplySlipID,$EnrolEventID,'2');
}
##Form Mapping

$libenroll->DEL_EVENTCLASSLEVEL($EnrolEventID);

$ClasslvlArr['EnrolEventID'] = $EnrolEventID;
for ($i = 0; $i < sizeof($classlvl); $i++) {
    $ClasslvlArr['ClassLevelID'] = IntegerSafe($classlvl[$i]);
	$libenroll->ADD_EVENTCLASSLEVEL($ClasslvlArr);
}

##Group Mapping
$libenroll->Change_Club_Event_GroupMapping($enrolConfigAry['Activity'],$EnrolEventID, $houseIdAry);

/**************** assign staff ******************/
$libenroll->DEL_EVENTSTAFF($EnrolEventID);

$PICArr['EnrolEventID'] = $EnrolEventID;
$PICArr['StaffType'] = "PIC";
$pic = IntegerSafe($pic);
for ($i = 0; $i < sizeof($pic); $i++) {
	if($pic[$i]){
		$PICArr['UserID'] = $pic[$i];
		$libenroll->ADD_EVENTSTAFF($PICArr);
	}
}

$PICArr['StaffType'] = "HELPER";
for ($i = 0; $i < sizeof($helper); $i++) {
	if($helper[$i]){
		$PICArr['UserID'] = $helper[$i];
		$libenroll->ADD_EVENTSTAFF($PICArr);
	}
}

/******************** Set Right *********************/

$StudentArr['EnrolEventID'] = $EnrolEventID;
$StudentArr['ApplyUserType'] = $ApplyUserType;
$libenroll->UPDATE_APPLY_USERTYPE($StudentArr);

if ($plugin['iPortfolio']) {
	$dataAry['CategoryID'] = $category;
	$dataAry['OLE_Component'] = (count($ele_global)==0) ? "" : implode(',', $ele_global);
	$dataAry['Organization'] = trim(stripslashes($organization));
	$dataAry['Details'] = trim(stripslashes($details));
	$dataAry['SchoolRemark'] = trim(stripslashes($SchoolRemarks));
	$dataAry['TitleLang'] = $enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['English'];
	$dataAry['IntExt'] = $intExt;
	
	$libenroll->Update_Enrolment_Default_OLE_Setting('activity', $EnrolEventID, $dataAry);
}

intranet_closedb();
//header("Location: event_new1b.php?EnrolEventID=$EnrolEventID&isNew=$isNew");
if(IntegerSafe($DirectToMeetingActivity)){
	$_para = 'AcademicYearID='.$AcademicYearID.'&recordId='.$EnrolEventID.'&recordType='.$enrolConfigAry['Activity'].'&DirectToMemberActivity=1';
	$link = "meeting_date.php?$_para";
	header("Location: ".$link);
} else {
//	header("Location: event.php?msg=2&pic_view=".$pic_view);
    if (isset($result['add'])) {
        $returnMsgKey = $result['add'] ? 'AddSuccess' : 'AddUnSuccess';
    }
    else if (isset($result['edit'])) {
        $returnMsgKey = $result['edit'] ? 'UpdateSuccess' : 'UpdateUnSuccess';
    }
	header("Location: event.php?pic_view=".$pic_view."&AcademicYearID=".$AcademicYearID."&returnMsgKey=".$returnMsgKey);
}
?>