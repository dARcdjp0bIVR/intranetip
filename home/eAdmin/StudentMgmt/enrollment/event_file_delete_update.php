<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$EventArr['EnrolEventID'] = $EnrolEventID;
$EventArr['FileToDel'] = $FileToDel;

$EnrolEventID = $libenroll->DEL_EVENTINFO_FILE($EventArr);

intranet_closedb();
header("Location: event_setting.php?EnrolEventID=$EnrolEventID");
?>