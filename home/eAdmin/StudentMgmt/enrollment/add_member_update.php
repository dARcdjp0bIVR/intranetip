<?php
## Using By: 
######################################
##	Modification Log:
##  Date:   2019-11-21 (Henry)
##          bug fix for Fail to approve some time crash student
##			bug fix for calling $libeClassApp->isEnabledSendBulkPushMessageInBatches()
##
##  Date:   2018-09-28 (Anna)
##          added check only send push message when student in $uid
##
##	Date:	2017-10-09 (Anna)
##			add send push message when check time crashed 
##
##	Date:	2016-12-20 (Carlos) 
##			$sys_custom['StudentAttendance']['SyncDataToPortfolio'] - sync student activity profile and portfolio.
##
##	Date:	2015-10-07 (Omas)
## 		-Improved $sys_custom['eEnrolment']['ActivityAddRole'] , Get Role title from ID
##
##	Date:	2014-12-02 Omas
##			New setting $libenroll->notCheckTimeCrash, $libenroll->Event_notCheckTimeCrash bypass time crash checking
##			comment( skip time crash checking if $libenroll->oneEnrol != 1 )
##
##	Date:	2014-10-14	(Omas)
##			For type 0, 3 updating activeStatus while assign member to clubs 
##	
##	2011-11-10 (Henry Chow)
##	skip time crash checking if $libenroll->oneEnrol != 1
##
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition
######################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();


//debug_pr($_POST);

if ($plugin['eEnrollment'])
{
	$libenroll = new libclubsenrol();
	$libenroll->Syn_Student_Number_Of_Approved_Club();

	$_POST['activeStatus'] = IntegerSafe($_POST['activeStatus']);
	$AcademicYearID = IntegerSafe($AcademicYearID);
	$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
	$EnrolEventID = IntegerSafe($EnrolEventID);
	$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
	$keyword = cleanHtmlJavascript($keyword);

	$CurrentPICID = $_SESSION['UserID'];
	$EnrolGroupNameArray = $libenroll->getGroupInfo($GroupID);
	$CurrentPICNameArr = $libenroll->Get_User_Info_By_UseID($CurrentPICID);
	$CurrentEventName = $libenroll->GET_EVENT_TITLE($EnrolEventID);
	
	$CurrentPICChiName = $CurrentPICNameArr[0]['ChineseName'];
	$CurrentPICEngName = $CurrentPICNameArr[0]['EnglishName'];
	
	$EnrolGroupEngName = $EnrolGroupNameArray[0]['Title'] == ''? $CurrentEventName :  $EnrolGroupNameArray[0]['Title'];
	$EnrolGroupChiName = $EnrolGroupNameArray[0]['TitleChinese'] == ''? $EnrolGroupEngName: $EnrolGroupNameArray[0]['TitleChinese'];

	$CurrentEventChiName= $CurrentEventName==''?  $EnrolGroupNameArray[0]['TitleChinese']: $CurrentEventName;
	$CurrentEventEngName= $CurrentEventName==''?  $EnrolGroupNameArray[0]['Title']: $CurrentEventName;
	
	$lg = new libgroup($GroupID);
	
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($EnrolGroupID) && !$libenroll->IS_EVENT_PIC($EnrolEventID) && !$lg->hasAdminBasicInfo($UserID) && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		
	$StuArr['GroupID'] = $GroupID;
	$StuArr['EnrolGroupID'] = $EnrolGroupID;
	$StuArr['EnrolEventID'] = $EnrolEventID;
	$StuArr['EventEnrolID'] = $EnrolEventID;
	$StuArr['RecordStatus'] = 0;
	
	$result_ary = array();
	$imported_student = array();
	$crash_GroupID_ary = array();
	$crash_EnrolEventID_ary = array();
	
	# all students who are time-crashed
	$crashedStudentArr = unserialize(rawurldecode($crashedStudentArr));
	$crashedClubStudentEnrolGroupIDArr = unserialize(rawurldecode($crashedClubEnrolGroupIDArr)); // crashed club enrol group id
	$crashedEventIDArr =  unserialize(rawurldecode($crashedEventIDAry));


	if ($fromImport == "1")
	{
		# arrays from import member page
		if (isset($student))
		{
			$student = unserialize(rawurldecode($student));
		}
		if (isset($RoleIDArr))
		{
			$RoleIDArr = unserialize(rawurldecode($RoleIDArr));
		}
		if (isset($RoleTitleArr))
		{
			$RoleTitleArr = unserialize(rawurldecode($RoleTitleArr));
		}
		if (isset($ImportIDArr))
		{
			$ImportIDArr = unserialize(rawurldecode($ImportIDArr));
		}
		if (isset($ImportTitleArr))
		{
			$ImportTitleArr = unserialize(rawurldecode($ImportTitleArr));
		}
		
		# arrays from add member result page after checking time clash
		if (isset($crashedRoleIDArr))
		{
			$crashedRoleIDArr = unserialize(rawurldecode($crashedRoleIDArr));
		}
		if (isset($crashedRoleTitleArr))
		{
			$crashedRoleTitleArr = unserialize(rawurldecode($crashedRoleTitleArr));
		}
		if (isset($crashedImportTitleArr))
		{
			$crashedImportTitleArr = unserialize(rawurldecode($crashedImportTitleArr));
		}
		if (isset($crashedImportIDArr))
		{
			$crashedImportIDArr = unserialize(rawurldecode($crashedImportIDArr));
		}
	}
		
	/*
	 *	type=0 => add club member with Role selection (from member_index.php)
	 *  type=1 => add activity student with RecordStatus selection (from event_member.php)
	 *  type=2 => add club member with RecordStatus selection (from group_member.php)
	 *  type=3 => add activity student with Role selection (from event_member_index.php)
	 */
	
	if($sys_custom['eEnrolment']['ActivityAddRole']){
		// using RoleID to get the Role Title value
		$sql = "SELECT RoleID ,Title FROM INTRANET_ROLE WHERE RecordType = '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'";
		$roleArr = BuildMultiKeyAssoc( $libenroll->returnResultSet($sql) , 'RoleID', array('Title'),1);
	}
	$type = IntegerSafe($type);
	#new setting for not checking time crash 2014-12-02
	if ($type==0 || $type==2 ){
		$notCheckTimeCrashSetting = $libenroll->notCheckTimeCrash; 
	}
	else if($type==1 || $type==3 ){
		$notCheckTimeCrashSetting = $libenroll->Event_notCheckTimeCrash;
	}

	if ($from == "addMemberResult")	//add directly without checking
	{
		$student = $crashedStudentArr;
		$student = array_values($student);
		$ImportTitleArr = $crashedImportTitleArr;
		
		if ($fromImport == "1")
		{
			$RoleIDArr = $crashedRoleIDArr;
			$RoleTitleArr = $crashedRoleTitleArr;
		}
		
		if ($type == 0 || $type == 2)
		{
			for ($i = 0; $i < sizeof($student); $i++) {
				$StuArr['StudentID'] = $student[$i];
				
				if ($fromImport == "1")
				{
					$RoleID = $RoleIDArr[$i];
				}
				
				if ($importAll)
				{
					$GroupID = $crashedImportIDArr[$i];
				}
				
				// $uid is the approved students array
				if (in_array($StuArr['StudentID'],$uid))
				{
					// add student to the member list
					$libenroll->Add_Student_To_Club_Enrol_List($StuArr['StudentID'], $EnrolGroupID);
					if ($type == 0 || ($type == 2 && $enrolStatus==2))
					{
						if ($type == 0) {
							$ActiveMemberStatus = $_POST['activeStatus'];
						}
						else if ($type == 2 && $enrolStatus==2) {
							$ActiveMemberStatus = 'null';
						}
						
						$libenroll->Add_Student_To_Club_Member_List($StuArr['StudentID'], $EnrolGroupID, $RoleID, $ActiveMemberStatus);
						$email_student[] = $StuArr['StudentID'];
					}
					array_push($imported_student, $StuArr['StudentID']);
				}
				else
				{
					$result_ary[$i] = "rejected";
				}
			}
		}
		else if ($type == 1)
		{
			for ($i = 0; $i < sizeof($student); $i++) {
				$StuArr['StudentID'] = $student[$i];
				
				// $uid is the approved students array
				if (in_array($StuArr['StudentID'],$uid))
				{
					//add student to the event with specific status
					$libenroll->ADD_EVENTSTUDENT($StuArr);
					if ($enrolStatus==2)	//approved => add to activity enrol list as well
				    {
						$libenroll->APPROVE_EVENT_STU($StuArr);
						$email_student[] = $StuArr['StudentID'];
					}
					array_push($imported_student, $StuArr['StudentID']);
				}
				else
				{
					$result_ary[$i] = "rejected";
				}
			}
		}
		else if ($type == 3)
		{
			$ActiveMemberStatus = $_POST['activeStatus'];
			for ($i = 0; $i < sizeof($student); $i++) {
				$StuArr['StudentID'] = $student[$i];
				$StuArr['isActiveMember'] = $ActiveMemberStatus;

				if ($fromImport == "1")
				{
					$RoleTitle = $RoleTitleArr[$i];
				}
				
				if ($importAll)
				{
					$EnrolEventID = $crashedImportIDArr[$i];
				}
				
				// $uid is the approved students array
				if (in_array($StuArr['StudentID'],$uid))
				{
					// add student to the activity list
					$libenroll->ADD_EVENTSTUDENT($StuArr);
					$libenroll->APPROVE_EVENT_STU($StuArr);
					if($sys_custom['eEnrolment']['ActivityAddRole']){
						// using RoleID to get the Role Title value
						$roleID = $_POST['RoleTitle'];
						$RoleTitle = $roleArr[$roleID];
					}
					$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET RoleTitle = '".$RoleTitle."' WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '".$StuArr['StudentID']."'";
					$libenroll->db_db_query($sql);
					array_push($imported_student, $StuArr['StudentID']);
					$email_student[] = $StuArr['StudentID'];
				}
				else
				{
					$result_ary[$i] = "rejected";
				}
			}
		}
	}
	else	//check time conflict if not from add_member_result
	{	
		if ($type==0){
			for ($i = 0; $i < sizeof($student); $i++) {
				$StuArr['StudentID'] = $student[$i];
				
				if ($importAll)
				{
					$thisGroupID = $ImportIDArr[$i];
					
					if ($thisGroupID == -1)
					{
						$result_ary[$i] = "club_not_found";
						continue;
					}
					else if ($thisGroupID == -2)
					{
						$result_ary[$i] = "clubs_with_same_name";
						continue;
					}
					else
					{
						$GroupID = $thisGroupID;
					}
				}
				
				if($StuArr['StudentID']==-1)
				{
					$result_ary[$i] = "student_not_found";
					continue;
				}
				
				if ($fromImport == "1")
				{
					$RoleID = $RoleIDArr[$i];
				}
				
				//check if the student is already a member	
				if ($libenroll->Is_Club_Member($StuArr['StudentID'], $EnrolGroupID))
				{
					//ignore student if the student is already a member
					$result_ary[$i] = "student_is_member";
					continue;	
				}
				
				/* Need not to check the club and student quota for manual approval
				//check quota of club
				$clubQuotaLeft = $libenroll->GET_GROUP_QUOTA_LEFT($GroupID);
				if ($clubQuotaLeft <= 0)
				{
					//no quota left
					$result_ary[$i] = "club_quota_exceeded";
					continue;	
				}
				
				//check if the student has enrolled any clubs
				if ($libenroll->Is_Student_Enrolled_In_Club($StuArr['StudentID']))
				{
					//check quota of student if one has applied for clubs already
					$studentQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($StuArr['StudentID']);
					{
						if ($studentQuotaLeft <= 0)
						{
							//no quota left
							$result_ary[$i] = "student_quota_exceeded";
							continue;
						}
					}
				}
				*/
				if ($notCheckTimeCrashSetting !=1){
					//check for time crash of clubs
					$crashClub = false;
		
//					if($libenroll->oneEnrol) {
						//get all clubs that the student has joined
						//$sql = "SELECT EnrolGroupID FROM INTRANET_USERGROUP WHERE UserID = '".$StuArr['StudentID']."'";
						//$StudentClub = $libenroll->returnArray($sql,1);
						$StudentClub = $libenroll->Get_Student_Club_Info($StuArr['StudentID']);
						$numOfStudentClub = count($StudentClub);
						for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
							if ( ($libenroll->IS_CLUB_CRASH($EnrolGroupID, $StudentClub[$CountCrash]['EnrolGroupID'], $StudentClub)) ) {
								$crashClub = true;
								$crash_EnrolGroupID_ary[$i][] = $StudentClub[$CountCrash]['EnrolGroupID'];
								continue;
							}
						}
//					}
	
					if ($crashClub)
					{
						$result_ary[$i] = "crash_group";
					}

					//check for time crash of activities
					$crashActivity = false;
					//get student's enrolled event list, check for any time crash
					$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($StuArr['StudentID']);
					$numOfStudentEvent = count($StudentEvent);
					for ($CountEvent = 0; $CountEvent < $numOfStudentEvent; $CountEvent++) {
						if ($libenroll->IS_EVENT_GROUP_CRASH($EnrolGroupID, $StudentEvent[$CountEvent]['EnrolEventID'])) {
							$crashActivity = true;
							$crash_EnrolEventID_ary[$i][] = $StudentEvent[$CountEvent]['EnrolEventID'];
							continue;
						}
					}
	
					if ($crashActivity)
					{
						$result_ary[$i] = "crash_activity";
					}
		
					if ($crashActivity && $crashClub)
					{
						$result_ary[$i] = "crash_group_activity";
					}
				
					if ($crashActivity || $crashClub)
					{
						continue;
					}
					
				}
			

				// add student to the member list
				$libenroll->Add_Student_To_Club_Enrol_List($StuArr['StudentID'], $EnrolGroupID);
				
				$ActiveMemberStatus = $_POST['activeStatus'];
				$libenroll->Add_Student_To_Club_Member_List($StuArr['StudentID'], $EnrolGroupID, $RoleID, $ActiveMemberStatus);
				array_push($imported_student, $StuArr['StudentID']);
				$email_student[] = $StuArr['StudentID'];
			}
		}
		else if ($type==1){	//add student to activity (with record status)	
			for ($i = 0; $i < sizeof($student); $i++) {
				$StuArr['StudentID'] = $student[$i];
				
				//check if the student has already enrolled in the activity
				$sql = "SELECT COUNT(DISTINCT StudentID) FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '".$EnrolEventID."' AND StudentID = '".$StuArr['StudentID']."' AND RecordStatus = 2";
				$result = $libenroll->returnArray($sql);				
				//ignore student if one has joined the activity already
				if ($result[0][0]!=0)
				{
					$result_ary[$i] = "student_is_joined_event";
					continue;
				}
				
				/* Need not to check the quota for manual approval
				//check default school quota
				if ($libenroll->WITHIN_ENROLMENT_STAGE("activity") && ($libenroll->GET_STUDENT_NUMBER_OF_ENROLLED_EVENT($StuArr['StudentID']) >= $libenroll->Event_EnrollMax))
				{
					//exceeded school default quota of enrolment
					$result_ary[$i] = "default_school_quota_exceeded";
					continue;	
				}
				
				//check quota of activity
				$actQuotaLeft = $libenroll->GET_EVENT_QUOTA_LEFT($EnrolEventID);
				if ($actQuotaLeft <= 0)
				{
					//no quota left
					$result_ary[$i] = "act_quota_exceeded";
					continue;	
				}
				*/
				
				if ($notCheckTimeCrashSetting !=1){
					//check for time crash of clubs
					$crashClub = false;
					//get all clubs that the student has joined -> check time crash
					//$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$StuArr['StudentID']."'";
					//$StudentClub = $libenroll->returnArray($sql,1);
					$StudentClub = $libenroll->Get_Student_Club_Info($StuArr['StudentID']);
					$numOfStudentClub = count($StudentClub);
					for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
						if ( ($libenroll->IS_EVENT_GROUP_CRASH($StudentClub[$CountCrash]['EnrolGroupID'], $EnrolEventID))) {
							$crashClub = true;
							$crash_EnrolGroupID_ary[$i][] = $StudentClub[$CountCrash]['EnrolGroupID'];
							continue;
						}
					}
					if ($crashClub)
					{
						$result_ary[$i] = "crash_group";
					}
					
					//check for time crash of activities
					$crashActivity = false;
					//get student's enrolled event list, check for any time crash
					$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($StuArr['StudentID']);
					$numOfStudentEvent = count($StudentEvent);
					for ($CountEvent = 0; $CountEvent < $numOfStudentEvent; $CountEvent++) {
						if ($libenroll->IS_EVENT_CRASH($EnrolEventID, $StudentEvent[$CountEvent]['EnrolEventID'], $StudentEvent)) {
							$crashActivity = true;
							$crash_EnrolEventID_ary[$i][] = $StudentEvent[$CountEvent]['EnrolEventID'];
							continue;
						}
					}
					if ($crashActivity)
					{
						$result_ary[$i] = "crash_activity";
					}	
					
					if ($crashActivity && $crashClub)
					{
						$result_ary[$i] = "crash_group_activity";
					}
					
					if ($crashActivity || $crashClub)
					{
						continue;
					}		
				}
				
				//add student to the event with specific status
				$libenroll->ADD_EVENTSTUDENT($StuArr);
				if ($status==2)	//approved => add to activity enrol list as well
			    {
					$libenroll->APPROVE_EVENT_STU($StuArr);
					$email_student[] = $StuArr['StudentID'];
				}
				array_push($imported_student, $StuArr['StudentID']);
			}
		}	
		else if ($type==2){	//add student to the enrolment list
			for ($i = 0; $i < sizeof($student); $i++) {
				$StuArr['StudentID'] = $student[$i];
				
				//check if the student is already a member
				//ignore student if the student is already a member
				if ($libenroll->Is_Club_Member($StuArr['StudentID'], $EnrolGroupID))
				{
					$result_ary[$i] = "student_is_member";
					continue;	
				}
				
				/* Need not to check the quota for manual approval
				//check quota of club
				$clubQuotaLeft = $libenroll->GET_GROUP_QUOTA_LEFT($GroupID);
				if ($clubQuotaLeft <= 0)
				{
					//no quota left
					$result_ary[$i] = "club_quota_exceeded";
					continue;	
				}
				
				if ($libenroll->Is_Student_Enrolled_In_Club($StuArr['StudentID']))
				{
					//check quota of student
					$studentQuotaLeft = $libenroll->GET_STUDENT_QUOTA_LEFT($StuArr['StudentID']);
					{
						if ($studentQuotaLeft <= 0)
						{
							//no quota left
							$result_ary[$i] = "student_quota_exceeded";
							continue;
						}
					}
				}
				*/
				if ($notCheckTimeCrashSetting !=1){
					//check for time crash of clubs
					$crashClub = false;
					//get all clubs that the student has joined
					//$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$StuArr['StudentID']."'";
					//$StudentClub = $libenroll->returnArray($sql,1);
					$StudentClub = $libenroll->Get_Student_Club_Info($StuArr['StudentID']);
					$numOfStudentClub = count($StudentClub);
					for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
						if ( ($libenroll->IS_CLUB_CRASH($EnrolGroupID, $StudentClub[$CountCrash]['EnrolGroupID'], $StudentClub)) ) {
							$crashClub = true;
							$crash_EnrolGroupID_ary[$i][] = $StudentClub[$CountCrash]['EnrolGroupID'];
							continue;
						}
					}
					if ($crashClub)
					{
						$result_ary[$i] = "crash_group";
					}
					
					//check for time crash of activities
					$crashActivity = false;
					//get student's enrolled event list, check for any time crash
					$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($StuArr['StudentID']);
					$numOfStudentEvent = count($StudentEvent);
					for ($CountEvent = 0; $CountEvent < $numOfStudentEvent; $CountEvent++) {
						if ($libenroll->IS_EVENT_GROUP_CRASH($EnrolGroupID, $StudentEvent[$CountEvent]['EnrolEventID'])) {
							$crashActivity = true;
							$crash_EnrolEventID_ary[$i][] = $StudentEvent[$CountEvent]['EnrolEventID'];
							continue;
						}
					}
					if ($crashActivity)
					{
						$result_ary[$i] = "crash_activity";
					}
					
					if ($crashActivity && $crashClub)
					{
						$result_ary[$i] = "crash_group_activity";
					}
					
					if ($crashActivity || $crashClub)
					{
						continue;
					}
				}
				
				// add student to the member list
				$libenroll->Add_Student_To_Club_Enrol_List($StuArr['StudentID'], $EnrolGroupID);
				if ($status==2)	//approved => add to member list as well
			    {
					$libenroll->Add_Student_To_Club_Member_List($StuArr['StudentID'], $EnrolGroupID, $RoleID);
					$email_student[] = $StuArr['StudentID'];
				}
				array_push($imported_student, $StuArr['StudentID']);
			}
		}
		else if ($type==3){	//add student to activity enrol list directly
			$ActiveMemberStatus = $_POST['activeStatus'];
			for ($i = 0; $i < sizeof($student); $i++) {
				$StuArr['StudentID'] = $student[$i];
				$StuArr['isActiveMember'] = $ActiveMemberStatus;
				if($StuArr['StudentID']==-1)
				{
					$result_ary[$i] = "student_not_found";
					continue;
				}
				
				if ($importAll)
				{
					$thisEnrolEventID = $ImportIDArr[$i];
					
					if ($thisEnrolEventID == -1)
					{
						$result_ary[$i] = "activity_not_found";
						continue;
					}
					else if ($thisEnrolEventID == -2)
					{
						$result_ary[$i] = "activity_with_same_name";
						continue;
					}
					else
					{
						$EnrolEventID = $thisEnrolEventID;
					}
				}
				
				if ($fromImport == "1")
				{
					$RoleTitle = $RoleTitleArr[$i];
				}
			
				//check if the student has already enrolled in the activity
				$sql = "SELECT COUNT(DISTINCT StudentID) FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '".$EnrolEventID."' AND StudentID = '".$StuArr['StudentID']."' AND RecordStatus = 2";
				$result = $libenroll->returnArray($sql);				
				//ignore student if one has joined the activity already
				if ($result[0][0]!=0)
				{
					$result_ary[$i] = "student_is_joined_event";
					continue;
				}
				
				/* Need not to check the club quota for manual approval
				//check default school quota
				if ($libenroll->WITHIN_ENROLMENT_STAGE("activity") && ($libenroll->GET_STUDENT_NUMBER_OF_ENROLLED_EVENT($StuArr['StudentID']) >= $libenroll->Event_EnrollMax))
				{
					//exceeded school default quota of enrolment
					$result_ary[$i] = "default_school_quota_exceeded";
					continue;	
				}
				
				//check quota of activity
				$actQuotaLeft = $libenroll->GET_EVENT_QUOTA_LEFT($EnrolEventID);
				if ($actQuotaLeft <= 0)
				{
					//no quota left
					$result_ary[$i] = "act_quota_exceeded";
					continue;
				}
				*/
				
				if ($notCheckTimeCrashSetting !=1){
					//check for time crash of clubs
					$crashClub = false;
					//get all clubs that the student has joined -> check time crash
					//$sql = "SELECT GroupID FROM INTRANET_USERGROUP WHERE UserID = '".$StuArr['StudentID']."'";
					//$StudentClub = $libenroll->returnArray($sql,1);
					$StudentClub = $libenroll->Get_Student_Club_Info($StuArr['StudentID']);
					$numOfStudentClub = count($StudentClub);
					for ($CountCrash = 0; $CountCrash < $numOfStudentClub; $CountCrash++) {
						if ( ($libenroll->IS_EVENT_GROUP_CRASH($StudentClub[$CountCrash]['EnrolGroupID'], $EnrolEventID))) {
							$crashClub = true;
							$crash_EnrolGroupID_ary[$i][] = $StudentClub[$CountCrash]['EnrolGroupID'];
							continue;
						}
					}
					if ($crashClub)
					{
						$result_ary[$i] = "crash_group";
					}
				
					//check for time crash of activities
					$crashActivity = false;
					//get student's enrolled event list, check for any time crash
					$StudentEvent = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($StuArr['StudentID']);
					$numOfStudentEvent = count($StudentEvent);
					for ($CountEvent = 0; $CountEvent < $numOfStudentEvent; $CountEvent++) {
						if ($libenroll->IS_EVENT_CRASH($EnrolEventID, $StudentEvent[$CountEvent]['EnrolEventID'], $StudentEvent)) {
							$crashActivity = true;
							$crash_EnrolEventID_ary[$i][] = $StudentEvent[$CountEvent]['EnrolEventID'];
							continue;
						}
					}
					if ($crashActivity)
					{
						$result_ary[$i] = "crash_activity";
					}		
					
					if ($crashActivity && $crashClub)
					{
						$result_ary[$i] = "crash_group_activity";
					}
					
					if ($crashActivity || $crashClub)
					{
						continue;
					}
				}
				
				// add student to the activity list
				$libenroll->ADD_EVENTSTUDENT($StuArr);
				$libenroll->APPROVE_EVENT_STU($StuArr);
				if($sys_custom['eEnrolment']['ActivityAddRole']){
					// using RoleID to get the Role Title value
					$roleID = $_POST['RoleTitle'];
					
					$RoleTitle = $roleArr[$roleID];
				}			
				
				$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET RoleTitle = '".$RoleTitle."' WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '".$StuArr['StudentID']."'";
				$success = $libenroll->db_db_query($sql);
				
				array_push($imported_student, $StuArr['StudentID']);
				$email_student[] = $StuArr['StudentID'];
			}
		}
	}
	
	if($sys_custom['eEnrolment']['SyncDataToPortfolio']){
		if ($type==0 || $type==2) {
			$libenroll->SyncProfileRecordsForClub($AcademicYearID, $YearTermID, $EnrolGroupID);
		} else if ($type==1 || $type==3) {
			$libenroll->SyncProfileRecordsForActivity($AcademicYearID, $YearTermID, $EnrolEventID);
		}
		$libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
	}
	
	if($plugin['eClassTeacherApp']){
		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
		$libeClassApp = new libeClassApp();
		$lookupClashestoSendPushMessagetoClubPIC = $libenroll->lookupClashestoSendPushMessagetoClubPIC;
		if($lookupClashestoSendPushMessagetoClubPIC){
		    $messageTitle = $Lang['eEnrolment']['PushMessage']['ClubCrashTitle'];
		    $appType = $eclassAppConfig['appType']['Teacher'];
		  
		    for($k=0;$k<sizeof($crashedClubStudentEnrolGroupIDArr);$k++){
					
		            $studentCrashedClubEnrolGroupIDAry = $crashedClubStudentEnrolGroupIDArr[$k];
					$thiscrasedClubStudentID = $crashedStudentArr[$k];
					
					if( 
					    (in_array($thiscrasedClubStudentID,(array)$uid) && ($type==1||$type == 2))
					    || ($type==0||$type == 3)
					   ){		
			
// 					$StudentInfoArr = $libenroll->Get_User_Info_By_UseID($thiscrasedClubStudentID);
// 					$StudentEnglishName = $StudentInfoArr[0]['EnglishName'];
// 					$StudentChineseName = $StudentInfoArr[0]['ChineseName'];
// 					$StudentName =Get_Lang_Selection($StudentChineseName,$StudentEnglishName)==''?$StudentEnglishName:Get_Lang_Selection($StudentChineseName,$StudentEnglishName);
			
					    $StudentNameArr = $libenroll->Get_User_Info_By_UseID($thiscrasedClubStudentID);
				
					    $individualMessageInfoAry=array();
					    for($i=0;$i<sizeof($studentCrashedClubEnrolGroupIDAry);$i++){
    						
					        $thisEnrolGroupID = $studentCrashedClubEnrolGroupIDAry[$i];
    						$crashedClubPICArr = $libenroll->GET_GROUPSTAFF($thisEnrolGroupID,'PIC');
    						
    						$crashedClubGroupID = $libenroll->GET_GROUPID($thisEnrolGroupID);
    						$crashedClubGroupNameArray = $libenroll->getGroupInfo($crashedClubGroupID);
    						
    // 						$crashedClubName =Get_Lang_Selection($crashedClubGroupNameArray[0]['TitleChinese'],$crashedClubGroupNameArray[0]['Title']) ==''?$crashedClubGroupNameArray[0]['Title']:Get_Lang_Selection($crashedClubGroupNameArray[0]['TitleChinese'],$crashedClubGroupNameArray[0]['Title']);
    						$PICStudentAssoAry = array();
    						for($j=0;$j<sizeof($crashedClubPICArr);$j++){
    							
    							$crashedClubPICid = $crashedClubPICArr[$j]['UserID'];
    							
    							$PICStudentAssoAry[$crashedClubPICid] = array($thiscrasedClubStudentID);
    							
    						}
    						
    						$messageContent= str_replace("__StudentEngName__", $StudentNameArr[0]['EnglishName'], $Lang['eEnrolment']['PushMessage']['ClubCrash']);
    						$messageContent= str_replace("__StudentChiName__", $StudentNameArr[0]['ChineseName'], $messageContent);
    					
    						$messageContent= str_replace("__CurrentPICEng__", $CurrentPICEngName,$messageContent);
    						$messageContent= str_replace("__CurrentPICChi__", $CurrentPICChiName,$messageContent);
    					
    						$messageContent= str_replace("__CurrentClubEngName__", $EnrolGroupEngName,$messageContent);
    						$messageContent= str_replace("__CurrentClubChiName__", $EnrolGroupChiName,$messageContent);
    						
    						$messageContent= str_replace("__ClubChiName__", $crashedClubGroupNameArray[0]['TitleChinese'],$messageContent);
    						$messageContent= str_replace("__ClubEngName__", $crashedClubGroupNameArray[0]['Title'],$messageContent);
    						
    						
    						$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $PICStudentAssoAry;
    							
    						if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
    							$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
    						}
    						else {
    							$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
    						}
    						
    					}
					}
				}

		}
		if($libenroll->lookupClashestoSendPushMessagetoEventPIC){
				for($k=0;$k<sizeof($crashedEventIDArr);$k++){
					
					$crashedEventID = $crashedEventIDArr[$k];
					$thiscrasedEventStudentID = $crashedStudentArr[$k];
					
			//		$StudentInfoArr = $libenroll->Get_User_Info_By_UseID($thiscrasedEventStudentID);
					
					$StudentNameArr =$libenroll->Get_User_Info_By_UseID($thiscrasedEventStudentID);
					if((in_array($thiscrasedEventStudentID,(array)$uid) && ($type==1||$type == 2))
					    || ($type==0||$type == 3)
					    ){			
	   
					   $individualMessageInfoAry=array();
    					for($i=0;$i<sizeof($crashedEventID);$i++){
    						$thisEventID = $crashedEventID[$i];
    						$crashedEventPICArr = $libenroll->GET_EVENTSTAFF($thisEventID,'PIC');
    						
    						$crashedEventName = $libenroll->GET_EVENT_TITLE($thisEventID);
    						
    						$PICStudentAssoAry = array();
    						for($j=0;$j<sizeof($crashedEventPICArr);$j++){
    							$crashedEventPICid = $crashedEventPICArr[$j]['UserID'];
    							$PICStudentAssoAry[$crashedEventPICid] = array($thiscrasedEventStudentID);
    							
    						}
    						
    						$messageContent= str_replace("__StudentEngName__", $StudentNameArr[0]['EnglishName'], $Lang['eEnrolment']['PushMessage']['ActivityCrash']);
    						$messageContent= str_replace("__StudentChiName__", $StudentNameArr[0]['ChineseName'], $messageContent);
    						
    						$messageContent= str_replace("__CurrentPICEng__", $CurrentPICEngName,$messageContent);
    						$messageContent= str_replace("__CurrentPICChi__", $CurrentPICChiName,$messageContent);
    						
    						$messageContent= str_replace("__CurrentActivityChiName__", $CurrentEventChiName,$messageContent);
    						$messageContent= str_replace("__CurrentActivityEngName__", $CurrentEventEngName,$messageContent);
    						
    						$messageContent= str_replace("__ActivityName__", $crashedEventName,$messageContent);
    // 						$messageContent= str_replace("__ActivityEngName__", $crashedEventName,$messageContent);
    						
    	
    						$individualMessageInfoAry[0]['relatedUserIdAssoAry'] = $PICStudentAssoAry;
    						
    						
    						if ($libeClassApp->isEnabledSendBulkPushMessageInBatches()) {
    							$notifyMessageId = $libeClassApp->sendPushMessageByBatch($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
    						}
    						else {
    							$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, $messageContent, $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
    						}
    						                              
    					}
					}
				}	
 		}
	}


	// send email
	if ($type==0 || $type==2) {	# if type 0 or type 2, send club
		$RecordType = "club";
		$GroupEventId = $GroupID;
	} else if ($type==1 || $type==3) {	# if type 1 or type 3, send activity
		$RecordType = "activity";
		$GroupEventId = $EnrolEventID;
	} else {
		// do nothing
	}
	$successResult = true;
	$libenroll->Send_Enrolment_Result_Email($email_student, $GroupEventId, $RecordType, $successResult);
	
	intranet_closedb();

	?>
	
	<body>
	<form name="form1" method="post" action="add_member_result.php">
		<input type="hidden" name="from" value="ManualAdd">
		<input type="hidden" name="type" value="<?=$type?>">
		<input type="hidden" name="GroupID" value="<?=$GroupID?>">
		<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
		<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
		<input type="hidden" name="data" value="<?=rawurlencode(serialize($student));?>">
		<input type="hidden" name="result_ary" value="<?=rawurlencode(serialize($result_ary));?>">
		<input type="hidden" name="imported_student" value="<?=rawurlencode(serialize($imported_student));?>">
		<? if ($from == "addMemberResult"){ ?>
			<input type="hidden" name="enrolStatus" value="<?=$enrolStatus?>">
			<input type="hidden" name="crashedStudentArr" value="<?=$crashedStudentArr?>">
		<? } else { ?>
			<input type="hidden" name="enrolStatus" value="<?=$status?>">
		<? } ?>
		<input type="hidden" name="RoleID" value="<?=$RoleID?>">
		<input type="hidden" name="RoleTitle" value="<?=$RoleTitle?>">
		<input type="hidden" name="crash_EnrolGroupID_ary" value="<?=rawurlencode(serialize($crash_EnrolGroupID_ary));?>">
		<input type="hidden" name="crash_EnrolEventID_ary" value="<?=rawurlencode(serialize($crash_EnrolEventID_ary));?>">	
		<input type="hidden" name="filter" id="filter" value="<?= $filter?>" />
		<input type="hidden" name="field" id="field" value="<?= $field?>" />
		<input type="hidden" name="order" id="order" value="<?= $order?>" />
		<input type="hidden" name="keyword" id="keyword" value="<?= $keyword?>" />
		
		<input type="hidden" name="fromImport" id="fromImport" value="<?= $fromImport?>" />
		<input type="hidden" name="RoleIDArr" value="<?=rawurlencode(serialize($RoleIDArr));?>">
		<input type="hidden" name="RoleTitleArr" value="<?=rawurlencode(serialize($RoleTitleArr));?>">
		<input type="hidden" name="ImportIDArr" value="<?=rawurlencode(serialize($ImportIDArr));?>">
		<input type="hidden" name="ImportTitleArr" value="<?=rawurlencode(serialize($ImportTitleArr));?>">
		<input type="hidden" name="importAll" value="<?=$importAll?>">
		<input type="hidden" name="importType" value="<?=$importType?>">
		<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
		
		<input type="hidden" name="NotFoundClassNameArr" value="<?=$NotFoundClassNameArr?>">
		<input type="hidden" name="NotFoundClassNumberArr" value="<?=$NotFoundClassNumberArr?>">
		<input type="hidden" name="activeStatus" id="activeStatus" value="<?=$_POST['activeStatus']?>">
	</form>
	
	<script language="Javascript">
		document.form1.submit();
	</script>
	
	</body>
	
<?
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>