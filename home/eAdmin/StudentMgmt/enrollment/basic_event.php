<?php
#Modify : 
/********************************************************
 *  Modification Log
 *  2017-10-06 Anna
 *  	- Add lookupClashestoSendPushMessagetoEventPIC setting
 *  
 *  2015-01-26 Kenneth
 * 		- Add class 'simple' / 'advanced' to hide/show elements in js: changemode()
 * 
 *  2015-06-01 Omas
 * 		- Clear all button disabled
 *  2013-02-26	Rita
 * 		- add Default Attendance Status Contol for customization
 * 
 * 	2012-12-21  Ivan
 * 		- added term-based application quota settings
 * 
 * 	2012-11-01	Rita
 * 		- add Attendance Helper Control 
 * 
 *		2011-09-07	YatWoon
 *		- lite version implementation
 * 
 * 	2010-08-02  Thomas
 * 		- Add options 'Disable Add/Update enrollment data' in Advanced settings
 *        Once activated, users can't update or add enrollment data
 *
 ********************************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}



if ($plugin['eEnrollment'])
{
	$CurrentPage = "PageSysSettingBasicActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

	//if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	//	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$enrolInfo = $libenroll->getEnrollmentInfo();

    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($Lang['eEnrolment']['GeneralSetting'], "", 1);
        $TAGS_OBJ[] = array($Lang['eEnrolment']['CategorySetting'], "category_setting_event.php", 0);
         
        # tags 
        //$current_tab = 1;
        //include_once("settings_tabs.php");

        $linterface->LAYOUT_START();
 
        
$basicmode = ($libenroll->Event_mode == "")? 0 : $libenroll->Event_mode;
$AppStart = $libenroll->Event_AppStart;
$AppStartHour = $libenroll->Event_AppStartHour;
$AppStartMin = $libenroll->Event_AppStartMin;
$AppEnd = $libenroll->Event_AppEnd;
$AppEndHour = $libenroll->Event_AppEndHour;
$AppEndMin = $libenroll->Event_AppEndMin;
//$GpStart = $libenroll->Event_GpStart;
//$GpEnd = $libenroll->Event_GpEnd;
$defaultMin = $libenroll->Event_defaultMin;
$defaultMax = $libenroll->Event_defaultMax;
//$tiebreak = $libenroll->Event_tiebreak;
$Description = $libenroll->Event_Description;
$EnrollMax = $libenroll->Event_EnrollMax;
//$enrollPersontype = $libenroll->Event_enrollPersontype;
$disableStatus = $libenroll->Event_disableStatus;
($libenroll->Event_activeMemberPer == NULL)? $activeMemberPer=0 : $activeMemberPer=$libenroll->Event_activeMemberPer;
$oneEnrol = $libenroll->Event_oneEnrol;
$onceEmail = $libenroll->Event_onceEmail;
$clubPICcreateNAAct = $libenroll->clubPICcreateNAAct;
$disableUpdate = $libenroll->Event_DisableUpdate;
$disableHelperModificationRightLimitation = $libenroll->Event_DisableHelperModificationRightLimitation;
$disableCheckingNoOfActivityStuWantToJoin = $libenroll->disableCheckingNoOfActivityStuWantToJoin;
if($plugin['eClassTeacherApp']){
	$lookupClashestoSendPushMessagetoEventPIC = $libenroll->lookupClashestoSendPushMessagetoEventPIC;
}


if($libenroll->enableActivityDefaultAttendaceSelectionControl()){
	$defaultAttendanceStatus = $libenroll->Event_DefaultAttendanceStatus;
}

if($disableStatus==1)
{
	$chkDisableStauts = "CHECKED";	
}
else
{
	$chkDisableStauts = "";	
}
	 

$chkOneEnrol = ($oneEnrol==1)? "CHECKED" : "";

$chkOnceEmail = ($onceEmail==1)? "checked='checked'" : "";
$chkDisableUpdate = ($disableUpdate==1)? "checked='checked'" : "";
$chkDisableHelperModificationRightLimitation = ($disableHelperModificationRightLimitation==1)? "checked='checked'" : "";


$basicmode += 0;
$chkDisabled = ($basicmode==0? "CHECKED":"");
$chkSimple = ($basicmode==1? "CHECKED":"");
$chkAdvanced = ($basicmode==2? "CHECKED":"");
$chkclubPICcreateNAAct = ($clubPICcreateNAAct==1? "CHECKED":"");
$chkDisableCheckingNoOfActivityStuWantToJoin = ($disableCheckingNoOfActivityStuWantToJoin==1)? "CHECKED" : "";
if($plugin['eClassTeacherApp']){
	$chkDisablelookupClashestoSendPushMessagetoEventPIC= ($lookupClashestoSendPushMessagetoEventPIC==1)? "CHECKED":"";
}

$min_numbers = array (0,1,2,3,4,5,6,7,8,9);
$max_numbers = array (array(0,$i_ClubsEnrollment_NoLimit));
for ($i=1; $i<10; $i++)
{
     $max_numbers[] = array($i,$i);
}

$minSelection = getSelectByValue($min_numbers,"name='defaultMin'",$defaultMin);
$maxSelection = getSelectByArray($max_numbers,"name='defaultMax'",$defaultMax);
$enrollMaxSelection = getSelectByArray($max_numbers,"name='EnrollMax'",$EnrollMax);

/*	No tiebreak and personal type selection for activity
$tiebreak += 0;
$chkRan = ($tiebreak==0? "SELECTED":"");
$chkTime = ($tiebreak==1? "SELECTED":"");
$tiebreakerSelection = "<SELECT name=tiebreak>\n";
$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
$tiebreakerSelection.= "</SELECT>\n";

$chkStu = ($enrollPersontype==0? "SELECTED":"");
$chkPa = ($enrollPersontype==1? "SELECTED":"");
$enrollPersontypeSelection = "<SELECT name='enrollPersontype'>\n";
$enrollPersontypeSelection.= "<OPTION value=0 $chkStu>".$eEnrollment['student']."</OPTION>\n";
$enrollPersontypeSelection.= "<OPTION value=1 $chkPa>".$eEnrollment['parent']."</OPTION>\n";
$enrollPersontypeSelection.= "</SELECT>\n";
*/

## Category Selection
//$CatSelection = $libenroll->GET_CATEGORY_SEL($sel_category, $sel_category, 'this.form.submit();', $Lang['General']['PleaseSelect'] );

## Application Quota Table
$htmlAry['applicationQuotaTable'] = $libenroll_ui->Get_Enrollment_Application_Quota_Settings_Table($enrolConfigAry['Activity'], $CategoryID=0);


if($libenroll->enableActivityDefaultAttendaceSelectionControl()){
	### default attendance status 
	$libclubsenrol_ui = new libclubsenrol_ui();
	$htmlAry['attendanceDefaultSelection'] = $libclubsenrol_ui->Get_Student_Attendance_Selection('DefaultAttendanceStatus', 'DefaultAttendanceStatus', $defaultAttendanceStatus);
}	

?>

<script language="javascript">
function changemode(obj,value){
     switch (value)
     {
         case 0:
              $('.simple').hide();
              $('.advanced').hide();
              obj.AppStart.disabled = true;
              obj.AppEnd.disabled = true;
              obj.enrolStartHour.disabled = true;
              obj.enrolStartMin.disabled = true;
              obj.enrolEndHour.disabled = true;
              obj.enrolEndMin.disabled = true;
              obj.Description.disabled = true;
              //obj.defaultMin.disabled = true;
              //obj.defaultMax.disabled = true;
              //obj.EnrollMax.disabled = true;
              $('select.ApplyMinSel').attr('disabled', 'disabled').val(0);
              $('select.ApplyMaxSel').attr('disabled', 'disabled').val(0);
              $('select.EnrollMaxSel').attr('disabled', 'disabled').val(0);
              //obj.tiebreak.disabled = true;
              //obj.enrollPersontype.disabled = true;
              obj.disableStatus.disabled = true;
              obj.activeMemberPer.disabled = true;
              obj.oneEnrol.disabled = true;
              obj.onceEmail.disabled = true;
			  obj.clubPICcreateNAAct.disabled = true;
			  //obj.clearRecord.disabled = true;
			  obj.disableUpdate.disabled = true;
			  obj.disableCheckingNoOfActivityStuWantToJoin.disabled = true;
			  obj.Event_DisableHelperModificationRightLimitation.disabled = true;
			  <?php if($libenroll->enableActivityDefaultAttendaceSelectionControl()){?>
                  obj.defaultAttendanceStatus.disabled = true;
              <?}?>
              break;
         case 1:
              $('.simple').show();
              $('.advanced').hide();
              obj.AppStart.disabled = false;
              obj.AppEnd.disabled = false;
              obj.enrolStartHour.disabled = false;
              obj.enrolStartMin.disabled = false;
              obj.enrolEndHour.disabled = false;
              obj.enrolEndMin.disabled = false;
              obj.Description.disabled = false;
              //obj.defaultMin.disabled = false;
              //obj.defaultMax.disabled = false;
              //obj.EnrollMax.disabled = false;
              $('select.ApplyMinSel').attr('disabled', '');
              $('select.ApplyMaxSel').attr('disabled', '');
              $('select.EnrollMaxSel').attr('disabled', '');
              //obj.tiebreak.disabled = true;
              //obj.enrollPersontype.disabled = false;
              obj.disableStatus.disabled = true;
              obj.activeMemberPer.disabled = true;
              obj.oneEnrol.disabled = true;
              obj.onceEmail.disabled = true;
			  obj.clubPICcreateNAAct.disabled = true;
			  //obj.clearRecord.disabled = true;
			  obj.disableUpdate.disabled = true;
			  obj.disableCheckingNoOfActivityStuWantToJoin.disabled = true;
			  obj.Event_DisableHelperModificationRightLimitation.disabled = true;
			  <?php if($libenroll->enableActivityDefaultAttendaceSelectionControl()){?>
              	obj.defaultAttendanceStatus.disabled = true;
              <?}?>
              break;
         case 2:
              $('.simple').show();
              $('.advanced').show();
              obj.AppStart.disabled = false;
              obj.AppEnd.disabled = false;
              obj.enrolStartHour.disabled = false;
              obj.enrolStartMin.disabled = false;
              obj.enrolEndHour.disabled = false;
              obj.enrolEndMin.disabled = false;
              obj.Description.disabled = false;
              //obj.defaultMin.disabled = false;
              //obj.defaultMax.disabled = false;
              //obj.EnrollMax.disabled = false;
              $('select.ApplyMinSel').attr('disabled', '');
              $('select.ApplyMaxSel').attr('disabled', '');
              $('select.EnrollMaxSel').attr('disabled', '');
              //obj.tiebreak.disabled = false;
              //obj.enrollPersontype.disabled = false;
              
              <? if($plugin['eEnrollmentLite']) {?>
              obj.disableStatus.disabled = true;
              obj.activeMemberPer.disabled = true;
              obj.onceEmail.disabled = true;
              <? } else {?>
              obj.disableStatus.disabled = false;
              obj.activeMemberPer.disabled = false;
              obj.onceEmail.disabled = false;
              <? } ?>
              
              obj.oneEnrol.disabled = false;
			  obj.clubPICcreateNAAct.disabled = false;
			  //obj.clearRecord.disabled = false;
			  obj.disableUpdate.disabled = false;
			  obj.disableCheckingNoOfActivityStuWantToJoin.disabled = false;
			  obj.Event_DisableHelperModificationRightLimitation.disabled = false;
			  <?php if($libenroll->enableActivityDefaultAttendaceSelectionControl()){?>
              	obj.defaultAttendanceStatus.disabled = false;
              <?}?>
              break;
     }
}

function checkform(obj){
	if ((obj.basicmode[1].checked)||(obj.basicmode[2].checked))
	{
		if (obj.defaultMax.value != 0 && obj.defaultMin.value > obj.defaultMax.value)
		{
			alert("<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>");
			return false;
		}
		if (obj.defaultMax.value != 0)
		{
			//if ( (obj.EnrollMax.value == 0) || (obj.EnrollMax.value < obj.defaultMin.value) || (obj.EnrollMax.value > obj.defaultMax.value) )
			if ( (obj.EnrollMax.value == 0) || (obj.EnrollMax.value > obj.defaultMax.value) )
			{
				alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
				return false;
			}
		}
		
//		var isVaild = true;
//		$('select.ApplyMinSel').each( function() {
//			var _id = $(this).attr('id');
//			var _idPieces = _id.split('_');
//			var _termNum = _idPieces[1];
//			var _categoryId = _idPieces[2];
//			
//			var _applyMin = $(this).val();
//			var _applyMax = $('select#ApplyMax_' + _termNum + '_' + _categoryId + '_Sel').val();
//			var _enrollMax = $('select#EnrollMax_' + _termNum + '_' + _categoryId + '_Sel').val();
//			
//			if (_applyMax != 0 && _applyMin > _applyMax) {
//				isVaild = false;
//				
//				alert('<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>');
//				$(this).focus();
//				return false; 	// break
//			}
//			
//			if (_applyMax != 0) {
//				if ( (_enrollMax == 0) || (_enrollMax > _applyMax) ) {
//					isVaild = false;
//					
//					alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
//					$(this).focus();
//					return false; 	// break
//				}
//			}
//		});
//		
//		if (isVaild == false) {
//			return false;
//		}
		
		
		if (obj.AppStart.value != '')
		{
			if (!check_date(obj.AppStart,"<?php echo $i_invalid_date; ?>")) return false;
		}
		if (obj.AppEnd.value != '')
		{
			if (!check_date(obj.AppEnd,"<?php echo $i_invalid_date; ?>")) return false;
		}

		if (obj.AppStart.value > obj.AppEnd.value)
		{
			alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
			return false;
		}
		else if (obj.AppStart.value == obj.AppEnd.value)
		{
			// check hours
			if (obj.enrolStartHour.value > obj.enrolEndHour.value)
			{
				alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
				return false;
			}
			else if (obj.enrolStartHour.value == obj.enrolEndHour.value)
			{
				// check minutes
				if (obj.enrolStartMin.value > obj.enrolEndMin.value)
				{
					alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
					return false;
				}
			}
		}	
		
		if (obj.disableStatus.checked)
		{
			obj.disableStatus.value = 1;
		}
		else
		{
			obj.disableStatus.value = 0;
		}
		
		if (isNaN(parseInt(obj.activeMemberPer.value)) || parseInt(obj.activeMemberPer.value) < 0 || parseInt(obj.activeMemberPer.value) > 100)
		{
			alert("<?=$eEnrollment['active_member_alert']?>");
			return false;
		}
		else
		{
			if (!IsNumeric(obj.activeMemberPer.value))
			{
				alert("<?=$eEnrollment['active_member_alert']?>");
				return false;
			}
		}
		

	}

//	if (obj.basicmode[1].checked)
//	{
//		if (obj.defaultMax.value != 0 && obj.defaultMin.value > obj.defaultMax.value)
//		{
//			alert("<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>");
//			return false;
//		}
//		if ( (obj.defaultMin.value > obj.EnrollMax.value) ||
//			 (obj.EnrollMax.value > obj.defaultMax.value) )
//		{
//			alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
//			return false;
//		}
//	}
	obj.action="basic_event_update.php";
	obj.submit();
	return true;
}

function showHideAlert()
{
	var alertSpan = document.getElementById("removeAlert");
	var state = alertSpan.style.visibility;
	
	if (state == "hidden")
	{
		alertSpan.style.visibility = "visible";
	}
	else
	{
		alertSpan.style.visibility = "hidden";
	}
}

</script>

<br/>
<form name="form1" method="post" enctype="multipart/form-data">
	<table width="98%" border="0" cellpadding="4" cellspacing="0" align="center">
		<tr><td colspan="2" class="tabletext" align="right"><? if ($msg == 2) print $linterface->GET_SYS_MSG("update"); ?></td></tr>
		<tr><td colspan="2" class="tabletext" ><?=$CatSelection?></td></tr>
		
		<!- Mode Selection -->
		<tr class="tablerow2"><td colspan="2" class="tabletext">
		<input type="radio" id="mode0" name="basicmode" value="0" onClick="changemode(this.form,0)" <?=$chkDisabled?>><label for="mode0"><?=$i_ClubsEnrollment_Disable?></label>
		<input type="radio" id="mode1" name="basicmode" value="1" onClick="changemode(this.form,1)" <?=$chkSimple?>><label for="mode1"><?=$i_ClubsEnrollment_Simple?></label>
		<input type="radio" id="mode2" name="basicmode" value="2" onClick="changemode(this.form,2)" <?=$chkAdvanced?>><label for="mode2"><?=$i_ClubsEnrollment_Advanced?></label>
		</td></tr>
		
		<!- Enrolment Stage -->
		<tr class="tablegreenrow2">
			<td colspan="2" valign="top"  class="formfieldtitle tabletext simple">
				<?= $eEnrollment['app_stage_event']?>
				<br />
				<?= $linterface->GET_DATE_PICKER("AppStart",$AppStart) ?>
				<!-- <input class="textboxnum" type="text" name="AppStart" size="10" maxlength="10" value="<?php echo $AppStart; ?>"> <?= $linterface->GET_CALENDAR("form1", "AppStart")?> -->
				<?= getTimeSel("enrolStart", $AppStartHour, $AppStartMin) ?>
				&nbsp;<?= $eEnrollment['event_and']?>&nbsp;
				<?= $linterface->GET_DATE_PICKER("AppEnd",$AppEnd) ?>
				<!-- <input class="textboxnum" type="text" name="AppEnd" size="10" maxlength="10" value="<?php echo $AppEnd; ?>"> <?= $linterface->GET_CALENDAR("form1", "AppEnd")?> -->
				<?= getTimeSel("enrolEnd", $AppEndHour, $AppEndMin) ?>
			</td>
		</tr>
		
		<!- Instruction -->
		<tr class="tablegreenrow2 simple">
			<td width="40%" valign="top"  class="formfieldtitle tabletext"><?php echo $i_ClubsEnrollment_ApplicationDescription; ?></td>
			<td><textarea name="Description" rows="5" cols="40" class="textboxtext"><?php echo $Description; ?></textarea></td>
		</tr>
		
		<!- Default minimum number of applications -->
		<tr class="tablegreenrow2 simple">
			<td valign="top"  class="formfieldtitle tabletext"><?= $eEnrollment['default_min_act']?></td>
			<td><?=$minSelection?></td>
		</tr>
		
		<!- Default maximum number of applications -->
		<tr class="tablegreenrow2 simple">
			<td valign="top"  class="formfieldtitle tabletext"><?= $eEnrollment['default_max_act']?></td>
			<td><?=$maxSelection?></td>
		</tr>
		
		<!- Default maximum number of enrolled activities -->
		<tr class="tablegreenrow2 simple">
			<td valign="top"  class="formfieldtitle tabletext"><?= $eEnrollment['default_enroll_max_act']?></td>
			<td><?=$enrollMaxSelection?></td>
		</tr>
		
		<!--tr class="tablegreenrow2">
			<td valign="top"  class="formfieldtitle tabletext"><?=$Lang['eEnrolment']['ApplicationQuota']?></td>
			<td><?=$htmlAry['applicationQuotaTable']?></td>
		</tr-->
		
		<tr><td>&nbsp;</td></tr>
		
		<!- Active Member Percentage -->
		<? if($plugin['eEnrollmentLite']) 
				$active_member_per_short = "<font color='cccccc'><i>". $eEnrollment['active_member_per_short'] ."</i></font>";
			else
				$active_member_per_short = $eEnrollment['active_member_per_short'];
		?>
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$active_member_per_short?></td>
			<td valign="top" class="tabletext"><input class="textboxnum" type="text" name="activeMemberPer" maxlength="6" value="<?=$activeMemberPer?>"> %</td>
		</tr>
		
		<!- Disable changing of applicants' status after payment -->
		<? if($plugin['eEnrollmentLite']) 
				$disable_status = "<font color='cccccc'><i>". $eEnrollment['disable_status'] ."</i></font>";
			else
				$disable_status = $eEnrollment['disable_status'];
		?>
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$disable_status?></td>
			<td valign="top"><input type="checkbox" name="disableStatus" value="1" <?=$chkDisableStauts?>></td>
		</tr>
		
		<!- Enrol one activity only if Time Crash Occurs -->
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$eEnrollment['one_act_when_time_crash']?></td>
			<td valign="top"><input type="checkbox" name="oneEnrol" value="1" <?=$chkOneEnrol?>></td>
		</tr>
		
		<!- Disable Checking Of Number Of Club a Student Want To Join -->
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$Lang['eEnrolment']['DisableCheckingOfNumberOfActivityStudentWantToJoin']?></td>
			<td valign="top"><input type="checkbox" name="disableCheckingNoOfActivityStuWantToJoin" value="1" <?=$chkDisableCheckingNoOfActivityStuWantToJoin?> /></td>
		</tr>
		
		<!- Send email at once -->
		<? if($plugin['eEnrollmentLite']) 
				$SendEmailAtOnce = "<font color='cccccc'><i>". $Lang['eEnrolment']['SendEmailAtOnce'] ."</i></font>";
			else
				$SendEmailAtOnce = $Lang['eEnrolment']['SendEmailAtOnce'];
		?>
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$SendEmailAtOnce?></td>
			<td valign="top"><input type="checkbox" name="onceEmail" value="1" <?=$chkOnceEmail?> /></td>
		</tr>
		
		<!- Allow Club PIC create non-club activity -->
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$Lang['eEnrolment']['AllowClubPICCrateNAActviity']?></td>
			<td valign="top"><input type="checkbox" name="clubPICcreateNAAct" value="1" <?=$chkclubPICcreateNAAct?> /></td>
		</tr>
		
		<!- Disable Add/Update function-->
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$Lang['eEnrolment']['disableUpdate']?></td>
			<td valign="top"><input type="checkbox" name="disableUpdate" value="1" <?=$chkDisableUpdate?> /></td>
		</tr>
		
		<!- Disable Helper Modification Right's Limitation function-->
		<tr class="advanced">
			<td valign="top"  class="formfieldtitle tabletext"><?=$Lang['eEnrolment']['disableHelperModificationRightLimitation']?></td>
			<td valign="top"><input type="checkbox" name="Event_DisableHelperModificationRightLimitation" value="1" <?=$chkDisableHelperModificationRightLimitation?> /></td>
		</tr>
		
		<?php if($plugin['eClassTeacherApp']){ ?>
			<!- Allow send push message to event PIC when check clash -->
			<tr class="advanced">
				<td class="field_title"><?php echo $Lang['eEnrolment']['lookupClashestoSendPushMessagetoeventPIC'] ; ?></td>
				<td><input type="checkbox" name="lookupClashestoSendPushMessagetoEventPIC" value="1" value="1" <?=$chkDisablelookupClashestoSendPushMessagetoEventPIC?> ></td>
			</tr>
		<?php }?>
		
		<?php if($libenroll->enableActivityDefaultAttendaceSelectionControl()){ ?>
			<!- Enable Attendance Default Selection function-->
			<tr class="advanced">
				<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?=$i_StudentAttendance_default_attend_status?></td>
				<td><?=$htmlAry['attendanceDefaultSelection']?></td>
			</tr>
		<?}?>
		
		
		<!- Clear all records -->
		<tr>
			<td valign="top"  class="formfieldtitle tabletext"><span class="tabletextremark"><?php echo $i_ClubsEnrollment_ClearRecord; ?></span></td>
			<td valign="top"><input type="checkbox" name="clearRecord1" value="1" onClick="showHideAlert();" disabled><?=toolBarSpacer()?><span class="tabletextremark"><?=$Lang['eEnrolment']['Settings']['ClearDataIsMoved']?></span><span id="removeAlert" style="visibility:hidden"><font color="red"><?=$eEnrollment['alert_all_enrolment_records_deleted_activity']?></font></span></td>
		</tr>
	</table>
	
	
	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td align="center" colspan="6">
			<div style="padding-top: 5px">
				<?= $linterface->GET_ACTION_BTN($button_save, "button", "checkform(document.form1)")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
			</div>
		</td></tr>
	</table>
</form>

<SCRIPT LANGUAGE=Javascript>
changemode(document.form1,<?=$basicmode?>);
</SCRIPT>
<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>