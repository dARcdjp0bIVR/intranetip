<?php
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");

intranet_auth();
intranet_opendb();


$LibUser = new libuser($UserID);
$linterface = new interface_html();

# Check access right
if (!$plugin['eEnrollment'])
{
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();	
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))){
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageTranserOLE";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], "", 1);

$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step1'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step2'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step3'], 1);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step4'], 0);

$linterface->LAYOUT_START();

// $FormChkArr[$GroupID][$YearID] = 1;
$FormChkArr = $_REQUEST['FormChkArr'];

for($i=0;$i<sizeof($ID);$i++) { 
	//${"organization_".$ID[$i]} = intranet_htmlspecialchars(trim(${"organization_".$ID[$i]}));
	//${"details_".$ID[$i]} = intranet_htmlspecialchars(trim(${"details_".$ID[$i]}));
	//${"SchoolRemarks_".$ID[$i]} = intranet_htmlspecialchars(trim(${"SchoolRemarks_".$ID[$i]}));
}

$ID_Str = implode(",",$ID);
$RecordTypeStr = $RecordType=="club" ? $eEnrollment['Club_Records'] : $eEnrollment['Activity_Records'];
$TypeSelect = $eEnrollment['Record_Type'].": " . $RecordTypeStr ;


# get all data of club/activity
if($RecordType=="club")
{
	$TitleField = Get_Lang_Selection('b.TitleChinese', 'b.Title');
	
	# get title and IDs of all clubs first
	$sql = "SELECT 
				$TitleField as title, 
				a.EnrolGroupID as enrolment_ID, 
				a.OLE_ProgramID as OLE_ID,
				left(min(c.ActivityDateStart),10) as Startdate,
				left(max(c.ActivityDateEnd),10) as Enddate,
				if (MONTH(left(min(c.ActivityDateStart),10)) > 8,
					CONCAT(YEAR(left(min(c.ActivityDateStart),10)),'-',YEAR(left(min(c.ActivityDateStart),10))+1),
					CONCAT(YEAR(left(min(c.ActivityDateStart),10))-1,'-',YEAR(left(min(c.ActivityDateStart),10)))) AS Period_Enrolment
			FROM 
				INTRANET_ENROL_GROUPINFO as a 
				LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
				LEFT OUTER JOIN INTRANET_ENROL_GROUP_DATE as c ON c.EnrolGroupID  = a.EnrolGroupID
			WHERE
				a.EnrolGroupID in ($ID_Str)
				AND
				(c.RecordStatus IS NULL OR c.RecordStatus = 1)
			GROUP BY
				a.EnrolGroupID
			ORDER BY
				$TitleField
			";
	$groupInfoArr = $libenroll->returnArray($sql, 6);
}
else
{	
	$sql ="
		SELECT
			a.EnrolEventID as enrolment_ID,
			a.EventTitle as title,
			a.OLE_ProgramID as OLE_ID,
			left(min(b.ActivityDateStart),10) as Startdate,
			left(max(b.ActivityDateEnd),10) as Enddate,
			if (MONTH(left(min(b.ActivityDateStart),10)) > 8,
				CONCAT(YEAR(left(min(b.ActivityDateStart),10)),'-',YEAR(left(min(b.ActivityDateStart),10))+1),
				CONCAT(YEAR(left(min(b.ActivityDateStart),10))-1,'-',YEAR(left(min(b.ActivityDateStart),10)))) AS Period_Enrolment
		FROM
			INTRANET_ENROL_EVENTINFO as a
			LEFT JOIN INTRANET_ENROL_EVENT_DATE as b ON a.EnrolEventID = b.EnrolEventID
		WHERE
			a.EnrolEventID in ($ID_Str)	
			AND
			(b.RecordStatus IS NULL OR b.RecordStatus = 1)
		GROUP BY
			a.EnrolEventID
		ORDER BY
			a.EventTitle
	";
	$groupInfoArr = $libenroll->returnArray($sql, 6);
}

# check if OLE record existing and count number of student record in OLE for each club/activity
for ($i=0; $i<sizeof($groupInfoArr); $i++)
{
	$tempGroupID = $groupInfoArr[$i]['enrolment_ID'];
	$tempOLE_ID = $groupInfoArr[$i]['OLE_ID'];
	$tempTitle = $groupInfoArr[$i]['title'];
	$Startdate = $groupInfoArr[$i]['Startdate'];
	$Enddate = $groupInfoArr[$i]['Enddate'];
	$temp_enrol_period = $groupInfoArr[$i]['Period_Enrolment'];

	# check if OLE Program record exist
//	$sql = "SELECT
//				ProgramID,
//				if (MONTH(StartDate) > 8,
//					CONCAT(YEAR(StartDate),'-',YEAR(StartDate)+1),
//					CONCAT(YEAR(StartDate)-1,'-',YEAR(StartDate))) as aaa
//			FROM
//				{$eclass_db}.OLE_PROGRAM
//			WHERE
//				Title = '".$libenroll->Get_Safe_Sql_Query($tempTitle)."'
//				AND
//				StartDate = '$Startdate'
//				AND
//				EndDate = '$Enddate'
//			";
//	$OLEProgramID = $libenroll->returnVector($sql);
	$OLEProgramID = '';
	if ($tempOLE_ID != '') {
		$OLE_PROGRAM = $eclass_db.'.OLE_PROGRAM';
		$sql = "Select ProgramID From $OLE_PROGRAM Where ProgramID = '".$tempOLE_ID."'";
		$tempProgramArr = $libenroll->returnVector($sql);
		
		if (count($tempProgramArr) > 0) {
			$OLEProgramID = $tempOLE_ID;
		}
	}
	
	if ($OLEProgramID != '')
	{
		$OLE_RecordArr[$tempGroupID]['Program'] = 1;
		$tempOLE_ProgramID = $OLEProgramID;
		
		$thisFormIDArr = array_keys($FormChkArr[$tempGroupID]);
		
		# get the number of OLE Student Records
		$sql = "SELECT 
					COUNT(os.RecordID)
				FROM 
					{$eclass_db}.OLE_STUDENT as os
					Left Outer Join
					YEAR_CLASS_USER as ycu
					On (os.UserID = ycu.UserID)
					Left Outer Join
					YEAR_CLASS as yc
					On (ycu.YearClassID = yc.YearClassID)
				WHERE
					os.ProgramID = '$tempOLE_ProgramID'
					And
					yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
					And
					yc.YearID In (".implode(',', $thisFormIDArr).")
				GROUP BY
					os.ProgramID
				";
		$result = $libenroll->returnVector($sql);
		
		
		# build associative array of key=GroupID
		if ($result != NULL)
		{
			$OLE_RecordArr[$tempGroupID]['Student'] = $result[0];
		}		
	}
}	


# "Replace All" buttons
// for program records
$programReplaceAllBtn = "";
$programReplaceAllBtn .= "<input type='checkbox' name='ProgramReplaceAll' id='ProgramReplaceAll' onClick=\"javascript:ProgramReplaceChangeState()\" checked>";
$programReplaceAllBtn .= "<label id='labal_ProgramReplaceAll' for='ProgramReplaceAll'>".$eEnrollment['replace_all']." </label>";
$programReplaceAllBtn .= "</input>";

// for student records
$studentReplaceAllBtn = "";
$studentReplaceAllBtn .= "<input type='checkbox' name='StudentReplaceAll' id='StudentReplaceAll' onClick=\"javascript:StudentReplaceChangeState()\" checked>";
$studentReplaceAllBtn .= "<label id='labal_StudentReplaceAll' for='StudentReplaceAll'>".$eEnrollment['replace_all']." </label>";
$studentReplaceAllBtn .= "</input>";

# Construct table
//construct title
$display = "<table class=\"tabletop\" align=\"center\" width=\"90%\" cellpadding=\"5\" cellspacing=\"0\">";
	$display .= "<tr>";				
		$display .= "	<td width=\"1\" class=\"tabletop tabletopnolink\">#</td>
						<td class=\"tabletop tabletopnolink\">".$ec_iPortfolio['title']."</td>
				  		<td align=\"center\" class=\"tabletop tabletopnolink\">".$eEnrollment['No_of_Student']."</td>
		          		<td class=\"tabletop tabletopnolink\">".$eEnrollment['club_act_in_OLE']."</td>
				  		<td class=\"tabletop tabletopnolink\">".$eEnrollment['student_OLE_records']."</td>";
	$display .= "</tr>";
	
	$display .= "<tr>";				
		$display .= "	<td width=\"1\" class=\"tablebottom tablebottomnolink\">&nbsp</td>
						<td class=\"tablebottom tablebottomnolink\">&nbsp</td>
				  		<td align=\"center\" class=\"tablebottom tablebottomnolink\">&nbsp</td>
		          		<td class=\"tablebottom tablebottomnolink\">".$programReplaceAllBtn."</td>
				  		<td class=\"tablebottom tablebottomnolink\">".$studentReplaceAllBtn."</td>";
	$display .= "</tr>";
	
	$total_col = 5;
   	$NumRow = sizeof($groupInfoArr);
    
    //construct content
    $display .= ($NumRow==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
	    
    $js_programChkBox = array();
    $js_studentChkBox = array();
	for ($i=0; $i<$NumRow; $i++)
	{
		$tr_css = ($i % 2 == 1) ? "tablerow2" : "tablerow1";
		$rowNumber = $i+1;
		
		$tempGroupID = $groupInfoArr[$i]['enrolment_ID'];
		
		# title
		$tempTitle = $groupInfoArr[$i]['title'];
		
		# No. of Student
		# settings
		$ParData['enrolmentID'] = $tempGroupID;
		$ParData['studentSetting'] = ${"studentSetting_".$tempGroupID}; 	
		$ParData['zeroHour'] = ${"zeroHour_".$tempGroupID};			
		$ParData['hourSetting'] = ${"hours_setting_".$tempGroupID};		
		$ParData['leastAttendance'] = ${"leastAttendance_".$tempGroupID};	
		$ParData['manualHours'] = ${"manualHours_".$tempGroupID};
		
		$FormIDArr = array_keys((array)$FormChkArr[$tempGroupID]);
		$OLE_StudentHoursArr = $libenroll->GET_STUDENT_OLE_HOURS($ParData, $RecordType, $returnAssociativeArr=0, $FormIDArr);
		$numOfSatisfiedStudent = sizeof($OLE_StudentHoursArr);
		
		$NumStudentDisplay = '';
		if ($numOfSatisfiedStudent == 0)
		{
			$NumStudentDisplay = 0;
		}
		else
		{
			$FormIDList = implode(',', $FormIDArr);
			$NumStudentDisplay .= "<a href=\"javascript:void(0)\" class=\"tablelink\" style=\"background-color:#A6A6A6\" onClick=\"javascript:ViewDetails($tempGroupID, '$FormIDList')\">";
			$NumStudentDisplay .= $numOfSatisfiedStudent;
			$NumStudentDisplay .= "</a>";
		}
		
		# replace program column in OLE
		$ProgramDisplay = "";
		if ($OLE_RecordArr[$tempGroupID]['Program'] != NULL)
		{
			$ProgramDisplay .= $eEnrollment['Record_exists'];
			$ProgramDisplay .= "<br />";
			$ProgramDisplay .= "<input type=\"checkbox\" name=\"ProgramReplace_".$tempGroupID."\" id=\"ProgramReplace_".$tempGroupID."\" value=\"overwrite\" checked>";
			$ProgramDisplay .= "<label id=\"labal_ProgramReplace_".$tempGroupID."\" for=\"ProgramReplace_".$tempGroupID."\">".$eEnrollment['replace']." </label>";
			
			$js_programChkBox[] = "ProgramReplace_".$tempGroupID;
		}
		else
		{
			$ProgramDisplay .= "--";
		}
		
		# replace student OLE records column
		$StudentDisplay = "";
		if ($OLE_RecordArr[$tempGroupID]['Student'] != NULL)
		{
			$curLabel = ($numOfSatisfiedStudent == 0)? $button_remove : $eEnrollment['replace'];
			$StudentDisplay .= $OLE_RecordArr[$tempGroupID]['Student']." ".$eEnrollment['record_exists'];
			$StudentDisplay .= "<br />";
			$StudentDisplay .= "<input type=\"checkbox\" name=\"StudentReplace_".$tempGroupID."\" id=\"StudentReplace_".$tempGroupID."\" value=\"overwrite\" checked>";
			$StudentDisplay .= "<label id=\"StudentReplace_".$tempGroupID."\" for=\"StudentReplace_".$tempGroupID."\">".$curLabel." </label>";
			
			$js_studentChkBox[] = "StudentReplace_".$tempGroupID;			
		}
		else
		{
			$StudentDisplay .= "--";
		}
		 	
		# output the data		
		$display .= "<tr class=\"$tr_css\">";
		$display .= "<td class=\"tabletext\" valign=\"top\">".$rowNumber."</td>";
		$display .= "<td class=\"tabletext\" valign=\"top\">".$tempTitle."</td>";
		$display .= "<td class=\"tabletext\" valign=\"top\" align=\"center\">".$NumStudentDisplay."</td>";
		$display .= "<td class=\"tabletext\" valign=\"top\">".$ProgramDisplay."</td>";
		$display .= "<td class=\"tabletext\" valign=\"top\">".$StudentDisplay."</td>";
		
		$display .= "</tr>";
	}
	
$display .= "</table>";
?>

<script language="javascript">
function ProgramReplaceChangeState()
{
	var replaceAllChkBox = document.getElementById("ProgramReplaceAll");
	var state = replaceAllChkBox.checked;
	
	<? for ($iPHP=0; $iPHP<sizeof($js_programChkBox); $iPHP++) { ?>
		var targetChkBoxID = '<?=$js_programChkBox[$iPHP]?>';
		var targetChkBox = document.getElementById(targetChkBoxID);	
		
		targetChkBox.checked = state;	
	<? } ?>
}

function StudentReplaceChangeState()
{
	var replaceAllChkBox = document.getElementById("StudentReplaceAll");
	var state = replaceAllChkBox.checked;
	
	<? for ($iPHP=0; $iPHP<sizeof($js_studentChkBox); $iPHP++) { ?>
		var targetChkBoxID = '<?=$js_studentChkBox[$iPHP]?>';
		var targetChkBox = document.getElementById(targetChkBoxID);	
		
		targetChkBox.checked = state;	
	<? } ?>
}

function ViewDetails(targetID, jsFormIDList)
{
	obj = document.form1;
	
	obj.target = "blank";
	obj.method = "POST";
	obj.action = "student_list_popup.php?FromStep=3&ParID=" + targetID + "&FormIDList=" + jsFormIDList;
	obj.submit();
}

function SubmitForm(targetID)
{
	obj = document.form1;
	
	obj.target = "";
	obj.method = "POST";
	obj.action = "transfer_ole2_update.php";
	obj.submit();
}

function SubmitBack(targetID)
{
	obj = document.form1;
	
	obj.target = "";
	obj.method = "POST";
	obj.action = "transfer_ole2.php";
	obj.submit();
}

</script>

<br />
<form name="form1" method="GET" action="transfer_ole2_update.php">
	<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td>
				<?= $linterface->GET_STEPS($STEPS_OBJ) ?>
				<br />
			</td>
		</tr>
		
		<tr>
			<td>
				<?=$display?>
			</td>
		</tr>
	</table>
	
	<br />
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td height="1" class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
		</tr>
		<tr><td>&nbsp</td></tr>
		<tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript:SubmitForm();") ?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "") ?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:SubmitBack();") ?>&nbsp;
			</td>
		</tr>
	</table>
	

<? for($i=0;$i<sizeof($ID);$i++) { 
	$curCategory = ${"category_".$ID[$i]};
	$curOrganization = intranet_htmlspecialchars(trim(${"organization_".$ID[$i]}));
	$curDetails = intranet_htmlspecialchars(trim(${"details_".$ID[$i]}));
	$curSchoolRemarks = intranet_htmlspecialchars(trim(${"SchoolRemarks_".$ID[$i]}));
				
	$curHourSetting = ${"hours_setting_".$ID[$i]};
	$curLeastAttendance = ${"leastAttendance_".$ID[$i]};
	$curManualHours = ${"manualHours_".$ID[$i]};
	$curZeroHour = ${"zeroHour_".$ID[$i]};
	$curStudentSetting = ${"studentSetting_".$ID[$i]};
		
	?>
	<input type="hidden" name="ID[]" value="<?=$ID[$i]?>" />
	<input type="hidden" name="category_<?=$ID[$i]?>" value="<?=$curCategory?>" />
	<input type="hidden" name="organization_<?=$ID[$i]?>" value="<?=$curOrganization?>" />
	<input type="hidden" name="details_<?=$ID[$i]?>" value="<?=$curDetails?>" />
	<input type="hidden" name="SchoolRemarks_<?=$ID[$i]?>" value="<?=$curSchoolRemarks?>" />
	<input type="hidden" name="hours_setting_<?=$ID[$i]?>" value="<?=$curHourSetting?>" />
	<input type="hidden" name="leastAttendance_<?=$ID[$i]?>" value="<?=$curLeastAttendance?>" />
	<input type="hidden" name="manualHours_<?=$ID[$i]?>" value="<?=$curManualHours?>" />
	<input type="hidden" name="zeroHour_<?=$ID[$i]?>" value="<?=$curZeroHour?>" />
	<input type="hidden" name="studentSetting_<?=$ID[$i]?>" value="<?=$curStudentSetting?>" />
	
	<? 
	$eleArr = ${"ele_".$ID[$i]};
	$numELE = sizeof($eleArr);
	for($j=0; $j<$numELE; $j++) {
		$curValue = $eleArr[$j];
	?>
		<input type="hidden" name="ele_<?=$ID[$i]?>[]" value="<?=$curValue?>" />
	<? } ?>
<? } ?>
<input type="hidden" name="RecordType" value="<?=$RecordType?>" />

<input type="hidden" name="category_global" value="<?=$category_global?>" />
<input type="hidden" name="hours_setting_global" value="<?=$hours_setting_global?>" />
<input type="hidden" name="leastAttendance_global" value="<?=$leastAttendance_global?>" />
<input type="hidden" name="manualHours_global" value="<?=$manualHours_global?>" />
<input type="hidden" name="zeroHour_global" value="<?=$zeroHour_global?>" />
<input type="hidden" name="studentSetting_global" value="<?=$studentSetting_global?>" />

<? 
$eleArr = $ele_global;
$numELE = sizeof($eleArr);
for($j=0; $j<$numELE; $j++) {
	$curValue = $eleArr[$j];
?>
	<input type="hidden" name="ele_global[]" value="<?=$curValue?>" />
<? } ?>

<input type="hidden" name="FormChkArr" value="<?=rawurlencode(serialize($FormChkArr));?>">
<input type="hidden" name="TargetFormChkAllArr" value="<?=rawurlencode(serialize($TargetFormChkAllArr));?>">

</form>
</br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>