<?php
## Using By : 
########################################
##	Modification Log:
##
##  2018-08-06 Cameron
##  - hide items for HKPF
##
##	2016-07-22 Cara
##	- Add InputBy, InputDate, Modified by, ModifiedDate 
##
##	2011-10-31 YatWoon
##	- Add Group Code for edit with flag $sys_custom['eEnrolment']['TempClubCode'] (until the group code as general deployment) # for client Heung To Middle School at the moment
##
##	2010-12-14 YatWoon
##	- IP25 UI standard
##	- add club Chinese name
##
##	20100114 Max (201001141638)
##	- Change the wording from "Rights to post public Announcements/Events/Surveys" to "Can use public Announcements/Events"
##	- Remove "No display on Organization Page" row
##	- Remove "Allowing using all group tools" row
##	- Remove "Rights to use club tools" row
########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libalbum.php");
include_once($PATH_WRT_ROOT."includes/liborganization.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['gro_setting'], "", 1);
        $Step4 = $Lang['eEnrolment']['CreatedAndModifiedRecords'];

        $linterface->LAYOUT_START();

        $PAGE_NAVIGATION[] = array($button_edit." ".$eEnrollmentMenu['club'], "");
        
        $EnrolGroupID = $_GET['EnrolGroupID'];
		$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
		
		$li = new libgroup($GroupID);

		
		//$lgc = new libgroupcategory($li->RecordType);
		# Force update Group Category 5 only (ECA �ҥ~����), 20080903 Yat Woon
		$lgc = new libgroupcategory(5);
		$lorg = new liborganization();
		$la = new libalbum();
		$checked = ($li->AnnounceAllowed==1? "CHECKED":"");
		$allToolsChecked = ($li->isAccessAllTools()? "CHECKED":"");
		$availableTools = $li->getSelectCurrentAvailableFunctions();
		$hideChked = ($lorg->isGroupHidden($GroupID)? "CHECKED":"");
		
		
		## Get Club Main Info
		$GroupInfoArr = $libenroll->Get_Group_Info_By_GroupID($GroupID);
		
		$groupClubInfoAry = $libenroll->Get_Club_Info_By_GroupID($GroupID);
		
		$DateInput = $groupClubInfoAry[0]['DateInput'];
		$InputBy = $groupClubInfoAry[0]['InputBy'];
		if ($InputBy == Null){
			$InputBy = $Lang['General']['EmptySymbol'];
		} else{
			$LibUser = new libuser($InputBy);
			$CreatorEnglishName = $LibUser->EnglishName;
			$CreatorChineseName = $LibUser->ChineseName;
			$InputBy = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
		}
		
		if ($DateInput == Null){
			$DateInput = $Lang['General']['EmptySymbol'];
		}
		
		$ModifiedBy = $groupClubInfoAry[0]['ModifiedBy'];
		$DateModified = $groupClubInfoAry[0]['DateModified'];
		if ($ModifiedBy == Null){
			$ModifiedBy = $Lang['General']['EmptySymbol'];
		} else{
			$LibUser = new libuser($ModifiedBy);
			$CreatorEnglishName = $LibUser->EnglishName;
			$CreatorChineseName = $LibUser->ChineseName;
			$ModifiedBy = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
		}
		if ($DateModified == Null){
			$DateModified = $Lang['General']['EmptySymbol'];
		}
		$ClubType = $GroupInfoArr['ClubType'];
		$ApplyOnceOnly = $GroupInfoArr['ApplyOnceOnly'];
		$FirstSemEnrolOnly = $GroupInfoArr['FirstSemEnrolOnly'];
		
		if ($ClubType == 'Y')
		{
			$yearChecked = 1;
			$yearTrDefaultDisplay = '';
			$semesterChecked = 0;
			$semesterTrDefaultDisplay = 'style="display:none"';
			
			$chkApplyOnceOnly = '';
			$chkFirstSemEnrolOnly = ($FirstSemEnrolOnly == 1)? 'checked' : '';
		}
		else if ($ClubType == 'S')
		{
			$yearChecked = 0;
			$yearTrDefaultDisplay = 'style="display:none"';
			$semesterChecked = 1;
			$semesterTrDefaultDisplay = '';
			
			$chkApplyOnceOnly = ($ApplyOnceOnly == 1)? 'checked' : '';
			$chkFirstSemEnrolOnly = '';
		}
		
		## Get Clubs info of the Group
		$ClubSemArr = $libenroll->Get_Group_Involoved_Semester($GroupID);
		
		### Semester Checkboxes
		$sem_ary = getSemesters($li->AcademicYearID);
		
		$SemCheckedTable = "<table border='0' cellspacing='0' cellpadding='0' class='inside_form_table'>";
		$SemCheckedTable .= "<tr><td class='tabletext'><input type='checkbox' name='Sem_all' id='Sem_all' onClick=(this.checked)?setChecked(1,this.form,'Sem[]'):setChecked(0,this.form,'Sem[]')> <label for='Sem_all'>". $Lang['Btn']['SelectAll'] ."</label></td></tr>";
		$si = 0;
		foreach($sem_ary as $SemID => $SemName)
		{
			$thisChecked = (in_array($SemID, $ClubSemArr))? "checked" : "";
			
			$SemCheckedTable .= !($si%4) ? "<tr>" : "";
			$SemCheckedTable .= "<td class='tabletext'><input type='checkbox' name='Sem[]' value='{$SemID}' id='Sem_".$si."' onClick='unset_checkall(this, \"Sem_all\");' ".$thisChecked."> <label for='Sem_".$si."'>".$SemName."</label></td>";
			$si++;
			$SemCheckedTable .= !($si%4) ? "</tr>" : "";
		}
		$SemCheckedTable .= "</table>";
?>

<script language="javascript">
function checkform(obj)
{
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['ClubNameEn']; ?>.", "div_Title_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Title";
	}
	
	if(!check_text_30(obj.TitleChinese, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['ClubNameCh']; ?>.", "div_TitleChinese_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "TitleChinese";
	}

<?php if (!$sys_custom['project']['HKPF']):?>	
	if(!check_positive_int_30(obj.Quota, "<?php echo $i_GroupQuotaIsInt; ?>.", 0,0, "div_Quota_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Quota";
	}
	
	if (document.getElementById('SemesterBaseRadio').checked == true)
	{
		if (check_checkbox(document.form1, 'Sem[]') == false)
		{
			document.getElementById('div_Semester_err_msg').innerHTML = '<font color="red"><?=$eEnrollment['js_select_semester']?></font>';
			error_no++;
			focus_field = "Sem_all";
			return false;
		}
	} 
<?php endif;?>

	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
<?php if ($sys_custom['project']['HKPF']):?>
		var jsYearBaseChecked = true;
<?php else: ?>
		var jsYearBaseChecked = document.getElementById('YearBaseRadio').checked;
<?php endif;?>		
		if (jsYearBaseChecked == true)
			jsClubType = 'Y';
		else
			jsClubType = 'S';
			
		if ("<?=$ClubType?>" != jsClubType)
		{
		 if (!confirm("<?=$eEnrollment['ChangeClubTypeWarning']?>"))
				return false;  
		}
	}
}

function allToolsChecked(obj)
{
         var val;
         var i=0;
         len=obj.elements.length;
         if (obj.alltools.checked)
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=true;
                      obj.elements[i].checked=true;
                  }
             }
         }
         else
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=false;
                  }
             }
         }
}

function unset_checkall(obj, objAll_ID)
{
	if (obj.checked == false)
		document.getElementById(objAll_ID).checked = false;
}

function jsChangeClubType(ClubType)
{
	if (ClubType == 'Year')
	{
		//document.getElementById('SemesterBase_SemesterSelection_Tr').style.display = "none";
		//document.getElementById('SemesterBase_JoinOption_Tr').style.display = "none";
		//document.getElementById('YearBase_JoinOption_Tr').style.display = "inline";
		$('tr#SemesterBase_SemesterSelection_Tr').hide();
		$('tr#SemesterBase_JoinOption_Tr').hide();
		$('tr#YearBase_JoinOption_Tr').show();
	}
	else if (ClubType == 'Semester')
	{
		//document.getElementById('SemesterBase_SemesterSelection_Tr').style.display = "inline";
		//document.getElementById('SemesterBase_JoinOption_Tr').style.display = "inline";
		//document.getElementById('YearBase_JoinOption_Tr').style.display = "none";
		$('tr#SemesterBase_SemesterSelection_Tr').show();
		$('tr#SemesterBase_JoinOption_Tr').show();
		$('tr#YearBase_JoinOption_Tr').hide();
	}
}

function click_reset()
{
<?php if (!$sys_custom['project']['HKPF']):?>	
	document.getElementById('SemesterBase_SemesterSelection_Tr').style.display = "none";
<?php endif;?>	
	reset_innerHtml();
	document.form1.reset();
}

function reset_innerHtml()
{
 	document.getElementById('div_Title_err_msg').innerHTML = "";
 	document.getElementById('div_TitleChinese_err_msg').innerHTML = "";
<?php if (!$sys_custom['project']['HKPF']):?> 	
 	document.getElementById('div_Quota_err_msg').innerHTML = "";
 	document.getElementById('div_Semester_err_msg').innerHTML = "";
<?php endif;?> 	
}

</script>

<form name="form1" action="group_edit_update.php" method="post" onSubmit="return checkform(this);">

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<div class="form_table">
<table class="form_table_v30">
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameEn'] ?> </td>
	<td><input class="textboxtext" type="text" name="Title" size="30" maxlength="255" value="<?php echo $li->Title; ?>"><br><span id='div_Title_err_msg'></span></td>
</tr>
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameCh'] ?> </td>
	<td><input class="textboxtext" type="text" name="TitleChinese" size="30" maxlength="255" value="<?php echo $li->TitleChinese; ?>"><br><span id='div_TitleChinese_err_msg'></span></td>
</tr>
<?/* if($sys_custom['eEnrolment']['TempClubCode']) {*/?>
<tr>
	<td class="field_title"><?php echo $Lang['eEnrolment']['ClubCode']?> </td>
	<td><input class="textboxnum" type="text" name="ClubCode" size="50" maxlength="20" value="<?php echo $li->GroupCode; ?>"></td>
</tr>
<?/* } */?>
<tr>
	<td class="field_title"><?php echo $i_GroupDescription; ?></td>
	<td><?= $linterface->GET_TEXTAREA("Description", $li->Description)?></td>
</tr>
<?php if (!$sys_custom['project']['HKPF']):?>
    <tr>
    	<td class="field_title"><?php echo $i_GroupQuota; ?></td>
    	<td><input class="textboxnum" type="text" name="Quota" size="5" maxlength="5" value="<?=$li->StorageQuota?>"><br><span id='div_Quota_err_msg'></span></td>
    </tr>
    <? if ($plugin['album']) { ?>
    <tr>
    	<td class="field_title"><?php echo $i_AlbumQuota; ?></td>
    	<td><input class="textboxnum" type="text" name="AlbumQuota" size="5" maxlength="5" value="<?=$la->returnStorageQuota($GroupID)?>"></td>
    </tr>
    <tr>
    	<td class="field_title"><?php echo $i_AlbumAccessType; ?></td>
    	<td><?php echo $la->returnSelectAccessType("name=AllowedAccessType",true,0,$la->returnAlowedAccessType($GroupID)); ?></td>
    </tr>
    <? } ?>
    <? if ($special_announce_public_allowed) { ?>
    <tr>
    	<td class="field_title"><?php echo $Lang['Group']['CanUsePublicAnnouncementEvents']; ?></td>
    	<td><input type="checkbox" name="AnnounceAllowed" value="1" <?=$checked?>></td>
    </tr>
    <? } ?>
    <? # Force update Group Category 5 only (ECA �ҥ~����), 20080903 Yat Woon ?>
    <tr>
    	<td class="field_title"><?php echo $i_GroupRecordType; ?></td>
    	<td><?php echo $lgc->CategoryName; ?></td>
    </tr>
    
    <!-- Club Type: Year Base or Semester Base -->
    <tr>
    	<td class="field_title"><?php echo $eEnrollment['ClubType'] ?></td>
    	<td>
    		<?= $linterface->Get_Radio_Button('YearBaseRadio', 'ClubType', $value='YearBase', $yearChecked, "", $eEnrollment['YearBased'], "jsChangeClubType('Year')"); ?>
    		<?= $linterface->Get_Radio_Button('SemesterBaseRadio', 'ClubType', $value='SemesterBase', $semesterChecked, "", $eEnrollment['SemesterBased'], "jsChangeClubType('Semester')"); ?>
    	</td>
    </tr>
    
    <tr id="YearBase_JoinOption_Tr" <?=$yearTrDefaultDisplay?>>
    	<td class="field_title"><?php echo $eEnrollment['only_allow_student_to_join_the_club_in_the_first_semester'] ?></td>
    	<td><input TYPE="checkbox" NAME="FirstSemEnrolOnly" VALUE="1" <?=$chkFirstSemEnrolOnly?>></td>
    </tr>
    
    <tr id="SemesterBase_SemesterSelection_Tr" <?=$semesterTrDefaultDisplay?>>
    	<td class="field_title"><?php echo $Lang['General']['Term'] ?></td>
    	<td><?= $SemCheckedTable ?><span id='div_Semester_err_msg'></span></td>
    </tr>
    
    <tr id="SemesterBase_JoinOption_Tr" <?=$semesterTrDefaultDisplay?>>
    	<td class="field_title"><?php echo $eEnrollment['enrol_same_club_once_only'] ?></td>
    	<td><input TYPE="checkbox" NAME="ApplyOnceOnly" VALUE="1" <?=$chkApplyOnceOnly?>></td>
    </tr>
<?php endif;?>

     <tr>
     
		<td colspan="2"><br><br><?= $linterface->GET_NAVIGATION2($Step4) ?><br/>
		</td>
	</tr>
     <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['CreateUserName']?></td>
		<td> 
		<?php echo $InputBy?>
     	</td>
     </tr>
      <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['CreateUserDate']?></td>
		<td> 
			<?php echo $DateInput?>
     	</td>
     </tr>    
          <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['LastModifiedBy']?></td>
		<td> 
		<?php echo $ModifiedBy?>
     	</td>
     </tr>
      <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['LastModified']?></td>
		<td> 
			<?php echo $DateModified?>
     	</td>
     </tr>   

</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_save, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "button","click_reset();")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='group.php?AcademicYearID=$AcademicYearID&Semester=$Semester'")?>
<p class="spacer"></p>
</div>

</div>


<input type="hidden" name="GroupID" value="<?=$GroupID?>">
<input type="hidden" name="Semester" value="<?=$Semester?>">
<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
<input type="hidden" name="RecordType" value='5'>
<?php if ($sys_custom['project']['HKPF']):?>
	<input type="hidden" name="ClubType" value='YearBase'>
<?php endif;?>
</form>
<?= $linterface->FOCUS_ON_LOAD("form1.Title") ?>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>

<?php
intranet_closedb();
?>