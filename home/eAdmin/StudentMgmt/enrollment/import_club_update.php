<?
//modifying by:

##########################
#	Date: 2017-09-15 Anna
#		added webSAMS code and type
#
#	Date: 2016-07-19 Omas
#		 replace split to explode for PHP5.4
#	Date: 2015/12/29 Omas
#		 add support to ole default setting import if have iPortfolio
#
#	Date: 2014/11/21 (Omas)
#		Insert Record by Change_Club_Event_GroupMapping() -  - 'A' for no limit (if A no record will add for house mapping)
#
##########################




$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

if(!isset($_SESSION['eEnrolmentImportdata']) || count($_SESSION['eEnrolmentImportdata'])==0) {
	header("Location: import_club_info.php?msg=ImportUnsuccess_NoRecord");
	exit;
}

$data = $_SESSION['eEnrolmentImportdata'];

$NoOfData = count($data);
$tempCount = 0;

for($i=0; $i<$NoOfData; $i++) {
	
	if($plugin['iPortfolio']) {
		list($groupCode, $clubNameEn, $clubNameCh, $announceAllowed, $content, $catName, $yearTerm, $targetForm, $targetHouse, $age, $gender, $fee, $quota, $minMember, $pic, $helper, $schedule, $intExt , $titleLang , $category , $ole_component ,$organization ,$details , $SchoolRemarks,$websamscode,$websamstype) = $data[$i];
	}
	else{
		list($groupCode, $clubNameEn, $clubNameCh, $announceAllowed, $content, $catName, $yearTerm, $targetForm, $targetHouse, $age, $gender, $fee, $quota, $minMember, $pic, $helper, $schedule,$websamscode,$websamstype) = $data[$i];	
	}
	
	$EnrolGroupIDArr = array();
	
	//echo "<br><br>############### ROW $i ###################<br><br>";
	//echo $content; continue;
	
	# Store record in INTRANET_GROUP
	$fieldname = "Title, TitleChinese, Description, RecordType, DateInput, DateModified, StorageQuota, FunctionAccess, AcademicYearID, GroupCode";
	$fieldvalue = "'".$libenroll->Get_Safe_Sql_Query($clubNameEn)."', '".$libenroll->Get_Safe_Sql_Query($clubNameCh)."', '".$libenroll->Get_Safe_Sql_Query($content)."', '5', now(), now(), '$quota', 'NULL', '$AcademicYearID', '$groupCode'";
	if ($announceAllowed == "Y")
	{
	    $fieldname .= ", AnnounceAllowed";
	    $fieldvalue .= ", '1'";
	}
	$sql = "INSERT INTO INTRANET_GROUP ($fieldname) VALUES ($fieldvalue)";
	
	$libenroll->db_db_query($sql);
	$GroupID = $libenroll->db_insert_id();
	//echo $sql." ($GroupID)<br>";
	
	### Insert Enrolment Club Main Info
	$ClubTypeForDB = (STRTOUPPER($yearTerm) == 'YEARBASE')? 'Y' : 'S';
	$ApplyOnceOnly = 0;			// For Semester-Based Club
	$FirstSemEnrolOnly = 0;		// For Year-Based Club
	
	$DataArr = array();
	$DataArr['GroupID'] = $GroupID;
	$DataArr['ClubType'] = $ClubTypeForDB;
	$DataArr['ApplyOnceOnly'] = $ApplyOnceOnly;
	$DataArr['FirstSemEnrolOnly'] = $FirstSemEnrolOnly;
	$SuccessArr['Insert_Group_Main_Info'] = $libenroll->Insert_Group_Main_Info($DataArr);
	//debug_pr($DataArr);
	
	### Create Enrolment Club Record
	if (STRTOUPPER($yearTerm) == 'YEARBASE')
	{
		$SuccessArr['Insert_Year_Based_Club'] = $libenroll->Insert_Year_Based_Club($GroupID);
		$EnrolGroupID =  mysql_insert_id();	
		$EnrolGroupIDArr[0] = $EnrolGroupID;
	}
	else
	{
		$tempSemesterArr = explode(',',$yearTerm);
		$numOfSemester = count($tempSemesterArr);
		$SemesterArr = array();
		$tempCount = 0;
		for ($a=0; $a<$numOfSemester; $a++)
		{
			$thisSemesterID = trim($tempSemesterArr[$a]);
			$sql = "SELECT YearTermID FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='$AcademicYearID' AND YearTermID='$thisSemesterID'";
			$_temp = $libenroll->returnVector($sql);
			
			if(count($_temp)>0) {
				$thisSemester = $_temp[0];
				$SuccessArr['Insert_Semester_Based_Club'] = $libenroll->Insert_Semester_Based_Club($GroupID, $thisSemester);
				$EnrolGroupIDArr[$thisSemester] = mysql_insert_id();
				$SemesterArr[] = $thisSemester;
				if($SuccessArr['Insert_Semester_Based_Club']) $tempCount++;
				//echo $EnrolGroupIDArr[$thisSemester].'/';
			}
			if($tempCount>10) exit;
			
		}
		//$EnrolGroupID = $EnrolGroupIDArr[$SemesterArr[0]]; // go to the first sem
	}
	
	$sql = "SELECT CategoryID, OleCategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$libenroll->Get_Safe_Sql_Query(trim($catName))."'";
	$catInfo = $libenroll->returnResultSet($sql);
	
	if(count($EnrolGroupIDArr) > 0) {
		foreach($EnrolGroupIDArr as $thisSemester => $EnrolGroupID) {
		
			//$EnrolGroupID = $EnrolGroupIDArr[$a];
		
			$GroupArr['GroupID'] = $GroupID;
			$GroupArr['EnrolGroupID'] = $EnrolGroupID;
			$GroupArr['Discription'] = $libenroll->Get_Safe_Sql_Query($content);
			if(STRTOUPPER($age)=="A") {
				$GroupArr['LowerAge'] = 0;
				$GroupArr['UpperAge'] = 0;
			} else {
				$ageArange = explode('-', $age);
				$GroupArr['LowerAge'] = ($age) ? $ageArange[0] : "";
				$GroupArr['UpperAge'] = ($age) ? $ageArange[1] : "";
			}
			$GroupArr['Gender'] = $gender;
			$GroupArr['GroupCategory'] = $catInfo[0]['CategoryID'];
			$GroupArr['Quota'] = $quota;
			$GroupArr['MinQuota'] = $minMember;
			
			if(STRTOUPPER($fee)=="TBC") {
				$GroupArr['PaymentAmount'] = 0;
				$GroupArr['isAmountTBC'] = 1; 
			} else {
				$GroupArr['PaymentAmount'] = $fee;
				$GroupArr['isAmountTBC'] = 0; 
			}
			$GroupArr['WebSAMSCode'] = $websamscode;
			$GroupArr['WebSAMSSTAType'] = $websamstype;
			
			//debug_pr($GroupArr);
			if ($libenroll->IS_GROUPINFO_EXISTS($EnrolGroupID)) {
				$EnrolGroupID = $libenroll->EDIT_GROUPINFO($GroupArr);
				//echo $EnrolGroupID.'/1<br>';
			} else {
				// Must have INRANET_ENROL_GROUPINFO for eEnrolment1.2
				//$EnrolGroupID = $libenroll->ADD_GROUPINFO($GroupArr);
				//echo $EnrolGroupID.'/<font color=red>2</font><br>';
			}
			
			# remove existing group class level
			$SuccessArr['Delete_Old_Form_Mapping'] = $libenroll->DEL_GROUPCLASSLEVEL($EnrolGroupID);
			
			# create group class level
			if($targetForm =="A"){
				$libenrolllass = new libclass();
				$ClassLvltempArr = $libenrolllass->getLevelArray();
				foreach($ClassLvltempArr as $temp){
					$ClasslvlArr['ClassLevelID'] = trim($temp[0]);
					$ClasslvlArr['EnrolGroupID'] = $EnrolGroupID;
					$libenroll->ADD_GROUPCLASSLEVEL($ClasslvlArr);
				}
				
			}
			else{
				$sql = "SELECT YearID, YearName FROM YEAR WHERE RecordStatus=1";
				$Form = $libenroll->returnArray($sql);
				for($a=0; $a<count($Form); $a++) {
					list($_id, $_name) = $Form[$a];
					$tempForm[$_name] = $_id;	
				}
				$Form = $tempForm;
				
				$ClasslvlArr['EnrolGroupID'] = $EnrolGroupID;
				
				if(count($targetForm)>0) {
					$newFormAry = explode(',', $targetForm);
				}
				if(STRTOUPPER($targetForm)=="ALL") {
	
				} else {
					for ($a = 0; $a < sizeof($newFormAry); $a++) {
						$YearName = trim($newFormAry[$a]);
						if(isset($Form[$YearName])) {
							$ClasslvlArr['ClassLevelID'] = $Form[$YearName];
							$libenroll->ADD_GROUPCLASSLEVEL($ClasslvlArr);
						}
					}
				}
			}			
			
			#Add house mapping
			if($targetHouse != "A"){
				$targetHouseAry = explode(',',$targetHouse);
				$targetHouseAry = array_map('trim', $targetHouseAry);
				$libenroll->Change_Club_Event_GroupMapping($enrolConfigAry['Club'], $EnrolGroupID, $targetHouseAry);
			}
			
			$sql = "SELECT UserID FROM INTRANET_ENROL_GROUPSTAFF WHERE EnrolGroupID = '".$EnrolGroupID."'";
			$oldStaffArr = $libenroll->returnArray($sql,1);
			
			# remove group staff (PIC / Helper)
			$libenroll->DEL_GROUPSTAFF($EnrolGroupID);
			
			# store PIC
			if(trim($pic)!="") {
				$picAry = explode(',',$pic);
				$PICArr['EnrolGroupID'] = $EnrolGroupID;
				$PICArr['StaffType'] = "PIC";
				for ($a = 0; $a < sizeof($picAry); $a++) {
					$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".trim($picAry[$a])."'";
					$userid = $libenroll->returnVector($sql);
					$PICArr['UserID'] = $userid[0];
					$libenroll->ADD_GROUPSTAFF($PICArr);
					
					//check if the staff is removed (for deletion later)
					for ($j = 0; $j < sizeof($oldStaffArr); $j++){
						if ($PICArr['UserID'] == $oldStaffArr[$j]['UserID']){
							$oldStaffArr['notRemoved'][$j][0] = true;
						}
					}
						
				}
			}
			
			# store Helper
			if(trim($helper)!="") {
				$PICArr['StaffType'] = "HELPER";
				$helpAry = explode(',',$helper);
				for ($a = 0; $a < sizeof($helpAry); $a++) {
					$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin='".trim($helpAry[$a])."'";
					$userid = $libenroll->returnVector($sql);
					$PICArr['UserID'] = $userid[0];
					$libenroll->ADD_GROUPSTAFF($PICArr);
					
					# if is Student => do not add to member list
					# otherwise, add to member list
					$sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '".$PICArr['UserID']."'";
					$personTypeArr = $libenroll->returnVector($sql);
					$personType = $personTypeArr[0];
					
					//check if the staff is removed for deletion later
					for ($j = 0; $j < sizeof($oldStaffArr); $j++){
						if ($PICArr['UserID'] == $oldStaffArr[$j]['UserID']){
							$oldStaffArr['notRemoved'][$j][0] = true;
						}
					}
					
				}
			}
			//delete staff from member list if is removed by the user
			for ($a = 0; $a < sizeof($oldStaffArr); $a++) {
				$sql = "SELECT RecordType FROM INTRANET_USER WHERE UserID = '".$oldStaffArr[$a]['UserID']."'";
				$personTypeArr = $libenroll->returnVector($sql);
				$personType = $personTypeArr[0];
				
				if (!$oldStaffArr['notRemoved'][$a][0] && $personType != 2){
					$sql = "DELETE FROM INTRANET_USERGROUP WHERE ((EnrolGroupID = '".$EnrolGroupID."') AND (UserID = '".$oldStaffArr[$a]['UserID']."'))";
					$libenroll->db_db_query($sql);
				}	
			}
			
			// assign schedule
			if(trim($schedule)!="") {
				$dataAry = array();
				$scheduleAry = explode(',',$schedule);
				for($a=0; $a<count($scheduleAry); $a++) {
					unset($dataAry);
					$dataAry['EnrolGroupID'] = $EnrolGroupID;
					$dataAry['ActivityDateStart'] = substr(trim($scheduleAry[$a]),0,16).":00";
					$dataAry['ActivityDateEnd'] = substr(trim($scheduleAry[$a]),0,11).substr(trim($scheduleAry[$a]),17).":00";
					$SuccessArr['Assign_Club_Schedule'][] = $libenroll->ADD_ENROL_GROUP_DATE($dataAry);
				}
			}
			
//			// Enrolment Cat to iPo Cat mapping setting
//			if($plugin['iPortfolio'] && $catInfo[0]['OleCategoryID'] != 0) {
//				$newOleSetting = array();
//				$newOleSetting['CategoryID'] = $catInfo[0]['OleCategoryID'];
//				$SuccessArr['DefaultOleSettings'][] = $libenroll->Update_Enrolment_Default_OLE_Setting('club', $EnrolGroupID, $newOleSetting);		
//			}
			
			if($plugin['iPortfolio']) {
				$newOleSetting = array();
				$newOleSetting['IntExt'] = $intExt;
				$newOleSetting['TitleLang'] = $titleLang;
				if($category == '' && $catInfo[0]['OleCategoryID'] != 0){
					$category = $catInfo[0]['OleCategoryID'];
				}
				$newOleSetting['CategoryID'] = $category;
				if($intExt=='EXT'){
					$newOleSetting['OLE_Component'] = '';
				}
				else{
					$newOleSetting['OLE_Component'] = $ole_component;
				}
				$newOleSetting['Organization'] = trim(stripslashes($organization));
				$newOleSetting['Details'] = trim(stripslashes($details));
				$newOleSetting['SchoolRemark'] = trim(stripslashes($SchoolRemarks));
				$libenroll->Update_Enrolment_Default_OLE_Setting('club', $EnrolGroupID, $newOleSetting);
			}
		}
	}
	
}

unset($_SESSION['eEnrolmentImportdata']);
header("Location: import_club_info.php?msg=ImportSuccess");

?>