<?php
// Editing by 
/*
 * 2017-09-15 (Anna): add webSAMS code and type
 * 2016-07-19 (Omas): replace split to explode for PHP5.4
 * 2015/12/29 (Omas): Add support to ole default setting import if have iPortfolio
 * 2014-11-21 (Omas): Import new field column I for target House - 'A' for no limit
 * 2014-07-09 (Carlos): $sys_custom['eEnrolment']['TWGHCYMA'] - belongToGroup changed to use INTRANET_GROUP.GroupCode
 * 2014-04-10 (Carlos): Added $sys_custom['eEnrolment']['TWGHCYMA'] TWGHs C Y MA MEMORIAL COLLEGE customization
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_auth();
intranet_opendb();

if (!$plugin['eEnrollment']){
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libenroll = new libclubsenrol();
$linterface = new interface_html();
$limport = new libimporttext();
$lo = new libfilesystem();

unset($_SESSION['eEnrolmentImportdata']);

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->IS_CLUB_PIC($EnrolGroupID) && !$libenroll->IS_EVENT_PIC($EnrolEventID)) {
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;
}

$filepath = $userfile;
$filename = $userfile_name;

$ext = strtoupper($lo->file_ext($filename));

# check extension of import file 
if($ext != ".CSV" && $ext != ".TXT")
{
	header("Location: import_activity_info.php?AcademicYearID=$AcademicYearID&msg=WrongFileFormat");
	exit();
}		

if($limport->CHECK_FILE_EXT($filename)) {
	# read file into array
	# return 0 if fail, return csv array if success
	$data = $limport->GET_IMPORT_TXT($filepath);
}


# check column of import file
if(is_array($data))
{
	$col_name = array_shift($data);
}

//$file_format = array('Belong to club', 'Activity Name', 'Activity Code', 'Content', 'Category', 'Nature', 'Target Form', 'Target Age Group', 'Target Gender', 'Tentative Fee', 'PIC', 'Helper', 'Enrolment Method', 'Participant Quota', 'Enrolment Period Start', 'Enrolment Period End', 'Enrolment Setting', 'Schedule');
$file_format = array('Belong to club', 'Activity Name', 'Activity Code', 'Content', 'Category');
if($sys_custom['eEnrolment']['TWGHCYMA']){
	$file_format = array_merge($file_format,array('Volunteer Service','Volunteer Service Hour','Activity Location','School Location','External Location'));
}
$file_format = array_merge($file_format,array('Nature', 'Target Form','Target House', 'Target Age Group', 'Target Gender', 'Tentative Fee', 'PIC', 'Helper', 'Enrolment Method', 'Participant Quota', 'Enrolment Period Start', 'Enrolment Period End', 'Enrolment Setting', 'Schedule'));
# add OLE setting
if($plugin['iPortfolio']) {
	$file_format[] = 'Record Type';
	$file_format[] = 'Language';
	$file_format[] = 'OLE Category';
	$file_format[] = 'OLE Components';
	$file_format[] = 'Partner Organizations';
	$file_format[] = 'Details';
	$file_format[] = 'School Remarks';
}
$file_format[] = 'WebSAMS STA Code';
$file_format[] = 'WebSAMS STA Type';

$format_wrong = false;
for($i=0; $i<sizeof($file_format); $i++)
{
	if ($col_name[$i]!=$file_format[$i])
	{
		$format_wrong = true;
		break;
	}
}

if($format_wrong)
{
	header("Location: import_activity_info.php?AcademicYearID=$AcademicYearID&msg=ImportUnsuccess_IncorrectHeaderFormat");
	exit();
}

# check any record in import file
if(sizeof($data)==0) {
	header("Location: import_activity_info.php?AcademicYearID=$AcademicYearID&msg=ImportUnsuccess_NoRecord");
	exit();	
}
	
	

$CurrentPage = "PageMgtActivity";
$CurrentPageArr['eEnrolment'] = 1;
$tab_type = "activity";
$current_tab = 1;

$PAGE_NAVIGATION[] = array($Lang['Btn']['Import']." ".$Lang['eEnrolment']['Activity'], "");


include_once("management_tabs.php");
	

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$STEPS_OBJ[] = array($i_general_select_csv_file, 0);
$STEPS_OBJ[] = array($i_general_confirm_import_data, 1);
$STEPS_OBJ[] = array($i_general_imported_result, 0);

$linterface->LAYOUT_START();

# Result Table Title
$resultTable .= "<table width=\"100%\" border=\"0\" cellpadding=\"5\" cellspacing=\"0\">\n";
$resultTable .= "<tr>\n";
$resultTable .= "<td width=\"1\" class=\"tablebluetop tabletopnolink\">#</td>\n";
$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['belong_to_group']}</td>\n";
$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['add_activity']['act_name']}</td>\n";
$resultTable .= "<td width=\"4%\" class=\"tablebluetop tabletopnolink\">{$Lang['eEnrolment']['ActivityCode']}</td>\n";
$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['add_activity']['act_content']}</td>\n";
$resultTable .= "<td width=\"8%\" class=\"tablebluetop tabletopnolink\" >{$eEnrollment['add_activity']['act_category']}</td>\n";
if($sys_custom['eEnrolment']['TWGHCYMA']){
	$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\" >".$Lang['eEnrolment']['VolunteerService']."</td>\n";
	$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\" >".$Lang['eEnrolment']['VolunteerServiceHour']."</td>\n";
	$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\" >".$Lang['eEnrolment']['ActivityLocation']."</td>\n";
	$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\" >".$Lang['eEnrolment']['SchoolLocation']."</td>\n";
	$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\" >".$Lang['eEnrolment']['ExternalLocation']."</td>\n";
}
$resultTable .= "<td width=\"8%\" class=\"tablebluetop tabletopnolink\" >{$Lang['eEnrolment']['Nature']}</td>\n";
$resultTable .= "<td width=\"10%\" class=\"tablebluetop tabletopnolink\" >{$eEnrollment['add_activity']['act_target']}</td>\n";
$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\" >{$Lang['eEnrolment']['TargetHouse']}</td>\n";
$resultTable .= "<td width=\"5%\" class=\"tablebluetop tabletopnolink\" >{$eEnrollment['add_activity']['act_age']}</td>\n";
$resultTable .= "<td width\"5%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['add_activity']['act_gender']}</td>\n";
$resultTable .= "<td width\"5%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['PaymentAmount']}</td>\n";
$resultTable .= "<td width\"5%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['add_activity']['act_pic']}</td>\n";
$resultTable .= "<td width\"5%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['add_activity']['act_assistant']}</td>\n";
$resultTable .= "<td width\"5%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['add_activity']['app_method']}</td>\n";
$resultTable .= "<td width\"4%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['participant_quota']}</td>\n";
$resultTable .= "<td width\"8%\" class=\"tablebluetop tabletopnolink\">{$Lang['eEnrolment']['EnrolmentPeriodStart']}</td>\n";
$resultTable .= "<td width\"8%\" class=\"tablebluetop tabletopnolink\">{$Lang['eEnrolment']['EnrolmentPeriodEnd']}</td>\n";
$resultTable .= "<td width\"5%\" class=\"tablebluetop tabletopnolink\">{$eEnrollment['add_activity']['apply_ppl_type']}</td>\n";
$resultTable .= "<td width\"10%\" class=\"tablebluetop tabletopnolink\">{$Lang['eEnrolment']['Schedule']}</td>\n";
$resultTable .= "<td width\"10%\" class=\"tablebluetop tabletopnolink\">&nbsp;</td>\n";
$resultTable .= "</tr>\n";


$numOfData = count($data);
$importFail = false;
$activityNameAry = array();
for($i=0; $i<$numOfData; $i++)
{
	# Get student club / activity info
	if($plugin['iPortfolio']) {
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $volunteerService, $volunteerServiceHour, $activityLocation, $schoolLocation, $externalLocation, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$intExt , $titleLang , $category , $ole_component ,$organization ,$details , $SchoolRemarks,$websamscode,$websamstype) = $data[$i];
		}else{
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$intExt , $titleLang , $category , $ole_component ,$organization ,$details , $SchoolRemarks,$websamscode,$websamstype) = $data[$i];
		}
	}
	else{
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $volunteerService, $volunteerServiceHour, $activityLocation, $schoolLocation, $externalLocation, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$websamscode,$websamstype) = $data[$i];
		}else{
			list($belongToClub, $activityName, $activityCode, $content, $activityCategory, $nature, $targetForm,$targetHouse, $age, $gender, $fee, $pic, $helper, $enrolmentMethod, $quota, $applyPeriodStart, $applyPeriodEnd, $setting, $schedule,$websamscode,$websamstype) = $data[$i];
		}		
	}
	
	#################################
	### check import data [Start] ###
	$error = array();
	
	// check Belong to club
	if(trim($belongToClub)=="") {
		$error['BelongToClub'] = $eEnrollment['belong_to_group'].$Lang['StudentRegistry']['IsMissing'];		//	Belong to club missing	
	} else {
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			$sql = "SELECT ".Get_Lang_Selection("g.TitleChinese","g.Title")." FROM INTRANET_GROUP g INNER JOIN INTRANET_ENROL_GROUPINFO i ON (i.GroupID=g.GroupID and g.GroupCode='$belongToClub') WHERE g.GroupCode='$belongToClub'";
		}else{
			$sql = "SELECT ".Get_Lang_Selection("g.TitleChinese","g.Title")." FROM INTRANET_GROUP g INNER JOIN INTRANET_ENROL_GROUPINFO i ON (i.GroupID=g.GroupID and i.EnrolGroupID='$belongToClub') WHERE EnrolGroupID='$belongToClub'";
		}
		$groupName = $libenroll->returnVector($sql);
		
		if($groupName[0]=="" && $belongToClub!="[NA]") {
			$error['BelongToClub'] = $eEnrollment['belong_to_group'].$Lang['StudentRegistry']['IsInvalid'];		//	Belong to club invalid
		}	
	}
	
	// check activity name
	if(trim($activityName)=="") {
		$error['ActivityName'] = $eEnrollment['add_activity']['act_name'].$Lang['StudentRegistry']['IsMissing'];		//	activity name missing	
	}
	
	if (isset($activityNameAry[STRTOUPPER(trim($activityName))])) {
		$error['ActivityNameDuplicated'] = $Lang['eEnrolment']['Warning']['ActivityNameDuplicated'];		//	activity name duplicated
	}
	
	// check content
	if(trim($content)=="") {
		$error['Content'] = $eEnrollment['add_activity']['act_content'].$Lang['StudentRegistry']['IsMissing'];		// club content missing
	}
	
	// check category
	if(trim($activityCategory)=="") {
		$error['Category'] = $eEnrollment['add_activity']['act_category'].$Lang['StudentRegistry']['IsMissing'];		// category name missing
	} else {
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			$activityCategoryAry = explode(",",trim($activityCategory));
			for($j=0;$j<count($activityCategoryAry);$j++) 
			{
				$sql = "SELECT CategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$libenroll->Get_Safe_Sql_Query(trim($activityCategoryAry[$j]))."'";
				$catInfo = $libenroll->returnVector($sql);
				if(count($catInfo)==0) {
					$error['Category'] = 	$eEnrollment['add_activity']['act_category'].$Lang['StudentRegistry']['IsInvalid'];		// category name invalid
				}
			}
		}else{
			$sql = "SELECT CategoryID FROM INTRANET_ENROL_CATEGORY WHERE CategoryName='".$libenroll->Get_Safe_Sql_Query(trim($activityCategory))."'";
			$catInfo = $libenroll->returnVector($sql);
			if(count($catInfo)==0) {
				$error['Category'] = 	$eEnrollment['add_activity']['act_category'].$Lang['StudentRegistry']['IsInvalid'];		// category name invalid
			}
		}
	}
	
	if($sys_custom['eEnrolment']['TWGHCYMA']){
		
		if(strtoupper($volunteerService) == 'YES' && (!is_numeric($volunteerServiceHour) || $volunteerServiceHour < 0)){
			$error['VolunteerServiceHour'] = $Lang['eEnrolment']['VolunteerServiceHour'].$Lang['StudentRegistry']['IsInvalid'];
		}
		
		if(trim($activityLocation) == '' || !in_array($activityLocation,array('0','1','2'))){
			$error['ActivityLocation'] = $Lang['eEnrolment']['ActivityLocation'].$Lang['StudentRegistry']['IsInvalid'];
		}
		
		if(trim($nature)=="") {
			$error['Nature'] = $Lang['eEnrolment']['Nature'].$Lang['StudentRegistry']['IsMissing'];		// activity nature missing
		} else {
			$activityNatureAry = explode(",",trim($nature));
			for($j=0;$j<count($activityNatureAry);$j++) 
			{
				$sql = "SELECT NatureID FROM INTRANET_ENROL_ACTIVITY_NATURE WHERE Nature='".$libenroll->Get_Safe_Sql_Query(trim($activityNatureAry[$j]))."'";
				$natureInfo = $libenroll->returnVector($sql);
				if(count($natureInfo)==0) {
					$error['Nature'] = 	$Lang['eEnrolment']['Nature'].$Lang['StudentRegistry']['IsInvalid'];		// activity nature invalid
				}
			}
		}
		
	}else{
	
		// check nature
		if(trim($nature)=="") {
			$error['Nature'] = $Lang['eEnrolment']['Nature'].$Lang['StudentRegistry']['IsMissing'];		// nature missing
		} else {
			$natureAry = array("1"=>$Lang['eEnrolment']['SchoolActivity'], "0"=>$Lang['eEnrolment']['NonSchoolActivity'], "2"=>$Lang['eEnrolment']['MixedActivity']);
			if(!array_key_exists($nature, $natureAry)) {
				$error['Nature'] = 	$Lang['eEnrolment']['Nature'].$Lang['StudentRegistry']['IsInvalid'];		// Nature invalid
			}
		}
	}
	
	// check form
	if(trim($targetForm)=="") {
		$error['Form'] = $eEnrollment['add_activity']['act_target'].$Lang['StudentRegistry']['IsMissing'];	// form missing	
	} else {
		$targetForm = trim($targetForm);
		
		if($targetForm!="A") {
			$_termForm = explode(',', $targetForm);
			$NoOfForm = 0;
			$delim = "";
			$formList = "";
			for($a=0; $a<count($_termForm); $a++) {
				if(trim($_termForm[$a])!="") {
					$formList .= $delim."'".trim($_termForm[$a])."'";
					$delim = ", ";
					$NoOfForm++;
				}	
			}	
			$sql = "SELECT COUNT(*) FROM YEAR WHERE YearName IN ($formList)";
			$formResult = $libenroll->returnVector($sql);
			
			if($formResult[0] != $NoOfForm) {
				$error['Form'] = $eEnrollment['add_activity']['act_target'].$Lang['StudentRegistry']['IsInvalid'];	
			}
		}
	}
	
	// check house
	if(trim($targetHouse)=="") {
		$error['House'] = 'House社'.$Lang['StudentRegistry']['IsMissing'];	// HOUSE missing	
	} else {
		$targetHouse = trim($targetHouse);
		
		if($targetHouse!="A") {
			$_termHouse = explode(',', $targetHouse);
			$NoOfHouse = 0;
			$delim = "";
			$houseList = "";
			for($a=0; $a<count($_termHouse); $a++) {
				if(trim($_termHouse[$a])!="") {
					$houseList .= $delim."'".trim($_termHouse[$a])."'";
					$delim = ", ";
					$NoOfHouse++;
				}	
			}	
			$sql = "SELECT COUNT(*) FROM INTRANET_GROUP WHERE GroupID IN ($houseList)";
			$houseResult = $libenroll->returnVector($sql);
			
			if($houseResult[0] != $NoOfHouse) {
				$error['House'] = $Lang['eEnrolment']['TargetHouse'].$Lang['StudentRegistry']['IsInvalid'];	
			}
		}
	}
	
	// check age
	if(trim($age)=="") {
		$error['Age'] = $eEnrollment['add_activity']['act_age'].$Lang['StudentRegistry']['IsMissing'];	// age missing		
	} else {
		$age = STRTOUPPER(trim($age));
		
		if($age!="A") {
			if(strrpos($age,'-')=="") {
				$error['Age'] = $eEnrollment['add_activity']['act_age'].$Lang['StudentRegistry']['IsInvalid'];	// age invalid
			} else {
				$ageAry = explode('-',$age);
				$lower = trim($ageAry[0]);
				$upper = trim($ageAry[1]);
				if(!is_numeric($lower) || !is_numeric($upper) || $lower>$upper || $lower<11 || $upper>=21) {
					$error['Age'] = $eEnrollment['add_activity']['act_age'].$Lang['StudentRegistry']['IsInvalid'];	// age invalid
				}
			}
		}	
	}
	
	// check gender
	if(trim($gender)=="") {
		$error['Gender'] = $eEnrollment['add_activity']['act_gender'].$Lang['StudentRegistry']['IsMissing'];	// gender missing		
	} else {
		$gender = STRTOUPPER(trim($gender));
		if($gender!="A" && $gender!="M" && $gender!="F") {
			$error['Gender'] = $eEnrollment['add_activity']['act_gender'].$Lang['StudentRegistry']['IsInvalid'];	// gender invalid
		}	
	}
	
	// check fee
	if(trim($fee)=="") {
		$error['Fee'] = $eEnrollment['PaymentAmount'].$Lang['StudentRegistry']['IsMissing'];	// fee missing	
	} else {
		$fee = STRTOUPPER(trim($fee));
		if($fee!="TBC" && !is_numeric($fee)) {
			$error['Fee'] = $eEnrollment['PaymentAmount'].$Lang['StudentRegistry']['IsInvalid'];	// fee invalid	
		}	
	}
	
	// check PIC
	if(trim($pic)=="") {
		$error['PIC'] = $eEnrollment['add_activity']['act_pic'].$Lang['StudentRegistry']['IsMissing'];	// PIC missing
	} else {
		$_picAry = explode(',', $pic);
		$NoOfPic = 0;
		$delim = "";
		$picList = "";
		for($a=0; $a<count($_picAry); $a++) {
			if(trim($_picAry[$a])!="") {
				$picList .= $delim."'".trim($_picAry[$a])."'";
				$delim = ", ";
				$NoOfPic++;
			}	
		}	
		$sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE UserLogin IN ($picList)";
		$picResult = $libenroll->returnVector($sql);
		
		if($picResult[0] != $NoOfPic) {
			$error['PIC'] = $eEnrollment['add_activity']['act_pic'].$Lang['StudentRegistry']['IsInvalid'];	
		} 
	}
	
	// check helper
	if(trim($helper) != "") {
		$_helperAry = explode(',', $helper);
		$NoOfHelper = 0;
		$delim = "";
		$helperList = "";
		for($a=0; $a<count($_helperAry); $a++) {
			if(trim($_helperAry[$a])!="") {
				$helperList .= $delim."'".trim($_helperAry[$a])."'";
				$delim = ", ";
				$NoOfHelper++;
			}	
		}	
		$sql = "SELECT COUNT(*) FROM INTRANET_USER WHERE UserLogin IN ($helperList)";
		$helperResult = $libenroll->returnVector($sql);
		
		
		if($helperResult[0] != $NoOfHelper) {
			$error['Helper'] = $eEnrollment['add_activity']['act_assistant'].$Lang['StudentRegistry']['IsInvalid'];	
		} 
	}
	
	// check Enrolment method
	if(trim($enrolmentMethod)=="") {
		$error['EnrolmentMethod'] = $eEnrollment['add_activity']['app_method'].$Lang['StudentRegistry']['IsMissing'];
	} else {
		$methodAry = array("0"=>$i_ClubsEnrollment_Random, "1"=>$i_ClubsEnrollment_AppTime);	
		if(!array_key_exists($enrolmentMethod, $methodAry)) {
			$error['EnrolmentMethod'] = $eEnrollment['add_activity']['app_method'].$Lang['StudentRegistry']['IsInvalid'];
		}
	}
	
	// check member quota
	if(trim($quota)=="") {
		$error['MemberQuota'] = $eEnrollment['club_quota'].$Lang['StudentRegistry']['IsMissing'];	// member quota missing
	} else {
		if(!is_numeric(trim($quota)) || trim($quota)<1) {
			$error['MemberQuota'] = $eEnrollment['club_quota'].$Lang['StudentRegistry']['IsInvalid'];	// member quota invalid	
		}
	}
	
	$hr = array('00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23');
	$minutes = array('00','05','10','15','20','25','30','35','40','35','40','45','50','55');
			
	// check enrolment period start time
	if(trim($applyPeriodStart)=="") {
		$error['StartPeriod'] = $Lang['eEnrolment']['EnrolmentPeriodStart'].$Lang['StudentRegistry']['IsMissing'];	// start period missing
	} else {
		$thisDate = substr($applyPeriodStart,0,10);
		$thisStartTime = substr($applyPeriodStart,11,5);
		$startTimestamp = strtotime($thisDate.' '.$thisStartTime);
		
		if(($thisDate.' '.$thisStartTime)!=date('Y-m-d H:i', $startTimestamp)) {
				$error['StartPeriod'] = $Lang['eEnrolment']['EnrolmentPeriodStart'].$Lang['StudentRegistry']['IsInvalid'];
		}
	}
	
	// check enrolment period end time
	if(trim($applyPeriodEnd)=="") {
		$error['MemberQuota'] = $Lang['eEnrolment']['EnrolmentPeriodEnd'].$Lang['StudentRegistry']['IsMissing'];	// end period missing
	} else {
		$thisDate = substr($applyPeriodEnd,0,10);
		$thisEndTime = substr($applyPeriodEnd,11,5);
		$endTimestamp = strtotime($thisDate.' '.$thisEndTime);
		
		if(($thisDate.' '.$thisEndTime)!=date('Y-m-d H:i', $endTimestamp)) {
				$error['EndPeriod'] = $Lang['eEnrolment']['EnrolmentPeriodEnd'].$Lang['StudentRegistry']['IsInvalid'];
		}
	}
	
	// check Enrolment setting
	if(trim($setting)=="") {
		$error['EnrolmentSetting'] = $eEnrollment['add_activity']['apply_ppl_type'].$Lang['StudentRegistry']['IsMissing'];	// Enrolment setting missing
	} else {
		$settingAry = array("S"=>$eEnrollment['add_activity']['student_enroll'], "P"=>$eEnrollment['add_activity']['parent_enroll']);
		$setting = STRTOUPPER($setting);
		if(!array_key_exists($setting, $settingAry)) {
			$error['EnrolmentSetting'] = $eEnrollment['add_activity']['apply_ppl_type'].$Lang['StudentRegistry']['IsInvalid'];	// Enrolment setting invalid	
		}
	}
	
	// check schedule
	if(trim($schedule) != "") {
		$_scheduleAry = explode(",", $schedule);
		
		for($a=0; $a<count($_scheduleAry); $a++) {
			$thisSchedule = trim($_scheduleAry[$a]);
			$thisDate = substr($thisSchedule,0,10);
			$thisStartTime = substr($thisSchedule,11,5);
			$thisEndTime = substr($thisSchedule,17,5);
			$startTimestamp = strtotime($thisDate.' '.$thisStartTime);
			$endTimestamp = strtotime($thisDate.' '.$thisEndTime);
			
			// check schedule format (YYYY-MM-DD hh:mm)
			if((($thisDate.' '.$thisStartTime)!=date('Y-m-d H:i', $startTimestamp)) || (($thisDate.' '.$thisEndTime)!=date('Y-m-d H:i', $endTimestamp))) {
				$error['Schedule'] = $Lang['eEnrolment']['ScheduleFormat'].$Lang['StudentRegistry']['IsInvalid'];
				break;
			} else {
				// check start time & end time
				$pos1 = strpos($thisStartTime,":");
				$pos2 = strrpos($thisEndTime,":");
				
				if(!in_array(substr($thisStartTime,$pos1-2,2), $hr) || !in_array(substr($thisStartTime,$pos1+1,2), $minutes) || !in_array(substr($thisEndTime,$pos1-2,2), $hr) || !in_array(substr($thisEndTime,$pos1+1,2), $minutes)) {
					$error['Schedule'] = $Lang['eEnrolment']['ScheduleFormat'].$Lang['StudentRegistry']['IsInvalid'];
					break;
				}
			}
		}
	}
	
	############check webSAMS type
	if($websamstype == 'E' || $websamstype=='S'|| $websamstype=='I' || $websamstype == ''){
		// 	/	continue;
	}else{
		$error['webSAMSType'] = $Lang['eEnrolment']['Import']['WebSAMS']['TypeError'];
	}
	############ webSAMS type check END #########
	

	
	// OLE default setting
	if($plugin['iPortfolio']) {
		if($intExt != '' && !($intExt == 'INT' || $intExt == 'EXT') ){
			$error['intExt'] = $Lang['eEnrolment']['Import']['OLESetting1_Warn'];
		}
		if($titleLang != '' && !($titleLang == 'C' || $titleLang == 'E') ){
			$error['titleLang'] = $Lang['eEnrolment']['Import']['OLESetting2_Warn'];
		}
		if($category != ''){
			$cat_array = $libenroll->Get_Ole_Category_Array();
			$cat_asso_array = BuildMultiKeyAssoc($cat_array,'0',array('1'),1);
			if(empty($cat_asso_array[$category])){
				$error['OLE_Cat'] = $Lang['eEnrolment']['Import']['OLESetting3_Warn'];
			}
		}
		if($intExt == 'INT' && $ole_component != ''){
			include_once($PATH_WRT_ROOT."includes/libportfolio.php");
			include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
			$LibPortfolio = new libpf_slp();
			$DefaultELEArray = $LibPortfolio->GET_ELE();
			
			$selected_component_arr = explode(',',$ole_component);
			$warn = $Lang['eEnrolment']['Import']['OLESetting4_Warn'];
			foreach($selected_component_arr as $code){
				if(empty($DefaultELEArray[$code])){
					if(empty($error['OLE_Com'])){
						$error['OLE_Com'] = $warn.$code;
					}
					else{
						$error['OLE_Com'] .= ','.$code;
					}
				}
				else{
					continue;
				}
			}
		}
	}
	
	### check import data [End] ###
	###############################
	

	# CSS of result table
	$fontColorStart = "<font color='red'>";
	$fontColorEnd = "</font>";
	$yearTermText = "";

	# Year-Base / Term-Base
	if(STRTOUPPER(trim($yearTerm))=="YEARBASE") {
		$yearTermText = $eEnrollment['YearBased'];	
	} else {
		$tempSemesterArr = explode(',',$yearTerm);
		$numOfSemester = count($tempSemesterArr);
		$delim = "";
		for ($a=0; $a<$numOfSemester; $a++)
		{
			$thisSemesterID = trim($tempSemesterArr[$a]);
			$sql = "SELECT ".Get_Lang_Selection("YearTermNameB5","YearTermNameEN")." FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='$AcademicYearID' AND YearTermID='$thisSemesterID'";
			$_temp = $libenroll->returnVector($sql);
			
			if(count($_temp)>0) {
				$yearTermText .= $delim.$_temp[0];
				$delim = ", ";
			}
		}
	}
	if($yearTermText=="") {
		$yearTermText = trim($yearTerm);	
	}
	
	
	
	# Build result table
	$css = "tabletext";
	$thisImportStatusDisplay = "";
	
	if(sizeof($error) > 0) {
		$importFail = true;
		
		$thisImportStatusDisplay .= "<ul>";
		
		foreach($error as $_errorMsg)
			$thisImportStatusDisplay .= "<li>".$_errorMsg."</li>";
		
		$thisImportStatusDisplay .= "</ul>";
	}
	
	##print house name by the code
	if($targetHouse=="A"){
		$displayTargetHouse = $Lang['eEnrolment']['NoLimit'];
	}
	else{
		$houseAry = $libenroll->Get_GroupMapping_Group($AcademicYearID, HOUSE_RECORDTYPE_IN_INTRANET_GROUP);
		$HouseAssoArr = BuildMultiKeyAssoc($houseAry, 'GroupID',array('Title'),1);
		$tempTargetHouse = explode(',',$targetHouse);
		$displayTargetHouse = '';
		foreach($tempTargetHouse as $house){
			if(trim($HouseAssoArr[trim($house)]) == ""){
				$tempAry[] = trim($house);
			}else{
				$tempAry[] = $HouseAssoArr[trim($house)];	
			}
			$displayTargetHouse = implode(',<br>',$tempAry);	
		}
	}
	
	$resultTable .= "<tr class=\"tablebluerow".(($i % 2) + 1)."\">\n";
	$resultTable .= "<td class=\"$css\">".($i+1)."</td>\n";
	
	# display club / activity name if from import of all clubs / activities
	
	$resultTable .= "<td class=\"$css\">". ($groupName[0]?$groupName[0]:'-') ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $activityName ."</td>\n";
	$resultTable .= "<td class=\"$css\">". ($activityCode?$activityCode:'-') ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $content ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $activityCategory ."</td>\n";
	if($sys_custom['eEnrolment']['TWGHCYMA']){
		$resultTable .= "<td class=\"$css\">". $volunteerService ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $volunteerServiceHour ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $activityLocation ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $schoolLocation ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $externalLocation ."</td>\n";
		$resultTable .= "<td class=\"$css\">". $nature ."</td>\n";
	}else{
		$resultTable .= "<td class=\"$css\">". $natureAry[$nature] ."</td>\n";
	}
	$resultTable .= "<td class=\"$css\">". $targetForm ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $displayTargetHouse ."</td>\n";
	$resultTable .= "<td class=\"$css\">". ($age=="A" ? $Lang['eEnrolment']['NoLimit'] : $age) ."</td>\n";
	$resultTable .= "<td class=\"$css\">". ($gender=="A"?$eEnrollment['all']:$gender) ."</td>\n";
	$resultTable .= "<td class=\"$css\">". ($fee=="TBC" ? $eEnrollment['ToBeConfirmed'] : $fee) ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $pic ."</td>\n";
	$resultTable .= "<td class=\"$css\">". ($helper=="" ? "-" : $helper) ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $methodAry[$enrolmentMethod] ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $quota ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $applyPeriodStart."</td>\n";
	$resultTable .= "<td class=\"$css\">". $applyPeriodEnd ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $settingAry[$setting] ."</td>\n";
	$resultTable .= "<td class=\"$css\">". $schedule ."</td>\n";
	$resultTable .= "<td class=\"$css\">".$fontColorStart.$thisImportStatusDisplay.$fontColorEnd."</td>\n";
	$resultTable .= "</tr>\n";
	
	$activityNameAry[STRTOUPPER(trim($activityName))] = true;
}  
        
$resultTable .= "</table>\n";


//buttons at the bottom
$buttons = "";

	
if($importFail==false) {	
	$buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "submit", "")."&nbsp;";
}
	
$buttons .= $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "window.location = 'import_activity_info.php?AcademicYearID=$AcademicYearID'");

	



?>

<script language="javascript">
function submitForm(obj,page)
{
	if(countChecked(obj,'clash_studentID[]')==0)
		alert(globalAlertMsg2);
	else
	{
		if(confirm(globalAlertMsg4)){
			obj.action=page;
			obj.method="POST";
			obj.submit();
			return;
		}
	}
}


</script>

<br />
<form name="form1" method="post" action="import_activity_update.php">
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
    <td><?= $linterface->GET_STEPS($STEPS_OBJ) ?>
    </td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr>
	<td align="center">
    	<table width="95%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td align="left" class="tabletext">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
                	<td colspan="2"><?=$resultTable?></td>
				</tr>
				
				<tr>
                	<td colspan="2">&nbsp;</td>
				</tr>
				
				<tr>
                	<td colspan="2"><?=$TimeCrashTable?></td>
				</tr>
				
				</table>
			</td>
		</tr>
		</table>
    </td>
</tr>
<tr>
	<td>        
    	<table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        	<tr>
            	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
            </tr>
            <tr>
				<td align="center" colspan="2">
				<?= $buttons ?>
				</td>
			</tr>
	    </table>                                
	</td>
</tr>
</table>
<br />
		
		<input type="hidden" name="isFromResultPage" value="<?=1?>">
		<input type="hidden" name="type" value="<?=$type?>">
		<input type="hidden" name="GroupID" value="<?=$GroupID?>">
		<input type="hidden" name="EnrolGroupID" value="<?=$EnrolGroupID?>">
		<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
		<input type="hidden" name="crashedStudentInfoArr" value="<?=rawurlencode(serialize($crashedStudentInfoArr))?>">
		<input type="hidden" name="Semester" id="Semester" value="<?=$Semester?>"/>
		<input type="hidden" name="AcademicYearID" value="<?=$AcademicYearID?>">
		
</form>

<?
$_SESSION['eEnrolmentImportdata'] = $data;
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
