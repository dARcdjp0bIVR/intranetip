<?php
# using: 

/*
 * 2020-11-03 (Ray)
 * - added merchant checking
 *
 * 2020-10-30 (Ray)
 * - added quota
 *
 * 2020-07-03 (Ray)
 * - added multi payment gateway
 *
 * 2020-02-27 (Ray)
 * - added MerchantAccountID
 *
 * 2019-10-23 (Henry)
 * - bug fix for the PaymentAmount [Case#E173980]
 * 
 * 2015-06-16 (Omas)
 * - handling payment for event
 * 
 * 2015-06-02 (Evan)
 * - Replace the old style with UI standard
 * - When access via club, the tab is Record instead of Process
 * 
 * 2014-03-31 (Ivan) [2014-0324-1344-50066]
 * - improved to prevent double submission
 */

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$lpayment = new libpayment();
$use_merchant_account = $lpayment->useEWalletMerchantAccount();
if($use_merchant_account) {
	if ($sys_custom['ePayment']['MultiPaymentGateway']) {

	} else {
		$merchants = $lpayment->getMerchantAccounts(array('RecordStatus' => 1, 'order_by_accountname' => true));
		$merchant_selection_data = array();
		for ($i = 0; $i < count($merchants); $i++) {
			$merchant_selection_data[] = array($merchants[$i]['AccountID'], $merchants[$i]['AccountName']);
		}
	}
}

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	if (isset($_REQUEST['EnrolGroupID'])) 
	{
		$EnrolGroupID = $_REQUEST['EnrolGroupID'];
		$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
		
		$CurrentPage = "PageMgtClub";
		$TempArr = $libenroll->getGroupInfo($GroupID);
		$TempAmount = $libenroll->getGroupPaymentAmount($EnrolGroupID);
		//$title = $TempArr[0][1]." - ".$eEnrollmentMenu['act_status_stat'];
		$title = ($intranet_session_language=="en"?$TempArr[0][1]:$TempArr[0][2])." - ".$eEnrollmentMenu['club_enroll'];
		//$back_url = "club_status.php";
		$back_url = "group.php";
		# tags 
	    $tab_type = "club";
	    //$current_tab = 2;
	    $current_tab = 1;

	}
	if (isset($_REQUEST['EnrolEventID'])) 
	{
		$CurrentPage = "PageMgtActivity";
		$TempArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		$TempAmount = $TempArr['PaymentAmount'];
		//$title = $TempArr[3]." - ".$eEnrollmentMenu['act_event_status_stat'];
		$title = $TempArr[3]." - ".$eEnrollmentMenu['event_enroll'];
		//$back_url = "event_status.php";
		$back_url = "event.php";
		# tags 
	    $tab_type = "activity";
	    //$current_tab = 2;
	    $current_tab = 1;

	}
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $title .= " ".$eEnrollment['payment'];
        //$TAGS_OBJ[] = array($title, "", 1);
        # tags 
	    include_once("management_tabs.php");
	    # navigation
        $PAGE_NAVIGATION[] = array($title, "");

		$STEPS_OBJ[] = array($eEnrollment['add_payment']['step_1'], 1);
        $STEPS_OBJ[] = array($eEnrollment['add_payment']['step_2'], 0);
        
        $linterface->LAYOUT_START();

        # category selection
        ####################################################################
        $Sql = "
        			SELECT
							CatID, Name, DisplayOrder, Description, RecordType, RecordStatus
        			FROM
        					PAYMENT_PAYMENT_CATEGORY
        		";
        $ReturnArr = $libenroll->returnArray($Sql, 6);
                
        $CatSel = "<select id='CatID' name='CatID'>";
        for ($i = 0; $i < sizeof($ReturnArr); $i++) {
	        $CatSel .= "<option value='".$ReturnArr[$i][0]."'>".$ReturnArr[$i][1]."</option>";
        }
        $CatSel .= "</select>";
        ####################################################################
        
        $lpayment = new libpayment();
        $nextOrder = $lpayment->getPaymentItemNextDisplayOrder($CatID);
        
?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	if(!check_text(obj.ItemName, "<?php echo $i_alert_pleasefillin.$eEnrollment['pay']['item_name']; ?>.")) return false;
	if(!check_numeric(obj.ItemAmount, 0, "<?php echo $i_alert_pleasefillin.$eEnrollment['pay']['amount']; ?>.")) return false;
	
	if(!check_date(obj.StartDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
	if(!check_date(obj.EndDate,'<?=$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format?>')) return false;
	if(!check_numeric(obj.DisplayOrder,'','<?=$i_Payment_Warning_Invalid_DisplayOrder?>')) return false;
	if(!check_numeric(obj.PayPriority,'','<?=$i_Payment_Warning_Invalid_PayPriority?>')) return false;
	
	
	if (isNaN(parseInt(obj.ItemAmount.value)) || parseInt(obj.ItemAmount.value) <= 0)
	{
		alert("<?=$eEnrollment['js_amount_cannot_be_zero']?>");
		return false;
	}

	<?php if($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
    var have_merchantid = false;
    $("select[id^=MerchantAccountID_]").each(function(e) {
        if($(this).val() != '') {
            have_merchantid = true;
        }
    });
    if(have_merchantid == false) {
        alert('<?=$Lang['ePOS']['SelectMerchantID']?>');
        return false;
    }
	<?php } ?>

	//AlertPost(obj, 'payment_adjust.php', '<?= $eEnrollment['js_payment_alert']?>');
	//obj.action = 'payment_adjust.php';
	//obj.submit();
	$('input#nextBtn').attr('disabled', 'disabled');
    obj.action = 'payment_adjust.php';
    obj.method = "post";
    obj.submit();
}

function onMerchantChange(obj, sp) {
    var id = $(obj).val();
    $(".quota_"+sp).hide();
    $(".quota_id_"+id).show();
}

</SCRIPT>
<form name="form1" action="" method="POST">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<!--
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
-->
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
</table>
<table class="form_table_v30">
<tr>
	<td class="field_title">
		<?= $linterface->RequiredSymbol().$eEnrollment['pay']['item_name']?>  
	</td>
	<td class="tabletext">
		<input type="text" class="textboxtext" id="ItemName" name="ItemName" value="<?= $title?>">
	</td>
</tr>
<tr>
	<td class="field_title">
		<?= $linterface->RequiredSymbol().$eEnrollment['pay']['amount']?>  
	</td>
	<td class="tabletext">
		<input type="text" class="textboxnum" id="ItemAmount" name="ItemAmount" value="<?= $TempAmount?>">
	</td>
</tr>
<tr>
	<td class="field_title">
		<?= $linterface->RequiredSymbol().$Lang['eNotice']['PaymentCategory']?>
	</td>
	<td class="tabletext">
		<?= $CatSel?>
	</td>
</tr>

<tr>
	<td class="field_title">
		<?=$linterface->RequiredSymbol().$i_general_startdate?>  
	</td>
	<td>
		<input type="text" name="StartDate" maxlength="10" value="<?=date('Y-m-d')?>" class="textboxnum"> <?= $linterface->GET_CALENDAR("form1", "StartDate")?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$linterface->RequiredSymbol().$i_general_enddate?>  
	</td>
	<td>
		<input type="text" name="EndDate" maxlength="10" value="<?=date('Y-m-d', time()+7*24*60*60)?>" class="textboxnum"> <?= $linterface->GET_CALENDAR("form1", "EndDate")?>
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$linterface->RequiredSymbol().$i_Payment_Field_DisplayOrder?>  
	</td>
	<td>
		<input type="text" name="DisplayOrder" maxlength="10" value="<?=$nextOrder?>" class="textboxnum">
	</td>
</tr>
<tr>
	<td class="field_title">
		<?=$linterface->RequiredSymbol().$i_Payment_Field_PayPriority?>  
	</td>
	<td class="tabletext">
		<input type="text" name="PayPriority" maxlength="10" value="<?=0?>" class="textboxnum"> <span class="tabletextremark">(<?=$i_Payment_Note_Priority?>)</span>
	</td>
</tr>
<tr>
	<td class="field_title" valign="top">
		<?=$i_general_description?>
	</td>
	<td>
		<?= $linterface->GET_TEXTAREA("Discription", "")?>
	</td>
</tr>


<?php if($use_merchant_account) { ?>
    <tr>
        <td class="field_title">
            <?=$Lang['ePayment']['MerchantAccount']?>&nbsp;
        </td>
        <td class="tabletext">
            <?php if ($sys_custom['ePayment']['MultiPaymentGateway']) { ?>
            <table>
                <?php
                $x = '';
                $service_provider_list = $lpayment->getPaymentServiceProviderMapping(false,true);
                foreach($service_provider_list as $k=>$temp) {
                    $merchants = $lpayment->getMerchantAccounts(array('ServiceProvider'=>$k,'RecordStatus'=>1,'order_by_accountname'=>true));
                    $merchant_selection_data = array();
                    for($i=0;$i<count($merchants);$i++){
                        $merchant_selection_data[] = array($merchants[$i]['AccountID'],$merchants[$i]['AccountName']);
                    }
                    $x .= '<tr>';
                    $x .= '<td>'.$temp.'</td>';
                    $x .= '<td>';
                    $x .= $linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID_'.$k.'" name="MerchantAccountID_'.$k.'" onchange="onMerchantChange(this,\''.$k.'\')"', $Lang['General']['EmptySymbol'], '');
					foreach($merchant_selection_data as $merchant_data) {
						$year = date('Y');
						$month = date('m');
						$sql = "SELECT * FROM PAYMENT_MERCHANT_ACCOUNT_QUOTA WHERE Year='$year' AND Month='$month' AND AccountID='".$merchant_data[0]."'";
						$rs_quota = $lpayment->returnArray($sql);
						if(count($rs_quota)>0) {
							$remain_quota = $rs_quota[0]['Quota'] - $rs_quota[0]['UsedQuota'];
							if($remain_quota < 0) {
								$remain_quota = 0;
							}
							$quota_str = str_replace('{COUNT}', $remain_quota, $Lang['ePayment']['CurrentRemainQuota']);
							$x .= ' <span class="quota_' . $k . ' quota_id_' . $merchant_data[0] . '" style="display: none;">(' . $quota_str . ')</span>';
						}
					}
                    $x .= '</td>';
                    $x .= '</tr>';
                }
                echo $x;
                ?>
            </table>
            <?php } else { ?>
            <?=$linterface->GET_SELECTION_BOX($merchant_selection_data, ' id="MerchantAccountID" name="MerchantAccountID" ', '', count($merchants)>0?$merchants[0]['AccountID']:'');?>
            <?php } ?>
        </td>
    </tr>
<?php } else {	?>
<tr>
	<td class="field_title">
		<?= $eEnrollment['debit_method']?>&nbsp;
	</td>
	<td class="tabletext">
		<input type="radio" id="debit_directly" name="debit_type" value="debit_directly" checked><label for="debit_directly"><?= $eEnrollment['debit_directly']?></label>
		<input type="radio" id="debit_later" name="debit_type" value="debit_later"><label for="debit_later"><?= $eEnrollment['debit_later']?></label>
	</td>
</tr>
<?php } ?>
</table>


<table width="96%" border="0" cellspacing="0" cellpadding="4" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_next, "button", "FormSubmitCheck(this.form)", "nextBtn")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='$back_url'")?>
</div>
</td></tr>
</table>
<br />

<input type="hidden" id="GroupID" name="GroupID" value="<?= $GroupID?>">
<input type="hidden" id="EnrolGroupID" name="EnrolGroupID" value="<?= $EnrolGroupID?>">
<input type="hidden" id="EnrolEventID" name="EnrolEventID" value="<?= $EnrolEventID?>">

</form>


    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>