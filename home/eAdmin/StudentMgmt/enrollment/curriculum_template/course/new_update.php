<?php
/*
 *  Using:
 *  
 *  2018-10-04 [Cameron] add IsCombined (Lesson) to INTRANET_ENROL_CURRICULUM_TEMPLATE_LESSON
 *  
 * 	2018-07-11 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
 
$result = array();
$dataAry = array();
$templateID = IntegerSafe($_POST['TemplateID']);
$dataAry['TemplateID'] = $templateID;
$dataAry['Title'] = standardizeFormPostValue($_POST['Title']);
$dataAry['Description'] = standardizeFormPostValue($_POST['Description']);
$dataAry['CategoryID'] = IntegerSafe($_POST['CategoryID']);
$isCombinedCourse = $_POST['IsCombinedCourse'];
$dataAry['IsCombinedCourse'] = $isCombinedCourse ? '1' : '0';
$dataAry['DateInput'] = 'now()'; 
$dataAry['InputBy'] = $_SESSION['UserID'];
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_CURRICULUM_TEMPLATE_COURSE',$dataAry,array(),false);

$libenroll->Start_Trans();

$res = $libenroll->db_db_query($sql);
$result[] = $res;
if ($res) {
    $courseID = $libenroll->db_insert_id();
    $lessonAry = $_POST['LessonTitle'];
    $numberOfLesson = IntegerSafe($_POST['NumberOfLesson']);
    if ($isCombinedCourse) {
        $weekNumberAry = IntegerSafe($_POST['WeekNumber']);
        $dayNumberAry = IntegerSafe($_POST['DayNumber']);
        $startHourAry = IntegerSafe($_POST['StartHour']);
        $startMinuteAry = IntegerSafe($_POST['StartMinute']);
        $endHourAry = IntegerSafe($_POST['EndHour']);
        $endMinuteAry = IntegerSafe($_POST['EndMinute']);
        
        for($i=0;$i<$numberOfLesson;$i++) {
            unset($dataAry);
            $dataAry['CourseID'] = $courseID;
            $dataAry['Title'] = standardizeFormPostValue($lessonAry[$i]);
            $dataAry['WeekNumber'] = $weekNumberAry[$i];
            $dataAry['DayNumber'] = $dayNumberAry[$i];
            $dataAry['StartTime'] = str_pad($startHourAry[$i], 2, "0", STR_PAD_LEFT) . ':'. str_pad($startMinuteAry[$i], 2, "0", STR_PAD_LEFT).':00';
            $dataAry['EndTime'] = str_pad($endHourAry[$i], 2, "0", STR_PAD_LEFT) . ':'. str_pad($endMinuteAry[$i], 2, "0", STR_PAD_LEFT).':00';
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_CURRICULUM_TEMPLATE_LESSON',$dataAry,array(),false,false,false);
            $result[] = $libenroll->db_db_query($sql);
        }
    }
    else {
        for($i=0;$i<$numberOfLesson;$i++) {
            $isCombinedLesson = IntegerSafe($_POST['IsCombinedLesson_'.$i]);
            unset($dataAry);
            $dataAry['CourseID'] = $courseID;
            $dataAry['Title'] = standardizeFormPostValue($lessonAry[$i]);
            $dataAry['WeekNumber'] = null;
            $dataAry['DayNumber'] = null;
            $dataAry['StartTime'] = null;
            $dataAry['EndTime'] = null;
            $dataAry['IsCombined'] = $isCombinedLesson;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_CURRICULUM_TEMPLATE_LESSON',$dataAry,array(),false,false,false);
            $result[] = $libenroll->db_db_query($sql);
        }
    }
}

if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'AddSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'AddUnsuccess';
}

header("location: index.php?TemplateID=".$templateID."&returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>