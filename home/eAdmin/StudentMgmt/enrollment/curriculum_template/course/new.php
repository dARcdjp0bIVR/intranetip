<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-06 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$templateID = IntegerSafe($TemplateID);  // can be from GET and POST
if (! $templateID) {
    header("location: index.php");
}

$curriculumTemplateAry = $libenroll->getCurriculumTemplate($templateID);
if (count($curriculumTemplateAry)) {
    $curriculumTemplate = $curriculumTemplateAry[0];
    $curriculumTitle = Get_Lang_Selection($curriculumTemplate['TitleChinese'], $curriculumTemplate['TitleEnglish']);
}

if ($sys_custom['eEnrolment']['CategoryType']) {
    $categoryTypeID = array(2);	// Activity
}
else {
    $categoryTypeID = 0;
}

$categorySelection = $libenroll->GET_CATEGORY_SEL($ParSelectedStr='', $ParSelected=1, $ParOnChange='', $button_select, $noFirst=0,$categoryTypeID,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='CategoryID');

$linterface = new libclubsenrol_ui();
$numberOfLessonSelection = $linterface->getNumberOfLessonSelection(1);
$lessonAry = array();
$isCombinedCourse = false;
$lessonTitleList = $linterface->getLessonTitleList($lessonAry,$isCombinedCourse);

# Page heading setting
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtCurriculumTemplate";

### Title ###
$TAGS_OBJ = $libenroll->getCurriculumTemplateTabs("template");

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['Btn']['New']. ' ' . $Lang['eEnrolment']['curriculumTemplate']['course']['course'], "");

$linterface->LAYOUT_START();

$form_action = "new_update.php";
include("course.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>