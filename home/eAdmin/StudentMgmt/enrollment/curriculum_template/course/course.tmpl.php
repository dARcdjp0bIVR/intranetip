<?php
/*
 * 	Log
 *
 *  2018-10-24 [Cameron]
 *      - fix the show/hide of lesson schedule and "is combined lesson" when NumberOfLesson change 
 *      
 *  2018-10-02 [Cameron]
 *      - add field IsCombinedLesson to Lesson if the course is Non-Combined course 
 *      
 *  2018-08-10 [Cameron]
 *      - fix: should parseInt for NumberOfLesson
 *      
 * 	2018-07-06 [Cameron]
 * 		create this file
 */
?>
<script type="text/javascript">

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
 	
	if($('#Title').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'Title';
 		$('#ErrTitle').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if($('#Description').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'Description';
 		$('#ErrDescription').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if($('#CategoryID').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'CategoryID';
 		$('#ErrCategoryID').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if($('#NumberOfLesson').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'NumberOfLesson';
 		$('#ErrNumberOfLesson').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
	
	if ($("input:radio[name='IsCombinedCourse']:checked").length == 0) {
 		error++;
 		if (focusField == '') focusField = 'IsCombinedCourseNo';
 		$('#ErrIsCombinedCourse').addClass('error_msg_show').removeClass('error_msg_hide');
	}

 	$('.show :input[name="LessonTitle\[\]"]').each(function(){
 		if ($.trim($(this).val()) == '') {
 	 		error++;
 	 		if (focusField == '') focusField = $(this).attr('id');
 	 		$('#Err' + $(this).attr('id')).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});

 	if ($('#IsCombinedCourseYes').is(':checked')) {
 	 	var ary, row;
 	 	var dateStartId = '';
 	 	var dateEndId = '';
 	 	var secondStartId = '';
 	 	var secondEndId = '';
 	 	var hourStartId, minuteStartId, hourEndId, minuteEndId;
 	 	
 	 	$('.show :select[name="StartHour\[\]"]').each(function(){
			ary = $(this).attr('id').split('_');
	 		row = ary[1];
	 		hourStartId = 'StartHour_' + row;
	 		minuteStartId = 'StartMinute_' + row;
	 		hourEndId = 'EndHour_' + row;
	 		minuteEndId = 'EndMinute_' + row;
	 		if (!compareTimeByObjId(dateStartId, hourStartId, minuteStartId, secondStartId, dateEndId, hourEndId, minuteEndId, secondEndId)) {
 	 	 		error++;
 	 	 		if (focusField == '') focusField = $(this).attr('id');
 	 	 		$('#ErrTimeLogic_' + row).addClass('error_msg_show').removeClass('error_msg_hide');
 	 		} 	
 	 	});
 	}
 	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function hideError() 
{
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

var prevNrOfLesson;

$(document).ready(function(){
	prevNrOfLesson = $('#NumberOfLesson').val();
		
	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

		if (checkForm(document.form1)) {
   			$('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

	$('#NumberOfLesson').change(function(){
		var nrOfLesson = parseInt($('#NumberOfLesson').val());
		if (nrOfLesson > 0 ) {
			if (nrOfLesson < prevNrOfLesson) {
				if (confirm("<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['changeToLessLesson'];?>")) {
	    			for(var i=nrOfLesson; i < prevNrOfLesson; i++) {
	    				$('#divLesson_' + i).removeClass('show').addClass('hide');
	    				if ($('#IsCombinedCourseYes').is(':checked')) {
	    					$('#divLesson_' + i +' div').removeClass('show').addClass('hide');
	    					$('#divLesson_' + i +' .isCombinedSpan').removeClass('hide').addClass('show');
	    				}
	    				else {
	    					$('#divLesson_' + i +' div').removeClass('hide').addClass('show');
	    					$('#divLesson_' + i +' .isCombinedSpan').removeClass('show').addClass('hide');
	    				}
	    			}
	    			prevNrOfLesson = parseInt($('#NumberOfLesson').val());
				}
				else {
					$('#NumberOfLesson').val(prevNrOfLesson);
				}
			}
			else {
    			for(var i=0; i < nrOfLesson; i++) {
    				$('#divLesson_' + i).removeClass('hide').addClass('show');
    				if ($('#IsCombinedCourseYes').is(':checked')) {
    					$('#divLesson_' + i +' div').removeClass('hide').addClass('show');
    					$('#divLesson_' + i +' .isCombinedSpan').removeClass('show').addClass('hide');
    				}
    				else {
    					$('#divLesson_' + i +' div').removeClass('show').addClass('hide');
    					$('#divLesson_' + i +' .isCombinedSpan').removeClass('hide').addClass('show');
    				}
    			}
    			prevNrOfLesson = $('#NumberOfLesson').val();
			}
		}		
	});

	$('input:radio[name="IsCombinedCourse"]').change(function(){
		if ($(this).val() == '1') {
			$('[id^="divLesson_"].show div').removeClass('hide').addClass('show');
			$('[id^="divLesson_"].show .isCombinedSpan').removeClass('show').addClass('hide');
		}
		else {
			$('[id^="divLesson_"].show div').removeClass('show').addClass('hide');
			$('[id^="divLesson_"].show .isCombinedSpan').removeClass('hide').addClass('show');
		}
	});
});

	
</script>

<style>
.hide {display:none};
.show {display:''};
 </style>

<form name="form1" id="form1" method="post" action="<?php echo $form_action;?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="navigation"><?php echo $linterface->GET_NAVIGATION($PAGE_NAVIGATION); ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['curriculum']['title'];?></td>
								<td class="tabletext"><?php echo intranet_htmlspecialchars($curriculumTitle);?></td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['title'];?></td>
								<td class="tabletext">
									<input type="text" name="Title" id="Title" class="textboxtext" value="<?php echo intranet_htmlspecialchars($course['Title']);?>">
									<span class="error_msg_hide" id="ErrTitle"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseTitle'];?></span>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['description'];?></td>
								<td class="tabletext">
									<?php echo $linterface->GET_TEXTAREA("Description", $course['Description'], 80, 10);?>
									<span class="error_msg_hide" id="ErrDescription"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['inputCourseDescription'];?></span>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['category'];?></td>
								<td class="tabletext"><?php echo $categorySelection;?>
									<span class="error_msg_hide" id="ErrCategoryID"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['selectCourseCategory'];?></span>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['combinedCourse'];?></td>
								<td class="tabletext">
									<input type="radio" name="IsCombinedCourse" id="IsCombinedCourseNo" value="0"<?php echo $course['IsCombinedCourse']?'':' checked'?>><label for="IsCombinedCourseNo"><?php echo $Lang['General']['No'];?></label>
									<input type="radio" name="IsCombinedCourse" id="IsCombinedCourseYes" value="1"<?php echo $course['IsCombinedCourse']?' checked':'';?>><label for="IsCombinedCourseYes"><?php echo $Lang['General']['Yes'];?></label>
									<span class="error_msg_hide" id="ErrIsCombinedCourse"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['selectCombinedCourse'];?></span>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['numberOfLesson'];?></td>
								<td class="tabletext"><?php echo $numberOfLessonSelection;?>
									<span class="error_msg_hide" id="ErrNumberOfLesson"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['selectNumberOfLesson'];?></span>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['course']['lessonTitle'];?></td>
								<td class="tabletext"><?php echo $lessonTitleList;?></td>
							</tr>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?php echo $i_general_required_field;?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?php echo "{$image_path}/{$LAYOUT_SKIN}"; ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
    									<?php echo $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
    									<?php echo $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
    									<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php?TemplateID=$templateID'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="TemplateID" name="TemplateID" value="<?php echo $templateID;?>">
<input type=hidden id="CourseID" name="CourseID" value="<?php echo $courseID;?>">
</form>
