<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-12 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if (is_array($CourseID)) {
    $courseID = (count($CourseID) == 1) ? $CourseID[0] : '';
} else {
    $courseID = $CourseID;
}
$courseID = IntegerSafe($courseID);

if (! $courseID) {
    header("location: index.php");
}

$courseAry = $libenroll->getCourseTemplate($courseID);
if (count($courseAry)) {
    $course = $courseAry[0];
    $templateID = $course['TemplateID'];
    $curriculumTitle = $course['CurriculumTitle'];
    $categoryID = $course['CategoryID'];
    $isCombinedCourse = $course['IsCombinedCourse'];
}
else {
    header("location: index.php");
}

$lessonAry = $libenroll->getLessonTemplate($courseID);
$numberOfLesson = count($lessonAry);

if ($sys_custom['eEnrolment']['CategoryType']) {
    $categoryTypeID = array(2);	// Activity
}
else {
    $categoryTypeID = 0;
}

$categorySelection = $libenroll->GET_CATEGORY_SEL($categoryID, $ParSelected=1, $ParOnChange='', $button_select, $noFirst=0,$categoryTypeID,$IsMultiple=0, $ParDefaultSelectedAll=false, $DeflautName='CategoryID');

$linterface = new libclubsenrol_ui();
$numberOfLessonSelection = $linterface->getNumberOfLessonSelection($numberOfLesson);

$lessonTitleList = $linterface->getLessonTitleList($lessonAry,$isCombinedCourse);

# Page heading setting
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtCurriculumTemplate";

### Title ###
$TAGS_OBJ = $libenroll->getCurriculumTemplateTabs("template");

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']. ' ' . $Lang['eEnrolment']['curriculumTemplate']['course']['course'], "");

$linterface->LAYOUT_START();

$form_action = "edit_update.php";
include("course.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>