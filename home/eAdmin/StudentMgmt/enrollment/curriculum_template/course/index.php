<?php
/*
 *  Using:
 *  
 *  Purpose:    show course list covered in a curriculum template
 *  
 *  
 * 	2018-07-06 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
} 
else 
{
	updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$templateID = IntegerSafe($TemplateID);  // can be from GET and POST
if (! $templateID) {
    header("location: ../curriculum/index.php");
}

$curriculumTemplateAry = $libenroll->getCurriculumTemplate($templateID);
if (count($curriculumTemplateAry)) {
    $curriculumTemplate = $curriculumTemplateAry[0];
    $curriculumTitle = Get_Lang_Selection($curriculumTemplate['TitleChinese'], $curriculumTemplate['TitleEnglish']);
}

# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtCurriculumTemplate";

### Title ###
$TAGS_OBJ = $libenroll->getCurriculumTemplateTabs("template");

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['curriculumTemplate']['curriculum']['details'], "");
$returnMsg = $Lang['General']['ReturnMessage'][$returnMsgKey];
$linterface->LAYOUT_START($returnMsg);

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;		// default ascending
$field = ($field == "") ? 0 : $field;

$li = new libdbtable2007($field, $order, $pageNo);

$sql = "SELECT
				CONCAT('<a href=\"edit.php?CourseID=',c.CourseID,'\">',c.Title,'</a>'),
				c.Description,
                a.CategoryName,
                s.NumberOfLesson,                
				CONCAT('<input type=\'checkbox\' name=\'CourseID[]\' id=\'CourseID[]\' value=\'', c.`CourseID`,'\'>')
        FROM 
				INTRANET_ENROL_CURRICULUM_TEMPLATE_COURSE c
		LEFT JOIN 
                INTRANET_ENROL_CATEGORY a ON a.CategoryID=c.CategoryID
        LEFT JOIN
                (SELECT 
                        CourseID, COUNT(LessonID) AS NumberOfLesson
                 FROM 
                        INTRANET_ENROL_CURRICULUM_TEMPLATE_LESSON
                 GROUP BY 
                        CourseID) AS s ON s.CourseID=c.CourseID                        
        WHERE   c.TemplateID='".$templateID."' ";

$extra_column = 2;
$checkbox_col = "<th width='1'>".$li->check("CourseID[]")."</th>\n";
$toolbar = $linterface->GET_LNK_NEW("javascript:checkGet(document.form1,'new.php?TemplateID=$templateID')",$button_new,"","","",0);
//$manage_record_bar  = '<a href="javascript:checkEdit(document.form1,\'CourseID[]\',\'edit.php\')" class="tool_edit">'.$button_edit.'</a>';
$manage_record_bar = '<a href="javascript:checkRemove(document.form1,\'CourseID[]\',\'remove.php\')" class="tool_delete">'.$button_delete.'</a>';

$li->field_array = array("Title", "Description", "CategoryName", "NumberOfLesson");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$li->IsColOff = "IP25_table";
$li->column_array = array(0,18,0,0);

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='25%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['course']['title'])."</th>\n";
$li->column_list .= "<th width='50%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['course']['description'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['course']['category'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['course']['numberOfLesson'])."</th>\n";
$li->column_list .= $checkbox_col;

?>

<form name="form1" id="form1" method="POST" action="index.php">

    <table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
    		<td align="center">
    			<table width="100%" border="0" cellspacing="0" cellpadding="0">
    				<tr>
    					<td class="navigation"><?php echo $linterface->GET_NAVIGATION($PAGE_NAVIGATION); ?></td>
    				</tr>
    			</table>
    			<table width="100%" border="0" cellpadding="0" cellspacing="0">
    				<tr>
    					<td>
    						<table align="center" class="form_table_v30">
    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['curriculum']['title'];?></td>
    								<td class="tabletext"><?php echo intranet_htmlspecialchars($curriculumTitle);?></td>
    							</tr>
    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['curriculum']['description'];?></td>
    								<td class="tabletext"><?php echo nl2br(intranet_htmlspecialchars($curriculumTemplate['Description']));?></td>
    							</tr>
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    </table>
		
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%"><?=$toolbar ?></td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%">&nbsp;</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">
				<div class="table_filter">&nbsp;</div> 
			</td>
			<td valign="bottom">
				<div class="common_table_tool">
					<?=$manage_record_bar?>					
				</div>
			</td>
		</tr>
	</table>
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?=$li->display()?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />
	<input type="hidden" name="TemplateID" value="<?php echo $TemplateID;?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>