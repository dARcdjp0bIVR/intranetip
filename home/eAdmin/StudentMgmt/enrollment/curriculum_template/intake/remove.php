<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-13 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
 
$result = array();
$IntakeID = IntegerSafe($IntakeID);

if (count($IntakeID) > 0) {
    $sql = "SELECT ClassID FROM INTRANET_ENROL_INTAKE_CLASS WHERE IntakeID IN ('".implode("','",(array)$IntakeID)."') ORDER BY ClassID";
    $classAry = $libenroll->returnVector($sql);

    $libenroll->Start_Trans();

    if (count($classAry)) {
        $sql = "DELETE FROM INTRANET_ENROL_INTAKE_INSTRUCTOR WHERE ClassID IN ('".implode("','",(array)$classAry)."')";
        $result[] = $libenroll->db_db_query($sql);
    }
    
    $sql = "DELETE FROM INTRANET_ENROL_INTAKE_CLASS WHERE IntakeID IN ('".implode("','",(array)$IntakeID)."')";
    $result[] = $libenroll->db_db_query($sql);
    
    $sql = "DELETE FROM INTRANET_ENROL_INTAKE WHERE IntakeID IN ('".implode("','",(array)$IntakeID)."')";
    $result[] = $libenroll->db_db_query($sql);
    
}

if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'DeleteSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'DeleteUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>