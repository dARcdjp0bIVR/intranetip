<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-12 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$linterface = new libclubsenrol_ui();
$classListLayout = $linterface->getIntakeClassTable();

$defaultEndDate = date('Y-m-d',strtotime('+'.$enrolConfigAry['CurriculumTemplate']['CourseLength'].' day'));
$_rowCounter = 1;

# Page heading setting

$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtIntake";

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eEnrolment']['curriculumTemplate']['intake']['intake']);

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['Btn']['New']. ' ' . $Lang['eEnrolment']['curriculumTemplate']['intake']['intake'], "");

$linterface->LAYOUT_START();

$form_action = "new_update.php";
include("intake.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>