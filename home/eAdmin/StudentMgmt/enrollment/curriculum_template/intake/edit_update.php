<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-13 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$result = array();

$libenroll->Start_Trans();

$classesToDelete = $_POST['classesToDelete'];
if (!empty($classesToDelete)) {
    $classesToDelete = substr($classesToDelete,1,strlen($classesToDelete));     // remove first comma
    $classToDeleteAry = explode(',',$classesToDelete);
    $classToDeleteAry = IntegerSafe($classToDeleteAry);
    $classesToDelete = implode("','", $classToDeleteAry);
    
    // Step 1. delete classes instructor
    $sql = "DELETE FROM INTRANET_ENROL_INTAKE_INSTRUCTOR WHERE ClassID IN ('".$classesToDelete."')";
    $result[] = $libenroll->db_db_query($sql);
    
    // Step 2. delete classes
    $sql = "DELETE FROM INTRANET_ENROL_INTAKE_CLASS WHERE ClassID IN ('".$classesToDelete."')";
    $result[] = $libenroll->db_db_query($sql);
}


// Step 3: update intake
$dataAry = array();
$dataAry['Title'] = standardizeFormPostValue($_POST['Title']);
$dataAry['StartDate'] = $_POST['StartDate'];
$dataAry['EndDate'] = $_POST['EndDate'];
$dataAry['LastModifiedBy'] = $_SESSION['UserID'];
$intakeID = IntegerSafe($_POST['IntakeID']);
$sql = $libenroll->UPDATE2TABLE('INTRANET_ENROL_INTAKE',$dataAry,array('IntakeID'=>$intakeID),false);
$result[] = $libenroll->db_db_query($sql);

$classIDAry = IntegerSafe($_POST['ClassID']);
$classNameAry = $_POST['ClassName'];

$instructors = $_POST['Instructor'];
foreach((array)$instructors as $_instructor) {
    $instructorAry[] = IntegerSafe($_instructor);
}

$numberOfStudentAry = IntegerSafe($_POST['NumberOfStudent']);
$locationAry = IntegerSafe($_POST['Location']);

// Step 4: update / insert classes
$numberOfClass = count($classNameAry);
for($i=0;$i<$numberOfClass;$i++) {
    unset($dataAry);
    $dataAry['IntakeID'] = $intakeID;
    $dataAry['ClassName'] = standardizeFormPostValue($classNameAry[$i]);
    $dataAry['LocationID'] = $locationAry[$i];
    $dataAry['NumberOfStudent'] = $numberOfStudentAry[$i];
    $dataAry['LastModifiedBy'] = $_SESSION['UserID'];
    
    $classID = $classIDAry[$i];
    
    if ($classID) {     // update
        $sql = $libenroll->UPDATE2TABLE('INTRANET_ENROL_INTAKE_CLASS',$dataAry,array('ClassID'=>$classID),false);
        $result[] = $libenroll->db_db_query($sql);
    }
    else {      // insert
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_INTAKE_CLASS',$dataAry,array(),false);
        $res = $libenroll->db_db_query($sql);
        $result[] = $res;
        $classID = $libenroll->db_insert_id();
    }
    
    // Step 5: delete instructor first
    $sql = "DELETE FROM INTRANET_ENROL_INTAKE_INSTRUCTOR WHERE ClassID='".$classID."'";
    $result[] = $libenroll->db_db_query($sql);
    
    // Step 6:
    $instructorAry[$i] = array_unique((array)$instructorAry[$i]);      // remove duplicate instructor
    foreach((array)$instructorAry[$i] as $_instructorID) {
        if ($_instructorID) {
            unset($dataAry);
            $dataAry['ClassID'] = $classID;
            $dataAry['InstructorID'] = $_instructorID;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_INTAKE_INSTRUCTOR',$dataAry,array(),false, false, false);
            $result[] = $libenroll->db_db_query($sql);
        }
    }
}


if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'UpdateSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'UpdateUnsuccess';
}

header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>