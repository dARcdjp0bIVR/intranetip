<?php
/*
 *  Using:
 *  
 *  
 * 	2018-07-13 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if (is_array($IntakeID)) {      // from $_POST
    $intakeID = (count($IntakeID) == 1) ? $IntakeID[0] : '';
} else {    // from $_GET
    $intakeID = $IntakeID;
}
$intakeID = IntegerSafe($intakeID);

if (! $intakeID) {
    header("location: index.php");
}

$intakeAssoc = $libenroll->getIntake($intakeID);
//debug_pr($intakeAssoc);
$intake = $intakeAssoc[$intakeID];   
$classAssoc = $intake['ClassAry'];

$linterface = new libclubsenrol_ui();
$classListLayout = '';
$_rowCounter = 0;
foreach($classAssoc as $_classID => $_classAssoc) {
    $classListLayout .= $linterface->getIntakeClassTable($_classAssoc, $_rowCounter++);
}


# Page heading setting

$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtIntake";

### Title ###
$TAGS_OBJ = array();
$TAGS_OBJ[] = array($Lang['eEnrolment']['curriculumTemplate']['intake']['intake']);

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']. ' ' . $Lang['eEnrolment']['curriculumTemplate']['intake']['intake'], "");

$linterface->LAYOUT_START();

$form_action = "edit_update.php";
include("intake.tmpl.php");

$linterface->LAYOUT_STOP();
intranet_closedb();
?>