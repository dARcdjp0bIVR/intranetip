<?php
/*
 *  Using:
 *  
 *  2019-02-22 Cameron
 *  - add parameter ENROL_TYPE_ACTIVITY to UpdateEnrolEbookingRelation()
 *   
 *  2018-11-16 [Cameron]
 *  - fix: should use HidClassCombination instead of CombinedLessonInstructor in loop for adding combined lesson record because instructor could be empty at this stage
 *  
 *  2018-10-08 [Cameron] 
 *  - add table INTRANET_ENROL_EVENT_INTAKE_CLASS to store course-class info 
 *  - add INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS to store lesson-class info
 *  
 *  2018-09-04 [Cameron] set time_limit and unlimit memory(memory_limit)
 *  
 * 	2018-07-19 [Cameron] create this file
 */

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}
 
$result = array();
$dataAry = array();

$templateID = IntegerSafe($_POST['TemplateID']);
$intakeID = IntegerSafe($_POST['Intake']);
$courseTitleAry = $_POST['CourseTitle'];
$courseCodeAry = $_POST['CourseCode'];
$courseDescriptionAry = $_POST['CourseDescription'];
$lessonTitleAry = $_POST['LessonTitle'];
$locationAry = $_POST['Location'];
$isCombinedCourseAry = $_POST['IsCombinedCourse'];
$startHourAry = $_POST['StartHour'];
$startMinuteAry = $_POST['StartMinute'];
$endHourAry = $_POST['EndHour'];
$endMinuteAry = $_POST['EndMinute'];
$instructorAry = $_POST['Instructor'];

$classCourseTitleAry = $_POST['ClassCourseTitle'];
$classCourseCodeAry = $_POST['ClassCourseCode'];
$classCourseDescriptionAry = $_POST['ClassCourseDescription'];
$classLessonTitleAry = $_POST['ClassLessonTitle'];
$classInstructorAry = $_POST['ClassInstructor'];

$combinedLessonCourseTitleAry = $_POST['CombinedLessonCourseTitle'];
$combinedLessonCourseCodeAry = $_POST['CombinedLessonCourseCode'];
$combinedLessonCourseDescriptionAry = $_POST['CombinedLessonCourseDescription'];
$combinedLessonTitleAry = $_POST['CombinedLessonTitle'];
$combinedLessonInstructorAry = $_POST['CombinedLessonInstructor'];
$combinedLessonClassAry = $_POST['CombinedLessonClass'];
$combinedLessonHidClassCombinationAry = $_POST['HidClassCombination'];


$academicYearID = Get_Current_Academic_Year_ID();

// debug_pr($_POST);
// exit;

$courseTemplateAry = $libenroll->getCourseTemplate($courseID='', $templateID);
$courseTemplateAssoc= BuildMultiKeyAssoc($courseTemplateAry, array('CourseID'), array(), 1);
unset($courseTemplateAry);

$locationAssoc = $libenroll->getLocationList();

$intakeAry = $libenroll->getIntakeList($intakeID);
$intakeStartDate = count($intakeAry) ? $intakeAry[0]['StartDate'] : date('Y-m-d');

$intakeClassAssoc = $libenroll->getIntakeClass($intakeID);

// Step 1. add record to INTRANET_GROUP
$dataAry['Title'] = standardizeFormPostValue($_POST['TitleEnglish']);
$dataAry['TitleChinese'] = standardizeFormPostValue($_POST['TitleChinese']);
$dataAry['RecordType'] = '5';   // extra curriculum activities (ECA)
$dataAry['AcademicYearID'] = $academicYearID;
$dataAry['DateInput'] = 'now()'; 
$sql = $libenroll->INSERT2TABLE('INTRANET_GROUP',$dataAry,array(),false);

$libenroll->Start_Trans();

$res = $libenroll->db_db_query($sql);
$result['AddGroup'] = $res;
if ($res) {
    $groupID = $libenroll->db_insert_id();

    // Step 2: add record to INTRANET_ENROL_GROUPINFO_MAIN
    unset($dataAry);
    $dataAry['GroupID'] = $groupID;
    $dataAry['ClubType'] = 'Y';             // year-based
    $dataAry['ApplyOnceOnly'] = '0';
    $dataAry['FirstSemEnrolOnly'] = '1';    // for year-based club
    $dataAry['TemplateID'] = $templateID;
    $dataAry['IntakeID'] = $intakeID;
    $dataAry['DateInput'] = 'now()';
    $dataAry['InputBy'] = $_SESSION['UserID'];
    $dataAry['ModifiedBy'] = $_SESSION['UserID'];
    $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_GROUPINFO_MAIN',$dataAry,array(),false);
    $res = $libenroll->db_db_query($sql);
    $result['AddGroupMain'] = $res;
    if ($res) {
        $groupMainInfoID = $libenroll->db_insert_id();
    }
    
    // Step 3: add record to INTRANET_ENROL_GROUPINFO
    $res = $libenroll->Insert_Year_Based_Club($groupID);
    $result['AddClub'] = $res;
    if ($res) {
        $enrolGroupID = $libenroll->db_insert_id();
    }

    ///////////////////////////////////////////////////////////////
    // Combined Course
    
    // Step 4: add combined course record to INTRANET_ENROL_EVENTINFO 
    foreach((array)$courseTitleAry as $_courseID => $_courseTitle) {
        $_courseTitle = standardizeFormPostValue($_courseTitle);
        unset($dataAry);
        $dataAry['EventTitle'] = $_courseTitle;
        $dataAry['EventCategory'] = $courseTemplateAssoc[$_courseID]['CategoryID'];
        $dataAry['EnrolGroupID'] = $enrolGroupID;
        $dataAry['AcademicYearID'] = $academicYearID;
        $dataAry['ActivityCode'] = standardizeFormPostValue($courseCodeAry[$_courseID]);
        $dataAry['Description'] = standardizeFormPostValue($courseDescriptionAry[$_courseID]);
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENTINFO',$dataAry,array(),false);
        $res = $libenroll->db_db_query($sql);
        $result["AddEventInfo_{$_courseID}"] = $res;
        if ($res) {
            $enrolEventID = $libenroll->db_insert_id();

            $requestAry = array();
            $i = 0;
            
            // Step 5: add record to INTRANET_ENROL_EVENT_DATE
            foreach((array)$lessonTitleAry[$_courseID] as $__lesson) {

                $lessonDateName = 'LessonDate_'.$_courseID.'_'.$i;
                $lessonDate = $_POST[$lessonDateName];
                $startHour = str_pad(IntegerSafe($startHourAry[$_courseID][$i]), 2, "0", STR_PAD_LEFT);
                $startMinute = str_pad(IntegerSafe($startMinuteAry[$_courseID][$i]), 2, "0", STR_PAD_LEFT);
                $activityDateStart = $lessonDate.' '.$startHour.':'.$startMinute.':00';
                $endHour = str_pad(IntegerSafe($endHourAry[$_courseID][$i]), 2, "0", STR_PAD_LEFT);
                $endMinute = str_pad(IntegerSafe($endMinuteAry[$_courseID][$i]), 2, "0", STR_PAD_LEFT);
                $activityDateEnd = $lessonDate.' '.$endHour.':'.$endMinute.':00';
                
                $locationID = $locationAry[$_courseID][$i];
//                $location = $locationAssoc[$locationID]['Location'];     // Location Name

                unset($dataAry);
                $dataAry['EnrolEventID'] = $enrolEventID;
                $dataAry['ActivityDateStart'] = $activityDateStart;
                $dataAry['ActivityDateEnd'] = $activityDateEnd;                
                $dataAry['LocationID'] = $locationID;     // store locationID instead of location name for HKPF 
                $dataAry['Lesson'] = standardizeFormPostValue($__lesson);
                $dataAry['RecordStatus'] = $enrolConfigAry['MeetingDate_RecordStatus']['Active'];
                $dataAry['DateInput'] = 'now()';
                $dataAry['InputBy'] = $_SESSION['UserID'];
                $dataAry['ModifiedBy'] = $_SESSION['UserID'];
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE',$dataAry,array(),false);
                $res = $libenroll->db_db_query($sql);
                $result["AddEventDate_{$_courseID}_{$i}"] = $res;
                if ($res) {
                    $eventDateID = $libenroll->db_insert_id();
                    
                    $instructorAry[$_courseID][$i] = array_unique((array)$instructorAry[$_courseID][$i]);      // remove duplicate instructor
                    
                    // Step 6: add record to INTRANET_ENROL_EVENT_DATE_TRAINER
                    foreach((array)$instructorAry[$_courseID][$i] as $___instructor) {
                        if ($___instructor) {       // filter out empty record
                            unset($dataAry);
                            $dataAry['EventDateID'] = $eventDateID;
                            $dataAry['TrainerID'] = $___instructor;
                            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_TRAINER',$dataAry,array(),false, false, false);
                            $result["AddTrainer_{$_courseID}_{$i}"] = $libenroll->db_db_query($sql);
                        }
                    }
                    
                    // Step 7: prepare adding booking records:
                    $requestAry = array();
                    $bookingAry = array();
                    $startTime = $startHour.':'.$startMinute.':00';
                    $endTime = $endHour.':'.$endMinute.':00';
                    $bookingAry['BookingType'] = 'room';
                    $bookingAry['Date'] = $lessonDate;
                    $bookingAry['StartTime'] = $startTime;
                    $bookingAry['EndTime'] = $endTime;
                    $bookingAry['RoomID'] = $locationID;
                    $requestAry['BookingDetails'][$i] = $bookingAry;
                 
                    if (($endTime > $startTime) && $locationID) {
                        // Step 8: add booking record
                        $requestAry['BookingFrom'] = 'eEnrol';
                        $libebooking_api = new libebooking_api();
                        $bookingResultAry = $libebooking_api->RequestBooking($requestAry);
                        if (count($bookingResultAry)) {
                            $bookingResult = array_pop($bookingResultAry);  // one element
                            $result["AddBooking_{$_courseID}_{$i}"] = $bookingResult['Status'];
                            $bookingID = $bookingResult['BookingID'];
                            $result["AddBookingRelation_{$_courseID}_{$i}"] = $libenroll->UpdateEnrolEbookingRelation($eventDateID, $bookingID, ENROL_TYPE_ACTIVITY);
                        }
                    }
                    
                    
                    // Step 9: add record to lesson-class
                    foreach((array)$intakeClassAssoc as $___classID => $___lessonClass) {
                        if ($___classID) {
                            unset($dataAry);
                            $dataAry['EventDateID'] = $eventDateID;
                            $dataAry['IntakeClassID'] = $___classID;
                            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS',$dataAry,array(),false, false, false);
                            $result["AddCombinedCourseLessonClass_{$_courseID}_{$eventDateID}_{$___classID}"] = $libenroll->db_db_query($sql);
                            
                            // Step 10: add course-class
                            unset($dataAry);
                            $dataAry['EnrolEventID'] = $enrolEventID;
                            $dataAry['IntakeClassID'] = $___classID;
                            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_INTAKE_CLASS',$dataAry,array(),false, true, false);
                            $result["AddCombinedCourseCourseClass_{$_courseID}}_{$enrolEventID}_{$___classID}"] = $libenroll->db_db_query($sql);
                            
                        }
                    }
                    
                }
                
                $i++;
            }       // end loop for each lesson
            
        }   // EnrolEventID create successfully 
        
    }   // end loop for each course
 

    
    $courseEnrolAssoc = array();        // use to find EnrolEventID by look up of template course id

    ///////////////////////////////////////////////////////////////
    // Combined Lesson
    
    // Step 11: add combined lesson course record to INTRANET_ENROL_EVENTINFO
    $intakeClassID = -1;    // denotes combined lesson
    foreach((array)$combinedLessonCourseTitleAry as $_courseID => $_courseTitle) {
        
        $_courseTitle = standardizeFormPostValue($_courseTitle);
        unset($dataAry);
        $dataAry['EventTitle'] = $_courseTitle;
        $dataAry['EventCategory'] = $courseTemplateAssoc[$_courseID]['CategoryID'];
        $dataAry['EnrolGroupID'] = $enrolGroupID;
        $dataAry['AcademicYearID'] = $academicYearID;
        $dataAry['ActivityCode'] = standardizeFormPostValue($combinedLessonCourseCodeAry[$_courseID]);
        $dataAry['Description'] = standardizeFormPostValue($combinedLessonCourseDescriptionAry[$_courseID]);
        $dataAry['IntakeClassID'] = $intakeClassID;
        $dataAry['DateInput'] = 'now()';
        $dataAry['InputBy'] = $_SESSION['UserID'];
        $dataAry['ModifiedBy'] = $_SESSION['UserID'];
        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENTINFO',$dataAry,array(),false);
        $res = $libenroll->db_db_query($sql);
        $result["AddCombinedLessonEventInfo_{$_courseID}"] = $res;
        
        if ($res) {
            $enrolEventID = $libenroll->db_insert_id();
            $courseEnrolAssoc[$_courseID] = $enrolEventID;

            // Step 12: add record to INTRANET_ENROL_EVENT_DATE ( event date use the intake start date)
            foreach((array)$combinedLessonTitleAry[$_courseID] as $__lessonID=> $__lesson) {
                $lessonDate = $intakeStartDate;
                $activityDateStart = $intakeStartDate.' 00:00:00';
                $activityDateEnd = $activityDateStart;
                
                foreach((array)$combinedLessonHidClassCombinationAry[$_courseID][$__lessonID] as $___instructorRow=>$___val) {
                    unset($dataAry);
                    $dataAry['EnrolEventID'] = $enrolEventID;
                    $dataAry['ActivityDateStart'] = $activityDateStart;
                    $dataAry['ActivityDateEnd'] = $activityDateEnd;
                    $dataAry['Lesson'] = standardizeFormPostValue($__lesson);
                    $dataAry['DateInput'] = 'now()';
                    $dataAry['InputBy'] = $_SESSION['UserID'];
                    $dataAry['ModifiedBy'] = $_SESSION['UserID'];
                    $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE',$dataAry,array(),false);
                    $res = $libenroll->db_db_query($sql);
                    $result["AddCombinedLessonEventDate_{$_courseID}_{$__lessonID}"] = $res;

                    if ($res) {
                        $eventDateID = $libenroll->db_insert_id();
                        
                        $___instructorAry = $combinedLessonInstructorAry[$_courseID][$__lessonID][$___instructorRow];
                        $___instructorAry = array_unique((array)$___instructorAry);      // remove duplicate instructor
                        
                        // Step 13: add record to INTRANET_ENROL_EVENT_DATE_TRAINER
                        $instructorCounter = 0;
                        foreach((array)$___instructorAry as $____instructor) {
                            if ($____instructor) {       // filter out empty record
                                unset($dataAry);
                                $dataAry['EventDateID'] = $eventDateID;
                                $dataAry['TrainerID'] = $____instructor;
                                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_TRAINER',$dataAry,array(),false, false, false);
                                $result["AddCombinedLessonTrainer_{$_courseID}_{$__lessonID}_{$___instructorRow}_{$instructorCounter}"] = $libenroll->db_db_query($sql);
                                $instructorCounter++;
                            }
                        }
                        
                        // Step 14: add record to lesson-class
                        foreach((array)$combinedLessonClassAry[$_courseID][$__lessonID][$___instructorRow] as $____classID => $____lessonClass) {
                            if ($____lessonClass) {
                                unset($dataAry);
                                $dataAry['EventDateID'] = $eventDateID;
                                $dataAry['IntakeClassID'] = $____classID;
                                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS',$dataAry,array(),false, false, false);
                                $result["AddCombinedLessonClass_{$_courseID}_{$__lessonID}_{$___instructorRow}_{$____classID}"] = $libenroll->db_db_query($sql);
                                
                                // Step 15: add course-class
                                unset($dataAry);
                                $dataAry['EnrolEventID'] = $enrolEventID;
                                $dataAry['IntakeClassID'] = $____classID;
                                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_INTAKE_CLASS',$dataAry,array(),false, true, false);
                                $result["AddCourseClass_{$_courseID}_{$____classID}"] = $libenroll->db_db_query($sql);
                            }
                        }
                    }
                }       // end loop for class combination
                
            }           // end loop for each lesson
            
        }   // EnrolEventID create successfully
    }   // end loop for each course
    
    
    
    ///////////////////////////////////////////////////////////////
    // Class Course
 
    // Step 16: add class course record to INTRANET_ENROL_EVENTINFO
    foreach((array)$classCourseTitleAry as $intakeClassID => $courseTitleAry) {
        
        foreach((array)$courseTitleAry as $_courseID => $_courseTitle) {
            
            if ($courseEnrolAssoc[$_courseID]) {
                $enrolEventID = $courseEnrolAssoc[$_courseID];      // course already created
            }
            else {
                $_courseTitle = standardizeFormPostValue($_courseTitle);
                unset($dataAry);
                $dataAry['EventTitle'] = $_courseTitle;
                $dataAry['EventCategory'] = $courseTemplateAssoc[$_courseID]['CategoryID'];
                $dataAry['EnrolGroupID'] = $enrolGroupID;
                $dataAry['AcademicYearID'] = $academicYearID;
                $dataAry['ActivityCode'] = standardizeFormPostValue($classCourseCodeAry[$intakeClassID][$_courseID]);
                $dataAry['Description'] = standardizeFormPostValue($classCourseDescriptionAry[$intakeClassID][$_courseID]);
                $dataAry['IntakeClassID'] = $intakeClassID;
                $dataAry['DateInput'] = 'now()';
                $dataAry['InputBy'] = $_SESSION['UserID'];
                $dataAry['ModifiedBy'] = $_SESSION['UserID'];
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENTINFO',$dataAry,array(),false);
                $res = $libenroll->db_db_query($sql);
                $result["AddClassEventInfo_{$intakeClassID}_{$_courseID}"] = $res;
                if ($res) {
                    $enrolEventID = $libenroll->db_insert_id();
                    $courseEnrolAssoc[$_courseID] = $enrolEventID;
                }
            }
            
            
            $i = 0;
            // Step 17: add record to INTRANET_ENROL_EVENT_DATE ( event date use the intake start date)
            foreach((array)$classLessonTitleAry[$intakeClassID][$_courseID] as $__lesson) {
                $lessonDate = $intakeStartDate;
                $activityDateStart = $intakeStartDate.' 00:00:00';
                $activityDateEnd = $activityDateStart;

                unset($dataAry);
                $dataAry['EnrolEventID'] = $enrolEventID;
                $dataAry['ActivityDateStart'] = $activityDateStart;
                $dataAry['ActivityDateEnd'] = $activityDateEnd;
                $dataAry['Lesson'] = standardizeFormPostValue($__lesson);
                $dataAry['DateInput'] = 'now()';
                $dataAry['InputBy'] = $_SESSION['UserID'];
                $dataAry['ModifiedBy'] = $_SESSION['UserID'];
                $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE',$dataAry,array(),false);
                $res = $libenroll->db_db_query($sql);
                $result["AddClassEventDate_{$intakeClassID}_{$_courseID}_{$i}"] = $res;
                if ($res) {
                    $eventDateID = $libenroll->db_insert_id();
                    
                    $classInstructorAry[$intakeClassID][$_courseID][$i] = array_unique((array)$classInstructorAry[$intakeClassID][$_courseID][$i]);      // remove duplicate instructor
                    
                    // Step 18: add record to INTRANET_ENROL_EVENT_DATE_TRAINER
                    foreach((array)$classInstructorAry[$intakeClassID][$_courseID][$i] as $___instructor) {
                        if ($___instructor) {       // filter out empty record
                            unset($dataAry);
                            $dataAry['EventDateID'] = $eventDateID;
                            $dataAry['TrainerID'] = $___instructor;
                            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_TRAINER',$dataAry,array(),false, false, false);
                            $result["AddClassTrainer_{$intakeClassID}_{$_courseID}_{$i}"] = $libenroll->db_db_query($sql);
                        }
                    }
                    
                    // Step 19: add lesson-class
                    unset($dataAry);
                    $dataAry['EventDateID'] = $eventDateID;
                    $dataAry['IntakeClassID'] = $intakeClassID;
                    $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_DATE_INTAKE_CLASS',$dataAry,array(),false, true, false);
                    $result["AddNonCombinedLessonClass_{$intakeClassID}_{$_courseID}_{$eventDateID}"] = $libenroll->db_db_query($sql);
                    
                }
                
                $i++;
            }       // end loop for each lesson
            
            // Step 20: add course-class
            unset($dataAry);
            $dataAry['EnrolEventID'] = $enrolEventID;
            $dataAry['IntakeClassID'] = $intakeClassID;
            $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_EVENT_INTAKE_CLASS',$dataAry,array(),false, true, false);
            $result["AddCourseClass_{$_courseID}_{$intakeClassID}"] = $libenroll->db_db_query($sql);
            
        }   // end loop for each course
        
    }   // end loop for each class 
    
    
}

if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $returnMsgKey = 'generateSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $returnMsgKey = 'generateUnSuccess';
}

// debug_pr($result);
// debug_pr($returnMsgKey);
header("location: index.php?returnMsgKey=".$returnMsgKey);

intranet_closedb();
?>