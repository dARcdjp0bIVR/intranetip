<?php
/*
 *  Using:
 *
 *  Purpose:    generate curriculum from template
 *  
 *  2019-02-20 [Cameron] break into two steps to avoid ERR_CONNECTION_RESET error when change intake via ajax: select intake and others
 *  
 *  2018-11-21 [Cameron] disable isIntakeTimeConflict checking when generate (because client use intake start date for all combined course date, they will change later)
 *  
 *  2018-11-07 [Cameron] change lang from $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflict'] to $Lang['eEnrolment']['curriculumTemplate']['warning']['instructorIsUnavailable']
 *          in error checking before submit
 *          
 *  2018-10-24 [Cameron] handle intake time conflict (don't allow to process if combined course time is conflict with existing record with the same IntakeID)
 *  
 *  2018-10-05 [Cameron] add getCombinedLessonLayout(), addTemplateClassInstructor(), removeCombinedLessonInstructor()
 *
 *  2018-09-14 [Ivan] added getInstructorApplyToAllShortCutLayout() table, added javascript function applyCategoryClassInstructorToAllCourses()
 *
 *  2018-09-04 [Cameron] set time_limit and unlimit memory(memory_limit)
 *  
 *  2018-08-17 [Cameron] add time overlap checking in checkForm()
 *  
 * 	2018-07-17 [Cameron] create this file
 */

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if (is_array($TemplateID)) {
    $templateID = (count($TemplateID) == 1) ? $TemplateID[0] : '';
} else {
    $templateID = $TemplateID;
}
$templateID = IntegerSafe($templateID);

if (! $templateID) {
    header("location: index.php");
}

$curriculumTemplateAry = $libenroll->getCurriculumTemplate($templateID);

if (count($curriculumTemplateAry)) {
    $curriculumTemplate = $curriculumTemplateAry[0];
    $curriculumTitleChinese = $curriculumTemplate['TitleChinese'];
    $curriculumTitleEnglish = $curriculumTemplate['TitleEnglish'];
    $templateCurriculumTitle = Get_Lang_Selection($curriculumTitleChinese, $curriculumTitleEnglish);
}

$linterface = new libclubsenrol_ui();
$intakeSelection = $linterface->getIntakeSelection();


$intakeList = $libenroll->getIntakeList();
if (count($intakeList)){
    $intakeID = $intakeList[0]['IntakeID'];     // get latest intakeID as default
    $intakeStartDate = $intakeList[0]['StartDate'];
}
else {
    $intakeID = '';
    $intakeStartDate = '';
}


# Page heading setting
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtCurriculumTemplate";

### Title ###
$TAGS_OBJ = $libenroll->getCurriculumTemplateTabs("template");

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['curriculumTemplate']['generateCurriculum'], "");
$linterface->LAYOUT_START();

# step information
$STEPS_OBJ[] = array($Lang['eEnrolment']['curriculumTemplate']['generateStep1'], 1);
$STEPS_OBJ[] = array($Lang['eEnrolment']['curriculumTemplate']['generateStep2'], 0);

?>

<script type="text/javascript">

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
 	
	if ($.trim($('select#Intake').val()) == '') {
 		error++; 	 		
 		if (focusField == '') focusField = 'Intake';
 		$('span#ErrIntake').addClass('error_msg_show').removeClass('error_msg_hide');
	} 	
 	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function hideError() 
{
	$('span.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

$(document).ready(function(){

	$('input#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

		if (checkForm(document.form1)) {
			$('form#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});
});


function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

</script>

<form name="form1" id="form1" method="POST" action="generate_step2.php">

    <table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
    		<td align="center">
    			<table width="100%" border="0" cellspacing="0" cellpadding="0">
    				<tr>
    					<td class="navigation"><?php echo $linterface->GET_NAVIGATION($PAGE_NAVIGATION); ?></td>
    				</tr>
    			</table>
    			
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
                		<td><?php echo $linterface->GET_STEPS($STEPS_OBJ); ?></td>
                	</tr>
				</table>    			

    			<table width="100%" border="0" cellpadding="0" cellspacing="0">
    				<tr>
    					<td>
    						<table align="center" class="form_table_v30">
    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['tabs']['curriculumTemplate'];?></td>
    								<td class="tabletext"><?php echo intranet_htmlspecialchars($templateCurriculumTitle);?></td>
    							</tr>

    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['intake'];?></td>
    								<td class="tabletext"><?php echo $intakeSelection;?></td>
    							</tr>
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	
		<tr><td>&nbsp;</td></tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="tabletextremark"><?php echo $i_general_required_field;?></td>
		</tr>
		
		<tr>
			<td height="1" class="dotline"><img src="<?php echo "{$image_path}/{$LAYOUT_SKIN}"; ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		
		<tr><td>&nbsp;</td></tr>
		    	
		<tr>
			<td align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center">
							<span>
								<?php echo $linterface->GET_ACTION_BTN($Lang['Btn']['Continue'], "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
								<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>
							</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
    	
    </table>
		

	<input type="hidden" name="TemplateID" id="TemplateID" value="<?php echo $templateID;?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>