<?php
/*
 *  Using:
 *
 *  Purpose:    show generated curriculum list ( who / when generate from which template, what intake)
 *
 *  2018-08-17 [Cameron] add Intake Start Date column
 *  
 * 	2018-08-09 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");

# set cookies
$arrCookies[] = array("ck_page_size", "numPerPage");
$arrCookies[] = array("ck_page_no", "pageNo");
$arrCookies[] = array("ck_page_order", "order");
$arrCookies[] = array("ck_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
    clearCookies($arrCookies);
}
else
{
    updateGetCookies($arrCookies);
}

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

# Page heading setting
$linterface = new interface_html();
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtCurriculumTemplate";

### Title ###
$TAGS_OBJ = $libenroll->getCurriculumTemplateTabs("generated");

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

$pageNo = ($pageNo == "") ? 1 : $pageNo;
$order = ($order == "") ? 1 : $order;		// default ascending
$field = ($field == "") ? 0 : $field;

$li = new libdbtable2007($field, $order, $pageNo);

$cond = '';
$templateID = IntegerSafe($_POST['TemplateID']);
$keyword = $_POST['keyword'] ? standardizeFormPostValue($_POST['keyword']) : standardizeFormGetValue($keyword);
if($keyword!="")
{
    $kw = $li->Get_Safe_Sql_Like_Query($keyword);
    $cond .= " AND (t.TitleChinese LIKE '%$kw%' OR t.TitleEnglish LIKE '%$kw%' OR g.TitleChinese LIKE '%$kw%' OR g.Title LIKE '%$kw%')";
    unset($kw);
}

if ($templateID) {
    $cond .= " AND m.TemplateID='".$templateID."'";
}


$templateName = Get_Lang_Selection("t.TitleChinese","t.TitleEnglish");
$curriculumName = Get_Lang_Selection("g.TitleChinese","g.Title");
$generatedBy = getNameFieldWithClassNumberByLang("u.", $sys_custom['hideTeacherTitle']);

$sql = "SELECT
                ".$templateName." AS TemplateName, 
                ".$curriculumName." AS CurriculumName,
                ".$generatedBy." AS GeneratedBy,
                m.DateInput AS GeneratedOn,
                i.Title AS Intake,
                i.StartDate
        FROM
                INTRANET_ENROL_GROUPINFO_MAIN m
        INNER JOIN
                INTRANET_GROUP g ON g.GroupID=m.GroupID
        INNER JOIN
                INTRANET_ENROL_INTAKE i ON i.IntakeID=m.IntakeID
        INNER JOIN
                INTRANET_ENROL_CURRICULUM_TEMPLATE t ON t.TemplateID=m.TemplateID
        LEFT JOIN
                INTRANET_USER u ON u.UserID=m.InputBy
        WHERE
                m.TemplateID>0
        AND
                m.IntakeID>0".$cond;

$extra_column = 1;

$li->field_array = array("TemplateName", "CurriculumName", "GeneratedBy", "GeneratedOn", "Intake", "StartDate");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+$extra_column;
$li->title = $Lang['General']['Record'];
$li->IsColOff = "IP25_table";
$li->column_array = array(0,0,0,0,0,0);

$pos = 0;
$li->column_list .= "<th width='1' class='tabletoplink'>#</th>\n";
$li->column_list .= "<th width='22%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['curriculum']['template'])."</th>\n";
$li->column_list .= "<th width='22%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['curriculum']['title'])."</th>\n";
$li->column_list .= "<th width='16%' >".$li->column($pos++, $Lang['General']['CreatedBy'])."</th>\n";
$li->column_list .= "<th width='12%' >".$li->column($pos++, $Lang['General']['RecordDate'])."</th>\n";
$li->column_list .= "<th width='16%' >".$li->column($pos++, $Lang['eEnrolment']['curriculumTemplate']['intake']['title'])."</th>\n";
$li->column_list .= "<th width='10%' >".$li->column($pos++, $Lang['General']['StartDate'])."</th>\n";

$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);

# Template Filter
$templateAry = $libenroll->getCurriculumTemplate();
$templateTitle = Get_Lang_Selection('TitleChinese', 'TitleEnglish');
foreach((array)$templateAry as $template) {
    $templateList[] = array($template['TemplateID'],$template[$templateTitle]);
}
$templateFilter = getSelectByArray($templateList,"name='TemplateID' onChange='this.form.submit();'",$templateID,0,0,$Lang['eEnrolment']['curriculumTemplate']['allTemplate']);

?>

<form name="form1" id="form1" method="POST" action="generated_curriculum.php">
	<div class="content_top_tool">
		
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="20%">&nbsp;</td>
				<td width="50%" align="center">&nbsp;</td>
				<td width="30%">
					<div class="content_top_tool"  style="float: right;">
						<?=$htmlAry['searchBox']?>     
						<br style="clear:both" />
					</div>
				</td>
			</tr>
		</table>
		
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">
				<div class="table_filter">
					<?php echo $templateFilter;?>
				</div> 
			</td>
			<td valign="bottom">&nbsp;</td>
		</tr>
	</table>
	</div>

	<div class="table_board">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td valign="bottom">

				<?=$li->display()?>
 
			</td>
		</tr>
	</table>
	</div>

	<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
	<input type="hidden" name="order" value="<?php echo $li->order; ?>" />
	<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
	<input type="hidden" name="page_size_change" value="" />
	<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>