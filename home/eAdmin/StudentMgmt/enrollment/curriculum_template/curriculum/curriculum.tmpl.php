<?php
/*
 * 	Log
 * 
 * 	2018-07-06 [Cameron]
 * 		create this file
 */
?>
<script type="text/javascript">

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
 	
	if($('#TitleEnglish').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'TitleEnglish';
 		$('#ErrTitleEnglish').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if($('#TitleChinese').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'TitleChinese';
 		$('#ErrTitleChinese').addClass('error_msg_show').removeClass('error_msg_hide');
 	}
	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function hideError() 
{
	$('.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

$(document).ready(function(){
	
	$('#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

		if (checkForm(document.form1)) {
   			$('#form1').submit();
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});
		
});

	
</script>

<form name="form1" id="form1" method="post" action="<?php echo $form_action;?>">
<table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="navigation"><?php echo $linterface->GET_NAVIGATION($PAGE_NAVIGATION); ?></td>
				</tr>
			</table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td>
						<table align="center" class="form_table_v30">
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameEn'];?></td>
								<td class="tabletext">
									<input type="text" name="TitleEnglish" id="TitleEnglish" class="textboxtext" value="<?php echo intranet_htmlspecialchars($curriculumTemplate['TitleEnglish']);?>">
									<span class="error_msg_hide" id="ErrTitleEnglish"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['inputTitleEnglish'];?></span>
								</td>
							</tr>

							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameCh'];?></td>
								<td class="tabletext">
									<input type="text" name="TitleChinese" id="TitleChinese" class="textboxtext" value="<?php echo intranet_htmlspecialchars($curriculumTemplate['TitleChinese']);?>">
									<span class="error_msg_hide" id="ErrTitleChinese"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['inputTitleChinese'];?></span>
								</td>
							</tr>
							
							<tr valign="top">
								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['curriculum']['description'];?></td>
								<td class="tabletext"><?php echo $linterface->GET_TEXTAREA("Description", $curriculumTemplate['Description'], 80, 10);?></td>
							</tr>
							
							<tr>
								<td valign="top" nowrap="nowrap" class="tabletextremark"><?php echo $i_general_required_field;?></td>
								<td width="80%">&nbsp;</td>
							</tr>
						</table>
						
					</td>
				</tr>
				<tr>
					<td height="1" class="dotline"><img src="<?php echo "{$image_path}/{$LAYOUT_SKIN}"; ?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td align="right">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td align="center">
									<span>
    									<?php echo $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
    									<?php echo $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
    									<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>
									</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>
		</td>
	</tr>
</table>
<input type=hidden id="TemplateID" name="TemplateID" value="<?php echo $templateID;?>">
</form>
