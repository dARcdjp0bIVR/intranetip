<?php
/*
 *  Using:
 *
 *  Purpose:    generate curriculum from template
 *
 *  2019-08-26 [Cameron] fix: jquery selector for Intake has changed from select to input in getLocationSelection() and addCombinedLessonClassInstructor()
 *
 *  2018-11-21 [Cameron] disable isIntakeTimeConflict checking when generate (because client use intake start date for all combined course date, they will change later)
 *  
 *  2018-11-07 [Cameron] change lang from $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflict'] to $Lang['eEnrolment']['curriculumTemplate']['warning']['instructorIsUnavailable']
 *          in error checking before submit
 *          
 *  2018-10-24 [Cameron] handle intake time conflict (don't allow to process if combined course time is conflict with existing record with the same IntakeID)
 *  
 *  2018-10-05 [Cameron] add getCombinedLessonLayout(), addTemplateClassInstructor(), removeCombinedLessonInstructor()
 *
 *  2018-09-14 [Ivan] added getInstructorApplyToAllShortCutLayout() table, added javascript function applyCategoryClassInstructorToAllCourses()
 *
 *  2018-09-04 [Cameron] set time_limit and unlimit memory(memory_limit)
 *  
 *  2018-08-17 [Cameron] add time overlap checking in checkForm()
 *  
 * 	2018-07-17 [Cameron] create this file
 */

@set_time_limit(21600);
@ini_set('max_input_time', 21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$TemplateID = $_POST['TemplateID'];
$intakeID = IntegerSafe($_POST['Intake']);
if (is_array($TemplateID)) {
    $templateID = (count($TemplateID) == 1) ? $TemplateID[0] : '';
} else {
    $templateID = $TemplateID;
}
$templateID = IntegerSafe($templateID);

if (! $templateID) {
    header("location: index.php");
}

$curriculumTemplateAry = $libenroll->getCurriculumTemplate($templateID);

if (count($curriculumTemplateAry)) {
    $curriculumTemplate = $curriculumTemplateAry[0];
    $curriculumTitleChinese = $curriculumTemplate['TitleChinese'];
    $curriculumTitleEnglish = $curriculumTemplate['TitleEnglish'];
    $templateCurriculumTitle = Get_Lang_Selection($curriculumTitleChinese, $curriculumTitleEnglish);
}

$linterface = new libclubsenrol_ui();
//$intakeSelection = $linterface->getIntakeSelection();


$intakeList = $libenroll->getIntakeList($intakeID);
if (count($intakeList)){
    $intakeStartDate = $intakeList[0]['StartDate'];
    $intakeTitle = $intakeList[0]['Title'];
}
else {
    $intakeStartDate = '';
    $intakeTitle = '';
}


# Page heading setting
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageMgtCurriculumTemplate";

### Title ###
$TAGS_OBJ = $libenroll->getCurriculumTemplateTabs("template");

$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['curriculumTemplate']['generateCurriculum'], "");
$linterface->LAYOUT_START();

# step information
$STEPS_OBJ[] = array($Lang['eEnrolment']['curriculumTemplate']['generateStep1'], 0);
$STEPS_OBJ[] = array($Lang['eEnrolment']['curriculumTemplate']['generateStep2'], 1);

?>

<script type="text/javascript">
var isLoading = true;
var loadingImg = '<?php echo $linterface->Get_Ajax_Loading_Image(); ?>';

//return true the two time slot is overlapped in the same date
//ts1 and te1 are ref point start and point end, ts2 and te2 are time slot to compare 
function isTimeOverlap(hr_s1, min_s1, hr_e1, min_e1, hr_s2, min_s2, hr_e2, min_e2) {
	var ts1 = str_pad(hr_s1, 2) + str_pad(min_s1, 2);
	var te1 = str_pad(hr_e1, 2) + str_pad(min_e1, 2);
	var ts2 = str_pad(hr_s2, 2) + str_pad(min_s2, 2);	// ref start
	var te2 = str_pad(hr_e2, 2) + str_pad(min_e2, 2);	// ref end

	if (ts1 == te1 && ts1 >= ts2 && ts1<=te2) {			// ref point is within compared timeslot
		return true;
	}
	else if (ts2 == te2 && ts2 >= ts1 && ts2 <= te1) {	// compared point is within ref timeslot
		return true;
	}
	else if (ts1 < te2 && te2 <= te1) {		// compared end point is within ref timslot (overlapped on right part)
		return true;
	}
	else if (ts2 >= ts1 && ts2 < te1) {		// compared start point is within ref timslot (overlapped on left part)
		return true;
	}
	else if (ts2 < ts1 && ts1 < te1 && te1 < te2) {	// compared time slot fully overlapped ref time slot
		return true;
	}
	else {
		return false;
	}
}

function checkForm(obj) 
{
	var error = 0;
	var focusField = '';
	hideError();
 	
	if($('input#TitleEnglish').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'TitleEnglish';
 		$('span#ErrTitleEnglish').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

	if($('input#TitleChinese').val()=='')  {    
 		error++;
 		if (focusField == '') focusField = 'TitleChinese';
 		$('span#ErrTitleChinese').addClass('error_msg_show').removeClass('error_msg_hide');
 	}

 	
// 	if ($.trim($('select#Intake').val()) == '') {
//  		error++; 	 		
//  		if (focusField == '') focusField = 'Intake';
//  		$('span#ErrIntake').addClass('error_msg_show').removeClass('error_msg_hide');
// 	} 	

 	$(':input[name*="CourseTitle"]').each(function(){
	    var thisID = $(this).attr('id');
	    thisID = thisID.replace(/\[/g,"\\\[");
	    thisID = thisID.replace(/\]/g,"\\\]");
 		if ($.trim($(this).val()) == '') {
 	 		error++; 	 		
 	 		if (focusField == '') focusField = thisID;
			$('span#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});

 	$(':input[name*="LessonTitle"]').each(function(){
	    var thisID = $(this).attr('id');
	    thisID = thisID.replace(/\[/g,"\\\[");
	    thisID = thisID.replace(/\]/g,"\\\]");
 		if ($.trim($(this).val()) == '') {
 	 		error++;
 	 		if (focusField == '') focusField = thisID;
			$('span#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide');
 		} 	
 	});

	$(':input[name^="StartHour"]').each(function(){
		var thisID = $(this).attr('id');
 		var ary = $(this).attr('id').split('\]\[');	// 2D array
 		var courseID = ary[0].replace('StartHour\[','');
 		var row = ary[1].replace('\]','');
 		var startHour = $(this).val();
 		var startMinute = $('select#StartMinute\\['+courseID+'\\]\\['+row+'\\]').val();
 		var endHour = $('select#EndHour\\['+courseID+'\\]\\['+row+'\\]').val();
 		var endMinute = $('select#EndMinute\\['+courseID+'\\]\\['+row+'\\]').val();

 		// check if start time <= end time
 		if (!validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
 	 		error++;
 	 		if (focusField == '') {
 	 		    thisID = thisID.replace(/\[/g,"\\\[");
 		  	    thisID = thisID.replace(/\]/g,"\\\]");
 	 	 		focusField = thisID;
 	 		}
			$('span#ErrTimeLogic_' + courseID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
 		}

 		// check if there's time conflict in the schedule
 		var refDateID = $('input#LessonDate_' +courseID+'_'+row).attr('id');
 		var refDate = $('#'+ refDateID).val();
 		var refLocationID = $('select#Location\\['+courseID+'\\]\\['+row+'\\]').val();

 		$(':input[name^="LessonDate_"]:not([id="'+refDateID+'"])').each(function(){
 	 		var cmpDate = $(this).val();
			var cmpDateID = $(this).attr('id');
	 		var cmpAry = $(this).attr('id').split('_');	// 2D array
	 		var cmpCourseID = cmpAry[1];
	 		var cmpRow = cmpAry[2];

 	 		var cmpLocationID = $('select#Location\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
			if (cmpDate == refDate && refLocationID != '' && refLocationID == cmpLocationID) {

		 		var cmpStartHour = $('select#StartHour\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
		 		var cmpStartMinute = $('select#StartMinute\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
		 		var cmpEndHour = $('select#EndHour\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();
		 		var cmpEndMinute = $('select#EndMinute\\['+cmpCourseID+'\\]\\['+cmpRow+'\\]').val();

		 		if (isTimeOverlap(cmpStartHour, cmpStartMinute, cmpEndHour, cmpEndMinute, startHour, startMinute, endHour, endMinute)) {
		 			$('span#ErrTimeLogic_' + courseID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictUnknown'];?>');
		 			$('span#ErrTimeLogic_' + cmpCourseID + '_' + cmpRow).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictUnknown'];?>');
		 			error++;
		 	 		if (focusField == '') {
		 	 	 		focusField = cmpDateID;
		 	 		}
		 		}
			}
 		});
 		
	});

 	$(':input[name^="LessonDate"]').each(function(){
	    var thisID = $(this).attr('id');
	    var thisDate = $.trim($(this).val());
	    var intakeStartDate = $('input#IntakeStartDate').val();
 		if (compareDate(thisDate, intakeStartDate) == '-1') {
 	 		error++;
 	 		if (focusField == '') focusField = thisID;
			$('span#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['beforeStartDate'];?>');
 		} 	
 	});

 	$('input.CombinedLessonClass').each(function(){
	    var thisID = $(this).attr('id');
	    var ary = thisID.split('_');
	    var courseID = ary[1] ;
	    var lessonID = ary[2] ;
	    var rowID = ary[3] ;
	    var classID = ary[4] ;
	    var courseLessonCounter = $('input#CourseLessonCounter_'+courseID+'_'+lessonID).val();
		var nextRow = parseInt(rowID) + 1;

		if ($(this).is(':checked')) {
    	    for (var i=nextRow; i < courseLessonCounter; i++) {
	 			if ($('input#CombinedLessonClass_'+courseID+'_'+lessonID+'_'+i+'_'+classID).is(':checked')) {
     	 			error++;
     	 			if (focusField == '') focusField = thisID;
    				$('span#ErrCourseLesson_'+courseID+'_'+lessonID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['duplicateClassSelection'];?>');
	 			}
 			}
 		} 	
 	});
 	
	if (error > 0) {
		if (focusField != '') {
			$('#' + focusField).focus();
		}
		return false
	}
	else {		
		return true;
	}
}

function hideError() 
{
	$('span.error_msg_show').each(function() {
		$(this).addClass('error_msg_hide').removeClass('error_msg_show');
	});	
}

$(document).ready(function(){

//	var orgIntakeID = $('#Intake').val();
	
	$('input#btnSubmit').click(function(e) {
		 
	    e.preventDefault();
	    $('input.actionBtn').attr('disabled', 'disabled');

	    checkSelectInstructor();
	    
		if (checkForm(document.form1)) {
			$.ajax({
				dataType: "json",
				type: "POST",
				url: '../ajax.php?action=checkeBooking',
				data : $('form#form1').serialize(),
				success: function(ajaxReturn){
					if (ajaxReturn != null && ajaxReturn.success){
						var isRoomBooked = ajaxReturn.html;
						var isInstructorTimeConflict = ajaxReturn.html2;
//						var isIntakeTimeConflict = ajaxReturn.isIntakeTimeConflict;
						var courseID, row, ary;
						var isBooked = false;
						var isTimeConflict = false;

						$(':input[name^="Location"]').each(function(){
						    var thisID = $(this).attr('id');
						    thisID = thisID.replace(/\[/g,"\\\[");
						    thisID = thisID.replace(/\]/g,"\\\]");
							ary = $(this).attr('id').split('\]\[');	// 2D array
					 		courseID = ary[0].replace('Location\[','');
					 		row = ary[1].replace('\]','');

					 		if ((isRoomBooked != '') && (typeof isRoomBooked[courseID] != 'undefined') && (typeof isRoomBooked[courseID][row] != 'undefined') && (isRoomBooked[courseID][row] == true)) {
								isBooked = true;
								$('span#Err' + thisID).addClass('error_msg_show').removeClass('error_msg_hide');
								$('input.actionBtn').attr('disabled', '');
							}


					 		if ((isInstructorTimeConflict != '') && (typeof isInstructorTimeConflict[courseID] != 'undefined') && (typeof isInstructorTimeConflict[courseID][row] != 'undefined') && (isInstructorTimeConflict[courseID][row] == true)) {
					 			isTimeConflict = true;
					 			$('span#ErrInstructor\\['+courseID+'\\]\\['+row+'\\]').addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['instructorIsUnavailable'];?>');
								$('input.actionBtn').attr('disabled', '');
							}

// 					 		if ((isIntakeTimeConflict != '') && (typeof isIntakeTimeConflict[courseID] != 'undefined') && (typeof isIntakeTimeConflict[courseID][row] != 'undefined') && (isIntakeTimeConflict[courseID][row] == true)) {
// 					 			isTimeConflict = true;
//					 			$('span#ErrTimeLogic_' + courseID + '_' + row).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['timeConflictLesson'];?>');
// 								$('input.actionBtn').attr('disabled', '');
// 							}
					 		
						});

						
						if (!isBooked && !isTimeConflict) {
//		        		if (isBooked == true) {
//		        			alert('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['locationOrTimeNotAvailable'];?>');
// 		        			$('input.actionBtn').attr('disabled', '');
// 		        		} 
// 		        		else {
		        			var conf = confirm('<?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['confirmGenerateCourse'];?>');
		        			if (!conf) {
		        				$('input.actionBtn').attr('disabled', '');
		        				return;
		        			}
		        			else {
		           				$('form#form1').submit();
		        			}
		        		}
					}
					else {
						$('input.actionBtn').attr('disabled', '');
					}
				},
				error: function(){
					$('input.actionBtn').attr('disabled', '');
					show_ajax_error();
				}
			});
		}
		else {
			$('input.actionBtn').attr('disabled', '');
		}
	});

// 	$('select#Intake').change(function(){
// // 		var conf;
		
// // 		if (isSetValue() == true) {
//			conf = confirm('<?php //echo $Lang['eEnrolment']['curriculumTemplate']['warning']['confirmChangeIntake'];?>');
// // 		}
// // 		else {
// // 			conf = true;
// // 		}
// //  		if (conf) {
// 			isLoading = true;
// 			$('td#CourseLayout').html(loadingImg);
			
//     		$.ajax({
//     			dataType: "json",
//     			async: true,			
//     			type: "POST",
//     			url: '../ajax.php',
//     			data : {
//     				'action': 'getCoursesByIntake',
//     				'IntakeID': $(this).val(),
//     				'TemplateID': $('input#TemplateID').val() 
//     			},		  
//     			success: function(ajaxReturn){
//     				if (ajaxReturn != null && ajaxReturn.success){
// //    					$('#CombinedCourseTable').html(ajaxReturn.html);
//      					var ret = ajaxReturn.html2;
//      					$('input#IntakeStartDate').val(ret[0]); 
// //     					$('#IntakeCapacity').html(ret[1]);
// 						$('td#CourseLayout').html(ajaxReturn.html);
//     					isLoading = false;
//     				}
//     			},
//     			error: show_ajax_error
//     		});
// // 		}
// //  		else {
// //  			$(this).val(orgIntakeID);
// //  		}
 		
// 	});
	
});

function changeDateTimeSelect(obj)
{
	$this = $(obj);	
	var ary = $this.attr('id').split('\]\[');	// 2D array
	var idx = ary[0].indexOf('[');
	if (idx != -1 ) {
		var courseID = ary[0].substr(idx+1);	
 		var row = ary[1].replace('\]','');
 		var lessonDate = $('input#LessonDate_'+courseID+'_'+row).val();
		var startHour = $('select#StartHour\\['+courseID+'\\]\\['+row+'\\]').val();
 		var startMinute = $('select#StartMinute\\['+courseID+'\\]\\['+row+'\\]').val();
 		var endHour = $('select#EndHour\\['+courseID+'\\]\\['+row+'\\]').val();
 		var endMinute = $('select#EndMinute\\['+courseID+'\\]\\['+row+'\\]').val();
 		
        isLoading = true;
		$('td#LocationCol_'+courseID+'_'+row).html(loadingImg);
 		
		var errMsgObj = $('span#ErrTimeLogic_' + courseID + '_' + row);
 		if (validateTimeLogic(startHour, startMinute, endHour, endMinute)) {
 			errMsgObj.addClass('error_msg_hide').removeClass('error_msg_show');
			getLocationSelection(lessonDate, startHour, startMinute, endHour, endMinute, courseID, row);
 		}
 		else {
 			errMsgObj.addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['Warning']['TimeRangeInvalid'];?>');
 		}
	}
}

function checkSelectInstructor()
{
    $(':select[name*="Instructor"]').each(function(){
	    var thisID = $(this).attr('id');
	    thisID = thisID.replace(/\[/g,"\\\[");
	    thisID = thisID.replace(/\]/g,"\\\]");
		$('#' + thisID + ' option').attr('selected',true);
    });
}

function isSetValue()
{
	var hasSetValue = false;
	$(':input[name^="Location"]').each(function(){
		if ($(this).val() != '') {
			hasSetValue = true; 
		}
	});
	
	checkSelectInstructor();
	
	$(':select[name*="Instructor"]').each(function(){
		if ($(this).find('option:selected').length > 0) {
			hasSetValue = true;
		}
	});

	$(':input[name^="CourseCode"]').each(function(){
		if ($.trim($(this).val()) != '') {
			hasSetValue = true;
		}
	});
	return hasSetValue;
}

function changeLessonDate(obj)
{
	var $this = $(obj);
	var lessonDateID = $this.attr('id');
	var str = lessonDateID.split('_');
	var prefix = str[0];
	var courseID = str[1];
	var row = str[2];
	var startDate = $this.val();
	var startHour = $('select#StartHour\\['+courseID+'\\]\\['+row+'\\]').val();
	var startMinute = $('select#StartMinute\\['+courseID+'\\]\\['+row+'\\]').val();
	var endHour = $('select#EndHour\\['+courseID+'\\]\\['+row+'\\]').val();
	var endMinute = $('select#EndMinute\\['+courseID+'\\]\\['+row+'\\]').val();
	var intakeStartDate = $('input#IntakeStartDate').val();
	
	if (compareDate(startDate, intakeStartDate) == '-1') {
		$('span#Err' + lessonDateID).addClass('error_msg_show').removeClass('error_msg_hide').html('<?php echo $Lang['eEnrolment']['course']['warning']['beforeStartDate'];?>');
	}
	else { 	
		$('span#Err' + lessonDateID).addClass('error_msg_hide').removeClass('error_msg_show');
	}

    isLoading = true;
    $('td#LocationCol_'+courseID+'_'+row).html(loadingImg);
	getLocationSelection(startDate, startHour, startMinute, endHour, endMinute, courseID, row);
}

function validateTimeLogic(startHour, startMinute, endHour, endMinute)
{
	var hourStart = (startHour=='')? '00' : str_pad(startHour, 2);
	var minStart = (startMinute=='')? '00' : str_pad(startMinute, 2);
	var timeTextStart = hourStart + minStart;
	
	var hourEnd = (endHour=='')? '00' : str_pad(endHour, 2);
	var minEnd = (endMinute=='')? '00' : str_pad(endMinute, 2);
	var timeTextEnd = hourEnd + minEnd;
	if (timeTextStart > timeTextEnd) {
		return false;
	}
	else {
		return true;
	}
}

function getLocationSelection(startDate, startHour, startMinute, endHour, endMinute, courseID, row)
{
	$.ajax({
		dataType: "json",
		async: true,			
		type: "POST",
		url: '../ajax.php',
		data : {
			'action': 'getAvailableLocationList',
			'startDate': startDate,
			'startHour': startHour,
			'startMinute': startMinute,
			'endHour': endHour,
			'endMinute': endMinute,
			'courseID': courseID,
			'row': row,
			'intakeID': $('input#Intake').val()
		},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('td#LocationCol_'+courseID+'_'+row).html(ajaxReturn.html);
				isLoading = false;
			}
		},
		error: show_ajax_error
	});
}

function show_ajax_error() {
	alert('<?php echo $Lang['General']['AjaxError'];?>');
}

function applyCategoryClassInstructorToAllCourses() {
	$('select.defaultCategoryClassInstructorSel').each( function() {
		var _defaultSelObj = $(this);
		var _defaulSelID = _defaultSelObj.attr('id');
		var _defaultSelIDAry = _defaulSelID.split("_");
		var _categoryID = _defaultSelIDAry[1];
		var _intakeClassID = _defaultSelIDAry[2];
		
		$('select.ClassInstructorSel_' + _categoryID + '_' + _intakeClassID).each( function() {
			var __thisTargetSelID = $(this).attr('id');
			
			$('select#' + getJQuerySafeId(__thisTargetSelID) + ' option').remove();
			$(this).append($('select#' + getJQuerySafeId(_defaulSelID) + ' >  option').clone());
		});

		if ($('select#' + getJQuerySafeId(_defaulSelID) + ' >  option').length > 0) {
			
    		$('input:checkbox[id$="_'+ _intakeClassID+'"]:checked').each(function(){
        	    var __thisID = $(this).attr('id');
        	    var __ary = __thisID.split('_');
        	    var __courseID = __ary[1] ;
        	    var __lessonID = __ary[2] ;
        	    var __rowID = __ary[3] ;
    			var __instructorID = 'CombinedLessonInstructor_'+getJQuerySafeId(__courseID)+'_'+getJQuerySafeId(__lessonID)+'_'+getJQuerySafeId(__rowID);
    
     			$('select#'+__instructorID).append($('select#' + getJQuerySafeId(_defaulSelID) + ' >  option').clone());

     			var optionList = [];
     			$('select#'+__instructorID+' option').each(function() {
         			if (optionList[this.value] || (this.value == '')){
             			$(this).remove();
         			}
         			else {
						optionList[this.value] = true;
         			}
     			});
    		});		
		}		
	});
	
}

function addCombinedLessonClassInstructor(obj)
{
	var $this = $(obj);
	var courseLessonID = $this.attr('data-CourseLessonID');
	var str = courseLessonID.split('_');
	var courseID = str[0];
	var lessonID = str[1];
	var rowCounter = parseInt($('input#CourseLessonCounter_'+courseID+'_'+lessonID).val());

	$.ajax({
		dataType: "json",
		async: true,			
		type: "POST",
		url: '../ajax.php',
		data : {
			'action': 'addCombinedLessonClassInstructor',
			'IntakeID': $('input#Intake').val(),
			'CourseID': courseID,
			'LessonID': lessonID,
			'RowID': rowCounter,
			'TemplateID': $('input#TemplateID').val()  
		},		  
		success: function(ajaxReturn){
			if (ajaxReturn != null && ajaxReturn.success){
				$('#CourseLessonDiv_'+courseID+'_'+lessonID).append(ajaxReturn.html);
				$('input#CourseLessonCounter_'+courseID+'_'+lessonID).val(rowCounter+1)
			}
		},
		error: show_ajax_error
	});
	
}

function removeCombinedLessonInstructor(obj)
{
	var $this = $(obj);
	var courseLessonID = $this.attr('data-ClassInstructorID');
	var str = courseLessonID.split('_');
	var courseID = str[0];
	var lessonID = str[1];
	var rowID = str[2];
	$('div#DivCourseLessonRow_'+courseID+'_'+lessonID+'_'+rowID).remove();
}

</script>

<form name="form1" id="form1" method="POST" action="generate_update.php">

    <table id="main_table" width="98%" border="0" cellspacing="0" cellpadding="0">
    	<tr>
    		<td align="center">
    			<table width="100%" border="0" cellspacing="0" cellpadding="0">
    				<tr>
    					<td class="navigation"><?php echo $linterface->GET_NAVIGATION($PAGE_NAVIGATION); ?></td>
    				</tr>
    			</table>
    			
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
                		<td><?php echo $linterface->GET_STEPS($STEPS_OBJ); ?></td>
                	</tr>
				</table>    			
    			
    			<table width="100%" border="0" cellpadding="0" cellspacing="0">
    				<tr>
    					<td>
    						<table align="center" class="form_table_v30">
    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['tabs']['curriculumTemplate'];?></td>
    								<td class="tabletext"><?php echo intranet_htmlspecialchars($templateCurriculumTitle);?></td>
    							</tr>

    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameEn'];?></td>
    								<td class="tabletext">
    									<input type="text" name="TitleEnglish" id="TitleEnglish" class="textboxtext" value="<?php echo intranet_htmlspecialchars($curriculumTitleEnglish);?>">
    									<span class="error_msg_hide" id="ErrTitleEnglish"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['inputTitleEnglish'];?></span>
    								</td>
    							</tr>
    
    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameCh'];?></td>
    								<td class="tabletext">
    									<input type="text" name="TitleChinese" id="TitleChinese" class="textboxtext" value="<?php echo intranet_htmlspecialchars($curriculumTitleChinese);?>">
    									<span class="error_msg_hide" id="ErrTitleChinese"><?php echo $Lang['eEnrolment']['curriculumTemplate']['warning']['inputTitleChinese'];?></span>
    								</td>
    							</tr>
    							
    							<tr valign="top">
    								<td width="20%" valign="top" nowrap="nowrap" class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['intake'];?></td>
    								<td class="tabletext"><?php echo intranet_htmlspecialchars($intakeTitle);?></td>
    							</tr>
    							
    						</table>
    					</td>
    				</tr>
    			</table>
    		</td>
    	</tr>
    	
    	<tr>
    		<td id="CourseLayout">
        		<table style="width:100%">
        			<?php echo $linterface->getInstructorApplyToAllShortCutLayout($intakeID);?>
        			
                	<?php echo $linterface->getCombinedCourseLayout($templateID, $intakeID);?>
                	
                	<?php echo $linterface->getCombinedLessonLayout($templateID, $intakeID);?>
            		
            		<?php echo $linterface->getNonCombinedCourseLayout($templateID, $intakeID);?>
        		</table>
    		</td>
		</tr>
		
		<tr><td>&nbsp;</td></tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="tabletextremark"><?php echo $i_general_required_field;?></td>
		</tr>
		
		<tr>
			<td height="1" class="dotline"><img src="<?php echo "{$image_path}/{$LAYOUT_SKIN}"; ?>/10x10.gif" width="10" height="1"></td>
		</tr>
		
		<tr><td>&nbsp;</td></tr>
		    	
		<tr>
			<td align="right">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td align="center">
							<span>
								<?php echo $linterface->GET_ACTION_BTN($button_submit, "button","","btnSubmit", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
								<?php echo $linterface->GET_ACTION_BTN($button_reset, "reset","","btnReset", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>&nbsp;
								<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location='index.php'","btnCancel", $ParOtherAttribute="", $ParDisabled=0, $ParClass="", $ParExtraClass="actionBtn");?>
							</span>
						</td>
					</tr>
				</table>
			</td>
		</tr>
    	
    </table>
		

	<input type="hidden" name="TemplateID" id="TemplateID" value="<?php echo $templateID;?>" />
	<input type="hidden" name="Intake" id="Intake" value="<?php echo $intakeID;?>" />
	<input type="hidden" name="IntakeStartDate" id="IntakeStartDate" value="<?php echo $intakeStartDate;?>" />

</form>

<?php
    

$linterface->LAYOUT_STOP();
intranet_closedb();
?>