<?php

# usint: yat

##### Change Log [Start] #####
#
#	Date	:	2011-09-08	YatWoon
# 				lite version should not access this page 
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'] && !$plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageMgtActEventOverall";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	$enrolInfo = $lc->getEnrollmentInfo();
	
	list($students,$received,$notSchool,$notOwn,$own) = $enrolInfo;
	if ($students != $received)
	{
		$linkNotHandin = "[".$i_ClubsEnrollment_NotHandinList." - ";
	    $linkNotHandin .= "<a class='tablelink' href=\"javascript:openList(1)\">".$button_print."</a>";
	    $linkNotHandin .= " | ";
	    $linkNotHandin .= "<a class='tablelink' href=\"javascript:jsPopUpNotify()\">".$button_notify."</a>";
	    $linkNotHandin .= " ]";
	}
	else
	{
	    $linkNotHandin = "";
	}
	if ($notSchool != 0)
	{
	    $linkNotSchool = "<a class='tablelink' href=\"javascript:openList(2)\">$i_ClubsEnrollment_NameList</a>";
	}
	else
	{
	    $linkNotSchool = "";
	}
	if ($notOwn != 0)
	{
	    $linkNotOwn = "<a class='tablelink' href=\"javascript:openList(3)\">$i_ClubsEnrollment_NameList</a>";
	}
	else
	{
	    $linkNotOwn = "";
	}
	if ($own != 0)
	{
	    $linkSat = "<a class='tablelink' href=\"javascript:openList(4)\">$i_ClubsEnrollment_NameList</a>";
	}
	else
	{
	    $linkSat = "";
	}
	
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['act_event_status_stat_overall'], "", 1);

        
		//$ConfArr['IsHorizontal'] = 1;
		$ConfArr['IsVertical'] = 1;
		$ConfArr['ChartX'] = $eEnrollmentMenu['club_enroll_status'];
		$ConfArr['ChartY'] = $eEnrollmentMenu['head_count'];
		$ConfArr['MaxWidth'] = 500;		
		
		$DataArr[] = array($i_ClubsEnrollment_NumOfStudent, $students);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfReceived ".$linkNotHandin, $received);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfNotFulfilSchool ".$linkNotSchool, $notSchool);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfNotFulfilOwn ".$linkNotOwn, $notOwn);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfSatisfied ".$linkSat, $own);
		



		
		/*
		$DataArr[] = array($i_ClubsEnrollment_NumOfStudent, $students);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfReceived $linkNotHandin", $received);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfNotFulfilSchool $linkNotSchool", $notSchool);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfNotFulfilOwn $linkNotOwn", $notOwn);
		$DataArr[] = array("$i_ClubsEnrollment_NumOfSatisfied $linkSat", $own);
		*/

        $linterface->LAYOUT_START();
		//$linterface->GET_SMALL_BTN($i_Discipline_System_Approval_View, "button", "javascript:viewApproval('1');");
?>

<script language="javascript">
function viewApproval(type){
    document.location.href = '<?=$intranet_httppath?>/home/admin/discipline/approval/index.php?MeritType='+ type ;
    //window.close();
}

function openList(type)
{
    newWindow('list.php?type='+type,1);
}

function jsPopUpNotify()
{
	newWindow('reports/club_enrolment_statistics/notify_student.php', 24);
}
</script>
<form name="form1" action="" method="POST">
<br/>
<?= $linterface->GEN_CHART($ConfArr, $DataArr)?>

</form>


    <?

        $linterface->LAYOUT_STOP();
        intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
	intranet_closedb();
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
?>