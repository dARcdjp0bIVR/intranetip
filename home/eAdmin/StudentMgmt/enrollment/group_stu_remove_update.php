<?php
## Using By: 
######################################
##	Modification Log:
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition

## 2010-01-08 Max (201001071635)
## - Case the email send by "OnceEmail"
########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$EnrolGroupID = IntegerSafe($EnrolGroupID);

$libenroll = new libclubsenrol();

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID))) {
	No_Access_Right_Pop_Up();
}

$StuArr['EnrolGroupID'] = $EnrolGroupID;
$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
$UserArr = $uid;

for ($i = 0; $i < sizeof($UserArr); $i++) {
	//check if the student is rejected already
	$sql = "SELECT RecordStatus FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '$EnrolGroupID' AND StudentID = '".$UserArr[$i]."'";
	$result = $libenroll->returnArray($sql,1);
	if ($result[0][0]==1)
	{
		//ignore rejected student
		continue;	
	}
	
	$StuArr['StudentID'] = $UserArr[$i];
	$libenroll->DEL_GROUP_STU($StuArr);
	$updatedStuList[] = $UserArr[$i];
}

// send email
$RecordType = "club";
$successResult = false;
$libenroll->Send_Enrolment_Result_Email($updatedStuList, $GroupID, $RecordType, $successResult);


intranet_closedb();
header("Location: group_member.php?EnrolGroupID=$EnrolGroupID&msg=2&field=$field&order=$order&filter=$filter&keyword=$keyword");
?>