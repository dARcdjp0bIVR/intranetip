<?php
/*using:
 * 2020-11-02 (Ray)
 * - remove UseSubsidies
 * 2020-07-03 (Ray)
 * - added multi payment gateway
 * 2020-02-27 (Ray)
 * - added MerchantAccountID
 * 2015-06-12 (Evan)
 * - Student without class can also be shown
 * - Update table style
 * - When access via club, the tab is Record instead of Process
 * 
 * 2014-03-31 (Ivan) [2014-0324-1344-50066]
 * - improved to prevent double submission
 */
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$LibUser = new libuser();

if ($plugin['eEnrollment'])
{
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");

	if ($EnrolGroupID != "") {
		$CurrentPage = "PageMgtClub";
		$TempArr = $libenroll->getGroupInfo($EnrolGroupID);
		$title = $TempArr[0][1]." - ".$eEnrollmentMenu['act_status_stat'];
		$backurl = "payment.php?EnrolGroupID=$EnrolGroupID";
		# tags 
	    $tab_type = "club";
	    $current_tab = 1;
	}
	if ($EnrolEventID != "") {
		$CurrentPage = "PageMgtActivity";
		$TempArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		$title = $TempArr[3]." - ".$eEnrollmentMenu['act_event_status_stat'];
		$backurl = "payment.php?EnrolEventID=$EnrolEventID";
		# tags 
	    $tab_type = "activity";
	    $current_tab = 1;
	}
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();
        $title .= " ".$eEnrollment['payment'];
        //$TAGS_OBJ[] = array($title, "", 1);
        # tags 
	    include_once("management_tabs.php");
	    # navigation
        $PAGE_NAVIGATION[] = array($title, "");
        
		$STEPS_OBJ[] = array($eEnrollment['add_payment']['step_1'], 0);
        $STEPS_OBJ[] = array($eEnrollment['add_payment']['step_2'], 1);

        $linterface->LAYOUT_START();
		
		
// get enrol student list
if ($EnrolGroupID != "") {
	// get group enrol student list	
	//$StudentArr = $libenroll->GET_ENROLLED_GROUPSTUDENT($EnrolGroupID);
	$StudentArr = $libenroll->Get_Club_Member_Info($EnrolGroupID,$RecordType=2);
			
} else if ($EnrolEventID != "") {
	// get event enrol student list
	$StudentArr = $libenroll->GET_ENROLLED_EVENTSTUDENT($EnrolEventID);
}


$ReturnTable = "<table class=\"common_table_list_v30 edit_table_list_v30\">";
$ReturnTable .= "<thead><tr><th>#</th>";
$ReturnTable .= "<th>".$eEnrollment['student_name']."</th>";
$ReturnTable .= "<th>".$eEnrollment['amount_paid_by_student']."</th>";
//$ReturnTable .= "<th>".$eEnrollment['amount_paid_by_school']."</th>";
$ReturnTable .= "</tr></thead>";

for ($i = 0; $i < sizeof($StudentArr); $i++) {
	$order=$i+1;
	$ReturnTable .= "<tr><td>".$order."</td>";
	$ReturnTable .= "<td>".$LibUser->getNameWithClassNumber($StudentArr[$i]['StudentID'])."</td>";
	$ReturnTable .= "<td><input type=\"text\" class=\"textboxnum\" name=\"ItemAmount[".$StudentArr[$i]['StudentID']."]\" id=\"ItemAmount".$StudentArr[$i]['StudentID']."\" value=\"".$_POST['ItemAmount']."\"></td>";
	//$ReturnTable .= "<td><input type=\"text\" class=\"textboxnum\" name=\"ItemAmount[".$StudentArr[$i]['StudentID']."]\" id=\"ItemAmount".$StudentArr[$i]['StudentID']."\" value=\"".$_POST['ItemAmount']."\" onKeyUp=\"(this.value != ".$_POST['ItemAmount'].") ? document.getElementById('UseSubsidies".$StudentArr[$i]['StudentID']."').disabled=false: document.getElementById('UseSubsidies".$StudentArr[$i]['StudentID']."').disabled=true;\"></td>";
	//$ReturnTable .= "<td><input type=\"checkbox\" disabled id=\"UseSubsidies".$StudentArr[$i]['StudentID']."\" name=\"UseSubsidies[".$StudentArr[$i]['StudentID']."]\" value=\"1\"></td>";
	$ReturnTable .= "</tr>\n";
}
$ReturnTable .= "</table>";

?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	//if(!check_text(obj.ItemName, "<?php echo $i_alert_pleasefillin.$eEnrollment['pay']['item_name']; ?>.")) return false;
	//if(!check_numeric(obj.ItemAmount, 0, "<?php echo $i_alert_pleasefillin.$eEnrollment['pay']['amount']; ?>.")) return false;
<? for ($i = 0; $i < sizeof($StudentArr); $i++) { ?>
	if(!check_numeric(obj.ItemAmount<?= $StudentArr[$i]['StudentID']?>, <?= $_POST['ItemAmount']?>, "<?php echo $i_alert_pleasefillin.$eEnrollment['pay']['amount']; ?>.")) return false;
<? } ?>
	//AlertPost(obj, 'payment_update.php', '<?= $eEnrollment['js_payment_alert']?>');
	if (confirm('<?=$eEnrollment['js_payment_alert']?>')){
       $('input#confirmBtn').attr('disabled', 'disabled');
       obj.action = 'payment_update.php';
       obj.method = "post";
       obj.submit();
    }
}
</SCRIPT>
<form name="form1" action="payment_update.php" method="POST">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<!--
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
-->
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
</table>
<?= $ReturnTable?>


<table width="96%" border="0" cellspacing="0" cellpadding="4" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_confirm, "button", "FormSubmitCheck(this.form)", "confirmBtn")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='$backurl'")?>
</div>
</td></tr>
</table>

<input type="hidden" id="GroupID" name="GroupID" value="<?= $GroupID?>">
<input type="hidden" id="EnrolGroupID" name="EnrolGroupID" value="<?= $EnrolGroupID?>">
<input type="hidden" id="EnrolEventID" name="EnrolEventID" value="<?= $EnrolEventID?>">
<input type="hidden" id="CatID" name="CatID" value="<?= $CatID?>">
<input type="hidden" id="ItemName" name="ItemName" value="<?= $ItemName?>">
<input type="hidden" id="DisplayOrder" name="DisplayOrder" value="<?= $DisplayOrder?>">
<input type="hidden" id="PayPriority" name="PayPriority" value="<?= $PayPriority?>">
<input type="hidden" id="Discription" name="Discription" value="<?= $Discription?>">
<input type="hidden" id="StartDate" name="StartDate" value="<?= $StartDate?>">
<input type="hidden" id="EndDate" name="EndDate" value="<?= $EndDate?>">
<input type="hidden" id="OriItemAmount" name="OriItemAmount" value="<?= $ItemAmount?>">
<input type="hidden" id="debit_type" name="debit_type" value="<?= $debit_type?>">
<input type="hidden" id="MerchantAccountID" name="MerchantAccountID" value="<?= $MerchantAccountID?>">
<?php
if ($sys_custom['ePayment']['MultiPaymentGateway']) {
	$lpayment = new libpayment();
    $service_provider_list = $lpayment->getPaymentServiceProviderMapping(false,true);
    foreach($service_provider_list as $k=>$temp) {
        echo '<input type="hidden" id="MerchantAccountID_'.$k.'" name="MerchantAccountID_'.$k.'" value="'.${"MerchantAccountID_".$k}.'">';
    }
} ?>
</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>
<?
/*

# update AddToPayment
if ($GroupID != "") {
	$Sql = "
				UPDATE
						INTRANET_ENROL_GROUPINFO
				SET
						AddToPayment = 1
				WHERE
						GroupID = '$GroupID'
			";
} else if ($EnrolEventID != "") {
	$Sql = "
				UPDATE
						INTRANET_ENROL_EVENTINFO
				SET
						AddToPayment = 1
				WHERE
						EnrolEventID = '$EnrolEventID'
			";
}
$libenroll->db_db_query($Sql);


# add PAYMENT_PAYMENT_ITEM ################################################################################
$Sql = "
			INSERT INTO
						PAYMENT_PAYMENT_ITEM
						(CatID, Name, DisplayOrder, PayPriority, Description, RecordStatus, 
						 ProcessingTerminalUser, ProcessingTerminalIP, ProcessingAdminUser,
						 ProcessingUserID,
						 StartDate, EndDate, DefaultAmount, DateInput, DateModified)
			VALUES
						(
						'".$_POST['CatID']."',
						'".$_POST['ItemName']."',
						'".$_POST['DisplayOrder']."',
						'".$_POST['PayPriority']."',
						'".$_POST['Description']."',
						'0', 						
						NULL, NULL, NULL,
						'$UserID',
						'".$_POST['StartDate']."',
						'".$_POST['EndDate']."',
						'".$_POST['ItemAmount']."',
						NOW(), NOW()
						)
		";
$libenroll->db_db_query($Sql);
$ItemID = $libenroll->db_insert_id($Sql);
# add PAYMENT_PAYMENT_ITEM ################################################################################

// get enrol student list
if ($GroupID != "") {
	// get group enrol student list	
	$StudentArr = $libenroll->GET_ENROLLED_GROUPSTUDENT($GroupID);
} else if ($EnrolEventID != "") {
	// get event enrol student list
	$StudentArr = $libenroll->GET_ENROLLED_EVENTSTUDENT($EnrolEventID);
}

for ($i = 0; $i < sizeof($StudentArr); $i++) {
	
	$Sql = "
				INSERT INTO
							PAYMENT_PAYMENT_ITEMSTUDENT
							(ItemID, StudentID, Amount, ProcessingUserID, RecordType, RecordStatus, 
							 DateInput, DateModified)
				VALUES
							(
							'".$ItemID."',
							'".$StudentArr[$i][1]."',
							'".$_POST['ItemAmount']."',
							'".$UserID."',
							0, 0, 
							NOW(), NOW()
							)
			";
	$libenroll->db_db_query($Sql);
	$PaymentID = $libenroll->db_insert_id($Sql);

	if ($debit_type == "debit_directly") {
		
		# get current balance
		$Sql = "
					SELECT
								Balance
					FROM
								PAYMENT_ACCOUNT
					WHERE
								StudentID = '".$StudentArr[$i][1]."'
				";
		
		$Temp = $libenroll->returnVector($Sql);
		$Balance = $Temp[0];
		$BalanceAfter = $Balance - $_POST['ItemAmount'];

		# add PAYMENT_OVERALL_TRANSACTION_LOG
		$Sql = "
					INSERT INTO
								PAYMENT_OVERALL_TRANSACTION_LOG
								(StudentID, TransactionType, Amount, RelatedTransactionID,
								 BalanceAfter, TransactionTime, Details  )
					VALUES
								(
								'".$StudentArr[$i][1]."',
								'2',
								'".$_POST['ItemAmount']."',
								'".$PaymentID."',
								'".$BalanceAfter."',
								NOW(),
								'�ҥ~���ʶO / ECA Fee'
								)
				";
		$Temp = $libenroll->db_db_query($Sql);
		$logID = $libenroll->db_insert_id($Sql);
		
		$Sql = "
					UPDATE
								PAYMENT_OVERALL_TRANSACTION_LOG
					SET
								RefCode = CONCAT('PAY',LogID)
					WHERE
								LogID = $logID
				";
		$libenroll->db_db_query($Sql);

		# update PAYMENT_ACCOUNT
		$Sql = "
					UPDATE
								PAYMENT_ACCOUNT
					SET
								Balance = '".$BalanceAfter."',
								LastUpdateByAdmin = NULL,
								LastUpdateByTerminal = NULL,
								LastUpdateByUser = NOW(),
								LastUpdated = NOW()
					WHERE
								StudentID = '".$StudentArr[$i][1]."'
				";				
		$libenroll->db_db_query($Sql);
		
		# update PAYMENT_PAYMENT_ITEMSTUDENT
		$Sql = "
				UPDATE				
							PAYMENT_PAYMENT_ITEMSTUDENT
				SET
							ProcessingTerminalUser = NULL,
							ProcessingTerminalIP = NULL,
							ProcessingAdminUser = NULL,
							RecordStatus = 1,
							PaidTime = NOW(),
							DateModified = NOW()
				WHERE
							ItemID = '".$ItemID."'
						AND
							StudentID = '".$StudentArr[$i][1]."'
			";
		$libenroll->db_db_query($Sql);		
	}
}

if ($GroupID != "") {
	$URL = "club_status.php?msg=2";
}
if ($EnrolEventID != "") {
	$URL = "event_status.php?msg=2";
}
*/

intranet_closedb();
//header("Location: $URL");

?>