<?php
##################################### Change Log [Start] #####################################################
#
# 2014-12-30 Omas:	Add filter to choose class and show gender
#
###################################### Change Log [End] ######################################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");

$lclass = new libclass();
$LibUser = new libuser($UserID);
$linterface = new interface_html("popup.html");

//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
//	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");

if ($libenroll->hasAccessRight($_SESSION['UserID']))
{
	switch ($type)
	{
	        case 1:
	        	 # Student who did not submit the club application
	             $studentList = $libenroll->returnNoHandinList($_GET['classID']); //20141230
	             $heading = $i_ClubsEnrollment_HeadingNotHandinList;
	             break;
	        case 2:
	        	 # Students NOT satisfying school requirements
	             $studentList = $libenroll->returnNotFulfilSchoolList($_GET['classID']);//20141230
	             $heading = $i_ClubsEnrollment_HeadingNotFulfilSchool;
	             break;
	        case 3:
	        	 # Students satisfying school requirements but not own requirements 
	             $studentList = $libenroll->returnFulfilSchoolButNotFulfilOwnList($_GET['classID']);//20141230
	             //$studentList = $libenroll->returnNotFulfilOwnList();
	             $heading = $i_ClubsEnrollment_HeadingNotFulfilOwn;
	             break;
	        case 4:
	        	 # Students satisfying own requirements 
	             $studentList = $libenroll->returnSatisfiedList($_GET['classID']);
	             $heading = $i_ClubsEnrollment_HeadingSatisfied;
	             break;
	        default:
	             $studentList = array();
	             $heading = "Not Implemented";
	             break;
	}
			
			
		    $ClassSelection = $lclass->getSelectClassID("id='classID' name='classID' onChange='this.form.submit();'", $classID, "1","" ,$i_general_all_classes);
	
	$x = "
		<form name='form1' method='get' action='list.php'>
		<table width='100%' align='center' class='print_hide' border='0'>
		<tr>
			<td>".$ClassSelection."</td>
		</tr>
		<tr>
			<td>$heading</td>
			<td align='right'>".$linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")."</td>
		</tr>
		</table>
		<input type='hidden' name='type' value='$type' />
		</form>";
	
	$x .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='eSporttableborder'>\n";
	$x .= "<tr class='eSporttdborder eSportprinttabletitle'>
				<td width=10 class='eSporttdborder eSportprinttabletitle'>#</td>
				<td class='eSporttdborder eSportprinttabletitle'>$i_UserClassName</td>
				<td class='eSporttdborder eSportprinttabletitle'>$i_UserClassNumber </td>
				<td class='eSporttdborder eSportprinttabletitle'>$i_UserStudentName </td>
				<td class='eSporttdborder eSportprinttabletitle'>$i_UserLogin </td>
				<td class='eSporttdborder eSportprinttabletitle'>".$Lang['StudentRegistry']['Gender']."</td>
			</tr>\n";
	for ($i=0; $i<sizeof($studentList); $i++)
	{
	     $count = $i+1;
	     $css = (($j%2)+1);
	     list($id,$class,$classnumber,$name,$login,$gender) = $studentList[$i];
	     if ($class == "") $class = "-";
	     if ($classnumber == "") $classnumber = "-";
	     if ($name == "") $name = "-";
	     if ($login == "") $login = "-";
	     $x .= "<tr class='eSporttdborder eSportprinttext'>
	     			<td width='10' class='eSporttdborder eSportprinttext'>$count</td>
	     			<td class='eSporttdborder eSportprinttext'>$class</td>
	     			<td class='eSporttdborder eSportprinttext'>$classnumber</td>
	     			<td class='eSporttdborder eSportprinttext'>$name</td>
	     			<td class='eSporttdborder eSportprinttext'>$login</td>
					<td class='eSporttdborder eSportprinttext'>$gender</td>
	     		</tr>\n";
	}
	$x .= "</table>\n";
	?>
	<script language="javascript">
	
	</script>
	
	<?
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
	?>
	
	<?=$x?>
	<br>
	<!--
	<table width='100%' border='0'>
		<tr><td align='center'>
		<?= $linterface->GET_ACTION_BTN($button_print, "button", "javascript:window.print()")?>
		</td></tr>
	</table>
	-->
	<?php
	//include_once($PATH_WRT_ROOT."templates/filefooter.php");
	include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	intranet_closedb();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>