<?php
# using:  

######### Change Log Start ###############
#
#   Date:   2020-02-28 (Tommy)
#           - changed ApplyUserType default select parent for KIS
#
#   Date:   2019-08-27 (Henry)
#           - added addslashes for the filenames
#
#   Date:   2019-05-24 (Anna)
#           - Changed attachment file from view to downlowd_attachemnt format
#
#   Date:   2018-10-03 Philips
#           - modified js Func EmptyAttachment() (return {a,b} since IE not supported)
#
#   Date:   2018-08-21 Anna
#           Added $sys_custom['eEnrolment']['OptionalClubChineseName']
#
#	Date:	2017-12-14 Pun
#			Add NCS cust
#
#	Date:	2017-09-14 Anna
#			Add WebSAMSCode setting
#
#	Date:	2017-04-21 Villa #P110324
#			Add ReplySlip Related	
#
#	Date:   2016-07-22 Cara
#			Add InputBy, InputDate, ModifiedBy, ModifiedDate
#
#	Date:   2016-06-10 Cara
#			Remove the checking of content
#
#   Date:   2015-06-09 Shan
#           Checking of content added  
#
#	Date:	2014-11-21 Omas
#			Add target house check box to enable house mapping
#
#	Date:	2014-10-14 Pun
#			- Add round report for SIS customization
#
#	Date:	2014-07-22 Bill
#			- Add DirectToMeetingGroup for the process of new group setting
#
#	Date:	2014-05-02 Carlos
#			Customization $sys_custom['eEnrolment']['TWGHCYMA'] - added INTRANET_GROUP relation
#
#	Date:	2013-08-09 Cameron
#			- Pass categoryTypeID to GET_CATEGORY_SEL()			
#
#	Date:	2013-08-01 Cameron
#			- Add Enrollment Setting (Target Applicant)			
#
#	Date:	2011-09-30 Henry Chow
#			- display OLE Default Setting at the bottom
#
#	Date:	2010-12-14 YAtWoon
#			- display club name according to lang session
#			- change select pic/helper popup path to /home/common_choose/enrolment_pic_helper.php
#
#   Date:	2010-08-02  Thomas
# 			- Check the data is freezed or not
#			  Show alert message when adding / updating the data
#
#	Date:	2010-05-07	YatWoon
#			enlarge the width of html editor
#
#	Date:	2010-03-09	Marcus
#			replace content textarea by html editor
#############################################


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

if($plugin['SIS_eEnrollment']){
	include_once($PATH_WRT_ROOT."lang/cust/SIS_eEnrollment_lang.$intranet_session_language.php");
}
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol($AcademicYearID);
if (!$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$AcademicYearID = IntegerSafe($AcademicYearID);
$Semester = IntegerSafe($Semester);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$Round = IntegerSafe($Round);

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	if (isset($EnrolGroupID))
	{
		$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
		//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
	}

	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club", $AcademicYearID)))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	
	$cancel_btn_para = "AcademicYearID=$AcademicYearID&Semester=$Semester&Round=$Round";
	if (($LibUser->isStudent())||($LibUser->isParent())) 
	{
		if ($libenroll->IS_ENROL_PIC($UserID, $EnrolGroupID, 'Club'))
		{
			$CurrentPageArr['eEnrolment'] = 0;
			$CurrentPageArr['eServiceeEnrolment'] = 1;
		
			$cancel_btn_para .= "&pic_view=1";
		}
	}
		
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$libenroll = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
		include_once($PATH_WRT_ROOT."includes/libgroup.php");
		
        $libgroup = new libgroup($GroupID);
        $linterface = new interface_html();
        //$TAGS_OBJ[] = array($eEnrollmentMenu['add_act_event'], "", 1);
        # tags 
	    $tab_type = "club";
	    $current_tab = 1;
	    include_once("management_tabs.php");
	    # navigation
        $PAGE_NAVIGATION[] = array($eEnrollment['enrolment_settings'], "");

        $Step1 = $eEnrollment['add_club_activity']['step_1'];
        $Step2 = $eEnrollment['add_club_activity']['step_1b'];
        $Step3 = $eEnrollment['add_club_activity']['WebSAMSSetting'];
        
        $Step4 = $Lang['eEnrolment']['CreatedAndModifiedRecords'];
//		$STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_2'], "");
		
		$tiebreakerSelection = "<SELECT name=tiebreak>\n";
		$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
		$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
		$tiebreakerSelection.= "</SELECT>\n";
		
        $linterface->LAYOUT_START();
/*     
if (isset($gid))
{
	$GroupID = $gid[0];
}
*/
$EnrollGroupArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
$TitleDisplay = $intranet_session_language=="en" ? $EnrollGroupArr['Title'] : $EnrollGroupArr['TitleChinese'];

if($sys_custom['eEnrolment']['OptionalClubChineseName']){
    $TitleDisplay = $EnrollGroupArr['TitleChinese']==''? $EnrollGroupArr['Title']:$TitleDisplay;   
}
if ($sys_custom['eEnrolment']['CategoryType']) {
	$categoryTypeID = array(1);	// Club
}
else {
	$categoryTypeID = 0;
}

# House limit Selection
###############################################################################################
$HouseArr = $libenroll->Get_GroupMapping_Group($AcademicYearID, HOUSE_RECORDTYPE_IN_INTRANET_GROUP);
$HouseAssoArr = BuildMultiKeyAssoc($HouseArr, 'GroupID',array('Title'),1);

if(isset($_GET)){
	$houseMapping = $libenroll->Get_Club_Event_Mapping($enrolConfigAry['Club'], $EnrolGroupID);
	if(empty($houseMapping)){
		$setDefaultCheck = 'checked';
		$setHouseCheck = '';
		$houseDiv ='display:none';
	}
	else{
		$setDefaultCheck = '';
		$setHouseCheck = 'checked';
		$houseDiv ='display:block';
	}
}
else{
	$setDefaultCheck = 'checked';
	$setHouseCheck = '';
}

#build checkbox
$houseChkbox = '';
$houseChkbox .= "<div id=\"house\" name=\"house\" style=\"$houseDiv;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	foreach($HouseAssoArr as $ID => $name){
		if(in_array($ID,$houseMapping)){
			$houseChk = 1;
		}
		else{
			$houseChk = 0;
		}
		$houseChkbox .= $linterface->Get_Checkbox('house_Id_'.$ID, 'houseIdAry[]', $Value=$ID, $isChecked=$houseChk, $Class='', $Display=$name, $Onclick='', $Disabled='');
	}
$houseChkbox .= "</div>";

###############################################################################################


# Last selection - PIC
###############################################################################################
$picArr = $libenroll->GET_GROUPSTAFF($EnrollGroupArr['EnrolGroupID'], "PIC");
$PICSel = "<select name='pic[]' id='pic[]' size='6' multiple='multiple'>";

for($i = 0; $i < sizeof($picArr); $i++) {
	/*
	$username_field = getNameFieldByLang("a.");
	$Sql = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) FROM INTRANET_USER AS a WHERE a.UserID = '".$picArr[$i][0]."'";
	$TempArr = $libenroll->returnArray($Sql);
	debug_r($TempArr);
	*/
	$PICSel .= "<option value=\"".$picArr[$i][0]."\">".$LibUser->getNameWithClassNumber($picArr[$i][0], $IncludeArchivedUser=1)."</option>";
}
$PICSel .= "</select>";

//if (sizeof($picArr)>0)
//{
	$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");
//}
###############################################################################################


# Last selection - helper
###############################################################################################
$helperArr = $libenroll->GET_GROUPSTAFF($EnrollGroupArr['EnrolGroupID'], "HELPER");
$HELPERSel = "<select name='helper[]' id='helper[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($helperArr); $i++) {
	/*
	$username_field = getNameFieldByLang("a.");
	$Sql = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) FROM INTRANET_USER AS a WHERE a.UserID = '".$picArr[$i][0]."'";
	$TempArr = $libenroll->returnArray($Sql);
	debug_r($TempArr);
	*/
	$HELPERSel .= "<option value=\"".$helperArr[$i][0]."\">".$LibUser->getNameWithClassNumber($helperArr[$i][0], $IncludeArchivedUser=1)."</option>";
}
$HELPERSel .= "</select>";

//if (sizeof($helperArr)>0)
//{
	$button_remove_html_helper = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['helper[]'])");
//}
###############################################################################################

$libenrolllass = new libclass();
$ClassLvlArr = $libenrolllass->getLevelArray();
$GroupArr = $libenroll->GET_GROUPCLASSLEVEL($EnrollGroupArr['EnrolGroupID']);
$GroupYearID = Get_Array_By_Key($GroupArr,"ClassLevelID");

if ($sys_custom['eEnrolment']['HideClassLevel']) {
	$ClassLvlChk = "<input type=\"hidden\" id=\"classlvlall\">";
	for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
		$checked = "";
		for ($j = 0; $j < sizeof($GroupArr); $j++) {
			
			if (in_array($ClassLvlArr[$i][0],$GroupYearID)) {
				$checked = " checked ";
				break;
			}
		}
		$ClassLvlChk .= "<input type=\"hidden\" id=\"classlvl{$i}\" name=\"classlvl[]\" value=\"".$ClassLvlArr[$i][0]."\">";
	}
} else {
	$ClassLvlChk = "<input type=\"checkbox\" id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.form1, 'classlvl[]') : setChecked(0, document.form1, 'classlvl[]');\"><label for=\"classlvlall\">".$eEnrollment['all']."</label>";
	$ClassLvlChk .= "<table class=\"form_table_v30\">";
	for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
		$checked = "";
		for ($j = 0; $j < sizeof($GroupArr); $j++) {	
			
			if (in_array($ClassLvlArr[$i][0],$GroupYearID)) {	
				 $checked = " checked ";
				 break;
			}
		}
		
		if (($i % 10) == 0) $ClassLvlChk .= "<tr>";
		$ClassLvlChk .= "<td class=\"tabletext\"><input type=\"checkbox\" $checked id=\"classlvl{$i}\" name=\"classlvl[]\" value=\"".$ClassLvlArr[$i][0]."\"><label for=\"classlvl{$i}\">".$ClassLvlArr[$i][1]."</label></td>";
		if (($i % 10) == 9) $ClassLvlChk .= "</tr>";
	}
	$ClassLvlChk .= "</table>";
}


if ($EnrollGroupArr['LowerAge'] != 0) $LowerAge = $EnrollGroupArr['LowerAge'];
$LowerAgeSel = "<select name=\"LowerAge\" id=\"LowerAge\" onclick=\"jsSelectAgeRange();\">";
for ($i = 11; $i < 21; $i++) {
	$LowerAgeSel .= "<option value=\"$i\"";
	if ($LowerAge == $i) $LowerAgeSel .= " selected ";
	$LowerAgeSel .= ">".$i."</option>";
}
$LowerAgeSel .= "</select>";

if ($EnrollGroupArr['UpperAge'] != 0) $UpperAge = $EnrollGroupArr['UpperAge'];
$UpperAgeSel = "<select name=\"UpperAge\" id=\"UpperAge\" onclick=\"jsSelectAgeRange();\">";
for ($i = 11; $i < 21; $i++) {
	$UpperAgeSel .= "<option value=\"$i\"";
	if (($UpperAge == $i)||(($i == 20)&&(!$UpperAgeSelected))) {
		$UpperAgeSelected = true;
		$UpperAgeSel .= " selected ";
	}
	$UpperAgeSel .= ">".$i."</option>";
}
$UpperAgeSel .= "</select>";


$filecount = 0;
for ($i = 10; $i < 15; $i++) {
	if ($EnrollGroupArr[$i] != "") $filecount++;
}

## Fee - check if to be confirmed
if ($EnrollGroupArr['isAmountTBC'] == 1)
{
	$TBC_checked = true;
	$Amount_disabled = 'disabled';
}
else
{
	$TBC_checked = false;
	$Amount_disabled = '';
}

# Get HTML editor
$msg_box_width = 600;
$msg_box_height = 200;
$obj_name = "Discription";
$editor_width = $msg_box_width;
$editor_height = $msg_box_height;

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$oFCKeditor = new FCKeditor($obj_name,$editor_width,$editor_height);
$oFCKeditor->Value =  $EnrollGroupArr['Description'];
$HTMLEditor = $oFCKeditor->Create2();


### Apply Settings to Club Of Other Terms
$groupClubInfoAry = $libenroll->Get_Club_Info_By_GroupID($GroupID);
// debug_pr($groupClubInfoAry);
$DateInput = $groupClubInfoAry[0]['DateInput'];
$InputBy = $groupClubInfoAry[0]['InputBy'];
if ($InputBy == Null){
	$InputBy = $Lang['General']['EmptySymbol'];
} else{
	$LibUser = new libuser($InputBy);
	$CreatorEnglishName = $LibUser->EnglishName;
	$CreatorChineseName = $LibUser->ChineseName;
	$InputBy = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
}

if ($DateInput == Null){
	$DateInput = $Lang['General']['EmptySymbol'];
}

$ModifiedBy = $groupClubInfoAry[0]['ModifiedBy'];
$DateModified = $groupClubInfoAry[0]['DateModified'];
if ($ModifiedBy == Null){
	$ModifiedBy = $Lang['General']['EmptySymbol'];
} else{
	$LibUser = new libuser($ModifiedBy);
	$CreatorEnglishName = $LibUser->EnglishName;
	$CreatorChineseName = $LibUser->ChineseName;
	$ModifiedBy = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
}
if ($DateModified == Null){
	$DateModified = $Lang['General']['EmptySymbol'];
}

$numOfGroupClub = count($groupClubInfoAry);
$applySettingsToClubOfOtherTermTr = '';
if ($numOfGroupClub > 1) {
	$applySettingsToClubOfOtherTermTr .= '	<tr>
												<td colspan="2"><br/><br/></td>
											</tr>
											<tr>
												<td class="field_title">'.$Lang['eEnrolment']['ApplySettingsToClubOfOtherTerms'].'</td>
												<td><input type="checkbox" id="applySettingsToClubOfOtherTerms" name="applySettingsToClubOfOtherTerms" value="1" /></td>
											</tr>';	
}


### Category List for OLE category mapping
$categoryAry = $libenroll->GET_CATEGORY_LIST();
$categoryAssoAry = BuildMultiKeyAssoc($categoryAry, 'CategoryID', 'OleCategoryID', $SingleValue=1);
$x = '';
$x .= 'var categoryMappingAry = new Array();'."\n";
foreach ((array)$categoryAssoAry as $_categoryId => $_oleCategoryId) {
	$x .= 'categoryMappingAry['.$_categoryId.'] = \''.$_oleCategoryId.'\';'."\n";
}
$htmlAry['initializeCategoryMappingAry'] = $x;

### Round Selection
if($plugin['SIS_eEnrollment']){
	$roundSelectHTML = '';
	for($i= 1 ;$i<=$sis_eEnrollmentConfig['totalRound']; $i++){
		if($EnrollGroupArr['RoundNumber'] == $i){
			$selected = ' checked';
		}else{
			$selected = '';
		}
		$roundSelectHTML .= <<<HTML
			<input type="radio" name="Round" id="Round{$i}" value="{$i}" {$selected}/>
			<label for="Round{$i}">{$Lang['SIS_eEnrollment']['editCCA']['Round'][$i]}</label>
HTML;
	}
}
#P110324
if($sys_custom['eEnrolment']['ReplySlip']){
	//Get ALL the ReplySlip Related to this Group
	$rs = $libenroll->getReplySlipEnrolGroupRelation('',$EnrolGroupID);
	$ReplySlipGroupMappingID = $rs[0]['ReplySlipGroupMappingID'];
	if($ReplySlipGroupMappingID){
		$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
	}
	$ReplySlipID = $rs[0]['ReplySlipID'];
	//Get ReplySlip Detail
	if($ReplySlipID){
		$ReplySlipInfo = $libenroll->getReplySlip($ReplySlipID);
	}
	
	$replyFormHTML = '';
	$replyFormHTML .= '<tr>';
	$replyFormHTML .= '<td class="field_title">'.$eEnrollment['replySlip']['replySlip'].'</td>';
	$replyFormHTML .= '<td>';
	if(!$ReplySlipReply){
		$replyFormHTML .= '<input type="button" class="formsubbutton" onclick="newWindow(\'ReplySlip_Edit.php\',1)" value="'.$eEnrollment['replySlip']['Edit'].'" onmouseover="this.className=\'formsubbutton\'" onmouseout="this.className=\'formsubbutton\'">';
	}
	$replyFormHTML .= '<input type="button" class="formsubbutton" onclick="newWindow(\'ReplySlip_Preview.php\',10)" value="'.$eEnrollment['replySlip']['Preview'].'" onmouseover="this.className=\'formsubbutton\'" onmouseout="this.className=\'formsubbutton\'">';
	$replyFormHTML .= '</td>';
	$replyFormHTML .= '</tr>';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipID" value="'.$ReplySlipID.'">';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipTitle" value="'.$ReplySlipInfo[0]['Title'].'">';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipInstruction" value="'.$ReplySlipInfo[0]['Instruction'].'">';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipRecordType" value="'.$rs[0]['RecordType'].'">';
	$replyFormHTML .= '<input type="hidden" name="qStr" value="'.$ReplySlipInfo[0]['Detail'].'">';
	$replyFormHTML .= '<input type="hidden" name="aStr" value="">';
	$replyFormHTML .= '<input type="hidden" name="DisplayQuestionNumber" value="'.$ReplySlipInfo[0]['DisplayQuestionNumber'].'">';
	$replyFormHTML .= '<input type="hidden" name="AllFieldsReq" value="'.$ReplySlipInfo[0]['AllFieldsReq'].'">';
}
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/html_editor/fckeditor_cust.js" ></script>
<script type="text/javascript" >

<?=$htmlAry['initializeCategoryMappingAry']?>

$(document).ready(function () {
	clickedIntExtRadio();
	<?php for($i=1;$i<=10;$i++){
	    $temp_str = "AttachmentLink". $i;
	    if($EnrollGroupArr[$temp_str]!=""){
	        $url =  $file_path. "/file/" . $EnrollGroupArr[$temp_str];
// 	        echo "filelinks[$i] = '/file/" . $EnrollGroupArr[$temp_str] . "';\n";
	        echo "filelinks[$i] = '../../../../home/download_attachment.php?target_e=".getEncryptedText($url)."';\n";
	        echo "filenames[$i] = '" .addslashes($libenroll->ShowFileName($EnrollGroupArr[$temp_str])) . "';\n";
	    }
	}?>
	initAttachment();
	if(emptyfields.length > 0){
		popAttachment();
	}
});
	var filecount = <?=$filecount?>;
	var filelinks = [];
	var filenames = [];
	var emptyfields = [];
	function initAttachment(){
		for(var i=1; i<=10; i++){
			if(filelinks[i]!=null) existedAttachment(i);
			else {
				emptyfields.push(emptyAttachment(i));
			}
		}
	}
	function emptyAttachment(index){
		var a = document.createElement('input');
		a.setAttribute('type', 'file');
		a.setAttribute('id', 'attach[]');
		a.setAttribute('name', 'attach[]');
		a.setAttribute('class', 'textboxtext');
		var onchangeScript = "var Ary";
		onchangeScript += index-1;
		onchangeScript += " = this.value.split('\\\\'); document.getElementById('attachname";
		onchangeScript += index-1;
		onchangeScript += "').value = Ary" ;
		onchangeScript += index-1; 
		onchangeScript += "[Ary";
		onchangeScript += index-1;
		onchangeScript += ".length-1];";
		console.log(onchangeScript);
		a.setAttribute('onChange', onchangeScript);
		var b = document.createElement('input');
		b.setAttribute('type', 'hidden');
		var attachname = "attachname" + (index-1);
		b.setAttribute('name', attachname);
		b.setAttribute('id', attachname);
		return {a:a, b:b};
	}
	function popAttachment(){
		var obj = emptyfields.shift();
		<?php $button = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button","$(this).hide();popAttachment();","addNewAttachment")); ?>
		var btn = '<?=$button?>';
		var td = $('#attachmentTD');
		td.append(obj.a).append(obj.b);
		if(emptyfields.length > 0){
			td.append(btn);
		}
	}
	function existedAttachment(index){
		var td = $('#attachmentTD');
		var a = document.createElement('a');
		a.setAttribute('href', filelinks[index]);
		a.innerHTML = filenames[index];
		var b = document.createElement('a');
		b.setAttribute('href', "<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>");
		b.setAttribute('onClick', "document.getElementById('FileToDel').value='"+index+"';document.form1.action='file_delete_update.php';");
		b.setAttribute('class', 'tablelink');
		b.innerHTML = "<?=$button_remove?>";
		td.append($(a));
		td.append('&nbsp;(');
		td.append($(b));
		td.append(')<br/>');
	}

function getEditorValue(instanceName) {
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	
	// Get the editor contents as XHTML.
	return oEditor.GetXHTML( true ) ; // "true" means you want it formatted.
}

function FormSubmitCheck(obj)
{
// 	if( word_count("Discription").total== 0 )
// 	{
//		alert("<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_content']; ?>");
// 		return false;
// 	}
	if (obj.sel_category.value == '') {
		alert("<?php echo $i_alert_pleaseselect.$eEnrollment['add_activity']['act_category']; ?>.");
		return false;
	}
	
	//check target house
	if($('input#set_house').attr('checked')){
		var counter = 0;
		$('[name="houseIdAry[]"]').each(function() {
			if($(this).attr('checked')){
				counter++;
			}
		});
		if(counter == 0){
			alert('<?=$Lang['eEnrolment']['jsWarning']['SpecifyHouse']?>');
			return false;
		}	
	}	
	
	if ($('#LowerAge').val() > $('#UpperAge').val())
	{
		alert('<?=$Lang['eEnrolment']['Warning']['AgeRangeInvalid']?>');
		return false;
	}
	
	if (!check_positive_int(obj.PaymentAmount,"<?=$eEnrollment['js_fee_positive']?>"))
		return false;
		
	if(!check_positive_int(obj.Quota,"<?=$ec_warning['please_enter_pos_integer']?> (<?=$eEnrollment['js_zero_means_no_limit']?>)","",0))
	{
    	return false;	
	}

	if(!check_text(obj.PaymentAmount, "<?php echo $i_alert_pleasefillin.$eEnrollment['PaymentAmount']; ?>.")) return false;
	
	// Target Group Checking - at least select one target group
	var i;
	var allNotChecked = true;
	for (i=0; i< <?=sizeof($ClassLvlArr)?>; i++)
	{
		var classLevelChkBox = document.getElementById('classlvl'+i);
		if (classLevelChkBox.checked)
		{
			allNotChecked = false;
		}
	}
	if (allNotChecked)
	{
		alert('<?=$eEnrollment['js_target_group_alert']?>');
		return false;	
	}
	
	/*
	if (confirm("<?= $eEnrollment['js_del_warning']?>")) {
		obj.submit();
	} else {
		return false;
	}
	*/
	
	// enable back the payment amount so that the value can be saved
	document.getElementById('PaymentAmount').disabled = false;
	
	/*--------------- check pic & helper -----------------*/
	var jsPICArr = document.getElementById('pic[]');
	var jsNumOfPIC = jsPICArr.length;
	
	if (jsNumOfPIC == 0) {
		alert('<?= $eEnrollment['js_sel_pic']?>');
		return false;
	}
	
	var jsHelperArr = document.getElementById('helper[]');
	var jsNumOfHelper = jsHelperArr.length;
	
	// Check if PIC and helper duplicated
	if (jsNumOfPIC > 0 && jsNumOfHelper > 0)
	{
		var i;
		var j;
		
		for (i=0; i<jsNumOfHelper; i++)
		{
			for (j=0; j<jsNumOfPIC; j++)
			{
				if (jsHelperArr[i].value == jsPICArr[j].value && jsHelperArr[i].value != '' && jsPICArr[j].value != '')
				{
					alert("<?=$Lang['eEnrolment']['jsWarning']['DuplicatedPersonInPicAndHelper']?>");
					return false;
				}
			}
		}
	}
	
	<? if($plugin['iPortfolio']) {?>
	/*
		if(obj.category.selectedIndex==0) {
			alert("<?=$i_alert_pleaseselect." ".$eEnrollment['OLE_Category']?>");
			return false;
		}
	
		var total = countChecked(obj, 'ele_global[]');
		if(total==0) {
			alert("<?=$i_alert_pleaseselect." ".$eEnrollment['OLE_Components']?>");
			return false;
		}
	*/
	<?}?>
	
	if ($('input#applySettingsToClubOfOtherTerms').attr('checked')) {
		if (!confirm('<?=$Lang['eEnrolment']['ApplySettingsToClubOfOtherTermsWarning']?>')) {
			return false;
		}
	}
	
	generalFormSubmitCheck(obj);
	
	<? if(IntegerSafe($DirectToMeeting)){?>
		if(confirm("<?=$Lang['eEnrolment']['DirectToDateSetting']?>"))
		{
			obj.DirectToMeetingGroup.value = 1;
		} 
	<? } ?>
	obj.submit();
	
}

function jsSelectAgeRange()
{
	$('#set_age_range').attr('checked', true);
}

function js_ClickFeeConfirmed()
{
	if (document.getElementById('isAmountTBC').checked == false)
	{
		document.getElementById('PaymentAmount').disabled = false;
	}
	else
	{
		document.getElementById('PaymentAmount').disabled = true;
	}
}

function generalFormSubmitCheck(obj)
{	
	checkOptionAll(obj.elements["pic[]"]);
	checkOptionAll(obj.elements["helper[]"]);
}

function clickedIntExtRadio() {
	var isINT = $('#intExt_INT').attr('checked');
	
	if (isINT) {
		$('.eleChk').attr('disabled', '');
	}
	else {
		$('.eleChk').attr('disabled', 'disabled');
	}
}

function changedCategorySelection() {
	var categoryId = $('select#sel_category').val();
	var oleCategoryId = categoryMappingAry[categoryId]; 
	
	if (oleCategoryId != 0 && oleCategoryId != '') {
		var oleCategorySelId = 'category_' + $('input#EnrolGroupID').val();
		$('select#' + oleCategorySelId).val(oleCategoryId);
	}
}

function houseSelection(val) {
	if(val == '0') {
		$('div#house').css('display', 'none');
		$('[name="houseIdAry[]"]').attr('checked', false);
	}
	else{
		$('div#house').css('display', 'block');
	}
}

</script>

<form name="form1" action="group_setting_update.php" method="POST" enctype="multipart/form-data">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table border="0" class="form_table_v30">
<tr><td>
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td colspan="2"><?= $linterface->GET_NAVIGATION2($Step1) ?><br/>
		</td>
	</tr>
<?php 
if($sys_custom['eEnrolment']['TWGHCYMA']){
	$intranet_groups = $libenroll->Get_Intranet_Groups($EnrollGroupArr['AcademicYearID']);
	$intranet_group_selection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($intranet_groups, ' id="RelatedGroupID" name="RelatedGroupID" ', $Lang['General']['NA'], $EnrollGroupArr['RelatedGroupID']);
	$related_group_row = '<tr>
							<td class="field_title">'.$Lang['eEnrolment']['BelongToGroup'].' <span class="tabletextrequire">*</span></td>
							<td>'.$intranet_group_selection.'</td>
						</tr>';
	echo $related_group_row;
}
?>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $i_ClubsEnrollment_ClubName?></span></td>
		<td><?=$TitleDisplay?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_content']?> <span class="tabletextrequire"></span></td>
		<td><?=$HTMLEditor?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['Location']?> </td>
		<td class="tabletext">
			<input type="text" id="Location" name="Location" value="<?=intranet_htmlspecialchars($EnrollGroupArr['Location'])?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_category']?> <span class="tabletextrequire">*</span></td>
		<td>
		<?= $libenroll->GET_CATEGORY_SEL($EnrollGroupArr['GroupCategory'], 1, "changedCategorySelection();", $button_select,0,$categoryTypeID)?></td>
	</tr>
<?php if ($sys_custom['eEnrolment']['HideClassLevel']) {
	echo $ClassLvlChk;
} else { ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_target']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext"><?= $ClassLvlChk?></td>
	</tr>
<?php } ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eEnrolment']['TargetHouse']?><span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" id="set_house_limit" name="set_house" value="0" onclick="houseSelection('0');" <?=$setDefaultCheck?> ><label for="set_house_limit"> <?= $eEnrollment['no_limit']?></label><br/>
			<input type="radio" id="set_house" name="set_house" onclick="houseSelection('1');" value="1" <?=$setHouseCheck?>>
			<label for="set_house">
				<?=$eEnrollment['Specify']?><br>
				<?=$houseChkbox?>
			</label>
		</td>
	</tr>
	
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_age']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" id="set_age_limit" name="set_age" value="0" checked><label for="set_age_limit"> <?= $eEnrollment['no_limit']?></label><br/>
			<input type="radio" id="set_age_range" name="set_age" value="1" <? if (($EnrollGroupArr['UpperAge'] > 0)||($EnrollGroupArr['LowerAge'] > 0)) print "checked"; ?>>
			<?= $LowerAgeSel?>
			<label for="set_age_range">
				&nbsp;-&nbsp;
			</label>
			<?= $UpperAgeSel?>
			<label for="set_age_range">
				&nbsp;<?= $eEnrollment['add_activity']['age']?>
			</label>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_gender']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" name="gender" id="gender_all" value="A" checked> <label for="gender_all"><?= $eEnrollment['all']?></label>
			<input type="radio" name="gender" id="gender_m" value="M" <? if ($EnrollGroupArr['Gender'] == "M") print "checked"?>> <label for="gender_m"><?= $eEnrollment['male']?></label>
			<input type="radio" name="gender" id="gender_f" value="F" <? if ($EnrollGroupArr['Gender'] == "F") print "checked"?>> <label for="gender_f"><?= $eEnrollment['female']?></label>
		</td>
	</tr>
	<?php if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){ ?>
		<input type="hidden" id="PaymentAmount" name="PaymentAmount" value="0" />
		<input type="hidden" id="isAmountTBC" name="isAmountTBC" value="1" />
	<?php }else{ ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['PaymentAmount']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="text" id="PaymentAmount" name="PaymentAmount" value="<?= (0+$EnrollGroupArr['PaymentAmount'])?>" class="textboxnum" <?=$Amount_disabled?>>
			&nbsp;&nbsp;
			<?= $linterface->Get_Checkbox("isAmountTBC", "isAmountTBC", 1, $TBC_checked, $Class="", $eEnrollment['ToBeConfirmed'], $Onclick="js_ClickFeeConfirmed()"); ?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['club_quota']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="text" id="Quota" name="Quota" value="<?= (0+$EnrollGroupArr['Quota'])?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['MinimumMemberQuota']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="text" id="MinQuota" name="MinQuota" value="<?= (0+$EnrollGroupArr['MinQuota'])?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['attachment']?></td>
		<td class="tabletext" id="attachmentTD"></td>
		<!-- <td class="tabletext">
		<? if ($EnrollGroupArr['AttachmentLink1'] == "")  { ?>
			<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary = this.value.split('\\'); document.getElementById('attachname0').value=Ary[Ary.length-1];"><input type="hidden" name="attachname0" id="attachname0">
		<? } else {?>
			<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink1']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink1'])?></a>&nbsp;(<a href="<?= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();"?>" onClick="document.getElementById('FileToDel').value='1'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
		<? } ?>
			<? if ($filecount == 0) { ?> <a class="tablelink" href="#more_file_a" onClick="javascript:document.getElementById('more_file').style.display = 'block';document.getElementById('tohide').style.display = 'none'"><span id="tohide"><?= $eEnrollment['add_activity']['more_attachment']?></span></a> <? } ?>
			<div id="more_file" style="display : <? ($filecount > 0) ? print "block": print "none"; ?>"><a name="more_file_a"></a>
			<? if ($EnrollGroupArr['AttachmentLink2'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary1 = this.value.split('\\'); document.getElementById('attachname1').value=Ary1[Ary1.length-1];"><input type="hidden" name="attachname1" id="attachname1">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink2']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink2'])?></a>&nbsp;(<a href="<?= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();"?>" onClick="document.getElementById('FileToDel').value='2'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr['AttachmentLink3'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary2 = this.value.split('\\'); document.getElementById('attachname2').value=Ary2[Ary2.length-1];"><input type="hidden" name="attachname2" id="attachname2">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink3']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink3'])?></a>&nbsp;(<a href="<?= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();"?>" onClick="document.getElementById('FileToDel').value='3'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr['AttachmentLink4'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary3 = this.value.split('\\'); document.getElementById('attachname3').value=Ary3[Ary3.length-1];"><input type="hidden" name="attachname3" id="attachname3">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink4']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink4'])?></a>&nbsp;(<a href="<?= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();"?>" onClick="document.getElementById('FileToDel').value='4'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr['AttachmentLink5'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary4 = this.value.split('\\'); document.getElementById('attachname4').value=Ary4[Ary4.length-1];"><input type="hidden" name="attachname4" id="attachname4">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink5']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink5'])?></a>&nbsp;(<a href="<?= ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();"?>" onClick="document.getElementById('FileToDel').value='5'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			</div>
		</td>-->
	</tr>
	<?php if($plugin['SIS_eEnrollment']){ ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['SIS_eEnrollment']['Round']?> <span class="tabletextrequire">*</span></td>
			<td class="tabletext">
				<?=$roundSelectHTML?>
			</td>
		</tr>
	<?php } ?>
	<?=$replyFormHTML?>
	<tr>
		<td colspan="2"><br><?= $linterface->GET_NAVIGATION2($Step2) ?><br/>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title" width="30%"><?= $eEnrollment['add_activity']['act_pic']?> <span class="tabletextrequire">*</span></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $PICSel?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/enrolment_pic_helper.php?fieldname=pic[]&ppl_type=pic&permitted_type=1,2', 9)")?><br />
				&nbsp;<?=$button_remove_html?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_assistant']?></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $HELPERSel?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/enrolment_pic_helper.php?fieldname=helper[]&ppl_type=helper&permitted_type=1,2', 9)")?><br />
				&nbsp;<?=$button_remove_html_helper?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	
<?	
/////>>>>>
	$SetRightTitle = $eEnrollment['add_activity']['step_3'];
	if ($sys_custom['eEnrolment']['ClubTargetApplicant']) {
?>	
	<tr>
		<td colspan="2">
			<br>
			<?=$linterface->GET_NAVIGATION2($SetRightTitle)?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['apply_ppl_type']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" name="ApplyUserType" id="student" value="S" checked> <label for="student"><?= $eEnrollment['add_activity']['student_enroll']?></label>
			<input type="radio" name="ApplyUserType" id="parent" value="P" <? if ($EnrollGroupArr['ApplyUserType'] == "P" || $_SESSION["platform"] == "KIS") print "checked"; ?> > <label for="parent"><?= $eEnrollment['add_activity']['parent_enroll']?></label><br/>
		</td>
	</tr>
<?
	}
/////<<<<<
?>
	<? if($_SESSION["platform"] != "KIS"){?>
	<?
	if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
	    //noop
	}else if($plugin['iPortfolio']) {
		include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
		$libenroll_ui = new libclubsenrol_ui();
		echo $libenroll_ui->Display_Enrolment_Default_OLE_Setting('club', array($EnrolGroupID));	
	}
	?>
	<?=$applySettingsToClubOfOtherTermTr?> 
 
       </td>
     </tr> 
     
     
     <tr>
		<td colspan="2"><br><?= $linterface->GET_NAVIGATION2($Step3) ?><br/>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['WebSAMSSTACode']?></td>
		<td class="tabletext">
			<input type="text" id="WebSAMSSTACode" name="WebSAMSSTACode" value="<?= $EnrollGroupArr['WebSAMSCode']?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['WebSAMSSTAType']?></td>
		<td class="tabletext">
		<?php 
			$ECheck = '';
			$SCheck = '';
			$ICheck = '';
			$Check = '';
			if($EnrollGroupArr['WebSAMSSTAType'] == 'E'){
				$ECheck = 'selected';
			}else if($EnrollGroupArr['WebSAMSSTAType'] == 'S' ){
				$SCheck = 'selected';
			}else if($EnrollGroupArr['WebSAMSSTAType'] == 'I'){
				$ICheck = 'selected';
			}else{
				$Check = 'selected';
			}
		
		?>
			<select id="WebSAMSSTAType" name="WebSAMSSTAType">
			
				<option name="STAType" <?=$Check?> value = "" ><?=Get_Selection_First_Title($Lang['General']['PleaseSelect'])?></option>
				<option name="STAType" <?=$ECheck?> value = "E" ><?= $eEnrollment['WebSAMSSTA']['ECA']?></option>
				<option name="STAType" <?=$SCheck?> value = "S" ><?= $eEnrollment['WebSAMSSTA']['ServiceDuty']?></option>
				<option name="STAType" <?=$ICheck?> value = "I" ><?= $eEnrollment['WebSAMSSTA']['InterSchoolActivities']?></option>
			</select>
		</td>
	</tr>
	
     <? } ?>
     
     <tr>
     
		<td colspan="2"><br><?= $linterface->GET_NAVIGATION2($Step4) ?><br/>
		</td>
	</tr>
     <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['CreateUserName']?></td>
		<td> 
		<?php echo $InputBy?>
     	</td>
     </tr>
      <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['CreateUserDate']?></td>
		<td> 
			<?php echo $DateInput?>
     	</td>
     </tr>    
          <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['LastModifiedBy']?></td>
		<td> 
		<?php echo $ModifiedBy?>
     	</td>
     </tr>
      <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['LastModified']?></td>
		<td> 
			<?php echo $DateModified?>
     	</td>
     </tr>   
     
     
     <tr>
     <td colspan="2"> 
     <?=$i_general_required_field?>
      </td>
     </tr>
     
     <tr>
     <td colspan="2">
     <div class="edit_bottom_v30">
<?if($libenroll->AllowToEditPreviousYearData) {?>
<?= $linterface->GET_ACTION_BTN($button_save, "button", ($libenroll->disableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: FormSubmitCheck(document.form1)")?>&nbsp;
<?}?>
<?// if (sizeof($EnrollGroupArr) > 0) print $linterface->GET_ACTION_BTN($eEnrollment['skip'], "button", "self.location='group_new1b.php?EnrolGroupID=$EnrolGroupID&Semester=$Semester'")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='group.php?".$cancel_btn_para."'")?>
</div>
 </td>
 </tr>
</table>

</td></tr>
</table>
 

 

<input type="hidden" name="flag" value="0" />
<input type="hidden" name="GroupID" id="GroupID" value="<?= $GroupID?>" />
<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="<?= $EnrolGroupID?>" />
<input type="hidden" name="Semester" id="Semester" value="<?= $Semester?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />
<input type="hidden" name="DirectToSetting" id="DirectToSetting" />
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>"/>
<?if(IntegerSafe($DirectToMeeting)){?>
	<input type="hidden" name="DirectToMeetingGroup" id="DirectToMeetingGroup" />
<?}?>
</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>