<?
# Using: 

###################################
#	Date:	2017-04-18 Villa
#			Create the File, copied and modified from eNotice
#
###################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();

$AllFieldsReq = $_POST['AllFieldsReq'];
$DisplayQNum = $_POST['DisplayQNum'];
$CurrentQStr = $_POST['CurrentQStr'];
$ReplySlipTitle = $_POST['ReplySlipTitle'];
$libenroll->saveReplySlip($ReplySlipID='',$Title=$ReplySlipTitle,$Detail=$CurrentQStr,$DisplayQuestionNumber=$DisplayQNum,$AllFieldsReq=$AllFieldsReq,$IsTemplate =1,$IsDeleted='');
?>