<?php
// Using: Ivan

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
if (!$libenroll->hasAccessRight($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");


$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
	if (!$isEnrolAdmin)
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$linterface = new interface_html();
        
    # tags 
    $tab_type = "club";
    $current_tab = 1;
    include_once("management_tabs.php");
  
    $linterface->LAYOUT_START();
    
    $FromStep2 = $_REQUEST['FromStep2'];
    $FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
    $ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
    $TermMappingArr =  unserialize(rawurldecode($_REQUEST['TermMappingArr']));
    $CopyClubArr =  unserialize(rawurldecode($_REQUEST['CopyClubArr']));
    $CopyMemberArr = unserialize(rawurldecode( $_REQUEST['CopyMemberArr']));
    $CopyActivityArr =  unserialize(rawurldecode($_REQUEST['CopyActivityArr']));
    
   echo $libenroll_ui->Copy_Club_Step1_UI($FromStep2, $FromAcademicYearID, $ToAcademicYearID, $TermMappingArr, $CopyClubArr, $CopyMemberArr, $CopyActivityArr);
				
?>

<script language="javascript">
function js_Change_Checkbox_Status(jsChecked, jsCheckBoxClass)
{
	$('input.' + jsCheckBoxClass + ':enabled').each( function() {
		$(this).attr('checked', jsChecked);
	})
	
	// enable / disable the "Copy Member" and "Copy Activity" checkboxes if the Copy Club option is changed
	if (jsCheckBoxClass=='CopyClubChk')
	{
		var jsDisabled = (jsChecked==true)? false : true;
		
		$('input.CopyMemberChk').each( function() {
			$(this).attr('disabled', jsDisabled);
		})
		
		$('input.CopyActivityChk').each( function() {
			$(this).attr('disabled', jsDisabled);
		})
	}
}

function js_Check_Uncheck_Global_Selection(jsChecked, jsGlobalID, jsEnrolGroupID)
{
	if (jsChecked == false)
		$('input#' + jsGlobalID).attr('checked', jsChecked);
		
	if (jsGlobalID == 'CopyClub_Global')
	{
		var jsDisabled = (jsChecked==true)? false : true;
		
		$('input#CopyMemberChk_' + jsEnrolGroupID).attr('disabled', jsDisabled);
		$('input#CopyActivityChk_' + jsEnrolGroupID).attr('disabled', jsDisabled);
	}
}

function js_Changed_FromAcademicYear_Selection(jsFromAcademicYearID)
{
	var jsToAcademicYearID = $('select#ToAcademicYearID').val();
	
	js_Reload_Selection('ToAcademicYearSelection', 'ToAcademicYearID', 'onchange="js_Changed_ToAcademicYear_Selection(this.value);"', jsToAcademicYearID, jsFromAcademicYearID, 1);
	js_Reload_Term_Mapping_Table(jsFromAcademicYearID, jsToAcademicYearID);
	js_Reload_Club_Selection_Table(jsFromAcademicYearID, jsToAcademicYearID);
}

function js_Changed_ToAcademicYear_Selection(jsToAcademicYearID)
{
	var jsFromAcademicYearID = $('select#FromAcademicYearID').val();
	
	js_Reload_Selection('FromAcademicYearSelection', 'FromAcademicYearID', 'onchange="js_Changed_FromAcademicYear_Selection(this.value);"', jsFromAcademicYearID, jsToAcademicYearID, 0);
	js_Reload_Term_Mapping_Table(jsFromAcademicYearID, jsToAcademicYearID);
	js_Reload_Club_Selection_Table(jsFromAcademicYearID, jsToAcademicYearID);
}

function js_Reload_Term_Mapping_Table(jsFromAcdemicYearID, jsToAcademicYearID)
{
	var DivID = 'TermMappingDiv';
	$('div#' + DivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$('div#' + DivID).load(
		"ajax_reload.php", 
		{ 
			Action: 'Copy_Club_Term_Mapping_Table',
			FromAcademicYearID: jsFromAcdemicYearID,
			ToAcademicYearID: jsToAcademicYearID
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Reload_Club_Selection_Table(jsFromAcdemicYearID, jsToAcdemicYearID)
{
	var DivID = 'ClubSelectionDiv';
	$('div#' + DivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
	$('div#' + DivID).load(
		"ajax_reload.php", 
		{ 
			Action: 'Copy_Club_Club_Selection_Table',
			FromAcademicYearID: jsFromAcdemicYearID,
			ToAcademicYearID: jsToAcdemicYearID
		},
		function(ReturnData)
		{
			
		}
	);
}

function js_Reload_Selection(jsDivID, jsSelectionID, jsTag, jsAcademicYearID, jsExcludeAcademicYearID, jsNoPastYear)
{
//	$('div#' + jsDivID).html('<?=$linterface->Get_Ajax_Loading_Image()?>');
//	$('div#' + jsDivID).load(
//		"ajax_reload.php", 
//		{ 
//			Action: 'Copy_Club_Academic_Year_Selection',
//			SelectionID: jsSelectionID,
//			Tag: jsTag,
//			AcademicYearID: jsAcademicYearID,
//			NoPastYear: jsNoPastYear
//		},
//		function(ReturnData)
//		{
//			
//		}
//	);
}

function js_Go_Back()
{
	window.location = "group.php";
}

function js_Submit_Form()
{
	if ($('select#FromAcademicYearID').val() == $('select#ToAcademicYearID').val())
	{
		alert('<?=$Lang['Group']['warnSelectDifferentYear']?>');
		$('select#FromAcademicYearID').focus();
		return false;
	}
	
	if ($('input.CopyClubChk:checked').length == 0)
	{
		alert('<?=$Lang['eEnrolment']['CopyClub']['jsWarningArr']['SelectAtLeastOneClub']?>');
		return false;
	}
	
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'copy_club_step2.php';
	ObjForm.submit();
}
</script>

<?
	$linterface->LAYOUT_STOP();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>