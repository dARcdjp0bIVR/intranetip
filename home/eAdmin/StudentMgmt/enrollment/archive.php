<?php
// USING : ivan
# ********************** Change Log ***********************/
#		Date	:	2010-08-02 [Ivan]
#		Details	:	Re-organized the UI and add Academic Year Name input for user to customize the year name to be transferred
#
#       Date    :   2010-05-26 [FAI]
#       Details :   Allow user to select which type of data to transfer to student proflie 
/******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	//if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
	//	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageBatchProcess";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], "", 1);

        $linterface->LAYOUT_START();

        if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update")."<br/>";
        
        $WarningMsgBox = $linterface->Get_Warning_Message_Box('', $Lang['eEnrolment']['TransferToSPWarningArr'], $others="");
        
$current_year = getCurrentAcademicYear();
//$current_sem = getCurrentSemester();
$current_semID = getCurrentSemesterID();
$objTerm = new academic_year_term($current_semID);
$current_sem = $objTerm->YearTermNameEN;

//$InstructionBox = $linterface->Get_Warning_Message_Box($title, $content, $others="");

?>
<script type="text/javascript">  
	function onClickAction()
	{
		try{
			if ($('input[name=transferItem[]]:checked').length < 1) throw '<?=$eEnrollment['selectTypeTransferToStudentProflie']?>';
	        document.form1.action ="archive_perform.php";
			document.form1.submit();	
		}
		catch(e){
			alert(e);
		}
	}
</script>
<form name="form1" method="get">
<br/>
<table width="90%" border="0" cellpadding="4" cellspacing="0" align="center">
<tr>
	<td align="right"><?= $SysMsg ?></td>
</tr>
<tr><td><?=$WarningMsgBox?></td></tr>
<tr>
	<td class="tabletext">
		<br>
		<?=$eEnrollment['archive_instruction']?>
		<br>
		<br>
		<li><?="$i_ActivityYear : <font color=red>$current_year</font>"?>
		<br>
		<li><?="$i_ActivitySemester : <font color=red>$current_sem</font>"?>
		<br>
		<li><?="{$eEnrollment['last_modified']} : <font color=red>".$libenroll->retrieveArchive()."</font>"?>
		<br>
		<br>
		<!--
		<?=$eEnrollment['archive_footer1']?>
		<br>
		<br>
		-->
		<?=$eEnrollment['archive_footer2']?>&nbsp;&nbsp;<input type = "checkbox" name="transferItem[]" value = "club" id="transferItem_club" >&nbsp;<label for="transferItem_club"><?=$eEnrollmentMenu['club']?></label> &nbsp; <input type = "checkbox" name="transferItem[]" value = "activity" id="transferItem_activity" >&nbsp;<label for="transferItem_activity"><?=$eEnrollmentMenu['activity']?></label>&nbsp;&nbsp;
	</td>
</tr>
<tr>

		<td height="1" class="dotline" align = "center">
			<img src="<?=$image_path."/".$LAYOUT_SKIN?>/10x10.gif" width="10" height="1">
		</td>
</tr>
<tr>
		<td align = "center">			<br/>
<?= $linterface->GET_ACTION_BTN($eEnrollment['confirm'], "button", "javascript:onClickAction()")?>
		</td>
</tr>

</table>

<br/>

<br/>

</form>
<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>