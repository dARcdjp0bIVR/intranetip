<?php
## Using By : 

###########################################
##	Modification Log:

##  2020-11-05 [Tommy]
##  - added $quota_left for showing quota left

##  2020-04-29 [Tommy]
##  - add $sys_custom['eEnrollment']['setTargetForm'] layout #R171077

##   2020-03-18 [Tommy]
##   - added new export button for export max club student want Case #Q180165

##	2020-01-30 Henry
##	- Added EnrollMin logic

##	2017-09-28 Anna
##	- added reply slip

##	2017-08-18 Anna
##	- added clear_perform_drawing function 
##  - added checkbox to select specific clubs to drawing

##	2015-07-10 Shan
##	- update searchbox style

##	2015-06-16 Omas
##  - hide payment

##	2015-06-16 Evan
##	- Fix the problem in searching special characters and symbols

##	2015-01-30 Omas
##	- Add button export all club application info - case#V74712

##	2014-10-14 Pun
##	- Add round report for SIS customization

##	2014-09-17 Bill
##	- get correct number of limit

##	2012-02-06 Henry Chow
##	- assign $Semester=0 if $Semester is empty

##	2011-09-07	YatWoon
##	- lite version implementation

##	2011-06-27	YatWoon
##	Improved: fine tune the Chinese wording of remark

##	2011-03-04 YatWoon
##	Fixed: if change lang, sql query will check with search box default wordings (Enter Club Name) that cannot search any result

##	2010-12-14 YatWoon
##	club name eng/chinese

##	2010-01-28 Max (201001281139)
##	Add search bar to search club name

##	2010-01-08 Max (201001081039)
##	Send Email for Enrollment Result
###########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
if($plugin['SIS_eEnrollment']){
	include_once($PATH_WRT_ROOT."lang/cust/SIS_eEnrollment_lang.$intranet_session_language.php");
}

intranet_auth();
intranet_opendb();

### Cookies handling
# set cookies
$arrCookies = array();
$arrCookies[] = array("ck_club_status_semester", "Semester");	
$arrCookies[] = array("ck_club_status_keyword", "keyword");
$arrCookies[] = array("ck_club_status_round", "Round"); 

if(isset($clearCoo) && $clearCoo == 1)
	clearCookies($arrCookies);
else 
	updateGetCookies($arrCookies);


if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();

if(!isset($Semester ) || $Semester=="") 
	$Semester = 0;
	
	
$AcademicYearID = IntegerSafe($AcademicYearID);
$Semester = IntegerSafe($Semester);
$Round = IntegerSafe($Round);


$LibUser = new libuser($UserID);
if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol($AcademicYearID);	
	if($plugin['SIS_eEnrollment']){
		if(!isset($Round ) || $Round==""){ 
			$Round = $libenroll->Round = $libenroll->targetRound;
		}
		$libenroll = new libclubsenrol($AcademicYearID, $CategoryID="", $Round);
	}
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_CLUB_PIC()))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		if ($helper_view)
			$CurrentPage = "PageClubAttendanceMgt";
			
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
	}
	
    // Many Query ******* 
    $MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
//	$libenroll->UPDATE_GROUPS_APPROVED();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID'])) 
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        
        # tags
        $tab_type = "club";
        $current_tab = 2;
        include_once("management_tabs.php");
        
		# Search box
		//do not search for the initial textbox text
		if ($keyword == $Lang['eEnrolment']['EnterClubName'] || $keyword == $Lang['eEnrolment']['EnterClubName_EN'] || $keyword == $Lang['eEnrolment']['EnterClubName_B5']) $keyword = "";
		
		//$searchbar = $libenroll->GET_STUDENT_NAME_SERACH_BOX(intranet_htmlspecialchars($keyword), "keyword", $Lang['eEnrolment']['EnterClubName']);
		//$searchbar .= $linterface->GET_SMALL_BTN($button_search, "button", "document.form1.keyword.value = Trim(document.form1.keyword.value); document.form1.action='club_status.php'; document.form1.submit()");
		//$name_conds = " AND ig.Title LIKE '%$keyword%' ";        
        
       $htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', stripslashes($keyword));
        
        
        # navigation
    	$PAGE_NAVIGATION[] = array($eEnrollment['member_selection_and_drawing'], "");

        $linterface->LAYOUT_START();
        
        $tiebreak = $libenroll->tiebreak;
        
		($tiebreak) ? $tiebreak_mode = $i_ClubsEnrollment_AppTime : $tiebreak_mode = $i_ClubsEnrollment_Random;
		
//		$defaultMin = $libenroll->defaultMin;
//		$defaultMax = $libenroll->defaultMax;
		
		# Get Selected Term Number 
		include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');
		$selectedYearTerm = new academic_year_term($Semester);
		$selectedYearTerm = $selectedYearTerm->Get_Term_Number();
		$selectedYearTerm = $selectedYearTerm == -1? 0 : $selectedYearTerm;
		# Set to Full Year if Year Base
		$selectedYearTerm = (trim($libenroll->quotaSettingsType) == 'YearBase')? 0 : $selectedYearTerm;
		
		# Get Quota Information 
		$quotaSettingsAry = $libenroll->Get_Application_Quota_Settings($enrolConfigAry['Club']);
		$quotaSettingsAssoAry = BuildMultiKeyAssoc($quotaSettingsAry, array('TermNumber', 'CategoryID','SettingName'));
		
		# Get Enrol & Setting Limit
		if($libenroll->UseCategorySetting)
		{
			$defaultMin = $quotaSettingsAssoAry[$selectedYearTerm][$CategoryID]['ApplyMin']['SettingValue'];
			$defaultMax = $quotaSettingsAssoAry[$selectedYearTerm][$CategoryID]['ApplyMax']['SettingValue'];
			$EnrollMax = $quotaSettingsAssoAry[$selectedYearTerm][$CategoryID]['EnrollMax']['SettingValue'];
			$EnrollMin = $quotaSettingsAssoAry[$selectedYearTerm][$CategoryID]['EnrollMin']['SettingValue'];
		}
		else
		{
			$defaultMin = $quotaSettingsAssoAry[$selectedYearTerm][0]['ApplyMin']['SettingValue'];
			$defaultMax = $quotaSettingsAssoAry[$selectedYearTerm][0]['ApplyMax']['SettingValue'];
			$EnrollMax = $quotaSettingsAssoAry[$selectedYearTerm][0]['EnrollMax']['SettingValue'];
			$EnrollMin = $quotaSettingsAssoAry[$selectedYearTerm][0]['EnrollMin']['SettingValue'];
		}
		
// 		if ($defaultMax==0) $defaultMax = $eEnrollment['no_limit'];
		
		if($plugin['SIS_eEnrollment']){
			$defaultMin = $libenroll->defaultMin;
			$defaultMax = $libenroll->defaultMax;
			$EnrollMax = $libenroll->EnrollMax;
		}
		$defaultMax = $defaultMax==0 ? $eEnrollment['no_limit'] : $defaultMax . $eEnrollment['student_enrolment_rule_activity2'] ;
//		$EnrollMax = $libenroll->EnrollMax;
// 		if ($EnrollMax==0) $EnrollMax = $eEnrollment['no_limit'];
		$EnrollMax = $EnrollMax==0 ? $eEnrollment['no_limit'] : $EnrollMax . $eEnrollment['student_enrolment_rule_activity2'] ;
		$EnrollMin = $EnrollMin==0 ? '0' : $EnrollMin;
		
		$AppStart = $libenroll->AppStart;
		$AppEnd = $libenroll->AppEnd;
		
		$AppStartDisplay = $libenroll->AppStart." ".$libenroll->AppStartHour.":".$libenroll->AppStartMin;
		$AppEndDisplay = $libenroll->AppEnd." ".$libenroll->AppEndHour.":".$libenroll->AppEndMin;
		
		$GpStart = $libenroll->GpStart;
		$GpEnd = $libenroll->GpEnd;
		
		$NextRoundInstruction = $eEnrollment['next_round_instruction'];
		$DrawingInstruction = str_replace("<!--DrawingMethod-->", $tiebreak_mode,$eEnrollment['drawing_instruction']);
		$ClearDrawingInstruction = $Lang['eEnrolment']['ClearDrawingInstruction'];
		if(!$libenroll->UseCategorySetting)
		{
			$StudentEnrolmentRule = str_replace("<!--MinNumApp-->", $defaultMin, ($sys_custom['eEnrolment']['EnableEnrollMin']?$eEnrollment['student_enrolment_rule_with_min']:$eEnrollment['student_enrolment_rule']));
			$StudentEnrolmentRule = str_replace("<!--MaxNumApp-->", $defaultMax, $StudentEnrolmentRule);
			$StudentEnrolmentRule = str_replace("<!--MaxEnrol-->", $EnrollMax, $StudentEnrolmentRule);
			if($sys_custom['eEnrolment']['EnableEnrollMin']){
				$StudentEnrolmentRule = str_replace("<!--MinEnrol-->", $EnrollMin, $StudentEnrolmentRule);
			}
			
			if($sys_custom['eEnrollment']['setTargetForm']){
			    $defaultMax = str_replace($eEnrollment['student_enrolment_rule_activity2'],"",$defaultMax);
			    $EnrollMax = str_replace($eEnrollment['student_enrolment_rule_activity2'],"",$EnrollMax);
			    if($defaultMin != $defaultMax){
			        if($defaultMin == 0 && $defaultMax == $eEnrollment['no_limit'])
			            $StudentChooseEnrolRole = $eEnrollment['no_limit'];
			        else
			            $StudentChooseEnrolRole = $defaultMin." - ".$defaultMax;
			    }else{
			        $StudentChooseEnrolRole = $defaultMax;
			    }
			    
			    if($EnrollMin != $EnrollMax){
			        if($EnrollMin == 0 && $EnrollMax == $eEnrollment['no_limit'])
			            $StudentJoinEnrolRole = $EnrollMax;
			        else    
			             $StudentJoinEnrolRole = $EnrollMin." - ".$EnrollMax;
			    }else{
			        $StudentJoinEnrolRole = $EnrollMax;
			    }
// 			    if($sys_custom['eEnrolment']['EnableEnrollMin']){
// 			        $StudentJoinEnrolRole = $EnrollMin;
// 			    }
			}
		}
		$EnrolmentPeriodDisplay = $eEnrollment['enrolment_period']." ".$AppStartDisplay." ".$eEnrollment['to']." ".$AppEndDisplay;
		if($plugin['SIS_eEnrollment']){
			$EnrolmentPeriodDisplay = '';
		}
		## Semester filter - default = Whole Year
		/*
		if (!isset($Semester))
		{	
			$Semester = getCurrentSemester();
		}
		*/
		
		# Academic Year Selection       
		$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.Semester.selectedIndex=0;document.form1.action=\'club_status.php\';document.form1.submit();"', 1, 0, $AcademicYearID);
		
		if (!isset($Semester))
			$Semester = 0;
		$SemesterFilter = $libenroll->Get_Term_Selection('Semester', $AcademicYearID, $Semester, $OnChange="document.form1.action='club_status.php'; document.form1.submit()", $NoFirst=1, $NoPastTerm=0, $withWholeYear=1);
		
		if($plugin['SIS_eEnrollment']){
			$SemesterFilter = $libenroll->Get_Round_Selection('Round', $Round,"document.form1.action='club_status.php'; document.form1.submit()", $showAllRound=false);
		}
		
		//$GroupInfoArr = $libenroll->getGroupInfoList(0, $Semester, '', 1, $name_conds);
		//get number of waiting,approved,rejected applicants
		$waitingNum = array();
		$approvedNum = array();
		$rejectedNum = array();
		
// 		$GroupInfoArr = $libenroll->Get_Club_Student_Enrollment_Info('', $AcademicYearID, $ShowUserRegOnly=1, array($Semester), /*$keyword*/intranet_htmlspecialchars(stripslashes($keyword)));
		$GroupInfoArr = $libenroll->Get_Club_Student_Enrollment_Info('', $AcademicYearID, $ShowUserRegOnly=1, array($Semester), /*$keyword*/intranet_htmlspecialchars(stripslashes($keyword)), 0, 1, 1, 1, '', '', '', 0);
		if($plugin['SIS_eEnrollment']){
			$GroupInfoArr = $libenroll->Get_Club_Student_Enrollment_Info('', $AcademicYearID, $ShowUserRegOnly=1, $Round, $keyword);
		}
		$numOfClub = count($GroupInfoArr);
	
		foreach($GroupInfoArr as $thisEnrolGroupID => $thisClubInfo)
		{
			$thisWaitingNum = count((array)$GroupInfoArr[$thisEnrolGroupID]['StatusStudentArr'][0]);
			$thisRejectedNum = count((array)$GroupInfoArr[$thisEnrolGroupID]['StatusStudentArr'][1]);
			$thisApprovedNum = count((array)$GroupInfoArr[$thisEnrolGroupID]['StatusStudentArr'][2]);
			
			$waitingNum[$thisEnrolGroupID] = ($thisWaitingNum=='')? 0 : $thisWaitingNum;
			$rejectedNum[$thisEnrolGroupID] = ($thisRejectedNum=='')? 0 : $thisRejectedNum;
			$approvedNum[$thisEnrolGroupID] = ($thisApprovedNum=='')? 0 : $thisApprovedNum;
			
			/*
			$sql = "SELECT COUNT(*) FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '".$thisEnrolGroupID."' AND RecordStatus = 0 ";
			$result = $libenroll->returnVector($sql);
			$waitingNum[$thisEnrolGroupID] = $result[0];
			$sql = "SELECT COUNT(*) FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '".$thisEnrolGroupID."' AND RecordStatus = 2 ";
			$result = $libenroll->returnVector($sql);
			$approvedNum[$thisEnrolGroupID] = $result[0];
			$sql = "SELECT COUNT(*) FROM INTRANET_ENROL_GROUPSTUDENT WHERE EnrolGroupID = '".$thisEnrolGroupID."' AND RecordStatus = 1 ";
			$result = $libenroll->returnVector($sql);
			$rejectedNum[$thisEnrolGroupID] = $result[0];
			*/
		}
		
		
		### Print and Email Icons
		$toolbar = '';
		if (!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])  || $libenroll->IS_CLUB_PIC())
		{
			if($plugin['SIS_eEnrollment']){
				$toolbar .= $linterface->GET_LNK_PRINT("javascript:newWindow('print.php?type=club&targetRound=$Round',4);", $button_print, '', '', '', 0);
			}else{
				$toolbar .= $linterface->GET_LNK_PRINT("javascript:newWindow('print.php?type=club&targetSemester=$Semester',4);", $button_print, '', '', '', 0);
			}
			$toolbar .= toolBarSpacer();
		}
		
		# temp disable for deployment (Ivan 20091110)
		# enable for development (Max 20100111)
		if($plugin['eEnrollmentLite'])
		{
			$toolbar .= "<div class='Conntent_tool'><i class='email'>".$Lang['eEnrolment']['SendEmailForEnrollmentResult']."</i></div>";
		}
		else
		{
			if ($libenroll->AllowToEditPreviousYearData && !$libenroll->IS_NORMAL_USER($_SESSION['UserID']))
				$toolbar .= $linterface->GET_LNK_EMAIL("club_email.php", $Lang['eEnrolment']['SendEmailForEnrollmentResult'], '', '', '', 0);
		}
		

		$buttonHref = "javascript:void(0);";
		$option = array(
		  array("export.php?type=9&field=0&order=1&filter=0&keyword=&priority=1&AcademicYearID=".$AcademicYearID, $Lang['eEnrolment']['ExportClubApplicantList'], "1"),
		  array("export.php?type=13&field=0&order=1&filter=0&keyword=&priority=1&AcademicYearID=".$AcademicYearID, $Lang['eEnrolment']['ExportStudentMaxWanted'], "1"),
		);
		    
		$toolbar .= '<div class="Conntent_tool">';
		$toolbar .= $linterface->Get_Content_Tool_v30("export", $buttonHref, $Lang['Btn']['Export'], $option);
		$toolbar .= '</div>';

// 		$toolbar .= $linterface->GET_LNK_EXPORT("export.php?type=9&field=0&order=1&filter=0&keyword=&priority=1&AcademicYearID=".$AcademicYearID."", $Lang['Btn']['Export'], '', '', '', 0);
		
// 		$toolbar .= $linterface->GET_ACTION_BTN($eEnrollment['button']['perform_drawing'], "button", "javascript:goDrawing(1);");
//		$toolbarright .=$linterface->GET_ACTION_BTN($Lang['eEnrolment']['button']['perform_drawing'], "button","javascript:goDrawing(1);");
		echo $linterface->Include_Thickbox_JS_CSS();
		if($sys_custom['eEnrolment']['UnitRolePoint']){
			$toolbar .='<div class="Conntent_tool">';
			$toolbar .= $linterface->GET_LNK_EMAIL('javascript:loadeNoticeThickBox();',$Lang['eEnrolment']['SysRport']['SendeNotice'] , '', 'id="fixedSizeThickboxLink"', ' tablelink email', 1);
		//	$toolbar .= $linterface->GET_LNK_EMAIL("javascript:newWindow('send_notice_check_member_status.php?academicYearId=$AcademicYearID&yearTermId=$Semester');",$Lang['eEnrolment']['SysRport']['SendeNotice'] , '', 'id="fixedSizeThickboxLink"', 'email tablelink', 0);
			$toolbar .='</div>';
		}
		if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("", $i_Payment_con_PaymentProcessed)."<br/>";
		if ($msg == 15) $SysMsg = $linterface->GET_SYS_MSG("", $i_con_msg_enrollment_next)."<br/>";
		//if ($msg == 16) $SysMsg = $linterface->GET_SYS_MSG("", $i_con_msg_enrollment_confirm)."<br/>";
		if ($msg == 16) $SysMsg = $linterface->GET_SYS_MSG("", $i_con_msg_notification_sent)."<br/>";

		
		if($sys_custom['eEnrolment']['ReplySlip']){
			echo $linterface->Include_Thickbox_JS_CSS();
		}
		
		if($sys_custom['eEnrollment']['setTargetForm']){
		    
		    $x = "<table id=\"EnrolmentPeriodSelection\" class=\"form_table_v30\">";
		    
		    $x .= "<tr>
                       <td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">".$eEnrollment['enrolment_period']."</td>
                       <td class=\"tabletext\">".$AppStartDisplay." ".$eEnrollment['to']." ".$AppEndDisplay
                       ."</tr>";
		    
           $classlvl = ($libenroll->settingTargetForm == '') ? ' -- ' : str_replace(",", ", ", $libenroll->settingTargetForm);
		    
           $x .= "<tr>
                       <td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\"> ".$Lang['eEnrolment']['Process']['TargetForm']." </td>
                       <td class=\"tabletext\">".$classlvl
                       ."</tr>";
           
           $x .= "<tr>
                       <td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\"> ".$Lang['eEnrolment']['Process']['StudentChoose']." </td>
                       <td class=\"tabletext\">".$StudentChooseEnrolRole
                       ."</tr>";
           
           $x .= "<tr>
                       <td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\"> ".$Lang['eEnrolment']['Process']['StudentJoin']." </td>
                       <td class=\"tabletext\">".$StudentJoinEnrolRole
                       ."</tr>";
           
           if ((date("Y-m-d H:i") > $AppEndDisplay)&& (!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) ) {
               $x .= "<tr>
                           <td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">".$DrawingInstruction."</td>";
               $SelectClub = 0;
               if($sys_custom['eEnrolment']['SelectClubDrawing'] == true){
                   $SelectClub = 1;
               }
    
               $x .= "<td class=\"tabletext\">".$linterface->GET_ACTION_BTN($Lang['eEnrolment']['button']['perform_drawing'], "button", "javascript:goDrawing($SelectClub);")
                      ."&nbsp;</tr>";
               
               $x .= "<tr>
                           <td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">".$ClearDrawingInstruction."</td>
                           <td class=\"tabletext\">".$linterface->GET_ACTION_BTN($Lang['eEnrolment']['button']['clear_perform_drawing'], "button", "ClearDrawing();")
                           ."&nbsp;</tr>";
               
               $x .= "<tr>
                           <td valign=\"top\" nowrap=\"nowrap\" class=\"field_title\">".$NextRoundInstruction."</td>
                           <td class=\"tabletext\">".$linterface->GET_ACTION_BTN($eEnrollment['button']['next_round'], "button", "js_Go_Confirm_Delete_Unhandled_Data();")
                           ."&nbsp;</tr>";
    						
    		   $x .= "</table>";
           }
							
		}
		
?>
<script type="text/javascript">

$(document).ready( function() {
	$('a#fixedSizeThickboxLink').click( function() {
		load_dyn_size_thickbox_ip('Send notice to parent', 'loadeNoticeThickBox();', inlineID='', defaultHeight=400, defaultWidth=600);
	});
});
function PaymentConfirm(obj, jParID) {
	AlertPost(obj, 'payment.php?EnrolGroupID='+jParID, '<?= $eEnrollment['js_prepayment_alert']?>');
}

function js_Go_Confirm_Delete_Unhandled_Data()
{
	jsEnrolGroupIDList = $('input#EnrolGroupIDList').val();
	
	if (jsEnrolGroupIDList == '')
	{
		alert('<?=$Lang['eEnrolment']['ClearUnhandledData']['jsWarningArr']['NoData']?>');
		return false;
	}
	
	var jsFormObj = document.getElementById('form1');
	jsFormObj.action = 'clear2next_confirm.php';
	jsFormObj.submit();
}

function goDrawing(all) {
	var academicYearId = $('select#AcademicYearID').val();

	if(all==0){
		var EnrolGroupID = "";
	}else{
		var EnrolGroupID = "";
		if($( "input[name='EnrolGroupID[]']:checked" ).length == 0){
			alert('<?=$Lang['eEnrolment']['PerformDrawing']['jsWarningArr']['NoSelected']?>');
			return false;
		}
		EnrolGroupID = $('input[name=' + 'EnrolGroupID[]' + ']:checked').map(function(){ return this.value;}).get().join()
	}
	newWindow('lottery.php?AcademicYear=' + academicYearId + '&EnrolGroupID=' + EnrolGroupID, 9)
}
function ClearDrawing(){
	 if (confirm("<?=$Lang['eEnrolment']['ClearDrawing']['jsWarningArr']?>") == true) {
		 document.getElementById('form1').action = 'rollback_lottery.php';
		 document.getElementById('form1').submit();
	    } else { 
		    return false;
	    }
}
<?php if ($sys_custom['eEnrolment']['UnitRolePoint']){?>
function loadeNoticeThickBox(){
	$('div#TB_ajaxContent').load(
			"send_notice_check_member_status.php", 
			{ 
				academicYearId: <?=$AcademicYearID?>,
				yearTermId: <?=$Semester?>
			},
			function(ReturnData) {
				adjust_dyn_size_thickbox_content_height_ip('thickboxContainerDiv', 'thickboxContentDiv', 'editBottomDiv');
			}
		);

	
}
<?php } ?>



<?php if($sys_custom['eEnrolment']['ReplySlip']){?>
		function onLoadThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
		
			load_dyn_size_thickbox_ip('<?=$eEnrollment['replySlip']['ReplySlipDetail']?>', 'onLoadReplySlipThickBox('+ReplySlipGroupMappingID+','+EnrolGroupID+','+ReplySlipID+');');
		}
		function onLoadReplySlipThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
			$.ajax({
				url: "ReplySlip_ajax_ReplySlipReply_view2.php",
				type: "POST",
				data: {
					"ReplySlipGroupMappingID": ReplySlipGroupMappingID, 
					"EnrolGroupID": EnrolGroupID,
					"ReplySlipID":ReplySlipID,
					"Status" : $("#filter").val()
					},
				success: function(ReturnData){
					$('div#TB_ajaxContent').html(ReturnData);
				}
			});
		}
		function goExport(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
		    	document.form1.action = "ReplySlip_ajax_ReplySlipReply_view2.php?ReplySlipGroupMappingID="+ReplySlipGroupMappingID+"&EnrolGroupID="+EnrolGroupID+"&ReplySlipID="+ReplySlipID+"&isExport="+1+"&isActivity=true";
		    	document.form1.target = "_blank";
		    	document.form1.method = "post";
		    	document.form1.submit();
		}
<?php }?>
</script>
<br />
<form id="form1" name="form1" action="" method="POST">
	<!-- navigation and search bar -->
	<table width="100%" border="0" cellspacing="4" cellpadding="4">
		<tr>
			<td>
				<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
			</td>
			<td align="right">
				<?= $SysMsg ?>
			</td>
		</tr>
	</table>
	<table width="96%" border="0" cellspacing="0" cellpadding="5">
		<tr>
			<td valign="top"><?= $yearFilter."&nbsp;".$SemesterFilter ?></td>
			<td valign="top" align="right"><?= $htmlAry['searchBox'] ?></td>
		</tr>
		<tr>
			<td colspan="2">
			<? if($libenroll->AllowToEditPreviousYearData) {?>
			<? if($sys_custom['eEnrollment']['setTargetForm']){
			     echo $x;
			   }else{
			?>
				<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
					<tr>
						<td align="center" class="tabletext">
							<?=$StudentEnrolmentRule?>
						</td>
					</tr>
					<tr>
						<td align="center" class="tabletext">
							<?=$EnrolmentPeriodDisplay?>
						</td>
					</tr>
					<? if ((date("Y-m-d H:i") > $AppEndDisplay)&& (!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) ) { ?>
						<tr><td align="center" class="tabletext">&nbsp;</td></tr>
						<tr>
							<td align="center" class="tabletext">
								<?=$NextRoundInstruction?>
							</td>
						</tr>
						<tr>
							<td align="center" class="tabletext">
								<?= $linterface->GET_ACTION_BTN($eEnrollment['button']['next_round'], "button", "js_Go_Confirm_Delete_Unhandled_Data();")?>&nbsp;
							</td>
						</tr>
						
						<tr><td align="center" class="tabletext">&nbsp;</td></tr>
						<tr>
							<td align="center" class="tabletext">
								<?=$DrawingInstruction?>
							</td>
						</tr>
						<tr>
							<td align="center" class="tabletext">
							<?php 
							$SelectClub = 0;
							if($sys_custom['eEnrolment']['SelectClubDrawing'] == true){ 
									$SelectClub = 1;
							}?>
							
								<?= $linterface->GET_ACTION_BTN($Lang['eEnrolment']['button']['perform_drawing'], "button", "javascript:goDrawing($SelectClub);")?>&nbsp;
							</td>
						</tr>
						
						
						<tr><td align="center" class="tabletext">&nbsp;</td></tr>
						<tr>
							<td align="center" class="tabletext">
								<?=$ClearDrawingInstruction?>
							</td>
						</tr>
						<tr>
							<td align="center" class="tabletext">
								<?= $linterface->GET_ACTION_BTN($Lang['eEnrolment']['button']['clear_perform_drawing'], "button", "ClearDrawing();")?>&nbsp;
							</td>
						</tr>
					<? } ?>
				</table>
				<?}?>
				<br />
				
				<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
					<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td height="3"></td></tr>
				</table>

			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
					<tr>
						<td colspan="7" align="left">
							<?= $toolbar ?>
						</td>
						<td align="right">
							<div class="table_row_tool row_content_tool" >
							<?= $toolbarright ?>
							</div>
						</td>
					</tr>
					<tr class="tabletopnolink tableorangetop">
						<td rowspan="2" nowrap><?=$eEnrollment['club_name']?></td>
						<?php if($plugin['SIS_eEnrollment']){ ?>
							<td rowspan="2" align="center"><?=$Lang['SIS_eEnrollment']['editCCA']['RoundType']?></td>
						<?php }else{ ?>
							<td rowspan="2" align="center"><?=$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term']?></td>
						<?php }?>
						<td rowspan="2" align="center"><?=$eEnrollment['club_quota']?></td>
						<td rowspan="2" align="center"><?=$eEnrollment['MinimumQuota']?></td>
						<td colspan="3" align="center"><?=$eEnrollment['number_of_applicants']?></td>
						<?php if($sys_custom['eEnrolment']['ReplySlip']){?>
							<td rowspan="2" class="tableTitle" align="center"><span class="tabletopnolink"><?=$Lang['eEnrolment']['ReplySlip']?></span></td>
						<?php }?>
						<td rowspan="2" align="center"><?=$eEnrollment['quota_left']?></td>
						<?php if($sys_custom['eEnrolment']['SelectClubDrawing'] == true && (date("Y-m-d H:i") > $AppEndDisplay)&& (!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) ){ ?>
							<td rowspan="2" align="center"><input type="checkbox"  name="checkmaster" onClick="(this.checked)?setChecked(1,document.form1,'EnrolGroupID[]'):setChecked(0,document.form1,'EnrolGroupID[]')"></td>
						<?php }?>
						<!--td colspan="2" align="center"><?=$eEnrollment['payment']?></td-->
					</tr>
					<tr class="tabletopnolink tableorangetop">						
						<td align="center"><?= $eEnrollment['unhandled']?></td>
						<td align="center"><?= $i_ClubsEnrollment_StatusApproved?></td>
						<td align="center"><?= $i_ClubsEnrollment_StatusRejected?></td>
						
						<!--td align="center"><//?=$eEnrollment['pay']['amount']?></td>
						<td align="center"><//?=$eEnrollment['process']?></td-->
					</tr>
					<?
					$DisplayedEnrolGroupIDArr = array();
					foreach ($GroupInfoArr as $thisEnrolGroupID => $thisClubInfoArr)
					{
						//($id,$name,$quota,$approved)
						$quota = $thisClubInfoArr['Quota'];
						$quota_left = $thisClubInfoArr['QuotaLeftDisplay'];

						//$quota_left = $GroupInfoArr[$i][2] - $GroupInfoArr[$i][3];  
	//					$quota_left = $libenroll->GET_GROUP_QUOTA_LEFT($thisEnrolGroupID);
						if ($quota == 0) {
							$quota = $i_ClubsEnrollment_NoLimit;
							$quota_left = $i_ClubsEnrollment_NoLimit;
						}
						// show quota = 0 even though it is negative
						if ($quota_left < 0)
						{
							$quota_left = 0;
						}
			//30
						$GroupName = $intranet_session_language=="en"?$thisClubInfoArr['GroupTitle']:$thisClubInfoArr['TitleChinese'];
						//if  ($GroupName == "") $GroupName = $eEnrollment['no_name'];
						
//						if ($GroupInfoArr[$i]['Semester']=='')
//						{
//							$thisSemester = $i_ClubsEnrollment_WholeYear;
//						}
//						else
//						{
//							$objYearTerm = new academic_year_term($thisClubInfoArr['YearTermID']);
//							$thisSemester = $objYearTerm->Get_Year_Term_Name();
//						}
						$thisSemester = $GroupInfoArr[$thisEnrolGroupID]['YearTermName'];
						if($plugin['SIS_eEnrollment']){
							$thisRound = $GroupInfoArr[$thisEnrolGroupID]['RoundNumber'];
						}
						
						$thisMinQuota = ($thisClubInfoArr['MinQuota']==0)? $i_ClubsEnrollment_NoLimit : $thisClubInfoArr['MinQuota'];
						
						// Amount
						if ($thisClubInfoArr['isAmountTBC'] == 1)
							$thisAmountDisplay = $eEnrollment['ToBeConfirmed'];
						else
							$thisAmountDisplay = "$".$thisClubInfoArr['PaymentAmount'];
						
						if (
							(($libenroll->IS_NORMAL_USER($UserID))&&($libenroll->IS_ENROL_PIC($UserID, $thisEnrolGroupID, "Club"))||
							(!$libenroll->IS_NORMAL_USER($_SESSION['UserID']))||($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))||($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
							&& ($GroupName != "")
							) {
					?>
							<tr class="tabletext tableorangerow<?= (($displayed_record % 2)+1) ?>">
								<!-- Group Name -->
								<td><?= $GroupName ?></td>
								
								<!-- Semester -->
								<?php if($plugin['SIS_eEnrollment']){ ?>
									<td align="center"><?= $thisRound ?></td>
								<?php }else{ ?>
									<td align="center"><?= $thisSemester ?></td>
								<?php } ?>
								
								<!-- Quota -->
								<td align="center"><?= $quota ?></td>
								
								<!-- Min Quota -->
								<td align="center"><?= $thisMinQuota ?></td>
								
								<!-- Waiting -->
								<td align="center">
									<a class="tablelink" href="group_member.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$thisEnrolGroupID?>&filter=0">
										<?= $waitingNum[$thisEnrolGroupID] ?>
									</a>
								</td>
								
								<!-- Approved -->
								<td align="center">
									<a class="tablelink" href="group_member.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$thisEnrolGroupID?>&filter=2">
										<?= $approvedNum[$thisEnrolGroupID] ?>
									</a>
								</td>
								
								<!-- Rejected -->
								<td align="center">
									<a class="tablelink" href="group_member.php?AcademicYearID=<?=$AcademicYearID?>&EnrolGroupID=<?=$thisEnrolGroupID?>&filter=1">
										<?= $rejectedNum[$thisEnrolGroupID] ?>
									</a>
								</td>
								
								
								<?php 
		
								if($sys_custom['eEnrolment']['ReplySlip']){
									$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$thisEnrolGroupID,1);
								
									$ReplySlipRelation = $ReplySlipRelation[0];
									$ReplySlipGroupMappingID = $ReplySlipRelation['ReplySlipGroupMappingID'];
									$ReplySlipID = $ReplySlipRelation['ReplySlipID'];
									
									$link = '';
									$ReplySlipReply = '';
									
									if($ReplySlipGroupMappingID){
										$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
										$link = '<div class="Conntent_tool" ><a align="center" href="javascript:void(0);" onclick="onLoadThickBox('.$ReplySlipGroupMappingID.','.$thisEnrolGroupID.','.$ReplySlipID.')" style="background:url(\'/images/2009a/eOffice/icon_resultview.gif\') no-repeat;">'.$eEnrollment['replySlip']['replySlip'].'</a></div>';
									}
								?>
						
									<!- ReplySlip -->
									<td valign="top" align="center" width="90">
										<span><?= $link?></span>
									</td>	
								<?php 
									}
								?>
								
								<!-- Quota Left -->
								<td align="center"><?= $quota_left ?></td>	
								
								<?php  if($sys_custom['eEnrolment']['SelectClubDrawing'] == true && (date("Y-m-d H:i") > $AppEndDisplay)&& (!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) ){ ?>
								<!-- Checkbox to perform drawing -->
								
								<td align="center" ><input type="checkbox" name="EnrolGroupID[]" value="<?=$thisEnrolGroupID?>" onClick="unset_checkall(this,document.getElementById('form1'))"></input></td>
								
								<?php }?>
								<!-- Amount -->
								<!--td align="center"><//?= $thisAmountDisplay ?></td-->	
								
								<!-- Process -->						
								<!--td align="center">
								<//? if ($libenroll->ALLOW_PAYMENT($thisEnrolGroupID)) {
										if ($plugin['payment'] && $thisClubInfoArr['PaymentAmount']!=0 && $thisClubInfoArr['isAmountTBC']!=1) { ?>	
											<//? if(!$plugin['eEnrollmentLite']) { ?>	
											<a href="javascript:PaymentConfirm(document.form1, <//?= $thisEnrolGroupID?>)" ><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_payment.gif" border="0" alt="<?= $eEnrollment['payment']?>"></a>
											<//? } else {?>
											<img src="<//?= "{$image_path}/{$LAYOUT_SKIN}" ?>/icon_payment_dim.gif" border="0" alt="<?= $eEnrollment['payment']?>">
											<//? } ?>
									 <//? } else { 
										 	print "---"; 
										}
								   } 
								   else 
								   { 
									   print $eEnrollment['processed']; 
								   } 
								?>
								</td>
							</tr-->
					<? 
							$displayed_record++;
							$DisplayedEnrolGroupIDArr[] = $thisEnrolGroupID;
						}
					}
					if ($displayed_record == 0) {
						print "<tr><td class=\"tabletext\" align=\"center\" colspan=\"20\">".$eEnrollment['no_record']."</td></tr>";
					}
					
					$DisplayedEnrolGroupIDList = implode(',', $DisplayedEnrolGroupIDArr);
					?>
				</table>

			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
					<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
					<tr><td align="center" colspan="6"></td></tr>
				</table>
				<br/>
			</td>
		</tr>
	</table>
	<br />
	
	<input type="hidden" id="EnrolGroupIDList" name="EnrolGroupIDList" value="<?=$DisplayedEnrolGroupIDList?>" />
</form>

    <?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>