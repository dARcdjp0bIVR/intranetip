<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$DataArr['CommentStudent'] = $CommentStudent;
$DataArr['Performance'] = $performance;
$DataArr['StudentID'] = $StudentID;
$DataArr['EnrolEventID'] = $EnrolEventID;

$libenroll->ADD_EVENT_STUDENT_COMMENT($DataArr);

intranet_closedb();
header("Location: event_student_comment.php?StudentID=$StudentID&EnrolEventID=$EnrolEventID&msg=2");
?>