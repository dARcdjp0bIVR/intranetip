<?php

## Using By :

###########################################
## Modification Log:
## 2017-10-13 (Simon) [B115479]
##  handle the class techer and pic email
##
## 2015-11-11 (Omas)
##  improved avoid app send to 0 user, modified log format , add $groupTitle to app log title
##
## 2015-09-24 (Omas)
##	add column for seperating send to parent and student logic add attendance rate to content		
##
## 2013-11-04 (Henry)
##			File created
##
###########################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT.'includes/libwebmail.php');
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');

intranet_auth();
intranet_opendb();

// debug_pr($_POST['hid_query']);

$libenroll = new libclubsenrol($AcademicYearID);
//$libenroll_ui = new libclubsenrol_ui();
$libuser = new libuser();
$lwebmail = new libwebmail();
$libeClassApp = new libeClassApp();
$li = new libdb();
//$academicYearId = $_POST['academicYearId'];
//$yearTermId = $_POST['yearTermId'];
$hid_query = $_POST['hid_query'];
//$emailTitle = $_POST['emailTitle'];
//$emailContent = $_POST['emailContent'];
$hid_studentList = $_POST['hid_studentList'];
$hid_fromLink = $_POST['hid_fromLink'];
$hid_activityDateStart = $_POST['hid_activityDateStart'];
$hid_activityGroupID = IntegerSafe($_POST['hid_activityGroupID']);
$groupInfo = $libenroll->getGroupInfo($hid_activityGroupID);
$groupTitle = Get_Lang_Selection($groupInfo[0]['TitleChinese'], $groupInfo[0]['Title']);
$studentArray = explode(',',$hid_studentList);
$studentArray = array_unique((array)$_POST['userIdAry']);
$parentArray = array_unique((array)$_POST['parentIdAry']);

$hid_activityEventID = IntegerSafe($_POST['hid_activityEventID']);


$allStudentArr = array_unique(array_merge((array)$parentArray,(array)$studentArray));

// get attendance data
if(!empty($hid_activityGroupID)){
	$enrolGroupInfo = $libenroll->Get_Club_Info_By_GroupID($hid_activityGroupID);
	$enrolGroupID = $enrolGroupInfo[0]['EnrolGroupID'];
	$studentAttendance = $libenroll->Get_Student_Club_Attendance_Info($allStudentArr,(array)$enrolGroupID);
}
else{
	// activity data ready here
	$studentAttendance = $libenroll->Get_Student_Activity_Attendance_Info($allStudentArr,(array)$hid_activityEventID);
	$eventInfoArr = $libenroll->GET_EVENTINFO($hid_activityEventID);
	$enrolGroupID = $hid_activityEventID;
	$groupTitle = $eventInfoArr['EventTitle'];
}

if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false/* || ($action!="view" && $canEdit==false)*/)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
if($hid_fromLink == 0){
	$attendanceType = $Lang['eEnrolment']['Club'];
}
else{
	$attendanceType = $Lang['eEnrolment']['Activity'];
}

// loop student column
foreach($studentArray as $aStudent){
	$teacherIdAry = array();
	$aStudentArr = array();
	$aStudentArr[] = $aStudent;
	
	// handle the email to class teacher
	$currentAYId = Get_Current_Academic_Year_ID();
	if(isset($_POST['classTeacher']) &&
			$_POST['classTeacher'] == 'yes')
	{
		$sql = "select yc.ClassTitleEN
					from
					YEAR_CLASS_USER as ycu
				inner join
					YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
				where
					yc.AcademicYearID = '$currentAYId'
				and
					ycu.UserID = '$aStudent'
				";
		$className = $li->returnResultSet($sql);
		$_className = $className[0]['ClassTitleEN'];
		
		// get the teacher email by StudentID / UserID
		$sql = "select yct.UserID
				from
					YEAR_CLASS_USER as ycu
				inner join
					YEAR_CLASS as yc on ycu.YearClassID = yc.YearClassID
				inner join
					YEAR_CLASS_TEACHER as yct on yc.YearClassID = yct.YearClassID
				where
					yc.AcademicYearID = '$currentAYId'
				and
					ycu.UserID = '$aStudent'
				and 
					yc.ClassTitleEN = '$_className'
				";
		
		$teacherIdAry = $li->returnResultSet($sql);
		
		if(!empty($teacherIdAry)){
			foreach($teacherIdAry as $teacherId){
				$aStudentArr[] = $teacherId['UserID'];
				$aStudentArr = array_values(array_unique($aStudentArr));
			}
		}
	}
	
	$emailTitle = str_replace("<!--clubName-->",$attendanceType,$Lang['eEnrolment']['AttenanceMailTitleFixed']).' ('.$groupTitle.')';
	$emailContent = str_replace("<!--studentName-->",$libuser->getNameWithClassNumber($aStudent),$Lang['eEnrolment']['AttenanceMailContentFixed']);
	$emailContent = str_replace("<!--clubName-->",$groupTitle,$emailContent);
	$emailContent = str_replace("<!--datetime-->",$hid_activityDateStart,$emailContent);
	$emailContent = str_replace("<!--attendanceRate-->",$studentAttendance[$aStudent][$enrolGroupID]['AttendancePercentageRounded'].'%',$emailContent);
//	$success[] = $lwebmail->sendModuleMail($aStudentArr,$emailTitle,$emailContent,1,'','ParentOnly');
	$success[] = $lwebmail->sendModuleMail($aStudentArr,$emailTitle,$emailContent,1,'','User');
}

// handle the email to PIC persons
if(isset($_POST['pic']) &&
		$_POST['pic'] == 'yes')
{
	$picIdAry = array();
	$_picIdAry = array();
	$picIdAry = $libenroll->GET_GROUPSTAFF($enrolGroupID, 'PIC');
	foreach($picIdAry as $picId){
		$_picIdAry[] = $picId['UserID'];
	}
	
	if(!empty($_picIdAry)){
		$emailTitle = str_replace("<!--clubName-->",$attendanceType,$Lang['eEnrolment']['AttenanceMailTitleFixed']).' ('.$groupTitle.')';
		$emailContent = str_replace("<!--studentName-->",$libuser->getNameWithClassNumber($aStudent),$Lang['eEnrolment']['AttenanceMailContentFixed']);
		$emailContent = str_replace("<!--clubName-->",$groupTitle,$emailContent);
		$emailContent = str_replace("<!--datetime-->",$hid_activityDateStart,$emailContent);
		$emailContent = str_replace("<!--attendanceRate-->",$studentAttendance[$aStudent][$enrolGroupID]['AttendancePercentageRounded'].'%',$emailContent);
		//	$success[] = $lwebmail->sendModuleMail($aStudentArr,$emailTitle,$emailContent,1,'','ParentOnly');
		
		$success[] = $lwebmail->sendModuleMail($_picIdAry,$emailTitle,$emailContent,1,'','User');
	}
}

// loop parent column
$i=0;
foreach($parentArray as $aStudentID){
	$emailTitle = str_replace("<!--clubName-->",$attendanceType,$Lang['eEnrolment']['AttenanceMailTitleFixed']).' ('.$groupTitle.')';
	$emailContent = str_replace("<!--studentName-->",$libuser->getNameWithClassNumber($aStudentID),$Lang['eEnrolment']['AttenanceMailContentFixed']);
	$emailContent = str_replace("<!--clubName-->",$groupTitle,$emailContent);
	$emailContent = str_replace("<!--datetime-->",$hid_activityDateStart,$emailContent);
	$emailContent = str_replace("<!--attendanceRate-->",$studentAttendance[$aStudentID][$enrolGroupID]['AttendancePercentageRounded'].'%',$emailContent);
	
	$appParentIdAry = $libuser->getParentUsingParentApp($aStudentID);
	if(!empty($appParentIdAry)){
		$sendToAppArr[] = $aStudentID;
		
		// Parent Using App
		$_individualMessageInfoAry = array();
		foreach ((array)$appParentIdAry as $parentId) {
//			$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$aStudentID;
			$_individualMessageInfoAry['relatedUserIdAssoAry'][$parentId] = (array)$aStudentID;
		}
		$_individualMessageInfoAry['messageTitle'] = $emailTitle;
		$emailContent = str_replace('<br/>',"\n\r",$emailContent);
		$_individualMessageInfoAry['messageContent'] = $emailContent;
		$individualMessageInfoAry[$i] = $_individualMessageInfoAry;

	}
	else{
		$sendToEmailArr[] = $aStudentID;
		
		// No App send Email
		$success[] = $lwebmail->sendModuleMail((array)$aStudentID,$emailTitle,$emailContent,1,'','ParentOnly');
	}
	
	//add Log
//	$Parents = $libuser->getParent($aStudentID);
//	$ParentsArr = array();
//	foreach($Parents as $aParent){
//		$sql = "INSERT INTO MODULE_RECORD_UPDATE_LOG (Module, Section, RecordDetail, LogDate, LogBy) Values ('eEnrolment','Send_Email_To_Absent_Student', '".$aParent['ParentID'].','.$attendanceType."', now(), '".$_SESSION['UserID']."')";
//		$libenroll->db_db_query($sql);
//		$ParentsArr[] = $aParent['ParentID'];
//	}
	$i++;
}

//Log
// for log
$targetStudent = implode(',',(array)$studentArray); 
$targetStudentP = implode(',',(array)$parentArray); // studentID
$studentP_app = implode(',',(array)$sendToAppArr);
$studentP_email = implode(',',(array)$sendToEmailArr);
$numMail = count($success);
$numSuccessMail = count(array_filter($success)); // include both student & parent
$LogDetails = "groupTitle: $groupTitle; attendance type: $attendanceType; targetStudent: $targetStudent; targetStudentP:$targetStudentP; studentP_app:$studentP_app; studentP_email:$studentP_email; numMail:$numMail; numSuccessMail:$numSuccessMail;";
$sql = "INSERT INTO MODULE_RECORD_UPDATE_LOG (Module, Section, RecordDetail, LogDate, LogBy) Values ('eEnrolment','Send_Email_To_Absent_Student', '".$LogDetails."', now(), '".$_SESSION['UserID']."')";
$libenroll->db_db_query($sql);

// prevent send to 0 parent app 
if(!empty($individualMessageInfoAry)){
	$appType = $eclassAppConfig['appType']['Parent'];
	$sendTimeMode = "";
	$sendTimeString = "";
	$isPublic = "N";
	$messageTitle = $Lang['eEnrolment']['AbsentNotification'].'('.$groupTitle.')';
	$notifyMessageId = $libeClassApp->sendPushMessage($individualMessageInfoAry, $messageTitle, 'MULTIPLE MESSAGES', $isPublic, $recordStatus=1, $appType, $sendTimeMode, $sendTimeString);
}

//$success = $lwebmail->sendModuleMail($studentArray,$emailTitle,$emailContent,1,'','ParentOnly');
//debug_pr($emailTitle);
//debug_pr($emailContent);
//debug_pr($hid_studentList);
if($hid_fromLink == 0)
	$link = "club_attendance_mgt.php?".$hid_query."&msg=3";
else if($hid_fromLink == 1)
	$link = "event_attendance_mgt.php?".$hid_query."&msg=3";
//if(!in_array(false,$success))
	header("Location: $link");	
?>