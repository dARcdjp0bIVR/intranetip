<?php
//using: anna
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");

intranet_auth();
intranet_opendb();
$li = new libdb();
$libenroll = new libclubsenrol();
$libgenral_settings = new libgeneralsettings();


$SettingArray = $libgenral_settings->Get_General_Setting($libenroll->ModuleTitle);
$SendEmail = $SettingArray['Committee_Recruitment_Send_Email'];

if ($type == "approve")
{	
	$status = 1;
}
else if ($type == "reject")
{
	$status = 0;
}

for ($i=0; $i<sizeof($UID); $i++)
{
    $thisUserID = $UID[$i];
    $sql = "UPDATE INTRANET_ENROL_COMMITTEE_STUDENT SET RecordStatus = '$status' WHERE EnrolGroupID = '".$EnrolGroupID."' and StudentID = '".$thisUserID."'";
	$li->db_db_query($sql);
}

if($SendEmail){
    $lwebmail = new libwebmail();
    
    $ToArray = $UID;
    $ClubInfo = $libenroll->GET_GROUPINFO($EnrolGroupID);
    $clubName = $ClubInfo['Title'];
    
    $email_subject= $Lang['EmailNotification']['eEnrolment']['CommitteeRecruitmentResult']['Subject'];
    $bodyContent = $status=='1'? $Lang['EmailNotification']['eEnrolment']['CommitteeRecruitmentResult']['ApproveContent'] :$Lang['EmailNotification']['eEnrolment']['CommitteeRecruitmentResult']['RejectContent'] ;

    $email_content = str_replace("__CLUB_NAME__", $clubName, $bodyContent);

    $lwebmail->sendModuleMail($ToArray,$email_subject,$email_content,1,'','User',true);
}

header("Location: committee_member_index.php?AcademicYearID=$AcademicYearID&msg=2&EnrolGroupID=$EnrolGroupID");
?>