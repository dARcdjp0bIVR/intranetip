<?php
# using:  

#############################################

#
#############################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$GroupID = $_GET['GroupID'];

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->HAVE_CLUB_MGT() && !$libenroll->HAVE_EVENT_MGT()  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
    header("Location: {$PATH_WRT_ROOT}/home/eAdmin/StudentMgmt/enrollment/");
    
    //$EnrolGroupID = $_REQUEST['EnrolGroupID'];
    $GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
    
    
    $libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin', $EnrolGroupID);
    
    $isClubPIC = $libenroll->IS_CLUB_PIC($EnrolGroupID);
    $isClubAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
    $CurrentPage = "PageMgtClub";# tags
    $tab_type = "club";
    $current_tab = 3;
    
    
    include_once("management_tabs.php");
    
    $CurrentPageArr['eEnrolment'] = 1;
    $MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
    
//     $STEPS_OBJ[] = array($eEnrollment['select_student'], 1);
//     $STEPS_OBJ[] = array($eEnrollment['lookup_for_clashes'], 0);
    
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
        
        $linterface = new interface_html();
        
        
        $PAGE_NAVIGATION[] = array($eEnrollment['button']['add'], "");
        
        
        
        $linterface->LAYOUT_START();
        
        //convert the array into a string to pass to another page for showing the selected student in the later viewing of member_index.php
        if(sizeof($UID)>0)
        {
            $uid_list = implode(",",$UID);
        }
        
        //convert the list of string into an array from add_role.php so as to show the previously selected student
        if($SelectedStudentID_list!="")
        {
            $StudentArr = explode(",", $SelectedStudentID_list);
        }
        
        if (!isset($StudentArr)){
            $StudentArr = array();
        }
        $StudentSel = "<select id='student[]' name='student[]' size='6' multiple='multiple'>";
        for($i = 0; $i < sizeof($StudentArr); $i++) {
            $StudentSel .= "<option value=\"".$StudentArr[$i]."\">".$LibUser->getNameWithClassNumber($StudentArr[$i])."</option>";
        }
        $StudentSel .= "</select>";
        
        ### UserID Selection Box start
        
        ### Auto-complete ClassName ClassNumber StudentName search
        $UserClassNameClassNumberInput = '';
        $UserClassNameClassNumberInput .= '<div style="float:left;">';
        $UserClassNameClassNumberInput .= '<input type="text" id="UserClassNameClassNumberSearchTb" name="UserClassNameClassNumberSearchTb" value="" />';
        $UserClassNameClassNumberInput .= '</div>';
        
        ### ClassName ClassNumber StudentName Textarea input
        $UserClassNameClassNumberTextarea = '';
        $UserClassNameClassNumberTextarea .= '<div style="float:left;" class="student_textarea">';
        $UserClassNameClassNumberTextarea .= '<textarea style="height:100px;" id="UserClassNameClassNumberAreaSearchTb" name="UserClassNameClassNumberAreaSearchTb"></textarea><span id="textarea_studentmsg"></span><br><br>';
        $UserClassNameClassNumberTextarea .= $linterface->GET_SMALL_BTN($Lang['AppNotifyMessage']['searchAndInsertStudent'], "button" , "");
        $UserClassNameClassNumberTextarea .= '</div>';
        
        ### Auto-complete login search
        $UserLoginInput = '';
        $UserLoginInput .= '<div style="float:left;">';
        $UserLoginInput .= '<input type="text" id="UserLoginSearchTb" name="UserLoginSearchTb" value="" />';
        $UserLoginInput .= '</div>';
        
        $StudentSel = "<select name='student[]' id='student[]' size='6' multiple='multiple'>";
        for($i = 0; $i < sizeof($StudentArr); $i++) {
            $StudentSel .= "<option value=\"".$StudentArr[$i]."\">".$LibUser->getNameWithClassNumber($StudentArr[$i])."</option>";
        }
        $StudentSel .= "</select>";
        
        $button_add_html = '<input type="button" class="formsubbutton" onClick="javascript:newWindow(\''.'/home/common_choose/enrolment_pic_helper.php?fieldname=student[]&page_title=SelectMembers&CatID=-2&permitted_type=2&type='.$type.'&AcademicYearID='.$AcademicYearID.'&EnrolGroupID='.$EnrolGroupID.'&GroupID='.$GroupID.'&filter='.$filter.'&checkAdmin=2, 9\');"  value="'.$button_select.'"><br />';
        
        $button_remove_html = $linterface->GET_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");
        
        $nextSelect .= "<table>";
        $nextSelect .= '<tr>
									<td class="tablerow2" valign="top">
										<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td class="tabletext">'.$i_general_from_class_group.'</td>
										</tr>
										<tr>
											<td class="tabletext">'.$button_add_html.'</td>
										</tr>
										<tr>
											<td class="tabletext"><i>'.$Lang['General']['Or'].'</i></td>
										</tr>
										<tr>
											<td class="tabletext">
												'.$i_general_search_by_inputformat.'
												<br />
												'.$UserClassNameClassNumberInput.'
											</td>
										</tr>
										</table>
									</td>
									<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
									<td class="tablerow2">
										<table width="100%" border="0" cellpadding="3" cellspacing="0">
										<tr>
											<td><i>'.$Lang['General']['Or'].'</i></td>
										</tr>
										<tr>
											<td class="tabletext">
												'.$Lang['AppNotifyMessage']['msgSearchAndInsertInfo'].'
												<br />
												'.$UserClassNameClassNumberTextarea.'
											</td>
										</tr>
										</table>
									</td>
									<td class="tabletext" ><img src="'. $image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
									<td align="left" valign="top">
										<table width="100%" border="0" cellpadding="5" cellspacing="0">
										<tr>
											<td align="left">
												'. $StudentSel .'
												'.$button_remove_html.'
											</td>
										</tr>
										<tr>
											<td>
												'.$linterface->Get_Thickbox_Warning_Msg_Div("MemberSelectionWarningDiv", '* '.$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember']).'
											</td>
										</tr>
										</table>
									</td>
								</tr>';
        $nextSelect .= "</table>";
        ### UserID Selection Box End
        
        $button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['student[]'])");
        
        //group-based class-based selection drop-down list
        $select_status_array = array (
            array (0,$i_ClubsEnrollment_StatusWaiting),
            array (2,$i_ClubsEnrollment_StatusApproved)
        );
        
        $StatusOptions = getSelectByArray($select_status_array, "name=\"status\"",$status, 0, 1);
        echo $linterface->Include_AutoComplete_JS_CSS();
        
        
        ?>
		
		<script language="JavaScript">
				
			
			function generalFormSubmitCheck(obj)
			{
			         checkOptionAll(obj.elements["student[]"]);

			         //obj.elements["student[]"].focus();
			         //obj.submit();
			}
			
		

					//K113573 Alternative Selection Medthod Start
			$( document ).ready(function(){
				Init_JQuery_AutoComplete('UserClassNameClassNumberSearchTb', 'UserLoginSearchTb');	
				eFormAppJS.func.init();
			});

			function Init_JQuery_AutoComplete(InputID_ClassNameClassNumber, InputID_UserLogin) {
				
				AutoCompleteObj_ClassNameClassNumber = $("input#" + InputID_ClassNameClassNumber).autocomplete(
					"ajax_search_user_by_classname_classnumber.php",
					{  			
						onItemSelect: function(li) {
							Add_Selected_User(li.extra[1], li.selectValue, InputID_ClassNameClassNumber);
						},
						formatItem: function(row) {
							return row[0];
						},
						maxItemsToShow: 100,
						minChars: 1,
						delay: 0,
						width: 200
					}
				);
				
				Update_Auto_Complete_Extra_Para();
			}
			function Add_Selected_User(UserID, UserName, InputID) {
				var UserID = UserID || "";
				var UserName = UserName || "";
				var UserSelected = document.getElementById('student[]');

				UserSelected.options[UserSelected.length] = new Option(UserName, UserID);
				
				Update_Auto_Complete_Extra_Para();
				
				// reset and refocus the textbox
				$('input#' + InputID).val('').focus();
			}
			
			function Update_Auto_Complete_Extra_Para(){
				checkOptionAll(document.getElementById('form1').elements["student[]"]);
				ExtraPara = new Array();
				ExtraPara['SelectedUserIDList'] = Get_Selection_Value('student[]', 'Array', true);
				AutoCompleteObj_ClassNameClassNumber[0].autocompleter.setExtraParams(ExtraPara);
			}
			var eFormAppJS = {
					vars: {
						ajaxURL: "ajax_search_user_by_classname_classnumber.php",
						xhr: "",
						selectedUserContainer: "UserClassNameClassNumberSearchTb"
					},
					listener: {
						insertPICFromTextarea: function(e) {
							var textarea = $('div.pic_textarea textarea').eq(0);

							if (textarea.val() == "") {
								textarea.focus();
							} else {
								eFormAppJS.func.ajaxFromTextArea(textarea, "pic");
							}
						},
						insertFromTextarea: function(e) {
							var textarea = $('div.student_textarea textarea').eq(0);
							if (textarea.val() == "") {
								textarea.focus();
							} else {
								eFormAppJS.func.ajaxFromTextArea(textarea, "student");
							}
							e.preventDefault();
						}
					},
					func: {
						ajaxFromTextArea: function(textarea, contentType) {
							var targetSelectID = "student[]";
							var textareaMsgObj = '#textarea_studentmsg';
							switch (contentType) {
								case "pic":
									textareaMsgObj = '#textarea_msg';
									targetSelectID = "target_PIC";
									break;
							}
							if (textarea.val() == "") {
								textarea.focus();
								$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
							} else {
								var xhr = eFormAppJS.vars.xhr;
								if (xhr != "") {
									xhr.abort();
								}
								var textareaVal = textarea.val();
								var getTargetListVal = "";
								if (contentType == "pic") {
									$('select#' + targetSelectID + ' option').each(function(ndex, obj) {
										if (getTargetListVal != "") {
											getTargetListVal += ",";
										}
										getTargetListVal += $(this).val();
									});
								} else { 
									$('select[name="' + targetSelectID + '"] option').each(function(ndex, obj) {
										if (getTargetListVal != "") {
											getTargetListVal += ",";
										}
										getTargetListVal += $(this).val();
									});
								} 
								
								eFormAppJS.vars.xhr = $.ajax({
									url: eFormAppJS.vars.ajaxURL,
									type: 'get',
									data: { "q": textareaVal.split("\n").join("||"), "searchfrom":"textarea", "nt_userType" : contentType, "SelectedUserIDList" : getTargetListVal },
									error: function() {
										textarea.focus();
									},
									success: function(data) {
										var lineRec = data.split("\n");
										if (lineRec.length > 0) {
											var UserSelected = "";
											if (contentType == "pic") {
												UserSelected = $('#' + targetSelectID);
											} else {
												UserSelected = $('select[name="' + targetSelectID + '"]').eq(0);
											}
											var afterTextarea = "";
											$.each(lineRec, function(index, data) {
												if (data != "") {
													console.log(data);
													var tmpData = data.split("||");
													console.log(tmpData);
													if (typeof tmpData[0] != "undefined" && typeof tmpData[1] != "undefined" && tmpData[1] != "NOT_FOUND") {  
														// Add_Selected_User(tmpData[1], tmpData[0], eFormAppJS.vars.selectedUserContainer);
														// UserSelected.options[UserSelected.length] = new Option(tmpData[0], tmpData[1]);
														UserSelected.append($("<option></option>")
											                    .attr("value", tmpData[1])
											                    .text(tmpData[0])); 
														
													} else {
														if (afterTextarea != "") {
															afterTextarea += "\n";
														}
														afterTextarea += tmpData[2];									
													}
												}
											});
											if (afterTextarea != "") {
												textarea.val(afterTextarea);
												$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
											} else {
												textarea.val("");
												$(textareaMsgObj).html('');
											}
											checkOptionAll(document.getElementById('form1').elements[targetSelectID]);
										} else {
											textarea.focus();
											$(textareaMsgObj).css({"color":"#f00"}).html('<br>' + '<?php echo $Lang['eNotice']['WrongUserLogin']; ?>');
										}
									}
								});
							}
						},
						init: function() {
							$('.form_table_v30 tr td tr td').css({"border-bottom":"0px"});

							$('div.student_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertFromTextarea);
							$('div.pic_textarea input[type="button"]').unbind('click', eFormAppJS.listener.insertPICFromTextarea);
							
							$('div.student_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertFromTextarea);
							$('div.pic_textarea input[type="button"]').bind('click', eFormAppJS.listener.insertPICFromTextarea);
						}
					}
				};
			//K113573 Alternative Selection Medthod End
			
		</script>

		<form id="form1" name="form1" action="add_committee_member_update.php" method="POST" >
			<table width="100%" border="0" cellspacing="4" cellpadding="4">
				<tr><td>
					<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
				</td></tr>
			</table>
				
		<table width="89%" align="center">	
			<tr><td>
		
			<table id="body_frame1" class="form_table_v30" width="75%">
				<br />		
				<tr>
					<? 	
					$thisTitle =  $eEnrollment['add_member'];					
					?>
					<td class="field_title"><?= $linterface->RequiredSymbol().$thisTitle?> </td>
					<td>
						<table> <!--[Select] and [Remove] button-->
							<tr>
								<td style="border-bottom:0px"><?= $nextSelect?></td>
							</tr>
						</table>
					</td>
				</tr>
		
			</table>		

			<table class="form_table_v30">
				<tr><td class="dotline" colspan="6" style="border-bottom: 0px;"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr><td align="center" colspan="6" style="border-bottom: 0px;">
					<div class="edit_bottom_v30">
						<?= $linterface->GET_ACTION_BTN($button_save, "submit", "javascript: generalFormSubmitCheck(document.form1);")?>&nbsp;				
						<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='committee_member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&UID_list=$uid_list&field=$field&order=$order&filter=$filter&keyword=$keyword'")?>&nbsp;					
					</div>
				</td></tr>
			</table>
			</td>
			</tr>
		</table>
			<input type="hidden" name="GroupID" id="GroupID" value="<?= $GroupID ?>" />
			<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="<?= $EnrolGroupID ?>" />
			<input type="hidden" name="keyword" id="keyword" value="<?= $keyword?>" />	
			<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>" />
		</form>
    <?
    	intranet_closedb();
	
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
?>