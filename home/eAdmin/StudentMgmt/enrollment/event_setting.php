<?php
// modifying: 

######### Change Log Start ###############
#	Date:	2020-10-14 (Philips)
#			- Hide enrolGroup, House, Age, Gender, AppMethod, Quota, AppStart, AppEnd, AppUserType for KIS with SDAS
#
#	Date:	2020-06-01 (Philips)
#			- Added Cust fields SDAS_Category & SDAS_AllowSync For CEES KG
#
#   Date:   2020-02-28 (Tommy)
#           - changed ApplyUserType default select parent for KIS
#
#   Date:   2020-01-16 (Tommy)
#           - hide ole and WebSAMS in KIS
#
#   Date:   2019-11-22 (Henry)
#           - bug fix: no access right of club related activities of previous year as club PIC
#
#   Date:   2019-09-13 (Cameron)
#           - unlimit time and memory for HKPF
#
#   Date:   2019-05-24 (Anna)
#           - Changed attachment file from view to downlowd_attachemnt format
#
#   Date:   2018-10-25 Cameron
#           - hide N/A option for EnrolGroupID selection
#
#   Date:   2018-10-03 Philips
#           - modified js Func EmptyAttachment() (return {a,b} since IE not supported)
#
#   Date:   2018-08-06 Cameron
#           hide items for HKPF
#
#	Date:	2017-12-14 Pun
#			added NCS cust
#			
#	Date:	2017-12-13 Anna
#			added $IsActivityPIC
#			
#	Date:	2017-09-28 Anna
#			Added submit disable after click
#
#	Date:	2017-09-28 Anna #P110324
#			new event add reply slip
#
#	Date:	2017-09-18 Anna #E117616
#			Added WebSAMSSetting 
#
#	Date:	2017-04-24 Villa #P110324 
#			Add ReplySlip Related 	
#
##	Date:	2016-07-22 Cara
##			Add InputBy, InputDate, ModifiedBy, ModifiedDate
##
##	Date:	2016-06-22 Henry HM
##			Add SPTSS Cust //$sys_custom['eEnrolment']['ActivityIncludeInReports']
##
#	Date:	2015-08-12 Omas
#			fix checking word count for fckeditor to Description
#
#	Date:	2015-06-08	Evan
#			Update UI
#
#	Date:	2014-11-21 Omas
#			Add target house check box to enable house mapping
#
#	Date:	2014-07-23 Carlos
#			Customization $sys_custom['eEnrolment']['TWGHCYMA'] - added RelatedGroupID and other Joint Activity fields
#
#	Date:	2014-06-19 Bill
#			Customization $sys_custom['eEnrolment']['ActivityBudgetField'] - added budget field
#
#	Date:	2014-04-29 Carlos
#			Customization $sys_custom['eEnrolment']['TWGHCYMA'] - added INTRANET_GROUP relation
#
#	Date:	2014-04-25 Ivan
#			added GatheringLocation and DismissLocation field
#
#	Date:	2014-04-07 Carlos
#			- Added customization $sys_custom['eEnrolment']['TWGHCYMA'] for TWGHs C Y MA MEMORIAL COLLEGE
#			- 1) Multiple categories; 2) Volunteer Service & Service Hour; 3) Activity Location; 4) Activity Natures
#
#	Date:	2013-08-09 Cameron
#			- Pass categoryTypeID to GET_CATEGORY_SEL()			
#
#	Date:	2013-05-07 Rita
#			add transport, location, organizor, rewards/certificatino customization field
#
#	Date: 	2012-12-04 Rita
#			Add $libenroll->enableActivitySurveyFormRight() customization for survey form  [Case#2012-0803-1543-49054]
#
#	Date:	2011-11-11	YatWoon
#			Fixed: Failed to display term-base club for selection
#
#	Date:	2010-07-22/2010-08-25	YatWoon
#			according to $libenroll->clubPICcreateNAAct, check the user can create NA activity or not
#
#   Date:	2010-08-02 Thomas
#			Check the data is freezed or not
#			Show alert message when adding / updating the data
#
#	Date:	2010-05-07	YatWoon
#			enlarge the width of html editor
#
#	Date:	2010-03-09	Marcus
#			replace content textarea by html editor
#
#	Date:	2010-01-28	YatWoon
#			Add "School Activity" option
#
######### Change Log Start ################

set_time_limit(0);
ini_set('memory_limit',-1);

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();
$AcademicYearID = IntegerSafe($AcademicYearID);
if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();
	
$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolEventID = IntegerSafe($EnrolEventID);

$libenroll = new libclubsenrol($AcademicYearID);

if(trim($EnrolEventID)!= '')
{
	$EnrollEventArr = $libenroll->GET_EVENTINFO($EnrolEventID);
	$DateInput = $EnrollEventArr['DateInput'];
	$InputBy = $EnrollEventArr['InputBy'];
	
	if ($InputBy == Null){
		$InputBy = $Lang['General']['EmptySymbol'];
	} else{
 		$LibUser = new libuser($InputBy);
 		$CreatorEnglishName = $LibUser->EnglishName;
		$CreatorChineseName = $LibUser->ChineseName;
		$InputBy = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
	}
	
	if ($DateInput == Null){
		$DateInput = $Lang['General']['EmptySymbol'];
	}
	
	$ModifiedBy = $EnrollEventArr['ModifiedBy'];
	$DateModified = $EnrollEventArr['DateModified'];
	if ($ModifiedBy == Null){
		$ModifiedBy = $Lang['General']['EmptySymbol'];
	} else{
 		$LibUser = new libuser($ModifiedBy);
 		$CreatorEnglishName = $LibUser->EnglishName;
 		$CreatorChineseName = $LibUser->ChineseName;
		$ModifiedBy = Get_Lang_Selection($CreatorChineseName, $CreatorEnglishName);
	}
	if ($DateModified == Null){
		$DateModified = $Lang['General']['EmptySymbol'];
	}
	$ActivityEnrolGroupID = $EnrollEventArr['EnrolGroupID'];
	$IsActivityClubPIC = $libenroll->IS_CLUB_PIC($ActivityEnrolGroupID, $AcademicYearID);
	$IsActivityPIC = $libenroll->IS_EVENT_PIC($EnrolEventID);

	
	if($sys_custom['eEnrolment']['TWGHCYMA'] && $plugin['eBooking'] && in_array($EnrollEventArr['SchoolActivity'],array(1,2))){
		if($libenroll->Remove_Non_Existing_Activity_Location_Booking_Record($EnrolEventID)){
			$EnrollEventArr = $libenroll->GET_EVENTINFO($EnrolEventID); // reload the record info
		}
	}
	$EnrollEventArr['SDAS_AllowSync'] = explode(',',$EnrollEventArr['SDAS_AllowSync']);
} else {
	$EnrollEventArr['SDAS_AllowSync'] = array();
}

if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && ((trim($EnrolEventID)=='' && !$libenroll->IS_CLUB_PIC()) || (trim($EnrolEventID)!='' && !$IsActivityClubPIC))
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

$LibUser = new libuser($UserID);	

if ($plugin['eEnrollment'])
{
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
	//	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	//$CurrentPage = "PageMgtActEventAdd";
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent()) || ($LibUser->isParent())) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
		
		$button_cancel_para = '&pic_view=1';
		$picView_HiddenField = "<input type='hidden' name='pic_view' value='1' />";
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	//$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        //$TAGS_OBJ[] = array($eEnrollmentMenu['mgt_event'], "", 1);
        # tags 
        $tab_type = "activity";
        $current_tab = 1;
        include_once("management_tabs.php");
        # navigation
        if (isset($EnrolEventID))
        {
	        $PAGE_NAVIGATION[] = array($eEnrollment['edit_activity'], "");
	        $isNew = 0;
        }
        else
        {
	        $PAGE_NAVIGATION[] = array($eEnrollment['new_activity'], "");
	        $isNew = 1;
        }
        

        $DefineActivityTitle = $eEnrollment['add_activity']['step_1'];
        $AssignStaffTitle = $eEnrollment['add_activity']['step_1b'];
		$SetRightTitle = $eEnrollment['add_activity']['step_3'];
		$Step3 = $eEnrollment['add_club_activity']['WebSAMSSetting'];
		$Step4 = $Lang['eEnrolment']['CreatedAndModifiedRecords'];
        
        $linterface->LAYOUT_START();


$AppStart = ($libenroll->Event_AppStart != '')? $libenroll->Event_AppStart : date('Y-m-d');
$AppStartHour = ($libenroll->Event_AppStartHour != '')? $libenroll->Event_AppStartHour : '00';
$AppStartMin = ($libenroll->Event_AppStartMin != '')? $libenroll->Event_AppStartMin : '00';

$AppEnd = ($libenroll->Event_AppEnd != '')? $libenroll->Event_AppEnd : date('Y-m-d');
$AppEndHour = ($libenroll->Event_AppEndHour != '')? $libenroll->Event_AppEndHour : '23';
$AppEndMin = ($libenroll->Event_AppEndMin != '')? $libenroll->Event_AppEndMin : '55';

($EnrollEventArr['ApplyMethod'] == 0) ? $chkRan = "selected" : $chkTime = "selected" ;
$tiebreakerSelection = "<SELECT name=tiebreak>\n";
$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
$tiebreakerSelection.= "</SELECT>\n";

if ($sys_custom['eEnrolment']['CategoryType']) {
	$categoryTypeID = array(2);	// Activity
}
else {
	$categoryTypeID = 0;
}

# House limit Selection
###############################################################################################
$HouseArr = $libenroll->Get_GroupMapping_Group($AcademicYearID, HOUSE_RECORDTYPE_IN_INTRANET_GROUP);
$HouseAssoArr = BuildMultiKeyAssoc($HouseArr, 'GroupID',array('Title'),1);

if(isset($_GET)){
	$houseMapping = $libenroll->Get_Club_Event_Mapping($enrolConfigAry['Activity'], $EnrolEventID);
	if(empty($houseMapping)){
		$setDefaultCheck = 'checked';
		$setHouseCheck = '';
		$houseDiv ='display:none';
	}
	else{
		$setDefaultCheck = '';
		$setHouseCheck = 'checked';
		$houseDiv ='display:block';
	}
}
else{
	$setDefaultCheck = 'checked';
	$setHouseCheck = '';
}

#build checkbox
$houseChkbox = '';
$houseChkbox .= "<div id=\"house\" name=\"house\" style=\"$houseDiv;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	foreach($HouseAssoArr as $ID => $name){
		if(in_array($ID,$houseMapping)){
			$houseChk = 1;
		}
		else{
			$houseChk = 0;
		}
		$houseChkbox .= $linterface->Get_Checkbox('house_Id_'.$ID, 'houseIdAry[]', $Value=$ID, $isChecked=$houseChk, $Class='', $Display=$name, $Onclick='', $Disabled='');
	}
$houseChkbox .= "</div>";
###############################################################################################

# Last selection - PIC
###############################################################################################
$picArr = $libenroll->GET_EVENTSTAFF($EnrollEventArr['EnrolEventID'], "PIC");
$PICSel = "<select id='pic[]' name='pic[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($picArr); $i++) {
	$PICSel .= "<option value=\"".$picArr[$i][0]."\">".$LibUser->getNameWithClassNumber($picArr[$i][0], $IncludeArchivedUser=1)."</option>";
}
$PICSel .= "</select>";

//if (sizeof($picArr)>0)
//{
	$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");
//}
###############################################################################################


# Last selection - helper
###############################################################################################
$helperArr = $libenroll->GET_EVENTSTAFF($EnrollEventArr['EnrolEventID'], "HELPER");
$HELPERSel = "<select id='helper[]' name='helper[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($helperArr); $i++) {
	$HELPERSel .= "<option value=\"".$helperArr[$i][0]."\">".$LibUser->getNameWithClassNumber($helperArr[$i][0], $IncludeArchivedUser=1)."</option>";
}
$HELPERSel .= "</select>";

//if (sizeof($helperArr)>0)
//{
	$button_remove_html_helper = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['helper[]'])");
//}
###############################################################################################
#P110324
if($sys_custom['eEnrolment']['ReplySlip']){
	//Get ALL the ReplySlip Related to this Group
	if($EnrolEventID !=''){
		$rs = $libenroll->getReplySlipEnrolGroupRelation('',$EnrolEventID,'2');
		$ReplySlipGroupMappingID = $rs[0]['ReplySlipGroupMappingID'];
		if($ReplySlipGroupMappingID){
			$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
		}
		$ReplySlipID = $rs[0]['ReplySlipID'];
		//Get ReplySlip Detail
		$ReplySlipInfo = $libenroll->getReplySlip($ReplySlipID);
	}else{
		$ReplySlipID = '';
		$ReplySlipInfo = array();
	}

	
	$replyFormHTML = '';
	$replyFormHTML .= '<tr>';
	$replyFormHTML .= '<td class="field_title">'.$eEnrollment['replySlip']['replySlip'].'</td>';
	$replyFormHTML .= '<td>';
//	if(!$ReplySlipReply){
		$replyFormHTML .= '<input type="button" class="formsubbutton" onclick="newWindow(\'ReplySlip_Edit.php\',1)" value="'.$eEnrollment['replySlip']['Edit'].'" onmouseover="this.className=\'formsubbutton\'" onmouseout="this.className=\'formsubbutton\'">';
//	}
	$replyFormHTML .= '<input type="button" class="formsubbutton" onclick="newWindow(\'ReplySlip_Preview.php\',10)" value="'.$eEnrollment['replySlip']['Preview'].'" onmouseover="this.className=\'formsubbutton\'" onmouseout="this.className=\'formsubbutton\'">';
	$replyFormHTML .= '</td>';
	$replyFormHTML .= '</tr>';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipID" value="'.$ReplySlipID.'">';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipTitle" value="'.$ReplySlipInfo[0]['Title'].'">';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipInstruction" value="'.$ReplySlipInfo[0]['Instruction'].'">';
	$replyFormHTML .= '<input type="hidden" name="ReplySlipRecordType" value="'.$rs[0]['RecordType'].'">';
	$replyFormHTML .= '<input type="hidden" name="qStr" value="'.$ReplySlipInfo[0]['Detail'].'">';
	$replyFormHTML .= '<input type="hidden" name="aStr" value="">';
	$replyFormHTML .= '<input type="hidden" name="DisplayQuestionNumber" value="'.$ReplySlipInfo[0]['DisplayQuestionNumber'].'">';
	$replyFormHTML .= '<input type="hidden" name="AllFieldsReq" value="'.$ReplySlipInfo[0]['AllFieldsReq'].'">';
}


$lclass = new libclass();
$ClassLvlArr = $lclass->getLevelArray();
$GroupArr = $libenroll->GET_EVENTCLASSLEVEL($EnrollEventArr['EnrolEventID']);
$GroupYearID = Get_Array_By_Key($GroupArr,"ClassLevelID");

if (trim($EnrolEventID)=='')
	$checked = " checked ";
else
	$checked = '';
$ClassLvlChk = "<input type=\"checkbox\" id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.form1, 'classlvl[]') : setChecked(0, document.form1, 'classlvl[]');\" $checked><label for=\"classlvlall\">".$eEnrollment['all']."</label>";

$ClassLvlChk .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = "";
	for ($j = 0; $j < sizeof($GroupArr); $j++) {	
		if (in_array($ClassLvlArr[$i][0],$GroupYearID)) {	
			 $checked = " checked ";
			 break;
		}
	}
	
	if (trim($EnrolEventID)=='')
		$checked = " checked ";
	
	if (($i % 10) == 0) $ClassLvlChk .= "<tr>";
	$ClassLvlChk .= "<td class=\"tabletext\"><input type=\"checkbox\" $checked id=\"classlvl{$i}\" name=\"classlvl[]\" value=\"".$ClassLvlArr[$i][0]."\"><label for=\"classlvl{$i}\">".$ClassLvlArr[$i][1]."</label></td>";
	if (($i % 10) == 9) $ClassLvlChk .= "</tr>";
}
$ClassLvlChk .= "</table>";

if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    $minAge = 3;
    $maxAge = 7;
}else{
    $minAge = 11;
    $maxAge = 21;
}
if ($EnrollEventArr['LowerAge'] != 0) $LowerAge = $EnrollEventArr['LowerAge'];
$LowerAgeSel = "<select name=\"LowerAge\" id=\"LowerAge\">";
for ($i = $minAge; $i < $maxAge; $i++) {
	$LowerAgeSel .= "<option value=\"$i\"";
	if ($LowerAge == $i) $LowerAgeSel .= " selected ";
	$LowerAgeSel .= ">".$i."</option>";
}
$LowerAgeSel .= "</select>";

if ($EnrollEventArr['UpperAge'] != 0) $UpperAge = $EnrollEventArr['UpperAge'];
$UpperAgeSel = "<select name=\"UpperAge\" id=\"UpperAge\">";
for ($i = $minAge; $i < $maxAge; $i++) {
	$UpperAgeSel .= "<option value=\"$i\"";
	if (($UpperAge == $i)||(($i == $maxAge-1)&&(!$UpperAgeSelected))) {
		$UpperAgeSelected = true;
		$UpperAgeSel .= " selected ";
	}
	$UpperAgeSel .= ">".$i."</option>";
}
$UpperAgeSel .= "</select>";


$filecount = 0;
//for ($i = 10; $i < 15; $i++) {
for ($i = 8; $i < 18; $i++) {
	if ($EnrollEventArr[$i] != "") $filecount++;
}

$groups = $libenroll->getGroupsForEnrolSet("","","","",$AcademicYearID);
$quotas = $libenroll->getGroupQuota();
for ($i=0; $i<sizeof($groups); $i++)
{
     $id = $groups[$i][0];
     if ($quotas[$id]!="")
     {
         $chkAllow[$id] = true;
     }
}
if (trim($EnrolEventID==''))
	$ActivityQuota = 0;
else
	$ActivityQuota = $EnrollEventArr['Quota'];

	
if ($libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
{
	# Enrolment Admin & Enrolment Master => Show all clubs
	//$GroupInfoArr = $libenroll->getGroupInfoList("","","","","",$AcademicYearID);
	$GroupInfoArr = $libenroll->getGroupInfoList(1,"","","","",$AcademicYearID,"GroupCode");
	# Can Select N/A option
	if (!$sys_custom['project']['HKPF']) {
    	$NA_Option_HTML = "<option value=\"\">".$i_notapplicable."</option>";
    	$selection = true;
	}
}
else
{

    if(($libenroll->clubPICcreateNAAct || $IsActivityPIC) && !$sys_custom['project']['HKPF'])
	{
		$NA_Option_HTML = "<option value=\"\">".$i_notapplicable."</option>";
		$selection = true;
	}
	else
	{
		# Cannot Select N/A option
		$NA_Option_HTML = "";
	}	
//	debug_pr($libenroll->clubPICcreateNAAct);
	$PIC_EnrolGroupIDArr = $libenroll->Get_PIC_Club($_SESSION['UserID']);
	$numOfPICClub = count($PIC_EnrolGroupIDArr);
	
	if ($numOfPICClub > 0)
	{
		# Club PIC => can select pic clubs and current activity's club
		if ($EnrollEventArr['EnrolGroupID'] != '')
			$PIC_EnrolGroupIDArr[] = $EnrollEventArr['EnrolGroupID'];
			
			$GroupInfoArr = $libenroll->getGroupInfoList($isAll=1, '', $PIC_EnrolGroupIDArr,"","",$AcademicYearID,"GroupCode");
		$selection = true;
	}
	else
	{
		$selection = false;
		# Only allowed to select the current activity's club
		if ($EnrollEventArr['EnrolGroupID'] != '')
			$DisplayGroupIDArr = array($EnrollEventArr['EnrolGroupID']);
		if(sizeof($DisplayGroupIDArr)>0)
			$GroupInfoArr = $libenroll->getGroupInfoList($isAll=1, '', $DisplayGroupIDArr,"","",$AcademicYearID,"GroupCode");
	}
}

// debug_pr($GroupInfoArr);
// if(!$libenroll->clubPICcreateNAAct )

# if NA club and 
if($EnrolEventID && !$NA_Option_HTML && $EnrollEventArr['EnrolGroupID']==0)
{
	$GroupSel = $i_notapplicable;
	$CannotAddUpdate = true;
}
else
{
	$GroupSel = "<select id=\"EnrolGroupID\" name=\"EnrolGroupID\"".($sys_custom['eEnrolment']['TWGHCYMA']?" onchange=\"EnrolGroupChanged(this);\" ":"").">";
	$GroupSel .= $NA_Option_HTML;
	for ($i = 0; $i < sizeof($GroupInfoArr); $i++) {
		$id = $GroupInfoArr[$i]['EnrolGroupID'];
		$Code = $GroupInfoArr[$i]['GroupCode'];
		$CodeDisplay = $Code==''? "": $Code." - ";
		if ($chkAllow[$id]) {
			($EnrollEventArr['EnrolGroupID'] == $id) ? $selected = "selected" : $selected = "";
			
			# Attach Semester Name
			if ($GroupInfoArr[$i]['Semester'] == '' || $GroupInfoArr[$i]['Semester'] == 0)
			{
				$thisSemName = $i_ClubsEnrollment_WholeYear;
			}
			else
			{
				$objTerm = new academic_year_term($GroupInfoArr[$i]['Semester']);
				$thisSemName = $objTerm->Get_Year_Term_Name();
			}
			
			$TitleDisplay = $intranet_session_language=="en"?$GroupInfoArr[$i][1]:$GroupInfoArr[$i]['TitleChinese'];
			$GroupSel .= "<option value=\"".$id."\" $selected>".$CodeDisplay.$TitleDisplay." ($thisSemName)"."</option>";	
		}
	}
	$GroupSel .= "</select>";
}

$EventTitle = intranet_htmlspecialchars($EnrollEventArr['EventTitle']);

## Fee - check if to be confirmed
if ($EnrollEventArr['isAmountTBC'] == 1)
{
	$TBC_checked = true;
	$Amount_disabled = 'disabled';
}
else
{
	$TBC_checked = false;
	$Amount_disabled = '';
}

### Student cannot remove teacher staff - add js array to check
if ($LibUser->isStudent())
{
	$StaffIDArr = $libenroll->Get_Staff_List();
	
	foreach($StaffIDArr as $key => $StaffUserID)
	{
		$jsArray .= "jsStaffIDArr[$key] = $StaffUserID;\n";
	}
}

# Get HTML editor
$msg_box_width = 600;
$msg_box_height = 200;
$obj_name = "Description";
$editor_width = $msg_box_width;
$editor_height = $msg_box_height;

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$oFCKeditor = new FCKeditor($obj_name,$editor_width,$editor_height);
$oFCKeditor->Value =  $EnrollEventArr['Description'];
$HTMLEditor = $oFCKeditor->Create2();

$activityCode = $EnrollEventArr['ActivityCode'];

### Category List for OLE category mapping
$categoryAry = $libenroll->GET_CATEGORY_LIST();
$categoryAssoAry = BuildMultiKeyAssoc($categoryAry, 'CategoryID', 'OleCategoryID', $SingleValue=1);
$x = '';
$x .= 'var categoryMappingAry = new Array();'."\n";
foreach ((array)$categoryAssoAry as $_categoryId => $_oleCategoryId) {
	$x .= 'categoryMappingAry['.$_categoryId.'] = \''.$_oleCategoryId.'\';'."\n";
}
$htmlAry['initializeCategoryMappingAry'] = $x;
?>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/html_editor/fckeditor_cust.js" ></script>
<script language="javascript">
<?=$htmlAry['initializeCategoryMappingAry']?>
$(document).ready(function () {
	clickedIntExtRadio();
	<?php for($i=1;$i<=10;$i++){
	    $temp_str = "AttachmentLink". $i;
	    if($EnrollEventArr[$temp_str]!=""){
      
	        $url = $file_path."/file/" . $EnrollEventArr[$temp_str];
	        echo "filelinks[$i] = '../../../../home/download_attachment.php?target_e=".getEncryptedText($url)."';\n";	        
// 	        echo "filelinks[$i] = '/file/" . $EnrollEventArr[$temp_str] . "';\n";
	        echo "filenames[$i] = '" .$libenroll->ShowFileName($EnrollEventArr[$temp_str]) . "';\n";
	    }
	}?>
	initAttachment();
	if(emptyfields.length > 0){
		popAttachment();
	}
// 	$("#form1").submit(function () {
// 		   $("#submitform").attr("disabled", true);
// 		     return true;
// 	 });

	<?php if($plugin['SDAS_module']['KISMode']){?>
		$('input[name=nature]').change(function(){
			checkActivityNature();
		});
		$('input[name=nature]').change();
	<?php }?>
});
var filecount = <?=$filecount?>;
var filelinks = [];
var filenames = [];
var emptyfields = [];
function initAttachment(){
	for(var i=1; i<=10; i++){
		if(filelinks[i]!=null) existedAttachment(i);
		else {
			emptyfields.push(emptyAttachment(i));
		}
	}
}
function emptyAttachment(index){
	var a = document.createElement('input');
	a.setAttribute('type', 'file');
	a.setAttribute('id', 'attach[]');
	a.setAttribute('name', 'attach[]');
	a.setAttribute('class', 'textboxtext');
	var onchangeScript = "var Ary";
	onchangeScript += index-1;
	onchangeScript += " = this.value.split('\\\\'); document.getElementById('attachname";
	onchangeScript += index-1;
	onchangeScript += "').value = Ary" ;
	onchangeScript += index-1; 
	onchangeScript += "[Ary";
	onchangeScript += index-1;
	onchangeScript += ".length-1];";
	console.log(onchangeScript);
	a.setAttribute('onChange', onchangeScript);
	var b = document.createElement('input');
	b.setAttribute('type', 'hidden');
	var attachname = "attachname" + (index-1);
	b.setAttribute('name', attachname);
	b.setAttribute('id', attachname);
	return {a:a, b:b};
}
function popAttachment(){
	var obj = emptyfields.shift();
	<?php $button = str_replace("'","\'", $linterface->GET_SMALL_BTN($Lang['Btn']['Add'], "button","$(this).hide();popAttachment();","addNewAttachment")); ?>
	var btn = '<?=$button?>';
	var td = $('#attachmentTD');
	td.append(obj.a).append(obj.b);
	if(emptyfields.length > 0){
		td.append(btn);
	}
}
function existedAttachment(index){
	var td = $('#attachmentTD');
	var a = document.createElement('a');
	a.setAttribute('href', filelinks[index]);
	a.innerHTML = filenames[index];
	var b = document.createElement('a');
	b.setAttribute('href', "<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>");
	b.setAttribute('onClick', "document.getElementById('FileToDel').value='"+index+"';document.form1.action='event_file_delete_update.php';");
	b.setAttribute('class', 'tablelink');
	b.innerHTML = "<?=$button_remove?>";
	td.append($(a));
	td.append('&nbsp;(');
	td.append($(b));
	td.append(')<br/>');
}
function getEditorValue(instanceName) {
	// Get the editor instance that we want to interact with.
	var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;
	
	// Get the editor contents as XHTML.
	return oEditor.GetXHTML( true ) ; // "true" means you want it formatted.
}

function FormSubmitCheck(obj)
{
	/******************** define activity form checking start *******************/
	if(!check_text(obj.act_title, "<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_name']; ?>.")) return false;

<?php if ($userBrowser->platform!="iPad" && $userBrowser->platform!="Andriod") { ?> 
// 	if(word_count("Description").total== 0)
// 	{
//		alert("<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_content']; ?>");
// 		return false;
// 	}
<?php } ?>

	//if(!check_text(obj.Description, "<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_content']; ?>.")) return false;
	//if(!check_text(obj.act_category, "<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_category']; ?>.")) return false;	
<?php if($sys_custom['eEnrolment']['TWGHCYMA']){ ?>
		if($('input[name="sel_category[]"]:checked').length == 0){
			alert("<?php echo $i_alert_pleaseselect.$eEnrollment['add_activity']['act_category']; ?>.");
			return false;
		}
		
		if($('input[name="IsVolunteerService"]:checked').val()=="1"){
			var str_service_hour = $.trim($('#VolunteerServiceHour').val());
			var num_service_hour = parseFloat(str_service_hour);
			if(str_service_hour == '' || (isNaN(num_service_hour) || num_service_hour < 0)){
				alert('<?=$Lang['eEnrolment']['InputVolunteerServiceHourWarning']?>');
				return false;
			}
		}
		
		if($('#nature0').is(':checked') || $('#nature2').is(':checked')){
			var str_ext_location = $.trim($('#ActivityExtLocation').val());
			if(str_ext_location == ''){
				alert('<?=$Lang['eEnrolment']['InputActivityExternalLocationWarning']?>');
				return false;
			}
		}
		
		if($('input[name="activity_nature[]"]:checked').length == 0){
			alert('<?=$Lang['eEnrolment']['SelectActivityNatureWarning']?>.');
			return false;
		}
	
<?php }else{ ?>	
		if (obj.sel_category.value == '') {
			alert("<?php echo $i_alert_pleaseselect.$eEnrollment['add_activity']['act_category']; ?>.");
			return false;
		}	
<?php } ?>	


<?php if (!$sys_custom['project']['HKPF']): // ***** start not HKPF js?>

	if (!check_positive_int(obj.PaymentAmount,"<?=$eEnrollment['js_fee_positive']?>")) return false;	
	
	<?php if($sys_custom['eEnrolment']['ActivityBudgetField']) { ?>
			if (!check_positive_int(obj.BudgetAmount,"<?=$Lang['eEnrolment']['Settings']['BudgetAmountEmpty']?>"))
				return false;
	<? } ?>


	

		
	if(!check_text(obj.PaymentAmount, "<?php echo $i_alert_pleasefillin.$eEnrollment['PaymentAmount']; ?>.")) return false;
	
	<?php if($sys_custom['eEnrolment']['ActivityBudgetField']) { ?>
			if(!check_text(obj.BudgetAmount, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['Settings']['BudgetAmount']; ?>.")) return false;
	<? } ?>
	
	// Target Group Checking - at least select one target group
	var i;
	var allNotChecked = true;
	for (i=0; i< <?=sizeof($ClassLvlArr)?>; i++)
	{
		var classLevelChkBox = document.getElementById('classlvl'+i);
		if (classLevelChkBox.checked)
		{
			allNotChecked = false;
		}
	}
	if (allNotChecked)
	{
		alert('<?=$eEnrollment['js_target_group_alert']?>');
		return false;	
	}
	
	/*
	if (confirm("<?= $eEnrollment['js_del_warning']?>")) {
		obj.submit();
	} else {
		return false;
	}
	*/
	
	//check target house
	if($('input#set_house').attr('checked')){
		var counter = 0;
		$('[name="houseIdAry[]"]').each(function() {
			if($(this).attr('checked')){
				counter++;
			}
		});
		if(counter == 0){
			alert('<?=$Lang['eEnrolment']['jsWarning']['SpecifyHouse']?>');
			return false;
		}	
	}
		
	
	// enable back the payment amount so that the value can be saved
	document.getElementById('PaymentAmount').disabled = false;
	
	/******************** define activity form checking end *******************/
	
	/******************** assign staff form checking start *******************/
	var jsPICArr = document.getElementById('pic[]');
	var jsNumOfPIC = jsPICArr.length;
	
	if (jsNumOfPIC == 0) {
		alert('<?= $eEnrollment['js_sel_pic']?>');
		return false;
	}
	
	var jsHelperArr = document.getElementById('helper[]');
	var jsNumOfHelper = jsHelperArr.length;
	
	// Check if PIC and helper duplicated
	if (jsNumOfPIC > 0 && jsNumOfHelper > 0)
	{
		var i;
		var j;
		
		for (i=0; i<jsNumOfHelper; i++)
		{
			for (j=0; j<jsNumOfPIC; j++)
			{
				if (jsHelperArr[i].value == jsPICArr[j].value && jsHelperArr[i].value != '' && jsPICArr[j].value != '')
				{
					alert("<?=$Lang['eEnrolment']['jsWarning']['DuplicatedPersonInPicAndHelper']?>");
					return false;
				}
			}
		}
	}
	
//	if (!check_positive_int(obj.Quota,"<?=$ec_warning['please_enter_pos_integer']?>"))
//		return false;


<?php if(!($plugin['SDAS_module']['KISMode'] && $_SESSION['platform'] == "KIS")){?>
	if (obj.ApplyStartTime.value != '')
	{
		if (!check_date(obj.ApplyStartTime,"<?php echo $i_invalid_date; ?>")) return false;
	}
	if (obj.ApplyEndTime.value != '')
	{
		if (!check_date(obj.ApplyEndTime,"<?php echo $i_invalid_date; ?>")) return false;
	}

	if (obj.ApplyStartTime.value > obj.ApplyEndTime.value)
	{
		alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
		return false;
	}
	else if (obj.ApplyStartTime.value == obj.ApplyEndTime.value)
	{
		// check hours
		if (obj.ApplyStartHour.value > obj.ApplyEndHour.value)
		{
			alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
			return false;
		}
		else if (obj.ApplyStartHour.value == obj.ApplyEndHour.value)
		{
			// check minutes
			if (obj.ApplyStartMin.value > obj.ApplyEndMin.value)
			{
				alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
				return false;
			}
		}
	}

	if (!check_positive_int(obj.Quota,"<?=$ec_warning['please_enter_pos_integer']?>"))
	{ 
    	return false;	
	}
	if(!check_text(obj.Quota, "<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_quota']; ?>.")){
		return false;
	}
<?php }?>
	
	/******************** assign staff form checking end *******************/
	
	generalFormSubmitCheck(obj);
	
	<? if($isNew){?>
		if(confirm("<?=$Lang['eEnrolment']['DirectToDateSetting']?>"))
		{
			obj.DirectToMeetingActivity.value = 1;
		} 
	<? } ?>

<?php endif; // ***** end not HKPF js?>

	obj.submit();

}

function js_ClickFeeConfirmed()
{
	if (document.getElementById('isAmountTBC').checked == false)
	{
		document.getElementById('PaymentAmount').disabled = false;
	}
	else
	{
		document.getElementById('PaymentAmount').disabled = true;
	}
	
}

/** assign staff script **/
Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++) {
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}

var jsStaffIDArr = new Array();
<?=$jsArray?>

function checkOptionRemove(obj){
	checkOption(obj);
	i = obj.selectedIndex;
	while(i!=-1){
		if (jsStaffIDArr.in_array(obj.options[i].value))
		{
			alert("<?=$Lang['eEnrolment']['jsWarning']['StudentCannotRemoveTeacher']?>");
			break;
		}
		
		if (obj.options[i].value == <?= $_SESSION['UserID'] ?>)
		{
			if (!confirm("<?=$Lang['eEnrolment']['jsWarning']['RemoveMyselfWarning']?>"))
				break;
		}
			
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
}

function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["pic[]"]);
         checkOptionAll(obj.elements["helper[]"]);
         //obj.submit();
}
/** assign script end**/


function clickedIntExtRadio() {
	var isINT = $('#intExt_INT').attr('checked');
	
	if (isINT) {
		$('.eleChk').attr('disabled', '');
	}
	else {
		$('.eleChk').attr('disabled', 'disabled');
	}
}

function changedCategorySelection() {
	var categoryId = $('select#sel_category').val();
	var oleCategoryId = categoryMappingAry[categoryId]; 
	
	if (oleCategoryId != 0 && oleCategoryId != '') {
		var oleCategorySelId = 'category_' + $('input#EnrolEventID').val();
		$('select#' + oleCategorySelId).val(oleCategoryId);
	}
}

function houseSelection(val) {
	if(val == '0') {
		$('div#house').css('display', 'none');
		$('[name="houseIdAry[]"]').attr('checked', false);
	}
	else{
		$('div#house').css('display', 'block');
	}
}

function checkActivityNature(){
	selectedValue = document.form1.nature.value;
	if(selectedValue == 1){
		$('#SDAS_CategoryI').removeAttr('disabled').show();
		$('#SDAS_CategoryO').attr('disabled',true).hide();
		$('#SDAS_Category_SpanO').hide();
		$('#SDAS_Category_SpanI').show();
		$('#SDAS_AllowSync_O').removeAttr('checked').attr('disabled',true);		
		$('#SDAS_AllowSync_I').removeAttr('disabled');
	} else {
		$('#SDAS_CategoryI').attr('disabled',true).hide();
		$('#SDAS_CategoryO').removeAttr('disabled').show();
		$('#SDAS_Category_SpanI').hide();
		$('#SDAS_Category_SpanO').show();
		$('#SDAS_AllowSync_I').removeAttr('checked').attr('disabled',true);		
		$('#SDAS_AllowSync_O').removeAttr('disabled');
	}
}
<?php if($sys_custom['eEnrolment']['TWGHCYMA']){ ?>
function onChangeVolunteerService(onOff, divId)
{
	if(onOff == 1){
		$('#'+divId).show();
	}else{
		$('#'+divId).hide();
	}
}

function onChangeVolunteerServiceHour(inputTextObj)
{
	var str_val = $.trim(inputTextObj.value);
	var num_val = parseFloat(str_val);
	
	if(str_val != '' && (isNaN(num_val) || num_val < 0)){
		inputTextObj.value = '';
	}
}

function onClickedSchoolActivity(showIdAry, hideIdAry)
{
	for(var i=0;i<showIdAry.length;i++){
		$('#'+showIdAry[i]).show();
	}
	for(var i=0;i<hideIdAry.length;i++){
		$('#'+hideIdAry[i]).hide();
	}
}

function EnrolGroupChanged(obj)
{
	var sel_val = obj.options[obj.selectedIndex].value;
	
	if(sel_val == ''){
		$('#RowRelatedGroupID').show();
	}else{
		$('#RowRelatedGroupID').hide();
	}
}

function jointActvityChanged(sel_val)
{
	if(sel_val == '1'){
		$('#RowJointActOrganiser').show();
		$('#RowJointActOrganisedGroup').show();
	}else{
		$('#RowJointActOrganiser').hide();
		$('#RowJointActOrganisedGroup').hide();
	}
}

function jointActvityAwardedChanged(sel_val)
{
	if(sel_val == '1'){
		$('#RowJointActAwardType').show();
		$('#RowJointActChiAuthority').show();
		$('#RowJointActEngAuthority').show();
		$('#RowJointActChiAwardName').show();
		$('#RowJointActEngAwardName').show();
	}else{
		$('#RowJointActAwardType').hide();
		$('#RowJointActChiAuthority').hide();
		$('#RowJointActEngAuthority').hide();
		$('#RowJointActChiAwardName').hide();
		$('#RowJointActEngAwardName').hide();
	}
}
<?php } ?>
</SCRIPT>
<form name="form1" id="form1" action="event_setting_update.php" method="POST" enctype="multipart/form-data">
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	<table id="html_body_frame" class="form_table_v30" >
		<tr>
			<td colspan="2">
				<br>
				<?=$linterface->GET_NAVIGATION2($DefineActivityTitle)?>
			</td>
		</tr>
		<?php if(!($plugin['SDAS_module']['KISMode'] && $_SESSION['platform'] == "KIS")){?>
		<tr>
			<td class="field_title"><?= $eEnrollment['belong_to_group']?> <span class="tabletextrequire">*</span></td>
			<td><?= $GroupSel?></td>
		</tr>
		<?}?>
		<?php
		
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			$intranet_groups = $libenroll->Get_Intranet_Groups($EnrollEventArr['AcademicYearID']);
			$intranet_group_selection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($intranet_groups, ' id="RelatedGroupID" name="RelatedGroupID" ', $Lang['General']['NA'], $EnrollEventArr['RelatedGroupID']);
			$related_group_row = '<tr id="RowRelatedGroupID" '.($EnrollEventArr['EnrolGroupID']!='' && $EnrollEventArr['EnrolGroupID']>0?' style="display:none;"':'').'>
									<td class="field_title">'.$Lang['eEnrolment']['BelongToGroup'].' <span class="tabletextrequire">*</span></td>
									<td>'.$intranet_group_selection.'</td>
								</tr>';
			echo $related_group_row;
		}
		
		?>
		<tr>
			<td class="field_title"><?= $eEnrollment['add_activity']['act_name']?> <span class="tabletextrequire">*</span></td>
			<td><input type="text" id="act_title" name="act_title" value="<?=$EventTitle?>" class="textboxtext"></td>
		</tr>
		<tr>
			<td class="field_title"><?php echo $Lang['eEnrolment']['ActivityCode']?></td>
			<td>
				<input class="textboxnum" type="text" name="ActivityCode" id="ActivityCode" size="50" maxlength="20" value="<?=intranet_htmlspecialchars($activityCode)?>">
				<?php  if($plugin['SDAS_module']['KISMode']) echo '<span class="tabletextremark">('.$Lang['eEnrolment']['ActivityCodeTWGHsHint'].')</span>'?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_content']?></td>
			<td><?= $HTMLEditor?></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_category']?> <span class="tabletextrequire">*</span></td>
			<td>
			<!--<input type="text" id="act_category" name="act_category" value="<?= htmlspecialchars($EnrollEventArr[19])?>" class="textboxnum">-->
			<? 
				/*
					$lc->GET_EVENTINFO_CATEGORY_SEL($EnrollEventArr[19], 0, "document.getElementById('act_category').value = this.value; this.selectedIndex = 0", $button_select)
				*/
			?>
			<?php
			if($sys_custom['eEnrolment']['TWGHCYMA']){
				$CategoryList = $libenroll->GET_CATEGORY_LIST();
				$EventCategoryIdList = $libenroll->Get_Activity_Event_Categories($EnrolEventID);
				
				$CategorySelectionHtml = '';
				if(sizeof($CategoryList)>0){
					foreach($CategoryList as $CategoryRecord){
						$CategorySelectionHtml .= $linterface->Get_Checkbox("sel_category".$CategoryRecord['CategoryID'], "sel_category[]", $CategoryRecord['CategoryID'], in_array($CategoryRecord['CategoryID'],$EventCategoryIdList), '', $CategoryRecord['CategoryName'], '', '').'<br>';
					}
				}
				echo $CategorySelectionHtml;
			}else{
				echo $libenroll->GET_CATEGORY_SEL($EnrollEventArr['EventCategory'], 1, "changedCategorySelection();", $button_select,0,$categoryTypeID);
			}
			?>
			</td>
		</tr>
		<?php if($sys_custom['eEnrolment']['TWGHCYMA']){ ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eEnrolment']['VolunteerService']?> <span class="tabletextrequire">*</span></td>
			<td>
				<?=$linterface->Get_Radio_Button("IsVolunteerServiceYes", "IsVolunteerService", "1", $EnrollEventArr['IsVolunteerService']==1, "",$Lang['General']['Yes'], "onChangeVolunteerService(1,'VolunteerServiceDiv');",0).'&nbsp;'?>
				<?=$linterface->Get_Radio_Button("IsVolunteerServiceNo", "IsVolunteerService", "0", $EnrollEventArr['IsVolunteerService']!=1, "",$Lang['General']['No'], "onChangeVolunteerService(0,'VolunteerServiceDiv');",0)?>
				<div id="VolunteerServiceDiv" <?=$EnrollEventArr['IsVolunteerService']!=1?'style="display:none"':''?>>
					<?=$linterface->GET_TEXTBOX_NUMBER("VolunteerServiceHour", "VolunteerServiceHour", $EnrollEventArr['VolunteerServiceHour'], '', array('onchange'=>'onChangeVolunteerServiceHour(this);')).'&nbsp;'.$Lang['eEnrolment']['Hour']?>
				</div>
			</td>
		</tr>
		<?php } ?>


<?php if (!$sys_custom['project']['HKPF']): // ***** start not HKPF?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$sys_custom['eEnrolment']['TWGHCYMA']?$Lang['eEnrolment']['ActivityLocation']:$Lang['eEnrolment']['Nature']?> <span class="tabletextrequire">*</span></td>
			<td>
				<?php
					if($sys_custom['eEnrolment']['TWGHCYMA']){
						$SchoolActivityClickedEvent = array();
						$SchoolActivityClickedEvent[0] = ' onclick="onClickedSchoolActivity([\'ExternalLocationDiv\'],[\'SchoolLocationDiv\']);" ';
						$SchoolActivityClickedEvent[1] = ' onclick="onClickedSchoolActivity([\'SchoolLocationDiv\'],[\'ExternalLocationDiv\']);" ';
						$SchoolActivityClickedEvent[2] = ' onclick="onClickedSchoolActivity([\'ExternalLocationDiv\',\'SchoolLocationDiv\'],[]);" ';
					}
				if($plugin['SDAS_module']['KISMode']){
					include_once($PATH_WRT_ROOT.'includes/cust/student_data_analysis_system_kis/libSDAS.php');
					include_once($PATH_WRT_ROOT.'lang/kis/apps/lang_cees_'.$intranet_session_language.'.php');
				?>
					<input type="radio" id="nature1" name="nature" value="1" checked <?=$sys_custom['eEnrolment']['TWGHCYMA']?$SchoolActivityClickedEvent[1]:''?>><label for="nature1"> <?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool']?></label> 
					<input type="radio" id="nature0" name="nature" value="0" <? if ($EnrollEventArr['SchoolActivity'] == 0 && isset($EnrollEventArr['SchoolActivity'])) print "checked"; ?> <?=$sys_custom['eEnrolment']['TWGHCYMA']?$SchoolActivityClickedEvent[0]:''?>><label for="nature0"> <?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool']?></label>
				<?php }else{ ?>
					<input type="radio" id="nature1" name="nature" value="1" checked <?=$sys_custom['eEnrolment']['TWGHCYMA']?$SchoolActivityClickedEvent[1]:''?>><label for="nature1"> <?=$Lang['eEnrolment']['SchoolActivity']?></label> 
					<input type="radio" id="nature0" name="nature" value="0" <? if ($EnrollEventArr['SchoolActivity'] == 0 && isset($EnrollEventArr['SchoolActivity'])) print "checked"; ?> <?=$sys_custom['eEnrolment']['TWGHCYMA']?$SchoolActivityClickedEvent[0]:''?>><label for="nature0"> <?=$Lang['eEnrolment']['NonSchoolActivity']?></label>
					<input type="radio" id="nature2" name="nature" value="2" <? if ($EnrollEventArr['SchoolActivity'] == 2 && isset($EnrollEventArr['SchoolActivity'])) print "checked"; ?> <?=$sys_custom['eEnrolment']['TWGHCYMA']?$SchoolActivityClickedEvent[2]:''?>><label for="nature2"> <?=$Lang['eEnrolment']['MixedActivity']?></label>
				<?}?>
				<?php
					if($sys_custom['eEnrolment']['TWGHCYMA']){
						$LocationDiv = '<div id="SchoolLocationDiv"'.(($EnrollEventArr['SchoolActivity']=='' || $EnrollEventArr['SchoolActivity']=='1' || $EnrollEventArr['SchoolActivity']=='2')?'':'style="display:none"').'>';
							$LocationDiv .= $Lang['eEnrolment']['SchoolLocation'].': '. Get_String_Display($EnrollEventArr['ActivityInLocation']);
							$LocationDiv .= '<div class="tabletextremark">('.$Lang['eEnrolment']['SchoolLocationSettingInstruction'].')</div>';
						$LocationDiv.= '</div>';
						$LocationDiv.= '<div id="ExternalLocationDiv"'.(($EnrollEventArr['SchoolActivity']=='0' || $EnrollEventArr['SchoolActivity']=='2')?'':'style="display:none"').'>';
							$LocationDiv.= $Lang['eEnrolment']['ExternalLocation'].': '. $linterface->GET_TEXTBOX("ActivityExtLocation", "ActivityExtLocation", $EnrollEventArr['ActivityExtLocation'], '', array());
						$LocationDiv.= '</div>';
						echo $LocationDiv;
					}
				?>
			</td>
		</tr>
		<?php if($plugin['SDAS_module']['KISMode']){ 
			?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_Category']?> <span class="tabletextrequire">*</span></td>
			<td>
				<?php $libSDAS = new libSDAS();
					  echo $libSDAS->getECACategorySelect('name="SDAS_Category" id="SDAS_CategoryO"', $EnrollEventArr['SDAS_Category'],1);
					  echo $libSDAS->getECACategorySelect('name="SDAS_Category" id="SDAS_CategoryI" style="display: none;"', $EnrollEventArr['SDAS_Category'], 0);
				?>
				
				<!-- 
				(<?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetReport']?>: 
				<span id="SDAS_Category_SpanI"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool']?></span>
				<span id="SDAS_Category_SpanO" style="display: none;"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool']?></span>)
				-->
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_AllowSync']?></td>
			<td>
				<input type='checkbox' name='SDAS_AllowSync[]' id='SDAS_AllowSync_I' value='I' <?=(in_array('I',$EnrollEventArr['SDAS_AllowSync'])?'checked':'')?> />
				<label for='SDAS_AllowSync_I'><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'].'('.$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'].')'?></label>
				<input type='checkbox' name='SDAS_AllowSync[]' id='SDAS_AllowSync_O' value='O' <?=(in_array('O',$EnrollEventArr['SDAS_AllowSync'])?'checked':'')?>/>
				<label for='SDAS_AllowSync_O'><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'].'('.$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'].')'?></label>
				<input type='checkbox' name='SDAS_AllowSync[]' id='SDAS_AllowSync_M' value='M' <?=(in_array('M',$EnrollEventArr['SDAS_AllowSync'])?'checked':'')?>/>
				<label for='SDAS_AllowSync_M'><?=$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title']?></label>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ActivityTarget']?></td>
			<td>
				<input class="textboxnum" type="text" name="ActivityTarget" id="ActivityTarget" value="<?=$EnrollEventArr['ActivityTarget']?>">
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Guest']?></td>
			<td>
				<input class="textboxnum" type="text" name="Guest" value="<?=$EnrollEventArr['Guest']?>">
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParticipatedSchool']?></td>
			<td>
				<input class="textboxnum" type="text" name="ParticipatedSchool" value="<?=$EnrollEventArr['ParticipatedSchool']?>">
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TotalParticipant']?></td>
			<td>
				<input class="textboxnum" type="text" name="TotalParticipant" id="TotalParticipant" size="50" maxlength="20" value="<?=$EnrollEventArr['TotalParticipant']?>">
			</td>
		</tr>
		<?php }?>
		<?php if($sys_custom['eEnrolment']['TWGHCYMA']){ ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eEnrolment']['Nature']?> <span class="tabletextrequire">*</span></td>
			<td>
				<?php
					$ActivityNatureList = $libenroll->Get_Activity_Nature_List();
					$EventActivityNatureIdList = $libenroll->Get_Activity_Event_Natures($EnrolEventID);
					
					$CategorySelectionHtml = '';
					if(sizeof($ActivityNatureList)>0){
						foreach($ActivityNatureList as $CategoryRecord){
							$CategorySelectionHtml .= $linterface->Get_Checkbox("activity_nature".$CategoryRecord['NatureID'], "activity_nature[]", $CategoryRecord['NatureID'], in_array($CategoryRecord['NatureID'],$EventActivityNatureIdList), '', $CategoryRecord['Nature'], '', '').'<br>';
						}
					}
					echo $CategorySelectionHtml;
				?>
			</td>
		</tr>
		<?php } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_target']?> <span class="tabletextrequire">*</span></td>
			<td class="tabletext"><?= $ClassLvlChk?>
				
			</td>
		</tr>
		
		<?php  if(!($plugin['SDAS_module']['KISMode'] && $_SESSION['platform'] == "KIS")){ ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eEnrolment']['TargetHouse']?><span class="tabletextrequire">*</span></td>
			<td class="tabletext">
				<input type="radio" id="set_house_limit" name="set_house" value="0" onclick="houseSelection('0');" <?=$setDefaultCheck?> ><label for="set_house_limit"> <?= $eEnrollment['no_limit']?></label><br/>
				<input type="radio" id="set_house" name="set_house" onclick="houseSelection('1');" value="1" <?=$setHouseCheck?> >
				<label for="set_house">
					<?=$eEnrollment['Specify']?><br>
					<?=$houseChkbox?>
				</label>
			</td>
		</tr>
		
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_age']?> <span class="tabletextrequire">*</span></td>
			<td class="tabletext">
				<input type="radio" id="set_age_limit" name="set_age" value="0" checked><label for="set_age_limit"> <?= $eEnrollment['no_limit']?></label><br/>
				<input type="radio" id="set_age_range" name="set_age" value="1" <? if (($EnrollEventArr['UpperAge'] > 0)||($EnrollEventArr['LowerAge'] > 0)) print "checked"; ?>>
				<label for="set_age_range">
				<?= $LowerAgeSel?>
				&nbsp;-&nbsp;
				<?= $UpperAgeSel?>&nbsp;<?= $eEnrollment['add_activity']['age']?>
				</label>
			</td>
		</tr>

		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_gender']?> <span class="tabletextrequire">*</span></td>
			<td class="tabletext">
				<input type="radio" name="gender" id="gender_all" value="A" checked> <label for="gender_all"><?= $eEnrollment['all']?></label>
				<input type="radio" name="gender" id="gender_m" value="M" <? if ($EnrollEventArr['Gender'] == "M") print "checked"?>> <label for="gender_m"><?= $eEnrollment['male']?></label>
				<input type="radio" name="gender" id="gender_f" value="F" <? if ($EnrollEventArr['Gender'] == "F") print "checked"?>> <label for="gender_f"><?= $eEnrollment['female']?></label>
			</td>
		</tr>
		<?php }?>
		<?php if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){ ?>
			<input type="hidden" id="PaymentAmount" name="PaymentAmount" value="0" />
			<input type="hidden" id="isAmountTBC" name="isAmountTBC" value="1" />
		<?php }else{ ?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['PaymentAmount']?> <span class="tabletextrequire">*</span></td>
    			<td class="tabletext">
    				<input type="text" id="PaymentAmount" name="PaymentAmount" value="<?= (0+$EnrollEventArr['PaymentAmount'])?>" class="textboxnum" <?=$Amount_disabled?>>
    				&nbsp;&nbsp;
    				<?= $linterface->Get_Checkbox("isAmountTBC", "isAmountTBC", 1, $TBC_checked, $Class="", $eEnrollment['ToBeConfirmed'], $Onclick="js_ClickFeeConfirmed()"); ?>
    			</td>
    		</tr>
		<?php } ?>
		
		<?if($sys_custom['eEnrolment']['ActivityBudgetField']){?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['Settings']['BudgetAmount']?> <span class="tabletextrequire">*</span></td>
			<td class="tabletext">
				<input type="text" id="BudgetAmount" name="BudgetAmount" value="<?= (0+$EnrollEventArr['BudgetAmount'])?>" class="textboxnum">
			</td>
		</tr>
		<?}?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['attachment']?></td>
			<td class="tabletext" id="attachmentTD"></td>
			<!-- 
			<td class="tabletext">
				<? if ($EnrollEventArr['AttachmentLink1'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary = this.value.split('\\'); document.getElementById('attachname0').value=Ary[Ary.length-1];"><input type="hidden" name="attachname0" id="attachname0">
				<? } else {?>
				<a href="<?= "/file/".$EnrollEventArr['AttachmentLink1']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink1'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='1'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($filecount == 0) { ?> <a href="#more_file_a" onClick="javascript:document.getElementById('more_file').style.display = 'block';document.getElementById('tohide').style.display = 'none'"><span id="tohide" class="tablelink"><?= $eEnrollment['add_activity']['more_attachment']?></span></a> <? } ?>
				<div id="more_file" style="display : <? ($filecount > 0) ? print "block": print "none"; ?>"><a name="more_file_a"></a>
				<? if ($EnrollEventArr['AttachmentLink2'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary1 = this.value.split('\\'); document.getElementById('attachname1').value=Ary1[Ary1.length-1];"><input type="hidden" name="attachname1" id="attachname1">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink2']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink2'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='2'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink3'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary2 = this.value.split('\\'); document.getElementById('attachname2').value=Ary2[Ary2.length-1];"><input type="hidden" name="attachname2" id="attachname2">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink3']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink3'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='3'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink4'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary3 = this.value.split('\\'); document.getElementById('attachname3').value=Ary3[Ary3.length-1];"><input type="hidden" name="attachname3" id="attachname3">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink4']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink4'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='4'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink5'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary4 = this.value.split('\\'); document.getElementById('attachname4').value=Ary4[Ary4.length-1];"><input type="hidden" name="attachname4" id="attachname4">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink5']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink5'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='5'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink6'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary5 = this.value.split('\\'); document.getElementById('attachname5').value=Ary5[Ary5.length-1];"><input type="hidden" name="attachname5" id="attachname5">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink6']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink6'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='6'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink7'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary6 = this.value.split('\\'); document.getElementById('attachname6').value=Ary6[Ary6.length-1];"><input type="hidden" name="attachname6" id="attachname6">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink7']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink7'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='7'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink8'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary7 = this.value.split('\\'); document.getElementById('attachname7').value=Ary7[Ary7.length-1];"><input type="hidden" name="attachname7" id="attachname7">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink8']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink8'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='8'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink9'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary8 = this.value.split('\\'); document.getElementById('attachname8').value=Ary8[Ary8.length-1];"><input type="hidden" name="attachname8" id="attachname8">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink9']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink9'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='9'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				<? if ($EnrollEventArr['AttachmentLink10'] == "")  { ?>
					<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary9 = this.value.split('\\'); document.getElementById('attachname9').value=Ary9[Ary9.length-1];"><input type="hidden" name="attachname9" id="attachname9">
				<? } else {?>
					<a href="<?= "/file/".$EnrollEventArr['AttachmentLink10']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollEventArr['AttachmentLink10'])?></a>&nbsp;(<a href="<?=($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: document.form1.submit();" ?>" onClick="document.getElementById('FileToDel').value='10'; document.form1.action='event_file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
				<? } ?>
				</div>
			</td> -->
		</tr>
		
		<?php 
		
		$locationField = '';
		if($libenroll->enableActivityLocationField()){
			$locationField .= '<tr>';
			$locationField .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['Location'].'</td>';
			$locationField .= '<td class="tabletext">';
			$locationField .= $linterface->GET_TEXTBOX('location', 'location', $EnrollEventArr['Location'], 'class="textboxtext" type="text" size="50"  ');
			$locationField .= '</td>';
			$locationField .= '</tr>';
			echo $locationField;
		}
		
		$gatheringLocationField = '';
		if($libenroll->enableActivityGatheringLocationField()){
			$gatheringLocationField .= '<tr>';
			$gatheringLocationField .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['GatheringLocation'].'</td>';
			$gatheringLocationField .= '<td class="tabletext">';
			$gatheringLocationField .= $linterface->GET_TEXTBOX('gatheringLocation', 'gatheringLocation', $EnrollEventArr['GatheringLocation'] , 'class="textboxtext" type="text" size="50"');
			$gatheringLocationField .= '</td>';
			$gatheringLocationField .= '</tr>';
			echo $gatheringLocationField;
		}
		
		$dismissLocationField = '';
		if($libenroll->enableActivityDismissLocationField()){
			$dismissLocationField .= '<tr>';
			$dismissLocationField .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['DismissLocation'].'</td>';
			$dismissLocationField .= '<td class="tabletext">';
			$dismissLocationField .= $linterface->GET_TEXTBOX('dismissLocation', 'dismissLocation', $EnrollEventArr['DismissLocation'] , 'class="textboxtext" type="text" size="50"');
			$dismissLocationField .= '</td>';
			$dismissLocationField .= '</tr>';
			echo $dismissLocationField;
		}
			
		$transportationField = '';
		if($libenroll->enableActivityTransportationField()){
			$transportationField .= '<tr>';
			$transportationField .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['Transportation'].'</td>';
			$transportationField .= '<td class="tabletext">';
			$transportationField .= $linterface->GET_TEXTBOX('transportation', 'transportation', $EnrollEventArr['Transport'] , 'class="textboxtext" type="text" size="50" ');
			$transportationField .= '</td>';
			$transportationField .= '</tr>';
			echo $transportationField;
		}
		
		$organizerField = '';
		if($libenroll->enableActivityOrganizerField()){
			$organizerField .= '<tr>';
			$organizerField .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['Organizer'].'</td>';
			$organizerField .= '<td class="tabletext">';
			$organizerField .= $linterface->GET_TEXTBOX('organizer', 'organizer', $EnrollEventArr['Organizer'], 'class="textboxtext" type="text" size="50" ');
			$organizerField .= '</td>';
			$organizerField .= '</tr>';
			echo $organizerField;
		}
		
		$rewardOrCertField = '';
		if($libenroll->enableActivityRewardOrCertificationField()){
			$rewardOrCertField .= '<tr>';
			$rewardOrCertField .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['RewardOrCertification'].'</td>';
			$rewardOrCertField .= '<td class="tabletext">';
			$rewardOrCertField .= $linterface->GET_TEXTBOX('rewardOrCert', 'rewardOrCert', $EnrollEventArr['RewardOrCert'] , 'class="textboxtext" type="text" size="50"');
			$rewardOrCertField .= '</td>';
			$rewardOrCertField .= '</tr>';
			echo $rewardOrCertField;
		}
		
		if($sys_custom['eEnrolment']['TWGHCYMA']){
			$jointActivityFields = '';
			$jointActivityFields .= '<tr>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['JointActivity'].'</td>';
				$jointActivityFields .= '<td class="tabletext">';
					$jointActivityFields .= $linterface->Get_Radio_Button("JointActY", "JointAct", "1", $EnrollEventArr['JointAct']=='1', "", $Lang['General']['Yes'], "jointActvityChanged(this.value);", 0);
					$jointActivityFields .= $linterface->Get_Radio_Button("JointActN", "JointAct", "0", $EnrollEventArr['JointAct']!='1', "", $Lang['General']['No'], "jointActvityChanged(this.value);", 0);
				$jointActivityFields .= '</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr id="RowJointActOrganiser" '.($EnrollEventArr['JointAct']!='1'?'style="display:none;"':'').'>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['Organiser'].'</td>';
				$jointActivityFields .= '<td>'.$linterface->GET_TEXTBOX("JointActOrganiser", "JointActOrganiser", $EnrollEventArr['JointActOrganiser'], '', array('size'=>"80")).'</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr id="RowJointActOrganisedGroup" '.($EnrollEventArr['JointAct']!='1'?'style="display:none;"':'').'>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['Partner'].'</td>';
				$jointActivityFields .= '<td>'.$linterface->GET_TEXTBOX("JointActOrganisedGroup", "JointActOrganisedGroup", $EnrollEventArr['JointActOrganisedGroup'], '', array('size'=>"80")).'</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['AwardedInActivity'].'</td>';
				$jointActivityFields .= '<td class="tabletext">';
					$jointActivityFields .= $linterface->Get_Radio_Button("JointActAwardedY", "JointActAwarded", "1", $EnrollEventArr['JointActAwarded']=='1', "", $Lang['General']['Yes'], "jointActvityAwardedChanged(this.value);", 0);
					$jointActivityFields .= $linterface->Get_Radio_Button("JointActAwardedN", "JointActAwarded", "0", $EnrollEventArr['JointActAwarded']!='1', "", $Lang['General']['No'], "jointActvityAwardedChanged(this.value);", 0);
				$jointActivityFields .= '</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr id="RowJointActAwardType" '.($EnrollEventArr['JointActAwarded']!='1'?'style="display:none;"':'').'>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['AwardType'].'</td>';
				$jointActivityFields .= '<td>'.$linterface->GET_TEXTBOX("JointActAwardType", "JointActAwardType", $EnrollEventArr['JointActAwardType'], '', array('size'=>"80")).'</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr id="RowJointActChiAuthority" '.($EnrollEventArr['JointActAwarded']!='1'?'style="display:none;"':'').'>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAuthorityName'].'</td>';
				$jointActivityFields .= '<td>'.$linterface->GET_TEXTBOX("JointActChiAuthority", "JointActChiAuthority", $EnrollEventArr['JointActChiAuthority'], '', array('size'=>"80")).'</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr id="RowJointActEngAuthority" '.($EnrollEventArr['JointActAwarded']!='1'?'style="display:none;"':'').'>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAuthorityName'].'</td>';
				$jointActivityFields .= '<td>'.$linterface->GET_TEXTBOX("JointActEngAuthority", "JointActEngAuthority", $EnrollEventArr['JointActEngAuthority'], '', array('size'=>"80")).'</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr id="RowJointActChiAwardName" '.($EnrollEventArr['JointActAwarded']!='1'?'style="display:none;"':'').'>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAwardName'].'</td>';
				$jointActivityFields .= '<td>'.$linterface->GET_TEXTBOX("JointActChiAwardName", "JointActChiAwardName", $EnrollEventArr['JointActChiAwardName'], '', array('size'=>"80")).'</td>';
			$jointActivityFields .= '</tr>';
			
			$jointActivityFields .= '<tr id="RowJointActEngAwardName" '.($EnrollEventArr['JointActAwarded']!='1'?'style="display:none;"':'').'>';
				$jointActivityFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAwardName'].'</td>';
				$jointActivityFields .= '<td>'.$linterface->GET_TEXTBOX("JointActEngAwardName", "JointActEngAwardName", $EnrollEventArr['JointActEngAwardName'], '', array('size'=>"80")).'</td>';
			$jointActivityFields .= '</tr>';
			
			echo $jointActivityFields;
		}
		
		if($sys_custom['eEnrolment']['ActivityIncludeInReports']){
			$activityIncludeInReportsFields = '';
			$activityIncludeInReportsFields .= '<tr>';
				$activityIncludeInReportsFields .= '<td valign="top" nowrap="nowrap" class="field_title">'.$Lang['eEnrolment']['IncludeInReports'].'</td>';
				$activityIncludeInReportsFields .= '<td class="tabletext">';
					$activityIncludeInReportsFields .= $linterface->Get_Radio_Button("IncludeInReportsY", "IncludeInReports", "1", ((trim($EnrolEventID)=='') || ($EnrollEventArr['IncludeInReport']=='1')), "", $Lang['General']['Yes'], "", 0);
					$activityIncludeInReportsFields .= $linterface->Get_Radio_Button("IncludeInReportsN", "IncludeInReports", "0", ((trim($EnrolEventID)!='') && ($EnrollEventArr['IncludeInReport']!='1')), "", $Lang['General']['No'], "", 0);
				$activityIncludeInReportsFields .= '</td>';
			$activityIncludeInReportsFields .= '</tr>';
			
			echo $activityIncludeInReportsFields;
		}
		if($sys_custom['eEnrolment']['ReplySlip']){
			echo $replyFormHTML;
		}
		?>
		
		<?php if($libenroll->enableActivitySurveyFormRight()){
			$qStr = $qStr=="" ? $EnrollEventArr['Question'] : $qStr;
			$preAllFieldsReq = $ar=="" ? $EnrollEventArr['AllFieldsReq'] : $ar;
		?>
			<!-- // 2012-12-04	(temp) customization for survey form --> 			
			<tr>
				<td valign="top" nowrap="nowrap" class="field_title"><?=$Lang['eSurvey']['SurveyForm']?></td>
				<td>
					<?=$linterface->GET_BTN($Lang['Btn']['Edit'], "button","newWindow('editform.php?AcademicYearID=$AcademicYearID',1)");?>
					<?=$linterface->GET_BTN($Lang['eSurvey']['Preview'], "button","newWindow('preview.php',1)");?>
					<br />
					<input name='DisplayQuestionNumber' type="checkbox" value="1" id="DisplayQuestionNumber" <?=($EnrollEventArr['DisplayQuestionNumber']==1?"CHECKED":"");?>> <label for="DisplayQuestionNumber"><?=$Lang['eSurvey']['DisplayQuestionNumber']?></label>
					<br />
					<input name="AllFieldsReq" type="checkbox" value="1" id="AllFieldsReq" <?=($preAllFieldsReq==1?"CHECKED":"");?>> <label for="AllFieldsReq"><?=$i_Survey_AllRequire2Fill?></label>
					<input type="hidden" name="qStr" value="<?=str_replace("___","#",str_replace("_AND_","&",$qStr))?>">
					<input type="hidden" name="aStr" value="">				
				</td>
			</tr>
			<!-- // 2012-12-04	End of (temp) customization for survey form	--> 
		<?}?> 
					
		<tr>
			<td colspan="2">
				<br>
				<?=$linterface->GET_NAVIGATION2($AssignStaffTitle)?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_pic']?> <span class="tabletextrequire">*</span></td>
			<td>
				<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><?= $PICSel?></td>
					<td valign="bottom">
					&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/enrolment_pic_helper.php?fieldname=pic[]&ppl_type=pic&permitted_type=1,2', 9)")?><br />
					&nbsp;<?=$button_remove_html?>
					</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['act_assistant']?></td>
			<td>
				<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td><?= $HELPERSel?></td>
					<td valign="bottom">
					&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('/home/common_choose/enrolment_pic_helper.php?fieldname=helper[]&ppl_type=helper&permitted_type=1,2', 9)")?><br />
					&nbsp;<?=$button_remove_html_helper?>
					</td>
				</tr>
				</table>
			</td>
		</tr>	
		<?php  if(!($plugin['SDAS_module']['KISMode'] && $_SESSION['platform'] == "KIS")){ ?>
		<?php if(!isset($_SESSION['ncs_role']) || $_SESSION['ncs_role']==""){ ?>
    		<tr>
    			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['app_method']?></td>
    			<td><?= $tiebreakerSelection?></td>
    		</tr>
		<?php } ?>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['participant_quota']?> <span class="tabletextrequire">*</span></td>
			<td><input type="text" id="Quota" name="Quota" value="<?= $ActivityQuota ?>" class="textboxnum"></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['app_period']?> <span class="tabletextrequire">*</span></td>
			<td class="tabletext">
			
				<?
				
					if ($EnrollEventArr['ApplyStartTime'] == "0000-00-00 00:00:00" ||empty($EnrollEventArr['ApplyStartTime'])) $EnrollEventArr['ApplyStartTime'] = "{$AppStart} {$AppStartHour}:{$AppStartMin}:00";
					if ($EnrollEventArr['ApplyEndTime'] == "0000-00-00 00:00:00" ||empty($EnrollEventArr['ApplyEndTime'])) $EnrollEventArr['ApplyEndTime'] = "{$AppEnd} {$AppEndHour}:{$AppEndMin}:00";
				?>
				<?=$linterface->GET_DATE_PICKER("ApplyStartTime", date("Y-m-d", strtotime($EnrollEventArr['ApplyStartTime'])))?>
				<!-- <input type="text" id="ApplyStartTime" name="ApplyStartTime" value="<?= date("Y-m-d", strtotime($EnrollEventArr['ApplyStartTime']))?>" class="textboxnum"> <?= $linterface->GET_CALENDAR("form1", "ApplyStartTime")?> -->
				<?= getTimeSel("ApplyStart", date("H", strtotime($EnrollEventArr['ApplyStartTime'])), date("i", strtotime($EnrollEventArr['ApplyStartTime']))) ?> 
				&nbsp;<?= $eEnrollment['to']?>&nbsp;
				<?=$linterface->GET_DATE_PICKER("ApplyEndTime", date("Y-m-d", strtotime($EnrollEventArr['ApplyEndTime'])))?>
				<!-- <input type="text" id="ApplyEndTime" name="ApplyEndTime" value="<?= date("Y-m-d", strtotime($EnrollEventArr['ApplyEndTime']))?>" class="textboxnum"> <?= $linterface->GET_CALENDAR("form1", "ApplyEndTime")?> -->
				<?= getTimeSel("ApplyEnd", date("H", strtotime($EnrollEventArr['ApplyEndTime'])), date("i", strtotime($EnrollEventArr['ApplyEndTime']))) ?> 
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<br>
				<?=$linterface->GET_NAVIGATION2($SetRightTitle)?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['add_activity']['apply_ppl_type']?> <span class="tabletextrequire">*</span></td>
			<td class="tabletext">
				<input type="radio" name="ApplyUserType" id="student" value="S" checked> <label for="student"><?= $eEnrollment['add_activity']['student_enroll']?></label>
				<input type="radio" name="ApplyUserType" id="parent" value="P" <? if ($EnrollEventArr['ApplyUserType'] == "P" || $_SESSION['platform'] == "KIS") print "checked"; ?> > <label for="parent"><?= $eEnrollment['add_activity']['parent_enroll']?></label><br/>
			</td>
		</tr>
		<?php }?>
		<?
		if((isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!="") || $_SESSION['platform'] == "KIS"){
		    // noop
		}else if($plugin['iPortfolio']) {
			
			include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
			$libenroll_ui = new libclubsenrol_ui();
			echo $libenroll_ui->Display_Enrolment_Default_OLE_Setting('activity', array($EnrolEventID));	
		}
		?>
		<?php 
		  if($_SESSION['platform'] != "KIS"){
		?>
		<tr>
			<td colspan="2">
				<br>
				<?=$linterface->GET_NAVIGATION2($Step3)?>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['WebSAMSSTACode']?></td>
			<td class="tabletext">
				<input type="text" id="WebSAMSSTACode" name="WebSAMSSTACode" value="<?= $EnrollEventArr['WebSAMSCode']?>" class="textboxnum">
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="field_title"><?= $eEnrollment['WebSAMSSTAType']?></td>
			<td class="tabletext">
			<?php 
				$ECheck = '';
				$SCheck = '';
				$ICheck = '';
				$Check = '';
				if($EnrollEventArr['WebSAMSSTAType'] == 'E'){
					$ECheck = 'selected';
				}else if($EnrollEventArr['WebSAMSSTAType'] == 'S' ){
					$SCheck = 'selected';
				}else if($EnrollEventArr['WebSAMSSTAType'] == 'I'){
					$ICheck = 'selected';
				}else{
					$Check = 'selected';
				}
			
			?>
				<select id="WebSAMSSTAType" name="WebSAMSSTAType">
				
					<option name="STAType" <?=$Check?> value = "" ><?=Get_Selection_First_Title($Lang['General']['PleaseSelect'])?></option>
					<option name="STAType" <?=$ECheck?> value = "E" ><?= $eEnrollment['WebSAMSSTA']['ECA']?></option>
					<option name="STAType" <?=$SCheck?> value = "S" ><?= $eEnrollment['WebSAMSSTA']['ServiceDuty']?></option>
					<option name="STAType" <?=$ICheck?> value = "I" ><?= $eEnrollment['WebSAMSSTA']['InterSchoolActivities']?></option>
				</select>
			</td>
		</tr>
		<?php } ?>
<?php endif;    // ***** end not HKPF?>		
		
		
		   <tr>
     
		<td colspan="2"><br><?= $linterface->GET_NAVIGATION2($Step4) ?><br/>
		</td>
	</tr>

     <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['CreateUserName']?></td>
		<td> 
		<?php echo $InputBy?>
     	</td>
     </tr>
      <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['eEnrolment']['CreateUserDate']?></td>
		<td> 
			<?php echo $DateInput?>
     	</td>
     </tr>    
          <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['LastModifiedBy']?></td>
		<td> 
		<?php echo $ModifiedBy?>
     	</td>
     </tr>
      <tr>
		<td valign="top" nowrap="nowrap" class="field_title"><?= $Lang['General']['LastModified']?></td>
		<td> 
			<?php echo $DateModified?>
     	</td>
     </tr>   
	</table>

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">

<div style="padding-top: 5px">
<!--
<?= $linterface->GET_ACTION_BTN($button_save_continue, "button", "javascript: generalFormSubmitCheck(document.form1);","submitform")?>&nbsp;
-->
<?if(!$CannotAddUpdate) {?>
<? if($libenroll->AllowToEditPreviousYearData) {?>
	<?= $linterface->GET_ACTION_BTN($button_save, "button", ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript: FormSubmitCheck(document.form1);","submitform")?>&nbsp;
<?}?>
<?// if ($EnrolEventID > 0) print $linterface->GET_ACTION_BTN($eEnrollment['skip'], "button", "self.location='event_new1b.php?EnrolEventID=$EnrolEventID'")?>&nbsp;
	<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='event.php?AcademicYearID=$AcademicYearID".$button_cancel_para."'")?>
<? } ?>
</div>
</td></tr>
  

</table>


<input type="hidden" name="flag" value="0" />
<input type="hidden" name="EnrolEventID" id="EnrolEventID" value="<?= $EnrolEventID?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />
<input type="hidden" name="isNew" id="isNew" value="<?= $isNew?>" />
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?= $AcademicYearID?>" />
<?=$picView_HiddenField?>
<?php if(!($plugin['SDAS_module']['KISMode'] && $_SESSION['platform'] == "KIS")){?>
<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="0" />
<input type="hidden" name="set_house" id="set_house_limit" value="0" />
<input type="hidden" name="set_age" id="set_age_limit" value="0" />
<input type="hidden" name="gender" id="gender" value="A" />
<input type="hidden" name="tiebreak" id="tiebreak" value="0" />
<input type="hidden" name="ApplyUserType" id="ApplyUserType" value="S" />
<input type="hidden" name="ApplyStartTime" id="ApplyStartTime" value="<?= date('y-m-d')?>" />
<input type="hidden" name="ApplyStartHour" id="ApplyStartHour" value="00" />
<input type="hidden" name="ApplyStartMin" id="ApplyStartMin" value="00" />
<input type="hidden" name="ApplyEndTime" id="ApplyEndTime" value="<?= date('y-m-d')?>" />
<input type="hidden" name="ApplyEndHour" id="ApplyEndHour" value="00" />
<input type="hidden" name="ApplyEndMin" id="ApplyEndMin" value="00" />
<?php }?>
<?=$kisMode_HiddenField?>
<?php if($isNew){ ?>
	<input type="hidden" name="DirectToMeetingActivity" id="DirectToMeetingActivity"/>
<?php }?>

</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>