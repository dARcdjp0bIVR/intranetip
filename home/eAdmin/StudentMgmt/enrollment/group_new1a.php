<?php
# using: 

######### Change Log Start ################
#
#	Date:	2010-03-09	Marcus
#			replace content textarea by html editor
#############################################


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	if (isset($EnrolGroupID))
	{
		$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
		//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
	}

	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club")))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	
	$cancel_btn_para = "Semester=$Semester";
	if (($LibUser->isStudent())||($LibUser->isParent())) 
	{
		if ($libenroll->IS_ENROL_PIC($UserID, $EnrolGroupID, 'Club'))
		{
			$CurrentPageArr['eEnrolment'] = 0;
			$CurrentPageArr['eServiceeEnrolment'] = 1;
		
			$cancel_btn_para .= "&pic_view=1";
		}
	}
		
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$libenroll = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
		include_once($PATH_WRT_ROOT."includes/libgroup.php");
		
        $libgroup = new libgroup($GroupID);
        $linterface = new interface_html();
        //$TAGS_OBJ[] = array($eEnrollmentMenu['add_act_event'], "", 1);
        # tags 
	    $tab_type = "club";
	    $current_tab = 1;
	    include_once("management_tabs.php");
	    # navigation
        $PAGE_NAVIGATION[] = array($eEnrollment['enrolment_settings'], "");

        $STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_1'], 1);
        $STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_1b'], 0);
		$STEPS_OBJ[] = array($eEnrollment['add_club_activity']['step_2'], 0);
		
		$tiebreakerSelection = "<SELECT name=tiebreak>\n";
		$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
		$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
		$tiebreakerSelection.= "</SELECT>\n";
		
        $linterface->LAYOUT_START();
/*     
if (isset($gid))
{
	$GroupID = $gid[0];
}
*/
$EnrollGroupArr = $libenroll->GET_GROUPINFO($EnrolGroupID);

# Last selection - PIC
###############################################################################################
$picArr = $libenroll->GET_GROUPSTAFF($EnrollGroupArr['EnrolGroupID'], "PIC");
$PICSel = "<select name='pic[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($picArr); $i++) {
	/*
	$username_field = getNameFieldByLang("a.");
	$Sql = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) FROM INTRANET_USER AS a WHERE a.UserID = '".$picArr[$i][0]."'";
	$TempArr = $libenroll->returnArray($Sql);
	debug_r($TempArr);
	*/
	$PICSel .= "<option value=\"".$picArr[$i][0]."\">".$LibUser->getNameWithClassNumber($picArr[$i][0])."</option>";
}
$PICSel .= "</select>";

//if (sizeof($picArr)>0)
//{
	$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");
//}
###############################################################################################


# Last selection - helper
###############################################################################################
$helperArr = $libenroll->GET_GROUPSTAFF($EnrollGroupArr['EnrolGroupID'], "HELPER");
$HELPERSel = "<select name='helper[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($helperArr); $i++) {
	/*
	$username_field = getNameFieldByLang("a.");
	$Sql = "SELECT a.UserID, CONCAT($username_field, IF (a.ClassNumber IS NULL OR a.ClassNumber = '', '', CONCAT(' (',a.ClassName,'-',a.ClassNumber,')') ) ) FROM INTRANET_USER AS a WHERE a.UserID = '".$picArr[$i][0]."'";
	$TempArr = $libenroll->returnArray($Sql);
	debug_r($TempArr);
	*/
	$HELPERSel .= "<option value=\"".$helperArr[$i][0]."\">".$LibUser->getNameWithClassNumber($helperArr[$i][0])."</option>";
}
$HELPERSel .= "</select>";

//if (sizeof($helperArr)>0)
//{
	$button_remove_html_helper = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['helper[]'])");
//}
###############################################################################################

$libenrolllass = new libclass();
$ClassLvlArr = $libenrolllass->getLevelArray();
$GroupArr = $libenroll->GET_GROUPCLASSLEVEL($EnrollGroupArr['EnrolGroupID']);
$ClassLvlChk = "<input type=\"checkbox\" id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.form1, 'classlvl[]') : setChecked(0, document.form1, 'classlvl[]');\"><label for=\"classlvlall\">".$eEnrollment['all']."</label>";
$ClassLvlChk .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = "";
	for ($j = 0; $j < sizeof($GroupArr); $j++) {	
		if (($ClassLvlArr[$i][0] == $GroupArr[$j][0])&&(in_array($GroupArr[$j], $ClassLvlArr))) {	
			 $checked = " checked ";
			 break;
		}
	}
	
	if (($i % 10) == 0) $ClassLvlChk .= "<tr>";
	$ClassLvlChk .= "<td class=\"tabletext\"><input type=\"checkbox\" $checked id=\"classlvl{$i}\" name=\"classlvl[]\" value=\"".$ClassLvlArr[$i][0]."\"><label for=\"classlvl{$i}\">".$ClassLvlArr[$i][1]."</label></td>";
	if (($i % 10) == 9) $ClassLvlChk .= "</tr>";
}
$ClassLvlChk .= "</table>";

if ($EnrollGroupArr['LowerAge'] != 0) $LowerAge = $EnrollGroupArr['LowerAge'];
$LowerAgeSel = "<select name=\"LowerAge\" id=\"LowerAge\" onclick=\"jsSelectAgeRange();\">";
for ($i = 11; $i < 21; $i++) {
	$LowerAgeSel .= "<option value=\"$i\"";
	if ($LowerAge == $i) $LowerAgeSel .= " selected ";
	$LowerAgeSel .= ">".$i."</option>";
}
$LowerAgeSel .= "</select>";

if ($EnrollGroupArr['UpperAge'] != 0) $UpperAge = $EnrollGroupArr['UpperAge'];
$UpperAgeSel = "<select name=\"UpperAge\" id=\"UpperAge\" onclick=\"jsSelectAgeRange();\">";
for ($i = 11; $i < 21; $i++) {
	$UpperAgeSel .= "<option value=\"$i\"";
	if (($UpperAge == $i)||(($i == 20)&&(!$UpperAgeSelected))) {
		$UpperAgeSelected = true;
		$UpperAgeSel .= " selected ";
	}
	$UpperAgeSel .= ">".$i."</option>";
}
$UpperAgeSel .= "</select>";


$filecount = 0;
for ($i = 10; $i < 15; $i++) {
	if ($EnrollGroupArr[$i] != "") $filecount++;
}

## Fee - check if to be confirmed
if ($EnrollGroupArr['isAmountTBC'] == 1)
{
	$TBC_checked = true;
	$Amount_disabled = 'disabled';
}
else
{
	$TBC_checked = false;
	$Amount_disabled = '';
}

# Get HTML editor
$msg_box_width = 500;
$msg_box_height = 200;
$obj_name = "Discription";
$editor_width = $msg_box_width;
$editor_height = $msg_box_height;

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$oFCKeditor = new FCKeditor($obj_name,$editor_width,$editor_height);
$oFCKeditor->Value =  $EnrollGroupArr['Description'];
$HTMLEditor = $oFCKeditor->Create2();
?>
<script language="javascript">
function getEditorValue(instanceName) {
// Get the editor instance that we want to interact with.
var oEditor = FCKeditorAPI.GetInstance( instanceName ) ;

// Get the editor contents as XHTML.
return oEditor.GetXHTML( true ) ; // "true" means you want it formatted.
}

function FormSubmitCheck(obj)
{
	
	if(getEditorValue("Discription")=='')
	{
		alert("<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_content']; ?>");
		return false;
	}
	if (obj.sel_category.value == '') {
		alert("<?php echo $i_alert_pleaseselect.$eEnrollment['add_activity']['act_category']; ?>.");
		return false;
	}	
	
	if ($('#LowerAge').val() > $('#UpperAge').val())
	{
		alert('<?=$Lang['eEnrolment']['Warning']['AgeRangeInvalid']?>');
		return false;
	}
	
	if (!check_positive_int(obj.PaymentAmount,"<?=$eEnrollment['js_fee_positive']?>"))
		return false;
		
	if(!check_positive_int(obj.Quota,"<?=$ec_warning['please_enter_pos_integer']?> (<?=$eEnrollment['js_zero_means_no_limit']?>)","",0))
	{
    	return false;	
	}

	if(!check_text(obj.PaymentAmount, "<?php echo $i_alert_pleasefillin.$eEnrollment['PaymentAmount']; ?>.")) return false;
	
	// Target Group Checking - at least select one target group
	var i;
	var allNotChecked = true;
	for (i=0; i< <?=sizeof($ClassLvlArr)?>; i++)
	{
		var classLevelChkBox = document.getElementById('classlvl'+i);
		if (classLevelChkBox.checked)
		{
			allNotChecked = false;
		}
	}
	if (allNotChecked)
	{
		alert('<?=$eEnrollment['js_target_group_alert']?>');
		return false;	
	}
	
	/*
	if (confirm("<?= $eEnrollment['js_del_warning']?>")) {
		obj.submit();
	} else {
		return false;
	}
	*/
	
	// enable back the payment amount so that the value can be saved
	document.getElementById('PaymentAmount').disabled = false;
	
	obj.submit();
	
}

function jsSelectAgeRange()
{
	$('#set_age_range').attr('checked', true);
}

function js_ClickFeeConfirmed()
{
	if (document.getElementById('isAmountTBC').checked == false)
	{
		document.getElementById('PaymentAmount').disabled = false;
	}
	else
	{
		document.getElementById('PaymentAmount').disabled = true;
	}
}
</SCRIPT>
<form name="form1" action="group_new1a_update.php" method="POST" enctype="multipart/form-data">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr><td class="tabletext" align="center">
<!-- <?= $eEnrollment['del_warning'] ?> --><br/><br/>
</td></tr>
<tr><td>
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_content']?> <span class="tabletextrequire">*</span></td>
		<td><?=$HTMLEditor?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_category']?> <span class="tabletextrequire">*</span></td>
		<td>
		<?= $libenroll->GET_CATEGORY_SEL($EnrollGroupArr['GroupCategory'], 1, "", $button_select)?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_target']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext"><?= $ClassLvlChk?>
			
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_age']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" id="set_age_limit" name="set_age" value="0" checked><label for="set_age_limit"> <?= $eEnrollment['no_limit']?></label><br/>
			<input type="radio" id="set_age_range" name="set_age" value="1" <? if (($EnrollGroupArr['UpperAge'] > 0)||($EnrollGroupArr['LowerAge'] > 0)) print "checked"; ?>>
			<?= $LowerAgeSel?>
			<label for="set_age_range">
				&nbsp;-&nbsp;
			</label>
			<?= $UpperAgeSel?>
			<label for="set_age_range">
				&nbsp;<?= $eEnrollment['add_activity']['age']?>
			</label>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_gender']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="radio" name="gender" id="gender_all" value="A" checked> <label for="gender_all"><?= $eEnrollment['all']?></label>
			<input type="radio" name="gender" id="gender_m" value="M" <? if ($EnrollGroupArr['Gender'] == "M") print "checked"?>> <label for="gender_m"><?= $eEnrollment['male']?></label>
			<input type="radio" name="gender" id="gender_f" value="F" <? if ($EnrollGroupArr['Gender'] == "F") print "checked"?>> <label for="gender_f"><?= $eEnrollment['female']?></label>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['PaymentAmount']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="text" id="PaymentAmount" name="PaymentAmount" value="<?= (0+$EnrollGroupArr['PaymentAmount'])?>" class="textboxnum" <?=$Amount_disabled?>>
			&nbsp;&nbsp;
			<?= $linterface->Get_Checkbox("isAmountTBC", "isAmountTBC", 1, $TBC_checked, $Class="", $eEnrollment['ToBeConfirmed'], $Onclick="js_ClickFeeConfirmed()"); ?>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['club_quota']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="text" id="Quota" name="Quota" value="<?= (0+$EnrollGroupArr['Quota'])?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['MinimumMemberQuota']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
			<input type="text" id="MinQuota" name="MinQuota" value="<?= (0+$EnrollGroupArr['MinQuota'])?>" class="textboxnum">
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['attachment']?></td>
		<td class="tabletext">
		<? if ($EnrollGroupArr['AttachmentLink1'] == "")  { ?>
			<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary = this.value.split('\\'); document.getElementById('attachname0').value=Ary[Ary.length-1];"><input type="hidden" name="attachname0" id="attachname0">
		<? } else {?>
			<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink1']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink1'])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='1'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
		<? } ?>
			<? if ($filecount == 0) { ?> <a class="tablelink" href="#more_file_a" onClick="javascript:document.getElementById('more_file').style.display = 'block';document.getElementById('tohide').style.display = 'none'"><span id="tohide"><?= $eEnrollment['add_activity']['more_attachment']?></span></a> <? } ?>
			<div id="more_file" style="display : <? ($filecount > 0) ? print "block": print "none"; ?>"><a name="more_file_a"></a>
			<? if ($EnrollGroupArr['AttachmentLink2'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary1 = this.value.split('\\'); document.getElementById('attachname1').value=Ary1[Ary1.length-1];"><input type="hidden" name="attachname1" id="attachname1">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink2']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink2'])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='2'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr['AttachmentLink3'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary2 = this.value.split('\\'); document.getElementById('attachname2').value=Ary2[Ary2.length-1];"><input type="hidden" name="attachname2" id="attachname2">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink3']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink3'])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='3'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr['AttachmentLink4'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary3 = this.value.split('\\'); document.getElementById('attachname3').value=Ary3[Ary3.length-1];"><input type="hidden" name="attachname3" id="attachname3">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink4']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink4'])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='4'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			<? if ($EnrollGroupArr['AttachmentLink5'] == "")  { ?>
				<input type="file" id="attach[]" name="attach[]" class="textboxtext" onChange="var Ary4 = this.value.split('\\'); document.getElementById('attachname4').value=Ary4[Ary4.length-1];"><input type="hidden" name="attachname4" id="attachname4">
			<? } else {?>
				<a href="<?= "/file/".$EnrollGroupArr['AttachmentLink5']?>" target="_blank" class="tablelink"><?= $libenroll->ShowFileName($EnrollGroupArr['AttachmentLink5'])?></a>&nbsp;(<a href="javascript: document.form1.submit();" onClick="document.getElementById('FileToDel').value='5'; document.form1.action='file_delete_update.php'" class="tablelink"><?= $button_remove ?></a>)<br/>
			<? } ?>
			</div>
		</td>
	</tr>


</table>

</td></tr>

<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_save_continue, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<? if (sizeof($EnrollGroupArr) > 0) print $linterface->GET_ACTION_BTN($eEnrollment['skip'], "button", "self.location='group_new1b.php?EnrolGroupID=$EnrolGroupID&Semester=$Semester'")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='group.php?".$cancel_btn_para."'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>

</table>
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="GroupID" id="GroupID" value="<?= $GroupID?>" />
<input type="hidden" name="EnrolGroupID" id="EnrolGroupID" value="<?= $EnrolGroupID?>" />
<input type="hidden" name="Semester" id="Semester" value="<?= $Semester?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />

</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>