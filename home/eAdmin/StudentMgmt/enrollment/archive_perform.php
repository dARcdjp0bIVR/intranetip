<?php
// USING : 
# ********************** Change Log ***********************/
#       Date    :	2010-06-10 [Ivan]
#       Details :   Bug fix on update statment
#
#       Date    :	2010-06-04 [Ivan]
#       Details :   Fixed transfer the rejected student of the activity problem ("And a.RecordStatus='2'" vs "Where a.RecordStatus='2'")
#
#       Date    :   2010-05-26 [FAI]
#       Details :   Add control to allow user to select which type of data to transfer to student proflie "$transferClubFlag  , #      $transferActivityFlag "
# ******************* End Of Change Log *******************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$transferClubFlag = false;
$transferActivityFlag = false;

for($i = 0; $i < sizeof($transferItem); $i++)
{
	$_requestType = $transferItem[$i];

	switch(strtoupper($_requestType)){
		case "CLUB": 
			$transferClubFlag = true;
		break;
		case "ACTIVITY":
			$transferActivityFlag = true;
		break;
	}
}




$li = new libdb();

//if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
//	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");


$libenroll->setArchive();

$year = getCurrentAcademicYear();
//$sem = getCurrentSemester();

$ECA_groupCategory = 5;
$type_student = 2;

if($transferClubFlag == true)
{
	$TitleField = Get_Lang_Selection('ig.TitleChinese', 'ig.Title');
	# Extract current records
	$sql = "SELECT 
					iug.UserID, $TitleField as GroupTitle, ir.Title as RoleTitle, iug.Performance,iu.ClassName,iu.ClassNumber,iegi.Semester,iegs.EnrolSemester
			FROM 
					INTRANET_USERGROUP as iug
					LEFT OUTER JOIN 
					INTRANET_GROUP as ig ON iug.GroupID = ig.GroupID
					LEFT OUTER JOIN 
					INTRANET_ENROL_GROUPINFO as iegi ON iug.EnrolGroupID = iegi.EnrolGroupID
					LEFT OUTER JOIN 
					INTRANET_ROLE as ir ON iug.RoleID = ir.RoleID
					LEFT OUTER JOIN 
					INTRANET_USER as iu ON iug.UserID = iu.UserID
					LEFT OUTER JOIN 
					INTRANET_ENROL_GROUPSTUDENT as iegs ON (iug.UserID = iegs.StudentID And iegs.EnrolGroupID = iug.EnrolGroupID)
			WHERE 
					ig.RecordType = '$ECA_groupCategory'
					AND
					iu.RecordType = '$type_student'
					And
					ig.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			";
	$result = $li->returnArray($sql,6);

	for ($i=0; $i<sizeof($result); $i++)
	{
		list ($uid,$group,$role,$perform,$classname,$classnumber,$semID,$enrolSemID) = $result[$i];

		if ($semID == '')
		{
		   # Year-based event => transfer the enrol semester
		   $semID = $enrolSemID;
		}
		
		# Get sem name
		$objTerm = new academic_year_term($semID);
		$sem = $objTerm->YearTermNameEN;
		
		 # Try to find matched record first
		 $conds = "UserID = '$uid' AND ActivityName = '$group' AND Year = '$year' AND Semester = '$sem'";
		 $sql = "SELECT StudentActivityID FROM PROFILE_STUDENT_ACTIVITY WHERE $conds";
		 $temp = $li->returnVector($sql);
		 $act_id = $temp[0]+0;

		 if ($act_id==0)     # No matched record
		 {
			 # Perform insert
			 $sql = "INSERT INTO PROFILE_STUDENT_ACTIVITY (UserID,Year,Semester,ActivityName,Role,Performance,DateInput,DateModified,ClassName,ClassNumber)
					 VALUES ($uid,'$year','$sem','$group','$role','$perform',now(),now(),'$classname','$classnumber')";
			 $li->db_db_query($sql);
		 }
		 else
		 {
			 # update
			 $fields = "Role = '$role', Performance = '$perform'";
			 $sql = "UPDATE PROFILE_STUDENT_ACTIVITY SET $fields, DateModified = now() WHERE StudentActivityID = '$act_id'";
			 $li->db_db_query($sql);
		 }
	}
}

#########################################################################################################

if($transferActivityFlag == true){

	$semID = getCurrentSemesterID();
	$objTerm = new academic_year_term($semID);
	$sem = $objTerm->YearTermNameEN;

	$CurrentYearClubArr = $libenroll->Get_Current_AcademicYear_Club();
	$EnrolGroupID_conds = '';
	if (count($CurrentYearClubArr) > 0)
	{
		$CurrentYearClubList = implode(',', $CurrentYearClubArr);
		$EnrolGroupID_conds = " Or b.EnrolGroupID In ($CurrentYearClubList) ";
	}
		
	# Extract current records (events)     
	$sql = "SELECT a.StudentID, b.EventTitle, '".$eEnrollment['event_member']."', a.Performance, d.ClassName, d.ClassNumber
			FROM INTRANET_ENROL_EVENTSTUDENT as a
				 LEFT OUTER JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID
				 LEFT OUTER JOIN INTRANET_USER as d ON a.StudentID = d.UserID
			Where
				 a.RecordStatus = '2'
				 And
				 (b.EnrolGroupID = '' Or b.EnrolGroupID Is Null $EnrolGroupID_conds)
		   ";
	$result = $li->returnArray($sql,6);

	for ($i=0; $i<sizeof($result); $i++)
	{
		 list ($uid,$group,$role,$perform,$classname,$classnumber) = $result[$i];

		 # Try to find matched record first
		 $conds = "UserID = '$uid' AND ActivityName = '$group' AND Year = '$year' AND Semester = '$sem'";
		 $sql = "SELECT StudentActivityID FROM PROFILE_STUDENT_ACTIVITY WHERE $conds";
		 $temp = $li->returnVector($sql);
		 $act_id = $temp[0]+0;

		 if ($act_id==0)     # No matched record
		 {
			 # Perform insert
			 $sql = "INSERT INTO PROFILE_STUDENT_ACTIVITY (UserID,Year,Semester,ActivityName,Role,Performance,DateInput,DateModified,ClassName,ClassNumber)
					 VALUES ($uid,'$year','$sem','$group','$role','$perform',now(),now(),'$classname','$classnumber')";
			 $li->db_db_query($sql);
		 }
		 else
		 {
			 # update
			 $fields = "Role = '$role', Performance = '$perform'";
			 $sql = "UPDATE PROFILE_STUDENT_ACTIVITY SET $fields, DateModified = now() WHERE StudentActivityID = '$act_id'";
			 $li->db_db_query($sql);
		 }
	}
}
intranet_closedb();
header("Location: archive.php?msg=2");
?>