<?php

/*
 *  2018-08-08 Cameron
 *      - add $resultAry to return action result
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$AcademicYearID = IntegerSafe($AcademicYearID);

$resultAry = array();
for ($i = 0; $i < sizeof($EnrolEventID); $i++) {
	$resultAry[] = $libenroll->DEL_EVENT($EnrolEventID[$i]);
}

if (!in_array(false,$resultAry)) {
    $returnMsgKey = 'DeleteSuccess';
}
else {
    $returnMsgKey = 'DeleteUnSuccess';
}

intranet_closedb();
//header("Location: event.php?msg=3&AcademicYearID=$AcademicYearID");
header("Location: event.php?AcademicYearID=$AcademicYearID&returnMsgKey=".$returnMsgKey);
?>