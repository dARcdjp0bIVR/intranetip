<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$li = new libgroup($GroupID);

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	if ($type == "activity")
	{
		$CurrentPage = "PageMgtActivity";
		$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		$CurrentPageArr['eEnrolment'] = 1;
	    //$TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['notification_letter'], "", 1);
	    # tags 
        $tab_type = "activity";
        # navigation
        $PAGE_NAVIGATION[] = array($EventInfoArr[3]." ".$eEnrollment['notification_letter'], "");
	}
	else if ($type == "club")
	{
		$CurrentPage = "PageMgtClub";
		$CurrentPageArr['eEnrolment'] = 1;
		//$TAGS_OBJ[] = array($li->Title." ".$eEnrollment['notification_letter'], "", 1);
		# tags 
        $tab_type = "club";
        # navigation
        $PAGE_NAVIGATION[] = array($li->Title." ".$eEnrollment['notification_letter'], "");
	}	
	$current_tab = 1;
    include_once("management_tabs.php");
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        

	    ################################################################
	    /* Old code
		    Old Approach: user select student in this page
		    New Approach: user select student before entering this page
	    if ($type=="activity")
	    {
		    
			// get members' userID, name and class number
        	$name_field = getNameFieldByLang("b.");
        	$sql = "SELECT
        					b.UserID, $name_field, b.ClassName, b.ClassNumber
					FROM
							INTRANET_ENROL_EVENTSTUDENT As a
					LEFT JOIN
							INTRANET_USER As b
						ON
							a.StudentID = b.UserID
					WHERE
							b.RecordType = 2
						AND
							a.RecordStatus = 2
						AND
							a.EnrolEventID = '$EnrolEventID'
					ORDER BY 
							b.ClassName, b.ClassNumber
					";
		    $row = $libenroll->returnArray($sql,4);
		    $memberOption = "";
            for($i=0; $i<sizeof($row); $i++){
                    $displayName = $row[$i][1]." (".$row[$i]['ClassName']."-".$row[$i]['ClassNumber'].")";
                    $memberOption .= "<option value=".$row[$i]['UserID']." SELECTED>$displayName</option>\n";
            }
			
	    }
	    else if ($type == "club")
	    {
		    // get members' userID, name and class number
        	$name_field = getNameFieldByLang("b.");
        	$sql = "SELECT
        					b.UserID, $name_field, b.ClassName, b.ClassNumber
					FROM
							INTRANET_USERGROUP As a
					LEFT JOIN
							INTRANET_USER As b
						ON
							a.UserID = b.UserID
					WHERE
							b.RecordType = 2
						AND
							a.GroupID = '$GroupID'
					ORDER BY 
							b.ClassName, b.ClassNumber
					";
		    $row = $libenroll->returnArray($sql,4);
		    $memberOption = "";
            for($i=0; $i<sizeof($row); $i++){
                    $displayName = $row[$i][1]." (".$row[$i]['ClassName']."-".$row[$i]['ClassNumber'].")";
                    $memberOption .= "<option value=".$row[$i]['UserID']." SELECTED>$displayName</option>\n";
            }
	    }
		*/
		
		$studentIDArr = $UID;
		$studentList = implode(",", $studentIDArr);
	    
	    # get student name, class and class number for display
	    $name_field = getNameFieldWithClassNumberByLang();
	    $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($studentList) ORDER BY ClassName,ClassNumber";
	    $studentDisplayArr = $libenroll->returnVector($sql);
	    
	    $toRowsHTML = "";
	    for ($i=0; $i<sizeof($studentDisplayArr); $i++)
	    {
		    $toRowsHTML .= "<tr><td class=\"tabletext formfieldtitle\" width=\"30%\" valign=\"top\">";
		    # show "To" in the first row
			if ($i==0)
			{
				$toRowsHTML .= $i_email_to;
			}
			else
			{
				$toRowsHTML .= "&nbsp;";
			}
			$toRowsHTML .= "</td>";
			
			$toRowsHTML .= "<td class=\"tabletext\">{$studentDisplayArr[$i]}</td>";
			$toRowsHTML .= "</tr>";
	    }
	    
	    $linterface->LAYOUT_START();
?>

<script language="javascript">
function showLetter()
{
       	checkPost(document.form1, 'notification_letter_view.php');
}
</script>

<form name="form1" target="_blank">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>

<table width='90%' border='0' cellpadding='4' cellspacing='0'>
<?=$toRowsHTML?>
</table>

<input type='hidden' name='GroupID' value="<?= $GroupID ?>">
<input type='hidden' name='EnrolEventID' value="<?= $EnrolEventID ?>">
<input type='hidden' name='type' value="<?= $type ?>">
<input type="hidden" name="memberList" value="<?=rawurlencode(serialize($UID));?>">

<table width="90%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">

<?= $linterface->GET_ACTION_BTN($button_view, "button", "javascript:showLetter()")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
</div>
</td></tr>
</table>

</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>