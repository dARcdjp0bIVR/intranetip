<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && (!$libenroll->IS_CLUB_PIC())
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$TempEventArr = $libenroll->GET_EVENTINFO($EnrolEventID);

$EventArr['EnrolEventID'] = $EnrolEventID;
$EventArr['Description'] = addslashes($TempEventArr['Description']);
$EventArr['UpperAge'] = $TempEventArr['UpperAge'];
$EventArr['LowerAge'] = $TempEventArr['LowerAge'];
$EventArr['Gender'] = $TempEventArr['Gender'];
$EventArr['attach'] = $_FILES['attach'];
$EventArr['Quota'] = $Quota;
$EventArr['EventTitle'] = addslashes($TempEventArr['EventTitle']);
$EventArr['EventCategory'] = $TempEventArr['EventCategory']; 
//$EventArr['ApplyStartTime'] = $ApplyStartTime;
//$EventArr['ApplyEndTime'] = $ApplyEndTime;
$EventArr['ApplyStartTime'] = $ApplyStartTime." ".$ApplyStartHour.":".$ApplyStartMin.":00";
$EventArr['ApplyEndTime'] = $ApplyEndTime." ".$ApplyEndHour.":".$ApplyEndMin.":00";
$EventArr['ApplyMethod'] = $tiebreak;
$EventArr['GroupID'] = $TempEventArr['GroupID'];
$EventArr['EnrolGroupID'] = $TempEventArr['EnrolGroupID'];
$EventArr['PaymentAmount'] = $TempEventArr['PaymentAmount'];
$EventArr['Nature'] = $TempEventArr['SchoolActivity'];

if ($libenroll->IS_EVENTINFO_EXISTS($EnrolEventID)) {
	$EnrolEventID = $libenroll->EDIT_EVENTINFO($EventArr);
} else {
	$EnrolEventID = $libenroll->ADD_EVENTINFO($EventArr);
}

$libenroll->DEL_EVENTSTAFF($EnrolEventID);

$PICArr['EnrolEventID'] = $EnrolEventID;
$PICArr['StaffType'] = "PIC";
for ($i = 0; $i < sizeof($pic); $i++) {
	$PICArr['UserID'] = $pic[$i];
	$libenroll->ADD_EVENTSTAFF($PICArr);
}

$PICArr['StaffType'] = "HELPER";
for ($i = 0; $i < sizeof($helper); $i++) {
	$PICArr['UserID'] = $helper[$i];
	$libenroll->ADD_EVENTSTAFF($PICArr);
}

intranet_closedb();
header("Location: event_new2.php?EnrolEventID=$EnrolEventID&isNew=$isNew");
?>