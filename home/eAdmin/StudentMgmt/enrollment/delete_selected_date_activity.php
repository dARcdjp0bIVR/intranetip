<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

if (!$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$libenroll = new libclubsenrol();
$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EnrolEventID); 

# update INTRANET_ENROL_GROUP_DATE status
foreach($toSet as $k=>$d)
{
	$libenroll->INACTIVATE_ENROL_EVENT_DATE($DateArr[$d-1]['EventDateID']);
}

intranet_closedb();

header("Location: event_new2.php?EnrolEventID=$EnrolEventID");
?>