<?php
#Modify :
/*
 * 	2020-01-30 Henry
 * 				Added EnrollMin logic
 * 
 *  2018-04-27  Anna
 *              Added Committee Recruitment Setting
 *              
 * 	2014-11-13	Omas
 * 				Comment $enrolInfo improve Performance
 * 
 *  	Date:	2013-08-12 (Cameron) Add argument CategoryTypeID to Get_Category_Setting_UI()
 * 
 */

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol("",$sel_category);

if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if ($plugin['eEnrollment'])
{
	$CurrentPage = "PageSysSettingBasic";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

	//$enrolInfo = $libenroll->getEnrollmentInfo(); ##Comment by Omas

    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
		include_once($PATH_WRT_ROOT."includes/libinterface.php");
		include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
		include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

		$libenroll_ui = new libclubsenrol_ui();

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($Lang['eEnrolment']['GeneralSetting'], "basic.php", 0);
        $TAGS_OBJ[] = array($Lang['eEnrolment']['CategorySetting'], "", 1);
        if($sys_custom['eEnrolment']['CommitteeRecruitment']){
            $TAGS_OBJ[] = array($Lang['eEnrolment']['CommitteeRecruitmentSetting'], "committee_recruitment_setting.php", 0);
        }
        if($msg==2)
        	$ReturnMessage = $Lang['General']['ReturnMessage']['UpdateSuccess'];
        
		$linterface->LAYOUT_START($ReturnMessage);
		
		if ($sys_custom['eEnrolment']['CategoryType']) {
			$categoryTypeID = array(1);	// Club
		}
		else {
			$categoryTypeID = 0;
		}
		
		echo $libenroll_ui->Get_Category_Setting_UI("Club",$sel_category,$categoryTypeID);
?>

<script language="javascript">
$(document).ready( function() {	
	js_Toggle_Status();
	clickedQuotaSettingsType('<?=$libenroll->quotaSettingsType?>');
});

function js_Toggle_Status()
{
	var Status = $("input[name='CategorySettingStatus']:checked","tbody#StatusTable").val()
	if(Status==1)
	{
		$("select.selection","tbody#SettingTable").attr("disabled","");
		$("#sel_category","tbody#CategoryTable").attr("disabled","");
	}
	else
	{
		$("select.selection","tbody#SettingTable").attr("disabled","disabled");
		$("#sel_category","tbody#CategoryTable").attr("disabled","disabled");
	}
}

function checkform(obj)
{
	if(obj.sel_category)
	{
		if(!obj.sel_category.disabled && $(obj.sel_category).val() == '')
		{
			alert("<?=$Lang['eEnrolment']['PleaseSelectCategory']?>");
			return false;
		}
	}
	
//	if(obj.defaultMax && obj.defaultMin && obj.EnrollMax)
//	{
//		if (obj.defaultMax.value != 0 && obj.defaultMin.value > obj.defaultMax.value)
//		{
//			alert("<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>");
//			return false;
//		}
//		if (obj.defaultMax.value != 0)
//		{
//			if ( (obj.EnrollMax.value == 0) || (obj.EnrollMax.value < obj.defaultMin.value) || (obj.EnrollMax.value > obj.defaultMax.value) )
//			{
//				alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
//				return false;
//			}
//		}
//	}

	var isVaild = true;
	$('select.ApplyMinSel').each( function() {
		var _id = $(this).attr('id');
		var _idPieces = _id.split('_');
		var _termNum = _idPieces[1];
		var _categoryId = _idPieces[2];
		
		var _applyMin = $(this).val();
		var _applyMax = $('select#ApplyMax_' + _termNum + '_' + _categoryId + '_Sel').val();
		var _enrollMax = $('select#EnrollMax_' + _termNum + '_' + _categoryId + '_Sel').val();
		var _enrollMin = $('select#EnrollMin_' + _termNum + '_' + _categoryId + '_Sel').val();
		
		if (_applyMax != 0 && _applyMin > _applyMax) {
			isVaild = false;
			
			alert('<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>');
			$(this).focus();
			return false; 	// break
		}
		
		if (_enrollMax != 0 && _enrollMin > _enrollMax) {
			isVaild = false;
			
			alert('<?=$i_ClubsEnrollment_DefaultMaxMinWrong?>');
			$('select.EnrollMinSel').focus();
			return false; 	// break
		}
		
		if (_applyMax != 0) {
			if ( (_enrollMax == 0) || (_enrollMax > _applyMax) ) {
				isVaild = false;
				
				alert("<?=$i_ClubsEnrollment_EnrollMaxWrong?>");
				$(this).focus();
				return false; 	// break
			}
		}
		
		if (_enrollMin != 0 && _enrollMin > _applyMin) {
			isVaild = false;
			
			alert('<?=$Lang['eEnrolment']['jsWarning']['EnrollMinWrong']?>');
			$('select.ApplyMinSel').focus();
			return false; 	// break
		}
	});
	
	if (isVaild == false) {
		return false;
	}
	

	$("input.CategorySettingStatus","#form1").attr("disabled","")
	obj.action="category_setting_club_update.php";
	obj.submit();
}


function clickedQuotaSettingsType(targetType) {
	if (targetType == 'YearBase') {
		$('.termCol').hide();
	}
	else {
		$('.termCol').show();
	}
}
</script>

<?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>