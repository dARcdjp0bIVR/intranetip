<?php
/*
 * 	Log:
 * 	
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$EventAwardIDArr = $_POST['EventAwardID'];
$awardInfo['RelatedToType'] = $_POST['eventRecordType'];
$awardInfo['RelatedToID'] = $_POST['eventID'];

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])){
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;
}

foreach((array)$EventAwardIDArr as$EventAwardID ){
	$studentIDArr = $libenroll->getStudentIDAward($EventAwardID);
	$libenroll->RemoveEventAwardToStudent($EventAwardID,$studentIDArr);
	$sql = "DELETE from INTRANET_ENROL_EVENT_AWARD where EventAwardID = '".$EventAwardID."' ";
	$libenroll->db_db_query($sql);
}

intranet_closedb();
header("Location: index.php?ID=".$_POST['eventID']."&Type=".$_POST['eventRecordType']."&ReturnMsgKey=$ReturnMsgKey");
?>