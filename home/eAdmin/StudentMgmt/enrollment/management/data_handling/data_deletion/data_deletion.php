<?
//using: 
##### Change Log [Start] #####
#
#   Date    :   2020-07-09  Tommy
#               added form selection for $sys_custom['eEnrollment']['setTargetForm']
#
#   Date    :   2020-01-16  Tommy
#               hide to SLP & to WebSMAS for KIS client
#
#	Date	:	2015-06-01	Omas
# 				Create this page
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$libenroll = new libclubsenrol();
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);

if((!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$htmlAry['instructionBox'] = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eEnrolment']['DataDeletionRemarks']);

# Tab
$curTab = 'data_deletion';
$data_batch_process_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_sp/step1.php";
$transer_to_websams_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_websams/step1.php";
$transer_to_ole_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_ole/step1.php?clearCoo=1";
$data_deletion_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/data_deletion/data_deletion.php";
$transer_to_cees_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_cees/step1.php";
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], $data_batch_process_link, $curTab=='to_sp');
if($_SESSION["platform"] != "KIS"){
    if($plugin['iPortfolio']) {
    	$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], $transer_to_ole_link, $curTab=='to_ole');
    }
    if(!$sys_custom['project']['NCS']){
    $TAGS_OBJ[] = array($Lang['eEnrolment']['data_handling']['export_websams'], $transer_to_websams_link, $curTab=='to_websams');
    }
}
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClearAllEnrol'], $data_deletion_link, $curTab=='data_deletion');

if($plugin['SDAS_module']['KISMode']){
	$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_CEES'], $transer_to_cees_link, $curTab=='to_cees');
}
//$STEPS_OBJ = $libenroll->GET_STEPS_OBJ_ARR($Lang['eEnrolment']['Transfer_to_SP']['StepArr'], 1);
				
if($msg!="") {
	$displayMsg = $Lang['General']['ReturnMessage'][$msg];	
}

# Header Row
$HeaderRow = "<tr>";
$HeaderRow .= "<th>&nbsp;</th>";
$HeaderRow .= "<th>&nbsp;".$Lang['eEnrolment']['ClearEnrol']."&nbsp;</th>";
$HeaderRow .= "<th>&nbsp;".$Lang['eEnrolment']['LastClearEnrol']."&nbsp;</th>";
$HeaderRow .= "</tr>";


$clearClubArr = $libenroll->Get_last_clear_enrol_data_time('C');
$clearActivityArr = $libenroll->Get_last_clear_enrol_data_time('A');
$clearDateClub = $clearClubArr['LogDate'];
$clearNameClub = $clearClubArr['DisplayName'];
if($clearDateClub == ''&& $clearNameClub == ''){
	$displayClubLog = $Lang['General']['EmptySymbol'];
}
else{
	$displayClubLog = $clearDateClub.' '.$Lang['RepairSystem']['by'].' '.$clearNameClub;
}
$clearDateActivity = $clearActivityArr['LogDate'];
$clearNameActivity = $clearActivityArr['DisplayName'];
if($clearDateActivity == ''&& $clearNameActivity == ''){
	$displayActivityLog = $Lang['General']['EmptySymbol'];
}
else{
	$displayActivityLog = $clearDateActivity.' '.$Lang['RepairSystem']['by'].' '.$clearNameActivity;
}

if($sys_custom['eEnrollment']['setTargetForm']){
    include_once($PATH_WRT_ROOT."includes/libclass.php");
    $libenrolllass = new libclass();
    $ClassLvlArr = $libenrolllass->getLevelArray();
    $disabledClassArr = $libenroll->getMinMax();
    $sql = "SELECT * FROM GENERAL_SETTING WHERE SettingName = 'Club_TargetForm'";
    $result = $libenroll->returnArray($sql);
    $selectedForm = array();
    $allClassName = array();
    $disabledClassNameArr = array();
    $canUseEnrolClass = array();
    
    if(!empty($result[0])){
        $selectedForm = explode(",", $libenroll->settingTargetForm);
    }else{
        foreach($ClassLvlArr as $classlvl){
            $selectedForm[] = $classlvl["LevelName"];
        }
    }
    
    if(sizeof($disabledClassArr) > 0){
        foreach ($disabledClassArr as $disabledClassID => $classVal){
            $disabledClassNameArr[] = $libenrolllass->getLevelName($disabledClassID);
        }
    }
    
    for($i = 0; $i < sizeof($ClassLvlArr); $i++){
        $allClassName[] = $ClassLvlArr[$i]["LevelName"];
    }
    
    $canUseEnrolClass = array_diff($allClassName, $disabledClassNameArr);
    
    $classNum = 0;
    $ClassLvlChk = "<form name=\"form1\" action=\"\" method=\"post\">";
    if ($sys_custom['eEnrolment']['HideClassLevel']) {
        $ClassLvlChk .= "<input type=\"hidden\" id=\"classlvlall\">";
        foreach($canUseEnrolClass as $className){
            $checked = "";
            for ($j = 0; $j < sizeof($selectedForm); $j++) {
                if (in_array($className,$selectedForm)) {
                    $checked = " checked ";
                    break;
                }
            }
            $ClassLvlChk .= "<input type=\"hidden\" id=\"classlvl{$classNum}\" name=\"classlvl[]\" value=\"".$className."\">";
            $classNum++;
        }
    } else {
        if(sizeof($canUseEnrolClass) == sizeof($selectedForm) || empty($result[0])){
            $allChecked = " checked ";
        }
        $ClassLvlChk .= "<input type=\"checkbox\" $allChecked id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.form1, 'classlvl[]') : setChecked(0, document.form1, 'classlvl[]'); \"><label for=\"classlvlall\">".$eEnrollment['all']."</label>";
        $ClassLvlChk .= "<table>";
        foreach($canUseEnrolClass as $className){
            $checked = "";
            for ($j = 0; $j < sizeof($selectedForm); $j++) {
                if (in_array($className,$selectedForm)) {
                    $checked = " checked ";
                    break;
                }
            }
            
            if (($classNum % 10) == 0) $ClassLvlChk .= "<tr>";
            $ClassLvlChk .= "<td class=\"tabletext\"><input type=\"checkbox\" $checked id=\"classlvl{$classNum}\" name=\"classlvl[]\" value=\"".$className."\"><label for=\"classlvl{$classNum}\">".$className."</label></td>";
            if (($classNum % 10) == 9) $ClassLvlChk .= "</tr>";
            $classNum++;
        }
        $ClassLvlChk .= "</table>";
    }
    $ClassLvlChk .= "</form>";
}

# Club Row
$ButtonClass = "class='formsubbutton'";
$ClubRow = "<tr>";
$ClubRow .=	"<td>".$eEnrollment['club']."</td>";
$ClubRow .=	"<td>".$linterface->GET_BTN($button_erase, "button", "doRemove('C')")."</td>";
$ClubRow .=	"<td>".$displayClubLog."</td>";
$ClubRow .= "</tr>";

# Activity Row
$ActivityRow = "<tr>";
$ActivityRow .=	"<td>".$eEnrollment['activity']."</td>";
$ActivityRow .=	"<td>".$linterface->GET_BTN($button_erase, "button", "doRemove('A')")."</td>";
$ActivityRow .=	"<td>".$displayActivityLog."</td>";
$ActivityRow .= "</tr>";

$Content = "<table class='common_table_list'>";
$Content .= $HeaderRow;
$Content .= $ClubRow;
$Content .= $ActivityRow;
$Content .= "</table>";

$linterface->LAYOUT_START();

?>
<script>
function doRemove(type){
	var isEnable = document.getElementById('setTargetForm').value;
	var targetForm = [];

	if(isEnable){
		$('input:checkbox[name="classlvl[]"]:checked').each(function(){
			targetForm.push($(this).val());
		});
		window.location = 'data_deletion_confirm.php?type='+ type + '&form=' + targetForm;
	}else{
		window.location = 'data_deletion_confirm.php?type='+ type;
	}
}
</script>
<?=$htmlAry['instructionBox']?>
<?=$ClassLvlChk?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td align="center">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td>
			<?=$Content?>
			</td></tr>
		</table>
	</td></tr>
</table>
<input type="hidden" id="setTargetForm" name="setTargetForm" value="<?=$sys_custom['eEnrollment']['setTargetForm']?>">
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

