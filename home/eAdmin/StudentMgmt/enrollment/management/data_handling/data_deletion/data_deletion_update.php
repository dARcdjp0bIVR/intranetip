<?
//using: Tommy
##### Change Log [Start] #####
#
#	Date	:	2015-06-01	Omas
# 				Create this page
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();
//$linterface = new interface_html();
$libenroll = new libclubsenrol();
$linterface = new libclubsenrol_ui();
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);

if (!$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$type = $_GET['type'];
$targetform = $_GET['form'];

if($type != ''){
    $libenroll->Clear_All_Data($type, $targetform);	
}

header("Location: data_deletion.php");
?>;