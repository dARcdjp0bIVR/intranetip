<?
//using: 
##### Change Log [Start] #####
#
#   Date    :   2020-07-09 Tommy
#               added parameter to $confirmTable
#
#	Date	:	2015-06-01	Omas
# 				Create this page
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();
//$linterface = new interface_html();
$libenroll = new libclubsenrol();
$linterface = new libclubsenrol_ui();
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);

if (!$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
$type = $_GET['type'];
$targetform = $_GET['form'];
# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Tab
$curTab = 'data_deletion';
$data_batch_process_link = $plugin['eEnrollmentLite'] ? "lite" : $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_sp/step1.php";
$transer_to_websams_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_websams/step1.php";
$transer_to_ole_link = $plugin['eEnrollmentLite'] ? "lite" : $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_ole/step1.php?clearCoo=1";
$data_deletion_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/data_deletion/data_deletion.php";
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], $data_batch_process_link, $curTab=='to_sp');
if($plugin['iPortfolio']) {
	$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], $transer_to_ole_link, $curTab=='to_ole');
}
if(!$sys_custom['project']['NCS']){
$TAGS_OBJ[] = array($Lang['eEnrolment']['data_handling']['export_websams'], $transer_to_websams_link, $curTab=='to_websams');
}
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClearAllEnrol'], $data_deletion_link, $curTab=='data_deletion');


$confirmTable = $linterface->Get_Clear_Data_Confirm_Table($type, $targetform);

### Navaigtion
$displayTitle = ($type == 'C')? $eEnrollment['club']:$eEnrollment['activity'];
$PAGE_NAVIGATION[] = array($Lang['eEnrolment']['ClearAllEnrol'], "javascript:js_Go_Back();");
$PAGE_NAVIGATION[] = array($displayTitle, '', 1);
	 	
### Warning Msg Box
$WarningBox = $linterface->GET_WARNING_TABLE('', $Lang['eEnrolment']['EnrolRecord'], '');
		
### Buttons
$DeleteBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['DeleteAll'], "button", "js_Go_Clear_Data();", "", "id='submitBtn'", 1, "formbutton_alert");
$CancelBtn = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "js_Go_Back();", "", "", "", "formbutton_v30");
		
$x = '';
$x .= '<form id="form1" name="form1" method="post">'."\n";
	$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td>'."\n";
				$x .= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION)."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td>'."\n";
				$x .= $WarningBox."\n";
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
		$x .= '<tr>'."\n";
			$x .= '<td>'."\n";
				$x .= $confirmTable;
			$x .= '</td>'."\n";
		$x .= '</tr>'."\n";
	$x .= '</table>'."\n";	
	$x .= '<div class="edit_bottom_v30">';
		$x .= $DeleteBtn."\n";
		$x .= $CancelBtn."\n";
	$x .= '</div>';
$x .= '</form>'."\n";
$x .= '<br style="clear:both;" />'."\n";
$linterface->LAYOUT_START();
?>
<script>
function js_Go_Back(){
	window.location = 'data_deletion.php';
}
function js_Go_Clear_Data(){
	window.location = 'data_deletion_update.php?type=<?=$type?>&form=<?=$targetform?>';
}

function disableBtn(){
	
	var checkingEnrolList = $('input#notAllowSubmit').val(); // from $confirmTable
	
	if(checkingEnrolList == 1){
		notAllowToSubmit = 1;
	}
	else{
		notAllowToSubmit = 0;
	}
	
	if(notAllowToSubmit){
		// nth
	}
	else{
		document.getElementById("submitBtn").removeAttribute("disabled");
		document.getElementById("submitBtn").className = "formbutton_alert";
	}
	
}
$(document).ready( function() {
	disableBtn();
});
</script>
<?=$x?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>