<?php
#######################
#	Date	:	2015-07-07	Omas
#				Fixed: Club / Activity disappear issue #P80762
# 				updated left menu 
#
#######################
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");


intranet_auth();
intranet_opendb();

//$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$linterface = new interface_html();

# Check access right
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], "", 1);

$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step1'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step2'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step3'], 0);
$STEPS_OBJ[] = array($eEnrollment['transfer_finish']['step4'], 1);
		
$linterface->LAYOUT_START();

$numTransferredArr = explode(",", $numTransferredList);


if($RecordType=="club")
{
	$titleField = Get_Lang_Selection('b.TitleChinese', 'b.Title');
	$sql = "SELECT 
				$titleField as title,  
				if(left(min(c.ActivityDateStart),10) = left(max(c.ActivityDateEnd),10), left(min(c.ActivityDateStart),10), concat(left(min(c.ActivityDateStart),10) , ' - ' , left(max(c.ActivityDateEnd),10))),
				COUNT(DISTINCT(d.RecordID)),
				a.OLE_ProgramID
			FROM 
				INTRANET_ENROL_GROUPINFO as a 
				LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
				LEFT OUTER JOIN INTRANET_ENROL_GROUP_DATE as c ON c.EnrolGroupID  = a.EnrolGroupID and (c.RecordStatus IS NULL OR c.RecordStatus = 1)
				LEFT OUTER JOIN {$eclass_db}.OLE_STUDENT as d ON d.ProgramID = a.OLE_ProgramID
			WHERE
				/*(c.RecordStatus IS NULL OR c.RecordStatus = 1)
				AND*/
				a.EnrolGroupID in ($ID_Str)
			GROUP BY
				a.EnrolGroupID
			ORDER BY
				b.Title
			";
}
else
{
	$sql ="
		SELECT
			a.EventTitle as title,
			if(left(min(b.ActivityDateStart),10) = left(max(b.ActivityDateEnd),10), left(min(b.ActivityDateStart),10), concat(left(min(b.ActivityDateStart),10) , ' - ' , left(max(b.ActivityDateEnd),10))),
			COUNT(DISTINCT(c.RecordID)),
			a.OLE_ProgramID
		FROM
			INTRANET_ENROL_EVENTINFO as a
			LEFT JOIN INTRANET_ENROL_EVENT_DATE as b on b.EnrolEventID = a.EnrolEventID AND (b.RecordStatus IS NULL OR b.RecordStatus = 1) 
			LEFT OUTER JOIN {$eclass_db}.OLE_STUDENT as c ON c.ProgramID = a.OLE_ProgramID
		WHERE
			/*(b.RecordStatus IS NULL OR b.RecordStatus = 1)
			AND*/
			a.EnrolEventID in ($ID_Str)
		GROUP BY
			a.EnrolEventID
		ORDER BY
			a.EventTitle
	";
}
$result = $libenroll->returnArray($sql);

### Academic Year and Record Type Display
$AcademicYearObj = new academic_year($AcademicYearID);
$AcademicYearName = $AcademicYearObj->Get_Academic_Year_Name();

$RecordTypeStr = $RecordType=="club" ? $eEnrollment['Club_Records'] : $eEnrollment['Activity_Records'];

$SelectedInfoTable = '';
$SelectedInfoTable .= '<table class="tabletext" cellpadding="2" style="width:100%;" align="center">'."\n";
	$SelectedInfoTable .= '<tr>'."\n";
		$SelectedInfoTable .= '<td class="formfieldtitle" width="30%">'.$Lang['General']['SchoolYear'].'</td>'."\n";
		$SelectedInfoTable .= '<td>'.$AcademicYearName.'</td>'."\n";
	$SelectedInfoTable .= '</tr>'."\n";
	$SelectedInfoTable .= '<tr>'."\n";
		$SelectedInfoTable .= '<td class="formfieldtitle" width="30%">'.$eEnrollment['Record_Type'].'</td>'."\n";
		$SelectedInfoTable .= '<td>'.$RecordTypeStr.'</td>'."\n";
	$SelectedInfoTable .= '</tr>'."\n";
$SelectedInfoTable .= '</table>'."\n";

?>
<script language="javascript">
<!--
function ClickTransfer()
{
	var obj = document.form1;
	var element = "ID[]";
	
	if(countChecked(obj,element)>0) 
	{
 		obj.action='transfer_ole2.php';
 		obj.submit();
    } else {
		alert(globalAlertMsg2);
    }
}
//-->
</script>

<br />
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr>
        <td align="center">
                <table width="96%" border="0" cellspacing="0" cellpadding="04">
                <tr>
					<td align="left" class="tabletext">
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<?=$SelectedInfoTable?>
								<br />
							</td>
						</tr>
						
                                <tr>
                                        <td colspan="2">
                                               <!-- Content //-->
                                               <table width='100%' border='0' cellpadding='4' cellspacing='0' align='center' bgcolor='#CCCCCC'>
													<tr class='tabletop'>
													<td class='tabletop tabletopnolink'>#</td>
													<td class='tabletop tabletopnolink'><?=$ec_iPortfolio['title']?></td>
													<td class='tabletop tabletopnolink'><?=$eEnrollment['record_in_OLE']?></td>
													<td class='tabletop tabletopnolink'><?=$eEnrollment['Date'] ." / " . $eEnrollment['Period']?></td>
													<td class='tabletop tabletopnolink'><?=$eEnrollment['num_transferred_record']?></td>
													</tr>
													
													<? for($i=0;$i<sizeof($result);$i++) 
													{ 
														list($tempTitle, $tempDate, $tempStudentNo, $OLE_ProgramID) = $result[$i];
														$css = ($i % 2) + 1;	
														$curNumTransferred = $numTransferredArr[$i];
														?>
														
													<tr class='tablerow<?=$css?>'>
													<td class='tabletext tablerow'><?=$i+1?></td>
													<td class='tabletext tablerow'><?=$tempTitle?></td>
													<td class='tabletext tablerow'><a href='javascript:newWindow("participants_list.php?ProgramID=<?=$OLE_ProgramID?>&RecordType=<?=$RecordType?>", 10)' class='tablelink'><?=$tempStudentNo?></a></td>
													<td class='tabletext tablerow'><?=$tempDate?></td>
													<td class='tabletext tablerow'><?=$curNumTransferred?></td>
													
													
													</tr>
													<? } ?>

												</table>
                                               <!-- Content End //-->
                                        </td>
                                </tr>
                                </table>
                        </td>
                </tr>
                <tr>
					<td height="1" class="dotline"><img src="<?=$PATH_WRT_ROOT?>images/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
				</tr>
				<tr>
					<td align="center">
					<?= $linterface->GET_ACTION_BTN($eEnrollment['Transfer_Another_OLE'], "button", "window.location='step1.php'") ?>
					</td>
				</tr>						
                </table>
        </td>
</tr>
</table>

</br>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>