<?php
#using by:

#######################
#	Date	:	2015-07-07	Omas
#				Fixed: Club / Activity disappear issue #P80762 
#
#	Date	:	2015-05-18	Omas
#			Fix: OLE_PROGRAM -> academicYearID wrongly following start date to determine academic year id
#
#	Date	:	2014-10-16	Omas
#			If no date/period is set in clubs/activity
#			will auto assign the date and period according to YearTermID of those clubs & activities
#			If it is a club activities will follow the YearTermID(semester) of the club 
#
#######################
 
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");
include_once($PATH_WRT_ROOT."/includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

//$LibUser = new libuser($UserID);
$linterface = new interface_html();
$libenroll = new libclubsenrol();


$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');


$AcademicYearID = $_POST['AcademicYearID'];
$RecordType = $_POST['RecordType'];
$ID = $_POST['ID'];
$FormChkArr = unserialize(rawurldecode($_POST['FormChkArr']));


//$LibPortfolio = new libportfolio2007();
$LibPortfolio = new libpf_slp();
$ID_Str = implode(",",$ID);

if($RecordType=="club")
{
	$sql = "SELECT 
				a.EnrolGroupID,
				b.Title,
				b.Title as TitleEn,
				b.TitleChinese as TitleCh,
				left(min(c.ActivityDateStart),10) as Startdate,
				left(max(c.ActivityDateEnd),10) as Enddate,
				a.OLE_ProgramID,
				c.EnrolGroupID,
				a.Semester,
				b.AcademicYearID,
				'' as ClubEnrolGroupID
			FROM 
				INTRANET_ENROL_GROUPINFO as a
				LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
				LEFT OUTER JOIN INTRANET_ENROL_GROUP_DATE as c ON c.EnrolGroupID  = a.EnrolGroupID and (c.RecordStatus IS NULL OR c.RecordStatus = 1)
			WHERE
				/*(c.RecordStatus IS NULL OR c.RecordStatus = 1)
				AND*/
				a.EnrolGroupID in ($ID_Str)
			GROUP BY
				a.EnrolGroupID
			ORDER BY
				b.Title
			";
	
}
else
{
	$sql ="
		SELECT
			a.EnrolEventID,
			a.EventTitle,
			a.EventTitle as TitleEn,
			a.EventTitle as TitleCh,
			left(min(e.ActivityDateStart),10) as Startdate,
			left(max(e.ActivityDateEnd),10) as Enddate,
			a.OLE_ProgramID,
			e.EnrolEventID,
			a.YearTermID,
			a.AcademicYearID,
			a.EnrolGroupID as ClubEnrolGroupID
		FROM
			INTRANET_ENROL_EVENTINFO as a
			LEFT JOIN INTRANET_ENROL_EVENTCLASSLEVEL as b ON b.EnrolEventID = a.EnrolEventID
			LEFT JOIN INTRANET_CLASS as c ON b.ClassLevelID = c.ClassLevelID
			LEFT JOIN INTRANET_USER as d ON d.ClassName = c.ClassName
			LEFT JOIN INTRANET_ENROL_EVENT_DATE as e on e.EnrolEventID = a.EnrolEventID AND (e.RecordStatus IS NULL OR e.RecordStatus = 1)
		WHERE
			/*(e.RecordStatus IS NULL OR e.RecordStatus = 1)
			AND*/
			a.EnrolEventID in ($ID_Str)
		GROUP BY
			a.EnrolEventID
		ORDER BY
			a.EventTitle
	";
}
$groupInfoArr = $libenroll->returnArray($sql);

# loop each group
$numTransferredArr = array();
$ProgramType = $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"];
for($i=0;$i<sizeof($groupInfoArr);$i++)
{
	list($tempGroupID, $tempTitle, $tempTitleEn, $tempTitleCh, $tempStartdate, $tempEnddate, $tempOLE_ProgramID, $EnrolGroupID, $YearTermID, $AcademicYearID, $tempClubEnrolGroupID) = $groupInfoArr[$i];

	# data
	
	$tempTitle = (${"titleLang_".$tempGroupID}==$enrolConfigAry['ENROLMENT_TO_OLE_SETTING']['TitleLang']['English'])? $tempTitleEn : $tempTitleCh;
	$title = addslashes($tempTitle);
	$category = ${"category_".$tempGroupID};
	$ELEList = ${"ele_".$tempGroupID};
	$ELEListStr = (sizeof($ELEList)!=0) ? implode(",", $ELEList) : "";
	$organization = HTMLtoDB(stripslashes(${"organization_".$tempGroupID}));
	$details = HTMLtoDB(stripslashes(${"details_".$tempGroupID}));
	$SchoolRemarks = HTMLtoDB(stripslashes(${"SchoolRemarks_".$tempGroupID}));
	//$SubmitType = 'INT';		# OLE
	$SubmitType = ${"intExt_".$tempGroupID};

	$startdate = $tempStartdate;
	$enddate = $tempEnddate;
	$currentSchYear = true;		# if eEnrollment record is for current school year
	
#check for activities which dont have date period and is club activites. Activity start end date will follow the club' semester
	if($RecordType!="club") {
		if ($tempClubEnrolGroupID > 0) {
			$sql = "select Semester from INTRANET_ENROL_GROUPINFO where EnrolGroupID = $tempClubEnrolGroupID";
			$TempArray= $libenroll->returnResultSet($sql);
			$YearTermID = $TempArray[0]['Semester'];
		}
	}

#check for activities which dont have date period assign start date & end date accordign to their YearTermID / Academic ID 
	if ($startdate == null || $startdate == ''){
		if ($YearTermID == null || $YearTermID == 0){
			$academicYearObj = new academic_year($AcademicYearID);
			$startdate = $academicYearObj->AcademicYearStart;
			$enddate = $academicYearObj->AcademicYearEnd;
		}
		else{
			$termObj = new academic_year_term($YearTermID);
			$startdate = $termObj->TermStart;
			$enddate = $termObj->TermEnd;
		}
	}
	
	if ($startdate == $enddate) {
		$enddate = '0000-00-00';	// no end date for OLE record if start date and end date are the same||single day event
	}
		
	# settings
	$duplicate_ole = ${"ProgramReplace_".$tempGroupID};
	$duplicate_pp = ${"StudentReplace_".$tempGroupID};
	$hours_setting = ${"hours_setting_".$tempGroupID};
	$zeroHour = ${"zeroHour_".$tempGroupID};
	$studentSetting = ${"studentSetting_".$tempGroupID};
	$leastAttendance = ${"leastAttendance_".$tempGroupID};
	$manualHours = ${"manualHours_".$tempGroupID};
	
	# settings for calculate student OLE hours
	$ParData['enrolmentID'] = $tempGroupID;
	$ParData['studentSetting'] = $studentSetting; 	
	$ParData['zeroHour'] = $zeroHour;			
	$ParData['hourSetting'] = $hours_setting;		
	$ParData['leastAttendance'] = $leastAttendance;	
	$ParData['manualHours'] = $manualHours;	
	$OLE_HoursArr = $libenroll->GET_STUDENT_OLE_HOURS($ParData, $RecordType, $returnAssociativeArr=1, '', $AcademicYearID);
		
	# check if the records in enrolment and OLE are in current school year or not
	$sql = "SELECT
				if (MONTH(StartDate) > 8,
					CONCAT(YEAR(StartDate),'-',YEAR(StartDate)+1),
					CONCAT(YEAR(StartDate)-1,'-',YEAR(StartDate))) AS Period_OLE,
				if (MONTH(NOW()) > 8,
					CONCAT(YEAR(NOW()),'-',YEAR(NOW())+1),
					CONCAT(YEAR(NOW())-1,'-',YEAR(NOW()))) AS Period_Now,
				if (MONTH('".$startdate."') > 8,
					CONCAT(YEAR('".$startdate."'),'-',YEAR('".$startdate."')+1),
					CONCAT(YEAR('".$startdate."')-1,'-',YEAR('".$startdate."'))) AS Period_Enrolment
			FROM
				{$eclass_db}.OLE_PROGRAM
			WHERE
				ProgramID = '$tempOLE_ProgramID'
			";
	$recordYearArr = $libenroll->returnArray($sql, 2);
	$Period_Now = $recordYearArr[0]['Period_Now'];
	$Period_Enrolment = $recordYearArr[0]['Period_Enrolment'];
	$Period_OLE = $recordYearArr[0]['Period_OLE'];

	if($tempOLE_ProgramID!="")	# already transfer
	{
		$currentSchYear = ($Period_Enrolment == $Period_Now)? true : false;
		
		# check if there is any OLE records in the school year of the enrolment record
		# also, in case after transfered , the OLE_PROGRAM is removed from iPortfolio OLE side. Then it will be a ProgramID in the 
		# INTRANET_GROUP and  INTRANET_ENROL_EVENTINFO but not exist in OLE_PROGRAM again, so need to create
//		$sql = "SELECT
//					ProgramID
//				FROM
//					{$eclass_db}.OLE_PROGRAM
//				WHERE
//					(Title = '".$libenroll->Get_Safe_Sql_Query($tempTitleEn)."' Or Title = '".$libenroll->Get_Safe_Sql_Query($tempTitleCh)."')
//					AND
//					StartDate = '$startdate'
//					AND
//					EndDate = '$enddate'
//					AND
//					IntExt = 'INT'
//				";
		$sql = "SELECT
					ProgramID
				FROM
					{$eclass_db}.OLE_PROGRAM
				WHERE
					ProgramID = '".$tempOLE_ProgramID."'
				";
		$OLEProgramID = $libenroll->returnVector($sql);

		if ($OLEProgramID == NULL) {
			# no current school year record
			$actionFlag = "add";
		}
		else {
			# record existed - check skip or overwite
			if($duplicate_ole=="overwrite")
			{
				$actionFlag = "update";
			}
			else	# skip
			{					
				$actionFlag = "";	
			}
			$ProgramID = $OLEProgramID[0];
		}
	}
	else
	{
		$currentSchYear = ($Period_Enrolment == $Period_Now)? true : false;
		$actionFlag = "add";
	}
		
	// omas 2015-05-18 - academic yearID and term follow $groupInfoArr 
	//$aytArr = getAcademicYearInfoAndTermInfoByDate($startdate);
	//$ayID = $aytArr[0];
	$ayID = $AcademicYearID;
	//$ytID = $aytArr[2];
	$ytID = $YearTermID;

	if($actionFlag=="add")
	{
/*
		# insert / update to OLE_PROGRAM
		$fields = "(ProgramType, Title, Category, ELE, Organization";
		$fields .= ", Details, SchoolRemarks ";
		$fields .= ",StartDate, EndDate, CreatorID, InputDate, ModifiedDate, IntExt)";
		$values = "('". $ProgramType ."', '".$title."', '".$category."', '".$ELEListStr."', '".$organization."'";
		$values .= ", '".$details."', '".$SchoolRemarks."'";
		$values .= ", '".$startdate."', '".$enddate."', $UserID, now(), now(), '".$SubmitType."')";
	
		$sql = "INSERT INTO {$eclass_db}.OLE_PROGRAM $fields VALUES $values";
		$LibPortfolio->db_db_query($sql);
		
		$ProgramID = $LibPortfolio->db_insert_id();
*/

		$objOLEPROGRAM = new ole_program();		

		$objOLEPROGRAM->setProgramType($ProgramType);
		$objOLEPROGRAM->setTitle($title);
		$objOLEPROGRAM->setCategory($category);
		$objOLEPROGRAM->setSubCategoryID(-9);
		$objOLEPROGRAM->setELE($ELEListStr);		
		$objOLEPROGRAM->setOrganization($organization);
		$objOLEPROGRAM->setDetails($details);
		$objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
		$objOLEPROGRAM->setStartDate($startdate);
		$objOLEPROGRAM->setEndDate($enddate);
		$objOLEPROGRAM->setCreatorID($UserID);
		$objOLEPROGRAM->setIntExt($SubmitType);
		$objOLEPROGRAM->setAcademicYearID($ayID);
		$objOLEPROGRAM->setYearTermID($ytID);
		$objOLEPROGRAM->setCanJoinStartDate("null");
		$objOLEPROGRAM->setCanJoinEndDate("null");
		$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["enrollmentTransfer"]);

		$ProgramID = $objOLEPROGRAM->SaveProgram();	

	}
	else if($actionFlag=="update")
	{
/*
		$sql  = "UPDATE {$eclass_db}.OLE_PROGRAM set ";
		$sql .= "Title ='". $title ."', ";
		$sql .= "Category ='". $category ."', ";
		$sql .= "ELE ='". $ELEListStr ."', ";
		$sql .= "Organization ='". $organization ."', ";
		$sql .= "Details ='". $details ."', ";
		$sql .= "SchoolRemarks ='". $SchoolRemarks ."', ";
		$sql .= "StartDate ='". $startdate ."', ";
		$sql .= "EndDate ='". $enddate ."', ";
		$sql .= "CreatorID ='". $UserID ."', ";
		$sql .= "ModifiedDate = now() ";
		$sql .= "where ProgramID=$ProgramID";
		
		$LibPortfolio->db_db_query($sql);
*/

		$objOLEPROGRAM = new ole_program($ProgramID);
		$objOLEPROGRAM->setTitle($title);
		$objOLEPROGRAM->setCategory($category);
		$objOLEPROGRAM->setELE($ELEListStr);		
		$objOLEPROGRAM->setOrganization($organization);
		$objOLEPROGRAM->setDetails($details);
		$objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
		$objOLEPROGRAM->setStartDate($startdate);
		$objOLEPROGRAM->setEndDate($enddate);		
		$objOLEPROGRAM->setCreatorID($UserID);
		$objOLEPROGRAM->setIntExt($SubmitType);
		$objOLEPROGRAM->setAcademicYearID($ayID);
		$objOLEPROGRAM->setYearTermID($ytID);
		$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["enrollmentTransfer"]);  // for safe in data migration , set the program come from  to "enrollmentTransfer"

		$ProgramID = $objOLEPROGRAM->SaveProgram();	

	}
	
	
	if($RecordType=="club")
	{
		# update INTRANET_GROUP field "OLE_ProgramID"
		$sql = "UPDATE INTRANET_ENROL_GROUPINFO SET OLE_ProgramID = '$ProgramID' WHERE EnrolGroupID = '".$tempGroupID."'";
	}
	else
	{
		# update INTRANET_ENROL_EVENTINFO field "OLE_ProgramID"
		$sql = "UPDATE INTRANET_ENROL_EVENTINFO SET OLE_ProgramID = '$ProgramID' WHERE EnrolEventID = '".$tempGroupID."'";
	}

	$LibPortfolio->db_db_query($sql);
		
			
	# get member list
	if($RecordType=="club")
	{
		$stu_sql = "select 
					a.UserGroupID,
					a.UserID, 
					c.Title,
				";

		# Eric Yip (20090618): Sync different field for Belilios
		if($sys_custom["portfolio_customized_report_bps"])
			$stu_sql .= "a.Performance,";
		else
			$stu_sql .= "a.CommentStudent,";

		$stu_sql .=	"
					a.OLE_STUDENT_RecordID,
					a.isActiveMember,
					a.Achievement
				from 
					INTRANET_USERGROUP as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
					LEFT OUTER JOIN INTRANET_ROLE as c ON a.RoleID = c.RoleID
				where 
					b.RecordType = 2 
					and 
					a.EnrolGroupID=$tempGroupID
			";
	}
	else
	{
		$stu_sql = "select 
					a.EventStudentID,
					a.StudentID, 
					a.RoleTitle,
				";

		# Eric Yip (20090618): Sync different field for Belilios
		if($sys_custom["portfolio_customized_report_bps"])
			$stu_sql .= "a.Performance,";
		else
			$stu_sql .= "a.CommentStudent,";

		$stu_sql .=	"
					a.OLE_STUDENT_RecordID,
					a.isActiveMember,
					a.Achievement
				from 
					INTRANET_ENROL_EVENTSTUDENT as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				where 
					a.RecordStatus = 2 
					AND
					a.EnrolEventID = $tempGroupID
					And
					b.RecordType = 2
				";
	}
	$stuResult = $libenroll->returnArray($stu_sql);
	
		
	### delete record in OLE if the student is no longer a member
	# get all OLE records of the group
	// syn member list of eEnrolment with OLE even if the transferring record is not current school year
//	if ($currentSchYear)
//	{
		if($RecordType=="club")
		{
			$sqlOLE = "SELECT RecordID FROM {$eclass_db}.OLE_STUDENT WHERE GroupID = '$tempGroupID' AND ProgramID = '$ProgramID' AND EnrolType = 'C' ";
		}
		else
		{
			$sqlOLE = "SELECT RecordID FROM {$eclass_db}.OLE_STUDENT WHERE GroupID = '$tempGroupID' AND ProgramID = '$ProgramID' AND EnrolType = 'A' ";
		}
		$OLERecord = $libenroll->returnVector($sqlOLE);
		
		# build Group OLE_STUDENT_RecordID array
		$GroupOLERecord = array();
		for ($j=0; $j<sizeof($stuResult); $j++)
		{
			$GroupOLERecord[] = $stuResult[$j]['OLE_STUDENT_RecordID'];
		}
		
		# get students who are not a current member of the group
		$nonMemberArr = array_diff($OLERecord, $GroupOLERecord);	//set $nonMemberArr  = data exist in $OLERecord but not in $GroupOLERecord
		$nonMemberList = implode(",", $nonMemberArr);
		
		# delete records
		$sqlOLE = "DELETE FROM {$eclass_db}.OLE_STUDENT WHERE RecordID IN ($nonMemberList)";
		$LibPortfolio->db_db_query($sqlOLE);
//	}

	### Get the Student List to be transferred
	$thisFormArr = array_keys((array)$FormChkArr[$tempGroupID]);
	$condsFormID = " And yc.YearID In (".implode(',', $thisFormArr).") ";
	$sql = "Select 
				ycu.UserID
			From
				YEAR_CLASS_USER as ycu
				Inner Join
				YEAR_CLASS as yc
				On (ycu.YearClassID = yc.YearClassID)
			Where
				yc.AcademicYearID = '".$AcademicYearID."'
				$condsFormID
			";
	$thisFormStudentIDArr = $libenroll->returnVector($sql);
	
	
	### Get the latest OLE Program Title
	$sql = "SELECT Title FROM {$eclass_db}.OLE_PROGRAM Where ProgramID = '".$ProgramID."'";
	$oleProgramInfoAry = $libenroll->returnArray($sql);
	$oleProgramTitle = $oleProgramInfoAry[0]['Title'];
	unset($oleProgramInfoAry);
	
	### add student
	$numTransferred = 0;
	for($j=0;$j<sizeof($stuResult);$j++)
	{
		list($tempUserGroupID, $tempStuID, $tempRole, $tempComment, $tempOLE_STUDENT_RecordID, $tempIsActiveMember, $tempAchievement) = $stuResult[$j];
				
		### Skip the students who are not in the selected Form
		if (!in_array($tempStuID, $thisFormStudentIDArr))
			continue;
				
		$RecordStatus = 2;
// 		$tempRole = addslashes($tempRole);
// 		$teacher_comment = addslashes($tempComment);
		$tempRole = $tempRole;
		$teacher_comment = $tempComment;
		$role = $tempRole;
		
		# double check student is duplicate or not
		$dup = "select RecordID from {$eclass_db}.OLE_STUDENT where UserID=$tempStuID and ProgramID=$ProgramID";
		$dup_result = $libenroll->returnArray($dup);
		if(sizeof($dup_result))	
		{
			if($duplicate_pp=="overwrite")	
			{
				$del_sql = "delete from {$eclass_db}.OLE_STUDENT where UserID=$tempStuID and ProgramID=$ProgramID";
				$LibPortfolio->db_db_query($del_sql);
			}
			else
			{
				continue;
			}
		}
		
		# skip non-active member if the user choose to transfer active member only
		if ($studentSetting == "activeOnly" && $tempIsActiveMember != 1)
		{
			continue;	
		}
		
		# get student OLE hours
		$hours = $OLE_HoursArr[$tempStuID];
		
		# Do not transfer zero hour record if the option is selected
		if ($zeroHour == "notTransfer" && $hours == 0)
		{
			continue;
		}
					
		$fields = "(UserID, StartDate, EndDate, Title, Details";
		$fields .= ", Category, Role, Hours, Achievement, Remark, ApprovedBy ";
		$fields .= ",RecordStatus, ProcessDate, InputDate, ModifiedDate, ELE";
		$fields .= ",Organization, ProgramID, TeacherComment, IntExt, GroupID, EnrolType,ComeFrom)";
		
		$enrolType = ($RecordType=="club")? "C" : "A";
		#cater the record which donit have start date and end date
				
		$values = "('". $tempStuID ."', '".$startdate."', '".$enddate."', '".$libenroll->Get_Safe_Sql_Query($oleProgramTitle)."', '".$libenroll->Get_Safe_Sql_Query($details)."'";
		$values .= ", '".$category."', '".$libenroll->Get_Safe_Sql_Query($role)."', '". $hours ."', '".$libenroll->Get_Safe_Sql_Query($tempAchievement)."', '". $libenroll->Get_Safe_Sql_Query($SchoolRemarks) ."', '". $UserID ."'";
		$values .= ", '".$RecordStatus."', now(), now(), now(), '".$ELEListStr."'";
		$values .= ", '".$libenroll->Get_Safe_Sql_Query($organization)."', '".$ProgramID."', '". $libenroll->Get_Safe_Sql_Query($teacher_comment) ."', '".$SubmitType."', '".$tempGroupID."', '".$enrolType."','".$ipf_cfg["OLE_STUDENT_COMEFROM"]["enrollmentTransfer"]."')";

		$sql = "INSERT INTO {$eclass_db}.OLE_STUDENT $fields VALUES $values";
		
		$LibPortfolio->db_db_query($sql);
		$OLE_STUDENT_RecordID = $LibPortfolio->db_insert_id();
		$numTransferred++;		
		
		if($RecordType=="club")
		{
			# update INTRANET_USERGROUP field "OLE_STUDENT_RecordID"
			$sql = "UPDATE INTRANET_USERGROUP SET OLE_STUDENT_RecordID = '$OLE_STUDENT_RecordID' WHERE UserGroupID = '".$tempUserGroupID."'";
		}
		else
		{
			# update INTRANET_ENROL_EVENTSTUDENT field "OLE_STUDENT_RecordID"
			$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET OLE_STUDENT_RecordID = '$OLE_STUDENT_RecordID' WHERE EventStudentID = '".$tempUserGroupID."'";
		}
		$LibPortfolio->db_db_query($sql);
	} // End loop student
	
	$numTransferredArr[] = $numTransferred;	
} // End loop Group

$numTransferredList = implode(",", $numTransferredArr);

header("Location: step4.php?AcademicYearID=$AcademicYearID&RecordType=$RecordType&ID_Str=$ID_Str&numTransferredList=$numTransferredList");

intranet_closedb();
?>