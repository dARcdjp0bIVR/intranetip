<?php

// using: 

/**************************************
 * Date：	2019-10-28 Henry
 * 			- support inactive member (ActiveMemberOnly = 2)
 * 	
 * Date：	2017-09-19	Anna
 * 			- add LEFT JOIN INTRANET_ROLE for activity webSAMS code
 * 	
 * Date:	2016-01-12	Kenneth
 * 			- Export WebSAMS Code
 * 
 * Date: 2013-06-28 (Rita)
 * Details: create this page
 **************************************/

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$lfcm = new form_class_manage();

//debug_pr($_POST);

# Get Post Value
$thisAcademicYearID = $_POST['AcademicYearID'];
//$thisYearTermID =  $_POST['YearTermID'];
$thisYearTermIDAry =  $_POST['YearTermIDAry'];
$thisDataTypeArr = $_POST['DataTypeArr'];
$thisActiveMemberOnly = $_POST['ActiveMemberOnly'];
$thisYearIDArr = $_POST['YearIDArr'];

# Header
$exportColumnLang = array();
$exportColumnLang['En'][] = 'eClass Post';
$exportColumnLang['En'][] = 'WebSAMS STA Post Code';
$exportColumnLang['Ch'][] = 'eClass 職位';
$exportColumnLang['Ch'][] = 'WebSAMS 活動職位代碼';

$ColumnPropertyArr = array(1, 1);
$exportColumn = $lexport->GET_EXPORT_HEADER_COLUMN($exportColumnLang, $ColumnPropertyArr);

### Club
$clubRoleAry = array();
if(in_array('Club',(array)$thisDataTypeArr))
{
//	$targetYearTermId = '';
//	if ($thisYearTermID == '') {
//		$targetYearTermId = array(0);
//	}
//	$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $targetYearTermId, $OrderBy='ClubCode');
	$clubInfoAry = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $thisAcademicYearID, $CategoryIDArr='', $Keyword='', $thisYearTermIDAry, $OrderBy='ClubCode');
	//debug_pr($clubInfoAry);
	if(count($clubInfoAry)>0){
		$enrolGroupIdAry = Get_Array_By_Key($clubInfoAry, 'EnrolGroupID');
		
		$conds_activeMember = '';
		if ($thisActiveMemberOnly == 1) {
			$conds_activeMember = " And iug.isActiveMember = '1' ";
		}
		else if ($thisActiveMemberOnly == 2) {
			$conds_activeMember = " And iug.isActiveMember <> '1' ";
		}
	
		$sql = "Select
						ir.Title as RoleTitle,	
						ir.WebSAMSCode		
				From
						INTRANET_USERGROUP as iug
						Inner Join INTRANET_ROLE as ir On (iug.RoleID = ir.RoleID) 
						Inner join INTRANET_ENROL_GROUPCLASSLEVEL ieg on iug.EnrolGroupID = ieg.EnrolGroupID	
				Where
						ir.RecordType = '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'
						And iug.EnrolGroupID In ('".implode("','", (array)$enrolGroupIdAry)."')
						AND ieg.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
						$conds_activeMember
				Group By 
						BINARY ir.Title
				Order By
						BINARY ir.Title
				";
		
		$clubArr = $libenroll->returnArray($sql);
//		$clubRoleAry = $libenroll->returnVector($sql);
//		
//		for($i=0;$i<count($clubArr);$i++){
//			$clubCodeAry[$i] =  $clubArr[$i]['WebSAMSCode'];
//		}
	}
}
//debug_pr($clubArrAmend);
### Activity
$activityRoleAry = array();
if(in_array('Activity',(array)$thisDataTypeArr))
{
	$activityInfoAry = $libenroll->GET_EVENTINFO_LIST($ParCategory="", $EnrolGroupID='', $thisAcademicYearID);
	$enrolEventIdAry = Get_Array_By_Key($activityInfoAry, 'EnrolEventID');	
	
	$conds_activeMember = '';
	if ($thisActiveMemberOnly == 1) {
		$conds_activeMember = " And ies.isActiveMember = '1' ";
	}
	else if ($thisActiveMemberOnly == 2) {
		$conds_activeMember = " And ies.isActiveMember <> '1' ";
	}
	
	$sql = "Select
					ies.RoleTitle,
					ir.WebSAMSCode as WebSAMSCode
			From
					INTRANET_ENROL_EVENTSTUDENT ies
					LEFT OUTER JOIN INTRANET_ROLE AS ir ON (ir.Title = ies.RoleTitle)
					Inner Join INTRANET_ENROL_EVENTCLASSLEVEL iee on iee.EnrolEventID = ies.EnrolEventID
			Where
					ies.EnrolEventID In ('".implode("','", (array)$enrolEventIdAry)."')
					AND iee.ClassLevelID In ('".implode("','", (array)$thisYearIDArr)."')
					AND ir.RecordType= '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'
					$conds_activeMember
					Group By
					BINARY ies.RoleTitle
					Order By
					BINARY ies.RoleTitle
			";
	$activityArr=$libenroll->returnArray($sql);
	//$activityRoleAry = $libenroll->returnVector($sql);
}

### Combine all roles from Clubs and Activities
if(empty($clubArr)){
	$result = $activityArr;
}else if(empty($activityArr)){
	$result = $clubArr;
}else{
	$result = array_merge_recursive($clubArr, $activityArr);
}



//$allRoleAry = array_values(array_remove_empty(array_unique(array_merge($clubRoleAry, $activityRoleAry))));

//debug_pr($allRoleAry);
//$numOfRole = count($result);
//
//$exportDataAry = array();
//$counter_i = 0;
//
//for ($i=0; $i<$numOfRole; $i++) {
//	$_roleTitle = $allRoleAry[$i];
//	
//	$exportDataAry[$counter_i][] = $_roleTitle;
//	$counter_i++;
//}

$export_content = $lexport->GET_EXPORT_TXT($result, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);

//debug_pr($exportColumn);
//$export_content = $lexport->GET_EXPORT_TXT_WITH_REFERENCE($exportDataAry, $exportColumn);
//debug_pr($export_content);
intranet_closedb();

$filename = 'post_mapping_'.date('Ymd_His').'.csv';
$lexport->EXPORT_FILE($filename, $export_content);
?>