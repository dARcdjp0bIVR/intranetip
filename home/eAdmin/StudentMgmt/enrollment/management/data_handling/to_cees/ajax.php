<?php
//using: 

@set_time_limit(21600);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system_kis/libSDAS.php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_cees_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$libSDAS = new libSDAS();
$JSONobj = new JSON_obj();

$Action = trim(urldecode(stripslashes($_REQUEST['Action'])));
// debug_pr($_REQUEST);die();
switch($Action){
	case 'SyncReport':
		if($type!=2){
			$rID = $ReportID;
			$report = ($type==0 ? $libSDAS->getInterSchoolECA($academicYearId,$rID) : $libSDAS->getIntraSchoolECA($academicYearId,$rID));
			$isSubmit = $report['SubmitStatus'] == '1';
			$startDate = $report['StartDate'];
			$endDate = $report['EndDate'];
		} else {
			$rID = $ReportID;
			$report = $libSDAS->getMonthlyECA($academicYearId, $rID);
			if(empty($report)){
				$yearName = getAcademicYearByAcademicYearID($academicYearId);
				$yearName = substr($yearName, 0, 4);
				$month = $rID;
				if($month > 9)
					$yearName = strval($yearName) + 1;
					$yearName = $nextYr;
					$reportID = $libSDAS->initMonthlyECA($academicYearId, $month, date($yearName.'-'.($month<9?$month:'0'.$month).'-01'));
					$report = $libSDAS->getMonthlyECA($academicYearId, $month);
			}
			$rID = $report['ReportID'];
			$year = $report['Year'];
			$month = (strval($report['Month'])<9?'0'.$report['Month']:$report['Month']);
			$startDate = date($year.'-'.$month.'-01');
			$endDate = date($year.'-'.$month.'-t');
		}
		// 1. Erase Existed Record
		$result = array();
		switch($type){
			case '0':
				$result['Delete'] = $libSDAS->clearInterSchoolECA($rID);
				break;
			case '1':
				$result['Delete'] = $libSDAS->clearIntraSchoolECA($rID,$exceptAward = true);
				break;
			case '2':
				$result['Delete'] = $libSDAS->clearMonthlyECA($rID,$exceptAward = true);
				break;
		}
		// 2. Insert Sync Record
		$recordAry = getSyncEnrolmentRecord($libSDAS, $type, $startDate, $endDate);
		foreach($recordAry as &$record) $record['SDAS_Category'] = mapCategory($record['SDAS_Category'], $type);
// 		debug_pr($recordAry);die();
		$colMap = array(
				"SDAS_Category" => "Section",
				"EventDate" => "RecordDate",
				"EventTitle" => "Event",
				"Location" => "Venue",
				"Guest" => "Guest",
// 				"Total" => "TotalParticipant",
				"ParticipatedSchool" => "ParticipatedSchool",
				"ActivityTarget" => "Target"
		);
		$colMap["Total"] = ($type==0) ? "TotalParticipant" : "Participant";
		$insertDataAry = array();
		foreach($recordAry as $rc){
			$row = array();
			foreach($colMap as $key => $col){
				if($rc[$key] != '')	$row[$col] = $rc[$key];
			}
			$row['ReportID'] = $rID;
			$insertDataAry[] = $row;
		}
// 		debug_pr($insertDataAry);
		switch($type){
			case '0':
				$result['Insert'] = $libSDAS->insertInterSchoolECARecord($rID, $insertDataAry);
				$result['Update'] = $libSDAS->updateInterSchoolECA($rID, array());
				break;
			case '1':
				$result['Insert'] = $libSDAS->insertIntraSchoolECARecord($rID, $insertDataAry);
				$result['Update'] = $libSDAS->updateIntraSchoolECA($rID, array());
				break;
			case '2':
				$result['Insert'] = $libSDAS->insertMonthlyECARecord($rID, $insertDataAry);
				$result['Update'] = $libSDAS->updateMonthlyECA($rID, array());
				break;
		}
		// 3. Refresh
	case 'RefreshReport':
		if($type!=2){
			$rID = $ReportID;
			$report = ($type==0 ? $libSDAS->getInterSchoolECA($academicYearId,$rID) : $libSDAS->getIntraSchoolECA($academicYearId,$rID));
		} else {
			$rID = $month;
			$report = $libSDAS->getMonthlyECA($academicYearId, $month);
		}
		$isSubmit = $report['SubmitStatus'] == '1';
		$isDraft= $report['DraftStatus'] == '1';
		if(!$isSubmit && !$isDraft){
			$_updateBtn = "<input name='button' type='button' value='".$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync']."' onClick='js_SyncECA(\"".$type."\",\"".$rID."\")' $ButtonClass>";
		} else {
			$_updateBtn = $Lang['CEES']['Management']['SchoolActivityReport']['Hint']['AlreadySubmitted'];
		}
		$ModifiedDate = $report['ModifiedDate'] ? $report['ModifiedDate']: $Lang['General']['EmptySymbol'];
		echo $JSONobj->encode(array($_updateBtn,$ModifiedDate));
		break;
	case 'initTable':
		$tableItem = array();
		if($month== ''){
			$month = date('m');
		}
		$ECAReport = array(
			BuildMultiKeyAssoc($libSDAS->getInterSchoolECA($academicYearId),'ReportID'),
			BuildMultiKeyAssoc($libSDAS->getIntraSchoolECA($academicYearId),'ReportID'),
			$libSDAS->getMonthlyECA($academicYearId, $month)
		);
		$tableItem[0][0] = $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'] . ')';
		$tableItem[1][0] = $Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] . '(' . $Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'] . ')';
		$tableItem[2][0] = $Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title'];
		$tableHtml = '';
		for($i=0; $i<3; $i++){
			if($i!=2 && count($ECAReport[$i])<=0){
				$tableItem[$i][1] = $Lang['General']['EmptySymbol'];
				$tableItem[$i][2] = $Lang['General']['EmptySymbol'];
				$tableItem[$i][3] = $Lang['General']['EmptySymbol'];
			} else {
				if($i!=2){
					if($ReportID[$i]){
						$rID = $ReportID[$i];
					} else {
						$firstReport = current($ECAReport[$i]);
						$rID = $firstReport['ReportID'];
					}
				}
				$ECAReportListData = array();
				if($i!=2){
					foreach($ECAReport[$i] as $id => $ecar){
						$ECAReportListData[] = array($id, $ecar['StartDate'] . ' ' . $Lang['General']['To'] . ' ' . $ecar['EndDate']);
					}
				} else {
					$yearName = getAcademicYearByAcademicYearID($academicYearId);
					$yearName = substr($yearName,0,4);
					$nextYr = strval($yearName) + 1;
					for($m=1;$m<=12;$m++){
						$selected = ($m==$selectedMonth) ? 'selected' : '';
						$str = ($m>=9? $yearName:$nextYr).'/'.($m<10?'0':'').$m;
						$ECAReportListData[] = array($m, $str);
						if($m==$month) $rID = $m;
					}
				}
				$onChange = ($i!=2) ? 'js_SchoolECA_Selection('.$i.',this.value);' : 'js_MonthlyECA_Month_Selection(this.value);';
				$ECAReportSelect = getSelectByArray($ECAReportListData, 'name="ReportID['.$i.']" id="ReportID_'.$i.'" onChange="'.$onChange.'"', $rID, 0, $noFirst=1, $FirstTitle="");
				
				$isSubmit = $ECAReport[$i][$rID]['SubmitStatus'] == '1';
				$isDraft = $ECAReport[$i][$rID]['DraftStatus']=='1';
				if($i==2 && empty($ECAReport[$i]) ){
					$_updateBtn = $Lang['General']['EmptySymbol'];
				}else if(!$isSubmit && !$isDraft){
					$_updateBtn = "<input name='button' type='button' value='".$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync']."' onClick='js_SyncECA(\"".$i."\",\"".$rID."\")' $ButtonClass>";
				} else {
					$_updateBtn = $Lang['CEES']['Management']['SchoolActivityReport']['Hint']['AlreadySubmitted'];
				}
				$tableItem[$i][1] = $ECAReportSelect;
				$tableItem[$i][2] = $_updateBtn;
				$tableItem[$i][3] = $ECAReport[$i][$rID]['ModifiedDate'] ? $ECAReport[$i][$rID]['ModifiedDate']: $Lang['General']['EmptySymbol'];
			}
			$tableHtml .= '<tr valign="middle" class="tablerow'. ($i%2 +1).'">';
				foreach($tableItem[$i] as $k => $item){
					$tableHtml .= '<td nowrap="nowrap" align="center" id="contentRow'.$i.'_'.$k.'">' . $item . '</td>';
				}
			$tableHtml .= '</tr>';
		}
		
		?>
		<table width="100%" border="0" cellpadding="4" cellspacing="1" bgcolor="#CCCCCC">
        	<tr class="tabletop">
        		<td nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetReport']?></td>
        		<td nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SelectReportPeriod']?></td>
        		<td nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync']?></td>
        		<td nowrap="nowrap" align="center" class="tabletopnolink"><?=$Lang['CEES']['Management']['SchoolActivityReport']['Category']['LastModificatedDate']?></td>
        	</tr>
		    <?=$tableHtml?>
        </table>
		<?php
		break;
	default:
		break;
}

intranet_closedb();

function getSyncEnrolmentRecord($db, $ReportType, $startDate, $endDate){
	$CategoryAry= array(
			'O1' => 'ProfessionalDevelopmentAndTrainning',
			'O2' => 'LearnAndTeach',
			'O3' => 'StudentSupport',
			'I1' => array('SchoolAdministration','ProfessionalDevelopmentAndTrainning','InterflowAndSharing'),
			'I2' => array('InsideSchoolActivity', 'OutsideSchoolActivity'),
			'I3' => array('ParentSchoolActivity','ParentEducation','OtherSupportActivity','Others')
	);
	$allowSyncMap = array(
		'0' => 'O',
		'1' => 'I',
		'2' => 'M'
	);
	
	$table = 'INTRANET_ENROL_EVENTINFO';
	$joinTable = 'INTRANET_ENROL_EVENT_DATE';
	$joinTable2 = 'INTRANET_ENROL_EVENTSTUDENT';
	
	$cols = 'a.EventTitle, a.SDAS_Category, a.SDAS_AllowSync, GROUP_CONCAT(DISTINCT DATE_FORMAT(b.ActivityDateStart, "%Y-%m-%d")) AS EventDate, a.Location, IF(a.TotalParticipant NOT IN ("0", ""), a.TotalParticipant, COUNT(*)) AS Total,
			 a.ActivityTarget, a.Guest, a.ParticipatedSchool';
	
	// Staff Event
	$join2 = "LEFT OUTER JOIN $joinTable2 c
	ON a.EnrolEventID = c.EnrolEventID";
	
	
	switch($ReportType){
		case '0':
			$targetCategory = "'O1_$CategoryAry[O1]'";
			break;
		case '1':
			$targetCategory = "";
			for($i=1;$i<=1;$i++){
				foreach($CategoryAry['I'.$i] as $cat) $targetCategory .= "'I{$i}_{$cat}',";
			}
			if($targetCategory!='') $targetCategory = substr($targetCategory,0,-1);
			break;
		case '2':
			$targetCategory = "";
			$targetCategory .= "'O1_$CategoryAry[O1]',";
			for($i=1;$i<=1;$i++){
// 				foreach($CategoryAry['O'.$i] as $cat) $targetCategory .= "'O{$i}_{$cat}',";
				foreach($CategoryAry['I'.$i] as $cat) $targetCategory .= "'I{$i}_{$cat}',";
			}
			if($targetCategory!='') $targetCategory = substr($targetCategory,0,-1);
			break;
					
	}
	$conds = " a.SDAS_AllowSync LIKE '%{$allowSyncMap[$ReportType]}%' AND a.SDAS_Category IN ($targetCategory) ";
	$conds .= " AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') >= '$startDate' AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') <= '$endDate' ";
	
	$sql = "SELECT $cols FROM $table a
	INNER JOIN $joinTable b
	ON a.EnrolEventID = b.EnrolEventID
	$join2
	WHERE $conds
	Group By a.EnrolEventID
	";
	
	$recordAry1 = $db->returnArray($sql);
// 	debug_pr($sql);
	// Student Event
	//$join2 = "INNER JOIN $joinTable2 c
	$join2 = "LEFT JOIN $joinTable2 c
	ON a.EnrolEventID = c.EnrolEventID";
	
	switch($ReportType){
		case '0':
			$targetCategory = "'O1_$CategoryAry[O1]','O2_$CategoryAry[O2]','O3_$CategoryAry[O3]'";
			break;
		case '1':
			$targetCategory = "";
			for($i=2;$i<=3;$i++){
				foreach($CategoryAry['I'.$i] as $cat) $targetCategory .= "'I{$i}_{$cat}',";
			}
			if($targetCategory!='') $targetCategory = substr($targetCategory,0,-1);
			break;
		case '2':
			$targetCategory = "";
			$targetCategory .= "'O1_$CategoryAry[O1]','O2_$CategoryAry[O2]','O3_$CategoryAry[O3]',";
			for($i=2;$i<=3;$i++){
// 				foreach($CategoryAry['O'.$i] as $cat) $targetCategory .= "'O{$i}_{$cat}',";
				foreach($CategoryAry['I'.$i] as $cat) $targetCategory .= "'I{$i}_{$cat}',";
			}
			if($targetCategory!='') $targetCategory = substr($targetCategory,0,-1);
			break;
	}
	$conds = " a.SDAS_AllowSync LIKE '%{$allowSyncMap[$ReportType]}%' AND a.SDAS_Category IN ($targetCategory) ";
	$conds .= " AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') >= '$startDate' AND DATE_FORMAT(b.ActivityDateStart, '%Y-%m-%d') <= '$endDate' ";
	
	$sql = "SELECT $cols FROM $table a
	INNER JOIN $joinTable b
	ON a.EnrolEventID = b.EnrolEventID
	$join2
	WHERE $conds
	Group By a.EnrolEventID
	";
// 	debug_pr($sql);
	$recordAry2 = $db->returnArray($sql);
	$recordAry = array_merge($recordAry1, $recordAry2);
	return $recordAry;
}
function mapCategory($catString, $type){
	$interSchoolSectionFix = $type=='2' ? 1 : 0;
	$CategoryAry= array(
			'O1_ProfessionalDevelopmentAndTrainning' => ($type=='0'? '1' : '1_1'),
			'O2_LearnAndTeach' => ($type=='0'? '2' : '2_1'),
			'O3_StudentSupport'  => ($type=='0'? '3' : '3_1'),
			'I1_SchoolAdministration'=>'1_'.($interSchoolSectionFix+1),'I1_ProfessionalDevelopmentAndTrainning'=>'1_'.($interSchoolSectionFix+2),'I1_InterflowAndSharing'=>'1_'.($interSchoolSectionFix+3),
			'I2_InsideSchoolActivity'=> '2_'.($interSchoolSectionFix+1), 'I2_OutsideSchoolActivity' => '2_'.($interSchoolSectionFix+2),
			'I3_ParentSchoolActivity' => '3_'.($interSchoolSectionFix+1),'I3_ParentEducation' => '3_'.($interSchoolSectionFix+2),'I3_OtherSupportActivity' => '3_'.($interSchoolSectionFix+3),'I3_Others'  => '3_'.($interSchoolSectionFix+4)
	);
	return $CategoryAry[$catString];
}
?>