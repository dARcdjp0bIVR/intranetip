<?php 
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/cust/student_data_analysis_system_kis/libSDAS.php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_cees_".$intranet_session_language.".php");


### Cookies handling
$arrCookies = array();

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
}
else
{
	updateGetCookies($arrCookies);
}


intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();
$libSDAS = new libSDAS();
$lay = new academic_year();

# Check access right
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
$accessRight = $libSDAS->getAssessmentStatReportAccessRight();

if ($plugin['eEnrollmentLite'] || !$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster && !$accessRight['admin']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Tab
$curTab = 'to_cees';
$data_batch_process_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_sp/step1.php";
$transer_to_websams_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_websams/step1.php";
$transer_to_ole_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_ole/step1.php?clearCoo=1";
$data_deletion_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/data_deletion/data_deletion.php";
$transer_to_cees_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_cees/step1.php";
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], $data_batch_process_link, $curTab=='to_sp');
if($_SESSION["platform"] != "KIS"){
	if($plugin['iPortfolio']) {
		$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], $transer_to_ole_link, $curTab=='to_ole');
	}
	if(!$sys_custom['project']['NCS']){
		$TAGS_OBJ[] = array($Lang['eEnrolment']['data_handling']['export_websams'], $transer_to_websams_link, $curTab=='to_websams');
	}
}
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClearAllEnrol'], $data_deletion_link, $curTab=='data_deletion');
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_CEES'], $transer_to_cees_link, $curTab=='to_cees');

$academic_year_arr = $lay->Get_All_Year_List();
$AcademicYearSelection = '<label>'.$Lang['SDAS']['toCEES']['SchoolYear'].'</label>'."\r\n";;
$AcademicYearSelection .= getSelectAcademicYear('AcademicYearID', $tag='onchange="js_init_Table();"', $noFirst=0, $noPastYear=0);
$AcademicYearSelection .= '&nbsp;<a href="javascript:js_init_Table();" id="refresh_btn" class="tablelink">'.$i_general_refresh.'</a>';
$AcademicYearSelection .= '<span id="ajax_loading"></span>';

$linterface->LAYOUT_START();

?>

<script language="javascript">
<?
if($ReturnMsg != "") {
	echo "Get_Return_Message('".$ReturnMsg."')\n";	
}
?>

$(document).ready( function () {
	// init table items
	js_init_Table();
});

function js_init_Table(){
	let academicYearId = $('#AcademicYearID').val();
	$.ajax({
		url: 'ajax.php',
		method: 'POST',
		data:{'Action':'initTable','academicYearId':academicYearId},
		success: function(html){
			$('#Content_Block').html(html);
		}
	});
}

function js_SyncECA(type,id){
	let confirm = window.confirm('<?=$Lang['CEES']['Managemnet']['SchoolActivityReport']['Hint']['SyncEnrolmentAlert']?>');
	if(confirm){
		let academicYearId = $('#AcademicYearID').val();
		$.ajax({
			url: 'ajax.php',
			method: 'POST',
			dataType: 'json',
			data:{'Action': 'SyncReport','academicYearId':academicYearId, 'ReportID': id, 'type': type},
			success: function(data){
// 				console.log(data);return;
				$('#contentRow'+type+'_2').html(data[0]);
				$('#contentRow'+type+'_3').html(data[1]);
			}
		});
	}
}

function js_SchoolECA_Selection(type,id){
	let academicYearId = $('#AcademicYearID').val();
	$.ajax({
		url: 'ajax.php',
		method: 'POST',
		dataType: 'json',
		data:{'Action': 'RefreshReport','academicYearId':academicYearId, 'ReportID': id, 'type': type},
		success: function(data){
			$('#contentRow'+type+'_2').html(data[0]);
			$('#contentRow'+type+'_3').html(data[1]);
		}
	});
}

function js_MonthlyECA_Month_Selection(id){
	let academicYearId = $('#AcademicYearID').val();
	$.ajax({
		url: 'ajax.php',
		method: 'POST',
		dataType: 'json',
		data:{'Action': 'RefreshReport','academicYearId':academicYearId, 'month': id, 'type': 2},
		success: function(data){
			$('#contentRow2_2').html(data[0]);
			$('#contentRow2_3').html(data[1]);
		}
	});
}
</script>
<form name="form1" method="post">
<div class="table_board">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td><?//= $linterface->GET_STEPS($STEPS_OBJ) ?>
			<?=$AcademicYearSelection?>
		</td>
	</tr>
	<tr>
	    <td id='Content_Block'>
	    </td>
	</tr>
</table>
</div>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>