<?php
//using: 

##### Change Log [Start] #####
#
#   Date    :   2020-01-16  Tommy
#               hide to SLP & to WebSMAS for KIS client
#
#	Date	:	2015-06-01	Omas
#				Merged to Data Handling , added Tab
#
#	Date	:	2011-09-08	YatWoon
# 				lite version should not access this page 
#
#	Date	:	2011-07-08 [Henry Chow]
#				display "Last Transferred" next to "Semester" filter
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

### Cookies handling
$arrCookies = array();
$arrCookies[] = array("eEnrol_management_transfer2SP_AcademicYearID", "AcademicYearID");
$arrCookies[] = array("eEnrol_management_transfer2SP_YearTermID", "YearTermID");
$arrCookies[] = array("eEnrol_management_transfer2SP_DataTypeArr", "DataTypeText");
$arrCookies[] = array("eEnrol_management_transfer2SP_ActiveMemberOnly", "ActiveMemberOnly");
$arrCookies[] = array("eEnrol_management_transfer2SP_YearIDArr", "YearIDText");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$AcademicYearID = '';
	$YearTermID = 0;
	$DataTypeArr = '';
	$ActiveMemberOnly = '';
	$YearIDArr = '';
}
else 
{
	updateGetCookies($arrCookies);
	$DataTypeArr = explode(',', $DataTypeText);
	$YearIDArr = explode(',', $YearIDText);
}


intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

# Check access right
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
if ($plugin['eEnrollmentLite'] || !$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Tab
$curTab = 'to_sp';
$data_batch_process_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_sp/step1.php";
$transer_to_websams_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_websams/step1.php";
$transer_to_ole_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_ole/step1.php?clearCoo=1";
$data_deletion_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/data_deletion/data_deletion.php";
$transer_to_cees_link = $PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/management/data_handling/to_cees/step1.php";
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], $data_batch_process_link, $curTab=='to_sp');
if($_SESSION["platform"] != "KIS"){
    if($plugin['iPortfolio']) {
    	$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_OLE'], $transer_to_ole_link, $curTab=='to_ole');
    }
    if(!$sys_custom['project']['NCS']){
    $TAGS_OBJ[] = array($Lang['eEnrolment']['data_handling']['export_websams'], $transer_to_websams_link, $curTab=='to_websams');
    }
}
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClearAllEnrol'], $data_deletion_link, $curTab=='data_deletion');
if($plugin['SDAS_module']['KISMode']){
	$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_CEES'], $transer_to_cees_link, $curTab=='to_cees');
}

$STEPS_OBJ = $libenroll->GET_STEPS_OBJ_ARR($Lang['eEnrolment']['Transfer_to_SP']['StepArr'], 1);
		
$linterface->LAYOUT_START();

echo $libenroll_ui->Get_Management_TransferToSP_Step1_UI($AcademicYearID, $YearTermID, $DataTypeArr, $ActiveMemberOnly, $YearIDArr);

?>

<script language="javascript">
<?
if($ReturnMsg != "") {
	echo "Get_Return_Message('".$ReturnMsg."')\n";	
}
?>

var jsCurAcademicYearID = '<?=$AcademicYearID?>';
var jsCurYearTermID = '<?=$YearTermID?>';

$(document).ready( function () {
	jsCurAcademicYearID = $('select#AcademicYearID').val();
	js_Reload_Term_Selection(jsCurAcademicYearID);
});

function js_Changed_Academic_Year_Selection(jsValue)
{
	jsCurAcademicYearID = jsValue;
	js_Reload_Term_Selection(jsValue);
}

function js_Reload_Term_Selection(jsAcademicYearID)
{
	$('div#YearTermSelectionDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../../../ajax_reload.php", 
		{ 
			Action: 'Term_Selection',
			SelectionID: 'YearTermID',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsCurYearTermID,
			NoFirst: 0,
			AllTitle: '<?=$Lang['eEnrolment']['WholeYear']?>',
			OnChange: 'js_Relaod_SP_Last_Transfer_Date('+jsAcademicYearID+', this.value)'
		},
		function(ReturnData)
		{
			if ($('select#YearTermID').val() != jsCurYearTermID)
			{
				$('select#YearTermID').val($('select#YearTermID :first-child').val());
				jsCurYearTermID = $('select#YearTermID').val();
			}
			js_Relaod_SP_Last_Transfer_Date(jsAcademicYearID, jsCurYearTermID);
		}
	);
}

function js_Relaod_SP_Last_Transfer_Date(jsAcademicYearID, jsYearTermID) {
	$('div#LastTransferDiv').html('<?/*=$linterface->Get_Ajax_Loading_Image()*/?>').load(
		"../../../ajax_reload.php", 
		{ 
			Action: 'Reload_SP_Last_Transfer_Date',
			SelectionID: 'YearTermID',
			AcademicYearID: jsAcademicYearID,
			YearTermID: jsYearTermID,
			NoFirst: 0,
			AllTitle: '<?=$Lang['eEnrolment']['WholeYear']?>',
			WithLastTransferDate: 1,
			OnChange: 'js_Relaod_SP_Last_Transfer_Date('+jsAcademicYearID+')'
		},
		function(ReturnData)
		{
			$('#LastTransferDiv').val(ReturnData);
		}
	);
}


function js_Check_Form()
{
	// Club and Activity
	var jsHasCheckDataType = false;
	$('.DataTypeChk').each( function() {
		if ($(this).attr('checked') == true)
		{
			jsHasCheckDataType = true;
			return false;
		}
	})
	if (jsHasCheckDataType == false)
	{
		alert('<?=$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['SelectDataSource']?>');
		$('input#SelectAll_DataTypeChk').focus();
		return false;
	}
	
	// Form
	var jsHasCheckForm = false;
	$('.FormChk').each( function() {
		if ($(this).attr('checked') == true)
		{
			jsHasCheckForm = true;
			return false;
		}
	})
	if (jsHasCheckForm == false)
	{
		alert('<?=$Lang['eEnrolment']['Transfer_to_SP']['jsWarningArr']['SelectForm']?>');
		$('input#SelectAll_TargetFormChk').focus();
		return false;
	}
	
	$('form#form1').attr('action', 'step2.php').submit();
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>