<?php
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

# Check access right
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
if (!$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html("popup.html");

$AcademicYearID = $_REQUEST['AcademicYearID'];
$YearTermID = $_REQUEST['YearTermID'];
$DataTypeArr = explode(',', $_REQUEST['DataTypeArr']);
$ActiveMemberOnly = $_REQUEST['ActiveMemberOnly'];
$YearIDArr = explode(',', $_REQUEST['YearIDArr']);
$DataNameLang = $_POST['DataNameLang'];
echo $libenroll_ui->Get_Management_TransferToSP_Step2_PrintUI($AcademicYearID, $YearTermID, $DataTypeArr, $ActiveMemberOnly, $YearIDArr, $DataNameLang);

include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
intranet_closedb();
?>