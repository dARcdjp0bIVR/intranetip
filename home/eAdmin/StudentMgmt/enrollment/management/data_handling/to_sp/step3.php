<?php
//using: 
##########################
# Date	: 2015-07-08 Omas updated left menu
##########################
$PATH_WRT_ROOT = "../../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

# Check access right
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
if (!$plugin['eEnrollment'] || (!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page setting
$CurrentPageArr['eEnrolment'] = 1;		# top menu 
$CurrentPage = "PageDataHandling";		# left menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$TAGS_OBJ[] = array($eEnrollmentMenu['data_handling_to_SP'], "", 1);
		
$linterface->LAYOUT_START();

$ReturnMsgKey = $_REQUEST['ReturnMsgKey'];
echo $libenroll_ui->Get_Management_TransferToSP_Step3_UI($ReturnMsgKey);

?>

<script language="javascript">
function js_Go_Transfer_Other_Records()
{
	window.location = 'step1.php';
}
</script>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>