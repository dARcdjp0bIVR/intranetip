<?
##### Change Log [Start] #####
#
#	Date	:	2015-06-01	Omas
# 				Create this page
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_opendb();
$libenroll = new libclubsenrol();

$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
intranet_closedb();
if((!$isEnrolAdmin && !$isEnrolMaster))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if($plugin['eEnrollmentLite']){
	header("Location: data_deletion/data_deletion.php");
	exit;
}
else{
	header("Location: to_sp/step1.php");
	exit;	
}
?>

