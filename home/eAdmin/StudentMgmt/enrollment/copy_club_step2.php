<?php
// Using: Ivan

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
if (!$libenroll->hasAccessRight($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");


$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	
	if (!$isEnrolAdmin)
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$linterface = new interface_html();
	
	$FromAcademicYearID = $_REQUEST['FromAcademicYearID'];
	$ToAcademicYearID = $_REQUEST['ToAcademicYearID'];
	$TermMappingArr = $_REQUEST['TermMappingArr'];
	$CopyClubArr = $_REQUEST['CopyClubArr'];
	$CopyMemberArr = $_REQUEST['CopyMemberArr'];
	$CopyActivityArr = $_REQUEST['CopyActivityArr'];
	
	# tags 
    $tab_type = "club";
    $current_tab = 1;
    include_once("management_tabs.php");
  
    $linterface->LAYOUT_START();        
    echo $libenroll_ui->Copy_Club_Step2_UI($FromAcademicYearID, $ToAcademicYearID, $TermMappingArr, $CopyClubArr, $CopyMemberArr, $CopyActivityArr);
			
?>

<script language="javascript">
function js_Go_Back()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'copy_club_step1.php';
	ObjForm.submit();
}

function js_Cancel()
{
	window.location = "group.php";
}

function js_Submit_Form()
{
	var ObjForm = document.getElementById('form1');
	ObjForm.action = 'copy_club_step2_update.php';
	ObjForm.submit();
}
</script>

<?
	$linterface->LAYOUT_STOP();
	intranet_closedb();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>