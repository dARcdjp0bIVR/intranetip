<?php
# using: yat

$PATH_WRT_ROOT = "../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_cust.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lg = new libgroup();
$lclass = new libclass();
$linterface = new interface_html();
$lc = new libclubsenrol_cust();
     
if($showTarget=="class" && isset($targetClassID) && $targetClassID!="")
{
	$YearClassObj = new year_class($targetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
	$studentNameArr = $YearClassObj->ClassStudentList;
	$this_ClassNameDisplay = $YearClassObj->Get_Class_Name();
} 
else if($showTarget=="club" && isset($targetClub) && $targetClub!="") 
{
	$club_info = $libenroll->GET_GROUPINFO($targetClub);
	$this_ClubDisplay = $club_info['Title'];
	$studentIDArr = array();
	$studentNameArr = array();
	$studentNameArr = $libenroll->Get_Club_Member_Info(array($targetClub), $PersonTypeArr=array(2), Get_Current_Academic_Year_ID(), $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
} 
else if($showTarget=="house" && isset($targetHouse) && $targetHouse!="") 
{
	$house_info = $libenroll->getGroupInfo($targetHouse);
	$this_HouseDisplay = $house_info[0]['Title'];
	$IndicatorPrefix = '<span class="tabletextrequire">';
	$IndicatorSuffix = "</span>";
	
	$temp_stuInfo = $lg->returnGroupUser4Display($targetHouse, 2);
	for($i=0; $i<sizeof($temp_stuInfo); $i++) {
	$studentIDArr[] = $temp_stuInfo[$i]['UserID'];
	$studentNameArr[$i]['UserID'] = $temp_stuInfo[$i]['UserID'];
	
	$studentNameArr[$i]['StudentName'] = (empty($temp_stuInfo[$i]['ClassName']) ? $IndicatorPrefix."^".$IndicatorSuffix:"") . $temp_stuInfo[$i][3];
	$studentNameArr[$i]['ClassName'] = $temp_stuInfo[$i]['ClassName'];
	$studentNameArr[$i]['ClassNumber'] = $temp_stuInfo[$i]['ClassNumber'];
	}
}

$table_rows = "";
if(!empty($studentNameArr))
{
	$UserID_str = "";
	# retrieve string of StudentID
	foreach($studentNameArr as $k=>$d)
	{
		$UserID_str .= $d['UserID'] . ",";
	}
	$UserID_str = substr($UserID_str, 0 , strlen($UserID_str)-1);
	
	# retrieve club perform 
	$result = $lc->returnClubPerformanceByUserIDs($UserID_str);
	$performance_ary = array();
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['UserID']] = $d['performance'] ? $d['performance'] : 0;
	}
			
	# retrieve activity perform 
	$result = $lc->returnActivityPerformanceByUserIDs($UserID_str);
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
			$performance_ary[$d['StudentID']] += $d['performance'] ? $d['performance'] : 0;
	}
	
	# retrieve adjuestment
	$result = $lc->returnAdjustmentByUserIDs($UserID_str);
	$adj_ary = array();
	if(!empty($result))
	{
 		foreach($result as $k=>$d)	
 		{
			$adj_ary[$d['UserID']]['Adjustment'] = $d['Adjustment'];
			$adj_ary[$d['UserID']]['DateModified'] = $d['DateModified'];
		}
	}
	
	foreach($studentNameArr as $k=>$d)
	{
		$this_rows = "";

		$this_StudentID = $d['UserID'];
		$this_ClassName = $d['ClassName'] ? $d['ClassName'] : $Lang['General']['EmptySymbol'];
		$this_ClassNumber = $d['ClassNumber'] ? $d['ClassNumber'] : $Lang['General']['EmptySymbol'];
		$this_StudentName = $d['StudentName'] ? $d['StudentName'] : $Lang['General']['EmptySymbol'];
		$this_Performance = $performance_ary[$this_StudentID] ? $performance_ary[$this_StudentID] : 0;
		$this_adjustment = $adj_ary[$this_StudentID]['Adjustment'] ? $adj_ary[$this_StudentID]['Adjustment'] : 0;
		$this_last_modified = $adj_ary[$this_StudentID]['DateModified'] ? $adj_ary[$this_StudentID]['DateModified'] : $Lang['General']['EmptySymbol'];
		$this_total = $this_Performance + $this_adjustment;

		$this_rows .= "<tr>";
		$this_rows .= "<td>". $this_ClassName ."</td>";
		$this_rows .= "<td>". $this_ClassNumber ."</td>";
		$this_rows .= "<td>". $this_StudentName ."</td>";
		$this_rows .= "<td><span id='perf_". $this_StudentID."'>". $this_Performance ."</span></td>";
		$this_rows .= "<td>$this_adjustment</td>";
		$this_rows .= "<td><span id='total_". $this_StudentID ."'>". $this_total ."</span></td>";
		$this_rows .= "<td>". $this_last_modified ."</td>";
		$this_rows .= "</tr>";
		
		$table_rows .= $this_rows;
	}	
}

?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>
	
<div class="table_content">

<div class="table_board">
<table class="form_table_v30">
<tr>
	<td nowrap="nowrap" class="field_title"><?=$i_Discipline_Target?></td>
	<td>
	<? if($showTarget=="class") { ?><?=$i_general_class?> - <?=$this_ClassNameDisplay?><? } ?>
	<? if($showTarget=="club") { ?><?=$eEnrollmentMenu['club']?> - <?=$this_ClubDisplay?><? } ?>
	<? if($showTarget=="house") { ?><?=$i_House?> - <?=$this_HouseDisplay?><? } ?>
	</td>
</tr>
</table>
</div>
 
<!--- Content table //-->
<table class="common_table_list_v30 edit_table_list_v30">
<thead>
<tr>
	<th><?=$i_UserClassName?></th>
	<th><?=$i_UserClassNumber?></th>
	<th><?=$i_UserStudentName?></th>
	<th><?=$Lang['eEnrolment']['ClubActTotalPerformance']?></th>
	<th><?=$Lang['eEnrolment']['Adjustment']?></th>	
	<th><?=$Lang['eEnrolment']['TotalPerformance']?></th>	
	<th><?=$Lang['General']['LastModified']?></th>	
</tr>
</thead>
  
<?=$table_rows?>

</table>


</div>


<?
intranet_closedb();
?>