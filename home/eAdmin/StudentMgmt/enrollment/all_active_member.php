<?php
// using: 

/***********************************************
 * Date:	2015-06-08 (Evan)
 * Details:	UI update
 * 
 * Date:	2015-06-02 (Evan)
 * Details:	Replace the old style with UI standard
 * 
 * Date: 	2013-05-28 (Rita)
 * Details: create this page 
 ***********************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}

$pageSizeChangeEnabled = true;


$AcademicYearID = IntegerSafe($AcademicYearID);
if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol($AcademicYearID);	
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) && !$libenroll->HAVE_CLUB_MGT() && !$libenroll->HAVE_EVENT_MGT()  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID)))
	{
		header("Location: {$PATH_WRT_ROOT}/home/eAdmin/StudentMgmt/enrollment/");
	}	
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        $linterface = new interface_html();
        $libenroll_ui = new libclubsenrol_ui();      
       
        $CurrentPageArr['eEnrolment'] = 1;                     
        $AcademicYearID = $_POST['AcademicYearID']?$_POST['AcademicYearID']:$_GET['AcademicYearID'];
        $AcademicYearID = IntegerSafe($AcademicYearID);
        ##### Club Page #####
        if ($type=="club")
        {   
            # Current Page Info 
            $CurrentPage = "PageMgtClub";			
			$TAGS_OBJ[] = array($li->Title." ".$eEnrollment['active_member'], "", 1);  
			$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR(); 
			
			   
			# Get POST Value	
        	$EnrolGroupIDArr = $_POST['EnrolGroupID']?$_POST['EnrolGroupID']:explode(',',$_GET['EnrolGroupID']);
                          
        	# Global Active Member Percentage
        	$globalActivePer = $libenroll->activeMemberPer;
     
        	# Club Active Member Percentage
        	$result = $libenroll->Get_Club_Active_Member_Percentage($EnrolGroupIDArr);       	        	
        	$enrollStudentInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupIDArr, $PersonTypeArr=array(2), $AcademicYearID, $WithIndicator=1, $IndicatorWithStyle=1);
   			
        }       
        ##### Activity Page ##### 
        else if ($type=="activity")
        {        
            # Current Page Info 
            $CurrentPage = "PageMgtActivity";
            $TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['active_member'], "", 1);
            $MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR(); 
             
            # Get POST Value
            $EnrolEventIDArr = $_POST['EnrolEventID']?$_POST['EnrolEventID']:explode(',',$_GET['EnrolEventID']);            
            $globalActivePer = $libenroll->Event_activeMemberPer; 	
            
            # Event Active Member Percentage
            $result = $libenroll->Get_Event_Active_Member_Percentage($EnrolEventIDArr);       	        	
   			$enrollStudentInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventIDArr, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1);	        
	    }       
         
     	$linterface->LAYOUT_START();
     	
   
     	     	
     	
		$numOfResult = count($result);	
		for($i=0;$i<$numOfResult;$i++){     			
		 	if ($type=="club")
		 	{
				$thisActiveMemberPercentage = $result[$i]['ActiveMemberPercentage'];
				$thisEnrolID = $result[$i]['EnrolGroupID'];
				$thisEnrollTitle = $result[$i]['GroupTitle']; 
				$thisGroupID = $result[$i]['GroupID'];
			}elseif($type=="activity")
			{				
				$thisActiveMemberPercentage = $result[$i]['ActiveMemberPercentage'];
				$thisEnrolID = $result[$i]['EnrolEventID'];
				$thisEnrollTitle = $result[$i]['EventTitle'];				
		 	}
			
			if(isset($_GET['activeMemberPer']))
		 	{
		 		$activeMemberPer = $_GET['activeMemberPer'];
		 	
		 	}else
		 	{
	//			if($thisActiveMemberPercentage=='')
	//			{
					if($globalActivePer == '' || $globalActivePer == NULL)
					{
						$activeMemberPer = 0;
					}else
					{
						$activeMemberPer = $globalActivePer;	
					}	
	//			}
	//			else
	//			{
	//				$activeMemberPer = $thisActiveMemberPercentage;
	//			}
		 	}
		 	  	 			
			if ($type=="club")
			{
				$StudentArray = $enrollStudentInfoArr[$thisEnrolID];								  	
			}
			elseif($type=="activity")
			{
				$StudentArray = $enrollStudentInfoArr[$thisEnrolID]['StatusStudentArr'][2];	
			}
					
			$numOfStudent = count($StudentArray);
	        for ($j=0; $j<$numOfStudent; $j++)
	        {		        	        
		   		# Get Student Attendance
		   		$DataArr['StudentID'] = $StudentArray[$j]['StudentID'];
	        	$DataArr['EnrolGroupID'] = $thisEnrolID;
	        	$DataArr['EnrolEventID'] = $thisEnrolID;
	        	
	        	if ($type=="club")
	       		{
	        		$StudentArray[$j]['Attendance'] = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr);
	    		}
	    		elseif ($type=="activity")
	        	{
		        	$StudentArray[$j]['Attendance'] = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);
	        	}
		        
	        	//check if student is set to be a active member already
	        	//if not, set according to the active member percentage
	        	if ($type=="club")
	       		{
	       			$activeStatusResult = $libenroll->Get_Club_Active_Member_Status($thisEnrolID, $StudentArray[$j]['StudentID']);	        	
	        	}
	        	else if ($type=="activity")
	       		{
	        		$activeStatusResult = $libenroll->Get_Event_Active_Member_Status($thisEnrolID, $StudentArray[$j]['StudentID']);
	    		}
	        		        	
	        	if ($activeStatusResult[0]['isActiveMember']==NULL)
	        	{
		        	//no setting of active member => according to active member percentage
		        	if ($StudentArray[$j]['Attendance']>= $activeMemberPer)
		        	{
			        	$StudentArray[$j]['isActiveMember'] = 1;
		        	}
		        	else
		        	{
			        	$StudentArray[$j]['isActiveMember'] = 0;
		        	}
	        	}
	        	else
	        	{
		        	//set according to the value of isActiveMember in the database
		        	$StudentArray[$j]['isActiveMember'] = $activeStatusResult[0]['isActiveMember'];
	        	}
	        }
				        
			## Construct the table
			//construct title
			$display .= $linterface->GET_NAVIGATION2($thisEnrollTitle);
			
	
			
			$display .= "<br />";
			$display .= "<table class=\"tablegreentop\" width=\"100%\" cellpadding=\"5\" cellspacing=\"0\">";
			$display .= "<tr>";
			
			$display .= "<td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$Lang['SysMgr']['FormClassMapping']['Class']."</td>
						<td width=\"10%\" class=\"tablegreentop tabletopnolink\">".$Lang['SysMgr']['FormClassMapping']['StudentClassNumber']."</td>
						<td class=\"tablegreentop tabletopnolink\">".$eEnrollment['student_name']."</td>
						<td class=\"tablegreentop tabletopnolink\">".$i_admintitle_role."</td>
				  		<td align=\"center\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['attendence']."</td>
				  		<td width=\"15%\" align=\"center\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['active']."</td>
				  		<td width=\"15%\" align=\"center\" class=\"tablegreentop tabletopnolink\">".$eEnrollment['inactive']."</td>";
			$display .= "</tr>";
					    			
			$total_col = 7;
			$StudentSize = sizeof($StudentArray);
					
			//construct content
			$display .= ($StudentSize==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
				    
			for ($j=0; $j<$StudentSize; $j++)
			{
				$thisStudentID = $StudentArray[$j]['UserID'];			
			 	$tr_css = ($j % 2 == 1) ? "tablegreenrow2" : "tablegreenrow1";
				
				$radioName = "radioID[".$thisEnrolID."][".$thisStudentID."]";
			
				$radioID_isActive = "isActiveRadioID".$j;
				$radioID_notActive = "notActiveRadioID".$j;
				$radioValue_isActive = 1; 
				$radioValue_notActive = 0;   
				
				$display .= '<tr class="'.$tr_css.'">';
				$display .= '<td class="tabletext" valign="top">'.$StudentArray[$j]['ClassName'].'</td>
							<td class="tabletext" valign="top">'.$StudentArray[$j]['ClassNumber'].'</td>
							<td class="tabletext" valign="top">'.$StudentArray[$j]['StudentName'].'</td>
							<td class="tabletext" valign="top">'.$StudentArray[$j]['RoleTitle'].'</td>
							<td class="tabletext" align="center" valign="top">'.$StudentArray[$j]['Attendance'].'%</td>
							<td class="tabletext" align="center" valign="top">';
				      	  
				if ($StudentArray[$j]['isActiveMember'])
				{
				    $isActive = "checked";
					$notActive = "";
				}
				else
				{
				    $isActive = "";
					$notActive = "checked";
				}
								
				$display .= "<input type=\"radio\" name=\"$radioName\" id=\"$radioID_isActive\" value=\"$radioValue_isActive\" $isActive></input>";  
				$display .= '</td>';
				
				$display .= '<td class="tabletext" align="center" valign="top">';
				$display .= "<input type=\"radio\" name=\"$radioName\" id=\"$radioID_notActive\" value=\"$radioValue_notActive\" $notActive></input>";
				$display .= '</td></tr>';
			        
			}
						
			$display .= '</table>';
			$display .= '<br />';
			$display .= '<br />';
		 }
		 
		 
		 
		$activeMemberPerdisplay = '';
 		$activeMemberPerdisplay .= "<table class=\"form_table_v30\">";
		$activeMemberPerdisplay .= "<tr>";
		$activeMemberPerdisplay .= "<td class=\"field_title\">";
		$activeMemberPerdisplay .= $eEnrollment['active_member_per'];
		$activeMemberPerdisplay .= "</td>";
		$activeMemberPerdisplay .= "<td valign='top' class='tabletext'>";
		
		$activeMemberPerdisplay .= "<input class='textboxnum' type='text' name='activeMemberPer' maxlength='6' value='".number_format($activeMemberPer,2)."'> %";
		
		if($libenroll->AllowToEditPreviousYearData) {
			$activeMemberPerdisplay .= toolBarSpacer();
			$activeMemberPerdisplay .= $linterface->GET_SMALL_BTN($button_update, "button", "javascript:checkForm(document.form1,'all_active_member_update.php',1,$thisEnrolID)");			   
		}
		
		$activeMemberPerdisplay .= "</td>";
		$activeMemberPerdisplay .= "<td align='right'>";
		$activeMemberPerdisplay .= $SysMsg ;
		$activeMemberPerdisplay .= "</td>";
		$activeMemberPerdisplay .= "</tr>";
		$activeMemberPerdisplay .= "<br />";
		$activeMemberPerdisplay .= "</table>";
     	   	  
?>

<script language="javascript">

function checkForm(obj,page,isUpdatePer,UpdatePerEnrollIDVal){
	objActiveMemberPer = document.getElementById("isUpdatePercentage");
	objActiveMemberPer.value = isUpdatePer;
	obj.action=page;
	
	$('#UpdatePerEnrollID').val(UpdatePerEnrollIDVal);
	
	if (isUpdatePer == 1)
	{					
		if(confirm("<?=$eEnrollment['js_update_active_member_per']?>"))
		{			
			obj.submit();
		}
		else
		{
			return false;	
		}
	}
	else
	{
    	obj.submit();
	}
}
</script>
<form name="form1" method="POST">
			<table width="100%" border="0" cellspacing="4" cellpadding="4">
				<tr>
					<td>
						<?=$linterface->GET_NAVIGATION($PAGE_NAVIGATION);?>
					</td>
				</tr>
			</table>
						
			<br />		
			<?=$activeMemberPerdisplay?>	
			<?=$display?>			
			<?=$libenroll_ui->Get_Student_Role_And_Status_Remarks();?>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
				<tr><td class="field_title" align="center">
					<? if($libenroll->AllowToEditPreviousYearData) {?>
						<?= $linterface->GET_ACTION_BTN($button_save, "button", "javascript:checkForm(document.form1,'all_active_member_update.php',0,'')")?>&nbsp;
						<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
					<?}?>
						<? if ($type=="club") { ?>
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='group.php?AcademicYearID=$AcademicYearID&sel_category=$sel_category&Semester=$Semester'")?>&nbsp;
						<? } else if ($type=="activity") { ?>
							<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='event.php?AcademicYearID=$AcademicYearID&sel_category=$sel_category'")?>&nbsp;
						<? } ?>
						
				</td></tr>
			</table>
			
			<input type="hidden" name="GroupID" value="<?= $GroupID ?>">
			<input type="hidden" name="EnrolGroupIDArr" value="<?= implode(',',$EnrolGroupIDArr) ?>">
			<input type="hidden" name="EnrolEventIDArr" value="<?= implode(',',$EnrolEventIDArr) ?>">
			<input type="hidden" name="type" value="<?=$type?>">
			<input type="hidden" id="isUpdatePercentage" name="isUpdatePercentage">
			<input type="hidden" name="StudentSize" value="<?= $StudentSize ?>">
			<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?= $AcademicYearID ?>">			
			<input type="hidden" name="UpdatePerEnrollID" id="UpdatePerEnrollID" value="">
			
		</form>
		<br />
		
<?
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
		You have no priviledge to access this page.
    <?
    }
}
else
{
?>
		You have no priviledge to access this page.
<?
}
?>