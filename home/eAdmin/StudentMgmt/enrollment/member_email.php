<?php
#using : 
/******************************************
 * Modification Log:
 * Date:    2019-10-25 Tommy
 * Details: change hidden UID to idCode for server not change the value to be 0
 * 
 * Date:	2015-06-08 Evan
 * Details:	UI bug fixed
 * 
 * Date:	2015-06-02 Evan
 * Details:	Replace the old style with UI standard
 * 
 *****************************************/ 
 
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$RoleID = IntegerSafe($_POST['RoleID']);
$filter = IntegerSafe($_POST['filter']);
$adminflag = IntegerSafe($_POST['adminflag']);
$GroupID = IntegerSafe($_POST['GroupID']);
$EnrolGroupID = IntegerSafe($_POST['EnrolGroupID']);
$Semester = IntegerSafe($_POST['Semester']);
$AcademicYearID = IntegerSafe($_POST['AcademicYearID']);

$li = new libgroup($GroupID);
if (isset($ck_page_size) && $ck_page_size != "")
{
    $page_size = $ck_page_size;
}
$pageSizeChangeEnabled = true;

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	$libenroll = new libclubsenrol();	
	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	if ($type == "activity")
	{
		$CurrentPage = "PageMgtActivity";
		$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		$CurrentPageArr['eEnrolment'] = 1;
	    //$TAGS_OBJ[] = array($EventInfoArr[3]." ".$button_email, "", 1);
	    # tags 
        $tab_type = "activity";
        # navigation
        $PAGE_NAVIGATION[] = array($EventInfoArr[3]." ".$button_email, "");
	}
	else
	{
		$CurrentPage = "PageMgtClub";
		$CurrentPageArr['eEnrolment'] = 1;
		//$TAGS_OBJ[] = array($li->Title." ".$button_email, "", 1);
		# tags 
        $tab_type = "club";
        # navigation
        $PAGE_NAVIGATION[] = array($li->Title." ".$button_email, "");
	}	
	
	$current_tab = 1;
    include_once("management_tabs.php");
        
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        

	    ################################################################
	    /* Old code
		    Old Approach: user select student in this page
		    New Approach: user select student before entering this page
	    if ($type=="activity")
	    {
			$userEmails = $libenroll->GET_EVENT_STUDENT_EMAIL_OPTION($EnrolEventID);
	    }
	    else
	    {
		    $userEmails = $li->displayGroupUsersEmailOption();
	    }
	    */
	    
	    
	    $studentIDArr = $UID;
		$studentList = implode(",", $studentIDArr);
	    
	    # get student name, class and class number for display
	    $name_field = getNameFieldWithClassNumberByLang();
	    $sql = "SELECT $name_field FROM INTRANET_USER WHERE UserID IN ($studentList) ORDER BY ClassName,ClassNumber";
	    $studentDisplayArr = $libenroll->returnVector($sql);
	    
	    $toRowsHTML = "";
	    
	    //Displaying multiple address in the same field
	    $toRowsHTML .= "<tr><td class=\"field_title\">";
		$toRowsHTML .= $i_email_to;
		$toRowsHTML .= "<td>";
	    for ($i=0; $i<sizeof($studentDisplayArr); $i++)
	    {
			$toRowsHTML .= "{$studentDisplayArr[$i]}<br />";
	    }
	    $toRowsHTML .= "</tr>";
	    
	    $linterface->LAYOUT_START();
?>

<script language="javascript">
function checkform(obj){
	if(!check_text(obj.subject, "<?php echo $i_alert_pleasefillin.$i_email_subject; ?>.")) return false;
	if(!check_text(obj.message, "<?php echo $i_alert_pleasefillin.$i_email_message; ?>.")) return false;
	return (confirm("<?php echo $i_email_sendemail; ?>?")) ? true : false;
}
</script>

<form name="form1" action='member_email_update.php' method='post' onSubmit="return checkform(this);">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
</table>
<table class="form_table_v30">
<?= $toRowsHTML ?>
<tr>
	<td class="field_title"><?=$linterface->RequiredSymbol().$i_email_subject; ?></td>
	<td><input class='textboxtext' type='text' name='subject'></td>
</tr>
<tr>
	<td class="field_title"><?=$linterface->RequiredSymbol().$eEnrollment['Content']?></td>
	<td><?= $linterface->GET_TEXTAREA("message", "", $taCols=55, $taRows=15) ?></td>
</tr>
</table>

<input type='hidden' name='GroupID' value="<?= $GroupID; ?>">
<input type='hidden' name='EnrolGroupID' value="<?= $EnrolGroupID; ?>">
<input type='hidden' name='EnrolEventID' value="<?= $EnrolEventID; ?>">
<input type='hidden' name='filter' value="<?=$filter; ?>">
<input type='hidden' name='type' value="<?=$type; ?>">
<input type="hidden" name="field" value="<?=$field?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="pageNo" value="<?=$pageNo?>">
<input type="hidden" name="page_size_change" value="">
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>">
<input type="hidden" name="idCode" value="<?=rawurlencode(serialize($UID));?>">
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>">

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_send, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "javascript:history.back()")?>&nbsp;
</div>
</td></tr>
</table>

</form>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>