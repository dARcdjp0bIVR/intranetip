<?php
// using : 

########## Change Log ##############
##  Date:   2018-12-10 Anna
##          Added string replace for SelectionID,AllTitle and OnChange for Cross Site Scripting Attack
##
##	Date:	2017-09-15 Anna
##			New flag 'WebSAMSSTAType'
##
##	Date:	2016-01-04	Kenneth
##			Edit flag activityNature, activityLocation
##
##	Date:	2015-12-29	Omas
##			New flag 'ole_type' , 'ole_lang' , 'ole_category' , 'ole_component' - ole default setting import 
##
##	Date: 	2015/10/27 Kenneth
##			Remarks table update for import_activity_info.php and import_club_info.php
##			****Old Code are not yet removed due to the uncertainly code having used by other modules
##
##	Date:	2014-11-21	Omas
##			New flag 'house' for showing house code 
##
##	Date:	2014-07-09 Carlos
##			$sys_custom['eEnrolment']['TWGHCYMA'] - for $flag 'existingClub', display GorupCode instead of EnrolGroupID
##
##	Date:	2014-04-10 Carlos
##			TWGHs C Y MA MEMORIAL COLLEGE $sys_custom['eEnrolment']['TWGHCYMA'] - added $flag [activityLocation], [activityNature]	
##
##	Date:	2011-01-31 YAtWoon
##			update Club_Selection, Club_Attendance_Info_Table, Reload_Club_Attendance_Table, add academic year id
##
#####################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == "Copy_Club_Term_Mapping_Table")
{
	$FromAcademicYearID = stripslashes($_REQUEST['FromAcademicYearID']);
	$ToAcademicYearID = stripslashes($_REQUEST['ToAcademicYearID']);
	
	echo $libenroll_ui->Get_Copy_Club_Step1_Term_Mapping_Table($FromAcademicYearID, $ToAcademicYearID);
}
else if ($Action == "Copy_Club_Club_Selection_Table")
{
	$FromAcademicYearID = stripslashes($_REQUEST['FromAcademicYearID']);
	$ToAcademicYearID = stripslashes($_REQUEST['ToAcademicYearID']);
	
	echo $libenroll_ui->Get_Copy_Club_Step1_Club_Selection_Table($FromAcademicYearID, $ToAcademicYearID);
}
else if ($Action == "Copy_Club_By_Term_Club_Selection_Table")
{
    $AcademicYearID = trim(stripslashes(IntegerSafe($_POST['AcademicYearID'])));
	$FromYearTermID = trim(stripslashes($_POST['FromYearTermID']));
	$ToYearTermID = trim(stripslashes($_POST['ToYearTermID']));
	
	echo $libenroll_ui->Get_Copy_Club_By_Term_Step1_Club_Selection_Table($AcademicYearID, $FromYearTermID, $ToYearTermID);
}
else if ($Action == 'Copy_Club_Academic_Year_Selection')
{
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$Tag = stripslashes($_REQUEST['Tag']);
	$AcademicYearID = stripslashes(IntegerSafe($_REQUEST['AcademicYearID']));
	$ExcludeAcademicYearID = stripslashes($_REQUEST['ExcludeAcademicYearID']);
	$NoPastYear = stripslashes($_REQUEST['NoPastYear']);

	$SelectionID= str_replace( 
	    array('<', '>','"','\'','/'),  
	    array('','','',''),
	    $SelectionID);
	
	if ($ExcludeAcademicYearID == '')
		$ExcludeAcademicYearID = array();
	else
		$ExcludeAcademicYearID = array($ExcludeAcademicYearID);
	
		
	echo getSelectAcademicYear($SelectionID, $Tag, $noFirst=1, $NoPastYear, $AcademicYearID, $displayAll=0, $pastAndCurrentYearOnly=0, $OrderBySequence=1, $excludeCurrentYear=0, array($ExcludeAcademicYearID));
}
else if ($Action == 'Term_Selection')
{
	include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
	$libscm_ui = new subject_class_mapping_ui();
	
	$SelectionID = stripslashes($_REQUEST['SelectionID']);
	$AcademicYearID = stripslashes(IntegerSafe($_REQUEST['AcademicYearID']));
	$YearTermID = stripslashes($_REQUEST['YearTermID']);
	$NoFirst = stripslashes($_REQUEST['NoFirst']);
// 	$OnChange = stripslashes($_REQUEST['OnChange']);

	$OnChange = $OnChange == "js_Reload_Club_Info_Table();"?stripslashes($_REQUEST['OnChange']): str_replace(array('<', '>',';','\'','/'),'',stripslashes($_REQUEST['OnChange']));
	$AllTitle = trim(urldecode(stripslashes($_REQUEST['AllTitle'])));
	$WithLastTransferDate = $_REQUEST['WithLastTransferDate'];
	$AllTitle = str_replace(
	    array('<', '>','"','\'','/'),
	    array('','','',''),
	    $AllTitle);
	$SelectionID= str_replace(
	    array('<', '>','"','\'','/'),
	    array('','','',''),
	    $SelectionID);


	$ExcludeYearTermIDList = trim(stripslashes($_REQUEST['ExcludeYearTermIDList']));
	$ExcludeYearTermIDArr = explode(',', $ExcludeYearTermIDList);
	
	echo $libscm_ui->Get_Term_Selection($SelectionID, $AcademicYearID, $YearTermID, $OnChange, $NoFirst, $NoPastTerm=0, $displayAll=0, $AllTitle, $PastTermOnly=0, $CompareYearTermID='', $IsMultiple=0, $IncludeYearTermIDArr='', $ExcludeYearTermIDArr);
}
else if ($Action == 'Term_Checkbox')
{
    $AcademicYearID = stripslashes(IntegerSafe($_REQUEST['AcademicYearID']));
	echo $libenroll_ui->Get_Term_Checkbox($AcademicYearID);
}
else if ($Action == 'Club_Selection')
{
    $ID = trim(urldecode(stripslashes(cleanHtmlJavascript($_REQUEST['ID']))));
	$Name = trim(urldecode(stripslashes(cleanHtmlJavascript($_REQUEST['Name']))));
	$TargetEnrolGroupID = trim(urldecode(stripslashes($_REQUEST['TargetEnrolGroupID'])));
	$CategoryID = trim(urldecode(stripslashes($_REQUEST['CategoryID'])));
	$IsAll = trim(urldecode(stripslashes($_REQUEST['IsAll'])));
	$NoFirst = trim(urldecode(stripslashes($_REQUEST['NoFirst'])));
	$OnChange = $OnChange== "js_Changed_Club_Selection(this.value);"? trim(urldecode(stripslashes($_REQUEST['OnChange']))):"";

	$AcademicYearID = trim(urldecode(stripslashes(IntegerSafe($_REQUEST['AcademicYearID']))));
// 	$OnChange = str_replace(
// 	    array('<', '>','"','\'','/'),
// 	    array('','','',''),
// 	    $OnChange);
	
	echo $libenroll_ui->Get_Club_Selection($ID, $Name, $TargetEnrolGroupID, $CategoryID, $IsAll, $NoFirst, $OnChange, $AcademicYearID);
}
else if ($Action == 'Activity_Selection')
{
    $ID = trim(urldecode(stripslashes(cleanHtmlJavascript($_REQUEST['ID']))));
	$Name = trim(urldecode(stripslashes(cleanHtmlJavascript($_REQUEST['Name']))));
	$TargetEnrolEventID = trim(urldecode(stripslashes($_REQUEST['TargetEnrolEventID'])));
	$CategoryID = trim(urldecode(stripslashes($_REQUEST['CategoryID'])));
	$IsAll = trim(urldecode(stripslashes($_REQUEST['IsAll'])));
	$NoFirst = trim(urldecode(stripslashes($_REQUEST['NoFirst'])));
	$OnChange = $OnChange=="js_Changed_Activity_Selection(this.value);"?trim(urldecode(stripslashes($_REQUEST['OnChange']))):"";
	$AcademicYearID = IntegerSafe($_REQUEST['AcademicYearID']);
	
	echo $libenroll_ui->Get_Activity_Selection($ID, $Name, $TargetEnrolEventID, $CategoryID, $IsAll, $NoFirst, $OnChange, $AcademicYearID);
}
else if ($Action == 'Club_Attendance_Info_Table')
{
    $EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
	$CanEdit = IntegerSafe($_REQUEST['CanEdit']);
	$PageAction = $_REQUEST['PageAction'];
	$AcademicYearID = trim(urldecode(stripslashes(IntegerSafe($_REQUEST['AcademicYearID']))));
	
	echo $libenroll_ui->Get_Management_Club_Attendance_UI($EnrolGroupID, $CanEdit, $PageAction,$AcademicYearID);
}
else if ($Action == 'Activity_Attendance_Info_Table')
{
    $EnrolEventID = IntegerSafe($_REQUEST['EnrolEventID']);
	$CanEdit = IntegerSafe($_REQUEST['CanEdit']);
	$PageAction = $_REQUEST['PageAction'];
	//$AcademicYearID = $_REQUEST['AcademicYearID'];
	echo $libenroll_ui->Get_Management_Activity_Attendance_UI($EnrolEventID, $CanEdit, $PageAction, $AcademicYearID);
}
else if ($Action == 'Reload_Club_Attendance_Table')
{
    $EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
	$CanEdit = IntegerSafe($_REQUEST['CanEdit']);
	$PageAction = $_REQUEST['PageAction'];
	$DisplayPhoto = $_REQUEST['DisplayPhoto'];
	$AcademicYearID = trim(urldecode(stripslashes(IntegerSafe($_REQUEST['AcademicYearID']))));
	echo $libenroll_ui->Get_Management_Club_Attendance_Info_Table($EnrolGroupID, $CanEdit, $PageAction, array(), $DisplayPhoto, $AcademicYearID);
}
else if ($Action == 'Reload_Activity_Attendance_Table')
{
	$EnrolEventID = $_REQUEST['EnrolEventID'];
	$CanEdit = IntegerSafe($_REQUEST['CanEdit']);
	$PageAction = $_REQUEST['PageAction'];
	$DisplayPhoto = $_REQUEST['DisplayPhoto'];
	//$AcademicYearID = $_REQUEST['AcademicYearID'];
	
	echo $libenroll_ui->Get_Management_Activity_Attendance_Info_Table($EnrolEventID, $CanEdit, $PageAction, array(), $DisplayPhoto, $AcademicYearID);
	
} else if ($Action == 'Reload_SP_Last_Transfer_Date') {
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$libgs = new libgeneralsettings();
	
	# display last transfer time
	$yearTermText = ($YearTermID=="") ? "wholeyear" : $YearTermID;	
	//echo '"TransferToSP_'.$AcademicYearID.'_'.$yearTermText.'"/';
	$x = $libgs->Get_General_Setting('eEnrolment', array('"TransferToSP_'.IntegerSafe($AcademicYearID).'_'.$yearTermText.'"'));
	
	if(sizeof($x)>0) {
		$value = array_values($x);
		//$data = split("::", $value[0]);
		$data = explode("::", $value[0]);		
	}	
	//$date = ($data[0]!="") ? substr($data[0],0,10) : "-";
	$date = ($data[0]!="") ? $data[0] : $Lang['General']['EmptySymbol'];
	echo "<span class='tabletextremark'>".Get_Last_Modified_Remark($date, "", $data[1], $eDiscipline["LastTransferringDate"]." : <!--DaysAgo-->")."</span>";
	
}
else if ($Action == 'getMeetingDate_dateSelectionCalendar') {
	$recordType = $_POST['recordType'];
	$recordId = $_POST['recordId'];
	$year = $_POST['year'];
	$month = $_POST['month'];
	
	echo $libenroll_ui->Get_Meeting_Date_Selection_Calendar($recordType, $recordId, $year, $month);
}
else if ($Action == 'getMeetingDate_dateTimeMgmtTable') {
	$recordType = $_POST['recordType'];
	$recordId = $_POST['recordId'];
	
	echo $libenroll_ui->Get_Meeting_Date_Time_Mgmt_Table($recordType, $recordId);
}
else if ($Action == 'getMeetingDate_periodicDateSelectionTable') {
	echo $libenroll_ui->Get_Meeting_Date_Periodic_Date_Selection_Table();
}
else if ($Action == "Club_Attendance_Info_Table_Schedule_Detail") {
	
	if ($libenroll->enableAttendanceScheduleDetail() || $libenroll->enableUserJoinDateRange()) {
		include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui_cust.php");
		$libenroll_ui_cust = new libclubsenrol_ui_cust();
		$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);
		$ActivityID = $_REQUEST['ActivityID'];
		$CanEdit = IntegerSafe($_REQUEST['CanEdit']);
		$PageAction = $_REQUEST['PageAction'];
		$DisplayPhoto = $_REQUEST['DisplayPhoto'];
		$AcademicYearID = trim(urldecode(stripslashes(IntegerSafe($_REQUEST['AcademicYearID']))));

		echo $libenroll_ui_cust->Get_Management_Club_Attendance_SC_Info_Table($EnrolGroupID, $ActivityID, $CanEdit, $PageAction, array(), $DisplayPhoto, $AcademicYearID);
	}
}
else if ($Action == "Activity_Attendance_Info_Table_Schedule_Detail") {
	if ($libenroll->enableAttendanceScheduleDetail() || $libenroll->enableUserJoinDateRange()) {
		include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui_cust.php");
		$libenroll_ui_cust = new libclubsenrol_ui_cust();
		
		$EnrolEventID = $_REQUEST['EnrolEventID'];
		$ActivityID = $_REQUEST['ActivityID'];
		$CanEdit = IntegerSafe($_REQUEST['CanEdit']);
		$PageAction = $_REQUEST['PageAction'];
		$DisplayPhoto = $_REQUEST['DisplayPhoto'];
		//$AcademicYearID = $_REQUEST['AcademicYearID'];
		
		echo $libenroll_ui_cust->Get_Management_Activity_Attendance_SC_Info_Table($EnrolEventID, $ActivityID, $CanEdit, $PageAction, array(), $DisplayPhoto, $AcademicYearID);
	}
}
else if($flag != "") {
	
	# table [Start]
	$data = '<table width="90%" border="0" cellpadding="0" cellspacing="0" bgcolor="#CCCCCC" class="layer_table_detail">';
	if($flag == 'announce') {
//	2015-10-31 Kenneth Replaced by new code		
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$Lang['General']['Remark'].'</td>
//				</tr>';
//		
//		$data .= '<tr class="tablerow1">
//					<td>Y</td>
//					<td>'.$Lang['General']['Yes'].'</td>
//				</tr>';			
//		$data .= '<tr class="tablerow1">
//					<td>N</td>
//					<td>'.$Lang['General']['No'].'</td>
//				</tr>';	
				
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$Lang['General']['Remark'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>Y</td><td>'.$Lang['General']['Yes'].'</td>';
		$RemarkDetailsArr[1] = '<td>N</td><td>'.$Lang['General']['No'].'</td>';
		
		
		
	} else if($flag == 'category') {
		$sql = "SELECT CategoryName FROM INTRANET_ENROL_CATEGORY ORDER BY DisplayOrder";
		$cat = $libenroll->returnResultSet($sql);
//	2015-10-31 Kenneth Replaced by new code		
//		$data .='<tr class="tabletop">
//					<td>'.$eEnrollment['add_activity']['act_category'].'</td>
//				</tr>';
//				
//		for($i=0; $i<count($cat); $i++) {
//			$data .= '<tr class="tablerow1">
//						<td>'.$cat[$i]['CategoryName'].'</td>
//					</tr>';		
//		}
//		
		//New Style
		$title = '<th>';
		$title .= $eEnrollment['add_activity']['act_category'];
		$title .= '</th>';
		for($i=0; $i<count($cat); $i++){
			$RemarkDetailsArr[$i] = '<td>'.$cat[$i]['CategoryName'].'</td>';
		}
		
	} else if($flag == 'term') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['YearTermID'].'</td>
//					<td>'.$Lang['General']['TermName'].'</td>
//				</tr>';	
		$sql = "SELECT YearTermID, ".Get_Lang_Selection("YearTermNameB5","YearTermNameEN")." as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID='$AcademicYearID' ORDER BY TermStart";
		$termInfo = $libenroll->returnResultSet($sql);
//	2015-10-31 Kenneth Replaced by new code		
//		$data .= '<tr class="tablerow1">
//						<td>YearBase</td>
//						<td>'.$eEnrollment['YearBased'].'</td>
//					</tr>';
//					
//		for($i=0; $i<count($termInfo); $i++) {
//			$data .= '<tr class="tablerow1">
//						<td>'.$termInfo[$i]['YearTermID'].'</td>
//						<td>'.$termInfo[$i]['TermName'].'</td>
//					</tr>';		
//		}		
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['YearTermID'].'</th>
					<th>'.$Lang['General']['TermName'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>YearBase</td><td>'.$eEnrollment['YearBased'].'</td>';
		
		for($i=0; $i<count($termInfo); $i++) {
			$RemarkDetailsArr[$i+1]= '<td>'.$termInfo[$i]['YearTermID'].'</td>
						<td>'.$termInfo[$i]['TermName'].'</td>';		
		}		
		
		
		
	} else if($flag == 'form') {
		
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'/'.$Lang['General']['Form'].'</td>
//				</tr ="tablerow1">
//				<tr>
//					<td>A represent All Forms</td>
//				</tr>';
				
		$sql = "SELECT YearName FROM YEAR WHERE RecordStatus=1 ORDER BY Sequence";
		$form = $libenroll->returnResultSet($sql);
		
//	2015-10-31 Kenneth Replaced by new code
//		for($i=0; $i<count($form); $i++) {
//			$data .= '<tr class="tablerow1">
//						<td>'.$form[$i]['YearName'].'</td>
//					</tr>';		
//		}		
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'/'.$Lang['General']['Form'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>A represent All Forms</td>';
		for($i=0; $i<count($form); $i++) {		
			$RemarkDetailsArr[$i+1] .= '<td>'.$form[$i]['YearName'].'</td>';
		}		
	
	} else if($flag == 'house') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$i_House.'</td>
//				</tr>
//				<tr class="tablerow1">
//						<td>A</td>
//						<td>'.$eEnrollment['no_limit'].'</td>
//				</tr>';
		$HouseArr = $libenroll->Get_GroupMapping_Group(Get_Current_Academic_Year_ID(), HOUSE_RECORDTYPE_IN_INTRANET_GROUP);
//	2015-10-31 Kenneth Replaced by new code
//		for($i=0; $i<count($HouseArr); $i++) {
//			$data .= '<tr class="tablerow1">
//						<td>'.$HouseArr[$i]['GroupID'].'</td>
//						<td>'.$HouseArr[$i]['Title'].'</td>
//					</tr>';		
//		}
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$i_House.'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>A</td><td>'.$eEnrollment['no_limit'].'</td>';
		for($i=0; $i<count($HouseArr); $i++) {		
			$RemarkDetailsArr[$i+1] .= '<td>'.$HouseArr[$i]['GroupID'].'</td>
						<td>'.$HouseArr[$i]['Title'].'</td>';
		}		
		
	} else if($flag == 'age') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$Lang['eEnrolment']['AgeGroup'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>A</td>
//					<td>'.$Lang['eEnrolment']['NoLimit'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td colspan="2">['.$Lang['General']['Or'].']</td>
//				</tr>
//				<tr class="tablerow1">
//					<td colspan="2">'.$Lang['eEnrolment']['AgeGroupRange'].'</td>
//				</tr>
//				';		
				
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$Lang['eEnrolment']['AgeGroup'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>A</td><td>'.$Lang['eEnrolment']['NoLimit'].'</td>';
		$RemarkDetailsArr[1] = '<td colspan="2">['.$Lang['General']['Or'].']</td>';
		$RemarkDetailsArr[2] = '<td colspan="2">'.$Lang['eEnrolment']['AgeGroupRange'].'</td>';
		
	} else if($flag == 'gender') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$Lang['StudentRegistry']['Gender'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>A</td>
//					<td>'.$Lang['General']['All'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>M</td>
//					<td>'.$Lang['General']['Male'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>F</td>
//					<td>'.$Lang['General']['Female'].'</td>
//				</tr>
//				';			
				
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$Lang['StudentRegistry']['Gender'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>A</td><td>'.$Lang['General']['All'].'</td>';
		$RemarkDetailsArr[1] = '<td>M</td><td>'.$Lang['General']['Male'].'</td>';
		$RemarkDetailsArr[2] = '<td>F</td><td>'.$Lang['General']['Female'].'</td>';
		
	} else if($flag == 'fee') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$Lang['eEnrolment']['TentativeFee'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>TBC</td>
//					<td>'.$eEnrollment['ToBeConfirmed'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td colspan="2">['.$Lang['General']['Or'].']</td>
//				</tr>
//				<tr class="tablerow1">
//					<td colspan="2">'.$Lang['eEnrolment']['DirectlyInputFee'].'</td>
//				</tr>';
				
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$Lang['eEnrolment']['TentativeFee'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>TBC</td><td>'.$eEnrollment['ToBeConfirmed'].'</td>';
		$RemarkDetailsArr[1] = '<td colspan="2">['.$Lang['General']['Or'].']</td>';
		$RemarkDetailsArr[2] = '<td colspan="2">'.$Lang['eEnrolment']['DirectlyInputFee'].'</td>';
		
	} else if($flag == 'existingClub') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$Lang['eEnrolment']['Category'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>[NA]</td>
//					<td>'.$Lang['General']['NA'].'</td>
//				</tr>';
		$GroupInfoArr = $libenroll->getGroupInfoList($isAll=1, '', $DisplayGroupIDArr="","","",$AcademicYearID);
		
//	2015-10-31 Kenneth Replaced by new code
//		for($i=0; $i<count($GroupInfoArr); $i++) {
//			
//			$group_code = $sys_custom['eEnrolment']['TWGHCYMA'] ? $GroupInfoArr[$i]['GroupCode'] : $GroupInfoArr[$i]['EnrolGroupID'];
//			$data .= '
//				<tr class="tablerow1">
//					<td>'.$group_code.'</td>
//					<td>'.$GroupInfoArr[$i]['Title'].'</td>
//				</tr>';
//		}	
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$Lang['eEnrolment']['Category'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>[NA]</td><td>'.$Lang['General']['NA'].'</td>';
		for($i=0; $i<count($GroupInfoArr); $i++) {
			$group_code = $sys_custom['eEnrolment']['TWGHCYMA'] ? $GroupInfoArr[$i]['GroupCode'] : $GroupInfoArr[$i]['EnrolGroupID'];
			
			$RemarkDetailsArr[$i+1] .= '<td>'.$group_code.'</td>' .
					'<td>'.$GroupInfoArr[$i]['Title'].'</td>';
		}	
		

	} else if($flag == 'nature') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$Lang['eEnrolment']['Nature'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>1</td>
//					<td>'.$Lang['eEnrolment']['SchoolActivity'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>0</td>
//					<td>'.$Lang['eEnrolment']['NonSchoolActivity'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>2</td>
//					<td>'.$Lang['eEnrolment']['MixedActivity'].'</td>
//				</tr>
//				';	
				
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$Lang['eEnrolment']['Nature'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>1</td><td>'.$Lang['eEnrolment']['SchoolActivity'].'</td>';
		$RemarkDetailsArr[1] = '<td>0</td><td>'.$Lang['eEnrolment']['NonSchoolActivity'].'</td>';
		$RemarkDetailsArr[2] = '<td>2</td><td>'.$Lang['eEnrolment']['MixedActivity'].'</td>';
			
	
	} else if($flag == 'enrolMethod') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$eEnrollment['add_activity']['app_method'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>0</td>
//					<td>'.$i_ClubsEnrollment_Random.'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>1</td>
//					<td>'.$i_ClubsEnrollment_AppTime.'</td>
//				</tr>';	
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$eEnrollment['add_activity']['app_method'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>0</td><td>'.$i_ClubsEnrollment_Random.'</td>';
		$RemarkDetailsArr[1] = '<td>1</td><td>'.$i_ClubsEnrollment_AppTime.'</td>';
		
		
	} else if($flag == 'enrolSetting') {
//	2015-10-31 Kenneth Replaced by new code
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$eEnrollment['add_activity']['apply_ppl_type'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>S</td>
//					<td>'.$eEnrollment['add_activity']['student_enroll'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>P</td>
//					<td>'.$eEnrollment['add_activity']['parent_enroll'].'</td>
//				</tr>';	
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$eEnrollment['add_activity']['apply_ppl_type'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>S</td><td>'.$eEnrollment['add_activity']['student_enroll'].'</td>';
		$RemarkDetailsArr[1] = '<td>P</td><td>'.$eEnrollment['add_activity']['parent_enroll'].'</td>';
		
		
	} else if($flag == 'activityLocation') {
//		$data .='<tr class="tabletop">
//					<td>'.$Lang['General']['Code'].'</td>
//					<td>'.$Lang['eEnrolment']['ActivityLocation'].'</td>
//				</tr>';	
//		$data .='<tr class="tablerow1">
//					<td>0</td>
//					<td>'.$Lang['eEnrolment']['NonSchoolActivity'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>1</td>
//					<td>'.$Lang['eEnrolment']['SchoolActivity'].'</td>
//				</tr>
//				<tr class="tablerow1">
//					<td>2</td>
//					<td>'.$Lang['eEnrolment']['MixedActivity'].'</td>
//				</tr>';	

		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$Lang['eEnrolment']['ActivityLocation'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>0</td><td>'.$Lang['eEnrolment']['NonSchoolActivity'].'</td>';
		$RemarkDetailsArr[1] = '<td>1</td><td>'.$Lang['eEnrolment']['SchoolActivity'].'</td>';
		$RemarkDetailsArr[2] = '<td>2</td><td>'.$Lang['eEnrolment']['MixedActivity'].'</td>';
	}else if($flag == 'activityNature'){
		$natureList = $libenroll->Get_Activity_Nature_List();
		
		
				
		//debug_pr($natureList);
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['eEnrolment']['Nature'].'</th>';
		$title .= '</tr>';
		
		for($i=0; $i<count($natureList); $i++) {
			$data .= '<tr class="tablerow1">
						<td>'.$natureList[$i]['Nature'].'</td>
					</tr>';		
			$RemarkDetailsArr[$i]='<td>'.$natureList[$i]['Nature'].'</td>';
		}
		//$RemarkDetailsArr[0] = $Lang['eEnrolment']['NonSchoolActivity'].'</td>';
		
		
	}else if($flag == 'ole_lang'){
		
		$cat_array = $libenroll->Get_Ole_Category_Array();
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.str_replace('<!--DataName-->',$Lang['eEnrolment']['TargetNameLang'],$Lang['eEnrolment']['Import']['DefaultOLE']).'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>'.'C'.'</td><td>'.$Lang['General']['Chinese'].'</td>';
		$RemarkDetailsArr[1] = '<td>'.'E'.'</td><td>'.$Lang['General']['English'].'</td>';
		
	}else if($flag == 'ole_type'){
		include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
		$cat_array = $libenroll->Get_Ole_Category_Array();
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.str_replace('<!--DataName-->',$Lang['eEnrolment']['RecordType'],$Lang['eEnrolment']['Import']['DefaultOLE']).'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[0] = '<td>'.'INT'.'</td><td>'.$Lang['iPortfolio']['OEA']['OLE_Name2'].'</td>';
		$RemarkDetailsArr[1] = '<td>'.'EXT'.'</td><td>'.$iPort['external_record'].'</td>';
		
	}else if($flag == 'ole_category'){
		
		$cat_array = $libenroll->Get_Ole_Category_Array();
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.str_replace('<!--DataName-->',$eEnrollment['OLE_Category'],$Lang['eEnrolment']['Import']['DefaultOLE']).'</th>';
		$title .= '</tr>';
		
		$countCat = count($cat_array);
		for($x = 0; $x < $countCat; $x++ ){
			$RemarkDetailsArr[$x] = '<td>'.$cat_array[$x][0].'</td><td>'.$cat_array[$x][1].'</td>';
			$count++;
		}
	}else if($flag == 'ole_component'){
		
		include_once($PATH_WRT_ROOT."includes/libportfolio.php");
		include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
		$LibPortfolio = new libpf_slp();
		$DefaultELEArray = $LibPortfolio->GET_ELE();
		
		//New Style
		$title = '<tr>';
		$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.str_replace('<!--DataName-->',$eEnrollment['OLE_Components'],$Lang['eEnrolment']['Import']['DefaultOLE']).'</th>';
		$title .= '</tr>';
		
		$count = 0;
		foreach((array)$DefaultELEArray as $code => $componentName){
			$RemarkDetailsArr[$count] = '<td>'.$code.'</td><td>'.$componentName.'</td>';
			$count++;
		}
	}else if($flag == 'WebSAMSSTAType'){
		
		//New Style
		$title = '<tr>';
			$title .= '<th>'.$Lang['General']['Code'].'</th>
					<th>'.$eEnrollment['WebSAMSSTAType'].'</th>';
		$title .= '</tr>';
		
		$RemarkDetailsArr[]='<td> E </td><td>'.$eEnrollment['WebSAMSSTA']['ECA'].'</td>';
		$RemarkDetailsArr[]='<td> S </td><td>'.$eEnrollment['WebSAMSSTA']['ServiceDuty'].'</td>';
		$RemarkDetailsArr[]='<td> I </td><td>'.$eEnrollment['WebSAMSSTA']['InterSchoolActivities'].'</td>';
		
	}
	
	
	# table [End]
	$data .='</table>';		
	
	
	# prepare output table
//	2015-10-31 Kenneth Replaced by new code
//	$output = '<table width="300" border="0" cellpadding="0" cellspacing="0">
//			  <tr>
//				<td height="19"><table width="100%" border="0" cellspacing="0" cellpadding="0">
//				  <tr>
//					<td width="5" height="19"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_01.gif" width="5" height="19"></td>
//					<td height="19" valign="middle" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_02.gif">&nbsp;</td>
//					<td width="19" height="19"><a href="javascript:Hide_Window(\'RefDiv\')"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_close_off.gif" name="pre_close1" width="19" height="19" border="0" id="pre_close1"></a></td>
//				  </tr>
//				</table></td>
//			  </tr>
//			  <tr>
//				<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
//				  <tr>
//					<td width="5" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_04.gif" width="5" height="19"></td>
//					<td align="left" bgcolor="#FFFFF7"><table width="100%" border="0" cellspacing="0" cellpadding="0">
//					  
//					  <tr>
//						<td height="150" align="left" valign="top">
//							<div id="list" style="overflow: auto; z-index:1; height: 150px;">
//								'.$data.'
//							</div>
//						</td></tr>
//							  <tr>
//						    <td><br><table width="95%" border="0" align="center" cellpadding="0" cellspacing="0">
//						      <tr>
//							       <td height="1" class="dotline"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/10x10.gif" width="10" height="1"></td>
//						      </tr>
//						     </table></td>
//						     </tr>
//                 </table></td>
//					<td width="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_06.gif" width="6" height="6"></td>
//				  </tr>
//				  <tr>
//					<td width="5" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_07.gif" width="5" height="6"></td>
//					<td height="6" background="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_08.gif" width="5" height="6"></td>
//					<td width="6" height="6"><img src="'.$image_path.'/'.$LAYOUT_SKIN.'/can_board_09.gif" width="6" height="6"></td>
//				  </tr>
//					</table></td>
//			  </tr>
//			</table>';
##Title	
if($title == ''){
	$title = '<th>Title Undefined</th>';
	}

##Get all category name
//$RemarkDetailsArr = array('a','b');	
##Remarks Layer			

$RemarksLayer .= '';
$thisRemarksType = 'type';
				//$RemarksLayer .= '<div id="remarkDiv_type class="selectbox_layer" style="width:400px;">'."\r\n";
                $RemarksLayer .='<div class = "stickyHeader" id= "stickyRemarks" style = "background-color: #f3f3f3;position: sticky; top: 0; margin-bottom: 1px;">';
                    $RemarksLayer .= '<div style="display: inline;">';
                        $RemarksLayer .= '&nbsp';
                    $RemarksLayer .= '</div>';
                    $RemarksLayer .= '<div style="display: inline; float: right; padding-right: 2px;">';
                        $RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" style="float:right"></a>'."\r\n";
                    $RemarksLayer .= '</div>';
                $RemarksLayer .= '</div>';
                $RemarksLayer .= '<div id="tableDiv" style="overflow-y: auto; max-height: 250px;">';
					$RemarksLayer .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
						$RemarksLayer .= '<tbody>'."\r\n";
// 							$RemarksLayer .= '<tr>'."\r\n";
// 								$RemarksLayer .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
// 									$RemarksLayer .= '<a href="javascript:hideRemarkLayer(\''.$thisRemarksType.'\');" ><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif" style="float:right"></a>'."\r\n";
// 								$RemarksLayer .= '</td>'."\r\n";
// 							$RemarksLayer .= '</tr>'."\r\n";
							$RemarksLayer .= '<tr>'."\r\n";
								$RemarksLayer .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
									$RemarksLayer .= '<table class="common_table_list_v30 view_table_list_v30">'."\r\n";
										$RemarksLayer .= '<thead>'."\n";
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= $title."\n";
											$RemarksLayer .= '</tr>'."\n";
										$RemarksLayer .= '</thead>'."\n";
										$RemarksLayer .= '<tbody>'."\n";
										foreach ($RemarkDetailsArr as $RemarkDetail){
											$RemarksLayer .= '<tr>'."\n";
												$RemarksLayer .= $RemarkDetail."\n";
											$RemarksLayer .= '</tr>'."\n";
										}							
										$RemarksLayer .= '</tbody>'."\n";
									$RemarksLayer .= '</table>'."\r\n";
								$RemarksLayer .= '</td>'."\r\n";
							$RemarksLayer .= '</tr>'."\r\n";
						$RemarksLayer .= '</tbody>'."\r\n";
					$RemarksLayer .= '</table>'."\r\n";
				$RemarksLayer .= '</div>';
				//$RemarksLayer .= '</div>'."\r\n";
		
		if($flag!=''){
			$output = $RemarksLayer;
		}
		
	echo $output;
}

intranet_closedb();

?>

