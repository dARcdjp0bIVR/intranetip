<?php


$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$LibUser = new libuser($UserID);	
	$CurrentPage = "PageMgtEventStat";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
		
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        
        $EventInfoArr = $libenroll->GET_EVENTINFO($EventEnrolID);

        /*
        EnrolEventID, Quota, Approved, EventTitle, Description,
		UpperAge, LowerAge, Gender, 
		AttachmentLink1, AttachmentLink2, AttachmentLink3, 
		AttachmentLink4, AttachmentLink5, 
		ApplyStartTime, ApplyEndTime, ApplyUserType, ApplyMethod, 
		DateInput, DateModified
        */
        
        $TAGS_OBJ[] = array($EventInfoArr[3]." ".$eEnrollment['app_stu_list'], "", 1);

        $linterface->LAYOUT_START();

        $ParArr['EnrolEventID'] = $EventEnrolID;
        $ParArr['RecordStatus'] = $RecordStatus;
        $StudentArr = $libenroll->GET_APPLY_EVENT_STUDENT($ParArr);
        //debug_r($StudentArr);
        if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update")."<br/>";
?>

<form name="form1" action="event_stu_remove_update.php" method="POST">
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right"><?= $SysMsg ?></td>
</tr>
<tr><td>
<table id="html_body_frame" width="95%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr class="tablegreentop">
		<td class="tableTitle" nowrap align="center"><span class="tabletoplink"><?= $eEnrollment['student_name']?></span></td>
		<td class="tableTitle" nowrap align="center"><span class="tabletoplink"><?= $eEnrollment['status'] ?></span></td>
		<td class="tableTitle" align="center"><span class="tabletoplink"><?= $eEnrollment['decline_apply']?> <br/><input type="checkbox" id="reject" name="reject" value="" onClick="(this.checked) ? setChecked(1, document.form1, 'uid[]') : setChecked(0, document.form1, 'uid[]');"></span></td>
	</tr>
	<?
		for ($i = 0; $i < sizeof($StudentArr); $i++) {
			switch($StudentArr[$i][2]) {
				case 0:
						$Status = $eEnrollment['waiting'];
						break;
				case 2:
						$Status = $eEnrollment['approved'];
						break;
				case -1:
						$Status = $eEnrollment['denied'];
						break;
				default:
						$Status = "---";
						break;
			}
	?>
	<tr class="tablegreenrow<?= (($i % 2)+1) ?>">
		<td align="center"><span class="tabletext"><?= $LibUser->getNameWithClassNumber($StudentArr[$i][0]) ?></span></td>
		<td align="center"><span class="tabletext"><?= $Status ?></span></td>
		<td align="center"><span class="tabletext"><input type="checkbox" id="uid[]" name="uid[]" value="<?= $StudentArr[$i][0]?>"></span></td>
	</tr>
	<? } ?>
</table>

</td></tr>

<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_remove, "button", "AlertPost (this.form, 'event_stu_remove_update.php' , '".$eEnrollment['js_confirm_del']."')")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "reset")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='event_status.php'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>

</table>

<input type="hidden" id="EventEnrolID" name="EventEnrolID" value="<?= $EventEnrolID?>" />
</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>