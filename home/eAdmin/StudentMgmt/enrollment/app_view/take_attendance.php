<?php 
//using: 
#############################################
#
#   Date:   2020-06-24  Tommy
#           - changed date checking for helper to change today attendance
#
##############################################

$PATH_WRT_ROOT = "../../../../../";
$PATH_WRT_ROOT_NEW = "../../../../..";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/libstudentattendance_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."lang/eclassapp_lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

$uid = IntegerSafe($_GET['uid']);
$id = IntegerSafe($_GET['id']);

$UserID = $uid;
$_SESSION['UserID'] = $uid;

$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

intranet_auth();
intranet_opendb();

 if(!isset($id)||$id==""||!isset($charactor)||$charactor==""||!isset($date)||$date=="")header('Location:attendance_list.php');


if(!isset($status_switch) || $status_switch=="")
{
	$status_switch = "all";
}

$libdb = new libdb();
$lu = new libuser();
$linterface = new interface_html();
$libenroll = new libclubsenrol();


$conds = "";
$TABLES = "";
$Title = "";
$student_list = "";
$present_count = 0;
$exempt_count = 0;
$absent_count = 0;
$late_count = 0;
$nottake_count = 0;
$order = "";
$status_array = "";
$student_array = array();


if($charactor=="club"){
	//get students list
	$sql = "select 
			   iu.UserID,
			   ".getNameFieldByLang("iu.")." as name,
			   yc.".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN').",
			   ycu.ClassNumber	  						   					 
			from
			   INTRANET_ENROL_GROUP_DATE as iegd
			   inner Join INTRANET_USERGROUP as iug On (iegd.EnrolGroupID = iug.EnrolGroupID)
		       inner Join INTRANET_USER as iu On (iug.UserID = iu.UserID And iu.RecordType = 2)
		       LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
    		   LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID )
    		   LEFT OUTER JOIN YEAR as y ON (yc.YearID = y.YearID) 		
			where
			    iegd.GroupDateID = '".$id."' and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			order by
				y.Sequence, yc.Sequence, ycu.ClassNumber, iu.EnglishName    		
			";
	$result = $libdb->returnArray($sql);
	
	//get club title
    $sql2="select ig.Title,ig.TitleChinese,iegd.EnrolGroupID
    	   from INTRANET_ENROL_GROUP_DATE as iegd
			    inner join INTRANET_ENROL_GROUPINFO as iegi on iegi.EnrolGroupID=iegd.EnrolGroupID
			    inner join INTRANET_GROUP as ig on ig.GroupID = iegi.GroupID
    		where  iegd.GroupDateID = '".$id."'";
    $result2 = $libdb->returnArray($sql2);
    $Title = Get_Lang_Selection($result2[0][1], $result2[0][0]);
    $EnrolID = $result2[0][2];
    
    $disableSubmit = false;
    
    $isClubHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolID, "Club");
    
    if($isClubHelper && $libenroll->disableHelperModificationRightLimitation !=true){
        $sql = "SELECT GroupDateID, DateModified, LastModifiedRole FROM INTRANET_ENROL_GROUP_ATTENDANCE WHERE EnrolGroupID = '$EnrolID' AND GroupDateID = '$id'";
        $haveResult = $libenroll->returnArray($sql,2);
        
        $sql = "SELECT ActivityDateStart, ActivityDateEnd FROM INTRANET_ENROL_GROUP_DATE WHERE GroupDateID = ". $haveResult[0]["GroupDateID"];
        $activityDate = $libenroll->returnArray($sql,2);
        
        if (count($haveResult) > 0){
            $disableSubmit = true;
           
//             if (date("Y-m-d") == date("Y-m-d", strtotime($haveResult[0]['DateModified']))  && ($result[0]['LastModifiedBy'] == $_SESSION['UserID']))
            if (date("Y-m-d") == date("Y-m-d", strtotime($activityDate[0]['ActivityDateStart'])) && date("Y-m-d") == date("Y-m-d", strtotime($activityDate[0]['ActivityDateEnd'])))
            {
                $disableSubmit = false;
            }
        }
    }

}

if($charactor=="activity"){
	$sql = "select
			   iu.UserID,
			   ".getNameFieldByLang("iu.")." as name,
			   yc.".Get_Lang_Selection('ClassTitleB5', 'ClassTitleEN').",
			   ycu.ClassNumber					   			   		
			from
			   INTRANET_ENROL_EVENT_DATE as ieed
			   inner Join INTRANET_ENROL_EVENTSTUDENT as iees On (ieed.EnrolEventID = iees.EnrolEventID And iees.RecordStatus = 2)
			   inner Join INTRANET_USER as iu On (iees.StudentID = iu.UserID And iu.RecordType = 2)
			   LEFT OUTER JOIN YEAR_CLASS_USER as ycu ON (iu.UserID = ycu.UserID)
    		   LEFT OUTER JOIN YEAR_CLASS as yc ON (ycu.YearClassID = yc.YearClassID )
    		   LEFT OUTER JOIN YEAR as y ON (yc.YearID = y.YearID) 			
			where
			    ieed.EventDateID = '".$id."' and yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
			order by
				y.Sequence, yc.Sequence, ycu.ClassNumber, iu.EnglishName
			";
	$result = $libdb->returnArray($sql);
	$sql2 = "select ieei.EventTitle,ieed.EnrolEventID
			 from  INTRANET_ENROL_EVENT_DATE as ieed
			       inner join INTRANET_ENROL_EVENTINFO as ieei on ieei.EnrolEventID=ieed.EnrolEventID
			 where ieed.EventDateID = '".$id."'";
	$result2 = $libdb->returnArray($sql2);
	$Title = $result2[0][0];
	$EnrolID = $result2[0][1];
}
$all_count = count($result);
for($j=0;$j<sizeof($result);$j++){
	list($user_id,$Student_name,$studentClassName,$studentClassNumber) = $result[$j];
	if($charactor=="club"){
		$sql1="select RecordStatus,GroupAttendanceID
		   from INTRANET_ENROL_GROUP_ATTENDANCE
		   where StudentID = '".$user_id."' and GroupDateID = '".$id."'";
		$result1 = $libdb->returnArray($sql1);
	}
	if($charactor=="activity"){
		$sql1="select RecordStatus,EventAttendanceID
		   from INTRANET_ENROL_EVENT_ATTENDANCE
		   where StudentID = '".$user_id."' and EventDateID = '".$id."'";
		$result1 = $libdb->returnArray($sql1);
	}
	list($status,$recordID) = $result1[0];
    if(count($result1)==0){
    	$status=-1;
    	$recordID=-1;
    }
	if($status==1){
		$present_count++;
	}
	if($status==2){
		$exempt_count++;
	}
	if($status==3) {
		$absent_count++;
	}
	if($sys_custom['eEnrolment']['Attendance_Late']){
		if($status==4) {
		   $late_count++;
	    }
	}
	
	$status_array.="<input type='hidden' id='status_".$user_id."' name='status_".$user_id."' value='".$status."'>";
	$studentsname_array.="<input type='hidden' id='studentsName_".$user_id."' name='studentsName_".$user_id."' value='".$Student_name."'>";
	//$recordID_array.="<input type='hidden' id='recordID_".$user_id."' name='recordID_".$user_id."' value='".$recordID."'>";
	
	$student_array[] = $user_id;
	$photo = $lu->GET_OFFICIAL_PHOTO_BY_USER_ID($user_id);
   
    
	if($status==-1){
	    $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
	    		              <a href='javascript:choose(".$user_id.");' id='a_".$user_id."'>
	    		                <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
				                <p>".$Student_name."<br>(".$studentClassName." - ".$studentClassNumber.")</p>
				              </a>
				          </li>";

	
	}
	if($status==1){
		$student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
				              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#75b70d;' id='a_".$user_id."'>
		                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
			                    <p>".$Student_name."<br>(".$studentClassName." - ".$studentClassNumber.")</p>
			                    <h2 style='color:#75b70d;' id='h2_".$user_id."'>".$Lang['eEnrolment']['Attendance']['Present']."</h2>
			                  </a>
			              </li>";

	}
	
	if($status==2){
		$student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
				              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#00a7d8;' id='a_".$user_id."'>
		                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
			                    <p>".$Student_name."<br>(".$studentClassName." - ".$studentClassNumber.")</p>
			                    <h2 style='color:#0094bf;' id='h2_".$user_id."'>".$Lang['eEnrolment']['Attendance']['Exempt']."</h2>
			                  </a>
			              </li>";

	}
    if ($status==3){		
		$student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
				              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#fe5353;' id='a_".$user_id."'>
		                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>		
			                    <p>".$Student_name."<br>(".$studentClassName." - ".$studentClassNumber.")</p>		
			                    <p class='absent' id='ab_".$user_id."'></p>		
			                    <h2 style='color:#d92b2b;' id='h2_".$user_id."'>".$Lang['eEnrolment']['Attendance']['Absent']."</h2>		
		                      </a>		
		                  </li>";

	}
	if($status==4){
	    if($sys_custom['eEnrolment']['Attendance_Late']){
		$student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none'>
				              <a href='javascript:choose(".$user_id.");' style='border:2px;border-style: solid; border-color:#FE9A2E;' id='a_".$user_id."'>
		                        <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
			                    <p>".$Student_name."<br>(".$studentClassName." - ".$studentClassNumber.")</p>
			                    <h2 style='color:#FE9A2E;' id='h2_".$user_id."'>".$Lang['eEnrolment']['Attendance']['Late']."</h2>
			                  </a>
			              </li>";	  	
	    }else{
	    $student_list .= "<li id='li_".$user_id."'><input type='checkbox' name='students_status[]' value='".$user_id."' id='c_".$user_id."' style='display:none' >
	    		              <a href='javascript:choose(".$user_id.");' id='a_".$user_id."'>
	    		                <img src='".$photo[1]."' class=ui-li-thumb style='width:80px;height:104px;'>
				                <p>".$Student_name."<br>(".$studentClassName." - ".$studentClassNumber.")</p>
				              </a>
				          </li>"; 	
	    }
	}

	
}
if($sys_custom['eEnrolment']['Attendance_Late']){
	$nottake_count = $all_count-$present_count-$exempt_count-$absent_count-$late_count;	
}else{
	$nottake_count = $all_count-$present_count-$exempt_count-$absent_count;
}
$student_array_string = implode(";",$student_array);
echo $libeClassApp->getAppWebPageInitStart();
?>
<script>
    var array_string = '<?= $student_array_string?>' ;
    var student_array = array_string.split(';');
    
	jQuery(document).ready( function() {
		$("#status_select").hide(); 
		$("#mode").val("single");
		//document.getElementById("<?=$status_switch?>").selected=true;
		//$('select').selectmenu('refresh', true);

	});

	function show_status_select(){

		 $("#mode").val("multiple");
		 $("#status_select").show();
	     $("#mutiple_select").hide();

	}

	function show_mutiple_select(){

		 $("#mode").val("single");
		 $("input[type='checkbox']").prop("checked",false).checkboxradio("refresh");
	     $("img.selected").remove();
		 $("#status_select").hide();
	     $("#mutiple_select").show();

	}
		
	function refresh(){
		
		   document.form1.action="take_attendance.php";
		   document.form1.submit();
	}

    function choose(userID){
        <? if(!$disableSubmit) {?>
    	  var hasLate = '<?=$sys_custom['eEnrolment']['Attendance_Late']?true:false;?>';
          if($("#mode").val()=="multiple"){
		        if(!$("#c_"+userID)[0].checked){
		    	   $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
		    	   $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
		        }
		        else{
			       $("#c_"+userID).prop("checked",false).checkboxradio("refresh");	
			       $("#s_"+userID).remove();
			       
			    }
          } 
          else{
              if($("#status_"+userID).val()==-1){
                  
                  $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"}).append("<h2 style='color:#75b70d;' id='h2_"+userID+"'><?=$Lang['eEnrolment']['Attendance']['Present']?></h2>");
		              
                  $("#status_"+userID).val(1);

                  var present_count = parseInt($("#present_count").val())+1;
                  var nottake_count = parseInt($("#nottake_count").val())-1;
                  $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");
                  $("#nottake").text("<?=$Lang['eClassApp']['eEnrollment']['NotTake']?> ("+nottake_count+")");   
                  $("#present_count").val(present_count);
                  $("#nottake_count").val(nottake_count);                  
                  $('select').selectmenu('refresh', true);
               }    
              else if($("#status_"+userID).val()==1){
                  
                  $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"});
                  $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?>").css({"color":"#0094bf"});              
                  $("#status_"+userID).val(2);

                  var exempt_count = parseInt($("#exempt_count").val())+1;
                  var present_count = parseInt($("#present_count").val())-1;
                  $("#exempt").text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?> ("+exempt_count+")");   
                  $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");                 
                  $("#exempt_count").val(exempt_count);
                  $("#present_count").val(present_count);                  
                  $('select').selectmenu('refresh', true);
              }
              
              else if($("#status_"+userID).val()==2){
                  
             	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append(" <p class='absent' id='ab_"+userID+"'></p>");
                 $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Absent']?>").css({"color":"#d92b2b"});              
	             	 
                 $("#status_"+userID).val(3);

                 var absent_count = parseInt($("#absent_count").val())+1;
                 var exempt_count = parseInt($("#exempt_count").val())-1;
                 $("#absent").text("<?=$Lang['eEnrolment']['Attendance']['Absent']?> ("+absent_count+")");
                 $("#exempt").text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?> ("+exempt_count+")");
                 $("#absent_count").val(absent_count);
                 $("#exempt_count").val(exempt_count);                 
                 $('select').selectmenu('refresh', true);
                 
              }
              
              else if($("#status_"+userID).val()==3){

                   if(hasLate){
	             	   $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#FE9A2E"});
	               	   $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Late']?>").css({"color":"#FE9A2E"});              
	                   $("#ab_"+userID).remove();           	 
	                   $("#status_"+userID).val(4);
	
	                   var absent_count = parseInt($("#absent_count").val())-1;
	                   var late_count = parseInt($("#late_count").val())+1;
	                   $("#absent").text("<?=$Lang['eEnrolment']['Attendance']['Absent']?> ("+absent_count+")");   
	                   $("#late").text("<?=$Lang['eEnrolment']['Attendance']['Late']?> ("+late_count+")");                 
	                   $("#absent_count").val(absent_count);
	                   $("#late_count").val(late_count);                  
	                   $('select').selectmenu('refresh', true);                       	
                   }else{
	             	   $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"});
	               	   $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Present']?>").css({"color":"#75b70d"});              
	                   $("#ab_"+userID).remove();           	 
	                   $("#status_"+userID).val(1);
	
	                   var absent_count = parseInt($("#absent_count").val())-1;
	                   var present_count = parseInt($("#present_count").val())+1;
	                   $("#absent").text("<?=$Lang['eEnrolment']['Attendance']['Absent']?> ("+absent_count+")");   
	                   $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");                 
	                   $("#absent_count").val(absent_count);
	                   $("#present_count").val(present_count);                  
	                   $('select').selectmenu('refresh', true);                  	
                   }

               }     
               
               else if($("#status_"+userID).val()==4){
                  if(hasLate){
	                   $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"});
	               	   $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Present']?>").css({"color":"#75b70d"});              
	                   $("#ab_"+userID).remove();           	 
	                   $("#status_"+userID).val(1);
	
	                   var late_count = parseInt($("#late_count").val())-1;
	                   var present_count = parseInt($("#present_count").val())+1;
	                   $("#late").text("<?=$Lang['eEnrolment']['Attendance']['Late']?> ("+late_count+")");   
	                   $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");                 
	                   $("#late_count").val(late_count);
	                   $("#present_count").val(present_count);                  
	                   $('select').selectmenu('refresh', true);         	
                  }else{
                  	
	                   $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"}).append("<h2 style='color:#75b70d;' id='h2_"+userID+"'><?=$Lang['eEnrolment']['Attendance']['Present']?></h2>");
			              
	                   $("#status_"+userID).val(1);
	
	                   var present_count = parseInt($("#present_count").val())+1;
	                   var nottake_count = parseInt($("#nottake_count").val())-1;
	                   $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");
	                   $("#nottake").text("<?=$Lang['eClassApp']['eEnrollment']['NotTake']?> ("+nottake_count+")");   
	                   $("#present_count").val(present_count);
	                   $("#nottake_count").val(nottake_count);                  
	                   $('select').selectmenu('refresh', true);
                  	
                  }

               }             
          }
         <? } ?>
    }

    function choose_all(){

		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#li_"+userID).css("display")=="block"){
				 if(!$("#c_"+userID)[0].checked){
	            	 $("#c_"+userID).prop("checked",true).checkboxradio("refresh");
		             $("#a_"+userID).append("<img src='<?=$image_path?>/<?=$LAYOUT_SKIN?>/smartcard/icon_selected.png' class='selected' id='s_"+userID+"'>");
	             }
             }
         }
    }

    function turn_present(){
    	 var hasLate = '<?=$sys_custom['eEnrolment']['Attendance_Late']?true:false;?>';
         var present_count = 0;
         var exempt_count = 0;
         var absent_count = 0;         
         var nottake_count = 0;
         var late_count = 0;
         
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 
             if($("#c_"+userID)[0].checked){

			     if($("#status_"+userID).val()==-1||($("#status_"+userID).val()==4&&!hasLate)){
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"}).append("<h2 style='color:#75b70d;' id='h2_"+userID+"'><?=$Lang['eEnrolment']['Attendance']['Present']?></h2> ");                 
                 }
                 else{
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#75b70d"});                 
                 	 $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Present']?>").css({"color":"#75b70d"});       
                  	 $("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                 }

                 $("#status_"+userID).val(1);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();                            
             }

             if($('#status_'+userID).val()==1) present_count++;
             else if($('#status_'+userID).val()==2) exempt_count++;
             else if($('#status_'+userID).val()==3) absent_count++;
             else if(hasLate){
             	if($('#status_'+userID).val()==-1)nottake_count++;
             	else if($('#status_'+userID).val()==4) late_count++;
             }else{
             	if($('#status_'+userID).val()==-1||$('#status_'+userID).val()==4) nottake_count++;
             }
             
             
             
         }
         $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");
         $("#exempt").text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?> ("+exempt_count+")");   
         $("#absent").text("<?=$Lang['eEnrolment']['Attendance']['Absent']?> ("+absent_count+")");                 
         $("#nottake").text("<?=$Lang['eClassApp']['eEnrollment']['NotTake']?> ("+nottake_count+")");  
         if(hasLate){
            $("#late").text("<?=$Lang['eEnrolment']['Attendance']['Late']?> ("+late_count+")");  
         }          
         $("#present_count").val(present_count);
         $("#exempt_count").val(exempt_count);
         $("#absent_count").val(absent_count);
         $("#nottake_count").val(nottake_count);
         if(hasLate){
            $("#late_count").val(late_count);
         } 
         $('select').selectmenu('refresh', true);
         
    }
    
    function turn_exempt(){
    	 var hasLate = '<?=$sys_custom['eEnrolment']['Attendance_Late']?true:false;?>';
    	 var present_count = 0;
         var exempt_count = 0;
         var absent_count = 0;
         var nottake_count = 0;
         var late_count = 0;
         
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
            if($("#c_"+userID)[0].checked){
                               
                if($("#status_"+userID).val()==-1||($("#status_"+userID).val()==4&&!hasLate)){
                	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"}).append("<h2 style='color:#0094bf;' id='h2_"+userID+"'><?=$Lang['eEnrolment']['Attendance']['Exempt']?></h2> ");                 
                }
                else{
                	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#00a7d8"});                 
                	 $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?>").css({"color":"#0094bf"});       
                 	 $("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                }

                $("#status_"+userID).val(2);                 
           	    $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			    $("#s_"+userID).remove();
	                                 
            }
            if($('#status_'+userID).val()==1) present_count++;
            else if($('#status_'+userID).val()==2) exempt_count++;
            else if($('#status_'+userID).val()==3) absent_count++;
            else if(hasLate){
             	if($('#status_'+userID).val()==-1)nottake_count++;
             	else if($('#status_'+userID).val()==4) late_count++;
            }else{
             	if($('#status_'+userID).val()==-1||$('#status_'+userID).val()==4) nottake_count++;
            }
         } 
         
		 $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");
         $("#exempt").text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?> ("+exempt_count+")");   
         $("#absent").text("<?=$Lang['eEnrolment']['Attendance']['Absent']?> ("+absent_count+")");                 
         $("#nottake").text("<?=$Lang['eClassApp']['eEnrollment']['NotTake']?> ("+nottake_count+")");     
         if(hasLate){
            $("#late").text("<?=$Lang['eEnrolment']['Attendance']['Late']?> ("+late_count+")");  
         }         
         $("#present_count").val(present_count);
         $("#exempt_count").val(exempt_count);
         $("#absent_count").val(absent_count);
         $("#nottake_count").val(nottake_count);
         if(hasLate){
            $("#late_count").val(late_count);
         } 
         $('select').selectmenu('refresh', true);
   }
    
   function turn_absent(){
   	    var hasLate = '<?=$sys_custom['eEnrolment']['Attendance_Late']?true:false;?>';
    	var present_count = 0;
        var exempt_count = 0;
        var absent_count = 0;
        var nottake_count = 0;
        var late_count = 0;
		
		for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
             if($("#c_"+userID)[0].checked){
                 
                 
                 if($("#status_"+userID).val()==-1||($("#status_"+userID).val()==4&&!hasLate)){
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p><h2 style='color:#d92b2b;' id='h2_"+userID+"'><?=$Lang['eEnrolment']['Attendance']['Absent']?></h2> ");                 
                 }
                 else if($("#status_"+userID).val()==1||$("#status_"+userID).val()==2||$("#status_"+userID).val()==4){

                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#fe5353"}).append("<p class='absent' id='ab_"+userID+"'></p>");                 
                 	 $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Absent']?>").css({"color":"#d92b2b"});              
                 	                   	
                 }

              	          	 		             
                 $("#status_"+userID).val(3);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();
	                                 
             }

             if($('#status_'+userID).val()==1) present_count++;
             else if($('#status_'+userID).val()==2) exempt_count++;
             else if($('#status_'+userID).val()==3) absent_count++;
             else if(hasLate){
             	if($('#status_'+userID).val()==-1)nottake_count++;
             	else if($('#status_'+userID).val()==4) late_count++;
             }else{
             	if($('#status_'+userID).val()==-1||$('#status_'+userID).val()==4) nottake_count++;
             }
         }
		$("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");
        $("#exempt").text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?> ("+exempt_count+")");   
        $("#absent").text("<?=$Lang['eEnrolment']['Attendance']['Absent']?> ("+absent_count+")");                 
        $("#nottake").text("<?=$Lang['eClassApp']['eEnrollment']['NotTake']?> ("+nottake_count+")");    
        if(hasLate){
            $("#late").text("<?=$Lang['eEnrolment']['Attendance']['Late']?> ("+late_count+")");  
        }           
        $("#present_count").val(present_count);
        $("#exempt_count").val(exempt_count);
        $("#absent_count").val(absent_count);
        $("#nottake_count").val(nottake_count);
        if(hasLate){
            $("#late_count").val(late_count);
        } 
        $('select').selectmenu('refresh', true);
    
    }
    
    function turn_late(){
    	 var hasLate = '<?=$sys_custom['eEnrolment']['Attendance_Late']?true:false;?>';
         var present_count = 0;
         var exempt_count = 0;
         var absent_count = 0;         
         var nottake_count = 0;
         var late_count = 0;
         
		 for(var i=0; i<student_array.length;i++){
			 var userID = student_array[i];
			 
             if($("#c_"+userID)[0].checked){

			     if($("#status_"+userID).val()==-1){
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#FE9A2E"}).append("<h2 style='color:#FE9A2E;' id='h2_"+userID+"'><?=$Lang['eEnrolment']['Attendance']['Late']?></h2> ");                 
                 }
                 else{
                 	 $("#a_"+userID).css({"border":"2px","border-style":"solid","border-color":"#FE9A2E"});                 
                 	 $("#h2_"+userID).text("<?=$Lang['eEnrolment']['Attendance']['Late']?>").css({"color":"#FE9A2E"});       
                  	 $("#ab_"+userID).remove();           	 		                      	 		                              	                         	                   	
                 }

                 $("#status_"+userID).val(4);                 
            	 $("#c_"+student_array[i]).prop("checked",false).checkboxradio("refresh");
			     $("#s_"+userID).remove();                            
             }

             if($('#status_'+userID).val()==1) present_count++;
             else if($('#status_'+userID).val()==2) exempt_count++;
             else if($('#status_'+userID).val()==3) absent_count++;
             else if(hasLate){
             	if($('#status_'+userID).val()==-1)nottake_count++;
             	else if($('#status_'+userID).val()==4) late_count++;
             }else{
             	if($('#status_'+userID).val()==-1||$('#status_'+userID).val()==4) nottake_count++;
             }
             
             
             
         }
         $("#present").text("<?=$Lang['eEnrolment']['Attendance']['Present']?> ("+present_count+")");
         $("#exempt").text("<?=$Lang['eEnrolment']['Attendance']['Exempt']?> ("+exempt_count+")");   
         $("#absent").text("<?=$Lang['eEnrolment']['Attendance']['Absent']?> ("+absent_count+")");                 
         $("#nottake").text("<?=$Lang['eClassApp']['eEnrollment']['NotTake']?> ("+nottake_count+")");  
         if(hasLate){
            $("#late").text("<?=$Lang['eEnrolment']['Attendance']['Late']?> ("+late_count+")");  
         }          
         $("#present_count").val(present_count);
         $("#exempt_count").val(exempt_count);
         $("#absent_count").val(absent_count);
         $("#nottake_count").val(nottake_count);
         if(hasLate){
            $("#late_count").val(late_count);
         } 
         $('select').selectmenu('refresh', true);
         
    }

    function submit(){
    	 if (confirm("<?=$Lang['eClassApp']['eEnrollment']['AreYouSureToSubmit'] ?>"))
    	   {
    	        document.form1.action = "handle_attendance.php";
    	        document.form1.submit(); 
    	   }
    	 
       
    }
    
    function back(){
		 window.location.href="attendance_list.php?date=<?=$date?>&charactor=<?=$charactor?>&token=<?=$token?>&uid=<?=$uid?>&ul=<?=$ul?>&parLang=<?=$parLang?>";			 			 
    }
    
  
    function select(){
    	var hasLate = '<?=$sys_custom['eEnrolment']['Attendance_Late']?true:false;?>';
    	var selected=document.getElementById("status_switch");
    	var selectedID = selected.selectedIndex;
    	if(selectedID==0){
	   	    for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
			    $('#li_'+userID).show();
	        }   
        }
    	else if(selectedID==1){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==1){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    	else if(selectedID==2){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==2){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}
    	else if(selectedID==3){
	   		for(var i=0; i<student_array.length;i++){
				var userID = student_array[i];
				if($('#status_'+userID).val()==3){
					$('#li_'+userID).show();				
			    }
				else{
					$('#li_'+userID).hide();								
			    }
	        }   
    	}    
		else{
			if(hasLate){
			   if(selectedID==4){
				   		for(var i=0; i<student_array.length;i++){
							var userID = student_array[i];
							if($('#status_'+userID).val()==4){
								$('#li_'+userID).show();				
						    }
							else{
								$('#li_'+userID).hide();								
						    }
				        }   			
				}
				else if(selectedID==5){
				   		for(var i=0; i<student_array.length;i++){
							var userID = student_array[i];
							if($('#status_'+userID).val()==-1){
								$('#li_'+userID).show();				
						    }
							else{
								$('#li_'+userID).hide();								
						    }
				        }   			
				}
			}
			else{   			
		    	if(selectedID==4){
			   		for(var i=0; i<student_array.length;i++){
						var userID = student_array[i];
						if($('#status_'+userID).val()==-1||$('#status_'+userID).val()==4){
							$('#li_'+userID).show();				
					    }
						else{
							$('#li_'+userID).hide();								
					    }
			        }   
		    	}   		
			}	
	    }
   }
</script>
<style type="text/css">
    #body{
        
         background:#ffffff;
         text-shadow:none;
    }
    #show_class{
    
         text-shadow:none;
    }
    a:link { text-decoration: none; color: black;}
    a:active {text-decoration: none; color: black !important;} 
    a:hover {text-decoration: none;color: black !important;}  
    a:visited {text-decoration: none;color: black !important;} 
   
   	.header a:link { text-decoration: none; color: white;}
    .header a:active {text-decoration: none; color: white !important;} 
    .header a:hover {text-decoration: none;color: white !important;}  
    .header a:visited {text-decoration: none;color: white !important;} 
		
    .student_list {
    }
	.student_list .ui-content{
       padding:0;

    }
	.student_list .ui-listview li {
		float: left;
        width: 80px;
		height: 104px;
        margin: 10px;
        margin-top:0 ;
	    margin-left:10px ;		
	}
	.student_list .ui-listview li > .ui-btn {
		-webkit-box-sizing: border-box; /* include padding and border in height so we can set it to 100% */
		-moz-box-sizing: border-box;
		-ms-box-sizing: border-box;
		box-sizing: border-box;
		height: 100%;
		text-shadow:none;/*no shadow*/
		width:80px;
		padding-right:0;
 		border:2px; 
 		border-style: solid;  
 		border-color:#D8D8D8;
/*      border-color:#75b70d; */

        
	}
	.student_list .ui-listview li.ui-li-has-thumb .ui-li-thumb {
		height: auto; /* To keep aspect ratio. */
		max-width: 100%;
		max-height: none;
	}
	/* Make all list items and anchors inherit the border-radius from the UL. */
	.student_list .ui-listview li,
	.student_list .ui-listview li .ui-btn,
	.student_list .ui-listview .ui-li-thumb {
		-webkit-border-radius: inherit;
		border-radius: inherit;
		padding:0 !important;
		
	}
	/* Hide the icon */
	.student_list .ui-listview .ui-btn-icon-right:after {
		display: none;
	}
	/* Make text wrap. */
	.student_list .ui-listview p {
		white-space: normal;
		overflow: visible;
		position: absolute;
		left: 0;
		right: 0;
	}
	/* Text position */
	.student_list .ui-listview p {
		font-size: 0.8em;
		margin: 0;
        text-align:center;
		min-height: 20%;
		bottom:0;
		width:80px !important;
	}
	/* Semi transparent background and different position if there is a thumb. The button has overflow hidden so we don't need to set border-radius. */
	.ui-listview .ui-li-has-thumb p {
		background: rgba(255,255,255,.7);
	}

	.ui-listview .ui-li-has-thumb p {
		min-height: 20%;
	}
	.student_list .ui-listview .absent {
		width: auto;
		min-height: 100%;
		top: 0;
		left: 0;
		bottom: 0;
		background: rgba(255,255,255,.7);
		-webkit-border-top-right-radius: inherit;
		border-top-right-radius: inherit;
		-webkit-border-bottom-left-radius: inherit;
		border-bottom-left-radius: inherit;
		-webkit-border-bottom-right-radius: 0;
		border-bottom-right-radius: 0;
		
	}

	/* Animate focus and hover style, and resizing. */
	.student_list .ui-listview li,
	.student_list .ui-listview .ui-btn {
		-webkit-transition: all 500ms ease;
		-moz-transition: all 500ms ease;
		-o-transition: all 500ms ease;
		-ms-transition: all 500ms ease;
		transition: all 500ms ease;
	}
    .student_list .ui-listview .selected {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		right: 0;
    	bottom: 0;
    	opacity:0.8;
    	padding-right:2px;
        padding-bottom:2px;    	
        width:30%;
        z-index:1;   
    }
    
    .student_list .ui-listview h2 {
    
		white-space: normal;
		overflow: visible;
		position: absolute;
		top: 0;
		right: 0; 
	    font-size: 1em;
 	    margin-top:0.3em; 
 	    margin-right:0.3em; 
		text-shadow:0 0 2px #FFFFFF;
		z-index:1;
    }
</style>
</head>
	<body>
	
		<div data-role="page" id="body"  class="student_list">			
			<form name="form1" method="post" id="form1">
			
			    <div id="header" data-role="header" data-position="fixed" style="border:0;height:80px">
			       
			        <div id="show_class">	                
			
			           <table width="100%" style ="background-color:#429DEA;height:60px;font-size:1em;font-weight:normal;">
			
			             <tr>
			
			                <td style="padding-left:0em;">
			                   <table> 
			                     <tr><td rowspan="2">
			                    <a href="javascript:back();"> <img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/iMail/app_view/white/icon_outbox_white.png" style="width:2em; height:2em;" /></a>
			                     </td><td><?=$Title?>
			                     </td></tr>
			                     <tr><td><?=$date?></td></tr>
			                   </table>
			                </td>
			
			                <td  align= "right" style="padding-right:0.2em;">
								
								    <select name="status_switch" id="status_switch" data-mini="true" onchange="select()"">
								
								        <option value="all" id="all"><?=$Lang['Btn']['All']?> (<?=$all_count?>)</option>
								 						
								        <option value="present" id="present"><?=$Lang['eEnrolment']['Attendance']['Present']?> (<?=$present_count?>)</option>
								       
								        <option value="exempt" id="exempt"><?=$Lang['eEnrolment']['Attendance']['Exempt']?> (<?=$exempt_count?>)</option>															        
								        
										<option value="absent" id="absent"><?=$Lang['eEnrolment']['Attendance']['Absent']?> (<?=$absent_count?>)</option>
									
										<?php if($sys_custom['eEnrolment']['Attendance_Late']){?>										
								      	   <option value="late" id="late"><?=$Lang['eEnrolment']['Attendance']['Late']?> (<?=$late_count?>)</option>											

										<?php } ?>
															
								        <option value="nottake" id="nottake"><?=$Lang['eClassApp']['eEnrollment']['NotTake']?> (<?=$nottake_count?>)</option>
								
								    </select>								
								
			                </td>	              
			
			              </tr>
			
			            </table>
			
			          </div>
					<?php if(!$disableSubmit){ ?>
			          <div id="mutiple_select" align="right" style="background-color:white;height: 30px;font-size:1em;">     
			            <table style="">
			               <tr>
			                 <td>
			                    <a href="javascript:show_status_select();" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change.png" style="width:1.4em; height:1.4em;" /></a>
			                 </td>
			                 <td style="padding-right: 1em">
			                    <a href="javascript:show_status_select();" ><?=$Lang['eClassApp']['eEnrollment']['MultiSelect']?></a>
			                 </td>
			               </tr>
			               
			            </table>
			          </div>
			          <? } ?>
			          <div id="status_select" style="background-color:white;height: 30px;font-size:1em;" >
			            <table style="width:100%">
			               <tr>
			                 <td style="padding-left: 0.3em;">
			                    <a href="javascript:choose_all();" ><?=$Lang['Btn']['SelectAll']?></a>
			                 </td>
			                 <td>
				                 <table>
					                 <tr>
						                 <td >
						                    <a href="javascript:show_mutiple_select()" ><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/smartcard/icon_change_select.png" style="width:1.4em; height:1.4em;" /></a>
						                 </td>
						                 <td >
						                    <a href="javascript:show_mutiple_select()" style="color: grey;"><?=$Lang['eClassApp']['eEnrollment']['TurnTo']?></a>
						                 </td>
					                 </tr>
				                 </table>
			                 </td>
			                 <td >
			                    <a href="javascript:turn_present();" ><?=$Lang['eEnrolment']['Attendance']['Present']?></a>
			                 </td>
			                 <td >
			                    <a href="javascript:turn_exempt();" ><?=$Lang['eEnrolment']['Attendance']['Exempt']?></a>
			                 </td>
			                 <?php if($sys_custom['eEnrolment']['Attendance_Late']){?>			
				                 <td >
				                    <a href="javascript:turn_absent();" ><?=$Lang['eEnrolment']['Attendance']['Absent']?></a>
				                 </td>  
				                 <td  style="padding-right:0.1em">
				                    <a href="javascript:turn_late();" ><?=$Lang['eEnrolment']['Attendance']['Late']?></a>
				                 </td>
			                 <?php }else{ ?>
			                 	
				           		 <td  style="padding-right:0.1em">
				                    <a href="javascript:turn_absent();" ><?=$Lang['eEnrolment']['Attendance']['Absent']?></a>
				                 </td>      	
			                 <?php } ?>
			               </tr>
			               
			            </table>			
			          </div>		
			          
			       </div>
			
	            <div data-role="content">
	            
	            
	                 <ul data-role="listview" data-inset="true">
	                 
	                    <?=$student_list?>           
	            	            
			         </ul>
	            </div>
	            
				<?php if(!$disableSubmit){ ?>
	            <div id="footer" data-role="footer" data-position="fixed"  style ="background-color:white;height:70px" align="center">
	               <a href="javascript:submit();" class="ui-btn ui-corner-all" style ="background-color:#429DEA;display:block;margin-left:10px;margin-right:10px;
	                     padding-top:11px;padding-bottom:11px;font-size:1.3em;font-weight:normal;color:white !important;text-shadow:none;" ><?=$Lang['eClassApp']['eEnrollment']['Confirm']?></a>
	            </div>	 	            	            
		        <? }?>
  	    	    <input type="hidden" value = "<?=$id?>" name="id">
  	    	    <input type="hidden" value=<?=$EnrolID?> name="EnrolID" id="EnrolID">
			    <input type="hidden" value = "<?=$charactor?>" name="charactor">
			    <input type="hidden" value = "<?=$Title?>" name="title">
			    <input type="hidden" value = "<?=$date?>" name="date">
			    <input type="hidden" value = "<?=$student_array_string?>" name="student_list">
			    <input type="hidden" value = "" name="mode" id="mode"> 
			    <input type="hidden" value=<?=$token?> name="token">
                <input type="hidden" value=<?=$uid?> name="uid">
                <input type="hidden" value=<?=$ul?> name="ul">
                <input type="hidden" value=<?=$parLang?> name="parLang">
                <input type="hidden" value=<?=$present_count?> name="present_count" id="present_count">
                <input type="hidden" value=<?=$exempt_count?> name="exempt_count" id="exempt_count">
                <input type="hidden" value=<?=$absent_count?> name="absent_count" id="absent_count">
                <input type="hidden" value=<?=$late_count?> name="late_count" id="late_count">
                <input type="hidden" value=<?=$nottake_count?> name="nottake_count" id="nottake_count">
                
			     <?=$status_array?> 
			     <?=$studentsname_array?> 		     

			</form>

		</div>
		
	</body>
</html>

<?php

intranet_closedb();

?>