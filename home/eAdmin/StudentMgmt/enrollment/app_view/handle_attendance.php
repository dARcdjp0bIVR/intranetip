<?php 
//USING anna
/**************************
 * 
 *  2019-12-10 Ray
 *   added attendance log
 * 
 *  2018-12-04 Anna
 *   added new flag $sys_custom['eEnrolment']['AllAttendanceStatusSendPushMessageToParent']
 * 
 * 
 * *************************/


$PATH_WRT_ROOT = "../../../../../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.".$_SESSION['intranet_session_language'].".php");
include_once($PATH_WRT_ROOT.'includes/eClassApp/libeClassApp.php');
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

$uid = IntegerSafe($_POST['uid']);
$id = IntegerSafe($_POST['id']);
$student_list = IntegerSafe($student_list,";");

$UserID = $uid;
$_SESSION['UserID'] = $uid;
$libeClassApp = new libeClassApp();
$isTokenValid = $libeClassApp->isTokenValid($token, $uid, $ul);
if(!$isTokenValid) {
	echo $i_general_no_access_right	;
	exit;
}

intranet_auth();
intranet_opendb();

if(!isset($id)||$id==""||!isset($charactor)||$charactor==""||!isset($date)||$date=="")header('Location:attendance_list.php');

$log_row = date("Y-m-d H:i:s").' '.$_SERVER['REQUEST_URI'].' '.$_SESSION['UserID'].' '.$charactor.' ';
$libdb = new libdb();
$lu = new libuser($UserID);
$libenroll = new libclubsenrol();

//$isClubPIC = $libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club");
if($charactor=="club"){
	
	$DataArr['EnrolGroupID'] = IntegerSafe($EnrolID);
	$DataArr['GroupID'] = $libenroll->GET_GROUPID($DataArr['EnrolGroupID']);
	$DataArr['GroupDateID'] = $id;
	
	$isClubHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolID, "Club");
	
}else if($charactor=="activity"){
	
	$DataArr['EnrolEventID'] = IntegerSafe($EnrolID);
	$DataArr['EventDateID'] = $id;
	$isClubHelper = $libenroll->IS_ENROL_HELPER($_SESSION['UserID'], $EnrolID, "Activity");
}

if ($isClubHelper)
{
	$StaffRole = "H";
}
else
{
	$StaffRole = "A";
}	
$DataArr['StaffRole'] = $StaffRole;

if($student_list!=""){
	$student_array =explode(";",$student_list);
	### get parent student mapping
	$sql_parent = "SELECT 
					ip.ParentID, ip.StudentID
			from 
					INTRANET_PARENTRELATION AS ip 
					Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
			WHERE 
					ip.StudentID IN ('".implode("','", (array)$student_array)."') 
					AND iu.RecordStatus = 1";
	$studentParentAssoAry = BuildMultiKeyAssoc($libenroll->returnResultSet($sql_parent), 'StudentID', array('ParentID'), $SingleValue=1, $BuildNumericArray=1);	
	
	### build push message info array
	$messageInfoAry = array();
	$pushMsgCount = 0;

    if($charactor=="club"){
    	for($i=0;$i<count($student_array);$i++){
    		$old_status==-2;
    		$status = $_POST['status_'.$student_array[$i].''];
    		$studentsName = $_POST['studentsName_'.$student_array[$i].'']; 
    		if($status!=-1){
    			$DataArr['Status'] = $status;
    			$DataArr['StudentID'] = $student_array[$i];
    			//$libenroll->ADD_STU_GROUP_ATTENDENCE($DataArr);
    			//$AttendanceId = $_POST['recordID_'.$student_array[$i].''];			
				
    		  	$sql1="select GroupAttendanceID,RecordStatus
				   from INTRANET_ENROL_GROUP_ATTENDANCE
				   where StudentID = '".$DataArr['StudentID']."' and GroupDateID = '".$id."'";
    			$result1 = $libdb->returnArray($sql1);

    			if(count($result1)==0){
    				$AttendanceId = -1;
					$log_row .= '['.$DataArr['EnrolGroupID'].' '.$DataArr['GroupID'].' '.$DataArr['GroupDateID'].' '.$DataArr['StudentID'].' NULL '.$DataArr['Status'].']';
    			}else{
    				$AttendanceId = $result1[0][0];
    				$old_status = $result1[0][1];
					$log_row .= '['.$DataArr['EnrolGroupID'].' '.$DataArr['GroupID'].' '.$DataArr['GroupDateID'].' '.$DataArr['StudentID'].' '.$old_status.' '.$DataArr['Status'].']';
    			}
    			
    			if ($AttendanceId != -1) {
    				$sql = "Update
								INTRANET_ENROL_GROUP_ATTENDANCE
						Set
								DateModified = NOW(),
								LastModifiedBy = '".$_SESSION['UserID']."',
								LastModifiedRole = '".$DataArr['StaffRole']."',
								RecordStatus = '".$DataArr['Status']."'
						Where
								GroupAttendanceID = '".$AttendanceId."'
						";
    			}
    			else {
    				$sql = "
						INSERT INTO
										INTRANET_ENROL_GROUP_ATTENDANCE
										(GroupDateID, GroupID, EnrolGroupID, StudentID, DateInput, DateModified, LastModifiedBy, LastModifiedRole, RecordStatus)
						VALUES
										(
										'".$DataArr['GroupDateID']."',
										'".$DataArr['GroupID']."',
										'".$DataArr['EnrolGroupID']."',
										'".$DataArr['StudentID']."',
										NOW(),
										NOW(),
										'".$_SESSION['UserID']."',
										'".$DataArr['StaffRole']."',
										'".$DataArr['Status']."'
										)
					";
    			}
    			$libdb->db_db_query($sql);
    			$msg = 1;
    			
    			$SendPushMessage[$i] = false;
    			//For push message to parent
                if($DataArr['Status']==1){
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['present']['Content'];               
                }elseif($DataArr['Status']==2){
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['exempt']['Content'];                              	
                }elseif($DataArr['Status']==3){     
                    $SendPushMessage[$i] = true;
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['Absent']['Content'];                              	                	
                }elseif($DataArr['Status']==4){               	
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['Late']['Content'];                              	                	
                }
                
                if($sys_custom['eEnrolment']['AllAttendanceStatusSendPushMessageToParent']){
                    $SendPushMessage[$i] = true;
                }

		        $MessageCentent = str_replace("[StudentName]", $studentsName, $MessageCentent);
                $MessageCentent = str_replace("[DateInvolved]", $date, $MessageCentent);
                $MessageCentent = str_replace("[eEnrolmentTitle]", $title, $MessageCentent);
               	$MessageTitle = $Lang['AppNotifyMessage']['eEnrolment']['Title'];
                $_parentIdAry = $studentParentAssoAry[$DataArr['StudentID']];
                
				$_numOfParent = count($_parentIdAry);
				if ($_numOfParent > 0&&$old_status!=$DataArr['Status']) {
					for ($j=0; $j<$_numOfParent; $j++) {
						$__parentId = $_parentIdAry[$j];
						$messageInfoAry[$pushMsgCount]['relatedUserIdAssoAry'][$__parentId] = array($DataArr['StudentID']);
					}
					
					$messageInfoAry[$pushMsgCount]['messageTitle'] = $MessageTitle;
					$messageInfoAry[$pushMsgCount]['messageContent'] = $MessageCentent;
					$pushMsgCount++;
				}			
				//push message part end
    		}
    	}
    }else if($charactor=="activity"){
    	for($i=0;$i<count($student_array);$i++){
    		$old_status==-2;
    		$status = $_POST['status_'.$student_array[$i].''];
    		$studentsName = $_POST['studentsName_'.$student_array[$i].''];    	
    		if($status!=-1){
    			$DataArr['Status'] = $status;
    			$DataArr['StudentID'] = $student_array[$i];
    			//$AttendanceId = $_POST['recordID_'.$student_array[$i].''];    	
						 
    			$sql1="select EventAttendanceID,RecordStatus
		            from INTRANET_ENROL_EVENT_ATTENDANCE
		            where StudentID = '".$DataArr['StudentID']."' and EventDateID = '".$id."'";
    			$result1 = $libdb->returnArray($sql1);
    			
    			if(count($result1)==0){
    				$AttendanceId = -1;
					$log_row .= '['.$DataArr['EnrolEventID'].' '.$DataArr['EventDateID'].' '.$DataArr['StudentID'].' NULL '.$DataArr['Status'].']';
    			}else{
    				$AttendanceId = $result1[0][0];
    				$old_status = $result1[0][1];
					$log_row .= '['.$DataArr['EnrolEventID'].' '.$DataArr['EventDateID'].' '.$DataArr['StudentID'].' '.$old_status.' '.$DataArr['Status'].']';
    			} 
    			
			    //$libenroll->ADD_STU_EVENT_ATTENDENCE($DataArr);
    			if ($AttendanceId != -1) {
    				$sql = "Update
								INTRANET_ENROL_EVENT_ATTENDANCE
						Set
								DateModified = NOW(),
								LastModifiedBy = '".$_SESSION['UserID']."',
								LastModifiedRole = '".$DataArr['StaffRole']."',
								RecordStatus = '".$DataArr['Status']."'
						Where
								EventAttendanceID = '".$AttendanceId."'
						";
    			}
    			else {
    				$sql = "
						INSERT INTO
										INTRANET_ENROL_EVENT_ATTENDANCE
										(EventDateID, EnrolEventID, StudentID, DateInput, DateModified, LastModifiedBy, LastModifiedRole, RecordStatus)
						VALUES
										(
										'".$DataArr['EventDateID']."',
										'".$DataArr['EnrolEventID']."',
										'".$DataArr['StudentID']."',
										NOW(),
										NOW(),
										'".$_SESSION['UserID']."',
										'".$DataArr['StaffRole']."',
										'".$DataArr['Status']."'
										)
					";
    			}
    		    $libdb->db_db_query($sql);    			
    			$msg = 1;
    			
    			$SendPushMessage[$i] = false;
    			//For push message to parent
                if($DataArr['Status']==1){
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['present']['Content'];               
                }elseif($DataArr['Status']==2){
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['exempt']['Content'];                              	
                }elseif($DataArr['Status']==3){    
                     $SendPushMessage[$i] = true;
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['Absent']['Content'];                              	                	
                }elseif($DataArr['Status']==4){               	
                     $MessageCentent = $Lang['AppNotifyMessage']['eEnrolment']['Late']['Content'];                              	                	
                }
                if($sys_custom['eEnrolment']['AllAttendanceStatusSendPushMessageToParent']){
                    $SendPushMessage[$i] = true;
                }
                
		        $MessageCentent = str_replace("[StudentName]", $studentsName, $MessageCentent);
                $MessageCentent = str_replace("[DateInvolved]", $date, $MessageCentent);
                $MessageCentent = str_replace("[eEnrolmentTitle]", $title, $MessageCentent);
               	$MessageTitle = $Lang['AppNotifyMessage']['eEnrolment']['Title'];
                $_parentIdAry = $studentParentAssoAry[$DataArr['StudentID']];
                
				$_numOfParent = count($_parentIdAry);
				if ($_numOfParent > 0&&$old_status!=$DataArr['Status']) {
					for ($j=0; $j<$_numOfParent; $j++) {
						$__parentId = $_parentIdAry[$j];
						$messageInfoAry[$pushMsgCount]['relatedUserIdAssoAry'][$__parentId] = array($DataArr['StudentID']);
					}
					
					$messageInfoAry[$pushMsgCount]['messageTitle'] = $MessageTitle;
					$messageInfoAry[$pushMsgCount]['messageContent'] = $MessageCentent;
					$pushMsgCount++;
				}			
				//push message part end
    		}
    	}
    }	

    if($sys_custom['eEnrolment']['PushMessageToParent'] && in_array(true,$SendPushMessage)){
        ### send notification to parent
	    $successAry['sendPushMessage'] = $libeClassApp->sendPushMessage($messageInfoAry, $MessageTitle, 'MULTIPLE MESSAGES');
    }
}
$libenroll->log($log_row);
intranet_closedb();
header('Location:attendance_list.php?msg='.$msg.'&date='.$date.'&charactor='.$charactor.'&token='.$token.'&uid='.$uid.'&ul='.$ul.'&parLang='.$parLang.'');
?>