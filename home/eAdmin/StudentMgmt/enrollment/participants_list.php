<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
//include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include_once($PATH_WRT_ROOT."includes/libaccount.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Check access right
if (!$plugin['eEnrollment'])
{
	echo "You have no priviledge to access this page.";
	exit;
}
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
$libenroll = new libclubsenrol();	
$LibPortfolio = new libpf_slp();
if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) or !($libenroll->hasAccessRight($_SESSION['UserID'])))
{
	echo "You have no priviledge to access this page.";
	exit;
}

$db = new libdb();
$linterface = new interface_html("popup.html");

# Club / Activity Info
$data = $LibPortfolio->RETURN_OLE_PROGRAM_BY_PROGRAMID($ProgramID);
list($startdate, $enddate, $title, $category, $ele, $organization, $details, $remark, $input_date, $modified_date, $period, $submit_type) = $data;

if($startdate=="" || $startdate=="0000-00-00")
	$date_display = "--";
else if($enddate!="" && $enddate != "0000-00-00")
	$date_display = $startdate."&nbsp;".$profiles_to."&nbsp;".$enddate;
else
	$date_display = $startdate;
	
$Cats = $LibPortfolio->GET_OLR_Category();	

# get ELE Array
if($ele!="")
{
	# get the ELE code array
	$DefaultELEArray = $LibPortfolio->GET_ELE();
	$ELEArr = explode(",", $ele);
	for($i=0; $i<sizeof($ELEArr); $i++)
	{
		$t_ele = trim($ELEArr[$i]);
		$ele_display .= "- ".$DefaultELEArray[$t_ele]."<br />";
	}
}
else
	$ele_display = "--";	

# Student List
$tempStudnetTable = $RecordType=="club" ? "INTRANET_USERGROUP" : "INTRANET_ENROL_EVENTSTUDENT";
$sql = "select 
		IF(c.ClassNumber IS NULL OR c.ClassNumber = '', CONCAT(c.ClassName,' (-)'), CONCAT(c.ClassName,' (',c.ClassNumber,')')) as StuClass,
	    c.EnglishName, 
	    c.ChineseName,
	    a.Role,
		a.Hours,
		a.TeacherComment
		from 
		{$eclass_db}.OLE_STUDENT as a
		left join {$tempStudnetTable} as b on (a.RecordID = b.OLE_STUDENT_RecordID)
		left join INTRANET_USER as c on (c.UserID = a.UserID)
		where 
		a.ProgramID='$ProgramID' 
		";
//$stuResult = $libenroll->returnArray($sql);

# default sorting : ClassName, ClassNumber ASC
if (!isset($field))
{
    $field = 0;
}
if ($order=="" || !isset($order))
{
	$order = ($order == 0) ? 1 : 0;
}

# Table settings
$li = new libdbtable2007($field, $order);
$li->field_array = array("StuClass", "c.EnglishName","c.ChineseName");
$li->sql = $sql;
$li->no_col = 7;
$li->IsColOff = "displayEnrolmentTransferOLE";

$li->column_list .= "<td width=1 class='tablebluetop tabletopnolink'><span class='tabletoplink'>#</span></td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>". $i_ClassNameNumber ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>". $i_UserEnglishName ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>". $i_UserChineseName ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>". $ec_iPortfolio['ole_role'] ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$ec_iPortfolio['hours'] ."</td>\n";
$li->column_list .= "<td class='tablebluetop tabletopnolink'>".$ec_iPortfolio['comment'] ."</td>\n";

$MODULE_OBJ['title'] = $eEnrollment['app_stu_list']; 
$linterface->LAYOUT_START();

?>

<br />
	<table align="center" width="96%" border="0" cellpadding="4" cellspacing="0" style="border:dashed 1px #666666">
		<tr>
			<td width="80%" valign="top">
			<table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['title']; ?></span></td>
				<td width="80%" valign="top"><?=$title;?></td>
			</tr>
			
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['year']?></span></td>
				<td><?=$period;?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['date']?> / <?=$ec_iPortfolio['period']?></span></td>
				<td><span class="tabletext"><?=$date_display;?></span></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['category']; ?></span></td>
				<td><?=$Cats[$category]?></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?php echo $ec_iPortfolio['ele']; ?></span></td>
				<td><?=$ele_display?></td>
			</tr>
			<tr>
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['organization']?></span></td>
				<td><?=($organization==""?"--":nl2br($organization))?></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['details']?></span></td>
				<td><?=($details==""?"--":nl2br($details))?></td>
			</tr>
			<tr valign="top">
				<td valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$ec_iPortfolio['school_remark']?></span></td>
				<td><?=($remark==""?"--":nl2br($remark))?></td>
			</tr>
			</table>
			</td>
		</tr>
		</table>
		
<br />	

<table align="center" width="96%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td><?=$li->display("blue", 0);?></td>
</tr>
</table>	

<br />	

<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();")?>
</div>
</td></tr>
</table>

<?		
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

