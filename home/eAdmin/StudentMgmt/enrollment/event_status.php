<?php
## Using By : 
###########################################
##	Modification Log:
##	2017-09-27 Anna
##	- Added $sys_custom['eEnrolment']['ReplySlip'] for reply slip
##
##	2015-07-10	Evan
##	- Update search box style
##
##	2015-06-30	Evan
##  - Fix the bug of searching special symbols and characters
##
##	2015-05-26	Omas
##	- added export button
##
##	2011-09-07	YatWoon
##	- lite version implementation
##
##	2011-06-27	YatWoon
##	Improved: fine tune the Chinese wording of remark
##
##	2010-01-28 Max (201001281139)
##	Add search bar to search club name
###########################################
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();


$libenroll = new libclubsenrol($AcademicYearID);

# Acadermic Year Selection        	
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.action=\'event_status.php\';document.form1.submit();"', 1, 0, $AcademicYearID);
		
//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_EVENT_PIC()))
if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_EVENT_PIC()) && (!$libenroll->IS_CLUB_PIC()))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_EVENT_PIC()))
	//	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	
	// Many Queries *********
//	$libenroll->UPDATE_EVENTS_APPROVED();
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        
        # tags 
        $tab_type = "activity";
        $current_tab = 2;
        include_once("management_tabs.php");
        
        # Search box
		$searchTag = "<input name=\"keyword\" id=\"keyword\" type=\"text\" value=\"" . intranet_htmlspecialchars(stripslashes($keyword)) . "\" onkeyup=\"Check_Go_Search(event);\"/>";
		
		//do not search for the initial textbox text
		if ($keyword == $Lang['eEnrolment']['EnterActivityName']) $keyword = "";
		$name_conds = " AND ieei.EventTitle LIKE '%".$libenroll->Get_Safe_Sql_Like_Query(stripslashes($keyword))."%' ";        
                
        # navigation
    	$PAGE_NAVIGATION[] = array($eEnrollment['participant_selection_and_drawing'], "");
        
        # activity enrolment info
        $defaultMin = $libenroll->Event_defaultMin;
		$defaultMax = $libenroll->Event_defaultMax;
// 		if ($defaultMax==0) $defaultMax = $eEnrollment['no_limit'];
		$defaultMax = $defaultMax==0 ? $eEnrollment['no_limit'] : $defaultMax . $eEnrollment['student_enrolment_rule_activity2'] ;
		$EnrollMax = $libenroll->Event_EnrollMax;
// 		if ($EnrollMax==0) $EnrollMax = $eEnrollment['no_limit'];
		$EnrollMax = $EnrollMax==0 ? $eEnrollment['no_limit'] : $EnrollMax . $eEnrollment['student_enrolment_rule_activity2'] ;
		$AppStart = $libenroll->Event_AppStart;
		$AppEnd = $libenroll->Event_AppEnd;		
		$AppStartDisplay = $libenroll->Event_AppStart." ".$libenroll->Event_AppStartHour.":".$libenroll->Event_AppStartMin;
		$AppEndDisplay = $libenroll->Event_AppEnd." ".$libenroll->Event_AppEndHour.":".$libenroll->Event_AppEndMin;		
		
		$StudentEnrolmentRule = str_replace("<!--MinNumApp-->", $defaultMin, $eEnrollment['student_enrolment_rule_activity']);
		$StudentEnrolmentRule = str_replace("<!--MaxNumApp-->", $defaultMax, $StudentEnrolmentRule);
		$StudentEnrolmentRule = str_replace("<!--MaxEnrol-->", $EnrollMax, $StudentEnrolmentRule);
		$EnrolmentPeriodDisplay = $eEnrollment['enrolment_period_activity']." ".$AppStartDisplay." ".$eEnrollment['to']." ".$AppEndDisplay;
		
        $EventArr = $libenroll->GET_ADMIN_EVENTINFO_LIST(0, $name_conds, $AcademicYearID); ############################################
        
        //get waiting,approved,rejected number of applicants
		$waitingNum = array();
		$approvedNum = array();
		$rejectedNum = array();
		$ActivityStudentEnrolArr = $libenroll->Get_Activity_Student_Enrollment_Info('', $keyword, $AcademicYearID);
		
		$numOfActivity = count($EventArr);
		for ($i=0; $i<$numOfActivity; $i++)
		{
			$thisEnrolEventID = $EventArr[$i]['EnrolEventID'];
			
			$thisWaitingNum = count((array)$ActivityStudentEnrolArr[$thisEnrolEventID]['StatusStudentArr'][0]);
			$thisRejectedNum = count((array)$ActivityStudentEnrolArr[$thisEnrolEventID]['StatusStudentArr'][1]);
			$thisApprovedNum = count((array)$ActivityStudentEnrolArr[$thisEnrolEventID]['StatusStudentArr'][2]);
			
			$waitingNum[$thisEnrolEventID] = ($thisWaitingNum=='')? 0 : $thisWaitingNum;
			$rejectedNum[$thisEnrolEventID] = ($thisRejectedNum=='')? 0 : $thisRejectedNum;
			$approvedNum[$thisEnrolEventID] = ($thisApprovedNum=='')? 0 : $thisApprovedNum;
			
			/*
			$sql = "SELECT COUNT(*) FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '".$EventArr[$i]['EnrolEventID']."' AND RecordStatus = 0 ";
			$result = $libenroll->returnVector($sql);
			$waitingNum[] = $result[0];
			$sql = "SELECT COUNT(*) FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '".$EventArr[$i]['EnrolEventID']."' AND RecordStatus = 2 ";
			$result = $libenroll->returnVector($sql);
			$approvedNum[] = $result[0];
			$sql = "SELECT COUNT(*) FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '".$EventArr[$i]['EnrolEventID']."' AND RecordStatus = 1 ";
			$result = $libenroll->returnVector($sql);
			$rejectedNum[] = $result[0];
			*/
		}
		
		### Print and Email Icons
		$toolbar = '';
		if (!$libenroll->IS_NORMAL_USER($_SESSION['UserID']) || $libenroll->IS_CLUB_PIC() || $libenroll->IS_EVENT_PIC())
		{
			$toolbar .= $linterface->GET_LNK_PRINT("javascript:newWindow('print.php?type=activity&AcademicYearID=$AcademicYearID',4);", $button_print, '', '', '', 0);
			$toolbar .= toolBarSpacer();
		}
		
		# temp disable for deployment (Ivan 20091110)
		# enable for development (Max 20100111)
		if($plugin['eEnrollmentLite'])
		{
			$toolbar .= "<div class='Conntent_tool'><i class='email'>".$Lang['eEnrolment']['SendEmailForEnrollmentResult']."</i></div>";
		}
		else
		{
			if ($libenroll->AllowToEditPreviousYearData && !$libenroll->IS_NORMAL_USER($_SESSION['UserID']) && !$plugin['eEnrollmentLite'])
				$toolbar .= $linterface->GET_LNK_EMAIL("event_email.php", $Lang['eEnrolment']['SendEmailForEnrollmentResult'], '', '', '', 0);
		}
        $toolbar .= $linterface->GET_LNK_EXPORT("export.php?type=10&field=0&order=1&filter=0&keyword=&priority=1&AcademicYearID=".$AcademicYearID."", $Lang['Btn']['Export'], '', '', '', 0);
        
        $linterface->LAYOUT_START();

        if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("", $i_Payment_con_PaymentProcessed)."<br/>";
        
        if($sys_custom['eEnrolment']['ReplySlip']){
        	echo $linterface->Include_Thickbox_JS_CSS();
        }
?>
<script type="text/javascript">

<!--
function PaymentConfirm(obj, jParID) {
	AlertPost(obj, 'payment.php?EnrolEventID='+jParID, '<?= $eEnrollment['js_prepayment_alert']?>');
}

function checkLottery(enrolEventID){
	
	if(confirm("<?=$i_ClubsEnrollment_Confirm_GoLottery?>"))
	{
		var url = "event_lottery.php?EnrolEventID=" + enrolEventID;
		newWindow(url, 9);
	}
	else
	{
		return;	
	}
}

//-->

<?php if($sys_custom['eEnrolment']['ReplySlip']):?>
	function onLoadThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
		load_dyn_size_thickbox_ip('<?=$eEnrollment['replySlip']['ReplySlipDetail']?>', 'onLoadReplySlipThickBox('+ReplySlipGroupMappingID+','+EnrolGroupID+','+ReplySlipID+');');
	}
	function onLoadReplySlipThickBox(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
		$.ajax({
			url: "ReplySlip_ajax_ReplySlipReply_view2.php",
			type: "POST",
			data: {
				"ReplySlipGroupMappingID": ReplySlipGroupMappingID, 
				"EnrolGroupID": EnrolGroupID,
				"ReplySlipID":ReplySlipID,
				"Status" : $("#filter").val(),
				"isActivity":true
				},
			success: function(ReturnData){
				$('div#TB_ajaxContent').html(ReturnData);
			}
		});
	}
	function goExport(ReplySlipGroupMappingID,EnrolGroupID,ReplySlipID){
	    	document.form1.action = "ReplySlip_ajax_ReplySlipReply_view2.php?ReplySlipGroupMappingID="+ReplySlipGroupMappingID+"&EnrolGroupID="+EnrolGroupID+"&ReplySlipID="+ReplySlipID+"&isExport="+1+"&isActivity=true";
	    	document.form1.target = "_blank";
	    	document.form1.method = "post";
	    	document.form1.submit();
	}
	<?php endif?>
</script>

<form name="form1" action="" method="POST">
<br/>
<!-- navigation and search bar -->
<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr>
		<td>
			<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
		</td>
		<td align="right">
			<div class="Conntent_search"><?=$searchTag?></div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<?= $yearFilter ?>
		</td>
	</tr>
</table>
<table width="96%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right"><?= $SysMsg ?></td>
</tr>
<tr><td>
<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
	<tr>
		<td align="center" class="tabletext">
			<?
				print $StudentEnrolmentRule;
			?>
		</td>
	</tr>
</table>
<?if($libenroll->AllowToEditPreviousYearData){?>
<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
	<tr>
		<td nowrap align="center" class="tabletext">
			<?= $EnrolmentPeriodDisplay ?>
		</td>
	</tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
	<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
	<tr><td align="center" colspan="6"></td></tr>	
</table>
<?}?>

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5" align="center">
	<tr>
		<td colspan="6">
			<?= $toolbar ?>
		</td>
	</tr>
	<tr class="tableorangetop">
		<td rowspan="2" class="tableTitle" nowrap><span class="tabletopnolink"><?= $eEnrollment['act_name']?></span></td>
		<td rowspan="2" class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['club_quota']?></span></td>
		<td colspan="3" class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['number_of_applicants']?></span></td>
		<?php if($sys_custom['eEnrolment']['ReplySlip']){?>
			<td rowspan="2" class="tableTitle" align="center"><span class="tabletopnolink"><?=$Lang['eEnrolment']['ReplySlip']?></span></td>
		<?php }?>
		<td rowspan="2" class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['random_drawing']?></span></td>
		<td rowspan="2" colspan="2" class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['quota_left']?></span></td>
		<!---<td colspan="2" class="tableTitle" align="center"><span class="tabletopnolink"><//?=$eEnrollment['payment']?></span></td>--->
	</tr>
	<tr class="tableorangetop">						
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?= $eEnrollment['unhandled']?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?= $i_ClubsEnrollment_StatusApproved?></span></td>
		<td class="tableTitle" align="center"><span class="tabletopnolink"><?= $i_ClubsEnrollment_StatusRejected?></span></td>
		
		<!---<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['pay']['amount']?></span></td>--->
		<!---<td class="tableTitle" align="center"><span class="tabletopnolink"><?=$eEnrollment['process']?></span></td>--->
	</tr>

<?
	for ($i = 0; $i < sizeof($EventArr); $i++) {
		$thisEnrolEventID = $EventArr[$i]['EnrolEventID'];
		$thisEnrolGroupID = $EventArr[$i]['EnrolGroupID'];
		
		### If the activity does not belong to any club
		### Hide the activity if the user is a normal user and the user is not the activity's PIC
		if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']) && !$libenroll->IS_EVENT_PIC($thisEnrolEventID) && ($thisEnrolGroupID==''||$thisEnrolGroupID==0))
		{
			continue;
		}
		
		### If the activity is belong to a club
		### Hide the activity if the user is not the activity's PIC and the activity's club's PIC and the user is a normal user
		if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']) && $thisEnrolGroupID!='' && $thisEnrolGroupID!=0 && !$libenroll->IS_CLUB_PIC($thisEnrolGroupID) && !$libenroll->IS_EVENT_PIC($thisEnrolEventID))
		{
			continue;
		}
			
		$quota = $EventArr[$i][1];
		if ($quota == 0) {
			$quota = $i_ClubsEnrollment_NoLimit;
			$quota_left = $i_ClubsEnrollment_NoLimit;
		} else {
			$quota_left = $quota - $EventArr[$i][2];
		}
		// show quota = 0 even though it is negative
		if ($quota_left < 0)
		{
			$quota_left = 0;
		}
		
		// Amount
		if ($EventArr[$i]['isAmountTBC'] == 1)
			$thisAmountDisplay = $eEnrollment['ToBeConfirmed'];
		else
			$thisAmountDisplay = "$".$EventArr[$i]['PaymentAmount'];
			
		//if (!($quota_left > 0)) $quota_left = 0;
		if ($libenroll->CAN_EVENT_PREFORM_LOTTERY($EventArr[$i]['EnrolEventID'])) {
			$buttonLottery = "";			
			//$buttons .= "<a href=\"event_member.php?EventEnrolID=".$EventArr[$i]['EnrolEventID']."&RecordStatus=0\"><img src=\"$image_path/$LAYOUT_SKIN/eEnrollment/icon_applicants.gif\" border=\"0\" alt=\"".$eEnrollment['manage_app_stu_list']."\"></a> ";			
			//$buttonLottery .= $linterface->GET_ACTION_BTN($i_ClubsEnrollment_GoLottery, "button", "javascript:newWindow('event_lottery.php?EnrolEventID=".$EventArr[$i]['EnrolEventID']."', 9)");
			//$buttonLottery .= $linterface->GET_ACTION_BTN($i_ClubsEnrollment_GoLottery, "button", "javascript:checkLottery('".$EventArr[$i]['EnrolEventID']."')");
			$buttonLottery .= "<a href=\"javascript:checkLottery('".$EventArr[$i]['EnrolEventID']."')\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eEnrollment/icon_lottery.gif\" border=\"0\" alt=\"".$i_ClubsEnrollment_GoLottery."\"></a> ";	
			$rowspan = "rowspan=\"2\"";
			//$buttons .= "<a href=\"event_member_index.php?EnrolEventID=".$EventArr[$i]['EnrolEventID']."\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eEnrollment/icon_approved_student.gif\" border=\"0\" alt=\"".$eEnrollment['manage_groupmates_list']."\"></a> ";
			$buttonsp = "---";
		} else {
			$buttonLottery = "---";
			//$buttons .= "<a href=\"event_member.php?EventEnrolID=".$EventArr[$i]['EnrolEventID']."&RecordStatus=2\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eEnrollment/icon_applicants.gif\" border=\"0\" alt=\"".$eEnrollment['manage_app_stu_list']."\"></a> ";	
			//$buttons .= "<a href=\"event_member_index.php?EnrolEventID=".$EventArr[$i]['EnrolEventID']."\"><img src=\"{$image_path}/{$LAYOUT_SKIN}/eEnrollment/icon_approved_student.gif\" border=\"0\" alt=\"".$eEnrollment['manage_groupmates_list']."\"></a> ";

			if (!$EventArr[$i][19]) {	//AddToPayment
				# show payment icon if the event is not free of charge
				if ($plugin['payment'] && $EventArr[$i]['PaymentAmount']!=0 && $EventArr[$i]['isAmountTBC']!=1)
				{
					if(!$plugin['eEnrollmentLite'])
					{
						if($libenroll->AllowToEditPreviousYearData) {
							$buttonsp = "
									<a href=\"javascript:PaymentConfirm(document.form1, ".$EventArr[$i]['EnrolEventID'].")\" >
									<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_payment.gif\" border=\"0\" alt=\"".$eEnrollment['payment']."\">
									</a>
									";
						} else {
							$buttonsp = "
									<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_payment.gif\" border=\"0\" alt=\"".$eEnrollment['payment']."\">
									";						
						}
					}
					else
					{
						$buttonsp = "
									<img src=\"{$image_path}/{$LAYOUT_SKIN}/icon_payment_dim.gif\" border=\"0\" alt=\"".$eEnrollment['payment']."\">
									";	
					}
				}
				else
				{
					$buttonsp = "---";
				}
			} else {
				$buttonsp = $eEnrollment['processed'];
			}
		}
		
		/*
		if (($libenroll->IS_NORMAL_USER($UserID) && ($libenroll->IS_ENROL_PIC($UserID, $EventArr[$i]['EnrolEventID'], "Activity") || $libenroll->IS_CLUB_PIC($EventArr[$i]['GroupID'])))
			|| $libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])
			|| $libenroll->IS_ENROL_MASTER($_SESSION['UserID'])
			) {
		*/
?>	
	
	<tr class="tableorangerow<?= ($displayed_record % 2) + 1?>">
		<!- Activity Name -->
		<td valign="top" nowrap><span class="tabletext"><?= $EventArr[$i][3]?></span></td>
		
		<!- Quota -->
		<td valign="top" align="center"><span class="tabletext"><?= $quota?></span></td>
		
		<!- Waiting -->
		<td valign="top" align="center">
			<a class="tablelink" href="event_member.php?AcademicYearID=<?=$AcademicYearID?>&EnrolEventID=<?=$thisEnrolEventID?>&filter=0">
				<?= $waitingNum[$thisEnrolEventID] ?>
			</a>
		</td>
		
		<!- Approved -->
		<td valign="top" align="center">
			<a class="tablelink" href="event_member.php?AcademicYearID=<?=$AcademicYearID?>&EnrolEventID=<?=$thisEnrolEventID?>&filter=2">
				<?= $approvedNum[$thisEnrolEventID] ?>
			</a>
		</td>
		
		<!- Rejected -->
		<td valign="top" align="center">
			<a class="tablelink" href="event_member.php?AcademicYearID=<?=$AcademicYearID?>&EnrolEventID=<?=$thisEnrolEventID?>&filter=1">
				<?= $rejectedNum[$thisEnrolEventID] ?>
			</a>
		</td>
		
		
		<?php 
		
		if($sys_custom['eEnrolment']['ReplySlip']){
			$ReplySlipRelation = $libenroll->getReplySlipEnrolGroupRelation('',$thisEnrolEventID,2);
			$ReplySlipRelation = $ReplySlipRelation[0];
			$ReplySlipGroupMappingID = $ReplySlipRelation['ReplySlipGroupMappingID'];
			$ReplySlipID = $ReplySlipRelation['ReplySlipID'];
			
			$link = '';
			$ReplySlipReply = '';
			
			if($ReplySlipGroupMappingID){
				$ReplySlipReply = $libenroll->getReplySlipReply('',$ReplySlipGroupMappingID);
				$link = '<div class="Conntent_tool" align="center"><a href="javascript:void(0);" onclick="onLoadThickBox('.$ReplySlipGroupMappingID.','.$thisEnrolEventID.','.$ReplySlipID.')" style="background:url(\'/images/2009a/eOffice/icon_resultview.gif\') no-repeat;">'.$eEnrollment['replySlip']['replySlip'].'</a></div>';
			}
		?>

			<!- ReplySlip -->
			<td valign="top" align="center" width="90">
				<?= $link?>
			</td>	
		<?php 
			}
		?>
		
			
		
		
		<!- Lottery Button -->
		<td valign="top" align="center"><span class="tabletext"><?= $buttonLottery?></span></td>
		
		<!- Quota Left -->
		<td valign="top" align="center"><span class="tabletext"><?= $quota_left?></span></td>
		
		<!- Amount -->
		<!--<td valign="top" align="center"><span class="tabletext"><//?= $thisAmountDisplay?></span></td>-->
		
		<!- Process -->
		<td valign="top" align="center">
			<!--<//?= $buttonsp?>-->
		</td>
	</tr>
		
<? 
			$displayed_record++;
		//}
	}
	
	if ($displayed_record == 0) {
		print "<tr><td class=\"tabletext\" align=\"center\" colspan=\"10\">".$eEnrollment['no_record']."</td></tr>";
	}
?>

</table>

</td></tr>
<tr><td>

<table width="100%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
</td></tr>
</table>
<br/>
</td></tr>
</table>

</form>


    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>