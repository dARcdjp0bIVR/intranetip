<?php
# using: yat

##################################
# 
#
###################################

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $Lang['eEnrolment']['Reminder'];
$linterface = new interface_html("popup.html");
$libenroll = new libclubsenrol();

$linterface->LAYOUT_START();

?>

<table width="100%">
<tr>
	<td><?=$libenroll->popup_content?></td>
</tr>
</table>


<div class="edit_bottom">
	<input name="submit2" type="button" class="formbutton" value="<?=$Lang['Btn']['Close']?>" onClick="window.close();"/>
	<p class="spacer"></p>
</div>	

<?php

$linterface->LAYOUT_STOP();
intranet_closedb();
?>