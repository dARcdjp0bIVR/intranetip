<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$libenroll->DEL_EVENTSTUDENT($EnrolEventID);

$EventArr['EnrolEventID'] = $EnrolEventID;
$EventArr['Description'] = $Discription;
if ($set_age) $EventArr['UpperAge'] = $UpperAge;
if ($set_age) $EventArr['LowerAge'] = $LowerAge;
$EventArr['Gender'] = $gender;
$EventArr['attach'] = $_FILES['attach'];
$EventArr['Quota'] = $Quota;
$EventArr['EventTitle'] = $act_title;
$EventArr['EventCategory'] = $sel_category;
$EventArr['ApplyStartTime'] = $ApplyStartTime;
$EventArr['ApplyEndTime'] = $ApplyEndTime;
$EventArr['ApplyMethod'] = $tiebreak;
$EventArr['EnrolGroupID'] = $EnrolGroupID;
$EventArr['PaymentAmount'] = $PaymentAmount;

if ($libenroll->IS_EVENTINFO_EXISTS($EnrolEventID)) {
	$EnrolEventID = $libenroll->EDIT_EVENTINFO($EventArr);
} else {
	$EnrolEventID = $libenroll->ADD_EVENTINFO($EventArr);
}

$libenroll->DEL_EVENTSTAFF($EnrolEventID);
$libenroll->DEL_EVENTCLASSLEVEL($EnrolEventID);

$PICArr['EnrolEventID'] = $EnrolEventID;
$PICArr['StaffType'] = "PIC";
for ($i = 0; $i < sizeof($pic); $i++) {
	$PICArr['UserID'] = $pic[$i];
	$libenroll->ADD_EVENTSTAFF($PICArr);
}

$PICArr['StaffType'] = "HELPER";
for ($i = 0; $i < sizeof($helper); $i++) {
	$PICArr['UserID'] = $helper[$i];
	$libenroll->ADD_EVENTSTAFF($PICArr);
}

$ClasslvlArr['EnrolEventID'] = $EnrolEventID;
for ($i = 0; $i < sizeof($classlvl); $i++) {
	$ClasslvlArr['ClassLevelID'] = $classlvl[$i];
	$libenroll->ADD_EVENTCLASSLEVEL($ClasslvlArr);
}

intranet_closedb();
header("Location: event_new2.php?EnrolEventID=$EnrolEventID");
?>