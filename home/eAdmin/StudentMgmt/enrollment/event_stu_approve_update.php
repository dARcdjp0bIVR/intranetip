<?php
## Using By: 
######################################
##	Modification Log:
##
##	Date:	2014-12-02 Omas
##			New setting $libenroll->notCheckTimeCrash, $libenroll->Event_notCheckTimeCrash bypass time crash checking
##
##	2012-12-24: Rita 
##	Add approve reason flag checking (enableActivityFillInEnrolReasonRight) for customization [#2012-0803-1543-49054]
##
##	2010-02-01: Max (201001271658)
##	- adjust the send email condition
##
## 2010-01-08 Max (201001071635)
## - Case the email send by "EventOnceEmail"
########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolEventID = IntegerSafe($EnrolEventID);

$notCheckTimeCrashSetting = $libenroll->Event_notCheckTimeCrash;

if ($plugin['eEnrollment'])
{
	$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID);
	
	$StuArr['EventEnrolID'] = $EnrolEventID;
	
	if($libenroll->Event_UseCategorySetting)
	{
		$EventInfo = $libenroll->GET_EVENTINFO($EnrolEventID);
		$EnrolSetting = $libenroll->Get_Category_Enrol_Setting($EventInfo['EventCategory']);
		$EnrolMax = $EnrolSetting[$EventInfo['EventCategory']]['Activity']['EnrollMax'];
	}
	else
	{
		$EnrolMax = $libenroll->Event_EnrollMax;
	}
	
	# all students who are time-crashed
	$crashedStudentArr = unserialize(rawurldecode($crashedStudentArr));
	
	
	# set the student list array
	if ($from == "addMemberResult")	
	{
		$UserArr = $crashedStudentArr;
	}
	else
	{
		$UserArr = $uid;
	}
	
	# initializing arrays
	$result_ary = array();
	$imported_student = array();
	$crash_EnrolGroupID_ary = array();
	$crash_EnrolEventID_ary = array();
	
	$QuotaLeft = $libenroll->GET_EVENT_QUOTA_LEFT($EnrolEventID);
	
	for ($i = 0; $i < sizeof($UserArr); $i++) {
		$thisStudentID = $UserArr[$i];
		
		// do checking if not come back from add_member_result
		if ($from != "addMemberResult")	
		{
			//check default school quota
			if ($libenroll->WITHIN_ENROLMENT_STAGE("activity") && ($libenroll->GET_STUDENT_NUMBER_OF_ENROLLED_EVENT($UserArr[$i]) >= $EnrolMax && $EnrolMax != 0))
			{
				//exceeded school default quota of enrolment
				$result_ary[$i] = "default_school_quota_exceeded";
				continue;	
			}
			
			// check if club quota exceeded
			if ($i >= $QuotaLeft)
			{
				//no quota left
				$result_ary[$i] = "act_quota_exceeded";
				continue;	
			}
			
			//check quota of student
			$studentQuotaLeft = $libenroll->GET_STUDENT_ACTIVITY_QUOTA_LEFT($thisStudentID, $EventInfo['EventCategory']);

			if ($libenroll->disableCheckingNoOfActivityStuWantToJoin == false && $studentQuotaLeft <= 0)
			{
				//no quota left
				$result_ary[$i] = "student_quota_exceeded_activity";
				continue;
			}
			
			//check if student is approved already
			$sql = "SELECT RecordStatus FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '".$UserArr[$i]."'";
			$result = $libenroll->returnArray($sql,1);
			if ($result[0][0]==2)
			{
				//ignore approved student
				$result_ary[$i] = "student_is_member";
				continue;	
			}
			
			if ($notCheckTimeCrashSetting != 1){
				$crashActivity = false;	
				// get enroll list, check for this $EnrolEventID and enrolled
				$StudentEnrolledList = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($UserArr[$i]);
				for ($j = 0; $j < sizeof($StudentEnrolledList); $j++) {
					if ($libenroll->IS_EVENT_CRASH($EnrolEventID, $StudentEnrolledList[$j][0], $StudentEnrolledList)) {
						$crashActivity = true; 
						$crash_EnrolEventID_ary[$i][] = $StudentEnrolledList[$j][0];
					}
				}
				if ($crashActivity)
				{
					$result_ary[$i] = "crash_activity";
				}
				
				$crashClub = false;	
				// get student's enrolled group list, check for any time crash
				$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($UserArr[$i]);
				for ($j = 0; $j < sizeof($StudentClub); $j++) {
					if ($libenroll->IS_EVENT_GROUP_CRASH($StudentClub[$j][0], $EnrolEventID)) {
						$crashClub = true;
						$crash_EnrolGroupID_ary[$i][] = $StudentClub[$j][0];
					}
				}
				if ($crashClub)
				{
					$result_ary[$i] = "crash_group";
				}
				
				if ($crashActivity || $crashClub)
				{
					continue;
				}
			}
			
			# approve student to the activity
			$StuArr['StudentID'] = $UserArr[$i];
			$libenroll->APPROVE_EVENT_STU($StuArr);
			array_push($imported_student, $UserArr[$i]);
			$email_student[] = $UserArr[$i];
			
		}
		else
		{
			// $uid is the approved students array
			if (in_array($UserArr[$i],$uid))
			{
				# approve student to the activity
				$StuArr['StudentID'] = $UserArr[$i];
				$libenroll->APPROVE_EVENT_STU($StuArr);
				array_push($imported_student, $UserArr[$i]);
				$email_student[] = $UserArr[$i];
			}
			else
			{
				$result_ary[$i] = "rejected";
			}
		}
	}
	$libenroll->UPDATE_EVENTS_APPROVED();
	
	if($libenroll->enableActivityFillInEnrolReasonRight()==false){
		// send email
		$RecordType = "activity";
		$successResult = true;
		$libenroll->Send_Enrolment_Result_Email($email_student, $EnrolEventID, $RecordType, $successResult);
	}
	
	intranet_closedb();
	?>
	
	<body>
	<form name="form1" method="post" action="add_member_result.php">
		<input type="hidden" name="from" value="EventEnrolList">
		<input type="hidden" name="type" value="-1">
		<input type="hidden" name="GroupID" value="<?=$GroupID?>">
		<input type="hidden" name="EnrolEventID" value="<?=$EnrolEventID?>">
		<input type="hidden" name="data" value="<?=rawurlencode(serialize($UserArr));?>">
		<input type="hidden" name="result_ary" value="<?=rawurlencode(serialize($result_ary));?>">
		<input type="hidden" name="imported_student" value="<?=rawurlencode(serialize($imported_student));?>">
		<input type="hidden" name="enrolStatus" value="2">
		<input type="hidden" name="crash_EnrolGroupID_ary" value="<?=rawurlencode(serialize($crash_EnrolGroupID_ary));?>">
		<input type="hidden" name="crash_EnrolEventID_ary" value="<?=rawurlencode(serialize($crash_EnrolEventID_ary));?>">
		<input type="hidden" name="filter" value="<?=$filter?>">
	</form>

	<script language="Javascript">
		document.form1.submit();
	</script>
	</body>
<?
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>