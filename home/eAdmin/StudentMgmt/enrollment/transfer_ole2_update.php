<?php
//update by: 
if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libeclass.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/slp/ole_program.php");
include_once($PATH_WRT_ROOT."includes/portfolio25/iPortfolioConfig.inc.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$linterface = new interface_html();
$libenroll = new libclubsenrol();	

# Check access right
if (!$plugin['eEnrollment'])
{
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();	
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))){
	header("Location: ".$PATH_WRT_ROOT."home/eService/enrollment/");
	exit;
}

$FormChkArr = unserialize(rawurldecode($_REQUEST['FormChkArr']));

//$LibPortfolio = new libportfolio2007();
$LibPortfolio = new libpf_slp();
$ID_Str = implode(",",$ID);

if($RecordType=="club")
{
	$TitleField = Get_Lang_Selection('b.TitleChinese', 'b.Title');
	
	$sql = "SELECT 
				a.EnrolGroupID,
				$TitleField as Title,
				left(min(c.ActivityDateStart),10) as Startdate,
				left(max(c.ActivityDateEnd),10) as Enddate,
				a.OLE_ProgramID,
				c.EnrolGroupID
			FROM 
				INTRANET_ENROL_GROUPINFO as a
				LEFT OUTER JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
				LEFT OUTER JOIN INTRANET_ENROL_GROUP_DATE as c ON c.EnrolGroupID  = a.EnrolGroupID
			WHERE
				(c.RecordStatus IS NULL OR c.RecordStatus = 1)
				AND
				a.EnrolGroupID in ($ID_Str)
			GROUP BY
				a.EnrolGroupID
			ORDER BY
				$TitleField
			";
	
}
else
{
	$sql ="
		SELECT
			a.EnrolEventID,
			a.EventTitle,
			left(min(e.ActivityDateStart),10) as Startdate,
			left(max(e.ActivityDateEnd),10) as Enddate,
			a.OLE_ProgramID,
			e.EnrolEventID
		FROM
			INTRANET_ENROL_EVENTINFO as a
			LEFT JOIN INTRANET_ENROL_EVENTCLASSLEVEL as b ON b.EnrolEventID = a.EnrolEventID
			LEFT JOIN INTRANET_CLASS as c ON b.ClassLevelID = c.ClassLevelID
			LEFT JOIN INTRANET_USER as d ON d.ClassName = c.ClassName
			LEFT JOIN INTRANET_ENROL_EVENT_DATE as e on e.EnrolEventID = a.EnrolEventID
		WHERE
			(e.RecordStatus IS NULL OR e.RecordStatus = 1)
			AND
			a.EnrolEventID in ($ID_Str)
		GROUP BY
			a.EnrolEventID
		ORDER BY
			a.EventTitle
	";
}
$groupInfoArr = $libenroll->returnArray($sql);
# loop each group
$numTransferredArr = array();
$ProgramType = $ipf_cfg["OLE_PROGRAM_PROGRAMTYPE"]["createdByTeacher"];
for($i=0;$i<sizeof($groupInfoArr);$i++)
{
	list($tempGroupID, $tempTitle, $tempStartdate, $tempEnddate, $tempOLE_ProgramID, $EnrolGroupID) = $groupInfoArr[$i];
	
	# data

	$title = addslashes($tempTitle);
	$category = ${"category_".$tempGroupID};
	$ELEList = ${"ele_".$tempGroupID};
	$ELEListStr = (sizeof($ELEList)!=0) ? implode(",", $ELEList) : "";
	$organization = HTMLtoDB(stripslashes(${"organization_".$tempGroupID}));
	$details = HTMLtoDB(stripslashes(${"details_".$tempGroupID}));
	$SchoolRemarks = HTMLtoDB(stripslashes(${"SchoolRemarks_".$tempGroupID}));
	$SubmitType = 'INT';		# OLE
	$startdate = $tempStartdate;
	$enddate = $tempEnddate;
	$currentSchYear = true;		# if eEnrollment record is for current school year
	
	# settings
	$duplicate_ole = ${"ProgramReplace_".$tempGroupID};
	$duplicate_pp = ${"StudentReplace_".$tempGroupID};
	$hours_setting = ${"hours_setting_".$tempGroupID};
	$zeroHour = ${"zeroHour_".$tempGroupID};
	$studentSetting = ${"studentSetting_".$tempGroupID};
	$leastAttendance = ${"leastAttendance_".$tempGroupID};
	$manualHours = ${"manualHours_".$tempGroupID};
	
	# settings for calculate student OLE hours
	$ParData['enrolmentID'] = $tempGroupID;
	$ParData['studentSetting'] = $studentSetting; 	
	$ParData['zeroHour'] = $zeroHour;			
	$ParData['hourSetting'] = $hours_setting;		
	$ParData['leastAttendance'] = $leastAttendance;	
	$ParData['manualHours'] = $manualHours;	
	$OLE_HoursArr = $libenroll->GET_STUDENT_OLE_HOURS($ParData, $RecordType, $returnAssociativeArr=1);
		
	# check if the records in enrolment and OLE are in current school year or not
	$sql = "SELECT
				if (MONTH(StartDate) > 8,
					CONCAT(YEAR(StartDate),'-',YEAR(StartDate)+1),
					CONCAT(YEAR(StartDate)-1,'-',YEAR(StartDate))) AS Period_OLE,
				if (MONTH(NOW()) > 8,
					CONCAT(YEAR(NOW()),'-',YEAR(NOW())+1),
					CONCAT(YEAR(NOW())-1,'-',YEAR(NOW()))) AS Period_Now,
				if (MONTH('".$startdate."') > 8,
					CONCAT(YEAR('".$startdate."'),'-',YEAR('".$startdate."')+1),
					CONCAT(YEAR('".$startdate."')-1,'-',YEAR('".$startdate."'))) AS Period_Enrolment
			FROM
				{$eclass_db}.OLE_PROGRAM
			WHERE
				ProgramID = '$tempOLE_ProgramID'
			";
	$recordYearArr = $libenroll->returnArray($sql, 2);
	$Period_Now = $recordYearArr[0]['Period_Now'];
	$Period_Enrolment = $recordYearArr[0]['Period_Enrolment'];
	$Period_OLE = $recordYearArr[0]['Period_OLE'];

	if($tempOLE_ProgramID!="")	# already transfer
	{
		$currentSchYear = ($Period_Enrolment == $Period_Now)? true : false;
		
		# check if there is any OLE records in the school year of the enrolment record
		# also, in case after transfered , the OLE_PROGRAM is removed from iPortfolio OLE side. Then it will be a ProgramID in the 
		# INTRANET_GROUP and  INTRANET_ENROL_EVENTINFO but not exist in OLE_PROGRAM again, so need to create
//		$sql = "SELECT
//					ProgramID
//				FROM
//					{$eclass_db}.OLE_PROGRAM
//				WHERE
//					Title = '".$libenroll->Get_Safe_Sql_Query($tempTitle)."'
//					AND
//					StartDate = '$startdate'
//					AND
//					EndDate = '$enddate'
//					AND
//					IntExt = 'INT'
//				";
//		$OLEProgramID = $libenroll->returnVector($sql);

		$OLE_PROGRAM = $eclass_db.'.OLE_PROGRAM';
		$sql = "Select ProgramID From $OLE_PROGRAM Where ProgramID = '".$tempOLE_ProgramID."'";
		$OLEProgramID = $libenroll->returnVector($sql);
		
		if (count($OLEProgramID) == 0)
		{
			# no current school year record
			$actionFlag = "add";
		}
		else
		{
			# record existed - check skip or overwite
			if($duplicate_ole=="overwrite")
			{
				$actionFlag = "update";
			}
			else	# skip
			{					
				$actionFlag = "";	
			}
			$ProgramID = $OLEProgramID[0];
		}
	}
	else
	{
		$currentSchYear = ($Period_Enrolment == $Period_Now)? true : false;
		$actionFlag = "add";
	}
		
	$aytArr = getAcademicYearInfoAndTermInfoByDate($startdate);
	$ayID = $aytArr[0];
	$ytID = $aytArr[2];

	if($actionFlag=="add")
	{
/*
		# insert / update to OLE_PROGRAM
		$fields = "(ProgramType, Title, Category, ELE, Organization";
		$fields .= ", Details, SchoolRemarks ";
		$fields .= ",StartDate, EndDate, CreatorID, InputDate, ModifiedDate, IntExt)";
		$values = "('". $ProgramType ."', '".$title."', '".$category."', '".$ELEListStr."', '".$organization."'";
		$values .= ", '".$details."', '".$SchoolRemarks."'";
		$values .= ", '".$startdate."', '".$enddate."', $UserID, now(), now(), '".$SubmitType."')";
	
		$sql = "INSERT INTO {$eclass_db}.OLE_PROGRAM $fields VALUES $values";
		$LibPortfolio->db_db_query($sql);
		
		$ProgramID = $LibPortfolio->db_insert_id();
*/

		$objOLEPROGRAM = new ole_program();		

		$objOLEPROGRAM->setProgramType($ProgramType);
		$objOLEPROGRAM->setTitle($title);
		$objOLEPROGRAM->setCategory($category);
		$objOLEPROGRAM->setSubCategoryID(-9);
		$objOLEPROGRAM->setELE($ELEListStr);		
		$objOLEPROGRAM->setOrganization($organization);
		$objOLEPROGRAM->setDetails($details);
		$objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
		$objOLEPROGRAM->setStartDate($startdate);
		$objOLEPROGRAM->setEndDate($enddate);
		$objOLEPROGRAM->setCreatorID($UserID);
		$objOLEPROGRAM->setIntExt($SubmitType);
		$objOLEPROGRAM->setAcademicYearID($ayID);
		$objOLEPROGRAM->setYearTermID($ytID);
		$objOLEPROGRAM->setCanJoinStartDate("null");
		$objOLEPROGRAM->setCanJoinEndDate("null");
		$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["enrollmentTransfer"]);

		$ProgramID = $objOLEPROGRAM->SaveProgram();	

	}
	else if($actionFlag=="update")
	{
/*
		$sql  = "UPDATE {$eclass_db}.OLE_PROGRAM set ";
		$sql .= "Title ='". $title ."', ";
		$sql .= "Category ='". $category ."', ";
		$sql .= "ELE ='". $ELEListStr ."', ";
		$sql .= "Organization ='". $organization ."', ";
		$sql .= "Details ='". $details ."', ";
		$sql .= "SchoolRemarks ='". $SchoolRemarks ."', ";
		$sql .= "StartDate ='". $startdate ."', ";
		$sql .= "EndDate ='". $enddate ."', ";
		$sql .= "CreatorID ='". $UserID ."', ";
		$sql .= "ModifiedDate = now() ";
		$sql .= "where ProgramID=$ProgramID";
		
		$LibPortfolio->db_db_query($sql);
*/

		$objOLEPROGRAM = new ole_program($ProgramID);
		$objOLEPROGRAM->setTitle($title);
		$objOLEPROGRAM->setCategory($category);
		$objOLEPROGRAM->setELE($ELEListStr);		
		$objOLEPROGRAM->setOrganization($organization);
		$objOLEPROGRAM->setDetails($details);
		$objOLEPROGRAM->setSchoolRemarks($SchoolRemarks);
		$objOLEPROGRAM->setStartDate($startdate);
		$objOLEPROGRAM->setEndDate($enddate);		
		$objOLEPROGRAM->setCreatorID($UserID);
		$objOLEPROGRAM->setAcademicYearID($ayID);
		$objOLEPROGRAM->setYearTermID($ytID);
		$objOLEPROGRAM->setComeFrom($ipf_cfg["OLE_PROGRAM_COMEFROM"]["enrollmentTransfer"]);  // for safe in data migration , set the program come from  to "enrollmentTransfer"

		$ProgramID = $objOLEPROGRAM->SaveProgram();	

	}
	
	
	if($RecordType=="club")
	{
		# update INTRANET_GROUP field "OLE_ProgramID"
		$sql = "UPDATE INTRANET_ENROL_GROUPINFO SET OLE_ProgramID = '$ProgramID' WHERE EnrolGroupID = '".$tempGroupID."'";
	}
	else
	{
		# update INTRANET_ENROL_EVENTINFO field "OLE_ProgramID"
		$sql = "UPDATE INTRANET_ENROL_EVENTINFO SET OLE_ProgramID = '$ProgramID' WHERE EnrolEventID = '".$tempGroupID."'";
	}

	$LibPortfolio->db_db_query($sql);
		
			
	# get member list
	if($RecordType=="club")
	{
		$stu_sql = "select 
					a.UserGroupID,
					a.UserID, 
					c.Title,
				";

		# Eric Yip (20090618): Sync different field for Belilios
		if($sys_custom["portfolio_customized_report_bps"])
			$stu_sql .= "a.Performance,";
		else
			$stu_sql .= "a.CommentStudent,";

		$stu_sql .=	"
					a.OLE_STUDENT_RecordID,
					a.isActiveMember,
					a.Achievement
				from 
					INTRANET_USERGROUP as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.UserID = b.UserID
					LEFT OUTER JOIN INTRANET_ROLE as c ON a.RoleID = c.RoleID
				where 
					b.RecordType = 2 
					and 
					a.EnrolGroupID=$tempGroupID
			";
	}
	else
	{
		$stu_sql = "select 
					a.EventStudentID,
					a.StudentID, 
					a.RoleTitle,
				";

		# Eric Yip (20090618): Sync different field for Belilios
		if($sys_custom["portfolio_customized_report_bps"])
			$stu_sql .= "a.Performance,";
		else
			$stu_sql .= "a.CommentStudent,";

		$stu_sql .=	"
					a.OLE_STUDENT_RecordID,
					a.isActiveMember,
					a.Achievement
				from 
					INTRANET_ENROL_EVENTSTUDENT as a
					LEFT OUTER JOIN INTRANET_USER as b ON a.StudentID = b.UserID
				where 
					a.RecordStatus = 2 
					AND
					a.EnrolEventID = $tempGroupID
					And
					b.RecordType = 2
				";
	}
	$stuResult = $libenroll->returnArray($stu_sql);
		
	### delete record in OLE if the student is no longer a member
	# get all OLE records of the group
	// syn member list of eEnrolment with OLE even if the transferring record is not current school year
//	if ($currentSchYear)
//	{
		if($RecordType=="club")
		{
			$sqlOLE = "SELECT RecordID FROM {$eclass_db}.OLE_STUDENT WHERE GroupID = '$tempGroupID' AND ProgramID = '$ProgramID' AND EnrolType = 'C' ";
		}
		else
		{
			$sqlOLE = "SELECT RecordID FROM {$eclass_db}.OLE_STUDENT WHERE GroupID = '$tempGroupID' AND ProgramID = '$ProgramID' AND EnrolType = 'A' ";
		}
		$OLERecord = $libenroll->returnVector($sqlOLE);
		
		# build Group OLE_STUDENT_RecordID array
		$GroupOLERecord = array();
		for ($j=0; $j<sizeof($stuResult); $j++)
		{
			$GroupOLERecord[] = $stuResult[$j]['OLE_STUDENT_RecordID'];
		}
		
		# get students who are not a current member of the group
		$nonMemberArr = array_diff($OLERecord, $GroupOLERecord);	//set $nonMemberArr  = data exist in $OLERecord but not in $GroupOLERecord
		$nonMemberList = implode(",", $nonMemberArr);
		
		# delete records
		$sqlOLE = "DELETE FROM {$eclass_db}.OLE_STUDENT WHERE RecordID IN ($nonMemberList)";
		$LibPortfolio->db_db_query($sqlOLE);
//	}

	### Get the Student List to be transferred
	$thisFormArr = array_keys((array)$FormChkArr[$tempGroupID]);
	$condsFormID = " And yc.YearID In (".implode(',', $thisFormArr).") ";
	$sql = "Select 
				ycu.UserID
			From
				YEAR_CLASS_USER as ycu
				Inner Join
				YEAR_CLASS as yc
				On (ycu.YearClassID = yc.YearClassID)
			Where
				yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
				$condsFormID
			";
	$thisFormStudentIDArr = $libenroll->returnVector($sql);
	
	### add student
	$numTransferred = 0;
	for($j=0;$j<sizeof($stuResult);$j++)
	{
		list($tempUserGroupID, $tempStuID, $tempRole, $tempComment, $tempOLE_STUDENT_RecordID, $tempIsActiveMember, $tempAchievement) = $stuResult[$j];
				
		### Skip the students who are not in the selected Form
		if (!in_array($tempStuID, $thisFormStudentIDArr))
			continue;
				
		$RecordStatus = 2;
		$tempRole = addslashes($tempRole);
		$teacher_comment = addslashes($tempComment);
		$role = $tempRole;
		
		# double check student is duplicate or not
		$dup = "select RecordID from {$eclass_db}.OLE_STUDENT where UserID=$tempStuID and ProgramID=$ProgramID";
		$dup_result = $libenroll->returnArray($dup);
		if(sizeof($dup_result))	
		{
			if($duplicate_pp=="overwrite")	
			{
				$del_sql = "delete from {$eclass_db}.OLE_STUDENT where UserID=$tempStuID and ProgramID=$ProgramID";
				$LibPortfolio->db_db_query($del_sql);
			}
			else
			{
				continue;
			}
		}
		
		# skip non-active member if the user choose to transfer active member only
		if ($studentSetting == "activeOnly" && $tempIsActiveMember != 1)
		{
			continue;	
		}
		
		# get student OLE hours
		$hours = $OLE_HoursArr[$tempStuID];
		
		# Do not transfer zero hour record if the option is selected
		if ($zeroHour == "notTransfer" && $hours == 0)
		{
			continue;
		}
					
		$fields = "(UserID, StartDate, EndDate, Title, Details";
		$fields .= ", Category, Role, Hours, Achievement, Remark, ApprovedBy ";
		$fields .= ",RecordStatus, ProcessDate, InputDate, ModifiedDate, ELE";
		$fields .= ",Organization, ProgramID, TeacherComment, IntExt, GroupID, EnrolType,ComeFrom)";
		
		$enrolType = ($RecordType=="club")? "C" : "A";
		
		$values = "('". $tempStuID ."', '".$startdate."', '".$enddate."', '".$title."', '".$details."'";
		$values .= ", '".$category."', '".$role."', '". $hours ."', '".$libenroll->Get_Safe_Sql_Query($tempAchievement)."', '". $SchoolRemarks ."', '". $UserID ."'";
		$values .= ", '".$RecordStatus."', now(), now(), now(), '".$ELEListStr."'";
		$values .= ", '".$organization."', '".$ProgramID."', '". $teacher_comment ."', '".$SubmitType."', '".$tempGroupID."', '".$enrolType."','".$ipf_cfg["OLE_STUDENT_COMEFROM"]["enrollmentTransfer"]."')";
		
		$sql = "INSERT INTO {$eclass_db}.OLE_STUDENT $fields VALUES $values";		

		$LibPortfolio->db_db_query($sql);
		$OLE_STUDENT_RecordID = $LibPortfolio->db_insert_id();
		$numTransferred++;		
		
		if($RecordType=="club")
		{
			# update INTRANET_USERGROUP field "OLE_STUDENT_RecordID"
			$sql = "UPDATE INTRANET_USERGROUP SET OLE_STUDENT_RecordID = '$OLE_STUDENT_RecordID' WHERE UserGroupID = '".$tempUserGroupID."'";
		}
		else
		{
			# update INTRANET_ENROL_EVENTSTUDENT field "OLE_STUDENT_RecordID"
			$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET OLE_STUDENT_RecordID = '$OLE_STUDENT_RecordID' WHERE EventStudentID = '".$tempUserGroupID."'";
		}
		$LibPortfolio->db_db_query($sql);
	} // End loop student
	
	$numTransferredArr[] = $numTransferred;	
} // End loop Group

$numTransferredList = implode(",", $numTransferredArr);

header("Location: transfer_ole3.php?RecordType=$RecordType&ID_Str=$ID_Str&numTransferredList=$numTransferredList");

intranet_closedb();
?>