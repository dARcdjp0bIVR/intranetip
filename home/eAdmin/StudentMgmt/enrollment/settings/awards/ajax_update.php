<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();

$Action = $_POST['Action'];
if ($Action == 'Reorder_Award') {
	$DisplayOrderString = $_POST['DisplayOrderString'];
	$displayOrderArr = $libenroll->Get_Update_Display_Order_Arr($DisplayOrderString);
	$success = $libenroll->Update_Award_Record_DisplayOrder($displayOrderArr);
	echo $success? '1' : '0';
}

intranet_closedb($Action);
?>