<?php
/*
 * 	Log
 * 	Date:	2018-08-21 Anna
 *          Added $sys_custom['eEnrolment']['EnableEditUsedRole'] 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');


$CurrentPage = "PageRoleSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();


## Display Result
/////>>>>>
$thWidthArr = array(3,37,60); //define the percentages of width
$tdHeading = array('#',$eEnrollment['role'],$Lang['eEnrolment']['Settings']['Role']['WebSAMSCode']);

##Get Role data
$sql = 'SELECT RoleID, Title, WebSAMSCode FROM INTRANET_ROLE WHERE RecordType = 5';
$roleArr = $libenroll->returnResultSet($sql);

// $libdb = new libdb();
// $roleArr = $libdb ->returnArray($sql);

$toolbar = '';
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php')", '', '', '', '', 0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('export.php')", '', '', '', '', 0);

//get role in used 
$sql = "select distinct iug.RoleID 
		from INTRANET_USERGROUP as iug
		inner join INTRANET_GROUP as ig on (ig.GroupID = iug.GroupID) 
		Where ig.RecordType = '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'";
$RoleIDAry = $libenroll->returnResultSet($sql); 
$RoleID = array();
for($i=0;$i<count($RoleIDAry);$i++){
	$RoleID[] = $RoleIDAry[$i]['RoleID'];	
}

/////<<<<<
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="post" action="edit_update.php">'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					//$x .= '<div class="content_top_tool">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .=$toolbar;
							//$x .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Category();")."\n";
						$x .= '</div>'."\n";
				//	$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x.='<div class="table_row_tool row_content_tool">';
						$x.='<div style="display: inline-block; padding: 10px;">'.$linterface->GET_SMALL_BTN($button_edit, "button","javascript:editForm();","EditBtn").'</div>';
						$x.='<div style="display: inline-block;">'.$linterface->GET_SMALL_BTN($button_delete, "button","checkRemove(document.form1,'RoleID[]','role_remove.php');","DeleteBtn").'</div>';
					$x.='</div>';
					//$x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);			
						$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$countOfColumn = count($tdHeading);
									for($i=0;$i<$countOfColumn;$i++){
										$x .= '<th style="width:'.$thWidthArr[$i].'%;">'.$tdHeading[$i].'</th>'."\n";
									}
									$x .='<th class="num_check"><input type="checkbox" onClick="(this.checked)?setChecked(1,document.form1,\'RoleID[]\'):setChecked(0,document.form1,\'RoleID[]\')"></th>';
								$x .= '<tr>'."\n";
							$x .= '</thead>'."\n";
					
							$x .= '<tbody>'."\n";
								
								if (empty($roleArr)) {
									$x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
								else {
									$countOfRow = count($roleArr);
									for($i=0;$i<$countOfRow;$i++){
										$x .= '<tr id="'.$roleArr[$i]['RoleID'].'">';
										$x .= '<td>'.($i+1).'</td>';
										if($sys_custom['eEnrolment']['EnableEditUsedRole']){
										    $x .= '<td class="websams_text">'.$roleArr[$i]['Title'].'<input type="hidden" name="roleID[]" value="'.$roleArr[$i]['RoleID'].'"></td>';
										    $x .= '<td class="websams_editable" style="display:none;">';
										    $x .= '<input type="text" value="'.$roleArr[$i]['Title'].'" name="RoleTitle[]"></td>';
										}else{
										    $x .= '<td>'.$roleArr[$i]['Title'].'<input type="hidden" name="roleID[]" value="'.$roleArr[$i]['RoleID'].'"></td>';
										}
										
										
										$x .= '<td class="websams_text">'.$roleArr[$i]['WebSAMSCode'].'</td>';
										$x .= '<td class="websams_editable" style="display:none;">';
											$x .= '<input type="text" value="'.$roleArr[$i]['WebSAMSCode'].'" name="WebSAMS[]">';
											if(!in_array($roleArr[$i]['RoleID'],$RoleID)){
												$x .= '<td class="num_check" align="center" valign="top"><input type="checkbox" name="RoleID[]" value="'.$roleArr[$i]['RoleID'].'"></td>';
											}else{
												$x .='<td></td>';
											}
										
											$x .= '</td>';
									$x .= '</tr>';		
									}	
								}
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";	
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .='<div class="edit_bottom_v30">';
	$x .='<p class="spacer"></p>';
		$x .= '<div style="display: inline-block; padding: 5px;">'.$linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'").'</div>';
		$x .= '<div style="display: inline-block; padding: 5px;">'.$linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'").'</div>';
	$x .='<p class="spacer"></p>';
	$x .='</div>';
$x .= '</form>'."\n";
$x .= '<br />'."\n";


$TAGS_OBJ[] = array($eEnrollment['role'], "", 1);
$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_JS_CSS();	
?>

<script language="javascript">
function editForm(){
	$('#EditBtn').hide();
	$('#DeleteBtn').hide();
	$('#UpdateBtn').show();
	$('#cancelbtn').show();
	$('.websams_text').hide();
	$('.websams_editable').show();
	$('.num_check').hide();
	
}
// function deleteRole(obj,element,page){
//         	var id = document.getElementsByName(element);
        	
//         	var len = id.length;
//         	var targetIndex;
//         	for( i=0 ; i<len ; i++) {
//                 if (id[i].checked)
//                 {
//                 	targetIndex = i;
//                 	break;
//             	}
// 	        }
//             obj.action=page;
//             obj.submit();
 
// }
function ResetInfo(){
	document.form1.reset();	
	$('#EditBtn').show();
	$('#UpdateBtn').hide();
	$('#cancelbtn').hide();
	$('.websams_text').show();
	$('.websams_editable').hide();
	$('.num_check').show();
	$('#DeleteBtn').show();
	
}
</script>
<?=$x?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>