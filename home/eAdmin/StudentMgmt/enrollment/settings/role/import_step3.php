<?php
//using  
################## Change Log [Start] #################
#
#	Date	:	2017-06-05	Anna
# 			Create this page
#
################## Change Log [End] ###################


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

$CurrentPage = "PageRoleSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();


$result = array();
####importing data
$result = $libenroll->GET_ENROL_ROLE_TEMP();


$successArr = array();

foreach($result as $_RoleData){
	 	
		 
	$_role = $_RoleData['Title'];
 	$_webSAMSCode = $_RoleData['WebSAMS'];
 
 $successArr[] =  $libenroll->Add_Import_Role($_role,$_webSAMSCode);

}

if(in_array(false,$successArr)){
	$numSuccess = 0;
}
else{
	$numSuccess = count($successArr);
}


### iFrame for validation
$thisSrc = "ajax_task.php?task=validateSelfAccountImport&targetFilePath=".$targetFilePath;
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";


// ### navigation
$navigationAry[] = array($eEnrollment['role'], 'javascript: goCancel();');
$navigationAry[] = array($Lang['Btn']['Import']); 
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=3);

### Buttons
$htmlAry['doneBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Done'], "button", "done();");

$TAGS_OBJ[] = array($eEnrollment['role'], "", 1);
$linterface->LAYOUT_START();
?>

<script type="text/javascript">
$(document).ready( function() {
	
});

function done() {
	window.location = 'role.php';
}
function goCancel(){
	window.location = 'role.php';
	
}

</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<br />
	<?=$htmlAry['steps']?>
	
		<div class="table_board">
		<table width="100%">
			<tr><td style="text-align:center;">
				<span id="NumOfProcessedPageSpan"><?=$numSuccess?></span> <?=$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully']?>
			</td></tr>
		</table>
		<?=$htmlAry['iframe']?>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
	
 <div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['doneBtn']?>
		<p class="spacer"></p>
	</div>
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>