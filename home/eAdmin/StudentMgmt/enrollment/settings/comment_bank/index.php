<?php
//# using:  
/********************************************************
 *  Modification Log
  *	Date: 2015-07-16 (Shan)
 * 		Update Searchbox Style
 * 
 *	Date: 2014-11-13 (Omas)
 * 		Add 3 Tag of different Record Type A,C,P
 *  
 * Date: 2013-04-11 (Rita)
 * Details: add access right checking
 * 
 * Date: 2013-03-26 (Rita)
 * Details: add this page for Achievement Comment Bank
 ********************************************************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
intranet_auth();
intranet_opendb();

# Set Cookies
$arrCookies = array();
$arrCookies[] = array("ck_eEnrol_Setting_Achievement_Comment_Bank_page_size", "numPerPage");
$arrCookies[] = array("ck_eEnrol_Setting_Achievement_Comment_Bank_page_number", "PageNumber");
$arrCookies[] = array("ck_eEnrol_Setting_Achievement_Comment_Bank_page_order", "order");
$arrCookies[] = array("ck_eEnrol_Setting_Achievement_Comment_Bank_page_field", "field");

if(isset($clearCoo) && $clearCoo == 1)
{
	clearCookies($arrCookies);
	$pic_view = '';
	$helper_view = '';
}
else {
	updateGetCookies($arrCookies);
}


$keyword = standardizeFormGetValue($keyword);


$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();


# Access Right Checking 
if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

# Page Info
$CurrentPage = "PageCommentBankSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();



//$curTab = 'sample1';
//$TAGS_OBJ[] = array('Page Sample 1', 'javascript: void(0);', $curTab=='sample1');
//$TAGS_OBJ[] = array('Page Sample 2', '#', $curTab=='sample2');
//$TAGS_OBJ[] = array('Page Sample 3', 'javascript: void(0);', $curTab=='sample3');
# Page Tag 
$RecordType = ($RecordType=='')? $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'] : $RecordType;
$TAGS_OBJ[] = array($Lang['eEnrolment']['PerformanceCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['AchievementCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['TeacherCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment']);

# Tool Bar
$toolbar = '';
$toolbar .= $linterface->GET_LNK_NEW("javascript:checkNew('new.php?RecordType=$RecordType')", '', '', '', '', 0);
$toolbar .= $linterface->GET_LNK_IMPORT("javascript:checkNew('import.php?RecordType=$RecordType')", '', '', '', '', 0);
$toolbar .= $linterface->GET_LNK_EXPORT("javascript:checkNew('export.php?RecordType=$RecordType')", '', '', '', '', 0);

//# Get Achievement Data
//$achievementCommentSql= $libenroll->Get_Achievement_Comment_Sql();
//$achievementCommentDataArray = $libenroll->db_db_query($achievementCommentSql);


$htmlAry['searchBox'] = $linterface->Get_Search_Box_Div('keyword', $keyword);


//$sql = " SELECT CommentCode,Comment  From INTRANET_ENROL_COMMENT_BANK 
//where RecordStatus LIKE \"1\"
//and RecordType LIKE \"".$RecordType."\"
//and (CommentCode LIKE \"%".$keyword."%\" or Comment LIKE \"%.$keyword.%\" )" ;
//$SearchKey = $libenroll->returnResultSet($sql);
		
if ($keyword == ""){	
	$html['AchievementTableDisplay'] = '';
$html['AchievementTableDisplay'] = $libenroll_ui->Get_Achievement_Comment_Bank_DBTable('',$pageNo,'','','',$RecordType);
}

else{
	$searchResult = $libenroll_ui->Get_Achievement_Comment_Bank_DBTable($keyword,$pageNo,'','','',$RecordType);
	//debug_pr($searchResult);	
 	$html['AchievementTableDisplay'] =$searchResult;			
}
 
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);

?>

<form name="form1" method="get">
<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
	<tr>
		<td align="left"><?=$toolbar?></td>	
		<td><?=$htmlAry['searchBox']?></td>
	</tr>
</table>

<?= $html['AchievementTableDisplay'];?>

<input type="hidden" id="RecordType" name="RecordType" value="<?=$RecordType?>" />
</form>

<?

intranet_closedb();
$linterface->LAYOUT_STOP();
    
?>
