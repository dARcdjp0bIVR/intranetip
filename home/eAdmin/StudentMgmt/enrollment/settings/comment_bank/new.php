<?php
//# using:  

/********************************************************
 *  Modification Log
 * 
 *  Date: 2016-01-22 (Kenneth)
 * 		Return WebSAMS Code for RecordType = 'P'
 * 		Add * into compulsory input
 * 
 *	Date: 2014-11-13 (Omas)
 *		Pass RecordType to sepearate Achievement, Comments, Performance
 *
 *  Date: 2013-03-26 (Rita)
 * Detail: add this page for Achievement Comment Bank
 ********************************************************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping_ui.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();

# Access Right Checking 
if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageCommentBankSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Page tag
//$RecordType = ($RecordType=='')? $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'] : $RecordType;
$TAGS_OBJ[] = array($Lang['eEnrolment']['PerformanceCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['AchievementCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['TeacherCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment']);
 
# Page Navigation 
$PAGE_NAVIGATION[] = array($button_new);

# Get Current Record Info
$CommentID = $_GET['CommentID'][0];

if($CommentID){
	$sql = $libenroll->Get_Achievement_Comment_Sql($CommentID,'',$RecordType);
	$commentRecordInfo =  $libenroll->returnArray($sql);
	$thisCommentId = $commentRecordInfo[0]['CommentID'];
	$thisCommentCode = $commentRecordInfo[0]['CommentCode'];
	$thisComment = $commentRecordInfo[0]['Comment'];
	$thisWebSAMSCode = $commentRecordInfo[0]['WebSAMSCode'];
}

# Get Existing Data	
$html['NewCommentTable'] ='';		
$html['NewCommentTable'] = '<table width="100%" class="form_table_v30">';

# Comment Code
$html['NewCommentTable'] .= '<tr>';
	$html['NewCommentTable'] .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['General']['Code'] . '<td>';
	$html['NewCommentTable'] .= '<td  width="75%">' . $linterface->GET_TEXTBOX('code', 'code', $thisCommentCode, 'size="16"') .'<td>';
$html['NewCommentTable'] .= '</tr>';

# Comment 
$html['NewCommentTable'] .= '<tr>';
	$html['NewCommentTable'] .= '<td class="field_title"><span class="tabletextrequire">*</span>' . $Lang['Group']['Comment']. '<td>';
	$html['NewCommentTable'] .= '<td  width="75%">' . $linterface->GET_TEXTAREA('comment', $thisComment) .'<td>';
$html['NewCommentTable'] .= '</tr>';

if($RecordType=='P'){
	# WebSAMS
	$html['NewCommentTable'] .= '<tr>';
	$html['NewCommentTable'] .= '<td class="field_title">' . $Lang['eEnrolment']['Settings']['Role']['WebSAMSCode']. '<td>';
	$html['NewCommentTable'] .= '<td  width="75%">' . $linterface->GET_TEXTBOX('WebSAMScode', 'WebSAMScode', $thisWebSAMSCode, 'size="16"') .'<td>';
	$html['NewCommentTable'] .= '</tr>';
}

$html['NewCommentTable'] .= '<input type="hidden" id="commentID" name="commentID" value="'.$thisCommentId.'" />';
$html['NewCommentTable'] .= '</table>';
		
		
$linterface->LAYOUT_START();
		
?>
<script type="text/JavaScript" language="JavaScript">

function checkForm() {
	var error = 0;
	obj = document.form1;
	var code = $.trim($('#code').val());
	
	// Code length connot > 16
	var codelength = code.length;
	if(codelength>16){
		var extralength = codelength - 16;	
	}
	code = code.slice(0,16);
	
	var comment = $.trim($('#comment').val());
	var commendId = $.trim($('#commentID').val());
	var webSAMSCode = $.trim($('#WebSAMScode').val());
	
	// Code If Comment Is Blank
	if (!comment) {
		error++;
		alert('<?php echo $Lang['eEnrolment']['jaWarning']['PleaseFillInComment'];?>');
		obj.elements["commentEng"].focus();		 
	}
	
	// Code If webSAMSCode Is Blank
//	if (!webSAMSCode) {
//		error++;
//		alert('<?php echo $Lang['eEnrolment']['jaWarning']['PleaseFillInWebSAMS'];?>');
//		obj.elements["WebSAMScode"].focus();		 
//	}
	
	// Check If Code Is Blank
	if (!code) {
		error++;
		alert('<?php echo $Lang['eEnrolment']['jaWarning']['PleaseFillInCode'];?>');		 
		obj.elements["code"].focus();
	}
	else
	{
		// Check Code Duplication
		$.post("ajax_check_comment_code.php",
		 {
		   code: code,
		   comment: comment,
		   commendId: commendId
		 },
		 function(data)
		 {
		   if(data=='AlreadyExist'){
		   	error++;
		   	alert('<?php echo $Lang['eEnrolment']['jaWarning']['CodeAlreadyExists'];?>');
		   }		    
		   if(error==0){
				obj.submit();
			}	
		});
	}	
}

</script>

<form name="form1" action="new_update.php" method="POST">
	<table width="100%" border="0" cellpadding="3" cellspacing="0" align="center">
		<tr>
			<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
			<td align="right"> <?=$linterface->GET_SYS_MSG($Result, $SpMessage);?></td>
		</tr>
		<tr>
			<td>
			<?=$html['NewCommentTable'];?>
			</td>	
		</tr>
		<tr>
			<td class="dotline">
				<img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1">
			</td>
		</tr>
		<tr>
			<td align="center">	
				<?= $linterface->GET_ACTION_BTN($button_submit, "button", "javascript: return checkForm()")?>&nbsp;
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php?RecordType=$RecordType'")?>					
			</td>
		</tr>
	</table>
	<input type="hidden" name="RecordType" value=<?=$RecordType?> />
</form>



<?

intranet_closedb();
$linterface->LAYOUT_STOP();

?>
