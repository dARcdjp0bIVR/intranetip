<?php
// Using: 
/********************************************************
 *  Modification Log
 *  
 * Date: 2016-11-19 (Kenneth)
 * 		modify sql with 'WebSAMSCode'
 * 
 * Date: 2014-11-13 (Omas)
 *		Pass RecordType to sepearate Achievement, Comments, Performance
 * 
 * Date: 	2013-04-17	(Rita)
 * Details:	add standardizeFormPostValue() for $_POST Value
 * 
 * Date: 	2013-03-26 (Rita)
 * Detail: 	add this page for Achievement Comment Bank
 ********************************************************/
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$plugin['eEnrollment']){
	intranet_closedb();
	header("Location:/index.php");
}

if (!isset($_POST))
{
	header("Location:new.php");
}

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();

if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	header("Location:/index.php");
}

$code = standardizeFormPostValue($_POST['code']);
$comment = standardizeFormPostValue($_POST['comment']);
$commentID = trim($_POST['commentID']);

if($commentID!=''){	
	$successResult =  $libenroll->Update_Achievement_Comment($commentID,$code,$comment,$WebSAMScode);	

}else{
	$successResult = $libenroll->Add_Achievement_Comment($code, $comment, $RecordType, $WebSAMScode);
}


$xmsg ='';
if($successResult){
	$xmsg = 'UpdateSuccess';
}else{
	$xmsg = 'UpdateUnsuccess';
}
if(isset($RecordType)){
	$RecordTypePOST = "&RecordType=$RecordType";
}
header("Location:index.php?xmsg=$xmsg$RecordTypePOST");


?>