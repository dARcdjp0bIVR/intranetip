<?php
// Using:  

/************************************
 * Date: 2016-01-19 (Kenneth)
 * 		Add Remarks - WebSAMS Code for 'Performance'
 * 		Redirect another sample csv file for 'Performance'
 *  
 * Date: 2015-06-10 (Shan)
 * 	    Updating UI standard, Import Format, goSubmit(), button
 * 
 * 	Date: 2015-06-02 (Evan)
 * 		Update the interface according to UI standard
 * 
 *	Date: 2014-11-13 (Omas)
 *		Passing new parameter Record Type A,C,P
 * 
 *  Date: 2013-04-11  (Rita)
 * Details: Create this page
 ************************************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageCommentBankSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Page Tag
//$RecordType = ($RecordType=='')? $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'] : $RecordType;
$TAGS_OBJ[] = array($Lang['eEnrolment']['PerformanceCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['AchievementCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['TeacherCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment']);

### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=1);

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
if ($RecordType=="P") $navigationAry[] =array($Lang['eEnrolment']['PerformanceCommentBank'], 'index.php?RecordType='.$RecordType);
else if ($RecordType=="A") $navigationAry[] =array($Lang['eEnrolment']['AchievementCommentBank'], 'index.php?RecordType='.$RecordType);
else if  ($RecordType=="C") $navigationAry[] =array($Lang['eEnrolment']['TeacherCommentBank'], 'index.php?RecordType='.$RecordType);
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


# Sample CSV
if($RecordType=='P'){
	$SampleCSV = "sample_achievement_comment_performace.csv";	
}else{
	$SampleCSV = "sample_achievement_comment.csv";	
}
$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$xmsg]);


//### Import Format
$ColumnTitleArr[] = $Lang['General']['Code'];
$ColumnTitleArr[] = $Lang['Btn']['Comment'];

if($RecordType=='P'){
	$ColumnTitleArr[] = $Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'];
	$ColumnPropertyArr = array(1,1,0);
}else{
	$ColumnPropertyArr = array(1,1);
}


$ImportPageColumn = $linterface->Get_Import_Page_Column_Display($ColumnTitleArr, $ColumnPropertyArr, $RemarksArr);
$htmlAry['columnRemarks'] = $ImportPageColumn;


?>
<script type="text/JavaScript" language="JavaScript">
function resetForm() {
	document.form1.reset();
	changePicClass()
	return true;
}

function trim(str)
{
	return str.replace(/^\s+|\s+$/g,"");
}

function checkForm(){
	if($("#csvfile").val() == ''){
		alert("<?=$Lang['General']['warnSelectcsvFile']?>");
		return false;
	}
	else{
		return true;
	}
	
}

function goSubmit() {
	if(checkForm()==true){
		$('form#form1').attr('action', 'import_step2.php?RecordType=<?=$RecordType?>').submit();	
	}
}

</script>

	

<form name="form1" id="form1" method="POST"  enctype="multipart/form-data">

<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<br \>
		<tr>		
		   <td><?=$htmlAry['navigation']?></td>
		</tr>
		<tr>
		   <td>&nbsp;</td><td>&nbsp;</td>
		</tr>
		<tr>
		   <td><?=$htmlAry['steps']?></td>
		</tr>
		<tr>
		   <td>&nbsp;</td><td>&nbsp;</td>
		</tr>
	
		<tr>
	    <td colspan="2">
	       <table width="90%" border="0" cellpadding="0" cellspacing="0" align="center" >
 		   	<tr>
			<td>
				<table class="form_table_v30">
					<tr>
						<td class="field_title">
						 <?= $linterface->RequiredSymbol().$Lang['General']['SourceFile'].  $Lang['General']['CSVFileFormat'] ?> </td> 
						<td class="tabletext"><input class="file" type="file" name="csvfile" id="csvfile"></td> 
				
					</tr>
					
					<tr>
					    <td class="field_title">
						 <?= $Lang['General']['CSVSample'] ?>
						</td>
						<td valign="top" nowrap="nowrap" colspan='2' class="tabletext">
							<a id="SampleCsvLink" class="tablelink" href="<?=GET_CSV($SampleCSV)?>" target="_blank">
								<img src='<?=$PATH_WRT_ROOT?>/images/<?=$LAYOUT_SKIN?>/icon_files/xls.gif' border='0' align='absmiddle' hspace='3'> 
								<?=$i_general_clickheredownloadsample?>
							</a>
						</td>
				   </tr>
						   
				    <tr>					   	   				 
						<td class="field_title">
						 <?= $Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn'] ?>
						</td>
						<td width="75%" class='tabletext'>
						 <?= $htmlAry['columnRemarks']?>
						</td>
				 	</tr>	
				</table>
			</td>
		</tr>
		<tr><td class='tabletext' colspan="max"><?= $linterface->MandatoryField();?> </td></tr>
		</table>
		<div class="edit_bottom_v30">
			<p class="spacer"></p>
			<?=  $linterface->Get_Action_Btn($Lang['Btn']['Continue'],  "button", "goSubmit()", 'sumbitBtn' )?>
			<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "window.location = 'index.php?RecordType=$RecordType'")?>
			<p class="spacer"></p>
		</div>
   </td>
  </tr>
 </table>
 
<input type="hidden" name="RecordType" value=<?=$RecordType?> />
</form>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();

?>
