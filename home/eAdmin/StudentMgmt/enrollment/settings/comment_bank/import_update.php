<?php
// using: 
/**********************************************
 *	Date: 2014-11-13 (Omas)
 *		Passing new parameter RecordType A,C,P for redirecting
 *
 *
 *  Date: 	2013-04-11 (Rita)
 * Details: create this page for import comment
 **********************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$limport = new libimporttext();

if (!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	header("Location:/index.php");
}

# Get http variable
$IsAdditional = (!isset($_POST['IsAdditional']) || $_POST['IsAdditional']=='')? 0 : $_POST['IsAdditional'];
$format_array = array("Code","Comment Content");

$li = new libdb();
$filepath = $userfile;
$filename = $userfile_name;

# Get the Comment Code of all comment to prevent duplicate comment code
$sql = $libenroll->Get_Achievement_Comment_Sql();	
$commentArr = $libenroll->returnArray($sql);

$commentCodeIdMap = array();
for($i=0; $i<sizeof($commentArr); $i++)
{
	$commentCodeIdMap[$commentArr[$i]["CommentCode"]] = $commentArr[$i]["CommentID"];
}

if($filepath=="none" || $filepath == "")
{   # import failed
	header("Location: import.php?Result=import_failed2");
	exit();
} 
else 
{
    if($limport->CHECK_FILE_EXT($filename)) 
    {
		# read file into array
		# return 0 if fail, return csv array if success
		$data = $limport->GET_IMPORT_TXT($filepath);
		#$toprow = array_shift($data);                   # drop the title bar
		
		$limport->SET_CORE_HEADER($format_array);
		if (!$limport->VALIDATE_HEADER($data, true) || !$limport->VALIDATE_DATA($data)) {
			header("Location: import.php?Result=import_failed");
			exit();
		}
		array_shift($data);
		$successCount = 0;
		$failCount = 0;
		$csvCodeList = array();
		$result = '';
	    for ($i=0; $i<sizeof($data); $i++) {
			if (empty($data[$i])) continue;
			
				list($code,$comment) = $data[$i];
				$code = strtoupper(trim($code));
				$comment = addslashes(intranet_htmlspecialchars(convert2unicode($comment)));
				
				# Update Existing Record Only if Same Comment Code
				if (isset($commentCodeIdMap[$code])) {	
					$result = $libenroll->Update_Achievement_Comment($commentCodeIdMap[$code], $code, $comment);						
				# Create New Record 
				} else if (!in_array($code, $csvCodeList)) {	
					$result = $libenroll->Add_Achievement_Comment($code, $comment,$RecordType);
					$csvCodeList[] = $code;
				}
				
				if($result){
					$successCount++;
				}	
	    }
	
	} else {
		header("Location: import.php?Result=import_failed&RecordType=$RecordType");
		exit();
	}
}

intranet_closedb();
$CommentType = ($type == '0') ? "class" : "subject";
if ($successCount === 0){
	$xmsg = "ImportUnsuccess";
}else{
	$xmsg = "ImportSuccess";
}
header("Location: index.php?xmsg=$xmsg&RecordType=$RecordType");
?>