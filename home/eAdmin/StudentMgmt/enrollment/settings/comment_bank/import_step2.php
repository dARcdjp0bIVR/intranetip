<?php
//using 
################## Change Log [Start] #################
#
#	Date	:	2015-06-10	Shan
# 			Create this page
#
################## Change Log [End] ###################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT.'includes/libimporttext.php');

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$lfs = new libfilesystem();
$limport = new libimporttext();

$CurrentPage = "PageCommentBankSetting";
$CurrentPageArr['eEnrolment'] = 2;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

### CSV Checking
$name = $_FILES['csvfile']['name'];
$ext = strtoupper($lfs->file_ext($name));

if(!($ext == ".CSV" || $ext == ".TXT"))
{
	intranet_closedb();
	header("location: import.php?xmsg=import_failed&RecordType=$RecordType"); 
	exit();
}
### move to temp folder first for others validation
$folderPrefix = $intranet_root."/file/import_temp/eEnrolment/comment_bank";
if (!file_exists($folderPrefix)) {
	$lfs->folder_new($folderPrefix);
}

$targetFileName = date('Ymd_His').'_'.$_SESSION['UserID'].$ext;
$targetFilePath = stripslashes($folderPrefix."/".$targetFileName);

$successAry['MoveCsvFileToTempFolder'] = $lfs->lfs_move($csvfile, $targetFilePath);

### Get Data from the csv file
$csvData = $limport->GET_IMPORT_TXT($targetFilePath);
$csvColName = array_shift($csvData);
$numOfData = count($csvData);

### iFrame for validation
$thisSrc = "ajax_task.php?task=validateCommentBankImport&RecordType=".$RecordType."&targetFilePath=".$targetFilePath;
$htmlAry['iframe'] = '<iframe id="ImportIFrame" name="ImportIFrame" src="'.$thisSrc.'" style="width:100%;height:300px;display:none;"></iframe>'."\n";


# Page Tag
//$RecordType = ($RecordType=='')? $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'] : $RecordType;
$TAGS_OBJ[] = array($Lang['eEnrolment']['PerformanceCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Performance']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['AchievementCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Achievement']);
$TAGS_OBJ[] = array($Lang['eEnrolment']['TeacherCommentBank'], "index.php?RecordType=".$enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment'], $RecordType == $enrolConfigAry['INTRANET_ENROL_COMMENT_BANK']['RecordType']['Comment']);

### navigation
// $navigationAry[] = array($displayLang, $onclickJs);
if ($RecordType=="P") $navigationAry[] =array($Lang['eEnrolment']['PerformanceCommentBank'], 'index.php?RecordType='.$RecordType);
else if ($RecordType=="A") $navigationAry[] =array($Lang['eEnrolment']['AchievementCommentBank'], 'index.php?RecordType='.$RecordType);
else if  ($RecordType=="C") $navigationAry[] =array($Lang['eEnrolment']['TeacherCommentBank'], 'index.php?RecordType='.$RecordType);
$navigationAry[] = array($Lang['Btn']['Import']);
$htmlAry['navigation'] = $linterface->GET_NAVIGATION_IP25($navigationAry);


### steps
$htmlAry['steps'] = $linterface->GET_IMPORT_STEPS($CurrStep=2);

### Block UI Msg
$processingMsg = str_replace('<!--NumOfRecords-->', '<span id="BlockUISpan">0</span> / '.$numOfData, $Lang['General']['ImportArr']['RecordsValidated']);

### Buttons
$htmlAry['importBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Import'], "button", "goImport();", 'ImportBtn', '', $Disabled=1);
$htmlAry['backBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();");
$htmlAry['cancelBtn'] = $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "goCancel();");

### Top Info Table
$x = '';
$x .= '<table class="form_table_v30">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['SuccessfulRecord'].'</td>'."\n";
		$x .= '<td><div id="SuccessCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td class="field_title">'.$Lang['General']['FailureRecord'].'</td>'."\n";
		$x .= '<td><div id="FailCountDiv"></div></td>'."\n";
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";
$htmlAry['formTable'] = $x;


$linterface->LAYOUT_START($ReturnMsg);
?>
<script type="text/javascript">
$(document).ready( function() {
	Block_Document('<?=$processingMsg?>');
});

function goCancel() {
	window.location = 'index.php?RecordType=<?=$RecordType?>';
}

function goBack() {
	window.location = 'import.php?RecordType=<?=$RecordType?>';
}

function goImport() {
	$('form#form1').attr('action', 'import_step3.php?RecordType=<?=$RecordType?>').submit();
}

</script>
<form name="form1" id="form1" method="POST">
	<?=$htmlAry['navigation']?>
	<br />	
	<?=$htmlAry['steps']?>
	
		<div class="table_board">
		<?=$htmlAry['formTable']?>
		<?=$htmlAry['iframe']?>
		<div id="ErrorTableDiv"></div>
		<p class="spacer"></p>
	</div>
	<br style="clear:both;" />
		
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?=$htmlAry['importBtn']?>
		<?=$htmlAry['backBtn']?>
		<?=$htmlAry['cancelBtn']?>
		<p class="spacer"></p>
	</div>
	
	
</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>