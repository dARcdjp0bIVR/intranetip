<?php
/*
 *  Log: 
 * 	Date: 2018-06-12 Anna
 *        Created this page
 * 
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");


intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$CurrentPage = "PageNameSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();


$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.action=\'activity_name.php\';document.form1.submit();"', 1, 0, $AcademicYearID);


$AvtivityInfoAry = $libenroll->Get_Activity_Info_By_Conditions('','',$AcademicYearID);
$AvtivityNameAry = array();
$EnrolEventIDAry = array();
foreach ($AvtivityInfoAry as $AvtivityInfo){
    $AvtivityNameAry[] = $AvtivityInfo['EventTitle'];
    $EnrolEventIDAry[] = $AvtivityInfo['EnrolEventID'];
}
$thisYearActivityNum = count($AvtivityInfoAry);

$TAGS_OBJ[] = array($Lang['eEnrolment']['NameSettings']['ClubNameSetting'], "club_name.php", 0);
$TAGS_OBJ[] = array($Lang['eEnrolment']['NameSettings']['ActivityNameSetting'], "", 1);

$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_JS_CSS();	
?>

<script language="javascript">
function editForm(){
	$('#EditBtn').hide();
	$('#UpdateBtn').show();
	$('#cancelbtn').show();
	$('.activity_text').hide();
	$('.activity_editable').show();
}

function ResetInfo(){
	document.form1.reset();	
	$('#EditBtn').show();
	$('#UpdateBtn').hide();
	$('#cancelbtn').hide();
	$('.activity_text').show();
	$('.activity_editable').hide();
}
</script>

<form id="form1" name="form1" method="post" action="edit_update.php">
    <div class="table_board">
        <table id="html_body_frame" width="100%">
            <tr>
                <td valign="bottom">
                    <div class="table_filter">
                    <?php echo $yearFilter;?>
                    </div>
                </td>
            </tr>
            <tr>
            	<td>
            		<div class="table_row_tool row_content_tool">
            		<?php echo  $linterface->GET_SMALL_BTN($button_edit, "button","javascript:editForm();","EditBtn");?>
            		</div>
            		<table id="ContentTable" class="common_table_list_v30">
            			<thead>
            				<tr>
            					<th>#</th>
            					<th><?php echo $Lang['eEnrolment']['NameSettings']['CurrentName'];?></th>
            					<th><?php echo $Lang['eEnrolment']['NameSettings']['DisplayName'];?></th>
            				</tr>
            			</thead>
            			<tbody>
            			<?php for ($i=0;$i<$thisYearActivityNum;$i++){?>
            			<?php    $_avtivityName = $AvtivityNameAry[$i];
            			         $_enrolEventID = $EnrolEventIDAry[$i];
            			         $_displayNameAry = $libenroll->get_Event_DisplayName($_enrolEventID);
            			         $_displayName = $_displayNameAry == ''? $_avtivityName :$_displayNameAry;
            				
            				?>
            				<tr>
            					
            					<td><?php echo ($i+1);?></td>
            					<td ><?php echo ($_avtivityName)?> <input type="hidden" name="EnrolEventIDAry[]" value="<?php echo $_enrolEventID;?>"></td>
            					<td class="activity_text"><?php echo $_displayName;?></td>
            					<td class="activity_editable" style="display:none;">
            					<input type="text" value="<?php echo $_displayName;?>" name="EventNameAry[]"></td>
            					
            				</tr>
            			<?php }?>
            			</tbody>
            		</table>
            		
            	</td>
            </tr>

        </table>
    </div>
    <div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?php  echo $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'");?>
		<?php echo $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'"); ?>
		<p class="spacer"></p>
	</div>
    <input name="changeName" type="hidden" value="activityName">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>