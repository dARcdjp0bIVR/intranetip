<?php 
# using: yat

################## Change Log [Start] ##############
#
#	Date:	2011-09-08	YatWoon
# 			lite version should not access this page 
#
#	Date:	2011-08-25	Yuen
#			support iPad/Andriod
#
################## Change Log [End] ##############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageSysSettingPopup";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
$linterface = new interface_html();
$TAGS_OBJ[] = array($Lang['eEnrolment']['PopupNotification']);
$linterface->LAYOUT_START($xmsg);


list($start_date, $start_time) = explode(" ", $libenroll->popup_startdate);
list($start_timeH, $start_timeM, $start_timeS) = explode(":", $start_time);
list($end_date, $end_time) = explode(" ", $libenroll->popup_enddate);
list($end_timeH, $end_timeM, $end_timeS) = explode(":", $end_time);

$display_date_range = $start_date . " ". $start_timeH .":".$start_timeM." " . $eEnrollment['to'] . " " . $end_date . " ".$end_timeH .":".$end_timeM;
$date_range = $libenroll->popup_startdate ? $display_date_range : $Lang['General']['EmptySymbol'];
$popup_content = $libenroll->popup_content ? $libenroll->popup_content : $Lang['General']['EmptySymbol'];

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
$objHtmlEditor = new FCKeditor ( 'popup_content' , "100%", "320", "", "", $libenroll->popup_content);

?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'display: '); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', 'display: ');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
	$('#popup_content').css("width", "100%");
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

function FCKeditor_OnComplete(EDITOR){
	EDITOR.SetHTML($('span#Content').html());
}
//-->
</script>
		
<form name="form1" method="post" action="edit_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","EditInfo();","EditBtn")?>
	</div>

	<table class="form_table_v30">
	
			<!-- Enable Popup Notification //-->
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eEnrolment']['EnablePopupNotification']?></td>
				<td> 
				<span class="Edit_Hide"><?=$libenroll->enable_popup_notification ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="enable_popup_notification" value="1" <?=$libenroll->enable_popup_notification ? "checked":"" ?> id="enable_popup_notification1"> <label for="enable_popup_notification1"><?=$i_general_yes?></label> 
				<input type="radio" name="enable_popup_notification" value="0" <?=$libenroll->enable_popup_notification ? "":"checked" ?> id="enable_popup_notification0"> <label for="enable_popup_notification0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			
			<!-- Popup date range //--> 
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eEnrolment']['PopupDateRange']?></td>
				<td> 
				<span class="Edit_Hide"><?=$date_range?></span>
				<span class="Edit_Show" style="display:none">
					<?= $linterface->GET_DATE_PICKER("start_date",$start_date) ?>
					<?= getTimeSel("start_time", $start_timeH, $start_timeM) ?>&nbsp;
					<?= $eEnrollment['to']?>&nbsp;
					<?= $linterface->GET_DATE_PICKER("end_date",$end_date) ?>
					<?= getTimeSel("end_time", $end_timeH, $end_timeM) ?>
				</span></td>
			</tr>
			
			<!-- Popup Notification Content //-->
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eEnrolment']['PopupContent']?></td>
				<td><span class="Edit_Hide" id="Content"><?=$popup_content?></span>
				<span class="Edit_Show" style="display:none"><?=$objHtmlEditor->CreateHtml()?></span>
				</td>
			</tr>
			
			
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>




<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
