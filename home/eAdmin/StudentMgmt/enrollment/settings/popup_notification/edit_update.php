<?php
# using:

################# Change Log [Start] ############
# 
#	Date:	2011-08-25	Yuen
#			handled description for iPad/Andriod
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
$lgeneralsettings = new libgeneralsettings();

$data = array();
$data['enable_popup_notification'] = $enable_popup_notification;
$data['popup_startdate'] = $start_date . " " . $start_timeHour .":".$start_timeMin.":00";
$data['popup_enddate'] = $end_date. " " . $end_timeHour .":".$end_timeMin.":59";
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($popup_content==strip_tags($popup_content))
	{
		$popup_content = nl2br($popup_content);
	}
}
$data['popup_content'] = stripslashes($popup_content);

# store in DB
$lgeneralsettings->Save_General_Setting($libenroll->ModuleTitle, $data);

intranet_closedb();
header("Location: index.php?xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);

?>