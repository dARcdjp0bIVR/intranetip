<?
# Using: 
##################
#
#	Date:	2015-06-03 Bill
#			Display system message using key from update.php
#
#	Date:	2015-06-02 Evan
#			Update the interface according to UI standard
#
##################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['eEnrollment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$libclub = new libclubsenrol();
$GeneralSetting = new libgeneralsettings();
$CurrentPageArr["eEnrolment"] = 1;
$CurrentPage = "PageTerminalSetting";
$linterface = new interface_html();

// Get saved Terminal IP
$SettingList[] = "'TerminalIP'";
$Settings = $GeneralSetting->Get_General_Setting('eEnrolment',$SettingList);
$ips = $Settings['TerminalIP'];

$TAGS_OBJ[] = array($i_SmartCard_TerminalSettings, "", 0);
$MODULE_OBJ = $libclub->GET_MODULE_OBJ_ARR();
// use key from update.php
//$Msg = urldecode($Msg);
$Msg = $Msg? $Lang['StudentAttendance'][urldecode($Msg)] : "";
$linterface->LAYOUT_START($Msg);

//if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
?>

<?=$linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $i_SmartCard_Description_Terminal_IP_Settings)?>

<form name="form1" method="post" action="update.php">
<table class="form_table_v30">
	<tr>
		<td colspan="2" align="right" style="border-bottom:0px;">
			<?=$SysMsg?>
		</td>
	</tr>
	<tr>
		<td class="field_title">
			<?=$i_SmartCard_Terminal_IPList?>
		</td>
		<td class="tabletext" width="70%">
			<span class="tabletextremark"><?=$i_SmartCard_Terminal_IPInput?><br />
			<?=$i_SmartCard_Terminal_YourAddress?>:<?=$HTTP_SERVER_VARS['REMOTE_ADDR']?></span><br />
			<?=$linterface->GET_TEXTAREA("IPList", $ips)?>
		</td>
	</tr>
	
</table>
<table width="95%" border="0" cellpadding="5" cellspacing="0" align="center">
	<tr>
    	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
    </tr>
    <tr>
	    <td align="center">
			<?= $linterface->GET_ACTION_BTN($button_save, "submit", "") ?>
			<?= $linterface->GET_ACTION_BTN($button_reset, "reset", "","reset2"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"") ?>
		</td>
	</tr>
</table>
</form>
<?
$linterface->LAYOUT_STOP();
print $linterface->FOCUS_ON_LOAD("form1.IPList");
intranet_closedb();
?>