<?
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_auth();
intranet_opendb();

if (!$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"] || !$_SESSION['SSV_PRIVILEGE']['plugin']['eEnrollment']) {
	header ("Location: /");
	intranet_closedb();
	exit();
}

$GeneralSetting = new libgeneralsettings();
$SettingList['TerminalIP'] = $_REQUEST['IPList'];

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting('eEnrolment',$SettingList)) {
	$GeneralSetting->Commit_Trans();
//	$Msg = $Lang['StudentAttendance']['SettingApplySuccess'];
	$Msg = "SettingApplySuccess";
}
else {
	$GeneralSetting->RollBack_Trans();
//	$Msg = $Lang['StudentAttendance']['SettingApplyFail'];
	$Msg = "SettingApplyFail";
}

header("Location: index.php?Msg=".urlencode($Msg));
?>