<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


if (!$plugin['eEnrollment'] && !$le->hasAccessRight($UserID) && !$le->IS_ENROL_ADMIN($UserID)) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
	//header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
}

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();	
$CurrentPage = "PageCategorySetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();


$linterface = new interface_html();
$TAGS_OBJ[] = array($eEnrollmentMenu['cat_setting'], "category.php", 0);
if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
	$TAGS_OBJ[] = array($Lang['eEnrolment']['GradingScheme'], "grading_scheme.php", 1);
}

$linterface->LAYOUT_START();

$category_selection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "document.form1.submit()", $eEnrollment['all_categories']);

//$CategoryList = $libenroll->GET_CATEGORY_LIST();

if($sel_category!="")
	$conds = " AND r.CategoryID='$sel_category'";

$sql = "
		SELECT
			CONCAT('<a title=\"', MinScore,'-',MaxScore,'\", href=\"grading_scheme_new.php?RuleID=', r.RuleID ,'\" class=\"tablelink\">', r.MinScore, ' - ', r.MaxScore, '</a>') as Score,
			r.GradeChar,
			c.CategoryName,
			r.DateModified, 
			CONCAT('<input type=checkbox name=\"RuleID[]\" value=\"', r.RuleID ,'\">')
		FROM
			INTRANET_ENROL_GRADE_RULE r INNER JOIN
			INTRANET_ENROL_CATEGORY c ON (c.CategoryID=r.CategoryID $conds)
		";

            
               	
if ($page_size_change == 1)
{
    $ck_page_size = $numPerPage;
}

if($order == "") $order = 0;
if($field == "") $field = 0;
$pageSizeChangeEnabled = true;

if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;

# TABLE INFO
$li = new libdbtable2007($field, $order, $pageNo);
$li->field_array = array("Score", "GradeChar", "CategoryName", "DateModified");
$li->sql = $sql;
$li->no_col = sizeof($li->field_array)+2;
$li->title = "";
$li->column_array = array(0,0);
$li->IsColOff = "IP25_table";

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<th width=1>#</th>\n";
$li->column_list .= "<th width=30%>".$li->column_IP25($pos++, $Lang['eEnrolment']['ScoreRange'])."</th>\n";
$li->column_list .= "<th width=20%>".$li->column_IP25($pos++, $Lang['eEnrolment']['Grade'])."</th>\n";
$li->column_list .= "<th width=30%>".$li->column_IP25($pos++, $Lang['eEnrolment']['CategoryName'])."</th>\n";
$li->column_list .= "<th width=20%>".$li->column_IP25($pos++, $Lang['General']['LastModified'])."</th>\n";
$li->column_list .= "<th width=1>".$li->check("CategoryID[]")."</th>\n";


# edit
$BtnArr[] = array("edit", "javascript:checkEdit(document.form1,'RuleID[]','grading_scheme_new.php')", $button_edit);
# delete
$BtnArr[] = array("delete", "javascript:checkRemove(document.form1,'RuleID[]','grading_scheme_remove_update.php')", $button_delete);
$actionBtn = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);
?>

<script language="javascript">
<?
if($msg != "") {
	$returnMsg = $Lang['General']['ReturnMessage'][$msg];
	echo "Get_Return_Message(\"$returnMsg\");";	
}
?>


function checkform(obj){
        return true;
}

</script>


<br/>
<form name="form1" method="POST" >
<div class="Conntent_tool">
	<a href="grading_scheme_new.php" class="new"><?=$Lang['Btn']['New']?></a>
</div>
<br style="clear:both">

<?=$category_selection?>

<?=$actionBtn?>

<?=$li->display()?>

<br />
<br />



<input type="hidden" name="pageNo" id="pageNo" value="<?=$li->pageNo; ?>">
<input type="hidden" name="order" id="order" value="<?=$li->order; ?>">
<input type="hidden" name="field" id="field" value="<?=$li->field; ?>">
<input type="hidden" name="page_size_change" id="page_size_change" value="">
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$li->page_size?>">
</form>

<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>