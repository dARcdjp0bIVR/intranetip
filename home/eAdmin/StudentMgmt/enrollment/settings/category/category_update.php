<?php
/*  
 *  Using: 
 * 	Log:
 * 
 *  Date:   2018-07-31 [Cameron] Add which teacher can teach which category [HKPF]
 *     
 * 	Date:	2013-08-09 [Cameron] Add CategoryTypeID for SIS
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$CategoryArr['CategoryName'] = $CategoryName;
$CategoryArr['DisplayOrder'] = $DisplayOrder;
$CategoryArr['OleCategoryID'] = ($_POST['OleCategoryID']=='')? 0 : $_POST['OleCategoryID'];
$CategoryArr['CategoryTypeID'] = ($_POST['CategoryTypeID']=='')? 0 : $_POST['CategoryTypeID'];

$instructorIDAry = $_POST['Instructor'];

$successAry = array();
$dataAry = array();
$libenroll->Start_Trans();

if ($CategoryID == "") {
	#Get Max DisplayOrder's value and increment it by 1
	$sql = "SELECT MAX(DisplayOrder) FROM INTRANET_ENROL_CATEGORY";
	$ReturnMaxOrderVal = $libenroll->returnVector($sql);
	$CategoryArr['DisplayOrder'] = $ReturnMaxOrderVal[0] + 1;
	
	$successAry['add'] = $libenroll->ADD_CATEGORY($CategoryArr);
	$categoryID = $libenroll->db_insert_id();
	
	if ($sys_custom['project']['HKPF'] && count($instructorIDAry[0])) {
	    $i = 0;
	    $instructorIDAry[0] = array_unique((array)$instructorIDAry[0]);      // remove duplicate instructor
	    foreach((array)$instructorIDAry[0] as $_instructorID) {
	        if ($_instructorID) {
            	unset($dataAry);
            	$dataAry['CategoryID'] = $categoryID;
            	$dataAry['InstructorID'] = IntegerSafe($_instructorID);
            	$sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_CATEGORY_INSTRUCTOR',$dataAry,array(),false, false, false);
            	$successAry["AddCategoryInstructor_{$i}"] = $libenroll->db_db_query($sql);
            	$i++;
	        }
	    }
	}	
	
} else {
    $CategoryID = IntegerSafe($CategoryID);
	$CategoryArr['CategoryID'] = $CategoryID;
	
	$CategoryInfoArr = $libenroll->GET_CATEGORYINFO($CategoryID);
	$CategoryArr['DisplayOrder'] = $CategoryInfoArr['DisplayOrder'];
	
	$successAry['edit'] = $libenroll->EDIT_CATEGORY($CategoryArr);
	
	if ($sys_custom['project']['HKPF']) {
	    $sql = "DELETE FROM INTRANET_ENROL_CATEGORY_INSTRUCTOR WHERE CategoryID='".$CategoryID."'";
	    $successAry["DeleteCategoryInstructor_{$CategoryID}"] = $libenroll->db_db_query($sql);
	    
	    $i = 0;
	    if (count($instructorIDAry[0])) {
    	    $instructorIDAry[0] = array_unique((array)$instructorIDAry[0]);      // remove duplicate instructor
    	    foreach((array)$instructorIDAry[0] as $_instructorID) {
    	        if ($_instructorID) {
        	        unset($dataAry);
        	        $dataAry['CategoryID'] = $CategoryID;
        	        $dataAry['InstructorID'] = IntegerSafe($_instructorID);
        	        $sql = $libenroll->INSERT2TABLE('INTRANET_ENROL_CATEGORY_INSTRUCTOR',$dataAry,array(),false, false, false);
        	        $successAry["AddCategoryInstructor_{$i}"] = $libenroll->db_db_query($sql);
        	        $i++;
    	        }
    	    }
	    }
	}
	
}

if (in_array(false, $successAry)) {
	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'UpdateUnsuccess'; 
}
else {
	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'UpdateSuccess';
}

intranet_closedb();
header("Location: category.php?ReturnMsgKey=$ReturnMsgKey");
?>