<?php
/*
 *  Using:
 *  
 *  2018-07-31 Cameron
 *      - also delete INTRANET_ENROL_CATEGORY_INSTRUCTOR
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$successAry = array();
$libenroll->Start_Trans();

for ($i = 0; $i < sizeof($CategoryID); $i++) {
	$successAry['delete'][] = $libenroll->DEL_CATEGORY($CategoryID[$i]);
	
	if ($sys_custom['project']['HKPF']) {
	    $categoryID = IntegerSafe($CategoryID[$i]);
	    $sql = "DELETE FROM INTRANET_ENROL_CATEGORY_INSTRUCTOR WHERE CategoryID='".$categoryID."'";
	    $successAry["DeleteCategoryInstructor_{$categoryID}"] = $libenroll->db_db_query($sql);
	}
}
$successAry['reorder'] = $libenroll->Update_Category_Order();

if (in_array(false, $successAry)) {
	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'DeleteUnsuccess'; 
}
else {
	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'DeleteSuccess';
}

intranet_closedb();
header("Location: category.php?ReturnMsgKey=$ReturnMsgKey");
?>