<?php
/*//using 
 * 	Log
 *  Date:   2019-09-25 [Cameron] set memory and execution time to unlimit for HKPF
 * 	Date:   2018-06-21 [Marco] add export
 *  Date:	2018-01-25 [Anna] add in used category cannot be deleted
 * 	Date:	2013-08-09 [Cameron] Add Column CategoryType in list view
 * 
 */

set_time_limit(0);
ini_set('memory_limit',-1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable.php");
//include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$CurrentPage = "PageCategorySetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$TAGS_OBJ[] = array($eEnrollmentMenu['cat_setting'], "", 1);
if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
	$TAGS_OBJ[] = array($Lang['eEnrolment']['GradingScheme'], "grading_scheme.php", 0);
}

#SQL Query for Display Result 
$sql = "SELECT
				CategoryID,
				CONCAT('<a href=\"category_new.php?CategoryID=', CategoryID ,'\" class=\"tablelink\">', CategoryName, '</a>') as CName,
				OleCategoryID,
				CategoryTypeID
		FROM
				INTRANET_ENROL_CATEGORY
		Order By
				DisplayOrder
		";
$categoryInfoAry = $libenroll->returnResultSet($sql);
$numOfCategory = count($categoryInfoAry);
$ReturnMsgKey = $_GET['ReturnMsgKey'];
$ReturnMsg = $Lang['General']['ReturnMessage'][$ReturnMsgKey];

$InUsedEventCategory = $libenroll->getEventInUsedCategory();
$InusedClubCategory = $libenroll->getClubInUsedCategory();

$oleCategoryAssoAry = $libenroll->Get_Ole_Category_Array($returnAsso=true);
$categoryTypeAssoAry = $libenroll->Get_CategoryType_Array($returnAsso=true);

$linterface->LAYOUT_START($ReturnMsg);
echo $linterface->Include_JS_CSS();	

## Edit/ Delete Buttons	
$ActionBtnArr = array();
$ActionBtnArr[] = array('edit', 'javascript:checkEdit(document.form1,\'CategoryID[]\',\'category_new.php\')');
$ActionBtnArr[] = array('delete', 'javascript:checkRemove2(document.form1,\'CategoryID[]\',\'goDeleteCategory();\')');

## Display Result
/////>>>>>
$widthCatTitle = 89;
$widthOleCategory = 40;
$widthCategoryType = 20;
if($plugin['iPortfolio']) {
	$widthCatTitle -= $widthOleCategory; 
}
if ($sys_custom['eEnrolment']['CategoryType']) {
	$widthCatTitle -= $widthCategoryType;
	$widthOleCategory -= $widthCategoryType;
}
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="get" action="category2.php">'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<div class="content_top_tool">'."\n";
						$x .= '<div class="Conntent_tool">'."\n";
							$x .= $linterface->Get_Content_Tool_v30("new","javascript:js_Go_New_Category();");
							$x .= $linterface->Get_Content_Tool_v30("export","javascript:goToExport();")."\n";
						$x .= '</div>'."\n";
					$x .= '</div>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= $linterface->Get_DBTable_Action_Button_IP25($ActionBtnArr);			
						$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$x .= '<th style="width:3%;">#</th>'."\n";
									$x .= '<th style="width:'.$widthCatTitle.'%;">'.$eEnrollmentMenu['cat_title'].'</th>'."\n";
									if($plugin['iPortfolio']) {
										$x .= '<th style="width:'.$widthOleCategory.'%;">'.$Lang['eEnrolment']['OleCategory'].'</th>'."\n";
									}
									if ($sys_custom['eEnrolment']['CategoryType']) {
										$x .= '<th style="width:'.$widthCategoryType.'%;">'.$Lang['eEnrolment']['CategoryType'].'</th>'."\n";
									}									
									$x .= '<th style="width:5%;">&nbsp;</th>'."\n";
									$x .= '<th style="width:3%;"><input type="checkbox" onclick="(this.checked)?setChecked(1,this.form,\'CategoryID[]\'):setChecked(0,this.form,\'CategoryID[]\')" name="checkmaster"></th>'."\n";
								$x .= '<tr>'."\n";
							$x .= '</thead>'."\n";
					
							$x .= '<tbody>'."\n";
								if ($numOfCategory == 0) {
									$x .= '<tr><td colspan="100%" style="text-align:center;">'.	$Lang['General']['NoRecordAtThisMoment'].'</td></tr>'."\n";
								}
								else {
									for ($i=0; $i<$numOfCategory; $i++) {
										$thisCategoryID  = $categoryInfoAry[$i]['CategoryID'];					
										$thisCategoryNameLink = $categoryInfoAry[$i]['CName'];
										$thisOleCategoryID = $categoryInfoAry[$i]['OleCategoryID'];
										$thisOleCategoryName = $oleCategoryAssoAry[$thisOleCategoryID];
										$thisOleCategoryName = ($thisOleCategoryName=='')? $Lang['General']['EmptySymbol'] : $thisOleCategoryName;
										$thisCategoryTypeID = $categoryInfoAry[$i]['CategoryTypeID'];
										$thisCategoryType = $categoryTypeAssoAry[$thisCategoryTypeID];

										$x .= '<tr id="tr_'.$thisCategoryID .'">'."\n";
											$x .= '<td><span class="rowNumSpan">'.($i + 1).'</td>'."\n";
											$x .= '<td>'.$thisCategoryNameLink.'</td>'."\n";
											if($plugin['iPortfolio']) {
												$x .= '<td>'.$thisOleCategoryName.'</td>'."\n";
											}
											if ($sys_custom['eEnrolment']['CategoryType']) {
												$x .= '<td>'.$thisCategoryType.'</td>'."\n";
											}
											$x .= '<td class="Dragable">'."\n";
												$x .= $linterface->GET_LNK_MOVE("#", $Lang['Btn']['Move'])."\n";
											$x .= '</td>'."\n";
											$x .= '<td>'."\n";
											if(!in_array($thisCategoryID,$InusedClubCategory) && !in_array($thisCategoryID,$InUsedEventCategory)){
												$x .= '<input type="checkbox" id="CategoryChk" class="CategoryChk" name="CategoryID[]" value="'.$thisCategoryID.'">'."\n";
											}else{
												$x .='';
											}
											$x .= '</td>'."\n";
									$x .= '</tr>'."\n";
									}					
								}
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";	
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
$x .= '</form>'."\n";
$x .= '<br />'."\n";

if ($msg == 1) $SysMsg = $linterface->GET_SYS_MSG("add");
if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
if ($msg == 3) $SysMsg = $lintCatagoryHiddenerface->GET_SYS_MSG("delete");
$htmlAry['CatagoryHidden'] = $linterface->GET_HIDDEN_INPUT('CategoryData', 'CategoryData', 'aaa');
?>

<script language="javascript">
$(document).ready( function() {	
	js_Init_DND_Table();
});

function js_Go_New_Category() {
	window.location = 'category_new.php';
}

function js_Init_DND_Table() {
	$(".common_table_list_v30").tableDnD({
		onDrop: function(table, DroppedRow) {
			if(table.id == "ContentTable") {
				Block_Element(table.id);
				var rows = table.tBodies[0].rows;
				var RecordOrder = "";
				for (var i=0; i<rows.length; i++) {
					if (rows[i].id != "")
					{
						var thisID = rows[i].id;
						var thisIDArr = thisID.split('_');
						var thisObjectID = thisIDArr[1];
						RecordOrder += thisObjectID + ",";
					}
				}

				// Update DB
				$.post(
					"ajax_update.php", 
					{ 
						Action: "Reorder_Category",
						DisplayOrderString: RecordOrder
					},
					function(ReturnData)
					{
						js_Reset_Display_Range();
						
						// Get system message
						if (ReturnData=='1') {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderSuccess']?>';			
						}
						else {
							jsReturnMsg = '<?=$Lang['eEnrolment']['Settings']['ReturnMsgArr']['CategoryReorderFailed']?>';
						}
						
						UnBlock_Element(table.id);
						Scroll_To_Top();
						Get_Return_Message(jsReturnMsg);
					}
				);
			}
		},
		onDragStart: function(table, DraggedRow) {
			//$('#debugArea').html("Started dragging row "+row.id);	
		},
		dragHandle: "Dragable", 
		onDragClass: "move_selected"
	});
}
	
//Delete Category	
function goDeleteCategory() {
	var jsSelectedCategoryIDArr = new Array();
	$('input.CategoryChk:checked').each( function() {
		jsSelectedCategoryIDArr[jsSelectedCategoryIDArr.length] = $(this).val();
	});
	var jsSelectedCategoryIDList = jsSelectedCategoryIDArr.join(',');

	$.post(
		"ajax_validate.php", 
		{ 
			Action: "Is_Category_Record",
			CategoryIDList: jsSelectedCategoryIDList	
		},
		
		function(ReturnData) {
			var jsCanSubmit = false;
			if (ReturnData == '1') {
				if (confirm('<?=$Lang['eEnrolment']['Settings']['WarningArr']['CategoryLinkedToDataAlready']?>')) {
					jsCanSubmit = true;
				}
				else {
					jsCanSubmit = false;
				}
			}
			else {
				jsCanSubmit = true;
			}
			if (jsCanSubmit) {
				$('form#form1').attr('action', 'category_remove.php').submit();
			}
		}
	);
}

//Reset the ranking display
function js_Reset_Display_Range(){
	var jsRowCounter = 0;
	$('span.rowNumSpan').each( function () {
		$(this).html(++jsRowCounter);
	});
}


function goToExport(){
	$('input#CategoryData').val();
	$('form#form2').attr('action','export.php').submit();
}


</script>
<form name="form2" id="form2" method="POST" >
<?php echo $htmlAry['CatagoryHidden'] ?>
</form>


<?php
echo $x;
?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>