<?php
/*
 *  Using:  
 *  
 * 	Log
 * 
 *  Date:   2019-09-13 [Cameron] set memory and execution time to unlimit for HKPF
 *  
 *  Date:   2018-07-31 [Cameron] Add which teacher can teach which category [HKPF] 
 * 
 * 	Date:	2013-08-09 [Cameron] Add field "Category Type" for SIS
 *  
 */

set_time_limit(0);
ini_set('memory_limit',-1);

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);	

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
	$libenroll = new libclubsenrol();	
	$libenroll_ui = new libclubsenrol_ui();
	
	if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
	$CurrentPage = "PageCategorySetting";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID'])) {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['mgt_cat'], "", 1);
        if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
        	$TAGS_OBJ[] = array($Lang['eEnrolment']['GradingScheme'], "grading_scheme.php", 0);
        }
        $linterface->LAYOUT_START();

		if (is_array($CategoryID)) {
			$CategoryID = $CategoryID[0];
		}
		        
		$CategoryArr = $libenroll->GET_CATEGORYINFO($CategoryID);
		$OleCategoryID = $CategoryArr['OleCategoryID']; 
		$CategoryTypeID = $CategoryArr['CategoryTypeID'];
		
		# page navigation (leave the array empty if no need)
		if ($CategoryID != "") {
			$PAGE_NAVIGATION[] = array($button_edit." ".$eEnrollment['add_activity']['act_category'], "");
			$button_title = $button_save;
		} else {
			$PAGE_NAVIGATION[] = array($button_new." ".$eEnrollment['add_activity']['act_category'], "");
			$button_title = $button_submit;
		}
		
		if($plugin['iPortfolio']) {
			$htmlAry['oleCategoryMappingRemarksTable'] = $linterface->Get_Warning_Message_Box($Lang['General']['Remark'], $Lang['eEnrolment']['OleCategoryRemarks']);
			
			$x = '';
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$Lang['eEnrolment']['OleCategory'].'</td>'."\r\n";
				$x .= '<td>'.$libenroll_ui->Get_Ole_Category_Selection('OleCategoryID_sel', 'OleCategoryID', $OleCategoryID).'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$htmlAry['oleCategoryMappingTr'] = $x;
		}
		
		if ($sys_custom['eEnrolment']['CategoryType']) {
			$x = '';
			$x .= '<tr>'."\r\n";
				$x .= '<td class="field_title">'.$Lang['eEnrolment']['CategoryType'].'</td>'."\r\n";
				$x .= '<td>'.$libenroll_ui->Get_Category_Type_Selection('CategoryTypeID_sel', 'CategoryTypeID', $CategoryTypeID).'</td>'."\r\n";
			$x .= '</tr>'."\r\n";
			$htmlAry['categoryTypeMappingTr'] = $x;		
		}			

		if ($sys_custom['project']['HKPF']) {
		    $instructorAry = $libenroll->getCategoryInstructor($CategoryID);
		    
		    $x = '';
		    $x .= '<tr>'."\r\n";
		    $x .= '<td class="field_title">'.$Lang['eEnrolment']['category']['qualifiedInstructor'].'</td>'."\r\n";
		    $x .= '<td>'.$libenroll_ui->getInstructorSelection($instructorAry, $row=0, $courseID=null, $tableClass='', $classID='', $height='200px').'</td>'."\r\n";
		    $x .= '</tr>'."\r\n";
		    $htmlAry['qualifiedInstructorMappingTr'] = $x;
		}
?>
<script language="javascript">
function FormSubmitCheck(obj)
{
	if(!check_text(obj.CategoryName, "<?php echo $i_alert_pleasefillin.$eEnrollmentMenu['cat_title']; ?>.")) return false;
	
<?php if ($sys_custom['project']['HKPF']): ?>
	$('#Instructor_0 option').attr('selected',true);
<?php endif;?>

<?
//if ($sys_custom['eEnrolment']['CategoryType']) {
//	print "if(!check_text(obj.CategoryTypeID, \"". $i_alert_pleasefillin.$Lang['eEnrolment']['CategoryType']. "\")) return false;\n";
//}
?>
	obj.submit();
}
</SCRIPT>
<form name="form1" action="category_update.php" method="POST" enctype="multipart/form-data">
<br/>

<table width="100%" border="0" cellspacing="4" cellpadding="4">
<tr><td>
	<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
</td></tr>
<tr><td>
<?=$htmlAry['oleCategoryMappingRemarksTable']?>
<table class="form_table_v30">
	<tr>
		<td class="field_title"><?=$linterface->RequiredSymbol()?><?= $eEnrollmentMenu['cat_title']?></td>
		<td><input type="text" id="CategoryName" name="CategoryName" value="<?= htmlspecialchars($CategoryArr[1])?>" class="textboxtext"></td>
	</tr>
	<?=$htmlAry['oleCategoryMappingTr']?>
	<?=$htmlAry['categoryTypeMappingTr']?>
	<?php echo $htmlAry['qualifiedInstructorMappingTr'];?>
</table>

</td></tr>

</table>
<table width="98%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark"><?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_title, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='category.php'")?>
</div>
</td></tr>
</table>
<br/>
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="CategoryID" id="CategoryID" value="<?= $CategoryID?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />

</form>
<?= $linterface->FOCUS_ON_LOAD("form1.CategoryName") ?>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>