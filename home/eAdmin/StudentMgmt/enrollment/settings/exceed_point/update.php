<?php
# using:
/*
 * 	
 * 	Date:	2017-09-07 (Anna)
 *          Created yhis page
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
$lgeneralsettings = new libgeneralsettings();
$libdb = new libdb();



$ExceedPoint= $_POST['ExceedPoint'];
$FormName= $_POST['FormName'];
$AcademicYearID =IntegerSafe(standardizeFormPostValue($_POST['AcademicYearID']));

$countOfLoop = count($FormName);

$UserID = $_SESSION['UserID'];


// IF not exist this EnrolGroupID, insert  
// if(empty($UnitRolePointResult)){
	for($i=0;$i<$countOfLoop;$i++){
	    
	    $_thisExceedPoint = $ExceedPoint[$i];
	    $_thisExceedPoint= ($_thisExceedPoint=='')? 'null' : $_thisExceedPoint;
	    
	    $_thisFormName = $FormName[$i];
	    $ExceedPointResult =  $libenroll->GetFormExceedPoint($AcademicYearID,$_thisFormName);
	    if($ExceedPointResult == ''){
	        $sql = "INSERT INTO INTRANET_ENROL_ROLE_EXCEED_POINT
	        (AcademicYearID ,FormName ,ExceedPoint ,DateInput ,InputBy ,DateModified , LastModifiedBy )
	        Values
	        ('$AcademicYearID','$_thisFormName','$_thisExceedPoint',now(),'$UserID',now(),'$UserID')";
	        $ReturnVal = $libdb->db_db_query($sql);
	       
	    }else{
	        $sql = "UPDATE 
                        INTRANET_ENROL_ROLE_EXCEED_POINT
        	        SET ExceedPoint  = '$_thisExceedPoint',
                        DateModified = NOW(),
                        LastModifiedBy = '$UserID'
                    WHERE  AcademicYearID = '$AcademicYearID' 
                    AND FormName = '$_thisFormName'";
	        $ReturnVal = $libdb->db_db_query($sql);	        
	    }	
	}
intranet_closedb();
header("Location: index.php?AcademicYearID=$AcademicYearID&xmsg=".$Lang['eNotice']['SettingsUpdateSuccess']);
?>