<?php
/************
 * 
 */


$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT.'includes/form_class_manage.php');

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$CurrentPage = "PageExceedPointSetting";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();


if (!isset($AcademicYearID) || $AcademicYearID=="")
    $AcademicYearID = Get_Current_Academic_Year_ID();

$Semester = isset($Semester) && $Semester != ''? $Semester : 0;

$AcademicYearID = IntegerSafe($AcademicYearID);
$Semester = IntegerSafe($Semester);

$AcademicYearID = standardizeFormGetValue($_GET['AcademicYearID']) != null?  standardizeFormGetValue($_GET['AcademicYearID']) : $AcademicYearID;
$Semester = standardizeFormGetValue($_GET['Semester']) != null?  standardizeFormGetValue($_GET['Semester']) : $Semester;


$TAGS_OBJ[] = array($Lang['eEnrolment']['MenuTitle']['Settings']['ExceedPointSetting'], "", 1);

$fcm = new form_class_manage();
// $FormList = $fcm->Get_Class_List_By_Academic_Year($AcademicYearID);

$FormList= $fcm->Get_Form_List('','',$AcademicYearID);
$FormCodeListAssocAry = BuildMultiKeyAssoc($FormList, 'WEBSAMSCode');
// $FormCodeListAry= Get_Array_By_Key($FormList, 'WEBSAMSCode');
$FormCodeListAry = array();
foreach($FormCodeListAssocAry as $FromCode=>$FormInfoAry){
    $FormCodeListAry[] = $FromCode;
}

# Acadermic Year Selection
$yearFilter = getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.action=\'index.php\';document.form1.submit();"', 1, 0, $AcademicYearID);


$thWidthArr = array(50,50); //define the percentages of width
///$tdHeading = array('#',$eEnrollment['role'],$Lang['eEnrolment']['Settings']['Role']['WebSAMSCode'],$Lang['eEnrolment']['ExceedPoint']['Point']);
$tdHeading = array($Lang['General']['Form'],$Lang['eEnrolment']['UnitRolePoint']['ExceedPoint']);


/////<<<<<
$x = '';
$x .= '<br />'."\n";
$x .= '<form id="form1" name="form1" method="post">'."\n";
	$x .= '<div class="table_board">'."\n";
		$x .= '<table id="html_body_frame" width="100%">'."\n";
			$x .= '<tr>'."\n";
			    $x .= '<td>'."\n";
        			$x .=$yearFilter;
			    $x .= '</td>'."\n";
			$x .= '</tr>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x.='<div class="table_row_tool row_content_tool">';
						$x.=$linterface->GET_SMALL_BTN($button_edit, "button","javascript:editForm();","EditBtn");
					$x.='</div>';
						$x .= '<table id="ContentTable" class="common_table_list_v30">'."\n";
							$x .= '<thead>'."\n";
								$x .= '<tr>'."\n";
									$countOfColumn = count($tdHeading);
									for($i=0;$i<$countOfColumn;$i++){
										$x .= '<th style="width:'.$thWidthArr[$i].'%;">'.$tdHeading[$i].'</th>'."\n";
									}
								$x .= '<tr>'."\n";
							$x .= '</thead>'."\n";		
							
							$x .= '<tbody>'."\n";

							$countOfForm = count($FormCodeListAry);
							for($i=0;$i<$countOfForm;$i++){
							    $_FormName= $FormCodeListAry[$i];	
							 
							    $_Point =  $libenroll->GetFormExceedPoint($AcademicYearID,$_FormName);
							      $x .= '<tr id="'.$_FormName.'">';										
							      $x .= '<td>'.$_FormName.'<input type="hidden" name="FormName[]" value="'.$_FormName.'"></td>';
																	
							      $x .= '<td class="ExceedPoint_text">'.$_Point.'</td>';
								  $x .= '<td class="ExceedPoint_editable" style="display:none;">';
								  $x .= $linterface->GET_TEXTBOX_NUMBER('ExceedPoint_'.$_FormName, 'ExceedPoint[]', $_Point, $OtherClass='', $OtherPar=array('maxlength'=>2));
								  $x .= '</td>';
								$x .= '</tr>';		
							}	
							$x .= '</tbody>'."\n";
						$x .= '</table>'."\n";	
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
	$x .= '</div>'."\n";
	$x .='<div class="edit_bottom_v30">';
	$x .='<p class="spacer"></p>';
		$x .= $linterface->GET_ACTION_BTN($button_update, "button","CheckUpdate();","UpdateBtn", "style='display: none'");
		$x .= $linterface->GET_ACTION_BTN($button_cancel, "button", "CancelEdit();","cancelbtn", "style='display: none'");
	$x .='<p class="spacer"></p>';
	$x .='</div>';
$x .= '</form>'."\n";
$x .= '<br />'."\n";


$returnMsg = $Lang['General']['ReturnMessage'][$xmsg];
$linterface->LAYOUT_START($returnMsg);
echo $linterface->Include_JS_CSS();	
?>

<script language="javascript">
function editForm(){
	$('#EditBtn').hide();
	$('#DeleteBtn').hide();
	$('#UpdateBtn').show();
	$('#cancelbtn').show();
	$('.ExceedPoint_text').hide();
	$('.ExceedPoint_editable').show();
	
	$('.num_check').hide();
	
}

function CheckUpdate(){
	var point = document.getElementsByName("ExceedPoint[]");
	var pointlength = point.length;

	var check;
	check = true;	
	for(var i=0;i<pointlength;i++){
	//	var inputvalue = parseFloat(point[i].value);
			var inputvalue =point[i].value;
		if(inputvalue != parseInt(inputvalue) && inputvalue !== ""){
			check = false;
		}
		if(inputvalue.length >= 3){
			check = false;
		}
		
	}
	
	if(check){
		$("#form1").attr("action", "update.php").submit();

	}else{
		alert("Please input right point(integer less than 100)");
	}
}
function CancelEdit(){
	document.form1.reset();	
	$('#EditBtn').show();
	$('#UpdateBtn').hide();
	$('#cancelbtn').hide();
	$('.ExceedPoint_text').show();
	$('.ExceedPoint_editable').hide();
	$('.num_check').show();
	$('#DeleteBtn').show();
	
}
</script>
<?=$x?>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>