<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageAdminSetting";

$linterface = new interface_html();
$LibUser = new libuser($UserID);

$PAGE_NAVIGATION[] = array($Lang['Btn']['Edit']);

# Left menu 
$TAGS_OBJ[] = array($Lang['eEnrolment']['AdminSettings']);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

$AdminLevel = $libenroll->GET_ENROL_ACCESS_LEVEL($AdminUserID[0]);

if ($AdminLevel == 1)
{
    $strFull = "CHECKED";
    $strNormal = "";
}
else
{
    $strFull = "";
    $strNormal = "CHECKED";
}

$numOfAdmin = count($AdminUserID);
$nameDisplayHTML = '';
for ($i=0; $i<$numOfAdmin; $i++)
{
	$thisLibUser = new libuser($AdminUserID[$i]);
	$nameDisplayHTML .= $thisLibUser->UserNameLang();
	
	if ( $i < ($numOfAdmin-1) )
		$nameDisplayHTML .= '<br />';
}

$AdminUserIDList = implode(', ', $AdminUserID);

?>

<br />   
<form name="form1" method="post" action="edit_update.php">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td><?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?></td>
	<td align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['StaffName']?></td>
			<td class='tabletext'><?=$nameDisplayHTML?></td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['AdminLevel']?></td>
			<td class='tabletext'>
			<input type="radio" name="AdminLevel" value="0" id="AdminLevel0" <?=$strNormal?>> <label for="AdminLevel0"><?=$Lang['eEnrolment']['UserRole']['NormalUser']?></label> <br>
			<input type="radio" name="AdminLevel" value="1" id="AdminLevel1" <?=$strFull?>> <label for="AdminLevel1"><?=$Lang['eEnrolment']['UserRole']['EnrolmentMaster']?></label>
			</td>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />

<input type="hidden" name="AdminUserIDList" value="<?=$AdminUserIDList?>">
</form>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>