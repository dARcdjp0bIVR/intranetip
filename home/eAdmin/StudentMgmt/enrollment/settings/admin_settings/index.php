<?php
# using: 

#################################
##	Date:	2015-07-21	Evan
##			Add search function
##
##	Date:	2011-02-16	YatWoon
##			update $linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);
##	
##	Date:	2011-02-15	YatWoon
##			hidden "Admin Level" column
##
#################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

if ($page_size_change == 1)
{
    setcookie("ck_page_size", $numPerPage, 0, "", "", 0);
    $ck_page_size = $numPerPage;
}

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


$linterface 	= new interface_html();
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageAdminSetting";
$LibUser = new libuser($UserID);

// if($UserLevel > -1)
// 	$cond = " and a.UserLevel = $UserLevel";
$name_field = getNameFieldByLang("b.");
/*
$sql = "
	SELECT 
		$name_field as NameField,
		CASE a.UserLevel
			WHEN 0 THEN '".$Lang['eEnrolment']['UserRole']['NormalUser']."'
			WHEN 1 THEN '".$Lang['eEnrolment']['UserRole']['EnrolmentMaster']."'
			ELSE ''
		END as AdminLevel,
		CONCAT('<input type=\"checkbox\" name=\"AdminUserID[]\" value=\"', b.UserID ,'\">')
	FROM 
		INTRANET_ENROL_USER_ACL as a
		INNER JOIN INTRANET_USER as b ON a.UserID = b.UserID
	WHERE 
		a.UserLevel = 1
		$cond
";
*/

$sql = "
	SELECT 
		$name_field as NameField,
		CONCAT('<input type=\"checkbox\" name=\"AdminUserID[]\" value=\"', b.UserID ,'\">')
	FROM 
		INTRANET_ENROL_USER_ACL as a
		INNER JOIN INTRANET_USER as b ON a.UserID = b.UserID
	WHERE 
		a.UserLevel = 1
";

if (isset($_POST['keyword'])&& $_POST['keyword']!= ""){
	$sql .= "AND (EnglishName LIKE '%".$keyword."%' OR ChineseName LIKE '%".$keyword."%')";
}


# TABLE INFO
if (isset($ck_page_size) && $ck_page_size != "") $page_size = $ck_page_size;
$pageSizeChangeEnabled = true;

if($field=="") $field = 0;
$li = new libdbtable2007($field, 1, $pageNo);
$li->field_array = array("NameField");
$li->sql = $sql;
$li->no_col = 2;
$li->IsColOff = "GeneralDisplayWithNavigation";

// TABLE COLUMN
$li->column_list .= "<th width='100%'>". $Lang['eEnrolment']['StaffName'] ."</th>\n";
// $li->column_list .= "<th width='20%'>". $Lang['eEnrolment']['AdminLevel'] ."</th>\n";
$li->column_list .= "<th>".$li->check("AdminUserID[]")."</th>\n";

### Button
$AddBtn = $linterface->GET_LNK_ADD("add.php","","","","",0);
//$editBtn = $linterface->GET_LNK_EDIT("javascript:checkEditMultiple(document.form1,'AdminUserID[]','edit.php')","","","","",0);
$delBtn = $linterface->GET_LNK_REMOVE("javascript:checkRemove(document.form1,'AdminUserID[]','remove.php')");

###Search bar
$searchTag = $linterface->Get_Search_Box_Div('keyword', stripslashes($keyword));

### Filter
// $AdminList = array();
// $AdminList[] = array("-1", $Lang['Btn']['All']);
// $AdminList[] = array("1", $Lang['eEnrolment']['UserRole']['EnrolmentMaster']);
// $AdminList[] = array("0", $Lang['eEnrolment']['UserRole']['NormalUser']);
//$UserLevelSelection = $Lang['eEnrolment']['AdminLevel']." : ".getSelectByArray($AdminList,"name='UserLevel' onChange='this.form.submit()'",$UserLevel,0,1);

### Title ###
$TAGS_OBJ[] = array($Lang['eEnrolment']['AdminSettings']);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START($Lang['General']['ReturnMessage'][$msg]);
?>
<form name="form1" method="post">
<div class="content_top_tool">
       <?=$AddBtn?>
       <div align="right"><?=$searchTag?></div>
<br style="clear:both" />

<div class="table_board">


<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td valign="bottom">
		<div class="table_filter"><?=$UserLevelSelection?></div>
	</td>
	<td valign="bottom">
		<div class="common_table_tool"><?=$editBtn?><?=$delBtn?></div>
	</td>
</tr>
</table>
        	
<?php
	echo $li->display($li->no_col);
?>
	
<input type="hidden" name="pageNo" value="<?php echo $li->pageNo; ?>" />
<input type="hidden" name="field" value="<?php echo $li->field; ?>" />
<input type="hidden" name="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" value="<?=$li->page_size?>" />


</div>
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>