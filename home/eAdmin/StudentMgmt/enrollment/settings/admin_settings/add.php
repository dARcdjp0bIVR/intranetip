<?php
# using: 
############################
#	Date:	2015-07-21	Evan
#			Modified path of select button, add checkAdmin = 1
#
#	Date:	2015-06-02	Evan
#			UI update
#
#	Date:	2015-06-02	Evan
#			Update the interface according to UI standard
#
############################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$linterface = new interface_html();
$CurrentPageArr['eEnrolment'] = 1;
$CurrentPage = "PageAdminSetting";
$LibUser = new libuser($UserID);

$PAGE_NAVIGATION[] = array($Lang['Btn']['Add']);

# Left menu 
$TAGS_OBJ[] = array($Lang['eEnrolment']['AdminSettings']);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Start layout
$linterface->LAYOUT_START();

# 
//for($i = 0; $i < 40; $i++) $space .= "&nbsp;";
$StaffSelection .= "<td width=\"1\"><select name=target[] size=4 multiple></select></td><td>";
//$StaffSelection .= $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button","newWindow('../../choose/index_all.php?fieldname=target[]&ppl_type=pic', 9)")."<br>";
$StaffSelection .= $linterface->GET_SMALL_BTN($Lang['Btn']['Select'], "button","newWindow('/home/common_choose/enrolment_pic_helper.php?fieldname=target[]&ppl_type=pic&permitted_type=1&checkAdmin=1', 9)")."<br>";


$StaffSelection .= $linterface->GET_SMALL_BTN($Lang['Btn']['RemoveSelected'], "button","checkOptionRemove(document.form1.elements['target[]'])");
$StaffSelection .= "</td>";
?>

<script language="javascript">
<!--
function checkform()
{
	obj = document.form1;
	
	if(obj.elements["target[]"].length==0)
	{
		alert("<?php echo $Lang['Circular']['PleaseSelectStaff']; ?>"); 
		return false; 
	}
	checkOptionAll(obj.elements["target[]"]);
}
//-->
</script>

<br />   
<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
<div align='right'><?=$linterface->GET_SYS_MSG($xmsg);?></div>


<form name="form1" method="get" action="add_update.php" onSubmit="return checkform();">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">
<tr>
	<td colspan="2">
		<blockquote>
		<table class="form_table_v30">
		<tr valign='top'>
			<td class="field_title"><?=$linterface->RequiredSymbol().$Lang['Circular']['StaffName']?></td>
			<?=$StaffSelection?>
		</tr>
		</table>
		</blockquote>
	</td>
</tr>

<? /* ?>
<tr>
	<td colspan="2">
		<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
		<tr valign='top'>
			<td width='30%' valign="top" class="formfieldtitle" nowrap><?=$Lang['Circular']['AdminLevel']?></td>
			<td class='tabletext'>
			<input type="radio" name="AdminLevel" value="1" id="AdminLevel1" checked> <label for="AdminLevel1"><?=$Lang['eEnrolment']['UserRole']['EnrolmentMaster']?></label>
			<!--
			<input type="radio" name="AdminLevel" value="0" id="AdminLevel0"> <label for="AdminLevel0"><?=$Lang['eEnrolment']['UserRole']['NormalUser']?></label> <br>
			-->
			
			</td>
		</tr>
		</table>
	</td>
</tr>
<? */ ?>

<tr>
	<td colspan="2">        
        <table width="95%" border="0" cellspacing="0" cellpadding="5" align="center">
        <tr>
        	<td class="dotline"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
        </tr>
        <tr>
			<td align="center">
				<?= $linterface->GET_ACTION_BTN($button_submit, "submit") ?>
				<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "history.back()","cancelbtn") ?>
			</td>
		</tr>
        </table>                                
	</td>
</tr>

</table>                        
<br />
<input type="hidden" name="flag" value="">
</form>
</blockquote>

<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>