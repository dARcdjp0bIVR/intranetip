<?php

# using: yat

######################
#
#	Date:	2011-02-16	YatWoon
#			- update $msg value
#
######################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

if(!empty($AdminUserID))
{
	if (is_array($AdminUserID))
		$AdminIDStr = implode(", ", $AdminUserID);
	else
		$AdminIDStr = $AdminUserID;
		
	$success = $libenroll->Remove_Admin_User($AdminIDStr);
	
	if ($success)
		$msg = "DeleteSuccess";
	else
		$msg = "DeleteUnsuccess";
}
else
	$msg = "DeleteUnsuccess";

intranet_closedb();
header("Location: index.php?msg=$msg");
?>