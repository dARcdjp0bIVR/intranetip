<?php 
# using: yat

##################################
#
#	Date:	2012-03-16	YatWoon
#			update incorrect header title (Change from "Reminder" to "System Properties"
#
##################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lgeneralsettings = new libgeneralsettings();
$linterface = new interface_html();

if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageSysSettingBasicSettings";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
$settingAry = array("'AllowToEditPreviousYearData'");
$settingData = $lgeneralsettings->Get_General_Setting($libenroll->ModuleTitle, $settingAry);	


$TAGS_OBJ[] = array($Lang['General']['SystemProperties']);
$xmsg = $Lang['eNotice'][urldecode($xmsg)];
$linterface->LAYOUT_START($xmsg);



?>

<script language="javascript">
<!--
function EditInfo()
{
	$('#EditBtn').attr('style', 'visibility: hidden'); 
	$('.Edit_Hide').attr('style', 'display: none');
	$('.Edit_Show').attr('style', '');
	
	$('#UpdateBtn').attr('style', '');
	$('#cancelbtn').attr('style', '');
}		

function ResetInfo()
{
	document.form1.reset();
	$('#EditBtn').attr('style', '');
	$('.Edit_Hide').attr('style', '');
	$('.Edit_Show').attr('style', 'display: none');
	
	$('#UpdateBtn').attr('style', 'display: none');
	$('#cancelbtn').attr('style', 'display: none');
}

function FCKeditor_OnComplete(EDITOR){
	EDITOR.SetHTML($('span#Content').html());
}
//-->
</script>
		
<form name="form1" method="post" action="edit_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","javascript:EditInfo();","EditBtn")?>
	</div>

	<table class="form_table_v30">
	
			<!-- Enable Popup Notification //-->
			<tr valign='top'>
				<td class="field_title" nowrap><?=$Lang['eEnrolment']['AllowToManageRecordOfPreviousYear']?></td>
				<td> 
				<span class="Edit_Hide"><?=$settingData['AllowToEditPreviousYearData'] ? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
				<input type="radio" name="AllowToEditPreviousYearData" value="1" <?=$settingData['AllowToEditPreviousYearData'] ? "checked":"" ?> id="AllowToEditPreviousYearData1"> <label for="AllowToEditPreviousYearData1"><?=$i_general_yes?></label> 
				<input type="radio" name="AllowToEditPreviousYearData" value="0" <?=$settingData['AllowToEditPreviousYearData'] ? "":"checked" ?> id="AllowToEditPreviousYearData0"> <label for="AllowToEditPreviousYearData0"><?=$i_general_no?></label>
				</span></td>
			</tr>
			
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "ResetInfo();","cancelbtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>




<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>
