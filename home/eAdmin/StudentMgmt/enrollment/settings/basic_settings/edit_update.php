<?php
# using: 

################# Change Log [Start] ############
# 
#
################# Change Log [End] ############

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
$lgeneralsettings = new libgeneralsettings();

$data = array();
$data['AllowToEditPreviousYearData'] = $AllowToEditPreviousYearData;


# store in DB
$lgeneralsettings->Save_General_Setting($libenroll->ModuleTitle, $data);

$xmsg = 'SettingsUpdateSuccess';

intranet_closedb();
header("Location: index.php?xmsg=".urlencode($xmsg));

?>