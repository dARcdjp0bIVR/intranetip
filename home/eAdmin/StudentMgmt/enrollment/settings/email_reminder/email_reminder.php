<?php
// Editing by 
/*
 * 2014-04-14 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$sys_custom['eEnrolment']['TWGHCYMA'] || !$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageSysSettingEmailReminder";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
$linterface = new interface_html();
$TAGS_OBJ[] = array($Lang['eEnrolment']['EmailReminder']);
$linterface->LAYOUT_START($Msg);

$GeneralSetting = new libgeneralsettings();
$Settings = array();

$SettingList[] = "'UseEmailReminder'";
$SettingList[] = "'EmailReminderSubject'";
$SettingList[] = "'EmailReminderContent'";
$SettingList[] = "'EmailReminderDaysBefore'";
$SettingList[] = "'EmailReminderSendTime'";

$Settings = $GeneralSetting->Get_General_Setting($libenroll->ModuleTitle,$SettingList);
if($Settings['EmailReminderDaysBefore'] == ''){
	$Settings['EmailReminderDaysBefore'] = 0;
}
if($Settings['EmailReminderSendTime'] == ''){
	$Settings['EmailReminderSendTime'] = '00:00:00';
}

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod"){
	$EmailReminderContentHtml = '<textarea name="EmailReminderContent" id="EmailReminderContent" cols="75%" rows="20">'.$Settings['EmailReminderContent'].'</textarea>';
}else{
	$EmailReminderContentEditor = new FCKeditor ( 'EmailReminderContent' , "100%", "320");
	$EmailReminderContentEditor->Value = $Settings['EmailReminderContent'];
	$EmailReminderContentHtml = $EmailReminderContentEditor->Create2();
}

?>
<script language="javascript">
<!--
function EditInfo(edit)
{
	if(edit == 1){
		$('.Edit_Hide').hide();
		$('.Edit_Show').show();
		$('#UpdateBtn').show();
		$('#CancelBtn').show();
		$('#EditBtn').hide();
	}else{
		$('.Edit_Hide').show();
		$('.Edit_Show').hide();
		$('#UpdateBtn').hide();
		$('#CancelBtn').hide();
		$('#EditBtn').show();
	}
}

function FCKeditor_OnComplete(EDITOR){
	EDITOR.SetHTML($('#Content').html());
}

function onChangeDaysBefore(obj)
{
	var str_val = $.trim(obj.value);
	var num_val = parseInt(str_val);
	
	if(str_val == '' || (str_val != '' && isNaN(num_val)) || (str_val!='' && num_val < 0)){
		obj.value = 0;
	}
}
//-->
</script>

<form name="form1" method="post" action="email_reminder_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","EditInfo(1);","EditBtn")?>
	</div>

	<table class="form_table_v30">
	
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['UseEmailReminder']?></td>
			<td> 
				<span class="Edit_Hide"><?=$Settings['UseEmailReminder']==1? $i_general_yes:$i_general_no?></span>
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="UseEmailReminder" value="1" <?=$Settings['UseEmailReminder']==1? "checked":"" ?> id="UseEmailReminder1"> <label for="UseEmailReminder1"><?=$i_general_yes?></label> 
					<input type="radio" name="UseEmailReminder" value="0" <?=$Settings['UseEmailReminder']!=1 ? "checked":"" ?> id="UseEmailReminder0"> <label for="UseEmailReminder0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['EmailSubject']?></td>
			<td> 
				<span class="Edit_Hide"><?=$Settings['EmailReminderSubject']?></span>
				<span class="Edit_Show" style="display:none">
					<input id="EmailReminderSubject" type="text" value="<?=$Settings['EmailReminderSubject']?>" name="EmailReminderSubject" size="100" maxlength="256" /><br>
					<span class="tabletextremark">(<?=$Lang['eEnrolment']['EmailReminderRemark']?>)</span>
				</span>
			</td>
		</tr>
		
		<tr valign='top' class="Edit_Hide">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['Content']?></td>
			<td id="Content">
				<?=$Settings['EmailReminderContent']?>
			</td>
		</tr>
		
		<tr valign='top' class="Edit_Show" style="display:none">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['Content']?></td>
			<td>
				<?=$EmailReminderContentHtml?><br>
				<span class="tabletextremark">(<?=$Lang['eEnrolment']['EmailReminderRemark']?>)</span>
			</td>
		</tr>
		
		<tr valign='top'>
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['RemindTime']?></td>
			<td>
				<span class="Edit_Hide" id="Content"><?=$Settings['EmailReminderDaysBefore']?></span>
				<span class="Edit_Show" style="display:none">
					<?=$linterface->GET_TEXTBOX_NUMBER("EmailReminderDaysBefore", "EmailReminderDaysBefore", $Settings['EmailReminderDaysBefore'], '', array('onchange'=>'onChangeDaysBefore(this);'))?>
				</span>
				<?=$Lang['eEnrolment']['DaysBefore'].'&nbsp;'.$Lang['eEnrolment']['At'].'&nbsp;'?>
				<span class="Edit_Hide"><?=$Settings['EmailReminderSendTime']?></span>
				<span class="Edit_Show" style="display:none"><?=$linterface->Get_Time_Selection('EmailReminderSendTime',$Settings['EmailReminderSendTime'])?></span>
			</td>
		</tr>
		
	</table>                        
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "EditInfo(0);","CancelBtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>