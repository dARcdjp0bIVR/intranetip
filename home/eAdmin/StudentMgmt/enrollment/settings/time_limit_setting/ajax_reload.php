<?php
// using : 
/*
 *  Note: this page is for adapting $PATH_WRT_ROOT used in date picker 
 *  
 *  2018-08-01 Cameron
 *      copy from enrolment root directory
 * 
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_opendb();

$libenroll = new libclubsenrol($AcademicYearID);
$libenroll_ui = new libclubsenrol_ui();

# Get data
$Action = stripslashes($_REQUEST['Action']);

$returnString = '';
if ($Action == 'getMeetingDate_periodicDateSelectionTable') {
	echo $libenroll_ui->Get_Meeting_Date_Periodic_Date_Selection_Table();
}

intranet_closedb();

?>

