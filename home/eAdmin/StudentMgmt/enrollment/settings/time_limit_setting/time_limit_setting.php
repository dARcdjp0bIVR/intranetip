<?php 
/*
 *  2018-08-21 Cameron
 *      - fix: instructor that contains &
 *      
 *  2018-08-01 Cameron
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/liburlparahandler.php");

intranet_auth();
intranet_opendb();


$LibUser = new libuser($_SESSION['UserID']);
$linterface = new interface_html();
$libenroll = new libclubsenrol();

####################################################
########## Access Right Checking [Start] ###########
####################################################
$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
$canAccess =  $isEnrolAdmin ? true : false;

if (!$canAccess) {
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}
####################################################
########### Access Right Checking [End] ############
####################################################



####################################################
############# Get General Data [Start] #############
####################################################
$AcademicYearID = Get_Current_Academic_Year_ID();
$AcademicStart = date("Y-m-d", getStartOfAcademicYear('', $AcademicYearID));
$AcademicEnd   = date("Y-m-d", getEndOfAcademicYear('', $AcademicYearID));
####################################################
############## Get General Data [End] ##############
####################################################

$staffID = IntegerSafe($_REQUEST['StaffID']);
$staffAry = $libenroll->getStaffList(null, $staffID);
if (count($staffAry)) {
    $staffName = $staffAry[0]['Name'];
}
else {
    $staffName = '';
}

####################################################
############## Layout Display [Start] ##############
####################################################
$CurrentPage = "PageTimeLimitSetting";
$CurrentPageArr['eEnrolment'] = 1;

### top tab
$TAGS_OBJ[] = array($Lang['eEnrolment']['settings']['timeLimitSetting']['title'], "", 1);

### left-hand menu
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


$h_schedulingDatesNavigation = $linterface->GET_NAVIGATION2($Lang['eEnrolment']['settings']['timeLimitSetting']['instructorCannotTeachDate']);
$h_schedulingTimesNavigation = $linterface->GET_NAVIGATION2($Lang['eEnrolment']['settings']['timeLimitSetting']['instructorCannotTeachTime']);


### Instruction
$h_instruction = '';
if ($libenroll->AllowToEditPreviousYearData) {
    $h_instruction .= $linterface->Get_Warning_Message_Box($Lang['General']['Instruction'], $Lang['eEnrolment']['settings']['timeLimitSetting']['instruction']);
}

### Add Meeting Date Type
$h_dateTypeSelection = '';
$h_dateTypeSelection .= $linterface->Get_Radio_Button("dateType_".$enrolConfigAry['MeetingDateType']['Single'], "dateType", $enrolConfigAry['MeetingDateType']['Single'], true, $Class="dateType", $Lang['eEnrolment']['DateTypeArr']['Single'], "changedDateType()");
$h_dateTypeSelection .= '&nbsp;';
$h_dateTypeSelection .= $linterface->Get_Radio_Button("dateType_".$enrolConfigAry['MeetingDateType']['Periodic'], "dateType", $enrolConfigAry['MeetingDateType']['Periodic'], false, $Class="dateType", $Lang['eEnrolment']['DateTypeArr']['Periodic'], "changedDateType()");

### Time Selection for "Add by Single Date"
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalHourStart', 'hour', 0);
$h_singleDate_globalTimeSeletion .= ' : '; 
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalMinStart', 'min', 0, $others_tab='', $interval=5);
$h_singleDate_globalTimeSeletion .= ' '.$eEnrollment['to'].' ';
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalHourEnd', 'hour', 0);
$h_singleDate_globalTimeSeletion .= ' : ';
$h_singleDate_globalTimeSeletion .= $linterface->Get_Time_Selection_Box('singleDate_globalMinEnd', 'min', 0, $others_tab='', $interval=5);
$h_singleDate_globalTimeSeletion .= $linterface->Get_Form_Warning_Msg('singleDate_global_invalidTimeRangeWarningDiv', $Lang['eEnrolment']['Warning']['TimeRangeInvalid'], 'warningDiv');

### Date Time Action Button
$h_actionBtn = '';
if ($libenroll->AllowToEditPreviousYearData) {
	$BtnArr = array();
	$BtnArr[] = array('delete', 'javascript:deleteDate();');
	$h_actionBtn = $linterface->Get_DBTable_Action_Button_IP25($BtnArr);
}

### Submit & Cancel Button
if ($libenroll->AllowToEditPreviousYearData) {
	$h_submitBtn = $linterface->GET_ACTION_BTN($button_save, "button", ($disableUpdateSettings)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:checkForm();");
}
$h_cancelBtn = $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='index.php'");


### School & Public Holiday
$StartDate = getStartDateOfAcademicYear($AcademicYearID);
$EndDate = getEndDateOfAcademicYear($AcademicYearID);
$sql = "SELECT 
			DATE_FORMAT(EventDate,'%Y-%m-%d')
		FROM 
	        INTRANET_EVENT
		where 
			EventDate BETWEEN '$StartDate' AND '$EndDate' and RecordType IN (3,4)";
$holidayArr = $libenroll->returnVector($sql);

//echo $linterface->Include_Thickbox_JS_CSS();

####################################################
############### Layout Display [End] ###############
####################################################
?>
<script language="javascript">
var jsHolidayArr = new Array('<?=implode("','",$holidayArr)?>');

var date = new Date();
var curYear;
var curMonth;

$(document).ready( function() {
	changedDateType();
	reloadDateTimeTable();
});

// function PasteToAllLocation(){
// 	var value = $('#globalLocation').val();
// 	var numOfRow = getMaxRowNum()+1;
// 	for (var i=1; i<numOfRow ; i++){
// 		$('#Location_'+i).val(value);
// 	}
// }

function getRecordType() {
	return $('input#recordType').val();
}

function getRecordId() {
	return $('input#recordId').val();
}

function getMaxRowNum() {
	var maxRowNum = 0;
	$('tr.dateTimeTr').each( function() {
		var _rowNum = parseInt($(this).attr('id').replace('dateTimeTr_', ''));
		if (_rowNum > maxRowNum) {
			maxRowNum = _rowNum;
		}
	});
	
	return maxRowNum;
}

function getStartDateWanringMsg() {
	return '<?=str_replace('<!--targetDate-->', $AcademicStart, $Lang['eEnrolment']['Warning']['StartDateInvalid'])?>';
}

function getEndDateWanringMsg() {
	return '<?=str_replace('<!--targetDate-->', $AcademicEnd, $Lang['eEnrolment']['Warning']['EndDateInvalid'])?>';
}

function resetCurYearMonth() {
	curYear = date.getFullYear();
	curMonth = date.getMonth() + 1;
}

function changedDateType() {
	resetCurYearMonth();
	
	if ($('input#dateType_' + '<?=$enrolConfigAry['MeetingDateType']['Single']?>').attr('checked')) {
		$('div#singleDateGlobalTimeSelDiv').show();
		reloadDateTable();
	}
	else if ($('input#dateType_' + '<?=$enrolConfigAry['MeetingDateType']['Periodic']?>').attr('checked')) {
		$('div#singleDateGlobalTimeSelDiv').hide();
		reloadPeriodicDateSelectionTable();
	}
}

function changeMonth(prevnext) {
	var newMonth = parseInt(curMonth) + prevnext;
	
	if(newMonth==13) {
		curMonth = 1;
		curYear += 1;
	}
	else if(newMonth==0) {
		curMonth = 12;
		curYear -= 1;
	}
	else {
		curMonth = newMonth;
	}
	reloadDateTable();
}

function addSingleDate(dateText) {
	var canAddDate = true;
	if (!compareTimeByObjId('', 'singleDate_globalHourStart', 'singleDate_globalMinStart', '', '', 'singleDate_globalHourEnd', 'singleDate_globalMinEnd', '')) {
		$('div#singleDate_global_invalidTimeRangeWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#singleDate_global_invalidTimeRangeWarningDiv').hide();
	}
	
	if (canAddDate) {
		var defaultStartHour = $('select#singleDate_globalHourStart').val();
		var defaultStartMin = $('select#singleDate_globalMinStart').val();
		var defaultEndHour = $('select#singleDate_globalHourEnd').val();
		var defaultEndMin = $('select#singleDate_globalMinEnd').val();
		
		addDateRowToTable(dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
	}
}

function addPeriodicDate() {
	var startDate = $('input#StartDate').val();
	var endDate = $('input#EndDate').val();
	var canAddDate = true;
	
	// check if the date range is valid
	if (startDate=='' || endDate=='' || startDate > endDate) {
		$('div#invalidDateRangeWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#invalidDateRangeWarningDiv').hide();
	}
	
	// check if the date is within academic year
	if (startDate < '<?=$AcademicStart?>') {
		$('div#invalidStartDateWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#invalidStartDateWarningDiv').hide();
	}
	if (endDate > '<?=$AcademicEnd?>') {
		$('div#invalidEndDateWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#invalidEndDateWarningDiv').hide();
	}
	
	// check if selected any weekday
	if($('#rcChoice_p').attr('checked')){
		if ($('input.weekdayChk:checked').length==0) {
			$('div#selectWeekdayWarningDiv').show();
			canAddDate = false;
		}
		else {
			$('div#selectWeekdayWarningDiv').hide();
		}
	} else {
		var selected = 0;
		$('input[name="cycleday_text[]"]').each(function(){
			if($(this).attr('checked')) selected++;
		});
		if(selected==0) canAddDate = false;
	}
	
	if (!compareTimeByObjId('', 'globalPeriodicHourStart', 'globalPeriodicMinStart', '', '', 'globalPeriodicHourEnd', 'globalPeriodicMinEnd', '')) {
		$('div#periodicDate_global_invalidTimeRangeWarningDiv').show();
		canAddDate = false;
	}
	else {
		$('div#periodicDate_global_invalidTimeRangeWarningDiv').hide();
	}
	
	if (canAddDate) {
		var defaultStartHour = $('select#globalPeriodicHourStart').val();
		var defaultStartMin = $('select#globalPeriodicMinStart').val();
		var defaultEndHour = $('select#globalPeriodicHourEnd').val();
		var defaultEndMin = $('select#globalPeriodicMinEnd').val();
		
		var startDatePiece = startDate.split('-');
		var startYear = startDatePiece[0];
		var startMonth = startDatePiece[1] - 1;
		var startDay = startDatePiece[2];
		
		// Get the date which fulfill the requirements
		var dateObj = new Date(startYear, startMonth, startDay);
		var currentDate = getDateTextByDateObject(dateObj);
		var isExcludeHoiday = $('#excludeHoliday').attr('checked');
		var targetDateAry = new Array();

		if($('#rcChoice_p').attr('checked')){
    		while (currentDate <= endDate) {
    			var _day = dateObj.getDay();
    // 			console.log(_day);
    			if ($('input#WeekDay' + _day).attr('checked')) {
    				var thisdate = getDateTextByDateObject(dateObj);
    				if(isExcludeHoiday ){
    					if($.inArray(thisdate,jsHolidayArr)>-1){
    						// do nth
    					}
    					else{
    						targetDateAry.push(thisdate);	
    					}
    				}
    				else{
    					targetDateAry.push(thisdate);
    				}
    			}
    			
    			dateObj.setDate(dateObj.getDate() + 1);
    			currentDate = getDateTextByDateObject(dateObj);
    		}
    		var numOfDate = targetDateAry.length;
    		var i;
    		for (i=0; i<numOfDate; i++) {
    			var _dateText = targetDateAry[i];
    			addDateRowToTable(_dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
    		}
		} else {
			var targetCycleDay = "";
			$('input[name="cycleday_text[]"]').each(function(){
				if($(this).attr('checked')){
					targetCycleDay += $(this).val() + ",";
				}
			});
			$.ajax({
				method: 'POST',
				url: '../../ajax_retriveCycleDayTextByDateRange.php',
				data: {
					action: "dummy",
					StartDate: $('#StartDate').val(),
					EndDate: $('#EndDate').val(),
					targetCycleDays: targetCycleDay
				},
				success: function(responseText){
					//console.log(responseText);
					var spAry = responseText.split(',');
					//console.log(spAry);
					for(var x in spAry){
						if(x!='in_array'){
							var _dateText = spAry[x];
							if(isExcludeHoiday){
		    					if($.inArray(_dateText,jsHolidayArr)>-1){
		    						// do nth
		    					}
		    					else{
		    						addDateRowToTable(_dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
		    					}
		    				} else {
		    					addDateRowToTable(_dateText, defaultStartHour, defaultStartMin, defaultEndHour, defaultEndMin);
		    				}
						}
					}
				}
			});
		}
	}
}

function addDateRowToTable(dateText, startHour, startMin, endHour, endMin) {
	// find the corresponding row to be inserted
	var insertRowNum = 0;
	$('tr.dateTimeTr').each( function() {
		var _rowNum = parseInt($(this).attr('id').replace('dateTimeTr_', ''));
		var _dateText = $('input#dateTextHidden_' + _rowNum).val();
		
		if (insertRowNum == 0 && dateText < _dateText) { 
			insertRowNum = _rowNum;
			return false;	// break
		}
	});
	// if not inserted in the middle of the table => insert at the bottom of the table 
	var maxRowNum = getMaxRowNum();
	if (insertRowNum == 0) {
		insertRowNum = maxRowNum + 1;
	}
	
	// shift the rowNum of the affected row first
	var i;
	for (i=maxRowNum; i>=insertRowNum; i--) {
		var _oldRowNum = i;
		var _newRowNum = i + 1;
		
		convertRowNumOfTr(_oldRowNum, _newRowNum);
	}
	
	// insert the new row to the table
	var trTemplate = htmlspecialchars_decode($('textarea#dateTimeTrTemplate').html());
	trTemplate = trTemplate.replace(/<?=$enrolConfigAry['MeetingDateNewRowNumTempCode']?>/g, insertRowNum).replace(/<?=$enrolConfigAry['MeetingDateNewRowNumTempDateStart']?>/g, dateText);
	
	if (insertRowNum == 1) {
		$('table#dateTimeSettingTable > tbody').prepend(trTemplate);
	}
	else {
		var targetRowNum = insertRowNum - 1;
		$('tr#dateTimeTr_' + targetRowNum).after(trTemplate);
	}
	
	// preset the value of the inserted row
	$('select#hourStart_' + insertRowNum).val(startHour);
	$('select#minStart_' + insertRowNum).val(startMin);
	$('select#hourEnd_' + insertRowNum).val(endHour);
	$('select#minEnd_' + insertRowNum).val(endMin);
	
	// hide the no data display row
	$('tr#noRecordTr').hide();
}

function getDateTextByDateObject(dateObj) {
	return dateObj.getFullYear() + '-' + str_pad((dateObj.getMonth() + 1), 2) + '-' + str_pad(dateObj.getDate(), 2);;
}

function convertRowNumOfTr(fromRowNum, toRowNum) {
	$('input#dateRecordId_' + fromRowNum).attr('id', 'dateRecordId_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][dateRecordId]');
	$('tr#dateTimeTr_' + fromRowNum).attr('id', 'dateTimeTr_' + toRowNum);
	$('div#rowNumDiv_' + fromRowNum).attr('id', 'rowNumDiv_' + toRowNum).html(toRowNum);
	$('input#dateTextHidden_' + fromRowNum).attr('id', 'dateTextHidden_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][dateText]');
	$('select#hourStart_' + fromRowNum).attr('id', 'hourStart_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][hourStart]');
	$('select#minStart_' + fromRowNum).attr('id', 'minStart_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][minStart]');
	$('select#hourEnd_' + fromRowNum).attr('id', 'hourEnd_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][hourEnd]');
	$('select#minEnd_' + fromRowNum).attr('id', 'minEnd_' + toRowNum).attr('name', 'dateInfoAry[' + toRowNum + '][minEnd]');
	$('input#dateChk_' + fromRowNum).attr('id', 'dateChk_' + toRowNum).attr('name', 'dateChk_' + toRowNum);
}

function deleteDate() {
	if ($('input.dateChk:checked').length == 0) {
		alert(globalAlertMsg2);
	}
	else {
		$('input.dateChk:checked').each( function() {
			var _rowNum = parseInt($(this).attr('id').replace('dateChk_', ''));
			$('tr#dateTimeTr_' + _rowNum).remove();
			$('input#dateRecordId_' + _rowNum).remove();
		});
		
		var rowCounter = 1;
		$('tr.dateTimeTr').each( function() {
			var _rowNum = parseInt($(this).attr('id').replace('dateTimeTr_', ''));
			convertRowNumOfTr(_rowNum, rowCounter++);
		});
	}
	
	// show the no data display row if deleted all items
	if ($('tr.dateTimeTr').length == 0) {
		$('tr#noRecordTr').show();
	}
}

function applyTimeToDate(selectedDateOnly) {
	var canApply = true;
	
	if (!compareTimeByObjId('', 'globalHourStart', 'globalMinStart', '', '', 'globalHourEnd', 'globalMinEnd', '')) {
		$('div#global_invalidTimeRangeWarningDiv').show();
		canApply = false;
	}
	else {
		$('div#global_invalidTimeRangeWarningDiv').hide();
	}
	
	if (canApply) {
		var selectedPara = '';
		if (selectedDateOnly == 1) {
			selectedPara = ':checked';
		}
		
		var targetStartHour = $('select#globalHourStart').val();
		var targetStartMin = $('select#globalMinStart').val();
		var targetEndHour = $('select#globalHourEnd').val();
		var targetEndMin = $('select#globalMinEnd').val();
		
		$('input.dateChk' + selectedPara).each( function() {
			var _rowNum = parseInt($(this).attr('id').replace('dateChk_', ''));
			
			$('select#hourStart_' + _rowNum).val(targetStartHour);
			$('select#minStart_' + _rowNum).val(targetStartMin);
			$('select#hourEnd_' + _rowNum).val(targetEndHour);
			$('select#minEnd_' + _rowNum).val(targetEndMin);
		});
	}
}

function reloadDateTable() {
	//$("div#meetingDateCalendarDiv").html(getAjaxLoadingMsg());
	Block_Element('meetingDateCalendarDiv', getAjaxLoadingMsg(false));
	
	$.post(
		"../../ajax_reload.php",
		{
			Action: 'getMeetingDate_dateSelectionCalendar',
			recordType: getRecordType(),
			recordId: getRecordId(),
			year: curYear,
			month: curMonth
		},
		function (data) {
			$("div#meetingDateCalendarDiv").html(data);
			
			UnBlock_Element('meetingDateCalendarDiv');
		}
	);
}

function reloadPeriodicDateSelectionTable() {
	Block_Element('meetingDateCalendarDiv', getAjaxLoadingMsg(false));
	
	$.post(
		"ajax_reload.php",
		{
			Action: 'getMeetingDate_periodicDateSelectionTable'
		},
		function (data) {
			$('div#meetingDateCalendarDiv').html(data);
			$('input#StartDate').val('<?=$AcademicStart?>');
			$('input#EndDate').val('<?=$AcademicEnd?>');
			$('span#periodicDate_academicYearStartDateSpan').html(getStartDateWanringMsg());
			$('span#periodicDate_academicYearEndDateSpan').html(getEndDateWanringMsg());
			$('div#invalidStartDateWarningDiv').html($('div#invalidStartDateWarningDiv').html().replace('<!--targetDate-->', '<?=$AcademicStart?>'));
			$('div#invalidEndDateWarningDiv').html($('div#invalidEndDateWarningDiv').html().replace('<!--targetDate-->', '<?=$AcademicEnd?>'));
			
			UnBlock_Element('meetingDateCalendarDiv');
		}
	);
}

function reloadDateTimeTable() {
	$.post(
		"../../ajax_reload.php",
		{
			Action: 'getMeetingDate_dateTimeMgmtTable',
			recordType: getRecordType(),
			recordId: getRecordId()
		},
		function (data) {
			$("div#meetingDateTimeDiv").html(data);
		}
	);
}

function checkForm() {
	var canSubmit = true;
	
	// hide all warning message
	$('div.warningDiv').hide();
	
	// check time range valid and overlapping
	var maxRowNum = getMaxRowNum();
	var i, j;
	
	for (i=1; i<=maxRowNum; i++) {
		var _rowNum1 = i;
		var _dateText1 = $('input#dateTextHidden_' + _rowNum1).val();
		
		var _hourStart1 = 'hourStart_' + _rowNum1;
		var _minStart1 = 'minStart_' + _rowNum1;
		var _hourEnd1 = 'hourEnd_' + _rowNum1;
		var _minEnd1 = 'minEnd_' + _rowNum1;
		
		if (!compareTimeByObjId(_dateText1, _hourStart1, _minStart1, '', _dateText1, _hourEnd1, _minEnd1, '')) {
			$('div#invalidTimeRangeWarningDiv_' + _rowNum1).show();
			canSubmit = false;
		}
		
		for (j=i+1; j<=maxRowNum; j++) {
			var __rowNum2 = j;
			var __dateText2 = $('input#dateTextHidden_' + __rowNum2).val();
				
			if (_dateText1 == __dateText2) {
				var __hourStart2 = 'hourStart_' + __rowNum2;
				var __minStart2 = 'minStart_' + __rowNum2;
				var __hourEnd2 = 'hourEnd_' + __rowNum2;
				var __minEnd2 = 'minEnd_' + __rowNum2;
				
				if (checkTimeRangeOverlappedByObjId(_dateText1, _hourStart1, _minStart1, '', _dateText1, _hourEnd1, _minEnd1, '', __dateText2, __hourStart2, __minStart2, '', __dateText2, __hourEnd2, __minEnd2, '')) {
					$('div#timeOverlapWarningDiv_' + _rowNum1).show();
					$('div#timeOverlapWarningDiv_' + __rowNum2).show();
					canSubmit = false;
				}
			}
		}
	}

	if (canSubmit) {
		$('form#form1').attr('action', 'time_limit_setting_update.php').submit();
	}
}

function checkRCChoice(){
	if($('#rcChoice_p').attr('checked')){
		$('#periodTr').show();
		$('#cycleTr').hide();
	} else {
		//$('#rcChoice_c').attr('checked')
		$.ajax({
			method: 'POST',
			url: '../../ajax_retriveCycleDayTextByDateRange.php',
			data: {
				StartDate: $('#StartDate').val(),
				EndDate: $('#EndDate').val()
			},
			success: function(responseText){
				$('#cycleTr td:nth-child(2)').html(responseText);
			}
		});
		$('#cycleTr').show();
		$('#periodTr').hide();
	}
}

</script>
<br />
<form id="form1" name="form1" action="" method="post">
	
	<?=$h_instruction?>
	<br style="clear:both;" />
	
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?php echo $Lang['eEnrolment']['curriculumTemplate']['intake']['instructor'];?></td>
			<td><?php echo $staffName;?></td>
		</tr>
	</table>
	<br />
	
	<? if ($libenroll->AllowToEditPreviousYearData) { ?>
	<div style="float:left"><?=$h_schedulingDatesNavigation?></div>
	<table class="form_table_v30">
		<tr>
			<td class="field_title"><?=$Lang['eEnrolment']['DateType']?></td>
			<td><?=$h_dateTypeSelection?></td>
		</tr>
		<tr>
			<td class="field_title"><?=$Lang['General']['Date']?></td>
			<td>
				<div id="singleDateGlobalTimeSelDiv"><?=$h_singleDate_globalTimeSeletion?></div>
				<div>
					<table border="0" cellpadding="5" cellspacing="0">
						<tr>
							<td style="padding:5px; background-color:#dbedff" valign="top">
								<div id='meetingDateCalendarDiv'></div>											
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
	</table>
	<br style="clear:both;" />
	<? } ?>
	
	<div style="float:left"><?=$h_schedulingTimesNavigation?></div>
	<br style="clear:both;" />
	<?=$h_actionBtn?>
	<div id="meetingDateTimeDiv"></div>
	<br style="clear:both;" />
	
	
	<br style="clear:both;" />
	<div class="edit_bottom_v30">
		<?=$h_submitBtn?>
		<?=$h_cancelBtn?>
	</div>
	<div id="HiddenDiv"></div>
	<input type="hidden" id="recordType" name="recordType" value="<?php echo $enrolConfigAry['Setting']['TimeLimit'];?>" />
	<input type="hidden" id="recordId" name="recordId" value="<?php echo $staffID;?>" />
	<input type="hidden" id="StaffID" name="StaffID" value="<?php echo $staffID;?>" />
	
	<?=$picView_HiddenField?>
</form>
<?php
$linterface->LAYOUT_STOP();
?>