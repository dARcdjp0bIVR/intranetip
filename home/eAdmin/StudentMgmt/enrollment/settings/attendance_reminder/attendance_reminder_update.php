<?php
##### Change Log [Start] #####
#
#	Date	:	2015-09-24	Omas
# 			Create this page
#
###### Change Log [End] ######
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcrontab.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$GeneralSetting = new libgeneralsettings();

$Settings['UseAttendanceReminder'] = $UseAttendanceReminder;
$Settings['AttendanceReminderSubject'] = stripslashes($AttendanceReminderSubject);

if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod")
{
	if ($AttendanceReminderContent==strip_tags($AttendanceReminderContent))
	{
		$AttendanceReminderContent = nl2br($AttendanceReminderContent);
	}
}
$Settings['AttendanceReminderContent'] = stripslashes($AttendanceReminderContent);

$Settings['AttendanceReminderDaysBefore'] = $AttendanceReminderDaysBefore;
$Settings['AttendanceReminderSendTime'] = sprintf("%02d:%02d:%02d",$AttendanceReminderSendTime_hour,$AttendanceReminderSendTime_min,$AttendanceReminderSendTime_sec);

$GeneralSetting->Start_Trans();
if ($GeneralSetting->Save_General_Setting($libenroll->ModuleTitle,$Settings)) {
	$GeneralSetting->Commit_Trans();
	
	// update cron job
	$lcrontab = new libcrontab();
	$site = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"");
	$script = $site."/schedule_task/enrolment_attendance_reminder.php";
	
	if($UseAttendanceReminder == 1){
		$lcrontab->setJob($AttendanceReminderSendTime_min, $AttendanceReminderSendTime_hour, '*', '*', '*', $script);
	}else{
		$lcrontab->removeJob($script);
	}
	
	$Msg = $Lang['General']['ReturnMessage']['SettingSaveSuccess'];
}
else {
	$GeneralSetting->RollBack_Trans();
	$Msg = $Lang['General']['ReturnMessage']['SettingSaveFail'];
}

intranet_closedb();
header("Location: attendance_reminder.php?Msg=".urlencode($Msg));
?>