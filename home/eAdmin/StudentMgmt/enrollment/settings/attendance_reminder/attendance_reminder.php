<?php
##### Change Log [Start] #####
#
#	Date	:	2015-09-24	Omas
# 			Create this page
#
###### Change Log [End] ######
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) || $plugin['eEnrollmentLite'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$CurrentPage = "PageSysSettingAttendanceReminder";
$CurrentPageArr['eEnrolment'] = 1;
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

//$linterface = new interface_html();	
$linterface = new libclubsenrol_ui();
$TAGS_OBJ[] = array($Lang['eEnrolment']['EmailReminder']);
$linterface->LAYOUT_START($Msg);

$GeneralSetting = new libgeneralsettings();
$Settings = array();

$SettingList[] = "'UseAttendanceReminder'";
$SettingList[] = "'AttendanceReminderSubject'";
$SettingList[] = "'AttendanceReminderContent'";
$SettingList[] = "'AttendanceReminderDaysBefore'";
$SettingList[] = "'AttendanceReminderSendTime'";

$Settings = $GeneralSetting->Get_General_Setting($libenroll->ModuleTitle,$SettingList);
if($Settings['EmailReminderDaysBefore'] == ''){
	$Settings['EmailReminderDaysBefore'] = 0;
}
if($Settings['EmailReminderSendTime'] == ''){
	$Settings['EmailReminderSendTime'] = '00:00:00';
}

include_once($PATH_WRT_ROOT.'templates/html_editor/fckeditor.php');
if ($userBrowser->platform=="iPad" || $userBrowser->platform=="Andriod"){
	$AttendanceReminderContentHtml = '<textarea name="AttendanceReminderContent" id="AttendanceReminderContent" cols="75%" rows="20">'.$Settings['AttendanceReminderContent'].'</textarea>';
}else{
	$AttendanceReminderContentEditor = new FCKeditor ( 'AttendanceReminderContent' , "100%", "320");
	$AttendanceReminderContentEditor->Value = $Settings['AttendanceReminderContent'];
	$AttendanceReminderContentHtml = $AttendanceReminderContentEditor->Create2();
}

// select Day
$selectDaysBefore = $linterface->getDaysSelect(1,$Settings['AttendanceReminderDaysBefore']);

if($Settings['AttendanceReminderDaysBefore'] == 0){
	$displayReminderDays = $Lang['eEnrolment']['AttendanceReminderToday'];
}
else if($Settings['AttendanceReminderDaysBefore'] > 0){
//	$displayReminderDays = str_replace('<!--Days-->',$Settings['AttendanceReminderDaysBefore'],$Lang['eEnrolment']['AttendanceReminderDayAfter']);
	$displayReminderDays = $Settings['AttendanceReminderDaysBefore'].' '.$Lang['eEnrolment']['AttendanceReminderDayAfter'];
}
?>
<script language="javascript">
<!--
var off = "<?=$Settings['UseAttendanceReminder']!=1?>";

$(document).ready( function() {
	checkOnOff();
});

function checkOnOff(){
	if(off){
		$('.Off_Hide').hide();
	}
	else{
		$('.Off_Hide').show();
	}
}

function EditInfo(edit)
{
	if(edit == 1){
		$('.Edit_Hide').hide();
		$('.Edit_Show').show();
		$('#UpdateBtn').show();
		$('#CancelBtn').show();
		$('#EditBtn').hide();
	}else{
		$('.Edit_Hide').show();
		$('.Edit_Show').hide();
		$('#UpdateBtn').hide();
		$('#CancelBtn').hide();
		$('#EditBtn').show();
		checkOnOff();
	}
}

function FCKeditor_OnComplete(EDITOR){
	EDITOR.SetHTML($('#Content').html());
}

//-->
</script>

<form name="form1" method="post" action="attendance_reminder_update.php">

<div class="table_board">
	<div class="table_row_tool row_content_tool">
		<?= $linterface->GET_SMALL_BTN($button_edit, "button","EditInfo(1);","EditBtn")?>
	</div>

	<table class="form_table_v30">
		<!--On Off Button-->
		<tr valign='top' class="Edit_Hide">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['UseAttendanceReminder']?></td>
			<td> 
				<span class="Edit_Hide"><?=$Settings['UseAttendanceReminder']==1? $i_general_yes:$i_general_no?></span>
			</td>
		</tr>
		<tr valign='top' class="Edit_Show" style="display:none">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['UseAttendanceReminder']?></td>
			<td> 
				<span class="Edit_Show" style="display:none">
					<input type="radio" name="UseAttendanceReminder" value="1" <?=$Settings['UseAttendanceReminder']==1? "checked":"" ?> id="UseAttendanceReminder1"> <label for="UseAttendanceReminder1"><?=$i_general_yes?></label> 
					<input type="radio" name="UseAttendanceReminder" value="0" <?=$Settings['UseAttendanceReminder']!=1 ? "checked":"" ?> id="UseAttendanceReminder0"> <label for="UseAttendanceReminder0"><?=$i_general_no?></label>
				</span>
			</td>
		</tr>
		<!--Title-->
		<tr valign='top' class="Edit_Hide Off_Hide">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['AttendanceReminderTitle']?></td>
			<td> 
				<span class="Edit_Hide"><?=$Settings['AttendanceReminderSubject']?></span>
			</td>
		</tr>
		<tr valign='top' class="Edit_Show" style="display:none">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['AttendanceReminderTitle']?></td>
			<td> 
				<span class="Edit_Show" style="display:none">
					<input id="AttendanceReminderSubject" type="text" value="<?=$Settings['AttendanceReminderSubject']?>" name="AttendanceReminderSubject" size="100" maxlength="256" /><br>
				</span>
			</td>
		</tr>
		<!--Content-->
		<tr valign='top' class="Edit_Hide Off_Hide">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['AttendanceReminderContent']?></td>
			<td id="Content">
				<?=$Settings['AttendanceReminderContent']?>
			</td>
		</tr>
		<tr valign='top' class="Edit_Show" style="display:none">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['AttendanceReminderContent']?></td>
			<td>
				<?=$AttendanceReminderContentHtml?><br>
				<span class="tabletextremark">(<?=$Lang['eEnrolment']['AttenanceReminderRemark']?>)</span>
			</td>
		</tr>
		<!--Reminder Time-->
		<tr valign='top' class="Edit_Hide Off_Hide" >
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['AttendanceReminderTime']?></td>
			<td>
				<span class="Edit_Hide" id="Content"><?=$displayReminderDays?></span>
				<?='&nbsp;'.$Lang['eEnrolment']['AttendanceReminderAt'].'&nbsp;'?>
				<span class="Edit_Hide"><?=$Settings['AttendanceReminderSendTime']?></span>
			</td>
		</tr>
		<tr valign='top' class="Edit_Show" style="display:none">
			<td class="field_title" nowrap><?=$Lang['eEnrolment']['AttendanceReminderTime']?></td>
			<td>
				<span class="Edit_Show" style="display:none">
					<?=$selectDaysBefore?>
				</span>
				<?='&nbsp;'.$Lang['eEnrolment']['AttendanceReminderAt'].'&nbsp;'?>
				<span class="Edit_Show" style="display:none"><?=$linterface->Get_Time_Selection('AttendanceReminderSendTime',$Settings['AttendanceReminderSendTime'])?></span>
			</td>
		</tr>
	</table>
	
	<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_update, "submit","","UpdateBtn", "style='display: none'") ?>
		<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "EditInfo(0);","CancelBtn", "style='display: none'") ?>
		<p class="spacer"></p>
	</div>
</div>

</form>
<?
$linterface->LAYOUT_STOP();
intranet_closedb();
?>