<?php
$PATH_WRT_ROOT = "../../../../../../";
$PageRight = "ADMIN";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

if(!$sys_custom['eEnrolment']['TWGHCYMA']){
	No_Access_Right_Pop_Up();
}

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();

$Action = $_REQUEST['Action'];
if ($Action == 'Is_Category_Record') {
	$CategoryIDList = $_POST['CategoryIDList'];
	$CategoryIDArr = explode(',', $CategoryIDList);
	echo ($libenroll->Is_Activity_Nature_Linked_Data($CategoryIDArr))? '1' : '0';	
}
intranet_closedb();
?>