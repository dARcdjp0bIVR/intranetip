<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

if(!$sys_custom['eEnrolment']['TWGHCYMA']){
	No_Access_Right_Pop_Up();
}

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$successAry = array();
$libenroll->Start_Trans();

$successAry['delete'] = $libenroll->Delete_Activity_Nature($NatureID);
//$successAry['reorder'] = $libenroll->Update_Category_Order();

if (in_array(false, $successAry)) {
	$libenroll->RollBack_Trans();
	$ReturnMsgKey = 'DeleteUnsuccess'; 
}
else {
	$libenroll->Commit_Trans();
	$ReturnMsgKey = 'DeleteSuccess';
}

intranet_closedb();
header("Location: activity_nature.php?ReturnMsgKey=$ReturnMsgKey");
?>