<?php
# using: yat

################ Change Log [Start] 
#
#	Date:	2016-07-22 Cara
#			Update ModifiedBy
#	
#	Date:	2011-10-31 YatWoon
#			Add Group Code for edit with flag $sys_custom['eEnrolment']['TempClubCode'] (until the group code as general deployment) # for client Heung To Middle School at the moment
#
#	Date:	2010-12-14 YatWoon
#			add "Club name (Chinese)"
#
###################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/liborganization.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

$li = new libdb();
$libenroll = new libclubsenrol();
$lo = new libgroup($GroupID);
$oldName = $lo->Title;

if (isset($grouptools) && sizeof($grouptools)!=0)
{
    $grouptools = array_unique($grouptools);
    $grouptools = array_values($grouptools);
}


if($lo->RecordType<>$RecordType){
     $sql = "UPDATE INTRANET_USERGROUP SET RoleID = NULL WHERE GroupID = '$GroupID'";
     $li->db_db_query($sql);
}

$ClubCode = intranet_htmlspecialchars(trim(cleanHtmlJavascript($ClubCode)));
$Title = intranet_htmlspecialchars(trim($Title));
$TitleChinese = intranet_htmlspecialchars(trim($TitleChinese));
$Description = intranet_htmlspecialchars(trim($Description));
$AnnounceAllowed = ($AnnounceAllowed==1? 1:0);
if (!is_numeric($Quota) || $Quota < 0) $Quota = 5;
if ($alltools == 1)
{
    $functionAccess = "NULL";
}
else
{
    # Change the tool rights to integer
    $sum = 0;
    for ($i=0; $i<sizeof($grouptools); $i++)
    {
         $target = intval($grouptools[$i]);
         if ($target == 5)        # Question bank is for academic only
         {
             if ($RecordType == 2)
             {
                 $sum += pow(2,$target);
             }
         }
         else
         {
             $sum += pow(2,$target);
         }
    }
    $functionAccess = "'$sum'";
}

$RecordType += 0;
$fieldname  = "Title = '$Title', ";
$fieldname  .= "TitleChinese = '$TitleChinese', ";
$fieldname .= "Description = '$Description', ";
$fieldname .= "RecordType = '$RecordType', ";
$fieldname .= "StorageQuota = '$Quota',";
$fieldname .= "AnnounceAllowed = '$AnnounceAllowed',";
//$fieldname .= "FunctionAccess = $functionAccess,";
$fieldname  .= "GroupCode = '$ClubCode', ";
$fieldname .= "DateModified = now()";
$sql = "UPDATE INTRANET_GROUP SET $fieldname WHERE GroupID = '$GroupID'";
$li->db_db_query($sql);

$li->UpdateRole_UserGroup();

# Map to INTRANET_CLASS
$sql = "UPDATE INTRANET_CLASS SET GroupID = NULL WHERE ClassName = '$oldName'";
$li->db_db_query($sql);
$sql = "UPDATE INTRANET_CLASS SET GroupID = '$GroupID' WHERE ClassName = '$Title'";
$li->db_db_query($sql);

# INTRANET_ORPAGE_GROUP
$lorg = new liborganization();
if ($hide==1)
{
    $lorg->setGroupHidden($GroupID);
}
else
{
    $lorg->setGroupDisplay($GroupID);
}

#Update ModifiedBy
$ModifiedBy = $_SESSION['UserID'];
$sql = "UPDATE INTRANET_ENROL_GROUPINFO SET ModifiedBy = '$ModifiedBy', DateModified = now() WHERE GroupID = '$GroupID'";
$li->db_db_query($sql);


### Group Type edit
### Case 1: From Year to Year - Do nothing
### Case 2: From Year to Semester - Delete all related Year Club info and Insert new Semester Club(s) info
### Case 3: From Semester to Year - Delete all related Semester Club(s) info and Insert new Year Club info
### Case 4: From Semester to Semester - Check if the semesters are the same
###										if not the same, deleted the removed club(s) and insert the newly added club(s)
$targetClubType = ($ClubType == 'YearBase')? 'Y' : 'S';

$GroupInfoArr = $libenroll->Get_Group_Info_By_GroupID($GroupID);
$curClubType = $GroupInfoArr['ClubType'];

# Get all clubs corresponding to this group
$ClubArr = $libenroll->Get_Club_Info_By_GroupID($GroupID);
$numOfClub = count($ClubArr);

## Delete Club(s)
$DeleteEnrolGroupIDArr = array();
if ($curClubType == 'Y' && $targetClubType == 'S' || $curClubType == 'S' && $targetClubType == 'Y')
{
	# Case 2 and Case 3
	for ($i=0; $i<$numOfClub; $i++)
		$DeleteEnrolGroupIDArr[] = $ClubArr[$i]['EnrolGroupID'];
}
else if ($curClubType == 'S' && $targetClubType == 'S')
{
	$curSemesterArr = array();
	for ($i=0; $i<$numOfClub; $i++)
		$curSemesterArr[] = $ClubArr[$i]['Semester'];
		
	$DeleteSemArr = array_diff($curSemesterArr, $Sem);
	$numOfSem = count($DeleteSemArr);
	for ($i=0; $i<$numOfSem; $i++)
	{
		$thisSemName = $DeleteSemArr[$i];
		$thisEnrolGroupInfoArr = $libenroll->Get_Club_Info_By_GroupID($GroupID, $thisSemName);
		$DeleteEnrolGroupIDArr[] = $thisEnrolGroupInfoArr[0]['EnrolGroupID'];
	}
}
$numOfDeleteClub = count($DeleteEnrolGroupIDArr);


## Insert Club(s)
$newEnrolGroupIdAry = array();
if ($curClubType == 'S' && $targetClubType == 'Y')
{
	# case 3: insert year-based club
	$SuccessArr['Insert_YearBasedClub'] = $libenroll->Insert_Year_Based_Club($GroupID);
	if ($SuccessArr['Insert_YearBasedClub']) {
		$newEnrolGroupIdAry[] = $libenroll->db_insert_id();
	}
}
else if ($curClubType == 'Y' && $targetClubType == 'S' || $curClubType == 'S' && $targetClubType == 'S')
{
	# case 2 and case 4: insert semester-based club
	if(sizeof($curSemesterArr)==0) {		# use year-based originally (no existing semester-based club info
		$TargetSemesterArr = $Sem;	
	} else {
		$TargetSemesterArr = array_diff($Sem, $curSemesterArr);
	}
	$numOfSemester = count($TargetSemesterArr);
	
	if ($numOfSemester > 0) {
		foreach ($TargetSemesterArr as $key => $thisSemester) {
			$SuccessArr['Insert_TermBasedClub'][$thisSemester] = $libenroll->Insert_Semester_Based_Club($GroupID, $thisSemester);
			if ($SuccessArr['Insert_TermBasedClub'][$thisSemester]) {
				$newEnrolGroupIdAry[] = $libenroll->db_insert_id();
			}
		}
	}
}
$numOfNewClub = count($newEnrolGroupIdAry);


// Clone Club data from the original club if change type 
if ($curClubType == 'Y' && $targetClubType == 'S' || $curClubType == 'S' && $targetClubType == 'Y') {
	if ($numOfDeleteClub > 0 && $numOfNewClub > 0) {
		$sourceEnrolGroupId = $DeleteEnrolGroupIDArr[0];
		for ($i=0; $i<$numOfNewClub; $i++) {
			$_newEnrolGroupId = $newEnrolGroupIdAry[$i];
			$SuccessArr['From_'.$sourceEnrolGroupId.'_To_'.$_newEnrolGroupId]['copyClubData'] = $libenroll->Copy_Club_Info($sourceEnrolGroupId, $_newEnrolGroupId);
			$SuccessArr['From_'.$sourceEnrolGroupId.'_To_'.$_newEnrolGroupId]['copyClubMember'] = $libenroll->Copy_Club_Member($sourceEnrolGroupId, $_newEnrolGroupId);
			$SuccessArr['From_'.$sourceEnrolGroupId.'_To_'.$_newEnrolGroupId]['copyClubStaff'] = $libenroll->Copy_Club_Staff($sourceEnrolGroupId, $_newEnrolGroupId);
		}
	}
}


// Delete Club
for ($i=0; $i<$numOfDeleteClub; $i++) {
	$thisEnrolGroupID = $DeleteEnrolGroupIDArr[$i];
	
	# Get the Enrol Group by the Semester name
//	$thisEnrolGroupInfoArr = $libenroll->Get_Club_Info_By_GroupID($GroupID, $thisSemName);
//	$thisEnrolGroupID = $thisEnrolGroupInfoArr[0]['EnrolGroupID'];
	$SuccessArr['DeleteClub_' + $thisEnrolGroupID] = $libenroll->Delete_Club_Data($thisEnrolGroupID);
}


### Group Main Info Edit
$DataArr['GroupID'] = $GroupID;
$DataArr['ClubType'] = $targetClubType;
$DataArr['ApplyOnceOnly'] = ($ApplyOnceOnly == 1)? 1 : 0;			// For Semester-Based Club
$DataArr['FirstSemEnrolOnly'] = ($FirstSemEnrolOnly == 1)? 1 : 0;	// For Year-Based Club
$libenroll->Update_Group_Main_Info($DataArr);



intranet_closedb();
header("Location: group.php?AcademicYearID=$AcademicYearID&Semester=$Semester&xmsg=UpdateSuccess");
?>