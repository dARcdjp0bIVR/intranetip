<?php
# Modify : 

/********************************************************
 *  Modification Log
 *  2017-10-06 Anna
 *  	- added lookupClashestoSendPushMessagetoEventPIC setting
 *  
 *  2016-07-26 Anna
 *  	- add insert log for change setting
 *  
 *  2015-05-27  Omas
 * 		- add delete log for clear records
 * 	2014-12-02 Omas
 * 		- New setting $SettingArr['Activity_notCheckTimeCrash']
 *
 * 	2013-02-26  Rita
 * 		- add $Event_DisallowPICtoAddOrRemoveMembers for Customization - Acitvity Default Attendance
 * 
 * 	2012-12-21  Ivan
 * 		- added term-based application quota settings
 * 
 *  2012-11-01	Rita
 * 		- add Attendance Helper Control - Activity_DisableHelperModificationRightLimitation
 * 
 * 	2010-08-02  Thomas
 * 		- Add 'SettingArr['Activity_DisableUpdate']' to store the setting of
 *        'Update Disable' in Database
 *
 ********************************************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/json.php");
include_once($PATH_WRT_ROOT.'includes/liblog.php');

intranet_auth();
intranet_opendb();

//$appQuotaAry = $_POST['appQuotaAry'];

$libenroll = new libclubsenrol();
$lgs = new libgeneralsettings();
$json = new JSON_obj();


if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

/*
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
$lf = new libfilesystem();



$AppStart = intranet_htmlspecialchars($AppStart);
$AppEnd = intranet_htmlspecialchars($AppEnd);
//$GpStart = intranet_htmlspecialchars($GpStart);
//$GpEnd = intranet_htmlspecialchars($GpEnd);
$Description = intranet_htmlspecialchars($Description);

$setting_file = "$intranet_root/file/clubenroll_setting_event.txt";
$desp_file = "$intranet_root/file/clubenroll_desp_event.txt";
$file_content = $lf->file_read($setting_file);
$lines = split("\n",$file_content);
$lines[0] = $basicmode;
*/

if($libenroll->enableActivityDefaultAttendaceSelectionControl()){
	$DefaultAttendanceStatus = $_POST['DefaultAttendanceStatus'];
}


$SettingArr = array();
$SettingArr['Activity_EnrolmentMode'] = $basicmode;
$SettingArr['Activity_EnrollDescription'] = stripslashes($Description);

$SettingArr['Activity_ApplicationStart'] 	= NULL;
$SettingArr['Activity_ApplicationEnd']		= NULL;
$SettingArr['Activity_DefaultMin'] 		= NULL;
$SettingArr['Activity_DefaultMax'] 		= NULL;
$SettingArr['Activity_EnrollMax'] 			= NULL;
$SettingArr['Activity_DisableStatus'] 		= NULL;
$SettingArr['Activity_ActiveMemberPer'] 	= NULL;
$SettingArr['Activity_OneEnrol'] 			= NULL;
$SettingArr['Activity_notCheckTimeCrash']		= NULL;
$SettingArr['Activity_DisableCheckingNoOfActivityStuWantToJoin'] 	= NULL;
$SettingArr['Activity_OnceEmail'] 			= NULL;
$SettingArr['clubPICcreateNAAct'] 			= NULL;
$SettingArr['Activity_DisableUpdate']      = NULL;
$SettingArr['Activity_DisableHelperModificationRightLimitation']  = NULL;
$SettingArr['Activity_DisallowPICtoAddOrRemoveMembers'] = NULL;
$SettingArr['Activity_DefaultAttendanceStatus'] = NULL;
if($plugin['eClassTeacherApp']){
	$SettingArr['Activity_lookupClashestoSendPushMessagetoEventPIC'] = NULL;
}

switch ($basicmode)
{
        case 0:           # Disabled
             break;
        case 1:           # Simple
             $SettingArr['Activity_ApplicationStart'] 	= $AppStart." ".$enrolStartHour.":".$enrolStartMin.":00";
             $SettingArr['Activity_ApplicationEnd'] 	= $AppEnd." ".$enrolEndHour.":".$enrolEndMin.":00";
             $SettingArr['Activity_DefaultMin'] 		= $defaultMin;
             $SettingArr['Activity_DefaultMax'] 		= $defaultMax;
             $SettingArr['Activity_EnrollMax'] 			= $EnrollMax;
//             $SettingArr['Activity_OnceEmail'] 			= $onceEmail;
             break;
        case 2:           # Advanced
             $SettingArr['Activity_ApplicationStart'] 	= $AppStart." ".$enrolStartHour.":".$enrolStartMin.":00";
             $SettingArr['Activity_ApplicationEnd']		= $AppEnd." ".$enrolEndHour.":".$enrolEndMin.":00";
             $SettingArr['Activity_DefaultMin'] 		= $defaultMin;
             $SettingArr['Activity_DefaultMax'] 		= $defaultMax;
             $SettingArr['Activity_EnrollMax'] 			= $EnrollMax;
             $SettingArr['Activity_DisableStatus'] 		= $disableStatus;
             $SettingArr['Activity_ActiveMemberPer'] 	= $activeMemberPer;
             $SettingArr['Activity_OneEnrol'] 			= $oneEnrol;
             $SettingArr['Activity_notCheckTimeCrash'] 	= $notCheckTimeCrash;
             $SettingArr['Activity_DisableCheckingNoOfActivityStuWantToJoin'] 	= $disableCheckingNoOfActivityStuWantToJoin;
             $SettingArr['Activity_OnceEmail'] 			= $onceEmail;
             $SettingArr['clubPICcreateNAAct'] 			= $clubPICcreateNAAct;
             $SettingArr['Activity_DisableUpdate']      = $disableUpdate;
             $SettingArr['Activity_DisableHelperModificationRightLimitation'] = $Event_DisableHelperModificationRightLimitation;
             $SettingArr['Activity_DisallowPICtoAddOrRemoveMembers'] = $Event_DisallowPICtoAddOrRemoveMembers;
             $SettingArr['Activity_DefaultAttendanceStatus'] = $DefaultAttendanceStatus;
             if($plugin['eClassTeacherApp']){
             	$SettingArr['Activity_lookupClashestoSendPushMessagetoEventPIC'] = $lookupClashestoSendPushMessagetoEventPIC;
             }
             break;
}

$Success['EditSettings'] = $lgs->Save_General_Setting($libenroll->ModuleTitle, $SettingArr);

//$Success['SaveQuotaSettings'] = $libenroll->Save_Application_Quota_Settings($enrolConfigAry['Activity'], $appQuotaAry);

/*
$updatedcontent = implode("\n",$lines);
$lf->file_write($updatedcontent,$setting_file);
$lf->file_write($Description, $desp_file);
*/

if ($clearRecord==1)
{
    intranet_opendb();
    $li = new libdb();
//    $sql = "DELETE FROM INTRANET_ENROL_STUDENT WHERE MaxEvent IS NOT NULL";
//    $li->db_db_query($sql);
	//$sql = "UPDATE INTRANET_ENROL_STUDENT Set MaxEvent = 0 ";
	$sql = "UPDATE INTRANET_ENROL_STUDENT Set MaxEvent = null ";
	$li->db_db_query($sql);
	$sql = "UPDATE INTRANET_ENROL_STUDENT_CATEGORY_ENROL Set MaxEvent = 0 ";
	$li->db_db_query($sql);
    $sql = "DELETE FROM INTRANET_ENROL_EVENTSTUDENT";
    $li->db_db_query($sql);
    $sql = "UPDATE INTRANET_ENROL_EVENTINFO SET Approved = 0, AddToPayment = 0";
    $li->db_db_query($sql);
    
    # Insert delete log
    include_once($PATH_WRT_ROOT.'includes/liblog.php');
	$liblog = new liblog();
	$SuccessArr['Log_Delete'] = $liblog->INSERT_LOG('eEnrolment', 'Delete_All_Activity_Enrol_Records');
}


$SettingArr['ModifiedBy'] = $_SESSION['UserID'];
$jsonSettingArr = $json->encode($SettingArr);

# insert log
$liblog = new liblog();
$SuccessArr['Log_Change'] = $liblog->INSERT_LOG($libenroll->ModuleTitle, 'Change_Activity_Setting', $jsonSettingArr, 'GENERAL_SETTING');


header("Location: basic_event.php?msg=2");
?>