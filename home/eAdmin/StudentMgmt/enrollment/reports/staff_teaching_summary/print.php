<?php
/*
 *  using:
 *  
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-08-21 Cameron
 *      - fix: should check if record exist before calling foreach loop when using pass by ref. 
 *      
 *  2018-08-14 Cameron
 *      create this file
 */ 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();

if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$displayType = $_POST['displayType'];
$reportTitle = $_POST['reportTitle'];

if ($displayType == 'detail') {
    $trainerAry = $_POST['trainerAry'];
    $courseAry = $_POST['courseAry'];
    $lessonAry = $_POST['lessonAry'];
    
    if (count($trainerAry)) {
        foreach($trainerAry as $_trainerID => &$trainer){
            $trainer = standardizeFormPostValue($trainer);
            if (count($courseAry[$_trainerID])) {
                foreach($courseAry[$_trainerID] as $__courseID => &$course) {
                    $course = standardizeFormPostValue($course);
                    if (count($lessonAry[$_trainerID][$__courseID])) {
                        foreach($lessonAry[$_trainerID][$__courseID] as $___lessonID => &$lesson) {
                            if (count($lesson)) {
                                foreach($lesson as &$lessonCol) {
                                    $lessonCol = standardizeFormPostValue($lessonCol);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    $dataAry['trainer'] = $trainerAry;
    $dataAry['course'] = $courseAry;
    $dataAry['lesson'] = $lessonAry;
    
    $htmlAry['resultTable'] = $libenroll_report->getStaffTeachingDetailReport($dataAry, $returnHidden=false, $reportTitle);
}
else {
    $headerAry = $_POST['headerAry'];
    $contentAry= $_POST['contentAry'];
    $widthAry= $_POST['widthAry'];
    
    if (count($headerAry)) {
        foreach($headerAry as &$header){
        	$header = standardizeFormPostValue($header);
        }
    }
    if (count($contentAry)) {
        foreach($contentAry as &$contentRow){
            if (count($contentRow)) {
            	foreach($contentRow as &$data){
            		$data = standardizeFormPostValue($data);
            	}
            }
        }
    }

    $htmlAry['resultTable'] = $libenroll_report->getGeneralEnrolmentReportTableHtml($headerAry, $contentAry,$widthAry);
}

?>
<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN('Print', "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
	<tr>
		<td align="center" class="page_title_print" style="text-decoration: none">
			<?php echo $reportTitle;?>
		</td>
	</tr>
</table>

<?=$htmlAry['resultTable']?>
<?
intranet_closedb();
?>