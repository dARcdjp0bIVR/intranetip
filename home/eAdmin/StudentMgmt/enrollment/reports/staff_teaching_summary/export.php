<?php
/*
 *  using:
 *  
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-08-21 Cameron
 *      - fix: should check if record exist before calling foreach loop when using pass by ref. 
 *      
 *  2018-08-14 Cameron
 *      - create this file
 */
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();

if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$displayType = $_POST['displayType'];

if ($displayType == 'detail') {
    $filename = "staff_teaching_detail_report.csv";
    
    $trainerAry = $_POST['trainerAry'];
    $courseAry = $_POST['courseAry'];
    $lessonAry = $_POST['lessonAry'];
    $reportTitle = $_POST['reportTitle'];
    
    $rowCount = 0;
    
    if ($reportTitle) {
        $dataAry[$rowCount++][0] = standardizeFormPostValue($reportTitle);
    }
    
    if (count($trainerAry) == 0) {
        // column header
        $dataAry[$rowCount][] = $Lang['eEnrolment']['curriculumTemplate']['course']['course'];
        $dataAry[$rowCount][] = $Lang['eEnrolment']['curriculumTemplate']['course']['lesson'];
        $dataAry[$rowCount][] = $Lang['General']['Date'];
        $dataAry[$rowCount][] = $Lang['General']['Time'];
        
        $rowCount++;
        $dataAry[$rowCount][0] = $Lang['General']['NoRecordAtThisMoment'];
    }
    else {
        foreach((array)$trainerAry as $_trainerID => $_trainer) {
            if ($rowCount > 1) {
                $dataAry[$rowCount++][0] = '';  // line break;
            }
            
            $dataAry[$rowCount++][0] = standardizeFormPostValue($_trainer);
            // column header
            $colCount = 0;
            $dataAry[$rowCount][$colCount++] = $Lang['eEnrolment']['curriculumTemplate']['course']['course']; 
            $dataAry[$rowCount][$colCount++] = $Lang['eEnrolment']['curriculumTemplate']['course']['lesson'];
            $dataAry[$rowCount][$colCount++] = $Lang['General']['Date'];
            $dataAry[$rowCount++][$colCount++] = $Lang['General']['Time'];
            
            foreach((array)$courseAry[$_trainerID] as $__courseID => $__course) {
                $colCount = 0;
                $lessonCount = 0;
                $dataAry[$rowCount][$colCount++] = standardizeFormPostValue($__course);
                
                foreach((array)$lessonAry[$_trainerID][$__courseID] as $___lessonID => $___lessonAry) {
                    $___lesson = standardizeFormPostValue($___lessonAry['Lesson']);
                    $___date = $___lessonAry['ActivityDate'];
                    $___time = $___lessonAry['TimeSlot'];
                    if ($lessonCount > 0) {
                        $colCount = 0;
                        $dataAry[$rowCount][$colCount++] = '';
                    }
                    
                    $dataAry[$rowCount][$colCount++] = $___lesson;
                    $dataAry[$rowCount][$colCount++] = $___date;
                    $dataAry[$rowCount++][$colCount++] = $___time;
                    $lessonCount++;
                }
            }
            
        }
    }
}
else {  // summary
    $filename = "staff_teaching_summary_report.csv";

    $headerAry = $_POST['headerAry'];
    $dataAry = $_POST['contentAry'];
    $seperator = $_POST['seperator'];
    $reportTitle = $_POST['reportTitle'];
    $reportTitle = array(standardizeFormPostValue($reportTitle));
    
    if (count($headerAry)) {
        foreach($headerAry as &$header){
        	$header = standardizeFormPostValue($header);
        }
    }
    if (count($dataAry)) {
        foreach($dataAry as &$contentRow){
            if (count($contentRow)) {
            	foreach($contentRow as &$data){
            		$data = standardizeFormPostValue($data);
            	}
            }
        }
    }
    else {
        $dataAry[0][0] = $Lang['General']['NoRecordAtThisMoment'];
    }
    
    $dataAry = array_merge(array($reportTitle, $headerAry), $dataAry);
    unset($headerAry);
    
}

/**
 * Turn <br/> to /n
 */
$numOfRow = count($dataAry);
for($i=0;$i<$numOfRow;$i++){
    $numOfColumn = count($dataAry[$i]);
    for($j=0;$j<$numOfColumn;$j++){
        $dataAry[$i][$j]=str_replace($seperator,"\n",$dataAry[$i][$j]);
    }
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);


intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>