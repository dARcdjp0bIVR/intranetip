<?php
## Modifying By: 

/********************************************
 * Date: 	2013-04-19 (Rita)
 * Details:	create this page
 ********************************************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Check User Access
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&& (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	# Check flag
	if(!$libenroll->enableCCHPWOutOfSchoolActivityStudentAttendConfirmList()){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$linterface = new interface_html();

# Current Page Info
$CurrentPage = "PageCCHPWOutOfSchoolActivityStudentList";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']["OutOfSchoolActivityStudentList"], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Get Current Academic Year
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();

# Option Table Display
$htmlAry['OptionTableDisplay'] = $libenroll_ui->Get_Activity_Report_Option_Table($sel_category, $Period, $AcademicYearID);

$linterface->LAYOUT_START();
?>

<script language="javascript">

function changeTerm(val) {
	
	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
	
	getActivityCategory();
	
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
		
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
		
			xmlHttp = GetXmlHttpObject()

	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "../../get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year=<?=$AcademicYearID?>";
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if(!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) {?>
	url += "&ownClassOnly=1";
	<?}?>
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);
	if (val=='student') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function getActivityCategory() { 
	
	var categoryID = document.form1.sel_category.value;
	
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if(jPeriod=="DATE")		
	{
		// force display current year's clubs
		var yearID = <?=$AcademicYearID?>;
	}
	else
	{
		// display selected school year's clubs
		var yearID = document.form1.selectYear.value;
	}
	
	$('#CategoryElements').load(
		'../out_of_school_activity_application_form/ajax_reload.php', 
		{
			Action: 'CategoryElements',
			categoryID: categoryID,
			AcademicYearID: yearID
		}, 
		function (data){
			//SelectAll($("#activityCategory").get(0));
			SelectAll(document.getElementById('activityCategory'));
		});
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	document.getElementById('div_Target_err_msg').innerHTML = "";
 	document.getElementById('div_Times_err_msg').innerHTML = "";
 	document.getElementById('div_Hours_err_msg').innerHTML = "";
 	document.getElementById('div_Category_err_msg').innerHTML = "";
}

function checkForm()
{
	var obj = document.form1;
	
	var error_no = 0;
	var focus_field = "";
	var choiceSelected;
		
	//// Reset div innerHtml
	//reset_innerHtml();
	
	///// Period
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate(obj.textFromDate.value, obj.textToDate.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_invalid_date?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "textFromDate";
	}
	
	///// Target
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Form?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Class?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if(document.getElementById('rankTarget').value == "student") 
	{
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		
		if(choiceSelected==0) 
		{
			document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "rankTarget";
		}
	}
	
	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}	 
	
	///// category
	if(!countOption(obj.elements["activityCategory[]"]))
	{
		document.getElementById('div_Category_err_msg').innerHTML = '<font color="red"><?=$Lang['General']['ActivityParticipationReport']['SelectAtLeastOneActivity']?></font>';
		error_no++;
		focus_field = "sel_category";
	}

	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true;
	}
}



</script>
<?=$htmlAry['OptionTableDisplay'];?>
	
<script language="javascript">
	$(document).ready(function(){
		changeTerm($("#selectYear").val());
	 	showRankTargetResult($("#rankTarget").val(), '');
	 	getActivityCategory();
	});
</script>	

<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>