<?php 
/*
 *  Change Log:
 *  Date 2017-02-15 Villa Redo the file 
 *  Date 2016-11-30 Villa[R99066] Open the file
 */
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$lexport = new libexporttext();

$AcademicYear = Get_Current_Academic_Year_ID();
##### Class Filter START#####
$ClassIDArr = implode(",",$targetClassID);
$ClassIDArr = "(".$ClassIDArr.")";
$sql = "
		SELECT 
			UserID 
		FROM 
			YEAR_CLASS_USER 
		WHERE 
			YearClassID in $ClassIDArr;
		";
$student = $libenroll->returnResultSet($sql);
$student = Get_Array_By_Key($student, "UserID");
$studentArr = implode(",",$student);
$studentArr = "(".$studentArr.")";
##### Class Filter END #####
if($Semester){
	$Semester_sql = implode("','",$Semester);
	$Semester_cond = " AND (ieg.Semester  IN ('{$Semester_sql}') ";
	if($Semester[0]=='0'){
		$Semester_cond .= "OR ieg.Semester IS NULL ";
	}
	$Semester_cond .= ") ";
}
##### Export Data #####

$sql = "
		SELECT 
			UserID, UserLogin, EnglishName, ChineseName, Gender, ClassName, ClassNumber
		FROM 
			INTRANET_USER
		WHERE 
			UserID in $studentArr
		Order By
			ClassName, length(ClassNumber), ClassNumber
		";
$Student_Data = $libenroll->returnResultSet($sql);

foreach ($targetType as $_targetType){
	if($_targetType=='group'){ //club
		$sql = "
				SELECT 
				 	ig.Title, ig.TitleChinese, 
				 	iegi.Semester, iegi.EnrolGroupID,
					iu.UserID, iu.UserLogin, iu.EnglishName, iu.ChineseName, iu.ClassName, iu.ClassNumber, 
					ieg.DateModified, ieg.RecordStatus, ieg.Choice, ieg.GroupID
				FROM 
					INTRANET_ENROL_GROUPSTUDENT ieg
				INNER JOIN
					INTRANET_GROUP ig ON ieg.GroupID = ig.GroupID
				INNER JOIN
					INTRANET_ENROL_GROUPINFO iegi ON ig.GroupID = iegi.GroupID and ieg.EnrolGroupID = iegi.EnrolGroupID
				INNER JOIN
					INTRANET_USER iu ON iu.UserID = ieg.StudentID
				WHERE 
					ieg.StudentID in $studentArr
				AND
					ig.AcademicYearID = {$AcademicYear}
				ORDER BY
					iu.ClassName, iu.ClassNumber, ieg.Choice
			";
		$header_sql = "
						SELECT
							Distinct
							ig.GroupID, ig.TitleChinese, ig.Title, 
							ieg.Semester, ieg.EnrolGroupID
						FROM
							INTRANET_GROUP ig
						INNER JOIN
							INTRANET_ENROL_GROUPINFO ieg ON ig.GroupID = ieg.GroupID
						WHERE 
							ig.AcademicYearID = {$AcademicYear}
						$Semester_cond
						ORDER BY
							ieg.Semester asc
			
						";
		$data = $libenroll->returnResultSet($sql);
		$temp = $libenroll->returnResultSet($header_sql);
		$header_group = BuildMultiKeyAssoc($temp, 'EnrolGroupID'); //header for group => all group in this year 
		foreach ($data as $_data){
// 			$GropuID_TermID = $_data['GroupID'].'_'.$_data['Semester'];
			$finalData[$_data['UserID']][$_data['EnrolGroupID']]['Priority'] = $_data['Choice'];
			$finalData[$_data['UserID']][$_data['EnrolGroupID']]['RecordStatus'] = $_data['RecordStatus'];
		
		}
		foreach($Student_Data as $_Student_Data){
			$result[$_Student_Data['UserID']] ['StudentInfo'] = $_Student_Data;
			foreach ($header_group as $_header_group){
// 				$GropuID_TermID = $_header_group['GroupID'].'_'.$_header_group['Semester'];
				$result[$_Student_Data['UserID']] ['Group'] [$_header_group['EnrolGroupID']] ['Priority']= $finalData[$_Student_Data['UserID']] [$_header_group['EnrolGroupID']] ['Priority'];
				$result[$_Student_Data['UserID']] ['Group'] [$_header_group['EnrolGroupID']] ['RecordStatus']= $finalData[$_Student_Data['UserID']] [$_header_group['EnrolGroupID']] ['RecordStatus'];
			}
		}
	}else{ //activity
		$sql = "
			SELECT
				ie.EventTitle,
				iu.UserID, iu.UserLogin, iu.EnglishName, iu.ChineseName, iu.ClassName, iu.ClassNumber,
				iee.DateModified, iee.RecordStatus, iee.EnrolEventID
			FROM
				INTRANET_ENROL_EVENTSTUDENT iee
			INNER JOIN
				INTRANET_ENROL_EVENTINFO ie ON iee.EnrolEventID = ie.EnrolEventID AND ie.AcademicYearID = {$AcademicYear}
			INNER JOIN
				INTRANET_USER iu ON iu.UserID = iee.StudentID
			WHERE
				iee.StudentID in $studentArr
			ORDER BY
				iu.ClassName, iu.ClassNumber
		";
			
		$header_sql = "
		SELECT
			EnrolEventID, EventTitle
		FROM
			INTRANET_ENROL_EVENTINFO iee
		WHERE
			iee.AcademicYearID = {$AcademicYear}
			
		";
		
		$data = $libenroll->returnResultSet($sql);
		$temp = $libenroll->returnResultSet($header_sql);
		$header_activity = BuildMultiKeyAssoc($temp, 'EnrolEventID');
		foreach ($data as $_data){
			$ActivityID_TermID = $_data['EnrolEventID'];
			$finalData[$_data['UserID']][$ActivityID_TermID]['RecordStatus'] = $_data['RecordStatus'];
		
		}
		foreach($Student_Data as $_Student_Data){
			$result[$_Student_Data['UserID']] ['StudentInfo'] = $_Student_Data;
			foreach ($header_activity as $_header_activity){
				$ActivityID_TermID = $_header_activity['EnrolEventID'];
				$result[$_Student_Data['UserID']] ['Activity'] [$ActivityID_TermID] ['RecordStatus']= $finalData[$_Student_Data['UserID']] [$ActivityID_TermID] ['RecordStatus'];
			}
		}
	}
}
	
##### Export Header START #####
$exportHeader[] =  "Class";
$exportHeader[] =  "Class Number";
if(!$sys_custom['eEnrolment']['HiddenLoginName'] ){
	$exportHeader[] =  "UserLogin";
}
$exportHeader[] =  "English Name";
$exportHeader[] =  "Chinese Name";
$exportHeader[] = "Gender";
// if($header_group||$header_activity){
	$exportHeader[] = "Approved Club/ Activity";
// }
if($header_group){
	foreach ($header_group as $_header_group){
		$exportHeader[] = $_header_group['Title'];
	}
}
if($header_activity ){
	foreach ($header_activity as $_header_activity){
		$exportHeader[] = $_header_activity['EventTitle'];
	}
}
##### Export Header END #####

##### Export Body START #####
$row = 0;
foreach ($result as $student){
	$exportData[$row][] = $student['StudentInfo']['ClassName'];
	$exportData[$row][] = $student['StudentInfo']['ClassNumber'];
	if(!$sys_custom['eEnrolment']['HiddenLoginName'] ){
		$exportData[$row][] = $student['StudentInfo']['UserLogin'];
	}
	$exportData[$row][] = $student['StudentInfo']['EnglishName'];
	$exportData[$row][] = $student['StudentInfo']['ChineseName'];
	$exportData[$row][] = $student['StudentInfo']['Gender'];
	if($header_group||$header_activity){
		$ApproveDisplay = "";
		if($header_group){
			foreach ($header_group as $_header_group){
				if($student['Group'][$_header_group['EnrolGroupID']]['RecordStatus']=='2'){
					$ApproveDisplay .= $_header_group['Title'];
					$ApproveDisplay .= "\n";
				}
			}
		}
		if($header_activity){
			foreach ($header_activity as $_header_activity){
				$ActivityID_TermID = $_header_activity['EnrolEventID'];
				if($student['Activity'][$ActivityID_TermID]['RecordStatus']=='2'){
					$ApproveDisplay .= $_header_activity['EventTitle'];
					$ApproveDisplay .= "\n";
				}
			}
		}
	
		$exportData[$row][] = $ApproveDisplay;
	}else{
		$exportData[$row][] = "";
	}
	if($header_group){
		foreach ($header_group as $_header_group){
// 			$GropuID_TermID = $_header_group['GroupID'].'_'.$_header_group['Semester'];
			$exportData[$row][] = $student['Group'][$_header_group['EnrolGroupID']]['Priority'];
		}
	}
	if($header_activity ){
		foreach ($header_activity as $_header_activity){
			$ActivityID_TermID = $_header_activity['EnrolEventID'];
			$exportData[$row][] = $student['Activity'][$ActivityID_TermID]['RecordStatus']=='2'? 'V':"";
		}
	}
	$row++;
}
##### Export Body END #####
$filename = "enrolment_status";
$filename = $filename.".csv";
$export_content = $lexport->GET_EXPORT_TXT($exportData, $exportHeader, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
intranet_closedb();
// Output the file to user browser
$lexport->EXPORT_FILE($filename, $export_content);
?>