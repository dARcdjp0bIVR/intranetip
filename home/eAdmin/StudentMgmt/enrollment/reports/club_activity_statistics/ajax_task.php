<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ($plugin['eEnrollmentLite'] || ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (count($classInfo)==0) &&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) or !$plugin['eEnrollment']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

if($task == 'get_club_selection')
{
	$YearTermIDArr = $YearTermID;
	if ($YearTermIDArr != '')
	{
		if (!is_array($YearTermIDArr))
			$YearTermIDArr = array($YearTermIDArr);
			
		if (in_array(0, $YearTermIDArr))
			$conds_YearTermIDArr = " And (iegi.Semester In ('".implode("','", $YearTermIDArr)."') Or iegi.Semester Is Null Or iegi.Semester = '') ";
		else
			$conds_YearTermIDArr = " And iegi.Semester In ('".implode("','", $YearTermIDArr)."') ";
	}
	
	$sql = "SELECT 
					iegi.EnrolGroupID, 
					ig.Title,
					iec.CategoryName,
					iegi.Semester,
					iegi.RecordStatus
			FROM 
					INTRANET_GROUP as ig 
					Inner Join INTRANET_ENROL_GROUPINFO as iegi ON ig.GroupID = iegi.GroupID
					LEFT JOIN INTRANET_ENROL_CATEGORY as iec ON iec.CategoryID=iegi.GroupCategory 
					LEFT JOIN ACADEMIC_YEAR_TERM as ayt On (iegi.Semester = ayt.YearTermID) 
			WHERE 
				(
					(iegi.Semester IS NULL Or iegi.Semester = '')
					OR 
					ayt.YearTermID IS NOT NULL
				)
				AND ig.RecordType=5
				AND ig.AcademicYearID = '$AcademicYearID' $conds_YearTermIDArr 
			GROUP BY iegi.EnrolGroupID,iec.CategoryID 
			ORDER BY 
				iec.DisplayOrder,ig.GroupCode";
	//debug_r($sql);
	$club_list = $libenroll->returnArray($sql);
	$club_selection = $linterface->GET_SELECTION_BOX_WITH_OPTGROUP($club_list, ' name="sel_club[]" id="sel_club" multiple="mutiple" size="10" ', '', '');
	
	echo $club_selection;
}

intranet_closedb();
?>