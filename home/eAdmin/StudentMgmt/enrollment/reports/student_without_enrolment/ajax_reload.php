<?php
// using: 

##############################################
#	Date: 2015-12-05	Kenneth
#		- getStudentWithoutEnrolmentReportResultTableHtml, add CategoryID as filter
#
##############################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$linterface = new interface_html();
$libenroll_report = new libclubsenrol_report();

$RecordType = $_POST['RecordType'];

$targetCategory = explode(',',$_POST['targetCategory']);

if ($RecordType == 'reportResult') {
	$enrolmentSourceAry = $_POST['enrolmentSourceAry'];
	$clubTermIdAry = $_POST['clubTermIdAry'];
	$AcademicYearID = $_POST['AcademicYearID'];
	
	$numOfTerm = count($clubTermIdAry);
	for ($i=0; $i<$numOfTerm; $i++) {
		$clubTermIdAry[$i] = convert2unicode(trim(stripslashes($clubTermIdAry[$i])), 1, 0);
	}
	
	// get result table with target enrolment type and year terms
	echo $libenroll_report->getStudentWithoutEnrolmentReportResultTableHtml($enrolmentSourceAry, $clubTermIdAry, $AcademicYearID,$targetCategory);

}

?>