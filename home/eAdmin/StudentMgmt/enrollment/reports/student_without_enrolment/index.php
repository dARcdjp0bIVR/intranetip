<?php
// # Modifying By: 

// ######################################
//  Date:   2018-11-30 Anna [C153581]
//      Fixed keep loading problem
//
// Date: 2018-05-23 Philipsh
// - Modified Style and highCharts with tooltip formatter and overflow
//
// Date: 2018-05-17 Philips
// - Add HighCharts Graph
// Date: 2015-12-05 Kenneth
// - Add Category as filter
//
// ######################################
$PATH_WRT_ROOT = "../../../../../../";

include_once ($PATH_WRT_ROOT . "includes/global.php");
include_once ($PATH_WRT_ROOT . "includes/lib.php");
include_once ($PATH_WRT_ROOT . "includes/libdb.php");
include_once ($PATH_WRT_ROOT . "includes/libclass.php");
include_once ($PATH_WRT_ROOT . "includes/libinterface.php");
include_once ($PATH_WRT_ROOT . "includes/libclubsenrol.php");
include_once ($PATH_WRT_ROOT . "lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

// user access right checking
$libenroll = new libclubsenrol();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Admin');

$linterface = new interface_html();

// setting the current page
$CurrentPage = "PageStudentWithoutEnrolmentReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array(
    $Lang['eEnrolment']['StudentWithoutEnrolmentReport'],
    "",
    1
);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

// Availabe Semester
if ($AcademicYearID == null) {
    $semester_data = getSemesters(Get_Current_Academic_Year_ID());
} else {
    $semester_data = getSemesters($AcademicYearID);
}

// $numOfSemesterData = count($semester_data);
// $termIdAry = array();
// $termIdAry[0] = $i_ClubsEnrollment_WholeYear;
// Set Year term ID
// foreach ($semester_data as $yearsemester) {
// $line = split("::",$yearsemester);
// list ($name,$current) = $line;
// $termIdAry[$name] = $name;
// }

// Option Div after result table created
$divName = 'reportOptionOuterDiv';
$tdId = 'tdOption';
$x = '';
$x .= '<div id="showHideOptionDiv" style="display:none;">' . "\n";
$x .= '<table style="width:100%;">' . "\n";
$x .= '<tr>' . "\n";
$x .= '<td id="tdOption" class="report_show_option">' . "\n";
$x .= '<span id="spanShowOption_' . $divName . '">' . "\n";
$x .= $linterface->Get_Show_Option_Link("javascript:js_Show_Option_Div('$divName', '$tdId');", '', '', 'spanShowOption_reportOptionOuterDivBtn');
$x .= '</span>' . "\n";
$x .= '<span id="spanHideOption_' . $divName . '" style="display:none">' . "\n";
$x .= $linterface->Get_Hide_Option_Link("javascript:js_Hide_Option_Div('$divName', '$tdId');", '', '', 'spanHideOption_reportOptionOuterDivBtn');
$x .= '</span>' . "\n";
$x .= '<div id="' . $divName . '" style="display:none;">' . "\n";
$x .= '</div>' . "\n";
$x .= '</td>' . "\n";
$x .= '</tr>' . "\n";
$x .= '</table>' . "\n";
$x .= '</div>' . "\n";
$htmlAry['reportOptionOuterDiv'] = $x;

// Option Div
$divName = 'reportOptionDiv';
$x = '';
$x .= '<div id="' . $divName . '">' . "\n";
$x .= '<div>' . "\n";
$x .= '<table id="filterOptionTable" class="form_table_v30">' . "\n";

// Enrolment Year
$x .= '<tr>' . "\n";
$x .= '<td class="field_title">' . $Lang['eEnrolment']['ReportArr']['StudentWithoutEnrolmentReport']['EnrolmentYear'] . '</td>' . "\n";
$x .= '<td>' . "\n";
// Drop down list to select required academic year
$x .= getSelectAcademicYear("AcademicYearID", 'onChange="document.form1.action=\'index.php\';document.form1.submit();"', 1, 0, $AcademicYearID); // $x .= $linterface->Get_Checkbox('enrolmentSourceChk_club', 'enrolmentSourceAry[]', 'club', $isChecked=true, $Class='', $eEnrollment['club'], 'clickedEnrolmentSourceCheckbox(\'club\', this.checked);');
$x .= '</td>' . "\n";
$x .= '</tr>' . "\n";

// Enrolment Source
$x .= '<tr>' . "\n";
$x .= '<td class="field_title">' . $linterface->RequiredSymbol() . $Lang['eEnrolment']['ReportArr']['StudentWithoutEnrolmentReport']['EnrolmentSource'] . '</td>' . "\n";
$x .= '<td>' . "\n";
$x .= $linterface->Get_Checkbox('enrolmentSourceChk_club', 'enrolmentSourceAry[]', 'club', $isChecked = true, $Class = '', $eEnrollment['club'], 'clickedEnrolmentSourceCheckbox(\'club\', this.checked);');
// Whole year
$x .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;' . "\n";
$x .= $linterface->Get_Checkbox('clubTermId_0', 'clubTermIdAry[]', 0, $isChecked = true, $Class = 'clubTermChk', $i_ClubsEnrollment_WholeYear);
// Available Semester
foreach ((array) $semester_data as $allowSemesterName => $_termName) {
    $x .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;' . "\n";
    $x .= $linterface->Get_Checkbox('clubTermId_' . $allowSemesterName, 'clubTermIdAry[]', $allowSemesterName, $isChecked = true, $Class = 'clubTermChk', $_termName);
}
$x .= $linterface->Get_Form_Warning_Msg('clubTermWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['StudentWithoutEnrolmentReport']['ChooseSource'], $Class = 'warnMsgDiv') . "\n";
$x .= '<br />' . "\n";
$x .= $linterface->Get_Checkbox('enrolmentSourceChk_activity', 'enrolmentSourceAry[]', 'activity', $isChecked = true, $Class = "", $eEnrollment['activity'], 'clickedEnrolmentSourceCheckbox(\'activity\', this.checked);');
$x .= $linterface->Get_Form_Warning_Msg('enrolmentSourceWarnMsgDiv', $Lang['eEnrolment']['WarningArr']['StudentWithoutEnrolmentReport']['ChooseSource'], $Class = 'warnMsgDiv') . "\n";
$x .= '</td>' . "\n";
$x .= '</tr>' . "\n";

/**
 * Category selector
 */

$x .= '<tr>';
$x .= '<td class="field_title">' . $linterface->RequiredSymbol() . $eEnrollment['category'] . '</td>';
$x .= '<td>';
$x .= $libenroll->GET_CATEGORY_SEL($TargetCategory, $ParSelected = 1, '', $ParSelLang, $noFirst = 1, $CategoryTypeID = 0, $IsMultiple = 1, true, $DeflautName = 'targetCategory');
$x .= "&nbsp;" . $linterface->Get_Small_Btn($Lang['Btn']['SelectAll'], "button", "js_Select_All_With_OptGroup('targetCategory',1);");
$x .= $linterface->spacer();
$x .= $linterface->MultiSelectionRemark();
$x .= '</td>';
$x .= '</tr>';
/**
 * End of Category selector
 */

$x .= '</table>' . "\n";
$x .= '</div>' . "\n";
$x .= '<br style="clear:both;" />' . "\n";

$x .= '<div style="text-align:left;">' . "\n";
$x .= $linterface->MandatoryField();
$x .= '</div>' . "\n";
$x .= '<br style="clear:both;" />' . "\n";

$x .= '<div class="edit_bottom_v30">' . "\n";
$x .= $linterface->GET_ACTION_BTN($Lang['Btn']['View'], "button", "refreshReport();");
$x .= '</div>' . "\n";
$x .= '</div>' . "\n";
$htmlAry['reportOptionDiv'] = $x;

// Tool bar
$x = '';
$x .= '<div id="contentToolDiv" class="content_top_tool" style="display:none;">' . "\n";
$x .= '<div class="Conntent_tool">' . "\n";
$x .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
$x .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
$x .= '</div>' . "\n";
$x .= '</div>' . "\n";
$htmlAry['contentToolDiv'] = $x;

// Result Div
$htmlAry['reportResultDiv'] = '<div id="reportResultDiv"></div>';

$linterface->LAYOUT_START();
?>

<!-- Added by Philips -->
<script type="text/javascript"
	src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript"
	src="https://code.highcharts.com/modules/exporting.js"></script>
<style>
.highcharts-container {
    overflow: visible !important;
}
.MyChartTooltip {
    position: relative;
    z-index: 50;
    padding: 20px;
    font-size: 9pt;
    overflow: auto;
    height: 30vh;
}
.highcharts-tooltip {
  pointer-events: all !important;
}
.tooltip_class{
position: sticky;
 top: 0;
  background-color: lightgrey;
  opacity: 0.7;
}
</style>
<!-- Added by Philips -->

<script type="text/javascript">
$(document).ready(function () {

});

function clickedEnrolmentSourceCheckbox(targetSource, elementChecked) {
	if (targetSource == 'club') {
		var disabledStatus = (elementChecked)? '' : 'disabled';
		$('input.clubTermChk').attr('disabled', disabledStatus);
	}
}

function refreshReport() {
	var canSubmit = true;
	var isFocused = false;
	
	$('div.warnMsgDiv').hide();
	
	// choose enrollment source
	var enrolmentSourceChkName = getJQuerySaveId('enrolmentSourceAry[]');
	if ($('input[name="' + enrolmentSourceChkName + '"]:checked').length == 0) {
		canSubmit = false;
		$('div#enrolmentSourceWarnMsgDiv').show();
		if (!isFocused) {
			$('input#enrolmentSourceChk_club').focus();
			isFocused = true;
		}
	}
	
	// choose club term if selected club as enrollment source
	if ($('input#enrolmentSourceChk_club').attr('checked')) {
		if ($('input.clubTermChk:checked').length == 0) {
			canSubmit = false;
			$('div#clubTermWarnMsgDiv').show();
			if (!isFocused) {
				$('input#clubTermId_0').focus();
				isFocused = true;
			}
		}
	}
	
	var targetCategories = $('#targetCategory').val();
	
	
	
	if (canSubmit) {
		Block_Document();
		if (Trim($('div#reportOptionOuterDiv').html()) == '') {
			saveFormValues();
			
			$('div#reportOptionDiv > div').clone(true, true).appendTo('div#reportOptionOuterDiv');
			$('div#reportOptionDiv').html('');
			$('div#showHideOptionDiv').show();
			
			loadFormValues();
			$('div#contentToolDiv').show();
		}
		else {
			$('a#spanHideOption_reportOptionOuterDivBtn')[0].click();
		}
		
		$('div#reportResultDiv').html('<?=$linterface->Get_Ajax_Loading_Image()?>');
		$.ajax({
			url:      	"ajax_reload.php",
			type:     	"POST",
			data:     	$("#form1").serialize() + '&RecordType=reportResult'+ '&targetCategory='+targetCategories,
			success:  	function(xml) {
							$('div#reportResultDiv').html(xml);
							Scroll_To_Top();
							UnBlock_Document();
							createChart(); // Added by Philips (For highcharts graph)
			}
		});
	}
}

//Added by Philips (For highcharts graph)
function createChart(){
	var tableContent = $('table.common_table_list_v30 tbody'); // table elements
	var dataArray = []; // store [className] : [...studentNames];
	var ll = 0; // Count of classes
	var ii = 1; // Count of class students
	tableContent.children().each(function(key, val){
		// <tr> elements
		var list = $(val).children();
		if(!($(list[1]).text() in dataArray)){
			dataArray[$(list[1]).text()] = []; // initial array
			ll++; // count classes
			ii = 1;
		}
		dataArray[$(list[1]).text()].push(ii + '. ' + $(list[3]).text() + ' (' + $(list[2]).text() + ')'); // Format: list number. StudentName (Class No.)
		ii++;
	});
	var chartDiv = '<div id="chart_report" style="width: 100%; height: ' + (ll * 52) + 'px; margin: 0 auto;clear: left; float: left;"></div>';
	//Each class may have 52px height
	$('td.main_content').append(chartDiv);
	//Append Div element as the target
	drawChart('chart_report', { // Call to draw chart
		name: <?if($intranet_session_language == 'en')  echo "'Number of students'"; else echo "'學���人數'";?>,
		array: dataArray,
		title: $('span.contenttitle').text(),
		unit: <?if($intranet_session_language == 'en')  echo "'<Student Name (Class No.)>'"; else echo "'<學���姓��� (������)>'";?>,
	});
}

function drawChart(chartid, obj){
	// Transaction
	var series = [{name: '' ,data:[]}];
	var cate = [];
	for( var index in obj.array){
		if(index!='in_array'){
			series[0].data.push([obj.array[index].length]);
			cate.push(index); // Get Class Name into array
		}
	}
	// Call Highchart Contructor
	var chart = Highcharts.chart(chartid, {
		 chart: {
			 type: 'bar'
		 },
		 title: {
			 text: obj.title
		 },
		 xAxis:{
			categories: cate,
			title:{
				text: null
			}
		 },
		 yAxis: {
			tickInterval: 1,
			min: 0,
			max: obj.type=='percent' ? 100 : undefined,
			title: {
				text: obj.name,
				align: 'high'
			},
		 },
		 tooltip: {
			 followPointer: false,
			 useHTML: true,
			 formatter: function(){
				 var str = "<div class='MyChartTooltip'>";
				 str += '<b class="tooltip_class">Class : ' + this.x +' </b><br/>';
				 str += obj.unit + '<br/>';
				 for( var i in obj.array[this.x]){
					 if(i!='in_array'){ 
					 	str += obj.array[this.x][i] + '<br/>';
					 }
				 };
				 return str + '</div>';
			 }
		 },
		 plotOptions: {
			 bar: {
				 dataLabels: {
					 enabled: true
				 }
			 }
		 },
		 legend: {
			 enabled: false
		 },
		 credits: {
			 enabled: false
		 },
		 series: series,
		 exporting :{
			 buttons:{
				 contextButton:{
					 menuItems: [{
						textKey: 'downloadPNG',
						onclick: function () {
						    this.exportChart();
						}
					}, {
						textKey: 'downloadJPEG',
						onclick: function () {
						    this.exportChart({
						        type: 'image/jpeg'
						    });
						}
					}]
				 }
			 }
		 },
	});
	return chart;
}
//Added by Philips (For highcharts graph)

function goExport() {
	$('form#form1').attr('target', '_self').attr('action', 'export.php').submit();
}

function goPrint() {
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
}
</script>


<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['reportOptionOuterDiv']?>
	<br />
	
	<?=$htmlAry['reportOptionDiv']?>
	<?=$htmlAry['contentToolDiv']?>
	<?=$htmlAry['reportResultDiv']?>
	<br />
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>