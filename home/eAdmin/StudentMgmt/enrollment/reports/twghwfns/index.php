<?php
## Modifying By: 

/********************************************
 * 
 ********************************************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# Check User Access
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
if(!$plugin['eEnrollment'] || !$sys_custom['eEnrolment']['twghwfns']['AwardMgmt']||!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])){
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	exit;	
}
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);
$linterface = new interface_html();

# Current Page Info
$CurrentPage = "PageAwardReport_CustWFN";	
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['AwardMgmt']['twghwfns']['ActivityAwardReport'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

# Get Current Academic Year
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();

# Option Table Display
//$htmlAry['OptionTableDisplay'] = $libenroll_ui->Get_Activity_Report_Option_Table($sel_category, $Period, $AcademicYearID, 'ApplicationForm');

# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);
# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";

# Award
$reportType	= $linterface->Get_Radio_Button('Report_1', 'ReportType', '1', '1','',$Lang['eEnrolment']['AwardMgmt']['twghwfns']['ECM']).'&nbsp;'
			.$linterface->Get_Radio_Button('Report_2', 'ReportType', '2', '0','',$Lang['eEnrolment']['AwardMgmt']['twghwfns']['IMC'])
			.$linterface->Get_Radio_Button('Report_3', 'ReportType', '3', '0','',$Lang['eEnrolment']['AwardMgmt']['twghwfns']['MagazineReport']);

$optionTable .= '<form name="form1" method="post" action="result.php" target="" onSubmit="return checkForm();">';
$optionTable .= '<table class="form_table_v30">';
$optionTable .= '<tr valign="top">';
$optionTable .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang['eEnrolment']['AwardMgmt']['ReportType'].'</td>';
$optionTable .= '<td>'.$reportType.'</td>';
$optionTable .= '</tr>';
$optionTable .= '<tr valign="top">';
$optionTable .= '<td class="field_title"><span class="tabletextrequire">*</span>'.$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"].'</td>';
$optionTable .= '<td>';
$optionTable .= '<table class="inside_form_table">';
$optionTable .= '<tr>';
$optionTable .= '<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true;">';
$optionTable .= '<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" '.($Period!="DATE"?" checked":"") .'  onClick="">';
$optionTable .= $Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"];
$optionTable .= $selectSchoolYearHTML;
$optionTable .= $Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"];
$optionTable .= '<span id="spanSemester">'.$selectSemesterHTML.'</span>';
$optionTable .= '</td>';
$optionTable .= '</tr>';
$optionTable .= '<tr>';
$optionTable .= '<td onClick="document.form1.radioPeriod_Date.checked=true;"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" '.($Period=="DATE"?" checked":"") .' onClick="">'.$i_From. '</td>';
$optionTable .= '<td onClick="changeRadioSelection(\'DATE\')" onFocus="changeRadioSelection(\'DATE\')">' . $linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='' ");
$optionTable .= '<br><span id=\'div_DateEnd_err_msg\'></span></td>';
$optionTable .= '<td>'. $i_To . '</td>';
$optionTable .= '<td onClick="changeRadioSelection(\'DATE\')" onFocus="changeRadioSelection(\'DATE\')">' . $linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='' ");
$optionTable .= '</tr>';
$optionTable .= '</table>';
$optionTable .= '</td>';
$optionTable .= '</tr>';
$optionTable .= '</table>';
$optionTable .= '<div class="edit_bottom_v30">';
$optionTable .= '<p class="spacer"></p>';
$optionTable .=  $linterface->GET_ACTION_BTN($Lang['Btn']['Generate'] , "submit");
$optionTable .= '<p class="spacer"></p>';
$optionTable .= '</div>';
$optionTable .= '</form>';

$linterface->LAYOUT_START();
	
?>

<script language="javascript">


function changeTerm(val) {
	
	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	document.getElementById('div_Target_err_msg').innerHTML = "";
 	document.getElementById('div_Times_err_msg').innerHTML = "";
 	document.getElementById('div_Hours_err_msg').innerHTML = "";
 	document.getElementById('div_Category_err_msg').innerHTML = "";
}

function checkForm()
{
	var obj = document.form1;
	
	var error_no = 0;
	var focus_field = "";
	var choiceSelected;
		
	//// Reset div innerHtml
	//reset_innerHtml();
	
	// report type
	reportType = $("input[name='ReportType']:checked").val();
	if(!(reportType > 0)){
		alert('<?=$Lang['General']['PleaseSelect'].' '.$Lang['eEnrolment']['AwardMgmt']['ReportType']?>')
		return false;
	}
	
	///// Period
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate(obj.textFromDate.value, obj.textToDate.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_invalid_date?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "textFromDate";
	}

	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true;
	}
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

</script>

<?=$optionTable?>
	
<script language="javascript">
$(document).ready(function(){
	changeTerm($("#selectYear").val());
});
</script>	

<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>