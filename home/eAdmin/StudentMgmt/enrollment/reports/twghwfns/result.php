<?
## Modifying By: 

/********************************************
 * 
 ********************************************/
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
//include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once('cust_lang.php');

intranet_auth();
intranet_opendb();

# Check User Access
$libenroll = new libclubsenrol();
$linterface = new interface_html();
//$libenroll_ui = new libclubsenrol_ui();

# Current Page Info
$CurrentPage = "PageAwardReport_CustWFN";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['AwardMgmt']['twghwfns']['ActivityAwardReport'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();
?>

<script>
function goBack(){
	window.location.href = 'index.php';
}
</script>
<?
//------ Report initiation Par ready -----// 
if($_POST['radioPeriod']=='YEAR'){
	$academicYearID = $_POST['selectYear'];
	$termID = $_POST['selectSemester'];
	if($academicYearID<1){
		$academicYearID = Get_Current_Academic_Year_ID();
	}
	$termID = ($termID == 0 ? '':$termID) ;
	$PeriodOfAcademicYear = getPeriodOfAcademicYear($academicYearID);
	$startDate = substr($PeriodOfAcademicYear['StartDate'], 0, 10);
	$endDate = substr($PeriodOfAcademicYear['EndDate'], 0, 10);

}
else if($_POST['radioPeriod']=='DATE'){
	$startDate = $_POST['textFromDate'];
	$endDate = $_POST['textToDate'];	
}
else{
	if($academicYearID<1){
		$academicYearID = Get_Current_Academic_Year_ID();
	}
	$PeriodOfAcademicYear = getPeriodOfAcademicYear($academicYearID);
	$startDate = substr($PeriodOfAcademicYear['StartDate'], 0, 10);
	$endDate = substr($PeriodOfAcademicYear['EndDate'], 0, 10);
}

$eventIDArr = $libenroll->Get_Event_Within_Period($startDate,$endDate);
$studentAwardInfoArr = $libenroll->getEventAwardToStudent($eventIDArr);
//------ Report initiation Par ready (End) -----//

if($_POST['ReportType'] == 1){ 
//EMC
	$studentAwardAssoArr = array();
	foreach((array)$studentAwardInfoArr as $_awardInfoArr){
		$_EventAwardID = $_awardInfoArr['EventAwardID'];
		$_EnrolEventID = $_awardInfoArr['EnrolEventID'];
		$_AwardID = $_awardInfoArr['AwardID'];
		
		$studentAwardAssoArr[$_EnrolEventID][$_EventAwardID][$_AwardID][] = $_awardInfoArr;
	}
	
	$awardTitleArr = $libenroll->GET_ENROL_AWARD();
	$awardTitleAssoArr = BuildMultiKeyAssoc($awardTitleArr,'AwardID');
	
	$ReportStringArr = array();
	foreach((array)$studentAwardAssoArr as $_EventID => $_EventAwardArr){

		$EventInfo = $libenroll->GET_EVENTINFO($_EventID);
		$OLE_setting = $libenroll->Get_Enrolment_Default_OLE_Setting('activity',(array)$_EventID);
		
		$numOfParticipant = $EventInfo['Approved'];
		$eventName = $EventInfo['EventTitle'];
		// remove all the content in []
		$eventName = preg_replace('/\[[^\]]*\]/','',$eventName);
		
		$String = '';
		if($numOfParticipant <=1){
			$String .= 	$cust_lang['participate'];
		}
		else{
			$String .= $numOfParticipant.$cust_lang['a'].$cust_lang['student'].$cust_lang['participate'];
		}
		
		if(!empty($OLE_setting[0]['Organization'])){
			$String .= $cust_lang['by'].$OLE_setting[0]['Organization'].$cust_lang['hold'];
		}
		$String .= $eventName;
		$String .= $cust_lang['comma'];
		$String .= $cust_lang['get'];
		
		$i = 0;
		$_numTypeAward = count($_EventAwardArr);
		foreach($_EventAwardArr as $__AwardArr){
			
			$i++;
			if($i == $_numTypeAward){
				$Punctuation = $cust_lang['fullstop'];
			}
			else{
				if($i == ($_numTypeAward-1)){
					$Punctuation = $cust_lang['and'];
				}
				else{
					$Punctuation = $cust_lang['comma2'];						
				}
			}
			foreach($__AwardArr as $___AwardID => $___AwardInfoArr){
				$Division = $___AwardInfoArr[0]['Division'];
				$numAward = count($___AwardInfoArr);
				$AwardName = $awardTitleAssoArr[$___AwardID]['AwardNameCh'];
				
				if(empty($Division)){
					$String .= $numAward.$cust_lang['item'].$AwardName.$Punctuation;
				}
				else{				
					$String .= $Division.$AwardName.$Punctuation;
				}
			}
		}
		
		$ReportStringArr[] = $String;
	}

	$table_html = '';
	if(count($ReportStringArr)>0){
		$table_html .= '<table align="center">';
		foreach((array)$ReportStringArr as $number=> $String){
			$table_html .= '<tr>';
			$table_html .= '<td>';
			$table_html .= ($number+1).'.';
			$table_html .= '</td>';
			$table_html .= '<td>';
			$table_html .= $String;
			$table_html .= '</td>';
			$table_html .= '</tr>';
		}
		$table_html .= '</table>';
	}
	else{
		$table_html = '<p class="spacer"></p>';
		$table_html .= '<p class="spacer"></p>';
		$table_html .= '<p align="center">'.$eEnrollment['no_record'].'</p>';
	}
	
	$report_title = $Lang['eEnrolment']['AwardMgmt']['twghwfns']['ECM'].'('.$startDate.' - '.$endDate.')';
	$report_content = $table_html;
}
else if($_POST['ReportType'] == 2){
//IMC
	$meetingDateArr = $libenroll->Get_Activity_Meeting_Date($eventIDArr);
	foreach((array) $meetingDateArr as $_arr){
		$_enrolEventID = $_arr['EnrolEventID'];
		$meetingDateAssoArr[$_enrolEventID][] = $_arr;
	}

	$haveAwardEventIDArr = array_unique(Get_Array_By_Key($studentAwardInfoArr,'EnrolEventID'));
	$withoutAwardEventIDArr = array_diff($eventIDArr,$haveAwardEventIDArr);
	
	if(count($haveAwardEventIDArr)<1 && count($withoutAwardEventIDArr)<1){
		$report_content = '<p class="spacer"></p>';
		$report_content .= '<p class="spacer"></p>';
		$report_content .= '<p align="center">'.$eEnrollment['no_record'].'</p>';
	}
	else{
		//-- Without Award Acitivity --//	
		$activity_html = '<table width="80%" align="center" border="1" style=" border-collapse: collapse;">';
		foreach((array)$withoutAwardEventIDArr as $_EnrolEventID){
			$EventInfo = $libenroll->GET_EVENTINFO($_EnrolEventID);
			$OLE_setting = $libenroll->Get_Enrolment_Default_OLE_Setting('activity',(array)$_EnrolEventID);
			
			$numOfParticipant = $EventInfo['Approved'];
			$eventName = $EventInfo['EventTitle'];
			$eventDescript = $EventInfo['Description'];
			$organisationName = $OLE_setting[0]['Organization'];
			
			$dateString = '';
			$descriptionSting = '';
			$dateArr = array();
			foreach((array)$meetingDateAssoArr[$_EnrolEventID] as $meetingDateInfo){
				$thisDate =  date('Y-m-d',strtotime($meetingDateInfo['ActivityDateStart']));
				$dateArr[] .= $thisDate;
			}
			if($numOfParticipant>0){
				$descriptionSting .= $numOfParticipant.$cust_lang['a'].$cust_lang['student'].$cust_lang['participate'];
			}
			else{
				$descriptionSting .= $cust_lang['student'].$cust_lang['participate'];
			}
			if(!empty($organisationName)){
				$descriptionSting .= $cust_lang['by'].$organisationName.$cust_lang['hold'].$eventName;
			}
			else{
				$descriptionSting .= $eventName;
			}
			if(!empty($eventDescript)){
				$descriptionSting .= $cust_lang['comma'].$eventDescript.$cust_lang['fullstop'];
			}
			else{
				$descriptionSting .= $cust_lang['fullstop'];
			}
			$activity_html .= '<tr><td width="20%">'.$libenroll->displayDateInText($dateArr).'</td><td width="80%">'.$descriptionSting.'</td></tr>';
		}
		$activity_html .= '</table>';
		
		// -- With Award Activity --//
		$studentAwardInfoArr = $libenroll->getEventAwardToStudent($haveAwardEventIDArr);
		foreach((array)$studentAwardInfoArr as $_awardInfoArr){
			$_EventAwardID = $_awardInfoArr['EventAwardID'];
			$_EnrolEventID = $_awardInfoArr['EnrolEventID'];
			$_AwardID = $_awardInfoArr['AwardID'];
			
			$studentAwardAssoArr[$_EnrolEventID][$_EventAwardID][$_AwardID][] = $_awardInfoArr;
		}
		
		$awardTitleArr = $libenroll->GET_ENROL_AWARD();
		$awardTitleAssoArr = BuildMultiKeyAssoc($awardTitleArr,'AwardID');
		
		$Aarray = array();
		$award_html .= '<table width="80%" align="center" border="1" style=" border-collapse: collapse;">';
		foreach((array)$studentAwardAssoArr as $_EventID => $_EventAwardArr){
			// same event different award
			$EventInfo = $libenroll->GET_EVENTINFO($_EventID);
			$OLE_setting = $libenroll->Get_Enrolment_Default_OLE_Setting('activity',(array)$_EventID);
			
			$numOfParticipant = $EventInfo['Approved'];
			$eventName = $EventInfo['EventTitle'];
			// remove all the content in []
			$eventName = preg_replace('/\[[^\]]*\]/','',$eventName);
			$OLE_setting['Organization'];
			
			$String = '';
			//	$String .= $numOfParticipant.$cust_lang['a'].$cust_lang['student'].$cust_lang['participate'];
			$String .= $cust_lang['student'].$cust_lang['participate'];
			$organisationName = $OLE_setting[0]['Organization'];
			if(!empty($organisationName)){
				$String .= $cust_lang['by'].$organisationName.$cust_lang['hold'];
			}
			$String .= $eventName;
			$String .= $cust_lang['comma'];
			$String .= $cust_lang['get'];
			
			$i = 0;
			$_numTypeAward = count($_EventAwardArr);
			foreach($_EventAwardArr as $__AwardArr){
				// Same award different user
				$i++;
				if($i == $_numTypeAward){
					$Punctuation = $cust_lang['fullstop'];
				}
				else{
					if($i == ($_numTypeAward-1)){
						$Punctuation = $cust_lang['and'];
					}
					else{
						$Punctuation = $cust_lang['comma2'];						
					}
				}
				foreach($__AwardArr as $___AwardID => $___UserAwardInfoArr){
					// UserAward Arr
					$isGroupAward = $___UserAwardInfoArr[0]['IsGroupAward'];
					$GroupName = $___UserAwardInfoArr[0]['GroupName'];
					$Division = $___UserAwardInfoArr[0]['Division'];
					$numAward = count($___UserAwardInfoArr);
					$AwardName = $awardTitleAssoArr[$___AwardID]['AwardNameCh'];
					
					if(empty($Division)){
						$String .= $numAward.$cust_lang['item'].$AwardName.$Punctuation;
					}
					else{				
						$String .= $Division.$AwardName.$Punctuation;
					}
					if($isGroupAward){
						$String = $GroupName.$String;
					}
					
					
				}
			}
			
			$dateArr = array();
			foreach((array)$meetingDateAssoArr[$_EventID] as $_meetingDatesArr){
				$thisDate =  date('Y-m-d',strtotime($_meetingDatesArr['ActivityDateStart']));
				$dateArr[] .= $thisDate;
			}
			
			$award_html .= '<tr><td width="20%">'.$libenroll->displayDateInText($dateArr).'</td><td width="80%">'.$String.'</td></tr>';
			
		}
		$award_html .= '</table>';
		$report_content = $activity_html.'<br />'.$award_html;
	}
	$report_title = $Lang['eEnrolment']['AwardMgmt']['twghwfns']['IMC'].'('.$startDate.' - '.$endDate.')';
}
else if($_POST['ReportType'] == 3){
    include_once($PATH_WRT_ROOT."includes/libuser.php");
    
	$categoryArr = $libenroll->GET_CATEGORY_LIST();
	$haveAwardEventIDArr = array_unique(Get_Array_By_Key($studentAwardInfoArr,'EnrolEventID'));
	
	// get meeting date info
	$meetingDateInfoAssoArr = BuildMultiKeyAssoc($libenroll->Get_Activity_Meeting_Date($haveAwardEventIDArr), 'EnrolEventID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
	
	// get PIC data
	$picUserAry = $libenroll->GET_EVENTSTAFF($haveAwardEventIDArr, $ParType='PIC');
	$picUserAssoAry = BuildMultiKeyAssoc($picUserAry, 'EnrolEventID', $IncludedDBField=array(), $SingleValue=0, $BuildNumericArray=1);
	$picUserIdAry = Get_Array_By_Key($picUserAry, 'UserID');
	$picUserObj = new libuser('','',$picUserIdAry);
	
	
	// STUDENT AWARD DATA
	$studentAwardInfoArr = $libenroll->getEventAwardToStudent($haveAwardEventIDArr);
	$userGetAwardIdArr = array_unique(Get_Array_By_Key($studentAwardInfoArr,'UserID'));
	foreach((array)$studentAwardInfoArr as $_awardInfoArr){
		$_EventAwardID = $_awardInfoArr['EventAwardID'];
		$_EnrolEventID = $_awardInfoArr['EnrolEventID'];
		$_AwardID = $_awardInfoArr['AwardID'];
		
		$studentAwardAssoArr[$_EnrolEventID][$_EventAwardID][$_AwardID][] = $_awardInfoArr;
	}
	
	if(count($studentAwardAssoArr)<1){
		$report_content = '<p class="spacer"></p>';
		$report_content .= '<p class="spacer"></p>';
		$report_content .= '<p align="center">'.$eEnrollment['no_record'].'</p>';
	}
	else{
		// Get All student Name
		include_once($PATH_WRT_ROOT."includes/libuser.php");
		$luser = new libuser('','',$userGetAwardIdArr);
		
		$sql = "SELECT EnrolEventID ,EventCategory FROM INTRANET_ENROL_EVENTINFO WHERE EnrolEventID IN ('".implode("','",$haveAwardEventIDArr)."')";
		$eventCategoryArr = $libenroll->returnResultSet($sql);
		foreach((array) $eventCategoryArr as $_eventCategoryInfoArr){
			$_CatID = $_eventCategoryInfoArr['EventCategory'];
			$eventCategoryAssoArr[$_CatID][] = $_eventCategoryInfoArr;
		}
		
		$categoryAssoArr = BuildMultiKeyAssoc($categoryArr,'CategoryID');
		$schoolMag_html = '';
		foreach((array) $categoryArr as $_categoryInfoArr){
			$_eventCatID = $_categoryInfoArr['CategoryID'];
			if(!empty($eventCategoryAssoArr[$_eventCatID])){
				$schoolMag_html .= '<table align="center" width="600" ><tr><td>'.$categoryAssoArr[$_eventCatID]['CategoryName'].'</td></tr></table>';
				$schoolMag_html .= '<table align="center" width="600" border="1" style=" border-collapse: collapse;">
									<tr>
                                    <td width="10%">日期</td>
									<td width="15%">比賽</td>
									<td width="15%">合作機構</td>
									<td width="15%">分組</td>
									<td width="15%">獎項</td>
									<td width="19%">學生名稱</td>
                                    <td width="11%">負責老師</td>
									</tr>';
				foreach((array) $eventCategoryAssoArr[$_eventCatID] as $__eventCat ){
					$__EnrolEventID = $__eventCat['EnrolEventID'];
					$EventInfo = $libenroll->GET_EVENTINFO($__EnrolEventID);
					
					$__meetingDate = substr($meetingDateInfoAssoArr[$__EnrolEventID][0]['ActivityDateStart'], 0, 10);
					
					$__picUserIdAry = Get_Array_By_Key($picUserAssoAry[$__EnrolEventID], 'UserID');
					$__numOfPic = count($__picUserIdAry);
					$__tmpPicNameAry = array();
					for ($i=0; $i<$__numOfPic; $i++) {
					    $___picUserId = $__picUserIdAry[$i];
					    
					    $picUserObj->LoadUserData($___picUserId);
					    $__tmpPicNameAry[] = Get_Lang_Selection($picUserObj->ChineseName, $picUserObj->EnglishName);
					}
					$__picNameDisplay = implode(', ', (array)$__tmpPicNameAry);
					
					
					// remove all the content in []
					$eventName = preg_replace('/\[[^\]]*\]/','',$EventInfo['EventTitle']);
					$OLE_setting = $libenroll->Get_Enrolment_Default_OLE_Setting('activity',(array)$__EnrolEventID);
					if(!empty($studentAwardAssoArr[$__EnrolEventID])){
						$__numRowOfEvent = count($studentAwardAssoArr[$__EnrolEventID]);
						$__count = 0;
						foreach((array) $studentAwardAssoArr[$__EnrolEventID] as $___awardArr){
							// same event different award
							foreach((array)$___awardArr as $____studentAwardInfoArr){
								// same award different user
								$_____userIdArr = array();
								foreach((array)$____studentAwardInfoArr as $_____awardInfo){
									$_____awardInfo['AwardNameCh'];
									$_____awardInfo['Division'];
									$_____awardInfo['IsGroupAward'];
									$_____awardInfo['GroupName'];
									$luser->LoadUserData($_____awardInfo['UserID']);
									if(!empty($luser->ChineseName)){
										$_____userIdArr[] = $luser->ChineseName;
									}
									else{
										$_____userIdArr[] = $luser->EnglishName;
									}
									if($_____awardInfo['IsGroupAward'] == 1){
										$displayName = $_____awardInfo['GroupName'];
									}
									else{
										$displayName = implode($cust_lang['comma2'],$_____userIdArr);
									}
								}
							}
						$__count ++;
						if($__count == 1){
							// first row of same event
							$schoolMag_html .= '<tr>';
							$schoolMag_html .= '<td rowspan="'.$__numRowOfEvent.'">'.$__meetingDate.'</td>';
							$schoolMag_html .= '<td rowspan="'.$__numRowOfEvent.'">'.$eventName.'</td>';
							$schoolMag_html .= '<td rowspan="'.$__numRowOfEvent.'">'.$OLE_setting[0]['Organization'].'</td>';
							$schoolMag_html .= '<td>'.$_____awardInfo['Division'].'</td>';
							$schoolMag_html .= '<td>'.$_____awardInfo['AwardNameCh'].'</td>';
							$schoolMag_html .= '<td>'.$displayName.'</td>';
							$schoolMag_html .= '<td rowspan="'.$__numRowOfEvent.'">'.$__picNameDisplay.'</td>';
							$schoolMag_html .= '</tr>';
						}
						else{
							$schoolMag_html .= '<tr>';
							$schoolMag_html .= '<td>'.$_____awardInfo['Division'].'</td>';
							$schoolMag_html .= '<td>'.$_____awardInfo['AwardNameCh'].'</td>';
							$schoolMag_html .= '<td>'.$displayName.'</td>';
							$schoolMag_html .= '</tr>';
						}
						
						}
					}
				}
				$schoolMag_html .= '</table>';
			}
			else{
				continue;
			}
		}
		
		$report_content = $schoolMag_html;
	}
	$report_title = $Lang['eEnrolment']['AwardMgmt']['twghwfns']['MagazineReport'].' ('.$startDate.' - '.$endDate.')';
}

$backBtn .= '<div class="edit_bottom_v30">';
$backBtn .= '<p class="spacer"></p>';
$backBtn .=  $linterface->GET_ACTION_BTN($Lang['Btn']['Back'], "button", "goBack();","CancelBtn"," class='formbutton' onMouseOver=\"this.className='formbuttonon'\" onMouseOut=\"this.className='formbutton'\"");
$backBtn .= '<p class="spacer"></p>';
$backBtn .= '</div>';
?>
<table width="100%">
	<tr>
		<td align="center"><?=$report_title?></td>
	</tr>
	<tr>
		<td align="center"><?=$report_content?></td>
	</tr>
	<tr>
		<td><?=$backBtn?></td>
	</tr>
</table> 
<?
$linterface->LAYOUT_STOP();
?>
