<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'ClassTeacher');

$headerInfoAry = $libenroll_report->getClassMembershipReportHeaderAry();
$headerAry = $headerInfoAry['title'];

$dataAry = $libenroll_report->getClassMembershipReportResultExportAry($_SESSION['UserID']);


$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "class_membership.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>