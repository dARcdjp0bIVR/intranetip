<?php
// using: 

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");

intranet_auth();
intranet_opendb();

# user access right checking
$linterface = new interface_html();
$libenroll = new libclubsenrol();
$lreport = new libclubsenrol_report();
$libenroll->hasAccessRight($_SESSION['UserID'], 'ClassTeacher');


$CurrentPage = "report_classMembership";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($eEnrollmentMenu['class_membership'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
$linterface->LAYOUT_START();


# action button
$x = '';
$x .= '<div id="contentToolDiv" class="content_top_tool">'."\n";
	$x .= '<div class="Conntent_tool">'."\n";
		$x .= $linterface->Get_Content_Tool_v30('export', 'javascript:goExport();');
		$x .= $linterface->Get_Content_Tool_v30('print', 'javascript:goPrint();');
	$x .= '</div>'."\n";
$x .= '</div>'."\n";
$htmlAry['contentToolDiv'] = $x;


# report result table
$htmlAry['reportResultDiv'] = '<div id="reportResultDiv">'.$lreport->getClassMembershipReportResultTableHtml($_SESSION['UserID']).'</div>';

?>
<script type="text/javascript">
$(document).ready(function () {
});

function goExport() {
	$('form#form1').attr('target', '_self').attr('action', 'export.php').submit();
}

function goPrint() {
	$('form#form1').attr('target', '_blank').attr('action', 'print.php').submit();
}
</script>
<br />
<form id="form1" name="form1" method="post">
	<?=$htmlAry['contentToolDiv']?>
	<br />
	<br />
	
	<?=$htmlAry['reportResultDiv']?>
	<br />
</form>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>