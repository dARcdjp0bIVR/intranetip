<?php
# Modifying By: Rita

/************************************
 *	Date:		2013-04-19 (Rita) 
 * 	Details: 	Create this page
 ***********************************/

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
//include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

# authentication and open db
intranet_auth();
intranet_opendb();

# declare common using library
$li = new libdb();
$linterface = new interface_html();
$libclubsenrol = new libclubsenrol();


if($Action=="CategoryElements")
{
	$elements = array();
	
	//$tempGroupInfo = $libclubsenrol->getGroupInfoforSel($categoryID, $AcademicYearID);
	$tempGroupInfo = $libclubsenrol->GET_EVENTINFO_LIST($categoryID, '', $AcademicYearID);
	foreach ($tempGroupInfo as $k => $d)
	{
			$elements[$d['EnrolEventID']] = $d['EventTitle'];
	}
	$multipleSelectionBox = $linterface->Get_Multiple_Selection_Box($elements, "activityCategory", "activityCategory[]", 10,0, explode(",",$selectedClubs));
	$x = $multipleSelectionBox;
}
if($Action=="Retrieve_Details")
{
	//$title_display = $intranet_session_language=="en" ? "b.Title":"b.TitleChinese";
	$sql ="
		select 
			e.ActivityDateStart, b.EventTitle, TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart) as hours
		from 
			INTRANET_ENROL_EVENTSTUDENT as c
			inner join INTRANET_USER as d on (d.UserID=c.StudentID and d.RecordType=2 and c.RecordStatus = 2)
			inner join INTRANET_ENROL_EVENTINFO as b on (b.EnrolEventID=c.EnrolEventID)
			inner join INTRANET_ENROL_EVENT_DATE as e on (e.EnrolEventID=c.EnrolEventID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (e.ActivityDateStart >= '$SQL_startdate' and e.ActivityDateStart <= '$SQL_enddate'))
			inner join INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=e.EventDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
		where
			c.EnrolEventID in ($selectedClub)
			and c.StudentID = $studentID
		order by
			e.ActivityDateStart
		";		
		$result = $libclubsenrol->returnArray($sql);
				
		if(!empty($result))
		{
			$display .= "<a href='#' onClick='javascript:click_show_details($studentID);' class='close_layer_v30'>". $Lang['Btn']['Close'] ."</a><p class='spacer'></p>";

			$display .= "<table class='common_table_list_v30 view_table_list_v30'>";
			$display .= "<tr>";
			$display .= "<th width='100'>". $Lang['General']['Date'] ."</th>";
			$display .= "<th>$i_ClubsEnrollment_GroupName</th>";
			$display .= "<th width='100'>". $Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"] ."</th>";
			$display .= "</tr>";
			
			foreach($result as $k1=>$d1)
			{
				list($this_date, $this_clubname, $this_hours) = $d1;
				$this_date = substr($this_date,0,10);
				$this_hr = $libclubsenrol->timeToDecimal($this_hours);
				$this_class_total_hours += $this_hr;
				
				$display .= "<tr>";
				$display .= "<td>$this_date</td>";
				$display .= "<td>$this_clubname</td>";
				$display .= "<td>$this_hr</td>";
				$display .= "</tr>";
			}
				
			$display .= "<tr>";
			$display .= "<td colspan='2' align='right'>$list_total</td>";
			$display .= "<td>$this_class_total_hours</td>";
			$display .= "</tr>";	
		}
		
	$x = $display;
}

echo $x;






















exit;
# initial parameters from previous page [Post/Get]
$Action = $Action;
$returnType = $returnType;
$userid=$userid;
$yearclassid=$yearclassid;
// Period
$radioPeriod = $radioPeriod;
$selectYear = $selectYear;
$selectSemester = $selectSemester;
$textFromDate = $textFromDate;
$textToDate = $textToDate;
// Target
$rankTarget = $rankTarget;
$rankTargetDetail = explode(",",$rankTargetDetail);
// Nature
$nature = $nature;
// Activity Category
$sel_Category = $sel_Category;
$activityCategory = $activityCategory;
// Times
$times = $times;
$timesRange = $timesRange;
// Hours
$hours = $hours;
$hoursRange = $hoursRange;
// Display Unit
$displayUnit = $displayUnit;
// Sort By
$sortBy = $sortBy;
$sortByOrder = $sortByOrder;

# Program Action Switching
switch ($Action) {
	case "ActivityCategoryElements":
		$IS_CDATA = true;
		$IS_ESC_HTML = false;
		$XML = generateXML(generateActiveCategoryElementsContent($sel_Category, $nature, $returnType), $IS_CDATA, $IS_ESC_HTML);
		break;
	case "GenerateActivityParticipantReport":
		$IS_CDATA = true;
		$IS_ESC_HTML = false;
		$XML = generateXML(
				generateActivityParticipationReportContent(
					$sel_Category, $returnType, $radioPeriod,
					$selectYear,$selectSemester,$textFromDate,$textToDate,
					$rankTarget, $rankTargetDetail,
					$activityCategory, $nature, $times,
					$timesRange, $hours, $hoursRange,
					$displayUnit, $sortBy, $sortByOrder,
					$returnType
				),
				$IS_CDATA, $IS_ESC_HTML
			);
		break;
	case "ActivityPrint":
		$IS_CDATA = true;
		$IS_ESC_HTML = false;
		$XML = generateXML(
				generateActivityParticipationReportContent(
					$sel_Category, $returnType, $radioPeriod,
					$selectYear,$selectSemester,$textFromDate,$textToDate,
					$rankTarget, $rankTargetDetail,
					$activityCategory, $nature, $times,
					$timesRange, $hours, $hoursRange,
					$displayUnit, $sortBy, $sortByOrder,
					$returnType, "Print"
				),
				$IS_CDATA, $IS_ESC_HTML
			);
		break;
	case "GetStudentInfo":
		$IS_CDATA = true;
		$IS_ESC_HTML = false;
		$XML = generateXML(
				generateStudentInfoContent(
					$sel_Category, $returnType, $radioPeriod,
					$selectYear,$selectSemester,$textFromDate,$textToDate,
					$rankTarget, $rankTargetDetail,
					$activityCategory, $nature, $times,
					$timesRange, $hours, $hoursRange,
					$displayUnit, $sortBy, $sortByOrder,
					$returnType, $userid, $yearclassid
				),
				$IS_CDATA, $IS_ESC_HTML
			);
		break;
	default:
		break;
}
header("Content-Type:   text/xml");
echo $XML;


//////////////////////////////// FUNCTIONS ////////////////////////////////////
### CORE FUNCTIONS ###
function generateActiveCategoryElementsContent($ParCategoryId=-1, $ParIsSchoolActivity=-1, $ParReturnType="Html") {
	global $linterface;

	if (empty($ParCategoryId) || $ParCategoryId==-1) {
		$elements = array();
	} else {
		$elements = getActiveCategoryElements($ParCategoryId, $ParIsSchoolActivity);
		$elements = turnArrayWithKeyValuePair($elements);
	}
	if ($ParReturnType == "Html") {
		$multipleSelectionBox = $linterface->Get_Multiple_Selection_Box($elements, "activityCategory", "activityCategory[]", 10);
		$multipleSelectionBox = str_replace("style=\"width:99%\"", "", $multipleSelectionBox);	//The box set the style =>  style="width:99%" , which will limit the display width in I.E.
		$returnArray = array(array("activityCategory",$multipleSelectionBox));
	} else {
		// do nothing
	}
	return $returnArray;
}

function generateActivityParticipationReportContent(
													$ParSel_Category, $ParReturnType, $ParRadioPeriod,
													$ParSelectYear,$ParSelectSemester,$ParTextFromDate,$ParTextToDate,
													$ParRankTarget, $ParRankTargetDetail,
													$ParActivityCategory, $ParNature, $ParTimes,
													$ParTimesRange, $ParHours, $ParHoursRange,
													$ParDisplayUnit, $ParSortBy, $ParSortByOrder,
													$ParReturnType, $ParOutputType="Normal"
													) {
	global $libclubsenrol;
	# get the result elements
	// S: 201003100933
//	$elements = $libclubsenrol->activityParticipationReportElements(1,
//													$ParRadioPeriod,
//													$ParSelectYear,$ParSelectSemester,$ParTextFromDate,$ParTextToDate,
//													$ParRankTarget, $ParRankTargetDetail, $ParDisplayUnit,
//													$ParSel_Category, $ParActivityCategory, $ParNature, $ParSortBy,
//													$ParSortByOrder
//													);
	$elements = $libclubsenrol->activityParticipationReportElements(1,
													$ParRadioPeriod,
													$ParSelectYear,$ParSelectSemester,$ParTextFromDate,$ParTextToDate,
													$ParRankTarget, $ParRankTargetDetail, 0,
													$ParSel_Category, $ParActivityCategory, $ParNature, $ParSortBy,
													$ParSortByOrder
													);
	// E: 201003100933

	# construct the table
	if (empty($elements[0]["CATEGORYNAME"])) {
		$categoryList = $libclubsenrol->GET_CATEGORY_LIST();
		foreach($categoryList as $key=>$catElement) {
			if ($catElement["CategoryID"] == $ParSel_Category) {
				$categoryName = $catElement["CategoryName"];
			}
		}
	} else {
		$categoryName = $elements[0]["CATEGORYNAME"];
	}
	if ($ParOutputType=="Normal") {
		$tableShell = constructTableShell($ParDisplayUnit, $categoryName);
	} else {
		$tableShell = constructTableShellPrint($ParDisplayUnit, $categoryName);
	}
	
	$outputHtml = getTableContent($ParDisplayUnit, $elements, $ParTimesRange, $ParTimes, $ParHoursRange, $ParHours,$tableShell, $ParOutputType);

	$returnArray = array(array("activityParticipationReport", $outputHtml));
	return $returnArray;
}

function generateStudentInfoContent(
									$ParSel_Category, $ParReturnType, $ParRadioPeriod,
									$ParSelectYear,$ParSelectSemester,$ParTextFromDate,$ParTextToDate,
									$ParRankTarget, $ParRankTargetDetail,
									$ParActivityCategory, $ParNature, $ParTimes,
									$ParTimesRange, $ParHours, $ParHoursRange,
									$ParDisplayUnit, $ParSortBy, $ParSortByOrder,
									$ParReturnType, $ParUserId, $ParYearClassId, $ParOutputType="Normal"
									) {
	global $libclubsenrol;
	$elements = $libclubsenrol->activityParticipationReportElements(1,
													$ParRadioPeriod,
													$ParSelectYear,$ParSelectSemester,$ParTextFromDate,$ParTextToDate,
													$ParRankTarget, $ParRankTargetDetail, $ParDisplayUnit,
													$ParSel_Category, $ParActivityCategory, $ParNature, $ParSortBy,
													$ParSortByOrder, true, $ParUserId, $ParYearClassId
													);

	$content = generateStudentTable($elements, $ParTimesRange, $ParTimes, $ParHoursRange, $ParHours);

	$returnArray = array(array("studentAttendanceInfo", $content));

	return $returnArray;
}

### MISC FUNCTIONS ###
function getLayerTable() {
	global $image_path, $LAYOUT_SKIN;
	$table .= "
<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"layer_table\">
      <tbody>
        <tr>
          <td height=\"19\">
            <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
              <tbody>
                <tr>
                  <td width=\"5\" height=\"19\"><img width=\"5\" height=\"19\" src=\"$image_path/$LAYOUT_SKIN/can_board_01.gif\" alt=\"Image\" /></td>
                  <td valign=\"middle\" height=\"19\" background=\"$image_path/$LAYOUT_SKIN/can_board_02.gif\">&nbsp;</td>
                  <td width=\"19\" height=\"19\"><a onclick=\"MM_showHideLayers('studentDisplay{{{ LAYER ID }}}','','hide')\" href=
                    \"javascript:;\"><img width=\"19\" height=\"19\" border=\"0\" onmouseout=
                    \"MM_swapImgRestore()\" onmouseover=
                    \"MM_swapImage('pre_close21','','$image_path/$LAYOUT_SKIN/can_board_close_on.gif',1)\" id=
                    \"pre_close21\" name=\"pre_close21\" src=\"$image_path/$LAYOUT_SKIN/can_board_close_off.gif\"
                    alt=\"Image\" /></a></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">
              <tbody>
                <tr>
                  <td width=\"5\" background=\"$image_path/$LAYOUT_SKIN/can_board_04.gif\"><img width=\"5\" height=\"19\" src=\"$image_path/$LAYOUT_SKIN/can_board_04.gif\" alt=\"Image\" /></td>
                  <td bgcolor=\"#FFFFF7\">
                    <div class=\"c1\">
                      {{{ LAYER TABLE DETAIL }}}
                    </div><br />
                  </td>
                  <td width=\"6\" background=\"$image_path/$LAYOUT_SKIN/can_board_06.gif\"><img width=\"6\" height=\"6\" src=\"$image_path/$LAYOUT_SKIN/can_board_06.gif\" alt=\"Image\" /></td>
                </tr>
                <tr>
                  <td width=\"5\" height=\"6\"><img width=\"5\" height=\"6\" src=\"$image_path/$LAYOUT_SKIN/can_board_07.gif\" alt=\"Image\" /></td>
                  <td height=\"6\" background=\"$image_path/$LAYOUT_SKIN/can_board_08.gif\"><img width=\"5\" height=\"6\" src=\"$image_path/$LAYOUT_SKIN/can_board_08.gif\" alt=\"Image\" /></td>
                  <td width=\"6\" height=\"6\"><img width=\"6\" height=\"6\" src=\"$image_path/$LAYOUT_SKIN/can_board_09.gif\" alt=\"Image\" /></td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
";
return $table;
}

function generateStudentTable($ParElements, $ParTimesRange, $ParTimesLimit, $ParHoursRange, $ParHoursLimit) {
//	debug_r($ParElements);
	global $Lang, $libclubsenrol;
	$className = $ParElements[0]["CLASSTITLE"];
	$classYearId = $ParElements[0]["YEARCLASSID"];
	$studentName = $ParElements[0]["NAME"];
	$studentNumber = $ParElements[0]["CLASSNUMBER"];
	
	$display .= "<table border=\"0\" width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" class=\"layer_table_detail eSporttableborder\">\n";
	$display .= "<tr>";
	$display .= "<td colspan=\"3\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$className." ".$studentName." "."(" . $studentNumber . ")"."</td>";
	$display .= "</tr>";
	$sizeOfElements = count($ParElements);
	$css_row = 0;
	$totalHour = 0;
//	debug_r($ParElements);
	for($i=0;$i<$sizeOfElements;$i++) {
		$elements = $ParElements[$i];
		$HOURS = $libclubsenrol->timeToDecimal($elements["HOURS"]);
		if ($ParTimesRange==0 && $ParElements[$i]["TIMES"] < $ParTimesLimit) {	// on or above
    		continue;
    	} else if ($ParTimesRange==1 && $ParElements[$i]["TIMES"] > $ParTimesLimit) {	// on or below
    		continue;
    	} else {
    		// do nothing
    	}
    	if ($ParHoursRange==0 && $HOURS < $ParHoursLimit) {	// on or above
    		continue;
    	} else if ($ParHoursRange==1 && $HOURS > $ParHoursLimit) {	// on or below
    		continue;
    	} else {
    		// do nothing
    	}
		
		
		$tr_css = ($css_row++ % 2 == 1) ? "tablebluerow2" : "tablebluerow1";
		
		$display .= "<tr class='$tr_css'>";
		$display .= "
						<td nowrap=\"true\" class=\"tabletext\" align=\"left\" valign=\"top\">" . date('Y-m-d',strtotime($elements["ACTIVITYDATESTART"])) . "</td>
						<td nowrap=\"true\" class=\"tabletext\" align=\"left\" valign=\"top\">" . $elements["EVENTTITLE"] . "</td>
						<td nowrap=\"true\" class=\"tabletext\" align=\"left\" valign=\"top\">" . $HOURS . " " . $Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] . "</td>
					";
		$display .= "</tr>";
		$totalHour += $HOURS;
	}
	$display .= "<tr>
					<td style='text-align:right' class='tablebluebottom' colspan='2'>" . $Lang["eEnrolment"]["ActivityParticipationReport"]["Total"] . "</td>
					<td class='tablebluebottom'>$totalHour " . $Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] . "</td>
				</tr>";
	$display .= "</table>";
	$layerTable = getLayerTable();
	$layerTable = str_replace("{{{ LAYER ID }}}", $classYearId, $layerTable);
	$display = str_replace("{{{ LAYER TABLE DETAIL }}}", $display, $layerTable); 
	return $display;
}

function getPrintExportLink() {
	global $linterface;
	# export parameters
    $type = 6;
	$export_parameters = "type=$type&category_conds=$category_conds&name_conds=$name_conds";	
	
	$table = '
			<table width="96%" border="0" cellspacing="0" cellpadding="5" align="center">
				<tr><td valign="top" width="25%">
					'.$linterface->GET_LNK_PRINT("javascript:jsGoPrint()", '', '', '', '', 0).'
				</td>
				<td valign="top">
					' . $linterface->GET_LNK_EXPORT("javascript:jsGoExport()", '', '', '', '', 0) . '
				</td>
				</tr>
			</table>
';
	return $table;
}

function getTableContent($ParDisplayUnit, $ParElements, $ParTimesRange=0, $ParTimesLimit=0, $ParHoursRange=0, $ParHoursLimit=0, $ParTableShell="",$ParOutputType="Normal") {
	
	global $i_no_record_exists_msg, $Lang, $libclubsenrol;
	$numberOfColumn = $ParTableShell["NUMBEROFCOLUMN"];
	$tableShellHtml = $ParTableShell["HTML"];
	$sizeOfElements = sizeof($ParElements);

	if ($ParOutputType == "Normal") {
		$printExportLink = getPrintExportLink();
	} else {
		// do nothing
	}
	
	# $sizeOfElements > 0
	# if DisplayUnit == 0
		# categorize elements
		# for each category, generate one table
	# else
		# generate one table
	if ($sizeOfElements > 0) {
		if ($ParDisplayUnit == 0) {
			$returnedElements = $libclubsenrol->categorizeElements($ParElements);
			$sizeOfRE = count($returnedElements);
			if (is_array($returnedElements) && $sizeOfRE> 0) {
				foreach($returnedElements as $key=>$elements) {
					$display .= generateOneTable($elements, $ParTimesRange, $ParTimesLimit, $ParHoursRange, $ParHoursLimit, $ParOutputType, $ParDisplayUnit, $numberOfColumn, $tableShellHtml);
					if (empty($display)) {
						// do nothing
					} else {
						$studentDisplaySpan = "<div onLoad=\"moveObject('studentDisplay" . $elements[0]["YEARCLASSID"] . "', true);\" id=\"studentDisplay" . $elements[0]["YEARCLASSID"] . "\" style=\"background-color:white;display:block; position:absolute; width:400px; \" ></div><br />";
						$display .= $studentDisplaySpan;
					}
				}
			}
			
			$libenroll_ui = new libclubsenrol_ui();
			$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
			$display .= "<br/>$RemarksTable";
		} else {
			// S: 201003100933
//			$display = generateOneTable($ParElements, $ParTimesRange, $ParTimesLimit, $ParHoursRange, $ParHoursLimit, $ParOutputType, $ParDisplayUnit, $numberOfColumn, $tableShellHtml);
			# filter and sum elements $ParElements
			$returnedElements = $libclubsenrol->categorizeElements($ParElements);
			$sizeOfRE = count($returnedElements);
			if (is_array($returnedElements) && $sizeOfRE> 0) {
				foreach($returnedElements as $key=>$elements) {
					$tempArray = $libclubsenrol->filterForDisplayUnitClass($elements, $ParTimesRange, $ParTimesLimit, $ParHoursRange, $ParHoursLimit);
					if (isset($tempArray)) {
						$classElement[] = $tempArray;
					} else {
						// do nothing
					}
				}
			}
//			debug_r($classElement);die;
			# generate one table
			$display = generateOneTable($classElement, 0, 0, 0, 0, $ParOutputType, $ParDisplayUnit, $numberOfColumn, $tableShellHtml);
			// E: 201003100933			
		}

		if (empty($display)) {
		} else {
			$display = $printExportLink.$display;
		}
	}
	if ($sizeOfElements == 0 || empty($display))  {
		if ($ParOutputType == "Normal") {
		   	//construct table content
		   	$display .= "<tr><td align='center' colspan='$numberOfColumn' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n";
		   	
		} else if ($ParOutputType == "Print") {
			$display .= "<tr><td align='center' colspan='$numberOfColumn' class='eSporttdborder eSportprinttext' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n";
		} else {
			// do nothing
		}
		
		$display = $printExportLink.str_replace("{{{ CONTENT }}}", $display, $tableShellHtml)."<br />";
	}
	
	return $display;
}

function generateOneTable($ParElements, $ParTimesRange, $ParTimesLimit, $ParHoursRange, $ParHoursLimit, $ParOutputType, $ParDisplayUnit, $ParNumberOfColumn, $ParTableShellHtml) {
	global $Lang,$libclubsenrol;

	$sizeOfElements = count($ParElements);
	
	$css_row = 0;
    $totalHours = 0;

    for ($i=0; $i<$sizeOfElements; $i++)
    {
    	if ($ParDisplayUnit == 0) {
	    	$HOURS = $libclubsenrol->timeToDecimal($ParElements[$i]["HOURS"]);
	    	$TIMES = $ParElements[$i]["TIMES"];
	    	
	    	if ($ParTimesRange==0 && $TIMES < $ParTimesLimit) {	// on or above
	    		continue;
	    	} else if ($ParTimesRange==1 && $TIMES > $ParTimesLimit) {	// on or below
	    		continue;
	    	} else {
	    		// do nothing
	    	}
	    	if ($ParHoursRange==0 && $HOURS < $ParHoursLimit) {	// on or above
	    		continue;
	    	} else if ($ParHoursRange==1 && $HOURS > $ParHoursLimit) {	// on or below
	    		continue;
	    	} else {
	    		// do nothing
	    	}    		
    	} else {
    		$HOURS = $ParElements[$i]["HOURS"];
    	}
    	// E: 201003100933
    	
    	$acacemic_year = new academic_year(Get_Current_Academic_Year_ID());
    	
    	if ($acacemic_year->Check_User_Existence_In_Academic_Year($ParElements[$i]["USERID"])) {
    		$displayUserName = $ParElements[$i]["NAME"];
    	} else {
    		$displayUserName = "<span class=\"tabletextrequire\">^</span>".$ParElements[$i]["NAME"];
    	}
    	if ($ParOutputType == "Normal") {
		 	$tr_css = ($css_row++ % 2 == 1) ? "tablebluerow2" : "tablebluerow1";
		 				
			$display .= '<tr class="'.$tr_css.'">';
			if ($ParDisplayUnit == 0) {
				$display .= '
							<td class="tabletext" valign="top" nowrap=\"true\">'.$ParElements[$i]["CLASSTITLE"]. '</td>
							<td nowrap=\"true\" valign="top">'.$ParElements[$i]["CLASSNUMBER"].'</td>
			          	  	<td nowrap=\"true\" class="tablelink" align="left" valign="top"><a href="javascript: void(0)" onClick="moveObject(\'studentDisplay' . $ParElements[$i]["YEARCLASSID"] . '\', true, 280);getStudentInfo(\'' . $ParElements[$i]["USERID"] . '\', \'' . $ParElements[$i]["YEARCLASSID"] . '\'); MM_showHideLayers(\'studentDisplay' . $ParElements[$i]["YEARCLASSID"] . '\', \'\', \'show\');" class="tablelink">'.$displayUserName.'</a></td>
			          	  	<td nowrap=\"true\" class="tabletext" align="left" valign="top">'.$ParElements[$i]["TIMES"].'</td>
			          	  	<td nowrap=\"true\" class="tabletext" align="left" valign="top">'.$HOURS.' ' . $Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] . '</td>
			          	  	';
			} else {
				$display .= '
							<td class="tabletext" valign="top" nowrap=\"true\">'.$ParElements[$i]["CLASSTITLE"].'</td>
			          	  	<td nowrap=\"true\" class="tabletext" align="left" valign="top">'.$ParElements[$i]["TIMES"].'</td>
			          	  	<td nowrap=\"true\" class="tabletext" align="left" valign="top">'.$HOURS.' ' . $Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] . '</td>
			          	  	';
				
			}      	  	
	    	$display .= '</tr>';
    	} else if ($ParOutputType == "Print") {
    		$display .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">";
			if ($ParDisplayUnit == 0) {
				$display .= "
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $ParElements[$i]["CLASSTITLE"] . "&nbsp</td>
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $ParElements[$i]["CLASSNUMBER"] . "&nbsp</td>
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $displayUserName . "&nbsp</td>
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $ParElements[$i]["TIMES"] . "&nbsp</td>
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $HOURS . " " . $Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] . "</td>";
			} else {
				$display .= "
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $ParElements[$i]["CLASSTITLE"] . "&nbsp</td>
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $ParElements[$i]["TIMES"] . "&nbsp</td>
	        		<td nowrap=\"true\" class=\"eSporttdborder eSportprinttext\">" . $HOURS . " " . $Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] . "</td>
			          	  	";
				
			}  
			$display .= "</tr>";
    	} else {
    		// do nothing
    	}

	$totalHours += $HOURS;
	}

	if ($ParOutputType == "Print") {
		$style = "eSporttdborder eSportprinttabletitle";
	} else {
		$style = "tablebluebottom";
	}
		$totalShell = '
	<tr>
	<td style="text-align:right" class="' . $style . '" colspan="' . ($ParNumberOfColumn-1) . '"> {{{ TOTAL CAPTION }}} ' . $Lang["eEnrolment"]["ActivityParticipationReport"]["Total"] . ':</td>
	<td class="' . $style . '" >{{{ TOTAL HOURS }}} ' . $Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] . '</td>
	</tr>
	';
	
	if ($ParDisplayUnit == 0) {
		$totalCaption = $ParElements[0]["CLASSTITLE"];
	} else {
		$totalCaption = "";
	}

	if (empty($display)) {
		
	} else {
	$display .= str_replace("{{{ TOTAL HOURS }}}", $totalHours, $totalShell);
	$display = str_replace("{{{ TOTAL CAPTION }}}", $totalCaption, $display);
	$display = str_replace("{{{ CONTENT }}}", $display, $ParTableShellHtml)."<br />";
	}
	return $display;
}

function constructTableShellPrint($ParDisplayUnit, $ParCategoryName) {
	global $intranet_session_language, $Lang;
		//construct table title
	$display .= "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
		
	# caption
	$caption = array();
	if ($ParDisplayUnit == 0) {
		array_push($caption, "<td width=\"22%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["Class"]."</td>");
		array_push($caption, "<td width=\"10%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"]."</td>");
		array_push($caption, "<td width=\"34%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["Name"]."</td>");
		if ($intranet_session_language == "en") {
			$timesFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TimesFor"]." ".$ParCategoryName;
			$totalHoursFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"]." ".$ParCategoryName;
		} else {
			$timesFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TimesFor"];
			$totalHoursFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"];
		}
		array_push($caption, "<td width=\"22%\" class=\"eSporttdborder eSportprinttabletitle\">".$timesFor."</td>");
		array_push($caption, "<td width=\"22%\" class=\"eSporttdborder eSportprinttabletitle\">".$totalHoursFor."</td>");
	} else {
		array_push($caption, "<td width=\"50%\" class=\"eSporttdborder eSportprinttabletitle\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["Class"]."</td>");
		if ($intranet_session_language == "en") {
			$totalTimesFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TotalTimesFor"]." ".$ParCategoryName;
			$totalHoursFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"]." ".$ParCategoryName;
		} else {
			$totalTimesFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalTimesFor"];
			$totalHoursFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"];
		}
		array_push($caption, "<td width=\"25%\" class=\"eSporttdborder eSportprinttabletitle\">".$totalTimesFor."</td>");
		array_push($caption, "<td width=\"25%\" class=\"eSporttdborder eSportprinttabletitle\">".$totalHoursFor."</td>");
	}
			
	$display .= "<tr class=\"eSporttdborder eSportprinttabletitle\">";
	while($element = current($caption)) {
		$display .= $element;
		next($caption);
	}
	$display .= "</tr>";
	reset($caption);
	$display .= "{{{ CONTENT }}}";
	$display .= "</table>";
	$returnArray = array("NUMBEROFCOLUMN"=>count($caption), "HTML"=>$display);
	return $returnArray;
}

function constructTableShell($ParDisplayUnit, $ParCategoryName) {
	global $intranet_session_language, $Lang;
	
	$display = "<table border=\"0\" width=\"96%\" cellpadding=\"5\" cellspacing=\"0\">";
	
	# caption
	$caption = array();
	if ($ParDisplayUnit == 0) {
		array_push($caption, "<td width=\"22%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["Class"]."</td>");
		array_push($caption, "<td width=\"10%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"]."</td>");
		array_push($caption, "<td width=\"34%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["Name"]."</td>");
		if ($intranet_session_language == "en") {
			$timesFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TimesFor"]." ".$ParCategoryName;
			$totalHoursFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"]." ".$ParCategoryName;
		} else {
			$timesFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TimesFor"];
			$totalHoursFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"];
		}
		array_push($caption, "<td width=\"22%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$timesFor."</td>");
		array_push($caption, "<td width=\"22%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$totalHoursFor."</td>");
	} else {
		array_push($caption, "<td width=\"50%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$Lang["eEnrolment"]["ActivityParticipationReport"]["Class"]."</td>");
		if ($intranet_session_language == "en") {
			$totalTimesFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TotalTimesFor"]." ".$ParCategoryName;
			$totalHoursFor = $Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"]." ".$ParCategoryName;
		} else {
			$totalTimesFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalTimesFor"];
			$totalHoursFor = $ParCategoryName.$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"];
		}
		array_push($caption, "<td width=\"25%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$totalTimesFor."</td>");
		array_push($caption, "<td width=\"25%\" nowrap=\"true\" class=\"tablebluetop tabletopnolink\">".$totalHoursFor."</td>");
	}
	$display .= "<tr>";
	while($element = current($caption)) {
		$display .= $element;
		next($caption);
	}
	$display .= "</tr>";
	reset($caption);
	$display .= "{{{ CONTENT }}}";
	$display .= "</table>";
	$returnArray = array("NUMBEROFCOLUMN"=>count($caption), "HTML"=>$display);
	return $returnArray;
}

function turnArrayWithKeyValuePair($InArray) {
	if (isset($InArray) && count($InArray) > 0){
		while($element=current($InArray)) {
			list($key, $value) = $element;
			$returnArray[$key] = $value;
			next($InArray);		
		}
		reset($InArray);
		return $returnArray;
	} else {
		return array();
	}
}

function getActiveCategoryElements($ParCategoryId, $ParIsSchoolActivity) {
	global $li;
	$conditionArray = array();
	if ($ParCategoryId > 0) {
		$conditionArray[] = "
	EVENTCATEGORY = $ParCategoryId
		";
	} else {
		// do nothing
	}
	if ($ParIsSchoolActivity>-1) {
		$conditionArray[] = "
	SCHOOLACTIVITY = $ParIsSchoolActivity
		";
	} else {
		// do nothing
	}
	$cond = "";
	if ($conditionArray>0) {
		$cond = " WHERE ".implode(" AND ", $conditionArray);
	} else {
		// do nothing
	}

	$sql = "
SELECT ENROLEVENTID, EVENTTITLE
FROM INTRANET_ENROL_EVENTINFO
$cond
		";
	return $li->returnArray($sql);
}

/**
 * generate the output xml
 * @param	array	ParElementsArr	the child elements for the xml output, string [0] - child tag name, string [1] - child content
 * @return	xml	the xml to output
 */
function generateXML($ParElementsArr, $ParCData=false, $ParSpcChar=false) {
	$xml .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	$numofElements = count($ParElementsArr);
	for ($i=0;$i<$numofElements;$i++) {
		list($TAG_NAME, $CONTENT) = $ParElementsArr[$i];
		$x .= sxe($TAG_NAME, $CONTENT, $ParCData, $ParSpcChar);
	}
	$xml .= sxe("elements", $x);
	return $xml;
}

/**
 * sxe - simple xml embed with format <xx> yy </xx>
 * @param	string	ParTag	the tag name of an xml element
 * @param	string	ParValue	corresponding content for an element
 * @param	boolean	ParCData	quote the content with <![CDATA[ content ]]>	// already html format string
 * @param	boolean	ParSpcChar	escape special characters (single quote and double quotes)	// for strings to put into html
 * @return	string	xml tag
 */
function sxe($ParTag="element", $ParValue="content", $ParCData=false, $ParSpcChar=false) {
//	$quotes = array("\"","'");
//	$htmlEntities = array("&quot;","&#039;");
	return "<".$ParTag.">".($ParCData?"<![CDATA[":"").($ParSpcChar?intranet_htmlspecialchars($ParValue):$ParValue).($ParCData?"]]>":"")."</".$ParTag.">";
}

intranet_closedb();
?>