<?php
## Modifying By: 

/************************************
 *	Date:		2014-06-19 (Bill) - 2014-09-25 (Bill) 
 * 	Details: 	Modify layout
 * 	Date:		2014-08-11 (Bill)
 * 	Details:	Radio: Date range - academic year according to event date
 *  Date:		2014-06-19 (Bill) 
 * 	Details: 	Modify layout and add budget 
 *	Date:		2013-04-19 (Rita) 
 * 	Details: 	Create this page
 ***********************************/
 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
intranet_auth();
intranet_opendb();
$libuser = new libuser();
##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

$li = new libdb();
# Check User Access Right
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&& (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	# Check flag
	if(!$libenroll->enableCCHPWOutOfSchoolActivityApplicationForm()){
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
}

$linterface = new interface_html();
//debug_pr($_POST);
# Obtain Post Value
$rankTarget = $_POST['rankTarget'];
$rankTargetDetail = $_POST['rankTargetDetail'];
$studentID = $_POSR['studentID'];
$radioPeriod = $_POST['radioPeriod'];
$selectYear = $_POST['selectYear'];
$selectSemester = $_POST['selectSemester'];
$textFromDate = $_POST['textFromDate'];
$textToDate = $_POST['textToDate'];
$sel_category = $_POST['sel_category'];
$enrolEventID = $_POST['activityCategory'];
$studentID = $_POST['studentID'];
# prepare report data 
if ($radioPeriod == "YEAR") 
{
	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester);
	$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester);
	
	$academicYearId = $selectYear;
	
	# Radio: Year
	// All activities in the same academic year
	$academicYearObj = new academic_year($academicYearId);
	$academicYearName = $academicYearObj->Get_Academic_Year_Name();
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
	
	// Not affected by School Year Selection
	$selectYear = "";
	
//	$academicInfoAry = getAcademicYearAndYearTermByDate($textFromDate);
//	$academicYearId = $academicInfoAry['AcademicYearID'];
}
//$academicYearObj = new academic_year($academicYearId);
//$academicYearName = $academicYearObj->Get_Academic_Year_Name();
//debug_pr($SQL_startdate.$SQL_enddate);

$formIDArr = array();
$studentIDArr = array();

if($rankTarget == 'form'){
	$formIDArr = $rankTargetDetail;
}elseif($rankTarget =='class'){
	$studentIDArr = $libenroll->getTargetUsers($rankTarget,$rankTargetDetail, "vector");
}elseif($rankTarget =='student'){
	$studentIDArr = $studentID;
}

# Activity Date Info
$thisActivityDateInfoArr = $libenroll->GET_ENROL_EVENT_DATE($enrolEventID,$SQL_startdate,$SQL_enddate);
//debug_pr($libenroll->GET_ENROL_EVENT_DATE($enrolEventID,$SQL_startdate,$SQL_enddate));
$thisActivityDateInfoIDArrAssoc = BuildMultiKeyAssoc($thisActivityDateInfoArr, array('EnrolEventID','EventDateID'));
//debug_pr($thisActivityDateInfoIDArrAssoc);
$thisActivityDateInfoIDArr = array_keys($thisActivityDateInfoIDArrAssoc);	
//debug_pr($thisActivityDateInfoIDArr);
//debug_pr($selectYear);
//debug_pr($formIDArr);
//debug_pr($studentIDArr);
//debug_pr($sel_category);

# Activity Info 
if($thisActivityDateInfoIDArr){
	$activityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($thisActivityDateInfoIDArr,'',$selectYear,0,1,1,0,$formIDArr,'',true,$studentIDArr, $sel_category);
}

$data_ary = array();
$display = "";

//debug_pr($activityInfoArr);
foreach ((array)$activityInfoArr as $thisActivityID=>$thisEventInfo){
	## Variables for Display
	$thisActivityName = $thisEventInfo['EventTitle']?$thisEventInfo['EventTitle']:$Lang['General']['EmptySymbol'];
	
	// nature
	switch ($thisEventInfo['SchoolActivity']) {
		case 0:
			$_natureDisplay = $Lang['eEnrolment']['NonSchoolActivity'];
			break;
		case 1:
			$_natureDisplay = $Lang['eEnrolment']['SchoolActivity'];
			break;
		case 2:
			$_natureDisplay = $Lang['eEnrolment']['MixedActivity'];
			break;
		default:
			$_natureDisplay = $Lang['eEnrolment']['SchoolActivity'];
	}
	/* Comment: upper and lower age
	$thisUpperAge = $thisEventInfo['UpperAge'];
	$thisLowerAge = $thisEventInfo['LowerAge'];
	if($thisUpperAge!='0' && $thisLowerAge!='0'){
		$thisStudentAgeRange = $thisLowerAge . ' - ' .  $thisUpperAge;
	}else{
		$thisStudentAgeRange = $Lang['General']['EmptySymbol'];
	}
	*/ // Add $ 
	$thisPaymentAmount = ($thisEventInfo['PaymentAmount'] == '0'? $Lang['General']['EmptySymbol']:'$'.$thisEventInfo['PaymentAmount']);
	$thisBudgetAmount = (($thisEventInfo['BudgetAmount'] == '0' || $thisEventInfo['BudgetAmount'] == NULL)? $Lang['General']['EmptySymbol']:'$'.$thisEventInfo['BudgetAmount']);
	
	# Staff Info
	$activityStaffInfo = $libenroll->GET_EVENTSTAFF($thisActivityID);
	$numOfActivityStaffInfo = count($activityStaffInfo);
	
	// PIC first and then helper
	$thisPicIDArr = array();
	for($i=0;$i<$numOfActivityStaffInfo;$i++){
		$thisStaffType = $activityStaffInfo[$i]['StaffType'];	
		if($thisStaffType == 'PIC'){
			$thisPicIDArr[]= $activityStaffInfo[$i]['UserID'];
		}
	}
	for($i=0;$i<$numOfActivityStaffInfo;$i++){
		$thisStaffType = $activityStaffInfo[$i]['StaffType'];	
		if($thisStaffType == 'HELPER'){
			$thisPicIDArr[]= $activityStaffInfo[$i]['UserID'];
		}
	}
	
	# PIC
	$thisUserInfoArr =  $libuser->returnUser('','',$thisPicIDArr);
	
	$numOfUserInfoArr = count($thisUserInfoArr);
	$thisUserNameArr = array();
	$thisPICTelArr = array();
	for($i=0;$i<$numOfUserInfoArr;$i++){
		$thisUserChineseName = $thisUserInfoArr[$i]['ChineseName'];
		$thisUserEnglishName = $thisUserInfoArr[$i]['EnglishName'];
		$thisUserName = $thisUserChineseName?$thisUserChineseName:$thisUserEnglishName;
		$thisUserNameArr[]= $thisUserName;
		//$thisUserTel= $thisUserInfoArr[$i]['MobileTelNo']?$thisUserInfoArr[$i]['MobileTelNo']:$Lang['General']['EmptySymbol']; 
		//$thisPICTelArr[]= $thisUserTel;
		if (trim($thisUserInfoArr[$i]['MobileTelNo']) != ""){
			$thisPICTelArr[]= $thisUserInfoArr[$i]['MobileTelNo'];
		}
	}

//	Replace ，  with 、
	$thisPICArrString = implode("、",$thisUserNameArr);
	if(count($thisPICTelArr) > 0){
		$thisPICTelArrString = implode("、",$thisPICTelArr);
	} else {
		$thisPICTelArrString = $Lang['General']['EmptySymbol'];
	}
	
	## Activity Student 
	$thisActivityStudentInfo =  $thisEventInfo['StatusStudentArr'][2];
	//debug_pr($thisActivityStudentInfo);
	
	$thisNumOfJoinedStudent = count($thisActivityStudentInfo);
		
	## Activity Date time	
	$thisEventInfoArr = $thisActivityDateInfoIDArrAssoc[$thisActivityID];
	
	## Location
	$thisLocation = $thisEventInfo['Location']?$thisEventInfo['Location']:$Lang['General']['EmptySymbol'];
	
	## Transportation
	$thisTransportation = $thisEventInfo['Transport']?$thisEventInfo['Transport']:$Lang['General']['EmptySymbol'];
	
	## Organizer
	$thisOrganizer = $thisEventInfo['Organizer']?$thisEventInfo['Organizer']:$Lang['General']['EmptySymbol'];
	
	## Remarks
	$oleInfoAry = $libenroll->Get_Enrolment_Default_OLE_Setting('activity', array($thisActivityID));
	$schoolRemarks = nl2br($oleInfoAry[0]['SchoolRemark']);
	$schoolRemarks = ($schoolRemarks=='')? '&nbsp;' : $schoolRemarks;
	
	# Get age range
	$_age = array();
	$thisStudentAgeRange = 0;
	
	if($thisNumOfJoinedStudent > 0){
		
		$birthDateArray = "(";
		for($i=0; $i<($thisNumOfJoinedStudent-1); $i++){
			$birthDateArray .= $thisActivityStudentInfo[$i][StudentID].', ';
		}
		$birthDateArray .= $thisActivityStudentInfo[$i][StudentID].")";
	
		$sql = "SELECT u.DateOfBirth FROM INTRANET_USER as u WHERE u.UserID IN ".$birthDateArray;
		$agearray = $li->returnArray($sql);
		
		for($i=0; $i<$thisNumOfJoinedStudent; $i++){
			if ($agearray[$i][0]!= '0000-00-00 00:00:00' && $agearray[$i][0]!= NULL ){
				$targetdate = substr($agearray[$i][0],0,10);
				$_age[] = floor((time() - strtotime($targetdate)) / 31556926);
			}
		}
	}
	
	$age_count = count($_age);
	if ($age_count == 1){
		$thisStudentAgeRange = $_age[0];
	} 
	elseif ($age_count > 1){
		if (min($_age) == max($_age)){
			$thisStudentAgeRange = min($_age);
		} else {
			$thisStudentAgeRange = min($_age).'-'.max($_age);
		}
	}
	else {
		$thisStudentAgeRange = $Lang['General']['EmptySymbol'];
	}
	
	if($thisNumOfJoinedStudent>0){
			
		foreach ((array)$thisEventInfoArr as $thisEventDateID=>$thisEventDateInfo){
			$thisActivityDateStart = $thisEventDateInfo['ActivityDateStart'];
			$thisActivityDateEnd = $thisEventDateInfo['ActivityDateEnd']; 
			
			# Radio: Date Range
			// Each activities could belong to different academic year
			if ($radioPeriod != "YEAR") {
				// Get academic year according to activity date
				$academicInfoAry = getAcademicYearAndYearTermByDate($thisActivityDateStart);
				$academicYearId = $academicInfoAry['AcademicYearID'];
				if(!$academicYearObj || $academicYearObj->AcademicYearID != $academicYearId){
					$academicYearObj = new academic_year($academicYearId);
					$academicYearName = $academicYearObj->Get_Academic_Year_Name();
				}
			}
			
			$thisActivityDateStartDateTimeArr = explode(' ', $thisActivityDateStart);
			$thisActivityDateEndDateTimeArr = explode(' ', $thisActivityDateEnd);
			
			if($thisActivityDateStartDateTimeArr[0]==$thisActivityDateEndDateTimeArr[0])
			{
				$thisActivityDate = $thisActivityDateStartDateTimeArr[0];
			}else{
				$thisActivityDate = $thisActivityDateStartDateTimeArr[0] . ' 至 ' . $thisActivityDateEndDateTimeArr[0];	
			}
			
//			$thisActivityTime = $thisActivityDateStartDateTimeArr[1] . ' 至 ' . $thisActivityDateEndDateTimeArr[1];	
			
		##### Display Table Start #####
		
		
		########   Header Start   ########
		$display .= "<table width='1000px' style='page-break-after: always; border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '>";
			
			# Form Number
			$display .= "<tr>";
				$display .= "<td align='right' style='font-size:16px;'>表格編組： N/LWL01(1)</td>";
			$display .= "</tr>";
			
			# School Name
			$display .= "<tr>";
				//$display .= "<td style='line-height: 20px;' align='center' >";
				$display .= "<td align='center'>";
				//$display .= " 青松侯寶垣中學 <br />2012-2013 <br />培育委員會<br />全方位學習組 <br />校　外　活　動　申　請　表<br />
				$display .= "青松侯寶垣中學 <br />".$academicYearName ."<br />培育委員會<br />全方位學習組 <br />校　外　活　動　申　請　表<br />
				
	 						 <br />";
				$display .= "</td>";
	
			$display .= "</tr>";
			
		########   Header End   ########	
			
			# First Part Start
			$display .= "<tr >";
				$display .= "<td style='border: 1px solid #000; padding: 0px; font-family: \"標楷體\"; '>";
					
					# Fist Part - Left Start
					$display .= "<table cellspacing='3' cellpadding='3' width='100%' style='font-family: \"標楷體\"; font-size: 22px;'>";
						$display .= "<tr>";
							$display .= "<td width='760px' style='vertical-align:top; padding-top: 15px;'>";
								$display .= "<table  cellspacing='3' cellpadding='3' width='100%' style='font-family: \"標楷體\"; font-size: 22px;'>";
									
									# Table Content Start								
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style=' display: table-cell; width: 230px;'>舉辦單位<span style='font-size: 18px;'>(Organization)</span>:</span><span style='width:472px; display:table-cell; padding-left:10px;text-align:left;'>".$thisOrganizer."</span></span>";													
										$display .= "</td>"; 
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 220px;'>活動名稱<span style='font-size: 18px;'>(Programmes)</span>:</span><span style='display:table-cell; width:482px; padding-left:10px;text-align:left;'>". $thisActivityName ."</span></span>";					
										$display .= "</td>"; 
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 120px;'>活動性質*:</span><span style='display:table-cell; width: 586px; padding-left:10px;text-align:left;'>".$_natureDisplay."</span>";					
										$display .= "</td>"; 
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 120px;'>地　點　：</span><span style='display: table-cell; width: 215px; padding-left:10px;text-align:left; '>".$thisLocation."</span>";					
										$display .= "<span style='display: table-cell; width: 140px;'>　日　期　：</span><span style='display: table-cell; width: 215px; padding-left:10px;text-align:left;'>".date("d-m-Y",strtotime($thisActivityDate))."</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td>";
//										$display .= "<span style='display: table-cell; width: 170px;'>時　間　：　由</span><span style='display: table-cell; width: 220px; padding-left:10px; text-align:left;'>".$thisActivityDateStartDateTimeArr[1]."</span>";					
//										$display .= "<span style='display: table-cell; width: 80px;'>　至　</span><span style='display: table-cell; width: 220px; padding-left:10px;text-align:left; '>".$thisActivityDateEndDateTimeArr[1]."</span>";													
										$display .= "<span style='display: table-cell; width: 170px;'>時　間　：　由</span><span style='display: table-cell; text-align:left;'>".substr($thisActivityDateStartDateTimeArr[1], 0, 5)."</span>";					
										$display .= "<span style='display: table-cell; padding-left:10px; '>至</span><span style='display: table-cell; padding-left:10px;text-align:left; '>".substr($thisActivityDateEndDateTimeArr[1], 0, 5)."</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";								
									
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 120px;'>學生人數：</span><span style='display: table-cell; width: 225px; padding-left:10px;text-align:left; '>". $thisNumOfJoinedStudent ."</span>";					
										$display .= "<span style='display: table-cell; width: 120px;'>學生年齡：</span><span style='display: table-cell; width: 225px; padding-left:10px; text-align:left;'>".($thisStudentAgeRange?$thisStudentAgeRange:"&nbsp;")."</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";
								
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style=' display: table-cell; width: 120px;'>老師名單：</span><span style=' width: 570px; display: table-cell; padding-left:10px; text-align:left;'>".$thisPICArrString."</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";		
	
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style=' display: table-cell; width: 200px;'>負責老師手提電話：</span><span style=' width: 490px; display: table-cell; padding-left:10px; text-align:left;'>".$thisPICTelArrString."</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style=' display: table-cell; width: 120px;'>交通工具：</span><span style=' width: 570px; display: table-cell; padding-left:10px;text-align:left; '>".$thisTransportation."</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";	
								
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style=' display: table-cell; width: 120px;'>所需工具：</span><span style=' width: 570px; display: table-cell; padding-left:10px;text-align:left; '>&nbsp;</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";	
	
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 120px;'>學生費用：</span><span style='display: table-cell; width: 215px; padding-left:10px;text-align:left;'>".$thisPaymentAmount."</span>";					
										if($sys_custom['eEnrolment']['ActivityBudgetField']){										
											$display .= "<span style='display: table-cell; width: 140px;'>　財政預算：</span><span style='display: table-cell; width: 215px; padding-left:10px;text-align:left;'>".$thisBudgetAmount."</span>";													
										}
										$display .= "</td>"; 
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td>";
										$display .= "<span style=' display: table-cell; width: 120px;'>備　註　：</span><span style=' width: 570px; display: table-cell; padding-left:10px;text-align:left;'>".$schoolRemarks."</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";	
	
									$display .= "<tr>";
										$display .= "<td align='right'> &nbsp";
										$display .= "</td>"; 
									$display .= "</tr>";	
									
									$display .= "<tr>";
										$display .= "<td >";
										$display .= "<span style=' display: table-cell; width: 300px;'>　</span><span style=' display: table-cell; width: 150px;'>負責老師簽署:</span><span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";	
	
									$display .= "<tr>";
										$display .= "<td >";
										$display .= "<span style=' display: table-cell; width: 300px;'>　</span><span style=' display: table-cell; width: 150px;'>日 　期　:</span><span style='width: 250px; display: table-cell; border-bottom: 1px solid #000; '>&nbsp;</span>";													
										$display .= "</td>"; 
									$display .= "</tr>";
								
									$display .= "<tr>";
										$display .= "<td>&nbsp;";
										$display .= "</td>";
									$display .= "</tr>";																																							
								$display .= "</table>";
								
							$display .= "</td>";
							
							# Fist Part - Right Start
							
								$display .= "<td width='240px' style='border-left: 1px solid #000'>";
									$display .= "<table cellspacing='3' cellpadding='3' width='100%' style='font-family: \"標楷體\"; font-size: 22px;'>";
									
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span>升學及就業輔導組</span><br/>";
										$display .= "<span>組長簽署</span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr><td>&nbsp;</td></tr>";
																	
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span>全方位學習組</span><br/>";
										$display .= "<span>組長簽署</span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr><td>&nbsp;</td></tr>";
																	
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span>副校長簽署</span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr><td>&nbsp;</td></tr>";
																	
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span>校長簽署</span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr><td>&nbsp;</td></tr>";
									
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";
										$display .= "</td>";
									$display .= "</tr>";
											
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span><u>校長批示</u></span>";
										$display .= "</td>";
									$display .= "</tr>";
									
									$display .= "<tr><td>&nbsp;</td></tr>";
									
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span>&nbsp;應否知會警方：</span>";
										$display .= "</td>";
									$display .= "</tr>";								
									
									$display .= "<tr><td>&nbsp;</td></tr>";
	
									
									$display .= "<tr>";
										$display .= "<td align='center'>";
										$display .= "<span style='border: solid 1px #000; width:14px; '>  &nbsp;&nbsp;</span> 是 / <span style='border: solid 1px #000; width:14px;' >&nbsp;&nbsp;</span> 否";
										$display .= "</td>";
									$display .= "</tr>";	
									
									$display .= "<tr><td>&nbsp;</td></tr>";
	
									
									$display .= "</table>";
								$display .= "<td>";
							$display .= "</tr>";
	
					$display .= "</table>";
				$display .= "</td>";
		
			$display .= "</tr>";
									
			$display .= "<tr>";
				$display .= "<td>";
				$display .= "* 若學生是參加外界團體舉辦的比賽、義工服務、研討會等，請註明主辦機構。";
				$display .= "</td>";
			$display .= "</tr>";
		
			$display .= "<tr><td>&nbsp;</td></tr>";
			$display .= "<tr><td>&nbsp;</td></tr>";	
	
					
			$display .= "<tr align='center'>";
				$display .= "<td>";
				$display .= "<u>取消 / 延期</u>";
				$display .= "</td>";
			$display .= "</tr>";
			
		# Second Part	
		$display .= "<tr >";
				$display .= "<td style='border: 1px solid #000; padding: 0px; font-family: \"標楷體\";'>";
				
					$display .= "<table cellspacing='3' cellpadding='0' width='100%' style='font-family: \"標楷體\"; font-size: 22px;'>";
					
						$display .= "<tr>";
							$display .= "<td width='760px'>";
								$display .= "<table cellspacing='3' cellpadding='3' width='100%' style='font-family: \"標楷體\"; font-size: 22px;'>";
								$display .= "<tr>";
									$display .= "<td>";
									$display .= "<span style='display: table-cell;' ><span style='border: solid 1px #000; width:14px; height: 14px;'>&nbsp;&nbsp;</span></span> <span style='display: table-cell; width: 140px;'>　取消</span>";
									$display .= "</td>";
								$display .= "</tr>";
								$display .= "<tr>";
									$display .= "<td>";
										$display .= "<span style='display: table-cell;' ><span style='border: solid 1px #000; width:14px; height: 14px;'>&nbsp;&nbsp;</span></span> <span style='display: table-cell; width: 140px;'>　延期至：</span>";		
										$display .= "<span style='width: 130px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span><span style='display: table-cell; width: 40px;'>年</span> ";
										$display .= "<span style='width: 130px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span><span style='display: table-cell; width: 40px;'>月</span> ";
										$display .= "<span style='width: 130px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span><span style='display: table-cell; width: 40px;'>日</span> ";
									$display .= "</td>";
								$display .= "</tr>";
	
								$display .= "<tr>";
									$display .= "<td>";
									$display .= "<span style='display: table-cell; width: 150px;'>時　間　:　由</span><span style='display: table-cell; width: 235px; border-bottom: 1px solid #000;  '>&nbsp;</span>";					
									$display .= "<span style='display: table-cell; width: 80px;'>　至　</span><span style='display: table-cell; width: 235px; border-bottom: 1px solid #000;  '>&nbsp;</span>";													
									$display .= "</td>"; 
								$display .= "</tr>";	
														
								$display .= "<tr>";
									$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 120px;'>更改原因：</span><span style='display: table-cell; width: 580px; border-bottom: 1px solid #000;  '>&nbsp;</span>";					
									$display .= "</td>";
								$display .= "</tr>";
	
								$display .= "<tr>";
									$display .= "<td>";
										$display .= "<span style='display: table-cell; width: 120px;'>備　註　：</span><span style='display: table-cell; width: 580px; border-bottom: 1px solid #000;  '>&nbsp;</span>";					
									$display .= "</td>";
								$display .= "</tr>";
								
								$display .= "<tr><td>&nbsp;</td></tr>";
								
								$display .= "<tr>";
									$display .= "<td >";
									$display .= "<span style=' display: table-cell; width: 300px;'>　</span><span style=' display: table-cell; width: 150px;'>負責老師簽署:</span><span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";													
									$display .= "</td>"; 
								$display .= "</tr>";	
	
								$display .= "<tr>";
									$display .= "<td >";
									$display .= "<span style=' display: table-cell; width: 300px;'>　</span><span style=' display: table-cell; width: 150px;'>日 　期　:</span><span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";													
									$display .= "</td>"; 
								$display .= "</tr>";	
								
								$display .= "<tr><td>&nbsp;</td></tr>";	
																																
								$display .= "</table>";						
							$display .= "</td>";
							
							
							# Second Part - Right Start
								$display .= "<td width='240px' style='border-left: 1px solid #000'>";
									$display .= "<table cellspacing='3' cellpadding='3' width='100%' style='font-family: \"標楷體\"; font-size: 22px; '>";
										
										$display .= "<tr>";
											$display .= "<td align='center'>";
											$display .= "全方位學習組組長簽署";
											$display .= "</td>";
										$display .= "</tr>";
										
										$display .= "<tr><td>&nbsp;</td></tr>";
											
										$display .= "<tr>";
											$display .= "<td align='center'>";
											$display .= "<span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";
											$display .= "</td>";
										$display .= "</tr>";	
										
										$display .= "<tr><td>&nbsp;</td></tr>";
											
										$display .= "<tr>";
											$display .= "<td align='center'>";
											$display .= "校長簽署";
											$display .= "</td>";
										$display .= "</tr>";
										
										$display .= "<tr><td>&nbsp;</td></tr>";
										
										$display .= "<tr>";
											$display .= "<td align='center'>";
											$display .= "<span style='width: 250px; display: table-cell; border-bottom: 1px solid #000;'>&nbsp;</span>";
											$display .= "</td>";
										$display .= "</tr>";
											
										$display .= "<tr><td>&nbsp;</td></tr>";
																			
									$display .= "</table>";
							$display .= "<td>";
						$display .= "</tr>";
			
				$display .= "</table>";
			$display .= "</tr>";
			
			$display .= "<tr>";
				$display .= "<td>";
				$display .= "* 請於出發前一個月遞交申請表格。";
				$display .= "</td>";
			$display .= "</tr>";
			
		$display .= "</table>";
		}
	}
}


if($display==''){
	$display .= '<center>';
	$display .= "<table width='1000px' style=' border:0px; margin: 0 auto; font-size: 22px; font-family: 標楷體; '>";
	$display .= '<tr><td align="center">';
	$display .= '沒有活動紀錄。';
	$display .= '</td></tr></table></center>';
}





?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<!-- Display result //-->
<?=$display?>

<?
intranet_closedb();
// include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
?>



