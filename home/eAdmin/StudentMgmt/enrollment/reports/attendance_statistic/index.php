<?php

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ($plugin['eEnrollmentLite'] || ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (count($classInfo)==0) &&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) or !$plugin['eEnrollment']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();

# setting the current page
$CurrentPage = "PageAttendanceStatisticReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang["eEnrolment"]['ReportArr']["AttendanceStatistic"], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();



# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
//$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='hideSpan(\"spanStudent\"); changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); document.form1.rankTarget.selectedIndex=0; showResult(\"form\",\"\")'", "", $selectYear);
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";


#Category
$CategorySelection = $libenroll->GET_CATEGORY_SEL($TargetCategory, $ParSelected=1, 'filterByCategory();', $ParSelLang, $noFirst=1, $CategoryTypeID=0,$IsMultiple=1, true,$DeflautName='TargetCategory');
$CategorySelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('TargetCategory',1);filterByCategory()");
$CategorySelection .= $linterface->spacer();
$CategorySelection .= $linterface->MultiSelectionRemark();
$CategorySelection .= '<br />'."\n";
$CategorySelection .= '<br />'."\n";
	

$categoryRowHTML = $CategorySelection;	//Club selection list

	
# select club/avtivity
$x .= '<tr>'."\n";
	$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$Lang['eEnrolment']['record_type'].'</td>'."\n";
	$x .= '<td>'."\n";
	
		$x .= $linterface->Get_Checkbox('GlobalChk', $Name, $Value, $isChecked=true, $Class="", $Lang['Btn']['SelectAll'], 'clickedCheckbox(\'global\', this.checked);');
		$x .= '<br />&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
		$x .= $linterface->Get_Checkbox('Chk_club', 'club[]', 'club', $isChecked=true, $Class='', $eEnrollment['club'], 'clickedCheckbox(\'club\', this.checked);');
		$x .= $linterface->Get_Checkbox('Chk_activity', 'activity[]', 'activity', $isChecked=true, $Class="", $eEnrollment['activity'], 'clickedCheckbox(\'activity\', this.checked);');
		$x .= $linterface->Get_Form_Warning_Msg('WarnMsgDiv', $Lang['eEnrolment']['WarningArr']['ChooseSource'], $Class='warnMsgDiv')."\n";

		$x .= '<br />'."\n";
	$x .= '</td>'."\n";
$x .= '</tr>'."\n";
	
//category		
$x .= '<tr>'."\n";
	$x .= '<td class="field_title">'.$linterface->RequiredSymbol().$eEnrollment['category'].'</td>'."\n";
		$x .= '<td>'."\n";	
			$x .= $categoryRowHTML;
	//	$x .= '</div>'."\n";
//club
		$x .= '<div id="Div_club">'."\n";
			$clubSel = $libenroll_ui->Get_Club_Selection('clubSel', 'clubIdAry[]', $TargetEnrolGroupID, $CategoryID='', $IsAll=0, $NoFirst=1, '', $AcademicYearID='', $IncludeEnrolGroupIdAry='', $Disabled=false, $Class='', $isMultiple=true);
			$clubSelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('clubSel', 1);");
			$x .= '<b>'.$eEnrollment['club'].'</b>'."\n";
			$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($clubSel, $clubSelectAllBtn, 'clubSelectionBox')."\n";
			$x .= '<br />'."\n";	
			$x .= $linterface->Get_Form_Warning_Msg('clubWarnMsgDiv', $Lang['General']['ClubParticipationReport']['SelectAtLeastOneClub'], $Class='warnMsgDiv')."\n";
		
		$x .= '</div>'."\n";
//activity	
		$x .= '<div id="Div_activity">'."\n";
			$activitySel = $libenroll_ui->Get_Activity_Selection('activitySel', 'activityIdAry[]', $TargetEnrolEventID, $CategoryID='', $IsAll=0, $NoFirst=1, '', $AcademicYearID='', $isMultiple=true);
			$activitySelectAllBtn = $linterface->GET_SMALL_BTN($Lang['Btn']['SelectAll'], "button", "js_Select_All('activitySel', 1);");
			$x .= '<b>'.$eEnrollment['activity'].'</b>'."\n";
			$x .= $linterface->Get_MultipleSelection_And_SelectAll_Div($activitySel, $activitySelectAllBtn, 'activitySelectionBox')."\n";
		$x .= '<br />'."\n";
		$x .= $linterface->Get_Form_Warning_Msg('activityWarnMsgDiv', $Lang['General']['ActivityParticipationReport']['SelectAtLeastOneActivity'], $Class='warnMsgDiv')."\n";			
		$x .= '</div>'."\n";		
	$x .= '</td>'."\n";
$x .= '</tr>'."\n";

		
$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
		
$linterface->LAYOUT_START();
	
?>
<script language="javascript">
$(document).ready(
	function () {
		js_Select_All('clubSel', 1);
		js_Select_All('activitySel', 1);

		changeTerm($("#selectYear").val());
	}
);

function changeTerm(val) {

	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
	$('span#spanSemester').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
			"../../ajaxGetSemester.php", 
			{ 
				year: val
			},			
			function(ReturnData) {
			}
		);

	changesemester();
}

function changesemester(value){
var semester =	 value;	
	filterByCategory();
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

function clickedCheckbox(targetSource, elementChecked) {
	if (targetSource == 'global') {
		Set_Checkbox_Value('club[]', elementChecked);
		Set_Checkbox_Value('activity[]', elementChecked);
		
		clickedCheckbox('club', elementChecked);
		clickedCheckbox('activity', elementChecked);
	}
	else {
		Uncheck_SelectAll('GlobalChk', elementChecked);
	
		var selectionDivId = 'Div_' + targetSource;

		if (elementChecked) {
			$('div#' + selectionDivId).show();
		}
		else {
			$('div#' + selectionDivId).hide();
		}
	}
}

	

function filterByCategory(){
	var selectedCategory = $('#TargetCategory').val();
	var CategoryIDList =selectedCategory.join(',');

	var selectedClub = $('#clubSel').val();
	if(selectedClub == null ){
		var selectedClubList = '';
		}
	else{
		var selectedClubList = selectedClub.join(',');
	}
	
	var selectedAct = $('#activitySel').val();
	if(selectedAct == null){
		var selectedActList='';
	}else{
		var selectedActList = selectedAct.join(',');
	}
	
	
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if(jPeriod=="DATE")		
	{
		// force display current year's clubs
		var yearID = <?=$AcademicYearID?>;

	}
	else
	{
		// display selected school year's clubs
		var yearID = document.form1.selectYear.value;
		
	}

	
	$('span#clubSelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'clubSelection',
			CategoryIDList: CategoryIDList,
			SelectionID: 'clubSel',	
			SelectionName: 'clubIdAry[]',
			TargetEnrolGroupID: selectedClubList,
			AcademicYearID:yearID			
		},
		function(ReturnData) {
			js_Select_All('clubSel', 1);
		}
	);

	$('span#activitySelectionBox').html('<?=$linterface->Get_Ajax_Loading_Image()?>').load(
		"../ajax_reload.php", 
		{ 
			Action: 'activitySelection',
			CategoryIDList: CategoryIDList,
			SelectionID: 'activitySel',
			SelectionName: 'activityIdAry[]',
			TargetEnrolEventID: selectedActList,
			AcademicYearID: yearID				
		},
			
		function(ReturnData) {
			js_Select_All('activitySel', 1);
		}
	);

}


function getSelectionId(targetType) {
	var selectionId = '';
	if (targetType == 'club') {
		selectionId = 'clubSel';
	}
	else if (targetType == 'activity') {
		selectionId = 'activitySel';
	}	
	return selectionId;
}

function checkForm(){
	var obj = document.form1;

	var canSubmit = true;
	var isFocused = false;
	
		
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate( document.form1.textFromDate.value,  document.form1.textToDate.value) > 0) 
	{	
		canSubmit = false;
		$('div#div_DateEnd_err_msg').show();
	}

	/////club
	if ($('input#Chk_club').attr('checked') && $('select#clubSel option:selected').length==0) {
	
		canSubmit = false;
		$('div#clubWarnMsgDiv').show();
		if (!isFocused) {
			$('input#clubSel').focus();
			isFocused = true;
		}
		
	}

	/////activity
	if ($('input#Chk_activity').attr('checked') && $('select#activitySel option:selected').length==0) {
		canSubmit = false;
		$('div#activityWarnMsgDiv').show();
		if (!isFocused) {
			$('input#activitySel').focus();
			isFocused = true;
		}
		
	}


	if (canSubmit == false ) {
		return false
		}	
}

</script>

<form name="form1" method="post"  onSubmit="return checkForm();" action = "result.php" >

<table class="form_table_v30">

<!-- Date Range -->
<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true; filterByCategory();">
					<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  onClick="filterByCategory();">
					<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
					<?=$selectSchoolYearHTML?>&nbsp;
					<?php 
			
					if($plugin['SIS_eEnrollment']){ 
						$hideSemester = ' style="display:none;"';
					}
					?>
					<span <?=$hideSemester?>>
						<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
						<span id="spanSemester" onclick = "changesemester(this.value)"><?=$selectSemesterHTML?></span>
						</span>
					</td>
				</tr>
				<tr>
					<td onClick="document.form1.radioPeriod_Date.checked=true; filterByCategory();"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE")?" checked":"" ?> onClick="filterByCategory();"> <?=$i_From?> </td>
					<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='filterByCategory();' ")?>
					<br><?=$linterface->Get_Form_Warning_Msg('div_DateEnd_err_msg',$i_invalid_date, $Class='warnMsgDiv')?></td>
					<td><?=$i_To?> </td>
					<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='filterByCategory();' ")?>
					
				</tr>
			</table>
		</td>
	</tr>
	
	
	<!-- Category -->
	
		<?=$x?>	
	
		
	
	</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
<p class="spacer"></p>
</div>

<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />

</form>
	
<script language="javascript">

</script>	

<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>
