<?php
## Modifying By: 
###########################################
## Modification Log:
##
##  Date:   2020-04-23 Tommy
##          fix: cannot output number of student is PRESENT, ABSENT or EXEMPT,
##               change GetActStudentStatusNumber() to GetActStudentStatus()
##
###########################################
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once $PATH_WRT_ROOT.'includes/table/table_commonTable.php';
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libclubsenrol_ui = new libclubsenrol_ui();
$libenroll->hasAccessRight($_SESSION['UserID'], 'AnyAdminOrHelperOfClubAndActivity');
$tableObj = new table_commonTable();


$headerAry = array();
$headerAry[] =$eEnrollment['club']."/".$eEnrollment['activity'];
$headerAry[] =$Lang['eEnrolment']['MeetingSchedule'];
$headerAry[] =$Lang["eEnrolment"]['AttendanceStatistic']['TotalNum'];
$headerAry[] =$Lang["eEnrolment"]['AttendanceStatistic']['PresentNum'];
$headerAry[] =$Lang["eEnrolment"]['AttendanceStatistic']['AbsentNum'];
$headerAry[] =$Lang["eEnrolment"]['AttendanceStatistic']['ExemptNum'];
if($sys_custom['eEnrolment']['Attendance_Late']){
	$headerAry[] =$Lang["eEnrolment"]['AttendanceStatistic']['LateNum'];
}


if ($radioPeriod == "YEAR")
{
	//	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = substr(getStartDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
	$SQL_enddate = substr(getEndDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
}

$Club_MeetingAry = $libenroll->Get_Club_Meeting_Info_By_Date($clubIdAry,0,$SQL_startdate,$SQL_enddate);
//$club_meeting = $libenroll->Get_Club_Meeting_Date($clubIdAry,0);
$NumberOfclub = sizeof($Club_MeetingAry);


$ClubdataAry = array();
if(!empty($club)){
	for($i=0;$i<$NumberOfclub;$i++){
		
		$ClubEventStartDate = $Club_MeetingAry[$i]['ActivityDateStart'];
		$ClubEventEndDate = $Club_MeetingAry[$i]['ActivityDateEnd'];
		$ClubGroupDateID = $Club_MeetingAry[$i]['GroupDateID'];
		$ClubEnrolGroupID = $Club_MeetingAry[$i]['EnrolGroupID'];
		$EnrolGroupInfo = $libenroll->Get_Club_Info_By_EnrolGroupID($ClubEnrolGroupID);
		
		$ClubStartDate = substr($ClubEventStartDate,0,16);
		$ClubEndDate = substr($ClubEventEndDate,11,5);
		
		$StartDate = substr($ClubEventStartDate,0,10);
		$EndDate =  substr($ClubEventEndDate,0,10);
		
		$ClubTitle = Get_Lang_Selection($EnrolGroupInfo['TitleChinese'],$EnrolGroupInfo['Title']);
		
		$NumberofClubStudent = $libenroll->Get_Club_Member_Info($ClubEnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0);
		//		debug_pr($NumberofClubStudent);
		$StudentIDArr = Get_Array_By_Key($NumberofClubStudent, 'StudentID');
		$StudentIDList = implode(",",(array)$StudentIDArr);
		
		
		$NumbOfClubMumber = sizeof($StudentIDArr);
		
		$StudentNumberResultArr = $libenroll->GetClubStudentStatusNumber($ClubEnrolGroupID,$ClubGroupDateID,$StudentIDList);
		
		$StudentStatus = BuildMultiKeyAssoc($StudentNumberResultArr,RecordStatus,array(StudentID,RecordStatus),0,1);
		$PresentStudent = $StudentStatus[ENROL_ATTENDANCE_PRESENT];
		$AbsentStudent = $StudentStatus[ENROL_ATTENDANCE_ABSENT];
		$ExemptStudent = $StudentStatus[ENROL_ATTENDANCE_EXEMPT];
		$LateStudent = $StudentStatus[ENROL_ATTENDANCE_LATE];
		
		$NumberOfPresent = sizeof($PresentStudent);
		$NumberOfAbsent= sizeof($AbsentStudent);
		$NumberOfExempt = sizeof($ExemptStudent);
		if($sys_custom['eEnrolment']['Attendance_Late']){
			$NumberOfLate= sizeof($LateStudent);
		}
		if($ClubTitle !== null){
			$ClubdataAry[$i][] = $ClubTitle;
			$ClubdataAry[$i][] = $ClubStartDate."--".$ClubEndDate;
			$ClubdataAry[$i][] = $NumbOfClubMumber;
			$ClubdataAry[$i][] = $NumberOfPresent;
			$ClubdataAry[$i][] = $NumberOfAbsent;
			$ClubdataAry[$i][] = $NumberOfExempt;
			
			if($sys_custom['eEnrolment']['Attendance_Late']){
				$ClubdataAry[$i][] = $NumberOfLate;
			}
		}
		
	}
}

$Activity_MeetingAry = $libenroll->Get_Activity_Meeting_Info_By_Date($activityIdAry,0,$SQL_startdate,$SQL_enddate);
$NumberOfActivity = sizeof($Activity_MeetingAry);


$ActivitydataAry = array();

if(!empty($activity)){
	
	for($i=0;$i<$NumberOfActivity;$i++){
		
		$ActivityEventStartDate = $Activity_MeetingAry[$i]['ActivityDateStart'];
		$ActivityEventEndDate = $Activity_MeetingAry[$i]['ActivityDateEnd'];
		$ActivityEventDateID = $Activity_MeetingAry[$i]['EventDateID'];
		$ActivityEnrolEventID = $Activity_MeetingAry[$i]['EnrolEventID'];
		
		$ActivityTitle = $libenroll->GET_EVENT_TITLE($ActivityEnrolEventID);
		//		debug_pr($EnrolActivityTitle);
		//	$ClubTitle = $EnrolGroupInfo['Title'];
		$ActStartDate = substr($ActivityEventStartDate,0,16);
		$ActEndDate = substr($ActivityEventEndDate,11,5);
		
		
		$ActivityMemberInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info($ActivityEnrolEventID, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ActiveMemberOnly=0, $FormIDArr='');
		$ActivityMemberInfoArr = (array)$ActivityMemberInfoArr[$ActivityEnrolEventID]['StatusStudentArr'][2];
		
		$StudentIDArr = Get_Array_By_Key($ActivityMemberInfoArr, 'StudentID');
		$NumOfActivityMumber = sizeof($StudentIDArr);
		
		$StudentIDList = implode(",",(array)$StudentIDArr);
		
		
// 		$StudentArr = $libenroll->GetActStudentStatusNumber($ActivityEnrolEventID,$ActivityEventDateID,$StudentIDList);
		$StudentArr = $libenroll->GetActStudentStatus($ActivityEnrolEventID,$ActivityEventDateID,$StudentIDList);
		
		$StudentStatus = BuildMultiKeyAssoc($StudentArr,RecordStatus,array(StudentID,RecordStatus),0,1);
		$PresentStudentAry = $StudentStatus[ENROL_ATTENDANCE_PRESENT];
		$AbsentStudentAry = $StudentStatus[ENROL_ATTENDANCE_ABSENT];
		$ExemptStudentAry = $StudentStatus[ENROL_ATTENDANCE_EXEMPT];
		
		if($sys_custom['eEnrolment']['Attendance_Late']){
			$LateStudentAry = $StudentStatus[ENROL_ATTENDANCE_LATE];
			$NumberOfLate= sizeof($LateStudentAry);
		}
		
		$NumberOfPresent = sizeof($PresentStudentAry);
		$NumberOfAbsent= sizeof($AbsentStudentAry);
		$NumberOfExempt = sizeof($ExemptStudentAry);
		
		
		if($ActivityTitle !== null ){
			
			$ActivitydataAry[$i][] = $ActivityTitle;
			$ActivitydataAry[$i][] =$ActStartDate."--".$ActEndDate;
			$ActivitydataAry[$i][] =$NumOfActivityMumber;
			$ActivitydataAry[$i][] =$NumberOfPresent;
			$ActivitydataAry[$i][] =$NumberOfAbsent;
			$ActivitydataAry[$i][] =$NumberOfExempt;
			if($sys_custom['eEnrolment']['Attendance_Late']){
				$ActivitydataAry[$i][] =$NumberOfLate;
			}
			
		}
	}
}
$PrintDataAry = array_merge($ClubdataAry,$ActivitydataAry);

$tableObj->setTableType('view');

$tableObj->addHeaderTr(new tableTr());

for ($i=0; $i<count($headerAry); $i++) {
	$tableObj->addHeaderCell(new tableTh($headerAry[$i]));
}
for ($i=0; $i<count($PrintDataAry); $i++) {
	$_rowContentAry = $PrintDataAry[$i];
	$_numOfCol = count($_rowContentAry);
	
	$tableObj->addBodyTr(new tableTr());
	
	for ($j=0; $j<$_numOfCol; $j++) {
		$tableObj->addBodyCell(new tableTd($_rowContentAry[$j]));
	}
}


?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<? echo $tableObj->returnHtml();?>
<?
intranet_closedb();
?>