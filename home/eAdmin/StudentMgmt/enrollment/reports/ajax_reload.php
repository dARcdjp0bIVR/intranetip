<?php
$PATH_WRT_ROOT = "../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");


intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$linterface = new interface_html();

$Action = trim(stripslashes($_POST['Action']));	

//header('Content-Type: text/html; charset=big5');
//header('Content-Type: text/html; charset=utf8');

if ($Action == 'Reload_Club_Selection') {
	$elementId = trim(stripslashes($_POST['ElementID']));
	$elementName = trim(stripslashes($_POST['ElementName']));
	$clubTypeStr = trim(stripslashes($_POST['ClubTypeStr']));
	
	$clubTypeArr = explode(',', $clubTypeStr);
	
	$clubInfoArr = $libenroll->Get_All_Club_Info();
	$numOfClub = count($clubInfoArr);
	
	$selectArr = array();
	$counter = 0;
	for($i=0; $i<$numOfClub; $i++) {
		$thisEnrolGroupID = $clubInfoArr[$i]['EnrolGroupID'];
		$thisGroupCode = $clubInfoArr[$i]['GroupCode'];
		$thisGroupName = $clubInfoArr[$i]['Title'];
		$thisSemester = $clubInfoArr[$i]['Semester'];
		
		$thisSemesterName = ($clubInfoArr[$i]['Semester']=='')? $i_ClubsEnrollment_WholeYear : $clubInfoArr[$i]['Semester'];
		$thisClubName = intranet_undo_htmlspecialchars($thisGroupName).' ('.$thisSemesterName.') ['.$thisGroupCode.']';
		
		$thisClubTypeCode = substr($thisGroupCode, 0, 2);
		if ($thisClubTypeCode=='' || !in_array($thisClubTypeCode, $clubTypeArr)) {
			continue;
		}
		
		$selectArr[$counter][] = $thisEnrolGroupID;
		$selectArr[$counter][] = $thisClubName;
		$counter++;
	}
	
	$elementTag = ' id="'.$elementId.'" name="'.$elementName.'" size="10" multiple';
	
	$h_clubSelection = getSelectByArray($selectArr, $elementTag, '', $all=0, $noFirst=1); 
	$h_clubSelectAllBtn = $linterface->GET_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('$elementId', 1);");
	
	echo $linterface->Get_MultipleSelection_And_SelectAll_Div($h_clubSelection, $h_clubSelectAllBtn, '');
}
else if ($Action == 'clubSelection') {
	$selectionID = trim(stripslashes($_POST['SelectionID']));
	$selectionName = trim(stripslashes($_POST['SelectionName']));
	$onChange = trim(stripslashes($_POST['OnChange']));
	$CategoryIDList = trim(stripslashes($_POST['CategoryIDList']));
	$CategoryIDAry = explode(',', $CategoryIDList);
	$AcademicYearID = $AcademicYearID;
	$TargetGroupID = trim(stripslashes($_POST['TargetEnrolGroupID']));
	$TargetEnrolGroupIDAry = explode(',', $TargetGroupID);
	
	$studentSource_clubSel = $libenroll_ui->Get_Club_Selection($selectionID, $selectionName, $TargetEnrolGroupIDAry, $CategoryIDAry, $IsAll=0, $NoFirst=1, $onChange, $AcademicYearID, $IncludeEnrolGroupIdAry='', $Disabled=false, $Class='preserveValue', $isMultiple=true);
	echo $studentSource_clubSel;
}
else if($Action == 'activitySelection'){
	$selectionID = trim(stripslashes($_POST['SelectionID']));
	$selectionName = trim(stripslashes($_POST['SelectionName']));
	$onChange = trim(stripslashes($_POST['OnChange']));
	$CategoryIDList = trim(stripslashes($_POST['CategoryIDList']));
	$CategoryIDAry = explode(',', $CategoryIDList);
	$AcademicYearID = $AcademicYearID;
	$TargetEventID = trim(($_POST['TargetEnrolEventID']));
	$TargetEnrolEventIDAry = explode(',', $TargetEventID);

	$studentSource_activitySel = $libenroll_ui->Get_Activity_Selection($selectionID, $selectionName, $TargetEnrolEventIDAry, $CategoryIDAry, $IsAll=0, $NoFirst=1, $onChange, $AcademicYearID, $isMultiple=true);
	
	echo $studentSource_activitySel;
}

else if ($Action == 'studentSelection') {
	include_once($PATH_WRT_ROOT."includes/libclass.php");
	$libclass = new libclass();
	
	$sourceType = $_POST['SourceType'];
	$selectedIdList = $_POST['SelectedIdList'];
	$studentSelId = $_POST['studentSelId'];
	$studentSelName = $_POST['studentSelName'];
	
	$selectedIdAry = explode(',', $selectedIdList);
	
	$studentIdAry = array();
	if ($sourceType == 'club') {
		$studentInfoAry = $libenroll->Get_Club_Member_Info($selectedIdAry, $PersonTypeArr='', $AcademicYearID='', $IndicatorArr=0, $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=false);
		
	}
	else if ($sourceType == 'activity') {
		//$studentInfoAry = $libenroll->Get_Activity_Student_Enrollment_Info($selectedIdAry, $ActivityKeyword='', $AcademicYearID='', $WithNameIndicator=0, $IndicatorWithStyle=1, $WithEmptySymbol=1,
		//																	$ActiveMemberOnly=0, $FormIDArr='', $enrolConfigAry['EnrolmentRecordStatus_Approved'], $ReturnAsso=false);
		
		$numOfSelectedIdAry = count($selectedIdAry);
		for($i=0;$i<$numOfSelectedIdAry;$i++){
			$studentInfoAryAll[] = $libenroll->Get_Activity_Participant_Info($selectedIdAry[$i]);	
		}
			
		foreach ($studentInfoAryAll as $studentInfoAryAllKey=> $studentInfoAryAllArrInfo){
			if(count($studentInfoAryAllArrInfo)>1){
				foreach ($studentInfoAryAllArrInfo as $studentInfoAryAllArrInfoKey => $studentInfoAryInfo){
					$studentInfoAry[] = $studentInfoAryInfo;
				}
			}		
		}
	
	}
	else if ($sourceType == 'formClass') {		
		$studentInfoAry = $libclass->getStudentByClassId($selectedIdAry);
	}

	$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
	$studentInfoAry = $libenroll->Get_Student_Info($studentIdAry);
	
	$numOfStudent = count($studentInfoAry);
	
	$selectAry = array();
	for ($i=0; $i<$numOfStudent; $i++) {
		$_studentId = $studentInfoAry[$i]['UserID'];
		$_studentName = $studentInfoAry[$i]['StudentName'];
		
		$selectAry[$_studentId] = $_studentName;
	}
	
	echo getSelectByAssoArray($selectAry, 'id="'.$studentSelId.'" name="'.$studentSelName.'" multiple="multiple" size="10"', $selected="", $all=0, $noFirst=1);
}
?>