<?php
## Modifying By: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
intranet_auth();
intranet_opendb();

$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libclubsenrol_ui = new libclubsenrol_ui();
$libenroll->hasAccessRight($_SESSION['UserID'], 'AnyAdminOrHelperOfClubAndActivity');
$libenroll_report = new libclubsenrol_report();

$targetStartDate = $_POST['targetStartDate'];
$studentSource = $_POST['studentSource'];
$studentIdAry = $_POST['studentIdAry'];
$studentSource_individualStudent = $_POST['studentSource_individualStudent'];	// studentName, studentLogin, studentClassNameClassNumber
$studentSource_studentName = trim(stripslashes($_POST['studentSource_studentName']));
$studentSource_studentLogin = trim(stripslashes($_POST['studentSource_studentLogin']));
$studentSource_individualStudent_classId = $_POST['studentSource_individualStudent_classId'];
$studentSource_individualStudent_classNumber = $_POST['studentSource_individualStudent_classNumber'];
$attendanceSource = $_POST['attendanceSource'];
$attendanceSource_clubIdAry = $_POST['attendanceSource_clubIdAry'];
$attendanceSource_activityIdAry = $_POST['attendanceSource_activityIdAry'];
$attendanceStatus = $_POST['attendanceStatus'];
$showMainGuardianInfo = $_POST['showMainGuardianInfo'];

$htmlAry['resultTable'] = $libenroll_report->getAttendanceStatusReportResultTableHtml($targetStartDate, $targetStartDate, $studentSource, $studentIdAry, $studentSource_individualStudent, $studentSource_studentName, $studentSource_studentLogin, $studentSource_individualStudent_classId, $studentSource_individualStudent_classNumber, 
						$attendanceSource, $attendanceSource_clubIdAry, $attendanceSource_activityIdAry, $attendanceStatus, $showMainGuardianInfo);
?>
<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
</table>
<?=$htmlAry['resultTable']?>
<?
intranet_closedb();
?>