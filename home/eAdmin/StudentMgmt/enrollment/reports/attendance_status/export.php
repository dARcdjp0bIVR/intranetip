<?php
# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();
$linterface = new interface_html();
$libenroll = new libclubsenrol();
$libenroll_report = new libclubsenrol_report();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'AnyAdminOrHelperOfClubAndActivity');

$targetStartDate = $_POST['targetStartDate'];
$studentSource = $_POST['studentSource'];
$studentIdAry = $_POST['studentIdAry'];
$studentSource_individualStudent = $_POST['studentSource_individualStudent'];	// studentName, studentLogin, studentClassNameClassNumber
$studentSource_studentName = trim(stripslashes($_POST['studentSource_studentName']));
$studentSource_studentLogin = trim(stripslashes($_POST['studentSource_studentLogin']));
$studentSource_individualStudent_classId = $_POST['studentSource_individualStudent_classId'];
$studentSource_individualStudent_classNumber = $_POST['studentSource_individualStudent_classNumber'];
$attendanceSource = $_POST['attendanceSource'];
$attendanceSource_clubIdAry = $_POST['attendanceSource_clubIdAry'];
$attendanceSource_activityIdAry = $_POST['attendanceSource_activityIdAry'];
$attendanceStatus = $_POST['attendanceStatus'];
$showMainGuardianInfo = $_POST['showMainGuardianInfo'];

$headerAry = $libenroll_report->getAttendanceStatusReportResultHeaderAry($showMainGuardianInfo);
$dataAry = $libenroll_report->getAttendanceStatusReportResultAry($targetStartDate, $targetStartDate, $studentSource, $studentIdAry, $studentSource_individualStudent, $studentSource_studentName, $studentSource_studentLogin, $studentSource_individualStudent_classId, $studentSource_individualStudent_classNumber, 
						$attendanceSource, $attendanceSource_clubIdAry, $attendanceSource_activityIdAry, $attendanceStatus, $showMainGuardianInfo);


$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "attendance_status.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>