<?php
## Modifying By: Vito
/*
 * Change Log:
 * Date:    2018-06-20 Vito - Create two charts (times and hours)
 * Date:	2017-03-30 Frankie - handle User's Date Range
 * Date:	2017-01-25 Villa #B112166 allow hours can be 0
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

# user access right checking
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&& (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

# setting the current page
$CurrentPage = "PageClubParticipationReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang["eEnrolment"]["ClubParticipationReport"]["ClubParticipationReport"], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 

# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
# temp test
// $currentYear = $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
//$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='hideSpan(\"spanStudent\"); changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); document.form1.rankTarget.selectedIndex=0; showResult(\"form\",\"\")'", "", $selectYear);
//$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")'", "", $selectYear);
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";

$category_selection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "getActivityCategory()", $button_select_all);
$activityCategoryHTMLBtn .= " ".$linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('activityCategory'));return false;", "selectAllBtnActCat");

$linterface->LAYOUT_START();
	
// debug_pr($radioPeriod);
// debug_pr($selectYear);
// debug_pr($selectSemester);
// debug_pr($textFromDate);
// debug_pr($textToDate);
// debug_pr($rankTarget);
// debug_pr($rankTargetDetail);
$rankTargetDetail_str = implode(",",(array)$rankTargetDetail);
if($rankTarget!="student")	$studentID = array();
// debug_pr($studentID);
// debug_pr($studentFlag);
// debug_pr($sel_category);
// debug_pr($activityCategory);
$selectedClub = implode(",",(array)$activityCategory);
// debug_pr($times);
// debug_pr($timesRange);
// debug_pr($hours);
// debug_pr($hoursRange);
// debug_pr($displayUnit);
// debug_pr($sortBy);
// debug_pr($sortByOrder);

# prepare report data 
if ($radioPeriod == "YEAR") 
{
	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = substr(getStartDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
	$SQL_enddate = substr(getEndDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
}

if (sizeof($studentID)>0) {
	$studentID_str = implode(",",$studentID);
} else {
	$userList = $libenroll->getTargetUsers($rankTarget,$rankTargetDetail, "vector");
	$studentID_str = implode(",",$userList);
}

# times sql
// if($times)
// {
	$times_sym = $timesRange ? "<=" : ">=";
	$having_sql[] = " times $times_sym $times ";
// }
// if($hours)
// {
	$hours_sym = $hoursRange ? "<=" : ">=";
	$having_sql[] = " hours $hours_sym '". ($hours<10?"0":"") ."$hours:00:00' ";
// }
$having_con = !empty($having_sql) ? "having " . implode(" and " , $having_sql) : "";

$sortType = $sortByOrder ? "desc" : "asc";
switch($sortBy)
{
	case 0:
		$order_by = "d.ClassName $sortType, d.ClassNumber $sortType";	
		break;
	case 1:
		$order_by = "times $sortType";	
		break;
	case 2:
		$order_by = "hours $sortType";	
		break;
}

$name_field = getNameFieldByLang("d.");
// $sql ="
// 	select 
// 		c.UserID,  d.ClassName, d.ClassNumber, $name_field, count(c.EnrolGroupID) as times,
// 		SEC_TO_TIME(SUM(TIME_TO_SEC(IF (TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)<TIME('00:00:00'), '00:00:00', TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart))))) as hours
// 	from 
// 		INTRANET_USERGROUP as c
// 		inner join INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
// 		inner join INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5)
// 		inner join INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (e.ActivityDateStart>='$SQL_startdate' and e.ActivityDateStart<='$SQL_enddate'))
// 		inner join INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
// 	where
// 		c.EnrolGroupID in ($selectedClub)
// 		and c.UserID in ($studentID_str)
// 	group by 
// 		d.UserID
// 	$having_con
// 	order by
// 		$order_by, d.ClassName, d.ClassNumber
// ";
$sql = "
		SELECT
			c.UserID,
			d.ClassName,
			d.ClassNumber,
			$name_field,
			SUM(
				IF(f.RecordStatus=3,'0','1')
			) as times,
			SEC_TO_TIME(
				SUM(
					TIME_TO_SEC(
						IF( f.RecordStatus=3,
							'00:00:00',
							IF( TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)<TIME('00:00:00'),
								'00:00:00',
								TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart)
							)
						)
					)
				)
			) as hours,
			f.RecordStatus
		FROM
			INTRANET_USERGROUP as c
		INNER JOIN
			INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
		INNER JOIN
			INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5)
		INNER JOIN
			INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID 
			and (e.RecordStatus Is Null Or e.RecordStatus = 1)
			and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
		INNER JOIN
			INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID";
if ($libenroll->enableUserJoinDateRange()) {
	$sql .=" 
		AND (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%' OR EnrolAvailiableDateStart <= ActivityDateStart)
		AND (EnrolAvailiableDateEnd IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%' OR EnrolAvailiableDateEnd >= ActivityDateEnd)";
}
$sql .=") 
        where 	
			d.UserID in ($studentID_str)
		GROUP BY
			d.UserID
		$having_con
		ORDER BY
			$order_by, d.ClassName, d.ClassNumber
		";
//debug_pr($sql);
$result = $libenroll->returnArray($sql);

$data_ary = array();
$display = "";

$sql_BarChart = "
		SELECT
			distinct d.ClassName,
            SUM(
				IF(f.RecordStatus=3,'0','1')
			) as times,
			SEC_TO_TIME(
				SUM(
					TIME_TO_SEC(
						IF( f.RecordStatus=3,
							'00:00:00',
							IF( TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)<TIME('00:00:00'),
								'00:00:00',
								TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart)
							)
						)
					)
				)
			) as hours
		FROM
			INTRANET_USERGROUP as c
		INNER JOIN
			INTRANET_USER as d on (d.UserID=c.UserID and d.RecordType=2)
		INNER JOIN
			INTRANET_GROUP as b on (b.GroupID=c.GroupID and b.RecordType=5)
		INNER JOIN
			INTRANET_ENROL_GROUP_DATE as e on (e.EnrolGroupID=c.EnrolGroupID
			and (e.RecordStatus Is Null Or e.RecordStatus = 1)
			and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
		INNER JOIN
			INTRANET_ENROL_GROUP_ATTENDANCE as f on (f.GroupDateID=e.GroupDateID and f.StudentID=d.UserID)
        GROUP BY
                d.ClassName
                ";

//debug_pr($sql_BarChart);
$result_BarChart= $libenroll->returnArray($sql_BarChart);
$numRow = count($result_BarChart);
//debug_pr($result_BarChart_Xaxis);
//echo $result_BarChart[0]['ClassName'];

$dataAry_BarChartWithQ = array();
for ($i = 0; $i < $numRow; $i++){
    $_col = 0;
    $dataAry_BarChartWithQ[$i][$_col++] = "'" . $result_BarChart[$i]['ClassName'] . "'";
    $dataAry_BarChartWithQ[$i][$_col++] = $result_BarChart[$i]['times'];
    $dataAry_BarChartWithQ[$i][$_col++] = $result_BarChart[$i]['hours'];
    //debug_pr($dataAry_BarChartWithQ[$i][0]);
}

$str_BarChart_Xaxis = $dataAry_BarChartWithQ[0][0];
$str_BarChart_times = $dataAry_BarChartWithQ[0][1];
$str_BarChart_hours = $libenroll->timeToDecimal($dataAry_BarChartWithQ[0][2]);
// echo ($str_BarChart_times);
// echo ($str_BarChart_hours);
//  echo ($str_BarChart_Xaxis);
for ($i=1;$dataAry_BarChartWithQ[$i][0]!=null;$i++){
    $str_BarChart_Xaxis .= "," . $dataAry_BarChartWithQ[$i][0];
    $str_BarChart_times .= "," . $dataAry_BarChartWithQ[$i][1];
    $str_BarChart_hours .= "," . $libenroll->timeToDecimal($dataAry_BarChartWithQ[$i][2]);
     //echo ($str_BarChart_Xaxis);
//     echo ($i . ",");
}
//echo ($str_BarChart_Xaxis);
//echo($str_BarChart_times);
//echo($str_BarChart_hours);
if(!empty($result))
{
	if($displayUnit==0)	# student
	{
		### build data array
		foreach($result as $k=>$d)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d;
// 			if(in_array($this_UserID,explode(',',$studentID_str))){
			    $data_ary[$this_ClassName][] = array($this_UserID, $this_ClassNumber, $this_student, $this_times, $this_hours);
// 			}
		
		}
		
		### build display table
		foreach($data_ary as $this_ClassName=>$d)
		{
			$display .= $linterface->GET_NAVIGATION2_IP25($this_ClassName);
			$display .= "<table class='common_table_list_v30 view_table_list_v30'>";
			$display .= "<tr>";
			$display .= "<th width='100'>$i_ClassNumber</th>";
			$display .= "<th>$i_UserName</th>";
			$display .= "<th width='100'>". $Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"] ."</th>";
			$display .= "<th width='100'>". $Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"] ."</th>";
			$display .= "</tr>";
			
			$this_class_total_times = 0;
			$this_class_total_hours = 0;
			foreach($d as $k1=>$d1)
			{
				list($this_UserID, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d1;
// 				if(in_array($this_UserID,explode(',',$studentID_str))){
				    $this_class_total_times += $this_times;
				    $this_hr = $libenroll->timeToDecimal($this_hours);
				    $this_class_total_hours += $this_hr;
				    
				    $display .= "<tr>";
				    $display .= "<td>$this_ClassNumber</td>";
				    $display .= "<td><a href='javascript:click_show_details($this_UserID);'>$this_student</a>
				    <br><span id=\"layer_details_$this_UserID\" class=\"selectbox_layer selectbox_layer_show_info_v30\" style=\"visibility: hidden;width:500px;\"></span>
				    </td>";
				    $display .= "<td>$this_times</td>";
				    $display .= "<td>$this_hr</td>";
				    $display .= "</tr>";
// 				}			
			}
			
			$display .= "<tr>";
			$display .= "<td colspan='2' align='right'>$list_total</td>";
			$display .= "<td>$this_class_total_times</td>";
			$display .= "<td>$this_class_total_hours</td>";
			$display .= "</tr>";
				
			$display .= "</table><br>";
		}
	}
	else				# class
	{
		### build data array
		foreach($result as $k=>$d)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d;
			$this_hr = $libenroll->timeToDecimal($this_hours);
			$data_ary[$this_ClassName]['t'] += $this_times;
			$data_ary[$this_ClassName]['h'] += $this_hr;
		}
		
		if($sortBy!=0)
		{
			# sorting 
			foreach ($data_ary as $key => $row) {
			    $t[$key]  = $row['t'];
			    $h[$key] = $row['h'];
			}

			switch($sortBy)
			{
				case 1:
					$sort_field = $t;	
					break;
				case 2:
					$sort_field = $h;	
					break;
			}
			$sort_order = $sortByOrder ? SORT_DESC : SORT_ASC;
			array_multisort($sort_field, $sort_order, $data_ary);
		}

		$display .= "<table class='common_table_list_v30 view_table_list_v30'>";
		$display .= "<tr>";
		$display .= "<th>$i_ClassName</th>";
		$display .= "<th>". $Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"] ."</th>";
		$display .= "<th>". $Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"] ."</th>";
		$display .= "</tr>";
		
		$this_class_total_times = 0;
		$this_class_total_hours = 0;
		foreach($data_ary as $this_ClassName=>$d)
		{
			$this_class_total_times += $d[t];
			$this_class_total_hours += $d[h];
			$display .= "<tr>";
			$display .= "<td>$this_ClassName</td>";
			$display .= "<td>$d[t]</td>";
			$display .= "<td>$d[h]</td>";
			$display .= "</tr>";
		}
		
		$display .= "<tr>";
		$display .= "<td align='right'>$list_total</td>";
		$display .= "<td>$this_class_total_times</td>";
		$display .= "<td>$this_class_total_hours</td>";
		$display .= "</tr>";
			
		$display .= "</table>";
	}
}
else
{
	$display = "<div class='no_record_find_v30'>". $i_no_record_searched_msg ."</div>";
}

### Get toolbar
	$x = '';
	// Export
	$buttonHref = '';
	$optionAry = array();
	$buttonHref = "javascript:void(0);";
	$optionAry[] = array("javascript:click_export('simple');", $Lang['eEnrolment']['SimpleView'], 'simpleExportBtn');
	$optionAry[] = array("javascript:click_export('details');", $Lang['eEnrolment']['DetailedView'], 'detailedExportBtn');
	$x .= $linterface->Get_Content_Tool_v30('export', $buttonHref, $text="", $optionAry, $other="", $divID='', $extra_class='', $hideDivAfterClicked=false, $extra_onclick="hideAllOptionLayer();");
	
	// Print
	$buttonHref = '';
	$optionAry = array();
	$buttonHref = "javascript:void(0);";
	$optionAry[] = array("javascript:click_print('simple');", $Lang['eEnrolment']['SimpleView'], 'simplePrintBtn');
	$optionAry[] = array("javascript:click_print('details');", $Lang['eEnrolment']['DetailedView'], 'detailedPrintBtn');
	$x .= $linterface->Get_Content_Tool_v30('print', $buttonHref, $text="", $optionAry, $other="", $divID='', $extra_class='', $hideDivAfterClicked=false, $extra_onclick="hideAllOptionLayer();");
	$htmlAry['toolbar'] = $x;

?>

<script language="javascript">
<!--
function changeTerm(val) {
	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)
	
	getActivityCategory();
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function hideOptionLayer()
{
	$('.Form_Span').attr('style', 'display: none');
	$('.spanHideOption').attr('style', 'display: none');
	$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	$('.Form_Span').attr('style', '');
	$('.spanShowOption').attr('style', 'display: none');
	$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
		
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
		
	xmlHttp = GetXmlHttpObject()
	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "../../get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	//url += "&year="+document.getElementById('selectYear').value;
	url += "&year=<?=$AcademicYearID?>";
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + \"".implode(',',$studentID)."\";";
	} ?>		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);

	if (val=='student') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function getActivityCategory() {
	
	var categoryID = document.form1.sel_category.value;
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if(jPeriod=="DATE")		
	{
		// force display current year's clubs
		var yearID = <?=$AcademicYearID?>;
	}
	else
	{
		// display selected school year's clubs
		var yearID = document.form1.selectYear.value;
	}	
	
	$('#CategoryElements').load(
		'ajax_ClubParticipationReport.php', 
		{
			Action: 'CategoryElements',
			categoryID: categoryID,
			selectedClubs: '<?=implode(",",$activityCategory)?>',
			AcademicYearID: yearID
		}, 
		function (data){
			//SelectAll($("#activityCategory").get(0));
		});
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	document.getElementById('div_Target_err_msg').innerHTML = "";
 	document.getElementById('div_Times_err_msg').innerHTML = "";
 	document.getElementById('div_Hours_err_msg').innerHTML = "";
 	document.getElementById('div_Category_err_msg').innerHTML = "";
}

function checkForm()
{
	var obj = document.form1;
	
	var error_no = 0;
	var focus_field = "";
	var choiceSelected;
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	///// Period
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate(obj.textFromDate.value, obj.textToDate.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_invalid_date?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "textFromDate";
	}
	
	///// Target
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Form?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Class?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if(document.getElementById('rankTarget').value == "student") 
	{
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		
		if(choiceSelected==0) 
		{
			document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "rankTarget";
		}
	}
	
	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}	 
	
	///// category
	if(!countOption(obj.elements["activityCategory[]"]))
	{
		document.getElementById('div_Category_err_msg').innerHTML = '<font color="red"><?=$Lang['General']['ClubParticipationReport']['SelectAtLeastOneClub']?></font>';
		error_no++;
		focus_field = "sel_category";
	}
	
	///// Times
	if(!check_positive_int_30(obj.times, "<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>","","","div_Times_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "times";
	}
	
	///// Hours
	if(!check_positive_int_30(obj.hours, "<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>","","","div_Hours_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "hours";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true
	}
}

var current_open_layer_id = "";

function click_show_details(uid)
{
	obj = "layer_details_" + uid;
	obj_cur_display = document.getElementById("layer_details_"+uid).style.visibility;
	
	if(obj_cur_display=="visible")
	{
		// hide current layer
		MM_showHideLayers("layer_details_"+uid,"","hide");
		current_open_layer_id = "";
		document.getElementById("layer_details_"+uid).style.height = "0px";
	}
	else
	{
		// hide opened layer
		MM_showHideLayers("layer_details_"+current_open_layer_id,"","hide");
		current_open_layer_id = "";
		document.getElementById("layer_details_"+uid).style.height = "0px";
		
		// show current layer
		aj_retrieve_student_details(uid);
		document.getElementById("layer_details_"+uid).style.height = "200px";
		MM_showHideLayers("layer_details_"+uid,"","show");
		current_open_layer_id = uid;
	}
}

function aj_retrieve_student_details(uid) 
{
	$('#layer_details_'+uid).load(
		'ajax_ClubParticipationReport.php', 
		{
			Action: 'Retrieve_Details',
			SQL_startdate: '<?=$SQL_startdate?>',
			SQL_enddate: '<?=$SQL_enddate?>',
			studentID: uid,
			selectedClub: '<?=implode(",",$activityCategory)?>'
		}, 
		function (data){
		});
}

function click_export(export_type)
{
	checkOption(document.form1.elements['activityCategory[]']);
	if (export_type == 'details'){
		document.form1.action = "export_details.php";
	} else {
		document.form1.action = "export.php";
	}
	document.form1.submit();
	document.form1.action = "result.php";
}

function click_print(print_type)
{
	if(print_type == 'details'){
		document.form1.action="print_details.php";
	} else {
		document.form1.action="print.php";
	}
	document.form1.target="_blank";
	document.form1.submit();
	
	document.form1.action="result.php";
	document.form1.target="_self";
}
//-->
</script>

<div id="div_form" class="report_option report_hide_option">
	<span id="spanShowOption" class="spanShowOption">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	<span class="Form_Span" style="display:none">
		<form name="form1" method="post" action="result.php" onSubmit="return checkForm();">
		
		<table class="form_table_v30">
		<!-- Target -->
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"]?></td>
			<td>
				<table class="inside_form_table">
					<tr>
						<td valign="top">
							<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
								<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
								<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
								<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
							</select>
						</td>
						<td valign="top" nowrap>
								<!-- Form / Class //-->
								<span id='rankTargetDetail'>
								<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="7"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
									
								<!-- Student //-->
								<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
									<select name="studentID[]" multiple size="7" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
								</span>
						</td>
					</tr>
					<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
				</table>
				<span id='div_Target_err_msg'></span>
			</td>
		</tr>
		
		<!-- Period -->
		<tr valign="top">
			<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
			<td>
				<table class="inside_form_table">
					<tr>
						<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true; getActivityCategory();">
							<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($radioPeriod!="DATE")?" checked":"" ?> onClick="getActivityCategory();">
							<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
							<?=$selectSchoolYearHTML?>&nbsp;
							<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
							<span id="spanSemester"><?=$selectSemesterHTML?></span>
						</td>
					</tr>
					<tr>
						<td onClick="document.form1.radioPeriod_Date.checked=true; getActivityCategory();"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($radioPeriod=="DATE")?" checked":"" ?> onClick="getActivityCategory();"> <?=$i_From?> </td>
						<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='getActivityCategory();' ")?>
						<br><span id='div_DateEnd_err_msg'></span></td>
						<td> <?=$i_To?> </td>
						<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='getActivityCategory();' ")?>
					</tr>
				</table>
			</td>
		</tr>
		
		<!-- Category -->
		<tr>
			<td class="field_title"><span class="tabletextrequire">*</span><?=$eEnrollment['add_activity']['act_category']?></td>
			<td>
				<table class="inside_form_table" width="100%">
					<tr>
						<td>
							<?=$category_selection?>
						</td>
					</tr>
					<tr>
						<td>
							<span id="CategoryElements"></span> <?=$activityCategoryHTMLBtn?>
						</td>
					</tr>
				</table>
				<span id='div_Category_err_msg'></span>
			</td>
		</tr>
		
		<!-- Time(s) -->
		<tr>
			<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"]?></td>
			<td><input type="text" name="times" id="times" size="3" value="<?=$times?>"/>
				<?=$linterface->Get_Radio_Button("timesOnAbove", "timesRange", 0, $timesRange==0, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"])?>
				<?=$linterface->Get_Radio_Button("timesOnBelow", "timesRange", 1,$timesRange==1, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"])?>
				<br><span id='div_Times_err_msg'></span>
			</td>
		</tr>
		
		<!-- Hour(s) -->
		<tr>
			<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"]?></td>
			<td><input type="text" name="hours" id="hours" size="3" value="<?=$hours?>" />
				<?=$linterface->Get_Radio_Button("hoursOnAbove", "hoursRange", 0, $hoursRange==0, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"])?>
				<?=$linterface->Get_Radio_Button("hoursOnBelow", "hoursRange", 1, $hoursRange==1, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"])?>
				<br><span id='div_Hours_err_msg'></span>
			</td>
		</tr>
		
		<!-- Display Unit -->
		<tr>
			<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["DisplayUnit"]?></td>
			<td>
				<?=$linterface->Get_Radio_Button("displayUnitStudent", "displayUnit", 0, $displayUnit==0, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Student"])?>
				<?=$linterface->Get_Radio_Button("displayUnitClass", "displayUnit", 1, $displayUnit==1, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Class"])?>
			</td>
		</tr>
		
		<!-- Sort By -->
		<tr>
			<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"]?></td>
			<td>
				<?=$linterface->Get_Radio_Button("sortByClassNameNumber", "sortBy", 0, $sortBy==0, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"] . " / " . $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"])?>
				<?=$linterface->Get_Radio_Button("sortByTimes", "sortBy", 1, $sortBy==1, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"])?>
				<?=$linterface->Get_Radio_Button("sortByHours", "sortBy", 2, $sortBy==2, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"])?>
				<br>
				<?=$linterface->Get_Radio_Button("sortByOrderAsc", "sortByOrder", 0, $sortByOrder==0, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["AscendingOrder"])?>
				<?=$linterface->Get_Radio_Button("sortByOrderDesc", "sortByOrder", 1, $sortByOrder==1, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["DescendingOrder"])?>				
			</td>
		</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
		<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="studentFlag" id="studentFlag" value="<?=$studentFlag?>">
		</form>
	
	
	</span>
</div>

<!-- Export & Print button //-->
<div class="content_top_tool">
	<div class="Conntent_tool">
		<!--<a href="javascript:click_export();" class="export"> <?=$Lang['Btn']['Export']?></a>-->
		<!--<a href="javascript:click_print()" class="print"> <?=$Lang['Btn']['Print']?></a>-->
		<?=$htmlAry['toolbar']?>
	</div>
<br style="clear:both" />
</div>

<!-- Display result //-->
<?=$display?>

<!-- Generate bar/column chart -->
<script src="<?php echo $PATH_WRT_ROOT?>templates/highchart/highcharts.js"></script>
<script src="<?php echo $PATH_WRT_ROOT?>templates/highchart/modules/exporting.js"></script>
<div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
<!-- Chart 1 -->
<script type="text/javascript">
// 	Highcharts.chart('container', {
// 	  chart: {
// 	    type: 'bar'
// 	  },
// 	  title: {
// 	    text: 'Club Participation Report (Times)'
// 	  },
// 	  subtitle: {
// 	    text: null
// 	  },
// 	  xAxis: {  	
//		    categories: [<?php echo ($str_BarChart_Xaxis);?>],
// 		    title: {
// 		      text: null
// 		    }
// 		  },	
// 	  yAxis: {
// 	    min: 0,
// 	    title: {
// 	      text: null,
// 	      align: 'high'
// 	    },
// 	    labels: {
// 	      overflow: 'justify'
// 	    }
// 	  },
// 	  tooltip: {
// 	    valueSuffix: ' times'
// 	  },
// 	  plotOptions: {
// 	    bar: {
// 	      dataLabels: {
// 	        enabled: true
// 	      }
// 	    }
// 	  },
// 	  legend: {
// 	    layout: 'vertical',
// 	    align: 'right',
// 	    verticalAlign: 'top',
// 	    x: -40,
// 	    y: 80,
// 	    floating: true,
// 	    borderWidth: 1,
// 	    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
// 	    shadow: true
// 	  },
// 	  credits: {
// 	    enabled: false
// 	  },
// 	  series: [{
// 	    name: 'Times',
//	    data: [<?php echo ($str_BarChart_times);?>]
// 	  }]
// 	});
// 	Coloumn chart1
Highcharts.chart('container', {
		  chart: {
		    type: 'column'
		  },
		  title: {
		    text: 'Club Participation Report (Times)'
		  },
		  subtitle: {
		    text: null
		  },
		  xAxis: {
		    categories: [<?php echo ($str_BarChart_Xaxis);?>
		    ],
		    crosshair: true
		  },
		  yAxis: {
		    min: 0,
		    title: {
		      text: null
		    }
		  },
		  tooltip: {
		    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		      '<td style="padding:0"><b>{point.y:.1f} times</b></td></tr>',
		    footerFormat: '</table>',
		    shared: true,
		    useHTML: true
		  },
		  plotOptions: {
		    column: {
		      pointPadding: 0.2,
		      borderWidth: 0
		    },
		    column:{
			    dataLabels: {
		        	enabled: true
		    	}
		    }
		  },
		  series: [{
		      animation: false,
		      dataLabels: {		        
			      enabled: true
		      }
		  }],
		  series: [{
		    name: 'Hours',
		    data: [<?php echo (($str_BarChart_times));?>]
		  }],
		  exporting: {
			    enabled: false
		  },
		  credits: {
			    enabled: false
		  }
		});
</script>
<!-- Chart 2 -->
<div id="container2" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
// 	Highcharts.chart('container2', {
// 	  chart: {
// 	    type: 'bar'
// 	  },
// 	  title: {
// 	    text: 'Club Participation Report (Hours)'
// 	  },
// 	  subtitle: {
// 	    text: null
// 	  },
// 	  xAxis: {  	
//		    categories: [<?php echo ($str_BarChart_Xaxis);?>],
// 		    title: {
// 		      text: null
// 		    }
// 		  },	
// 	  yAxis: {
// 	    min: 0,
// 	    title: {
// 	      text: null,
// 	      align: 'high'
// 	    },
// 	    labels: {
// 	      overflow: 'justify'
// 	    }
// 	  },
// 	  tooltip: {
// 	    valueSuffix: ' hours'
// 	  },
// 	  plotOptions: {
// 	    bar: {
// 	      dataLabels: {
// 	        enabled: true
// 	      }
// 	    }
// 	  },
// 	  legend: {
// 	    layout: 'vertical',
// 	    align: 'right',
// 	    verticalAlign: 'top',
// 	    x: -40,
// 	    y: 80,
// 	    floating: true,
// 	    borderWidth: 1,
// 	    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
// 	    shadow: true
// 	  },
// 	  credits: {
// 	    enabled: false
// 	  },
// 	  series: [{
// 	    name: 'Hours',
//	    data: [<?php echo (($str_BarChart_hours));?>]
// 	  }]
// 	});
// 	Coloumn chart2
	Highcharts.chart('container2', {
		  chart: {
		    type: 'column'
		  },
		  title: {
		    text: 'Club Participation Report (Hours)'
		  },
		  subtitle: {
		    text: null
		  },
		  xAxis: {
		    categories: [<?php echo ($str_BarChart_Xaxis);?>
		    ],
		    crosshair: true
		  },
		  yAxis: {
		    min: 0,
		    title: {
		      text: null
		    }
		  },
		  tooltip: {
		    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
		    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
		      '<td style="padding:0"><b>{point.y:.1f} hours</b></td></tr>',
		    footerFormat: '</table>',
		    shared: true,
		    useHTML: true
		  },
		  plotOptions: {
		    column: {
		      pointPadding: 0.2,
		      borderWidth: 0
		    },
		    column:{
			    dataLabels: {
		        	enabled: true
		    	}
		    }
		  },
		  series: [{
		    name: 'Hours',
		    data: [<?php echo (($str_BarChart_hours));?>]
		  }],
		  exporting: {
			    enabled: false
		  },
		  credits: {
			    enabled: false
		  }  
		});
</script>

<script language="javascript">
$(document).ready(function(){
	changeTerm($("#selectYear").val());
 	
 	showRankTargetResult($("#rankTarget").val(), '<?=$rankTargetDetail_str?>');
 	

	<?if(sizeof($studentID)>0) { ?>
	var xmlHttp2;
	
	initialStudent();	
		
	function initialStudent() {
		xmlHttp2 = GetXmlHttpObject()
		url = "../../get_live.php?target=student2ndLayer&value=<?=implode(',',$rankTargetDetail)?>&rankTargetDetail=<?=implode(',',$rankTargetDetail)?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID);?>";
		xmlHttp2.onreadystatechange = stateChanged2
		xmlHttp2.open('GET',url,true);
		xmlHttp2.send(null);
		
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
	}
	
	function stateChanged2() 
	{ 
		if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
		{ 
			document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
			document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
			
			//SelectAll(form1.elements['studentID[]']);
		} 
	}
	<? } ?>
	getActivityCategory();
 	
});

</script>	
<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>