<?php
## Modifying By: 

##### Change Log [Start] #####
#	Date	:	2014-10-14	Pun
# 				Add round report for SIS customization
#
#	Date	:	2011-09-08	YatWoon
# 				lite version should not access this page 
#
###### Change Log [End] ######

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ($plugin['eEnrollmentLite'] || ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (count($classInfo)==0) &&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) or !$plugin['eEnrollment']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();

# setting the current page
$CurrentPage = "PageClubParticipationReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang["eEnrolment"]["ClubParticipationReport"]["ClubParticipationReport"], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 
# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
//$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='hideSpan(\"spanStudent\"); changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); document.form1.rankTarget.selectedIndex=0; showResult(\"form\",\"\")'", "", $selectYear);
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";

$category_selection = $libenroll->GET_CATEGORY_SEL($sel_category, 1, "getActivityCategory()", $button_select_all);
$activityCategoryHTMLBtn .= " ".$linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('activityCategory'));return false;", "selectAllBtnActCat");

$linterface->LAYOUT_START();
	
?>

<script language="javascript">
<!--

function changeTerm(val) {
	
	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
		
	xmlHttp3 = GetXmlHttpObject()
	
	if (xmlHttp3==null)
	{
		alert ("Browser does not support HTTP Request")
		return
	} 

	var url = "";
		
	url = "../../ajaxGetSemester.php";
	url += "?year=" + val;
	url += "&term=<?=$selectSemester?>";
	url += "&field=selectSemester";
	xmlHttp3.onreadystatechange = stateChanged3 
	xmlHttp3.open("GET",url,true)
	xmlHttp3.send(null)

	getActivityCategory();
	
} 

function stateChanged3() 
{ 
	if (xmlHttp3.readyState==4 || xmlHttp3.readyState=="complete")
	{ 
		document.getElementById("spanSemester").innerHTML = xmlHttp3.responseText;
		document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
	} 
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
		
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
		
			xmlHttp = GetXmlHttpObject()

	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "../../get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year=<?=$AcademicYearID?>";
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if(!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) {?>
	url += "&ownClassOnly=1";
	<?}?>
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);
	if (val=='student') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function getActivityCategory() { 
	
	var categoryID = document.form1.sel_category.value;
	
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if(jPeriod=="DATE")		
	{
		// force display current year's clubs
		var yearID = <?=$AcademicYearID?>;
	}
	else
	{
		// display selected school year's clubs
		var yearID = document.form1.selectYear.value;
	}
	
	$('#CategoryElements').load(
		'ajax_ClubParticipationReport.php', 
		{
			Action: 'CategoryElements',
			categoryID: categoryID,
			AcademicYearID: yearID
		}, 
		function (data){
			SelectAll($("#activityCategory").get(0));
		});
}

function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_DateEnd_err_msg').innerHTML = "";
 	document.getElementById('div_Target_err_msg').innerHTML = "";
 	document.getElementById('div_Times_err_msg').innerHTML = "";
 	document.getElementById('div_Hours_err_msg').innerHTML = "";
 	document.getElementById('div_Category_err_msg').innerHTML = "";
}

function checkForm()
{
	var obj = document.form1;
	
	var error_no = 0;
	var focus_field = "";
	var choiceSelected;
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	///// Period
	jPeriod = $("input[name='radioPeriod']:checked").val();
	if (jPeriod=='DATE' && compareDate(obj.textFromDate.value, obj.textToDate.value) > 0) 
	{
		document.getElementById('div_DateEnd_err_msg').innerHTML = '<font color="red"><?=$i_invalid_date?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "textFromDate";
	}
	
	///// Target
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Form?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Class?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if(document.getElementById('rankTarget').value == "student") 
	{
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		
		if(choiceSelected==0) 
		{
			document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "rankTarget";
		}
	}
	
	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}	 
	
	///// category
	if(!countOption(obj.elements["activityCategory[]"]))
	{
		document.getElementById('div_Category_err_msg').innerHTML = '<font color="red"><?=$Lang['General']['ClubParticipationReport']['SelectAtLeastOneClub']?></font>';
		error_no++;
		focus_field = "sel_category";
	}
	
	///// Times
	if(!check_positive_int_30(obj.times, "<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>","","","div_Times_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "times";
	}
	
	///// Hours
	if(!check_positive_int_30(obj.hours, "<?=$Lang['General']['JS_warning']['InputPositiveInteger'];?>","","","div_Hours_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "hours";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		return true
	}
}

//-->
</script>

<form name="form1" method="post" action="result.php" onSubmit="return checkForm();">

<table class="form_table_v30">

<!-- Target -->
<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"]?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td valign="top">
					<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
						<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
						<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
						<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
					</select>
				</td>
				<td valign="top" nowrap>
					<!-- Form / Class //-->
					<span id='rankTargetDetail'>
					<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="7"></select>
					</span>
					<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
						
					<!-- Student //-->
					<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
						<select name="studentID[]" multiple size="7" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
					</span>
					
				</td>
			</tr>
			<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
		</table>
		<span id='div_Target_err_msg'></span>
	</td>
</tr>

<!-- Period -->
<tr valign="top">
	<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
	<td>
		<table class="inside_form_table">
			<tr>
				<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true; getActivityCategory();">
					<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?>  onClick="getActivityCategory();">
					<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
					<?=$selectSchoolYearHTML?>&nbsp;
					<?php 
					if($plugin['SIS_eEnrollment']){ 
						$hideSemester = ' style="display:none;"';
					}
					?>
					<span <?=$hideSemester?>>
						<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
						<span id="spanSemester"><?=$selectSemesterHTML?></span>
					</span>
				</td>
			</tr>
			<tr>
				<td onClick="document.form1.radioPeriod_Date.checked=true; getActivityCategory();"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE")?" checked":"" ?> onClick="getActivityCategory();"> <?=$i_From?> </td>
				<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate," onClick='getActivityCategory();' ")?>
				<br><span id='div_DateEnd_err_msg'></span></td>
				<td><?=$i_To?> </td>
				<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate," onClick='getActivityCategory();' ")?>
				
			</tr>
		</table>
	</td>
</tr>



<!-- Category -->
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span><?=$eEnrollment['add_activity']['act_category']?></td>
	<td>
		<table class="inside_form_table" width="100%">
			<tr>
				<td><?=$category_selection?></td>
			</tr>
			<tr>
				<td>
					<span id="CategoryElements"></span> <?=$activityCategoryHTMLBtn?>
				</td>
			</tr>
		</table>
		<span id='div_Category_err_msg'></span>
	</td>
</tr>

<!-- Time(s) -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"]?></td>
	<td><input type="text" name="times" id="times" size="3" value="0"/>
		<?=$linterface->Get_Radio_Button("timesOnAbove", "timesRange", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"])?>
		<?=$linterface->Get_Radio_Button("timesOnBelow", "timesRange", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"])?>
		<br><span id='div_Times_err_msg'></span>
	</td>
</tr>

<!-- Hour(s) -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"]?></td>
	<td><input type="text" name="hours" id="hours" size="3" value="0" />
		<?=$linterface->Get_Radio_Button("hoursOnAbove", "hoursRange", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"])?>
		<?=$linterface->Get_Radio_Button("hoursOnBelow", "hoursRange", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"])?>
		<br><span id='div_Hours_err_msg'></span>
	</td>
</tr>

<!-- Display Unit -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["DisplayUnit"]?></td>
	<td>
		<?=$linterface->Get_Radio_Button("displayUnitStudent", "displayUnit", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Student"])?>
		<?=$linterface->Get_Radio_Button("displayUnitClass", "displayUnit", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Class"])?>
	</td>
</tr>

<!-- Sort By -->
<tr>
	<td class="field_title"><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"]?></td>
	<td>
		<?=$linterface->Get_Radio_Button("sortByClassNameNumber", "sortBy", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"] . " / " . $Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"])?>
		<?=$linterface->Get_Radio_Button("sortByTimes", "sortBy", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"])?>
		<?=$linterface->Get_Radio_Button("sortByHours", "sortBy", 2, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"])?>
		<br>
		<?=$linterface->Get_Radio_Button("sortByOrderAsc", "sortByOrder", 0, true, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["AscendingOrder"])?>
		<?=$linterface->Get_Radio_Button("sortByOrderDesc", "sortByOrder", 1, false, "", $Lang["eEnrolment"]["ActivityParticipationReport"]["DescendingOrder"])?>				
	</td>
</tr>
</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
<p class="spacer"></p>
</div>

<? /* ?>
<input type="hidden" name="submit_flag" id="submit_flag" value="NO" />
<input type="hidden" name="categoryID" id="categoryID" value="<?=$categoryID?>" />
<input type="hidden" name="Period" id="Period" value="<?=$Period?>" />
<input type="hidden" name="submitSelectBy" id="submitSelectBy" value="<?=$rankTarget?>" />
<input type="hidden" name="StatType" id="StatType" value="<?=$StatType?>" />
<input type="hidden" name="PeriodField1" id="PeriodField1" value="<?=$parPeriodField1?>" />
<input type="hidden" name="PeriodField2" id="PeriodField2" value="<?=$parPeriodField2?>" />
<? if($submit_flag=="YES") {?>
<input type="hidden" name="ByField" id="ByField" value="<?=implode(",", $parByFieldArr)?>" />
<input type="hidden" name="StatsField" id="StatsField" value="<?=implode(",", $parStatsFieldArr)?>" />
<input type="hidden" name="StatsFieldText" id="StatsFieldText" value="<?=intranet_htmlspecialchars(stripslashes(implode(",", $parStatsFieldTextArr)))?>" />
<input type="hidden" name="newItemID" id="newItemID" value="<? if(sizeof($parStatsFieldArr)>0) echo implode(",", $parStatsFieldArr)?>">
<? } ?>
<input type="hidden" name="SelectedForm" id="SelectedForm" />
<input type="hidden" name="SelectedFormText" id="SelectedFormText" />
<input type="hidden" name="SelectedClass" id="SelectedClass" />
<input type="hidden" name="SelectedClassText" id="SelectedClassText" />
<input type="hidden" name="SelectedItemID" id="SelectedItemID" />
<input type="hidden" name="SelectedItemIDText" id="SelectedItemIDText" />
<input type="hidden" name="id" id="id" value="">
<input type="hidden" name="itemNameColourMappingArr" id="itemNameColourMappingArr" value="<?=rawurlencode(serialize($itemNameColourMappingArr));?>">
<input type="hidden" name="showStatButton" id="showStatButton" value="0" />


<? */ ?>

<input type="hidden" name="studentFlag" id="studentFlag" value="0">
</form>
	
<script language="javascript">
$(document).ready(function(){
	changeTerm($("#selectYear").val());
 	showRankTargetResult($("#rankTarget").val(), '');
 	getActivityCategory();
});
</script>	

<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>