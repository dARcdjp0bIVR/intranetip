<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

if (!$plugin['eEnrollment'] || !($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");


# Check access right
/*
if (!$plugin['eEnrollment'])
{
	echo "You have no priviledge to access this page.";
	exit();
}

$libenroll = new libclubsenrol();	
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))){
	echo "You have no priviledge to access this page.";
	exit();
}
*/


### Send email to students
$studentIDArr = unserialize(rawurldecode($studentIDArr));
$studentMailSubject = stripslashes(intranet_htmlspecialchars(addslashes($student_subject)));
$studentMailBody = stripslashes(intranet_htmlspecialchars(addslashes($student_message)));
$teacherMailBody = "<p>".str_replace("\n", "<br />", $teacherMailBody)."</p>";
/*
debug_r($studentIDArr);
debug_r("studentMailSubject = ".$studentMailSubject);
debug_r("studentMailBody = ".$studentMailBody);
*/

$laccess = new libaccess();
// send email
if ($laccess->retrieveAccessCampusmailForType(2))     # if can access campusmail
{
	$lcmail = new libcampusmail();		
	$successArr["student"] = $lcmail->sendMail($studentIDArr, $studentMailSubject, $studentMailBody);
}

### Send email to teachers
$ClassTeacherInfoArr = unserialize(rawurldecode($ClassTeacherInfoArr));
$studentClassNameMapping = unserialize(rawurldecode($studentClassNameMapping));
$teacherMailSubject = stripslashes(intranet_htmlspecialchars(addslashes($teacher_subject)));
$teacherMailBody = stripslashes(intranet_htmlspecialchars(addslashes($teacher_message)));
$teacherMailBody = str_replace("\n", "<br />", $teacherMailBody);
/*
debug_r($ClassTeacherInfoArr);
debug_r($studentClassNameMapping);
debug_r("teacherMailSubject = ".$teacherMailSubject);
debug_r("teacherMailBody = ".$teacherMailBody);
*/

$numClassTeacher = count($ClassTeacherInfoArr);
for ($i=0; $i<$numClassTeacher; $i++)
{
	$thisTeacherID = $ClassTeacherInfoArr[$i]["UserID"];
	$thisClassName = $ClassTeacherInfoArr[$i]["ClassName"];
	$thisStudentNameArr = $studentClassNameMapping[$thisClassName];
	
	$thisStudentList = "";
	$numStudentName = count($thisStudentNameArr);
	for ($j=0; $j<$numStudentName; $j++)
	{
		$thisCounter = $j + 1;
		$thisStudentName = $thisStudentNameArr[$j];
		
		$thisStudentList .= $thisCounter.". ";
		$thisStudentList .= $thisStudentName."<br />";
	}
	
	$thisMailBody = "<p>".$teacherMailBody."<br /><br />".$thisStudentList."</p>";
	$successArr[$thisTeacherID] = $lcmail->sendMail(array($thisTeacherID), $teacherMailSubject, $thisMailBody);
}

if (in_array(false, $successArr))
{
	$msg = "failed";
}
else
{
	$msg = "success";
}

intranet_closedb();
?>

<body>
	<form name="form1" action="notify_student.php" method="post">
		<input type="hidden" name="field" value="<?=$field?>">
		<input type="hidden" name="order" value="<?=$order?>">
		<input type="hidden" name="pageNo" value="<?=$pageNo?>">
		<input type="hidden" name="page_size_change" value="<?=$page_size_change?>">
		<input type="hidden" name="numPerPage" value="<?=$numPerPage?>">
		<input type="hidden" name="targetClassName" value="<?=$targetClassName?>">
		
		<input type="hidden" name="msg" value="<?=$msg?>">
	</form>
</body>

<script language="javascript">
	document.form1.submit();
</script>
