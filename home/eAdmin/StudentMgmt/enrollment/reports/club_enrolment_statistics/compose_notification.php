<?php
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();

if (!$plugin['eEnrollment'] || !($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");



# Check access right
if (!$plugin['eEnrollment'])
{
	echo "You have no priviledge to access this page.";
	exit();
}

$libenroll = new libclubsenrol();	
if (!($libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && !($libenroll->IS_ENROL_MASTER($_SESSION['UserID']))){
	echo "You have no priviledge to access this page.";
	exit();
}

# Menu Display
$linterface = new interface_html("popup.html");
$MODULE_OBJ['title'] = $eEnrollment['send_club_enrolment_notification'];
$linterface->LAYOUT_START();

# Initialization
$libenroll = new libclubsenrol();

if (is_array($EmailStudentID))
{
	$studentIDArr = $EmailStudentID;
	$studentList = implode(",", $EmailStudentID);
}
else
{
	$studentIDArr = array($EmailStudentID);
	$studentList = $EmailStudentID;
}
	

# get student name, class and class number for display
$name_field = getNameFieldWithClassNumberByLang();
$sql = "SELECT $name_field, ClassName FROM INTRANET_USER WHERE UserID IN ($studentList) ORDER BY ClassName,ClassNumber";
$studentDisplayArr = $libenroll->returnArray($sql, 2);

# build an associate array $studentClassNameMapping[#ClassName] = #array of Student Name
$studentClassNameMapping = array();
for ($i=0; $i<count($studentDisplayArr); $i++)
{
	$thisStudentName = $studentDisplayArr[$i][0];
	$thisClassName = $studentDisplayArr[$i][1];
	
	if (is_array($studentClassNameMapping[$thisClassName]))
	{
		$studentClassNameMapping[$thisClassName][] = $thisStudentName;
	}
	else
	{
		$studentClassNameMapping[$thisClassName] = array();
		$studentClassNameMapping[$thisClassName][] = $thisStudentName;
	}
}

$StudentRowHTML = "";
for ($i=0; $i<count($studentDisplayArr); $i++)
{
	$thisStudentName = $studentDisplayArr[$i][0];
	
    # show "To" in the first row
	if ($i==0)
	{
		$StudentRowHTML .= "<tr><td class=\"tabletext\" width=\"30%\" valign=\"top\">";
		$StudentRowHTML .= $eEnrollment['to_student'];
	}
	else if ($i==count($studentDisplayArr)-1)
	{
		$StudentRowHTML .= "<tr><td class=\"tabletext formfieldtitle\" width=\"30%\" valign=\"top\">";
		$StudentRowHTML .= "&nbsp;";
	}
	else
	{
		$StudentRowHTML .= "<tr><td class=\"tabletext\" width=\"30%\" valign=\"top\">";
		$StudentRowHTML .= "&nbsp;";
	}
	$StudentRowHTML .= "</td>";
	
	$StudentRowHTML .= "<td class=\"tabletext\">".$thisStudentName."</td>";
	$StudentRowHTML .= "</tr>";
}

# Student mail default title and message
$AppEnd = $libenroll->AppEnd;
$AppEndHour = $libenroll->AppEndHour;
$AppEndMin = $libenroll->AppEndMin;
$deadline = $AppEnd." ".$AppEndHour.":".$AppEndMin;

$studentMailTitle = $eEnrollment['default_student_notification_title'];
$studentMailTitle = str_replace("<--deadline-->", $deadline, $studentMailTitle);

$studentMailContent = $eEnrollment['default_student_notification_content'];
$studentMailContent = str_replace("<--deadline-->", $deadline, $studentMailContent);

$studentTextArea = $linterface->GET_TEXTAREA("student_message", $studentMailContent, $taCols=55, $taRows=15, "", 2);

$StudentRowHTML .= "</tr> \n";
	$StudentRowHTML .= "<td class=\"tabletext formfieldtitle\" valign=\"top\">".$i_email_subject."</td> \n ";
	$StudentRowHTML .= "<td class=\"tabletext\"><input class='textboxtext' type='text' name='student_subject' value=\"".$studentMailTitle."\"></td> \n ";
$StudentRowHTML .= "</tr> \n ";
$StudentRowHTML .= "<tr> \n ";
	$StudentRowHTML .= "<td class=\"tabletext formfieldtitle\" valign=\"top\">".$eComm['Content']."</td> \n ";
	$StudentRowHTML .= "<td>".$studentTextArea."</td> \n ";
$StudentRowHTML .= "</tr> \n ";



## Class teacher list
$libclass = new libclass();

# Get all classes name of student
//$sql = "SELECT DISTINCT(ClassName) FROM INTRANET_USER WHERE UserID IN ($studentList) AND ClassName != \"\" AND ClassName IS NOT NULL";
$sql = "Select 
				yc.YearClassID 
		From 
				YEAR_CLASS as yc 
				Inner Join
				YEAR_CLASS_USER as ycu
				On yc.YearClassID = ycu.YearClassID
		Where
				ycu.UserID IN ($studentList)
				And
				yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
		";
$ClassIDArr = $libenroll->returnVector($sql);
$ClassIDList = "'".implode("','", $ClassIDArr)."'";

$name_field = getNameFieldByLang('iu.');
/*
$sql = "SELECT 
				c.UserID,
				$name_field as CTeacher,
				a.ClassName
        FROM 
        		INTRANET_CLASSTEACHER as b 
        		LEFT OUTER JOIN 
        		INTRANET_CLASS as a ON a.ClassID = b.ClassID
        		LEFT OUTER JOIN 
        		INTRANET_USER as c on c.UserID=b.UserID
        WHERE 
        		a.ClassName IN ($ClassNameList)
        ORDER BY
        		a.ClassName
        ";
*/
$sql = "Select
				yct.UserID,
				$name_field as CTeacher,
				yc.YearClassID,
				yc.ClassTitleEN as ClassName
		From
				YEAR_CLASS as yc
				Inner Join
				YEAR_CLASS_TEACHER as yct
				On yc.YearClassID = yct.YearClassID
				Inner Join
				INTRANET_USER as iu
				On yct.UserID = iu.UserID
		Where
				yc.YearClassID In ($ClassIDList)
		";
$ClassTeacherInfoArr = $libenroll->returnArray($sql);


# check if there are any classes that have no class teacher
$ClassWithClassTeacherArr = array();
$ClassWithoutClassTeacherArr = array();

for ($i=0; $i<count($ClassTeacherInfoArr); $i++)
{
	if (!in_array($ClassTeacherInfoArr[$i]["YearClassID"], $ClassWithClassTeacherArr))
		$ClassWithClassTeacherArr[] = $ClassTeacherInfoArr[$i]["YearClassID"];
}
$ClassWithoutClassTeacherArr = array_diff($ClassIDArr, $ClassWithClassTeacherArr);

$TeacherRowHTML = "";
## Separator
$TeacherRowHTML .= "<tr><td class=\"dotline\" colspan=\"2\"><img src=\"".$image_path."/".$LAYOUT_SKIN."/10x10.gif\" width=\"10\" height=\"1\"></td></tr>";
$TeacherRowHTML .= "<tr><td colspan=\"2\">&nbsp;</td></tr>";

# display classes which do not have class teacher
if (count($ClassWithoutClassTeacherArr) > 0)
{
	# build class name list for display
	$displayClassList = "";
	$numWithoutClassTeacher = count($ClassWithoutClassTeacherArr);
	
	foreach ($ClassWithoutClassTeacherArr as $key => $thisClassName)
	{
		$displayClassList .= $thisClassName;
		if ($i < $numClassName-2)
		{
			$displayClassList .= ", ";
		}
		else if ($i == $numClassName-2)
		{
			$displayClassList .= " ".$eEnrollment['and']." ";
		}
	}
	
	$warning = str_replace("<--ClassList-->", $displayClassList, $eEnrollment['no_class_teacher_warning']);
	$TeacherRowHTML .= "<tr><td colspan=\"2\" class=\"tabletext\"><i><font color=\"red\">".$warning."</font></i></td></tr>";
}

# display class teacher email subject and content
if (count($ClassTeacherInfoArr) > 0)
{
	for ($i=0; $i<count($ClassTeacherInfoArr); $i++)
	{
		$thisTeacherName = $ClassTeacherInfoArr[$i]["CTeacher"];
		$thisClassName = $ClassTeacherInfoArr[$i]["ClassName"];
		$thisDisplay = $thisTeacherName." (".$thisClassName.")";
		
	    # show "To" in the first row
		if ($i==0)
		{
			$TeacherRowHTML .= "<tr><td class=\"tabletext\" width=\"30%\" valign=\"top\">";
			$TeacherRowHTML .= $eEnrollment['to_class_teacher'];
		}
		else if ($i==count($ClassTeacherInfoArr)-1)
		{
			$TeacherRowHTML .= "<tr><td class=\"tabletext formfieldtitle\" width=\"30%\" valign=\"top\">";
			$TeacherRowHTML .= "&nbsp;";
		}
		else
		{
			$TeacherRowHTML .= "<tr><td class=\"tabletext\" width=\"30%\" valign=\"top\">";
			$TeacherRowHTML .= "&nbsp;";
		}
		$TeacherRowHTML .= "</td>";
		
		$TeacherRowHTML .= "<td class=\"tabletext\">".$thisDisplay."</td>";
		$TeacherRowHTML .= "</tr>";
	}
	
	
	# Class teacher mail default title and message
	$teacherMailTitle = $eEnrollment['default_class_teacher_notification_title'];
	$teacherMailTitle = str_replace("<--deadline-->", $deadline, $teacherMailTitle);
	
	$teacherMailContent = $eEnrollment['default_class_teacher_notification_content'];
	$teacherMailContent = str_replace("<--deadline-->", $deadline, $teacherMailContent);
	
	$teacherTextArea = $linterface->GET_TEXTAREA("teacher_message", $teacherMailContent, $taCols=55, $taRows=15, "", 4);

	$TeacherRowHTML .= "<tr>";
		$TeacherRowHTML .= "<td class=\"tabletext formfieldtitle\" valign=\"top\">". $i_email_subject ."</td>";
		$TeacherRowHTML .= "<td class=\"tabletext\"><input class='textboxtext' type='text' name='teacher_subject' value=\"".$teacherMailTitle."\"></td>";
	$TeacherRowHTML .= "</tr>";
	$TeacherRowHTML .= "<tr>";
		$TeacherRowHTML .= "<td class=\"tabletext formfieldtitle\" valign=\"top\">".$eComm['Content']."</td>";
		$TeacherRowHTML .= "<td>". $teacherTextArea ."</td>";
	$TeacherRowHTML .= "</tr>";
	$TeacherRowHTML .= "<tr>";
		$TeacherRowHTML .= "<td class=\"tabletextremark\" valign=\"top\">&nbsp;</td>";
		$TeacherRowHTML .= "<td class=\"tabletextremark\" valign=\"top\"><font color=\"red\">*</font> ".$eEnrollment['system_append_student_list_atuomatically']."</td>";
	$TeacherRowHTML .= "</tr>";
}



?>

<script language="javascript">

function jsBack(){
	document.form1.action = "notify_student.php";
	document.form1.submit();
}

function jsClose(){
	if (confirm("<?=$eEnrollment['js_close_compose_email_window']?>"))
	{
		window.close();
	}
	else
	{
		return true;
	}
}

function jsSend(){
	document.form1.action = "send_notification.php";
	document.form1.submit();
}

</script>

<form name="form1" method="POST">
	<br />
	<br />
	<table width="90%" border="0" cellpadding="3" cellspacing="0">
		<?=$StudentRowHTML?>
		
		<tr><td>&nbsp;</td></tr>
		
		
		<?=$TeacherRowHTML?>
		
		
		
		

	</table>
	<br />
	
	<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
		<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
		<tr><td align="center" colspan="6">
			<div style="padding-top: 5px">
				<?= $linterface->GET_ACTION_BTN($button_send, "button", "jsSend()")?>
				<?= $linterface->GET_ACTION_BTN($button_back, "button", "jsBack()")?>
				<?= $linterface->GET_ACTION_BTN($button_close, "button", "jsClose()")?>
			</div>
		</td></tr>
	</table>
	<br />
	<br />
	
	<!-- For back to notify_student.php -->
	<input type="hidden" name="field" value="<?=$field?>">
	<input type="hidden" name="order" value="<?=$order?>">
	<input type="hidden" name="pageNo" value="<?=$pageNo?>">
	<input type="hidden" name="page_size_change" value="<?=$page_size_change?>">
	<input type="hidden" name="numPerPage" value="<?=$numPerPage?>">
	<input type="hidden" name="targetClassName" value="<?=$targetClassName?>">
	<input type="hidden" name="EmailStudentIDList" value="<?=$EmailStudentIDList?>">
	
	<!-- For send email -->
	<input type="hidden" name="studentIDArr" value="<?=rawurlencode(serialize($studentIDArr));?>">
	<input type="hidden" name="studentClassNameMapping" value="<?=rawurlencode(serialize($studentClassNameMapping));?>">
	<input type="hidden" name="ClassTeacherInfoArr" value="<?=rawurlencode(serialize($ClassTeacherInfoArr));?>">
</form>

<?		
$linterface->LAYOUT_STOP();
intranet_closedb();
?>

