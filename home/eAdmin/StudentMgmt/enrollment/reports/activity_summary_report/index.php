<?php
// Editing by 
/*
 * 2014-04-16 (Carlos): Created
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/iportfolio_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_cust.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol_cust();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ($plugin['eEnrollmentLite'] || ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (count($classInfo)==0) &&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) or !$plugin['eEnrollment']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();
$LibPortfolio = new libpf_slp();
//$DefaultELEArray = $LibPortfolio->GET_ELE();

# setting the current page
$CurrentPage = "PageActivitySummaryReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['ActivitySummaryReport']['ActivitySummaryReport'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 
# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
//$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='hideSpan(\"spanStudent\"); changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); document.form1.rankTarget.selectedIndex=0; showResult(\"form\",\"\")'", "", $selectYear);
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";
/*
$column_list = array();
$column_list[] = array('EventTitle',$eEnrollment['act_name']);
$column_list[] = array('GroupTitle',$Lang['eEnrolment']['ActivitySummaryReport']['GroupOrClub']);
$column_list[] = array('ActivityCode',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityCode']);
$column_list[] = array('InternalOrExternal',$Lang['eEnrolment']['ActivitySummaryReport']['InternalOrExternal']);
$column_list[] = array('PersonInCharge',$Lang['eEnrolment']['ActivitySummaryReport']['PersonInCharge']);
$column_list[] = array('SupportTeachers',$Lang['eEnrolment']['ActivitySummaryReport']['SupportTeachers']);
$column_list[] = array('ActivityGoal',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityGoal']);
$column_list[] = array('ActivityNature',$Lang['eEnrolment']['Nature']);
$column_list[] = array('OLEType',$Lang['eEnrolment']['ActivitySummaryReport']['OLEType']);
$column_list[] = array('Target',$eEnrollment['Target']);
$column_list[] = array('IsVolunteerService',$Lang['eEnrolment']['VolunteerService']);
$column_list[] = array('ActivityStartDate',$Lang['eEnrolment']['StartDate']);
$column_list[] = array('ActivityEndDate',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityEndDate']);
$column_list[] = array('NumbrOfActivity',$Lang['eEnrolment']['ActivitySummaryReport']['NumberOfActivity']);
$column_list[] = array('NumberOfParticipants',$Lang['eEnrolment']['ActivitySummaryReport']['NumberOfParticipants']);
$column_list[] = array('ActivityTotalTime',$Lang['eEnrolment']['ActivitySummaryReport']['ActivityTotalHour']);
$column_list[] = array('JointActivity',$Lang['eEnrolment']['ActivitySummaryReport']['JointActivity']);
$column_list[] = array('SchoolOrganizedActivity',$Lang['eEnrolment']['ActivitySummaryReport']['SchoolOrganizedActivity']);
$column_list[] = array('ExternalOrganizationOrganizedActivity',$Lang['eEnrolment']['ActivitySummaryReport']['ExternalOrganizationOrganizedActivity']);
$column_list[] = array('Organiser',$Lang['eEnrolment']['ActivitySummaryReport']['Organiser']);
$column_list[] = array('OrganisedGroupOrganizationName',$Lang['eEnrolment']['ActivitySummaryReport']['OrganisedGroupOrganizationName']);
$column_list[] = array('AwardedInActivity',$Lang['eEnrolment']['ActivitySummaryReport']['AwardedInActivity']);
$column_list[] = array('AwardType',$Lang['eEnrolment']['ActivitySummaryReport']['AwardType']);
$column_list[] = array('ChineseAuthorityName',$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAuthorityName']);
$column_list[] = array('EnglishAuthorityName',$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAuthorityName']);
$column_list[] = array('ChineseAwardName',$Lang['eEnrolment']['ActivitySummaryReport']['ChineseAwardName']);
$column_list[] = array('EnglishAwardName',$Lang['eEnrolment']['ActivitySummaryReport']['EnglishAwardName']);
$column_list[] = array('AwardWinners',$Lang['eEnrolment']['ActivitySummaryReport']['AwardWinners']);
*/
$column_list = $libenroll->getActivitySummaryReportColumnArray();
$selected_columns = Get_Array_By_Key($column_list,0);

$column_selection = $linterface->GET_SELECTION_BOX($column_list,' id="sel_column" name="sel_column[]" size="10" multiple="multiple" ','',$selected_columns);
$selectAllBtn .= " ".$linterface->GET_BTN($button_select_all, "button", "Select_All_Options('sel_column',true);return false;", "selectAllBtn");

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">
function changeTerm(val) {
	
	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
	
	$.get(
		'../../ajaxGetSemester.php?year='+val+'&term=<?=$selectSemester?>'+'&field=selectSemester',
		{},
		function(responseText){
			document.getElementById("spanSemester").innerHTML = responseText;
			document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
		}
	);
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
}

function hideOptionLayer()
{
	//$('.Form_Span').attr('style', 'display: none');
	$('.Form_Span').hide();
	$('.spanHideOption').hide();
	$('.spanShowOption').show();
	//$('.spanHideOption').attr('style', 'display: none');
	//$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	//$('.Form_Span').attr('style', '');
	$('.Form_Span').show();
	$('.spanHideOption').show();
	$('.spanShowOption').hide();
	//$('.spanShowOption').attr('style', 'display: none');
	//$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	var radio_period = $('input[name="radioPeriod"]:checked').val();
	var sel_column_count = $('#sel_column option:selected').length;
	
	if(radio_period == 'DATE'){
		var from_date = $.trim($('#textFromDate').val());
		var to_date = $.trim($('#textToDate').val());
		
		if(from_date == '' || to_date == ''){
			valid = false;
			//$('#DateWarnDiv span').html('<?=$Lang['General']['InvalidDateFormat']?>');
			//$('#DateWarnDiv').show();
			$('#textFromDate').focus();
		}
		if(valid){
			if(!check_date_without_return_msg(document.getElementById('textFromDate'))){
				valid = false;
				//$('#DateWarnDiv span').html('<?=$Lang['General']['InvalidDateFormat']?>');
				//$('#DateWarnDiv').show();
				$('#textFromDate').focus();
			}
			if(!check_date_without_return_msg(document.getElementById('textToDate'))){
				valid = false;
				//$('#DateWarnDiv span').html('<?=$Lang['General']['InvalidDateFormat']?>');
				//$('#DateWarnDiv').show();
				$('#textToDate').focus();
			}
		}
		if(valid){
			if(from_date > to_date){
				valid = false;
				$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
				$('#DateWarnDiv').show();
				$('#textFromDate').focus();
			}
		}
		
		if(valid){
			$('#DateWarnDiv span').html('');
			$('#DateWarnDiv').hide();
		}
	}
	
	if(sel_column_count == 0){
		$('#ColumnWarnDiv').show();
		$('#sel_column').focus();
		valid = false;
	}else{
		$('#ColumnWarnDiv').hide();
	}
	
	if(!valid) return;
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target','');
		Block_Element('PageDiv');
		
		$.post(
			'report.php',
			$('#form1').serialize(),
			function(data){
				$('#ReportDiv').html(data);
				document.getElementById('div_form').className = 'report_option report_hide_option';
				$('.spanShowOption').show();
				$('.spanHideOption').hide();
				$('.Form_Span').hide();
				UnBlock_Element('PageDiv');
			}
		);
	}else if(format == 'csv'){
		form_obj.attr('target','');
		form_obj.submit();
	}else if(format == 'print'){
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

$(document).ready(function(){
	changeTerm($("#selectYear").val());
});
</script>

<div id="PageDiv">
<div id="div_form">
	<span id="spanShowOption" class="spanShowOption" style="display:none">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php" onsubmit="submitForm('web');return false;">
		<table class="form_table_v30">
			<!-- Period -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true;">
								<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?> >
								<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
								<?=$selectSchoolYearHTML?>&nbsp;
								<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
								<span id="spanSemester"><?=$selectSemesterHTML?></span>
								
							</td>
						</tr>
						<tr>
							<td onClick="document.form1.radioPeriod_Date.checked=true;"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE")?" checked":"" ?>> <?=$i_From?> </td>
							<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
							<br><span id='div_DateEnd_err_msg'></span></td>
							<td><?=$i_To?> </td>
							<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
								<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- Category -->
			<tr>
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eEnrolment']['ActivitySummaryReport']['DisplayData']?></td>
				<td>
					<table class="inside_form_table" width="100%">
						<tr>
							<td><?=$column_selection.'&nbsp;'.$selectAllBtn?></td>
						</tr>
						<tr><td class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
					</table>
					<span id='div_column_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("ColumnWarnDiv",$i_alert_pleaseselect.$Lang['eEnrolment']['ActivitySummaryReport']['DisplayData'])?>
				</td>
			</tr>
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
		<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="format" id="format" value="web">
		</form>
	</span>
</div>
<div id="ReportDiv"></div>
</div>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>