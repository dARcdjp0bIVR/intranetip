<?php
## Modifying By: 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) or !$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}


$linterface = new interface_html();

# setting the current page
$CurrentPage = "PageStudentEnrolmentReport";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['StudentEnrolmentReport'].$Lang['eEnrolment']['tkogss_crossyear'], "index.php", 0);
$TAGS_OBJ[] = array($Lang['eEnrolment']['StudentEnrolmentReport'].$Lang['eEnrolment']['tkogss_eachyear'], "student.php", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();	

$AcademicYearID = Get_Current_Academic_Year_ID(); 

# School year
if(!isset($selectYear) || count($selectYear) == 0) $selectYear = array($AcademicYearID);
$years = $libenroll->returnAllYearsSelectionArray();
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear[]' id='selectYear'", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\">";
$selectSemesterHTML .= "</select>";


$linterface->LAYOUT_START();

?>

<script language="javascript">
<!--

function goPrint() {
	document.form1.action = "student_print.php";
	document.form1.target = "_blank";
	document.form1.submit();
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showOption(){
	hideSpan('spanShowOption');
	showSpan('spanHideOption');
	showSpan('spanOptionContent');
	document.getElementById('tdOption').className = '';
}

function hideOption(){
	hideSpan('spanOptionContent');
	hideSpan('spanHideOption');
	showSpan('spanShowOption');
	document.getElementById('tdOption').className = 'report_show_option';
}

function showResult(str, choice)
{

	if(str != "student")
		document.getElementById("studentFlag").value = 0;
		
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
		
	xmlHttp = GetXmlHttpObject()

	if (xmlHttp==null)
		{
			alert ("Browser does not support HTTP Request")
			return
		} 
		
	url = "../../get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	url += "&year=<?=$AcademicYearID?>";
	url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>		
	xmlHttp.onreadystatechange = stateChanged 
	
	xmlHttp.open("GET",url,true)
	xmlHttp.send(null)
} 

function stateChanged() 
{ 
	
	var showIn = "";
	if(document.getElementById("studentFlag").value == 0) {
		showIn = "rankTargetDetail";
	} else {
		showIn = "spanStudent";	
	}
	
	if (xmlHttp.readyState==4 || xmlHttp.readyState=="complete")
	{ 
		document.getElementById(showIn).innerHTML = xmlHttp.responseText;
		document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		
	} 
}

function showRankTargetResult(val, choice) {

	showResult(val,choice);
	if (val=='student') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}


function studentSelection(val) {
	var rank = document.getElementById('rankTargetDetail[]');
	for(i=0; i<rank.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_Target_err_msg').innerHTML = "";
}

function checkForm()
{
	var obj = document.form1;
	
	var error_no = 0;
	var focus_field = "";
	var choiceSelected;
	
	//// Reset div innerHtml
	reset_innerHtml();
	
		
	///// Target
	if (document.getElementById('rankTarget').value=='form' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Form?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if (document.getElementById('rankTarget').value=='class' && countOption(document.getElementById('rankTargetDetail[]'))<1) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$i_Discipline_Class?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if(document.getElementById('rankTarget').value == "student") 
	{
		choiceSelected = 0;
		choice = document.getElementById('studentID[]');
	
		for(i=0;i<choice.options.length;i++) {
			if(choice.options[i].selected==true)
				choiceSelected = 1;
		}
		
		if(choiceSelected==0) 
		{
			document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
			error_no++;
			if(focus_field=="")	focus_field = "rankTarget";
		}
	}
	
	choice = document.getElementById('rankTargetDetail[]');
	choiceSelected = 0;
	
	for(i=0;i<choice.options.length;i++) {
		if(choice.options[i].selected==true)
			choiceSelected = 1;
	}
	
	if(choiceSelected==0) 
	{
		document.getElementById('div_Target_err_msg').innerHTML = '<font color="red"><?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?></font>';
		error_no++;
		if(focus_field=="")	focus_field = "rankTarget";
	}
	
	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
		document.form1.target = "_self";
		document.form1.action = "student_result.php";
		return true
	}
}

//-->
</script>

<form name="form1" method="post" action="student_result.php" onSubmit="return checkForm();">

<table class="form_table_v30">

	<!-- Title -->
	<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eEnrolment']['tkogss_title']?></td>
		<td>
			<input type="text" name="title" id="title" value="<?=$Lang['eEnrolment']['StudentEnrolmentReport']?>" class="textbox">	
		</td>
	</tr>

	<!-- Period -->
	<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['General']['SchoolYear']?></td>
		<td>
			<?=$selectSchoolYearHTML?>
			<span id='div_SchoolYear_err_msg'></span>
		</td>
	</tr>
	
	<!-- Target -->
	<tr valign="top">
		<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eEnrolment']['tkogss_target']?></td>
		<td>
			<table class="inside_form_table">
				<tr>
					<td valign="top">
						<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
							<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
							<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
							<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
						</select>
					</td>
					<td valign="top" nowrap>
						<!-- Form / Class //-->
						<span id='rankTargetDetail'>
						<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="7"></select>
						</span>
						<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
							
						<!-- Student //-->
						<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
							<select name="studentID[]" multiple size="7" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('studentID[]'), true);return false;")?>
						</span>
						
					</td>
				</tr>
				<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
			</table>
			<span id='div_Target_err_msg'></span>
		</td>
	</tr>
</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
<p class="spacer"></p>
</div>


<input type="hidden" name="studentFlag" id="studentFlag" value="0">
<input type="hidden" name="SubmitFlag" id="SubmitFlag" value="1">
</form>

	
<script language="javascript">
$(document).ready(function(){
 	showRankTargetResult($("#rankTarget").val(), '<?if(count($rankTargetDetail)) echo implode(',',$rankTargetDetail);?>');
 	
 	initialStudent();	
 	
});

		
	function initialStudent() {
		xmlHttp2 = GetXmlHttpObject()
		url = "../../get_live.php?target=student2ndLayer&value=<? if(count($rankTargetDetail)>0) echo implode(',',$rankTargetDetail)?>&rankTargetDetail=<? if(count($rankTargetDetail)>0) echo (implode(',',$rankTargetDetail))?>&student=1&studentid=<? if(sizeof($studentID)>0) echo implode(',',$studentID);?>";
		xmlHttp2.onreadystatechange = stateChanged2
		xmlHttp2.open('GET',url,true);
		xmlHttp2.send(null);
		
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
	}
	
	function stateChanged2() 
	{ 
		if (xmlHttp2.readyState==4 || xmlHttp2.readyState=="complete")
		{ 
			document.getElementById('spanStudent').innerHTML = xmlHttp2.responseText;
			document.getElementById('spanStudent').style.border = "0px solid #A5ACB2";
			
			//SelectAll(form1.elements['studentID[]']);
		} 
	}
</script>	

<?
intranet_closedb();
$linterface->LAYOUT_STOP();

?>