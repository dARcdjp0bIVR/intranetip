<?php
## Modifying By: 
/*
 * Change Log:
 * Date:	2017-03-30 Frankie - handle User's Date Range
 * Date 2017-01-26 Villa: B112166 Allow 0hours/ times
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

##### make sure form submit
if(empty($radioPeriod))
{
	header("Location: index.php");
	exit;
}

# user access right checking
$libenroll = new libclubsenrol();
$le = new libexporttext();

$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (count($classInfo)==0) or !$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$rankTargetDetail_str = implode(",",(array)$rankTargetDetail);
if($rankTarget!="student")	$studentID = array();
$selectedClub = implode(",",(array)$activityCategory);

# prepare report data 
if ($radioPeriod == "YEAR") 
{
	$selectSemester = $selectSemester ? $selectSemester : "";
	$SQL_startdate = substr(getStartDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
	$SQL_enddate = substr(getEndDateOfAcademicYear($selectYear, $selectSemester), 0, 10);
}
else
{
	$SQL_startdate = $textFromDate;
	$SQL_enddate = $textToDate;
}
if (sizeof($studentID)>0) {
	$studentID_str = implode(",",$studentID);
} else {
	$userList = $libenroll->getTargetUsers($rankTarget,$rankTargetDetail, "vector");
	$studentID_str = implode(",",$userList);
}

# times sql
// if($times)
// {
	$times_sym = $timesRange ? "<=" : ">=";
	$having_sql[] = " times $times_sym $times ";
// }
// if($hours)
// {
	$hours_sym = $hoursRange ? "<=" : ">=";
	$having_sql[] = " hours $hours_sym '". ($hours<10?"0":"") ."$hours:00:00' ";
// }
$having_con = !empty($having_sql) ? "having " . implode(" and " , $having_sql) : "";

$sortType = $sortByOrder ? "desc" : "asc";
switch($sortBy)
{
	case 0:
		$order_by = "d.ClassName $sortType, d.ClassNumber $sortType";	
		break;
	case 1:
		$order_by = "times $sortType";	
		break;
	case 2:
		$order_by = "hours $sortType";	
		break;
}

$name_field = getNameFieldByLang("d.");
// $sql ="
// 	select 
// 		c.StudentID as UserID,  d.ClassName, d.ClassNumber, $name_field, count(c.EnrolEventID) as times,
// 		SEC_TO_TIME(SUM(TIME_TO_SEC(IF (TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart) < TIME('00:00:00'), '00:00:00', TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart))))) as hours
// 	from 
// 		INTRANET_ENROL_EVENTSTUDENT as c
// 		inner join INTRANET_USER as d on (d.UserID=c.StudentID and d.RecordType=2 and c.RecordStatus = 2)
// 		inner join INTRANET_ENROL_EVENT_DATE as e on (e.EnrolEventID=c.EnrolEventID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
// 		inner join INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=e.EventDateID and f.StudentID=d.UserID and (f.RecordStatus =1 Or f.RecordStatus = 2))
// 	where
// 		c.EnrolEventID in ($selectedClub)
// 		and c.StudentID in ($studentID_str)
// 	group by 
// 		d.UserID
// 	$having_con
// 	order by
// 		$order_by, d.ClassName, d.ClassNumber
// ";
$sql ="
		SELECT
			c.StudentID as UserID,
			d.ClassName,
			d.ClassNumber,
			$name_field,
			SUM(
				IF(f.RecordStatus=3,'0','1')
			) as times,
			SEC_TO_TIME(
				SUM(
					TIME_TO_SEC(
						IF( f.RecordStatus=3,
							'00:00:00',
							IF( TIMEDIFF(e.ActivityDateEnd,e.ActivityDateStart)<TIME('00:00:00'),
								'00:00:00',
								TIMEDIFF(e.ActivityDateEnd, e.ActivityDateStart)
							)
						)
					)
				)
			) as hours,
			f.RecordStatus
		FROM
			INTRANET_ENROL_EVENTSTUDENT as c
		INNER JOIN
			INTRANET_USER as d on (d.UserID=c.StudentID and d.RecordType=2 and c.RecordStatus = 2)
		INNER JOIN
			INTRANET_ENROL_EVENT_DATE as e on (e.EnrolEventID=c.EnrolEventID and (e.RecordStatus Is Null Or e.RecordStatus = 1) and (DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') >= '$SQL_startdate' and DATE_FORMAT(e.ActivityDateStart,'%Y-%m-%d') <= '$SQL_enddate'))
		INNER JOIN
			INTRANET_ENROL_EVENT_ATTENDANCE as f on (f.EventDateID=e.EventDateID and f.StudentID=d.UserID";
if ($libenroll->enableUserJoinDateRange()) {
	$sql .=" 
		AND (EnrolAvailiableDateStart IS NULL OR EnrolAvailiableDateStart LIKE '0000-00-00%' OR EnrolAvailiableDateStart <= ActivityDateStart)
		AND (EnrolAvailiableDateEnd IS NULL OR EnrolAvailiableDateEnd LIKE '0000-00-00%' OR EnrolAvailiableDateEnd >= ActivityDateEnd)";
}
$sql .=")
		WHERE
			c.EnrolEventID in ($selectedClub)
		AND
			c.StudentID in ($studentID_str)
		GROUP BY
			d.UserID
		$having_con
		ORDER BY
			$order_by, d.ClassName, d.ClassNumber
";
$result = $libenroll->returnArray($sql);		

$data_ary = array();
$display = "";

if(!empty($result))
{
	$export_ary = array();
	
	if($displayUnit==0)	# student
	{
		### build data array
		foreach($result as $k=>$d)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d;
			$data_ary[$this_ClassName][] = array($this_UserID, $this_ClassNumber, $this_student, $this_times, $this_hours);
		}
		
		### build display table
		foreach($data_ary as $this_ClassName=>$d)
		{
			$export_ary[] = array($i_ClassName,$i_ClassNumber,$i_UserName,$Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"],$Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"]);
			
			$this_class_total_times = 0;
			$this_class_total_hours = 0;
			foreach($d as $k1=>$d1)
			{
				list($this_UserID, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d1;
				$this_class_total_times += $this_times;
				$this_hr = $libenroll->timeToDecimal($this_hours);
				$this_class_total_hours += $this_hr;
				
				$export_ary[] = array($this_ClassName,$this_ClassNumber,$this_student,$this_times,$this_hr);
				
			}
			
			$export_ary[] = array('','',$list_total,$this_class_total_times,$this_class_total_hours);
			$export_ary[] = array();
		}
	}
	else				# class
	{
		### build data array
		foreach($result as $k=>$d)
		{
			list($this_UserID, $this_ClassName, $this_ClassNumber, $this_student, $this_times, $this_hours) = $d;
			$this_hr = $libenroll->timeToDecimal($this_hours);
			$data_ary[$this_ClassName]['t'] += $this_times;
			$data_ary[$this_ClassName]['h'] += $this_hr;
		}
		
		if($sortBy!=0)
		{
			# sorting 
			foreach ($data_ary as $key => $row) {
			    $t[$key]  = $row['t'];
			    $h[$key] = $row['h'];
			}

			switch($sortBy)
			{
				case 1:
					$sort_field = $t;	
					break;
				case 2:
					$sort_field = $h;	
					break;
			}
			$sort_order = $sortByOrder ? SORT_DESC : SORT_ASC;
			array_multisort($sort_field, $sort_order, $data_ary);
		}

		$export_ary[] = array($i_ClassName,$Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"],$Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"]);
		
		$this_class_total_times = 0;
		$this_class_total_hours = 0;
		foreach($data_ary as $this_ClassName=>$d)
		{
			$this_class_total_times += $d[t];
			$this_class_total_hours += $d[h];
			
			$export_ary[] = array($this_ClassName,$d[t],$d[h]);
		}
		$export_ary[] = array($list_total,$this_class_total_times,$this_class_total_hours);
	}
}

$utf_content = "";
foreach($export_ary as $k=>$d)
{
	$utf_content .= implode("\t", $d);
	$utf_content .= "\r\n";
}

intranet_closedb();

$filename = "activity_participation_report.csv";
$le->EXPORT_FILE($filename, $utf_content); 
	
	

?>