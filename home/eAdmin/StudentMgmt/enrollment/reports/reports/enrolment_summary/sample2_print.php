<?php
############## Change Log [start] ####
#
#	
#
#############################################


$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{
	$linterface = new interface_html("popup.html");
	$libenroll = new libclubsenrol();
	$libenroll_ui = new libclubsenrol_ui();
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC()))
		header("Location: ../../enrollment/");
		
	$applyPageBreak = $_POST['simpleApplyPageBreak'];
	if ($applyPageBreak) {
		$pageBreakStyle = 'page-break-after:always;';
	}
		$GroupStudentAssoc = unserialize(rawurldecode($exportGroupStudentAssoc));
		
		$DateRowHTML = "";
		if ($usePeriod == "on") {
			$DateRowHTML .= "<tr class=\"tabletext\"align=\"left\">";
				$DateRowHTML .= "<td>".$eEnrollment['Period']."</td>";
				$DateRowHTML .= "<td width=\"10\"></td>";
				$DateRowHTML .= "<td>".$i_From." ".$textFromDate." ".$i_To." ".$textToDate."</td>";
			$DateRowHTML .= "</tr>";
		}
		
		//loop 
		$studentInfoArr = unserialize(rawurldecode($studentInfoArr));
		
		$tempClub = $libenroll->Get_All_Club_Info();
       	$ClubInfoAssoc = BuildMultiKeyAssoc($tempClub,"EnrolGroupID");
			
		foreach((array)$GroupStudentAssoc as $ClassClubID => $thisStudentIDArr)
		{
	
			$thisStudentIDArr = array_values($thisStudentIDArr);
		
			//construct information above the table
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"90%\">\n";
			$x .= "<tr><td>";
			$x .= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\">\n";
		    	$x .= "<tr class=\"tabletext\"align=\"left\"><td>{$eEnrollment['Target']} : </td><td width=\"10\"></td><td>";
		    	
		    	if($showTarget=="class") {
		    		$YearClassObj = new year_class($ClassClubID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=false);
		    		$targetClass = $YearClassObj->Get_Class_Name();
	    		
			    	$x .= $targetClass;
	    		} else {
		    		//$sql = "SELECT Title FROM INTRANET_GROUP WHERE GroupID=$targetClub";
		    		//$result = $libenroll->returnVector($sql);
//		    		$ClubInfoArr = $libenroll->GET_GROUPINFO($ClassClubID);
		    		$x .= $ClubInfoAssoc[$ClassClubID]['TitleWithSemester'];
	    		}
		    	$x .= "</td></tr>\n";
		    	$x .= $DateRowHTML;
		    $x .= "</table>\n";
		    $x .= "</td></tr>";
			$x .= "</table>\n";
// 			$x .= $header ;
			
			$studentIDArr = $thisStudentIDArr;
	
			//loop through all students to get performance and construct table contents
			
				$NumberOfClubArr = array();
				$NumberOfActArr = array();
				$body = '';
				
			for ($i=0; $i<sizeOf($studentIDArr); $i++)
			{
				$studentID = $studentIDArr[$i];
		     	$className = $studentInfoArr[$studentID]['ClassName'];
		     	$classNumber = $studentInfoArr[$studentID]['ClassNumber'];
		     	$studentName = $studentInfoArr[$studentID]['StudentName'];
		     	$clubInfoArr = $studentInfoArr[$studentID]['ClubInfoArr'];
		     	$activityInfoArr = $studentInfoArr[$studentID]['ActivityInfoArr'];
		     	
		     	$thisClubTitleArr = array();
		     	$thisActivityTitleArr = array();
			
				//construct table title
				
				for ($j=0; $j<count($clubInfoArr); $j++)
				{
					$thisTitle = $clubInfoArr[$j][1];
					$thisClubTitleArr[] = $thisTitle;
				}	
			
				$NumOfClub = count($thisClubTitleArr);
				$NumberOfClubArr[]= $NumOfClub;			
				$maxClub = max($NumberOfClubArr);
		
				for ($j=0; $j<count($activityInfoArr); $j++)
				{
					$thisTitle = $activityInfoArr[$j][1];
					$thisActivityTitleArr[] = $thisTitle;
				}

				$NumOfAct = sizeof($thisActivityTitleArr);
				$NumberOfActArr[]= $NumOfAct;
				$maxAct = max($NumberOfActArr);
				$differentAct = $maxAct - $NumOfAct;
			}
			
			$maxClub = (count($NumberOfClubArr) > 0)? max($NumberOfClubArr) : 1;
			$maxClubNumber = ($maxClub>0)?  $maxClub : 1;
		//	debug_pr($maxClubNumber);
			$maxAct = (count($NumberOfActArr) > 0)? max($NumberOfActArr) : 1;
			$maxActNumber = ($maxAct > 0)? $maxAct :1 ;
		//	debug_pr($NumberOfActArr);
			for ($i=0; $i<sizeOf($studentIDArr); $i++) {
				$studentID = $studentIDArr[$i];
				$className = $studentInfoArr[$studentID]['ClassName'];
				$classNumber = $studentInfoArr[$studentID]['ClassNumber'];
				$studentName = $studentInfoArr[$studentID]['StudentName'];
				$clubInfoArr = $studentInfoArr[$studentID]['ClubInfoArr'];
				$activityInfoArr = $studentInfoArr[$studentID]['ActivityInfoArr'];
				
				$thisClubTitleArr = array();
				$thisActivityTitleArr = array();
					
				
				for ($j=0; $j<count($clubInfoArr); $j++)
				{
					$thisTitle = $clubInfoArr[$j][1];
					$thisClubTitleArr[] = $thisTitle;
				}
				
					
				$NumOfClub = count($thisClubTitleArr);
				$NumberOfClubArr[]= $NumOfClub;
				$differ = $maxClubNumber - $NumOfClub;
				
				for ($j=0; $j<count($activityInfoArr); $j++)
				{
					$thisTitle = $activityInfoArr[$j][1];
					$thisActivityTitleArr[] = $thisTitle;
				}
 				$NumOfAct = sizeof($thisActivityTitleArr);
 				$NumberOfActArr[]= $NumOfAct;
 				$differentAct = $maxActNumber - $NumOfAct;
 				
				//construct content of the table	
				$count = $i+1;
				$css = (($i%2)+1);
					
				$body .= "<tr class=\"eSporttdborder eSportprinttext\" valign=\"top\">
				<td width=\"10\" class=\"eSporttdborder eSportprinttext\">{$count}&nbsp</td>
				<td class=\"eSporttdborder eSportprinttext\">{$className}&nbsp</td>
				<td class=\"eSporttdborder eSportprinttext\">{$classNumber}&nbsp</td>
				<td class=\"eSporttdborder eSportprinttext\">{$studentName}&nbsp</td>";
			
				
						for ($j=0; $j<$NumOfClub; $j++){
							$body .= "<td class=\"eSporttdborder eSportprinttext\">{$thisClubTitleArr[$j]}</td>
							";
						}
						for($j=0;$j<$differ;$j++){
							$body .= "<td class=\"eSporttdborder eSportprinttext\">".''."</td>
							";
						}
						for ($j=0; $j<$NumOfAct; $j++){
							$body .= "<td class=\"eSporttdborder eSportprinttext\">{$thisActivityTitleArr[$j]}</td>
							";
		       			}
						for($j=0;$j<$differentAct;$j++){
							$body .= "<td class=\"eSporttdborder eSportprinttext\">".''."</td>
							";
						}
						$body .= "</tr>";
						
									
				}

			
			
			//header	
			$header = "<table width=\"90%\" border=\"0\" cellpadding=\"2\" cellspacing=\"0\" class=\"eSporttableborder\">\n";
				$header .= "<tr class=\"eSporttdborder eSportprinttabletitle\">
					<td width=10 class=\"eSporttdborder eSportprinttabletitle\">#</td>
					<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassName</td>
					<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserClassNumber</td>
					<td class=\"eSporttdborder eSportprinttabletitle\">$i_UserStudentName</td>
				";
				//header of club
				if($maxClub == 0){
					$header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['Club'].'1'."</td>
					";
				} else{
					for($q=1;$q<=$maxClub;$q++){
						$header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['Club'].''.$q."</td>
					";
					}
				}
				//Header of activity
				if($maxAct == 0){
					$header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['Activity'].'1'."</td></tr>";
				}
				else{
					for($p=1;$p<=$maxAct;$p++){
						$header .= "<td class=\"eSporttdborder eSportprinttabletitle\">".$Lang['eEnrolment']['Activity'].''.$p."</td>";
					}
					$header .= "</tr>";
					
				}
				$x .= $header.$body;

		 	if (sizeof($studentIDArr)==0)
		    {
		        $x .= "<tr class=\"eSporttdborder eSportprinttext\"><td colspan=\"6\" align=\"center\" class=\"eSporttdborder eSportprinttext\">$i_no_record_exists_msg</td></tr>\n";
		    }
		    
		    $x .= "</table>\n";
	    	$x .= '<div style="'.$pageBreakStyle.'">'.$libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent')).'<br /></div>'."\n\n";
		}


	################################################################
	
	?>
	<table width="100%" align="center" class="print_hide" border="0">
	<tr>
		<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
	</tr>
	</table>
	<?=$x?>
	
	<?
	//include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_footer.php");
	intranet_closedb();
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>