<?php
/*
 *  Using:
 *  
 * 	2018-10-23 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
    if (phpversion_compare('5.2') != 'ELDER') {
        $characterset = 'utf-8';
    }
    else {
        $characterset = 'big5';
    }
}
else {
    $characterset = 'utf-8';
}
header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();
$json['success'] = false;

$result = array();

$eventDateID = IntegerSafe($_POST['EventDateID']);      // Lesson ID


$libenroll->Start_Trans();
    
    $result['DeleteLesson'] = $libenroll->DEL_ENROL_EVENT_DATE('', array($eventDateID));
    
if (!in_array(false,$result)) {
    $libenroll->Commit_Trans();
    $json['deleteResult']= 'DeleteSuccess';
}
else {
    $libenroll->RollBack_Trans();
    $json['deleteResult']= 'DeleteUnsuccess';
}

echo $ljson->encode($json);


intranet_closedb();
?>