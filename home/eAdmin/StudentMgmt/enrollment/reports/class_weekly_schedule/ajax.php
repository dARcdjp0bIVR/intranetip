<?
/*
 * 	Using:
 *
 * 	Description: output json format data
 *
 *  2018-10-23 Cameron
 *      - change case getCourseCategory to getCourseCategoryAndLesson
 *      
 *  2018-10-16 Cameron
 *      - add case getCourseCategory
 *  
 *  2018-10-15 Cameron
 *      - add case editLesson
 *      
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-09-13 [Cameron]
 *      - add case getNumberOfStudentByCourse
 *      
 *  2018-09-12 [Cameron]
 *      - add case addLesson 
 *      
 *  2018-09-06 [Cameron]
 *      - retrieve weekly schedule for all classes if it's empty
 *      - change firstTitle for 'All Classes' in getClassList
 *       
 *  2018-08-29 [Cameron]
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
	if (phpversion_compare('5.2') != 'ELDER') {
		$characterset = 'utf-8';
	}
	else {
		$characterset = 'big5';
	}
}
else {
	$characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$nonJson = false;
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
//$remove_dummy_chars = ($junior_mck) ? false : true;	// whether to remove new line, carriage return, tab and back slash

$linterface = new libclubsenrol_ui();
$libenroll_report = new libclubsenrol_report();

switch($action) {
    
    case 'getClassList':
        $intakeID = IntegerSafe($_POST['IntakeID']);
        $classID = '';
        $firstTitle = $Lang['SysMgr']['FormClassMapping']['AllClass'];
        $classSelection = $linterface->getClassSelection($intakeID, $classID, $name='IntakeClassID', $firstTitle);
        $x = $classSelection;
        $json['success'] = true;
        break;
        
    case 'getClassWeeklyScheduleTable':
        $weekStartTimeStamp = $_POST['WeekStartTimeStamp'];
        $startDate = date('Y-m-d', $weekStartTimeStamp);
        $intakeID = IntegerSafe($_POST['IntakeID']);
        $classID = IntegerSafe($_POST['IntakeClassID']);
        if ($intakeID) {
            if ($classID == '') {   // all classes
                $classList = $libenroll->getIntakeClass($intakeID);
                
                $i = 0;
                $scheduleTable = '';
                foreach ((array)$classList as $_class) {
                    $_classID = $_class['ClassID'];
                    $_className = $_class['ClassName'];
                    
                    if ($i) {
                        $scheduleTable .= "<br /><br /><br />";
                    }
                    $scheduleTable .= $libenroll_report->getClassWeeklyScheduleTable($intakeID, $_classID, $startDate, $fromPrint=0, $_className);
                    
                    $i++;
                }
                
            }
            else {
                $scheduleTable = $libenroll_report->getClassWeeklyScheduleTable($intakeID, $classID, $startDate, $fromPrint=0);
            }
            $x = $scheduleTable;
            $json['success'] = true;
        }
        break;
        
    case 'getDisplayWeekByCal':
        $weekStartDate = $_POST['WeekStartDate'];
        $displayWeekInfoAry = $libenroll_report->getDisplayWeekByCal($weekStartDate);
        $x = $displayWeekInfoAry['TimeStamp'];
        $y = $displayWeekInfoAry['Display'];
        
        $json['success'] = true;
        break;
        
    case 'getDisplayWeekByNav':
        $weekStartTimeStamp = $_POST['WeekStartTimeStamp'];
        $offset = IntegerSafe($_POST['offset']);
        $weekStartDate = date('Y-m-d',strtotime($offset.' day', $weekStartTimeStamp));
        $displayWeekInfoAry = $libenroll_report->getDisplayWeekByCal($weekStartDate);
        $x = $displayWeekInfoAry['TimeStamp'];
        $y = $displayWeekInfoAry['Display'];
        
        $json['success'] = true;
        
        break;
        
    case 'saveWeelyScheduleSetting':
        $startTime = $_POST['StartTime'];
        $endTime = $_POST['EndTime'];
        $timeInterval = IntegerSafe($_POST['TimeInterval']);
        $x = $libenroll->saveClassWeeklyScheduleSetting($startTime, $endTime, $timeInterval);
        if ($x) {
            $json['success'] = true;
        }
        break;
        
    case 'addLesson':
        $lessonDate = $_GET['LessonDate'];
        $startTime = $_GET['StartTime'];
        $targetEndTime = $_GET['TargetEndTime'];
        $maxEndTime = $_GET['MaxEndTime'];
        $intakeClassID = IntegerSafe($_GET['IntakeClassID']);
        
        $x = $linterface->getAddLessonLayout($intakeClassID, $lessonDate, $startTime, $targetEndTime, $maxEndTime);
        $nonJson = true;
        break;
        
//     case 'getNumberOfStudentByCourse':
//         $courseID = $_POST['CourseID'];
//         $ret = $libenroll->getNumberOfStudentByCourse($courseID);
        
//         if (count($ret)) {
//             $x = $ret['NumberOfStudent'] . '~^' . $ret['CourseCategoryID'];
//             $json['success'] = true;
//         }
//         break;
        
    case 'editLesson':
        $eventDateID = IntegerSafe($_GET['EventDateID']);
        $intakeClassID = IntegerSafe($_GET['IntakeClassID']);
        
        $x = $linterface->getEditLessonLayout($eventDateID, $intakeClassID);
        $nonJson = true;
        break;

    case 'getCourseCategoryAndLesson':
        $courseID = $_POST['CourseID'];
        $courseInfoAry = $libenroll->GET_EVENTINFO($courseID);
        
        $y = $linterface->getLessonSelectionByCourse($courseID);
        if (!empty($y)) {
            $json['success'] = true;
        }
        
        if (count($courseInfoAry)) {
            $x = $courseInfoAry['EventCategory'];
            $json['success'] = true;
        }
        
        
        break;
        
}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
	$x = convert2unicode($x,true,1);
	$y = convert2unicode($y,true,1);
}

$json['html'] = $x;
$json['html2'] = $y;

echo $nonJson ? $x : $ljson->encode($json);

intranet_closedb();
?>