<?php
# using: 
############## Change Log [start] ####
#
#
########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();


$libenroll = new libclubsenrol();
$lexport = new libexporttext();
$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin');


$studentId = $_POST['studentID'];
$exportStudentViewClubArr = unserialize(rawurldecode($exportStudentViewClubArr));
$exportStudentViewActivityArr = unserialize(rawurldecode($exportStudentViewActivityArr));


$headerAry = array();
$dataAry = array();


$headerAry[] = $i_UserClassName;
$headerAry[] = $i_UserClassNumber;
$headerAry[] = $i_UserStudentName;
$headerAry[] = $Lang['eEnrolment']['record_type'];
$headerAry[] = $Lang['eEnrolment']['RecordName'];
$headerAry[] = $eEnrollment['role'];
$headerAry[] = $eEnrollment['attendence'];
$headerAry[] = $eEnrollment['performance'];


$libuser = new libuser($studentId);
$studentInfoAry = $libuser->getStudentInfoByAcademicYear(Get_Current_Academic_Year_ID());
$studentClassName = Get_Lang_Selection($studentInfoAry[0]['ClassNameCh'], $studentInfoAry[0]['ClassNameEn']);
$studentClassNumber = $studentInfoAry[0]['ClassNumber'];


$iCount = 0;
if($SizeOfClub=count($exportStudentViewClubArr)) {
	for ($i=0; $i<$SizeOfClub; $i++) {
		list($thisTitle, $thisRole, $thisAttendance, $thisPerformance) = $exportStudentViewClubArr[$i];
		
		$dataAry[$iCount][] = $studentClassName;
		$dataAry[$iCount][] = $studentClassNumber;
		$dataAry[$iCount][] = Get_Lang_Selection($libuser->ChineseName, $libuser->EnglishName);
		$dataAry[$iCount][] = $Lang['eEnrolment']['Club'];
		$dataAry[$iCount][] = $thisTitle;
		$dataAry[$iCount][] = $thisRole;
		$dataAry[$iCount][] = $thisAttendance;
		$dataAry[$iCount][] = $thisPerformance;
		
		$iCount++;
	}
}
						
						
if($SizeOfAct = count($exportStudentViewActivityArr)) {
	for ($i=0; $i<$SizeOfAct; $i++) {
		list($thisTitle, $thisRole, $thisAttendance, $thisPerformance) = $exportStudentViewActivityArr[$i];
		
		$dataAry[$iCount][] = $studentClassName;
		$dataAry[$iCount][] = $studentClassNumber;
		$dataAry[$iCount][] = Get_Lang_Selection($libuser->ChineseName, $libuser->EnglishName);
		$dataAry[$iCount][] = $Lang['eEnrolment']['Activity'];
		$dataAry[$iCount][] = $thisTitle;
		$dataAry[$iCount][] = $thisRole;
		$dataAry[$iCount][] = $thisAttendance;
		$dataAry[$iCount][] = $thisPerformance;
		
		$iCount++;
	}
}


$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "enrolment_summary_student.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>