<?php
# using: 

############## Change Log [start] ####
#
#	Date:	2016-03-10	Kenneth
#			cancel Changed back btn action (2015-06-22), submit form with GET param
#
#	Date:	2015-06-22	Omas
#			Changed back btn action 
#
########################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);	

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
	$libenroll = new libclubsenrol();	
	$libenroll_ui = new libclubsenrol_ui();
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_NORMAL_USER($_SESSION['UserID'])))
		header("Location: ../../enrollment/");
	
	$CurrentPage = "PageEnrolmentSummaryReport";
	$CurrentPageArr['eEnrolment'] = 1;
   	$TAGS_OBJ[] = array($eEnrollment['enrolment_summary_report'], "", 1);
   	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
   	
   	# navigation
    $PAGE_NAVIGATION[] = array($eEnrollmentMenu['attendance_mgt_stu_attend'], "");
    
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();

        $linterface->LAYOUT_START();
        
        $GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
        $GroupEnrollArr = $libenroll->GET_GROUPINFO($EnrolGroupID);
        $GroupArr = $libenroll->getGroupInfo($GroupID);
        $title_display = $intranet_session_language=="en" ? $GroupArr[0]['Title'] : $GroupArr[0]['TitleChinese'];
        $GroupDateArr = $libenroll->GET_ENROL_GROUP_DATE($GroupEnrollArr['EnrolGroupID']);
        
        $DataArr['StudentID'] = $studentID;
        $DataArr['EnrolGroupID'] = $EnrolGroupID;
        $CommentStudent = $libenroll->GET_GROUP_STUDENT_COMMENT($DataArr);
        $Performance = $libenroll->GET_GROUP_STUDENT_PERFORMANCE($DataArr);
        //debug_r($GroupEnrollArr);
        $total = 0;
        $attend = 0;
        
		$lword = new libwordtemplates();
		$performancelist = $lword->getSelectPerformance("onChange=\"this.form.performance.value=this.value\"");
        
        if ($msg == 2) $SysMsg = $linterface->GET_SYS_MSG("update");
        
        $words = $lword->getWordList(8);                
        $select_word .= $linterface->CONVERT_TO_JS_ARRAY($words, "jArrayWords", 1);
		$select_word .= $linterface->GET_PRESET_LIST("jArrayWords", 0, "performance");
		
		$libuser_student = new libuser($studentID);
		$thisWebSAMSRegNo = $libuser_student->WebSamsRegNo;
		# get offical photos
		list($thisPhotoFilePath, $thisPhotoURL) = $libuser_student->GET_OFFICIAL_PHOTO($thisWebSAMSRegNo);
		$fs = new libfilesystem();
		if (file_exists($thisPhotoFilePath))
		{
			list($originalWidth, $originalHeight) = getimagesize($thisPhotoFilePath);
			$thisImgTag = $libenroll->Get_Offical_Photo_Img($thisPhotoURL, $originalWidth, $originalHeight);
			$thisPhotoSpan = "<br /><span class=\"tabletext\">";
				$thisPhotoSpan .= $thisImgTag;
			$thisPhotoSpan .= "</span>\n";
		}
		else
		{
			$thisPhotoSpan = "";
		}
		
		$AttendanceIconRemarks = '';
		$AttendanceIconRemarks .= '<div>'."\n";
			$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_PRESENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Present'].'</span>'."\n";
			$AttendanceIconRemarks .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
			$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_ABSENT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Absent'].'</span>'."\n";
			$AttendanceIconRemarks .= '&nbsp;&nbsp;&nbsp;&nbsp;'."\n";
			$AttendanceIconRemarks .= $libenroll_ui->Get_Attendance_Icon(ENROL_ATTENDANCE_EXEMPT).' <span style="vertical-align:middle">'.$Lang['eEnrolment']['Attendance']['Exempt'].'</span>'."\n";
		$AttendanceIconRemarks .= '</div>'."\n";
        

?>
<script language="javascript">
function jsBack(){
	var isIE = /*@cc_on!@*/false || !!document.documentMode;
	if(isIE){
		document.form1.action = "index.php";
		document.form1.target = "_self";
		document.form1.submit();
	}else{
		window.history.back();
	}
}
</script>
<form name="form1" method="GET">
<br/>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td align="right"><?= $SysMsg ?></td>
</tr>
</table>

<table align="center">
<tr><td>
<table><tr><td class="tabletext"><?= GET_ACADEMIC_YEAR(date("Y-m-d"))?> <?= $eEnrollment['year']?></td></tr></table>
</td><td>

<table><tr><td class="tabletext">
<?= $eEnrollment['activity']?> : 
</td><td class="tabletext">
<?= $title_display?>
</td></tr></table>
</td></tr></table>

<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td class="tabletext" align="center"><?= $LibUser->getNameWithClassNumber($studentID) . $thisPhotoSpan ?></td>
</tr> 
<tr height="10"><td></td></tr>
<tr><td>
<table id="html_body_frame" width="60%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr class="tablegreentop">
		<td class="tableTitle" nowrap align="center"><span class="tabletopnolink"><?= $eEnrollment['add_activity']['act_date']?></span></td>
		<td><span class="tabletopnolink"> </span></td>
	</tr>	
	<? for ($i = 0; $i < sizeof($GroupDateArr); $i++) { ?>
	<tr class="tablegreenrow<?= (($i % 2) + 1)?>">
		<td class="tabletext" align="center"><?= date("Y-m-d H:i", strtotime($GroupDateArr[$i][2]))?>-<?= date("H:i", strtotime($GroupDateArr[$i][3]))?></td>
		<td>		
		<?
			$DataArr['GroupDateID'] = $GroupDateArr[$i][0];
			$DataArr['StudentID'] = $studentID;
			
			if (date("Y-m-d", strtotime($GroupDateArr[$i][2])) > date("Y-m-d"))
			{
				$temp = "future";
			}
			else
			{
				$total++;
//				if ($status = $libenroll->Get_Group_Attendance_Status($GroupDateArr[$i][0],$studentID,$EnrolGroupID)) {
//					$temp = $status==1?"present":"exempt";
//					$attend++;
//				} else {
//					$temp = "absent";
//				}
				$status = $libenroll->Get_Group_Attendance_Status($GroupDateArr[$i][0],$studentID,$EnrolGroupID);
				if (in_array($status, array(ENROL_ATTENDANCE_PRESENT, ENROL_ATTENDANCE_EXEMPT)))
					$attend++;
				$thisIcon = $libenroll_ui->Get_Attendance_Icon($status);
			}
		?>
		
		<? if ($temp == "future") { ?>
			<?=$Lang['General']['EmptySymbol']?>
		<? } else { ?>
			<?=$thisIcon?>
		<? } ?>
		
		</td>
	</tr>
	<? } ?>
	<tr class="tablegreenbottom">
		<td class="tabletext" nowrap align="center"><?= $eEnrollment['attendence']?></td>
		<? ($total != 0) ? $attendPercentage = intval($attend / $total * 100) : $attendPercentage = "0"; ?>
		<td class="tabletext"><?= $attendPercentage ?>%</td>
	</tr>
</table>
<table id="html_body_frame" width="60%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr><td align="right"><?= $AttendanceIconRemarks ?></td></tr>
</table>


</td></tr>
<tr><td>
<br/>
<table width="60%" border="0" cellspacing="0" cellpadding="4" align="center">
<tr>
	<td class="tabletext formfieldtitle" width="30%" valign="top" nowrap="nowrap">
		<?= $i_ActivityPerformance; ?>
	</td>
	<td class="tabletext">
		<?= $Performance ?>
	</td>
</tr>
<tr><td valign="top" nowrap="nowrap" class="formfieldtitle tabletext" width="30%">
<?= $eEnrollment['teachers_comment']?>
</td><td>
<?= $CommentStudent ?>
</td></tr>
</table>
</td></tr>

<tr><td>&nbsp;</td></tr>

<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($button_back, "button", "jsBack()")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>
</table>
<input type="hidden" id="showTarget" name="showTarget" value="<?= $showTarget?>" />
<input type="hidden" id="targetClass" name="targetClass" value="<?= $targetClass?>" />
<input type="hidden" id="targetClub" name="targetClub" value="<?= $targetClub?>" />
<input type="hidden" id="usePeriod" name="usePeriod" value="<?= $usePeriod?>" />
<input type="hidden" id="useCategory" name="useCategory" value="<?= $useCategory?>" />
<input type="hidden" id="textFromDate" name="textFromDate" value="<?= $textFromDate?>" />
<input type="hidden" id="textToDate" name="textToDate" value="<?= $textToDate?>" />

</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>