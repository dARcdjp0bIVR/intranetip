<?php


$roundFilter = IntegerSafe($roundFilter);

$sql = "SELECT 
	i.ClassName AS CLASS_NAME,
	i.ClassNumber AS CLASS_NUMBER,
	i.EnglishName AS ENGLISH_NAME,
	i.ChineseName AS CHINESE_NAME,
	UPPER(i.UserLogin) AS ADMISSION_NO,
	g.title AS TITLE, 
	gs.choice AS CHOICE, 
	s.Max AS MAX_CHOICES
FROM 
	INTRANET_ENROL_GROUPSTUDENT AS gs 
INNER JOIN 
	INTRANET_GROUP AS g 
ON 
	gs.groupid = g.groupid
INNER JOIN 
	INTRANET_USER AS i 
ON 
	gs.studentid = i.userid
LEFT JOIN 
	INTRANET_ENROL_STUDENT AS s 
ON 
	gs.studentid = s.studentid
INNER JOIN 
	INTRANET_ENROL_GROUPINFO IEG 
ON 
	gs.EnrolGroupID = IEG.EnrolGroupID 
AND
	IEG.RoundNumber = {$roundFilter}
WHERE 
	g.recordtype = 5
ORDER BY i.ClassName,CAST(i.ClassNumber AS UNSIGNED),gs.choice";

$result = $libenroll->returnArray($sql);
$enrollmentAry = array();
//$columnFlag = array();
$maxColumn = 0;
$studentInfo = array();

for($x = 0,$x_max = count($result); $x < $x_max; $x++){
	$_admissionNo = $result[$x]['ADMISSION_NO'];
	$_title = $result[$x]['TITLE'];
	$_choice =$result[$x]['CHOICE'];
	$_maxChoices=$result[$x]['MAX_CHOICES'];
	$_className =$result[$x]['CLASS_NAME'];
	$_classNumber =$result[$x]['CLASS_NUMBER'];
	$_englishName =$result[$x]['ENGLISH_NAME'];
	$_chineseName =$result[$x]['CHINESE_NAME'];

//	$columnFlag[$_admissionNo] = $columnFlag[$_admissionNo] + 1;
//	$maxColumn = ($columnFlag[$_admissionNo] > $maxColumn) ?$columnFlag[$_admissionNo] : $maxColumn;
	$maxColumn = ($_choice > $maxColumn) ?$_choice : $maxColumn;

//					$enrollmentAry[$_admissionNo][] = array('TITLE'=>$_title,'CHOICE'=>$_choice);
	$enrollmentAry[$_admissionNo][$_choice] = array('TITLE'=>$_title,'CHOICE'=>$_choice);
	$studentInfo[$_admissionNo] =	array('CLASS_NAME'=>$_className,
										  'CLASS_NUMBER'=>$_classNumber,
										  'ENGLISH_NAME'=>$_englishName,
										  'CHINESE_NAME'=>$_chineseName,
										  'ADMISSION_NO'=>$_admissionNo,
										  'MAX_CHOICES'=>$_maxChoices
									);

}


$delimiter = ($g_encoding_unicode) ? "\t" : ",";
$valueQuote = ($g_encoding_unicode) ? "" : "\"";

$h = '<table border = "1">';
$h .= '<tr>';
$h .= '<td>Class Name</td><td>Class Number</td><td>Student Name</td><td>Student Number</td><td>Max. of choice</td>';

$csvContentHeader[] ='Class Name'; 
$csvContentHeader[] ='Class Number';
$csvContentHeader[] ='Student Name';
$csvContentHeader[] ='Student Number';
$csvContentHeader[] ='Max. of choice';

for($ll = 1;$ll <= $maxColumn;$ll++){
	$h .= '<td>Priority '.$ll.'</td>';	
	$csvContentHeader[] = 'Priority  '.$ll;
}
$h .= '</tr>';

$csvContent = '';
for($c = 0,$c_max = count($csvContentHeader);$c < $c_max;$c++){
	$csvContent .= $valueQuote.$csvContentHeader[$c].$valueQuote;
	if($c == intval($c_max -1)){
		$csvContent .= "\n";
	}else{
		$csvContent .= $delimiter;
	}
}


foreach ($studentInfo as $_admissionNo => $_info){

	$_className   = $_info['CLASS_NAME'];
	$_classNumber = $_info['CLASS_NUMBER'];
	$_englishName = $_info['ENGLISH_NAME'];
	$_admissionNo = $_info['ADMISSION_NO'];
	$_maxChoices  = $_info['MAX_CHOICES'];
	
//					$_chineseName = $_info['CHINESE_NAME'];
	$h .= '<tr>';
	$h .= '<td>'.$_className.'</td>';
	$h .= '<td>'.$_classNumber.'</td>';
	$h .= '<td>'.$_englishName.'</td>';
	$h .= '<td>'.$_admissionNo.'</td>';
	$h .= '<td>'.$_maxChoices.'</td>';

	$_studentEnrolInfo = $enrollmentAry[$_admissionNo];

	$csvContent .= $valueQuote.$_className.$valueQuote.$delimiter;
	$csvContent .= $valueQuote.$_classNumber.$valueQuote.$delimiter;
	$csvContent .= $valueQuote.$_englishName.$valueQuote.$delimiter;
	$csvContent .= $valueQuote.$_admissionNo.$valueQuote.$delimiter;
	$csvContent .= $valueQuote.$_maxChoices.$valueQuote.$delimiter;


	for($e = 0;$e < $maxColumn;$e++){
		if($_studentEnrolInfo[intval($e +1)] == ''){
			$_data = '';
		}else{
			$_data = $_studentEnrolInfo[intval($e +1)]['TITLE'];
		}
		$h .= '<td>'.$_data.'&nbsp;</td>';

		$csvContent .= $valueQuote.$_data.$valueQuote;

		if($e == intval($maxColumn-1)){
			$csvContent .= "\n";
		}else{
			$csvContent .= $delimiter;
		}
	}
	$h .= '</tr>';
}				
$h .= '</table>';

$fileName = date("YmdGis").'_enrolResult.csv';
$lexport = new libexporttext();
$lexport->EXPORT_FILE($fileName, $csvContent);


?>