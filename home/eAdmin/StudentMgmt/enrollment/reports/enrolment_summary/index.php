<?php
// using: 

############## Change Log [start] ####
#   Date:   2020-03-31 Tommy
#           modified permission checking
#
#   Date:   2020-03-09 Tommy
#           add new export for cust semester performance report (Case #Q178421)
#
#   Date:   2018-03-05 Anna
#           When target is club, and no student in club, show no record message
#
#	Date: 	2017-01-11 Omas
#			Modified TAGS_OBJ lang
#
#	Date:	2017-01-09 Anna
#			select all when taegt club is empty
#
#	Date:	2017-10-10 Anna #H128321 
#			show GroupName chinese according to intranet_session_language
#
#	Date:	2017-10-09 Anna
#			added display PIC list after title  
#
#	Date:	2017-07-17 Omas
#			fix default check all option js causing after self-submit export wrongly
#
#	Date:	2017-01-19 Omas
#			$sys_custom['eEnrolment']['heungto_enrolment_report'] will not support B111852
#
#	Date:	2017-01-18 Villa
#			B111852 - add checkform/ change UI supporting checkbox from selection box
#
#	Date	2016-09-14	Anna (marked by Omas)
#			add new format for export-#B98234 
#
#	Date:	2015-10-30	Kenneth
#			Fix - skip getting category / activity info if targetCategory = '';
#
#	Date:	2015-10-30	Kenneth
#			Fix - validation check of detailed print 
#
#	Date:	2015-08-17	Omas
#			Fix - after export form target is changed causing  cannot change condition view other data 
#
#	Date:	2015-06-10	Evan
#			Update the style of tables
#
#	Date:	2015-06-10	Evan
#			Update the style of tables in student view
#
#	Date:	2015-06-09	Evan
#			Change the method of the form into POST
#			Update the style of tables
#
#	Date:	2015-06-02	Evan
#			Replace the old style with UI standard
#
#	Date:	2015-05-26	Omas #J31222 
#			remove flag $sys_custom['eEnrolment']['heungto_enrolment_report'] to general deploy the feature
#
#	Date:	2014-11-10	Omas
#			Add new filtering Category, for club/activities table and student view
#
#	Date:	2014-10-14	Pun
# 				Add round report for SIS customization
#
#	Date:	2014-07-23	Bill
#			Allow user to print or export details with club that activity belongs to
#
#	Date:	2012-02-15	YatWoon
#			Improved: can print details for selected student only [Case#2011-0223-1816-24071]
#			$special_feature['eEnrolment']['EnrolmentSummary_DetailedPrint_select_student'] 
#
#	Date:	2011-10-11	YatWoon
#			Customization: $sys_custom['eEnrolment']['heungto_enrolment_report']
#
#	Date:	2011-09-08	YatWoon
# 			lite version should not access this page 
#
#	Date:	2011-02-02	Marcus
#			cater multi selection
#
#	Date:	2010-05-24	YatWoon
#			add "bulk print individual student enrolment records"
#
############## Change Log [End] ####

$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
intranet_auth();
intranet_opendb();
$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();

include_once($PATH_WRT_ROOT."includes/libuser.php");

include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libteaching.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
if($plugin['SIS_eEnrollment']){
	include_once($PATH_WRT_ROOT."lang/cust/SIS_eEnrollment_lang.$intranet_session_language.php");
}
if ($plugin['eEnrollment'] && !$plugin['eEnrollmentLite'])
{
	
//	if(!isset($_POST['targetClassID'])){
//		$targetClassID = unserialize(rawurldecode($_POST['selectedTargetClassIDArr']));
//	}else{
//		$targetClassID = $_GET['selectedTargetClassIDArr'];
//	}
	
	$lclass = new libclass();
	$lteaching = new libteaching();
	
	if (!isset($AcademicYearID) || $AcademicYearID=="")
		$AcademicYearID = Get_Current_Academic_Year_ID();
		
	//if($sys_custom['eEnrolment']['heungto_enrolment_report'])
// 	$SemesterFilter = $libenroll->Get_Term_Selection('Semester', $AcademicYearID, $Semester, "", $NoFirst=0, $NoPastTerm=0, $withWholeYear=1, $DisplayAll=1);
	
	$semester_data = getSemesters($AcademicYearID);

	if($sys_custom['eEnrolment']['heungto_enrolment_report']){
		// this school cust cannot support multi year  by omas
		if(!isset($Semester)){
			$Semester = '-999';
		}
	
		$selectArr['-999'] = $Lang['General']['All'];
		$selectArr['0'] = $eEnrollment['YearBased'];
		foreach((array)$semester_data as $_value => $_semesterName){
			$selectArr[$_value] = $eEnrollment['SemesterBased'].' - ('.$_semesterName.')';
		}
		$SemesterFilter = getSelectByAssoArray($selectArr, ' id="Semester" name="Semester" ', $Semester, 0, 1);
	
		if($Semester == '-999'){
			$Semester = '';
		}
	}else{
		// default check all
		if(!empty($_POST['Semester'])){
			$Semester = $_POST['Semester'];
		}else{
			$Semester = array();
			$Semester [] = 0;
			foreach((array)$semester_data as $_value => $_semesterName){
				$Semester[] = $_value;
			}
		}
		$SemesterFilter = $libenroll->Get_Term_Selection_CheckBox('Semester', $AcademicYearID, $Semester);
	}
	
	//check if the user is a class teacher if the user is not a enrol admin/master
	if ($libenroll->IS_NORMAL_USER($_SESSION['UserID']))
	{
		/*
		$sql = "SELECT ClassID FROM INTRANET_CLASSTEACHER WHERE UserID = '$UserID'";
		$result = $libenroll->returnArray($sql,1);
	
		if ($result==NULL)
		{
			$isClassTeacher = false;
		}
		else
		{
			$isClassTeacher = true;
			$targetClassID = $result[0][0];
		}
		*/
		
		$TeachingClassArr = $lteaching->returnTeacherClass($_SESSION['UserID'], Get_Current_Academic_Year_ID());
		
		//$targetClassID = $TeachingClassArr[0]['ClassID'];		// commented by Henry Chow on 20110809
		$isClassTeacher = ($targetClassID=='')? false : true;
	}

	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$isClassTeacher) &&(!$libenroll->IS_CLUB_PIC()))
	{
		include_once($PATH_WRT_ROOT."includes/libaccessright.php");
		$laccessright = new libaccessright();
		$laccessright->NO_ACCESS_RIGHT_REDIRECT();
		exit;
	}
	
	$LibUser = new libuser($UserID);
	
	$role = array("Admin", "ClassTeacher");
	
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$isClassTeacher))
	//	header("Location: ../../enrollment/");
	if ($libenroll->hasAccessRight($_SESSION['UserID'], $role))
    {
    	
        $linterface = new interface_html();
        
        $CurrentPage = "PageEnrolmentSummaryReport";
		$CurrentPageArr['eEnrolment'] = 1;
       	$TAGS_OBJ[] = array($eEnrollmentMenu['enrolment_summary'], "", 1);
       	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
       	if (isset($studentID) && $studentID!="")
       	{
	     	//student view
	     	$studentView = true;
	     	
	     	$exportStudentViewClubArr = array();
	     	$exportStudentViewActivityArr = array();
	     	
	     	$ObjClass = new year_class($targetClassID[0]);
	     	$classSelection = $ObjClass->Get_Class_Name();
	       	$targetClass = $ObjClass->Get_Class_Name(); 

	     	//get name of the student
	     	$name_field = getNameFieldByLang();
	     	$sql = "SELECT ".$name_field." FROM INTRANET_USER WHERE UserID = '$studentID'";
	     	$result = $libenroll->returnArray($sql,1);
	     	$studentNameDisplay = $result[0][0];
	     	
// 	     	$temp = $libenroll->getClubInfoByGroupID($targetClub);
// 	     	$clubSelection = $temp['Title'];
			$clubSelection = $libenroll->getClubInfoByEnrolGroupID($targetClub);
	     	
	     	################################## Club Table ##################################
	     	//get all groups of student
	     	#New filtering by Category
	     	if (empty($targetCategory)){
	     		$conds_category ='';
	     	}
	     	else{
	     		$conds_category = " AND d.GroupCategory IN ( '".implode("','", (array)$targetCategory)."' ) ";
	     		$specialJoin = ' INNER JOIN INTRANET_ENROL_GROUPINFO as d ON  (a.GroupID = d.GroupID) ';
	     	}
	     	
	     	$sql = "SELECT b.Title, a.Performance, a.EnrolGroupID, c.Title as Role, b.TitleChinese
	     			FROM 	INTRANET_USERGROUP as a LEFT JOIN INTRANET_GROUP as b ON a.GroupID = b.GroupID
	     					LEFT JOIN INTRANET_ROLE as c ON c.RoleID = a.RoleID
							LEFT JOIN INTRANET_USER as iu On (a.UserID = iu.UserID)
							$specialJoin
	     			WHERE a.UserID = '$studentID' AND b.RecordType = 5 And b.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
							and iu.RecordType = 2  
							$conds_category
	     			ORDER BY b.Title";
			
	     	$clubPerformArr = $libenroll->returnArray($sql);
	     	//construct club table title
	     	
	     	$clubDisplay = "<table class=\"common_table_list_v30 view_table_list_v30\">";
	     	$clubDisplay .= $linterface->GET_NAVIGATION2($eEnrollment['club']);
			$clubDisplay .= "<tr>";			
			$total_col = 1;
			$clubDisplay .= "<th>".$eEnrollment['club_name']."</th>";
			if($display_role)
			{
				$total_col++;
				$clubDisplay .= "<th><center>".$eEnrollment['role']."</th>";
			}
			if($display_attendance)
			{
				$total_col++;
				$clubDisplay .= "<th><center>".$eEnrollment['attendence']."</th>";
			}
			if($display_performance)
			{
				$total_col++;
				$clubDisplay .= "<th><center>".$eEnrollment['performance']."</th>";
			}
		    $clubDisplay .= "</tr>";
		    
// 		    $total_col = 3;
		   	$numOfClub = sizeof($clubPerformArr);
		   	
		   	//construct table content
			$clubDisplay .= ($numOfClub==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
			
	     	//loop through each club record to construct table content
	     	for ($i=0; $i<$numOfClub; $i++)
	     	{
	     		
	     		# Role
	     		$thisRole = $clubPerformArr[$i]['Role'];
	     		
	     		# Attendance
	     		//$ParDataArr['EnrolGroupID'] = $libenroll->GET_ENROLGROUPID($clubPerformArr[$i]['GroupID']);
	     		$ParDataArr['EnrolGroupID'] = $clubPerformArr[$i]['EnrolGroupID'];
	     		$ParDataArr['StudentID'] = $studentID;
	     		$thisAttendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($ParDataArr)."%";
	     		
	     		# Performance
	     		$thisPerformance = $clubPerformArr[$i]['Performance'];
	     		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
			    
				$clubDisplay .= '<tr class="'.$tr_css.'">';
				
				$titleDisplay = $intranet_session_language=="en" ? $clubPerformArr[$i]['Title'] : $clubPerformArr[$i]['TitleChinese'];
				$clubDisplay .= '<td valign="top">'.$titleDisplay.'</td>';
				$exportStudentViewClubArr[$i][] = $titleDisplay;
				if($display_role)
				{
			    	$clubDisplay .= '<td class="tabletext" align="center" valign="top">'.$thisRole.'</td>';
			    	$exportStudentViewClubArr[$i][] = $thisRole;
		    	}
				if($display_attendance)
				{
			    	$clubDisplay .= '<td class="tabletext" align="center" valign="top">'.$thisAttendance.'</td>';
			    	$exportStudentViewClubArr[$i][] = $thisAttendance;
		    	}
		    	if($display_performance)
				{
			    	$clubDisplay .= '<td class="tabletext" align="center" valign="top">'.$thisPerformance.'</td>';
			    	$exportStudentViewClubArr[$i][] = $thisPerformance;
		    	}
			    $clubDisplay .= '</tr>';
			    
	     	}
	     	
	     	$clubDisplay .= '</table>';
     		################################## Enf of Club Table ##################################
     		
     		################################## Activity Table #####################################
     		//get all activities of student
	     	if (empty($targetCategory)){
	     		$conds_categoryActivity ='';
	     	}
	     	else{
	     		$conds_categoryActivity = " AND b.EventCategory IN ( '".implode("','", (array)$targetCategory)."' ) ";
	     	}
	     	$sql = "SELECT b.EventTitle, a.Performance, a.EnrolEventID, a.RoleTitle as Role
	     			FROM 
							INTRANET_ENROL_EVENTSTUDENT as a 
							LEFT JOIN INTRANET_ENROL_EVENTINFO as b ON a.EnrolEventID = b.EnrolEventID
							LEFT JOIN INTRANET_USER as iu On (a.StudentID = iu.UserID)
	     			WHERE 
							a.StudentID = '$studentID'  
							AND a.RecordStatus = 2 
							AND a.EnrolEventID!='' 
							and iu.RecordType = 2  
							AND b.AcademicYearID='".Get_Current_Academic_Year_ID()."'
							$conds_categoryActivity
	     			ORDER BY b.EventTitle
	     			";
	     	$actPerformArr = $libenroll->returnArray($sql,2);
	     	
	     	//construct club table title
	     	
	     	$actDisplay .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
			$actDisplay .= $linterface->GET_NAVIGATION2($eEnrollment['activity']);
			$actDisplay .= "<thead><tr>";
			$total_col = 1;
			$actDisplay .= "<th>".$eEnrollment['act_name']."</th>";
			if($display_role)
			{
				$total_col++;
				$actDisplay .= "<th><center>".$eEnrollment['role']."</th>";
			}			
			if($display_attendance) 
			{
				$total_col++;
				$actDisplay .= "<th><center>".$eEnrollment['attendence']."</th>";
			}
			if($display_performance)
			{
				$total_col++;
				$actDisplay .= "<th><center>".$eEnrollment['performance']."</th>";
			}
			         
		    $actDisplay .= "</tr></thead>";
		    
// 		    $total_col = 3;
		   	$numOfAct = sizeof($actPerformArr);
		   	
		   	//construct table content
			$actDisplay .= ($numOfAct==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
	     	
	     	//loop through each club record to construct table content
	     	$totalPerform = 0;
	     	for ($i=0; $i<$numOfAct; $i++)
	     	{
	     		
	     		# Role
	     		$thisRole = $actPerformArr[$i]['Role'];
	     		
	     		# Attendance
	     		$ParDataArr['EnrolEventID'] = $actPerformArr[$i]['EnrolEventID'];
	     		$ParDataArr['StudentID'] = $studentID;
	     		$thisAttendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($ParDataArr)."%";
	     		
	     		# Performance
	     		$thisPerformance = $actPerformArr[$i]['Performance'];
	     		$thisPerformance = ($thisPerformance=="")? "---" : $thisPerformance;
			 				
				$actDisplay .= '<tr class="'.$tr_css.'">';
				$actDisplay .= '<td valign="top">'.$actPerformArr[$i]['EventTitle'].'</td>';
				
				if($display_role)
				{
					$actDisplay .= '<td class="tabletext" align="center" valign="top">'.$thisRole.'</td>';
				}
				if($display_attendance)
				{
					$actDisplay .= '<td class="tabletext" align="center" valign="top">'.$thisAttendance.'</td>';
				}
				if($display_performance)
				{
			    	$actDisplay .= '<td class="tabletext" align="center" valign="top">'.$thisPerformance.'</td>';
		    	}
			    $actDisplay .= '</tr>';
			    
			    $exportStudentViewActivityArr[$i][] = $actPerformArr[$i]['EventTitle'];
			    $exportStudentViewActivityArr[$i][] = $thisRole;
			    $exportStudentViewActivityArr[$i][] = $thisAttendance;
			    $exportStudentViewActivityArr[$i][] = $thisPerformance;
	     	}
	     	
	     	$actDisplay .= '</table>';

			$hiddenFieldDisplay .= "<input type='hidden' id='targetCategory' name='targetCategory' value='".$targetCategory."' />";
     		$hiddenFieldDisplay = "<input type='hidden' id='targetClass' name='targetClass' value='".$targetClass."' />";
     	 	$hiddenFieldDisplay .= "<input type='hidden' id='targetClub' name='targetClub' value='".$targetClub[0]."' />";
     		$hiddenFieldDisplay .= "<input type='hidden' id='usePeriod' name='usePeriod' value='".$usePeriod."' />";
     		$hiddenFieldDisplay .= "<input type='hidden' id='useCategory' name='useCategory' value='".$useCategory."' />";
     		$hiddenFieldDisplay .= "<input type='hidden' id='showPICchecked' name='showPICchecked' value='".$showPICchecked."' />";
     		$hiddenFieldDisplay .= "<input type='hidden' id='textFromDate' name='textFromDate' value='".$textFromDate."' />";
	     	$hiddenFieldDisplay .= "<input type='hidden' id='textToDate' name='textToDate' value='".$textToDate."' />";
     		
	     	
     		################################## End of Activity Table ##############################
       	}
       	else
       	{
	       	//class view
	       	$classView = true;
	       	if ($isClassTeacher)
	       	{
		       	//get the class name
		       	/*
		       	$sql = "SELECT ClassName FROM INTRANET_CLASS WHERE ClassID = '$targetClassID'";
		       	$result = $libenroll->returnArray($sql,1);
		       	$classSelection = $result[0][0];
		       	$targetClass = $result[0][0];
		       	*/
		       	
		       	$ObjClass = new year_class($targetClassID);
		       	$ObjFcm = new form_class_manage();
		       	$classSelection = $ObjClass->Get_Class_Name();
		       	
		       	$classSelection = $lclass->getSelectClassID(" id=\"targetClassID\" name=\"targetClassID[]\" multiple size=\"10\"", $targetClassID,2, Get_Current_Academic_Year_ID(), "", "", 1);
		       	$classSelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetClassID',1)");
		       	$classSelection .= $linterface->spacer();  	 
				$classSelection .= $linterface->MultiSelectionRemark();  
		       	
		       	$targetClass = $ObjClass->Get_Class_Name(); 
		       	//$hiddenFieldDisplay = "<input type='hidden' id='targetClassID' name='targetClassID' value='".$targetClassID."' />";
	       	}
	       	else
	       	{
			//class-based selection drop-down list
//				$classSelection = $lclass->getSelectClassID("name=\"targetClassID\"", $targetClassID);
			
				$classSelection = $lclass->getSelectClassID(" id=\"targetClassID\" name=\"targetClassID[]\" multiple size=\"10\"", $targetClassID,2);
				
				$classSelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetClassID',1)");
				$classSelection .= $linterface->spacer();  	 
				$classSelection .= $linterface->MultiSelectionRemark();  	 
	       	}
	       	
	       	$tempClub = $libenroll->Get_All_Club_Info();
	       	$ClubInfoAssoc = BuildMultiKeyAssoc($tempClub,"EnrolGroupID");
	       	$clubInfoArray = array();
	       	$p = 0;
	       	for($i=0; $i<sizeof($tempClub); $i++) {
		     	$clubInfoArray[$p][] = $tempClub[$i]['EnrolGroupID']; 	
		     	$clubInfoArray[$p][] = $intranet_session_language=="en" ? $tempClub[$i]['TitleWithSemester'] : $tempClub[$i]['TitleChineseWithSemester'];
		     	$p++;
	       	}
	      
	       	$clubSelection = getSelectByArray($clubInfoArray," id=\"targetClub\" name=\"targetClub[]\" multiple size=10 ",$targetClub,0,1,"- ".$i_general_please_select." -");
	       	$clubSelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetClub',1)");
	       	$clubSelection .= $linterface->spacer();  	 
			$clubSelection .= $linterface->MultiSelectionRemark(); 
	       	
	       	# Date selection
	       	$FromDateField = $linterface->GET_DATE_PICKER("textFromDate", $textFromDate);
	       	$ToDateField = $linterface->GET_DATE_PICKER("textToDate", $textToDate);
	       	
	       	//$FromDateField = $linterface->GET_DATE_FIELD2("textFromDateSpan", "form1", "textFromDate", $textFromDate, 1, "textFromDate");
	       	//$ToDateField = $linterface->GET_DATE_FIELD2("textToDateSpan", "form1", "textToDate", $textToDate, 1, "textToDate");
	       	$DateSelection = "";
	       	$DateSelection .= $i_From." ".$FromDateField." ".$i_To." ".$ToDateField;
	       	
	       	if ($usePeriod)
	       	{
				$usePeriodChecked = "checked";
				$periodDisplay = "block";
	       	}
	       	else
	       	{
				$usePeriodChecked = "";
				$periodDisplay = "none";
	       	}
	       	
	       	
			if($plugin['SIS_eEnrollment']){
		       	if($showTarget == 'priority'){
		       		$hidePeriodTR = 'style="display:none;"';
		       	}
			}
	       	
			$periodRowHTML = "";
			$periodRowHTML .= "<tr id=\"periodTR\" {$hidePeriodTR}>";
				$periodRowHTML .= "<td class=\"field_title\">";
					$periodRowHTML .= $eEnrollment['Period'];
				$periodRowHTML .= "</td>";
				$periodRowHTML .= "<td valign=\"top\" class=\"tabletext\">";
					$periodRowHTML .= "<input type=\"checkbox\" id=\"usePeriod\" name=\"usePeriod\" onclick=\"jsShowHideDiv('usePeriod','date_selection')\" ".$usePeriodChecked."><label for='usePeriod'>";
					$periodRowHTML .= $eEnrollment['Specify'];
					$periodRowHTML .= "<label><div id=\"date_selection\" style=\"display:".$periodDisplay."\">";
						$periodRowHTML .= $DateSelection;
					$periodRowHTML .= "</div>";
				$periodRowHTML .= "</td>";
			$periodRowHTML .= "</tr>";
			
	       	//show all student if a target class is set
	       	if(($showTarget=="class" && isset($targetClassID) && $targetClassID!="") || ($showTarget=="club" && isset($targetClub) && $targetClub!=""))
	       	{
		       
		       	if($showTarget=="class" && isset($targetClassID) && $targetClassID!="") {
			       	
					//$studentIDArr = $lclass->getClassStudentListOrderByClassNo($targetClassID); //UserID
					//$studentNameArr = $lclass->getStudentNameListByClassName($targetClassID);	//UserID, $name_field, ClassNumber
					
					$studentNameArr = array();
					$ClassStudentAssoc = array();
					foreach((array)$targetClassID as $thistargetClassID)
					{
						$YearClassObj = new year_class($thistargetClassID,$GetYearDetail=true,$GetClassTeacherList=false,$GetClassStudentList=true);
			        	$YearClassObj->Get_Class_StudentList($ArchiveSymbol='^');
			        	$studentNameArr =  array_merge($studentNameArr,$YearClassObj->ClassStudentList);
			        	$GroupStudentAssoc[$thistargetClassID] = Get_Array_By_Key($YearClassObj->ClassStudentList, 'UserID');
			        }

					$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
					
					if (!isset($useCategory)){
						unset($targetCategory);
					}
//					debug_pr($targetCategory);
					if($targetCategory==''&&$useCategory=='on'){
						$studentClubArr = array();
						$studentActivityArr = array();
					}else{
						$studentClubArr = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIDArr,$targetCategory);
						$studentActivityArr = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($studentIDArr,'',$targetCategory);
					}
			     	
			     	
//			     	debug_pr($studentClubArr);
			     				     	
		     	} else {
			     	$studentIDArr = array();
			     	$studentNameArr = array();
			     	
			     	/*
			     	$name_field = getNameFieldByLang('USR.');
			     	$sql = "SELECT USR.UserID, USR.ClassName, USR.ClassNumber, $name_field FROM INTRANET_USERGROUP USRGP 
			     			LEFT OUTER JOIN INTRANET_USER USR ON (USRGP.UserID=USR.UserID) 
			     			LEFT OUTER JOIN INTRANET_GROUP GP ON (GP.GroupID=USRGP.GroupID)
			     			WHERE USRGP.EnrolGroupID=$targetClub AND USR.RecordType=2 And GP.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
									
			     			ORDER BY USR.ClassName, USR.ClassNumber";
			     	$studentInfo = $libenroll->returnArray($sql);
			     	
			     	for($i=0; $i<sizeof($studentInfo); $i++) {
						list($uID, $clsName, $clsNo, $name) = $studentInfo[$i];
						$studentIDArr[] = $uID;
						$studentNameArr[$i][] = $uID;
						$studentNameArr[$i][] = $name;
						$studentNameArr[$i][] = $clsName." - ".$clsNo;
						
			     	}
					*/
					
					$studentNameArr = $libenroll->Get_Club_Member_Info((array)$targetClub, $PersonTypeArr=array(2), Get_Current_Academic_Year_ID(), $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=1, $WithEmptySymbol=1, $ReturnAsso=0, $YearTermIDArr='', $ClubKeyword='', $ClubCategoryArr='');
					$GroupStudentAssoc = BuildMultiKeyAssoc($studentNameArr, array("EnrolGroupID","UserID"),"UserID",1);
					
					foreach($targetClub as $targetClubID){
					    if($GroupStudentAssoc[$targetClubID] == ''){
					        $GroupStudentAssoc[$targetClubID] = array();
					    }
					}
					$studentIDArr = Get_Array_By_Key($studentNameArr, 'UserID');
					
					
					if (!isset($useCategory)){
						unset($targetCategory);
					}
					if($targetCategory==''&&$useCategory=='on'){
						$studentClubArr = array();
						$studentActivityArr = array();
					}else{
						$studentClubArr = $libenroll->GET_STUDENT_ENROLLED_CLUB_LIST($studentIDArr,$targetCategory);
						$studentActivityArr = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($studentIDArr,'',$targetCategory);
					}
					
//			     	debug_pr($studentClubArr);
			     	
		     	}

			     	if ($usePeriod)
			     	{
				     	$fromDateTime = $textFromDate." 00:00:00";
				     	$toDateTime = $textToDate." 23:59:59";
				     	$clubWithinPeriodArr = $libenroll->Get_Club_Within_Period($fromDateTime, $toDateTime);
				     	$activityWithinPeriodArr = $libenroll->Get_Event_Within_Period($fromDateTime, $toDateTime);
			     	}
			     	
			     	if(isset($Semester))
			     	{
			     		//villa B111852 
				     	//$term_group_list_temp = $libenroll->getGroupInfoList(0, $Semester, $EnrolGroupIDArr='', $getRegOnly=0, $ParCond="", $AcademicYearID);
				     	if(is_array($Semester)){
				     		$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', $Semester);
				     	}else{
				     		$term_group_list_temp = $libenroll->Get_All_Club_Info($EnrolGroupIDArr='', $AcademicYearID, $sel_category='', $keyword='', array($Semester));
				     	}
				     	$term_group_list = array();
				     	if(!empty($term_group_list_temp))
				     	{
					     	foreach($term_group_list_temp as $k=>$d)
					     		$term_group_list[] = $d['EnrolGroupID'];
				     	}
			     	}
			     	
		     		# build associative array
			     	$studentInfoArr = array();
//			     	debug_pr($studentNameArr);
//debug_pr($studentClubArr);
			     	foreach ($studentNameArr as $key => $studentDataArr)
			     	{
			     		$thisUserID = $studentDataArr['UserID'];
						$studentInfoArr[$thisUserID]['StudentName'] = $studentDataArr['StudentName'];
						$studentInfoArr[$thisUserID]['ClassName'] = $studentDataArr['ClassName'];
						$studentInfoArr[$thisUserID]['ClassNumber'] = $studentDataArr['ClassNumber'];
			     	}
			     	foreach ($studentClubArr as $key => $clubDataArr)
			     	{
			     		
						list($thisEnrolGroupID, $thisTitle, $thisUserID, $roleTitle, $TitleChinese) = $clubDataArr;
						$thisTitle = $intranet_session_language=="en" ? $thisTitle : $TitleChinese;
						$thisTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
						
						# filter the group if the group do not have gathering within the selected period
						if ($usePeriod && !in_array($thisEnrolGroupID, $clubWithinPeriodArr))
							continue;
						
//						if($sys_custom['eEnrolment']['heungto_enrolment_report'] && isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
						if(isset($Semester) && $Semester!="" && !in_array($thisEnrolGroupID, $term_group_list))
						{
							continue;
						}
						
						if (!isset($studentInfoArr[$thisUserID]['ClubInfoArr']))
						{
							$studentInfoArr[$thisUserID]['ClubInfoArr'] = array();
						}
						
						$studentInfoArr[$thisUserID]['ClubInfoArr'][] = array($thisEnrolGroupID, $thisTitle);
//			     	debug_pr($studentInfoArr);
			     	}
			     	foreach ($studentActivityArr as $key => $activityDataArr)
			     	{
						list($thisEnrolEventID, $thisEventTitle, $thisUserID, $roleTitle) = $activityDataArr;
						
						$thisEventTitle .= ($roleTitle!="") ? " (".$roleTitle.")" : "";
						
						# filter the group if the group do not have gathering within the selected period
						if ($usePeriod && !in_array($thisEnrolEventID, $activityWithinPeriodArr))
							continue;
						
						if (!isset($studentInfoArr[$thisUserID]['ActivityInfoArr']))
						{
							$studentInfoArr[$thisUserID]['ActivityInfoArr'] = array();
						}
						
						$studentInfoArr[$thisUserID]['ActivityInfoArr'][] = array($thisEnrolEventID, $thisEventTitle);
			     	}
			     	# end of building associative array

		     	//construct table title
		     	foreach((array)$GroupStudentAssoc as $ClassClubID => $StudentIDArr)
		     	{
		     		if($showTarget=="class")
		     		{
		     			$ObjClass = new year_class($ClassClubID);
	       				$GroupName = $ObjClass->Get_Class_Name();
		     		}
		     		else
		     		{
		     			$GroupName = $intranet_session_language=="en" ? $ClubInfoAssoc[$ClassClubID]['TitleWithSemester']:$ClubInfoAssoc[$ClassClubID]['TitleChineseWithSemester'];
		     		}
		     		$StudentIDArr = array_values((array)$StudentIDArr);
		     		
		     		$display .= "<br>".$linterface->GET_NAVIGATION2($GroupName);
		     	 	$display .= "<table class=\"common_table_list_v30 view_table_list_v30\">";
					$display .= "<thead><tr>";
					$display .= "
								<th>".$i_UserClassName."</td>
								<th>".$i_UserClassNumber."</td>
								<th>".$i_UserStudentName."</td>
								<th>".$eEnrollmentMenu['club']."</td>
								<th>".$eEnrollmentMenu['activity']."</td>
								";
					if($special_feature['eEnrolment']['EnrolmentSummary_DetailedPrint_select_student'])
					{
						$display .= "<td class=\"tablebluetop tabletopnolink\">&nbsp;</td>"; 
					}         
				    $display .= "</tr></thead>";
				    
				    $total_col = 5;
				   	$StudentSize = sizeof($StudentIDArr);
				   	
				   	//construct table content
				   	$display .= ($StudentSize==0) ? "<tr><td align='center' colspan='$total_col' class='tabletext tablerow2' height='80'><br>$i_no_record_exists_msg<br><br></td></tr>\n" : "";
		        	    
				    for ($i=0; $i<$StudentSize; $i++)
				    {
//				    	debug_pr($_POST);
//				    	$thisClubArr = array();
//				    	debug_pr($thisClubArr);
					    $thisStudentID = $StudentIDArr[$i];
					    $thisStudentName = $studentInfoArr[$thisStudentID]['StudentName'];
					    $thisClassName = $studentInfoArr[$thisStudentID]['ClassName'];
					    $thisClassNumber = $studentInfoArr[$thisStudentID]['ClassNumber'];
					     
					    $thisClubArr = $studentInfoArr[$thisStudentID]['ClubInfoArr'];
					    $thisActivityArr = $studentInfoArr[$thisStudentID]['ActivityInfoArr'];
					   
					    # build name link
					    $thisNameLink = "";
					    $thisNameLink .= "<a class='tablelink' href='javascript:goStudentDetails(".$thisStudentID.")' >".$thisStudentName."</a>";
					    
					    # build clubs link
					    $sizeClubArr = count($thisClubArr);
					    $thisClubLink = "";
					    for ($j=0; $j<$sizeClubArr; $j++)
					    {
						    list($thisEnrolGroupID, $thisTitle) = $thisClubArr[$j];
						    
						    
						    $ClubPICArr = $libenroll->GET_GROUPSTAFF($thisEnrolGroupID,'PIC');
						    $ClubPICName= array();
						    for($k=0;$k<sizeof($ClubPICArr);$k++){
						    	$ClubPICID =$ClubPICArr[$k]['UserID'];
						    	$ClubPICNameAry = $libenroll->Get_User_Info_By_UseID($ClubPICID);
						    	$ClubPICName[] = Get_Lang_Selection($ClubPICNameAry[0]['ChineseName'], $ClubPICNameAry[0]['EnglishName']);
						    	
						    }
						    
						    $ClubPICNameList = implode(',',$ClubPICName);
						    $thisClubLink .= "<a class='tablelink' href=\"javascript: jsGoEventDetails('".$thisStudentID."', '".$thisEnrolGroupID."', 'club')\" >\n";
						    if($showPICname){
						    	$thisClubLink .= $thisTitle." ".$ClubInfo[0]['Title']." (".$Lang['eEnrolment']['ClubPIC'].":".$ClubPICNameList.")\n";
						    }else{
						    	$thisClubLink.= $thisTitle."\n";
						    }
						    
						    $thisClubLink .= "</a>\n";
						    
						    if ($j < $sizeClubArr-1 )
						    {
							    $thisClubLink .= "<br />\n";
						    }
						    
					    }
					    # build activities link
					    $sizeActivityArr = count($thisActivityArr);
					    $thisActivityLink = "";
					    for ($j=0; $j<$sizeActivityArr; $j++)
					    {
						    list($thisEnrolEventID, $thisTitle) = $thisActivityArr[$j];
						    $thisActivityLink .= "<a class='tablelink' href=\"javascript: jsGoEventDetails('".$thisStudentID."', '".$thisEnrolEventID."', 'activity')\" >\n";
						    
						    $EventPICArr = $libenroll->GET_EVENTSTAFF($thisEnrolEventID,'PIC');
						    $EventPICName = array();
						    for($k=0;$k<sizeof($EventPICArr);$k++){
						    	$EventPICID = $EventPICArr[$k]['UserID'];
						    	$EventPICNameAry = $libenroll->Get_User_Info_By_UseID($EventPICID);
						    	$EventPICName[] = Get_Lang_Selection($EventPICNameAry[0]['ChineseName'],$EventPICNameAry[0]['EnglishName']);
						    }
						    
						    $EventPICList = implode(', ',$EventPICName);
						    
						    
						    $thisActivityLink .= "<a class='tablelink' href=\"javascript: jsGoEventDetails('".$thisStudentID."', '".$thisEnrolEventID."', 'activity')\" >\n";
						    
						    if($showPICname){
						    	$thisActivityLink .= $thisTitle." (".$Lang['eEnrolment']['ActivityPIC'].":".$EventPICList.")\n";
						    }else{
						    	$thisActivityLink .= $thisTitle."\n";
						    }
						    
						    $thisActivityLink .= "</a>\n";
						    
						    if ($j < $sizeActivityArr-1 )
						    {
							    $thisActivityLink .= "<br />\n";
						    }
					    }			    
				 				
						$display .= '<tr>';
						$display .= '
									<td class="tabletext" valign="top">'.$thisClassName.'</td>
									<td class="tabletext" valign="top">'.$thisClassNumber.'</td>
									<td valign="top">'.$thisNameLink.'</td>
					          	  	<td class="tabletext" align="left" valign="top">'.$thisClubLink.'</td>
					          	  	<td class="tabletext" align="left" valign="top">'.$thisActivityLink.'</td>
					          	  	';
					    if($special_feature['eEnrolment']['EnrolmentSummary_DetailedPrint_select_student'])
					    {
					    	$display .= '<td class="tabletext" align="left" valign="top"><input type="checkbox" name="selected_student[]" value="'. $thisStudentID.'"></td>';
				    	}
				    	$display .= '</tr>';
					}
				    $display .= '</table>';
		     	}
	       	}else if( ($plugin['SIS_eEnrollment']) && (strtolower($showTarget) == strtolower('priority')) ){
	       		include_once($PATH_WRT_ROOT."includes/libexporttext.php");
	       		include('export_priority.php');
	       		exit();
	       	}
       	}
       	if($plugin['SIS_eEnrollment']){
       		$prioritySelection = '';
	       	if(empty($roundFilter)){
	       		$roundFilter = $libenroll->targetRound;
	       	}
	       	for ($i = 1; $i <= $sis_eEnrollmentConfig['totalRound']; $i++) {
	       		if($roundFilter == $i){
	       			$checked = 'checked';
	       		}else{
	       			$checked = '';
	       		}
	       		$prioritySelection .= '<input type="radio" id="roundFilter'.$i.'" name="roundFilter" value="'.$i.'" '.$checked.'/>';
	       		$prioritySelection .= '<label for="roundFilter'.$i.'">'.$Lang['SIS_eEnrollment']['editCCA']['Round'][$i].'</label>&nbsp;';
	       	}
       	}
    }      
    
	$linterface->LAYOUT_START();
	//debug_pr($clubPerformArr[2]['Performance']);
	### Get toolbar
	$x = '';
	// Export
	$buttonHref = '';
	$optionAry = array();
	if ($classView) {
		$buttonHref = "javascript:void(0);";
		
		$optionAry[] = array("javascript:clickedExport('$showTarget', 'simple', 'simpleExportBtn');", $Lang['eEnrolment']['SimpleView1'], 'simpleExportBtn');
		$optionAry[] = array("javascript:clickedExport('$showTarget', 'simple2', 'simple2ExportBtn');", $Lang['eEnrolment']['SimpleView2'], 'simple2ExportBtn');
		$optionAry[] = array("javascript:clickedExport('$showTarget', 'detailed', 'detailedExportBtn');", $Lang['eEnrolment']['DetailedView'], 'detailedExportBtn');

		//if ($sys_custom['eEnrolment']['heungto_enrolment_report'] && $showTarget == 'class') {
		if ($showTarget == 'class') {
			$optionAry[] = array("javascript:clickedExport('class', 'enrolledClubList');", $Lang['eEnrolment']['EnrolledClubList']);
		}
		
		if($showTarget == 'class' && $sys_custom['eEnrolment']['Refresh_Performance']){
// 		    $optionAry[] = array("javascript:clickedExport('class', 'enrolledClubList2');", $Lang['eEnrolment']['EnrolledClubList']."(".$Lang['eEnrolment']['SemesterGrade'].")");
		}
	}
	else if ($studentView) {
		$buttonHref = "javascript:clickedExport('student');";
	}
	$x .= $linterface->Get_Content_Tool_v30('export', $buttonHref, $text="", $optionAry, $other="", $divID='', $extra_class='', $hideDivAfterClicked=false, $extra_onclick="hideAllOptionLayer();");
	
	// Print
	$buttonHref = '';
	$optionAry = array();
	if ($classView) {
		$buttonHref = "javascript:void(0);";
		
		$optionAry[] = array("javascript:clickedPrint('$showTarget', 'simple', 'simplePrintBtn');", $Lang['eEnrolment']['SimpleView1'], 'simplePrintBtn');
		$optionAry[] = array("javascript:clickedPrint('$showTarget', 'simple2', 'simple2PrintBtn');", $Lang['eEnrolment']['SimpleView2'], 'simple2PrintBtn');
		$optionAry[] = array("javascript:clickedPrint('$showTarget', 'detailed', 'detailedPrintBtn');", $Lang['eEnrolment']['DetailedView'], 'detailedPrintBtn');
		
		$applyPageBreakTitle = ($showTarget=='club')? $Lang['eEnrolment']['ApplyPageBreakForEachClub'] : $Lang['eEnrolment']['ApplyPageBreakForEachClass'];
	}
	else if ($studentView) {
		$buttonHref = "javascript:clickedPrint('student');";
	}
	$x .= $linterface->Get_Content_Tool_v30('print', $buttonHref, $text="", $optionAry, $other="", $divID='', $extra_class='', $hideDivAfterClicked=false, $extra_onclick="hideAllOptionLayer();");
	$htmlAry['toolbar'] = $x;
	
	
	### Get simple print / export option layer
	$x = '';
	$x .= '<div id="simpleOptionDiv" class="selectbox_layer" style="width:400px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideSimpleOptionDiv();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="form_table_v30">'."\r\n";
							$x .= '<colgroup>'."\r\n";
								$x .= '<col class="field_title" style="vertical-align:top">'."\r\n";
								$x .= '<col class="field_c" style="vertical-align:top">'."\r\n";
							$x .= '</colgroup>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td>'.$applyPageBreakTitle.'</td>'."\r\n";
								$x .= '<td>:</td>'."\r\n";
								$x .= '<td>'."\r\n";
									$x .= $linterface->Get_Radio_Button('simpleApplyPageBreak_yes', 'simpleApplyPageBreak', 1, $isChecked=1, $Class="", $Lang['General']['Yes'])."\r\n";
									$x .= $linterface->Get_Radio_Button('simpleApplyPageBreak_no', 'simpleApplyPageBreak', 0, $isChecked=0, $Class="", $Lang['General']['No'])."\r\n";
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
						$x .= '</table>'."\r\n";
						$x .= '<div class="edit_bottom_v30">'."\r\n";
							$x .= '<div id="simplePrintBtnDiv">'."\r\n";
								$x .= $linterface->GET_BTN($Lang['Btn']['Print'], "button", "goPrint('$showTarget', 'simple');");
							$x .= '</div>'."\r\n";
							$x .= '<div id="simpleExportBtnDiv">'."\r\n";
								$x .= $linterface->GET_BTN($Lang['Btn']['Export'], "button", "goExport('$showTarget', 'simple');");
							$x .= '</div>'."\r\n";
						$x .= '</div>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	$htmlAry['simpleOptionDiv'] = $x;
	
	
	### Get detailed print / export option layer
	$x = '';
	$x .= '<div id="detailedOptionDiv" class="selectbox_layer" style="width:400px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideDetailedOptionDiv();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="form_table_v30">'."\r\n";
							$x .= '<colgroup>'."\r\n";
								$x .= '<col class="field_title" style="vertical-align:top">'."\r\n";
								$x .= '<col class="field_c" style="vertical-align:top">'."\r\n";
							$x .= '</colgroup>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td>'.$Lang['eEnrolment']['record_type'].'</td>'."\r\n";
								$x .= '<td>:</td>'."\r\n";
								$x .= '<td>'."\r\n";
									$x .= '<input type="checkbox" name="display_club" value="1" id="display_club" checked> <label for="display_club">'.$eEnrollment['club'].'</label><br>'."\r\n";
									$x .= '<input type="checkbox" name="display_activity" value="1" id="display_activity" checked> <label for="display_activity">'.$eEnrollment['activity'].'</label>'."\r\n";
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
							$x .= '<tr>'."\r\n";
								$x .= '<td>'.$Lang['eEnrolment']['show_data'].'</td>'."\r\n";
								$x .= '<td>:</td>'."\r\n";
								$x .= '<td>'."\r\n";
									$x .= '<input type="checkbox" name="display_role" value="1" id="display_role" checked> <label for="display_role">'.$eEnrollment['role'].'</label><br>'."\r\n";
									$x .= '<input type="checkbox" name="display_attendance" value="1" id="display_attendance" checked> <label for="display_attendance">'.$Lang['eEnrolment']['attendance_percent'].'</label><br>'."\r\n";
									$x .= '<input type="checkbox" name="display_performance" value="1" id="display_performance" checked> <label for="display_performance">'.$eEnrollment['performance'].'</label><br>'."\r\n";
									// Allow user to select for related group
									$x .= '<input type="checkbox" name="display_belong__to_group" value="1" id="display_belong__to_group" checked> <label for="display_belong__to_group">'.$eEnrollment['belong_to_group'].'</label><br>'."\r\n";
									$x .= '<input type="checkbox" name="display_email" value="1" id="display_email" checked> <label for="display_email">'.$Lang['eEnrolment']['show_email'].'</label><br>'."\r\n";
									$x .= '<input type="checkbox" name="display_picname" value="1" id="display_picname" checked> <label for="display_picname">'.$Lang['eEnrolment']['ClubPIC'].'</label>'."\r\n";
									
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
							$x .= '<tr id="detailedApplyPageBreakTr">'."\r\n";
								$x .= '<td>'.$Lang['eEnrolment']['ApplyPageBreakForEachStudent'].'</td>'."\r\n";
								$x .= '<td>:</td>'."\r\n";
								$x .= '<td>'."\r\n";
									$x .= $linterface->Get_Radio_Button('detailedApplyPageBreak_yes', 'detailedApplyPageBreak', 1, $isChecked=1, $Class="", $Lang['General']['Yes'])."\r\n";
									$x .= $linterface->Get_Radio_Button('detailedApplyPageBreak_no', 'detailedApplyPageBreak', 0, $isChecked=0, $Class="", $Lang['General']['No'])."\r\n";
								$x .= '</td>'."\r\n";
							$x .= '</tr>'."\r\n";
							if ($special_feature['eEnrolment']['EnrolmentSummary_DetailedPrint_select_student'] ) {
								$x .= '<tr>'."\r\n";
									$x .= '<td>'.$Lang['eEnrolment']['DisplaySelectedStudentOnly'].'</td>'."\r\n";
									$x .= '<td>:</td>'."\r\n";
									$x .= '<td>'."\r\n";
										$x .= '<input type="radio" name="selected_printing" value="1" id="selected_printing1"> <label for="selected_printing1">'.$i_general_yes.'</label>'."\r\n"; 
										$x .= '<input type="radio" name="selected_printing" value="0" id="selected_printing0" checked> <label for="selected_printing0">'.$i_general_no.'</label>'."\r\n";
									$x .= '</td>'."\r\n";
								$x .= '</tr>'."\r\n";
							}
						$x .= '</table>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
		$x .= '<div class="edit_bottom_v30">'."\r\n";
			$x .= '<div id="detailedPrintBtnDiv">'."\r\n";
				$x .= $linterface->GET_BTN($Lang['Btn']['Print'], "button", "goPrint('$showTarget', 'detailed');");
			$x .= '</div>'."\r\n";
			$x .= '<div id="detailedExportBtnDiv">'."\r\n";
				$x .= $linterface->GET_BTN($Lang['Btn']['Export'], "button", "goExport('$showTarget', 'detailed');");
			$x .= '</div>'."\r\n";
		$x .= '</div>'."\r\n";
	$x .= '</div>'."\r\n";
	$htmlAry['detailedOptionDiv'] = $x;
	
	### Get simple2 export/print option layer
	$x = '';
	$x .= '<div id="simple2OptionDiv" class="selectbox_layer" style="width:400px;">'."\r\n";
		$x .= '<table cellspacing="0" cellpadding="3" border="0" width="100%">'."\r\n";
			$x .= '<tbody>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="right" style="border-bottom: medium none;">'."\r\n";
						$x .= '<a href="javascript:hideDetail2OptionDiv();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<td align="left" style="border-bottom: medium none;">'."\r\n";
						$x .= '<table class="form_table_v30">'."\r\n";
							$x .= '<colgroup>'."\r\n";
								$x .= '<col class="field_title" style="vertical-align:top">'."\r\n";
								$x .= '<col class="field_c" style="vertical-align:top">'."\r\n";
							$x .= '</colgroup>'."\r\n";
							
						$x .= '</table>'."\r\n";
						$x .= '<div class="edit_bottom_v30">'."\r\n";
							$x .= '<div id="simple2PrintBtnDiv">'."\r\n";
								$x .= $linterface->GET_BTN($Lang['Btn']['Print'], "button", "goPrint('$showTarget', 'simple2');");
							$x .= '</div>'."\r\n";
							$x .= '<div id="simple2ExportBtnDiv">'."\r\n";
								$x .= $linterface->GET_BTN($Lang['Btn']['Export'], "button", "goExport('$showTarget', 'simple2');");
							$x .= '</div>'."\r\n";
						$x .= '</div>'."\r\n";
					$x .= '</td>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	$htmlAry['simple2OptionDiv'] = $x;
	
	

	### Get remarks display
	$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks(array('InactiveStudent'));
	
	### Club Category Selection
	$ClubCategoryArr = $libenroll->GET_CATEGORY_LIST();
	$CategorySelection = getSelectByArray($ClubCategoryArr," id=\"targetCategory\" name=\"targetCategory[]\" multiple size=10 ",$targetCategory,0,1,"- ".$i_general_please_select." -");
	$CategorySelection .= "&nbsp;".$linterface->Get_Small_Btn($Lang['Btn']['SelectAll'],"button","js_Select_All_With_OptGroup('targetCategory',1)");
	$CategorySelection .= $linterface->spacer();  	 
	$CategorySelection .= $linterface->MultiSelectionRemark();  

	$ClubCategoryAssoArr = BuildMultiKeyAssoc($ClubCategoryArr,"CategoryID",$IncludedDBField=array("CategoryName"));

	if (!empty($targetCategory)){
		if($useCategory){
			foreach ($targetCategory as $CatID){
				$SelectedCat[] = $ClubCategoryAssoArr[$CatID]['CategoryName']. " ";
			}
			$displayCat = implode(', ',$SelectedCat);
		}
		else{
			$displayCat = '';
		}
	}
	
 	if ($useCategory){
		$useCategoryChecked = "checked";
		$categoryDisplay = "block";
	}
	else{
		$useCategoryChecked = "";
		$categoryDisplay = "none";
	}

	if (!$studentView){
		$categoryRowHTML .= "<td class=\"field_title\">".$eEnrollment['category']."</td>";
			$categoryRowHTML .= "<td>";	
			$categoryRowHTML .= "<input type=\"checkbox\" id=\"useCategory\" name=\"useCategory\" $useCategoryChecked onclick=\"jsShowHideDiv('useCategory', 'category_selection');\" >";
			$categoryRowHTML .= "<label for='useCategory'>".$eEnrollment['Specify']."</label>";
			$categoryRowHTML .= "<div id=\"category_selection\" style=\"display:$categoryDisplay;\">";
			$categoryRowHTML .= $CategorySelection;
			$categoryRowHTML .= "</div>";
		$categoryRowHTML .= "</td>";
	}
	else if($studentView && $useCategory) {
		$categoryRowHTML .= "<td class=\"field_title\">".$eEnrollment['category']."</td>";
		$categoryRowHTML .= "<td><label for=\"category\">$displayCat</label></td>";
	}
	else{
		$categoryRowHTML ='';
	}
	
	$showPICchecked = "";
	if($showPICname){
		$showPICchecked = "checked";
	}
	
	if (!$studentView){
		$PICNameRowHTML.= "<td class=\"field_title\">".$Lang['eEnrolment']['show_data']."</td>";
		$PICNameRowHTML.= "<td>";
		$PICNameRowHTML.= "<input type=\"checkbox\" id=\"showPICname\" name=\"showPICname\" $showPICchecked >".$Lang['eEnrolment']['ClubPIC']."</input>";
		$PICNameRowHTML.= "</td>";
	}else{
		$PICNameRowHTML= "";
	}

?>

<script language="javascript">

$(document).ready( function() {
	if ($('input#target01').attr('checked')) {
		clickedClassRadio();
		<?php if(empty($_POST)){?>
		js_Select_All('targetClassID', 1);
		<?php }?>
	}
	else if ($('input#target02').attr('checked')) {
		clickedClubRadio();
		<?php if(empty($_POST)){?>
		js_Select_All('targetClub', 1);
		<?php }?>
	}
});

function goStudentDetails(studentID) {
	document.getElementById("studentID").value = studentID;
	
	document.form1.action = "index.php";
	document.form1.target = "_self";
	document.form1.method = "post";
	document.form1.submit();
}

function formRefresh(){

	if($("#target01").attr("checked") && !$("#targetClassID").val()) {
		alert('<?=$Lang['eEnrolment']['jsWarning']['SelectClass']?>');
		return false;
	}
	else if($("#target02").attr("checked") && !$("#targetClub").val()) {
		alert('<?=$Lang['eEnrolment']['jsWarning']['SelectClub']?>');
		return false;
	}
	
	if (compareDate($('#textToDate').val(), $('#textFromDate').val()) < 0) {
		alert('<?=$i_con_msg_date_startend_wrong_alert?>');
		return false;
	}
	
	document.form1.action = "index.php";
	document.form1.target = "_self";
	document.form1.method = "post";
	document.form1.submit();
}

function jsBackClassView(){
	var isIE = /*@cc_on!@*/false || !!document.documentMode;
	if(isIE){
		document.form1.action = "index.php";
		document.form1.target = "_self";
		document.getElementById("studentID").value = "";
		document.form1.method = "post";
		document.form1.submit();
	}else{
		window.history.back();
	}
}

function jsGoEventDetails(studentID, eventID, actionType){
	if (actionType == "club")
	{
		document.getElementById("studentID").value = studentID;
		document.getElementById("EnrolGroupID").value = eventID;
		document.form1.action = "group_student_comment.php";
	}
	else
	{
		document.getElementById("studentID").value = studentID;
		document.getElementById("EnrolEventID").value = eventID;
		document.form1.action = "event_student_comment.php";
	}
	document.form1.target = "_self";
	document.form1.method = "post";
	document.form1.submit();
}

function jsShowHideDiv(chkBoxID,divID){
	var Chkbox = $('input#'+chkBoxID);
	var Div = $('div#'+divID);
	if (Chkbox.attr("checked")){
		Div.css('display', 'block');
	}
	else{
		Div.css('display', 'none');
	}
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
}

function clickedClassRadio() {
	showSpan('spanClass');
	hideSpan('spanClub');
	<?php if($plugin['SIS_eEnrollment']){ ?>
		hideSpan('spanPriority');
		$('#periodTR').show();
	<?php } ?>
}
function clickedClubRadio() {
// 	js_Select_All('targetClub', 1);
	showSpan('spanClub');
	hideSpan('spanClass');
	<?php if($plugin['SIS_eEnrollment']){ ?>
		hideSpan('spanPriority');
		$('#periodTR').show();
	<?php } ?>
}
<?php if($plugin['SIS_eEnrollment']){ ?>
function clickedPriorityRadio() { // SIS customization
	hideSpan('spanClub');
	hideSpan('spanClass');
	$('#spanPriority').attr('style', 'display: table-row;');
	$('#periodTR').hide();
	if($('#usePeriod').attr('checked')){
		$('#usePeriod').click();
	}
}
<?php } ?>

function clickedExport(viewSource, viewMode, btnId) {
	hideAllOptionLayer();
	
	if (viewSource == 'class' || viewSource == 'club') {
		if (viewMode == 'simple') {
			goExport(viewSource, viewMode);
		}
		else if (viewMode == 'simple2') {
			goExport(viewSource, viewMode);
		}
		else if (viewMode == 'detailed') {
			$('div#detailedPrintBtnDiv').hide();
			$('tr#detailedApplyPageBreakTr').hide();
			$('div#detailedExportBtnDiv').show();
			showDetailedOptionDiv(btnId, 'export', -20);
		}
		else if (viewMode == 'enrolledClubList') {
			document.form1.action = "export_enroled_club_list.php";
			document.form1.target = "_self";
			document.form1.method = "post";
			document.form1.submit();
			document.form1.action = "";
		}
		else if (viewMode == 'enrolledClubList2') {
			document.form1.action = "export_enroled_club_list2.php";
			document.form1.target = "_self";
			document.form1.method = "post";
			document.form1.submit();
			document.form1.action = "";
		}
	}
	else if (viewSource == 'student') {
		goExport(viewSource, viewMode);
	}
}

function clickedPrint(viewSource, viewMode, btnId) {
	hideAllOptionLayer();
	
	if (viewSource == 'class' || viewSource == 'club') {
		if (viewMode == 'simple') {
			$('div#simplePrintBtnDiv').show();
			$('div#simpleExportBtnDiv').hide();
			showSimpleOptionDiv(btnId, 'print');
		}
		if(viewMode == 'simple2'){
// 			$('div#simple2PrintBtnDiv').show();
// 			$('div#simple2ExportBtnDiv').hide();
			goPrint(viewSource, viewMode);
			}
		else if (viewMode == 'detailed') {
			$('div#detailedPrintBtnDiv').show();
			$('tr#detailedApplyPageBreakTr').show();
			$('div#detailedExportBtnDiv').hide();
			showDetailedOptionDiv(btnId, 'print', -20);
		}
	}
	else if (viewSource == 'student') {
		document.getElementById("PrintType").value = 'student_view';
	
		document.form1.action = "print.php";
		document.form1.target = "_blank";
		document.form1.method = "post";
		document.form1.submit();
	}
}

function goPrint(viewSource, viewMode) {
	
	var canPrint = false;
	
	if (viewSource == 'class' || viewSource == 'club') {
	
		document.getElementById("PrintType").value = 'class_view';
	}
	else if (viewSource == 'student') {
		document.getElementById("PrintType").value = 'student_view';
	}
	
	if (viewMode == 'simple') {
		canPrint = true;
		document.form1.action = "print.php";
	}
	if (viewMode == 'simple2') {
		canPrint = true;
		document.form1.action = "sample2_print.php";
	}
	else if (viewMode == 'detailed' && checkDetailedOptionValid()) {
		canPrint = true;
		document.form1.action = "bulk_print.php";
	}
	
	if (canPrint) {
		document.form1.target = "_blank";
		document.form1.method = "post";
		document.form1.submit();
		document.form1.action = "";
		document.form1.target = "";
		hideAllOptionLayer();
		js_Clicked_Option_Layer_Button('print_option', 'btn_print');
	}
}

function goExport(viewSource, viewMode) {
	var canExport = false;
	
	if (viewSource == 'class' || viewSource == 'club') {
		if (viewMode == 'simple') {
			canExport = true;
			document.form1.action = "export_simple.php";
		}
		else if (viewMode == 'simple2' && checkDetailedOptionValid()) {
			
			canExport = true;
			document.form1.action = "export_simple2.php";
		}
		else if (viewMode == 'detailed' && checkDetailedOptionValid()) {
			
			canExport = true;
			document.form1.action = "export_detailed.php";
		}
		
	}
	else if (viewSource == 'student') {
		canExport = true;
			document.form1.action = "export_student.php";
	}
	
	if (canExport) {
		document.form1.target = "_self";
		document.form1.method = "post";
		document.form1.submit();
		document.form1.action = "";
		hideAllOptionLayer();
		js_Clicked_Option_Layer_Button('export_option', 'btn_export');
	}
}

function showSimpleOptionDiv(btnId, btnType) {
	var optionDivId = 'simpleOptionDiv';
	var leftAdjustment = $('div#' + btnType + '_option').width();
	var topAdjustment = 0;
	
	changeLayerPosition(btnId, optionDivId, leftAdjustment, topAdjustment);
	MM_showHideLayers(optionDivId, '', 'show');
}
function hideSimpleOptionDiv() {
	MM_showHideLayers('simpleOptionDiv', '', 'hide');
}

function showDetailedOptionDiv(btnId, btnType, topAdjustment) {
	
	var optionDivId = 'detailedOptionDiv';
	var leftAdjustment = $('div#' + btnType + '_option').width();
	topAdjustment = topAdjustment || 0;
	
	changeLayerPosition(btnId, optionDivId, leftAdjustment, topAdjustment);
	MM_showHideLayers(optionDivId, '', 'show');
	
}
function hideDetailedOptionDiv() {
	MM_showHideLayers('detailedOptionDiv', '', 'hide');
}

function hideAllOptionLayer() {
	hideSimpleOptionDiv();
	hideDetailedOptionDiv();
}

function checkDetailedOptionValid() {
	
	var check1 = 1;
	if(returnChecked(document.form1, "display_club")==null && returnChecked(document.form1, "display_activity")==null) {	
		check1 = 0;
		alert("<?=$Lang['eEnrolment']['select_club_activity']?>");
	}
		
	// role, attendance & performance (at least one)
	var check2 = 1;
	
	if(returnChecked(document.form1, "display_role")==null && 
			returnChecked(document.form1, "display_attendance")==null && 
			returnChecked(document.form1, "display_performance")==null &&
			returnChecked(document.form1, "display_belong__to_group")==null&&
			returnChecked(document.form1, "display_email")==null
			){
		check2 = 0;
		alert("<?=$Lang['eEnrolment']['select_role_attendance_performance']?>");
	}
	

	var check3 = 1;
	<? if($special_feature['eEnrolment']['EnrolmentSummary_DetailedPrint_select_student']) {?>
	if(document.form1.selected_printing[0].checked) {
		if(!check_checkbox(document.form1,"selected_student[]")) {
			check3 = 0;
			alert("<?=$Lang['General']['JS_warning']['SelectAtLeastOneStudent']?>");
		}
	}
	<? } ?>
	
	return (check1 && check2 && check3)? true : false;
}
function checkform(){
	var check;
	check = false;
	<?php if($sys_custom['eEnrolment']['heungto_enrolment_report']){?>
	check = true;
	<?php }else{?>
	$('.termCheckBox').each(function(){
		if($(this).attr('checked')){
			check = true;
		}
	});
	<?php }?>
	if(check){
		form1.submit();
	}else{
		alert("<?=$Lang['eEnrolment']['jsWarning']['NoClubTypeIsSelected'] ?>");
		return false;
	}
}
</script>
	<br>
	<form name="form1" method="POST">
				<table class="form_table_v30">
						<tr>
							<td class="field_title"><?=$linterface->RequiredSymbol().$eEnrollment['Target']?></td>
							<td valign="top" class="tabletext">
								<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target01" value="class" <? if($showTarget=="" || $showTarget=="class") echo "checked"; ?> onclick="clickedClassRadio();"><? } ?><? if((!$studentView) || $showTarget=="class") { ?><label for="target01"><?=$i_general_class?></label><? } ?>
								<? if(!$studentView) { ?><input type="radio" name="showTarget" id="target02" value="club" <? if($showTarget=="club") echo "checked";?> onclick="clickedClubRadio();"><? } ?><? if((!$studentView) || $showTarget=="club") { ?><label for="target02"><?=$eEnrollmentMenu['club']?></label><? } ?>
								<? if($plugin['SIS_eEnrollment'] && !$studentView) { ?><input type="radio" name="showTarget" id="target03" value="priority" <? if($showTarget=="priority") echo "checked";?> onclick="clickedPriorityRadio();"><? } ?><? if((!$studentView) || $showTarget=="priority") { ?><label for="target03"><?=$Lang['SIS_eEnrollment']['report']['enrolmentSummary']['priorityReport']?></label><? } ?>
								<br>
								<div id="spanClass" style="display:<? if($showTarget=="" || $showTarget=="class") {echo "inline";} else {echo "none";} ?>">
									<?= $classSelection ?>
								</div>
								<div id="spanClub" style="display:<? if($showTarget=="club") {echo "inline";} else {echo "none";} ?>">
									<?=$clubSelection?>
								</div>
								
							</td>
							
						</tr>
						
							<?= $periodRowHTML ?>						

							<?=$categoryRowHTML?>
						<tr>
							<?=$PICNameRowHTML?>
						</tr>
						<?php if($plugin['SIS_eEnrollment']){ ?>
							<tr id="spanPriority" style="display:<? if($showTarget=="priority") {echo "table-row";} else {echo "none";} ?>" >
								<td width="10%" nowrap="nowrap" class="formfieldtitle tabletext" valign="top"><?=$Lang['SIS_eEnrollment']['Round']?></td>
								<td valign="top" class="tabletext">
									<?=$prioritySelection?>
								</td>
							</tr>
						<?php } ?>
						
						<? //if($sys_custom['eEnrolment']['heungto_enrolment_report']) {?>
						<? if (!$studentView){?>
						<tr>
							<td class="field_title"><?=$eEnrollment['ClubType']?></td>
							<td valign="top" class="tabletext" ><?=$SemesterFilter?> <span class="tabletextremark"></span></td>
						</tr>
						
						<? } ?>
							<?
							if ($classView)
							{
							?>
								<tr>
									<td colspan="2" valign="top" style="border-bottom: 0px;">
										<div class="edit_bottom_v30">
											<p class="spacer"></p>
												<?= $linterface->GET_ACTION_BTN($eEnrollment['generate_or_update'], "button", "checkform()")?>&nbsp;	
											<p class="spacer"></p>
										</div>
									</td>
								</tr>
							<?
							}
							?>
							
							<?
							if ($studentView)
							{
							?>
							<tr>
								<td class="field_title"><?=$i_UserStudentName?></td>
								<td valign="top" class="tabletext">
									<?= $studentNameDisplay ?>
								</td>
							</tr>
							<?
							}
							?>
				</table>
		
		<?
		if ($classView && (((isset($targetClassID) && $targetClassID!="")) || (isset($targetClub) && $targetClub!="")))
		{
		?>						
			<br />
				<div class="content_top_tool">
					<div class="Conntent_tool">
						<?=$htmlAry['toolbar']?>
					</div>
				</div>
			<br />
		<?
		}
		?>				
		
		<?
		if ($classView && ((isset($targetClassID) && $targetClassID!="") || $targetClub))
		{
		?>
			<?=$display?>
			<br />
			<?=$RemarksTable?>
		<?
		}
		else if ($studentView)
		{
		?>	<br />
			<div class="dotline"><img src="/images/2009a/10x10.gif" width="10" height="1"></div>
			<table>
				<tr>
					<td valign="top">
						<div class="content_top_tool">
							<div class="Conntent_tool">
								<?=$htmlAry['toolbar']?>
							</div>
						</div>
					</td>
				</tr>
			</table>
			
			<?=$clubDisplay?><br />
			
			<?=$actDisplay?>
			
			<br />
			<?= $linterface->MandatoryField()?>
			<?=$RemarksTable?>			
		<?
		}
		?>
		
		<?=$hiddenFieldDisplay?>
		

			<?
			if ($studentView)
			{
			?>
			<?
			}
			?>
			
				<?
				if ($classView)
				{
				?>

				<?
				}
				else if ($studentView)
				{
				?>		
					<div class="edit_bottom_v30">
						<p class="spacer"></p>
							<input type="hidden" name="showTarget" value="<?=$showTarget?>">
								<?= $linterface->GET_ACTION_BTN($button_back, "button", "jsBackClassView()")?>&nbsp;
						<p class="spacer"></p>
					</div>

				<?
				}
				?>

		</blockquote>
		
		<?=$htmlAry['simpleOptionDiv']?>
		<?=$htmlAry['detailedOptionDiv']?>		
		
		<input type="hidden" id="studentID" name="studentID" value="<?=$studentID?>"/>
		<input type="hidden" id="PrintType" name="PrintType" />
		<input type="hidden" id="EnrolEventID" name="EnrolEventID" />
		<input type="hidden" id="EnrolGroupID" name="EnrolGroupID" />
		<input type="hidden" name="selectedTargetClassIDArr" value="<?=rawurlencode(serialize($targetClassID))?>">
		<input type="hidden" name="studentIDArr" value="<?=rawurlencode(serialize($studentIDArr))?>">
		<input type="hidden" name="studentInfoArr" value="<?=rawurlencode(serialize($studentInfoArr))?>">
		<input type="hidden" name="exportStudentViewClubArr" value="<?=rawurlencode(serialize($exportStudentViewClubArr))?>">
		<input type="hidden" name="exportStudentViewActivityArr" value="<?=rawurlencode(serialize($exportStudentViewActivityArr))?>">
		<input type="hidden" id="studentName" name="studentName" value="<?=$studentNameDisplay?>" />
		<input type="hidden" id="first_in" name="first_in" value="<?=$first_in?>" />
		<input type="hidden" id="exportGroupStudentAssoc" name="exportGroupStudentAssoc" value="<?=rawurlencode(serialize($GroupStudentAssoc))?>" />
		
	</form>
	
	<br>
	
	
<?


	//$libenroll->db_show_debug_log_by_query_number(1);
	intranet_closedb();
	if ($classView && $showTarget!="club") {
		//echo $linterface->FOCUS_ON_LOAD("form1.targetClassID");
		echo $linterface->FOCUS_ON_LOAD("getElementById('targetClassID')");
	}
    $linterface->LAYOUT_STOP();
}
else
{
	intranet_closedb();
	
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
	
}
?>