<?php
# using: 
###########################################
#   Date:   2019-09-04 Henry
#   Added Max number of club want and controlled by flag
#
#   Date:   2018-03-05 Anna
#   Added club title when target is club
#
############################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$lexport = new libexporttext();

$libenroll->hasAccessRight($_SESSION['UserID'], 'Club_Admin');


$showTarget = $_POST['showTarget'];		// 'class' or 'club'
$groupStudentAssoc = unserialize(rawurldecode($_POST['exportGroupStudentAssoc']));
$studentInfoAry = unserialize(rawurldecode($_POST['studentInfoArr']));

$headerAry = array();
$dataAry = array();
$exportStudentViewClubArr = array();
$exportStudentViewActivityArr = array();

if ($showTarget == 'club') {
    $headerAry[] = $Lang['eEnrolment']['ClubName'];
}
$headerAry[] = $i_UserClassName;
$headerAry[] = $i_UserClassNumber;
$headerAry[] = $i_UserStudentName;
if($sys_custom['eEnrolment']['DisplayMaxWantInEnrolmentSummarySimple2Export']){
	$headerAry[] = $eEnrollment['MaxNoOfClubWantToParticipate'];
}

$tempClubInfoAry = $libenroll->Get_All_Club_Info();
$clubInfoAssoAry = BuildMultiKeyAssoc($tempClubInfoAry, "EnrolGroupID");
unset($tempClubInfoAry);

if($sys_custom['eEnrolment']['DisplayMaxWantInEnrolmentSummarySimple2Export']){
	$sql = "SELECT StudentID, Max FROM INTRANET_ENROL_STUDENT";
	$enrolInfo = $libenroll->returnArray($sql);
	$enrolInfoAssoAry = BuildMultiKeyAssoc($enrolInfo, "StudentID");
}

$iCount = 0;
foreach((array)$groupStudentAssoc as $_classClubId => $_studentIdAry) {
	$_studentIdAry = array_values($_studentIdAry);
	$_numOfStudent = count($_studentIdAry);

	//loop through all students to get performance and construct table contents
	$__NumberOfClubArr = array();
	for ($i=0; $i<$_numOfStudent; $i++) {
			
		$__studentId = $_studentIdAry[$i];
		$__className = $studentInfoAry[$__studentId]['ClassName'];
		$__classNumber = $studentInfoAry[$__studentId]['ClassNumber'];

		$__studentName = strip_tags($studentInfoAry[$__studentId]['StudentName']);
		if($sys_custom['eEnrolment']['DisplayMaxWantInEnrolmentSummarySimple2Export']){
			$__maxWant = $enrolInfoAssoAry[$__studentId]['Max'];
		}
		$__clubInfoAry = $studentInfoAry[$__studentId]['ClubInfoArr'];
		$__activityInfoAry = $studentInfoAry[$__studentId]['ActivityInfoArr'];

		 
		$__clubTitleAry = Get_Array_By_Key($__clubInfoAry, 1);
		$__activityTitleAry = Get_Array_By_Key($__activityInfoAry, 1);

		$__NumberOfClub = sizeof($__clubInfoAry);
		$__NumberOfClubArr[] = $__NumberOfClub;
		$__NumberOfAct = sizeof($__activityInfoAry);
		$__NumberOfActArr[] = $__NumberOfAct;
		
	}
	$__MaxClub = (count($__NumberOfClubArr)>0)? max($__NumberOfClubArr) : 1;
	$__MaxClubNumber = ($__MaxClub>0)? $__MaxClub : 1 ;
	$__MaxClubNumberArr [] = $__MaxClubNumber;	
	
	
	$__MaxAct = (count($__NumberOfActArr)>0)? max($__NumberOfActArr) : 1;
	$__MaxActNumber = ($__MaxAct>0)? $__MaxAct : 1 ;
	$__MaxActNumberArr [] = $__MaxActNumber;
}

$__MaxNumberofClub = max($__MaxClubNumberArr);

foreach((array)$groupStudentAssoc as $_classClubId => $_studentIdAry) {
	$_studentIdAry = array_values($_studentIdAry);
	$_numOfStudent = count($_studentIdAry);

	$thisClubTitle = $clubInfoAssoAry[$_classClubId]['Title'];
	
	if(empty($_studentIdAry) && $showTarget == 'club'){
	    $dataAry[$iCount++][0] = $thisClubTitle;
	}
	for ($i=0; $i<$_numOfStudent; $i++) {

		$__studentId = $_studentIdAry[$i];
		$__className = $studentInfoAry[$__studentId]['ClassName'];
		$__classNumber = $studentInfoAry[$__studentId]['ClassNumber'];
			
		$__studentName = strip_tags($studentInfoAry[$__studentId]['StudentName']);
		if($sys_custom['eEnrolment']['DisplayMaxWantInEnrolmentSummarySimple2Export']){
			$__maxWant = $enrolInfoAssoAry[$__studentId]['Max'];
		}
		$__clubInfoAry = $studentInfoAry[$__studentId]['ClubInfoArr'];
		$__activityInfoAry = $studentInfoAry[$__studentId]['ActivityInfoArr'];

		$__clubTitleAry = Get_Array_By_Key($__clubInfoAry, 1);
		$__activityTitleAry = Get_Array_By_Key($__activityInfoAry, 1);
			
		$__NumberOfClub = sizeof($__clubTitleAry);
		$__NumberOfAct = sizeof($__activityTitleAry);
		
		$different = $__MaxNumberofClub - $__NumberOfClub;			
		$jCount = 0;
		if ($showTarget == 'club') {
		    $dataAry[$iCount][$jCount++] = $thisClubTitle;
		}

		$dataAry[$iCount][$jCount++] = $__className;
		$dataAry[$iCount][$jCount++] = $__classNumber;
		$dataAry[$iCount][$jCount++] = $__studentName;
		if($sys_custom['eEnrolment']['DisplayMaxWantInEnrolmentSummarySimple2Export']){
			$dataAry[$iCount][$jCount++] = $__maxWant;
		}


		for ($k=0; $k<$__NumberOfClub; $k++) {
			$dataAry[$iCount][$jCount++] = $__clubTitleAry[$k];
		}
		for($k=0;$k<$different;$k++){

			$dataAry[$iCount][$jCount++] = "";
		}
		for ($k=0; $k<$__NumberOfAct; $k++) {
			$dataAry[$iCount][$jCount++] = $__activityTitleAry[$k];
		}
		$iCount++;
	}
}


$__MaxClubNumberOfAllClub = max($__MaxClubNumberArr);
$__MaxActNumberOfAllClub = max($__MaxActNumberArr);
for($j=1;$j<=$__MaxClubNumberOfAllClub;$j++){
	$headerAry[] = $Lang['eEnrolment']['Club'].' '.$j;
}
for($j=1;$j<=$__MaxActNumberOfAllClub;$j++){
	$headerAry[] = $Lang['eEnrolment']['Activity'].' '.$j;
}

$export_text = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "enrolment_summary_simple2.csv";

intranet_closedb();
$lexport->EXPORT_FILE($filename,$export_text);
?>