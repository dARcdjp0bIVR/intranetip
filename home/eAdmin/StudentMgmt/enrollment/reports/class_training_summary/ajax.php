<?
//Using: 
/*
 * 	Log
 * 
 * 	Description: output json format data
 *
 *  2018-09-14 Cameron
 *      - allow staff to view report (by calling isHKPFValidateUser to check access right)
 *      
 *  2018-08-15 [Cameron]
 *      - create this file
 */

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_report.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->isHKPFValidateUser())
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

if ($junior_mck) {
	if (phpversion_compare('5.2') != 'ELDER') {
		$characterset = 'utf-8';
	}
	else {
		$characterset = 'big5';
	}
}
else {
	$characterset = 'utf-8';
}

header('Content-Type: text/html; charset='.$characterset);

$ljson = new JSON_obj();

$json['success'] = false;
$x = '';
$action = $_GET['action'] ? $_GET['action'] : $_POST['action'];
//$remove_dummy_chars = ($junior_mck) ? false : true;	// whether to remove new line, carriage return, tab and back slash

$linterface = new libclubsenrol_ui();
$libenroll_report = new libclubsenrol_report();

switch($action) {
    
    case 'getClassList':
        $intakeID = IntegerSafe($_POST['Intake']);
        $classAry = $libenroll->getIntakeClass($intakeID);
        if (count($classAry)) {
            foreach((array)$classAry as $_classID=>$_classAry) {
                $classIdxAry[] = array($_classID,$_classAry['ClassName']);
            }
        }
        $x = getSelectByArray($classIdxAry, "name='Class[]' id='Class' multiple='multiple' size='10' style='min-width:100px;'", $selected="", $all=0, $noFirst=1);
        
        $intakeList = $libenroll->getIntakeList($intakeID);
        if (count($intakeList)){
            $intakeDate = $intakeList[0]['StartDate']. ' ~ ' . $intakeList[0]['EndDate'];
        }
        else {
            $intakeDate = '';
        }
        $y = $intakeDate;
        
        $json['success'] = true;
        break;
        
    case 'getClassTrainingReport':
        $intakeID = $_POST['Intake'];
        $classAry = $_POST['Class'];
        
        $sortBy = $displayType == 'detail' ? 'date' : '';
        $x = $libenroll_report->getClassTrainingReport($intakeID, $classAry, $displayType, $sortBy);
        
        $json['success'] = true;
        break;
}

//if ($remove_dummy_chars) {
//	$x = remove_dummy_chars_for_json($x);
//}

if (($junior_mck) && (phpversion_compare('5.2') != 'ELDER')) {
	$x = convert2unicode($x,true,1);
	$y = convert2unicode($y,true,1);
}

$json['html'] = $x;
$json['html2'] = $y;
echo $ljson->encode($json);

intranet_closedb();
?>