<?php
// Editing by 
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ($plugin['eEnrollmentLite'] || ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (count($classInfo)==0) &&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) or !$plugin['eEnrollment']))
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$linterface = new interface_html();

# setting the current page
$CurrentPage = "PageClubRolesAndComments";
$CurrentPageArr['eEnrolment'] = 1;
$TAGS_OBJ[] = array($Lang['eEnrolment']['ClubRolesAndComments']['ClubRolesAndComments'], "", 1);
$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();

$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID(); 
# School year
$selectYear = ($selectYear == '') ? Get_Current_Academic_Year_ID() : $selectYear;
$years = $libenroll->returnAllYearsSelectionArray();
//$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='hideSpan(\"spanStudent\"); changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value); document.form1.rankTarget.selectedIndex=0; showResult(\"form\",\"\")'", "", $selectYear);
$selectSchoolYearHTML = $linterface->GET_SELECTION_BOX($years, "name='selectYear' id='selectYear' onclick='changeRadioSelection(\"YEAR\")' onChange='changeTerm(this.value);' ", "", $selectYear);

# Semester
$currentSemester = getCurrentSemester();
$selectSemesterHTML = "<select name=\"selectSemester\" id=\"selectSemester\" onclick=\"changeRadioSelection('YEAR')\">";
$selectSemesterHTML .= "</select>";

$record_types = array();
$record_types[] = array('',$Lang['Btn']['All']);
$record_types[] = array('1',$Lang['eEnrolment']['ClubRolesAndComments']['StudentsWithComment']);
$record_types[] = array('2',$Lang['eEnrolment']['ClubRolesAndComments']['StudentsWithoutComment']);
$record_type_selection = $linterface->GET_SELECTION_BOX($record_types, ' id="sel_record_type" name="sel_record_type" ','','');

$linterface->LAYOUT_START();

?>
<script type="text/javascript" language="JavaScript">
function changeTerm(val) {
	
	if (val.length==0)
	{ 
		document.getElementById("spanSemester").innerHTML = "";
		document.getElementById("spanSemester").style.border = "0px";
		return
	}
	
	$.get(
		'../../ajaxGetSemester.php?year='+val+'&term=<?=$selectSemester?>'+'&field=selectSemester',
		{},
		function(responseText){
			document.getElementById("spanSemester").innerHTML = responseText;
			document.getElementById("spanSemester").style.border = "0px solid #A5ACB2";
		}
	);
}

function changeRadioSelection(type)
{
	var jsYearRadioObj = document.getElementById("radioPeriod_Year");
	var jsDateRadioObj = document.getElementById("radioPeriod_Date");
	
	if (type == "YEAR")
	{
		jsYearRadioObj.checked = true;
		jsDateRadioObj.checked = false;
	}
	else
	{
		jsYearRadioObj.checked = false;
		jsDateRadioObj.checked = true;
	}
	
	showRankTargetResult($('#rankTarget').val(),'');
}

function showSpan(span) {
	document.getElementById(span).style.display="inline";
	if(span == 'spanStudent') {
		document.getElementById('selectAllBtn01').disabled = true;
		document.getElementById('selectAllBtn01').style.visibility = 'hidden';
		var temp = document.getElementById('studentID[]');
		
		while(temp.options.length>0) {
			temp.options[0] = null;	
		}
	} 
}

function hideSpan(span) {
	document.getElementById(span).style.display="none";
	if(span=='spanStudent') {
		document.getElementById('selectAllBtn01').disabled = false;
		document.getElementById('selectAllBtn01').style.visibility = 'visible';
	}
}

function showResult(str, choice)
{
	if(str != "student2ndLayer")
		document.getElementById("studentFlag").value = 0;
		
	if (str.length==0)
	{ 
		document.getElementById("rankTargetDetail").innerHTML = "";
		document.getElementById("rankTargetDetail").style.border = "0px";
		return
	}
	
	if(str == 'student2ndLayer'){
		choice = '';
		var options = $('select#rankTargetDetail\\[\\] option:selected');
		var delim = '';
		for(var i=0;i<options.length;i++){
			choice += delim + options.get(i).value;
			delim = ',';
		}
	}
	
	var url = "../../get_live.php";
	url = url + "?target=" + str + "&rankTargetDetail=" + choice;
	if($('input[name=radioPeriod]:checked').val()=='YEAR'){
		url += "&year="+$('#selectYear').val();
	}else{
		url += "&year=<?=$AcademicYearID?>";
	}
	//url += "&student=1&value="+document.getElementById('rankTargetDetail[]').value;
	url += "&student=1&value="+choice;
	<? if(!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) {?>
	url += "&ownClassOnly=1";
	<?}?>
	<? if($studentID != '') { 
		echo "url += \"&studentid=\" + ".implode(',',$studentID).";";
	} ?>
	
	$.get(
		url,
		{},
		function(responseText){
			var showIn = "";
			if(document.getElementById("studentFlag").value == 0) {
				showIn = "rankTargetDetail";
			} else {
				showIn = "spanStudent";	
			}
			
			document.getElementById(showIn).innerHTML = responseText;
			document.getElementById(showIn).style.border = "0px solid #A5ACB2";
		}
	);
}

function showRankTargetResult(val, choice) {
	showResult(val,choice);
	if (val=='student') {
		showSpan('spanStudent');
	} else {
		hideSpan('spanStudent');
	}
}

function studentSelection(val) {
/*
	var rank = document.getElementById('rankTargetDetail[]');
	for(var i=0; i<rank.options.length; i++) {
		if(rank.options[i].value == val) {
			rank.options[i].selected = true;	
		} else {
			rank.options[i].selected = false;	
		}
	}
*/
}

function hideOptionLayer()
{
	//$('.Form_Span').attr('style', 'display: none');
	$('.Form_Span').hide();
	$('.spanHideOption').hide();
	$('.spanShowOption').show();
	//$('.spanHideOption').attr('style', 'display: none');
	//$('.spanShowOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_hide_option';
}

function showOptionLayer()
{
	//$('.Form_Span').attr('style', '');
	$('.Form_Span').show();
	$('.spanHideOption').show();
	$('.spanShowOption').hide();
	//$('.spanShowOption').attr('style', 'display: none');
	//$('.spanHideOption').attr('style', '');
	document.getElementById('div_form').className = 'report_option report_show_option';
}

function SelectAll(obj){
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = true;
	}
}

function SelectAllItem(obj, flag) {
	for (i=0; i<obj.length; i++){
		obj.options[i].selected = flag;
	}
}

function submitForm(format)
{
	var valid = true;
	var form_obj = $('#form1');
	var radio_period = $('input[name="radioPeriod"]:checked').val();
	var rank_target = $('#rankTarget').val();
	
	if(radio_period == 'DATE'){
		var from_date = $.trim($('#textFromDate').val());
		var to_date = $.trim($('#textToDate').val());
		
		if(from_date == '' || to_date == ''){
			valid = false;
			$('#textFromDate').focus();
		}
		if(valid){
			if(!check_date_without_return_msg(document.getElementById('textFromDate'))){
				valid = false;
				$('#textFromDate').focus();
			}
			if(!check_date_without_return_msg(document.getElementById('textToDate'))){
				valid = false;
				$('#textToDate').focus();
			}
		}
		if(valid){
			if(from_date > to_date){
				valid = false;
				$('#DateWarnDiv span').html('<?=$Lang['General']['JS_warning']['InvalidDateRange']?>');
				$('#DateWarnDiv').show();
				$('#textFromDate').focus();
			}
		}
		
		if(valid){
			$('#DateWarnDiv span').html('');
			$('#DateWarnDiv').hide();
		}
	}
	
	if(rank_target == 'form' || rank_target == 'class'){
		if($('select#rankTargetDetail\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$iDiscipline['RankingTarget']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}else if(rank_target == 'student'){
		if($('select#studentID\\[\\] option:selected').length == 0){
			valid = false;
			$('#TargetWarnDiv span').html('<?=$i_alert_pleaseselect.$Lang['Btn']['Student']?>');
			$('#TargetWarnDiv').show();
		}else{
			$('#TargetWarnDiv').hide();
		}
	}
	
	if(!valid) return;
	$('input#format').val(format);
	if(format == 'web'){
		form_obj.attr('target','');
		Block_Element('PageDiv');
		
		$.post(
			'report.php',
			$('#form1').serialize(),
			function(data){
				$('#ReportDiv').html(data);
				document.getElementById('div_form').className = 'report_option report_hide_option';
				$('.spanShowOption').show();
				$('.spanHideOption').hide();
				$('.Form_Span').hide();
				UnBlock_Element('PageDiv');
			}
		);
	}else if(format == 'csv'){
		form_obj.attr('target','');
		form_obj.submit();
	}else if(format == 'print'){
		form_obj.attr('target','_blank');
		form_obj.submit();
	}
}

$(document).ready(function(){
	changeTerm($("#selectYear").val());
	showRankTargetResult($("#rankTarget").val(), '');
});
</script>

<div id="PageDiv">
<div id="div_form">
	<span id="spanShowOption" class="spanShowOption" style="display:none">
		<a href="javascript:showOptionLayer();"><?=$Lang['Btn']['ShowOption'] ?></a>
	</span>
	<span id="spanHideOption" class="spanHideOption" style="display:none">
		<a href="javascript:hideOptionLayer();"><?=$Lang['Btn']['HideOption']?></a>
	</span>
	
	<p class="spacer"></p> 
	<span class="Form_Span">
		<form id="form1" name="form1" method="post" action="report.php" onsubmit="submitForm('web');return false;">
		<table class="form_table_v30">
			<!-- Period -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"]?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td colspan="6" onClick="document.form1.radioPeriod_Year.checked=true;showRankTargetResult($('#rankTarget').val(),'');">
								<input name="radioPeriod" type="radio" id="radioPeriod_Year" value="YEAR" <? echo ($Period!="DATE")?" checked":"" ?> onclick="showRankTargetResult($('#rankTarget').val(),'');">
								<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"]?>
								<?=$selectSchoolYearHTML?>&nbsp;
								<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"]?>
								<span id="spanSemester"><?=$selectSemesterHTML?></span>
								
							</td>
						</tr>
						<tr>
							<td onClick="document.form1.radioPeriod_Date.checked=true;showRankTargetResult($('#rankTarget').val(),'');"><input name="radioPeriod" type="radio" id="radioPeriod_Date" value="DATE" <? echo ($Period=="DATE")?" checked":"" ?> onclick="showRankTargetResult($('#rankTarget').val(),'');"> <?=$i_From?> </td>
							<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textFromDate",$textFromDate,"")?>
							<br><span id='div_DateEnd_err_msg'></span></td>
							<td><?=$i_To?> </td>
							<td onClick="changeRadioSelection('DATE')" onFocus="changeRadioSelection('DATE')"><?=$linterface->GET_DATE_PICKER("textToDate",$textToDate,"")?>
								<?=$linterface->Get_Form_Warning_Msg("DateWarnDiv","")?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		
			<!-- Target -->
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"]?></td>
				<td>
					<table class="inside_form_table">
						<tr>
							<td valign="top">
								<select name="rankTarget" id="rankTarget" onChange="showRankTargetResult(this.value,'')">
									<option value="form" <? if($rankTarget=="form" || !isset($rankTarget) || $rankTarget=="") { echo "selected";} ?>><?=$i_Discipline_Form?></option>
									<option value="class" <? if($rankTarget=="class") { echo "selected";} ?>><?=$i_Discipline_Class?></option>
									<option value="student" <? if($rankTarget=="student") { echo "selected";} ?>><?=$i_UserStudentName?></option>
								</select>
							</td>
							<td valign="top" nowrap>
								<!-- Form / Class //-->
								<span id='rankTargetDetail'>
								<select name="rankTargetDetail[]" id="rankTargetDetail[]" size="7"></select>
								</span>
								<?= $linterface->GET_BTN($button_select_all, "button", "SelectAll(document.getElementById('rankTargetDetail[]'));return false;", "selectAllBtn01")?>
									
								<!-- Student //-->
								<span id='spanStudent' style='<? if($rankTarget=="student") { echo "display:inline;";} else { echo "display:none;";} ?>'>
									<select name="studentID[]" multiple size="7" id="studentID[]"></select> <?= $linterface->GET_BTN($button_select_all, "button", "SelectAllItem(document.getElementById('studentID[]'), true);return false;")?>
								</span>
								
							</td>
						</tr>
						<tr><td colspan="3" class="tabletextremark">(<?=$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"]?>)</td></tr>
					</table>
					<span id='div_Target_err_msg'></span>
					<?=$linterface->Get_Form_Warning_Msg("TargetWarnDiv","")?>
				</td>
			</tr>
			
			<tr valign="top">
				<td class="field_title"><span class="tabletextrequire">*</span><?=$Lang['eEnrolment']['record_type']?></td>
				<td><?=$record_type_selection?></td>
			</tr>
				
		</table>
		
		<?=$linterface->MandatoryField();?>
		
		<div class="edit_bottom_v30">
		<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($i_Discipline_Generate_Update_Statistic, "submit")?>
		<p class="spacer"></p>
		</div>
		
		<input type="hidden" name="format" id="format" value="web">
		<input type="hidden" name="studentFlag" id="studentFlag" value="0">
		</form>
	</span>
</div>
<div id="ReportDiv"></div>
</div>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
