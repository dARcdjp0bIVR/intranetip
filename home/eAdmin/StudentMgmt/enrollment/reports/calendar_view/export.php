<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_opendb();
$ldb = new libdb();
$libenroll = new libclubsenrol();
$lexport = new libexporttext();
if($_POST['timeString2'] != ''){
    // normal flow
    
    $date_condition = $_POST['timeString2'];
    $dateArr = explode('-',$date_condition);
    $year = $dateArr[0];
    $month = $dateArr[1];
    
    // str_pad to the month
    $date_condition = $year.'-'.str_pad($month,2,'0',STR_PAD_LEFT);
}
else{
    // print page
    $date_condition = $_POST['year'].'-'.str_pad($_POST['month'],2,'0',STR_PAD_LEFT);
    
    $isPrintMode = true;
}
$month_display = date('F Y',strtotime($date_condition.'-01'));
$groupDateArr = $libenroll->getMeetingDateOfTheMonth('Club',$date_condition);
$enrolGroupIDArr = array_unique(Get_Array_By_Key($groupDateArr,'EnrolGroupID'));
$groupInfoArr = BuildMultiKeyAssoc( $libenroll->Get_All_Club_Info($enrolGroupIDArr, $AcademicYearID='', $CategoryIDArr='', $Keyword='', $YearTermIDArr='', $OrderBy='', $OrderDesc='asc', $ApplyUserType='', $Round='', $GetLangDisplay = true) , 'EnrolGroupID' );
$groupDateAssocArr = array();
foreach((array)$groupDateArr as $_ary){
    $groupDateAssocArr[$_ary['Date']][] = $_ary;
}

// Get Event Meeting Date & Event Info
$eventDateArr = $libenroll->getMeetingDateOfTheMonth('Event',$date_condition);
$enrolEventIDArr = array_unique(Get_Array_By_Key($eventDateArr,'EnrolEventID'));
$enrolInfoArr = $libenroll->Get_Activity_Info_By_Id($enrolEventIDArr);

$eventDateAssocArr = array();
foreach((array)$eventDateArr as $_ary){
    $eventDateAssocArr[$_ary['Date']][] = $_ary;
}

// Start Prepare calendar
$first_of_month = mktime (0,0,0, $month, 1, $year);
$maxdays   = date('t', $first_of_month); # number of days in the month
$date_info = getdate($first_of_month);   # get info about the first day of the month
$month = $date_info['mon'];
$year = $date_info['year'];
$weekday = $date_info['wday']; #weekday (zero based) of the first day of the month

$groupDateAssocArr = array();
foreach((array)$groupDateArr as $_ary){
    $groupDateAssocArr[$_ary['Date']][] = $_ary;
}

// Get Event Meeting Date & Event Info
$eventDateArr = $libenroll->getMeetingDateOfTheMonth('Event',$date_condition);
$enrolEventIDArr = array_unique(Get_Array_By_Key($eventDateArr,'EnrolEventID'));
$enrolInfoArr = $libenroll->Get_Activity_Info_By_Id($enrolEventIDArr);

$eventDateAssocArr = array();
foreach((array)$eventDateArr as $_ary){
    $eventDateAssocArr[$_ary['Date']][] = $_ary;
}

// Start Prepare calendar
$first_of_month = mktime (0,0,0, $month, 1, $year);
$maxdays   = date('t', $first_of_month); # number of days in the month
$date_info = getdate($first_of_month);   # get info about the first day of the month
$month = $date_info['mon'];
$year = $date_info['year'];
$weekday = $date_info['wday']; #weekday (zero based) of the first day of the month



$dataAry = array();
$j=$weekday;
$i=0;    
for ($day=1; $day<=$maxdays; $day++) {
    
    $ts = mktime(0,0,0,$month,$day,$year);
    $academicYearAry = getAcademicYearAndYearTermByDate($year.'-'.$month.'-'.$day);
    $academicYearId = $academicYearAry['AcademicYearID'];
   
    $groupMeetingArr = (!empty($groupDateAssocArr[$day])) ? $groupDateAssocArr[$day] : array() ;
    $eventMeetingArr = (!empty($eventDateAssocArr[$day])) ? $eventDateAssocArr[$day] : array() ;
    // creating
    $dataAry[$i][$j] = $day;
    
    if(!empty($groupMeetingArr) || !empty($eventMeetingArr) ){
        // sorting
        sortByColumn2($groupMeetingArr,'startTime',0,0, 'endTime');
        sortByColumn2($eventMeetingArr,'startTime',0,0, 'endTime');
        
        // meeting dates table
        
        foreach((array)$groupMeetingArr as $_groupDateInfo){
            $_enrolGroupID = $_groupDateInfo['EnrolGroupID'];
            $_startTime = substr($_groupDateInfo['startTime'],0,5);
            $_endTime = substr($_groupDateInfo['endTime'],0,5);
            $_groupTitle = $groupInfoArr[$_enrolGroupID]['Title'];
            
            if ($isPrintMode) {
                $_groupTitleDisplay = $_groupTitle;
            }
            else {
                $_groupTitleDisplay = $_groupTitle;
            }
            $_display = $_startTime.' - '.$_endTime.' '.$_groupTitleDisplay;
            $dataAry[$i][$j] .="\n".$_display."\n";
        }
        foreach((array)$eventMeetingArr as $_eventDateInfo){
            $_enrolEventID = $_eventDateInfo['EnrolEventID'];
            $_startTime = substr($_eventDateInfo['startTime'],0,5);
            $_endTime = substr($_eventDateInfo['endTime'],0,5);
            $_eventTitle = $enrolInfoArr[$_enrolEventID]['EventTitle'];
            
            if ($isPrintMode) {
                $_eventTitleDisplay = $_eventTitle;
            }
            else {
                $_eventTitleDisplay = $_eventTitle;
            }
            
            $_display = $_startTime.' - '.$_endTime.' '.$_eventTitleDisplay;

            $dataAry[$i][$j] .= "\n".$_display."\n";
        }
        
        
        
    }
    //generate a new column when it meets Sunday
    if($j == 6) {
        $i++;
        $j=0;
    }else{
        $j++;
    }
}
// fill null to undefined index in the first column
for($a=0;$a<7;$a++){
    if($dataAry[0][$a]!=null){      
    }else{
        $dataAry[0][$a]=null;
    }
}

// build header array
$headerAry = array();
$headerAry[] = "Sun";
$headerAry[] = "Mon";
$headerAry[] = "Tue";
$headerAry[] = "Wed";
$headerAry[] = "Thu";
$headerAry[] = "Fri";
$headerAry[] = "Sat";


$exportContent = $lexport->GET_EXPORT_TXT($dataAry, $headerAry, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="11", $includeLineBreak=1);

$fileName = 'calendar_view.csv';
$lexport->EXPORT_FILE($fileName, $exportContent);
?>