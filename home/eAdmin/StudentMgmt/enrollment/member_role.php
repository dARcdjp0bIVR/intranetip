<?php
// using by 
/*
 * 	2016-12-20 (Carlos)
 *  $sys_custom['StudentAttendance']['SyncDataToPortfolio'] - sync student activity profile and portfolio.
 * 
 *  2015-10-07 (Omas)
 *  -Improved $sys_custom['eEnrolment']['ActivityAddRole'] , Get Role title from ID
*/
$PATH_WRT_ROOT = "../../../../";

include($PATH_WRT_ROOT."includes/global.php");
include($PATH_WRT_ROOT."includes/libdb.php");
include($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);
$UID = IntegerSafe($UID);
$RoleID = IntegerSafe($RoleID);

$libenroll = new libclubsenrol();
$li = new libdb();
if (is_array($UID) && sizeof($UID)!=0)
{
	if ($type=="activity")
	{
		if($sys_custom['eEnrolment']['ActivityAddRole']){
			// using RoleID to get the Role Title value
			$sql = "SELECT RoleID ,Title FROM INTRANET_ROLE WHERE RecordType = '".ENROLMENT_RECORDTYPE_IN_INTRANET_GROUP."'";
			$roleArr = BuildMultiKeyAssoc( $li->returnResultSet($sql) , 'RoleID', array('Title'),1);
			$roleID = $RoleTitle;
			$RoleTitle = $roleArr[$roleID];
		}
		$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET RoleTitle = '$RoleTitle', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' WHERE EnrolEventID = '$EnrolEventID' AND StudentID IN ('".implode("','", $UID)."')";
		$li->db_db_query($sql);
		
		if($sys_custom['eEnrolment']['SyncDataToPortfolio']){
			$libenroll->SyncProfileRecordsForActivity($AcademicYearID, $YearTermID, $EnrolEventID);
			$libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
		}
	}
	else
	{
		$sql = "UPDATE INTRANET_USERGROUP SET RoleID = '$RoleID', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' WHERE EnrolGroupID = '$EnrolGroupID' AND UserID IN ('".implode("','", $UID)."')";
	    $li->db_db_query($sql);
	    
	    if($sys_custom['eEnrolment']['SyncDataToPortfolio']){
			$libenroll->SyncProfileRecordsForClub($AcademicYearID, $YearTermID, $EnrolGroupID);
			$libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
		}
	}
}

intranet_closedb();
if ($type=="activity")
{
	header("Location: event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&msg=2&field=$field&order=$order&pageNo=$pageNo&page_size_change=$page_size_change&numPerPage=$numPerPage");
}
else
{
	header("Location: member_index.php?AcademicYearID=$AcademicYearID&EnrolGroupID=$EnrolGroupID&filter=$filter&msg=2");
}

?>