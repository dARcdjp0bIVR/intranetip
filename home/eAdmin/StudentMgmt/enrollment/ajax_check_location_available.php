<?php
// Editing by 

$PATH_WRT_ROOT = "../../../../"; 
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libinventory.php");
include_once($PATH_WRT_ROOT."includes/libebooking.php");
include_once($PATH_WRT_ROOT."includes/libebooking_ui.php");

intranet_opendb();

$linterface = new interface_html();
$lebooking_ui = new libebooking_ui();
$linventory = new libinventory();

$today = date("Y-m-d");
$datetime_count = count($TargetDatetimes);
$returnUnavailableOnly = $_REQUEST['returnUnavailableOnly'] == '1'; // if true only output unavailable rooms message

$arrDateAvailable = array();
$AvailableDateArr = array();
$arrNotAvailableDate = array();
$arrNotAvailableDateRemark = array();
for($j=0;$j<$datetime_count;$j++){
	
	$datetime_parts = explode(",",$TargetDatetimes[$j]);
	$targetDate = $datetime_parts[0];
	$StartTime = $datetime_parts[1];
	$EndTime = $datetime_parts[2];
	
	//if($targetDate < $today) continue;
	
	$display_time = $targetDate."&nbsp;".$StartTime."&nbsp;".$Lang['General']['To']."&nbsp;".$EndTime;
	
	if($lebooking_ui->checkUserCanBookTheFacilityWithTargetDate(LIBEBOOKING_FACILITY_TYPE_ROOM, $FacilityID, $_SESSION['UserID'], $targetDate))
	{
		### Available
				
		# get user pending booking records
		$curPendingRecordAry = $lebooking_ui->Get_All_Facility_Booking_Record($BookingID='', $BookingStatus=LIBEBOOKING_BOOKING_STATUS_PENDING, $targetDate, $targetDate, $ManagementGroup='', $keyword='', $CheckInOutRecord='', LIBEBOOKING_FACILITY_TYPE_ROOM, $ParUserID=$_SESSION['UserID'], $FacilityID);
		$numOfPeningRecord = count($curPendingRecordAry);
		$havePendingRecord = false;
		for ($i=0; $i<$numOfPeningRecord; $i++) {
			$_pendingRecordStartTime = $curPendingRecordAry[$i]['StartTime'];
			$_pendingRecordEndTime = $curPendingRecordAry[$i]['EndTime'];
			
			if ($_pendingRecordStartTime==$StartTime && $_pendingRecordEndTime==$EndTime) {
				$havePendingRecord = true;
				break;
			}
		}
		
		if ($havePendingRecord) {
			if(!$returnUnavailableOnly){
				$arrNotAvailableDate[] = $display_time;
				$arrNotAvailableDateRemark[$display_time] = $Lang['eBooking']['MakePendingBookingAlready'];
			} 
		}
		else if($lebooking_ui->Check_If_Facility_Is_Available(LIBEBOOKING_FACILITY_TYPE_ROOM,$FacilityID,$targetDate,$StartTime,$EndTime))
		{
			$tempArr[0]["StartTime"] = $StartTime;
			$tempArr[0]["EndTime"] = $EndTime;
			$arrDateAvailable[$targetDate][] = $tempArr[0];
			
			$AvailableDateArr[] = $display_time;
		}else{
			$arrNotAvailableDate[] = $display_time;
		}
		
	}else{
		### Not Available
		$arrNotAvailableDate[] = $display_time;
	}
}



if(sizeof($arrNotAvailableDate)>0)
{
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td>";
	$layer_content .= $Lang['eEnrolment']['RoomNotAvailableAtTheFollowingTimes'];
	$layer_content .= "</td></tr>";
	foreach($arrNotAvailableDate as $key=>$not_available_date)
	{
		$layer_content .= "<tr><td>";
		$layer_content .= $not_available_date;
		
		if (isset($arrNotAvailableDateRemark[$not_available_date])) {
			$layer_content .= ' ('.$arrNotAvailableDateRemark[$not_available_date].')';
		}
		
		$layer_content .= "</td></tr>";
	}
	$layer_content .= "</table>";
}
if(!$returnUnavailableOnly && sizeof($AvailableDateArr) > 0)
{
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td>";
	$layer_content .= $Lang['eEnrolment']['RoomAvailableAtTheFollowingTimes'];
	$layer_content .= "</td></tr>";
	foreach($AvailableDateArr as $key=>$available_date)
	{
		$layer_content .= "<tr><td>";
		$layer_content .= $available_date;
		$layer_content .= "</td></tr>";
	}
	$layer_content .= "</table>";
}
if(!$returnUnavailableOnly && sizeof($arrDateAvailable) > 0)
{
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td>";
	$layer_content .= $lebooking_ui->Get_New_Booking_Select_LocationItem_Table($FacilityID,$arrDateAvailable,'' ,'' ,'', 1);
	$layer_content .= "</td></tr>";
	$layer_content .= "</table>";
	
	$layer_content .= "<table border='0' width='100%'>";
	$layer_content .= "<tr><td align='center'>";
	$layer_content .= "<div class=\"edit_bottom_v30\">";
	//$layer_content .= $linterface->GET_SMALL_BTN($Lang['Btn']['Submit'], "button", "javascript:js_Submit_Booking();", "btnSubmit")."&nbsp;";
	//$layer_content .= $linterface->GET_SMALL_BTN($Lang['Btn']['Cancel'], "button", "window.top.tb_remove();", "btnCancel");
	$layer_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Submit'], "button", "js_Submit_Booking();","btnSubmit")."&nbsp;";
	$layer_content .= $linterface->GET_ACTION_BTN($Lang['Btn']['Cancel'], "button", "window.top.tb_remove();", "btnCancel");
	$layer_content .= "</div>";
	$layer_content .= "</td></tr>";
	$layer_content .= "</table>";
	
	$locationStr = $linventory->returnLocationStr($FacilityID);
	$layer_content .= '<input type="hidden" id="hiddenLocationName" name="hiddenLocationName" value="'.htmlspecialchars($locationStr,ENT_QUOTES).'">';
	
	
	### Layer to display Item Booking Details
	$layer_content .= '<div id="ItemBookingDetailsLayer" class="selectbox_layer" style="width:100%; height:200px; overflow:auto; display:none;">'."\n";
	$layer_content .= '<table cellspacing="0" cellpadding="3" border="0" width="95%">'."\n";
	$layer_content .= '<tbody>'."\n";
	### Hide Layer Button
	$layer_content .= '<tr>'."\n";
	$layer_content .= '<td align="right" style="border-bottom: medium none;">'."\n";
	$layer_content .= '<a href="javascript:js_Hide_Detail_Layer();"><img border="0" src="'.$PATH_WRT_ROOT.$image_path.'/'.$LAYOUT_SKIN.'/ecomm/btn_mini_off.gif"></a>'."\n";
	$layer_content .= '</td>'."\n";
	$layer_content .= '</tr>'."\n";
	### Layer Content
	$layer_content .= '<tr>'."\n";
	$layer_content .= '<td align="left" style="border-bottom: medium none;">'."\n";
	$layer_content .= '<div id="ItemBookingDetailsLayerContentDiv"></div>'."\n";
	$layer_content .= '</td>'."\n";
	$layer_content .= '</tr>'."\n";
	$layer_content .= '</tbody>'."\n";
	$layer_content .= '</table>'."\n";
	$layer_content .= '</div>'."\n";
	
}

echo $layer_content;

intranet_closedb();
?>