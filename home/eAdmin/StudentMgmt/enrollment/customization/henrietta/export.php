<?php
// Editing by
/*
 * 2019-01-23 Anna: Added yearidcond when target is student [#155858]
 * 2018-12-31 Anna: hardcode not show 1st term club records
 * 2018-05-14 Carlos: No records students are also displayed.
 * 2018-05-11 Carlos: Excluded eEnrolment activity events.
 * 2018-03-27 Carlos: Activity achievement is treated as a merit record.
 * 2018-03-02 Carlos: $sys_custom['eEnrolment']['HenriettaExportActivityAwardMeritData'] created.
 */
$PATH_WRT_ROOT = "../../../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

# user access right checking
$libenroll = new libclubsenrol();
$classInfo = $libenroll->getClassInfoByClassTeacherID($UserID);

if ($plugin['eEnrollmentLite'] || ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (count($classInfo)==0) &&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) or !$plugin['eEnrollment'] or !$sys_custom['eEnrolment']['HenriettaExportActivityAwardMeritData']))
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
    $laccessright = new libaccessright();
    $laccessright->NO_ACCESS_RIGHT_REDIRECT();
    exit;
}

$CmpField = 'ClassName';
$CmpField2 = 'ClassNumber';
function ___USortCmp($a,$b)
{
    global $CmpField, $CmpField2;
    /*
     if(!is_numeric($a[$CmpField])||!is_numeric($b[$CmpField])) {
     if ($CmpField2 != '' && $a[$CmpField] == $b[$CmpField]) {
     return strcasecmp($a[$CmpField2],$b[$CmpField2]);
     }
     else {
     return strcasecmp($a[$CmpField],$b[$CmpField]);
     }
     }
     */
    if ($a[$CmpField] == $b[$CmpField]) {
        if ($CmpField2 != '') {
            if ($a[$CmpField2] == $b[$CmpField2]) {
                $result =  0;
            }
            else {
                $result = ($a[$CmpField2] < $b[$CmpField2]) ? -1 : 1;
            }
        }
        else {
            $result =  0;
        }
    }
    else
    {
        $result = ($a[$CmpField] < $b[$CmpField]) ? -1 : 1;
    }
    return $result;
}

function removeSpace($str)
{
    //return str_replace("\xc2\xa0", '', trim($str));
    return str_replace(array("\r\n","\n"),' ',trim($str));
}

$ldis = new libdisciplinev12();
$lexport = new libexporttext();

$selectYear = $_POST['selectYear'];
$selectSemester = $_POST['selectSemester'];
$rankTarget = $_POST['rankTarget'];

$academic_year = new academic_year($selectYear);
$academic_year_name = $academic_year->YearNameEN;

$SQL_startdate = getStartDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);
$SQL_enddate = getEndDateOfAcademicYear($selectYear, $selectSemester=='0'?'':$selectSemester);

$YearTermIDArr = $selectSemester;

if($rankTarget == 'form'){
    $yearIdCond = " AND y.YearID IN ('".implode("','",(array)$_POST['rankTargetDetail'])."') ";
}else if($rankTarget == 'class'){
    $classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$_POST['rankTargetDetail'])."') ";
}else if($rankTarget == 'student'){
    $classIdCond = " AND yc.YearClassID IN ('".implode("','",(array)$_POST['rankTargetDetail'])."') ";
    $studentIdCond = " AND ycu.UserID IN ('".implode("','",(array)$_POST['studentID'])."')";
    // 	$yearIdCond = " AND y.YearID IN ('".implode("','",(array)$_POST['rankTargetDetail'])."') ";
}
$showClubRecord = true;
if ($YearTermIDArr != '')
{
    if (!is_array($YearTermIDArr)){
        $YearTermIDArr = array($YearTermIDArr);
    }
    
    if (in_array(0, $YearTermIDArr)){
        // 		$conds_YearTermIDArr = " And (ayt.YearTermID In ('".implode("','", $YearTermIDArr)."') Or ayt.YearTermID Is Null Or ayt.YearTermID = '') ";
    }else{
        $conds_YearTermIDArr = " And ayt.YearTermID In ('".implode("','", $YearTermIDArr)."') ";
    }
    
    
    $sql = "SELECT YearTermNameEN FROM ACADEMIC_YEAR_TERM WHERE YearTermID = '".$YearTermIDArr[0]."'";
    $YerTermName = $libenroll->returnVector($sql);
    
    if(in_array('1st Term',$YerTermName)){
        $showClubRecord = false;
    }
}

$AcademicYearNameField = Get_Lang_Selection('ay.YearNameB5','ay.YearNameEN');
$SemesterNameField = Get_Lang_Selection('ayt.YearTermNameB5' ,'ayt.YearTermNameEN');
$YearClassNameField = Get_Lang_Selection('yc.ClassTitleB5','yc.ClassTitleEN');
$StudentNameField = getNameFieldByLang("iu.");

$clsName = $YearClassNameField;

$sql = "SELECT
iu.UserID as StudentID,
$YearClassNameField as ClassName,
ycu.ClassNumber,
$StudentNameField as StudentName
FROM INTRANET_USER as iu
LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID
LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID
LEFT JOIN YEAR as y ON y.YearID=yc.YearID
WHERE iu.RecordType=2 AND yc.AcademicYearID='".$selectYear."' $yearIdCond $classIdCond $studentIdCond
ORDER BY ClassName,ycu.ClassNumber";
$student_records = $libenroll->returnResultSet($sql);
$student_records_size = count($student_records);


/*
 $sql = "SELECT
 ieei.EnrolEventID,
 ies.StudentID,
 $AcademicYearNameField as YearName,
 IF(ayt.YearTermID IS NULL,'".$Lang['eEnrolment']['WholeYear']."',$SemesterNameField) as Semester,
 $YearClassNameField as ClassName,
 ycu.ClassNumber,
 $StudentNameField as StudentName,
 IF(ies.RoleTitle IS NOT NULL AND ies.RoleTitle<>'',CONCAT(ieei.EventTitle,', ',ies.RoleTitle),ieei.EventTitle) as Detail,
 ies.Performance,
 ies.Achievement
 FROM INTRANET_ENROL_EVENTINFO as ieei
 INNER JOIN INTRANET_ENROL_EVENTSTUDENT as ies On (ieei.EnrolEventID = ies.EnrolEventID)
 INNER JOIN INTRANET_USER as iu On (ies.StudentID = iu.UserID And iu.RecordType = 2)
 INNER JOIN INTRANET_ENROL_EVENT_DATE as ieed ON ieed.EnrolEventID=ieei.EnrolEventID
 LEFT JOIN ACADEMIC_YEAR_TERM as ayt On (ieei.YearTermID = ayt.YearTermID)
 LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID=ieei.AcademicYearID
 LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID
 LEFT JOIN YEAR_CLASS as yc ON yc.YearClassID=ycu.YearClassID AND yc.AcademicYearID=ieei.AcademicYearID
 LEFT JOIN YEAR as y ON y.YearID=yc.YearID
 WHERE ieei.AcademicYearID = '".$selectYear."'
 $yearIdCond $classIdCond $studentIdCond $conds_YearTermIDArr
 AND ieed.ActivityDateStart>='$SQL_startdate 00:00:00' AND ieed.ActivityDateStart<='$SQL_enddate 23:59:59'
 GROUP BY ieei.EnrolEventID,ies.EventStudentID
 ORDER BY ClassName,ycu.ClassNumber ";
 $activity_records = $libenroll->returnResultSet($sql);
 $activity_records_size = count($activity_records);
 */

//debug_pr($sql);
//debug_pr($activity_records);
if($showClubRecord){
    $sql = "SELECT
    ug.UserID as StudentID,
    i.EnrolGroupID,
    g.GroupID,
    $AcademicYearNameField as YearName,
    IF(ayt.YearTermID IS NULL,'".$Lang['eEnrolment']['WholeYear']."',$SemesterNameField) as Semester,
    $YearClassNameField as ClassName,
    ycu.ClassNumber,
    $StudentNameField as StudentName,
    IF(r.Title IS NOT NULL AND r.Title<>'',CONCAT(g.Title,', ',r.Title),g.Title) as Detail,
    ug.Performance,
    ug.Achievement
    FROM INTRANET_USER as iu
    INNER JOIN INTRANET_USERGROUP as ug ON ug.UserID=iu.UserID
    INNER JOIN INTRANET_GROUP as g on ug.GroupID = g.GroupID and g.RecordType = 5
    INNER JOIN INTRANET_ENROL_GROUPINFO as i on g.GroupID = i.GroupID
    INNER join INTRANET_ENROL_GROUP_DATE as iegd on i.EnrolGroupID = iegd.EnrolGroupID and iegd.RecordStatus = 1
    LEFT JOIN INTRANET_ROLE as r ON r.RoleID=ug.RoleID
    LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID=g.AcademicYearID
    LEFT JOIN ACADEMIC_YEAR_TERM as ayt ON ay.AcademicYearID=ayt.AcademicYearID
    LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=ug.UserID
    LEFT JOIN YEAR_CLASS as yc ON yc.AcademicYearID=g.AcademicYearID AND yc.YearClassID=ycu.YearClassID
    LEFT JOIN YEAR as y ON y.YearID=yc.YearID
    WHERE iu.RecordType=2 AND g.AcademicYearID='".$selectYear."' $yearIdCond $classIdCond $studentIdCond
    AND iegd.ActivityDateStart>='$SQL_startdate 00:00:00' AND iegd.ActivityDateStart<='$SQL_enddate 23:59:59'
    GROUP BY i.EnrolGroupID,ug.UserID
    ORDER BY ClassName,ycu.ClassNumber ";
    $club_records = $libenroll->returnResultSet($sql);
    $club_records_size = count($club_records);
}
//debug_pr($sql);
//debug_pr($club_records);


$sql = "SELECT
psa.UserID as StudentID,
psa.StudentAwardID,
$AcademicYearNameField as YearName,
IF(ayt.YearTermID IS NULL,'".$Lang['eEnrolment']['WholeYear']."',$SemesterNameField) as Semester,
$YearClassNameField as ClassName,
ycu.ClassNumber,
$StudentNameField as StudentName,
IF(psa.Remark IS NOT NULL AND psa.Remark<>'',CONCAT(psa.AwardName,', ',psa.Remark),psa.AwardName) as Detail
FROM INTRANET_USER as iu
INNER JOIN PROFILE_STUDENT_AWARD as psa ON psa.UserID=iu.UserID
LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID='$selectYear' AND ay.YearNameEN LIKE CONCAT(psa.Year,'%')
LEFT JOIN ACADEMIC_YEAR_TERM as ayt ON ay.AcademicYearID=ayt.AcademicYearID AND (psa.Semester = ayt.YearTermNameEN OR psa.Semester = ayt.YearTermNameB5)
LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=psa.UserID
LEFT JOIN YEAR_CLASS as yc ON yc.AcademicYearID=ay.AcademicYearID AND yc.YearClassID=ycu.YearClassID
LEFT JOIN YEAR as y ON y.YearID=yc.YearID
WHERE iu.RecordType=2 AND ay.AcademicYearID='".$selectYear."' $yearIdCond $classIdCond $studentIdCond
$conds_YearTermIDArr

GROUP BY psa.StudentAwardID
ORDER BY ClassName,ycu.ClassNumber";
$award_records = $libenroll->returnResultSet($sql);
$award_records_size = count($award_records);

//	AND psa.DateInput >='$SQL_startdate 00:00:00' AND psa.DateInput <= '$SQL_enddate 23:59:59'

//debug_pr($sql);
//debug_pr($award_records);

$sql = "SELECT
dmr.StudentID,
dmr.RecordID,
$AcademicYearNameField as YearName,
IF(ayt.YearTermID IS NULL,'".$Lang['eEnrolment']['WholeYear']."',$SemesterNameField) as Semester,
$YearClassNameField as ClassName,
ycu.ClassNumber,
$StudentNameField as StudentName,
IF(dmr.Remark IS NOT NULL AND dmr.Remark <> '',CONCAT(dmr.ItemText,', ',dmr.Remark),dmr.ItemText) as Detail,
dmr.ProfileMeritType,
dmr.ProfileMeritCount
FROM INTRANET_USER as iu
INNER JOIN DISCIPLINE_MERIT_RECORD as dmr ON dmr.StudentID=iu.UserID
LEFT JOIN ACADEMIC_YEAR as ay ON ay.AcademicYearID=dmr.AcademicYearID
LEFT JOIN ACADEMIC_YEAR_TERM as ayt ON ay.AcademicYearID=ayt.AcademicYearID  AND (dmr.Semester = ayt.YearTermNameEN OR dmr.Semester = ayt.YearTermNameB5)
LEFT JOIN YEAR_CLASS_USER as ycu ON ycu.UserID=iu.UserID
LEFT JOIN YEAR_CLASS as yc ON yc.AcademicYearID=ay.AcademicYearID AND yc.YearClassID=ycu.YearClassID
LEFT JOIN YEAR as y ON y.YearID=yc.YearID
WHERE iu.RecordType=2 AND dmr.AcademicYearID='".$selectYear."' $yearIdCond $classIdCond $studentIdCond
AND dmr.MeritType='".GOOD_CONDUCT."' AND dmr.RecordDate >='$SQL_startdate 00:00:00' AND dmr.RecordDate <= '$SQL_enddate 23:59:59'
$conds_YearTermIDArr
GROUP BY dmr.RecordID
ORDER BY ClassName,ycu.ClassNumber ";
$merit_records = $ldis->returnResultSet($sql);
$merit_records_size = count($merit_records);

//debug_pr($sql);
//debug_pr($merit_records);

$record_type_map = array('ActivityRecords'=>$eEnrollment['WebSAMSSTA']['ECA'],'AwardRecords'=>$Lang['eEnrolment']['AwardMgmt']['Award'],'MeritRecords'=>$Lang['eDiscipline']['AddedMeritRecord']);

$uidToRecords = array();

for($i=0;$i<$student_records_size;$i++){
    $student_id = $student_records[$i]['StudentID'];
    $class_name = $student_records[$i]['ClassName'];
    $class_number = $student_records[$i]['ClassNumber'];
    $student_name = $student_records[$i]['StudentName'];
    
    $uidToRecords[$student_id] = array('StudentID'=>$student_id,'ClassName'=>$class_name,'ClassNumber'=>$class_number,'StudentName'=>$student_name);
}

/*
 for($i=0;$i<$activity_records_size;$i++){
 
 $student_id = $activity_records[$i]['StudentID'];
 $class_name = $activity_records[$i]['ClassName'];
 $class_number = $activity_records[$i]['ClassNumber'];
 $student_name = $activity_records[$i]['StudentName'];
 $detail = $activity_records[$i]['Detail'];
 $achievement = trim($activity_records[$i]['Achievement']);
 $performance = trim($activity_records[$i]['Performance']);
 if(!isset($uidToRecords[$student_id])){
 $uidToRecords[$student_id] = array('StudentID'=>$student_id,'ClassName'=>$class_name,'ClassNumber'=>$class_number,'StudentName'=>$student_name,'ActivityRecords'=>array(),'AwardRecords'=>array(),'MeritRecords'=>array());
 }
 
 if($achievement != ''){
 $uidToRecords[$student_id]['MeritRecords'][] = $detail.', '.$achievement;
 }else{
 $uidToRecords[$student_id]['ActivityRecords'][] = $detail.($performance!=''?', '.$performance:'');
 }
 }
 */
for($i=0;$i<$club_records_size;$i++){
    
    $student_id = $club_records[$i]['StudentID'];
    $class_name = $club_records[$i]['ClassName'];
    $class_number = $club_records[$i]['ClassNumber'];
    $student_name = $club_records[$i]['StudentName'];
    $detail = removeSpace($club_records[$i]['Detail']);
    $achievement = removeSpace($club_records[$i]['Achievement']);
    $performance = removeSpace($club_records[$i]['Performance']);
    if(!isset($uidToRecords[$student_id])){
        $uidToRecords[$student_id] = array('StudentID'=>$student_id,'ClassName'=>$class_name,'ClassNumber'=>$class_number,'StudentName'=>$student_name,'ActivityRecords'=>array(),'AwardRecords'=>array(),'MeritRecords'=>array());
    }
    
    if($achievement != ''){
        $uidToRecords[$student_id]['MeritRecords'][] = $detail.', '.$achievement;
    }else{
        $uidToRecords[$student_id]['ActivityRecords'][] = $detail.($performance!=''?', '.$performance:'');
    }
}

for($i=0;$i<$award_records_size;$i++){
    
    $student_id = $award_records[$i]['StudentID'];
    $class_name = $award_records[$i]['ClassName'];
    $class_number = $award_records[$i]['ClassNumber'];
    $student_name = $award_records[$i]['StudentName'];
    $detail = removeSpace($award_records[$i]['Detail']);
    if(!isset($uidToRecords[$student_id])){
        $uidToRecords[$student_id] = array('StudentID'=>$student_id,'ClassName'=>$class_name,'ClassNumber'=>$class_number,'StudentName'=>$student_name,'ActivityRecords'=>array(),'AwardRecords'=>array(),'MeritRecords'=>array());
    }
    
    $uidToRecords[$student_id]['AwardRecords'][] = $detail;
}

for($i=0;$i<$merit_records_size;$i++){
    
    $student_id = $merit_records[$i]['StudentID'];
    $class_name = $merit_records[$i]['ClassName'];
    $class_number = $merit_records[$i]['ClassNumber'];
    $student_name = $merit_records[$i]['StudentName'];
    $detail = str_replace(array("\r\n","\n"),'', removeSpace($merit_records[$i]['Detail']));
    $merit_type = $merit_records[$i]['ProfileMeritType'];
    $merit_name = '';
    switch($merit_type)
    {
        case 0:
            $merit_name = $i_Merit_Warning;
            break;
        case 1:
            $merit_name = $i_Merit_Merit;
            break;
        case 2:
            $merit_name = $i_Merit_MinorCredit;
            break;
        case 3:
            $merit_name = $i_Merit_MajorCredit;
            break;
        case 4:
            $merit_name = $i_Merit_SuperCredit;
            break;
        case 5:
            $merit_name = $i_Merit_UltraCredit;
            break;
        case -1:
            $merit_name = $i_Merit_BlackMark;
            break;
        case -2:
            $merit_name = $i_Merit_MinorDemerit;
            break;
        case -3:
            $merit_name = $i_Merit_MajorDemerit;
            break;
        case -4:
            $merit_name = $i_Merit_SuperDemerit;
            break;
        case -5:
            $merit_name = $i_Merit_UltraDemerit;
            break;
    }
    $merit_detail = $ldis->returnDeMeritStringWithNumber($merit_records[$i]['ProfileMeritCount'], $merit_name);
    if($merit_detail != '' && $merit_detail != '--'){
        $detail .= ', '.$merit_detail;
    }
    if(!isset($uidToRecords[$student_id])){
        $uidToRecords[$student_id] = array('StudentID'=>$student_id,'ClassName'=>$class_name,'ClassNumber'=>$class_number,'StudentName'=>$student_name,'ActivityRecords'=>array(),'AwardRecords'=>array(),'MeritRecords'=>array());
    }
    
    $uidToRecords[$student_id]['MeritRecords'][] = $detail;
}


//sortByColumn2($uidToRecords,'ClassName',$reverse=0,$sortWithIndex=0,'ClassNumber');
usort($uidToRecords,'___USortCmp');

//debug_pr($uidToRecords);
$rows = '';
$sep = "\t";
$nl = "\n";

if(count($uidToRecords)>0)
{
    foreach($uidToRecords as $uid => $ary){
        
        $class_name = $ary['ClassName'];
        $class_number = $ary['ClassNumber'];
        $student_name = $ary['StudentName'];
        $row = $class_name.$sep.$class_number.$sep.$student_name;
        //$row = $class_name.$sep.$class_number;
        foreach($record_type_map as $field_name => $display_name)
        {
            $size = count($ary[$field_name]);
            if($size > 0){
                $row .= $sep.$display_name;
                foreach($ary[$field_name] as $detail){
                    $row .= $sep.$detail;
                }
            }
        }
        $row.= $nl;
        
        $rows .= $row;
    }
}

intranet_closedb();

//echo $rows;
$lexport->EXPORT_FILE('activity_award_merit_records_'.$academic_year_name.'.csv', $rows);

?>