<?php
//Using: 
##################Change Log##########
##  2020-01-20 Tommy
##  added 'Performance Adjusted Reason'
##
##  2018-05-11 Anna
##  DELETE  $li ->field_array  #139432
##
##	2016-03-22	Kenneth
##	- bug fix: add back attendance and role
######################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


if(!$plugin['eEnrollment'] && !$_SESSION["SSV_USER_ACCESS"]["eAdmin-eEnrolment"]  && (!$libenroll->IS_CLUB_PIC()))
{
	echo "You have no priviledge to access this page.";
	exit;
}

include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($_REQUEST['EnrolGroupID']);

$libenroll = new libclubsenrol();	
$libenroll_ui = new libclubsenrol_ui();
$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);

if ($field=="" || !isset($field))
{
	$field = 0;
}
if ($order=="" || !isset($order))
{
	$order = 1;
}
if($filter=="" || !isset($filter)){
	$filter = 2;
}

$filter_array = array (
			   array (3,$i_identity_parent),
               array (2,$i_identity_student),
               array (1,$i_identity_teachstaff),
               );
               
if($UID_list!="")
{
	$UID = explode(",", $UID_list);
}

if ($plugin['eEnrollment'])
{
	if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)))
		header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $ligroup = new libgroup($GroupID);
        
        # navigation
        $title_display = $intranet_session_language=="en" ? $ligroup->Title : $ligroup->TitleChinese;
        
        # Construc information above the table
        $quotaDisplay = "<table cellspacing=\"0\" cellpadding=\"4\" border=\"0\" width=\"100%\">";
		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\" width=\"30%\">";
		$quotaDisplay .= "$i_ClubsEnrollment_GroupName </td><td>".$title_display."</td></tr>";

// 		$groupQuota = $libenroll->getQuota($EnrolGroupID);
// 		$ClubInfoArr = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr=array(2), Get_Current_Academic_Year_ID());
// 		$memberCount = count($ClubInfoArr);
		
		//$memberCount = $libenroll->GET_NUMBER_OF_MEMBER($EnrolGroupID, "club");
// 		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\" width=\"30%\">$i_ClubsEnrollment_GroupQuota1 </td><td>$groupQuota</td></tr>";
// 		$quotaDisplay .= "<tr class=\"tabletext\"><td class=\"formfieldtitle\" width=\"30%\">$i_ClubsEnrollment_CurrentSize </td><td>$memberCount</td></tr>";
		$quotaDisplay .= "</table>";
		
		//do not search for the initial textbox text
		if ($keyword == $eEnrollment['enter_name']) $keyword = "";
        
		if ($filter == 1 || $filter == 2 || $filter == 3){
			$conds = "a.RecordType = $filter";
		}
		else{
			$conds = "a.RecordType = 2";
		}
       	
// 		if ($filter == 2)
// 		{	
// 			$li->field_array = array("iu.ClassName", "iu.ClassNumber", "StudentName", "ir.Title", "Performance", "Comment", "ActiveMemberStatus", "iu.UserID");
// 			$li->fieldorder2 = ", iu.ClassNumber";
// 		}
// 		else
// 		{
// 			$li->field_array = array("StudentName", "iegs.StaffType", "iu.UserID");
// 			$li->fieldorder2 = ", iu.ClassNumber";
// 		}
		
		$display = "<div class='table_board'>";
		$display .="<table class='common_table_list_v30 view_table_list_v30'>";
		if ($filter == 2)
		{
			$display .= "<tr>";
			$display .= "<th>#</th>";
			$display .= "<th>". $i_general_class ."</th>";
			$display .= "<th>". $i_ClassNumber ."</th>";
			$display .= "<th>". $i_UserStudentName ."</th>";
			$display .= "<th>". $i_admintitle_role ."</th>";
			$display .= "<th>". $eEnrollment['attendence'] ."</th>";
			$display .= "<th>". $eEnrollment['performance'] ."</th>";
			if($sys_custom['eEnrolment']['Refresh_Performance'])
			    $display .= "<th>". $Lang['eEnrolment']['Refresh_Performance']['AdjustedReason'] ."</th>";
			$display .= "<th>". $Lang['eEnrolment']['Achievement'] ."</th>";
			$display .= "<th>". $eEnrollment['comment'] ."</th>";
			$display .= "<th>". $eEnrollment['active']."/".$eEnrollment['inactive'] ."</th>";
			$display .= "</tr>";
		}
		else
		{
			$display .= "<tr>";
			$display .= "<th>#</th>";
			$display .= "<th>". $i_UserName ."</th>";
			$display .= "<th>". $i_admintitle_role ."</th>";
			$display .= "</tr>";
		}
		$sql = $libenroll->Get_Management_Club_Member_Sql($GroupID, $EnrolGroupID, $filter, $keyword, $ForExport=1, $AcademicYearID);
		if ($filter == 2)
		{	
			$sql .= " order by iu.ClassName, iu.ClassNumber";
		}
		else
		{
			$sql .= " order by StudentName, iu.ClassNumber";
		}
		
		$result = $libenroll->returnArray($sql);
		
		if(empty($result))
		{
			$display .= '<tr>';
			$display .= '<td valign="top" colspan="9" align="center">'. $Lang['General']['NoRecordAtThisMoment'] .'</td>';
			$display .= '</tr>';	
		}
		for($i=0;$i<sizeof($result);$i++)
		{
			$display .= '<tr>';
			$display .= '<td valign="top">'.($i+1).'</td>';
			
			if ($filter == 2)
			{
				//list($thisClassName, $thisClassNumber, $thisStudentName, $thisTitle, $thisPerformance, $thisComment, $thisActiveMemberStatus, $thisUserID) = $result[$i];
				
				$thisClassName = $result[$i]['ClassName'];
				$thisClassNumber = $result[$i]['ClassNumber'];
				$thisStudentName = $result[$i]['StudentName'];
				$thisTitle = $result[$i]['Role'];
				$thisPerformance = $result[$i]['Performance'];
				$thisAdjustReason = $result[$i]['AdjustReason'];
				$thisAchievement = nl2br($result[$i]['Achievement']);
				$thisComment = nl2br($result[$i]['Comment']);
				$thisActiveMemberStatus = $result[$i]['ActiveMemberStatus'];
				$thisUserID = $result[$i]['UserID'];
				
				
				$DataArr['StudentID'] = $thisUserID;
				$DataArr['EnrolGroupID'] = $EnrolGroupID;
				$this_attendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr);
				
				$display .= '<td valign="top">'.$thisClassName.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisClassNumber.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisStudentName.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisTitle.'&nbsp;</td>';
				$display .= '<td valign="top">'.$this_attendance.'%&nbsp;</td>';
				$display .= '<td valign="top">'.$thisPerformance.'&nbsp;</td>';
				if($sys_custom['eEnrolment']['Refresh_Performance']){
				    if($thisAdjustReason == '--')
				        $display .= '<td valign="top">&nbsp;</td>';
				    else
				        $display .= '<td valign="top">'.$thisAdjustReason.'&nbsp;</td>';
				}
				$display .= '<td valign="top">'.$thisAchievement.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisComment.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisActiveMemberStatus.'&nbsp;</td>';
			}
			else
			{
				list($thisName, $thisTitle) = $result[$i];
				$display .= '<td valign="top">'.$thisName.'&nbsp;</td>';
				$display .= '<td valign="top">'.$thisTitle.'&nbsp;</td>';
			}
			
			$display .= '</tr>';	
		}
		
		
		$display .= '</table>';
		$display .= "</div>"; 	

		$RemarksTable = $libenroll_ui->Get_Student_Role_And_Status_Remarks(1,1);
		
		?>
		
		<div class="table_board">
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
				<tr><td><?=$quotaDisplay?></td></tr>
			</table>
			<br />
				<?=$display?>
				<?=$RemarksTable?>
				
		</div>

	<?
		intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>