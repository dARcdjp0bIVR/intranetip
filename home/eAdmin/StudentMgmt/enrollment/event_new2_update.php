<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && (!$libenroll->IS_CLUB_PIC())
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

# need not delete
//$libenroll->DEL_EVENTSTUDENT($EnrolEventID);

$EventArr['EnrolEventID'] = $EnrolEventID;

# need not delete, inactivate instead
//$libenroll->DEL_ENROL_EVENT_DATE($EventArr['EnrolEventID']);
$libenroll->INACTIVATE_ALL_ENROL_EVENT_DATE($EventArr['EnrolEventID']);

for ($i = 0; $i < sizeof($tableDates); $i++) {
	$EventArr['ActivityDateStart'] = $tableDates[$i]." ".$StartHour[$i].":".$StartMin[$i].":00";
	$EventArr['ActivityDateEnd'] = $tableDates[$i]." ".$EndHour[$i].":".$EndMin[$i].":00";
	
	$sql = "SELECT EventDateID FROM INTRANET_ENROL_EVENT_DATE 
			WHERE EnrolEventID = '".$EventArr['EnrolEventID']."'
					AND ActivityDateStart = '".$EventArr['ActivityDateStart']."' 
					AND ActivityDateEnd = '".$EventArr['ActivityDateEnd']."'";
	$result = $libenroll->returnArray($sql,1);

	if ($result == NULL)
	{
		$libenroll->ADD_ENROL_EVENT_DATE($EventArr);
	}
	else
	{
		$libenroll->ACTIVATE_ENROL_EVENT_DATE($result[0][0]);
	}
}

intranet_closedb();
//$link = "event_new3.php?EnrolEventID=$EnrolEventID&isNew=$isNew";
//if ($page_link != "") $link = $page_link;
//header("Location: $link");
$link = "event.php?msg=2";
//if ($page_link != "") $link = $page_link;
header("Location: $link");

?>