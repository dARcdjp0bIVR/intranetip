<?php
#Modify : 
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);	

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
	if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
		 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
		 && (!$libenroll->IS_CLUB_PIC())
		 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
		)
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        //$TAGS_OBJ[] = array($eEnrollmentMenu['mgt_event'], "", 1);
        # tags 
        $tab_type = "activity";
        $current_tab = 1;
        include_once("management_tabs.php");
        # navigation
        if ($isNew == 1)
        {
	        $PAGE_NAVIGATION[] = array($eEnrollment['new_activity'], "");
        }
        else
        {
	        $PAGE_NAVIGATION[] = array($eEnrollment['edit_activity'], "");
        }
        
        $STEPS_OBJ[] = array($eEnrollment['add_activity']['step_1'], 0);
        $STEPS_OBJ[] = array($eEnrollment['add_activity']['step_1b'], 1);
		$STEPS_OBJ[] = array($eEnrollment['add_activity']['step_2'], 0);
		$STEPS_OBJ[] = array($eEnrollment['add_activity']['step_3'], 0);
        
        $linterface->LAYOUT_START();

        
$EnrollEventArr = $lc->GET_EVENTINFO($EnrolEventID);
$AppStart = $libenroll->Event_AppStart;
$AppStartHour = $libenroll->Event_AppStartHour;
$AppStartMin = $libenroll->Event_AppStartMin;
$AppEnd = $libenroll->Event_AppEnd;
$AppEndHour = $libenroll->Event_AppEndHour;
$AppEndMin = $libenroll->Event_AppEndMin;
//debug_r($EnrollEventArr);

($EnrollEventArr[16] == 0) ? $chkRan = "selected" : $chkTime = "selected" ;

$tiebreakerSelection = "<SELECT name=tiebreak>\n";
$tiebreakerSelection.= "<OPTION value=0 $chkRan>$i_ClubsEnrollment_Random</OPTION>\n";
$tiebreakerSelection.= "<OPTION value=1 $chkTime>$i_ClubsEnrollment_AppTime</OPTION>\n";
$tiebreakerSelection.= "</SELECT>\n";


# Last selection - PIC
###############################################################################################
if ($isNew)
	$picArr = $lc->GET_EVENTSTAFF_FROM_CLUB($EnrollEventArr['EnrolEventID'], "PIC");
else
	$picArr = $lc->GET_EVENTSTAFF($EnrollEventArr['EnrolEventID'], "PIC");
	
$PICSel = "<select name='pic[]' id='pic[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($picArr); $i++) {
	$PICSel .= "<option value=\"".$picArr[$i][0]."\">".$LibUser->getNameWithClassNumber($picArr[$i][0])."</option>";
}
$PICSel .= "</select>";

//if (sizeof($picArr)>0)
//{
	$button_remove_html = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['pic[]'])");
//}
###############################################################################################


# Last selection - helper
###############################################################################################
if ($isNew)
	$helperArr = $lc->GET_EVENTSTAFF_FROM_CLUB($EnrollEventArr['EnrolEventID'], "HELPER");
else
	$helperArr = $lc->GET_EVENTSTAFF($EnrollEventArr['EnrolEventID'], "HELPER");

$HELPERSel = "<select name='helper[]' id='helper[]' size='6' multiple='multiple'>";
for($i = 0; $i < sizeof($helperArr); $i++) {
	$HELPERSel .= "<option value=\"".$helperArr[$i][0]."\">".$LibUser->getNameWithClassNumber($helperArr[$i][0])."</option>";
}
$HELPERSel .= "</select>";

//if (sizeof($helperArr)>0)
//{
	$button_remove_html_helper = $linterface->GET_SMALL_BTN($button_remove, "button", "javascript:checkOptionRemove(document.form1.elements['helper[]'])");
//}
###############################################################################################

$lclass = new libclass();
$ClassLvlArr = $lclass->getLevelArray();
$GroupArr = $lc->GET_EVENTCLASSLEVEL($EnrollEventArr[0]);
$ClassLvlChk = "<input type=\"checkbox\" id=\"classlvlall\" name=\"classlvl\" onClick=\"(this.checked) ? setChecked(1, document.form1, 'classlvl[]') : setChecked(0, document.form1, 'classlvl[]');\"><label for=\"classlvlall\">".$eEnrollment['all']."</label>";
$ClassLvlChk .= "<table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">";
for ($i = 0; $i < sizeof($ClassLvlArr); $i++) {
	$checked = "";
	for ($j = 0; $j < sizeof($GroupArr); $j++) {	
		if (($ClassLvlArr[$i][0] == $GroupArr[$j][0])&&(in_array($GroupArr[$j], $ClassLvlArr))) {	
			 $checked = " checked ";
			 break;
		}
	}
	
	if (($i % 10) == 0) $ClassLvlChk .= "<tr>";
	$ClassLvlChk .= "<td class=\"tabletext\"><input type=\"checkbox\" $checked id=\"classlvl{$i}\" name=\"classlvl[]\" value=\"".$ClassLvlArr[$i][0]."\"><label for=\"classlvl{$i}\">".$ClassLvlArr[$i][1]."</label></td>";
	if (($i % 10) == 9) $ClassLvlChk .= "</tr>";
}
$ClassLvlChk .= "</table>";

if ($EnrollEventArr[6] != 0) $LowerAge = $EnrollEventArr[6];
$LowerAgeSel = "<select name=\"LowerAge\" id=\"LowerAge\">";
for ($i = 11; $i < 21; $i++) {
	$LowerAgeSel .= "<option value=\"$i\"";
	if ($LowerAge == $i) $LowerAgeSel .= " selected ";
	$LowerAgeSel .= ">".$i."</option>";
}
$LowerAgeSel .= "</select>";

if ($EnrollEventArr[5] != 0) $UpperAge = $EnrollEventArr[5];
$UpperAgeSel = "<select name=\"UpperAge\" id=\"UpperAge\">";
for ($i = 11; $i < 21; $i++) {
	$UpperAgeSel .= "<option value=\"$i\"";
	if (($UpperAge == $i)||(($i == 20)&&(!$UpperAgeSelected))) {
		$UpperAgeSelected = true;
		$UpperAgeSel .= " selected ";
	}
	$UpperAgeSel .= ">".$i."</option>";
}
$UpperAgeSel .= "</select>";


$filecount = 0;
for ($i = 10; $i < 15; $i++) {
	if ($EnrollEventArr[$i] != "") $filecount++;
}

//debug_r($_SESSION);

$groups = $lc->getGroupsForEnrolSet();
$quotas = $lc->getGroupQuota();
for ($i=0; $i<sizeof($groups); $i++)
{
     $id = $groups[$i][0];
     if ($quotas[$id]!="")
     {
         $chkAllow[$id] = true;
     }
}

$GroupInfoArr = $lc->getGroupInfoList();
$GroupSel = "<select id=\"GroupID\" name=\"GroupID\">";
$GroupSel .= "<option value=\"\">".$i_notapplicable."</option>";
for ($i = 0; $i < sizeof($GroupInfoArr); $i++) {
	$id = $GroupInfoArr[$i][0];
	if ($chkAllow[$id]) {
		($EnrollEventArr[20] == $id) ? $selected = "selected" : $selected = "";
		$GroupSel .= "<option value=\"".$id."\" $selected>".$GroupInfoArr[$i][1]."</option>";	
	}
}
$GroupSel .= "</select>";


### Student cannot remove teacher staff - add js array to check
echo "\n\n<script language='JavaScript'>\n";
echo "var jsStaffIDArr = new Array();\n";
if ($LibUser->isStudent())
{
	$StaffIDArr = $lc->Get_Staff_List();
	
	foreach($StaffIDArr as $key => $StaffUserID)
	{
		echo "jsStaffIDArr[$key] = $StaffUserID;\n";
	}
}
echo "</script>\n";

?>
<script language="javascript">
function generalFormSubmitCheck(obj)
{
         checkOptionAll(obj.elements["pic[]"]);
         checkOptionAll(obj.elements["helper[]"]);
         //obj.submit();
}

function FormSubmitCheck(obj)
{
	var jsPICArr = document.getElementById('pic[]');
	var jsNumOfPIC = jsPICArr.length;
	
	if (jsNumOfPIC == 0) {
		alert('<?= $eEnrollment['js_sel_pic']?>');
		return false;
	}
	
	var jsHelperArr = document.getElementById('helper[]');
	var jsNumOfHelper = jsHelperArr.length;
	
	// Check if PIC and helper duplicated
	if (jsNumOfPIC > 0 && jsNumOfHelper > 0)
	{
		var i;
		var j;
		
		for (i=0; i<jsNumOfHelper; i++)
		{
			for (j=0; j<jsNumOfPIC; j++)
			{
				if (jsHelperArr[i].value == jsPICArr[j].value)
				{
					alert("<?=$Lang['eEnrolment']['jsWarning']['DuplicatedPersonInPicAndHelper']?>");
					return false;
				}
			}
		}
	}
	
	if (!check_positive_int(obj.Quota,"<?=$ec_warning['please_enter_pos_integer']?>"))
		return false;
		
	if (obj.ApplyStartTime.value != '')
	{
		if (!check_date(obj.ApplyStartTime,"<?php echo $i_invalid_date; ?>")) return false;
	}
	if (obj.ApplyEndTime.value != '')
	{
		if (!check_date(obj.ApplyEndTime,"<?php echo $i_invalid_date; ?>")) return false;
	}

	if (obj.ApplyStartTime.value > obj.ApplyEndTime.value)
	{
		alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
		return false;
	}
	else if (obj.ApplyStartTime.value == obj.ApplyEndTime.value)
	{
		// check hours
		if (obj.ApplyStartHour.value > obj.ApplyEndHour.value)
		{
			alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
			return false;
		}
		else if (obj.ApplyStartHour.value == obj.ApplyEndHour.value)
		{
			// check minutes
			if (obj.ApplyStartMin.value > obj.ApplyEndMin.value)
			{
				alert("<?=$i_ClubsEnrollment_AppStartAppEnd?>");
				return false;
			}
		}
	}
	
	if(!check_text(obj.Quota, "<?php echo $i_alert_pleasefillin.$eEnrollment['add_activity']['act_quota']; ?>.")) return false;
	generalFormSubmitCheck(obj);
	obj.submit();
}

Array.prototype.in_array = function(p_val) {
	for(var i = 0, l = this.length; i < l; i++) {
		if(this[i] == p_val) {
			return true;
		}
	}
	return false;
}


function checkOptionRemove(obj){
	checkOption(obj);
	i = obj.selectedIndex;
	while(i!=-1){
		if (jsStaffIDArr.in_array(obj.options[i].value))
		{
			alert("<?=$Lang['eEnrolment']['jsWarning']['StudentCannotRemoveTeacher']?>");
			break;
		}
		
		if (obj.options[i].value == <?= $_SESSION['UserID'] ?>)
		{
			if (!confirm("<?=$Lang['eEnrolment']['jsWarning']['RemoveMyselfWarning']?>"))
				break;
		}
			
		obj.options[i] = null;
		i = obj.selectedIndex;
	}
}
</SCRIPT>
<form name="form1" action="event_new1b_update.php" method="POST" enctype="multipart/form-data">
<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
	<td><?= $linterface->GET_STEPS($STEPS_OBJ) ?><br/>
	</td>
</tr>
<tr><td>
<table id="html_body_frame" width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_pic']?> <span class="tabletextrequire">*</span></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $PICSel?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('choose/index_all.php?fieldname=pic[]&ppl_type=pic', 9)")?><br />
				&nbsp;<?=$button_remove_html?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['act_assistant']?></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td><?= $HELPERSel?></td>
				<td valign="bottom">
				&nbsp;<?= $linterface->GET_SMALL_BTN($button_select, "button", "javascript:newWindow('choose/index_all.php?fieldname=helper[]&ppl_type=helper', 9)")?><br />
				&nbsp;<?=$button_remove_html_helper?>
				</td>
			</tr>
			</table>
		</td>
	</tr>	
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['app_method']?></td>
		<td><?= $tiebreakerSelection?></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['participant_quota']?> <span class="tabletextrequire">*</span></td>
		<td><input type="text" id="Quota" name="Quota" value="<?= $EnrollEventArr[1]?>" class="textboxnum"></td>
	</tr>
	<tr>
		<td valign="top" nowrap="nowrap" class="formfieldtitle tabletext"><?= $eEnrollment['add_activity']['app_period']?> <span class="tabletextrequire">*</span></td>
		<td class="tabletext">
		
			<?
				if ($EnrollEventArr[13] == "0000-00-00 00:00:00") $EnrollEventArr[13] = "{$AppStart} {$AppStartHour}:{$AppStartMin}:00";
				if ($EnrollEventArr[14] == "0000-00-00 00:00:00") $EnrollEventArr[14] = "{$AppEnd} {$AppEndHour}:{$AppEndMin}:00";
			?>
			<?=$linterface->GET_DATE_PICKER("ApplyStartTime", date("Y-m-d", strtotime($EnrollEventArr[13])))?>
			<!-- <input type="text" id="ApplyStartTime" name="ApplyStartTime" value="<?= date("Y-m-d", strtotime($EnrollEventArr[13]))?>" class="textboxnum"> <?= $linterface->GET_CALENDAR("form1", "ApplyStartTime")?> -->
			<?= getTimeSel("ApplyStart", date("H", strtotime($EnrollEventArr[13])), date("i", strtotime($EnrollEventArr[13]))) ?> 
			&nbsp;<?= $eEnrollment['to']?>&nbsp;
			<?=$linterface->GET_DATE_PICKER("ApplyEndTime", date("Y-m-d", strtotime($EnrollEventArr[14])))?>
			<!-- <input type="text" id="ApplyEndTime" name="ApplyEndTime" value="<?= date("Y-m-d", strtotime($EnrollEventArr[14]))?>" class="textboxnum"> <?= $linterface->GET_CALENDAR("form1", "ApplyEndTime")?> -->
			<?= getTimeSel("ApplyEnd", date("H", strtotime($EnrollEventArr[14])), date("i", strtotime($EnrollEventArr[14]))) ?> 
		</td>
	</tr>
</table>

</td></tr>

<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="left" class="tabletextremark">&nbsp; &nbsp; &nbsp; <?=$i_general_required_field?></td></tr>
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<!--
<?= $linterface->GET_ACTION_BTN($button_save_continue, "submit", "javascript: generalFormSubmitCheck(document.form1);")?>&nbsp;
-->
<?= $linterface->GET_ACTION_BTN($button_save_continue, "button", "javascript: FormSubmitCheck(document.form1);")?>&nbsp;
<? if (sizeof($picArr) > 0) print $linterface->GET_ACTION_BTN($eEnrollment['skip'], "button", "self.location='event_new2.php?EnrolEventID=$EnrolEventID'")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_back, "button", "self.location='event_new1a.php?EnrolEventID=$EnrolEventID'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>

</table>
<input type="hidden" name="flag" value="0" />
<input type="hidden" name="EnrolEventID" id="EnrolEventID" value="<?= $EnrolEventID?>" />
<input type="hidden" name="FileToDel" id="FileToDel" value="" />
<input type="hidden" name="isNew" id="isNew" value="<?= $isNew?>" />

</form>

    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>