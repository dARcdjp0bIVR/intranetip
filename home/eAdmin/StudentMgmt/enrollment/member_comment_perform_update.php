<?php

// Using: 

/********************************
 *  2020-01-21 (Tommy)
 *  fix: AdjustReason cannot update after using symbol
 * 
 *  2020-01-17 (Tommy)
 *  updated update sql for updating $sys_custom['eEnrolment']['Refresh_Performance']
 * 
 *  2016-12-20 (Carlos)
 *  $sys_custom['StudentAttendance']['SyncDataToPortfolio'] - sync student activity profile and portfolio.
 *  
 *  2015-12-16 (Omas)
 *  add activity merit
 * 
 *  2013-05-22 (Rita)
 *  add club merit
 * *****************************/

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

if (!$plugin['eEnrollment']) {
	intranet_closedb();
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
}

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);
$GroupID = IntegerSafe($GroupID);

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

$libenroll = new libclubsenrol();

if ($libenroll->hasAccessRight($_SESSION['UserID']))
{
	if ($type=="activity")
	{
		$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
		
		//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_EVENT_PIC($EnrolEventID)))
		if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) 
			 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) 
			 && (!$libenroll->IS_ENROL_PIC($UserID, $EnrolEventID, 'Activity'))
			 && (!$libenroll->IS_ENROL_PIC($UserID, $EventInfoArr['EnrolGroupID'], 'Club'))
			)
			header("Location: ".$PATH_WRT_ROOT."home/eAdmin/StudentMgmt/enrollment/");
			
		for ($i=0; $i<$StudentSize; $i++){
//			# update performance
//			$performance = trim($performID[$i]);
//			$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET Performance = '".$performance."' WHERE EnrolEventID = $EnrolEventID AND StudentID = ".${"UID".$i};
//			$libenroll->db_db_query($sql);
//			
//			# update comment
//			$comment = trim($commentID[$i]);
//			$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET CommentStudent = '".$comment."' WHERE EnrolEventID = $EnrolEventID AND StudentID = ".${"UID".$i};
//			$libenroll->db_db_query($sql);

			$_studentId = ${"UID".$i};
			
			$performance = trim(stripslashes($performID[$i]));
			$comment = trim(stripslashes($commentID[$i]));
			$achievement = trim(stripslashes($achievementID[$i]));
			
			$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET Performance = '".$libenroll->Get_Safe_Sql_Query($performance)."', CommentStudent = '".$libenroll->Get_Safe_Sql_Query($comment)."', Achievement = '".$libenroll->Get_Safe_Sql_Query($achievement)."', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' WHERE EnrolEventID = '".$EnrolEventID."' AND StudentID = '".$_studentId."'";
			$SuccessArr[$_studentId] = $libenroll->db_db_query($sql);
			
			# update Performance Mark [CRM : 2011-0616-1516-53126]
			if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
				$eca = trim(${'eca'.$i});
				$ss = trim(${'ss'.$i});
				$cs = trim(${'cs'.$i});
				$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET ECA = '".$eca."', SS = '".$ss."', CS = '".$cs."' WHERE EnrolEventID = '$EnrolEventID' AND StudentID = '".${"UID".$i}."'";
				$libenroll->db_db_query($sql);
			}
			
			if ($libenroll->enableUserJoinDateRange()) {
				
				$EnrolAvailiableDateStart = trim(stripslashes($AvailiableDateStart[$i]));
				$EnrolAvailiableDateEnd = trim(stripslashes($AvailiableDateEnd[$i]));
				
				$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET EnrolAvailiableDateStart = '".$libenroll->Get_Safe_Sql_Query($EnrolAvailiableDateStart)." 00:00:00', EnrolAvailiableDateEnd = '".$libenroll->Get_Safe_Sql_Query($EnrolAvailiableDateEnd)." 23:59:00' WHERE EnrolEventID = '".$EnrolEventID."' AND StudentID = '".$_studentId."'";
				$libenroll->db_db_query($sql);
			}
		}
		
		if($sys_custom['eEnrolment']['SyncDataToPortfolio']){
			$libenroll->SyncProfileRecordsForActivity($AcademicYearID, $YearTermID, $EnrolEventID);
			$libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
		}
		
		# Activity 	
		############################ Merit Record Update ###########################################
		if($libenroll->enableActivityRecordMeritInfoRight()){
			$thisMeritCount = $_POST['meritCount'];				
			$meritResult = array();			
			foreach ($thisMeritCount as $thisStudentID=>$studentDataArr){
				foreach ($studentDataArr as $meritType=>$meritCountArr){
					$meritCountValue =  $meritCountArr['CountValue'];
					$meritRecordId =  $meritCountArr['MeritRecordID'];	
					if($meritRecordId!='' && $meritCountValue!=''){
						$meritResult[]= $libenroll->Update_Merit_Record('',$meritCountValue,$meritType, $thisStudentID, $EnrolEventID,true);
					}elseif($meritRecordId=='' && $meritCountValue!=''){
						$meritResult[]= $libenroll->Add_Merit_Record($thisStudentID,$EnrolEventID,$meritCountValue,$meritType,true);
					}elseif($meritRecordId!='' && $meritCountValue==''){
						$meritResult[]= $libenroll->Remove_Merit_Record($meritRecordId,'','','',true);
					}
				}	
			}								
		}
		##############################################################################################	
	}
	else
	{
		if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))&&(!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))&&(!$libenroll->IS_CLUB_PIC($EnrolGroupID)))
			header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		
		if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
		    $termNameLang = Get_Lang_Selection('YearTermNameB5', 'YearTermNameEN');
		    
		    $sqlTemp = "SELECT YearTermID, $termNameLang as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
		    $termAry = $libenroll->returnResultSet($sqlTemp);
		    $numOfTerm = count($termAry);
		    
		    $termBasedPerformanceAssoAry = BuildMultiKeyAssoc($libenroll->getClubTermBasedStudentPerformance($EnrolGroupID), array('StudentID', 'YearTermID'));
		}
		
		$termBasedPerConsolidatedDataAry = array();
		for ($i=0; $i<$StudentSize; $i++){
//			# update performance
//			$performance = trim($performID[$i]);
//			$sql = "UPDATE INTRANET_USERGROUP SET Performance = '".$performance."' WHERE EnrolGroupID = $EnrolGroupID AND UserID = ".${"UID".$i};
//			$libenroll->db_db_query($sql);
//						
//			# update comment
//			$comment = trim($commentID[$i]);
//			$sql = "UPDATE INTRANET_USERGROUP SET CommentStudent = '".$comment."' WHERE EnrolGroupID = $EnrolGroupID AND UserID = ".${"UID".$i};
//			$libenroll->db_db_query($sql);
			
			$_studentId = ${"UID".$i};
			
			$performance = trim(stripslashes($performID[$i]));
			if($sys_custom['eEnrolment']['Refresh_Performance']){
			    $adjustedReasonValue = trim(stripslashes($adjustedReason[$i]));
			    $adjustedReasonValue = addslashes($adjustedReasonValue);
			    $adjustedReasonSQL = ", AdjustReason = '".$adjustedReasonValue."'";
			}
			$comment = trim(stripslashes($commentID[$i]));
			$achievement = trim(stripslashes($achievementID[$i]));

			$sql = "UPDATE INTRANET_USERGROUP SET Performance = '".$libenroll->Get_Safe_Sql_Query($performance)."', CommentStudent = '".$libenroll->Get_Safe_Sql_Query($comment)."', Achievement = '".$libenroll->Get_Safe_Sql_Query($achievement)."', DateModified = now(), ModifiedBy = '".$_SESSION['UserID']."' ".$adjustedReasonSQL." WHERE EnrolGroupID = '".$EnrolGroupID."' AND UserID = '".$_studentId."'";
			$SuccessArr[$_studentId] = $libenroll->db_db_query($sql);
			
			if ($libenroll->enableUserJoinDateRange()) {
			
				$EnrolAvailiableDateStart = trim(stripslashes($AvailiableDateStart[$i]));
				$EnrolAvailiableDateEnd = trim(stripslashes($AvailiableDateEnd[$i]));
				$sql = "UPDATE INTRANET_USERGROUP SET EnrolAvailiableDateStart = '".$libenroll->Get_Safe_Sql_Query($EnrolAvailiableDateStart)." 00:00:00', EnrolAvailiableDateEnd = '".$libenroll->Get_Safe_Sql_Query($EnrolAvailiableDateEnd)." 23:59:00' WHERE EnrolGroupID = '".$EnrolGroupID."' AND UserID = '".$_studentId."'";
				$libenroll->db_db_query($sql);
			}
			
			if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
			    for ($j=0; $j<$numOfTerm; $j++) {
			        $__termId = $termAry[$j]['YearTermID'];
			        
			        $__newPerformance = stripslashes($_POST['termBasedPerformanceAry'][$_studentId][$__termId]);
			        $__existingRecordId = $termBasedPerformanceAssoAry[$_studentId][$__termId]['RecordID'];
			        
			        $__tmpAry = array();
			        if ($__existingRecordId > 0) {
			            $__tmpAry['RecordID'] = $__existingRecordId;
			        }
			        $__tmpAry['StudentID'] = $_studentId;
			        $__tmpAry['YearTermID'] = $__termId;
			        $__tmpAry['EnrolGroupID'] = $EnrolGroupID;
			        $__tmpAry['Performance'] = $__newPerformance;
			        
			        $termBasedPerConsolidatedDataAry[] = $__tmpAry;
			    }
			}
		}
		
		if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub']) {
		    $success = $libenroll->saveClubTermBasedStudentPerformance($termBasedPerConsolidatedDataAry);
		}
		
		if($sys_custom['eEnrolment']['SyncDataToPortfolio']){
			$libenroll->SyncProfileRecordsForClub($AcademicYearID, $YearTermID, $EnrolGroupID);
			$libenroll->SyncProfileRecordToPortfolio($AcademicYearID, $YearTermID);
		}
		
			# Club 	
		############################ Merit Record Update ###########################################
		if($libenroll->enableClubRecordMeritInfoRight()){
			$thisMeritCount = $_POST['meritCount'];				
			$meritResult = array();			
			foreach ($thisMeritCount as $thisStudentID=>$studentDataArr){
				foreach ($studentDataArr as $meritType=>$meritCountArr){
					$meritCountValue =  $meritCountArr['CountValue'];
					$meritRecordId =  $meritCountArr['MeritRecordID'];	
					if($meritRecordId!='' && $meritCountValue!=''){
						$meritResult[]= $libenroll->Update_Merit_Record('',$meritCountValue,$meritType, $thisStudentID, $EnrolGroupID);
					}elseif($meritRecordId=='' && $meritCountValue!=''){
						$meritResult[]= $libenroll->Add_Merit_Record($thisStudentID,$EnrolGroupID,$meritCountValue,$meritType);
					}elseif($meritRecordId!='' && $meritCountValue==''){
						$meritResult[]= $libenroll->Remove_Merit_Record($meritRecordId);
					}
				}	
			}								
		}
		##############################################################################################	
		
	}
	
	

		
	intranet_closedb();
	
	if ($type=="activity")
	{
		header("Location:event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&msg=2");
	}
	else
	{
		header("Location:member_index.php?AcademicYearID=$AcademicYearID&GroupID=$GroupID&EnrolGroupID=$EnrolGroupID&msg=2");
	}
}
else{
	intranet_closedb();
	
	if ($type=="activity")
	{
		header("Location:event_member_index.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID");
	}
	else
	{
		header("Location:member_index.php?AcademicYearID=$AcademicYearID&GroupID=$GroupID&EnrolGroupID=$EnrolGroupID");
	}
}
?>
	
