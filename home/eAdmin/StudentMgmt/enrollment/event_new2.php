<?php
#using : yat

/******************************************
 * Modification Log:
  *	2011-06-13	YatWoon
 *	- add confirm message [Case#2011-0607-1139-34067]
 *
 * 		20100802 Thomas
 * 			- Check the data is freezed or not
 *			  Show alert message when adding / updating the data
 *
 *		20100730 Thomas
 *			- Added Javascript Function
 *			  1. getWeekOfMonth 2. DateStr_To_DateObj 3. DateObj_To_DateStr 4. AddRepeatActivity
 *			- Modified Javascript Function
 *			  1. GoToMonth 2. jAddEnrollmentTime 3. jDeleteTableRow
 *			- Added Javascript Variable
 *			  1. NewAddDate (Array)
 *			- Modified Javascript Variable
 *			  1. MonthMap[0]
 *
 ******************************************/

$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");

intranet_auth();
intranet_opendb();

if (!isset($AcademicYearID) || $AcademicYearID=="")
	$AcademicYearID = Get_Current_Academic_Year_ID();


$libenroll = new libclubsenrol($AcademicYearID);

//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
//if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
//	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
//	 && (!$libenroll->IS_CLUB_PIC())
//	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
//	)
	
$EnrollEventArr = $libenroll->GET_EVENTINFO($EnrolEventID);
$ActivityEnrolGroupID = $EnrollEventArr['EnrolGroupID'];
$IsActivityClubPIC = $libenroll->IS_CLUB_PIC($ActivityEnrolGroupID);
if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && (!$IsActivityClubPIC)
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
//		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
		
$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EnrolEventID);  
//$EnrollEventArr = $libenroll->GET_EVENTINFO($EnrolEventID);

if (isset($_POST['column2add'])) {
	$column2add = $_POST['column2add'];
} else if (sizeof($DateArr) > 0) {
	$column2add = sizeof($DateArr) + 1;
} else {
	$column2add = 1;
}

if (!isset($_POST['tableDates'])) $tableDates = array();

$LibUser = new libuser($UserID);

if (!isset($thisMonth)) {
	$thisMonth = date('Y-m-d');
}

if ($plugin['eEnrollment'])
{
	
	$CurrentPage = "PageMgtActivity";
	$CurrentPageArr['eEnrolment'] = 1;
	if (($LibUser->isStudent())||($LibUser->isParent())) {
		$CurrentPageArr['eEnrolment'] = 0;
		$CurrentPageArr['eServiceeEnrolment'] = 1;
	}
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        //$TAGS_OBJ[] = array($eEnrollmentMenu['mgt_event'], "", 1);
        # tags 
        $tab_type = "activity";
        $current_tab = 1;
        include_once("management_tabs.php");
        # navigation
//        if ($isNew == 1)
//        {
//	        $PAGE_NAVIGATION[] = array($eEnrollment['new_activity'], "");
//        }
//        else
//        {
	        $PAGE_NAVIGATION[] = array($eEnrollment['edit_activity'], "");
	        $PAGE_NAVIGATION[] = array($EnrollEventArr['EventTitle'], "");
//        }

//        $STEPS_OBJ[] = array($eEnrollment['add_activity']['step_1'], 0);
//        $STEPS_OBJ[] = array($eEnrollment['add_activity']['step_1b'], 0);
//		$STEPS_OBJ[] = array($eEnrollment['add_activity']['step_2'], 1);
//		$STEPS_OBJ[] = array($eEnrollment['add_activity']['step_3'], 0);
		
        $linterface->LAYOUT_START();
        
      
//debug_r($DateArr);

for ($i = 0; $i < sizeof($DateArr); $i++) {	
	if (!in_array(date("Y-m-d", strtotime($DateArr[$i][2])), $tableDates)) {
		$tableDates[] = date("Y-m-d", strtotime($DateArr[$i][2]));
		$StartHour[] = date("H", strtotime($DateArr[$i][2]));
		$StartMin[] = date("i", strtotime($DateArr[$i][2]));
		$EndHour[] = date("H", strtotime($DateArr[$i][3]));
		$EndMin[] = date("i", strtotime($DateArr[$i][3]));
	}
}

$schedulingDatesTitle = $linterface->GET_NAVIGATION2($eEnrollment['schedule_settings']['scheduling_dates']);
$schedulingTimesTitle = $linterface->GET_NAVIGATION2($eEnrollment['schedule_settings']['scheduling_times']);

$AcademicStart = date("Y-m-d", getStartOfAcademicYear());
$AcademicEnd   = date("Y-m-d", getEndOfAcademicYear());
?>

<script language="javascript">
var NewAddDate = new Array();

var MultiCheck = false;
function CheckHorizontal(jParNo, jParCheck, CurrentMonth) {
	MultiCheck = true;
	for (i = 0; i < 7; i++) {
		if (document.getElementById('checkboxDate[' + CurrentMonth + '][' + jParNo + '][' + i + ']') != null) {	
			if (jParCheck) {
				
				if (!document.getElementById('checkboxDate[' + CurrentMonth + '][' + jParNo + '][' + i + ']').checked) {
					if (document.getElementById('fieldName[' + CurrentMonth + '][' + jParNo + '][' + i + ']') != null) {
							//jAddEnrollmentTime(document.FormMain, 'EnrollmentTable', 'column2add', document.getElementById('fieldName[' + CurrentMonth + '][' + jParNo + '][' + i + ']').value, document.getElementById('checkboxDate[' + CurrentMonth + '][' + jParNo + '][' + i + ']').value);
							jAddEnrollmentTime(document.getElementById('checkboxDate[' + CurrentMonth + '][' + jParNo + '][' + i + ']').value);
					}
				}
				
			} else {
				if (document.getElementById('fieldName[' + CurrentMonth + '][' + jParNo + '][' + i + ']') != null) 
						//jDeleteTableRow('EnrollmentTable', 'colNo'+document.getElementById('fieldName[' + CurrentMonth + '][' + jParNo + '][' + i + ']').value, 'column2add', 'h'+jParNo , 'v'+i,document.getElementById('checkboxDate[' + CurrentMonth + '][' + jParNo + '][' + i + ']').value);
						jDeleteTableRow(document.getElementById('checkboxDate[' + CurrentMonth + '][' + jParNo + '][' + i + ']').value);
				}			
			document.getElementById('checkboxDate[' + CurrentMonth + '][' + jParNo + '][' + i + ']').checked = jParCheck;
		}
	}
	SortRows();
	MultiCheck = false;
}

function CheckVertical(jParNo, jParCheck, CurrentMonth) {	
	MultiCheck = true;
	for (i = 0; i <= 5; i++) {
		
		if (document.getElementById('checkboxDate[' + CurrentMonth + '][' + i + '][' + jParNo + ']') != null)	{			
			if (jParCheck) {				
				if (!document.getElementById('checkboxDate[' + CurrentMonth + '][' + i + '][' + jParNo + ']').checked) {					
					if ((document.getElementById('fieldName[' + CurrentMonth + '][' + i + '][' + jParNo + ']') != null)) {
						//jAddEnrollmentTime(document.FormMain, 'EnrollmentTable', 'column2add', document.getElementById('fieldName[' + CurrentMonth + '][' + i + '][' + jParNo + ']').value, document.getElementById('checkboxDate[' + CurrentMonth + '][' + i + '][' + jParNo + ']').value);
						jAddEnrollmentTime(document.getElementById('checkboxDate[' + CurrentMonth + '][' + i + '][' + jParNo + ']').value);						
					}
				}
			} else {
				if (document.getElementById('fieldName[' + CurrentMonth + '][' + i + '][' + jParNo + ']') != null) 
					
					
					//jDeleteTableRow('EnrollmentTable', 'colNo'+document.getElementById('fieldName[' + CurrentMonth + '][' + i + '][' + jParNo + ']').value, 'column2add', 'h'+i , 'v'+jParNo,document.getElementById('checkboxDate[' + CurrentMonth + '][' + i + '][' + jParNo + ']').value);
					jDeleteTableRow(document.getElementById('checkboxDate[' + CurrentMonth + '][' + i + '][' + jParNo + ']').value);
				}
				
			document.getElementById('checkboxDate[' + CurrentMonth + '][' + i + '][' + jParNo + ']').checked = jParCheck;
		}			
	}
	SortRows();
	MultiCheck = false;
}

function setPeriods(jObj, jParElementName, jSetAll) {
	jqStartHour = $("[name='StartHour[]']");
	jqEndHour = $("[name='EndHour[]']");
	jqStartMin = $("[name='StartMin[]']");
	jqEndMin = $("[name='EndMin[]']");
	jqtoSet = $("[name='toSet[]']");

	for(var i=0; i<jqStartHour.length;i++)
	{		
		if(jqtoSet[i].checked||jSetAll)
		{
			jqStartHour[i].value = $("#SetStartHour").val();
			jqEndHour[i].value = $("#SetEndHour").val();
			jqStartMin[i].value = $("#SetStartMin").val();
			jqEndMin[i].value = $("#SetEndMin").val();
		}
	}
	
//	len = jObj.elements.length;
//	var i = 0, temp = 0;
//	for(i = 0 ; i < len ; i++) {
//		if ( jObj.elements[i].name == jParElementName && (jObj.elements[i].checked || jSetAll == 1) )
//		{
//			temp = jObj.elements[i].value;
//			
//			jObj.elements['StartHour[]'][temp].value = document.getElementById('SetStartHour').value;
//			jObj.elements['StartMin[]'][temp].value = document.getElementById('SetStartMin').value;
//			jObj.elements['EndHour[]'][temp].value = document.getElementById('SetEndHour').value;
//			jObj.elements['EndMin[]'][temp].value = document.getElementById('SetEndMin').value;
//		}
//	}
}


//function jInsertTableRow(jTableName, jFileNumber){
//	// first argument is jTableName
//	// other arguments after will be td contents
//	var row = jTableName.insertRow(jFileNumber);
//	
//	for(var i=2; i<arguments.length; i++){
//        cell = row.insertCell(i-2);
//		cell.innerHTML = arguments[i];
//    }
//}

//function jDeleteTableRow(jTableName, jRowtoDelete, jFieldName, jHorizontalName, jVerticalName, jDate)
function jDeleteTableRow(jDate)
{

	$("#timetr"+jDate).remove();
	
	var NewAddDate_key = jDate.substr(0,7); 
	
	for(var i=0; i<NewAddDate[NewAddDate_key].length; i++)
	{
		//alert(jDate == DateObj_To_DateStr(NewAddDate[NewAddDate_key][i]));
		if(jDate == NewAddDate[NewAddDate_key][i])
		{
			//alert(NewAddDate[NewAddDate_key].toString());
			NewAddDate[NewAddDate_key][i] = null;
			//alert(NewAddDate[NewAddDate_key].toString());
			NewAddDate[NewAddDate_key] = NewAddDate[NewAddDate_key].sort();
			//alert(NewAddDate[NewAddDate_key].toString());
			NewAddDate[NewAddDate_key].pop();
			//alert(NewAddDate[NewAddDate_key].toString());
			break;
		}
	}

//  var tbl = document.getElementById(jTableName); 
//   
//  //var rowNumber = document.getElementById(jRowtoDelete).value - 1;
//  var rowNumber = document.getElementById(jRowtoDelete).value;
//	//alert(jRowtoDelete);
//	//alert(document.getElementById(jRowtoDelete).value); 
//
//  tbl.deleteRow(rowNumber);
//  document.getElementById(jFieldName).value = document.getElementById(jFieldName).value - 1;
//  document.getElementById(jHorizontalName).checked = 0;
//  document.getElementById(jVerticalName).checked = 0;
//
//  var preString = "<?= date("Ym", strtotime($thisMonth))?>";
//  
//  <?/*
//  		for ($i = 1; $i <= date('t'); $i++) {
//	  		($i < 10) ? $no = "0".$i : $no = $i;
//  ?>  
//  if (document.getElementById('colNo'+preString+'<?= $no?>') != null) {
//  	//if (document.getElementById('colNo'+preString+'<?= $no?>').value == (parseInt(rowNumber)+1)) document.getElementById('colNo'+preString+'<?= $no?>').value = "";
//  	if (document.getElementById('colNo'+preString+'<?= $no?>').value >= (parseInt(rowNumber)+1)) document.getElementById('colNo'+preString+'<?= $no?>').value = document.getElementById('colNo'+preString+'<?= $no?>').value - 1;  
//  }
//  <? }*/ ?>


}

//function jAddEnrollmentTime(jFormObj, jTableName, jNumberName, jFieldName, jDate){
function jAddEnrollmentTime(jDate, jStart_h, jStart_m, jEnd_h, jEnd_m){
	// prepare td contents
	// manipulate jInsertTableRow()	
//	if (document.all || document.getElementById)
//	{
//		var table = document.all ? document.all[jTableName] : document.getElementById(jTableName);
//		var col_no = parseInt(document.getElementById(jNumberName).value);
//		var count;
//		var temp;

		jStart_h = jStart_h? jStart_h : "00";
		jStart_m = jStart_m? jStart_m : "00";
		jEnd_h = jEnd_h? jEnd_h : "00";
		jEnd_m = jEnd_m? jEnd_m : "00";
		
		if (!document.getElementById('timetr'+jDate))
		{
			var td1 = "<td>" + jDate + "<input type='hidden' name='tableDates[]' id='tableDates[]' value='" + jDate + "'></td>";
			var td2 = "<td>";
				td2 += "<select id='StartHour[]' name='StartHour[]'>";
				for (count = 0; count < 24; count++) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'" + (temp == jStart_h? " Selected" : "") + ">"+ temp + "</option>";
				}
				td2 += "</select> : ";
				
				td2 += "<select id='StartMin[]' name='StartMin[]'>";
				for (count = 0; count < 56; count+= 5) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'" + (temp == jStart_m? " Selected" : "") + ">"+ temp + "</option>";
				}
				td2 += "</select>";
				
				td2 += "<span class=\"tabletext\"> <?= $eEnrollment['to']?> </span>";
				
				td2 += "<select id='EndHour[]' name='EndHour[]'>";
				for (count = 0; count < 24; count++) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'" + (temp == jEnd_h? " Selected" : "") + ">"+ temp + "</option>";
				}
				td2 += "</select> : ";
				
				td2 += "<select id='EndMin[]' name='EndMin[]'>";
				for (count = 0; count < 56; count+= 5) {
					(count < 10) ? temp = "0" + count : temp = count;
					td2 += "<option value='"+ temp + "'" + (temp == jEnd_m? " Selected" : "") + ">"+ temp + "</option>";
				}
				td2 += "</select>";
				td2 += "</td>";
			var td3 = "<td><input type='checkbox' id='toSet[]' name='toSet[]' value='" + jDate + "'></td>";
			
			$("#EnrollmentTable>tbody").append("<tr id=\"timetr"+jDate+"\">"+td1+td2+td3+"</tr>");
			
			if(!MultiCheck) SortRows();
			
//			jInsertTableRow(table, col_no, td1, td2, td3);
//			document.getElementById('colNo'+jFieldName).value = table.rows.length - 1;
//			document.getElementById(jNumberName).value = col_no + 1;
		//}
//	}
			if(!NewAddDate[jDate.substr(0,7)])
				NewAddDate[jDate.substr(0,7)] = new Array();
			NewAddDate[jDate.substr(0,7)].push(jDate);
		}
}

function AddRepeatActivity()
{
	MultiCheck = true;
	
	var no_err = true;
	var err = new Array();
	err[0] = false;
	err[1] = false;
	err[2] = false;
	err[3] = false;
	
	var StartDateString = document.getElementById('RepeatActStart').value;
	var Start_y   = parseInt(StartDateString.substr(0,4), 10);
	var Start_m   = parseInt(StartDateString.substr(5,7), 10);
	var Start_d   = parseInt(StartDateString.substr(8,10), 10);
	var Start_hr  = document.getElementById('RepeatActStartHour').value;
	var Start_min = document.getElementById('RepeatActStartMin').value;
	
	var EndDateString = document.getElementById('RepeatActEnd').value;
	var End_y   = parseInt(EndDateString.substr(0,4), 10);
	var End_m   = parseInt(EndDateString.substr(5,7), 10);
	var End_d   = parseInt(EndDateString.substr(8,10), 10);
	var End_hr  = document.getElementById('RepeatActEndHour').value;
	var End_min = document.getElementById('RepeatActEndMin').value;
	
	var StartDate = new Date(Start_y, (Start_m-1), Start_d, 0, 0, 0);
	var StartDay = StartDate.getDay();
	
	var EndDate = new Date(End_y, (End_m-1), End_d, 23, 59, 59);
	
	var AcademicStart = new Date(<?= substr($AcademicStart,0,4)?>, (<?= substr($AcademicStart,5,2)?>-1), <?= substr($AcademicStart,8,2)?>, 0, 0, 0);
	var AcademicEnd   = new Date(<?= substr($AcademicEnd,0,4)?>, (<?= substr($AcademicEnd,5,2)?>-1), <?= substr($AcademicEnd,8,2)?>, 23, 59, 59);
	
	if(StartDate < AcademicStart || StartDate > AcademicEnd)
		err[0] = true;
	
	if(EndDate < AcademicStart || EndDate > AcademicEnd)
		err[1] = true;
		
	if(StartDate > EndDate)
		err[2] = true;

	err[3] = true
	for(var i=0;i<7;i++)
	{
		var obj = document.getElementById('RepeatDay_'+i);
		if(obj.checked)
		{
			err[3] = false;
			if(!(err[0]||err[1]||err[2]))
			{
				DaysB4Start = ((i-StartDay)+7)%7;
				var tempActDate = new Date();
				tempActDate.setFullYear(Start_y, (Start_m-1), Start_d);
				tempActDate.setDate(StartDate.getDate()+DaysB4Start);
				while(tempActDate<=EndDate)
				{
					var tempActDateStr = DateObj_To_DateStr(tempActDate);
					
					if(document.getElementById('checkboxDate[' + tempActDateStr.substr(0,7) + '-01][' + getWeekOfMonth(tempActDate) + '][' + i + ']'))
						document.getElementById('checkboxDate[' + tempActDateStr.substr(0,7) + '-01][' + getWeekOfMonth(tempActDate) + '][' + i + ']').checked = 1;
					
					//if (!document.getElementById('timetr'+tempActDateStr))
					//{
						jAddEnrollmentTime(tempActDateStr, Start_hr, Start_min, End_hr, End_min);
					
	
					//}
					tempActDate.setDate(tempActDate.getDate()+7);
				}
			}
			else
				break;
		}
	}
	SortRows();
	MultiCheck = false;
	
	for(var i=0;i<4;i++)
	{
		if(err[i])
		{
			$('#ErrorLog').attr('style', '');
			$('#Err_'+i).attr('style', '');
			no_err = false;
		}
		else
			$('#Err_'+i).attr('style', 'display: none');
	}
	
	if(no_err)
	{
		$('#ErrorLog').attr('style', 'display: none');
	}
}

function getWeekOfMonth(DateObj)
{
	var DayofMonth = DateObj.getDate();
	var FirstDateofMonth = new Date(DateObj.getFullYear(), DateObj.getMonth(), 1);
	var DayofFirstDate = FirstDateofMonth.getDay();
	
	return Math.ceil((DayofMonth + DayofFirstDate)/7)-1;
}

function DateStr_To_DateObj(DateStr)
{
	if(DateStr.length = 10)
	{
		var Date_y   = parseInt(DateStr.substr(0,4), 10);
		var Date_m   = parseInt(DateStr.substr(5,7), 10);
		var Date_d   = parseInt(DateStr.substr(8,10), 10);
		
		var DateObj = new Date(Date_y, (Date_m-1), Date_d);
		return DateObj;
	}
	else
		return false;
}

function DateObj_To_DateStr(DateObj)
{
	if(DateObj)
	{
		var DateObj_y_str  = DateObj.getFullYear().toString();
		var DateObj_m_str  = ((DateObj.getMonth() + 1 < 10)? '0'+(DateObj.getMonth()+1) : DateObj.getMonth()+1).toString();
		var DateObj_d_str  = ((DateObj.getDate() < 10)? '0'+DateObj.getDate() : DateObj.getDate()).toString();
				
		var DateObjStr = DateObj_y_str + '-' + DateObj_m_str + '-' + DateObj_d_str;
		return DateObjStr;
	}
	else
		return false;
}

function sortRowFunc(a,b)
{
	// obj.id is the date of the event
	return compareDate2(a.id.substring(6),b.id.substring(6));
}

function SortRows()
{
	// sort the time entries by date in Scheduling Times 			
	jRowObj = $("#EnrollmentTable>tbody").find("tr[id]").get();
	jRowObj.sort(sortRowFunc);
	$("#EnrollmentTable>tbody").find("tr[id]").remove();
	for(var i in jRowObj)
		$("#EnrollmentTable>tbody").append(jRowObj[i]);
}

function checkSubmit(obj) {
	jqStartHour = $("[name='StartHour[]']");
	jqEndHour = $("[name='EndHour[]']");
	jqStartMin = $("[name='StartMin[]']");
	jqEndMin = $("[name='EndMin[]']");
	
	for(var i=0; i<jqStartHour.length;i++)
	{
		if (jqStartHour[i].value> jqEndHour[i].value ||	(jqStartHour[i].value == jqEndHour[i].value && jqStartMin[i].value > jqEndMin[i].value))
		{
			alert("<?= $eEnrollment['js_time_invalid']?>");
			return false;
		}
	}
	
	if(confirm("<?=$Lang['eEnrolment']['ConfirmChangeScheduleTime']?>"))
	{
		obj.action = 'event_new2_update.php';
		obj.submit();
	}

//	if (document.getElementById('column2add').value == 1) {
//		alert('<?= $eEnrollment['js_sel_date']?>');
//	} else {
//		
//		len = obj.elements.length;
//		var i = 0, temp = 0;		
//		for(i = 0 ; i < len ; i++) {
//			if (obj.elements[i].name == 'toSet[]')
//			{
//				temp = obj.elements[i].value;				
//				if ( (obj.elements['StartHour[]'][temp] != undefined && obj.elements['EndHour[]'][temp] !=undefined) &&
//					(obj.elements['StartHour[]'][temp].value > obj.elements['EndHour[]'][temp].value) ||
//					((obj.elements['StartHour[]'][temp].value == obj.elements['EndHour[]'][temp].value) &&
//					 (obj.elements['StartMin[]'][temp].value > obj.elements['EndMin[]'][temp].value)
//					)
//					)
//				{
//					alert("<?= $eEnrollment['js_time_invalid']?>");
//					return false;
//				}
//			}
//		}
//		
//		/*
//		if (confirm("<?= $eEnrollment['js_del_warning']?>")) {
//			obj.action = 'group_new2_update.php';
//			obj.submit();
//		}
//		*/
//		obj.action = 'group_new2_update.php';
//		obj.submit();
//	}
}

//var thisMonth="<?=$thisMonth?>";
var MonthStart = 0;
var MonthEnd = 0;
var monthptr = 0;
var MonthMap = new Array();
MonthMap[0] = "<?=$thisMonth?>";
function GoToMonth(PrevNext)
{
	if(monthptr + PrevNext>MonthEnd||monthptr + PrevNext<MonthStart)
	{
		var CurrentMonth = $("#CurrentMonth").val();
		$.post(
			'ajaxGetCalendar.php', 
			{ 
				CurrentMonth: CurrentMonth, 
				Move: PrevNext,
				EnrolGroupID:'<?=$EnrolEventID?>',
				IsEvent:1 
			},
			function(data) {
				monthptr += PrevNext;
		  		if(monthptr>MonthEnd)
		  			MonthEnd = monthptr;
		  		else if(monthptr<MonthStart)
		  			MonthStart = monthptr;
		  			
		  		var DataArr = data.split("|=|");
		  		$("#CurrentMonth").val(DataArr[0]);
		  		if(PrevNext>0)
		  		{
		  			$("#MonthTR").append("<td valign='top' class='test'>"+DataArr[1]+"</td>");
		  		}
		  		else
		  		{
		  			$("#MonthTR").prepend("<td valign='top' class='test'>"+DataArr[1]+"</td>");
		  			$("#MonthTable").css("left",parseInt($("#MonthTable").css("left"))-550);
		  		}
		  		
		  		if(NewAddDate[DataArr[0].substr(0,7)])
		  		{
		  			//alert(NewAddDate[DataArr[0].substr(0,7)].toString());
		  			for(var i=0;i<NewAddDate[DataArr[0].substr(0,7)].length;i++)
		  			{
		  				var tempDateObj = new Date();
		  				tempDateObj = DateStr_To_DateObj(NewAddDate[DataArr[0].substr(0,7)][i]);
		  				//if(document.getElementById('checkboxDate[' + DataArr[0].substr(0,7) + '-01][' + getWeekOfMonth(tempDateObj) + '][' + tempDateObj.getDay() + ']'))
							document.getElementById('checkboxDate[' + DataArr[0].substr(0,7) + '-01][' + getWeekOfMonth(tempDateObj) + '][' + tempDateObj.getDay() + ']').checked = 1;
		  			}
		  		}
		  		
		  		MonthMap[monthptr] = DataArr[0];
		  		var destiny = PrevNext*-550;
				$("#MonthTable").animate({left: '+='+destiny},800);
		  		
		});
	}
	else
	{
		monthptr += PrevNext;
		$("#CurrentMonth").val(MonthMap[monthptr]);
		var destiny = PrevNext*-550;
		$("#MonthTable").animate({left: '+='+destiny},550);
		
	}
	


}

$().ready(function(){
	var MonthStart = 0;
	var MonthEnd = 0;
	var monthptr = 0;
	$("#CurrentMonth").val("<?=$thisMonth?>");
	$("#CurrentMonthTd").load(
		'ajaxGetCalendar.php', 
		{ 
			CurrentMonth: "<?=$thisMonth?>", 
			Move: 0,
			EnrolGroupID:'<?=$EnrolEventID?>',
			IsEvent: 1 
		}
	);
	
});

function delSelectedDate()
{
	if(check_checkbox(document.FormMain, "toSet[]"))
	{
		if (confirm("<?=$Lang['eEnrolment']['ConfirmDelete']?>")) 
		{
			document.FormMain.action="delete_selected_date_activity.php";
			document.FormMain.submit();
		}
	}
	else
	{
		alert("<?=$Lang['eEnrolment']['delete_selected_date_warning']?>");
	}
}

</script>

<form name="FormMain" action="" method="POST">
<!--<input type="hidden" id="StartHour[]" name="StartHour[]" value="">
<input type="hidden" id="StartMin[]" name="StartMin[]" value="">
<input type="hidden" id="EndHour[]" name="EndHour[]" value="">
<input type="hidden" id="EndMin[]" name="EndMin[]" value="">-->
<input type="hidden" id="CurrentMonth" name="CurrentMonth" value="<?=$thisMonth?>">

<br/>
<table width="100%" border="0" cellspacing="4" cellpadding="4">
	<tr><td>
		<?= $linterface->GET_NAVIGATION($PAGE_NAVIGATION) ?>
	</td></tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">

<tr>
	<td class="tabletext">
		<table width="90%" border="0" cellspacing="0" cellpadding="4" align="center">
			<tr><td class="tabletext"><?=$eEnrollment['schedule_settings']['instruction']?></td></tr>
		</table>
	</td>
</tr>
<? if($libenroll->AllowToEditPreviousYearData) { ?>
<tr>
	<td class="tabletext">
		<table width="65%" border="0" cellspacing="0" cellpadding="4" align="center">
			<tr><td class="tabletext"><?=$schedulingDatesTitle?></td></tr>
		</table>
	</td>
</tr>

<tr>
	<td width="100%" align="center">
		<div style="width:550px; height:320px; overflow:hidden; padding:0; margin:0; position:relative;">
		<table cellpadding="0" cellspacing="0" border="0" align="center" id='MonthTable' style="position:relative; left:0;">
			<tr id="MonthTR">
				<td id="CurrentMonthTd" valign=top></td>
			</tr>
		</table>
		</div>
	</td>
</tr>
<tr>
	<td style='overflow:hidden'>
		<!--<?= $libenroll->GET_CALENDAR($thisMonth, $_POST, $tableDates, $EnrolEventID)?>-->
	</td>
</tr>

<!-- Function for adding weekly functions(Added by Thomas on 2010-07-30) -->
<tr>
	<td width="100%" align="center">
		<table width="550px" border="0" cellspacing="0" cellpadding="4" align="cneter">
			<tr>
				<td colspan="2" class="sectiontitle"><?= $Lang['eEnrolment']['AddWeeklyAct'] ?></td>
			</tr>
			<tr>
				<td width="20%"><?= $Lang['eEnrolment']['StartDate'] ?> :</td>
				<td>
					<?= $linterface->GET_DATE_PICKER("RepeatActStart", date("Y-m-d")) ?>
					<span><?= $Lang['eEnrolment']['MustBeLater'].": ".$AcademicStart?></span>
				</td>
			</tr>
			<tr>
				<td width="20%"><?= $Lang['eEnrolment']['EndDate'] ?> :</td>
				<td>
					<?= $linterface->GET_DATE_PICKER("RepeatActEnd", date("Y-m-d")) ?>
					<span><?= $Lang['eEnrolment']['MustBeEarlier'].": ".$AcademicEnd?></span>
				</td>
			</tr>
			<tr>
				<td width="20%"><?= $eEnrollment['add_activity']['act_time'] ?> :</td>
				<td><?= getTimeSel("RepeatActStart") ?>&nbsp;to&nbsp;<?= getTimeSel("RepeatActEnd") ?></td>
			</tr>
			<tr>
				<td width="20%"><?= $Lang['eEnrolment']['OnEvery'] ?> :</td>	
				<td>
					<input id="RepeatDay_0" type="checkbox" name="RepeatDay" value="0" /><label for="RepeatDay_0"><?=$eEnrollment['weekday']['sun']?></label>
					<input id="RepeatDay_1" type="checkbox" name="RepeatDay" value="1" /><label for="RepeatDay_1"><?=$eEnrollment['weekday']['mon']?></label>				
					<input id="RepeatDay_2" type="checkbox" name="RepeatDay" value="2" /><label for="RepeatDay_2"><?=$eEnrollment['weekday']['tue']?></label>
					<input id="RepeatDay_3" type="checkbox" name="RepeatDay" value="3" /><label for="RepeatDay_3"><?=$eEnrollment['weekday']['wed']?></label>				
					<input id="RepeatDay_4" type="checkbox" name="RepeatDay" value="4" /><label for="RepeatDay_4"><?=$eEnrollment['weekday']['thu']?></label>
					<input id="RepeatDay_5" type="checkbox" name="RepeatDay" value="5" /><label for="RepeatDay_5"><?=$eEnrollment['weekday']['fri']?></label>				
					<input id="RepeatDay_6" type="checkbox" name="RepeatDay" value="6" /><label for="RepeatDay_6"><?=$eEnrollment['weekday']['sat']?></label>
				</td>
			</tr>
			<tr id="ErrorLog" valign="top" style="display: none">
				<td style="color: #FF0000"><?= $Lang['eEnrolment']['Error']."!" ?></td>
				<td style="color: #FF0000">
					<ul style="margin: 0; padding:0">
						<li id="Err_0" style="display: none"><?= $Lang['eEnrolment']['StartDate'].$Lang['eEnrolment']['Err_IsInvalid'] ?></li>
						<li id="Err_1" style="display: none"><?= $Lang['eEnrolment']['EndDate'].$Lang['eEnrolment']['Err_IsInvalid'] ?></li>
						<li id="Err_2" style="display: none"><?= $Lang['eEnrolment']['PeriodOfTime'].$Lang['eEnrolment']['Err_IsInvalid'] ?></li>
						<li id="Err_3" style="display: none"><?= "'".$Lang['eEnrolment']['OnEvery']."' - ".$Lang['eEnrolment']['Err_NotSelect']?></li>
					</ul>
				</td>
			</tr>
			<tr>
				<td align="right" colspan="2"><?= $linterface->GET_SMALL_BTN($button_add, "button", "javascript:AddRepeatActivity();") ?></td>
			</tr>
		</table>
	</td>
</tr>
<!-- End of Function for adding weekly functions-->

<tr><td class="tabletext">&nbsp;</td></tr>
<? } ?>
<tr>
	<td class="tabletext">
		<table width="65%" border="0" cellspacing="0" cellpadding="4" align="center">
			<tr><td class="tabletext"><?=$schedulingTimesTitle?></td></tr>
		</table>
	</td>
</tr>


<tr><td>
<table width="65%" border="0" cellspacing="0" cellpadding="4" align="center" name="EnrollmentTable" id="EnrollmentTable">
<? if($libenroll->AllowToEditPreviousYearData) {?>
<tr>
	<td colspan="3" align="right"><?= $linterface->GET_SMALL_BTN($Lang['eEnrolment']['delete_selected_date'], "button", ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "javascript:delSelectedDate();") ?></td>
<tr>
<?}?>
<tr class="tablegreentop tabletopnolink">
	<td><?= $eEnrollment['date_chosen']?></td><td><?= $eEnrollment['add_activity']['act_time']?></td><td><?= $eEnrollment['add_activity']['select']?><br/><input type="checkbox" onclick="(this.checked) ? setChecked(1, document.FormMain, 'toSet[]') : setChecked(0, document.FormMain, 'toSet[]');"></td>
</tr>

<tr class="tablegreenbottom">
	<td></td>
	<td colspan="2" class="tabletext">
		<select id="SetStartHour" name="SetStartHour">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="SetStartMin" name="SetStartMin">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select>
		
		<span class="tabletext"><?= $eEnrollment['to']?></span>
		
		<select id="SetEndHour" name="SetEndHour">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="SetEndMin" name="SetEndMin">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>"><?= $temp?></option>
		<? } ?>
		</select>
		<?= $linterface->GET_SMALL_BTN($eEnrollment['add_activity']['set_all'], "button", "javascript:setPeriods(document.FormMain, 'toSet[]', 1);") ?>
		<?= $linterface->GET_SMALL_BTN($eEnrollment['add_activity']['set_select_period'], "button", "javascript:setPeriods(document.FormMain, 'toSet[]', 0);") ?>
	</td>
</tr>

<?	for ($j = 0; $j < sizeof($tableDates); $j++) { ?>		
<tr id="timetr<?= $tableDates[$j]?>">
	<td class="tabletext" ><?= $tableDates[$j]?><input type='hidden' name='tableDates[]' id='tableDates[]' value='<?= $tableDates[$j]?>'></td>
	<td>
		<select id="StartHour[]" name="StartHour[]">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>" <? if ($StartHour[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="StartMin[]" name="StartMin[]">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
				<option value="<?= $temp?>" <? if ($StartMin[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select>
		
		<span class="tabletext"><?= $eEnrollment['to']?></span>
		
		<select id="EndHour[]" name="EndHour[]">
		<?
			for ($i = 0; $i < 24; $i++) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>" <? if ($EndHour[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select> : 
		
		<select id="EndMin[]" name="EndMin[]">
		<?
			for ($i = 0; $i < 56; $i += 5) {
				($i < 10) ? $temp = "0".$i : $temp = $i;
		?>
			<option value="<?= $temp?>" <? if ($EndMin[$j] == $temp) print "selected"?>><?= $temp?></option>
		<? } ?>
		</select>
	</td>
	<td>
	<!--
		<input type='checkbox' id='toSet[]' name='toSet[]' value='<?= $_POST["colNo".date("Ymd", strtotime($_POST['tableDates'][$j]))] ?>'>
	-->
		<input type='checkbox' id='toSet[]' name='toSet[]' value='<?= ($j+1) ?>'>
		<!--<input type='text' size='2' value='<?= ($j+1) ?>'>-->
	</td>
</tr>
<? } ?>

</table>

</td></tr>



<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td class="dotline" colspan="6"><img src="<?= "{$image_path}/{$LAYOUT_SKIN}" ?>/10x10.gif" width="10" height="1"></td></tr>
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<? if($libenroll->AllowToEditPreviousYearData) {?>
<?= $linterface->GET_ACTION_BTN($button_save, "button", ($libenroll->Event_DisableUpdate==1)? "javascript:alert('".$Lang['eEnrolment']['disableUpdateAlertMsg']."')" : "checkSubmit(document.FormMain)")?>&nbsp;
<?}?>
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='event.php?AcademicYearID=$AcademicYearID'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>
</table>
<input type="hidden" id="column2add" name="column2add" value="<?= $column2add?>"/>
<input type="hidden" id="page_link" name="page_link" value=""/>
<input type="hidden" id="EnrolEventID" name="EnrolEventID" value="<?= $EnrolEventID?>"/>
<input type="hidden" name="isNew" id="isNew" value="<?= $isNew?>" />


</form>
    <?
		intranet_closedb();
        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>