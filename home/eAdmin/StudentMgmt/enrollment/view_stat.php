<?
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
include_once($PATH_WRT_ROOT."includes/json.php");

intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
$libenroll_ui = new libclubsenrol_ui();
$json = new JSON_obj();

$linterface = new interface_html("popup.html");
$MODULE_OBJ['title'] = $eEnrollment['view_statistics'];
$linterface->LAYOUT_START();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);

if($EnrolGroupID)
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	$isClubPIC = $libenroll->IS_CLUB_PIC($EnrolGroupID);
	$isClubHelper = $libenroll->IS_CLUB_HELPER($EnrolGroupID);
	
	## temp
	$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
	if (!$isEnrolAdmin && !$isEnrolMaster && !$isClubPIC && !$isClubHelper)
		$canEdit = 0;
	else
		$canEdit = 1;
	
	if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
	{
		No_Access_Right_Pop_Up();
	}
	
	### Get Club Attendance Records
	$ClubAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($EnrolGroupID,'',$AcademicYearID);
	//$ClubAttendanceInfoArr = $ClubAttendanceInfoArr[$EnrolGroupID];
	$ClubAttendanceInfoArr = $ClubAttendanceInfoArr[$EnrolGroupID];
	$ClubAttendanceArr = $libenroll_ui->Get_Management_Club_Attendance_Chart($EnrolGroupID, $ClubAttendanceInfoArr,true);
	$ClubAttendanceDataArr = $json->encode($ClubAttendanceArr);
	$NumberofClubMeeting = sizeof($ClubAttendanceArr);

	
}
else
{
	$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
	$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
	$isActivityPIC = $libenroll->IS_EVENT_PIC($EnrolEventID);
	$isActivityHelper = $libenroll->IS_EVENT_HELPER($EnrolEventID);
	
	$EventInfo = $libenroll->GET_EVENTINFO($EnrolEventID);
	$ActivityEnrolGroupID = $EventInfo['EnrolGroupID'];
	$IsActivityClubPIC = $libenroll->IS_CLUB_PIC($ActivityEnrolGroupID);
	
	
	if (!$isEnrolAdmin && !$isEnrolMaster && !$isActivityPIC && !$isActivityHelper && !$IsActivityClubPIC)
		$canEdit = false;
	else
		$canEdit = true;
	
	
	if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
	{
		No_Access_Right_Pop_Up();
	}
	
	### Get Club Attendance Records
	$ActivityAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($EnrolEventID);
	$ActivityAttendanceInfoArr = $ActivityAttendanceInfoArr[$EnrolEventID];
	$ActivityAttendanceArr = $libenroll_ui->Get_Management_Activity_Attendance_Chart($EnrolEventID, $ActivityAttendanceInfoArr,true);
	$ActivityAttendanceDataArr = $json->encode($ActivityAttendanceArr);
	$NumberOfActMeeting = sizeof($ActivityAttendanceArr);
	
}

?>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<style type="text/css">
${demo.css}
		</style>
<script type="text/javascript">
ClubData = '<?=$ClubAttendanceDataArr?>';
ActData = '<?=$ActivityAttendanceDataArr?>';

var data;
if (ClubData == ''){
	data = JSON.parse(ActData);
	number = '<?=$NumberOfActMeeting?>';
}else{
	data = JSON.parse(ClubData);
	number = '<?=$NumberofClubMeeting?>';
}
<? // Initialize lang for types of attandance data
	$attandanceLang = array(
		'chi' => array(
			'attend' => '出席人數',
			'exempt' => '豁免人數',
			'late' => '遲到人數',
			'el' => '早退人數'
		),
		'en' => array(
			'attend' => 'Attandance',
			'exempt' => 'Exempt',
			'late' => 'Late',
			'el' => 'Early Leave'
		),
	);
	$attandanceTitle = ($intranet_session_language=='en') ? $attandanceLang['en'] : $attandanceLang['chi'];
?>
// Find the Exempt Counts
var exemptdata = [];
for(var i=0; i<data.length;i++){
 exemptdata[i]  = [];
 exemptdata[i][0] = data[i][0];
 exemptdata[i][1] = data[i][2];
 data[i][1] - exemptdata[i][1];
}
// Find the Late Counts
if(data[0].length >= 4 && data[0][3] !=null){
	var latedata = [];
	for(var i=0; i<data.length;i++){
 		latedata[i]  = [];
 		latedata[i][0] = data[i][0];
 		latedata[i][1] = data[i][3];
 		data[i][1] - latedata[i][1];
	}
}
// Find the Early Leave Counts
if(data[0].length >= 5 && data[0][4] != null){
	var eldata = [];
	for(var i=0; i<data.length;i++){
 		eldata[i]  = [];
 		eldata[i][0] = data[i][0];
 		eldata[i][1] = data[i][4];
 		data[i][1] - eldata[i][1];
	}
}
var series = [];
var colors = ['grey', 'orange','skyblue'];
if(eldata!=null){
	series.push({
		name: '<?=$attandanceTitle['el']?>',
		data: eldata,
		legendIndex: 4
	});
	colors.push('green');
}
if(latedata!=null){
	series.push({
		name: '<?=$attandanceTitle['late']?>',
		data: latedata,
		legendIndex: 3
	});
	colors.push('brown');
}
series.push({
	name: '<?=$attandanceTitle['exempt']?>',
	data: exemptdata,
	legendIndex: 2
}); 
series.push({
	name: '<?=$attandanceTitle['attend']?>',
	data:  data,
	legendIndex: 1
});
if(number<10){
	number = 10;
}
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar',
            height: number*30,
        },
        title: {
            text: '<?=$eEnrollmentMenu['attendance_mgt_stu_stat']?>'
        },
        xAxis: {
            type: 'category',
      //      categories: TitleData, 
            labels: {
                rotation: 0,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            } //,
// 			title:{
//				text: '<?=$eEnrollmentMenu['act_date_time']?>',
// 				}      
        },
        yAxis: {
            min: 0,
            title: {
                text: '<?=$Lang['AccountMgmt']['NoOfStudents']?>',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
//         tooltip: {
//             valueSuffix: ' millions'
//         },
        plotOptions: {
            series: {
				stacking: true
            },
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
//         legend: {
//             layout: 'vertical',
//             align: 'right',
//             verticalAlign: 'top',
//             x: -40,
//             y: 20,
//             floating: true,
//             borderWidth: 1,
//             backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
//             shadow: true
//         },
        exporting: {
         enabled: false
        },
        credits: {
            enabled: false
        },
        series: series,
        colors : colors
    });
    console.log(data);
});
		</script>
	</head>
	<body>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>


<div id="container" style="min-width: 310px; max-width: 800px; height: 102%; margin: 0 auto">
</div>
<div class="edit_bottom_v30">
	<p class="spacer"></p>
		<?= $linterface->GET_ACTION_BTN($button_close, "button", "self.close();","cancelbtn") ?>
	<p class="spacer"></p>
</div>
<?
intranet_closedb();
$linterface->LAYOUT_STOP();
?>