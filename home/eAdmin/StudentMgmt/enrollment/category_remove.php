<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");


for ($i = 0; $i < sizeof($CategoryID); $i++) {
	$libenroll->DEL_CATEGORY($CategoryID[$i]);
}

intranet_closedb();
header("Location: category.php?msg=3");
?>