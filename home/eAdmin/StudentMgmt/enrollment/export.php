<?php
# using: 

#############################################
#
#   Date     2020-06-11 (Tommy)
#            [$type=9] modified format to only export target form student
#            [$type=13] modified format to only export target form student
#
#   Date     2020-03-17 (Tommy)
#            [$type=9] modified format to export max club student want Case #Q180165
#            added type 11 for only output student information and max club student want 
#
#   Date    2020-02-26 Tommy
#           [$type=0] added Term Performance export
#           [$type=6] added Term Performance export
#           [$type=12] added Term Performance export sample
#
#   Date    2020-01-20 Tommy
#           [$type=0] added 'Performance Adjusted Reason'
#
#	Date	2018-07-03 Pun [121822] [ip.2.5.9.7.1]
#			[$type=2] Added NCS cust
#
#	Date	2018-01-29 (Anna)
#			[$type=7] added belong to club info
#
#	Date	2017-06-01 (Anna)
#			[$type=6] add AcademicYearID
#
#	Date	2017-05-24 (Anna)
#			[$type== 1] add NickName column
#			[$type== 5] add NickNamecolumn
#
#	Date	2016-09-14 (Villa)
#			$type == 11 export activity pacticipant	List	
#
#	Date	2016-09-12 (Omas)
#			$type == 9 skip club that is not opened online  #L103192
#
#	Date	2016-07-05 (Omas)
#			$type == 7 add merit info
#
#	Date	2016-06-16 (Omas)
#			fix type = 8, Minor Merit => MinorMerit following import_member_update.php - #F97488 
#
#	Date	2016-02-29 (Kenneth)
#			added cust flag $sys_custom['eEnrolment']['ClubActivity']['export']['GroupCode'] to export Group Code for type = 6 / 7
#			deploy: ip.2.5.7.3.1
#
#	Date	2015-12-16 (Omas)
#			added activity merit - enableActivityRecordMeritInfoRight() to type 4
#
#	Date	2015-05-26 (Omas)
#			add type 10 export all activity application info case#J31222
#
#	Date	2015-04-08 (Omas)
#			type 8 import template add field userlogin
#			type 0,1,2,3,4,5,6,7,9 export - add field userlogin //P74036
#
#	Date	2015-01-30 (Omas)
#			Add type 9 export all club application info case#V74712
#
#	Date	2014-10-13 (Omas)
# 			Customization to export with TeacherName
#			[$type == 0] Add "Approved By" column
#           [$type == 6] Add "Approved By" column
#
#	Date	2014-07-28 (Bill)
#			add priority export to club applicants list
#
#	Date 	2013-01-02 (Rita)
#			add Merit for Customization (Type 0, Type 6)
#
#	Date 	2012-12-24 (Rita)
#			add Reason field for customization
#
#	Date	2012-03-30	(Ivan)
#			[$type == 0] Add "Achievement" column
#			[$type == 4] Add "Achievement" column
#			[$type == 6] Add "Achievement" column
#			[$type == 7] Add "Achievement" column
#
#	Date:	2011-08-04	(Henry Chow)
#			allow to export member list of activity on previous years
#
#	Date:	2011-05-24	YatWoon
#			[$type == 0] Add "Attendance" column
#			[$type == 4] Add "Attendance" column
#
#############################################

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libstudentprofile.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();


$LibUser = new libuser($UserID);
$lstudentprofile = new libstudentprofile();

$AcademicYearID = IntegerSafe($AcademicYearID);
$EnrolGroupID = IntegerSafe($EnrolGroupID);
$EnrolEventID = IntegerSafe($EnrolEventID);

if ($plugin['eEnrollment']) {
	global $sys_custom;

	include_once($PATH_WRT_ROOT."includes/libgroup.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libclubsenrol_ui.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable.php");
	include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
	
	
	$libenroll = new libclubsenrol($AcademicYearID);
	$libenroll_ui = new libclubsenrol_ui();
		
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    if (!isset($_GET)) {
			header("Location: index.php");
			exit();
		}
		
		$ExportArr = array();
		$lexport = new libexporttext();
		
		/*
		 *	$type == 0 => Club Member List (with Performance) 			[member_index.php]
		 *	$type == 1 => Activity Applicants List (with RecordStatus)	[event_member.php]
		 *	$type == 2 => Club Attendance 								[club_attendance_mgt.php]
		 *	$type == 3 => Activity Attendance							[event_attendance_mgt.php]
		 *  $type == 4 => Activity Student List (with Performance) 		[event_member_index.php]
		 *  $type == 5 => Club Applicants List (with RecordStatus) 		[group_member.php]
		 *  $type == 6 => All Club Member List					 		[group.php]
		 *  $type == 7 => All Activity Participant Member List			[event.php]
		 * 	$type == 8 => Import Club Member sample file				[import.php]
		 *  $type == 9 => export all Club Applicants List with priority [club_status.php]
		 *  $type == 10 => export all Activity Applicants List			[event_status.php]
		 *  $type == 11 => export activity pacticipant	List			[impot.php]
		 */
		 
		if ($type == 0){	//club information
		
			if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID)) && (!$ligroup->hasAdminBasicInfo($UserID))  && (!$libenroll->isIntranetGroupAdminWithMgmtUserRight($EnrolGroupID))) {
				No_Access_Right_Pop_Up();
			}
		
			$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);	
			
			$li = new libgroup($GroupID);	
			$title = str_replace(' ', '_', $li->Title);
			$filename = "member_list_of_".$title;
			//$filename = iconv("UTF-8", "Big5", $filename);
			$filename = $filename.".csv";
			
			# Show active member status and attendance
			//$activeMemberArr = $libenroll->Get_Active_Member_Arr($GroupID, "club", 1);
			//$AttendanceArr = $libenroll->Get_Student_Attendance_Arr($GroupID, "club", 1);
		
			//$sql = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['ExportSQL']));
			
			# TABLE INFO
			$li = new libdbtable2007($field, $order, $pageNo);	
			
			$sql = $libenroll->Get_Management_Club_Member_Sql($GroupID, $EnrolGroupID, $filter, $keyword, $ForExport=0, $AcademicYearID, $approvedByType);
			if ($filter == 2) {	
				$li->field_array = array("iu.ClassName", "iu.ClassNumber","UserLogin", "StudentName", "ir.Title", "Performance", "Achievement", "Comment", "ActiveMemberStatus", "iu.UserID");
				$li->fieldorder2 = ", iu.ClassNumber";
			}
			else {
				$li->field_array = array("StudentName", "iegs.StaffType", "iu.UserID");
				$li->fieldorder2 = ", iu.ClassNumber";
			}
			
			if ($keyword == $eEnrollment['enter_name']) $keyword = "";
			
			### Record the sql for export first	
			$li->sql = $libenroll->Get_Management_Club_Member_Sql($GroupID, $EnrolGroupID, $filter, $keyword, $ForExport=1, $AcademicYearID, $approvedByType);
			//echo $li->sql.'<br>';
			$ExportSQLTemp = $li->built_sql();
			$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
			$sql = substr($ExportSQLTemp, 0, $LimitIndex);
			
			$row = $libenroll->returnArray($sql);
            //define column title
            
            $exportColumn = array();
            
            $termAry = $libenroll->getTermColumn($AcademicYearID);
            $numOfTerm = count($termAry);
            
            if ($filter == 2)
			{
				$exportColumn[] = "ClassName";
				$exportColumn[] = "ClassNumber";
				$exportColumn[] = "UserLogin";
				$exportColumn[] = "StudentName";
				$exportColumn[] = "Role";
				$exportColumn[] = "Attendance";
				if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
				    for ($i = 0; $i < $numOfTerm; $i ++) {
				        $exportColumn[] = str_replace(' ', '', $termAry[$i]["TermName"] . "Performance");
				    }
				}
				$exportColumn[] = "Performance";
				if($sys_custom['eEnrolment']['Refresh_Performance'])
				    $exportColumn[] = "Performance Adjusted Reason";
				$exportColumn[] = "Achievement";
				$exportColumn[] = "Comment";
				$exportColumn[] = "Active/Inactive";
				$exportColumn[] = "AdminRole";
								
				if($libenroll->enableClubRecordMeritInfoRight()){
					if (!$lstudentprofile->is_merit_disabled) {
						$exportColumn[] = "Merit";
					}
					if (!$lstudentprofile->is_min_merit_disabled) {
						$exportColumn[] = "Minor Merit";
					}
					if (!$lstudentprofile->is_maj_merit_disabled) {
						$exportColumn[] = "Major Merit";
					}
					if (!$lstudentprofile->is_sup_merit_disabled) {
						$exportColumn[] = "Super Merit";
					}
					if (!$lstudentprofile->is_ult_merit_disabled) {	
						$exportColumn[] = "Ultra Merit";
					}			
				}				
				
				if($sys_custom['eEnrolment']['ClubExportWithTeacherName'])
				{
					$exportColumn[] = "ApprovedBy";
				}

			}
			else
			{
				$exportColumn[] = $i_UserName;
				$exportColumn[] = $i_admintitle_role;
				
			}
            
            // Create data array for export
			$ExportArr = array();
			
			for($i=0; $i<sizeof($row); $i++){
			    $termPerformanceAry = $libenroll->getTermPerformance($AcademicYearID, $EnrolGroupID, $row[$i]["UserID"]);

				if ($filter == 2)
				{
					$ExportArr[$i][] = $row[$i]["ClassName"];
					$ExportArr[$i][] = $row[$i]["ClassNumber"];
					$ExportArr[$i][] = $row[$i]["UserLogin"];
					$ExportArr[$i][] = $row[$i]["StudentName"];
					$ExportArr[$i][] = $row[$i]["Title"];
					
					## Attendance
					$DataArr['StudentID'] = $row[$i]['UserID'];
					$DataArr['EnrolGroupID'] = $EnrolGroupID;
					$this_attendance = $libenroll->GET_GROUP_STUDENT_ATTENDANCE($DataArr);
					$ExportArr[$i][] = $this_attendance;
					if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']){
					    for($term = 0; $term < sizeof($termPerformanceAry); $term++){
					        if(sizeof($termPerformanceAry[$term]) > 0){
					            $ExportArr[$i][] = $termPerformanceAry[$term][0];
					        }else{
					            $ExportArr[$i][] = "";
					        }
					    }
					}
					$ExportArr[$i][] = $row[$i]["Performance"];
					if($sys_custom['eEnrolment']['Refresh_Performance']){
					    if($row[$i]["AdjustReason"] == '--')
					        $ExportArr[$i][] = "";
					    else
					        $ExportArr[$i][] = $row[$i]["AdjustReason"];
					}
					$ExportArr[$i][] = $row[$i]["Achievement"];
					$ExportArr[$i][] = $row[$i]["Comment"];
					$ExportArr[$i][] = $row[$i]["ActiveMemberStatus"];
					$ExportArr[$i][] = '';
						
		
					if($libenroll->enableClubRecordMeritInfoRight()){
						if (!$lstudentprofile->is_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["merit"];
						}
						if (!$lstudentprofile->is_min_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["minorMerit"];
						}
						if (!$lstudentprofile->is_maj_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["majorMerit"];
						}
						if (!$lstudentprofile->is_sup_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["superMerit"];
						}
						if (!$lstudentprofile->is_ult_merit_disabled) {	
							$ExportArr[$i][] = $row[$i]["ultraMerit"];
						}
					}
					if($sys_custom['eEnrolment']['ClubExportWithTeacherName'])
					{
						$ExportArr[$i][] = $row[$i]["TeacherName"];
					}
				}
				else
				{
					$ExportArr[$i][] = $row[$i]["StudentName"];
					$ExportArr[$i][] = $row[$i]["StaffType"];
					
				}
			}
		}
		else if ($type == 1){	//activity information		
				
			$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
			//$title = intranet_undo_htmlspecialchars($EventInfoArr[3]);	//$EventInfoArr[3] is the title of activity
			$title = str_replace(' ', '_', $EventInfoArr[3]);
			$filename = "information_of_".$title;
			//$filename = iconv("UTF-8", "Big5", $filename);
			$filename = $filename.".csv";
			
			$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID);
			
			/*
			$field_array = array("Class","a.EnglishName","a.ChineseName","b.RecordStatus","b.DateInput");
				
			
			if ($filter != -1)
			{
			    $conds = "AND b.RecordStatus = $filter";
			}
			else
			{
			    $conds = "";
			}
			            
			
            if ((($field !== 0) && ($field == "")) || $field >= sizeof($field_array))
            {
                $order_str = "Class";
            }
            else
            {
	            $order_str = $field_array[$field];
            }
            
            $order += 0;
            if ($order == 0)
            {
                $order_type = " DESC";
            }
            else
            {
                $order_type = " ASC";
            }
            $order_str = str_replace(",","$order_type,",$order_str);
            $order_str .= $order_type . " , b.EventStudentID";     
                   
            $ClassNameField = Get_Lang_Selection('yc.ClassTitleB5', 'yc.ClassTitleEN');
			$sql = "SELECT
							IF(ycu.ClassNumber IS NULL OR ycu.ClassNumber = '', '-', CONCAT($ClassNameField,' (',ycu.ClassNumber,')')) as Class,
							a.EnglishName,
							a.ChineseName,
							CASE b.RecordStatus
							     WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
							     WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
							     WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
							END,
							b.DateInput,
							CONCAT('<input type=checkbox name=uid[] value=', a.UserID ,'>'),
							b.ApprovedBy
					FROM 
							INTRANET_USER as a
							Inner Join
							INTRANET_ENROL_EVENTSTUDENT as b
							On (a.UserID = b.StudentID)
							Inner Join
							YEAR_CLASS_USER as ycu
							On (a.UserID = ycu.UserID)
							Inner Join
							YEAR_CLASS as yc
							On (ycu.YearClassID = yc.YearClassID)
					WHERE 
							b.EnrolEventID = '$EnrolEventID'
							AND
							(a.EnglishName LIKE '%".$keyword."%' OR a.ChineseName LIKE '%".$keyword."%')
							And
							a.RecordType = 2
							And
							a.RecordStatus = 1
							And
							ycu.ClassNumber is not null
							And
							yc.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
							$conds
			        ";
	        $sql .= " ORDER BY ".$order_str;
			*/
			
			//$sql = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['ExportSQL']));
			
			
			if ($keyword == $eEnrollment['enter_student_name']) $keyword = "";

			if ($field=="" || !isset($field))
			{
				$field = 0;
			}
			
			switch ($field){
				case 0: $field = 0; break;
				case 1: $field = 1; break;
				case 2: $field = 2; break;
				case 3: $field = 3; break;
				case 4: $field = 4; break;
				case 5: $field = 5; break;
				default: $field = 0; break;
			}
			if ($order=="" && !isset($order))
			{
				$order = ($order == 0) ? 1 : 0;
			}
			if (!isset($filter) || $filter=="") {
				$filter = 2;
			}
			
			
			# TABLE INFO
			$li = new libdbtable2007($field, $order, $pageNo);
			
			$li->field_array = array();
			$li->field_array[] = "ClassName";
			$li->field_array[] = "ClassNumber";
			$li->field_array[] = "UserLogin";
			$li->field_array[] = "EnglishName";
			$li->field_array[] = "ChineseName";
			if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
				$li->field_array[] = "NickName";
			}
			$li->field_array[] = "iees.RecordStatus";
			$li->field_array[] = "iees.DateInput";
			if ($libenroll->enableActivitySurveyFormRight()) {
				$li->field_array[] = "SurveyResult";
			}
			if ($libenroll->enableActivityFillInEnrolReasonRight()) {
				$li->field_array[] = "Reason";
			}
			$li->field_array[] = "iu.UserID";
			
			$li->fieldorder2 = ", iees.EventStudentID";
			
			$li->sql = $libenroll->Get_Management_Activity_Participant_Enrollment_Sql(array($EnrolEventID), array($filter), $keyword, $ForExport=1);
			$ExportSQLTemp = $li->built_sql();
			$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
			$sql = substr($ExportSQLTemp, 0, $LimitIndex);
			
			//$result = $libenroll->returnArray($sql, 5);
			$result = $libenroll->returnResultSet($sql);
			$NumOfResult = count($result);

			for($i=0; $i<$NumOfResult; $i++){	 
//				$ExportArr[$i][0] = $result[$i][0];	//className,classNumber
//				$ExportArr[$i][1] = $result[$i][1];	//English Name
//				$ExportArr[$i][2] = $result[$i][2];	//Chinese Name
//				$ExportArr[$i][3] = $result[$i][3];	//Status
//				$ExportArr[$i][4] = $result[$i][4];	//Last Submission Time
				$col = 0;
				$ExportArr[$i][$col++] = $result[$i]['ClassName'];//className,classNumber
				$ExportArr[$i][$col++] = $result[$i]['ClassNumber'];
				$ExportArr[$i][$col++] = $result[$i]['UserLogin'];	
				$ExportArr[$i][$col++] = $result[$i]['EnglishName'];	//English Name
				$ExportArr[$i][$col++] = $result[$i]['ChineseName'];	//Chinese Name
				if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
					$ExportArr[$i][$col++] = $result[$i]['NickName'];
				}
				$ExportArr[$i][$col++] = $result[$i]['RecordStatus'];	//Status
				$ExportArr[$i][$col++] = $result[$i]['DateInput'];	//Last Submission Time
	
				# Enrol Reason Customization 
				if($libenroll->enableActivityFillInEnrolReasonRight()){
					//$ExportArr[$i][5] = $result[$i][6] ;	// Reason
					$ExportArr[$i][$col++] = $result[$i]['Reason'];	//Reason
	
				}
				
			}
			
			//define column title
			$exportColumn = array();
			$exportColumn[] = $i_general_class;
			$exportColumn[] = $i_ClassNumber;
			$exportColumn[] = $Lang['AccountMgmt']['StudentImportFields']['0'];
			$exportColumn[] = $i_UserEnglishName;
			$exportColumn[] = $i_UserChineseName;
			if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
				$exportColumn[] = $i_UserNickName;
			}
			$exportColumn[] = $i_general_status;
			$exportColumn[] = $i_ClubsEnrollment_LastSubmissionTime;
			
			if($libenroll->enableActivityFillInEnrolReasonRight()){
				$exportColumn[] = $Lang['eEnrolment']['Reason'];
			}
		}
//		else if ($type == 2){	//club attendance
//			$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
//			$li = new libgroup($GroupID);
//			//$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
//			//$title = intranet_undo_htmlspecialchars($li->Title);
//			$title = str_replace(' ', '_', $li->Title);
//			$filename = "attendance_record_of_".$title;
//			//$filename = iconv("UTF-8", "Big5", $filename);
//			$filename = $filename.".csv";
//			
//			// get date data from the db and make the array
//			$DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);	//GroupDateID, EnrolGroupID, ActivityDateStart, ActivityDateEnd
//			
//			// construct a date title array
//			for ($i = 0; $i < sizeof($DateArr); $i++) {
//				$DateTitleArr[$i] .= date("Y-m-d", strtotime($DateArr[$i][2]));
//				$DateTitleArr[$i] .= " ";
//				$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][2]));
//				$DateTitleArr[$i] .= "-";
//				$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][3]));
//			}
//			
//			// get members information
//			/*
//			$sql = "SELECT b.UserID, b.EnglishName, b.ChineseName, b.ClassName, b.ClassNumber
//					FROM INTRANET_USERGROUP as a LEFT JOIN INTRANET_USER as b ON a.UserID = b.UserID
//					WHERE (a.EnrolGroupID = ".$EnrolGroupID." AND b.RecordType = 2 and b.RecordStatus = 1 and b.ClassName Is Not Null and b.ClassNumber Is Not Null)
//					ORDER BY b.ClassName, b.ClassNumber
//					";
//			$StuArr = $libenroll -> returnArray($sql, 3);
//			*/
//			$StuArr = $libenroll->Get_Club_Member_Info($EnrolGroupID, $PersonTypeArr=array(2), $AcademicYearID, $IndicatorArr=array('InactiveStudent'), $IndicatorWithStyle=0, $WithEmptySymbol=0, $ReturnAsso=0);
//			$numOfMember = count($StuArr);
//								
//			$totalAttendance = array();
//			// calculate the attendance
//			for ($i = 0; $i < $numOfMember; $i++) {
//				$totalMeeting = 0;
//				$attended = 0;
//				$totalhours = 0;
//				
//				// get the attendance record for each date and calculate the attendance percentage
//				for ($j = 0; $j < sizeof($DateArr); $j++) {
//					$attend = $libenroll->Get_Group_Attendance_Status($DateArr[$j][0],$StuArr[$i]['UserID'],$EnrolGroupID);				
//						
//					$data[0][0] = Date("H:i",strtotime($DateArr[$j]["ActivityDateStart"]));
//					$data[0][1] = Date("H:i",strtotime($DateArr[$j]["ActivityDateEnd"]));
//					$thisHour = $libenroll->GET_TOTAL_HOURS($data);
//					
//					if (date("Y-m-d", strtotime($DateArr[$j][2])) <= date("Y-m-d")) {
//						$totalMeeting++;					
//						$totalArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//						if (in_array($attend, array(ENROL_ATTENDANCE_PRESENT, ENROL_ATTENDANCE_EXEMPT))) {
//							$attended++;
//							$totalhours+=$thisHour;
//							$StuArr[$i]['Attended'][$j] = $attend;
//							$addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//						}
//						else
//						{
//							$StuArr[$i]['Attended'][$j] = $attend;
//						}
//					}
//					else
//					{
//						// future event
//						$StuArr[$i]['Attended'][$j] = 'f';	
//					}
//				}
//				$StuArr[$i]['Attendance'] = (($totalMeeting > 0)&&($attended > 0))? my_round(($attended / $totalMeeting * 100), 2)."%" : $StuArr[$i]['Attendance'] = "0%";
//				$StuArr[$i]['AttendHours'] = $totalhours;
//				$totalAttendance[] = (($totalMeeting > 0)&&($attended > 0))? ($attended / $totalMeeting * 100) : 0;
//				
//				# show "-" for future event
//				if (date("Y-m-d", strtotime($DateArr[$j][2])) > date("Y-m-d")) 
//				{ 
//					$StuArr[$i]['Attendance'] = $Lang['General']['EmptySymbol'];
//				}
//			}
//			
//			for ($j = 0; $j < sizeof($DateArr); $j++) {
//				if ( ($totalArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0) &&
//					 ($addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0))
//				{
//					$TmpDateAttendance = $addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] / $totalArr[date("Ymd", strtotime($DateArr[$j][2]))];
//					$DateAttendance[$j] = my_round(($TmpDateAttendance * 100), 2);
//					$DateAttendance[$j] .= "%";
//				} else {
//					$DateAttendance[$j] = "0%";
//				}
//				
//				# show "-" for future event
//				if (date("Y-m-d", strtotime($DateArr[$j][2])) > date("Y-m-d")) 
//				{ 
//					$DateAttendance[$j] = $Lang['General']['EmptySymbol'];
//				}
//			}
//			
//			// organize the results in an array
//			for($i=0; $i<$numOfMember; $i++){
//				
//				# updated on 19 Mar 2009 by Ivan - separate the class name and class number field
//				// add class name and class number to the name field
//				/*
//				if (($StuArr[$i]['ClassName']!=NULL || $StuArr[$i]['ClassName']!="") && ($StuArr[$i]['ClassNumber']!=NULL || $StuArr[$i]['ClassNumber']!="")){
//					$ExportArr[$i][0] .= "(".$StuArr[$i]['ClassName']."-".$StuArr[$i]['ClassNumber'].")";
//				}
//				*/
//				$ExportArr[$i][0] = $StuArr[$i]['ClassName'];
//				$ExportArr[$i][1] = $StuArr[$i]['ClassNumber'];
//				
//				/*
//				if ($intranet_session_language == "en"){	//current language is Eng
//					if ($StuArr[$i]['EnglishName'] == NULL || $StuArr[$i]['EnglishName'] == ""){	//no English Name => show Chinese
//						$ExportArr[$i][2] = $StuArr[$i]['ChineseName'];
//					}
//					else{
//						$ExportArr[$i][2] = $StuArr[$i]['EnglishName'];
//					}
//				}
//				else{
//					if ($StuArr[$i]['ChineseName'] == NULL || $StuArr[$i]['ChineseName'] == ""){	//no English Name => show Chinese
//						$ExportArr[$i][2] = $StuArr[$i]['EnglishName'];
//					}
//					else{
//						$ExportArr[$i][2] = $StuArr[$i]['ChineseName'];
//					}
//				}
//				*/
//				$ExportArr[$i][2] = $StuArr[$i]['StudentName'];
//				
//				$startIndex = 3;
//				for($j=0; $j<sizeof($DateTitleArr); $j++){
//					if ($StuArr[$i]['Attended'][$j] == 'f' || $StuArr[$i]['Attended'][$j] == ''){
//						// future event
//						$ExportArr[$i][$j+$startIndex] = $Lang['General']['EmptySymbol'];
//					}
//					elseif ($StuArr[$i]['Attended'][$j] == 1){
//						$ExportArr[$i][$j+$startIndex] = $Lang['eEnrolment']['Attendance']['Present'];
//					}
//					elseif ($StuArr[$i]['Attended'][$j] == 2){
//						$ExportArr[$i][$j+$startIndex] = $Lang['eEnrolment']['Attendance']['Exempt'];
//					}
//					elseif ($StuArr[$i]['Attended'][$j] == 3){
//						$ExportArr[$i][$j+$startIndex] = $Lang['eEnrolment']['Attendance']['Absent'];
//					}
//				}
//				
//				$ExportArr[$i][sizeof($DateTitleArr)+$startIndex] = $StuArr[$i]['Attendance'];
//				$ExportArr[$i][sizeof($DateTitleArr)+$startIndex+1] = $StuArr[$i]['AttendHours'];
//			}
//			
//			// add last row showing Activity Attendance Summary
//			$ExportArr[$i][0] = $eEnrollment['act_attendence'];
//			
//			for ($j=1; $j<$startIndex; $j++) {
//				$ExportArr[$i][$j] = '';
//			}
//			for ($j = 0; $j < sizeof($DateAttendance); $j++) {
//				$ExportArr[$i][$j+$startIndex] = $DateAttendance[$j];
//			}
//			
//			// Average Attendance
//			$ExportArr[$i][sizeof($DateAttendance)+$startIndex] = $libenroll->getAverage($totalAttendance, 2, "", "%");
//			
//			//define column title
//			$exportColumn[] = "ClassName";
//			$exportColumn[] = "ClassNumber";
//			$exportColumn[] = "StudentName";
//						
//			for ($i = 0 ; $i < sizeof($DateTitleArr); $i++) {
//				$exportColumn[] = $DateTitleArr[$i];
//			}
//			
//			$exportColumn[] = "Attendance Rate (Up to ".date("Y-m-d").")";
//			$exportColumn[] = "Attendance Hour (Up to ".date("Y-m-d").")";
//			
//		}
//		else if ($type == 3){	//activity attendance
//			//set the output file name
//			$Sql = "	SELECT
//									EventTitle
//						FROM
//									INTRANET_ENROL_EVENTINFO
//						WHERE
//									EnrolEventID = '".$EnrolEventID."'
//					";
//			$result = $libenroll->returnArray($Sql, 1);
//			//$title = intranet_undo_htmlspecialchars($result[0][0]);
//			$title = str_replace(' ', '_', $result[0][0]);
//			$filename = "attendance_record_of_".$title;
//			//$filename = iconv("UTF-8", "Big5", $filename);
//			$filename = $filename.".csv";
//		
//			// get date data from the db and make the array
//			$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EnrolEventID);
//			
//			// construct a date title array
//			for ($i = 0; $i < sizeof($DateArr); $i++) {
//				$DateTitleArr[$i] .= date("Y-m-d", strtotime($DateArr[$i][2]));
//				$DateTitleArr[$i] .= " ";
//				$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][2]));
//				$DateTitleArr[$i] .= "-";
//				$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i][3]));
//			}
//			/*
//			$sql = "
//						SELECT
//								a.UserID, a.EnglishName, a.ChineseName, a.ClassName, a.ClassNumber
//						FROM
//								INTRANET_USER as a, INTRANET_ENROL_EVENTSTUDENT as b
//						WHERE
//								a.UserID = b.StudentID
//								AND
//								b.EnrolEventID = '".$EnrolEventID."'
//								AND
//								b.RecordStatus = 2
//								AND 
//								a.RecordType = 2
//								and
//								a.RecordStatus = 1
//								and
//								a.ClassName Is Not Null
//								and 
//								a.ClassNumber Is Not Null
//						ORDER BY
//								a.ClassName, a.ClassNumber
//					";
//			$StuArr = $libenroll -> returnArray($sql, 3);
//			*/
//			$ActivityInfoArr = $libenroll->Get_Activity_Student_Enrollment_Info(array($EnrolEventID), $ActivityKeyword='', Get_Current_Academic_Year_ID(), $WithNameIndicator=1, $IndicatorWithStyle=0, $WithEmptySymbol=0);
//			$StuArr = (array)$ActivityInfoArr[$EnrolEventID]['StatusStudentArr'][2];
//			$numOfStudent = count($StuArr);
//			
//			$totalAttendance = array();
//			// calculate the attendance
//			for ($i = 0; $i < sizeof($StuArr); $i++) {
//				$totalMeeting = 0;
//				$attended = 0;
//				$totalhours = 0;
//			
//				// get the attendance record for each date and calculate the attendance percentage
//				for ($j = 0; $j < sizeof($DateArr); $j++) {
//					$attend = $libenroll->Get_Event_Attendance_Status( $DateArr[$j][0],$StuArr[$i]['UserID']);				
//					
//					$data[0][0] = Date("H:i",strtotime($DateArr[$j]["ActivityDateStart"]));
//					$data[0][1] = Date("H:i",strtotime($DateArr[$j]["ActivityDateEnd"]));
//					$thisHour = $libenroll->GET_TOTAL_HOURS($data);
//					
//					if (date("Y-m-d", strtotime($DateArr[$j][2])) <= date("Y-m-d")) {
//						$totalMeeting++;					
//						$totalArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//						if (in_array($attend, array(ENROL_ATTENDANCE_PRESENT, ENROL_ATTENDANCE_EXEMPT))) {
//							$attended++;
//							$totalhours+=$thisHour;
//							$StuArr[$i]['Attended'][$j] = $attend;
//							$addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))]++;
//						}
//						else
//						{
//							$StuArr[$i]['Attended'][$j] = $attend;
//						}
//					}
//					else
//					{
//						// future event
//						$StuArr[$i]['Attended'][$j] = 'f';	
//					}
//				}
//				$StuArr[$i]['Attendance'] = (($totalMeeting > 0)&&($attended > 0))? my_round(($attended / $totalMeeting * 100), 2)."%" : $StuArr[$i]['Attendance'] = "0%";
//				$StuArr[$i]['AttendHours'] = $totalhours; 
//				$totalAttendance[] = (($totalMeeting > 0)&&($attended > 0))? ($attended / $totalMeeting * 100) : 0;
//				
//				# show "-" for future event
//				if (date("Y-m-d", strtotime($DateArr[$j][2])) > date("Y-m-d")) 
//				{ 
//					$StuArr[$i]['Attendance'] = "-";
//				}
//			}
//				
//			for ($j = 0; $j < sizeof($DateArr); $j++) {
//				if ( ($totalArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0) &&
//					 ($addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] > 0))
//				{
//					$TmpDateAttendance = $addtendedArr[date("Ymd", strtotime($DateArr[$j][2]))] / $totalArr[date("Ymd", strtotime($DateArr[$j][2]))];
//					$DateAttendance[$j] = my_round(($TmpDateAttendance * 100), 2);
//					$DateAttendance[$j] .= "%";
//				} else {
//					$DateAttendance[$j] = "0%";
//				}
//				
//				# show "-" for future event
//				if (date("Y-m-d", strtotime($DateArr[$j][2])) > date("Y-m-d")) 
//				{ 
//					$DateAttendance[$j] = "-";
//				}
//			}
//				
//			// organize the results in an array
//			for($i=0; $i<sizeof($StuArr); $i++){
//				
//				$ExportArr[$i][0] = $StuArr[$i]['ClassName'];
//				$ExportArr[$i][1] = $StuArr[$i]['ClassNumber'];
//				
//				/*
//				if ($intranet_session_language == "en"){	//current language is Eng
//					if ($StuArr[$i]['EnglishName'] == NULL || $StuArr[$i]['EnglishName'] == ""){	//no English Name => show Chinese
//						$ExportArr[$i][2] = $StuArr[$i]['ChineseName'];
//					}
//					else{
//						$ExportArr[$i][2] = $StuArr[$i]['EnglishName'];
//					}
//				}
//				else{
//					if ($StuArr[$i]['ChineseName'] == NULL || $StuArr[$i]['ChineseName'] == ""){	//no English Name => show Chinese
//						$ExportArr[$i][2] = $StuArr[$i]['EnglishName'];
//					}
//					else{
//						$ExportArr[$i][2] = $StuArr[$i]['ChineseName'];
//					}
//				}
//				*/
//				$ExportArr[$i][2] = $StuArr[$i]['StudentName'];
//				
//				// add class name and class number to the name field
//				/*
//				if (($StuArr[$i]['ClassName']!=NULL || $StuArr[$i]['ClassName']!="") && ($StuArr[$i]['ClassNumber']!=NULL || $StuArr[$i]['ClassNumber']!="")){
//					$ExportArr[$i][0] .= "(".$StuArr[$i]['ClassName']."-".$StuArr[$i]['ClassNumber'].")";
//				}				
//				*/
//				
//				$startIndex = 3;
//				for($j=0; $j<sizeof($DateTitleArr); $j++){
//					if ($StuArr[$i]['Attended'][$j] == 'f' || $StuArr[$i]['Attended'][$j] == ''){
//						// future event
//						$ExportArr[$i][$j+$startIndex] = $Lang['General']['EmptySymbol'];
//					}
//					elseif ($StuArr[$i]['Attended'][$j] == 1){
//						$ExportArr[$i][$j+$startIndex] = $Lang['eEnrolment']['Attendance']['Present'];
//					}
//					elseif ($StuArr[$i]['Attended'][$j] == 2){
//						$ExportArr[$i][$j+$startIndex] = $Lang['eEnrolment']['Attendance']['Exempt'];
//					}
//					elseif ($StuArr[$i]['Attended'][$j] == 3){
//						$ExportArr[$i][$j+$startIndex] = $Lang['eEnrolment']['Attendance']['Absent'];
//					}
//				}
//				
//				$ExportArr[$i][sizeof($DateTitleArr)+$startIndex] = $StuArr[$i]['Attendance'];
//				$ExportArr[$i][sizeof($DateTitleArr)+$startIndex+1] = $StuArr[$i]['AttendHours'];
//				
//			}
//				
//			// add last row showing Activity Attendance Summary
//			$ExportArr[$i][0] = $eEnrollment['act_attendence'];
//			for ($j=1; $j<$startIndex; $j++) {
//				$ExportArr[$i][$j] = '';
//			}
//			for ($j = 0; $j < sizeof($DateAttendance); $j++) {
//				$ExportArr[$i][$j+$startIndex] = $DateAttendance[$j];
//			}
//			
//			// Average Attendance
//			$ExportArr[$i][sizeof($DateAttendance)+$startIndex] = $libenroll->getAverage($totalAttendance, 2, "", "%");
//			
//									
//			//define column title
//			$exportColumn[] = "ClassName";
//			$exportColumn[] = "ClassNumber";
//			$exportColumn[] = "StudentName";
//						
//			for ($i = 0 ; $i < sizeof($DateTitleArr); $i++) {
//				$exportColumn[] = $DateTitleArr[$i];
//			}
//			
//			$exportColumn[] = "Attendance Rate (Up to ".date("Y-m-d").")";
//			$exportColumn[] = "Attendance Hour (Up to ".date("Y-m-d").")";
//
//		}
		else if ($type == 2 || $type == 3){	// club or activity attendance
			$forSample = $_GET['forSample'];
			
			if ($type == 2) {
				// club
				
				$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
				$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
				$isClubPIC = $libenroll->IS_CLUB_PIC($EnrolGroupID);
				$isClubHelper = $libenroll->IS_CLUB_HELPER($EnrolGroupID);
				
				## temp
				$AcademicYearID = $AcademicYearID ? $AcademicYearID : Get_Current_Academic_Year_ID();
				if (!$isEnrolAdmin && !$isEnrolMaster && !$isClubPIC && !$isClubHelper)
					$canEdit = 0;
				else
					$canEdit = 1;
				
				if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
				{
					No_Access_Right_Pop_Up();
				}
				
				$recordId = IntegerSafe($_GET['EnrolGroupID']);
				$dateIdFieldName = 'GroupDateID';
				
				$groupId = $libenroll->GET_GROUPID($recordId);
				$lgroup = new libgroup($groupId);
				$title = str_replace(' ', '_', $lgroup->Title);
				$filename = "club_attendance_".$title.".csv";
				
				$meetingDateInfoAry = $libenroll->GET_ENROL_GROUP_DATE($recordId);
				$studentInfoAry = $libenroll->Get_Club_Member_Info($recordId, array(USERTYPE_STUDENT));
				$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
				$memberAttendanceInfoArr = $libenroll->Get_Student_Club_Attendance_Info($studentIdAry, array($recordId));
				$overallAttendanceInfoArr = $libenroll->Get_Club_Attendance_Info($recordId, "", $AcademicYearID);
				
				$recordTypeCode = $enrolConfigAry['Club'];
			}
			else if ($type == 3) {
				// activity
				$isEnrolAdmin = $libenroll->IS_ENROL_ADMIN($_SESSION['UserID']);
				$isEnrolMaster = $libenroll->IS_ENROL_MASTER($_SESSION['UserID']);
				$isActivityPIC = $libenroll->IS_EVENT_PIC($EnrolEventID);
				$isActivityHelper = $libenroll->IS_EVENT_HELPER($EnrolEventID);
				
				$EventInfo = $libenroll->GET_EVENTINFO($EnrolEventID);
				$ActivityEnrolGroupID = $EventInfo['EnrolGroupID'];
				$IsActivityClubPIC = $libenroll->IS_CLUB_PIC($ActivityEnrolGroupID);
				
				
				if (!$isEnrolAdmin && !$isEnrolMaster && !$isActivityPIC && !$isActivityHelper && !$IsActivityClubPIC)
					$canEdit = false;
				else
					$canEdit = true;
				
				
				if ($plugin['eEnrollmentLite'] || $libenroll->hasAccessRight($_SESSION['UserID']) == false || ($action!="view" && $canEdit==false))
				{
					No_Access_Right_Pop_Up();
				}
				
				$filename = "activity_attendance.csv";
				$recordId = IntegerSafe($_GET['EnrolEventID']);
				$dateIdFieldName = 'EventDateID';
				
				$activityInfoAssoAry = $libenroll->Get_Activity_Info_By_Id($recordId);
				$academicYearId = $activityInfoAssoAry[$recordId]['AcademicYearID'];
				$activityTitle = str_replace(' ', '_', $activityInfoAssoAry[$recordId]['EventTitle']);
				$filename = "activity_attendance_".$activityTitle;
				$filename = $filename.".csv";
				
				$meetingDateInfoAry = $libenroll->GET_ENROL_EVENT_DATE($recordId);
				$studentInfoAry = $libenroll->Get_Activity_Participant_Info($recordId, $academicYearId);
				$studentIdAry = Get_Array_By_Key($studentInfoAry, 'UserID');
				$memberAttendanceInfoArr = $libenroll->Get_Student_Activity_Attendance_Info($studentIdAry, array($recordId));
				$overallAttendanceInfoArr = $libenroll->Get_Activity_Attendance_Info($recordId, "", $AcademicYearID);
				
				$recordTypeCode = $enrolConfigAry['Activity'];
			}
			
			if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
				include_once($PATH_WRT_ROOT."includes/libclubsenrol_cust.php");
				$libenroll_cust = new libclubsenrol_cust();
				$extraInfoAry = $libenroll_cust->Get_Student_Extra_Info($recordTypeCode, $studentIdAry);
				$extraInfoAssoAry = BuildMultiKeyAssoc($extraInfoAry, 'StudentID');
				unset($extraInfoAry);
			}
			
			$numOfMeeting = count($meetingDateInfoAry);
			$numOfStudent = count($studentIdAry);
			
			$iCounter = 0;
			$jCounter = 0;
			for ($i=0; $i<$numOfStudent; $i++) {
				$_studentId = $studentInfoAry[$i]['UserID'];
				$_studentName = Get_Lang_Selection($studentInfoAry[$i]['ChineseName'], $studentInfoAry[$i]['EnglishName']);
				$_className = $studentInfoAry[$i]['ClassName'];
				$_classNumber = $studentInfoAry[$i]['ClassNumber'];
				$_userLogin = $studentInfoAry[$i]['UserLogin'];
				$_attendancePercentage = $memberAttendanceInfoArr[$_studentId][$recordId]['AttendancePercentageRounded'];
				$_attendanceHours = $memberAttendanceInfoArr[$_studentId][$recordId]['Hours'];
				
				
				$jCounter = 0;
				
				$ExportArr[$iCounter][$jCounter++] = $_className;
				$ExportArr[$iCounter][$jCounter++] = $_classNumber;
				$ExportArr[$iCounter][$jCounter++] = $_userLogin;
				
				if (!$forSample) {
					$ExportArr[$iCounter][$jCounter++] = $_studentName;
				}
				
				for ($j=0; $j<$numOfMeeting; $j++) {
					$__dateId = $meetingDateInfoAry[$j][$dateIdFieldName];
					$__attendanceStatus = $memberAttendanceInfoArr[$_studentId][$recordId][$__dateId]['RecordStatus'];
					$__attendanceRemark = $memberAttendanceInfoArr[$_studentId][$recordId][$__dateId]['Remark'];
					
					$ExportArr[$iCounter][$jCounter++] = ($forSample)? '' : $libenroll_ui->Get_Attendance_Display($__attendanceStatus);
					if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
					    $ExportArr[$iCounter][$jCounter++] = ($forSample)? '' : $__attendanceRemark;
					}
				}
				
				if (!$forSample) {
					// attendance percentage
					$_display = ($_attendancePercentage=='')? 0 : $_attendancePercentage;
					$ExportArr[$iCounter][$jCounter++] = $_display.'%';
					
					// total attendance hour
					$ExportArr[$iCounter][$jCounter++] = ($_attendanceHours=='')? 0 : $_attendanceHours;
					
					if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
						$ExportArr[$iCounter][$jCounter++] = $extraInfoAssoAry[$_studentId]['EmergencyPhone'];
						$ExportArr[$iCounter][$jCounter++] = $extraInfoAssoAry[$_studentId]['FatherName'];
						$ExportArr[$iCounter][$jCounter++] = $extraInfoAssoAry[$_studentId]['FatherPhone'];
						$ExportArr[$iCounter][$jCounter++] = $extraInfoAssoAry[$_studentId]['MotherName'];
						$ExportArr[$iCounter][$jCounter++] = $extraInfoAssoAry[$_studentId]['MotherPhone'];
					}
				}
				
				$iCounter++;
			}
			
			// Bottom Summary data
			if (!$forSample) {
				$jCounter = 0;
				$ExportArr[$iCounter][$jCounter++] = $eEnrollment['act_attendence'];
				$ExportArr[$iCounter][$jCounter++] = '';
				$ExportArr[$iCounter][$jCounter++] = '';
				if (!$forSample) {
					$ExportArr[$iCounter][$jCounter++] = '';
				}
				for ($i=0; $i<$numOfMeeting; $i++) {
					$_dateId = $meetingDateInfoAry[$i][$dateIdFieldName];
					$_dateStart = $meetingDateInfoAry[$i]['ActivityDateStart'];
					$_dateStart = date("Y-m-d", strtotime($_dateStart));
					
					if ($libenroll->Is_Future_Date($_dateStart)) {
						$_display = $Lang['General']['EmptySymbol'];
					}
					else {
						$_dateAverageAttendancePercentage = $overallAttendanceInfoArr[$recordId]['MeetingDateArr'][$_dateId]['AttendancePercentageRounded'];
						$_display = ($_dateAverageAttendancePercentage=='')? '0%' : $_dateAverageAttendancePercentage.'%';
					}
					
					$ExportArr[$iCounter][$jCounter++] = $_display;
    				if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
        				$ExportArr[$iCounter][$jCounter++] = '';
    				}
				}
				// Club or Activity average attendance percentage
				$averageAtendancePercentage = $overallAttendanceInfoArr[$recordId]['AttendancePercentageRounded'];
				$display = ($averageAtendancePercentage=='')? 0 : $averageAtendancePercentage;
				$ExportArr[$iCounter][$jCounter++] = $display.'%';
				
				// Club or Activity average attendance hours
				$averageAtendanceHour = $overallAttendanceInfoArr[$recordId]['AttendancePercentageHourRounded'];
				$display = ($averageAtendanceHour=='')? 0 : $averageAtendanceHour;
				$ExportArr[$iCounter][$jCounter++] = $Lang['General']['EmptySymbol'];
			}
			
			
			### Header
			$exportColumn[] = $i_ClassName;
			$exportColumn[] = $i_ClassNumber;
			$exportColumn[] = $Lang['AccountMgmt']['StudentImportFields']['0'];
			if (!$forSample) {
				$exportColumn[] = $i_UserStudentName;
			}
			for ($i=0; $i<$numOfMeeting; $i++) {
				$_dateStart = $meetingDateInfoAry[$i]['ActivityDateStart'];
				$_dateEnd = $meetingDateInfoAry[$i]['ActivityDateEnd'];
				
				$_dateDisplay = '';
				$_dateDisplay .= date("Y-m-d", strtotime($_dateStart));
				$_dateDisplay .= " ";
				$_dateDisplay .= date("H:i", strtotime($_dateStart));
				$_dateDisplay .= "-";
				$_dateDisplay .= date("H:i", strtotime($_dateEnd));
				
				$exportColumn[] = $_dateDisplay;
				if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
				    $exportColumn[] = $Lang['General']['Remark'];
				}
			}
			if (!$forSample) {
				$exportColumn[] = $eEnrollment['studnet_attendence']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
				$exportColumn[] = $Lang['eEnrolment']['AttendanceHour']."(".$eEnrollment['up_to']." ".date("Y-m-d").")";
				
				if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
					$exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['EmergencyPhoneNumber'];
					$exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['FatherName'];
					$exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['FatherPhoneNumber'];
					$exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['MotherName'];
					$exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['MotherPhoneNumber'];
				}
			}
		}
		else if ($type == 4){	//event student list
				
			$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
			$title = str_replace(' ', '_', $EventInfoArr[3]);
			//$title = intranet_undo_htmlspecialchars($EventInfoArr[3]);	
			$filename = "information_of_".$title;
			//$filename = iconv("UTF-8", "Big5", $filename);
			$filename = $filename.".csv";
			
			$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID);
			
			$activeMemberArr = $libenroll->Get_Active_Member_Arr($EnrolEventID, "activity", 1);
			$AttendanceArr = $libenroll->Get_Student_Attendance_Arr($EnrolEventID, "activity", 1);
			
			/*
			if($filter=="" || !isset($filter)){
				$filter = 2;
			}
           	if (!isset($field) || $field=="")
	       	{
		        $field = ($filter==2)? 0 : 2;
	        }
			if ($order=="" || !isset($order))
			{
				$order = ($order == 0) ? 1 : 0;
			}
            
            if (isset($keyword))
			{
				$conds = "AND (a.EnglishName LIKE '%{$keyword}%' OR a.ChineseName LIKE '%{$keyword}%')";
			}
                   
			$name_field = getNameFieldByLang2("a.");
			if ($filter == 2)
			{
				//student
				$sql = "SELECT  
							IF(a.ClassName IS NULL OR a.ClassName='', '-', a.ClassName) as ClassName,
							IF(a.ClassNumber IS NULL OR a.ClassNumber='', '-', a.ClassNumber) as ClassNumber,
							$name_field as StudentName,
							IF(b.RoleTitle IS NULL, '-', b.RoleTitle) as RoleTitle,
			                IF(b.Performance IS NULL, '-', b.Performance) as Performance,
			                IF(b.CommentStudent IS NULL, '-', b.CommentStudent) as Comment,
			                IF(b.isActiveMember IS NULL,
			                	'-',
			                	IF (b.isActiveMember = '1', '".$eEnrollment['active']."', '".$eEnrollment['inactive']."')
		                	) as ActiveMemberStatus
			        FROM 
			        		INTRANET_USER as a 
			        	LEFT OUTER JOIN
			        		INTRANET_ENROL_EVENTSTUDENT as b ON a.UserID = b.StudentID
			        WHERE 
			        		b.EnrolEventID = '$EnrolEventID' 
			        		AND 
			        		b.RecordStatus = 2
			        		AND 
			        		a.RecordType = 2 and a.RecordStatus = 1 and a.ClassName Is Not Null and a.ClassNumber Is Not Null
			        		$conds    
			        ";		
		        $field_array = array("ClassName", "ClassNumber", "StudentName", "RoleTitle", "Performance","Comment","ActiveMemberStatus");
			}
			else
			{
				//staff
				$sql = "SELECT  
								$name_field as StudentName,
								b.StaffType as RoleTitle
				        FROM 
				        		INTRANET_USER as a 
				        	LEFT OUTER JOIN 
				        		INTRANET_ENROL_EVENTSTAFF as b ON a.UserID = b.UserID
				        WHERE 
				        		b.EnrolEventID = '$EnrolEventID'
				        		AND 
				        		a.RecordType = '$filter' 
				        		$conds    
				        ";
				$field_array = array("StudentName", "RoleTitle");
			}
			
			$order_str = $field_array[$field];
            if ($order == 1)
            {
                $order_type = " ASC";
            }
            else
            {
                $order_type = " DESC";
            }
            $order_str = str_replace(",","$order_type,",$order_str);
            
            if ($filter == 2) 
            	$order_str .= $order_type . " , ClassName, ClassNumber, b.EventStudentID";
            else
            	$order_str .= ", StudentName";
            
	        $sql .= " ORDER BY ".$order_str;	
	        */
	        
	        //$sql = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['ExportSQL']));
	        
	        $li = new libdbtable2007($field, $order, $pageNo);
	        
	        if($filter=="" || !isset($filter)){
			$filter = 2;
			}	
	        if (!isset($field))
	        {
		        $field = ($filter==2)? 0 : 2;
	        }
	
			switch ($field){
				case 0: $field = 0; break;
				case 1: $field = 1; break;
				case 2: $field = 2; break;
				case 3: $field = 3; break;
				case 4: $field = 4; break;
				case 5: $field = 5; break;
				default: $field = 0; break;
			}
			if ($order=="" || !isset($order))
			{
				$order = ($order == 0) ? 1 : 0;
			}
			
	        if ($keyword == $eEnrollment['enter_name']) {
	        	$keyword = "";
	        }
	        
	        if ($filter == 2) {
	        	$li->field_array = array("ClassName", "ClassNumber", "UserLogin", "StudentName", "RoleTitle", "Performance", "Achievement", "Comment","ActiveMemberStatus");
				if ($libenroll->enableActivitySurveyFormRight()) {
					$li->field_array[] = "SurveyResult";
				}
				if ($libenroll->enableActivityFillInEnrolReasonRight()) {
					$li->field_array[] = "Reason";
				}
				$li->field_array[] = "iu.UserID";
				
				
				if($sys_custom['eEnrolment']['tkogss_student_enrolment_report']) {
					$li->field_array = array_merge($li->field_array, array("ECA","SS","CS"));	
				}
				$li->fieldorder2 = ",ClassName, iu.ClassNumber, iees.EventStudentID";
	        }
	        else {
	        	$li->field_array = array("StudentName", "RoleTitle", "iu.UserID");
				$li->fieldorder2 = ", StudentName";
	        }
	        
	        $li->sql = $libenroll->Get_Management_Activity_Participant_Sql(array($EnrolEventID), array(2), $filter, $keyword, $ForExport=1, $AcademicYearID, $approvedByType);
			$ExportSQLTemp = $li->built_sql();
			$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
			$sql = substr($ExportSQLTemp, 0, $LimitIndex);
	        $row = $libenroll->returnArray($sql,7);
	        
	        $exportColumn = array();
	        if ($filter == 2)
			{
			
				//define column title
				$exportColumn[] = "ClassName";
				$exportColumn[] = "ClassNumber";
				$exportColumn[] = "UserLogin";
				$exportColumn[] = "StudentName";
				$exportColumn[] = "Role";
				$exportColumn[] = "Attendance";
				$exportColumn[] = "Performance";
				$exportColumn[] = "Achievement";
				$exportColumn[] = "Comment";
				$exportColumn[] = "Active/Inactive";
				if($libenroll->enableActivityRecordMeritInfoRight()){
					if (!$lstudentprofile->is_merit_disabled) {
						$exportColumn[] = "Merit";
					}
					if (!$lstudentprofile->is_min_merit_disabled) {
						$exportColumn[] = "Minor Merit";
					}
					if (!$lstudentprofile->is_maj_merit_disabled) {
						$exportColumn[] = "Major Merit";
					}
					if (!$lstudentprofile->is_sup_merit_disabled) {
						$exportColumn[] = "Super Merit";
					}
					if (!$lstudentprofile->is_ult_merit_disabled) {
						$exportColumn[] = "Ultra Merit";
					}
				}
				
				if($libenroll->enableActivityFillInEnrolReasonRight()){
					$exportColumn[] = "Reason";
				}
								
				for($i=0; $i<sizeof($row); $i++){
					$ExportArr[$i][] = $row[$i]["ClassName"];
					$ExportArr[$i][] = $row[$i]["ClassNumber"];
					$ExportArr[$i][] = $row[$i]["UserLogin"];
					$ExportArr[$i][] = $row[$i]["StudentName"];
					$ExportArr[$i][] = $row[$i]["RoleTitle"];
					
					## Attendance
					$DataArr['StudentID'] = $row[$i]['UserID'];
					$DataArr['EnrolEventID'] = $EnrolEventID;
					$this_attendance = $libenroll->GET_EVENT_STUDENT_ATTENDANCE($DataArr);
					$ExportArr[$i][] = $this_attendance;
					
					$ExportArr[$i][] = $row[$i]["Performance"];
					$ExportArr[$i][] = $row[$i]["Achievement"];
					$ExportArr[$i][] = $row[$i]["Comment"];
					$ExportArr[$i][] = $row[$i]["ActiveMemberStatus"];
					
					if($libenroll->enableActivityRecordMeritInfoRight()){
						if (!$lstudentprofile->is_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["merit"];
						}
						if (!$lstudentprofile->is_min_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["minorMerit"];
						}
						if (!$lstudentprofile->is_maj_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["majorMerit"];
						}
						if (!$lstudentprofile->is_sup_merit_disabled) {
							$ExportArr[$i][] = $row[$i]["superMerit"];
						}
						if (!$lstudentprofile->is_ult_merit_disabled) {	
							$ExportArr[$i][] = $row[$i]["ultraMerit"];
						}
					}
					
					# Enrol Reason Customization 
					if($libenroll->enableActivityFillInEnrolReasonRight()){
						$ExportArr[$i][] = $row[$i]["Reason"];	// Reason
					}
				}
			}
			else
			{
				//define column title
				$exportColumn = array(	"UserName", 
										"Role"
									);
									
				for($i=0; $i<sizeof($row); $i++){
					$ExportArr[$i][] = $row[$i]["StudentName"];
					$ExportArr[$i][] = $row[$i]["RoleTitle"];
				}
			}
			
			
		}
		else if ($type == 5)	//club applicants list
		{	
			$EnrolGroupID = $_REQUEST['EnrolGroupID'];
			$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);
			$li = new libgroup($GroupID);
			$title = str_replace(' ', '_', $li->Title);
			$filename = "applicant_list_of_".$title;
			$filename = $filename.".csv";
			
			if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($EnrolGroupID))) {
				No_Access_Right_Pop_Up();
			}
				
			/*
			$field_array = array("iu.ClassName", "iu.ClassNumber", "iu.EnglishName","iu.ChineseName","RecordStatus","iegs.DateInput");
			
			$conds_status = "";
			if ($filter != -1)
			{
			    $conds_status = "AND iegs.RecordStatus = $filter";
			}
			
			$sql = "SELECT
							iu.ClassName,
							iu.ClassNumber,
							iu.EnglishName,
							iu.ChineseName,
							CASE iegs.RecordStatus
								WHEN 0 THEN '$i_ClubsEnrollment_StatusWaiting'
								WHEN 1 THEN '$i_ClubsEnrollment_StatusRejected'
								WHEN 2 THEN '$i_ClubsEnrollment_StatusApproved'
							END as RecordStatus,
							iegs.DateInput
			        FROM
			        		INTRANET_USER as iu
			        		Inner Join
			        		INTRANET_ENROL_GROUPSTUDENT as iegs
			        		On iu.UserID = iegs.StudentID
			        WHERE
			        		iegs.EnrolGroupID = '$EnrolGroupID'
			        		AND
			        		(iu.EnglishName LIKE '%".$keyword."%' OR iu.ChineseName LIKE '%".$keyword."%')
							and
							iu.RecordStatus = 1 and iu.ClassName Is Not Null and iu.ClassNumber Is Not Null
			        		$conds_status
			        ";
			if ((($field !== 0) && ($field == "")) || $field >= sizeof($field_array))
            {
	            $order_str = "Class";
            }
            else
            {
	            $order_str = $field_array[$field];
            }
            
            $order = ($order == "") ? 1 : $order;
            if ($order == 0)
            {
                $order_type = " DESC";
            }
            else
            {
                $order_type = " ASC";
            }
            $order_str = str_replace(",","$order_type,",$order_str);
            $order_str .= $order_type;
            
            $sql .= "ORDER BY ".$order_str;
            */
            //$sql = intranet_undo_htmlspecialchars(stripslashes($_REQUEST['ExportSQL']));
            ### Get the export Sql first
            if ($keyword == $eEnrollment['enter_student_name']) {
            	$keyword = "";
            }
            $li = new libdbtable2007($field, $order, $pageNo);
			$li->field_array = array("iu.ClassName", "iu.ClassNumber", "iu.EnglishName", "iu.ChineseName");
			if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
				$li->field_array[] ="iu.NickName";		
			}
			$li->field_array[] ="iegs.RecordStatus";
			$li->field_array[] ="iegs.DateInput";
			$li->field_array[] ="iu.UserID";
			$li->sql = $libenroll->Get_Management_Club_Enrollment_Student_Sql(array($EnrolGroupID), $AcademicYearID, $keyword, $filter, $ForExport=1, $showAll);

			$ExportSQLTemp = $li->built_sql();
			$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
			$ExportSQL = substr($ExportSQLTemp, 0, $LimitIndex);
			
			$result = $libenroll->returnArray($ExportSQL);
			$numOfResult = count($result);
//			debug_pr($li->sql);
//			debug_pr($result);exit();
			
			// Initate: output row count
			$rows = 0;
			// Get Category Setting
			$useCAT = $libenroll->UseCategorySetting;
			
			for($i=0; $i<$numOfResult; $i++){
				
				// data with priority
				if($priority){
					// Define column title
// 					$exportColumn = array("ClassName", "ClassNumber", "UserLogin", "EnglishName", "ChineseName");
// 					if($sys_custom['eEnrolment']['ShowStudentNickName'] = true){
// 						$exportColumn[] = "NickName";
// 					}
// 					$exportColumn[] ="Status";
// 					$exportColumn[] ="Priority";
// 					$exportColumn[] ="Club Name";
// 					$exportColumn[] ="Status";
// 					$exportColumn[] ="LastSubmissionTime";
					
					if($useCAT){
						$exportColumn = array("ClassName", "ClassNumber", "UserLogin", "EnglishName", "ChineseName");
						if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
							$exportColumn[] = "NickName";
						}
						$exportColumn[] = "Status";
						$exportColumn[] = "Category Name";
						$exportColumn[] = "Priority";
						$exportColumn[] = "Club Name";
						$exportColumn[] = "Status";
						$exportColumn[] = "LastSubmissionTime";
							
					}else{
						$exportColumn = array("ClassName", "ClassNumber", "UserLogin", "EnglishName", "ChineseName");
						if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
							$exportColumn[] = "NickName";
						}
						$exportColumn[] ="Status";
						$exportColumn[] ="Priority";
						$exportColumn[] ="Club Name";
						$exportColumn[] ="Status";
						$exportColumn[] ="LastSubmissionTime";
					}
				//	$exportColumn = $useCAT? array("ClassName", "ClassNumber", "UserLogin", "EnglishName", "ChineseName", "Status", "Category Name", "Priority", "Club Name", "Status", "LastSubmissionTime") : $exportColumn;
					
					// Get related club info for each student
					$studentEnrollmentInfoAry = $libenroll->Get_Student_Applied_Club_Info($result[$i][UserID], '', $recordStatusAry='', $categoryIdAry='', $AcademicYearID);
					$noOfEnrollmentResult = count($studentEnrollmentInfoAry);
					
					// Data for export	
					for($j=0; $j<$noOfEnrollmentResult; $j++){
						if($j == 0){
							$col = 0;
//							$ExportArr[$rows][0] = $result[$i][ClassName];					//ClassName
//							$ExportArr[$rows][1] = $result[$i][ClassNumber];				//ClassNumber
//							$ExportArr[$rows][2] = $result[$i][2];							//English Name
//							$ExportArr[$rows][3] = $result[$i][3];							//Chinese Name
//							$ExportArr[$rows][4] = $result[$i][RecordStatus];				//Student Enrollment Status
							$ExportArr[$rows][$col++] = $result[$i][ClassName];					//ClassName
							$ExportArr[$rows][$col++] = $result[$i][ClassNumber];				//ClassNumber
							$ExportArr[$rows][$col++] = $result[$i]['UserLogin'];				
							$ExportArr[$rows][$col++] = $result[$i][2];							//English Name
							$ExportArr[$rows][$col++] = $result[$i][3];							//Chinese Name
							if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
								$ExportArr[$rows][$col++] = $result[$i][NickName];	
									
							}
							$ExportArr[$rows][$col++] = $result[$i][RecordStatus];				//Student Enrollment Status
						} else {
							// No need to generate duplicate data
							$ExportArr[$rows][0] = '';
							$ExportArr[$rows][1] = '';
							$ExportArr[$rows][2] = '';
							$ExportArr[$rows][3] = '';
							$ExportArr[$rows][4] = '';
							$ExportArr[$rows][5] = '';
							if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
								$ExportArr[$rows][6] = '';	
							}
							
						}
						
						if($useCAT){
							$catName = $studentEnrollmentInfoAry[$j][CategoryName];
							$catName = ($catName == null || trim($catName) == "")? "--" : $catName;
							$ExportArr[$rows][] = $catName;									// Category Name
						}
						$ExportArr[$rows][] = $studentEnrollmentInfoAry[$j][Choice];		// Priority
						$ExportArr[$rows][] = $studentEnrollmentInfoAry[$j][Title];			// Club Name
																							// Club Enrollment Status
						$ExportArr[$rows][] = $libenroll_ui->Get_Enrollment_Status_Display($studentEnrollmentInfoAry[$j][RecordStatus]);
						$ExportArr[$rows][] = $result[$i][DateInput];						//Last Submission Time
						$rows++;
					}
					
				} else {
					//define column title
					$exportColumn = array("ClassName", "ClassNumber", "UserLogin","EnglishName", "ChineseName");
					if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
						$exportColumn[] = "NickName";
					}
					$exportColumn[] = "Status";
					$exportColumn[] = "LastSubmissionTime";
					
//					$ExportArr[$i][0] = $result[$i][ClassName];					//ClassName
//					$ExportArr[$i][1] = $result[$i][ClassNumber];				//ClassNumber
//					$ExportArr[$i][2] = $result[$i][2];							//English Name
//					$ExportArr[$i][3] = $result[$i][3];							//Chinese Name
//					$ExportArr[$i][4] = $result[$i][RecordStatus];				//Student Enrollment Status
//					$ExportArr[$i][5] = $result[$i][DateInput];					//Last Submission Time
					
					$ExportArr[$i][] = $result[$i][ClassName];					//ClassName
					$ExportArr[$i][] = $result[$i][ClassNumber];				//ClassNumber
					$ExportArr[$i][] = $result[$i]['UserLogin'];
					$ExportArr[$i][] = $result[$i][2];							//English Name
					$ExportArr[$i][] = $result[$i][3];							//Chinese Name
					
					if($sys_custom['eEnrolment']['ShowStudentNickName'] == true){
						$ExportArr[$i][] = $result[$i][NickName];
					}
					
					$ExportArr[$i][] = $result[$i][RecordStatus];				//Student Enrollment Status
					$ExportArr[$i][] = $result[$i][DateInput];					//Last Submission Time
				}
			}
	//		debug_pr($sys_custom['eEnrolment']['ShowStudentNickName']);
			//define column title
//			$exportColumn = array("ClassName", "ClassNumber", "EnglishName", "ChineseName", "Status", "priority", "Club Name", "Status", "LastSubmissionTime");
		}
		else if ($type == 6){
			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
			
			$fromExtraInfoCust = $_POST['fromExtraInfoCust'];
			$studentExtraInfoFieldAry = $_POST['studentExtraInfoFieldAry'];
			$numOfStudentExtraInfoField = is_array($studentExtraInfoFieldAry)? count((array)$studentExtraInfoFieldAry) : 0;
			
			## csv file name
			$filename = "club_member_list.csv";
			
			## csv header
			$exportColumn = array();
			
			$termAry = $libenroll->getTermColumn($AcademicYearID);
			$numOfTerm = count($termAry);
			
//			$exportColumn = array(	
//									"ClubName(EN)",
//									"Term",
//									"ClassName",
//									"ClassNumber", 
//									"StudentName", 
//									"Role", 
//									"Performance",
//									"Achievement",
//									"Comment",
//									"Active/Inactive",
//									"AdminRole"
//								);
								
			if($sys_custom['eEnrolment']['ClubActivity']['export']['GroupCode']){
				$exportColumn[] = "ClubCode";
			}					
			$exportColumn[] = "ClubName(EN)";
			$exportColumn[] = "Term";
			$exportColumn[] = "ClassName";
			$exportColumn[] = "ClassNumber";
			$exportColumn[] = "UserLogin"; //P74036
			$exportColumn[] = "StudentName";
			$exportColumn[] = "Role";
			if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub'] && $showTerm == 1) {
			    for ($i = 0; $i < $numOfTerm; $i ++) {
			        $exportColumn[] = str_replace(' ', '', $termAry[$i]["TermName"] . "Performance");
			    }
			}
			$exportColumn[] = "Performance";
			$exportColumn[] = "Achievement";
			$exportColumn[] = "Comment";
			$exportColumn[] = "Active/Inactive";
			$exportColumn[] = "AdminRole";
			
			if($libenroll->enableClubRecordMeritInfoRight()){
				//2014-0519-1338-56177
//				$exportColumn[] = "Merit";
//				$exportColumn[] = "Minor Merit";
//				$exportColumn[] = "Major Merit";
//				$exportColumn[] = "Super Merit";	
//				$exportColumn[] = "Ultra Merit";
				
				if (!$lstudentprofile->is_merit_disabled) {
					$exportColumn[] = "Merit";
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$exportColumn[] = "Minor Merit";
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$exportColumn[] = "Major Merit";
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$exportColumn[] = "Super Merit";	
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$exportColumn[] = "Ultra Merit";
				}			
			}
			
			if($sys_custom['eEnrolment']['ClubExportWithTeacherName'])
			{
				$exportColumn[] = "ApprovedBy";
			}
				
			
			if ($numOfStudentExtraInfoField > 0) {
				for ($i=0; $i<$numOfStudentExtraInfoField; $i++) {
					$_field = $studentExtraInfoFieldAry[$i];
					$exportColumn[] = $_field;
				}
			}
													
			## Get Data
			# All Club Member List
			if ($fromExtraInfoCust) {
// 				$category_conds = intranet_undo_htmlspecialchars($category_conds);
// 				$name_conds = intranet_undo_htmlspecialchars($name_conds);
// 				$semester_conds = intranet_undo_htmlspecialchars($semester_conds);
				$CategoryID = $_GET['CategoryID'];
				$Keyword = $_GET['Keyword'];
				$Semester = $_GET[Semester];
			
			}
// 			$category_conds = stripslashes(str_replace("%20", " ", $category_conds));
// 			$name_conds = stripslashes(str_replace("%20", " ", $name_conds));
// 			$semester_conds = stripslashes(str_replace("%20", " ", $semester_conds));

		//	$ClubInfoArr = $libenroll->getGroupsForEnrolSet("",$category_conds, $name_conds, $semester_conds, $AcademicYearID);
			$ClubInfoArr = $libenroll->getGroupsForEnrolSetByConditions($AcademicYearID,$CategoryID, $Keyword, $Semester);
			
			$numOfClub = count($ClubInfoArr);
			
			if ($numOfClub != 0)
			{
				$EnrolGroupIDArr = array();
				for ($i=0; $i<$numOfClub; $i++)
				{
					$thisEnrolGroupID = $ClubInfoArr[$i]["EnrolGroupID"];
					$EnrolGroupIDArr[] = $thisEnrolGroupID;
				}
				$EnrolGroupIDList = implode(", ", $EnrolGroupIDArr);
				
				$name_field = getNameFieldByLang2("b.");
				
				//student => find in Role in INTRANET_USERGROUP
				/*
				$sql = "SELECT	
								IF(b.ClassName IS NULL, '-', b.ClassName) as ClassName,
								IF(b.ClassNumber IS NULL, '-', b.ClassNumber) as ClassNumber,
								$name_field as StudentName,
				                IF(a.RecordType = 'A', CONCAT(c.Title, '*'), c.Title) as RoleTitle, 
				                IF(a.Performance IS NULL, '-', a.Performance) as Performance,
				                IF(a.CommentStudent IS NULL, '-', a.CommentStudent) as Comment,
				                IF(a.isActiveMember IS NULL,
				                	'-',
				                	IF (a.isActiveMember = '1', '".$eEnrollment['active']."', '".$eEnrollment['inactive']."')
			                	) as ActiveMemberStatus,
			                	d.Title as ClubTitle,
			                	ieg.Semester
				        FROM
				        		INTRANET_ENROL_GROUPINFO as ieg 
				        		INNER JOIN 
				        		INTRANET_USERGROUP as a ON ieg.EnrolGroupID = a.EnrolGroupID
				        		INNER JOIN 
				        		INTRANET_USER as b ON a.UserID = b.UserID 
				        		LEFT OUTER JOIN
				        		INTRANET_ROLE as c ON a.RoleID = c.RoleID
				        		LEFT OUTER JOIN
				        		INTRANET_GROUP as d ON a.GroupID = d.GroupID
				        WHERE 
				        		b.RecordType = 2 and b.RecordStatus = 1 and b.ClassName Is Not Null and b.ClassNumber Is Not Null
				        		AND
				        		d.RecordType = 5
				        		AND
				        		a.EnrolGroupID IN ($EnrolGroupIDList)
				        		And
				        		d.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
				        ORDER BY
				        	d.Title, b.ClassName, b.ClassNumber
				        ";
				*/
				$row = $libenroll->Get_Club_Member_Info($EnrolGroupIDArr, $PersonTypeArr=array(2), $AcademicYearID, $WithIndicator=1, $IndicatorWithStyle=0, $WithEmptySymbol=0, $ReturnAsso=0);
				
				//$row = $libenroll->returnArray($sql,8);
				
				if ($numOfStudentExtraInfoField > 0) {
					$studentIdAry = Get_Array_By_Key($row, 'UserID');
					$userObj = new libuser('', '', $studentIdAry);
				}
				
				
				
				// Create data array for export
				for($i=0; $i<sizeof($row); $i++){
					$_studentId = $row[$i]["UserID"];
					if($sys_custom['eEnrolment']['ClubActivity']['export']['GroupCode']){
						$ExportArr[$i][] = $row[$i]["GroupCode"];
					}
					$ExportArr[$i][] = $row[$i]["GroupTitle"];
					
					$termPerformanceAry = $libenroll->getTermPerformance($AcademicYearID, $row[$i]["EnrolGroupID"], $row[$i]["UserID"]);

					/*
					$thisSemester = $row[$i]["Semester"];
					
					if ($thisSemester == '')
					{
						$ExportArr[$i][] = $i_ClubsEnrollment_WholeYear;
					}
					else
					{
						$ObjTerm = new academic_year_term($thisSemester);
						$ExportArr[$i][] = $ObjTerm->Get_Year_Term_Name();
					}
					*/
					
					//debug_pr($ExportArr);
					
					$ExportArr[$i][] = $row[$i]["YearTermName"];
					$ExportArr[$i][] = $row[$i]["ClassName"];
					$ExportArr[$i][] = $row[$i]["ClassNumber"];
					$ExportArr[$i][] = $row[$i]["UserLogin"];
					$ExportArr[$i][] = $row[$i]["StudentName"];
					$ExportArr[$i][] = $row[$i]["RoleTitle"];
					if($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub'] && $showTerm == 1){
					   for($term = 0; $term < sizeof($termPerformanceAry); $term++){
					       if(sizeof($termPerformanceAry[$term]) > 0){
					           $ExportArr[$i][] = $termPerformanceAry[$term][0];
					       }else{
					           $ExportArr[$i][] = "";
					       }
					   }
					}
					$ExportArr[$i][] = $row[$i]["Performance"];
					$ExportArr[$i][] = $row[$i]["Achievement"];
					$ExportArr[$i][] = $row[$i]["Comment"];
					$ExportArr[$i][] = $row[$i]["ActiveMemberStatus"];
					$ExportArr[$i][] = $row[$i]["StaffType"];
					
					if ($numOfStudentExtraInfoField > 0) {
						$userObj->LoadUserData($_studentId);
						
						for ($j=0; $j<$numOfStudentExtraInfoField; $j++) {
							$__field = $studentExtraInfoFieldAry[$j];
							$__fieldValue = $userObj->{$__field};
							
							if ($__field == 'DateOfBirth' && is_date_empty($__fieldValue)) {
								$__fieldValue = '';
							}
							
							$ExportArr[$i][] = $__fieldValue;
						}
					}
					
				    # Merit Customization
					if($libenroll->enableClubRecordMeritInfoRight()){
						$meritEnrolGroupID = $row[$i]["EnrolGroupID"];
						$meritRecordArr = $libenroll->Get_Merit_Record($_studentId,$meritEnrolGroupID,$meritType='');
						$meritRecordArrAsso = BuildMultiKeyAssoc($meritRecordArr, 'MeritType');
						
						if (!$lstudentprofile->is_merit_disabled) {
							$ExportArr[$i][] = $meritRecordArrAsso['1']["MeritCount"];
						}
						if (!$lstudentprofile->is_min_merit_disabled) {
							$ExportArr[$i][] = $meritRecordArrAsso['2']["MeritCount"];
						}
						if (!$lstudentprofile->is_maj_merit_disabled) {
							$ExportArr[$i][] = $meritRecordArrAsso['3']["MeritCount"];
						}
						if (!$lstudentprofile->is_sup_merit_disabled) {
							$ExportArr[$i][] = $meritRecordArrAsso['4']["MeritCount"];
						}
						if (!$lstudentprofile->is_ult_merit_disabled) {
							$ExportArr[$i][] = $meritRecordArrAsso['5']["MeritCount"];
						}
					}
					if($sys_custom['eEnrolment']['ClubExportWithTeacherName'])
					{
						$ExportArr[$i][] = $row[$i]["TeacherName"];
					}
				}
			}
		}
		else if ($type == 7){
			
			## csv file name
			$filename = "activity_participant_list.csv";
			
			$fromExtraInfoCust = $_POST['fromExtraInfoCust'];
			$studentExtraInfoFieldAry = $_POST['studentExtraInfoFieldAry'];
			$numOfStudentExtraInfoField = is_array($studentExtraInfoFieldAry)? count((array)$studentExtraInfoFieldAry) : 0;
			
			## csv header
			$exportColumn = array();
			if($sys_custom['eEnrolment']['ClubActivity']['export']['GroupCode']){
				$exportColumn[] = "ActivityCode";
			}		
			$exportColumn[] = "ActivityTitle";
			$exportColumn[] = "ClassName";
			$exportColumn[] = "ClassNumber";
			$exportColumn[] = "UserLogin"; //P74036
			$exportColumn[] = "StudentName";
			$exportColumn[] = "Role";
			$exportColumn[] = "Performance";
			$exportColumn[] = "Achievement";
			$exportColumn[] = "Comment";
			$exportColumn[] = "Active/Inactive";
			$exportColumn[] = "AdminRole";
			if($libenroll->enableActivityRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$exportColumn[] = "Merit";
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$exportColumn[] = "Minor Merit";
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$exportColumn[] = "Major Merit";
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$exportColumn[] = "Super Merit";
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$exportColumn[] = "Ultra Merit";
				}
			}
			
			if($libenroll->enableActivityFillInEnrolReasonRight()){
				$exportColumn[] = "Reason";
			}
			
			if ($numOfStudentExtraInfoField > 0) {
				for ($i=0; $i<$numOfStudentExtraInfoField; $i++) {
					$_field = $studentExtraInfoFieldAry[$i];
					$exportColumn[] = $_field;
				}
			}
			
			$exportColumn[] = "Belong to Club";
			
			## Get data
			$CategoryID = $_GET['CategoryID'];
			$Keyword = $_GET['Keyword'];
		//	$category_conds = stripslashes(str_replace("%20", " ", $category_conds));
		//	$name_conds = stripslashes(str_replace("%20", " ", $name_conds));
		//	$academicYear_conds =  stripslashes(str_replace("%20", " ", $academicYear_conds));
		//	$ActivityInfoArr = $libenroll->Get_Activity_Info($category_conds.$name_conds,$AcademicYearID);
			$ActivityInfoArr = $libenroll->Get_Activity_Info_By_Conditions($CategoryID,$Keyword,$AcademicYearID);
			$numOfActivity = count($ActivityInfoArr);
			
			if ($numOfActivity != 0)
			{
				$EnrolEventIDArr = array();
				for ($i=0; $i<$numOfActivity; $i++)
				{
					$thisEnrolEventID = $ActivityInfoArr[$i]["EnrolEventID"];
					$EnrolEventIDArr[] = $thisEnrolEventID;
				}
				
				/*
				$EnrolEventIDList = implode(", ", $EnrolEventIDArr);
				
				$name_field = getNameFieldByLang2("a.");
				
				$sql = "SELECT  
							IF(a.ClassName IS NULL OR a.ClassName='', '-', a.ClassName) as ClassName,
							IF(a.ClassNumber IS NULL OR a.ClassNumber='', '-', a.ClassNumber) as ClassNumber,
							$name_field as StudentName,
							IF(b.RoleTitle IS NULL, '-', b.RoleTitle) as RoleTitle,
			                IF(b.Performance IS NULL, '-', b.Performance) as Performance,
			                IF(b.CommentStudent IS NULL, '-', b.CommentStudent) as Comment,
			                IF(b.isActiveMember IS NULL,
			                	'-',
			                	IF (b.isActiveMember = '1', '".$eEnrollment['active']."', '".$eEnrollment['inactive']."')
		                	) as ActiveMemberStatus,
		                	c.EventTitle
			        FROM 
			        		INTRANET_USER as a 
			        		LEFT OUTER JOIN
			        		INTRANET_ENROL_EVENTSTUDENT as b ON a.UserID = b.StudentID
			        		LEFT OUTER JOIN
			        		INTRANET_ENROL_EVENTINFO as c ON c.EnrolEventID = b.EnrolEventID
			        WHERE 
			        		b.EnrolEventID IN ($EnrolEventIDList)
			        		AND 
			        		b.RecordStatus = 2
			        		AND 
			        		a.RecordType = 2 and a.RecordStatus = 1 and a.ClassName Is Not Null and a.ClassNumber Is Not Null
			        ORDER BY
			        		c.EventTitle, a.ClassName, a.ClassNumber
			        ";
				$row = $libenroll->returnArray($sql,8);
				*/
				
				$AcademicYearID = ($AcademicYearID=="") ? Get_Current_Academic_Year_ID() : $AcademicYearID;
				$ActivityParticipantInfo = $libenroll->Get_Activity_Student_Enrollment_Info($EnrolEventIDArr, $ActivityKeyword='', $AcademicYearID, $WithNameIndicator=1, $IndicatorWithStyle=0, $WithEmptySymbol=0);

				if ($numOfStudentExtraInfoField > 0) {
					$studentIdAry = array();
					foreach((array)$ActivityParticipantInfo as $_enrolEventId => $_activityInfoAry) {
						foreach((array)$_activityInfoAry['StatusStudentArr'] as $__status => $__memberInfoAry) {
							$studentIdAry = array_merge($studentIdAry, Get_Array_By_Key($__memberInfoAry, 'UserID'));
						}
					}
					$studentIdAry = array_values(array_unique($studentIdAry));
					$userObj = new libuser('', '', $studentIdAry);
				}
				
				$Counter_i = 0;
				$Counter_j = 0;
				foreach ($ActivityParticipantInfo as $thisEnrolEventID => $thisActivityInfoArr) {
					$thisActivityTitle = $thisActivityInfoArr['EventTitle'];
					$thisParticipantInfoArr = $thisActivityInfoArr['StatusStudentArr'][2];
	
	
					if($libenroll->enableActivityFillInEnrolReasonRight()){
						$thisParticipantUserIdAssoArr = BuildMultiKeyAssoc($thisParticipantInfoArr, 'UserID');
						$thisParticipantUserIdArr = array_keys($thisParticipantUserIdAssoArr);
	
					    $studentInfoAssoArr = $libenroll->GET_EVENTSTUDENT($thisEnrolEventID, $thisParticipantUserIdArr);
						$thisParticipantEnrolReasonAssoArr = BuildMultiKeyAssoc($studentInfoAssoArr, 'StudentID');
					    				
					}
					$BelongClubID = $libenroll->Get_Activity_Belong_To_Club($thisEnrolEventID);
					$BelongClubName = $libenroll->getClubInfoByEnrolGroupID($BelongClubID);
			
					$numOfParticipant = count($thisParticipantInfoArr);
					for ($i=0; $i<$numOfParticipant; $i++) {
						$_studentId = $thisParticipantInfoArr[$i]['UserID'];
						$Counter_j = 0;
						
						if($sys_custom['eEnrolment']['ClubActivity']['export']['GroupCode']){
							$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['ActivityCode'];
						}
						//$ExportArr[$Counter_i][$Counter_j++] = $thisActivityTitle;
						//$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['ActivityCode'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['EventTitle'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['ClassName'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['ClassNumber'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['UserLogin'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['StudentName'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['RoleTitle'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['Performance'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['Achievement'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['CommentStudent'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['ActiveMemberStatus'];
						$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]['StaffType'];

					
						if($libenroll->enableActivityFillInEnrolReasonRight()){
							$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantEnrolReasonAssoArr[$_studentId]['Reason'];
						}
						
						if ($numOfStudentExtraInfoField > 0) {
							$userObj->LoadUserData($_studentId);
							
							for ($j=0; $j<$numOfStudentExtraInfoField; $j++) {
								$__field = $studentExtraInfoFieldAry[$j];
								$__fieldValue = $userObj->{$__field};
								
								if ($__field == 'DateOfBirth' && is_date_empty($__fieldValue)) {
									$__fieldValue = '';
								}
								
								$ExportArr[$Counter_i][$Counter_j++] = $__fieldValue;
							}
						}
						
						if($libenroll->enableActivityRecordMeritInfoRight()){
							if (!$lstudentprofile->is_merit_disabled) {
								$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]["merit"];
							}
							if (!$lstudentprofile->is_min_merit_disabled) {
								$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]["minorMerit"];
							}
							if (!$lstudentprofile->is_maj_merit_disabled) {
								$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]["majorMerit"];
							}
							if (!$lstudentprofile->is_sup_merit_disabled) {
								$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]["superMerit"];
							}
							if (!$lstudentprofile->is_ult_merit_disabled) {
								$ExportArr[$Counter_i][$Counter_j++] = $thisParticipantInfoArr[$i]["ultraMerit"];
							}
						}
						
						
						$ExportArr[$Counter_i][$Counter_j++] = $BelongClubName;
						
						$Counter_i++;
					}
				}
							
				/*
		        for($i=0; $i<sizeof($row); $i++)
		        {
			        $ExportArr[$i][] = $row[$i]["EventTitle"];
					$ExportArr[$i][] = $row[$i]["ClassName"];
					$ExportArr[$i][] = $row[$i]["ClassNumber"];
					$ExportArr[$i][] = $row[$i]["StudentName"];
					$ExportArr[$i][] = $row[$i]["RoleTitle"];
					$ExportArr[$i][] = $row[$i]["Performance"];
					$ExportArr[$i][] = $row[$i]["Comment"];
					$ExportArr[$i][] = $row[$i]["ActiveMemberStatus"];
				}
				*/
			}
		}
		else if ($type == 8) {
			$importType = $_GET['importType'];
			
			if ($importType == 'clubAllMember') {
				$filename = "import_all_member_sample_unicode.csv";
			}
			else {
				$filename = "import_member_sample_unicode.csv";
			}
			
			$exportColumn = array();
			if ($importType == 'clubAllMember') {
				$exportColumn[] = "ClubName(EN)";
				$exportColumn[] = "Term";
			}
			$exportColumn[] = "ClassName";
			$exportColumn[] = "ClassNumber";
			$exportColumn[] = "UserLogin";
			$exportColumn[] = "Role";
			$exportColumn[] = "Performance";
			$exportColumn[] = "Achievement";
			$exportColumn[] = "Comment";
			$exportColumn[] = "Active/Inactive";
			$exportColumn[] = "AdminRole";
			if($libenroll->enableClubRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$exportColumn[] = "Merit";
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$exportColumn[] = "MinorMerit";
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$exportColumn[] = "MajorMerit";
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$exportColumn[] = "SuperMerit";
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$exportColumn[] = "UltraMerit";
				}
			}
			
			$rowCount = 0;
			$ExportArr = array();
			if ($importType == 'clubAllMember') {
				$ExportArr[$rowCount][] = "Reading Club";
				$ExportArr[$rowCount][] = "Term 1";
			}
			$ExportArr[$rowCount][] = "1A";
			$ExportArr[$rowCount][] = "4";
			$ExportArr[$rowCount][] = "s1101234";
			$ExportArr[$rowCount][] = "Member";
			$ExportArr[$rowCount][] = "Good";
			$ExportArr[$rowCount][] = "Certificate of award";
			$ExportArr[$rowCount][] = "Active participation";
			$ExportArr[$rowCount][] = "Active";
			$ExportArr[$rowCount][] = "PIC";
			if($libenroll->enableClubRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ExportArr[$rowCount][] = 5;
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ExportArr[$rowCount][] = 3;
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ExportArr[$rowCount][] = 0.5;
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ExportArr[$rowCount][] = 0.25;
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ExportArr[$rowCount][] = 1;
				}
			}
			$rowCount++;
			
			if ($importType == 'clubAllMember') {
				$ExportArr[$rowCount][] = "Choir";
				$ExportArr[$rowCount][] = "Term 2";
			}
			$ExportArr[$rowCount][] = "5B";
			$ExportArr[$rowCount][] = "12";
			$ExportArr[$rowCount][] = "s1302367";
			$ExportArr[$rowCount][] = "Chairman";
			$ExportArr[$rowCount][] = "Unsatisfactory";
			$ExportArr[$rowCount][] = "";
			$ExportArr[$rowCount][] = "Always late";
			$ExportArr[$rowCount][] = "Inactive";
			$ExportArr[$rowCount][] = "HELPER";
			if($libenroll->enableClubRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ExportArr[$rowCount][] = 3;
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ExportArr[$rowCount][] = 2;
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ExportArr[$rowCount][] = 0.25;
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ExportArr[$rowCount][] = 1;
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ExportArr[$rowCount][] = 0.25;
				}
			}
			$rowCount++;
		}
		else if ($type == 9) {
			$EnrolGroupID = $_REQUEST['EnrolGroupID'];
			
			if(isset($EnrolGroupID)){
				$EnrolGroupIDArr[] = $EnrolGroupID;
			}
			else{
				$sql = "SELECT iegi.ENROLGROUPID as EnrolGroupID
						FROM INTRANET_ENROL_GROUPINFO as iegi 
							 INNER JOIN INTRANET_GROUP as ig ON iegi.GROUPID = ig.GROUPID 
						where 
							iegi.RecordStatus =1 AND ig.AcademicYearID ='".$AcademicYearID."' ";
				$tempEnrolGroupIDArr = $libenroll->returnArray($sql);
				
				if($sys_custom['eEnrollment']['setTargetForm']){
				    $yearName = str_replace(",", "','", $libenroll->settingTargetForm);
				    $sql = "SELECT YearID FROM YEAR WHERE YearName IN ('". $yearName ."')";
				    $yearID = $libenroll->returnVector($sql);
				    $targetFormEnrol = array();
				    
				    for($i = 0; $i < sizeof($tempEnrolGroupIDArr); $i++){
				        //get student form name
				        $sql = "SELECT ClassLevelID FROM INTRANET_ENROL_GROUPCLASSLEVEL WHERE EnrolGroupID = '" . $tempEnrolGroupIDArr[$i]["EnrolGroupID"] . "'";
				        $enrolClass = $libenroll->returnVector($sql);
				        
				        if(sizeof(array_intersect($enrolClass, $yearID)) > 0){
				            $targetFormEnrol[] = $tempEnrolGroupIDArr[$i];
				        }
				    }
				    $tempEnrolGroupIDArr = array();
				    $tempEnrolGroupIDArr = $targetFormEnrol;
				}
				
				$EnrolGroupIDArr = Get_Array_By_Key($tempEnrolGroupIDArr,'EnrolGroupID');

			}
			
			$rows = 0;$a=0;
			$ExportArr = array();
			$studentInExportArr = array();
			foreach($EnrolGroupIDArr as $_EnrolGroupID ){
				$GroupID = $libenroll->GET_GROUPID($_EnrolGroupID);
				$li = new libgroup($GroupID);
				
				if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_CLUB_PIC($_EnrolGroupID))) {
					No_Access_Right_Pop_Up();
				}
							
	            ### Get the export Sql first
	            if ($keyword == $eEnrollment['enter_student_name']) {
	            	$keyword = "";
	            }
	            $li = new libdbtable2007($field, $order, $pageNo);
				$li->field_array = array("iu.ClassName", "iu.ClassNumber", "iu.EnglishName", "iu.ChineseName", "iegs.RecordStatus", "iegs.DateInput", "iu.UserID");
				$li->sql = $libenroll->Get_Management_Club_Enrollment_Student_Sql(array($_EnrolGroupID), $AcademicYearID, $keyword, -1, $ForExport=1, 0);
	
				$ExportSQLTemp = $li->built_sql();
				$LimitIndex = strripos($ExportSQLTemp, "LIMIT");
				$ExportSQL = substr($ExportSQLTemp, 0, $LimitIndex);

				$result = $libenroll->returnArray($ExportSQL);
				$numOfResult = count($result);

				// Get Category Setting
				$useCAT = $libenroll->UseCategorySetting;
				
				for($i=0; $i<$numOfResult; $i++){
					
					// Define column title
// 					$exportColumn = array("ClassName", "ClassNumber", "UserLogin","EnglishName", "ChineseName", "Priority", "Club Name", "Status", "LastSubmissionTime");
				    $exportColumn = array("ClassName", "ClassNumber", "UserLogin","EnglishName", "ChineseName", "Number of clubs a student would like to join", "Priority", "Club Name", "Status", "LastSubmissionTime");	    
				    $exportColumn = $useCAT? array("ClassName", "ClassNumber", "UserLogin", "EnglishName", "ChineseName", "Category Name", "Priority", "Club Name", "Status", "LastSubmissionTime") : $exportColumn;
					
					$_studentId = $result[$i][UserID];
					
					if(in_array($_studentId,$studentInExportArr)){
						continue;
					}
// 					$_studentIdArray[] = $_studentId;
					// Get related club info for each student
					$studentEnrollmentInfoAry = $libenroll->Get_Student_Applied_Club_Info($result[$i][UserID], '', $recordStatusAry='', $categoryIdAry='', $AcademicYearID);
					$noOfEnrollmentResult = count($studentEnrollmentInfoAry);
					
					if($noOfEnrollmentResult > 0){
						$studentInExportArr[] = $result[$i][UserID];
					}
					
					// Data for export	
					for($j=0; $j<$noOfEnrollmentResult; $j++){
						if(!in_array($studentEnrollmentInfoAry[$j]['EnrolGroupID'], $EnrolGroupIDArr )){
							// skip club that is not opened online  #L103192 
							continue;
						}
						$sql = "SELECT Max FROM INTRANET_ENROL_STUDENT WHERE StudentID = ". $_studentId;
						$max = $libenroll->returnVector($sql);
						
//						$ExportArr[$rows][0] = $result[$i][ClassName];					//ClassName
//						$ExportArr[$rows][1] = $result[$i][ClassNumber];				//ClassNumber
//						$ExportArr[$rows][2] = $result[$i][2];							//English Name
//						$ExportArr[$rows][3] = $result[$i][3];							//Chinese Name
						$ExportArr[$rows][0] = $result[$i][ClassName];					//ClassName
						$ExportArr[$rows][1] = $result[$i][ClassNumber];				//ClassNumber
						$ExportArr[$rows][2] = $result[$i]['UserLogin'];				//UserLogin
						$ExportArr[$rows][3] = $result[$i][2];							//English Name
						$ExportArr[$rows][4] = $result[$i][3];							//Chinese Name
						
						if($useCAT){
							$catName = $studentEnrollmentInfoAry[$j][CategoryName];
							$catName = ($catName == null || trim($catName) == "")? "--" : $catName;
							$ExportArr[$rows][] = $catName;									// Category Name
						}

						$ExportArr[$rows][] = $max[0];
						$ExportArr[$rows][] = $studentEnrollmentInfoAry[$j][Choice];		// Priority
						$ExportArr[$rows][] = $studentEnrollmentInfoAry[$j][Title];			// Club Name
																							// Club Enrollment Status
						$ExportArr[$rows][] = $libenroll_ui->Get_Enrollment_Status_Display($studentEnrollmentInfoAry[$j][RecordStatus]);
						$ExportArr[$rows][] = $result[$i][DateInput];						//Last Submission Time
						$rows++;
					}					
				}
			}
			
			# Sort array by classname,classno
			$classname = array();
			$classno = array();
			foreach((array)$ExportArr as $key => $applicationInfoArr){
				$classname[$key] = $applicationInfoArr[0]; //classname
				$classno[$key] = $applicationInfoArr[1]; //classno
			}
			array_multisort($classname, SORT_ASC, $classno, SORT_ASC, $classno, $ExportArr);
			
			$filename = "club_approval_status";
			$filename = $filename.".csv";
		}
		else if ($type == 10) {
				//activity information		
				
//			$EventInfoArr = $libenroll->GET_EVENTINFO($EnrolEventID);
//			$libenroll->hasAccessRight($_SESSION['UserID'], 'Activity_Admin', $EnrolEventID);
						
			# TABLE INFO
			$sql = $libenroll->Get_Management_Activity_Participant_Enrollment_Sql('', '', '', $ForExport=1,1);
			$result = $libenroll->returnResultSet($sql);
			$NumOfResult = count($result);

			for($i=0; $i<$NumOfResult; $i++){	 

				$ExportArr[$i][0] = $result[$i]['ClassName'];	//className,classNumber
				$ExportArr[$i][1] = $result[$i]['ClassNumber'];	//className,classNumber
				$ExportArr[$i][2] = $result[$i]['UserLogin'];	
				$ExportArr[$i][3] = $result[$i]['EnglishName'];	//English Name
				$ExportArr[$i][4] = $result[$i]['ChineseName'];	//Chinese Name
				$ExportArr[$i][5] = $result[$i]['EventTitle'];	//Event Name
				$ExportArr[$i][6] = $result[$i]['RecordStatus'];	//Status
				$ExportArr[$i][7] = $result[$i]['DateInput'];	//Last Submission Time
	
				# Enrol Reason Customization 
				if($libenroll->enableActivityFillInEnrolReasonRight()){
					$ExportArr[$i][8] = $result[$i]['Reason'];	//Reason
	
				}
				
			}
			
			//define column title
			$exportColumn = array();
			$exportColumn[] = $i_general_class;
			$exportColumn[] = $i_ClassNumber;
			$exportColumn[] = $Lang['AccountMgmt']['StudentImportFields']['0'];
			$exportColumn[] = $i_UserEnglishName;
			$exportColumn[] = $i_UserChineseName;
			$exportColumn[] = $i_ActivityName;
			$exportColumn[] = $i_general_status;
			$exportColumn[] = $i_ClubsEnrollment_LastSubmissionTime;
			
			if($libenroll->enableActivityFillInEnrolReasonRight()){
				$exportColumn[] = $Lang['eEnrolment']['Reason'];
			}
		
			$filename = "applicant_list_of_activities";
			$filename = $filename.".csv";
		}elseif($type == 11){
			
			$importType = $_GET['importType'];
			

			$filename = "import_all_participant_sample_unicode.csv";
		
// 			if($libenroll->enableActivityFillInEnrolReasonRight()){
			
			$exportColumn = array();
			if ($importType=="activityAllParticipant"){
				$exportColumn[] = "ActivityTitle";
			}//else -> activityPacticipant
			$exportColumn[] = "ClassName";
			$exportColumn[] = "ClassNumber";
			$exportColumn[] = "UserLogin";
			$exportColumn[] = "Role";
			$exportColumn[] = "Performance";
			$exportColumn[] = "Achievement";
			$exportColumn[] = "Comment";
			$exportColumn[] = "Active/Inactive";
			$exportColumn[] = "AdminRole";
			if ($importType=="activityAllParticipant"){
				
				if($libenroll->enableActivityFillInEnrolReasonRight()){
					$exportColumn[] = "Reason";
				}
			}
			
			if($libenroll->enableActivityRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$exportColumn[] = "Merit";
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$exportColumn[] = "MinorMerit";
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$exportColumn[] = "MajorMerit";
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$exportColumn[] = "SuperMerit";
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$exportColumn[] = "UltraMerit";
				}
			}
				
			$rowCount = 0;
			$ExportArr = array();
			if ($importType=="activityAllParticipant"){
				$ExportArr[$rowCount][] = "Reading day";
			}
			$ExportArr[$rowCount][] = "1A";
			$ExportArr[$rowCount][] = "4";
			$ExportArr[$rowCount][] = "s1101234";
			$ExportArr[$rowCount][] = "Member";
			$ExportArr[$rowCount][] = "Good";
			$ExportArr[$rowCount][] = "Certificate of award";
			$ExportArr[$rowCount][] = "Active participation";
			$ExportArr[$rowCount][] = "Active";
			$ExportArr[$rowCount][] = "PIC";
			
			if ($importType=="activityAllParticipant"){
				if($libenroll->enableActivityFillInEnrolReasonRight() ){
				$ExportArr[$rowCount][] = "Sick";
				}
			}
			
			if($libenroll->enableActivityRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ExportArr[$rowCount][] = 5;
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ExportArr[$rowCount][] = 3;
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ExportArr[$rowCount][] = 0.5;
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ExportArr[$rowCount][] = 0.25;
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ExportArr[$rowCount][] = 1;
				}
			}
			$rowCount++;
			
			if ($importType=="activityAllParticipant"){
					$ExportArr[$rowCount][] = "Football match";
			}
			$ExportArr[$rowCount][] = "5B";
			$ExportArr[$rowCount][] = "12";
			$ExportArr[$rowCount][] = "s1302367";
			$ExportArr[$rowCount][] = "Chairman";
			$ExportArr[$rowCount][] = "Unsatisfactory";
			$ExportArr[$rowCount][] = "";
			$ExportArr[$rowCount][] = "Always late";
			$ExportArr[$rowCount][] = "Inactive";
			$ExportArr[$rowCount][] = "HELPER";
			if ($importType=="activityAllParticipant"){
				if($libenroll->enableActivityFillInEnrolReasonRight() ){
				$ExportArr[$rowCount][] = "Have Duty";
				}
			}
			
			if($libenroll->enableActivityRecordMeritInfoRight()){
				if (!$lstudentprofile->is_merit_disabled) {
					$ExportArr[$rowCount][] = 3;
				}
				if (!$lstudentprofile->is_min_merit_disabled) {
					$ExportArr[$rowCount][] = 2;
				}
				if (!$lstudentprofile->is_maj_merit_disabled) {
					$ExportArr[$rowCount][] = 0.25;
				}
				if (!$lstudentprofile->is_sup_merit_disabled) {
					$ExportArr[$rowCount][] = 1;
				}
				if (!$lstudentprofile->is_ult_merit_disabled) {
					$ExportArr[$rowCount][] = 0.25;
				}
			}
			$rowCount++;
			
		} else if ($type == 12) {
		    $importType = $_GET['importType'];
		    
		    if ($importType == 'clubAllMember') {
		        $filename = "import_all_member_sample_unicode.csv";
		    }
		    else {
		        $filename = "import_member_sample_unicode.csv";
		    }
		    
		    //get school year term
		    $sqlTemp = "SELECT YearTermID, YearTermNameEN as TermName FROM ACADEMIC_YEAR_TERM WHERE AcademicYearID = '".$AcademicYearID."'";
		    $termAry = $libenroll->returnResultSet($sqlTemp);
		    $numOfTerm = count($termAry);
		    
		    $exportColumn = array();
		    if ($importType == 'clubAllMember') {
		        $exportColumn[] = "ClubName(EN)";
		        $exportColumn[] = "Term";
		    }
		    $exportColumn[] = "ClassName";
		    $exportColumn[] = "ClassNumber";
		    $exportColumn[] = "UserLogin";
		    $exportColumn[] = "Role";
		    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
		        for ($i = 0; $i < $numOfTerm; $i ++) {
		            $exportColumn[] = str_replace(' ', '', $termAry[$i]["TermName"] . "Performance");
		        }
		    }
		    $exportColumn[] = "Performance";
		    $exportColumn[] = "Achievement";
		    $exportColumn[] = "Comment";
		    $exportColumn[] = "Active/Inactive";
		    $exportColumn[] = "AdminRole";
		    if($libenroll->enableClubRecordMeritInfoRight()){
		        if (!$lstudentprofile->is_merit_disabled) {
		            $exportColumn[] = "Merit";
		        }
		        if (!$lstudentprofile->is_min_merit_disabled) {
		            $exportColumn[] = "MinorMerit";
		        }
		        if (!$lstudentprofile->is_maj_merit_disabled) {
		            $exportColumn[] = "MajorMerit";
		        }
		        if (!$lstudentprofile->is_sup_merit_disabled) {
		            $exportColumn[] = "SuperMerit";
		        }
		        if (!$lstudentprofile->is_ult_merit_disabled) {
		            $exportColumn[] = "UltraMerit";
		        }
		    }
		    
		    $rowCount = 0;
		    $ExportArr = array();
		    if ($importType == 'clubAllMember') {
		        $ExportArr[$rowCount][] = "Reading Club";
		        $ExportArr[$rowCount][] = "Term 1";
		    }
		    $ExportArr[$rowCount][] = "1A";
		    $ExportArr[$rowCount][] = "4";
		    $ExportArr[$rowCount][] = "s1101234";
		    $ExportArr[$rowCount][] = "Member";
		    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
		        if($numOfTerm == 2){
		            $ExportArr[$rowCount][] = "B";
		            $ExportArr[$rowCount][] = "C";
		        }elseif($numOfTerm == 3){
		            $ExportArr[$rowCount][] = "B";
		            $ExportArr[$rowCount][] = "C";
		            $ExportArr[$rowCount][] = "D";
		        }
		    }
		    $ExportArr[$rowCount][] = "Good";
		    $ExportArr[$rowCount][] = "Certificate of award";
		    $ExportArr[$rowCount][] = "Active participation";
		    $ExportArr[$rowCount][] = "Active";
		    $ExportArr[$rowCount][] = "PIC";
		    if($libenroll->enableClubRecordMeritInfoRight()){
		        if (!$lstudentprofile->is_merit_disabled) {
		            $ExportArr[$rowCount][] = 5;
		        }
		        if (!$lstudentprofile->is_min_merit_disabled) {
		            $ExportArr[$rowCount][] = 3;
		        }
		        if (!$lstudentprofile->is_maj_merit_disabled) {
		            $ExportArr[$rowCount][] = 0.5;
		        }
		        if (!$lstudentprofile->is_sup_merit_disabled) {
		            $ExportArr[$rowCount][] = 0.25;
		        }
		        if (!$lstudentprofile->is_ult_merit_disabled) {
		            $ExportArr[$rowCount][] = 1;
		        }
		    }
		    $rowCount++;
		    
		    if ($importType == 'clubAllMember') {
		        $ExportArr[$rowCount][] = "Choir";
		        $ExportArr[$rowCount][] = "Term 2";
		    }
		    $ExportArr[$rowCount][] = "5B";
		    $ExportArr[$rowCount][] = "12";
		    $ExportArr[$rowCount][] = "s1302367";
		    $ExportArr[$rowCount][] = "Chairman";
		    if ($sys_custom['eEnrolment']['TermBasedPerformanceForClub'] && $sys_custom['eEnrolment']['ImportTermBasedPerformanceForClub']) {
		        if($numOfTerm == 2){
		            $ExportArr[$rowCount][] = "B";
		            $ExportArr[$rowCount][] = "C";
		        }elseif($numOfTerm == 3){
		            $ExportArr[$rowCount][] = "B";
		            $ExportArr[$rowCount][] = "C";
		            $ExportArr[$rowCount][] = "D";
		        }
		    }
		    $ExportArr[$rowCount][] = "Unsatisfactory";
		    $ExportArr[$rowCount][] = "";
		    $ExportArr[$rowCount][] = "Always late";
		    $ExportArr[$rowCount][] = "Inactive";
		    $ExportArr[$rowCount][] = "HELPER";
		    if($libenroll->enableClubRecordMeritInfoRight()){
		        if (!$lstudentprofile->is_merit_disabled) {
		            $ExportArr[$rowCount][] = 3;
		        }
		        if (!$lstudentprofile->is_min_merit_disabled) {
		            $ExportArr[$rowCount][] = 2;
		        }
		        if (!$lstudentprofile->is_maj_merit_disabled) {
		            $ExportArr[$rowCount][] = 0.25;
		        }
		        if (!$lstudentprofile->is_sup_merit_disabled) {
		            $ExportArr[$rowCount][] = 1;
		        }
		        if (!$lstudentprofile->is_ult_merit_disabled) {
		            $ExportArr[$rowCount][] = 0.25;
		        }
		    }
		    $rowCount++;
		    
		}else if ($type == 13) {
		    # UserLogin
		    # ChineseName
		    # EnglishName
		    # ClassName
		    # ClassNumner
		    # Max
		    
		    $EnrolGroupID = $_REQUEST['EnrolGroupID'];
		    
		    if (isset($EnrolGroupID)) {
		        $EnrolGroupIDArr[] = $EnrolGroupID;
		    } else {
		        $sql = "SELECT EnrolGroupID FROM INTRANET_ENROL_GROUPINFO WHERE RecordStatus = 1";
		        $tempEnrolGroupIDArr = $libenroll->returnArray($sql);
		        $EnrolGroupIDArr = Get_Array_By_Key($tempEnrolGroupIDArr, 'EnrolGroupID');
		    }
		    
		    // Loop Club[Start]
		    $rowNo = 0;
		    $ExportArr = array();
		    $studentInExportArr = array();
		    $filename = "number_of_clubs_wish_to_join.csv";
		    //             foreach ($EnrolGroupIDArr as $_EnrolGroupID) {
		    //                 $GroupID = $libenroll->GET_GROUPID($_EnrolGroupID);
		    //                 $li = new libgroup($GroupID);
		    // $title = intranet_undo_htmlspecialchars($li->Title);
		    // $filename = "applicant_list_of_".$title.".csv";3
		    // $filename = "club_applicant_list.csv";
		    
		    if ((! $libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (! $libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (! $libenroll->IS_CLUB_PIC($_EnrolGroupID))) {
		        No_Access_Right_Pop_Up();
		    }
		    
		    // $field_array = array("Class","a.EnglishName","a.ChineseName","b.RecordStatus","b.DateInput","a.UserID");
		    $field_array = array(
		        "a.ClassName",
		        "a.ClassNumber",
		        "a.EnglishName",
		        "a.ChineseName",
		        "b.RecordStatus",
		        "b.DateInput"
		    );
		    if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
		        $field_array[] = "b.ApplicantInfo";
		    }
		    $field_array[] = "a.UserID";
		    
		    $applicantInfoField = '';
		    if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
		        $applicantInfoField = "b.ApplicantInfo, ";
		    }
		    if ($keyword != '') {
		        $keyword_conds = "AND (a.EnglishName LIKE '%" . $keyword . "%' OR a.ChineseName LIKE '%" . $keyword . "%') ";
		    }
		    $EnrolGroupID = implode(",", $EnrolGroupIDArr);
		    
		    if($sys_custom['eEnrollment']['setTargetForm']){
		        $sql = "SELECT YearID FROM YEAR WHERE YearName IN ('". str_replace(",", "','", $libenroll->settingTargetForm)."')";
		        $yearIDArr = $libenroll->returnVector($sql);
		        
		        $sql = "SELECT ClassTitleEN FROM YEAR_CLASS WHERE YearID IN ('".implode("','", $yearIDArr)."')";
		        $targetFormClassName = $libenroll->returnVector($sql);
		        
		        $conds_targetFormClassName = "AND a.ClassName IN ('".implode("','", $targetFormClassName)."')";
		    }
		    
		    $sql = "SELECT
				              a.ClassName,
                              a.ClassNumber,
                              a.EnglishName,
				              a.ChineseName,
							  b.StudentID,
							  a.UserLogin as UserLogin
				        FROM
							  INTRANET_ENROL_GROUPSTUDENT as b
							  Inner Join INTRANET_USER as a On (b.StudentID = a.UserID)
                        WHERE
                            b.EnrolGroupID IN (".$EnrolGroupID.")
                            $keyword_conds
                            $conds_targetFormClassName
                        GROUP BY
                            b.StudentID
                        ORDER BY
                            a.ClassName, a.ClassNumber, UserLogin
				        ";
                            
                            $result = $libenroll->returnArray($sql);
                            
                            if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
                                include_once ($PATH_WRT_ROOT . "includes/libclubsenrol_cust.php");
                                $libenroll_cust = new libclubsenrol_cust();
                                
                                $studentIdAry = array_values(array_unique((Get_Array_By_Key($result, 'StudentID'))));
                                $extraInfoAry = $libenroll_cust->Get_Student_Extra_Info($enrolConfigAry['Club'], $studentIdAry);
                                $extraInfoAssoAry = BuildMultiKeyAssoc($extraInfoAry, 'StudentID');
                                unset($extraInfoAry);
                            }
                            // Get Category Setting
                            $useCAT = $libenroll->UseCategorySetting;
                            
                            // Export Data
                            for ($i = 0; $i < sizeof($result); $i ++) {
                                $_studentId = $result[$i]['StudentID'];
                                if (in_array($_studentId, $studentInExportArr)) {
                                    continue;
                                }
                                $studentInExportArr[] = $_studentId;
                                
                                // Priority Export
                                if ($priority) {
                                    // Get student applied club information
                                    $studentEnrollmentInfoAry = $libenroll->Get_Student_Applied_Club_Info($_studentId);
                                    $noOfEnrollmentResult = count($studentEnrollmentInfoAry);
                                    
                                    $checked = 0;
                                    
                                    // Data of applied club
                                    for ($j = 0; $j < $noOfEnrollmentResult; $j ++) {
                                        if (! in_array($studentEnrollmentInfoAry[$j]['EnrolGroupID'], $EnrolGroupIDArr)) {
                                            // skip club that is not opened online #L103192
                                            continue;
                                        }
                                        
                                        if($checked == 0){
                                            $sql = "SELECT Max FROM INTRANET_ENROL_STUDENT WHERE StudentID = ". $result[$i]['StudentID'];
                                            $max = $libenroll->returnVector($sql);
                                            
                                            $ExportArr[$rowNo][] = $result[$i]['ClassName']; // ClassName
                                            $ExportArr[$rowNo][] = $result[$i]['ClassNumber']; // ClassNumber
                                            $ExportArr[$rowNo][] = $result[$i]['UserLogin']; // UserLogin
                                            $ExportArr[$rowNo][] = $result[$i]['EnglishName']; // English Name
                                            $ExportArr[$rowNo][] = $result[$i]['ChineseName']; // Chinese Name
                                            
                                            if($max[0] != "")
                                                $ExportArr[$rowNo][] = $max[0]; // max club student want
                                            else
                                                $ExportArr[$rowNo][] = "--";
                                                        
                                            $checked = 1;
                                            $rowNo ++;
                                        }
                                    }
                                } else {
                                    $ExportArr[$rowNo][] = $result[$i]['Class']; // ClassName & classNumber
                                    $ExportArr[$rowNo][] = $result[$i]['UserLogin']; // UserLogin
                                    $ExportArr[$rowNo][] = $result[$i]['EnglishName']; // English Name
                                    $ExportArr[$rowNo][] = $result[$i]['ChineseName']; // Chinese Name
                                    // $ExportArr[$rowNo][] = $result[$i]['RecordStatus']; // Status
                                    
                                    if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
                                        $ExportArr[$rowNo][] = $result[$i]['ApplicantInfo']; // ApplicantInfo
                                    }
                                    
                                    if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
                                        $ExportArr[$rowNo][] = $extraInfoAssoAry[$_studentId]['EmergencyPhone'];
                                        $ExportArr[$rowNo][] = $extraInfoAssoAry[$_studentId]['FatherName'];
                                        $ExportArr[$rowNo][] = $extraInfoAssoAry[$_studentId]['FatherPhone'];
                                        $ExportArr[$rowNo][] = $extraInfoAssoAry[$_studentId]['MotherName'];
                                        $ExportArr[$rowNo][] = $extraInfoAssoAry[$_studentId]['MotherPhone'];
                                    }
                                    $ExportArr[$rowNo][] = $result[$i]['DateInput']; // Last Submission Time
                                    $rowNo ++;
                                }
                            }
                            //             }
                            
                            if ($rowNo > 0) {
                                // Sort array by classname,classno
                                foreach ($ExportArr as $key => $applicationInfoArr) {
                                    $classname[$key] = $applicationInfoArr[0]; // classname
                                    $classno[$key] = $applicationInfoArr[1]; // classno
                                }
                                
                                //                 array_multisort($classname, SORT_ASC, $classno, SORT_ASC, $classno, $ExportArr);
                            }
                            
                            // Loop Club[End]
                            // define column title
                            $exportColumn = array(
                                $i_ClassName,
                                $i_ClassNumber,
                                $Lang['eEnrolment']['AllClubMember_Import_FileDescription']['4'],
                                $i_UserEnglishName,
                                $i_UserChineseName,
                                $Lang['eEnrolment']['MaxClub']
                            );

                            if ($sys_custom['eEnrolment']['ApplicationExtraInfoTextarea']) {
                                $exportColumn[] = $Lang['eEnrolment']['ApplicantInformation'];
                            }
                            if ($sys_custom['eEnrolment']['StudentExtraInfo_Club']) {
                                $exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['EmergencyPhoneNumber'];
                                $exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['FatherName'];
                                $exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['FatherPhoneNumber'];
                                $exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['MotherName'];
                                $exportColumn[] = $Lang['eEnrolment']['StudentExtraInfoArr']['MotherPhoneNumber'];
                            }
                            if ($priority) {
                                if ($useCAT) {
                                    $exportColumn[] = $eEnrollment['category'];
                                }                         
                            }
           
		}

		//$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
		
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}

?>