<?php
// Editing by 
/*
 * 2014-04-11 (Carlos): $sys_custom['eEnrolment']['ActivityAddRole'] - add member role for Activity/Event
 */
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

if ($plugin['eEnrollment'])
{
	$libenroll = new libclubsenrol();
	
	$RoleID = $libenroll->ADD_ROLE(intranet_htmlspecialchars(trim($roleName)));
	
	//convert the array to a string to pass to another page
	if(sizeof($student)>0)
	{
		$student_list = implode(",",$student);
	}
	
	intranet_closedb();
	
	if($sys_custom['eEnrolment']['ActivityAddRole'] && $EnrolEventID != ''){
		header("Location: add_member.php?AcademicYearID=$AcademicYearID&EnrolEventID=$EnrolEventID&RoleTitle=".intranet_htmlspecialchars(trim($roleName))."&SelectedStudentID_list=$student_list&type=$type&field=$field&order=$order&filter=$filter&keyword=");
	}else{
		header("Location: add_member.php?GroupID=$GroupID&EnrolGroupID=$EnrolGroupID&RoleID=$RoleID&SelectedStudentID_list=$student_list");
	}
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>