<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();	
//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolEventID, "Event")))
if ( (!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']))
	 && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID']))
	 && (!$libenroll->IS_CLUB_PIC())
	 && (!$libenroll->IS_EVENT_PIC($EnrolEventID))
	)
	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	
$StudentArr['EnrolEventID'] = $EnrolEventID;
$StudentArr['ApplyUserType'] = $ApplyUserType;
$libenroll->UPDATE_APPLY_USERTYPE($StudentArr);

# add student to the activity if the user choose "select participants"
if ($ApplyUserType == "T") {	
	
	# construct a list of existing members
	$sql = "SELECT StudentID FROM INTRANET_ENROL_EVENTSTUDENT WHERE EnrolEventID = '$EnrolEventID'";
	$enrolledStuIDArr = $libenroll->returnVector($sql);
	$isEnrolled = array();
	for ($i=0; $i<sizeof($enrolledStuIDArr); $i++)
	{
		$isEnrolled[$enrolledStuIDArr[$i]] = true;
	}
	
	# construct a list of students who should be removed
	$deleteList = array();
	for ($i=0; $i<sizeof($enrolledStuIDArr); $i++)
	{
		if (!in_array($enrolledStuIDArr[$i],$student))
		{
			$deleteList[] = $enrolledStuIDArr[$i];
		}
	}
	
	# remove student in the delete list
	$libenroll->DEL_EVENTSTUDENT($EnrolEventID, $deleteList);
	$numDeleted = sizeof($deleteList);
	$sql = "UPDATE INTRANET_ENROL_EVENTINFO SET Approved = Approved - $numDeleted WHERE (EnrolEventID = '$EnrolEventID')";
	$libenroll->db_db_query($sql);
	
	# add students in the list selected by the user
	$StudentArr['RecordStatus'] = 2;
	for ($i = 0; $i < sizeof($student); $i++) {
		$StudentArr['StudentID'] = $student[$i];
		
		# add student who has not joined the activity yet
		if (!$isEnrolled[$student[$i]])
		{
			$LibUser = new libuser($student[$i]);
			$crash = false;		
				
			// get enroll list, check for this $EnrolEventID and enrolled
			$StudentEnrolledList = $libenroll->GET_STUDENT_ENROLLED_EVENT_LIST($student[$i]);
			for ($j = 0; $j < sizeof($StudentEnrolledList); $j++) {
				if ($libenroll->IS_EVENT_CRASH($EnrolEventID, $StudentEnrolledList[$j][0])) {
					$crash = true;
					continue;
				}
			}		
			
			// get student's enrolled group list, check for any time crash
			$StudentClub = $libenroll->STUDENT_ENROLLED_CLUB($student[$i]);
			for ($j = 0; $j < sizeof($StudentClub); $j++) {
				if ($libenroll->IS_EVENT_GROUP_CRASH($StudentClub[$j][0], $EnrolEventID)) {
					$crash = true;
					continue;
				}
			}
			
			// check for time crash, and prevent add student
			if (!$crash) {
				$libenroll->ADD_EVENTSTUDENT($StudentArr);
				$sql = "UPDATE INTRANET_ENROL_EVENTINFO SET Approved = Approved + 1 WHERE (EnrolEventID = '$EnrolEventID')";
				$libenroll->db_db_query($sql);
			} else {
				$CrashStuArr[] = $student[$i];
			}
		}
	}
	
	if (sizeof($CrashStuArr) > 0) {
		
#####################################################################################################
#####################################################################################################
#####################################################################################################
#####################################################################################################
		
$PATH_WRT_ROOT = "../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
//include_once($PATH_WRT_ROOT."home/admin/enrollment/templang.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libaccount.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();	
	$CurrentPage = "PageMgtActEventAdd";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
	$lc = new libclubsenrol();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        $TAGS_OBJ[] = array($eEnrollmentMenu['mgt_event'], "", 1);
        		
        $linterface->LAYOUT_START();        
?>
<br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr><td class="tabletext" align="center">
<?= $eEnrollment['add_student_conflict_warning']?><br/>
</td></tr>
<tr><td class="tabletext">
<table id="html_body_frame" border="0" cellspacing="0" cellpadding="4" align="center">
<? for ($i = 0; $i < sizeof($CrashStuArr); $i++) { ?>
	<tr><td class="tabletext"><?= $LibUser->getNameWithClassNumber($CrashStuArr[$i])?></td></tr>
<? } ?>
</table>

</td></tr>

<tr><td>
<table width="96%" border="0" cellspacing="0" cellpadding="1" align="center">
<tr><td align="center" colspan="6">
<div style="padding-top: 5px">
<?= $linterface->GET_ACTION_BTN($eEnrollment['back_to_event_index'], "button", "self.location='event.php'")?>
</div>
</td></tr>
</table>
<br/>
</td></tr>

</table>


    <?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
#####################################################################################################
#####################################################################################################
#####################################################################################################
#####################################################################################################
		
	} else {
		intranet_closedb();
		header("Location: event.php?msg=2&pic_view=".$pic_view);
	}
	
} else {
	intranet_closedb();
	header("Location: event.php?msg=2&pic_view=".$pic_view);
}

?>