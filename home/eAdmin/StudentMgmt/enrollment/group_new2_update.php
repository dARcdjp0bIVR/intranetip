<?php

$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if (!$plugin['eEnrollment'])
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

$EnrolGroupID = $_REQUEST['EnrolGroupID'];
$GroupID = $libenroll->GET_GROUPID($EnrolGroupID);

if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_PIC($_SESSION['UserID'], $EnrolGroupID, "Club")))
		header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");

$GroupArr['EnrolGroupID'] = $EnrolGroupID;

# NOT necessary to delete
//$libenroll->DEL_GROUPSTUDENT($GroupID);

# inactive the date instead of deleting it
//$libenroll->DEL_ENROL_GROUP_DATE($GroupArr['EnrolGroupID']);
$libenroll->INACTIVATE_ALL_ENROL_GROUP_DATE($EnrolGroupID);


for ($i = 0; $i < sizeof($tableDates); $i++)
{
	$GroupArr['ActivityDateStart'] = $tableDates[$i]." ".$StartHour[$i].":".$StartMin[$i].":00";
	$GroupArr['ActivityDateEnd'] = $tableDates[$i]." ".$EndHour[$i].":".$EndMin[$i].":00";
	
	$sql = "SELECT GroupDateID FROM INTRANET_ENROL_GROUP_DATE 
			WHERE EnrolGroupID = '".$GroupArr['EnrolGroupID']."' 
					AND ActivityDateStart = '".$GroupArr['ActivityDateStart']."' 
					AND ActivityDateEnd = '".$GroupArr['ActivityDateEnd']."'";
	$result = $libenroll->returnArray($sql,1);
	
	if ($result == NULL)
	{
		$libenroll->ADD_ENROL_GROUP_DATE($GroupArr);
	}
	else
	{
		$libenroll->ACTIVATE_ENROL_GROUP_DATE($result[0]['GroupDateID']);
	}
		
}

intranet_closedb();
$link = "group.php?msg=2&pic_view=$pic_view&Semester=$Semester";
//if ($page_link != "") $link = $page_link;
header("Location: $link");
?>