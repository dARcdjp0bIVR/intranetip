<?php
## Using By :  anna
########################################
##	Modification Log:
##  2018-08-21 Anna
##  - Added $sys_custom['eEnrolment']['OptionalClubChineseName']
##
##  2018-08-06 Cameron
##  - hide items for HKPF
##
##	2017-12-14 Pun
##	- Add NCS cust
##
##	2017-09-29 Anna
##	- Add submit disable after click
##
##	2014-10-14 Pun
##	- Add round report for SIS customization
##
##	2010-12-14 YatWoon
##	- IP25 UI standard
##	- add club Chinese name
##
##	20100114 Max (201001141638)
##	- Change the wording from "Rights to post public Announcements/Events/Surveys" to "Can use public Announcements/Events"
##	- Remove "No display on Organization Page" row
##	- Remove "Rights to use club tools" row
########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
intranet_auth();
intranet_opendb();

$libenroll = new libclubsenrol();
if(!$plugin['eEnrollment'] || !$libenroll->IS_ENROL_ADMIN($_SESSION['UserID']) && !$libenroll->IS_ENROL_MASTER($_SESSION['UserID']) )
{
	include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT();
	exit;
}

include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libgroup.php");
include_once($PATH_WRT_ROOT."includes/libgroupcategory.php");
include_once($PATH_WRT_ROOT."includes/libalbum.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

if($plugin['SIS_eEnrollment']){
	include_once($PATH_WRT_ROOT."lang/cust/SIS_eEnrollment_lang.$intranet_session_language.php");
}

$AcademicYearID = $_GET['AcademicYearID'];
if ($AcademicYearID == '') {
	$AcademicYearID = Get_Current_Academic_Year_ID();
}

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment'])
{
	//if ((!$libenroll->IS_ENROL_ADMIN($_SESSION['UserID'])) && (!$libenroll->IS_ENROL_MASTER($_SESSION['UserID'])))
	//	header("Location: $PATH_WRT_ROOT/home/eAdmin/StudentMgmt/enrollment/");
	$CurrentPage = "PageMgtClub";
	$CurrentPageArr['eEnrolment'] = 1;
	$MODULE_OBJ = $libenroll->GET_MODULE_OBJ_ARR();
	
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
        include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

        $linterface = new interface_html();
        //$TAGS_OBJ[] = array($eEnrollmentMenu['gro_setting'], "", 1);
        # tags 
	    $tab_type = "club";
	    $current_tab = 1;
	    include_once("management_tabs.php");

        $linterface->LAYOUT_START();

		$PAGE_NAVIGATION[] = array($button_new." ".$eEnrollment['club'], "");
		
//$filter = ($filter == 0) ? 1 : $filter;
//$filter = 5;
$lg = new libgroup();
$lgc = new libgroupcategory(5);
$la = new libalbum();
$availableTools = $lg->getSelectAvailableFunctions();

### Semester Checkboxes

$sem_ary = getSemesters($AcademicYearID);

$SemCheckedTable = "<table border='0' cellspacing='0' cellpadding='0' class='inside_form_table'>";
$SemCheckedTable .= "<tr><td class='tabletext'><input type='checkbox' name='Sem_all' id='Sem_all' onClick=(this.checked)?setChecked(1,this.form,'Sem[]'):setChecked(0,this.form,'Sem[]')> <label for='Sem_all'>".$Lang['Btn']['SelectAll']."</label></td></tr>";
$si = 0;

foreach($sem_ary as $SemID => $SemName)
{
	$SemCheckedTable .= !($si%4) ? "<tr>" : "";
	$SemCheckedTable .= "<td class='tabletext'><input type='checkbox' name='Sem[]' value='{$SemID}' id='Sem_".$si."' onClick='unset_checkall(this, \"Sem_all\");'> <label for='Sem_".$si."'>".$SemName."</label></td>";
	$si++;
	$SemCheckedTable .= !($si%4) ? "</tr>" : "";
}
$SemCheckedTable .= "</table>";

?>

<script language="javascript">
$(document).ready(function () {
    $("#form1").submit(function () {
        $(".submit").attr("disabled", true);
        return true;
    });
});
function checkform(obj)
{	
	var error_no = 0;
	var focus_field = "";
	
	//// Reset div innerHtml
	reset_innerHtml();
	
	//isDuplicateNameAndCode(); 
	//return false;
	
	if(!check_text_30(obj.Title, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['ClubNameEn']; ?>.", "div_Title_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Title";
	}
	 
	<? if(!$sys_custom['eEnrolment']['OptionalClubChineseName']){?>
    	if(!check_text_30(obj.TitleChinese, "<?php echo $i_alert_pleasefillin.$Lang['eEnrolment']['ClubNameCh']; ?>.", "div_TitleChinese_err_msg"))
    	{
    		error_no++;
    		if(focus_field=="")	focus_field = "TitleChinese";
    	}
	<? }?>


<?php if (!$sys_custom['project']['HKPF']):?>	
	if(!check_positive_int_30(obj.Quota, "<?php echo $i_GroupQuotaIsInt; ?>.", 0,0, "div_Quota_err_msg"))
	{
		error_no++;
		if(focus_field=="")	focus_field = "Quota";
	}
	
	if (document.getElementById('SemesterBaseRadio').checked == true)
	{
		if (check_checkbox(document.form1, 'Sem[]') == false)
		{
			document.getElementById('div_Semester_err_msg').innerHTML = '<font color="red"><?=$eEnrollment['js_select_semester']?></font>';
			error_no++;
			focus_field = "Sem_all";
			return false;
		}
	} 
<?php endif;?>

	if(error_no>0)
	{
		eval("obj." + focus_field +".focus();");
		return false;
	}
	else
	{
<?php if (!$sys_custom['project']['HKPF']):?>		
		if(confirm("<?=$Lang["eEnrolment"]["DirectToSetting"]?>"))
		{
			obj.DirectToSetting.value = 1;
		} 
<?php endif;?>	
		return true
	}
}

function click_reset()
{
<?php if (!$sys_custom['project']['HKPF']):?>	
	document.getElementById('SemesterBase_SemesterSelection_Tr').style.display = "none";
<?php endif;?>	
	reset_innerHtml();
	document.form1.reset();
}

function allToolsChecked(obj)
{
         var val;
         var i=0;
         len=obj.elements.length;
         if (obj.alltools.checked)
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=true;
                      obj.elements[i].checked=true;
                  }
             }
         }
         else
         {
             for( i=0 ; i<len ; i++)
             {
                  if (obj.elements[i].name=='grouptools[]')
                  {
                      obj.elements[i].disabled=false;
                  }
             }
         }
}

function unset_checkall(obj, objAll_ID)
{
	if (obj.checked == false)
		document.getElementById(objAll_ID).checked = false;
}

function jsChangeClubType(ClubType)
{
	if (ClubType == 'Year')
	{
		//document.getElementById('SemesterBase_SemesterSelection_Tr').style.display = "none";
		//document.getElementById('SemesterBase_JoinOption_Tr').style.display = "none";
		//document.getElementById('YearBase_JoinOption_Tr').style.display = "";
		
		$('tr#SemesterBase_SemesterSelection_Tr').hide();
		$('tr#SemesterBase_JoinOption_Tr').hide();
		$('tr#YearBase_JoinOption_Tr').show();
		if($plugin['SIS_eEnrollment']){
			$('tr#RoundBase_JoinOption_Tr').hide();
		}
	}
	else if (ClubType == 'Semester')
	{
		//document.getElementById('SemesterBase_SemesterSelection_Tr').style.display = "";
		//document.getElementById('SemesterBase_JoinOption_Tr').style.display = "";
		//document.getElementById('YearBase_JoinOption_Tr').style.display = "none";
		
		$('tr#SemesterBase_SemesterSelection_Tr').show();
		$('tr#SemesterBase_JoinOption_Tr').show();
		$('tr#YearBase_JoinOption_Tr').hide();
		if($plugin['SIS_eEnrollment']){
			$('tr#RoundBase_JoinOption_Tr').hide();
		}
	}
	else if (ClubType == 'Round')
	{
		//document.getElementById('SemesterBase_SemesterSelection_Tr').style.display = "";
		//document.getElementById('SemesterBase_JoinOption_Tr').style.display = "";
		//document.getElementById('YearBase_JoinOption_Tr').style.display = "none";
		
		$('tr#SemesterBase_SemesterSelection_Tr').hide();
		$('tr#SemesterBase_JoinOption_Tr').hide();
		$('tr#YearBase_JoinOption_Tr').hide();
		$('tr#RoundBase_JoinOption_Tr').show();		
	}
}

function reset_innerHtml()
{
 	document.getElementById('div_Title_err_msg').innerHTML = "";
 	
 	<?php if(!$sys_custom['eEnrolment']['OptionalClubChineseName']){ ?>
 	document.getElementById('div_TitleChinese_err_msg').innerHTML = "";
 	<?php }?>
 	
<?php if (!$sys_custom['project']['HKPF']):?> 	
 	document.getElementById('div_Quota_err_msg').innerHTML = "";
 	document.getElementById('div_Semester_err_msg').innerHTML = "";
<?php endif;?> 	
}

function isDuplicateNameAndCode() {

	var PostVar = Get_Form_Values(document.getElementById("form1"));
	
	$.post(
		"ajax_check_duplicate_name_and_code.php", PostVar,
		function(ReturnData)
		{
			$("#tempdiv").html(ReturnData);
			return ReturnData;
		}
	);		
	
}


</script>
<div id='tempdiv'></div>
<form name="form1" action="group_new_update.php" method="post" onSubmit="return checkform(this);">

<?= $linterface->GET_NAVIGATION_IP25($PAGE_NAVIGATION) ?>

<div class="form_table">
<table class="form_table_v30">
<?php if (!$sys_custom['project']['HKPF']):?>
    <tr>
    	<td class="field_title"><?php echo $Lang['General']['AcademicYear']; ?></td>
    	<td><?=getAcademicYearByAcademicYearID($AcademicYearID)?></td>
    </tr>
<?php endif;?>
<tr>
	<td class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameEn']; ?></td>
	<td><input class="textboxtext" type="text" name="Title" size="30" maxlength="255"><br><span id='div_Title_err_msg'></span></td>
</tr>
<?php if($sys_custom['eEnrolment']['OptionalClubChineseName']){ ?>
<tr>
    <td class="field_title"><?php echo $Lang['eEnrolment']['ClubNameCh']; ?> </td>
	<td><input class="textboxtext" type="text" name="TitleChinese" size="30" maxlength="255"></td>
</tr>
    
<?php }else{?>
    <tr>
    <td class="field_title"><span class="tabletextrequire">*</span> <?php echo $Lang['eEnrolment']['ClubNameCh']; ?> </td>
	<td><input class="textboxtext" type="text" name="TitleChinese" size="30" maxlength="255"><br><span id='div_TitleChinese_err_msg'></span></td>
</tr>
   
<?php }?>

<tr>
	<td class="field_title"><?php echo $Lang['eEnrolment']['ClubCode']?> </td>
	<td><input class="textboxnum" type="text" name="ClubCode" size="50" maxlength="20" value=""></td>
</tr>
<tr>
	<td class="field_title"><?php echo $i_GroupDescription; ?></td>
	<td><?= $linterface->GET_TEXTAREA("Description", "")?></td>
</tr>
<?php if (!$sys_custom['project']['HKPF']):?>
    <tr>
    	<td class="field_title"><?php echo $i_GroupQuota; ?></td>
    	<td><input class="textboxnum" type="text" name="Quota" size="5" maxlength="5" value="5"><br><span id='div_Quota_err_msg'></span></td>
    </tr>
    <? if ($plugin['album']) { ?>
    <tr>
    	<td class="field_title"><?php echo $i_AlbumQuota; ?></td>
    	<td><input class="textboxnum" type="text" name="AlbumQuota" size="5" maxlength="5" value="5"></td>
    </tr>
    
    <tr>
    	<td class="field_title"><?php echo $i_AlbumAccessType; ?></td>
    	<td><?php echo $la->returnSelectAccessType("name=AllowedAccessType",true,0,$filter); ?></td>
    </tr>
    <? } ?>
    <? if ($special_announce_public_allowed) { ?>
    <tr>
    	<td class="field_title"><?php echo $Lang['Group']['CanUsePublicAnnouncementEvents']; ?></td>
    	<td><input TYPE="checkbox" NAME="AnnounceAllowed" VALUE="1"></td>
    </tr>
    <? } ?>
    <tr style="<?=(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!="")?'display: none;':'' ?>">
    	<td class="field_title"><?php echo $i_GroupRecordType; ?></td>
    	<td><?php echo $lgc->CategoryName; ?></td>
    </tr>
    
    <!-- Club Type: Year Base or Semester Base -->
    <?php if($plugin['SIS_eEnrollment']){ ?>
    	<tr id="RoundBase_JoinOption_Tr">
    		<td class="field_title"><?=$Lang['SIS_eEnrollment']['Round']?></td>
    		<td>
    			<?php 
    				foreach($Lang['SIS_eEnrollment']['editCCA']['Round'] as $value => $name) { 
    					if($value == '1'){
    						$checked = 'checked';
    					}else{
    						$checked = '';
    					}
    			?>
    				<input TYPE="radio" id="Round<?=$value?>" NAME="Round" VALUE="<?=$value?>" <?=$checked?>><label for="Round<?=$value?>"><?=$name?></label>
    			<?php 
    				} 
    			?>
    			<input type="hidden" name="ClubType" value="RoundBase"/>
    		</td>
    	</tr>
    	<?php $hideAllTr = ' style="display:none"';?>
    <?php } ?>
    	
    	<?php 
    	if(isset($_SESSION['ncs_role']) && $_SESSION['ncs_role']!=""){
    	    $hideAllTr = ' style="display:none"';
    	}
        ?>
    	
    	<tr <?=$hideAllTr?>>
    		<td class="field_title"><?php echo $eEnrollment['ClubType'] ?></td>
    		<td>
    			<?= $linterface->Get_Radio_Button('YearBaseRadio', 'ClubType', $value='YearBase', $checked=(($plugin['SIS_eEnrollment'])? 0 : 1), "", $eEnrollment['YearBased'], "jsChangeClubType('Year')"); ?>
    			<?= $linterface->Get_Radio_Button('SemesterBaseRadio', 'ClubType', $value='SemesterBase', $checked=0, "", $eEnrollment['SemesterBased'], "jsChangeClubType('Semester')"); ?>
    		</td>
    	</tr>
    
    	<tr id="YearBase_JoinOption_Tr" <?=$hideAllTr?>>
    		<td class="field_title"><?php echo $eEnrollment['only_allow_student_to_join_the_club_in_the_first_semester'] ?></td>
    		<td><input TYPE="checkbox" NAME="FirstSemEnrolOnly" VALUE="1"></td>
    	</tr>
    	
    	<tr id="SemesterBase_SemesterSelection_Tr" style="display:none">
    		<td class="field_title"><?php echo $Lang['General']['Term'] ?></td>
    		<td><?= $SemCheckedTable ?><span id='div_Semester_err_msg'></span></td>
    	</tr>
    	
    	<tr id="SemesterBase_JoinOption_Tr" style="display:none">
    		<td class="field_title"><?php echo $eEnrollment['enrol_same_club_once_only'] ?></td>
    		<td><input TYPE="checkbox" NAME="ApplyOnceOnly" VALUE="1"></td>
    	</tr>
<?php endif;?>    	
</table>

<?=$linterface->MandatoryField();?>

<div class="edit_bottom_v30">
<p class="spacer"></p>
<?= $linterface->GET_ACTION_BTN($button_submit, "submit")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_reset, "button","click_reset();")?>&nbsp;
<?= $linterface->GET_ACTION_BTN($button_cancel, "button", "self.location='group.php?Semseter=$Semester'")?>
<p class="spacer"></p>
</div>


</div>

<input type="hidden" name="eEnrollmentAddGroup" id="eEnrollmentAddGroup" value="1" />
<input type="hidden" name="Semester" id="Semester" value="<?=$Semester?>" />
<input type="hidden" name="DirectToSetting" id="DirectToSetting" />
<input type="hidden" name="AcademicYearID" id="AcademicYearID" value="<?=$AcademicYearID?>"/>
<input type="hidden" name="RecordType" value='5'>
<?php if ($sys_custom['project']['HKPF']):?>
	<input type="hidden" name="ClubType" value='YearBase'>
<?php endif;?>

</form>

<?= $linterface->FOCUS_ON_LOAD("form1.Title") ?>
<?
        $linterface->LAYOUT_STOP();
        intranet_closedb();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>

<?php
?>