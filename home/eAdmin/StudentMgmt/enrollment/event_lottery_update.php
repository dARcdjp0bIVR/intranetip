<?php
##	Modifying By :  
########################################
##	Modification Log:
##	2015-10-07:	Omas
##	- Added logic to check EnrolMax during activity enrollment period 
##
##	2015-06-12:	Evan
##	- Fix the bug of drawing in accordance with the rule of first com first served
##
##	2010-01-28: Max (201001271658)
##	- Append parents email to email recepient if $libenroll->Event_onceEmail == 1 
########################################
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libcampusmail.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
intranet_auth();
intranet_opendb();
$laccess = new libaccess();

if ($plugin['eEnrollment'])
{

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	$libenroll = new libclubsenrol();
	$MODULE_OBJ['title'] = $eEnrollment['drawing'];
    if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    $linterface = new interface_html("popup.html");

	    ################################################################

if ($libenroll->hasAccessRight($_SESSION['UserID']))
{

}
else
{
    echo "You have no priviledge to access this page.";
    exit();
}

$lc = new libclubsenrol();
$lclass = new libclass();

$EventInfoArr = $lc->GET_EVENTINFO($EnrolEventID);
# Get EnrolMax
if($libenroll->Event_UseCategorySetting){
	$EnrolSetting = $libenroll->Get_Category_Enrol_Setting($EventInfoArr['EventCategory']);
	$EnrolMax = $EnrolSetting[$EventInfoArr['EventCategory']]['Activity']['EnrollMax'];
}
else{
	$EnrolMax = $libenroll->Event_EnrollMax;
}

$i = 0;
//if ($lc->tiebreak == 0)         # Tiebreaker
if ($EventInfoArr['ApplyMethod']==0)
{
    $tiebreak_ord = "RAND()";
}
else  # 1
{
    //$tiebreak_ord = "DateModified";
    $tiebreak_ord = "DateInput";
}

$Quota = $libenroll->GET_EVENT_QUOTA_LEFT($EnrolEventID);

$Sql = "
			SELECT
					StudentID
			FROM
					INTRANET_ENROL_EVENTSTUDENT
			WHERE
					EnrolEventID = '$EnrolEventID'
				AND
					RecordStatus = 0
			ORDER BY
					$tiebreak_ord";
					
$TempStudents = $lc->returnArray($Sql,1);

$approvedStudents = array();
#### filter student, check for time crash to a new array ####

for ($checkCrashCount = 0; $checkCrashCount < sizeof($TempStudents); $checkCrashCount++) {	            
	
	# check quota left first
	if ($i == $Quota) break;
	
	$crash = 0;
	
	$StudentEvent = $lc->GET_STUDENT_ENROLLED_EVENT_LIST($TempStudents[$checkCrashCount][0]);            	
	for ($CountCrash = 0; $CountCrash < sizeof($StudentEvent); $CountCrash++) {
		if ($lc->IS_EVENT_CRASH($EnrolEventID, $StudentEvent[$CountCrash][0])) {
			$crash = 1;
			continue;
		}
	}

	// get enrolled group event, check if crash
	if (!$crash) {
		$StudentClub = $lc->STUDENT_ENROLLED_CLUB($TempStudents[$checkCrashCount][0]);
		for ($ClubCount = 0; $ClubCount < sizeof($StudentClub); $ClubCount++) {
			if ($lc->IS_EVENT_GROUP_CRASH($StudentClub[$ClubCount][0], $EnrolEventID)) {
				$crash = true;
				continue;
			}
		}
	}
	
	//check default school quota
	$reachLimit = false;
	if ( 	$libenroll->WITHIN_ENROLMENT_STAGE("activity") && 
			($libenroll->GET_STUDENT_NUMBER_OF_ENROLLED_EVENT($TempStudents[$checkCrashCount][0]) >= $EnrolMax && 
			$EnrolMax != 0) 	)
	{
		$reachLimit = true;
	}
	
	if ( (!$reachLimit) && (!$crash) && (!in_array($TempStudents[$checkCrashCount][0], $approvedStudents)) ) {
		$approvedStudents[] = $TempStudents[$checkCrashCount][0];
		$i++;
	}
	
}

#### filter student, check for time crash to a new array ####

$delimiter = "";
$enrolIDList = "";
$studentIDList = "";

$numApproved = sizeof($approvedStudents);
$quota_left = $lc->GET_EVENT_QUOTA_LEFT($EnrolEventID);
if (($quota_left != 0)&&($quota_left < $numApproved)) $numApproved = $quota_left;

for ($k = 0; $k < $numApproved; $k++) {
	$studentID = $approvedStudents[$k];
	$studentIDList .= "$delimiter $studentID";
	$delimiter = ",";
}
if ($numApproved > 0)
{
	$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET RecordStatus = 2 WHERE (EnrolEventID = '$EnrolEventID') and (StudentID IN ($studentIDList))";
	$lc->db_db_query($sql);
	
	$sql = "UPDATE INTRANET_ENROL_EVENTINFO SET Approved = Approved + $numApproved WHERE (EnrolEventID = '$EnrolEventID')";
	$lc->db_db_query($sql);
	
	$sql = "UPDATE INTRANET_ENROL_EVENTSTUDENT SET RecordStatus = '1' WHERE (EnrolEventID = '$EnrolEventID') and (RecordStatus = 0) ";
	$lc->db_db_query($sql);
}

// send email
if ($laccess->retrieveAccessCampusmailForType(2) && $libenroll->Event_onceEmail == 1)     # if can access campusmail and send email to student and parent is ticked
{

	$sql = "SELECT StudentID FROM INTRANET_ENROL_EVENTSTUDENT where EnrolEventID = '$EnrolEventID'";
	$students = $lc->returnArray($sql,1);

	for ($i=0; $i<sizeof($students); $i++)
	{
		# define record type
		$RecordType = "activity";
		
		# get student list
		$userArray = array();			
		$userArray[] = $students[$i][0];
		
		# get success result
		$successResult = false;
		$sql = "
	SELECT
		RecordStatus
	FROM
			INTRANET_ENROL_EVENTSTUDENT
	WHERE
			StudentID = '".$students[$i][0]."'
		AND
			EnrolEventID = '".$EnrolEventID."'";
		$choices = $lc->returnArray($sql,1);

		if ($choices[0][0] == 2) {
			$successResult = true;
		} else if ($choices[0][0] == 1){
			$successResult = false;
		} else {
			// do nothing
		}

		$libenroll->Send_Enrolment_Result_Email($userArray, $EnrolEventID, $RecordType, $successResult);

	}
	
	
}
intranet_closedb();

################################################################

$linterface->LAYOUT_START();

?>

<div style="height: 400px;">
<br/><br/><br/><br/>
<?=$i_ClubsEnrollment_LotteryFinished?><br/><br/>
<?= $linterface->GET_ACTION_BTN($button_close, "button", "javascript:self.close()")?>
</div>
<script type="text/javascript">
	window.opener.location.reload();
</script>

<?

        $linterface->LAYOUT_STOP();
    }
    else
    {
    ?>
You have no priviledge to access this page.
    <?
    }
}
else
{
?>
You have no priviledge to access this page.
<?
}
?>