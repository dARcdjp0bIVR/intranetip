<?php
$PATH_WRT_ROOT = "../../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

intranet_auth();
intranet_opendb();

$LibUser = new libuser($UserID);

if ($plugin['eEnrollment']) {

	include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
	include_once($PATH_WRT_ROOT."includes/libgroup.php");
	
	$libenroll = new libclubsenrol();
		
	if ($libenroll->hasAccessRight($_SESSION['UserID']))
    {
	    if (!isset($_GET)) {
			header("Location: index.php");
			exit();
		}
		
		$ExportArr = array();
		$lexport = new libexporttext();
					
		if ($type=="clubAttendance")
		{	//club attendance
			$EnrolGroupID = $libenroll->GET_ENROLGROUPID($GroupID);
			$filename = "club_attendance_sample.csv";		
			// get date data from the db and make the array
			$DateArr = $libenroll->GET_ENROL_GROUP_DATE($EnrolGroupID);	//GroupDateID, EnrolGroupID, ActivityDateStart, ActivityDateEnd
		}
		else if ($type=="activityAttendance")
		{
			$filename = "activity_attendance_sample.csv";	
			// get date data from the db and make the array
			$DateArr = $libenroll->GET_ENROL_EVENT_DATE($EnrolEventID);
		}
			
		// Construct sample data
		for($i=0; $i<2; $i++)
		{
			
			$ExportArr[$i][0] = "1A";
			$ExportArr[$i][1] = "0".strval($i+1);	//add a zero before the number
			
			for($j=0; $j<sizeof($DateArr); $j++)
			{
				if ((($j+$i) % 3) == 0)
				{
					$ExportArr[$i][$j+2] = "Present";
				}
				else if ((($j+$i) % 3) == 1)
				{
					$ExportArr[$i][$j+2] = "Absent";
				}
				else if ((($j+$i) % 3) == 2)
				{
					$ExportArr[$i][$j+2] = "Exempt";
				}
			}
		}		
		
		// construct a date title array
		for ($i = 0; $i < sizeof($DateArr); $i++) {
			$DateTitleArr[$i] .= date("Y-m-d", strtotime($DateArr[$i]['ActivityDateStart']));
			$DateTitleArr[$i] .= " ";
			$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i]['ActivityDateStart']));
			$DateTitleArr[$i] .= "-";
			$DateTitleArr[$i] .= date("H:i", strtotime($DateArr[$i]['ActivityDateEnd']));
		}
		
		//define column title
		$exportColumn[] = $eEnrollment['sample_csv']['ClassName'];
		$exportColumn[] = $eEnrollment['sample_csv']['ClassNumber'];
					
		for ($i = 0 ; $i < sizeof($DateTitleArr); $i++) {
			$exportColumn[] = $DateTitleArr[$i];
		}
		
		$export_content = $lexport->GET_EXPORT_TXT($ExportArr, $exportColumn);
		
		intranet_closedb();
		
		// Output the file to user browser
		$lexport->EXPORT_FILE($filename, $export_content);
	} else {
		echo "You have no priviledge to access this page.";
	}
} else {
	echo "You have no priviledge to access this page.";
}

?>
