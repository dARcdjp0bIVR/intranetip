<?php

/* *******************************************
 * Modification log
 * 		2013-8-27	Roy
 * 			- change to use retrieveStudentHouseInfo() to retrieve CURRENT house name
 * 
 * 
 * *******************************************/

# using: 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lexport = new libexporttext();

$targetEventID = array();
if(!empty($targetEventIDstr))	$targetEventID = explode(",",$targetEventIDstr);

$allGroupsTemp = $lsports->retrieveAgeGroupIDNames();
$allGroups = array();
foreach($allGroupsTemp as $k => $d)
	$allGroups[] = $d['AgeGroupID'];

$allGroups[] = -1;
$allGroups[] = -2;
$allGroups[] = -4;

$ageGroups = (isset($ageGroup) && $ageGroup) ? array($ageGroup) : $allGroups;

$export_header_ary = array($i_Sports_field_Group,$i_Sports_menu_Settings_TrackFieldEvent,$Lang['eSports']['EventCode'], $Lang['eSports']['Athlete'],$i_UserClassName,$i_UserClassNumber,
						$i_UserGender,$i_Sports_House, $i_Sports_Event_Set_CountIndividualScore,$i_Sports_Event_Set_CountClassScore,
						$i_Sports_Event_Set_CountHouseScore,$i_Sports_field_Rank,$Lang['eSports']['Result'],$Lang['eSports']['EnrolScore'],
						$Lang['eSports']['AttendanceScore'],$Lang['eSports']['RankScore'],$Lang['eSports']['RecordBrokenScore'],
						$Lang['eSports']['QualifiedScore'],$Lang['eSports']['TotalScore']);

$export_content_ary = array();
for($i=0;$i<sizeof($ageGroups);$i++)
{
	$ageGroup = $ageGroups[$i];
	
	$EventAry = $lsports->retrieveEventIDByAgeGroup($ageGroup);
	$GropuInfo = $lsports->retrieveAgeGroupDetail($ageGroup);
	$EventGroupInfo = $lsports->Get_EventGroup_Info();
	// debug_pr($EventAry);
	// debug_pr($GropuInfo);
	// debug_pr($EventGroupInfo);
	
// 	$EventCodeArr = BuildMultiKeyAssoc($EventGroupInfo, array("EventID","GroupID"),"EventCode",1);
	
	$GroupName = $intranet_session_language=="en"? $GropuInfo['EnglishName'] : $GropuInfo['ChineseName'];
	
	$GroupName = $ageGroup == -1 ? $i_Sports_Event_Boys_Open : $GroupName;
	$GroupName = $ageGroup == -2 ? $i_Sports_Event_Girls_Open : $GroupName;
	$GroupName = $ageGroup == -4 ? $i_Sports_Event_Mixed_Open : $GroupName;
	
	foreach($EventAry as $k=>$event)
	{
		list($EventID, $EventName, $EventType) = $event;
		
		if  ((!empty($targetEventID) && in_array($EventID, $targetEventID)) || empty($targetEventID))
		{
			# retrieve Event Group Details
			list($thisEventGroupID, $thisEventType)  = $lsports->retrieveEventGroupID($EventID, $ageGroup);
// 			$ExtInfo = $lsports->retrieveEventGroupExtInfo($thisEventGroupID, $thisEventType);
			$EventGroupDetails = $lsports->retrieveEventGroupDetail($thisEventGroupID);
			// debug_pr($EventGroupDetails);
			$thisEventCode = $EventGroupDetails['EventCode'];
			$hasSetStandard = $EventGroupDetails["StandardAsMS"];
			$thisCountIndividualScore = $EventGroupDetails['CountIndividualScore'] ? $Lang['General']['Yes'] : $Lang['General']['No'];
			$thisCountClassScore = $EventGroupDetails['CountClassScore'] ? $Lang['General']['Yes'] : $Lang['General']['No'];
			$thisCountHouseScore = $EventGroupDetails['CountHouseScore'] ? $Lang['General']['Yes'] : $Lang['General']['No'];

			# retrieve this event score
			$thisEventRankScore = $lsports->retrieveScoreStandardDetail($EventGroupDetails['ScoreStandardID']);
			
			if($EventType == 3 || $EventType==4)    continue;
	
			switch($EventType)
			{
				case 2:		// field event
					$result = $lsports->retrieveTFResult($EventID, $ageGroup);
					$BestResult=$lsports->returnBestRecordResultOfFieldEventGroup($thisEventGroupID); 
					break;
				case 3:	
					// no need to show house relay score (useless)
					//$result = $lsports->retrieveHRResult($EventID, $ageGroup);
					break;
				case 4:
					// no need to show class relay score (useless)
					//$result = $lsports->retrieveCRResult($EventID, $ageGroup);
					break;
				default:	// track event
					$result = $lsports->retrieveTFResult($EventID, $ageGroup);
					$BestResult=$lsports->returnBestRecordResultOfTrackEventGroup($thisEventGroupID); 
					break;
			}
			# retrieve the record broken is belongs to who
			$RecordBrokenStudentID = $BestResult[0]['StudentID'];
			// debug_pr($result);
// 			if($thisEventGroupID==77)
// 			{
// 				debug_pr($result);
// 				debug_pr($BestResult);
//				debug_pr($RecordBrokenStudentID);
// 				exit;
// 			}

			foreach($result as $sid=>$data)
			{
				
				$rank = "";
				$overall_score = 0;
				$rank_score = 0;
				$thisQualifiedScore = 0;
				
				
				# get round 1
				$rank = $data[1]['Rank'];
				$thisAttendScore = $data[1]['RecordStatus']==1 ? "-".$lsports->scoreAbsent : ($data[1]['RecordStatus']==6 ? 0 : $lsports->scorePresent);
				$thisQualifiedScore = $data[1]['RecordStatus']==3 || $data[1]['RecordStatus']==4 ? $thisEventRankScore['Qualified']:$thisQualifiedScore;
				$overall_score = $data[1]['Score'];
					
				# get round 2 (if 2 exists) [Second Record]
				$rank = $data[2]['Rank'] ? $data[2]['Rank'] : $rank;
				$thisQualifiedScore = $data[2]['RecordStatus']==3 || $data[2]['RecordStatus']==4 ? $thisEventRankScore['Qualified']:$thisQualifiedScore;
				$overall_score += $data[2]['Score'];
				
				# get round 0  (if 0 exists) [Final Round]
				$rank = $data[0]['Rank'] ? $data[0]['Rank'] : $rank;
				$thisQualifiedScore = $data[0]['RecordStatus']==3 || $data[0]['RecordStatus']==4 ? $thisEventRankScore['Qualified']:$thisQualifiedScore;
				$overall_score += $data[0]['Score'];
				
				// #########
// 				if($thisEventCode==77)
// 				{
// 					$lu = new libuser($sid);
// 					$student_name = $lu->UserNameLang();
// 					debug_pr("~~~~~~~~~~~~~~~~~~~~~~~".$student_name);
// 					debug_pr($data[1]['RecordStatus']);
// 					debug_pr($data[2]['RecordStatus']);
// 					debug_pr($data[0]['RecordStatus']);
// 				}
								
				if($rank)
				{
					$rank_score	= $thisEventRankScore[$rank];	
				}
				
				// for($j=0; $j<3; $j++)
				// {
						// $metre = $data[$j]['ResultMetre'];
						// $min = $data[$j]['ResultMin'];
						// $sec = $data[$j]['ResultSec'];
						// $ms = $data[$j]['ResultMs'];
						// if(!empty($metre) || (!empty($min)||!empty($sec)||!empty($ms)))
						// break;
				// }
				
				# get final result
				$metre = $data[0]['ResultMetre'];
				$min = $data[0]['ResultMin'];
				$sec = $data[0]['ResultSec'];
				$ms = $data[0]['ResultMs'];
				if (!empty($metre) || (!empty($min)||!empty($sec)||!empty($ms))) {
					# if final result exists, do nothing
				} else {
					# get latest result
					for ($j=2; $j>0; $j--) {
					$metre = $data[$j]['ResultMetre'];
					$min = $data[$j]['ResultMin'];
					$sec = $data[$j]['ResultSec'];
					$ms = $data[$j]['ResultMs'];
					if(!empty($metre) || (!empty($min)||!empty($sec)||!empty($ms)))
					break;
					}
				}
				
				if($metre)
					$event_result = $metre ."m";
				else if($min || $sec || $ms)
					$event_result = $lsports->Format_TimerArr(array($min,$sec,$ms));
				else
					$event_result = "";
				
				$lu = new libuser($sid);
				$student_name = $lu->UserNameLang();
				$class_name = $lu->ClassName;
				$class_number = $lu->ClassNumber;
				// $housename = $lsports->retrieveStudentHouse($sid);
				$house_info = $lsports->retrieveStudentHouseInfo($sid);
				$housename = $house_info[0][0];
				$student_gender = $lu->Gender;
 				$thisRecordBrokenScore = $RecordBrokenStudentID==$sid ? $thisEventRankScore['RecordBroken'] : 0;
				
// 				if($thisEventCode==17)
// 				{
// 				debug_pr("thisQualifiedScore: ". $thisQualifiedScore);
// 				debug_pr("<hr>");
// 				}
				
				$export_content_ary[] = array($GroupName,$EventName, $thisEventCode,$student_name,$class_name,$class_number,$student_gender,
											$housename, $thisCountIndividualScore,$thisCountClassScore,$thisCountHouseScore,
											$rank, $event_result, $lsports->scoreEnrol, $thisAttendScore, $rank_score,
											$thisRecordBrokenScore, $thisQualifiedScore, $overall_score); 
			}
		}
	}
}

// debug_pr($export_header_ary);
// exit;


	$filename = "all_result.csv";
	$export_content = $lexport->GET_EXPORT_TXT($export_content_ary, $export_header_ary);	
	
	//debug_pr($export_content);
	//exit;
	$lexport->EXPORT_FILE($filename, $export_content);

?>

