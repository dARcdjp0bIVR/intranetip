<?php
# using: 

########### Change Log [Start] #######
#
#   Date:   2019-07-17  Bill    [2019-0717-1119-51206]
#           fixed php error message
#
#   Date:   2018-10-19  Bill    [2018-1009-1038-48066]
#           trim() column data for csv export
#
#	Date:   2016-06-13 Cara
#			updated css style for data table
#
#	Date:	2012-09-26	YatWoon
#			- Add "All Classes" for selection [Case#2011-1117-1717-32132]
#			- Re-order the column 
#
#	Date:	2010-12-1 Marcus
#			Rewrite the whole summary, Add House, performance tuning(save queries)	
#
#	Date:	2010-05-28 YatWoon
#			Can select "Form" to view whole form's student	
#
########### Change Log [End] #######

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageParticipantRecord";

$lsports = new libsports();
$lclass = new libclass();
$lsports->authSportsSystem();
$fcm = new form_class_manage();

$thisClassID = $classid;

### default to the first class [start]
if(trim($thisClassID)=='')
{
	$obj_Year = new Year();
	$ClassArr = $obj_Year->Get_All_Classes();
	
	$thisClassID = "::".$ClassArr[0]['YearClassID'];
	$classid = $thisClassID;
}
### default to the first class [end]	
if($thisClassID=="0")					# all classes
{	
	$obj_Year = new Year();
	$ClassNameArr = BuildMultiKeyAssoc($obj_Year->Get_All_Classes(),"YearClassID",Get_Lang_Selection("ClassTitleB5","ClassTitleEN"),1);
	$Students = $fcm->Get_Active_Student_List();
		
	$export_classname = $i_general_all_classes;
}
elseif(substr($thisClassID,0,2)=="::")	# class
{	
	$thisClassID = str_replace("::","",$thisClassID);
	$obj_Class = new Year_Class($thisClassID);
	$className = $obj_Class->Get_Class_Name();
	$Students = $fcm->Get_Active_Student_List('',$thisClassID);

	$export_classname = $className;
}	
else									# whole form
{
 	$YearID = $thisClassID;
	$obj_Year = new Year($YearID);
	$ClassNameArr = BuildMultiKeyAssoc($obj_Year->Get_All_Classes(),"YearClassID",Get_Lang_Selection("ClassTitleB5","ClassTitleEN"),1);
	
	$Students = $fcm->Get_Active_Student_List($YearID);
	
	$export_classname = $obj_Year->YearName;
}
$selected_classid = $classid;

# prepare Student Basic Info
$StudentsIDArr = Get_Array_By_Key($Students,"UserID");
$StudentInfoArr = BuildMultiKeyAssoc($lsports->Get_Student_Basic_Info($StudentsIDArr),"UserID");
 
# Create Class Selection List
$class_select = $lclass->getSelectClassWithWholeForm("name=classid onChange='document.form2.action=\"class_enrol.php\";document.form2.submit()'", $selected_classid, $i_general_all_classes);

# Event Name Arr
$EventGroups = build_assoc_array($lsports->retrieveTrackFieldEventName());

# Event EventGroup Mapping
$EventGroupEventIDArr = BuildMultiKeyAssoc($lsports->Get_EventGroup_Info(), "EventGroupID", "EventID");

# use to count present and absent
$EnroledAttendanceArr = $lsports->Get_Student_EventGroup_Attendance($StudentsIDArr);

# AgeGroupInfo 
$AgeGroupInfo = $lsports->retrieveAgeGroupInfo();
$AgeGroupInfo = BuildMultiKeyAssoc($AgeGroupInfo, "AgeGroupID");

$export_content = "$i_Sports_menu_Report_ClassEnrolment\n\n";

# This part is to count the attendant rate
$totalPresent = 0;
$totalAbsent = 0;
$typeIDs = array(1,2);

foreach((array)$EnroledAttendanceArr as $thisStudentID => $EventGroupAttend)
{
	foreach((array)$EventGroupAttend as $thisEventGroupID => $Attend)
	{
		if($Attend==1)
			$totalPresent++;
		else if($Attend==2)
			$totalAbsent++;
			
		$thisEventID = $EventGroupEventIDArr[$thisEventGroupID]['EventID'];
		$StudentEventArr[$thisStudentID][] = $thisEventID; 
		
		if($sys_cust['eSports_display_group_ClassEnrolment'])
		{
			# retrieve GroupID
			$sql = "select b.GradeChar from 
					SPORTS_EVENTGROUP as a
					left join SPORTS_AGE_GROUP as b on b.AgeGroupID=a.GroupID
					where a.EventGroupID=$thisEventGroupID";
			$result = $lsports->returnVector($sql);
			$StudentEventArr_GroupID[$thisStudentID][$thisEventID] = $result[0];
		}
	}
}

# export / table start
### Table content
$content = "";
$export_content .= $Lang["eSports"]["Class"] ." / ". $Lang["eSports"]["Form"] ."\t";
$export_content .= trim($export_classname)."\n";
$export_content .= $i_Sports_Present."\t";
$export_content .= $totalPresent.$i_Sports_Count."\n";
$export_content .= $i_Sports_Absent."\t";
$export_content .= $totalAbsent.$i_Sports_Count."\n\n";

	$content .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
	$content .= "<tr>";
	$content .= "<td class='tablebluetop tabletopnolink'>$i_ClassName</td>";
	$content .= "<td class='tablebluetop tabletopnolink'>$i_ClassNumber</td>";
	$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_Participant</td>";
	$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_field_Gender</td>";
	$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_field_Group</td>";
	$content .= "<td class='tablebluetop tabletopnolink'>$i_House</td>";
	$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_menu_Settings_Participant</td>";

	$export_content .= $i_ClassName."\t";
	$export_content .= $i_ClassNumber."\t";
	$export_content .= $i_Sports_Participant."\t";
	$export_content .= $i_Sports_field_Gender."\t";
	$export_content .= $i_Sports_field_Group."\t";
	$export_content .= $i_House."\t";
	$export_content .= $i_Sports_menu_Settings_Participant."\t";

	$tab = "";
	foreach((array)$EventGroups as $thisEventID => $eventName)
	{
		$content .= "<td class='tablebluetop tabletopnolink' align='center'>".$eventName."</td>";
		$export_content .= $tab.trim($eventName);
		$tab = "\t";
	}

	$export_content .= "\n";
	$content .= "</tr>";
	
	
	foreach((array)$StudentInfoArr as $StudentID => $StudentInfo)
	{
		$thisHouseInfo= $lsports->retrieveHouseInfo($StudentInfo['HouseID']);
		$thisAgeGroupInfo = $AgeGroupInfo[$StudentInfo['AgeGroupID']];
		$NameLang = Get_Lang_Selection("ChineseName","EnglishName");
		$thisAgeGroupName = $thisAgeGroupInfo[$NameLang];
		
		$content .= "<tr class='tablebluerow".($j++%2?"2":"1")."'>";
		$content .= "<td class='tabletext'>".$StudentInfo['ClassName']."</td>";
		$content .= "<td class='tabletext'>". $StudentInfo['ClassNumber'] ."</td>";
		$content .= "<td class='tabletext'>".$StudentInfo[$NameLang]."</td>";
		$content .= "<td class='tabletext'>". $StudentInfo['Gender'] ."</td>";
		$content .= "<td class='tabletext'>". $thisAgeGroupName ."</td>";
		$content .= "<td class='tabletext'>". $thisHouseInfo['HouseName'] ."</td>";
		$content .= "<td class='tabletext'>".$StudentInfo['AthleticNum']."</td>";
		
		$export_content .= trim($StudentInfo['ClassName'])."\t"; 
		$export_content .= trim($StudentInfo['ClassNumber'])."\t";
		$export_content .= trim($StudentInfo[$NameLang])."\t";
		$export_content .= trim($StudentInfo['Gender'])."\t";
		$export_content .= trim($thisAgeGroupName)."\t";
		$export_content .= "\"".trim($thisHouseInfo['HouseName'])."\"\t";
		$export_content .= trim($StudentInfo['AthleticNum'])."\t";
		
		$tab = "";
		foreach((array)$EventGroups as $thisEventID => $eventName)
		{
			if(in_array($thisEventID,(array)$StudentEventArr[$StudentID]))
			{
				$Enrolled = "X";
				if($sys_cust['eSports_display_group_ClassEnrolment'])
				{
					$Enrolled .= " (".$StudentEventArr_GroupID[$StudentID][$thisEventID].")";
				}
			}
			else
			{
				$Enrolled = "";
			}
				
			
				
			$content .= "<td class='tabletext' align='center'><span title='".$eventName."'>".$Enrolled."</span></td>";
			$export_content .= $tab.trim($Enrolled);
			$tab = "\t";
		}
		
		$content .= "</tr>";
		$export_content .= "\n";
	}
	$content .= "</table>";
//for($j=0; $j<sizeof($EventGroups); $j++)
//{
//	$eid = $EventGroups[$j][0];
//
//	for($i=0; $i<sizeof($Students); $i++)
//	{
//		$sid= $Students[$i][0];	
//
//		#return the age group id that the student belonged to
//		$ageGroupID = $lsports->retrieveAgeGroupByStudentID($sid);
//		
//		# return the event group id of the event
//		$egid = $lsports->retrieveEventGroupID($eid, $ageGroupID, $typeIDs);
//
//		$status = $lsports->returnFirstRoundStudentAttendantStatus($sid, $egid[0]);
//
//		for($k=0; $k<sizeof($status); $k++)
//		{
//			if($status[$k]==1)
//				$totalAbsent++;
//			else if($status[$k]==2 || $status[$k]==3 ||$status[$k]==4 ||$status[$k]==5)
//				$totalPresent++;
//		}
//	}
//}
//
//### Table content
//$content = "";
//$export_content .= $Lang["eSports"]["Class"] ." / ". $Lang["eSports"]["Form"] ."\t";
//$export_content .= $export_classname."\n";
//$export_content .= $i_Sports_Present."\t";
//$export_content .= $totalPresent.$i_Sports_Count."\n";
//$export_content .= $i_Sports_Absent."\t";
//$export_content .= $totalAbsent.$i_Sports_Count."\n\n";
//
//$content .= "<table width='100%' border='0' cellspacing='0' cellpadding='4'>";
//$content .= "<tr>";
//$content .= "<td class='tablebluetop tabletopnolink'>$i_ClassName</td>";
//$content .= "<td class='tablebluetop tabletopnolink'>$i_ClassNumber</td>";
//$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_field_Group</td>";
//$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_field_Gender</td>";
//$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_field_House</td>";
//$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_Participant</td>";
//$content .= "<td class='tablebluetop tabletopnolink'>$i_Sports_menu_Settings_Participant</td>";
//
//
//		$export_content .= $i_ClassName."\t";
//		$export_content .= $i_ClassNumber."\t";
//		$export_content .= $i_Sports_field_Group."\t";
//		$export_content .= $i_Sports_field_House."\t";
//		$export_content .= $i_Sports_field_Gender."\t";		
//		$export_content .= $i_Sports_Participant."\t";
//		$export_content .= $i_Sports_menu_Settings_Participant."\t";
//
//		$eName = array();
//		for($i=0; $i<sizeof($EventGroups); $i++)
//		{
//			$eventName = $EventGroups[$i][1];
//			$content .= "<td class='tablebluetop tabletopnolink' align='center'>".$eventName."</td>";
//			$eName[$i] = $eventName;
//			if(($i+1)<sizeof($EventGroups))
//				$export_content .= $eventName."\t";
//			else
//				$export_content .= $eventName."\n";
//		}
//		$content .= "</tr>";
//
//		for($j=0; $j<sizeof($Students); $j++)
//		{				
//			list($student_id, $student_name, $student_classno, $student_classname) = $Students[$j];		
//			$student_group_id = $lsports->retrieveAgeGroupByStudentID($student_id);
//			$student_group = $lsports->retrieveAgeGroupName($student_group_id);
//			$student_gender = $lsports->retrieveAgeGroupDetail($student_group_id);
//			$enrolRecord = $lsports->retrievePersonalEnrolmentRecord($student_id);
//			$athleticNum = $lsports->returnStudentAthleticNum($student_id);
//			$content .= "<tr class='tablebluerow".($j%2?"2":"1")."'>";
//			$content .= "<td class='tabletext'>".$student_classname."</td>";
//			$content .= "<td class='tabletext'>". $student_classno ."</td>";
//			$content .= "<td class='tabletext'>". $student_group ."</td>";
//			$content .= "<td class='tabletext'>". $student_gender["Gender"] ."</td>";
//			$content .= "<td class='tabletext'>".$student_name."</td>";
//			$content .= "<td class='tabletext'>".$athleticNum."</td>";
//			
//			$export_content .= $student_classname."\t";
//			$export_content .= $student_classno."\t";
//			$export_content .= $student_group."\t";
//			$export_content .= $student_gender["Gender"]."\t";
//			$export_content .= "\"".$student_name."\"\t";
//			$export_content .= $athleticNum."\t";
//
//			for($k=0; $k<sizeof($EventGroups); $k++)
//			{
//				$eid = $EventGroups[$k][0];
//					
//				if($enrolRecord[$eid] == 1)
//				{
//					$content .= "<td class='tabletext' align='center'><span title='".$eName[$k]."'>X</span></td>";
//
//					if(($k+1)<sizeof($EventGroups))
//					{
//						$export_content .= "X";
//						$export_content .= "\t";
//					}
//					else
//					{
//						$export_content .= "X";
//						$export_content .= "\n";
//					}
//				}
//				else
//				{
//					$content .= "<td>&nbsp;</td>";
//					if(($k+1)<sizeof($EventGroups))
//						$export_content .= "\t";
//					else
//						$export_content .= "\n";
//				}
//			}
//			$content .= "</tr>";
//		}
//		$content .= "</table>";
//### Table Content - End 

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Report_ClassEnrolment,"class_enrol.php", 1);
$TAGS_OBJ[] = array($i_Sports_menu_Report_HouseEnrolment,"house_enrol.php", 0);
$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();

$export_content = str_replace('"','&quot;',$export_content);
?>

<br />
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
                        	<form name="form2" action="class_enrol.php" method="post">
                                <table class="form_table_v30">
                                <tr>
                                <td class="field_title"><span class="tabletext"><?=$Lang["eSports"]["Class"]?> / <?=$Lang["eSports"]["Form"]?></span></td>
                                <td><?=$class_select?></td>
                                </tr>
                                <tr>
                                <td class="field_title"><span class="tabletext"><?=$i_Sports_Present?> </span></td>
                                <td><?=$totalPresent." ".$i_Sports_Count?></td>
                                </tr>
                                <tr>
                                <td class="field_title"><span class="tabletext"><?=$i_Sports_Absent?> </span></td>
                                <td><?=$totalAbsent." ".$i_Sports_Count?></td>
                                </tr>
                                </table>
                                </form>
			</td>
		</tr>
                <tr>
                	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td><br /><?=$content?></td>
</tr>        

<form name="form1" action='general_export.php' method="post">
<tr>
	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
</tr>     
<tr>
	<td align='center'>
		<?=$linterface->GET_ACTION_BTN($button_export, "submit", "","submit2")?>
		<?if(file_exists("class_enrol_export2.php")) echo $linterface->GET_ACTION_BTN($Lang['Btn']['Export2'], "button","document.form2.action='class_enrol_export2.php';document.form2.submit();")?>
		</td>
</tr>
 
<input type="hidden" name="exportFileName" value="class-enrol">
<input type="hidden" name="exportContent" value="<?=$export_content?>">
</form>

</table>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>