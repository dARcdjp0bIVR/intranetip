<?php
# using: 

/* *******************************************
 * Modification log
 *
 *  2018-10-29  Ivan    [2018-1026-1022-53235]
 *  - Show House Name
 *
 *	2011-10-27	YatWoon
 *	- Fixed: Display incorrect "qualify icon" [case#2011-1027-1107-40071]
 *
 *	2011-06-14	YatWoon
 *	- Improved: Display event for user selection [Case#2011-0106-1041-15073]
 *
 * *******************************************/

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."templates/". $LAYOUT_SKIN ."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$linterface = new interface_html();

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
$targetEventID = array();
if(!empty($targetEventIDstr))	$targetEventID = explode(",",$targetEventIDstr);

# Add Boys Open and Girls Open
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TFEventGroups = $lsports->retrieveTrackFieldEventName();
$HREventGroups = $lsports->retrieveDistinctHouseRelayName();
$CREventGroups = $lsports->retrieveDistinctClassRelayName();

$EventGroupInfo = $lsports->Get_EventGroup_Info();
$EventCodeArr = BuildMultiKeyAssoc($EventGroupInfo,"EventGroupID","EventCode",1);

$tfNum = sizeof($TFEventGroups);
$maxRank = 8;

### Content
$content = "";

for($i=0; $i<sizeof($AgeGroups); $i++)
{
    list($gid, $gname) = $AgeGroups[$i];
    
    if($gid>0) {
        $EventGroupsArr = array_merge($TFEventGroups, $HREventGroups);
    }
    else {
        $EventGroupsArr = array_merge($TFEventGroups, $HREventGroups, $CREventGroups);
    }
    
    if($ageGroup==$gid || $ageGroup=="")
    {
        $content .= $linterface->GET_NAVIGATION2($gname);
        
        $content .= "<table class='common_table_list_v30'>";
        $content .= "<tr>";
        $content .= "<th>$i_Sports_Report_Item_Name</th>";
        
        for($j=1; $j<=$maxRank; $j++)
        {
            if($intranet_session_language=="en")
            {
                if($j==1) {
                    $temp = $j."st";
                }
                else if($j==2) {
                    $temp = $j."nd";
                }
                else if($j==3) {
                    $temp = $j."rd";
                }
                else {
                    $temp = $j."th";
                }
            }
            else
            {
                $temp = $i_Sports_The.$j.$i_Sports_Rank;
            }
            $content .= "<td align='center' width='10%'><b>".$temp."</b></th>";
        }
        $content .= "</tr>";
        
        ### Table Content
        $tableDisplay = "";
        $css = 0;
        for($j=0; $j<sizeof($EventGroupsArr); $j++)
        {
            list($eventID, $ename) = $EventGroupsArr[$j];
            
            if((!empty($targetEventID) && in_array($eventID, $targetEventID)) || empty($targetEventID))
            {
                $temp_data = $lsports->retrieveTrackFieldDetail($eventID);
                list($tempEventType, $tempEnglishName, $tempChineseName, $tempDisplayOrder, $tempIsJump) = $temp_data;
                
                $tempEventGroupID = $lsports->retrieveEventGroupID($eventID, $gid, $tempEventType);
                $EventGroupID = $tempEventGroupID['EventGroupID'];
                
                if($j<$tfNum)
                {
                    # only display the event is student enroled
                    $enroledStudentNo = $lsports->retrieveEventEnroledCount($EventGroupID);
                    if(!$enroledStudentNo)	continue;
                    
                    $Ranking = $lsports->retrieveTFRanking($eventID, $gid);
                    $isRelay = 0;
                }
                else
                {
                    if($tempEventType==3)
                    {
                        # only display the event is student enroled
                        $enroledNo = $lsports->retrieveHREnroledCount($EventGroupID);
                        if($enroledNo==0 or $enroledNo=="")	continue;
                        
                        $Ranking = $lsports->retrieveHRRanking($eventID, $gid);
                    }
                    else
                    {
                        # only display the event is student enroled
                        $enroledNo = $lsports->retrieveCREnroledCount($EventGroupID);
                        if(!$enroledNo)	continue;
                        
                        $Ranking = $lsports->retrieveCRRanking($eventID, $gid);
                    }
                    $isRelay = 1;
                }
                
                $tableDisplay .= "<tr>";
                $tableDisplay .= "<td>". $ename."</td>";
                
                $tctr=0; //count all same-ranked individual in each event and print empty td at the end
                $col =0;
                for($k=0; $k<$maxRank; $k++)
                {
                    if(sizeof($Ranking[$k])!=0)
                    {
                        if(!$isRelay)
                        {
                            $ctr=0; //count same-ranked individual
                            $all_stu_info="";
                            do{
                                list($sname, $sid, $rank, $status, $stu_name, $stu_class, $min, $sec, $ms, $metre) = $Ranking[$k+$ctr];
                                if($metre) {
                                    $event_result = $metre ."m";
                                }
                                else {
                                    $event_result = $min ."'". $sec ."''". $ms;
                                }
                                
                                $house = $lsports->retrieveStudentHouseInfo($sid);
                                $hcode = $house[0][3];
                                $hcolor = $house[0][2];
                                // [K151652]
                                $hname = $house[0][0];
                                
                                $h_sq="";
                                if($hcolor) {
                                    $h_sq = '<span style="height:15; background-color:#'. $hcolor .'; border:1px solid #CCCCCC">&nbsp;&nbsp;&nbsp;</span>';
                                }
                                
                                // [K151652]
                                //$stu_info = $stu_name."<br>". $h_sq. $stu_class."<br>";
                                $stu_info = $stu_name."<br>". $h_sq. $hname.' '.$stu_class."<br>";
                                
                                $status_tag = "";
                                if($status==3 || $status==4)
                                {
                                    $status_img = ($status==3?"icon_qualified.gif":($status==4?"icon_recordbroken.gif":""));
                                    $status_tag = "<img src='{$image_path}/{$LAYOUT_SKIN}/esport/$status_img' width='20' height='20' align='absmiddle' />";
                                }
                                $stu_info .= $status_tag." ".$event_result;
                                
                                $all_stu_info .= $stu_info;
                                
                                $ctr++;
                                
                                $sname = "(".$hcode.")".$sname;
                                if($status==3) {
                                    $sname = "#".$sname;
                                }
                                else if($status==4) {
                                    $sname = "*".$sname;
                                }
                            }while($Ranking[$k+$ctr]["Rank"]==$rank);
                            $ctr-=1;
                            $tableDisplay .= "<td class='tabletext' nowrap align='center'>".$all_stu_info."</td>";
                            $col++; // count no of columns printed
                            $k+=$ctr;
                            
                            if($k<7)	  //if more than 1 student rank in 8 , don't need to print empty td at the end
                            {
                                if($lsports->RankPattern=="1223")
                                {
                                    $tctr+=$ctr;
                                }
                                else
                                {
                                    for($ctr;$ctr>0;$ctr--)
                                    {
                                        $tableDisplay .= "<td class='tabletext'>&nbsp;</td>";
                                        $col++; // count no of columns printed
                                    }
                                }
                            }
                        }
                        else	# 20081014 process relay ranking (yatwooon)
                        {
                            $ctr=0;
                            $all_house_info="";
                            do{
                                list($housename, $hid, $rank, $status, $min, $sec, $ms) = $Ranking[$k+$ctr];
                                $event_result = $min ."'". $sec ."''". $ms;
                                
                                if($tempEventType==3)
                                {
                                    $house = $lsports->getHouseDetail($hid);
                                    $hcode = $house[5];
                                    $hcolor = $house[4];
                                    $h_sq="";
                                    if($hcolor) {
                                        $h_sq = '<span style="height:15; background-color:#'. $hcolor .'; border:1px solid #CCCCCC">&nbsp;&nbsp;&nbsp;</span>';
                                        
                                    }
// 							      	$display_name = $lsports->house_flag2($hcolor, $housename);
                                    $display_name = $h_sq . " " . $housename;
                                }
                                else
                                {
                                    $display_name = $housename;
                                }
                                
                                ### house/class info
                                $house_info = $display_name."<br>";
                                $status_tag = "";
                                if($status==3 || $status==4)
                                {
                                    $status_img = ($status==3?"icon_qualified.gif":($status==4?"icon_recordbroken.gif":""));
                                    $status_tag = "<img src='{$image_path}/{$LAYOUT_SKIN}/esport/$status_img' width='20' height='20' align='absmiddle' />";
                                }
                                $house_info .= $status_tag." ".$event_result;
                                $all_house_info .=$house_info;
                                $ctr++;
                                
                                $sname = "(".$hcode.")".$sname;
                                if($status==3) {
                                    $housename = "#".$housename;
                                }
                                else if($status==4) {
                                    $housename = "*".$housename;
                                }
                            }while($Ranking[$k+$ctr]["Rank"]==$rank);
                            $ctr-=1;
                            $tableDisplay .= "<td class='tabletext' align='center'>".$all_house_info."</td>";
                            $col++; // count no of columns printed
                            $k+=$ctr;
                            
                            if($k<7)	 //if more than 1 student rank in 8 , don't need to print empty td at the end
                            {
                                if($lsports->RankPattern=="1223")
                                {
                                    $tctr+=$ctr;
                                }
                                else
                                {
                                    for($ctr;$ctr>0;$ctr--)
                                    {
                                        $tableDisplay .= "<td width='10%'>&nbsp;</td>";
                                        $col++; // count no of columns printed
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        $tableDisplay .= "<td width='10%'>&nbsp;</td>";
                        $col++; // count no of columns printed
                    }
                }
                
                //print empty td at the end
                for($col=$maxRank-$col;$col>0;$col--) {
                    $tableDisplay .= "<td class='tabletext'>&nbsp;</td>";
                }
                $tableDisplay .= "</tr>";
                
                $css++;
            }
        }
        
        $content .= $tableDisplay;
        $content .= "</table>";
        
        $content .= "<table cellspacing='0' cellpadding='3' border='0'>";
        $content .= "<tr>";
        $content .= "<td nowrap='nowrap' class='tabletextremark'><img src='{$image_path}/{$LAYOUT_SKIN}/esport/icon_recordbroken.gif' width='20' height='20' align='absmiddle' /> $i_Sports_RecordBroken</td>";
        $content .= "<td class='tabletextremark'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='10' /></td>";
        $content .= "<td nowrap='nowrap' class='tabletextremark'><img src='{$image_path}/{$LAYOUT_SKIN}/esport/icon_qualified.gif' width='20' height='20' align='absmiddle' /> $i_Sports_Qualified</td>";
        $content .= "</tr>";
        $content .= "</table><br />";
        
        $content .= "</table>";
        $content .= "<br>";
    }
}
?>

<table width="100%" align="center" class="print_hide" border="0">
<tr>
	<td align="right"><?= $linterface->GET_SMALL_BTN($button_print, "button", "javascript:window.print();","submit2")?></td>
</tr>
</table>

<?=$content?>

<?php
intranet_closedb();
?>