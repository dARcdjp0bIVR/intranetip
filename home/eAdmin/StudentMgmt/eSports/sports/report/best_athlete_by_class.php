<?php
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");

intranet_auth();
intranet_opendb();

$linterface 	= new interface_html();
$CurrentPage	= "PageRaceTopRecord";

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

# retrieve a initial age group ID
$sql = "SELECT AgeGroupID FROM SPORTS_AGE_GROUP ORDER BY AgeGroupID LIMIT 0, 1";
$default_group = $lsports->returnVector($sql);

if($ageGroup=="")
	$ageGroup = $default_group[0];

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names

# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

# Create Age Group Selection List
//$select_age_group = getSelectByArray($AgeGroups,"name=ageGroup onChange='document.form2.submit()'","$ageGroup",0,1);

//$TrackEventGroups = $lsports->retrieveTrackEventIDAndName();
//$FieldEventGroups = $lsports->retrieveFieldEventIDAndName();
//$HREventGroups = $lsports->retrieveDistinctHouseRelayName();
//$CREventGroups = $lsports->retrieveDistinctClassRelayName();

$studentCount = $lsports->retrieveClassStudentCount();
#Export Content
//$export_content .= $i_Sports_menu_Report_House."\n\n";

### Table 1
$t = 1;
$t1 = "";
$t1 .= "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td>";
$t1 .= "<table width='100%' border='0' cellpadding='4' cellspacing='0'>";
$t1 .= "<tr>";
$t1 .= "	<td class='tablebluetop tabletopnolink'>&nbsp;</td>";
$t1 .= "	<td class='tablebluetop tabletopnolink'>tem Male </td>";
$t1 .= "	<td class='tablebluetop tabletopnolink'>tem Female</td>";
$t1 .= "</tr>";

$export_content .= ",";
$export_content .= "tem male".",";
$export_content .= "tem female"."\n";

## push the data into multi-array
$data = array();
$namefield = getNameFieldByLang("a.");
$sql = "	 SELECT 
					f.LevelName,
					c.ClassName,
					a.ClassNumber,
					a.UserID,
					a.Gender,
					$namefield as Name,
					e.AthleticNum,	
					ROUND(SUM(d.score),1) as sum
				FROM 
					INTRANET_USER a ,
					INTRANET_USERGROUP b,
					INTRANET_CLASS c ,
					SPORTS_LANE_ARRANGEMENT d,
					SPORTS_STUDENT_ENROL_INFO e,
					INTRANET_CLASSLEVEL f
				WHERE 
					a.UserID=b.UserID
					AND b.GroupID=c.GroupID
					AND a.UserID=d.StudentID
					AND c.RecordStatus = 1
					AND d.score IS NOT NULL
					AND d.StudentID = e.StudentID
					AND c.ClassLevelID = f.ClassLevelID
				GROUP BY  a.UserID, c.ClassName
				order by f.LevelName, c.ClassName, sum DESC 
		";
$data = $lsports->returnArray($sql,7);

$SortClassAry = Array();
$SortLevelAry = Array();
$StudentInfo = Array();
$i = 0;
$j = 0;
foreach($data as  $row)
{
	list($LevelName,$ClassName,$ClassNumber,$UserID,$Gender,$Name,$AthleticNum,$totalscore) = $row;
	
	if(!isset($SortClassAry[$ClassName]))
		$SortLevelAry["Class"][] = $ClassName;
	
	if(!isset($SortClassAry[$ClassName][$Gender]))
		$i=0;
	else
		$i = sizeof($SortLevelAry[$LevelName][$Gender]);

	$SortClassAry[$ClassName][$Gender][$i]["UserID"] = $UserID;
	$SortClassAry[$ClassName][$Gender][$i]["totalscore"] = $totalscore;
	
	if(!isset($SortLevelAry[$LevelName][$Gender]))
		$j = 0;
	else
		$j = sizeof($SortLevelAry[$LevelName][$Gender]);

	$SortLevelAry[$LevelName][$Gender][$j]["UserID"] = $UserID;
	$SortLevelAry[$LevelName][$Gender][$j]["totalscore"] = $totalscore;
	
	$StudentInfo[$UserID]["Name"] = $Name;
	$StudentInfo[$UserID]["AthleticNum"] = $AthleticNum;

	$i++;
	$j++;
}

function scorecmp($a, $b)
{
	if ($a["totalscore"] == $b["totalscore"]) {
		return 0;
	}
	return ($a["totalscore"] > $b["totalscore"]) ? -1 : 1;
}

hdebug_r($SortLevelAry);
//usort($SortLevelAry["S1"]["F"],"scorecmp");
//hdebug_r($SortLevelAry["S1"]["F"]);
	
$display1 = "";
for($i=0; $i<sizeof($data1); $i++)
{
                $display1 .= "<tr class='tablebluerow".($i%2?"2":"1")."'>";
		$display1 .= "<td class='tabletext' width='20%'>".$lsports->house_flag2($data1[$i]['house_color'], $data1[$i]['house_name'])."</td>";
		$display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['s_count']."</td>";
		$display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['e_count']."</td>";
                $display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['score']."</td>";
		$display1 .= "<td class='tabletext' width='20%'>".$data1[$i]['averageCount']."</td>";
		$display1 .= "</tr>";
		
		$export_content .=$data1[$i]['house_name'].",";
		$export_content .= $data1[$i]['s_count'].",";
		$export_content .= $data1[$i]['e_count'].",";
		$export_content .= $data1[$i]['score'].",";
		$export_content .= $data1[$i]['averageCount']."\n";
}

$t1 .= $display1;
$t1 .= "</table></td></tr>";
$t1 .= "<tr><td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td></tr>";
$t1 .= "</table><br />";
$export_content .= "\n";

## Table 1 End

### Title ###
$TAGS_OBJ[] = array($i_Sports_menu_Report_RaceTopRecord , "race.php", 1);    
$TAGS_OBJ[] = array($i_Sports_menu_Report_Best_Athlete_By_Group, "best_athlete_by_group.php", 0);    
$TAGS_OBJ[] = array($i_Sports_menu_Report_Best_Athlete_By_Class, "best_athlete_by_class.php", 0);    

$MODULE_OBJ = $lsports->GET_MODULE_OBJ_ARR();

$linterface->LAYOUT_START();
?>

<br />
<table width="96%" border="0" cellpadding="5" cellspacing="0">
<tr>
	<td><br />
		<table width="100%" border="0" cellpadding="0" cellspacing="0">
<!--		<tr>
			<td>
                        	<form name="form2" action="race.php" method="post">
                                <table align="center" width="100%" border="0" cellpadding="5" cellspacing="0">
                                <tr>
                                <td width="30%" valign="top" nowrap="nowrap" class="formfieldtitle"><span class="tabletext"><?=$i_Sports_field_Group?> </span></td>
                                <td valign="top" class="tabletext"><?=$select_age_group?></td>
                                </tr>
                                </table>
                                </form>
			</td>
		</tr>-->
                <tr>
                	<td height="1" class="dotline"><img src="<?=$image_path?>/<?=$LAYOUT_SKIN?>/10x10.gif" width="10" height="1"></td>
                </tr>
                </table>
	</td>
</tr>
<tr>
	<td><br /><?=$content?></td>
</tr>        

<form name="form1" action='general_export.php' method="post">
<tr>
	<td height='1' class='dotline'><img src='{$image_path}/{$LAYOUT_SKIN}/10x10.gif' width='10' height='1'></td>
</tr>     
<tr>
	<td align='center'><?=$linterface->GET_ACTION_BTN($button_export, "submit", "","submit2")?></td>
</tr>

<input type="hidden" name="exportFileName" value="race">
<input type="hidden" name="exportContent" value="<?=$export_content?>">
</form>

</table>
<br />

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>
