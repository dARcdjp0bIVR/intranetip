<?php
# using: Philips

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

$linterface = new interface_html();
intranet_auth();
intranet_opendb();

$lsports = new libsports();
$lsports->authSportsSystem();
$ageGroup = $_POST['ageGroup'];
$groupType = $_POST['groupType'];
$className = $_POST['className'];
$house = $_POST['house'];
$raceDay = $_POST['raceDay'];

$displayItems = $_POST['displayItem'] ? $_POST['displayItem'] : array();
if($sys_custom['eSports']['SportDay_AthleteNumberPrintout_MKSS']){
	$Students = $lsports->getAthleticNumDeatil($groupType, $className, $house, $ageGroup, $raceDay, $displayItems, $fixedPos = 1, $event_font_family, $event_font_size);
} else {
	$Students = $lsports->getAthleticNumDeatil($groupType, $className, $house, $ageGroup, $raceDay, $displayItems, $fixedPos = 0, $event_font_family, $event_font_size);
	//$Students = $lsports->getAthleticNumDeatil($groupType, $className, $house, $ageGroup, $raceDay);
}

?>

<?php
$content = "";
if(sizeof($Students) != 0)
{
	$table_content_ary = array();
	
	foreach($Students as $key => $value)
	{
	    
	    if($sys_custom['eSports']['KaoYipReports']){
	        list($sname, $gname, $ahtleticNum, $detail) = $value;
	        
	        $table_content = "";
	        $table_content .= "<table width='80%' border='0' cellpadding='0' cellspacing='0' align='center'>";
	        $table_content .= "<tr>";
	        $table_content .= "<td align='right' width='40%' style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$gname."</td>";
	        $table_content .= "<td align='center' width='25%' style='font-family:". $student_font_family."; font-size:". $student_font_size."'></td>";
	        $table_content .= "<td style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$sname."</td>";
	        $table_content .= "</tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3' align='center' style=\"font-family:'". $athlete_font_family."'; font-size:". $athlete_font_size."; font-weight: bolder;\">".$ahtleticNum."</td></tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3' style='font-family:". $event_font_family."; font-size:". $event_font_size."'>&nbsp;".$detail."</td></tr>";
	        $table_content .= "</table>";
	        $table_content_ary[] = $table_content;
	    } else {
	        list($sname, $gname, $hname, $ahtleticNum, $detail) = $value;
	        
	        $table_content = "";
	        $table_content .= "<table width='80%' border='0' cellpadding='0' cellspacing='0' align='center'>";
	        $table_content .= "<tr>";

            if($sys_custom['eSports']['SportDay_AthleteNumberPrintout_MKSS']){
                $table_content .= "<td align='right' width='35%' style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$gname."</td>";
                $table_content .= "<td align='center' width='30%' style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$hname."</td>";
                $table_content .= "<td align='left' width='35%' style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$sname."</td>";
            }
            else{
				$table_content .= "<td align='right' width='40%' style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$gname."</td>";
				$table_content .= "<td align='center' width='25%' style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$hname."</td>";
				$table_content .= "<td style='font-family:". $student_font_family."; font-size:". $student_font_size."'>".$sname."</td>";
            }
	        $table_content .= "</tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3' align='center' style=\"font-family:'". $athlete_font_family."'; font-size:". $athlete_font_size."; font-weight: bolder;\">".$ahtleticNum."</td></tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3'>&nbsp;</td></tr>";
	        $table_content .= "<tr><td colspan='3' style='font-family:". $event_font_family."; font-size:". $event_font_size."'>&nbsp;".$detail."</td></tr>";
	        $table_content .= "</table>";
	        $table_content_ary[] = $table_content;
	    }
	}
	
	for($i=0;$i<sizeof($table_content_ary);$i++)
	{
		if($i%2==0)
		{
			$breakStyle = ($i<sizeof($table_content_ary)-2)?"style='page-break-after:always'":"";
			$content .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' $breakStyle>";
		}

        if($sys_custom['eSports']['SportDay_AthleteNumberPrintout_MKSS']){
            $content .= "
				<tr>
					<td width=5% height='500'>&nbsp;</td>
					<td width=90% align='center' valign='middle' height='500'>". $table_content_ary[$i] ."</td>
					<td width=5% align='right' valign='top' height='500' class='print_hide'>". ($i==0 ? $linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2") : "&nbsp;") ."</td>
				</tr>
			";
        }
        else {
			$content .= "
					<tr>
						<td width=5% height='450'>&nbsp;</td>
						<td width=90% align='center' valign='middle' height='450'>". $table_content_ary[$i] ."</td>
						<td width=5% align='right' valign='top' height='450' class='print_hide'>". ($i==0 ? $linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2") : "&nbsp;") ."</td>
					</tr>
			";
        }
		if($i%2==1)
			$content .= "</table>";
	}
}
else
{
	$content .= "<table width='70%' border='0' cellpadding='0' cellspacing='0' align='center' $breakStyle>";
	$content .= "<tr><td align='center' class='tabletext'>".$i_no_record_exists_msg."</td></tr>";
	$content .= "<tr><td>&nbsp;</td></tr>";
        $content .= "<tr><td align='center'>". $linterface->GET_BTN($button_close, "button", "javascript:self.close();","clost_btn") ."</td></tr>";
	$content .= "</table>";
}
echo $content;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>