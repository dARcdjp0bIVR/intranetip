<?php
# using: 

###########################################################
#
#	Date:	2019-05-08  Bill    [2019-0508-1200-59235]
#           prevent SQL Injection
#           Support "EventGroupType[]" first, then "EventGroupID[]"
#
#	Date:	2018-11-20	Bill     [2018-1023-1036-47066]
#			added Athlete Number display    ($sys_custom['eSports']['ResultPaperAddAthleteNum'])
#
#	Date:	2013-01-16	YatWoon
#			display Classname and Classnumber [Case#2013-0115-1200-21156]
#
#	Date:	2012-09-21	YatWoon
#			display English name and Chinese name [Case#2012-0918-1832-44147]
#
###########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");

intranet_auth();
intranet_opendb();

$lexport = new libexporttext();
$lu = new libuser();
$lsports = new libsports();
$lsports->authSportsSystem();

$openGroupList = $_POST["openGroupList"];
$openGroupArr = explode(",", $openGroupList);

$numberOfLane = $lsports->numberOfLanes;

$EventExport = array();

# Get EventGroupID order by EventCode
$AllEventGroupArr = $lsports->Get_EventGroup_Info('','',"EventCode + 0");
$AllEventGroupIDArr = Get_Array_By_Key($AllEventGroupArr,"EventGroupID");

# prepare assoc array for sorting
$EventGroupID = isset($EventGroupType)? $EventGroupType : $EventGroupID;
$EventGroupID = (array)$EventGroupID;
for($x=0; $x<sizeof($EventGroupID); $x++)
{
	$theID = $EventGroupID[$x];
	$temp = explode("_", $theID);
	$tmpEventGroupID[$temp[0]][] = $theID;
}

# Sort the EventGroupID
$EventGroupID = array();
for($x=0; $x<sizeof($AllEventGroupIDArr); $x++)
{
	if($tmpEventGroupID[$AllEventGroupIDArr[$x]])
	{
		foreach($tmpEventGroupID[$AllEventGroupIDArr[$x]] as $thisID) {
			$EventGroupID[] = $thisID;
		}
	}
}

for($x=0; $x<sizeof($EventGroupID); $x++)
{
	$tf_flag = 0;
	$hr_flag = 0;
	$cr_flag = 0;
	$theID = $EventGroupID[$x];
	
	$temp = explode("_", $theID);
	$eventGroupID = $temp[0];
	$eventGroupID = IntegerSafe($eventGroupID);
	$roundType = $temp[1];
	$roundType = IntegerSafe($roundType);
    
    # Check if it is open event
	$isOpen = 0;
	for($j=0; $j<sizeof($openGroupArr); $j++)
	{
		if($openGroupArr[$j] == $eventGroupID)
		{
			$isOpen = 1;
			break;
		}
	}
	
	# Retreive the Event ID and Group ID of this Event
	$eventGroupInfo = $lsports->retrieveEventGroupDetail($eventGroupID);
	$eventID = $eventGroupInfo[1];
	$groupID = $eventGroupInfo[2];
	$eventCode = $eventGroupInfo[9];
    
	# retrieve the event info of the event
	$eventInfo = $lsports->returnEventInfo($eventID);
	$eventType = $eventInfo[0];
	$eventName = $eventInfo[1];
	$isJump = $eventInfo[2];
    
	# retrieve the group name of the event
	if($groupID > 0) {
		$groupName = $lsports->returnAgeGroupName($groupID);
	} else if($groupID=='-1') {
		$groupName = $i_Sports_Event_Boys_Open;
	} else if($groupID=='-2') {
		$groupName = $i_Sports_Event_Girls_Open;
	} else if($groupID=='-4') {
		$groupName = $i_Sports_Event_Mixed_Open;
	}

	# retrieve ext info of the event
	$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
	
	# Check if the number of lanes of this round equal to the lane number
	$isLaneNum = 0;
	if($eventType==1)
	{
		if(($ExtInfo["FirstRoundGroupCount"]==0 && $roundType==1) || ($ExtInfo["SecondRoundLanes"]==0 && $roundType==2) || ($ExtInfo["FinalRoundNum"]==0 && $roundType==0))
		{
			$isLaneNum = 1;
		}
	}
    
	$roundName = "";
	if($roundType==1 && $ExtInfo["FinalRoundReq"]==1) {
		$roundName = "(".$i_Sports_First_Round.")";
	} else if($roundType==2) {
		$roundName = "(".$i_Sports_Second_Round.")";
	} else if($roundType==0) {
		$roundName = "(".$i_Sports_Final_Round.")";
	}

	if($eventType==2)
	{
		$record = $ExtInfo["RecordMetre"] . $i_Sports_Metres;
	}
	else
	{
		$record = $ExtInfo["RecordMin"];
		$record .= "'".$ExtInfo["RecordSec"];
		$record .= "''".$ExtInfo["RecordMs"];
	}
    
	# Retrive total number of heat in this round
	$sql = "SELECT MAX(Heat) FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RoundType = '$roundType'";
	$maxHeat = $lsports->returnVector($sql);

	if($eventType==1 || $eventType==2)		# Track and Field Events Table
	{
		$Arrange = $lsports->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 1, $roundType);

		if(sizeof($Arrange)!=0)
		{
			$orderField = ($eventType==1) ? $i_Sports_Line : $i_Sports_Order;
			$Field_Round_Num = ($isJump==1) ? JUMP_NUMBER : FIELD_NUMBER;
			
			$currHeat = "";
			for($k=0; $k<sizeof($Arrange); $k++)
			{
				//list($heat, $order, $sname, $sid) = $Arrange[$k];
				list($heat, $order, $sname, $sid,$rank, $score, $trackresult, $fieldresult, $trial1, $trial2, $trial3, $s_ename, $s_cname, $s_classname, $s_classnumber) = $Arrange[$k];
				$nextHeat = $Arrange[$k+1][0];

				$athleticNum = $lsports->returnStudentAthleticNum($sid);
				$className =  $lsports->returnStudentClassName($sid);
				$house = $lsports->retrieveStudentHouseInfo($sid);
				$thisGender = $lu->getUserGender($sid);
				$houseName = $house[0][0];
				
				if($heat != $currHeat || $currHeat == "")
				{
					$EventExport[] = array($Lang['eSports']['csv']['EventCode'], $eventCode); 
					$EventExport[] = array($i_Sports_Item, $eventName . $roundName); 
					$EventExport[] = array($i_Sports_field_Group, $groupName); 
					$EventExport[] = array($i_Sports_Record, $record); 
					$EventExport[] = "";
					$EventExport[] = array($i_Sports_The ." ". $heat . " " .$i_Sports_Group); 
					
					$TempAry = array();
					//$TempAry = array($orderField, $i_Sports_Participant, $i_Sports_House, $i_UserGender);
					if($sys_custom['eSports']['ResultPaperAddAthleteNum']) {
					    $TempAry = array($orderField, $i_Sports_Participant_Number, $Lang['General']['EnglishName'], $Lang['General']['ChineseName'], $i_ClassName, $i_ClassNumber, $i_Sports_House, $i_UserGender);
					} else {
					    $TempAry = array($orderField, $Lang['General']['EnglishName'], $Lang['General']['ChineseName'], $i_ClassName, $i_ClassNumber, $i_Sports_House, $i_UserGender);
					}
					$EventExport[]  = $TempAry;
				}
				
				if($sys_custom['eSports']['ResultPaperAddAthleteNum']) {
				    $EventExport[] = array($order, $athleticNum, $s_ename, $s_cname, $s_classname, $s_classnumber, $houseName, $thisGender);
				} else {
				    $EventExport[] = array($order, $s_ename, $s_cname, $s_classname, $s_classnumber, $houseName, $thisGender);
				}
				$currHeat = $heat;
				
				$EventExport[] = "";
			}
		}
	}
	else if($eventType==3 || $eventType==4)	# House/Class Relay Event Tables
	{
		if($eventType==3) {
			$Arrange = $lsports->returnHRLaneArrangeDetailByEventGroupID($eventGroupID);
		} else {
			$Arrange = $lsports->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);
		}
		$countLane = 1;
		
		if(sizeof($Arrange) != 0)
		{
			$EventExport[] = array($Lang['eSports']['csv']['EventCode'], $eventCode); 
			$EventExport[] = array($i_Sports_Item, $eventName . $roundName); 
			$EventExport[] = array($i_Sports_field_Group, $groupName); 
			$EventExport[] = array($i_Sports_Record, $record); 
			$EventExport[] = "";
			$EventExport[] = "";
					
			$EventExport[] = array($i_Sports_Line, ($eventType==3?$i_Sports_House:$i_general_class)); 

			for($j=0; $j<sizeof($Arrange); $j++)
			{
				if($eventType==3)
				{
					list($hname, $order, $hid, $colorCode) = $Arrange[$j];
				}
				else
				{
					list($hname_en,$hname_b5, $order, $hid, $colorCode) = $Arrange[$j];
					$hname = Get_Lang_Selection($hname_b5,$hname_en);
				}					
				$nextExist = (sizeof($Arrange[$j+1])==0) ? 0 : 1;

				while(($countLane!=$order && $countLane<=$numberOfLane))
				{
					$EventExport[] = array($countLane); 
					$countLane++;
				}
				
				$EventExport[] = array($order, $hname); 
				$countLane++;
			}
			
			$EventExport[] = "";
		}
	}
}

$ExportArr = $EventExport;

$filename = "EventResultPaper2.csv";
$export_content = $lexport->GET_EXPORT_TXT($ExportArr, "", "\t", "\r\n", "\t", 0, "11");
$lexport->EXPORT_FILE($filename, $export_content);

intranet_closedb();
?>