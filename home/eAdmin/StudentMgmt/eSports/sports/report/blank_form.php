<?php
# using: marcus
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

//$openGroupList = $_POST["openGroupList"];
//$openGroupArr = explode(",", $openGroupList);

//$numberOfLane = $lsports->numberOfLanes;

$linterface = new interface_html();

//Print and Close button able
$displayTable = "
	<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'>".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td>
	</tr>
</table>";

$breakStyle = "style='page-break-after:always'";

$displayTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' valign='top'>";


//for($i=0; $i<sizeof($EventGroupID); $i++)
//{
	//$tf_flag =0;
	//$hr_flag = 0;
	//$cr_flag = 0;
	//$theID = $EventGroupID[$i];
	
	//$temp = explode("_", $theID);
	//$eventGroupID = $temp[0];
	//$roundType = $temp[1];

    # Check if it is open event
	//$isOpen = 0;
	//for($j=0; $j<sizeof($openGroupArr); $j++)
	//{
	//	if($openGroupArr[$j] == $eventGroupID)
	//	{
	//		$isOpen = 1;
	//		break;
	//	}
	//}
/*	
	# Retreive the Event ID and Group ID of this Event
	#$eventGroupInfo = $lsports->retrieveEventGroupDetail($eventGroupID);
	$eventID = $eventGroupInfo[1];
	$groupID = $eventGroupInfo[2];

	# retrieve the event info of the event
	$eventInfo = $lsports->returnEventInfo($eventID);
	$eventType = $eventInfo[0];
	$eventName = $eventInfo[1];
	$isJump = $eventInfo[2];

	# retrieve the group name of the event
	if($groupID>0)
	{
		$groupName = $lsports->returnAgeGroupName($groupID);
	}
	else if($groupID=='-1')
		$groupName = $i_Sports_Event_Boys_Open;
	else if($groupID=='-2')
		$groupName = $i_Sports_Event_Girls_Open;
	else if($groupID=='-4')
		$groupName = $i_Sports_Event_Mixed_Open;

	# retrieve ext info of the event
	$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
	
	# Check if the number of lanes of this round equal to the lane number
	$isLaneNum = 0;
	if($eventType==1)
	{
		if(($ExtInfo["FirstRoundGroupCount"]==0 && $roundType==1) || ($ExtInfo["SecondRoundLanes"]==0 && $roundType==2) || ($ExtInfo["FinalRoundNum"]==0 && $roundType==0))
		{
			$isLaneNum = 1;
		}
	}

	$roundName = "";
	if($roundType==1 && $ExtInfo["FinalRoundReq"]==1)
		$roundName = "(".$i_Sports_First_Round.")";
	else if($roundType==2)
		$roundName = "(".$i_Sports_Second_Round.")";
	else if($roundType==0)
		$roundName = "(".$i_Sports_Final_Round.")";

	if($eventType==2)
	{
		$record = $ExtInfo["RecordMetre"];
		$standard = $ExtInfo["StandardMetre"];
	}
	else
	{
		$record = $ExtInfo["RecordMin"];
		$record .= "'".$ExtInfo["RecordSec"];
		$record .= "''".$ExtInfo["RecordMs"];

		$standard = $ExtInfo["StandardMin"];
		$standard .= "'".$ExtInfo["StandardSec"];
		$standard .= "''".$ExtInfo["StandardMs"];
	}

	# Retrive total number of heat in this round
	$sql = "SELECT MAX(Heat) FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RoundType = '$roundType'";
	$maxHeat = $lsports->returnVector($sql);
*/
	//if($eventType==1 || $eventType==2)		# Track and Field Events Table
	//{
		//$Arrange = $lsports->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 1, $roundType);

		//if(sizeof($Arrange)!=0)
		//{
			//add blank rows to Jump 
			//if($isJump==1)
			//{
				for($j=0;$j<20;$j++)
				{
					$emptyfield[0]="&nbsp;"; //assign heat for emptyfield
					$emptyfield[1]=$j+1; //assign arrange order
					for($i=2;$i<4;$i++)
						$emptyfield[$i]="&nbsp;";
					$Arrange[]=$emptyfield;
				}
			//}
//d($Arrange);
			$orderField = $i_Sports_Order;
			$Field_Round_Num =  FIELD_NUMBER;
			$spaceReq = $Field_Round_Num+4;
			$contentTable = "";
			$currHeat = "";
			for($k=0; $k<sizeof($Arrange); $k++)
			{
				list($heat, $order, $sname, $sid) = $Arrange[$k];
				$nextHeat = $Arrange[$k+1][0];

				$athleticNum = $lsports->returnStudentAthleticNum($sid);
				$className =  $lsports->returnStudentClassName($sid);
				$house = $lsports->retrieveStudentHouseInfo($sid);
				$houseName = $house[0][0];
				
				if($heat != $currHeat || $currHeat == "")
				{
					$displayTable .= "<tr><td align='center' $breakStyle>\n";

					$displayTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>";
					$displayTable .= "<tr>";
					$displayTable .= "<td class='eSportprinttitle' colspan=2>".$i_Sports_Item.": <strong>".$eventName." ".$roundName."</strong></td>";
					$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Record.": ".$record."</td>";
					$displayTable .= "</tr>";
					$displayTable .= "<tr>";
					$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_field_Group.": <strong>".$groupName."</strong></td>";
					$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_field_Heat.": <strong>".$heat."/".$maxHeat[0]."</strong></td>";
					$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Standard_Record.": ".$standard."</td>";
					$displayTable .= "</tr>";
					$displayTable .= "</table>";

                    			$displayTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='eSporttableborder'>";
					$displayTable .= "<tr>";
					$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$orderField."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_Participant_Number."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_UserName."</td>";
					$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_Class."</td>";
					$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_House."</td>";
					if($eventType==1)
					{
						$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
						$displayTable .= "<td width=80 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_Time."</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Remark."</td>";
					}
					else
					{
						$displayTable .= "<td width=35 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_Present."</td>";
						for($n=1; $n<=$Field_Round_Num; $n++)
						{
							$displayTable .= ($isJump==1) ? "<td width=45 colspan=3 class='eSporttdborder eSportprinttabletitle'>".$n."</td>" : "<td width=45 class='eSporttdborder eSportprinttabletitle'>".$n."</td>";
						}
						$displayTable .= "<td width=60 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Result."</td>";
						$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
						$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
					}
					$displayTable .= "</tr>";
					$currHeat = $heat;

					if($isLaneNum==1)
					{
						$diff = $order-1;
						for($a=1; $a<=$diff; $a++)
						{
							$displayTable .= "<tr>";
							$displayTable .= "<td class='eSporttdborder eSportprinttext'>".$a."</td>";
							for($m=1; $m<=$spaceReq+4; $m++)
							{
								$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
							}
							$displayTable .= "</tr>";
						}
					}
				}
				$displayTable .= "<tr>";
				$displayTable .= "<td class='eSporttdborder eSportprinttext'>".$order."</td>";
				$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($athleticNum==""?"&nbsp;":$athleticNum)."</td>";
				$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($sname==""?"&nbsp;":$sname)."</td>";
				$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($className==""?"&nbsp;":$className)."</td>";
				$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($houseName==""?"&nbsp;":$houseName)."</td>";
				
				if($isJump==1)
				{
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
					for($m=1; $m<=$Field_Round_Num; $m++)
					{
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
					}
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";

				}
				else
				{
					for($m=1; $m<=$spaceReq; $m++)
					{
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
					}
				}
				$displayTable .= "</tr>";

				if($heat != $nextHeat)
				{
					if($isLaneNum==1)
					{
						for($a=($order+1); $a<=$numberOfLane; $a++)
						{
							$displayTable .= "<tr>";
							$displayTable .= "<td class='eSporttdborder eSportprinttext'>".$a."</td>";
							for($m=1; $m<=$spaceReq+4; $m++)
							{
								$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
							}
							$displayTable .= "</tr>";
						}
					}
					$displayTable .= "</table>";
					$displayTable .= "<table align=right border=0>";
                    			$displayTable .= "<tr height=50><td width='120' colspan='2' align='center' class='eSportunderline'>&nbsp;</td></tr>";
					$displayTable .= "<tr><td align='center' class='eSportprinttext'>(</td><td width='120' class='eSportprinttext'>&nbsp;</td><td class='eSportprinttext'>)</td></tr>";
					$displayTable .= "<tr><td align='center' colspan='2' class='eSportprinttext'>TEACHER I/C</td></tr>";
					$displayTable .= "</table>";

					$displayTable .= "</td></tr>";
				}
			}
		//}
	//}
	/*else if($eventType==3 || $eventType==4)	# House/Class Relay Event Tables
	{
		if($eventType==3)
			$Arrange = $lsports->returnHRLaneArrangeDetailByEventGroupID($eventGroupID);
		else
			$Arrange = $lsports->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);
		$countLane = 1;
		
		if(sizeof($Arrange)!=0)
		{
			$displayTable .= "<tr><td align='center' $breakStyle>\n";

			$displayTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>";
			$displayTable .= "<tr>";
			$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Item.": <strong>".$eventName." ".$roundName."</strong></td>";
			$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Record.": <strong>".$record."</strong></td>";
			$displayTable .= "</tr>";
			$displayTable .= "<tr>";
			$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_field_Group.": <strong>".$groupName."</strong>	</td>";
			$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Standard_Record.": <strong>".$standard."</strong></td>";
			$displayTable .= "</tr>";
			$displayTable .= "</table>";

			$displayTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='eSporttableborder'>";
			$displayTable .= "<tr>";
			$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_Line."</td>";
			$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".($eventType==3?$i_Sports_House:$i_general_class)."</td>";
			$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
			$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_Time."</td>";
			$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
			$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_field_Remark."</td>";
			$displayTable .= "</tr>";
			for($j=0; $j<sizeof($Arrange); $j++)
			{
				list($hname, $order, $hid, $colorCode) = $Arrange[$j];
				$nextExist = (sizeof($Arrange[$j+1])==0) ? 0 : 1;

				while(($countLane!=$order && $countLane<=$numberOfLane))
				{
					$displayTable .= "<tr>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".$countLane."</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					$displayTable .= "</tr>";
					$countLane++;
				}
				
				$displayTable .= "<tr>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".$order."</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".$hname."</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
				$displayTable .= "</tr>";
				$countLane++;

				if($nextExist==0 && $order!=$numberOfLane)
				{
					for($k=($order+1); $k<=$numberOfLane; $k++)
					{
						$displayTable .= "<tr>";
						$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".$k."</td>";
						$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
						$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
						$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
						$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
						$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
						$displayTable .= "</tr>";
					}
				}
			}
			$displayTable .= "</table>";
			$displayTable .= "<table align=right border=0>";
			$displayTable .= "<tr height=50><td width='120' colspan='2' align='center' class='eSportunderline'>&nbsp;</td></tr>";
			$displayTable .= "<tr><td align='center' class='eSportprinttext'>(</td><td width='120' class='eSportprinttext'>&nbsp;</td><td class='eSportprinttext'>)</td></tr>";
			$displayTable .= "<tr><td align='center' colspan='2' class='eSportprinttext'>TEACHER I/C</td></tr>";
			$displayTable .= "</table>";

			$displayTable .= "</td></tr>";
		}
	}*/
//}
$displayTable .= "</table>";

echo $displayTable;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();

?>
