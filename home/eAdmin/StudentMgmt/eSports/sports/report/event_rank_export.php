<?php
# using: 

/* *******************************************
 *  2019-11-20  Philips [2019-1024-1748-37066]
 *  - Replace $Lang['eSports']['IndividualEventRanking'] by $Lang['eSports']['IndividualEventRanking']
 * 
 *	2011-06-14	YatWoon
 *	- Improved: Display event for user selection [Case#2011-0106-1041-15073]
 *
 *	Date: 2011-03-18	YatWoon
 *		add Eventcode
 *
 * *******************************************/
 
$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libexporttext.php");

intranet_auth();
intranet_opendb();

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();
$lexport = new libexporttext();

$AgeGroups = $lsports->retrieveAgeGroupIDNames();	# retrieve all age group ids and names
$targetEventID = array();
if(!empty($targetEventIDstr))	$targetEventID = explode(",",$targetEventIDstr);

# Add Boys Open and Girls Open 
$openBoy[0] = '-1';
$openBoy[1] = $i_Sports_Event_Boys_Open;
$openGirl[0] = '-2';
$openGirl[1] = $i_Sports_Event_Girls_Open;
$openMixed[0] = '-4';
$openMixed[1] = $i_Sports_Event_Mixed_Open;
$AgeGroups[] = $openBoy;
$AgeGroups[] = $openGirl;
$AgeGroups[] = $openMixed;

$TFEventGroups = $lsports->retrieveTrackFieldEventName();
$HREventGroups = $lsports->retrieveDistinctHouseRelayName();
$CREventGroups = $lsports->retrieveDistinctClassRelayName();

$EventGroupInfo = $lsports->Get_EventGroup_Info();
$EventCodeArr = BuildMultiKeyAssoc($EventGroupInfo,"EventGroupID","EventCode",1);
$tfNum = sizeof($TFEventGroups);
$maxRank = 8;

$export_ary = array();

$export_ary[] = array($Lang['eSports']['IndividualEventRanking']);
$export_ary[] = array("");

for($i=0; $i<sizeof($AgeGroups); $i++)
{
	list($gid, $gname) = $AgeGroups[$i];
	
    if($gid>0)
		$EventGroupsArr = array_merge($TFEventGroups, $HREventGroups);
	else
		$EventGroupsArr = array_merge($TFEventGroups, $HREventGroups, $CREventGroups);

	if($ageGroup==$gid || $ageGroup=="")
	{
		$this_row = array();
		
		$export_ary[] = array($gname);
		$this_row[] = $i_Sports_Report_Item_Name;
		$this_row[] = $Lang['eSports']['EventCode'];

        for($j=1; $j<=$maxRank; $j++)
		{
			if($intranet_session_language=="en")
			{
				if($j==1)
					$temp = $j."st";
				else if($j==2)
					$temp = $j."nd";
				else if($j==3)
					$temp = $j."rd";
				else
					$temp = $j."th";
			}
			else
			{
				$temp = $i_Sports_The.$j.$i_Sports_Rank;
			}

			$this_row[] = $temp;
		}
               
		$export_ary[] = $this_row;
               
		for($j=0; $j<sizeof($EventGroupsArr); $j++)
		{			
			list($eventID, $ename) = $EventGroupsArr[$j];
			
			if  ((!empty($targetEventID) && in_array($eventID, $targetEventID)) || empty($targetEventID))
			{
				$temp_data = $lsports->retrieveTrackFieldDetail($eventID);
				list($tempEventType, $tempEnglishName, $tempChineseName, $tempDisplayOrder, $tempIsJump) = $temp_data;
				
				$tempEventGroupID = $lsports->retrieveEventGroupID($eventID, $gid, $tempEventType);
				$EventGroupID = $tempEventGroupID['EventGroupID'];
	
				if($j<$tfNum)
				{
					# only display the event is student enroled
		 			$enroledStudentNo = $lsports->retrieveEventEnroledCount($EventGroupID);
					if(!$enroledStudentNo)	continue;
					
					$Ranking = $lsports->retrieveTFRanking($eventID, $gid);
					$isRelay = 0;
				}
				else
				{
					if($tempEventType==3)
					{
						# only display the event is student enroled
						$enroledNo = $lsports->retrieveHREnroledCount($EventGroupID);
						if($enroledNo==0 or $enroledNo=="")	continue;
						
						$Ranking = $lsports->retrieveHRRanking($eventID, $gid);
					}
					else
					{
						# only display the event is student enroled
						$enroledNo = $lsports->retrieveCREnroledCount($EventGroupID);
						if(!$enroledNo)	continue;
						
						$Ranking = $lsports->retrieveCRRanking($eventID, $gid);
					}
					$isRelay = 1;
				}
				
				$this_row = array();
				$this_row[] = $ename;
				# Event code
				$this_row[] = $EventCodeArr[$EventGroupID];
	
				$tctr=0; //count all same-ranked individual in each event and print empty td at the end
				$col =0;
				for($k=0; $k<$maxRank; $k++)
				{
					if(sizeof($Ranking[$k])!=0)
					{
						if(!$isRelay)
						{
							$ctr=0; //count same-ranked individual 
							$all_stu_info="";
							do{
								list($sname, $sid, $rank, $status, $stu_name, $stu_class, $min, $sec, $ms, $metre) = $Ranking[$k+$ctr];
								if($metre)
									$event_result = $metre ."m";
								else
									$event_result = $min ."'". $sec ."''". $ms;
								
								$house = $lsports->retrieveStudentHouseInfo($sid);
								$hcode = $house[0][3];
			
								$ctr++;
								$h_info = $hcode ? "(".$hcode.")" : "";
								$sname = $h_info.$sname;
								if($status==3)
									$sname = "#".$sname;
								else if($status==4)
									$sname = "*".$sname;
	
								$all_stu_info .= $all_stu_info ? "\n" : "";
								$all_stu_info .= $sname . "\n". $event_result;
							}while($Ranking[$k+$ctr]["Rank"]==$rank);
							
							$this_row[] = $all_stu_info;
							
		                    $ctr-=1;
							$col++; // count no of columns printed
							$k+=$ctr;
							
							if($k<7)	  //if more than 1 student rank in 8 , don't need to print empty td at the end
							{
								if($lsports->RankPattern=="1223")
								{
									$tctr+=$ctr;
								}
								else 
								{
				                   for($ctr;$ctr>0;$ctr--)
				                   {
				                   		$this_row[] = "";
										$col++; // count no of columns printed
				                   }
								}
							}
						}
						else	# 20081014 process relay ranking (yatwooon)
						{
							$ctr=0;
							$all_house_info="";
							
							do{
								list($housename, $hid, $rank, $status, $min, $sec, $ms) = $Ranking[$k+$ctr];
								$event_result = $min ."'". $sec ."''". $ms;
								
								if($tempEventType==3)
								{
									$house = $lsports->getHouseDetail($hid);
									$hcode = $house[5];
								}
								else
								{
									$hcode = "";	
								}
								
								### house/class info
								$ctr++;
								
								$h_info = $hcode ? "(".$hcode.")" : "";
								$housename = $h_info.$housename;
								if($status==3)
									$housename = "#".$housename;
								else if($status==4)
									$housename = "*".$housename;
	
								$all_house_info .= $all_house_info ? "\n" : "";
								$all_house_info .= $housename . "\n". $event_result;
								
							}while($Ranking[$k+$ctr]["Rank"]==$rank);
							
							$this_row[] = $all_house_info;
							
		                    $ctr-=1;
							$col++; // count no of columns printed
							$k+=$ctr;
							
							if($k<7)	 //if more than 1 student rank in 8 , don't need to print empty td at the end
							{
								if($lsports->RankPattern=="1223")
								{
									$tctr+=$ctr;
								}
								else 
								{
				                   for($ctr;$ctr>0;$ctr--)
				                   {
										$this_row[] = "";
										$col++; // count no of columns printed
				                   }
								}
							}
							
						}
					}
				}
				
				$export_ary[] = $this_row;
			}
		}
		
		$export_ary[] =  array("(*) ".$i_Sports_RecordBroken);
		$export_ary[] =  array("(#) ".$i_Sports_Qualified);
		$export_ary[] = array("");
	}
}

$export_content = $lexport->GET_EXPORT_TXT($export_ary, array(""), $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00", $includeLineBreak=1);
$filename = "event_rank.csv";
$lexport->EXPORT_FILE($filename, $export_content);
intranet_closedb();
?>
