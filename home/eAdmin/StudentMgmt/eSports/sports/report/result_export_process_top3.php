<?php
# using: 

###########################################################
#
#   Date:   2019-06-14  Bill    [DM#3631]
#           Add team name if one house has more than one team   ($sys_custom['eSports']['HouseTeamNameDisplay'])
#
#	Date:	2019-05-08  Bill    [2019-0508-1200-59235]
#           prevent SQL Injection
#           Support "EventGroupType[]" first, then "EventGroupID[]"
#
#	Date:	2013-09-25	YatWoon
#			update the page break issue (due to browser issue) [Case#2013-0925-1051-29071]
#
#	Date:	2013-01-16	YatWoon
#			display Classname and Classnumber [Case#2013-0115-1200-21156]
#
#	Date:	2012-09-21	YatWoon
#			display English name and Chinese name [Case#2012-0918-1832-44147]
#
###########################################################

$PATH_WRT_ROOT = "../../../../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libsports.php");
include_once($PATH_WRT_ROOT."includes/libclass.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_header.php");

intranet_auth();
intranet_opendb();

function convertToChineseNumber($number)
{
    global $Lang;
    
    if($number <= 0) {
        return '';
    }
    
    /*
     $tens = '十';
     
     $str = '';
     if($number > 10) {
     $tens_digit = floor($number / 10);
     if($tens_digit > 1) {
     $str .= $digits_arr[($tens_digit - 1)];
     }
     $str .= $tens;
     }
     */
    $units_digit = $number % 10;
    $str = $Lang['eSports']['RelayTeamNumber'][$units_digit];
    
    return $str;
}

$lclass = new libclass();
$lsports = new libsports();
$lsports->authSportsSystem();

$numberOfLane = $lsports->numberOfLanes;
$numOfAttempt = 3;

# 2020-01-31 Philips - MKSS Top3
if($_GET['top3']){
	$top3 = true;
}

$linterface = new interface_html();

# Get all Event Result 
$EventResultArr = $lsports->Get_All_Event_Round_Result();
$EventResultArr = BuildMultiKeyAssoc($EventResultArr, array("StudentID","EventGroupID","RoundType"), "EventResult", 1);

# Prepare $LastRoundMap
$EventInvolvedRound = $lsports->Get_EventGroup_Involved_Round();
//$LastRoundMap[ROUND_TYPE_FINALROUND] = ROUND_TYPE_SECONDROUND;
//$LastRoundMap[ROUND_TYPE_SECONDROUND] = ROUND_TYPE_FIRSTROUND;

// Print and Close button
$displayTable = "
	<table width='100%' align='center' class='print_hide' border=0>
	<tr>
		<td align='right'>".$linterface->GET_BTN($button_print, "button", "javascript:window.print();","submit2")."</td>
	</tr>
</table>";

$breakStyle = "style='page-break-after:always'";

$displayTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' valign='top'>";

//$EventGroupID = (array)$EventGroupID;
$EventGroupID = isset($EventGroupType)? $EventGroupType : $EventGroupID;
$EventGroupID = (array)$EventGroupID;
for($x=0; $x<sizeof($EventGroupID); $x++)
{
	$tf_flag = 0;
	$hr_flag = 0;
	$cr_flag = 0;
	$theID = $EventGroupID[$x];
	
	$temp = explode("_", $theID);
	$eventGroupID = $temp[0];
	$eventGroupID = IntegerSafe($eventGroupID);
	$roundType = $temp[1];
	$roundType = IntegerSafe($roundType);
	
    # Check Open Event
//	$isOpen = 0;
//	for($j=0; $j<sizeof($openGroupArr); $j++)
//	{
//		if($openGroupArr[$j] == $eventGroupID)
//		{
//			$isOpen = 1;
//			break;
//		}
//	}
	
	# Retreive EventID and GroupID
	$eventGroupInfo = $lsports->retrieveEventGroupDetail($eventGroupID);
	$eventID = $eventGroupInfo[1];
	$groupID = $eventGroupInfo[2];
	
	# Retrieve Event Info
	$eventInfo = $lsports->returnEventInfo($eventID);
	$eventType = $eventInfo[0];
	$eventName = $eventInfo[1];
	$isJump = $eventInfo[2];
	
	# Retrieve Group Name
	if($groupID > 0) {
		$groupName = $lsports->returnAgeGroupName($groupID);
	}
	else if($groupID=='-1') {
		$groupName = $i_Sports_Event_Boys_Open;
	}
	else if($groupID=='-2') {
		$groupName = $i_Sports_Event_Girls_Open;
	}
	else if($groupID=='-4') {
		$groupName = $i_Sports_Event_Mixed_Open;
	}
	
	# Retrieve Ext Info
	$ExtInfo = $lsports->retrieveEventGroupExtInfo($eventGroupID, $eventType);
	
	//$roundTypeArr = $temp[1];
	if(trim($roundType)=='')
	{
		if($eventType <= 2) {
			$roundTypeArr = $lsports->Get_Exist_Round_Of_EventGroup($eventGroupID);
			$roundTypeArr = array($roundTypeArr[sizeof($roundTypeArr)-1]);
		}
		else {
			$roundTypeArr = array(0);
		}
	}
	else
	{
		$roundTypeArr = $roundType;
	}
	
	foreach((array)$roundTypeArr as $roundType)
	{
		# Check if number of lanes of this round equal to lane number
		$isLaneNum = 0;
		if($eventType==1) {
			if(($ExtInfo["FirstRoundGroupCount"]==0 && $roundType==1) || ($ExtInfo["SecondRoundLanes"]==0 && $roundType==2) || ($ExtInfo["FinalRoundNum"]==0 && $roundType==0)) {
				$isLaneNum = 1;
			}
		}
		
		$roundName = "";
		if($roundType==1 && $ExtInfo["FinalRoundReq"]==1) {
			$roundName = "(".$i_Sports_First_Round.")";
		}
		else if($roundType==2) {
			$roundName = "(".$i_Sports_Second_Round.")";
		}
		else if($roundType==0) {
			$roundName = "(".$i_Sports_Final_Round.")";
		}
		
		if($eventType==2)
		{
			$record = $ExtInfo["RecordMetre"];
			$standard = $ExtInfo["StandardMetre"];
		}
		else
		{
			$record = $ExtInfo["RecordMin"];
			$record .= "'".$ExtInfo["RecordSec"];
			$record .= "''".$ExtInfo["RecordMs"];
	
			$standard = $ExtInfo["StandardMin"];
			$standard .= "'".$ExtInfo["StandardSec"];
			$standard .= "''".$ExtInfo["StandardMs"];
		}
		
		# Retrive total number of heat in this round
		//$sql = "SELECT MAX(Heat) FROM SPORTS_LANE_ARRANGEMENT WHERE EventGroupID = '$eventGroupID' AND RoundType = '$roundType'";
		//$maxHeat = $lsports->returnVector($sql);
		$maxHeat = $lsports->Get_Max_Heat_Of_Round($eventGroupID,$roundType);
		
		# Track and Field Events Table
		if($eventType==1 || $eventType==2)
		{
			$Arrange = $lsports->returnTFLaneArrangeDetailByEventGroupID($eventGroupID, 1, $roundType);
			$thisLastRound = $EventInvolvedRound[$eventGroupID][array_search($roundType,$EventInvolvedRound[$eventGroupID])-1];
			
			if(sizeof($Arrange)!=0)
			{
				// Add blank rows to Field Event 
				if($eventType==2 && !$isResult)
				{
					for($j=0; $j<2; $j++)
					{
						$emptyfield[0] = $Arrange[0][0]; 						// Assign Heat for empty field
						$emptyfield[1] = $Arrange[sizeof($Arrange)-1][1]+1; 	// Assign Arrange Order
						for($i=2; $i<4; $i++) {
							$emptyfield[$i] = "&nbsp;";
						}
						$Arrange[] = $emptyfield;
					}
				}
				
				$orderField = ($eventType==1) ? $i_Sports_Line : $i_Sports_Order;
				$Field_Round_Num = ($isJump==1 || $isResult) ? JUMP_NUMBER : FIELD_NUMBER;
				$spaceReq = ($eventType==1 || $isResult) ? 5 : $Field_Round_Num+4;
				$contentTable = "";
				$currHeat = "";

                $Arrange = SortByColumn($Arrange, 'Rank');
				for($k=0; $k<sizeof($Arrange); $k++)
				{
					list($heat, $order, $sname, $sid,$rank, $score, $trackresult, $fieldresult, $trial1, $trial2, $trial3, $s_ename, $s_cname, $s_classname, $s_classnumber) = $Arrange[$k];
					if($rank <= 0 || $rank > 3) {
					    continue;
                    }

					$nextHeat = $Arrange[$k+1][0];
					
					$athleticNum = $lsports->returnStudentAthleticNum($sid);
					$className =  $lsports->returnStudentClassName($sid);
					$house = $lsports->retrieveStudentHouseInfo($sid);
					$houseName = $house[0][0];
					
					if($heat != $currHeat || $currHeat == "")
					{
						##### 
						$displayTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' valign='top' $breakStyle>";
						$displayTable .= "<tr><td align='center'>\n";
						
						$displayTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>";
						$displayTable .= "<tr>";
						$displayTable .= "<td class='eSportprinttitle' colspan=2>".$i_Sports_Item.": <strong>".$eventName." ".$roundName."</strong></td>";
						$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Record.": ".$record."</td>";
						$displayTable .= "</tr>";
						$displayTable .= "<tr>";
						$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_field_Group.": <strong>".$groupName."</strong></td>";
						$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_field_Heat.": <strong>".$heat."/".$maxHeat[0]."</strong></td>";
						$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Standard_Record.": ".$standard."</td>";
						$displayTable .= "</tr>";
						$displayTable .= "</table>";
						
	                    $displayTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='eSporttableborder'>";
	                    $displayTable .= "<tr>";
	                    if($eventType==1 || $isResult)
	                    {
	                    	$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
	                    	if($eventType == EVENT_TYPE_FIELD && !$isJump)
	                    		for($t=1;$t<=$numOfAttempt;$t++)
	                    			$displayTable .= "<td width=80 class='eSporttdborder eSportprinttabletitle'>".$lsports->Get_Lang_Trial($t)."</td>";
                    			$displayTable .= "<td width=80 class='eSporttdborder eSportprinttabletitle'>".($eventType==2?$i_Sports_field_Result:$i_Sports_Time)."</td>";
                    			$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
// 	                    		$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Remark."</td>";
	                    }
	                    else
	                    {
	                    	for($n=1; $n<=$Field_Round_Num; $n++) {
	                    		$displayTable .= ($isJump==1) ? "<td width=45 colspan=3 class='eSporttdborder eSportprinttabletitle'>".$n."</td>" : "<td width=45 class='eSporttdborder eSportprinttabletitle'>".$n."</td>";
	                    	}
	                    	$displayTable .= "<td width=60 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Result."</td>";
	                    	$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
	                    	$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
	                    }
	                    $displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$orderField."</td>";
	                    $displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_ClassName."</td>";
	                    $displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_ClassNumber."</td>";
	                    $displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_House."</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_Participant_Number."</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['EnglishName']."</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$Lang['General']['ChineseName']."</td>";
						if($thisLastRound && !$isResult) {
							$displayTable .= "<td width=80 class='eSporttdborder eSportprinttabletitle'>".$Lang['eSports']['RoundResult'][$thisLastRound]."</td>";
						}
						$displayTable .= "<td width=35 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_Present."</td>";
						if($eventType==1 || $isResult)
						{
// 							$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
// 							if($eventType == EVENT_TYPE_FIELD && !$isJump)
// 								for($t=1;$t<=$numOfAttempt;$t++)
// 									$displayTable .= "<td width=80 class='eSporttdborder eSportprinttabletitle'>".$lsports->Get_Lang_Trial($t)."</td>";
// 							$displayTable .= "<td width=80 class='eSporttdborder eSportprinttabletitle'>".($eventType==2?$i_Sports_field_Result:$i_Sports_Time)."</td>";
// 							$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
							$displayTable .= "<td class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Remark."</td>";
						}
						else
						{
// 							for($n=1; $n<=$Field_Round_Num; $n++) {
// 								$displayTable .= ($isJump==1) ? "<td width=45 colspan=3 class='eSporttdborder eSportprinttabletitle'>".$n."</td>" : "<td width=45 class='eSporttdborder eSportprinttabletitle'>".$n."</td>";
// 							}
// 							$displayTable .= "<td width=60 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Result."</td>";
// 							$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
// 							$displayTable .= "<td width=40 class='eSporttdborder eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
						}
						$displayTable .= "</tr>";
						$currHeat = $heat;
						
						//if($isLaneNum==1)
						//{
						//	$diff = $order-1;
						//	for($a=1; $a<=$diff; $a++)
						//	{
						//		$displayTable .= "<tr>";
						//		$displayTable .= "<td class='eSporttdborder eSportprinttext'>".$a."</td>";
						//		for($m=1; $m<=$spaceReq+6; $m++) {
						//			$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
						//		}
						//		$displayTable .= "</tr>";
						//	}
						//}
					}
					
					$displayTable .= "<tr>";
					if($isResult)
					{
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($rank?$rank:"&nbsp;")."&nbsp</td>";
						if($eventType == EVENT_TYPE_FIELD && !$isJump)
							for($t=1; $t<=$numOfAttempt; $t++)
								$displayTable .= "<td width=80 class='eSporttdborder eSportprinttext'>".${"trial$t"}."&nbsp</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($eventType == 1?$trackresult:$fieldresult)."&nbsp</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>$score&nbsp</td>";
// 						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
					}
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>".$order."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($s_classname==""?"&nbsp;":$s_classname)."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($s_classnumber==""?"&nbsp;":$s_classnumber)."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($houseName==""?"&nbsp;":$houseName)."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($athleticNum==""?"&nbsp;":$athleticNum)."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($s_ename==""?"&nbsp;":$s_ename)."</td>";
					$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($s_cname==""?"&nbsp;":$s_cname)."</td>";
					
					if($isResult)
					{
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>"; // present
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>"; // remark
					}
					/*if($isResult)
					{
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>"; // present
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($rank?$rank:"&nbsp;")."&nbsp</td>";
						if($eventType == EVENT_TYPE_FIELD && !$isJump)
							for($t=1; $t<=$numOfAttempt; $t++)
								$displayTable .= "<td width=80 class='eSporttdborder eSportprinttext'>".${"trial$t"}."&nbsp</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>".($eventType == 1?$trackresult:$fieldresult)."&nbsp</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>$score&nbsp</td>";
						$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
					}
					else
					{
						if($isJump==1)
						{
							if($thisLastRound) {
								$displayTable .= "<td width=80 class='eSporttdborder eSportprinttext'>".$EventResultArr[$sid][$eventGroupID][$thisLastRound]."&nbsp;</td>";
							}
							$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
							for($m=1; $m<=$Field_Round_Num; $m++)
							{
								$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
								$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
								$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
							}
							$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
							$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
							$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp;</td>";
						}
						else 
						{
							if($thisLastRound) {
								$displayTable .= "<td width=80 class='eSporttdborder eSportprinttext'>".$EventResultArr[$sid][$eventGroupID][$thisLastRound]."&nbsp;</td>";
							}
							for($m=1; $m<=$spaceReq; $m++) {
								$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
							}
						}
					}*/
					$displayTable .= "</tr>";
					
					//if($heat != $nextHeat)
					//{
					//	if($isLaneNum==1)
					//	{
					//		for($a=($rank+1); $a<=$numberOfLane && $a < 4; $a++)
					//		{
					//			$displayTable .= "<tr>";
					//			$displayTable .= "<td class='eSporttdborder eSportprinttext'>".$a."</td>";
					//			for($m=1; $m<=$spaceReq+6; $m++) {
					//				$displayTable .= "<td class='eSporttdborder eSportprinttext'>&nbsp</td>";
					//			}
					//			$displayTable .= "</tr>";
					//		}
					//	}
					//	$displayTable .= "</table>";
					//	$displayTable .= "<table align=right border='0'>";
	                 //   $displayTable .= "<tr height=50><td width='120' colspan='2' align='center' class='eSportunderline'>&nbsp;</td></tr>";
					//	$displayTable .= "<tr><td align='center' class='eSportprinttext'>(</td><td width='120' class='eSportprinttext'>&nbsp;</td><td class='eSportprinttext'>)</td></tr>";
					//	$displayTable .= "<tr><td align='center' colspan='2' class='eSportprinttext'>TEACHER I/C</td></tr>";
					//	$displayTable .= "</table>";
					//
					//	$displayTable .= "</td></tr>";
					//	$displayTable .= "</table>";
					//}
				}
			}
		}
		# House / Class Relay Event Tables
		else if($eventType==3 || $eventType==4)
		{
			if($eventType==3)
			{
			    $Arrange = $lsports->returnHRLaneArrangeDetailByEventGroupID($eventGroupID);
			    
			    // [DM#3631]
			    if($sys_custom['eSports']['HouseTeamNameDisplay'])
			    {
			        $houseArrangedArr = array();
			        $houseLineArrangeArr = array();
			        for($i=0; $i<sizeof($Arrange); $i++)
			        {
			            list($hname, $order, $hid, $h_color) = $Arrange[$i];
			            if(empty($houseArrangedArr[$hname])) {
			                $houseArrangedArr[$hname] = 0;
			            }
			            $houseArrangedArr[$hname]++;
			            $houseLineArrangeArr[$i] = $houseArrangedArr[$hname];
			        }
			    }
			}
			else
			{
				$Arrange = $lsports->returnCRLaneArrangeDetailByEventGroupID($eventGroupID);
			}
			$Arrange = SortByColumn($Arrange, 'Rank');
			$countLane = 1;
			if(sizeof($Arrange)!=0)
			{
				$displayTable .= "<table width='100%' border='0' cellpadding='0' cellspacing='0' align='center' valign='top' $breakStyle>";
				$displayTable .= "<tr><td align='center'>\n";
				
				$displayTable .= "<table width='100%' border='0' cellpadding='3' cellspacing='0' align='center'>";
				$displayTable .= "<tr>";
				$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Item.": <strong>".$eventName." ".$roundName."</strong></td>";
				$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Record.": <strong>".$record."</strong></td>";
				$displayTable .= "</tr>";
				$displayTable .= "<tr>";
				$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_field_Group.": <strong>".$groupName."</strong>	</td>";
				$displayTable .= "<td class='eSportprinttitle'>".$i_Sports_Standard_Record.": <strong>".$standard."</strong></td>";
				$displayTable .= "</tr>";
				$displayTable .= "</table>";
				
				$displayTable .= "<table width='100%' border='0' cellpadding='4' cellspacing='0' class='eSporttableborder'>";
				$displayTable .= "<tr>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_field_Rank."</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_Time."</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_field_Score."</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_Line."</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".($eventType==3?$i_Sports_House:$i_general_class)."</td>";
				$displayTable .= "<td class='eSporttdborder  eSportprinttabletitle'>".$i_Sports_field_Remark."</td>";
				$displayTable .= "</tr>";
				
				for($j=0; $j<sizeof($Arrange); $j++)
				{
					if($eventType==3) {
					    list($hname, $order, $hid, $colorCode, $Rank, $Score, $TrackResult) = $Arrange[$j];
                        if($Rank <= 0 || $Rank > 3) {
                            continue;
                        }
					    
					    // [DM#3631]
					    if($sys_custom['eSports']['HouseTeamNameDisplay'] && $houseArrangedArr[$hname] > 1) {
					        $hname .= convertToChineseNumber($houseLineArrangeArr[$j]);
					    }
					}
					else {
						list($hname_en,$hname_b5, $order, $hid, $Rank, $Score, $TrackResult) = $Arrange[$j];
                        if($Rank <= 0 || $Rank > 3) {
                            continue;
                        }

						$hname = Get_Lang_Selection($hname_b5,$hname_en);
						$Score = ''; // class do not count score
					}
					$nextExist = (sizeof($Arrange[$j+1])==0) ? 0 : 1;
					$displayTable .= "<tr>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".($isResult?$Rank:"")."&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".($isResult?$TrackResult:"")."&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".($isResult?$Score:"")."&nbsp;</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".$order."</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".$hname."</td>";
					$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					$displayTable .= "</tr>";
					$countLane++;
					
					//if($nextExist==0 && $order!=$numberOfLane)
					//{
					//	for($k=($Rank+1); $k<=$numberOfLane && $k < 4; $k++)
					//	{
					//		$displayTable .= "<tr>";
					//		$displayTable .= "<td class='eSporttdborder  eSportprinttext'>".$k."</td>";
					//		$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					//		$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					//		$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					//		$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					//		$displayTable .= "<td class='eSporttdborder  eSportprinttext'>&nbsp;</td>";
					//		$displayTable .= "</tr>";
					//	}
					//}
				}
				$displayTable .= "</table>";
				$displayTable .= "<table align=right border=0>";
				$displayTable .= "<tr height=50><td width='120' colspan='2' align='center' class='eSportunderline'>&nbsp;</td></tr>";
				$displayTable .= "<tr><td align='center' class='eSportprinttext'>(</td><td width='120' class='eSportprinttext'>&nbsp;</td><td class='eSportprinttext'>)</td></tr>";
				$displayTable .= "<tr><td align='center' colspan='2' class='eSportprinttext'>TEACHER I/C</td></tr>";
				$displayTable .= "</table>";
				
				$displayTable .= "</td></tr>";
				$displayTable .= "</table>";
			}
		}
	}
}
$displayTable .= "</table>";

echo $displayTable;

include_once($PATH_WRT_ROOT."/templates/".$LAYOUT_SKIN."/layout/print_footer.php");
intranet_closedb();
?>